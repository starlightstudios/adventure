--Prototype To Do List
--Set [COMMENTDESCRIPTION] to describe the intended effect of the skill. If the intended effect is different from the actual effect, a bugfixer will refer to intended version.
--Set [SKILLOWNER] with the owner of the skill. This is the character name, such as Mei, or Enemies if this is an enemy skill.
--Set [SKILLCLASS] with the class of the skill. For enemies this is usually a chapter name such as Ch1.
--Set [SKILLNAME] with the name of the skill, no spaces. Can include spaces.
--Set [SKILLNAMENOSPACES] with the name of the skill, no spaces. This is used for lua variables.
--If the skill requires a display name different from the skill name, change "Use Skill Name" to the desired name.
--Replace [ICONPATH] with the path of the icon. Ex: Mei|Fencer|Blind.
--Set [MPCOST], [CPCOST], [COOLDOWN], [CPGEN], and [TARGET FLAG]. Use giNoMPCost, giNoCPCost, giNoCooldown, giNoCPGeneration if needed.
--Set [DAMAGETYPE] (series: gciDamageType_Slashing), [MISSRATE] (default is 5), [DAMAGEFACTOR] (normal attack is 1.00).
--Set [EFFSTR] and [EFFSTRCRIT]. Set [EFFTYPE] (can be different from damage type). Set [EFFTITLE] and [EFFTITLECRIT]. Set [EFFCOLOR] (debuffs are usually Color:Purple).
--Set [ADDBONUSMALUSTAGS]. These are optional. Don't forget some standards such as {"Blind Standard"}.
--Set [EFFECTUNIQUENAME]. To avoid name conflicts, this is usually owner.class.skillname, ex: Mei.Fencer.Blind. However, this is not strict, it just has to be unique!
--Set [DURATION] and [DURATIONCRIT]. Set [APPLYSTRENGTH] and [APPLYSTRENGTHCRIT].
--Set [STATSTRING]. Ex: Acc|Flat|-50 to reduce accuracy by 50.
--Set [INSPECTORDESC1], [INSPECTORDESC2], etc.
--Set [EFFTAGS]. Can be {}. Optional.
--Set [VOICEDATA], or comment out the line if voice handling is not ready. If the skill is intended not to have voice at all, delete the line.
--PROTOTYPE SHOULD REMOVE (This string is searchable, meant to check for cases where a user did not remove the instructions from a prototype)
--Delete these instructions.

--Note: Add flag samples and versioning system.

-- |[ ==================================== [SKILLNAME] ==================================== ]|
-- |[Description]|
--[COMMENTDESCRIPTION]

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.[SKILLOWNER].[SKILLCLASS].[SKILLNAMENOSPACES] == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("[SKILLCLASS]", "[SKILLNAME]", "Use Skill Name", gciJP_Cost_Normal, gbIsNotFreeAction, "Debuff", "Active", "[ICONPATH]", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\nEffect description here.\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Description Line 1.\nDescription Line 2.\n\n\n\nCosts [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability([MPCOST], [CPCOST], [COOLDOWN], "[TARGET FLAG]", [CPGEN])
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Pierce")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage([DAMAGETYPE], [MISSRATE], [DAMAGEFACTOR])
    
    -- |[Execution Effect Package]|
  --zAbiStruct:fnAddEffect(piStrNormal, piStrCrit, piApplyType, psApplyText, psCritApplyText, psApplyColorString, $psaApplyBonus, $psaApplyMalus, $psaSeverityBonus, $psaSeverityMalus)
    zAbiStruct:fnAddEffect([EFFSTR], [EFFSTRCRIT], [EFFTYPE], "[EFFTITLE]", "[EFFTITLECRIT]", "[EFFCOLOR]", [ADDBONUSMALUSTAGS])
    
    -- |[Use of Effect Prototype]|
    zAbiStruct.zExecutionEffPackage.sPrototypeName = "[EFFECTUNIQUENAME]"

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
        local saDescription = {}
        saDescription[1] = "[INSPECTORDESC1]"
        saDescription[2] = "[INSPECTORDESC2]"
      --EffectList:fnCreateStatPrototype(pzEffectpackage, psName, pbIsBuff, psDisplayName, psIcon, piTurns, piApplyStrength, psStatString, psaDescription, $pzaTagList, $piCritTurns, $piCritApplyStrength)
        EffectList:fnCreateStatPrototype(zAbiStruct.zExecutionEffPackage, sLocalPrototypeName, gbIsDebuff, [SKILLNAME], "[ICONPATH]", [DURATION], [APPLYSTRENGTH], "[STATSTRING]", saDescription, {}, [DURATIONCRIT], [APPLYSTRENGTHCRIT])
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "[VOICEDATA]"

    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.[SKILLOWNER].[SKILLCLASS].[SKILLNAMENOSPACES] = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.[SKILLOWNER].[SKILLCLASS].[SKILLNAMENOSPACES]

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
