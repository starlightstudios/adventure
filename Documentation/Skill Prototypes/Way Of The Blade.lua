-- |[ ==================================== Way of the Blade ==================================== ]|
-- |[Description]|
--Passive, increases Slashing resistance by 5 and slash damage by 15%.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.Fencer.WayOfTheBlade == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Fencer", "Way Of The Blade", "Way of the Blade", gciJP_Cost_Passive, gbIsNotFreeAction, "Buff", "Passive", "Mei|Fencer|WayOfTheBlade", "Passive")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "+5[Slsh] resistance, +15% [Slsh] damage.\n\n\n\n\n\nPassive."
    zAbiStruct.sSimpleDescMarkdown  = "Greatly increases [Slsh](Slash) Resistance and\nDamage.\n\n\n\nPassive."
    
    -- |[Passive Effect]|
    zAbiStruct.bNeverAvailable   = true --Passive abilities have no usability requirements.
    zAbiStruct.sPassivePrototype = "Mei.Fencer.WayOfTheBlade"
    zAbiStruct.bHasGUIEffects    = true
    
    -- |[Tags]|
    --Apply tags to increase slashing damage.
    zAbiStruct.zaTags = {{"Slash Damage Dealt +", 15}}

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    -- Note: The effect doesn't increase slash damage, the ability itself does.
    if(EffectList:fnEntryExists(zAbiStruct.sPassivePrototype) == false) then

        -- |[Creation]|
        --This effect uses a standardized StatMod script.
        local sDisplayName = "Way of the Blade"
        local iDuration = -1
        local bIsBuff = true
        local sIcon = "Root/Images/AdventureUI/Abilities/Mei|Fencer|WayOfTheBlade"
        local sStatString = "ResSls|Flat|5"

        --Needs to manually set the description to account for the tag damage.
        local saDescription = {}
        saDescription[1] = "Mastery of the blade leads to a resistance and"
        saDescription[2] = "insistence on cuts. Increases [Slsh](Slash) resist"
        saDescription[3] = "and damage."
        saDescription[4] = ""
        saDescription[5] = ""
        saDescription[6] = "[Buff] [Slsh]+5Res +15Dam"
        saDescription[7] = ""

        --Run the standardized builder.
        local zPrototype = fnCreateStatmodEffectPrototype(sDisplayName, iDuration, bIsBuff, sIcon, sStatString, saDescription, true)

        -- |[Overrides]|
        --Passive, not removed on KO, uses different frame.
        zPrototype.sFrame      = gsAbility_Frame_Passive
        zPrototype.bRemoveOnKO = false
    
        -- |[Register]|
        fnRegisterEffectPrototype("Mei.Fencer.WayOfTheBlade", gciEffect_Is_StatMod, zPrototype)
    
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.Fencer.WayOfTheBlade = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.Fencer.WayOfTheBlade

-- |[ ===================================== All Other Cases ==================================== ]|
if(iSwitchType ~= gciAbility_GUIApplyEffect) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

-- |[ ==================================== GUI: Apply Effect =================================== ]|
--When showing a character's statistics, all equipped abilities are queried. This is not reflected
-- in combat, this is GUI only.
--The standard does not handle this.
elseif(iSwitchType == gciAbility_GUIApplyEffect) then
    AdvCombatAbility_SetProperty("Push Owner")
        fnApplyStatBonus(gciStatIndex_Resist_Slash, 5)
    DL_PopActiveObject()
    gzRefAbility = nil
end
