-- |[ ================================ AI Script Documentation ================================= ]|
--This is documentation concerning Starlight Engine's AIScript class and its numerous properties.
-- It is a lua file as it contains actual executable code samples that could be dropped right into
-- an AIScript file.

-- |[ =================================== Order of Operation =================================== ]|
--Sample.
sString = 
"[GVAR,iHasRunOnce,N:==:0]{SVAR:iHasRunOnce:N:1}{SVAR:iHowlCooldown:N:Rand,1,1} || " ..
"{TOZERO:iHowlCooldown:1} || " ..
"[GVAR,iHowlCooldown,N:<=:0][LowestHPPercent,Friendly:<=:50]{SVAR:iHowlCooldown:N:3}{Prime:Enemy|Rallying Howl}{Target:Lowest Health}{Finish} || " ..
"[GVAR,iHowlCooldown,N:<=:0][LowestHPPercent,Friendly:>:50] {SVAR:iHowlCooldown:N:3}{Prime:Enemy|Piercing Howl}{Target:Random}{Finish} || " ..
"[GVAR,iAttackRoll,N:<:70]{Prime:Enemy|Claw Attack}{Target:Threat}{Finish} || "..
"{Prime:Enemy|Werewolf Infect}{Target:Threat}{Finish}"
AIString:new("Howling Wolf", sString)

--AIStrings are broken into sequences. They are read left to right, then top to bottom. The end of a sequence is denoted by a "||".
-- For simplicity these are usually at the end of a line, but the string:
sString = 
"[GVAR,iAttackRoll,N:<:70]{Prime:Enemy|Claw Attack}{Target:Threat}{Finish} || {Prime:Enemy|Werewolf Infect}{Target:Threat}{Finish}"

--Is also totally valid.

--Each sequence is broken into a set of chunks, denoted by [] and {}.
--A [] indicates that this is a test. Each test is run left to right. If all tests pass, then the actions, {}, execute.
--If any test fails, then the entire sequence stops and proceeds to the next one. That is, these are a series of logical AND cases.
--Therefore, sequences are put in order of priority. At the end of a sequence, the {Finish} chunk stops AI execution. 
--It is possible for multiple sequences to execute if they all resolve to true, the AI continues to run until it runs out of sequences
-- or hits a {Finish} chunk.

--You can make an enemy perform multiple actions in a single turn by not using a {Finish} tag. To do this safely, after the {Target} 
-- call, use {Continue}. Failing to use the continue tag will print warnings.

--The {Prohibit} chunk will allow the AIString to continue executing, but will block all Prime and Target actions from occurring.
-- Use this to allow an "ending" to an AIString, where common code executes at the end but you don't need to worry about additional
-- actions firing.

-- |[ ======================================== Queries ========================================= ]|
--All queries are done with a [] chunk. The format is:
-- [TERM,argument,argument,argument:COMPARISON:TERM,argument,argument]
--Note that not all Term cases require arguments.

--A simple example is done with constants.
-- [30:>=10]
--This asks if the number 30 is greater than or equal to 10. It is, so the query is true.

--Comparisons are of the following:
-- ==           Exactly equal to
-- >=           Greater than or equal to
-- <=           Less than or equal to
-- >            Greater than
-- <            Less than
-- !=           Is not equal to
-- ~=           Is not equal to

--Typically, Term that are functions will resolve to a number which can then be compared.

--Note that a Term can be a function which performs an operation in addition to returning a value.
-- That is, it is possible for a Term to return a number AND also set a number or activate some
-- functionality. These are scripts, anything is possible.

-- |[ ======================================= Executions ======================================= ]|
--Executions are done with a {} chunk. The format is:
-- {EXECUTION:argument:argument:argument}
--Once again, not all Executions require arguments.

--Executions are one-per-chunk, no comparisons are required. It is assumed that all queries are
-- run before any executions run, but there's actually no need to do that.

-- |[ ======================================= Variables ======================================== ]|
--[GVAR,(VariableName),(N/S Type):==:0]
--{SVAR:VarName:N/S:Value}

--Variables are typically numbers (but can be strings) that are stored LOCALLY by the enemy. That is,
-- if there are multiple enemies of the same type on the field, each can store its own version of
-- a variable and act independently of the others.
--Variables are used to allow entities to store cooldowns, but can can be used for anything you can
-- think of. Variables are queried by GVAR and set by SVAR.

-- |[ =================================== Priming, Targeting =================================== ]|
--One of the most important things an entity can do is attack the player or heal themselves. You know,
-- enemy stuff. To do this, an ability must be 'Primed' and then select a 'Target'. Priming will cause
-- the available targets to be populated for selection. For example, an ability that targets all hostile
-- entities will be primed and create a list of all entities in the enemy party. Self-target abilities
-- typically create a target list that includes the user.
sString = 
"{Prime:Enemy|Werewolf Infect}{Target:Threat}{Finish}"
AIString:new("Howling Wolf", sString)

--This very simple example primes the ability "Enemy|Werewolf Infect" and then orders it to target
-- by "Threat". Threat is a value each enemy stores for each player character. The highest threat is
-- most likely to be attacked, but there is a random element here.

local sString = "{Prime:By Roll}{Target:Threat}{Finish}"
AIString:new("Enemy Standard", sString)

--When constructing enemies, a number gets associated with each ability they have. This is the
-- weighted percentage chance of selecting that ability. Using {Prime:By Roll} will randomly select
-- an ability and prime it. This allows for very hands-off AIs since most of the work is done by
-- initialization.

-- |[ ================================ Lists and List Handling ================================= ]|
--Sometimes, you need to create more advanced target handling. There are several pre-built target
-- handlers but inevitably you're going to need complex AND and OR handlers. This is what lists
-- are for.

--The program tracks an invisible global list. There is ONLY ONE so all operations take place on
-- the same list.
--To reset the list, use:
--{List:New List:(PartyType)}

--(PartyType) is a term used to figure out who is subject to being on the list initially. It is
-- an optional argument. Using {List:New List} will create a blank list that you can add to.

--List of all PartyType values:
--"Combat"          --The combat party is the player's party BEFORE any team switching occurs. The combat part equals the active party at combat start.
--"Active"          --The active party is the player's party as it currently exists, including summoned allies.
--"Enemy"           --The enemy party is the bad guys that AIs usually control.
--"Hostile"         --The hostile party is whatever party that opposes the AI. While that's usually the player's party, AIScripts can work on summoned allies too.
--"Friendly"        --As above except the party the AI is currently in.

--One a party has been assembled, it's time to edit the list.
--{List:Remove With Tag:(Tag Name)}
--This execution will remove any entities currently on the list who have the listed tag. For example, if an ability can only be used against enemies
-- that cannot see (are blind), you might remove the tag "Is Able To See".

--{List:Remove Without Tag:(Tag Name)}
--Removes entities that do not have the given tag.

--{List:Remove Self}
--Removes the current AI entity from the list. Useful for abilities that can only be used on an ally but not on self.

--Once you are done setting up the list, use the chunk {Target:Use List} to make the primed ability use the list to pick a target.
-- All targets on the list get equal chance of being selected.

-- |[ ============================= Resettables and Other Globals ============================== ]|
--To allow some common behaviors and allow enemies to perform basic teamwork, there are certain global
-- values and common variables that AIs can access.

-- |[iAttackRoll]|
--At the start of their turn, every entity rolls the variable iAttackRoll from 1 to 100. You can use
-- this to easily set your own weighted odds of picking certain abilities.
--The below AI sample has a 70% chance to use "Enemy|Claw Attack" and a 30% chance to use "Enemy|Werewolf Infect".
sString =
"[GVAR,iAttackRoll,N:<:70]{Prime:Enemy|Claw Attack}{Target:Threat}{Finish} || "..
"{Prime:Enemy|Werewolf Infect}{Target:Threat}{Finish}"
AIString:new("Calling Wolf", sString)

-- |[Global Variables]|
--[GetGlobal,(VariableName),(N/S Type):==:0]
--{SetGlobal:VarName:N/S:(Value)}

--Global variables are the same as conventional variables except they are not local to each enemy on
-- the field. Instead, all enemies retrieve from the same global pool of variables. This allows
-- enemies to coordinate between one another. If one uses a healing skill, they can set a global
-- that tells the other enemies not to use a healing skill the same turn.

-- |[Resettable Variables]|
--{InitResettable:VarName:N/S:(Value)}
--[GetResettable,(VariableName),(N/S Type):==:0]
--{SetResettable:VarName:N/S:Value}

--Resettables are global variables that automatically reset to 0 or "Null" at the start of a new turn,
-- depending on their type. They are otherwise the same as global variables.

--These allow you to easily coordinate multiple enemies not to spam powerful skills by limiting them
-- and setting a resettable to a non-zero value, then querying that same variable in the next AI script.

--You must use the {InitResettable} call before a resettable can be used. This is typically done once
-- at combat start by the first AI to execute. You can use a global variable to make sure only the
-- first AI to execute primes the resettable.

-- |[ ============================= Incrementing and Decrementing ============================== ]|
--You can use SVAR to directly set variables with increases. But that's for lame-o's. The AIScript
-- comes with several easily used functions that can perform common modifications for you.

--Since variables are often used for cooldowns, you may never need to perform real math.

--{INCR:VarName:Amount}
--Increments the given variable by the given amount.

--{DECR:VarName:Amount}
--Decrements the given variable by the given amount. Can go negative.

--{TOZERO:VarName:Amount}
--Moves the variable towards zero by the given amount, and locks it at zero if it would pass zero.
-- Very useful for cooldowns, prevents you from accidentally going over zero.
-- Note: This can actually increment if the variable started as a negative.

-- |[ ====================================== Diagnostics ======================================= ]|
--Made a series of improbably mistakes and screwed up royally? Diagnostics are here to help.

--{Print:'Dogs are cute!'}
--Prints the given string (remember to surround the string with '') when the chunk executes. Can be
-- used to see if a given chunk is executing or getting stopped by a condition.

--{PrintTerm:Rand,(LoRange),(HiRange)}
--Evaluates the provided term, then prints the result. In this case, will print a random number.
-- You can use this to check what the result of a Term is if you're not sure if it's -1 or 0.

--{ReportGlobals}
--Prints a list of all globals and resettables and what their current values are.




















