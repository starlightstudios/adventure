-- |[ ===================================== AI String Terms ==================================== ]|
--This is a list of all Terms which can be used in a query chunk, as well as what they may return
-- on error or in other such cases.

-- |[ ======================== Constants ========================= ]|
--A term can just be a number or a string.
--[30:==:0]

--Strings are formatted thusly:
--["StringHere":==:0]

--Strings don't do a whole lot when put into a query but hey, you can do it.

-- |[ =================== Generative Functions =================== ]|
-- |[Random Number]|
--Format: [Rand,(LoRange),(HiRange):==:0]
--Returns a random integer between the low and high range. If the ranges are reversed, they will swap.
-- The ranges must be constants.

-- |[ ===================== Variable Queries ===================== ]|
-- |[Get Variable From Local Entity]|
--Format: [GVAR,(VariableName),(N/S Type):==:0]
--Returns the value of the named variable. Typically this is used for numbers. If the variable does not
-- exist it will always return 0.
    
-- |[Get Variable From Global]|
--Format: [GetGlobal,(VariableName),(N/S Type):==:0]
--Same as GVAR above, except retrieves entries from "Root/Variables/Combat/AIGlobals/".
    
-- |[Get Variable From Global Resettable]|
--Format: [GetResettable,(VariableName),(N/S Type):==:0]
--Same as GVAR above, except retrieves entries from "Root/Variables/Combat/ResetAtTurnStart/", which is specified to reset at
-- the start of every turn.

-- |[ ======================== List Terms ======================== ]|
-- |[Length of Generated List]|
--Format: [ListSize:==:0]
--When a list is generated and then pruned, you can use this function to get how many entries are on the list. Can be 0, never negative.

-- |[ ======================== Properties ======================== ]|
--These functions check some property in the caller or a party member. Properties can be anything from HP to tags to infections (WHI only).

-- |[Get Tag Count, Self]|
--Format: [SelfTagCount,(Tag Name):==:0]
--Returns the number of times a given tag appears on the acting entity. Can be used for self-buff checks. For example, if a buff has the 
-- tag "Is Empowered", you can check if that tag has a count >= 1, and if so, not use the buff again.
    
-- |[Function: Get HP Percent, Self]|
--Format: [SelfHPPct:==:0]
--Returns the HP percent of the caller, as a number between 0 and 100.
--It is possible for an entity to run its AI at 0 HP but usually you should consider than an impossibility.

-- |[Slot of Character]|
--Format: [GetPartySlot,(Party Type),ByName,(Character Name):==:0]
--Format: [GetPartySlot,(Party Type),WithTag,(Tag Name):==:0]
--Which slot a character with a property is in. Properties can include things like names or tags. Disabled
-- combat party members are trimmed by default.
--Set the AI String Basics.lua file for a list of party type strings.
--If the given character is not found, or there is nobody with a tag, returns -1.
    
-- |[Lowest HP Percent in Given Party]|
--Format: [LowestHPPercent,(Party Type):==:0]
--Finds the entity with the lowest HP and returns the percentage value (from 0 to 100, integers) of their current health, in the provided party.
--Note: This skips entities with 0 HP (knocked out) in case of party members. Enemies are removed from the enemy party entirely at 0HP.
--You can use this to check if *anyone* in the party is below a threshold.

-- |[Function: Size Of Party]|
--Format: [SizeOfParty,(Party Type):==:0]
--Returns the size of the listed party. Always at least 1 or the battle is probably over. Keep in mind that the player characters are not
-- removed from the party if knocked out, but enemies are.

-- |[HP by Name]|
--Format: [HPByName,(Party),(Entity Name):==:0]
--Get the HP of a named entity. If the entity is not found, returns 0.
--This is the actual HP value, it is NOT a percentage.

-- |[HP by Slot]|
--Format: [HPBySlot,(Party),Slot:==:0]
--Get the HP of the entity in the given slot. Note that Active Party is always in the same order
-- but Combat Party changes as characters rotate in and out, and Combat Party typically has a fixed
-- max size. If a slot is empty, 0 is returned. Note that slot counts start at 0.
--This is the actual HP value, it is NOT a percentage.

--In normal WHI gameplay, Izana is in slot 0, Caelyn 1, Cyrano 2. With party switching once Yuki or a
-- modded character joins, THIS CAN CHANGE.

-- |[HP Percent by Slot]|
--Format: [HPPctBySlot,(Party),Slot:==:0]
--As above but returns the HP Percent, integer from 0 to 100.

-- |[Infection by Name]|
--Format: [InfectionByName,(Entity Name):==:0]
--Get the slot of the infection afflicting the named character. If no infection, it's slot 0. Infections are used in
-- Witch Hunter Izana, so using this in any other game will always return 0. A list of infections is in System/203 Infection Packs.lua
--You can use this to make enemies not 'fight' over infections and only infect when a party member is vulnerable to their infection.

-- |[Infection by Combat Slot]|
--Format: [InfectionBySlot,(Entity Slot):==:0]
--As above except uses combat party slots. Infections can't affect enemies so only the player's party can be queried.

-- |[Infection Level by Name]|
--Format: [InfectionLevelByName,(Entity Name):==:0]
--Get the value of the infection currently afflicting the named character. 0 means no infection.
    
-- |[Infection Level by Combat Slot]|
--Format: [InfectionLevelBySlot,(Entity Slot):==:0]
--Get the value of the infection currently afflicting the slotted character. 0 means no infection.

--If you want more functions for some weird ramshackle project, bug Salty or maybe take up the mantle of Lua coder and add it yourself.