-- |[ ================================= Chapter 1 Map Assembly ================================= ]|
--Compile all the map load instructions into a single autoloader SLF file.
--This must be called AFTER all building is done AND all datafiles are copied. In order to correctly
-- clone an autoloader list, the datafile must be in its final destination so it can be read and
-- its autoloader lump queried.
local sExpectedDatafilePath = "../../Games/AdventureMode/Datafiles/"

-- |[ ======================================== Function ======================================== ]|
local function fnCloneAutoloadFromFile(psFilePath, psAutoLoadName, psAutoLoadDestination)

    --Query the file.
    SLF_QueryAutoLoadInFile(psFilePath, psAutoLoadName)
    
    --Copy all elements from source to destination. 
    local iCount = SLF_GetQueryCount()
    for q = 0, iCount-1, 1 do
        local sName, sPath = SLF_GetQueryEntry(q)
        AutoLoader_Register(psAutoLoadDestination, sName, sPath)
    end
end

-- |[ ===================================== File Creation ====================================== ]|
--Create the file.
SLF_Open("Output/Maps_CH1_Autoloader.slf")

--Create the execution lump.
local sImmediateLump = "AutoLoad|Adventure|Maps_Ch1_Immediate"
SLF_RegisterAutoLoadLump(sImmediateLump)

--Trannadar File
AutoLoader_Register(sImmediateLump, "FILESWITCH", "Datafile Maps Ch1 Trannadar")
fnCloneAutoloadFromFile(sExpectedDatafilePath .. "Maps_Ch1_Trannadar.slf", "AutoLoad|Adventure|Map_Trannadar", sImmediateLump)

--Trannadar West File
AutoLoader_Register(sImmediateLump, "FILESWITCH", "Datafile Maps Ch1 Trannadar West")
fnCloneAutoloadFromFile(sExpectedDatafilePath .. "Maps_Ch1_TrannadarWest.slf", "AutoLoad|Adventure|Map_TrannadarWest", sImmediateLump)

--Mausoleum File
AutoLoader_Register(sImmediateLump, "FILESWITCH", "Datafile Maps Ch1 Mausoleum")
fnCloneAutoloadFromFile(sExpectedDatafilePath .. "Maps_Ch1_Mausoleum.slf", "AutoLoad|Adventure|Map_Mausoleum", sImmediateLump)

--Nix Nedar File
AutoLoader_Register(sImmediateLump, "FILESWITCH", "Datafile Maps Nix Nedar")
fnCloneAutoloadFromFile(sExpectedDatafilePath .. "Maps_NixNedar.slf", "AutoLoad|Adventure|Background_NixNedar", sImmediateLump)

--System Parts.
AutoLoader_Register(sImmediateLump, "FILESWITCH", "Datafile Maps System")
fnCloneAutoloadFromFile(sExpectedDatafilePath .. "Maps_System.slf", "AutoLoad|Adventure|Map_Ch1Pieces", sImmediateLump)

-- |[ ======================================= Finish Up ======================================== ]|
SLF_Close()
