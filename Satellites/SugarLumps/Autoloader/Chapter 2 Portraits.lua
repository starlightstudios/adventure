-- |[ ============================== Chapter 2 Portrait Assembly =============================== ]|
--Compile all the portraits needed for chapter 2 into two autoloader lumps. The lumps will either
-- load immediately, or as-needed, based on the game options.
--This must be called AFTER all building is done AND all datafiles are copied. In order to correctly
-- clone an autoloader list, the datafile must be in its final destination so it can be read and
-- its autoloader lump queried.
local sExpectedDatafilePath = "../../Games/AdventureMode/Datafiles/"

-- |[ ================================ File Creation and Setup ================================= ]|
--Create the file and the Immediate/AsNeeded autoloader lumps.
SLF_Open("Output/Portraits_CH2_Autoloader.slf")

--Create a class handler.
local zAutoloadTable = AutoloadTable:new()

-- |[ ================================ Create Autoload Entries ================================= ]|
--Each autoload entry is something the program can call to immediately execute all of the instructions
-- stored within the entry. 

-- |[Prototype]|
--AutoloadTable:fnRegisterAutoloadLump(psLumpName)

-- |[Basic Lumps]|
zAutoloadTable:fnRegisterAutoloadLump("AutoLoad|Adventure|Portrait_Ch2_Immediate")     --Always loads immediately at chapter boot
zAutoloadTable:fnRegisterAutoloadLump("AutoLoad|Adventure|Portrait_Ch2_AsNeeded")      --Loads on-the-fly when a character appears

-- |[Sanya's Lumps]|
--Sanya Emotes. Loaded by costume orders.
zAutoloadTable:fnRegisterAutoloadLump("AutoLoad|Adventure|Portrait_Ch2_Emt_Sanya_Bunny")
zAutoloadTable:fnRegisterAutoloadLump("AutoLoad|Adventure|Portrait_Ch2_Emt_Sanya_Harpy")
zAutoloadTable:fnRegisterAutoloadLump("AutoLoad|Adventure|Portrait_Ch2_Emt_Sanya_Human")
zAutoloadTable:fnRegisterAutoloadLump("AutoLoad|Adventure|Portrait_Ch2_Emt_Sanya_HumanNoRifle")
zAutoloadTable:fnRegisterAutoloadLump("AutoLoad|Adventure|Portrait_Ch2_Emt_Sanya_HumanWerebatA")
zAutoloadTable:fnRegisterAutoloadLump("AutoLoad|Adventure|Portrait_Ch2_Emt_Sanya_HumanWerebatB")
zAutoloadTable:fnRegisterAutoloadLump("AutoLoad|Adventure|Portrait_Ch2_Emt_Sanya_Kitsune")
zAutoloadTable:fnRegisterAutoloadLump("AutoLoad|Adventure|Portrait_Ch2_Emt_Sanya_Sevavi")
zAutoloadTable:fnRegisterAutoloadLump("AutoLoad|Adventure|Portrait_Ch2_Emt_Sanya_SevaviNoRifle")
zAutoloadTable:fnRegisterAutoloadLump("AutoLoad|Adventure|Portrait_Ch2_Emt_Sanya_Werebat")

--Sanya Combat. Loaded by costume orders.
zAutoloadTable:fnRegisterAutoloadLump("AutoLoad|Adventure|Portrait_Ch2_Cbt_Sanya_Bunny")
zAutoloadTable:fnRegisterAutoloadLump("AutoLoad|Adventure|Portrait_Ch2_Cbt_Sanya_Harpy")
zAutoloadTable:fnRegisterAutoloadLump("AutoLoad|Adventure|Portrait_Ch2_Cbt_Sanya_Human")
zAutoloadTable:fnRegisterAutoloadLump("AutoLoad|Adventure|Portrait_Ch2_Cbt_Sanya_HumanNoRifle")
zAutoloadTable:fnRegisterAutoloadLump("AutoLoad|Adventure|Portrait_Ch2_Cbt_Sanya_Kitsune")
zAutoloadTable:fnRegisterAutoloadLump("AutoLoad|Adventure|Portrait_Ch2_Cbt_Sanya_Sevavi")
zAutoloadTable:fnRegisterAutoloadLump("AutoLoad|Adventure|Portrait_Ch2_Cbt_Sanya_SevaviNoRifle")
zAutoloadTable:fnRegisterAutoloadLump("AutoLoad|Adventure|Portrait_Ch2_Cbt_Sanya_Werebat")

-- |[Other Major Character Lumps]|
--Emotes/Combats for not-Sanya, still loaded by costume orders.
zAutoloadTable:fnRegisterAutoloadLump("AutoLoad|Adventure|Portrait_Ch2_Emt_Izuna_Miko")
zAutoloadTable:fnRegisterAutoloadLump("AutoLoad|Adventure|Portrait_Ch2_Cbt_Izuna")
zAutoloadTable:fnRegisterAutoloadLump("AutoLoad|Adventure|Portrait_Ch2_Emt_Zeke_Goat")
zAutoloadTable:fnRegisterAutoloadLump("AutoLoad|Adventure|Portrait_Ch2_Cbt_Zeke")
zAutoloadTable:fnRegisterAutoloadLump("AutoLoad|Adventure|Portrait_Ch2_Emt_Empress_Conquerer")
zAutoloadTable:fnRegisterAutoloadLump("AutoLoad|Adventure|Portrait_Ch2_Cbt_Empress")
zAutoloadTable:fnRegisterAutoloadLump("AutoLoad|Adventure|Portrait_Ch2_Emt_Odar_Prince")
zAutoloadTable:fnRegisterAutoloadLump("AutoLoad|Adventure|Portrait_Ch2_Cbt_Odar")

-- |[ ================================== Instruction Building ================================== ]|
-- |[Prototype]|
--AutoloadTable:fnRegisterAutoloadInstruction(psLumpName, psDatafileName, psDatafilePath, psLoadInstruction)

-- |[Sanya's Talk Sprites]|
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_Emt_Sanya_Bunny",         "Datafile Sanya",   sExpectedDatafilePath .. "Portraits_Sanya.slf", "AutoLoad|Adventure|Emote_Sanya_Bunny")
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_Emt_Sanya_Harpy",         "Datafile Sanya",   sExpectedDatafilePath .. "Portraits_Sanya.slf", "AutoLoad|Adventure|Emote_Sanya_Harpy")
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_Emt_Sanya_Human",         "Datafile Sanya",   sExpectedDatafilePath .. "Portraits_Sanya.slf", "AutoLoad|Adventure|Emote_Sanya_Human")
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_Emt_Sanya_HumanNoRifle",  "Datafile Sanya",   sExpectedDatafilePath .. "Portraits_Sanya.slf", "AutoLoad|Adventure|Emote_Sanya_HumanNoRifle")
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_Emt_Sanya_HumanWerebatA", "Datafile Sanya",   sExpectedDatafilePath .. "Portraits_Sanya.slf", "AutoLoad|Adventure|Emote_Sanya_HumanWerebatA")
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_Emt_Sanya_HumanWerebatB", "Datafile Sanya",   sExpectedDatafilePath .. "Portraits_Sanya.slf", "AutoLoad|Adventure|Emote_Sanya_HumanWerebatB")
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_Emt_Sanya_Kitsune",       "Datafile Sanya",   sExpectedDatafilePath .. "Portraits_Sanya.slf", "AutoLoad|Adventure|Emote_Sanya_Kitsune")
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_Emt_Sanya_Sevavi",        "Datafile Sanya",   sExpectedDatafilePath .. "Portraits_Sanya.slf", "AutoLoad|Adventure|Emote_Sanya_Sevavi")
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_Emt_Sanya_SevaviNoRifle", "Datafile Sanya",   sExpectedDatafilePath .. "Portraits_Sanya.slf", "AutoLoad|Adventure|Emote_Sanya_SevaviNoRifle")
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_Emt_Sanya_Werebat",       "Datafile Sanya",   sExpectedDatafilePath .. "Portraits_Sanya.slf", "AutoLoad|Adventure|Emote_Sanya_Werebat")

-- |[Sanya's Combat Portraits]|
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_Cbt_Sanya_Bunny",         "Datafile Sanya",   sExpectedDatafilePath .. "Portraits_Sanya.slf", "AutoLoad|Adventure|Combat_Sanya_Bunny")
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_Cbt_Sanya_Harpy",         "Datafile Sanya",   sExpectedDatafilePath .. "Portraits_Sanya.slf", "AutoLoad|Adventure|Combat_Sanya_Harpy")
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_Cbt_Sanya_Human",         "Datafile Sanya",   sExpectedDatafilePath .. "Portraits_Sanya.slf", "AutoLoad|Adventure|Combat_Sanya_Human")
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_Cbt_Sanya_HumanNoRifle",  "Datafile Sanya",   sExpectedDatafilePath .. "Portraits_Sanya.slf", "AutoLoad|Adventure|Combat_Sanya_HumanNoRifle")
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_Cbt_Sanya_Kitsune",       "Datafile Sanya",   sExpectedDatafilePath .. "Portraits_Sanya.slf", "AutoLoad|Adventure|Combat_Sanya_Kitsune")
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_Cbt_Sanya_Sevavi",        "Datafile Sanya",   sExpectedDatafilePath .. "Portraits_Sanya.slf", "AutoLoad|Adventure|Combat_Sanya_Sevavi")
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_Cbt_Sanya_SevaviNoRifle", "Datafile Sanya",   sExpectedDatafilePath .. "Portraits_Sanya.slf", "AutoLoad|Adventure|Combat_Sanya_SevaviNoRifle")
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_Cbt_Sanya_Werebat",       "Datafile Sanya",   sExpectedDatafilePath .. "Portraits_Sanya.slf", "AutoLoad|Adventure|Combat_Sanya_Werebat")

-- |[Izuna]|
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_Emt_Izuna_Miko", "Datafile Izuna", sExpectedDatafilePath .. "Portraits_Izuna.slf", "AutoLoad|Adventure|Emote_Izuna_Miko")
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_Cbt_Izuna",      "Datafile Izuna", sExpectedDatafilePath .. "Portraits_Izuna.slf", "AutoLoad|Adventure|Combat_Izuna")

-- |[Zeke]|
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_Emt_Zeke_Goat", "Datafile Zeke", sExpectedDatafilePath .. "Portraits_Zeke.slf", "AutoLoad|Adventure|Emote_Zeke")
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_Cbt_Zeke",      "Datafile Zeke", sExpectedDatafilePath .. "Portraits_Zeke.slf", "AutoLoad|Adventure|Combat_Zeke")

-- |[Empress]|
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_Emt_Empress_Conquerer", "Datafile Empress", sExpectedDatafilePath .. "Portraits_Empress.slf", "AutoLoad|Adventure|Emote_Empress_Conquerer")
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_Cbt_Empress",           "Datafile Empress", sExpectedDatafilePath .. "Portraits_Empress.slf", "AutoLoad|Adventure|Combat_Empress")

-- |[Odar]|
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_Emt_Odar_Prince", "Datafile Odar", sExpectedDatafilePath .. "Portraits_Odar.slf", "AutoLoad|Adventure|Emote_Odar_Prince")
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_Cbt_Odar",        "Datafile Odar", sExpectedDatafilePath .. "Portraits_Odar.slf", "AutoLoad|Adventure|Combat_Odar")

-- |[Major Characters]|
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_AsNeeded", "Datafile Cassandra", sExpectedDatafilePath .. "Portraits_Cassandra.slf", "AutoLoad|Adventure|Emote_Cassandra")

-- |[Minor/Generic Characters]|
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_AsNeeded", "Datafile Ch0Emote", sExpectedDatafilePath .. "Portraits_CH0Emote.slf", "AutoLoad|Adventure|Emote_CrowbarChanMob")
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_AsNeeded", "Datafile Ch1Emote", sExpectedDatafilePath .. "Portraits_CH1Emote.slf", "AutoLoad|Adventure|Emote_Alraune")
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_AsNeeded", "Datafile Ch2Emote", sExpectedDatafilePath .. "Portraits_CH2Emote.slf", "AutoLoad|Adventure|Emote_Ch2_NPC")
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_AsNeeded", "Datafile Mia",      sExpectedDatafilePath .. "Portraits_Mia.slf",      "AutoLoad|Adventure|Emote_Mia")
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_AsNeeded", "Datafile Miso",     sExpectedDatafilePath .. "Portraits_Miso.slf",     "AutoLoad|Adventure|Emote_Miso")
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_AsNeeded", "Datafile Yukina",   sExpectedDatafilePath .. "Portraits_Yukina.slf",   "AutoLoad|Adventure|Emote_Yukina")

-- |[Enemy Combat Portraits]|
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_AsNeeded", "Datafile Ch0Combat", sExpectedDatafilePath .. "Portraits_CH0Combat.slf", "AutoLoad|Adventure|Portrait_Ch0_Enemy_For_Ch2")
zAutoloadTable:fnRegisterAutoloadInstruction("AutoLoad|Adventure|Portrait_Ch2_AsNeeded", "Datafile Ch2Combat", sExpectedDatafilePath .. "Portraits_CH2Combat.slf", "AutoLoad|Adventure|Portrait_Ch2_Enemy")

-- |[ ======================================= Execution ======================================== ]|
-- |[Object Execution]|
zAutoloadTable:fnExecute()

-- |[Clean Up]|
SLF_Close()
