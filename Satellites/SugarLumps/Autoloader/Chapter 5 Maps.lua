-- |[ ================================= Chapter 5 Map Assembly ================================= ]|
--Compile all the map load instructions into a single autoloader SLF file.
--This must be called AFTER all building is done AND all datafiles are copied. In order to correctly
-- clone an autoloader list, the datafile must be in its final destination so it can be read and
-- its autoloader lump queried.
local sExpectedDatafilePath = "../../Games/AdventureMode/Datafiles/"

-- |[ ======================================== Function ======================================== ]|
local function fnCloneAutoloadFromFile(psFilePath, psAutoLoadName, psAutoLoadDestination)

    --Query the file.
    SLF_QueryAutoLoadInFile(psFilePath, psAutoLoadName)
    
    --Copy all elements from source to destination. 
    local iCount = SLF_GetQueryCount()
    for q = 0, iCount-1, 1 do
        local sName, sPath = SLF_GetQueryEntry(q)
        AutoLoader_Register(psAutoLoadDestination, sName, sPath)
    end
end

-- |[ ===================================== File Creation ====================================== ]|
--Create the file.
SLF_Open("Output/Maps_CH5_Autoloader.slf")

--Create the execution lump.
local sImmediateLump = "AutoLoad|Adventure|Maps_Ch5_Immediate"
SLF_RegisterAutoLoadLump(sImmediateLump)

--Biolabs Map.
AutoLoader_Register(sImmediateLump, "FILESWITCH", "Datafile Maps Biolabs")
fnCloneAutoloadFromFile(sExpectedDatafilePath .. "Maps_Biolabs.slf", "AutoLoad|Adventure|Map_Biolabs", sImmediateLump)

--CRT Corruption Sequence.
AutoLoader_Register(sImmediateLump, "FILESWITCH", "Datafile Maps CRT Noise")
fnCloneAutoloadFromFile(sExpectedDatafilePath .. "Maps_CRTNoise.slf", "AutoLoad|Adventure|Overlay_CRTNoise", sImmediateLump)

--Cryogenics Lower Map.
AutoLoader_Register(sImmediateLump, "FILESWITCH", "Datafile Maps Cryogenics Lower")
fnCloneAutoloadFromFile(sExpectedDatafilePath .. "Maps_CryoLower.slf", "AutoLoad|Adventure|Map_CryoLower", sImmediateLump)

--Cryogenics Main Map.
AutoLoader_Register(sImmediateLump, "FILESWITCH", "Datafile Maps Cryogenics Main")
fnCloneAutoloadFromFile(sExpectedDatafilePath .. "Maps_CryoMain.slf", "AutoLoad|Adventure|Map_CryoMain", sImmediateLump)

--Equinox.
AutoLoader_Register(sImmediateLump, "FILESWITCH", "Datafile Maps Equinox")
fnCloneAutoloadFromFile(sExpectedDatafilePath .. "Maps_Equinox.slf", "AutoLoad|Adventure|Map_Equinox", sImmediateLump)

--LRT East.
AutoLoader_Register(sImmediateLump, "FILESWITCH", "Datafile Maps LRT East")
fnCloneAutoloadFromFile(sExpectedDatafilePath .. "Maps_LRTEast.slf", "AutoLoad|Adventure|Map_LRTEast", sImmediateLump)

--LRT West.
AutoLoader_Register(sImmediateLump, "FILESWITCH", "Datafile Maps LRT West")
fnCloneAutoloadFromFile(sExpectedDatafilePath .. "Maps_LRTWest.slf", "AutoLoad|Adventure|Map_LRTWest", sImmediateLump)

--Nix Nedar Background.
AutoLoader_Register(sImmediateLump, "FILESWITCH", "Datafile Maps Nix Nedar")
fnCloneAutoloadFromFile(sExpectedDatafilePath .. "Maps_NixNedar.slf", "AutoLoad|Adventure|Background_NixNedar", sImmediateLump)

--Regulus Map.
AutoLoader_Register(sImmediateLump, "FILESWITCH", "Datafile Maps Regulus")
fnCloneAutoloadFromFile(sExpectedDatafilePath .. "Maps_Regulus.slf", "AutoLoad|Adventure|Map_Regulus", sImmediateLump)

--System. Stores the PDU Frame image.
AutoLoader_Register(sImmediateLump, "FILESWITCH", "Datafile Maps System")
fnCloneAutoloadFromFile(sExpectedDatafilePath .. "Maps_System.slf", "AutoLoad|Adventure|Map_Ch5Pieces", sImmediateLump)

-- |[ ======================================= Finish Up ======================================== ]|
SLF_Close()
