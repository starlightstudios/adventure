-- |[ ============================== Chapter 1 Portrait Assembly =============================== ]|
--Compile all the portraits needed for chapter 2 into two autoloader lumps. The lumps will either
-- load immediately, or as-needed, based on the game options.
--This must be called AFTER all building is done AND all datafiles are copied. In order to correctly
-- clone an autoloader list, the datafile must be in its final destination so it can be read and
-- its autoloader lump queried.
local sExpectedDatafilePath = "../../Games/AdventureMode/Datafiles/"

-- |[ ===================================== File Creation ====================================== ]|
--Create the file and the Immediate/AsNeeded autoloader lumps.
SLF_Open("Output/Portraits_CH1_Autoloader.slf")

--Tables that will contain loading data.
local zaImmediate                      = {}
local zaAsNeeded                       = {}

--Mei.
local zaMeiAlraune                     = {}
local zaMeiAlrauneMC                   = {}
local zaMeiBee                         = {}
local zaMeiBeeMC                       = {}
local zaMeiBeeQueen                    = {}
local zaMeiBeeQueenMC                  = {}
local zaMeiGhost                       = {}
local zaMeiHuman                       = {}
local zaMeiHumanMC                     = {}
local zaMeiSlime                       = {}
local zaMeiSlimeBlue                   = {}
local zaMeiSlimeInk                    = {}
local zaMeiSlimePink                   = {}
local zaMeiSlimeYellow                 = {}
local zaMeiMannequin                   = {}
local zaMeiWerecat                     = {}
local zaMeiWisphag                     = {}
local zaMeiGravemarker                 = {}
local zaMeiRubber                      = {}
local zaMeiRubberQueen                 = {}
local zaMeiAlrauneCombat               = {}
local zaMeiAlrauneMCCombat             = {}
local zaMeiBeeCombat                   = {}
local zaMeiBeeMCCombat                 = {}
local zaMeiBeeQueenCombat              = {}
local zaMeiZombeeQueenCombat           = {}
local zaMeiGhostCombat                 = {}
local zaMeiHumanCombat                 = {}
local zaMeiHumanMCCombat               = {}
local zaMeiSlimeCombat                 = {}
local zaMeiSlimeBlueCombat             = {}
local zaMeiSlimeInkCombat              = {}
local zaMeiSlimePinkCombat             = {}
local zaMeiSlimeYellowCombat           = {}
local zaMeiMannequinCombat             = {}
local zaMeiWerecatCombat               = {}
local zaMeiWisphagCombat               = {}
local zaMeiGravemarkerCombat           = {}
local zaMeiRubberCombat                = {}
local zaMeiRubberQueenCombat           = {}

--Florentina.
local zaFlorentinaAgarist              = {}
local zaFlorentinaLurker               = {}
local zaFlorentinaMediator             = {}
local zaFlorentinaMerchant             = {}
local zaFlorentinaMerchantRubber       = {}
local zaFlorentinaTreasureHunter       = {}
local zaFlorentinaAgaristCombat        = {}
local zaFlorentinaLurkerCombat         = {}
local zaFlorentinaMediatorCombat       = {}
local zaFlorentinaMerchantCombat       = {}
local zaFlorentinaMerchantRubberCombat = {}
local zaFlorentinaTreasureHunterCombat = {}

-- |[Table of Tables]|
local zaTableTable = {}

--Basic.
table.insert(zaTableTable, {zaImmediate, "AutoLoad|Adventure|Portrait_Ch1_Immediate"})
table.insert(zaTableTable, {zaAsNeeded,  "AutoLoad|Adventure|Portrait_Ch1_AsNeeded"})

--Mei, Emote.
table.insert(zaTableTable, {zaMeiAlraune,     "AutoLoad|Adventure|Portrait_Ch1_Emt_Mei_Alraune"})
table.insert(zaTableTable, {zaMeiAlrauneMC,   "AutoLoad|Adventure|Portrait_Ch1_Emt_Mei_AlrauneMC"})
table.insert(zaTableTable, {zaMeiBee,         "AutoLoad|Adventure|Portrait_Ch1_Emt_Mei_Bee"})
table.insert(zaTableTable, {zaMeiBeeMC,       "AutoLoad|Adventure|Portrait_Ch1_Emt_Mei_BeeMC"})
table.insert(zaTableTable, {zaMeiBeeQueen,    "AutoLoad|Adventure|Portrait_Ch1_Emt_Mei_BeeQueen"})
table.insert(zaTableTable, {zaMeiBeeQueenMC,  "AutoLoad|Adventure|Portrait_Ch1_Emt_Mei_BeeQueenMC"})
table.insert(zaTableTable, {zaMeiGhost,       "AutoLoad|Adventure|Portrait_Ch1_Emt_Mei_Ghost"})
table.insert(zaTableTable, {zaMeiHuman,       "AutoLoad|Adventure|Portrait_Ch1_Emt_Mei_Human"})
table.insert(zaTableTable, {zaMeiHumanMC,     "AutoLoad|Adventure|Portrait_Ch1_Emt_Mei_HumanMC"})
table.insert(zaTableTable, {zaMeiSlime,       "AutoLoad|Adventure|Portrait_Ch1_Emt_Mei_Slime"})
table.insert(zaTableTable, {zaMeiSlimeBlue,   "AutoLoad|Adventure|Portrait_Ch1_Emt_Mei_SlimeBlue"})
table.insert(zaTableTable, {zaMeiSlimeInk,    "AutoLoad|Adventure|Portrait_Ch1_Emt_Mei_SlimeInk"})
table.insert(zaTableTable, {zaMeiSlimePink,   "AutoLoad|Adventure|Portrait_Ch1_Emt_Mei_SlimePink"})
table.insert(zaTableTable, {zaMeiSlimeYellow, "AutoLoad|Adventure|Portrait_Ch1_Emt_Mei_SlimeYellow"})
table.insert(zaTableTable, {zaMeiMannequin,   "AutoLoad|Adventure|Portrait_Ch1_Emt_Mei_Mannequin"})
table.insert(zaTableTable, {zaMeiWerecat,     "AutoLoad|Adventure|Portrait_Ch1_Emt_Mei_Werecat"})
table.insert(zaTableTable, {zaMeiWisphag,     "AutoLoad|Adventure|Portrait_Ch1_Emt_Mei_Wisphag"})
table.insert(zaTableTable, {zaMeiGravemarker, "AutoLoad|Adventure|Portrait_Ch1_Emt_Mei_Gravemarker"})
table.insert(zaTableTable, {zaMeiRubber,      "AutoLoad|Adventure|Portrait_Ch1_Emt_Mei_Rubber"})
table.insert(zaTableTable, {zaMeiRubberQueen, "AutoLoad|Adventure|Portrait_Ch1_Emt_Mei_RubberQueen"})

--Mei, Combat.
table.insert(zaTableTable, {zaMeiAlrauneCombat,     "AutoLoad|Adventure|Portrait_Ch1_Cbt_Mei_Alraune"})
table.insert(zaTableTable, {zaMeiAlrauneMCCombat,   "AutoLoad|Adventure|Portrait_Ch1_Cbt_Mei_AlrauneMC"})
table.insert(zaTableTable, {zaMeiBeeCombat,         "AutoLoad|Adventure|Portrait_Ch1_Cbt_Mei_Bee"})
table.insert(zaTableTable, {zaMeiBeeMCCombat,       "AutoLoad|Adventure|Portrait_Ch1_Cbt_Mei_BeeMC"})
table.insert(zaTableTable, {zaMeiBeeQueenCombat,    "AutoLoad|Adventure|Portrait_Ch1_Cbt_Mei_BeeQueen"})
table.insert(zaTableTable, {zaMeiZombeeQueenCombat, "AutoLoad|Adventure|Portrait_Ch1_Cbt_Mei_BeeQueenMC"})
table.insert(zaTableTable, {zaMeiGhostCombat,       "AutoLoad|Adventure|Portrait_Ch1_Cbt_Mei_Ghost"})
table.insert(zaTableTable, {zaMeiHumanCombat,       "AutoLoad|Adventure|Portrait_Ch1_Cbt_Mei_Human"})
table.insert(zaTableTable, {zaMeiHumanMCCombat,     "AutoLoad|Adventure|Portrait_Ch1_Cbt_Mei_HumanMC"})
table.insert(zaTableTable, {zaMeiSlimeCombat,       "AutoLoad|Adventure|Portrait_Ch1_Cbt_Mei_Slime"})
table.insert(zaTableTable, {zaMeiSlimeBlueCombat,   "AutoLoad|Adventure|Portrait_Ch1_Cbt_Mei_SlimeBlue"})
table.insert(zaTableTable, {zaMeiSlimeInkCombat,    "AutoLoad|Adventure|Portrait_Ch1_Cbt_Mei_SlimeInk"})
table.insert(zaTableTable, {zaMeiSlimePinkCombat,   "AutoLoad|Adventure|Portrait_Ch1_Cbt_Mei_SlimePink"})
table.insert(zaTableTable, {zaMeiSlimeYellowCombat, "AutoLoad|Adventure|Portrait_Ch1_Cbt_Mei_SlimeYellow"})
table.insert(zaTableTable, {zaMeiMannequinCombat,   "AutoLoad|Adventure|Portrait_Ch1_Cbt_Mei_Mannequin"})
table.insert(zaTableTable, {zaMeiWerecatCombat,     "AutoLoad|Adventure|Portrait_Ch1_Cbt_Mei_Werecat"})
table.insert(zaTableTable, {zaMeiWisphagCombat,     "AutoLoad|Adventure|Portrait_Ch1_Cbt_Mei_Wisphag"})
table.insert(zaTableTable, {zaMeiGravemarkerCombat, "AutoLoad|Adventure|Portrait_Ch1_Cbt_Mei_Gravemarker"})
table.insert(zaTableTable, {zaMeiRubberCombat,      "AutoLoad|Adventure|Portrait_Ch1_Cbt_Mei_Rubber"})
table.insert(zaTableTable, {zaMeiRubberQueenCombat, "AutoLoad|Adventure|Portrait_Ch1_Cbt_Mei_RubberQueen"})
table.insert(zaTableTable, {zaMeiRubberQueenCombat, "AutoLoad|Adventure|Portrait_Ch1_Cbt_Mei_ZombeeQueen"})

--Florentina, emote.
table.insert(zaTableTable, {zaFlorentinaAgarist,        "AutoLoad|Adventure|Portrait_Ch1_Emt_Florentina_Agarist"})
table.insert(zaTableTable, {zaFlorentinaLurker,         "AutoLoad|Adventure|Portrait_Ch1_Emt_Florentina_Lurker"})
table.insert(zaTableTable, {zaFlorentinaMediator,       "AutoLoad|Adventure|Portrait_Ch1_Emt_Florentina_Mediator"})
table.insert(zaTableTable, {zaFlorentinaMerchant,       "AutoLoad|Adventure|Portrait_Ch1_Emt_Florentina_Merchant"})
table.insert(zaTableTable, {zaFlorentinaMerchantRubber, "AutoLoad|Adventure|Portrait_Ch1_Emt_Florentina_MerchantRubber"})
table.insert(zaTableTable, {zaFlorentinaTreasureHunter, "AutoLoad|Adventure|Portrait_Ch1_Emt_Florentina_Treasure Hunter"})

--Florentina, combat.
table.insert(zaTableTable, {zaFlorentinaAgaristCombat,        "AutoLoad|Adventure|Portrait_Ch1_Cbt_Florentina_Agarist"})
table.insert(zaTableTable, {zaFlorentinaLurkerCombat,         "AutoLoad|Adventure|Portrait_Ch1_Cbt_Florentina_Lurker"})
table.insert(zaTableTable, {zaFlorentinaMediatorCombat,       "AutoLoad|Adventure|Portrait_Ch1_Cbt_Florentina_Mediator"})
table.insert(zaTableTable, {zaFlorentinaMerchantCombat,       "AutoLoad|Adventure|Portrait_Ch1_Cbt_Florentina_Merchant"})
table.insert(zaTableTable, {zaFlorentinaMerchantRubberCombat, "AutoLoad|Adventure|Portrait_Ch1_Cbt_Florentina_MerchantRubber"})
table.insert(zaTableTable, {zaFlorentinaTreasureHunterCombat, "AutoLoad|Adventure|Portrait_Ch1_Cbt_Florentina_Treasure Hunter"})

-- |[ ===================================== Table Building ===================================== ]|
-- |[Party Members]|
--Mei
table.insert(zaMeiAlraune,           {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Emote_Mei_Alraune"})
table.insert(zaMeiAlrauneMC,         {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Emote_Mei_AlrauneMC"})
table.insert(zaMeiBee,               {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Emote_Mei_Bee"})
table.insert(zaMeiBeeMC,             {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Emote_Mei_BeeMC"})
table.insert(zaMeiBeeQueen,          {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Emote_Mei_BeeQueen"})
table.insert(zaMeiBeeQueenMC,        {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Emote_Mei_BeeQueenMC"})
table.insert(zaMeiGhost,             {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Emote_Mei_Ghost"})
table.insert(zaMeiHuman,             {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Emote_Mei_Human"})
table.insert(zaMeiHumanMC,           {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Emote_Mei_HumanMC"})
table.insert(zaMeiSlime,             {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Emote_Mei_Slime"})
table.insert(zaMeiSlimeBlue,         {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Emote_Mei_SlimeBlue"})
table.insert(zaMeiSlimeInk,          {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Emote_Mei_SlimeInk"})
table.insert(zaMeiSlimePink,         {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Emote_Mei_SlimePink"})
table.insert(zaMeiSlimeYellow,       {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Emote_Mei_SlimeYellow"})
table.insert(zaMeiMannequin,         {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Emote_Mei_Mannequin"})
table.insert(zaMeiWerecat,           {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Emote_Mei_Werecat"})
table.insert(zaMeiWisphag,           {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Emote_Mei_Wisphag"})
table.insert(zaMeiGravemarker,       {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Emote_Mei_Gravemarker"})
table.insert(zaMeiRubber,            {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Emote_Mei_Rubber"})
table.insert(zaMeiRubberQueen,       {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Emote_Mei_RubberQueen"})
table.insert(zaMeiAlrauneCombat,     {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Combat_Mei_Alraune"})
table.insert(zaMeiAlrauneMCCombat,   {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Combat_Mei_AlrauneMC"})
table.insert(zaMeiBeeCombat,         {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Combat_Mei_Bee"})
table.insert(zaMeiBeeMCCombat,       {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Combat_Mei_Zombee"})
table.insert(zaMeiBeeQueenCombat,    {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Combat_Mei_BeeQueen"})
table.insert(zaMeiZombeeQueenCombat, {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Combat_Mei_ZombeeQueen"})
table.insert(zaMeiGhostCombat,       {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Combat_Mei_Ghost"})
table.insert(zaMeiHumanCombat,       {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Combat_Mei_Human"})
table.insert(zaMeiHumanMCCombat,     {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Combat_Mei_HumanMC"})
table.insert(zaMeiSlimeCombat,       {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Combat_Mei_Slime"})
table.insert(zaMeiSlimeBlueCombat,   {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Combat_Mei_SlimeBlue"})
table.insert(zaMeiSlimeInkCombat,    {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Combat_Mei_SlimeInk"})
table.insert(zaMeiSlimePinkCombat,   {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Combat_Mei_SlimePink"})
table.insert(zaMeiSlimeYellowCombat, {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Combat_Mei_SlimeYellow"})
table.insert(zaMeiMannequinCombat,   {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Combat_Mei_Mannequin"})
table.insert(zaMeiWerecatCombat,     {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Combat_Mei_Werecat"})
table.insert(zaMeiWisphagCombat,     {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Combat_Mei_Wisphag"})
table.insert(zaMeiGravemarkerCombat, {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Combat_Mei_Gravemarker"})
table.insert(zaMeiRubberCombat,      {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Combat_Mei_Rubber"})
table.insert(zaMeiRubberQueenCombat, {"Datafile Mei", sExpectedDatafilePath .. "Portraits_Mei.slf", "AutoLoad|Adventure|Combat_Mei_RubberQueen"})

--Florentina
table.insert(zaFlorentinaAgarist,        {"Datafile Florentina", sExpectedDatafilePath .. "Portraits_Florentina.slf", "AutoLoad|Adventure|Emote_Florentina_Agarist"})
table.insert(zaFlorentinaLurker,         {"Datafile Florentina", sExpectedDatafilePath .. "Portraits_Florentina.slf", "AutoLoad|Adventure|Emote_Florentina_Lurker"})
table.insert(zaFlorentinaMediator,       {"Datafile Florentina", sExpectedDatafilePath .. "Portraits_Florentina.slf", "AutoLoad|Adventure|Emote_Florentina_Mediator"})
table.insert(zaFlorentinaMerchant,       {"Datafile Florentina", sExpectedDatafilePath .. "Portraits_Florentina.slf", "AutoLoad|Adventure|Emote_Florentina_Merchant"})
table.insert(zaFlorentinaMerchantRubber, {"Datafile Florentina", sExpectedDatafilePath .. "Portraits_Florentina.slf", "AutoLoad|Adventure|Emote_Florentina_MerchantRubber"})
table.insert(zaFlorentinaTreasureHunter, {"Datafile Florentina", sExpectedDatafilePath .. "Portraits_Florentina.slf", "AutoLoad|Adventure|Emote_Florentina_Treasure Hunter"})

table.insert(zaFlorentinaAgaristCombat,        {"Datafile Florentina", sExpectedDatafilePath .. "Portraits_Florentina.slf", "AutoLoad|Adventure|Combat_Florentina_Agarist"})
table.insert(zaFlorentinaLurkerCombat,         {"Datafile Florentina", sExpectedDatafilePath .. "Portraits_Florentina.slf", "AutoLoad|Adventure|Combat_Florentina_Lurker"})
table.insert(zaFlorentinaMediatorCombat,       {"Datafile Florentina", sExpectedDatafilePath .. "Portraits_Florentina.slf", "AutoLoad|Adventure|Combat_Florentina_Mediator"})
table.insert(zaFlorentinaMerchantCombat,       {"Datafile Florentina", sExpectedDatafilePath .. "Portraits_Florentina.slf", "AutoLoad|Adventure|Combat_Florentina_Merchant"})
table.insert(zaFlorentinaTreasureHunterCombat, {"Datafile Florentina", sExpectedDatafilePath .. "Portraits_Florentina.slf", "AutoLoad|Adventure|Combat_Florentina_Treasure Hunter"})

-- |[Major Characters]|
table.insert(zaAsNeeded, {"Datafile Cassandra", sExpectedDatafilePath .. "Portraits_Cassandra.slf", "AutoLoad|Adventure|Emote_Cassandra"})

-- |[Minor/Generic Characters]|
table.insert(zaAsNeeded, {"Datafile Ch1Emote", sExpectedDatafilePath .. "Portraits_CH1Emote.slf", "AutoLoad|Adventure|Emote_Denise"})
table.insert(zaAsNeeded, {"Datafile Ch1Emote", sExpectedDatafilePath .. "Portraits_CH1Emote.slf", "AutoLoad|Adventure|Emote_Kona"})
table.insert(zaAsNeeded, {"Datafile Ch1Emote", sExpectedDatafilePath .. "Portraits_CH1Emote.slf", "AutoLoad|Adventure|Emote_Meryl"})
table.insert(zaAsNeeded, {"Datafile Ch1Emote", sExpectedDatafilePath .. "Portraits_CH1Emote.slf", "AutoLoad|Adventure|Emote_Mycela"})
table.insert(zaAsNeeded, {"Datafile Ch1Emote", sExpectedDatafilePath .. "Portraits_CH1Emote.slf", "AutoLoad|Adventure|Emote_Victoria"})
table.insert(zaAsNeeded, {"Datafile Ch1Emote", sExpectedDatafilePath .. "Portraits_CH1Emote.slf", "AutoLoad|Adventure|Emote_BanditCatB"})
table.insert(zaAsNeeded, {"Datafile Ch1Emote", sExpectedDatafilePath .. "Portraits_CH1Emote.slf", "AutoLoad|Adventure|Emote_Xanna"})

-- |[Enemy Combat Portraits]|
table.insert(zaAsNeeded, {"Datafile Ch0Combat", sExpectedDatafilePath .. "Portraits_CH0Combat.slf", "AutoLoad|Adventure|Portrait_Ch0_Enemy_For_Ch1"})
table.insert(zaAsNeeded, {"Datafile Ch1Combat", sExpectedDatafilePath .. "Portraits_CH1Combat.slf", "AutoLoad|Adventure|Portrait_Ch1_Enemy"})

-- |[ ======================================= Execution ======================================== ]|
-- |[Table Iteration]|
--Iterate across the tables.
for i = 1, #zaTableTable, 1 do
    
    --Variables.
    local zaUseTable    = zaTableTable[i][1]
    local sAutoLoadLump = zaTableTable[i][2]
    
    --Register the lump.
    SLF_RegisterAutoLoadLump(sAutoLoadLump)
    
    --Iterate across the table.
    for p = 1, #zaUseTable, 1 do
    
        --Variables.
        local sFileswitch   = zaUseTable[p][1]
        local sFilePath     = zaUseTable[p][2]
        local sAutoLoadName = zaUseTable[p][3]
    
        --If the fileswitch is "Null", don't switch files. This saves a bit of time.
        if(sFileswitch ~= "Null") then
            AutoLoader_Register(sAutoLoadLump, "FILESWITCH", sFileswitch)
        end

        --Query the file.
        SLF_QueryAutoLoadInFile(sFilePath, sAutoLoadName)
        
        --Copy all elements from source to destination. 
        local iCount = SLF_GetQueryCount()
        for q = 0, iCount-1, 1 do
            local sName, sPath = SLF_GetQueryEntry(q)
            AutoLoader_Register(sAutoLoadLump, sName, sPath)
        end
    end
end

-- |[ ======================================= Finish Up ======================================== ]|
SLF_Close()
