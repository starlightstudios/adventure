-- |[ ================================ Chapter 1 Sprite Assembly =============================== ]|
--Compile all the sprite load instructions into a single autoloader SLF file.
--This must be called AFTER all building is done AND all datafiles are copied. In order to correctly
-- clone an autoloader list, the datafile must be in its final destination so it can be read and
-- its autoloader lump queried.
local sExpectedDatafilePath = "../../Games/AdventureMode/Datafiles/"

-- |[ ======================================== Function ======================================== ]|
local function fnCloneAutoloadFromFile(psFilePath, psAutoLoadName, psAutoLoadDestination)

    --Query the file.
    SLF_QueryAutoLoadInFile(psFilePath, psAutoLoadName)
    
    --Copy all elements from source to destination. 
    local iCount = SLF_GetQueryCount()
    for q = 0, iCount-1, 1 do
        local sName, sPath = SLF_GetQueryEntry(q)
        AutoLoader_Register(psAutoLoadDestination, sName, sPath)
    end
end

-- |[ ===================================== File Creation ====================================== ]|
--Setup.
local sDatafilePath = sExpectedDatafilePath .. "Sprites.slf"

--Create the file.
SLF_Open("Output/Sprites_CH1_Autoloader.slf")

--Create the execution lump.
local sImmediateLump = "AutoLoad|Adventure|Sprites_Ch1"
SLF_RegisterAutoLoadLump(sImmediateLump)

--Switch to the sprites file. All sprites are in there.
AutoLoader_Register(sImmediateLump, "FILESWITCH", "Datafile Sprites")

-- |[Party]|
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMei|Alraune",     sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMei|AlrauneMC",   sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMei|Bee",         sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMei|BeeQueen",    sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMei|BeeMC",       sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMei|Ghost",       sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMei|Gravemarker", sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMei|Human",       sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMei|MC",          sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMei|Mannequin",   sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMei|Rubber",      sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMei|RubberQueen", sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMei|Slime",       sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMei|SlimeBlue",   sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMei|SlimeInk",    sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMei|SlimePink",   sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMei|SlimeYellow", sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMei|Werecat",     sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMei|Wisphag",     sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMei|ZombeeQueen", sImmediateLump)

-- |[Major Characters]|
--fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMajor|CrowbarChanMobster", sImmediateLump)

-- |[Minor Characters]|
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMinor|Denise",   sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMinor|Kona",     sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMinor|Laura",    sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMinor|Meryl",    sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMinor|Mycela",   sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMinor|Victoria", sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMinor|Xanna",    sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMinor|XannaBee", sImmediateLump)

-- |[Generics and Enemies]|
--NPCs
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesGeneric|NPC_F0", sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesGeneric|NPC_M0", sImmediateLump)

--Enemies, Ch0
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesEnemiesCh0|Bandit_Female", sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesEnemiesCh0|Bandit_Male",   sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesEnemiesCh0|BanditCatB",    sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesEnemiesCh0|CultistFA",     sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesEnemiesCh0|MercDemon",     sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesEnemiesCh0|Bandit_Female", sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesEnemiesCh0|Bandit_Male",   sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesEnemiesCh0|BanditCatB",    sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesEnemiesCh0|MercDemon",     sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesEnemiesCh0|RedRobe",       sImmediateLump) --No paragaon

--Enemies, Ch1
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesEnemiesCh1|MannBanditF",   sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesEnemiesCh1|MannBanditM",   sImmediateLump)

--Paragons, Ch0
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesParagonsCh0|Bandit_Female", sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesParagonsCh0|Bandit_Male",   sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesParagonsCh0|BanditCatB",    sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesParagonsCh0|MercDemon",     sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesParagonsCh0|Bandit_Female", sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesParagonsCh0|Bandit_Male",   sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesParagonsCh0|BanditCatB",    sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesParagonsCh0|MercDemon",     sImmediateLump)

--Paragons, Ch1
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesParagonsCh1|MannBanditF",   sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesParagonsCh1|MannBanditM",   sImmediateLump)

-- |[Animals]|
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesGeneric|Wisp", sImmediateLump)

-- |[System/Misc]|
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|Sprites_Ch1", sImmediateLump)

-- |[ ======================================= Finish Up ======================================== ]|
SLF_Close()
