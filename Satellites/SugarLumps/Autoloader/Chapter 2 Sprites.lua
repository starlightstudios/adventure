-- |[ ================================ Chapter 2 Sprite Assembly =============================== ]|
--Compile all the sprite load instructions into a single autoloader SLF file.
--This must be called AFTER all building is done AND all datafiles are copied. In order to correctly
-- clone an autoloader list, the datafile must be in its final destination so it can be read and
-- its autoloader lump queried.
local sExpectedDatafilePath = "../../Games/AdventureMode/Datafiles/"

-- |[ ======================================== Function ======================================== ]|
local function fnCloneAutoloadFromFile(psFilePath, psAutoLoadName, psAutoLoadDestination)

    --Query the file.
    SLF_QueryAutoLoadInFile(psFilePath, psAutoLoadName)
    
    --Copy all elements from source to destination. 
    local iCount = SLF_GetQueryCount()
    for q = 0, iCount-1, 1 do
        local sName, sPath = SLF_GetQueryEntry(q)
        AutoLoader_Register(psAutoLoadDestination, sName, sPath)
    end
end

-- |[ ===================================== File Creation ====================================== ]|
--Setup.
local sDatafilePath = sExpectedDatafilePath .. "Sprites.slf"

--Create the file.
SLF_Open("Output/Sprites_CH2_Autoloader.slf")

--Create the execution lump.
local sImmediateLump = "AutoLoad|Adventure|Sprites_Ch2"
SLF_RegisterAutoLoadLump(sImmediateLump)

--Switch to the sprites file. All sprites are in there.
AutoLoader_Register(sImmediateLump, "FILESWITCH", "Datafile Sprites")

-- |[Party]|
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesSanya|Bunny_Rifle",     sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesSanya|Harpy_Rifle",     sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesSanya|Human",           sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesSanya|Human",           sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesSanya|Human_Carrying",  sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesSanya|Human_Rifle",     sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesSanya|Sevavi",          sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesSanya|Sevavi_Carrying", sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesSanya|Sevavi_Rifle",    sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesSanya|Werebat",         sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesIzuna|Miko",            sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesIzuna|Underwear",       sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesZeke|Goat",             sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesEmpress|Conquerer",     sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesOdar|Prince",           sImmediateLump)

-- |[Major Characters]|
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMajor|Mia",                sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMajor|Miso",               sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMajor|Yukina",             sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMajor|CrowbarChanMobster", sImmediateLump)

-- |[Minor Characters]|
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMinor|Angelface", sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMinor|Chikage",   sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMinor|Esmerelda", sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesMinor|Takahn",    sImmediateLump)

-- |[Generics and Enemies]|
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|Sprite_Ch2_Enemy",                   sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesGeneric|Alraune_Gen0",        sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesGeneric|NPC_F0",              sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesGeneric|NPC_F1",              sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesGeneric|NPC_F2",              sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesGeneric|NPC_F3",              sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesGeneric|NPC_F4",              sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesGeneric|NPC_M0",              sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesGeneric|NPC_M1",              sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesGeneric|NPC_M2",              sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesGeneric|NPC_M3",              sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesGeneric|NPC_M4",              sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesGeneric|NPC_M5",              sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesGeneric|NPC_MercM",           sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesGeneric|NPC_MercF",           sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesGeneric|NPC_KitsuneA",        sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesGeneric|NPC_KitsuneB",        sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesGeneric|NPC_KitsuneC",        sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesGeneric|NPC_KitsuneD",        sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesGeneric|NPC_KitsuneWarriorA", sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesGeneric|NPC_KitsuneWarriorB", sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesEnemiesCh0|Wisphag",          sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesEnemiesCh1|Alraune",          sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesEnemiesCh1|WerecatThief",     sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesEnemiesCh5|Raiju",            sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesParagonsCh0|Wisphag",         sImmediateLump)

-- |[Animals]|
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesGeneric|Chicken",  sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesGeneric|Sheep",    sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesGeneric|Horse",    sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesGeneric|PackMule", sImmediateLump)

-- |[System]|
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesCh2System|Sparkles",  sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesCh2System|Impacts",   sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesCh2System|Fragments", sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesCh2System|Loot",      sImmediateLump)
fnCloneAutoloadFromFile(sDatafilePath, "AutoLoad|Adventure|SpritesCh2System|Tracks",    sImmediateLump)

-- |[ ======================================= Finish Up ======================================== ]|
SLF_Close()
