-- |[ ================================= Chapter 1 Map Assembly ================================= ]|
--Compile all the map load instructions into a single autoloader SLF file.
--This must be called AFTER all building is done AND all datafiles are copied. In order to correctly
-- clone an autoloader list, the datafile must be in its final destination so it can be read and
-- its autoloader lump queried.
local sExpectedDatafilePath = "../../Games/AdventureMode/Datafiles/"

-- |[ ======================================== Function ======================================== ]|
local function fnCloneAutoloadFromFile(psFilePath, psAutoLoadName, psAutoLoadDestination)

    --Query the file.
    SLF_QueryAutoLoadInFile(psFilePath, psAutoLoadName)
    
    --Copy all elements from source to destination. 
    local iCount = SLF_GetQueryCount()
    for q = 0, iCount-1, 1 do
        local sName, sPath = SLF_GetQueryEntry(q)
        AutoLoader_Register(psAutoLoadDestination, sName, sPath)
    end
end

-- |[ ===================================== File Creation ====================================== ]|
--Create the file.
SLF_Open("Output/Maps_CH2_Autoloader.slf")

--Create the execution lump.
local sImmediateLump = "AutoLoad|Adventure|Maps_Ch2_Immediate"
SLF_RegisterAutoLoadLump(sImmediateLump)

--Northwoods File
AutoLoader_Register(sImmediateLump, "FILESWITCH", "Datafile Maps Northwoods")
fnCloneAutoloadFromFile(sExpectedDatafilePath .. "Maps_Northwoods.slf", "AutoLoad|Adventure|Map_Northwoods", sImmediateLump)

--System Parts.
AutoLoader_Register(sImmediateLump, "FILESWITCH", "Datafile Maps System")
fnCloneAutoloadFromFile(sExpectedDatafilePath .. "Maps_System.slf", "AutoLoad|Adventure|Map_Ch2Pieces", sImmediateLump)

--Mt. Sarulente File
AutoLoader_Register(sImmediateLump, "FILESWITCH", "Datafile Maps MtSarulente")
fnCloneAutoloadFromFile(sExpectedDatafilePath .. "Maps_MtSarulente.slf", "AutoLoad|Adventure|Map_MtSarulente", sImmediateLump)

--Westwoods File
AutoLoader_Register(sImmediateLump, "FILESWITCH", "Datafile Maps Westwoods")
fnCloneAutoloadFromFile(sExpectedDatafilePath .. "Maps_Westwoods.slf", "AutoLoad|Adventure|Map_Westwoods", sImmediateLump)

-- |[ ======================================= Finish Up ======================================== ]|
SLF_Close()
