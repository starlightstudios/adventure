-- |[ ================================ Marriedraunes Portraits ================================= ]|
--The married alraunes.
local sBasePath = fnResolvePath()

--[File Creation]
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_Marriedraunes.slf")
else
	SLF_Open("Output/PortraitsLD_Marriedraunes.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ========================================= Emotes ========================================= ]|
--[Automated Ripper Function]
--Ripper for this entity's emote pattern.
local fnAutoRip = function(sBaseName, sFormName, sPath)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|JNeutral", sPath, gciHDX0, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|JClimb",   sPath, gciHDX1, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|JGrab",    sPath, gciHDX2, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|SNeutral", sPath, gciHDX0, gciHDY1, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|SClimb",   sPath, gciHDX1, gciHDY1, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|SGrab",    sPath, gciHDX2, gciHDY1, gciWid0, gciHei0, 0)
end

--[General Purpose]
fnAutoRip("Marriedraunes", "", sBasePath .. "Marriedraunes_Emotes.png")

--[Special Frames]
--None yet.

-- |[ ========================================= Combat ========================================= ]|
--This is just a simple list of combat frames.

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the SLF file.
SLF_Close()
