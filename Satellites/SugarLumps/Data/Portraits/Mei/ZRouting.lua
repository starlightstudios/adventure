-- |[ ===================================== Mei Portraits ====================================== ]|
--Subscript for Mei's portraits. Contains both combat and emotes.
local sBasePath = fnResolvePath()

-- |[File Creation]|
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_Mei.slf")
else
	SLF_Open("Output/PortraitsLD_Mei.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ======================================== Emotes ========================================== ]|
-- |[Automated Ripper Function]|
--Ripper designed for Christine's emote pattern.
local function fnAutoRip(sBaseName, sFormName, sPath, bOnlyNeutralSmirk)
    
    --Autoloader.
    local sAutoloaderName = "AutoLoad|Adventure|Emote_Mei_" .. sFormName
    local sAutoloaderPath = "Root/Images/Portraits/MeiDialogue/" .. sFormName
    SLF_RegisterAutoLoadLump(sAutoloaderName)
    
    --Switch based on emotion set.
    if(bOnlyNeutralSmirk == false) then
        fnRipImageAuto("Por|" .. sBaseName .. sFormName .. "|Smirk",    sPath, gciHDX0, gciHDY0, gciWid0, gciHei0, gciNoTransparencies, sAutoloaderName, sAutoloaderPath .. "Smirk")
        fnRipImageAuto("Por|" .. sBaseName .. sFormName .. "|Happy",    sPath, gciHDX1, gciHDY0, gciWid0, gciHei0, gciNoTransparencies, sAutoloaderName, sAutoloaderPath .. "Happy")
        fnRipImageAuto("Por|" .. sBaseName .. sFormName .. "|Blush",    sPath, gciHDX2, gciHDY0, gciWid0, gciHei0, gciNoTransparencies, sAutoloaderName, sAutoloaderPath .. "Blush")
        fnRipImageAuto("Por|" .. sBaseName .. sFormName .. "|Neutral",  sPath, gciHDX3, gciHDY0, gciWid0, gciHei0, gciNoTransparencies, sAutoloaderName, sAutoloaderPath .. "Neutral")
        fnRipImageAuto("Por|" .. sBaseName .. sFormName .. "|Sad",      sPath, gciHDX0, gciHDY1, gciWid0, gciHei0, gciNoTransparencies, sAutoloaderName, sAutoloaderPath .. "Sad")
        fnRipImageAuto("Por|" .. sBaseName .. sFormName .. "|Surprise", sPath, gciHDX1, gciHDY1, gciWid0, gciHei0, gciNoTransparencies, sAutoloaderName, sAutoloaderPath .. "Surprise")
        fnRipImageAuto("Por|" .. sBaseName .. sFormName .. "|Offended", sPath, gciHDX2, gciHDY1, gciWid0, gciHei0, gciNoTransparencies, sAutoloaderName, sAutoloaderPath .. "Offended")
        fnRipImageAuto("Por|" .. sBaseName .. sFormName .. "|Cry",      sPath, gciHDX0, gciHDY2, gciWid0, gciHei0, gciNoTransparencies, sAutoloaderName, sAutoloaderPath .. "Cry")
        fnRipImageAuto("Por|" .. sBaseName .. sFormName .. "|Laugh",    sPath, gciHDX1, gciHDY2, gciWid0, gciHei0, gciNoTransparencies, sAutoloaderName, sAutoloaderPath .. "Laugh")
        fnRipImageAuto("Por|" .. sBaseName .. sFormName .. "|Angry",    sPath, gciHDX2, gciHDY2, gciWid0, gciHei0, gciNoTransparencies, sAutoloaderName, sAutoloaderPath .. "Angry")
    
    --Only neutral and smirk emotions:
    else
        fnRipImageAuto("Por|" .. sBaseName .. sFormName .. "|Neutral", sPath, gciHDX0, gciHDY0, gciWid0, gciHei0, gciNoTransparencies, sAutoloaderName, sAutoloaderPath .. "Neutral")
        fnRipImageAuto("Por|" .. sBaseName .. sFormName .. "|Smirk",   sPath, gciHDX3, gciHDY0, gciWid0, gciHei0, gciNoTransparencies, sAutoloaderName, sAutoloaderPath .. "Smirk")
    end
end

-- |[General Purpose]|
fnAutoRip("Mei", "Alraune",     sBasePath .. "Mei_Alraune_Emote.png",     false)
fnAutoRip("Mei", "Bee",         sBasePath .. "Mei_Bee_Emote.png",         false)
fnAutoRip("Mei", "BeeQueen",    sBasePath .. "Mei_BeeQueen_Emote.png",    false)
fnAutoRip("Mei", "Ghost",       sBasePath .. "Mei_Ghost_Emote.png",       false)
fnAutoRip("Mei", "Human",       sBasePath .. "Mei_Human_Emote.png",       false)
fnAutoRip("Mei", "Slime",       sBasePath .. "Mei_Slime_Emote.png",       false)
fnAutoRip("Mei", "SlimeBlue",   sBasePath .. "Mei_SlimeBlue_Emote.png",   false)
fnAutoRip("Mei", "SlimeInk",    sBasePath .. "Mei_SlimeInk_Emote.png",    false)
fnAutoRip("Mei", "SlimePink",   sBasePath .. "Mei_SlimePink_Emote.png",   false)
fnAutoRip("Mei", "SlimeYellow", sBasePath .. "Mei_SlimeYellow_Emote.png", false)
fnAutoRip("Mei", "Werecat",     sBasePath .. "Mei_Werecat_Emote.png",     false)
fnAutoRip("Mei", "Wisphag",     sBasePath .. "Mei_Wisphag_Emote.png",     false)
fnAutoRip("Mei", "Gravemarker", sBasePath .. "Mei_Gravemarker_Emote.png", false)
fnAutoRip("Mei", "RubberQueen", sBasePath .. "Mei_RubberQueen_Emote.png", false)

-- |[Special Frames]|
--One-offs that don't have a full emotion set. Each one needs to create its own autoloader set.
sAutoloaderName = "AutoLoad|Adventure|Emote_Mei_"
sAutoloaderPath = "Root/Images/Portraits/MeiDialogue/"

--Rip.
fnRipImageAutoCr("Por|MeiAlraune|MC",  sBasePath .. "Mei_Alraune_Emote.png",   gciHDX3, gciHDY1, gciWid0, gciHei0, gciNoTransparencies, sAutoloaderName .. "AlrauneMC", sAutoloaderPath .. "AlrauneMC")
fnRipImageAutoCr("Por|MeiBee|MC",      sBasePath .. "Mei_Bee_Emote.png",       gciHDX3, gciHDY1, gciWid0, gciHei0, gciNoTransparencies, sAutoloaderName .. "BeeMC",     sAutoloaderPath .. "BeeMC")
fnRipImageAutoCr("Por|MeiBeeQueen|MC", sBasePath .. "Mei_BeeQueen_Emote.png",  gciHDX3, gciHDY1, gciWid0, gciHei0, gciNoTransparencies, sAutoloaderName .. "BeeQueenMC",sAutoloaderPath .. "BeeQueenMC")
fnRipImageAutoCr("Por|Mei|MC",         sBasePath .. "Mei_Human_Emote.png",     gciHDX3, gciHDY1, gciWid0, gciHei0, gciNoTransparencies, sAutoloaderName .. "HumanMC",   sAutoloaderPath .. "HumanMC")
fnRipImageAutoCr("Por|Mei|Rubber",     sBasePath .. "Mei_Rubber_Emote.png",    gciHDX0, gciHDY0, gciWid0, gciHei0, gciNoTransparencies, sAutoloaderName .. "Rubber",    sAutoloaderPath .. "Rubber")
fnRipImageAutoCr("Por|Mei|Mannequin",  sBasePath .. "Mei_Mannequin_Emote.png", gciHDX0, gciHDY0, gciWid0, gciHei0, gciNoTransparencies, sAutoloaderName .. "Mannequin", sAutoloaderPath .. "Mannequin")

-- |[ ======================================== Combat ========================================== ]|
-- |[Autoloader Info]|
sAutoloaderName = "AutoLoad|Adventure|Combat_Mei_"
sAutoloaderPath = "Root/Images/Portraits/Combat/Mei_"

-- |[Combat Frames]|
--This is just a simple list of combat frames.
fnRipImageAutoCr("Party|Mei_Alraune",     sBasePath .. "Mei_Alraune_Combat.png",     0, 0, -1, -1, gciNoTransparencies, sAutoloaderName .. "Alraune",     sAutoloaderPath .. "Alraune")
fnRipImageAutoCr("Party|Mei_AlrauneMC",   sBasePath .. "Mei_Alraune_CombatMC.png",   0, 0, -1, -1, gciNoTransparencies, sAutoloaderName .. "AlrauneMC",   sAutoloaderPath .. "AlrauneMC")
fnRipImageAutoCr("Party|Mei_Bee",         sBasePath .. "Mei_Bee_Combat.png",         0, 0, -1, -1, gciNoTransparencies, sAutoloaderName .. "Bee",         sAutoloaderPath .. "Bee")
fnRipImageAutoCr("Party|Mei_BeeQueen",    sBasePath .. "Mei_BeeQueen_Combat.png",    0, 0, -1, -1, gciNoTransparencies, sAutoloaderName .. "BeeQueen",    sAutoloaderPath .. "BeeQueen")
fnRipImageAutoCr("Party|Mei_Ghost",       sBasePath .. "Mei_Ghost_Combat.png",       0, 0, -1, -1, gciNoTransparencies, sAutoloaderName .. "Ghost",       sAutoloaderPath .. "Ghost")
fnRipImageAutoCr("Party|Mei_Gravemarker", sBasePath .. "Mei_Gravemarker_Combat.png", 0, 0, -1, -1, gciNoTransparencies, sAutoloaderName .. "Gravemarker", sAutoloaderPath .. "Gravemarker")
fnRipImageAutoCr("Party|Mei_Human",       sBasePath .. "Mei_Human_Combat.png",       0, 0, -1, -1, gciNoTransparencies, sAutoloaderName .. "Human",       sAutoloaderPath .. "Human")
fnRipImageAutoCr("Party|Mei_HumanMC",     sBasePath .. "Mei_Human_CombatMC.png",     0, 0, -1, -1, gciNoTransparencies, sAutoloaderName .. "HumanMC",     sAutoloaderPath .. "HumanMC")
fnRipImageAutoCr("Party|Mei_Mannequin",   sBasePath .. "Mei_Mannequin_Combat.png",   0, 0, -1, -1, gciNoTransparencies, sAutoloaderName .. "Mannequin",   sAutoloaderPath .. "Mannequin")
fnRipImageAutoCr("Party|Mei_Rubber",      sBasePath .. "Mei_Rubber_Combat.png",      0, 0, -1, -1, gciNoTransparencies, sAutoloaderName .. "Rubber",      sAutoloaderPath .. "Rubber")
fnRipImageAutoCr("Party|Mei_RubberQueen", sBasePath .. "Mei_RubberQueen_Combat.png", 0, 0, -1, -1, gciNoTransparencies, sAutoloaderName .. "RubberQueen", sAutoloaderPath .. "RubberQueen")
fnRipImageAutoCr("Party|Mei_Slime",       sBasePath .. "Mei_Slime_Combat.png",       0, 0, -1, -1, gciNoTransparencies, sAutoloaderName .. "Slime",       sAutoloaderPath .. "Slime")
fnRipImageAutoCr("Party|Mei_SlimeBlue",   sBasePath .. "Mei_SlimeBlue_Combat.png",   0, 0, -1, -1, gciNoTransparencies, sAutoloaderName .. "SlimeBlue",   sAutoloaderPath .. "SlimeBlue")
fnRipImageAutoCr("Party|Mei_SlimeInk",    sBasePath .. "Mei_SlimeInk_Combat.png",    0, 0, -1, -1, gciNoTransparencies, sAutoloaderName .. "SlimeInk",    sAutoloaderPath .. "SlimeInk")
fnRipImageAutoCr("Party|Mei_SlimePink",   sBasePath .. "Mei_SlimePink_Combat.png",   0, 0, -1, -1, gciNoTransparencies, sAutoloaderName .. "SlimePink",   sAutoloaderPath .. "SlimePink")
fnRipImageAutoCr("Party|Mei_SlimeYellow", sBasePath .. "Mei_SlimeYellow_Combat.png", 0, 0, -1, -1, gciNoTransparencies, sAutoloaderName .. "SlimeYellow", sAutoloaderPath .. "SlimeYellow")
fnRipImageAutoCr("Party|Mei_Werecat",     sBasePath .. "Mei_Werecat_Combat.png",     0, 0, -1, -1, gciNoTransparencies, sAutoloaderName .. "Werecat",     sAutoloaderPath .. "Werecat")
fnRipImageAutoCr("Party|Mei_Wisphag",     sBasePath .. "Mei_Wisphag_Combat.png",     0, 0, -1, -1, gciNoTransparencies, sAutoloaderName .. "Wisphag",     sAutoloaderPath .. "Wisphag")
fnRipImageAutoCr("Party|Mei_Zombee",      sBasePath .. "Mei_Zombee_Combat.png",      0, 0, -1, -1, gciNoTransparencies, sAutoloaderName .. "Zombee",      sAutoloaderPath .. "Zombee")
fnRipImageAutoCr("Party|Mei_ZombeeQueen", sBasePath .. "Mei_ZombeeQueen_Combat.png", 0, 0, -1, -1, gciNoTransparencies, sAutoloaderName .. "ZombeeQueen", sAutoloaderPath .. "ZombeeQueen")

--Countermasks. Used for the UI.
fnRipImageAutoCr("Party|Mei_Alraune_Countermask", sBasePath .. "Mei_Alraune_CombatCountermask.png", 0, 0, -1, -1, gciNoTransparencies, sAutoloaderName .. "Alraune", sAutoloaderPath .. "Alraune")
fnRipImageAutoCr("Party|Mei_Human_Countermask",   sBasePath .. "Mei_Human_CombatCountermask.png",   0, 0, -1, -1, gciNoTransparencies, sAutoloaderName .. "Human",   sAutoloaderPath .. "Human")

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the SLF file.
SLF_Close()
