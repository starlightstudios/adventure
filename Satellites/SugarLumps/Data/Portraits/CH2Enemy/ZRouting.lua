-- |[ ============================ Chapter 2 Enemy Combat Portraits ============================ ]|
--Combat portraits for chapter 2's enemies.
local sBasePath = fnResolvePath()

-- |[File Creation]|
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_CH2Combat.slf")
else
	SLF_Open("Output/PortraitsLD_CH2Combat.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[Autoloader]|
local sAutoloaderName = "AutoLoad|Adventure|Portrait_Ch2_Enemy"
local sAutoloaderPath = "Root/Images/Portraits/Combat/"
local sAutoloaderPara = "Root/Images/Portraits/Paragon/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[ ========================================= Combat ========================================= ]|
-- |[Normal Enemies and Paragons]|
--Assemble list
local saList = {"BanditRed", "BanditGreen", "Bat", "Brimhog", "Buffodil", "BunEye", "Bunny", "Butterbomber", "CaveCrawler", "Enforcer", "FrostCrawler", "Grub", "GrubNest", "HarpyOfficer", "HarpyRecruit", "Kitsune", 
                "Omnigoose", "PoisonVine", "Pseudogriffon", "Raccoon", "RedCap", "Sevavi", "SuckFly", "Toxishroom", "Treant", "Turkerus", "Unielk"}

--For each element in the list, rip normal and paragon.
for i = 1, #saList, 1 do
    fnRipImageAuto("Enemy|"    .. saList[i], sBasePath .. "Enemy_"   .. saList[i] .. ".png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. saList[i])
    fnRipImageAuto("Paragon|"  .. saList[i], sBasePath .. "Paragon_" .. saList[i] .. ".png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPara .. saList[i])
end

-- |[Bosses]|
-- |[Special]|
-- |[Chapter 0 Entities]|
--Entities on this list appear in chapter 1, and are stored in the chapter 0 datafile.
saList = {"Wisphag"}
local sAutoload = "AutoLoad|Adventure|Portrait_Ch0_Enemy_For_Ch2"
SLF_RegisterAutoLoadLump(sAutoload)

for i = 1, #saList, 1 do
    AutoLoader_Register(sAutoload, "Enemy|"   .. saList[i], "Root/Images/Portraits/Combat/"  .. saList[i])
    AutoLoader_Register(sAutoload, "Paragon|" .. saList[i], "Root/Images/Portraits/Paragon/" .. saList[i])
end

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the SLF file.
SLF_Close()
