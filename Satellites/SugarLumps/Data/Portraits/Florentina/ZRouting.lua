-- |[ ================================== Florentina Portraits ================================== ]|
--Subscript for Florentina's portraits. Contains both combat and emotes.
local sBasePath = fnResolvePath()

-- |[File Creation]|
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_Florentina.slf")
else
	SLF_Open("Output/PortraitsLD_Florentina.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ========================================= Emotes ========================================= ]|
-- |[Automated Ripper Function]|
--Ripper designed for Florentina's emote pattern.
local function fnAutoRip(sBaseName, sFormName, sPath)
    
    --Autoloader.
    local sAutoloaderName = "AutoLoad|Adventure|Emote_Florentina_" .. sFormName
    local sAutoloaderPath = "Root/Images/Portraits/FlorentinaDialogue/" .. sFormName
    SLF_RegisterAutoLoadLump(sAutoloaderName)
    
    --Rip emotions.
    fnRipImageAuto("Por|" .. sBaseName .. sFormName .. "|Neutral",  sPath, gciHDX0, gciHDY0, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Neutral")
    fnRipImageAuto("Por|" .. sBaseName .. sFormName .. "|Happy",    sPath, gciHDX1, gciHDY0, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Happy")
    fnRipImageAuto("Por|" .. sBaseName .. sFormName .. "|Blush",    sPath, gciHDX2, gciHDY0, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Blush")
    fnRipImageAuto("Por|" .. sBaseName .. sFormName .. "|Facepalm", sPath, gciHDX0, gciHDY1, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Facepalm")
    fnRipImageAuto("Por|" .. sBaseName .. sFormName .. "|Surprise", sPath, gciHDX1, gciHDY1, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Surprise")
    fnRipImageAuto("Por|" .. sBaseName .. sFormName .. "|Offended", sPath, gciHDX2, gciHDY1, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Offended")
    fnRipImageAuto("Por|" .. sBaseName .. sFormName .. "|Confused", sPath, gciHDX0, gciHDY2, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Confused")
end

-- |[General Purpose]|
fnAutoRip("Florentina", "Merchant",        sBasePath .. "Florentina_Merchant_Emote.png")
fnAutoRip("Florentina", "Mediator",        sBasePath .. "Florentina_Mediator_Emote.png")
fnAutoRip("Florentina", "Treasure Hunter", sBasePath .. "Florentina_TreasureHunter_Emote.png")
fnAutoRip("Florentina", "Agarist",         sBasePath .. "Florentina_Agarist_Emote.png")

-- |[Special Frames]|
--One-offs that don't have a full emotion set. Each one needs to create its own autoloader set.
sAutoloaderName = "AutoLoad|Adventure|Emote_Florentina_"
sAutoloaderPath = "Root/Images/Portraits/FlorentinaDialogue/"

--Rip.
fnRipImageAutoCr("Por|FlorentinaMerchant|Rubber", sBasePath .. "Florentina_Merchant_Emote.png", gciHDX3, gciHDY2, gciWid0, gciHei0, 0, sAutoloaderName .. "MerchantRubber", sAutoloaderPath .. "MerchantRubber")
fnRipImageAutoCr("Por|FlorentinaLurker|Neutral",         sBasePath .. "Florentina_Lurker_Emote.png",   gciHDX0, gciHDY0, gciWid0, gciHei0, 0, sAutoloaderName .. "Lurker",         sAutoloaderPath .. "Lurker")

-- |[ ========================================= Combat ========================================= ]|
-- |[Autoloader Info]|
sAutoloaderName = "AutoLoad|Adventure|Combat_Florentina_"
sAutoloaderPath = "Root/Images/Portraits/Combat/Florentina_"

-- |[Combat Frames]|
--This is just a simple list of combat frames.
fnRipImageAutoCr("Party|Florentina_Lurker",         sBasePath .. "Florentina_Lurker_Combat.png",         0, 0, -1, -1, 0, sAutoloaderName .. "Lurker",          sAutoloaderPath .. "Lurker")
fnRipImageAutoCr("Party|Florentina_Mediator",       sBasePath .. "Florentina_Mediator_Combat.png",       0, 0, -1, -1, 0, sAutoloaderName .. "Mediator",        sAutoloaderPath .. "Mediator")
fnRipImageAutoCr("Party|Florentina_Merchant",       sBasePath .. "Florentina_Merchant_Combat.png",       0, 0, -1, -1, 0, sAutoloaderName .. "Merchant",        sAutoloaderPath .. "Merchant")
fnRipImageAutoCr("Party|Florentina_Agarist",        sBasePath .. "Florentina_Agarist_Combat.png",        0, 0, -1, -1, 0, sAutoloaderName .. "Agarist",         sAutoloaderPath .. "Agarist")
fnRipImageAutoCr("Party|Florentina_TreasureHunter", sBasePath .. "Florentina_TreasureHunter_Combat.png", 0, 0, -1, -1, 0, sAutoloaderName .. "Treasure Hunter", sAutoloaderPath .. "Treasure Hunter")

--Countermask, used for the sitting poses.
fnRipImageAutoCr("Party|FlorentinaCountermask", sBasePath .. "Florentina_CombatCountermask.png", 0, 0, -1, -1, 0, sAutoloaderName .. "Merchant", sAutoloaderPath .. "Merchant")

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the SLF file.
SLF_Close()
