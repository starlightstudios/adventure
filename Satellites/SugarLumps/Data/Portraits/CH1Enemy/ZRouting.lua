-- |[ ============================ Chapter 1 Enemy Combat Portraits ============================ ]|
--Combat portraits for chapter 1's enemies.
local sBasePath = fnResolvePath()

-- |[File Creation]|
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_CH1Combat.slf")
else
	SLF_Open("Output/PortraitsLD_CH1Combat.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ========================================= Combat ========================================= ]|
-- |[Autoloader]|
local sAutoloaderName = "AutoLoad|Adventure|Portrait_Ch1_Enemy"
local sAutoloaderPath = "Root/Images/Portraits/Combat/"
local sAutoloaderPara = "Root/Images/Portraits/Paragon/"
SLF_RegisterAutoLoadLump(sAutoloaderName)
    
-- |[Normal Enemies and Paragons]|
--Assemble list
local saList = {"Alraune", "BeeBuddy", "BeeGirl", "BloodyMirror", "CandleHaunt", "CultistF", "CultistM", "EyeDrawer", "Gravemarker", "Ink_Slime", "Jelli", "MaidGhost", "MannBanditF", "MannBanditM", "Mushraune", "Slime",
                "SlimeB", "SlimeG", "Sproutling", "SproutPecker", "Werecat", "Werecat_Burglar", "Zombee"}

--For each element in the list, rip normal and paragon.
for i = 1, #saList, 1 do
    fnRipImageAuto("Enemy|"   .. saList[i], sBasePath .. "Enemy_"   .. saList[i] .. ".png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. saList[i])
    fnRipImageAuto("Paragon|" .. saList[i], sBasePath .. "Paragon_" .. saList[i] .. ".png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPara .. saList[i])
end

-- |[Bosses]|
fnRipImageAuto("Enemy|Arachnophelia", sBasePath .. "Boss_Arachnophelia.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Arachnophelia")
fnRipImageAuto("Enemy|Infirm",        sBasePath .. "Boss_Infirm.png",        0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Infirm")
fnRipImageAuto("Enemy|Mycela",        sBasePath .. "Boss_Mycela.png",        0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Mycela")
fnRipImageAuto("Enemy|Victoria",      sBasePath .. "Boss_Victoria.png",      0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Victoria")

-- |[Special]|
fnRipImageAuto("Enemy|AlrauneRubber",        sBasePath .. "Rubber_Alraune.png",         0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "AlrauneRubber")
fnRipImageAuto("Enemy|BeeGirlRubber",        sBasePath .. "Rubber_BeeGirl.png",         0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "BeeGirlRubber")
fnRipImageAuto("Enemy|CultistFRubber",       sBasePath .. "Rubber_CultistF.png",        0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "CultistFRubber")
fnRipImageAuto("Enemy|CultistMRubber",       sBasePath .. "Rubber_CultistM.png",        0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "CultistMRubber")
fnRipImageAuto("Enemy|WerecatRubber",        sBasePath .. "Rubber_Werecat.png",         0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "WerecatRubber")
fnRipImageAuto("Enemy|WerecatBurglarRubber", sBasePath .. "Rubber_Werecat_Burglar.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "WerecatBurglarRubber")

fnRipImageAuto("Paragon|AlrauneRubber",        sBasePath .. "ParagonRubber_Alraune.png",         0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPara .. "AlrauneRubber")
fnRipImageAuto("Paragon|BeeGirlRubber",        sBasePath .. "ParagonRubber_BeeGirl.png",         0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPara .. "BeeGirlRubber")
fnRipImageAuto("Paragon|CultistFRubber",       sBasePath .. "ParagonRubber_CultistF.png",        0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPara .. "CultistFRubber")
fnRipImageAuto("Paragon|CultistMRubber",       sBasePath .. "ParagonRubber_CultistM.png",        0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPara .. "CultistMRubber")
fnRipImageAuto("Paragon|WerecatRubber",        sBasePath .. "ParagonRubber_Werecat.png",         0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPara .. "WerecatRubber")
fnRipImageAuto("Paragon|WerecatBurglarRubber", sBasePath .. "ParagonRubber_Werecat_Burglar.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPara .. "WerecatBurglarRubber")

-- |[Chapter 0 Entities]|
--Entities on this list appear in chapter 1, and are stored in the chapter 0 datafile.
saList = {"BanditCatBlack", "BanditF", "BanditM", "DemonMerc", "SkullCrawler", "Wisphag"}
local sAutoload = "AutoLoad|Adventure|Portrait_Ch0_Enemy_For_Ch1"
SLF_RegisterAutoLoadLump(sAutoload)

for i = 1, #saList, 1 do
    AutoLoader_Register(sAutoload, "Enemy|"   .. saList[i], "Root/Images/Portraits/Combat/"  .. saList[i])
    AutoLoader_Register(sAutoload, "Paragon|" .. saList[i], "Root/Images/Portraits/Paragon/" .. saList[i])
end

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the SLF file.
SLF_Close()
