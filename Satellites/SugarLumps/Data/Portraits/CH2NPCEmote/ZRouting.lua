-- |[ ===================================== NPC Portraits ====================================== ]|
--Subscript for NPCs portraits. Contains both combat and emotes.
local sBasePath = fnResolvePath()

-- |[File Creation]|
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_CH2Emote.slf")
else
	SLF_Open("Output/PortraitsLD_CH2Emote.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[Autoloader]|
sAutoloaderName = "AutoLoad|Adventure|Emote_Ch2_NPC"
sAutoloaderPath = "Root/Images/Portraits/Ch2Generic/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[ ========================================= Emotes ========================================= ]|
-- |[Master Sheet NPCs]|
--Normal Width, Page A
fnRipImageAuto("Por|Takahn|Neutral",        sBasePath .. "NPC_EmoteA.png", gciHDX0, gciHDY0, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Takahn|Neutral")
fnRipImageAuto("Por|KitsuneA|Neutral",      sBasePath .. "NPC_EmoteA.png", gciHDX1, gciHDY0, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "KitsuneA|Neutral")
fnRipImageAuto("Por|Chikage|Neutral",       sBasePath .. "NPC_EmoteA.png", gciHDX2, gciHDY0, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Chikage|Neutral")
fnRipImageAuto("Por|Esmerelda|Neutral",     sBasePath .. "NPC_EmoteA.png", gciHDX0, gciHDY1, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Esmerelda|Neutral")
fnRipImageAuto("Por|BunnySmuggler|Neutral", sBasePath .. "NPC_EmoteA.png", gciHDX1, gciHDY1, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "BunnySmuggler|Neutral")
fnRipImageAuto("Por|Angelface|Neutral",     sBasePath .. "NPC_EmoteA.png", gciHDX2, gciHDY1, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Angelface|Neutral")
fnRipImageAuto("Por|Arisu|Neutral",         sBasePath .. "NPC_EmoteA.png", gciHDX3, gciHDY1, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Arisu|Neutral")
fnRipImageAuto("Por|HarpyRecruit|Neutral",  sBasePath .. "NPC_EmoteA.png", gciHDX0, gciHDY2, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "HarpyRecruit|Neutral")
fnRipImageAuto("Por|HarpyOfficer|Neutral",  sBasePath .. "NPC_EmoteA.png", gciHDX1, gciHDY2, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "HarpyOfficer|Neutral")
fnRipImageAuto("Por|Morus|Neutral",         sBasePath .. "NPC_EmoteA.png", gciHDX2, gciHDY2, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Morus|Neutral")
fnRipImageAuto("Por|Morus|Smirk",           sBasePath .. "NPC_EmoteA.png", gciHDX3, gciHDY2, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Morus|Smirk")
fnRipImageAuto("Por|Sevavi|Neutral",        sBasePath .. "NPC_EmoteA.png", gciHDX0, gciHDY3, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Sevavi|Neutral")
fnRipImageAuto("Por|Sevavi|Neutral",        sBasePath .. "NPC_EmoteA.png", gciHDX1, gciHDY3, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Sevavi|Smirk")
fnRipImageAuto("Por|Tetra|Neutral",         sBasePath .. "NPC_EmoteA.png", gciHDX2, gciHDY3, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Tetra|Neutral")
fnRipImageAuto("Por|Tetra|Smirk",           sBasePath .. "NPC_EmoteA.png", gciHDX3, gciHDY3, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Tetra|Smirk")

--Normal Width, Page B
fnRipImageAuto("Por|Arne|Neutral",    sBasePath .. "NPC_EmoteB.png", gciHDX0, gciHDY0, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Arne", sAutoloaderPath .. "Arne|Neutral")
fnRipImageAuto("Por|Arne|Smirk",      sBasePath .. "NPC_EmoteB.png", gciHDX1, gciHDY0, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Arne", sAutoloaderPath .. "Arne|Smirk")
fnRipImageAuto("Por|Raccoon|Neutral", sBasePath .. "NPC_EmoteB.png", gciHDX2, gciHDY0, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Raccoon|Neutral")

--Extended Width
--ImageLump_Rip("Por|Bee|Neutral", sBasePath .. "NPC_EmoteWide.png", gciWideX0, gciWideY0, gciWideWid, gciWideHei, 0)

-- |[Gina]|
local sPath = sBasePath .. "Gina.png"
fnRipImageAutoCr("Por|Gina|Neutral", sPath, gciHDX0, gciHDY0, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Gina", "Root/Images/Portraits/Gina/Neutral")
fnRipImageAutoCr("Por|Gina|Shock",   sPath, gciHDX1, gciHDY0, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Gina", "Root/Images/Portraits/Gina/Shock")
fnRipImageAutoCr("Por|Gina|Space",   sPath, gciHDX2, gciHDY0, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Gina", "Root/Images/Portraits/Gina/Space")
fnRipImageAutoCr("Por|Gina|Happy",   sPath, gciHDX3, gciHDY0, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Gina", "Root/Images/Portraits/Gina/Happy")

-- |[Raelynn]|
sPath = sBasePath .. "Raelynn.png"
fnRipImageAutoCr("Por|Raelynn|Neutral", sPath, gciHDX0, gciHDY0, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Raelynn", "Root/Images/Portraits/Raelynn/Neutral")
fnRipImageAutoCr("Por|Raelynn|Snide",   sPath, gciHDX1, gciHDY0, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Raelynn", "Root/Images/Portraits/Raelynn/Snide")
fnRipImageAutoCr("Por|Raelynn|Laugh",   sPath, gciHDX2, gciHDY0, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Raelynn", "Root/Images/Portraits/Raelynn/Laugh")
fnRipImageAutoCr("Por|Raelynn|Flirt",   sPath, gciHDX3, gciHDY0, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Raelynn", "Root/Images/Portraits/Raelynn/Flirt")

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the SLF file.
SLF_Close()
