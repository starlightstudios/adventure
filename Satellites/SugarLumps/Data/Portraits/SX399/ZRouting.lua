-- |[ ==================================== SX-399 Portraits ==================================== ]|
--Subscript for SX-399's portraits. Contains both combat and emotes.
local sBasePath = fnResolvePath()

-- |[File Creation]|
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_SX399.slf")
else
	SLF_Open("Output/PortraitsLD_SX399.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ========================================= Emotes ========================================= ]|
-- |[Automated Ripper Function]|
--Ripper designed for SX-399's emote pattern.
local fnAutoRip = function(sBaseName, sFormName, sPath)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Neutral", sPath, gciHDX0, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Happy",   sPath, gciHDX1, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Blush",   sPath, gciHDX2, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Smirk",   sPath, gciHDX3, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Sad",     sPath, gciHDX0, gciHDY1, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Flirt",   sPath, gciHDX1, gciHDY1, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Angry",   sPath, gciHDX2, gciHDY1, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Flirt2",  sPath, gciHDX0, gciHDY2, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Steam",   sPath, gciHDX3, gciHDY2, gciWid0, gciHei0, 0)
end

-- |[General Purpose]|
fnAutoRip("SX-399", "", sBasePath .. "SX-399_Shocktrooper_Emote.png")

-- |[Special Frames]|
--None yet.

-- |[ ========================================= Combat ========================================= ]|
--This is just a simple list of combat frames.
ImageLump_Rip("Party|SX-399", sBasePath .. "SX-399_Shocktrooper_Combat.png", 0, 0, -1, -1, 0)

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the SLF file.
SLF_Close()
