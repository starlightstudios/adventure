-- |[ ==================================== Yukina Portraits ==================================== ]|
--Yukina, the elder of the kitsune village in Trafal.
local sBasePath = fnResolvePath()

-- |[File Creation]|
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_Yukina.slf")
else
	SLF_Open("Output/PortraitsLD_Yukina.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[Autoloader]|
local sAutoloaderName = "AutoLoad|Adventure|Emote_Yukina"
local sAutoloaderPath = "Root/Images/Portraits/Yukina/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[ ========================================= Emotes ========================================= ]|
--Just neutral.
fnRipImageAuto("Por|YukinaKitsune|Neutral", sBasePath .. "Yukina_Emote.png", gciWideX0, gciWideY0, gciWideWid, gciWideHei, 0, sAutoloaderName, sAutoloaderPath .. "Neutral")
fnRipImageAuto("Por|YukinaKitsune|Upset",   sBasePath .. "Yukina_Emote.png", gciWideX1, gciWideY0, gciWideWid, gciWideHei, 0, sAutoloaderName, sAutoloaderPath .. "Upset")

-- |[ ========================================= Combat ========================================= ]|
--This is just a simple list of combat frames.

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the SLF file.
SLF_Close()
