-- |[ ==================================== Izuna Portraits ===================================== ]|
--Subscript for Izuna's portraits. Contains both combat and emotes.
local sBasePath = fnResolvePath()

-- |[File Creation]|
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_Izuna.slf")
else
	SLF_Open("Output/PortraitsLD_Izuna.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ======================================== Emotes ========================================== ]|
-- |[Autoloader Info]|
local sAutoloaderName = "AutoLoad|Adventure|Emote_Izuna_Miko"
local sAutoloaderPath = "Root/Images/Portraits/IzunaMikoDialogue/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[Automated Ripper Function]|
--Ripper designed for this character's emote pattern.
local function fnAutoRip(psBaseName, psFormName, psPath, pbOnlyNeutralSmirk)
    
    --Switch based on emotion set.
    if(pbOnlyNeutralSmirk == false) then
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Neutral", psPath, gciHDX0, gciHDY0, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Neutral")
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Happy",   psPath, gciHDX1, gciHDY0, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Happy")
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Cry",     psPath, gciHDX2, gciHDY0, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Cry")
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Smirk",   psPath, gciHDX3, gciHDY0, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Smirk")
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Explain", psPath, gciHDX0, gciHDY1, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Explain")
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Jot",     psPath, gciHDX1, gciHDY1, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Jot")
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Ugh",     psPath, gciHDX2, gciHDY1, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Ugh")
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Trip",    psPath, gciHDX3, gciHDY1, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Trip")
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Blush",   psPath, gciHDX0, gciHDY2, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Blush")
    
    --Only neutral and smirk emotions:
    else
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Neutral", psPath, gciHDX0, gciHDY0, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Neutral")
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Smirk",   psPath, gciHDX3, gciHDY0, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Smirk")
    end
end

-- |[General Purpose]|
fnAutoRip("Izuna", "Miko", sBasePath .. "Izuna_Miko_Emote.png", false)

-- |[Special Frames]|
--Put them here.

-- |[ ======================================== Combat ========================================== ]|
-- |[Autoloader Info]|
sAutoloaderName = "AutoLoad|Adventure|Combat_Izuna"
sAutoloaderPath = "Root/Images/Portraits/Combat/Izuna_"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[Combat Frames]|
--This is just a simple list of combat frames.
fnRipImageAuto("Party|Izuna_Miko", sBasePath .. "Izuna_Miko.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Miko")

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the SLF file.
SLF_Close()
