-- |[ ============================ Chapter 5 Enemy Combat Portraits ============================ ]|
--Combat portraits for chapter 5's enemies.
local sBasePath = fnResolvePath()

-- |[File Creation]|
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_CH5Combat.slf")
else
	SLF_Open("Output/PortraitsLD_CH5Combat.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ========================================= Combat ========================================= ]|
--Normal Enemies
ImageLump_Rip("Enemy|DarkmatterGirl", sBasePath .. "Enemy_DarkmatterGirl.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|Doll",           sBasePath .. "Enemy_Doll.png",           0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|Dreamer",        sBasePath .. "Enemy_Dreamer.png",        0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|GolemLord",      sBasePath .. "Enemy_GolemLord.png",      0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|GolemSlave",     sBasePath .. "Enemy_GolemSlave.png",     0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|LatexDrone",     sBasePath .. "Enemy_LatexDrone.png",     0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|Motilvac",       sBasePath .. "Enemy_Motilvac.png",       0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|MrWipey",        sBasePath .. "Enemy_MrWipey.png",        0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|Plannerbot",     sBasePath .. "Enemy_Plannerbot.png",     0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|Raibie",         sBasePath .. "Enemy_Raibie.png",         0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|Scraprat",       sBasePath .. "Enemy_Scraprat.png",       0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|Secrebot",       sBasePath .. "Enemy_Secrebot.png",       0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|SecrebotPink",   sBasePath .. "Enemy_SecrebotPink.png",   0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|SecrebotYellow", sBasePath .. "Enemy_SecrebotYellow.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|WreckedBot",     sBasePath .. "Enemy_WreckedBot.png",     0, 0, -1, -1, 0)

--Paragons
ImageLump_Rip("Paragon|DarkmatterGirl", sBasePath .. "Paragon_DarkmatterGirl.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Paragon|Doll",           sBasePath .. "Paragon_Doll.png",           0, 0, -1, -1, 0)
ImageLump_Rip("Paragon|Dreamer",        sBasePath .. "Paragon_Dreamer.png",        0, 0, -1, -1, 0)
ImageLump_Rip("Paragon|GolemLord",      sBasePath .. "Paragon_GolemLord.png",      0, 0, -1, -1, 0)
ImageLump_Rip("Paragon|GolemSlave",     sBasePath .. "Paragon_GolemSlave.png",     0, 0, -1, -1, 0)
ImageLump_Rip("Paragon|LatexDrone",     sBasePath .. "Paragon_LatexDrone.png",     0, 0, -1, -1, 0)
ImageLump_Rip("Paragon|Motilvac",       sBasePath .. "Paragon_Motilvac.png",       0, 0, -1, -1, 0)
ImageLump_Rip("Paragon|MrWipey",        sBasePath .. "Paragon_MrWipey.png",        0, 0, -1, -1, 0)
ImageLump_Rip("Paragon|Plannerbot",     sBasePath .. "Paragon_Plannerbot.png",     0, 0, -1, -1, 0)
ImageLump_Rip("Paragon|Raibie",         sBasePath .. "Paragon_Raibie.png",         0, 0, -1, -1, 0)
ImageLump_Rip("Paragon|Scraprat",       sBasePath .. "Paragon_Scraprat.png",       0, 0, -1, -1, 0)
ImageLump_Rip("Paragon|Secrebot",       sBasePath .. "Paragon_Secrebot.png",       0, 0, -1, -1, 0)
ImageLump_Rip("Paragon|SecrebotPink",   sBasePath .. "Paragon_SecrebotPink.png",   0, 0, -1, -1, 0)
ImageLump_Rip("Paragon|SecrebotYellow", sBasePath .. "Paragon_SecrebotYellow.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Paragon|WreckedBot",     sBasePath .. "Paragon_WreckedBot.png",     0, 0, -1, -1, 0)

--Bosses
ImageLump_Rip("Enemy|609144",          sBasePath .. "Boss_609144.png",          0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|Serenity",        sBasePath .. "Boss_Serenity.png",        0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|TechMonstrosity", sBasePath .. "Boss_TechMonstrosity.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Enemy|Vivify",          sBasePath .. "Boss_Vivify.png",          0, 0, -1, -1, 0)

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the SLF file.
SLF_Close()
