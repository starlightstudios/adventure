-- |[ ============================ Chapter 0 Enemy Combat Portraits ============================ ]|
--Combat portraits for enemies not restricted or predominant in one single chapter.
local sBasePath = fnResolvePath()

-- |[File Creation]|
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_CH0Combat.slf")
else
	SLF_Open("Output/PortraitsLD_CH0Combat.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ========================================= Combat ========================================= ]|
-- |[Normal Enemies and Paragons]|
--Assemble list
local saList = {"BanditCatBlack", "BanditF", "BanditM", "BandageGoblin", "BestFriend", "DemonMerc", "Hoodie", "Horrible", "InnGeisha", "MirrorImage", "SkullCrawler", "VoidRift", "Wisphag"}

--For each element in the list, rip normal and paragon.
for i = 1, #saList, 1 do
    ImageLump_Rip("Enemy|"   .. saList[i], sBasePath .. "Enemy_"   .. saList[i] .. ".png", 0, 0, -1, -1, gciNoTransparencies)
    ImageLump_Rip("Paragon|" .. saList[i], sBasePath .. "Paragon_" .. saList[i] .. ".png", 0, 0, -1, -1, gciNoTransparencies)
end

-- |[ ====================================== Autoloaders ======================================= ]|
-- |[Chapter 1]|
--Entities on this list appear in chapter 1.
saList = {"BanditCatBlack", "BanditF", "BanditM", "DemonMerc", "SkullCrawler", "Wisphag"}
local sAutoload = "AutoLoad|Adventure|Portrait_Ch0_Enemy_For_Ch1"
SLF_RegisterAutoLoadLump(sAutoload)

for i = 1, #saList, 1 do
    AutoLoader_Register(sAutoload, "Enemy|"   .. saList[i], "Root/Images/Portraits/Combat/"  .. saList[i])
    AutoLoader_Register(sAutoload, "Paragon|" .. saList[i], "Root/Images/Portraits/Paragon/" .. saList[i])
end

--Chapter 2 portraits.
sAutoload = "AutoLoad|Adventure|Portrait_Ch0_Enemy_For_Ch2"
SLF_RegisterAutoLoadLump(sAutoload)
AutoLoader_Register(sAutoload, "Enemy|Wisphag",   "Root/Images/Portraits/Combat/Wisphag")
AutoLoader_Register(sAutoload, "Paragon|Wisphag", "Root/Images/Portraits/Paragon/Wisphag")

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the SLF file.
SLF_Close()
