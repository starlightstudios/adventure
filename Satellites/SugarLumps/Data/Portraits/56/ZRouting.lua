-- |[ ====================================== 56 Portraits ====================================== ]|
--Subscript for 56's portraits. Contains both combat and emotes, though 56 never fights you.
local sBasePath = fnResolvePath()

-- |[File Creation]|
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_56.slf")
else
	SLF_Open("Output/PortraitsLD_56.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ========================================= Emotes ========================================= ]|
-- |[Automated Ripper Function]|
--Ripper designed for 55's emote pattern.
local fnAutoRip = function(sBaseName, sFormName, sPath)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Neutral",   sPath, gciHDX0, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Punchable", sPath, gciHDX1, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Angry",     sPath, gciHDX2, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Yelling",   sPath, gciHDX3, gciHDY0, gciWid0, gciHei0, 0)
end

-- |[General Purpose]|
fnAutoRip("56", "",     sBasePath .. "56_Doll_Emote.png")
fnAutoRip("56", "Nude", sBasePath .. "56_DollNude_Emote.png")

-- |[Special Frames]|
--None yet.

-- |[ ========================================= Combat ========================================= ]|
--This is just a simple list of combat frames.
ImageLump_Rip("Enemy|56", sBasePath .. "56_Doll_Combat.png", 0, 0, -1, -1, 0)

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the SLF file.
SLF_Close()
