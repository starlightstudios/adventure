-- |[ ===================================== Zeke Portraits ===================================== ]|
--Subscript for Zeke's portraits. Contains both combat and emote. Singular. Goats don't emote much.
local sBasePath = fnResolvePath()

-- |[File Creation]|
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_Zeke.slf")
else
	SLF_Open("Output/PortraitsLD_Zeke.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ======================================== Emotes ========================================== ]|
-- |[Autoloader Info]|
local sAutoloaderName = "AutoLoad|Adventure|Emote_Zeke"
local sAutoloaderPath = "Root/Images/Portraits/ZekeGoatDialogue/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[Automated Ripper Function]|
--Ripper designed for this character's layout. Notably, Zeke only has the one emote, and only one form,
-- so his form is not used as a prefix.
local function fnAutoRip(psBaseName, psFormName, psPath)
    fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Neutral", psPath, gciHDX0, gciHDY0, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Neutral")
end

-- |[General Purpose]|
fnAutoRip("Zeke", "Goat", sBasePath .. "Zeke_Goat_Emote.png")

-- |[Special Frames]|
--Put them here.

-- |[ ======================================== Combat ========================================== ]|
-- |[Autoloader Info]|
sAutoloaderName = "AutoLoad|Adventure|Combat_Zeke"
sAutoloaderPath = "Root/Images/Portraits/Combat/Zeke_"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[Combat Frames]|
--This is just a simple list of combat frames.
fnRipImageAuto("Party|Zeke_Goat", sBasePath .. "Zeke_Goat.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Goat")

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the SLF file.
SLF_Close()
