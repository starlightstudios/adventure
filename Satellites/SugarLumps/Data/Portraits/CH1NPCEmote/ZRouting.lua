-- |[ ===================================== NPC Portraits ====================================== ]|
--Subscript for NPCs portraits. Contains both combat and emotes.
local sBasePath = fnResolvePath()

-- |[File Creation]|
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_CH1Emote.slf")
else
	SLF_Open("Output/PortraitsLD_CH1Emote.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ========================================= Emotes ========================================= ]|
--Grid Enemies/NPCs
fnRipImageAutoCr("Por|Alraune|Neutral", sBasePath .. "Chapter1_NPC_Emote.png", gciHDX0, gciHDY0, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Alraune", "Root/Images/Portraits/Enemies/Alraune")
ImageLump_Rip("Por|CultistF|Neutral",   sBasePath .. "Chapter1_NPC_Emote.png", gciHDX1, gciHDY0, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|CultistM|Neutral",   sBasePath .. "Chapter1_NPC_Emote.png", gciHDX2, gciHDY0, gciWid0, gciHei0, 0)
fnRipImageAutoCr("Por|Mycela|Demush",   sBasePath .. "Chapter1_NPC_Emote.png", gciHDX3, gciHDY0, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Mycela", "Root/Images/Portraits/Mycela/Demush")
ImageLump_Rip("Por|Slime|Neutral",      sBasePath .. "Chapter1_NPC_Emote.png", gciHDX0, gciHDY1, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|SlimeG|Neutral",     sBasePath .. "Chapter1_NPC_Emote.png", gciHDX1, gciHDY1, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|SlimeB|Neutral",     sBasePath .. "Chapter1_NPC_Emote.png", gciHDX2, gciHDY1, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|SlimeI|Neutral",     sBasePath .. "Chapter1_NPC_Emote.png", gciHDX3, gciHDY1, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|Adina|Neutral",      sBasePath .. "Chapter1_NPC_Emote.png", gciHDX0, gciHDY2, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|Nadia|Neutral",      sBasePath .. "Chapter1_NPC_Emote.png", gciHDX1, gciHDY2, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|Rochea|Neutral",     sBasePath .. "Chapter1_NPC_Emote.png", gciHDX2, gciHDY2, gciWid0, gciHei0, 0)
fnRipImageAutoCr("Por|Meryl|Neutral",   sBasePath .. "Chapter1_NPC_Emote.png", gciHDX3, gciHDY2, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Meryl", "Root/Images/Portraits/Meryl/Neutral")
ImageLump_Rip("Por|Blythe|Neutral",     sBasePath .. "Chapter1_NPC_Emote.png", gciHDX0, gciHDY3, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|Breanne|Neutral",    sBasePath .. "Chapter1_NPC_Emote.png", gciHDX1, gciHDY3, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|Claudia|Neutral",    sBasePath .. "Chapter1_NPC_Emote.png", gciHDX2, gciHDY3, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|Karina|Neutral",     sBasePath .. "Chapter1_NPC_Emote.png", gciHDX3, gciHDY3, gciWid0, gciHei0, 0)
fnRipImageAutoCr("Por|Kona|Neutral",    sBasePath .. "Chapter1_NPC_Emote.png", gciHDX0, gciHDY4, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Kona",   "Root/Images/Portraits/Kona/Neutral")
fnRipImageAutoCr("Por|Kona|Happy",      sBasePath .. "Chapter1_NPC_Emote.png", gciHDX1, gciHDY4, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Kona",   "Root/Images/Portraits/Kona/Happy")
fnRipImageAutoCr("Por|Kona|Wink",       sBasePath .. "Chapter1_NPC_Emote.png", gciHDX2, gciHDY4, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Kona",   "Root/Images/Portraits/Kona/Wink")
fnRipImageAutoCr("Por|Denise|Neutral",  sBasePath .. "Chapter1_NPC_Emote.png", gciHDX0, gciHDY5, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Denise", "Root/Images/Portraits/Denise/Neutral")
fnRipImageAutoCr("Por|Denise|Smirk",    sBasePath .. "Chapter1_NPC_Emote.png", gciHDX1, gciHDY5, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Denise", "Root/Images/Portraits/Denise/Smirk")
fnRipImageAutoCr("Por|Denise|Insight",  sBasePath .. "Chapter1_NPC_Emote.png", gciHDX2, gciHDY5, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Denise", "Root/Images/Portraits/Denise/Insight")

--Wide-grid enemies
ImageLump_Rip("Por|Bee|Neutral",           sBasePath .. "Chapter1_NPC_EmoteWide.png", gciWideX0, gciWideY0, gciWideWid, gciWideHei, 0)
ImageLump_Rip("Por|MaidGhost|Neutral",     sBasePath .. "Chapter1_NPC_EmoteWide.png", gciWideX1, gciWideY0, gciWideWid, gciWideHei, 0)
ImageLump_Rip("Por|Werecat|Neutral",       sBasePath .. "Chapter1_NPC_EmoteWide.png", gciWideX2, gciWideY0, gciWideWid, gciWideHei, 0)
ImageLump_Rip("Por|Zombee|Neutral",        sBasePath .. "Chapter1_NPC_EmoteWide.png", gciWideX3, gciWideY0, gciWideWid, gciWideHei, 0)
fnRipImageAutoCr("Por|Mycela|Neutral",     sBasePath .. "Chapter1_NPC_EmoteWide.png", gciWideX0, gciWideY1, gciWideWid, gciWideHei, 0, "AutoLoad|Adventure|Emote_Mycela", "Root/Images/Portraits/Mycela/Neutral")
ImageLump_Rip("Por|WerecatThief|Neutral",  sBasePath .. "Chapter1_NPC_EmoteWide.png", gciWideX1, gciWideY1, gciWideWid, gciWideHei, 0)
ImageLump_Rip("Por|Mushraune|Neutral",     sBasePath .. "Chapter1_NPC_EmoteWide.png", gciWideX2, gciWideY1, gciWideWid, gciWideHei, 0)
ImageLump_Rip("Por|Laura|Neutral",         sBasePath .. "Chapter1_NPC_EmoteWide.png", gciWideX3, gciWideY1, gciWideWid, gciWideHei, 0)
ImageLump_Rip("Por|Lydie|Neutral",         sBasePath .. "Chapter1_NPC_EmoteWide.png", gciWideX0, gciWideY2, gciWideWid, gciWideHei, 0)
fnRipImageAutoCr("Por|BanditCatB|Neutral", sBasePath .. "Chapter1_NPC_EmoteWide.png", gciWideX1, gciWideY2, gciWideWid, gciWideHei, 0, "AutoLoad|Adventure|Emote_BanditCatB", "Root/Images/Portraits/Enemies/BanditCatB")

--Rubber NPC, Normal Grid
ImageLump_Rip("Por|CultistFRubber|Neutral", sBasePath .. "Chapter1_Rubber_Emote.png", gciHDX1, gciHDY0, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|AdinaRubber|Neutral",    sBasePath .. "Chapter1_Rubber_Emote.png", gciHDX0, gciHDY2, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|NadiaRubber|Neutral",    sBasePath .. "Chapter1_Rubber_Emote.png", gciHDX1, gciHDY2, gciWid0, gciHei0, 0)
ImageLump_Rip("Por|RocheaRubber|Neutral",   sBasePath .. "Chapter1_Rubber_Emote.png", gciHDX2, gciHDY2, gciWid0, gciHei0, 0)

--Rubber, Wide.
ImageLump_Rip("Por|BeeRubber|Neutral",          sBasePath .. "Chapter1_NPC_EmoteWideRubber.png", gciWideX0, gciWideY0, gciWideWid, gciWideHei, 0)
ImageLump_Rip("Por|WerecatThiefRubber|Neutral", sBasePath .. "Chapter1_NPC_EmoteWideRubber.png", gciWideX1, gciWideY0, gciWideWid, gciWideHei, 0)
ImageLump_Rip("Por|WerecatRubber|Neutral",      sBasePath .. "Chapter1_NPC_EmoteWideRubber.png", gciWideX2, gciWideY0, gciWideWid, gciWideHei, 0)
ImageLump_Rip("Por|AlrauneRubber|Neutral",      sBasePath .. "Chapter1_NPC_EmoteWideRubber.png", gciWideX3, gciWideY0, gciWideWid, gciWideHei, 0)

--Victoria.
fnRipImageAutoCr("Por|Victoria|Neutral",   sBasePath .. "Victoria.png", gciHDX0, gciHDY0, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Victoria", "Root/Images/Portraits/Victoria/Neutral")
fnRipImageAutoCr("Por|Victoria|Apology",   sBasePath .. "Victoria.png", gciHDX1, gciHDY0, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Victoria", "Root/Images/Portraits/Victoria/Apology")
fnRipImageAutoCr("Por|Victoria|Evil",      sBasePath .. "Victoria.png", gciHDX2, gciHDY0, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Victoria", "Root/Images/Portraits/Victoria/Evil")
fnRipImageAutoCr("Por|Victoria|Disgust",   sBasePath .. "Victoria.png", gciHDX3, gciHDY0, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Victoria", "Root/Images/Portraits/Victoria/Disgust")
fnRipImageAutoCr("Por|Victoria|Concern",   sBasePath .. "Victoria.png", gciHDX0, gciHDY1, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Victoria", "Root/Images/Portraits/Victoria/Concern")
fnRipImageAutoCr("Por|Victoria|Disbelief", sBasePath .. "Victoria.png", gciHDX1, gciHDY1, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Victoria", "Root/Images/Portraits/Victoria/Disbelief")
fnRipImageAutoCr("Por|Victoria|Mannequin", sBasePath .. "Victoria.png", gciHDX3, gciHDY1, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Victoria", "Root/Images/Portraits/Victoria/Mannequin")

--Xanna.
fnRipImageAutoCr("Por|Xanna|Neutral",    sBasePath .. "Xanna.png", gciHDX0, gciHDY0, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Xanna",  "Root/Images/Portraits/Xanna/Neutral")
fnRipImageAutoCr("Por|Xanna|Upset",      sBasePath .. "Xanna.png", gciHDX1, gciHDY0, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Xanna",  "Root/Images/Portraits/Xanna/Upset")
fnRipImageAutoCr("Por|XannaBee|Neutral", sBasePath .. "Xanna.png", gciHDX0, gciHDY1, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Xanna",  "Root/Images/Portraits/Xanna/BeeNeutral")
fnRipImageAutoCr("Por|XannaBee|Upset",   sBasePath .. "Xanna.png", gciHDX1, gciHDY1, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Xanna",  "Root/Images/Portraits/Xanna/BeeUpset")

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the SLF file.
SLF_Close()
