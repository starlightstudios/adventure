-- |[ ================================== Cassandra Portraits =================================== ]|
--Subscript for Cassandra's portraits. Contains both combat and emotes.
local sBasePath = fnResolvePath()

-- |[File Creation]|
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_Cassandra.slf")
else
	SLF_Open("Output/PortraitsLD_Cassandra.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ========================================= Emotes ========================================= ]|
-- |[General Purpose]|
fnRipImageAutoCr("Por|Cassandra|Neutral", sBasePath .. "Cassandra_All_Emote.png", gciHDX0, gciHDY0, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Cassandra", "Root/Images/Portraits/Cassandra/Neutral")
fnRipImageAutoCr("Por|Cassandra|Werecat", sBasePath .. "Cassandra_All_Emote.png", gciHDX1, gciHDY0, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Cassandra", "Root/Images/Portraits/Cassandra/Werecat")
fnRipImageAutoCr("Por|Cassandra|Golem",   sBasePath .. "Cassandra_All_Emote.png", gciHDX2, gciHDY0, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Cassandra", "Root/Images/Portraits/Cassandra/Golem")
fnRipImageAutoCr("Por|Cassandra|Raptor",  sBasePath .. "Cassandra_All_Emote.png", gciHDX3, gciHDY0, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_Cassandra", "Root/Images/Portraits/Cassandra/Raptor")

-- |[Special Frames]|
--None yet.

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the SLF file.
SLF_Close()
