-- |[ ==================================== Empress Portraits =================================== ]|
--Subscript for Empress's portraits. Contains both combat and emotes.
local sBasePath = fnResolvePath()

-- |[File Creation]|
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_Empress.slf")
else
	SLF_Open("Output/PortraitsLD_Empress.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ========================================= Emotes ========================================= ]|
-- |[Autoloader Info]|
local sAutoloaderName = "AutoLoad|Adventure|Emote_Empress_Conquerer"
local sAutoloaderPath = "Root/Images/Portraits/EmpressConquererDialogue/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[Automated Ripper Function]|
--Ripper designed for this character's emote pattern.
local function fnAutoRip(psBaseName, psFormName, psPath, pbOnlyNeutralSmirk)
    
    --Switch based on emotion set.
    if(pbOnlyNeutralSmirk == false) then
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Neutral", psPath, gciHDX0, gciHDY0, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Neutral")
        --[=[
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Happy",   psPath, gciHDX1, gciHDY0, gciWid0, gciHei0, 0)
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Blush",   psPath, gciHDX2, gciHDY0, gciWid0, gciHei0, 0)
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Smirk",   psPath, gciHDX3, gciHDY0, gciWid0, gciHei0, 0)
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Sad",     psPath, gciHDX0, gciHDY1, gciWid0, gciHei0, 0)
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Scared",  psPath, gciHDX1, gciHDY1, gciWid0, gciHei0, 0)
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Offended",psPath, gciHDX2, gciHDY1, gciWid0, gciHei0, 0)
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Cry",     psPath, gciHDX0, gciHDY2, gciWid0, gciHei0, 0)
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Laugh",   psPath, gciHDX1, gciHDY2, gciWid0, gciHei0, 0)
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Angry",   psPath, gciHDX2, gciHDY2, gciWid0, gciHei0, 0)
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|PDU",     psPath, gciHDX3, gciHDY2, gciWid0, gciHei0, 0)
    ]=]
    
    --Only neutral and smirk emotions:
    else
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Neutral", psPath, gciHDX0, gciHDY0, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Neutral")
    end
end

-- |[General Purpose]|
fnAutoRip("Empress", "Conquerer", sBasePath .. "Empress_Conquerer_Emote.png",         false)

-- |[Special Frames]|
--Add them here.

-- |[ ========================================= Combat ========================================= ]|
-- |[Autoloader Info]|
sAutoloaderName = "AutoLoad|Adventure|Combat_Empress"
sAutoloaderPath = "Root/Images/Portraits/Combat/Empress_"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[Combat Frames]|
--This is just a simple list of combat frames.
fnRipImageAuto("Party|Empress_Conquerer", sBasePath .. "Empress_Conquerer_Combat.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Conquerer")

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the SLF file.
SLF_Close()
