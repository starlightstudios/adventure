-- |[ ===================================== NPC Portraits ====================================== ]|
--Subscript for NPCs portraits. Contains both combat and emotes.
local sBasePath = fnResolvePath()

-- |[File Creation]|
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_CH0Emote.slf")
else
	SLF_Open("Output/PortraitsLD_CH0Emote.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ======================================== Emotes ========================================== ]|
--Each character gets their own autoloader lump.
fnRipImageAutoCr("Por|CrowbarChan|Neutral",    sBasePath .. "Chapter0_NPC_Emote.png", gciHDX0, gciHDY0, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_CrowbarChan",    "Root/Images/Portraits/CrowbarChanMob/Neutral")
fnRipImageAutoCr("Por|CrowbarChanMob|Neutral", sBasePath .. "Chapter0_NPC_Emote.png", gciHDX1, gciHDY0, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_CrowbarChanMob", "Root/Images/Portraits/CrowbarChanMob/Neutral")
fnRipImageAutoCr("Por|MercF|Neutral",          sBasePath .. "Chapter0_NPC_Emote.png", gciHDX0, gciHDY1, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_MercF",          "Root/Images/Portraits/NPCs/MercF")
fnRipImageAutoCr("Por|MercM|Neutral",          sBasePath .. "Chapter0_NPC_Emote.png", gciHDX1, gciHDY1, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_MercM",          "Root/Images/Portraits/NPCs/MercM")
fnRipImageAutoCr("Por|HumanNPCF0|Neutral",     sBasePath .. "Chapter0_NPC_Emote.png", gciHDX2, gciHDY1, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_HumanF0",        "Root/Images/Portraits/NPCs/HumanNPCF0")
fnRipImageAutoCr("Por|HumanNPCF1|Neutral",     sBasePath .. "Chapter0_NPC_Emote.png", gciHDX3, gciHDY1, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_HumanF1",        "Root/Images/Portraits/NPCs/HumanNPCF1")
fnRipImageAutoCr("Por|HumanNPCF0B|Neutral",    sBasePath .. "Chapter0_NPC_Emote.png", gciHDX0, gciHDY2, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_HumanF0_B",      "Root/Images/Portraits/NPCs/HumanNPCF0B")
fnRipImageAutoCr("Por|HumanNPCF1B|Neutral",    sBasePath .. "Chapter0_NPC_Emote.png", gciHDX1, gciHDY2, gciWid0, gciHei0, 0, "AutoLoad|Adventure|Emote_HumanF1_B",      "Root/Images/Portraits/NPCs/HumanNPCF1B")

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the SLF file.
SLF_Close()
