-- |[ ===================================== Odar Portraits ===================================== ]|
--Subscript for Odar's portraits. Contains both combat and emotes.
local sBasePath = fnResolvePath()

-- |[File Creation]|
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_Odar.slf")
else
	SLF_Open("Output/PortraitsLD_Odar.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ======================================== Emotes ========================================== ]|
-- |[Autoloader Info]|
local sAutoloaderName = "AutoLoad|Adventure|Emote_Odar_Prince"
local sAutoloaderPath = "Root/Images/Portraits/OdarPrinceDialogue/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[Automated Ripper Function]|
--Ripper designed for this character's emote pattern.
local function fnAutoRip(psBaseName, psFormName, psPath, pbOnlyNeutralSmirk)
    
    --Switch based on emotion set.
    if(pbOnlyNeutralSmirk == false) then
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Neutral", psPath, gciHDX0, gciHDY0, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Neutral")
    
    --Only neutral and smirk emotions:
    else
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Neutral", psPath, gciHDX0, gciHDY0, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Neutral")
    end
end

-- |[General Purpose]|
fnAutoRip("Odar", "Prince", sBasePath .. "Odar_Prince_Emote.png",   false)

-- |[Special Frames]|
--Put them here.

-- |[ ======================================== Combat ========================================== ]|
-- |[Autoloader Info]|
sAutoloaderName = "AutoLoad|Adventure|Combat_Odar"
sAutoloaderPath = "Root/Images/Portraits/Combat/Odar_"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[Combat Frames]|
--This is just a simple list of combat frames.
fnRipImageAuto("Party|Odar_Prince", sBasePath .. "Odar_Prince.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Prince")

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the SLF file.
SLF_Close()
