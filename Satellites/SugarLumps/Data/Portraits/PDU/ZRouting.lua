-- |[ ===================================== PDU Portraits ====================================== ]|
--Yes, the PDU gets his own portraits. You go, little guy!
local sBasePath = fnResolvePath()

-- |[File Creation]|
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_PDU.slf")
else
	SLF_Open("Output/PortraitsLD_PDU.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ========================================= Emotes ========================================= ]|
-- |[Automated Ripper Function]|
--Ripper designed for Florentina's emote pattern.
local fnAutoRip = function(sBaseName, sFormName, sPath)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Neutral",  sPath, gciHDX0, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Happy",    sPath, gciHDX1, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Alarmed",  sPath, gciHDX2, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Question", sPath, gciHDX3, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Quiet",    sPath, gciHDX0, gciHDY1, gciWid0, gciHei0, 0)
end

-- |[General Purpose]|
fnAutoRip("PDU", "Robot", sBasePath .. "PDU_Robot_Emote.png")

-- |[Special Frames]|
--None yet.

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the SLF file.
SLF_Close()
