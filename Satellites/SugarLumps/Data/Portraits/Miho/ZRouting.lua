--[ ======================================= Miho Portraits ====================================== ]
--Miho, the hardass kitsune bounty hunter.
local sBasePath = fnResolvePath()

--[File Creation]
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_Miho.slf")
else
	SLF_Open("Output/PortraitsLD_Miho.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

--[ ========================================== Emotes =========================================== ]
--[Automated Ripper Function]
--Ripper designed for the character's emote pattern.
local fnAutoRip = function(sBaseName, sFormName, sPath)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Neutral",   sPath, gciHDX0, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Angry",     sPath, gciHDX1, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Drunk",     sPath, gciHDX2, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|VeryDrunk", sPath, gciHDX3, gciHDY0, gciWid0, gciHei0, 0)
end

--[General Purpose]
fnAutoRip("Miho", "Kitsune", sBasePath .. "Miho_Emote.png")

--[Special Frames]
ImageLump_Rip("Por|MihoKitsune|Rubber", sBasePath .. "Miho_Rubber.png", gciHDX0, gciHDY0, gciWid0, gciHei0, 0)

--[ ========================================== Combat =========================================== ]
--This is just a simple list of combat frames.

--[ ========================================= Finish Up ========================================= ]
--Close and write the SLF file.
SLF_Close()
