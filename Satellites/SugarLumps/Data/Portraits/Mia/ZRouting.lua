-- |[ ===================================== Mia Portraits ====================================== ]|
--Mia, Izuna's editor.
local sBasePath = fnResolvePath()

-- |[File Creation]|
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_Mia.slf")
else
	SLF_Open("Output/PortraitsLD_Mia.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[Autoloader]|
sAutoloaderName = "AutoLoad|Adventure|Emote_Mia"
sAutoloaderPath = "Root/Images/Portraits/Mia/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[ ========================================= Emotes ========================================= ]|
--Just neutral.
fnRipImageAuto("Por|MiaKitsune|Neutral", sBasePath .. "Mia_Emote.png", gciHDX0, gciHDY0, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Neutral")

-- |[ ========================================= Combat ========================================= ]|
--This is just a simple list of combat frames.

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the SLF file.
SLF_Close()
