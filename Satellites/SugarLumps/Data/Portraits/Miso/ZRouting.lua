-- |[ ===================================== Miso Portraits ===================================== ]|
--Miso, the master craftsfox.
local sBasePath = fnResolvePath()

-- |[File Creation]|
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_Miso.slf")
else
	SLF_Open("Output/PortraitsLD_Miso.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[Autoloader]|
sAutoloaderName = "AutoLoad|Adventure|Emote_Miso"
sAutoloaderPath = "Root/Images/Portraits/Miso/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[ ========================================= Emotes ========================================= ]|
--Just neutral.
fnRipImageAuto("Por|MisoKitsune|Neutral", sBasePath .. "Miso_Emote.png", gciHDX0, gciHDY0, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Neutral")

-- |[ ========================================= Combat ========================================= ]|
--This is just a simple list of combat frames.

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the SLF file.
SLF_Close()
