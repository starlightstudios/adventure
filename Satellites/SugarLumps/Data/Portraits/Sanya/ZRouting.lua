-- |[ ==================================== Sanya Portraits ===================================== ]|
--Subscript for Sanya's portraits. Contains both combat and emotes.
local sBasePath = fnResolvePath()

-- |[File Creation]|
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_Sanya.slf")
	ImageLump_SetScales(1.00, 1.00, 0, 0)
else
	SLF_Open("Output/PortraitsLD_Sanya.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ======================================== Emotes ========================================== ]|
-- |[Automated Ripper Function]|
--Ripper designed for this character's emote pattern.
local function fnAutoRip(psBaseName, psFormName, psPath, pbOnlyNeutralSmirk)
    
    --Autoloader.
    local sAutoloaderName = "AutoLoad|Adventure|Emote_Sanya_" .. psFormName
    local sAutoloaderPath = "Root/Images/Portraits/SanyaDialogue/" .. psFormName
    SLF_RegisterAutoLoadLump(sAutoloaderName)
    
    --Switch based on emotion set.
    if(pbOnlyNeutralSmirk == false) then
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Neutral",   psPath, gciHDX0, gciHDY0, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Neutral")
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Happy",     psPath, gciHDX1, gciHDY0, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Happy")
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Blush",     psPath, gciHDX2, gciHDY0, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Blush")
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Smirk",     psPath, gciHDX3, gciHDY0, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Smirk")
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Sad",       psPath, gciHDX0, gciHDY1, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Sad")
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Surprised", psPath, gciHDX1, gciHDY1, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Surprised")
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Offended",  psPath, gciHDX2, gciHDY1, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Offended")
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Cry",       psPath, gciHDX0, gciHDY2, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Cry")
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Laugh",     psPath, gciHDX1, gciHDY2, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Laugh")
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Angry",     psPath, gciHDX2, gciHDY2, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Angry")
    
    --Only neutral and smirk emotions:
    else
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Neutral", psPath, gciHDX0, gciHDY0, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Neutral")
        fnRipImageAuto("Por|" .. psBaseName .. psFormName .. "|Smirk",   psPath, gciHDX3, gciHDY0, gciWid0, gciHei0, 0, sAutoloaderName, sAutoloaderPath .. "Smirk")
    end
end

-- |[General Purpose]|
fnAutoRip("Sanya", "Bunny",         sBasePath .. "Sanya_Bunny_Emote.png",          false)
fnAutoRip("Sanya", "Harpy",         sBasePath .. "Sanya_Harpy_Emote.png",          false)
fnAutoRip("Sanya", "Human",         sBasePath .. "Sanya_Human_Emote.png",          false)
fnAutoRip("Sanya", "HumanNoRifle",  sBasePath .. "Sanya_Human_Emote_NoRifle.png",  false)
fnAutoRip("Sanya", "HumanWerebatA", sBasePath .. "Sanya_Human_Emote_WerebatA.png", false)
fnAutoRip("Sanya", "HumanWerebatB", sBasePath .. "Sanya_Human_Emote_WerebatB.png", false)
fnAutoRip("Sanya", "Kitsune",       sBasePath .. "Sanya_Kitsune_Emote.png",        false)
fnAutoRip("Sanya", "Sevavi",        sBasePath .. "Sanya_Sevavi_Emote.png",         false)
fnAutoRip("Sanya", "SevaviNoRifle", sBasePath .. "Sanya_SevaviNoRifle_Emote.png",  false)
fnAutoRip("Sanya", "Werebat",       sBasePath .. "Sanya_Werebat_Emote.png",        false)

-- |[Special Frames]|
--Put them here.

-- |[ ======================================== Combat ========================================== ]|
-- |[Autoloader Info]|
sAutoloaderName = "AutoLoad|Adventure|Combat_Sanya_"
sAutoloaderPath = "Root/Images/Portraits/Combat/Sanya_"

-- |[Combat Frames]|
--This is just a simple list of combat frames.
fnRipImageAutoCr("Party|Sanya_Bunny",         sBasePath .. "Sanya_Bunny.png",          0, 0, -1, -1, 0, sAutoloaderName .. "Bunny",         sAutoloaderPath .. "Bunny")
fnRipImageAutoCr("Party|Sanya_Harpy",         sBasePath .. "Sanya_Harpy.png",          0, 0, -1, -1, 0, sAutoloaderName .. "Harpy",         sAutoloaderPath .. "Harpy")
fnRipImageAutoCr("Party|Sanya_Human",         sBasePath .. "Sanya_Human.png",          0, 0, -1, -1, 0, sAutoloaderName .. "Human",         sAutoloaderPath .. "Human")
fnRipImageAutoCr("Party|Sanya_HumanNoRifle",  sBasePath .. "Sanya_Human_NoRifle.png",  0, 0, -1, -1, 0, sAutoloaderName .. "HumanNoRifle",  sAutoloaderPath .. "HumanNoRifle")
fnRipImageAutoCr("Party|Sanya_Kitsune",       sBasePath .. "Sanya_Kitsune.png",        0, 0, -1, -1, 0, sAutoloaderName .. "Kitsune",       sAutoloaderPath .. "Kitsune")
fnRipImageAutoCr("Party|Sanya_Sevavi",        sBasePath .. "Sanya_Sevavi.png",         0, 0, -1, -1, 0, sAutoloaderName .. "Sevavi",        sAutoloaderPath .. "Sevavi")
fnRipImageAutoCr("Party|Sanya_SevaviNoRifle", sBasePath .. "Sanya_Sevavi_NoRifle.png", 0, 0, -1, -1, 0, sAutoloaderName .. "SevaviNoRifle", sAutoloaderPath .. "SevaviNoRifle")
fnRipImageAutoCr("Party|Sanya_Werebat",       sBasePath .. "Sanya_Werebat.png",        0, 0, -1, -1, 0, sAutoloaderName .. "Werebat",       sAutoloaderPath .. "Werebat")

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the SLF file.
SLF_Close()
