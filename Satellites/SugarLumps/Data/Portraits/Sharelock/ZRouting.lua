--[ ===================================== Share;lock Portraits ===================================== ]
--Share;lock, the not at all confused robot detective!
local sBasePath = fnResolvePath()

--[File Creation]
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_Sharelock.slf")
else
	SLF_Open("Output/PortraitsLD_Sharelock.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

--[ ========================================== Emotes =========================================== ]
--[Automated Ripper Function]
--Ripper designed for the character's emote pattern.
local fnAutoRip = function(sBaseName, sFormName, sPath)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Neutral",  sPath, gciHDX0, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Thinking", sPath, gciHDX1, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|AhHa",     sPath, gciHDX2, gciHDY0, gciWid0, gciHei0, 0)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Sheepish", sPath, gciHDX3, gciHDY0, gciWid0, gciHei0, 0)
end

--[General Purpose]
fnAutoRip("Sharelock", "Steamdroid", sBasePath .. "Sharelock_Emote.png")

--[Special Frames]
--None yet.

--[ ========================================== Combat =========================================== ]
--This is just a simple list of combat frames.

--[ ========================================= Finish Up ========================================= ]
--Close and write the SLF file.
SLF_Close()
