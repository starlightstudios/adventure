-- |[ =================================== Septima Portraits ==================================== ]|
--Septima's emotes use special sizing in order to fit her parasol.
local sBasePath = fnResolvePath()
local ciSeptimaSpecialHei = 812

-- |[File Creation]|
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_Septima.slf")
else
	SLF_Open("Output/PortraitsLD_Septima.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ========================================= Emotes ========================================= ]|
-- |[Automated Ripper Function]|
--Ripper designed for Sammy's emote pattern.
local fnAutoRip = function(sBaseName, sFormName, sPath)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Neutral",   sPath, gciHDX0, gciHDY0, gciWid0, ciSeptimaSpecialHei, gciNoTransparencies)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|NeutralUp", sPath, gciHDX1, gciHDY0, gciWid0, ciSeptimaSpecialHei, gciNoTransparencies)
end

-- |[General Purpose]|
fnAutoRip("Septima", "", sBasePath .. "Septima_Rilmani_Emote.png")

-- |[Special Frames]|
--None yet.

-- |[ ========================================= Combat ========================================= ]|
--This is just a simple list of combat frames.

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the SLF file.
SLF_Close()
