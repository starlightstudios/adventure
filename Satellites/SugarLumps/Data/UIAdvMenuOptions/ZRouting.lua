-- |[ ======================================= Options UI ======================================= ]|
--Options UI. Get item listings and descriptions.
local sBasePath = fnResolvePath()

-- |[Setup]|
SLF_Open("Output/UIAdvMenuOptions.slf")
ImageLump_SetCompression(1)

--Autoloader Variables
local sAutoloaderName = "AutoLoad|Adventure|AdvMenuOptions"
local sDLPath = "Root/Images/AdventureUI/Options/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[ ======================================== Ripping ========================================= ]|
-- |[Lists]|
local sPrefix = "AdvOptions|"
local saNames = {"Arrow Lft", "Arrow Rgt", "Button Defaults", "Button Okay", "Description Frame", "Expandable Header", "Frame Detail", "Frame", "Header", "Option Header", "Slider Indicator", "Slider", "Unsaved Changes Frame"}
local saPaths = saNames

--Ripping loop.
for i = 1, #saNames, 1 do
	fnRipImageAuto(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. saNames[i])
end

-- |[ ======================================== Clean Up ======================================== ]|
--Finish
SLF_Close()


