-- |[ ===================================== Puzzle Battle ====================================== ]|
--Contains both UI and field elements for the puzzle battle sequences in chapter 2.
local sBasePath = fnResolvePath()

-- |[Setup]|
SLF_Open("Output/PuzzleBattle.slf")
ImageLump_SetCompression(1)

-- |[ ===================================== Field Objects ====================================== ]|
--These elements appear in the field, translated to world size. This includes the highlight cursor
-- which renders in the world, not on the UI layer.

-- |[Lists]|
local sPrefix = "Field|"
local saNames = {"Highlight", "World Arrow Down", "World Arrow Lft", "World Arrow Rgt", "World Arrow Up", "World Boost", "World Guard", "World Melee", "World Ranged", "World Power Up", "World Perfect Guard"}
local saPaths = saNames

--Ripping loop.
local i = 1
while(saNames[i] ~= nil) do
	ImageLump_Rip(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0)
	i = i + 1
end

-- |[ ======================================= GUI Objects ====================================== ]|
--These are UI elements appearing over the world.

-- |[Lists]|
sPrefix = "UI|"
saNames = {"Health Fill Lft", "Health Fill Rgt", "Health Frame Lft", "Health Frame Rgt", "Moves Box", "Time Header", "Confirm Box", "Text Bottom", "Text Top"}
saPaths = saNames

--Ripping loop.
i = 1
while(saNames[i] ~= nil) do
	ImageLump_Rip(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0)
	i = i + 1
end

-- |[ ======================================== Clean Up ======================================== ]|
--Finish
SLF_Close()


