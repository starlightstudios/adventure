-- |[ ===================================== Map Animations ===================================== ]|
--Temporary animations used by specific maps. They get unloaded when the map unloads.
local sBasePath = fnResolvePath()

--Setup
SLF_Open("Output/MapAnimations.slf")
ImageLump_SetCompression(1)

-- |[ ======= Variable Setup ======= ]|
-- |[Paths]|
local sInfileName      = ""   --Name the animation has inside the SLF file.
local sFolderPath      = ""   --Folder to look inside for the images.
local sSheetName       = ""   --Name of the images inside the folder. Must be in numeric sequence, starting at 0.
local iSizeX           = 1    --Size, in pixels, of each animation frame.
local iaFramesPerSheet = {}   --List of integers specifying how many frames to rip on each sheet.
local iFrameCounter    = 0    --Counts how many frames are ripped so far, used to give each frame a unique name.

-- |[ ===== Ripping Sequences ====== ]|
-- |[Inspection]|
--Occurs between Latex Christine and Sophie in the repair bay. Each sheet has 20 frames.
sInfileName = "Inspection"
sFolderPath = "Inspection"
sSheetName  = "Inspection"
iSizeX      = 351
iaFramesPerSheet = {20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20}
iFrameCounter = 0

--Rip.
for i = 1, #iaFramesPerSheet, 1 do

    --Setup.
    local sLoadPath = string.format(sBasePath .. sFolderPath .. "/" .. sSheetName .. "%i.png", i-1)
    
    --Iteration of internal frames.
    for p = 1, iaFramesPerSheet[i], 1 do
        ImageLump_Rip(string.format(sInfileName .. "|%03i", iFrameCounter), sLoadPath, (p-1) * iSizeX, 0, iSizeX, -1, 0)
        iFrameCounter = iFrameCounter + 1
    end
end

-- |[Christine to Golem]|
--Human Christine becomes Golem Christine.
sInfileName = "ChristineGolem"
sFolderPath = "ChristineGolem"
sSheetName  = "ChristineGolem"
iSizeX      = 348
iaFramesPerSheet = {20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 40}
iFrameCounter = 0
for i = 1, #iaFramesPerSheet, 1 do

    --Setup.
    local sLoadPath = string.format(sBasePath .. sFolderPath .. "/" .. sSheetName .. "%02i.png", i-1)
    
    --Iteration of internal frames.
    for p = 1, iaFramesPerSheet[i], 1 do
        ImageLump_Rip(string.format(sInfileName .. "|%03i", iFrameCounter), sLoadPath, (p-1) * iSizeX, 0, iSizeX, -1, 0)
        iFrameCounter = iFrameCounter + 1
    end
end

-- |[Miho Dollified]|
--Miho is transformed into a porcelain doll.
sInfileName = "MihoDollify"
sFolderPath = "MihoDollify"
sSheetName  = "Segment"
iSizeX      = 326
iaFramesPerSheet = {30, 30, 30, 30, 30, 30, 30, 9}
iFrameCounter = 0 

--Iteration.
for i = 1, #iaFramesPerSheet, 1 do
    
    --Setup.
    local sLoadPath = string.format(sBasePath .. sFolderPath .. "/" .. sSheetName .. "%i.png", i-1)
    
    --Iteration of internal frames.
    for p = 1, iaFramesPerSheet[i], 1 do
        ImageLump_Rip(string.format(sInfileName .. "|%03i", iFrameCounter), sLoadPath, (p-1) * iSizeX, 0, iSizeX, -1, 0)
        iFrameCounter = iFrameCounter + 1
    end
end

--Finish
SLF_Close()