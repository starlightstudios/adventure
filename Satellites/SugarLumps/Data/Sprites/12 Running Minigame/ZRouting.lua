-- |[ ==================================== Running Minigame ==================================== ]|
--A minigame in which the player's party runs from a horde of angry hivemind robots.
local sBasePath = fnResolvePath()

-- |[Ripping]|
ImageLump_SetBlockTrimmingFlag(true)
ImageLump_Rip("Run|All",       sBasePath .. "RunGraphic.png",  0,   0, -1, -1, 0)
ImageLump_SetBlockTrimmingFlag(false)
ImageLump_Rip("Run|Tutorial0", sBasePath .. "Tutorial0.png",   0,   0, -1, -1, 0)
ImageLump_Rip("Run|Tutorial1", sBasePath .. "Tutorial1.png",   0,   0, -1, -1, 0)
ImageLump_Rip("Run|Tutorial2", sBasePath .. "Tutorial2.png",   0,   0, -1, -1, 0)
ImageLump_Rip("Run|Tutorial3", sBasePath .. "Tutorial3.png",   0,   0, -1, -1, 0)
ImageLump_Rip("Run|Tutorial4", sBasePath .. "Tutorial4.png",   0,   0, -1, -1, 0)
ImageLump_Rip("Run|Tutorial5", sBasePath .. "Tutorial5.png",   0,   0, -1, -1, 0)