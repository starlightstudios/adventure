-- |[ ======================================= Mei Sprites ====================================== ]|
--Mei's sprite sheets. Assumes the sprite SLF file is already open.
local sBasePath = fnResolvePath()
local sAutoRoot = "Root/Images/Sprites/"

-- |[ ===================== Standard Frames ====================== ]|
fnRipRunebearer("Mei_Alraune",     sBasePath .. "Mei_Alraune.png",     "MeiAlraune",     "AutoLoad|Adventure|SpritesMei|Alraune",     sAutoRoot)
fnRipRunebearer("Mei_Alraune_MC",  sBasePath .. "Mei_Alraune_MC.png",  "MeiAlrauneMC",   "AutoLoad|Adventure|SpritesMei|AlrauneMC",   sAutoRoot)
fnRipRunebearer("Mei_Bee",         sBasePath .. "Mei_Bee.png",         "MeiBee",         "AutoLoad|Adventure|SpritesMei|Bee",         sAutoRoot)
fnRipRunebearer("Mei_BeeQueen",    sBasePath .. "Mei_BeeQueen.png",    "MeiBeeQueen",    "AutoLoad|Adventure|SpritesMei|BeeQueen",    sAutoRoot)
fnRipRunebearer("Mei_Bee_MC",      sBasePath .. "Mei_Bee_MC.png",      "MeiBeeMC",       "AutoLoad|Adventure|SpritesMei|BeeMC",       sAutoRoot)
fnRipRunebearer("Mei_Ghost",       sBasePath .. "Mei_Ghost.png",       "MeiGhost",       "AutoLoad|Adventure|SpritesMei|Ghost",       sAutoRoot)
fnRipRunebearer("Mei_Gravemarker", sBasePath .. "Mei_Gravemarker.png", "MeiGravemarker", "AutoLoad|Adventure|SpritesMei|Gravemarker", sAutoRoot)
fnRipRunebearer("Mei_Human",       sBasePath .. "Mei_Human.png",       "Mei",            "AutoLoad|Adventure|SpritesMei|Human",       sAutoRoot)
fnRipRunebearer("Mei_Human_MC",    sBasePath .. "Mei_Human_MC.png",    "MeiMC",          "AutoLoad|Adventure|SpritesMei|MC",          sAutoRoot)
fnRipRunebearer("Mei_Mannequin",   sBasePath .. "Mei_Mannequin.png",   "MeiMannequin",   "AutoLoad|Adventure|SpritesMei|Mannequin",   sAutoRoot)
fnRipRunebearer("Mei_Rubber",      sBasePath .. "Mei_Rubber.png",      "MeiRubber",      "AutoLoad|Adventure|SpritesMei|Rubber",      sAutoRoot)
fnRipRunebearer("Mei_RubberQueen", sBasePath .. "Mei_RubberQueen.png", "MeiRubberQueen", "AutoLoad|Adventure|SpritesMei|RubberQueen", sAutoRoot)
fnRipRunebearer("Mei_Slime",       sBasePath .. "Mei_Slime.png",       "MeiSlime",       "AutoLoad|Adventure|SpritesMei|Slime",       sAutoRoot)
fnRipRunebearer("Mei_SlimeBlue",   sBasePath .. "Mei_SlimeBlue.png",   "MeiSlimeBlue",   "AutoLoad|Adventure|SpritesMei|SlimeBlue",   sAutoRoot)
fnRipRunebearer("Mei_SlimeInk",    sBasePath .. "Mei_SlimeInk.png",    "MeiSlimeInk",    "AutoLoad|Adventure|SpritesMei|SlimeInk",    sAutoRoot)
fnRipRunebearer("Mei_SlimePink",   sBasePath .. "Mei_SlimePink.png",   "MeiSlimePink",   "AutoLoad|Adventure|SpritesMei|SlimePink",   sAutoRoot)
fnRipRunebearer("Mei_SlimeYellow", sBasePath .. "Mei_SlimeYellow.png", "MeiSlimeYellow", "AutoLoad|Adventure|SpritesMei|SlimeYellow", sAutoRoot)
fnRipRunebearer("Mei_Werecat",     sBasePath .. "Mei_Werecat.png",     "MeiWerecat",     "AutoLoad|Adventure|SpritesMei|Werecat",     sAutoRoot)
fnRipRunebearer("Mei_Wisphag",     sBasePath .. "Mei_Wisphag.png",     "MeiWisphag",     "AutoLoad|Adventure|SpritesMei|Wisphag",     sAutoRoot)
fnRipRunebearer("Mei_ZombeeQueen", sBasePath .. "Mei_ZombeeQueen.png", "MeiZombeeQueen", "AutoLoad|Adventure|SpritesMei|ZombeeQueen", sAutoRoot)

-- |[ ====================== Idle Animations ===================== ]|
fnRipIdleAnimation("Mei_Alraune",     sBasePath .. "Mei_Alraune.png",     0, 400, 10, "AutoLoad|Adventure|SpritesMei|Alraune",     sAutoRoot)
fnRipIdleAnimation("Mei_Bee_MC",      sBasePath .. "Mei_Bee_MC.png",      0, 400,  4, "AutoLoad|Adventure|SpritesMei|BeeMC",       sAutoRoot)
fnRipIdleAnimation("Mei_Bee",         sBasePath .. "Mei_Bee.png",         0, 400,  5, "AutoLoad|Adventure|SpritesMei|Bee",         sAutoRoot)
fnRipIdleAnimation("Mei_BeeQueen",    sBasePath .. "Mei_BeeQueen.png",    0, 400,  8, "AutoLoad|Adventure|SpritesMei|BeeQueen",    sAutoRoot)
fnRipIdleAnimation("Mei_Ghost",       sBasePath .. "Mei_Ghost.png",       0, 400,  8, "AutoLoad|Adventure|SpritesMei|Ghost",       sAutoRoot)
fnRipIdleAnimation("Mei_Gravemarker", sBasePath .. "Mei_Gravemarker.png", 0, 400,  4, "AutoLoad|Adventure|SpritesMei|Gravemarker", sAutoRoot)
fnRipIdleAnimation("Mei_Human",       sBasePath .. "Mei_Human.png",       0, 400, 11, "AutoLoad|Adventure|SpritesMei|Human",       sAutoRoot)
fnRipIdleAnimation("Mei_Mannequin",   sBasePath .. "Mei_Mannequin.png",   0, 400,  3, "AutoLoad|Adventure|SpritesMei|Mannequin",   sAutoRoot)
fnRipIdleAnimation("Mei_Slime",       sBasePath .. "Mei_Slime.png",       0, 400,  9, "AutoLoad|Adventure|SpritesMei|Slime",       sAutoRoot)
fnRipIdleAnimation("Mei_SlimeBlue",   sBasePath .. "Mei_SlimeBlue.png",   0, 400,  9, "AutoLoad|Adventure|SpritesMei|SlimeBlue",   sAutoRoot)
fnRipIdleAnimation("Mei_SlimeInk",    sBasePath .. "Mei_SlimeInk.png",    0, 400,  9, "AutoLoad|Adventure|SpritesMei|SlimeInk",    sAutoRoot)
fnRipIdleAnimation("Mei_SlimePink",   sBasePath .. "Mei_SlimePink.png",   0, 400,  9, "AutoLoad|Adventure|SpritesMei|SlimePink",   sAutoRoot)
fnRipIdleAnimation("Mei_SlimeYellow", sBasePath .. "Mei_SlimeYellow.png", 0, 400,  9, "AutoLoad|Adventure|SpritesMei|SlimeYellow", sAutoRoot)
fnRipIdleAnimation("Mei_Werecat",     sBasePath .. "Mei_Werecat.png",     0, 400,  7, "AutoLoad|Adventure|SpritesMei|Werecat",     sAutoRoot)
fnRipIdleAnimation("Mei_Wisphag",     sBasePath .. "Mei_Wisphag.png",     0, 400,  4, "AutoLoad|Adventure|SpritesMei|Wisphag",     sAutoRoot)
fnRipIdleAnimation("Mei_RubberQueen", sBasePath .. "Mei_RubberQueen.png", 0, 400,  9, "AutoLoad|Adventure|SpritesMei|RubberQueen", sAutoRoot)
fnRipIdleAnimation("Mei_ZombeeQueen", sBasePath .. "Mei_ZombeeQueen.png", 0, 400,  8, "AutoLoad|Adventure|SpritesMei|ZombeeQueen", sAutoRoot)

-- |[ =================== Other Special Frames =================== ]|
--Mannequin Transformation. Only applies to Human.
for i = 0, 17-1, 1 do
    fnRipImageAuto("MannTF|" .. i,  sBasePath .. "Mei_Human.png", 0 + (32 * i), 440, 32, 40, 0, "AutoLoad|Adventure|SpritesMei|Human", "Root/Images/Sprites/MannTF/" .. i)
end
