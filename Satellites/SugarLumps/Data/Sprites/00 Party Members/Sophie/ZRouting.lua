-- |[ ===================================== Sophie Sprites ===================================== ]|
--Sophie's sprite sheets. Assumes the sprite SLF file is already open.
local sBasePath = fnResolvePath()

-- |[ ===================== Standard Frames ====================== ]|
fnRipSheet("Sophie",          sBasePath .. "Sophie.png",           true, false, true)
fnRipSheet("SophieDress",     sBasePath .. "SophieDress.png",      true, false, true)
fnRipSheet("SophieLeather",   sBasePath .. "SophieLeather.png",    true, false, true)

-- |[ ====================== Idle Animations ===================== ]|

-- |[ =================== Other Special Frames =================== ]|
--Normal
ImageLump_Rip("Spcl|Sophie|Laugh0", sBasePath .. "Sophie.png",       0, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Sophie|Laugh1", sBasePath .. "Sophie.png",      32, 360, 32, 40, 0)

--Gala Dress
ImageLump_Rip("Spcl|Sophie|Cry0",   sBasePath .. "SophieDress.png",  0, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Sophie|Cry1",   sBasePath .. "SophieDress.png", 32, 360, 32, 40, 0)
