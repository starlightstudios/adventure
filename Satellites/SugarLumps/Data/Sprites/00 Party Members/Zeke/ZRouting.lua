-- |[ ====================================== Zeke Sprites ====================================== ]|
--Zeke's sprite sheets. Assumes the sprite SLF file is already open.
local sBasePath = fnResolvePath()
local sAutoRoot = "Root/Images/Sprites/"

-- |[ ===================== Standard Frames ====================== ]|
fnRipSheet("Zeke_Goat", sBasePath .. "Zeke_Goat.png",  cbEightDir, cbIsNotWide, cbHasRunAnim, cbNoCrouch, cbNoWound, "AutoLoad|Adventure|SpritesZeke|Goat", sAutoRoot)

-- |[ ====================== Idle Animations ===================== ]|

-- |[ =================== Other Special Frames =================== ]|
fnRipImageAuto("Spcl|Zeke_Goat|Crouch",  sBasePath .. "Zeke_Goat.png",  0, 360, 32, 40, 0, "AutoLoad|Adventure|SpritesZeke|Goat", "Root/Images/Sprites/Special/Zeke_Goat|Crouch")
fnRipImageAuto("Spcl|Zeke_Goat|Wounded", sBasePath .. "Zeke_Goat.png", 32, 360, 32, 40, 0, "AutoLoad|Adventure|SpritesZeke|Goat", "Root/Images/Sprites/Special/Zeke_Goat|Wounded")
