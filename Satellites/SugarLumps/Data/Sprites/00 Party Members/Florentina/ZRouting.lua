-- |[ =================================== Florentina Sprites =================================== ]|
--Florentina's sprite sheets. Assumes the sprite SLF file is already open.
local sBasePath = fnResolvePath()

-- |[ ===================== Standard Frames ====================== ]|
fnRipSheet("Florentina",        sBasePath .. "Florentina.png",         true, false, true)
fnRipSheet("FlorentinaAgarist", sBasePath .. "Florentina_Agarist.png", true, false, true)
fnRipSheet("FlorentinaLurker",  sBasePath .. "Florentina_Lurker.png",  true, false, true)
fnRipSheet("FlorentinaMed",     sBasePath .. "Florentina_Med.png",     true, false, true)
fnRipSheet("FlorentinaTH",      sBasePath .. "Florentina_TH.png",      true, false, true)

-- |[ ====================== Idle Animations ===================== ]|
fnRipIdleAnimation("Florentina",        sBasePath .. "Florentina.png",         0, 400, 15)
fnRipIdleAnimation("FlorentinaAgarist", sBasePath .. "Florentina_Agarist.png", 0, 400,  3)
fnRipIdleAnimation("FlorentinaLurker",  sBasePath .. "Florentina_Lurker.png",  0, 400,  3)
fnRipIdleAnimation("FlorentinaMed",     sBasePath .. "Florentina_Med.png",     0, 400, 15)
fnRipIdleAnimation("FlorentinaTH",      sBasePath .. "Florentina_TH.png",      0, 400, 15)

-- |[ =================== Other Special Frames =================== ]|
--Merchant.
ImageLump_Rip("Spcl|Florentina|Crouch",    sBasePath .. "Florentina.png",      0, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Florentina|Wounded",   sBasePath .. "Florentina.png",     32, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Florentina|Sleep",     sBasePath .. "Florentina.png",     64, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Florentina|Drink0",    sBasePath .. "Florentina.png",     96, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Florentina|Drink1",    sBasePath .. "Florentina.png",    128, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Florentina|Drink2",    sBasePath .. "Florentina.png",    160, 360, 32, 40, 0)

--Lurker (Mannequin).
ImageLump_Rip("Spcl|FlorentinaLurker|Crouch",  sBasePath .. "Florentina_Lurker.png",  0, 360, 32, 40, 0)
ImageLump_Rip("Spcl|FlorentinaLurker|Wounded", sBasePath .. "Florentina_Lurker.png", 32, 360, 32, 40, 0)

--Mediator.
ImageLump_Rip("Spcl|FlorentinaMed|Crouch", sBasePath .. "Florentina_Med.png",  0, 360, 32, 40, 0)
ImageLump_Rip("Spcl|FlorentinaMed|Wounded",sBasePath .. "Florentina_Med.png", 32, 360, 32, 40, 0)
ImageLump_Rip("Spcl|FlorentinaMed|Drink0", sBasePath .. "Florentina_Med.png", 96, 360, 32, 40, 0)
ImageLump_Rip("Spcl|FlorentinaMed|Drink1", sBasePath .. "Florentina_Med.png",128, 360, 32, 40, 0)
ImageLump_Rip("Spcl|FlorentinaMed|Drink2", sBasePath .. "Florentina_Med.png",160, 360, 32, 40, 0)

--Agarist (Mushraune).
ImageLump_Rip("Spcl|FlorentinaAgarist|Crouch", sBasePath .. "Florentina_Agarist.png",  0, 360, 32, 40, 0)
ImageLump_Rip("Spcl|FlorentinaAgarist|Wounded",sBasePath .. "Florentina_Agarist.png", 32, 360, 32, 40, 0)

--Treasure Hunter.
ImageLump_Rip("Spcl|FlorentinaTH|Crouch",  sBasePath .. "Florentina_TH.png",   0, 360, 32, 40, 0)
ImageLump_Rip("Spcl|FlorentinaTH|Wounded", sBasePath .. "Florentina_TH.png",  32, 360, 32, 40, 0)
ImageLump_Rip("Spcl|FlorentinaTH|Drink0",  sBasePath .. "Florentina_TH.png",  96, 360, 32, 40, 0)
ImageLump_Rip("Spcl|FlorentinaTH|Drink1",  sBasePath .. "Florentina_TH.png", 128, 360, 32, 40, 0)
ImageLump_Rip("Spcl|FlorentinaTH|Drink2",  sBasePath .. "Florentina_TH.png", 160, 360, 32, 40, 0)
