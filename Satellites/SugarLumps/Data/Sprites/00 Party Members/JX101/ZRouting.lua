-- |[ ===================================== JX-101 Sprites ===================================== ]|
--JX-101's sprite sheets. Assumes the sprite SLF file is already open.
local sBasePath = fnResolvePath()

-- |[ ===================== Standard Frames ====================== ]|
fnRipSheet("JX-101",   sBasePath  .. "JX-101.png",  true,  false, true)

-- |[ ====================== Idle Animations ===================== ]|

-- |[ =================== Other Special Frames =================== ]|

