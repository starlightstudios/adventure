-- |[ =================================== Bearer Routing File ================================== ]|
--The bearers are the six chapter protagonists. Because they can transform a lot, they have a ton
-- of sprites and thus get their own subfolder.
local sBasePath = fnResolvePath()

-- |[ ======================================== Subfiles ======================================== ]|
--Most of the work is done by calling subfiles.
LM_ExecuteScript(sBasePath .. "Aquillia"   .. "/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "Christine"  .. "/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "Empress"    .. "/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "Florentina" .. "/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "Izuna"      .. "/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "Jeanne"     .. "/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "JX101"      .. "/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "Lotta"      .. "/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "Mei"        .. "/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "Odar"       .. "/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "Sanya"      .. "/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "Sophie"     .. "/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "SX399"      .. "/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "Tiffany"    .. "/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "Zeke"       .. "/ZRouting.lua")
