-- |[ ===================================== Jeanne Sprites ===================================== ]|
--Jeanne's sprite sheets. Assumes the sprite SLF file is already open.
local sBasePath = fnResolvePath()

-- |[ ===================== Standard Frames ====================== ]|
fnRipRunebearer("Jeanne_Human", sBasePath .. "Jeanne_Human.png", "Null")

-- |[ ====================== Idle Animations ===================== ]|

-- |[ =================== Other Special Frames =================== ]|
