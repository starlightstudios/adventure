-- |[ ==================================== Tiffany Sprites ===================================== ]|
--Tiffany's sprite sheets. Assumes the sprite SLF file is already open.
local sBasePath = fnResolvePath()
local sAutoRoot = "Root/Images/Sprites/"

-- |[ ===================== Standard Frames ====================== ]|
fnRipSheet("Tiffany",         sBasePath .. "Tiffany.png",          true, false, true)
fnRipSheet("TiffanySundress", sBasePath .. "Tiffany_Sundress.png", true, false, true)

-- |[ ====================== Idle Animations ===================== ]|
fnRipIdleAnimation("Tiffany",         sBasePath .. "Tiffany.png",          0, 480, 7, "AutoLoad|Adventure|SpritesTiffany|Normal",   sAutoRoot)
fnRipIdleAnimation("TiffanySundress", sBasePath .. "Tiffany_Sundress.png", 0, 360, 7, "AutoLoad|Adventure|SpritesTiffany|Sundress", sAutoRoot)

-- |[ =================== Other Special Frames =================== ]|
--Command Unit
ImageLump_Rip("Spcl|Tiffany|Crouch",          sBasePath .. "Tiffany.png",           32,   0, 32, 40, 0)
ImageLump_Rip("Spcl|Tiffany|Wounded",         sBasePath .. "Tiffany.png",           64,   0, 32, 40, 0)
ImageLump_Rip("Spcl|Tiffany|Chair0",          sBasePath .. "Tiffany.png",            0, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Tiffany|Chair1",          sBasePath .. "Tiffany.png",           32, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Tiffany|Chair2",          sBasePath .. "Tiffany.png",           64, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Tiffany|Chair3",          sBasePath .. "Tiffany.png",           96, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Tiffany|Chair4",          sBasePath .. "Tiffany.png",          128, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Tiffany|Draw0",           sBasePath .. "Tiffany.png",          160, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Tiffany|Draw1",           sBasePath .. "Tiffany.png",          192, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Tiffany|Draw2",           sBasePath .. "Tiffany.png",          224, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Tiffany|Downed",          sBasePath .. "Tiffany.png",           96,   0, 32, 40, 0)
ImageLump_Rip("Spcl|Tiffany|Stealth",         sBasePath .. "Tiffany.png",          128,   0, 32, 40, 0)
ImageLump_Rip("Spcl|Tiffany|Exec0",           sBasePath .. "Tiffany.png",            0, 400, 64, 40, 0)
ImageLump_Rip("Spcl|Tiffany|Exec1",           sBasePath .. "Tiffany.png",           64, 400, 64, 40, 0)
ImageLump_Rip("Spcl|Tiffany|Exec2",           sBasePath .. "Tiffany.png",          128, 400, 64, 40, 0)
ImageLump_Rip("Spcl|Tiffany|Exec3",           sBasePath .. "Tiffany.png",          192, 400, 64, 40, 0)
ImageLump_Rip("Spcl|Tiffany|Exec4",           sBasePath .. "Tiffany.png",          256, 400, 64, 40, 0)
ImageLump_Rip("Spcl|Tiffany|Exec5",           sBasePath .. "Tiffany.png",          320, 400, 64, 40, 0)
ImageLump_Rip("Spcl|Tiffany|Heavy0",          sBasePath .. "Tiffany.png",            0, 440, 32, 40, 0)
ImageLump_Rip("Spcl|Tiffany|Heavy1",          sBasePath .. "Tiffany.png",           32, 440, 32, 40, 0)
ImageLump_Rip("Spcl|Tiffany|Heavy2",          sBasePath .. "Tiffany.png",           64, 440, 32, 40, 0)
ImageLump_Rip("Spcl|Tiffany|Heavy3",          sBasePath .. "Tiffany.png",           96, 440, 32, 40, 0)

--Sundress
ImageLump_Rip("Spcl|TiffanySundress|Crouch",  sBasePath .. "Tiffany_Sundress.png",  32,   0, 32, 40, 0)
ImageLump_Rip("Spcl|TiffanySundress|Wounded", sBasePath .. "Tiffany_Sundress.png",  64,   0, 32, 40, 0)