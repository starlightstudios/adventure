-- |[ ====================================== Izuna Sprites ===================================== ]|
--Izuna's sprite sheets. Assumes the sprite SLF file is already open.
local sBasePath = fnResolvePath()
local sAutoRoot = "Root/Images/Sprites/"

-- |[ ===================== Standard Frames ====================== ]|
fnRipSheet("Izuna_Miko",      sBasePath .. "Izuna_Miko.png",      cbEightDir, cbIsNotWide, cbHasRunAnim, cbNoCrouch, cbNoWound, "AutoLoad|Adventure|SpritesIzuna|Miko",      sAutoRoot)
fnRipSheet("Izuna_Underwear", sBasePath .. "Izuna_Underwear.png", cbEightDir, cbIsNotWide, cbHasRunAnim, cbNoCrouch, cbNoWound, "AutoLoad|Adventure|SpritesIzuna|Underwear", sAutoRoot)

-- |[ ====================== Idle Animations ===================== ]|

-- |[ =================== Other Special Frames =================== ]|
--Miko.
fnRipImageAuto("Spcl|Izuna_Miko|Crouch",      sBasePath .. "Izuna_Miko.png",   0, 360, 32, 40, 0, "AutoLoad|Adventure|SpritesIzuna|Miko", "Root/Images/Sprites/Special/Izuna_Miko|Crouch")
fnRipImageAuto("Spcl|Izuna_Miko|Wounded",     sBasePath .. "Izuna_Miko.png",  32, 360, 32, 40, 0, "AutoLoad|Adventure|SpritesIzuna|Miko", "Root/Images/Sprites/Special/Izuna_Miko|Wounded")
fnRipImageAuto("Spcl|Izuna_Miko|Fall0",       sBasePath .. "Izuna_Miko.png",   0, 400, 32, 40, 0, "AutoLoad|Adventure|SpritesIzuna|Miko", "Root/Images/Sprites/Special/Izuna_Miko|Fall0")
fnRipImageAuto("Spcl|Izuna_Miko|Fall1",       sBasePath .. "Izuna_Miko.png",  32, 400, 32, 40, 0, "AutoLoad|Adventure|SpritesIzuna|Miko", "Root/Images/Sprites/Special/Izuna_Miko|Fall1")
fnRipImageAuto("Spcl|Izuna_Miko|Fall2",       sBasePath .. "Izuna_Miko.png",  64, 400, 32, 40, 0, "AutoLoad|Adventure|SpritesIzuna|Miko", "Root/Images/Sprites/Special/Izuna_Miko|Fall2")
fnRipImageAuto("Spcl|Izuna_Miko|LyingClosed", sBasePath .. "Izuna_Miko.png",  96, 400, 32, 40, 0, "AutoLoad|Adventure|SpritesIzuna|Miko", "Root/Images/Sprites/Special/Izuna_Miko|LyingClosed")
fnRipImageAuto("Spcl|Izuna_Miko|LyingOpen",   sBasePath .. "Izuna_Miko.png", 128, 400, 32, 40, 0, "AutoLoad|Adventure|SpritesIzuna|Miko", "Root/Images/Sprites/Special/Izuna_Miko|LyingOpen")
fnRipImageAuto("Spcl|Izuna_Miko|LyingHeal0",  sBasePath .. "Izuna_Miko.png", 160, 400, 32, 40, 0, "AutoLoad|Adventure|SpritesIzuna|Miko", "Root/Images/Sprites/Special/Izuna_Miko|LyingHeal0")
fnRipImageAuto("Spcl|Izuna_Miko|LyingHeal1",  sBasePath .. "Izuna_Miko.png", 192, 400, 32, 40, 0, "AutoLoad|Adventure|SpritesIzuna|Miko", "Root/Images/Sprites/Special/Izuna_Miko|LyingHeal1")
fnRipImageAuto("Spcl|Izuna_Miko|LyingHeal2",  sBasePath .. "Izuna_Miko.png", 224, 400, 32, 40, 0, "AutoLoad|Adventure|SpritesIzuna|Miko", "Root/Images/Sprites/Special/Izuna_Miko|LyingHeal2")
