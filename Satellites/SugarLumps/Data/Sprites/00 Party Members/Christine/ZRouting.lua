-- |[ ==================================== Christine Sprites =================================== ]|
--Christine's sprite sheets. Assumes the sprite SLF file is already open.
local sBasePath = fnResolvePath()
local sAutoRoot = "Root/Images/Sprites/"

-- |[ ===================== Standard Frames ====================== ]|
fnRipRunebearer("Christine_Male",          sBasePath .. "Chris_Human.png",             "Christine_Male")
fnRipRunebearer("Christine_Darkmatter",    sBasePath .. "Christine_Darkmatter.png",    "Christine_Darkmatter")
fnRipRunebearer("Christine_DreamGirl",     sBasePath .. "Christine_DreamGirl.png",     "Christine_DreamGirl")
fnRipRunebearer("Christine_Doll",          sBasePath .. "Christine_Doll.png",          "Christine_Doll")
fnRipRunebearer("Christine_Electrosprite", sBasePath .. "Christine_Electrosprite.png", "Christine_Electrosprite")
fnRipRunebearer("Christine_Human",         sBasePath .. "Christine_Human.png",         "Christine_Human")
fnRipRunebearer("Christine_Human_Nude",    sBasePath .. "Christine_Human_Nude.png",    "Christine_Human_Nude")
fnRipRunebearer("Christine_Golem",         sBasePath .. "Christine_Golem.png",         "Christine_Golem")
fnRipRunebearer("Christine_GolemDress",    sBasePath .. "Christine_GolemDress.png",    "Christine_GolemDress")
fnRipRunebearer("Christine_Latex",         sBasePath .. "Christine_Latex.png",         "Christine_Latex")
fnRipRunebearer("Christine_Raibie",        sBasePath .. "Christine_Raibie.png",        "Christine_Raibie")
fnRipRunebearer("Christine_Raiju",         sBasePath .. "Christine_Raiju.png",         "Christine_Raiju")
fnRipRunebearer("Christine_RaijuClothes",  sBasePath .. "Christine_RaijuClothes.png",  "Christine_RaijuClothes")
fnRipRunebearer("Christine_Secrebot",      sBasePath .. "Christine_Secrebot.png",      "Christine_Secrebot")
fnRipRunebearer("Christine_SteamDroid",    sBasePath .. "Christine_SteamDroid.png",    "Christine_SteamDroid")

-- |[ ====================== Idle Animations ===================== ]|
fnRipIdleAnimation("Christine_Human",         sBasePath .. "Christine_Human.png",         0, 400, 12, "AutoLoad|Adventure|SpritesChristine|Human",         sAutoRoot)
fnRipIdleAnimation("Christine_Darkmatter",    sBasePath .. "Christine_Darkmatter.png",    0, 400, 11, "AutoLoad|Adventure|SpritesChristine|Darkmatter",    sAutoRoot)
fnRipIdleAnimation("Christine_Doll",          sBasePath .. "Christine_Doll.png",          0, 400,  9, "AutoLoad|Adventure|SpritesChristine|Doll",          sAutoRoot)
fnRipIdleAnimation("Christine_RaijuA",        sBasePath .. "Christine_Raiju.png",         0, 400,  5, "AutoLoad|Adventure|SpritesChristine|RaijuNude",     sAutoRoot)
fnRipIdleAnimation("Christine_RaijuB",        sBasePath .. "Christine_Raiju.png",         0, 440,  5, "AutoLoad|Adventure|SpritesChristine|RaijuNude",     sAutoRoot)
fnRipIdleAnimation("Christine_RaijuC",        sBasePath .. "Christine_Raiju.png",         0, 480,  5, "AutoLoad|Adventure|SpritesChristine|RaijuNude",     sAutoRoot)
fnRipIdleAnimation("Christine_RaijuClothes",  sBasePath .. "Christine_RaijuClothes.png",  0, 400,  5, "AutoLoad|Adventure|SpritesChristine|Raiju",         sAutoRoot)
fnRipIdleAnimation("Christine_RaijuClothes",  sBasePath .. "Christine_RaijuClothes.png",  0, 440,  5, "AutoLoad|Adventure|SpritesChristine|Raiju",         sAutoRoot)
fnRipIdleAnimation("Christine_DreamGirl",     sBasePath .. "Christine_DreamGirl.png",     0, 400, 16, "AutoLoad|Adventure|SpritesChristine|Dreamer",       sAutoRoot)
fnRipIdleAnimation("Christine_Latex",         sBasePath .. "Christine_Latex.png",         0, 400,  9, "AutoLoad|Adventure|SpritesChristine|Latex",         sAutoRoot)
fnRipIdleAnimation("Christine_Electrosprite", sBasePath .. "Christine_Electrosprite.png", 0, 400,  8, "AutoLoad|Adventure|SpritesChristine|Electrosprite", sAutoRoot)
fnRipIdleAnimation("Christine_Golem",         sBasePath .. "Christine_Golem.png",         0, 440,  9, "AutoLoad|Adventure|SpritesChristine|Golem",         sAutoRoot)
fnRipIdleAnimation("Christine_Secrebot",      sBasePath .. "Christine_Secrebot.png",      0, 400,  6, "AutoLoad|Adventure|SpritesChristine|Secrebot",      sAutoRoot)
fnRipIdleAnimation("Christine_SteamDroidA",   sBasePath .. "Christine_SteamDroid.png",    0, 400,  9, "AutoLoad|Adventure|SpritesChristine|SteamDroid",    sAutoRoot)
fnRipIdleAnimation("Christine_SteamDroidB",   sBasePath .. "Christine_SteamDroid.png",    0, 440, 10, "AutoLoad|Adventure|SpritesChristine|SteamDroid",    sAutoRoot)

-- |[ =================== Other Special Frames =================== ]|
--Golem.
ImageLump_Rip("Spcl|Christine_Golem|Sad",    sBasePath .. "Christine_Golem.png", 64, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_Golem|Laugh0", sBasePath .. "Christine_Golem.png",  0, 400, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_Golem|Laugh1", sBasePath .. "Christine_Golem.png", 32, 400, 32, 40, 0)

--Human.
ImageLump_Rip("Spcl|Christine_Human|BedSleep", sBasePath .. "Christine_Human.png",  64, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_Human|BedWake",  sBasePath .. "Christine_Human.png",  96, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_Human|BedFull",  sBasePath .. "Christine_Human.png", 128, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_Human|BedWakeR", sBasePath .. "Christine_Human.png", 160, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_Human|BedFullR", sBasePath .. "Christine_Human.png", 192, 360, 32, 40, 0)

--Doll.
ImageLump_Rip("Spcl|Christine_Doll|FlatbackC", sBasePath .. "Christine_Doll.png",  64, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_Doll|FlatbackO", sBasePath .. "Christine_Doll.png",  96, 360, 32, 40, 0)
