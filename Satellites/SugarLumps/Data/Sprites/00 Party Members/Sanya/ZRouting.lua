-- |[ ====================================== Sanya Sprites ===================================== ]|
--Sanya's sprite sheets. Assumes the sprite SLF file is already open.
local sBasePath = fnResolvePath()
local sAutoRoot = "Root/Images/Sprites/"

-- |[ ===================== Standard Frames ====================== ]|
fnRipRunebearer("Sanya_Bunny_Rifle",     sBasePath .. "Sanya_Bunny_Rifle.png",     "Sanya_Bunny_Rifle",     "AutoLoad|Adventure|SpritesSanya|Bunny_Rifle",     sAutoRoot)
fnRipRunebearer("Sanya_Harpy_Rifle",     sBasePath .. "Sanya_Harpy_Rifle.png",     "Sanya_Harpy_Rifle",     "AutoLoad|Adventure|SpritesSanya|Harpy_Rifle",     sAutoRoot)
fnRipRunebearer("Sanya_Human",           sBasePath .. "Sanya_Human_Normal.png",    "Sanya_Human",           "AutoLoad|Adventure|SpritesSanya|Human",           sAutoRoot)
fnRipRunebearer("Sanya_Human_Carrying",  sBasePath .. "Sanya_Human_Carrying.png",  "Sanya_Human_Carrying",  "AutoLoad|Adventure|SpritesSanya|Human_Carrying",  sAutoRoot)
fnRipRunebearer("Sanya_Human_Rifle",     sBasePath .. "Sanya_Human_Rifle.png",     "Sanya_Human_Rifle",     "AutoLoad|Adventure|SpritesSanya|Human_Rifle",     sAutoRoot)
fnRipRunebearer("Sanya_Sevavi",          sBasePath .. "Sanya_Sevavi_Normal.png",   "Sanya_Sevavi",          "AutoLoad|Adventure|SpritesSanya|Sevavi",          sAutoRoot)
fnRipRunebearer("Sanya_Sevavi_Carrying", sBasePath .. "Sanya_Sevavi_Carrying.png", "Sanya_Sevavi_Carrying", "AutoLoad|Adventure|SpritesSanya|Sevavi_Carrying", sAutoRoot)
fnRipRunebearer("Sanya_Sevavi_Rifle",    sBasePath .. "Sanya_Sevavi_Rifle.png",    "Sanya_Sevavi_Rifle",    "AutoLoad|Adventure|SpritesSanya|Sevavi_Rifle",    sAutoRoot)
fnRipRunebearer("Sanya_Werebat_Rifle",   sBasePath .. "Sanya_Werebat_Rifle.png",   "Sanya_Werebat_Rifle",   "AutoLoad|Adventure|SpritesSanya|Werebat",         sAutoRoot)

-- |[ ====================== Idle Animations ===================== ]|
-- |[ =================== Other Special Frames =================== ]|
-- |[General]|
--Falling/Arm Up. Used for certain scenes. ArmUp is used for puzzle battles.
fnRipImageAuto("Spcl|Sanya_Human|Falling",       sBasePath .. "Sanya_Human_Normal.png",  0, 600, 32, 40, 0, "AutoLoad|Adventure|SpritesSanya|Human",       "Root/Images/Sprites/Special/Sanya_Human|Fall")
fnRipImageAuto("Spcl|Sanya_Human|ArmUp",         sBasePath .. "Sanya_Human_Normal.png", 32, 600, 32, 40, 0, "AutoLoad|Adventure|SpritesSanya|Human",       "Root/Images/Sprites/Special/Sanya_Human|ArmUp")
fnRipImageAuto("Spcl|Sanya_Human_Rifle|Falling", sBasePath .. "Sanya_Human_Rifle.png",   0, 600, 32, 40, 0, "AutoLoad|Adventure|SpritesSanya|Human_Rifle", "Root/Images/Sprites/Special/Sanya_Human_Rifle|Fall")
fnRipImageAuto("Spcl|Sanya_Human_Rifle|ArmUp",   sBasePath .. "Sanya_Human_Rifle.png",  32, 600, 32, 40, 0, "AutoLoad|Adventure|SpritesSanya|Human_Rifle", "Root/Images/Sprites/Special/Sanya_Human_Rifle|ArmUp")

--Catching Izuna.
fnRipImageAuto("Spcl|Sanya_Human|Catch0",       sBasePath .. "Sanya_Human_Normal.png",  64, 600, 32, 40, 0, "AutoLoad|Adventure|SpritesSanya|Human",       "Root/Images/Sprites/Special/Sanya_Human|Catch0")
fnRipImageAuto("Spcl|Sanya_Human|Catch1",       sBasePath .. "Sanya_Human_Normal.png",  96, 600, 32, 40, 0, "AutoLoad|Adventure|SpritesSanya|Human",       "Root/Images/Sprites/Special/Sanya_Human|Catch1")
fnRipImageAuto("Spcl|Sanya_Human|Catch2",       sBasePath .. "Sanya_Human_Normal.png", 128, 600, 32, 40, 0, "AutoLoad|Adventure|SpritesSanya|Human",       "Root/Images/Sprites/Special/Sanya_Human|Catch2")
fnRipImageAuto("Spcl|Sanya_Human|Catch3",       sBasePath .. "Sanya_Human_Normal.png", 160, 600, 32, 40, 0, "AutoLoad|Adventure|SpritesSanya|Human",       "Root/Images/Sprites/Special/Sanya_Human|Catch3")
fnRipImageAuto("Spcl|Sanya_Human_Rifle|Catch0", sBasePath .. "Sanya_Human_Rifle.png",   64, 600, 32, 40, 0, "AutoLoad|Adventure|SpritesSanya|Human_Rifle", "Root/Images/Sprites/Special/Sanya_Human_Rifle|Catch0")
fnRipImageAuto("Spcl|Sanya_Human_Rifle|Catch1", sBasePath .. "Sanya_Human_Rifle.png",   96, 600, 32, 40, 0, "AutoLoad|Adventure|SpritesSanya|Human_Rifle", "Root/Images/Sprites/Special/Sanya_Human_Rifle|Catch1")
fnRipImageAuto("Spcl|Sanya_Human_Rifle|Catch2", sBasePath .. "Sanya_Human_Rifle.png",  128, 600, 32, 40, 0, "AutoLoad|Adventure|SpritesSanya|Human_Rifle", "Root/Images/Sprites/Special/Sanya_Human_Rifle|Catch2")
fnRipImageAuto("Spcl|Sanya_Human_Rifle|Catch3", sBasePath .. "Sanya_Human_Rifle.png",  160, 600, 32, 40, 0, "AutoLoad|Adventure|SpritesSanya|Human_Rifle", "Root/Images/Sprites/Special/Sanya_Human_Rifle|Catch3")

-- |[Kicking Frames]|
--Used for block puzzles. Must be in the same position on all sheets.
local saKickList  = {"KickS0", "KickS1", "KickN0", "KickN1", "KickE0", "KickE1", "KickW0", "KickW1"}
local saNameList = {}
local saImageList = {}
local saAutoList = {}
local function fnAdd(psName, psImage, psAuto)
    table.insert(saNameList, psName)
    table.insert(saImageList, psImage)
    table.insert(saAutoList, psAuto)
end
fnAdd("Sanya_Bunny_Rifle",     "Sanya_Bunny_Rifle.png",     "Bunny_Rifle")
fnAdd("Sanya_Harpy_Rifle",     "Sanya_Harpy_Rifle.png",     "Harpy_Rifle")
fnAdd("Sanya_Human",           "Sanya_Human_Normal.png",    "Human")
fnAdd("Sanya_Human_Carrying",  "Sanya_Human_Carrying.png",  "Human_Carrying")
fnAdd("Sanya_Human_Rifle",     "Sanya_Human_Rifle.png",     "Human_Rifle")
fnAdd("Sanya_Sevavi",          "Sanya_Sevavi_Normal.png",   "Sevavi")
fnAdd("Sanya_Sevavi_Rifle",    "Sanya_Sevavi_Rifle.png",    "Sevavi_Rifle")
fnAdd("Sanya_Sevavi_Carrying", "Sanya_Sevavi_Carrying.png", "Sevavi_Carrying")
fnAdd("Sanya_Werebat_Rifle",   "Sanya_Werebat_Rifle.png",   "Werebat")

for i = 1, #saKickList, 1 do
    for p = 1, #saNameList, 1 do
    
        --Resolve variables.
        local sRipName = "Spcl|" .. saNameList[p] .. "|" .. saKickList[i]
        local sPath = sBasePath .. saImageList[p]
        local sAutoName = "AutoLoad|Adventure|SpritesSanya|" .. saAutoList[p]
        local sAutoPath = "Root/Images/Sprites/Special/" .. saNameList[p] .. "|" .. saKickList[i]
        local iXPos = (i-1) * 32
        local iYPos = 400
        
        --Rip.
        fnRipImageAuto(sRipName, sPath, iXPos, iYPos, 32, 40, 0, sAutoName, sAutoPath)
    end
end

-- |[Rifle Frames]|
--Used for shooting stuff for puzzles. Must be in the same position on all sheets, but some sheets do not have these.
local sPrefix = "Shoot"
local saShootList = {}
for i = 0, 3, 1 do
    table.insert(saShootList, {sPrefix .. "N" ..i,   0 + (32 * i), 440})
    table.insert(saShootList, {sPrefix .. "S" ..i, 128 + (32 * i), 440})
    table.insert(saShootList, {sPrefix .. "W" ..i,   0 + (32 * i), 480})
    table.insert(saShootList, {sPrefix .. "E" ..i, 128 + (32 * i), 480})
    table.insert(saShootList, {sPrefix .. "NW"..i,   0 + (32 * i), 520})
    table.insert(saShootList, {sPrefix .. "NE"..i, 128 + (32 * i), 520})
    table.insert(saShootList, {sPrefix .. "SW"..i,   0 + (32 * i), 560})
    table.insert(saShootList, {sPrefix .. "SE"..i, 128 + (32 * i), 560})
end

--Create a list of costumes that have the shoot frames and rip them.
saNameList = {}
saImageList = {}
saAutoList = {}
fnAdd("Sanya_Bunny_Rifle",   "Sanya_Bunny_Rifle.png",   "Bunny_Rifle")
fnAdd("Sanya_Harpy_Rifle",   "Sanya_Harpy_Rifle.png",   "Harpy_Rifle")
fnAdd("Sanya_Human_Rifle",   "Sanya_Human_Rifle.png",   "Human_Rifle")
fnAdd("Sanya_Sevavi_Rifle",  "Sanya_Sevavi_Rifle.png",  "Sevavi_Rifle")
fnAdd("Sanya_Werebat_Rifle", "Sanya_Werebat_Rifle.png", "Werebat")
for p = 1, #saNameList, 1 do
    for i = 1, #saShootList, 1 do
    
        --Resolve variables.
        local sRipName = "Spcl|" .. saNameList[p] .. "|" .. saShootList[i][1]
        local sPath = sBasePath .. saImageList[p]
        local sAutoName = "AutoLoad|Adventure|SpritesSanya|" .. saAutoList[p]
        local sAutoPath = "Root/Images/Sprites/Special/" .. saNameList[p] .. "|" .. saShootList[i][1]
        
        --Rip.
        fnRipImageAuto(sRipName, sPath, saShootList[i][2], saShootList[i][3], 32, 40, 0, sAutoName, sAutoPath)
    end
end
