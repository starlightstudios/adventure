-- |[ ===================================== SX-399 Sprites ===================================== ]|
--SX-399's sprite sheets. Assumes the sprite SLF file is already open.
local sBasePath = fnResolvePath()
local sAutoRoot = "Root/Images/Sprites/"

-- |[ ===================== Standard Frames ====================== ]|
fnRipSheet("SX-399",     sBasePath .. "SX-399.png",     true, false, true) --Non-party version. Party version is "Lord".
fnRipSheet("SX-399Lord", sBasePath .. "SX-399Lord.png", true, false, true)

-- |[ ====================== Idle Animations ===================== ]|
fnRipIdleAnimation("SX-399Lord", sBasePath .. "SX-399Lord.png", 0, 400, 11, "AutoLoad|Adventure|SpritesSX399|Lord", sAutoRoot)

-- |[ =================== Other Special Frames =================== ]|
--Steam Lord
ImageLump_Rip("Spcl|SX-399Lord|Crouch",  sBasePath .. "SX-399Lord.png",  0, 360, 32, 40, 0)
ImageLump_Rip("Spcl|SX-399Lord|Wounded", sBasePath .. "SX-399Lord.png", 32, 360, 32, 40, 0)
ImageLump_Rip("Spcl|SX-399Lord|Laugh0",  sBasePath .. "SX-399Lord.png", 64, 360, 32, 40, 0)
ImageLump_Rip("Spcl|SX-399Lord|Laugh1",  sBasePath .. "SX-399Lord.png", 96, 360, 32, 40, 0)
