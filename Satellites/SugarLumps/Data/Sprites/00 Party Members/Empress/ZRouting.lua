-- |[ ===================================== Empress Sprites ==================================== ]|
--Empress's sprite sheets. Assumes the sprite SLF file is already open.
local sBasePath = fnResolvePath()
local sAutoRoot = "Root/Images/Sprites/"

-- |[ ===================== Standard Frames ====================== ]|
fnRipRunebearer("Empress_Conquerer", sBasePath .. "Empress_Conquerer.png", "Empress_Conquerer", "AutoLoad|Adventure|SpritesEmpress|Conquerer", sAutoRoot)

-- |[ ====================== Idle Animations ===================== ]|
-- |[]|
--None Yet.

-- |[ =================== Other Special Frames =================== ]|
--Form Name.
--ImageLump_Rip("Spcl|Christine_Golem|Sad",    sBasePath .. "Christine_Golem.png", 64, 360, 32, 40, 0)
