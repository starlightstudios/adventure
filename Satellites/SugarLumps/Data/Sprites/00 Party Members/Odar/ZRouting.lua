-- |[ ====================================== Odar Sprites ====================================== ]|
--Odar's sprite sheets. Assumes the sprite SLF file is already open.
local sBasePath = fnResolvePath()
local sAutoRoot = "Root/Images/Sprites/"

-- |[ ===================== Standard Frames ====================== ]|
fnRipSheet("Odar_Prince", sBasePath .. "Odar_Prince.png", cbEightDir, cbIsNotWide, cbHasRunAnim, cbNoCrouch, cbNoWound, "AutoLoad|Adventure|SpritesOdar|Prince", sAutoRoot)

-- |[ ====================== Idle Animations ===================== ]|

-- |[ =================== Other Special Frames =================== ]|
fnRipImageAuto("Spcl|Odar_Prince|Crouch",  sBasePath .. "Odar_Prince.png",   0, 360, 32, 40, 0, "AutoLoad|Adventure|SpritesOdar|Prince", "Root/Images/Sprites/Special/Odar_Prince|Crouch")
fnRipImageAuto("Spcl|Odar_Prince|Wounded", sBasePath .. "Odar_Prince.png",  32, 360, 32, 40, 0, "AutoLoad|Adventure|SpritesOdar|Prince", "Root/Images/Sprites/Special/Odar_Prince|Wounded")
fnRipImageAuto("Spcl|Odar_Prince|PainS0",  sBasePath .. "Odar_Prince.png",   0, 400, 32, 40, 0, "AutoLoad|Adventure|SpritesOdar|Prince", "Root/Images/Sprites/Special/Odar_Prince|PainS0")
fnRipImageAuto("Spcl|Odar_Prince|PainS1",  sBasePath .. "Odar_Prince.png",  32, 400, 32, 40, 0, "AutoLoad|Adventure|SpritesOdar|Prince", "Root/Images/Sprites/Special/Odar_Prince|PainS1")
fnRipImageAuto("Spcl|Odar_Prince|PainN0",  sBasePath .. "Odar_Prince.png",  64, 400, 32, 40, 0, "AutoLoad|Adventure|SpritesOdar|Prince", "Root/Images/Sprites/Special/Odar_Prince|PainN0")
fnRipImageAuto("Spcl|Odar_Prince|PainN1",  sBasePath .. "Odar_Prince.png",  96, 400, 32, 40, 0, "AutoLoad|Adventure|SpritesOdar|Prince", "Root/Images/Sprites/Special/Odar_Prince|PainN1")
fnRipImageAuto("Spcl|Odar_Prince|PainW0",  sBasePath .. "Odar_Prince.png", 128, 400, 32, 40, 0, "AutoLoad|Adventure|SpritesOdar|Prince", "Root/Images/Sprites/Special/Odar_Prince|PainW0")
fnRipImageAuto("Spcl|Odar_Prince|PainW1",  sBasePath .. "Odar_Prince.png", 160, 400, 32, 40, 0, "AutoLoad|Adventure|SpritesOdar|Prince", "Root/Images/Sprites/Special/Odar_Prince|PainW1")
fnRipImageAuto("Spcl|Odar_Prince|PainE0",  sBasePath .. "Odar_Prince.png", 192, 400, 32, 40, 0, "AutoLoad|Adventure|SpritesOdar|Prince", "Root/Images/Sprites/Special/Odar_Prince|PainE0")
fnRipImageAuto("Spcl|Odar_Prince|PainE1",  sBasePath .. "Odar_Prince.png", 224, 400, 32, 40, 0, "AutoLoad|Adventure|SpritesOdar|Prince", "Root/Images/Sprites/Special/Odar_Prince|PainE1")
