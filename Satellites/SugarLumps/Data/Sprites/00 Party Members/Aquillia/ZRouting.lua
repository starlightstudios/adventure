-- |[ ==================================== Aquillia Sprites ==================================== ]|
--Aquillia's sprite sheets. Assumes the sprite SLF file is already open.
local sBasePath = fnResolvePath()

-- |[ ===================== Standard Frames ====================== ]|
fnRipSheet("Aquillia",         sBasePath .. "Aquillia.png",         true, false, true)
fnRipSheet("AquilliaNoJacket", sBasePath .. "AquilliaNoJacket.png", true, false, true)

-- |[ ====================== Idle Animations ===================== ]|
-- |[ =================== Other Special Frames =================== ]|
ImageLump_Rip("Spcl|Aquillia|Wounded", sBasePath  .. "Aquillia.png", 32, 360, 32, 40, 0)
