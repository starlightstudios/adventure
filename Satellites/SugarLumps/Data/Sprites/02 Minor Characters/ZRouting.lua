-- |[ ================================ Minor Character Sprites ================================= ]|
--Sprites for characters who are not playable, but play a role in their chapter and have a unique sprite.
local sBasePath = fnResolvePath()
local sAutoRoot = "Root/Images/Sprites/"

-- |[ ============================== Chapter 1 ============================= ]|
-- |[Sheets]|
fnRipSheet("Denise",   sBasePath .. "Denise.png",   cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbHasWound, "AutoLoad|Adventure|SpritesMinor|Denise",   sAutoRoot)
fnRipSheet("Kona",     sBasePath .. "Kona.png",     cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbHasWound, "AutoLoad|Adventure|SpritesMinor|Kona",     sAutoRoot)
fnRipSheet("Laura",    sBasePath .. "Laura.png",    cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbNoWound,  "AutoLoad|Adventure|SpritesMinor|Laura",    sAutoRoot)
fnRipSheet("Mycela",   sBasePath .. "Mycela.png",   cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbHasWound, "AutoLoad|Adventure|SpritesMinor|Mycela",   sAutoRoot)
fnRipSheet("Victoria", sBasePath .. "Victoria.png", cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbHasWound, "AutoLoad|Adventure|SpritesMinor|Victoria", sAutoRoot)
fnRipSheet("Meryl",    sBasePath .. "Meryl.png",    cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbHasWound, "AutoLoad|Adventure|SpritesMinor|Meryl",    sAutoRoot)
fnRipSheet("Xanna",    sBasePath .. "Xanna.png",    cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbHasWound, "AutoLoad|Adventure|SpritesMinor|Xanna",    sAutoRoot)
fnRipSheet("XannaBee", sBasePath .. "XannaBee.png", cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbHasWound, "AutoLoad|Adventure|SpritesMinor|XannaBee", sAutoRoot)

-- |[Special Frames]|
--Mycela
ImageLump_Rip("Spcl|Mycela|MushKneel", sBasePath .. "Mycela.png", 0, 200, 32, 40, 0)

--Kona.
fnRipImageAuto("Spcl|Kona|Workout0", sBasePath .. "Kona.png",  0, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesMinor|Kona", "Root/Images/Sprites/Special/Kona|Workout0")
fnRipImageAuto("Spcl|Kona|Workout1", sBasePath .. "Kona.png", 32, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesMinor|Kona", "Root/Images/Sprites/Special/Kona|Workout1")
fnRipImageAuto("Spcl|Kona|Workout2", sBasePath .. "Kona.png", 64, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesMinor|Kona", "Root/Images/Sprites/Special/Kona|Workout2")
fnRipImageAuto("Spcl|Kona|Workout3", sBasePath .. "Kona.png", 96, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesMinor|Kona", "Root/Images/Sprites/Special/Kona|Workout3")
fnRipImageAuto("Spcl|Kona|Workout4", sBasePath .. "Kona.png",  0, 240, 32, 40, 0, "AutoLoad|Adventure|SpritesMinor|Kona", "Root/Images/Sprites/Special/Kona|Workout4")
fnRipImageAuto("Spcl|Kona|Workout5", sBasePath .. "Kona.png", 32, 240, 32, 40, 0, "AutoLoad|Adventure|SpritesMinor|Kona", "Root/Images/Sprites/Special/Kona|Workout5")
fnRipImageAuto("Spcl|Kona|Workout6", sBasePath .. "Kona.png", 64, 240, 32, 40, 0, "AutoLoad|Adventure|SpritesMinor|Kona", "Root/Images/Sprites/Special/Kona|Workout6")
fnRipImageAuto("Spcl|Kona|Workout7", sBasePath .. "Kona.png", 96, 240, 32, 40, 0, "AutoLoad|Adventure|SpritesMinor|Kona", "Root/Images/Sprites/Special/Kona|Workout7")

-- |[ ============================== Chapter 2 ============================= ]|
-- |[Autoloader]|
local sAutoloaderName = "AutoLoad|Adventure|Sprite_Ch2_MinorChar"
local sAutoloaderPath = "Root/Images/Sprites/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[Sheets]|
fnRipSheet("Angelface", sBasePath ..  "Angelface.png", cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbNoWound, "AutoLoad|Adventure|SpritesMinor|Angelface", sAutoRoot)
fnRipSheet("Chikage",   sBasePath ..  "Chikage.png",   cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbNoWound, "AutoLoad|Adventure|SpritesMinor|Chikage",   sAutoRoot)
fnRipSheet("Esmerelda", sBasePath ..  "Esmerelda.png", cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbNoWound, "AutoLoad|Adventure|SpritesMinor|Esmerelda", sAutoRoot)
fnRipSheet("Takahn",    sBasePath ..  "Takahn.png",    cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbNoWound, "AutoLoad|Adventure|SpritesMinor|Takahn",    sAutoRoot)

-- |[Special Frames]|
fnRipImageAuto("Spcl|Angelface|Draw0", sBasePath .. "Angelface.png",  0, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesMinor|Angelface", "Root/Images/Sprites/Special/Angelface|Draw0")
fnRipImageAuto("Spcl|Angelface|Draw1", sBasePath .. "Angelface.png", 32, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesMinor|Angelface", "Root/Images/Sprites/Special/Angelface|Draw1")
fnRipImageAuto("Spcl|Angelface|Draw2", sBasePath .. "Angelface.png", 64, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesMinor|Angelface", "Root/Images/Sprites/Special/Angelface|Draw2")

-- |[ ============================== Chapter 3 ============================= ]|
-- |[ ============================== Chapter 4 ============================= ]|
-- |[ ============================== Chapter 5 ============================= ]|
-- |[Sheets]|
fnRipSheet("1969",          sBasePath .. "1969.png",          cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbNoWound, "AutoLoad|Adventure|SpritesMinor|1969",          sAutoRoot)
fnRipSheet("609144",        sBasePath .. "609144.png",        cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbNoWound, "AutoLoad|Adventure|SpritesMinor|609144",        sAutoRoot)
fnRipSheet("Boombox",       sBasePath .. "Boombox.png",       cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbNoWound, "AutoLoad|Adventure|SpritesMinor|Boombox",       sAutoRoot)
fnRipSheet("Ellie",         sBasePath .. "Ellie.png",         cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbNoWound, "AutoLoad|Adventure|SpritesMinor|Ellie",         sAutoRoot)
fnRipSheet("Katarina",      sBasePath .. "Katarina.png",      cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbNoWound, "AutoLoad|Adventure|SpritesMinor|Katarina",      sAutoRoot)
fnRipSheet("NightHuman",    sBasePath .. "NightHuman.png",    cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbNoWound, "AutoLoad|Adventure|SpritesMinor|NightHuman",    sAutoRoot)
fnRipSheet("NightSecrebot", sBasePath .. "NightSecrebot.png", cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbNoWound, "AutoLoad|Adventure|SpritesMinor|NightSecrebot", sAutoRoot)
fnRipSheet("Talos",         sBasePath .. "Talos.png",         cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbNoWound, "AutoLoad|Adventure|SpritesMinor|Talos",         sAutoRoot)

-- |[Special Frames]|
--1969.
fnRipImageAuto("Spcl|1969|Workout0", sBasePath .. "1969.png",  0, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesMinor|1969", "Root/Images/Sprites/Special/1969|Workout0")
fnRipImageAuto("Spcl|1969|Workout1", sBasePath .. "1969.png", 32, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesMinor|1969", "Root/Images/Sprites/Special/1969|Workout1")
fnRipImageAuto("Spcl|1969|Workout2", sBasePath .. "1969.png", 64, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesMinor|1969", "Root/Images/Sprites/Special/1969|Workout2")
fnRipImageAuto("Spcl|1969|Workout3", sBasePath .. "1969.png", 96, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesMinor|1969", "Root/Images/Sprites/Special/1969|Workout3")

--Boombox.
fnRipImageAuto("Spcl|Boombox|Song0", sBasePath .. "Boombox.png",  0, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesMinor|Boombox", "Root/Images/Sprites/Special/Boombox|Song0")
fnRipImageAuto("Spcl|Boombox|Song1", sBasePath .. "Boombox.png", 32, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesMinor|Boombox", "Root/Images/Sprites/Special/Boombox|Song1")
fnRipImageAuto("Spcl|Boombox|Song2", sBasePath .. "Boombox.png", 64, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesMinor|Boombox", "Root/Images/Sprites/Special/Boombox|Song2")
fnRipImageAuto("Spcl|Boombox|Song3", sBasePath .. "Boombox.png", 96, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesMinor|Boombox", "Root/Images/Sprites/Special/Boombox|Song3")

--Night.
fnRipImageAuto("Spcl|Night|Dust0", sBasePath .. "NightSecrebot.png",  0, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesMinor|NightSecrebot", "Root/Images/Sprites/Special/NightSecrebot|Dust0")
fnRipImageAuto("Spcl|Night|Dust1", sBasePath .. "NightSecrebot.png", 32, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesMinor|NightSecrebot", "Root/Images/Sprites/Special/NightSecrebot|Dust1")
fnRipImageAuto("Spcl|Night|Dust2", sBasePath .. "NightSecrebot.png", 64, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesMinor|NightSecrebot", "Root/Images/Sprites/Special/NightSecrebot|Dust2")
fnRipImageAuto("Spcl|Night|Dust3", sBasePath .. "NightSecrebot.png", 96, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesMinor|NightSecrebot", "Root/Images/Sprites/Special/NightSecrebot|Dust3")
fnRipImageAuto("Spcl|Night|Mop0",  sBasePath .. "NightSecrebot.png",  0, 240, 32, 40, 0, "AutoLoad|Adventure|SpritesMinor|NightSecrebot", "Root/Images/Sprites/Special/NightSecrebot|Mop0")
fnRipImageAuto("Spcl|Night|Mop1",  sBasePath .. "NightSecrebot.png", 32, 240, 32, 40, 0, "AutoLoad|Adventure|SpritesMinor|NightSecrebot", "Root/Images/Sprites/Special/NightSecrebot|Mop1")
fnRipImageAuto("Spcl|Night|Mop2",  sBasePath .. "NightSecrebot.png", 64, 240, 32, 40, 0, "AutoLoad|Adventure|SpritesMinor|NightSecrebot", "Root/Images/Sprites/Special/NightSecrebot|Mop2")
fnRipImageAuto("Spcl|Night|Mop3",  sBasePath .. "NightSecrebot.png", 96, 240, 32, 40, 0, "AutoLoad|Adventure|SpritesMinor|NightSecrebot", "Root/Images/Sprites/Special/NightSecrebot|Mop3")

-- |[ ============================== Chapter 6 ============================= ]|
