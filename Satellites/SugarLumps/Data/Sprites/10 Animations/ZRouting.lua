-- |[ ======================================= Animations ======================================= ]|
--Animations that typically appear either as NPC special frames or as map objects.
local sBasePath = fnResolvePath()
local sAutoloaderName = nil
local sAutoloaderPath = nil

-- |[ ================================= System Sprite (Padded) ================================= ]|
sAutoloaderName = "AutoLoad|Adventure|SystemSprites_Padded"
sAutoloaderPath = "Root/Images/Sprites/Goat/"

-- |[Goat]|
--Baa.
fnRipImageAuto("Goat|0", sBasePath .. "Goat.png",  0, 0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "0")
fnRipImageAuto("Goat|1", sBasePath .. "Goat.png", 32, 0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "1")
fnRipImageAuto("Goat|2", sBasePath .. "Goat.png", 64, 0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "2")

-- |[ ============ Ball Lightning ============ ]|
--Variables.
sAutoloaderPath = "Root/Images/Sprites/BallLightning/"
local ciLitWid = 60
local ciLitHei = 60

--Rip.
for i = 0, 10, 1 do
    local sName = string.format("Lightning|%02i", i)
    local sAuto = string.format("%02i", i)
    fnRipImageAuto(sName, sBasePath .. "Ball Lightning.png", ciLitWid * i, 0, ciLitWid, ciLitHei, 0, sAutoloaderName, sAutoloaderPath .. sAuto)
end

-- |[ ============ Boat Animations =========== ]|
--Used during the moth movie sequence.
local sBoatPath = sBasePath .. "Boats.png"
for i = 0, 9, 1 do
    ImageLump_Rip("SammyBoatNormal|" .. i, sBoatPath, 120 * i,   0, 120, 40, 0)
    ImageLump_Rip("SammyBoatGuns|" .. i,   sBoatPath, 120 * i,  40, 120, 40, 0)
    ImageLump_Rip("SammyBoatShoot|" .. i,  sBoatPath, 120 * i,  80, 120, 40, 0)
    ImageLump_Rip("GolemBoat|" .. i,       sBoatPath, 120 * i, 120, 120, 40, 0)
end
for i = 0, 8, 1 do
    ImageLump_Rip("GolemBoatDestroyed|" .. i, sBoatPath, 120 * i, 160, 120, 40, 0)
end
for i = 0, 24, 1 do
    ImageLump_Rip("GolemBoatDestroying|" .. i, sBoatPath, 120 * i, 200, 120, 40, 0)
end
ImageLump_Rip("SammyBoatRock", sBoatPath, 0, 240, 120, 40, 0)

-- |[ ========= Christine and Sophie ========= ]|
--Special frames for Christine and Sophie at the same time.
ImageLump_Rip("Spcl|ChristineSophie|HoldHands0", sBasePath .. "ChristineSophieSpecialAnim.png",  0,   0, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|HoldHands1", sBasePath .. "ChristineSophieSpecialAnim.png",  0,  40, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|HoldHands2", sBasePath .. "ChristineSophieSpecialAnim.png",  0,  80, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|HoldHands3", sBasePath .. "ChristineSophieSpecialAnim.png",  0, 120, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|HoldHands4", sBasePath .. "ChristineSophieSpecialAnim.png",  0, 160, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|HoldHands5", sBasePath .. "ChristineSophieSpecialAnim.png",  0, 200, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|HoldHands6", sBasePath .. "ChristineSophieSpecialAnim.png",  0, 240, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|HoldHands7", sBasePath .. "ChristineSophieSpecialAnim.png",  0, 280, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|HoldHands8", sBasePath .. "ChristineSophieSpecialAnim.png",  0, 320, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|HoldHands9", sBasePath .. "ChristineSophieSpecialAnim.png",  0, 360, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|Kiss0",      sBasePath .. "ChristineSophieSpecialAnim.png", 48,   0, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|Kiss1",      sBasePath .. "ChristineSophieSpecialAnim.png", 48,  40, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|Kiss2",      sBasePath .. "ChristineSophieSpecialAnim.png", 48,  80, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|Kiss3",      sBasePath .. "ChristineSophieSpecialAnim.png", 48, 120, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|Kiss4",      sBasePath .. "ChristineSophieSpecialAnim.png", 48, 160, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|Kiss5",      sBasePath .. "ChristineSophieSpecialAnim.png", 48, 200, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|Kiss6",      sBasePath .. "ChristineSophieSpecialAnim.png", 48, 240, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|Kiss7",      sBasePath .. "ChristineSophieSpecialAnim.png", 48, 280, 48, 40, 0)

-- |[ ============= Static Burst ============= ]|
--Appears during the chapter 5 flashback scene.
for i = 0, 12, 1 do
    ImageLump_Rip("Spcl|StaticAnim" .. i, sBasePath .. "Static" .. i .. ".png",  0,   0, -1, -1, 16)
end
