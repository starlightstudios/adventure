-- |[ ================================ Chapter 5 System Sprites ================================ ]|
--Sprites used for chapter 5 representing various objects.
local sBasePath = fnResolvePath()

-- |[Autoloader Setup]|
sAutoloaderName = "AutoLoad|Adventure|Sprites_Ch5"

-- |[ ========================= The Tram ========================= ]|
-- |[Tram]|
--Not an NPC but uses the same system as one. It's *complicated*.
fnRipSheet("Tram", sBasePath .. "Tram.png", false, false, false)

-- |[ ======================= World Objects ====================== ]|
-- |[Falling Rocks]|
sAutoloaderPath = "Root/Images/Sprites/RockFalls/"
ImageLump_SetBlockTrimmingFlag(true)
fnRipImageAuto("Obj|FallingRockRegA", sBasePath .. "Falling Rocks.png",  0, 0,  7,  9, 0, sAutoloaderName, sAutoloaderPath .. "RockRegA")
fnRipImageAuto("Obj|FallingRockRegB", sBasePath .. "Falling Rocks.png", 16, 0,  6,  7, 0, sAutoloaderName, sAutoloaderPath .. "RockRegB")
fnRipImageAuto("Obj|FallingRockRegC", sBasePath .. "Falling Rocks.png", 32, 0,  6,  6, 0, sAutoloaderName, sAutoloaderPath .. "RockRegC")
ImageLump_SetBlockTrimmingFlag(false)

-- |[Electronics]|
sAutoloaderPath = "Root/Images/Sprites/"
fnRipImageAuto("Obj|PDU",     sBasePath .. "Objects.png", 0,  0, 16, 16, 0, sAutoloaderName, sAutoloaderPath .. "Objects/PDU")
fnRipImageAuto("Obj|Lantern", sBasePath .. "Objects.png", 0, 16, 16, 16, 0, sAutoloaderName, sAutoloaderPath .. "Objects/Lantern")