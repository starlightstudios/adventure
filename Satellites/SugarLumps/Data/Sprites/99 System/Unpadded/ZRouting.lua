-- |[ ===================================== Un-Padded Pieces ===================================== ]|
--Sprites used for system components, like minor animations, shadows, and UI pieces, that are not
-- specially aligned for use on/with entities.
local sBasePath = fnResolvePath()

-- |[Autoloader Setup]|
sAutoloaderName = "AutoLoad|Adventure|SystemSprites_Unpadded"
sAutoloaderPath = "Root/Images/Sprites/"

-- |[ ======================== Warp Unlock ======================= ]|
--Sequence used when the player steps near a campfire and can warp back to it.
sAutoloaderPath = "Root/Images/Sprites/WarpUnlock/"
local q = 0
for i = 0, 11, 1 do
    fnRipImageAuto("Obj|WarpUnlock" .. q, sBasePath .. "Warp Unlocked.png", i * 64, 0, 64, 40, 0, sAutoloaderName, sAutoloaderPath .. q)
    q = q + 1
end
for i = 0, 8, 1 do
    fnRipImageAuto("Obj|WarpUnlock" .. q, sBasePath .. "Warp Unlocked.png", i * 64, 40, 64, 40, 0, sAutoloaderName, sAutoloaderPath .. q)
    q = q + 1
end

-- |[ ===================== Sprite UI Pieces ===================== ]|
--Enemy Spotting Effects
sAutoloaderPath = "Root/Images/Sprites/Spotted/"
fnRipImageAuto("Spotted|Exclamation", sBasePath .. "Spotted_Exclamation.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Exclamation")
fnRipImageAuto("Spotted|Question",    sBasePath .. "Spotted_Question.png",    0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Question")

--Screen Auras
sAutoloaderPath = "Root/Images/Sprites/Aura/"
fnRipImageAuto("Auras|Wisp", sBasePath .. "Wisp.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Wisp")

-- |[ ====================== World Objects ======================= ]|
-- |[Doors]|
sAutoloaderPath = "Root/Images/Sprites/Objects/"
ImageLump_SetBlockTrimmingFlag(true)
fnRipImageAuto("Obj|DoorNS",          sBasePath .. "Objects.png",  0,   0, 16, 32, 0, sAutoloaderName, sAutoloaderPath .. "DoorNS")
fnRipImageAuto("Obj|DoorEW",          sBasePath .. "Objects.png", 16,   0, 16, 32, 0, sAutoloaderName, sAutoloaderPath .. "DoorEW")
fnRipImageAuto("Obj|DoorNSDungeonC",  sBasePath .. "Objects.png",  0,  32, 16, 32, 0, sAutoloaderName, sAutoloaderPath .. "DoorNSDungeonC")
fnRipImageAuto("Obj|DoorNSDungeonO",  sBasePath .. "Objects.png", 16,  32, 16, 32, 0, sAutoloaderName, sAutoloaderPath .. "DoorNSDungeonO")
fnRipImageAuto("Obj|DoorNSSilverC",   sBasePath .. "Objects.png", 32,  32, 16, 32, 0, sAutoloaderName, sAutoloaderPath .. "DoorNSSilverC")
fnRipImageAuto("Obj|DoorNSSilverO",   sBasePath .. "Objects.png", 48,  32, 16, 32, 0, sAutoloaderName, sAutoloaderPath .. "DoorNSSilverO")
fnRipImageAuto("Obj|DoorNSSpooky",    sBasePath .. "Objects.png",  0, 112, 16, 32, 0, sAutoloaderName, sAutoloaderPath .. "DoorNSSpooky")
fnRipImageAuto("Obj|DoorNSDungeonCV", sBasePath .. "Objects.png", 48,  64, 16, 32, 0, sAutoloaderName, sAutoloaderPath .. "DoorNSDungeonCV")
fnRipImageAuto("Obj|DoorNSDungeonOV", sBasePath .. "Objects.png", 64,  64, 16, 32, 0, sAutoloaderName, sAutoloaderPath .. "DoorNSDungeonOV")
fnRipImageAuto("Obj|DoorNSRegulusC",  sBasePath .. "Objects.png", 48,  96, 16, 32, 0, sAutoloaderName, sAutoloaderPath .. "DoorNSRegulusC")
fnRipImageAuto("Obj|DoorNSRegulusO",  sBasePath .. "Objects.png", 64,  96, 16, 32, 0, sAutoloaderName, sAutoloaderPath .. "DoorNSRegulusO")
fnRipImageAuto("Obj|DoorNSRegulusWC", sBasePath .. "Objects.png", 48, 128, 32, 32, 0, sAutoloaderName, sAutoloaderPath .. "DoorNSRegulusWC")
fnRipImageAuto("Obj|DoorNSRegulusWO", sBasePath .. "Objects.png", 80, 128, 32, 32, 0, sAutoloaderName, sAutoloaderPath .. "DoorNSRegulusWO")
fnRipImageAuto("Obj|DoorEWRegulusC",  sBasePath .. "Objects.png", 80,  64, 16, 32, 0, sAutoloaderName, sAutoloaderPath .. "DoorEWRegulusC")
fnRipImageAuto("Obj|DoorEWRegulusFO", sBasePath .. "Objects.png", 16, 128, 16, 32, 0, sAutoloaderName, sAutoloaderPath .. "DoorEWRegulusFO")
fnRipImageAuto("Obj|DoorEWRegulusFC", sBasePath .. "Objects.png", 32, 128, 16, 32, 0, sAutoloaderName, sAutoloaderPath .. "DoorEWRegulusFC")
ImageLump_SetBlockTrimmingFlag(false)

-- |[Stairs / Ladders / Ropes]|
--Stairs
fnRipImageAuto("Obj|StairUR",   sBasePath .. "Objects.png", 48, 16, 16, 16, 0, sAutoloaderName, sAutoloaderPath .. "StairUR")
fnRipImageAuto("Obj|StairUL",   sBasePath .. "Objects.png", 64, 16, 16, 16, 0, sAutoloaderName, sAutoloaderPath .. "StairUL")
fnRipImageAuto("Obj|StairDR",   sBasePath .. "Objects.png", 80, 16, 16, 16, 0, sAutoloaderName, sAutoloaderPath .. "StairDR")
fnRipImageAuto("Obj|StairDL",   sBasePath .. "Objects.png", 96, 16, 16, 16, 0, sAutoloaderName, sAutoloaderPath .. "StairDL")

--Ladders, Ropes, Etc
fnRipImageAuto("Obj|LadderTop",  sBasePath .. "Objects.png",  0, 64, 16, 16, 0, sAutoloaderName, sAutoloaderPath .. "LadderTop")
fnRipImageAuto("Obj|LadderMid",  sBasePath .. "Objects.png",  0, 80, 16, 16, 0, sAutoloaderName, sAutoloaderPath .. "LadderMid")
fnRipImageAuto("Obj|LadderBot",  sBasePath .. "Objects.png",  0, 96, 16, 16, 0, sAutoloaderName, sAutoloaderPath .. "LadderBot")
fnRipImageAuto("Obj|RopeTop",    sBasePath .. "Objects.png", 16, 64, 16, 16, 0, sAutoloaderName, sAutoloaderPath .. "RopeTop")
fnRipImageAuto("Obj|RopeMid",    sBasePath .. "Objects.png", 16, 80, 16, 16, 0, sAutoloaderName, sAutoloaderPath .. "RopeMid")
fnRipImageAuto("Obj|RopeBot",    sBasePath .. "Objects.png", 16, 96, 16, 16, 0, sAutoloaderName, sAutoloaderPath .. "RopeBot")
fnRipImageAuto("Obj|RopeAnchor", sBasePath .. "Objects.png", 32, 64, 16, 16, 0, sAutoloaderName, sAutoloaderPath .. "RopeAnchor")

-- |[Save Points]|
--Campfire
fnRipImageAuto("Obj|FireUnlit", sBasePath .. "Objects.png", 48,  0, 16, 16, 0, sAutoloaderName, sAutoloaderPath .. "FireUnlit")
fnRipImageAuto("Obj|FireLit0",  sBasePath .. "Objects.png", 64,  0, 16, 16, 0, sAutoloaderName, sAutoloaderPath .. "FireLit0")
fnRipImageAuto("Obj|FireLit1",  sBasePath .. "Objects.png", 80,  0, 16, 16, 0, sAutoloaderName, sAutoloaderPath .. "FireLit1")

--Heating Coils
fnRipImageAuto("Obj|CoilUnlit", sBasePath .. "Objects.png", 144,  0, 16, 16, 0, sAutoloaderName, sAutoloaderPath .. "CoilUnlit")
fnRipImageAuto("Obj|CoilLit0",  sBasePath .. "Objects.png", 160,  0, 16, 16, 0, sAutoloaderName, sAutoloaderPath .. "CoilLit0")
fnRipImageAuto("Obj|CoilLit1",  sBasePath .. "Objects.png", 176,  0, 16, 16, 0, sAutoloaderName, sAutoloaderPath .. "CoilLit1")

--Benches
fnRipImageAuto("Obj|BenchWood",  sBasePath .. "Objects.png", 192,  0, 32, 16, 0, sAutoloaderName, sAutoloaderPath .. "BenchWood")
fnRipImageAuto("Obj|BenchMetal", sBasePath .. "Objects.png", 224,  0, 32, 16, 0, sAutoloaderName, sAutoloaderPath .. "BenchMetal")

-- |[Treasure Chests]|
--Wooden
ImageLump_SetBlockTrimmingFlag(true)
fnRipImageAuto("Obj|ChestC",  sBasePath .. "Objects.png", 112,  0, 16, 32, 0, sAutoloaderName, sAutoloaderPath .. "ChestC")
fnRipImageAuto("Obj|ChestO",  sBasePath .. "Objects.png", 128,  0, 16, 32, 0, sAutoloaderName, sAutoloaderPath .. "ChestO")
fnRipImageAuto("Obj|ChestBC", sBasePath .. "Objects.png", 112, 48, 16, 32, 0, sAutoloaderName, sAutoloaderPath .. "ChestBC")
fnRipImageAuto("Obj|ChestBO", sBasePath .. "Objects.png", 128, 48, 16, 32, 0, sAutoloaderName, sAutoloaderPath .. "ChestBO")
fnRipImageAuto("Obj|ChestGC", sBasePath .. "Objects.png", 208, 48, 16, 32, 0, sAutoloaderName, sAutoloaderPath .. "ChestGC")
fnRipImageAuto("Obj|ChestGO", sBasePath .. "Objects.png", 224, 48, 16, 32, 0, sAutoloaderName, sAutoloaderPath .. "ChestGO")
ImageLump_SetBlockTrimmingFlag(false)

--Futuristic Chests
ImageLump_SetBlockTrimmingFlag(true)
fnRipImageAuto("Obj|ChestFC",  sBasePath .. "Objects.png", 144, 48, 16, 32, 0, sAutoloaderName, sAutoloaderPath .. "ChestFC")
fnRipImageAuto("Obj|ChestFO",  sBasePath .. "Objects.png", 160, 48, 16, 32, 0, sAutoloaderName, sAutoloaderPath .. "ChestFO")
fnRipImageAuto("Obj|ChestFBC", sBasePath .. "Objects.png", 176, 48, 16, 32, 0, sAutoloaderName, sAutoloaderPath .. "ChestFBC")
fnRipImageAuto("Obj|ChestFBO", sBasePath .. "Objects.png", 192, 48, 16, 32, 0, sAutoloaderName, sAutoloaderPath .. "ChestFBO")
ImageLump_SetBlockTrimmingFlag(false)

-- |[Switches]|
--Switches
ImageLump_SetBlockTrimmingFlag(true)
fnRipImageAuto("Obj|SwitchUp",      sBasePath .. "Objects.png",  64, 32, 16, 32, 0, sAutoloaderName, sAutoloaderPath .. "SwitchUp")
fnRipImageAuto("Obj|SwitchDn",      sBasePath .. "Objects.png",  80, 32, 16, 32, 0, sAutoloaderName, sAutoloaderPath .. "SwitchDn")
fnRipImageAuto("Obj|Block",         sBasePath .. "Objects.png", 112, 80, 16, 16, 0, sAutoloaderName, sAutoloaderPath .. "Block")
fnRipImageAuto("Obj|FloorSwitchUp", sBasePath .. "Objects.png", 128, 80, 16, 16, 0, sAutoloaderName, sAutoloaderPath .. "FloorSwitchUp")
fnRipImageAuto("Obj|FloorSwitchDn", sBasePath .. "Objects.png", 144, 80, 16, 16, 0, sAutoloaderName, sAutoloaderPath .. "FloorSwitchDn")
ImageLump_SetBlockTrimmingFlag(false)

-- |[Poof Cloud]|
--Poof! Used when enemies are defeated.
sAutoloaderPath = "Root/Images/Sprites/EnemyPoof/"
fnRipImageAuto("Obj|Poof0", sBasePath .. "Poof.png",  0,  0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "0")
fnRipImageAuto("Obj|Poof1", sBasePath .. "Poof.png", 32,  0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "1")
fnRipImageAuto("Obj|Poof2", sBasePath .. "Poof.png", 64,  0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "2")
fnRipImageAuto("Obj|Poof3", sBasePath .. "Poof.png", 96,  0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "3")

-- |[Splash]|
--Sploooosh.
sAutoloaderPath = "Root/Images/Sprites/Splash/"
fnRipImageAuto("Obj|Splash0", sBasePath .. "Splash.png",  0,  0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "0")
fnRipImageAuto("Obj|Splash1", sBasePath .. "Splash.png", 32,  0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "1")
fnRipImageAuto("Obj|Splash2", sBasePath .. "Splash.png", 64,  0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "2")
fnRipImageAuto("Obj|Splash3", sBasePath .. "Splash.png", 96,  0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "3")

-- |[Bullet Impacts]|
sAutoloaderPath = "Root/Images/Sprites/Impacts/"
fnRipImageAuto("Impact|Bullet0", sBasePath .. "BulletImpact.png",  0, 0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "Bullet0")
fnRipImageAuto("Impact|Bullet1", sBasePath .. "BulletImpact.png", 32, 0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "Bullet1")
fnRipImageAuto("Impact|Bullet2", sBasePath .. "BulletImpact.png", 64, 0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "Bullet2")
fnRipImageAuto("Impact|Bullet3", sBasePath .. "BulletImpact.png", 96, 0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "Bullet3")

-- |[ ================== Apartment Customization ================= ]|
--Cut content from chapter 5, the skeleton is left in place for modders to maybe look at.
sAutoloaderPath = "Root/Images/Sprites/Quarters/"
fnRipImageAuto("AptObj|Tube0",    sBasePath .. "ApartmentObjects.png",  0,   0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "Tube0")
fnRipImageAuto("AptObj|Tube1",    sBasePath .. "ApartmentObjects.png", 32,   0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "Tube1")
fnRipImageAuto("AptObj|Tube2",    sBasePath .. "ApartmentObjects.png", 64,   0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "Tube2")
fnRipImageAuto("AptObj|Tube3",    sBasePath .. "ApartmentObjects.png", 96,   0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "Tube3")
fnRipImageAuto("AptObj|TubeLD",   sBasePath .. "ApartmentObjects.png", 64,  80, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "TubeLD")
fnRipImageAuto("AptObj|Tube55",   sBasePath .. "ApartmentObjects.png", 96,  80, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "Tube55")
fnRipImageAuto("AptObj|CounterL", sBasePath .. "ApartmentObjects.png",  0,  40, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "CounterL")
fnRipImageAuto("AptObj|CounterM", sBasePath .. "ApartmentObjects.png", 32,  40, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "CounterM")
fnRipImageAuto("AptObj|CounterR", sBasePath .. "ApartmentObjects.png", 64,  40, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "CounterR")
fnRipImageAuto("AptObj|Coffee",   sBasePath .. "ApartmentObjects.png",  0,  80, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "Coffee")
fnRipImageAuto("AptObj|TV",       sBasePath .. "ApartmentObjects.png",  0, 120, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "TV")
fnRipImageAuto("AptObj|ChairS",   sBasePath .. "ApartmentObjects.png",  0, 160, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "ChairS")
fnRipImageAuto("AptObj|CouchSL",  sBasePath .. "ApartmentObjects.png",  0, 200, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "CouchSL")
fnRipImageAuto("AptObj|CouchSM",  sBasePath .. "ApartmentObjects.png", 32, 200, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "CouchSM")
fnRipImageAuto("AptObj|CouchSR",  sBasePath .. "ApartmentObjects.png", 64, 200, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "CouchSR")
fnRipImageAuto("AptObj|ChairN",   sBasePath .. "ApartmentObjects.png",  0, 240, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "ChairN")
fnRipImageAuto("AptObj|CouchNL",  sBasePath .. "ApartmentObjects.png",  0, 280, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "CouchNL")
fnRipImageAuto("AptObj|CouchNM",  sBasePath .. "ApartmentObjects.png", 32, 280, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "CouchNM")
fnRipImageAuto("AptObj|CouchNR",  sBasePath .. "ApartmentObjects.png", 64, 280, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "CouchNR")
