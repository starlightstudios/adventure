-- |[ ====================================== Padded Pieces ===================================== ]|
--"Padded" sprites used by many chapters. Padded sprites are specially aligned with entities on the
-- field and can thus be used as their overlays or replacements.
local sBasePath = fnResolvePath()

-- |[Autoloader Setup]|
sAutoloaderName = "AutoLoad|Adventure|SystemSprites_Padded"

-- |[ =================== Catalysts ==================== ]|
--Setup.
sAutoloaderPath = "Root/Images/Sprites/Catalyst/"

--Health.
fnRipImageAuto("Catalyst|Heart",          sBasePath .. "Catalyst.png",   0, 0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "Heart")
fnRipImageAuto("Catalyst|HeartFirework0", sBasePath .. "Catalyst.png",  32, 0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "HeartFirework0")
fnRipImageAuto("Catalyst|HeartFirework1", sBasePath .. "Catalyst.png",  64, 0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "HeartFirework1")
fnRipImageAuto("Catalyst|HeartFirework2", sBasePath .. "Catalyst.png",  96, 0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "HeartFirework2")
fnRipImageAuto("Catalyst|HeartFirework3", sBasePath .. "Catalyst.png", 128, 0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "HeartFirework3")

--Attack Power.
fnRipImageAuto("Catalyst|Sword",          sBasePath .. "Catalyst.png",   0, 40, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "Sword")
fnRipImageAuto("Catalyst|SwordFirework0", sBasePath .. "Catalyst.png",  32, 40, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "SwordFirework0")
fnRipImageAuto("Catalyst|SwordFirework1", sBasePath .. "Catalyst.png",  64, 40, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "SwordFirework1")
fnRipImageAuto("Catalyst|SwordFirework2", sBasePath .. "Catalyst.png",  96, 40, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "SwordFirework2")
fnRipImageAuto("Catalyst|SwordFirework3", sBasePath .. "Catalyst.png", 128, 40, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "SwordFirework3")

--Accuracy.
fnRipImageAuto("Catalyst|Target",          sBasePath .. "Catalyst.png",   0, 80, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "Target")
fnRipImageAuto("Catalyst|TargetFirework0", sBasePath .. "Catalyst.png",  32, 80, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "TargetFirework0")
fnRipImageAuto("Catalyst|TargetFirework1", sBasePath .. "Catalyst.png",  64, 80, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "TargetFirework1")
fnRipImageAuto("Catalyst|TargetFirework2", sBasePath .. "Catalyst.png",  96, 80, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "TargetFirework2")
fnRipImageAuto("Catalyst|TargetFirework3", sBasePath .. "Catalyst.png", 128, 80, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "TargetFirework3")

--Initiative.
fnRipImageAuto("Catalyst|Boot",          sBasePath .. "Catalyst.png",   0, 120, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "Boot")
fnRipImageAuto("Catalyst|BootFirework0", sBasePath .. "Catalyst.png",  32, 120, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "BootFirework0")
fnRipImageAuto("Catalyst|BootFirework1", sBasePath .. "Catalyst.png",  64, 120, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "BootFirework1")
fnRipImageAuto("Catalyst|BootFirework2", sBasePath .. "Catalyst.png",  96, 120, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "BootFirework2")
fnRipImageAuto("Catalyst|BootFirework3", sBasePath .. "Catalyst.png", 128, 120, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "BootFirework3")

--Evasion.
fnRipImageAuto("Catalyst|Dodge",          sBasePath .. "Catalyst.png",   0, 160, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "Dodge")
fnRipImageAuto("Catalyst|DodgeFirework0", sBasePath .. "Catalyst.png",  32, 160, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "DodgeFirework0")
fnRipImageAuto("Catalyst|DodgeFirework1", sBasePath .. "Catalyst.png",  64, 160, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "DodgeFirework1")
fnRipImageAuto("Catalyst|DodgeFirework2", sBasePath .. "Catalyst.png",  96, 160, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "DodgeFirework2")
fnRipImageAuto("Catalyst|DodgeFirework3", sBasePath .. "Catalyst.png", 128, 160, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "DodgeFirework3")

--Evasion.
fnRipImageAuto("Catalyst|Skill",          sBasePath .. "Catalyst.png",   0, 200, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "Skill")
fnRipImageAuto("Catalyst|SkillFirework0", sBasePath .. "Catalyst.png",  32, 200, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "SkillFirework0")
fnRipImageAuto("Catalyst|SkillFirework1", sBasePath .. "Catalyst.png",  64, 200, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "SkillFirework1")
fnRipImageAuto("Catalyst|SkillFirework2", sBasePath .. "Catalyst.png",  96, 200, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "SkillFirework2")
fnRipImageAuto("Catalyst|SkillFirework3", sBasePath .. "Catalyst.png", 128, 200, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "SkillFirework3")

-- |[ =============== Doctor Bag Refill ================ ]|
--Refills the doctor bag and despawns when examined.
sAutoloaderPath = "Root/Images/Sprites/BagRefill/"
fnRipImageAuto("BagRefill|0", sBasePath .. "BagRefill.png",  0, 0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "0")
fnRipImageAuto("BagRefill|1", sBasePath .. "BagRefill.png", 32, 0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "1")
fnRipImageAuto("BagRefill|2", sBasePath .. "BagRefill.png", 64, 0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "2")

-- |[ ================= Adamantite Node ================ ]|
--Provides a script-determined item when examined.
sAutoloaderPath = "Root/Images/Sprites/AdamantiteNode/"
fnRipImageAuto("AdamantiteNode|0", sBasePath .. "AdamantiteNode.png",   0, 0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "0")
fnRipImageAuto("AdamantiteNode|1", sBasePath .. "AdamantiteNode.png",  32, 0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "1")
fnRipImageAuto("AdamantiteNode|2", sBasePath .. "AdamantiteNode.png",  64, 0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "2")
fnRipImageAuto("AdamantiteNode|3", sBasePath .. "AdamantiteNode.png",  96, 0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "3")
fnRipImageAuto("AdamantiteNode|4", sBasePath .. "AdamantiteNode.png", 128, 0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "4")
fnRipImageAuto("AdamantiteNode|5", sBasePath .. "AdamantiteNode.png", 160, 0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "5")

-- |[ =================== Mugging UI =================== ]|
sAutoloaderPath = "Root/Images/Sprites/Mugging/"
fnRipImageAuto("MugBarEmpty", sBasePath .. "MugBarEmpty.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "BarEmpty")
fnRipImageAuto("MugBarFull",  sBasePath .. "MugBarFull.png",  0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "BarFull")
fnRipImageAuto("MugStun0",    sBasePath .. "MugStun0.png",    0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Stun0")
fnRipImageAuto("MugStun1",    sBasePath .. "MugStun1.png",    0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Stun1")
fnRipImageAuto("MugStun2",    sBasePath .. "MugStun2.png",    0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Stun2")

-- |[ ============== Shadows, Water, Etc. ============== ]|
sAutoloaderPath = "Root/Images/Sprites/Shadows/"
fnRipImageAuto("Default Depth Marker", sBasePath .. "Default Depth Marker.png", 0, 0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "Depth")
fnRipImageAuto("Default Shadow",       sBasePath .. "Default Shadow.png",       0, 0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "Generic")
