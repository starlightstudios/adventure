-- |[ ================================ Chapter 1 System Sprites ================================ ]|
--Sprites used for chapter 1 representing various objects.
local sBasePath = fnResolvePath()

-- |[Autoloader Setup]|
sAutoloaderName = "AutoLoad|Adventure|Sprites_Ch1"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[ ===================== Farming Minigame ===================== ]|
sAutoloaderPath = "Root/Images/Sprites/FarmIcons/"
fnRipImageAuto("FarmIcon|Water0", sBasePath .. "FarmIcons.png",  0,   0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "Water0")
fnRipImageAuto("FarmIcon|Water1", sBasePath .. "FarmIcons.png", 32,   0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "Water1")
fnRipImageAuto("FarmIcon|Ferti0", sBasePath .. "FarmIcons.png",  0,  40, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "Ferti0")
fnRipImageAuto("FarmIcon|Ferti1", sBasePath .. "FarmIcons.png", 32,  40, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "Ferti1")
fnRipImageAuto("FarmIcon|Grass0", sBasePath .. "FarmIcons.png",  0,  80, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "Grass0")
fnRipImageAuto("FarmIcon|Grass1", sBasePath .. "FarmIcons.png", 32,  80, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "Grass1")
fnRipImageAuto("FarmIcon|Polln0", sBasePath .. "FarmIcons.png",  0, 120, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "Polln0")
fnRipImageAuto("FarmIcon|Polln1", sBasePath .. "FarmIcons.png", 32, 120, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "Polln1")

-- |[ ======================= Other Sprites ====================== ]|
sAutoloaderPath = "Root/Images/Sprites/Hacksaw/"
fnRipImageAuto("Hacksaw", sBasePath .. "Hacksaw.png", 0, 0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "Hacksaw")