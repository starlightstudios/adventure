-- |[ ===================================== System Sprites ===================================== ]|
--Sprites used for system components, like minor animations, shadows, and UI pieces. Calls subfolders.
local sBasePath = fnResolvePath()
LM_ExecuteScript(sBasePath .. "Padded/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "Unpadded/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "Chapter 1/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "Chapter 2/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "Chapter 5/ZRouting.lua")
