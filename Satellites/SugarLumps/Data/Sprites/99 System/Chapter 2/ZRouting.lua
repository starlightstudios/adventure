-- |[ ================================ Chapter 2 System Sprites ================================ ]|
--Sprites used for chapter 1 representing various objects.
local sBasePath = fnResolvePath()

-- |[ ======================== Animations ======================== ]|
-- |[Sparkles]|
--What exactly were you eating, Zeke?
sAutoloaderName = "AutoLoad|Adventure|SpritesCh2System|Sparkles"
sAutoloaderPath = "Root/Images/Sprites/Sparkles/"
SLF_RegisterAutoLoadLump(sAutoloaderName)
for i = 0, 15, 1 do
    local sName = string.format("Obj|Sparkle%02i", i)
    local sAuto = string.format(sAutoloaderPath .. "%02i", i)
    fnRipImageAuto(sName, sBasePath .. "Sparkles.png", i * 32,  0, 32, 40, 0, sAutoloaderName, sAuto)
end

-- |[ ===================== Physics Objects ====================== ]|
-- |[Bouncy Ball]|
sAutoloaderName = "AutoLoad|Adventure|SpritesCh2System|Impacts"
sAutoloaderPath = "Root/Images/Sprites/BouncyBall/"
SLF_RegisterAutoLoadLump(sAutoloaderName)
fnRipImageAuto("Obj|BouncyBall", sBasePath .. "BouncyBall.png",  0,  0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "0")

-- |[Fragments]|
--Pieces used to blow open the wooden gate in the mines in chapter 2.
sAutoloaderName = "AutoLoad|Adventure|SpritesCh2System|Fragments"
sAutoloaderPath = "Root/Images/Sprites/Fragments/"
SLF_RegisterAutoLoadLump(sAutoloaderName)
fnRipImageAuto("Obj|FragmentWood0", sBasePath .. "Fragments.png",  0,  0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "Wood0")
fnRipImageAuto("Obj|FragmentWood1", sBasePath .. "Fragments.png", 32,  0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "Wood1")
fnRipImageAuto("Obj|FragmentWood2", sBasePath .. "Fragments.png", 64,  0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "Wood2")
fnRipImageAuto("Obj|FragmentRock0", sBasePath .. "Fragments.png",  0, 40, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "Rock0")
fnRipImageAuto("Obj|FragmentRock1", sBasePath .. "Fragments.png", 32, 40, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "Rock1")

-- |[ ======================= Hunting Pieces ===================== ]|
-- |[Loot Drop]|
sAutoloaderName = "AutoLoad|Adventure|SpritesCh2System|Loot"
sAutoloaderPath = "Root/Images/Sprites/FallenLoot/"
SLF_RegisterAutoLoadLump(sAutoloaderName)
fnRipImageAuto("LootDrop|0", sBasePath .. "FallenLoot.png",  0, 0, 32, 40, 0, sAutoloaderName, sAutoloaderPath .. "0")

-- |[Tracks]|
--Indicates the direction an enemy moved.
sAutoloaderName = "AutoLoad|Adventure|SpritesCh2System|Tracks"
sAutoloaderPath = "Root/Images/Sprites/Tracks/"
SLF_RegisterAutoLoadLump(sAutoloaderName)
ImageLump_SetBlockTrimmingFlag(true)
fnRipImageAuto("Obj|HuntTracksN",  sBasePath .. "Tracks.png",   0, 0, 16, 16, 0, sAutoloaderName, sAutoloaderPath .. "TracksN")
fnRipImageAuto("Obj|HuntTracksNE", sBasePath .. "Tracks.png",  16, 0, 16, 16, 0, sAutoloaderName, sAutoloaderPath .. "TracksNE")
fnRipImageAuto("Obj|HuntTracksE",  sBasePath .. "Tracks.png",  32, 0, 16, 16, 0, sAutoloaderName, sAutoloaderPath .. "TracksE")
fnRipImageAuto("Obj|HuntTracksSE", sBasePath .. "Tracks.png",  48, 0, 16, 16, 0, sAutoloaderName, sAutoloaderPath .. "TracksSE")
fnRipImageAuto("Obj|HuntTracksS",  sBasePath .. "Tracks.png",  64, 0, 16, 16, 0, sAutoloaderName, sAutoloaderPath .. "TracksS")
fnRipImageAuto("Obj|HuntTracksSW", sBasePath .. "Tracks.png",  80, 0, 16, 16, 0, sAutoloaderName, sAutoloaderPath .. "TracksSW")
fnRipImageAuto("Obj|HuntTracksW",  sBasePath .. "Tracks.png",  96, 0, 16, 16, 0, sAutoloaderName, sAutoloaderPath .. "TracksW")
fnRipImageAuto("Obj|HuntTracksNW", sBasePath .. "Tracks.png", 112, 0, 16, 16, 0, sAutoloaderName, sAutoloaderPath .. "TracksNW")
ImageLump_SetBlockTrimmingFlag(false)
