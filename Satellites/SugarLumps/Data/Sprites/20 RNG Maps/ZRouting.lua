-- |[ ===================================== RLG Texturing ====================================== ]|
--Randomly generated levels need tilemaps to work. These don't use the same logic as the MapData.slf
-- versions, instead they are mapped by lua scripts to fill certain slots.
local sBasePath = fnResolvePath()
sAutoloaderName = "AutoLoad|Adventure|SystemSprites_Unpadded"
sAutoloaderPath = "Root/Images/Sprites/LevelTextures/"

-- |[Cave Levels]|
ImageLump_SetBlockTrimmingFlag(true)
fnRipImageAuto("Level|RegulusCave",       sBasePath .. "RegulusCave.png",           0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "RegulusCave")
fnRipImageAuto("Level|RegulusCaveFungus", sBasePath .. "RegulusCaveFungusFull.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "RegulusCaveFungus")
ImageLump_SetBlockTrimmingFlag(false)
