-- |[ ======================================== Monsters ======================================== ]|
--Sprites for monsters. Paragons use a parallel file pattern.
local sBasePath = fnResolvePath()
local sAutoloadBase = "Root/Images/Sprites/"

-- |[ ============================= Chapter 0 ============================== ]|
-- |[Setup]|
local sMonstersCH0Path        = sBasePath .. "MonstersCH0/"
local sMonstersCH0PathParagon = sBasePath .. "ParagonsCH0/"

-- |[Normal]|
fnRipSheet("Bandit_Female", sMonstersCH0Path .. "Bandit_Female.png", false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh0|Bandit_Female", sAutoloadBase)
fnRipSheet("Bandit_Male",   sMonstersCH0Path .. "Bandit_Male.png",   false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh0|Bandit_Male",   sAutoloadBase)
fnRipSheet("BanditCatB",    sMonstersCH0Path .. "BanditCatB.png",    false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh0|BanditCatB",    sAutoloadBase)
fnRipSheet("BestFriend",    sMonstersCH0Path .. "BestFriend.png",    false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh0|BestFriend",    sAutoloadBase)
fnRipSheet("CultistF",      sMonstersCH0Path .. "Cultist_F.png",     false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh0|CultistF",      sAutoloadBase)
fnRipSheet("CultistFA",     sMonstersCH0Path .. "Cultist_FA.png",    false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh0|CultistFA",     sAutoloadBase)
fnRipSheet("CultistM",      sMonstersCH0Path .. "Cultist_M.png",     false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh0|CultistM",      sAutoloadBase)
fnRipSheet("MercDemon",     sMonstersCH0Path .. "MercDemon.png",     false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh0|MercDemon",     sAutoloadBase)
fnRipSheet("MirrorImage",   sMonstersCH0Path .. "MirrorImage.png",   false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh0|MirrorImage",   sAutoloadBase)
fnRipSheet("SkullCrawler",  sMonstersCH0Path .. "SkullCrawler.png",  false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh0|SkullCrawler",  sAutoloadBase)
fnRipSheet("Wisphag",       sMonstersCH0Path .. "Wisphag.png",       false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh0|Wisphag",       sAutoloadBase)
fnRipSheet("RedRobe",       sMonstersCH0Path .. "RedRobe.png",       false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh0|RedRobe",       sAutoloadBase)--Has no paragon!

-- |[Paragon]|
fnRipSheet("Bandit_Female" .. "Paragon", sMonstersCH0PathParagon .. "Bandit_Female.png", false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh0|Bandit_Female", sAutoloadBase)
fnRipSheet("Bandit_Male"   .. "Paragon", sMonstersCH0PathParagon .. "Bandit_Male.png",   false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh0|Bandit_Male",   sAutoloadBase)
fnRipSheet("BanditCatB"    .. "Paragon", sMonstersCH0PathParagon .. "BanditCatB.png",    false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh0|BanditCatB",    sAutoloadBase)
fnRipSheet("BestFriend"    .. "Paragon", sMonstersCH0PathParagon .. "BestFriend.png",    false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh0|BestFriend",    sAutoloadBase)
fnRipSheet("CultistF"      .. "Paragon", sMonstersCH0PathParagon .. "Cultist_F.png",     false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh0|CultistF",      sAutoloadBase)
fnRipSheet("CultistM"      .. "Paragon", sMonstersCH0PathParagon .. "Cultist_M.png",     false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh0|CultistM",      sAutoloadBase)
fnRipSheet("MercDemon"     .. "Paragon", sMonstersCH0PathParagon .. "MercDemon.png",     false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh0|MercDemon",     sAutoloadBase)
fnRipSheet("MirrorImage"   .. "Paragon", sMonstersCH0PathParagon .. "MirrorImage.png",   false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh0|MirrorImage",   sAutoloadBase)
fnRipSheet("SkullCrawler"  .. "Paragon", sMonstersCH0PathParagon .. "SkullCrawler.png",  false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh0|SkullCrawler",  sAutoloadBase)
fnRipSheet("Wisphag"       .. "Paragon", sMonstersCH0PathParagon .. "Wisphag.png",       false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh0|Wisphag",       sAutoloadBase)

-- |[ ============================= Chapter 1 ============================== ]|
-- |[Setup]|
local sMonstersCH1Path        = sBasePath .. "MonstersCH1/"
local sMonstersCH1PathParagon = sBasePath .. "ParagonsCH1/"

-- |[Normal]|
--Standard Enemies
fnRipSheet("Alraune",      sMonstersCH1Path .. "Alraune.png",      false, false, false, true,  true, "AutoLoad|Adventure|SpritesEnemiesCh1|Alraune",      sAutoloadBase)
fnRipSheet("BeeBuddy",     sMonstersCH1Path .. "BeeBuddy.png",     false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh1|BeeBuddy",     sAutoloadBase)
fnRipSheet("BeeGirl",      sMonstersCH1Path .. "Beegirl_A.png",    false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh1|BeeGirl",      sAutoloadBase)
fnRipSheet("BeeGirlZ",     sMonstersCH1Path .. "Beegirl_Z.png",    false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh1|BeeGirlZ",     sAutoloadBase)
fnRipSheet("BloodyMirror", sMonstersCH1Path .. "BloodyMirror.png", false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh1|BloodyMirror", sAutoloadBase)
fnRipSheet("CandleHaunt",  sMonstersCH1Path .. "CandleHaunt.png",  false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh1|CandleHaunt",  sAutoloadBase)
fnRipSheet("EyeDrawer",    sMonstersCH1Path .. "EyeDrawer.png",    false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh1|EyeDrawer",    sAutoloadBase)
fnRipSheet("Gravemarker",  sMonstersCH1Path .. "Gravemarker.png",  false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh1|Gravemarker",  sAutoloadBase)
fnRipSheet("InkSlime",     sMonstersCH1Path .. "InkSlime.png",     false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh1|InkSlime",     sAutoloadBase)
fnRipSheet("Jelli",        sMonstersCH1Path .. "Jelli.png",        false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh1|Jelli",        sAutoloadBase)
fnRipSheet("MaidGhost",    sMonstersCH1Path .. "MaidGhost.png",    false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh1|MaidGhost",    sAutoloadBase)
fnRipSheet("MannBanditF",  sMonstersCH1Path .. "MannBanditF.png",  false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh1|MannBanditF",  sAutoloadBase)
fnRipSheet("MannBanditM",  sMonstersCH1Path .. "MannBanditM.png",  false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh1|MannBanditM",  sAutoloadBase)
fnRipSheet("Mushraune",    sMonstersCH1Path .. "Mushraune.png",    false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh1|Mushraune",    sAutoloadBase)
fnRipSheet("Rilmani",      sMonstersCH1Path .. "Rilmani.png",      false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh1|Rilmani",      sAutoloadBase)
fnRipSheet("Slime",        sMonstersCH1Path .. "Slime.png",        false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh1|Slime",        sAutoloadBase)
fnRipSheet("SlimeB",       sMonstersCH1Path .. "SlimeBlue.png",    false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh1|SlimeB",       sAutoloadBase)
fnRipSheet("SlimeG",       sMonstersCH1Path .. "SlimeGreen.png",   false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh1|SlimeG",       sAutoloadBase)
fnRipSheet("Sproutling",   sMonstersCH1Path .. "Sproutling.png",   false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh1|Sproutling",   sAutoloadBase)
fnRipSheet("Sproutpecker", sMonstersCH1Path .. "Sproutpecker.png", false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh1|Sproutpecker", sAutoloadBase)
fnRipSheet("Werecat",      sMonstersCH1Path .. "Werecat.png",      false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh1|Werecat",      sAutoloadBase)
fnRipSheet("WerecatThief", sMonstersCH1Path .. "WerecatThief.png", false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh1|WerecatThief", sAutoloadBase)

--Rubber variants.
fnRipSheet("AlrauneR",      sMonstersCH1Path .. "AlrauneR.png",      false, false, false, true,  true, "AutoLoad|Adventure|SpritesEnemiesCh1|AlrauneR",      sAutoloadBase)
fnRipSheet("BeeGirlR",      sMonstersCH1Path .. "BeegirlR.png",      false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh1|BeeGirlR",      sAutoloadBase)
fnRipSheet("WerecatR",      sMonstersCH1Path .. "WerecatR.png",      false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh1|WerecatR",      sAutoloadBase)
fnRipSheet("WerecatThiefR", sMonstersCH1Path .. "WerecatThiefR.png", false, false, false, false, true, "AutoLoad|Adventure|SpritesEnemiesCh1|WerecatThiefR", sAutoloadBase)

--Special Frames
ImageLump_Rip("Spcl|Werecat|Slump",  sMonstersCH1Path .. "Werecat.png",   0, 200, 32, 40, 0)
ImageLump_Rip("Spcl|Werecat|Drink0", sMonstersCH1Path .. "Werecat.png",  32, 200, 32, 40, 0)
ImageLump_Rip("Spcl|Werecat|Drink1", sMonstersCH1Path .. "Werecat.png",  64, 200, 32, 40, 0)
ImageLump_Rip("Spcl|Werecat|Drink2", sMonstersCH1Path .. "Werecat.png",  96, 200, 32, 40, 0)

-- |[Paragon]|
fnRipSheet("Alraune"      .. "Paragon", sMonstersCH1PathParagon .. "Alraune.png",      false, false, false, true,  true, "AutoLoad|Adventure|SpritesParagonsCh1|Alraune",      sAutoloadBase)
fnRipSheet("BeeBuddy"     .. "Paragon", sMonstersCH1PathParagon .. "BeeBuddy.png",     false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh1|BeeBuddy",     sAutoloadBase)
fnRipSheet("BeeGirl"      .. "Paragon", sMonstersCH1PathParagon .. "Beegirl_A.png",    false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh1|BeeGirl",      sAutoloadBase)
fnRipSheet("BeeGirlZ"     .. "Paragon", sMonstersCH1PathParagon .. "Beegirl_Z.png",    false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh1|BeeGirlZ",     sAutoloadBase)
fnRipSheet("BloodyMirror" .. "Paragon", sMonstersCH1PathParagon .. "BloodyMirror.png", false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh1|BloodyMirror", sAutoloadBase)
fnRipSheet("CandleHaunt"  .. "Paragon", sMonstersCH1PathParagon .. "CandleHaunt.png",  false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh1|CandleHaunt",  sAutoloadBase)
fnRipSheet("EyeDrawer"    .. "Paragon", sMonstersCH1PathParagon .. "EyeDrawer.png",    false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh1|EyeDrawer",    sAutoloadBase)
fnRipSheet("Gravemarker"  .. "Paragon", sMonstersCH1PathParagon .. "Gravemarker.png",  false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh1|Gravemarker",  sAutoloadBase)
fnRipSheet("InkSlime"     .. "Paragon", sMonstersCH1PathParagon .. "InkSlime.png",     false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh1|InkSlime",     sAutoloadBase)
fnRipSheet("Jelli"        .. "Paragon", sMonstersCH1PathParagon .. "Jelli.png",        false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh1|Jelli",        sAutoloadBase)
fnRipSheet("MaidGhost"    .. "Paragon", sMonstersCH1PathParagon .. "MaidGhost.png",    false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh1|MaidGhost",    sAutoloadBase)
fnRipSheet("MannBanditF"  .. "Paragon", sMonstersCH1PathParagon .. "MannBanditF.png",  false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh1|MannBanditF",  sAutoloadBase)
fnRipSheet("MannBanditM"  .. "Paragon", sMonstersCH1PathParagon .. "MannBanditM.png",  false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh1|MannBanditM",  sAutoloadBase)
fnRipSheet("Mushraune"    .. "Paragon", sMonstersCH1PathParagon .. "Mushraune.png",    false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh1|Mushraune",    sAutoloadBase)
fnRipSheet("Rilmani"      .. "Paragon", sMonstersCH1PathParagon .. "Rilmani.png",      false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh1|Rilmani",      sAutoloadBase)
fnRipSheet("Slime"        .. "Paragon", sMonstersCH1PathParagon .. "Slime.png",        false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh1|Slime",        sAutoloadBase)
fnRipSheet("SlimeB"       .. "Paragon", sMonstersCH1PathParagon .. "SlimeBlue.png",    false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh1|SlimeB",       sAutoloadBase)
fnRipSheet("SlimeG"       .. "Paragon", sMonstersCH1PathParagon .. "SlimeGreen.png",   false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh1|SlimeG",       sAutoloadBase)
fnRipSheet("Sproutling"   .. "Paragon", sMonstersCH1PathParagon .. "Sproutling.png",   false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh1|Sproutling",   sAutoloadBase)
fnRipSheet("Sproutpecker" .. "Paragon", sMonstersCH1PathParagon .. "Sproutpecker.png", false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh1|Sproutpecker", sAutoloadBase)
fnRipSheet("Werecat"      .. "Paragon", sMonstersCH1PathParagon .. "Werecat.png",      false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh1|Werecat",      sAutoloadBase)
fnRipSheet("WerecatThief" .. "Paragon", sMonstersCH1PathParagon .. "WerecatThief.png", false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh1|WerecatThief", sAutoloadBase)

--Rubber.
fnRipSheet("AlrauneR"      .. "Paragon", sMonstersCH1PathParagon .. "AlrauneR.png",      false, false, false, true,  true, "AutoLoad|Adventure|SpritesParagonsCh1|AlrauneR",      sAutoloadBase)
fnRipSheet("BeeGirlR"      .. "Paragon", sMonstersCH1PathParagon .. "BeegirlR.png",      false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh1|BeeGirlR",      sAutoloadBase)
fnRipSheet("WerecatR"      .. "Paragon", sMonstersCH1PathParagon .. "WerecatR.png",      false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh1|WerecatR",      sAutoloadBase)
fnRipSheet("WerecatThiefR" .. "Paragon", sMonstersCH1PathParagon .. "WerecatThiefR.png", false, false, false, false, true, "AutoLoad|Adventure|SpritesParagonsCh1|WerecatThiefR", sAutoloadBase)

-- |[ ============================= Chapter 2 ============================== ]|
-- |[Setup]|
local sMonstersCH2Path        = sBasePath .. "MonstersCH2/"
local sMonstersCH2PathParagon = sBasePath .. "ParagonsCH2/"

-- |[Autoloader]|
local sAutoloaderName = "AutoLoad|Adventure|Sprite_Ch2_Enemy"
local sAutoloaderPath = "Root/Images/Sprites/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[Normal]|
local saMon2List = {"BanditA", "BanditB", "Bat", "Brimhog", "Buffodil", "BunEye", "Bunny", "Butterbomber", "CaveCrawler", "Enforcer", "FrostCrawler", "Grub", "Grubhub", "HarpyOfficer", "HarpyRecruit", 
                    "Kitsune", "Omnigoose", "PoisonVine", "Pseudogriffon", "Redcap", "Sevavi", "SuckFly", "Toxishroom", "Treant", "Turkerus", "Unielk"}
for i = 1, #saMon2List, 1 do
    
    --Resolve names.
    local sAutoName = "AutoLoad|Adventure|SpritesEnemiesCh2|"..saMon2List[i]
    
    --Rip the sheet.
    fnRipSheet(saMon2List[i], sMonstersCH2Path .. saMon2List[i] .. ".png", false, false, false, false, true, sAutoName, sAutoloaderPath)
    
    --Duplicate the sheet onto the general enemy loader.
    fnCloneAuto(sAutoName, sAutoloaderName)
end

-- |[Paragon]|
for i = 1, #saMon2List, 1 do
    
    --Resolve names.
    local sAutoName = "AutoLoad|Adventure|SpritesParagonsCh2|"..saMon2List[i]
    
    --Rip the sheet.
    fnRipSheet(saMon2List[i] .. "Paragon", sMonstersCH2PathParagon .. saMon2List[i] .. ".png", false, false, false, false, true, sAutoName, sAutoloaderPath)

    --Duplicate the sheet onto the general enemy loader.
    fnCloneAuto(sAutoName, sAutoloaderName)
end

-- |[Special]|
fnRipImageAuto("Spcl|Sevavi|Crouch", sMonstersCH2Path .. "Sevavi.png", 32, 0, 32, 40, 0, "AutoLoad|Adventure|SpritesEnemiesCh2|Sevavi", "Root/Images/Sprites/Special/Sevavi|Crouch")
AutoLoader_Register(sAutoloaderName, "Spcl|Sevavi|Crouch", "Root/Images/Sprites/Special/Sevavi|Crouch")

-- |[ ============================= Chapter 3 ============================== ]|
-- |[Setup]|
local sMonstersCH3Path        = sBasePath .. "MonstersCH3/"
local sMonstersCH3PathParagon = sBasePath .. "ParagonsCH3/"

-- |[Normal]|
fnRipSheet("Imp", sMonstersCH3Path .. "Imp.png", false, false, false)

-- |[Paragon]|
-- |[ ============================= Chapter 4 ============================== ]|
-- |[Setup]|
local sMonstersCH4Path        = sBasePath .. "MonstersCH4/"
local sMonstersCH4PathParagon = sBasePath .. "ParagonsCH4/"

-- |[Normal]|
-- |[Paragon]|
-- |[ ============================= Chapter 5 ============================== ]|
-- |[Setup]|
local sMonstersCH5Path        = sBasePath .. "MonstersCH5/"
local sMonstersCH5PathParagon = sBasePath .. "ParagonsCH5/"

cbEightDir   = true
cbFourDir    = false
cbIsWide     = true
cbIsNotWide  = false
cbHasRunAnim = true
cbNoRunAnim  = false
cbHasCrouch  = true
cbNoCrouch   = false
cbHasWound   = true
cbNoWound    = false

-- |[Normal]|
--Standard sheets.
fnRipSheet("BandageImp",       sMonstersCH5Path .. "BandageImp.png",        cbFourDir,  cbIsNotWide, cbNoRunAnim, cbNoCrouch,  cbHasWound, "AutoLoad|Adventure|SpritesEnemiesCh5|BandageImp",       sAutoloadBase)
fnRipSheet("DarkmatterGirl",   sMonstersCH5Path .. "DarkMatterGirl.png",    cbEightDir, cbIsNotWide, cbNoRunAnim, cbHasCrouch, cbHasWound, "AutoLoad|Adventure|SpritesEnemiesCh5|DarkmatterGirl",   sAutoloadBase)
fnRipSheet("Doll",             sMonstersCH5Path .. "Doll.png",              cbFourDir,  cbIsNotWide, cbNoRunAnim, cbNoCrouch,  cbHasWound, "AutoLoad|Adventure|SpritesEnemiesCh5|Doll",             sAutoloadBase)
fnRipSheet("DollGrn",          sMonstersCH5Path .. "DollGrn.png",           cbFourDir,  cbIsNotWide, cbNoRunAnim, cbNoCrouch,  cbNoWound,  "AutoLoad|Adventure|SpritesEnemiesCh5|DollGrn",          sAutoloadBase)
fnRipSheet("DollPrp",          sMonstersCH5Path .. "DollPrp.png",           cbFourDir,  cbIsNotWide, cbNoRunAnim, cbNoCrouch,  cbNoWound,  "AutoLoad|Adventure|SpritesEnemiesCh5|DollPrp",          sAutoloadBase)
fnRipSheet("DollInfluenced",   sMonstersCH5Path .. "DollInfluenced.png",    cbFourDir,  cbIsNotWide, cbNoRunAnim, cbNoCrouch,  cbNoWound,  "AutoLoad|Adventure|SpritesEnemiesCh5|DollInfluenced",   sAutoloadBase)
fnRipSheet("EldritchDream",    sMonstersCH5Path .. "EldritchDream.png",     cbFourDir,  cbIsNotWide, cbNoRunAnim, cbNoCrouch,  cbHasWound, "AutoLoad|Adventure|SpritesEnemiesCh5|EldritchDream",    sAutoloadBase)
fnRipSheet("Electrosprite",    sMonstersCH5Path .. "Electrosprite.png",     cbFourDir,  cbIsNotWide, cbNoRunAnim, cbNoCrouch,  cbNoWound,  "AutoLoad|Adventure|SpritesEnemiesCh5|Electrosprite",    sAutoloadBase)
fnRipSheet("GolemLordA",       sMonstersCH5Path .. "Golem_LordA.png",       cbFourDir,  cbIsNotWide, cbNoRunAnim, cbNoCrouch,  cbHasWound, "AutoLoad|Adventure|SpritesEnemiesCh5|GolemLordA",       sAutoloadBase)
fnRipSheet("GolemLordB",       sMonstersCH5Path .. "Golem_LordB.png",       cbFourDir,  cbIsNotWide, cbNoRunAnim, cbNoCrouch,  cbNoWound,  "AutoLoad|Adventure|SpritesEnemiesCh5|GolemLordB",       sAutoloadBase)
fnRipSheet("GolemLordC",       sMonstersCH5Path .. "Golem_LordC.png",       cbFourDir,  cbIsNotWide, cbNoRunAnim, cbNoCrouch,  cbNoWound,  "AutoLoad|Adventure|SpritesEnemiesCh5|GolemLordC",       sAutoloadBase)
fnRipSheet("GolemLordD",       sMonstersCH5Path .. "Golem_LordD.png",       cbFourDir,  cbIsNotWide, cbNoRunAnim, cbNoCrouch,  cbNoWound,  "AutoLoad|Adventure|SpritesEnemiesCh5|GolemLordD",       sAutoloadBase)
fnRipSheet("GolemSlaveP",      sMonstersCH5Path .. "Golem_SlavePack.png",   cbFourDir,  cbIsNotWide, cbNoRunAnim, cbNoCrouch,  cbHasWound, "AutoLoad|Adventure|SpritesEnemiesCh5|GolemSlaveP",      sAutoloadBase)
fnRipSheet("GolemSlave",       sMonstersCH5Path .. "Golem_SlaveNoPack.png", cbFourDir,  cbIsNotWide, cbNoRunAnim, cbNoCrouch,  cbHasWound, "AutoLoad|Adventure|SpritesEnemiesCh5|GolemSlave",       sAutoloadBase)
fnRipSheet("GolemSlaveR",      sMonstersCH5Path .. "Golem_SlaveRebel.png",  cbFourDir,  cbIsNotWide, cbNoRunAnim, cbNoCrouch,  cbNoWound,  "AutoLoad|Adventure|SpritesEnemiesCh5|GolemSlaveR",      sAutoloadBase)
fnRipSheet("Hoodie",           sMonstersCH5Path .. "Hoodie.png",            cbFourDir,  cbIsNotWide, cbNoRunAnim, cbNoCrouch,  cbHasWound, "AutoLoad|Adventure|SpritesEnemiesCh5|Hoodie",           sAutoloadBase)
fnRipSheet("Horrible",         sMonstersCH5Path .. "Horrible.png",          cbFourDir,  cbIsNotWide, cbNoRunAnim, cbNoCrouch,  cbHasWound, "AutoLoad|Adventure|SpritesEnemiesCh5|Horrible",         sAutoloadBase)
fnRipSheet("InnGeisha",        sMonstersCH5Path .. "InnGeisha.png",         cbFourDir,  cbIsNotWide, cbNoRunAnim, cbNoCrouch,  cbHasWound, "AutoLoad|Adventure|SpritesEnemiesCh5|InnGeisha",        sAutoloadBase)
fnRipSheet("LatexDrone",       sMonstersCH5Path .. "LatexDrone.png",        cbFourDir,  cbIsNotWide, cbNoRunAnim, cbNoCrouch,  cbHasWound, "AutoLoad|Adventure|SpritesEnemiesCh5|LatexDrone",       sAutoloadBase)
fnRipSheet("Motilvac",         sMonstersCH5Path .. "Motilvac.png",          cbFourDir,  cbIsNotWide, cbNoRunAnim, cbNoCrouch,  cbHasWound, "AutoLoad|Adventure|SpritesEnemiesCh5|Motilvac",         sAutoloadBase)
fnRipSheet("MrWipey",          sMonstersCH5Path .. "MrWipey.png",           cbFourDir,  cbIsNotWide, cbNoRunAnim, cbNoCrouch,  cbHasWound, "AutoLoad|Adventure|SpritesEnemiesCh5|MrWipey",          sAutoloadBase)
fnRipSheet("Plannerbot",       sMonstersCH5Path .. "Plannerbot.png",        cbFourDir,  cbIsNotWide, cbNoRunAnim, cbNoCrouch,  cbHasWound, "AutoLoad|Adventure|SpritesEnemiesCh5|Plannerbot",       sAutoloadBase)
fnRipSheet("Raibie",           sMonstersCH5Path .. "Raibie.png",            cbFourDir,  cbIsNotWide, cbNoRunAnim, cbNoCrouch,  cbHasWound, "AutoLoad|Adventure|SpritesEnemiesCh5|Raibie",           sAutoloadBase)
fnRipSheet("Raiju",            sMonstersCH5Path .. "Raiju.png",             cbFourDir,  cbIsNotWide, cbNoRunAnim, cbNoCrouch,  cbHasWound, "AutoLoad|Adventure|SpritesEnemiesCh5|Raiju",            sAutoloadBase)
fnRipSheet("Scraprat",         sMonstersCH5Path .. "Scraprat.png",          cbFourDir,  cbIsNotWide, cbNoRunAnim, cbNoCrouch,  cbHasWound, "AutoLoad|Adventure|SpritesEnemiesCh5|Scraprat",         sAutoloadBase)
fnRipSheet("Secrebot",         sMonstersCH5Path .. "Secrebot.png",          cbFourDir,  cbIsNotWide, cbNoRunAnim, cbNoCrouch,  cbHasWound, "AutoLoad|Adventure|SpritesEnemiesCh5|Secrebot",         sAutoloadBase)
fnRipSheet("SecrebotPink",     sMonstersCH5Path .. "SecrebotPink.png",      cbFourDir,  cbIsNotWide, cbNoRunAnim, cbNoCrouch,  cbHasWound, "AutoLoad|Adventure|SpritesEnemiesCh5|SecrebotPink",     sAutoloadBase)
fnRipSheet("SecrebotYellow",   sMonstersCH5Path .. "SecrebotYellow.png",    cbFourDir,  cbIsNotWide, cbNoRunAnim, cbNoCrouch,  cbHasWound, "AutoLoad|Adventure|SpritesEnemiesCh5|SecrebotYellow",   sAutoloadBase)
fnRipSheet("SecurityBot",      sMonstersCH5Path .. "SecurityBot.png",       cbFourDir,  cbIsNotWide, cbNoRunAnim, cbNoCrouch,  cbHasWound, "AutoLoad|Adventure|SpritesEnemiesCh5|SecurityBot",      sAutoloadBase)
fnRipSheet("SecurityBotBroke", sMonstersCH5Path .. "SecurityBotBroke.png",  cbFourDir,  cbIsNotWide, cbNoRunAnim, cbNoCrouch,  cbHasWound, "AutoLoad|Adventure|SpritesEnemiesCh5|SecurityBotBroke", sAutoloadBase)
fnRipSheet("SteamDroid",       sMonstersCH5Path .. "SteamDroid.png",        cbFourDir,  cbIsNotWide, cbNoRunAnim, cbNoCrouch,  cbNoWound,  "AutoLoad|Adventure|SpritesEnemiesCh5|SteamDroid",       sAutoloadBase)
fnRipSheet("VoidRift",         sMonstersCH5Path .. "VoidRift.png",          cbFourDir,  cbIsNotWide, cbNoRunAnim, cbNoCrouch,  cbHasWound, "AutoLoad|Adventure|SpritesEnemiesCh5|VoidRift",         sAutoloadBase)

--Latex Drones with hair!
fnRipSheet("LatexBlonde", sMonstersCH5Path .. "LatexDrone_Blonde.png", cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbNoWound, "AutoLoad|Adventure|SpritesEnemiesCh5|LatexBlonde", sAutoloadBase)
fnRipSheet("LatexBrown",  sMonstersCH5Path .. "LatexDrone_Brown.png",  cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbNoWound, "AutoLoad|Adventure|SpritesEnemiesCh5|LatexBrown",  sAutoloadBase)
fnRipSheet("LatexRed",    sMonstersCH5Path .. "LatexDrone_Red.png",    cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbNoWound, "AutoLoad|Adventure|SpritesEnemiesCh5|LatexRed",    sAutoloadBase)

--Latex Drone's arm falls off.
ImageLump_Rip("Spcl|LatexDrone|Arm0",    sMonstersCH5Path .. "LatexDrone.png",  0, 200, 32, 40, 0)
ImageLump_Rip("Spcl|LatexDrone|Arm1",    sMonstersCH5Path .. "LatexDrone.png", 32, 200, 32, 40, 0)
ImageLump_Rip("Spcl|LatexDrone|Arm2",    sMonstersCH5Path .. "LatexDrone.png", 64, 200, 32, 40, 0)
ImageLump_Rip("Spcl|LatexDrone|Arm3",    sMonstersCH5Path .. "LatexDrone.png", 96, 200, 32, 40, 0)
ImageLump_Rip("Spcl|LatexDrone|Arm4",    sMonstersCH5Path .. "LatexDrone.png",  0, 240, 32, 40, 0)

--Raiju Workout
ImageLump_Rip("Spcl|RaijuWorkout|Kick0", sMonstersCH5Path .. "Raiju.png",  0, 200, 32, 40, 0)
ImageLump_Rip("Spcl|RaijuWorkout|Kick1", sMonstersCH5Path .. "Raiju.png", 32, 200, 32, 40, 0)
ImageLump_Rip("Spcl|RaijuWorkout|Kick2", sMonstersCH5Path .. "Raiju.png", 64, 200, 32, 40, 0)
ImageLump_Rip("Spcl|RaijuWorkout|Kick3", sMonstersCH5Path .. "Raiju.png", 96, 200, 32, 40, 0)

--Raiju Dance
ImageLump_Rip("Spcl|RaijuWorkout|Dance0", sMonstersCH5Path .. "Raiju.png",  0, 240, 32, 40, 0)
ImageLump_Rip("Spcl|RaijuWorkout|Dance1", sMonstersCH5Path .. "Raiju.png", 32, 240, 32, 40, 0)
ImageLump_Rip("Spcl|RaijuWorkout|Dance2", sMonstersCH5Path .. "Raiju.png", 64, 240, 32, 40, 0)
ImageLump_Rip("Spcl|RaijuWorkout|Dance3", sMonstersCH5Path .. "Raiju.png", 96, 240, 32, 40, 0)

--Other special frames.
ImageLump_Rip("Spcl|GolemSlave|Mutant",    sMonstersCH5Path .. "Golem_SlaveNoPack.png", 32, 200, 32, 40, 0)
ImageLump_Rip("Spcl|GolemSlaveR|Wounded",  sMonstersCH5Path .. "Golem_SlaveRebel.png",   0, 200, 32, 40, 0)
ImageLump_Rip("Spcl|GolemSlaveR|Sitting",  sMonstersCH5Path .. "Golem_SlaveRebel.png",  32, 200, 32, 40, 0)

--Scraprat-splosion!
for i = 0, 11, 1 do
	local sName = "Spcl|Scraprat|Explode"
	if(i < 10) then sName = sName .. "0" end
	sName = sName .. i
	ImageLump_Rip(sName, sMonstersCH5Path .. "Scraprat.png", i * 32, 200, 32, 64, 0)
end

--Golem Explosion
for i = 0, 9, 1 do
    ImageLump_Rip("Spcl|GolemSlave|Explode" .. i, sMonstersCH5Path .. "Golem_SlaveNoPack.png", 0 + (i*32), 240, 32, 40, 0)
end

-- |[Paragon]|
fnRipSheet("BandageImp"       .. "Paragon", sMonstersCH5PathParagon .. "BandageImp.png",        false, false, false, false, true)
fnRipSheet("DarkmatterGirl"   .. "Paragon", sMonstersCH5PathParagon .. "DarkMatterGirl.png",    true,  false, false, true,  true) --Uses 8-way movement for some cutscenes.
fnRipSheet("Doll"             .. "Paragon", sMonstersCH5PathParagon .. "Doll.png",              false, false, false, false, true)
fnRipSheet("DollInfluenced"   .. "Paragon", sMonstersCH5PathParagon .. "DollInfluenced.png",    false, false, false)
fnRipSheet("EldritchDream"    .. "Paragon", sMonstersCH5PathParagon .. "EldritchDream.png",     false, false, false, false, true)
fnRipSheet("GolemLordA"       .. "Paragon", sMonstersCH5PathParagon .. "Golem_LordA.png",       false, false, false, false, true)
fnRipSheet("GolemSlaveP"      .. "Paragon", sMonstersCH5PathParagon .. "Golem_SlavePack.png",   false, false, false, false, true)
fnRipSheet("GolemSlave"       .. "Paragon", sMonstersCH5PathParagon .. "Golem_SlaveNoPack.png", false, false, false, false, true)
fnRipSheet("Hoodie"           .. "Paragon", sMonstersCH5PathParagon .. "Hoodie.png",            false, false, false, false, true)
fnRipSheet("Horrible"         .. "Paragon", sMonstersCH5PathParagon .. "Horrible.png",          false, false, false, false, true)
fnRipSheet("InnGeisha"        .. "Paragon", sMonstersCH5PathParagon .. "InnGeisha.png",         false, false, false, false, true)
fnRipSheet("LatexDrone"       .. "Paragon", sMonstersCH5PathParagon .. "LatexDrone.png",        false, false, false, false, true)
fnRipSheet("Motilvac"         .. "Paragon", sMonstersCH5PathParagon .. "Motilvac.png",          false, false, false, false, true)
fnRipSheet("MrWipey"          .. "Paragon", sMonstersCH5PathParagon .. "MrWipey.png",           false, false, false, false, true)
fnRipSheet("Plannerbot"       .. "Paragon", sMonstersCH5PathParagon .. "Plannerbot.png",        false, false, false, false, true)
fnRipSheet("Raibie"           .. "Paragon", sMonstersCH5PathParagon .. "Raibie.png",            false, false, false, false, true)
fnRipSheet("Scraprat"         .. "Paragon", sMonstersCH5PathParagon .. "Scraprat.png",          false, false, false, false, true)
fnRipSheet("Secrebot"         .. "Paragon", sMonstersCH5PathParagon .. "Secrebot.png",          false, false, false, false, true)
fnRipSheet("SecrebotPink"     .. "Paragon", sMonstersCH5PathParagon .. "SecrebotPink.png",      false, false, false, false, true)
fnRipSheet("SecrebotYellow"   .. "Paragon", sMonstersCH5PathParagon .. "SecrebotYellow.png",    false, false, false, false, true)
fnRipSheet("SecurityBot"      .. "Paragon", sMonstersCH5PathParagon .. "SecurityBot.png",       false, false, false, false, true)
fnRipSheet("SecurityBotBroke" .. "Paragon", sMonstersCH5PathParagon .. "SecurityBotBroke.png",  false, false, false, false, true)
fnRipSheet("VoidRift"         .. "Paragon", sMonstersCH5PathParagon .. "VoidRift.png",          false, false, false, false, true)

-- |[ ============================= Chapter 6 ============================== ]|
-- |[Setup]|
local sMonstersCH6Path        = sBasePath .. "MonstersCH6/"
local sMonstersCH6PathParagon = sBasePath .. "ParagonsCH6/"

-- |[Normal]|
-- |[Paragon]|
