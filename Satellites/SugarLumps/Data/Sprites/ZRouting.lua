-- |[ ========================================= Sprites ======================================== ]|
--Spritesheets used for characters in Adventure Mode. Sheets use a standardized format.
local sBasePath = fnResolvePath()

--Setup
SLF_Open("Output/Sprites.slf")
ImageLump_SetCompression(1)

-- |[ ====================================== Autoloaders ======================================= ]|
SLF_RegisterAutoLoadLump("AutoLoad|Adventure|SystemSprites_Padded")   --General-use sprites that have special padding to match entity positions
SLF_RegisterAutoLoadLump("AutoLoad|Adventure|SystemSprites_Unpadded") --All other general-use sprites.
SLF_RegisterAutoLoadLump("AutoLoad|Adventure|Sprites_Ch1")
SLF_RegisterAutoLoadLump("AutoLoad|Adventure|Sprites_Ch2")
SLF_RegisterAutoLoadLump("AutoLoad|Adventure|Sprites_Ch3")
SLF_RegisterAutoLoadLump("AutoLoad|Adventure|Sprites_Ch4")
SLF_RegisterAutoLoadLump("AutoLoad|Adventure|Sprites_Ch6")
SLF_RegisterAutoLoadLump("AutoLoad|Adventure|Sprites_Ch5")

-- |[ ======================================== Subfiles ======================================== ]|
LM_ExecuteScript(sBasePath .. "00 Party Members"    .. "/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "01 Major Characters" .. "/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "02 Minor Characters" .. "/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "03 Generics"         .. "/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "04 Monsters"         .. "/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "10 Animations"       .. "/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "11 Dancing Minigame" .. "/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "12 Running Minigame" .. "/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "20 RNG Maps"         .. "/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "99 System"           .. "/ZRouting.lua")

-- |[ =================================== Title Autoloading ==================================== ]|
--These sprites appear on the title screen during file loading. This occurs even if the character isn't
-- in the current game (such as Witch Hunter Izana) because savefiles don't discriminate.
SLF_RegisterAutoLoadLump("AutoLoad|Adventure|Sprites_Title")

--Variables.
local sSpriteSuffix = "|SW|0"
local sDLPath = "Root/Images/GUI/LoadingImg/"
local saChapterList = {} --Pattern: {"Sprite_Name", "Class_Name"}

--Order Autoloader to modify sprite flags.
AutoLoader_Register("AutoLoad|Adventure|Sprites_Title", "MAGFILTER NEAREST", "Null")
AutoLoader_Register("AutoLoad|Adventure|Sprites_Title", "MINFILTER NEAREST", "Null")

-- |[Setup]|
saChapterList = { 

-- |[Chapter 1]|
--Mei
{"Mei_Human", "Mei_Fencer"},         {"Mei_Alraune", "Mei_Nightshade"},    {"Mei_Bee", "Mei_Hive Scout"}, {"Mei_Slime", "Mei_Smarty Sage"}, {"Mei_Werecat", "Mei_Prowler"},  {"Mei_Ghost", "Mei_Maid"}, 
{"Mei_Gravemarker", "Mei_Petraian"}, {"Mei_Rubber", "Mei_Squeaky Thrall"}, {"Mei_Bee_MC", "Mei_Zombee"},  {"Mei_Mannequin", "Mei_Stalker"}, {"Mei_Wisphag", "Mei_Soulherd"}, 

--Florentina
{"Florentina", "Florentina_Merchant"}, {"FlorentinaMed", "Florentina_Mediator"}, {"FlorentinaTH", "Florentina_TreasureHunter"}, {"FlorentinaAgarist", "Florentina_Agarist"}, {"FlorentinaLurker", "Florentina_Lurker"},

-- |[Chapter 2]|
--Sanya
{"Sanya_Human", "Sanya_Hunter"}, {"Sanya_Sevavi", "Sanya_Agontea"}, {"Sanya_Werebat_Rifle", "Sanya_Skystriker"}, {"Sanya_Harpy_Rifle", "Sanya_Ranger"}, {"Sanya_Bunny_Rifle", "Sanya_Scofflaw"},

--Izuna
{"Izuna_Miko", "Izuna_Miko"},

--Zeke
{"Zeke_Goat", "Zeke_Goat"},

--Empress
{"Empress_Conquerer", "Empress_Conquerer"},

-- |[Chapter 3]|
-- |[Chapter 4]|
-- |[Chapter 5]|
--Christine
{"Christine_Human", "Christine_Lancer"},  {"Christine_Doll",  "Christine_Commissar"},   {"Christine_Darkmatter",    "Christine_Starseer"},  {"Christine_SteamDroid", "Christine_Brass Brawler"},
{"Christine_Latex", "Christine_Drone"},   {"Christine_Golem", "Christine_Repair Unit"}, {"Christine_Electrosprite", "Christine_Live Wire"}, {"Christine_DreamGirl",  "Christine_Handmaiden"},
{"Christine_Male",  "Christine_Teacher"}, {"Christine_Raiju", "Christine_Thunder Rat"}, {"Christine_Secrebot",      "Christine_Menialist"},

--55
{"Tiffany", "Tiffany_Assault"}, {"Tiffany", "Tiffany_Support"}, {"Tiffany", "Tiffany_Subvert"}, {"Tiffany", "Tiffany_Spearhead"},

--JX-101
{"JX-101", "JX-101_Tactician"},

--SX-399
{"SX-399Lord", "SX-399_Shocktrooper"}

-- |[Chapter 6]|
-- |[Finish]|
}

-- |[Run]|
for i = 1, #saChapterList, 1 do
    AutoLoader_Register("AutoLoad|Adventure|Sprites_Title", saChapterList[i][1] .. sSpriteSuffix, sDLPath .. saChapterList[i][2])
end

--Order Autoloader to clean sprite flags.
AutoLoader_Register("AutoLoad|Adventure|Sprites_Title", "RESET FLAGS", "Null")
    
-- |[ ======================================== Finish Up ======================================= ]|
--Finish
SLF_Close()
