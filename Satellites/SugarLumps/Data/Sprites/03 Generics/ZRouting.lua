-- |[ ====================================== Generic NPCs ====================================== ]|
--Sprites for generic characters. Characters who don't have specific names or appear in crowds.
local sBasePath = fnResolvePath()
local sAutoloadBase = "Root/Images/Sprites/"

-- |[ ======================================= Autoloaders ====================================== ]|
--Each character in this group has an autoloader lump associated with it. Chapters then append these
-- lumps into their own to make one large autoloader lump.

-- |[ =============================== Animals ============================== ]|
--NPCs using 2-way movement
local sAnimalPath = sBasePath .. "Animals/"
fnRipSheetHalf("Chicken", sAnimalPath .. "Chicken.png", "AutoLoad|Adventure|SpritesGeneric|Chicken", "Root/Images/Sprites/")
fnRipSheetHalf("Sheep",   sAnimalPath .. "Sheep.png",   "AutoLoad|Adventure|SpritesGeneric|Sheep",   "Root/Images/Sprites/")

--4-way movement.
fnRipSheet("Corgi",     sAnimalPath .. "Corgi.png",     false, false, false, false, true, "AutoLoad|Adventure|SpritesGeneric|Corgi",    "Root/Images/Sprites/")
fnRipSheet("Horse",     sAnimalPath .. "Horse.png",     false, false, false, false, true, "AutoLoad|Adventure|SpritesGeneric|Horse",    "Root/Images/Sprites/")
fnRipSheet("Pack Mule", sAnimalPath .. "Pack Mule.png", false, false, false, false, true, "AutoLoad|Adventure|SpritesGeneric|PackMule", "Root/Images/Sprites/")
fnRipSheet("Wisp",      sAnimalPath .. "Wisp.png",      false, false, false, false, true, "AutoLoad|Adventure|SpritesGeneric|Wisp",     "Root/Images/Sprites/")

-- |[ ============================== Alraunes ============================== ]|
-- |[Sheets]|
local sAlraunePath = sBasePath .. "Alraunes/"
fnRipSheet("AlrauneGen0",  sAlraunePath .. "AlrauneGen0.png",  false, false, false, false, true, "AutoLoad|Adventure|SpritesGeneric|Alraune_Gen0",  sAutoloadBase)
fnRipSheet("AlrauneMerc0", sAlraunePath .. "AlrauneMerc0.png", false, false, false, false, true, "AutoLoad|Adventure|SpritesGeneric|Alraune_Merc0", sAutoloadBase)

-- |[ =============================== Humans =============================== ]|
-- |[Sheets]|
local sHumanPath = sBasePath .. "Humans/"
fnRipSheet("GenericF0", sHumanPath .. "Generic_NPC_F_0.png", false, false, false, false, true, "AutoLoad|Adventure|SpritesGeneric|NPC_F0",    sAutoloadBase)
fnRipSheet("GenericF1", sHumanPath .. "Generic_NPC_F_1.png", false, false, false, false, true, "AutoLoad|Adventure|SpritesGeneric|NPC_F1",    sAutoloadBase)
fnRipSheet("GenericF2", sHumanPath .. "Generic_NPC_F_2.png", false, false, false, false, true, "AutoLoad|Adventure|SpritesGeneric|NPC_F2",    sAutoloadBase)
fnRipSheet("GenericF3", sHumanPath .. "Generic_NPC_F_3.png", false, false, false, false, true, "AutoLoad|Adventure|SpritesGeneric|NPC_F3",    sAutoloadBase)
fnRipSheet("GenericF4", sHumanPath .. "Generic_NPC_F_4.png", false, false, false, false, true, "AutoLoad|Adventure|SpritesGeneric|NPC_F4",    sAutoloadBase)
fnRipSheet("GenericM0", sHumanPath .. "Generic_NPC_M_0.png", false, false, false, false, true, "AutoLoad|Adventure|SpritesGeneric|NPC_M0",    sAutoloadBase)
fnRipSheet("GenericM1", sHumanPath .. "Generic_NPC_M_1.png", false, false, false, false, true, "AutoLoad|Adventure|SpritesGeneric|NPC_M1",    sAutoloadBase)
fnRipSheet("GenericM2", sHumanPath .. "Generic_NPC_M_2.png", false, false, false, false, true, "AutoLoad|Adventure|SpritesGeneric|NPC_M2",    sAutoloadBase)
fnRipSheet("GenericM3", sHumanPath .. "Generic_NPC_M_3.png", false, false, false, false, true, "AutoLoad|Adventure|SpritesGeneric|NPC_M3",    sAutoloadBase)
fnRipSheet("GenericM4", sHumanPath .. "Generic_NPC_M_4.png", false, false, false, false, true, "AutoLoad|Adventure|SpritesGeneric|NPC_M4",    sAutoloadBase)
fnRipSheet("GenericM5", sHumanPath .. "Generic_NPC_M_5.png", false, false, false, false, true, "AutoLoad|Adventure|SpritesGeneric|NPC_M5",    sAutoloadBase)
fnRipSheet("MercF",     sHumanPath .. "Merc_Female.png",     false, false, false, false, true, "AutoLoad|Adventure|SpritesGeneric|NPC_MercF", sAutoloadBase)
fnRipSheet("MercM",     sHumanPath .. "Merc_Male.png",       false, false, false, false, true, "AutoLoad|Adventure|SpritesGeneric|NPC_MercM", sAutoloadBase)

-- |[Biolabs NPCs]|
--Biolabs NPCs. These use sheets on the same image.
gfOffsetX = 0
gfOffsetY = 0
fnRipSheet("GenericBiolabsFBlue",   sHumanPath .. "BiolabsNPCsF.png", false, false, false, "AutoLoad|Adventure|SpritesGeneric|NPC_BiolabsFBlue",   sAutoloadBase)
gfOffsetX = 128
fnRipSheet("GenericBiolabsFRed",    sHumanPath .. "BiolabsNPCsF.png", false, false, false, "AutoLoad|Adventure|SpritesGeneric|NPC_BiolabsFRed",    sAutoloadBase)
gfOffsetX = 256
fnRipSheet("GenericBiolabsFGreen",  sHumanPath .. "BiolabsNPCsF.png", false, false, false, "AutoLoad|Adventure|SpritesGeneric|NPC_BiolabsFGreen",  sAutoloadBase)
gfOffsetX = 384
fnRipSheet("GenericBiolabsFYellow", sHumanPath .. "BiolabsNPCsF.png", false, false, false, "AutoLoad|Adventure|SpritesGeneric|NPC_BiolabsFYellow", sAutoloadBase)

--Males in the Biolabs.
gfOffsetX = 0
fnRipSheet("GenericBiolabsMBlue",   sHumanPath .. "BiolabsNPCsM.png", false, false, false, "AutoLoad|Adventure|SpritesGeneric|NPC_BiolabsMBlue",   sAutoloadBase)
gfOffsetX = 128
fnRipSheet("GenericBiolabsMRed",    sHumanPath .. "BiolabsNPCsM.png", false, false, false, "AutoLoad|Adventure|SpritesGeneric|NPC_BiolabsMRed",    sAutoloadBase)
gfOffsetX = 256
fnRipSheet("GenericBiolabsMGreen",  sHumanPath .. "BiolabsNPCsM.png", false, false, false, "AutoLoad|Adventure|SpritesGeneric|NPC_BiolabsMGreen",  sAutoloadBase)
gfOffsetX = 384
fnRipSheet("GenericBiolabsMYellow", sHumanPath .. "BiolabsNPCsM.png", false, false, false, "AutoLoad|Adventure|SpritesGeneric|NPC_BiolabsMYellow", sAutoloadBase)

--Clean.
gfOffsetX = 0
gfOffsetY = 0

-- |[Special Frames]|
fnRipImageAuto("Spcl|GenericF0|Drink0",     sHumanPath .. "Generic_NPC_F_0.png",  0, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_F0",    "Root/Images/Sprites/Special/GenericF0|Drink0")
fnRipImageAuto("Spcl|GenericF0|Drink1",     sHumanPath .. "Generic_NPC_F_0.png", 32, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_F0",    "Root/Images/Sprites/Special/GenericF0|Drink1")
fnRipImageAuto("Spcl|GenericF0|Drink2",     sHumanPath .. "Generic_NPC_F_0.png", 64, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_F0",    "Root/Images/Sprites/Special/GenericF0|Drink2")
fnRipImageAuto("Spcl|GenericF0|Workout0",   sHumanPath .. "Generic_NPC_F_0.png",  0, 240, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_F0",    "Root/Images/Sprites/Special/GenericF0|Workout0")
fnRipImageAuto("Spcl|GenericF0|Workout1",   sHumanPath .. "Generic_NPC_F_0.png", 32, 240, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_F0",    "Root/Images/Sprites/Special/GenericF0|Workout1")
fnRipImageAuto("Spcl|GenericF0|Workout2",   sHumanPath .. "Generic_NPC_F_0.png", 64, 240, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_F0",    "Root/Images/Sprites/Special/GenericF0|Workout2")
fnRipImageAuto("Spcl|GenericF0|Workout3",   sHumanPath .. "Generic_NPC_F_0.png", 96, 240, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_F0",    "Root/Images/Sprites/Special/GenericF0|Workout3")
fnRipImageAuto("Spcl|GenericF0|Workout4",   sHumanPath .. "Generic_NPC_F_0.png",  0, 280, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_F0",    "Root/Images/Sprites/Special/GenericF0|Workout4")
fnRipImageAuto("Spcl|GenericF0|Workout5",   sHumanPath .. "Generic_NPC_F_0.png", 32, 280, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_F0",    "Root/Images/Sprites/Special/GenericF0|Workout5")
fnRipImageAuto("Spcl|GenericF0|Workout6",   sHumanPath .. "Generic_NPC_F_0.png", 64, 280, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_F0",    "Root/Images/Sprites/Special/GenericF0|Workout6")
fnRipImageAuto("Spcl|GenericF0|Workout7",   sHumanPath .. "Generic_NPC_F_0.png", 96, 280, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_F0",    "Root/Images/Sprites/Special/GenericF0|Workout7")
fnRipImageAuto("Spcl|GenericF1|WoundedSpc", sHumanPath .. "Generic_NPC_F_1.png", 32,   0, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_F1",    "Root/Images/Sprites/Special/GenericF1|WoundedSpc")
fnRipImageAuto("Spcl|GenericF1|Drink0",     sHumanPath .. "Generic_NPC_F_1.png",  0, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_F1",    "Root/Images/Sprites/Special/GenericF1|Drink0")
fnRipImageAuto("Spcl|GenericF1|Drink1",     sHumanPath .. "Generic_NPC_F_1.png", 32, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_F1",    "Root/Images/Sprites/Special/GenericF1|Drink1")
fnRipImageAuto("Spcl|GenericF1|Drink2",     sHumanPath .. "Generic_NPC_F_1.png", 64, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_F1",    "Root/Images/Sprites/Special/GenericF1|Drink2")
fnRipImageAuto("Spcl|GenericF3|Drink0",     sHumanPath .. "Generic_NPC_F_3.png",  0, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_F3",    "Root/Images/Sprites/Special/GenericF3|Drink0")
fnRipImageAuto("Spcl|GenericF3|Drink1",     sHumanPath .. "Generic_NPC_F_3.png", 32, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_F3",    "Root/Images/Sprites/Special/GenericF3|Drink1")
fnRipImageAuto("Spcl|GenericF3|Drink2",     sHumanPath .. "Generic_NPC_F_3.png", 64, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_F3",    "Root/Images/Sprites/Special/GenericF3|Drink2")
fnRipImageAuto("Spcl|GenericM0|Workout0",   sHumanPath .. "Generic_NPC_M_0.png",  0, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_M0",    "Root/Images/Sprites/Special/GenericM0|Workout0")
fnRipImageAuto("Spcl|GenericM0|Workout1",   sHumanPath .. "Generic_NPC_M_0.png", 32, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_M0",    "Root/Images/Sprites/Special/GenericM0|Workout1")
fnRipImageAuto("Spcl|GenericM0|Workout2",   sHumanPath .. "Generic_NPC_M_0.png", 64, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_M0",    "Root/Images/Sprites/Special/GenericM0|Workout2")
fnRipImageAuto("Spcl|GenericM0|Workout3",   sHumanPath .. "Generic_NPC_M_0.png", 96, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_M0",    "Root/Images/Sprites/Special/GenericM0|Workout3")
fnRipImageAuto("Spcl|GenericM0|Workout4",   sHumanPath .. "Generic_NPC_M_0.png",  0, 240, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_M0",    "Root/Images/Sprites/Special/GenericM0|Workout4")
fnRipImageAuto("Spcl|GenericM0|Workout5",   sHumanPath .. "Generic_NPC_M_0.png", 32, 240, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_M0",    "Root/Images/Sprites/Special/GenericM0|Workout5")
fnRipImageAuto("Spcl|GenericM0|Workout6",   sHumanPath .. "Generic_NPC_M_0.png", 64, 240, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_M0",    "Root/Images/Sprites/Special/GenericM0|Workout6")
fnRipImageAuto("Spcl|GenericM0|Workout7",   sHumanPath .. "Generic_NPC_M_0.png", 96, 240, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_M0",    "Root/Images/Sprites/Special/GenericM0|Workout7")
fnRipImageAuto("Spcl|MercM|Drink0",         sHumanPath .. "Merc_Male.png",        0, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_MercM", "Root/Images/Sprites/Special/MercM|Drink0")
fnRipImageAuto("Spcl|MercM|Drink1",         sHumanPath .. "Merc_Male.png",       32, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_MercM", "Root/Images/Sprites/Special/MercM|Drink1")
fnRipImageAuto("Spcl|MercM|Drink2",         sHumanPath .. "Merc_Male.png",       64, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_MercM", "Root/Images/Sprites/Special/MercM|Drink2")

-- |[ ============================== Kitsunes ============================== ]|
LM_ExecuteScript(sBasePath .. "Kitsunes/ZRouting.lua")

-- |[ ============================== Special =============================== ]|
--NPCs with no movement frames.
local sSpecialNPCPath = sBasePath .. "SpecialNPCSheets/"
local sGolemFancyAutoload = "AutoLoad|Adventure|SpritesGeneric|NPC_GolemFancy"
fnRipFacingSheet("GolemFancyA", sSpecialNPCPath .. "Golem_FancySheet.png",   0,   0, sGolemFancyAutoload, sAutoloadBase)
fnRipFacingSheet("GolemFancyB", sSpecialNPCPath .. "Golem_FancySheet.png",  32,   0, sGolemFancyAutoload, sAutoloadBase)
fnRipFacingSheet("GolemFancyC", sSpecialNPCPath .. "Golem_FancySheet.png",  64,   0, sGolemFancyAutoload, sAutoloadBase)
fnRipFacingSheet("GolemFancyD", sSpecialNPCPath .. "Golem_FancySheet.png",  96,   0, sGolemFancyAutoload, sAutoloadBase)
fnRipFacingSheet("GolemFancyE", sSpecialNPCPath .. "Golem_FancySheet.png", 128,   0, sGolemFancyAutoload, sAutoloadBase)
fnRipFacingSheet("GolemFancyF", sSpecialNPCPath .. "Golem_FancySheet.png", 160,   0, sGolemFancyAutoload, sAutoloadBase)
fnRipFacingSheet("GolemFancyG", sSpecialNPCPath .. "Golem_FancySheet.png",   0, 160, sGolemFancyAutoload, sAutoloadBase)
fnRipFacingSheet("GolemFancyH", sSpecialNPCPath .. "Golem_FancySheet.png",  32, 160, sGolemFancyAutoload, sAutoloadBase)
fnRipFacingSheet("GolemFancyI", sSpecialNPCPath .. "Golem_FancySheet.png",  64, 160, sGolemFancyAutoload, sAutoloadBase)
fnRipFacingSheet("GolemFancyJ", sSpecialNPCPath .. "Golem_FancySheet.png",  96, 160, sGolemFancyAutoload, sAutoloadBase)
fnRipFacingSheet("GolemFancyK", sSpecialNPCPath .. "Golem_FancySheet.png", 128, 160, sGolemFancyAutoload, sAutoloadBase)
fnRipFacingSheet("GolemFancyL", sSpecialNPCPath .. "Golem_FancySheet.png", 160, 160, sGolemFancyAutoload, sAutoloadBase)
fnRipFacingSheet("GolemFancyM", sSpecialNPCPath .. "Golem_FancySheet.png",   0, 320, sGolemFancyAutoload, sAutoloadBase)
fnRipFacingSheet("GolemFancyN", sSpecialNPCPath .. "Golem_FancySheet.png",  32, 320, sGolemFancyAutoload, sAutoloadBase)
fnRipFacingSheet("GolemFancyO", sSpecialNPCPath .. "Golem_FancySheet.png",  64, 320, sGolemFancyAutoload, sAutoloadBase)
fnRipFacingSheet("GolemFancyP", sSpecialNPCPath .. "Golem_FancySheet.png",  96, 320, sGolemFancyAutoload, sAutoloadBase)
fnRipFacingSheet("GolemFancyQ", sSpecialNPCPath .. "Golem_FancySheet.png", 128, 320, sGolemFancyAutoload, sAutoloadBase)
fnRipFacingSheet("GolemFancyR", sSpecialNPCPath .. "Golem_FancySheet.png", 160, 320, sGolemFancyAutoload, sAutoloadBase)
fnRipFacingSheet("GolemFancyS", sSpecialNPCPath .. "Golem_FancySheet.png",   0, 480, sGolemFancyAutoload, sAutoloadBase)
fnRipFacingSheet("GolemFancyT", sSpecialNPCPath .. "Golem_FancySheet.png",  32, 480, sGolemFancyAutoload, sAutoloadBase)
fnRipFacingSheet("GolemFancyU", sSpecialNPCPath .. "Golem_FancySheet.png",  64, 480, sGolemFancyAutoload, sAutoloadBase)
fnRipFacingSheet("GolemFancyV", sSpecialNPCPath .. "Golem_FancySheet.png",  96, 480, sGolemFancyAutoload, sAutoloadBase)
fnRipFacingSheet("GolemFancyW", sSpecialNPCPath .. "Golem_FancySheet.png", 128, 480, sGolemFancyAutoload, sAutoloadBase)
fnRipFacingSheet("GolemFancyX", sSpecialNPCPath .. "Golem_FancySheet.png", 160, 480, sGolemFancyAutoload, sAutoloadBase)

-- |[Clean Up]|
gfOffsetX = 0
