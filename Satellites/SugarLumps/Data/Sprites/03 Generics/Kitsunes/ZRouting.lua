-- |[ ===================================== Kistune Sheets ===================================== ]|
--Kitsune NPC sprites. Expects the Sprites.slf file to already be open.
local sBasePath = fnResolvePath()
local sAutoloadBase = "Root/Images/Sprites/"

--Civilians.
fnRipSheet("KitsuneA", sBasePath .. "KitsuneA.png", false, false, false, false, false, "AutoLoad|Adventure|SpritesGeneric|NPC_KitsuneA", sAutoloadBase)
fnRipSheet("KitsuneB", sBasePath .. "KitsuneB.png", false, false, false, false, false, "AutoLoad|Adventure|SpritesGeneric|NPC_KitsuneB", sAutoloadBase)
fnRipSheet("KitsuneC", sBasePath .. "KitsuneC.png", false, false, false, false, false, "AutoLoad|Adventure|SpritesGeneric|NPC_KitsuneC", sAutoloadBase)
fnRipSheet("KitsuneD", sBasePath .. "KitsuneD.png", false, false, false, false, false, "AutoLoad|Adventure|SpritesGeneric|NPC_KitsuneD", sAutoloadBase)

--Warriors.
fnRipSheet("KitsuneWarriorA", sBasePath .. "KitsuneWarriorA.png", false, false, false, false, false, "AutoLoad|Adventure|SpritesGeneric|NPC_KitsuneWarriorA", sAutoloadBase)
fnRipSheet("KitsuneWarriorB", sBasePath .. "KitsuneWarriorB.png", false, false, false, false, false, "AutoLoad|Adventure|SpritesGeneric|NPC_KitsuneWarriorB", sAutoloadBase)

--Special Frames
fnRipImageAuto("Spcl|KitsuneD|Sweep0", sBasePath .. "KitsuneD.png",  0, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_KitsuneD", "Root/Images/Sprites/Special/KitsuneD|Sweep0")
fnRipImageAuto("Spcl|KitsuneD|Sweep1", sBasePath .. "KitsuneD.png", 32, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_KitsuneD", "Root/Images/Sprites/Special/KitsuneD|Sweep1")
fnRipImageAuto("Spcl|KitsuneD|Sweep2", sBasePath .. "KitsuneD.png", 64, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_KitsuneD", "Root/Images/Sprites/Special/KitsuneD|Sweep2")
fnRipImageAuto("Spcl|KitsuneD|Sweep3", sBasePath .. "KitsuneD.png", 96, 200, 32, 40, 0, "AutoLoad|Adventure|SpritesGeneric|NPC_KitsuneD", "Root/Images/Sprites/Special/KitsuneD|Sweep3")
