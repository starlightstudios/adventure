-- |[ ================================ Major Character Sprites ================================= ]|
--Sprites for characters who are not playable, but play a large role in their chapters, or play a
-- role in more than one chapter.
--Characters are sorted by the first chapter they appear in.
local sBasePath = fnResolvePath()
local sAutoRoot = "Root/Images/Sprites/"

-- |[ ============================== Cassandra ============================= ]|
-- |[Sheets]|
fnRipSheet("CassandraH", sBasePath  .. "Cassandra_Human.png",   cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbNoWound, "AutoLoad|Adventure|SpritesMajor|CassandraH", sAutoRoot)
fnRipSheet("CassandraW", sBasePath  .. "Cassandra_Werecat.png", cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbNoWound, "AutoLoad|Adventure|SpritesMajor|CassandraW", sAutoRoot)
fnRipSheet("CassandraG", sBasePath  .. "Cassandra_Golem.png",   cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbNoWound, "AutoLoad|Adventure|SpritesMajor|CassandraG", sAutoRoot)

-- |[Special Frames]|
--Cassandra
ImageLump_Rip("Spcl|CassandraH|Wounded", sBasePath  .. "Cassandra_Human.png",   32,   0, 32, 40, 0)
ImageLump_Rip("Spcl|CassandraW|Wounded", sBasePath  .. "Cassandra_Werecat.png", 32,   0, 32, 40, 0)

-- |[ ============================ Crowbar-Chan ============================ ]|
--I wanted to give her her own game, but in this case, a heading will have to do.

-- |[Autoloader]|
SLF_RegisterAutoLoadLump("AutoLoad|Adventure|SpritesMajor|CrowbarChan")
SLF_RegisterAutoLoadLump("AutoLoad|Adventure|SpritesMajor|CrowbarChanMobster")

--Normal Frames
local sAutoGroup = "AutoLoad|Adventure|SpritesMajor|CrowbarChan"
local sAutoPath = "Root/Images/Sprites/CrowbarChan/"
fnRipImageAuto("CrowbarChan|0", sBasePath .. "CrowbarChan.png",  0, 0, 32, 40, 0, sAutoGroup, sAutoPath .. "0")
fnRipImageAuto("CrowbarChan|1", sBasePath .. "CrowbarChan.png", 32, 0, 32, 40, 0, sAutoGroup, sAutoPath .. "1")
fnRipImageAuto("CrowbarChan|2", sBasePath .. "CrowbarChan.png", 64, 0, 32, 40, 0, sAutoGroup, sAutoPath .. "2")

--Eyyy, forget aboud it.
sAutoGroup = "AutoLoad|Adventure|SpritesMajor|CrowbarChanMobster"
sAutoPath = "Root/Images/Sprites/CrowbarChanMobster/"
fnRipImageAuto("CrowbarChanMob|0", sBasePath .. "CrowbarChanMobster.png",  0, 0, 32, 40, 0, sAutoGroup, sAutoPath .. "0")
fnRipImageAuto("CrowbarChanMob|1", sBasePath .. "CrowbarChanMobster.png", 32, 0, 32, 40, 0, sAutoGroup, sAutoPath .. "1")
fnRipImageAuto("CrowbarChanMob|2", sBasePath .. "CrowbarChanMobster.png", 64, 0, 32, 40, 0, sAutoGroup, sAutoPath .. "2")

-- |[ ============================== Chapter 1 ============================= ]|
-- |[Sheets]|
fnRipSheet("Adina",     sBasePath .. "Adina.png",         cbFourDir,  cbIsNotWide, cbNoRunAnim,  cbNoCrouch, cbNoWound,  "AutoLoad|Adventure|SpritesMajor|Adina",     sAutoRoot)
fnRipSheet("Blythe",    sBasePath .. "Blythe.png",        cbFourDir,  cbIsNotWide, cbNoRunAnim,  cbNoCrouch, cbNoWound,  "AutoLoad|Adventure|SpritesMajor|Blythe",    sAutoRoot)
fnRipSheet("Breanne",   sBasePath .. "Breanne_Human.png", cbEightDir, cbIsNotWide, cbHasRunAnim, cbNoCrouch, cbNoWound,  "AutoLoad|Adventure|SpritesMajor|Breanne",   sAutoRoot)
fnRipSheet("Claudia",   sBasePath .. "Claudia.png",       cbFourDir,  cbIsNotWide, cbNoRunAnim,  cbNoCrouch, cbNoWound,  "AutoLoad|Adventure|SpritesMajor|Claudia",   sAutoRoot)
fnRipSheet("Jackie",    sBasePath .. "Jackie.png",        cbFourDir,  cbIsNotWide, cbHasRunAnim, cbNoCrouch, cbNoWound,  "AutoLoad|Adventure|SpritesMajor|Jackie",    sAutoRoot)
fnRipSheet("Lydie",     sBasePath .. "Lydie.png",         cbFourDir,  cbIsNotWide, cbNoRunAnim,  cbNoCrouch, cbNoWound,  "AutoLoad|Adventure|SpritesMajor|Lydie",     sAutoRoot)
fnRipSheet("Miho",      sBasePath .. "Miho.png",          cbFourDir,  cbIsNotWide, cbNoRunAnim,  cbNoCrouch, cbHasWound, "AutoLoad|Adventure|SpritesMajor|Miho",      sAutoRoot)
fnRipSheet("Nadia",     sBasePath .. "Nadia.png",         cbFourDir,  cbIsNotWide, cbNoRunAnim,  cbNoCrouch, cbNoWound,  "AutoLoad|Adventure|SpritesMajor|Nadia",     sAutoRoot)
fnRipSheet("Polaris",   sBasePath .. "Polaris.png",       cbFourDir,  cbIsNotWide, cbNoRunAnim,  cbNoCrouch, cbNoWound,  "AutoLoad|Adventure|SpritesMajor|Polaris",   sAutoRoot)
fnRipSheet("Rochea",    sBasePath .. "Rochea.png",        cbFourDir,  cbIsNotWide, cbNoRunAnim,  cbNoCrouch, cbNoWound,  "AutoLoad|Adventure|SpritesMajor|Rochea",    sAutoRoot)
fnRipSheet("Sammie",    sBasePath .. "Sammie.png",        cbFourDir,  cbIsNotWide, cbHasRunAnim, cbNoCrouch, cbNoWound,  "AutoLoad|Adventure|SpritesMajor|Sammie",    sAutoRoot)
fnRipSheet("Septima",   sBasePath .. "Septima.png",       cbEightDir, cbIsNotWide, cbHasRunAnim, cbNoCrouch, cbNoWound,  "AutoLoad|Adventure|SpritesMajor|Septima",   sAutoRoot)
fnRipSheet("Sharelock", sBasePath .. "Sharelock.png",     cbFourDir,  cbIsNotWide, cbNoRunAnim,  cbNoCrouch, cbNoWound,  "AutoLoad|Adventure|SpritesMajor|Sharelock", sAutoRoot)

-- |[Rubber Variants]|
fnRipSheet("AdinaR",   sBasePath  .. "AdinaR.png",   cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbNoWound,  "AutoLoad|Adventure|SpritesMajor|AdinaR",   sAutoRoot)
fnRipSheet("PolarisR", sBasePath ..  "PolarisR.png", cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbNoWound,  "AutoLoad|Adventure|SpritesMajor|PolarisR", sAutoRoot)
fnRipSheet("MihoR",    sBasePath ..  "MihoR.png",    cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbHasWound, "AutoLoad|Adventure|SpritesMajor|MihoR",    sAutoRoot)
fnRipSheet("RocheaR",  sBasePath  .. "RocheaR.png",  cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbNoWound,  "AutoLoad|Adventure|SpritesMajor|RocheaR",  sAutoRoot)

-- |[Special Frames]|
--Miho
ImageLump_Rip("Spcl|Miho|Drink0",   sBasePath .. "Miho.png",   0, 200, 32, 40, 0)
ImageLump_Rip("Spcl|Miho|Drink1",   sBasePath .. "Miho.png",  32, 200, 32, 40, 0)
ImageLump_Rip("Spcl|Miho|Drink2",   sBasePath .. "Miho.png",  64, 200, 32, 40, 0)
ImageLump_Rip("Spcl|Miho|Meditate", sBasePath .. "Miho.png",  32,   0, 32, 40, 0)

--Nadia
ImageLump_Rip("Spcl|Nadia|Sleep", sBasePath  .. "Nadia.png", 0, 200, 32, 40, 0)

-- |[ ============================== Chapter 2 ============================= ]|
-- |[Sheets]|
fnRipSheet("Mia",     sBasePath ..  "Mia.png",     cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbNoWound, "AutoLoad|Adventure|SpritesMajor|Mia",    sAutoRoot)
fnRipSheet("Miso",    sBasePath ..  "Miso.png",    cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbNoWound, "AutoLoad|Adventure|SpritesMajor|Miso",   sAutoRoot)
fnRipSheet("Yukina",  sBasePath ..  "Yukina.png",  cbFourDir, cbIsNotWide, cbNoRunAnim, cbNoCrouch, cbNoWound, "AutoLoad|Adventure|SpritesMajor|Yukina", sAutoRoot)

-- |[Special Frames]|
-- |[ ============================== Chapter 3 ============================= ]|
-- |[ ============================== Chapter 4 ============================= ]|
-- |[ ============================== Chapter 5 ============================= ]|
-- |[Sheets]|
fnRipSheet("20",          sBasePath .. "20.png",          cbFourDir,  cbIsNotWide, cbNoRunAnim,  cbNoCrouch, cbNoWound, "AutoLoad|Adventure|SpritesMajor|20",          sAutoRoot)
fnRipSheet("56",          sBasePath .. "56.png",          cbEightDir, cbIsNotWide, cbHasRunAnim, cbNoCrouch, cbNoWound, "AutoLoad|Adventure|SpritesMajor|56",          sAutoRoot)
fnRipSheet("Maisie",      sBasePath .. "Maisie.png",      cbFourDir,  cbIsNotWide, cbNoRunAnim,  cbNoCrouch, cbNoWound, "AutoLoad|Adventure|SpritesMajor|Maisie",      sAutoRoot)
fnRipSheet("Sammy",       sBasePath .. "Sammy.png",       cbEightDir, cbIsNotWide, cbHasRunAnim, cbNoCrouch, cbNoWound, "AutoLoad|Adventure|SpritesMajor|Sammy",       sAutoRoot)
fnRipSheet("Vivify",      sBasePath .. "Vivify.png",      cbFourDir,  cbIsWide,    cbNoRunAnim,  cbNoCrouch, cbNoWound, "AutoLoad|Adventure|SpritesMajor|Vivify",      sAutoRoot)
fnRipSheet("VivifyBlack", sBasePath .. "VivifyBlack.png", cbFourDir,  cbIsWide,    cbNoRunAnim,  cbNoCrouch, cbNoWound, "AutoLoad|Adventure|SpritesMajor|VivifyBlack", sAutoRoot)

-- |[Special Frames]|
--Vivify
ImageLump_Rip("VivifyFreeze0", sBasePath .. "Vivify.png",   0, 236, 50, 59, 0)
ImageLump_Rip("VivifyFreeze1", sBasePath .. "Vivify.png",  50, 236, 50, 59, 0)
ImageLump_Rip("VivifyFreeze2", sBasePath .. "Vivify.png", 100, 236, 50, 59, 0)
ImageLump_Rip("VivifyFreeze3", sBasePath .. "Vivify.png", 150, 236, 50, 59, 0)
ImageLump_Rip("VivifyFreeze4", sBasePath .. "Vivify.png", 200, 236, 50, 59, 0)

-- |[Sammy's Action Frames]|
--Dancing frames used in the moth videograph.
for i = 0, 7, 1 do
    ImageLump_Rip("SammyDanceA|" .. i, sBasePath .. "SammyDance.png", 32 * i,   0, 32, 40, 0)
    ImageLump_Rip("SammyDanceB|" .. i, sBasePath .. "SammyDance.png", 32 * i,  40, 32, 40, 0)
    ImageLump_Rip("SammyDanceC|" .. i, sBasePath .. "SammyDance.png", 32 * i,  80, 32, 40, 0)
    ImageLump_Rip("SammyDanceD|" .. i, sBasePath .. "SammyDance.png", 32 * i, 120, 32, 40, 0)
    ImageLump_Rip("SammyDanceE|" .. i, sBasePath .. "SammyDance.png", 32 * i, 160, 32, 40, 0)
    ImageLump_Rip("SammyDanceF|" .. i, sBasePath .. "SammyDance.png", 32 * i, 200, 32, 40, 0)
    ImageLump_Rip("SammyDanceG|" .. i, sBasePath .. "SammyDance.png", 32 * i, 240, 32, 40, 0)
    ImageLump_Rip("SammyDanceH|" .. i, sBasePath .. "SammyDance.png", 32 * i, 280, 32, 40, 0)
end

--Sammy's swimming frames.
ImageLump_Rip("SammySwimS", sBasePath .. "Sammy.png", 32, 0, 32, 40, 0)
ImageLump_Rip("SammySwimW", sBasePath .. "Sammy.png", 64, 0, 32, 40, 0)

-- |[ ============================== Chapter 6 ============================= ]|

