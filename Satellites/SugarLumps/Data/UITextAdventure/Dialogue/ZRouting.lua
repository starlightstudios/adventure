-- |[ ====================================== Dialogue UI ======================================= ]|
--Very simple UI, has a name panel version and a nameless version. That's it.
local sBasePath = fnResolvePath()

--Variable setup.
local sPrefix = "TxtDialogue|"
local saNames = {"BorderCard", "CommandList", "ExpandArrow", "NameBox", "NamelessBox", "NamePanel", "PopupBorderCard", "TextAdventureMapParts", "TextAdventureScrollbar", "TextInput"}
local saPaths = {"BorderCard", "CommandList", "ExpandArrow", "NameBox", "NamelessBox", "NamePanel", "PopupBorderCard", "TextAdventureMapParts", "TextAdventureScrollbar", "TextInput"}

--Ripping loop.
for i = 1, #saNames, 1 do
	ImageLump_Rip(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0)
end
