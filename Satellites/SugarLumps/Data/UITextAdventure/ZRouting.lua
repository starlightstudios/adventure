-- |[ =================================== Text Adventure UI ==================================== ]|
--UI for Text Adventure modes. This includes the "basic" and "advanced" cases. Both borrow from the same files.
local sBasePath = fnResolvePath()

-- |[Setup]|
--File creation.
SLF_Open("Output/UITextAdventure.slf")
ImageLump_SetCompression(1)

--Autoloader.
local sAutoloaderName = "AutoLoad|Adventure|TextAdventureUI"
local sDLPath = "Root/Images/TxtAdv/UI/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[UI Components]|
--Border Card
fnRipImageAuto("CombatWordBorderCard", sBasePath .. "CombatWordBorderCard.png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "CombatWordBorderCard")

--Navigation Buttons
fnRipImageAuto("Navigation|Look",     sBasePath .. "NavButtons.png",  0,   0,  48, 48, 0, sAutoloaderName, sDLPath .. "Look")
fnRipImageAuto("Navigation|North",    sBasePath .. "NavButtons.png", 48,   0,  48, 48, 0, sAutoloaderName, sDLPath .. "North")
fnRipImageAuto("Navigation|Up",       sBasePath .. "NavButtons.png", 96,   0,  48, 48, 0, sAutoloaderName, sDLPath .. "Up")
fnRipImageAuto("Navigation|West",     sBasePath .. "NavButtons.png",  0,  48,  48, 48, 0, sAutoloaderName, sDLPath .. "West")
fnRipImageAuto("Navigation|Wait",     sBasePath .. "NavButtons.png", 48,  48,  48, 48, 0, sAutoloaderName, sDLPath .. "Wait")
fnRipImageAuto("Navigation|East",     sBasePath .. "NavButtons.png", 96,  48,  48, 48, 0, sAutoloaderName, sDLPath .. "East")
fnRipImageAuto("Navigation|Think",    sBasePath .. "NavButtons.png",  0,  96,  48, 48, 0, sAutoloaderName, sDLPath .. "Think")
fnRipImageAuto("Navigation|South",    sBasePath .. "NavButtons.png", 48,  96,  48, 48, 0, sAutoloaderName, sDLPath .. "South")
fnRipImageAuto("Navigation|Down",     sBasePath .. "NavButtons.png", 96,  96,  48, 48, 0, sAutoloaderName, sDLPath .. "Down")
fnRipImageAuto("Navigation|ZoomBar",  sBasePath .. "NavButtons.png",  0, 144, 143, 17, 0, sAutoloaderName, sDLPath .. "ZoomBar")
fnRipImageAuto("Navigation|ZoomTick", sBasePath .. "NavButtons.png",  0, 161,   7, 11, 0, sAutoloaderName, sDLPath .. "ZoomTick")

-- |[Subfiles]|
LM_ExecuteScript(sBasePath .. "Dialogue/ZRouting.lua")

--Finish
SLF_Close()