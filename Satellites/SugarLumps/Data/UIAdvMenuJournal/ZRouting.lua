-- |[ ======================================= Journal UI ======================================= ]|
--Journal UI, to show quests, bestiary, location info, and combat help.
local sBasePath = fnResolvePath()

-- |[Setup]|
SLF_Open("Output/UIAdvMenuJournal.slf")
ImageLump_SetCompression(1)

--Autoloader Variables
local sAutoloaderName = "AutoLoad|Adventure|AdvMenuJournal"
local sDLPath = "Root/Images/AdventureUI/Journal/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[ ======================================== Ripping ========================================= ]|
-- |[Lists]|
local sPrefix = "AdvJournal|"
local saNames = {"Achievement Block", "Achievement Scrollbar Back", "BestiaryIconsL", "BestiaryIconsR", "BestiaryIconsRMonoceros", "BestiaryNameBanner", "BestiaryPortraitMask", "ParagonSticker", "ProfilesNameBanner", 
                 "Static Parts Achievements", "Static Parts Paragons", "Static Parts", "Scrollbar Front", "Scrollbar Back", "Static Parts Achievements Over Ch1", "Static Parts Achievements Over Ch2", 
				 "Static Parts Achievements Over Ch5"}
local saPaths = saNames

--Ripping loop.
for i = 1, #saNames, 1 do
	fnRipImageAuto(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. saNames[i])
end

-- |[ =================================== Achievement Icons ==================================== ]|
-- |[Common]|
local iCategoryW = 18
local iCategoryH = 18
local iAchievementW = 64
local iAchievementH = 64
sDLPath = "Root/Images/AdventureUI/AchievementIco/"

-- |[System]|
fnRipImageAuto("Achievements|System|CheckboxNo",  sBasePath .. "Achievements/CheckboxNo.png",  0, 0, -1, -1, gciNoTransparencies, sAutoloaderName, sDLPath .. "System|CheckboxNo")
fnRipImageAuto("Achievements|System|CheckboxYes", sBasePath .. "Achievements/CheckboxYes.png", 0, 0, -1, -1, gciNoTransparencies, sAutoloaderName, sDLPath .. "System|CheckboxYes")

-- |[Category Icons]|
sPath = sBasePath .. "Achievements/CategoryIcons.png"
sPrefix = "Achievements|CategoryIcons|"
fnRipImageAuto(sPrefix .. "General",  sPath, iCategoryW * 0, 0, iCategoryW, iCategoryH, gciNoTransparencies, sAutoloaderName, sDLPath .. "CategoryIcons|General")
fnRipImageAuto(sPrefix .. "Chapter1", sPath, iCategoryW * 1, 0, iCategoryW, iCategoryH, gciNoTransparencies, sAutoloaderName, sDLPath .. "CategoryIcons|Chapter1")
fnRipImageAuto(sPrefix .. "Chapter2", sPath, iCategoryW * 2, 0, iCategoryW, iCategoryH, gciNoTransparencies, sAutoloaderName, sDLPath .. "CategoryIcons|Chapter2")
fnRipImageAuto(sPrefix .. "Chapter5", sPath, iCategoryW * 5, 0, iCategoryW, iCategoryH, gciNoTransparencies, sAutoloaderName, sDLPath .. "CategoryIcons|Chapter5")

-- |[General Icons]|
sPath = sBasePath .. "Achievements/General Sheet.png"
sPrefix = "Achievements|General|"
fnRipImageAuto(sPrefix .. "Locked",      sPath, iAchievementW *  0, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "General|Locked")
fnRipImageAuto(sPrefix .. "Needlemouse", sPath, iAchievementW *  1, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "General|Needlemouse")

-- |[Chapter 1]|
sPath = sBasePath .. "Achievements/Chapter 1 Sheet.png"
sPrefix = "Achievements|Chapter1|"
fnRipImageAuto(sPrefix .. "EscapeBasement",    sPath, iAchievementW *  0, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1|EscapeBasement")
fnRipImageAuto(sPrefix .. "MeetAdina",         sPath, iAchievementW *  1, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1|MeetAdina")
fnRipImageAuto(sPrefix .. "MeetFlorentina",    sPath, iAchievementW *  2, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1|MeetFlorentina")
fnRipImageAuto(sPrefix .. "MeetPolaris",       sPath, iAchievementW *  3, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1|MeetPolaris")
fnRipImageAuto(sPrefix .. "MeetSharelock",     sPath, iAchievementW *  4, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1|MeetSharelock")
fnRipImageAuto(sPrefix .. "DrinkingContest",   sPath, iAchievementW *  5, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1|DrinkingContest")
fnRipImageAuto(sPrefix .. "Marriedraunes",     sPath, iAchievementW *  6, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1|Marriedraunes")
fnRipImageAuto(sPrefix .. "WereCatsandra",     sPath, iAchievementW *  7, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1|WereCatsandra")
fnRipImageAuto(sPrefix .. "BecomeAlraune",     sPath, iAchievementW *  8, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1|BecomeAlraune")
fnRipImageAuto(sPrefix .. "BecomeBee",         sPath, iAchievementW *  9, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1|BecomeBee")
fnRipImageAuto(sPrefix .. "BecomeGhost",       sPath, iAchievementW * 10, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1|BecomeGhost")
fnRipImageAuto(sPrefix .. "BecomeSlime",       sPath, iAchievementW * 11, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1|BecomeSlime")
fnRipImageAuto(sPrefix .. "BecomeGravemarker", sPath, iAchievementW * 12, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1|BecomeGravemarker")
fnRipImageAuto(sPrefix .. "BecomeWerecat",     sPath, iAchievementW * 13, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1|BecomeWerecat")
fnRipImageAuto(sPrefix .. "BecomeZombee",      sPath, iAchievementW * 14, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1|BecomeZombee")
fnRipImageAuto(sPrefix .. "BecomeRubber",      sPath, iAchievementW * 15, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1|BecomeRubber")
fnRipImageAuto(sPrefix .. "FinishZombee",      sPath, iAchievementW * 16, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1|FinishZombee")
fnRipImageAuto(sPrefix .. "FinishDungeon",     sPath, iAchievementW * 17, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1|FinishDungeon")
fnRipImageAuto(sPrefix .. "FinishManor",       sPath, iAchievementW * 18, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1|FinishManor")
fnRipImageAuto(sPrefix .. "FinishConvent",     sPath, iAchievementW * 19, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1|FinishConvent")
fnRipImageAuto(sPrefix .. "FindGemcutter",     sPath, iAchievementW * 20, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1|FindGemcutter")
fnRipImageAuto(sPrefix .. "FinishPie",         sPath, iAchievementW * 21, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1|FinishPie")
fnRipImageAuto(sPrefix .. "FindClaudia",       sPath, iAchievementW * 22, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1|FindClaudia")
fnRipImageAuto(sPrefix .. "FindSlimeville",    sPath, iAchievementW * 23, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1|FindSlimeville")
fnRipImageAuto(sPrefix .. "FindMediator",      sPath, iAchievementW * 24, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1|FindMediator")
fnRipImageAuto(sPrefix .. "FinishChapter",     sPath, iAchievementW * 25, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1|FinishChapter")
fnRipImageAuto(sPrefix .. "SeePollination",    sPath, iAchievementW * 26, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1|SeePollination")
fnRipImageAuto(sPrefix .. "MeetCrowbarChan",   sPath, iAchievementW * 27, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1|MeetCrowbarChan")
fnRipImageAuto(sPrefix .. "BecomeWisphag",     sPath, iAchievementW * 28, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1|BecomeWisphag")
fnRipImageAuto(sPrefix .. "FinishMannequin",   sPath, iAchievementW * 29, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1|FinishMannequin")
fnRipImageAuto(sPrefix .. "FinishDescent",     sPath, iAchievementW * 30, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1|FinishDescent")

-- |[Chapter 1 Paragons]|
sPath = sBasePath .. "Achievements/Chapter 1 Paragons.png"
sPrefix = "Achievements|Chapter1Para|"
fnRipImageAuto(sPrefix .. "UnlockAny", sPath, iAchievementW * 0, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1Para|UnlockAny")
fnRipImageAuto(sPrefix .. "Nature",    sPath, iAchievementW * 1, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1Para|Nature")
fnRipImageAuto(sPrefix .. "Spooky",    sPath, iAchievementW * 2, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1Para|Spooky")
fnRipImageAuto(sPrefix .. "Powerful",  sPath, iAchievementW * 3, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1Para|Powerful")
fnRipImageAuto(sPrefix .. "WeDidIt",   sPath, iAchievementW * 4, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter1Para|WeDidIt")

-- |[Chapter 5]|
sPath = sBasePath .. "Achievements/Chapter 5 Sheet.png"
sPrefix = "Achievements|Chapter5|"
fnRipImageAuto(sPrefix .. "FindBlueKey",       sPath, iAchievementW *  0, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|FindBlueKey")
fnRipImageAuto(sPrefix .. "MeetSophie",        sPath, iAchievementW *  1, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|MeetSophie")
fnRipImageAuto(sPrefix .. "GolemCassandra",    sPath, iAchievementW *  2, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|GolemCassandra")
fnRipImageAuto(sPrefix .. "TextAdventure",     sPath, iAchievementW *  3, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|TextAdventure")
fnRipImageAuto(sPrefix .. "Sickos",            sPath, iAchievementW *  4, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|Sickos")
fnRipImageAuto(sPrefix .. "FindCrowbar",       sPath, iAchievementW *  5, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|FindCrowbar")
fnRipImageAuto(sPrefix .. "GetMilked",         sPath, iAchievementW *  6, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|GetMilked")
fnRipImageAuto(sPrefix .. "MeetEileen",        sPath, iAchievementW *  7, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|MeetEileen")
fnRipImageAuto(sPrefix .. "BecomeGolem",       sPath, iAchievementW *  8, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|BecomeGolem")
fnRipImageAuto(sPrefix .. "BecomeDarkmatter",  sPath, iAchievementW *  9, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|BecomeDarkmatter")
fnRipImageAuto(sPrefix .. "BecomeLatex",       sPath, iAchievementW * 10, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|BecomeLatex")
fnRipImageAuto(sPrefix .. "BecomeElectro",     sPath, iAchievementW * 11, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|BecomeElectro")
fnRipImageAuto(sPrefix .. "BecomeSteam",       sPath, iAchievementW * 12, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|BecomeSteam")
fnRipImageAuto(sPrefix .. "BecomeDreamer",     sPath, iAchievementW * 13, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|BecomeDreamer")
fnRipImageAuto(sPrefix .. "BecomeRaiju",       sPath, iAchievementW * 14, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|BecomeRaiju")
fnRipImageAuto(sPrefix .. "BecomeRaibie",      sPath, iAchievementW * 15, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|BecomeRaibie")
fnRipImageAuto(sPrefix .. "BecomeDoll",        sPath, iAchievementW * 16, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|BecomeDoll")
fnRipImageAuto(sPrefix .. "FinishEquinox",     sPath, iAchievementW * 17, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|FinishEquinox")
fnRipImageAuto(sPrefix .. "FinishSerenity",    sPath, iAchievementW * 18, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|FinishSerenity")
fnRipImageAuto(sPrefix .. "FinishMines",       sPath, iAchievementW * 19, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|FinishMines")
fnRipImageAuto(sPrefix .. "FinishLRT",         sPath, iAchievementW * 20, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|FinishLRT")
fnRipImageAuto(sPrefix .. "MeetSX",            sPath, iAchievementW * 21, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|MeetSX")
fnRipImageAuto(sPrefix .. "FixSX",             sPath, iAchievementW * 22, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|FixSX")
fnRipImageAuto(sPrefix .. "FinishCryo",        sPath, iAchievementW * 23, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|FinishCryo")
fnRipImageAuto(sPrefix .. "GetInspected",      sPath, iAchievementW * 24, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|GetInspected")
fnRipImageAuto(sPrefix .. "FinishManufactory", sPath, iAchievementW * 25, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|FinishManufactory")
fnRipImageAuto(sPrefix .. "FinishShopping",    sPath, iAchievementW * 26, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|FinishShopping")
fnRipImageAuto(sPrefix .. "HeavySupport",      sPath, iAchievementW * 27, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|HeavySupport")
fnRipImageAuto(sPrefix .. "ReachBiolabs",      sPath, iAchievementW * 28, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|ReachBiolabs")
fnRipImageAuto(sPrefix .. "FinishReika",       sPath, iAchievementW * 29, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|FinishReika")
fnRipImageAuto(sPrefix .. "FinishAquatic",     sPath, iAchievementW * 30, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|FinishAquatic")
fnRipImageAuto(sPrefix .. "MeetMosquito",      sPath, iAchievementW * 31, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|MeetMosquito")
fnRipImageAuto(sPrefix .. "FinishMovie",       sPath, iAchievementW * 32, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|FinishMovie")
fnRipImageAuto(sPrefix .. "FinishEpsilon",     sPath, iAchievementW * 33, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|FinishEpsilon")
fnRipImageAuto(sPrefix .. "Ch5BadEnd",         sPath, iAchievementW * 34, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|Ch5BadEnd")
fnRipImageAuto(sPrefix .. "Ch5GoodEnd",        sPath, iAchievementW * 35, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|Ch5GoodEnd")
fnRipImageAuto(sPrefix .. "BecomeSecrebot",    sPath, iAchievementW * 36, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|BecomeSecrebot")
fnRipImageAuto(sPrefix .. "NonCanonStory",     sPath, iAchievementW * 37, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5|NonCanonStory")

-- |[Chapter 5 Paragons]|
sPath = sBasePath .. "Achievements/Chapter 5 Paragons.png"
sPrefix = "Achievements|Chapter5Para|"
fnRipImageAuto(sPrefix .. "UnlockAny",  sPath, iAchievementW * 0, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5Para|UnlockAny")
fnRipImageAuto(sPrefix .. "Impossible", sPath, iAchievementW * 1, 0, iAchievementW, iAchievementH, gciNoTransparencies, sAutoloaderName, sDLPath .. "Chapter5Para|Impossible")

-- |[ ==================================== Glossary Entries ==================================== ]|
--Each glossary page has its own subfolder. These are put on a different autoloader block as they
-- are not loaded until the journal is opened.
sAutoloaderName = "AutoLoad|Adventure|AdvMenuJournalGlossary"
sPrefix = "AdvJournal|Glossary|"
sDLPath = "Root/Images/AdventureUI/CombatGlossary/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[Combat Stats]|
local sPath = sBasePath .. "Combat Stats/"
fnRipImageAuto(sPrefix .. "CombatStats|0 Health Icon",     sPath .. "0 Health Icon.png",     0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "CombatStats|0 Health Icon")
fnRipImageAuto(sPrefix .. "CombatStats|1 Power Icon",      sPath .. "1 Power Icon.png",      0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "CombatStats|1 Power Icon")
fnRipImageAuto(sPrefix .. "CombatStats|2 Initiative Icon", sPath .. "2 Initiative Icon.png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "CombatStats|2 Initiative Icon")
fnRipImageAuto(sPrefix .. "CombatStats|3 AccEvade Icon",   sPath .. "3 AccEvade Icon.png",   0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "CombatStats|3 AccEvade Icon")

-- |[Other Stats]|
sPath = sBasePath .. "Other Stats/"
fnRipImageAuto(sPrefix .. "OtherStats|0 Adrenaline Icon", sPath .. "0 Adrenaline Icon.png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "OtherStats|0 Adrenaline Icon")
fnRipImageAuto(sPrefix .. "OtherStats|1 Shield Icon",     sPath .. "1 Shield Icon.png",     0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "OtherStats|1 Shield Icon")

-- |[Damage Types]|
sPath = sBasePath .. "Damage Types/"
fnRipImageAuto(sPrefix .. "DamageTypes|0 Damage Icons",     sPath .. "0 Damage Icons.png",     0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "DamageTypes|0 Damage Icons")
fnRipImageAuto(sPrefix .. "DamageTypes|1 Alt Damage Icons", sPath .. "1 Alt Damage Icons.png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "DamageTypes|1 Alt Damage Icons")

-- |[Critical Strikes]|
sPath = sBasePath .. "Critical Strikes/"
fnRipImageAuto(sPrefix .. "CriticalStrikes|0 Crit Example", sPath .. "0 Crit Example.png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "CriticalStrikes|0 Crit Example")

-- |[Stun]|
sPath = sBasePath .. "Stun/"
fnRipImageAuto(sPrefix .. "Stun|0 Stun Example", sPath .. "0 Stun Example.png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "Stun|0 Stun Example")

-- |[Combat Inspector]|
sPath = sBasePath .. "Combat Inspector/"
fnRipImageAuto(sPrefix .. "Inspector|0 Inspector Example", sPath .. "0 Inspector Example.png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "Inspector|0 Inspector Example")

-- |[Effects]|
sPath = sBasePath .. "Effects/"
fnRipImageAuto(sPrefix .. "Effects|0 Effect Example", sPath .. "0 Effect Example.png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "Effects|0 Effect Example")
fnRipImageAuto(sPrefix .. "Effects|1 Dot Frame",      sPath .. "1 Dot Frame.png",      0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "Effects|1 Dot Frame")
fnRipImageAuto(sPrefix .. "Effects|2 Buff Frame",     sPath .. "2 Buff Frame.png",     0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "Effects|2 Buff Frame")
fnRipImageAuto(sPrefix .. "Effects|3 Debuff Frame",   sPath .. "3 Debuff Frame.png",   0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "Effects|3 Debuff Frame")
fnRipImageAuto(sPrefix .. "Effects|4 Hot Frame",      sPath .. "4 Hot Frame.png",      0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "Effects|4 Hot Frame")

-- |[Threat]|
sPath = sBasePath .. "Threat/"
fnRipImageAuto(sPrefix .. "Threat|0 Threat Example", sPath .. "0 Threat Example.png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "Threat|0 Threat Example")

-- |[Items]|
sPath = sBasePath .. "Items/"
fnRipImageAuto(sPrefix .. "Items|0 Item Example", sPath .. "0 Item Example.png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "Items|0 Item Example")

-- |[Free Actions]|
sPath = sBasePath .. "Free Actions/"
fnRipImageAuto(sPrefix .. "Free Actions|0 Free Action Example", sPath .. "0 Free Action Example.png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "Free Actions|0 Free Action Example")

-- |[MP and CP]|
sPath = sBasePath .. "MP CP/"
fnRipImageAuto(sPrefix .. "MP CP Finishers|0 MP Display", sPath .. "0 MP Display.png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "MP CP Finishers|0 MP Display")
fnRipImageAuto(sPrefix .. "MP CP Finishers|1 CP Display", sPath .. "1 CP Display.png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "MP CP Finishers|1 CP Display")

-- |[Gems]|
sPath = sBasePath .. "Gems/"
fnRipImageAuto(sPrefix .. "Gems|0 Mergeable Gems", sPath .. "0 Mergeable Gems.png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "Gems|0 Mergeable Gems")
fnRipImageAuto(sPrefix .. "Gems|1 Merged Gems",    sPath .. "1 Merged Gems.png",    0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "Gems|1 Merged Gems")
fnRipImageAuto(sPrefix .. "Gems|2 Green Gems",     sPath .. "2 Green Gems.png",     0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "Gems|2 Green Gems")

-- |[Paragons]|
sPath = sBasePath .. "Paragons/"
fnRipImageAuto(sPrefix .. "Paragons|0 Paragon Header", sPath .. "0 Paragon Header.png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "Paragons|0 Paragon Header")

-- |[ ======================================== Clean Up ======================================== ]|
--Finish
SLF_Close()
