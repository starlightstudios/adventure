-- |[ =================================== Combat Animations ==================================== ]|
--These are the animations that play over the character portraits in combat. There are variations
-- for all the major element types, as well as miscellaneous cases like healing, buffs, debuff removal,
-- and so on. They come in 4x4 grids of fixed sizes.
SLF_Open("Output/CombatAnimations.slf")

-- |[Autoloader Setup]|
sAutoloaderName = "AutoLoad|Adventure|CombatAnimations"
sAutoloaderPath = "Root/Images/CombatAnimations/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[ =================================== Automated Ripping ==================================== ]|
--Setup.
local sBasePath = fnResolvePath()

-- |[Data Function]|
local zaData = {}
local function fnAddData(psFolderName, piCount, psImgPattern, psAutoloadName, pbStartFromZero)
    if(pbStartFromZero == nil) then pbStartFromZero = false end
    local zEntry = {}
    zEntry.sFolderName    = psFolderName
    zEntry.iCount         = piCount
    zEntry.sImgPattern    = psImgPattern
    zEntry.sAutoloadName  = psAutoloadName
    zEntry.bStartFromZero = pbStartFromZero
    table.insert(zaData, zEntry)
end

-- |[Data Series]|
fnAddData("Ankh",        27, "ankh",               "Ankh",   true)
fnAddData("ArrowM",      40, "arrow_multi",        "ArrowM", true)
fnAddData("ArrowS",      11, "arrow_single",       "ArrowS", true)
fnAddData("Bleed",       19, "bleeding",           "Bleed")
fnAddData("Blind",       10, "blind",              "Blind")
fnAddData("Buff",        24, "buffa-color",        "Buff")
fnAddData("Corrode",     41, "corrosion",          "Corrode")
fnAddData("Debuff",      24, "debuffa-color",      "Debuff")
fnAddData("Electricity", 24, "electricitya-color", "Electricity")
fnAddData("Fear",        30, "fear_00",            "Fear")
fnAddData("Fire",        40, "fire",               "Fire")
fnAddData("Flamewall",   30, "flame",              "Flamewall", true)
fnAddData("GunShot",     20, "gunshot",            "GunShot")
fnAddData("Haste",       50, "haste",              "Haste")
fnAddData("Healing",     24, "healinga-color",     "Healing")
fnAddData("Ice",         40, "ice",                "Ice")
fnAddData("LaserShot",   20, "lasershot",          "LaserShot")
fnAddData("Light",       30, "light",              "Light")
fnAddData("Music",       36, "music",              "Music", true)
fnAddData("Pierce",      24, "piercinga-color",    "Pierce")
fnAddData("PierceF",     20, "flame-pierce",       "PierceF", true)
fnAddData("Poison",      50, "poison",             "Poison")
fnAddData("Protect",     30, "protect",            "Protect", true)
fnAddData("ShadowA",     40, "shadow_a",           "ShadowA")
fnAddData("ShadowB",     50, "shadow_b",           "ShadowB")
fnAddData("ShadowC",     38, "shadow_c",           "ShadowC")
fnAddData("Slash1",      24, "slash1a-color",      "SlashClaw")
fnAddData("Slash2",      24, "slash2a-color",      "SlashCross")
fnAddData("SlashL",      30, "lava-slash-b",       "SlashLava", true)
fnAddData("Slow",        70, "slow",               "Slow")
fnAddData("Strike",      10, "strike2b-color",     "Strike")
fnAddData("Whip",        14, "whip_",              "Whip", true)

-- |[Execute Rip]|
--For each folder:
for i = 1, #zaData, 1 do
    
    --Autoloader
    sAutoloaderPath = "Root/Images/CombatAnimations/" .. zaData[i].sAutoloadName .. "/"
    
    --Resolve to start at 0 or 1.
    local iStart = 1
    local iEnd = zaData[i].iCount
    if(zaData[i].bStartFromZero) then
        iStart = iStart - 1
        iEnd = iEnd - 1
    end
    
    --For each image in the set:
	for p = iStart, iEnd, 1 do
    
        --Count.
        local iCount = p - 1
        if(zaData[i].bStartFromZero) then iCount = iCount + 1 end
    
        --Resolve variables.
		local sName = string.format("ComFX|%s%02i", zaData[i].sFolderName, iCount)
		local sPath = sBasePath .. zaData[i].sFolderName .. "/" .. string.format("%s%02i", zaData[i].sImgPattern, p) .. ".png"
        local sAuto = string.format(sAutoloaderPath .. "%02i", iCount)
        
        --Debug:
        --io.write("Name: " .. sName .. " " .. sPath .. "\n")
        
        --Rip.
		fnRipImageAuto(sName, sPath, 0, 0, -1, -1, 0, sAutoloaderName, sAuto)
    end
end

-- |[ ======================================== Overlays ======================================== ]|
--Overlays, meant to go over top of existing images with offsets.
sAutoloaderPath = "Root/Images/CombatAnimations/BuffText/"
fnRipImageAuto("BuffAccuracy",   sBasePath .. "BuffText/Accuracy.png",   0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Accuracy")
fnRipImageAuto("BuffDefense",    sBasePath .. "BuffText/Defense.png",    0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Defense")
fnRipImageAuto("BuffEvade",      sBasePath .. "BuffText/Evade.png",      0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Evade")
fnRipImageAuto("BuffInitiative", sBasePath .. "BuffText/Initiative.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Initiative")
fnRipImageAuto("BuffPower",      sBasePath .. "BuffText/Power.png",      0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Power")
fnRipImageAuto("BuffResists",    sBasePath .. "BuffText/Resists.png",    0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Resists")

-- |[ ======================================== Clean Up ======================================== ]|
--Finish
SLF_Close()
