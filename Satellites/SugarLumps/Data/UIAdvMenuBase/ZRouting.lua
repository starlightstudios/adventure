-- |[ ====================================== Base Menu UI ====================================== ]|
--Base menu. Used whenever the player presses cancel on the overworld.
local sBasePath = fnResolvePath()

-- |[Setup]|
--File.
SLF_Open("Output/UIAdvMenuBase.slf")
ImageLump_SetCompression(1)

--Autoloader Variables
local sAutoloaderName = "AutoLoad|Adventure|AdvMenuBase"
local sDLPath = "Root/Images/AdventureUI/BaseMenu/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[ ======================================== Ripping ========================================= ]|
-- |[Lists]|
local sPrefix = "AdvBaseMenu|"
local saNames = {"Adamantite Detail", "Adamantite Frame", "Adamantite Header", "Catalyst Detail", "Catalyst Frame", "Catalyst Header", "Character Frame", "Character HP Fill", "Character HP Frame", "Character Level Back",
                 "Character Level Front", "Character Name Banner", "Footer", "Header", "Platina Frame"}
local saPaths = saNames

--Ripping loop.
for i = 1, #saNames, 1 do
	fnRipImageAuto(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. saNames[i])
end

-- |[Icons]|
--These are loaded under a different autoloader lump since they are filtered.
sAutoloaderName = "AutoLoad|Adventure|AdvMenuBaseFilter"
SLF_RegisterAutoLoadLump(sAutoloaderName)

--Setup.
local iIconW = 96
local iIconH = 96
local sIconPath = sBasePath .. "MenuIcons.png"

--Rip.
ImageLump_SetBlockTrimmingFlag(true)
fnRipImageAuto(sPrefix .. "ICO|Inventory", sIconPath, iIconW* 0, iIconH*0, iIconW, iIconH, 0, sAutoloaderName, sDLPath .. "ICO|Inventory")
fnRipImageAuto(sPrefix .. "ICO|Equipment", sIconPath, iIconW* 1, iIconH*0, iIconW, iIconH, 0, sAutoloaderName, sDLPath .. "ICO|Equipment")
fnRipImageAuto(sPrefix .. "ICO|DoctorBag", sIconPath, iIconW* 2, iIconH*0, iIconW, iIconH, 0, sAutoloaderName, sDLPath .. "ICO|DoctorBag")
fnRipImageAuto(sPrefix .. "ICO|Status",    sIconPath, iIconW* 3, iIconH*0, iIconW, iIconH, 0, sAutoloaderName, sDLPath .. "ICO|Status")
fnRipImageAuto(sPrefix .. "ICO|Skills",    sIconPath, iIconW* 4, iIconH*0, iIconW, iIconH, 0, sAutoloaderName, sDLPath .. "ICO|Skills")
fnRipImageAuto(sPrefix .. "ICO|Map",       sIconPath, iIconW* 5, iIconH*0, iIconW, iIconH, 0, sAutoloaderName, sDLPath .. "ICO|Map")
fnRipImageAuto(sPrefix .. "ICO|Options",   sIconPath, iIconW* 6, iIconH*0, iIconW, iIconH, 0, sAutoloaderName, sDLPath .. "ICO|Options")
fnRipImageAuto(sPrefix .. "ICO|Quit",      sIconPath, iIconW* 7, iIconH*0, iIconW, iIconH, 0, sAutoloaderName, sDLPath .. "ICO|Quit")
fnRipImageAuto(sPrefix .. "ICO|Journal",   sIconPath, iIconW* 8, iIconH*0, iIconW, iIconH, 0, sAutoloaderName, sDLPath .. "ICO|Journal")
fnRipImageAuto(sPrefix .. "ICO|Switch",    sIconPath, iIconW* 9, iIconH*0, iIconW, iIconH, 0, sAutoloaderName, sDLPath .. "ICO|Switch")
fnRipImageAuto(sPrefix .. "ICO|SkillNote", sIconPath, iIconW*10, iIconH*0, iIconW, iIconH, 0, sAutoloaderName, sDLPath .. "ICO|SkillNote")
ImageLump_SetBlockTrimmingFlag(false)

-- |[ ======================================== Clean Up ======================================== ]|
--Finish
SLF_Close()


