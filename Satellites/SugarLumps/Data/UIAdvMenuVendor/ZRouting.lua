-- |[ ======================================== Vendor UI ======================================= ]|
--Status UI. No interactivity, displays data.
local sBasePath = fnResolvePath()

-- |[Setup]|
SLF_Open("Output/UIAdvMenuVendor.slf")
ImageLump_SetCompression(1)

--Autoloader Variables
local sAutoloaderName = "AutoLoad|Adventure|AdvMenuVendor"
local sDLPath = "Root/Images/AdventureUI/Vendor/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[ ======================================== Ripping ========================================= ]|
-- |[Lists]|
local sPrefix = "AdvVendor|"
local saNames = {"Arrow Lft", "Arrow Rgt", "Buy Item Popup", "Comparison Frame", "Description Frame", "Detail Buy", "Detail Buyback", "Detail Gems", "Detail Gems Merge", "Detail Sell", "Expandable Header", "Header", 
                 "Platina Frame", "Mat Arrow Lft", "Mat Arrow Rgt", "Scrollbar Static", "Scrollbar Scroller", "Vendor Frame", "Unlock Frame", "Unlock Stencil", "Category Frames"}
local saPaths = saNames

--Ripping loop.
for i = 1, #saNames, 1 do
	fnRipImageAuto(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. saNames[i])
end

-- |[ ===================================== Category Icons ===================================== ]|
local sCategoryIconPath = sBasePath .. "Category Icons.png"
local ciCategoryIcoW = 38
local ciCategoryIcoH = 38
fnRipImageAuto(sPrefix .. "Category|All",         sCategoryIconPath, ciCategoryIcoW * 0, 0, ciCategoryIcoW, ciCategoryIcoH, 0, sAutoloaderName, sDLPath .. "Category|All")
fnRipImageAuto(sPrefix .. "Category|Weapons",     sCategoryIconPath, ciCategoryIcoW * 1, 0, ciCategoryIcoW, ciCategoryIcoH, 0, sAutoloaderName, sDLPath .. "Category|Weapons")
fnRipImageAuto(sPrefix .. "Category|Armors",      sCategoryIconPath, ciCategoryIcoW * 2, 0, ciCategoryIcoW, ciCategoryIcoH, 0, sAutoloaderName, sDLPath .. "Category|Armors")
fnRipImageAuto(sPrefix .. "Category|Accessories", sCategoryIconPath, ciCategoryIcoW * 3, 0, ciCategoryIcoW, ciCategoryIcoH, 0, sAutoloaderName, sDLPath .. "Category|Accessories")
fnRipImageAuto(sPrefix .. "Category|Upgrading",   sCategoryIconPath, ciCategoryIcoW * 4, 0, ciCategoryIcoW, ciCategoryIcoH, 0, sAutoloaderName, sDLPath .. "Category|Upgrading")

-- |[ ======================================== Gems Set ======================================== ]|
--A subset UI.
saNames = {"Gem Disassemble Popup", "Gems Adamantite Frame", "Gems Comparison Frame", "Gems Contents Frame", "Gems Inventory Frame", "Gems Name Frame", "Gems Platina Frame"}
saPaths = saNames

--Ripping loop.
for i = 1, #saNames, 1 do
	fnRipImageAuto(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. saNames[i])
end

-- |[ ======================================== Clean Up ======================================== ]|
--Finish
SLF_Close()
