-- |[ ====================================== Standard UI ======================================= ]|
--Standard UI. Contains parts that many other UIs use.
local sBasePath = fnResolvePath()

-- |[Setup]|
SLF_Open("Output/UIAdvMenuStandard.slf")
ImageLump_SetCompression(1)

--Autoloader Variables
local sAutoloaderName = "AutoLoad|Adventure|AdvMenuStandard"
local sDLPath = "Root/Images/AdventureUI/Standard/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[ ======================================== Ripping ========================================= ]|
-- |[Lists]|
local sPrefix = "AdvStandard|"
local saNames = {"Description Frame", "Expandable Header", "Grid Frame", "Help Frame", "Highlight", "Scrollbar Front"}
local saPaths = saNames

--Ripping loop.
for i = 1, #saNames, 1 do
	fnRipImageAuto(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. saNames[i])
end

-- |[ ======================================== Clean Up ======================================== ]|
--Finish
SLF_Close()
