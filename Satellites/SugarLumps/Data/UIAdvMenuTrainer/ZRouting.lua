-- |[ ======================================= Trainer UI ======================================= ]|
--Trainer UI, allows the player to spend money to buy EXP.
local sBasePath = fnResolvePath()

-- |[Setup]|
SLF_Open("Output/UIAdvMenuTrainer.slf")
ImageLump_SetCompression(1)

--Autoloader Variables
local sAutoloaderName = "AutoLoad|Adventure|AdvMenuTrainer"
local sDLPath = "Root/Images/AdventureUI/Trainer/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[ ======================================== Ripping ========================================= ]|
-- |[Lists]|
local sPrefix = "AdvTrainer|"
local saNames = {"Frame_Base", "Frame_Confirm", "Frame_Header", "Frame_Payment", "Frame_Platina", "Overlay_ArrowRgt", "Overlay_ArrowUp", "Overlay_NameBanner"}
local saPaths = saNames

--Ripping loop.
for i = 1, #saNames, 1 do
	fnRipImageAuto(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. saNames[i])
end

-- |[ ======================================== Clean Up ======================================== ]|
--Finish
SLF_Close()
