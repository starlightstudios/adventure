-- |[ ======================================== Doctor UI ======================================= ]|
--Doctor UI. Heal your party.
local sBasePath = fnResolvePath()

-- |[Setup]|
SLF_Open("Output/UIAdvMenuDoctor.slf")
ImageLump_SetCompression(1)

--Autoloader Variables
local sAutoloaderName = "AutoLoad|Adventure|AdvMenuDoctor"
local sDLPath = "Root/Images/AdventureUI/Doctor/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[ ======================================== Ripping ========================================= ]|
-- |[Lists]|
local sPrefix = "AdvDoctor|"
local saNames = {"Bar Fill", "Bar Frame", "Frame", "Header", "Symbol"}
local saPaths = saNames

--Ripping loop.
for i = 1, #saNames, 1 do
	fnRipImageAuto(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. saNames[i])
end

-- |[ ======================================== Clean Up ======================================== ]|
--Finish
SLF_Close()


