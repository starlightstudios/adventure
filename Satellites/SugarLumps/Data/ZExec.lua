-- |[ ============================== Image Compression Execution =============================== ]|
--Execs image compression for your project.
Debug_PushPrint(true, "Beginning image compression\n")

--Debug flags.
local sCurrentPath = fnResolvePath()

-- |[Functions / Constants]|
--Causes images to compress at 1/4th size. Greatly reduces file size and image quality.
gbUseLowDefinition = false

--Disables/Enables trimming of the edges of an image. If false, transparent edge pixels are purged.
-- ImageLump_SetBlockTrimmingFlag(true)

--Disables/Enables compressing images by lines. Each line is scanned to remove the transparent edges.
-- This takes extra time but can reduce file sizes. If a line-compressed image is larger than an
-- uncompressed image (the case for no transparencies) the uncompressed data is used.
ImageLump_SetAllowLineCompression(true)

--Global flag. Put this as a flag during compression to ignore the three transparent colors.
gciNoTransparencies = 16

-- |[ ==================================== Folder Execution ==================================== ]|
-- |[Setup]|
local baCompressionFlags = {}

-- |[High-Def Execution]|
--Auto-exec the folders.
gbUseLowDefinition = false
ImageLump_SetAllowLineCompression(true)
baCompressionFlags[ 1] = fnCompressFolder(sCurrentPath, "CombatAnimations", "ZRouting")
baCompressionFlags[ 2] = fnCompressFolder(sCurrentPath, "ElectrospriteAdventure", "ZRouting")
baCompressionFlags[ 3] = fnCompressFolder(sCurrentPath, "MapAnimations", "ZRouting")
baCompressionFlags[ 4] = fnCompressFolder(sCurrentPath, "Sprites", "ZRouting")
baCompressionFlags[ 5] = fnCompressFolder(sCurrentPath, "UIAdventure", "ZRouting")
baCompressionFlags[ 6] = fnCompressFolder(sCurrentPath, "UIAdvCombat", "ZRouting")
baCompressionFlags[ 7] = fnCompressFolder(sCurrentPath, "UIAdvMenuBase", "ZRouting")
baCompressionFlags[ 8] = fnCompressFolder(sCurrentPath, "UIAdvMenuFieldAbilities", "ZRouting")
baCompressionFlags[ 9] = fnCompressFolder(sCurrentPath, "UIAdvMenuJournal", "ZRouting")
baCompressionFlags[10] = fnCompressFolder(sCurrentPath, "UIAdvMenuSkills", "ZRouting")
baCompressionFlags[11] = fnCompressFolder(sCurrentPath, "UIControls", "ZRouting")
baCompressionFlags[12] = fnCompressFolder(sCurrentPath, "UITextAdventure", "ZRouting")
baCompressionFlags[13] = fnCompressFolder(sCurrentPath, "UIAdvIcon", "ZRouting")
baCompressionFlags[14] = fnCompressFolder(sCurrentPath, "UIRegionMarkers", "ZRouting")
baCompressionFlags[15] = fnCompressFolder(sCurrentPath, "PuzzleBattle", "ZRouting")
baCompressionFlags[16] = fnCompressFolder(sCurrentPath, "UIAdvMenuEquipment", "ZRouting")
baCompressionFlags[17] = fnCompressFolder(sCurrentPath, "UIAdvMenuStatus", "ZRouting")
baCompressionFlags[18] = fnCompressFolder(sCurrentPath, "UIAdvMenuInventory", "ZRouting")
baCompressionFlags[19] = fnCompressFolder(sCurrentPath, "UIAdvMenuOptions", "ZRouting")
baCompressionFlags[20] = fnCompressFolder(sCurrentPath, "UIAdvMenuVendor", "ZRouting")
baCompressionFlags[21] = fnCompressFolder(sCurrentPath, "UIAdvMenuStandard", "ZRouting")
baCompressionFlags[22] = fnCompressFolder(sCurrentPath, "UIAdvMenuCampfire", "ZRouting")
baCompressionFlags[23] = fnCompressFolder(sCurrentPath, "UIAdvMenuFileSelect", "ZRouting")
baCompressionFlags[24] = fnCompressFolder(sCurrentPath, "UIAdvMenuQuit", "ZRouting")
baCompressionFlags[25] = fnCompressFolder(sCurrentPath, "UIAdvMenuDoctor", "ZRouting")
baCompressionFlags[26] = fnCompressFolder(sCurrentPath, "UIAbyCombat", "ZRouting")
baCompressionFlags[27] = fnCompressFolder(sCurrentPath, "UIAdvMenuTrainer", "ZRouting")
ImageLump_SetAllowLineCompression(false)

-- |[Low-Def Execution]|
--Re-execute with low-definition flags on for any compressed files.

-- |[ ======================================= Subfolders ======================================= ]|
--Execute subfolders which follow the same pattern as this file.
LM_ExecuteScript(sCurrentPath .. "AdventureScenes/ZExec.lua")
LM_ExecuteScript(sCurrentPath .. "Portraits/ZExec.lua")
LM_ExecuteScript(sCurrentPath .. "Maps/ZExec.lua")

-- |[ ======================================== Finish Up ======================================= ]|
--Done
Debug_PopPrint("Finished image compression activities!\n")
