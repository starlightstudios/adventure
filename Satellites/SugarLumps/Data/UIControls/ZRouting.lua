-- |[ ====================================== Controls UI ======================================= ]|
--Stores images that related to the keyboard/mouse/joypad.
local sBasePath = fnResolvePath()

-- |[Setup]|
SLF_Open("Output/UIControls.slf")
ImageLump_SetCompression(1)

-- |[ ======================================= Functions ======================================== ]|
local function fnRip(piXStart, piYStart, piWidth, piHeight, piXSpacing, psaList)
    local sImgPath = sBasePath .. "Keyboard.png"
    for i = 1, #psaList, 1 do
        local iX = piXStart + (piXSpacing * (i-1))
        local iY = piYStart
        ImageLump_Rip("Keyboard|" .. psaList[i], sImgPath, iX, iY, piWidth, piHeight, 0)
    end
end

-- |[ ======================================== Keyboard ======================================== ]|

-- |[Function Row]|
fnRip(16, 0, 28, 19,  0, {"Escape"})
fnRip(45, 0, 19, 19, 20, {"F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9", "F10", "F11", "F12"})

-- |[Numeric Row]|
fnRip( 25, 20, 19, 19, 20, {"Tilde", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "-", "="})
fnRip(285, 20, 39, 19,  0, {"Backspace"})
fnRip(325, 20, 19, 19, 20, {"Ins", "Home", "PgUp"})

-- |[QWERTY Row]|
fnRip( 14, 40, 30, 19,  0, {"Tab"})
fnRip( 45, 40, 19, 19, 20, {"Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "LBrace", "RBrace", "Backslash"})
fnRip(325, 40, 19, 19, 20, {"Del", "End", "PgDn"})

-- |[ASDF Row]|
fnRip(  0, 60, 44, 19,  0, {"Capslock"})
fnRip( 45, 60, 19, 19, 20, {"A", "S", "D", "F", "G", "H", "J", "K", "L", "Semicolon", "Apostrophie", "Pound"})
fnRip(285, 60, 39, 19,  0, {"Enter"})
fnRip(345, 60, 19, 19,  0, {"ArrU"})

-- |[ZXCV Row]|
fnRip(  0, 80, 49, 19,  0, {"LShift"})
fnRip( 50, 80, 19, 19, 20, {"Z", "X", "C", "V", "B", "N", "M", "Comma", "Period", "Slash"})
fnRip(250, 80, 51, 19,  0, {"RShift"})
fnRip(325, 80, 19, 19, 20, {"ArrL", "ArrD", "ArrR"})

-- |[Utility Row]|
fnRip(  0, 100, 41, 19,  0, {"LCtrl"})
fnRip( 64, 100, 35, 19,  0, {"LAlt"})
fnRip(105, 100, 42, 19,  0, {"Space"})
fnRip(220, 100, 37, 19,  0, {"RAlt"})
fnRip(258, 100, 43, 19,  0, {"RCtrl"})

-- |[Special]|
fnRip( 62, 120, 28, 19,  0, {"Error"})

-- |[ ======================================== Clean Up ======================================== ]|
--Finish
SLF_Close()