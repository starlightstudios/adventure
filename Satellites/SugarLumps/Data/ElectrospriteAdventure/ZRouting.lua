-- |[ ================================ Electrosprite Adventure ================================= ]|
--Non-UI graphics used by the Electrosprite Text Adventure minigame in chapter 5.
local sBasePath = fnResolvePath()

-- |[Setup]|
--Open.
SLF_Open("Output/ElectrospriteAdventure.slf")

--Autoloader Variables
local sAutoloaderName = "AutoLoad|Adventure|ElectrospriteImages"
local sDLPath = "Root/Images/ElectrospriteAdv/Characters/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[Ripping]|
--Player sprites.
fnRipImageAuto("Player",  sBasePath .. "Player.png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "Player")

--Five NPCs, from A to E. All have the same sequences.
local saArray = {"A", "B", "C", "D", "E"}
for i = 1, 5, 1 do
    local sUsePath = sDLPath .. "NPC" .. saArray[i]
    fnRipImageAuto("NPC" ..  saArray[i] .. "Cored",  sBasePath .. "NPC " ..  saArray[i] .. " Cored.png",  0, 0, -1, -1, 0, sAutoloaderName, sUsePath .. "Cored")
    fnRipImageAuto("NPC" ..  saArray[i] .. "Golem",  sBasePath .. "NPC " ..  saArray[i] .. " Golem.png",  0, 0, -1, -1, 0, sAutoloaderName, sUsePath .. "Golem")
    fnRipImageAuto("NPC" ..  saArray[i] .. "Normal", sBasePath .. "NPC " ..  saArray[i] .. " Normal.png", 0, 0, -1, -1, 0, sAutoloaderName, sUsePath .. "Normal")
    fnRipImageAuto("NPC" ..  saArray[i] .. "TF0",    sBasePath .. "NPC " ..  saArray[i] .. " TF0.png",    0, 0, -1, -1, 0, sAutoloaderName, sUsePath .. "TF0")
    fnRipImageAuto("NPC" ..  saArray[i] .. "TF1",    sBasePath .. "NPC " ..  saArray[i] .. " TF1.png",    0, 0, -1, -1, 0, sAutoloaderName, sUsePath .. "TF1")
    fnRipImageAuto("NPC" ..  saArray[i] .. "TF2",    sBasePath .. "NPC " ..  saArray[i] .. " TF2.png",    0, 0, -1, -1, 0, sAutoloaderName, sUsePath .. "TF2")
end

-- |[Finish]|
SLF_Close()
