-- |[ ====================================== Abyss Combat ====================================== ]|
--Alternate combat layout, can be used by mods.
local sBasePath = fnResolvePath()

-- |[Setup]|
SLF_Open("Output/AbyCombat.slf")
ImageLump_SetCompression(1)

-- |[Autoloader Info]|
local sAutoloaderName = "AutoLoad|Adventure|AbyssUI"
local sAutoloaderPath = "Root/Images/AbyssUI/Pieces/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[Function]|
local function fnRip(sDirPath, sPrefix, saNames)
    for i = 1, #saNames, 1 do
        ImageLump_Rip(sPrefix .. saNames[i], sDirPath .. saNames[i] .. ".png", 0, 0, -1, -1, 0)
        AutoLoader_Register(sAutoloaderName, sPrefix .. saNames[i], sAutoloaderPath .. saNames[i])
    end
end

-- |[ ======================================= Base Parts ======================================= ]|
local sDirPath = sBasePath
local sPrefix = "Base|"
local saNames = {"Fill_EnemyHP", "Fill_EnemyStar", "Fill_EnemyStun", "Fill_PlayerHP", "Fill_PlayerMP", "Frame_Description", "Frame_EnemyHP", "Frame_EnemyStun", "Frame_Option", "Frame_Platina", "Frame_PlayerHP",
                 "Frame_PlayerMP", "Frame_Prediction", "Frame_SkillItems", "Frame_TurnOrder", "Frame_TurnOrderCur", "Frame_VictoryDialogue", "Frame_VictoryLoot", "Overlay_CPPip", "Overlay_EnemyHPIcon",
                 "Overlay_Exclamation", "Overlay_HPIcon", "Overlay_IconArcane", "Overlay_IconBane", "Overlay_IconBuff", "Overlay_IconDebuff", "Overlay_IconFlame", "Overlay_IconHealing", "Overlay_IconPhysical", 
                 "Overlay_IconStun", "Overlay_IconTarget", "Overlay_OptionSelect", "Overlay_SkillItemSelect", "Overlay_SkillItemShade", "Overlay_VictorySelection", "Overlay_VictoryShade", "SpeechBubble",  
                 "SpeechBubbleBot", "Scrollbar_Scroller", "Scrollbar_Static"}
fnRip(sDirPath, sPrefix, saNames)

-- |[ ======================================== Clean Up ======================================== ]|
--Finish
SLF_Close()