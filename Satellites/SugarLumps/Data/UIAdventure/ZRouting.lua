-- |[UI Adventure]|
--UI elements used for Adventure Mode.
SLF_Open("Output/UIAdventure.slf")
ImageLump_SetCompression(1)

--Routing. Call each of the subfolders.
local sBasePath = fnResolvePath()
LM_ExecuteScript(sBasePath .. "Dialogue/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "Icons/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "StaminaBar/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "WorldOverlays/ZRouting.lua")

--Finish
SLF_Close()