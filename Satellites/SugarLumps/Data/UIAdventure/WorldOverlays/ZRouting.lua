-- |[ ===================================== World Overlays ===================================== ]|
--Used for areas like deep forests or underwater, these overlays provide some interesting flavour
-- to the game's visual style.
local sBasePath = fnResolvePath()

--Autoloader Variables
local sAutoloaderName = "AutoLoad|Adventure|WorldOverlays"
local sDLPath = "Root/Images/AdventureUI/MapOverlays/"

--Create Autoload Lump
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[Utility]|
fnRipImageAuto("Overlay|Fog",        sBasePath .. "FogOverlay.png",        0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "Fog")
fnRipImageAuto("Overlay|FogDeep",    sBasePath .. "FogOverlayDeep.png",    0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "FogDeep")
fnRipImageAuto("Overlay|ForestA",    sBasePath .. "ForestOverlayA.png",    0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "ForestA")
fnRipImageAuto("Overlay|ForestB",    sBasePath .. "ForestOverlayB.png",    0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "ForestB")
fnRipImageAuto("Overlay|ForestC",    sBasePath .. "ForestOverlayC.png",    0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "ForestC")
fnRipImageAuto("Overlay|Water",      sBasePath .. "WaterOverlay.png",      0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "Water")
fnRipImageAuto("Overlay|Underwater", sBasePath .. "UnderwaterOverlay.png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "Underwater")

-- |[Backgrounds]|
fnRipImageAuto("Underlay|Clouds",         sBasePath .. "Clouds.png",         0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "Clouds")
fnRipImageAuto("Underlay|PlateauSunset0", sBasePath .. "PlateauSunset0.png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "PlateauSunset0")
fnRipImageAuto("Underlay|PlateauSunset1", sBasePath .. "PlateauSunset1.png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "PlateauSunset1")
fnRipImageAuto("Underlay|UmumAsru",       sBasePath .. "UmumAsru.png",       0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "UmumAsru")
fnRipImageAuto("Underlay|ServerBanks",    sBasePath .. "ServerBanks.png",    0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. "ServerBanks")
