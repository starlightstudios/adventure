-- |[ ======================================= Stamina Bar ====================================== ]|
--The small bar that appears in the top-left corner of the screen when the player is in control.
local sBasePath = fnResolvePath()

--Variable setup.
local sPrefix = "AdvStamina|"
local saNames = {"PlatinumCompass", "StaminaBarMiddle", "StaminaBarOver", "StaminaBarUnder", "StaminaRingOver", "StaminaRingUnder", "StaminaRingDanger"}
local saPaths = {"PlatinumCompass", "StaminaBarMiddle", "StaminaBarOver", "StaminaBarUnder", "StaminaRingOver", "StaminaRingUnder", "StaminaRingDanger"}

--Autoloader Variables
local sAutoloaderName = "AutoLoad|Adventure|StaminaBar"
local sDLPath = "Root/Images/AdventureUI/Stamina/"

--Create Autoload Lump
SLF_RegisterAutoLoadLump(sAutoloaderName)

--Ripping loop.
for i = 1, #saNames, 1 do
	ImageLump_Rip(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0)
    AutoLoader_Register(sAutoloaderName, sPrefix .. saNames[i], sDLPath .. saNames[i])
end
