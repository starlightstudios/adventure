-- |[ ======================================= Item Icons ======================================= ]|
--Icons used for items. Items are organized by row based on chapter.

-- |[Setup]|
--Variable setup.
local sBasePath = fnResolvePath()

-- |[ =============================== Chapter Completion Badges ================================ ]|
--Variables.
local cfWid = 32
local cfHei = 32
local sPrefix = "CompletionBadge|"
local sPath = sBasePath .. "ChapterComplete.png"

--Autoloader Variables
local sAutoloaderName = "AutoLoad|Adventure|ChapterCompletion"
local sDLPath = "Root/Images/AdventureUI/ChapterComplete/"

--Create Autoload Lump
SLF_RegisterAutoLoadLump(sAutoloaderName)

--Rip.
fnRipImageAuto(sPrefix .. "Chapter1", sPath, 0 * cfWid, 0, cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Chapter1")
fnRipImageAuto(sPrefix .. "Chapter2", sPath, 1 * cfWid, 0, cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Chapter2")
fnRipImageAuto(sPrefix .. "Chapter3", sPath, 2 * cfWid, 0, cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Chapter3")
fnRipImageAuto(sPrefix .. "Chapter4", sPath, 3 * cfWid, 0, cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Chapter4")
fnRipImageAuto(sPrefix .. "Chapter5", sPath, 4 * cfWid, 0, cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Chapter5")
fnRipImageAuto(sPrefix .. "Chapter6", sPath, 5 * cfWid, 0, cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Chapter6")

-- |[ ==================================== 22px Item Icons ===================================== ]|
-- |[22px Item Icons]|
--Variables.
cfWid = 22
cfHei = 22
sPrefix = "AdvItem22Px|"
sPath = sBasePath .. "22pxIcons.png"

--Autoloader Variables
sAutoloaderName = "AutoLoad|Adventure|AdvItemIcons22px"
sDLPath = "Root/Images/AdventureUI/Symbols22/"

--Create Autoload Lump
SLF_RegisterAutoLoadLump(sAutoloaderName)

--Array Setup
local saArray = {}
saArray[ 1] = {"ArmorMei", "WepMeiKatana", "WepMeiJian", "WepMeiBleed"}
saArray[ 2] = {"ArmorFlorentina", "WepFlorentinaKnife", "WepFlorentinaPoisonA", "WepFlorentinaStrike", "WepFlorentinaPoisonB"}
saArray[ 3] = {"SKIP"} --Maram
saArray[ 4] = {"ArmorSanya", "WepSanyaFist", "AmmoSanyaRage", "WepSanyaRifle", "AmmoSanyaPrc"}
saArray[ 5] = {"ArmorIzuna", "WepIzunaSilver"}
saArray[ 6] = {"ArmorEmpress", "WepEmpress"}
saArray[ 7] = {"CollarZeke"}
saArray[ 8] = {"SKIP"} --Jeanne
saArray[ 9] = {"SKIP"} --Aquillia
saArray[10] = {"SKIP"} --Gallena
saArray[11] = {"SKIP"} --Lotta
saArray[12] = {"SKIP"} --Junia
saArray[13] = {"SKIP"} --Edea
saArray[14] = {"ArmorChris", "ArmorChristine", "WepChrisTazer", "WepChristineSpear", "WepChristineFire", "WepChristineIce", "WepChristineShock"}
saArray[15] = {"ArmorTiffany", "WepTiffanyDiffractor", "WepTiffanyRifle", "WepTiffanyBullpup", "WepTiffanySniper"}
saArray[16] = {"ArmorSX399", "WepJX101", "WepSX399Fire", "WepSX399Ice", "WepSX399Rifle"}
saArray[17] = {"SKIP"} --Talia
saArray[18] = {"RuneMei", "RuneMeiMkII"}
saArray[19] = {"RuneSanya", "RuneSanyaMkII"}
saArray[20] = {"SKIP"} --Jeanne Runes
saArray[21] = {"SKIP"} --Lotta Runes
saArray[22] = {"RuneChristine", "RuneChristineMkII"}
saArray[23] = {"SKIP"} --Talia Runes
saArray[24] = {"GemSlot", "SystemUnequip", "AdmPowder", "AdmFlakes", "AdmShards", "AdmPieces", "AdmChunks", "AdmOre", "Platina"}
saArray[25] = {"SKIP"} --Generic Damage Indicators
saArray[26] = {"ArmGenLight", "ArmGenMedium", "ArmGenHeavy", "SKIP", "SKIP", "SKIP", "SKIP", "SKIP", "AccCapeBrown", "AccCapeRed", "AccCapeGay"} 
saArray[27] = {"AccRing", "AccBracer", "AccBoots", "AccGloves", "AccGlovesBlk", "AccGlovesBlue", "SKIP", "SKIP", "AccRingBrnA", "AccRingBrnB", "AccRingBrnC", "AccRingBrnD"}
saArray[28] = {"ItemScope", "ItemSuppressor", "SKIP", "SKIP", "SKIP", "SKIP", "SKIP", "SKIP", "AccRingSlvA", "AccRingSlvB", "AccRingSlvC", "AccRingSlvD"}
saArray[29] = {"ItemPotionRed", "ItemPotionGrn", "ItemMedkit", "ItemFirstAidKid", "ItemSyringe", "ItemInjectorA", "ItemInjectorB", "SKIP", "AccRingGldA", "AccRingGldB", "AccRingGldC", "AccRingGldD", "PendantR", "PendantB", "PendantG", "HarpyBadge"}
saArray[30] = {"ItemTech", "ItemGrenade", "ItemFlashbang", "ItemPamphlet"}
saArray[31] = {"EverlastingHoney", "PepperPie", "SmellingSalts", "Cookies", "Paint", "Bomb"}
saArray[32] = {"GrnPie", "Molotov", "PsnShroom", "BreadA", "BreadB", "Clock"}
saArray[33] = {"Crowbar", "KeyBlack", "KeySilver", "KeyBronze", "Hacksaw", "BoatOars", "ComputerChip", "JunkA", "JunkB"}
saArray[34] = {"BookPurple", "BookBlue", "BookRed", "BookOrange", "BookGreen"}
saArray[35] = {"JarGreen", "JarYellow", "JarRed", "JarPurple", "JarBlue", "JarEmpty"}
saArray[36] = {"FlowerRed", "FlowerWhite", "FlowerPurple", "FlowerBlue", "FlowerPink"}

-- |[Advanced Gems]|
--GRBOVY
saArray[37] = {"Gem1|R",   "Gem1|V",   "Gem1|Y",   "Gem1|O",   "Gem1|G",   "Gem1|B", "SKIP", "GemYellow", "GemPurple", "GemGreen", "GemPink"}
saArray[38] = {"Gem2|RV",  "Gem2|VY",  "Gem2|OV",  "Gem2|GV",  "Gem2|BV",  "Gem2|RY",  "Gem2|RO",  "Gem2|GR",  "Gem2|RB",  "Gem2|OY",  "Gem2|GY",  "Gem2|BY",  "Gem2|GO",  "Gem2|BO",  "Gem2|GB"}
saArray[39] = {"Gem3|RVY", "Gem3|ROV", "Gem3|GRV", "Gem3|RBV", "Gem3|OVY", "Gem3|GVY","Gem3|BVY", "Gem3|GOV", "Gem3|BOV", "Gem3|GBV", "Gem3|ROY", "Gem3|RBY", "Gem3|GRO", "Gem3|GRY", "Gem3|RBO", "Gem3|GRB", "Gem3|GOY", "Gem3|BOY", "Gem3|GBO", "Gem3|GBY"}
saArray[40] = {"Gem4|ROVY", "Gem4|GRVY", "Gem4|RBVY", "Gem4|GROV", "Gem4|RBOV", "Gem4|GRBV", "Gem4|GOVY", "Gem4|BOVY", "Gem4|GBVY", "Gem4|GBOV", "Gem4|GROY", "Gem4|RBOY", "Gem4|GRBY", "Gem4|GRBO", "Gem4|GBOY"}
saArray[41] = {"Gem5|RBOVY", "Gem5|GROVY", "Gem5|GRBVY", "Gem5|GRBOV", "Gem5|GRBOY", "Gem5|GBOVY", "Gem6"}

-- |[Badges]|
saArray[42] = {"Badge|Alraune", "Badge|Bee", "Badge|Ghost", "Badge|Slime", "Badge|Werecat", "Badge|Wisphag"}
saArray[43] = {"Badge|Darkmatter"}

-- |[Ripping Loop]|
for i = 1, #saArray, 1 do
    for p = 1, #saArray[i], 1 do
        
        --"SKIP" is ignored:
        if(saArray[i][p] == "SKIP") then
            
        --Rip.
        else
            ImageLump_Rip(sPrefix .. saArray[i][p], sPath, (p-1) * cfWid, (i - 1) * cfHei, cfWid, cfHei, 0)
            AutoLoader_Register(sAutoloaderName, sPrefix .. saArray[i][p], sDLPath .. saArray[i][p])
        end
    end
end

-- |[ ========================================== Other ========================================= ]|
-- |[Character Faces]|
--This is a sheet, not individual entries.

--Autoloader Variables
sAutoloaderName = "AutoLoad|Adventure|CharacterFaces"
sDLPath = "Root/Images/AdventureUI/Symbols22/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

--Create Autoload Lump
ImageLump_Rip("AdvItemIco|CharacterFaces", sBasePath .. "CharacterFaces.png", 0, 0, -1, -1, 0)
AutoLoader_Register(sAutoloaderName, "AdvItemIco|CharacterFaces", "Root/Images/AdventureUI/ItemIcons/CharacterFaces")
