-- |[ ====================================== Dialogue UI ======================================= ]|
--Very simple UI, has a name panel version and a nameless version. That's it.
local sBasePath = fnResolvePath()

-- |[ =========== Normal Filtering =========== ]|
--Autoloader Variables
local sAutoloaderName = "AutoLoad|Adventure|DialogueUINoFilter"
local sDLPath = "Root/Images/AdventureUI/Dialogue/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

--Variable setup.
local sPrefix = "AdvDialogue|"
local saNames = {"BorderCard", "CommandList", "ExpandArrow", "NameBox", "NamelessBox", "NamePanel", "TextInput"}
local saPaths = {"BorderCard", "CommandList", "ExpandArrow", "NameBox", "NamelessBox", "NamePanel", "TextInput"}

--Ripping loop.
for i = 1, #saNames, 1 do
	ImageLump_Rip(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0)
    AutoLoader_Register(sAutoloaderName, sPrefix .. saNames[i], sDLPath .. saNames[i])
end

-- |[ ========== Alternate Filtering ========= ]|
--These are split up because they need to use a different filtering during loading.
sAutoloaderName = "AutoLoad|Adventure|DialogueUIFilter"
SLF_RegisterAutoLoadLump(sAutoloaderName)

--Rip.
saNames = {"PopupBorderCard", "TextAdventureMapParts", "TextAdventureScrollbar"}
saPaths = {"PopupBorderCard", "TextAdventureMapParts", "TextAdventureScrollbar"}

--Ripping loop.
for i = 1, #saNames, 1 do
	ImageLump_Rip(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0)
    AutoLoader_Register(sAutoloaderName, sPrefix .. saNames[i], sDLPath .. saNames[i])
end