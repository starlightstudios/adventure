-- |[ ======================================== Status UI ======================================= ]|
--Status UI. No interactivity, displays data.
local sBasePath = fnResolvePath()

-- |[Setup]|
SLF_Open("Output/UIAdvMenuStatus.slf")
ImageLump_SetCompression(1)

--Autoloader Variables
local sAutoloaderName = "AutoLoad|Adventure|AdvMenuStatus"
local sDLPath = "Root/Images/AdventureUI/Status/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[ ======================================== Ripping ========================================= ]|
-- |[Lists]|
local sPrefix = "AdvStatus|"
local saNames = {"Arrow Lft", "Arrow Rgt", "Equipment Backing", "Equipment Detail", "Equipment Header", "Header", "Name Banner", "Resistance Backing", "Resistance Detail", "Resistance Header", "Status Backing", "Status Detail", "Status HP Frame", "Status HP Bar Fill", "Status Level Backing", "Status Level Outer", "Status XP Frame", "Status XP Bar Fill"}
local saPaths = saNames

--Ripping loop.
for i = 1, #saNames, 1 do
	fnRipImageAuto(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. saNames[i])
end

-- |[ ======================================== Clean Up ======================================== ]|
--Finish
SLF_Close()


