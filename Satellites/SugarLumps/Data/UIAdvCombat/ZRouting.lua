-- |[ ======================================= Combat UI ======================================== ]|
--WAAAAAAAAAAAAAAAAAARRRRRRRRRRRRRR!
local sBasePath = fnResolvePath()

-- |[Setup]|
SLF_Open("Output/AdvCombat.slf")
ImageLump_SetCompression(1)

-- |[Autoloader]|
local sAutoloaderName = "AutoLoad|Adventure|UIAdvCombat"
local sAutoloaderPath = "Root/Images/AdventureUI/Combat/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[Function]|
local function fnRip(psDirPath, psPrefix, psaNames, psAutoLump, psAutoPath)
    
    -- |[Argument Check]|
    if(psDirPath  == nil) then return end
    if(psPrefix   == nil) then return end
    if(psaNames   == nil) then return end
    if(psAutoLump == nil) then return end
    if(psAutoPath == nil) then return end
    
    -- |[Iterate]|
    for i = 1, #psaNames, 1 do
        
        --Resolve names.
        local sName = psPrefix  .. psaNames[i]
        local sPath = psDirPath .. psaNames[i] .. ".png"
        
        --Rip.
        fnRipImageAuto(sName, sPath, 0, 0, -1, -1, 0, psAutoLump, psAutoPath .. psPrefix .. psaNames[i])
    
    end
end

-- |[ ======================================= Base Parts ======================================= ]|
local sDirPath = sBasePath
local sPrefix = "Base|"
local saNames = {"BG Gradient", "Card Attack", "Card Job", "Card Memorized", "Card Tactics", "Character Backing", "Character Banner", "Character CP Bar Pip", "Character CP Frame", "Character HP Fill", "Character HP Frame",
                 "Character MP Fill", "Character MP Frame", "Description", "Job Skills Frame", "Memorized Skills Frame", "Tactics Skills Frame"}
fnRip(sDirPath, sPrefix, saNames, sAutoloaderName, sAutoloaderPath)

-- |[ ======================================== Ally Bar ======================================== ]|
sDirPath = sBasePath .. "AllyBar/"
sPrefix = "AllyBar|"
saNames = {"AllyCPPip", "AllyFrame", "AllyHPFill", "AllyMPFill", "AllyPortraitMask"}
fnRip(sDirPath, sPrefix, saNames, sAutoloaderName, sAutoloaderPath)

-- |[ ==================================== Defeat Overlay ====================================== ]|
sDirPath = sBasePath .. "Defeat/"
sPrefix = "Defeat|"
saNames = {"Defeat_0", "Defeat_1", "Defeat_2", "Defeat_3", "Defeat_4", "Defeat_5"}
fnRip(sDirPath, sPrefix, saNames, sAutoloaderName, sAutoloaderPath)

-- |[ =================================== Enemy Health Bar ===================================== ]|
sDirPath = sBasePath .. "EnemyHealthBar/"
sPrefix = "EnemyHealthBar|"
saNames = {"Enemy HP Fill", "Enemy HP Frame", "Enemy Stun Bar Fill", "Enemy Stun Fill", "Enemy Stun Frame"}
fnRip(sDirPath, sPrefix, saNames, sAutoloaderName, sAutoloaderPath)

-- |[ ====================================== Expert Parts ====================================== ]|
sDirPath = sBasePath .. "Expert UI/"
sPrefix = "Expert|"
saNames = {"CP Fill", "HP Fill", "MP Fill", "Page Arrow D", "Page Arrow U", "Page Frame", "Skills Frame", "Statistic Frames Back", "Statistic Frames"}
fnRip(sDirPath, sPrefix, saNames, sAutoloaderName, sAutoloaderPath)

--CP Animation.
fnRipImageAuto("Expert|CPAnim0", sDirPath .. "CP Anim.png", 23 * 0, 0, 23, 23, 0, sAutoloaderName, sAutoloaderPath .. "CPAnim0")
fnRipImageAuto("Expert|CPAnim1", sDirPath .. "CP Anim.png", 23 * 1, 0, 23, 23, 0, sAutoloaderName, sAutoloaderPath .. "CPAnim1")
fnRipImageAuto("Expert|CPAnim2", sDirPath .. "CP Anim.png", 23 * 2, 0, 23, 23, 0, sAutoloaderName, sAutoloaderPath .. "CPAnim2")
fnRipImageAuto("Expert|CPAnim3", sDirPath .. "CP Anim.png", 23 * 3, 0, 23, 23, 0, sAutoloaderName, sAutoloaderPath .. "CPAnim3")
fnRipImageAuto("Expert|CPAnim4", sDirPath .. "CP Anim.png", 23 * 4, 0, 23, 23, 0, sAutoloaderName, sAutoloaderPath .. "CPAnim4")
fnRipImageAuto("Expert|CPAnim5", sDirPath .. "CP Anim.png", 23 * 5, 0, 23, 23, 0, sAutoloaderName, sAutoloaderPath .. "CPAnim5")
fnRipImageAuto("Expert|CPAnim6", sDirPath .. "CP Anim.png", 23 * 6, 0, 23, 23, 0, sAutoloaderName, sAutoloaderPath .. "CPAnim6")
fnRipImageAuto("Expert|CPAnim7", sDirPath .. "CP Anim.png", 23 * 7, 0, 23, 23, 0, sAutoloaderName, sAutoloaderPath .. "CPAnim7")

--CP Frame Animation.
fnRipImageAuto("Expert|CPFrameAnim0", sDirPath .. "CP Frame Anim.png", 50 * 0, 0, 50, 50, 0, sAutoloaderName, sAutoloaderPath .. "CPFrameAnim0")
fnRipImageAuto("Expert|CPFrameAnim1", sDirPath .. "CP Frame Anim.png", 50 * 1, 0, 50, 50, 0, sAutoloaderName, sAutoloaderPath .. "CPFrameAnim1")
fnRipImageAuto("Expert|CPFrameAnim2", sDirPath .. "CP Frame Anim.png", 50 * 2, 0, 50, 50, 0, sAutoloaderName, sAutoloaderPath .. "CPFrameAnim2")
fnRipImageAuto("Expert|CPFrameAnim3", sDirPath .. "CP Frame Anim.png", 50 * 3, 0, 50, 50, 0, sAutoloaderName, sAutoloaderPath .. "CPFrameAnim3")

-- |[ ======================================= Inspector ======================================== ]|
sDirPath = sBasePath .. "Inspector/"
sPrefix = "Inspector|"
saNames = {"AllyCPPip", "AllyHPFill", "AllyMPFill", "Effects Detail", "Effects Frame", "Effects Scrollbar", "Entities Frame", "Entities Scrollbar", "Header", "Resistances Detail", "Resistances Frame", "Summary Frame",
          "Scrollbar_Front"}
fnRip(sDirPath, sPrefix, saNames, sAutoloaderName, sAutoloaderPath)

-- |[ =================================== Player Interface ===================================== ]|
sDirPath = sBasePath .. "PlayerInterface/"
sPrefix = "PlayerInterface|"
saNames = {"ArrowLft", "ArrowRgt", "PredictionBox"}
fnRip(sDirPath, sPrefix, saNames, sAutoloaderName, sAutoloaderPath)

-- |[ ====================================== Turn Order ======================================== ]|
sDirPath = sBasePath .. "TurnOrder/"
sPrefix = "TurnOrder|"
saNames = {"TurnOrderEnemy", "TurnOrderFriendly", "TurnOrderNext"}
fnRip(sDirPath, sPrefix, saNames, sAutoloaderName, sAutoloaderPath)

-- |[ ======================================== Victory ========================================= ]|
sDirPath = sBasePath .. "Victory/"
sPrefix = "Victory|"
saNames = {"BannerBack", "BannerBig", "DoctorBack", "DoctorFill", "DoctorFrame", "DropFrame", "ExpFrameBack", "ExpFrameFill", "ExpFrameFront", "ExpFrameMask"}
fnRip(sDirPath, sPrefix, saNames, sAutoloaderName, sAutoloaderPath)

-- |[ ===================================== Turn Portraits ===================================== ]|
-- |[Ripper Function]|
--Automated, creates images named "PorSml|Alraune" and such. Uses tables.
local function fnRipImages(psPrefix, psRipPath, psaRipNames, piXValue, piYValue, piWid, piHei, psCharPrefix, psAutoLump, psAutoPath)
	
	-- |[Argument Check]|
	if(psPrefix    == nil) then return end
	if(psRipPath   == nil) then return end
	if(psaRipNames == nil) then return end
	if(piXValue    == nil) then return end
	if(piYValue    == nil) then return end
	--sCharPrefix can be nil. It's only used for ripping the main characters.
	if(psAutoLump  == nil) then return end
	if(psAutoPath  == nil) then return end
	
    -- |[Iteration]|
	--Let 'er rip!
    for i = 1, #psaRipNames, 1 do
		
		--If the name is "SKIP", then don't rip here. This is used because slots are empty until later in some cases.
		if(psaRipNames[i] == "SKIP") then
		else
		
            --Name resolve.
            local sUseName = psPrefix .. psaRipNames[i]
            local sUseAuto = sAutoloaderPath .. psaRipNames[i]
            if(psCharPrefix ~= nil) then
                sUseName = psPrefix .. psCharPrefix .. psaRipNames[i]
                sUseAuto = sAutoloaderPath .. psCharPrefix .. psaRipNames[i]
            end
        
            --Rip.
            fnRipImageAuto(sUseName, psRipPath, piXValue + (piWid * (i-1)), piYValue, piWid, piHei, 0, psAutoLump, sUseAuto)
			
		end
	end
end

-- |[ ====== Character Ripping ===== ]|
--Constants
local ciWid = 84
local ciHei = 54
local sRipPath = sBasePath .. "TurnPortraits/TurnPortraitsSheet.png"

--Autoloader
sAutoloaderName = "AutoLoad|Adventure|UIAdvCombat"
sAutoloaderPath = "Root/Images/AdventureUI/TurnPortraits/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

--Christine's row.
local saRipNames = {"Human", "Male", "Golem", "GolemSerious", "GolemDress", "Latex", "Darkmatter", "Electrosprite", "SteamDroid", "Eldritch", "Raiju", "Doll", "Raibie", "Secrebot"}
fnRipImages("PorSml|", sRipPath, saRipNames, ciWid * 0, ciHei * 0, ciWid, ciHei, "Christine_", sAutoloaderName, sAutoloaderPath)

--Jeanne. Not implemented yet.

--Mei's row.
saRipNames = {"Human", "Alraune", "Bee", "BeeQueen", "Ghost", "Slime", "Werecat", "Gravemarker", "Zombee", "Rubber", "Wisphag", "Mannequin", "RubberQueen", "ZombeeQueen", "SlimeYellow", "SlimeInk", "SlimeBlue", "SlimePink"}
fnRipImages("PorSml|", sRipPath, saRipNames, ciWid * 0, ciHei * 2, ciWid, ciHei, "Mei_", sAutoloaderName, sAutoloaderPath)

--Sanya's row.
saRipNames = {"Human", "Sevavi", "Kitsune", "Dragon", "Werebat", "Bunny", "Harpy"}
fnRipImages("PorSml|", sRipPath, saRipNames, ciWid * 0, ciHei * 3, ciWid, ciHei, "Sanya_", sAutoloaderName, sAutoloaderPath)

--Lotta. Not implemented yet.

--Talia. Not implemented yet.

--Other Party Members. They don't have multiple forms.
saRipNames = {"Florentina", "TiffanyAssault", "JX-101", "SX-399", "Izuna_Miko", "Zeke_Goat", "Empress_Conquerer"}
fnRipImages("PorSml|", sRipPath, saRipNames, ciWid * 0, ciHei * 6, ciWid, ciHei, nil, sAutoloaderName, sAutoloaderPath)

--First row of classes for other party members.
saRipNames = {"FlorentinaTH", "TiffanySubvert"}
fnRipImages("PorSml|", sRipPath, saRipNames, ciWid * 0, ciHei * 7, ciWid, ciHei, nil, sAutoloaderName, sAutoloaderPath)

--Second row of classes for other party members.
saRipNames = {"FlorentinaAg", "TiffanySupport"}
fnRipImages("PorSml|", sRipPath, saRipNames, ciWid * 0, ciHei * 8, ciWid, ciHei, nil, sAutoloaderName, sAutoloaderPath)

--Third row of classes.
saRipNames = {"FlorentinaLurk", "TiffanySpearhead"}
fnRipImages("PorSml|", sRipPath, saRipNames, ciWid * 0, ciHei * 9, ciWid, ciHei, nil, sAutoloaderName, sAutoloaderPath)

--Fourth row of classes.
saRipNames = {"SKIP", "TiffanySundress"}
fnRipImages("PorSml|", sRipPath, saRipNames, ciWid * 0, ciHei * 10, ciWid, ciHei, nil, sAutoloaderName, sAutoloaderPath)

-- |[ ======== Enemy Ripping ======= ]|
sRipPath = sBasePath .. "TurnPortraits/TurnPortraitsSheetEnemies.png"

--Chapter 1
saRipNames = {"Alraune", "Bee", "CultistF", "CultistM", "Ghost", "Slime", "Werecat", "Zombee", "SkullCrawler", "Arachnophelia", "Infirm", "BeeBuddy", "BestFriend", "Gravemarker", "Jelli", "MirrorImage", "WerecatThief",
              "Sproutling", "Sproutpecker", "EyeDrawer", "BloodyMirror", "CandleHaunt", "Mycela", "SlimeG", "SlimeB", "InkSlime", "Mushraune", "WerecatR", "AlrauneR", "BeeR", "WerecatThiefR", "BanditM", "BanditF",
              "BanditCatB", "MercDemon", "Victoria", "MannBanditF", "MannBanditM"}
fnRipImages("PorSml|", sRipPath, saRipNames, ciWid * 0, ciHei * 0, ciWid, ciHei, nil, sAutoloaderName, sAutoloaderPath)

--Chapter 2
saRipNames = {"Sevavi", "Bat", "Brimhog", "Bunny", "Butterbomber", "CaveCrawler", "HarpyOfficer", "HarpyRecruit", "Kitsune", "Omnigoose", "PoisonVine", "Treant", "Turkerus", "Unielk", "Buffodil", "Enforcer",
              "BanditG", "BanditR", "FrostCrawler", "Grub", "GrubNest", "RedCap", "SuckFly", "Toxishroom", "WispHag", "BunEye", "Pseudogriffon", "Raccoon"}
fnRipImages("PorSml|", sRipPath, saRipNames, ciWid * 0, ciHei * 1, ciWid, ciHei, nil, sAutoloaderName, sAutoloaderPath)

--Chapter 5
saRipNames = {"Doll", "Golem", "GolemLord", "LatexDrone", "Scraprat", "SecurityWrecked", "DarkmatterGirl", "Dreamer", "Vivify", "TechMonstrosity", "VoidRift", "Horrible", "Serenity", "609144", "Raibie", "BandageGoblin", "Hoodie", "InnGeisha", "Motilvac", "MrWipey", "Secrebot", "Plannerbot", "EnemyChristineRaibie"}
fnRipImages("PorSml|", sRipPath, saRipNames, ciWid * 0, ciHei * 4, ciWid, ciHei, nil, sAutoloaderName, sAutoloaderPath)

--Allies and Other
saRipNames = {"BeeAlly"}
fnRipImages("PorSml|", sRipPath, saRipNames, ciWid * 0, ciHei * 6, ciWid, ciHei, nil, sAutoloaderName, sAutoloaderPath)

-- |[ ========== Paragons ========== ]|
sRipPath = sBasePath .. "TurnPortraits/TurnPortraitsSheetParagons.png"
sAutoloaderPath = "Root/Images/AdventureUI/TurnPortraitsParagon/"

--Chapter 1
saRipNames = {"Alraune", "Bee", "CultistF", "CultistM", "Ghost", "Slime", "Werecat", "Zombee", "SkullCrawler", "SKIP", "SKIP", "BeeBuddy", "BestFriend", "Gravemarker", "Jelli", "MirrorImage", "WerecatThief", "Sproutling", "Sproutpecker", "EyeDrawer", "BloodyMirror", "CandleHaunt", "SKIP", "SlimeG", "SlimeB", "InkSlime", "Mushraune", "WerecatR", "AlrauneR", "BeeR", "WerecatThiefR", "BanditM", "BanditF", "BanditCatB", "MercDemon"}
fnRipImages("PorSmlParagon|", sRipPath, saRipNames, ciWid * 0, ciHei * 0, ciWid, ciHei, nil, sAutoloaderName, sAutoloaderPath)

--Chapter 2
saRipNames = {"Sevavi", "Bat", "Brimhog", "Bunny", "Butterbomber", "CaveCrawler", "HarpyOfficer", "HarpyRecruit", "Kitsune", "Omnigoose", "PoisonVine", "Treant", "Turkerus", "Unielk", "Buffodil", "Enforcer",
              "BanditG", "BanditR", "FrostCrawler", "Grub", "GrubNest", "RedCap", "SuckFly", "Toxishroom", "WispHag", "BunEye", "Pseudogriffon"}
fnRipImages("PorSmlParagon|", sRipPath, saRipNames, ciWid * 0, ciHei * 1, ciWid, ciHei, nil, sAutoloaderName, sAutoloaderPath)

--Chapter 5
saRipNames = {"Doll", "Golem", "GolemLord", "LatexDrone", "Scraprat", "SecurityWrecked", "DarkmatterGirl", "Dreamer", "SKIP", "SKIP", "VoidRift", "Horrible", "Serenity", "SKIP", "Raibie", "BandageGoblin", "Hoodie", 
			  "InnGeisha", "Motilvac", "MrWipey", "Secrebot", "Plannerbot", "SKIP"}
fnRipImages("PorSmlParagon|", sRipPath, saRipNames, ciWid * 0, ciHei * 4, ciWid, ciHei, nil, sAutoloaderName, sAutoloaderPath)

-- |[ ======================================== Clean Up ======================================== ]|
--Finish
SLF_Close()