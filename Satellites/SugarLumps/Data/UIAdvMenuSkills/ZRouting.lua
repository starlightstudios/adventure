-- |[ ================================ Adventure Menu - Skills ================================= ]|
--Skills UI. Description and Memorized Frame/Jobs Frame are shared with the combat UI.
local sBasePath = fnResolvePath()

-- |[Setup]|
SLF_Open("Output/UIAdvMenuSkills.slf")
ImageLump_SetCompression(1)

--Autoloader Variables
local sAutoloaderName = "AutoLoad|Adventure|AdvMenuSkills"
local sDLPath = "Root/Images/AdventureUI/SkillsMenu/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[ ======================================== Ripping ========================================= ]|
-- |[Lists]|
local sPrefix = "AdvMenuSkill|"
local saNames = {"Arrow Lft", "Arrow Rgt", "Buy Skill Popup", "Class Default Icon", "Description Frame", "Equipped Icon", "Header", "Job Skills Frame", "Jobs Detail", "Jobs Frame", "Jobs Scrollbar", "Memorized Frame", 
                 "Skills Detail", "Skills Detail Special", "Skills Frame", "Skills Scrollbar", "Tactics Skills Frame", "Scrollbar Scroller"}
local saPaths = saNames

--Ripping loop.
for i = 1, #saNames, 1 do
	fnRipImageAuto(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. saNames[i])
end

-- |[ ======================================== Profiles ======================================== ]|
sPrefix = "AdvMenuSkill|"
saNames = {"Profiles Detail", "Profiles Frame", "Profiles Header", "Profiles Instructions Frame", "Profiles Memorized", "Profiles Scrollbar"}
saPaths = saNames

--Ripping loop.
for i = 1, #saNames, 1 do
	fnRipImageAuto(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. saNames[i])
end

-- |[ ======================================== Clean Up ======================================== ]|
--Finish
SLF_Close()