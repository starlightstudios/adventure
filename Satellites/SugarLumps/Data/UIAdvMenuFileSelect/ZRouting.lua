-- |[ ===================================== File Select UI ===================================== ]|
--File Selection UI.
local sBasePath = fnResolvePath()

-- |[Setup]|
SLF_Open("Output/UIAdvMenuFileSelect.slf")
ImageLump_SetCompression(1)

--Autoloader Variables
local sAutoloaderName = "AutoLoad|Adventure|AdvMenuFileSelect"
local sDLPath = "Root/Images/AdventureUI/FileSelect/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[ ======================================== Ripping ========================================= ]|
-- |[Lists]|
local sPrefix = "AdvFileSelect|"
local saNames = {"Arrow Lft", "Arrow Rgt", "Grid Backing", "Header", "New File"}
local saPaths = saNames

--Ripping loop.
for i = 1, #saNames, 1 do
	fnRipImageAuto(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. saNames[i])
end

-- |[ ======================================== Clean Up ======================================== ]|
--Finish
SLF_Close()


