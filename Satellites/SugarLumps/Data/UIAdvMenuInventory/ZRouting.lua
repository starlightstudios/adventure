-- |[ ====================================== Inventory UI ====================================== ]|
--Inventory UI. Get item listings and descriptions.
local sBasePath = fnResolvePath()

-- |[Setup]|
SLF_Open("Output/UIAdvMenuInventory.slf")
ImageLump_SetCompression(1)

--Autoloader Variables
local sAutoloaderName = "AutoLoad|Adventure|AdvMenuInventory"
local sDLPath = "Root/Images/AdventureUI/Inventory/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[ ======================================== Ripping ========================================= ]|
-- |[Lists]|
local sPrefix = "AdvInventory|"
local saNames = {"Deconstruct Box", "Description Frame", "Expandable Header", "Header", "Item Listing Detail", "Item Listing Frame", "Scrollbar Back", "Scrollbar Front", "Sort Arrow"}
local saPaths = saNames

--Ripping loop.
for i = 1, #saNames, 1 do
	fnRipImageAuto(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. saNames[i])
end

-- |[ ======================================== Clean Up ======================================== ]|
--Finish
SLF_Close()


