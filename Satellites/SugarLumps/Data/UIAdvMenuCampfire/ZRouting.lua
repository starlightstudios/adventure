-- |[ ==================================== Campfire Menu UI ==================================== ]|
--Campfire menu, available at campfires, benches, etc.
local sBasePath = fnResolvePath()

-- |[Setup]|
--File.
SLF_Open("Output/UIAdvMenuCampfire.slf")
ImageLump_SetCompression(1)

--Autoloader Variables
local sAutoloaderName = "AutoLoad|Adventure|AdvMenuCampfire"
local sDLPath = "Root/Images/AdventureUI/CampfireMenu/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[ ========================= Functions ======================== ]|
-- |[Globals]|
--Master List. Stores icon rip information of a prefix and a table of entries.
local zaIconMasterList = {}

--Names
local sIconImage = sBasePath .. "Dummy.png"
local sIconPrefix = "FillIn|"

--Icon Sizes
local cfIconW = 96.0
local cfIconH = 96.0
local cfIconOffX = 3.0
local cfIconOffY = 3.0
local cfIconSizeX = 90.0
local cfIconSizeY = 90.0

-- |[fnAddCharacter]|
--Adds a character to the icon master list. Pattern: {sCharName, {sCharClassA, sCharClassB, sCharClassC}}
local function fnAddCharacter(psName, saIconNames)
    local zEntry = {}
    zEntry.sName = psName
    zEntry.saIcons = saIconNames
    table.insert(zaIconMasterList, zEntry)
end

-- |[fnRipFromMasterList]|
--Runs across the list zaIconMasterList and rips icons using the above provided pattern of prefix and entry tables.
local function fnRipFromMasterList()

    --Flag setup:
    ImageLump_SetBlockTrimmingFlag(true)
    
    --For each character:
    for i = 1, #zaIconMasterList, 1 do

        --Variables.
        local sCharacterName = zaIconMasterList[i].sName

        --For each icon:
        for p = 1, #zaIconMasterList[i].saIcons, 1 do
            
            --If it's NULL, skip it.
            local sIconName = zaIconMasterList[i].saIcons[p]
            if(sIconName == "NULL") then
                --skip
            else
                local sName = sIconPrefix .. sCharacterName .. sIconName
                local sAutoPath = sDLPath .. sCharacterName .. sIconName
                fnRipImageAuto(sName, sIconImage, (cfIconW * (p-1)) + cfIconOffX, (cfIconH * (i-1)) + cfIconOffY, cfIconSizeX, cfIconSizeY, 0, sAutoloaderName, sAutoPath)
            end
        end
    end
    
    --Clean.
    ImageLump_SetBlockTrimmingFlag(false)
end

-- |[ ===================== Normal Filtering ===================== ]|
--Setup.
local sPrefix = "AdvCampfireMenu|"
local saNames = {"Header", "Footer", "MapPin", "MapPinSelected", "Map Location Backing", "Map Location Scrollbar", "Pin_Accuracy", "Pin_Evade", "Pin_Health", "Pin_Initiative", "Pin_Power", "Pin_Skill"}
local saPaths = saNames

--Ripping loop.
for i = 1, #saNames, 1 do
	fnRipImageAuto(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. saNames[i])
end

-- |[ =================== Alternate Filtering ==================== ]|
--Icons use a modified filtering set.
local iIconSizeX = 96
local iIconSizeY = 96
local sIconPath = sBasePath .. "Campfire Icons/Campfire Icons.png"

-- |[Menu Base Icons]|
--Setup.
sDLPath = "Root/Images/AdventureUI/CampfireMenuIcon/"
sPrefix = "AdvCampfireIcon|"

--Rip.
ImageLump_SetBlockTrimmingFlag(true)
fnRipImageAuto(sPrefix .. "Rest",               sIconPath, iIconSizeX * 0, iIconSizeY * 0, iIconSizeX, iIconSizeY, 0, sAutoloaderName, sDLPath .. "Rest")
fnRipImageAuto(sPrefix .. "Unavailable",        sIconPath, iIconSizeX * 0, iIconSizeY * 1, iIconSizeX, iIconSizeY, 0, sAutoloaderName, sDLPath .. "Unavailable")
fnRipImageAuto(sPrefix .. "NoCostumes",         sIconPath, iIconSizeX * 1, iIconSizeY * 1, iIconSizeX, iIconSizeY, 0, sAutoloaderName, sDLPath .. "NoCostumes")
fnRipImageAuto(sPrefix .. "Chat",               sIconPath, iIconSizeX * 1, iIconSizeY * 0, iIconSizeX, iIconSizeY, 0, sAutoloaderName, sDLPath .. "Chat")
fnRipImageAuto(sPrefix .. "Save",               sIconPath, iIconSizeX * 2, iIconSizeY * 0, iIconSizeX, iIconSizeY, 0, sAutoloaderName, sDLPath .. "Save")
fnRipImageAuto(sPrefix .. "Skills",             sIconPath, iIconSizeX * 3, iIconSizeY * 0, iIconSizeX, iIconSizeY, 0, sAutoloaderName, sDLPath .. "Skills")
fnRipImageAuto(sPrefix .. "TransformMei",       sIconPath, iIconSizeX * 4, iIconSizeY * 0, iIconSizeX, iIconSizeY, 0, sAutoloaderName, sDLPath .. "TransformMei")
fnRipImageAuto(sPrefix .. "TransformSanya",     sIconPath, iIconSizeX * 4, iIconSizeY * 1, iIconSizeX, iIconSizeY, 0, sAutoloaderName, sDLPath .. "TransformSanya")
fnRipImageAuto(sPrefix .. "TransformChristine", sIconPath, iIconSizeX * 4, iIconSizeY * 4, iIconSizeX, iIconSizeY, 0, sAutoloaderName, sDLPath .. "TransformChristine")
fnRipImageAuto(sPrefix .. "Warp",               sIconPath, iIconSizeX * 5, iIconSizeY * 0, iIconSizeX, iIconSizeY, 0, sAutoloaderName, sDLPath .. "Warp")
fnRipImageAuto(sPrefix .. "ReliveMei",          sIconPath, iIconSizeX * 6, iIconSizeY * 0, iIconSizeX, iIconSizeY, 0, sAutoloaderName, sDLPath .. "ReliveMei")
fnRipImageAuto(sPrefix .. "ReliveSanya",        sIconPath, iIconSizeX * 6, iIconSizeY * 1, iIconSizeX, iIconSizeY, 0, sAutoloaderName, sDLPath .. "ReliveSanya")
fnRipImageAuto(sPrefix .. "ReliveChristine",    sIconPath, iIconSizeX * 6, iIconSizeY * 4, iIconSizeX, iIconSizeY, 0, sAutoloaderName, sDLPath .. "ReliveChristine")
fnRipImageAuto(sPrefix .. "Costume",            sIconPath, iIconSizeX * 7, iIconSizeY * 0, iIconSizeX, iIconSizeY, 0, sAutoloaderName, sDLPath .. "Costume")
fnRipImageAuto(sPrefix .. "Password",           sIconPath, iIconSizeX * 8, iIconSizeY * 0, iIconSizeX, iIconSizeY, 0, sAutoloaderName, sDLPath .. "Password")
ImageLump_SetBlockTrimmingFlag(false)

-- |[Patterend Icon Setup]|
--Variables used for icons ripped according to specific patterns, usually associated with characters or chapters.
sAutoloaderName = "AutoLoad|Adventure|AdvMenuCampfireFilter"
sDLPath = "Root/Images/AdventureUI/CampfireMenu/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[Chat Icons]|
--Used for talking to party members.
sIconImage = sBasePath .. "Chat Icons/Chat Icon Sheet.png"
sIconPrefix = "AdvCampfireChatIcon|"
cfIconW = 192.0
cfIconH = 192.0
cfIconOffX = 6.0
cfIconOffY = 6.0
cfIconSizeX = 180.0
cfIconSizeY = 180.0
sDLPath = "Root/Images/AdventureUI/ChatIcons/"

--Build list.
zaIconMasterList = {}
fnAddCharacter("Florentina", {"Merchant", "Mediator", "TreasureHunter", "Mushraune", "Mannequin"})
fnAddCharacter("Tiffany",    {"All", "Sundress"})
fnAddCharacter("SX-399",     {"Shocktrooper"})
fnAddCharacter("Sophie",     {"Normal", "Gala"})
fnAddCharacter("Izuna",      {"Miko"})
fnAddCharacter("Zeke",       {"Goat"})
fnAddCharacter("Empress",    {"Conquerer"})

--Rip.
fnRipFromMasterList()

-- |[Form Icons]|
--Icons used for forms in the transformation, chat, costume, etc menus.
sIconImage = sBasePath .. "Form Icons/Form Icons.png"
sIconPrefix = "AdvCampfireFormIcon|"
cfIconW = 96.0
cfIconH = 96.0
cfIconOffX = 3.0
cfIconOffY = 3.0
cfIconSizeX = 90.0
cfIconSizeY = 90.0
sDLPath = "Root/Images/AdventureUI/FormIcons/"

--Build list.
zaIconMasterList = {}
fnAddCharacter("Mei",        {"Human", "Alraune", "Bee", "Ghost", "Gravemarker", "Slime", "Werecat", "Wisphag", "Mannequin"})
fnAddCharacter("Sanya",      {"Human", "Kitsune", "Sevavi", "Werebat", "Bunny", "Harpy"})
fnAddCharacter("Jeanne",     {"NULL"})
fnAddCharacter("Lotta",      {"NULL"})
fnAddCharacter("Christine",  {"Human", "Golem", "LatexDrone", "Raiju", "SteamDroid", "Electrosprite", "Eldritch", "Darkmatter", "Doll", "Secrebot"})
fnAddCharacter("Talia",      {"NULL"})
fnAddCharacter("Florentina", {"Merchant", "Mediator", "TreasureHunter", "Agarist", "Lurker"})
fnAddCharacter("Maram",      {"NULL"})
fnAddCharacter("Izuna",      {"Miko"})
fnAddCharacter("Empress",    {"Conquerer"})
fnAddCharacter("Zeke",       {"Goat"})
fnAddCharacter("Aquillia",   {"NULL"})
fnAddCharacter("Gallena",    {"NULL"})
fnAddCharacter("Junia",      {"NULL"})
fnAddCharacter("Edea",       {"NULL"})
fnAddCharacter("Tiffany",    {"Assault", "Support", "Subvert", "Spearhead"})
fnAddCharacter("SX-399",     {"Shocktrooper"})

--Rip.
fnRipFromMasterList()

-- |[Relive Icons]|
--Icons that indicate relive sequences for the player. Uses the same sizing as the form icons.
sIconImage = sBasePath .. "Relive Icons/Relive Icon Sheet.png"
sIconPrefix = "AdvCampfireReliveIcon|"
sDLPath = "Root/Images/AdventureUI/AdvCampfireReliveIcon/"

--Build list.
zaIconMasterList = {}
fnAddCharacter("Mei",       {"AlrauneC", "AlrauneV", "BeeV", "BeeC", "GhostV", "GhostC", "SlimeV", "SlimeC", "SlimeM", "WerecatC", "WerecatV", "Gravemarker", "Zombee", "Wisphag", "WisphagV", "Mannequin"})
fnAddCharacter("Sanya",     {"Null"})
fnAddCharacter("Jeanne",    {"Null"})
fnAddCharacter("Lotta",     {"Null"})
fnAddCharacter("Christine", {"Doll", "Golem", "LatexC", "LatexV", "Darkmatter", "Dreamer", "Electrosprite", "SteamDroid", "Raiju", "Secrebot", "SecrebotBadEnd"})
fnAddCharacter("Talia",     {"Null"})

--Rip.
fnRipFromMasterList()

-- |[Warp Icons]|
--Icons that indicate regions the player can warp to.
sIconPath = sBasePath .. "Warp Icons/Warp Icons.png"
sPrefix = "AdvCampfireWarpIcon|"
sDLPath = "Root/Images/AdventureUI/AdvCampfireWarpIcon/"

--Chapter 1
fnRipImageAuto(sPrefix .. "Trannadar",  sIconPath, iIconSizeX * 0, iIconSizeY * 0, iIconSizeX, iIconSizeY, 0, sAutoloaderName, sDLPath .. "Trannadar")
fnRipImageAuto(sPrefix .. "TrannadarW", sIconPath, iIconSizeX * 1, iIconSizeY * 0, iIconSizeX, iIconSizeY, 0, sAutoloaderName, sDLPath .. "TrannadarW")

--Chapter 2
fnRipImageAuto(sPrefix .. "Northwoods",  sIconPath, iIconSizeX * 0, iIconSizeY * 1, iIconSizeX, iIconSizeY, 0, sAutoloaderName, sDLPath .. "Northwoods")
fnRipImageAuto(sPrefix .. "Westwoods",   sIconPath, iIconSizeX * 1, iIconSizeY * 1, iIconSizeX, iIconSizeY, 0, sAutoloaderName, sDLPath .. "Westwoods")
fnRipImageAuto(sPrefix .. "MtSarulente", sIconPath, iIconSizeX * 2, iIconSizeY * 1, iIconSizeX, iIconSizeY, 0, sAutoloaderName, sDLPath .. "MtSarulente")

--Chapter 5
fnRipImageAuto(sPrefix .. "Regulus",    sIconPath, iIconSizeX * 0, iIconSizeY * 4, iIconSizeX, iIconSizeY, 0, sAutoloaderName, sDLPath .. "Regulus")
fnRipImageAuto(sPrefix .. "Cryogenics", sIconPath, iIconSizeX * 1, iIconSizeY * 4, iIconSizeX, iIconSizeY, 0, sAutoloaderName, sDLPath .. "Cryogenics")
fnRipImageAuto(sPrefix .. "Equinox",    sIconPath, iIconSizeX * 2, iIconSizeY * 4, iIconSizeX, iIconSizeY, 0, sAutoloaderName, sDLPath .. "Equinox")
fnRipImageAuto(sPrefix .. "LRTE",       sIconPath, iIconSizeX * 3, iIconSizeY * 4, iIconSizeX, iIconSizeY, 0, sAutoloaderName, sDLPath .. "LRTE")
fnRipImageAuto(sPrefix .. "LRTW",       sIconPath, iIconSizeX * 4, iIconSizeY * 4, iIconSizeX, iIconSizeY, 0, sAutoloaderName, sDLPath .. "LRTW")
fnRipImageAuto(sPrefix .. "Biolabs",    sIconPath, iIconSizeX * 5, iIconSizeY * 4, iIconSizeX, iIconSizeY, 0, sAutoloaderName, sDLPath .. "Biolabs")

-- |[ ======================================== Clean Up ======================================== ]|
--Finish
SLF_Close()
