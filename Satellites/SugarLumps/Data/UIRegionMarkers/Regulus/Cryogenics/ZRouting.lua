-- |[ ================================== Cryogenics Facility =================================== ]|
--Name Pattern: cryogenics_research_XX
--Starts at 1, goes to 22

--Create.
SLF_Open("Output/UIRegionCryogenics.slf")
ImageLump_SetCompression(1)

--Execute.
local sBasePath = fnResolvePath()
local sPrefix = "Cryogenics|"
for i = 1, 22, 1 do
    local sNumber = string.format("%02i", i)
    local sNumberMinus = string.format("%02i", i-1)
	ImageLump_Rip(sPrefix .. sNumberMinus, sBasePath .. "cryogenics_research_" .. sNumber .. ".png", 0, 0, -1, -1, gciNoTransparencies)
end

--Clean.
SLF_Close()
