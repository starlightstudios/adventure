-- |[ ================================== Serenity Observatory ================================== ]|
--Name Pattern: serenity_observatory_XX
--Starts at 0, goes to 21

--Create.
SLF_Open("Output/UIRegionSerenityObservatory.slf")
ImageLump_SetCompression(1)

--Execute.
local sBasePath = fnResolvePath()
local sPrefix = "SerenityObservatory|"
for i = 0, 21, 1 do
    local sNumber = string.format("%02i", i)
	ImageLump_Rip(sPrefix .. sNumber, sBasePath .. "serenity_observatory_" .. sNumber .. ".png", 0, 0, -1, -1, gciNoTransparencies)
end

--Clean.
SLF_Close()

