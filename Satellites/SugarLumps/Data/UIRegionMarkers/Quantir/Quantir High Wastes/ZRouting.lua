-- |[ ================================== Quantir High Wastes =================================== ]|
--Name Pattern: area_quantir_high_wastes_XX
--Starts at 1, goes to 22

--Create.
SLF_Open("Output/UIRegionQuantirHighWastes.slf")
ImageLump_SetCompression(1)

--Execute.
local sBasePath = fnResolvePath()
local sPrefix = "QuantirHighWastes|"
for i = 1, 22, 1 do
    local sNumber = string.format("%02i", i)
    local sNumberMinus = string.format("%02i", i-1)
	ImageLump_Rip(sPrefix .. sNumberMinus, sBasePath .. "area_quantir_high_wastes_" .. sNumber .. ".png", 0, 0, -1, -1, 0)
end

--Clean.
SLF_Close()
