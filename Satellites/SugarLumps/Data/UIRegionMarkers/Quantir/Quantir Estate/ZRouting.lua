-- |[ ===================================== Quantir Estate ===================================== ]|
--Name Pattern: area_quantir_estate_XX
--Starts at 1, goes to 22

--Create.
SLF_Open("Output/UIRegionQuantirEstate.slf")
ImageLump_SetCompression(1)

--Execute.
local sBasePath = fnResolvePath()
local sPrefix = "QuantirEstate|"
for i = 1, 22, 1 do
    local sNumber = string.format("%02i", i)
    local sNumberMinus = string.format("%02i", i-1)
	ImageLump_Rip(sPrefix .. sNumberMinus, sBasePath .. "area_quantir_estate_" .. sNumber .. ".png", 0, 0, -1, -1, 0)
end

--Clean.
SLF_Close()
