-- |[ ======================================= Northwoods ======================================= ]|
--Name Pattern: area_northwoods_XX
--Starts at 1, goes to 27

-- |[Variables]|
local sDatafileName     = "UIRegionNorthwoods.slf"
local sPrefix           = "Northwoods|"
local sPathIntermediate = "area_northwoods_"
local iLoCount          = 1
local iHiCount          = 27

-- |[Creation]|
SLF_Open("Output/" .. sDatafileName)
ImageLump_SetCompression(1)

-- |[Ripping]|
local sBasePath = fnResolvePath()
for i = iLoCount, iHiCount, 1 do
    local sNumber      = string.format("%02i", i)
    local sNumberMinus = string.format("%02i", i-iLoCount)
	ImageLump_Rip(sPrefix .. sNumberMinus, sBasePath .. sPathIntermediate .. sNumber .. ".png", 0, 0, -1, -1, 0)
end

-- |[Finish Up]|
SLF_Close()

