-- |[ ======================================== Westwoods ======================================= ]|
--Name Pattern: area_westwoods_XX
--Starts at 0, goes to 26

-- |[Variables]|
local sDatafileName     = "UIRegionWestwoods.slf"
local sPrefix           = "Westwoods|"
local sPathIntermediate = "area_westwoods_"
local iLoCount          = 0
local iHiCount          = 26

-- |[Creation]|
SLF_Open("Output/" .. sDatafileName)
ImageLump_SetCompression(1)

-- |[Ripping]|
local sBasePath = fnResolvePath()
for i = iLoCount, iHiCount, 1 do
    local sNumber      = string.format("%02i", i)
    local sNumberMinus = string.format("%02i", i-iLoCount)
	ImageLump_Rip(sPrefix .. sNumberMinus, sBasePath .. sPathIntermediate .. sNumber .. ".png", 0, 0, -1, -1, 0)
end

-- |[Finish Up]|
SLF_Close()

