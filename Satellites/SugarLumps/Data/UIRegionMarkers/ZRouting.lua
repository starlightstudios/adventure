-- |[ ===================================== Region Markers ===================================== ]|
--These appear in the top right when the player is in a given region. The first time the player
-- enters a new area, an animation plays out.

--Routing. Call each of the subfolders.
local sBasePath = fnResolvePath()

-- |[Other]|
local sOtherPath = sBasePath .. "Other/"
LM_ExecuteScript(sOtherPath .. "Nix Nedar/ZRouting.lua")

-- |[Quantir]|
local sQuantirPath = sBasePath .. "Quantir/"
LM_ExecuteScript(sQuantirPath .. "Quantir Estate/ZRouting.lua")
LM_ExecuteScript(sQuantirPath .. "Quantir High Wastes/ZRouting.lua")

-- |[Regulus]|
local sRegulusPath = sBasePath .. "Regulus/"
LM_ExecuteScript(sRegulusPath .. "Cryogenics/ZRouting.lua")
LM_ExecuteScript(sRegulusPath .. "Equinox/ZRouting.lua")
LM_ExecuteScript(sRegulusPath .. "LRT Facility/ZRouting.lua")
LM_ExecuteScript(sRegulusPath .. "Serenity Observatory/ZRouting.lua")

-- |[Regulus Biolabs]|
local sRegulusBiolabsPath = sBasePath .. "Regulus Biolabs/"
LM_ExecuteScript(sRegulusBiolabsPath .. "Aquatic Genetics/ZRouting.lua")
LM_ExecuteScript(sRegulusBiolabsPath .. "Biolabs Alpha/ZRouting.lua")
LM_ExecuteScript(sRegulusBiolabsPath .. "Biolabs Beta/ZRouting.lua")
LM_ExecuteScript(sRegulusBiolabsPath .. "Biolabs Delta/ZRouting.lua")
LM_ExecuteScript(sRegulusBiolabsPath .. "Biolabs Epsilon/ZRouting.lua")
LM_ExecuteScript(sRegulusBiolabsPath .. "Biolabs Gamma/ZRouting.lua")
LM_ExecuteScript(sRegulusBiolabsPath .. "Datacore/ZRouting.lua")
LM_ExecuteScript(sRegulusBiolabsPath .. "Hydroponics/ZRouting.lua")
LM_ExecuteScript(sRegulusBiolabsPath .. "Raiju Ranch/ZRouting.lua")

-- |[Regulus City]|
local sRegulusCityPath = sBasePath .. "Regulus City/"
LM_ExecuteScript(sRegulusCityPath .. "Arcane University/ZRouting.lua")
LM_ExecuteScript(sRegulusCityPath .. "Sector 15/ZRouting.lua")
LM_ExecuteScript(sRegulusCityPath .. "Sector 96/ZRouting.lua")

-- |[Trannadar]|
local sTrannadarPath = sBasePath .. "Trannadar/"
LM_ExecuteScript(sTrannadarPath .. "Arbonne Plains/ZRouting.lua")
LM_ExecuteScript(sTrannadarPath .. "Dimensional Trap/ZRouting.lua")
LM_ExecuteScript(sTrannadarPath .. "Evermoon Forest/ZRouting.lua")
LM_ExecuteScript(sTrannadarPath .. "Starfield Swamp/ZRouting.lua")
LM_ExecuteScript(sTrannadarPath .. "Trannadar Trading Post/ZRouting.lua")

-- |[Trafal]|
local sTrafalPath = sBasePath .. "Trafal/"
LM_ExecuteScript(sTrafalPath .. "Trafal Glacier/ZRouting.lua")
LM_ExecuteScript(sTrafalPath .. "Northwoods/ZRouting.lua")
LM_ExecuteScript(sTrafalPath .. "Westwoods/ZRouting.lua")
LM_ExecuteScript(sTrafalPath .. "Mt Sarulente/ZRouting.lua")
