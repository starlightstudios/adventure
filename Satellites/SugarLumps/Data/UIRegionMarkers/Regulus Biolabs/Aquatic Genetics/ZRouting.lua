-- |[ ==================================== Aquatic Genetics ==================================== ]|
--Name Pattern: aquatic_genetics_research_XX
--Starts at 0, goes to 21

--Create.
SLF_Open("Output/UIRegionAquaticGenetics.slf")
ImageLump_SetCompression(1)

--Execute.
local sBasePath = fnResolvePath()
local sPrefix = "AquaticGenetics|"
for i = 0, 21, 1 do
    local sNumber = string.format("%02i", i)
	ImageLump_Rip(sPrefix .. sNumber, sBasePath .. "aquatic_genetics_research_" .. sNumber .. ".png", 0, 0, -1, -1, gciNoTransparencies)
end

--Clean.
SLF_Close()
