-- |[ ======================================= Raiju Ranch ====================================== ]|
--Name Pattern: raiju_ranch_XX
--Starts at 0, goes to 21

--Create.
SLF_Open("Output/UIRegionBiolabsRaijuRanch.slf")
ImageLump_SetCompression(1)

--Execute.
local sBasePath = fnResolvePath()
local sPrefix = "RaijuRanch|"
for i = 0, 21, 1 do
    local sNumber = string.format("%02i", i)
	ImageLump_Rip(sPrefix .. sNumber, sBasePath .. "raiju_ranch_" .. sNumber .. ".png", 0, 0, -1, -1, gciNoTransparencies)
end

--Clean.
SLF_Close()

