-- |[ ================================= Trannadar Trading Post ================================= ]|
--Name Pattern: trannadar_trading_post_XX
--Starts at 1, goes to 22

--Create.
SLF_Open("Output/UIRegionTrannadarTradingPost.slf")
ImageLump_SetCompression(1)

--Execute.
local sBasePath = fnResolvePath()
local sPrefix = "TrannadarTradingPost|"
for i = 1, 22, 1 do
    local sNumber = string.format("%02i", i)
    local sNumberMinus = string.format("%02i", i-1)
	ImageLump_Rip(sPrefix .. sNumberMinus, sBasePath .. "trannadar_trading_post_" .. sNumber .. ".png", 0, 0, -1, -1, 0)
end

--Clean.
SLF_Close()
