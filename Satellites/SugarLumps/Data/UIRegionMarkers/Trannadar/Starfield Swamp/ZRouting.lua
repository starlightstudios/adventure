-- |[ ===================================== Starfield Swamp ==================================== ]|
--Name Pattern: starfield_swamp_XX
--Starts at 0, goes to 21

--Create.
SLF_Open("Output/UIRegionStarfieldSwamp.slf")
ImageLump_SetCompression(1)

--Execute.
local sBasePath = fnResolvePath()
local sPrefix = "StarfieldSwamp|"
for i = 0, 21, 1 do
    local sNumber = string.format("%02i", i)
	ImageLump_Rip(sPrefix .. sNumber, sBasePath .. "starfield_swamp_" .. sNumber .. ".png", 0, 0, -1, -1, 0)
end

--Clean.
SLF_Close()
