-- |[ ======================================== Sector 15 ======================================= ]|
--Name Pattern: regulus_city_sector15_XX
--Starts at 1, goes to 22

--Create.
SLF_Open("Output/UIRegionRegulusCitySector15.slf")
ImageLump_SetCompression(1)

--Execute.
local sBasePath = fnResolvePath()
local sPrefix = "RegulusCitySector15|"
for i = 1, 22, 1 do
    local sNumber = string.format("%02i", i)
    local sNumberMinus = string.format("%02i", i-1)
	ImageLump_Rip(sPrefix .. sNumberMinus, sBasePath .. "regulus_city_sector15_" .. sNumber .. ".png", 0, 0, -1, -1, gciNoTransparencies)
end

--Clean.
SLF_Close()
