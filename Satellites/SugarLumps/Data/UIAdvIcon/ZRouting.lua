-- |[ ==================================== Adventure Icons ===================================== ]|
--Icons used in various spots in Adventure Mode. These indiciate things like damage types, debuffs,
-- characters, weapons, inventory, and more!
local sBasePath = fnResolvePath()

-- |[Setup]|
--File creation.
SLF_Open("Output/UIAdvIcons.slf")
ImageLump_SetCompression(1)

--Autoloader setup.
local sAutoloaderName = "AutoLoad|Adventure|AdvIconsFilter"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[ ====================================== Damage Types ====================================== ]|
--Icons that indicate what type of damage an ability does, or resistances.
local cfWid = 19
local cfHei = 19
local sPath = sBasePath .. "DamageTypeIcons.png"
local sPrefix = "DmgTypeIco|"
local sDLPath = "Root/Images/AdventureUI/DamageTypeIcons/"

-- |[Small Icons]|
--Damage types.
fnRipImageAuto(sPrefix .. "Slashing",   sPath, (cfWid * 0.0), (cfHei * 0.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Slashing")
fnRipImageAuto(sPrefix .. "Striking",   sPath, (cfWid * 1.0), (cfHei * 0.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Striking")
fnRipImageAuto(sPrefix .. "Piercing",   sPath, (cfWid * 2.0), (cfHei * 0.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Piercing")
fnRipImageAuto(sPrefix .. "Flaming",    sPath, (cfWid * 3.0), (cfHei * 0.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Flaming")
fnRipImageAuto(sPrefix .. "Freezing",   sPath, (cfWid * 0.0), (cfHei * 1.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Freezing")
fnRipImageAuto(sPrefix .. "Shocking",   sPath, (cfWid * 1.0), (cfHei * 1.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Shocking")
fnRipImageAuto(sPrefix .. "Crusading",  sPath, (cfWid * 2.0), (cfHei * 1.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Crusading")
fnRipImageAuto(sPrefix .. "Obscuring",  sPath, (cfWid * 3.0), (cfHei * 1.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Obscuring")
fnRipImageAuto(sPrefix .. "Bleeding",   sPath, (cfWid * 0.0), (cfHei * 2.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Bleeding")
fnRipImageAuto(sPrefix .. "Poisoning",  sPath, (cfWid * 1.0), (cfHei * 2.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Poisoning")
fnRipImageAuto(sPrefix .. "Corroding",  sPath, (cfWid * 2.0), (cfHei * 2.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Corroding")
fnRipImageAuto(sPrefix .. "Terrifying", sPath, (cfWid * 3.0), (cfHei * 2.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Terrifying")
fnRipImageAuto(sPrefix .. "Protection", sPath, (cfWid * 0.0), (cfHei * 3.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Protection")

--Special:
fnRipImageAuto(sPrefix .. "Weapon", sPath, 19, 57, 56, 19, 0, sAutoloaderName, sDLPath .. "Weapon")

--Stats.
fnRipImageAuto(sPrefix .. "Health",     sPath, (cfWid * 0.0), (cfHei * 4.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Health")
fnRipImageAuto(sPrefix .. "Attack",     sPath, (cfWid * 1.0), (cfHei * 4.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Attack")
fnRipImageAuto(sPrefix .. "Initiative", sPath, (cfWid * 2.0), (cfHei * 4.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Initiative")
fnRipImageAuto(sPrefix .. "Accuracy",   sPath, (cfWid * 3.0), (cfHei * 4.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Accuracy")
fnRipImageAuto(sPrefix .. "Evade",      sPath, (cfWid * 0.0), (cfHei * 5.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Evade")
fnRipImageAuto(sPrefix .. "XP",         sPath, (cfWid * 1.0), (cfHei * 5.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "XP")
fnRipImageAuto(sPrefix .. "Platina",    sPath, (cfWid * 2.0), (cfHei * 5.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Platina")
fnRipImageAuto(sPrefix .. "JP",         sPath, (cfWid * 3.0), (cfHei * 5.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "JP")
fnRipImageAuto(sPrefix .. "LV",         sPath, (cfWid * 0.0), (cfHei * 6.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "LV")
fnRipImageAuto(sPrefix .. "Mana",       sPath, (cfWid * 1.0), (cfHei * 6.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Mana")
fnRipImageAuto(sPrefix .. "Shield",     sPath, (cfWid * 2.0), (cfHei * 6.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Shield")
fnRipImageAuto(sPrefix .. "Adrenaline", sPath, (cfWid * 3.0), (cfHei * 6.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Adrenaline")
fnRipImageAuto(sPrefix .. "Skill",      sPath, (cfWid * 0.0), (cfHei * 7.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Skill")

--Mana-per-turn, wider.
fnRipImageAuto(sPrefix .. "ManaPerTurn", sPath, (cfWid * 1.0), (cfHei * 7.0), cfWid*2, cfHei, 0, sAutoloaderName, sDLPath .. "ManaPerTurn")

--Miscellaneous.
fnRipImageAuto(sPrefix .. "Blind",  sPath, (cfWid * 4.0), (cfHei * 0.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Blind")
fnRipImageAuto(sPrefix .. "Stun",   sPath, (cfWid * 5.0), (cfHei * 0.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Stun")
fnRipImageAuto(sPrefix .. "Clock",  sPath, (cfWid * 6.0), (cfHei * 0.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Clock")
fnRipImageAuto(sPrefix .. "Threat", sPath, (cfWid * 7.0), (cfHei * 0.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Threat")
fnRipImageAuto(sPrefix .. "Buff",   sPath, (cfWid * 4.0), (cfHei * 1.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Buff")
fnRipImageAuto(sPrefix .. "Debuff", sPath, (cfWid * 5.0), (cfHei * 1.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Debuff")
fnRipImageAuto(sPrefix .. "CmbPnt", sPath, (cfWid * 6.0), (cfHei * 1.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "CmbPnt")

-- |[Large Icons]|
--Used exclusively for ability descriptions.
cfWid = 24
cfHei = 24
sPath = sBasePath .. "DamageTypeIconsLg.png"
sPrefix = "IconLG|"
sDLPath = "Root/Images/AdventureUI/DamageTypeIconsLg/"

--Damage Types.
fnRipImageAuto(sPrefix .. "Slashing",   sPath, (cfWid * 0.0), (cfHei * 0.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "Slashing")
fnRipImageAuto(sPrefix .. "Striking",   sPath, (cfWid * 1.0), (cfHei * 0.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "Striking")
fnRipImageAuto(sPrefix .. "Piercing",   sPath, (cfWid * 2.0), (cfHei * 0.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "Piercing")
fnRipImageAuto(sPrefix .. "Flaming",    sPath, (cfWid * 3.0), (cfHei * 0.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "Flaming")
fnRipImageAuto(sPrefix .. "Freezing",   sPath, (cfWid * 0.0), (cfHei * 1.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "Freezing")
fnRipImageAuto(sPrefix .. "Shocking",   sPath, (cfWid * 1.0), (cfHei * 1.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "Shocking")
fnRipImageAuto(sPrefix .. "Crusading",  sPath, (cfWid * 2.0), (cfHei * 1.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "Crusading")
fnRipImageAuto(sPrefix .. "Obscuring",  sPath, (cfWid * 3.0), (cfHei * 1.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "Obscuring")
fnRipImageAuto(sPrefix .. "Bleeding",   sPath, (cfWid * 0.0), (cfHei * 2.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "Bleeding")
fnRipImageAuto(sPrefix .. "Poisoning",  sPath, (cfWid * 1.0), (cfHei * 2.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "Poisoning")
fnRipImageAuto(sPrefix .. "Corroding",  sPath, (cfWid * 2.0), (cfHei * 2.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "Corroding")
fnRipImageAuto(sPrefix .. "Terrifying", sPath, (cfWid * 3.0), (cfHei * 2.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "Terrifying")
fnRipImageAuto(sPrefix .. "Protection", sPath, (cfWid * 0.0), (cfHei * 3.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "Protection")

--Special:
fnRipImageAuto(sPrefix .. "Weapon", sPath, 19, 57, 56, 19, gciNoTransparencies, sAutoloaderName, sDLPath .. "Weapon")

--Stats.
fnRipImageAuto(sPrefix .. "Health",     sPath, (cfWid * 0.0), (cfHei * 4.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "Health")
fnRipImageAuto(sPrefix .. "Attack",     sPath, (cfWid * 1.0), (cfHei * 4.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "Attack")
fnRipImageAuto(sPrefix .. "Initiative", sPath, (cfWid * 2.0), (cfHei * 4.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "Initiative")
fnRipImageAuto(sPrefix .. "Accuracy",   sPath, (cfWid * 3.0), (cfHei * 4.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "Accuracy")
fnRipImageAuto(sPrefix .. "Evade",      sPath, (cfWid * 0.0), (cfHei * 5.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "Evade")
fnRipImageAuto(sPrefix .. "XP",         sPath, (cfWid * 1.0), (cfHei * 5.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "XP")
fnRipImageAuto(sPrefix .. "Platina",    sPath, (cfWid * 2.0), (cfHei * 5.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "Platina")
fnRipImageAuto(sPrefix .. "JP",         sPath, (cfWid * 3.0), (cfHei * 5.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "JP")
fnRipImageAuto(sPrefix .. "LV",         sPath, (cfWid * 0.0), (cfHei * 6.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "LV")
fnRipImageAuto(sPrefix .. "Mana",       sPath, (cfWid * 1.0), (cfHei * 6.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "Mana")
fnRipImageAuto(sPrefix .. "Shield",     sPath, (cfWid * 2.0), (cfHei * 6.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "Shield")
fnRipImageAuto(sPrefix .. "Adrenaline", sPath, (cfWid * 3.0), (cfHei * 6.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "Adrenaline")
fnRipImageAuto(sPrefix .. "Skill",      sPath, (cfWid * 0.0), (cfHei * 7.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "Skill")

--Mana-per-turn, wider.
fnRipImageAuto(sPrefix .. "ManaPerTurn", sPath, (cfWid * 1.0), (cfHei * 7.0), cfWid*2, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "ManaPerTurn")

--Miscellaneous.
fnRipImageAuto(sPrefix .. "Blind",  sPath, (cfWid * 4.0), (cfHei * 0.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "Blind")
fnRipImageAuto(sPrefix .. "Stun",   sPath, (cfWid * 5.0), (cfHei * 0.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "Stun")
fnRipImageAuto(sPrefix .. "Clock",  sPath, (cfWid * 6.0), (cfHei * 0.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "Clock")
fnRipImageAuto(sPrefix .. "Threat", sPath, (cfWid * 7.0), (cfHei * 0.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "Threat")
fnRipImageAuto(sPrefix .. "Buff",   sPath, (cfWid * 4.0), (cfHei * 1.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "Buff")
fnRipImageAuto(sPrefix .. "Debuff", sPath, (cfWid * 5.0), (cfHei * 1.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "Debuff")
fnRipImageAuto(sPrefix .. "CmbPnt", sPath, (cfWid * 6.0), (cfHei * 1.0), cfWid, cfHei, gciNoTransparencies, sAutoloaderName, sDLPath .. "CmbPnt")

--Effect Indicators
fnRipImageAuto(sPrefix .. "EffectHostile",  sPath, (cfWid * 4.0), (cfHei * 2.0), 64, 24, gciNoTransparencies, sAutoloaderName, sDLPath .. "EffectHostile")
fnRipImageAuto(sPrefix .. "EffectFriendly", sPath, (cfWid * 4.0), (cfHei * 3.0), 64, 24, gciNoTransparencies, sAutoloaderName, sDLPath .. "EffectFriendly")

--Strengths
local iStrW = 48
local iStrL = (cfWid * 4.0)
local iStrR = (cfWid * 4.0) + iStrW
fnRipImageAuto(sPrefix .. "Str0",  sPath, iStrL, (cfHei * 4.0), iStrW, 24, gciNoTransparencies, sAutoloaderName, sDLPath .. "Str0")
fnRipImageAuto(sPrefix .. "Str1",  sPath, iStrR, (cfHei * 4.0), iStrW, 24, gciNoTransparencies, sAutoloaderName, sDLPath .. "Str1")
fnRipImageAuto(sPrefix .. "Str2",  sPath, iStrL, (cfHei * 5.0), iStrW, 24, gciNoTransparencies, sAutoloaderName, sDLPath .. "Str2")
fnRipImageAuto(sPrefix .. "Str3",  sPath, iStrR, (cfHei * 5.0), iStrW, 24, gciNoTransparencies, sAutoloaderName, sDLPath .. "Str3")
fnRipImageAuto(sPrefix .. "Str4",  sPath, iStrL, (cfHei * 6.0), iStrW, 24, gciNoTransparencies, sAutoloaderName, sDLPath .. "Str4")
fnRipImageAuto(sPrefix .. "Str5",  sPath, iStrR, (cfHei * 6.0), iStrW, 24, gciNoTransparencies, sAutoloaderName, sDLPath .. "Str5")
fnRipImageAuto(sPrefix .. "Str6",  sPath, iStrL, (cfHei * 7.0), iStrW, 24, gciNoTransparencies, sAutoloaderName, sDLPath .. "Str6")
fnRipImageAuto(sPrefix .. "Str7",  sPath, iStrR, (cfHei * 7.0), iStrW, 24, gciNoTransparencies, sAutoloaderName, sDLPath .. "Str7")
fnRipImageAuto(sPrefix .. "Str8",  sPath, iStrL, (cfHei * 8.0), iStrW, 24, gciNoTransparencies, sAutoloaderName, sDLPath .. "Str8")
fnRipImageAuto(sPrefix .. "Str9",  sPath, iStrR, (cfHei * 8.0), iStrW, 24, gciNoTransparencies, sAutoloaderName, sDLPath .. "Str9")

-- |[ ======================================== Debuffs ========================================= ]|
--Icons for debuffs. These are not robust categories, they are whatever the debuff wants to use.
cfWid = 19
cfHei = 19
sPath = sBasePath .. "StatusEffectIcons.png"
sPrefix = "StatEffectIco|"
sDLPath = "Root/Images/AdventureUI/DebuffIcons/"

--Autoloader setup.
sAutoloaderName = "AutoLoad|Adventure|AdvIcons"
SLF_RegisterAutoLoadLump(sAutoloaderName)

--Debuff icons.
fnRipImageAuto(sPrefix .. "Blind",    sPath, (cfWid * 0.0), (cfHei * 0.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Blind")
fnRipImageAuto(sPrefix .. "Buff",     sPath, (cfWid * 1.0), (cfHei * 0.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Buff")
fnRipImageAuto(sPrefix .. "Debuff",   sPath, (cfWid * 2.0), (cfHei * 0.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Debuff")
fnRipImageAuto(sPrefix .. "Threat",   sPath, (cfWid * 3.0), (cfHei * 0.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Threat")
fnRipImageAuto(sPrefix .. "Clock",    sPath, (cfWid * 0.0), (cfHei * 1.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Clock")
fnRipImageAuto(sPrefix .. "Accuracy", sPath, (cfWid * 0.0), (cfHei * 2.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Accuracy")
fnRipImageAuto(sPrefix .. "Stun",     sPath, (cfWid * 1.0), (cfHei * 2.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Stun")

--Effect Indicators
cfWid = 48
cfHei = 19
fnRipImageAuto(sPrefix .. "EffectHostile",  sPath, 0, 57, cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "EffectHostile")
fnRipImageAuto(sPrefix .. "EffectFriendly", sPath, 0, 76, cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "EffectFriendly")

--Strength Indicators
cfWid = 37
cfHei = 19
fnRipImageAuto(sPrefix .. "Str0",  sPath,     0, 95 + (cfHei * 0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Str0")
fnRipImageAuto(sPrefix .. "Str1",  sPath, cfWid, 95 + (cfHei * 0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Str1")
fnRipImageAuto(sPrefix .. "Str2",  sPath,     0, 95 + (cfHei * 1), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Str2")
fnRipImageAuto(sPrefix .. "Str3",  sPath, cfWid, 95 + (cfHei * 1), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Str3")
fnRipImageAuto(sPrefix .. "Str4",  sPath,     0, 95 + (cfHei * 2), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Str4")
fnRipImageAuto(sPrefix .. "Str5",  sPath, cfWid, 95 + (cfHei * 2), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Str5")
fnRipImageAuto(sPrefix .. "Str6",  sPath,     0, 95 + (cfHei * 3), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Str6")
fnRipImageAuto(sPrefix .. "Str7",  sPath, cfWid, 95 + (cfHei * 3), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Str7")
fnRipImageAuto(sPrefix .. "Str8",  sPath,     0, 95 + (cfHei * 4), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Str8")
fnRipImageAuto(sPrefix .. "Str9",  sPath, cfWid, 95 + (cfHei * 4), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Str9")

--Special: The EffectHostile and EffectFriendly also get loaded into the damage type DL Path.
AutoLoader_Register(sAutoloaderName, sPrefix .. "EffectHostile",  "Root/Images/AdventureUI/DamageTypeIcons/EffectHostile")
AutoLoader_Register(sAutoloaderName, sPrefix .. "EffectFriendly", "Root/Images/AdventureUI/DamageTypeIcons/EffectFriendly")

-- |[ ==================================== Comparison Icons ==================================== ]|
--Used for displaying stats and comparing them in the inventory/equipment displays.
local s18PxPath = sBasePath .. "CompareIco.png"
local saArray = {}
saArray[1] = {"Attack", "Protection", "Health", "Accuracy", "Evade", "Initiative", "Mana"}
saArray[2] = {"Blind", "Bleed", "Poison", "Corrode", "Shield", "Adrenaline", "ComboPoint"}
saArray[3] = {"Stun", "Turns", "Threat", "Skill"}
saArray[4] = {"Slash", "Pierce", "Strike"}
saArray[5] = {"Flaming", "Freezing", "Shocking"}
saArray[6] = {"Crusading", "Obscuring", "Terrify", "Debuff"}
saArray[7] = {"Underlay"}
saArray[8] = {"Downx3", "Downx2", "Downx1", "Neutral", "Upx1", "Upx2", "Upx3"}
saArray[9] = {"Rarrow"}

--Iteration.
sPrefix = "CompareIco|"
cfWid = 18
cfHei = 18
sDLPath = "Root/Images/AdventureUI/StatisticIcons/"
for i = 1, #saArray, 1 do
    for p = 1, #saArray[i], 1 do
        fnRipImageAuto(sPrefix .. saArray[i][p], s18PxPath, (p-1) * cfWid, (i - 1) * cfHei, cfWid, cfHei, 0, sAutoloaderName, sDLPath .. saArray[i][p])
    end
end

--Special: Used only for the combat inspector. Not as wide.
fnRipImageAuto("CompareIcoNr|Up", s18PxPath, 72, 108, 10, 18, 0, sAutoloaderName, "Root/Images/AdventureUI/StatisticIcons/NrUp")
fnRipImageAuto("CompareIcoNr|Dn", s18PxPath, 90, 108, 10, 18, 0, sAutoloaderName, "Root/Images/AdventureUI/StatisticIcons/NrDn")

-- |[ ================================= Ability Icons - General ================================ ]|
--Ability icons and parts not specific to any individual character.
cfWid = 50
cfHei = 50
sPath = sBasePath .. "AbilityIcons_System.png"
sPrefix = "AbilityIco|"
sDLPath = "Root/Images/AdventureUI/Abilities/"

--Items
fnRipImageAuto(sPrefix .. "Item|SilverRunestone",  sPath, (cfWid * 0.0), (cfHei * 5.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Item|SilverRunestone")
fnRipImageAuto(sPrefix .. "Item|SilverRunestone2", sPath, (cfWid * 1.0), (cfHei * 5.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Item|SilverRunestone2")
fnRipImageAuto(sPrefix .. "Item|CrimsonRunestone", sPath, (cfWid * 0.0), (cfHei * 6.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Item|CrimsonRunestone")
fnRipImageAuto(sPrefix .. "Item|CrimsonRunestone2",sPath, (cfWid * 1.0), (cfHei * 6.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Item|CrimsonRunestone2")
fnRipImageAuto(sPrefix .. "Item|VioletRunestone",  sPath, (cfWid * 0.0), (cfHei * 9.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Item|VioletRunestone")
fnRipImageAuto(sPrefix .. "Item|VioletRunestone2", sPath, (cfWid * 1.0), (cfHei * 9.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Item|VioletRunestone2")
fnRipImageAuto(sPrefix .. "Item|GenericPotionRed", sPath, (cfWid * 0.0), (cfHei *11.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Item|GenericPotionRed")
fnRipImageAuto(sPrefix .. "Item|GenericPotionGrn", sPath, (cfWid * 1.0), (cfHei *11.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Item|GenericPotionGrn")
fnRipImageAuto(sPrefix .. "Item|EverlastingHoney", sPath, (cfWid * 2.0), (cfHei *11.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Item|EverlastingHoney")
fnRipImageAuto(sPrefix .. "Item|PepperPie",        sPath, (cfWid * 3.0), (cfHei *11.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Item|PepperPie")
fnRipImageAuto(sPrefix .. "Item|SmellingSalts",    sPath, (cfWid * 4.0), (cfHei *11.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Item|SmellingSalts")
fnRipImageAuto(sPrefix .. "Item|BreadA",           sPath, (cfWid * 5.0), (cfHei *11.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Item|BreadA")
fnRipImageAuto(sPrefix .. "Item|BreadB",           sPath, (cfWid * 6.0), (cfHei *11.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Item|BreadB")
fnRipImageAuto(sPrefix .. "Item|Cookies",          sPath, (cfWid * 7.0), (cfHei *11.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Item|Cookies")
fnRipImageAuto(sPrefix .. "Item|PoisonMushroom",   sPath, (cfWid * 0.0), (cfHei *12.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Item|PoisonMushroom")
fnRipImageAuto(sPrefix .. "Item|Firebomb",         sPath, (cfWid * 1.0), (cfHei *12.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Item|Firebomb")
fnRipImageAuto(sPrefix .. "Item|Medkit",           sPath, (cfWid * 2.0), (cfHei *12.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Item|Medkit")
fnRipImageAuto(sPrefix .. "Item|Injection",        sPath, (cfWid * 3.0), (cfHei *12.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Item|Injection")
fnRipImageAuto(sPrefix .. "Item|Gun",              sPath, (cfWid * 4.0), (cfHei *12.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Item|Gun")
fnRipImageAuto(sPrefix .. "Item|Flashbang",        sPath, (cfWid * 5.0), (cfHei *12.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Item|Flashbang")
fnRipImageAuto(sPrefix .. "Item|Paint",            sPath, (cfWid * 6.0), (cfHei *12.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Item|Paint")
fnRipImageAuto(sPrefix .. "Item|Bomb",             sPath, (cfWid * 7.0), (cfHei *12.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Item|Bomb")
fnRipImageAuto(sPrefix .. "Item|Flamer",           sPath, (cfWid * 0.0), (cfHei *13.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Item|Flamer")
fnRipImageAuto(sPrefix .. "Item|Positive",         sPath, (cfWid * 1.0), (cfHei *13.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Item|Positive")
fnRipImageAuto(sPrefix .. "Item|Negative",         sPath, (cfWid * 2.0), (cfHei *13.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Item|Negative")

--Combo/Cooldown Indicators
fnRipImageAuto(sPrefix .. "CmbClock", sPath, (cfWid * 0.0), (cfHei * 0.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "CmbClock")
for i = 0, 9, 1 do
    fnRipImageAuto(sPrefix .. "Cmb"..i, sPath, (cfWid * (1+i)), (cfHei * 0.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Cmb" .. i)
end

--Turn Action Backing/Frames
fnRipImageAuto(sPrefix .. "OctBackGrey",   sPath, (cfWid * 0.0), (cfHei * 1.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "OctBackGrey")
fnRipImageAuto(sPrefix .. "OctBackRed",    sPath, (cfWid * 1.0), (cfHei * 1.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "OctBackRed")
fnRipImageAuto(sPrefix .. "OctBackGreen",  sPath, (cfWid * 2.0), (cfHei * 1.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "OctBackGreen")
fnRipImageAuto(sPrefix .. "OctBackBlue",   sPath, (cfWid * 3.0), (cfHei * 1.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "OctBackBlue")
fnRipImageAuto(sPrefix .. "OctBackPurple", sPath, (cfWid * 4.0), (cfHei * 1.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "OctBackPurple")
fnRipImageAuto(sPrefix .. "OctFrameRed",   sPath, (cfWid * 5.0), (cfHei * 1.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "OctFrameRed")
fnRipImageAuto(sPrefix .. "OctFrameBlue",  sPath, (cfWid * 6.0), (cfHei * 1.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "OctFrameBlue")
fnRipImageAuto(sPrefix .. "OctFrameTeal",  sPath, (cfWid * 7.0), (cfHei * 1.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "OctFrameTeal")
fnRipImageAuto(sPrefix .. "OctFramePurp",  sPath, (cfWid * 8.0), (cfHei * 1.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "OctFramePurp")

--Free Action Backing/Frames
fnRipImageAuto(sPrefix .. "SqrBackGrey",   sPath, (cfWid * 0.0), (cfHei * 2.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "SqrBackGrey")
fnRipImageAuto(sPrefix .. "SqrBackRed",    sPath, (cfWid * 1.0), (cfHei * 2.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "SqrBackRed")
fnRipImageAuto(sPrefix .. "SqrBackGreen",  sPath, (cfWid * 2.0), (cfHei * 2.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "SqrBackGreen")
fnRipImageAuto(sPrefix .. "SqrBackBlue",   sPath, (cfWid * 3.0), (cfHei * 2.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "SqrBackBlue")
fnRipImageAuto(sPrefix .. "SqrBackPurple", sPath, (cfWid * 4.0), (cfHei * 2.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "SqrBackPurple")
fnRipImageAuto(sPrefix .. "SqrFrameRed",   sPath, (cfWid * 5.0), (cfHei * 2.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "SqrFrameRed")
fnRipImageAuto(sPrefix .. "SqrFrameBlue",  sPath, (cfWid * 6.0), (cfHei * 2.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "SqrFrameBlue")
fnRipImageAuto(sPrefix .. "SqrFrameTeal",  sPath, (cfWid * 7.0), (cfHei * 2.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "SqrFrameTeal")
fnRipImageAuto(sPrefix .. "SqrFramePurp",  sPath, (cfWid * 8.0), (cfHei * 2.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "SqrFramePurp")

--Common Abilities
fnRipImageAuto(sPrefix .. "Attack",    sPath, (cfWid * 0.0), (cfHei * 3.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Attack")
fnRipImageAuto(sPrefix .. "Defend",    sPath, (cfWid * 1.0), (cfHei * 3.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Defend")
fnRipImageAuto(sPrefix .. "Special",   sPath, (cfWid * 2.0), (cfHei * 3.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Special")
fnRipImageAuto(sPrefix .. "Retreat",   sPath, (cfWid * 3.0), (cfHei * 3.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Retreat")
fnRipImageAuto(sPrefix .. "Surrender", sPath, (cfWid * 4.0), (cfHei * 3.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Surrender")
fnRipImageAuto(sPrefix .. "Volunteer", sPath, (cfWid * 5.0), (cfHei * 3.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Volunteer")
fnRipImageAuto(sPrefix .. "Lock",      sPath, (cfWid * 6.0), (cfHei * 3.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Lock")
fnRipImageAuto(sPrefix .. "WeaponLft", sPath, (cfWid * 7.0), (cfHei * 3.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "WeaponLft")
fnRipImageAuto(sPrefix .. "WeaponRgt", sPath, (cfWid * 8.0), (cfHei * 3.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "WeaponRgt")
fnRipImageAuto(sPrefix .. "Threat",    sPath, (cfWid * 9.0), (cfHei * 3.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Threat")
fnRipImageAuto(sPrefix .. "Unknown",   sPath, (cfWid *10.0), (cfHei * 3.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Unknown")
fnRipImageAuto(sPrefix .. "Catalyst",  sPath, (cfWid *11.0), (cfHei * 3.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "Catalyst")

--Common Effect Indicators
fnRipImageAuto(sPrefix .. "GenBleed",   sPath, (cfWid * 0.0), (cfHei * 4.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "GenBleed")
fnRipImageAuto(sPrefix .. "GenPoison",  sPath, (cfWid * 1.0), (cfHei * 4.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "GenPoison")
fnRipImageAuto(sPrefix .. "GenCorrode", sPath, (cfWid * 2.0), (cfHei * 4.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "GenCorrode")
fnRipImageAuto(sPrefix .. "GenBuff",    sPath, (cfWid * 3.0), (cfHei * 4.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "GenBuff")
fnRipImageAuto(sPrefix .. "GenDebuff",  sPath, (cfWid * 4.0), (cfHei * 4.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "GenDebuff")
fnRipImageAuto(sPrefix .. "GenSlash",   sPath, (cfWid * 5.0), (cfHei * 4.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "GenSlash")
fnRipImageAuto(sPrefix .. "GenStrike",  sPath, (cfWid * 6.0), (cfHei * 4.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "GenStrike")
fnRipImageAuto(sPrefix .. "GenPierce",  sPath, (cfWid * 7.0), (cfHei * 4.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "GenPierce")
fnRipImageAuto(sPrefix .. "GenFlame",   sPath, (cfWid * 8.0), (cfHei * 4.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "GenFlame")
fnRipImageAuto(sPrefix .. "GenFreeze",  sPath, (cfWid * 9.0), (cfHei * 4.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "GenFreeze")
fnRipImageAuto(sPrefix .. "GenShock",   sPath, (cfWid *10.0), (cfHei * 4.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "GenShock")
fnRipImageAuto(sPrefix .. "GenLight",   sPath, (cfWid *11.0), (cfHei * 4.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "GenLight")
fnRipImageAuto(sPrefix .. "GenDark",    sPath, (cfWid *12.0), (cfHei * 4.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "GenDark")
fnRipImageAuto(sPrefix .. "GenTerrify", sPath, (cfWid *13.0), (cfHei * 4.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "GenTerrify")
fnRipImageAuto(sPrefix .. "GenBlind",   sPath, (cfWid *14.0), (cfHei * 4.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "GenBlind")
fnRipImageAuto(sPrefix .. "GenBlindRes",sPath, (cfWid *15.0), (cfHei * 4.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "GenBlindRes")

--Character-Specific Attacks
fnRipImageAuto(sPrefix .. "AttackSpear",           sPath, (cfWid * 9.0), (cfHei * 5.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "AttackSpear")
fnRipImageAuto(sPrefix .. "AttackPulseDiffractor", sPath, (cfWid *10.0), (cfHei * 5.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "AttackPulseDiffractor")
fnRipImageAuto(sPrefix .. "AttackFissionCarbine",  sPath, (cfWid *11.0), (cfHei * 5.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "AttackFissionCarbine")
fnRipImageAuto(sPrefix .. "AttackRifle",           sPath, (cfWid *12.0), (cfHei * 5.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "AttackRifle")
fnRipImageAuto(sPrefix .. "AttackDragon",          sPath, (cfWid *13.0), (cfHei * 5.0), cfWid, cfHei, 0, sAutoloaderName, sDLPath .. "AttackDragon")

-- |[ ======================================= fnRipList() ====================================== ]|
--Rips a set of ability icons from top to bottom, left to right, provided a list of names.
local function fnRipList(pzaList, psPath, psPrefix, psAutoloaderName, psDLPath)
    
    -- |[Argument Check]|
    if(pzaList          == nil) then return end
    if(psPath           == nil) then return end
    if(psPrefix         == nil) then return end
    if(psAutoloaderName == nil) then return end
    if(psDLPath         == nil) then return end
    
    -- |[Constants]|
    cfWid = 50
    cfHei = 50
    
    -- |[Iterate]|
    --Iterate across the sublists.
    for i = 1, #pzaList, 1 do
    
        --Fast-access variables.
        local sJobName    = pzaList[i][1]
        local saEntryList = pzaList[i][2]
    
        --Y position.
        local cfUseY = (i-1) * cfHei
        
        --Iterate.
        for p = 1, #saEntryList, 1 do
            fnRipImageAuto(psPrefix .. sJobName .. saEntryList[p], psPath, (cfWid * (p-1)), cfUseY, cfWid, cfHei, 0, psAutoloaderName, psDLPath .. sJobName .. saEntryList[p])
        end
    end
end

-- |[ ================================== Ability Icons - Mei =================================== ]|
--Mei's ability icons.
sPath = sBasePath .. "AbilityIcons_Mei.png"
sPrefix = "AbilityIco|Mei|"
sDLPath = "Root/Images/AdventureUI/Abilities/Mei|"

--Listing
local zaMeiList = {}
zaMeiList[ 1] = {"Fencer|", {"WayOfTheBlade",    "PowerfulStrike", "Rend",       "BladeDance",       "PommelBash",      "Blind",          "Precognition",    "Taunt",         "QuickStrike", 
                             "Trip",             "Concentrate",    "Whirl",      "FastReflexes"}}
zaMeiList[ 2] = {"", {"WayOfTheDruid",    "SporeCloud",     "Regrowth",   "VineLash",         "PollenRend",      "FanOfLeaves",    "Ripple",          "NaturesBlessing", "AcidBlood",   "Mend"}}
zaMeiList[ 3] = {"", {"WayOfTheClaw",     "Pounce",         "JumpKick",   "Garrote",          "HuntersInstinct", "Jab",            "Lunge",           "Swipe",           "GloryKill"}}
zaMeiList[ 4] = {"", {"WayOfTheHive",     "Sting",          "FlapJump",   "ClawKick",         "Coordinate",      "WaxArmor",       "ScoutsHonor",     "CallForHelp",     "HiveLocus"}}
zaMeiList[ 5] = {"", {"WayOfTheDead",     "DusterBlast",    "TidyUp",     "TranslucentSmile", "FirstAid",        "IcyHand",        "Undying"}}
zaMeiList[ 6] = {"", {"WayOfTheSlime",    "Splash",         "Goopshot",   "FirmUp",           "JigglyChest",     "Absorb",         "BigDumbGrin",     "Reserves",        "Entangle"}}
zaMeiList[ 7] = {"", {"WayOfTheZombee",   "Rage",           "Fury",       "Roar",             "CorruptHoney",    "Crush"}}
zaMeiList[ 8] = {"", {"WayOfTheHoly",     "Bodyslam",       "Flutter",    "StoneSkin",        "HolyFlame",       "HolyInferno",    "PiercingLight",   "SoothingLight",   "Indomitable"}}
zaMeiList[ 9] = {"Soulherd|", {"WayOfTheWisps",    "ClawSlash",      "MendSpirit", "Lightburst",       "Shadowburst",     "ViciousStrike",  "DragBeneath",     "BurningSoul",     "CreepyGiggle"}}
zaMeiList[10] = {"Stalker|",  {"WayOfTheFaceless", "Strangle",       "Relentless", "UnlivingGrasp",    "YouMustLeave",    "AppearLifeless", "UnsettlingMoves", "NoRestraints",  "ResinSkin"}}
zaMeiList[11] = {"", {"SpreadRubber",     "Permagrin",      "RubberThralls"}}

--Rip call.
fnRipList(zaMeiList, sPath, sPrefix, sAutoloaderName, sDLPath)
    
-- |[ =============================== Ability Icons - Florentina =============================== ]|
--Florentina's ability icons.
sPath = sBasePath .. "AbilityIcons_Florentina.png"
sPrefix = "AbilityIco|Florentina|"
sDLPath = "Root/Images/AdventureUI/Abilities/Florentina|"

--Listing
local zaFlorentinaList = {}
zaFlorentinaList[1] = {"",         {"FlorentinasGuile",    "CriticalStab",   "DrippingBlade", "Intimidate",      "DrainVitality",  "Regrowth",       "CruelSlice",    "VineWrap",         "Tinkering",    "Botany"}}
zaFlorentinaList[2] = {"",         {"FlorentinasInstinct", "FromTheShadows", "Haymaker",      "LightStep",       "PickPocket",     "PoisonDarts",    "StickyFingers", "VineWhip",         "SmashAndGrab", "WhipTrip"}}
zaFlorentinaList[3] = {"",         {"FlorentinasWit",      "VeiledThreat",   "CalledShot",    "CounterArgument", "Encourage",      "Smug",           "Insult",        "TerriblePun",      "PickMeUp"}}
zaFlorentinaList[4] = {"Agarist|", {"FlorentinasWill",     "AcidicBurst",    "FeedingFury",   "Dissolve",        "Symbiosis",      "AcidWave",       "Parasitize",    "MightOfMycelium",  "LichenThis"}}
zaFlorentinaList[5] = {"Lurker|",  {"FlorentinasTerror",   "Strangle",       "Relentless",    "UnlivingGrasp",   "RelieveBurdens", "AppearLifeless", "DontBlink",     "Massacre",         "ResinSkin"}}

--Rip call.
fnRipList(zaFlorentinaList, sPath, sPrefix, sAutoloaderName, sDLPath)

-- |[ =============================== Ability Icons - Christine ================================ ]|
--Christines's ability icons.
sPath = sBasePath .. "AbilityIcons_Christine.png"
sPrefix = "AbilityIco|Christine|"
sDLPath = "Root/Images/AdventureUI/Abilities/Christine|"

--Listing
local zaChristineList = {}
zaChristineList[1]  = {"", {"DischargeMale", "DischargeGen", "Spearguard"}}
zaChristineList[2]  = {"", {"InspiringCourage", "Shatter", "Rally", "Taunt", "Bluff", "ExposeWeakness", "Unknown", "Inspire", "Batter", "TakePoint"}}
zaChristineList[3]  = {"", {"InspiringFortitude", "Sweep", "Ram", "Fix", "HyperRepair", "TuneUp", "Cover", "SingularStrike"}}
zaChristineList[4]  = {"", {"InspiringPerception", "TouchOfDarkness", "SpearOfLight", "GraspOfTheVoid", "Eclipse", "Sunder", "SubtleSkin", "Gravity", "Supernova"}}
zaChristineList[5]  = {"", {"InspiringLoyalty", "Overclock", "Encased", "AutoRepair", "RoundhouseKick", "MendChassis", "Bounceback", "Jackhammer", "QuickdrySplash"}}
zaChristineList[6]  = {"", {"InspiringOptimism", "Zap", "ChainZap", "Fry", "Burnout", "Overcharge", "Grounded", "SheerGlee", "Jumpstart"}}
zaChristineList[7]  = {"", {"InspiringFlexibility", "Hotwire", "ShortCircuit", "Embolden", "Storm", "Polarity", "Fireworks", "ThunderChomp", "Sparkshot", "BatteryDrainer"}}
zaChristineList[8]  = {"", {"InspiringEndurance", "Scavenge", "Bash", "SteamBlast", "Bloodboil", "HeatSiphon", "Fearless", "Pincushion", "PercussiveMaintenance", "Improvise", "SeenWorse"}}
zaChristineList[9]  = {"", {"InspiringMaturity", "Glower", "Glory", "ColdTouch", "Pulsate", "BeyondSight", "CloseWounds", "Influence", "AbsorbHope", "Obliterate"}}
zaChristineList[10] = {"", {"RageRatTF", "CorrodingFoam", "Rip", "Tear", "Break", "Explode", "Claw", "Smash"}}
zaChristineList[11] = {"", {"InspiringCommand", "FrontlineLeader", "VirusModule", "SpotRepairs", "Slavedriver", "PerfectStrike", "RotatingJoints", "XRayOcularUnits", "HighPowerShock"}}
zaChristineList[12] = {"", {"InspiringFashion", "ImpromptuOrganization", "MightyPen", "OilRun", "TakeNotes", "ForcedReschedule", "HoldMyCalls", "StressRelease", "Proofreading"}}

--Rip call.
fnRipList(zaChristineList, sPath, sPrefix, sAutoloaderName, sDLPath)

-- |[ ================================ Ability Icons - Empress ================================= ]|
--Empress's ability icons.
sPath = sBasePath .. "AbilityIcons_Empress.png"
sPrefix = "AbilityIco|Empress|"
sDLPath = "Root/Images/AdventureUI/Abilities/Empress|"

--Listing
local zaEmpressList = {}
zaEmpressList[1] = {"Common|",    {"Attack", "Block"}}
zaEmpressList[2] = {"Conquerer|", {"ConquerersWrath", "Frostwave", "Sweep", "TailWhip", "AttackMyTarget", "Throw", "FreezingBreath", "Canecussion", "Incoming", "MoveMove", "KneeOfJustice", "Tear", "FrostCauterize", "Piledriver"}}

--Rip call.
fnRipList(zaEmpressList, sPath, sPrefix, sAutoloaderName, sDLPath)

-- |[ ================================== Ability Icons - Izuna ================================= ]|
--Izuna's ability icons.
sPath = sBasePath .. "AbilityIcons_Izuna.png"
sPrefix = "AbilityIco|Izuna|"
sDLPath = "Root/Images/AdventureUI/Abilities/Izuna|"

--Listing
local zaIzunaList = {}
zaIzunaList[1] = {"Common|", {"Prayer", "ChangePamphlet", "ExtraExtra"}}
zaIzunaList[2] = {"Miko|",   {"Barrier", "DisciplineOfMind", "EnlightenedWave", "KindTouch", "Mend", "Purify", "RendingWard", "Renew", "SealingWard", "SearingTouch", "Sophistry", "SparkOfHope"}}

--Rip call.
fnRipList(zaIzunaList, sPath, sPrefix, sAutoloaderName, sDLPath)

-- |[ ================================== Ability Icons - Sanya ================================= ]|
--Sanya's ability icons.
sPath = sBasePath .. "AbilityIcons_Sanya.png"
sPrefix = "AbilityIco|Sanya|"
sDLPath = "Root/Images/AdventureUI/Abilities/Sanya|"

--Listing
local zaSanyaList = {}
zaSanyaList[1] = {"Common|",  {"Attack", "Prepare", "Reset"}}
zaSanyaList[2] = {"Hunter|",  {"ThrillOfTheHunt", "PenetratorRound", "AimedShot", "CripplingShot", "DisablingShot", "LeadTheShot", "ButtStrike", "AimAtJoints", "CrackShot", "CloseAssault", "StalkPrey", "ProtectiveRage"}}
zaSanyaList[3] = {"Sevavi|",  {"ThrillOfTheMoment", "BoulderPunch", "FullPowerShot", "GetBehindMe", "KissIzuna", "LiftSkirt", "Overpower", "SeductiveDance", "SexySway", "StandFirm", "ThighCrush", "Withstand"}}
zaSanyaList[4] = {"Werebat|", {"ThrillOfTheNight", "Bloodsuck", "Echolocate", "Killshot", "NightTerror", "Puncture", "Rend", "Swoop"}}
zaSanyaList[5] = {"Bunny|",   {"ThrillOfCrime", "CroatianHandshake", "FlashyShot", "FleetOfFoot", "SpicyRum", "SpinKick", "Typewriter"}}
zaSanyaList[6] = {"Harpy|",   {"ThrillOfBattle", "AirStrike", "CQC", "DeathMetalSolo", "Featherblast", "FlankTactics", "Pointbird", "Songstrike", "TranqShot"}}

--Rip call.
fnRipList(zaSanyaList, sPath, sPrefix, sAutoloaderName, sDLPath)

-- |[ ================================ Ability Icons - Tiffany ================================= ]|
--Tiffany's ability icons.
sPath = sBasePath .. "AbilityIcons_Tiffany.png"
sPrefix = "AbilityIco|Tiffany|"
sDLPath = "Root/Images/AdventureUI/Abilities/Tiffany|"

--Listing
local zaTiffanyList = {}
zaTiffanyList[1] = {"", {"TakeCover"}}
zaTiffanyList[2] = {"", {"Assault", "Breakthrough", "ChargedCircuits", "DisruptorBlast", "HighPowerShot", "PinShot", "Pulverize", "RubberSlugShot", "TargetOptics", "WideLensShot"}}
zaTiffanyList[3] = {"", {"Subvert", "LayLow", "CoolantCoagulant", "DisruptHomeostasis", "SabotageWiring", "BagOfTricks", "CommJammer", "Exploit", "PocketHacker", "LockdownVirus"}}
zaTiffanyList[4] = {"", {"Support", "SpotlessProtocol", "FocusFire", "ImplosionGrenade", "LaserPulse", "ShieldModule", "Ambush", "UserManuals", "CoveringFire", "TargetLegs", "NaniteCloud"}}
zaTiffanyList[5] = {"", {"Spearhead", "Wristwire", "PsycheUp", "WideLensBlast", "PowerJump", "RapidAction", "FearsomePresence", "IntoTheFray", "ConductiveSpray", "FullAuto", "ColdCalculation"}}

--Rip call.
fnRipList(zaTiffanyList, sPath, sPrefix, sAutoloaderName, sDLPath)

-- |[ ================================= Ability Icons - SX-399 ================================= ]|
--SX-399's ability icons. JX-101 borrows from this sheet as some abilities are identical.
sPath = sBasePath .. "AbilityIcons_SX399.png"
sPrefix = "AbilityIco|SX-399|"
sDLPath = "Root/Images/AdventureUI/Abilities/SX-399|"

--Listing
local zaSX399List = {}
zaSX399List[1] = {"", {"Quickload"}}
zaSX399List[2] = {"", {"Cheer", "FlakRound", "Improvise", "MeltaBlast", "PalookaPunch", "Pluck", "Scavenge", "ScrapRepair", "Sear", "ThunderboltSlug"}}
zaSX399List[3] = {"", {"AngryYelling", "HeatSiphon", "JScavenge", "SniperShot", "YearsOfExperience"}}

--Rip call.
fnRipList(zaSX399List, sPath, sPrefix, sAutoloaderName, sDLPath)

-- |[ ================================== Ability Icons - Zeke ================================== ]|
--Cute but deadly. Zeke's ability icons.
sPath = sBasePath .. "AbilityIcons_Zeke.png"
sPrefix = "AbilityIco|Zeke|"
sDLPath = "Root/Images/AdventureUI/Abilities/Zeke|"

--Listing
local zaZekeList = {}
zaZekeList[1] = {"Goat|",   {"Headbutt", "HornSlash", "Rupture", "Spit", "Nuzzle", "Superjump", "GoatQuake"}}
zaZekeList[2] = {"StatUp|", {"Health", "Power", "Accuracy", "Evade", "Initiative", "ItemPower", "BleedDam", "StrikeDam"}}

--Rip call.
fnRipList(zaZekeList, sPath, sPrefix, sAutoloaderName, sDLPath)

-- |[ ======================================== Clean Up ======================================== ]|
--Finish
SLF_Close()