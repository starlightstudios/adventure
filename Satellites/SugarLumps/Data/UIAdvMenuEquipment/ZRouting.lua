-- |[ ====================================== Equipment UI ====================================== ]|
--Equipment UI. Changed equipment and compare.
local sBasePath = fnResolvePath()

-- |[Setup]|
SLF_Open("Output/UIAdvMenuEquipment.slf")
ImageLump_SetCompression(1)

--Autoloader Variables
local sAutoloaderName = "AutoLoad|Adventure|AdvMenuEquipment"
local sDLPath = "Root/Images/AdventureUI/Equipment/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[ ======================================== Ripping ========================================= ]|
-- |[Lists]|
local sPrefix = "AdvEquipment|"
local saNames = {"Arrow Lft", "Arrow Rgt", "Description Frame", "Equipment Detail", "Expandable Header", "Frames Rgt", "Frames Top", "Replacement Divider", "Scrollbar Front", "Scrollbar Back"}
local saPaths = saNames

--Ripping loop.
for i = 1, #saNames, 1 do
	fnRipImageAuto(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0, sAutoloaderName, sDLPath .. saNames[i])
end
-- |[ ======================================== Clean Up ======================================== ]|
--Finish
SLF_Close()


