-- |[ ===================================== Northwoods Map ===================================== ]|
--A nice hike in the mountains.
local sBasePath = fnResolvePath()

-- |[Lo-Def Handlers]|
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Maps_Northwoods.slf")
    ImageLump_SetScales(1.0, 1.0, 0, 0)
else
	SLF_Open("Output/MapsLD_Northwoods.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ========================================= Ripping ======================================== ]|
-- |[Setup]|
ImageLump_SetCompression(1)
ImageLump_SetScales(1.0, 1.0, 0, 0)

-- |[Northwoods]|
local sAutoloaderName = "AutoLoad|Adventure|Map_Northwoods"
local sAutoloaderPath = "Root/Images/AdvMaps/Northwoods/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

--Base.
fnRipImageAuto("Northwoods|Base",  sBasePath .. "Northwoods_Base.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Base")
fnRipImageAuto("Northwoods|Rooms", sBasePath .. "Room Overlay.png",    0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Rooms")

--Individual Pieces
for i = 0, 28, 1 do
    local sName = string.format("Northwoods|Piece%02i", i)
    local sPath = string.format(sBasePath .. "Northwoods_%02i.png", i)
    local sAutoPath = string.format(sAutoloaderPath .. "Piece%02i", i)
    fnRipImageAuto(sName, sPath, 0, 0, -1, -1, 0, sAutoloaderName, sAutoPath)
end

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the file.
SLF_Close()

--Clean the scales if they got changed.
ImageLump_SetScales(1.0, 1.0, 0, 0)