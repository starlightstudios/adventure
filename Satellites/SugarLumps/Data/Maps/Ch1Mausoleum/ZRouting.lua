-- |[ ===================================== Mausoleum Map ====================================== ]|
--Depths of Trannadar.
local sBasePath = fnResolvePath()

-- |[Lo-Def Handlers]|
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Maps_Ch1_Mausoleum.slf")
    ImageLump_SetScales(1.0, 1.0, 0, 0)
else
	SLF_Open("Output/MapsLD_Ch1_Mausoleum.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ========================================= Ripping ======================================== ]|
-- |[Setup]|
ImageLump_SetCompression(1)
ImageLump_SetScales(1.0, 1.0, 0, 0)

-- |[Autoloader]|
sAutoloaderName = "AutoLoad|Adventure|Map_Mausoleum"
sAutoloaderPath = "Root/Images/AdvMaps/Mausoleum/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[Base/Overlay]|
fnRipImageAuto("Base",    sBasePath .. "Mausoleum_Base.png",    0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Base")
fnRipImageAuto("Overlay", sBasePath .. "Mausoleum_Overlay.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Overlay")

-- |[Slices]|
for i = 0, 11, 1 do
    local sNumber = string.format("%02i", i)
    fnRipImageAuto("Slice"..sNumber, sBasePath .. "Mausoleum_" .. sNumber .. ".png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Slice_" .. sNumber)
end

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the file.
SLF_Close()

--Clean the scales if they got changed.
ImageLump_SetScales(1.0, 1.0, 0, 0)