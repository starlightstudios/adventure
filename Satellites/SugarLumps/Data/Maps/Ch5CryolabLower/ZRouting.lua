-- |[ =================================== Lower Cryolabs Map =================================== ]|
--Layered map of the lower part of the cryogenics facility.
local sBasePath = fnResolvePath()

-- |[Lo-Def Handlers]|
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Maps_CryoLower.slf")
    ImageLump_SetScales(1.0, 1.0, 0, 0)
else
	SLF_Open("Output/MapsLD_CryoLower.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ========================================= Ripping ======================================== ]|
-- |[Setup]|
ImageLump_SetCompression(1)
ImageLump_SetScales(1.0, 1.0, 0, 0)

-- |[Autoloader]|
sAutoloaderName = "AutoLoad|Adventure|Map_CryoLower"
sAutoloaderPath = "Root/Images/AdvMaps/Cryogenics/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[Sequence]|
for i = 0, 5, 1 do
    fnRipImageAuto("CryolabLower" .. i, sBasePath .. "Map"..i..".png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "CryolabLower"  .. i)
end

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the file.
SLF_Close()

--Clean the scales if they got changed.
ImageLump_SetScales(1.0, 1.0, 0, 0)