-- |[ ==================================== CRT Noise Overlay =================================== ]|
--Used during the "dream" reprogramming sequence.
local sBasePath = fnResolvePath()

-- |[Lo-Def Handlers]|
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Maps_CRTNoise.slf")
    ImageLump_SetScales(1.0, 1.0, 0, 0)
else
	SLF_Open("Output/MapsLD_CRTNoise.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ========================================= Ripping ======================================== ]|
-- |[Setup]|
ImageLump_SetCompression(1)
ImageLump_SetScales(1.0, 1.0, 0, 0)

-- |[Autoloader]|
sAutoloaderName = "AutoLoad|Adventure|Overlay_CRTNoise"
sAutoloaderPath = "Root/Images/Overlays/CRTNoise/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[CRT Noise]|
--Overlay used during the finale of chapter 5.
for i = 0, 9, 1 do
    fnRipImageAuto("Overlay|CRT" .. i, sBasePath .. "Noise" .. i .. ".png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "CRT" .. i)
end

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the file.
SLF_Close()

--Clean the scales if they got changed.
ImageLump_SetScales(1.0, 1.0, 0, 0)