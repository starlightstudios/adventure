-- |[ ================================= Map Pieces and Frames ================================== ]|
--Parts like map indicators, frames, and other miscellaneous map parts.
local sBasePath = fnResolvePath()

-- |[Lo-Def Handlers]|
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Maps_System.slf")
    ImageLump_SetScales(1.0, 1.0, 0, 0)
else
	SLF_Open("Output/MapsLD_System.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ==================================== Chapter 1 Pieces ==================================== ]|
-- |[Setup]|
ImageLump_SetCompression(1)
ImageLump_SetScales(1.0, 1.0, 0, 0)

-- |[Map Indicators]|
--Icons used to indicate player position on the maps.
local sAutoloaderName = "AutoLoad|Adventure|Map_Ch1Pieces"
local sAutoloaderPath = "Root/Images/AdvMaps/MapIndicators/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

--Rip.
fnRipImageAuto("MapInd|Mei_Human", sBasePath .. "Mei_Human.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Mei_Human")

-- |[ ==================================== Chapter 2 Pieces ==================================== ]|
-- |[Setup]|
ImageLump_SetCompression(1)
ImageLump_SetScales(1.0, 1.0, 0, 0)

-- |[Map Indicators]|
--Icons used to indicate player position on the maps.
sAutoloaderName = "AutoLoad|Adventure|Map_Ch2Pieces"
sAutoloaderPath = "Root/Images/AdvMaps/MapIndicators/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

--Rip.
fnRipImageAuto("MapInd|Sanya_Human",  sBasePath .. "Sanya_Human.png",  0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Sanya_Human")
fnRipImageAuto("MapInd|Sanya_Sevavi", sBasePath .. "Sanya_Sevavi.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Sanya_Sevavi")
fnRipImageAuto("MapInd|Sanya_Bat",    sBasePath .. "Sanya_Bat.png",    0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Sanya_Bat")

--Note: Campfires and other map icons are placed on the maps directly, and are not compressed.

-- |[ ==================================== Chapter 5 Pieces ==================================== ]|
-- |[PDU Frame]|
--Appears on the edge of the computer maps.
sAutoloaderName = "AutoLoad|Adventure|Map_Ch5Pieces"
sAutoloaderPath = "Root/Images/AdvMaps/General/PDUFrame"
SLF_RegisterAutoLoadLump(sAutoloaderName)

--Rip.
fnRipImageAuto("PDUFrame", sBasePath .. "PDUFrame.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "PDUFrame")

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the file.
SLF_Close()

--Clean the scales if they got changed.
ImageLump_SetScales(1.0, 1.0, 0, 0)