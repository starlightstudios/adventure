-- |[ ==================================== Regulus Moon Map ==================================== ]|
--Map of chapter 5, single layer.
local sBasePath = fnResolvePath()

-- |[Lo-Def Handlers]|
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Maps_Regulus.slf")
    ImageLump_SetScales(1.0, 1.0, 0, 0)
else
	SLF_Open("Output/MapsLD_Regulus.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ========================================= Ripping ======================================== ]|
-- |[Setup]|
ImageLump_SetCompression(1)
ImageLump_SetScales(1.0, 1.0, 0, 0)

-- |[Autoloader]|
sAutoloaderName = "AutoLoad|Adventure|Map_Regulus"
sAutoloaderPath = "Root/Images/AdvMaps/General/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[Single Pieces]|
fnRipImageAuto("RegulusMap", sBasePath .. "RegulusMap.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "RegulusMap")

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the file.
SLF_Close()

--Clean the scales if they got changed.
ImageLump_SetScales(1.0, 1.0, 0, 0)