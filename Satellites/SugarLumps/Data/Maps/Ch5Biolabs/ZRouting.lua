-- |[ ======================================= Biolabs Map ====================================== ]|
--What it says on the tin.
local sBasePath = fnResolvePath()

-- |[Lo-Def Handlers]|
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Maps_Biolabs.slf")
    ImageLump_SetScales(1.0, 1.0, 0, 0)
else
	SLF_Open("Output/MapsLD_Biolabs.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ========================================= Ripping ======================================== ]|
-- |[Setup]|
ImageLump_SetCompression(1)

-- |[Autoloader]|
sAutoloaderName = "AutoLoad|Adventure|Map_Biolabs"
sAutoloaderPath = "Root/Images/AdvMaps/General/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[Single Pieces]|
fnRipImageAuto("BiolabsMap", sBasePath .. "BiolabsMap.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "BiolabsMap")

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the file.
SLF_Close()

--Clean the scales if they got changed.
ImageLump_SetScales(1.0, 1.0, 0, 0)