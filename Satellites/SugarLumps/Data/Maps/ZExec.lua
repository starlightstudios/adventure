-- |[ =================================== Maps and Overlays ==================================== ]|
--Maps that allow the player to see where the hell they are in Adventure Mode. This file calls
-- a series of subfiles.
local sBasePath = fnResolvePath()

-- |[ ==================================== Local Functions ===================================== ]|
-- |[ ======================================== Execution ======================================= ]|
-- |[Flags]|
local baCompressionFlags = {}
local saList = {"Ch1Mausoleum", "Ch1NixNedar", "Ch1Trannadar", "Ch1TrannadarWest", "Ch2MtSarulente", "Ch2Northwoods", "Ch2Westwoods", "Ch5Biolabs", "Ch5CryolabLower", 
                "Ch5CryolabMain", "Ch5Equinox", "Ch5LRTEast", "Ch5LRTWest", "Ch5Noise", "Ch5Regulus", "System"}
ImageLump_SetAllowLineCompression(true)

-- |[Normal Definition]|
--Normal definition. Always executes.
gbUseLowDefinition = false
for i = 1, #saList, 1 do
    baCompressionFlags[i] = fnCompressFolder(sBasePath, saList[i], "ZRouting")
end

-- |[Low Definition]|
--Low-definition stuff. Only executes if the normal definition version did.
gbUseLowDefinition = true
for i = 1, #saList, 1 do
    if(baCompressionFlags[i] == true) then 
        io.write(" " .. saList[i] .. " Lo-Def : Executing - ")
        LM_ExecuteScript(sBasePath .. saList[i] .. "/ZRouting.lua") 
        io.write("done.\n")
    end
end
gbUseLowDefinition = false

-- |[Clean]|
ImageLump_SetAllowLineCompression(false)


