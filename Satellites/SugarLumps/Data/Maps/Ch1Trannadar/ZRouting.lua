-- |[ ===================================== Trannadar Map ====================================== ]|
--'Central' Trannadar.
local sBasePath = fnResolvePath()

-- |[Lo-Def Handlers]|
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Maps_Ch1_Trannadar.slf")
    ImageLump_SetScales(1.0, 1.0, 0, 0)
else
	SLF_Open("Output/MapsLD_Ch1_Trannadar.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ========================================= Ripping ======================================== ]|
-- |[Setup]|
ImageLump_SetCompression(1)
ImageLump_SetScales(1.0, 1.0, 0, 0)

-- |[Autoloader]|
sAutoloaderName = "AutoLoad|Adventure|Map_Trannadar"
sAutoloaderPath = "Root/Images/AdvMaps/General/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[Base/Overlay]|
sAutoloaderPath = "Root/Images/AdvMaps/Trannadar/"
fnRipImageAuto("Base",    sBasePath .. "Trannadar_Base.png",    0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Base")
fnRipImageAuto("Overlay", sBasePath .. "Trannadar_Overlay.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Overlay")

-- |[Slices]|
for i = 0, 37, 1 do
    local sNumber = string.format("%02i", i)
    fnRipImageAuto("Slice"..sNumber, sBasePath .. "Trannadar_" .. sNumber .. ".png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Slice_" .. sNumber)
end

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the file.
SLF_Close()

--Clean the scales if they got changed.
ImageLump_SetScales(1.0, 1.0, 0, 0)