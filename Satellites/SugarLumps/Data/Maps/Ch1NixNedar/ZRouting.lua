-- |[ ================================== Nix Nedar Background ================================== ]|
--Stars in every direction.
local sBasePath = fnResolvePath()

-- |[Lo-Def Handlers]|
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Maps_NixNedar.slf")
    ImageLump_SetScales(1.0, 1.0, 0, 0)
else
	SLF_Open("Output/MapsLD_NixNedar.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ========================================= Ripping ======================================== ]|
-- |[Setup]|
ImageLump_SetCompression(1)
ImageLump_SetScales(1.0, 1.0, 0, 0)

-- |[Autoloader]|
sAutoloaderName = "AutoLoad|Adventure|Background_NixNedar"
sAutoloaderPath = "Root/Images/AdvMaps/Backgrounds/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

-- |[Single Pieces]|
fnRipImageAuto("NixNedarBacking", sBasePath .. "NixNedarBacking.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "NixNedar")

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the file.
SLF_Close()

--Clean the scales if they got changed.
ImageLump_SetScales(1.0, 1.0, 0, 0)