-- |[ =================================== Mt. Sarulente Map ==================================== ]|
--"The Dragon's Eye"
local sBasePath = fnResolvePath()

-- |[Lo-Def Handlers]|
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Maps_MtSarulente.slf")
    ImageLump_SetScales(1.0, 1.0, 0, 0)
else
	SLF_Open("Output/MapsLD_MtSarulente.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ========================================= Ripping ======================================== ]|
-- |[Setup]|
ImageLump_SetCompression(1)
ImageLump_SetScales(1.0, 1.0, 0, 0)

-- |[Westwoods]|
sAutoloaderName = "AutoLoad|Adventure|Map_MtSarulente"
sAutoloaderPath = "Root/Images/AdvMaps/MtSarulente/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

--Base.
fnRipImageAuto("MtSarulente|Base",  sBasePath .. "Base.png",    0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Base")
fnRipImageAuto("MtSarulente|Rooms", sBasePath .. "Overlay.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Rooms")

--Individual Pieces
for i = 0, 14, 1 do
    local sName = string.format("MtSarulente|Piece%02i", i)
    local sPath = string.format(sBasePath .. "%02i.png", i)
    local sAutoPath = string.format(sAutoloaderPath .. "Piece%02i", i)
    fnRipImageAuto(sName, sPath, 0, 0, -1, -1, 0, sAutoloaderName, sAutoPath)
end

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the file.
SLF_Close()

--Clean the scales if they got changed.
ImageLump_SetScales(1.0, 1.0, 0, 0)