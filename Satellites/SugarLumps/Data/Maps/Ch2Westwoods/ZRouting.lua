-- |[ ===================================== Westwoods Map ====================================== ]|
--"The Cauldron"
local sBasePath = fnResolvePath()

-- |[Lo-Def Handlers]|
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Maps_Westwoods.slf")
    ImageLump_SetScales(1.0, 1.0, 0, 0)
else
	SLF_Open("Output/MapsLD_Westwoods.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ========================================= Ripping ======================================== ]|
-- |[Setup]|
ImageLump_SetCompression(1)
ImageLump_SetScales(1.0, 1.0, 0, 0)

-- |[Westwoods]|
sAutoloaderName = "AutoLoad|Adventure|Map_Westwoods"
sAutoloaderPath = "Root/Images/AdvMaps/Westwoods/"
SLF_RegisterAutoLoadLump(sAutoloaderName)

--Base.
fnRipImageAuto("Westwoods|Base",  sBasePath .. "Westwoods Base.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Base")
fnRipImageAuto("Westwoods|Rooms", sBasePath .. "Room Overlay.png",   0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "Rooms")

--Individual Pieces
for i = 0, 38, 1 do
    local sName = string.format("Westwoods|Piece%02i", i)
    local sPath = string.format(sBasePath .. "Westwoods_%02i.png", i)
    local sAutoPath = string.format(sAutoloaderPath .. "Piece%02i", i)
    fnRipImageAuto(sName, sPath, 0, 0, -1, -1, 0, sAutoloaderName, sAutoPath)
end

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the file.
SLF_Close()

--Clean the scales if they got changed.
ImageLump_SetScales(1.0, 1.0, 0, 0)