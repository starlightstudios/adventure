-- |[ ================================== Christine TF Images =================================== ]|
--Scene images for Christine's many transformations.
local sBasePath = fnResolvePath()

-- |[File Creation]|
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/AdvScn_ChristineTF.slf")
else
	SLF_Open("Output/AdvScnLD_ChristineTF.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ======================================== Ripping ========================================= ]|
--Neutral poses.
ImageLump_Rip("Christine|Male",   sBasePath .. "Christine_Male.png",   0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|Female", sBasePath .. "Christine_Female.png", 0, 0, -1, -1, gciNoTransparencies)

--Christine to Darkmatter. Two parts.
ImageLump_Rip("Christine|DarkmatterTF0", sBasePath .. "Christine_Darkmatter_TF0.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|DarkmatterTF1", sBasePath .. "Christine_Darkmatter_TF1.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|DarkmatterTF2", sBasePath .. "Christine_Darkmatter_TF2.png", 0, 0, -1, -1, gciNoTransparencies)

--Christine to Doll. Six parts.
ImageLump_Rip("Christine|DollTF0", sBasePath .. "Christine_Doll_TF0.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|DollTF1", sBasePath .. "Christine_Doll_TF1.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|DollTF2", sBasePath .. "Christine_Doll_TF2.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|DollTF3", sBasePath .. "Christine_Doll_TF3.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|DollTF4", sBasePath .. "Christine_Doll_TF4.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|DollTF5", sBasePath .. "Christine_Doll_TF5.png", 0, 0, -1, -1, gciNoTransparencies)

--Chris to Christine to Golem. Six parts.
ImageLump_Rip("Christine|GolemTF0", sBasePath .. "Christine_Golem_TF0.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|GolemTF1", sBasePath .. "Christine_Golem_TF1.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|GolemTF2", sBasePath .. "Christine_Golem_TF2.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|GolemTF3", sBasePath .. "Christine_Golem_TF3.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|GolemTF4", sBasePath .. "Christine_Golem_TF4.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|GolemTF5", sBasePath .. "Christine_Golem_TF5.png", 0, 0, -1, -1, gciNoTransparencies)

--Christine to Latex Drone.
ImageLump_Rip("Christine|LatexTF0", sBasePath .. "Christine_Latex_TF0.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|LatexTF1", sBasePath .. "Christine_Latex_TF1.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|LatexTF2", sBasePath .. "Christine_Latex_TF2.png", 0, 0, -1, -1, gciNoTransparencies)

--Christine to Raiju
ImageLump_Rip("Christine|RaijuTF0", sBasePath .. "Christine_RaijuTF0.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|RaijuTF1", sBasePath .. "Christine_RaijuTF1.png", 0, 0, -1, -1, gciNoTransparencies)

--Christine to Steam Droid
ImageLump_Rip("Christine|SteamTF0", sBasePath .. "Christine_SteamDroidTF0.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|SteamTF1", sBasePath .. "Christine_SteamDroidTF1.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|SteamTF2", sBasePath .. "Christine_SteamDroidTF2.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|SteamTF3", sBasePath .. "Christine_SteamDroidTF3.png", 0, 0, -1, -1, gciNoTransparencies)

--Christine to Eldritch Dreamer
ImageLump_Rip("Christine|DreamerTF0", sBasePath .. "Christine_EDG_TF0.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|DreamerTF1", sBasePath .. "Christine_EDG_TF1.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|DreamerTF2", sBasePath .. "Christine_EDG_TF2.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|DreamerTF3", sBasePath .. "Christine_EDG_TF3.png", 0, 0, -1, -1, gciNoTransparencies)

--Christine to Electrosprite. Three parts.
ImageLump_Rip("Christine|ElectrospriteTF0", sBasePath .. "Christine_Electrosprite_TF0.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|ElectrospriteTF1", sBasePath .. "Christine_Electrosprite_TF1.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|ElectrospriteTF2", sBasePath .. "Christine_Electrosprite_TF2.png", 0, 0, -1, -1, gciNoTransparencies)

--Christine to Secrebot. 13 parts.
ImageLump_Rip("Christine|SecrebotTF00", sBasePath .. "Christine_Secrebot_TF00.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|SecrebotTF01", sBasePath .. "Christine_Secrebot_TF01.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|SecrebotTF02", sBasePath .. "Christine_Secrebot_TF02.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|SecrebotTF03", sBasePath .. "Christine_Secrebot_TF03.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|SecrebotTF04", sBasePath .. "Christine_Secrebot_TF04.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|SecrebotTF05", sBasePath .. "Christine_Secrebot_TF05.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|SecrebotTF06", sBasePath .. "Christine_Secrebot_TF06.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|SecrebotTF07", sBasePath .. "Christine_Secrebot_TF07.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|SecrebotTF08", sBasePath .. "Christine_Secrebot_TF08.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|SecrebotTF09", sBasePath .. "Christine_Secrebot_TF09.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|SecrebotTF10", sBasePath .. "Christine_Secrebot_TF10.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|SecrebotTF11", sBasePath .. "Christine_Secrebot_TF11.png", 0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Christine|SecrebotTF12", sBasePath .. "Christine_Secrebot_TF12.png", 0, 0, -1, -1, gciNoTransparencies)

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the SLF file.
SLF_Close()
