--[ =============================== Chapter 5 Gala Dress Sequence =============================== ]
--Christine and Sophie put on their lovely gala dresses.
local sBasePath = fnResolvePath()

--[File Creation]
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/AdvScn_GalaDress.slf")
else
	SLF_Open("Output/AdvScnLD_GalaDress.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

--[ ========================================== Ripping ========================================== ]
--Sequence
ImageLump_Rip("SophieDress|Seq0", sBasePath .. "SophieDressSeq0.png", 0, 0, -1, -1, 0)
ImageLump_Rip("SophieDress|Seq1", sBasePath .. "SophieDressSeq1.png", 0, 0, -1, -1, 0)
ImageLump_Rip("SophieDress|Seq2", sBasePath .. "SophieDressSeq2.png", 0, 0, -1, -1, 0)

--[ ========================================= Finish Up ========================================= ]
--Close and write the SLF file.
SLF_Close()
