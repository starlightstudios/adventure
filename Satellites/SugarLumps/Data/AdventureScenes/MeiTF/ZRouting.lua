-- |[ ===================================== Mei TF Images ====================================== ]|
--Scene images for Mei getting turned into various monstergirls.
local sBasePath = fnResolvePath()

-- |[File Creation]|
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/AdvScn_MeiTF.slf")
else
	SLF_Open("Output/AdvScnLD_MeiTF.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ======================================== Ripping ========================================= ]|
--Alraune Sequence
ImageLump_Rip("Mei|AlrauneTF0", sBasePath .. "Mei Alraune TF 0.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Mei|AlrauneTF1", sBasePath .. "Mei Alraune TF 1.png", 0, 0, -1, -1, 0)

--Bee Sequence
ImageLump_Rip("Mei|BeeTF0", sBasePath .. "Mei Bee TF 0.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Mei|BeeTF1", sBasePath .. "Mei Bee TF 1.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Mei|BeeTF2", sBasePath .. "Mei Bee TF 2.png", 0, 0, -1, -1, 0)

--Ghost Sequence
ImageLump_Rip("Mei|GhostTF0", sBasePath .. "Mei Ghost TF 0.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Mei|GhostTF1", sBasePath .. "Mei Ghost TF 1.png", 0, 0, -1, -1, 0)

--Gravemarker Sequence
ImageLump_Rip("Mei|GravemarkerTF0", sBasePath .. "Mei Gravemarker TF 0.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Mei|GravemarkerTF1", sBasePath .. "Mei Gravemarker TF 1.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Mei|GravemarkerTF2", sBasePath .. "Mei Gravemarker TF 2.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Mei|GravemarkerTF3", sBasePath .. "Mei Gravemarker TF 3.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Mei|GravemarkerTF4", sBasePath .. "Mei Gravemarker TF 4.png", 0, 0, -1, -1, 0)

--Slime Sequence
ImageLump_Rip("Mei|SlimeTF0", sBasePath .. "Mei Slime TF 0.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Mei|SlimeTF1", sBasePath .. "Mei Slime TF 1.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Mei|SlimeTF2", sBasePath .. "Mei Slime TF 2.png", 0, 0, -1, -1, 0)

--Werecat Sequence
ImageLump_Rip("Mei|WerecatTF0", sBasePath .. "Mei Werecat TF 0.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Mei|WerecatTF1", sBasePath .. "Mei Werecat TF 1.png", 0, 0, -1, -1, 0)

--Zombee Sequence
ImageLump_Rip("Mei|ZombeeTF0", sBasePath .. "Zombee TF 0.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Mei|ZombeeTF1", sBasePath .. "Zombee TF 1.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Mei|ZombeeTF2", sBasePath .. "Zombee TF 2.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Mei|ZombeeTF3", sBasePath .. "Zombee TF 3.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Mei|ZombeeTF4", sBasePath .. "Zombee TF 4.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Mei|ZombeeTF5", sBasePath .. "Zombee TF 5.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Mei|ZombeeTF6", sBasePath .. "Zombee TF 6.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Mei|ZombeeTF7", sBasePath .. "Zombee TF 7.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Mei|ZombeeTF8", sBasePath .. "Zombee TF 8.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Mei|ZombeeTF9", sBasePath .. "Zombee TF 9.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Mei|ZombeeTFA", sBasePath .. "Zombee TF A.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Mei|ZombeeTFB", sBasePath .. "Zombee TF B.png", 0, 0, -1, -1, 0)

-- |[ =================================== Autoload Sequences =================================== ]|
--Wisphag Sequence
local sAutoloaderName = "AutoLoad|Adventure|Mei_Wisphag_TF"
local sAutoloaderPath = "Root/Images/Scenes/Mei/WisphagTF"
SLF_RegisterAutoLoadLump(sAutoloaderName)
for i = 0, 7, 1 do
    fnRipImageAuto("Mei|WisphagTF"..i, sBasePath .. "Mei Wisphag TF "..i..".png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. i)
end

--Zombee Sequence
sAutoloaderName = "AutoLoad|Adventure|Mei_Zombee_TF"
sAutoloaderPath = "Root/Images/Scenes/Mei/ZombeeTF"
SLF_RegisterAutoLoadLump(sAutoloaderName)
fnRipImageAuto("Mei|ZombeeTF0", sBasePath .. "Zombee TF 0.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "0")
fnRipImageAuto("Mei|ZombeeTF1", sBasePath .. "Zombee TF 1.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "1")
fnRipImageAuto("Mei|ZombeeTF2", sBasePath .. "Zombee TF 2.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "2")
fnRipImageAuto("Mei|ZombeeTF3", sBasePath .. "Zombee TF 3.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "3")
fnRipImageAuto("Mei|ZombeeTF4", sBasePath .. "Zombee TF 4.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "4")
fnRipImageAuto("Mei|ZombeeTF5", sBasePath .. "Zombee TF 5.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "5")
fnRipImageAuto("Mei|ZombeeTF6", sBasePath .. "Zombee TF 6.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "6")
fnRipImageAuto("Mei|ZombeeTF7", sBasePath .. "Zombee TF 7.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "7")
fnRipImageAuto("Mei|ZombeeTF8", sBasePath .. "Zombee TF 8.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "8")
fnRipImageAuto("Mei|ZombeeTF9", sBasePath .. "Zombee TF 9.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "9")
fnRipImageAuto("Mei|ZombeeTFA", sBasePath .. "Zombee TF A.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "A")
fnRipImageAuto("Mei|ZombeeTFB", sBasePath .. "Zombee TF B.png", 0, 0, -1, -1, 0, sAutoloaderName, sAutoloaderPath .. "B")

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the SLF file.
SLF_Close()
