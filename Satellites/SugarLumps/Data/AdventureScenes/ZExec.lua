-- |[ ==================================== Scene Compression =================================== ]|
--Executes scene compression.
Debug_PushPrint(false, "Beginning scene compression.\n")

--Variables.
local sCurrentPath = fnResolvePath()
local baCompressionFlags = {}

-- |[Automated Compression Function]|
local fnCompressFolder = function(sFolderName, sExecFile)

	--Setup
	local bCompressedAnything = false
	local sCurrentPath = fnResolvePath()

	--Push the stack.
	TS_PushStack()

		--Get starting data.
		TS_LoadFrom(sCurrentPath .. sFolderName .. ".log")

		--Scan for new files/modifications/removals.
		local bHasDirectoryChanged = TS_ScanDirectory(sCurrentPath .. sFolderName .. "/")
		if(bHasDirectoryChanged) then
			--Compress here.
			io.write(" " .. sFolderName .. ": Files have changed - ")
			--io.write("Exec: " .. sCurrentPath .. sFolderName .. "/" .. sExecFile .. ".lua" .. "\n")
			LM_ExecuteScript(sCurrentPath .. sFolderName .. "/" .. sExecFile .. ".lua")
			io.write("done.\n")
			bCompressedAnything = true
			
			--Save a log file.
			TS_PrintTo(sCurrentPath .. sFolderName .. ".log")
		else
			io.write(" " .. sFolderName .. ": Files have not changed.\n")
		end
	
	--Clean up.
	TS_PopStack()
	return bCompressedAnything
end

-- |[Construction]|
--Adder function.
local iTotalEntries = 0
local saEntryList = {}
local fnAddEntry = function(sName, sRoutingFile)
    if(sName == nil) then return end
    if(sRoutingFile == nil) then sRoutingFile = "ZRouting" end
    
    iTotalEntries = iTotalEntries + 1
    saEntryList[iTotalEntries] = {sName, sRoutingFile}
end

--Build an ordered list.
fnAddEntry("CassandraTF")
fnAddEntry("CH0Major")
fnAddEntry("CH1Major")
fnAddEntry("CH2Major")
fnAddEntry("CH5Major")
fnAddEntry("ChristineTF")
fnAddEntry("GalaDress")
fnAddEntry("MeiTF")
fnAddEntry("Runestone_Mei")

-- |[Execution]|
--Auto-exec the folders.
gbUseLowDefinition = false
ImageLump_SetAllowLineCompression(true)
for i = 1, iTotalEntries, 1 do
    baCompressionFlags[i] = fnCompressFolder(saEntryList[i][1], saEntryList[i][2])
end
ImageLump_SetAllowLineCompression(false)

--Low-definition stuff.
gbUseLowDefinition = true
ImageLump_SetAllowLineCompression(true)
for i = 1, iTotalEntries, 1 do
    if(baCompressionFlags[i] == true) then
        LM_ExecuteScript(fnResolvePath() .. saEntryList[i][1] .. "/ZRouting.lua")
        
    end
end
ImageLump_SetAllowLineCompression(false)
gbUseLowDefinition = false

--Done
Debug_PopPrint("Finished portrait compression activities.\n")