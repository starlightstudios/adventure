-- |[ ================================= Chapter 1 Scene Images ================================= ]|
--Images used for chapter 1, but not part of a TF sequence.
local sBasePath = fnResolvePath()

-- |[File Creation]|
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/AdvScn_CH1Major.slf")
else
	SLF_Open("Output/AdvScnLD_CH1Major.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ======================================== Ripping ========================================= ]|
--Introduction
ImageLump_Rip("Introduction", sBasePath .. "Introduction.png", 0, 0, -1, -1, 0)

--Adina's Scene
ImageLump_Rip("AdinaSpecial", sBasePath .. "AdinaSpecial.png", 0, 0, -1, -1, 0)

--Slime Art Gallery
ImageLump_Rip("SlimeGallery|Goodiva",     sBasePath .. "Goodiva.png",     0, 0, -1, -1, 0)
ImageLump_Rip("SlimeGallery|SlimaLisa",   sBasePath .. "SlimaLisa.png",   0, 0, -1, -1, 0)
ImageLump_Rip("SlimeGallery|Washlimeton", sBasePath .. "Washlimeton.png", 0, 0, -1, -1, 0)

--Rubber Finale
ImageLump_Rip("RubberMeteor", sBasePath .. "RubberMeteor.png", 0, 0, -1, -1, 0)

--Mannequin Bad End
ImageLump_Rip("MannBadEnd0", sBasePath .. "MannBadEnd0.png", 0, 0, -1, -1, 0)
ImageLump_Rip("MannBadEnd1", sBasePath .. "MannBadEnd1.png", 0, 0, -1, -1, 0)

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the SLF file.
SLF_Close()
