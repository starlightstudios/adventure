-- |[ ================================= Chapter 2 Scene Images ================================= ]|
--Images used for chapter 2, but not part of a TF sequence.
local sBasePath = fnResolvePath()

-- |[File Creation]|
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/AdvScn_CH2Major.slf")
else
	SLF_Open("Output/AdvScnLD_CH2Major.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[Local Function]|
local function fnRipSequence(psInfileNamePattern, psImagePathPattern, piFrameCount, psAutoloaderName, psAutoloaderPathPattern)
	
	-- |[Argument Check]|
	if(psInfileNamePattern     == nil) then return end
	if(psImagePathPattern      == nil) then return end
	if(piFrameCount            == nil) then return end
	if(psAutoloaderName        == nil) then return end
	if(psAutoloaderPathPattern == nil) then return end
	
	-- |[Register]|
	SLF_RegisterAutoLoadLump(psAutoloaderName)
	
	-- |[Execution]|
	for i = 0, piFrameCount-1, 1 do
	
		--Establish names using the format function.
		local sInfileName = string.format(psInfileNamePattern, i)
		local sImagePath  = string.format(psImagePathPattern, i)
		local sDLPath     = string.format(psAutoloaderPathPattern, i)
	
		--Execute.
		fnRipImageAuto(sInfileName, sImagePath, 0, 0, -1, -1, 0, psAutoloaderName, sDLPath)
	end
end

-- |[ ======================================== Ripping ========================================= ]|
-- |[Automated Sequences]|
fnRipSequence("Introduction|Introduction%02i", sBasePath .. "Introduction/Introduction%02i.png",  1, "AutoLoad|Adventure|Ch2_Introduction", "Root/Images/Scenes/Introduction/Introduction%02i")
fnRipSequence("Kissing|TheKiss%02i",           sBasePath .. "Kiss/TheKiss%02i.png",               3, "AutoLoad|Adventure|Ch2_Kiss",         "Root/Images/Scenes/Ch2Kissing/Kiss%02i")
fnRipSequence("BunnyTF|BunnyTF%02i",           sBasePath .. "BunnyTF/SanyaBunn%02i.png",         21, "AutoLoad|Adventure|Ch2_BunnyTF",      "Root/Images/Scenes/BunnyTF/BunnyTF%02i")
fnRipSequence("SevaviTF|SevaviTF%02i",         sBasePath .. "SevaviTF/SanyaSevavi%02i.png",       7, "AutoLoad|Adventure|Ch2_SevaviTF",     "Root/Images/Scenes/SevaviTF/SevaviTF%02i")
fnRipSequence("HarpyTF|HarpyTF%02i",           sBasePath .. "HarpyTF/SanyaHarpy%02i.png",         8, "AutoLoad|Adventure|Ch2_HarpyTF",      "Root/Images/Scenes/HarpyTF/HarpyTF%02i")
fnRipSequence("HarpyFeetTF|HarpyFeetTF%02i",   sBasePath .. "HarpyTF/SanyaHarpyFeet%02i.png",     7, "AutoLoad|Adventure|Ch2_HarpyFeetTF",  "Root/Images/Scenes/HarpyTF/HarpyFeetTF%02i")

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the SLF file.
SLF_Close()
