-- |[ ==================================== fnCompressFolder ==================================== ]|
--Automation function. Given a folder name and execution file (usually ZRouting.lua), checks if the
-- log file indicates anything has changed since last time this function was called. If it has,
-- runs the routing file to compress the folder into a .slf file.
--If the log file does not exist, creates it and runs the execution file.
--Returns true if the folder was compressed, false if nothing happened.
function fnCompressFolder(psCurrentPath, psFolderName, psExecFile)

    -- |[Argument Check]|
    if(psCurrentPath == nil) then return end
    if(psFolderName  == nil) then return end
    if(psExecFile    == nil) then return end

    -- |[Setup]|
    --Variables.
	local bCompressedAnything = false

    -- |[Execution]|
	--Push the stack.
	TS_PushStack()

		--Get starting data.
		TS_LoadFrom(psCurrentPath .. psFolderName .. ".log")

		--Scan for new files/modifications/removals.
		local bHasDirectoryChanged = TS_ScanDirectory(psCurrentPath .. psFolderName .. "/")
		if(bHasDirectoryChanged) then
			--Compress here.
			io.write(" " .. psFolderName .. ": Files have changed - ")
			--io.write("Exec: " .. psCurrentPath .. psFolderName .. "/" .. psExecFile .. ".lua" .. "\n")
			LM_ExecuteScript(psCurrentPath .. psFolderName .. "/" .. psExecFile .. ".lua")
			io.write("done.\n")
			bCompressedAnything = true
			
			--Save a log file.
			TS_PrintTo(psCurrentPath .. psFolderName .. ".log")
		else
			io.write(" " .. psFolderName .. ": Files have not changed.\n")
		end
	
	-- |[Clean Up]|
	TS_PopStack()
	return bCompressedAnything
end