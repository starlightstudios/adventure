-- |[ =================================== fnRipRunebearer() ==================================== ]|
--Rips a runebearer. Basically calls fnRipSheet and then rips the standard Crouch/Wounded poses.
function fnRipRunebearer(psRipName, psRipPath, psCWMiddle, psAutoloaderName, psAutoPath)
    
	-- |[Arg Check]|
    if(psRipName  == nil) then return end
    if(psRipPath  == nil) then return end
    if(psCWMiddle == nil) then return end
    
    --Auto-handlers are optional.
    
    -- |[Autoloader]|
    --Create the lump if it doesn't already exist.
    if(psAutoloaderName ~= nil) then
        SLF_RegisterAutoLoadLump(psAutoloaderName)
    end
    
    -- |[Basics]|
    --Execution of basic fnRipSheet()
    fnRipSheet(psRipName, psRipPath, cbEightDir, cbIsNotWide, cbHasRunAnim, cbNoCrouch, cbNoWound, psAutoloaderName, psAutoPath)
    
    -- |[Standard Special Frames]|
    --Rip the Crouch/Wounded variant.
    if(psCWMiddle ~= "Null") then
        fnRipImageAuto("Spcl|" .. psCWMiddle .. "|Crouch",  psRipPath,  0, 360, 32, 40, 0, psAutoloaderName, "Root/Images/Sprites/Special/" .. psCWMiddle .. "|Crouch")
        fnRipImageAuto("Spcl|" .. psCWMiddle .. "|Wounded", psRipPath, 32, 360, 32, 40, 0, psAutoloaderName, "Root/Images/Sprites/Special/" .. psCWMiddle .. "|Wounded")
    end
end
