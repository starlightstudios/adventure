-- |[ ====================================== fnRipSheet() ====================================== ]|
--Used for sprites that follow specific patterns. Rips the various movement directions in a standardized format.
-- Has the optional gfOffsetX and gfOffsetY variables that can offset the read points in case multiple
-- sheets are in the same image.
gfOffsetX = 0
gfOffsetY = 0
function fnRipSheet(sBaseName, sImagePath, bIsEightDir, bIsWide, bHasRunAnim, bHasCrouch, bHasWound, psAutoloaderName, psAutoPath)
	
	-- |[Arg Check]|
	if(sBaseName == nil) then return end
	if(sImagePath == nil) then return end
	if(bIsEightDir == nil) then return end
	if(bIsWide == nil) then return end
	if(bHasRunAnim == nil) then return end
	if(bHasCrouch == nil) then bHasCrouch = false end
	if(bHasWound == nil) then bHasWound = false end
    
    --Auto-handlers are optional.
    
    -- |[Autoloader]|
    --Create the lump if it doesn't already exist.
    if(psAutoloaderName ~= nil) then
        SLF_RegisterAutoLoadLump(psAutoloaderName)
    end
	
	-- |[Constants]|
	local cfStartX = 0
	local cfStartY = 40
	local cfSizeX = 32
	local cfSizeY = 40
	local saSets =   {"South", "North", "West", "East"}
	local iaFrames = {      4,       4,      4,      4}
	if(bIsEightDir) then
		saSets =   {"South", "North", "West", "East", "NE", "NW", "SW", "SE"}
		iaFrames = {      4,       4,      4,      4,    4,    4,    4,    4}
	end
	
	-- |[Wide Case]|
	if(bIsWide ~= nil and bIsWide == true) then
		cfStartX = 0
		cfStartY = 0
		cfSizeX = 50
		cfSizeY = 59
	end
	
	-- |[Walking Animation]|
	local i = 1
	while(saSets[i] ~= nil) do
		
		--Iterate.
		for p = 1, iaFrames[i], 1 do
			
			--Name.
			local sUseName = sBaseName .. "|" .. saSets[i] .. "|" .. p-1
			
			--Position.
			local fRipX = cfStartX + (cfSizeX * (p-1))
			local fRipY = cfStartY + (cfSizeY * (i-1))
			
			--Rip.
			ImageLump_Rip(sUseName, sImagePath, fRipX + gfOffsetX, fRipY + gfOffsetY, cfSizeX, cfSizeY, 0)
            
            --Autoloader.
            if(psAutoloaderName ~= nil and psAutoPath ~= nil) then
                local sUseDLPath = psAutoPath .. sBaseName .. "/" .. saSets[i] .. "|" .. (p-1)
                AutoLoader_Register(psAutoloaderName, sUseName, sUseDLPath)
            end
		end
	
		--Next.
		i = i + 1
	end
	
    -- |[Running Animation]|
	--Running animation. Only used on some sprites, mostly party members.
	if(bHasRunAnim ~= nil and bHasRunAnim == true) then
		
		--Iterate again.
		i = 1
		while(saSets[i] ~= nil) do
			
			--Iterate.
			for p = 1, iaFrames[i], 1 do
				
				--Name.
				local sUseName = sBaseName .. "|" .. saSets[i] .. "|Run" .. p-1
				
				--Position.
				local fRipX = cfStartX + (cfSizeX * (p-1)) + (cfSizeX * 4)
				local fRipY = cfStartY + (cfSizeY * (i-1))
				
				--Rip.
				ImageLump_Rip(sUseName, sImagePath, fRipX + gfOffsetX, fRipY + gfOffsetY, cfSizeX, cfSizeY, 0)
            
                --Autoloader.
                if(psAutoloaderName ~= nil and psAutoPath ~= nil) then
                    local sUseDLPath = psAutoPath .. sBaseName .. "/" .. saSets[i] .. "|Run" .. (p-1)
                    AutoLoader_Register(psAutoloaderName, sUseName, sUseDLPath)
                end
			end
		
			--Next.
			i = i + 1
		end
	end
    
    -- |[Special Frame: Crouch]|
    --Optional, found at the bottom of the sprite sheet.
    if(bHasCrouch == true) then
        
        --Position
        local fX = cfSizeX * 1.0
        local fY = cfSizeY * 0.0
        
        --Rip.
        local sUseName = "Spcl|" .. sBaseName .. "|Crouch"
        ImageLump_Rip(sUseName, sImagePath, fX, fY, cfSizeX, cfSizeY, 0)
            
        --Autoloader.
        if(psAutoloaderName ~= nil and psAutoPath ~= nil) then
            local sUseDLPath = "Root/Images/Sprites/Special/" .. sBaseName .. "|Crouch"
            AutoLoader_Register(psAutoloaderName, sUseName, sUseDLPath)
        end
        
    end
    
    -- |[Special Frame: Wounded]|
    --Optional, found at the bottom of the sprite sheet.
    if(bHasWound == true) then
        
        --Position
        local fX = cfSizeX * 2.0
        local fY = cfSizeY * 0.0
        
        --Rip.
        local sUseName = "Spcl|" .. sBaseName .. "|Wounded"
        ImageLump_Rip(sUseName, sImagePath, fX, fY, cfSizeX, cfSizeY, 0)
            
        --Autoloader.
        if(psAutoloaderName ~= nil and psAutoPath ~= nil) then
            local sUseDLPath = "Root/Images/Sprites/Special/" .. sBaseName .. "|Wounded"
            AutoLoader_Register(psAutoloaderName, sUseName, sUseDLPath)
        end
    end
end
