-- |[ =================================== fnRipImageAutoCr() =================================== ]|
--Combines a standard rip operation with an autoload operation. Creates the named lump as well.
function fnRipImageAutoCr(psName, psImgPath, piX, piY, piW, piH, piFlags, psAutoloaderName, psAutoloaderPath)
    ImageLump_Rip(psName, psImgPath, piX, piY, piW, piH, piFlags)
    if(psAutoloaderName == nil) then return end
    SLF_RegisterAutoLoadLump(psAutoloaderName)
    AutoLoader_Register(psAutoloaderName, psName, psAutoloaderPath)
end