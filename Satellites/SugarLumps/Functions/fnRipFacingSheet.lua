-- |[ ==================================== fnRipFacingSheet ==================================== ]|
--The same as fnRipSheet, except rips only the idle frames. Used for NPCs that never move.
gfOffsetX = 0
gfOffsetY = 0
function fnRipFacingSheet(sBaseName, sImagePath, iXStart, iYStart, psAutoloaderName, psAutoPath)
	
	-- |[Arg Check]|
	if(sBaseName  == nil) then return end
	if(sImagePath == nil) then return end
	if(iXStart    == nil) then return end
	if(iYStart    == nil) then return end
    
    --Autoloaders are optional.
    
    -- |[Autoloader]|
    --Create the lump if it doesn't already exist.
    if(psAutoloaderName ~= nil) then
        SLF_RegisterAutoLoadLump(psAutoloaderName)
    end
	
	-- |[Constants]|
	local cfStartX = iXStart
	local cfStartY = iYStart
	local cfSizeX = 32
	local cfSizeY = 40
	local saSets =   {"South", "North", "West", "East"}
	local iaFrames = {      1,       1,      1,      1}
	
	-- |[Rip]|
	local i = 1
	while(saSets[i] ~= nil) do
		
		--Iterate.
		for p = 1, iaFrames[i], 1 do
			
			--Name.
			local sUseName = sBaseName .. "|" .. saSets[i] .. "|" .. p-1
			
			--Position.
			local fRipX = cfStartX + (cfSizeX * (p-1))
			local fRipY = cfStartY + (cfSizeY * (i-1))
			
			--Rip.
			ImageLump_Rip(sUseName, sImagePath, fRipX + gfOffsetX, fRipY + gfOffsetY, cfSizeX, cfSizeY, 0)
            
            --Autoloader.
            if(psAutoloaderName ~= nil and psAutoPath ~= nil) then
                local sUseDLPath = psAutoPath .. sBaseName .. "/" .. saSets[i] .. "|" .. (p-1)
                AutoLoader_Register(psAutoloaderName, sUseName, sUseDLPath)
            end
			
		end
	
		--Next.
		i = i + 1
	end
end