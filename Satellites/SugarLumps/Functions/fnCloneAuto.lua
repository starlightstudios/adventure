-- |[ ====================================== fnCloneAuto() ===================================== ]|
--Given a lump name, clones it into the destination.
function fnCloneAuto(psSrcLump, psDstLump)

    -- |[Argument Check]|
    if(psSrcLump == nil) then return end
    if(psDstLump == nil) then return end

    -- |[Creation]|
    --Create the destination lump.
    SLF_RegisterAutoLoadLump(psDstLump)

    --Get the size of the source lump.
    local iSrcSize = Autoloader_QuerySize(psSrcLump)

    --Run across the stored list and add its entries to the lump in question.
    for i = 0, iSrcSize-1, 1 do
        local sName, sPath = Autoloader_QueryEntry(psSrcLump, i)
        AutoLoader_Register(psDstLump, sName, sPath)
    end
end
