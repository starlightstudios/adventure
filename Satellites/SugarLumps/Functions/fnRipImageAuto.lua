-- |[ ==================================== fnRipImageAuto() ==================================== ]|
--Combines a standard rip operation with an autoload operation.
function fnRipImageAuto(psName, psImgPath, piX, piY, piW, piH, piFlags, psAutoloaderName, psAutoloaderPath)
    ImageLump_Rip(psName, psImgPath, piX, piY, piW, piH, piFlags)
    if(psAutoloaderName == nil) then return end
    AutoLoader_Register(psAutoloaderName, psName, psAutoloaderPath)
end