-- |[ =================================== fnRipIdleAnimation =================================== ]|
--Rips the idle animation for a given sheet. Party members often have these.
function fnRipIdleAnimation(psRipName, psRipPath, piXStart, piYStart, piFrames, psAutoloaderName, psAutoPath)
    
	-- |[Arg Check]|
    if(psRipName == nil) then return end
    if(psRipPath == nil) then return end
    if(piXStart  == nil) then return end
    if(piYStart  == nil) then return end
    if(piFrames  == nil) then return end
    
    --Auto-handlers are optional.
    
    -- |[Autoloader]|
    --Create the lump if it doesn't already exist.
    if(psAutoloaderName ~= nil) then
        SLF_RegisterAutoLoadLump(psAutoloaderName)
    end
    
    -- |[Execution]|
    for i = 0, piFrames-1, 1 do
        if(psAutoloaderName == nil) then
            ImageLump_Rip("IdleAnim|" .. psRipName .. "|" .. i,  psRipPath, piXStart + (32 * i), piYStart, 32, 40, 0)
        else
            fnRipImageAuto("IdleAnim|" .. psRipName .. "|" .. i,  psRipPath, piXStart + (32 * i), piYStart, 32, 40, 0, psAutoloaderName, psAutoPath .. "IdleAnim/" .. psRipName .. "|" .. i)
        end
    end
end
