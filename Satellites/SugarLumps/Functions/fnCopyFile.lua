-- |[ ======================================= fnCopyFile ======================================= ]|
--Copies a file with the given filename into the datafiles directory of your mod. Deletes the old
-- file implicitly.
--Can make use of global debug flags, gbCopyShowSuccess, gbCopyShowAttempts.
function fnCopyFile(psFileName)
    
    -- |[Argument Check]|
    if(psFileName == nil) then return end
    
    -- |[Setup]|
    --Pathnames.
    local sStartPath = "Output/" .. psFileName .. ".slf"
    local sFinalPath = "../../Games/AdventureMode/Datafiles/" .. psFileName .. ".slf"
    
    --Debug.
    if(gbCopyShowAttempts == true) then
        io.stderr:write("Checking copy: \"" .. sStartPath .. "\" -> \"" .. sFinalPath .. "\"\n")
    end
    
    -- |[Check Existence]|
    --Verify the file exists by attempting to open it.
    local fHandle = io.open(sStartPath, "r")
    if(fHandle == nil) then
        if(gbCopyShowAttempts == true) then
            io.stderr:write("Failed.\n")
        end
        return
    end
        
    --Close the file.
    io.close(fHandle)
    
    -- |[Copy Across]|
    --Delete the old one and move the new one.
    os.remove(sFinalPath)
    os.rename(sStartPath, sFinalPath)
    
    --Debug.
    if(gbCopyShowAttempts == true) then
        io.stderr:write("Copy successful.\n")
    end
    
    --Alt debug.
    if(gbCopyShowSuccess == true and gbCopyShowAttempts ~= true) then
        io.stderr:write("Detected and copied file: " .. psFileName .. "\n")
    end
end
