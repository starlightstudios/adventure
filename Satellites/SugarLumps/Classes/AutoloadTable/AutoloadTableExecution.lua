-- |[ ================================ AutoloadTable Execution ================================= ]|
--Functions related to the player entering warp mode.

-- |[ =============================== AutoloadTable:fnExecute() ================================ ]|
--Uses all registered entries in the AutoloadTable to build a new autoload file.
function AutoloadTable:fnExecute()

    -- |[Error Checks]|
    --Table should exist.
    if(self.zaGlobalTable == nil) then
        io.write("AutoloadTable:fnExecute() - Error, global table was nil.\n")
        return
    end
    
    --Table should have at least one entry.
    if(#self.zaGlobalTable < 1) then
        io.write("AutoloadTable:fnExecute() - Error, global table has no entries, which is probably not intentional.\n")
        return
    end

    -- |[Execution]|
    --Iterate across the tables.
    for i = 1, #self.zaGlobalTable, 1 do
        
        --Fast-access variables.
        local zaInstructionTable = self.zaGlobalTable[i].zaTable
        local sAutoLoadLumpName  = self.zaGlobalTable[i].sAutoloadLumpName
        
        --Register the lump.
        SLF_RegisterAutoLoadLump(sAutoLoadLumpName)
        
        --Iterate across the table. It contains AutoloadInstruction objects.
        for p = 1, #zaInstructionTable, 1 do
        
            --Fast-access variables.
            local sFileswitch  = zaInstructionTable[p].sDatafileName
            local sFilePath    = zaInstructionTable[p].sDatafilePath
            local sInstruction = zaInstructionTable[p].sLoadInstruction
        
            --If the fileswitch is "Null", don't switch files. This saves a bit of time.
            if(sFileswitch ~= "Null") then
                AutoLoader_Register(sAutoLoadLumpName, "FILESWITCH", sFileswitch)
            end

            --Query the file.
            SLF_QueryAutoLoadInFile(sFilePath, sInstruction)
            
            --Copy all elements from source to destination. 
            local iCount = SLF_GetQueryCount()
            for q = 0, iCount-1, 1 do
                local sName, sPath = SLF_GetQueryEntry(q)
                AutoLoader_Register(sAutoLoadLumpName, sName, sPath)
            end
        end
    end
end
