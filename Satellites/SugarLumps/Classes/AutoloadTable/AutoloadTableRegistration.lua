-- |[ =============================== AutoloadTable Registration =============================== ]|
--Registering entries to the table or to lumps.

-- |[ ========================= AutoloadTable:fnRegisterAutoloadLump() ========================= ]|
--Registers a new AutoloadLump to the global list.
function AutoloadTable:fnRegisterAutoloadLump(psLumpName)

    -- |[Argument Check]|
    if(psLumpName == nil) then return end
    
    -- |[Duplicate Check]|
    for i = 1, #self.zaGlobalTable, 1 do
        if(self.zaGlobalTable[i].sAutoloadLumpName == psLumpName) then
            io.write("AutoloadTable:fnRegisterAutoloadLump() - Warning, lump with name " .. psLumpName .. " already exists.\n")
            return
        end
    end
    
    -- |[New AutoloadLump]|
    local zaNewLump = {}
    zaNewLump.zaTable = {}
    zaNewLump.sAutoloadLumpName = psLumpName
    
    -- |[Register]|
    table.insert(self.zaGlobalTable, zaNewLump)
end

-- |[ ===================== AutoloadTable:fnRegisterAutoloadInstruction() ====================== ]|
--Registers a new AutoloadInstruction to the named AutoloadLump.
function AutoloadTable:fnRegisterAutoloadInstruction(psLumpName, psDatafileName, psDatafilePath, psLoadInstruction)

    -- |[Argument Check]|
    if(psLumpName        == nil) then return end
    if(psDatafileName    == nil) then return end
    if(psDatafilePath    == nil) then return end
    if(psLoadInstruction == nil) then return end

    -- |[Locate Lump]|
    --Locate the lump.
    local iValidSlot = -1
    for i = 1, #self.zaGlobalTable, 1 do
        if(self.zaGlobalTable[i].sAutoloadLumpName == psLumpName) then
            iValidSlot = i
            break
        end
    end
    
    --Not found. Print warning.
    if(iValidSlot == -1) then
        io.write("AutoloadTable:fnRegisterAutoloadInstruction() - Warning, no autoload lump found with name " .. psLumpName .. "\n")
        return
    end
    
    -- |[Duplicate Check]|
    --Get the lump.
    local zAutoloadLump = self.zaGlobalTable[iValidSlot]
    
    --Check if the lump already has that instruction.
    for i = 1, #zAutoloadLump.zaTable, 1 do
        if(zAutoloadLump.zaTable[i].sDatafileName == psDatafileName and zAutoloadLump.zaTable[i].sDatafilePath == psDatafilePath and zAutoloadLump.zaTable[i].sLoadInstruction == psLoadInstruction) then
            io.write("AutoloadTable:fnRegisterAutoloadInstruction() - Warning, duplicate instruction " .. psLoadInstruction .. " found.\n")
            return
        end
    end
    
    -- |[Create, Register]|
    --Create a new entry.
    local zaNewInstruction = {}
    zaNewInstruction.sDatafileName = psDatafileName
    zaNewInstruction.sDatafilePath = psDatafilePath
    zaNewInstruction.sLoadInstruction = psLoadInstruction
    
    --Register.
    table.insert(zAutoloadLump.zaTable, zaNewInstruction)
end