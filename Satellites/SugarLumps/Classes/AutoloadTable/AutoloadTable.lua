-- |[ ================================ AutoloadTable Class File ================================ ]|
--Class file for AutoloadTable. Call this once at initialization.

--Autoloading requires creating a set of entries that each then have a set of instruction in them.
-- This class stores a global table of those entries and instructions, and some functions to 
-- execute the autoload build operation when assembly is completed.

-- |[ =========== Globals/Statics ============ ]|
-- |[ ========== Object Prototypes =========== ]|
-- |[AutoloadLump]|
--Contains an instruction table and a name. The name is used by the engine to execute all the associated instructions.
--zAutoloadLump.zaTable = {}
--zAutoloadLump.sAutoloadLumpName = "Name"

-- |[AutoloadInstruction]|
--Contains the datafile name (for when the engine reads it), the datafile path (for when the compressor reads it), and the load instruction to save.
--zaAutoloadInstruction.sDatafileName = "Datafile Sanya"
--zaAutoloadInstruction.sDatafilePath = "../../Games/AdventureMode/Datafiles/Portraits_Sanya.slf"
--zaAutoloadInstruction.sLoadInstruction = "AutoLoad|Adventure|Emote_Sanya_Bunny"

-- |[ ============ Class Members ============= ]|
-- |[System]|
AutoloadTable = {}
AutoloadTable.__index = AutoloadTable

-- |[Map Variables]|
AutoloadTable.zaGlobalTable = {}                        --Table of all autoload lumps and their associated instructions.

-- |[ ============ Class Methods ============= ]|
--System
function AutoloadTable:new() end

--Registration
function AutoloadTable:fnRegisterAutoloadLump(psLumpName)                                                           end
function AutoloadTable:fnRegisterAutoloadInstruction(psLumpName, psDatafileName, psDatafilePath, psLoadInstruction) end

--Execution
function AutoloadTable:fnExecute() end

-- |[ ========== Class Constructor =========== ]|
--Creates and returns a new object.
function AutoloadTable:new()
    
    -- |[ ========= Setup ========== ]|
    -- |[Argument Check]|
    -- |[Create]|
    local zObject = {}
    setmetatable(zObject, AutoloadTable)
    
    -- |[ ======== Members ========= ]|
    --Local members.
    --Reset local lists so they don't use the global class listings.
    zObject.zaGlobalTable = {}
    
    -- |[ ======= Finish Up ======== ]|
    --Return object.
    return zObject
end

-- |[ ========= Call Implementations ========= ]|
LM_ExecuteScript(fnResolvePath() .. "AutoloadTableExecution.lua")
LM_ExecuteScript(fnResolvePath() .. "AutoloadTableRegistration.lua")
