-- |[ ====================================== Build Images ====================================== ]|
--Lua file which is executed if no other files were specified on the command line.

-- |[Baseline]|
--Enumerations
cbEightDir   = true
cbFourDir    = false
cbIsWide     = true
cbIsNotWide  = false
cbHasRunAnim = true
cbNoRunAnim  = false
cbHasCrouch  = true
cbNoCrouch   = false
cbHasWound   = true
cbNoWound    = false

--Run function scripts.
LM_ExecuteScript("Functions/fnAppendTables.lua")
LM_ExecuteScript("Functions/fnCompressFolder.lua")
LM_ExecuteScript("Functions/fnCloneAuto.lua")
LM_ExecuteScript("Functions/fnCopyFile.lua")
LM_ExecuteScript("Functions/fnExecAll.lua")
LM_ExecuteScript("Functions/fnResolvePath.lua")
LM_ExecuteScript("Functions/fnRipFacingSheet.lua")
LM_ExecuteScript("Functions/fnRipImageAuto.lua")
LM_ExecuteScript("Functions/fnRipImageAutoCr.lua")
LM_ExecuteScript("Functions/fnRipIdleAnimation.lua")
LM_ExecuteScript("Functions/fnRipRunebearer.lua")
LM_ExecuteScript("Functions/fnRipSheet.lua")
LM_ExecuteScript("Functions/fnRipSheetHalf.lua")

--Classes.
LM_ExecuteScript("Classes/AutoloadTable/AutoloadTable.lua")

-- |[Logging Options]|
--For the below, no logging is the default of 0. Flags are bitwise and may be BOR'd together or just added.

--Timestamp checking.
local ciDebug_Subdirs = 1
local ciDebug_FileLogs = 2
TS_SetDebugLevel(0)

--Image lump logging.
local ciDebug_Sizes = 1
local ciDebug_Positions = 2
ImageLump_SetDebugLevel(0)

--Auto-load lump logging.
local ciDebug_AutoloadNone = 0
local ciDebug_AutoloadCompile = 1
AutoLoader_SetDebugLevel(ciDebug_AutoloadNone)

--SLF writing and compilation logging.
local ciDebug_SLFStatus = 1
local ciDebug_SLFLumpNames = 2
local ciDebug_SLFAutoQuery = 4
SLF_SetDebugLevel(ciDebug_SLFStatus)

-- |[Execute]|
--Execute the primary ripping file.
io.stderr:write("\n")
io.stderr:write("============================ Running Ripping Files ============================\n")
LM_ExecuteScript("Data/ZExec.lua")

-- |[ ==================================== Datafile Copying ==================================== ]|
--Copy the datafiles from the Output/ folder into the Chapter's Datafiles/ folder.
--Options for local debug.
gbCopyShowAttempts = false
gbCopyShowSuccess = true

--Formatting:
io.stderr:write("\n")
io.stderr:write("========================== Handling Datafile Copying ==========================\n")

-- |[High-Res List]|
--This list is just literals with no LD cases.
local saList = {"AdvCombat", "CombatAnimations", "ElectrospriteAdventure", "MapAnimations", "Sprites", "UIAdventure", "UIAdvIcons", "UIAdvMenuBase", "UIAdvMenuFieldAbilities", "UIAdvMenuJournal", "UIAdvMenuSkills", 
                "UIControls", "UITextAdventure", "PuzzleBattle", "UIAdvMenuEquipment", "UIAdvMenuStatus", "UIAdvMenuInventory", "UIAdvMenuOptions", "UIAdvMenuVendor", "UIAdvMenuStandard", "UIAdvMenuCampfire",
                "UIAdvMenuFileSelect", "UIAdvMenuQuit", "UIAdvMenuDoctor", "AbyCombat", "UIAdvMenuTrainer"}
for i = 1, #saList, 1 do
    fnCopyFile(saList[i])
end

-- |[Region Markers]|
saList = {"UIRegionNixNedar", "UIRegionQuantirEstate", "UIRegionQuantirHighWastes", "UIRegionCryogenics", "UIRegionEquinox", "UIRegionLRTFacility", "UIRegionSerenityObservatory", "UIRegionAquaticGenetics", 
          "UIRegionBiolabsAlpha", "UIRegionBiolabsBeta", "UIRegionBiolabsDelta", "UIRegionBiolabsEpsilon", "UIRegionBiolabsGamma", "UIRegionBiolabsDatacore", "UIRegionBiolabsHydroponics", "UIRegionBiolabsRaijuRanch", 
          "UIRegionArcaneUniversity", "UIRegionRegulusCitySector15", "UIRegionRegulusCitySector96", "UIRegionTrafalGlacier", "UIRegionArbonnePlains", "UIRegionDimensionalTrap", "UIRegionEvermoonForest",
          "UIRegionStarfieldSwamp", "UIRegionTrannadarTradingPost", "UIRegionNorthwoods", "UIRegionMtSarulente", "UIRegionWestwoods"}
for i = 1, #saList, 1 do
    fnCopyFile(saList[i])
end

-- |[Scenes, Low-Def]|
local sHDPrefix = "AdvScn_"
local sLDPrefix = "AdvScnLD_"
saList = {"CassandraTF", "CH0Major", "CH1Major", "CH2Major", "CH5Major", "ChristineTF", "GalaDress", "MeiRune", "MeiTF"}
for i = 1, #saList, 1 do
    fnCopyFile(sHDPrefix .. saList[i])
    fnCopyFile(sLDPrefix .. saList[i])
end

-- |[Portraits, Low-Def]|
sHDPrefix = "Portraits_"
sLDPrefix = "PortraitsLD_"
saList = {"56", "Aquillia", "Cassandra", "CH0Combat", "CH0Emote", "CH1Combat", "CH1Emote", "CH2Combat", "CH2Emote", "CH5Combat", "CH5Emote", "Christine", "Empress", "Florentina", "Izuna", "JX101", "Maram", 
          "Marriedraunes", "Mei", "Mia", "Miho", "Miso", "Odar", "PDU", "Polaris", "Sammy", "Sanya", "Septima", "Sharelock", "Sophie", "SX399", "Tiffany", "Yukina", "Zeke"}
for i = 1, #saList, 1 do
    fnCopyFile(sHDPrefix .. saList[i])
    fnCopyFile(sLDPrefix .. saList[i])
end

-- |[Overhead Maps]|
sHDPrefix = "Maps_"
sLDPrefix = "MapsLD_"
saList = {"Biolabs", "Ch1_Mausoleum", "CRTNoise", "CryoLower", "CryoMain", "Equinox", "LRTEast", "LRTWest", "MtSarulente", "NixNedar", "Northwoods", "Regulus", "System", "Ch1_Trannadar", "Ch1_TrannadarWest", "Westwoods"}
for i = 1, #saList, 1 do
    fnCopyFile(sHDPrefix .. saList[i])
    fnCopyFile(sLDPrefix .. saList[i])
end

-- |[ ================================== Autoloader Compiling ================================== ]|
io.stderr:write("\n\n")
io.stderr:write("========================== Compiling Meta-Autoloader ==========================\n")
LM_ExecuteScript("Autoloader/Load UI.lua")
LM_ExecuteScript("Autoloader/Chapter 1 Maps.lua")
LM_ExecuteScript("Autoloader/Chapter 1 Portraits.lua")
LM_ExecuteScript("Autoloader/Chapter 1 Sprites.lua")
LM_ExecuteScript("Autoloader/Chapter 2 Maps.lua")
LM_ExecuteScript("Autoloader/Chapter 2 Portraits.lua")
LM_ExecuteScript("Autoloader/Chapter 2 Sprites.lua")
LM_ExecuteScript("Autoloader/Chapter 5 Maps.lua")
fnCopyFile("UI_Autoloader")
fnCopyFile("Maps_CH1_Autoloader")
fnCopyFile("Maps_CH2_Autoloader")
fnCopyFile("Portraits_CH1_Autoloader")
fnCopyFile("Portraits_CH2_Autoloader")
fnCopyFile("Sprites_CH1_Autoloader")
fnCopyFile("Sprites_CH2_Autoloader")
fnCopyFile("Maps_CH5_Autoloader")

--Formatting:
io.stderr:write("\n")
io.stderr:write("============================= Execution Completed =============================\n")

