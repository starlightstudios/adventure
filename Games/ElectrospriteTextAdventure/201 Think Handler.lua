--[ ======================================= Think Handler ======================================= ]
--Called whenever the player uses the "think" command. This lists out the player's current objectives
-- based on what form they are in and the scenario at hand.
--Does not consume a turn.

--[Golem]
--This is the only form available in this simulation.
if(gzTextVar.gzPlayer.sFormState == "Golem") then
    TL_SetProperty("Append", "You think about your goals...")
    TL_SetProperty("Append", "\nYour function is to locate, apprehend, and convert the local human populace. There are five in this area. You will receive 100 points for each conversion.\n\n")
    TL_SetProperty("Create Blocker")
    
    --Variables.
    local iSawProblemWithProgram = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawProblemWithProgram", "N")
    local iGotAmandaHint = VM_GetVar("Root/Variables/Chapter5/Scenes/iGotAmandaHint", "N")
    
    --Starting out.
    if(iSawProblemWithProgram == 0.0) then
        TL_SetProperty("Append", "(Christine: My real goal is to locate the bug in the game and figure out what's causing it. It's supposed to be somewhere near a river on the west side of the map.)")
        TL_SetProperty("Append", "\n(Christine: Though maybe I should just convert all the humans anyway. To be thorough!)\n\n")
    
    --Saw Psue.
    elseif(iSawProblemWithProgram == 1.0) then
    
        --No hint.
        if(iGotAmandaHint == 0.0) then
            TL_SetProperty("Append", "(Christine: My real goal is to locate the bug in the game and figure out what's causing it.)")
            TL_SetProperty("Append", "\n(Christine: I saw a strange NPC and she teleported away. I should go talk with Amanda and see if she has any ideas.)\n\n")
        else
            TL_SetProperty("Append", "(Christine: My real goal is to locate the bug in the game and figure out what's causing it.)")
            TL_SetProperty("Append", "(Christine: Amanda suggested using 'warp debug room' to chase the NPC to the secret room. May as well try it.)\n\n")
        end
    end

--[Other]
else
    TL_SetProperty("Append", "Your mind is completely empty.")

end