-- |[ ======================================== Game Info ======================================= ]|
--This file is executed at program startup to specify that this is a game the engine can load.
local sBasePath       = fnResolvePath()
local sGameName       = "Electrosprite Adventure"
local sGamePath       = "Root/Paths/System/Startup/sElectrospriteAdventurePath" --Must exist in the C++ listing.
local sButtonText     = "Play Electrosprite Adventure"
local sLauncherScript = "YMenuLaunch.lua" --File in the same folder as this file that will launch the program.
local iPriority       = gciAdventure_Set_Start + 1
local iNegativeLen    = -23 --Indicates length of launcher name. ZLaunch.lua is -12.

--Local variables.
local zLaunchPath = sBasePath .. "000 Launcher Script.lua"

-- |[Compatibility Check]|
if(LM_IsGameRegistered("Pandemonium") == false) then
    if(SysPaths.bShowFailedRegistrations) then
        io.write("Not registering Electrosprite Text Adventure. Engine is incompatible.\n")
    end
    return
end

-- |[Activation Check]|
--This game mode must be unlocked before it will appear on the menu.
local sDoesOptionExist = OM_OptionExists("Unlock Electrosprites")
if(sDoesOptionExist == "N") then

    --Get variable.
    local fUnlockElectrosprites = OM_GetOption("Unlock Electrosprites")
    
    --Unlock it.
    if(fUnlockElectrosprites == 1.0) then
        SysPaths:fnAddGameEntry(sGameName, sGamePath, sButtonText, iPriority, sLauncherScript, iNegativeLen)
    
    --Mark it, but don't add it.
    else
        SysPaths:fnAddGameEntry(sGameName, sGamePath, "DO NOT ADD", iPriority, sLauncherScript, iNegativeLen)
    end

--Option did not exist. Mark it, do not add it.
else
    SysPaths:fnAddGameEntry(sGameName, sGamePath, "DO NOT ADD", iPriority, sLauncherScript, iNegativeLen)
end

-- |[Path Adder]|
--Add search path. Happens regardless of whether the game is unlocked.
SysPaths:fnAddSearchPathToEntry(sGameName, zLaunchPath)
