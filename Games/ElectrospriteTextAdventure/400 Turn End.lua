--[ ========================================= Turn End ========================================== ]
--Called whenever the player ends their turn.
gzTextVar.iWorldTurns = gzTextVar.iWorldTurns + 1

--[ ==================================== AI Entity Handler ====================================== ]
--Run across the entities and order them to run their AI updates.
for i = 1, gzTextVar.zEntitiesTotal, 1 do
    
    --If there's no handler, do nothing.
    if(gzTextVar.zEntities[i].sAIHandler == nil) then
        
    --Run it.
    else
        LM_ExecuteScript(gzTextVar.zEntities[i].sAIHandler, i)
    end
end

--Rebuild locality info and visibility info.
fnRebuildEntityVisibility()
fnBuildLocalityInfo()
