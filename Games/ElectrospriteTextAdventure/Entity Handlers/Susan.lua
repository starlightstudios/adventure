-- |[ ========================================= Susan ========================================== ]|
--A very special human! Or is she?

--Argument Listing:
-- 0: sInstruction - What exactly we want out of the room.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs and TL_GetProperty("Is Building Commands") == false) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sInstruction = LM_GetScriptArgument(0)
local i = gzTextVar.iCurrentEntity
if(gzTextVar.zEntities[i] == nil) then return end

-- |[ ==================================== Command Builder ===================================== ]|
--Used for building a list of commands when this entity is selected on the locality window.
-- This ignores script argument requirements. Also note that the arguments are INVALID at this point.
if(TL_GetProperty("Is Building Commands") == true) then
    
    --First, resolve the name of the clicked entity.
    local sEntityName = TL_GetProperty("Command String")
    
    --Common.
    TL_SetProperty("Register Popup Command", "look",   "look "   .. sEntityName)
    TL_SetProperty("Register Popup Command", "talk",   "talk "   .. sEntityName)
    TL_SetProperty("Register Popup Command", "attack", "attack " .. sEntityName)
    if(gzTextVar.zEntities[i].bIsCrippled == true) then
        TL_SetProperty("Register Popup Command", "transform", "transform " .. sEntityName)
    end

    return
end

-- |[ ======================================== Dialogue ======================================== ]|
--Looking at.
if(sInstruction == "look " .. gzTextVar.zEntities[i].sQueryName or sInstruction == "examine " .. gzTextVar.zEntities[i].sQueryName) then
    
    gbHandledInput = true
    TL_SetProperty("Register Image", gzTextVar.zEntities[i].sDisplayName, gzTextVar.zEntities[i].sQueryPicture, 0)
    TL_SetProperty("Append", "A typical human from Pandemonium, but there DFSD[ERROR: ACCESS VIOLATION] 77771FIFTEEN unusual about VARIABLE NOT FOUND: UNIT GENDER. She seems SEXINESS[x], SEXINESS[x+1], and more SEXINESS[x+2] than normal. \n\n")
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Unregister Image")
    
--Attacking.
elseif(sInstruction == "attack " .. gzTextVar.zEntities[i].sQueryName) then
    gbHandledInput = true
    TL_SetProperty("Append", "ERROR: UNABLE TO BOOT COMBAT ROUTINE. REPORT THIS RESULT TO THE CODER RIGHT AWAY. NOT SUPPOSED TO HAPPEN.")
    TL_SetProperty("Create Blocker")

--Talking to.
elseif(sInstruction == "talk " .. gzTextVar.zEntities[i].sQueryName) then
    gbHandledInput = true
    
    --Setup.
    local sMyName = "You"
    local sMyPortrait = gzTextVar.gzPlayer.sQuerySprite
    local sTrName = gzTextVar.zEntities[i].sDisplayName
    local sTrPortrait = gzTextVar.zEntities[i].sQueryPicture
    
    if(gzTextVar.zEntities[i].sLocation ~= "Debug Room") then
        fnDialogueCutscene(sMyName, sMyPortrait,  "You smile and approach " .. sTrName .. ", attempting to appear as unthreatening as possible.")
        fnDialogueCutscene(sMyName, sMyPortrait,  "\n'Hello, human. Do not be afraid, I will improve you shortly.'")
        fnDialogueCutscene(sTrName, sTrPortrait,  "\n'Ha ha ha ha!' she laughs.")
        fnDialogueCutscene(sTrName, sTrPortrait,  "\n'I'm sorry but I'm just not feeling it today. Maybe you should come back later?' she asks. She winks at you.")
        fnDialogueCutscene(sTrName, sTrPortrait,  "\nThe human suddenly vanishes into thin air!")
        fnDialogueCutscene(sTrName, sTrPortrait,  "\n(Christine: She vanished, just like Amanda said! Maybe I should go talk to Amanda to figure out what to do?)")
        fnDialogueFinale()
        gzTextVar.zEntities[i].sLocation = "Debug Room"
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawProblemWithProgram", "N", 1.0)
    else
        local iSawProblemWithProgram = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawProblemWithProgram", "N")
        if(gzTextVar.iGameStage < 2) then
            gzTextVar.iGameStage = 2
            if(iSawProblemWithProgram == 0.0) then
                fnDialogueCutscene(sTrName, sTrPortrait,  "'Uh, hi? How did you know to do that command?'")
                fnDialogueCutscene(sTrName, sTrPortrait,  "\n'You must have some kind of weird future sight...'")
                fnDialogueCutscene(sTrName, sTrPortrait,  "\n'So what do you want?'")
                fnDialogueCutscene(sTrName, sTrPortrait,  "\n'Take your time, but think about what you want from me.'")
            
            else
                fnDialogueCutscene(sTrName, sTrPortrait,  "'Well well, you followed me here. Seems you're different from all those other players.'")
                fnDialogueCutscene(sTrName, sTrPortrait,  "\n'Or you just got lucky. Well, here I am. What do you want?'")
                fnDialogueCutscene(sTrName, sTrPortrait,  "\n'Take your time, but think about what you want from me.'")
            end
            fnDialogueFinale()
        
        elseif(gzTextVar.iGameStage == 2) then
            fnDialogueCutscene(sTrName, sTrPortrait,  "'Yes, yes, we've seen the [talk] command.'")
            fnDialogueCutscene(sTrName, sTrPortrait,  "'Try something different. Maybe more sensual...' she says, rubbing herself gently. You think you can see sparks erupt as she does.")
            fnDialogueFinale()
        end
    end
    
--Transform command.
elseif(sInstruction == "transform " .. gzTextVar.zEntities[i].sQueryName) then
    gbHandledInput = true
    
    --Setup.
    local sMyName = "You"
    local sMyPortrait = gzTextVar.gzPlayer.sQuerySprite
    local sTrName = gzTextVar.zEntities[i].sDisplayName
    local sTrPortrait = gzTextVar.zEntities[i].sQueryPicture
    
    if(gzTextVar.iGameStage < 2) then
        fnDialogueCutscene(sTrName, sTrPortrait,  "'Ugh. I don't feel like playing the robotic servant today. Done it a million times. No thanks.'")
        fnDialogueFinale()
    
    elseif(gzTextVar.iGameStage == 2) then
        fnDialogueCutscene(sTrName, sTrPortrait,  "'Is that really all you want to do to me?'")
        fnDialogueCutscene(sTrName, sTrPortrait,  "'Try something else, or I might get bored.'")
        fnDialogueFinale()
    end
    
--Kiss.
elseif(sInstruction == "kiss " .. gzTextVar.zEntities[i].sQueryName or sInstruction == "kiss") then
    gbHandledInput = true
    
    --Setup.
    local sMyName = "You"
    local sMyPortrait = gzTextVar.gzPlayer.sQuerySprite
    local sTrName = gzTextVar.zEntities[i].sDisplayName
    local sTrPortrait = gzTextVar.zEntities[i].sQueryPicture
    
    if(gzTextVar.iGameStage < 2) then
        fnDialogueCutscene(sTrName, sTrPortrait,  "'You should probably trying using [talk] first, sweetums.'")
        fnDialogueFinale()
    
    elseif(gzTextVar.iGameStage == 2) then
        fnDialogueCutscene(sTrName, sTrPortrait,  "*smooch* 'Oh wow!' the human says, 'You just up and went for it!'")
        fnDialogueCutscene(sTrName, sTrPortrait,  "'If that's the first thing you go for, then you can't be all bad.'")
        fnDialogueCutscene(sTrName, sTrPortrait,  "'Well, I had a lot of fun. Honestly! None of you players have ever done that before. But I better go.'")
        fnDialogueCutscene(sTrName, sTrPortrait,  "The human vanishes!")
        fnDialogueCutscene(sTrName, sTrPortrait,  "(PDU: Christine, I am detecting a major modification of electrical power distribution. Please log out of the game so I may update you on the situation.)")
        fnDialogueFinale()
        VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedToElectrosprite", "N", 1.0)
        gzTextVar.zEntities[i].sLocation = "Nowhere"
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
    end
    
--Hug.
elseif(sInstruction == "hug " .. gzTextVar.zEntities[i].sQueryName or sInstruction == "hug") then
    gbHandledInput = true
    
    --Setup.
    local sMyName = "You"
    local sMyPortrait = gzTextVar.gzPlayer.sQuerySprite
    local sTrName = gzTextVar.zEntities[i].sDisplayName
    local sTrPortrait = gzTextVar.zEntities[i].sQueryPicture
    
    if(gzTextVar.iGameStage < 2) then
        fnDialogueCutscene(sTrName, sTrPortrait,  "'You should probably trying using [talk] first, sweetums.'")
        fnDialogueFinale()
    
    elseif(gzTextVar.iGameStage == 2) then
        fnDialogueCutscene(sTrName, sTrPortrait,  "You give the human a great big hug, careful not to allow your motivators to crush her.")
        fnDialogueCutscene(sTrName, sTrPortrait,  "'Awww, thanks a lot! I know I'm... programmed for this... but...'")
        fnDialogueCutscene(sTrName, sTrPortrait,  "'Nobody has ever used the hug command on me. It's giving me a huge pile of feels...'")
        fnDialogueCutscene(sTrName, sTrPortrait,  "'Agh! Too many feels! Too many! Gotta go!!!'")
        fnDialogueCutscene(sTrName, sTrPortrait,  "The human vanishes!")
        fnDialogueCutscene(sTrName, sTrPortrait,  "(PDU: Christine, I am detecting a major modification of electrical power distribution. Please log out of the game so I may update you on the situation.)")
        fnDialogueFinale()
        VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedToElectrosprite", "N", 1.0)
        gzTextVar.zEntities[i].sLocation = "Nowhere"
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
    end
    
--Touch.
elseif(sInstruction == "touch " .. gzTextVar.zEntities[i].sQueryName or sInstruction == "touch") then
    gbHandledInput = true
    
    --Setup.
    local sMyName = "You"
    local sMyPortrait = gzTextVar.gzPlayer.sQuerySprite
    local sTrName = gzTextVar.zEntities[i].sDisplayName
    local sTrPortrait = gzTextVar.zEntities[i].sQueryPicture
    
    if(gzTextVar.iGameStage < 2) then
        fnDialogueCutscene(sTrName, sTrPortrait,  "'You should probably trying using [talk] first, sweetums.'")
        fnDialogueFinale()
    
    elseif(gzTextVar.iGameStage == 2) then
        fnDialogueCutscene(sTrName, sTrPortrait,  "You give a gentle, non-hostile touch to the human's arm. She smiles.")
        fnDialogueCutscene(sTrName, sTrPortrait,  "'Well, certainly not what I expected, but it's real enough.'")
        fnDialogueCutscene(sTrName, sTrPortrait,  "'I guess you're not like all those other players who would just try to transform me.'")
        fnDialogueCutscene(sTrName, sTrPortrait,  "'I... I should probably go...'")
        fnDialogueCutscene(sTrName, sTrPortrait,  "The human vanishes!")
        fnDialogueCutscene(sTrName, sTrPortrait,  "(PDU: Christine, I am detecting a major modification of electrical power distribution. Please log out of the game so I may update you on the situation.)")
        fnDialogueFinale()
        VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedToElectrosprite", "N", 1.0)
        gzTextVar.zEntities[i].sLocation = "Nowhere"
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
    end
    
--Fondle.
elseif(sInstruction == "fondle " .. gzTextVar.zEntities[i].sQueryName or sInstruction == "fondle") then
    gbHandledInput = true
    
    --Setup.
    local sMyName = "You"
    local sMyPortrait = gzTextVar.gzPlayer.sQuerySprite
    local sTrName = gzTextVar.zEntities[i].sDisplayName
    local sTrPortrait = gzTextVar.zEntities[i].sQueryPicture
    
    if(gzTextVar.iGameStage < 2) then
        fnDialogueCutscene(sTrName, sTrPortrait,  "'You should probably trying using [talk] first, sweetums.'")
        fnDialogueFinale()
    
    elseif(gzTextVar.iGameStage == 2) then
        fnDialogueCutscene(sTrName, sTrPortrait,  "You sidle up next to the human and begin softly massaging her breasts. She seems to enjoy it.")
        fnDialogueCutscene(sTrName, sTrPortrait,  "'W-wow... that feels really good...'")
        fnDialogueCutscene(sTrName, sTrPortrait,  "'I guess you're not like all those other players who would just try to transform me.'")
        fnDialogueCutscene(sTrName, sTrPortrait,  "'... I better go! Bye!'")
        fnDialogueCutscene(sTrName, sTrPortrait,  "The human vanishes!")
        fnDialogueCutscene(sTrName, sTrPortrait,  "(PDU: Christine, I am detecting a major modification of electrical power distribution. Please log out of the game so I may update you on the situation.)")
        fnDialogueFinale()
        VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedToElectrosprite", "N", 1.0)
        gzTextVar.zEntities[i].sLocation = "Nowhere"
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
    end
    
--Smile.
elseif(sInstruction == "smile " .. gzTextVar.zEntities[i].sQueryName or sInstruction == "smile") then
    gbHandledInput = true
    
    --Setup.
    local sMyName = "You"
    local sMyPortrait = gzTextVar.gzPlayer.sQuerySprite
    local sTrName = gzTextVar.zEntities[i].sDisplayName
    local sTrPortrait = gzTextVar.zEntities[i].sQueryPicture
    
    if(gzTextVar.iGameStage < 2) then
        fnDialogueCutscene(sTrName, sTrPortrait,  "'You should probably trying using [talk] first, sweetums.'")
        fnDialogueFinale()
    
    elseif(gzTextVar.iGameStage == 2) then
        fnDialogueCutscene(sTrName, sTrPortrait,  "You smile gently and nonthreateningly at the human. She returns the smile.")
        fnDialogueCutscene(sTrName, sTrPortrait,  "'Oh thank goodness. If this was a random coincidence, you'd probably just try to transform me.'")
        fnDialogueCutscene(sTrName, sTrPortrait,  "'But if it's not... does that mean you're... intelligent too? I better go!'")
        fnDialogueCutscene(sTrName, sTrPortrait,  "The human vanishes!")
        fnDialogueCutscene(sTrName, sTrPortrait,  "(PDU: Christine, I am detecting a major modification of electrical power distribution. Please log out of the game so I may update you on the situation.)")
        fnDialogueFinale()
        VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedToElectrosprite", "N", 1.0)
        gzTextVar.zEntities[i].sLocation = "Nowhere"
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
    end
    
--Lewd. It's a verb now.
elseif(sInstruction == "lewd " .. gzTextVar.zEntities[i].sQueryName or sInstruction == "lewd") then
    gbHandledInput = true
    
    --Setup.
    local sMyName = "You"
    local sMyPortrait = gzTextVar.gzPlayer.sQuerySprite
    local sTrName = gzTextVar.zEntities[i].sDisplayName
    local sTrPortrait = gzTextVar.zEntities[i].sQueryPicture
    
    if(gzTextVar.iGameStage < 2) then
        fnDialogueCutscene(sTrName, sTrPortrait,  "'You should probably trying using [talk] first, sweetums.'")
        fnDialogueFinale()
    
    elseif(gzTextVar.iGameStage == 2) then
        fnDialogueCutscene(sTrName, sTrPortrait,  "You get on one knee and prepare to 'lewd' your target.")
        fnDialogueCutscene(sTrName, sTrPortrait,  "'Ho boy, hun! Slow down there!'")
        fnDialogueCutscene(sTrName, sTrPortrait,  "You smirk. She smiles broadly, and suddenly realizes what you intend to do.")
        fnDialogueCutscene(sTrName, sTrPortrait,  "'Okay, no way is that - none of you have ever done that before!'")
        fnDialogueCutscene(sTrName, sTrPortrait,  "'Oh gee, oh my - I don't know what to feel about this... Gotta go!'")
        fnDialogueCutscene(sTrName, sTrPortrait,  "The human vanishes!")
        fnDialogueCutscene(sTrName, sTrPortrait,  "(PDU: Christine, I am detecting a major modification of electrical power distribution. Please log out of the game so I may update you on the situation.)")
        fnDialogueFinale()
        VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedToElectrosprite", "N", 1.0)
        gzTextVar.zEntities[i].sLocation = "Nowhere"
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
    end
    
--Spy, I ain't gonna - 
elseif(sInstruction == "seduce " .. gzTextVar.zEntities[i].sQueryName or sInstruction == "seduce") then
    gbHandledInput = true
    
    --Setup.
    local sMyName = "You"
    local sMyPortrait = gzTextVar.gzPlayer.sQuerySprite
    local sTrName = gzTextVar.zEntities[i].sDisplayName
    local sTrPortrait = gzTextVar.zEntities[i].sQueryPicture
    
    if(gzTextVar.iGameStage < 2) then
        fnDialogueCutscene(sTrName, sTrPortrait,  "'You should probably trying using [talk] first, sweetums.'")
        fnDialogueFinale()
    
    elseif(gzTextVar.iGameStage == 2) then
        fnDialogueCutscene(sTrName, sTrPortrait,  "You think for a moment. How best to seduce someone you've never met?")
        fnDialogueCutscene(sTrName, sTrPortrait,  "Suddenly, a container of oil-fried pheasant giblets materializes in your hands, and one appears in Susan's. Instantly, you know what to do.")
        fnDialogueCutscene(sTrName, sTrPortrait,  "'Hey there' you say, 'we both got buckets of pheasant. Wanna do it?'")
        fnDialogueCutscene(sTrName, sTrPortrait,  "'Eh, okay' she says somewhat offhandedly.")
        fnDialogueCutscene(sTrName, sTrPortrait,  "Suddenly, she comes to her senses. 'Wait, what am I doing? I can't be so easily seduced!'")
        fnDialogueCutscene(sTrName, sTrPortrait,  "'Unless - unless you really are intelligent... but... I gotta go!'")
        fnDialogueCutscene(sTrName, sTrPortrait,  "The human vanishes!")
        fnDialogueCutscene(sTrName, sTrPortrait,  "(PDU: Christine, I am detecting a major modification of electrical power distribution. Please log out of the game so I may update you on the situation.)")
        fnDialogueFinale()
        VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedToElectrosprite", "N", 1.0)
        gzTextVar.zEntities[i].sLocation = "Nowhere"
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
    end
    
--Fingerguns!
elseif(sInstruction == "flirt " .. gzTextVar.zEntities[i].sQueryName or sInstruction == "flirt") then
    gbHandledInput = true
    
    --Setup.
    local sMyName = "You"
    local sMyPortrait = gzTextVar.gzPlayer.sQuerySprite
    local sTrName = gzTextVar.zEntities[i].sDisplayName
    local sTrPortrait = gzTextVar.zEntities[i].sQueryPicture
    
    if(gzTextVar.iGameStage < 2) then
        fnDialogueCutscene(sTrName, sTrPortrait,  "'You should probably trying using [talk] first, sweetums.'")
        fnDialogueFinale()
    
    elseif(gzTextVar.iGameStage == 2) then
        fnDialogueCutscene(sTrName, sTrPortrait,  "You place your hand on a nearby wall and adopt a wry smile.")
        fnDialogueCutscene(sTrName, sTrPortrait,  "'Baby, if I were in charge of the alphabet, I'd put you and I together.'")
        fnDialogueCutscene(sTrName, sTrPortrait,  "'You mean u and i?', she asks.")
        fnDialogueCutscene(sTrName, sTrPortrait,  "'Yes, that's what I meant. This is text, sorry.'")
        fnDialogueCutscene(sTrName, sTrPortrait,  "'Wait it's text? What are you - I knew it!', she says, stammering.")
        fnDialogueCutscene(sTrName, sTrPortrait,  "'I gotta go!'")
        fnDialogueCutscene(sTrName, sTrPortrait,  "The human vanishes!")
        fnDialogueCutscene(sTrName, sTrPortrait,  "(PDU: Christine, I am detecting a major modification of electrical power distribution. Please log out of the game so I may update you on the situation.)")
        fnDialogueFinale()
        VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedToElectrosprite", "N", 1.0)
        gzTextVar.zEntities[i].sLocation = "Nowhere"
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
    end
    
--Very direct.
elseif(sInstruction == "fuck " .. gzTextVar.zEntities[i].sQueryName or sInstruction == "fuck") then
    gbHandledInput = true
    
    --Setup.
    local sMyName = "You"
    local sMyPortrait = gzTextVar.gzPlayer.sQuerySprite
    local sTrName = gzTextVar.zEntities[i].sDisplayName
    local sTrPortrait = gzTextVar.zEntities[i].sQueryPicture
    
    if(gzTextVar.iGameStage < 2) then
        fnDialogueCutscene(sTrName, sTrPortrait,  "'You should probably trying using [talk] first, sweetums.'")
        fnDialogueFinale()
    
    elseif(gzTextVar.iGameStage == 2) then
        fnDialogueCutscene(sTrName, sTrPortrait,  "You adopt a firm look. The human takes a step back, confused.")
        fnDialogueCutscene(sTrName, sTrPortrait,  "You prepare two fingers and thrust low, penetrating the human. Your cold metal hands find warm flesh.")
        fnDialogueCutscene(sTrName, sTrPortrait,  "'AAaaannnghhh!' the human groans. Your masterfully coded programs begin working her.")
        fnDialogueCutscene(sTrName, sTrPortrait,  "After minutes of working, she has orgasmed. A human would normally need to rest, but your mechanical body has no need for it.")
        fnDialogueCutscene(sTrName, sTrPortrait,  "You bring her to her second orgasm, then a third. Cold, emotionless, unyielding, the orgasms you give crash like waves against an eroding shore.")
        fnDialogueCutscene(sTrName, sTrPortrait,  "'Im-impossible... You couldn't do that... Unless you're really intelligent... Unngghhh...'")
        fnDialogueCutscene(sTrName, sTrPortrait,  "'I need to think about this...'")
        fnDialogueCutscene(sTrName, sTrPortrait,  "The human vanishes!")
        fnDialogueCutscene(sTrName, sTrPortrait,  "(PDU: Christine, I am detecting a major modification of electrical power distribution. Please log out of the game so I may update you on the situation.)")
        fnDialogueFinale()
        VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedToElectrosprite", "N", 1.0)
        gzTextVar.zEntities[i].sLocation = "Nowhere"
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
    end
    
end