-- |[ ================================ Standard Victory Handler ================================ ]|
--When called at the end of combat, all hostile entities in the same room as the player are defeated
-- and removed from play. This is not used if a transformation is going to occur.

--Get the room the player is in.
--local iRoom = fnGetRoomIndex(gzTextVar.gzPlayer.sLocation)
--if(iRoom == -1) then return end

--Setup.
gzTextVar.bIsCheckingHostility = true

--Iterate across all entities and check for location match and hostility match.
local bRemoved = true
while(bRemoved) do
    
    --Reset.
    bRemoved = false
    
    --Iterate.
    for i = 1, gzTextVar.zEntitiesTotal, 1 do
        
        --Entity is in the same location.
        if(gzTextVar.zEntities[i].sLocation == gzTextVar.gzPlayer.sLocation) then
            
            --Check if the entity passes the hostility test. If so, it gets removed.
            gzTextVar.bLastHostilityCheck = false
            LM_ExecuteScript(gzTextVar.zEntities[i].sAIHandler, i)
            
            if(gzTextVar.bLastHostilityCheck == true) then
                bRemoved = true
                gzTextVar.zEntities[i].bIsCrippled = true
                break
            end
        end
    end
end

--Store the player's HP.
TL_SetProperty("Reresolve Stats After Combat")
gzTextVar.gzPlayer.iHP = TL_GetProperty("Player HP")
TL_SetProperty("Set Player Stats", gzTextVar.gzPlayer.iHP, gzTextVar.gzPlayer.iHPMax, gzTextVar.gzPlayer.iAtp, gzTextVar.gzPlayer.iDef, gzTextVar.gzPlayer.sQuerySprite)

--Clean.
gzTextVar.bIsCheckingHostility = false
fnRebuildEntityVisibility()
fnBuildLocalityInfo()