-- |[ ======================================= Boot Fonts ======================================= ]|
--Boots fonts used by Electrosprite Adventure
if(gbBootedElectrospriteFonts == true) then return end
gbBootedElectrospriteFonts = true

-- |[Setup]|
--Paths.
local sEngineFontPath = "Data/Scripts/Fonts/"
local sLocalFontPath  = fnResolvePath()

--Special Flags
local ciFontNoFlags = 0
local ciFontNearest  = Font_GetProperty("Constant Precache With Nearest")
local ciFontEdge     = Font_GetProperty("Constant Precache With Edge")
local ciFontDownfade = Font_GetProperty("Constant Precache With Downfade")
local ciFontSpecialS = Font_GetProperty("Constant Precache With Special S")

-- |[ ===================================== Font Registry ====================================== ]|
--Segoe
Font_Register("Segoe 20", sLocalFontPath .. "segoeui.ttf", sLocalFontPath .. "Segoe20_Kerning.lua", 20, ciFontNoFlags)
Font_Register("Segoe 30", sLocalFontPath .. "segoeui.ttf", sLocalFontPath .. "Segoe30_Kerning.lua", 30, ciFontNoFlags)

--Trigger 28
Font_SetProperty("Downfade", 0.99, 0.60)
Font_SetProperty("Outline Width", 1)
Font_Register("Trigger 28 DFO", sLocalFontPath .. "TriggerModified.ttf", sLocalFontPath .. "Trigger28_Kerning.lua", 28, ciFontNearest + ciFontEdge + ciFontDownfade)

Font_SetProperty("Outline Width", 1)
Font_Register("Trigger 28 O", sLocalFontPath .. "TriggerModified.ttf", sLocalFontPath .. "Trigger28_Kerning.lua", 28, ciFontNearest + ciFontEdge)

-- |[ ======================================== Aliases ========================================= ]|
--Segoe 20
Font_SetProperty("Add Alias", "Segoe 20", "Text Level Medium")
Font_SetProperty("Add Alias", "Segoe 20", "Typing Combat Medium")

--Segoe 30
Font_SetProperty("Add Alias", "Segoe 30", "Text Level Large")

--Sanchez 35 (Engine Font)
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Typing Combat Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Text Lev Options Header")

--Trigger 28
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Typing Combat Command")

--Trigger 28 NDF
Font_SetProperty("Add Alias", "Trigger 28 O", "Text Lev Options Main")
