--[ ====================================== Map Construction ===================================== ]
--Build the map. Place positions and connectivity.
gzTextVar.zRoomList = {}
gzTextVar.iRoomsTotal = 0

--Run this script to build the standard room handler.
LM_ExecuteScript(gzTextVar.sRoomHandlers .. "ZStandardRoomHandler.lua")

--[Adder Function]
--Adds a new room to the list.
fnAddRoom = function(sRoomName, fRoomX, fRoomY, fRoomZ, bConnectN, bConnectS, bConnectE, bConnectW, bConnectU, bConnectD)
    
    --If the file doesn't exist, we create it.
    local fCheckFile = io.open(gzTextVar.sRoomHandlers .. sRoomName .. ".lua", "r")
    if(fCheckFile == nil) then
        file = io.open(gzTextVar.sRoomHandlers .. sRoomName .. ".lua", "w")
        file:write(gsRoomHandler)
        io.close(file)
    end
    
    --Argument Check
    if(sRoomName == nil) then return end
    if(fRoomX == nil) then return end
    if(fRoomY == nil) then return end
    if(fRoomZ == nil) then return end
    if(bConnectN == nil) then return end
    if(bConnectS == nil) then return end
    if(bConnectE == nil) then return end
    if(bConnectW == nil) then return end
    if(bConnectU == nil) then return end
    if(bConnectD == nil) then return end
    
    --Increment.
    gzTextVar.iRoomsTotal = gzTextVar.iRoomsTotal + 1
    
    --Set.
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal] = {}
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].sName = sRoomName
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].sDescription = "Room has no description"
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].fRoomX = fRoomX * gzTextVar.fMapDistance
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].fRoomY = fRoomY * gzTextVar.fMapDistance
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].fRoomZ = fRoomZ * gzTextVar.fMapDistance
    
    --Default movement connections.
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].sMoveN = bConnectN
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].sMoveS = bConnectS
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].sMoveE = bConnectE
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].sMoveW = bConnectW
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].sMoveU = bConnectU
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].sMoveD = bConnectD
    
    --Integer reps. All default to -1.
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].iMoveN = -1
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].iMoveS = -1
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].iMoveE = -1
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].iMoveW = -1
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].iMoveU = -1
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].iMoveD = -1
    
    --Object Listing
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].iObjectsTotal = 0
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].zObjects = {}
    
    --Path Listing
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].zPathListing = {}
end

--[Connector Function]
--Connects two rooms on the room list. Uses a non-optimized search function. The connection is A to B,
-- and the reverse connection auto-applies B to A.
--This uses string searches. It may be easier to read in some cases.
fnConnectRooms = function(sRoomA, sRoomB, sDirectionCode)
    
    --Argument Check
    if(sRoomA == nil) then return end
    if(sRoomB == nil) then return end
    if(sDirectionCode == nil) then return end
    
    --Locate rooms A and B.
    local iIndexA = -1
    local iIndexB = -1
    for i = 1, gzTextVar.iRoomsTotal, 1 do
        if(gzTextVar.zRoomList[i].sName == sRoomA) then
            iIndexA = i
            if(iIndexB ~= -1) then break end
        elseif(gzTextVar.zRoomList[i].sName == sRoomB) then
            iIndexB = i
            if(iIndexA ~= -1) then break end
        end
    end
    
    --Iterate.
    for i = 1, gzTextVar.iRoomsTotal, 1 do
        
        --If this is room A:
        if(gzTextVar.zRoomList[i].sName == sRoomA) then
            if(sDirectionCode == "N") then
                gzTextVar.zRoomList[i].sMoveN = sRoomB
                gzTextVar.zRoomList[i].iMoveN = iIndexB
            elseif(sDirectionCode == "S") then
                gzTextVar.zRoomList[i].sMoveS = sRoomB
                gzTextVar.zRoomList[i].iMoveS = iIndexB
            elseif(sDirectionCode == "E") then
                gzTextVar.zRoomList[i].sMoveE = sRoomB
                gzTextVar.zRoomList[i].iMoveE = iIndexB
            elseif(sDirectionCode == "W") then
                gzTextVar.zRoomList[i].sMoveW = sRoomB
                gzTextVar.zRoomList[i].iMoveW = iIndexB
            elseif(sDirectionCode == "U") then
                gzTextVar.zRoomList[i].sMoveU = sRoomB
                gzTextVar.zRoomList[i].iMoveU = iIndexB
            elseif(sDirectionCode == "D") then
                gzTextVar.zRoomList[i].sMoveD = sRoomB
                gzTextVar.zRoomList[i].iMoveD = iIndexB
            end
        end
    
        --If this is room B:
        if(gzTextVar.zRoomList[i].sName == sRoomB) then
            if(sDirectionCode == "N") then
                gzTextVar.zRoomList[i].sMoveS = sRoomA
                gzTextVar.zRoomList[i].iMoveS = iIndexA
            elseif(sDirectionCode == "S") then
                gzTextVar.zRoomList[i].sMoveN = sRoomA
                gzTextVar.zRoomList[i].iMoveN = iIndexA
            elseif(sDirectionCode == "E") then
                gzTextVar.zRoomList[i].sMoveW = sRoomA
                gzTextVar.zRoomList[i].iMoveW = iIndexA
            elseif(sDirectionCode == "W") then
                gzTextVar.zRoomList[i].sMoveE = sRoomA
                gzTextVar.zRoomList[i].iMoveE = iIndexA
            elseif(sDirectionCode == "U") then
                gzTextVar.zRoomList[i].sMoveD = sRoomA
                gzTextVar.zRoomList[i].iMoveD = iIndexA
            elseif(sDirectionCode == "D") then
                gzTextVar.zRoomList[i].sMoveU = sRoomA
                gzTextVar.zRoomList[i].iMoveU = iIndexA
            end
        end
    end
    
end

--This function attempts to connect rooms based on their cardinal directions. Easier to use if you
-- don't mind geometry, and it maintains internal consistency.
fnConnectRoomsByDirection = function(sRoomName, bNorth, bSouth, bEast, bWest, bUp, bDown)
    
    --Argument Check
    if(sRoomName == nil) then return end
    if(bNorth == nil) then return end
    if(bSouth == nil) then return end
    if(bEast == nil) then return end
    if(bWest == nil) then return end
    if(bUp == nil) then return end
    if(bDown == nil) then return end
    
    --Iterate.
    local i = -1
    for p = 1, gzTextVar.iRoomsTotal, 1 do
        
        --If this is our room, store it.
        if(gzTextVar.zRoomList[p].sName == sRoomName) then
            i = p
            break
        end
    end
    
    --Room not found.
    if(i == -1) then return end
    
    --Clear this room's connectivity variables back to strings.
    gzTextVar.zRoomList[i].sMoveN = "Null"
    gzTextVar.zRoomList[i].sMoveS = "Null"
    gzTextVar.zRoomList[i].sMoveE = "Null"
    gzTextVar.zRoomList[i].sMoveW = "Null"
    gzTextVar.zRoomList[i].sMoveU = "Null"
    gzTextVar.zRoomList[i].sMoveD = "Null"
    
    --Begin iterating looking for case matches.
    for p = 1, gzTextVar.iRoomsTotal, 1 do
        
        --North.
        if(bNorth and gzTextVar.zRoomList[p].fRoomY == gzTextVar.zRoomList[i].fRoomY - gzTextVar.fMapDistance and gzTextVar.zRoomList[i].fRoomX == gzTextVar.zRoomList[p].fRoomX) then
            gzTextVar.zRoomList[i].sMoveN = gzTextVar.zRoomList[p].sName
            gzTextVar.zRoomList[p].sMoveS = gzTextVar.zRoomList[i].sName
            
            gzTextVar.zRoomList[i].iMoveN = p
            gzTextVar.zRoomList[p].iMoveS = i
        end
        
        --South.
        if(bSouth and gzTextVar.zRoomList[p].fRoomY == gzTextVar.zRoomList[i].fRoomY + gzTextVar.fMapDistance and gzTextVar.zRoomList[i].fRoomX == gzTextVar.zRoomList[p].fRoomX) then
            gzTextVar.zRoomList[i].sMoveS = gzTextVar.zRoomList[p].sName
            gzTextVar.zRoomList[p].sMoveN = gzTextVar.zRoomList[i].sName
            
            gzTextVar.zRoomList[i].iMoveS = p
            gzTextVar.zRoomList[p].iMoveN = i
        end
        
        --West.
        if(bWest and gzTextVar.zRoomList[p].fRoomX == gzTextVar.zRoomList[i].fRoomX - gzTextVar.fMapDistance and gzTextVar.zRoomList[i].fRoomY == gzTextVar.zRoomList[p].fRoomY) then
            gzTextVar.zRoomList[i].sMoveW = gzTextVar.zRoomList[p].sName
            gzTextVar.zRoomList[p].sMoveE = gzTextVar.zRoomList[i].sName
            
            gzTextVar.zRoomList[i].iMoveW = p
            gzTextVar.zRoomList[p].iMoveE = i
        end
        
        --East.
        if(bEast and gzTextVar.zRoomList[p].fRoomX == gzTextVar.zRoomList[i].fRoomX + gzTextVar.fMapDistance and gzTextVar.zRoomList[i].fRoomY == gzTextVar.zRoomList[p].fRoomY) then
            gzTextVar.zRoomList[i].sMoveE = gzTextVar.zRoomList[p].sName
            gzTextVar.zRoomList[p].sMoveW = gzTextVar.zRoomList[i].sName
            
            gzTextVar.zRoomList[i].iMoveE = p
            gzTextVar.zRoomList[p].iMoveW = i
        end
        
    end
end

--[ ======================================== Room Listing ======================================= ]
--Headquarters. Humans go here for transformation and the player can restore HP here.
fnAddRoom("Headquarters",        0,  0, 0, true, false, false, false, false, false)

--Dusty path.
fnAddRoom("Dusty Path W", -1, -1, 0, false, false, true, true, false, false)
fnAddRoom("Dusty Path C",  0, -1, 0, false,  true, true, true, false, false)
fnAddRoom("Dusty Path E",  1, -1, 0, false, false, true, true, false, false)

--Farm Road 
fnAddRoom("Farm Road S", 2, -1, 0,  true, false,  true,  true, false, false)
fnAddRoom("Farm Road C", 2, -2, 0,  true,  true, false, false, false, false)
fnAddRoom("Farm Road N", 2, -3, 0, false,  true,  true, false, false, false)
fnAddRoom("Farmhouse",   3, -3, 0, false,  true, false,  true, false, false)

--Farm Field
fnAddRoom("Farm Field NW", 3, -1, 0, false,  true,  true,  true, false, false)
fnAddRoom("Farm Field NC", 4, -1, 0, false,  true,  true,  true, false, false)
fnAddRoom("Farm Field NE", 5, -1, 0, false,  true, false,  true, false, false)
fnAddRoom("Farm Field CW", 3,  0, 0,  true,  true,  true, false, false, false)
fnAddRoom("Farm Field CC", 4,  0, 0,  true,  true,  true,  true, false, false)
fnAddRoom("Farm Field CE", 5,  0, 0,  true,  true, false,  true, false, false)
fnAddRoom("Farm Field SW", 3,  1, 0,  true, false,  true, false, false, false)
fnAddRoom("Farm Field SC", 4,  1, 0,  true, false,  true,  true, false, false)
fnAddRoom("Farm Field SE", 5,  1, 0,  true, false, false,  true, false, false)

--Riverside Path
fnAddRoom("Riverside Path N", -2, -2, 0,  true,  true, false,  true, false, false)
fnAddRoom("Riverside Path C", -2, -1, 0,  true,  true,  true, false, false, false)
fnAddRoom("Riverside Path S", -2,  0, 0,  true,  true, false, false, false, false)

--Rocky Path
fnAddRoom("Rocky Path N", -2, -4, 0, false,  true, false, false, false, false)
fnAddRoom("Rocky Path S", -2, -3, 0,  true,  true, false, false, false, false)

--Hill Path
fnAddRoom("Hill Path N",   -2, 1, 0,  true,  true, false, false, false, false)
fnAddRoom("Hill Path C",   -2, 2, 0,  true,  true,  true, false, false, false)
fnAddRoom("Hill Path S",   -2, 3, 0,  true, false, false,  true, false, false)
fnAddRoom("Hill Cabin",    -3, 3, 0, false, false,  true, false, false, false)
fnAddRoom("Cave Entrance", -1, 2, 0, false, false,  true,  true, false, false)
fnAddRoom("Cave Rear A",    0, 2, 0, false,  true, false,  true, false, false)
fnAddRoom("Cave Rear B",    0, 3, 0,  true, false, false, false, false, false)

--The Bridge
fnAddRoom("Bridge", -3, -2, 0, false, false,  true,  true, false, false)

--Forest Path
fnAddRoom("Forest Path E", -4, -2, 0,  true,  true,  true,  true, false, false)
fnAddRoom("Forest Path C", -5, -2, 0, false, false,  true,  true, false, false)
fnAddRoom("Forest Path W", -6, -2, 0,  true,  true,  true, false, false, false)

--Deep Forest
fnAddRoom("Deep Forest NW", -6, -1, 0,  true,  true, false, false, false, false)
fnAddRoom("Deep Forest NE", -4, -1, 0,  true,  true, false, false, false, false)
fnAddRoom("Deep Forest W",  -6,  0, 0,  true, false,  true, false, false, false)
fnAddRoom("Deep Forest C",  -5,  0, 0, false, false,  true,  true, false, false)
fnAddRoom("Deep Forest E",  -4,  0, 0,  true,  true, false,  true, false, false)
fnAddRoom("Forest Cabin",   -4,  1, 0,  true, false, false, false, false, false)

--Shrubland
fnAddRoom("Shrubland W",  -6, -3, 0,  true,  true, false, false, false, false)
fnAddRoom("Shrubland E",  -4, -3, 0,  true,  true, false, false, false, false)
fnAddRoom("Shrubland NW", -6, -4, 0, false,  true,  true, false, false, false)
fnAddRoom("Shrubland NC", -5, -4, 0, false, false,  true,  true, false, false)
fnAddRoom("Shrubland NE", -4, -4, 0, false,  true, false,  true, false, false)

--Debug Room
fnAddRoom("Debug Room", 2, 3, 0, false, false, false, false, false, false)

--[ ===================================== Room Descriptions ===================================== ]
--Builds the room descriptions which are used when examining a room. Note that there is no fixed 
-- reason why a room could not use another description, this is simply the default which is used
-- 99% of the time. You can edit the room's caller script if you want to add if/else cases.
--The descriptions are build in an array then crossloaded. This makes it easier to insert a 
-- description if the order of the rooms changes. They should be parallel to the fnAddRoom() order.
local saDescriptionList = {}
local iDescriptionsTotal = 0
local fnAddDescription = function(sDescription)
    iDescriptionsTotal = iDescriptionsTotal + 1
    saDescriptionList[iDescriptionsTotal] = sDescription
end

--[Listing]
--Headquarters. Humans go here for transformation and the player can restore HP here.
fnAddDescription("A temporary headquarters for the abduction team, hastily assembled and fortified. A conversion chamber is here for cored humans to use. You can restore your health here with the [recharge] command.")

--Dusty path.
fnAddDescription("A path that passes your headquarters. There is a river to the west and beyond it, a forest.")
fnAddDescription("A path that passes your headquarters, which is to the south. It is very cleverly camouflaged and a human would never notice it with their simple eyes.")
fnAddDescription("A path that passes your headquarters. A farm is to the east.")

--Farm Road 
fnAddDescription("A road connecting a quaint, rustic farm. A farmhouse is to the north, while access to a grain field is to the east.")
fnAddDescription("A road connecting a quaint, rustic farm. A farmhouse is to the north.")
fnAddDescription("A road connecting a quaint, rustic farm. There is a farmhouse to the east with its door open and unbarred.")
fnAddDescription("A delightfully primitive farmhouse. There are beds, cooking utensils, and a fire pit. Fire pits were used to prevent cooking fires from burning the house down. Humans love fire.")

--Farm Field
fnAddDescription("A field of golden yellow grain. Despite the lack of pest control and genetic selection, the grain is almost looking good enough to process into oil and ingest!")
fnAddDescription("A field of golden yellow grain. Despite the lack of pest control and genetic selection, the grain is almost looking good enough to process into oil and ingest!")
fnAddDescription("A field of golden yellow grain. Despite the lack of pest control and genetic selection, the grain is almost looking good enough to process into oil and ingest!")
fnAddDescription("A field of golden yellow grain. Despite the lack of pest control and genetic selection, the grain is almost looking good enough to process into oil and ingest!")
fnAddDescription("A field of golden yellow grain. Despite the lack of pest control and genetic selection, the grain is almost looking good enough to process into oil and ingest!")
fnAddDescription("A field of golden yellow grain. Despite the lack of pest control and genetic selection, the grain is almost looking good enough to process into oil and ingest!")
fnAddDescription("A field of golden yellow grain. Despite the lack of pest control and genetic selection, the grain is almost looking good enough to process into oil and ingest!")
fnAddDescription("A field of golden yellow grain. Despite the lack of pest control and genetic selection, the grain is almost looking good enough to process into oil and ingest!")
fnAddDescription("A field of golden yellow grain. Despite the lack of pest control and genetic selection, the grain is almost looking good enough to process into oil and ingest!")

--Riverside Path
fnAddDescription("A path that edges a bubbling river. You can see a forest on its west bank. A rocky path is to the north, while a bridge crosses the river to the west.")
fnAddDescription("A path that edges a bubbling river. You can see a forest on its west bank. The path to your headquarters is to the east.")
fnAddDescription("A path that edges a bubbling river. You can see a forest on its west bank. A path into the hills is to your south.")

--Rocky Path
fnAddDescription("The uneven rocks here would likely prevent a human going any further, so you see no need to. There is a path to the south.")
fnAddDescription("A path composed of uneven rocks. Human footwear likely has difficulty traversing it, but you are undeterred. There is a river to the southwest.")

--Hill Path
fnAddDescription("The terrain around you has become unlevel as you enter the foothills. There is a river to your northwest, and a cabin to the south.")
fnAddDescription("You are now within the hills proper. There is a cave to your east, and a cabin to the south.")
fnAddDescription("You are standing outside a cabin constructed in the hills. It is made of crude stone bricks and wood with a thatched roof. The door is open and unbarred.")
fnAddDescription("You are within a crude stone dwelling with wood supports and a thatched roof. All around you are stone knives, tools, and animal skins. The occupants are likely hunter-gatherers with excellent physical traits.")
fnAddDescription("You are in the entrance to a cave. Simplistic painted drawings adorn the walls, mostly of animals the humans likely hunt. The exit is to the west.")
fnAddDescription("Deeper into the cave, it has become so dark that your optic sensors have switched to high-priority mode. Ashes can be seen from human torches. The exit is to the west.")
fnAddDescription("You are deep into the hill cave, and markings of human rituals are everywhere. The humans likely require tests of bravery to enter adulthood here.")

--The Bridge
fnAddDescription("This bridge joins the east and west sides of the fast-flowing river beneath you. It has been constructed with engineering principles even Regulus City has not transcended.")

--Forest Path
fnAddDescription("A meandering, rough path surrounded by thick forest. There is a bridge to the east.")
fnAddDescription("A meandering, rough path surrounded by thick forest. Stepping off the path would be difficult due to the thick brush.")
fnAddDescription("A meandering, rough path surrounded by thick forest. Stepping off the path would be difficult due to the thick brush.")

--Deep Forest
fnAddDescription("The forest is so thick that the sun is obscured almost completely. Little can be seen in any direction.")
fnAddDescription("The forest is so thick that the sun is obscured almost completely. Little can be seen in any direction.")
fnAddDescription("The forest is so thick that the sun is obscured almost completely. Little can be seen in any direction.")
fnAddDescription("The forest is so thick that the sun is obscured almost completely. Little can be seen in any direction.")
fnAddDescription("The forest is so thick that the sun is obscured almost completely. Little can be seen in any direction.")
fnAddDescription("Tucked deep within the forest is a cabin. It contains the usual trappings of simple forest life, such as a fireplace, beds, and mounted animal carcasses to demonstrate the abilities of the hunter.")

--Shrubland
fnAddDescription("Sparsely forested shrubland, with long grasses and open fields with scatterings of trees. The paths are rough and gravelly.")
fnAddDescription("Sparsely forested shrubland, with long grasses and open fields with scatterings of trees. The paths are rough and gravelly.")
fnAddDescription("Sparsely forested shrubland, with long grasses and open fields with scatterings of trees. The paths are rough and gravelly.")
fnAddDescription("Sparsely forested shrubland, with long grasses and open fields with scatterings of trees. The paths are rough and gravelly.")
fnAddDescription("Sparsely forested shrubland, with long grasses and open fields with scatterings of trees. The paths are rough and gravelly.")

--Debug room.
fnAddDescription("A room made for the devs, but if you're reading this, you already know that. Cheater. You can use [warp headquarters] to escape.")

--[Crossload]
--Once descriptions are built, load them into the holder array.
for i = 1, iDescriptionsTotal, 1 do
    gzTextVar.zRoomList[i].sDescription = saDescriptionList[i]
end

--[ ====================================== Upload Everything ==================================== ]
--Build connectivity listing.
for i = 1, gzTextVar.iRoomsTotal, 1 do
    fnConnectRoomsByDirection(gzTextVar.zRoomList[i].sName, gzTextVar.zRoomList[i].sMoveN, gzTextVar.zRoomList[i].sMoveS, gzTextVar.zRoomList[i].sMoveE, gzTextVar.zRoomList[i].sMoveW, gzTextVar.zRoomList[i].sMoveU, gzTextVar.zRoomList[i].sMoveD)
end

--Upload this to the automapper
for i = 1, gzTextVar.iRoomsTotal, 1 do
    TL_SetProperty("Register Room", gzTextVar.zRoomList[i].sName, gzTextVar.zRoomList[i].fRoomX, gzTextVar.zRoomList[i].fRoomY, gzTextVar.zRoomList[i].fRoomZ)
end

--After all the rooms are uploaded, upload the connections.
for i = 1, gzTextVar.iRoomsTotal, 1 do
    if(gzTextVar.zRoomList[i].sMoveN ~= "Null") then
        TL_SetProperty("Register Connection", gzTextVar.zRoomList[i].sName, gzTextVar.zRoomList[i].sMoveN)
    end
    if(gzTextVar.zRoomList[i].sMoveS ~= "Null") then
        TL_SetProperty("Register Connection", gzTextVar.zRoomList[i].sName, gzTextVar.zRoomList[i].sMoveS)
    end
    if(gzTextVar.zRoomList[i].sMoveE ~= "Null") then
        TL_SetProperty("Register Connection", gzTextVar.zRoomList[i].sName, gzTextVar.zRoomList[i].sMoveE)
    end
    if(gzTextVar.zRoomList[i].sMoveW ~= "Null") then
        TL_SetProperty("Register Connection", gzTextVar.zRoomList[i].sName, gzTextVar.zRoomList[i].sMoveW)
    end
    if(gzTextVar.zRoomList[i].sMoveU ~= "Null") then
        TL_SetProperty("Register Connection", gzTextVar.zRoomList[i].sName, gzTextVar.zRoomList[i].sMoveU)
    end
    if(gzTextVar.zRoomList[i].sMoveD ~= "Null") then
        TL_SetProperty("Register Connection", gzTextVar.zRoomList[i].sName, gzTextVar.zRoomList[i].sMoveD)
    end
end

--[Manual Connections]
--None here

--[ ======================================== Path Building ====================================== ]
--Every room on the map needs to know the path to every other room. This may take a while to build.
for i = 1, gzTextVar.iRoomsTotal, 1 do
    
    --List of rooms adjacent on the pulse.
    local zaAdjacentList = {}
    
    --Iterate across all other rooms. Set them to empty strings to initialize them.
    for p = 1, gzTextVar.iRoomsTotal, 1 do
        gzTextVar.zRoomList[i].zPathListing[p] = ""
    end
    
    --The first adjacency case is this one.
    local iAdjacencyCursor = 1
    zaAdjacentList[iAdjacencyCursor] = {}
    zaAdjacentList[iAdjacencyCursor].iIndex = i
    zaAdjacentList[iAdjacencyCursor].sMoves = ""
    
    --Begin pathing pulse.
    --io.write("Running path pulse.\n")
    while(iAdjacencyCursor <= #zaAdjacentList) do
        
        --Fast-access variables.
        local u = zaAdjacentList[iAdjacencyCursor].iIndex
        local sMoveString = zaAdjacentList[iAdjacencyCursor].sMoves
        local iArrayMax = #zaAdjacentList
        
        --io.write(" Operate " .. iAdjacencyCursor .. " - " .. zaAdjacentList[iAdjacencyCursor].sMoves .. "\n")
        
        --Check the north connection.
        if(gzTextVar.zRoomList[u].iMoveN ~= -1) then
            
            --Fast-access.
            local q = gzTextVar.zRoomList[u].iMoveN
            local iMoveLen = string.len(gzTextVar.zRoomList[i].zPathListing[q])
            
            --If the new path is longer than the old one, ignore it.
            if((string.len(sMoveString)+1 >= iMoveLen and iMoveLen > 0) or q == i) then
           
            --Otherwise, add it.
            else
                --Append the next movement to the path.
                gzTextVar.zRoomList[i].zPathListing[q] = "S" .. sMoveString
                
                --Create a new entry on the pulse list.
                zaAdjacentList[iArrayMax+1] = {}
                zaAdjacentList[iArrayMax+1].iIndex = q
                zaAdjacentList[iArrayMax+1].sMoves = gzTextVar.zRoomList[i].zPathListing[q]
                iArrayMax = iArrayMax + 1
            end
            
        end
        
        --South connection.
        if(gzTextVar.zRoomList[u].iMoveS ~= -1) then
            
            --Fast-access.
            local q = gzTextVar.zRoomList[u].iMoveS
            local iMoveLen = string.len(gzTextVar.zRoomList[i].zPathListing[q])
            
            --If the new path is longer than the old one, ignore it.
            if((string.len(sMoveString)+1 >= iMoveLen and iMoveLen > 0) or q == i) then
           
            --Otherwise, add it.
            else
                --Append the next movement to the path.
                gzTextVar.zRoomList[i].zPathListing[q] = "N" .. sMoveString
                
                --Create a new entry on the pulse list.
                zaAdjacentList[iArrayMax+1] = {}
                zaAdjacentList[iArrayMax+1].iIndex = q
                zaAdjacentList[iArrayMax+1].sMoves = gzTextVar.zRoomList[i].zPathListing[q]
                iArrayMax = iArrayMax + 1
            end
        end
        
        --West connection.
        if(gzTextVar.zRoomList[u].iMoveW ~= -1) then
            
            --Fast-access.
            local q = gzTextVar.zRoomList[u].iMoveW
            local iMoveLen = string.len(gzTextVar.zRoomList[i].zPathListing[q])
            
            --If the new path is longer than the old one, ignore it.
            if((string.len(sMoveString)+1 >= iMoveLen and iMoveLen > 0) or q == i) then
           
            --Otherwise, add it.
            else
                --Append the next movement to the path.
                gzTextVar.zRoomList[i].zPathListing[q] = "E" .. sMoveString
                
                --Create a new entry on the pulse list.
                zaAdjacentList[iArrayMax+1] = {}
                zaAdjacentList[iArrayMax+1].iIndex = q
                zaAdjacentList[iArrayMax+1].sMoves = gzTextVar.zRoomList[i].zPathListing[q]
                iArrayMax = iArrayMax + 1
            end
        end
        
        --East connection.
        if(gzTextVar.zRoomList[u].iMoveE ~= -1) then
            
            --Fast-access.
            local q = gzTextVar.zRoomList[u].iMoveE
            local iMoveLen = string.len(gzTextVar.zRoomList[i].zPathListing[q])
            
            --If the new path is longer than the old one, ignore it.
            if((string.len(sMoveString)+1 >= iMoveLen and iMoveLen > 0) or q == i) then
           
            --Otherwise, add it.
            else
                --Append the next movement to the path.
                gzTextVar.zRoomList[i].zPathListing[q] = "W" .. sMoveString
                
                --Create a new entry on the pulse list.
                zaAdjacentList[iArrayMax+1] = {}
                zaAdjacentList[iArrayMax+1].iIndex = q
                zaAdjacentList[iArrayMax+1].sMoves = gzTextVar.zRoomList[i].zPathListing[q]
                iArrayMax = iArrayMax + 1
            end
        end
        
        iAdjacencyCursor = iAdjacencyCursor + 1
    end
    
    
    --Debug.
    --for p = 1, gzTextVar.iRoomsTotal, 1 do
     --   io.write("Path to main hall for " .. gzTextVar.zRoomList[p].sName .. " is: " .. gzTextVar.zRoomList[1].zPathListing[p] .. "\n")
    --end
    --break
end

