--[ ======================================= Health Potion ======================================= ]
--Health Potion handler script. Deals with things like examining and consuming.

--Argument Listing:
-- 0: sInputString - The input string in question.
-- 1: iItemSlot - The item slot of the item in question.
-- 2: iRoomSlot - The room slot of the item in question.

--Arg check.
local iRequiredArgs = 3
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs and TL_GetProperty("Is Building Commands") == false) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--[ ====================================== Command Builder ====================================== ]
--Used for building a list of commands when this entity is selected on the locality window.
-- This ignores script argument requirements. Also note that the arguments are INVALID at this point.
if(TL_GetProperty("Is Building Commands") == true) then
    
    --First, resolve the name of the clicked entity.
    local sEntityHeader = TL_GetProperty("Header String")
    local sEntityName = TL_GetProperty("Command String")
    
    --In the inventory:
    if(sEntityHeader == "Inventory") then
        TL_SetProperty("Register Popup Command", "look",  "look "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "drink", "drink " .. sEntityName)
        TL_SetProperty("Register Popup Command", "drop",  "drop "  .. sEntityName)
    
    --On the ground:
    else
        TL_SetProperty("Register Popup Command", "look",  "look "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "drink", "drink " .. sEntityName)
        TL_SetProperty("Register Popup Command", "take",  "take "  .. sEntityName)
    end
    return
end

--[ ======================================= Verification ======================================== ]
--Arg resolve.
local sInputString = LM_GetScriptArgument(0)
local iItemSlot = tonumber(LM_GetScriptArgument(1))
local iRoomSlot = tonumber(LM_GetScriptArgument(2))

--Variables.
local sLocalName = "Null"

--Verify the item index.
if(iRoomSlot == -1) then
    
    --Range check.
    if(iItemSlot < 1 or iItemSlot > gzTextVar.gzPlayer.iItemsTotal) then return end
    
    --Store name.
    sLocalName = gzTextVar.gzPlayer.zaItems[iItemSlot].sDisplayName

--Item is a room object.
else
    
    --Range check.
    if(iRoomSlot < 1 or iRoomSlot > gzTextVar.iRoomsTotal) then return end
    if(iItemSlot < 1 or iItemSlot > gzTextVar.zRoomList[iRoomSlot].iObjectsTotal) then return end
    
    --Store name.
    sLocalName = gzTextVar.zRoomList[iRoomSlot].zObjects[iItemSlot].sDisplayName

end

--[ ====================================== Basic Commands ======================================= ]
--Examining it.
if(sInputString == "look " .. sLocalName or sInputString == "examine " .. sLocalName) then
    TL_SetProperty("Append", "A bubbling red potion. Will restore all HP when used with the [drink] command.\n")
    gbHandledInput = true

--Examining it in the inventory. One must exist in the inventory or this will fail.
elseif((sInputString == "look inv " .. sLocalName or sInputString == "examine inv " .. sLocalName) and iItemSlot ~= -1) then
    TL_SetProperty("Append", "A bubbling red potion. Will restore all HP when used with the [drink] command.\n")
    gbHandledInput = true

--Taking it.
elseif(sInputString == "take " .. sLocalName or sInputString == "get " .. sLocalName) then

    --Ignore this if the item in question is already in the inventory.
    if(iRoomSlot == -1) then
        
        --If there are potions in the inventory but not on the ground, print a warning message.
        local iIndexInRoom = fnGetIndexOfItemInRoom(gzTextVar.gzPlayer.sLocation, sLocalName)
        if(iIndexInRoom == -1) then
            TL_SetProperty("Append", "You already have that in your inventory.\n")
            gbHandledInput = true
        end
        return
    end
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to pick that up.\n")
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --Otherwise, take the item.
    TL_SetProperty("Append", "You pick up the [" .. sLocalName .. "].\n")
    
    --Increment
    gzTextVar.gzPlayer.iItemsTotal = gzTextVar.gzPlayer.iItemsTotal + 1
    local p = gzTextVar.gzPlayer.iItemsTotal
    
    --Create the item.
    gzTextVar.gzPlayer.zaItems[p] = {}
    gzTextVar.gzPlayer.zaItems[p] = gzTextVar.zRoomList[iRoomSlot].zObjects[iItemSlot]
    
    --Remove the item from the original room listing.
    fnRemoveItemFromRoom(iRoomSlot, iItemSlot)
    LM_ExecuteScript(gzTextVar.sTurnEndScript)
    gbHandledInput = true

--Drop Handler.
elseif(sInputString == "drop " .. sLocalName and iRoomSlot == -1) then
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to drop that.\n")
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end

    --Display.
    TL_SetProperty("Append", "You drop the [" .. sLocalName .. "].\n")
    
    --Determine where the player currently is.
    local iRoomIndex = fnGetRoomIndex(gzTextVar.gzPlayer.sLocation)
    if(iRoomIndex == -1) then return end

    --Add it to the room's inventory.
    gzTextVar.zRoomList[iRoomIndex].iObjectsTotal = gzTextVar.zRoomList[iRoomIndex].iObjectsTotal + 1
    local p = gzTextVar.zRoomList[iRoomIndex].iObjectsTotal
    
    --Create the object.
    gzTextVar.zRoomList[iRoomIndex].zObjects[p] = gzTextVar.gzPlayer.zaItems[iItemSlot]
    fnRemoveItemFromInventory(iItemSlot)
    gbHandledInput = true

--Drinking it.
elseif(sInputString == "drink " .. sLocalName) then

    --Check if there's one in the player's inventory. If so, remove it entirely.
    local iInRoomIndex = -1
    local bIsInventoryCase = false
    local iInInventoryIndex = fnGetIndexOfItemInInventory(sLocalName)
    if(iInInventoryIndex ~= -1) then
    
        --Wound handler.
        if(gzTextVar.gzPlayer.bIsCrippled) then
            TL_SetProperty("Append", "You are too hurt to drink that.\n")
            LM_ExecuteScript(gzTextVar.sTurnEndScript)
            return
        end
        
        --Standard.
        bIsInventoryCase = true

    --If the item was not found in the inventory, check if there's one on the floor.
    else
    
        --Check if there's one on the floor. If so, remove it.
        iInRoomIndex = fnGetIndexOfItemInRoom(gzTextVar.gzPlayer.sLocation, sLocalName)
        if(iInRoomIndex ~= -1) then
    
            --Wound handler.
            if(gzTextVar.gzPlayer.bIsCrippled) then
                gbHandledInput = true
                TL_SetProperty("Append", "You are too hurt to drink that.\n")
                LM_ExecuteScript(gzTextVar.sTurnEndScript)
                return
            end
            
            --Normal.
        
        --Item was not found.
        else
    
            --Wound handler.
            if(gzTextVar.gzPlayer.bIsCrippled) then
                gbHandledInput = true
                TL_SetProperty("Append", "You can't drink something you don't have, and are far too injured to do so anyway.\n")
                LM_ExecuteScript(gzTextVar.sTurnEndScript)
                return
            end
            
            --Normal.
            TL_SetProperty("Append", "You can't drink something you don't have.\n")
            gbHandledInput = true
            return
        end
    end
        
    --Check if the player has less than max HP.
    if(gzTextVar.gzPlayer.iHP >= gzTextVar.gzPlayer.iHPMax) then
        TL_SetProperty("Append", "You are at maximum health already.\n")
        gbHandledInput = true
    
    --Drink the potion.
    else
    
        --Remove.
        if(bIsInventoryCase) then
            fnRemoveItemFromInventory(iInInventoryIndex)
        else
            fnRemoveItemFromRoom(iRoomSlot, iInRoomIndex)
        end
    
        --Restore.
        gzTextVar.gzPlayer.iHP = gzTextVar.gzPlayer.iHPMax
        TL_SetProperty("Set Player Stats", gzTextVar.gzPlayer.iHP, gzTextVar.gzPlayer.iHPMax, gzTextVar.gzPlayer.iAtp, gzTextVar.gzPlayer.iDef, gzTextVar.gzPlayer.sQuerySprite)
    
        --Display.
        TL_SetProperty("Append", "You drink the potion and immediately feel much better. All HP restored!\n")
        gbHandledInput = true
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
    end

    --Display.
end