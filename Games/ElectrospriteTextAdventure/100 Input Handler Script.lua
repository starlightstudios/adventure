-- |[ ===================================== Input Handler ====================================== ]|
--Whenever the player inputs a string, this script gets called to handle it. Anything that handles
-- the input should return afterwards. If nothing handles the input, then an error message is shown.

--Argument Listing:
-- 0: sString - The string to be handled. Should be in all lowercase.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sString = LM_GetScriptArgument(0)

--Don't do anything if the string is empty.
if(sString == "") then return end

--Others.
local iRoom = fnGetRoomIndex(gzTextVar.gzPlayer.sLocation)
if(iRoom == -1) then return end

-- |[ ========================================= Common ========================================= ]|
--Common functionality goes here.
if(sString == "exit" or sString == "quit") then
    
    --Common.
    TL_SetProperty("Append", "Exiting program...")
    
    --Booted from the menu:
    if(bQuitsToMenu == true) then
        bQuitsToMenu = false
        MapM_BackToTitle()
    
    --From in the game itself:
    else
    
        --Delete the level and restore the existing level, if it was stored.
        TL_Unsuspend()
        
        if(gsPostExec ~= nil) then
            LM_ExecuteScript(gsPostExec)
        end
    end
    return
end

-- |[ ===================================== System Inputs ====================================== ]|
--These inputs ignore things like turn priority and are used for displaying help or setting options.
-- First is help, which explains how to play the game.
if(sString == "help") then
    TL_SetProperty("Append", "Welcome to the Text Adventure Conversion Training Program. Type commands here, or use the list on the right with your mouse.")
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "The 'think' command will allow you to review your current objectives in case you have forgotten them.")
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "Locate the humans in this area, subdue them, and convert them. All five must be converted to complete this exercise.")
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "You may adjust the difficulty with 'difficulty easy' or 'difficulty hard'. You will have less time on hard and more time on easy when fighting humans.")
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "To exit this exercise, type 'quit' or 'exit' in this console. Thank you!")
    TL_SetProperty("Create Blocker")
    LM_ExecuteScript(gzTextVar.sTurnEndScript)
    return
end

--Think shows the player their current objectives.
if(sString == "think") then
    LM_ExecuteScript(gzTextVar.sThinkHandler)
    return
end

--Change difficulty:
if(string.sub(sString, 1, 10) == "difficulty") then
    if(sString == "difficulty easy") then
        gzTextVar.gzPlayer.iDifficulty = 0
        TL_SetProperty("Append", "Combat difficulty is now 'easy'.")
    elseif(sString == "difficulty normal") then
        gzTextVar.gzPlayer.iDifficulty = 1
        TL_SetProperty("Append", "Combat difficulty is now 'normal'.")
    elseif(sString == "difficulty hard") then
        gzTextVar.gzPlayer.iDifficulty = 2
        TL_SetProperty("Append", "Combat difficulty is now 'hard'. The timer will start immediately and not wait for a keypress!")
    else
        TL_SetProperty("Append", "Unknown case. Use 'difficulty easy', 'difficulty normal', or 'difficulty hard'.")
    end
    return
end

--Show the player's inventory.
if(sString == "inventory") then
    
    --Player has nothing in their inventory.
    if(gzTextVar.gzPlayer.iItemsTotal == 0) then
        TL_SetProperty("Append", "You are not carrying anything.\n")
    
    --Player has one object in their inventory:
    elseif(gzTextVar.gzPlayer.iItemsTotal == 1) then
        TL_SetProperty("Append", "You are carrying [" .. gzTextVar.gzPlayer.zaItems[1].sDisplayName .. "].\n")
    
    --Two objects.
    elseif(gzTextVar.gzPlayer.iItemsTotal == 2) then
        TL_SetProperty("Append", "You are carrying [" .. gzTextVar.gzPlayer.zaItems[1].sDisplayName .. "] and [" .. gzTextVar.gzPlayer.zaItems[2].sDisplayName .. "].\n")
    
    --List builder.
    else
    
        --Setup.
        local sSentence = "You are carrying"
        
        --All members except the end.
        for i = 1, gzTextVar.gzPlayer.iItemsTotal - 1, 1 do
            sSentence = sSentence .. "[" .. gzTextVar.gzPlayer.zaItems[i].sDisplayName .. "], "
        end
        
        --Add to the end.
        sSentence = sSentence .. "and [" .. gzTextVar.gzPlayer.zaItems[gzTextVar.gzPlayer.iItemsTotal].sDisplayName .. "].\n"
        
        --Append it.
        TL_SetProperty("Append", sSentence)
    
    end
    return
end

--Objects shows objects in the same room as the player.
if(sString == "objects") then
    fnListObjects(gzTextVar.gzPlayer.sLocation, false)
    return
end

--Entities shows nearby entities.
if(sString == "entities") then
    fnListEntities(gzTextVar.gzPlayer.sLocation, false)
    return
end

--Self-examination.
if(sString == "look me" or sString == "examine me") then
    LM_ExecuteScript(gzTextVar.sEntityHandlers .. "Self.lua")
    return
end

--Recharge. In your HQ you can recharge your HP to max.
if(sString == "recharge") then

    --Player is in the HQ:
    if(gzTextVar.gzPlayer.sLocation == "Headquarters") then
        
        --Player needs HP:
        if(gzTextVar.gzPlayer.iHP < gzTextVar.gzPlayer.iHPMax) then
            gzTextVar.gzPlayer.iHP = 30
            TL_SetProperty("Set Player Stats", gzTextVar.gzPlayer.iHP, gzTextVar.gzPlayer.iHPMax, gzTextVar.gzPlayer.iAtp, gzTextVar.gzPlayer.iDef, gzTextVar.gzPlayer.iLPH, gzTextVar.gzPlayer.sQuerySprite)
            TL_SetProperty("Append", "You recharge your power core at the nearby repair station. The auto-repair nanites use the surplus to restore your damaged body.\n")
            LM_ExecuteScript(gzTextVar.sTurnEndScript)
        
        --No need for HP:
        else
            TL_SetProperty("Append", "Your body is currently undamaged and does not need a recharge.\n")
        end
    
    --Not in their HQ.
    else
        TL_SetProperty("Append", "Surprisingly, you cannot recharge when not near a repair station. Repair stations on Pandemonium are rare, so always be aware of the nearest one!\n")
    end
    return
end

-- |[ ======================================== Warping ========================================= ]|
--Used as a sort of debug command.
if(sString == "warp debug room" or sString == "warp debugroom" or sString == "cheat") then

    --Player is already there:
    if(gzTextVar.gzPlayer.sLocation == "Debug Room") then
        TL_SetProperty("Append", "You are already in the debug room, you goof.\n")
        return
    end

    --Unmark the previous location, and mark this one as visible.
    fnUnmarkMinimapForPosition(gzTextVar.gzPlayer.sLocation)
    fnMarkMinimapForPosition("Debug Room")

    --Success! Move the character.
    gzTextVar.gzPlayer.sLocation = "Debug Room"
    local fX, fY, fZ = fnGetRoomPosition(gzTextVar.gzPlayer.sLocation)
    TL_SetProperty("Set Map Focus", -fX, -fY)

    --Show the room's name, list of entities, and objects.
    TL_SetProperty("Append", "Warping to Debug Room.\n")
    fnListEntities(gzTextVar.gzPlayer.sLocation, true)
    fnListObjects(gzTextVar.gzPlayer.sLocation, true)

    --End the turn.
    LM_ExecuteScript(gzTextVar.sTurnEndScript)
    return

--Warp back to the headquarters:
elseif(sString == "warp headquarters") then

    --Player is already there:
    if(gzTextVar.gzPlayer.sLocation == "Headquarters") then
        TL_SetProperty("Append", "You are already in the headquarters, you goof.\n")
        return
    end

    --Unmark the previous location, and mark this one as visible.
    fnUnmarkMinimapForPosition(gzTextVar.gzPlayer.sLocation)
    fnMarkMinimapForPosition("Headquarters")

    --Success! Move the character.
    gzTextVar.gzPlayer.sLocation = "Headquarters"
    local fX, fY, fZ = fnGetRoomPosition(gzTextVar.gzPlayer.sLocation)
    TL_SetProperty("Set Map Focus", -fX, -fY)

    --Show the room's name, list of entities, and objects.
    TL_SetProperty("Append", "Warping to Headquarters.\n")
    fnListEntities(gzTextVar.gzPlayer.sLocation, true)
    fnListObjects(gzTextVar.gzPlayer.sLocation, true)

    --End the turn.
    LM_ExecuteScript(gzTextVar.sTurnEndScript)
    return
end

-- |[ ======================================= Subscripts ======================================= ]|
--Calls a set of subscripts to handle the input. If one handles it, stops there.
gbHandledInput = false
LM_ExecuteScript(gzTextVar.sRootPath .. "101 Move Handler.lua", sString)
if(gbHandledInput == true) then return end

-- |[ ===================================== Room Handlers ====================================== ]|
--Call the script that matches the room name. It handles internal logic for special cases. Not all
-- room scripts will do anything.
LM_ExecuteScript(gzTextVar.sRoomHandlers .. gzTextVar.gzPlayer.sLocation .. ".lua", sString)
if(gbHandledInput == true) then return end

--If the command "look" is used but was not handled, print this.
if(sString == "look" or sString == "examine") then
    TL_SetProperty("Append", "There is nothing interesting about this room.\n")
    fnListEntities(gzTextVar.gzPlayer.sLocation, true)
    fnListObjects(gzTextVar.gzPlayer.sLocation, true)
    return
end

-- |[ ==================================== Entity Handlers ===================================== ]|
--See if an entity handled the command.
for i = 1, gzTextVar.zEntitiesTotal, 1 do
   
   --Store
   gzTextVar.iCurrentEntity = i
   
    --Check if the entity is in the same room as the player.
    if(gzTextVar.zEntities[i].sLocation == gzTextVar.gzPlayer.sLocation) then
        
        --Run the handler. Most entities only respond to their own name, but special actions are possible.
        LM_ExecuteScript(gzTextVar.zEntities[i].sCommandHandler, sString)
        if(gbHandledInput) then return end
    end
end

--Clear the activity variable.
gzTextVar.iCurrentEntity = -1

-- |[ ==================================== Objects Handlers ==================================== ]|
--Handles objects, obviously. This includes inventory items. Inventory gets priority for the purposes
-- of using items, but the player can actually drink a potion right off the floor.
for i = 1, gzTextVar.gzPlayer.iItemsTotal, 1 do
    if(gzTextVar.gzPlayer.zaItems[i].sHandlerScript ~= nil) then
        LM_ExecuteScript(gzTextVar.gzPlayer.zaItems[i].sHandlerScript, sString, i, -1)
        if(gbHandledInput == true) then return end
    end
end

--Check objects local to the room.
for i = 1, gzTextVar.zRoomList[iRoom].iObjectsTotal, 1 do
    if(gzTextVar.zRoomList[iRoom].zObjects[i].sHandlerScript ~= nil) then
        LM_ExecuteScript(gzTextVar.zRoomList[iRoom].zObjects[i].sHandlerScript, sString, i, iRoom)
        if(gbHandledInput == true) then return end
    end
end

-- |[ ===================================== Other Actions ====================================== ]|
if(sString == "wait") then
    TL_SetProperty("Append", "Time passes.\n")
    LM_ExecuteScript(gzTextVar.sTurnEndScript)
    return
end

-- |[ ======================================== Default ========================================= ]|
--If we got this far, the command was not understood. Print a help message.
if(true) then
    TL_SetProperty("Append", "I was unable to understand what you meant. Please type 'help' if you need instructions, or use the list of commands on the right side with your mouse.\n\n")
    TL_SetProperty("Create Blocker")
    return
end