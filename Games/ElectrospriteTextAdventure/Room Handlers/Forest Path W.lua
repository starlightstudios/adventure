--[ ========================================== Room Description ======================================== ]
--Handler script for the player examining a location.

--Argument Listing:
-- 0: sInstruction - What exactly we want out of the room.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sInstruction = LM_GetScriptArgument(0)

--[ ======================================= System Commands ===================================== ]
--[Room Description]
if(sInstruction == "look") then

    --Flag input as handled.
    gbHandledInput = true
    
    --Locate which room index we are in. In order to call this, the player has to be here, so there's
    -- no need to search for it.
    local iIndex = fnGetRoomIndex(gzTextVar.gzPlayer.sLocation)
    
    --Now print the description with appropriate line breaks.
    TL_SetProperty("Append", gzTextVar.zRoomList[iIndex].sDescription .. "\n\n")
    
    --List nearby objects and entities.
    fnListEntities(gzTextVar.gzPlayer.sLocation, true)
    fnListObjects(gzTextVar.gzPlayer.sLocation, true)
    
    --Line breaks for formatting.
    TL_SetProperty("Append", "\n")

end