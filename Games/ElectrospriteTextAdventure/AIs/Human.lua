-- |[ ======================================== Human AI ======================================== ]|
--Human AI. Patrols between various positions. Betty patrols around the farm fields.
-- When cored, Betty changes her AI and this script is no longer used.

--Argument Listing:
-- 0: iIndex - The entity index of the acting entity.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local i = tonumber(LM_GetScriptArgument(0))

-- |[ =================================== Hostility Checker ==================================== ]|
--If this flag is set, the AI is checking entity hostility.
if(gzTextVar.bIsCheckingHostility == true) then
    
    --Crippled entities do not pass this check.
    if(gzTextVar.zEntities[i].bIsCrippled == false) then
        gzTextVar.bLastHostilityCheck = true
    end
    return
end

-- |[ ==================================== Detection Logic ===================================== ]|
--If in the same room, a battle will start.
if(gzTextVar.zEntities[i].sLocation == gzTextVar.gzPlayer.sLocation or gzTextVar.bIsTriggeringFights) then
    
    --Crippled entities cannot do this.
    if(gzTextVar.zEntities[i].bIsCrippled == false) then
        
        --Player stats must be stored before activating combat.
        TL_SetProperty("Set Player Stats", gzTextVar.gzPlayer.iHP, gzTextVar.gzPlayer.iHPMax, gzTextVar.gzPlayer.iAtp, gzTextVar.gzPlayer.iDef, gzTextVar.gzPlayer.sQuerySprite)
        
        TL_SetProperty("Activate Combat")
        TL_SetTypeCombatProperty("Add Word To Attack Pool", "strike")
        TL_SetTypeCombatProperty("Add Word To Attack Pool", "attack")
        TL_SetTypeCombatProperty("Add Word To Attack Pool", "hurt")
        TL_SetTypeCombatProperty("Add Word To Attack Pool", "punch")
        TL_SetTypeCombatProperty("Add Word To Attack Pool", "wound")
        TL_SetTypeCombatProperty("Add Word To Attack Pool", "damage")
        TL_SetTypeCombatProperty("Add Word To Attack Pool", "slice")
        TL_SetTypeCombatProperty("Add Word To Attack Pool", "kick")
        TL_SetTypeCombatProperty("Add Word To Attack Pool", "assault")
        TL_SetTypeCombatProperty("Add Word To Attack Pool", "injure")
        
        TL_SetTypeCombatProperty("Add Word To Defend Pool", "block")
        TL_SetTypeCombatProperty("Add Word To Defend Pool", "protect")
        TL_SetTypeCombatProperty("Add Word To Defend Pool", "guard")
        TL_SetTypeCombatProperty("Add Word To Defend Pool", "defend")
        TL_SetTypeCombatProperty("Add Word To Defend Pool", "weather")
        TL_SetTypeCombatProperty("Add Word To Defend Pool", "persist")
        TL_SetTypeCombatProperty("Add Word To Defend Pool", "survive")
        TL_SetTypeCombatProperty("Add Word To Defend Pool", "endure")
        TL_SetTypeCombatProperty("Add Enemy", 20, 12, 2, gzTextVar.zEntities[i].sQueryPicture)
        TL_SetProperty("Set Combat Scripts", gzTextVar.sRootPath .. "Combat Handlers/Standard Victory Handler.lua", gzTextVar.sRootPath .. "Combat Handlers/Standard Defeat Handler.lua")
        
        --Set difficulty.
        TL_SetTypeCombatProperty("Set Difficulty", gzTextVar.gzPlayer.iDifficulty)
        
        --Call the finalizer. This sends the instruction to start the attack turn.
        TL_SetProperty("Finalize Combat")
    end
    return
end

-- |[ ====================================== Patrol Logic ====================================== ]|
--Crippled. Don't move.
if(gzTextVar.zEntities[i].bIsCrippled == true) then return end

-- |[ ====================================== Patrol Logic ====================================== ]|
-- |[New Movement]|
--If the current movement index is -1, move to the next node on the patrol list.
if(gzTextVar.zEntities[i].iMoveIndex == -1) then
    
    --Get the current node.
    local p = gzTextVar.zEntities[i].iPatrolIndex
    gzTextVar.zEntities[i].iPatrolIndex = gzTextVar.zEntities[i].iPatrolIndex + 1
    local sPatrolTarget = gzTextVar.zEntities[i].saPatrolList[p]
    if(sPatrolTarget == nil) then
        sPatrolTarget = gzTextVar.zEntities[i].saPatrolList[1]
        gzTextVar.zEntities[i].iPatrolIndex = 2
    end
    if(sPatrolTarget == nil) then
        io.write("Warning: Entity " .. i .. " has no patrol paths.\n")
        return
    end
    
    --Make the first move in the direction in question.
    gzTextVar.zEntities[i].iMoveIndex = 1
    
    --Get the current room.
    local sRoomName = gzTextVar.zEntities[i].sLocation
    local iRoomIndex = fnGetRoomIndex(sRoomName)
    
    --Get the target room.
    local iTargetIndex = fnGetRoomIndex(sPatrolTarget)
    
    --Error checks.
    if(iRoomIndex == -1 or iTargetIndex == -1) then return end
    
    --Get the first letter of matching movement.
    local sPathInstructions = gzTextVar.zRoomList[iTargetIndex].zPathListing[iRoomIndex]
    if(sPathInstructions == "") then return end
    local sFirstLetter = string.sub(sPathInstructions, 1, 1)
    
    --Move in the requested direction:
    local sDestination = "Null"
    local sArrivesFrom = "nowhere"
    local sDepartsTo = "nowhere"
    if(sFirstLetter == "N") then
        sDestination = gzTextVar.zRoomList[iRoomIndex].sMoveN
        sArrivesFrom = "the south"
        sDepartsTo = "to the north"
    elseif(sFirstLetter == "S") then
        sDestination = gzTextVar.zRoomList[iRoomIndex].sMoveS
        sArrivesFrom = "the north"
        sDepartsTo = "to the south"
    elseif(sFirstLetter == "E") then
        sDestination = gzTextVar.zRoomList[iRoomIndex].sMoveE
        sArrivesFrom = "the west"
        sDepartsTo = "to the east"
    elseif(sFirstLetter == "W") then
        sDestination = gzTextVar.zRoomList[iRoomIndex].sMoveW
        sArrivesFrom = "the east"
        sDepartsTo = "to the west"
    elseif(sFirstLetter == "U") then
        sDestination = gzTextVar.zRoomList[iRoomIndex].sMoveU
        sArrivesFrom = "below"
        sDepartsTo = "down"
    elseif(sFirstLetter == "D") then
        sDestination = gzTextVar.zRoomList[iRoomIndex].sMoveD
        sArrivesFrom = "above"
        sDepartsTo = "up"
    end

    --Failure:
    if(sDestination == "Null") then
        return
    end

    --If the character was in the same location as the player:
    if(gzTextVar.zEntities[i].sLocation == gzTextVar.gzPlayer.sLocation) then
        TL_SetProperty("Append", gzTextVar.zEntities[i].sDisplayName .. " goes " .. sDepartsTo .. ".")
        
    --If the arrival position is the same as the player's:
    elseif(sDestination == gzTextVar.gzPlayer.sLocation) then
        TL_SetProperty("Append", gzTextVar.zEntities[i].sDisplayName .. " arrives from " .. sArrivesFrom .. ".")
    end

    --Success! Move the character.
    gzTextVar.zEntities[i].sLocation = sDestination
    
    --If the destination was reached, reset the move handler.
    if(gzTextVar.zEntities[i].sLocation == sPatrolTarget) then
        gzTextVar.zEntities[i].iMoveIndex = -1
    end

--Continuing a patrol path.
else
    
    --Get the destination again.
    local p = gzTextVar.zEntities[i].iPatrolIndex - 1
    local sPatrolTarget = gzTextVar.zEntities[i].saPatrolList[p]
    
    --Increment the move index.
    gzTextVar.zEntities[i].iMoveIndex = gzTextVar.zEntities[i].iMoveIndex + 1
    
    --Get the current room.
    local sRoomName = gzTextVar.zEntities[i].sLocation
    local iRoomIndex = fnGetRoomIndex(sRoomName)
    
    --Get the target room.
    local iTargetIndex = fnGetRoomIndex(sPatrolTarget)
    
    --Error checks.
    if(iRoomIndex == -1 or iTargetIndex == -1) then return end
    
    --Get the first letter of matching movement.
    local sPathInstructions = gzTextVar.zRoomList[iTargetIndex].zPathListing[iRoomIndex]
    if(sPathInstructions == "") then return end
    local sFirstLetter = string.sub(sPathInstructions, 1, 1)
    
    --Move in the requested direction:
    local sDestination = "Null"
    local sArrivesFrom = "nowhere"
    local sDepartsTo = "nowhere"
    if(sFirstLetter == "N") then
        sDestination = gzTextVar.zRoomList[iRoomIndex].sMoveN
        sArrivesFrom = "the south"
        sDepartsTo = "to the north"
    elseif(sFirstLetter == "S") then
        sDestination = gzTextVar.zRoomList[iRoomIndex].sMoveS
        sArrivesFrom = "the north"
        sDepartsTo = "to the south"
    elseif(sFirstLetter == "E") then
        sDestination = gzTextVar.zRoomList[iRoomIndex].sMoveE
        sArrivesFrom = "the west"
        sDepartsTo = "to the east"
    elseif(sFirstLetter == "W") then
        sDestination = gzTextVar.zRoomList[iRoomIndex].sMoveW
        sArrivesFrom = "the east"
        sDepartsTo = "to the west"
    elseif(sFirstLetter == "U") then
        sDestination = gzTextVar.zRoomList[iRoomIndex].sMoveU
        sArrivesFrom = "below"
        sDepartsTo = "down"
    elseif(sFirstLetter == "D") then
        sDestination = gzTextVar.zRoomList[iRoomIndex].sMoveD
        sArrivesFrom = "above"
        sDepartsTo = "up"
    end

    --Failure:
    if(sDestination == "Null") then
        return
    end

    --If the character was in the same location as the player:
    if(gzTextVar.zEntities[i].sLocation == gzTextVar.gzPlayer.sLocation) then
        TL_SetProperty("Append", gzTextVar.zEntities[i].sDisplayName .. " goes " .. sDepartsTo .. ".")
        
    --If the arrival position is the same as the player's:
    elseif(sDestination == gzTextVar.gzPlayer.sLocation) then
        TL_SetProperty("Append", gzTextVar.zEntities[i].sDisplayName .. " arrives from " .. sArrivesFrom .. ".")
    end

    --Success! Move the character.
    gzTextVar.zEntities[i].sLocation = sDestination
    
    --If the destination was reached, reset the move handler.
    if(gzTextVar.zEntities[i].sLocation == sPatrolTarget) then
        gzTextVar.zEntities[i].iMoveIndex = -1
    end

end