--[ ===================================== Scenario Variables ==================================== ]
--Variables local to this particular scenario.

--World Turns
gzTextVar.iWorldTurns = 0

--Combat special flags
gzTextVar.bTurnEndsWhenCombatEnds = false

--State Variables
gzTextVar.iGameStage = 1
gzTextVar.iHumansLeft = 5
gzTextVar.sConversionChamberTarget = "Nobody"
gzTextVar.iConversionState = 0
gzTextVar.iUnitDesignation = 844322

--[ ====================================== Player Variables ===================================== ]
--Place the player near the main hall.
gzTextVar.gzPlayer = {}
gzTextVar.gzPlayer.sName = "771852"
gzTextVar.gzPlayer.sLocation = "Headquarters"
gzTextVar.gzPlayer.sQuerySprite = "Root/Images/ElectrospriteAdv/Characters/Player"
gzTextVar.gzPlayer.bIsCrippled = false
gzTextVar.gzPlayer.sFormState = "Golem"

--Mark explored/visible for the player and everything adjacent.
fnMarkMinimapForPosition(gzTextVar.gzPlayer.sLocation)

--Default images.
TL_SetProperty("Default Image", "You", "Root/Images/ElectrospriteAdv/Characters/Player", 0)

--Player's combat statistics.
gzTextVar.gzPlayer.iHP = 20
gzTextVar.gzPlayer.iHPMax = 30
gzTextVar.gzPlayer.iAtp = 6
gzTextVar.gzPlayer.iDef = 2
gzTextVar.gzPlayer.iLPH = 7
TL_SetProperty("Set Player Stats", gzTextVar.gzPlayer.iHP, gzTextVar.gzPlayer.iHPMax, gzTextVar.gzPlayer.iAtp, gzTextVar.gzPlayer.iDef, gzTextVar.gzPlayer.sQuerySprite)

--[Inventory]
gzTextVar.gzPlayer.iItemsTotal = 0
gzTextVar.gzPlayer.zaItems = {}

-- |[Combat Difficulty]|
gzTextVar.gzPlayer.iDifficulty = 1

--[ ====================================== Entity Variables ===================================== ]
--These are entities other than the player. This includes allies and enemies.
gzTextVar.zEntitiesTotal = 0
gzTextVar.zEntities = {}

-- |[Susan]|
--Special NPC, played by an Electrosprite.
gzTextVar.zEntitiesTotal = gzTextVar.zEntitiesTotal + 1

--Shorthand
local i = gzTextVar.zEntitiesTotal
gzTextVar.iSusanSlot = i

gzTextVar.zEntities[i] = {}
gzTextVar.zEntities[i].sDisplayName = "Susan"
gzTextVar.zEntities[i].sQueryName = "susan"
gzTextVar.zEntities[i].sQueryPicture = "Root/Images/ElectrospriteAdv/Characters/NPCENormal"
gzTextVar.zEntities[i].sUniqueName = "susan"
gzTextVar.zEntities[i].sLocation = "Debug Room"
gzTextVar.zEntities[i].sAutoTopic = "susan"
gzTextVar.zEntities[i].bIsCrippled = false
gzTextVar.zEntities[i].sGenderA = "her"
gzTextVar.zEntities[i].sGenderB = "she"
gzTextVar.zEntities[i].sGenderCapitalA = "Her"
gzTextVar.zEntities[i].sGenderCapitalB = "She"

--Other images
gzTextVar.zEntities[i].sCoredPicture = "Root/Images/ElectrospriteAdv/Characters/NPCECored"
gzTextVar.zEntities[i].sTF0Picture = "Root/Images/ElectrospriteAdv/Characters/NPCETF0"
gzTextVar.zEntities[i].sTF1Picture = "Root/Images/ElectrospriteAdv/Characters/NPCETF1"
gzTextVar.zEntities[i].sTF2Picture = "Root/Images/ElectrospriteAdv/Characters/NPCETF2"
gzTextVar.zEntities[i].sGolemPicture = "Root/Images/ElectrospriteAdv/Characters/NPCEGolem"

--Command Handler
gzTextVar.zEntities[i].sCommandHandler = gzTextVar.sEntityHandlers .. "Susan.lua"
gzTextVar.zEntities[i].sAIHandler = gzTextVar.sRootPath .. "AIs/Susan.lua"
gzTextVar.zEntities[i].iMoveIndex = -1
gzTextVar.zEntities[i].iPatrolIndex = 1
gzTextVar.zEntities[i].saPatrolList = {""}

-- |[Betty]|
--NPC found wandering around the farm fields.
gzTextVar.zEntitiesTotal = gzTextVar.zEntitiesTotal + 1

--Shorthand
i = gzTextVar.zEntitiesTotal

gzTextVar.zEntities[i] = {}
gzTextVar.zEntities[i].sDisplayName = "Betty"
gzTextVar.zEntities[i].sQueryName = "betty"
gzTextVar.zEntities[i].sQueryPicture = "Root/Images/ElectrospriteAdv/Characters/NPCANormal"
gzTextVar.zEntities[i].sUniqueName = "betty"
gzTextVar.zEntities[i].sLocation = "Farm Field CC"
gzTextVar.zEntities[i].sAutoTopic = "betty"
gzTextVar.zEntities[i].bIsCrippled = false
gzTextVar.zEntities[i].sGenderA = "her"
gzTextVar.zEntities[i].sGenderB = "she"
gzTextVar.zEntities[i].sGenderCapitalA = "Her"
gzTextVar.zEntities[i].sGenderCapitalB = "She"

--Other images
gzTextVar.zEntities[i].sCoredPicture = "Root/Images/ElectrospriteAdv/Characters/NPCACored"
gzTextVar.zEntities[i].sTF0Picture = "Root/Images/ElectrospriteAdv/Characters/NPCATF0"
gzTextVar.zEntities[i].sTF1Picture = "Root/Images/ElectrospriteAdv/Characters/NPCATF1"
gzTextVar.zEntities[i].sTF2Picture = "Root/Images/ElectrospriteAdv/Characters/NPCATF2"
gzTextVar.zEntities[i].sGolemPicture = "Root/Images/ElectrospriteAdv/Characters/NPCAGolem"

--Command Handler
gzTextVar.zEntities[i].sCommandHandler = gzTextVar.sEntityHandlers .. "Betty.lua"
gzTextVar.zEntities[i].sAIHandler = gzTextVar.sRootPath .. "AIs/Human.lua"
gzTextVar.zEntities[i].iMoveIndex = -1
gzTextVar.zEntities[i].iPatrolIndex = 1
gzTextVar.zEntities[i].saPatrolList = {"Farm Field NW", "Farm Field SW", "Farm Field SE", "Farm Field NE"}

-- |[Clarissa]|
---NPC found wandering around the forest
gzTextVar.zEntitiesTotal = gzTextVar.zEntitiesTotal + 1

--Shorthand
i = gzTextVar.zEntitiesTotal

gzTextVar.zEntities[i] = {}
gzTextVar.zEntities[i].sDisplayName = "Clarissa"
gzTextVar.zEntities[i].sQueryName = "clarissa"
gzTextVar.zEntities[i].sQueryPicture = "Root/Images/ElectrospriteAdv/Characters/NPCBNormal"
gzTextVar.zEntities[i].sUniqueName = "clarissa"
gzTextVar.zEntities[i].sLocation = "Shrubland W"
gzTextVar.zEntities[i].sAutoTopic = "clarissa"
gzTextVar.zEntities[i].bIsCrippled = false
gzTextVar.zEntities[i].sGenderA = "her"
gzTextVar.zEntities[i].sGenderB = "she"
gzTextVar.zEntities[i].sGenderCapitalA = "Her"
gzTextVar.zEntities[i].sGenderCapitalB = "She"

--Other images
gzTextVar.zEntities[i].sCoredPicture = "Root/Images/ElectrospriteAdv/Characters/NPCBCored"
gzTextVar.zEntities[i].sTF0Picture = "Root/Images/ElectrospriteAdv/Characters/NPCBTF0"
gzTextVar.zEntities[i].sTF1Picture = "Root/Images/ElectrospriteAdv/Characters/NPCBTF1"
gzTextVar.zEntities[i].sTF2Picture = "Root/Images/ElectrospriteAdv/Characters/NPCBTF2"
gzTextVar.zEntities[i].sGolemPicture = "Root/Images/ElectrospriteAdv/Characters/NPCBGolem"

--Command Handler
gzTextVar.zEntities[i].sCommandHandler = gzTextVar.sEntityHandlers .. "Clarissa.lua"
gzTextVar.zEntities[i].sAIHandler = gzTextVar.sRootPath .. "AIs/Human.lua"
gzTextVar.zEntities[i].iMoveIndex = -1
gzTextVar.zEntities[i].iPatrolIndex = 1
gzTextVar.zEntities[i].saPatrolList = {"Shrubland NW", "Shrubland NE", "Forest Path E", "Forest Path W"}

-- |[Nancy]|
---NPC found in the cabin near the hills.
gzTextVar.zEntitiesTotal = gzTextVar.zEntitiesTotal + 1

--Shorthand
i = gzTextVar.zEntitiesTotal

gzTextVar.zEntities[i] = {}
gzTextVar.zEntities[i].sDisplayName = "Nancy"
gzTextVar.zEntities[i].sQueryName = "nancy"
gzTextVar.zEntities[i].sQueryPicture = "Root/Images/ElectrospriteAdv/Characters/NPCCNormal"
gzTextVar.zEntities[i].sUniqueName = "nancy"
gzTextVar.zEntities[i].sLocation = "Hill Cabin"
gzTextVar.zEntities[i].sAutoTopic = "nancy"
gzTextVar.zEntities[i].bIsCrippled = false
gzTextVar.zEntities[i].sGenderA = "her"
gzTextVar.zEntities[i].sGenderB = "she"
gzTextVar.zEntities[i].sGenderCapitalA = "Her"
gzTextVar.zEntities[i].sGenderCapitalB = "She"

--Other images
gzTextVar.zEntities[i].sCoredPicture = "Root/Images/ElectrospriteAdv/Characters/NPCCCored"
gzTextVar.zEntities[i].sTF0Picture = "Root/Images/ElectrospriteAdv/Characters/NPCCTF0"
gzTextVar.zEntities[i].sTF1Picture = "Root/Images/ElectrospriteAdv/Characters/NPCCTF1"
gzTextVar.zEntities[i].sTF2Picture = "Root/Images/ElectrospriteAdv/Characters/NPCCTF2"
gzTextVar.zEntities[i].sGolemPicture = "Root/Images/ElectrospriteAdv/Characters/NPCCGolem"

--Command Handler
gzTextVar.zEntities[i].sCommandHandler = gzTextVar.sEntityHandlers .. "Nancy.lua"
gzTextVar.zEntities[i].sAIHandler = gzTextVar.sRootPath .. "AIs/Human.lua"
gzTextVar.zEntities[i].iMoveIndex = -1
gzTextVar.zEntities[i].iPatrolIndex = 1
gzTextVar.zEntities[i].saPatrolList = {"Hill Cabin"}

-- |[Noreen]|
---NPC found in the farmhouse.
gzTextVar.zEntitiesTotal = gzTextVar.zEntitiesTotal + 1

--Shorthand
i = gzTextVar.zEntitiesTotal

gzTextVar.zEntities[i] = {}
gzTextVar.zEntities[i].sDisplayName = "Noreen"
gzTextVar.zEntities[i].sQueryName = "noreen"
gzTextVar.zEntities[i].sQueryPicture = "Root/Images/ElectrospriteAdv/Characters/NPCDNormal"
gzTextVar.zEntities[i].sUniqueName = "noreen"
gzTextVar.zEntities[i].sLocation = "Farmhouse"
gzTextVar.zEntities[i].sAutoTopic = "noreen"
gzTextVar.zEntities[i].bIsCrippled = false
gzTextVar.zEntities[i].sGenderA = "her"
gzTextVar.zEntities[i].sGenderB = "she"
gzTextVar.zEntities[i].sGenderCapitalA = "Her"
gzTextVar.zEntities[i].sGenderCapitalB = "She"

--Other images
gzTextVar.zEntities[i].sCoredPicture = "Root/Images/ElectrospriteAdv/Characters/NPCDCored"
gzTextVar.zEntities[i].sTF0Picture = "Root/Images/ElectrospriteAdv/Characters/NPCDTF0"
gzTextVar.zEntities[i].sTF1Picture = "Root/Images/ElectrospriteAdv/Characters/NPCDTF1"
gzTextVar.zEntities[i].sTF2Picture = "Root/Images/ElectrospriteAdv/Characters/NPCDTF2"
gzTextVar.zEntities[i].sGolemPicture = "Root/Images/ElectrospriteAdv/Characters/NPCDGolem"

--Command Handler
gzTextVar.zEntities[i].sCommandHandler = gzTextVar.sEntityHandlers .. "Noreen.lua"
gzTextVar.zEntities[i].sAIHandler = gzTextVar.sRootPath .. "AIs/Human.lua"
gzTextVar.zEntities[i].iMoveIndex = -1
gzTextVar.zEntities[i].iPatrolIndex = 1
gzTextVar.zEntities[i].saPatrolList = {"Farmhouse"}

--[ ==================================== Object Registration ==================================== ]
--Registration function.
fnRegisterObject = function(sRoomName, sObjectUniqueName, sObjectDisplayName, bCanBePickedUp, sHandlerScript)
    
    --Argument Check
    if(sRoomName == nil) then return end
    if(sObjectUniqueName == nil) then return end
    if(sObjectDisplayName == nil) then return end
    if(bCanBePickedUp == nil) then return end
    
    --Find the room in question.
    local iRoom = -1
    for i = 1, gzTextVar.iRoomsTotal, 1 do
        if(gzTextVar.zRoomList[i].sName == sRoomName) then
            iRoom = i
            break
        end
    end
    if(iRoom == -1) then return end
    
    --Increment object count.
    gzTextVar.zRoomList[iRoom].iObjectsTotal = gzTextVar.zRoomList[iRoom].iObjectsTotal + 1
    local p = gzTextVar.zRoomList[iRoom].iObjectsTotal
    
    --Create the object.
    gzTextVar.zRoomList[iRoom].zObjects[p] = {}
    gzTextVar.zRoomList[iRoom].zObjects[p].sUniqueName = sObjectUniqueName
    gzTextVar.zRoomList[iRoom].zObjects[p].sDisplayName = sObjectDisplayName
    gzTextVar.zRoomList[iRoom].zObjects[p].bCanBePickedUp = bCanBePickedUp
    gzTextVar.zRoomList[iRoom].zObjects[p].sHandlerScript = sHandlerScript
end

--Decorative Objects
fnRegisterObject("Headquarters", "conversionchamber", "conversion chamber", false, gzTextVar.sExamineHandler)

--Items
fnRegisterObject("Riverside Path N", "potion", "potion", true,  gzTextVar.sRootPath .. "Item Handlers/Health Potion.lua")
fnRegisterObject("Cave Rear A",      "potion", "potion", true,  gzTextVar.sRootPath .. "Item Handlers/Health Potion.lua")
fnRegisterObject("Deep Forest W",    "potion", "potion", true,  gzTextVar.sRootPath .. "Item Handlers/Health Potion.lua")
fnRegisterObject("Farmhouse",        "potion", "potion", true,  gzTextVar.sRootPath .. "Item Handlers/Health Potion.lua")
fnRegisterObject("Headquarters",     "potion", "potion", true,  gzTextVar.sRootPath .. "Item Handlers/Health Potion.lua")
fnRegisterObject("Headquarters",     "potion", "potion", true,  gzTextVar.sRootPath .. "Item Handlers/Health Potion.lua")

--[Visibility Builder]
--Build what objects are visible.
fnRebuildEntityVisibility()

--[Locality Builder]
--Builds initial locality information.
fnBuildLocalityInfo()
