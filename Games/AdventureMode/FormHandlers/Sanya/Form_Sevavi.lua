-- |[ ====================================== Sanya Sevavi ====================================== ]|
--Script called when Sanya shapeshifts to Sevavi at a save point.

-- |[Script Variables]|
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Sanya/sForm", "S", "Sevavi")
VM_SetVar("Root/Variables/Global/Sanya/iHasSevaviForm", "N", 1.0)

-- |[World]|
--Any enemies on the map of this type should now ignore the party.
AL_PulseIgnore("Sevavi")

-- |[Combat Statistics]|
--Change class.
AdvCombat_SetProperty("Push Party Member", "Sanya")
    AdvCombatEntity_SetProperty("Active Job", "Agontea")
DL_PopActiveObject()
    
-- |[Werebat TF Remover]|
--Reset the werebat TF stage if it has not completed before.
local iWerebatStage = VM_GetVar("Root/Variables/Chapter2/WerebatTF/iWerebatStage", "N")
if(iWerebatStage < gciWerebatTF_FullyTFd and iWerebatStage > gciWerebatTF_Uninfected) then
    
    --Reset the variable.
    VM_SetVar("Root/Variables/Chapter2/WerebatTF/iWerebatStage", "N", gciWerebatTF_Uninfected)
    
    --Reset the costume.
    VM_SetVar("Root/Variables/Costumes/Sanya/sCostumeHuman", "S", "Rifle")
    LM_ExecuteScript(gsCharacterAutoresolve, "Sanya")
end
