-- |[ ======================================= Sanya Bunny ====================================== ]|
--Script called when Sanya shapeshifts to Bunny at a save point.

-- |[Script Variables]|
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Sanya/sForm", "S", "Bunny")
VM_SetVar("Root/Variables/Global/Sanya/iHasBunnyForm", "N", 1.0)

-- |[World]|
--Any enemies on the map of this type should now ignore the party.
AL_PulseIgnore("Bunny")

-- |[Combat Statistics]|
--Change class.
AdvCombat_SetProperty("Push Party Member", "Sanya")
    AdvCombatEntity_SetProperty("Active Job", "Scofflaw")
DL_PopActiveObject()
