-- |[ ======================================= Sanya Harpy ====================================== ]|
--Script called when Sanya shapeshifts to Harpy at a save point.

-- |[Script Variables]|
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Sanya/sForm", "S", "Harpy")
VM_SetVar("Root/Variables/Global/Sanya/iHasHarpyForm", "N", 1.0)

-- |[World]|
--Any enemies on the map of this type should now ignore the party.
AL_PulseIgnore("Harpy")

-- |[Combat Statistics]|
--Change class.
AdvCombat_SetProperty("Push Party Member", "Sanya")
    AdvCombatEntity_SetProperty("Active Job", "Ranger")
DL_PopActiveObject()
