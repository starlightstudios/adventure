-- |[ ===================================== Sanya Werebat ====================================== ]|
--Script called when Sanya shapeshifts to Werebat at a save point.

-- |[Script Variables]|
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Sanya/sForm", "S", "Werebat")
VM_SetVar("Root/Variables/Global/Sanya/iHasWerebatForm", "N", 1.0)

-- |[World]|
--Any enemies on the map of this type should now ignore the party.
AL_PulseIgnore("Werebat")

-- |[Combat Statistics]|
--Change class.
AdvCombat_SetProperty("Push Party Member", "Sanya")
    AdvCombatEntity_SetProperty("Active Job", "Skystriker")
DL_PopActiveObject()
