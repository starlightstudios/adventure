-- |[ ====================================== Sanya Human ======================================= ]|
--Script called when Sanya shapeshifts to Human at a save point.

-- |[Script Variables]|
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Sanya/sForm", "S", "Human")

-- |[World]|
--Any enemies on the map of this type should now ignore Mei.
AL_PulseIgnore("Human")

-- |[Combat Statistics]|
--Change class.
AdvCombat_SetProperty("Push Party Member", "Sanya")
    AdvCombatEntity_SetProperty("Active Job", "Hunter")
DL_PopActiveObject()
