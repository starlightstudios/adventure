-- |[ ================================== SX-399 Shocktrooper =================================== ]|
--Script called when SX-399 changes jobs to Shocktrooper.

-- |[Combat Statistics]|
--Change Florentina's class. This will automatically handle sprite changes, and marks the active
-- job in the script variables.
AdvCombat_SetProperty("Push Party Member", "SX-399")
    AdvCombatEntity_SetProperty("Active Job", "Shocktrooper")
DL_PopActiveObject()
