-- |[ ===================================== Mei Mannequin ====================================== ]|
--Script called when Mei shapeshifts to Mannequin at a save point.

-- |[Script Variables]|
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Mei/sForm", "S", "Mannequin")
VM_SetVar("Root/Variables/Global/Mei/iHasMannequinForm", "N", 1.0)

-- |[Achievement]|
--AM_SetPropertyJournal("Unlock Achievement", "BecomeWerecat")

-- |[World]|
--Any enemies on the map of this type should now ignore Mei.
--AL_PulseIgnore("Wisphag")

-- |[Combat Statistics]|
--Change Mei's class, this automatically changes her appearance.
AdvCombat_SetProperty("Push Party Member", "Mei")
    AdvCombatEntity_SetProperty("Active Job", "Stalker")
DL_PopActiveObject()
