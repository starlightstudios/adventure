-- |[ ========================================= Mei Bee ======================================== ]|
--Script called when Mei shapeshifts to Bee at a save point.

-- |[Script Variables]|
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Mei/sForm", "S", "Bee")
VM_SetVar("Root/Variables/Global/Mei/iHasBeeForm", "N", 1.0)

-- |[Achievement]|
AM_SetPropertyJournal("Unlock Achievement", "BecomeBee")

-- |[World]|
--Any enemies on the map of this type should now ignore Mei.
AL_PulseIgnore("Bee")

-- |[Combat Statistics]|
--Change Mei's class to Bee. This automatically changes appearance.
AdvCombat_SetProperty("Push Party Member", "Mei")
    AdvCombatEntity_SetProperty("Active Job", "Hive Scout")
DL_PopActiveObject()

-- |[Badge Check]|
--If the player doesn't have the slime badge yet, add it to the inventory.
local sBadgeName = "Bee Vanguard Badge"
local iBadgeCount = AdInv_GetProperty("Item Count Plus Equip", sBadgeName)
if(iBadgeCount < 1) then
    LM_ExecuteScript(gsItemListing, sBadgeName)
end
