-- |[ ======================================= Mei Zombee ======================================= ]|
--Called when Mei transforms into a Zombee.

-- |[Script Variables]|
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Mei/sForm", "S", "Zombee")
VM_SetVar("Root/Variables/Global/Mei/iHasZombeeForm", "N", 1.0)

-- |[Achievement]|
AM_SetPropertyJournal("Unlock Achievement", "BecomeZombee")

-- |[World]|
--Any enemies on the map of this type should now ignore Mei.
AL_PulseIgnore("Zombee")

-- |[Combat Statistics]|
--Change Mei's class to Bee. This automatically changes appearance.
AdvCombat_SetProperty("Push Party Member", "Mei")
    AdvCombatEntity_SetProperty("Active Job", "Zombee")
DL_PopActiveObject()
