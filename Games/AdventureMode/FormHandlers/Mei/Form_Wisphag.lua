-- |[ ======================================= Mei Wisphag ====================================== ]|
--Script called when Mei shapeshifts to Wisphag at a save point.

-- |[Script Variables]|
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Mei/sForm", "S", "Wisphag")
VM_SetVar("Root/Variables/Global/Mei/iHasWisphagForm", "N", 1.0)

-- |[Achievement]|
AM_SetPropertyJournal("Unlock Achievement", "BecomeWisphag")

-- |[World]|
--Any enemies on the map of this type should now ignore Mei.
AL_PulseIgnore("Wisphag")

-- |[Combat Statistics]|
--Change Mei's class, this automatically changes her appearance.
AdvCombat_SetProperty("Push Party Member", "Mei")
    AdvCombatEntity_SetProperty("Active Job", "Soulherd")
DL_PopActiveObject()

-- |[Badge Check]|
--If the player doesn't have the slime badge yet, add it to the inventory.
local sBadgeName = "Burning Soul Badge"
local iBadgeCount = AdInv_GetProperty("Item Count Plus Equip", sBadgeName)
if(iBadgeCount < 1) then
    LM_ExecuteScript(gsItemListing, sBadgeName)
end
