-- |[ ======================================== Mei Slime ======================================= ]|
--Script called when Mei shapeshifts to Slime at a save point.

-- |[Script Variables]|
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Mei/sForm", "S", "Slime")
VM_SetVar("Root/Variables/Global/Mei/iHasSlimeForm", "N", 1.0)

-- |[Achievement]|
AM_SetPropertyJournal("Unlock Achievement", "BecomeSlime")

-- |[World]|
--Any enemies on the map of this type should now ignore Mei.
AL_PulseIgnore("Slime")

-- |[Combat Statistics]|
--Change Mei's class to Bee. This automatically changes appearance.
AdvCombat_SetProperty("Push Party Member", "Mei")
    AdvCombatEntity_SetProperty("Active Job", "Smarty Sage")
DL_PopActiveObject()

-- |[Badge Check]|
--If the player doesn't have the slime badge yet, add it to the inventory.
local sBadgeName = "Slime Pal Badge"
local iBadgeCount = AdInv_GetProperty("Item Count Plus Equip", sBadgeName)
if(iBadgeCount < 1) then
    LM_ExecuteScript(gsItemListing, sBadgeName)
end