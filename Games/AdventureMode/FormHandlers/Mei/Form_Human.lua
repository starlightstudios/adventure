-- |[ ======================================== Mei Human ======================================= ]|
--Script called when Mei shapeshifts to Human at a save point.

-- |[Script Variables]|
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Mei/sForm", "S", "Human")

-- |[World]|
--Any enemies on the map of this type should now ignore Mei.
AL_PulseIgnore("Human")

-- |[Combat Statistics]|
--Change Mei's class to Bee. This automatically changes appearance.
AdvCombat_SetProperty("Push Party Member", "Mei")
    AdvCombatEntity_SetProperty("Active Job", "Fencer")
DL_PopActiveObject()
