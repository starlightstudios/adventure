-- |[ ======================================= Mei Rubber ======================================= ]|
--Script called when Mei shapeshifts into Rubber.

-- |[Script Variables]|
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Mei/sForm", "S", "Rubber")
VM_SetVar("Root/Variables/Global/Mei/iHasRubberForm", "N", 1.0)

-- |[Achievement]|
AM_SetPropertyJournal("Unlock Achievement", "BecomeRubber")

-- |[World]|
--Any enemies on the map of this type should now ignore Mei.
AL_PulseIgnore("Rubber")

-- |[Combat Statistics]|
--Change Mei's class to Bee. This automatically changes appearance.
AdvCombat_SetProperty("Push Party Member", "Mei")
    AdvCombatEntity_SetProperty("Active Job", "Squeaky Thrall")
DL_PopActiveObject()
