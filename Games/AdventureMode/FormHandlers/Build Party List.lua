-- |[ ==================================== Build Party List ==================================== ]|
--Builds a list of all party members that can transform when the menu is selected. While this is almost
-- always just the runebearers in the party, there are a couple of exception cases.
--It is assumed the party list is cleared right before calling this.

-- |[Blockers]|
--These prevent transformation in certain cases.
local iIsRubberMode = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
if(iIsRubberMode == 1.0) then return end

--Being in "Nowhere" means we can never transform.
local sLevelName = AL_GetProperty("Name")
if(sLevelName == "Nowhere") then return end

--If the number of party members is nil, the game hasn't started yet. Stop.
if(giFollowersTotal == nil) then return end

-- |[Party Members]|
--Build a general-purpose list.
local iPartyListSize = 0
local sPartyList = {}

--Main character becomes the zero entry.
iPartyListSize = iPartyListSize + 1
sPartyList[iPartyListSize] = {gsPartyLeaderName, "Null"}

--Run through the follower listing, add the followers to the end of the list.
for i = 1, giFollowersTotal, 1 do
    iPartyListSize = iPartyListSize + 1
    sPartyList[iPartyListSize] = {gsaFollowerNames[i], "Null"}
end

-- |[ =================================== Verification Check =================================== ]|
--At least one party member must have at least one form, otherwise, we stop here. This will cause
-- the forms menu to not appear on the campfire menu.
local bAtLeastOneForm = false

--Iterate.
for i = 1, iPartyListSize, 1 do
    
    -- |[Christine]|
    if(sPartyList[i][1] == "Christine") then
        local sVarPath = "Root/Variables/Global/Christine/"
        local saVars = {"iHasDollForm", "iHasGolemForm", "iHasLatexForm", "iHasDarkmatterForm", "iHasEldritchForm", "iHasElectrospriteForm", "iHasSteamDroidForm", "iHasRaijuForm", "iHasSecrebotForm"}
        for p = 1, #saVars, 1 do
            if(VM_GetVar(sVarPath .. saVars[p], "N") == 1.0) then
                bAtLeastOneForm = true
                break
            end
        end
    
    -- |[Izuna]|
    elseif(sPartyList[i][1] == "Izuna") then
    
    -- |[JX-101]|
    elseif(sPartyList[i][1] == "JX-101") then
    
    -- |[Florentina]|
    elseif(sPartyList[i][1] == "Florentina") then
        local sVarPath = "Root/Variables/Global/Florentina/"
        local saVars = {"iHasJob_Mediator", "iHasJob_TreasureHunter", "iHasJob_Agarist", "iHasJob_Lurker"}
        for p = 1, #saVars, 1 do
            if(VM_GetVar(sVarPath .. saVars[p], "N") == 1.0) then
                bAtLeastOneForm = true
                break
            end
        end
    
    -- |[Mei]|
    elseif(sPartyList[i][1] == "Mei") then
        local sVarPath = "Root/Variables/Global/Mei/"
        local saVars = {"iHasAlrauneForm", "iHasBeeForm", "iHasGhostForm", "iHasGravemarkerForm", "iHasMannequinForm", "iHasSlimeForm", "iHasWerecatForm", "iHasWisphagForm"}
        for p = 1, #saVars, 1 do
            if(VM_GetVar(sVarPath .. saVars[p], "N") == 1.0) then
                bAtLeastOneForm = true
                break
            end
        end
        
    -- |[Sanya]|
    elseif(sPartyList[i][1] == "Sanya") then
        local sVarPath = "Root/Variables/Global/Sanya/"
        local saVars = {"iHasHarpyForm", "iHasBunnyForm", "iHasSevaviForm", "iHasWerebatForm"}
        for p = 1, #saVars, 1 do
            if(VM_GetVar(sVarPath .. saVars[p], "N") == 1.0) then
                bAtLeastOneForm = true
                break
            end
        end
        
    -- |[SX-399]|
    elseif(sPartyList[i][1] == "SX-399") then
        local sVarPath = "Root/Variables/Global/SX-399/"
        local saVars = {"iHasJob_Shocktrooper"}
        for p = 1, #saVars, 1 do
            if(VM_GetVar(sVarPath .. saVars[p], "N") == 1.0) then
                bAtLeastOneForm = true
                break
            end
        end
    
    -- |[Tiffany]|
    elseif(sPartyList[i][1] == "Tiffany") then
        local sVarPath = "Root/Variables/Global/Tiffany/"
        local saVars = {"iHasJob_Assault", "iHasJob_Support", "iHasJob_Subvert", "iHasJob_Spearhead"}
        for p = 1, #saVars, 1 do
            if(VM_GetVar(sVarPath .. saVars[p], "N") == 1.0) then
                bAtLeastOneForm = true
                break
            end
        end

    end
    if(bAtLeastOneForm) then
        break
    end
end

--No forms, stop.
if(bAtLeastOneForm == false) then return end

-- |[ ============================ Image Crossreference ============================ ]|
--This is a list of all entries for all characters, indicating what image they should use. Characters who can transform
-- use logic to resolve it.
--Character may be marked with "DELIST" which orders the list to ignore them.

-- |[Icon List]|
--Constants
local sDLPath = "Root/Images/AdventureUI/FormIcons/"

--Master List
local zaFormMasterList = {}

-- |[Character Adder]|
local fnAddCharacter = function(psName, psFormPath, saIconNames)
    local zEntry = {}
    zEntry.sName = psName
    zEntry.saIcons = saIconNames
    table.insert(zaFormMasterList, zEntry)
    
    --Resolve form. If it begins with "ALWAYS|" just use that as the form.
    if(string.sub(psFormPath, 1, 7) == "ALWAYS|") then
        zEntry.sForm = string.sub(psFormPath, 8)
        
    --Resolve.
    else
        zEntry.sForm = VM_GetVar(psFormPath, "S")
    end
end

-- |[Characters]|
fnAddCharacter("Mei",        "Root/Variables/Global/Mei/sForm",              {"Human", "Alraune", "Bee", "Ghost", "Gravemarker", "Mannequin", "Slime", "Werecat", "Wisphag"})
fnAddCharacter("Sanya",      "Root/Variables/Global/Sanya/sForm",            {"Human", "Kitsune", "Sevavi", "Werebat", "Bunny", "Harpy"})
fnAddCharacter("Jeanne",     "Null",                                         {"NULL"})
fnAddCharacter("Lotta",      "Null",                                         {"NULL"})
fnAddCharacter("Christine",  "Root/Variables/Global/Christine/sForm",        {"Human", "Golem", "LatexDrone", "Raiju", "SteamDroid", "Electrosprite", "Eldritch", "Darkmatter", "Doll", "Secrebot"})
fnAddCharacter("Talia",      "Null",                                         {"NULL"})
fnAddCharacter("Florentina", "Root/Variables/Global/Florentina/sCurrentJob", {"Merchant", "Mediator", "TreasureHunter", "Agarist", "Lurker"})
fnAddCharacter("Maram",      "Null",                                         {"NULL"})
fnAddCharacter("Izuna",      "Root/Variables/Global/Izuna/sCurrentJob",      {"Miko"})
fnAddCharacter("Empress",    "Root/Variables/Global/Empress/sCurrentJob",    {"Conquerer"})
fnAddCharacter("Zeke",       "ALWAYS|Goat",                                  {"Goat"})
fnAddCharacter("Aquillia",   "Null",                                         {"NULL"})
fnAddCharacter("Gallena",    "Null",                                         {"NULL"})
fnAddCharacter("Junia",      "Null",                                         {"NULL"})
fnAddCharacter("Edea",       "Null",                                         {"NULL"})
fnAddCharacter("Tiffany",    "Root/Variables/Global/Tiffany/sCurrentJob",    {"Assault", "Support", "Subvert", "Spearhead"})
fnAddCharacter("SX-399",     "Root/Variables/Global/SX-399/sCurrentJob",     {"Shocktrooper"})

-- |[Data Upload]|
--Now upload the data.
for i = 1, iPartyListSize, 1 do
    
    --If the follower is not a party member, don't add them. Only combat-ready characters have entries here.
    local sDisplayName = sPartyList[i][1]
    if(AdvCombat_GetProperty("Does Party Member Exist", sDisplayName) == true) then
    
        --Get the party member's display name.
        AdvCombat_SetProperty("Push Party Member", sPartyList[i][1])
            sDisplayName = AdvCombatEntity_GetProperty("Display Name")
        DL_PopActiveEntity()
        
        --Scan the master list to resolve the image.
        local sImagePath = "Null"
        for p = 1, #zaFormMasterList, 1 do
            
            --Match found.
            if(zaFormMasterList[p].sName == sPartyList[i][1]) then
                
                --Variables.
                local sCharacterName = zaFormMasterList[p].sName
                local sCharacterForm = zaFormMasterList[p].sForm
                
                --Scan the list of images.
                for o = 1, #zaFormMasterList[p].saIcons, 1 do
                
                    --If the image name matches the form name, we have a valid entry.
                    if(zaFormMasterList[p].saIcons[o] == sCharacterForm) then
                        sImagePath = sDLPath .. sCharacterName .. sCharacterForm
                        break
                    end
                end
            
                --Stop iteration on the master list.
                break
            end
        end
    
        --Set.
        AM_SetProperty("Register Form Member", sPartyList[i][1], sDisplayName, sImagePath)
        AM_SetProperty("Set Form Member Alignments", sPartyList[i][1], 0, 0, 0, 0, 90, 90)
    end
end
