-- |[ ======================================= Izuna Miko ======================================= ]|
--Script called when Izuna changes jobs to Miko.

-- |[Combat Statistics]|
--Change Izuna's class. This will automatically handle sprite changes, and marks the active
-- job in the script variables.
AdvCombat_SetProperty("Push Party Member", "Izuna")
    AdvCombatEntity_SetProperty("Active Job", "Miko")
DL_PopActiveObject()
