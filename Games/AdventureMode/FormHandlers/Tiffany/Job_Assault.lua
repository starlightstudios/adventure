-- |[ ===================================== Tiffany Assault ==================================== ]|
--Script called when Tiffany changes jobs to Assault.

-- |[Combat Statistics]|
--Change characters's class. This will automatically handle sprite changes, and marks the active
-- job in the script variables.
AdvCombat_SetProperty("Push Party Member", "Tiffany")
    AdvCombatEntity_SetProperty("Active Job", "Assault")
DL_PopActiveObject()
