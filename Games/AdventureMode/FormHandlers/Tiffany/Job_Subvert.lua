-- |[ ===================================== Tiffany Subvert ==================================== ]|
--Script called when Tiffany changes jobs to Subvert.

-- |[Combat Statistics]|
--Change character's class. This will automatically handle sprite changes, and marks the active
-- job in the script variables.
AdvCombat_SetProperty("Push Party Member", "Tiffany")
    AdvCombatEntity_SetProperty("Active Job", "Subvert")
DL_PopActiveObject()
