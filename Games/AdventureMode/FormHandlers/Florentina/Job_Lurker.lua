-- |[ ==================================== Florentina Lurker =================================== ]|
--Script called when Florentina changes jobs to Lurker.

-- |[Flag]|
VM_SetVar("Root/Variables/Global/Florentina/iHasJob_Lurker", "N", 1.0)

-- |[Combat Statistics]|
--Change Florentina's class. This will automatically handle sprite changes, and marks the active
-- job in the script variables.
AdvCombat_SetProperty("Push Party Member", "Florentina")
    AdvCombatEntity_SetProperty("Active Job", "Lurker")
DL_PopActiveObject()
