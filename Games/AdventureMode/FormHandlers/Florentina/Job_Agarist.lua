-- |[ =================================== Florentina Agarist =================================== ]|
--Script called when Florentina changes jobs to Agarist.

-- |[Flag]|
VM_SetVar("Root/Variables/Global/Florentina/iHasJob_Agarist", "N", 1.0)

-- |[Combat Statistics]|
--Change Florentina's class. This will automatically handle sprite changes, and marks the active
-- job in the script variables.
AdvCombat_SetProperty("Push Party Member", "Florentina")
    AdvCombatEntity_SetProperty("Active Job", "Agarist")
DL_PopActiveObject()
