-- |[ ===================================== Build Form List ==================================== ]|
--Builds the list of forms that can be accessed from the debug manager. These are in the format of "[Charactername]/Form_[Formname]".
-- Each form handler is supposed to operate on one character.
local saMeiList = {"Mei/Form_Alraune", "Mei/Form_Bee", "Mei/Form_Ghost", "Mei/Form_Gravemarker", "Mei/Form_Human", "Mei/Form_Rubber", "Mei/Form_Mannequin", "Mei/Form_Slime", "Mei/Form_Werecat", 
                   "Mei/Form_Wisphag", "Mei/Form_Zombee"}
local saFlorentinaList = {"Florentina/Job_Merchant", "Florentina/Job_Mediator", "Florentina/Job_TreasureHunter", "Florentina/Job_Agarist", "Florentina/Job_Lurker"}
local saChristineList = {"Christine/Form_Human", "Christine/Form_Darkmatter", "Christine/Form_Eldritch", "Christine/Form_Electro", "Christine/Form_Golem", "Christine/Form_Latex", "Christine/Form_SteamDroid", "Christine/Form_Raiju", "Christine/Form_Doll", "Christine/Form_Secrebot"}
local saTiffanyList = {"Tiffany/Job_Assault", "Tiffany/Job_Support", "Tiffany/Job_Subvert", "Tiffany/Job_Spearhead"}
local saSanyaList = {"Sanya/Form_Bunny", "Sanya/Form_Harpy", "Sanya/Form_Human", "Sanya/Form_Sevavi", "Sanya/Form_Werebat"}

-- |[Chapter Specification]|
saListList = {}
local iCurrentChapter = VM_GetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N")
if(iCurrentChapter == 1.0) then
    saListList[1] = saMeiList
    saListList[2] = saFlorentinaList

elseif(iCurrentChapter == 2.0) then
    saListList[1] = saSanyaList
    
elseif(iCurrentChapter == 3.0) then
elseif(iCurrentChapter == 4.0) then
elseif(iCurrentChapter == 5.0) then
    saListList[1] = saChristineList
    saListList[2] = saTiffanyList
    
elseif(iCurrentChapter == 6.0) then
end

-- |[Mods]|
--Call any mod scripts and add to the list.
fnExecModScript("FormHandlers/Build Form List.lua")

-- |[Count]|
--Sum.
local iTotalForms = 0
for i = 1, #saListList, 1 do
    iTotalForms = iTotalForms + #saListList[i]
end

--Allocate forms.
ADebug_SetProperty("Forms Total", iTotalForms)

-- |[Upload]|
--Set.
local o = 1
for i = 1, #saListList, 1 do
    for p = 1, #saListList[i], 1 do
        ADebug_SetProperty("Form Path", o-1, saListList[i][p])
        o = o + 1
    end
end

-- |[Clean]|
saListList = nil
