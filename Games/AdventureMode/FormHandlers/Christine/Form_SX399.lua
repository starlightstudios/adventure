-- |[ ==================================== Christine SX-399 ==================================== ]|
--Used during a cutscene where Christine impersonates SX-399.

-- |[Script Variables]|
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Christine/sForm", "S", "SteamDroid")
VM_SetVar("Root/Variables/Global/Christine/iHasSteamDroidForm", "N", 1.0)

-- |[World]|
--Any enemies on the map of this type should now ignore the player.
AL_PulseIgnore("SteamDroid")

-- |[Combat Statistics]|
--Special: Override Christine's costume so she looks like SX-399.
VM_SetVar("Root/Variables/Costumes/Christine/sCostumeSteamDroid", "S", "SX-399")

--Change character class.
AdvCombat_SetProperty("Push Party Member", "Christine")
    AdvCombatEntity_SetProperty("Active Job", "Brass Brawler")
DL_PopActiveObject()