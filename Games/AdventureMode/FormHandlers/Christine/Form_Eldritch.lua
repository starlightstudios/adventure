-- |[ =============================== Christine Eldritch Dreamer =============================== ]|
--Called when a character shapeshifts.

-- |[Script Variables]|
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Christine/sForm", "S", "Eldritch")
VM_SetVar("Root/Variables/Global/Christine/iHasEldritchForm", "N", 1.0)
        
-- |[Achievement]|
AM_SetPropertyJournal("Unlock Achievement", "BecomeDreamer")

-- |[World]|
--Any enemies on the map of this type should now ignore the player.
AL_PulseIgnore("Human")

-- |[Combat Statistics]|
--Change character class.
AdvCombat_SetProperty("Push Party Member", "Christine")
    AdvCombatEntity_SetProperty("Active Job", "Handmaiden")
DL_PopActiveObject()
