-- |[ ===================================== Christine Golem ==================================== ]|
--Called when a character shapeshifts.

-- |[Script Variables]|
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Christine/sForm", "S", "Golem")
VM_SetVar("Root/Variables/Global/Christine/iHasGolemForm", "N", 1.0)
        
-- |[Achievement]|
AM_SetPropertyJournal("Unlock Achievement", "BecomeGolem")

-- |[World]|
--Any enemies on the map of this type should now ignore the player.
AL_PulseIgnore("Golem")

-- |[Combat Statistics]|
--Change character class.
AdvCombat_SetProperty("Push Party Member", "Christine")
    AdvCombatEntity_SetProperty("Active Job", "Repair Unit")
DL_PopActiveObject()
