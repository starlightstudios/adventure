-- |[ ==================================== Christine Human ===================================== ]|
--Called when a character shapeshifts.

-- |[Script Variables]|
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Christine/sForm", "S", "Human")
--VM_SetVar("Root/Variables/Global/Christine/iHasHumanForm", "N", 1.0)

-- |[World]|
--Any enemies on the map of this type should now ignore the player.
AL_PulseIgnore("Human")

-- |[Combat Statistics]|
--Change Mei's class to Bee. This automatically changes appearance.
AdvCombat_SetProperty("Push Party Member", "Christine")
    AdvCombatEntity_SetProperty("Active Job", "Lancer")
DL_PopActiveObject()
