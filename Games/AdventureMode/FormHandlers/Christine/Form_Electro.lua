-- |[ ================================= Christine Electrosprite ================================ ]|
--Called when a character shapeshifts.

-- |[Script Variables]|
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Christine/sForm", "S", "Electrosprite")
VM_SetVar("Root/Variables/Global/Christine/iHasElectrospriteForm", "N", 1.0)
        
-- |[Achievement]|
AM_SetPropertyJournal("Unlock Achievement", "BecomeElectro")

-- |[World]|
--Any enemies on the map of this type should now ignore the player.
AL_PulseIgnore("Electrosprite")

-- |[Combat Statistics]|
--Change character class.
AdvCombat_SetProperty("Push Party Member", "Christine")
    AdvCombatEntity_SetProperty("Active Job", "Live Wire")
DL_PopActiveObject()
