-- |[ ================================== Christine Darkmatter ================================== ]|
--Called when a character shapeshifts.

-- |[Script Variables]|
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Christine/sForm", "S", "Darkmatter")
VM_SetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N", 1.0)
        
-- |[Achievement]|
AM_SetPropertyJournal("Unlock Achievement", "BecomeDarkmatter")

-- |[World]|
--Any enemies on the map of this type should now ignore the player.
AL_PulseIgnore("Darkmatter")

-- |[Combat Statistics]|
--Change character class.
AdvCombat_SetProperty("Push Party Member", "Christine")
    AdvCombatEntity_SetProperty("Active Job", "Starseer")
DL_PopActiveObject()

-- |[Badge Check]|
--If the player doesn't have the slime badge yet, add it to the inventory.
local sBadgeName = "Voidguard Badge"
local iBadgeCount = AdInv_GetProperty("Item Count Plus Equip", sBadgeName)
if(iBadgeCount < 1) then
    LM_ExecuteScript(gsItemListing, sBadgeName)
end
