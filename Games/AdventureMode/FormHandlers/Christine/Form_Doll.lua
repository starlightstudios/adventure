-- |[ ===================================== Christine Doll ===================================== ]|
--Called when a character shapeshifts.

-- |[Script Variables]|
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Christine/sForm", "S", "Doll")
VM_SetVar("Root/Variables/Global/Christine/iHasDollForm", "N", 1.0)
        
-- |[Achievement]|
AM_SetPropertyJournal("Unlock Achievement", "BecomeDoll")

-- |[World]|
--Any enemies on the map of this type should now ignore the player.
AL_PulseIgnore("Doll")

-- |[Combat Statistics]|
--Change character class.
AdvCombat_SetProperty("Push Party Member", "Christine")
    AdvCombatEntity_SetProperty("Active Job", "Commissar")
DL_PopActiveObject()

-- |[Topic]|
WD_SetProperty("Unlock Topic", "Doll", 1)
