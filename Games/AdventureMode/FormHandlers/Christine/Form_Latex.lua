-- |[ ================================= Christine Latex Drone ================================== ]|
--Called when a character shapeshifts.

-- |[Script Variables]|
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Christine/sForm", "S", "LatexDrone")
VM_SetVar("Root/Variables/Global/Christine/iHasLatexForm", "N", 1.0)
        
-- |[Achievement]|
AM_SetPropertyJournal("Unlock Achievement", "BecomeLatex")
WD_SetProperty("Unlock Topic", "LatexDrones", 1)
WD_SetProperty("Unlock Topic", "Latex Drones", 1)
WD_SetProperty("Unlock Topic", "Inhibitors", 1)

-- |[World]|
--Any enemies on the map of this type should now ignore the player.
AL_PulseIgnore("Golem")

-- |[Combat Statistics]|
--Change character class.
AdvCombat_SetProperty("Push Party Member", "Christine")
    AdvCombatEntity_SetProperty("Active Job", "Drone")
DL_PopActiveObject()
