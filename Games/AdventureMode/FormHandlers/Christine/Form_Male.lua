-- |[ ================================= Christine Human (Male) ================================= ]|
--Used very briefly at the start of chapter 5.

-- |[Script Variables]|
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Christine/sForm", "S", "Male")

-- |[World]|
--Any enemies on the map of this type should now ignore the player.
AL_PulseIgnore("Human")

-- |[Combat Statistics]|
--Change character class.
AdvCombat_SetProperty("Push Party Member", "Christine")
    AdvCombatEntity_SetProperty("Active Job", "Teacher")
DL_PopActiveObject()
