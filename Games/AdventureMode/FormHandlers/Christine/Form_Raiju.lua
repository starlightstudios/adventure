-- |[ ==================================== Christine Raiju ===================================== ]|
--Called when a character shapeshifts.

-- |[Script Variables]|
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Christine/sForm", "S", "Raiju")
VM_SetVar("Root/Variables/Global/Christine/iHasRaijuForm", "N", 1.0)
        
-- |[Achievement]|
AM_SetPropertyJournal("Unlock Achievement", "BecomeRaiju")

-- |[World]|
--Any enemies on the map of this type should now ignore the player.
AL_PulseIgnore("Raiju")

-- |[Combat Statistics]|
--Change character class.
AdvCombat_SetProperty("Push Party Member", "Christine")
    AdvCombatEntity_SetProperty("Active Job", "Thunder Rat")
DL_PopActiveObject()
