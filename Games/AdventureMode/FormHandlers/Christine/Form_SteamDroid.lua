-- |[ ================================= Christine Steam Droid ================================== ]|
--Called when a character shapeshifts.

-- |[Script Variables]|
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Christine/sForm", "S", "SteamDroid")
VM_SetVar("Root/Variables/Global/Christine/iHasSteamDroidForm", "N", 1.0)
        
-- |[Achievement]|
AM_SetPropertyJournal("Unlock Achievement", "BecomeSteam")

-- |[World]|
--Any enemies on the map of this type should now ignore the player.
AL_PulseIgnore("SteamDroid")

-- |[Combat Statistics]|
--Change character class.
AdvCombat_SetProperty("Push Party Member", "Christine")
    AdvCombatEntity_SetProperty("Active Job", "Brass Brawler")
DL_PopActiveObject()
