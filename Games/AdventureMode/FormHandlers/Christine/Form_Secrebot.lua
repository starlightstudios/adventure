-- |[ =================================== Christine Secrebot =================================== ]|
--Called when a character shapeshifts.

-- |[Script Variables]|
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Christine/sForm", "S", "Secrebot")
VM_SetVar("Root/Variables/Global/Christine/iHasSecrebotForm", "N", 1.0)
        
-- |[Achievement]|
AM_SetPropertyJournal("Unlock Achievement", "BecomeSecrebot")

-- |[World]|
--Any enemies on the map of this type should now ignore the player.
AL_PulseIgnore("Secrebot")

-- |[Combat Statistics]|
--Change character class.
AdvCombat_SetProperty("Push Party Member", "Christine")
    AdvCombatEntity_SetProperty("Active Job", "Menialist")
DL_PopActiveObject()
