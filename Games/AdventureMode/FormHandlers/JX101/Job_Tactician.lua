-- |[ ==================================== JX-101 Tactician ==================================== ]|
--Used during the brief section where JX-101 joins the party.

-- |[Combat Statistics]|
--Change Florentina's class. This will automatically handle sprite changes, and marks the active
-- job in the script variables.
AdvCombat_SetProperty("Push Party Member", "JX-101")
    AdvCombatEntity_SetProperty("Active Job", "Tactician")
DL_PopActiveObject()
