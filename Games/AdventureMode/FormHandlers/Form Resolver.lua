-- |[ ====================================== Form Resolver ===================================== ]|
--This script will be fired whenever the player uses the "Forms" menu at a save point. It will be fired with its
-- first argument being the name of the party member selected.
local iArgs = LM_GetNumOfArgs()
if(iArgs < 1) then return end

--Debug boolean. If set to true, the form handler itself is called to instantly perform the transformation. There
-- will be no cutscenes activated.
local bUseDirectFormHandlers = false

--Argument resolve.
local sEntityName = LM_GetScriptArgument(0)
local sCurrentForm = "Null"
local sChapter = "Null"
local sClassRoot = "Null"

--Diagnostics.
local bDiagnostics = Debug_GetFlag("Form Handling: All")
Debug_PushPrint(bDiagnostics, "===== Running Form Resolver =====\n")
Debug_Print("Entity Name: " .. sEntityName .. "\n")

-- |[ ======================================== Function ======================================== ]|
local iTotalForms = 0
local zaFormList = {}
local function fnAddTransformation(sVariablePath, sName, sDisplayName, sImageName, sJobScriptPath)
	iTotalForms = iTotalForms + 1
	zaFormList[iTotalForms] = {}
	zaFormList[iTotalForms].sName = sName
	zaFormList[iTotalForms].sDisplayName = sDisplayName
	zaFormList[iTotalForms].sVariablePath = sVariablePath
	zaFormList[iTotalForms].sImgPath = "Root/Images/AdventureUI/FormIcons/" .. sImageName
	zaFormList[iTotalForms].sJobScriptPath = sJobScriptPath
end

-- |[ ========================================= Lookups ======================================== ]|
-- |[Mei]|
--Mei's transformation cases.
if(sEntityName == "Mei") then
	
	--Current form.
	sCurrentForm = VM_GetVar("Root/Variables/Global/" .. sEntityName .. "/sForm", "S")
    sChapter = "Chapter 1/Scenes"
    sClassRoot = gsRoot .. "Combat/Party/Mei/Jobs/"
	
	--Form Listing:
    local sVar = "Root/Variables/Global/Mei/"
	fnAddTransformation("TRUE",                        "Human",       "Human",      "MeiHuman",       sClassRoot .. "Fencer/000 Job Script.lua")
	fnAddTransformation(sVar .. "iHasAlrauneForm",     "Alraune",     "Alraune",    "MeiAlraune",     sClassRoot .. "Nightshade/000 Job Script.lua")
	fnAddTransformation(sVar .. "iHasBeeForm",         "Bee",         "Bee",        "MeiBee",         sClassRoot .. "Hive Scout/000 Job Script.lua")
	fnAddTransformation(sVar .. "iHasSlimeForm",       "Slime",       "Slime",      "MeiSlime",       sClassRoot .. "Smarty Sage/000 Job Script.lua")
	fnAddTransformation(sVar .. "iHasGravemarkerForm", "Gravemarker", "Gravemarker","MeiGravemarker", sClassRoot .. "Petraian/000 Job Script.lua")
	fnAddTransformation(sVar .. "iHasGhostForm",       "Ghost",       "Ghost",      "MeiGhost",       sClassRoot .. "Maid/000 Job Script.lua")
	fnAddTransformation(sVar .. "iHasMannequinForm",   "Mannequin",   "Mannequin",  "MeiMannequin",   sClassRoot .. "Stalker/000 Job Script.lua")
	fnAddTransformation(sVar .. "iHasWerecatForm",     "Werecat",     "Werecat",    "MeiWerecat",     sClassRoot .. "Prowler/000 Job Script.lua")
	fnAddTransformation(sVar .. "iHasWisphagForm",     "Wisphag",     "Wisphag",    "MeiWisphag",     sClassRoot .. "Soulherd/000 Job Script.lua")
    
    --Debug
	--fnAddTransformation("TRUE", "Rubber", "Mei_Human")
	--fnAddTransformation("TRUE", "Zombee", "Mei_Bee_MC")

-- |[Christine]|
--Christine's transformation cases.
elseif(sEntityName == "Christine") then
	
	--Completely lock out all TFs if we haven't ended Regulus City yet.
	local iMet55InLowerRegulus = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N")
	if(iMet55InLowerRegulus == 0.0) then return end
	
	--Lock out TFs if Christine is in one of the sensitive areas.
	local sLevelName = AL_GetProperty("Name")
	if(sLevelName == "SprocketCityA") then return end
	if(sLevelName == "RegulusFlashbackA") then return end
	if(sLevelName == "RegulusArcaneA") then return end
	
	--Current Form.
	sCurrentForm = VM_GetVar("Root/Variables/Global/" .. sEntityName .. "/sForm", "S")
    sChapter = "Chapter 5/Scenes"
    sClassRoot = gsRoot .. "Combat/Party/Christine/Jobs/"
	
	--Form Listing:
    local sVar = "Root/Variables/Global/Christine/"
	fnAddTransformation("TRUE",                          "Human",         "Human",         "ChristineHuman",         sClassRoot .. "Lancer/000 Job Script.lua")
	fnAddTransformation(sVar .. "iHasGolemForm",         "Golem",         "Golem",         "ChristineGolem",         sClassRoot .. "Repair Unit/000 Job Script.lua")
	fnAddTransformation(sVar .. "iHasLatexForm",         "LatexDrone",    "LatexDrone",    "ChristineLatexDrone",    sClassRoot .. "Drone/000 Job Script.lua")
	fnAddTransformation(sVar .. "iHasDarkmatterForm",    "Darkmatter",    "Darkmatter",    "ChristineDarkmatter",    sClassRoot .. "Starseer/000 Job Script.lua")
	fnAddTransformation(sVar .. "iHasEldritchForm",      "Eldritch",      "Dreamer",       "ChristineEldritch",      sClassRoot .. "Handmaiden/000 Job Script.lua")
	fnAddTransformation(sVar .. "iHasElectrospriteForm", "Electrosprite", "Electrosprite", "ChristineElectrosprite", sClassRoot .. "Live Wire/000 Job Script.lua")
	fnAddTransformation(sVar .. "iHasRaijuForm",         "Raiju",         "Raiju",         "ChristineRaiju",         sClassRoot .. "Thunder Rat/000 Job Script.lua")
	fnAddTransformation(sVar .. "iHasSteamDroidForm",    "SteamDroid",    "SteamDroid",    "ChristineSteamDroid",    sClassRoot .. "Brass Brawler/000 Job Script.lua")
	fnAddTransformation(sVar .. "iHasDollForm",          "Doll",          "Doll",          "ChristineDoll",          sClassRoot .. "Commissar/000 Job Script.lua")
	fnAddTransformation(sVar .. "iHasSecrebotForm",      "Secrebot",      "Secrebot",      "ChristineSecrebot",      sClassRoot .. "Menialist/000 Job Script.lua")

-- |[Sanya]|
--Sanya's transformation cases.
elseif(sEntityName == "Sanya") then
	
	--Current Form.
	sCurrentForm = VM_GetVar("Root/Variables/Global/" .. sEntityName .. "/sForm", "S")
    sChapter = "Chapter 2/Scenes"
    sClassRoot = gsRoot .. "Combat/Party/Sanya/Jobs/"
	
	--Form Listing:
    local sVar = "Root/Variables/Global/Sanya/"
	fnAddTransformation("TRUE",                    "Human",   "Human",   "SanyaHuman",   sClassRoot .. "Hunter/000 Job Script.lua")
	fnAddTransformation(sVar .. "iHasSevaviForm",  "Sevavi",  "Sevavi",  "SanyaSevavi",  sClassRoot .. "Agontea/000 Job Script.lua")
	fnAddTransformation(sVar .. "iHasWerebatForm", "Werebat", "Werebat", "SanyaWerebat", sClassRoot .. "Skystriker/000 Job Script.lua")
	fnAddTransformation(sVar .. "iHasBunnyForm",   "Bunny",   "Bunny",   "SanyaBunny",   sClassRoot .. "Scofflaw/000 Job Script.lua")
	fnAddTransformation(sVar .. "iHasHarpyForm",   "Harpy",   "Harpy",   "SanyaHarpy",   sClassRoot .. "Ranger/000 Job Script.lua")

-- |[Empress]|
--Can change jobs via this menu.
elseif(sEntityName == "Empress") then

    --Job listing.
    local iHasJob_Conquerer = VM_GetVar("Root/Variables/Global/Empress/iHasJob_Conquerer", "N")
    local sCurrentJob       = VM_GetVar("Root/Variables/Global/Empress/sCurrentJob", "S")
     
    --Variables.
    sClassRoot = gsRoot .. "Combat/Party/Empress/Jobs/"
    
    --Conquerer:
    if(iHasJob_Conquerer == 1.0 and sCurrentJob ~= "Conquerer") then
        AM_SetProperty("Register Form Class", "Conquerer", "Conquerer", gsRoot .. "FormHandlers/Empress/Job_Conquerer.lua", sClassRoot .. "Conquerer/000 Job Script.lua", "Root/Images/Sprites/Empress_Conquerer/SW|0")
    end
    
    -- |[Finish]|
    Debug_PopPrint("Empress finished form resolver normally.\n")
    return
    
-- |[Florentina]|
--Can change jobs via this menu.
elseif(sEntityName == "Florentina") then

    -- |[Blocker]|
    --Cannot change during this sequence.
    local iBeganSequence = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iBeganSequence", "N")
    local iPolarisRunIn  = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iPolarisRunIn", "N")
    if(iBeganSequence == 1.0 and iPolarisRunIn == 0.0) then
        Debug_PopPrint("Florentina blocks form resolver during Mannequin sequence.\n")
        return
    end

    -- |[Execution]|
    --Job listing.
    local iHasJob_Merchant       = VM_GetVar("Root/Variables/Global/Florentina/iHasJob_Merchant", "N")
    local iHasJob_Mediator       = VM_GetVar("Root/Variables/Global/Florentina/iHasJob_Mediator", "N")
    local iHasJob_TreasureHunter = VM_GetVar("Root/Variables/Global/Florentina/iHasJob_TreasureHunter", "N")
    local iHasJob_Agarist        = VM_GetVar("Root/Variables/Global/Florentina/iHasJob_Agarist", "N")
    local iHasJob_Lurker         = VM_GetVar("Root/Variables/Global/Florentina/iHasJob_Lurker", "N")
    local sCurrentJob            = VM_GetVar("Root/Variables/Global/Florentina/sCurrentJob", "S")
    
    --Diagnostics report.
    if(bDiagnostics) then
        Debug_Print("Florentina job variables report.\n")
        Debug_Print(" Merchant:        " .. iHasJob_Merchant .. "\n")
        Debug_Print(" Mediator:        " .. iHasJob_Mediator .. "\n")
        Debug_Print(" Treasure Hunter: " .. iHasJob_TreasureHunter .. "\n")
        Debug_Print(" Agarist:         " .. iHasJob_Agarist .. "\n")
        Debug_Print(" Lurker:          " .. iHasJob_Lurker .. "\n")
        Debug_Print(" Current:         " .. sCurrentJob .. "\n")
        
    end
    
    --Variables.
    local sChapRoot = gsRoot .. "Chapter 1/Scenes/100 Transform/"
    local sFormRoot = gsRoot .. "FormHandlers/Florentina/"
    local sIconBase = "Root/Images/AdventureUI/FormIcons/"
    sClassRoot = gsRoot .. "Combat/Party/Florentina/Jobs/"
    
    --Merchant:
    if(iHasJob_Merchant == 1.0 and sCurrentJob ~= "Merchant") then
        AM_SetProperty("Register Form Class", "Merchant", "Merchant", sFormRoot .. "Job_Merchant.lua", sClassRoot .. "Merchant/000 Job Script.lua", sIconBase .. "FlorentinaMerchant")
        AM_SetProperty("Set Form Class Alignments", "Merchant", 0, 0, 0, 0, 90, 90)
    end
    
    --Mediator:
    if(iHasJob_Mediator == 1.0 and sCurrentJob ~= "Mediator") then
        AM_SetProperty("Register Form Class", "Mediator", "Mediator", sFormRoot .. "Job_Mediator.lua", sClassRoot .. "Mediator/000 Job Script.lua", sIconBase .. "FlorentinaMediator")
        AM_SetProperty("Set Form Class Alignments", "Mediator", 0, 0, 0, 0, 90, 90)
    end
    
    --Treasure Hunter:
    if(iHasJob_TreasureHunter == 1.0 and sCurrentJob ~= "TreasureHunter") then
        AM_SetProperty("Register Form Class", "Treasure Hunter", "Treasure Hunter", sFormRoot .. "Job_TreasureHunter.lua", sClassRoot .. "Treasure Hunter/000 Job Script.lua", sIconBase .. "FlorentinaTreasureHunter")
        AM_SetProperty("Set Form Class Alignments", "Treasure Hunter", 0, 0, 0, 0, 90, 90)
    end
    
    --Agarist:
    if(iHasJob_Agarist == 1.0 and sCurrentJob ~= "Agarist") then
        AM_SetProperty("Register Form Class", "Agarist", "Agarist", sChapRoot .. "Transform_FlorentinaToMushraune/Scene_Begin.lua", sClassRoot .. "Agarist/000 Job Script.lua", sIconBase .. "FlorentinaAgarist")
        AM_SetProperty("Set Form Class Alignments", "Agarist", 0, 0, 0, 0, 90, 90)
    end
    
    --Lurker:
    if(iHasJob_Lurker == 1.0 and sCurrentJob ~= "Lurker") then
        AM_SetProperty("Register Form Class", "Lurker", "Lurker", sFormRoot .. "Job_Lurker.lua", sClassRoot .. "Lurker/000 Job Script.lua", sIconBase .. "FlorentinaLurker")
        AM_SetProperty("Set Form Class Alignments", "Lurker", 0, 0, 0, 0, 90, 90)
    end
    
    -- |[Finish]|
    Debug_PopPrint("Florentina finished form resolver normally.\n")
    return

-- |[Izuna]|
--Can change jobs via this menu.
elseif(sEntityName == "Izuna") then

    --Job listing.
    local iHasJob_Miko = VM_GetVar("Root/Variables/Global/Izuna/iHasJob_Miko", "N")
    local sCurrentJob  = VM_GetVar("Root/Variables/Global/Izuna/sCurrentJob", "S")
    
    --Variables.
    sClassRoot = gsRoot .. "Combat/Party/Izuna/Jobs/"
    
    --Miko:
    if(iHasJob_Miko == 1.0 and sCurrentJob ~= "Miko") then
        AM_SetProperty("Register Form Class", "Miko", "Miko", gsRoot .. "FormHandlers/Izuna/Job_Miko.lua", sClassRoot .. "Miko/000 Job Script.lua", "Root/Images/Sprites/Izuna_Miko/SW|0")
    end
    
    -- |[Finish]|
    Debug_PopPrint("Izuna finished form resolver normally.\n")
    return

-- |[Tiffany]|
--Can change jobs via this menu.
elseif(sEntityName == "Tiffany") then
	
	--Current Form.
	sCurrentForm = VM_GetVar("Root/Variables/Global/" .. sEntityName .. "/sCurrentJob", "S")
    sChapter = "Chapter 5/Scenes"
    sClassRoot = gsRoot .. "Combat/Party/Tiffany/Jobs/"
	
	--Form Listing:
    local sVar = "Root/Variables/Global/Tiffany/"
	fnAddTransformation("TRUE",                                            "Assault",   "Assault",   "TiffanyAssault",   sClassRoot .. "Assault/000 Job Script.lua")
	fnAddTransformation("TRUE",                                            "Support",   "Support",   "TiffanySupport",   sClassRoot .. "Support/000 Job Script.lua")
	fnAddTransformation("TRUE",                                            "Subvert",   "Subvert",   "TiffanySubvert",   sClassRoot .. "Subvert/000 Job Script.lua")
	fnAddTransformation("Root/Variables/Global/Tiffany/iHasJob_Spearhead", "Spearhead", "Spearhead", "TiffanySpearhead", sClassRoot .. "Spearhead/000 Job Script.lua")

-- |[SX-399]|
--Can change jobs via this menu.
elseif(sEntityName == "SX-399") then
	
	--Current Form.
	sCurrentForm = VM_GetVar("Root/Variables/Global/" .. sEntityName .. "/sCurrentJob", "S")
    sChapter = "Chapter 5/Scenes"
    sClassRoot = gsRoot .. "Combat/Party/SX-399/Jobs/"
	
	--Form Listing:
    local sVar = "Root/Variables/Global/SX-399/"
	fnAddTransformation("TRUE", "Shocktrooper", "Shocktrooper", "SX-399Shocktrooper", sClassRoot .. "Shocktrooper/000 Job Script.lua")

--Error, name not recognized. Usually means the member has no TFs/Jobs.
else
	--Debug_ForcePrint("Form Resolver: Error, no entity " .. sEntityName .. " handled.\n")

end
    
-- |[ ========================================= Upload ========================================= ]|
--Once the characters have built their listing, run through the list and upload the data to the UI.
for i = 1, iTotalForms, 1 do
    
    --Check the form name against this one. If they match, this is the comparison form.
    if(sCurrentForm == zaFormList[i].sName) then
    
    --Check availability:
    else
        --Check variables.
        local iHasForm = 0.0
        if(zaFormList[i].sVariablePath == "TRUE") then
            iHasForm = 1.0
        else
            iHasForm = VM_GetVar(zaFormList[i].sVariablePath, "N")
        end
        
        --If the form is unlocked, add it to the list.
        if(iHasForm == 1.0) then
            
            --Direct handler.
            if(bUseDirectFormHandlers) then
                AM_SetProperty("Register Form Class", zaFormList[i].sName, zaFormList[i].sDisplayName, gsRoot .. "FormHandlers/" .. sEntityName .. "/Form_" .. zaFormList[i].sName .. ".lua", zaFormList[i].sJobScriptPath, zaFormList[i].sImgPath)
                AM_SetProperty("Set Form Class Alignments", zaFormList[i].sName, 0, 0, 0, 0, 90, 90)
    
            --Normal case. Can trigger cutscenes.
            else
                AM_SetProperty("Register Form Class", zaFormList[i].sName, zaFormList[i].sDisplayName, gsRoot .. sChapter .. "/100 Transform/Transform_" .. sEntityName .. "To" .. zaFormList[i].sName .. "/Scene_Begin.lua", zaFormList[i].sJobScriptPath, zaFormList[i].sImgPath)
                AM_SetProperty("Set Form Class Alignments", zaFormList[i].sName, 0, 0, 0, 0, 90, 90)
            end
        end
    end
end

-- |[ ======= Debug ====== ]|
Debug_PopPrint("===== Finished Form Resolver =====\n")
