-- |[ =================================== Audio Routing File =================================== ]|
--Adventure Mode's routing file. Calls subfolders to load their audio.
local sBasePath = fnResolvePath()

--Subfolder calls.
LM_ExecuteScript(sBasePath .. "CombatSFX/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "MenuSFX/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "Music/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "PuzzleFight/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "VoiceSFX/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "WorldInteractionSFX/ZRouting.lua")

--Voice callouts. Call class list, then class, then builders.
LM_ExecuteScript(sBasePath .. "Classes/VoiceData/VoiceData.lua")
LM_ExecuteScript(sBasePath .. "Classes/VoiceDataList/VoiceDataList.lua")

--Build effects.
LM_ExecuteScript(sBasePath .. "VoiceCallouts/Vocal Effects.lua")
