-- |[ =================================== Music Routing File =================================== ]|
--Loads music that is common to all chapters.

-- |[Variables]|
local sBasePath = fnResolvePath()

-- |[Battle Themes]|
local sBattlePath = sBasePath .. "Battle Themes/"
AudioManager_RegisterAdv("BossBattleTheme",      sBattlePath .. "Boss Theme.ogg",            "Stream|Music|Loop:" ..  "3.666:" ..  "90.364")
AudioManager_RegisterAdv("CombatVictory",        sBattlePath .. "CombatVictory.ogg",         "Stream|Music")
AudioManager_RegisterAdv("DesolateShort",        sBattlePath .. "Desolate Shortened.ogg",    "Stream|Music")

-- |[Misc Tracks]|
local sMiscPath = sBasePath .. "Misc Tracks/"

--Dummy Theme. Plays no sound, used for special effects.
AudioManager_RegisterAdv("DummyTrack", sMiscPath .. "DummyTrack.ogg", "Stream|Music|Loop:" .. "0.000:" .. "4.000")
