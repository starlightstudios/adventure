-- |[ ======================================== Voice SFX ======================================= ]|
--These are the voice ticks that play when a character speaks.
local fLocalVolume = 0.45
local sBasePath = fnResolvePath()
local sVoicePath = sBasePath

-- |[ ==================================== Local Functions ===================================== ]|
--Registers the voice and applies the local volume.
local fnRegisterVoice = function(sName, sFilename, fUseVolume)
    
    --Arg check.
    if(sName == nil) then return end
    
    --If no filename is provided, it's the same as the character name.
    if(sFilename == nil) then sFilename = sName end
    
    --Volume. If no volume is passed, used the local volume.
    local fApplyVolume = fLocalVolume
    if(fUseVolume ~= nil) then fApplyVolume = fUseVolume end
    
    --Register.
    AudioManager_Register("Voice|" .. sName,  "AsSound", "AsSample", sVoicePath .. sFilename .. ".ogg")
        AudioPackage_SetLocalVolume(fApplyVolume)
    
end

-- |[ ================================= By-Chapter Characters ================================== ]|
--If characters appear in many chapters, they are listed in the first chapter they appear in.

-- |[Chapter 1]|
sVoicePath = sBasePath .. "Ch1/"
local saList = {"Alraune", "Aquillia", "Bee", "Blythe", "Breanne", "CrowbarChan", "Crowd", "Florentina", "Ghost", "Maram", "MarriedJackie", "MarriedSammie",
                "Mei", "Miho", "Mycela", "Nadia", "Polaris", "Septima", "Sharelock", "Werecat", "Victoria", "Xanna"}
for i = 1, #saList, 1 do
    fnRegisterVoice(saList[i])
end

-- |[Chapter 2]|
sVoicePath = sBasePath .. "Ch2/"
saList = {"Angelface", "Bunny", "Empress", "Esmerelda", "HarpyA", "HarpyB", "HarpyC", "HarpyD", "Izuna", "Marigold", "Mia", "Miso", "Morus", "Odar", "Sanya", "Takahn", "Tetra", "Yukina", "Zeke"}
for i = 1, #saList, 1 do
    fnRegisterVoice(saList[i])
end

-- |[Chapter 3]|
-- |[Chapter 4]|
-- |[Chapter 5]|
--Normal:
sVoicePath = sBasePath .. "Ch5/"
saList = {"Chris", "Christine", "Darkmatter", "Ellie", "JX101", "Maisie", "Sophie", "SX399", "Tiffany", "Night", "Talos"}
for i = 1, #saList, 1 do
    fnRegisterVoice(saList[i])
end

--Special: Use a lower volume.
fnRegisterVoice("Administrator", "Administrator", 0.25)

-- |[ =================================== Generic Voice Ticks ================================== ]|
--These are often used by multiple minor characters.
sVoicePath = sBasePath .. "Generic/"
for i = 0, 17, 1 do
    AudioManager_Register(string.format("Voice|GenericF%02i", i), "AsSound", "AsSample", sVoicePath .. string.format("Fem%02i.ogg", i))
    AudioPackage_SetLocalVolume(fLocalVolume)
end
for i = 0, 19, 1 do
    AudioManager_Register(string.format("Voice|GenericM%02i", i), "AsSound", "AsSample", sVoicePath .. string.format("Mal%02i.ogg", i))
    AudioPackage_SetLocalVolume(fLocalVolume)
end
for i = 0, 3, 1 do
    AudioManager_Register(string.format("Voice|GenericN%02i", i), "AsSound", "AsSample", sVoicePath .. string.format("Neu%02i.ogg", i))
    AudioPackage_SetLocalVolume(fLocalVolume)
end

-- |[ ========================================== Other ========================================= ]|
-- |[Baaaa]|
--Baaaa
sVoicePath = sBasePath .. "Sheep/"
AudioManager_Register("Sheep00", "AsSound", "AsSample", sVoicePath .. "Sheep00.ogg")
AudioManager_Register("Sheep01", "AsSound", "AsSample", sVoicePath .. "Sheep01.ogg")
AudioManager_Register("Sheep02", "AsSound", "AsSample", sVoicePath .. "Sheep02.ogg")
AudioManager_Register("Sheep03", "AsSound", "AsSample", sVoicePath .. "Sheep03.ogg")
AudioManager_Register("Sheep04", "AsSound", "AsSample", sVoicePath .. "Sheep04.ogg")
AudioManager_Register("Sheep05", "AsSound", "AsSample", sVoicePath .. "Sheep05.ogg")
AudioManager_Register("Sheep06", "AsSound", "AsSample", sVoicePath .. "Sheep06.ogg")
AudioManager_Register("Sheep07", "AsSound", "AsSample", sVoicePath .. "Sheep07.ogg")
