-- |[ ================================= VoiceDataList Handling ================================= ]|
--List handlers, including registering and querying.

-- |[ =============================== VoiceDataList:fnRegister() =============================== ]|
--Creates a new form package, or barks an error upon duplication.
function VoiceDataList:fnRegister(pzEntry)
    
    -- |[Argument Check]|
    if(pzEntry == nil) then return end
    
    -- |[Duplicate Check]|
    local zVoiceData = self:fnLocateEntry(pzEntry.sLocalName)
    if(zVoiceData ~= nil) then
        io.write("VoiceDataList:fnRegister() - Warning, VoiceData entry " .. pzEntry.sLocalName .. " already exists.\n")
        return
    end

    -- |[Register]|
    table.insert(self.zaVoiceList, pzEntry)
end

-- |[ ============================= VoiceDataList:fnLocateEntry() ============================== ]|
--Locates and returns the named voice package, or nil if not found.
function VoiceDataList:fnLocateEntry(psName)
    
    -- |[Argument Check]|
    if(psName == nil) then 
        io.write("VoiceDataList:fnLocateEntry() - Warning, attempted to pass name that was nil. Called: " .. LM_GetCallStack(0) .. "\n")
        return nil
    end

    -- |[Scan]|
    for i = 1, #self.zaVoiceList, 1 do
        if(self.zaVoiceList[i].sLocalName == psName) then
            return self.zaVoiceList[i]
        end
    end
    
    -- |[Not Found]|
    return nil
end