-- |[ ================================ VoiceDataList Class File ================================ ]|
--Class file for VoiceDataList. Call this once at initialization, particularly before the audio loads.

--VoiceDataList stores a set of VoiceData's as a singleton, with some search functions.

-- |[ ============ Class Members ============= ]|
-- |[System]|
VoiceDataList = {}
VoiceDataList.__index = VoiceDataList

-- |[Lists]|
VoiceDataList.zaVoiceList = {}

-- |[ ============ Class Methods ============= ]|
--System
--List Handling
function VoiceDataList:fnRegister(pzEntry)   end --Static
function VoiceDataList:fnLocateEntry(psName) end --Static

-- |[ ========= Call Implementations ========= ]|
LM_ExecuteScript(fnResolvePath() .. "VoiceDataListHandling.lua")
