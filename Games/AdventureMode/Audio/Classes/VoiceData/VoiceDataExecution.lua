-- |[ ==================================== VoiceData Forms ===================================== ]|
--Form handlers, including registering and querying.

-- |[ ================================ VoiceData:fnHandlePain() ================================ ]|
--Handles a response callout when the subject is damaged.
function VoiceData:fnHandlePain(piTimer, piTargetID)
    
    -- |[ =============== Argument Check =============== ]|
    if(piTimer    == nil) then return end
    if(piTargetID == nil) then return end
    
    --Diagnostics.
    local bDiagnostics = Debug_GetFlag("Voice: All") or Debug_GetFlag("Voice: All Toggle") or Debug_GetFlag("Voice: Pain")
    Debug_PushPrint(bDiagnostics, "VoiceData:fnHandlePain() - Begin execution.\n")
    
    -- |[ ============== Pain Information ============== ]|
    -- |[Resolve HP Percent]|
    --Which pain sound plays is based on the final HP of the character.
    AdvCombat_SetProperty("Push Entity By ID", piTargetID)
        local iCurHP = AdvCombatEntity_GetProperty("Health")
        local iMaxHP = AdvCombatEntity_GetProperty("Health Max")
    DL_PopActiveObject()
    Debug_Print("Starting HP: " .. iCurHP .. "\n")
    Debug_Print("Maximum HP: " .. iMaxHP .. "\n")
    if(iMaxHP < 1) then iMaxHP = 1 end
    
    --Subtract the amount of damage taken by the last attack. It should still be stored in the global
    -- structure, if this is called at the right time.
    iCurHP = iCurHP - gzAbiPack.zTargetInfo.iDamageDealt
    local fHPPct = (iCurHP / iMaxHP) * 100
    Debug_Print("Projected damage: " .. gzAbiPack.zTargetInfo.iDamageDealt .. "\n")
    Debug_Print("Ending HP: " .. iCurHP .. "\n")
    Debug_Print("Ending HP Percent: " .. fHPPct .. "\n")
    
    -- |[Resolve Pain Type]|
    --Setup.
    local sPainPattern = "Lo"
    local iPainCount = 0
    
    -- |[Low Pain, High HP]|
    --If the character has over 50% HP, play the low-pain sound.
    if(fHPPct >= 50) then
        iPainCount = self.iPainLoCount
        sPainPattern = "Lo"
        
    --20% to 50%. Mid-pain sound.
    elseif(fHPPct >= 20) then
        iPainCount = self.iPainMdCount
        sPainPattern = "Md"
    
    --Very low HP, play the high-pain sound.
    else
        iPainCount = self.iPainHiCount
        sPainPattern = "Hi"
    end
    
    --Debug.
    Debug_Print("Pain count: " .. iPainCount .. "\n")
    Debug_Print("Pain pattern: " .. sPainPattern .. "\n")
    
    -- |[ ============== Rolling Chances =============== ]|
    -- |[Abstract Volume Check]|
    --Check the abstract volume associated with this calling entity. If it's lower than 0.01, don't ever play any sounds.
    -- They wouldn't be audible anyway, but we also don't want to cause a delay in the procession of combat.
    if(self.iAbstractVolSlot ~= -1) then
        
        --Query the volume.
        local sOptionName = string.format("AbstractVolume%02i", self.iAbstractVolSlot)
        local iVolume = OM_GetOption(sOptionName)
        
        --Volume too low:
        if(iVolume < 1) then
            Debug_PopPrint("Not playing a sound, abstract volume is zero.\n")
            return
        end
    end
    
    -- |[Adjusting Roll Chance By Option]|
    --Get this option from the options manager. It is a reference to a lookup table.
    local fVoicePlayFactor = 1.0
    local iVoicePlayLookup = OM_GetOption("Voice Line Play Chance")
    if(iVoicePlayLookup >= 0 and iVoicePlayLookup <= #gcaiChanceToPlayArray) then
        fVoicePlayFactor = gcaiChanceToPlayArray[iVoicePlayLookup]
    end
    
    --If always play callouts is true, bypass all other factors.
    local bAlwaysPlayCallouts = AbyCombat_GetProperty("Always Play Callouts")
    
    --If the voice play factor is 0, never play any sounds.
    if(fVoicePlayFactor == 0.0 and bAlwaysPlayCallouts == false) then
        Debug_PopPrint("Finished, play factor is zero, never plays a sound.\n")
        return
    end

    -- |[Chance To Play]|
    --There is always a 30% chance to play a pain sound, but on low HP this is a 100% chance.
    if(sPainPattern ~= "Hi" and bAlwaysPlayCallouts == false) then
        local iRoll = LM_GetRandomNumber(1, 100)
        if(iRoll > 30 * fVoicePlayFactor) then
            Debug_PopPrint("Finished, too low a chance to play.\n")
            return
        end
    end

    -- |[ =============== Playing Sound ================ ]|
    -- |[Register Pack to Play Sound]|
    --Error check.
    if(iPainCount < 1) then
        Debug_PopPrint("Finished, sound pattern " .. sPainPattern .. " has no entries.\n")
        return
    end

    --String.
    local iRoll = LM_GetRandomNumber(1, iPainCount)
    local sLetter = string.char(string.byte("A") + iRoll - 1)
    local sCallString = "Play Sound|" .. self.sPainCallPattern .. sPainPattern .. sLetter
    
    --Effects. Optional, depends on form.
    local sCharacterForm = VM_GetVar(self.sFormVarPath, "S")
    local zForm = self:fnLocateForm(sCharacterForm)
    if(zForm ~= nil) then
        sCallString = sCallString .. "|" .. "Effect:" .. zForm.sEffectName
    end

    --Register.
    AdvCombat_SetProperty("Register Application Pack", gzAbiPack.iOriginatorID, gzAbiPack.iTargetID, piTimer, sCallString)
    
    --Diagnostics.
    Debug_Print("Roll range: 1 to " .. iPainCount .. "\n")
    Debug_Print("Rolled: " .. iRoll .. "\n")
    Debug_Print("Application pack: " .. "Play Sound|" .. self.sPainCallPattern .. sPainPattern .. sLetter .. "\n")

    -- |[Finish]|
    Debug_PopPrint("Finished normally.\n")
end

-- |[ ============================= VoiceData:fnHandlePreAction() ============================== ]|
--Handles a callout when the character in question performs an action. This callout plays before
-- the action takes place, but can also flag itself to play after the action ends if needed.
function VoiceData:fnHandlePreAction(psCallString, piTimer)
    
    -- |[ =============== Argument Check =============== ]|
    if(psCallString == nil) then return end
    if(piTimer      == nil) then return end
    
    --Diagnostics.
    local bDiagnostics = Debug_GetFlag("Voice: All") or Debug_GetFlag("Voice: All Toggle") or Debug_GetFlag("Voice: Execution")
    Debug_PushPrint(bDiagnostics, "VoiceData:fnHandlePreAction() - " .. self.sLocalName .. " calls pre-action.\n")
    Debug_Print("Call string: " .. psCallString .. "\n")

    -- |[Setup]|
    --Subdivide the callout into segments. The first segment should be the name of the character, 
    -- after that each segment is one of the possible things the ability can use for a callout.
    local saSections = fnSubdivide(psCallString, "|")
    if(#saSections < 2) then
        io.write("VoidData:fnHandlePreAction() - Warning, did not have two or more subdivisions. String: " .. psCallString .. "\n")
        Debug_PopPrint("Failed due to a warning.\n")
        return
    end

    -- |[ =============== Chance To Play =============== ]|
    -- |[Abstract Volume Check]|
    --Check the abstract volume associated with this calling entity. If it's lower than 0.01, don't ever play any sounds.
    -- They wouldn't be audible anyway, but we also don't want to cause a delay in the procession of combat.
    if(self.iAbstractVolSlot ~= -1) then
        
        --Query the volume.
        local sOptionName = string.format("AbstractVolume%02i", self.iAbstractVolSlot)
        local iVolume = OM_GetOption(sOptionName)
        
        --Volume too low:
        if(iVolume < 1) then
            Debug_PopPrint("Not playing a sound, abstract volume is zero.\n")
            return
        end
    end
    
    -- |[Repeat/Too Early Check]|
    --Check to see that the game isn't going to get spammed with this character's voice lines if a lot
    -- of actions happen at the same time. The character tracks when it last used a voice clip in a global
    -- variable and will stop if not enough time has elapsed.
    local iMinimumTicks = 120 --Two seconds
    local iCurrentTick = OM_GetProgramTicksElapsed()
    if(iCurrentTick - self.iLastCalloutTick <= iMinimumTicks) then
        Debug_PopPrint("Not playing a sound, too close to previous call.\n")
        return
    end
    
    --Diagnostics.
    Debug_Print("Timing is valid.\n")
    
    -- |[Adjusting Roll Chance By Parameter]|
    --Call threshold may be modified by the options inherent to the voice data. The heading "Playchance" can affect this.
    local iBasePlayChance = 30
    local fPlayChanceFactor = 1.0
    for i = 2, #saSections, 1 do
        local saSubSections = fnSubdivide(saSections[i], ":")
        if(saSubSections[1] == "Playchance" and #saSubSections >= 2) then
            fPlayChanceFactor = saSubSections[2] / 100.0
        end
    end
    
    -- |[Adjusting Roll Chance By Option]|
    --Get this option from the options manager. It is a reference to a lookup table.
    local fVoicePlayFactor = 1.0
    local iVoicePlayLookup = OM_GetOption("Voice Line Play Chance")
    if(iVoicePlayLookup >= 0 and iVoicePlayLookup <= #gcaiChanceToPlayArray) then
        fVoicePlayFactor = gcaiChanceToPlayArray[iVoicePlayLookup]
    end
    
    -- |[Roll Chance]|
    --By default, voice callouts have a 30% chance to play. Each time a callout fails to play, the chance increases by 10%,
    -- and resets back to 30% when a callout plays.
    local iCallRoll = LM_GetRandomNumber(1, 100)
    
    --Debug flag, if set, always forces the callouts to play.
    local bAlwaysPlayCallouts = AbyCombat_GetProperty("Always Play Callouts")
    if(bAlwaysPlayCallouts) then iCallRoll = 0 end
    
    --If below the play chance, play the line.
    if(iCallRoll <= (iBasePlayChance + self.iCalloutChanceBoost) * fPlayChanceFactor * fVoicePlayFactor) then
        self.iCalloutChanceBoost = 0
    
    --Fail to play the line.
    else
        self.iCalloutChanceBoost = self.iCalloutChanceBoost + 10
        Debug_PopPrint("Not playing a sound, roll failed.\n")
        return
    end
    
    -- |[ =============== Playing Sound ================ ]|
    --Constants
    local iFixedTimeOffset = 0

    --Variables
    local sCharacterForm = VM_GetVar(self.sFormVarPath, "S")
    local sUserName = saSections[1]
    local saListOfCallouts = {}

    --Locate the active form. It can fail to resolve, in which case the Offense/Restore/Special cases are not called.
    local zForm = self:fnLocateForm(sCharacterForm)
    Debug_Print("Character form: " .. sCharacterForm .. "\n")
    if(zForm ~= nil) then
        Debug_Print("Form was found in internal lookups.\n")
    else
        Debug_Print("Form was not found in internal lookups.\n")
    end
    
    --Audio name patterns.
    --Debug_Print("Ability call pattern: " .. sAbilityPath .. "\n")
    --Debug_Print("General call pattern: " .. sGeneralPath .. "\n")

    -- |[Iterate Across Sections]|
    for i = 2, #saSections, 1 do
        self:fnHandleSection(sUserName, zForm, saSections[i], saListOfCallouts)
    end

    --Report.
    if(bDiagnostics == true) then
        Debug_Print("Callout listing:\n")
        for i = 1, #saListOfCallouts, 1 do
            Debug_Print(" " .. saListOfCallouts[i] .. "\n")
        end
    end

    -- |[Selection]|
    --Error check.
    if(#saListOfCallouts < 1) then
        Debug_PopPrint("Finished, no valid callouts found.\n")
        return
    end

    --Roll a random callout.
    local iRoll = LM_GetRandomNumber(1, #saListOfCallouts)

    --Resolve extra arguments.
    local sInstruction = "Play Sound|" .. saListOfCallouts[iRoll]
    if(zForm ~= nil) then
        sInstruction = sInstruction .. "|" .. "Effect:" .. zForm.sEffectName
    end

    --Get the length of the sound in question.
    local sNormalSoundName = ""
    local iLen = string.len(saListOfCallouts[iRoll])
    for i = 1, iLen, 1 do
        local sLetter = string.sub(saListOfCallouts[iRoll], i, i)
        if(sLetter ~= "\\") then
            sNormalSoundName = sNormalSoundName .. sLetter
        end
    end
    local fSoundLen = AudioManager_GetProperty("Length Of Sound", sNormalSoundName)
    local cfSecondsToTicks = 60.0

    --Play that sound.
    AdvCombat_SetProperty("Register Application Pack", gzAbiPack.iOriginatorID, gzAbiPack.iTargetID, piTimer + iFixedTimeOffset, sInstruction)
    Debug_Print("Playing sound: " .. saListOfCallouts[iRoll] .. "\n")
    --io.write("Instruction: " .. sInstruction .. " Length: " .. fSoundLen .. "\n")

    --Set flags.
    self.iLastCalloutTick = iCurrentTick
    giAdjustActionTimerForCallout = math.floor(fSoundLen * cfSecondsToTicks) - 10

    -- |[Finish]|
    Debug_PopPrint("Finished normally.\n")
end

-- |[ ============================= VoiceData:fnHandlePostAction() ============================= ]|
--Handles a callout if the pre-action ordered this object to play a callout after the action ended.
-- Some abilities do this to play after the ability ends for emphasis. If not flagged, this never
-- does anything.
function VoiceData:fnHandlePostAction(psCallString, piTimer)
    Debug_SetFlag("Voice: All Toggle", false)
end

-- |[ ============================== VoiceData:fnHandleSection() =============================== ]|
--Each callout is broken into sections, which are typically calls for general callouts, or specific
-- ability callouts. This function handles figuring out which callouts to add for each section.
--The resulting callouts are appending to psaListOfCallouts.
function VoiceData:fnHandleSection(psUserName, pzForm, psSection, psaListOfCallouts)
    
    -- |[Argument Check]|
    if(psUserName        == nil) then return end
    if(psSection         == nil) then return end
    if(psaListOfCallouts == nil) then return end
    
    --Setup.
    local iByteOfA = string.byte("A")
    local sAbilityPath = "Callout\\|" .. psUserName .. "\\|Ability\\|"
    local sGeneralPath = "Callout\\|" .. psUserName .. "\\|General\\|"
    
    -- |[Ability: Requires Further Tag]|
    --If this begins with "Ability:" then it is a specific ability callout.
    if(string.sub(psSection, 1, 8) == "Ability:") then
        local sAbilityName = string.sub(psSection, 9)
    
        --Search the voice data.
        for p = 1, #self.zaAbilities, 1 do
            if(self.zaAbilities[p][1] == sAbilityName) then
                for q = 1, self.zaAbilities[p][2], 1 do
                    table.insert(psaListOfCallouts, sAbilityPath .. sAbilityName .. string.char(iByteOfA + q - 1))
                
                end
                return
            end
        end
        return
    end
    
    -- |[General]|
    --If this begins with "General:" this is a general type callout, such as "Bleed" for any attack causing bleeds.
    if(string.sub(psSection, 1, 8) == "General:") then
        local sGeneralType = string.sub(psSection, 9)
    
        --Search the voice data.
        for p = 1, #self.zaGenerals, 1 do
            if(self.zaGenerals[p][1] == sGeneralType) then
                for q = 1, self.zaGenerals[p][2], 1 do
                    table.insert(psaListOfCallouts, sGeneralPath .. sGeneralType .. string.char(iByteOfA + q - 1))
                end
                return
            end
        end
        return
    end
    
    -- |[Form-Specific Types]|
    --Offense. This is form-specific.
    if(psSection == "Offense" and pzForm ~= nil) then
        for p = 1, pzForm.iOffenseCount, 1 do
            table.insert(psaListOfCallouts, pzForm.sCallPrefix .. "Offense" .. string.char(iByteOfA + p - 1))
        end
    
    --Restore. This is form-specific.
    elseif(psSection == "Restore" and pzForm ~= nil) then
        for p = 1, pzForm.iRestoreCount, 1 do
            table.insert(psaListOfCallouts, pzForm.sCallPrefix .. "Restore" .. string.char(iByteOfA + p - 1))
        end
    
    --Special. This is form-specific.
    elseif(psSection == "Special" and pzForm ~= nil) then
        for p = 1, pzForm.iSpecialCount, 1 do
            table.insert(psaListOfCallouts, pzForm.sCallPrefix .. "Special" .. string.char(iByteOfA + p - 1))
        end
    
    end
end

