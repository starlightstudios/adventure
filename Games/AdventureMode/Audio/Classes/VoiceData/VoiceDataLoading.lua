-- |[ =================================== VoiceData Loading ==================================== ]|

-- |[ ========================= VoiceData:fnCountFilesMeetingPattern() ========================= ]|
--Counts how many files meet the provided pattern, start at FileA.ogg and going to FileZ.ogg.
-- The pattern should include the file path.
function VoiceData:fnCountFilesMeetingPattern(psPattern)

    -- |[Argument Check]|
    if(psPattern == nil) then return 0 end
    
    --Constants.
    local iByteA = string.byte("A")
    
    -- |[Scan]|
    local iCount = 0
    while(true) do
    
        --Get the appropriate letter.
        local sLetter = string.char(iByteA + iCount)
    
        --Check if the file in question exists. If not, stop here.
        local sCheckPath = psPattern .. sLetter .. ".ogg"
        if(FS_Exists(sCheckPath) == false) then break end
    
        --File exists, add one to the count.
        iCount = iCount + 1
    end
    
    -- |[Finish]|
    return iCount
end

-- |[ ============================= VoiceData:fnRegisterPattern() ============================== ]|
--Worker function, registers a pattern of audio files like "YellA" to "YellC", using strings instead of numbers.
function VoiceData:fnRegisterPattern(psName, psPath, psStartLetter, psEndLetter, psPropertyString)

    -- |[Argument Check]|
    if(psName           == nil) then return end
    if(psPath           == nil) then return end
    if(psStartLetter    == nil) then return end
    if(psEndLetter      == nil) then return end
    if(psPropertyString == nil) then return end
    
    --Diagnostics.
    local bDiagnostics = Debug_GetFlag("Voice: All") or Debug_GetFlag("Voice: All Toggle") or Debug_GetFlag("Voice: Creation")
    Debug_PushPrint(bDiagnostics, "VoiceData:fnRegisterPattern() - Executing on " .. psName .." from " .. psStartLetter .. " to " .. psEndLetter .. "\n")

	--Range check. If the letter is out of the normal range, fail.
	local sCurrentLetter = psStartLetter
	local iByteValue = string.byte(sCurrentLetter)
	if(iByteValue < string.byte("A") or iByteValue > string.byte("Z")) then
        Debug_PopPrint("Finished, hit below A or above Z.\n")
        return
    end

    -- |[Iterate]|
	--Iterate across the letters. Always stops on Z. Also always stops on non-capitals.
	while(true) do
		
        --Resolve the sound name and path name.
		local sSoundName = psName .. sCurrentLetter
		local sPathName  = psPath .. sCurrentLetter .. ".ogg"
        
        --Run loading process.
        AudioManager_RegisterAdv(sSoundName, sPathName, psPropertyString)
        Debug_Print("Registered: " .. sSoundName .. "\n")
		
		--If this was the ending letter, or the letter 'Z', stop.
		if(sCurrentLetter == psEndLetter or sCurrentLetter == "Z") then break end
		
		--Move to the next letter.
		local iByteValue = string.byte(sCurrentLetter)
		sCurrentLetter = string.char(iByteValue + 1)
	end
    
    -- |[Finish Up]|
    Debug_PopPrint("Finished normally.\n")
end

-- |[ ============================ VoiceData:fnLoadGeneralCallout() ============================ ]|
--Given a name and file path, attempts to load audio files according to a pattern until the audio
-- files are not found. This automates the process of registering sound files for a callout.
function VoiceData:fnLoadGeneralCallout(psName, psSoundName, psFilePath, psPattern)
    
    -- |[Argument Check]|
    if(psName      == nil) then return end
    if(psSoundName == nil) then return end
    if(psFilePath  == nil) then return end
    if(psPattern   == nil) then return end
    
    --Diagnostics.
    local bDiagnostics = Debug_GetFlag("Voice: All") or Debug_GetFlag("Voice: All Toggle") or Debug_GetFlag("Voice: Creation")
    Debug_PushPrint(bDiagnostics, "VoiceData:fnLoadGeneralCallout() - Executing on " .. psName .." with pattern " .. psPattern .. "\n")
    
    -- |[Count Valid Files]|
    --Constants.
    local iByteA = string.byte("A")
    
    --Use a simple filesystem check to see if the given files exist and how many there are.
    local iCount = self:fnCountFilesMeetingPattern(psFilePath .. psPattern)

    --If the count came back as 0, do nothing.
    if(iCount < 1) then
        Debug_PopPrint("Count was 0. Stopping.\n")
        return
    end

    -- |[Load and Register]|
    --Resolve properties.
    local sPropertyString = "Sound|Stream|Memory|"
    if(self.iAbstractVolSlot ~= nil and self.iAbstractVolSlot ~= -1) then
        sPropertyString = sPropertyString .. "AbstractVolSlot:" .. self.iAbstractVolSlot .. "|"
    end
        
    --Load off the hard drive.
    local sFinalLetter = string.char(iByteA + iCount - 1)
    self:fnRegisterPattern(psSoundName, psFilePath .. psPattern, "A", sFinalLetter, sPropertyString)
    
    --Make a copy in the general listing.
    self:fnAddGeneralCallout(psName, iCount)
    
    --Diagnostics.
    Debug_PopPrint("Registered General " .. psName .. " with count " .. iCount .. ".\n")
end

-- |[ ============================ VoiceData:fnLoadAbilityCallout() ============================ ]|
--Same as above, but registers to the specific ability listing.
function VoiceData:fnLoadAbilityCallout(psName, psSoundName, psFilePath, psPattern)
    
    -- |[Argument Check]|
    if(psName      == nil) then return end
    if(psSoundName == nil) then return end
    if(psFilePath  == nil) then return end
    if(psPattern   == nil) then return end
    
    --Diagnostics.
    local bDiagnostics = Debug_GetFlag("Voice: All") or Debug_GetFlag("Voice: All Toggle") or Debug_GetFlag("Voice: Creation")
    Debug_PushPrint(bDiagnostics, "VoiceData:fnLoadAbilityCallout() - Executing on " .. psName .." with pattern " .. psPattern .. "\n")
    
    -- |[Count Valid Files]|
    --Constants.
    local iByteA = string.byte("A")
    
    --Use a simple filesystem check to see if the given files exist and how many there are.
    local iCount = self:fnCountFilesMeetingPattern(psFilePath .. psPattern)

    --If the count came back as 0, do nothing.
    if(iCount < 1) then
        Debug_PopPrint("Count was 0. Stopping.\n")
        return
    end

    -- |[Load and Register]|
    --Resolve properties.
    local sPropertyString = "Sound|Stream|Memory|"
    if(self.iAbstractVolSlot ~= nil and self.iAbstractVolSlot ~= -1) then
        sPropertyString = sPropertyString .. "AbstractVolSlot:" .. self.iAbstractVolSlot .. "|"
    end

    --Loadd off the hard drive.
    local sFinalLetter = string.char(iByteA + iCount - 1)
    self:fnRegisterPattern(psSoundName, psFilePath .. psPattern, "A", sFinalLetter, sPropertyString)
    
    --Make a copy in the general listing.
    self:fnAddAbilityCallout(psName, iCount)
    
    --Diagnostics.
    Debug_PopPrint("Registered Ability " .. psName .. " with count " .. iCount .. ".\n")
end

-- |[ ======================== VoiceData:fnLoadAbilityCalloutSequence() ======================== ]|
--Requires a list to be passed in, loads ability callouts using the above function in a sequence.
-- Name: AA_Ability1A, AA_Ability1B, AB_Ability2A, AB_Ability2B, etc
function VoiceData:fnLoadAbilityCalloutSequence(psaNames, psNamePattern, psPath)
    
    -- |[Argument Check]|
    if(psaNames      == nil) then return end
    if(psNamePattern == nil) then return end
    if(psPath        == nil) then return end
    
    -- |[Setup]|
    local sLetters = "AA"

    -- |[Iterate]|
    for i = 1, #psaNames, 1 do
        
        --Load ability.
        self:fnLoadAbilityCallout(psaNames[i], psNamePattern .. psaNames[i], psPath, sLetters .. "_" .. psaNames[i])
        
        --Advance the letter.
        local sLetterOne = string.sub(sLetters, 1, 1)
        local sLetterTwo = string.sub(sLetters, 2, 2)
        
        --Letter is at Z, reset to A and advance sLetterOne.
        if(sLetterTwo == "Z") then
            
            --If sLetterOne is somehow at Z, stop.
            if(sLetterOne == "Z") then
                break
            
            --Otherwise, advance by one.
            else
                local iByteOne = string.byte(sLetterOne)
                iByteOne = iByteOne + 1
                sLetterOne = string.char(iByteOne)
                sLetterTwo = "A"
            end
        
        --Advance sLetterTwo by one.
        else
            local iByteTwo = string.byte(sLetterTwo)
            iByteTwo = iByteTwo + 1
            sLetterTwo = string.char(iByteTwo)
        end
        
        --Set.
        sLetters = sLetterOne .. sLetterTwo
    end

end

-- |[ ================================= VoiceData:fnLoadPains() ================================ ]|
function VoiceData:fnLoadPains(psInternalPattern, psFilePath, psFilePattern)
    
    -- |[Argument Check]|
    if(psInternalPattern == nil) then return end
    if(psFilePattern     == nil) then return end
    if(psFilePath        == nil) then return end
    
    --Diagnostics.
    local bDiagnostics = Debug_GetFlag("Voice: All") or Debug_GetFlag("Voice: All Toggle") or Debug_GetFlag("Voice: Creation")
    Debug_PushPrint(bDiagnostics, "VoiceData:fnLoadPains() - Executing on " .. self.sLocalName .." with pattern " .. psInternalPattern .. "\n")
    Debug_Print("File Pattern: " .. psFilePattern .. "\n")

    -- |[Variables]|
    --Constants.
    local sBeginLetter = "A"
    local sFinalLetter = "A"
    local iByteA = string.byte("A")
    
    --Store the internal pattern.
    self.sPainPattern = psInternalPattern
    
    --Process the call pattern, which is the same as the base pattern but puts a \\ in front of the | so the C++ code can parse it.
    self.sPainCallPattern = ""
    for i = 1, string.len(self.sPainPattern), 1 do
        local sLetter = string.sub(self.sPainPattern, i, i)
        if(sLetter ~= "|") then
            self.sPainCallPattern = self.sPainCallPattern .. sLetter
        else
            self.sPainCallPattern = self.sPainCallPattern .. "\\|"
        end
    end
    
    -- |[Resolve Sound Properties]|
    --Properties are common to all pain types.
    local sPropertyString = "Sound|Stream|Memory|"
    if(self.iAbstractVolSlot ~= nil and self.iAbstractVolSlot ~= -1) then
        sPropertyString = sPropertyString .. "AbstractVolSlot:" .. self.iAbstractVolSlot .. "|"
    end
    
    -- |[Load Low Pains]|
    self.iPainLoCount = self:fnCountFilesMeetingPattern(psFilePath .. psFilePattern .. "Lo")
    sFinalLetter = string.char(iByteA + self.iPainLoCount - 1)
    self:fnRegisterPattern(self.sPainPattern .. "Lo", psFilePath .. psFilePattern .. "Lo", sBeginLetter, sFinalLetter, sPropertyString)
    
    -- |[Load Mid Pains]|
    self.iPainMdCount = self:fnCountFilesMeetingPattern(psFilePath .. psFilePattern .. "Md")
    sFinalLetter = string.char(iByteA + self.iPainMdCount - 1)
    self:fnRegisterPattern(self.sPainPattern .. "Md", psFilePath .. psFilePattern .. "Md", sBeginLetter, sFinalLetter, sPropertyString)
    
    -- |[Load Mid Pains]|
    self.iPainHiCount = self:fnCountFilesMeetingPattern(psFilePath .. psFilePattern .. "Hi")
    sFinalLetter = string.char(iByteA + self.iPainHiCount - 1)
    self:fnRegisterPattern(self.sPainPattern .. "Hi", psFilePath .. psFilePattern .. "Hi", sBeginLetter, sFinalLetter, sPropertyString)
    
    -- |[Diagnostics]|
    Debug_Print("Located Lo/Md/Hi pain sounds: " .. self.iPainLoCount .. "/" .. self.iPainMdCount .. "/" .. self.iPainHiCount .. "\n")
    Debug_PopPrint("Completed normally.\n")
end