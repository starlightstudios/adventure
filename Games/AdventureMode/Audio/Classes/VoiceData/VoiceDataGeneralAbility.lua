-- |[ ============================ VoiceData General/Ability Lists ============================= ]|
--The General/Ability lists which create specific callouts for specific abilities or ability properties.

-- |[ ============================ VoiceData:fnAddGeneralCallout() ============================= ]|
--Registers a general callout package.
function VoiceData:fnAddGeneralCallout(psName, piCount)
    
    -- |[Argument Check]|
    if(psName  == nil) then return end
    if(piCount == nil) then return end
    
    -- |[Duplicate Check]|
    local zCheckPack = self:fnLocateGeneralCallout(psName)
    if(zCheckPack ~= nil) then
        io.write("VoiceData:fnAddGeneralCallout() - Warning, already has general callout " .. psName .. " for character " .. self.sLocalName .. "\n")
        return
    end
    
    -- |[Register]|
    local zPack = {psName, piCount}
    table.insert(self.zaGenerals, zPack)
end

-- |[ =========================== VoiceData:fnLocateGeneralCallout() =========================== ]|
--Locates and returns a general callout package, or nil if not found.
function VoiceData:fnLocateGeneralCallout(psName)
    
    -- |[Argument Check]|
    if(psName == nil) then return nil end

    -- |[Scan]|
    for i = 1, #self.zaGenerals, 1 do
        if(self.zaGenerals[i][1] == psName) then
            return self.zaGenerals[i]
        end
    end
    
    -- |[Not Found]|
    return nil
end

-- |[ ============================ VoiceData:fnAddAbilityCallout() ============================= ]|
--Registers an ability callout package.
function VoiceData:fnAddAbilityCallout(psName, piCount) 
    
    -- |[Argument Check]|
    if(psName  == nil) then return end
    if(piCount == nil) then return end
    
    -- |[Duplicate Check]|
    local zCheckPack = self:fnLocateAbilityCallout(psName)
    if(zCheckPack ~= nil) then
        io.write("VoiceData:fnAddGeneralCallout() - Warning, already has ability callout " .. psName .. " for character " .. self.sLocalName .. "\n")
        return
    end
    
    -- |[Register]|
    local zPack = {psName, piCount}
    table.insert(self.zaAbilities, zPack)
end

-- |[ =========================== VoiceData:fnLocateAbilityCallout() =========================== ]|
--Locate and returns an ability callout package, or nil if not found.
function VoiceData:fnLocateAbilityCallout(psName) 
    
    -- |[Argument Check]|
    if(psName == nil) then return nil end

    -- |[Scan]|
    for i = 1, #self.zaAbilities, 1 do
        if(self.zaAbilities[i][1] == psName) then
            return self.zaAbilities[i]
        end
    end
    
    -- |[Not Found]|
    return nil
end
