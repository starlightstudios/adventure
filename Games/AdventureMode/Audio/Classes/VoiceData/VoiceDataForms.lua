-- |[ ==================================== VoiceData Forms ===================================== ]|
--Form handlers, including registering and querying.

-- |[ =============================== VoiceData:fnRegisterForm() =============================== ]|
--Creates a new form package, or barks an error upon duplication.
function VoiceData:fnRegisterForm(psName, psPrefix, psSoundPath)
    
    -- |[Argument Check]|
    if(psName      == nil) then return end
    if(psPrefix    == nil) then return end
    if(psSoundPath == nil) then return end

    -- |[Duplicate Check]|
    local zCheckForm = self:fnLocateForm(psName)
    if(zCheckForm ~= nil) then
        io.write("VoiceData:fnRegisterForm() - Warning, form " .. psName .. " already exists for character " .. self.sLocalName .. "\n")
        return
    end

    -- |[Create]|
    local zNewForm = {}
    zNewForm.sName = psName
    zNewForm.sPrefix = psPrefix
    zNewForm.sCallPrefix = ""
    zNewForm.sEffectName = "Null"
    zNewForm.iOffenseCount = 0
    zNewForm.iRestoreCount = 0
    zNewForm.iSpecialCount = 0

    -- |[Process]|
    --The Call Prefix is the prefix used when calling the sound. Because the | is used as a delimiter, we need
    -- to place a \\ in front of each |.
    for i = 1, string.len(zNewForm.sPrefix), 1 do
        local sLetter = string.sub(zNewForm.sPrefix, i, i)
        if(sLetter == "|") then
            zNewForm.sCallPrefix = zNewForm.sCallPrefix .. "\\|"
        else
            zNewForm.sCallPrefix = zNewForm.sCallPrefix .. sLetter
        end
    end
    
    -- |[Register]|
    table.insert(self.zaForms, zNewForm)
    
    -- |[Load Sounds]|
    --By default, sounds are meant to have the pattern "Offense", "Restore", "Special". This must be called
    -- after registering.
    self:fnLoadFormSounds(psName, psSoundPath, "Offense", "Restore", "Special") 
end

-- |[ ============================= VoiceData:fnSetEffectOnForm() ============================== ]|
--Creates a new form package, or barks an error upon duplication.
function VoiceData:fnSetEffectOnForm(psFormName, psEffectName)
    
    -- |[Argument Check]|
    if(psFormName   == nil) then return end
    if(psEffectName == nil) then return end

    -- |[Form Check]|
    local zCheckForm = self:fnLocateForm(psFormName)
    if(zCheckForm == nil) then
        io.write("VoiceData:fnSetEffectOnForm() - Warning, form " .. psFormName .. " not found on character " .. self.sLocalName .. "\n")
        return
    end
    
    -- |[Set]|
    zCheckForm.sEffectName = psEffectName
    
end

-- |[ ============================== VoiceData:fnLoadFormSounds() ============================== ]|
function VoiceData:fnLoadFormSounds(psFormName, psFilePath, psOffensePattern, psRestorePattern, psSpecialPattern) 
    
    -- |[Argument Check]|
    if(psFormName       == nil) then return end
    if(psFilePath       == nil) then return end
    if(psOffensePattern == nil) then return end
    if(psRestorePattern == nil) then return end
    if(psSpecialPattern == nil) then return end
    
    --Constants.
    local ciByteA = string.byte("A")
    
    -- |[Get Form]|
    local zForm = self:fnLocateForm(psFormName)
    if(zForm == nil) then return end
    
    -- |[Resolve Counts]|
    --Use the subroutine to figure out how many offense/restore/special callous there are. 0 is valid if none are found.
    zForm.iOffenseCount = self:fnCountFilesMeetingPattern(psFilePath .. psOffensePattern)
    zForm.iRestoreCount = self:fnCountFilesMeetingPattern(psFilePath .. psRestorePattern)
    zForm.iSpecialCount = self:fnCountFilesMeetingPattern(psFilePath .. psSpecialPattern)
    
    -- |[Resolve Sound Properties]|
    --Properties are common to all pain types.
    local sPropertyString = "Sound|Stream|Memory|"
    if(self.iAbstractVolSlot ~= nil and self.iAbstractVolSlot ~= -1) then
        sPropertyString = sPropertyString .. "AbstractVolSlot:" .. self.iAbstractVolSlot .. "|"
    end

    -- |[Load Offense Skills]|
    local sStartLetter = "A"
    local sFinalLetter = string.char(ciByteA + zForm.iOffenseCount - 1)
    self:fnRegisterPattern(zForm.sPrefix .. "Offense", psFilePath .. psOffensePattern, sStartLetter, sFinalLetter, sPropertyString)

    -- |[Load Restore Skills]|
    sFinalLetter = string.char(ciByteA + zForm.iRestoreCount - 1)
    self:fnRegisterPattern(zForm.sPrefix .. "Restore", psFilePath .. psRestorePattern, sStartLetter, sFinalLetter, sPropertyString)

    -- |[Load Special Skills]|
    sFinalLetter = string.char(ciByteA + zForm.iSpecialCount - 1)
    self:fnRegisterPattern(zForm.sPrefix .. "Special", psFilePath .. psSpecialPattern, sStartLetter, sFinalLetter, sPropertyString)
end

-- |[ ================================ VoiceData:fnLocateForm() ================================ ]|
--Returns the form package with the given name, or nil if not found.
function VoiceData:fnLocateForm(psName)

    -- |[Argument Check]|
    if(psName == nil) then return nil end

    -- |[Scan]|
    for i = 1, #self.zaForms, 1 do
        if(self.zaForms[i].sName == psName) then
            return self.zaForms[i]
        end
    end
    
    -- |[Not Found]|
    return nil
end