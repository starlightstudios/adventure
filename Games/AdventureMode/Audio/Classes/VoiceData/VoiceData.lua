-- |[ ================================== VoiceData Class File ================================== ]|
--Class file for VoiceData. Call this once at initialization, particularly before the audio loads.

--VoiceData is used to store character callouts during combat, organizing which voice lines are
-- available for which character and under which tag.

-- |[ =========== Diagnostic Flags =========== ]|
--The Audio files register before 000 Variables.lua is called, so these diagnostic flags are here.
Debug_RegisterFlag("Voice: All", false)
Debug_RegisterFlag("Voice: All Toggle", false) --Toggles off after voice execution finishes. Used to diagnose specific abilities.
Debug_RegisterFlag("Voice: Creation", false)
Debug_RegisterFlag("Voice: Execution", false)
Debug_RegisterFlag("Voice: Pain", false)

--Abstract Volume Constants
gciAbstractVol_Izana = 0
gciAbstractVol_Caelyn = 1
gciAbstractVol_Cyrano = 2
gciAbstractVol_Mei = 3
gciAbstractVol_Florentina = 4

-- |[ ============ Class Members ============= ]|
-- |[System]|
VoiceData = {}
VoiceData.__index = VoiceData
VoiceData.sLocalName = "Default"
VoiceData.sFormVarPath = "Default"
VoiceData.iLastCalloutTick = 0
VoiceData.iCalloutChanceBoost = 0
VoiceData.iAbstractVolSlot = -1

-- |[Pain]|
VoiceData.sPainPattern = ""
VoiceData.sPainCallPattern = ""
VoiceData.iPainLoCount = 0
VoiceData.iPainMdCount = 0
VoiceData.iPainHiCount = 0

-- |[Lists]|
VoiceData.zaForms     = {} --Uses Form Prototype below
VoiceData.zaGenerals  = {} --Format: {"Name", iCount}
VoiceData.zaAbilities = {} --Format: {"Name", iCount}

-- |[Form Prototype]|
--This describe a form, like "Human", "Alraune", etc. These have callouts for action types.
--zForm = {}
--zForm.sName = "Human"
--zForm.sPrefix = "Callout|Mei|Fencer|"
--zForm.sCallPrefix = "Callout\\|Mei\\|Fencer\\|"
--zForm.sEffectName = "Null"
--zForm.iOffenseCount = 5
--zForm.iRestoreCount = 4
--zForm.iSpecialCount = 3

-- |[ ============ Class Methods ============= ]|
--System
function VoiceData:new(psName, psFormVarPath) end

--Execution
function VoiceData:fnHandlePain(piTimer, piTargetID)                                 end
function VoiceData:fnHandlePreAction(psCallString, piTimer)                          end
function VoiceData:fnHandlePostAction(psCallString, piTimer)                         end
function VoiceData:fnHandleSection(psUserName, pzForm, psSection, psaListOfCallouts) end

--Form Handling
function VoiceData:fnRegisterForm(psName, psPrefix, psSoundPath)                                                   end
function VoiceData:fnSetEffectOnForm(psFormName, psEffectName)                                                     end
function VoiceData:fnLoadFormSounds(psFormName, psFilePath, psOffensePattern, psRestorePattern, psSpecialPattern)  end
function VoiceData:fnLocateForm(psName)                                                                            end

--General/Ability Lists
function VoiceData:fnAddGeneralCallout(psName, piCount) end
function VoiceData:fnLocateGeneralCallout(psName)       end
function VoiceData:fnAddAbilityCallout(psName, piCount) end
function VoiceData:fnLocateAbilityCallout(psName)       end

--Loading
function VoiceData:fnCountFilesMeetingPattern(psPattern)                                           end
function VoiceData:fnRegisterPattern(psName, psPath, psStartLetter, psEndLetter, psPropertyString) end
function VoiceData:fnLoadGeneralCallout(psName, psFilePath)                                        end
function VoiceData:fnLoadAbilityCallout(psName, psFilePath)                                        end
function VoiceData:fnLoadAbilityCalloutSequence(psaNames, psNamePattern, psPath)                   end
function VoiceData:fnLoadPains(psInternalPattern, psFilePath, psFilePattern)                       end

-- |[ ========== Class Constructor =========== ]|
--Must have a unique name for the character in question.
function VoiceData:new(psName, psFormVarPath)
    
    -- |[ ========= Setup ========== ]|
    -- |[Argument Check]|
    if(psName        == nil) then psName        = "Default" end
    if(psFormVarPath == nil) then psFormVarPath = "Default" end
    
    -- |[Create]|
    local zObject = {}
    setmetatable(zObject, VoiceData)
    
    -- |[ ======== Members ========= ]|
    --Local member.
    zObject.sLocalName = psName
    zObject.sFormVarPath = psFormVarPath
    
    --Reset local lists so they don't use the global class listings.
    zObject.zaForms     = {}
    zObject.zaGenerals  = {}
    zObject.zaAbilities = {}
    
    --Re-hash the functions as they may have changed since the class was initialized.
    --zObject.fnAbilityAnimate   = fnDefaultAbilityAnimate
    
    -- |[ ======= Finish Up ======== ]|
    --Diagnostics.
    local bDiagnostics = Debug_GetFlag("Voice: All") or Debug_GetFlag("Voice: All Toggle") or Debug_GetFlag("Voice: Creation")
    Debug_PushPrint(bDiagnostics, "VoiceData:new() - Registered new voice package " .. psName .. ".\n")
    Debug_PopPrint("Completed.\n")
    
    --Return object.
    return zObject
end

-- |[ ========= Call Implementations ========= ]|
LM_ExecuteScript(fnResolvePath() .. "VoiceDataExecution.lua")
LM_ExecuteScript(fnResolvePath() .. "VoiceDataForms.lua")
LM_ExecuteScript(fnResolvePath() .. "VoiceDataGeneralAbility.lua")
LM_ExecuteScript(fnResolvePath() .. "VoiceDataLoading.lua")
