-- |[ =================================== World Interaction ==================================== ]|
--Sound effects that involve things that happen in the world, in general. Basically, doesn't fit into another category.
-- To make them slightly easier (or harder) to find, they are divided into subfolders.

-- |[Field Abilities]|
local sFieldAbilityPath = fnResolvePath() .. "FieldAbilities/"
AudioManager_Register("FieldAbi|PounceImpact",        "AsSound", "AsSample", sFieldAbilityPath .. "Pounce Impact.ogg")
AudioManager_Register("FieldAbi|Pounce",              "AsSound", "AsSample", sFieldAbilityPath .. "Pounce.ogg")
AudioManager_Register("FieldAbi|BallLightningImpact", "AsSound", "AsSample", sFieldAbilityPath .. "Ball Lightning Impact.ogg")
AudioManager_Register("FieldAbi|BallLightningLaunch", "AsSound", "AsSample", sFieldAbilityPath .. "Ball Lightning Launch.ogg")

-- |[Firearms]|
local sFirearmPath = fnResolvePath() .. "Firearms/"
AudioManager_Register("Firearm|RifleFire",  "AsSound", "AsSample", sFirearmPath .. "RifleFire.ogg")
AudioManager_Register("Firearm|WorkAction", "AsSound", "AsSample", sFirearmPath .. "WorkAction.ogg")
AudioManager_Register("Firearm|ReadyGun",   "AsSound", "AsSample", sFirearmPath .. "ReadyGun.ogg")
AudioManager_Register("Firearm|LowerGun",   "AsSound", "AsSample", sFirearmPath .. "LowerGun.ogg")

-- |[Futuristic]|
--Sound effects IN SPAAAAAACE. Mostly used in chapter 5 or other chapters where golems show up with their
-- lazers and pew pew.
local sFuturisticPath = fnResolvePath() .. "Futuristic/"
AudioManager_Register("World|AdminLightsOn",  "AsSound", "AsSample", sFuturisticPath .. "AdminLightsOn.ogg")
AudioManager_Register("World|AdminPowerUp",   "AsSound", "AsSample", sFuturisticPath .. "AdminPowerUp.ogg")
AudioManager_Register("World|AutoDoorFail",   "AsSound", "AsSample", sFuturisticPath .. "AutoDoorFail.ogg")
AudioManager_Register("World|AutoDoorOpen",   "AsSound", "AsSample", sFuturisticPath .. "AutoDoorOpen.ogg")
AudioManager_Register("World|BlastDoorClose", "AsSound", "AsSample", sFuturisticPath .. "BlastDoorClose.ogg")
AudioManager_Register("World|Keycard",        "AsSound", "AsSample", sFuturisticPath .. "Keycard.wav")
AudioManager_Register("World|RemoteDoor",     "AsSound", "AsSample", sFuturisticPath .. "RemoteDoor.wav")
AudioManager_Register("World|SparksA",        "AsSound", "AsSample", sFuturisticPath .. "SparksA.ogg")
AudioManager_Register("World|SparksB",        "AsSound", "AsSample", sFuturisticPath .. "SparksB.ogg")
AudioManager_Register("World|SparksC",        "AsSound", "AsSample", sFuturisticPath .. "SparksC.ogg")
AudioManager_Register("World|SparksD",        "AsSound", "AsSample", sFuturisticPath .. "SparksD.ogg")
AudioManager_Register("World|TerminalOn",     "AsSound", "AsSample", sFuturisticPath .. "TerminalOn.ogg")
AudioManager_Register("World|TerminalOff",    "AsSound", "AsSample", sFuturisticPath .. "TerminalOff.ogg")
AudioManager_Register("World|TramArrive",     "AsSound", "AsSample", sFuturisticPath .. "TramArrive.ogg")
AudioManager_Register("World|TramRunning",    "AsSound", "AsStream", sFuturisticPath .. "TramRunning.ogg")
AudioManager_Register("World|TramStart",      "AsSound", "AsSample", sFuturisticPath .. "TramStart.ogg")
AudioManager_Register("World|TramStop",       "AsSound", "AsSample", sFuturisticPath .. "TramStop.ogg")
AudioManager_Register("World|ValveSqueak",    "AsSound", "AsSample", sFuturisticPath .. "ValveSqueak.ogg")

-- |[Horror]|
--Maximum fear.
local sHorrorPath = fnResolvePath() .. "Horror/"
AudioManager_Register("World|Chaos",         "AsSound", "AsSample", sHorrorPath .. "Chaos.ogg")
AudioManager_Register("World|Heartbeat",     "AsSound", "AsSample", sHorrorPath .. "HeartBeat.ogg")
AudioManager_Register("World|PuzzleComplete","AsSound", "AsSample", sHorrorPath .. "PuzzleComplete.ogg")
AudioManager_Register("World|ThingApproach", "AsSound", "AsSample", sHorrorPath .. "ThingApproach.ogg")
AudioManager_Register("World|ThingDistant",  "AsSound", "AsSample", sHorrorPath .. "ThingDistant.ogg")
AudioManager_Register("World|ThingRecede",   "AsSound", "AsSample", sHorrorPath .. "ThingRecede.ogg")
AudioManager_Register("World|ThingSmash",    "AsSound", "AsSample", sHorrorPath .. "ThingSmash.ogg")

-- |[Impacts]|
--Includes explosions, bumping into things, and gunshots.
local sImpactsPath = fnResolvePath() .. "Impacts/"
AudioManager_Register("World|BigExplosion",          "AsSound", "AsSample", sImpactsPath .. "BigExplosion.ogg")
AudioManager_Register("World|BigGlassShatter",       "AsSound", "AsSample", sImpactsPath .. "BigGlassShatter.ogg")
AudioManager_Register("World|BoardBreak",            "AsSound", "AsSample", sImpactsPath .. "BoardBreak.ogg")
AudioManager_Register("World|BulletImpact0",         "AsSound", "AsSample", sImpactsPath .. "BulletImpact0.ogg")
AudioManager_Register("World|BulletImpact1",         "AsSound", "AsSample", sImpactsPath .. "BulletImpact1.ogg")
AudioManager_Register("World|BulletImpact2",         "AsSound", "AsSample", sImpactsPath .. "BulletImpact2.ogg")
AudioManager_Register("World|BulletImpact3",         "AsSound", "AsSample", sImpactsPath .. "BulletImpact3.ogg")
AudioManager_Register("World|BulletImpactImportant", "AsSound", "AsSample", sImpactsPath .. "BulletImpactImportant.ogg")
AudioManager_Register("World|Explosion",             "AsSound", "AsSample", sImpactsPath .. "FarExplosion.ogg")
AudioManager_Register("World|GlassFall",             "AsSound", "AsSample", sImpactsPath .. "GlassFall.ogg")
AudioManager_Register("World|HardHit",               "AsSound", "AsSample", sImpactsPath .. "HardHit.ogg")
AudioManager_Register("World|HardLanding",           "AsSound", "AsSample", sImpactsPath .. "HardLanding.ogg")
AudioManager_Register("World|LaserDistant",          "AsSound", "AsSample", sImpactsPath .. "Impact_Laser_Distant.ogg")
AudioManager_Register("World|Knock",                 "AsSound", "AsSample", sImpactsPath .. "Knock.ogg")
AudioManager_Register("World|MachineGun",            "AsSound", "AsSample", sImpactsPath .. "MachineGun.ogg")
AudioManager_Register("World|Slap",                  "AsSound", "AsSample", sImpactsPath .. "Slap.ogg")
AudioManager_Register("World|Slide",                 "AsSound", "AsSample", sImpactsPath .. "Slide.ogg")
AudioManager_Register("World|Thump",                 "AsSound", "AsSample", sImpactsPath .. "Thump.wav")
AudioManager_Register("World|TranqShot",             "AsSound", "AsSample", sImpactsPath .. "TranqShot.ogg")
AudioManager_Register("World|VeryBigExplosion",      "AsSound", "AsSample", sImpactsPath .. "VeryBigExplosion.ogg")
AudioManager_Register("World|WhipCrack",             "AsSound", "AsSample", sImpactsPath .. "WhipCrack.ogg")

--Punches
AudioManager_Register("World|Punch00", "AsSound", "AsSample", sImpactsPath .. "Punch00.ogg")
AudioManager_Register("World|Punch01", "AsSound", "AsSample", sImpactsPath .. "Punch01.ogg")
AudioManager_Register("World|Punch02", "AsSound", "AsSample", sImpactsPath .. "Punch02.ogg")
AudioManager_Register("World|Punch03", "AsSound", "AsSample", sImpactsPath .. "Punch03.ogg")

--Mugging.
AudioManager_Register("World|MugHit",     "AsSound", "AsSample", sImpactsPath .. "Thump.wav")
AudioManager_Register("World|MugSuccess", "AsSound", "AsSample", sImpactsPath .. "Slap.ogg")
AudioManager_Register("World|MugMiss",    "AsSound", "AsSample", sImpactsPath .. "MugMiss.ogg")

-- |[Movement]|
--All footstep sounds, climbing, as well as opening chests and doors.
local sMovementPath = fnResolvePath() .. "Movement/"
AudioManager_Register("World|ClimbLadder",  "AsSound", "AsSample", sMovementPath .. "ClimbLadder.ogg")
AudioManager_Register("World|LimbCladder",  "AsSound", "AsSample", sMovementPath .. "LimbCladder.ogg")
AudioManager_Register("World|OpenChest",    "AsSound", "AsSample", sMovementPath .. "OpenChest.ogg")
AudioManager_Register("World|OpenDoor",     "AsSound", "AsSample", sMovementPath .. "OpenDoor.wav")
AudioManager_Register("World|Flee",         "AsSound", "AsSample", sMovementPath .. "Flee.ogg")
AudioManager_Register("World|FootstepL_00", "AsSound", "AsSample", sMovementPath .. "Running_StepL_00.ogg")
AudioManager_Register("World|FootstepR_00", "AsSound", "AsSample", sMovementPath .. "Running_StepR_00.ogg")
AudioManager_Register("World|TakeArmor",    "AsSound", "AsSample", sMovementPath .. "TakeArmor.wav")
AudioManager_Register("World|TakeItem",     "AsSound", "AsSample", sMovementPath .. "TakeItem.wav")
AudioManager_Register("World|TakeWeapon",   "AsSound", "AsSample", sMovementPath .. "TakeWeapon.wav")
AudioManager_Register("World|Jump",         "AsSound", "AsSample", sMovementPath .. "Jump.ogg")
AudioManager_Register("World|Land",         "AsSound", "AsSample", sMovementPath .. "Land.ogg")
AudioManager_Register("World|GlassMove",    "AsSound", "AsSample", sMovementPath .. "GlassMove.ogg")
AudioManager_Register("World|Wading",       "AsSound", "AsSample", sMovementPath .. "Wading.ogg")

-- |[Nature]|
--Pertaining to the natural world. Includes animal noises, water, trees, rocks falling, everyone dying.
local sNaturePath = fnResolvePath() .. "Nature/"
AudioManager_Register("World|BigSplash",     "AsSound", "AsSample", sNaturePath .. "BigSplash.ogg")
AudioManager_Register("World|Chirp",         "AsSound", "AsSample", sNaturePath .. "Chirp.ogg")
AudioManager_Register("World|DogBark",       "AsSound", "AsSample", sNaturePath .. "DogBark.ogg")
AudioManager_Register("World|FreezeOver",    "AsSound", "AsSample", sNaturePath .. "FreezeOver.ogg")
AudioManager_Register("World|Goat0",         "AsSound", "AsSample", sNaturePath .. "Goat0.ogg")
AudioManager_Register("World|Goat1",         "AsSound", "AsSample", sNaturePath .. "Goat1.ogg")
AudioManager_Register("World|Goat2",         "AsSound", "AsSample", sNaturePath .. "Goat2.ogg")
AudioManager_Register("World|Goat3",         "AsSound", "AsSample", sNaturePath .. "Goat3.ogg")
AudioManager_Register("World|LittleSplashA", "AsSound", "AsSample", sNaturePath .. "LittleSplashA.ogg")
AudioManager_Register("World|LittleSplashB", "AsSound", "AsSample", sNaturePath .. "LittleSplashB.ogg")
AudioManager_Register("World|MedSplash",     "AsSound", "AsSample", sNaturePath .. "MedSplash.ogg")
AudioManager_Register("World|RockRumbleA",   "AsSound", "AsSample", sNaturePath .. "RockRumbleA.ogg")
AudioManager_Register("World|RockRumbleB",   "AsSound", "AsSample", sNaturePath .. "RockRumbleB.ogg")
AudioManager_Register("World|RockRumbleC",   "AsSound", "AsSample", sNaturePath .. "RockRumbleC.ogg")

-- |[Magic]|
--MAGIC!!!!!
local sMagicPath = fnResolvePath() .. "Magic/"
AudioManager_Register("World|MagicA", "AsSound", "AsSample", sMagicPath .. "MagicA.ogg")
AudioManager_Register("World|MagicB", "AsSound", "AsSample", sMagicPath .. "MagicB.ogg")

-- |[Rubber]|
--Used during the rubber sequences in chapter 1.
local sRubberPath = fnResolvePath() .. "Rubber/"
AudioManager_Register("Rubber|MeiShout",  "AsSound", "AsSample", sRubberPath .. "MeiShout.ogg")
AudioManager_Register("Rubber|ChangeA",   "AsSound", "AsSample", sRubberPath .. "RubberChangeA.ogg")
AudioManager_Register("Rubber|ChangeB",   "AsSound", "AsSample", sRubberPath .. "RubberChangeB.ogg")
AudioManager_Register("Rubber|ChangeC",   "AsSound", "AsSample", sRubberPath .. "RubberChangeC.ogg")
AudioManager_Register("Rubber|SpreadA",   "AsSound", "AsSample", sRubberPath .. "RubberSpreadA.ogg")
AudioManager_Register("Rubber|SpreadB",   "AsSound", "AsSample", sRubberPath .. "RubberSpreadB.ogg")
AudioManager_Register("Rubber|SpreadC",   "AsSound", "AsSample", sRubberPath .. "RubberSpreadC.ogg")
AudioManager_Register("Rubber|SpreadBig", "AsSound", "AsSample", sRubberPath .. "RubberSpreadBig.ogg")
AudioManager_Register("Rubber|Stretch",   "AsSound", "AsSample", sRubberPath .. "RubberStretch.ogg")

-- |[Shouting]|
local sShoutPath = fnResolvePath() .. "Shouting/"
AudioManager_Register("World|Applause",    "AsSound", "AsSample", sShoutPath .. "Applause.ogg")
AudioManager_Register("World|CheersShort", "AsSound", "AsSample", sShoutPath .. "CheersShort.ogg")

-- |[Switches]|
--Switches, levers, and all that.
local sSwitchesPath = fnResolvePath() .. "Switches/"
AudioManager_Register("World|ButtonClick",      "AsSound", "AsSample", sSwitchesPath .. "ButtonClick.ogg")
AudioManager_Register("World|FlipSwitch",       "AsSound", "AsSample", sSwitchesPath .. "FlipSwitch.wav")
AudioManager_Register("World|PickingLock",      "AsSound", "AsSample", sSwitchesPath .. "PickingLock.ogg")
AudioManager_Register("World|LockOpen",         "AsSound", "AsSample", sSwitchesPath .. "LockOpen.ogg")
AudioManager_Register("World|DistantDoorOpen",  "AsSound", "AsSample", sSwitchesPath .. "DistantDoorClose.ogg")
AudioManager_Register("World|DistantDoorClose", "AsSound", "AsSample", sSwitchesPath .. "DistantDoorOpen.ogg")
AudioManager_Register("World|BlockSlide",       "AsSound", "AsSample", sSwitchesPath .. "BlockSlide.ogg")

-- |[System]|
--Used by the UI or other world events that often are chapter-independent.
local sSystemPath = fnResolvePath() .. "System/"
AudioManager_Register("World|Awoo",        "AsSound", "AsSample", sSystemPath .. "Awoo.ogg")
AudioManager_Register("World|Catalyst",    "AsSound", "AsSample", sSystemPath .. "Catalyst Signal.ogg")
AudioManager_Register("World|Fall",        "AsSound", "AsSample", sSystemPath .. "Fall.wav")
AudioManager_Register("World|Firework0",   "AsSound", "AsSample", sSystemPath .. "Firework0.ogg")
AudioManager_Register("World|Firework1",   "AsSound", "AsSample", sSystemPath .. "Firework1.ogg")
AudioManager_Register("World|Firework2",   "AsSound", "AsSample", sSystemPath .. "Firework2.ogg")
AudioManager_Register("World|Flash",       "AsSound", "AsSample", sSystemPath .. "Flash.ogg")
AudioManager_Register("World|GetCatalyst", "AsSound", "AsSample", sSystemPath .. "GetCatalyst.ogg")
AudioManager_Register("World|Noisemaker",  "AsSound", "AsSample", sSystemPath .. "Noisemaker.ogg")
AudioManager_Register("World|Stairs",      "AsSound", "AsSample", sSystemPath .. "Stairs.ogg")
AudioManager_Register("World|Stairs",      "AsSound", "AsSample", sSystemPath .. "Stairs.ogg")

-- |[Warping]|
--Used for the warp sequence.
local sWarpPath = fnResolvePath() .. "Warp/"
AudioManager_Register("World|Teleport",    "AsSound", "AsSample", sWarpPath .. "Teleport.wav")
AudioManager_Register("World|WarpAppear",  "AsSound", "AsSample", sWarpPath .. "Warp Appear.ogg")
AudioManager_Register("World|WarpDecrypt", "AsSound", "AsSample", sWarpPath .. "Warp Decrypt.ogg")
AudioManager_Register("World|WarpMove",    "AsSound", "AsSample", sWarpPath .. "Warp Move.ogg")
