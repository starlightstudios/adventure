-- |[ ====================================== Puzzle Fight ====================================== ]|
--SFX for the puzzle fight minigame.

-- |[Field Abilities]|
local sBasePath = fnResolvePath()
AudioManager_Register("Puzzle|ChangeDirections", "AsSound", "AsSample", sBasePath .. "ChangeDirections.ogg")
AudioManager_Register("Puzzle|LetterPrint",      "AsSound", "AsSample", sBasePath .. "LetterPrint.ogg")
AudioManager_Register("Puzzle|TitleAppear",      "AsSound", "AsSample", sBasePath .. "TitleAppear.ogg")
AudioManager_Register("Puzzle|PowerUp",          "AsSound", "AsSample", sBasePath .. "PowerUp.ogg")