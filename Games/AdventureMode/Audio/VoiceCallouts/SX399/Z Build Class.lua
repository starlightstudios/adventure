-- |[ ================================== SX-399 Voice Loading ================================== ]|
--Setup.
local sBasePath = fnResolvePath()
local sPrefix = ""
LM_StartTimer("Voice Load")

-- |[Repeat Check]|
--Don't re-load data if the class already exists.
local zCheckData = VoiceDataList:fnLocateEntry("SX-399")
if(zCheckData ~= nil) then return end

-- |[Create VoiceData]|
--Basic class.
local zVoiceData = VoiceData:new("SX-399", "Root/Variables/Global/SX-399/sVoiceJob")
zVoiceData.iAbstractVolSlot = gciAbstractVol_SX399
VoiceDataList:fnRegister(zVoiceData)

-- |[Form Registry]|
zVoiceData:fnRegisterForm("Shocktrooper", "Callout|SX-399|Shocktrooper|", sBasePath .. "00Shocktrooper/")

--Set forms which have a special effect associated with them. Not all forms use this, the default is "Null".
--zVoiceData:fnSetEffectOnForm("Bee", "Bee Hivemind")

-- |[Pain Handlers]|
zVoiceData:fnLoadPains("Callout|SX-399|Pain|", sBasePath .. "99Pain/", "Pain_")

-- |[Other Callouts]|
--General Callouts
zVoiceData:fnLoadGeneralCallout("Swing", "Callout|SX-399|General|Swing", sBasePath .. "20General/", "00_Swing")

--Ability Callouts, automated. Abilities are assumed to have the name pattern AA_abinameA, AA_abinameB, AB_secondabiA, etc.
local sAbiPath = sBasePath .. "21Abilities/"
local sAbiNames = {"MeltaBlast", "Sear", "FlakRound", "PalookaPunch", "Scavenge", "ScrapRepair", "Improvise", "Cheer", "ThunderboltSlug"}
zVoiceData:fnLoadAbilityCalloutSequence(sAbiNames, "Callout|SX-399|Ability|", sAbiPath)

local fElapsedTime = LM_FinishTimer("Voice Load")
--io.write("SX-399 lines took " .. fElapsedTime .. " to load.\n")