-- |[ ================================= Christine Voice Loading ================================ ]|
--Setup.
local sBasePath = fnResolvePath()
local sPrefix = ""
LM_StartTimer("Voice Load")

-- |[Repeat Check]|
--Don't re-load data if the class already exists.
local zCheckData = VoiceDataList:fnLocateEntry("Christine")
if(zCheckData ~= nil) then return end

-- |[Create VoiceData]|
--Basic class.
local zVoiceData = VoiceData:new("Christine", "Root/Variables/Global/Christine/sVoiceJob")
zVoiceData.iAbstractVolSlot = gciAbstractVol_Christine
VoiceDataList:fnRegister(zVoiceData)

-- |[Form Registry]|
--Christine only has one form and all the others use it as the base.
zVoiceData:fnRegisterForm("Lancer", "Callout|Christine|Lancer|", sBasePath .. "00Lancer/")

--Set forms which have a special effect associated with them. Not all forms use this, the default is "Null".
--zVoiceData:fnSetEffectOnForm("Bee", "Bee Hivemind")

-- |[Pain Handlers]|
zVoiceData:fnLoadPains("Callout|Christine|Pain|", sBasePath .. "99Pain/", "Pain_")

-- |[Other Callouts]|
--General Callouts
zVoiceData:fnLoadGeneralCallout("Swing", "Callout|Christine|General|Swing", sBasePath .. "20General/", "00_Swing")

--Lancer
local sAbiPath = sBasePath .. "00Lancer/"
local sAbiNames = {"Rally", "ExposeWeakness", "Shatter", "Inspire", "Taunt", "TakePoint", "Bluff", "Batter"}
zVoiceData:fnLoadAbilityCalloutSequence(sAbiNames, "Callout|Christine|Ability|", sAbiPath)

--Repair Unit
sAbiPath = sBasePath .. "01RepairUnit/"
sAbiNames = {"Sweep", "Ram", "Fix", "Cover", "TuneUp", "HyperRepair"}
zVoiceData:fnLoadAbilityCalloutSequence(sAbiNames, "Callout|Christine|Ability|", sAbiPath)

--Starseer
sAbiPath = sBasePath .. "02Starseer/"
sAbiNames = {"SpearOfLight", "TouchOfDarkness", "GraspOfTheVoid", "Sunder", "Gravity", "Eclipse", "Supernova"}
zVoiceData:fnLoadAbilityCalloutSequence(sAbiNames, "Callout|Christine|Ability|", sAbiPath)

--Livewire
sAbiPath = sBasePath .. "03Livewire/"
sAbiNames = {"Zap", "ChainZap", "Fry", "Grounded", "Overcharge", "Jumpstart"}
zVoiceData:fnLoadAbilityCalloutSequence(sAbiNames, "Callout|Christine|Ability|", sAbiPath)

--Drone
sAbiPath = sBasePath .. "04Drone/"
sAbiNames = {"RoundhouseKick", "QuickdrySplash", "Jackhammer", "Overclock", "AutoRepair", "Bounceback"}
zVoiceData:fnLoadAbilityCalloutSequence(sAbiNames, "Callout|Christine|Ability|", sAbiPath)

--Brass Brawler
sAbiPath = sBasePath .. "05BrassBrawler/"
sAbiNames = {"HeatSiphon", "SteamBlast", "PercussiveMaintenance", "Bash", "Pincushion", "Bloodboil", "Improvise", "Scavenge"}
zVoiceData:fnLoadAbilityCalloutSequence(sAbiNames, "Callout|Christine|Ability|", sAbiPath)

--Handmaiden
sAbiPath = sBasePath .. "06Handmaiden/"
sAbiNames = {"ColdTouch", "AbsorbHope", "Glower", "Influence", "Obliterate", "Glory", "Pulsate", "BeyondSight"}
zVoiceData:fnLoadAbilityCalloutSequence(sAbiNames, "Callout|Christine|Ability|", sAbiPath)

--Thunder Rat
sAbiPath = sBasePath .. "07ThunderRat/"
sAbiNames = {"Storm", "Hotwire", "Fireworks", "ShortCircuit", "Polarity", "BatteryDrainer", "Sparkshot", "Embolden"}
zVoiceData:fnLoadAbilityCalloutSequence(sAbiNames, "Callout|Christine|Ability|", sAbiPath)

--Commissar
sAbiPath = sBasePath .. "08Commissar/"
sAbiNames = {"PerfectStrike", "Slavedriver", "VirusModule", "XRayOcularUnits", "HighPowerShock", "SpotRepairs"}
zVoiceData:fnLoadAbilityCalloutSequence(sAbiNames, "Callout|Christine|Ability|", sAbiPath)

--Menialist
sAbiPath = sBasePath .. "09Menialist/"
sAbiNames = {"ImpromptuOrganization", "MightyPen", "OilRun", "TakeNotes", "Scheduling", "HoldMyCalls", "StressRelease"}
zVoiceData:fnLoadAbilityCalloutSequence(sAbiNames, "Callout|Christine|Ability|", sAbiPath)

-- |[Finish Up]|
local fElapsedTime = LM_FinishTimer("Voice Load")
--io.write("Christine lines took " .. fElapsedTime .. " to load.\n")