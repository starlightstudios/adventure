-- |[ ===================================== Vocal Effects ====================================== ]|
--Vocal Effects are special effects packages that are applied at runtime when a sound is played, making
-- the basic sound different. This is used to give odd effects to a character's voice, such as making
-- Bee Mei sound like she has a hive-mind.
--The effects can apply to any non-tempo streamed sound effect, not just voice callouts.

-- |[Types]|
local ciTypeRotate     = AudioManager_GetProperty("Bass Enumeration", "BASS_FX_BFX_ROTATE")
local ciTypeVolume     = AudioManager_GetProperty("Bass Enumeration", "BASS_FX_BFX_VOLUME")
local ciTypePeakEQ     = AudioManager_GetProperty("Bass Enumeration", "BASS_FX_BFX_PEAKEQ")
local ciTypeDampener   = AudioManager_GetProperty("Bass Enumeration", "BASS_FX_BFX_DAMP")
local ciTypeAutoWah    = AudioManager_GetProperty("Bass Enumeration", "BASS_FX_BFX_AUTOWAH")
local ciTypePhaser     = AudioManager_GetProperty("Bass Enumeration", "BASS_FX_BFX_PHASER")
local ciTypeChorus     = AudioManager_GetProperty("Bass Enumeration", "BASS_FX_BFX_CHORUS")
local ciTypeDistortion = AudioManager_GetProperty("Bass Enumeration", "BASS_FX_BFX_DISTORTION")
local ciTypeCompressor = AudioManager_GetProperty("Bass Enumeration", "BASS_FX_BFX_COMPRESSOR2")
local ciTypeEcho       = AudioManager_GetProperty("Bass Enumeration", "BASS_FX_BFX_ECHO4")
local ciTypePitchShift = AudioManager_GetProperty("Bass Enumeration", "BASS_FX_BFX_PITCHSHIFT")
local ciTypeReverb     = AudioManager_GetProperty("Bass Enumeration", "BASS_FX_BFX_FREEVERB")

-- |[Clear]|
--Clear existing profiles if any were left over.
AudioManager_SetProperty("Clear Effect Profiles")

-- |[ ===================================== Build Packages ===================================== ]|
-- |[Bee Hive Mind]|
AudioManager_SetProperty("Register Effect Profile",     "Bee Hivemind", ciTypeChorus)
AudioManager_SetProperty("Set Effect Profile Property", "Bee Hivemind", ciTypeChorus, "Dry Mix",    0.90)
AudioManager_SetProperty("Set Effect Profile Property", "Bee Hivemind", ciTypeChorus, "Wet Mix",    0.40)
AudioManager_SetProperty("Set Effect Profile Property", "Bee Hivemind", ciTypeChorus, "Feedback",   0.50)
AudioManager_SetProperty("Set Effect Profile Property", "Bee Hivemind", ciTypeChorus, "Min Sweep",  1.00)
AudioManager_SetProperty("Set Effect Profile Property", "Bee Hivemind", ciTypeChorus, "Max Sweep", 10.00)
AudioManager_SetProperty("Set Effect Profile Property", "Bee Hivemind", ciTypeChorus, "Rate",       5.00)
AudioManager_SetProperty("Set Effect Profile Property", "Bee Hivemind", ciTypeChorus, "Channel",     255)