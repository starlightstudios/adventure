-- |[ ================================ Florentina Voice Loading ================================ ]|
--Setup.
local sBasePath = fnResolvePath()
local sPrefix = ""
LM_StartTimer("Voice Load")

-- |[Repeat Check]|
--Don't re-load data if the class already exists.
local zCheckData = VoiceDataList:fnLocateEntry("Florentina")
if(zCheckData ~= nil) then return end

-- |[Create VoiceData]|
--Basic class.
local zVoiceData = VoiceData:new("Florentina", "Root/Variables/Global/Florentina/sVoiceJob")
zVoiceData.iAbstractVolSlot = gciAbstractVol_Florentina
VoiceDataList:fnRegister(zVoiceData)

-- |[Form Registry]|
zVoiceData:fnRegisterForm("Merchant", "Callout|Florentina|Merchant|", sBasePath .. "00Merchant/")

--Set forms which have a special effect associated with them. Not all forms use this, the default is "Null".
--zVoiceData:fnSetEffectOnForm("Bee", "Bee Hivemind")

-- |[Pain Handlers]|
zVoiceData:fnLoadPains("Callout|Florentina|Pain|", sBasePath .. "99Pain/", "Pain_")

-- |[Other Callouts]|
--General Callouts
zVoiceData:fnLoadGeneralCallout("Swing", "Callout|Florentina|General|Swing", sBasePath .. "20General/", "00_Swing")

--Ability Callouts
zVoiceData:fnLoadAbilityCallout("Encourage",       "Callout|Florentina|Ability|Encourage",       sBasePath .. "21Abilities/", "AA_Encourage")
zVoiceData:fnLoadAbilityCallout("Insult",          "Callout|Florentina|Ability|Insult",          sBasePath .. "21Abilities/", "AB_Insult")
zVoiceData:fnLoadAbilityCallout("TerriblePun",     "Callout|Florentina|Ability|TerriblePun",     sBasePath .. "21Abilities/", "AC_TerriblePun")
zVoiceData:fnLoadAbilityCallout("VeiledThreat",    "Callout|Florentina|Ability|VeiledThreat",    sBasePath .. "21Abilities/", "AD_VeiledThreat")
zVoiceData:fnLoadAbilityCallout("Counterargument", "Callout|Florentina|Ability|Counterargument", sBasePath .. "21Abilities/", "AE_Counterargument")
zVoiceData:fnLoadAbilityCallout("CalledShot",      "Callout|Florentina|Ability|CalledShot",      sBasePath .. "21Abilities/", "AF_CalledShot")
zVoiceData:fnLoadAbilityCallout("Fungus",          "Callout|Florentina|Ability|Fungus",          sBasePath .. "21Abilities/", "AG_Fungus")
zVoiceData:fnLoadAbilityCallout("Dissolve",        "Callout|Florentina|Ability|Dissolve",        sBasePath .. "21Abilities/", "AH_Dissolve")
zVoiceData:fnLoadAbilityCallout("FeedingFury",     "Callout|Florentina|Ability|FeedingFury",     sBasePath .. "21Abilities/", "AI_FeedingFury")
zVoiceData:fnLoadAbilityCallout("Symbiosis",       "Callout|Florentina|Ability|Symbiosis",       sBasePath .. "21Abilities/", "AJ_Symbiosis")
zVoiceData:fnLoadAbilityCallout("VineWhip",        "Callout|Florentina|Ability|VineWhip",        sBasePath .. "21Abilities/", "AK_VineWhip")
zVoiceData:fnLoadAbilityCallout("WhipTrip",        "Callout|Florentina|Ability|WhipTrip",        sBasePath .. "21Abilities/", "AL_WhipTrip")
zVoiceData:fnLoadAbilityCallout("PoisonDarts",     "Callout|Florentina|Ability|PoisonDarts",     sBasePath .. "21Abilities/", "AM_PoisonDarts")
zVoiceData:fnLoadAbilityCallout("Pickpocket",      "Callout|Florentina|Ability|Pickpocket",      sBasePath .. "21Abilities/", "AN_Pickpocket")
zVoiceData:fnLoadAbilityCallout("Haymaker",        "Callout|Florentina|Ability|Haymaker",        sBasePath .. "21Abilities/", "AO_Haymaker")
zVoiceData:fnLoadAbilityCallout("FromTheShadows",  "Callout|Florentina|Ability|FromTheShadows",  sBasePath .. "21Abilities/", "AP_FromTheShadows")
zVoiceData:fnLoadAbilityCallout("Intimidate",      "Callout|Florentina|Ability|Intimidate",      sBasePath .. "21Abilities/", "AQ_Intimidate")
zVoiceData:fnLoadAbilityCallout("DrainVitality",   "Callout|Florentina|Ability|DrainVitality",   sBasePath .. "21Abilities/", "AR_DrainVitality")
zVoiceData:fnLoadAbilityCallout("Regrowth",        "Callout|Florentina|Ability|Regrowth",        sBasePath .. "21Abilities/", "AS_Regrowth")
zVoiceData:fnLoadAbilityCallout("Tinker",          "Callout|Florentina|Ability|Tinker",          sBasePath .. "21Abilities/", "AT_Tinker")

local fElapsedTime = LM_FinishTimer("Voice Load")
--io.write("Florentina lines took " .. fElapsedTime .. " to load.\n")