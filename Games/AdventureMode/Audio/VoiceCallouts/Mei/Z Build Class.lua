-- |[ =================================== Mei Voice Loading ==================================== ]|
--Setup.
local sBasePath = fnResolvePath()
local sPrefix = ""
LM_StartTimer("Voice Load")

-- |[Repeat Check]|
--Don't re-load data if the class already exists.
local zCheckData = VoiceDataList:fnLocateEntry("Mei")
if(zCheckData ~= nil) then return end

-- |[Create VoiceData]|
--Basic class.
local zVoiceData = VoiceData:new("Mei", "Root/Variables/Global/Mei/sForm")
zVoiceData.iAbstractVolSlot = gciAbstractVol_Mei
VoiceDataList:fnRegister(zVoiceData)

-- |[Form Registry]|
zVoiceData:fnRegisterForm("Human",       "Callout|Mei|Fencer|",     sBasePath .. "00Human/")
zVoiceData:fnRegisterForm("Alraune",     "Callout|Mei|Nightshade|", sBasePath .. "01Alraune/")
zVoiceData:fnRegisterForm("Bee",         "Callout|Mei|HiveScout|",  sBasePath .. "02Bee/")
zVoiceData:fnRegisterForm("Ghost",       "Callout|Mei|Maid|",       sBasePath .. "03Ghost/")
zVoiceData:fnRegisterForm("Gravemarker", "Callout|Mei|Petraian|",   sBasePath .. "04Gravemarker/")
zVoiceData:fnRegisterForm("Slime",       "Callout|Mei|SmartySage|", sBasePath .. "05Slime/")
zVoiceData:fnRegisterForm("Werecat",     "Callout|Mei|Prowler|",    sBasePath .. "06Werecat/")
zVoiceData:fnRegisterForm("Wisphag",     "Callout|Mei|Soulherd|",   sBasePath .. "07Wisphag/")
zVoiceData:fnRegisterForm("Zombee",      "Callout|Mei|Zombee|",     sBasePath .. "08Zombee/")

--Set forms which have a special effect associated with them. Not all forms use this, the default is "Null".
--zVoiceData:fnSetEffectOnForm("Bee", "Bee Hivemind")
--zVoiceData:fnSetEffectOnForm("Gravemarker", "Bee Hivemind")

-- |[Pain Handlers]|
zVoiceData:fnLoadPains("Callout|Mei|Pain|", sBasePath .. "99Pain/", "Pain_")

-- |[Other Callouts]|
--General Callouts
zVoiceData:fnLoadGeneralCallout("Swing",  "Callout|Mei|General|Swing",  sBasePath .. "20General/", "00_Swing")
zVoiceData:fnLoadGeneralCallout("Bleed",  "Callout|Mei|General|Bleed",  sBasePath .. "20General/", "01_Bleed")
zVoiceData:fnLoadGeneralCallout("Poison", "Callout|Mei|General|Poison", sBasePath .. "20General/", "02_Poison")
zVoiceData:fnLoadGeneralCallout("Flame",  "Callout|Mei|General|Flame",  sBasePath .. "20General/", "03_Flame")

--Ability Callouts
zVoiceData:fnLoadAbilityCallout("Runestone",        "Callout|Mei|Ability|Runestone",        sBasePath .. "21Abilities/", "00_Runestone")
zVoiceData:fnLoadAbilityCallout("Blind",            "Callout|Mei|Ability|Blind",            sBasePath .. "21Abilities/", "AA_Blind")
zVoiceData:fnLoadAbilityCallout("Trip",             "Callout|Mei|Ability|Trip",             sBasePath .. "21Abilities/", "AB_Trip")
zVoiceData:fnLoadAbilityCallout("Concentrate",      "Callout|Mei|Ability|Concentrate",      sBasePath .. "21Abilities/", "AC_Concentrate")
zVoiceData:fnLoadAbilityCallout("Taunt",            "Callout|Mei|Ability|Taunt",            sBasePath .. "21Abilities/", "AD_Taunt")
zVoiceData:fnLoadAbilityCallout("Whirl",            "Callout|Mei|Ability|Whirl",            sBasePath .. "21Abilities/", "AE_Whirl")
zVoiceData:fnLoadAbilityCallout("FanOfLeaves",      "Callout|Mei|Ability|FanOfLeaves",      sBasePath .. "21Abilities/", "AG_FanOfLeaves")
zVoiceData:fnLoadAbilityCallout("Regrowth",         "Callout|Mei|Ability|Regrowth",         sBasePath .. "21Abilities/", "AH_Regrowth")
zVoiceData:fnLoadAbilityCallout("CallForHelp",      "Callout|Mei|Ability|CallForHelp",      sBasePath .. "21Abilities/", "AI_CallForHelp")
zVoiceData:fnLoadAbilityCallout("TranslucentSmile", "Callout|Mei|Ability|TranslucentSmile", sBasePath .. "21Abilities/", "AJ_TranslucentSmile")
zVoiceData:fnLoadAbilityCallout("IcyHand",          "Callout|Mei|Ability|IcyHand",          sBasePath .. "21Abilities/", "AK_IcyHand")
zVoiceData:fnLoadAbilityCallout("PiercingLight",    "Callout|Mei|Ability|PiercingLight",    sBasePath .. "21Abilities/", "AL_PiercingLight")
zVoiceData:fnLoadAbilityCallout("Absorb",           "Callout|Mei|Ability|Absorb",           sBasePath .. "21Abilities/", "AM_Absorb")
zVoiceData:fnLoadAbilityCallout("FirmUp",           "Callout|Mei|Ability|FirmUp",           sBasePath .. "21Abilities/", "AN_FirmUp")
zVoiceData:fnLoadAbilityCallout("JigglyChest",      "Callout|Mei|Ability|JigglyChest",      sBasePath .. "21Abilities/", "AO_JigglyChest")
zVoiceData:fnLoadAbilityCallout("Entangle",         "Callout|Mei|Ability|Entangle",         sBasePath .. "21Abilities/", "AP_Entangle")
zVoiceData:fnLoadAbilityCallout("GloryKill",        "Callout|Mei|Ability|GloryKill",        sBasePath .. "21Abilities/", "AQ_GloryKill")
zVoiceData:fnLoadAbilityCallout("CorruptedHoney",   "Callout|Mei|Ability|CorruptedHoney",   sBasePath .. "21Abilities/", "AS_CorruptedHoney")
zVoiceData:fnLoadAbilityCallout("Coordinate",       "Callout|Mei|Ability|Coordinate",       sBasePath .. "21Abilities/", "AT_Coordinate")
zVoiceData:fnLoadAbilityCallout("BurningSoul",      "Callout|Mei|Ability|BurningSoul",      sBasePath .. "21Abilities/", "AU_BurningSoul")

local fElapsedTime = LM_FinishTimer("Voice Load")
--io.write("Mei lines took " .. fElapsedTime .. " to load.\n")
