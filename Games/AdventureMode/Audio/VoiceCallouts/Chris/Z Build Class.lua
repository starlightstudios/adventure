-- |[ =================================== Chris Voice Loading ================================== ]|
--Setup.
local sBasePath = fnResolvePath()
local sPrefix = ""
LM_StartTimer("Voice Load")

-- |[Repeat Check]|
--Don't re-load data if the class already exists.
local zCheckData = VoiceDataList:fnLocateEntry("Chris")
if(zCheckData ~= nil) then return end

-- |[Create VoiceData]|
--Basic class.
local zVoiceData = VoiceData:new("Chris", "Root/Variables/Global/Christine/sVoiceJob")
zVoiceData.iAbstractVolSlot = gciAbstractVol_Christine
VoiceDataList:fnRegister(zVoiceData)

-- |[Form Registry]|
--Chris only has one 'forms', Male. All her non-male forms use the "Christine" package.
zVoiceData:fnRegisterForm("Male", "Callout|Chris|Male|", sBasePath .. "00Male/")

--Set forms which have a special effect associated with them. Not all forms use this, the default is "Null".
--zVoiceData:fnSetEffectOnForm("Bee", "Bee Hivemind")

-- |[Pain Handlers]|
zVoiceData:fnLoadPains("Callout|Chris|Pain|", sBasePath .. "99Pain/", "Pain_")

-- |[Other Callouts]|
--General Callouts
zVoiceData:fnLoadGeneralCallout("Swing", "Callout|Chris|General|Swing", sBasePath .. "20General/", "00_Swing")

--Lancer
local sAbiPath = sBasePath .. "21Abilities/"
local sAbiNames = {"Discharge"}
zVoiceData:fnLoadAbilityCalloutSequence(sAbiNames, "Callout|Chris|Ability|", sAbiPath)

-- |[Finish Up]|
local fElapsedTime = LM_FinishTimer("Voice Load")
--io.write("Chris lines took " .. fElapsedTime .. " to load.\n")