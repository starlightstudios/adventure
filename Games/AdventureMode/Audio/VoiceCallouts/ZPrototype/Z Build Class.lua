-- |[ ================================= CHARNAME Voice Loading ================================= ]|
--Setup.
local sBasePath = fnResolvePath()
local sPrefix = ""
LM_StartTimer("Voice Load")

-- |[Create VoiceData]|
--Basic class.
local zVoiceData = VoiceData:new("CHARNAME", "Root/Variables/Global/CHARNAME/sVoiceJob")
zVoiceData.iAbstractVolSlot = gciAbstractVol_CHARNAME
VoiceDataList:fnRegister(zVoiceData)

-- |[Form Registry]|
zVoiceData:fnRegisterForm("CLASSNAME", "Callout|CHARNAME|CLASSNAME|", sBasePath .. "00CLASSNAME/")

--Set forms which have a special effect associated with them. Not all forms use this, the default is "Null".
--zVoiceData:fnSetEffectOnForm("Bee", "Bee Hivemind")

-- |[Pain Handlers]|
zVoiceData:fnLoadPains("Callout|CHARNAME|Pain|", sBasePath .. "99Pain/", "Pain_")

-- |[Other Callouts]|
--General Callouts
zVoiceData:fnLoadGeneralCallout("Swing", "Callout|CHARNAME|General|Swing", sBasePath .. "20General/", "00_Swing")

--Ability Callouts
zVoiceData:fnLoadAbilityCallout("Encourage",       "Callout|CHARNAME|Ability|Encourage",       sBasePath .. "21Abilities/", "AA_Encourage")
zVoiceData:fnLoadAbilityCallout("Insult",          "Callout|CHARNAME|Ability|Insult",          sBasePath .. "21Abilities/", "AB_Insult")

local fElapsedTime = LM_FinishTimer("Voice Load")
io.write("CHARNAME lines took " .. fElapsedTime .. " to load.\n")