-- |[ ================================== JX-101 Voice Loading ================================== ]|
--Setup.
local sBasePath = fnResolvePath()
local sPrefix = ""
LM_StartTimer("Voice Load")

-- |[Repeat Check]|
--Don't re-load data if the class already exists.
local zCheckData = VoiceDataList:fnLocateEntry("JX-101")
if(zCheckData ~= nil) then return end

-- |[Create VoiceData]|
--Basic class.
local zVoiceData = VoiceData:new("JX-101", "Root/Variables/Global/JX-101/sVoiceJob")
zVoiceData.iAbstractVolSlot = gciAbstractVol_JX101
VoiceDataList:fnRegister(zVoiceData)

-- |[Form Registry]|
zVoiceData:fnRegisterForm("Tactician", "Callout|JX-101|Tactician|", sBasePath .. "00Tactician/")

--Set forms which have a special effect associated with them. Not all forms use this, the default is "Null".
--zVoiceData:fnSetEffectOnForm("Bee", "Bee Hivemind")

-- |[Pain Handlers]|
zVoiceData:fnLoadPains("Callout|JX-101|Pain|", sBasePath .. "99Pain/", "Pain_")

-- |[Other Callouts]|
--General Callouts
zVoiceData:fnLoadGeneralCallout("Swing", "Callout|JX-101|General|Swing", sBasePath .. "20General/", "00_Swing")

--Ability Callouts, automated. Abilities are assumed to have the name pattern AA_abinameA, AA_abinameB, AB_secondabiA, etc.
local sAbiPath = sBasePath .. "21Abilities/"
local sAbiNames = {"SniperShot", "FlakRound", "HeatSiphon", "Scavenge", "AngryYelling", "ThunderboltSlug"}
zVoiceData:fnLoadAbilityCalloutSequence(sAbiNames, "Callout|JX-101|Ability|", sAbiPath)

local fElapsedTime = LM_FinishTimer("Voice Load")
--io.write("JX-101 lines took " .. fElapsedTime .. " to load.\n")