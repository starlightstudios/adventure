-- |[ ================================== Tiffany Voice Loading ================================= ]|
--Setup.
local sBasePath = fnResolvePath()
local sPrefix = ""
LM_StartTimer("Voice Load")

-- |[Repeat Check]|
--Don't re-load data if the class already exists.
local zCheckData = VoiceDataList:fnLocateEntry("Tiffany")
if(zCheckData ~= nil) then return end

-- |[Create VoiceData]|
--Basic class.
local zVoiceData = VoiceData:new("Tiffany", "Root/Variables/Global/Tiffany/sVoiceJob")
zVoiceData.iAbstractVolSlot = gciAbstractVol_Tiffany
VoiceDataList:fnRegister(zVoiceData)

-- |[Form Registry]|
zVoiceData:fnRegisterForm("Assault", "Callout|Tiffany|Assault|", sBasePath .. "00Assault/")

--Set forms which have a special effect associated with them. Not all forms use this, the default is "Null".
--zVoiceData:fnSetEffectOnForm("Bee", "Bee Hivemind")

-- |[Pain Handlers]|
zVoiceData:fnLoadPains("Callout|Tiffany|Pain|", sBasePath .. "99Pain/", "Pain_")

-- |[Other Callouts]|
--General Callouts
zVoiceData:fnLoadGeneralCallout("Swing", "Callout|Tiffany|General|Swing", sBasePath .. "20General/", "00_Swing")

--Ability Callouts, automated. Abilities are assumed to have the name pattern AA_abinameA, AA_abinameB, AB_secondabiA, etc.
local sAbiPath = sBasePath .. "21Abilities/"
local sAbiNames = {"WideLensShot", "RubberSlugShot", "Breakthrough", "TargetOptics", "Pulverize", "Pinshot", "DisruptorBlast", "HighPowerShot", "TargetLegs", "LaserPulse", "ImplosionGrenade", "CoveringFire", "FocusFire",
                   "SpotlessProtocol", "ShieldModule", "NaniteCloud", "Ambush", "Exploit", "CoolantCoagulant", "DisruptHomeostasis", "SabotageWiring", "BagOfTricks", "CommJammer", "LockdownVirus", "Wristwire", 
                   "FearsomePresence", "IntoTheFray", "RapidAction", "WideLensBlast", "PowerJump", "PsycheUp", "ConductiveSpray", "ColdCalculation", "FullAuto"}
zVoiceData:fnLoadAbilityCalloutSequence(sAbiNames, "Callout|Tiffany|Ability|", sAbiPath)

local fElapsedTime = LM_FinishTimer("Voice Load")
--io.write("Tiffany lines took " .. fElapsedTime .. " to load.\n")