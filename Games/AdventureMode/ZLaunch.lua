-- |[ ========================================= Launch ========================================= ]|
--Launching script for Adventure Mode. If an argument is passed in, it will be the path of the save file to use.
gbAdventureBootDebug = false
if(gbAdventureBootDebug) then io.write("Launching Adventure Mode.\n") end
LM_StartTimer("ZLaunch")
local fStartTime = 0.0
local fEndTime = 0.0

-- |[WHI Special]|
--If using the WHI title screen, loading a game will mark WHI as active. This is only used to
-- make sure old saves for WHI correctly update which mods are active during play.
if(gsMandateTitle == "Witch Hunter Izana") then
    OM_SetModActive("Witch Hunter Izana")
end

-- |[Timer Constants]|
gcfTicksToSeconds = 1.0 / 60.0
gcfSecondsToTicks = 60/0 / 1.0

-- |[Loading Variables]|
gbIsLoadingSequence = false
gbOnlyBootSystemVars = false
gbNoOverlay = false

-- |[Statics Reset]|
AL_SetProperty("Disable All Mugging", false)

-- |[Steam Boot]|
giStringTyrantSteamIndex     = 1 --C++ Constant
giPandemoniumSteamIndex      = 2 
giWitchHunterIzanaSteamIndex = 3
Steam_Activate(giWitchHunterIzanaSteamIndex)

-- |[Argument Check]|
--If there are arguments provided, the game is loading a save.
local sSavePath = "None"
local iArgsTotal = LM_GetNumOfArgs()

--If savefile path is provided. A second argument will be the name of the
-- party leader, which determines which load sequence to use.
if(iArgsTotal >= 1) then
    
    --Flags
	gbIsLoadingSequence = true
	sSavePath = LM_GetScriptArgument(0)
    
    --Resolve party leadership.
    local sPartyLeader = LM_GetScriptArgument(1)
    if(sPartyLeader == nil) then sPartyLeader = "Null" end
    
    --Get the part up to the _ which indicates form.
    local iSplit = string.find(sPartyLeader, "_")
    
    --If no underscore is found, this is a "Null" case.
    if(iSplit == nil) then iSplit = 0 end
    sPartyLeader = string.sub(sPartyLeader, 1, iSplit-1)
    
    if(sPartyLeader == "Mei" or sPartyLeader == "Florentina") then
        LI_BootChapter1()
    elseif(sPartyLeader == "Christine" or sPartyLeader == "Tiffany" or sPartyLeader == "SX-399" or sPartyLeader == "JX-101") then
        LI_BootChapter5()
    else
		--When booting and using the WHI title screen, use the WHI load handler.
		if(gsMandateTitle == "Witch Hunter Izana") then
			LI_BootMonoceros()
		
		--Use a stock "Loading" screen.
		else
			LI_BootChapter0()
		end
    end
    
--New game boot.
else
		
	--When booting and using the WHI title screen, use the WHI load handler.
	if(gsMandateTitle == "Witch Hunter Izana") then
		LI_BootMonoceros()
	
	--Use a stock "Loading" screen.
	else
		LI_BootChapter0()
	end
end

-- |[Resolve Root Path]|
gsRoot = VM_GetVar("Root/Paths/System/Startup/sAdventurePath", "S")

--Achievement Globals
gbRunItemAchievementCheck = false
gsAchievementSet = "Runes of Pandemonium"
gsAchievementPath = gsRoot .. "System/200 Achievements.lua"

-- |[ ===================================== Audio Loading ====================================== ]|
-- |[Timer Setup]|
fStartTime = 0.0

-- |[Load Audio]|
--Main audio loading.
if(gbAdventureBootDebug) then io.write(" Loading audio.\n") end
LM_ExecuteScript(gsRoot .. "Audio/ZRouting.lua")

--Selective audio loading.
gbLoadedCreditsTheme5 = false

--Reset this object.
MapM_ReplaceDialogueWithWorldDialogue()

-- |[Timer Report]|
fEndTime = LM_CheckTimer("ZLaunch")
io.write(string.format("%6.4f: Audio Loading.\n", fEndTime - fStartTime))

-- |[ ======================================= Path Setup ======================================= ]|
-- |[Translation]|
--Call subfile.
LM_ExecuteScript(gsRoot .. "System/Strings/000 Routing.lua")

-- |[Timer]|
fStartTime = LM_CheckTimer("ZLaunch")

-- |[Reset Flags]|
local iAdventureMenuType = 33 --C++ constant
MapM_ChangeMenuHandler(iAdventureMenuType)

--Modify combat type.
local iAdventureCombatType = 51 --C++ constant
MapM_ChangeCombatHandler(iAdventureCombatType)

--Lua system strings. These strings are booted for chapter 1, other chapters override them.
if(gbAdventureBootDebug) then io.write(" Setting global strings.\n") end
gsAutoBoot = "Nowhere"
gsSaveHandler            = gsRoot .. "Save Handler/000 Save Handler.lua"
gsDatafilesPath          = gsRoot .. "Datafiles/"
gsMapDirectory           = gsRoot .. "Maps/"
gsItemListing            = gsRoot .. "Items/Item List.lua"
gsCatalystHandler        = gsRoot .. "Subroutines/Catalysts/Catalyst Handler.lua"
gsLayeredTrackRouter     = gsRoot .. "LayeredTrackProfiles/ZRouting.lua"
gsGemHandler             = gsRoot .. "Items/Gem Upgrade Mapping.lua"
gsFieldAbilityListing    = gsRoot .. "Field Abilities/"
gsStandardCombatResponse = gsRoot .. "Combat/Combat Response Script.lua"
gsStandardGameOver       = gsRoot .. "Chapter 1/Scenes/300 Standards/Defeat/Scene_Begin.lua"
gsStandardRetreat        = gsRoot .. "Chapter 1/Scenes/300 Standards/Retreat/Scene_Begin.lua"
gsStandardRevert         = gsRoot .. "Chapter 1/Scenes/300 Standards/Revert/Scene_Begin.lua"
gsStandardReliveBegin    = gsRoot .. "Chapter 1/Scenes/Z Relive Handler/Scene_Begin.lua"
gsStandardReliveEnd      = gsRoot .. "Chapter 1/Scenes/Z Relive Handler/Scene_End.lua"
gsStandardWarpHandler    = gsRoot .. "Maps/Z Standard/Standard Warp Handler.lua"
gsCharacterAutoresolve   = gsRoot .. "CostumeHandlers/YCharacterAutoresolve.lua"
gsCostumeAutoresolve     = gsRoot .. "CostumeHandlers/XFormAutoresolve.lua"
gsGemNameResolvePath     = gsRoot .. "Items/Resolve Gem Name.lua"
gsRegionMarkerPath       = gsRoot .. "Subroutines/Maps/Handle Region Marker.lua"
gsStandardShadow         = "Root/Images/Sprites/Shadows/Generic"

--Random Level Generation
gsRandomExaminationHandler = gsRoot .. "Subroutines/Random Levels/101 Random Examination.lua"

--Skillbook Paths
gsMeiSkillbook        = gsRoot .. "Combat/Party/Mei/300 Skillbook Handler.lua"
gsFlorentinaSkillbook = gsRoot .. "Combat/Party/Florentina/300 Skillbook Handler.lua"
gsSanyaSkillbook      = gsRoot .. "Combat/Party/Sanya/300 Skillbook Handler.lua"
gsIzunaSkillbook      = gsRoot .. "Combat/Party/Izuna/300 Skillbook Handler.lua"
gsJeanneSkillbook     = gsRoot .. "Combat/Party/Jeanne/300 Skillbook Handler.lua"
gsChristineSkillbook  = gsRoot .. "Combat/Party/Christine/300 Skillbook Handler.lua"
gsTiffanySkillbook    = gsRoot .. "Combat/Party/Tiffany/300 Skillbook Handler.lua"
gsSX399Skillbook      = gsRoot .. "Combat/Party/SX-399/300 Skillbook Handler.lua"

--Combat Paths
gsStandardJobPath              = gsRoot .. "Combat/Z Standards/Standard Job.lua"
gsStandardAbilityPath          = gsRoot .. "Combat/Z Standards/Ability Standard.lua"
gsStandardAbilityJobchangePath = gsRoot .. "Combat/Z Standards/Ability Standard Jobchange.lua"
gsStandardEffectDotPath        = gsRoot .. "Combat/Z Standards/Damage Over Time Ref Structure.lua"
gsStandardDoTPath              = gsRoot .. "Combat/Z Standards/Damage Over Time.lua"
gsStandardEffectHotPath        = gsRoot .. "Combat/Z Standards/Heal Over Time Ref Structure.lua"
gsStandardHoTPath              = gsRoot .. "Combat/Z Standards/Heal Over Time.lua"
gsStandardStatPath             = gsRoot .. "Combat/Z Standards/Stat Mod.lua"
gsThreatEffectPath             = gsRoot .. "Combat/Z Standards/Threat Vs.lua"
gsStandardAutoWinPath          = gsRoot .. "Subroutines/Combat/fnCheckAutoWin.lua"
gsEnemyPortraitRouting         = gsRoot .. "Combat/Enemies/Position Lookups/ZRouting.lua"
gsGlobalEffectPrototypePath    = gsRoot .. "Combat/Routines/Effects/Global Prototype Effect Script.lua"

--Minigame Paths
gsKPopPath    = gsRoot .. "Minigames/KPopBeats/000 Initializer.lua"
gsHuntingPath = gsRoot .. "Minigames/Hunting/"

--Field Ability, Nonstandard Path
gsFieldAbilityCheckPath = "Null"

--Default icon paths.
AM_SetProperty("Set Form Icon",    "Root/Images/AdventureUI/CampfireMenuIcon/TransformMei")
AM_SetProperty("Set Relive Icon",  "Root/Images/AdventureUI/CampfireMenuIcon/ReliveMei")
AM_SetProperty("Set Costume Icon", "Root/Images/AdventureUI/CampfireMenuIcon/Costume")

-- |[Static Paths]|
Save_SetSaveHandlerPath(gsSaveHandler)

-- |[Utility Functions]|
LM_ExecuteScript(gsRoot .. "Subroutines/System/fnSubdivide.lua")

-- |[Datafile Listing]|
--Build a list of all datafiles used here. These are not called immediately, they are in the 
-- loading paths.
gsaDatafilePaths = {}
LM_ExecuteScript(gsRoot .. "Subroutines/Startup/Build Datafile Paths.lua")

-- |[Electrosprite Adventure Info]|
--Special: Electrosprite Adventure Path. This is needed in chapter 5, but is stored in its own subgame
-- directory adjacent to the adventure directory.
gsCallElectrospritePath = "Null"

--Get the game entry. If found, change the call path.
local zElectrospriteGameEntry = SysPaths:fnGetGameEntry("Electrosprite Adventure")
if(zElectrospriteGameEntry ~= nil and zElectrospriteGameEntry.sActivePath ~= "Null") then
    gsCallElectrospritePath = zElectrospriteGameEntry.sActivePath
end

-- |[Internal Paths]|
--Set system strings so the C++ knows where things are.
if(gbAdventureBootDebug) then io.write(" Setting system strings.\n") end
AL_SetProperty("Root Path", gsRoot)
AL_SetProperty("Item Path", gsItemListing)
AL_SetProperty("Item Image Path", gsItemImageListing)
AL_SetProperty("Catalyst Path", gsCatalystHandler)
AL_SetProperty("Layered Track Routing Path", gsLayeredTrackRouter)
AL_SetProperty("Random Level Generator Path", "Subroutines/Random Levels/100 Random Level Generator.lua")
AL_SetProperty("Random Chest Script", gsRoot .. "Subroutines/Maps/Chest Response.lua")
AM_SetProperty("Party Resolve Script", gsRoot .. "Subroutines/Campfire Menu/Campfire Chat Resolve.lua")
AM_SetProperty("Rest Resolve Script", gsRoot .. "RestingDialogues/Z Rest Resolve.lua")
AM_SetProperty("Warp Resolve Script", gsRoot .. "Subroutines/Warping/Assemble Warp List.lua")
AM_SetProperty("Warp Execute Script", gsRoot .. "Subroutines/Warping/Execute Warp.lua")
AM_SetProperty("Relive Resolve Script", gsRoot .. "Subroutines/Campfire Menu/Assemble Relive List.lua")
AM_SetProperty("Costume Resolve Script", gsRoot .. "CostumeHandlers/ZCostumeListBuilder.lua")
AM_SetProperty("Field Ability Resolve Script", gsRoot .. "Field Abilities/ZAssembleFieldAbilityList.lua")
TA_SetProperty("Build Graphics From Name Path", gsRoot .. "Subroutines/Field Entities/Build Graphics From Name.lua")
TA_SetProperty("Standard Enemy Shadow", gsStandardShadow)
WD_SetProperty("Topic Directory", gsRoot .. "Topics/")
AM_SetProperty("Form Party Resolve Script", gsRoot .. "FormHandlers/Build Party List.lua")
AM_SetProperty("Form Resolve Script", gsRoot .. "FormHandlers/Form Resolver.lua")
ADebug_SetProperty("Set Profile Path", gsRoot .. "Combat/Party/Build Stat Profiles.lua")
AdInv_SetProperty("Gem Name Resolve Path", gsRoot .. "Items/Resolve Gem Name.lua")
AdInv_SetProperty("Set Internal Sort", 2)

--Order the script to pre-build the lookups.
if(gbAdventureBootDebug) then io.write(" Building item image listing.\n") end
LM_ExecuteScript(gsItemImageListing, "Null")

--Function Paths
if(gbAdventureBootDebug) then io.write(" Setting function paths.\n") end
gzFunctionPaths = {}
gzFunctionPaths.sSetPartyDialogue         = gsRoot .. "Subroutines/Dialogue/Set Party Dialogue.lua"
gzFunctionPaths.sSetPartyDialogueOpposite = gsRoot .. "Subroutines/Dialogue/Set Party Dialogue Opposite.lua"
gzFunctionPaths.sSetCombatPartyDialogue   = gsRoot .. "Subroutines/Dialogue/Set Combat Party Dialogue.lua"

-- |[Timer Report]|
fEndTime = LM_CheckTimer("ZLaunch")
io.write(string.format("%5.4f: Path Setup.\n", fEndTime - fStartTime))

-- |[ ======================================== Clearing ======================================== ]|
-- |[Timer]|
fStartTime = LM_CheckTimer("ZLaunch")

--Wipe the DataLibrary of any script variables. Create a new Script Variable set.
if(gbAdventureBootDebug) then io.write(" Wiping Data Library.\n") end
WD_SetProperty("Wipe Topic Data")
DL_Purge("Root/Variables/", false)
DL_AddPath("Root/Variables/")

--Clear the inventory of any outstanding items.
if(gbAdventureBootDebug) then io.write(" Clearing inventory.\n") end
AdInv_SetProperty("Clear")

--Wipe the level music.
if(gbAdventureBootDebug) then io.write(" Stopping music.\n") end
AL_SetProperty("Music", "Null")

--Build program lists which do not depend on functions.
if(gbAdventureBootDebug) then io.write(" Building character form listing.\n") end
ADebug_SetProperty("Set Form Builder Path", gsRoot .. "FormHandlers/Build Form List.lua")

--Build Variables
if(gbAdventureBootDebug) then io.write(" Setting variables.\n") end
LM_ExecuteScript(gsRoot .. "System/000 Variables.lua")
LM_ExecuteScript(gsRoot .. "System/002 Time Variables.lua")

--Combat ability lookups.
LM_ExecuteScript(gsRoot .. "Combat/Enemies/Ability Lookups.lua")

--Set the cleaner script.
if(gbAdventureBootDebug) then io.write(" Setting cleaner script.\n") end
EM_SetCleanerScript(gsRoot .. "System/900 Cleaner.lua")

-- |[Timer Report]|
fEndTime = LM_CheckTimer("ZLaunch")
io.write(string.format("%6.4f: Clearing Old Data, Building Variables.\n", fEndTime - fStartTime))

-- |[ ======================================= Subroutines ====================================== ]|
-- |[Timer]|
fStartTime = LM_CheckTimer("ZLaunch")

--Execute subroutines which will be used in the rest of the construction sequence.
if(gbAdventureBootDebug) then io.write(" Building function listing.\n") end

-- |[Primary Function Set]|
LM_ExecuteScript(gsRoot .. "Subroutines/Z Build Functions.lua")

-- |[Combat Function Set]|
LM_ExecuteScript(gsRoot .. "Combat/Routines/Z Build Functions.lua")

-- |[Topics]|
if(gbAdventureBootDebug) then io.write(" Setting topics.\n") end
LM_ExecuteScript(gsRoot .. "Topics/Build Topics.lua")

--Combat enemy effect prototypes.
LM_ExecuteScript(gsRoot .. "Combat/Enemies/Effects/Common Effect Prototypes.lua")

-- |[Timer Report]|
fEndTime = LM_CheckTimer("ZLaunch")
io.write(string.format("%6.4f: Building Subroutines.\n", fEndTime - fStartTime))

-- |[ ========================================= Loading ======================================== ]|
-- |[Font Loading]|
if(gbAdventureBootDebug) then io.write(" Loading fonts.\n") end
fStartTime = LM_CheckTimer("ZLaunch")
LM_ExecuteScript(gsRoot .. "Fonts/000 Boot Fonts.lua")
fEndTime = LM_CheckTimer("ZLaunch")
io.write(string.format("%6.4f: Font Loading.\n", fEndTime - fStartTime))

-- |[Graphics Loading]|
if(gbAdventureBootDebug) then io.write(" Loading graphics.\n") end
fStartTime = LM_CheckTimer("ZLaunch")
LM_ExecuteScript(gsRoot .. "System/001 Graphics.lua")
fEndTime = LM_CheckTimer("ZLaunch")
io.write(string.format("%6.4f: General Graphics Loading.\n", fEndTime - fStartTime))

-- |[Map Path Remapping]|
if(gbAdventureBootDebug) then io.write(" Building map path remaps.\n") end
fStartTime = LM_CheckTimer("ZLaunch")
LM_ExecuteScript(gsRoot .. "Maps/Build Map Path Remaps.lua")
fEndTime = LM_CheckTimer("ZLaunch")
io.write(string.format("%6.4f: Path-Remap Building.\n", fEndTime - fStartTime))

-- |[Clear Felled Enemies]|
--Order the program to wipe out dead enemies. The load handler will populate this with new data.
if(gbAdventureBootDebug) then io.write(" Clearing destroyed enemy data.\n") end
AL_SetProperty("Wipe Destroyed Enemies")

-- |[ ====================================== Boot Sequence ===================================== ]|
-- |[Normal Boot Sequence]|
gsAutoBoot = "Nowhere"
--gsAutoBoot = "Chapter 5"

--In the normal game boot sequence, load into the dimensional trap and create the party.
fStartTime = LM_CheckTimer("ZLaunch")
if(gbIsLoadingSequence == false) then
    
	-- |[Normal Boot]|
	--Boot the game to "Nowhere" which allows the player to select their chapter.
	if(gsAutoBoot == "Nowhere") then
        if(gbAdventureBootDebug) then io.write(" Booting Nowhere map.\n") end
		LM_ExecuteScript(gsMapDirectory .. "/Nowhere/Constructor.lua", gci_Constructor_Start)
	
	-- |[Autoboot Chapter 1]|
	--Used for debug.
	elseif(gsAutoBoot == "Chapter 1") then
        if(gbAdventureBootDebug) then io.write(" Running chapter 1 handler, autoboot.\n") end

        --Flag to make sure images load.
        gbShouldLoadChapter1Assets = true

        --Settings.
        gbBypassIntro = true
        LM_ExecuteScript(gsRoot .. "Chapter 1/000 Initialize.lua")
		LM_ExecuteScript(gsMapDirectory .. "/TrapBasement/TrapBasementC/Constructor.lua", gci_Constructor_Start)
        
        --Create Mei.
        TA_Create("Mei")
            giPartyLeaderID = RO_GetID()
            TA_SetProperty("Position", 52, 29)
            TA_SetProperty("Facing", gci_Face_South)
            TA_SetProperty("Clipping Flag", false)
            TA_SetProperty("Activation Script", "Null")
        DL_PopActiveObject()
        
        --Run Mei's costume handler.
        LM_ExecuteScript(gsCostumeAutoresolve, "Mei_Human")
	
        --Mark Mei as the actor entity.
        AL_SetProperty("Player Actor ID", giPartyLeaderID)
	
	-- |[Autoboot Chapter 5]|
	--Used for debug.
	elseif(gsAutoBoot == "Chapter 5") then
        if(gbAdventureBootDebug) then io.write(" Running chapter 5 handler, autoboot.\n") end

        --Flag to make sure images load.
        gbShouldLoadChapter5Assets = true

        --Settings.
        gbBypassIntro = true
        LM_ExecuteScript(gsRoot .. "Chapter 5/000 Initialize.lua")
		LM_ExecuteScript(gsMapDirectory .. "/RegulusCryo/RegulusCryoA/Constructor.lua", gci_Constructor_Start)

        --Create Christine.
        TA_Create("Christine")
            giPartyLeaderID = RO_GetID()
            TA_SetProperty("Position", 6, 2)
            TA_SetProperty("Facing", gci_Face_South)
            TA_SetProperty("Clipping Flag", false)
            TA_SetProperty("Activation Script", "Null")
        DL_PopActiveObject()
	
        --Mark Christine as the actor entity.
        AL_SetProperty("Player Actor ID", giPartyLeaderID)
	end
	
	--Mark sound settings as booted.
	VM_SetVar("Root/Variables/System/Special/iHasSoundSettings", "N", 1.0)

-- |[Load Boot Sequence]|
else

    -- |[Actors Store Paths]|
    --Actors will likely be created before the image loading has completed. If this is the case, they will
    -- store the path of the missing images. At the end of this file, all TilemapActors will then re-resolve
    -- any missing images.
    TA_SetProperty("Store Image Paths", true)
    
    --Tell the program not to boot map information yet.
    gbDisallowMapConstruction = true
    
    --Tell the program not to load any images yet.
    gbShouldLoadChapter1Assets = false
    gbShouldLoadChapter2Assets = false
    gbShouldLoadChapter5Assets = false
    
    --Tell the load handlers not to run any catalyst counters, the load handler does that.
    gbDontRecountCatalysts = true

    -- |[Autosave]|
    --Block autosaves when loading a save file.
    AL_SetProperty("Block Autosave Once")
    
    -- |[Variable boot]|
	--Standard loading sequence, these always get set. If loading from a different chapter, these will get overwritten.
    if(gbAdventureBootDebug) then io.write(" Setting standard variables.\n") end
	
	--Chapter initializers. If not found, don't run. Derived games may remove these to save space.
	if(FS_Exists(gsRoot .. "Chapter 1/000 Initialize.lua") == true) then
		LM_ExecuteScript(gsRoot .. "Chapter 1/000 Initialize.lua")
	end
	if(FS_Exists(gsRoot .. "Chapter 2/000 Initialize.lua") == true) then
		LM_ExecuteScript(gsRoot .. "Chapter 2/000 Initialize.lua")
	end
	if(FS_Exists(gsRoot .. "Chapter 3/000 Initialize.lua") == true) then
		LM_ExecuteScript(gsRoot .. "Chapter 3/000 Initialize.lua")
	end
	if(FS_Exists(gsRoot .. "Chapter 4/000 Initialize.lua") == true) then
		LM_ExecuteScript(gsRoot .. "Chapter 4/000 Initialize.lua")
	end
	if(FS_Exists(gsRoot .. "Chapter 5/000 Initialize.lua") == true) then
		LM_ExecuteScript(gsRoot .. "Chapter 5/000 Initialize.lua")
	end
	if(FS_Exists(gsRoot .. "Chapter 6/000 Initialize.lua") == true) then
		LM_ExecuteScript(gsRoot .. "Chapter 6/000 Initialize.lua")
	end
    
    --Mods.
    gbIsStartingChapter = true
    gbIsLoadingChapter = true
    for i = 1, #gsModDirectories, 1 do

        --Path.
        local sLoadHandlerPath = gsModDirectories[i] .. "System/000 Mod Setup.lua"
        
        --If it exists, run it.
        if(FS_Exists(sLoadHandlerPath) == true) then
            LM_ExecuteScript(sLoadHandlerPath)
        end
    end
    gbIsStartingChapter = nil
    gbIsLoadingChapter = nil
    
    --After initialization is done, we can allow map construction.
    gbDisallowMapConstruction = false
	
	--Run the load operation here. Don't create NPCs and don't check their costume variables yet, the Load Handler takes care of that.
    if(gbAdventureBootDebug) then io.write(" Running loading operation.\n") end
    gbSuppressLeaderCreation = true
    gbDontRunCostumeHandler = true
	Save_ExecuteAdventureLoad(sSavePath)
    gbSuppressLeaderCreation = false
    gbDontRunCostumeHandler = false
    
    --Interloper. We now know what mod is being used. Not all mods respond.
    for i = 1, #gsModDirectories, 1 do

        --Path.
        local sLoadHandlerPath = gsModDirectories[i] .. "System/002 Load Post Exec.lua"
        
        --If it exists, run it.
        if(FS_Exists(sLoadHandlerPath) == true) then
            LM_ExecuteScript(sLoadHandlerPath)
        end
    end
	
	--Execute the post-process script.
    if(gbAdventureBootDebug) then io.write(" Executing load post-process.\n") end
	LM_ExecuteScript(gsRoot .. "Load Handler/000 Load Handler.lua")
    if(gbAdventureBootDebug) then io.write(" Completed load post process.\n") end
	
	--Mark sound settings as booted.
	VM_SetVar("Root/Variables/System/Special/iHasSoundSettings", "N", 1.0)
    
    --Clean up.
    gbDontRecountCatalysts = nil

end
fEndTime = LM_CheckTimer("ZLaunch")
io.write(string.format("%6.4f: Chapter-Specific Loading\n", fEndTime - fStartTime))

-- |[ ======================================= Other Setup ====================================== ]|
-- |[Timer]|
fStartTime = LM_CheckTimer("ZLaunch")

-- |[Flags]|
--Autoresolve flag. Generally stays on for the entire program duration.
if(gbAdventureBootDebug) then io.write(" Setting misc flags.\n") end
WD_SetProperty("Autoresolve Target", true)

-- |[Combat System Properties]|
--Response path.
AdvCombat_SetProperty("Response Path", gsStandardCombatResponse)

--Combat system abilities. Some of the abilities in combat always stay the same and are shared by many
-- entities, so they get built once at startup.
AdvCombat_SetProperty("Set Pass Turn Properties", gsRoot .. "Combat/Party/Common/Abilities/Pass Turn.lua")
AdvCombat_SetProperty("Set Retreat Properties",   gsRoot .. "Combat/Party/Common/Abilities/Retreat.lua")
AdvCombat_SetProperty("Set Surrender Properties", gsRoot .. "Combat/Party/Common/Abilities/Surrender.lua")
AdvCombat_SetProperty("Set Volunteer Properties", gsRoot .. "Combat/Party/Common/Abilities/Volunteer.lua")

--Combat statistics path.
AdvCombat_SetProperty("Set Statistics Path", gsRoot .. "Combat/Routines/Statistics/Handle Statistic Update.lua")

-- |[Doctor Bag]|
--Doctor bag path.
AdvCombat_SetProperty("Set Doctor Resolve Path", gsRoot .. "Combat/Routines/Party/Resolve Doctor Bag.lua")

-- |[Auto-Win Handler]|
AdvCombat_SetProperty("Set Auto Win Path", gsStandardAutoWinPath)

-- |[ ====================================== Achievements ====================================== ]|
--Handled by a sub-call.
LM_ExecuteScript(gsAchievementPath)

-- |[ ================================ Chapter 5 Special Events ================================ ]|
--If the player has rescued Cassandra but didn't get the achievement, handle that here.
--[REMOVECHAPTER2]
local iResolvedCassandraQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iResolvedCassandraQuest", "N")
if(iResolvedCassandraQuest >= 1.0) then
    AM_SetPropertyJournal("Unlock Achievement", "GolemCassandra")
end
                
-- |[ ======================================= Game Setup ======================================= ]|
-- |[Menu]|
--Hide the main menu.
if(gbAdventureBootDebug) then io.write(" Finishing up.\n") end
MapM_PushMenuStackHead()
	FlexMenu_FlagClose()
DL_PopActiveObject()

--Drop all events. This prevents the game from running while the loading screen is firing.
Debug_DropEvents()
gbIsLoadingSequence = false
if(gbAdventureBootDebug) then io.write("Finished Launching Adventure Mode.\n") end

-- |[Timer Report]|
--Other stuff timer:
fEndTime = LM_CheckTimer("ZLaunch")
io.write(string.format("%6.4f: Misc Setup.\n", fEndTime - fStartTime))

--Time stuff.
local fElapsedTime = LM_FinishTimer("ZLaunch")
io.write(string.format("%6.4f: Total Boot Time.\n\n", fElapsedTime))
        
--Debug.
if(gbShowBitmapStatistics) then
    DL_PrintBitmapStatistics()
end
    
-- |[ ===================================== Mod Post-Exec ====================================== ]|
--Mods can optionally execute this after loading. Mods should set a flag to indicate they are active
-- in some way.
if(gbAdventureBootDebug) then io.write(" Executing mod post-execution.\n") end
for i = 1, #gsModDirectories, 1 do

    --Path.
    local sPostExecPath = gsModDirectories[i] .. "System/003 Load Post Exec.lua"
    
    --If it exists, run it.
    if(FS_Exists(sPostExecPath) == true) then
        LM_ExecuteScript(sPostExecPath)
    end
end

--Final diagnostics.
if(gbAdventureBootDebug) then io.write(" Completed launch file.\n") end
