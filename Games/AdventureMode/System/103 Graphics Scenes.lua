-- |[ ==================================== Graphics: Scenes ==================================== ]|
--Images used in TF scenes and CGs. Only universal images are loaded here, everything else is 
-- in a chapter-specific loading file to reduce load times.
--In derived games, this file may have been removed. Do nothing if it was.
if(FS_Exists(gsaDatafilePaths.sScnCh0Major) == false) then return end

-- |[Open]|
SLF_Open(gsaDatafilePaths.sScnCh0Major)

-- |[Miscellaneous]|
--Used for tutorials and the like.
DL_AddPath("Root/Images/Scenes/System/")
DL_ExtractBitmap("Controls", "Root/Images/Scenes/System/Controls")

--Credits Sequences. Only chapter 5's exists for now.
DL_ExtractBitmap("CreditsBacking5", "Root/Images/Scenes/System/CreditsBacking5")
