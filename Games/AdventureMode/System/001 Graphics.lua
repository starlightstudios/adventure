-- |[ ======================================== Graphics ======================================== ]|
--Loads all graphics for general-purposes Adventure Mode. Chapter-and-character-specific graphics are loaded elsewhere.

-- |[Load Handling]|
--Prevent double loads. Deprecated, delayed bitmap handlers now prevent double loading.
if(gbHasLoadedAssets ~= nil and false) then 
    AdvCombat_SetProperty("Construct")
    LM_ExecuteScript(gsRoot .. "Combat/Animations/ZRouting.lua")
    return
end
gbHasLoadedAssets = true

--Reset counters.
fnIssueLoadReset("AdventureMode")

-- |[Sub-Calls]|
--Subscripts handle each individual .slf file.
local sBasePath = fnResolvePath()
LM_ExecuteScript(sBasePath .. "100 Graphics Sprites.lua")
LM_ExecuteScript(sBasePath .. "101 Graphics Portraits.lua")
LM_ExecuteScript(sBasePath .. "102 Graphics UI.lua")
LM_ExecuteScript(sBasePath .. "103 Graphics Scenes.lua")
LM_ExecuteScript(sBasePath .. "104 Control Manager UI.lua")
SLF_Close()

-- |[Construction]|
--Some classes need to be told when images are done loading. Do that here. Also, make sure it's a basic WorldDialogue object.
WD_SetProperty("Construct")
AdvCombat_SetProperty("Construct")

--Create all combat animations.
LM_ExecuteScript(gsRoot .. "Combat/Animations/ZRouting.lua")

-- |[Timer Marking]|
--Finish.
fnCompleteLoadSequence()
