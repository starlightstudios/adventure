-- |[ ====================================== Graphics: UI ====================================== ]|
--User interface parts, such as combat and level-up. Also contains a lot of portrait pieces that are used in
-- the UI, such as the turn-order portraits.
--Unlike most other graphics, these all resolve immediately, as the UI often needs to know their
-- sizes and properties to construct itself.

-- |[ ===================================== Run Autoloaders ==================================== ]|
SLF_Open(gsDatafilesPath .. "UI_Autoloader.slf")
fnExecAutoload("AutoLoad|Adventure|AutorunUI")

-- |[ =================================== Build Static Lists =================================== ]|
--These will lazy-init when the first item is created that needs them, but mods may
-- want to override the lists so they are built here.
AdItem_SetProperty("Init Stat Icon List")

--Setup comparison lists.
local saCompareIcoNames = {"Attack", "Protection", "Health", "Accuracy", "Evade", "Initiative", "Mana", "Blind", "Bleed", "Poison", "Corrode", "Shield", "Adrenaline", "ComboPoint", "Stun", "Turns", "Threat", "Skill", 
                           "Slash", "Pierce", "Strike", "Flaming", "Freezing", "Shocking", "Crusading", "Obscuring", "Terrify", "Debuff", "Underlay", "Downx3", "Downx2", "Downx1", "Neutral", "Upx1", "Upx2", "Upx3", "Rarrow"}
local i18PxLookupsTotal = #saCompareIcoNames
local saBaseNames = {"Atk18", "Prt18", "HP18", "Acc18", "Evd18", "Ini18", "Blnd18", "Bld18", "Psn18", "Crd18", "Mgc18", "Cmb18", "Stn18", "Slsh18", "Prc18", "Str18", "Flm18", "Frz18", "Shk18", "Cru18", "Obs18"}

--Remaps used for the inventory.
AM_SetProperty("Text Image Remaps Total", i18PxLookupsTotal)
for i = 1, i18PxLookupsTotal, 1 do
    AM_SetProperty("Text Image Remaps", i-1, saBaseNames[i], "Root/Images/AdventureUI/CompareIcons/" .. saCompareIcoNames[i])
end
