-- |[ ====================================== Achievements ====================================== ]|
--Wipe any lingering achievements.
if(gsAchievementSet ~= "Runes of Pandemonium") then return end
AM_SetPropertyJournal("Clear Achievements")

-- |[Achievement Creation]|
--Constants
local iF = 0 --Fully visible
local iS = 1 --Spoilered
local iH = 2 --Hidden

--Storage.
local zaAchievements = {}

--Function that creates an achievement in script.
local function fnCreateAchievement(psCategory, psInternalName, psSteamName, psDisplayName, psIconPath, piVisibility, psDescription)
    
    --Error check.
    if(psCategory     == nil) then return end
    if(psInternalName == nil) then return end
    if(psSteamName    == nil) then return end
    if(psDisplayName  == nil) then return end
    if(psIconPath     == nil) then return end
    if(piVisibility   == nil) then return end
    if(psDescription  == nil) then return end
    
    --Create.
    local zEntry = {}
    zEntry.sCategory     = psCategory
    zEntry.sInternalName = psInternalName
    zEntry.sSteamName    = psSteamName
    zEntry.sDisplayName  = psDisplayName
    zEntry.sIconPath     = psIconPath
    zEntry.sDescription  = psDescription
    
    --Register.
    table.insert(zaAchievements, zEntry)
    
    --Immediately register it to the program.
    AM_SetPropertyJournal("Register Achievement", psCategory, psInternalName, psDisplayName, psIconPath, psDescription)
    
    --Spoilered:
    if(piVisibility == iS) then
        AM_SetPropertyJournal("Set Achievement Spoilered", psInternalName, true)
    
    --Hidden.
    elseif(piVisibility == iH) then
        AM_SetPropertyJournal("Set Achievement Locked", psInternalName, true)
    end
end

-- |[General]|
local sPrefix = "Root/Images/AdventureUI/AchievementIco/General|"
local sAch = "General"
AM_SetPropertyJournal("Register Achievement Category", sAch, "General Achievements", "Root/Images/AdventureUI/AchievementIco/CategoryIcons|General")
fnCreateAchievement(sAch, "Needlemouse", "Gen00Needlemouse", "Super Needlemouse", sPrefix .. "Needlemouse", iF, "Get one of each type of the six mergable gems.")
    AM_SetPropertyJournal("Allocate Achievement Requirements", "Needlemouse", 6)
    AM_SetPropertyJournal("Set Achievement Requirements", "Needlemouse", 0, "Grey",   1.0, false, true)
    AM_SetPropertyJournal("Set Achievement Requirements", "Needlemouse", 1, "Red",    1.0, false, true)
    AM_SetPropertyJournal("Set Achievement Requirements", "Needlemouse", 2, "Blue",   1.0, false, true)
    AM_SetPropertyJournal("Set Achievement Requirements", "Needlemouse", 3, "Orange", 1.0, false, true)
    AM_SetPropertyJournal("Set Achievement Requirements", "Needlemouse", 4, "Violet", 1.0, false, true)
    AM_SetPropertyJournal("Set Achievement Requirements", "Needlemouse", 5, "Yellow", 1.0, false, true)

-- |[Chapter 1 Gameplay]|
sPrefix = "Root/Images/AdventureUI/AchievementIco/Chapter1|"
sAch = "Chapter1"
AM_SetPropertyJournal("Register Achievement Category", sAch, "Chapter 1", "Root/Images/AdventureUI/AchievementIco/CategoryIcons|Chapter1")
fnCreateAchievement(sAch, "EscapeBasement",    "Ch100EscapeBasement", "Escape The Basement",               sPrefix .. "EscapeBasement",    iF, "Man, those guys are stupid!\n\nEscape the cult's basement prison.")
fnCreateAchievement(sAch, "MeetAdina",         "Ch101Adina",          "The Salt Flats",                    sPrefix .. "MeetAdina",         iF, "She's nice once you get to know her.\n\nMeet Adina at the Salt Flats.")
fnCreateAchievement(sAch, "MeetFlorentina",    "Ch102Florentina",     "If I Were A Bad Merchant...",       sPrefix .. "MeetFlorentina",    iF, "Just needs to get lost for a bit.\n\nRecruit Florentina.")
fnCreateAchievement(sAch, "MeetPolaris",       "Ch103Polaris",        "My Hat Precedes Me",                sPrefix .. "MeetPolaris",       iF, "She's not crazy, she just lives in a swamp.\n\nTalk to Polaris.")
fnCreateAchievement(sAch, "MeetSharelock",     "Ch104Sharelock",      "Expired Warranty",                  sPrefix .. "MeetSharelock",     iF, "Might want to get that hip checked out.\n\nFinish Sharelock's investigation.")
fnCreateAchievement(sAch, "DrinkingContest",   "Ch105Drinking",       "Nobody Outdrinks a Plant",          sPrefix .. "DrinkingContest",   iF, "HERE COMES THE NIGHT TRAIN.\n\nWin the drinking contest.")
fnCreateAchievement(sAch, "Marriedraunes",     "Ch106Married",        "I Can Do Whatever I Want",          sPrefix .. "Marriedraunes",     iF, "I like these characters so I gave them an achievement.")
fnCreateAchievement(sAch, "WereCatsandra",     "Ch107Cassandra",      "No Bad Outcome",                    sPrefix .. "WereCatsandra",     iF, "Cats and people get along, right?\n\nRescue Cassandra, or don't.")
fnCreateAchievement(sAch, "FindGemcutter",     "Ch108Gemcutter",      "It's a Hobby",                      sPrefix .. "FindGemcutter",     iF, "Meet Alicia the gemcutter.")
fnCreateAchievement(sAch, "FinishPie",         "Ch109Pie",            "What Were The Ingredients Again?",  sPrefix .. "FinishPie",         iF, "Have Breanne make you a Pepper Pie.")
fnCreateAchievement(sAch, "FindClaudia",       "Ch110Claudia",        "Penitance, For What?",              sPrefix .. "FindClaudia",       iF, "Rescue Claudia.")
fnCreateAchievement(sAch, "FindSlimeville",    "Ch111Slimeville",     "Masterworks",                       sPrefix .. "FindSlimeville",    iF, "Visit Slimeville.")
fnCreateAchievement(sAch, "FindMediator",      "Ch112Mediator",       "It's Real Silk",                    sPrefix .. "FindMediator",      iF, "Acquire Florentina's Mediator job.")
fnCreateAchievement(sAch, "SeePollination",    "Ch113Pollination",    "Getting Busy With A Bee",           sPrefix .. "SeePollination",    iF, "Get the bonus bee scene as alraune Mei.")
fnCreateAchievement(sAch, "MeetCrowbarChan",   "Ch114Crowbar",        "The Best, Bar None",                sPrefix .. "MeetCrowbarChan",   iF, "Reunite with an old friend you've never met.")
fnCreateAchievement(sAch, "BecomeAlraune",     "Ch115Alraune",        "Wild Rose",                         sPrefix .. "BecomeAlraune",     iF, "Transform into an alraune.")
fnCreateAchievement(sAch, "BecomeBee",         "Ch116Bee",            "Buzz Buzz",                         sPrefix .. "BecomeBee",         iF, "Transform into a bee.")
fnCreateAchievement(sAch, "BecomeGhost",       "Ch117Ghost",          "Opacity Not Included",              sPrefix .. "BecomeGhost",       iF, "Transform into a ghost.")
fnCreateAchievement(sAch, "BecomeSlime",       "Ch118Slime",          "Hope This Doesn't Awaken Anything", sPrefix .. "BecomeSlime",       iF, "Transform into a slimegirl.")
fnCreateAchievement(sAch, "BecomeGravemarker", "Ch119Gravemarker",    "Incorruptable",                     sPrefix .. "BecomeGravemarker", iF, "Transform into a gravemarker.")
fnCreateAchievement(sAch, "BecomeWerecat",     "Ch120Werecat",        "Slitted Eyes",                      sPrefix .. "BecomeWerecat",     iF, "Transform into a werecat.")
fnCreateAchievement(sAch, "BecomeWisphag",     "Ch128Wisphag",        "Warden of Souls",                   sPrefix .. "BecomeWisphag",     iF, "Transform into a wisphag.")
fnCreateAchievement(sAch, "BecomeZombee",      "Ch121Zombee",         "Return Of The Living Bees",         sPrefix .. "BecomeZombee",      iF, "Transform into a zombee.")
fnCreateAchievement(sAch, "BecomeRubber",      "Ch122Rubber",         "Pool Toy",                          sPrefix .. "BecomeRubber",      iS, "Transform into a rubberine.")
fnCreateAchievement(sAch, "FinishZombee",      "Ch123FinishZombee",   "Bittersweet",                       sPrefix .. "FinishZombee",      iS, "End the zombee threat in the hive.")
fnCreateAchievement(sAch, "FinishDungeon",     "Ch124FinishDungeon",  "Came Out Wrong",                    sPrefix .. "FinishDungeon",     iS, "Stop the ritual in the Dimensional Trap.")
fnCreateAchievement(sAch, "FinishManor",       "Ch125Manor",          "A Dish Served Cold",                sPrefix .. "FinishManor",       iS, "End the Warden's cruel revenge.")
fnCreateAchievement(sAch, "FinishConvent",     "Ch126Convent",        "Goth GF",                           sPrefix .. "FinishConvent",     iS, "Defeat the leader of the Mushraunes.")
fnCreateAchievement(sAch, "FinishMannequin",   "Ch129Mannequin",      "Plastic Paradise",                  sPrefix .. "FinishMannequin",   iS, "Defeat Victoria and her cronies.")
fnCreateAchievement(sAch, "FinishDescent",     "Ch130Descent",        "Realm of Nostalgia",                sPrefix .. "FinishDescent",     iH, "Find the expedition's deepest testament.")
fnCreateAchievement(sAch, "FinishChapter1",    "Ch127FinishChapter1", "The Future Is Monochrome",          sPrefix .. "FinishChapter",     iH, "Complete chapter 1.")
    
-- |[Chapter 1 Paragons]|
sPrefix = "Root/Images/AdventureUI/AchievementIco/Chapter1Para|"
sAch = "Chapter1Para"
AM_SetPropertyJournal("Register Achievement Category", sAch, "Chapter 1 Paragons", "Root/Images/AdventureUI/AchievementIco/CategoryIcons|Chapter1")
fnCreateAchievement(sAch, "Ch1ParaUnlockAny", "Ch1Para00Unlock", "Can't Be That Hard, Right?",                 sPrefix .. "UnlockAny", iF, "Defeat any paragon to unlock a region's paragons.")
fnCreateAchievement(sAch, "Ch1ParaNature",    "Ch1Para01Wilds",  "Fury of the Wilds",                          sPrefix .. "Nature",    iF, "Defeat the toughest nature-aligned paragons.")
    AM_SetPropertyJournal("Allocate Achievement Requirements", "Ch1ParaNature", 7)
    AM_SetPropertyJournal("Set Achievement Requirements", "Ch1ParaNature", 0, "Toxic Slimegirl", 1.0, false, true)
    AM_SetPropertyJournal("Set Achievement Requirements", "Ch1ParaNature", 1, "Alraune Keeper",  1.0, false, true)
    AM_SetPropertyJournal("Set Achievement Requirements", "Ch1ParaNature", 2, "Sprout Thrasher", 1.0, false, true)
    AM_SetPropertyJournal("Set Achievement Requirements", "Ch1ParaNature", 3, "Bee Skirmisher",  1.0, false, true)
    AM_SetPropertyJournal("Set Achievement Requirements", "Ch1ParaNature", 4, "Bee Tracker",     1.0, false, true)
    AM_SetPropertyJournal("Set Achievement Requirements", "Ch1ParaNature", 5, "Werecat Thief",   1.0, false, true)
    AM_SetPropertyJournal("Set Achievement Requirements", "Ch1ParaNature", 6, "Werecat Prowler", 1.0, false, true)

fnCreateAchievement(sAch, "Ch1ParaSpooky",    "Ch1Para02Spooky", "I'm Not S-S-S-Scared!",                      sPrefix .. "Spooky", iF,   "Defeat the very spooky paragons.")
    AM_SetPropertyJournal("Allocate Achievement Requirements", "Ch1ParaSpooky", 4)
    AM_SetPropertyJournal("Set Achievement Requirements", "Ch1ParaSpooky", 0, "Ghost Maid",    1.0, false, true)
    AM_SetPropertyJournal("Set Achievement Requirements", "Ch1ParaSpooky", 1, "Candle Haunt",  1.0, false, true)
    AM_SetPropertyJournal("Set Achievement Requirements", "Ch1ParaSpooky", 2, "Bloody Mirror", 1.0, false, true)
    AM_SetPropertyJournal("Set Achievement Requirements", "Ch1ParaSpooky", 3, "Eye Drawer",    1.0, false, true)

fnCreateAchievement(sAch, "Ch1ParaPowerful", "Ch1Para03Powerful", "Did Anyone Catch The Number Of That Truck?", sPrefix .. "Powerful", iF, "Defeat the powerful paragons.")
    AM_SetPropertyJournal("Allocate Achievement Requirements", "Ch1ParaPowerful", 5)
    AM_SetPropertyJournal("Set Achievement Requirements", "Ch1ParaPowerful", 0, "Gravemarker", 1.0, false, true)
    AM_SetPropertyJournal("Set Achievement Requirements", "Ch1ParaPowerful", 1, "Mushthrall",  1.0, false, true)
    AM_SetPropertyJournal("Set Achievement Requirements", "Ch1ParaPowerful", 2, "Zombee",      1.0, false, true)
    AM_SetPropertyJournal("Set Achievement Requirements", "Ch1ParaPowerful", 3, "Finger",      1.0, false, true)
    AM_SetPropertyJournal("Set Achievement Requirements", "Ch1ParaPowerful", 4, "Palm",        1.0, false, true)

fnCreateAchievement(sAch, "Ch1ParaWeDidIt", "Ch1Para04Overall", "For My Next Trick, I Will Beat Up God",      sPrefix .. "WeDidIt", iF,  "Get all of the paragon achievements.")
    AM_SetPropertyJournal("Allocate Achievement Requirements", "Ch1ParaWeDidIt", 3)
    AM_SetPropertyJournal("Set Achievement Requirements", "Ch1ParaWeDidIt", 0, "Fury of the Wilds",                          1.0, false, true)
    AM_SetPropertyJournal("Set Achievement Requirements", "Ch1ParaWeDidIt", 1, "I'm Not S-S-S-Scared",                       1.0, false, true)
    AM_SetPropertyJournal("Set Achievement Requirements", "Ch1ParaWeDidIt", 2, "Did Anyone Catch The Number Of That Truck?", 1.0, false, true)

-- |[Chapter 5 Gameplay]|
sPrefix = "Root/Images/AdventureUI/AchievementIco/Chapter5|"
sAch = "Chapter5"
AM_SetPropertyJournal("Register Achievement Category", sAch, "Chapter 5", "Root/Images/AdventureUI/AchievementIco/CategoryIcons|Chapter5")
fnCreateAchievement(sAch, "FindBlueKey",       "Ch500BlueKey",           "Containment Failure",       sPrefix .. "FindBlueKey",       iF, "Something big got out...\n\nFind the blue keycard.")
fnCreateAchievement(sAch, "MeetSophie",        "Ch501Sophie",            "My Darling Admirer",        sPrefix .. "MeetSophie",        iF, "Meet Sophie, maybe ask her out?")
fnCreateAchievement(sAch, "GolemCassandra",    "Ch502Cassandra",         "Cleaned Up That Mess",      sPrefix .. "GolemCassandra",    iF, "Convert Cassandra, or help her escape.\n\nBoth are good.")
fnCreateAchievement(sAch, "TextAdventure",     "Ch503TextAdventure",     "Primitive Graphics",        sPrefix .. "TextAdventure",     iF, "When was this game made, 2014?\n\nPlay the text adventure.")
fnCreateAchievement(sAch, "Sickos",            "Ch504Gems",              "Gem Enthusiast",            sPrefix .. "Sickos",            iF, "All gem dramas cancelled!\n\nMeet VN-19 in Lower Regulus City.")
fnCreateAchievement(sAch, "GetInspected",      "Ch505Inspection",        "Stand At Attention",        sPrefix .. "GetInspected",      iF, "Perform an inspection with Unit 499323.")
fnCreateAchievement(sAch, "FindCrowbar",       "Ch506Crowbar",           "Nyuuu!",                    sPrefix .. "FindCrowbar",       iF, "Your optical receptors are working fine.\n\nRecharge the batteries at Cryogenics.")
fnCreateAchievement(sAch, "MeetEileen",        "Ch507Eileen",            "Unexpected Ally",           sPrefix .. "MeetEileen",        iF, "Meet the command unit at Serenity Crater.")
fnCreateAchievement(sAch, "NonCanonStory",     "Ch546Story",             "What a story, Christine!",  sPrefix .. "NonCanonStory",     iF, "Hear Christine's non-canon story (but what if?)")
fnCreateAchievement(sAch, "BecomeGolem",       "Ch508Golem",             "Conversion Complete",       sPrefix .. "BecomeGolem",       iF, "Transform into a golem.")
fnCreateAchievement(sAch, "BecomeDarkmatter",  "Ch509Darkmatter",        "Melted Starlight",          sPrefix .. "BecomeDarkmatter",  iF, "Transform into a darkmatter girl.")
fnCreateAchievement(sAch, "BecomeLatex",       "Ch510Latex",             "Spiffy Wax Job",            sPrefix .. "BecomeLatex",       iF, "Transform into a latex drone.")
fnCreateAchievement(sAch, "BecomeElectro",     "Ch511Electro",           "So Psue Me!",               sPrefix .. "BecomeElectro",     iF, "Transform into an electrosprite.")
fnCreateAchievement(sAch, "BecomeSecrebot",    "Ch545Secrebot",          "It's a Living",             sPrefix .. "BecomeSecrebot",    iF, "Transform into a secrebot.")
fnCreateAchievement(sAch, "BecomeSteam",       "Ch512Steam",             "Literal Steampunk",         sPrefix .. "BecomeSteam",       iF, "Transform into a steam droid.")
fnCreateAchievement(sAch, "BecomeDreamer",     "Ch513Dreamer",           "Eternity of Bleeding",      sPrefix .. "BecomeDreamer",     iF, "Transform into a dreamer.")
fnCreateAchievement(sAch, "FinishEquinox",     "Ch514Equinox",           "First Drummer",             sPrefix .. "FinishEquinox",     iH, "Complete the Equinox Labs.")
fnCreateAchievement(sAch, "FinishSerenity",    "Ch515Serenity",          "It Was Always Here",        sPrefix .. "FinishSerenity",    iH, "Complete Serenity Crater.")
fnCreateAchievement(sAch, "FinishMines",       "Ch516Mines",             "The First We've Met Again", sPrefix .. "FinishMines",       iH, "Reach Floor 50 in the Tellurium Mines, talk to the person there.")
fnCreateAchievement(sAch, "MeetSX",            "Ch517MeetSX",            "Fire-Red Cherry",           sPrefix .. "MeetSX",            iF, "Meet SX-399.")
fnCreateAchievement(sAch, "FixSX",             "Ch518FixSX",             "Hotshot",                   sPrefix .. "FixSX",             iH, "Upgrade SX-399.")
fnCreateAchievement(sAch, "FinishCryo",        "Ch519FinishCryo",        "I Don't Trust Myself",      sPrefix .. "FinishCryo",        iH, "Make a decision in the Cryogenics reactor.")
fnCreateAchievement(sAch, "FinishManufactory", "Ch520FinishManufactory", "Nothing To Report",         sPrefix .. "FinishManufactory", iH, "Solve the mystery of the Manufactory.")
fnCreateAchievement(sAch, "FinishLRT",         "Ch521FinishLRT",         "The Master Is Displeased",  sPrefix .. "FinishLRT",         iH, "Complete the LRT facility.")

--Gala, Biolabs.
fnCreateAchievement(sAch, "FinishShopping",    "Ch522Shopping", "Faraway Promise",                            sPrefix .. "FinishShopping",    iS, "Attend the gala with a plus one.")
fnCreateAchievement(sAch, "HeavySupport",      "Ch523Support",  "Three of a Kind, Let's Do This",             sPrefix .. "HeavySupport",      iH, "Get a bit of unexpected heavy support.")
fnCreateAchievement(sAch, "ReachBiolabs",      "Ch524Biolabs",  "Vacation Time!",                             sPrefix .. "ReachBiolabs",      iS, "Reach the Biolabs.")
fnCreateAchievement(sAch, "FinishReika",       "Ch525Reika",    "The Brave Little Daffodil",                  sPrefix .. "FinishReika",       iH, "Save Reika.")
fnCreateAchievement(sAch, "GetMilked",         "Ch526Milk",     "You Know Exactly What Kind Of Game This Is", sPrefix .. "GetMilked",         iS, "Get milked in the Biolabs. Act innocent.")
fnCreateAchievement(sAch, "FinishAquatic",     "Ch527Aquatics", "Freedom At Any Price",                       sPrefix .. "FinishAquatic",     iH, "Rescue the units in Aquatic Genetics.")
fnCreateAchievement(sAch, "MeetMosquito",      "Ch528Mosquito", "Do I Look Like I Know What a JPEG Is?",      sPrefix .. "MeetMosquito",      iF, "Meet Dr. Maisie.")
fnCreateAchievement(sAch, "FinishMovie",       "Ch529Moth",     "Moth Fanatic",                               sPrefix .. "FinishMovie",       iF, "Find the evidence and prove Agent Almond's innocence.")
fnCreateAchievement(sAch, "FinishEpsilon",     "Ch530Epsilon",  "The Student, The Teacher",                   sPrefix .. "FinishEpsilon",     iH, "Resolve the Epsilon crisis.")
fnCreateAchievement(sAch, "BecomeRaiju",       "Ch531Raiju",    "Unbridled Affection",                        sPrefix .. "BecomeRaiju",       iF, "Transform into a raiju.")
fnCreateAchievement(sAch, "BecomeRaibie",      "Ch541Raibie",   "Modern Science",                             sPrefix .. "BecomeRaibie",      iS, "Transform into a raibie.")
fnCreateAchievement(sAch, "BecomeDoll",        "Ch542Doll",     "Mechanical Perfection",                      sPrefix .. "BecomeDoll",        iH, "Transform into a command unit.")
fnCreateAchievement(sAch, "Ch5BadEnd",         "Ch543BadEnd",   "She Becomes A Thing",                        sPrefix .. "Ch5BadEnd",         iS, "Get the very much non-canon bad end.")
fnCreateAchievement(sAch, "Ch5GoodEnd",        "Ch544GoodEnd",  "Mightier Than The Atom's Magnified A Thousandfold", sPrefix .. "Ch5GoodEnd", iS, "We can bring to birth a new world from the ashes of the old.\nFor the union makes us strong.")

-- |[Chapter 5 Paragons]|
sPrefix = "Root/Images/AdventureUI/AchievementIco/Chapter5Para|"
sAch = "Chapter5Para"
AM_SetPropertyJournal("Register Achievement Category", sAch, "Chapter 5 Paragons", "Root/Images/AdventureUI/AchievementIco/CategoryIcons|Chapter5")
fnCreateAchievement(sAch, "Ch5ParaUnlockAny", "Ch5Para00Unlock",  "Primed and Ready!",                 sPrefix .. "UnlockAny",  iF, "Defeat any paragon to unlock a region's paragons.")
fnCreateAchievement(sAch, "Ch5ParaDreamer",   "Ch5Para01Dreamer", "This Might Actually Be Impossible", sPrefix .. "Impossible", iF, "Defeat a Dreamer Paragon. Good luck, it might be impossible.\nSeriously.")
    AM_SetPropertyJournal("Allocate Achievement Requirements", "Ch5ParaDreamer", 1)
    AM_SetPropertyJournal("Set Achievement Requirements", "Ch5ParaDreamer", 0, "Dreamer", 1.0, false, true)

-- |[ =================================== Achievement Loading ================================== ]|
--Load achievements from save. This does not handle the Steam versions, just the internal versions.
AM_SetPropertyJournal("Read Achievements")

-- |[ =============================== Steam Achievement Handling =============================== ]|
--Allocate remaps then uploads them.
AM_SetPropertyJournal("Allocate Achievement Remaps", #zaAchievements)
for i = 1, #zaAchievements, 1 do
    AM_SetPropertyJournal("Set Achievement Remap", i-1, zaAchievements[i].sInternalName, zaAchievements[i].sSteamName)
end

--Handles crossreferencing these achievements with Steam. If Steam is not active, these do nothing.
AM_SetPropertyJournal("Crossreference Achievements")
