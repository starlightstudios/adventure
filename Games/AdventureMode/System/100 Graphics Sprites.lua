-- |[ ==================================== Graphics: Sprites =================================== ]|
--Sprite graphics info.
SLF_Open(gsDatafilesPath .. "Sprites.slf")

-- |[Common]|
--Modify the distance filters to keep everything pixellated. This is only for sprites.
local iNearest = DM_GetEnumeration("GL_NEAREST")
ALB_SetTextureProperty("MagFilter", iNearest)
ALB_SetTextureProperty("MinFilter", iNearest)
ALB_SetTextureProperty("Special Sprite Padding", true)
--Bitmap_ActivateAtlasing()

-- |[Load List]|
--Put everything in here on a "System Sprites" loading list. Sprites generally resolve ASAP.
local sEverythingList = "System Sprites"
fnCreateDelayedLoadList(sEverythingList)

-- |[ ======================================== Nowhere ========================================= ]|
--The six bearers always load their human sprites. Talia is missing since she's not created yet.
fnExtractSpriteList({"Human"}, true, "Mei_")
fnExtractSpriteList({"Human"}, true, "Sanya_")
fnExtractSpriteList({"Human"}, true, "Jeanne_")
fnExtractSpriteList({"Human"}, true, "Lotta_")
fnExtractSpriteList({"Human", "Male"}, true, "Christine_")

--Maram and Septima also load.
fnExtractSpriteList({"Septima"}, true)
fnExtractSpriteList({"Rilmani"}, false)

-- |[ ================================== Padded System Sprites ================================= ]|
ALB_SetTextureProperty("Special Sprite Padding", true)
fnExecAutoload("AutoLoad|Adventure|SystemSprites_Padded")
ALB_SetTextureProperty("Special Sprite Padding", false)

-- |[ ================================ Un-Padded System Sprites ================================ ]|
fnExecAutoload("AutoLoad|Adventure|SystemSprites_Unpadded")

-- |[ ==================================== Combat Animations ================================== ]|
-- |[Setup]|
--These are done after atlasing is disabled since they are large, transparent, and tend to slow down
-- the atlas loader.
SLF_Open(gsDatafilesPath .. "CombatAnimations.slf")
fnExecAutoload("AutoLoad|Adventure|CombatAnimations")

-- |[ ======================================== Finish Up ======================================= ]|
-- |[Issue Load Order]|
--Order all sprites to load when possible.
fnLoadDelayedBitmapsFromList(sEverythingList, gciDelayedLoadLoadImmediately)

-- |[Clean]|
--Bitmap_DeactivateAtlasing()
ALB_SetTextureProperty("Restore Defaults")
SLF_Close()
