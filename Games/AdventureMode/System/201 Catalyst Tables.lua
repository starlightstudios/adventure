-- |[ ==================================== Catalyst Tables ===================================== ]|
--Builds a list of all catalysts in the game and which rooms they are in. This is used to make
-- sure the catalyst listing is accurate for each chapter.
--Once built, this is uploaded to the MapManager, which keeps each set internally. The lists are
-- only built once per execution run unless explicitly flagged to rebuild.
--Note: Some chapters are broken into parts which have their own catalyst counts.
if(gzHasBuiltCatalystTables ~= nil) then return end

-- |[Set Flag]|
gzHasBuiltCatalystTables = true
MapM_ClearCatalystList()

-- |[Chapter 1]|
local zaChapter1List = {}
table.insert(zaChapter1List, {"ArbonnePlainsD",         "ChestB", "CATALYST|Accuracy"})
table.insert(zaChapter1List, {"EvermoonE",              "ChestB", "CATALYST|Accuracy"})
table.insert(zaChapter1List, {"TrannadarTradingPost",   "ChestB", "CATALYST|Accuracy"})
table.insert(zaChapter1List, {"TrapMainFloorCentral",   "ChestA", "CATALYST|Attack"})
table.insert(zaChapter1List, {"TrapMainFloorEast",      "ChestA", "CATALYST|Attack"})
table.insert(zaChapter1List, {"TrapBasementE",          "ChestB", "CATALYST|Attack"})
table.insert(zaChapter1List, {"BeehiveBasementB",       "ChestA", "CATALYST|Evade"})
table.insert(zaChapter1List, {"EvermoonS",              "ChestC", "CATALYST|Evade"})
table.insert(zaChapter1List, {"QuantirManseBasementW",  "ChestA", "CATALYST|Evade"})
table.insert(zaChapter1List, {"EvermoonNE",             "ChestB", "CATALYST|Health"})
table.insert(zaChapter1List, {"EvermoonSlimeVillage",   "ChestA", "CATALYST|Health"})
table.insert(zaChapter1List, {"PlainsNW",               "ChestA", "CATALYST|Health"})
table.insert(zaChapter1List, {"TrapBasementC",          "ChestA", "CATALYST|Health"})
table.insert(zaChapter1List, {"TrapBasementH",          "ChestA", "CATALYST|Health"})
table.insert(zaChapter1List, {"QuantirManseSecretExit", "ChestA", "CATALYST|Health"})
table.insert(zaChapter1List, {"StForasF",               "ChestF", "CATALYST|Health"})
table.insert(zaChapter1List, {"StarfieldMausoleumB",    "ChestA", "CATALYST|Health"})
table.insert(zaChapter1List, {"ArbonnePlainsC",         "ChestF", "CATALYST|Health"})
table.insert(zaChapter1List, {"WildsTowerA",            "ChestB", "CATALYST|Health"})
table.insert(zaChapter1List, {"EvermoonSEC",            "ChestA", "CATALYST|Initiative"})
table.insert(zaChapter1List, {"SaltFlats",              "ChestA", "CATALYST|Initiative"})
table.insert(zaChapter1List, {"QuantirManseBasementE",  "ChestF", "CATALYST|Initiative"})
table.insert(zaChapter1List, {"BeehiveBasementE",       "ChestA", "CATALYST|Skill"})
table.insert(zaChapter1List, {"TrapUpperFloorMain",     "ChestA", "CATALYST|Skill"})
table.insert(zaChapter1List, {"EvermoonCassandraA",     "ChestA", "CATALYST|Skill"})
table.insert(zaChapter1List, {"PlainsC",                "ChestD", "CATALYST|Skill"})
table.insert(zaChapter1List, {"StarfieldSwampB",        "ChestE", "CATALYST|Skill"})
table.insert(zaChapter1List, {"TrafalNW",               "ChestB", "CATALYST|Skill"})

-- |[Chapter 2]|
-- |[Chapter 5, Part 1]|
local zaChapter51List = {}
table.insert(zaChapter51List, {"RegulusCity198C",       "Chest A", "CATALYST|Health"})
table.insert(zaChapter51List, {"RegulusCryoC",          "Chest A", "CATALYST|Health"}) --Check
table.insert(zaChapter51List, {"RegulusExteriorWB",     "Chest H", "CATALYST|Health"})
table.insert(zaChapter51List, {"RegulusLRTEA",          "Chest A", "CATALYST|Health"}) --Check
table.insert(zaChapter51List, {"SprocketCityA",         "Chest A", "CATALYST|Health"})
table.insert(zaChapter51List, {"RegulusCryoLowerC",     "ChestB",  "CATALYST|Skill"})  --Check
table.insert(zaChapter51List, {"RegulusCryoPowerCoreD", "Chest A", "CATALYST|Skill"})
table.insert(zaChapter51List, {"RegulusExteriorTRNB",   "ChestA",  "CATALYST|Skill"})
table.insert(zaChapter51List, {"RegulusLRTHB",          "ChestA",  "CATALYST|Skill"})  --Check
table.insert(zaChapter51List, {"RegulusManufactoryG",   "ChestC",  "CATALYST|Skill"})
table.insert(zaChapter51List, {"SerenityObservatoryE",  "Chest A", "CATALYST|Skill"})
 
-- |[Chapter 5, Part 2]|
local zaChapter52List = {}
table.insert(zaChapter52List, {"RegulusBiolabsAmphibianE", "ChestA",  "CATALYST|Accuracy"})
table.insert(zaChapter52List, {"RegulusBiolabsAmphibianE", "ChestB",  "CATALYST|Accuracy"})
table.insert(zaChapter52List, {"RegulusBiolabsAmphibianE", "ChestC",  "CATALYST|Accuracy"})
table.insert(zaChapter52List, {"RegulusBiolabsAlphaC",     "Chest D", "CATALYST|Evade"})
table.insert(zaChapter52List, {"RegulusBiolabsDatacoreD",  "Chest C", "CATALYST|Evade"})
table.insert(zaChapter52List, {"RegulusBiolabsGeneticsC",  "Chest B", "CATALYST|Evade"})

-- |[Upload]|
for i = 1, #zaChapter1List, 1 do
    MapM_RegisterCatalyst("Chapter 1", zaChapter1List[i][1], zaChapter1List[i][2], zaChapter1List[i][3])
end
for i = 1, #zaChapter51List, 1 do
    MapM_RegisterCatalyst("Chapter 5-1", zaChapter51List[i][1], zaChapter51List[i][2], zaChapter51List[i][3])
end
for i = 1, #zaChapter52List, 1 do
    MapM_RegisterCatalyst("Chapter 5-2", zaChapter52List[i][1], zaChapter52List[i][2], zaChapter52List[i][3])
end
