-- |[ ===================================== World Strings ====================================== ]|
--World-related strings. Presently TilemapActor.

-- |[Tilemap Actor]|
TM_SetStringIn("TilemapActor", 0, "[iTurns] turns!")
TM_SetStringIn("TilemapActor", 1, "Preemptive Strike!")
