-- |[ ============================ Adventure String Initialization ============================= ]|
--All strings in use inside the base engine are initialized here by calling subfiles. This means
-- that a translation has the opportunity to change an internal string.
--This is called after an Adventure game is loading. Title screen strings need to be handled by
-- the engine version of this file.

-- |[Setter Function]|
--Global variable tracking which index is currently being edited.
giIndex = 0

--Function to reset the index.
function fnResetTransIndex()
	giIndex = 0
end

--Sets the text and advances the cursor.
function fnSetTransString(psModuleName, psText)
	TM_SetStringIn(psModuleName, giIndex, psText)
	giIndex = giIndex + 1
end

-- |[Call Listing]|
local sBasePath = fnResolvePath()
LM_ExecuteScript(sBasePath .. "001 World.lua")
LM_ExecuteScript(sBasePath .. "002 Items.lua")

-- |[Clean]|
giIndex = nil
fnResetTransIndex = nil
fnSetTransString = nil
