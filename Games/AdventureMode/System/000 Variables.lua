-- |[ ======================================= Variables ======================================== ]|
--Sets up variables and constants used by the Lua state. These are chapter-independent, this file
-- always runs.
--When marked with a c, assume the value is a "constant" and reflects something from the C++ code.
-- Editing these constants will not help you, I assure you.

-- |[ ====================================== Diagnostics ======================================= ]|
-- |[Autoloader]|
--Diagnostic Levels for Autoloader
gciAutoloader_Diagnostic_Off = 0
gciAutoloader_Diagnostic_ReportNames = 1
gciAutoloader_Diagnostic_ReportInvalidGroup = 2
gciAutoloader_Diagnostic_ReportEntries = 4
gciAutoloader_Diagnostic_All = 7

--Global flag.
gciAutoloader_Diagnostic_Level = gciAutoloader_Diagnostic_Off

-- |[Diagnostics for Extra, Extra!]|
gciExtraExtra_Diagnostic_Off = 0
gciExtraExtra_Diagnostic_On = 1

--Flag.
gciExtraExtra_Diagnostic_Level = gciExtraExtra_Diagnostic_Off

-- |[Combat Diagnostics]|
gciCombatExec_Diagnostic_Off = 0
gciCombatExec_Diagnostic_Announce = 1
gciCombatExec_Diagnostic_Arguments = 2
gciCombatExec_Diagnostic_Damage = 4
gciCombatExec_Diagnostic_All = 255

--Flag.
giCombatExec_Diagnostic_Level = gciCombatExec_Diagnostic_Off

-- |[AI Diagnostics]|
gciAI_Diagnostic_Off = 0
gciAI_Diagnostic_Decision = 1
gciAI_Diagnostic_SkillRoll = 2
gciAI_Diagnostic_All = 255

--Flag.
giAI_Diagnostic_Level = gciAI_Diagnostic_Off

-- |[Diagnostics Table]|
--This table stores diagnostic functions. It's basically a namespace.
gzDiagnostics = {}

-- |[Diagnostic Variables]|
--AIGeneral
Debug_RegisterFlag("AIGeneral: All", false)
Debug_RegisterFlag("AIGeneral: Decide Action", false)

--AIStrings
Debug_RegisterFlag("AIString: All",           false)
Debug_RegisterFlag("AIString: Conditions",    false)
Debug_RegisterFlag("AIString: Execute",       false)
Debug_RegisterFlag("AIString: Handle String", false)
Debug_RegisterFlag("AIString: Lists",         false)
Debug_RegisterFlag("AIString: Parsing",       false)
Debug_RegisterFlag("AIString: Run",           false)
Debug_RegisterFlag("AIString: Subterm",       false)
Debug_RegisterFlag("AIString: Term",          false)

--Catalysts
Debug_RegisterFlag("Catalysts: All", false)
Debug_RegisterFlag("Catalysts: Loading", false)

--Combat
Debug_RegisterFlag("Combat: Response Script", false)

--Costumes
Debug_RegisterFlag("Costume: All", false)
Debug_RegisterFlag("Costume: Character Resolve", false)
Debug_RegisterFlag("Costume: Form Resolve", false)

--DiaHelper
Debug_RegisterFlag("DiaHelper: All", false)
Debug_RegisterFlag("DiaHelper: Actors", false)

--Enemy Stat Chart
Debug_RegisterFlag("Enemy Stat Chart: All", false)

--Form Handlers
Debug_RegisterFlag("Form Handling: All", false)

--Hunting
Debug_RegisterFlag("Hunting: All", false)
Debug_RegisterFlag("Hunting: Create Hunt", false)
Debug_RegisterFlag("Hunting: Data Report", false)
Debug_RegisterFlag("Hunting: Constructor", false)

--Journal Handlers
Debug_RegisterFlag("Journal: All", false)
Debug_RegisterFlag("Journal: Lists", false)
Debug_RegisterFlag("Journal: Bestiary", false)

--Lights
Debug_RegisterFlag("Lights: All", false)

--Map Helper Class
Debug_RegisterFlag("MapHelper: All", false)
Debug_RegisterFlag("MapHelper: PregenRandom", false)

--Predictions.
Debug_RegisterFlag("Predictions: All", false)
Debug_RegisterFlag("Predictions: Creation", false)
Debug_RegisterFlag("Predictions: Execution", false)
Debug_RegisterFlag("Predictions: Build Prediction Box", false)
Debug_RegisterFlag("Predictions: Effect Module", false)

--Reversion
Debug_RegisterFlag("Reversion: All", false)

--Special Flag, only use when diagnosing a missing talk sprite. Characters normally reference unloaded portraits during loading
-- so this will bark errors that are harmless otherwise.
Debug_RegisterFlag("Talk Sprite: Missing Image", false)

--Statmod.
Debug_RegisterFlag("Statmod: All", false)
Debug_RegisterFlag("Statmod: Build Result Script", false)

--TurnResettable
Debug_RegisterFlag("TurnResettable: General",  false)
Debug_RegisterFlag("TurnResettable: Explicit", false)

--Unlocking Interface
Debug_RegisterFlag("Unlock: All", false)
Debug_RegisterFlag("Unlock: Reply", false)

--Misc
Debug_RegisterFlag("Uncurse Knockout",       false)
Debug_RegisterFlag("Party Reformation",      false)
Debug_RegisterFlag("Party Reinforcements",   false)
Debug_RegisterFlag("Standard Job: Creation", false)

--Diagnostics Samples:
--local bDiagnostics = Debug_GetFlag("Standard Job: Creation")
--local bDiagnostics = fnCheckDebugFlags("Costume: All", "Costume: Character Resolve")
--local bDiagnostics = fnAutoCheckDebugFlag("Costume: Character Resolve")
--Debug_PushPrint(bDiagnostics, "Beginning standard job script creation.\n")
--Debug_Print("Caller: " .. LM_GetCallStack(1) .. "\n")
--Debug_PopPrint("Finished.\n")

-- |[Checker Function]|
--Returns true if any of the diagnostic flags are true, false if all are false. Flags can be nil
-- in which case they are not checked.
function fnCheckDebugFlags(psFlagA, psFlagB, psFlagC, psFlagD, psFlagE)

    if(psFlagA ~= nil) then
        if(Debug_GetFlag(psFlagA) == true) then return true end
    end
    if(psFlagB ~= nil) then
        if(Debug_GetFlag(psFlagB) == true) then return true end
    end
    if(psFlagC ~= nil) then
        if(Debug_GetFlag(psFlagC) == true) then return true end
    end
    if(psFlagD ~= nil) then
        if(Debug_GetFlag(psFlagD) == true) then return true end
    end
    if(psFlagE ~= nil) then
        if(Debug_GetFlag(psFlagE) == true) then return true end
    end

    --All checks failed.
    return false
end

-- |[Auto-Checker Function]|
--Assuming an entry is formatted in format "NameOfThing: SubEntry" for diagnostic flags, checks if
-- the entry "NameOfThing: All" is true. If it is, returns the all flag, otherwise returns true if the
-- subflag is true.
function fnAutoCheckDebugFlag(psFlag)

    -- |[Argument Check]|
    if(psFlag == nil) then return false end
    
    -- |[All Check]|
    --Check for the location of the colon.
    local iColonLocation = string.find(psFlag, ":")
    if(iColonLocation ~= nil) then
        local sAllFlag = string.sub(psFlag, 1, iColonLocation-1) .. ": All"
        if(Debug_GetFlag(sAllFlag) == true) then return true end
    end
    
    -- |[Flag Check]|
    return Debug_GetFlag(psFlag)
end

-- |[ =================================== Class Constructors =================================== ]|
--Call all class constructors. Combat versions are done below.
LM_ExecuteScript(gsRoot .. "Classes/ZClassRouting.lua")

-- |[ ========================================== Other ========================================= ]|
-- |[Global Script Variables]|
--Variables for the game boot.
DL_AddPath("Root/Variables/Nowhere/Scenes/")
VM_SetVar("Root/Variables/Nowhere/Scenes/iHasSeenControlsDialogue", "N", 0.0)

--Paragon Handler
DL_AddPath("Root/Variables/Global/Paragons/")
VM_SetVar("Root/Variables/Global/Paragons/iParagonRoll", "N", LM_GetRandomNumber(-10000, 10000))
VM_SetVar("Root/Variables/Global/Paragons/iAlwaysParagon", "N", 0.0)
VM_SetVar("Root/Variables/Global/Paragons/iNeverParagon", "N", 0.0)

--Other Debug Vars
DL_AddPath("Root/Variables/Global/Debug/")
VM_SetVar("Root/Variables/Global/Debug/iAlwaysShowStats", "N", 0.0)

--Bitmap Statistics
gbShowBitmapStatistics = true

-- |[Debug]|
--Variables that can only be activated for debug cutscene purposes. These should all be false during normal play.
gbBypassTrannadarCutscene = false
gbBypassIntro = false
gbDontCancelMusic = false
gbLoadDebug = false

-- |[Facing Variable]|
--When going through certain exits, this variable can track facing changes. Mostly used by constructors.
giForceFacing = -1

-- |[Combat Standards]|
--Global variable marking surrender or volunteer.
gbIsVolunteer = false

--The function fnStandardAttack() populates these variables.
giStandardDamage = 0
gbStandardImmune = false
giStandardAttackType = 0

--The function fnStandardAccuracy() populates these variables.
gbAttackHit = false
gbAttackCrit = false
gbAttackGlances = false

--The function fnStandardExecution() uses this as a timer offset.
giCombatTimerOffset = 0

--This is used when a script execution package is used during fnStandardExecution(). It is populated with
-- the iTimer value of the abilities.
giStoredTimer = 0

--Used when applying HoTs and DoTs. DoTs always apply first. This timer gets reset to zero when a character
-- begins their action. DoTs always run first, and mark the highest tick they deal damage on. All HoTs then
-- must apply healing after that tick.
giLastDoTTick = 0

--Global structure used to pass information between subroutines when executing an ability.
gzAbiPack = {}          --Variables
gzaAbiHandlerFuncs = {} --Functions

--Reinforcements variable. If an enemy is created, this variable is used to determine when they arrive
-- in combat. It defaults to zero. Make sure to reset it after using it.
giReinforcementTurns = 0

-- |[Tag-Effect Lists]|
--When an attack is going through, sometimes a tag (on an effect or ability) will spawn additional effects.
-- For example, buffing a teammate's normal attack to cause a damage-over-time after every attack. When that
-- happens, these lists are used to generate the effects.
gzaTagEffectsFriendly = {}
gzaTagEffectsHostile = {}

-- |[Other]|
--If true, do not create the party leader when running fnStandardCharacter(). This prevents duplicate
-- party leaders being created when reloading the game.
gbSuppressLeaderCreation = false

--New Chapter Plus flag
gbIsNewChapterPlus = false

-- |[ ===================================== C++ Constants ====================================== ]|
--Constants derived from the C++ executable. Do not edit these.
gci_Constructor_Start = 0
gci_Constructor_PostExec = 1

--SugarFont
gci_Font_NoEffects = 0
gci_Font_Bold = 1
gci_Font_Italics = 2
gci_Font_Underline = 4
gci_Font_AutocenterX = 8
gci_Font_AutocenterY = 16
gci_Font_AutocenterXY = gci_Font_AutocenterX + gci_Font_AutocenterY
gci_Font_RightAlignX = 32
gci_Font_MirrorX = 64
gci_Font_MirrorY = 128
gci_Font_DoubleRenderX = 256
gci_Font_DoubleRenderY = 512

-- |[Directions]|
--Facing directions
gci_Face_North = 0
gci_Face_NorthEast = 1
gci_Face_East = 2
gci_Face_SouthEast = 3
gci_Face_South = 4
gci_Face_SouthWest = 5
gci_Face_West = 6
gci_Face_NorthWest = 7

-- |[Constants]|
--Script Execution Constants.

--Dialogue Constants.
gcbDialogue_Source_NPC = false
gcbDialogue_Source_Party = true
gcbDialogue_Source_Neither = false
gciDialogue_Fadeout_Then_Unload_Ticks = 60

--Fading Constants
gci_Fade_Under_Characters = 0
gci_Fade_Under_GUI = 1
gci_Fade_Over_GUI = 2
gci_Fade_Over_GUI_Blackout_World = 3

--UI Sizes
gci_UI_AdItem_Max_Description_Lines = 7

--AI Constants. Synced to the C++ code.
gci_ACE_Initialize = 0
gci_ACE_New_Round = 1
gci_ACE_Other_ACE_Begins_Turn = 2
gci_ACE_This_ACE_Begins_Turn = 3
gci_ACE_This_ACE_Dies_Turn = 4
gci_ACE_Combat_Ends = 5

--Flashwhite
gci_Flashwhite_Ticks_Total = 150

--Sizing Constants.
gciSizePerTile = 16

--Offsets for Tilemap Actors
gcfTADefaultXOffset = -8.0
gcfTADefaultYOffset = -16.0

--Camera movement speed in cutscenes. Note that the C++ default is 5.0, the value cannot go below 1.0.
gcfCamera_Cutscene_Speed = 5.0

--Target Additions. Modifies target properties to allow downed party members. Add these to the target flags.
gci_Target_Allow_Downed = 16
gci_Target_Allow_Downed_Only = 32

--Resistance types. Used by CombatActions.
gciFactor_Slash = 0
gciFactor_Pierce = 1
gciFactor_Strike = 2
gciFactor_Fire = 3
gciFactor_Ice = 4
gciFactor_Lightning = 5
gciFactor_Holy = 6
gciFactor_Shadow = 7
gciFactor_Bleed = 8
gciFactor_Blind = 9
gciFactor_Poison = 10
gciFactor_Corrode = 11
gciFactor_Terrify = 12

--UI Rendering Slots
gci_ACE_RenderSlot_Basemenu = 0
gci_ACE_RenderSlot_Combat_Base = 1
gci_ACE_RenderSlot_Combat_Ally = 2
gci_ACE_RenderSlot_Mono_Callout = 2
gci_ACE_RenderSlot_Combat_Inspector = 3
gci_ACE_RenderSlot_Equipment = 4
gci_ACE_RenderSlot_Skills = 5
gci_ACE_RenderSlot_Status = 6
gci_ACE_RenderSlot_Vendor = 7
gci_ACE_RenderSlot_Combat_Victory = 8
gci_ACE_RenderSlot_Combat_Enemy = 9
gci_ACE_RenderSlot_Mono_Levelup = 10
gci_ACE_RenderSlot_Mono_CombatUIOffset = 11
gci_ACE_RenderSlot_Trainer = 12
    
--Usability flags.
gciMei        =   1
gciFlorentina =   2
gciChristine  =   4
gciTiffany    =   8
gciSX399      =  16
gciSanya      =  32
gciIzuna      =  64
gciZeke       = 128
gciEmpress    = 256

--Usability prefabs:
gciUsableEveryone = 65535
gciUsableNotZeke = gciUsableEveryone - gciZeke

--Assemble usability lookup table.
gzaUsabilityTable = {}
table.insert(gzaUsabilityTable, {gciMei,        "Mei"})
table.insert(gzaUsabilityTable, {gciFlorentina, "Florentina"})
table.insert(gzaUsabilityTable, {gciChristine,  "Christine"})
table.insert(gzaUsabilityTable, {gciTiffany,    "Tiffany"})
table.insert(gzaUsabilityTable, {gciSX399,      "SX-399"})
table.insert(gzaUsabilityTable, {gciSanya,      "Sanya"})
table.insert(gzaUsabilityTable, {gciIzuna,      "Izuna"})
table.insert(gzaUsabilityTable, {gciZeke,       "Zeke"})
table.insert(gzaUsabilityTable, {gciEmpress,    "Empress"})

-- |[Combat Global Response Codes]|
--C++ Base is ADVCOMBAT_RESPONSE_GENERAL_
gciCombatResponse_BeginCombat = 0
gciCombatResponse_BeginTurnPreInitiative = 1
gciCombatResponse_BeginTurnPostInitiative = 2
gciCombatResponse_BeginTurnPostResponses = 3
gciCombatResponse_BeginAction = 4

-- |[Combat Position Codes]|
--Used for Position application packages.
gciACPoscode_Direct = 0 --Sets the X position manually
gciACPoscode_Party = 1 --Incorporates target into player party
gciACPoscode_Enemy = 2 --Incorporates target into enemy party

--How many ticks are needed for entities to move
gciAC_StandardMoveTicks = 25

-- |[Combat Paragon Codes]|
gciAC_Paragon_No = 0
gciAC_Paragon_Yes = 1
gciAC_Paragon_Despawn = 2

gciParagon_KO_Needed_To_Spawn = 15

-- |[Combat Resolution Codes]|
gciAC_Resolution_None = 0
gciAC_Resolution_Victory = 1
gciAC_Resolution_Defeat = 2
gciAC_Resolution_Retreat = 3
gciAC_Resolution_Surrender = 4

-- |[Combat Party Codes]|
--Codes that indicate which party grouping an entity is in.
gciACPartyGroup_None = 0
gciACPartyGroup_Party = 1
gciACPartyGroup_Enemy = 2
gciACPartyGroup_Graveyard = 3
gciACPartyGroup_Reinforcements = 4
gciACPartyGroup_Environment = 5
gciACPartyGroup_Other = 6

--Max Active Party
gciCombat_MaxActivePartySize = 4

--Stat Groupings
gciStatGroup_Base = 0        --Enemies use only the base
gciStatGroup_Job = 1         --Bonus from being in a job
gciStatGroup_Equipment = 2   --Bonus from worn equipment
gciStatGroup_Script = 3      --Bonus from story stuff
gciStatGroup_PermaEffect = 4 --Bonus from equipped abilities
gciStatGroup_TempEffect = 5  --Bonus from temporary effects
gciStatGroup_Final = 6       --All stats summed

--Stat Indexes
gciStatIndex_HPMax = 0
gciStatIndex_MPMax = 1
gciStatIndex_MPRegen = 2
gciStatIndex_CPMax = 3
gciStatIndex_FreeActionMax = 4
gciStatIndex_FreeActionGen = 5
gciStatIndex_Attack = 6
gciStatIndex_Initiative = 7
gciStatIndex_Accuracy = 8
gciStatIndex_Evade = 9
gciStatIndex_Protection = 10
gciStatIndex_Resist_Start = 11
gciStatIndex_Resist_Slash = 11
gciStatIndex_Resist_Strike = 12
gciStatIndex_Resist_Pierce = 13
gciStatIndex_Resist_Flame = 14
gciStatIndex_Resist_Freeze = 15
gciStatIndex_Resist_Shock = 16
gciStatIndex_Resist_Crusade = 17
gciStatIndex_Resist_Obscure = 18
gciStatIndex_Resist_Bleed = 19
gciStatIndex_Resist_Poison = 20
gciStatIndex_Resist_Corrode = 21
gciStatIndex_Resist_Terrify = 22
gciStatIndex_Resist_End = 22
gciStatIndex_StunCap = 23
gciStatIndex_ThreatMultiplier = 24

--Strings for Diagnostics
gcsaStatStrings = {"HPMax", "MPMax", "MPRegen", "CPMax", "FreeActionMax", "FreeActionGen", "Attack", "Initiative", "Accuracy", "Evade", "Protection", "ResSlash", "ResStrike", "ResPierce", "ResFlame", "ResFreeze",
                   "ResShock", "ResCrusade", "ResObscure", "ResBleed", "ResPoison", "ResCorrode", "ResTerrify", "StunCap", "ThreatMulti"}

--Damage Types
gciDamageType_UseWeapon = -1 --Only used by certain functions like fnSetAbilityExecutionDamage(), do NOT use this as a final damage type.
gciDamageType_Begin      = 0
gciDamageType_Slashing   = 0
gciDamageType_Striking   = 1
gciDamageType_Piercing   = 2
gciDamageType_Flaming    = 3
gciDamageType_Freezing   = 4
gciDamageType_Shocking   = 5
gciDamageType_Crusading  = 6
gciDamageType_Obscuring  = 7
gciDamageType_Bleeding   = 8
gciDamageType_Poisoning  = 9
gciDamageType_Corroding  = 10
gciDamageType_Terrifying = 11
gciDamageType_Total      = 12
gciDamageOffsetToResistances = 1 --Slot 0 is used for "Protection" when checking resistances.

--Damage Type Strings for Diagnostics
gcsaDamageStrings = {"Slashing", "Striking", "Piercing", "Flaming", "Freezing", "Shocking", "Crusading", "Obscuring", "Bleeding", "Poisoning", "Corroding", "Terrifying"}

--Resistance Immunity Threshhold
gciNormalResist = 8
gciResistanceForImmunity = 1000

--AI Script Execution
gciAI_CombatStart = 0
gciAI_TurnStart = 1
gciAI_ActionBegin = 2
gciAI_FreeActionBegin = 3
gciAI_ActionEnd = 4
gciAI_TurnEnd = 5
gciAI_KnockedOut = 6
gciAI_CombatEnds = 7
gciAI_Application_Start = 8
gciAI_Application_End = 9
gciAI_Decide_Action = 10

--AI Special, not a system call
gciAI_UpdateThreat = 1000

--AI Animation Lookups. Not used by RoP, but some mods use this.
gsAIAnimationScripts = {}
gciAIAnimUpdate_Initialize = 0
gciAIAnimUpdate_Update = 1
gciAIAnimUpdate_Render = 2

--KOs needed to show stats. These are the default values, can be overridden.
gciAI_KOsForStats = 5
gciAI_KOsForResists = 10

--AI Stun Handler. The 0th slot is the Stunned ability slot.
gciAI_StunAbilitySlot = 0
gciAI_AmbushAbilitySlot = 1
gciAI_AbilityRollStart = 2

--Stun Constants
gciStun_CapMaxFactor = 2
gciStun_ResistMax = 3
gciStun_ResistTurnDecrement = 3
gciStun_DecrementCapDivisor = 5
gciStun_ResistLo = 0
gciStun_ResistHi = 3
gciStun_ResistApply = {}
gciStun_ResistApply[0] = 1.00
gciStun_ResistApply[1] = 0.75
gciStun_ResistApply[2] = 0.40
gciStun_ResistApply[3] = 0.10

--Job Script Execution
gciJob_Create = 0
gciJob_SwitchTo = 1
gciJob_Level = 2
gciJob_Master = 3
gciJob_JobAssembleSkillList = 4
gciJob_BeginCombat = 5
gciJob_BeginTurn = 6
gciJob_BeginAction = 7
gciJob_BeginFreeAction = 8
gciJob_EndAction = 9
gciJob_EndTurn = 10
gciJob_EndCombat = 11
gciJob_EventQueued = 12

--Query Script Code
gciQuery_Script_Code = 1000

-- |[Ability Enumerations]|
--Free Action cases
gbIsFreeAction = true
gbIsNotFreeAction = false

--Flags indicating CP/MP/Etc costs that are zero
giNoCPCost = 0
giNoMPCost = 0
giNoCooldown = 0
giNoCPGeneration = 0

--Buff flags
gbIsBuff = true
gbIsDebuff = false

--Always-available Flags
gbIsAlwaysAvailable = true
gbIsNotAlwaysAvailable = false

--Chaser Flags
gbConsumeEffects = true
gbDoNotConsumeEffects = false

-- |[Dialogue and Portrait Enumerations]|
gbNormalWidth = true
gbIsWide = false

-- |[Ability Constants]|
--Ability Script Execution
gciAbility_Create = 0
gciAbility_AssumeJob = 1
gciAbility_BeginCombat = 2
gciAbility_BeginTurn = 3
gciAbility_BeginAction = 4
gciAbility_BeginFreeAction = 5
gciAbility_PostAction = 6
gciAbility_PaintTargets = 7
gciAbility_PaintTargetsResponse = 8
gciAbility_Execute = 9
gciAbility_TurnEnds = 10
gciAbility_CombatEnds = 11
gciAbility_CreateSpecial = 12
gciAbility_GUIApplyEffect = 13
gciAbility_EventQueued = 14
gciAbility_BuildPredictionBox = 15
gciAbility_QueryCanRun = 16
gciAbility_UIPurchased = 17
gciAbility_UIRecompute = 18

gciAbility_SpecialStart = 1000
gciAbility_SpecialClearEffects = 1001
gciAbility_SpecialLastExec = 1002
gciAbility_UseOnUI = 2000
gciAbility_SkillUIPopulate = 2001

--Standard Codes
gciAbility_ResponseStandard_DirectAction = 0
gciAbility_ResponseStandard_Passive = 1
gciAbility_ResponseStandard_Counterattack = 2
gciAbility_ResponseStandard_Ambush = 3

--Size of Memorized Block
gciAbility_Memorized_Block_Wid = 5
gciAbility_Memorized_Block_Hei = 3

--Ability Description Y Offsets
gcfAbilityImgOffsetY = -1.0
gcfAbilityImgOffsetLgY = 6.0
gcfAbilityImgOffsetPredY = 2.0

--Ability Timer Offset When Striking Many Targets
gciAbility_TicksOffsetPerTarget = 15

--Black-Flash Ticks
gciAbility_BlackFlashTicks = 15

--JP Costs
gciJP_Cost_Locked = -1
gciJP_Cost_NoCost = 0
gciJP_Cost_Cheap = 50
gciJP_Cost_Normal = 100
gciJP_Cost_Advanced = 150
gciJP_Cost_Passive = 300
gciJP_Cost_Unique = 500

-- |[Ability Layout]|
--Ability Grid Total Size
gciAbilityGrid_XSize = 17
gciAbilityGrid_YSize = 3

--Class Grid
gciAbility_Class_X0 = 0
gciAbility_Class_X1 = 2
gciAbility_Class_Y0 = 0
gciAbility_Class_Y1 = 2

--Memorized Grid
gciAbility_Saveblock_X1 = 3
gciAbility_Saveblock_X2 = 7
gciAbility_Saveblock_Y1 = 0
gciAbility_Saveblock_Y2 = 2

--Tactics Grid
gciAbility_Tactics_X = 8
gciAbility_Tactics_Y = 0

--Standard Positions for Tactics Grid
gciAbility_Tactics_Items_Std_X = 8
gciAbility_Tactics_Items_Std_Y = 0
gciAbility_Tactics_FormStartX = 10
gciAbility_Tactics_FormStartY = 0
gciAbility_Tactics_WeaponSwapA_X = 8
gciAbility_Tactics_WeaponSwapA_Y = 1
gciAbility_Tactics_WeaponSwapB_X = 9
gciAbility_Tactics_WeaponSwapB_Y = 1
gciAbility_Tactics_Retreat_X = 8
gciAbility_Tactics_Retreat_Y = 2
gciAbility_Tactics_Surrender_X = 9
gciAbility_Tactics_Surrender_Y = 2

--Expanded items positions (overrides weapon swap slots by default)
gciaAbility_Tactics_Items_X = {gciAbility_Tactics_Items_Std_X, gciAbility_Tactics_Items_Std_X + 1, gciAbility_Tactics_Items_Std_X,     gciAbility_Tactics_Items_Std_X + 1}
gciaAbility_Tactics_Items_Y = {gciAbility_Tactics_Items_Std_Y, gciAbility_Tactics_Items_Std_Y,     gciAbility_Tactics_Items_Std_Y + 1, gciAbility_Tactics_Items_Std_Y + 1}

-- |[ =============================== Effect Constants =============================== ]|
-- |[Effect Enumerations]|
gcbBuildShortText = true

-- |[Update Type Constants]|
gciEffect_Create = 0
gciEffect_ApplyStats = 1
gciEffect_UnApplyStats = 2
gciEffect_BeginTurn = 3
gciEffect_BeginAction = 4
gciEffect_BeginFreeAction = 5
gciEffect_PostAction = 6
gciEffect_TurnEnds = 7
gciEffect_CombatEnds = 8

--DoTs only:
giEffect_LastComputedDotDamage = 0 --Gets populated with the damage, if applicable.
gciEffect_DotComputeDamage = 100

--Stat Mod Standard: Description Handlers
gciEffect_StatMod_ApplyStats = 1001
gciEffect_StatMod_UnapplyStats = 1002
gciEffect_StatMod_SetShortText = 1003
gciEffect_StatMod_SetInspectorTitle = 1004
gciEffect_StatMod_SetDescription = 1005

--ThreatVs. Standard
gciEffect_ThreatVs_SetValues = 1006

--Converts gciEffect_ApplyStats to gciEffect_StatMod_ApplyStats
gciEffect_StatMod_ConvertToSubroutine = gciEffect_StatMod_ApplyStats - gciEffect_ApplyStats

--Constants for spawning effects when a target is attacked.
gciEffect_OnApply_Friendly = 2000
gciEffect_OnApply_Hostile = 2001

-- |[Example Statmod Structure]|
--Structure used by the StatMod script.
gzaStatModStruct = {}
gzaStatModStruct.sDisplayName = "Null"
gzaStatModStruct.iDuration    = 0 --If -1, effect never expires.
gzaStatModStruct.sBacking     = "Null" --One of the gsAbility_Backing_Buff series
gzaStatModStruct.sFrame       = "Null" --One of the gsAbility_Frame_Active
gzaStatModStruct.bRemoveOnKO  = true --Effect is removed if the target is KO'd
gzaStatModStruct.sFrontImg    = "Null"
gzaStatModStruct.sStatString  = "Null"
gzaStatModStruct.saStrings    = {}
gzaStatModStruct.sShortText   = "Null"

-- |[ ======================== Combat Prototypes And Globals ========================= ]|
--Ability prototypes. Combat entities store prototypes of their abilities in these
-- tables so the abilities don't need to be re-assembled whenever they are called.
gzPrototypes = {}
gzPrototypes.Combat = {}
gzPrototypes.Combat.Allies = {}
gzPrototypes.Combat.Allies.BeeBuddy = {}
gzPrototypes.Combat.Enemies = {}
gzPrototypes.Combat.Enemies.Ch0 = {}
gzPrototypes.Combat.Enemies.Ch1 = {}
gzPrototypes.Combat.Enemies.Ch2 = {}
gzPrototypes.Combat.Enemies.Ch3 = {}
gzPrototypes.Combat.Enemies.Ch4 = {}
gzPrototypes.Combat.Enemies.Ch5 = {}
gzPrototypes.Combat.Enemies.Ch6 = {}
gzPrototypes.Combat.Standard = {}
gzPrototypes.Combat.Items = {}

--Combat Class Constructors
LM_ExecuteScript(gsRoot .. "Combat/Classes/ZClassRouting.lua")

--Build AI Scripts
gbHasBuiltAIStringsCh1 = nil
LM_ExecuteScript(gsRoot .. "Combat/AIs/Chapter 1/ZRouting.lua")

-- |[ ============================= Not Effect Constants ============================= ]|
--Get around to sorting these.

-- |[Whirlpools]|
--Helpful constants that align jumping into whirlpools.
gcfWhirl2x2OffX = 0.75
gcfWhirl2x2OffY = 0.50
gcfWhirl3x2OffX = 1.25
gcfWhirl3x2OffY = 0.50

-- |[Application Constants]|
gciApplication_TextTicks = 15
gciApplication_EventPaddingTicks = 5
gciApplication_FlashWhiteTicks = 15
gciApplication_FlashWhiteHold = 15

--Combat Tick Timers
gciCombatTicks_StandardActionMinimum = 10
gciCombatTicks_StandardTitleLen = 120

-- |[Character Constants]|
gciCombatEntity_Create = 0
gciCombatEntity_BeginCombat = 1
gciCombatEntity_BeginTurn = 2
gciCombatEntity_BeginAction = 3
gciCombatEntity_BeginFreeAction = 4
gciCombatEntity_EndAction = 5
gciCombatEntity_EndTurn = 6
gciCombatEntity_EndCombat = 7
gciCombatEntity_EventQueued = 8
gciCombatEntity_AddedToActive = 9
gciCombatEntity_RemovedFromActive = 10

-- |[Threat Handling]|
gcfThreat_DamageMe = 1.0
gcfThreat_DamageAlly = 0.25
gcfThreat_Heal = 0.25
gcfThreat_EffectMe = 0.40
gcfThreat_EffectAlly = 0.10

-- |[Combat Animations]|
--Global array of timings.
giAnimationTimingsTotal = 0
gzaAnimationTimings = {}

--Defaults for ineligible animations/sounds.
gsDefaultAttackAnimation = "Sword Slash"
gsDefaultAttackSound = "Combat\\|Impact_Slash"
gsDefaultCriticalSound = "Combat\\|Impact_Slash_Crit"

--Animation Position Offsets
gfaAnimOffsets = {}
gfaAnimOffsets[1] = {}
gfaAnimOffsets[1].fX = 100.0
gfaAnimOffsets[1].fY = 50.0
gfaAnimOffsets[1].iTiming = 15
gfaAnimOffsets[2] = {}
gfaAnimOffsets[2].fX = -100.0
gfaAnimOffsets[2].fY =  50.0
gfaAnimOffsets[2].iTiming = 30
gfaAnimOffsets[3] = {}
gfaAnimOffsets[3].fX = 0.0
gfaAnimOffsets[3].fY = 120.0
gfaAnimOffsets[3].iTiming = 45
gfaAnimOffsets[4] = {}
gfaAnimOffsets[4].fX = 0.0
gfaAnimOffsets[4].fY = 0.0
gfaAnimOffsets[4].iTiming = 60

--Offsets.
giAnimationMissOffset = 10

--Jump Speed
gcfStdJumpSpeed = -3.5
gcfStdGravity = 0.675

-- |[Puzzle Fight Minigame]|
--Game State Codes
gciPuzzleFightState_PlayersTurn_Init = 0
gciPuzzleFightState_PlayersTurn_Run = 1
gciPuzzleFightState_PlayersTurn_Acting = 2
gciPuzzleFightState_EnemyTurn_Acting = 3

--Update Codes
gciPuzzleFight_Update_BeginTurn = 0
gciPuzzleFight_Update_SpawnedObjectsMoved = 1
gciPuzzleFight_Update_Moved = 2
gciPuzzleFight_Update_ConfirmActionComplete = 3
gciPuzzleFight_Update_EnemyTurn = 4

--Object Codes
gciPuzzleFight_Object_Empty = -1
gciPuzzleFight_Object_ArrowUp = 0
gciPuzzleFight_Object_ArrowRt = 1
gciPuzzleFight_Object_ArrowDn = 2
gciPuzzleFight_Object_ArrowLf = 3
gciPuzzleFight_Object_Boost = 4
gciPuzzleFight_Object_Guard = 5
gciPuzzleFight_Object_Melee = 6
gciPuzzleFight_Object_Ranged = 7

--Note: Further Puzzle Fight variables are in chapter 2's variables file.

-- |[ =================================== Non-C++ Constants ==================================== ]|
--Constants that affect game balance. Can be edited.

-- |[Effect Power]|
--For each level, all effects used by party members get this bonus chance to apply.
gciEffectPowerPerLevel = 2

-- |[Effect Priority]|
gciEffectPriorityHot = 1
gciEffectPriorityNeutral = 0
gciEffectPriorityDot = -1

-- |[Item Classifications]|
--Used to indicate if an item is armor, weapon, etc when using charts.
gciWeaponCode = 1
gciArmorCode = 2
gciAccessoryCode = 3
gciItemCode = 4

--Special Codes:
gciSanyaAmmoCode = 10

-- |[DoT Description Flags]|
--Used for effect standardization of DoTs.
gciDoTEven = 0
gciDoTBackload = 1
gciDoTFrontload = 2

-- |[UI Standards]|
--UI Description Standards
gcs_UI_DescriptionStd_Weapon     = "[VALUE][TAB2][GEMSLOTS]\n[TYPE][TAB2][DAMAGETYPE]\n[STATS]"
gcs_UI_DescriptionStd_Armor      = "[VALUE][TAB2][GEMSLOTS]\n[TYPE]\n[STATS]"
gcs_UI_DescriptionStd_Accessory  = "[VALUE][TAB2][GEMSLOTS]\n[TYPE]\n[STATS]"
gcs_UI_DescriptionStd_CombatItem = "[VALUE][TAB2][GEMSLOTS]\n[TYPE]\n[STATS]"
gcs_UI_DescriptionStd_Gem        = "[VALUE]\n[TYPE]\n[STATS]"
gcs_UI_DescriptionStd_Misc       = "[VALUE]"

-- |[Topic Construction]|
--When building topics, these indicate what "level" the topic starts at.
gciAvailableAtStart = 1
gciMustBeUnlocked = -1

-- |[Vendor]|
gciNoPriceOverride = -1
gciNoQuantityLimit = -1

-- |[Audio Callouts]|
gciPreAnimCallout = 0
gciPostAnimCallout = 1
gciPainCallout = 2
giAdjustActionTimerForCallout = 0

--Abstract Volume Constants. Note these are declared in Audio/Classes/VoiceData/VoiceData.lua
gciAbstractVol_Izana      = 0
gciAbstractVol_Caelyn     = 1
gciAbstractVol_Cyrano     = 2
gciAbstractVol_Mei        = 3
gciAbstractVol_Florentina = 4
gciAbstractVol_Christine  = 5
gciAbstractVol_Tiffany    = 6
gciAbstractVol_SX399      = 7
gciAbstractVol_JX101      = 8

--Play chances. Represented by the option "Voice Line Play Chance", where the integer value of that
-- option selects from this array to get the chance to play factor.
gcaiChanceToPlayArray = {}
gcaiChanceToPlayArray[0] = 1.000
gcaiChanceToPlayArray[1] = 0.660
gcaiChanceToPlayArray[2] = 0.500
gcaiChanceToPlayArray[3] = 0.330
gcaiChanceToPlayArray[4] = 0.250
gcaiChanceToPlayArray[5] = 0.125
gcaiChanceToPlayArray[6] = 0.000
gcaiChanceToPlayArray[7] = 2.000

-- |[ ==================================== Lua Enumerations ==================================== ]|
--These are meant to make Lua function calls a little easier to read.
gcbHideParallelTiming = false
gcbShowParallelTiming = true
gciParallelScatterStdLo = 0
gciParallelScatterStdHi = 1800

-- |[ ================================= Delayed Loading Lists ================================== ]|
--Creates a new delayed load listing. When loading bitmaps, you can optionally have the engine
-- store which file they were in and their name, and load the actual data later on-demand.
--Because similar images often load together in groups, we create named sets of these images.
-- With a single function we can order all of these images to load at once.
--These global variables store the lists. See GameDir/Subroutines/Loading Functions/ for related 
-- lua functions.
if(gzaDelayedLoadList == nil) then
    gzaDelayedLoadList = {}
end

--If this flag is true, all bitmaps load their assets immediately. Disables asset streaming.
-- Can be modified from the game options.
gbAlwaysLoadImmediately = not OM_GetOption("Allow Asset Streaming")

--Error suppress flags. If asset streaming is disabled, then always suppress these errors as the 
-- lists will not be built on subsequent re-runs. This is because the lists are built when the assets
-- are loaded, and if not streaming, the assets are assumed to be loaded to save time.
gbSuppressNoListErrors = not OM_GetOption("Allow Asset Streaming")

--If true, the dialogue portraits and combat portraits get split into their own streaming lists.
-- When dialogue closes, the portraits will unload to save memory, and re-load when dialogue
-- opens again.
gbUnloadDialoguePortraitsWhenDialogueCloses = OM_GetOption("Actors Unload After Dialogue")
WD_SetProperty("Set Unload On Close Flag", gbUnloadDialoguePortraitsWhenDialogueCloses)

--C++ Constants
gciDelayedLoadDontCare = 0
gciDelayedLoadLoadImmediately = 1
gciDelayedLoadLoadAtEndOfTick = 2

-- |[ ================================== Tag Standardization =================================== ]|
-- |[Tag Tables]|
--This table describes which tags modify damage for various types. These are a factor that affects attack
-- power but not application chance. This allows a detaching of resistance from damage.
gzaTagTable = {}
local saNames = {"Slash", "Strike", "Pierce", "Flame", "Freeze", "Shock", "Holy", "Shadow", "Bleed", "Poison", "Corrode", "Terrify"}
for i = gciDamageType_Slashing, gciDamageType_Terrifying, 1 do
    gzaTagTable[i] = {}
    gzaTagTable[i].saAtkAdd = {saNames[i+1] .. " Damage Dealt +"}
    gzaTagTable[i].saAtkSub = {saNames[i+1] .. " Damage Dealt -"}
    gzaTagTable[i].saDefAdd = {saNames[i+1] .. " Damage Taken +"}
    gzaTagTable[i].saDefSub = {saNames[i+1] .. " Damage Taken -"}
end

-- |[ ==================================== Field Abilities ===================================== ]|
--Switch Types
gciFieldAbility_Create = 0
gciFieldAbility_Run = 1
gciFieldAbility_RunWhileCooling = 2
gciFieldAbility_Cancel = 3
gciFieldAbility_TouchEnemy = 4
gciFieldAbility_ModifyActor = 5

--Combat Codes
gciFieldAbility_Response_BeginCombat = 0
gciFieldAbility_Response_BeginTurn = 1
gciFieldAbility_Response_BeginAction = 2

--Universal Codes
gbFieldAbilityHandledInput = false
gciFieldAbility_Activate_Null = 0
gciFieldAbility_Activate_Florentina_PickLock = 1
gciFieldAbility_Activate_Reset = 2
gciFieldAbility_Confirm_Reset = 1000
gciFieldAbility_Cancel_Reset = 2000
gciFieldAbility_Activate_ExtraExtra = 3

--Special Level Types
gciLevel_SpecialType_Chest = 1
gciLevel_SpecialType_FakeChest = 2
gciLevel_SpecialType_Door = 3
gciLevel_SpecialType_Exit = 4
gciLevel_SpecialType_Inflection = 5
gciLevel_SpecialType_Climbable = 6
gciLevel_SpecialType_Location = 7
gciLevel_SpecialType_Position = 8
gciLevel_SpecialType_InvisZone = 9
gciLevel_SpecialType_Actor = 10

--Slots.
gciFieldAbility_Slots = 5

--Field ability storage.
DL_AddPath("Root/Variables/FieldAbilities/All/")
VM_SetVar("Root/Variables/FieldAbilities/All/fBallLightningHitX", "N", 0.0)
VM_SetVar("Root/Variables/FieldAbilities/All/fBallLightningHitY", "N", 0.0)

-- |[ =================================== Stat Icon Markdown =================================== ]|
gsaStatIcons = {}
gsaStatIcons[1]  = {"[Buff]",    "Root/Images/AdventureUI/DebuffIcons/Buff"}
gsaStatIcons[2]  = {"[Debuff]",  "Root/Images/AdventureUI/DebuffIcons/Debuff"}
gsaStatIcons[3]  = {"[Atk]",     "Root/Images/AdventureUI/StatisticIcons/Attack"}
gsaStatIcons[4]  = {"[Prt]",     "Root/Images/AdventureUI/StatisticIcons/Protection"}
gsaStatIcons[5]  = {"[Hlt]",     "Root/Images/AdventureUI/StatisticIcons/Health"}
gsaStatIcons[6]  = {"[Acc]",     "Root/Images/AdventureUI/StatisticIcons/Accuracy"}
gsaStatIcons[7]  = {"[Evd]",     "Root/Images/AdventureUI/StatisticIcons/Evade"}
gsaStatIcons[8]  = {"[Ini]",     "Root/Images/AdventureUI/StatisticIcons/Initiative"}
gsaStatIcons[9]  = {"[Man]",     "Root/Images/AdventureUI/StatisticIcons/Mana"}
gsaStatIcons[10] = {"[Blnd]",    "Root/Images/AdventureUI/StatisticIcons/Blind"}
gsaStatIcons[11] = {"[Bld]",     "Root/Images/AdventureUI/DamageTypeIcons/Bleeding"}
gsaStatIcons[12] = {"[Psn]",     "Root/Images/AdventureUI/DamageTypeIcons/Poisoning"}
gsaStatIcons[13] = {"[Crd]",     "Root/Images/AdventureUI/DamageTypeIcons/Corroding"}
gsaStatIcons[14] = {"[Shld]",    "Root/Images/AdventureUI/DamageTypeIcons/Shield"}
gsaStatIcons[15] = {"[Adrn]",    "Root/Images/AdventureUI/StatisticIcons/Adrenaline"}
gsaStatIcons[16] = {"[Cmbp]",    "Root/Images/AdventureUI/StatisticIcons/ComboPoint"}
gsaStatIcons[17] = {"[Stun]",    "Root/Images/AdventureUI/StatisticIcons/Stun"}
gsaStatIcons[18] = {"[Turn]",    "Root/Images/AdventureUI/DebuffIcons/Clock"}
gsaStatIcons[19] = {"[Turns]",   "Root/Images/AdventureUI/DebuffIcons/Clock"}
gsaStatIcons[20] = {"[Clock]",   "Root/Images/AdventureUI/DebuffIcons/Clock"}
gsaStatIcons[21] = {"[Slsh]",    "Root/Images/AdventureUI/DamageTypeIcons/Slashing"}
gsaStatIcons[22] = {"[Prc]",     "Root/Images/AdventureUI/DamageTypeIcons/Piercing"}
gsaStatIcons[23] = {"[Stk]",     "Root/Images/AdventureUI/DamageTypeIcons/Striking"}
gsaStatIcons[24] = {"[Flm]",     "Root/Images/AdventureUI/DamageTypeIcons/Flaming"}
gsaStatIcons[25] = {"[Frz]",     "Root/Images/AdventureUI/DamageTypeIcons/Freezing"}
gsaStatIcons[26] = {"[Shk]",     "Root/Images/AdventureUI/DamageTypeIcons/Shocking"}
gsaStatIcons[27] = {"[Cru]",     "Root/Images/AdventureUI/DamageTypeIcons/Crusading"}
gsaStatIcons[28] = {"[Obs]",     "Root/Images/AdventureUI/DamageTypeIcons/Obscuring"}
gsaStatIcons[29] = {"[Tfy]",     "Root/Images/AdventureUI/DamageTypeIcons/Terrifying"}
gsaStatIcons[30] = {"[UpN]",     "Root/Images/AdventureUI/StatisticIcons/NrUp"}
gsaStatIcons[31] = {"[DnN]",     "Root/Images/AdventureUI/StatisticIcons/NrDn"}
gsaStatIcons[32] = {"[Str0]",    "Root/Images/AdventureUI/StrengthIndicator/Str0"}
gsaStatIcons[33] = {"[Str1]",    "Root/Images/AdventureUI/StrengthIndicator/Str1"}
gsaStatIcons[34] = {"[Str2]",    "Root/Images/AdventureUI/StrengthIndicator/Str2"}
gsaStatIcons[35] = {"[Str3]",    "Root/Images/AdventureUI/StrengthIndicator/Str3"}
gsaStatIcons[36] = {"[Str4]",    "Root/Images/AdventureUI/StrengthIndicator/Str4"}
gsaStatIcons[37] = {"[Str5]",    "Root/Images/AdventureUI/StrengthIndicator/Str5"}
gsaStatIcons[38] = {"[Str6]",    "Root/Images/AdventureUI/StrengthIndicator/Str6"}
gsaStatIcons[39] = {"[Str7]",    "Root/Images/AdventureUI/StrengthIndicator/Str7"}
gsaStatIcons[40] = {"[Str8]",    "Root/Images/AdventureUI/StrengthIndicator/Str8"}
gsaStatIcons[41] = {"[Str9]",    "Root/Images/AdventureUI/StrengthIndicator/Str9"}
gsaStatIcons[42] = {"[CPIco]",   "Root/Images/AdventureUI/StatisticIcons/ComboPoint"}
gsaStatIcons[43] = {"[MPIco]",   "Root/Images/AdventureUI/StatisticIcons/Mana"}
gsaStatIcons[44] = {"[HEffect]", "Root/Images/AdventureUI/EffectIndicator/EffectHostile"}
gsaStatIcons[45] = {"[FEffect]", "Root/Images/AdventureUI/EffectIndicator/EffectFriendly"}
gsaStatIcons[46] = {"[Threat]",  "Root/Images/AdventureUI/StatisticIcons/Threat"}
gsaStatIcons[47] = {"[MPT]",     "Root/Images/AdventureUI/DamageTypeIconsLg/ManaPerTurn"}
gsaStatIcons[48] = {"[MPReg]",   "Root/Images/AdventureUI/DamageTypeIconsLg/ManaPerTurn"}
giStatIconsTotal = 48

--Large Stat Icons. Identical, but uses the larger versions where applicable.
gsaStatIconsLg = {}
gsaStatIconsLg[1]  = {"[Buff]",    "Root/Images/AdventureUI/DamageTypeIconsLg/Buff"}
gsaStatIconsLg[2]  = {"[Debuff]",  "Root/Images/AdventureUI/DamageTypeIconsLg/Debuff"}
gsaStatIconsLg[3]  = {"[Atk]",     "Root/Images/AdventureUI/DamageTypeIconsLg/Attack"}
gsaStatIconsLg[4]  = {"[Prt]",     "Root/Images/AdventureUI/DamageTypeIconsLg/Protection"}
gsaStatIconsLg[5]  = {"[Hlt]",     "Root/Images/AdventureUI/DamageTypeIconsLg/Health"}
gsaStatIconsLg[6]  = {"[Acc]",     "Root/Images/AdventureUI/DamageTypeIconsLg/Accuracy"}
gsaStatIconsLg[7]  = {"[Evd]",     "Root/Images/AdventureUI/DamageTypeIconsLg/Evade"}
gsaStatIconsLg[8]  = {"[Ini]",     "Root/Images/AdventureUI/DamageTypeIconsLg/Initiative"}
gsaStatIconsLg[9]  = {"[Man]",     "Root/Images/AdventureUI/DamageTypeIconsLg/Mana"}
gsaStatIconsLg[10] = {"[Blnd]",    "Root/Images/AdventureUI/DamageTypeIconsLg/Blind"}
gsaStatIconsLg[11] = {"[Bld]",     "Root/Images/AdventureUI/DamageTypeIconsLg/Bleeding"}
gsaStatIconsLg[12] = {"[Psn]",     "Root/Images/AdventureUI/DamageTypeIconsLg/Poisoning"}
gsaStatIconsLg[13] = {"[Crd]",     "Root/Images/AdventureUI/DamageTypeIconsLg/Corroding"}
gsaStatIconsLg[14] = {"[Shld]",    "Root/Images/AdventureUI/DamageTypeIconsLg/Shield"}
gsaStatIconsLg[15] = {"[Adrn]",    "Root/Images/AdventureUI/DamageTypeIconsLg/Adrenaline"}
gsaStatIconsLg[16] = {"[Cmbp]",    "Root/Images/AdventureUI/DamageTypeIconsLg/CmbPnt"}
gsaStatIconsLg[17] = {"[Stun]",    "Root/Images/AdventureUI/DamageTypeIconsLg/Stun"}
gsaStatIconsLg[18] = {"[Turn]",    "Root/Images/AdventureUI/DamageTypeIconsLg/Clock"}
gsaStatIconsLg[19] = {"[Turns]",   "Root/Images/AdventureUI/DamageTypeIconsLg/Clock"}
gsaStatIconsLg[20] = {"[Clock]",   "Root/Images/AdventureUI/DamageTypeIconsLg/Clock"}
gsaStatIconsLg[21] = {"[Slsh]",    "Root/Images/AdventureUI/DamageTypeIconsLg/Slashing"}
gsaStatIconsLg[22] = {"[Prc]",     "Root/Images/AdventureUI/DamageTypeIconsLg/Piercing"}
gsaStatIconsLg[23] = {"[Stk]",     "Root/Images/AdventureUI/DamageTypeIconsLg/Striking"}
gsaStatIconsLg[24] = {"[Flm]",     "Root/Images/AdventureUI/DamageTypeIconsLg/Flaming"}
gsaStatIconsLg[25] = {"[Frz]",     "Root/Images/AdventureUI/DamageTypeIconsLg/Freezing"}
gsaStatIconsLg[26] = {"[Shk]",     "Root/Images/AdventureUI/DamageTypeIconsLg/Shocking"}
gsaStatIconsLg[27] = {"[Cru]",     "Root/Images/AdventureUI/DamageTypeIconsLg/Crusading"}
gsaStatIconsLg[28] = {"[Obs]",     "Root/Images/AdventureUI/DamageTypeIconsLg/Obscuring"}
gsaStatIconsLg[29] = {"[Tfy]",     "Root/Images/AdventureUI/DamageTypeIconsLg/Terrifying"}
gsaStatIconsLg[30] = {"[UpN]",     "Root/Images/AdventureUI/DamageTypeIconsLg/Buff"}
gsaStatIconsLg[31] = {"[DnN]",     "Root/Images/AdventureUI/DamageTypeIconsLg/Debuff"}
gsaStatIconsLg[32] = {"[Str0]",    "Root/Images/AdventureUI/DamageTypeIconsLg/Str0"}
gsaStatIconsLg[33] = {"[Str1]",    "Root/Images/AdventureUI/DamageTypeIconsLg/Str1"}
gsaStatIconsLg[34] = {"[Str2]",    "Root/Images/AdventureUI/DamageTypeIconsLg/Str2"}
gsaStatIconsLg[35] = {"[Str3]",    "Root/Images/AdventureUI/DamageTypeIconsLg/Str3"}
gsaStatIconsLg[36] = {"[Str4]",    "Root/Images/AdventureUI/DamageTypeIconsLg/Str4"}
gsaStatIconsLg[37] = {"[Str5]",    "Root/Images/AdventureUI/DamageTypeIconsLg/Str5"}
gsaStatIconsLg[38] = {"[Str6]",    "Root/Images/AdventureUI/DamageTypeIconsLg/Str6"}
gsaStatIconsLg[39] = {"[Str7]",    "Root/Images/AdventureUI/DamageTypeIconsLg/Str7"}
gsaStatIconsLg[40] = {"[Str8]",    "Root/Images/AdventureUI/DamageTypeIconsLg/Str8"}
gsaStatIconsLg[41] = {"[Str9]",    "Root/Images/AdventureUI/DamageTypeIconsLg/Str9"}
gsaStatIconsLg[42] = {"[CPIco]",   "Root/Images/AdventureUI/DamageTypeIconsLg/CmbPnt"}
gsaStatIconsLg[43] = {"[MPIco]",   "Root/Images/AdventureUI/DamageTypeIconsLg/Mana"}
gsaStatIconsLg[44] = {"[HEffect]", "Root/Images/AdventureUI/DamageTypeIconsLg/EffectHostile"}
gsaStatIconsLg[45] = {"[FEffect]", "Root/Images/AdventureUI/DamageTypeIconsLg/EffectFriendly"}
gsaStatIconsLg[46] = {"[Threat]",  "Root/Images/AdventureUI/DamageTypeIconsLg/Threat"}
gsaStatIconsLg[47] = {"[MPT]",     "Root/Images/AdventureUI/DamageTypeIconsLg/ManaPerTurn"}
gsaStatIconsLg[48] = {"[MPReg]",   "Root/Images/AdventureUI/DamageTypeIconsLg/ManaPerTurn"}

-- |[Modifier Table]|
--Used when applying stats in gsStandardStatPath, maps strings to the variables they affect. 
-- Also maps the matching icons, if available.
gzaModTable = {}
gzaModTable[ 1] = {"HPMax",      gciStatIndex_HPMax,            "[Hlt]"}
gzaModTable[ 2] = {"MPMax",      gciStatIndex_MPMax,            "[Man]"}
gzaModTable[ 3] = {"MPReg",      gciStatIndex_MPRegen,          "[MPT]"}
gzaModTable[ 4] = {"CPMax",      gciStatIndex_CPMax,            "[Cmbp]"}
gzaModTable[ 5] = {"FAMax",      gciStatIndex_FreeActionMax,    "[Adrn]"}
gzaModTable[ 6] = {"FAGen",      gciStatIndex_FreeActionGen,    "[Adrn]"}
gzaModTable[ 7] = {"Atk",        gciStatIndex_Attack,           "[Atk]"}
gzaModTable[ 8] = {"Ini",        gciStatIndex_Initiative,       "[Ini]"}
gzaModTable[ 9] = {"Acc",        gciStatIndex_Accuracy,         "[Acc]"}
gzaModTable[10] = {"Evd",        gciStatIndex_Evade,            "[Evd]"}
gzaModTable[11] = {"Prt",        gciStatIndex_Protection,       "[Prt]"}
gzaModTable[12] = {"ResSls",     gciStatIndex_Resist_Slash,     "[Slsh]"}
gzaModTable[13] = {"ResStk",     gciStatIndex_Resist_Strike,    "[Stk]"}
gzaModTable[14] = {"ResPrc",     gciStatIndex_Resist_Pierce,    "[Prc]"}
gzaModTable[15] = {"ResFlm",     gciStatIndex_Resist_Flame,     "[Flm]"}
gzaModTable[16] = {"ResFrz",     gciStatIndex_Resist_Freeze,    "[Frz]"}
gzaModTable[17] = {"ResShk",     gciStatIndex_Resist_Shock,     "[Shk]"}
gzaModTable[18] = {"ResCru",     gciStatIndex_Resist_Crusade,   "[Cru]"}
gzaModTable[19] = {"ResObs",     gciStatIndex_Resist_Obscure,   "[Obs]"}
gzaModTable[20] = {"ResBld",     gciStatIndex_Resist_Bleed,     "[Bld]"}
gzaModTable[21] = {"ResPsn",     gciStatIndex_Resist_Poison,    "[Psn]"}
gzaModTable[22] = {"ResCrd",     gciStatIndex_Resist_Corrode,   "[Crd]"}
gzaModTable[23] = {"ResTfy",     gciStatIndex_Resist_Terrify,   "[Tfy]"}
gzaModTable[24] = {"StunCap",    gciStatIndex_StunCap,          "[Stun]"}
gzaModTable[25] = {"ThreatMult", gciStatIndex_ThreatMultiplier, "[Threat]"}
giModTableTotal = 25

-- |[Tag Table]|
--Used for gems and other things that use tags to communicate a damage bonus.
gzaTagModTable = {}
gzaTagModTable[ 1] = {"SlshDam+", "Slash Damage Dealt +"}
gzaTagModTable[ 2] = {"PrcDam+",  "Pierce Damage Dealt +"}
gzaTagModTable[ 3] = {"StkDam+",  "Strike Damage Dealt +"}
gzaTagModTable[ 4] = {"FlmDam+",  "Flame Damage Dealt +"}
gzaTagModTable[ 5] = {"FrzDam+",  "Freeze Damage Dealt +"}
gzaTagModTable[ 6] = {"ShkDam+",  "Shock Damage Dealt +"}
gzaTagModTable[ 7] = {"CruDam+",  "Holy Damage Dealt +"}
gzaTagModTable[ 8] = {"ObsDam+",  "Shadow Damage Dealt +"}
gzaTagModTable[ 9] = {"BldDam+",  "Bleed Damage Dealt +"}
gzaTagModTable[10] = {"PsnDam+",  "Poison Damage Dealt +"}
gzaTagModTable[11] = {"CrdDam+",  "Corrode Damage Dealt +"}
gzaTagModTable[12] = {"TfyDam+",  "Terrify Damage Dealt +"}
giTagModTableTotal = 12
    
-- |[ ===================================== Ability Paths ====================================== ]|
--Normal Action
gsAbility_Backing_Direct = "Root/Images/AdventureUI/Abilities/OctBackGrey"
gsAbility_Backing_DoT    = "Root/Images/AdventureUI/Abilities/OctBackRed"
gsAbility_Backing_Heal   = "Root/Images/AdventureUI/Abilities/OctBackGreen"
gsAbility_Backing_Buff   = "Root/Images/AdventureUI/Abilities/OctBackBlue"
gsAbility_Backing_Debuff = "Root/Images/AdventureUI/Abilities/OctBackPurple"

gsAbility_Frame_Active  = "Root/Images/AdventureUI/Abilities/OctFrameRed"
gsAbility_Frame_Passive = "Root/Images/AdventureUI/Abilities/OctFrameBlue"
gsAbility_Frame_Special = "Root/Images/AdventureUI/Abilities/OctFrameTeal"
gsAbility_Frame_Combo   = "Root/Images/AdventureUI/Abilities/OctFramePurp"

--Free Action
gsAbility_Backing_Free_Direct = "Root/Images/AdventureUI/Abilities/SqrBackGrey"
gsAbility_Backing_Free_DoT    = "Root/Images/AdventureUI/Abilities/SqrBackRed"
gsAbility_Backing_Free_Heal   = "Root/Images/AdventureUI/Abilities/SqrBackGreen"
gsAbility_Backing_Free_Buff   = "Root/Images/AdventureUI/Abilities/SqrBackBlue"
gsAbility_Backing_Free_Debuff = "Root/Images/AdventureUI/Abilities/SqrBackPurple"

gsAbility_Frame_Free_Active  = "Root/Images/AdventureUI/Abilities/SqrFrameRed"
gsAbility_Frame_Free_Passive = "Root/Images/AdventureUI/Abilities/SqrFrameBlue"
gsAbility_Frame_Free_Special = "Root/Images/AdventureUI/Abilities/SqrFrameTeal"
gsAbility_Frame_Free_Combo   = "Root/Images/AdventureUI/Abilities/SqrFramePurp"

-- |[ =================================== Damage Type Remaps =================================== ]|
--Gives each damage type a set of values associated with it between variables.
gzDamageTypes = {}
local function fnAddDamageType(piTypeIndex, psAnimation, psDotText, psDamageTypeIcon, psTypeTag)
    
    --Index.
    local i = piTypeIndex

    --Create.
    gzDamageTypes[i] = {}
    gzDamageTypes[i].sAnimation = psAnimation
    gzDamageTypes[i].sDotText = psDotText
    gzDamageTypes[i].sDamageTypeIcon = psDamageTypeIcon
    gzDamageTypes[i].sTypeTag = psTypeTag

end

--Create.
fnAddDamageType(gciDamageType_Slashing,   "Sword Slash", "Slashed!",     "Slashing",   "Slashing DoT")
fnAddDamageType(gciDamageType_Striking,   "Strike",      "Shuddering!",  "Striking",   "Striking DoT")
fnAddDamageType(gciDamageType_Piercing,   "Pierce",      "Pierced!",     "Piercing",   "Piercing DoT")
fnAddDamageType(gciDamageType_Flaming,    "Flames",      "Burned!",      "Flaming",    "Flaming DoT")
fnAddDamageType(gciDamageType_Freezing,   "Freeze",      "Frozen!",      "Freezing",   "Freezing DoT")
fnAddDamageType(gciDamageType_Shocking,   "Shock",       "Shocked!",     "Shocking",   "Shocking DoT")
fnAddDamageType(gciDamageType_Crusading,  "Light",       "Overwhelmed!", "Crusading",  "Crusading DoT")
fnAddDamageType(gciDamageType_Obscuring,  "Shadow A",    "Subdued!",     "Obscuring",  "Obscuring DoT")
fnAddDamageType(gciDamageType_Bleeding,   "Bleed",       "Bleeding!",    "Bleeding",   "Bleeding DoT")
fnAddDamageType(gciDamageType_Poisoning,  "Poison",      "Poisoned!",    "Poisoning",  "Poisoning DoT")
fnAddDamageType(gciDamageType_Corroding,  "Corrode",     "Corroding!",   "Corroding",  "Corroding DoT")
fnAddDamageType(gciDamageType_Terrifying, "Terrify",     "Terrified!",   "Terrifying", "Terrifying DoT")

-- |[ ================================= Rune Animation Counts ================================== ]|
gciMeiRuneFramesTotal = 60

-- |[ ======================================== Mugging ========================================= ]|
--Constants modifying how much XP/JP/Cash is given from mugging. Advisories, scripts decide what the
-- final values are.
gcfMugPlatinaRate = 0.20
gcfMugExperienceRate = 0.20
gcfMugJPRate = 0.20

-- |[ ================================== Equipment Constants =================================== ]|
--Damage Type Equipment Categories
gciEquipment_DamageType_Base = 0
gciEquipment_DamageType_Override = 1
gciEquipment_DamageType_Bonus = 2

-- |[Adamantite and Crafting]|
--Indices
gciCraft_Adamantite_Powder = 0
gciCraft_Adamantite_Flakes = 1
gciCraft_Adamantite_Shard = 2
gciCraft_Adamantite_Piece = 3
gciCraft_Adamantite_Chunk = 4
gciCraft_Adamantite_Ore = 5
gciCraft_Adamantite_Total = 6

--Clear the starting crafting values
for i = 0, gciCraft_Adamantite_Total-1, 1 do
	AdInv_SetProperty("Crafting Material", i, 0)
end

-- |[Catalyst Constants]|
--Codes
gciCatalyst_Health = 0
gciCatalyst_Attack = 1
gciCatalyst_Initiative = 2
gciCatalyst_Evade = 3
gciCatalyst_Accuracy = 4
gciCatalyst_Skill = 5

--Needed per boost
gciCatalyst_Health_Needed = 5
gciCatalyst_Attack_Needed = 3
gciCatalyst_Initiative_Needed = 3
gciCatalyst_Evade_Needed = 3
gciCatalyst_Accuracy_Needed = 3
gciCatalyst_Skill_Needed = 6

--Amount per boost
gciCatalyst_Health_Bonus = 10
gciCatalyst_Attack_Bonus = 3
gciCatalyst_Initiative_Bonus = 4
gciCatalyst_Evade_Bonus = 3
gciCatalyst_Accuracy_Bonus = 3

-- |[Gem Colors]|
gciGemColor_Grey   =   1
gciGemColor_Red    =   2
gciGemColor_Blue   =   4
gciGemColor_Orange =   8
gciGemColor_Violet =  16
gciGemColor_Yellow =  32
gciGemColor_All    = 255

gciSubgemMax = 5
gciGemSlotsMax = 6

-- |[ ===================================== Party Variables ==================================== ]|
-- |[Special Variables]|
--Variables that indicate the player's party or NPCs associated with it.
gsPartyLeaderName = "No Leader"
giPartyLeaderID = 0

--Level music to play when reverting dialogue.
gsLevelMusic = "Null"

-- |[ ======================================= DataLibrary ====================================== ]|
-- |[Setup]|
--The VariableManager needs certain paths set up for chests. Chests are "open" if a variable exists in the following pattern:
-- "Root/Variables/Chests/[ROOMNAME]/[CHESTNAME]"
--The roomname is the name of the folder containing the room, and the chestname is set in Tiled as the Name property.
DL_AddPath("Root/Variables/Chests/")
DL_AddPath("Root/Variables/Global/")
DL_AddPath("Root/Variables/System/Startup/")
DL_AddPath("Root/Variables/System/Special/")

-- |[Green Chests Globals]|
--This tracks how many green chests have been opened since the player last rested.
DL_AddPath("Root/Variables/Chests/Random/")
VM_SetVar("Root/Variables/Chests/Random/iTotalGreensOpened", "N", 0)

-- |[Enemy Spawn Globals]|
--This variable is used when spawning entities who are specially flagged. It gets reset when the player rests.
DL_AddPath("Root/Variables/Enemies/Random/")
VM_SetVar("Root/Variables/Enemies/Random/iRandomSpawnSeed", "N", 0)

-- |[Chapter Completion]|
--Variables indicating overall chapter progression
DL_AddPath("Root/Variables/Global/ChapterComplete/")
VM_SetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N", 0.0)
VM_SetVar("Root/Variables/Global/ChapterComplete/iChapter1", "N", 0.0)
VM_SetVar("Root/Variables/Global/ChapterComplete/iChapter2", "N", 0.0)
VM_SetVar("Root/Variables/Global/ChapterComplete/iChapter3", "N", 0.0)
VM_SetVar("Root/Variables/Global/ChapterComplete/iChapter4", "N", 0.0)
VM_SetVar("Root/Variables/Global/ChapterComplete/iChapter5", "N", 0.0)

-- |[Load Setting]|
--Load Setter. Gets set to 0.0 when loading the game, is 1.0 when starting a chapter normally.
DL_AddPath("Root/Variables/Global/LoadSetting/")
VM_SetVar("Root/Variables/Global/LoadSetting/iIsLoaded", "N", 0.0)

--Autosave Setter. Gets set to 1.0 for autosaves.
DL_AddPath("Root/Variables/Global/Autosave/")
VM_SetVar("Root/Variables/Global/Autosave/iBlockAutosave", "N", 0.0)
VM_SetVar("Root/Variables/Global/Autosave/sLastSavePoint", "S", "Null")
VM_SetVar("Root/Variables/Global/Autosave/iIsAutosave", "N", 0.0)
VM_SetVar("Root/Variables/Global/Autosave/iPlayerX", "N", 0.0)
VM_SetVar("Root/Variables/Global/Autosave/iPlayerY", "N", 0.0)
VM_SetVar("Root/Variables/Global/Autosave/iPlayerZ", "N", 0.0)

-- |[Storage]|
--Storage States. If the file does not exist, a derived game may have deleted it.
if(FS_Exists(gsRoot .. "Chapter 1/Cleanup/900 Variable Listing.lua") == true) then
	LM_ExecuteScript(gsRoot .. "Chapter 1/Cleanup/900 Variable Listing.lua")
end

-- |[Catalyst]|
--Catalyst Tone. Gets sets to true when the player finds the Platinum Compass.
DL_AddPath("Root/Variables/Global/CatalystTone/")
VM_SetVar("Root/Variables/Global/CatalystTone/iHasCatalystTone", "N", 0.0)

-- |[Audio Options]|
--Sound settings.
VM_SetVar("Root/Variables/System/Special/iHasSoundSettings", "N", 0.0)
VM_SetVar("Root/Variables/System/Special/fMusicVolume", "N", AudioManager_GetProperty("Music Volume"))
VM_SetVar("Root/Variables/System/Special/fSoundVolume", "N", AudioManager_GetProperty("Sound Volume"))

-- |[Engine Options]|
--Ambient Light Boost
VM_SetVar("Root/Variables/System/Special/fAmbientLightBoost", "N", 0.0)
AL_SetProperty("Ambient Light Boost", 0.0)

-- |[Difficulty Options]|
--Tourist mode. Can be enabled from the options menu to make the game much, much easier.
-- Useful for players who don't want challenging combat and just want to see the TF stuff. Psychos!
VM_SetVar("Root/Variables/System/Special/iIsTouristMode", "N", 0.0)
AdvCombat_SetProperty("Tourist Mode", false)

-- |[Combat Options]|
--Combat Options
VM_SetVar("Root/Variables/System/Special/iAutoUseDoctorBag", "N", 1.0)
VM_SetVar("Root/Variables/System/Special/iMemoryCursor", "N", 1.0)
VM_SetVar("Root/Variables/System/Special/iAllowAutoWinOnMug", "N", 1.0)
AdvCombat_SetProperty("Auto Doctor Bag", true)
AdvCombat_SetProperty("Memory Cursor", true)

-- |[Dialogue Options]|
--Dialogue Options
VM_SetVar("Root/Variables/System/Special/iAutoHastenDialogue", "N", 0.0)
WD_SetProperty("Set AutoHasten", false)

-- |[Catalyst Storage]|
--Catalysts. These are tracked independently of the chapter and apply across chapters. Note that these are read-only values. They are only used for saving/loading.
-- The only place they should be modified is the designated scripts (which are chest-open handlers).
DL_AddPath("Root/Variables/Global/Catalysts/")
VM_SetVar("Root/Variables/Global/Catalysts/iHealth", "N", 0.0)
VM_SetVar("Root/Variables/Global/Catalysts/iAttack", "N", 0.0)
VM_SetVar("Root/Variables/Global/Catalysts/iInitiative", "N", 0.0)
VM_SetVar("Root/Variables/Global/Catalysts/iEvade", "N", 0.0)
VM_SetVar("Root/Variables/Global/Catalysts/iAccuracy", "N", 0.0)
VM_SetVar("Root/Variables/Global/Catalysts/iSkill", "N", 0.0)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Health, 0)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Attack, 0)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Initiative, 0)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Evade, 0)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Accuracy, 0)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Skill, 0)

--Special string constants.
DL_AddPath("Root/Variables/Global/Constants/")
VM_SetVar("Root/Variables/Global/Constants/sNormal", "S", "Normal")

--Total number of Catalysts in Chapter 1:
AM_SetProperty("Total Catalysts", 24)

--Quarters Constants. These are the ID values for items in Christine's quarters.
gci_Quarters_Nothing = 0
gci_Quarters_DefragPod = 1
gci_Quarters_CounterL = 2
gci_Quarters_CounterM = 3
gci_Quarters_CounterR = 4
gci_Quarters_OilMaker = 5
gci_Quarters_TV = 6
gci_Quarters_ChairS = 7
gci_Quarters_CouchSL = 8
gci_Quarters_CouchSM = 9
gci_Quarters_CouchSR = 10
gci_Quarters_ChairN = 11
gci_Quarters_CouchNL = 12
gci_Quarters_CouchNM = 13
gci_Quarters_CouchNR = 14

--Face Table Size
gci_FaceTable_Size = 32

-- |[ ================================== Stat Profile Globals ================================== ]|
--Used for the stat profiler and for standardized job stat computations.
giaHPArray  = {}
giaAtkArray = {}
giaAccArray = {}
giaEvdArray = {}
giaIniArray = {}

-- |[Global Job Stack]|
gzJobCallStack = {}

-- |[ ================================ Cross-Chapter Variables ================================= ]|
--Some things persist between chapters, like catalysts and doctor bag upgrades. These are their
-- base values.

-- |[Doctor Bag]|
--Starts at 100% charge rate, 100 charges max.
DL_AddPath("Root/Variables/CrossChapter/DoctorBag/")
VM_SetVar( "Root/Variables/CrossChapter/DoctorBag/iDoctorBagChargeSizeUpgrade", "N", 0)
VM_SetVar( "Root/Variables/CrossChapter/DoctorBag/fDoctorBagChargeRateUpgrade", "N", 0)
VM_SetVar( "Root/Variables/CrossChapter/DoctorBag/fDoctorBagPotencyUpgrade", "N", 0)
VM_SetVar( "Root/Variables/CrossChapter/DoctorBag/iDoctorBagChargesMax", "N", 100)
VM_SetVar( "Root/Variables/CrossChapter/DoctorBag/iDoctorBagChargesCur", "N", 100)

--Paths and global variables used by the doctor bag.
gsComputeDoctorBagTotalPath = gsRoot .. "Subroutines/Combat/Compute Doctor Bag Total.lua"
giTotalDoctorBagChargeSize = 100
gfTotalDoctorBagChargeRate = 1.0
gfTotalDoctorBagPotency = 1.0

--If true, the gsComputeDoctorBagTotalPath routine auto-syncs the results with the inventory.
gbAutoSetDoctorBagProperties = true 

--If true, the doctor bag current charges will equal its max charges when using the above script. It
-- then toggles itself to false. This is used for rest actions.
gbAutoSetDoctorBagCurrentValues = false

--This must be synced with the UI.
AdInv_SetProperty("Doctor Bag Charges", 100)
AdInv_SetProperty("Doctor Bag Charges Max", 100)
AdInv_SetProperty("Doctor Bag Charge Factor", 1.0)
AdInv_SetProperty("Doctor Bag Potency", 1.0)

--When bypassing the intro, start with the Doctor Bag:
if(gbBypassIntro == true) then
	VM_SetVar("Root/Variables/System/Special/iDoctorBagCharges", "N", 100.0)
	VM_SetVar("Root/Variables/System/Special/iDoctorBagChargesMax", "N", 100.0)
	AdInv_SetProperty("Doctor Bag Charges Max", 100)
	AdInv_SetProperty("Doctor Bag Charges", 100)
end

-- |[JP Tally]|
--Used to track how much JP the player has gained, gets cleared whenever the base menu opens. If
-- it crosses 500, the skills icon renders differently.
DL_AddPath("Root/Variables/CrossChapter/SkillsTracker/")
VM_SetVar( "Root/Variables/CrossChapter/SkillsTracker/iJPTally", "N", 0)

-- |[EXP Block]|
--If this variable is true, the player is blocked from gaining XP. It can be toggled on and off with
-- a password, and chapter trainers also allow it.
DL_AddPath("Root/Variables/CrossChapter/EXPTracker/")
VM_SetVar( "Root/Variables/CrossChapter/EXPTracker/iBarEXPGain", "N", 0)
VM_SetVar( "Root/Variables/CrossChapter/EXPTracker/iAlwaysLevelZero", "N", 0)

-- |[ =================================== Sprite Variables ===================================== ]|
--For any character that appears in a table with variable forms (like the Transform campfire menu),
-- these variables track their sprite. They are key-value pairs to be searchable.
--These can be updated by transformations or costume changes.
gsCharSprites = {}
gsCharSprites[1] = {"Mei",        "Root/Images/Sprites/Mei_Human/SW|0"}
gsCharSprites[2] = {"Florentina", "Root/Images/Sprites/Florentina/SW|0"}
gsCharSprites[3] = {"Christine",  "Root/Images/Sprites/Christine_Human/SW|0"}
gsCharSprites[4] = {"Tiffany",    "Root/Images/Sprites/Tiffany/SW|0"}
gsCharSprites[5] = {"JX-101",     "Root/Images/Sprites/JX101/SW|0"}
gsCharSprites[6] = {"SX-399",     "Root/Images/Sprites/SX399Lord/SW|0"}
gsCharSprites[7] = {"Sophie",     "Root/Images/Sprites/Sophie/SW|0"}
giCharSpritesTotal = 7

--Global, prevents fnSpecialCharacter from running the costume handler.
gbDontRunCostumeHandler = false

--How many frames to load when using fnLoadCharacterGraphics. Default is TA_MOVE_IMG_STANDARD (4).
giMoveFrames = 4

-- |[Costume Listing]|
--Global costume list. See YCharacterAutoresolve.lua for more on how this is used.
--Table format:
--gzCostumeResolveList[i].sCharacterName        Name of the character. Ex: "Mei", "Florentina"
--gzCostumeResolveList[i].saJobListing          String list of jobs. Ex: {"Fencer", "Nightshade"}
--gzCostumeResolveList[i].saFormListing         String list of forms. Ex: {"Human", "Alraune"}
gzCostumeResolveList = {}

-- |[ ==================================== Follower Remaps ===================================== ]|
--Remapping structure that maps a character name to a combat name and following variable.
gzaFollowerRemaps = {}
gzaFollowerRemaps[1] = {"Florentina", "Florentina", "Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene"}
gzaFollowerRemaps[2] = {"Tiffany",    "Tiffany",    "Root/Variables/Chapter5/Scenes/iIs55Following"}
gzaFollowerRemaps[3] = {"SX-399",     "SX-399",     "Root/Variables/Chapter5/Scenes/iSX399IsFollowing"}
gzaFollowerRemaps[4] = {"JX-101",     "JX-101",     "Root/Variables/Chapter5/Scenes/iIsJX101Following"}
gzaFollowerRemaps[5] = {"Sophie",     "Null",       "Null"}

-- |[ ==================================== Combat Variables ==================================== ]|
--General Combat.
DL_AddPath("Root/Variables/Global/Combat/")

--KO-Tracker. Each chapter tracks how many of each enemy type has been KO'd, to show their stats.
VM_SetVar("Root/Variables/Global/Combat/sKOTrackerPath", "S", "Root/Variables/Chapter1/KOTracker/")

--Hunting tracker. Can reveal entities despite not defeating them. Used in chapter 2.
VM_SetVar("Root/Variables/Global/Combat/sHuntTrackerPath", "S", "Root/Variables/Chapter2/HuntTracker/")

--Combat Ability Statistics. Stores information about combat. The script in Root/Combat/Routines/Statistics/Handle Statistic Update.lua
-- makes extensive use of this format.
gzaCombatAbilityStatistics = {}
gzaCombatAbilityStatistics.iTurnsElapsed = 0
gzaCombatAbilityStatistics.zaStatisticsPacks = {}

--Update constants for combat statistics.
giCombatStats_Begin = 0
giCombatStats_NewTurn = 1
giCombatStats_NewAction = 2

-- |[Voice Line Script]|
--Global path, this gets called during an attack if it's non-nil.
gsGlobalVoicePath = gsRoot .. "Combat/Routines/Voice/Z Voice Routing.lua"

--Variable that tracks when voice lines played, causes them to not play *too* often.
giLastVoiceTimer = 0

-- |[ =================================== Function Builders ==================================== ]|
--Constants for below functions
gciMaxEffectDescriptionLines = 7

-- |[ ===================================== Loading Lists ====================================== ]|
--Flags that indicate which chapter's assets need to be loaded.
gbShouldLoadChapter1Assets = false
gbShouldLoadChapter5Assets = false

--Catalyst Tables.
LM_ExecuteScript(fnResolvePath() .. "201 Catalyst Tables.lua")

-- |[Other Files]|
--Lookup tables stored in other files.
LM_ExecuteScript(gsRoot .. "Lookups/Ability Standard Effect Tags.lua")
