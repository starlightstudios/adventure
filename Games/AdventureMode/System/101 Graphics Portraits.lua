-- |[ ================================== Graphics: Portraits =================================== ]|
--Portraits that are universal between either all chapters, or for the Nowhere between chapters.

-- |[Load List]|
--Put everything in here on a "System Portraits" loading list. These resolve when convenient.
-- The "Nowhere" portraits resolve immediately.
local sEverythingList = "System Portraits"
fnCreateDelayedLoadList(sEverythingList)

-- |[ =================================== Loading Subroutine =================================== ]|
--Used for the automated list sections.
gbExtractListDebug = false
fnExtractList = function(sPrefix, sSuffix, sDLPath, saList)
	
	--Arg check.
	if(sPrefix == nil) then return end
	if(sSuffix == nil) then return end
	if(sDLPath == nil) then return end
	if(saList  == nil) then return end
	
	--Run across the list and extract everything.
	local i = 1
	while(saList[i] ~= nil) do
		
		--If the name is "SKIP", ignore it. This means it will be filled in later.
		if(saList[i] == "SKIP") then
		
		--Extract the image.
        elseif(sCharacterPrefix == nil) then
        
            --Diagnostic text:
            if(gbExtractListDebug == true) then
                io.write("Extract: " .. sPrefix .. saList[i] .. sSuffix .. " to " .. sDLPath .. saList[i] .. "\n")
            end
        
            --Execute.
			fnExtractDelayedBitmapToList(sEverythingList, sPrefix .. saList[i] .. sSuffix, sDLPath .. saList[i])
		end
		
		--Next.
		i = i + 1
	end
end

-- |[ =================================== Dialogue Portraits =================================== ]|
-- |[Nowhere Portraits]|
--All six bearers, as well as Maram and Septima, always load. Only the human variants are used. All portraits
-- get delayed-load orders, but are ordered to load right now. They will be unloaded later.
--First, create the Nowhere grouping.
local sNowhereName = "Nowhere Portraits"
fnCreateDelayedLoadList(sNowhereName)

SLF_Open(gsaDatafilePaths.sMeiPath)
DL_AddPath("Root/Images/Portraits/MeiDialogue/")
DL_AddPath("Root/Images/Portraits/Combat/")
fnExtractDelayedBitmapToList(sNowhereName, "Por|MeiHuman|Neutral", "Root/Images/Portraits/MeiDialogue/HumanNeutral")
fnExtractDelayedBitmapToList(sNowhereName, "Party|Mei_Human",      "Root/Images/Portraits/Combat/Mei_Human")

SLF_Open(gsaDatafilePaths.sSanyaPath)
DL_AddPath("Root/Images/Portraits/SanyaDialogue/")
DL_AddPath("Root/Images/Portraits/Combat/")
fnExtractDelayedBitmapToList(sNowhereName, "Por|SanyaHumanNoRifle|Neutral", "Root/Images/Portraits/SanyaDialogue/HumanNoRifleNeutral")
fnExtractDelayedBitmapToList(sNowhereName, "Party|Sanya_Human", "Root/Images/Portraits/Combat/Sanya_Human")

SLF_Open(gsaDatafilePaths.sChristinePath)
DL_AddPath("Root/Images/Portraits/ChristineDialogue/")
fnExtractDelayedBitmapToList(sNowhereName, "Por|Christine|Neutral", "Root/Images/Portraits/ChristineDialogue/Neutral")
fnExtractDelayedBitmapToList(sNowhereName, "Por|Chris|Neutral",     "Root/Images/Portraits/ChristineDialogue/Male|Neutral")
fnExtractDelayedBitmapToList(sNowhereName, "Party|Christine_Human", "Root/Images/Portraits/Combat/Christine_Human")

--Rilmani.
SLF_Open(gsaDatafilePaths.sSeptimaPath)
DL_AddPath("Root/Images/Portraits/Septima/")
fnExtractDelayedBitmapToList(sNowhereName, "Por|Septima|Neutral",   "Root/Images/Portraits/Septima/Neutral")
fnExtractDelayedBitmapToList(sNowhereName, "Por|Septima|NeutralUp", "Root/Images/Portraits/Septima/NeutralUp")

SLF_Open(gsaDatafilePaths.sMaramPath)
DL_AddPath("Root/Images/Portraits/Maram/")
fnExtractDelayedBitmapToList(sNowhereName, "Por|Maram|Neutral", "Root/Images/Portraits/Maram/Neutral")

--Combat portraits for nowhere characters.
fnExtractDelayedBitmapToList(sNowhereName, "Party|Mei_Human",       "Root/Images/Portraits/Combat/Mei_Human")
fnExtractDelayedBitmapToList(sNowhereName, "Party|Sanya_Human",     "Root/Images/Portraits/Combat/Sanya_Human")
fnExtractDelayedBitmapToList(sNowhereName, "Party|Christine_Human", "Root/Images/Portraits/Combat/Christine_Human")

--Order this list to load immediately.
fnLoadDelayedBitmapsFromList(sNowhereName, gciDelayedLoadLoadImmediately)

-- |[ ========================================= Actors ========================================= ]|
--Runebearers always have a dialogue actor with human/neutral. This is for chapter selection.
fnCreateDialogueActor("Mei",       "Voice|Mei",       "Root/Images/Portraits/MeiDialogue/",       true, {"NOALIAS"}, {"Neutral"})
fnCreateDialogueActor("Sanya",     "Voice|Sanya",     "Root/Images/Portraits/SanyaDialogue/",     true, {"NOALIAS"}, {"HumanNoRifleNeutral"})
fnCreateDialogueActor("Christine", "Voice|Christine", "Root/Images/Portraits/ChristineDialogue/", true, {"NOALIAS"}, {"Neutral"})

--Rilmani. Same as the bearers.
fnCreateDialogueActor("Septima", "Voice|Septima", "Root/Images/Portraits/Septima/", true, {"Rilmani"}, {"Neutral", "NeutralUp"})
fnCreateDialogueActor("Maram",   "Voice|Maram",   "Root/Images/Portraits/Maram/",   true, {"Rilmani"}, {"Neutral"})

--Actor with no portraits, used to represent crowds.
fnCreateDialogueActor("Crowd", "Voice|Crowd", "Root/Images/Portraits/NPCs/HumanNPCF0", true, {"NOALIAS"}, {"NEUTRALISPATH"})

--All other dialogue actors are chapter-specific.

-- |[Saving Images Out]|
--Sample code.
--DL_SaveBitmapToDrive("Root/Images/Portraits/Maram/Neutral", "Maram.png")
