-- |[ ================================= Chapter Initialization ================================= ]|
--Called when the chapter is initialized. This can be either because it was loaded in the chapter select
-- or because the game was loaded with the chapter already active.
--If loading the game, the initial values created her are overwritten afterwards. These thus serve
-- as initial values. If a variable exists here but does not exist in the save file, the default
-- value is used.
local sBasePath = fnResolvePath()
gbInitializeDebug = false
Debug_PushPrint(gbInitializeDebug, "Beginning Chapter 2 Initialization.\n")

-- |[ ========================================= Loading ======================================== ]|
if(gbLoadedChapter2Assets ~= true and gbShouldLoadChapter2Assets == true) then

    --Set load handlers to chapter 2.
	if(gsMandateTitle ~= "Witch Hunter Izana") then
		LI_BootChapter2()
	end
    
    --Flag.
    Debug_Print(" Loading assets for chapter 2.\n")
    gbLoadedChapter2Assets = true
    
    --Load Sequence
    fnIssueLoadReset("AdventureModeCH2")
    local sDirectory = gsRoot .. "Chapter 2/Loading/"
    LM_ExecuteScript(sDirectory .. "100 Sprites.lua")
    LM_ExecuteScript(sDirectory .. "101 Portraits.lua")
    LM_ExecuteScript(sDirectory .. "102 Actors.lua")
    LM_ExecuteScript(sDirectory .. "103 Scenes.lua")
    LM_ExecuteScript(sDirectory .. "104 Overlays.lua")
    LM_ExecuteScript(sDirectory .. "105 Audio.lua")
    SLF_Close()
    fnCompleteLoadSequence()  
    Debug_Print(" Loading assets done.\n")  

end

-- |[ ======================================= Path Setup ======================================= ]|
--Set standard paths for the rest of the chapter.
gsStandardGameOver     = gsRoot .. "Chapter 2/Scenes/300 Standards/Defeat/Scene_Begin.lua"
gsStandardRetreat      = gsRoot .. "Chapter 2/Scenes/300 Standards/Retreat/Scene_Begin.lua"
gsStandardRevert       = gsRoot .. "Chapter 2/Scenes/300 Standards/Revert/Scene_Begin.lua"
gsStandardReliveBegin  = gsRoot .. "Chapter 2/Scenes/Z Relive Handler/Scene_Begin.lua"
gsStandardReliveEnd    = gsRoot .. "Chapter 2/Scenes/Z Relive Handler/Scene_End.lua"

--Menu Paths
AM_SetPropertyJournal("Bestiary Resolve Script", gsRoot .. "Chapter 2/200 Bestiary Handler.lua")
AM_SetPropertyJournal("Profile Resolve Script",  gsRoot .. "Chapter 2/210 Profile Handler.lua")
AM_SetPropertyJournal("Quest Resolve Script",    gsRoot .. "Chapter 2/220 Quest Handler.lua")
AM_SetPropertyJournal("Location Resolve Script", gsRoot .. "Chapter 2/230 Location Handler.lua")
AM_SetPropertyJournal("Paragon Resolve Script",  gsRoot .. "Chapter 2/240 Paragon Handler.lua")
AM_SetPropertyJournal("Combat Resolve Script",   gsRoot .. "Chapter 0/240 Combat Handler.lua")

--Autosave Icon
AL_SetProperty("Autosave Icon Path", "Root/Images/AdventureUI/Symbols22/RuneSanya")

--Journal Icons
if(AM_GetPropertyJournal("Can Set Images") == true) then
    AM_SetPropertyJournal("Set Achievement Title",       "Plantchievements")
    AM_SetPropertyJournal("Set Achievement Show String", "[IMG0] Show Plantchievements")
    AM_SetPropertyJournal("Set Achievement Hide String", "[IMG0] Hide Plantchievements")
    AM_SetPropertyJournal("Set Achievement Over Image",  "Root/Images/AdventureUI/Journal/Static Parts Achievements Over Ch2")
end

-- |[ =============================== Chapter-Specific Functions =============================== ]|
--Used to resolve a single value for kitsunes to talk to you about, based on how news has gotten around.
LM_ExecuteScript(gsRoot .. "Subroutines/Chapter 2/fnResolveKitsuneState.lua")

--Used to build strings when Miso is telling you what ingredients are needed.
LM_ExecuteScript(gsRoot .. "Subroutines/Chapter 2/fnBuildMisoRequirementsString.lua")

--Standardized mountain background.
LM_ExecuteScript(gsRoot .. "Subroutines/Chapter 2/fnTrafalMountainUnderlay.lua")
        
-- |[ ======================================== Clearing ======================================== ]|
--Because of how the load handler works, there may be lingering equipment in the inventory if the
-- player saved and loaded in Nowhere, as characters are given their default gear at game load.
--Therefore, clear the inventory again.

-- |[Catalyst Handling]|
if(gbDontRecountCatalysts ~= true) then
    
    --Clear.
    AdInv_SetProperty("Clear")

    --Order the inventory to recompute how many catalysts we've picked up. Chapters can be done out
    -- of order or with NC+ so the current counts aren't necessarily zeroes.
    AdInv_SetProperty("Resolve Catalysts By String", "Chapter 2")
    
    --Get the counts from the inventory.
    local iHealthCatalyst     = AdInv_GetProperty("Catalyst Count", gciCatalyst_Health)
    local iAttackCatalyst     = AdInv_GetProperty("Catalyst Count", gciCatalyst_Attack)
    local iInitiativeCatalyst = AdInv_GetProperty("Catalyst Count", gciCatalyst_Initiative)
    local iEvadeCatalyst      = AdInv_GetProperty("Catalyst Count", gciCatalyst_Evade)
    local iAccuracyCatalyst   = AdInv_GetProperty("Catalyst Count", gciCatalyst_Accuracy)
    local iSkillCatalyst      = AdInv_GetProperty("Catalyst Count", gciCatalyst_Skill)

    --Set extra slots from skill catalysts.
    local iSlotCount = math.floor(iSkillCatalyst / gciCatalyst_Skill_Needed)
    AdvCombat_SetProperty("Set Skill Catalyst Slots", iSlotCount)

    --Mirror the catalyst counts into the DataLibrary.
    VM_SetVar("Root/Variables/Global/Catalysts/iHealth",     "N", iHealthCatalyst)
    VM_SetVar("Root/Variables/Global/Catalysts/iAttack",     "N", iAttackCatalyst)
    VM_SetVar("Root/Variables/Global/Catalysts/iInitiative", "N", iInitiativeCatalyst)
    VM_SetVar("Root/Variables/Global/Catalysts/iEvade",      "N", iEvadeCatalyst)
    VM_SetVar("Root/Variables/Global/Catalysts/iAccuracy",   "N", iAccuracyCatalyst)
    VM_SetVar("Root/Variables/Global/Catalysts/iSkill",      "N", iSkillCatalyst)
end

-- |[ ================================== Party Initialization ================================== ]|
-- |[ ========================== Sanya =========================== ]|
--Create Sanya. Her default job is "Hunter".
if(AdvCombat_GetProperty("Does Party Member Exist", "Sanya") == false) then
    Debug_Print(" Creating Sanya.\n")
    LM_ExecuteScript(gsRoot .. "Combat/Party/Sanya/000 Initialize.lua")

    --Create a Crimson Runestone and equip it to Sanya, along with her default equipment.
    Debug_Print(" Creating Sanya's equipment.\n")
    LM_ExecuteScript(gsItemListing, "Enfield Rifle")
    LM_ExecuteScript(gsItemListing, "7.62x56mmR Rounds")
    LM_ExecuteScript(gsItemListing, "Sanya's Jacket")
    LM_ExecuteScript(gsItemListing, "Crimson Runestone")
    AdvCombat_SetProperty("Push Party Member", "Sanya")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Weapon", "Enfield Rifle")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Ammo", "7.62x56mmR Rounds")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Body", "Sanya's Jacket")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Item A", "Crimson Runestone")
    DL_PopActiveObject()
end

--Run Sanya's costume routines.
LM_ExecuteScript(gsRoot .. "CostumeHandlers/Sanya/ZZ Common.lua")

-- |[ =========================== Izuna ========================== ]|
--Create Izuna. Her default job is "Miko".
if(AdvCombat_GetProperty("Does Party Member Exist", "Izuna") == false) then
    Debug_Print(" Creating Izuna.\n")
    LM_ExecuteScript(gsRoot .. "Combat/Party/Izuna/000 Initialize.lua")

    --Create Izuna's default equipment and equip it.
    Debug_Print(" Creating Izuna's equipment.\n")
    LM_ExecuteScript(gsItemListing, "Silver Straightsword")
    LM_ExecuteScript(gsItemListing, "Miko Robes")
    AdvCombat_SetProperty("Push Party Member", "Izuna")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Weapon", "Silver Straightsword")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Body", "Miko Robes")
    DL_PopActiveObject()
end

--Run Izuna's costume routines.
LM_ExecuteScript(gsRoot .. "CostumeHandlers/Izuna/ZZ Common.lua")

-- |[ ========================== Empress ========================= ]|
--Create Empress. Her default job is "Conquerer".
if(AdvCombat_GetProperty("Does Party Member Exist", "Empress") == false) then
    Debug_Print(" Creating Empress.\n")
    LM_ExecuteScript(gsRoot .. "Combat/Party/Empress/000 Initialize.lua")

    --Create Empress' default equipment and equip it.
    Debug_Print(" Creating Empress's equipment.\n")
    LM_ExecuteScript(gsItemListing, "Ancient Gauntlets")
    LM_ExecuteScript(gsItemListing, "Ancient Cuirass")
    AdvCombat_SetProperty("Push Party Member", "Empress")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Weapon", "Ancient Gauntlets")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Body", "Ancient Cuirass")
    DL_PopActiveObject()
end

--Run Izuna's costume routines.
LM_ExecuteScript(gsRoot .. "CostumeHandlers/Empress/ZZ Common.lua")

-- |[ =========================== Zeke =========================== ]|
--Create Zeke. He's a goat.
if(AdvCombat_GetProperty("Does Party Member Exist", "Zeke") == false) then
    Debug_Print(" Creating Zeke.\n")
    LM_ExecuteScript(gsRoot .. "Combat/Party/Zeke/000 Initialize.lua")

    --Create Zeke's default equipment and equip it.
    Debug_Print(" Creating Zeke's equipment.\n")
    LM_ExecuteScript(gsItemListing, "Zeke's Bell Collar")
    AdvCombat_SetProperty("Push Party Member", "Zeke")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Collar", "Zeke's Bell Collar")
    DL_PopActiveObject()
end

--Run Izuna's costume routines.
LM_ExecuteScript(gsRoot .. "CostumeHandlers/Zeke/ZZ Common.lua")

-- |[ =========================== Odar =========================== ]|
--Create Odar. He's a fungoid.
if(AdvCombat_GetProperty("Does Party Member Exist", "Odar") == false) then
    Debug_Print(" Creating Odar.\n")
    LM_ExecuteScript(gsRoot .. "Combat/Party/Odar/000 Initialize.lua")

    --Create a Crimson Runestone and equip it to Sanya, along with her default equipment.
    Debug_Print(" Creating Odar's equipment.\n")
    --LM_ExecuteScript(gsItemListing, "Crimson Runestone")
    --LM_ExecuteScript(gsItemListing, "Mei's Work Uniform")
    AdvCombat_SetProperty("Push Party Member", "Odar")
        --AdvCombatEntity_SetProperty("Equip Item To Slot", "Item A", "Crimson Runestone")
        --AdvCombatEntity_SetProperty("Equip Item To Slot", "Body", "Mei's Work Uniform")
    DL_PopActiveObject()
end

--Run Izuna's costume routines.
LM_ExecuteScript(gsRoot .. "CostumeHandlers/Odar/ZZ Common.lua")

-- |[ ========================================== Flags ========================================= ]|
--Mark Sanya as the party leader.
gsPartyLeaderName = "Sanya"

--Set the party positions. Sanya is the leader, all others are empty.
Debug_Print(" Positioning party slots.\n")
AdvCombat_SetProperty("Clear Party")
AdvCombat_SetProperty("Party Slot", 0, "Sanya")

--Setup the XP tables.
Debug_Print(" Booting XP table.\n")
LM_ExecuteScript(gsRoot .. "Combat/Party/XP Table Chapter 2.lua")

-- |[ ================================= Variable Initialization ================================ ]|
--Mark this as the current chapter.
Debug_Print(" Initializing chapter 2 variables.\n")
VM_SetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N", 2.0)

--Create script variables. This is done in its own file.
LM_ExecuteScript(sBasePath .. "001 Variables.lua")

--Create paths referring to enemies.
LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 2/Alias List.lua")

--Combat Paragon script
AdvCombat_SetProperty("Set Paragon Script",      gsRoot .. "Combat/Enemies/Chapter 2/Paragon Handler.lua")
AdvCombat_SetProperty("Set Enemy Auto Script",   gsRoot .. "Combat/Enemies/Chapter 2/Enemy Auto Handler.lua")
AdvCombat_SetProperty("Set Post Combat Handler", gsRoot .. "Combat/Enemies/Chapter 2/Post Combat Handler.lua")

--Combat Volunteer Script
AdvCombat_SetProperty("Set Volunteer Script", gsRoot .. "Chapter 2/100 Volunteer Handler.lua")

-- |[ ================================== Character Appearance ================================== ]|
--Call the costume handlers to build default appearances for the characters.
Debug_Print(" Running costume resolvers.\n")
local bSuppressErrorsOld = gbSuppressNoListErrors
gbSuppressNoListErrors = true
LM_ExecuteScript(gsCostumeAutoresolve, "Sanya_Human")
gbSuppressNoListErrors = bSuppressErrorsOld

-- |[ ===================================== Doctor Bag Zero ==================================== ]|
--Sanya starts the chapter with a doctor bag, but quickly loses it.
Debug_Print(" Setting doctor bag.\n")
gbAutoSetDoctorBagCurrentValues = true
LM_ExecuteScript(gsComputeDoctorBagTotalPath)

-- |[ ====================================== Party Restore ===================================== ]|
--Make sure everyone is at full HP. This affects the whole party roster, not just the active party.
Debug_Print(" Restoring party.\n")
AdvCombat_SetProperty("Restore Roster")

-- |[ ====================================== Path Building ===================================== ]|
--Run this script to build cutscenes that can be accessed from the debug menu.
Debug_Print(" Building paths.\n")
LM_ExecuteScript(sBasePath .. "Scenes/Build Scene List.lua")

--Build a set of aliases. These must be used in place of hard paths for in-engine script calls.
LM_ExecuteScript(sBasePath .. "Scenes/Build Alias List.lua")

--Build a list of locations the player can warp to from the debug menu.
LM_ExecuteScript(gsRoot .. "Maps/Build Debug Warp List.lua", "Chapter 2")

--Standard combat paths.
AdvCombat_SetProperty("Standard Retreat Script", gsRoot .. "Chapter 2/Scenes/300 Standards/Retreat/Scene_Begin.lua")
AdvCombat_SetProperty("Standard Defeat Script",  gsRoot .. "Chapter 2/Scenes/300 Standards/Defeat/Scene_Begin.lua")
AdvCombat_SetProperty("Standard Victory Script", gsRoot .. "Chapter 2/Scenes/300 Standards/Victory/Scene_Begin.lua")

--UI Icons
AM_SetProperty("Set Form Icon",    "Root/Images/AdventureUI/CampfireMenuIcon/TransformSanya")
AM_SetProperty("Set Relive Icon",  "Root/Images/AdventureUI/CampfireMenuIcon/ReliveSanya")
AM_SetProperty("Set Costume Icon", "Root/Images/AdventureUI/CampfireMenuIcon/Costume")

-- |[ ===================================== Music and Sound ==================================== ]|
--Default combat music.
Debug_Print(" Setting combat music.\n")
AdvCombat_SetProperty("Default Combat Music", "BattleThemeSanya", 0.000)

-- |[ ===================================== Field Abilities ==================================== ]|
--Purge existing abilities.
DL_Purge("Root/Special/Combat/FieldAbilities/", false)
DL_AddPath("Root/Special/Combat/FieldAbilities/")

--Create any abilities.
Debug_Print(" Setting field abilities.\n")
DL_AddPath("Root/Special/Combat/FieldAbilities/")
LM_ExecuteScript(gsFieldAbilityListing .. "Reset.lua",           gciFieldAbility_Create)
LM_ExecuteScript(gsFieldAbilityListing .. "Use Rifle.lua",       gciFieldAbility_Create)
LM_ExecuteScript(gsFieldAbilityListing .. "Extra Extra.lua",     gciFieldAbility_Create)
LM_ExecuteScript(gsFieldAbilityListing .. "Change Pamphlet.lua", gciFieldAbility_Create)

--Null off the ability set.
AdvCombat_SetProperty("Set Field Ability", 0, "NULL")
AdvCombat_SetProperty("Set Field Ability", 1, "NULL")
AdvCombat_SetProperty("Set Field Ability", 2, "NULL")
AdvCombat_SetProperty("Set Field Ability", 3, "NULL")
AdvCombat_SetProperty("Set Field Ability", 4, "NULL")

-- |[ ======================================== Catalysts ======================================= ]|
AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Health, 0)
AdInv_SetProperty("Catalyst Local Max", gciCatalyst_Health, 0)

AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Attack, 0)
AdInv_SetProperty("Catalyst Local Max", gciCatalyst_Attack, 0)

AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Initiative, 0)
AdInv_SetProperty("Catalyst Local Max", gciCatalyst_Initiative, 0)

AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Evade, 0)
AdInv_SetProperty("Catalyst Local Max", gciCatalyst_Evade, 0)

AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Accuracy, 0)
AdInv_SetProperty("Catalyst Local Max", gciCatalyst_Accuracy, 0)

AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Skill, 0)
AdInv_SetProperty("Catalyst Local Max", gciCatalyst_Skill, 0)
    
-- |[ ================================= Autosave Hunt Handling ================================= ]|
--When loading an autosave during the hunting tutorial, the hunt constructor gets called before it
-- has a chance to initialize. This will set the function to a value which basically creates the
-- function properly, fixing the autosave issue.
if(fnHuntConstructor == nil) then
    function fnHuntConstructor()
        LM_ExecuteScript(gsHuntingPath .. "000 Initialize.lua")
    end
end

-- |[ ====================================== Intro Bypass ====================================== ]|
--The player can optionally bypass the intro. If that happens, this code executes.
if(gbBypassIntro == true) then
    
end
Debug_PopPrint("Finished chapter 2 initialization.\n")

--Set deconstruction path.
--AM_SetProperty("Set Deconstruct Script", gsRoot .. "Items/Deconstruct.lua")