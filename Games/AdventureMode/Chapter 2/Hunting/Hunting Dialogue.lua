-- |[ ===================================== Dialogue Script ==================================== ]|
--Clues refer to this script when interacted with.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorBaseName = TA_GetProperty("Name")
    
    --Split the name up. The pattern is "PreyName|Direction".
    local iBarSlot   = string.find(sActorBaseName, "|")
    local sActorName = string.sub(sActorBaseName, 1, iBarSlot-1)
    local sActorDir  = string.sub(sActorBaseName, iBarSlot+1)
    
    -- |[Prey Locator]|
    --Scan across the prey and find the one matching the NPC name.
	local iPreyIndex, zPreyObject = Hunting:fnGetActivePrey(sActorName)
	if(zPreyObject ~= nil) then

		--Tutorial has a special case:
		local iIsTutorialHuntActive = VM_GetVar("Root/Variables/Chapter2/Northwoods/iIsTutorialHuntActive", "N")
		local iExaminedTracks       = VM_GetVar("Root/Variables/Chapter2/Northwoods/iExaminedTracks", "N")
		if(iIsTutorialHuntActive == 1.0 and iExaminedTracks == 0.0) then
			VM_SetVar("Root/Variables/Chapter2/Northwoods/iExaminedTracks", "N", 1.0)
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ Append("Sanya:[VOICE|Sanya] (Tracks that look like they're heading west.)[B][C]") ]])
			fnCutscene([[ Append("Sanya:[VOICE|Sanya] (Wild animals don't care about trees or whatnot, so I might have to find a way around.)[B][C]") ]])
			fnCutscene([[ Append("Sanya:[VOICE|Sanya] (In the future I won't have to examine the tracks, I think I can figure out their direction from a distance.)[B][C]") ]])
			fnCutscene([[ Append("Sanya:[VOICE|Sanya] (All right, dinner.[P] Come to mommy.)") ]])
			return
		end
		
		--Common:
		fnCutscene([[ WD_SetProperty("Show") ]])
		
		--Direction:
		if(sActorDir == "N") then
			fnCutscene([[ Append("Sanya:[VOICE|Sanya] (There's tracks here.[P] Looks like the animal went north.)") ]])
		elseif(sActorDir == "NE") then
			fnCutscene([[ Append("Sanya:[VOICE|Sanya] (There's tracks here.[P] Looks like the animal went northeast.)") ]])
		elseif(sActorDir == "E") then
			fnCutscene([[ Append("Sanya:[VOICE|Sanya] (There's tracks here.[P] Looks like the animal went east.)") ]])
		elseif(sActorDir == "SE") then
			fnCutscene([[ Append("Sanya:[VOICE|Sanya] (There's tracks here.[P] Looks like the animal went southeast.)") ]])
		elseif(sActorDir == "S") then
			fnCutscene([[ Append("Sanya:[VOICE|Sanya] (There's tracks here.[P] Looks like the animal went south.)") ]])
		elseif(sActorDir == "SW") then
			fnCutscene([[ Append("Sanya:[VOICE|Sanya] (There's tracks here.[P] Looks like the animal went southwest.)") ]])
		elseif(sActorDir == "W") then
			fnCutscene([[ Append("Sanya:[VOICE|Sanya] (There's tracks here.[P] Looks like the animal went west.)") ]])
		elseif(sActorDir == "NW") then
			fnCutscene([[ Append("Sanya:[VOICE|Sanya] (There's tracks here.[P] Looks like the animal went northwest.)") ]])
		end
    end
    
    
-- |[ ====================================== "Shot Check" ====================================== ]|
--If this entity is targeted by a rifle shot, this section of code returns whether or not a response is merited.
elseif(sTopicName == "Shot Check") then
    
    -- |[Name Resolve]|
    --Resolve the name of the calling entity. No further processing is needed.
    local sActorName = TA_GetProperty("Name")
    
    -- |[Prey Locator]|
    --Scan across the prey and find the one matching the NPC name.
    local bPreyHandledHit = false
    for i = 1, #Hunting.zaPreyList, 1 do
		
		--Fast-access pointer.
		local zActivePrey = Hunting.zaPreyList[i]
		
		--If hit, AND not already hit, reply.
        if(zActivePrey.sName == sActorName and zActivePrey.bDisabled == false) then
            
            --Flag for hit reply. Begin death sequence.
            bPreyHandledHit = true
            AL_SetProperty("Examinable Reply To Hit")
            EM_PushEntity(sActorName)
                TA_SetProperty("Begin Dying")
            DL_PopActiveObject()
            
        end
    end
    
    --Finish up if any prey handled the shot.
    if(bPreyHandledHit == true) then return end
    
    -- |[Non-Prey]|
    --If this entity got SHOT but was not a prey entity, check if it's an enemy. If it is, the enemy
    -- is stunned (similar to being mugged) on hit. NPCs don't respond to shots.
    EM_PushEntity(sActorName)
    
        --Check if we're an enemy.
        local bIsEnemy = TA_GetProperty("Is Enemy")
        
        --If so, respond to the shot and also activate the stun.
        if(bIsEnemy == true) then
            TA_SetProperty("Get Stunned")
        end
    DL_PopActiveObject()
    
    --Flag reply.
    if(bIsEnemy) then 
        AL_SetProperty("Examinable Reply To Hit")
    end

-- |[ ===================================== "Shot Handle" ====================================== ]|
--If this entity is targeted by a rifle shot, and responded to the shot check, this block of code is used.
elseif(sTopicName == "Shot Handle") then
    
    -- |[Name Resolve]|
    --Resolve the name of the calling entity. No further processing is needed.
    local sActorName = TA_GetProperty("Name")
    local fActorX, fActorY = TA_GetProperty("Position")
    
    -- |[Prey Locator]|
    --Scan across the prey and find the one matching the NPC name.
    for i = 1, #Hunting.zaPreyList, 1 do
		
		--Fast-access pointer.
		local zActivePrey = Hunting.zaPreyList[i]
		
		--If this is the hit actor, AND it hasn't already been hit:
        if(zActivePrey.sName == sActorName and zActivePrey.bDisabled == false) then
            
            --Spawn a loot bag with the loot in it.
            TA_Create("LootBag")
                TA_SetProperty("Position", (fActorX-4.0) / gciSizePerTile, (fActorY-10.0) / gciSizePerTile)
                TA_SetProperty("Facing", gci_Face_South)
                TA_SetProperty("Clipping Flag", false)
                TA_SetProperty("Activation Script", "Null")
                TA_SetProperty("Ignores Gravity", false)
                TA_SetProperty("Z Speed", gcfStdJumpSpeed * 1.20)
                fnSetCharacterGraphics("Root/Images/Sprites/GenericF0/", false)
                TA_SetProperty("Activate Enemy Mode", "Ignore")
                TA_SetProperty("Set Item Mode", zActivePrey.sLootDrop)
                TA_SetProperty("Wipe Special Frames")
                TA_SetProperty("Add Special Frame", "Bag", "Root/Images/Sprites/FallenLoot/0")
                TA_SetProperty("Set Special Frame", "Bag")
            DL_PopActiveObject()
    
            --Figure out which prototype this entity uses.
            local iProtoIndex, zUsePrototype = Hunting:fnGetPreyPrototypeByType(zActivePrey.iType)
            
            --Add a hunt KO.
            local sHuntTrackerPath = VM_GetVar("Root/Variables/Global/Combat/sHuntTrackerPath", "S")
            local iHuntCount = VM_GetVar(sHuntTrackerPath .. zUsePrototype.sCombatEnemy, "N")
            VM_SetVar(sHuntTrackerPath .. zUsePrototype.sCombatEnemy, "N", iHuntCount + 1)
            
            --Mark as disabled. It will spawn tracks, but never an entity.
            zActivePrey.bDisabled = true
            
            --Mark this as defeated in the DataLibrary so the defeat persists across save/load.
			local sPreyTrackerPath = string.format("Root/Variables/Chapter2/HuntTracker/%02i", i-1)
            VM_SetVar(sPreyTrackerPath, "N", 1)
        end
    end
end
