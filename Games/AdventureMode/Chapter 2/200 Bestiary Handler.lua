-- |[ ==================================== Bestiary Handler ==================================== ]|
--Whenever the journal is opened to bestiary mode, run this script to populate bestiary entries.

-- |[Stat Table Check]|
if(gczaChapter2Stats == nil) then return end

-- |[Setup]|
--Access variable.
local zEntry = nil 

--Create chart for storage.
gzaCh2BestiaryEntries = {}

--Set active globals. This reduces the number of arguments that need to be passed.
JournalEntry.zaEnemyStats = gczaChapter2Stats
JournalEntry.zaAppendList = gzaCh2BestiaryEntries

-- |[Prototype]|
--zEntry = JournalEntry:new(sUniqueName, sDisplayName)
--zEntry:fnSetToChartEntry (sNameOnStatChart, bAlwaysShow, fPortraitOffsetX, fPortraitOffsetY, bCanTFPlayer)
--zEntry:fnSetDescription  (sDescription)

-- |[ =============== Listing ================ ]|
-- |[Northwoods / Shrine of the Hero]|
zEntry = JournalEntry:new("Bat Seeker", "Bat Seeker")
zEntry:fnSetToChartEntry ("Bat Seeker", gbDoNotAlwaysShow, 702, 157, gbCannotTFPlayer)
zEntry:fnSetDescription  ("While most bats are perfectly content with insects or berries, the[BR]deep crimson color that stains the fur around this oversized bat's[BR]mouth is an indication it is far pickier with "..
                          "what it eats. Can heal[BR]itself with a blood drain attack. Be careful that its bites don't[BR]get infected...")

zEntry = JournalEntry:new("Quiet Butterbomber", "Quiet Butterbomber")
zEntry:fnSetToChartEntry ("Quiet Butterbomber", gbDoNotAlwaysShow, 570, 122, gbCannotTFPlayer)
zEntry:fnSetDescription  ("The green and purple gossamer of this large moth seems to glisten in[BR]even the faintest light, luring potential prey toward it and into its[BR]ever-present cloud "..
                          "of poisonous dust. Once its prey is sufficiently[BR]weakend, it uses its needle-like forelegs to pin its prey to the ground,[BR]where it begins to feast on the still-living meal. ")

zEntry = JournalEntry:new("Cave Muncher", "Cave Muncher")
zEntry:fnSetToChartEntry ("Cave Muncher", gbDoNotAlwaysShow, 654, 157, gbCannotTFPlayer)
zEntry:fnSetDescription  ("While the average giant centipedes avoid confrontations with[BR]creatures significantly larger than it, this giant centipede is[BR]significantly larger than the "..
                          "average giant centipede. Its thick[BR]chitin provides for a good defense as it hunts smaller prey with its[BR]corrosive bite. Smaller prey that just so happen to "..
                          "include Kitsune,[BR]goats, and misplaced humans. ")

zEntry = JournalEntry:new("Poison Vine", "Poison Vine")
zEntry:fnSetToChartEntry ("Poison Vine", gbDoNotAlwaysShow, 789, 137, gbCannotTFPlayer)
zEntry:fnSetDescription  ("When engaging in war, the Alraune have often turned to the Poison[BR]Vine to quickly lay waste to lay waste to their enemies. Its poisonous[BR]spores could be set "..
                          "adrift on the wind, neutralizing entire camps[BR]residing downwind. Once the Alraune are ready to move on, Poison[BR]Vines would be left behind as a deterrent to "..
                          "those who may[BR]resettle the captured territory.")
    
zEntry = JournalEntry:new("Buffodil", "Buffodil")
zEntry:fnSetToChartEntry ("Buffodil", gbDoNotAlwaysShow, 519, 170, gbCannotTFPlayer)
zEntry:fnSetDescription  ("The Buffodil is a powerful guardian created by the Alraune to protect[BR]the forest from threats. Being both more aggressive and significantly[BR]stronger than the "..
                          "Sprout Peckers, the Buffodil are typically used as[BR]front-line fighters by the Alraune. However, their size and the[BR]destructive impact their rampaging has on the "..
                          "forest causes[BR]many Alraune to see them as an ally of last resort.")

zEntry = JournalEntry:new("Bunny Smuggler", "Bunny Smuggler")
zEntry:fnSetToChartEntry ("Bunny Smuggler", gbDoNotAlwaysShow, 658, 139, gbCannotTFPlayer)
zEntry:fnSetDescription  ("At home in the deep snows of the Trafal glaciers, these fleet-footed[BR]bandits have earned a reputation for being willing to transport just[BR]about any type of product, "..
                          "for the right price. Their webbed hind[BR]feet allow them to move quickly over the snow to avoid both[BR]predators and the limited law enforcement who still inhabit the[BR]snowy expanse.")

zEntry = JournalEntry:new("Harpy Recruit", "Harpy Recruit")
zEntry:fnSetToChartEntry ("Harpy Recruit", gbDoNotAlwaysShow, 762, 147, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A low-ranking Harpy who has been sent out into the glaciers and[BR]mountains to locate potential recruits for the nest to transform.[BR]Though not especially dangerous "..
                          "on their own, birds of a feather do[BR]tend to flock together. Without further direction, they tend to grab[BR]any possible passerby regardless of how useful they would "..
                          "be for[BR]the nest.[BR]They have an intricate uniform, suggesting organized intent.")

zEntry = JournalEntry:new("Harpy Officer", "Harpy Officer")
zEntry:fnSetToChartEntry ("Harpy Officer", gbDoNotAlwaysShow, 762, 147, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A high-ranking Harpy who has proven themselves capable of finding[BR]high-quality recruits to transform and expand the nest. They typically[BR]lead several lower-ranked "..
                          "Harpies to keep them from becoming[BR]distracted as well as to teach them what qualities to look for in a[BR]potential recruit.[BR]Their intricate uniforms suggest organized "..
                          "intent, moreso than usual[BR]harpy societies.")

zEntry = JournalEntry:new("Treant Wanderer", "Treant Wanderer")
zEntry:fnSetToChartEntry ("Treant Wanderer", gbDoNotAlwaysShow, 528, 82, gbCannotTFPlayer)
zEntry:fnSetDescription  ("On the first glance a mere inanimate tree, its VERY animate nature[BR]will be revealed once one gets within its grab range, as well as its[BR]carnivorous tendencies. "..
                          "Strangely enough, alraunes can't speak[BR]with the ignoble Treant, leading to some very interesting theories on[BR]the species' origin. The one thing scholars can agree "..
                          "on is that the[BR]Treant is ancient; possibly an errant offshoot of the regular tree...[BR]or maybe a progenitor?")

zEntry = JournalEntry:new("Sevavi Guardian", "Sevavi Guardian")
zEntry:fnSetToChartEntry ("Sevavi Guardian", gbDoNotAlwaysShow, 734, 137, bCanTFPlayer)
zEntry:fnSetDescription  ("The chiseled beauty of the Guardians melds svelte musculature and[BR]winsome curves into a graceful form that would be a delight to watch[BR]as she dances to and fro in "..
                          "defense of the Hero's Shrine. That is, of[BR]course, if she were not trying to crush you between her thighs for the[BR]crime of trespassing.")

zEntry = JournalEntry:new("Puissant Omnigoose", "Puissant Omnigoose")
zEntry:fnSetToChartEntry ("Puissant Omnigoose", gbDoNotAlwaysShow, 798, 168, gbCannotTFPlayer)
zEntry:fnSetDescription  ("The sleek feathers, dainty webbed feat, and long, elegant neck of the[BR]omnigoose belie the fearsome force that hides within this[BR]malefic creature. It gazes out "..
                          "upon the world with its beady[BR]black eyes, and its honk will bring terror to those unfortunate enough[BR]to encounter them.")

zEntry = JournalEntry:new("Clever Turkerus", "Clever Turkerus")
zEntry:fnSetToChartEntry ("Clever Turkerus", gbDoNotAlwaysShow, 748, 209, gbCannotTFPlayer)
zEntry:fnSetDescription  ("Being a popular meat in the Trafal region, one enterprising farmer[BR]began experimenting with ways to fatten her turkeys. She settled on[BR]feeding them faster and "..
                          "decided the most effective way would be to[BR]transform them and add two additional heads. Unfortunately for the[BR]farmer, this also increased the number of beaks "..
                          "with which the[BR]turkeys could peck, and they quickly escaped into the wilds.")

zEntry = JournalEntry:new("Rowdy Brimhog", "Rowdy Brimhog")
zEntry:fnSetToChartEntry ("Rowdy Brimhog", gbDoNotAlwaysShow, 595, 136, gbCannotTFPlayer)
zEntry:fnSetDescription  ("The chaotic confluence that gave rise to the lesser chimera that is[BR]the brimhog has long been forgotten, but the effects the brimhog[BR]have had on the landscape "..
                          "have not. They find the meat of[BR]adventurers to be as tasty as the meat of truffles, and if a boar and[BR]a snake can be said to be happy at all, they are happy to "..
                          "use their[BR]tusks and fangs to forage for both.")

zEntry = JournalEntry:new("Unielk Grazer", "Unielk Grazer")
zEntry:fnSetToChartEntry ("Unielk Grazer", gbDoNotAlwaysShow, 658, 122, gbCannotTFPlayer)
zEntry:fnSetDescription  ("Unlike the mystical properties of their equine counterparts, the[BR]horns of the unielk do not have the ability to heal any ailment,[BR]prolong life, increase stamina, "..
                          "nor enlarge various body parts. It is,[BR]however, highly effective at perforating hapless adventurers who get[BR]in the way of the unielk's charge.")

-- |[The Old Capital Grounds]|
zEntry = JournalEntry:new("Cave Devourer", "Cave Devourer")
zEntry:fnSetToChartEntry ("Cave Devourer", gbDoNotAlwaysShow, 658, 122, gbCannotTFPlayer)
zEntry:fnSetDescription  ("If the Cave Muncher was 'significantly larger than the average giant[BR]centipede', then the Devourer would best be described as [BR]'astoundingly larger than the "..
                          "average giant centipede'. [BR]That larger stature means, of course, more strength and stamina, but more[BR]food intake to sustain it. The Devourer spends no less "..
                          "than 21 hours[BR]each day looking for something to eat. The other three hours?[BR]It's eating.")

zEntry = JournalEntry:new("Bat Hunter", "Bat Hunter")
zEntry:fnSetToChartEntry ("Bat Hunter", gbDoNotAlwaysShow, 658, 122, gbCannotTFPlayer)
zEntry:fnSetDescription  ("The crimson-soaked fur of these large bats is accentuated by its[BR]noticeably enlarged white fangs and penchant for murder. While[BR]some weaker specimens (Weaker "..
                          "being a very relative term) are[BR]content with hunting for sustenance, the Hunter seems to revel in the[BR]kill, sometimes bringing down game much larger than "..
                          "itself and just[BR]leaving it to rot.")

-- |[Westwoods]|
zEntry = JournalEntry:new("Bunny Enforcer", "Bunny Enforcer")
zEntry:fnSetToChartEntry ("Bunny Enforcer", gbDoNotAlwaysShow, 658, 122, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A far cry from the lowly Smugglers of the bunny community, the[BR]Enforcer has no reason to run- why would it, when it's toting the[BR]latest in weapons technology? "..
                          "This law-dismissin', suit-rockin',[BR]straight-shootin' lapine ain't takin' no for an answer- unless the[BR]question is 'Should we put the gun away?'[BR]In all "..
                          "seriousness, though, they might have a problem. Therapy is good[BR]for everyone, kids!")

zEntry = JournalEntry:new("Bandit Stalker", "Bandit Stalker")
zEntry:fnSetToChartEntry ("Bandit Stalker", gbDoNotAlwaysShow, 696, 127, gbCannotTFPlayer)
zEntry:fnSetDescription  ("When it comes to bandits, there are usually two types: the big, burly,[BR]smash-stuff-with-a-barstool types, and the sneaky, silent, slit-your-[BR]hroat-in-your-sleep "..
                          "types. This particular type tends to fall into the[BR]latter category, forgoing heavier armor (Or, in some cases, clothes[BR]altogether) for a boost to speed and "..
                          "dexterity. Despite the chill of the[BR]glaciers and mountains littering Trafal, they still manage to keep[BR]warm, somehow...")

zEntry = JournalEntry:new("Bandit Killer", "Bandit Killer")
zEntry:fnSetToChartEntry ("Bandit Killer", gbDoNotAlwaysShow, 696, 127, gbCannotTFPlayer)
zEntry:fnSetDescription  ("When it comes to bandits, there are usually two types: the big, burly,[BR]use-a-boulder-as-an-improvised-weapon types, and the sneaky, silent,[BR]stole-your-pants-"..
                          "buckle-and-now-everyone-can-see-your-heart-print[BR]-undies types. This particular type tends to fall into the former [BR]category, as their astute name implies. "..
                          "They sport heavier armor than[BR]their company, but are slower as a result.")

zEntry = JournalEntry:new("Frost Crawler", "Frost Crawler")
zEntry:fnSetToChartEntry ("Frost Crawler", gbDoNotAlwaysShow, 730, 209, gbCannotTFPlayer)
zEntry:fnSetDescription  ("Snow Golems are not an uncommon sight in any of the various[BR]magical institutions around the lands of Pandemonium, as it serves as[BR]a perfect medium for "..
                          "budding cryomancers to work with. After all,[BR]if it goes haywire, just leave it out in the sun for a few hours and it's[BR]not a problem anymore. But on "..
                          "the icy slopes of Trafal, where ice[BR]*doesn't* melt...")

zEntry = JournalEntry:new("Caustic Grub", "Caustic Grub")
zEntry:fnSetToChartEntry ("Caustic Grub", gbDoNotAlwaysShow, 722, 216, gbCannotTFPlayer)
zEntry:fnSetDescription  ("One of the common sights of the bog-like Westwoods, the Caustic[BR]Grub is one of many living in a nearby nest. They show signs of either[BR]psychic linking "..
                          "or a hivemind, but no scholar has ever gotten close[BR]enough to a nest to find out- as once the nest detects you using its[BR]many microscopic mycelia, the "..
                          "grubs'll be on you in an instant.[BR]Then through you, as their acidic saliva will liquefy pretty much [BR]anything unlucky enough to be brought into contact with it.")

zEntry = JournalEntry:new("Grub Nest", "Grub Nest")
zEntry:fnSetToChartEntry ("Grub Nest", gbDoNotAlwaysShow, 658, 122, gbCannotTFPlayer)
zEntry:fnSetDescription  ("The nests of the Caustic Grubs are very unique in the fact that no[BR]two are ever perfectly alike. Made from local bits of debris dissolved[BR]and warped down, "..
                          "the honeycomb-like inside is created from a type[BR]of symbiotic fungus which also gives the hive its defense system.[BR]Within the hardened shell, grubs of all "..
                          "types and stages of[BR]development can be seen. Whether or not there is a 'queen grub' is still[BR]up for debate.")

zEntry = JournalEntry:new("Brutal Redcap", "Brutal Redcap")
zEntry:fnSetToChartEntry ("Brutal Redcap", gbDoNotAlwaysShow, 668, 25, gbCannotTFPlayer)
zEntry:fnSetDescription  ("If one were hurriedly running through a moonlit forest, hoping to get[BR]home before a dastardly monster did oneself in, and one caught sight[BR]of the Redcap "..
                          "out of the corner of one's eye, you might mistake it for[BR]a red cloak of some kind, evocative of a story once told by one's[BR]grandmother. If lured close "..
                          "enough by its appearance, one would also[BR]be subsequently disemboweled by its tentacles, able to rip one's head[BR]off of one's shoulders before one's eyes could even register it.")

zEntry = JournalEntry:new("Hungry Suckfly", "Hungry Suckfly")
zEntry:fnSetToChartEntry ("Hungry Suckfly", gbDoNotAlwaysShow, 746, 175, gbCannotTFPlayer)
zEntry:fnSetDescription  ("Sporting a deadly needle-like proboscis of hardened chitin, the[BR]Suckfly needs not much else to be a contender in the hostile marshes[BR]of the Westwood. Of "..
                          "course, it has a bunch of other evolutions to[BR]perfectly compliment its pestilential existence: a sac of corrosive[BR]venom it can spit at foes and prey, whip-fast "..
                          "hover flight and[BR]reflexes, and worst of all- an annoying, ear-splitting 'EEEEEEEEEEEE'[BR] that never seems to go away.")

zEntry = JournalEntry:new("Neutral Toxishroom", "Neutral Toxishroom")
zEntry:fnSetToChartEntry ("Neutral Toxishroom", gbDoNotAlwaysShow, 783, 213, gbCannotTFPlayer)
zEntry:fnSetDescription  ("With a name like 'neutral', you might wonder how toxic the[BR]Toxishroom really is. But don't worry, this is merely the classification's[BR]way of letting you "..
                          "know the more vile versions of toxishroom found[BR]elsewhere are somehow worse. They still hurt to touch either way,[BR]and are highly resistant to poison and corrosion.")

zEntry = JournalEntry:new("Lurking Swamphag", "Lurking Swamphag")
zEntry:fnSetToChartEntry ("Lurking Swamphag", gbDoNotAlwaysShow, 522, 98, gbCannotTFPlayer)
zEntry:fnSetDescription  ("Possibly humans, possibly intelligent magic-using monsters animated[BR]by the wisps floating above their heads, the swamphag has many[BR]legends surrounding it. "..
                          "Mostly dragging people into the swamp to[BR]drown them, or let them boil alive in the corrosive goo.")

zEntry = JournalEntry:new("Evasive Buneye", "Evasive Buneye")
zEntry:fnSetToChartEntry ("Evasive Buneye", gbDoNotAlwaysShow, 809, 118, gbCannotTFPlayer)
zEntry:fnSetDescription  ("Predatory animals tend to have their eyes on the front of the head,[BR]allowing for greater accuracy and targeting, while prey animals tend[BR]to have side facing "..
                          "eyes to maximize view range at all times. The[BR]Buneye, then, is the next logical step in the evolutionary ladder. This[BR]hexacloptic beast boasts not only a 360 "..
                          "degree range of vision, but an[BR]increased brain capacity, necessary for processing all its visual input.[BR]Ironically, it is sought after to make blindness poisons.")

zEntry = JournalEntry:new("Pseudogriffon Hen", "Pseudogriffon Hen")
zEntry:fnSetToChartEntry ("Pseudogriffon Hen", gbDoNotAlwaysShow, 797, 216, gbCannotTFPlayer)
zEntry:fnSetDescription  ("It is unclear if the myth of griffons came first, or if this tiny beast[BR]somehow inspired them. This fluffy creature, though small in stature[BR]and cute as a button, "..
                          "can and will defend its territory to the death.[BR]It always has at least one clutch of eggs nearby, if not multiple, and if[BR]recovered they provide a hearty and "..
                          "tasty meal when scrambled,[BR]fried, or turned into an omelet. Despite many attempts at[BR]domestication, the pseudogriffon remains a wild spirit.")

zEntry = JournalEntry:new("Macho Brimhog", "Macho Brimhog")
zEntry:fnSetToChartEntry ("Macho Brimhog", gbDoNotAlwaysShow, 595, 136, gbCannotTFPlayer)
zEntry:fnSetDescription  ("As if the regular Brimhog weren't enough to deal with, every year or[BR]two they will enter a mating frenzy during which their physiques are[BR]flooded with hormones, "..
                          "rendering them restless, violent, and[BR]practically immune to pain. What's worse, once one hog becomes[BR]'flushed', this can trigger a chain reaction where an entire "..
                          "populace[BR]becomes frenzied. Truly not something you want to be stuck in the[BR]middle of. Yet here you are.")

zEntry = JournalEntry:new("Unielk Slatherer", "Unielk Slatherer")
zEntry:fnSetToChartEntry ("Unielk Slatherer", gbDoNotAlwaysShow, 658, 122, gbCannotTFPlayer)
zEntry:fnSetDescription  ("The Unielk Slatherer has some odd mannerisms, even for extremophile[BR]wildlife. It tends to chew up a type of herb indigenous to the Trafal[BR]scrubland. Chewed into a "..
                          "slimy cud, it then slathers this on a surface[BR]of its choosing, attracting swamp gnats that feast on the cud. After[BR]some time, the unielk eats the cud again, this "..
                          "time with a rich stew of[BR]gnats in it.")

-- |[ ============== Finish Up =============== ]|
-- |[Assemble List]|
for i = 1, #gzaCh2BestiaryEntries, 1 do
    gzaCh2BestiaryEntries[i]:fnUploadData()
end

-- |[Clean]|
--Unsets globals.
JournalEntry:fnCleanGlobals()

--Clear the list to save memory.
gzaCh2BestiaryEntries = nil

--[=[
Trained as a warrior, this high-ranked Kitsune has likely spent more time in combat than playing tricks on anyone. She's willing to teach you some of what she has learned over her many years but doing so seems to involve beating the lesson into you. Literally.

Oh! ye cruel Fates! What maleficence is brought upon you to find that even in the distant world of Pandemonium, there yet exists the maligned menace that is the Goose! Do not be deceived by its sleek feathers and long, elegant neck. The omnigoose knows only death and seeks naught but the destruction of all that is, was, or may ever be!
*Find a spot to put this description -Salty
]=]
