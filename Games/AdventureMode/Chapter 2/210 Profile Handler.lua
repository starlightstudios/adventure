-- |[ ===================================== Profile Handler ==================================== ]|
--Whenever the journal is opened to profiles mode, generates profiles.

-- |[Listing]|
--Setup.
gzaCh2ProfileEntries = {}

--Set globals.
JournalEntry.zaAppendList = gzaCh2ProfileEntries

-- |[Call Subhandlers]|
local sProfilePath = gsRoot .. "Chapter 2/Journal Profiles/"

--Party members.
LM_ExecuteScript(sProfilePath .. "Sanya.lua")
LM_ExecuteScript(sProfilePath .. "Zeke.lua")
LM_ExecuteScript(sProfilePath .. "Izuna.lua")
LM_ExecuteScript(sProfilePath .. "Empress.lua")

--Major characters.
LM_ExecuteScript(sProfilePath .. "Odar.lua")

--Minor characters.
LM_ExecuteScript(sProfilePath .. "Mia.lua")
LM_ExecuteScript(sProfilePath .. "Miso.lua")
LM_ExecuteScript(sProfilePath .. "Sonoko.lua")
LM_ExecuteScript(sProfilePath .. "Yukina.lua")
LM_ExecuteScript(sProfilePath .. "Angelface.lua")
LM_ExecuteScript(sProfilePath .. "Esmerelda.lua")
LM_ExecuteScript(sProfilePath .. "Takahn.lua")
LM_ExecuteScript(sProfilePath .. "Marigold.lua")


-- |[Assemble List]|
--Upload the information from the list.
for i = 1, #gzaCh2ProfileEntries, 1 do
    gzaCh2ProfileEntries[i]:fnUploadData()
end

-- |[Finish Up]|
--Reset globals.
JournalEntry:fnCleanGlobals()

--Clear the list to save memory.
gzaCh2ProfileEntries = nil