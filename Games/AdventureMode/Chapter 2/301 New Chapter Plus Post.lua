-- |[ ================================ New Chapter Plus - Post ================================= ]|
--After the initializer file is called, call this file to overwrite relevant variables.

-- |[Inventory]|
--Platina.
AdInv_SetProperty("Add Platina", VM_GetVar("Root/Variables/NewChapterPlus/Chapter2/iPlatina", "N"))

--Adamantite.
for i = gciCraft_Adamantite_Powder, gciCraft_Adamantite_Total-1, 1 do
    AdInv_SetProperty("Crafting Material", i, VM_GetVar("Root/Variables/NewChapterPlus/Chapter2/iAdamantite"..i, "N"))
end

--Inventory items.
local iItemsTotal = VM_GetVar("Root/Variables/NewChapterPlus/Chapter2/iItemsUnequipped", "N")
for i = 1, iItemsTotal, 1 do
    local sItemName     = VM_GetVar("Root/Variables/NewChapterPlus/Chapter2/sItem"..i.."Name",     "S")
    local iItemQuantity = VM_GetVar("Root/Variables/NewChapterPlus/Chapter2/iItem"..i.."Quantity", "N")
    for p = 1, iItemQuantity, 1 do
        LM_ExecuteScript(gsItemListing, sItemName)
    end
end

-- |[Doctor Bag Boosts]|
--None in chapter 2.

-- |[Costumes]|
--Sanya's costumes.
VM_SetVar("Root/Variables/Costumes/Sanya/iQueenBee", "N", VM_GetVar("Root/Variables/NewChapterPlus/Chapter2/iCostumeUnlocked_QueenBee", "N"))

-- |[Forms]|
--Forms unlocked for Sanya:
VM_SetVar("Root/Variables/Global/Sanya/iHasStatueForm", "N", VM_GetVar("Root/Variables/NewChapterPlus/Chapter2/iFormUnlocked_Statue",     "N"))

--Store which jobs are unlocked for Izuna:
--VM_SetVar("Root/Variables/Global/Izuna/iHasJob_Mediator", "N", VM_GetVar("Root/Variables/NewChapterPlus/Chapter2/iJobUnlocked_Mediator", "N"))

--Store which jobs are unlocked for Empress:

--Zeke. The goat with no jobs!

-- |[Skillbooks]|
VM_SetVar("Root/Variables/Global/Sanya/iSkillbookTotal", "N", VM_GetVar("Root/Variables/NewChapterPlus/Chapter2/iSkillbooksSanya", "N"))
VM_SetVar("Root/Variables/Global/Izuna/iSkillbookTotal", "N", VM_GetVar("Root/Variables/NewChapterPlus/Chapter2/iSkillbooksIzuna", "N"))

-- |[Characters]|
--Reset characters to their defaults.
LM_ExecuteScript(gsRoot .. "FormHandlers/Sanya/Form_Human.lua")
--LM_ExecuteScript(gsRoot .. "FormHandlers/Florentina/Job_Merchant.lua")

-- |[ ===================================== Chest Handling ===================================== ]|
--Store the catalyst chests.
local saList = {}
local function fnAddToList(psLevelName, psChestName, psChapter, psCatalystType)
    local i = #saList + 1
    saList[i] = {}
    saList[i].sLevelName    = psLevelName
    saList[i].sChestName    = psChestName
    saList[i].sChapter      = psChapter
    saList[i].sCatalystType = psCatalystType
end

--Chapter 1 Catalysts.
fnAddToList("ArbonnePlainsD",         "ChestB", "Chapter 1", "Accuracy")
fnAddToList("BeehiveBasementB",       "ChestA", "Chapter 1", "Evade")
fnAddToList("BeehiveBasementE",       "ChestA", "Chapter 1", "Skill")
fnAddToList("TrapMainFloorCentral",   "ChestA", "Chapter 1", "Attack")
fnAddToList("TrapMainFloorEast",      "ChestA", "Chapter 1", "Attack")
fnAddToList("TrapUpperFloorMain",     "ChestA", "Chapter 1", "Skill")
fnAddToList("EvermoonCassandraA",     "ChestA", "Chapter 1", "Skill")
fnAddToList("EvermoonE",              "ChestB", "Chapter 1", "Accuracy")
fnAddToList("EvermoonNE",             "ChestB", "Chapter 1", "Health")
fnAddToList("EvermoonS",              "ChestC", "Chapter 1", "Evade")
fnAddToList("EvermoonSEC",            "ChestA", "Chapter 1", "Initiative")
fnAddToList("EvermoonSlimeVillage",   "ChestA", "Chapter 1", "Health")
fnAddToList("PlainsC",                "ChestD", "Chapter 1", "Skill")
fnAddToList("PlainsNW",               "ChestA", "Chapter 1", "Health")
fnAddToList("SaltFlats",              "ChestA", "Chapter 1", "Initiative")
fnAddToList("TrannadarTradingPost",   "ChestB", "Chapter 1", "Accuracy")
fnAddToList("QuantirManseBasementE",  "ChestF", "Chapter 1", "Initiative")
fnAddToList("QuantirManseBasementW",  "ChestA", "Chapter 1", "Evade")
fnAddToList("QuantirManseSecretExit", "ChestA", "Chapter 1", "Health")
fnAddToList("StarfieldSwampB",        "ChestE", "Chapter 1", "Skill")
fnAddToList("TrafalNW",               "ChestB", "Chapter 1", "Skill")
fnAddToList("TrapBasementC",          "ChestA", "Chapter 1", "Health")
fnAddToList("TrapBasementE",          "ChestB", "Chapter 1", "Attack")
fnAddToList("TrapBasementH",          "ChestA", "Chapter 1", "Health")

--Chapter 2 Catalysts.
--fnAddToList("ArbonnePlainsD",         "ChestB", "Chapter 2", "Accuracy")
--Pending

--Chapter 5 Catalysts.
fnAddToList("RegulusBiolabsAlphaC",     "Chest D", "Chapter 5", "Evade")
fnAddToList("RegulusBiolabsAmphibianE", "ChestA",  "Chapter 5", "Accuracy")
fnAddToList("RegulusBiolabsAmphibianE", "ChestB",  "Chapter 5", "Accuracy")
fnAddToList("RegulusBiolabsAmphibianE", "ChestC",  "Chapter 5", "Accuracy")
fnAddToList("RegulusBiolabsDatacoreD",  "Chest C", "Chapter 5", "Evade")
fnAddToList("RegulusBiolabsGeneticsC",  "Chest B", "Chapter 5", "Evade")
fnAddToList("RegulusCity198C",          "Chest A", "Chapter 5", "Health")
fnAddToList("RegulusCryoC",             "Chest A", "Chapter 5", "Health")
fnAddToList("RegulusCryoLowerC",        "ChestB",  "Chapter 5", "Skill")
fnAddToList("RegulusCryoPowerCoreD",    "Chest A", "Chapter 5", "Skill")
fnAddToList("RegulusExteriorTRNB",      "ChestA",  "Chapter 5", "Skill")
fnAddToList("RegulusExteriorWB",        "Chest H", "Chapter 5", "Health")
fnAddToList("RegulusLRTEA",             "Chest A", "Chapter 5", "Health")
fnAddToList("RegulusLRTHB",             "ChestA",  "Chapter 5", "Skill")
fnAddToList("RegulusManufactoryG",      "ChestC",  "Chapter 5", "Skill")
fnAddToList("SprocketCityA",            "Chest A", "Chapter 5", "Health")
fnAddToList("SerenityObservatoryE",     "Chest A", "Chapter 5", "Skill")

-- |[Store]|
--Setup.
DL_AddPath("Root/Variables/NewChapterPlus/Chests/")
local iTotalStoredOpen = 0

--Catalyst counts.
local iCatalystCounts = {}
iCatalystCounts[gciCatalyst_Health] = 0
iCatalystCounts[gciCatalyst_Attack] = 0
iCatalystCounts[gciCatalyst_Initiative] = 0
iCatalystCounts[gciCatalyst_Evade] = 0
iCatalystCounts[gciCatalyst_Accuracy] = 0
iCatalystCounts[gciCatalyst_Skill] = 0

--Iterate across the list and store the open state of the chests.
for i = 1, #saList, 1 do
    
    --Check if the chest variable exists.
    local sChestPath = "Root/Variables/Chests/" .. saList[i].sLevelName .. "/" .. saList[i].sChestName
    local bExists = VM_Exists(sChestPath)
    if(bExists) then
        
        --Debug.
        --io.write("Chest " .. sChestPath .. " was opened.\n")
        
        --Create a variable. This will flag the chest as already opened.
        local sVariable = string.format("sChestUnlock%03i", iTotalStoredOpen)
        VM_SetVar("Root/Variables/NewChapterPlus/Chests/" .. sVariable, "S", sChestPath)
        
        --Increment the "current" count of the associated catalyst, if and only if it's the current chapter in play.
        if(saList[i].sChapter == "Chapter 2") then
            if(saList[i].sCatalystType == "Health") then
                iCatalystCounts[gciCatalyst_Health] = iCatalystCounts[gciCatalyst_Health] + 1
            elseif(saList[i].sCatalystType == "Attack") then
                iCatalystCounts[gciCatalyst_Attack] = iCatalystCounts[gciCatalyst_Attack] + 1
            elseif(saList[i].sCatalystType == "Initiative") then
                iCatalystCounts[gciCatalyst_Initiative] = iCatalystCounts[gciCatalyst_Initiative] + 1
            elseif(saList[i].sCatalystType == "Dodge" or saList[i].sCatalystType == "Evade") then
                iCatalystCounts[gciCatalyst_Evade] = iCatalystCounts[gciCatalyst_Evade] + 1
            elseif(saList[i].sCatalystType == "Accuracy") then
                iCatalystCounts[gciCatalyst_Accuracy] = iCatalystCounts[gciCatalyst_Accuracy] + 1
            elseif(saList[i].sCatalystType == "Skill") then
                iCatalystCounts[gciCatalyst_Skill] = iCatalystCounts[gciCatalyst_Skill] + 1
            end
        end
        
        --Increment.
        iTotalStoredOpen = iTotalStoredOpen + 1
    else
        --io.write("Chest " .. sChestPath .. " was not opened.\n")
    end
end
--io.write("Total number of opened chests: " .. iTotalStoredOpen .. "\n")

-- |[Clear]|
--Purge the chests part of the datalibrary.
DL_Purge("Root/Variables/Chests/", false)

-- |[Restore]|
--Now iterate back across the stored chests and write their variables.
for i = 0, iTotalStoredOpen - 1, 1 do
    
    --Get variables.
    local sVariable = string.format("sChestUnlock%03i", i)
    local sChestPath = VM_GetVar("Root/Variables/NewChapterPlus/Chests/" .. sVariable, "S")
    
    --Add the path to make sure.
    DL_AddPath(sChestPath)
    VM_SetVar(sChestPath, "N", 0.0)
    --io.write("Marked chest " .. sChestPath .. " as open.\n")
end

-- |[ ==================================== Catalyst Handling =================================== ]|
--The player retains catalysts for this chapter and from other chapters. The number of "current"
-- catalysts needs to be based on which chests were open.
AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Health,     iCatalystCounts[gciCatalyst_Health])
AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Attack,     iCatalystCounts[gciCatalyst_Attack])
AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Initiative, iCatalystCounts[gciCatalyst_Initiative])
AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Evade,      iCatalystCounts[gciCatalyst_Evade])
AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Accuracy,   iCatalystCounts[gciCatalyst_Accuracy])
AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Skill,      iCatalystCounts[gciCatalyst_Skill])

-- |[ ===================================== Topic Handling ===================================== ]|
WD_SetProperty("Set All Topics To Default")

-- |[ ==================================== Special Instances =================================== ]|
--Reset monsters. Execute a rest action.
AM_SetProperty("Execute Rest")

--Remove all special keys/items.
--AdInv_SetProperty("Remove Item", "Cultist Key")
