-- |[ ====================================== Actor Setup ======================================= ]|
-- |[Function Reference]|
--Helper functions are found in Games/AdventureMode/Classes/DiaHelper/
-- Function lines are provided here as reference. See Games/AdventureMode/Classes/DiaHelper/DiaHelperActor.lua
-- for more on how these work.
--DiaHelper:fnActorSetName(psName)
--DiaHelper:fnActorSetVoice(psVoice)
--DiaHelper:fnActorAddAlias(psAlias, psDisplayName)
--DiaHelper:fnActorSetAliases(psaAliasList, psaDisplayList)
--DiaHelper:fnActorAddEmotion(psEmotion, psImagePath, pbIsNormalWidth)
--DiaHelper:fnActorSetEmotions(pzaEmotionList)
--DiaHelper:fnActorUploadAndClear()
--DiaHelper:fnCreateSimpleActor(psActorName, psVoiceName, psPathPattern, pbIsNormalWidth, psaAliasList, psaEmotionList, psaAliasRemapList)

--Note: The first alias for a character should always be that character's name. This is for translations,
-- as aliases can have display names that allow the translator to change a character's name in the dialogue.
-- Even if a character has no other aliases, they should always have their name in the list.
--The exception to this rule is if the character never speaks under their internal name, which some of the
-- generic or monster NPCs do.

-- |[Common Variables]|
--Human-readable versions of special name sets.
local saNeutralIsPath       = {"NEUTRALISPATH"}		--psPathPattern will be the direct path to the emotion image, and the emotion will be "Neutral".
local saNoAliasDisplayNames = nil 					--Alias display names are typically only used for translations.

-- |[ ================================= Party/Important Actors ================================= ]|
-- |[Sanya]|
local saSanyaAliases  = {"Sanya"}
local saSanyaEmotions = {"Neutral", "Happy", "Blush", "Smirk", "Sad", "Surprised", "Offended", "Cry", "Laugh", "Angry"}
DiaHelper:fnCreateSimpleActor("Sanya", "Voice|Sanya", "Root/Images/Portraits/SanyaDialogue/", gbNormalWidth, saSanyaAliases, saSanyaEmotions, saNoAliasDisplayNames)

-- |[Izuna]|
local saIzunaAliases  = {"Izuna", "Weirdo"}
local saIzunaEmotions = {"Neutral", "Happy", "Cry", "Smirk", "Explain", "Jot", "Ugh", "Trip", "Blush"}
DiaHelper:fnCreateSimpleActor("Izuna", "Voice|Izuna", "Root/Images/Portraits/IzunaMikoDialogue/", gbNormalWidth, saIzunaAliases, saIzunaEmotions, saNoAliasDisplayNames)

-- |[Empress]|
local saEmpressAliases  = {"Empress", "Dragon"}
local saEmpressEmotions = {"Neutral"}
DiaHelper:fnCreateSimpleActor("Empress", "Voice|Empress", "Root/Images/Portraits/EmpressDragonDialogue/", gbNormalWidth, saEmpressAliases, saEmpressEmotions, saNoAliasDisplayNames)

-- |[Zeke]|
local saZekeAliases  = {"Zeke"}
local saZekeEmotions = {"Neutral"}
DiaHelper:fnCreateSimpleActor("Zeke", "Voice|Zeke", "Root/Images/Portraits/ZekeGoatDialogue/", gbNormalWidth, saZekeAliases, saZekeEmotions, saNoAliasDisplayNames)

-- |[Odar]|
local saOdarAliases  = {"Odar"}
local saOdarEmotions = {"Neutral"}
DiaHelper:fnCreateSimpleActor("Odar", "Voice|Odar", "Root/Images/Portraits/OdarPrinceDialogue/", gbNormalWidth, saOdarAliases, saOdarEmotions, saNoAliasDisplayNames)

-- |[Miho]|
--TODO

-- |[Setup]|
--Alias listings.
local saNoAliases    = {"NOALIAS"}
local saNoEmotions      = {"Neutral"}

-- |[ ====================================== Major Actors ====================================== ]|
--Refers to actors who are important enough to usually warrant their own sprite, but not a full emotion set.
-- May also refer to actors who play a big role but only in one section of the game.

-- |[Many-Chapter]|
--Cassandra is created specially, since her Neutral emotion is her human form. She gets transformed a lot.
local saCassandraAliases  = {"Cassandra", "Lady", "Fang"}
local saCassandraEmotions = {"Neutral", "Werecat", "Golem", "Raptor"}
DiaHelper:fnCreateSimpleActor("Cassandra", "Voice|GenericF13", "Root/Images/Portraits/Cassandra/", gbNormalWidth, saCassandraAliases,  saCassandraEmotions, saNoAliasDisplayNames)


DialogueActor_Create("Cassandra")
	DialogueActor_SetProperty("Add Alias", "Lady")
	DialogueActor_SetProperty("Add Alias", "Cassandra")
	DialogueActor_SetProperty("Add Alias", "Fang")
	DialogueActor_SetProperty("Add Emotion", "Neutral", "Root/Images/Portraits/CassandraDialogue/Human",   true)
	DialogueActor_SetProperty("Add Emotion", "Werecat", "Root/Images/Portraits/CassandraDialogue/Werecat", true)
	DialogueActor_SetProperty("Add Emotion", "Golem",   "Root/Images/Portraits/CassandraDialogue/Golem",   true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "Cassandra", "Voice|GenericF13")

-- |[Chapter 2]|
-- Mia, Izuna's editor.
DialogueActor_Create("Mia")
	DialogueActor_SetProperty("Add Alias", "Mia")
	DialogueActor_SetProperty("Add Emotion", "Neutral",  "Root/Images/Portraits/Mia/Neutral",  true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "Mia", "Voice|Mia")

-- Miso, a brilliant craftsfox.
DialogueActor_Create("Miso")
	DialogueActor_SetProperty("Add Alias", "Miso")
	DialogueActor_SetProperty("Add Emotion", "Neutral",  "Root/Images/Portraits/Miso/Neutral",  true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "Miso", "Voice|Miso")

--Yukina, the village elder.
DialogueActor_Create("Yukina")
	DialogueActor_SetProperty("Add Alias", "Yukina")
	DialogueActor_SetProperty("Add Emotion", "Neutral", "Root/Images/Portraits/Yukina/Neutral", false)
	DialogueActor_SetProperty("Add Emotion", "Upset",   "Root/Images/Portraits/Yukina/Upset",   false)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "Yukina", "Voice|Yukina")

--[=[
--Sharelock!
DialogueActor_Create("Sharelock")
	DialogueActor_SetProperty("Add Alias", "Robot")
	DialogueActor_SetProperty("Add Alias", "Share;Lock")
	DialogueActor_SetProperty("Add Emotion", "Neutral",  "Root/Images/Portraits/Sharelock/Neutral",  true)
	DialogueActor_SetProperty("Add Emotion", "Thinking", "Root/Images/Portraits/Sharelock/Thinking", true)
	DialogueActor_SetProperty("Add Emotion", "AhHa",     "Root/Images/Portraits/Sharelock/AhHa",     true)
	DialogueActor_SetProperty("Add Emotion", "Sheepish", "Root/Images/Portraits/Sharelock/Sheepish", true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "Sharelock", "Voice|Sharelock")

--Miho!
DialogueActor_Create("Miho")
	DialogueActor_SetProperty("Add Alias", "Miho")
	DialogueActor_SetProperty("Add Alias", "Kitsune")
	DialogueActor_SetProperty("Add Emotion", "Neutral",   "Root/Images/Portraits/Miho/Neutral",   true)
	DialogueActor_SetProperty("Add Emotion", "Angry",     "Root/Images/Portraits/Miho/Angry",     true)
	DialogueActor_SetProperty("Add Emotion", "Drunk",     "Root/Images/Portraits/Miho/Drunk",     true)
	DialogueActor_SetProperty("Add Emotion", "VeryDrunk", "Root/Images/Portraits/Miho/VeryDrunk", true)
	DialogueActor_SetProperty("Add Emotion", "Rubber",    "Root/Images/Portraits/Miho/Rubber",    true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "Miho", "Voice|Miho")
]=]

--Lady Morus
DialogueActor_Create("Morus")
	DialogueActor_SetProperty("Add Alias", "Morus")
	DialogueActor_SetProperty("Add Alias", "Sevavi")
	DialogueActor_SetProperty("Add Emotion", "Neutral", "Root/Images/Portraits/Ch2Generic/Morus|Neutral", true)
	DialogueActor_SetProperty("Add Emotion", "Smirk",   "Root/Images/Portraits/Ch2Generic/Morus|Smirk",   true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "Morus", "Voice|Morus")

--Tetra Arulente
DialogueActor_Create("Tetra")
	DialogueActor_SetProperty("Add Alias", "Tetra")
	DialogueActor_SetProperty("Add Alias", "Sevavi")
	DialogueActor_SetProperty("Add Emotion", "Neutral", "Root/Images/Portraits/Ch2Generic/Tetra|Neutral", true)
	DialogueActor_SetProperty("Add Emotion", "Smirk",   "Root/Images/Portraits/Ch2Generic/Tetra|Smirk",   true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "Tetra", "Voice|Tetra")

-- |[ ====================================== Minor Actors ====================================== ]|
--NPCs or Enemy actors, usually play small roles many times. Some have unique dialogue sprites because their enemy field
-- variants are too big to fit on the dialogue screen.

-- |[Humans]|
fnCreateDialogueActor("ManA",    "Voice|GenericM02", "Root/Images/Portraits/NPCs/MercM",                false, saNoAliases,        {"NEUTRALISPATH"})
fnCreateDialogueActor("MercF",   "Voice|GenericF11", "Root/Images/Portraits/NPCs/MercF",                true,  {"Soldier"},        {"NEUTRALISPATH"})
fnCreateDialogueActor("HumanF0", "Voice|GenericF03", "Root/Images/Portraits/NPCs/HumanNPCF0",           true,  {"Human"},          {"NEUTRALISPATH"})
fnCreateDialogueActor("HumanF1", "Voice|GenericF02", "Root/Images/Portraits/NPCs/HumanNPCF1",           true,  {"Human", "Norah"}, {"NEUTRALISPATH"})
fnCreateDialogueActor("HumanF2", "Voice|GenericF00", "Root/Images/Portraits/NPCs/HumanNPCF0",           true,  {"Human"},          {"NEUTRALISPATH"})
fnCreateDialogueActor("HumanF3", "Voice|GenericF01", "Root/Images/Portraits/NPCs/HumanNPCF0",           true,  {"Human"},          {"NEUTRALISPATH"})
fnCreateDialogueActor("HumanM0", "Voice|GenericM01", "Root/Images/Portraits/NPCs/HumanNPCM0",           true,  {"Human"},          {"NEUTRALISPATH"})
fnCreateDialogueActor("HumanM1", "Voice|GenericM00", "Root/Images/Portraits/NPCs/HumanNPCM1",           true,  {"Human"},          {"NEUTRALISPATH"})
fnCreateDialogueActor("HumanM2", "Voice|GenericM02", "Root/Images/Portraits/NPCs/HumanNPCM1",           true,  {"Human"},          {"NEUTRALISPATH"})
fnCreateDialogueActor("HumanM3", "Voice|GenericM03", "Root/Images/Portraits/NPCs/HumanNPCM1",           true,  {"Human"},          {"NEUTRALISPATH"})
fnCreateDialogueActor("Takahn",  "Voice|Takahn",     "Root/Images/Portraits/Ch2Generic/Takahn|Neutral", true,  {"Elf", "Takahn"},  {"NEUTRALISPATH"})

-- |[Bunnies]|
fnCreateDialogueActor("Angelface",      "Voice|Angelface",      "Root/Images/Portraits/Ch2Generic/Angelface|Neutral",     true, {"Boss", "Angelface"}, {"NEUTRALISPATH"})
fnCreateDialogueActor("Bunny",          "Voice|Bunny",          "Root/Images/Portraits/Ch2Generic/BunnySmuggler|Neutral", true, {"Bunny", "Smuggler"}, {"NEUTRALISPATH"})
fnCreateDialogueActor("CrowbarChanMob", "Voice|CrowbarChan",    "Root/Images/Portraits/CrowbarChanMob/Neutral",           true, {"Crowbar-Chan", "CC"}, {"NEUTRALISPATH"})

-- |[Alraunes]|
fnCreateDialogueActor("Alraune",  "Voice|Alraune",  "Root/Images/Portraits/Enemies/Alraune", true, {"Alraune"},  {"NEUTRALISPATH"})
fnCreateDialogueActor("Marigold", "Voice|Marigold", "Root/Images/Portraits/Enemies/Alraune", true, {"Alraune", "Marigold"},  {"NEUTRALISPATH"})

-- |[Kitsunes]|
--"Named" versions, these are characters but they use generic portraits.
fnCreateDialogueActor("KitsuneA",  "Voice|GenericF03", "Root/Images/Portraits/Ch2Generic/KitsuneA|Neutral", true, {"Kitsune", "Fox", "Farmer", "Melody"}, {"NEUTRALISPATH"})
fnCreateDialogueActor("KitsuneB",  "Voice|GenericF05", "Root/Images/Portraits/Ch2Generic/KitsuneA|Neutral", true, {"Kitsune", "Fox", "Farmer", "Aria"}, {"NEUTRALISPATH"})
fnCreateDialogueActor("KitsuneC",  "Voice|GenericF07", "Root/Images/Portraits/Ch2Generic/KitsuneA|Neutral", true, {"Kitsune", "Fox", "Warrior", "Sonoko"}, {"NEUTRALISPATH"})
fnCreateDialogueActor("KitsuneD",  "Voice|GenericF01", "Root/Images/Portraits/Ch2Generic/KitsuneA|Neutral", true, {"Kitsune", "Fox"}, {"NEUTRALISPATH"})
fnCreateDialogueActor("Chikage",   "Voice|GenericF03", "Root/Images/Portraits/Ch2Generic/Chikage|Neutral",  true, {"Kitsune", "Fox", "Warrior"}, {"NEUTRALISPATH"})

-- |[Harpies]|
fnCreateDialogueActor("HarpyRecruit", "Voice|HarpyA",    "Root/Images/Portraits/Ch2Generic/HarpyRecruit|Neutral", true, {"Harpy", "Recruit", "Cook"}, {"NEUTRALISPATH"})
fnCreateDialogueActor("HarpyOfficer", "Voice|HarpyB",    "Root/Images/Portraits/Ch2Generic/HarpyOfficer|Neutral", true, {"Harpy", "Officer"},         {"NEUTRALISPATH"})
fnCreateDialogueActor("HarpyC",       "Voice|HarpyC",    "Root/Images/Portraits/Ch2Generic/HarpyRecruit|Neutral", true, {"Harpy", "Recruit"},         {"NEUTRALISPATH"})
fnCreateDialogueActor("HarpyD",       "Voice|HarpyD",    "Root/Images/Portraits/Ch2Generic/HarpyRecruit|Neutral", true, {"Harpy", "Recruit"},         {"NEUTRALISPATH"})
fnCreateDialogueActor("Esmerelda",    "Voice|Esmerelda", "Root/Images/Portraits/Ch2Generic/Esmerelda|Neutral",    true, {"Harpy", "Esmerelda"},       {"NEUTRALISPATH"})

-- |[Sevavi]|
fnCreateDialogueActor("Sevavi", "Voice|GenericF01", "Root/Images/Portraits/Ch2Generic/Sevavi|Neutral", true, {"Statue", "Sevavi"},  {"NEUTRALISPATH"})

-- |[Werecats]|
--Never appears in major dialogue.
fnCreateDialogueActor("Werecat", "Voice|Werecat", "Root/Images/Portraits/NPCs/MercM", false, saNoAliases, {"NEUTRALISPATH"})
