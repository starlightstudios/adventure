-- |[ ===================================== Audio Loading ====================================== ]|
--Loads audio for this chapter.
local sBasePath = gsRoot .. "Audio/Music/"

-- |[Ambient Tracks]|
local sAmbientPath = sBasePath .. "Ambient Tracks/"
AudioManager_RegisterAdv("Windy Mountain",  sAmbientPath .. "Windy Mountain.ogg",  "Stream|Music|Loop:" ..  "2.000:" ..  "50.562")
AudioManager_RegisterAdv("Cave Quiet",      sAmbientPath .. "Cave Quiet.ogg",      "Stream|Music|Loop:" ..  "3.000:" .. "120.743")

-- |[Battle Themes]|
local sBattlePath = sBasePath .. "Battle Themes/"
AudioManager_RegisterAdv("BattleThemeSanya",     sBattlePath .. "Battle Theme Sanya.ogg",    "Stream|Music|Loop:" .. "11.781:" ..  "83.822")
AudioManager_RegisterAdv("PuzzleFight",          sBattlePath .. "PuzzleParty.ogg",           "Stream|Music|Loop:" ..  "3.209:" ..  "63.204")

-- |[Character Themes]|
local sCharThemePath = sBasePath .. "Character Themes/"
AudioManager_RegisterAdv("IzunasTheme",      sCharThemePath .. "Izunas Theme.ogg",            "Stream|Music|Loop:" ..  "0.637:" ..  "30.000")
AudioManager_RegisterAdv("SanyasTheme",      sCharThemePath .. "Sanyas Theme.ogg",            "Stream|Music|Loop:" ..  "1.864:" ..  "88.116")
AudioManager_RegisterAdv("Empress Maj",      sCharThemePath .. "Empress Major.ogg",           "Stream|Music|Loop:" ..  "9.000:" ..  "68.881")

-- |[Layered Tracks]|
local sLayeredPath = sBasePath .. "Layered Tracks/"

-- |[Misc Tracks]|
local sMiscPath = sBasePath .. "Misc Tracks/"
AudioManager_RegisterAdv("Meeting Empress",sMiscPath .. "Meeting Empress.ogg",  "Stream|Music|Loop:" ..   "2.000:" .. "128.480")

-- |[World Themes]|
local sWorldPath = sBasePath .. "World Themes/"
AudioManager_RegisterAdv("Choking Clouds",       sWorldPath .. "Choking Clouds.ogg",         "Stream|Music|Loop:" .. "91.003:" .. "170.976")
AudioManager_RegisterAdv("SerenityObservatory",  sWorldPath .. "SerenityObservatory.ogg",    "Stream|Music|Loop:" .. "14.983:" ..  "60.001")
AudioManager_RegisterAdv("TrafalCave",           sWorldPath .. "Trafal Cave.ogg",            "Stream|Music|Loop:" ..  "2.662:" .. "236.343")
AudioManager_RegisterAdv("TrafalDungeon",        sWorldPath .. "Trafal Dungeon.ogg",         "Stream|Music|Loop:" ..  "0.000:" ..  "97.970")
AudioManager_RegisterAdv("PuzzleTheme",          sWorldPath .. "PuzzleTheme.ogg",            "Stream|Music|Loop:" ..  "1.744:" ..  "97.742")
AudioManager_RegisterAdv("KitsuneVillage",       sWorldPath .. "Kitsune.ogg",                "Stream|Music|Loop:" ..  "4.504:" ..  "90.900")
AudioManager_RegisterAdv("Smuggling Operations", sWorldPath .. "Smuggling Operations.ogg",   "Stream|Music|Loop:" .. "43.176:" ..  "88.328")