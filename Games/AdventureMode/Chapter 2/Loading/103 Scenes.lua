-- |[ ==================================== Chapter 2 Scenes ==================================== ]|
--Scenes for chapter 2. This includes TF scenes and runestone flashes.

-- |[Streaming List]|
--Create an "Everything" list. All scene images are put in this list that aren't in another list,
-- and ordered to load immediately.
--Because scene images tend to be really big, this list *should* be empty in an optimized program.
local sEverythingList = "Chapter 2 Scenes"
fnCreateDelayedLoadList(sEverythingList)

-- |[ =================================== Autoloaded Scenes ==================================== ]|
--All scenes are done using a delayed load list created from an autoloader. The data is not loaded
-- but the names are stored for when the scene actually executes.
SLF_Open(gsaDatafilePaths.sScnCh2Major)

--Introduction.
local sTFCluster = "Chapter 2 Introduction"
fnCreateDelayedLoadList(sTFCluster)
fnExecAutoloadDelayed("AutoLoad|Adventure|Ch2_Introduction", sTFCluster)

--Kissing Scene.
sTFCluster = "Chapter 2 Kissing Scene"
fnCreateDelayedLoadList(sTFCluster)
fnExecAutoloadDelayed("AutoLoad|Adventure|Ch2_Kiss", sTFCluster)

--Bunny TF.
sTFCluster = "Chapter 2 Sanya Bunny TF"
fnCreateDelayedLoadList(sTFCluster)
fnExecAutoloadDelayed("AutoLoad|Adventure|Ch2_BunnyTF", sTFCluster)

--Sevavi TF.
sTFCluster = "Chapter 2 Sanya Sevavi TF"
fnCreateDelayedLoadList(sTFCluster)
fnExecAutoloadDelayed("AutoLoad|Adventure|Ch2_SevaviTF", sTFCluster)

--Harpy TF.
sTFCluster = "Chapter 2 Sanya Harpy TF"
fnCreateDelayedLoadList(sTFCluster)
fnExecAutoloadDelayed("AutoLoad|Adventure|Ch2_HarpyTF", sTFCluster)

--Harpy Feet TF.
sTFCluster = "Chapter 2 Sanya Harpy Feet TF"
fnCreateDelayedLoadList(sTFCluster)
fnExecAutoloadDelayed("AutoLoad|Adventure|Ch2_HarpyFeetTF", sTFCluster)

-- |[ ======================================== Clean Up ======================================== ]|
-- |[Load Issue]|
--Order everything to enqueue loading.
fnLoadDelayedBitmapsFromList(sEverythingList, gciDelayedLoadLoadAtEndOfTick)

-- |[Debug]|
if(false) then
    fnPrintDelayedBitmapList(sEverythingList)
end

-- |[Close]|
SLF_Close()
