-- |[ ================================== Chapter 2 Portraits =================================== ]|
--Loads all portraits for chapter 2.
Debug_PushPrint(gbLoadDebug, "Beginning Portrait loader.\n")

-- |[All Portraits List]|
--Create an "Everything" list. All delayed images not in another list get put in this list and loaded
-- as soon as possible, but not during the load sequence.
local sEverythingList = "Chapter 2 Portraits"
fnCreateDelayedLoadList(sEverythingList)

--List used for portraits that load if gbUnloadDialoguePortraitsWhenDialogueCloses is true, and don't when it's false.
local sCh2DialogueList = "Chapter 2 Dialogue"
fnCreateDelayedLoadList(sCh2DialogueList)

-- |[Dialogue Portraits Flag]|
--Setup. Allows suppression of streaming warnings if desired for some reason.
local bShowWarningsForDialoguePortraits = true
local bShowWarningsForCombatPortraits = true

-- |[Constants]|
local sMetaAutoload = gsaDatafilePaths.sChapter2PortraitAutoload

-- |[ ================================ Meta-Autoloader Function ================================ ]|
--Used for autoloader files that contain instructions to jump to other files. This re-loads the meta
-- file, then executes the autoload to the given costume. Basically compressed two lines into one.
local function fnMetaAutoload(psAutoloadSLF, psAutoloadLump, psCostumeDestination)
    SLF_Open(psAutoloadSLF)
    fnExecAutoloadDelayed(psAutoloadLump, psCostumeDestination)
end

-- |[ ======================================= Autoloader ======================================= ]|
--Order everything on these two portrait lists to load. One is the "load immediately", the other is
-- "load as needed". This distinction is done at compression time.
SLF_Open(gsaDatafilePaths.sChapter2PortraitAutoload)
fnExecAutoloadDelayed("AutoLoad|Adventure|Portrait_Ch2_Immediate", sEverythingList)

--Re-open the autoload file. The previous call may have changed which file is open.
SLF_Open(gsaDatafilePaths.sChapter2PortraitAutoload)
fnExecAutoloadDelayed("AutoLoad|Adventure|Portrait_Ch2_AsNeeded", sCh2DialogueList)

-- |[ =================================== Costume Autoloader =================================== ]|
--Characters who have changeable costumes register their images and then let the costume handlers load
-- and unload as needed. Most playable characters and some non-playable ones use this.

-- |[ ====== Sanya ======= ]|
--Loading Lists.
fnCreateDelayedLoadList("Sanya Costume Human")
fnCreateDelayedLoadList("Sanya Costume HumanNoRifle")
fnCreateDelayedLoadList("Sanya Costume HumanWerebatA")
fnCreateDelayedLoadList("Sanya Costume HumanWerebatB")
fnCreateDelayedLoadList("Sanya Costume HumanWerebatB")
fnCreateDelayedLoadList("Sanya Costume Human Combat")
fnCreateDelayedLoadList("Sanya Costume HumanNoRifle Combat")
fnCreateDelayedLoadList("Sanya Costume Kitsune")
fnCreateDelayedLoadList("Sanya Costume Kitsune Combat")
fnCreateDelayedLoadList("Sanya Costume Sevavi")
fnCreateDelayedLoadList("Sanya Costume SevaviNoRifle")
fnCreateDelayedLoadList("Sanya Costume Sevavi Combat")
fnCreateDelayedLoadList("Sanya Costume SevaviNoRifle Combat")
fnCreateDelayedLoadList("Sanya Costume Werebat")
fnCreateDelayedLoadList("Sanya Costume Werebat Combat")
fnCreateDelayedLoadList("Sanya Costume Bunny")
fnCreateDelayedLoadList("Sanya Costume Bunny Combat")
fnCreateDelayedLoadList("Sanya Costume Harpy")
fnCreateDelayedLoadList("Sanya Costume Harpy Combat")

--Lua global storage.
gsaSanyaDialogueList = {}
gsaSanyaDialogueList[1] = "Sanya Costume Human"
gsaSanyaDialogueList[2] = "Sanya Costume HumanNoRifle"
gsaSanyaDialogueList[3] = "Sanya Costume HumanWerebatA"
gsaSanyaDialogueList[4] = "Sanya Costume HumanWerebatB"
gsaSanyaDialogueList[5] = "Sanya Costume Kitsune"
gsaSanyaDialogueList[6] = "Sanya Costume Sevavi"
gsaSanyaDialogueList[7] = "Sanya Costume SevaviNoRifle"
gsaSanyaDialogueList[8] = "Sanya Costume Werebat"
gsaSanyaDialogueList[9] = "Sanya Costume Bunny"
gsaSanyaDialogueList[10]= "Sanya Costume Harpy"
gsaSanyaCombatList = {}
gsaSanyaCombatList[1] = "Sanya Costume Human Combat"
gsaSanyaCombatList[1] = "Sanya Costume HumanNoRifle Combat"
gsaSanyaCombatList[2] = "Sanya Costume Kitsune Combat"
gsaSanyaCombatList[3] = "Sanya Costume Sevavi Combat"
gsaSanyaCombatList[4] = "Sanya Costume SevaviNoRifle Combat"
gsaSanyaCombatList[5] = "Sanya Costume Werebat Combat"
gsaSanyaCombatList[6] = "Sanya Costume Bunny Combat"
gsaSanyaCombatList[7] = "Sanya Costume Harpy Combat"

--Execute load.
fnMetaAutoload(sMetaAutoload, "AutoLoad|Adventure|Portrait_Ch2_Emt_Sanya_Human",         "Sanya Costume Human")
fnMetaAutoload(sMetaAutoload, "AutoLoad|Adventure|Portrait_Ch2_Emt_Sanya_HumanNoRifle",  "Sanya Costume HumanNoRifle")
fnMetaAutoload(sMetaAutoload, "AutoLoad|Adventure|Portrait_Ch2_Emt_Sanya_HumanWerebatA", "Sanya Costume HumanWerebatA")
fnMetaAutoload(sMetaAutoload, "AutoLoad|Adventure|Portrait_Ch2_Emt_Sanya_HumanWerebatB", "Sanya Costume HumanWerebatB")
fnMetaAutoload(sMetaAutoload, "AutoLoad|Adventure|Portrait_Ch2_Emt_Sanya_Kitsune",       "Sanya Costume Kitsune")
fnMetaAutoload(sMetaAutoload, "AutoLoad|Adventure|Portrait_Ch2_Emt_Sanya_Sevavi",        "Sanya Costume Sevavi")
fnMetaAutoload(sMetaAutoload, "AutoLoad|Adventure|Portrait_Ch2_Emt_Sanya_SevaviNoRifle", "Sanya Costume SevaviNoRifle")
fnMetaAutoload(sMetaAutoload, "AutoLoad|Adventure|Portrait_Ch2_Emt_Sanya_Werebat",       "Sanya Costume Werebat")

fnMetaAutoload(sMetaAutoload, "AutoLoad|Adventure|Portrait_Ch2_Cbt_Sanya_Human",         "Sanya Costume Human Combat")
fnMetaAutoload(sMetaAutoload, "AutoLoad|Adventure|Portrait_Ch2_Cbt_Sanya_HumanNoRifle",  "Sanya Costume HumanNoRifle Combat")
fnMetaAutoload(sMetaAutoload, "AutoLoad|Adventure|Portrait_Ch2_Cbt_Sanya_Kitsune",       "Sanya Costume Kitsune Combat")
fnMetaAutoload(sMetaAutoload, "AutoLoad|Adventure|Portrait_Ch2_Cbt_Sanya_Sevavi",        "Sanya Costume Sevavi Combat")
fnMetaAutoload(sMetaAutoload, "AutoLoad|Adventure|Portrait_Ch2_Cbt_Sanya_SevaviNoRifle", "Sanya Costume SevaviNoRifle Combat")
fnMetaAutoload(sMetaAutoload, "AutoLoad|Adventure|Portrait_Ch2_Cbt_Sanya_Werebat",       "Sanya Costume Werebat Combat")
fnMetaAutoload(sMetaAutoload, "AutoLoad|Adventure|Portrait_Ch2_Cbt_Sanya_Bunny",         "Sanya Costume Bunny Combat")
fnMetaAutoload(sMetaAutoload, "AutoLoad|Adventure|Portrait_Ch2_Cbt_Sanya_Harpy",         "Sanya Costume Harpy Combat")

-- |[ ======= Izuna ====== ]|
--Loading Lists.
fnCreateDelayedLoadList("Izuna Costume Miko")
fnCreateDelayedLoadList("Izuna Costume Miko Combat")

--Lua global storage.
gsaIzunaDialogueList = {}
gsaIzunaDialogueList[1] = "Izuna Costume Miko"
gsaIzunaCombatList = {}
gsaIzunaCombatList[1] = "Izuna Costume Miko Combat"

--Execute load.
fnMetaAutoload(sMetaAutoload, "AutoLoad|Adventure|Portrait_Ch2_Emt_Izuna_Miko", "Izuna Costume Miko")
fnMetaAutoload(sMetaAutoload, "AutoLoad|Adventure|Portrait_Ch2_Cbt_Izuna",      "Izuna Costume Miko Combat")

-- |[ ======= Zeke ======= ]|
--Loading Lists.
fnCreateDelayedLoadList("Zeke Costume Goat")
fnCreateDelayedLoadList("Zeke Costume Goat Combat")

--Lua global storage.
gsaZekeDialogueList = {}
gsaZekeDialogueList[1] = "Zeke Costume Goat"
gsaZekeCombatList = {}
gsaZekeCombatList[1] = "Zeke Costume Goat Combat"

--Execute load.
fnMetaAutoload(sMetaAutoload, "AutoLoad|Adventure|Portrait_Ch2_Emt_Zeke_Goat", "Zeke Costume Goat")
fnMetaAutoload(sMetaAutoload, "AutoLoad|Adventure|Portrait_Ch2_Cbt_Zeke",      "Zeke Costume Goat Combat")

-- |[ ===== Empress ====== ]|
--Loading Lists.
fnCreateDelayedLoadList("Empress Costume Conquerer")
fnCreateDelayedLoadList("Empress Costume Conquerer Combat")

--Lua global storage.
gsaEmpressDialogueList = {}
gsaEmpressDialogueList[1] = "Empress Costume Conquerer"
gsaEmpressCombatList = {}
gsaEmpressCombatList[1] = "Empress Costume Conquerer Combat"

--Execute load.
fnMetaAutoload(sMetaAutoload, "AutoLoad|Adventure|Portrait_Ch2_Emt_Empress_Conquerer", "Empress Costume Conquerer")
fnMetaAutoload(sMetaAutoload, "AutoLoad|Adventure|Portrait_Ch2_Cbt_Empress",           "Empress Costume Conquerer Combat")

-- |[ ======= Odar ======= ]|
--Loading Lists.
fnCreateDelayedLoadList("Odar Costume Prince")
fnCreateDelayedLoadList("Odar Costume Prince Combat")

--Lua global storage.
gsaOdarDialogueList = {}
gsaOdarDialogueList[1] = "Odar Costume Prince"
gsaOdarCombatList = {}
gsaOdarCombatList[1] = "Odar Costume Prince Combat"

--Execute load.
fnMetaAutoload(sMetaAutoload, "AutoLoad|Adventure|Portrait_Ch2_Emt_Odar_Prince", "Odar Costume Prince")
fnMetaAutoload(sMetaAutoload, "AutoLoad|Adventure|Portrait_Ch2_Cbt_Odar",        "Odar Costume Prince Combat")

-- |[ ======================================== Finish Up ======================================= ]|
-- |[Reset Flag]|
--Default flag state is true.
Bitmap_SetNewBitmapNoHandleWarnings(true)

-- |[Issue Asset Stream]|
--If this flag is set, order all dialogue images that aren't linked to a costume to load.
if(gbUnloadDialoguePortraitsWhenDialogueCloses == false) then
    fnLoadDelayedBitmapsFromList(sCh2DialogueList, gciDelayedLoadLoadAtEndOfTick)
end

--Order all combat and unconditional images to load.
fnLoadDelayedBitmapsFromList(sEverythingList, gciDelayedLoadLoadAtEndOfTick)

-- |[Clean]|
SLF_Close()
