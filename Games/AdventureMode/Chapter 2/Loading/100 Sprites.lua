-- |[ =================================== Chapter 2 Loading ==================================== ]|
--This file loads assets unique to chapter 2. Subfiles are used where necessary.

--Modify the distance filters to keep everything pixellated. This is only for sprites.
local iNearest = DM_GetEnumeration("GL_NEAREST")
ALB_SetTextureProperty("MagFilter", iNearest)
ALB_SetTextureProperty("MinFilter", iNearest)
ALB_SetTextureProperty("Special Sprite Padding", true)

-- |[ ======================================= Autoloader ======================================= ]|
--Open the meta-autoloader.
SLF_Open(gsaDatafilePaths.sChapter2SpriteAutoload)
fnExecAutoload("AutoLoad|Adventure|Sprites_Ch2")

-- |[ ======================================== Clean Up ======================================== ]|
--Reset flags.
--Bitmap_DeactivateAtlasing()
ALB_SetTextureProperty("Restore Defaults")
ALB_SetTextureProperty("Special Sprite Padding", false)

--Close the slf file.
SLF_Close()
