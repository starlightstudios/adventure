-- |[ =================================== Chapter 2 Overlays =================================== ]|
--All of this is done via a meta-autoloader.
local sEverythingList = "Chapter 2 Overlays"
fnCreateDelayedLoadList(sEverythingList)

--Execute autoload.
SLF_Open(gsaDatafilePaths.sChapter2MapAutoload)
fnExecAutoloadDelayed("AutoLoad|Adventure|Maps_Ch2_Immediate", sEverythingList)

--Order everything to enqueue loading.
fnLoadDelayedBitmapsFromList(sEverythingList, gciDelayedLoadLoadAtEndOfTick)
