-- |[ ================================== Random Chest Handler ================================== ]|
--Called whenever a chest in chapter 2 has to be opened and roll a random item. Receives two arguments,
-- those being the level in which it was called, and the chest's name.
if(fnArgCheck(2) == false) then return end
local sLevelName = LM_GetScriptArgument(0)
local sChestName = LM_GetScriptArgument(1)

--Debug.
Debug_PushPrint(false, "Random generator for level " .. sLevelName .. " and chest " .. sChestName .. "\n")

-- |[ ====================================== Description ======================================= ]|
--The areas of chapter 2 are broken into several big random loot groupings: Northwoods, Westwoods,
-- The Old Capital, and The Ice Spires. The Underground versions of these areas have the same loot tables.
--A single loot table is rolled against which then selects a sub-table. The broad tiers 0-5 are
-- selected, with weight towards the lower tiers. Each area adds to the roll basis for selecting a tier.
--For example, it is not on the Northwoods loot table for loot that is more common in The Ice Spires.
--In addition, each green chest the player picks up adds one to the global green chest counter.
-- This bonus is applied to selecting which tier, so it is theoretically possible to get items normally
-- out of range, but it will take a lot of luck (or a lot of green chests).

--The nominal loot tables are:
--Tier 0: 30% chance. Junk, sell this.
--Tier 1: 30% chance. Crafting materials.
--Tier 2: 10% chance. Gems.
--Tier 3: 10% chance. Basic equipment, Northwoods hunting rewards.
--Tier 4:  5% chance. Intermediate equipment, Westwoods/Capital hunting rewards.
--Tier 5:  5% chance. Advanced equipment, Ice Spires hunting rewards.

--Note that these do not necessarily add up to 100%, as more tiers will be added later, and the ranges
-- are adjusted anyway.

--See the adamantite note near [Tier A] for more on how Adamantite is rolled.

-- |[ ======================================= Generation ======================================= ]|
--On the first pass, build the loot tables.
if(gzaLootTables == nil) then
    
    -- |[Base]|
    gzaLootTables = {}
    
    -- |[Tier A]|
    --The adamantite tier. Has special logic, in that there is a flat 15% chance that any chest will
    -- be adamantite, and then the loot bonus applies to the adamantite roll within this list.
    local zTierA = {}
    zTierA.sInternalName = "Adamantite"
    table.insert(zTierA, {"Adamantite Powder x1", 300})
    table.insert(zTierA, {"Adamantite Powder x2",  50})
    table.insert(zTierA, {"Adamantite Flakes x1", 200})
    table.insert(zTierA, {"Adamantite Flakes x2",  50})
    table.insert(zTierA, {"Adamantite Shard x1",  100})
    table.insert(zTierA, {"Adamantite Piece x1",   10})
    
    -- |[Tier 0]|
    --Lowest-roll tier. Basic sellable junk, money, etc. Contains JP drops so hey, get those chests!
    local zTierZero = {}
    zTierZero.sInternalName = "Tier Zero"
    table.insert(zTierZero, {"Tattered Rags", 30})
    table.insert(zTierZero, {"Ruined Armor",  30})
    table.insert(zTierZero, {"Broken Spear",  30})
    table.insert(zTierZero, {"Emerald",       20})
    table.insert(zTierZero, {"Aquamarine",    20})
    table.insert(zTierZero, {"Amethyst",      20})
    table.insert(zTierZero, {"Bruised Meat",  10})
    table.insert(zTierZero, {"Platina x15",   10})
    table.insert(zTierZero, {"Platina x50",    3})
    table.insert(zTierZero, {"Platina x100",   1})
    table.insert(zTierZero, {"JP x10",        10})
    table.insert(zTierZero, {"JP x30",         1})
    
    -- |[Tier 1]|
    --Basic tier of reusables. Contains basic crafting items.
    local zTierOne = {}
    zTierOne.sInternalName = "Tier One"
    table.insert(zTierOne, {"Light Leather",     1})
    table.insert(zTierOne, {"Light Fiber",       1})
    table.insert(zTierOne, {"Light Carapace",    1})
    table.insert(zTierOne, {"Gossamer",          1})
    table.insert(zTierOne, {"Hardened Bark",     1})
    
    -- |[Tier 2]|
    --Contains gems.
    local zTierTwo = {}
    zTierTwo.sInternalName = "Tier Two"
    table.insert(zTierTwo, {"Glintsteel Gem",   1})
    table.insert(zTierTwo, {"Phassicsteel Gem", 1})
    table.insert(zTierTwo, {"Arensteel Gem",    1})
    table.insert(zTierTwo, {"Yetesteel Gem",    1})
    table.insert(zTierTwo, {"Yemite Gem",       1})
    table.insert(zTierTwo, {"Romite Gem",       1})
    table.insert(zTierTwo, {"Morite Gem",       1})
    table.insert(zTierTwo, {"Kyite Gem",        1})
    table.insert(zTierTwo, {"Nockrion Gem",     1})
    table.insert(zTierTwo, {"Donion Gem",       1})
    table.insert(zTierTwo, {"Samlion Gem",      1})
    table.insert(zTierTwo, {"Rubose Gem",       1})
    table.insert(zTierTwo, {"Piorose Gem",      1})
    table.insert(zTierTwo, {"Iniorose Gem",     1})
    table.insert(zTierTwo, {"Whodorose Gem",    1})
    table.insert(zTierTwo, {"Blurleen Gem",     1})
    table.insert(zTierTwo, {"Mordreen Gem",     1})
    table.insert(zTierTwo, {"Quorine Gem",      1})
    table.insert(zTierTwo, {"Phirine Gem",      1})
    table.insert(zTierTwo, {"Qederphage Gem",   1})
    table.insert(zTierTwo, {"Phonophage Gem",   1})
    table.insert(zTierTwo, {"Thatophage Gem",   1})
    table.insert(zTierTwo, {"Cirrophage Gem",   1})
    
    -- |[Tier 3]|
    --Contains "Basic" combat items and armors. Accessories are more likely to drop.
    local zTierThree = {}
    zTierThree.sInternalName = "Tier Three"
    table.insert(zTierThree, {"Healing Tincture",   1})
    table.insert(zTierThree, {"Palliative",         1})
    table.insert(zTierThree, {"Smelling Salts",     1})
    table.insert(zTierThree, {"Troubadour's Robe",  1})
    table.insert(zTierThree, {"Traveller's Vest",   1})
    table.insert(zTierThree, {"Leather Greatcoat",  1})
    table.insert(zTierThree, {"Light Leather Vest", 1})
    table.insert(zTierThree, {"Sphalite Ring",      3})
    table.insert(zTierThree, {"Decorative Bracer",  3})
    table.insert(zTierThree, {"Jade Eye Ring",      3})
    table.insert(zTierThree, {"Arm Brace",          3})
    table.insert(zTierThree, {"Alacrity Bracer",    3})
    table.insert(zTierThree, {"Swamp Boots",        3})
    table.insert(zTierThree, {"Bird Meat",          5})
    table.insert(zTierThree, {"Slippery Boar Meat", 5})
    table.insert(zTierThree, {"Mighty Venison",     5})
    
    -- |[Tier 4]|
    --Contains "Intermediate" items and armors.
    local zTierFour = {}
    zTierFour.sInternalName = "Tier Four"
    table.insert(zTierFour, {"Bubbling Healing Tincture",   3})
    table.insert(zTierFour, {"Controversial Sugar Cookies", 3})
    table.insert(zTierFour, {"Infused Rye Bread",           3})
    table.insert(zTierFour, {"Sparkling Sourdough",         3})
    table.insert(zTierFour, {"Plated Leather Vest",         1})
    table.insert(zTierFour, {"Enchanted Ring",              4})
    table.insert(zTierFour, {"Jade Necklace",               4})
    table.insert(zTierFour, {"Comfy Socks",                 4})
    table.insert(zTierFour, {"Musket Rounds",               1})
    table.insert(zTierFour, {"7.62x56mmR FL",               1})
    table.insert(zTierFour, {"Argentum Straightsword",      1})
    table.insert(zTierFour, {"Electrum Straightsword",      1})
    
    -- |[Tier 5]|
    --The good stuff. Advanced items and equipment.
    local zTierFive = {}
    zTierFive.sInternalName = "Tier Five"
    table.insert(zTierFive, {"Fencer's Gauntlet", 1})
    table.insert(zTierFive, {"7.62x56mmR LT",     1})
    table.insert(zTierFive, {"7.62x56mmR Expd",   1})
    table.insert(zTierFive, {"7.62x56mmR AP",     1})
    table.insert(zTierFive, {"Goddess' Whisker",  1})
    
    -- |[Assembly]|
    --Given a loot table, this function adds the total of all rolls in that table and stores it in the table.
    local function fnCompileLootTable(pzaLootTable)
        
        --Setup.
        local iTotal = 0
        
        --Iterate across the table. Add up the values.
        for i = 1, #pzaLootTable, 1 do
            local iCount = pzaLootTable[i][2]
            iTotal = iTotal + iCount
        end
        
        --Store the values in the table.
        pzaLootTable.iTotalRoll = iTotal
    end
    
    --Add up the totals for each loot table.
    fnCompileLootTable(zTierA)
    fnCompileLootTable(zTierZero)
    fnCompileLootTable(zTierOne)
    fnCompileLootTable(zTierTwo)
    fnCompileLootTable(zTierThree)
    fnCompileLootTable(zTierFour)
    fnCompileLootTable(zTierFive)
    
    --Place all the loot tables in the master class.
    gzaLootTables.zaAdamantite = zTierA
    gzaLootTables.zaLootTiers = {}
    gzaLootTables.zaLootTiers[1] = zTierZero
    gzaLootTables.zaLootTiers[2] = zTierOne
    gzaLootTables.zaLootTiers[3] = zTierTwo
    gzaLootTables.zaLootTiers[4] = zTierThree
    gzaLootTables.zaLootTiers[5] = zTierFour
    gzaLootTables.zaLootTiers[6] = zTierFive

end

-- |[ ====================================== Area Resolve ====================================== ]|
--We need to figure out which area we are currently in. The map's name is passed in, so we check
-- that against a set of names.
local sAreaName = "Northwoods"

-- |[Northwoods Analogues]|
if(string.sub(sLevelName, 1, 10) == "Northwoods") then
    sAreaName = "Northwoods"
elseif(string.sub(sLevelName, 1, 10) == "SanyaCabin") then
    sAreaName = "Northwoods"
elseif(string.sub(sLevelName, 1, 10) == "SanyaMines") then
    sAreaName = "Northwoods"
elseif(string.sub(sLevelName, 1, 11) == "HeroPlateau") then
    sAreaName = "Northwoods"
elseif(string.sub(sLevelName, 1, 11) == "LowerShrine") then
    sAreaName = "Northwoods"
elseif(string.sub(sLevelName, 1, 8) == "VucaPass") then
    sAreaName = "Northwoods"
elseif(string.sub(sLevelName, 1, 12) == "GranvirePass") then
    sAreaName = "Northwoods"
elseif(string.sub(sLevelName, 1, 14) == "KitsuneVillage") then
    sAreaName = "Northwoods"
elseif(string.sub(sLevelName, 1, 11) == "MtSarulente") then
    sAreaName = "Northwoods"

-- |[Westwoods Analogues]|
elseif(string.sub(sLevelName, 1, 9) == "Westwoods") then
    sAreaName = "Westwoods"
elseif(string.sub(sLevelName, 1, 7) == "Warrens") then
    sAreaName = "Westwoods"
end

-- |[ ======================================== Selection ======================================= ]|
-- |[Common Setup]|
--Get the green chest bonus. Subtract 1 for the chest that we just opened!
local iTotalGreensOpened = VM_GetVar("Root/Variables/Chests/Random/iTotalGreensOpened", "N") - 1
Debug_Print(" Total green chests opened: " .. iTotalGreensOpened .. "\n")

--First, perform a basic roll to determine if we are getting conventional loot, or adamantite.
-- Adamantite has a fixed 15% chance to be selected.
local ciAdamantiteChance = 15
local iRootRoll = LM_GetRandomNumber(1, 100)

-- |[ ====== Adamantite Table ====== ]|
--Roll from the special adamantite table only.
if(iRootRoll <= ciAdamantiteChance) then

    -- |[Debug]|
    Debug_Print(" Rolled to select Adamantite.\n")

    -- |[Roll Ranges]|
    --Based on which area we're in, the roll range changes.
    local iRangeLo = 1
    local iRangeHi = 2
    
    --Northwoods:
    if(sAreaName == "Northwoods") then
        iRangeLo = 1
        iRangeHi = 500

    --Westwoods:
    elseif(sAreaName == "Westwoods") then
        iRangeLo = 50
        iRangeHi = 620
    end
    
    --A bonus of 5 is added per chest.
    local ciAdamantiteChestBonus = 5
    local iRollBonus = iTotalGreensOpened * ciAdamantiteChestBonus
    Debug_Print(" Roll bonus: " .. iRollBonus .. "\n")
    
    --Roll.
    local iRoll = LM_GetRandomNumber(iRangeLo, iRangeHi) + iRollBonus
    if(iRoll >= gzaLootTables.zaAdamantite.iTotalRoll) then iRoll = gzaLootTables.zaAdamantite.iTotalRoll end
    Debug_Print(" Final roll: " .. iRoll .. "\n")
    
    -- |[Resolve]|
    --Run across the table and get the item.
    for i = 1, #gzaLootTables.zaAdamantite, 1 do
        iRoll = iRoll - gzaLootTables.zaAdamantite[i][2]
        
        --If the value hits zero or lower, then this is the item.
        if(iRoll <= 0) then
            AL_SetProperty("Respawning Chest Script Result", gzaLootTables.zaAdamantite[i][1])
            Debug_PopPrint(" Selected item: " .. gzaLootTables.zaAdamantite[i][1] .. "\n")
            return
        end
    end
    
-- |[ ===== Conventional Items ===== ]|
else

    -- |[Debug]|
    Debug_Print(" Rolled to select Conventional Item.\n")

    -- |[Table Select]|
    --The base table roll is based on which area we're in.
    local iRangeLo = 1
    local iRangeHi = 2
    local iRollCap = 900
    
    --Northwoods:
    if(sAreaName == "Northwoods") then
        iRangeLo = 1
        iRangeHi = 800
    
    --Westwoods:
    elseif(sAreaName == "Westwoods") then
        iRangeLo = 100
        iRangeHi = 800
    end
    
    --A bonus of 5 is added per chest.
    local ciConventionalChestbonus = 20
    local iRollBonus = iTotalGreensOpened * ciConventionalChestbonus
    Debug_Print(" Roll bonus: " .. iRollBonus .. "\n")
    
    --Roll.
    local iRoll = LM_GetRandomNumber(iRangeLo, iRangeHi) + iRollBonus
    if(iRoll >= iRollCap) then iRoll = iRollCap end
    Debug_Print(" Tier roll: " .. iRoll .. "\n")

    -- |[Table Resolve]|
    --Run across the tables and pick which one to roll into.
    local zaUseTable = nil
    local ciTierZeroChance  =                     300
    local ciTierOneChance   = ciTierZeroChance  + 300
    local ciTierTwoChance   = ciTierOneChance   + 100
    local ciTierThreeChance = ciTierTwoChance   + 100
    local ciTierFourChance  = ciTierThreeChance +  50
    local ciTierFiveChance  = ciTierFourChance  +  50
    
    --Resolve.
    if(iRoll < ciTierZeroChance) then
        zaUseTable = gzaLootTables.zaLootTiers[1]
    elseif(iRoll < ciTierOneChance) then
        zaUseTable = gzaLootTables.zaLootTiers[2]
    elseif(iRoll < ciTierTwoChance) then
        zaUseTable = gzaLootTables.zaLootTiers[3]
    elseif(iRoll < ciTierThreeChance) then
        zaUseTable = gzaLootTables.zaLootTiers[4]
    elseif(iRoll < ciTierFourChance) then
        zaUseTable = gzaLootTables.zaLootTiers[5]
    elseif(iRoll < ciTierFiveChance) then
        zaUseTable = gzaLootTables.zaLootTiers[6]
    else
        zaUseTable = gzaLootTables.zaLootTiers[1]
    end
    
    -- |[Table Within Roll]|
    --Debug.
    Debug_Print(" Selected tier: " .. zaUseTable.sInternalName .. "\n")
    
    --Roll based on the table's max.
    local iSecondRoll = LM_GetRandomNumber(1, zaUseTable.iTotalRoll)
    Debug_Print(" Second roll: " .. iSecondRoll .. " versus max: " .. zaUseTable.iTotalRoll .. "\n")
    
    --Run across the table and get the item.
    for i = 1, #zaUseTable, 1 do
        iSecondRoll = iSecondRoll - zaUseTable[i][2]
        
        --If the value hits zero or lower, then this is the item.
        if(iSecondRoll <= 0) then
            AL_SetProperty("Respawning Chest Script Result", zaUseTable[i][1])
            Debug_PopPrint(" Selected item: " .. zaUseTable[i][1] .. "\n")
            return
        end
    end

end

-- |[ ========= Error Case ========= ]|
AL_SetProperty("Respawning Chest Script Result", "Null")
Debug_ForcePrint(" Warning: Random chest handler did not find a valid item. Returning 'Null'.\n")
Debug_PopPrint("Finished selection.\n")
