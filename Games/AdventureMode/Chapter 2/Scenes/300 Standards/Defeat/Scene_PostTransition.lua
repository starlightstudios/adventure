-- |[ ================================== Scene Post-Transition ================================= ]|
--After transition, play a random dialogue line.

-- |[ ================ Setup ================= ]|
-- |[Variables]|
local bIsIzunaPresent   = fnIsCharacterPresent("Izuna")
local bIsEmpressPresent = fnIsCharacterPresent("Empress")
local bIsZekePresent    = fnIsCharacterPresent("Zeke")

-- |[Overlay]|
--Set the overlay to fullblack. Fade in slowly.
fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Over_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
fnCutsceneWait(120)
fnCutsceneBlocker()

-- |[Position]|
--Call function to automatically position characters around the save point. This also sets everyone to "Wounded" frames.
SceneReversion:fnSavePointRearrange()

-- |[ ============== Execution =============== ]|
-- |[Fade In]|
fnCutscene([[ AL_SetProperty("Activate Fade", 180, gci_Fade_Over_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])

--Wait a bit.
fnCutsceneWait(240)
fnCutsceneBlocker()

-- |[Stand Up, Face Leader]|
--Handled by a subroutine, party stands up and rotates to face the leader.
SceneReversion:fnPartyStandUp()

--Re-transform. Subroutine makes party members return to their starting forms if they reverted.
SceneReversion:fnRetransform()

-- |[ ======================================== Dialogue ======================================== ]|
-- |[Solo]|
--Leader talks to herself.
if(giFollowersTotal == 0) then
	
	--Setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])

	--Roll.
	local iDialogueRoll = LM_GetRandomNumber(0, 8)
    iDialogueRoll = 0

	--Dialogue handlers.
	if(iDialogueRoll == 0) then
		fnCutscene([[ Append("Sanya:[E|Neutral] ...[P] Like falling off a bicycle!") ]])
	end

--Sanya/Izuna/Zeke.
else

	--Setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)

	--Roll.
	local iDialogueRoll = LM_GetRandomNumber(0, 3)

	--Dialogue handlers.
	if(iDialogueRoll == 0) then
		fnCutscene([[ Append("Sanya:[E|Neutral] Everyone's skin still attached?[B][C]") ]])
		fnCutscene([[ Append("Izuna:[E|Smirk] Skin, bones, cartilage.[P] Everything is here![B][C]") ]])
		fnCutscene([[ Append("Zeke:[E|Neutral] Baaa!") ]])

	elseif(iDialogueRoll == 1) then
		fnCutscene([[ Append("Sanya:[E|Neutral] Beginner's luck![P] We'll get 'em next time!") ]])

	elseif(iDialogueRoll == 2) then
		fnCutscene([[ Append("Zeke:[E|Neutral] NYEH![B][C]") ]])
		fnCutscene([[ Append("Izuna:[E|Smirk] Zeke thirsts for blood![B][C]") ]])
		fnCutscene([[ Append("Sanya:[E|Neutral] The seas shall run red with the blood of his enemies![B][C]") ]])
		fnCutscene([[ Append("Zeke:[E|Neutral] BAHHHH!!!") ]])

	elseif(iDialogueRoll == 3) then
		fnCutscene([[ Append("Izuna:[E|Cry] You doing okay?[P] Should I heal you?[B][C]") ]])
		fnCutscene([[ Append("Sanya:[E|Neutral] I'm feeling fine.[B][C]") ]])
		fnCutscene([[ Append("Izuna:[E|Smirk] Okay I'll - [P][CLEAR]") ]])
		fnCutscene([[ Append("Sanya:[E|Neutral] I didn't say not to touch me.[P] With your healing touch![B][C]") ]])
		fnCutscene([[ Append("Izuna:[E|Blush] Is it always sex time with you?[B][C]") ]])
		fnCutscene([[ Append("Sanya:[E|Neutral] Okay fine, this one time, no.") ]])
	end
end
fnCutsceneBlocker()

-- |[ ============== Finish Up =============== ]|
--Characters move onto the party leader. This is handled by the subroutine.
SceneReversion:fnSavePointFold()
