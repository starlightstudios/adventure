-- |[ ==================================== Quicket Pamphlets =================================== ]|
--Used for pamhplet diagnostics.
VM_SetVar("Root/Variables/Global/Izuna/sPamphlet", "S", "Dragon")
LM_ExecuteScript(gsItemListing, "Pamphlet: Dragon")
LM_ExecuteScript(gsItemListing, "Pamphlet: Green Westwoods")
LM_ExecuteScript(gsItemListing, "Pamphlet: Bandits in Westwoods")
LM_ExecuteScript(gsItemListing, "Pamphlet: Bun Runners")
        
--Add 'Extra, Extra!' to the first unused Field Ability slot. If none are open, do nothing.
local iUseSlot = -1
for i = 0, 4, 1 do
    if(AdvCombat_GetProperty("Field Ability Name", i) == "Null") then
        iUseSlot = i
        break
    end
end
if(iUseSlot ~= -1) then
    AdvCombat_SetProperty("Set Field Ability", iUseSlot, "Root/Special/Combat/FieldAbilities/Extra Extra")
end

--Add 'Change Pamphlet' to the first unused Field Ability slot. If none are open, do nothing.
iUseSlot = -1
for i = 0, 4, 1 do
    if(AdvCombat_GetProperty("Field Ability Name", i) == "Null") then
        iUseSlot = i
        break
    end
end
if(iUseSlot ~= -1) then
    AdvCombat_SetProperty("Set Field Ability", iUseSlot, "Root/Special/Combat/FieldAbilities/Change Pamphlet")
end