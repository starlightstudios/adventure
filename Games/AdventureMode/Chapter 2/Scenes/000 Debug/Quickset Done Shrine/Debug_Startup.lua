-- |[ ================================== Quicket Done Shrine =================================== ]|
--Quickly completes all the variables related to the Shrine of the Hero at the start of the chapter.
-- This sets variables up to right before Sanya and Zeke walk to the clifftop.
--This script does not add Zeke to the party. Be sure to do that via the adders.

--Sanya is carrying Izuna.
VM_SetVar("Root/Variables/Costumes/Sanya/sCostumeHuman", "S", "Carrying")
VM_SetVar("Root/Variables/Costumes/Sanya/sCostumeSevavi", "S", "Carrying")
LM_ExecuteScript(gsCharacterAutoresolve, "Sanya")

--Variables.
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iSnidePond", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iSwissPeople", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iTiredA", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iTiredB", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iTiredC", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iSawZeke", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iSawHostiles", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iGotResetAbility", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBSwitchB", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBSwitchC", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBBridge", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBDoor", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBSolved", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineCSolved", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineDSolved", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iMetZeke", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iEscapedShrine", "N", 0.0) --Player should have Zeke for the next scene.

--Field Abilities.
AdvCombat_SetProperty("Set Field Ability", 0, "Root/Special/Combat/FieldAbilities/Reset")