-- |[Special]|
--Quickly removes the named party member from the party.
fnRemovePartyMember("Zeke", true)
