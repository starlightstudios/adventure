-- |[ =============================== Scene Activate: Take Care ================================ ]|
--Fires the cutscene series where Sanya takes care of Izuna for a month. This is done by teleporting
-- the player to the cutscene trigger.
--No other variables are set for this.
local sLevelName = AL_GetProperty("Name")

--Already here, teleport.
if(sLevelName == "SanyaCabinA") then
    fnCutsceneTeleport("Sanya", 30.25, 31.50)

--Warp to the level and position.
else
    fnCutscene([[ AL_BeginTransitionTo("SanyaCabinA", "FORCEPOS:30.0x31.0x0") ]])

end
