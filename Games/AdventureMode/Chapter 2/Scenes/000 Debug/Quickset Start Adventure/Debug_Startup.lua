-- |[ ================================ Quicket Start Adventure ================================= ]|
--Quickly completes all the variables to the point where the player gains control of the party as
-- they set out for adventure. Concludes the hunting minigame.

-- |[Campfire Listing]|
VM_SetVar("Root/Variables/Chapter2/Campfires/iSanyaCabinA",     "N", 1.0)

-- |[Shrine of the Hero]|
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iSnidePond", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iSwissPeople", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iTiredA", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iTiredB", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iTiredC", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iSawZeke", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iSawHostiles", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iGotResetAbility", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBSwitchB", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBSwitchC", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBBridge", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBDoor", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBSolved", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineCSolved", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineDSolved", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iMetZeke", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iEscapedShrine", "N", 1.0)

-- |[Mines, vs. Odar]|
DL_AddPath("Root/Variables/Chapter2/SanyaMines/")
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iDroppedOffIzuna", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iIsIzunaInCabin", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iIsZekeInCabin", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iSawMysteryLight", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iSawMysteryLightAgain", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iDieHere", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iMinesCartAtNorth", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iMinesCSolved", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iMinesDSolved", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iMinesESolved", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iDidPuzzleFight", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iWonPuzzleFight", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iSawRifleScene", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iSanyaStupidStupid", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iSawMinesExitDialogue", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iCompletedMines", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iIsNightAtMines", "N", 0.0)

-- |[Hunting Tutorial]|
DL_AddPath("Root/Variables/Chapter2/Northwoods/")
VM_SetVar("Root/Variables/Chapter2/Northwoods/iRunHuntingTutorial", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Northwoods/iIsTutorialHuntActive", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Northwoods/iSawTracks", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/Northwoods/iExaminedTracks", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/Northwoods/iSpottedPrey", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/Northwoods/iCompletedHunt", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/Northwoods/iUpgradedCabin", "N", 0.0)

-- |[Field Abilities]|
--Set expected abilities.
VM_SetVar("Root/Variables/Global/Sanya/iRecoveredRifle", "N", 1.0)
AdvCombat_SetProperty("Set Field Ability", 0, "Root/Special/Combat/FieldAbilities/Reset")
AdvCombat_SetProperty("Set Field Ability", 1, "Root/Special/Combat/FieldAbilities/Use Rifle")

-- |[Costumes]|
VM_SetVar("Root/Variables/Costumes/Sanya/sCostumeHuman", "S", "Rifle")
VM_SetVar("Root/Variables/Costumes/Sanya/sCostumeSevavi", "S", "Rifle")
