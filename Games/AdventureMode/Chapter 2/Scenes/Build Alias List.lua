-- |[ ==================================== Scene Alias List ==================================== ]|
--Cutscenes can be stored under an alias. This is used for enemies on the overworld. When the player
-- is defeated or retreats or otherwise calls a script, the alias is checked instead of the literal
-- script path. Those aliases are built in this script.
local sDebugPath     = gsRoot .. "Chapter 2/Scenes/000 Debug/"
local sTransformPath = gsRoot .. "Chapter 2/Scenes/100 Transform/"
local sDefeatPath    = gsRoot .. "Chapter 2/Scenes/200 Defeat/"
local sStandardPath  = gsRoot .. "Chapter 2/Scenes/300 Standards/"

--Wipe previous aliases.
PathAlias_Clear()

-- |[ ===================================== Combat Scenes ====================================== ]|
--Whenever a scene occurs as a result of combat, be it due to retreat, surrender, defeat, victory,
-- or ending for cutscene reasons, an alias must be used.
PathAlias_CreatePath("Standard Defeat",  sStandardPath .. "Defeat/Scene_Begin.lua")
PathAlias_CreatePath("Standard Retreat", sStandardPath .. "Retreat/Scene_Begin.lua")
PathAlias_CreatePath("Standard Revert",  sStandardPath .. "Revert/Scene_Begin.lua")

--Defeat Cutscenes
PathAlias_CreatePath("Defeat Sevavi", sDefeatPath .. "Defeat_Sevavi/Scene_Begin.lua")
PathAlias_CreatePath("Defeat Bunny",  sDefeatPath .. "Defeat_Bunny/Scene_Begin.lua")

-- |[ ======================================== Aliases ========================================= ]|
--These are the aliases that refer to the paths. These appear in the .slf data saved from Tiled.
PathAlias_CreateAliasToPath("Defeat_Sevavi", "Defeat Sevavi")
PathAlias_CreateAliasToPath("Defeat_Bunny",  "Defeat Bunny")
