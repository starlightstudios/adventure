-- |[ ============================== Volunteer to Sevavi Pedestal ============================== ]|
--Turns Sanya into a Sevavi voluntarily. Slightly changes based on the story.

-- |[Variables]|
local iEscapedShrine = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iEscapedShrine", "N")

-- |[Flags]|
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iFriendsWithSevavi", "N", 1.0)

-- |[Image Streaming]|
fnLoadDelayedBitmapsFromList("Chapter 2 Sanya Sevavi TF", gciDelayedLoadLoadAtEndOfTick)

-- |[ ==================================== Scene Execution ===================================== ]|
-- |[Fade]|
fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

-- |[Izuna Switch Case]|
--Common.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])

--If Izuna is still being carried:
if(iEscapedShrine == 0.0) then
    fnCutscene([[ Append("Sanya delicately placed the fox girl on the ground nearby, being careful not to bend or twist her in any way.[P] She stepped placidly on the pedestal and struck a pose.[P] Her thoughts clouded over and she had a feeling of serenity.[B][C]") ]])
    fnCutscene([[ Append("As she stood there, holding a pose, she began to think of all the ways that her clothes hid her natural, perfect form.[P] They would need to go.") ]])
    
--Izuna not being carried, but she's still present.
else
    fnCutscene([[ Append("[VOICE|Izuna]'Sanya, what are you doing?'[P] [VOICE|Narrator]Izuna asked.[P] Sanya stepped on the pedestal and struck a pose, with a serene smile on her face.[B][C]") ]])
    fnCutscene([[ Append("[VOICE|Sanya]'It will be fine.[P] Just give me a moment,'[P] [VOICE|Narrator]Sanya replied.[P] Izuna shrugged, having a pretty good idea of what was about to happen.[P] She instead settled for patting Zeke to keep him calm.[B][C]") ]])
    fnCutscene([[ Append("As she stood there, holding a pose, Sanya began to think of all the ways that her clothes hid her natural, perfect form.[P] They would need to go.[P] Izuna's eyes widened.[B][C]") ]])
end
fnCutsceneBlocker()

-- |[Scene]|
fnCutscene([[ WD_SetProperty("Transition Scene", "Null", "Root/Images/Scenes/SevaviTF/SevaviTF00") ]])
fnCutscene([[ Append("Sanya discarded her clothes in a heap and resumed her pose.[P] It felt perfect to stand here, in her naked glory, a work of art in her own right.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/SevaviTF/SevaviTF00", "Root/Images/Scenes/SevaviTF/SevaviTF01") ]])
fnCutscene([[ Append("But like any artist, there was always something that could be improved, or removed, to make the final piece that much better.[P] Sanya thought about herself like an artist, and not an observer.[B][C]") ]])
if(iEscapedShrine == 0.0) then
    fnCutscene([[ Append("[VOICE|Sanya]'Wouldn't you like a heart-shaped butt?'[P][VOICE|Narrator] she heard.[P] She cast a sideways glance at the unconscious cosplayer, but it was not her speaking.[B][C]") ]])
else
    fnCutscene([[ Append("[VOICE|Sanya]'Wouldn't you like a heart-shaped butt?'[P][VOICE|Narrator] she heard.[P] She cast a sideways glance at Izuna.[P] Her lips were parted and her mouth hung open as she stared at Sanya as if enthralled.[P] It was clear she had not spoken.[B][C]") ]])
end
fnCutscene([[ Append("She had always wanted a bigger, firmer rear.[P] And as she thought of it, she felt a bit of herself harden.[P] Her skin became firmer, like stone, and her heatbeat slowed down.[B][C]") ]])
fnCutscene([[ Append("Again she thought of what she'd like to see in herself.[P] Maybe longer hair.[P] She had always wanted longer hair but it was so difficult to keep clean.[P] Always tying into knots and getting dirty.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/SevaviTF/SevaviTF01", "Root/Images/Scenes/SevaviTF/SevaviTF02") ]])
fnCutscene([[ Append("She felt a heaviness from her head and the delicate, firm strands of hair lengthen and tumble down her back.[P] Her skin had taken on a pale tone, and she began to feel a heavy weight pressing down on her.[P] Moments later, she realized that weight was simply her own body.[B][C]") ]])
fnCutscene([[ Append("[VOICE|Narrator]She changed her pose again to accentuate her new features. [VOICE|Sanya]'But don't you want a bigger chest?'[VOICE|Narrator] the voice asked.[P] Of course she did.[P] She wanted people to gawk at her enormous rack, it was fun being sexy.[B][C]") ]])
if(iEscapedShrine == 0.0) then
    fnCutscene([[ Append("She wondered if the fox cosplayer had spoken, but again she was still lying there, motionless.[B][C]") ]])
else
    fnCutscene([[ Append("It had clearly not been Izuna who had asked her that, but Sanya nonetheless thought that perhaps Izuna would like her more with a bigger chest.[B][C]") ]])
    fnCutscene([[ Append("Izuna was staring straight at her breasts, her mouth slightly agape.[P] Sanya smirked.[B][C]") ]])
end
fnCutscene([[ Append("She felt her body grow and change again, her chest becoming larger and defying gravity.[P] Her waist narrowed slightly to move the mass around.[P] It was all the same now, not muscle or bone or flesh but stone.[B][C]") ]])
fnCutscene([[ Append("Like an artist who had finished her latest project, Sanya held her pose in triumph.[P] But who had been prompting her?[P] Was it the magic of the pedestal itself?[P] No.[B][C]") ]])
fnCutscene([[ Append("Sanya realized the voice that had been speaking to her was her own.[P] She had been asking herself how she wanted to be, and becoming it, reshaping herself through the dissociation of the pedestal.[P] Able to see herself as if through a third eye.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/SevaviTF/SevaviTF02", "Root/Images/Scenes/SevaviTF/SevaviTF03") ]])
if(iEscapedShrine == 0.0) then
    fnCutscene([[ Append("Now she had become a perfect work of art, a piece of raw material crafted into an object of true beauty.[P] And if she could, she'd stand on the pedestal to be admired for all time.[B][C]") ]])
    fnCutscene([[ Append("But, with a heavy stone heart, she had to descend.[P] The cosplayer needed her help, and Zeke was waiting for her.[P] People were relying on her.[P] There would always be more time to be still and admired later, when her duties were seen to.") ]])
else
    fnCutscene([[ Append("Now she had become a perfect work of art, a piece of raw material crafted into an object of true beauty.[P] And if she could, she'd stand on the pedestal to be admired for all time.[B][C]") ]])
    fnCutscene([[ Append("But, with a heavy stone heart, she had to descend.[P] The world needed saving, people were counting on her.[P] Izuna was staring at her with undisguised lust.[P] That would need to be sated, of course.") ]])
end
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/SevaviTF/SevaviTF03", "Root/Images/Scenes/SevaviTF/SevaviTF04") ]])
fnCutscene([[ Append("As she left, she cast a forlorn glance back at the pedestal.[P] Maybe a nip here, a tuck there.[P] No work of art couldn't be improved on, right?[B][C]") ]])
fnCutscene([[ Append("Her clothes lay in a heap, but oddly, they had changed just as she had.[P] A tan and red uniform.[P] It looked right on her, accentuating her assets.[P] She put it on, and smiled, a perfectly sculpted statue woman...") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/SevaviTF/SevaviTF04", "Root/Images/Scenes/SevaviTF/SevaviTF05") ]])
fnCutscene([[ Append("Dressing herself in the gloom, she realized her eyes now glowed softly in the darkness with a firey red. Her eyes had lit up like rubies.[B][C]") ]])

--If Izuna is still being carried:
if(iEscapedShrine == 0.0) then
    fnCutscene([[ Append("Filled with the strength of rock, she picked up her fallen foxy cosplayer friend and resumed her journey.") ]])

--She's standing.
else
    fnCutscene([[ Append("Izuna was in the next room, and would be excited to see the changes.[P] Sanya couldn't wait to show off her new body...") ]])

end
fnCutsceneBlocker()

-- |[Exec TF]|
fnCutsceneCall(gsRoot .. "FormHandlers/Sanya/Form_Sevavi.lua")

-- |[ ======================================= Post-Scene ======================================= ]|
-- |[Post-Scene]|
--Still in the shrine.
if(iEscapedShrine == 0.0) then
    
    -- |[Position]|
    --Only Sanya needs to be placed.
    fnCutsceneTeleport("Sanya", 7.25, 6.50)
    fnCutsceneFace("Sanya", 0, 1)
    
    -- |[Fading]|
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneWait(45)
    fnCutsceneBlocker()

    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Hot damn, do I feel good right now.[P] Too bad you missed it, cosplayer.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] I guess that pedestal was cursed or something, but I got what I wanted out of it.[P] Let's hope being extra heavy and extra beautiful doesn't become a liability.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Okay, job hasn't changed.[P] Rescue weeaboo girl, escape creepy mole people dungeon.") ]])
    fnCutsceneBlocker()

--Escaped and returned.
else

    -- |[Position]|
    fnCutsceneTeleport("Sanya", 7.25, 6.50)
    fnCutsceneTeleport("Izuna", 6.25, 6.50)
    fnCutsceneTeleport("Zeke", 7.25, 7.50)
    fnCutsceneFace("Sanya", -1, 0)
    fnCutsceneFace("Izuna", 1, 0)
    fnCutsceneFace("Zeke", 0, -1)

    -- |[Fading]|
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneWait(45)
    fnCutsceneBlocker()

    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Sanya:[E|Happy] Hoo-wa, what a rush![P] I feel incredible![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Blush] You look incredible![B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] Really?[P] You think so?[P] I was worried for a split second you might not like it.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Blush] I only regret that I have one pair of eyes.[P] If I had more, I could gaze at more of you.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Guess those stone people put these pedestals here to...[P] transform others?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] That must be how they do it.[P] I've never seen another like it.[P] Maybe they're only here?[P] Certainly not at the Trials of the Dragon.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Jot] You don't feel any sort of compulsions?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Like what?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Jot] Protect this place from intruders, transform anyone who comes by into a statue, anything like that?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Nope, not at all.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Jot] What about the transformation process.[P] Can you describe it?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] Like I was looking at myself from a distance, asking myself what I could do to change my body and make it more perfect.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] With each change, I felt a rush of joy, and my body changed, and I got a little firmer.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Hey, are we doing an interview right now?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Jot] Of course![P] Would you like to remain anonymous?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] You might want to be careful if you publish these, there might be a rush of people coming in to become the most beautiful statues in the world.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Can't be helped, I suppose.[P] Still, always good to get a few quick notes down.[P] They might be a piece of a larger story someday.") ]])
    fnCutsceneBlocker()

    -- |[Fold Party]|
    fnAutoFoldParty()
    fnCutsceneBlocker()

end

-- |[ ======================================== Clean Up ======================================== ]|
-- |[Unload Images]|
fnCutscene([[ fnUnloadBitmapsFromList("Chapter 2 Sanya Sevavi TF") ]])
