-- |[ ================================ Volunteer to Harpy, Feet ================================ ]|
--Turns Sanya into a Harpy! This is the feet version.

-- |[Image Streaming]|
fnLoadDelayedBitmapsFromList("Chapter 2 Sanya Harpy Feet TF", gciDelayedLoadLoadAtEndOfTick)

-- |[ ==================================== Scene Execution ===================================== ]|
-- |[Fade]|
fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

-- |[Dialogue]|
--Common.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ WD_SetProperty("Transition Scene", "Null", "Root/Images/Scenes/HarpyTF/HarpyFeetTF00") ]])
fnCutscene([[ Append("It was dark, too dark to see anything.[P] It was cramped, too cramped to move.[P] Sanya's ears were ringing and her limbs were heavy, but deprived of her sight, her other senses began to heighten in order to compensate.[P] Feeling, Hearing, Smell, Taste;[P] first came feeling, as the strength came back to her limbs, she felt at her...[P] prison?[P] No, this wasn't a prison, she was supposed to be here for the time being.[P] It was an egg, she'd known she was going to be placed inside of an egg for her change.) ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/HarpyTF/HarpyFeetTF00", "Root/Images/Scenes/HarpyTF/HarpyFeetTF01") ]])
fnCutscene([[ Append("She was filled with a profound sense of liminality, this place was a boundary between her life as she'd known it, and life as it would be from now on.[P] Her hearing cleared, as beyond the confines of the egg, she began to hear a melody, piercing through the walls, echoing around inside to create a beautiful, ethereal sound, the most gorgeous music she'd ever heard in her life.[P] A birdsong that caused her entire body to thrum like a string.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/HarpyTF/HarpyFeetTF01", "Root/Images/Scenes/HarpyTF/HarpyFeetTF02") ]])
fnCutscene([[ Append("The song began to dispel the brain fog, blowing it away as each notes took root in her mind.[P] Each chord teaching a lesson.[P] The rising and falling notes taught her of how she was becoming a child of the sky, the earth was no longer her only home, she would be free![P] Free to go wherever her heart desired![P] The chorus, in perfect harmony, taught her of unity, of perfect sisterhood, she was part of a family now, a loving flock that would always be safe, and happy and warm within.[B][C]") ]])
fnCutscene([[ Append("A flock that was always looking to grow...") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/HarpyTF/HarpyFeetTF02", "Root/Images/Scenes/HarpyTF/HarpyFeetTF03") ]])
fnCutscene([[ Append("Middling bits of doubt lingered in her mind as she embraced and basked in the beautiful music.[P] She became aware of how hot and sweaty she was in this claustrophobic space.[P] But the song drowned out the fear as she felt herself begin to change.[P] Soft, downy feathers grew to cover her arms, sprouting out and blossoming into a majestic pair of wings that begged to be allowed to taste the cool air and soar through the sky.[P] Her nails sharpened themselves to points as her hands became as claws.[P] They felt so strong, yet so soft, perfect for capturing prey but also for cradling her new sisters as they sang each other to sleep...[B][C]") ]])
fnCutscene([[ Append("Then came her feet, they began to stretch as thin, pliable scales covered them, a soft and almost glossy surface, perfect for...[P] Perfect for...[P] Her nails on her toes grew just as they had on her fingers, there was nothing that could escape her grasp![P] Once she had something in her sights, she was pin them down, holding them so tightly, smothering them under her-[P] Smell![P] She drew a sharp breath of air as an acrid odor in the stuffy air made her shudder, she could tell that every part of her was positively dripping with sweat, but especially so from her feet.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/HarpyTF/HarpyFeetTF03", "Root/Images/Scenes/HarpyTF/HarpyFeetTF04") ]])
fnCutscene([[ Append("Sanya became aware of a part of the song that she hadn't noticed before, a single rogue voice, a counter-melody, expertly hiding itself within the greater song.[P] It had melted itself into the chorus so exquisitely that it would've continued to go completely unnoticed, if not for the singers growing excitement.[P] Now it was all she could hear, how it sang of how gorgeous, strong and fragrant Sanya's new feet were, just like her sisters'.[P] How she couldn't wait to feel Sanya's feet pressed against her face, the slippery, warm, heady sweat that seeped out of every pore dripping and drizzling onto her, coating her tongue, marking her with her scent.[P] Then she would delight in doing the same to Sanya with her own, uniquely soft and pungent feet.[B][C]") ]])
fnCutscene([[ Append("Sanya began to pant as the sweat that had begun from anxiety and nerves turned to sweat of pure arousal.[P] She reached down and rubbed her feet as she smiled, her tongue lolling out of her mouth.[P] She felt every silky ridge and scale of her taloned feet.[P] With each breath, the smell became more robust in her nostrils, as the song of her sisters had kept changing her all the while.[P] Her sense of smell became so much sharper, as his did her eyes, her pupils becoming that of a bird of prey.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/HarpyTF/HarpyFeetTF04", "Null") ]])
fnCutscene([[ Append("It was only with these new eyes that she could detect the faintest of lights piercing through the walls, and could see the rest of her new flock fluttering around her, awaiting her arrival.[P] Impulsively, she delicately ran two of her fingers across her feet, bringing them up to her mouth and lapped at them.[P] Taste![P] She'd never tasted anything so wonderful in her life, her own flavor that she was more than eager to share with the entire world;[P] with her prey, her sisters, and especially that mischievous sister that snuck her own verse into the flock's song.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Null", "Root/Images/Scenes/HarpyTF/HarpyFeetTF05") ]])
fnCutscene([[ Append("A new world and a new family was waiting for her outside, and she was ready to be with them.[P] She scrunched up her toes as their razor sharp points etched into the walls of this egg...[P] Her egg.[P] It had been the crucible of her rebirth into a new beautiful life, and now she was ready to leave.[P] She knew that a bird should leave its egg with its beak, but she had something so much better.[B][C]") ]])
fnCutscene([[ Append("She reared back and kicked at the wall with the base of her foot, with a loud percussive thud, hairline cracks appeared on the surface.[P] Outside, the song reached a crescendo as the flock sensed the arrival of its newest 'chick'.[P] Again, and again, she hammered away at the wall, more firm than its deceptively fragile appearance suggested.[P] She let out a trill as the cracks became larger and rays of sunlight started to poke through.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/HarpyTF/HarpyFeetTF05", "Root/Images/Scenes/HarpyTF/HarpyFeetTF06") ]])
fnCutscene([[ Append("Then, with one last powerful kick, the egg shattered, the brightness of the world engulfed her, and the cloud of musk that'd been forming inside of her egg was released into the world as the cool air swept in.[P] Her sisters flew closer to welcome her...[P] Only to recoil slightly as the scent of the air struck their sensitive noses.[P] All except for one, of course.[P] That lovely, naughty sister whose dissident tune has instilled in Sanya the new love and appreciation for feet that she herself had long harbored in secret.[P] Now, she wouldn't be alone, now they would have each other.[P] And soon, many more.") ]])
fnCutsceneBlocker()
