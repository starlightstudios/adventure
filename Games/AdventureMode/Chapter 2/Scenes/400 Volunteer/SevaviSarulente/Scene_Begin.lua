-- |[ ============================== Volunteer to Sevavi Pedestal ============================== ]|
--Turns Sanya into a Sevavi voluntarily. This is used in Empress' house, and has slightly modified
-- dialogue and positioning.

-- |[Variables]|
-- |[Flags]|
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iFriendsWithSevavi", "N", 1.0)

-- |[Fade]|
fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

-- |[Dialogue]|
--Common.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
--fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])

fnCutscene([[ Append("[VOICE|Izuna]'This one?[P] Looks like a normal pedestal to me.'[P] [VOICE|Narrator]Izuna said.[P] Sanya stepped on the pedestal and struck a pose, with a serene smile on her face.[B][C]") ]])
fnCutscene([[ Append("[VOICE|Sanya]'Sit back and enjoy the show,'[P] [VOICE|Narrator]Sanya replied.[P] Izuna shrugged, having a pretty good idea of what was about to happen.[P] She instead settled for patting Zeke to keep him calm.[B][C]") ]])
fnCutscene([[ Append("As she stood there, holding a pose, Sanya began to think of all the ways that her clothes hid her natural, perfect form.[P] They would need to go.[P] Izuna's eyes widened.[B][C]") ]])

--Common.
fnCutscene([[ Append("Sanya discarded her clothes in a heap and resumed her pose.[P] It felt perfect to stand here, in her naked glory, a work of art in her own right.[B][C]") ]])
fnCutscene([[ Append("But like any artist, there was always something that could be improved, or removed, to make the final piece that much better.[P] Sanya thought about herself like an artist, and not an observer.[B][C]") ]])

fnCutscene([[ Append("[VOICE|Sanya]'Wouldn't you like a heart-shaped butt?'[P][VOICE|Narrator] she heard.[P] She cast a sideways glance at Izuna.[P] Her lips were parted and her mouth hung open as she stared at Sanya as if enthralled.[P] It was clear she had not spoken.[B][C]") ]])
fnCutscene([[ Append("She had always wanted a bigger, firmer rear.[P] And as she thought of it, she felt a bit of herself harden.[P] Her skin became firmer, like stone, and her heatbeat slowed down.[B][C]") ]])
fnCutscene([[ Append("Again she thought of what she'd like to see in herself.[P] Maybe longer hair.[P] She had always wanted longer hair but it was so difficult to keep clean.[P] Always tying into knots and getting dirty.[B][C]") ]])
fnCutscene([[ Append("She felt a heaviness from her head and the delicate, firm strands of hair lengthen and tumble down her back.[P] Her skin had taken on a pale tone, and she began to feel a heavy weight pressing down on her.[P] Moments later, she realized that weight was simply her own body.[B][C]") ]])
fnCutscene([[ Append("[VOICE|Narrator]She changed her pose again to accentuate her new features. [VOICE|Sanya]'But don't you want a bigger chest?'[VOICE|Narrator] the voice asked.[P] Of course she did.[P] She wanted people to gawk at her enormous rack, it was fun being sexy.[B][C]") ]])

fnCutscene([[ Append("It had clearly not been Izuna who had asked her that, but Sanya nonetheless thought that perhaps Izuna would like her more with a bigger chest.[B][C]") ]])
fnCutscene([[ Append("Izuna was staring straight at her breasts, her mouth slightly agape.[P] Sanya smirked.[B][C]") ]])
fnCutscene([[ Append("She felt her body grow and change again, her chest becoming larger and defying gravity.[P] Her waist narrowed slightly to move the mass around.[P] It was all the same now, not muscle or bone or flesh but stone.[B][C]") ]])
fnCutscene([[ Append("Like an artist who had finished her latest project, Sanya held her pose in triumph.[P] But who had been prompting her?[P] Was it the magic of the pedestal itself?[P] No.[B][C]") ]])
fnCutscene([[ Append("Sanya realized the voice that had been speaking to her was her own.[P] She had been asking herself how she wanted to be, and becoming it, reshaping herself through the dissociation of the pedestal.[P] Able to see herself as if through a third eye.[B][C]") ]])
fnCutscene([[ Append("Now she had become a perfect work of art, a piece of raw material crafted into an object of true beauty.[P] And if she could, she'd stand on the pedestal to be admired for all time.[B][C]") ]])
fnCutscene([[ Append("But, with a heavy stone heart, she had to descend.[P] The world needed saving, people were counting on her.[P] Izuna was staring at her with undisguised lust.[P] That would need to be sated, of course.[B][C]") ]])
fnCutscene([[ Append("As she left, she cast a forlorn glance back at the pedestal.[P] Maybe a nip here, a tuck there.[P] No work of art couldn't be improved on, right?[B][C]") ]])
fnCutscene([[ Append("Her clothes lay in a heap, but oddly, they had changed just as she had.[P] A tan and red uniform.[P] It looked right on her, accentuating her assets.[P] She put it on, and smiled, a perfectly sculpted statue woman...") ]])
fnCutsceneBlocker()

-- |[Exec TF]|
fnCutsceneCall(gsRoot .. "FormHandlers/Sanya/Form_Sevavi.lua")

-- |[Post-Scene]|
--Still in the shrine.

-- |[Position]|
fnCutsceneTeleport("Sanya", 6.25, 20.50)
fnCutsceneTeleport("Izuna", 9.25, 20.50)
fnCutsceneTeleport("Zeke", 9.25, 21.50)
fnCutsceneFace("Sanya", 1, 0)
fnCutsceneFace("Izuna", -1, 0)
fnCutsceneFace("Zeke", -1, 0)

-- |[Fading]|
fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneWait(45)
fnCutsceneBlocker()

-- |[Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
fnCutscene([[ Append("Sanya:[E|Neutral] Hoo-wa, what a rush![P] I feel incredible![B][C]") ]])
fnCutscene([[ Append("Izuna:[E|Blush] You look incredible![B][C]") ]])
fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
fnCutscene([[ Append("Sanya:[E|Neutral] Really?[P] You think so?[P] I was worried for a split second you might not like it.[B][C]") ]])
fnCutscene([[ Append("Izuna:[E|Blush] I only regret that I have one pair of eyes.[P] If I had more, I could gaze at more of you.[B][C]") ]])
fnCutscene([[ Append("Izuna:[E|Neutral] I wonder if they use these pedestals to heal.[P] It's in the servant's quarters.[P] Maybe it's like sleeping to a statue?[B][C]") ]])
fnCutscene([[ Append("Sanya:[E|Neutral] Maybe.[P] Let's go see what Empress needed.[P] Maybe this is enough.[B][C]") ]])
fnCutscene([[ Append("Izuna:[E|Jot] Hmm, hmm, yeah.[P] Just a sec.[B][C]") ]])
fnCutscene([[ Append("Izuna:[E|Jot] This might be a thing to follow up on later.[P] Who made this magic?[P] Does it affect partirhumans?[P] Are there more pedestals like this?[B][C]") ]])
fnCutscene([[ Append("Sanya:[E|Neutral] Don't make me drag you![P] Let's go!") ]])
fnCutsceneBlocker()

-- |[Fold Party]|
fnAutoFoldParty()
fnCutsceneBlocker()
