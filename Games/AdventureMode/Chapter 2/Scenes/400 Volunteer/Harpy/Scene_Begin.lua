-- |[ ================================== Volunteer to Harpy =================================== ]|
--Turns Sanya into a Harpy! This is the non-feet version.

-- |[Image Streaming]|
fnLoadDelayedBitmapsFromList("Chapter 2 Sanya Harpy TF", gciDelayedLoadLoadAtEndOfTick)

-- |[ ==================================== Scene Execution ===================================== ]|
-- |[Fade]|
fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

-- |[Dialogue]|
--Common.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ Append("Warm.[P] Dark, but warm.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Null", "Root/Images/Scenes/HarpyTF/HarpyTF00") ]])
fnCutscene([[ Append("That was the feeling that came to Sanya as her mind drifted back to her senses.[P] It embraced her like a gentle emotion, empowered by a song that wrapped itself around her.[P] The song flowed from beyond the darkness.[P] It surrounded her, flowed into her, and then, it began to flow out of her.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/HarpyTF/HarpyTF00", "Root/Images/Scenes/HarpyTF/HarpyTF01") ]])
fnCutscene([[ Append("It seeped through her body as it flowed through her.[P] It sang of power and sisterhood.[P] Of unity and the strength that unity brought.[P] The song spread through her and into her limbs as its chorus whispered into her mind about the world in which she would be born anew.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/HarpyTF/HarpyTF01", "Root/Images/Scenes/HarpyTF/HarpyTF02") ]])
fnCutscene([[ Append("It filled her arms and she could feel them changing.[P] The hairs on her arms growing thick like feathers.[P] They were the feathers of a kind who may have once soared above the clouds to watch even the greatest of prey from out of sight.[P] The nails of her fingers grew strong.[P] They were the talons of a predator.[P] The song moved on.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/HarpyTF/HarpyTF02", "Root/Images/Scenes/HarpyTF/HarpyTF03") ]])
fnCutscene([[ Append("It slid into her legs, and she could feel them growing.[P] She was a hunter.[P] She had always been a hunter.[P] Long hours of walking and jogging were of little consequence to her.[P] But now she could feel her legs growing longer.[P] Growing lighter.[P] Her feet grew longer, her toes stretching and her nails growing stronger.[B][C]") ]])
fnCutscene([[ Append("They were the talons of a predator who would wield them to fell the prey who threatened the sisterhood.[P] The song moved on.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/HarpyTF/HarpyTF03", "Root/Images/Scenes/HarpyTF/HarpyTF04") ]])
fnCutscene([[ Append("Then it moved to her eyes, and as the song seemed to fill them, she found she could open them.[P] They were the sharp eyes of a predator, eyes that could see would-be prey from far away.[P] With these new eyes she could see a soft glow coming from the world around her.[P] The walls seemed close.[P] Tight.[P] Restrictive.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/HarpyTF/HarpyTF04", "Root/Images/Scenes/HarpyTF/HarpyTF05") ]])
fnCutscene([[ Append("From beyond them, a soft light seeped in and all about moved shadows.[P] Her mind filled itself with these shadows and she recognized them without even seeing them.[P] These were her sisters, and she yearned to be with them.[P] To fight alongside them.[P] To strike at their prey and share a victory feast with them.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/HarpyTF/HarpyTF05", "Root/Images/Scenes/HarpyTF/HarpyTF06") ]])
fnCutscene([[ Append("She smiled to herself, then pulled her head back.[P] With a great thrust forward, she drove her beak into the glowing wall that separated her from the sisterhood.[P] Her beak pierced the veil and light streamed in as she pulled her head back.[B][C]") ]])
fnCutscene([[ Append("Strike after strike she delivered to the glowing wall that separated her from her sisters, and all the while the song came louder and louder until it seemed as if it were she herself singing it within her own confines.[P] With one final strike of her beak, she burst through the glowing wall.[P] The song reached its crescendo as it spilled from her own lips and the feeling of warmness that had enveloped her slipped away, replaced by the cool chill of the glacier.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/HarpyTF/HarpyTF06", "Root/Images/Scenes/HarpyTF/HarpyTF07") ]])
fnCutscene([[ Append("In its place remained the warmth of knowing she was a part of her sisterhood and the song that permeated her.[P] The song her sisters sang as she pulled herself from her egg to join them.[P] The song that spilled from her own lips as she shook herself free of the last of her egg and rose to be a part of the sisterhood that was now reborn into.") ]])
fnCutsceneBlocker()