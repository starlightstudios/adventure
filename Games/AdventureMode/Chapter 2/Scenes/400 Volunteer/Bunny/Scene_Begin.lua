-- |[ ================================== Volunteer to Bunny =================================== ]|
--Turns Sanya into a Bunny!

-- |[Variables]|
-- |[Image Streaming]|
fnLoadDelayedBitmapsFromList("Chapter 2 Sanya Bunny TF", gciDelayedLoadLoadAtEndOfTick)

-- |[ ==================================== Scene Execution ===================================== ]|
-- |[Dialogue]|
--Common.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/BunnyTF/BunnyTF00") ]])
fnCutscene([[ Append("Sanya leaned against the cool wood desk.[P] Her clothes lay crumpled in a pile against the wall, and the cool air of the caves enveloped her naked body.[P] Angel stood before her, still fully dressed, though the voluminous tufts of fur that rose from the top of her vest whispered of what lay hidden beneath.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/BunnyTF/BunnyTF00", "Root/Images/Scenes/BunnyTF/BunnyTF01") ]])
fnCutscene([[ Append("A brief pang of regret overcame Sanya at the thought of not being witness to the full figure of her soon-to-be matron, even if only for a brief while.[P] The regret was met with a pang of envy at the bunnyboys who would experience what she would not.[P] Envy, she realized, and a degree of anger.[P] She was stronger than them.[P] She could be their leader and the soft-furred Angelface would be hers.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/BunnyTF/BunnyTF01", "Root/Images/Scenes/BunnyTF/BunnyTF02") ]])
fnCutscene([[ Append("She sighed and pushed the thoughts from her mind.[P] She was not a conqueror, no matter how much power she held.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/BunnyTF/BunnyTF02", "Root/Images/Scenes/BunnyTF/BunnyTF03") ]])
fnCutscene([[ Append("Angel stepped forward, oblivious to Sanya's thoughts, and stepped to Sanya's side.[P] Her left hand came up to support Sanya's back while she brought her right hand to rest against the exposed and warm folds of Sanya's vagina.[P] The soft fur of Angel's hands, contrasted with the tough pads, provided a shifting sense of touch that drove her lust higher.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/BunnyTF/BunnyTF03", "Root/Images/Scenes/BunnyTF/BunnyTF06") ]])
fnCutscene([[ Append("'Try to keep yerself steady,' Angel said.[P] 'Even the strongest of my boys struggled to keep their feet as they transformed.'[P] She looked down, then, taking note of the thin string of sticky fluid that stretched from Sanya's flushed sex to her hand and shrugged.[P] 'One gal's business is another gal's pleasure, I suppose.'[B][C]") ]])
fnCutscene([[ Append("Sanya only laughed.[P] She had been mixing business and pleasure since her arrival on this world.[P] Stopping now seemed like a strange thought to her.[B][C]") ]])
fnCutscene([[ Append("Angel brought her hand back to Sanya's sex and began to rub in a slow circle over her lips.[P] Sanya could feel her muscles spasm lightly each time the bunnygirl's furred thumb brushed against her clit.[P] She let her head fall back, her short hair brushing her neck, and closed her eyes.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/BunnyTF/BunnyTF06", "Root/Images/Scenes/BunnyTF/BunnyTF07") ]])
fnCutscene([[ Append("She shook her head slowly, in time with the circles Angel rubbed against her, and let her hair brush against her shoulders and upper back.[P] It was another unusual sensation, she realized, where her skin was accustomed to the weighty and often rough fabric of her clothes but was now feeling a softer touch.[P] A softness that was not so unlike Angel's own touch.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/BunnyTF/BunnyTF07", "Root/Images/Scenes/BunnyTF/BunnyTF08") ]])
fnCutscene([[ Append("She let her mind drift in the medley of sensations.[P] The cool air and desk against the warm touch and breath of the bunnygirl and the rising heat of her own body.[P] The rough wood of the desk and rough pads against the softness of Angel's fur.[P] The memory of the familiar weight of her clothes against the angel's touch that was her hair.[B][C]") ]])
fnCutscene([[ Append("She sighed at the thought, the memories of when she believed angels surrounded her, protecting her.[P] How ironic, she thought to herself, that a far different Angel was now providing a similarly different form of protection.[P] A protection that would allow her to be free, to be herself.[P] To even be weak like prey like rabbits were, if she wanted.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/BunnyTF/BunnyTF08", "Root/Images/Scenes/BunnyTF/BunnyTF09") ]])
fnCutscene([[ Append("The visage of the rune on her runestone flashed in her mind at the thought.[P] Her eyes snapped open and her body grew rigid.[P] Beside her, still supporting her, Angel raised a single eyebrow at Sanya's tension, but otherwise continued her slow ministrations in silence.[B][C]") ]])
fnCutscene([[ Append("'I am not weak,' Sanya thought to herself.[P] 'I will never be prey.'") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/BunnyTF/BunnyTF09", "Root/Images/Scenes/BunnyTF/BunnyTF10") ]])
fnCutscene([[ Append("Sanya looked down at her body. [P]The light brown hair that speckled her body had begun to turn a snowy white, and more was growing in as she watched.[P] Thicker, heavier, softer.[P] 'But not weaker,' she told herself.[B][C]") ]])
fnCutscene([[ Append("'Faster,' she thought, 'and always stronger.'") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/BunnyTF/BunnyTF10", "Root/Images/Scenes/BunnyTF/BunnyTF11") ]])
fnCutscene([[ Append("She put one hand over Angel's and turned toward the bunnygirl.[P] Angel's eyebrow, still raised, crept higher.[P] Sanya took her waist with her other hand and pulled the now-startled rabbit closer.[B][C]") ]])
fnCutscene([[ Append("'Business IS pleasure,' she said.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/BunnyTF/BunnyTF11", "Root/Images/Scenes/BunnyTF/BunnyTF13") ]])
fnCutscene([[ Append("She pulled Angel tight to her, turning her hand to grasp the bunnygirl's own womanhood through her pants even as she pulled her into a kiss.[P] She let her eyes slip closed as she did so, and against her bare chest, she could feel Angel shrug.[P] The bunnygirl began to return her kiss, methodical and with a hint of disinterest.[B][C]") ]])
fnCutscene([[ Append("Sanya let her left hand slide upwards, her fingers slipping between the buttons on Angel's vest, the tips of her fingers, now nearly paws themselves, brushing against her lover's stiffening nipples.[B][C]") ]])
fnCutscene([[ Append("'No,' she told herself.[P] 'Not lover. Partner. A simple business transaction.'") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/BunnyTF/BunnyTF13", "Root/Images/Scenes/BunnyTF/BunnyTF14") ]])
fnCutscene([[ Append("Her mind, if it could, shrugged.[P] This pleasure WAS her business.[B][C]") ]])
fnCutscene([[ Append("She pushed the intrusive thoughts from her mind and let her body lose itself in the transaction.[P] Her hand popped two of the buttons on Angel's vest, letting the curly fur that adorned her breasts fall free.[P] Already she could feel her ears growing, shifting upward, and her hearing growing along with them.[B][C]") ]])
fnCutscene([[ Append("She could hear Angel's heart as it quickened, ever so slightly, even as she could feel her thrumming pulse tickling against the thickening white fur that now covered her.[P] She could hear Angel's breath grow deeper and could feel the heat against her face as she pulled away from the kiss.[P] She could even hear the creak of the bones in her legs and feet as they grew longer, wider, more accustomed to running than she ever had before.[P] And more capable of striding atop all but the softest of snow.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/BunnyTF/BunnyTF14", "Root/Images/Scenes/BunnyTF/BunnyTF15") ]])
fnCutscene([[ Append("She could feel herself grow taller as her feet changed in tempo with the heat that rose beneath Angel's skilled hand.[P] No longer would she need to raise her head to look into the bunnygirl's eyes.[P] They were even now, or perhaps, Sanya mused with smiling glee, she had become just a little taller than her partner.[B][C]") ]])
fnCutscene([[ Append("A height that added all the more volume for the first spark of her impending orgasm to fill.[B][C]") ]])
fnCutscene([[ Append("She tilted her head and brought her lips to Angel's neck and buried her face in the downy fur.[P] She let her lips explore for the skin beneath, skin that would be unaccustomed to direct touch beneath the fur, and felt Angel's own breath catch.[B][C]") ]])
fnCutscene([[ Append("The circles of Angel's touch grew quicker as she recovered from the sudden assault on her neck.[P] Sanya hastened her own small circles over the bunnygirl's pants and matched the motion with her other hand as she cupped her breast and teased Angel's nipple.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/BunnyTF/BunnyTF15", "Root/Images/Scenes/BunnyTF/BunnyTF16") ]])
fnCutscene([[ Append("The small spark that had been growing in her core began to flood into her body.[P] She was warm beneath the thick fur that now covered her and insulated despite the cool air that her quick breaths pulled into her lungs.[P] She could even feel Angel growing closer, though the bunnygirl did her best to hide it.[P] Sanya smiled at the effort, and just as she felt her core about to burst, an old memory struck her.[P] A memory of cats, dogs, and rabbits, and their ears.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/BunnyTF/BunnyTF16", "Root/Images/Scenes/BunnyTF/BunnyTF17") ]])
fnCutscene([[ Append("She pulled her face out of her partner's neck and tilted her head up ever so slightly, and just as her orgasm broke, she let the strength behind it push the air from her chest and across her lips, out to blow against Angel's ears.[B][C]") ]])
fnCutscene([[ Append("Angel shook from the act and her guarded demeanor fell just as her own orgasm broke and washed over her.[P] Her body shuddered against Sanya's as she struggled to regain her lost composure, but Sanya had already seen the quiet indication that, even beneath the bunnygirl's tough personality, she still took a degree of pleasure from Sanya's efforts.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/BunnyTF/BunnyTF17", "Root/Images/Scenes/BunnyTF/BunnyTF18") ]])
fnCutscene([[ Append("They separated, Sanya releasing her grip on Angel and Angel turning with a quiet huff as she adjusted her now-damp pants and buttoned her vest.[P] Sanya, in turn, grinned to herself.[P] She was still strong.[P] Even as one of the bunnygirls, she would not be prey.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/BunnyTF/BunnyTF18", "Root/Images/Scenes/BunnyTF/BunnyTF19") ]])
fnCutscene([[ Append("She crossed her arms as she pondered all that she could do with her new form and turned to lean against Angel's desk.[P] She was content in her nakedness and warm despite the cool air of the cave.[P] Just as she sat herself against the edge of the desk, she felt a sharp and pointed pain pinch at her fur.[P] She let out a startled yelp and twisted around as Angel, similarly startled, looked on.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/BunnyTF/BunnyTF19", "Root/Images/Scenes/BunnyTF/BunnyTF20") ]])
fnCutscene([[ Append("Behind her, caught in a small crack in the desk, was a tuft of fur.[P] Sanya looked over her shoulder and realized that her tail, small though it may be, was just high enough to be pinched when she sat if she were not careful.[B][C]") ]])
fnCutscene([[ Append("Angel chuckled beside her.[P] 'That's what ya get when you mix business with pleasure,' she said.[P] Then, with a shrug, she added, 'Still, it wasn't the worst I've ever had.'") ]])
fnCutsceneBlocker()

-- |[ ======================================== Clean Up ======================================== ]|
-- |[Unload Images]|
fnCutscene([[ fnUnloadBitmapsFromList("Chapter 2 Sanya Bunny TF") ]])
