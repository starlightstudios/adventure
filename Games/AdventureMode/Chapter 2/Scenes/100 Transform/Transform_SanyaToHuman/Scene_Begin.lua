-- |[Transform Sanya to Human]|
--Used at save points when transforming.

--Special: Block transformations.
--[=[
local bBlockTransformation = false
local iHasSeenOutlandBeeScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenOutlandBeeScene", "N")
local iHasBeeForm = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")
if(iHasSeenOutlandBeeScene == 0.0 and iHasBeeForm == 1.0) then
	fnStandardMajorDialogue()
	fnCutscene([[ Append("Mei:[E|Neutral] (Strange, something seems to be stopping me from transforming...)") ]])
	fnCutsceneBlocker()
	return
end
]=]

-- |[Variables]|
--Store which form the character started in.
local sStartingForm = VM_GetVar("Root/Variables/Global/Sanya/sForm", "S")

-- |[Achievement]|
--AM_SetPropertyJournal("Unlock Achievement", "BecomeAlraune")
    
-- |[Execute Transformation]|
--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Flash the active character to white. Immediately after, execute the transformation.
Cutscene_CreateEvent("Flash Sanya White", "Actor")
	ActorEvent_SetProperty("Subject Name", "Sanya")
	ActorEvent_SetProperty("Flashwhite Quickly")
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Sanya/Form_Human.lua") ]])
fnCutsceneWait(gci_Flashwhite_Ticks_Total)
fnCutsceneBlocker()

--Now wait a little bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Cutscene Execution]|
--If there are any cutscenes after a transformation, they go here.