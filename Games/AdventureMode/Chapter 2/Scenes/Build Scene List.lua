-- |[ ================================= Chapter 2 Scene Listing ================================ ]|
--Builds a list of cutscenes the player can make use of through the debug menu. Cutscenes in the debug menu are
-- used at the player's own risk, they may produce unusable situations that can only be remedied with the debug menu.
-- It's an addiction, I tell you.
--Note that not all cutscenes are necessarily on the debug list. There may also be cutscenes stored
-- in map folders that are not intended to be executed via the debug menu.

-- |[Setup]|
--Scene Listing variable
local sPrefix = ""
local saSceneList = {}

-- |[Adder Function]|
local fnAddScene = function(sPath)
    local i = #saSceneList + 1
    saSceneList[i] = sPath
end

-- |[ ====================================== Debug Scenes ====================================== ]|
sPrefix = "Chapter 2/Scenes/000 Debug/"
fnAddScene(sPrefix .. "Party Add Izuna")
fnAddScene(sPrefix .. "Party Rem Izuna")
fnAddScene(sPrefix .. "Party Add Zeke")
fnAddScene(sPrefix .. "Party Rem Zeke")
fnAddScene(sPrefix .. "Party Add Empress")
fnAddScene(sPrefix .. "Party Rem Empress")
fnAddScene(sPrefix .. "Quickset Done Shrine")
fnAddScene(sPrefix .. "Scene Take Care")
fnAddScene(sPrefix .. "Hotsprings Finale")
fnAddScene(sPrefix .. "Quickset Start Adventure")
fnAddScene(sPrefix .. "Quickset Unlock Pamphlets")

-- |[ ================================== Save Point TF Scenes ================================== ]|
sPrefix = "Chapter 2/Scenes/100 Transform/"

-- |[ ====================================== Defeat Scenes ===================================== ]|
sPrefix = "Chapter 2/Scenes/200 Defeat/"
fnAddScene(sPrefix .. "Defeat_BackToSave")

-- |[ ========================================= Upload ========================================= ]|
--Upload these to the debug menu.
local i = 1
ADebug_SetProperty("Cutscenes Total", #saSceneList)
while(saSceneList[i] ~= nil) do
	ADebug_SetProperty("Cutscene Path", i-1, saSceneList[i])
	i = i + 1
end
