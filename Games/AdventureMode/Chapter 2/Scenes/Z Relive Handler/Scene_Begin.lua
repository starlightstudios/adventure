-- |[ ===================================== Relive Handler ===================================== ]|
--Special script, used for (most) of the instances of the player choosing to relive a transformation scene.
-- The scene requires the name of the transformation to be passed to it. The C++ code will pass the DISPLAY NAME
-- of the scene in. We would therefore expect "Transform: Alraune", for example.

-- |[Argument Check]|
--Verify.
if(fnArgCheck(1) == false) then return end

--Set.
local sSceneName = LM_GetScriptArgument(0)

-- |[Storage]|
--Store party leader's current form.
local sSanyaForm = VM_GetVar("Root/Variables/Global/Sanya/sForm", "S")
VM_SetVar("Root/Variables/Chapter2/Scenes/sOriginalForm", "S", sSanyaForm)

--Indicate we are reliving a scene. This causes them to end earlier or at different times.
VM_SetVar("Root/Variables/Chapter2/Scenes/iIsRelivingScene", "N", 1.0)

-- |[Common]|
--Wait a bit.
fnCutsceneWait(10)
fnCutsceneBlocker()

--Common.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])

--Other party members:
--[=[
local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
if(bIsFlorentinaPresent == true) then
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
end
]=]

-- |[ ======================================== Execution ======================================= ]|
--[=[
-- |[Alraune]|
if(sSceneName == "Alraune" or sSceneName == "Alraune (V)") then
	
	--Flag this relive sequence.
	VM_SetVar("Root/Variables/Chapter1/Scenes/sLastRelivedScene", "S", "Alraune")
    
    --Block autosaves.
    VM_SetVar("Root/Variables/Global/Autosave/iBlockAutosave", "N", 1.0)
	
	--Dialogue.
	fnCutscene([[ Append("Mei:[E|Blush] (Ah, becoming a leaf-sister...)[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] (Such a wonderful memory...)") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Execute the scene.
    if(sSceneName == "Alraune") then
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/200 Defeat/Defeat_Alraune/Debug_Startup.lua") ]])
    else
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/400 Volunteer/Alraune Pitstop/Debug_Startup.lua") ]])
    end
]=]
end
