-- |[ =================================== Relive End Handler =================================== ]|
--After reliving a scene, return to the save point. Reset all party members and forms that may
-- have changed during the relive.
VM_SetVar("Root/Variables/Chapter2/Scenes/iIsRelivingScene", "N", 0.0)
    
--Unblock autosaves.
VM_SetVar("Root/Variables/Global/Autosave/iBlockAutosave", "N", 0.0)

-- |[ ================================== Re-Add Party Members ================================== ]|
--Reliving scenes may scramble the party order. Return to the original order here.


-- |[ ======================================= Form Reset ======================================= ]|
--The relived scene may have changed the form. Return it to the original here. This auto-handles
-- costumes as well.
local sOriginalForm = VM_GetVar("Root/Variables/Chapter2/Scenes/sOriginalForm", "S")
if(sOriginalForm == "Human") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Sanya/Form_Human.lua")
end

-- |[ ======================================== Dialogue ======================================== ]|
--After a scene is relived, the character reminisces.

-- |[Setup]|
--Dialogue setup. Place party opposed.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)

-- |[Alraune]|
local sLastRelivedScene = VM_GetVar("Root/Variables/Chapter2/Scenes/sLastRelivedScene", "S")
--[=[
if(sLastRelivedScene == "Alraune" or sLastRelivedScene == "AlrauneVolunteer") then
	
	--If Mei is alone:
	if(bIsFlorentinaHere == false) then
		fnCutscene([[ Append("Mei:[E|Neutral] (Such a wonderful feeling, being joined...)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] (But, I have to keep going.[P] Got to find a way home!)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] (Then I can join my very own leaf-sisters on Earth![P] Lord knows the environment could use some tending...)") ]])
	
	--If Florentina is here:
	else
		fnCutscene([[ Append("Mei:[E|Neutral] Mmmmm...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Were you spacing out or something?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I was just remembering what it was like to be joined.[P] Was it pleasant for you?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Blush] It did feel pretty great...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] It was a very long time ago, Mei.[P] In fact, I really only clearly remember the feeling as the waters soaked into me.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] As far as these things go, I guess Alraune is one of the better ones.[P] Life could certainly be a lot worse.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Yeah...") ]])
	end
end
]=]
fnCutsceneBlocker()
	