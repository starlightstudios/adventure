-- |[ ====================================== Defeat Bunny ====================================== ]|
--Cutscene proper. Uses the dialogue's SceneHandler to get the TF sequence. Scene is not eligible
-- for relives.

-- |[Repeat Check]|
--If this form has already been accounted for, run the standard game over.
local iSawFailedBunnyTF = VM_GetVar("Root/Variables/Chapter2/BunnyWarrens/iSawFailedBunnyTF", "N")
if(iSawFailedBunnyTF == 1.0) then
	LM_ExecuteScript(gsStandardGameOver)
	return
end

-- |[ ===================================== Map Transition ===================================== ]|
--If we're not on the correct map, switch maps and re-run this script.

-- |[Map Check]|
--Make sure we're in the cutscene map.
local sCurrentLevelName = AL_GetProperty("Name")
if(sCurrentLevelName ~= "WarrensCaveB") then

	--If not doing a relive, run the "revert" cutscene. This will show the player party hitting
    -- the ground and switching back to human form, if applicable.
	if(iIsRelivingScene == 0.0) then
        LM_ExecuteScript(gsStandardRevert)
	end

	--Transfer to correct map.
	local sString = "AL_BeginTransitionTo(\"WarrensCaveB\", \"" .. LM_GetCallStack(0) .. "\")"
	fnCutscene(sString)
	return
end

-- |[ ======================================= Scene Setup ====================================== ]|
-- |[Variables]|
local iEmpressJoined = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")

-- |[Image Streaming]|
--None.

-- |[Form]|
--Change the party lead back to human.
LM_ExecuteScript(gsRoot .. "FormHandlers/Sanya/Form_Human.lua")

-- |[Combat]|
--Restore party to full HP in case the player doesn't want to revisit a save point.
AdvCombat_SetProperty("Restore Party")

-- |[Topics]|
--Unlock these topics if they weren't already.
--WD_SetProperty("Unlock Topic", "Alraunes", 1)

-- |[Music]|
AL_SetProperty("Music", "Null")

-- |[Flags]|
--Mark this form as being unlocked.
VM_SetVar("Root/Variables/Chapter2/BunnyWarrens/iSawFailedBunnyTF", "N", 1.0)

-- |[Spawn NPCs]|
TA_Create("Bunny")
	TA_SetProperty("Position", 7, 7)
	TA_SetProperty("Facing", gci_Face_East)
	TA_SetProperty("Clipping Flag", true)
	fnSetCharacterGraphics("Root/Images/Sprites/Bunny/", false)
DL_PopActiveObject()

-- |[ ==================================== Scene Execution ===================================== ]|
-- |[Fading]|
--Snap to blackness.
fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

-- |[Position]|
fnCutsceneTeleport("Sanya", 9.25, 7.50)
fnCutsceneFace("Sanya", -1, 0)
fnCutsceneTeleport("Izuna", 14.25, 13.50)
fnCutsceneFace("Izuna", -1, 0)
fnCutsceneTeleport("Zeke", 6.25, 14.50)
fnCutsceneFace("Zeke", 1, 0)
if(iEmpressJoined == 1.0) then
    fnCutsceneTeleport("Empress", 6.25, 15.50)
    fnCutsceneFace("Empress", 1, 0)
end
fnCutsceneBlocker()
fnCutsceneWait(185)
fnCutsceneBlocker()

-- |[Fading]|
fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()

-- |[Scene]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Sad") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Bunny", "Neutral") ]])
fnCutscene([[ Append("Bunny:[E|Neutral] Well, here we are...[B][C]") ]])
fnCutscene([[ Append("Sanya:[E|Sad] I'm totally helpless, you could do anything you wanted to me...[B][C]") ]])
fnCutscene([[ Append("Bunny:[E|Neutral] Oh ho, anything?[B][C]") ]])
fnCutscene([[ Append("Sanya:[E|Cry] Anything at all![P] You might touch me, strip me naked![B][C]") ]])
fnCutscene([[ Append("Sanya:[E|Cry] Even transform me into a bunny, just like you![B][C]") ]])
fnCutscene([[ Append("Bunny:[E|Neutral] Well why don't you start by emptying your wallet.[B][C]") ]])
fnCutscene([[ Append("Sanya:[E|Neutral] Wallet?[P] Okay.[B][C]") ]])
fnCutscene([[ Append("Sanya:[E|Neutral] You want any Euros?[P] I don't know if anyone takes debit around here.[B][C]") ]])
fnCutscene([[ Append("Bunny:[E|Neutral] The hell is this?[B][C]") ]])
fnCutscene([[ Append("Sanya:[E|Laugh] A Danish Kroner![P] Don't worry, it's not even real money where I come from![B][C]") ]])
fnCutscene([[ Append("Bunny:[E|Neutral] Where's the platina!?[B][C]") ]])
fnCutscene([[ Append("Sanya:[E|Smirk] I gave it to Izuna.[P] Look are you going to turn me into a bunny or what?[B][C]") ]])
fnCutscene([[ Append("Bunny:[E|Neutral] Hell no![B][C]") ]])
fnCutscene([[ Append("Sanya:[E|Sad] Do I have to pay you?[B][C]") ]])
fnCutscene([[ Append("Bunny:[E|Neutral] No, I wouldn't turn you into a bunny anyway.[P] You're...[P] not my type.[B][C]") ]])
fnCutscene([[ Append("Sanya:[E|Offended] Not your type!?[P] What type is the right type!?[P] Turn me into a bunny![B][C]") ]])
fnCutscene([[ Append("Bunny:[E|Neutral] No![B][C]") ]])
fnCutscene([[ Append("Sanya:[E|Angry] BUNNY ME![B][C]") ]])
fnCutscene([[ Append("Bunny:[E|Neutral] Taff you![P] Later!") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

-- |[Movement]|
fnCutsceneMove("Bunny", 7.25, 10.50, 2.50)
fnCutsceneBlocker()
fnCutsceneFace("Sanya", 0, 1)
fnCutsceneMove("Bunny", 9.75, 11.50, 2.50)
fnCutsceneMove("Bunny", 9.75, 16.50, 2.50)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

-- |[Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
fnCutscene([[ Append("Sanya:[E|Angry] Get back here and bunny me![P] I didn't throw that fight for nothing!") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

-- |[Movement]|
fnCutsceneMove("Izuna", 10.25, 13.50)
fnCutsceneMove("Izuna", 10.25,  7.50)
fnCutsceneFace("Izuna", -1, 0)
fnCutsceneMove("Zeke",  9.25, 13.50)
fnCutsceneMove("Zeke",  9.25, 8.50)
fnCutsceneFace("Zeke",  0, -1)
if(iEmpressJoined == 1.0) then
    fnCutsceneMove("Empress", 9.75, 14.50)
    fnCutsceneFace("Empress",  0, 1)
end
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

-- |[Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Zeke", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Sanya", "Sad") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Izuna", "Neutral") ]])
fnCutscene([[ Append("Izuna:[E|Smirk] How did it go, honey?[P] Did you at least get a kiss?[B][C]") ]])
fnCutscene([[ Append("Sanya:[E|Offended] That bouncy bastard didn't even touch me![P] I didn't get the transformation, and now I'm down five euros![B][C]") ]])
fnCutscene([[ Append("Zeke:[E|Neutral] BAH!?[B][C]") ]])
fnCutscene([[ Append("Sanya:[E|Sad] I know, buddy.[P] Luckily the grass is free.[B][C]") ]])
fnCutscene([[ Append("Sanya:[E|Neutral] But I don't get it.[P] Isn't the whole thing about monstergirls that you get defeated, and then they turn you into one?[B][C]") ]])
fnCutscene([[ Append("Sanya:[E|Neutral] Did a very, very specific niche of the internet lie to me?[B][C]") ]])
fnCutscene([[ Append("Izuna:[E|Explain] Only some![P] Kitsunes don't forcibly transform people![B][C]") ]])
fnCutscene([[ Append("Sanya:[E|Neutral] But bunnies do?[B][C]") ]])
if(iEmpressJoined == 0.0) then
    fnCutscene([[ Append("Izuna:[E|Explain] ...[P] Evidently not![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] I'm going to get bunnied if it's the last thing I do![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] If a bunny has to shove a carrot in your ear I'll snap their wrist like a twig and guide the damn thing into my head![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] I don't think that will be necessary but I appreciate the enthusiasm.[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] So what now?[P] We need to hit those buns where they live.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] I'm not sure.[P] They're smugglers.[P] We've known about them for a long time, but they're evasive and don't do any real crimes here in Trafal.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] They tend to run illicit goods to the cities to the north and south.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] What sort of illicit goods?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] Drugs, stolen valuables, weapons, religious items that have been banned.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] We don't ban drugs or religious items here in Trafal, and it's not like anyone would enforce it even if we did.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] The weapons are more concerning, but valuables?[P] How can you tell a stolen valuable from a normal one?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] So there's no laws against it here, no one to enforce it, so they often run goods across Trafal.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Hm, that means they're wherever people aren't.[P] What's the most inhospitable place in the mountains?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] Westwoods, or the Ice Spires![P] Both are very dangerous even for prepared travellers![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Okay, we'll have to sniff around there.[P] Let's go.") ]])
    fnCutsceneBlocker()
    
else
    fnCutscene([[ Append("Izuna:[E|Offended] ...[P] Evidently not!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Empress", 9.75, 10.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Empress:[E|Neutral] We go to Westwoods next.[P] Acquiring the bunnygirl form is important, it will improve your dexterity greatly.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Why Westwoods?[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] I noted the brown mud on that bunny's feet.[P] Only the Westwoods area has it, and it stains the fur around it with green tinge that does not easily wash.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] I suspect their warrens are somewhere there, likely on the northern rim, where the water is still potable.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] You have very keen eyes, Empress![B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] I had a feeling this would happen, so I observed carefully.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] You had a feeling!?[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] The bunnies are a smuggling organization.[P] Why would they recruit random passerby?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] I thought you were about to say I was ugly.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] I said she has keen eyes, silly![P] You're the prettiest![B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] NYEH?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Blush] P-[P]prettiest girl, Zeke![P] You're the best boy no matter what![B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Whatever.[P] Let's get back to the campfire and hunt down that bunny freak.[P] Come on.") ]])
    fnCutsceneBlocker()
end
fnCutsceneWait(25)
fnCutsceneBlocker()

-- |[ ======================================== Clean Up ======================================== ]|
-- |[Unload Images]|
--fnCutscene([[ fnUnloadBitmapsFromList("Chapter 1 Mei Alraune TF") ]])

-- |[Transition]|
--Last save point.
fnCutscene([[ AL_BeginTransitionTo("LASTSAVE", "Null") ]])
fnCutsceneBlocker()