-- |[ ====================================== Defeat Bunny ====================================== ]|
--This lua file is only ever called if the cutscene is triggered via the debug menu. It will make sure
-- the map is correct and that the characters are present and in their default positions. It will also,
-- if necessary, set scripting flags so the cutscene makes sense.
Debug_PushPrint(true, "Debug Firing Cutscene: Defeat_Bunny\n")

--In debug mode, the scene plays even if we've already seen it. Set this flag so it does not skip.
VM_SetVar("Root/Variables/Chapter2/BunnyWarrens/iSawFailedBunnyTF", "N", 0.0)

--Otherwise, run the file normally. It may or may not change the map itself.
LM_ExecuteScript(fnResolvePath() .. "Scene_Begin.lua")

--Debug.
Debug_PopPrint("Completed debug cutscene firing.\n")
