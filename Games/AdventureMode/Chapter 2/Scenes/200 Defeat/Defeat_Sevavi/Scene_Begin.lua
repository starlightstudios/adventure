-- |[ ====================================== Defeat Statue ===================================== ]|
--Cutscene proper. Uses the dialogue's SceneHandler to get the TF sequence.

-- |[Variables]|
local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsRelivingScene", "N")

-- |[Repeat Check]|
--If this form has already been accounted for, run the standard game over.
local iHasSevaviForm = VM_GetVar("Root/Variables/Global/Sanya/iHasSevaviForm", "N")
if(iHasSevaviForm == 1.0 and iIsRelivingScene == 0.0) then
	LM_ExecuteScript(gsStandardGameOver)
	return
end

-- |[ ===================================== Map Transition ===================================== ]|
--If we're not on the correct map, switch maps and re-run this script.

-- |[Map Check]|
--Make sure we're in the cutscene map.
local sCurrentLevelName = AL_GetProperty("Name")
if(sCurrentLevelName ~= "LowerShrineG") then

	--If not doing a relive, run the "revert" cutscene. This will show the player party hitting
    -- the ground and switching back to human form, if applicable.
	if(iIsRelivingScene == 0.0) then
        LM_ExecuteScript(gsStandardRevert)
	end

	--Transfer to correct map.
	local sString = "AL_BeginTransitionTo(\"LowerShrineG\", \"" .. LM_GetCallStack(0) .. "\")"
	fnCutscene(sString)
	return
end

-- |[ ==================================== Scene Execution ===================================== ]|
-- |[Image Streaming]|
fnLoadDelayedBitmapsFromList("Chapter 2 Sanya Sevavi TF", gciDelayedLoadLoadAtEndOfTick)

-- |[Variables]|
local iEscapedShrine = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iEscapedShrine", "N")

-- |[Form]|
--Change the party lead back to human.
LM_ExecuteScript(gsRoot .. "FormHandlers/Sanya/Form_Human.lua")

-- |[Combat]|
--Restore party to full HP in case the player doesn't want to revisit a save point.
AdvCombat_SetProperty("Restore Party")

-- |[Topics]|
--Unlock these topics if they weren't already.
--WD_SetProperty("Unlock Topic", "Alraunes", 1)

-- |[Music]|
AL_SetProperty("Music", "Null")

-- |[Flags]|
--Mark this form as being unlocked.
VM_SetVar("Root/Variables/Global/Sanya/iHasSevaviForm", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iFriendsWithSevavi", "N", 1.0)

-- |[Image Streaming]|
fnLoadDelayedBitmapsFromList("Chapter 2 Sanya Sevavi TF", gciDelayedLoadLoadAtEndOfTick)

-- |[ ==================================== Scene Execution ===================================== ]|
-- |[Fading]|
--Snap to blackness.
fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

-- |[Scene]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ Append("Two statue women gripped Sanya firmly by the arms and dragged her out of the room, towards a gallery of empty pedestals.[P] Her head spun, her vision was blurred.[B][C]") ]])
fnCutscene([[ Append("They seemed to have arrived.[P] One of the statues hefted her and placed her in a standing position on a pedestal, the ones she had walked past on the way in.[B][C]") ]])
if(iEscapedShrine == 0.0) then
    fnCutscene([[ Append("Her vision cleared up as the statues held her still.[P] She looked around to try to spot the fallen cosplayer.[P] The other stone girl had placed her nearby, at the head of the stairs.[B][C]") ]])
else
    fnCutscene([[ Append("Her vision cleared up as the statues held her still.[P] She looked around to try to spot Izuna and Zeke.[P] The other stone girl had placed them nearby, at the head of the stairs.[P] They were hurt, but still alive.[B][C]") ]])
end

fnCutscene([[ Append("The two held her up and posed her on the pedestal, looking her over.[P] Somehow, as they did this, Sanya's wounds ceased to pulse with pain.[P] She felt calm.[P] Serene.[P] Everything was going to be fine.[B][C]") ]])
fnCutscene([[ Append("One of the statues felt over her body, tugging at her clothes.[P] They delicately removed them and left them in a heap on the floor.[P] Another statue appeared from out of her sight, and deposited a fresh set of the same sort of tan robes they wore at the base of the pedestal.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Null", "Root/Images/Scenes/SevaviTF/SevaviTF06") ]])
fnCutscene([[ Append("Sanya realized that she couldn't change her pose anymore.[P] She couldn't fight against the women undressing her.[P] It had been an uneven fight before, unarmed and trying to carry a wounded girl.[P] Now it was even but she seemed unable to move.[P] It didn't really bother her.[B][C]") ]])
fnCutscene([[ Append("Having disrobed her and left her posed on the pedestal, exposed to the world, the statues seemed satisfied.[P] One of them leaned to her ear and whispered, 'Dispose of that one.'[P] She pointed at the cosplayer on the ground.[P] Sanya could not so much as nod, but the statue woman seemed to accept her assent.[B][C]") ]])
fnCutscene([[ Append("The statues left her standing there, posed on the pedestal, and Sanya's mind began to wander.[P] She couldn't move, but it didn't seem to matter.[P] She was a beautiful woman, it was okay if people wanted to come and see her here.[P] She had nothing to be ashamed of.[P] Her body was a work of art, and she was the artist.[B][C]") ]])
fnCutscene([[ Append("But like any artist, there was always something that could be improved to make the final piece better.[P] Sanya thought about herself like an artist, and not an observer.[B][C]") ]])
fnCutscene([[ Append("[VOICE|Sanya]'Wouldn't you like a heart-shaped butt?'[P][VOICE|Narrator] she heard.[P] She cast a sideways glance at the unconscious cosplayer, but it was not her speaking.[B][C]") ]])
fnCutscene([[ Append("She responded to the voice in her thoughts.[P] Yes, she had always wanted a bigger, firmer rear.[P] The confirmation had barely formed itself before she felt a bit of herself harden.[P] Her skin became firmer, like stone, and her heatbeat slowed down.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/SevaviTF/SevaviTF06", "Root/Images/Scenes/SevaviTF/SevaviTF02") ]])
fnCutscene([[ Append("Again she thought of what she'd like to see in herself.[P] Maybe longer hair.[P] She had always wanted longer hair but it was so difficult to keep clean.[P] Tangles.[P] Frayed ends.[P] Dirt and dander.[P] If only it were easier to care for, she knew she would have grown her hair out.[B][C]") ]])
fnCutscene([[ Append("She felt a heaviness from her head and the delicate, firm strands of hair lengthen and tumble down her back.[P] Her skin had taken on a pale tone, and she began to feel a heavy weight pressing down on her.[P] She thought this odd, at first, until she realized that weight was simply her own body.[B][C]") ]])
fnCutscene([[ Append("[VOICE|Narrator]She changed her pose again to accentuate her new features. [VOICE|Sanya]'But don't you want a bigger chest?'[VOICE|Narrator] the voice asked.[P] Of course she did.[P] She wanted people to stare in admiration.[P] It was fun being sexy.[B][C]") ]])
fnCutscene([[ Append("She wondered again if the fox cosplayer had spoken, but she was still lying there, motionless.[B][C]") ]])
fnCutscene([[ Append("She felt her body grow and change again, her chest becoming larger and defying gravity.[P] Her waist narrowed slightly to move the mass around.[P] It was all the same now, not muscle or bone or flesh,[P] but stone.[B][C]") ]])
fnCutscene([[ Append("Like an artist who had finished her latest project, Sanya held her pose in triumph.[P] But who had been prompting her?[P] Was it the magic of the pedestal itself?[B][C]") ]])
fnCutscene([[ Append("No.[B][C]") ]])
fnCutscene([[ Append("Sanya realized the voice that had been speaking to her was her own.[P] She had been asking herself how she wanted to be, and becoming it, reshaping herself through the dissociation of the pedestal.[P] Able to see herself as if through a third eye.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/SevaviTF/SevaviTF02", "Root/Images/Scenes/SevaviTF/SevaviTF03") ]])
fnCutscene([[ Append("Now she had become a perfect work of art, a piece of raw material crafted into an object of true beauty.[P] And if she could, she'd stand on the pedestal to be admired for all time.[B][C]") ]])
if(iEscapedShrine == 0.0) then
    fnCutscene([[ Append("But, with a heavy stone heart, she had to descend.[P] The cosplayer needed her help, and Zeke was waiting for her.[P] People were relying on her.[P] There would always be more time to be still and admired later, when her duties were seen to.[B][C]") ]])
else
    fnCutscene([[ Append("But, with a heavy stone heart, she had to descend.[P] Zeke and Izuna needed her help.[P] People were relying on her.[P] There would always be more time to be still and admired later, when her duties were seen to.[B][C]") ]])
end
fnCutscene([[ Append("As she left, she cast a forlorn glance back at the pedestal.[P] Maybe a nip here, a tuck there.[P] No work of art couldn't be improved on, right?") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/SevaviTF/SevaviTF03", "Root/Images/Scenes/SevaviTF/SevaviTF04") ]])
fnCutscene([[ Append("She picked up and donned the clothes the statues had left for her.[P] They really brought attention to the parts most needing it, and fit comfortably.[P] It was a fine gesture by the statues, and she smiled to herself.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/SevaviTF/SevaviTF04", "Root/Images/Scenes/SevaviTF/SevaviTF05") ]])
fnCutscene([[ Append("Dressing herself in the gloom, she realized her eyes now glowed softly in the darkness with a firey red.[P] Her eyes had lit up like rubies.[B][C]") ]])
if(iEscapedShrine == 0.0) then
	fnCutscene([[ Append("She felt healthier and stronger than she ever had before.[P] She had been ordered to 'dispose' of the cosplayer, but the statues didn't give her orders.[P] Nobody did.[P] They had made a big mistake, lending her all their power...") ]])
else
    fnCutscene([[ Append("Izuna was taking in the display, and was excited to see the changes.[P] Sanya couldn't wait to let her explore her new body...") ]])
end

-- |[ ======================================== Clean Up ======================================== ]|
-- |[Special]|
--If this is a reliving sequence, end it here.
if(iIsRelivingScene == 1.0) then
	
	--Wait a bit.
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--Return to the last save point and execute the post-script..
	fnCutscene([[ AL_BeginTransitionTo("LASTSAVE", gsStandardReliveEnd) ]])
	fnCutsceneBlocker()

-- |[Finish Up]|
--Non-relive scene.
else

    -- |[Form]|
    --Switch to statue form.
    fnCutsceneCall(gsRoot .. "FormHandlers/Sanya/Form_Sevavi.lua")
    fnCutsceneSetFrame("Sanya", "Null")

    -- |[Position]|
    --Only Sanya needs to be placed.
    fnCutsceneTeleport("Sanya", 7.25, 6.50)
    fnCutsceneFace("Sanya", 0, 1)
    
    -- |[Fading]|
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneWait(45)
    fnCutsceneBlocker()

    -- |[Dialogue]|
    --Defeated while escaping.
    if(iEscapedShrine == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Huh.[P] Now I guess I know why she wanted me to 'dispose' of the cosplayer.[P] They think I'm one of them.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Suckers![P] I'm still Sanya, but now hotter and more dangerous than before![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Still, better get that cosplayer to safety.[P] Didn't see another way out, so I have to go back through there.[P] They should leave me alone now, though.") ]])
        fnCutsceneBlocker()

    --Escaped and returned.
    else

        -- |[Position]|
        fnCutsceneTeleport("Sanya", 7.25, 6.50)
        fnCutsceneTeleport("Izuna", 6.25, 6.50)
        fnCutsceneTeleport("Zeke", 7.25, 7.50)
        fnCutsceneFace("Sanya", -1, 0)
        fnCutsceneFace("Izuna", 1, 0)
        fnCutsceneFace("Zeke", 0, -1)
        fnCutsceneSetFrame("Sanya", "Null")
        fnCutsceneSetFrame("Izuna", "Null")
        fnCutsceneSetFrame("Zeke", "Null")

        -- |[Fading]|
        fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
        fnCutsceneWait(45)
        fnCutsceneBlocker()

        -- |[Dialogue]|
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Sanya:[E|Happy] Hoo-wa, what a rush![P] I feel incredible![B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Blush] You look incredible![B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] You doing okay?[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Neutral] Just winded.[P] I don't think they meant to hit us that hard.[B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh...[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Smirk] Here, let me see...[P] I can heal you too, Zeke.[P] Hold still.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Happy] Well, joke's on them![P] Now I have their power, and they won't suspect us at all![B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Ugh] Is there a way to get a new form for you that doesn't involve us getting beaten up?[P] Can we do that next time?[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Choice is reserved for those with power, or something.[P] Which I have now, so sure.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Now they wanted me to 'dispose' of you.[P] So if anyone asks, I'm on the way to do that.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Ugh] I suppose we got what we wanted.[P] They dragged us over to the altar.[P] It's just north of here.[P] Let's go!") ]])
        fnCutsceneBlocker()

        -- |[Fold Party]|
        fnAutoFoldParty()
        fnCutsceneBlocker()
    end
end

-- |[ ======================================== Clean Up ======================================== ]|
-- |[Unload Images]|
fnCutscene([[ fnUnloadBitmapsFromList("Chapter 2 Sanya Sevavi TF") ]])
