-- |[ =================================== Menu Close Handler =================================== ]|
--Whenever the player closes the pause menu, this script gets executed. This is used to allow the
-- player to equip the harpy badge and make harpies no longer target the party.
if(fnArgCheck(1) == false) then return end
local sSwitchType = LM_GetScriptArgument(0)

-- |[ ==================================== Change Equipment ==================================== ]|
--Only handle this when changing equipment.
if(sSwitchType ~= "Change Equipment") then return end

--Don't run the pulse during the hunting tutorial as it will respawn enemies when there should be none.
local iIsTutorialHuntActive = VM_GetVar("Root/Variables/Chapter2/Northwoods/iIsTutorialHuntActive", "N")
if(iIsTutorialHuntActive == 1.0) then return end

--Pulse.
fnStandardEnemyPulse()
