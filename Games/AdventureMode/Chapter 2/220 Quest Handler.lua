-- |[ ====================================== Quest Handler ===================================== ]|
--Whenever the journal is opened to quest mode, generates quest entries.
local sQuestPath = gsRoot .. "Chapter 2/Journal Quests/"

-- |[Setup]|
--Create chart for storage.
gzaCh2QuestEntries = {}

--Set active globals. This reduces the number of arguments that need to be passed.
JournalEntry.zaAppendList = gzaCh2QuestEntries

-- |[Call]|
LM_ExecuteScript(sQuestPath .. "000 Main Quest.lua")
LM_ExecuteScript(sQuestPath .. "010 Kitsune Village.lua")
LM_ExecuteScript(sQuestPath .. "020 The Bunnies.lua")
LM_ExecuteScript(sQuestPath .. "030 Greenery in Westwoods.lua")
LM_ExecuteScript(sQuestPath .. "040 Merchant Cart.lua")
LM_ExecuteScript(sQuestPath .. "050 Tomb Rumours.lua")

-- |[ ============== Finish Up =============== ]|
-- |[Assemble List]|
for i = 1, #gzaCh2QuestEntries, 1 do
    gzaCh2QuestEntries[i]:fnUploadData()
end

-- |[Clean]|
--Unsets globals.
JournalEntry:fnCleanGlobals()

--Clear the list to save memory.
gzaCh2QuestEntries = nil
