-- |[ ========================================= Sonoko ========================================= ]|
--Warrior kitsune near the farm in Northwoods.

-- |[ ============ Setup =========== ]|
--Temp Variables.
local sBaseProfile  = ""
local sNeutralPath  = ""
local sInternalName = "Sonoko"
local sDisplayName  = "Sonoko"
local sDialogueName = "KitsuneC"

--Data Library Variables.
local bShowAllEntries = AM_GetPropertyJournal("Show All Entries")
local iMetSonoko = VM_GetVar("Root/Variables/Chapter2/KitsuneVillage/iMetSonoko", "N")

--Get dialogue portrait.
DialogueActor_Push(sDialogueName)
    sNeutralPath = DialogueActor_GetProperty("Portrait Path", "Neutral")
DL_PopActiveObject()

-- |[ ========== Activity ========== ]|
--Profile may not appear at all.
if(iMetSonoko == 0.0 and bShowAllEntries == false) then
    local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
    zProfileEntry:fnBlankProfile()
    return
end

-- |[ ====== Profile Handling ====== ]|
-- |[Basic Profile]|
--The profile that most cases have in common.
sBaseProfile = 
"One of the warrior kitsunes of Trafal,[BR]Sonoko is frequently abroad, seeking to[BR]make the world a better place by[BR]protecting the weak and bringing[BR]criminals to justice.[BR][BR]" .. 
"She has seen the world outside of[BR]Trafal become more dangerous. Crops[BR]wilt, the winds do not blow, and people[BR]everywhere become desperate. She has[BR]returned to Trafal to seek " .. 
"the elder's [BR]counsel.[BR][BR]" .. 
"She is currently protecting a farm near[BR]the kitsune temple from the various[BR]brigands on the glacier."

-- |[Variations and Additions]|
--None.

-- |[ =========== Upload =========== ]|
local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
zProfileEntry:fnSetToProfile(sNeutralPath, 0, 0)
zProfileEntry:fnSetDescription(sBaseProfile)
