-- |[ ========================================== Zeke ========================================== ]|
--What a handsome goat.

-- |[ ============ Setup =========== ]|
--Temp Variables.
local sBaseProfile  = ""
local sNeutralPath  = ""
local sInternalName = "Zeke"
local sDisplayName  = "Zeke"
local sDialogueName = "Zeke"

--Data Library Variables.
local bShowAllEntries = AM_GetPropertyJournal("Show All Entries")
local iSawZeke = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iSawZeke", "N")
local iCompletedHunt = VM_GetVar("Root/Variables/Chapter2/Northwoods/iCompletedHunt", "N")

--Get dialogue portrait.
DialogueActor_Push(sDialogueName)
    sNeutralPath = DialogueActor_GetProperty("Portrait Path", "Neutral")
DL_PopActiveObject()

-- |[ ========== Activity ========== ]|
--Profile may not appear at all.
if(iSawZeke == 0.0 and bShowAllEntries == false) then
    local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
    zProfileEntry:fnBlankProfile()
    return
end

-- |[ ====== Profile Handling ====== ]|
-- |[Basic Profile]|
--The profile that most cases have in common.
sBaseProfile = 
"Sanya's adorable pet goat. He is three[BR]years old and has spent most of his life[BR]living on the Pavletic's farm.[BR][BR]" .. 
"Sanya maintains that he is extremely[BR]intelligent despite his being a goat. He[BR]seeks to disprove this at every[BR]opportunity.[BR][BR]"

-- |[Variations and Additions]|
--Adds a blurb about Pandemonium.
if(iCompletedHunt == 1.0) then
    sBaseProfile = sBaseProfile .. "It is unknown why he made the trip to[BR]Pandemonium alongside Sanya. If he[BR]knows, he's not talking."
end

-- |[ =========== Upload =========== ]|
local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
zProfileEntry:fnSetToProfile(sNeutralPath, 0, 0)
zProfileEntry:fnSetDescription(sBaseProfile)
