-- |[ ========================================= Takahn ========================================= ]|
--FIRE!

-- |[ ============ Setup =========== ]|
--Temp Variables.
local sBaseProfile  = ""
local sNeutralPath  = ""
local sInternalName = "Takahn"
local sDisplayName  = "Takahn"
local sDialogueName = "Takahn"

--Data Library Variables.
local bShowAllEntries = AM_GetPropertyJournal("Show All Entries")
local iMetTakahn = VM_GetVar("Root/Variables/Chapter2/Westwoods/iMetTakahn", "N")

--Get dialogue portrait.
DialogueActor_Push(sDialogueName)
    sNeutralPath = DialogueActor_GetProperty("Portrait Path", "Neutral")
DL_PopActiveObject()

-- |[ ========== Activity ========== ]|
--Profile may not appear at all.
if(iMetTakahn == 0.0 and bShowAllEntries == false) then
    local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
    zProfileEntry:fnBlankProfile()
    return
end

-- |[ ====== Profile Handling ====== ]|
-- |[Basic Profile]|
--The profile that most cases have in common.
sBaseProfile = 
"Found in Westwoods working as a[BR]mercenary protecting a merchant[BR]caravan. He appears to be a flame mage,[BR]given all the scorched wood and singed[BR]dirt."

-- |[Variations and Additions]|
--None.

-- |[ =========== Upload =========== ]|
local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
zProfileEntry:fnSetToProfile(sNeutralPath, 0, 0)
zProfileEntry:fnSetDescription(sBaseProfile)
