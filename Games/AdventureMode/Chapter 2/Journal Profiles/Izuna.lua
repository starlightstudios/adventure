-- |[ ========================================= Izuna ========================================== ]|
--Who doesn't want a cute fox girlfriend?

-- |[ ============ Setup =========== ]|

--Temp Variables.
local sBaseProfile  = ""
local sNeutralPath  = ""
local sInternalName = "Izuna"
local sDisplayName  = "Izuna"
local sDialogueName = "Izuna"

--Data Library Variables.
local bShowAllEntries = AM_GetPropertyJournal("Show All Entries")
local iSwissPeople = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iSwissPeople", "N")
local iCompletedHunt = VM_GetVar("Root/Variables/Chapter2/Northwoods/iCompletedHunt", "N")

--Get dialogue portrait.
DialogueActor_Push(sDialogueName)
    sNeutralPath = DialogueActor_GetProperty("Portrait Path", "Neutral")
DL_PopActiveObject()

-- |[ ========== Activity ========== ]|
--Profile may not appear at all.
if(iSwissPeople == 0.0 and bShowAllEntries == false) then
    local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
    zProfileEntry:fnBlankProfile()
    return
end

-- |[ ====== Profile Handling ====== ]|
-- |[Basic Profile]|
--The profile that most cases have in common.
sBaseProfile = 
"A beautiful kitsune miko from Trafal.[BR]Believing that the time of[BR]the return of the great evil was nigh as[BR]foretold by an ancient legend, she[BR]ventured to the Shrine of the Hero. She[BR]was caught "..
"by the guards and thrown[BR]off a bridge, only to be rescued by[BR]Sanya, who she believes is the hero of[BR]legend. Also they kissed.[BR][BR]" ..
"Brash and reckless, she tends to act first[BR]and let her brain catch up afterwards.[BR]Despite this, she is extremely intelligent,[BR]and memorizes details easily. This has[BR]served her well in her role "..
"as a [BR]journalist in Trafal.[BR][BR]" .. 
"Like all mikos, she is trained in the[BR]healing arts. Her magic can restore[BR]health, remove poisons, and buff her[BR]allies, but she is not particularly strong.[BR][BR]" .. 
"She is pursuing a relationship with Sanya, and they are just[BR]nauseatingly cute together."

-- |[Variations and Additions]|
--Izuna hasn't woken up, so Sanya knows nothing about her.
if(iCompletedHunt == 0.0) then

    --Modify display name.
    sDisplayName = "Cosplayer"

    --Profile.
    sBaseProfile = "A strange woman, possibly from a local[BR]furry or anime convention, wearing[BR]bizarrely realistic fox cosplay.[BR][BR]" .. 
                   "She fell from above an underground[BR]river and slammed into a stone bridge,[BR]breaking a lot of bones. Sanya rescued[BR]her from the river but she fell into a[BR]coma."

end

-- |[ =========== Upload =========== ]|
local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
zProfileEntry:fnSetToProfile(sNeutralPath, 0, 0)
zProfileEntry:fnSetDescription(sBaseProfile)
