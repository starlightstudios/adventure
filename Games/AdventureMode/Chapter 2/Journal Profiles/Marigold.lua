-- |[ ======================================== Marigold ======================================== ]|
--I love marigolds. They're very frost resistant!

-- |[ ============ Setup =========== ]|
--Temp Variables.
local sBaseProfile  = ""
local sNeutralPath  = ""
local sInternalName = "Marigold"
local sDisplayName  = "Marigold"
local sDialogueName = "Marigold"

--Data Library Variables.
local bShowAllEntries = AM_GetPropertyJournal("Show All Entries")
local iBefriendedAlraunes = VM_GetVar("Root/Variables/Chapter2/FortKirysu/iBefriendedAlraunes", "N")

--Get dialogue portrait.
DialogueActor_Push(sDialogueName)
    sNeutralPath = DialogueActor_GetProperty("Portrait Path", "Neutral")
DL_PopActiveObject()

-- |[ ========== Activity ========== ]|
--Profile may not appear at all.
if(iBefriendedAlraunes == 0.0 and bShowAllEntries == false) then
    local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
    zProfileEntry:fnBlankProfile()
    return
end

-- |[ ====== Profile Handling ====== ]|
-- |[Basic Profile]|
--The profile that most cases have in common.
sBaseProfile = 
"Some time ago, Marigold and the[BR]alraunes in her covenant began to have[BR]strange dreams about Trafal. They[BR]journeyed to Westwoods and found a[BR]single tree growing in a lake of pure[BR]water, "..
"surrounded by blasted land.[BR][BR]"..
"She and her covenant are now dedicated[BR]to helping this tree purify Westwoods."

-- |[Variations and Additions]|
--None.

-- |[ =========== Upload =========== ]|
local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
zProfileEntry:fnSetToProfile(sNeutralPath, 0, 0)
zProfileEntry:fnSetDescription(sBaseProfile)
