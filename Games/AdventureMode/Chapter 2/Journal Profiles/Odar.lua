-- |[ ========================================== Odar ========================================== ]|
--What a nifty outfit.

-- |[ ============ Setup =========== ]|
--Temp Variables.
local sBaseProfile  = ""
local sNeutralPath  = ""
local sInternalName = "Odar"
local sDisplayName  = "Odar"
local sDialogueName = "Odar"

--Data Library Variables.
local bShowAllEntries = AM_GetPropertyJournal("Show All Entries")
local iDidPuzzleFight = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iDidPuzzleFight", "N")

--Get dialogue portrait.
DialogueActor_Push(sDialogueName)
    sNeutralPath = DialogueActor_GetProperty("Portrait Path", "Neutral")
DL_PopActiveObject()

-- |[ ========== Activity ========== ]|
--Profile may not appear at all.
if(iDidPuzzleFight == 0.0 and bShowAllEntries == false) then
    local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
    zProfileEntry:fnBlankProfile()
    return
end

-- |[ ====== Profile Handling ====== ]|
-- |[Basic Profile]|
--The profile that most cases have in common.
sBaseProfile = 
"A mysterious man who appears to be a[BR]floating mushroom. He claims he is of[BR]fey heritage.[BR]Whatever the hell that means.[BR][BR]" .. 
"Despite being kind of a dink, he did[BR]recover Sanya's equipment and deliver[BR]it to her, so he can't be all bad.[BR][BR]" .. 
"Unfortunately it was part of a 'trap'. He[BR]has some kind of issue with Sanya for[BR]unknown reasons.[BR][BR]" .. 
"Despite any misgivings, his face is[BR]amazingly punchable."

-- |[Variations and Additions]|
--None.

-- |[ =========== Upload =========== ]|
local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
zProfileEntry:fnSetToProfile(sNeutralPath, 0, 0)
zProfileEntry:fnSetDescription(sBaseProfile)
