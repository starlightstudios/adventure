-- |[ ========================================== Miso ========================================== ]|
--Once a mighty warrior, she can probably beat you with a soup ladel even if she's given up that life.

-- |[ ============ Setup =========== ]|
--Temp Variables.
local sBaseProfile  = ""
local sNeutralPath  = ""
local sInternalName = "Miso"
local sDisplayName  = "Miso"
local sDialogueName = "Miso"

--Data Library Variables.
local bShowAllEntries = AM_GetPropertyJournal("Show All Entries")
local iMetMiso = VM_GetVar("Root/Variables/Chapter2/Miso/iMetMiso", "N")

--Get dialogue portrait.
DialogueActor_Push(sDialogueName)
    sNeutralPath = DialogueActor_GetProperty("Portrait Path", "Neutral")
DL_PopActiveObject()

-- |[ ========== Activity ========== ]|
--Profile may not appear at all.
if(iMetMiso == 0.0 and bShowAllEntries == false) then
    local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
    zProfileEntry:fnBlankProfile()
    return
end

-- |[ ====== Profile Handling ====== ]|
-- |[Basic Profile]|
--The profile that most cases have in common.
sBaseProfile = 
"A former kitsune warrior turned[BR]craftsfox, Miso took her name after her[BR]delicious soups. She loves cooking,[BR]baking, and sampling new and exotic[BR]spices.[BR][BR]" .. 
"In the habit of the kitsune warriors, she[BR]can be found wandering the roads of[BR]Trafal. Except instead of vanquishing[BR]foes, she's more likely to make a hungry[BR]traveller some lunch.[BR][BR]" .. 
"As a friend of Izuna's, she has[BR]volunteered her skills to help on Sanya's[BR]journey. She can take various materials[BR]found around the glacier and turn them[BR]into unique armors and items."

-- |[Variations and Additions]|
--None.

-- |[ =========== Upload =========== ]|
local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
zProfileEntry:fnSetToProfile(sNeutralPath, 0, 0)
zProfileEntry:fnSetDescription(sBaseProfile)
