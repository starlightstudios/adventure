-- |[ ========================================== Sanya ========================================= ]|
--Sanya! Hero of the chapter, bearer of Patience. She have gun and blammo kaboom. Always appears in the journal.

-- |[ ============ Setup =========== ]|
--External.
local bShowAllEntries = AM_GetPropertyJournal("Show All Entries")

--Temp Variables.
local sBaseProfile  = ""
local sNeutralPath  = ""
local sInternalName = "Sanya"
local sDisplayName  = "Sanya"
local sDialogueName = "Sanya"

--Data Library Variables.
local sSanyaForm = VM_GetVar("Root/Variables/Global/Sanya/sForm", "S")

--Get dialogue portrait.
DialogueActor_Push("Sanya")
    sNeutralPath = DialogueActor_GetProperty("Portrait Path", "Neutral")
DL_PopActiveObject()

-- |[ ====== Profile Handling ====== ]|
-- |[Basic Profile]|
--The profile that most cases have in common.
sBaseProfile = 
"A 23 year old clothing salesperson from[BR]western Croatia. She grew up on a rural[BR]farm in the mountains, the youngest of[BR]five. She constantly roughoused with[BR]her older brothers and knows " .. 
"lots about[BR]fighting, grabs, holds, and throws.[BR][BR]" .. 
"She has very little education, with much[BR]of her country in shambles after the[BR]independence wars of the 1990's. Her[BR]family was poor and could not afford to[BR]send her abroad.[BR][BR]" .. 
"She instead learned the land, and knows[BR]about crops, soil, and animals. Her[BR]pet goat, Zeke, has been her companion[BR]the last few years when she isn't riding[BR]"..
"a bike to work at her part-time job.[BR][BR]" .. 
"Her rifle, a British-made Enfield, was[BR]paradropped to Croatian partisans[BR]during the second world war. Her[BR]great-grandfather notched it nine times. She hopes to add more[BR]notches of her own someday."

-- |[Variations and Additions]|
--Human form.
if(sSanyaForm == "Human") then
    --No changes.

--Sevavi.
elseif(sSanyaForm == "Sevavi") then
    
    sBaseProfile = sBaseProfile .. "[BR][BR]" .. 
    "Transformed into a living statue, her durability, power, and bust size[BR]have increased, but she otherwise barely has changed."

--Werebat.
elseif(sSanyaForm == "Werebat") then
    
    sBaseProfile = sBaseProfile .. "[BR][BR]" .. 
    "An untreated werebat curse overtook her and transformed her into a[BR]batgirl. Despite this, her appetite for fruit is only slightly increased.[BR]She can echolocate in the dark and smell blood at great distances."
    
--Harpy.
elseif(sSanyaForm == "Harpy") then
    
    sBaseProfile = sBaseProfile .. "[BR][BR]" .. 
    "Now a harpy, her thoughts are more regimented but much the same.[BR]She understands harpy culture and social mores but still ignores them[BR]because, come on. Does she look like she cares about that stuff?"

--Bunny.
elseif(sSanyaForm == "Bunny") then
    
    sBaseProfile = sBaseProfile .. "[BR][BR]" .. 
    "Having gained the speed of a bunny as well as their fondness for fresh[BR]veggies and crime, she is faster than before and nothing else."
end

-- |[ =========== Upload =========== ]|
local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
zProfileEntry:fnSetToProfile(sNeutralPath, 0, 0)
zProfileEntry:fnSetDescription(sBaseProfile)
