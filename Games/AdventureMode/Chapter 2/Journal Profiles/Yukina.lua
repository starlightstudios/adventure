-- |[ ========================================= Yukina ========================================= ]|
--OLD PEOPLE.

-- |[ ============ Setup =========== ]|
--Temp Variables.
local sBaseProfile  = ""
local sNeutralPath  = ""
local sInternalName = "Yukina"
local sDisplayName  = "Yukina"
local sDialogueName = "Yukina"

--Data Library Variables.
local bShowAllEntries = AM_GetPropertyJournal("Show All Entries")
local iRescuedIzuna = VM_GetVar("Root/Variables/Chapter2/KitsuneVillage/iRescuedIzuna", "N")

--Get dialogue portrait.
DialogueActor_Push(sDialogueName)
    sNeutralPath = DialogueActor_GetProperty("Portrait Path", "Neutral")
DL_PopActiveObject()

-- |[ ========== Activity ========== ]|
--Profile may not appear at all.
if(iRescuedIzuna == 0.0 and bShowAllEntries == false) then
    local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
    zProfileEntry:fnBlankProfile()
    return
end

-- |[ ====== Profile Handling ====== ]|
-- |[Basic Profile]|
--The profile that most cases have in common.
sBaseProfile = 
"Current elder of the kitsune temple in[BR]Trafal, she is the wisest there, with[BR]eight tails.[BR][BR]" .. 
"She urges caution, believing that Sanya[BR]is not the hero of legend, and that the[BR]legend itself is a mere fairy-tale. To[BR]convince her, she has advised Sanya and[BR]her friends to " .. 
"complete the Trials of the[BR]Dragon.[BR][BR]" .. 
"Kind and intelligent, she is a font of[BR]wisdom. She loves many kinds of tea[BR]and sitting around a nice fire and[BR]swapping stories."

-- |[Variations and Additions]|
--None.

-- |[ =========== Upload =========== ]|
local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
zProfileEntry:fnSetToProfile(sNeutralPath, 0, 0)
zProfileEntry:fnSetDescription(sBaseProfile)
