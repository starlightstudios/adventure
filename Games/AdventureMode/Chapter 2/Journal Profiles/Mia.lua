-- |[ =========================================== Mia ========================================== ]|
--She's got a brain, fashion sense, and cute little glasses!

-- |[ ============ Setup =========== ]|
--Temp Variables.
local sBaseProfile  = ""
local sNeutralPath  = ""
local sInternalName = "Mia"
local sDisplayName  = "Mia"
local sDialogueName = "Mia"

--Data Library Variables.
local bShowAllEntries = AM_GetPropertyJournal("Show All Entries")
local iMetMia = VM_GetVar("Root/Variables/Chapter2/Mia/iMetMia", "N")

--Get dialogue portrait.
DialogueActor_Push(sDialogueName)
    sNeutralPath = DialogueActor_GetProperty("Portrait Path", "Neutral")
DL_PopActiveObject()

-- |[ ========== Activity ========== ]|
--Profile may not appear at all.
if(iMetMia == 0.0 and bShowAllEntries == false) then
    local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
    zProfileEntry:fnBlankProfile()
    return
end

-- |[ ====== Profile Handling ====== ]|
-- |[Basic Profile]|
--The profile that most cases have in common.
sBaseProfile = 
"Editor of the Nine Tails Post and[BR]long-time friend of Izuna's, Mia has a[BR]sharp wit and a sharper pen. She works[BR]the printing press in the kitsune temple,[BR]and distributes the " ..
"informational[BR]pamphlets through traders far and wide.[BR][BR]" .. 
"She is always on the lookout for a new[BR]scoop to send Izuna on, and knows all[BR]the rumours on the glacier. If you need[BR]to know something, she's your go-to.[BR][BR]" .. 
"She can usually be found in her office at[BR]the Nine Tails Post, or visiting Poterup[BR]village to get supplies."

-- |[Variations and Additions]|
--None.

-- |[ =========== Upload =========== ]|
local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
zProfileEntry:fnSetToProfile(sNeutralPath, 0, 0)
zProfileEntry:fnSetDescription(sBaseProfile)
