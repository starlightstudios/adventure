-- |[ ======================================== Empress ========================================= ]|
--She's like 8 feet tall.

-- |[ ============ Setup =========== ]|
--Temp Variables.
local sBaseProfile  = ""
local sNeutralPath  = ""
local sInternalName = "Empress"
local sDisplayName  = "Empress"
local sDialogueName = "Empress"

--Data Library Variables.
local bShowAllEntries = AM_GetPropertyJournal("Show All Entries")
local iEmpressJoined = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
local iMetEmpress = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iMetEmpress", "N")

--Get dialogue portrait.
DialogueActor_Push(sDialogueName)
    sNeutralPath = DialogueActor_GetProperty("Portrait Path", "Neutral")
DL_PopActiveObject()

-- |[ ========== Activity ========== ]|
--Profile may not appear at all.
if(iMetEmpress == 0.0 and bShowAllEntries == false) then
    local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
    zProfileEntry:fnBlankProfile()
    return
end

-- |[ ====== Profile Handling ====== ]|
-- |[Basic Profile]|
--The profile that most cases have in common.
sBaseProfile = 
"The conquerer of the continent of[BR]Arulenta over seven centuries ago[BR]which she named after herself, Empress[BR]Arulente is a towering figure at 240cm[BR](8 feet).[BR][BR]"..
"Still alive, as dragons are long-lived, she[BR]fought Perfect Hatred herself and only[BR]survived by the grace of her magisters[BR]who sealed the creature away. Since[BR]then, she and the Sevavi created the[BR]"..
"Trials of the Dragon to await Sanya, and [BR]equip her for her ultimate task.[BR][BR]"..
"Physically, she was unmatched in her[BR]prime. Age has only slowed her a little,[BR]and her strength and agility are beyond[BR]the greatest human athletes. In addition,[BR]she has a keen mind, well-schooled "..
"in the[BR]sciences and political theory.[BR][BR]She is an excellent diplomat, warrior, scholar, and poet."

-- |[Variations and Additions]|
--Empress joined the party.
if(iEmpressJoined == 0.0) then
    sBaseProfile = sBaseProfile .. 
    "To help Sanya complete her task, she has joined the group as an advisor."
end

-- |[ =========== Upload =========== ]|
local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
zProfileEntry:fnSetToProfile(sNeutralPath, 0, 0)
zProfileEntry:fnSetDescription(sBaseProfile)
