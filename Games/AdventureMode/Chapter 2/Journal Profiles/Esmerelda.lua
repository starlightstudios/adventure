-- |[ ======================================= Esmerelda ======================================== ]|
--Finally, some intelligent conversation.

-- |[ ============ Setup =========== ]|
--Temp Variables.
local sBaseProfile  = ""
local sNeutralPath  = ""
local sInternalName = "Esmerelda"
local sDisplayName  = "Esmerelda"
local sDialogueName = "Esmerelda"

--Data Library Variables.
local bShowAllEntries = AM_GetPropertyJournal("Show All Entries")
local iMetEsmerelda = VM_GetVar("Root/Variables/Chapter2/HarpyBase/iMetEsmerelda", "N")

--Get dialogue portrait.
DialogueActor_Push(sDialogueName)
    sNeutralPath = DialogueActor_GetProperty("Portrait Path", "Neutral")
DL_PopActiveObject()

-- |[ ========== Activity ========== ]|
--Profile may not appear at all.
if(iMetEsmerelda == 0.0 and bShowAllEntries == false) then
    local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
    zProfileEntry:fnBlankProfile()
    return
end

-- |[ ====== Profile Handling ====== ]|
-- |[Basic Profile]|
--The profile that most cases have in common.
sBaseProfile = 
"Esmerelda de Artezzi, leader of the[BR]harpy brigade and former noble of[BR]House Artezzi in Jeffespeir.[BR][BR]"..
"Intelligent and well-organized, she[BR]wants to use the inherent unity of the[BR]harpy flock and their unique[BR]transformation skills to bring the[BR]continent of Arulenta together just as it[BR]was united seven ".. 
"centuries ago.[BR][BR]At the moment, she is using her army of[BR]harpies to patrol Trafal and stop a[BR]roving army of bandits that is[BR]threatening the glacier from the south."

-- |[Variations and Additions]|
--None.

-- |[ =========== Upload =========== ]|
local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
zProfileEntry:fnSetToProfile(sNeutralPath, 0, 0)
zProfileEntry:fnSetDescription(sBaseProfile)
