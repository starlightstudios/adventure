-- |[ ======================================= Angelface ======================================== ]|
--Mobster bunny.

-- |[ ============ Setup =========== ]|
--Temp Variables.
local sBaseProfile  = ""
local sNeutralPath  = ""
local sInternalName = "Angelface"
local sDisplayName  = "Angelface"
local sDialogueName = "Angelface"

--Data Library Variables.
local bShowAllEntries = AM_GetPropertyJournal("Show All Entries")
local iSawAngelfaceScene = VM_GetVar("Root/Variables/Chapter2/BunnyWarrens/iSawAngelfaceScene", "N")

--Get dialogue portrait.
DialogueActor_Push(sDialogueName)
    sNeutralPath = DialogueActor_GetProperty("Portrait Path", "Neutral")
DL_PopActiveObject()

-- |[ ========== Activity ========== ]|
--Profile may not appear at all.
if(iSawAngelfaceScene == 0.0 and bShowAllEntries == false) then
    local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
    zProfileEntry:fnBlankProfile()
    return
end

-- |[ ====== Profile Handling ====== ]|
-- |[Basic Profile]|
--The profile that most cases have in common.
sBaseProfile = 
"Head of the smuggling bunny gang[BR]headquartered in Trafal, Angelface is as[BR]tough as she is smart, and is plenty[BR]smart, pal.[BR][BR]"..
"She prefers men to women, and has[BR]partially transformed the bunnies so[BR]they retain their male characteristics[BR]but have adorable fuzzy fur. The entire[BR]gang is her harem.[BR][BR]"..
"She doesn't particularly like Sanya or[BR]her friends, and tolerates them only[BR]because Crowbar-Chan vouches for[BR]Sanya's credibility."

-- |[Variations and Additions]|
--None.

-- |[ =========== Upload =========== ]|
local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
zProfileEntry:fnSetToProfile(sNeutralPath, 0, 0)
zProfileEntry:fnSetDescription(sBaseProfile)
