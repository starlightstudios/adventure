-- |[ ==================================== Location Handler ==================================== ]|
--Whenever the journal is opened to locations mode, generates location entries.

-- |[ ======================================== Loading ========================================= ]|
--If this flag is nil, then we need to attempt to load the location images.
DL_AddPath("Root/Images/RegionNotifiers/Permanent/")
local sBaseDLPath = "Root/Images/RegionNotifiers/Permanent/"

--Lookups of information needed to load the image.
local zaImageList = {}
table.insert(zaImageList, {"Northwoods",  gsDatafilesPath .. "UIRegionNorthwoods.slf",  "Northwoods|26"})
table.insert(zaImageList, {"Westwoods",   gsDatafilesPath .. "UIRegionWestwoods.slf",   "Westwoods|26"})
table.insert(zaImageList, {"MtSarulente", gsDatafilesPath .. "UIRegionMtSarulente.slf", "MtSarulente|26"})
    
--Scan the image list.
for i = 1, #zaImageList, 1 do
    
    --Fast-access.
    local sImageName  = zaImageList[i][1]
    local sSLFPath    = zaImageList[i][2]
    local sInfileName = zaImageList[i][3]
    local sImagePath = sBaseDLPath .. sImageName
    
    --If the given image does not exist, order it to load.
    if(DL_Exists(sImagePath) == false) then
        
        --Open the requisite SLF file.
        SLF_Open(sSLFPath)
        
        --Create a bitmap entry.
        DL_ExtractDelayedBitmap(sInfileName, sImagePath)
        
        --Order that entry to load at the next opportunity.
        DL_LoadDelayedBitmap(sImagePath, gciDelayedLoadLoadAtEndOfTick)
    end
end

--Close the SLF file in case it was opened.
SLF_Close()

-- |[ ===================================== Entry Listing ====================================== ]|
--Entries must be re-created each time the file is opened in case a mod needs to add new ones.
local zJournalEntry = nil
gzaCh2LocationEntries = {}

--Set globals.
JournalEntry.zaAppendList = gzaCh2LocationEntries

-- |[ =============== Listing ================ ]|
--Northwoods.
zJournalEntry = JournalEntry:new("Northwoods")
zJournalEntry:fnSetVariable("Root/Variables/Regions/Northwoods/iHasSeenThisRegion")
zJournalEntry:fnSetToLocation("Root/Images/RegionNotifiers/Permanent/Northwoods", 0, 10)
zJournalEntry:fnSetDescription("[BR][BR][BR][BR]"..
    "A system of valleys between the great peaks of Trafal. It is a boreal[BR]forest with several lakes and rivers, fed by the meltwaters from the[BR]glaciers above.[BR][BR]" .. 
    "Trannadar provice is to the north, while the caldera of Westwoods is[BR]to the south through either Vuca or Granvire pass. Mt. Sarulente is to[BR]the west, and an entrance to the Shrine of " .. 
    "the Hero can be found[BR]in the northeast.")

--Westwoods.
zJournalEntry = JournalEntry:new("Westwoods")
zJournalEntry:fnSetVariable("Root/Variables/Regions/Westwoods/iHasSeenThisRegion")
zJournalEntry:fnSetToLocation("Root/Images/RegionNotifiers/Permanent/Westwoods", 0, 10)
zJournalEntry:fnSetDescription("[BR][BR][BR][BR]"..
    "Often called 'The Cauldron', the land is blasted and very little grows[BR]here. What does is sickly and often mutated. The wildlife is extremely[BR]hostile and far more aggressive "..
    "than it has any right to be. Clean water[BR]is hard to find as the lake in the center is pure poison. The presence of[BR]the old imperial highway still means merchants travel through here,[BR]"..
    "escorted by armed mercenaries.[BR][BR]"..
    "Two routes in the north lead to Northwoods, via Vuca or Granvire pass.[BR]The Ice Spires can be accessed to the east, and Mt. Kriyji is to the[BR]southwest. The remains of Fort Kirysu can be found "..
    "west of the lake[BR]in the center.")

--Mt. Sarulente.
zJournalEntry = JournalEntry:new("MtSarulente", "Mt. Sarulente")
zJournalEntry:fnSetVariable("Root/Variables/Regions/MtSarulente/iHasSeenThisRegion")
zJournalEntry:fnSetToLocation("Root/Images/RegionNotifiers/Permanent/Dimensional Trap", 0, 0)
zJournalEntry:fnSetDescription("[BR][BR][BR][BR]"..
    "Sometimes called the Dragon's Eye, it is said to be the highest[BR]mountain in western Trafal. It is also said to be the birthplace of the[BR]great dragon empress of 700 years ago.[BR][BR]"..
    "There are no permanent settlements on the mountain, only the remains[BR]of the ancient Fort Sarulente.")
                
-- |[ ================ Upload ================ ]|
for i = 1, #gzaCh2LocationEntries, 1 do
    gzaCh2LocationEntries[i]:fnUploadData()
end

--Clean.
JournalEntry:fnCleanGlobals()

--Clear the list to save memory.
gzaCh2LocationEntries = nil
