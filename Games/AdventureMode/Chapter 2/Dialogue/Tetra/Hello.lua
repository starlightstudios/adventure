-- |[ ====================================== Miso's Hello ====================================== ]|
--When speaking to Miso, this is the dialogue that plays. It handles introductions.

--Resolve what room the player is in. This changes dialogue slightly.
local sRoomName = AL_GetProperty("Name")

-- |[Variables]|
--local iSawKidnapIntro = VM_GetVar("Root/Variables/Chapter2/KitsuneVillage/iSawKidnapIntro", "N")
    
-- |[Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tetra", "Neutral") ]])
fnCutscene([[ Append("Tetra:[E|Neutral] Hi there![P] There will be more dialogue next version when the tunnel is cleared.") ]])
