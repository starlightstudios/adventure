-- |[ ====================================== Miso's Hello ====================================== ]|
--When speaking to Miso, this is the dialogue that plays. It handles introductions.

--Resolve what room the player is in. This changes dialogue slightly.
local sRoomName = AL_GetProperty("Name")

-- |[ ====================================== First Meeting ====================================== ]|
--Check if this is the first meeting.
local iMetMiso = VM_GetVar("Root/Variables/Chapter2/Miso/iMetMiso", "N")
if(iMetMiso == 0.0) then
    
    -- |[Variables]|
    local iSawKidnapIntro = VM_GetVar("Root/Variables/Chapter2/KitsuneVillage/iSawKidnapIntro", "N")
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter2/Miso/iMetMiso", "N", 1.0)
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Miso", "Neutral") ]])
    fnCutscene([[ Append("Izuna:[E|Happy] Miso![P] It's great to see you![B][C]") ]])
    fnCutscene([[ Append("Miso:[E|Neutral] Well well, if it isn't Izuna.[P] I knew you weren't dead.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] You're sounding awfully blase about this.[B][C]") ]])
    fnCutscene([[ Append("Miso:[E|Neutral] The Fox Goddess protects her followers.[P] I knew you'd turn up sooner or later.[B][C]") ]])
    fnCutscene([[ Append("Miso:[E|Neutral] Which is not to say I didn't look.[P] We sent out search parties.[B][C]") ]])
    
    -- |[Has Not Visited Village]|
    if(iSawKidnapIntro == 0.0) then
        fnCutscene([[ Append("Izuna:[E|Ugh] Oh dear...[B][C]") ]])
        fnCutscene([[ Append("Miso:[E|Neutral] I assume you were on your way to let everyone know you're all right.[B][C]") ]])
        
        -- |[Is Near Vuca Pass]|
        if(sRoomName == "NorthwoodsSED") then
            fnCutscene([[ Append("Izuna:[E|Neutral] Were you on your way to the temple?[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Just leaving, actually.[P] I can let the search parties I see know that you're all right, though.[B][C]") ]])
    
        -- |[Anywhere Else]|
        else
            fnCutscene([[ Append("Izuna:[E|Smirk] Of course![P] But if you get there first, please let them know for me.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] If my route takes me there, sure.[B][C]") ]])
        end
    
    -- |[Has Visited the Village]|
    else
        fnCutscene([[ Append("Izuna:[E|Ugh] So I heard.[P] The elder gave me quite a scolding![B][C]") ]])
        fnCutscene([[ Append("Miso:[E|Neutral] And a well-deserved one, at that.[P] Maybe you'll learn this time.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Smirk] Never again![B][C]") ]])
        fnCutscene([[ Append("Miso:[E|Neutral] If I run into any of the search parties I'll let them know you're all right.[B][C]") ]])
    end
    fnCutscene([[ Append("Sanya:[E|Neutral] So are you a wandering priest or something?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] Miso is a great warrior, Sanya![P] We can learn much from her![B][C]") ]])
    fnCutscene([[ Append("Miso:[E|Neutral] Oh please, Izuna.[P] War does not make one great.[B][C]") ]])
    fnCutscene([[ Append("Miso:[E|Neutral] No, I left that life behind me.[P] I'm a travelling cook.[P] Well, I make other things, too, but my soup is my calling.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] I was about to ask how you got the name Miso...[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Bah...[B][C]") ]])
    fnCutscene([[ Append("Miso:[E|Neutral] My name before I became a kitsune was Orla.[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh![P] Nyeh![B][C]") ]])
    fnCutscene([[ Append("Miso:[E|Neutral] Oh, hungry?[P] Here, try some of this broth, little guy.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] That's Zeke, and this is Sanya.[B][C]") ]])
    fnCutscene([[ Append("Miso:[E|Neutral] Seems he likes it.[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] *slurp gulp*[P] nyaaahhhhh![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Hey Miso.[P] Can you do anything with this stuff I've found in my hunts?[P] I'm not exactly a chef here.[B][C]") ]])
    fnCutscene([[ Append("Miso:[E|Neutral] A hunter, are you?[P] I suppose I can.[B][C]") ]])
    fnCutscene([[ Append("Miso:[E|Neutral] Tell you what, Sanya.[P] Since you're a friend of Izuna's, I can offer you my skills.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Happy] We'd be honored![P] Thank you very much![B][C]") ]])
    fnCutscene([[ Append("Miso:[E|Neutral] So here's how this works.[P] The creatures of Trafal have many unique properties.[P] Special creatures have special materials you get from hunting them.[B][C]") ]])
    fnCutscene([[ Append("Miso:[E|Neutral] The mountain winds here focus natural magical flows coming up from beneath us.[P] It's what makes our hot springs hot, and allows the underground to grow food.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] I'm not really from around here.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Miso knows a lot about Trafal.[P] She's very wise, you can tell because she has five tails![B][C]") ]])
    fnCutscene([[ Append("Miso:[E|Neutral] Ho ho![P] Izuna, don't go saying that![B][C]") ]])
    fnCutscene([[ Append("Miso:[E|Neutral] Because now the Fox Goddess will take it upon herself to make a kitsune with many tails say something truly foolish, to test you![B][C]") ]])
    fnCutscene([[ Append("Miso:[E|Neutral] You must not believe what someone says simply because of their credentials, young ones.[P] Divine tails or otherwise, nobody is so wise that they cannot lead you astray.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Except the Fox Goddess?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] She might, just to screw with you.[B][C]") ]])
    fnCutscene([[ Append("Miso:[E|Neutral] Now as I was saying, the creatures here have magical ingredients I can use.[P] I can take some of them from you to make items you'll not find anywhere else in the world.[B][C]") ]])
    fnCutscene([[ Append("Miso:[E|Neutral] I have some equipment for sale, and there is more from places like Poterup or the temple.[B][C]") ]])
    fnCutscene([[ Append("Miso:[E|Neutral] Get that equipment and some magical ingredients, and I can make you an improved version of the item.[P] Not all items can be improved, of course.[B][C]") ]])
    fnCutscene([[ Append("Miso:[E|Neutral] The list of equipment shows what is needed to improve it.[P] Each area of Trafal has different creatures to hunt, used for different items.[B][C]") ]])
    fnCutscene([[ Append("Miso:[E|Neutral] Typically, the items purchased in a region need that region's animals, but this isn't always the case.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Thank you, Miso.[P] I'm sure these will help on our journey.[B][C]") ]])
    fnCutscene([[ Append("Miso:[E|Neutral] Oh, what story are you after this time, Izuna?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Saving the world.[B][C]") ]])
    fnCutscene([[ Append("Miso:[E|Neutral] Really?[P] Come off it, Izuna.[P] It's an old legend.[P] Don't believe everything you read.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] She already knows about me?[B][C]") ]])
    fnCutscene([[ Append("Miso:[E|Neutral] Izuna wouldn't shut up about the hero of legend, and then she just left.[P] I put the pieces together.[P] Especially with a name like Sanya.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] I happen to believe the legend is true.[P] Can I ask you to believe it as I do?[B][C]") ]])
    fnCutscene([[ Append("Miso:[E|Neutral] Oh, I don't doubt that you believe it is true.[P] But a wise kitsune knows to require a bit more than faith, no matter how solid.[B][C]") ]])
    fnCutscene([[ Append("Miso:[E|Neutral] Besides, the most powerful aspect of well-placed faith is the knowledge that doubt can do it no harm.[B][C]") ]])
    fnCutscene([[ Append("Miso:[E|Neutral] If Sanya here really is the hero, then a bit of doubt won't change that, will it?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] Of course not![B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Nggrlbbyeh![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Zeke don't speak with your mouth full![B][C]") ]])
    fnCutscene([[ Append("Miso:[E|Neutral] Ha ha![P] Nevertheless, come to me whenever you need something made.[P] I'll be seeing you all over, I assume.") ]])
    fnCutsceneBlocker()
    
-- |[ =================================== Repeat Encounters ==================================== ]|
else
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Miso", "Neutral") ]])
    fnCutscene([[ Append("Miso:[E|Neutral] Hello there![P] Is there something I can do for you?[B][C]") ]])
    
	-- |[Topics]|
	--Activate topics mode once the dialogue is complete.
	fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Miso") ]])
    
    
end