-- |[ ================================= Pamphlet: Bun Runners ================================== ]|
--Called by a dialogue script when responding to the named pamphlet.
TA_SetProperty("Face Character", "PlayerEntity")
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Miso", "Neutral") ]])
fnCutscene([[ Append("Miso:[E|Neutral] Bunnies with a male harem, huh.[P] I've been on the road a lot and never seen that before.[B][C]") ]])
fnCutscene([[ Append("Miso:[E|Neutral] Bunnies, yes.[P] Smuggling, yes.[P] Harems, absolutely.[P] But monsterboys?[P] Learn something new every day.[B][C]") ]])
fnCutscene([[ Append("Izuna:[E|Neutral] That's quite a thing to hear from you, Miso.[P] I thought you'd seen everything.[B][C]") ]])
fnCutscene([[ Append("Miso:[E|Neutral] Evidently not![B][C]") ]])
fnCutscene([[ Append("Miso:[E|Neutral] In any case, I've got nothing against the bunnies myself.[P] They keep to themselves and don't make trouble on the roads.[B][C]") ]])
fnCutscene([[ Append("Miso:[E|Neutral] They also never ask me for lunch.[P] Rude!") ]])