-- |[ =============================== Pamphlet: Green Westwoods ================================ ]|
--Called by a dialogue script when responding to the named pamphlet.
TA_SetProperty("Face Character", "PlayerEntity")
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Miso", "Neutral") ]])
fnCutscene([[ Append("Miso:[E|Neutral] A covenant of alraunes in Westwoods, trying to heal the scarred land.[P] Amazing.[P] I should head over there.[B][C]") ]])
fnCutscene([[ Append("Miso:[E|Neutral] If nothing else, maybe they'll grow some particularly spicy foods.[P] Uh, I might need a spell to check for toxins.[B][C]") ]])
fnCutscene([[ Append("Sanya:[E|Happy] Oh yeah, nothing tastier than heavy metal toxicity![B][C]") ]])
fnCutscene([[ Append("Izuna:[E|Happy] Mercury![P] Sweetest of the transition metals![B][C]") ]])
fnCutscene([[ Append("Miso:[E|Neutral] Not what I meant but okay.") ]])