-- |[ ==================================== Empress's Hello ===================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.
local bIsZekePresent  = fnIsCharacterPresent("Zeke")
local bIsIzunaPresent = fnIsCharacterPresent("Izuna")

-- |[Music]|
--Change music to Empress's theme.
glLevelMusic = AL_GetProperty("Music")
    
-- |[Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])

--Zeke and Izuna are both not present.
if(bIsZekePresent == false and bIsIzunaPresent == false) then
    AL_SetProperty("Music", "Empress Maj")
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Empress", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Sanya", "Neutral") ]])

--Zeke is present, Izuna is not.
elseif(bIsZekePresent == true and bIsIzunaPresent == false) then
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Empress", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Zeke", "Neutral") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] Sanya, we do not have time for idle chit chat.[P] Izuna is our current concern.[P] Let us go.") ]])
    return

--Izuna is present but Zeke is not:
elseif(bIsZekePresent == false and bIsIzunaPresent == true) then
    AL_SetProperty("Music", "Empress Maj")
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Empress", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Izuna", "Neutral") ]])

--Both:
else
    AL_SetProperty("Music", "Empress Maj")
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Empress", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Zeke", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Izuna", "Neutral") ]])
end
	
	
--Dialogue.
fnCutscene([[ Append("Empress:[E|Neutral] Do you need guidance?[B][C]") ]])

-- |[Topics]|
--Activate topics mode once the dialogue is complete.
fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Empress") ]])
