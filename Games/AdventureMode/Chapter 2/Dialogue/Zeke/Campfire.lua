-- |[ ====================================== Zeke's Hello ====================================== ]|
--When speaking to Zeke. Zeke always takes the opposite side from Sanya.
local bIsIzunaPresent  = fnIsCharacterPresent("Izuna")
local bIsEmpressPresent = fnIsCharacterPresent("Empress")

-- |[Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])

--Just Sanya and Zeke:
if(bIsEmpressPresent == false and bIsIzunaPresent == false) then
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Zeke", "Neutral") ]])

--Izuna but not Empress:
elseif(bIsIzunaPresent == true and bIsEmpressPresent == false) then
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Izuna", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Zeke", "Neutral") ]])

--Empress but not Izuna:
elseif(bIsIzunaPresent == false and bIsEmpressPresent == true) then
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Empress", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Zeke", "Neutral") ]])

--Both:
else
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Empress", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Izuna", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Zeke", "Neutral") ]])
end

--Dialogue.
fnCutscene([[ Append("Zeke: Bahh?[B][C]") ]])

-- |[Topics]|
--Activate topics mode once the dialogue is complete.
fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Zeke") ]])