-- |[ ===================================== Izuna's Hello ====================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.
local bIsZekePresent    = fnIsCharacterPresent("Zeke")
local bIsEmpressPresent = fnIsCharacterPresent("Empress")

-- |[Music]|
--Change music to Izuna's theme.
glLevelMusic = AL_GetProperty("Music")
--AL_SetProperty("Music", "IzunasTheme")
    
-- |[Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])

--Just Sanya and Izuna:
if(bIsEmpressPresent == false and bIsZekePresent == false) then
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Izuna", "Neutral") ]])

--Zeke but not Empress:
elseif(bIsZekePresent == true and bIsEmpressPresent == false) then
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Zeke", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Izuna", "Neutral") ]])

--Empress but not Zeke:
elseif(bIsZekePresent == false and bIsEmpressPresent == true) then
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Empress", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Izuna", "Neutral") ]])

--Both:
else
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Empress", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Zeke", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Izuna", "Neutral") ]])
end
	
	
--Dialogue.
fnCutscene([[ Append("Izuna:[E|Smirk] Need some guidance, sweetheart?[B][C]") ]])

-- |[Topics]|
--Activate topics mode once the dialogue is complete.
fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Izuna") ]])
