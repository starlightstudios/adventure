-- |[ ====================================== Tomb Rumours ====================================== ]|
--I love graverobbing!

-- |[ ============ Setup =========== ]|
--Temporary Variables.
local sInternalName = "Tomb of the Empress"
local sObjective = ""
local sDescription = ""
local zQuestEntry = JournalEntry:new(sInternalName, sInternalName)

--Datalibrary Variables.
local iMiaGaveEmpressQuest = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iMiaGaveEmpressQuest", "N")
local iSawBigScene         = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iSawBigScene", "N")
local iMetEmpress          = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iMetEmpress", "N")
local iEmpressJoined       = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")

-- |[ ====== Activation Check ====== ]|
--Check if the quest should appear. If not, place a set of dashes to indicate a quest exists.
if(iMiaGaveEmpressQuest == 0.0) then
    zQuestEntry:fnBlankQuest()
    return
end

-- |[ ======= Quest Variants ======= ]|
--Mia sends you on the road.
if(iMiaGaveEmpressQuest == 1.0 and iSawBigScene == 0.0) then
    sObjective = "Search Mt. Sarulente for the tomb of the dragon empress"
    sDescription = "Mia thinks there might be something to rumours that the great dragon[BR]empress' tomb is on Mt. Sarulente in southwestern Northwoods.[BR][BR]"..
                   "Head over there and see what you can dig up."

--WOAH.
elseif(iSawBigScene == 1.0 and iMetEmpress == 0.0) then
    sObjective = "Find out what's really going on in that tomb"
    sDescription = "There was some kind of massive animatronic dragon in the tomb![BR]But Zeke saved the day as usual![BR][BR]"..
                   "Find out what's going on."

--She's really tall.
elseif(iMetEmpress == 1.0 and iEmpressJoined == 0.0) then
    sObjective = "Transform into a statue girl"
    sDescription = "The great dragon empress is actually still alive, and somewhat sassy.[BR]She wants to help your quest, but requests that you transform into a[BR]statue girl first.[BR][BR]"..
                   "Apparently the pedestals in her home can do this for you."
    
--Complete.
else
    sObjective = "Complete"
    sDescription = "Empress joined your team after confirming that you really are the[BR]destined hero. Go kick Perfect Hatred's ass!"
end

-- |[ =============================== Upload =============================== ]|
--Upload information.
zQuestEntry:fnSetToQuest(sObjective)
zQuestEntry:fnSetDescription(sDescription)
