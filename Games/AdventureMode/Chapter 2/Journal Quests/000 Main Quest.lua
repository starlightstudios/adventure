-- |[ ======================================= Main Quest ======================================= ]|
--The main quest to kill Perfect Hatred.

-- |[ ============ Setup =========== ]|
--Temporary Variables.
local sInternalName = "Main Quest"
local sObjective = ""
local sDescription = ""
local zQuestEntry = JournalEntry:new(sInternalName, sInternalName)

--Datalibrary Variables.
local sSanyaForm       = VM_GetVar("Root/Variables/Global/Sanya/sForm", "S")
local iSwissPeople     = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iSwissPeople", "N")
local iSawHostiles     = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iSawHostiles", "N")
local iEscapedShrine   = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iEscapedShrine", "N")
local iSawMysteryLight = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iSawMysteryLight", "N")
local iDidPuzzleFight  = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iDidPuzzleFight", "N")
local iCompletedMines  = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iCompletedMines", "N")
local iCompletedHunt   = VM_GetVar("Root/Variables/Chapter2/Northwoods/iCompletedHunt", "N")

-- |[ ====== Activation Check ====== ]|
--This quest always appears.

-- |[ ======= Quest Variants ======= ]|
-- |[Shrine of the Hero]|
--Has Not Met Izuna
if(iSwissPeople == 0.0) then
    sObjective = "Find a way out of the caves."
    sDescription = "You've fallen through a weak spot in the ground and landed in a cave[BR]so far below the surface, you can't see the sun. Find a way out!"

--Has Met Izuna, Save Her!
elseif(iSawHostiles == 0.0) then
    sObjective = "Find medical attention for the weeb."
    sDescription = "As if by magic, a cosplayer dressed like a fox fell from above and got[BR]hurt pretty bad. She's going to need a doctor, and you need to find one."

--Statue Girls!
elseif(iSawHostiles == 1.0 and sSanyaForm == "Human" and iEscapedShrine == 0.0) then
    sObjective = "Escape!"
    sDescription = "The only way out is blocked by statue women, and they don't look very[BR]friendly. Sneak past them and get the girl to safety."

--Statue Girls! And you're one of them!
elseif(iSawHostiles == 1.0 and sSanyaForm == "Sevavi" and iEscapedShrine == 0.0) then
    sObjective = "Escape!"
    sDescription = "You've been transformed into a living statue. As fun as that is, the[BR]injured cosplayer still needs your help. Find a way to escape this shrine."

--The Peak
elseif(iEscapedShrine == 1.0 and iSawMysteryLight == 0.0) then
    sObjective = "Find Help"
    sDescription = "You might well be stranded on an alien planet. Try to find some form[BR]of civilization and find a way to help the fallen cosplayer."

-- |[Mines]|
elseif(iSawMysteryLight == 1.0 and iDidPuzzleFight == 0.0) then
    sObjective = "Find out who has that light"
    sDescription = "Some cagey edgelord is nearby, shining a light. They might be able to[BR]help your injured ward. Chase them down!"
    
elseif(iDidPuzzleFight == 1.0 and iCompletedMines == 0.0) then
    sObjective = "Get back to the cabin"
    sDescription = "A mushroom man named Prince Odar kindly found your medkit for[BR]you. Now get back to the weeaboo and see to her wounds."
    
elseif(iCompletedHunt == 0.0) then
    sObjective = "A rifle! Find some meat!"
    sDescription = "You and the cosplayer need some food. You've got a fistfull of rifle and[BR]a head full of mad! Kill something so she doesn't starve."

-- |[Main Description]|
else
    sObjective = "Find and destroy Perfect Hatred"
    sDescription = "The world of Pandemonium will be destroyed by an ancient evil called[BR]Perfect Hatred if you don't find and kill him. You're the legendary hero[BR]foretold to defeat him, and Izuna "..
                   "suspects his seal is nearly broken, if[BR]it isn't already.[BR]Find him.[BR]End him."
end

-- |[ =============================== Upload =============================== ]|
--Upload information.
zQuestEntry:fnSetToQuest(sObjective)
zQuestEntry:fnSetDescription(sDescription)
