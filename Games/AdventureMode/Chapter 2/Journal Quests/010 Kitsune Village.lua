-- |[ ================================== The Kitsune Village =================================== ]|
--Izuna suggests checking in at the kitsune village.

-- |[ ============ Setup =========== ]|
--Temporary Variables.
local sInternalName = "Kitsune Village"
local sObjective = ""
local sDescription = ""
local zQuestEntry = JournalEntry:new(sInternalName, sInternalName)

--Datalibrary Variables.
local iCompletedHunt  = VM_GetVar("Root/Variables/Chapter2/Northwoods/iCompletedHunt", "N")
local iSawKidnapIntro = VM_GetVar("Root/Variables/Chapter2/KitsuneVillage/iSawKidnapIntro", "N")
local iRescuedIzuna   = VM_GetVar("Root/Variables/Chapter2/KitsuneVillage/iRescuedIzuna", "N")

-- |[ ====== Activation Check ====== ]|
--Check if the quest should appear. If not, place a set of dashes to indicate a quest exists.
if(iCompletedHunt == 0.0) then
    zQuestEntry:fnBlankQuest()
    return
end

-- |[ ======= Quest Variants ======= ]|
--Izuna mentioned the quest.
if(iSawKidnapIntro == 0.0) then
    sObjective = "Find the kitsune village"
    sDescription = "Izuna suggested a good place to get information would be the kitsune[BR]temple in Trafal. It is located in Vuca Pass, between Northwoods and[BR]Westwoods."

--Izuna was taken away!
elseif(iSawKidnapIntro == 1.0 and iRescuedIzuna == 0.0) then
    sObjective = "Rescue Izuna!"
    sDescription = "The kitsunes have abducted Izuna! Get her back! Try not to kill any of[BR]them because they seem really nice."

--Complete.
else
    sObjective = "Complete"
    sDescription = "It turns out Izuna wasn't abducted, she was just being punished for[BR]running off without telling anyone. The kitsunes are on your side. You[BR]may speak to the elder at any time for information."
end

-- |[ =============================== Upload =============================== ]|
--Upload information.
zQuestEntry:fnSetToQuest(sObjective)
zQuestEntry:fnSetDescription(sDescription)