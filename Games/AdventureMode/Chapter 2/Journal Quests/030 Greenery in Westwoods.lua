-- |[ ================================== Greenery in Westwoods ================================= ]|
--Open the door. NO.

-- |[ ============ Setup =========== ]|
--Temporary Variables.
local sInternalName = "Greenery in Westwoods"
local sObjective = ""
local sDescription = ""
local zQuestEntry = JournalEntry:new(sInternalName, sInternalName)

--Datalibrary Variables.
local iMiaGaveAlrauneQuest = VM_GetVar("Root/Variables/Chapter2/FortKirysu/iMiaGaveAlrauneQuest", "N")
local iMetAlraune          = VM_GetVar("Root/Variables/Chapter2/FortKirysu/iMetAlraune", "N")
local iGotSpringwater      = VM_GetVar("Root/Variables/Chapter2/FortKirysu/iGotSpringwater", "N")
local iBefriendedAlraunes  = VM_GetVar("Root/Variables/Chapter2/FortKirysu/iBefriendedAlraunes", "N")

-- |[ ====== Activation Check ====== ]|
--Check if the quest should appear. If not, place a set of dashes to indicate a quest exists.
if(iMiaGaveAlrauneQuest == 0.0) then
    zQuestEntry:fnBlankQuest()
    return
end

-- |[ ======= Quest Variants ======= ]|
--The hunt begins.
if(iMiaGaveAlrauneQuest == 1.0 and iMetAlraune == 0.0) then
    sObjective = "Check out Fort Kirysu"
    sDescription = "Mia heard some rumours that there's a spot of green in Westwoods,[BR]which is really unusual considering the place is a blasted hellscape.[BR][BR]"..
                   "Be a good journalist and find out what's going on! Investigate Fort[BR]Kirysu, west of the lake in Westwoods."
    
--Let me in! LET ME IN!
elseif(iMetAlraune == 1.0 and iGotSpringwater == 0.0) then
    sObjective = "Get some water for the alraunes"
    sDescription = "Some alraunes seem to have taken over Fort Kirysu in Westwoods, and[BR]there's actual, real green grass growing there.[BR][BR]"..
                   "Something is up, but they won't open the door. They said they'd accept[BR]a bottle of fresh springwater as proof of friendship.[BR][BR]"..
                   "Find the freshest spring in Northwoods and bring them their water.[BR]It will be off the beaten path."

--Give them their water.
elseif(iGotSpringwater == 1.0 and iBefriendedAlraunes == 0.0) then
    sObjective = "Give the alraunes some springwater"
    sDescription = "The alraunes in Westwoods asked for some fresh springwater, and you[BR]have it.[BR][BR]Go give them their precious water."

--Complete.
else
    sObjective = "Complete"
    sDescription = "You gave the alraunes their water and they allowed you into Fort[BR]Kirysu. You can ask them all about the details."
end

-- |[ =============================== Upload =============================== ]|
--Upload information.
zQuestEntry:fnSetToQuest(sObjective)
zQuestEntry:fnSetDescription(sDescription)