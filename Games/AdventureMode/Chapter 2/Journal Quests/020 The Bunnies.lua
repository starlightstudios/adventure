-- |[ =======================================The Bunnies ======================================= ]|
--Those bunnies refuse to TF you!

-- |[ ============ Setup =========== ]|
--Temporary Variables.
local sInternalName = "The Bunnies"
local sObjective = ""
local sDescription = ""
local zQuestEntry = JournalEntry:new(sInternalName, sInternalName)

--Datalibrary Variables.
local iSawFailedBunnyTF   = VM_GetVar("Root/Variables/Chapter2/BunnyWarrens/iSawFailedBunnyTF", "N")
local iOpenedWarrens      = VM_GetVar("Root/Variables/Chapter2/BunnyWarrens/iOpenedWarrens", "N")
local iSawAngelfaceScene  = VM_GetVar("Root/Variables/Chapter2/BunnyWarrens/iSawAngelfaceScene", "N")
local iGotBunnyClearance  = VM_GetVar("Root/Variables/Chapter2/BunnyWarrens/iGotBunnyClearance", "N")
local iGaveBunnyClearance = VM_GetVar("Root/Variables/Chapter2/BunnyWarrens/iGaveBunnyClearance", "N")

-- |[ ====== Activation Check ====== ]|
--Check if the quest should appear. If not, place a set of dashes to indicate a quest exists.
if(iSawFailedBunnyTF == 0.0) then
    zQuestEntry:fnBlankQuest()
    return
end

-- |[ ======= Quest Variants ======= ]|
--A bunny refused to TF Sanya. FIND THAT BUN.
if(iSawFailedBunnyTF == 1.0 and iOpenedWarrens == 0.0) then
    sObjective = "Find the Bunny HQ"
    sDescription = "Some rat-fucking bastard bunny refused to transform you even though[BR]you were totally at their mercy. They cannot be forgiven.[BR][BR]"..
                   "Find the bunny HQ. The smugglers must have a base of operations.[BR][BR]"..
                   "Don't hurt any of them, you just want to talk."

--This is their hideout. Find the leader!
elseif(iOpenedWarrens == 1.0 and iSawAngelfaceScene == 0.0) then
    sObjective = "Find the Bunny Leader"
    sDescription = "The bunnies set up their smuggling base in some warrens located in[BR]northwestern Westwoods.[BR][BR]"..
                   "Find their leader and politely ask her to transform you."

--You gotta do them a favour first.
elseif(iSawAngelfaceScene == 1.0 and iGotBunnyClearance == 0.0) then
    sObjective = "Get the Bunnies Past the Harpy Blockade"
    sDescription = "Angelface said she'd transform you if you got the harpy blockade[BR]lowered, or at least gave the bunnies free passage.[BR][BR]"..
                   "How you do that is your problem. Get back to her once you've dealt[BR]with the blockade."

--Go get bunny'd!
elseif(iGotBunnyClearance == 1.0 and iGaveBunnyClearance == 0.0) then
    sObjective = "Give Angelface the News"
    sDescription = "You've got a deal worked out for the bunnies. Go tell Angelface about[BR]it and get your reward."

--Complete.
else
    sObjective = "Complete"
    sDescription = "You did the bunnies a favour and got turned into one as a result."
end

-- |[ =============================== Upload =============================== ]|
--Upload information.
zQuestEntry:fnSetToQuest(sObjective)
zQuestEntry:fnSetDescription(sDescription)