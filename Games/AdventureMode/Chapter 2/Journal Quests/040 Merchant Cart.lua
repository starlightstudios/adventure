-- |[ ====================================== Merchant Cart ===================================== ]|
--Someone needs our help!

-- |[ ============ Setup =========== ]|
--Temporary Variables.
local sInternalName = "The Merchant Caravan"
local sObjective = ""
local sDescription = ""
local zQuestEntry = JournalEntry:new(sInternalName, sInternalName)

--Datalibrary Variables.
local iMiaGaveCartQuest = VM_GetVar("Root/Variables/Chapter2/Westwoods/iMiaGaveCartQuest", "N")
local iMetTakahn        = VM_GetVar("Root/Variables/Chapter2/Westwoods/iMetTakahn", "N")
local iHelpedTakahn     = VM_GetVar("Root/Variables/Chapter2/Westwoods/iHelpedTakahn", "N")

-- |[ ====== Activation Check ====== ]|
--Check if the quest should appear. If not, place a set of dashes to indicate a quest exists.
if(iMiaGaveCartQuest == 0.0) then
    zQuestEntry:fnBlankQuest()
    return
end

-- |[ ======= Quest Variants ======= ]|
--Help the innocent! And help the mercenaries too.
if(iMiaGaveCartQuest == 1.0 and iMetTakahn == 0.0) then
    sObjective = "Find the missing merchant caravan"
    sDescription = "Mia got word that a merchant caravan went missing in Westwoods.[BR]Even if 'people go missing in Westwoods' isn't exactly news, you gotta[BR]find them! You're a hero![BR][BR]"..
                   "Find the missing caravan, and rescue anyone who needs it."

--Dude needs wood.
elseif(iMetTakahn == 1.0 and iHelpedTakahn == 0.0) then
    sObjective = "Find some wood for the merchants"
    sDescription = "A merchant caravan got ambushed in southern Westwoods and needs[BR]wood for a replacement wheel, but the mercs can't leave the cart alone.[BR][BR]"..
                   "Get 3 Hardened Bark and 1 Light Fiber to Takahn."

--Complete.
else
    sObjective = "Complete"
    sDescription = "Takahn has the materials to fix the wheel and the caravan will head to[BR]Poterup village."
end

-- |[ =============================== Upload =============================== ]|
--Upload information.
zQuestEntry:fnSetToQuest(sObjective)
zQuestEntry:fnSetDescription(sDescription)
