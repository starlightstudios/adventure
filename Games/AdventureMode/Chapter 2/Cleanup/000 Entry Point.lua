-- |[ ==================================== Chapter 2 Cleanup =================================== ]|
--This script is fired once chapter 2 is completed. General states of chapter 2 are stored in a special
-- DataLibrary section to be queried later.

-- |[Flag Completion]|
VM_SetVar("Root/Variables/Global/ChapterComplete/iChapter2", "N", 1.0)

-- |[ ================================ New Chapter Plus Backup ================================= ]|
--Store variables that will be needed if the player decides to do a New Chapter Plus. This includes
-- inventory, forms, money, and more!
DL_AddPath("Root/Variables/NewChapterPlus/Chapter2/")

-- |[Inventory]|
--Basic Platina count.
VM_SetVar("Root/Variables/NewChapterPlus/Chapter2/iPlatina", "N", AdInv_GetProperty("Platina"))

--Adamantite counts.
for i = gciCraft_Adamantite_Powder, gciCraft_Adamantite_Total-1, 1 do
    local iCount = AdInv_GetProperty("Crafting Count", i)
    VM_SetVar("Root/Variables/NewChapterPlus/Chapter2/iAdamantite"..i, "N", iCount)
end

--Items in the inventory, not equipped:
local iFreeItems = AdInv_GetProperty("Total Items Unequipped")
VM_SetVar("Root/Variables/NewChapterPlus/Chapter2/iItemsUnequipped", "N", iFreeItems)

--Store the items and their counts:
for i = 1, iFreeItems, 1 do
    AdInv_PushItemI(i-1)
        local sName = AdItem_GetProperty("Name")
        local iQuantity = AdItem_GetProperty("Quantity")
        VM_SetVar("Root/Variables/NewChapterPlus/Chapter2/sItem"..i.."Name", "S", sName)
        VM_SetVar("Root/Variables/NewChapterPlus/Chapter2/iItem"..i.."Quantity", "N", iQuantity)
    DL_PopActiveObject()
end

-- |[Costumes]|
--Store if a costume is unlocked.
--VM_SetVar("Root/Variables/NewChapterPlus/Chapter2/iCostumeUnlocked_QueenBee", "N", VM_GetVar("Root/Variables/Costumes/Mei/iQueenBee", "N"))

-- |[Forms]|
--Store which forms are unlocked for Sanya:
VM_SetVar("Root/Variables/NewChapterPlus/Chapter2/iFormUnlocked_Statue", "N", VM_GetVar("Root/Variables/Global/Statue/iHasStatueForm", "N"))

--Store which jobs are unlocked for Izuna:
--VM_SetVar("Root/Variables/NewChapterPlus/Chapter2/iJobUnlocked_Mediator",       "N", VM_GetVar("Root/Variables/Global/Izuna/iHasJob_Mediator", "N"))

--Store which jobs are unlocked for Empress:

--Zeke is unemployed.

-- |[Skillbooks]|
--Store the total number of skillbooks unlocked. Individual skillbooks are handled later.
VM_SetVar("Root/Variables/NewChapterPlus/Chapter2/iSkillbooksSanya",   "N", VM_GetVar("Root/Variables/Global/Sanya/iSkillbookTotal",   "N"))
VM_SetVar("Root/Variables/NewChapterPlus/Chapter2/iSkillbooksIzuna",   "N", VM_GetVar("Root/Variables/Global/Izuna/iSkillbookTotal",   "N"))
VM_SetVar("Root/Variables/NewChapterPlus/Chapter2/iSkillbooksEmpress", "N", VM_GetVar("Root/Variables/Global/Empress/iSkillbookTotal", "N"))

-- |[ ================================== Storage and Takedown ================================== ]|
-- |[Major Variables]|
-- |[Catalyst Counts]|
local iCatalystH  = VM_GetVar("Root/Variables/Global/Catalysts/iHealth", "N")
local iCatalystAt = VM_GetVar("Root/Variables/Global/Catalysts/iAttack", "N")
local iCatalystI  = VM_GetVar("Root/Variables/Global/Catalysts/iInitiative", "N")
local iCatalystE  = VM_GetVar("Root/Variables/Global/Catalysts/iEvade", "N") + VM_GetVar("Root/Variables/Global/Catalysts/iDodge", "N")
local iCatalystAc = VM_GetVar("Root/Variables/Global/Catalysts/iAccuracy", "N")
local iCatalystSk = VM_GetVar("Root/Variables/Global/Catalysts/iMovement", "N") + VM_GetVar("Root/Variables/Global/Catalysts/iSkill", "N")

-- |[Inventory Handling]|
--Remove the Platinum Compass
VM_SetVar("Root/Variables/Global/CatalystTone/iHasCatalystTone", "N", 0.0)

--Change Mei to no longer be the party leader.
AL_SetProperty("Player Actor ID", 0)

--Remove Mei from the party.
AdvCombat_SetProperty("Clear Party")

--Set Cash to 0, remove crafting ingredients, drop the inventory entirely.
AdInv_SetProperty("Clear")

-- |[Reinstate Catalysts]|
--Clearing the inventory zeroes off the catalyst count, but it persists between chapters. Reset them here.
AdInv_SetProperty("Catalyst Count", gciCatalyst_Health,     iCatalystH)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Attack,     iCatalystAt)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Initiative, iCatalystI)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Evade,      iCatalystE)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Accuracy,   iCatalystAc)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Skill,      iCatalystSk)

--Call the auto-set function with "Null". Zeroes the cur/max counts for the catalysts.
AdInv_SetProperty("Resolve Catalysts By String", "Null")
