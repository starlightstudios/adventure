-- |[ ================================ New Chapter Plus - Init ================================= ]|
--When booting into this chapter with New Chapter Plus, call this before the initialization.
-- It will store variables related to the old combat stats, and overwrite after init is complete.
gbIsNewChapterPlus = true
