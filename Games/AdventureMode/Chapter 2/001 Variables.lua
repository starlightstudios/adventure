-- |[ ================================= Boot Script Variables ================================== ]|
--Listing of variables used by scripts in Chapter 2. These variables are retained for later chapters.
-- Note: When loading the game, these will get overwritten if newer copies are found.
DL_AddPath("Root/Variables/Chapter2/")

--Zeroth save point is the one right across from the start.
AL_SetProperty("Last Save Point", "LowerShrineC")

-- |[System]|
--The party leader's voice is Sanya's most of the time.
WD_SetProperty("Set Leader Voice", "Sanya")

--Special scene variables.
DL_AddPath("Root/Variables/Chapter2/Scenes/")
VM_SetVar("Root/Variables/Chapter2/Scenes/iHasNoMap", "N", 1.0)

-- |[ ==================================== Combat Variables ==================================== ]|
--Redirect the KO counter.
DL_AddPath("Root/Variables/Chapter2/KOTracker/")
VM_SetVar("Root/Variables/Global/Combat/sKOTrackerPath", "S", "Root/Variables/Chapter2/KOTracker/")

--Make sure the hunting tracker is set.
DL_AddPath("Root/Variables/Chapter2/HuntTracker/")
VM_SetVar("Root/Variables/Global/Combat/sHuntTrackerPath", "S", "Root/Variables/Chapter2/HuntTracker/")

-- |[ ================================== Main Quest Variables ================================== ]|
-- |[Campfire Listing]|
--These store which campfires the player has accessed. The player can warp between them at their discretion.
DL_AddPath("Root/Variables/Chapter2/Campfires/")
VM_SetVar("Root/Variables/Chapter2/Campfires/iLowerShrineC",    "N", 1.0) --Always available.
VM_SetVar("Root/Variables/Chapter2/Campfires/iSanyaCabinA",     "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Campfires/iNorthwoodsNEA",   "N", 0.0) 
VM_SetVar("Root/Variables/Chapter2/Campfires/iNorthwoodsNWE",   "N", 0.0) 
VM_SetVar("Root/Variables/Chapter2/Campfires/iNorthwoodsSWC",   "N", 0.0) 
VM_SetVar("Root/Variables/Chapter2/Campfires/iKitsuneVillageB", "N", 0.0) 
VM_SetVar("Root/Variables/Chapter2/Campfires/iVucaPassD",       "N", 0.0) 
VM_SetVar("Root/Variables/Chapter2/Campfires/iGranvireD",       "N", 0.0) 
VM_SetVar("Root/Variables/Chapter2/Campfires/iMtSarulenteF",    "N", 0.0) 
VM_SetVar("Root/Variables/Chapter2/Campfires/iHarpyBaseA",      "N", 0.0) 
VM_SetVar("Root/Variables/Chapter2/Campfires/iWestwoodsSEB",    "N", 0.0) 

-- |[Special Cutscene Variables]|
--This indicates that a cutscene is being "relived". The player may relive cutscenes from the save menu.
DL_AddPath("Root/Variables/Chapter2/Scenes/")
VM_SetVar("Root/Variables/Chapter2/Scenes/sOriginalForm", "S", "Human")
VM_SetVar("Root/Variables/Chapter2/Scenes/iIsRelivingScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Scenes/sLastRelivedScene", "S", "None")

-- |[Shrine of the Hero]|
DL_AddPath("Root/Variables/Chapter2/ShrineOfTheHero/")

--Initial Encounter
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iSnidePond", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iSwissPeople", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iSawFireScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iTiredA", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iTiredB", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iTiredC", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iSawZeke", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iSawHostiles", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iGotResetAbility", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBSwitchB", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBSwitchC", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBBridge", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBDoor", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBSolved", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineCSolved", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineDSolved", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iMetZeke", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iEscapedShrine", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iFriendsWithSevavi", "N", 0.0)

--Returning
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iIzunaExplained", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iEmpressMetSevavi", "N", 0.0)

-- |[Hero Plateau]|
--There are a series of five "times" that redden and then blacken the sky. These are RGBA values.
DL_AddPath("Root/Variables/Chapter2/HeroPlateau/")
VM_SetVar("Root/Variables/Chapter2/HeroPlateau/iTime", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/HeroPlateau/iIgnoreTime", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/HeroPlateau/iStumbleScene", "N", 0.0)
gcfaPlateauColor = {}
gcfaPlateauColor[1] = {1.0, 0.7, 0.7, 0.3}
gcfaPlateauColor[2] = {0.8, 0.4, 0.4, 0.3}
gcfaPlateauColor[3] = {0.6, 0.4, 0.4, 0.3}
gcfaPlateauColor[4] = {0.4, 0.3, 0.3, 0.6}
gcfaPlateauColor[5] = {0.1, 0.1, 0.1, 0.3}

-- |[Sanya's Cabin and Mines Sequence]|
DL_AddPath("Root/Variables/Chapter2/SanyaMines/")
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iDroppedOffIzuna", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iIsIzunaInCabin", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iIsZekeInCabin", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iSawMysteryLight", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iSawMysteryLightAgain", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iDieHere", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iMinesCartAtNorth", "N", 1.0)
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iMinesCSolved", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iMinesDSolved", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iMinesESolved", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iDidPuzzleFight", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iWonPuzzleFight", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iSawRifleScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iSanyaStupidStupid", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iSawMinesExitDialogue", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iCompletedMines", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/SanyaMines/iIsNightAtMines", "N", 1.0)

-- |[Hunting Tutorial]|
DL_AddPath("Root/Variables/Chapter2/Northwoods/")
VM_SetVar("Root/Variables/Chapter2/Northwoods/iRunHuntingTutorial", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Northwoods/iIsTutorialHuntActive", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Northwoods/iSawTracks", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Northwoods/iExaminedTracks", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Northwoods/iSpottedPrey", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Northwoods/iCompletedHunt", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Northwoods/iUpgradedCabin", "N", 0.0)

-- |[ =================================== Sidequest Variables ================================== ]|
-- |[Westwoods]|
DL_AddPath("Root/Variables/Chapter2/Westwoods/")
VM_SetVar("Root/Variables/Chapter2/Westwoods/iAreaExplained", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Westwoods/iMiaGaveCartQuest", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Westwoods/iMetTakahn", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Westwoods/iHelpedTakahn", "N", 0.0)

-- |[Mt. Sarulente]|
DL_AddPath("Root/Variables/Chapter2/MtSarulente/")
VM_SetVar("Root/Variables/Chapter2/MtSarulente/iActivatedFortSwitch", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/MtSarulente/iSawTomb", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/MtSarulente/iMiaGaveEmpressQuest", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/MtSarulente/iSawBigScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/MtSarulente/iMetEmpress", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/MtSarulente/iEmpressWaiting", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/MtSarulente/iEmpressOpenedLibrary", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/MtSarulente/iIzunaSawLibrary", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/MtSarulente/iIzunaFoundBook", "N", 0.0)

-- |[Bunny Warrens]|
DL_AddPath("Root/Variables/Chapter2/BunnyWarrens/")
VM_SetVar("Root/Variables/Chapter2/BunnyWarrens/iSawFailedBunnyTF", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/BunnyWarrens/iSawBunnyInCaves", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/BunnyWarrens/iOpenedWarrens", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/BunnyWarrens/iSawAngelfaceScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/BunnyWarrens/iGotBunnyClearance", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/BunnyWarrens/iGaveBunnyClearance", "N", 0.0)

-- |[The Harpy Blockade]|
DL_AddPath("Root/Variables/Chapter2/HarpyBlockade/")
VM_SetVar("Root/Variables/Chapter2/HarpyBlockade/iBlockadeResolved", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/HarpyBlockade/iSawBlockade", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/HarpyBlockade/iFlankedBlockade", "N", 0.0)

-- |[The Harpy Base]|
DL_AddPath("Root/Variables/Chapter2/HarpyBase/")
VM_SetVar("Root/Variables/Chapter2/HarpyBase/iSawKitsuneMeeting", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/HarpyBase/iSawCookTroubles", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/HarpyBase/iSentCookGear", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/HarpyBase/iMetEsmerelda", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/HarpyBase/iEmpressMetEsmerelda", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/HarpyBase/iDidHarpyFeet", "N", 0.0)

-- |[Fort Kirysu]|
DL_AddPath("Root/Variables/Chapter2/FortKirysu/")
VM_SetVar("Root/Variables/Chapter2/FortKirysu/iMiaGaveAlrauneQuest", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/FortKirysu/iMetAlraune", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/FortKirysu/iGotSpringwater", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/FortKirysu/iBefriendedAlraunes", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/FortKirysu/iSpawnMarigold", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/FortKirysu/iEmpressSawTree", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/FortKirysu/iEmpressExtraScene", "N", 0.0)

-- |[Kitsune Village]|
--Variables.
DL_AddPath("Root/Variables/Chapter2/KitsuneVillage/")
VM_SetVar("Root/Variables/Chapter2/KitsuneVillage/iSawKidnapIntro", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/KitsuneVillage/iRescuedIzuna", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/KitsuneVillage/iYukinaMetEmpress", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/KitsuneVillage/iSkillbookQuest", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/KitsuneVillage/iSawFoundBookTutorial", "N", 0.0)

--Farmers in NorthwoodsNEE.
VM_SetVar("Root/Variables/Chapter2/KitsuneVillage/iMetFarmers", "N", 0.0)

--Farmers in NorthwoodsSEC.
VM_SetVar("Root/Variables/Chapter2/KitsuneVillage/iMetSonoko", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/KitsuneVillage/iSonokoKnowsTransform", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/KitsuneVillage/sFormMetSonokoIn", "S", "Null")

--Constants.
gciKitsuneIntro = 0
gciKitsuneRescuedIzuna = 1

-- |[Werebat TF]|
--Enumerations
gciWerebatTF_Uninfected = 0
gciWerebatTF_Infected   = 1
gciWerebatTF_SkinChange = 2
gciWerebatTF_AlmostTFd  = 3
gciWerebatTF_FullyTFd   = 4

--Variable storage
DL_AddPath("Root/Variables/Chapter2/WerebatTF/")
VM_SetVar("Root/Variables/Chapter2/WerebatTF/iWerebatStage", "N", gciWerebatTF_Uninfected)
VM_SetVar("Root/Variables/Chapter2/WerebatTF/iHasSeenWerebatSkinChange", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/WerebatTF/iHasSeenWerebatAlmostTF", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/WerebatTF/iHasSeenWerebatFullTF", "N", 0.0)

-- |[ =================================== Character Variables ================================== ]|
-- |[Mia]|
DL_AddPath("Root/Variables/Chapter2/Mia/")
VM_SetVar("Root/Variables/Chapter2/Mia/iMetMia", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Mia/iMetMiaWithIzuna", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Mia/iMiaIsMad", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Mia/sFormMetMiaIn", "S", "Null")

--Pamphlets
VM_SetVar("Root/Variables/Chapter2/Mia/iPamphletCount", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Mia/iTurnedInBunnies", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Mia/iTurnedInSarulente", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Mia/iTurnedInWestwoodsGreens", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Mia/iTurnedInMerchantCart", "N", 0.0)

-- |[Miso]|
DL_AddPath("Root/Variables/Chapter2/Miso/")
VM_SetVar("Root/Variables/Chapter2/Miso/iMetMiso", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Miso/iMisoMetEmpress", "N", 0.0)

--Weapon Unlocks
VM_SetVar("Root/Variables/Chapter2/Miso/i762LT",            "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Miso/i762Expd",          "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Miso/iGoddessWhisker",   "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Miso/iBruiserGauntlets", "N", 0.0)

--Armor Unlocks
VM_SetVar("Root/Variables/Chapter2/Miso/iHuntersJacket",    "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Miso/iWarriorsPelt",     "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Miso/iHuntersArmor",     "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Miso/iLinedMikoRobes",   "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Miso/iInfusedMikoRobes", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Miso/iWarriorsRobes",    "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Miso/iThickCollar",      "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Miso/iShimmeringCollar", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Miso/iCatalyticCollar",  "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Miso/iJawellPlatemail",  "N", 0.0)

--Accessories Unlocks
VM_SetVar("Root/Variables/Chapter2/Miso/iWoodenGreaves",        "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Miso/iSlickedWoodenGreaves", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Miso/iTreatedWoodenGreaves", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Miso/iScaleGloves",          "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Miso/iGloveLiners",          "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Miso/iButterflyCape",        "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Miso/iAmmoBag",              "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Miso/iSilkPouch",            "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Miso/iRushBracers",          "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Miso/iWardingNecklace",      "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Miso/iEverchillPads",        "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Miso/iSparklingGloves",      "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Miso/iManiacCape",           "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Miso/iManaflowRing",         "N", 0.0)

--Item Unlocks
VM_SetVar("Root/Variables/Chapter2/Miso/iHeartySoup",     "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Miso/iBrutalSandwich", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Miso/iGourmetLunch",   "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Miso/iLampOil",        "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Miso/iPitchFirebomb",  "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/Miso/iClothingPatch",  "N", 0.0)

-- |[Izuna Explains]|
--Variables for the spots Izuna has explained.
VM_SetVar("Root/Variables/Chapter2/IzunaExplains/iExplainedPufferfinLake", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/IzunaExplains/iExplainedShrineMountain", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/IzunaExplains/iExplainedVucaPass", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/IzunaExplains/iExplainedSarulente", "N", 0.0)
VM_SetVar("Root/Variables/Chapter2/IzunaExplains/iExplainedPuzzleBattle", "N", 0.0)

-- |[ ==================================== Paragon Tracking ==================================== ]|
--Tracks kill counters and paragon spawn locations.
local saGroupings = {}
local function fnAddGrouping(psName)
    local i = #saGroupings + 1
    saGroupings[i] = psName
end

--Grouping list.
fnAddGrouping("ShrineOfTheHero")

--Create variables for each grouping.
for i = 1, #saGroupings, 1 do
    DL_AddPath("Root/Variables/Paragons/" .. saGroupings[i] .. "/")
    VM_SetVar("Root/Variables/Paragons/" .. saGroupings[i] .. "/iIsEnabled", "N", 1.0)
    VM_SetVar("Root/Variables/Paragons/" .. saGroupings[i] .. "/iKillCount", "N", 0.0)
    VM_SetVar("Root/Variables/Paragons/" .. saGroupings[i] .. "/iKillsNeeded", "N", gciParagon_KO_Needed_To_Spawn)
    VM_SetVar("Root/Variables/Paragons/" .. saGroupings[i] .. "/iHasDefeatedParagon", "N", 0.0)
end

-- |[ =================================== Region Variables ==================================== ]|
--Only the region setup is done here. The gsRegionMarkerPath file does the rest.
DL_AddPath("Root/Variables/Regions/Temporary/")
DL_AddPath("Root/Variables/Regions/Permanent/")

-- |[ =================================== Costume Variables ==================================== ]|
--Sanya, Unlocked
DL_AddPath("Root/Variables/Costumes/Sanya/")
--VM_SetVar("Root/Variables/Costumes/Mei/iQueenBee", "N", 0.0)

--Sanya, Wearing
VM_SetVar("Root/Variables/Costumes/Sanya/sCostumeHuman",   "S", "Rifle")
VM_SetVar("Root/Variables/Costumes/Sanya/sCostumeSevavi",  "S", "Normal")
VM_SetVar("Root/Variables/Costumes/Sanya/sCostumeWerebat", "S", "Rifle")
VM_SetVar("Root/Variables/Costumes/Sanya/sCostumeHarpy",   "S", "Rifle")
VM_SetVar("Root/Variables/Costumes/Sanya/sCostumeBunny",   "S", "Rifle")

--Izuna, Wearing
DL_AddPath("Root/Variables/Costumes/Izuna/")
VM_SetVar("Root/Variables/Costumes/Izuna/sCostumeMiko", "S", "Normal")

--Zeke, Wearing
DL_AddPath("Root/Variables/Costumes/Zeke/")
VM_SetVar("Root/Variables/Costumes/Zeke/sCostumeGoat", "S", "Normal")

--Empress, Wearing
DL_AddPath("Root/Variables/Costumes/Empress/")
VM_SetVar("Root/Variables/Costumes/Empress/sCostumeConquerer", "S", "Normal")

--Odar, Wearing
DL_AddPath("Root/Variables/Costumes/Odar/")
VM_SetVar("Root/Variables/Costumes/Odar/sCostumePrince", "S", "Normal")

-- |[ ====================================== Party Setup ======================================= ]|
-- |[Sanya's Stats]|
--System.
DL_AddPath("Root/Variables/Global/Sanya/")

--Sanya's forms/jobs.
VM_SetVar("Root/Variables/Global/Sanya/sForm", "S", "Human")
VM_SetVar("Root/Variables/Global/Sanya/iHasSevaviForm", "N", 0.0)
VM_SetVar("Root/Variables/Global/Sanya/iHasWerebatForm", "N", 0.0)
VM_SetVar("Root/Variables/Global/Sanya/iHasHarpyForm", "N", 0.0)
VM_SetVar("Root/Variables/Global/Sanya/iHasBunnyForm", "N", 0.0)

--Sanya's Skillbook Counter
VM_SetVar("Root/Variables/Global/Sanya/iSkillbookTotal", "N", 0.0)

--Rifle Codes
gciSanyaRifle_CanFire = 0
gciSanyaRifle_DontHave = 1
gciSanyaRifle_NotHere = 2
VM_SetVar("Root/Variables/Global/Sanya/iRecoveredRifle", "N", 0.0)
VM_SetVar("Root/Variables/Global/Sanya/iRifleCode", "N", gciSanyaRifle_DontHave)

-- |[Izuna's Stats]|
--System
DL_AddPath("Root/Variables/Global/Izuna/")

--Form.
VM_SetVar("Root/Variables/Global/Izuna/sForm", "S", "Kitsune")

--Izuna's Skillbook Counter
VM_SetVar("Root/Variables/Global/Izuna/iSkillbookTotal", "N", 0.0)

--Izuna's Jobs
VM_SetVar("Root/Variables/Global/Izuna/sCurrentJob", "S", "Miko")
VM_SetVar("Root/Variables/Global/Izuna/iHasJob_Miko", "N", 1.0)

--Pamphlets
VM_SetVar("Root/Variables/Global/Izuna/sPamphlet", "S", "None")

-- |[Empress's Stats]|
--System
DL_AddPath("Root/Variables/Global/Empress/")

--Form.
VM_SetVar("Root/Variables/Global/Empress/sForm", "S", "Dragon")

--Izuna's Skillbook Counter
VM_SetVar("Root/Variables/Global/Empress/iSkillbookTotal", "N", 0.0)

--Izuna's Jobs
VM_SetVar("Root/Variables/Global/Empress/sCurrentJob", "S", "Conquerer")
VM_SetVar("Root/Variables/Global/Empress/iHasJob_Conquerer", "N", 1.0)

-- |[Zeke's Stats]|
--System
DL_AddPath("Root/Variables/Global/Zeke/")

--Form.
VM_SetVar("Root/Variables/Global/Zeke/sForm", "S", "Goat")

--Zeke's Jobs. He only has the one but y'know.
VM_SetVar("Root/Variables/Global/Zeke/sCurrentJob", "S", "Goat")
VM_SetVar("Root/Variables/Global/Zeke/iHasJob_Goat", "N", 1.0)

--Zeke's repurchasable skill states. Each time the skill is purchased, these goes up by one.
VM_SetVar("Root/Variables/Global/Zeke/iRepurchaseTotal",        "N", 0.0)
VM_SetVar("Root/Variables/Global/Zeke/iRepurchaseHealth",       "N", 0.0)
VM_SetVar("Root/Variables/Global/Zeke/iRepurchasePower",        "N", 0.0)
VM_SetVar("Root/Variables/Global/Zeke/iRepurchaseAccuracy",     "N", 0.0)
VM_SetVar("Root/Variables/Global/Zeke/iRepurchaseEvade",        "N", 0.0)
VM_SetVar("Root/Variables/Global/Zeke/iRepurchaseInitiative",   "N", 0.0)
VM_SetVar("Root/Variables/Global/Zeke/iRepurchaseItemPower",    "N", 0.0)
VM_SetVar("Root/Variables/Global/Zeke/iRepurchaseBleedDamage",  "N", 0.0)
VM_SetVar("Root/Variables/Global/Zeke/iRepurchaseStrikeDamage", "N", 0.0)

-- |[Empress' Stats]|
-- |[Odar's Stats]|
--System
DL_AddPath("Root/Variables/Global/Odar/")

--Form.
VM_SetVar("Root/Variables/Global/Odar/sForm", "S", "Fungoid")

--Odar's Skillbook Counter
VM_SetVar("Root/Variables/Global/Odar/iSkillbookTotal", "N", 6.0)

--Odar's Jobs
VM_SetVar("Root/Variables/Global/Odar/sCurrentJob", "S", "Prince")
VM_SetVar("Root/Variables/Global/Odar/iHasJob_Prince", "N", 1.0)

-- |[ ================================= TF Sequence Variables ================================== ]|
--Variables related to specific transformation cutscenes. These are reset when reliving the scene.
-- Any permanent decisions should not be in this block.

-- |[Statue]|

-- |[ =================================== Dialogue Variables =================================== ]|
-- |[Dialogue: Izuna]|
--Dictates what Izuna does and does not know.
DL_AddPath("Root/Variables/Chapter2/Izuna/")
VM_SetVar("Root/Variables/Chapter2/Izuna/iHasSeenSanyaTransform", "N", 0.0)

-- |[World: State Variables]|
--Instances of things like objects being searched, doors being unlocked, switches pressed, and so on.
DL_AddPath("Root/Variables/Chapter2/WorldState")

-- |[ ==================================== Global Variables ==================================== ]|
-- |[Global Variables]|
--Variables.
gsPartyLeaderName = "Sanya"
giPartyLeaderID = 0

--Following Characters.
giFollowersTotal = 0
gsaFollowerNames = {}
giaFollowerIDs = {0}

-- |[Map Setup]|
LM_ExecuteScript(gsRoot .. "Maps/Z Map Lookups/Chapter 2 Lookups.lua")

-- |[ ====================================== Map Variables ===================================== ]|
--As the player explores, their maps get updated with new information. These variables track which
-- applicable rooms the player has entered.
gciNorthwoodsTotalMaps = 28
DL_AddPath("Root/Variables/Chapter2/NorthwoodsMap/")
for i = 0, 28, 1 do
    local sVariablePath = string.format("Root/Variables/Chapter2/NorthwoodsMap/iRoom%02i", i)
    VM_SetVar(sVariablePath, "N", 0.0)
end

--Westwoods
gciWestwoodsTotalMaps = 38
DL_AddPath("Root/Variables/Chapter2/WestwoodsMap/")
for i = 0, 38, 1 do
    local sVariablePath = string.format("Root/Variables/Chapter2/WestwoodsMap/iRoom%02i", i)
    VM_SetVar(sVariablePath, "N", 0.0)
end

--MtSarulente
gciMtSarulenteTotalMaps = 14
DL_AddPath("Root/Variables/Chapter2/MtSarulenteMap/")
for i = 0, 14, 1 do
    local sVariablePath = string.format("Root/Variables/Chapter2/MtSarulenteMap/iRoom%02i", i)
    VM_SetVar(sVariablePath, "N", 0.0)
end

-- |[ ================================= Puzzle Fight Minigame ================================== ]|
--These variables are used by the puzzle fighting minigame.
DL_AddPath("Root/Variables/Global/PuzzleFight/")

--Location Constants
gci_PuzzleFightLocation_Invalid = 0
gci_PuzzleFightLocation_SanyaMinesG = 1

--Tutorial States
gci_PuzzleFightTutorial_None = 0
gci_PuzzleFightTutorial_ExplainControls = 1
gci_PuzzleFightTutorial_PlayerFirstMove = 2
gci_PuzzleFightTutorial_PlayerFirstMoveFailed = 3
gci_PuzzleFightTutorial_PlayerFirstMoveSuccess = 4
gci_PuzzleFightTutorial_PlayerSecondMove = 5
gci_PuzzleFightTutorial_PlayerSecondMoveFailed = 6
gci_PuzzleFightTutorial_PlayerSecondMoveSuccess = 7

--Variables
VM_SetVar("Root/Variables/Global/PuzzleFight/iLocation", "N", gci_PuzzleFightLocation_Invalid)

--Lua-Side Structures. This is initialized in the minigame initializer script.
gzaPuzzleFight = nil
function fnInitPuzzleFight()
    
    -- |[Setu[|
    --Allocate.
    gzaPuzzleFight = {}
    
    -- |[Variables]|
    --System.
    gzaPuzzleFight.bIsGameOver = false
    gzaPuzzleFight.bPlayerWonGame = false
    
    --Tutorial and Script.
    gzaPuzzleFight.sPostExecScript = "Null"
    gzaPuzzleFight.iTutorialState = gci_PuzzleFightTutorial_None
    
    --Board Statistics.
    gzaPuzzleFight.iBoardStartX = 0
    gzaPuzzleFight.iBoardStartY = 0
    gzaPuzzleFight.iBoardWidth = 0
    gzaPuzzleFight.iBoardHeight = 0
    gzaPuzzleFight.iTileW = 1
    gzaPuzzleFight.iTileH = 1
    
    --Variables.
    gzaPuzzleFight.bHasPrintedTitle = false
    
    --Player Storage
    gzaPuzzleFight.sEntityName = "Null"
    gzaPuzzleFight.bPerfectGuardThisTurn = false
    gzaPuzzleFight.bAttackUpThisTurn = false
    gzaPuzzleFight.iTicksLeft = 0
    gzaPuzzleFight.iMovesLeft = 0
    
    --Enemy Storage
    gzaPuzzleFight.iEnemiesTotal = 0
    gzaPuzzleFight.zaEnemies = {}
    
    --Construction Variables
    gzaPuzzleFight.bArrowsEndedEarly = false
    
    -- |[General Functions]|
    --Position calculators.
    function gzaPuzzleFight.fnGetWorldX(pfBoardX)
        return (gzaPuzzleFight.iBoardStartX * gciSizePerTile) + (pfBoardX * gciSizePerTile * gzaPuzzleFight.iTileW) + (gzaPuzzleFight.iTileW * 0.40 * gciSizePerTile)
    end
    function gzaPuzzleFight.fnGetWorldY(pfBoardY)
        return (gzaPuzzleFight.iBoardStartY * gciSizePerTile) + (pfBoardY * gciSizePerTile * gzaPuzzleFight.iTileH) + (gzaPuzzleFight.iTileH * 0.50 * gciSizePerTile)
    end
    
    -- |[Enemy Functions]|
    --Adder
    function gzaPuzzleFight.fnAddEnemy(psActorName, piX, piY)
        
        --Arg check.
        if(psActorName == nil) then return end
        if(piX == nil) then return end
        if(piY == nil) then return end
        
        --Add space.
        gzaPuzzleFight.iEnemiesTotal = gzaPuzzleFight.iEnemiesTotal + 1
        local i = gzaPuzzleFight.iEnemiesTotal
        
        --Create, set.
        gzaPuzzleFight.zaEnemies[i] = {}
        gzaPuzzleFight.zaEnemies[i].sActorName = psActorName
        gzaPuzzleFight.zaEnemies[i].iX = piX
        gzaPuzzleFight.zaEnemies[i].iY = piY

    end

    --Get enemy at location.
    function gzaPuzzleFight.fnGetEnemyInSlot(piX, piY)
        
        --Arg check.
        if(piX == nil) then return -1 end
        if(piY == nil) then return -1 end
        
        --Scan.
        for i = 1, gzaPuzzleFight.iEnemiesTotal, 1 do
            if(gzaPuzzleFight.zaEnemies[i].iX == piX and gzaPuzzleFight.zaEnemies[i].iY == piY) then
                return i
            end
        end
        
        --None found.
        return -1
    end
    
    function gzaPuzzleFight.fnGetEnemyInLine(piStartX, piStartY, piChangeX, piChangeY)
        
        --Arg check.
        if(piStartX  == nil) then return -1 end
        if(piStartY  == nil) then return -1 end
        if(piChangeX == nil) then return -1 end
        if(piChangeY == nil) then return -1 end
        
        --Setup.
        local iCurX = piStartX
        local iCurY = piStartY
        
        --Iterate.
        while(true) do
        
            --Scan enemies on this position.
            for i = 1, gzaPuzzleFight.iEnemiesTotal, 1 do
                if(gzaPuzzleFight.zaEnemies[i].iX == iCurX and gzaPuzzleFight.zaEnemies[i].iY == iCurY) then
                    return i
                end
            end
        
            --No enemy found. Move cursor.
            iCurX = iCurX + piChangeX
            iCurY = iCurY + piChangeY
        
            --Range check.
            if(iCurX < 0) then return -1 end
            if(iCurY < 0) then return -1 end
            if(iCurX >= gzaPuzzleFight.iBoardWidth) then return -1 end
            if(iCurY >= gzaPuzzleFight.iBoardHeight) then return -1 end
        
        end
        
        --None found.
        return -1
    end

    --Enemy Clear
    function gzaPuzzleFight.fnClearEnemies()
        gzaPuzzleFight.iEnemiesTotal = 0
        gzaPuzzleFight.zaEnemies = {}
    end
    
end
