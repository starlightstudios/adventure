-- |[ ============================== Ability Standard Effect Tags ============================== ]|
--The function fnAddAbilityEffectPack() and similar calls can apply sets of tags to an ability application
-- with a single string. This is a table of those.
gzaEffectTagModifierList = {}

-- |[Damage Type Sets]|
--All damage type sets have an apply tag, and a damage taken tag.
local saDamageTypeList = {"Slash", "Strike", "Pierce", "Flame", "Freeze", "Shock", "Crusade", "Obscure", "Bleed", "Poison", "Corrode", "Terrify"}
for i = 1, #saDamageTypeList, 1 do

    --Create.
    local zDamageSet = {}
    zDamageSet.sTagName           = saDamageTypeList[i] .. " Standard"
    zDamageSet.saApplyTagBonus    = saDamageTypeList[i] .. " Apply +"
    zDamageSet.saApplyTagMalus    = saDamageTypeList[i] .. " Apply -"
    zDamageSet.saSeverityTagBonus = saDamageTypeList[i] .. " Damage Taken +"
    zDamageSet.saSeverityTagMalus = saDamageTypeList[i] .. " Damage Taken -"

    --Register.
    table.insert(gzaEffectTagModifierList, zDamageSet)
end

-- |[Other Modifier Sets]|
--Blind Set. Has an effect boost since Blind isn't a damage type.
local zBlindSet = {}
zBlindSet.sTagName           = "Blind Standard"
zBlindSet.saApplyTagBonus    = "Blind Apply +"
zBlindSet.saApplyTagMalus    = "Blind Apply -"
zBlindSet.saSeverityTagBonus = "Blind Effect +"
zBlindSet.saSeverityTagMalus = "Blind Effect -"
table.insert(gzaEffectTagModifierList, zBlindSet)