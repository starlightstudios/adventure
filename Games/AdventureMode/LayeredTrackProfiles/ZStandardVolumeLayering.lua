-- |[ ================================ Standard Volume Layering ================================ ]|
--Build the volume listing. There are 101 volume values from 0 to 100 for each track, representing
-- how loud each of the layers should be at that level of intensity.
--This is the 'standard' volume listing. Most layered tracks use it. Has optional flags to allow
-- normalize-up and normalize-down if the total volume across all tracks is below 1.0 or above 1.0.
--This particular instance is set for three tracks.
if(fnArgCheck(2) == false) then return end

--Resolve args
local iNormalizeUp = LM_GetScriptArgument(0, "N")
local iNormalizeDn = LM_GetScriptArgument(1, "N")

--Setup
local iTracksTotal = 3
local faVolumeList = {}
faVolumeList[0] = {}
faVolumeList[1] = {}
faVolumeList[2] = {}

-- |[ ========= Track Zero, Passive ========== ]|
--Setup
local ciLow = 30
local ciMid = 25
local ciHi  = 40
for iIntensity = 0, 100, 1 do
    
    --Below the low value? Track Zero is at maximum volume.
    if(iIntensity <= ciLow) then
        faVolumeList[0][iIntensity] = 1.0
    
    --Below the high value? The track ramps down until it reaches zero.
    elseif(iIntensity < ciHi) then
        local fVolume = 1.0 - ((iIntensity - ciLow) / (ciHi - ciLow))
        faVolumeList[0][iIntensity] = fVolume
    
    --Past the high value, the track is zeroed in volume.
    else
        faVolumeList[0][iIntensity] = 0.0
    end
end

-- |[ ========== Track One, Tension ========== ]|
ciLow = 30
ciMid = 60
ciHi = 70
for iIntensity = 0, 100, 1 do
    
    --Below the low value, Track One is at zero volume.
    if(iIntensity < ciLow) then
        faVolumeList[1][iIntensity] = 0.0
    
    --On the way to the medium intensity, Track One increases in volume. It peaks at medium intensity.
    elseif(iIntensity < ciMid) then
        local fVolume = ((iIntensity - ciLow) / (ciMid - ciLow))
        faVolumeList[1][iIntensity] = fVolume
    
    --Volume decreases after the medium intensity. It zeroes off at the high intensity.
    elseif(iIntensity < ciHi) then
        local fVolume = 1.0 - ((iIntensity - ciMid) / (ciHi - ciMid))
        faVolumeList[1][iIntensity] = fVolume
    
    --Beyond this point, the track is silent.
    else
        faVolumeList[1][iIntensity] = 0.0
    end
end

-- |[ ========== Track Two, Combat =========== ]|
ciLow = 65
ciHi = 100
for iIntensity = 0, 100, 1 do
    
    --Below the low value, Track Two is at zero volume.
    if(iIntensity < ciLow) then
        faVolumeList[2][iIntensity] = 0.0
    
    --After the low intensity is reached, ramp up until the high intensity.
    else
        local fVolume = ((iIntensity - ciLow) / (ciHi - ciLow))
        faVolumeList[2][iIntensity] = fVolume
    end
    
end

-- |[ ==== Volume Normalization and Input ==== ]|
--We need to make sure the sum of all volumes across all intensities is less than or equal 1.0. Normalize them.
-- For this particular script, we also normalize up to zero to prevent gaps.
for iIntensity = 0, 100, 1 do
    
    --Setup
    local fTotal = 0
    for i = 0, iTracksTotal-1, 1 do
        fTotal = fTotal + faVolumeList[i][iIntensity]
    end
    
    --If all volumes sum to zero, assume it was intentional.
    if(fTotal <= 0.0) then
    
    --If the volumes sum to a value beneath 1.0, then we need to proportionally raise them.
    elseif(fTotal < 1.0) then
    
        if(iNormalizeUp == 1.0) then
            for i = 0, iTracksTotal-1, 1 do
                faVolumeList[i][iIntensity] = faVolumeList[i][iIntensity] / fTotal
            end
        end
    
    --Otherwise, we need to normalize the values.
    else
        if(iNormalizeDn == 1.0) then
            for i = 0, iTracksTotal-1, 1 do
                faVolumeList[i][iIntensity] = faVolumeList[i][iIntensity] / fTotal
            end
        end
    end
    
    --Now input the value.
    for i = 0, iTracksTotal-1, 1 do
        AL_SetProperty("Layered Track Volume At Intensity", i, iIntensity, faVolumeList[i][iIntensity])
    end
    
end
