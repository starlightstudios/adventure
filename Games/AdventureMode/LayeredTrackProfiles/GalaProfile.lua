-- |[Layered Track: Gala]|
--Played during the gala segment. Does not auto-normalize since all volumes are script-mandated. The zero track
-- is silence since some areas have no music playing.
local iTracksTotal = 3
local saTrackNames = {"GalaTenseLayer", "GalaPianoDistantLayer", "GalaPianoLayer"}

--Set this flag to indicate we handled setting layered tracks.
AL_SetProperty("Is Layering Music", true)

--Set the tracks and names.
AL_SetProperty("Total Layering Tracks", iTracksTotal)
for i = 0, iTracksTotal-1, 1 do
    AL_SetProperty("Layered Track Name", i, saTrackNames[i+1])
end

-- |[Description]|
--Uses a non-standard layering structure, since all the intensity values are script-handled. The values are:
--20: Silence
--40: Tension Theme
--60: Tension Theme + Distant Piano
--80: Piano Only
local faVolumeList = {}
faVolumeList[0] = {}
faVolumeList[1] = {}
faVolumeList[2] = {}

-- |[Track Zero: Tension]|
--Setup
local ciSilent = 20
local ciThemePoint = 40
local ciDistantPoint = 60
local ciZero = 80
for iIntensity = 0, 100, 1 do
    
    --Below the silence value, the track is zeroed.
    if(iIntensity <= ciSilent) then
        faVolumeList[0][iIntensity] = 0.0
    
    --Ramp to 1.0.
    elseif(iIntensity <= ciThemePoint) then
        local fVolume = ((iIntensity - ciSilent) / (ciThemePoint - ciSilent))
        faVolumeList[0][iIntensity] = fVolume
    
    --Maintain 1.0 to the distant point.
    elseif(iIntensity <= ciDistantPoint) then
        faVolumeList[0][iIntensity] = 1.0
    
    --Fade down.
    elseif(iIntensity < ciZero) then
        local fVolume = 1.0 - ((iIntensity - ciDistantPoint) / (ciZero - ciDistantPoint))
        faVolumeList[0][iIntensity] = fVolume
    
    --Past the zero, silence.
    else
        faVolumeList[0][iIntensity] = 0.0
    end
end

-- |[Track One: Distant Piano]|
for iIntensity = 0, 100, 1 do
    
    --Below the silence value, the track is zeroed.
    if(iIntensity <= ciSilent) then
        faVolumeList[1][iIntensity] = 0.0
    
    --Maintain zero to the theme point.
    elseif(iIntensity <= ciThemePoint) then
        faVolumeList[1][iIntensity] = 0.0
    
    --Ramp to 1.0 to the distant point.
    elseif(iIntensity <= ciDistantPoint) then
        local fVolume = ((iIntensity - ciThemePoint) / (ciDistantPoint - ciThemePoint))
        faVolumeList[1][iIntensity] = fVolume
    
    --Fade down.
    elseif(iIntensity < ciZero) then
        local fVolume = 1.0 - ((iIntensity - ciDistantPoint) / (ciZero - ciDistantPoint))
        faVolumeList[1][iIntensity] = fVolume
    
    --Past the zero, silence.
    else
        faVolumeList[1][iIntensity] = 0.0
    end
end

-- |[Track Two: Full Piano]|
for iIntensity = 0, 100, 1 do
    
    --Below the distant value, zeroed.
    if(iIntensity <= ciDistantPoint) then
        faVolumeList[2][iIntensity] = 0.0
        
    --Ramp to 1.0 to the full piano point.
    elseif(iIntensity <= ciZero) then
        local fVolume = ((iIntensity - ciDistantPoint) / (ciZero - ciDistantPoint))
        faVolumeList[2][iIntensity] = fVolume
    
    --Maintain max volume to 100.
    else
        faVolumeList[2][iIntensity] = 1.0
    end
end

-- |[Volume Input]|
--No normalization.
for iIntensity = 0, 100, 1 do
    
    --Now input the value.
    for i = 0, iTracksTotal-1, 1 do
        AL_SetProperty("Layered Track Volume At Intensity", i, iIntensity, faVolumeList[i][iIntensity])
    end
    
end
