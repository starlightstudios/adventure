-- |[ ================================ Layered Track: Cultists ================================= ]|
--Information for the cultist layered tracks.
local iTracksTotal = 3
local saTrackNames = {"CultistPassive", "CultistTense", "CultistCombat"}

--Set this flag to indicate we handled setting layered tracks.
AL_SetProperty("Is Layering Music", true)
AL_SetProperty("Is Combat Max Intensity", true)

--Set the tracks and names.
AL_SetProperty("Total Layering Tracks", iTracksTotal)
for i = 0, iTracksTotal-1, 1 do
    AL_SetProperty("Layered Track Name", i, saTrackNames[i+1])
end

-- |[Description]|
--Use the standard volume layering techniques.
LM_ExecuteScript(fnResolvePath() .. "ZStandardVolumeLayering.lua", 1, 1)
