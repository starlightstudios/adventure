-- |[Layered Track: LRT Facility]|
--Information for the LRT's layered tracks. DrDissonance called it "Telecomms" because he's a wad.
local iTracksTotal = 3
local saTrackNames = {"TelecommLayer0", "TelecommLayer1", "TelecommLayer2"}

--Set this flag to indicate we handled setting layered tracks.
AL_SetProperty("Is Layering Music", true)
AL_SetProperty("Is Combat Max Intensity", true)

--Set the tracks and names.
AL_SetProperty("Total Layering Tracks", iTracksTotal)
for i = 0, iTracksTotal-1, 1 do
    AL_SetProperty("Layered Track Name", i, saTrackNames[i+1])
end

-- |[Description]|
--Use the standard volume layering techniques.
LM_ExecuteScript(fnResolvePath() .. "ZStandardVolumeLayering.lua", 1, 1)
