-- |[ ================================== Compute Damage Range ================================== ]|
-- |[Scattering Function]|
--Takes in a damage number and outputs the low and high range when scattered.
function fnScatterDamage(piDamage, piScatterRange)
    
    --Arg check.
    if(piDamage       == nil) then return 0, 0 end
    if(piScatterRange == nil) then return 0, 0 end
    
    --Zero damage.
    if(piDamage < 1) then return 0, 0 end
    
    --No scatter.
    if(piScatterRange < 1) then return piDamage, piDamage end
    
    --Set high and low values.
    local iLoDamage = piDamage
    local iHiDamage = piDamage
        
    --The number of possible elements in the scatter roll is 2x scatter + 1. The +1 is for the zero. A scatter of 1
    -- therefore has 3 possibilities: -1, 0, 1.
    local fLoRange = (100 - piScatterRange) / 100.0
    local fHiRange = (100 + piScatterRange) / 100.0
        
    --Modify.
    iLoDamage = math.floor(iLoDamage * fLoRange)
    iHiDamage = math.floor(iHiDamage * fHiRange)

    --Clamp.
    if(iLoDamage < 1 and bAtLeastOneNonImmuneType) then
        iLoDamage = 1
    end
    if(iHiDamage < 1 and bAtLeastOneNonImmuneType) then
        iHiDamage = 1
    end
    
    --Return.
    return iLoDamage, iHiDamage
end

-- |[Damage Range by Type]|
--Given attack power and resistance, returns the minimum and maximum damage dealt.
function fnComputeDamageRangeType(piAttackPower, piResistance, pbIsTargetImmune, piScatterRange)

    --Argument check.
    if(piAttackPower    == nil) then return 0, 0 end
    if(piResistance     == nil) then return 0, 0 end
    if(pbIsTargetImmune == nil) then return 0, 0 end
    if(piScatterRange   == nil) then return 0, 0 end
    
    --Attack power is zero.
    if(piAttackPower < 1) then return 0, 0 end
    
    --Target is immune!
    if(pbIsTargetImmune) then return 0, 0 end
    
    --Compute. This is the zero-scatter damage.
    local iDamage = fnComputeCombatDamage(piAttackPower, 0, piResistance)
    
    --Now scatter it.
    return fnScatterDamage(iDamage, piScatterRange)
end

-- |[Damage Range Given IDs]|
--Given IDs and a resistance type, returns the minimum and maximum damage dealt.
function fnComputeDamageRangeTypeID(piAttackerID, piDefenderID, piAttackType, piScatterRange, pbIgnoreProtection, piPenetration)

    -- |[Setup]|
    --Argument check.
    if(piAttackerID   == nil) then return 0, 0 end
    if(piDefenderID   == nil) then return 0, 0 end
    if(piAttackType   == nil) then return 0, 0 end
    if(piScatterRange == nil) then return 0, 0 end
    
    -- |[Attacker Statistics]|
    --Damage bonus from tags.
    local fDamageFactor = 1.00
    
    --Push and query attacker.
    AdvCombat_SetProperty("Push Entity By ID", piAttackerID)
    
        --Attack power.
        local iOriginatorAttackPower = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
        
        --Tag bonus.
        for i = 1, #gzaTagTable[piAttackType].saAtkAdd, 1 do
            fDamageFactor = fDamageFactor + (AdvCombatEntity_GetProperty("Tag Count", gzaTagTable[piAttackType].saAtkAdd[i]) * 0.01)
        end
        for i = 1, #gzaTagTable[piAttackType].saAtkSub, 1 do
            fDamageFactor = fDamageFactor - (AdvCombatEntity_GetProperty("Tag Count", gzaTagTable[piAttackType].saAtkSub[i]) * 0.01)
        end
    DL_PopActiveObject()
    
    -- |[Defender Statistics]|
    --Push and query defender.
    AdvCombat_SetProperty("Push Entity By ID", piDefenderID)
    
        --Defense values
        local iTargetProtection = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Protection)
        local iTargetResistance = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Resist_Start+piAttackType)
        local bIsTargetImmune   = fnIsEntityImmuneToDamageType(piAttackType)
        
        --If this flag is set, zero the protection if it is above zero.
        if(pbIgnoreProtection) then
            if(iTargetProtection > 0) then
                iTargetProtection = 0
            end
        
        --If this is set, subtract it from the protection value. Clamp at zero.
        elseif(piPenetration ~= 0) then
            iTargetProtection = iTargetProtection - piPenetration
            if(iTargetProtection < 0) then iTargetProtection = 0 end
        end
        
        --Tags.
        for i = 1, #gzaTagTable[piAttackType].saDefAdd, 1 do
            fDamageFactor = fDamageFactor + (AdvCombatEntity_GetProperty("Tag Count", gzaTagTable[piAttackType].saDefAdd[i]) * 0.01)
        end
        for i = 1, #gzaTagTable[piAttackType].saDefSub, 1 do
            fDamageFactor = fDamageFactor - (AdvCombatEntity_GetProperty("Tag Count", gzaTagTable[piAttackType].saDefSub[i]) * 0.01)
        end
    DL_PopActiveObject()
    
    -- |[Subroutine]|
    return fnComputeDamageRangeType(iOriginatorAttackPower * fDamageFactor, iTargetProtection + iTargetResistance, bIsTargetImmune, piScatterRange)
end

-- |[Damage Range by Weapon Type Using IDs]|
--Abilities that deal "Weapon" damage deal a variable type, which can include several types. This algorithm determines them and sums them together.
function fnComputeDamageRangeWeapon(piAttackerID, piDefenderID, piScatterRange, pbIgnoreProtection, piPenetration)
    
    -- |[Setup]|
    --Argument check.
    if(piAttackerID   == nil) then return 0, 0 end
    if(piDefenderID   == nil) then return 0, 0 end
    if(piScatterRange == nil) then return 0, 0 end
    
    -- |[Attacker Statistics]|
    --Push and query attacker.
    AdvCombat_SetProperty("Push Entity By ID", piAttackerID)
    
        --Get bases.
        local iOriginatorAttackPower = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
        local fOriginatoraDamageFactors = fnBuildWeaponDamage()
        
        --Check the damage bonus factor and multiply the weapon factor by it.
        local fTagFactor = 1.00
        for i = gciDamageType_Slashing, gciDamageType_Terrifying, 1 do
            
            --Reset factor.
            fTagFactor = 1.00
            
            --Scan for bonuses.
            for p = 1, #gzaTagTable[i].saAtkAdd, 1 do
                fTagFactor = fTagFactor + (AdvCombatEntity_GetProperty("Tag Count", gzaTagTable[i].saAtkAdd[p]) * 0.01)
                fTagFactor = fTagFactor - (AdvCombatEntity_GetProperty("Tag Count", gzaTagTable[i].saAtkSub[p]) * 0.01)
            end
            
            --Apply bonus to damage factor.
            fOriginatoraDamageFactors[i] = fOriginatoraDamageFactors[i] * fTagFactor
        end
        
    DL_PopActiveObject()
    
    -- |[Defender Statistics]|
    --List of tag-based resistance factors for the defender.
    local faDefenderFactors = {}
    
    --Push and query defender.
    AdvCombat_SetProperty("Push Entity By ID", piDefenderID)

        --Get variables.
        local fX, fY = AdvCombatEntity_GetProperty("Position")
        local iTargetProtection = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Protection)
        
        --If this flag is set, zero the protection if it is above zero.
        if(pbIgnoreProtection) then
            if(iTargetProtection > 0) then
                iTargetProtection = 0
            end
        
        --If this is set, subtract it from the protection value. Clamp at zero.
        elseif(piPenetration ~= 0) then
            iTargetProtection = iTargetProtection - piPenetration
            if(iTargetProtection < 0) then iTargetProtection = 0 end
        end
        
        --Get resistance factors.
        local iaTargetResistances = {}
        local baTargetImmunities = {}
        for i = 0, gciDamageType_Total-1, 1 do
            iaTargetResistances[i] = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Resist_Start+i)
            baTargetImmunities[i] = fnIsEntityImmuneToDamageType(i)
        end
        
        --Get tagged resistances.
        for i = gciDamageType_Slashing, gciDamageType_Terrifying, 1 do
            
            --Reset factor.
            faDefenderFactors[i] = 1.00
            
            --Scan for bonuses.
            for p = 1, #gzaTagTable[i].saAtkAdd, 1 do
                faDefenderFactors[i] = faDefenderFactors[i] + (AdvCombatEntity_GetProperty("Tag Count", gzaTagTable[i].saDefAdd[p]) * 0.01)
                faDefenderFactors[i] = faDefenderFactors[i] - (AdvCombatEntity_GetProperty("Tag Count", gzaTagTable[i].saDefSub[p]) * 0.01)
            end
        
            --Multiply the attacker's damage factor by this.
            fOriginatoraDamageFactors[i] = fOriginatoraDamageFactors[i] * faDefenderFactors[i]
        end

    --Clean.
    DL_PopActiveObject()
        
    -- |[Damage Computation]|
    --Iterate across the damage types.
    local bAtLeastOneNonImmuneType = false
    local iTotalDamage = 0
    for i = 0, gciDamageType_Total-1, 1 do
        
        --If the value is nonzero:
        if(fOriginatoraDamageFactors[i] > 0.0) then
            
            --Check if the target is immune to this damage type:
            if(baTargetImmunities[i] == true) then
            
            --Not immune. Add damage.
            else
            
                --Set this flag.
                bAtLeastOneNonImmuneType = true
            
                --Compute.
                local fBonusPct = 1.0
                local iDamage = fnComputeCombatDamage(iOriginatorAttackPower * fOriginatoraDamageFactors[i], iTargetProtection, iaTargetResistances[i])
                
                if(iDamage > 0) then
                    iTotalDamage = iTotalDamage + (iDamage * fBonusPct)
                end
            end
        end
    end

    -- |[Finish]|
    --If there were no non-immune types, return zero damage.
    if(bAtLeastOneNonImmuneType == false) then return 0, 0 end
    
    --Once total damage is computer, return the scattered value.
    return fnScatterDamage(iTotalDamage, piScatterRange)
end
