-- |[ ================================== Standard Damage Type ================================== ]|
--The 'Standard' damage computation, given an originator, a target, and a scatter range. This does not
-- handle any animation. The global damage and attack type values are populated so the caller can
-- handle animating this.
--Unlike fnStandardAttack, this uses only one damage type. They are otherwise very similar.

--The value giStandardDamage is populated with the damage.
--The value gbStandardImmune is populated with whether or not the target was immune to the damage type.

--The argument piAttackType should be one of gciDamageType_[x].
--The argument piScatterRange is a positive integer. Pass 0 for no scatter.
fnStandardDamageType = function(piAttackerID, piDefenderID, piAttackType, piScatterRange, pbIgnoreProtection, piPenetration)
    
    -- |[Setup]|
    --Argument check.
    if(piAttackerID   == nil) then return end
    if(piDefenderID   == nil) then return end
    if(piAttackType   == nil) then return end
    if(piScatterRange == nil) then return end
    
    --Set globals.
    giStandardDamage = 0
    gbStandardImmune = false
    giStandardAttackType = piAttackType
    
    -- |[Attacker Statistics]|
    --Push and query attacker.
    AdvCombat_SetProperty("Push Entity By ID", piAttackerID)
    
        --Basic attack power.
        local iOriginatorAttackPower = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
        
        --For each tag, add 1% damage. Can be negative.
        local iIncreaseTags = 0
        for i = 1, #gzaTagTable[giStandardAttackType].saAtkAdd, 1 do
            iIncreaseTags = iIncreaseTags + AdvCombatEntity_GetProperty("Tag Count", gzaTagTable[giStandardAttackType].saAtkAdd[i])
            iIncreaseTags = iIncreaseTags - AdvCombatEntity_GetProperty("Tag Count", gzaTagTable[giStandardAttackType].saAtkSub[i])
        end
    DL_PopActiveObject()
    
    -- |[Defender Statistics]|
    --Push and query defender.
    AdvCombat_SetProperty("Push Entity By ID", piDefenderID)
        local iTargetProtection = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Protection)
        local iTargetResistance = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Resist_Start+piAttackType)
        local bIsTargetImmune   = fnIsEntityImmuneToDamageType(piAttackType)
        
        --If this flag is set, protection is always zero, or negative if applicable.
        if(pbIgnoreProtection) then
            if(iTargetProtection > 0) then
                iTargetProtection = 0
            end
        
        --If this is above zero, reduce protection by this amount. Clamp at zero.
        elseif(piPenetration > 0) then
            iTargetProtection = iTargetProtection - piPenetration
            if(iTargetProtection < 0) then iTargetProtection = 0 end
        end
        
        --For each tag, add 1% damage. Can be negative.
        for i = 1, #gzaTagTable[giStandardAttackType].saAtkAdd, 1 do
            iIncreaseTags = iIncreaseTags + AdvCombatEntity_GetProperty("Tag Count", gzaTagTable[giStandardAttackType].saDefAdd[i])
            iIncreaseTags = iIncreaseTags - AdvCombatEntity_GetProperty("Tag Count", gzaTagTable[giStandardAttackType].saDefSub[i])
        end
    DL_PopActiveObject()
    
    --Target is immune!
    if(bIsTargetImmune) then
        gbStandardImmune = true
        return
    end
    
    --Compute.
    local fBonusFactor = 1.0 + (iIncreaseTags * 0.01)
    local iDamage = fnComputeCombatDamage(iOriginatorAttackPower * fBonusFactor, iTargetProtection, iTargetResistance)
    
    --No scattering, we're done.
    if(piScatterRange < 1.0) then
        giStandardDamage = iDamage
    end
    
    --Apply scatter.
    local iLoDamage, iHiDamage = fnScatterDamage(iDamage, piScatterRange)
    
    --Roll three numbers and divide by the max. This gives a bell-curve rather than a flat roll.
    local iRollA = LM_GetRandomNumber(0, 100)
    local iRollB = LM_GetRandomNumber(0, 100)
    local iRollC = LM_GetRandomNumber(0, 100)
    local fFinalPercent = (iRollA + iRollB + iRollC) / 300.0
    
    --Compute the final damage.
    local iFinalDamage = math.floor(iLoDamage + ((iHiDamage - iLoDamage) * fFinalPercent))

    -- |[Finish Up]|
    --Clamp to 0 damage.
    if(iFinalDamage < 0) then iFinalDamage = 0 end
    
    --Populate globals.
    giStandardDamage = iFinalDamage
    giStandardAttackType = iHighestDamageIndex
end
