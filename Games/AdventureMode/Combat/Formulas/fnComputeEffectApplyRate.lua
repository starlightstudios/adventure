-- |[ =============================== Compute Effect Apply Rate ================================ ]|
--Computes and returns the change for a given Effect to apply to a given defender. Returned value is an integer
-- from 0 to 100.
function fnComputeEffectApplyRate(piResistance, piStrength)
    
    -- |[Argument check]|
    if(piResistance == nil) then return 0 end
    if(piStrength   == nil) then return 0 end
    
    -- |[Finalize]|
    local iEffectiveResist = piResistance - piStrength
    local fApplyRate = (10 - iEffectiveResist) * 10.0
    return fApplyRate
end

--Given the target ID, returns the application rate.
function fnComputeEffectApplyRateID(piDefenderID, piStrength, piType)
    
    --Arg check.
    if(piDefenderID == nil) then return 0 end
    if(piStrength   == nil) then return 0 end
    if(piType       == nil) then return 0 end

    --Push and query defender.
    AdvCombat_SetProperty("Push Entity By ID", piDefenderID)
        local iResistance = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Resist_Start + piType)
    DL_PopActiveObject()
    
    --Finish up.
    return fnComputeEffectApplyRate(iResistance, piStrength)
end
