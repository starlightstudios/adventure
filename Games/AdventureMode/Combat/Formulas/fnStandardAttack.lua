-- |[ ===================================== Standard Attack ==================================== ]|
--The 'Standard' damage computation, given an originator, a target, and a scatter range. This does not
-- handle any animation. The global damage and attack type values are populated so the caller can
-- handle animating this.
--The value giStandardDamage is populated with the damage.
--The value gsStandardAttackType is populated with the dominant attack type, such as "Slashing" or "Piercing"
--The argument piScatterRange is a positive integer. Pass 0 for no scatter.
fnStandardAttack = function(piAttackerID, piDefenderID, piScatterRange, pbIgnoreProtection, piPenetration)
    
    -- |[Setup]|
    --Argument check.
    if(piAttackerID   == nil) then return end
    if(piDefenderID   == nil) then return end
    if(piScatterRange == nil) then return end
    
    --Set globals.
    giStandardDamage = 0
    gbStandardImmune = false
    giStandardAttackType = gciDamageType_Slashing
    
    --Subroutine gets the damage range using weapon damage.
    local iLoDamage, iHiDamage = fnComputeDamageRangeWeapon(piAttackerID, piDefenderID, piScatterRange, pbIgnoreProtection, piPenetration)
    
    --Special: The values -100, -100 means that the target was immune to all the damage types provided.
    if(iLoDamage == -100 and iHiDamage == -100) then
        gbStandardImmune = true
        return
    end
    
    --Roll three numbers and divide by the max. This gives a bell-curve rather than a flat roll.
    local iRollA = LM_GetRandomNumber(0, 100)
    local iRollB = LM_GetRandomNumber(0, 100)
    local iRollC = LM_GetRandomNumber(0, 100)
    local fFinalPercent = (iRollA + iRollB + iRollC) / 300.0
    
    --Compute the final damage.
    local iFinalDamage = math.floor(iLoDamage + ((iHiDamage - iLoDamage) * fFinalPercent))
    if(iFinalDamage < 1) then iFinalDamage = 1 end
    
    --Populate globals.
    giStandardDamage = iFinalDamage
    giStandardAttackType = gciDamageType_Slashing
end
