-- |[ ==================================== Compute Hit Rate ==================================== ]|
--Computes and returns the hit rate for a given attacker and defender. The value returned is a percent
-- indicator from 0 to 100.
--This is independent of the ability being used. If an ability does something exotic, like rolling 
-- twice, that needs to be handled in the calling script.
fnComputeHitRate = function(piAccBonus, piAccMalus, piBaseMiss)
    
    --Argument check.
    if(piAccBonus == nil) then return -1 end
    if(piAccMalus == nil) then return -1 end
    if(piBaseMiss == nil) then return -1 end
    
    --Compute.
    local iHitRate = 100 + piAccBonus - piAccMalus - piBaseMiss
    
    --Clamp.
    if(iHitRate <   0) then iHitRate =   0 end
    if(iHitRate > 100) then iHitRate = 100 end
    
    --Return.
    return iHitRate
end

--Version where the IDs are passed in.
fnComputeHitRateID = function(piAttackerID, piDefenderID, piBaseMiss)
    
    --Argument check.
    if(piAttackerID == nil) then return -1 end
    if(piDefenderID == nil) then return -1 end
    if(piBaseMiss   == nil) then return -1 end
    
    --Push and query attacker.
    AdvCombat_SetProperty("Push Entity By ID", piAttackerID)
        local iAccBonus = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Accuracy)
    DL_PopActiveObject()
    
    --Push and query defender.
    AdvCombat_SetProperty("Push Entity By ID", piDefenderID)
        local iAccMalus = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Evade)
    DL_PopActiveObject()
    
    --Call other function.
    return fnComputeHitRateVal(iAccBonus, iAccMalus, piBaseMiss)
end
