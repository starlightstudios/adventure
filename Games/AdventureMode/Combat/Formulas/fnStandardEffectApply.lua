-- |[ ==================================== Standard Effect ===================================== ]|
--The 'Standard' effect application routine. Used when an effect needs to check if the entity it
-- is trying to apply to can resist the effect. Note that immunity is checked here, and immune
-- targets always return false.
--The formula is that a random number from 0 to 9 is rolled. This value is added to the strength
-- of the ability. If that value is greater than or equal to the resistance value, the check passes.
--Therefore, each point of resistance decreases application chance by 10%, floor of 0%. Each point
-- of strength increases application chance by 10%, ceiling of 100%.
--The piType value is one of gciDamageType_[x] series.
fnStandardEffectApply = function(piDefenderID, piStrength, piType, piBonus)
    
    -- |[Argument check]|
    if(piDefenderID == nil) then return false end
    if(piStrength   == nil) then return false end
    if(piType       == nil) then return false end

    -- |[Defender Statistics]|
    --Push and query defender.
    AdvCombat_SetProperty("Push Entity By ID", piDefenderID)
        local iResistance = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Resist_Start + piType)
    DL_PopActiveObject()
    
    --Multiply by 10.
    iResistance = iResistance * 10
    
    -- |[Roll]|
    local iRoll = LM_GetRandomNumber(0, 9)
    local iTotal = (piStrength + iRoll) * 10
    
    --Add on the effect bonus. Can be negative.
    iTotal = iTotal + piBonus
    
    -- |[Result]|
    if(iTotal >= iResistance) then
        return true
    end
    return false
end
