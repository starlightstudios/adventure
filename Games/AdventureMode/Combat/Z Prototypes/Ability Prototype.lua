-- |[ ========================================= Attack ========================================= ]|
-- |[Description]|
--This prototype gives a sample version of a basic ability. In this case, it strikes with the
-- equipped weapon, same as attack. For DoT/Status Effects, see the other prototypes.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzUniqueNameHere == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "ClassName"
    zAbiStruct.sSkillName    = "AbilityName"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Mei|Trip"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\n+25%% [Atk] All Allies / 3 [Turns].\n\n5 [Turns] cooldown.\n[Costs]"
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Simple description.\n\n\n\n\nCosts [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false
    zAbiStruct.iRequiredMP = 0
    zAbiStruct.iRequiredCP = 0
    zAbiStruct.bRespectsCooldown = true
    zAbiStruct.iCooldown = 0
    zAbiStruct.bIsFreeAction = false
    zAbiStruct.iRequiredFreeActions = 0
    zAbiStruct.bRespectActionCap = false

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies Single"

    --Effects
    zAbiStruct.iCPGain = 1
    
    --Tags.
    zAbiStruct.zaTags = {}
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Primary Package]|
    --Package used to display predictions about the ability based on enemy properties.
    --All the modules are optional, but at least one should exist to display *something*.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Hit-Rate Module
    zAbiStruct.zPredictionPackage.bHasHitRateModule = true
    zAbiStruct.zPredictionPackage.zHitRateModule.iMissRate = 5
    
    --Damage Module
    zAbiStruct.zPredictionPackage.bHasDamageModule = true
    zAbiStruct.zPredictionPackage.zDamageModule.bUseWeaponType = false
    zAbiStruct.zPredictionPackage.zDamageModule.iDamageType = gciDamageType_Striking
    zAbiStruct.zPredictionPackage.zDamageModule.fDamageFactor = 1.20
    zAbiStruct.zPredictionPackage.zDamageModule.iScatterRange = 15
    
    --Stun Module
    zAbiStruct.zPredictionPackage.bHasStunModule = true
    zAbiStruct.zPredictionPackage.zStunModule.iStunBase = 0
    
    --Healing Module
    zAbiStruct.zPredictionPackage.bHasHealingModule = true
    zAbiStruct.zPredictionPackage.zHealingModule.iHealingFixed = 0
    zAbiStruct.zPredictionPackage.zHealingModule.fHealingPercent = 0.0
    zAbiStruct.zPredictionPackage.zHealingModule.fHealingFactor = 0.0

    --Shield Module
    zPackage.bHasShieldModule = false
    zPackage.zShieldModule = {}
    zPackage.zShieldModule.iShieldFinal = 0 --Internal
    zPackage.zShieldModule.iShieldFixed = 0
    zPackage.zShieldModule.iShieldFactor = 0.0
    
    --Self-Healing Module
    zPackage.bHasSelfHealingModule = false
    zPackage.zSelfHealingModule = {}
    zPackage.zSelfHealingModule.iHealingFinal   = 0   --Internal
    zPackage.zSelfHealingModule.iHealingFixed   = 0   --Constant healing
    zPackage.zSelfHealingModule.fHealingPercent = 0.0 --Percent of target's HP
    zPackage.zSelfHealingModule.fHealingFactor  = 0.0 --Percent of user's attack power
    
    --MP Generation Module
    zPackage.bHasMPGenerationModule = false
    zPackage.zMPGenerationModule = {}
    zPackage.zMPGenerationModule.iMPGeneration = 0
    
    --Lifesteal Module
    zAbiStruct.zPredictionPackage.bHasLifestealModule = true
    zAbiStruct.zPredictionPackage.zLifestealModule.fStealPercent = 0.0
    
    --Crit Module
    zAbiStruct.zPredictionPackage.bHasCritModule = true
    zAbiStruct.zPredictionPackage.zCritModule.iCritThreshold = 100
    zAbiStruct.zPredictionPackage.zCritModule.fCritBonus     = 1.25
    zAbiStruct.zPredictionPackage.zCritModule.iCritStun      = 25
    
    --Chaster Modules
    zPackage.bHasChaserModules = false
    zPackage.zaChaserModules = {}
    zPackage.iChaserModulesTotal = 0
    zPackage.zaChaserModules[1] = {}
    zPackage.zaChaserModules[1].bConsumeEffect = true                   --If true, DoT expires when attack lands
    zPackage.zaChaserModules[1].sDoTTag = "Slashing DoT"                --What tag to look for on effects.
    zPackage.zaChaserModules[1].fDamageFactorChangePerDoT = 1.0         --Added to the damage module's factor for each effect present.
    zPackage.zaChaserModules[1].fDamageRemainingFactor = 1.0            --Percentage of remaining DoT damage to be added.
    zPackage.zaChaserModules[1].sConsumeString = "Consumes [IMG0] DoTs" --Description to appear IF and ONLY IF the chaser consumes DoTs
    zPackage.zaChaserModules[1].iConsumeImgCount = 1
    zPackage.zaChaserModules[1].saConsumeImgPath = {}
    zPackage.zaChaserModules[1].saConsumeImgPath[1] = "Root/Images/AdventureUI/DamageTypeIcons/Bleeding"
    
    --Create an Effect module
    zAbiStruct.zPredictionPackage.zaEffectModules[1] = fnCreateEffectModule()
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iEffectType = gciDamageType_Striking
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iEffectStrength = 6
    zAbiStruct.zPredictionPackage.zaEffectModules[1].sResultScript = "-30 [IMG1], Slow / 3[IMG2]"
    zAbiStruct.zPredictionPackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/StatisticIcons/Evade", "Root/Images/AdventureUI/DebuffIcons/Clock"}
    
    --Tags are of the format {{"TagA", iTagCount}, {"TagB", iTagCount}}
    zPackage.zaTags = {}
    
    --Additional lines. These are plain text/symbols that do not have special properties.
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "Triggers Spearguard"
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
    
    -- |[Self-Prediction]|
    --If an ability has an additional effect on the user, use this package. It uses all the above fields.
    zAbiStruct.zSelfPredictionPack = fnCreatePredictionPack()
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Basic Execution]|
    --Basic package.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Strike")
    
    --Flags.
    zAbiStruct.zExecutionAbiPackage.bDoesNoDamage = false                         --Does not compute damage.
    zAbiStruct.zExecutionAbiPackage.bMandateDamage = false                        --Uses a script-resolved damage value and bypasses all resistence checks.
    zAbiStruct.zExecutionAbiPackage.bNeverGlances = false                         --Will hit or miss only, no glances.
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun = false                           --Does not inflict stun, even if stun is set or on a crit.
    zAbiStruct.zExecutionAbiPackage.bDoesNotAnimate = false                       --Does not show combat animation when firing.
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits = false                           --Always hits regardless of accuracy roll.
    zAbiStruct.zExecutionAbiPackage.bAlwaysCrits = false                          --Always crits regardless of accuracy roll.
    zAbiStruct.zExecutionAbiPackage.bNeverCrits = false                           --Does not crit regardless of accuracy roll.
    zAbiStruct.zExecutionAbiPackage.bEffectAlwaysApplies = false                  --Effect bypasses roll and always applies. Used for buffs.
    zAbiStruct.zExecutionAbiPackage.bTargetCanBeKOd = false                       --Allows target to be KOd, otherwise does nothing
    
    --Miss/Crit Threshold.
    zAbiStruct.zExecutionAbiPackage.iMissThreshold = 0                            --Accuracy roll failed to miss
    zAbiStruct.zExecutionAbiPackage.iCritThreshold = 100                          --Accuracy roll needed to crit
    
    --Damage flags.
    zAbiStruct.zExecutionAbiPackage.bUseWeapon = false                            --If true, uses the attacker's weapon to select damage type.
    zAbiStruct.zExecutionAbiPackage.bDealsOneDamageMinimum = true                 --If true, and damage computes to 0, deals 1 damage.
    zAbiStruct.zExecutionAbiPackage.iDamageType = gciDamageType_Slashing          --Damage type.
    zAbiStruct.zExecutionAbiPackage.fDamageFactor = 1.00                          --Factor multiplied by power to deal damage.
    zAbiStruct.zExecutionAbiPackage.bBypassProtection = false                     --Set to true to ignore protection altogether.
    zAbiStruct.zExecutionAbiPackage.iPenetration = 0                              --Reduces protection by this value, cannot go below zero.
    zAbiStruct.zExecutionAbiPackage.fCriticalFactor = 1.25                        --Multiplies by damage factor on a crit.
    zAbiStruct.zExecutionAbiPackage.iCriticalStunDamage = 25                      --How much stun is inflicted on a crit.
    zAbiStruct.zExecutionAbiPackage.iScatterRange = 15                            --Percentage variance in both directions to final damage.
    zAbiStruct.zExecutionAbiPackage.bDamageBenefitsFromItemPower = false          --Adds item power tags to damage factor.
    
    --Only used if bMandateDamage is true
    zAbiStruct.zExecutionAbiPackage.iMandatedDamage = 0                           --Amount of damage to deal, if bMandateDamage is true. Can be zero!
    
    --Factor, percent, base.
    zAbiStruct.zExecutionAbiPackage.iHealingFinal   = 0                           --Internal
    zAbiStruct.zExecutionAbiPackage.iHealingBase    = 0                           --Fixed healing value
    zAbiStruct.zExecutionAbiPackage.fHealingFactor  = 0.0                         --Multiplied by attack power
    zAbiStruct.zExecutionAbiPackage.fHealingPercent = 0.0                         --Percent of max HP
    zAbiStruct.zExecutionAbiPackage.bHealingBenefitsFromItemPower = false         --Adds item power tags to healing factor.
    
    --Heals the user, not the target. If target and user are the same, adds them.
    zAbiStruct.zExecutionAbiPackage.iSelfHealingFinal = 0                         --Internal
    zAbiStruct.zExecutionAbiPackage.iSelfHealingBase = 0                          --Fixed self-healing value
    zAbiStruct.zExecutionAbiPackage.fSelfHealingPercent = 0.0                     --Percent of user's max health
    zAbiStruct.zExecutionAbiPackage.fSelfHealingFactor = 0.0                      --Percent of user's attack power
    
    --By default, overwrites other shields. Note that shields are not always effects.
    zAbiStruct.zExecutionAbiPackage.iShieldsFinal  = 0                            --Internal
    zAbiStruct.zExecutionAbiPackage.iShieldsBase   = 0                            --Fixed shield value
    zAbiStruct.zExecutionAbiPackage.fShieldsFactor = 0.0                          --Multiplied by attack power

    --MP restore.
    zAbiStruct.zExecutionAbiPackage.iMPRestore  = 0                               --MP added to target's pool.

    --Adds to user's MP pool.
    zAbiStruct.zExecutionAbiPackage.iMPGeneration = 0                             --MP added to user's pool when ability is used.
    
    --Adds to self-healing if both are present. Attack must deal damage.
    zAbiStruct.zExecutionAbiPackage.fLifestealFactor = 0.0                        --Percentage of damage dealt healed to caller.
    
    --How much stun is inflicted.
    zAbiStruct.zExecutionAbiPackage.iBaseStunDamage = 0                           --Amount of stun damage inflicted on a hit/glance/crit.
    
    --String indicating job change on use.
    zAbiStruct.zExecutionAbiPackage.sChangeJobTo = "Null"                         --If not "Null", changes the user's job.
    
    --Variable number of chaser modules that activate when certain DoT tags present.
    zAbiStruct.zExecutionAbiPackage.bHasChaserModules = false
    zAbiStruct.zExecutionAbiPackage.zaChaserModules = {}
    zAbiStruct.zExecutionAbiPackage.iChaserModulesTotal = 0
    zAbiStruct.zExecutionAbiPackage.zaChaserModules[1] = {}
    zAbiStruct.zExecutionAbiPackage.zaChaserModules[1].bConsumeEffect = true           --If true, DoT expires when attack lands
    zAbiStruct.zExecutionAbiPackage.zaChaserModules[1].sDoTTag = "Slashing DoT"        --What tag to look for on effects.
    zAbiStruct.zExecutionAbiPackage.zaChaserModules[1].fDamageFactorChangePerDoT = 1.0 --Added to the damage module's factor for each effect present.
    zAbiStruct.zExecutionAbiPackage.zaChaserModules[1].fDamageRemainingFactor = 1.0    --Percentage of remaining DoT damage to be added.
    
    --Additional Animations.
    fnAddAnimationPackage(zAbiStruct.zExecutionAbiPackage, "Bleed", 0)
    
    -- |[Execution Effect Package]|
    --Optional
    --This applies the given effect to the target(s), if it hits and they fail the resist check.  
    zAbiStruct.zExecutionEffPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides.
    zAbiStruct.zExecutionEffPackage.iEffectStr      = 6
    zAbiStruct.zExecutionEffPackage.iEffectCritStr  = 9
    zAbiStruct.zExecutionEffPackage.iEffectType     = gciDamageType_Striking
    zAbiStruct.zExecutionEffPackage.sEffectPath     = fnResolvePath() .. "../Effects/Trip.lua"
    zAbiStruct.zExecutionEffPackage.sEffectCritPath = fnResolvePath() .. "../Effects/Trip Crit.lua"
    zAbiStruct.zExecutionEffPackage.sApplyText      = "Tripped!"
    zAbiStruct.zExecutionEffPackage.sApplyTextCol   = "Color:Purple"
    zAbiStruct.zExecutionEffPackage.sResistText     = "Resisted!"
    zAbiStruct.zExecutionEffPackage.sResistTextCol  = "Color:White"
    
    -- |[Execution Effect Package B]|
    --Optional
    --Same as above.
    zAbiStruct.zExecutionEffPackageB = fnConstructDefaultEffectPackage()
    
    --Apply overrides.
    zAbiStruct.zExecutionEffPackageB.iEffectStr      = 6
    zAbiStruct.zExecutionEffPackageB.iEffectCritStr  = 9
    zAbiStruct.zExecutionEffPackageB.iEffectType     = gciDamageType_Striking
    zAbiStruct.zExecutionEffPackageB.sEffectPath     = fnResolvePath() .. "../Effects/Trip.lua"
    zAbiStruct.zExecutionEffPackageB.sEffectCritPath = fnResolvePath() .. "../Effects/Trip Crit.lua"
    zAbiStruct.zExecutionEffPackageB.sApplyText      = "Tripped!"
    zAbiStruct.zExecutionEffPackageB.sApplyTextCol   = "Color:Purple"
    zAbiStruct.zExecutionEffPackageB.sResistText     = "Resisted!"
    zAbiStruct.zExecutionEffPackageB.sResistTextCol  = "Color:White"
    
    -- |[Execution Ally Effect Package]|
    --Optional
    --Applies the given effect to allies of the user.
    zAbiStruct.zAllyEffectPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides.
    zAbiStruct.zAllyEffectPackage.sEffectPath     = fnResolvePath() .. "../Effects/Inspire.lua"
    zAbiStruct.zAllyEffectPackage.sEffectCritPath = fnResolvePath() .. "../Effects/Inspire.lua"
    zAbiStruct.zAllyEffectPackage.sApplyText = "Inspired!"
    zAbiStruct.zAllyEffectPackage.sApplyTextCol = "Color:White"
    zAbiStruct.zAllyEffectPackage.sApplyAnimation = "Null"
    zAbiStruct.zAllyEffectPackage.sApplySound = "Combat\\|Impact_Buff"
    
    -- |[Execution Self-Effect Package]|
    --Optional
    --This applies the effect to the user of the ability.
    zAbiStruct.zExecutionSelfPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides.
    zAbiStruct.zExecutionSelfPackage.sEffectPath     = fnResolvePath() .. "../../Common/Effects/Spearguard.lua"
    zAbiStruct.zExecutionSelfPackage.sEffectCritPath = fnResolvePath() .. "../../Common/Effects/Spearguard.lua"
    zAbiStruct.zExecutionSelfPackage.sApplyText = "Spearguard"
    zAbiStruct.zExecutionSelfPackage.sApplyTextCol = "Color:White"
    zAbiStruct.zExecutionSelfPackage.sApplyAnimation = "Null"
    zAbiStruct.zExecutionSelfPackage.sApplySound = "Combat\\|Impact_Buff"
    
    -- |[Use of Effect Prototype]|
    zAbiStruct.zExecutionEffPackage.sPrototypeName = "Character.Class.EffectName"
    
    --Switch the effect path to:
    zAbiStruct.zExecutionEffPackage.sEffectPath     = gsGlobalEffectPrototypePath
    zAbiStruct.zExecutionEffPackage.sEffectCritPath = gsGlobalEffectPrototypePath

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    if(fnEffectPrototypeExists(zAbiStruct.zExecutionEffPackage.sPrototypeName) == false) then

        -- |[Creation]|
        --This effect uses a standardized StatMod script.
        local sDisplayName = "Concentrate"
        local iDuration = 1
        local bIsBuff = true
        local sIcon = "Root/Images/AdventureUI/Abilities/Mei|Concentrate"
        local sStatString = "Acc|Flat|30"

        --Description can be a variable number of lines.
        local saDescription = {}
        saDescription[1] = "Focus carefully on your target, then strike!"
        saDescription[2] = "Increases [Acc](Accuracy) for your next attack."

        --Run the standardized builder.
        local zPrototype      = fnCreateStatmodEffectPrototype(sDisplayName, iDuration, bIsBuff, sIcon, sStatString)
        zPrototype.saStrings  = fnStatmodBuildInspectorStrings(zPrototype, saDescription)
        zPrototype.sShortText = fnStatmodBuildShortText(zPrototype)

        -- |[Overrides]|
        --None Yet.
    
        -- |[Register]|
        fnRegisterEffectPrototype("Character.Class.EffectName", gciEffect_Is_StatMod, zPrototype)
    
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzUniqueNameHere = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzUniqueNameHere

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

--If you want to do manual overrides, you can use the sections below. Otherwise, the above call will
-- finish execution. Delete the rest of this if your ability does nothing "Special".

-- |[ ==================================== Creation Handler ==================================== ]|
--Called when the ability is created and added to the job prototype. This shows the ability's name
-- and icon, to inform the player what abilities are on the job.
if(iSwitchType == gciAbility_Create or iSwitchType == gciAbility_CreateSpecial) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

-- |[ ======================================= Job Taken ======================================== ]|
--Called when the character in question activates the job that contains this ability. The ability is
-- stored in the AdvCombatEntity.
elseif(iSwitchType == gciAbility_AssumeJob) then

-- |[ ================================= Combat: Combat Begins ================================== ]|
--Called when combat begins, to set up any local variables. Note that the Turn Begins code will be
-- called next, *after* turn order is resolved.
elseif(iSwitchType == gciAbility_BeginCombat) then

-- |[ ================================== Combat: Turn Begins =================================== ]|
--Called when the turn begins after turn order is resolved.
elseif(iSwitchType == gciAbility_BeginTurn) then

-- |[ ============================ Combat: Character Action Begins ============================= ]|
--Called when the character that possesses this ability begins their turn. This is where the ability
-- should enable or disable itself based on the character's MP/State/etc.
elseif(iSwitchType == gciAbility_BeginAction or iSwitchType == gciAbility_BeginFreeAction) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

-- |[ ============================= Combat: Character Action Ends ============================== ]|
--Called after a character executes any action. Used for passives or post-reaction abilities. Is not
-- called if the action was a free action (that's above).
elseif(iSwitchType == gciAbility_PostAction) then

-- |[ ================================= Combat: Paint Targets ================================== ]|
--For player-controlled characters, selects which targets can be used by this ability. AI-controlled
-- characters may optionally bypass this and select targets directly.
--If no targets are painted, the UI will play a fail sound and disable target selection.
elseif(iSwitchType == gciAbility_PaintTargets) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
    
-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--When the player is selecting targets, a little box appears above the targets. This is the prediction
-- box. This script is called once for each target cluster created when this ability is selected, and
-- prediction boxes can be made for entities in that target cluster.
--The AdvCombatAbility will be the active object.
elseif(iSwitchType == gciAbility_BuildPredictionBox) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

-- |[ ================================== Combat: Can Execute =================================== ]|
--Flags whether or not the event can actually run. If the originator got KOd by a DoT or a counterattack
-- or any other event, this will mark as false, and the event will stop here.
--This is only called for the ability in question.
elseif(iSwitchType == gciAbility_QueryCanRun) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
    
    -- |[Useful code]|
    --Get the target ID using the target index.
    --AdvCombat_SetProperty("Push Target", piTargetIndex)
    --    local iTargetID = RO_GetID()
    --DL_PopActiveObject()
    
-- |[ =================================== Combat: Turn Ends ==================================== ]|
--Called after the turn ends, before turn order for the next turn is resolved. The ability does not
-- need to have been used for this to be called.
elseif(iSwitchType == gciAbility_TurnEnds) then

-- |[ ================================== Combat: Combat Ends =================================== ]|
--Called after combat ends. The passed in flag dictates if Victory/Defeat occurred. This occurs
-- before Doctor Bag/XP/JP etc is awarded.
elseif(iSwitchType == gciAbility_CombatEnds) then

-- |[ ================================== Combat: Event Queued ================================== ]|
--Fired whenever a "main" event is queued, which is when the player or an AI decides which action to
-- perform. All abilities equipped by all entities then fire a response script. They can then enqueue
-- their own events before or after the main one.
--This occurs before the ability calls with gciAbility_Execute.
elseif(iSwitchType == gciAbility_EventQueued) then
    
-- |[ ==================================== Standard Handler ==================================== ]|
--Call standard handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
