-- |[ ================================== AIString Diagnostics ================================== ]|
--Diagnostics functions used by AIStrings. These do nothing if the matching diagnostics flag 
-- "AI Strings" is false.

-- |[ ============================== AIString:fnRunCombatStart() =============================== ]|
--Should be run once at combat start, resets global lists. This allows the variables declared by
-- AIStrings to be tracked for diagnostics.
function AIString:fnRunCombatStart()
    gzAIGlobalVariableList = {}
end

-- |[ ========================== AIString:fnRegisterGlobalVariable() =========================== ]|
--Used for diagnostics, tracks global variable creation.
function AIString:fnRegisterGlobalVariable(psPath, psType)
    
    -- |[Argument Check]|
    if(psPath == nil) then return end
    if(psType == nil) then return end

    -- |[Duplicate Check]|
    for i = 1, #gzAIGlobalVariableList, 1 do
        if(gzAIGlobalVariableList[i].sPath == psPath) then
            return
        end
    end

    -- |[Create, Register]|
    local zNewVariable = {}
    zNewVariable.sPath = psPath
    zNewVariable.sType = psType
    table.insert(gzAIGlobalVariableList, zNewVariable)

end

-- |[ =================================== AIString:fnPrint() =================================== ]|
--Prints the received string. This is meant to be simple and used for explicit diagnostics. It does
-- not have the full functionality of io.write.
function AIString:fnPrint(psString)
    if(psString == nil) then return end
    io.write(psString .. "\n")
end

-- |[ ================================= AIString:fnPrintTerm() ================================= ]|
--Prints a received term, which can be anything AIString:fnEvaluateTerm() can evaluate.
function AIString:fnPrintTerm(psTerm)
    if(psTerm == nil) then return end
    local zResult = self:fnEvaluateTerm(psTerm)
    io.write("Evaluating: " .. psTerm .. " - Result is: " .. zResult .. "\n")
end

-- |[ ============================ AIString:fnListGlobalVariables() ============================ ]|
--Used for diagnostics, prints global variable states.
function AIString:fnListGlobalVariables()
    
    --Setup.
    io.write("Writing global variable information.\n")
    
    --Iterate.
    for i = 1, #gzAIGlobalVariableList, 1 do
        local zValue = VM_GetVar(gzAIGlobalVariableList[i].sPath, gzAIGlobalVariableList[i].sType)
        
        io.write(" " .. gzAIGlobalVariableList[i].sPath .. " " .. gzAIGlobalVariableList[i].sType .. ": " .. zValue .. "\n")
    end
    
end

-- |[ ============================ AIString:fnListLocalVariables() ============================= ]|
--Used for diagnostics, prints all locally tracked variable states.
function AIString:fnListLocalVariables()
    
    --Header.
    io.write("AIString " .. self.sName .. " prints local variables.\n")
    io.write("There are currently " .. #self.zaTrackedVariables .. " variables being tracked.\n")
    
    --Iterate.
    for i = 1, #self.zaTrackedVariables, 1 do
        
        --Numeric:
        if(self.zaTrackedVariables[i].sType == "N") then
            local fValue = VM_GetVar("Root/Variables/Combat/" .. RO_GetID() .. "_AI/" .. self.zaTrackedVariables[i].sVarName, "N")
            io.write(" " .. self.zaTrackedVariables[i].sVarName .. ", Numeric, " .. fValue .. "\n")
        
        --String:
        else
            local sValue = VM_GetVar("Root/Variables/Combat/" .. RO_GetID() .. "_AI/" .. self.zaTrackedVariables[i].sVarName, "S")
            io.write(" " .. self.zaTrackedVariables[i].sVarName .. ", Alpha, " .. sValue .. "\n")
        end
    end
end

-- |[ ============================= AIString:fnPrintGraceWarning() ============================= ]|
--Because "Pass Turn" is an ambiguous use case, this function will warn the user if an enemy is passing
-- their turn because their AIString is broken, as opposed to passing their turn because they were
-- ordered not to act.
function AIString:fnPrintGraceWarning()
    
    --If the flag was set, do nothing.
    if(gbAIStringFinishedGracefully == true) then return end
    
    --If this flag is set, don't print anything. AIs can legally not perform an action.
    if(self.bCanBePartialAI == true) then return end
    
    --Print.
    io.write("Warning: AI used an AIString but failed to resolve gracefully.\n")
    io.write(" AIString Name: " .. self.sName .. "\n")
    io.write(" AIString String: " .. self.sString .. "\n")
end

-- |[ ============================ AIString:fnAddTrackingVariable() ============================ ]|
--Adds the given variable to the local tracking list, which can be printed with fnListLocalVariables().
-- This is only used for diagnostics.
function AIString:fnAddTrackingVariable(psVarName, psType)
    
    -- |[Argument Check]|
    if(psVarName == nil) then return end
    if(psType    == nil) then return end
    
    -- |[Duplicate Check]|
    --Scan, see if the variable is already being tracked.
    for i = 1, #self.zaTrackedVariables, 1 do
        if(self.zaTrackedVariables[i].sVarName == psVarName) then
            return
        end
    end
    
    -- |[Add]|
    --If we got this far, the variable is not being tracked. Add it.
    local zEntry = {}
    zEntry.sVarName = psVarName
    zEntry.sType = psType
    table.insert(self.zaTrackedVariables, zEntry)
end

-- |[ ============================= AIString:fnCheckContinueCase() ============================= ]|
--Scans the AI string line-by-line, looking for {Prime} calls that don't have a {Continue} call
-- on the same line. Barks a warning if such a case is found.
function AIString:fnCheckContinueCase()
    
    -- |[Trimming]|
    --If whitespace has not been trimmed yet, do that now.
    if(self.bTrimmed == false) then
        self:fnRemoveWhitespace()
    end

    -- |[Line Iteration]|
    --Split up the string into individual lines and execute them one at a time.
    local saStrings = fnSubdivideStrict(self.sString, "||")
    
    --Iterate.
    for i = 1, #saStrings, 1 do
        
        --Setup.
        local bHasPrime = false
        local bHasFinish = false
        local bHasContinue = false
        
        --Iterate across the chunks:
        local sChunk, iNextPos = self:fnGetNextChunk(saStrings[i], 1)
        while(sChunk ~= nil) do
            
            --If the chunk starts with "{Prime", flip this flag.
            if(string.sub(sChunk, 1, 6) == "{Prime") then
                bHasPrime = true
            end
            
            --If the chunk is a "{Finish}", flip this flag.
            if(sChunk == "{Finish}") then
                bHasFinish = true
            end
            
            --If the chunk is a "{Continue}", flip this flag.
            if(sChunk == "{Continue}") then
                bHasContinue = true
            end
            
            --Next.
            sChunk, iNextPos = self:fnGetNextChunk(saStrings[i], iNextPos)
        end
        
        --After iteration, if there is a prime but no finish or continue, print a warning.
        if(bHasPrime == true and bHasFinish == false and bHasContinue == false) then
            io.write("AIString:fnCheckContinueCase() - Warning. Found a problem in " .. self.sName .. " on line " .. i .. "\n")
        end
    end
end



















