-- |[ ================================== AIString Class File =================================== ]|
--Class file for AIString. Call this once at initialization.

--An AIString is a string that an enemy AI can use to resolve its action on a given turn. This
-- is nominally just a string, but it comes bundled with some diagnostic functions and execution
-- functions to make it easier to use.
--By default these are registered to a global list gzAIStringList that an enemy AI can then check
-- to find its associated string (typically with a Tag applied to the enemy) and then execute.

-- |[ =========== Globals/Statics ============ ]|
--Unordered list of AIStrings.
gbHasBuiltAIStringLookups = nil
gzAIStringList = {}

--Flag to indicate if execution finished gracefully. AIs should report if this flag is not tripped
-- in order to make sure a "Pass Turn" is deliberate.
gbAIStringFinishedGracefully = false

--Flag where an execution chunk, in curly braces {}, can act like a conditional and fail. The flag
-- is false by default, when true an execution package acts like a FALSE condition.
gbAIStringExecutionBreaksList = false

--List of all globally declared variables. Should get reset at combat start. Only used for diagnostics.
gzAIGlobalVariableList = {}

-- |[ ============ Class Members ============= ]|
--Meta-information.
AIString = {}
AIString.__index = AIString

--Meta.
AIString.bCanBePartialAI = false                --If true, the AIString will not print a warning if it doesn't do anything. This allows an AI script to use multiple strings.
AIString.bHandledAbility = false                --Flag that can be read to check if the AIString handled an action during its last run.

--Primitives.
AIString.bTrimmed = false
AIString.sName = "Null"
AIString.sString = "Null"
AIString.bIsProhibited = false                  --When toggled on by {Prohibit}, Target and Prime routines will silently fail. This allows AIStrings to have common endings.
AIString.bHasPrimed = false                     --Tracks if the string has already run a prime routine. If true, print a warning when running further prime routines. Use {continue} to clear.
AIString.bHasTargeted = false                   --Tracks if the string has already run a target routine. If true, prints a warning when running further target routines. Use {continue} to clear.

--Tracked Variables.
AIString.zaTrackedVariables = {}                --List of {sVarName, sType} entries, used to track variables as the string executes. Can be printed with fnListLocalVariables().

--Lists
AIString.iaList = {}

-- |[ ============ Class Methods ============= ]|
--System
function AIString:new(psName, psString, pbDoNotRegister) end

--Conditions
function AIString:fnHandleCondition(psCondition) end
function AIString:fnResolveValue(psString)       end

--Diagnostics
function AIString:fnRunCombatStart()                       end
function AIString:fnRegisterGlobalVariable(psPath, psType) end
function AIString:fnPrint(psString)                        end
function AIString:fnPrintTerm(psTerm)                      end
function AIString:fnPrintGraceWarning()                    end
function AIString:fnAddTrackingVariable(psVarName, psType) end

--Execution
function AIString:fnExecute()                  end
function AIString:fnHandleString(psString)     end
function AIString:fnHandlePrime(psAbilityName) end

--Parsing
function AIString:fnRemoveWhitespace() end

--Terms
function AIString:fnEvaluateTerm(psTerm)                end
function AIString:fnTermSubtypes(psTerm, pbIsFirstCall) end

--Targeting
function AIString:fnHandleTargetResolve(psaStrings) end
function AIString:fnHandleListCall(psaStrings)      end

--Utility
function AIString:fnGetStatIndexFromString(psStatName) end

-- |[ ========== Class Constructor =========== ]|
function AIString:new(psName, psString, pbDoNotRegister)
    
    -- |[Argument Check]|
    if(psName == nil) then return end
    
    -- |[Create]|
    local zObject = {}
    setmetatable(zObject, AIString)
    
    -- |[Initialize]|
    zObject.bTrimmed = false
    zObject.sName = psName
    zObject.sString = "Null"
    
    -- |[Optional Arguments]|
    --If a string is provided, store that.
    if(psString ~= nil) then
        zObject.sString = psString
    end
    
    -- |[Register]|
    --If the flag pbDoNotRegister is true, skip this step. Otherwise, auto-register to the global.
    if(pbDoNotRegister ~= true) then
        table.insert(gzAIStringList, zObject)
    end
    
    -- |[Return]|
    return zObject
end

-- |[ ========= Call Implementations ========= ]|
LM_ExecuteScript(fnResolvePath() .. "AIStringConditions.lua")
LM_ExecuteScript(fnResolvePath() .. "AIStringDiagnostics.lua")
LM_ExecuteScript(fnResolvePath() .. "AIStringExecute.lua")
LM_ExecuteScript(fnResolvePath() .. "AIStringParsing.lua")
LM_ExecuteScript(fnResolvePath() .. "AIStringTargeting.lua")
LM_ExecuteScript(fnResolvePath() .. "AIStringTerms.lua")
LM_ExecuteScript(fnResolvePath() .. "AIStringUtility.lua")
