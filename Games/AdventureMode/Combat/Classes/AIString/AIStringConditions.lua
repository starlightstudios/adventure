-- |[ =================================== AIString Conditions ================================== ]|
--Condition handler for the AIString class. Conditions frequently need to resolve variables so the
-- script gets ungainly. Hence it gets its own file.
--Note: The acting entity (AdvCombatEntity *) should be atop the activity stack.

-- |[ ============================== AIString:fnHandleCondition() ============================== ]|
--Given a condition string of the format VAR:COMPARE:VAR, returns true if the condition evaluates
-- to true, and false on error of it it evaluates to false.
function AIString:fnHandleCondition(psCondition)
    
    -- |[Argument Check]|
    if(psCondition == nil) then return false end
    
    --Diagnostics.
    local bDiagnostics = Debug_GetFlag("AIString: All") or Debug_GetFlag("AIString: Conditions")
    Debug_PushPrint(bDiagnostics, "[Handling Condition]\n")

    -- |[Special Cases]|
    --If the condition is just TRUE, no subdivision occurs and true is returned. Used for debug.
    if(psCondition == "TRUE") then
        Debug_PopPrint("Condition is TRUE and immediately succeeds.\n")
        return true
    end
    
    --If the condition is just FALSE, no subdivision occurs and false is returned. Used for debug.
    if(psCondition == "FALSE") then
        Debug_PopPrint("Condition is FALSE and immediately fails.\n")
        return false
    end

    -- |[Subdivide]|
    --Break the string into parts. There should be a multiple of 3 here, where each set of three
    -- is considered a logical OR to the others. If any condition is true, we succeed.
    local saStrings = fnSubdivide(psCondition, ":")
    if(#saStrings % 3 ~= 0) then
        io.write("AIString:fnHandleCondition() - Warning: A multiple of 3 pieces to a condition is required.\n")
        io.write(" Condition: " .. psCondition .. "\n")
        Debug_PopPrint("")
        return false
    end
    
    --Diagnostics.
    Debug_Print("There are " .. #saStrings .. " pieces to the provided condition.\n")

    -- |[Iteration]|
    --Iterate across the logical checks.
    for i = 1, #saStrings, 3 do

        --Fast-access values.
        local sValueA     = saStrings[i+0]
        local sComparison = saStrings[i+1]
        local sValueB     = saStrings[i+2]

        --Debug.
        Debug_Print(" Set: " .. i .. " is " .. sValueA .. " " .. sComparison .. " " .. sValueB .. "\n")

        --These values can be functions, constants, or queried variables. Note that the final type is 'z', indicating
        -- that the script doesn't actually know what type it is. It could be a string, a number, or a monthly curated
        -- box of snacks. Lua can handle comparing different types!
        local zFinalValueA = self:fnEvaluateTerm(sValueA)
        local zFinalValueB = self:fnEvaluateTerm(sValueB)
        
        --Debug.
        Debug_Print(" Finals: " .. i .. " is " .. zFinalValueA .. " " .. sComparison .. " " .. zFinalValueB .. "\n")

        --Run comparison.
        local bResult = false
        if(sComparison == "==") then
            bResult = (zFinalValueA == zFinalValueB)
        
        --Greater than or equal to.
        elseif(sComparison == ">=") then
            bResult = (zFinalValueA >= zFinalValueB)
        
        --Less than or equal to.
        elseif(sComparison == "<=") then
            bResult = (zFinalValueA <= zFinalValueB)
        
        --Greater than.
        elseif(sComparison == ">") then
            bResult = (zFinalValueA > zFinalValueB)
        
        --Less than.
        elseif(sComparison == "<") then
            bResult = (zFinalValueA < zFinalValueB)
        
        --Not equal to. Both Lua and C++ standard is accepted.
        elseif(sComparison == "!=" or sComparison == "~=") then
            bResult = (zFinalValueA ~= zFinalValueB)
        
        --Print an error.
        else
            io.write("AIString:fnHandleCondition() - Invalid comparison string used.\n")
            io.write(" Condition: " .. psCondition .. "\n")
            Debug_PopPrint("")
            return false
        end

        --If the result is true, then we're done.
        if(bResult == true) then
            Debug_PopPrint("Set resolved to true. Finishing.\n")
            return true
        end
    end

    --If we got this far, then none of the condition cases came out true.
    Debug_PopPrint("All sets resolved to false. Finishing.\n")
    return false
end
