-- |[ ==================================== AIString Parsing ==================================== ]|
--Parsing functions for the AIString, such as splitting off arguments or removing whitespace.

-- |[ ============================= AIString:fnRemoveWhitespace() ============================== ]|
--Removes unnecessary whitespace from a string.
function AIString:fnRemoveWhitespace()

    -- |[Error Check]|
    if(self.sString == nil) then return end
    
    --Diagnostics.
    local bDiagnostics = Debug_GetFlag("AIString: All") or Debug_GetFlag("AIString: Parsing")

    -- |[Setup]|
    --Final string.
    local sFinalString = ""
    
    --Interim variables.
    local iBraceStack = 0
    local iBlockStack = 0
    
    --Diagnostics.
    --Debug_PushPrint(bDiagnostics, "AIString:fnRemoveWhitespace() - Begin on: " .. self.sString .. "\n")

    -- |[Iterate]|
    local iLen = string.len(self.sString)
    for i = 1, iLen, 1 do

        --Get the letter in this slot.
        local sLetter = string.sub(self.sString, i, i)

        --Inside a brace or block, blindly append.
        if(iBraceStack > 0 or iBlockStack > 0) then
            sFinalString = sFinalString .. sLetter
            
            --Brace. Increment stack.
            if(sLetter == "{") then
                iBraceStack = iBraceStack + 1
            
            --Block. Increment stack.
            elseif(sLetter == "[") then
                iBlockStack = iBlockStack + 1
            
            --End-Brace. Decrement stack.
            elseif(sLetter == "}") then
                iBraceStack = iBraceStack - 1
            
            --End-Block. Decrement stack.
            elseif(sLetter == "]") then
                iBlockStack = iBlockStack - 1
            end
            
            --Range check.
            if(iBraceStack < 0) then
                iBraceStack = 0
                io.write("AIString:fnRemoveWhitespace(): Warning. Negative brace stack reached.\n")
            end
            if(iBlockStack < 0) then
                iBlockStack = 0
                io.write("AIString:fnRemoveWhitespace(): Warning. Negative block stack reached.\n")
            end
        
        --Not inside a stack.
        else

            --It's whitespace. Do not append.
            if(sLetter == " ") then

            --Non-whitespace. Append.
            else
                
                --Add.
                sFinalString = sFinalString .. sLetter

                --Brace. Increment stack.
                if(sLetter == "{") then
                    iBraceStack = iBraceStack + 1
                
                --Block. Increment stack.
                elseif(sLetter == "[") then
                    iBlockStack = iBlockStack + 1
                end
            end
        end
    end
    
    -- |[Finish Up]|
    self.bTrimmed = true
    self.sString = sFinalString
    --Debug_PopPrint("Finished trimming. " .. sFinalString .. "\n")
end
