-- |[ ==================================== AIString Execute ==================================== ]|
--Execution function, takes the string within this object and executes it for the AI provided. It
-- is assumed that the active object is the acting entity.

-- |[ ================================== AIString:fnExecute() ================================== ]|
--Main execution function.
function AIString:fnExecute()

    -- |[Diagnostics and Setup]|
    --Activate diagnostics.
    local bDiagnostics = Debug_GetFlag("AIString: All") or Debug_GetFlag("AIString: Run")
    Debug_PushPrint(bDiagnostics, "AIString:fnExecute() begins.\n")
    
    --Variables.
    local iOwnerID = RO_GetID()
    
    --Global flag.
    gbAIStringFinishedGracefully = false
    
    --Internal flag.
    self.bHandledAbility = false
    
    --Owner Variables.
    AdvCombat_SetProperty("Push Entity By ID", iOwnerID)
        local sOwnerName = AdvCombatEntity_GetProperty("Display Name")
    DL_PopActiveObject()
    
    -- |[Trimming]|
    --If whitespace has not been trimmed yet, do that now.
    if(self.bTrimmed == false) then
        self:fnRemoveWhitespace()
    end
    
    --Clear flags.
    self.bIsProhibited = false
    self.bHasPrimed = false
    self.bHasTargeted = false

    --Report.
    Debug_Print("Owner ID:        " .. iOwnerID .. "\n")
    Debug_Print("Owner Name:      " .. sOwnerName .. "\n")
    Debug_Print("AIString Name:   " .. self.sName .. "\n")
    Debug_Print("AIString String: " .. self.sString .. "\n")

    -- |[Line Iteration]|
    --Split up the string into individual lines and execute them one at a time.
    local saStrings = fnSubdivideStrict(self.sString, "||")
    Debug_Print("Subdivided into " .. #saStrings .. " strings.\n")
    if(bDiagnostics) then
        for i = 1, #saStrings, 1 do
            Debug_Print(" " .. saStrings[i] .. "\n")
        end
    end
    
    --Iterate.
    for i = 1, #saStrings, 1 do
        
        --Run subroutine.
        Debug_Print("Primary string execution:\n")
        local bIsFinished = self:fnHandleString(saStrings[i])
        
        --If the function came back true, stop execution.
        if(bIsFinished) then
            self.bHandledAbility = true
            bFinishedGracefully = true
            break
        end
    end

    -- |[Finish Up]|
    Debug_PopPrint("AIString:fnExecute() completes.\n\n")
    
    -- |[Grace Warning]|
    --If nothing flipped the flag gbAIStringFinishedGracefully, this function will report that
    -- to the player so they can fix the bug.
    self:fnPrintGraceWarning()
end

-- |[ =============================== AIString:fnGetNextChunk() ================================ ]|
--Given a string and a starting location, returns the next chunk, and the next starting location.
-- If there are no more chunks, returns nil, nil.
function AIString:fnGetNextChunk(psString, piStartLocation)
    
    -- |[Argument Check]|
    if(psString        == nil) then return nil, nil end
    if(piStartLocation == nil) then return nil, nil end
    
    --Check the length. If the start position is past the end of the string, stop.
    local iLen = string.len(psString)
    if(piStartLocation >= iLen) then return nil, nil end
    
    --Get the first letter. It should be a "{" or "[" if all whitespace has been cleared.
    local sFirstLetter = string.sub(psString, piStartLocation, piStartLocation)
    
    --Conditional:
    if(sFirstLetter == "[") then
            
        --Get the end. If it came back nil, the string is malformed.
        local iChunkEnd = string.find(psString, "]", piStartLocation+1)
        if(iChunkEnd == nil) then
            io.write("AIString:fnGetNextChunk() - Warning: Invalid chunk, no ending character ] found.\n")
            io.write(" String: " .. psString .. "\n")
            Debug_PopPrint("")
            return nil, nil
        end
        
        --Get and return the string.
        local sChunk = string.sub(psString, piStartLocation, iChunkEnd)
        return sChunk, iChunkEnd+1
    
    --Execution:
    elseif(sFirstLetter == "{") then
            
        --Get the end. If it came back nil, the string is malformed.
        local iChunkEnd = string.find(psString, "}", piStartLocation+1)
        if(iChunkEnd == nil) then
            io.write("AIString:fnGetNextChunk() - Warning: Invalid chunk, no ending character } found.\n")
            io.write(" String: " .. psString .. "\n")
            Debug_PopPrint("")
            return nil, nil
        end
        
        --Get and return the string.
        local sChunk = string.sub(psString, piStartLocation, iChunkEnd)
        return sChunk, iChunkEnd+1
    
    end
        
    --Malformed. If we got this far, an unhandled type was found.
    io.write("AIString:fnGetNextChunk() - Warning, malformed string. No whitespace is allowed, be sure to trim after setting.\n")
    io.write(" There must only be {} and [] blocks after whitespace is trimmed.\n")
    io.write(" Execution: " .. psString .. "\n")
    return nil, nil
end

-- |[ =============================== AIString:fnHandleString() ================================ ]|
--Given a string, checks relevant conditions and executions, front to back, and executes them or
-- stops if ordered. Returns true if the action has been handled and the AI should stop parsing,
-- false otherwise.
--It is expected that a string contains a set of Blocks [] or Braces {}. Anything else is considered
-- to be invalid and will bark a warning.
function AIString:fnHandleString(psString)
    
    -- |[Argument Check]|
    if(psString == nil) then return false end
    
    --Diagnostics.
    local bDiagnostics = Debug_GetFlag("AIString: All") or Debug_GetFlag("AIString: Handle String")
    Debug_PushPrint(bDiagnostics, "Handling string: " .. psString .. "\n")
        
    -- |[For Each Chunk]|
    --Iterate across the chunks:
    local sChunk, iNextPos = self:fnGetNextChunk(psString, 1)
    while(sChunk ~= nil) do
        
        --Create a no-edge version of the string.
        local sNoEdge = string.sub(sChunk, 2, -2)

        --Diagnostics.
        Debug_Print("Chunk: " .. sChunk .. "\n")
        Debug_Print("No-Edge: " .. sNoEdge .. "\n")

        --First letter is a "[".
        if(string.sub(sChunk, 1, 1) == "[") then
            
            --Diagnostics.
            Debug_Print(" Conditional.\n")
            
            --Handle conditional.
            local bResult = self:fnHandleCondition(sNoEdge)
            if(bResult == false) then
                Debug_PopPrint("Conditional failed.\n")
                return false
            end
            
            --Diagnostics.
            Debug_Print("Conditional succeeded. Running next chunk.\n")
        
        --First letter is a "{".
        elseif(string.sub(sChunk, 1, 1) == "{") then
            
            --Diagnostics.
            Debug_Print(" Execution.\n")
    
            --Handle execution. If it's true, a Finish tag was found and the AI can stop.
            local bResult = self:fnHandleExecution(sNoEdge)
            if(bResult == true) then
                Debug_PopPrint("Completed execution, ordered to stop parsing.\n")
                return true
            end
    
            --Otherwise, run the next chunk.
            Debug_Print("Execution succeeded. Running next chunk.\n")
        end
        
        --Next.
        sChunk, iNextPos = self:fnGetNextChunk(psString, iNextPos)
    end
    
    --[=[
    --Begin iterating.
    while(iCurrent < iLen) do
        
        --Get the current letter.
        local sCurrentLetter = string.sub(psString, iCurrent, iCurrent)
        
        --If it's a conditional:
        if(sCurrentLetter == "[") then
            
            --Get the end. If it came back nil, the string is malformed.
            local iChunkEnd = string.find(psString, "]", iCurrent)
            if(iChunkEnd == nil) then
                io.write("AIString:fnHandleString() - Warning: Invalid chunk, no ending character ] found\n")
                io.write(" String: " .. psString .. "\n")
                Debug_PopPrint("")
                return true
            end
            
            --Create a no-edge string.
            local sNoEdge = string.sub(psString, iCurrent+1, iChunkEnd-1)
    
            --Diagnostics.
            Debug_Print("Chunk: " .. string.sub(psString, iCurrent, iChunkEnd) .. " is a conditional.\n")
            Debug_Print("No-Edge: " .. sNoEdge .. "\n")
        
            --Execute subroutine. If it comes back false, execution of this string stops.
            local bResult = self:fnHandleCondition(sNoEdge)
            if(bResult == false) then
                Debug_PopPrint("Conditional failed.\n")
                return false
            end
        
            --Otherwise, run the next chunk.
            Debug_Print("Conditional succeeded. Running next chunk.\n")
            iCurrent = iChunkEnd + 1
        
        --If it's an execution:
        elseif(sCurrentLetter == "{") then
            
            --Get the end. If it came back nil, the string is malformed.
            local iChunkEnd = string.find(psString, "}", iCurrent)
            if(iChunkEnd == nil) then
                io.write("AIString:fnHandleString() - Warning: Invalid chunk, no ending character } found.\n")
                io.write(" String: " .. psString .. "\n")
                Debug_PopPrint("")
                return true
            end
            
            --Create a no-edge string.
            local sNoEdge = string.sub(psString, iCurrent+1, iChunkEnd-1)
    
            --Diagnostics.
            Debug_Print("Chunk: " .. string.sub(psString, iCurrent, iChunkEnd) .. " is an execution.\n")
            Debug_Print("No-Edge: " .. sNoEdge .. "\n")
        
            --Execute subroutine. If it comes back true, then the action is handled and the AI can stop now.
            local bResult = self:fnHandleExecution(sNoEdge)
            if(bResult == true) then
                Debug_PopPrint("Completed execution, ordered to stop parsing.\n")
                return true
            end
            
            --If this flag comes back true, then we behave like a failed conditional, and move to the next line.
            if(gbAIStringExecutionBreaksList == true) then
                Debug_PopPrint("Execution behaves like failed conditional from special flag.\n")
                return false
            end
        
            --Otherwise, run the next chunk.
            Debug_Print("Execution succeeded. Running next chunk.\n")
            iCurrent = iChunkEnd + 1
        
        --Malformed string.
        else
            io.write("AIString:fnHandleString() - Warning, malformed string. No whitespace is allowed, be sure to trim after setting.\n")
            io.write(" There must only be {} and [] blocks after whitespace is trimmed.\n")
            io.write(" Execution: " .. psString .. "\n")
            Debug_PopPrint("")
            return false
        end
    end
    ]=]
    
    -- |[Debug]|
    Debug_PopPrint("No more chunks. Finished.\n")
    return false
end

-- |[ ============================== AIString:fnHandleExecution() ============================== ]|
--Given an execution of the format E:EXECUTION with possible additional arguments, executes the
-- instruction. This can include things like setting variables in the owning character to printing
-- things to the console to stopping further execution.
--If this function returns true, then execution is handled and the AIString can stop. Otherwise it
-- should return false.
function AIString:fnHandleExecution(psExecution)
    
    -- |[ =============== Argument Check =============== ]|
    if(psExecution == nil) then return false end
    
    --Diagnostics.
    local bDiagnostics = Debug_GetFlag("AIString: All") or Debug_GetFlag("AIString: Execute")
    Debug_PushPrint(bDiagnostics, "{Handling Execution}\n")
    
    --Reset flag.
    gbAIStringExecutionBreaksList = false
    
    -- |[Special Check: Finish]|
    --If the execution string is just "Finish" then return true immediately. This is used to indicate
    -- to the AI that it needs to exit gracefully.
    if(psExecution == "Finish") then
        gbAIStringFinishedGracefully = true
        Debug_PopPrint("Finish case found, exiting.\n")
        return true
    end
    
    -- |[Subdivision]|
    --Subdivide the string with the delimiter ":". Some execution cases only need one entry, most need two.
    -- The second part is frequently a Term, but may not be.
    local saStrings = fnSubdivideStrict(psExecution, ":", "'")
    local iArgs = #saStrings
    
    --Trim stack delimiters.
    for i = 1, #saStrings, 1 do
        if(string.sub(saStrings[i], 1, 1) == "'") then
            saStrings[i] = string.sub(saStrings[i], 2, -2)
        end
    end
    
    -- |[ =============== Continue Flag ================ ]|
    --Unsers the prime/target flags, allowing more than one execution of actions per enemy turn. This can emulate free actions. You actually don't technically
    -- need to use this, but the code will bark warnings at you since you probably did multiple actions by mistake.
    if(saStrings[1] == "Continue") then
        self.bHasPrimed = false
        self.bHasTargeted = false
    
    --Prohibit. Blocks prime/target actions until the string ends, allowing common ending code.
    elseif(saStrings[1] == "Prohibit") then
        self.bIsProhibited = true
    
    -- |[ ============== Ability Priming =============== ]|
    -- |[Prime Ability]|
    --Sets an ability as the active ability, and runs the target routine.
    elseif(saStrings[1] == "Prime" and iArgs >= 2) then
        
        --Prohibit. Fails silently.
        if(self.bIsProhibited == true) then
            Debug_PopPrint("Finished execution due to Prohibit on a Prime call.\n")
            return false
        end
        
        --Prime warning.
        if(self.bHasPrimed == true) then
            io.write("AIString: Warning. Running Prime, but routine has already primed. Use {Continue} to mark that multiple actions in a turn is not a bug.\n")
        end
        
        --Execute.
        self.bHasPrimed = true
        self:fnHandlePrime(saStrings[2])
        
    -- |[PrimeByRoll]|
    --Frequently an AI selects a random skill from a set of abilities that have chances to be used. This is done by querying the variable
    -- iAttackRoll which is rolled from 1 to 100 at the start of each action. This function contains a compacted version of this functionality.
    -- Note that the chance is a flat percentage, you do NOT need to add the numbers for multiple executions. That is, if Bite has a 30% chance
    -- to activate, Peck has a 30% chance, and Punch has a 40%, then you use 30, 30, 40, NOT 30, 60, 100. The algorithm will handle decrementing
    -- the iAttackRoll value when called.
    --Format: {PrimeByRoll:AbilityName:Chance}
    elseif(saStrings[1] == "PrimeByRoll" and iArgs >= 3) then
        
        --Prohibit. Fails silently.
        if(self.bIsProhibited == true) then
            Debug_PopPrint("Finished execution due to Prohibit on a PrimeByRoll call.\n")
            return false
        end
        
        --Prime warning.
        if(self.bHasPrimed == true) then
            io.write("AIString: Warning. Running PrimeByRoll, but routine has already primed. Use {Continue} to mark that multiple actions in a turn is not a bug.\n")
        end
    
        --Get the attack roll.
        local iChance = tonumber(saStrings[3])
        local iAttackRoll = VM_GetVar("Root/Variables/Combat/" .. RO_GetID() .. "_AI/iAttackRoll", "N")
    
        --If the chance is within range, prime it.
        if(iChance >= iAttackRoll) then
            self.bHasPrimed = true
            self:fnHandlePrime(saStrings[2])
        
        --Otherwise, decrease the attack roll and go to the next line.
        else
            VM_SetVar("Root/Variables/Combat/" .. RO_GetID() .. "_AI/iAttackRoll", "N", iAttackRoll - iChance)
            gbAIStringExecutionBreaksList = true
        end
    
    -- |[ ============== Target Selection ============== ]|
    -- |[Target]|
    --Marks the provided target for the primed ability. There are a range of valid target options, some of which require arguments.
    elseif(saStrings[1] == "Target" and iArgs >= 2) then
        
        --Prohibit. Fails silently.
        if(self.bIsProhibited == true) then
            Debug_PopPrint("Finished execution due to Prohibit on a Target call.\n")
            return false
        end
        
        --Target warning.
        if(self.bHasTargeted == true) then
            io.write("AIString: Warning. Running Target, but routine has already targeted. Use {Continue} to mark that multiple actions in a turn is not a bug.\n")
        end
        
        --Execute.
        self.bHasTargeted = true
        self:fnHandleTargetResolve(saStrings)
    
    -- |[ =============== List Selection =============== ]|
    -- |[List]|
    --Allows multi-step list construction which can later be used to select targets.
    elseif(saStrings[1] == "List" and iArgs >= 2) then
        self:fnHandleListCall(saStrings)
    
    -- |[ ============= Internal Variables ============= ]|
    -- |[Set Variable]|
    --Sets a variable local to this entity to the given value. Value is a Term and therefore can have math done on it. Only N and S are valid
    -- types, if another type is used it defaults to numerical.
    --Format: {SVAR:VarName:N/S:Value}
    elseif(saStrings[1] == "SVAR" and iArgs >= 4) then
    
        --Fast-access.
        local sVariableName = saStrings[2]
        local sType = "N"
        if(saStrings[3] == "S") then sType = "S" end
        
        --The final value is resolved from the term. Call a function for this.
        local zFinalValue = self:fnEvaluateTerm(saStrings[4])
        
        --Add this variable for tracking.
        self:fnAddTrackingVariable(sVariableName, sType)
        
        --Set.
        VM_SetVar("Root/Variables/Combat/" .. RO_GetID() .. "_AI/" .. sVariableName, sType, zFinalValue)

    -- |[Set Turn Reset Global Variable]|
    --At the start of every turn, all variables in "Root/Variables/Combat/ResetAtTurnStart/" get reset to 0 or "Null". This function
    -- sets a value in that set, allowing AIs to talk to each other per-turn. It is otherwise the same as SVAR.
    --Format: {SetResettable:VarName:N/S:Value}
    elseif(saStrings[1] == "SetResettable" and iArgs >= 4) then
    
        --Fast-access.
        local sVariableName = saStrings[2]
        local sType = "N"
        if(saStrings[3] == "S") then sType = "S" end
        
        --The final value is resolved from the term. Call a function for this.
        local zFinalValue = self:fnEvaluateTerm(saStrings[4])
        
        --Set.
        VM_SetVar("Root/Variables/Combat/ResetAtTurnStart/" .. sVariableName, sType, zFinalValue)
    
    -- |[Increment Variable]|
    --Special case of above, increments a variable. Saves a few get/set steps. Assumed to be numeric.
    --Format: {INCR:VarName:Amount}
    elseif(saStrings[1] == "INCR" and iArgs >= 2) then
    
        --Fast-access.
        local sVariableName = saStrings[2]
        local iAmount = 1
        if(iArgs >= 3) then iAmount = tonumber(saStrings[3]) end
        
        --Add this variable for tracking.
        self:fnAddTrackingVariable(sVariableName, "N")
        
        --Set.
        local iValue = VM_GetVar("Root/Variables/Combat/" .. RO_GetID() .. "_AI/" .. sVariableName, "N")
        VM_SetVar("Root/Variables/Combat/" .. RO_GetID() .. "_AI/" .. sVariableName, "N", iValue + iAmount)

    -- |[Decrement Variable]|
    --Special case of above, decrements a variable. Saves a few get/set steps. Assumed to be numeric.
    --Format: {DECR:VarName:Amount}
    elseif(saStrings[1] == "DECR" and iArgs >= 2) then
    
        --Fast-access.
        local sVariableName = saStrings[2]
        local iAmount = 1
        if(iArgs >= 3) then iAmount = tonumber(saStrings[3]) end
        
        --Add this variable for tracking.
        self:fnAddTrackingVariable(sVariableName, "N")
        
        --Set.
        local iValue = VM_GetVar("Root/Variables/Combat/" .. RO_GetID() .. "_AI/" .. sVariableName, "N")
        VM_SetVar("Root/Variables/Combat/" .. RO_GetID() .. "_AI/" .. sVariableName, "N", iValue - iAmount)

    -- |[To-Zero]|
    --Special case of above, decrements a variable, but locks it to 0 if it goes lower. Can also increment the
    -- variable if the initial value was negative.
    --Format: {TOZERO:VarName:Amount}
    elseif(saStrings[1] == "TOZERO" and iArgs >= 2) then
    
        --Fast-access.
        local sVariableName = saStrings[2]
        local iAmount = 1
        if(iArgs >= 3) then iAmount = tonumber(saStrings[3]) end
        
        --Set.
        local iValue = VM_GetVar("Root/Variables/Combat/" .. RO_GetID() .. "_AI/" .. sVariableName, "N")
        if(iValue < 0) then 
            iValue = iValue + iAmount
            if(iValue > 0) then iValue = 0 end
        elseif(iValue > 0) then
            iValue = iValue - iAmount
            if(iValue < 0) then iValue = 0 end
        end
        
        --Add this variable for tracking.
        self:fnAddTrackingVariable(sVariableName, "N")
        
        --Set.
        VM_SetVar("Root/Variables/Combat/" .. RO_GetID() .. "_AI/" .. sVariableName, "N", iValue)

    -- |[ ============== Global Variables ============== ]|
    --Variables stored in "Root/Variables/Combat/AIGlobals/" can be get/set globally between AIs. Otherwise the same as SVAR.
    --Format: {SetGlobal:VarName:N/S:(Value)}
    elseif(saStrings[1] == "SetGlobal" and iArgs >= 4) then
    
        --Fast-access.
        local sVariableName = saStrings[2]
        local sType = "N"
        if(saStrings[3] == "S") then sType = "S" end
        
        --The final value is resolved from the term. Call a function for this.
        local zFinalValue = self:fnEvaluateTerm(saStrings[4])
        
        --Set.
        VM_SetVar("Root/Variables/Combat/AIGlobals/" .. sVariableName, sType, zFinalValue)
        
        --Create an entry for diagnostics.
        AIString:fnRegisterGlobalVariable("Root/Variables/Combat/AIGlobals/" .. sVariableName, sType)
    
    -- |[ ============ Turn-Reset Variables ============ ]|
    -- |[Initialize Turn Reset Global Variable]|
    --At the start of every turn, all variables in "Root/Variables/Combat/ResetAtTurnStart/" get reset to 0 or "Null". This function
    -- delcares such a variable and sets its initial value.
    --Format: {InitResettable:VarName:N/S:(Value)}
    elseif(saStrings[1] == "InitResettable" and iArgs >= 4) then
    
        --Fast-access.
        local sVariableName = saStrings[2]
        local sType = "N"
        if(saStrings[3] == "S") then sType = "S" end
        
        --The final value is resolved from the term. Call a function for this.
        local zFinalValue = self:fnEvaluateTerm(saStrings[4])
        
        --Set.
        gzTurnResettable:Register("Root/Variables/Combat/ResetAtTurnStart/" .. sVariableName, sType, zFinalValue)
        
        --Create an entry for diagnostics.
        AIString:fnRegisterGlobalVariable("Root/Variables/Combat/ResetAtTurnStart/" .. sVariableName, sType)
    
    -- |[Set Resettable Global Variable]|
    --Same as the SVAR case, except sets a variable in the ResetAtTurn/ path.
    --Format: {SetResettable:VarName:N/S:(Value)}
    elseif(saStrings[1] == "SetResettable" and iArgs >= 4) then
    
        --Fast-access.
        local sVariableName = saStrings[2]
        local sType = "N"
        if(saStrings[3] == "S") then sType = "S" end
        
        --The final value is resolved from the term. Call a function for this.
        local zFinalValue = self:fnEvaluateTerm(saStrings[4])
        
        --Set.
        VM_SetVar("Root/Variables/Combat/ResetAtTurnStart/" .. sVariableName, sType, zFinalValue)
    
    -- |[ =============== Error Case / Diagnostics =============== ]|
    -- |[Print]|
    --Prints the provided string. Remember to use ' '!
    --Format: {Print:'Dogs are cute!'}
    elseif(saStrings[1] == "Print" and iArgs >= 2) then
        self:fnPrint(saStrings[2])
        
    -- |[Print Term]|
    --Prints the provided term after evaluation.
    --Format: {PrintTerm:Rand,(LoRange),(HiRange)}
    elseif(saStrings[1] == "PrintTerm" and iArgs >= 2) then
        self:fnPrintTerm(saStrings[2])
    
    -- |[Report Globals]|
    --Reports all global variables and resettables, including current state and default value for resettables.
    --Format: {ReportGlobals}
    elseif(saStrings[1] == "ReportGlobals") then
        self:fnListGlobalVariables()
    
    -- |[Report Locals]|
    --Reports all local variables. This may report other junk if an AI is using more than just the AI String.
    --Format: {ReportLocals}
    elseif(saStrings[1] == "ReportLocals") then
        self:fnListLocalVariables()
    
    -- |[Error Case]|
    --None of the above cases were true. This may be due to a lack of arguments!
    else
        io.write("AIString:fnHandleExecution() - The type " .. saStrings[1] .." was invalid with " .. iArgs .. " arguments.\n")
        io.write(" Execution: " .. psExecution .. "\n")
    end
    
    -- |[Finish Up]|
    --Other than the special case, executions always return false.
    Debug_PopPrint("Finished execution normally.\n")
    return false
end

-- |[ ================================ AIString:fnHandlePrime() ================================ ]|
--Handles a {Prime:AbilityName} call, which can be called from some other spots in the code.
function AIString:fnHandlePrime(psAbilityName)
    
    -- |[Argument Check]|
    if(psAbilityName == nil) then return end
        
    -- |[Setup]|
    --Variables.
    local iAbilitySlot = 0
    
    -- |[Handle Special Cases]|
    --Special Case: "By Roll". Selects a random ability in the enemy's normal ability set.
    local bIsSpecialCase = false
    if(psAbilityName == "By Roll") then
        bIsSpecialCase = true
        iAbilitySlot = fnPickAbilityByRoll(false)
    
    --Normal case: Try to get the slot by the name.
    else
        iAbilitySlot = AdvCombatEntity_GetProperty("Slot of Ability In Master List", psAbilityName)
    end
    
    --Special Case: "Enemy|Pass Turn" is a system call. Enemies all have this ability registered
    -- and don't need to do any priming.
    if(psAbilityName == "Enemy|Pass Turn") then
        return
    end
    
    -- |[Ability Creation]|
    --Ability was not found. Scan the ability lookups and try to find it.
    if(iAbilitySlot == -1 and bIsSpecialCase == false) then
        
        --Scan list.
        local bFoundAbility = false
        for i = 1, #gczaEnemyAbilityLookups, 1 do
            if(gczaEnemyAbilityLookups[i][1] == psAbilityName) then
                LM_ExecuteScript(gczaEnemyAbilityLookups[i][2], gciAbility_Create)
                bFoundAbility = true
                break
            end
        end
        
        --Print a warning if the ability is not found in the lookups.
        if(bFoundAbility == false) then
            io.write("AIString:fnHandlePrime() - Warning, attempted to prime ability " .. psAbilityName .. " for enemy, but that ability was not found.\n")
            Debug_PopPrint("")
            return false
        
        --Re-get the slot.
        else
            iAbilitySlot = AdvCombatEntity_GetProperty("Slot of Ability In Master List", psAbilityName)
        end
        
        --If the slot is still invalid, the constructor script may be broken.
        if(iAbilitySlot == -1) then
            io.write("AIString:fnHandlePrime() - Warning, attempted to prime ability " .. psAbilityName .. " for enemy, but even after re-running ability script, ability is still not present.\n")
            io.write(" Check the ability constructor to make sure the name is correct.\n")
            Debug_PopPrint("")
            return false
        end
    end
    
    -- |[Prime Ability]|
    --Set the ability as in use, and run its target script.
    AdvCombat_SetProperty("Set Ability As Active", iAbilitySlot)
    AdvCombat_SetProperty("Run Active Ability Target Script")
end

