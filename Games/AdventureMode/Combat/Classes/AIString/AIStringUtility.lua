-- |[ =================================== AI String Utility ==================================== ]|
--Worker functions that typically do exactly what their function name suggests.

-- |[ ========================== AIString:fnGetStatIndexFromString() =========================== ]|
--Given a string, such as "HPMax" or "Atk" etc, returns the index of the stat which can be used to
-- query the stat in an AdvCombatEntity. This is done by scanning gzaModTable which contains a set
-- of string-to-index lookups. This is compatible with mods so long as the stat index is a valid
-- one in the C++ code.
--Returns the index in question, or -1 if not found.
function AIString:fnGetStatIndexFromString(psStatName)

    -- |[Argument Check]|
    if(psStatName == nil) then return -1 end
    
    -- |[Scan]|
    --Run across the mod table.
    for i = 1, #gzaModTable, 1 do
        if(gzaModTable[i][1] == psStatName) then
            return gzaModTable[i][2]
        end
    end

    --Not found.
    io.write("AIString:fnGetStatIndexFromString() - Warning. Unable to locate stat name " .. psStatName .. " on mod table with " .. #gzaModTable .. " entries. Failing.\n")
    return -1
end