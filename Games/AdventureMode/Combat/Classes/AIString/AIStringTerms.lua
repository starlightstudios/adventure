-- |[ ===================================== AI String Terms ==================================== ]|
--A "Term" is one or more statements that may be functions, variables, or constants, and which are
-- going to be mathed together in some manner. In an AIString a Term gets resolved to a final single
-- value which can be a number or a string.
--This script and function handles resolving terms.

-- |[ ================================ AIString:fnEvaluateTerm() =============================== ]|
--Given a string, gets a single final value for the string. The delimiter " " is used to allow simple
-- math operations on the terms, but order of operations is not supported. Terms are expected to come
-- in odd numbers or the term is malformed.
--Note that the final value can be a string or a number, or anything really. Lua allows comparisons
-- between unlike types. When possible values are integers. True is 1, False is 0.
--If the first part of the term resolves to a string, it is immediately returned. Concatenation is not allowed.
-- If a future part of the term resolves to a string after a numerical type in the first slow, it is ignored.
function AIString:fnEvaluateTerm(psTerm)
    
    -- |[ =============== Argument Check =============== ]|
    if(psTerm == nil) then return 0 end
    
    --Diagnostics.
    local bDiagnostics = Debug_GetFlag("AIString: All") or Debug_GetFlag("AIString: Term")
    Debug_PushPrint(bDiagnostics, "Evaluating Term: " .. psTerm .. "\n")
    
    -- |[ ==================== Setup =================== ]|
    --Variables.
    local zFinalValue = 0
    
    --Break the term into pieces. There is expected to be an odd number of terms.
    local saStrings = fnSubdivide(psTerm, " ", "'")
    if(#saStrings % 2 == 0) then
        io.write("AIString:fnEvaluateTerm() - Warning: Received an even number of terms. Terms should come in odd numbers.\n")
        io.write(" Term: " .. psTerm .. "\n")
        Debug_PopPrint("")
        return 0
    end
    
    --Trim stack delimiters.
    for i = 1, #saStrings, 1 do
        if(string.sub(saStrings[i], 1, 1) == "'") then
            saStrings[i] = string.sub(saStrings[i], 2, -2)
        end
    end
    
    --Diagnostics.
    Debug_Print("After subdivision, there are " .. #saStrings .. " terms.\n")
    if(bDiagnostics) then
        for i = 1, #saStrings, 1 do
            Debug_Print(" Term " .. i .. ": " .. saStrings[i] .. "\n")
        end
    end
    
    -- |[ ================== Iteration ================= ]|
    for i = 1, #saStrings, 2 do
    
        -- |[Run Term Resolver]|
        --Setup.
        local zCurrentValue = 0
        local bPerformOperation = true
        local bReturnImmediately = false
        
        --Run handler.
        zCurrentValue, bPerformOperation, bReturnImmediately = self:fnTermSubtypes(saStrings[i], (i == 1))
        
        -- |[Return Immediately]|
        --When receiving a string on the 0th argument or in case of error, return whatever value was resolved and
        -- ignore any further parts to the term.
        if(bReturnImmediately) then
            Debug_PopPrint("Finished evaluation with " .. zCurrentValue .. ", returned out immediately.\n")
            return zCurrentValue
        
        -- |[No Operator]|
        --There is no previous term, so the current value is the final value.
        elseif(i == 1) then
            zFinalValue = zCurrentValue
        
        -- |[Apply Operator]|
        --Get the previous part of the term, if it exists. Handle the operation between the two as needed.
        elseif(bPerformOperation == true) then
    
            --Operator.
            local sOperator = saStrings[i-1]
        
            --Diagnostics.
            if(sOperator ~= "%") then
                Debug_Print(" Evaluating operator: " .. sOperator .. "\n")
            else
                Debug_Print(" Evaluating operator: %%\n")
            end
    
            --Plus.
            if(sOperator == "+") then
                zFinalValue = zFinalValue + zCurrentValue
                Debug_Print(" Addition: " .. zFinalValue .. "\n")
        
            --Minus.
            elseif(sOperator == "-") then
                zFinalValue = zFinalValue - zCurrentValue
                Debug_Print(" Subtraction: " .. zFinalValue .. "\n")
        
            --Multiply.
            elseif(sOperator == "*") then
                zFinalValue = zFinalValue * zCurrentValue
                Debug_Print(" Multiplication: " .. zFinalValue .. "\n")
        
            --Divide. Division by zero barks an error.
            elseif(sOperator == "-") then
                if(zCurrentValue == 0) then
                    io.write("AIString:fnEvaluateTerm() - Warning: Term " .. zCurrentValue .. " resolved to zero, then a division occurred. Failing.\n")
                    io.write(" Term: " .. psTerm .. "\n")
                    Debug_PopPrint("")
                    return 0
                end
                Debug_Print(" Division: " .. zFinalValue .. "\n")
                zFinalValue = zFinalValue / zCurrentValue
            
            --Modulus. Modulus by zero barks an error.
            elseif(sOperator == "%") then
                if(zCurrentValue == 0) then
                    io.write("AIString:fnEvaluateTerm() - Warning: Term " .. zCurrentValue .. " resolved to zero, then a modulus occurred. Failing.\n")
                    io.write(" Term: " .. psTerm .. "\n")
                    Debug_PopPrint("")
                    return 0
                end
                Debug_Print(" Modulus: " .. zFinalValue .. "\n")
                zFinalValue = zFinalValue % zCurrentValue
            end
        end
    end

    -- |[ ================= Finish Up ================== ]|
    Debug_PopPrint("Finished evaluation with " .. zFinalValue .. "\n")
    return zFinalValue
end

-- |[ =============================== AIString:fnTermSubtypes() ================================ ]|
--The "meat" of handling a term, this function resolves which variable/constant/functions are in
-- the term and returns them as needed.
--Returns, in order:
-- zResult. Can be a number or a string.
-- bAllowOperations. Marks whether or not operations (+-/*%) should be allowed.
-- bReturnImmediately. Strings do not allow operations and return immediately.
function AIString:fnTermSubtypes(psTerm, pbIsFirstCall)
    
    -- |[ =============== Argument Check =============== ]|
    if(psTerm        == nil) then return 0, false, true end
    if(pbIsFirstCall == nil) then return 0, false, true end
    
    --Diagnostics.
    Debug_Print(" Subdividing term further: " .. psTerm .. "\n")
    
    --Setup.
    local bAllowOperations = true
    local bReturnImmediately = false
    
    --Constants.
    local i0Byte = string.byte("0")
    local i9Byte = string.byte("9")
    
    -- |[ =============== Subdivide Term =============== ]|
    --Further possible subdivision. Functions will delimit arguments with the comma (,). This is not used for constants
    -- so it is fully possible there will be no additional arguments.
    local saSubStrings = fnSubdivide(psTerm, ",", "'")
    local iSubArgs = #saSubStrings
    
    --Trim stack delimiters.
    for i = 1, #saSubStrings, 1 do
        if(string.sub(saSubStrings[i], 1, 1) == "'") then
            saSubStrings[i] = string.sub(saSubStrings[i], 2, -2)
        end
    end
    
    --Get the first entry in the sub-args and its first letter.
    local sPart = saSubStrings[1]
    local sFirstLetter = string.sub(sPart, 1, 1)
    
    --Diagnostics.
    local bDiagnostics = Debug_GetFlag("AIString: All") or Debug_GetFlag("AIString: Subterm")
    Debug_Print(" Evaluating term: " .. sPart .. " with " .. iSubArgs .. " arguments.\n")
    if(bDiagnostics and #saSubStrings > 1) then
        for p = 1, #saSubStrings, 1 do
            Debug_Print(" " .. saSubStrings[p] .. "\n")
        end
    end
    
    -- |[ ================= Primitives ================= ]|
    -- |[Constant]|
    --If the first letter of this is a number, it has to be a constant. If it's a minus sign, it is also expected to be a number.
    --Format: [30:==:0]
    if(string.byte(sFirstLetter) >= i0Byte and string.byte(sFirstLetter) <= i9Byte or sFirstLetter == "-") then
        zCurrentValue = tonumber(sPart)
        Debug_Print(" Constant. Resolves to " .. zCurrentValue .. "\n")
    
    -- |[String]|
    --If the first letter is a quote, this is a string. These are returned immediately if in slot 1, or ignored.
    --Format: ["StringHere":==:0]
    elseif(sFirstLetter == "\"") then
    
        --Get the string out.
        local iLen = string.len(sPart)
        zCurrentValue = string.sub(sPart, 2, iLen - 1)
        Debug_Print(" Constant String. Resolves to " .. zCurrentValue .. "\n")
        
        --On the 1st slot, return right away.
        if(pbIsFirstCall) then
            bAllowOperations = false
            bReturnImmediately = true
    
        --Otherwise, disallow operations.
        else
            Debug_Print("Resolved string on non-first term. Ignoring.")
            bPerformOperation = false
        end

    -- |[Function: Get Random Number]|
    --Returns a random number between the provided range.
    --Format: [Rand,(LoRange),(HiRange):==:0]
    elseif(sPart == "Rand" and iSubArgs >= 3) then
    
        --Fast-access.
        local iRangeLo = tonumber(saSubStrings[2])
        local iRangeHi = tonumber(saSubStrings[3])
        
        --Roll, return.
        zCurrentValue = LM_GetRandomNumber(iRangeLo, iRangeHi)
        Debug_Print(" Function: Random Number - From " .. iRangeLo .. " to " .. iRangeHi .. ": Final was " .. zCurrentValue .. "\n")
    
    -- |[Function: Get Variable From Local Entity]|
    --Entities store variables in the DataLibrary, which this function can query and return. N or S must be specified to get the numerical or string version.
    -- If the last argument is not provided, or is invalid, then numerical is used.
    --This function *does not* print a warning if the value does not exist, as it will default to zero and this is valid behavior.
    --Format: [GVAR,(VariableName),(N/S Type):==:0]
    elseif(sPart == "GVAR" and iSubArgs >= 2) then
    
        --Fast-access.
        local sVariableName = saSubStrings[2]
        local sType = "N"
        if(iSubArgs >= 3 and saSubStrings[3] == "S") then sType = "S" end
    
        --Numerical types are handled normally:
        if(sType == "N") then
            zCurrentValue = VM_GetVar("Root/Variables/Combat/" .. RO_GetID() .. "_AI/" .. sVariableName, sType)
        
        --String types are ignored if this isn't the 1st argument.
        else
            if(i == 1) then
                bAllowOperations = false
                bReturnImmediately = true
                zCurrentValue = VM_GetVar("Root/Variables/Combat/" .. RO_GetID() .. "_AI/" .. sVariableName, sType)
            else
                bPerformOperation = false
            end
        end
        Debug_Print(" GVAR call. Variable name is: " .. sVariableName .. ", resolved to " .. zCurrentValue .. "\n")
    
    -- |[Function: Get Variable From Global]|
    --Same as GVAR above, except retrieves entries from "Root/Variables/Combat/AIGlobals/".
    --Format: [GetGlobal,(VariableName),(N/S Type):==:0]
    elseif(sPart == "GetGlobal" and iSubArgs >= 2) then
    
        --Fast-access.
        local sVariableName = saSubStrings[2]
        local sType = "N"
        if(iSubArgs >= 3 and saSubStrings[3] == "S") then sType = "S" end
    
        --Numerical types are handled normally:
        if(sType == "N") then
            zCurrentValue = VM_GetVar("Root/Variables/Combat/AIGlobals/" .. sVariableName, sType)
        
        --String types are ignored if this isn't the 1st argument.
        else
            if(i == 1) then 
                bAllowOperations = false
                bReturnImmediately = true
                zCurrentValue = VM_GetVar("Root/Variables/Combat/AIGlobals/" .. sVariableName, sType)
            else
                bPerformOperation = false
            end
        end
    
    -- |[Function: Get Variable From Global Resettable]|
    --Same as GVAR above, except retrieves entries from "Root/Variables/Combat/ResetAtTurnStart/", which is specified to reset at
    -- the start of every turn.
    --Format: [GetResettable,(VariableName),(N/S Type):==:0]
    elseif(sPart == "GetResettable" and iSubArgs >= 2) then
    
        --Fast-access.
        local sVariableName = saSubStrings[2]
        local sType = "N"
        if(iSubArgs >= 3 and saSubStrings[3] == "S") then sType = "S" end
    
        --Numerical types are handled normally:
        if(sType == "N") then
            zCurrentValue = VM_GetVar("Root/Variables/Combat/ResetAtTurnStart/" .. sVariableName, sType)
        
        --String types are ignored if this isn't the 1st argument.
        else
            if(i == 1) then
                bAllowOperations = false
                bReturnImmediately = true
                zCurrentValue = VM_GetVar("Root/Variables/Combat/ResetAtTurnStart/" .. sVariableName, sType)
            else
                bPerformOperation = false
            end
        end

    -- |[ =============== Internal List ================ ]|
    --AIString objects contain an internal list of IDs which can be queried. They can be edited with the {List|} series of operations.
    -- This list can be used to pick targets as well.

    -- |[ListSize]|
    --Number of entries on the internal list, can be 0.
    --Format: [ListSize:==:0]
    elseif(sPart == "ListSize") then
        zCurrentValue = #self.iaList
        Debug_Print(" Requested size of internal list. Returned " .. zCurrentValue .. "\n")
    
    -- |[ ============== Caller Properties ============= ]|
    -- |[Function: Get Tag Count, Self]|
    --Returns the number of times a given tag appears on the acting entity. Can be used for self-buff checks.
    --Format: [SelfTagCount,(Tag Name):==:0]
    elseif(sPart == "SelfTagCount" and iSubArgs >= 2) then
        local sTagName = saSubStrings[2]
        zCurrentValue =  AdvCombatEntity_GetProperty("Tag Count", sTagName)
        
    -- |[Function: Get HP Percent, Self]|
    --Returns the HP percent of the caller, as a number between 0 and 100.
    --Format: [SelfHPPct:==:0]
    elseif(sPart == "SelfHPPct" and iSubArgs >= 1) then
    
        --Default.
        zCurrentValue = 0
    
        --Fetch from active object.
        local iHPCur = AdvCombatEntity_GetProperty("Health")
        local iHPMax = AdvCombatEntity_GetProperty("Health Max")
                
        --Get the HP Percent and multiply by 100.
        if(iHPMax > 0 and iHPCur > 0) then
            zCurrentValue = math.floor(iHPCur / iHPMax * 100)
        end

    -- |[ ======== Slot of Member With Property ======== ]|
    -- |[Slot of Character]|
    --Which slot a character with a property is in. Properties can include things like names or tags. Disabled
    -- combat party members are trimmed by default.
    --Format: [GetPartySlot,(Party Type),ByName,(Character Name):==:0]
    --Format: [GetPartySlot,(Party Type),WithTag,(Tag Name):==:0]
    elseif(sPart == "GetPartySlot" and iSubArgs >= 3) then
    
        --Init slot to -1.
        zCurrentValue = -1
    
        --Fast-access.
        local sPartyGroup = saSubStrings[2]
        local sSubType    = saSubStrings[3]
        
        --Build a party listing with this group.
        local iaIDList = fnBuildIDListByParty(sPartyGroup, RO_GetID(), true)
        Debug_Print("Searching for party slot in party " .. sPartyGroup .. " by subtype " .. sSubType .. "\n")
        Debug_Print("Found " .. #iaIDList .. " valid candidates.\n")
        
        -- |[By Name]|
        --Internal name must be an exact match for the provided name.
        if(sSubType == "ByName" and iSubArgs >= 4) then
            
            --Debug.
            Debug_Print("Trimming by name: " .. saSubStrings[4] .. "\n")
            
            --Iterate.
            local sNameToFind = saSubStrings[4]
            for i = 1, #iaIDList, 1 do
                
                --Get the name of the entity in the lost.
                AdvCombat_SetProperty("Push Entity By ID", iaIDList[i])
                    local sEntityName = AdvCombatEntity_GetProperty("Internal Name")
                DL_PopActiveObject()
                
                --Match.
                if(sEntityName == sNameToFind) then
                    zCurrentValue = i
                    break
                end
            end
        
        -- |[With Tag]|
        --First entry found in the party with the requested tag is returned.
        elseif(sSubType == "WithTag" and iSubArgs >= 4) then
            
            --Debug.
            Debug_Print("Trimming by tag: " .. saSubStrings[4] .. "\n")
            
            --Iterate.
            local sTagToFind = saSubStrings[4]
            for i = 1, #iaIDList, 1 do
                
                --Get the name of the entity in the lost.
                AdvCombat_SetProperty("Push Entity By ID", iaIDList[i])
                    local iTagCount = AdvCombatEntity_GetProperty("Tag Count", sTagToFind)
                DL_PopActiveObject()
                
                --Match.
                if(iTagCount > 0) then
                    zCurrentValue = i
                    break
                end
            end
        
        end
        
        --Diagnostics.
        Debug_Print("Result slot: " .. zCurrentValue .. "\n")
        
    -- |[ ========== Overall Party Properties ========== ]|
    -- |[Lowest HP Percent in Given Party]|
    --Finds the entity with the lowest HP and returns the percentage value (from 0 to 100, integers) of their current health, in the provided party.
    --Note: This skips entities with 0 HP (knocked out) in case of party members.
    --Format: [LowestHPPercent,(Party Type):==:0]
    elseif(sPart == "LowestHPPercent" and iSubArgs >= 2) then
    
        --Fast-access.
        local sPartyName = saSubStrings[2]
        
        --Build a party last based on the type provided.
        local iaIDList = fnBuildIDListByParty(sPartyName, RO_GetID())
        
        --List contains no entries, return 0.
        if(#iaIDList < 1) then
            zCurrentValue = 0

        --Get HPs from everyone in the listed party, find the one that is lowest.
        else
            local iLowestHP = 100
            for i = 1, #iaIDList, 1 do
                AdvCombat_SetProperty("Push Entity By ID", iaIDList[i])
                    local iHPCur = AdvCombatEntity_GetProperty("Health")
                    local iHPMax = AdvCombatEntity_GetProperty("Health Max")
                DL_PopActiveObject()
                
                --Get the HP Percent and multiply by 100.
                if(iHPMax > 0 and iHPCur > 0) then
                    local iPct = math.floor(iHPCur / iHPMax * 100)
                    if(iPct < iLowestHP) then iLowestHP = iPct end
                end
            end
            
            --After resolving this, return.
            zCurrentValue = iLowestHP
        end
        
        --Diagnostics.
        Debug_Print(" Function: Lowest HP Percent in Party. Party is: " .. sPartyName .. " and contained " .. #iaIDList .. " members.\n")
    
    -- |[Function: Size Of Party]|
    --Returns the size of the listed party.
    --Format: [SizeOfParty,(Party Type):==:0]
    elseif(sPart == "SizeOfParty" and iSubArgs >= 2) then
    
        --Fast-access.
        local sPartyName = saSubStrings[2]
        
        --Build party list.
        local iaPartyIDs = fnBuildIDListByParty(sPartyName, RO_GetID())
        
        --Return the size of the list.
        zCurrentValue = #iaPartyIDs

    -- |[ ======= Specific Individual Properties ======= ]|
    -- |[HP by Name]|
    --Get the HP of a named entity. If the entity is not found, returns 0.
    --Format: [HPByName,(Party),(Entity Name):==:0]
    elseif(sPart == "HPByName" and iSubArgs >= 3) then

        --Fast-access.
        local sPartyName  = saSubStrings[2]
        local sEntityName = saSubStrings[3]

        --List of party.
        local iaPartyIDs = fnBuildIDListByParty(sPartyName, RO_GetID())

        --Scan for match.
        for i = 1, #iaPartyIDs, 1 do
            AdvCombat_SetProperty("Push Entity By ID", iaPartyIDs[i])
                local sName = AdvCombatEntity_GetProperty("Internal Name")
                local iCurHP = AdvCombatEntity_GetProperty("Health")
            DL_PopActiveObject()
            
            --If this is a match, return the HP.
            if(sName == sEntityName) then
                zCurrentValue = iCurHP
                break
            end
        end
    
    -- |[HP by Slot]|
    --Get the HP of the entity in the given slot. Note that Active Party is always in the same order
    -- but Combat Party changes as characters rotate in and out, and Combat Party typically has a fixed
    -- max size. If a slot is empty, 0 is returned. Note that slot counts start at 0.
    --Format: [HPBySlot,(Party),Slot:==:0]
    elseif(sPart == "HPBySlot" and iSubArgs >= 3) then

        --Fast-access.
        local sPartyName  = saSubStrings[2]
        local iEntitySlot = saSubStrings[3]

        --List of party.
        local iaPartyIDs = fnBuildIDListByParty(sPartyName, RO_GetID())

        --Slot is out of range.
        if(iEntitySlot < 0 or iEntitySlot >= #iaPartyIDs) then
            zCurrentValue = 0
            Debug_Print(" Function: Get HP by party " .. sPartyName .. " in slot " .. iEntitySlot .. ": Was out of range.\n")
        
        --Slot is valid.
        else
            AdvCombat_SetProperty("Push Entity By ID", iaPartyIDs[iEntitySlot+1])
                zCurrentValue = AdvCombatEntity_GetProperty("Health")
            DL_PopActiveObject()
            Debug_Print(" Function: Get HP by party " .. sPartyName .. " in slot " .. iEntitySlot .. ": Was " .. zCurrentValue .. "\n")
        end
    
    -- |[HP Percent by Slot]|
    --As above but returns the HP Percent, integer from 0 to 100.
    --Format: [HPPctBySlot,(Party),Slot:==:0]
    elseif(sPart == "HPPctBySlot" and iSubArgs >= 3) then

        --Fast-access.
        local sPartyName  = saSubStrings[2]
        local iEntitySlot = tonumber(saSubStrings[3])

        --List of party.
        local iaPartyIDs = fnBuildIDListByParty(sPartyName, RO_GetID())

        --Slot is out of range.
        if(iEntitySlot < 0 or iEntitySlot >= #iaPartyIDs) then
            zCurrentValue = 0
            Debug_Print(" Function: Get HP by party " .. sPartyName .. " in slot " .. iEntitySlot .. ": Was out of range.\n")
        
        --Slot is valid.
        else
        
            --Get value.
            AdvCombat_SetProperty("Push Entity By ID", iaPartyIDs[iEntitySlot+1])
                zCurrentValue = AdvCombatEntity_GetProperty("Health")
                local iHPMax = AdvCombatEntity_GetProperty("Health Max")
            DL_PopActiveObject()
            
            --Error check.
            if(iHPMax > 0) then
                zCurrentValue = math.floor(zCurrentValue / iHPMax) * 100
                Debug_Print(" Function: Get HP by party " .. sPartyName .. " in slot " .. iEntitySlot .. ": Was " .. zCurrentValue .. "\n")
            else
                Debug_Print(" Function: Get HP by party " .. sPartyName .. " in slot " .. iEntitySlot .. ": Was " .. zCurrentValue .. "\n")
            end
        end
    
    -- |[Infection by Name]|
    --Get the slot of the infection afflicting the named character. If no infection, it's slot 0. Infections are used in
    -- Witch Hunter Izana, so using this in any other game will always return 0. A list of infections is in System/203 Infection Packs.lua
    --Format: [InfectionByName,(Entity Name):==:0]
    elseif(sPart == "InfectionByName" and iSubArgs >= 2) then

        --If the infection is not found, default to 0.
        zCurrentValue = 0

        --Fast-access.
        local sCharacterName = saSubStrings[2]

        --List of party.
        local iaPartyIDs = fnBuildIDListByParty("Active", RO_GetID())

        --Scan for match.
        for i = 1, #iaPartyIDs, 1 do
            AdvCombat_SetProperty("Push Entity By ID", iaPartyIDs[i])
                local sName = AdvCombatEntity_GetProperty("Internal Name")
            DL_PopActiveObject()
            
            --If this is a match, look for infections.
            if(sName == sCharacterName) then
                
                --Scan for infection using this ID.
                for p = 1, #gzaInfectionList, 1 do
                    
                    --Check.
                    local sVariablePath  = "Root/Variables/Combat/" .. iaPartyIDs[i] .. "/" .. gzaInfectionList[p].sVariableName
                    local iVariableValue = VM_GetVar(sVariablePath, "N")
                
                    --Value came back nonzero, return the slot of the infection.
                    if(iVariableValue > 0) then
                        zCurrentValue = p
                        break
                    end
                end
                break
            end
        end
        
    -- |[Infection by Combat Slot]|
    --As above except uses combat party slots. Infections can't affect enemies so only the player's party can be queried.
    --Format: [InfectionBySlot,(Entity Slot):==:0]
    elseif(sPart == "InfectionBySlot" and iSubArgs >= 2) then

        --If the infection is not found, default to 0.
        zCurrentValue = 0

        --Get ID.
        local iCombatPartyID = AdvCombat_GetProperty("Combat Party ID", math.floor(tonumber(saSubStrings[2])))

        --Scan for infection using this ID.
        for p = 1, #gzaInfectionList, 1 do
            
            --Check.
            local sVariablePath  = "Root/Variables/Combat/" .. iCombatPartyID .. "/" .. gzaInfectionList[p].sVariableName
            local iVariableValue = VM_GetVar(sVariablePath, "N")
        
            --Value came back nonzero, return the slot of the infection.
            if(iVariableValue > 0) then
                zCurrentValue = p
                break
            end
        end
    
    -- |[Infection Level by Name]|
    --Get the value of the infection currently afflicting the named character. 0 means no infection.
    --Format: [InfectionLevelByName,(Entity Name):==:0]
    elseif(sPart == "InfectionLevelByName" and iSubArgs >= 2) then

        --If the infection is not found, default to 0.
        zCurrentValue = 0

        --Fast-access.
        local sCharacterName = saSubStrings[2]

        --List of party.
        local iaPartyIDs = fnBuildIDListByParty("Active", RO_GetID())

        --Scan for match.
        for i = 1, #iaPartyIDs, 1 do
            AdvCombat_SetProperty("Push Entity By ID", iaPartyIDs[i])
                local sName = AdvCombatEntity_GetProperty("Internal Name")
            DL_PopActiveObject()
            
            --If this is a match, look for infections.
            if(sName == sCharacterName) then
                
                --Scan for infection using this ID.
                for p = 1, #gzaInfectionList, 1 do
                    
                    --Check.
                    local sVariablePath  = "Root/Variables/Combat/" .. iaPartyIDs[i] .. "/" .. gzaInfectionList[p].sVariableName
                    local iVariableValue = VM_GetVar(sVariablePath, "N")
                
                    --Value came back nonzero, return the slot of the infection.
                    if(iVariableValue > 0) then
                        zCurrentValue = iVariableValue
                        break
                    end
                end
                break
            end
        end
        
    -- |[Infection Level by Combat Slot]|
    --Get the value of the infection currently afflicting the slotted character. 0 means no infection.
    --Format: [InfectionLevelBySlot,(Entity Slot):==:0]
    elseif(sPart == "InfectionLevelBySlot" and iSubArgs >= 2) then

        --If the infection is not found, default to 0.
        zCurrentValue = 0

        --Get ID.
        local iCombatPartyID = AdvCombat_GetProperty("Combat Party ID", math.floor(tonumber(saSubStrings[2])))

        --Scan for infection using this ID.
        for p = 1, #gzaInfectionList, 1 do
            
            --Check.
            local sVariablePath  = "Root/Variables/Combat/" .. iCombatPartyID .. "/" .. gzaInfectionList[p].sVariableName
            local iVariableValue = VM_GetVar(sVariablePath, "N")
        
            --Value came back nonzero, return the slot of the infection.
            if(iVariableValue > 0) then
                zCurrentValue = iVariableValue
                break
            end
        end
    
    -- |[ ==================== Error =================== ]|
    -- |[Unhandled]|
    else
        io.write("AIString:fnTermSubtypes() - Warning: Term " .. sPart .. " was unhandled.\n")
        io.write(" Term: " .. psTerm .. "\n")
        return 0, false, true
    end
    
    -- |[ =================== Return =================== ]|
    --Pass back the resolved value and whether or not operations are allowed.
    return zCurrentValue, bAllowOperations, bReturnImmediately
end



