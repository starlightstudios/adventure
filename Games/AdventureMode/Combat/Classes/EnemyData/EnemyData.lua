-- |[ ================================== EnemyData Class File ================================== ]|
--Class file for EnemyData. Call this once at initialization.

--Stores enemy information, usually in chart form. The class itself contains the enemy information
-- and functions to handle table storage.

-- |[ ========== Globals/Constants =========== ]|
--When the adder functions are used, they get appended to this list. In order to store multiple
-- lists, once addition is done the list can be copied and stored elsewhere.
gzaActiveDataListing = nil

--List of all enemy lists. In RoP, many chapters have their own enemy lists. These lists are 
-- scanned for standard enemy spawning.
gzaAllEnemyDataLists = {}

--Portraint listing. The same portrait/offsets can be used by different enemies, so a global listing
-- of portrait data is referred to by individual enemies.
gzaPortraitInfo = {}

-- |[ ============ Class Members ============= ]|
-- |[System]|
EnemyData = {}
EnemyData.__index = EnemyData

-- |[System]|
EnemyData.sActualName   = ""                        --Internal name used to locate this entity.
EnemyData.sDisplayName  = ""                        --Name used for this entity when seen on the UI
EnemyData.iKOForStats   = 0                         --In Adventure, how many times this enemy must be defeated to reveal their stats in the combat inspector.
EnemyData.iKOForResists = 0                         --In Adventure, how many times this enemy must be defeated to reveal their resistances in the combat inspector.

-- |[Arrays]|
EnemyData.saAbilityList = {}                        --When using basic AIs, a list of abilities and probabilities for them to be used.
EnemyData.zaTags        = {}                        --A list of tags and counts to be applied to a spawning enemy.
EnemyData.saDrops       = {}                        --Item drops and probabilities.
EnemyData.saTopics      = {}                        --Topics that are unlocked as soon as this enemy is encountered.

-- |[Primary Stats]|
--Statistics.
EnemyData.iLevel  =   0
EnemyData.iHealth = 100
EnemyData.iMPMax  = 100
EnemyData.iCPMax  =   0
EnemyData.iAtk    =   0
EnemyData.iIni    =   0
EnemyData.iAcc    =   0
EnemyData.iEvd    =   0
EnemyData.iStun   =   0

--Defense/Resistance.
EnemyData.iPrt = 0
EnemyData.iResSls = 0
EnemyData.iResStk = 0
EnemyData.iResPrc = 0
EnemyData.iResFlm = 0
EnemyData.iResFrz = 0
EnemyData.iResShk = 0
EnemyData.iResCru = 0
EnemyData.iResObs = 0
EnemyData.iResBld = 0
EnemyData.iResPsn = 0
EnemyData.iResCrd = 0
EnemyData.iResTrf = 0

--Rewards.
EnemyData.iPlatina = 0
EnemyData.iExp     = 0
EnemyData.iJP      = 0

-- |[Paths]|
EnemyData.sAIPath = ""                              --Path called when the enemy needs to decide which action to use.

-- |[Display]|
EnemyData.sDialogueActor    = "Null"                --If the enemy needs to be in a dialogue sequence, name of the actor they use.
EnemyData.sPortraitPath     = "Null"                --Combat portrait path.
EnemyData.sTurnPortraitPath = "Null"                --Turn-order portrait path.

-- |[Random]|
EnemyData.iSeed = 1                                 --Used when rolling enemy statistics in a game that has random stat scattering.      

-- |[ ============ Class Methods ============= ]|
--System
function EnemyData:new() end

--Additions
--TODO

--Base List
function EnemyData:fnRegisterEntry(pzEntry)              end
function EnemyData:fnLocateEntry(psActualName)           end
function EnemyData:fnLocateEntryInAllLists(psActualName) end

--Combat Spawn
function EnemyData:fnSpawnEnemy() end
function EnemyData:fnHandleMug()  end

--Manipulators
function EnemyData:fnAddAbility(psActualName, psAbilityName, piProbability) end
function EnemyData:fnAddAbilities(psActualName, pzaSkillArray)              end
function EnemyData:fnAppendTag(psActualName, psTagName, piTagCount)         end
function EnemyData:fnAppendTagArray(psActualName, pzaTagArray)              end
function EnemyData:fnApplyAIString(psActualName, psAIStringName)            end

--Portraits
function EnemyData:fnRegisterPortrait(psPortraitPath, psTurnPath, psActorName) end

--Random
function EnemyData:fnRandom()                       end
function EnemyData:fnGetRandomNumber(piLow, piHigh) end

-- |[ ========== Class Constructor =========== ]|
--Expects the rough "type" for animations to be passed in. Animations are things like Pierce, Bleed, 
-- Terrify, Protect, etc. If nothing is passed in, no override is set.
--"Weapon" is a special case that flags the ability to resolve the weapon type.
function EnemyData:new()
    
    -- |[ ========= Setup ========== ]|
    -- |[Argument Check]|
    -- |[Create]|
    local zObject = {}
    setmetatable(zObject, EnemyData)
    
    -- |[ ======== Members ========= ]|
    --Default AI Path.
    zObject.sAIPath = gsRoot .. "Combat/AIs/000 Normal AI.lua"
    
    --Reset local lists so they don't use the global class listings.
    zObject.saAbilityList = {}
    zObject.zaTags        = {}
    zObject.saDrops       = {}
    zObject.saTopics      = {}

    -- |[ ======= Processing ======= ]|
    --Any additional processing for arguments is done here.
    return zObject
end

-- |[ ========= Call Implementations ========= ]|
LM_ExecuteScript(fnResolvePath() .. "EnemyDataAdditions.lua")
LM_ExecuteScript(fnResolvePath() .. "EnemyDataBaseList.lua")
LM_ExecuteScript(fnResolvePath() .. "EnemyDataCombatSpawn.lua")
LM_ExecuteScript(fnResolvePath() .. "EnemyDataManipulators.lua")
LM_ExecuteScript(fnResolvePath() .. "EnemyDataPortraits.lua")
LM_ExecuteScript(fnResolvePath() .. "EnemyDataRandom.lua")
