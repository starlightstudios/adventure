-- |[ ================================= EnemyData Manipulators ================================= ]|
--Common setter functions within the active enemy data array.

-- |[ ================================ EnemyData:fnAddAbility() ================================ ]|
--Locates the given entity and appends ability info to it.
function EnemyData:fnAddAbility(psActualName, psAbilityName, piProbability)

    -- |[Argument Check]|
    if(psActualName  == nil) then return end
    if(psAbilityName == nil) then return end
    if(piProbability == nil) then return end
    
    -- |[Locate]|
    --Get enemy entry.
    local zEnemyData = self:fnLocateEntry(psActualName)
    if(zEnemyData == nil) then
        io.write("EnemyData:fnAddAbility() - Entry " .. psActualName .. " was not found.\n")
        return
    end

    --Get the ability entry.
    local sAbilityPath = nil
    for i = 1, #gczaEnemyAbilityLookups, 1 do
        if(gczaEnemyAbilityLookups[i][1] == psAbilityName) then
            sAbilityPath = gczaEnemyAbilityLookups[i][2]
            break
        end
    end
    if(sAbilityPath == nil) then
        io.write("EnemyData:fnAddAbility() - Ability " .. psAbilityName .. " was not found.\n")
        return
    end

    -- |[Add]|
    --Add to the array.
    table.insert(zEnemyData.saAbilityList, {psAbilityName, sAbilityPath, piProbability})
end

-- |[ =============================== EnemyData:fnAddAbilities() =============================== ]|
--Locates the given entity and appends ability info to it.
--Format: { {sSkillnameA, iProbabilityA}, {sSkillnameB, iProbabilityB}, ... }
function EnemyData:fnAddAbilities(psActualName, pzaSkillArray)

    -- |[Argument Check]|
    if(psActualName  == nil) then return end
    if(pzaSkillArray == nil) then return end
    
    -- |[Locate]|
    --Get enemy entry.
    local zEnemyData = self:fnLocateEntry(psActualName)
    if(zEnemyData == nil) then
        io.write("EnemyData:fnAddAbility() - Entry " .. psActualName .. " was not found.\n")
        return
    end
    
    -- |[Iterate]|
    for i = 1, #pzaSkillArray, 1 do
        
        --Fast-access pointers.
        local sAbilityName = pzaSkillArray[i][1]
        local iProbability = pzaSkillArray[i][2]
        
        --Get the ability entry.
        local sAbilityPath = nil
        for p = 1, #gczaEnemyAbilityLookups, 1 do
            if(gczaEnemyAbilityLookups[p][1] == sAbilityName) then
                sAbilityPath = gczaEnemyAbilityLookups[p][2]
                break
            end
        end
        
        --Not found, print a warning.
        if(sAbilityPath == nil) then
            io.write("EnemyData:fnAddAbilities() - Ability " .. sAbilityName .. " was not found.\n")
        
        --Add it.
        else
            table.insert(zEnemyData.saAbilityList, {sAbilityName, sAbilityPath, iProbability})
        end
    end
end
    
-- |[ ================================ EnemyData:fnAppendTag() ================================= ]|
--Locates the given entity and appends the given tag data to it.
function EnemyData:fnAppendTag(psActualName, psTagName, piTagCount)

    -- |[Argument Check]|
    if(psActualName == nil) then return end
    if(psTagName    == nil) then return end
    if(piTagCount   == nil) then return end
    
    -- |[Locate]|
    local zEnemyData = self:fnLocateEntry(psActualName)
    if(zEnemyData == nil) then
        io.write("EnemyData:fnAppendTag() - Entry " .. psActualName .. " was not found.\n")
        return
    end
    
    -- |[Append]|
    TagTable:fnAddTag(zEnemyData.zaTags, psTagName, piTagCount)
end

-- |[ ============================== EnemyData:fnAppendTagArray() ============================== ]|
--Locates the given entity and appends the given tag data to it.
--Format: { {sTagNameA, iTagCountA}, {sTagNameB, iTagCountB}, ... }
function EnemyData:fnAppendTagArray(psActualName, pzaTagArray)

    -- |[Argument Check]|
    if(psActualName == nil) then return end
    if(pzaTagArray  == nil) then return end
    
    -- |[Locate]|
    local zEnemyData = self:fnLocateEntry(psActualName)
    if(zEnemyData == nil) then
        io.write("EnemyData:fnAppendTagArray() - Entry " .. psActualName .. " was not found.\n")
        return
    end
    
    -- |[Append]|
    for i = 1, #pzaTagArray, 1 do
        TagTable:fnAddTag(zEnemyData.zaTags, pzaTagArray[i][1], pzaTagArray[i][2])
    end
end

-- |[ ============================== EnemyData:fnApplyAIString() =============================== ]|
--Locates the given entity and the AI string index, then applies the needed tags to get the enemy
-- to use that AIString.
function EnemyData:fnApplyAIString(psActualName, psAIStringName)

    -- |[Argument Check]|
    if(psActualName   == nil) then return end
    if(psAIStringName == nil) then return end
    
    -- |[Locate Prototype]|
    local zEnemyData = self:fnLocateEntry(psActualName)
    if(zEnemyData == nil) then
        io.write("EnemyData:fnApplyAIString() - Warning. Enemy " .. psActualName .. " was not found. Stopping.\n")
        return
    end
    
    -- |[Locate String]|
    --Scan against the global AI string listing.
    for p = 1, #gzAIStringList, 1 do
    
        --Name match:
        if(gzAIStringList[p].sName == psAIStringName) then
    
            --Search tag array for AIString. If it already exists, override it.
            local bFoundMatch = false
            for q = 1, #zEnemyData.zaTags, 1 do
                if(zEnemyData.zaTags[q][1] == "AIString") then
                    bFoundMatch = true
                    zEnemyData.zaTags[q][2] = p
                    break
                end
            end

            --Apply tags if one wasn't found.
            if(bFoundMatch == false) then
                table.insert(zEnemyData.zaTags, {"AIString", p})
            end
            return
        end
    end
    
    -- |[Error]|
    io.write("fnApplyAIString(): Warning, no AIString named " .. psAIStringName .. " was found, for enemy " .. psActualName .. ".\n")
end
