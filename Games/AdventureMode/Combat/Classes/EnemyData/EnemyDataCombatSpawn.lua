-- |[ ================================= EnemyData Combat Spawn ================================= ]|
--Uses a prototype to spawn an enemy in battle.

-- |[ ================================ EnemyData:fnSpawnEnemy() ================================ ]|
--Spawns a combat enemy from the prototype. Sends enemy data to the C++ state. Returns the UniqueID
-- of the newly spawned enemy.
function EnemyData:fnSpawnEnemy()
    
    -- |[ ========== Pre-Spawn Work ========== ]|
    --Regardless of mugging or fighting an enemy, any topics created should be handled here.
    for p = 1, #self.saTopics, 1 do
        WD_SetProperty("Unlock Topic", self.saTopics[p], 1)
    end
    
    --If being mugged, provide a percentage of the base values immediately. Ends execution.
    if(TA_GetProperty("Is Mugging Check") == true) then
        return self:fnHandleMug()
    end

    -- |[ ======== Spawning In Combat ======== ]|
    --Add this entity to the combat roster.
    local iEnemyID = AdvCombat_GetProperty("Generate Unique ID")
    local sEnemyUniqueName = string.format("Autogen|%02i", iEnemyID)
    
    --Create.
    AdvCombat_SetProperty("Register Enemy Carnation", sEnemyUniqueName, 0)

        -- |[System]|
        --Name
        AdvCombatEntity_SetProperty("Display Name", self.sDisplayName)
        AdvCombatEntity_SetProperty("Cluster Name", self.sActualName)
        
        --Store mug level for auto-win.
        AdvCombatEntity_SetProperty("Level For Mug Auto Win", self.iLevel)
        
        --Get inspector properties.
        local sKOTrackerPath = VM_GetVar("Root/Variables/Global/Combat/sKOTrackerPath", "S")
        local sClusterName = AdvCombatEntity_GetProperty("Cluster Name")
        local iKOsSoFar = VM_GetVar(sKOTrackerPath .. psActualName, "I")
        
        --Never show abilities on the inspector.
        AdvCombatEntity_SetProperty("Inspector Show Abilities", 1)
        
        -- |[Inspector Show Stats]|
        --If this debug flag is set, show the stats.
        local iAlwaysShowStats = VM_GetVar("Root/Variables/Global/Debug/iAlwaysShowStats", "I")
        if(iAlwaysShowStats == 1.0) then
            AdvCombatEntity_SetProperty("Inspector Show Stats", 0)
        
        --If -1, never show stats:
        elseif(self.iKOForStats == -1) then
        
        --Otherwise, compute and set.
        else
            local iKOsNeeded = self.iKOForStats - iKOsSoFar
            if(iKOsNeeded < 1) then iKOsNeeded = 0 end
            AdvCombatEntity_SetProperty("Inspector Show Stats", iKOsNeeded)
        end
        
        -- |[Inspector Show Resistances]|
        --Same variable as above.
        if(iAlwaysShowStats == 1.0) then
            AdvCombatEntity_SetProperty("Inspector Show Resists", 0)
            
        --If -1, never show resists:
        elseif(self.iKOForResists == -1) then
        
        --Otherwise, compute and set.
        else
            local iKOsNeeded = self.iKOForResists - iKOsSoFar
            if(iKOsNeeded < 1) then iKOsNeeded = 0 end
            AdvCombatEntity_SetProperty("Inspector Show Resists", iKOsNeeded)
        end

        -- |[Data Library]|
        --DataLibrary Variables
        local iUniqueID = RO_GetID()
        giLastEnemyID = iUniqueID
        DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "/")
        
        --During dialogue, which actor represents this character.
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/sDialogueActor", "S", self.sDialogueActor)
        
        -- |[AI]|
        AdvCombatEntity_SetProperty("AI Script", self.sAIPath)

        -- |[Display]|
        --Images
        AdvCombatEntity_SetProperty("Combat Portrait", self.sPortraitPath)
        AdvCombatEntity_SetProperty("Turn Icon", self.sTurnPortraitPath)
        
        --UI Positions. Call the subscript.
        LM_ExecuteScript(gsEnemyPortraitRouting, self.sPortraitPath)
    
        -- |[Stats and Resistances]|
        --Force spawn to 1 HP.
        if(gbEnemiesAreWeak == true) then
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_HPMax, 1)
        
        --Normal:
        else
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_HPMax, self.iHealth)
        end

        --Statistics
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_SPMax,        0)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_SPRegen,      0)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Attack,       self.iAtk)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_MagicAttack,  self.iMAtk)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Defense,      self.iDef)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_MagicDefense, self.iMDef)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Initiative,   self.iIni)
        
        --Resistances
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Slash,  self.iResSls)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Strike, self.iResStk)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Pierce, self.iResPrc)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Flame,  self.iResFlm)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Freeze, self.iResFrz)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Shock,  self.iResShk)
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Pure,   self.iResPur)
        
        --Other stats.
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_StunCap, 100)
        
        --Special: If the flag gbEnemiesSuck is true, enemies get 1 HP, 0 Ini, 0 Evd.
        if(gbEnemiesSuck == true) then
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_HPMax,      1)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Initiative, 0)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Evade,      0)
        end
        
        --Sum.
        AdvCombatEntity_SetProperty("Recompute Stats")
        AdvCombatEntity_SetProperty("Health Percent", 1.00)
        
        -- |[Accuracy]|
        --Enemies bypass proficiency checks and use flat accuracy rates. These are 85 accuracy, 0 crit.
        CarnationCombatEntity_SetProperty("Ignore Proficiency", true)
        CarnationCombatEntity_SetProperty("Flat Accuracy", 85)
        CarnationCombatEntity_SetProperty("Flat Crit Rate", 0)
        
        -- |[Tags]|
        --Tags are non-standard properties that are used by the scripts to compute bonuses that cannot be easily
        -- communicated via resistances, or other special properties like cutscene stuff.
        --The tags array is formatted {{"TagA", iCount}, {"TagB", iCount}, etc}
        for p = 1, #self.zaTags, 1 do
            AdvCombatEntity_SetProperty("Add Tag", self.zaTags[p][1], self.zaTags[p][2])
        end

        -- |[Abilities]|
        --System Abilities
        fnEnemyStandardSystemAbilities()

        --Register abilities.
        local iRegX = 0
        local iRegY = 0
        for p = 1, #self.saAbilityList, 1 do

            --Set.
            LM_ExecuteScript(self.saAbilityList[p][2], gciAbility_Create)
            AdvCombatEntity_SetProperty("Set Ability Slot",             iRegX, iRegY, self.saAbilityList[p][1])
            AdvCombatEntity_SetProperty("Set Ability Slot Probability", iRegX, iRegY, self.saAbilityList[p][3])
            --io.write("Add ability: " .. iRegX .. "x" .. iRegY .. ": " .. self.saAbilityList[p][2] .. " - " .. self.saAbilityList[p][3] .. "\n")

            --Increment. Check if we need to move the line down.
            iRegX = iRegX + 1
            if(iRegX >= gciAbilityGrid_XSize) then
                iRegX = 0
                iRegY = iRegY + 1
                if(iRegY >= gciAbilityGrid_YSize) then 
                    break
                end
            end
        end

        -- |[Rewards Handling]|
        --Factor for mugging.
        local fXPFactor = 1.0
        local fPLFactor = 1.0
        if(TA_GetProperty("Was Mugged") == true) then
            fXPFactor = 1.0 - gcfMugExperienceRate
            fJPFactor = 1.0 - gcfMugPlatinaRate
        end
        
        --Rewards
        AdvCombatEntity_SetProperty("Reward XP",      math.floor(self.iExp     * fXPFactor))
        AdvCombatEntity_SetProperty("Reward JP",      0)
        AdvCombatEntity_SetProperty("Reward Platina", math.floor(self.iPlatina * fPLFactor))
        AdvCombatEntity_SetProperty("Reward Doctor", -1)
        
        --Item rewards. These only apply if the entity was not mugged.
        if(TA_GetProperty("Was Mugged") == false) then
            
            --Roll per-enemy.
            local iItemRoll = LM_GetRandomNumber(1, 100)
            
            --Scan drops.
            for p = 1, #self.saDrops, 1 do
                if(iItemRoll >= self.saDrops[p][2] and iItemRoll <= self.saDrops[p][3]) then
                    AdvCombatEntity_SetProperty("Reward Item", self.saDrops[p][1])
                end
            end
        end
        
    DL_PopActiveObject()
    
    --Return the generated ID.
    self = nil
    return iUniqueID
end

-- |[ ================================ EnemyData:fnHandleMug() ================================= ]|
--If currently mugging an enemy, data is uploaded based on factors that give reduced rewards. Not
-- all games have mugging, in which case they should override this function.
--Returns 0 as no UniqueID is generated.
function EnemyData:fnHandleMug()
        
    --Cash, XP, JP.
    TA_SetProperty("Mug Platina",    math.floor(self.iPlatina * gcfMugPlatinaRate))
    TA_SetProperty("Mug Experience", math.floor(self.iExp     * gcfMugExperienceRate))
    TA_SetProperty("Mug Job Points", 0)
    
    --Item Roller
    local iItemRoll = LM_GetRandomNumber(1, 100)
    for p = 1, #self.saDrops, 1 do
        if(iItemRoll >= self.saDrops[p][2] and iItemRoll <= self.saDrops[p][3]) then
            TA_SetProperty("Mug Item", self.saDrops[p][1])
        end
    end

    --Stop execution.
    return 0
end
