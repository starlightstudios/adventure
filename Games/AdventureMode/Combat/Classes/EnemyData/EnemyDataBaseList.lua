-- |[ ================================= EnemyData Base Listing ================================= ]|
--Adds and searches related to gzaActiveDataListing.

-- |[ ============================== EnemyData:fnRegisterEntry() =============================== ]|
--Registers a new entry onto gzaActiveDataListing. If the named entry already exists, reports an 
-- error and stops.
function EnemyData:fnRegisterEntry(pzEntry)
    
    -- |[Argument Check]|
    if(pzEntry == nil) then
        io.write("EnemyData:fnRegisterEntry() - Entry provided was nil. Failing.\n")
        return
    end
    
    --Make sure the active data listing exists.
    if(gzaActiveDataListing == nil) then
        io.write("EnemyData:fnRegisterEntry() - gzaActiveDataListing was nil. Failing.\n")
        return
    end
    
    -- |[Duplicate Check]|
    --Scan the list.
    local zExistingEntry = EnemyData:fnLocateEntry(pzEntry.sActualName)
    if(zExistingEntry ~= nil) then
        io.write("EnemyData:fnRegisterEntry() - Entry " .. pzEntry.sActualName .. " already exists. Stopping.\n")
        return
    end

    -- |[Register]|
    table.insert(gzaActiveDataListing, pzEntry)
end

-- |[ =============================== EnemyData:fnLocateEntry() ================================ ]|
--Returns an entry matching the given name. If no entry is found, returns nil.
function EnemyData:fnLocateEntry(psActualName)

    -- |[Argument Check]|
    if(psActualName == nil) then return nil end
    
    --Make sure the active data listing exists.
    if(gzaActiveDataListing == nil) then
        io.write("EnemyData:fnLocateEntry() - gzaActiveDataListing was nil. Failing.\n")
        return
    end
    
    -- |[Search]|
    --Scan the list for a match.
    for i = 1, #gzaActiveDataListing, 1 do
        if(gzaActiveDataListing[i].sActualName == psActualName) then
            return gzaActiveDataListing[i]
        end
    end

    --Not found.
    return nil
end

-- |[ ========================== EnemyData:fnLocateEntryInAllLists() =========================== ]|
--Returns an entry matching the given name from the finalized list-of-lists that stores all enemy data.
function EnemyData:fnLocateEntryInAllLists(psActualName)

    -- |[Argument Check]|
    if(psActualName == nil) then return nil end
    
    -- |[Scan]|
    for i = 1, #gzaAllEnemyDataLists, 1 do
        for p = 1, #gzaAllEnemyDataLists[i], 1 do
            if(gzaAllEnemyDataLists[i][p].sActualName == psActualName) then
                return gzaAllEnemyDataLists[i][p]
            end
        end
    end
    
    --Not found.
    return nil
end
