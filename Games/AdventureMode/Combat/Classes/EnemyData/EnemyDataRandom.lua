-- |[ ==================================== EnemyData Random ==================================== ]|
--Random generation utilities allowing internal seed control.

-- |[ ================================== EnemyData:fnRandom() ================================== ]|
--Generates a random number between the 0.0 and 1.0 and advances the internal seed.
function EnemyData:fnRandom()
    
    --If the seed is zero or negative somehow, set it to 1. Otherwise, the RNG "locks" to 0.
    -- This algorithm was also originally built for unsigned ints so we also remove negatives.
    if(self.iSeed <= 0) then self.iSeed = 1 end
    
    --Set and run.
    local a = 16807
    local m = 2147483647
    self.iSeed = (a * self.iSeed) % m
    return (self.iSeed / (m-1))
end

-- |[ ============================= EnemyData:fnGetRandomNumber() ============================== ]|
--Implementation that generates an integer within the specified range.
function EnemyData:fnGetRandomNumber(piLow, piHigh)
    
    -- |[Cycle]|
    --Always cycle the random generator even if the inputs were invalid or equal.
    local fRandomVal = self:fnRandom()
    
    -- |[Argument Check]|
    if(piLow  == nil) then return 0 end
    if(piHigh == nil) then return 0 end
    
    -- |[Switch Check]|
    --It is legal for the two values to be the same, just return either.
    if(piLow == piHigh) then return piLow end
    
    --If the values are out of order, switch them.
    if(piLow > piHigh) then 
        local iSwap = piHigh
        piHigh = piLow
        piLow = iSwap
    end
    
    -- |[Generate]|
    --To preserve accuracy, we only shift to integer right before returning.
    local iResult = ((piHigh - piLow) * fRandomVal) + piLow
    return math.round(iResult)
end

