-- |[ ================================== EnemyData Portraits =================================== ]|
--Functions related to the global portrait listing.

-- |[ ============================= EnemyData:fnRegisterPortrait() ============================= ]|
--Registers a portrait entry.
function EnemyData:fnRegisterPortrait(psPortraitPath, psTurnPath, psActorName)

    -- |[Argument Check]|
    if(psPortraitPath == nil) then return end
    if(psTurnPath     == nil) then return end
    if(psActorName    == nil) then return end
    
    -- |[Duplicate Check]|
    local zCheckPortrait = self:fnLocatePortraitEntry(psPortraitPath)
    if(zCheckPortrait ~= nil) then
        io.write("EnemyData:fnRegisterPortrait() - Warning. Portrait entry " .. psPortraitPath .. " already exists. Failing.\n")
        return
    end
    
    -- |[Register]|
    local zEntry = {}
    zEntry.sPath            = psPortraitPath
    zEntry.sTurnPortrait    = psTurnPath
    zEntry.sDialogueActor   = psActorName
    table.insert(gzaPortraitInfo, zEntry)
end

-- |[ =========================== EnemyData:fnLocatePortraitEntry() ============================ ]|
--Locates and returns a portrait entry according to its path. Returns nil if not found.
function EnemyData:fnLocatePortraitEntry(psPortraitPath)

    -- |[Argument Check]|
    if(psPortraitPath == nil) then return nil end
    
    -- |[Scan]|
    for i = 1, #gzaPortraitInfo, 1 do
        if(gzaPortraitInfo[i].sPath == psPortraitPath) then
            return gzaPortraitInfo[i]
        end
    end
    
    --Not found.
    return nil
end

-- |[ ========================== EnemyData:fnSetPortraitDataByPath() =========================== ]|
--Sets an enemy data entry to use a given portrait data set.
function EnemyData:fnSetPortraitDataByPath(psActualName, psPortraitPath)
    
    -- |[Argument Check]|
    if(psActualName   == nil) then return end
    if(psPortraitPath == nil) then return end
    
    -- |[Locate]|
    --Locate the enemy data.
    local zEnemyData = self:fnLocateEntry(psActualName)
    if(zEnemyData == nil) then
        io.write("EnemyData:fnSetPortraitDataByPath() - Warning. Enemy " .. psActualName .. " was not found. Stopping.\n")
        return
    end
    
    --Locate the portrait data.
    local zPortraitData = self:fnLocatePortraitEntry(psPortraitPath)
    if(zPortraitData == nil) then
        io.write("EnemyData:fnSetPortraitDataByPath() - Warning. Portrait " .. zPortraitData .. " was not found. Stopping.\n")
        return
    end
    
    -- |[Set]|
    zEnemyData.sDialogueActor    = zPortraitData.sDialogueActor
    zEnemyData.sPortraitPath     = zPortraitData.sPath
    zEnemyData.sTurnPortraitPath = zPortraitData.sTurnPortrait
end