-- |[ ================================ ExecStatPack Class File ================================= ]|
--Class file for ExecStatPack. Call this once at initialization.

--An ExecStatPack is created whenever an ability executes, typically in the file 100 fnAbilityHandler.lua
-- in the routines folder. It stores execution information about an ability on a global which gets
-- updated as the ability goes through the execution stages.

--During execution, the global variable gzAbiPack stores an ExecStatPack.

-- |[ ============ Class Members ============= ]|
-- |[System]|
ExecStatPack = {}
ExecStatPack.__index = ExecStatPack
    
-- |[Flags]|
--Flags, storing things like lifesteal, hits, crits, etc.
ExecStatPack.iTimerOffset = 0                                           --Increments as the ability executes to space out display of animations and sounds
ExecStatPack.bAlwaysHit = false                                         --If true, ability always connects
ExecStatPack.bAlwaysCrit = false                                        --If true, ability always connects and crits
ExecStatPack.iLifestealVal = 0                                          --Hp to return to originator when ability is done
ExecStatPack.iFinalStun = 0                                             --Final stun value to inflict after modifiers
ExecStatPack.iExecIndex = 0                                             --If multiple actions occur in the same ability use (such as hitting multiple targets) then the exec index tracks which one is firing now
ExecStatPack.bIsNormalTarget = false                                    --A normal target is any target with over 0 HP and not affected by some effect that prevents damage
ExecStatPack.bIsTargetStunnable = false                                 --If true, target can take stun damage
ExecStatPack.bIsTargetStunned = false                                   --If true, target can't dodge the attack
ExecStatPack.bIsTouristMode = false                                     --If set to true by player options, adjusts accuracy and damage
ExecStatPack.iPartyOfOriginator = gciACPartyGroup_None                  --Code indicating the party of the acting entity
ExecStatPack.iPartyOfTarget = gciACPartyGroup_None                      --Code indicating the party of the target

-- |[Timer Modifications]|
ExecStatPack.zaTimerFactors = {}                                        --Stores factors that adjust how many ticks elapse between parts of animations
ExecStatPack.zaTimerFactors.fStaggerFactor      = 1.0
ExecStatPack.zaTimerFactors.fBlackFlash         = 1.0
ExecStatPack.zaTimerFactors.fVoiceCalloutPre    = 1.0
ExecStatPack.zaTimerFactors.fAnimation          = 1.0
ExecStatPack.zaTimerFactors.fVoiceCalloutPost   = 1.0
ExecStatPack.zaTimerFactors.fText               = 1.0
ExecStatPack.zaTimerFactors.fDamage             = 1.0
ExecStatPack.zaTimerFactors.fVoiceCalloutDamage = 1.0
ExecStatPack.zaTimerFactors.fHealing            = 1.0
ExecStatPack.zaTimerFactors.fShields            = 1.0
ExecStatPack.zaTimerFactors.fMPGeneration       = 1.0
ExecStatPack.zaTimerFactors.fSelfHealing        = 1.0
ExecStatPack.zaTimerFactors.fMPRestore          = 1.0
ExecStatPack.zaTimerFactors.fStun               = 1.0
ExecStatPack.zaTimerFactors.fEffect             = 1.0
ExecStatPack.zaTimerFactors.fJobChange          = 1.0

-- |[Tag Storage]|
--Lists of friendly/hostile effects.
ExecStatPack.zaTagEffectsFriendly = {}
ExecStatPack.zaTagEffectsHostile = {}

--Local effects. Resolved during execution, not part of the initial effects.
ExecStatPack.zaLocalEffectsFriendly = {}
ExecStatPack.zaLocalEffectsHostile = {}

-- |[Target/Originator Information]|
--Target IDs before and after redirection, Originator ID.
ExecStatPack.iOriginatorID = 0
ExecStatPack.iTargetID = 0
ExecStatPack.iOrigTargetID = 0

-- |[Ability and Effect Packages]|
ExecStatPack.zAbilityPack = {}
ExecStatPack.zaEffectPacks = {}
ExecStatPack.baEffectsApplied = {}

-- |[Statistics Pack]|
--Stores statistics on what happened during this ability execution. Used for "reaction" skills.
ExecStatPack.zStatPack = {}
ExecStatPack.zTargetInfo = {}
    
-- |[ ============ Class Methods ============= ]|
--System
function ExecStatPack:new() end

--Major Setters
function ExecStatPack:fnAdvanceTimer(piTimer, piAdvance, pfFactor) end
function ExecStatPack:fnDiffTimer(piTimer, piNewTimer, pfFactor) end

function ExecStatPack:fnAdvanceTimer(piTimer, piAdvance, pfFactor)
    return (piTimer + math.floor(piAdvance * pfFactor))
end
function ExecStatPack:fnDiffTimer(piTimer, piNewTimer, pfFactor)
    local iDiff = piNewTimer - piTimer
    return (piTimer + math.floor(iDiff * pfFactor))
end

-- |[ ========== Class Constructor =========== ]|
--Expects the rough "type" for animations to be passed in. Animations are things like Pierce, Bleed, 
-- Terrify, Protect, etc. If nothing is passed in, no override is set.
--"Weapon" is a special case that flags the ability to resolve the weapon type.
function ExecStatPack:new()
    
    -- |[ ========= Setup ========== ]|
    -- |[Argument Check]|
    -- |[Create]|
    local zObject = {}
    setmetatable(zObject, ExecStatPack)
    
    -- |[ ======== Members ========= ]|
    --Reset local lists so they don't use the global class listings.
    zObject.zaTagEffectsFriendly   = {}
    zObject.zaTagEffectsHostile    = {}
    zObject.zaLocalEffectsFriendly = {}
    zObject.zaLocalEffectsHostile  = {}
    zObject.zAbilityPack           = {}
    zObject.zaEffectPacks          = {}
    zObject.baEffectsApplied       = {}
    zObject.zStatPack              = {}
    zObject.zTargetInfo            = {}
    
    -- |[ ======= Processing ======= ]|
    -- |[ ========= Finish ========= ]|
    return zObject
end

-- |[ ========= Call Implementations ========= ]|
--LM_ExecuteScript(fnResolvePath() .. "AbiExecPackAnimations.lua")
