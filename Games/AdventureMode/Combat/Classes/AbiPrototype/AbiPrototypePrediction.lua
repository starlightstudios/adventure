-- |[ ============================== Ability Prototype Prediction ============================== ]|
--Functions related to generating predictions for an ability.

-- |[ =========================== AbiPrototype:fnCreatePrediction() ============================ ]|
--Handles building predictions and running markdowns on descriptions. Sets this package as the internal
-- package, and returns it in case additional setup is needed.
function AbiPrototype:fnCreatePrediction()
    
    -- |[ ========= Setup ========== ]|
    --Error check.
    if(self.zExecutionAbiPackage == nil) then
        io.write("AbiPrototype:fnCreatePrediction() - Warning, attempting to create prediction, but execution package is nil.\n")
    end
    
    --Diagnostics.
    local bDiagnostics = Debug_GetFlag("Predictions: All") or Debug_GetFlag("Predictions: Creation")
    Debug_PushPrint(bDiagnostics, "AbiPrototype:fnCreatePrediction() begins.\n")
    Debug_Print("Ability name: " .. self.sSkillName .. "\n")
    Debug_Print("Job name:     " .. self.sJobName .. "\n")
    
    --Create an empty package.
    local zPredPack = fnCreatePredictionPack()
    
    --Fast-access pointers.
    local zExecPack = self.zExecutionAbiPackage
    
    -- |[ ======== Modules ========= ]|
    -- |[Hit Rate Module]|
    if(zExecPack.bAlwaysHits == false and zExecPack.bAlwaysCrits == false) then
        zPredPack.bHasHitRateModule = true
        zPredPack.zHitRateModule.iMissRate = zExecPack.iMissThreshold
        Debug_Print("Hit rate module applies.\n")
        Debug_Print(" Base miss rate: " .. zPredPack.zHitRateModule.iMissRate .. "\n")
    end
    
    -- |[Damage Module]|
    if(zExecPack.bDoesNoDamage == false) then
        zPredPack.bHasDamageModule = true
        zPredPack.zDamageModule.iDamageType = zExecPack.iDamageType
        zPredPack.zDamageModule.fDamageFactor = zExecPack.fDamageFactor
        if(zExecPack.bUseWeapon) then
            zPredPack.zDamageModule.bUseWeaponType = true
        end
        Debug_Print("Damage module applies.\n")
        Debug_Print(" Damage type:   " .. zPredPack.zDamageModule.iDamageType .. "\n")
        Debug_Print(" Damage factor: " .. zPredPack.zDamageModule.fDamageFactor .. "\n")
    end
    
    -- |[Stun Module]|
    if(zExecPack.iBaseStunDamage > 0) then
        zPredPack.bHasStunModule = true
        zPredPack.zStunModule.iStunBase = zExecPack.iBaseStunDamage
    end
    
    -- |[Healing Module]|
    if(zExecPack.iHealingBase > 0 or zExecPack.fHealingFactor > 0 or zExecPack.fHealingPercent > 0) then
        zPredPack.bHasHealingModule = true
        zPredPack.zHealingModule.iHealingFixed   = zExecPack.iHealingBase
        zPredPack.zHealingModule.fHealingPercent = zExecPack.fHealingPercent
        zPredPack.zHealingModule.fHealingFactor  = zExecPack.fHealingFactor
    end
    
    -- |[Shield Module]|
    if(zExecPack.iShieldsBase > 0 or zExecPack.fShieldsFactor > 0.0) then
        zPredPack.bHasShieldModule = true
        zPredPack.zShieldModule.iShieldFixed  = zExecPack.iShieldsBase
        zPredPack.zShieldModule.iShieldFactor = zExecPack.fShieldsFactor
        Debug_Print("Shield module applies.\n")
        Debug_Print(" Shield fixed:  " .. zPredPack.zShieldModule.iShieldFixed .. "\n")
        Debug_Print(" Shield factor: " .. zPredPack.zShieldModule.iShieldFactor .. "\n")
    end
    
    -- |[Self-Heal Module]|
    if(zExecPack.iSelfHealingBase > 0 or zExecPack.fSelfHealingPercent > 0 or zExecPack.fSelfHealingFactor > 0) then
        zPredPack.bHasSelfHealingModule = true
        zPredPack.zSelfHealingModule.iHealingFixed   = zExecPack.iSelfHealingBase
        zPredPack.zSelfHealingModule.fHealingPercent = zExecPack.fSelfHealingPercent
        zPredPack.zSelfHealingModule.fHealingFactor  = zExecPack.fSelfHealingFactor
    end

    -- |[MP Generation Module]|
    if(zExecPack.iMPGeneration > 0) then
        zPredPack.bHasMPGenerationModule = true
        zPredPack.zMPGenerationModule.iMPGeneration = zExecPack.iMPGeneration
    end

    -- |[Lifesteal Module]|
    if(zExecPack.fLifestealFactor > 0) then
        zPredPack.bHasLifestealModule = true
        zPredPack.zLifestealModule.fStealPercent = zExecPack.fLifestealFactor
    end
    
    -- |[Crit Rate Module]|
    --Ability must not have the no-crit case. Note than an ability that does 0 damage still has a crit package (for effects).
    if(zExecPack.bNeverCrits == false) then
        zPredPack.bHasCritModule = true
        zPredPack.zCritModule.iCritThreshold = zExecPack.iCritThreshold
        zPredPack.zCritModule.fCritBonus     = zExecPack.fCriticalFactor
        Debug_Print("Crit rate module applies.\n")
        Debug_Print(" Base crit rate: " .. zPredPack.zCritModule.iCritThreshold .. "\n")
    end
    
    -- |[Chaser Modules]|
    if(zExecPack.bHasChaserModules) then
        
        --Flag.
        zPredPack.bHasChaserModules = true
        zPredPack.iChaserModulesTotal = #zExecPack.zaChaserModules
        zPredPack.zaChaserModules = {}
    
        --Add each module.
        for i = 1, #zExecPack.zaChaserModules, 1 do
            
            --Basic flags.
            zPredPack.zaChaserModules[i] = {}
            zPredPack.zaChaserModules[i].bConsumeEffect            = zExecPack.zaChaserModules[i].bConsumeEffect
            zPredPack.zaChaserModules[i].sDoTTag                   = zExecPack.zaChaserModules[i].sDoTTag
            zPredPack.zaChaserModules[i].fDamageFactorChangePerDoT = zExecPack.zaChaserModules[i].fDamageFactorChangePerDoT
            zPredPack.zaChaserModules[i].fDamageRemainingFactor    = zExecPack.zaChaserModules[i].fDamageRemainingFactor
            zPredPack.zaChaserModules[1].sConsumeString            = "Consumes [IMG0] DoTs"
            zPredPack.zaChaserModules[1].iConsumeImgCount = 1
            zPredPack.zaChaserModules[1].saConsumeImgPath = {}
    
            --Locate the damage type tag in the global table to get the icon.
            for p = 1, #gzDamageTypes, 1 do
                if(gzDamageTypes[p].sTypeTag == zPredPack.zaChaserModules[i].sDoTTag) then
                    zPredPack.zaChaserModules[1].saConsumeImgPath[1] = "Root/Images/AdventureUI/DamageTypeIcons/" .. gzDamageTypes[p].sDamageTypeIcon
                    break
                end
            end
        end
    end
    
    -- |[Effect Packages]|
    --For each effect package on the ability, add lines.
    local iEffectModule = 0
    if(self.zExecutionEffPackage ~= nil and self.zExecutionEffPackage.sPrototypeName ~= nil) then
        
        --First effect module.
        iEffectModule = iEffectModule + 1
        zPredPack.zaEffectModules[iEffectModule] = fnCreatePredictionEffectModuleFromPrototype(self.zExecutionEffPackage.sPrototypeName)
        Debug_Print("Effect module added. Prototype: " .. self.zExecutionEffPackage.sPrototypeName .. "\n")
        
        --Second module.
        if(self.zExecutionEffPackageB ~= nil) then
            iEffectModule = iEffectModule + 1
            zPredPack.zaEffectModules[iEffectModule] = fnCreatePredictionEffectModuleFromPrototype(self.zExecutionEffPackageB.sPrototypeName)
            Debug_Print("Effect module B added. Prototype: " .. self.zExecutionEffPackageB.sPrototypeName .. "\n")
        end
        
        --Third module.
        if(self.zExecutionEffPackageC ~= nil) then
            iEffectModule = iEffectModule + 1
            zPredPack.zaEffectModules[iEffectModule] = fnCreatePredictionEffectModuleFromPrototype(self.zExecutionEffPackageC.sPrototypeName)
            Debug_Print("Effect module C added. Prototype: " .. self.zExecutionEffPackageC.sPrototypeName .. "\n")
        end
    end
    
    --Diagnostics.
    Debug_PopPrint("AbiPrototype:fnCreatePrediction() finishes.\n")
    
    -- |[Finish Up]|
    self.zPredictionPackage = zPredPack
    return self.zPredictionPackage
end
