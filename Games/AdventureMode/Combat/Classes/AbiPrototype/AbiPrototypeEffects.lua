-- |[ =============================== Ability Prototype Effects ================================ ]|
--Registers effect packages to the AbiPrototype. Has a generic version, as well as buff or debuff
-- specific variants that reduce the number of variables passed in.

-- |[ =============================== AbiPrototype:fnAddEffect() =============================== ]|
--Appends an ability effect package to the provided ability structure. This is specifically designed to use
-- the global effect prototype path, so you'll need to override it if you want to have local effect scripts.
--Returns the applied package if further overrides are needed, but inherently applies it to the structure.
function AbiPrototype:fnAddEffect(piStrNormal, piStrCrit, piApplyType, psApplyText, psCritApplyText, psApplyColorString, psaApplyBonus, psaApplyMalus, psaSeverityBonus, psaSeverityMalus)
    
    -- |[Argument Check]|
    if(piStrNormal        == nil) then return nil end
    if(piStrCrit          == nil) then return nil end
    if(piApplyType        == nil) then return nil end
    if(psApplyText        == nil) then return nil end
    if(psCritApplyText    == nil) then return nil end
    if(psApplyColorString == nil) then return nil end
    
    --Note: The apply bonus/malus/etc arrays are optional.
    
    -- |[Create Package]|
    --Construct the package with defaults.
    local zPack = fnConstructDefaultEffectPackage()
    
    --Strength and Application Type. Note that weapon type *cannot* be used here.
    zPack.iEffectStr      = piStrNormal
    zPack.iEffectCritStr  = piStrCrit
    zPack.iEffectType     = piApplyType
    
    --This assumes that a global effect is going to be created.
    zPack.sEffectPath     = gsGlobalEffectPrototypePath
    zPack.sEffectCritPath = gsGlobalEffectPrototypePath
    
    --On-apply, what string is printed and what color is it.
    zPack.sApplyText      = psApplyText
    zPack.sCritApplyText  = psCritApplyText
    zPack.sApplyTextCol   = psApplyColorString
    
    --Default resist case.
    zPack.sResistText     = "Resisted!"
    zPack.sResistTextCol  = "Color:White"
    
    -- |[Tag Handling]|
    --Initialize the tag arrays. These are completely optional to pass in, not all effects need tags.
    zPack.saApplyTagBonus    = {}
    zPack.saApplyTagMalus    = {}
    zPack.saSeverityTagBonus = {}
    zPack.saSeverityTagMalus = {}
    
    --Check the psaApplyBonus array for special strings. Only the first array is checked for these.
    if(psaApplyBonus ~= nil) then
    
        --Iterate across the array.
        for i = 1, #psaApplyBonus, 1 do
            
            --Check the standard set. This set is built in 000 Variables.lua at game start. The file
            -- that holds the lookups is Root/Lookups/Ability Standard Effect Tags.lua
            local bFoundStandard = false
            for p = 1, #gzaEffectTagModifierList, 1 do
                
                --Match: Append.
                if(psaApplyBonus[i] == gzaEffectTagModifierList[p].sTagName) then
                    bFoundStandard = true
                    table.insert(zPack.saApplyTagBonus,    gzaEffectTagModifierList[p].saApplyTagBonus)
                    table.insert(zPack.saApplyTagMalus,    gzaEffectTagModifierList[p].saApplyTagMalus)
                    table.insert(zPack.saSeverityTagBonus, gzaEffectTagModifierList[p].saSeverityTagBonus)
                    table.insert(zPack.saSeverityTagMalus, gzaEffectTagModifierList[p].saSeverityTagMalus)
                    break
                end
            end
                
            --No special cases, append.
            if(bFoundStandard == false) then
                table.insert(zPack.saApplyTagBonus, psaApplyBonus[i])
            end
        end
    end
    
    --All other tag arrays just append to their respective arrays.
    if(psaApplyMalus ~= nil) then
        for i = 1, #psaApplyMalus, 1 do
            table.insert(zPack.saApplyTagMalus, psaApplyMalus[i])
        end
    end
    if(saSeverityTagBonus ~= nil) then
        for i = 1, #saSeverityTagBonus, 1 do
            table.insert(zPack.saSeverityTagBonus, saSeverityTagBonus[i])
        end
    end
    if(saSeverityTagMalus ~= nil) then
        for i = 1, #saSeverityTagMalus, 1 do
            table.insert(zPack.saSeverityTagMalus, saSeverityTagMalus[i])
        end
    end
    
    -- |[Apply Package]|
    --No packages yet, put it in the A slot.
    if(self.zExecutionEffPackage == nil) then
        self.zExecutionEffPackage = zPack
        return self.zExecutionEffPackage
    
    --B slot.
    elseif(self.zExecutionEffPackageB == nil) then
        self.zExecutionEffPackageB = zPack
        return self.zExecutionEffPackageB
    
    --C slot.
    elseif(self.zExecutionEffPackageC == nil) then
        self.zExecutionEffPackageC = zPack
        return self.zExecutionEffPackageC
    end
    
    -- |[Error]|
    --Print a warning.
    io.write("AbiPrototype:fnAddEffect(): Warning, already has 3 effect packages on ability. Caller: " .. LM_GetCallStack(0) .. "\n")
    return nil
end

-- |[ ================================ AbiPrototype:fnAddBuff() ================================ ]|
--A variant of AbiPrototype:fnAddEffect() that explicitly applies a buff, and thus doesn't need to worry
-- about application strength, type, or crits.
function AbiPrototype:fnAddBuff(psPrototypeName, psApplyText, psApplyColorString, psApplyAnim, psApplySound)
    
    -- |[Argument Check]|
    if(psPrototypeName == nil) then return nil end
    
    -- |[Create Package]|
    --Construct the package with defaults.
    local zPack = fnConstructDefaultEffectPackage()
    
    --Strength and Application Type. Not actually used.
    zPack.iEffectStr      = 1000
    zPack.iEffectCritStr  = 1000
    zPack.iEffectType     = gciDamageType_Physical
    
    --This assumes that a global effect is going to be created.
    zPack.sEffectPath     = gsGlobalEffectPrototypePath
    zPack.sEffectCritPath = gsGlobalEffectPrototypePath
    
    --If an apply animation is provided, set it here. The default is "Null".
    if(psApplyAnim ~= nil) then
        zPack.sApplyAnimation = psApplyAnim
    end
    
    --If an apply sound is provided, set it here. The default is "Null".
    if(psApplySound ~= nil) then
        zPack.sApplySound = psApplySound
    end
    
    --On-apply, what string is printed and what color is it.
    if(psApplyText ~= nil) then
        zPack.sApplyText     = psApplyText
        zPack.sCritApplyText = psApplyText
    end
    if(psApplyColorString ~= nil) then
        zPack.sApplyTextCol = psApplyColorString
    end
    
    --Default resist case.
    zPack.sResistText     = "Resisted!"
    zPack.sResistTextCol  = "Color:White"
    
    -- |[Tag Handling]|
    --Initialize the tag arrays. These are completely optional to pass in, not all effects need tags.
    zPack.saApplyTagBonus    = {}
    zPack.saApplyTagMalus    = {}
    zPack.saSeverityTagBonus = {}
    zPack.saSeverityTagMalus = {}
    
    -- |[Prototype]|
    zPack.sPrototypeName = psPrototypeName
    
    -- |[Apply Package]|
    --No packages yet, put it in the A slot.
    if(self.zExecutionEffPackage == nil) then
        self.zExecutionEffPackage = zPack
        return self.zExecutionEffPackage
    
    --B slot.
    elseif(self.zExecutionEffPackageB == nil) then
        self.zExecutionEffPackageB = zPack
        return self.zExecutionEffPackageB
    
    --C slot.
    elseif(self.zExecutionEffPackageC == nil) then
        self.zExecutionEffPackageC = zPack
        return self.zExecutionEffPackageC
    end
    
    -- |[Error]|
    --Print a warning.
    io.write("fnAddAbilityBuffPack(): Warning, already has 3 effect packages on ability. Caller: " .. LM_GetCallStack(0) .. "\n")
    return nil
end

-- |[ ============================= AbiPrototype:fnAddSelfEffect() ============================= ]|
--Sets a self-effect to fire when this ability executes. The user of the ability gets the self-effect
-- regardless of the target, and it is implied to be a buff so strength doesn't apply.
function AbiPrototype:fnAddSelfEffect(psPrototypeName, psApplyText, psApplyColorString, psApplyAnim, psApplySound)
    
    -- |[Argument Check]|
    if(psPrototypeName == nil) then return end
    
    -- |[Construct]|
    --Builds the basic effect package with all its values at zero.
    self.zExecutionSelfPackage = fnConstructDefaultEffectPackage()
    
    -- |[Overrides]|
    --The effect path and crit path are always the prototype file.
    self.zExecutionSelfPackage.sEffectPath     = gsGlobalEffectPrototypePath
    self.zExecutionSelfPackage.sEffectCritPath = gsGlobalEffectPrototypePath
    
    --If an apply animation is provided, set it here. The default is "Null".
    if(psApplyAnim ~= nil and psApplyAnim ~= "") then
        self.zExecutionSelfPackage.sApplyAnimation = psApplyAnim
    end
    
    --If an apply sound is provided, set it here. The default is "Null".
    if(psApplySound ~= nil and psApplySound ~= "") then
        self.zExecutionSelfPackage.sApplySound = psApplySound
    end
    
    --On-apply, what string is printed and what color is it.
    if(psApplyText ~= nil and psApplyText ~= "") then
        self.zExecutionSelfPackage.sApplyText     = psApplyText
        self.zExecutionSelfPackage.sCritApplyText = psApplyText
    end
    if(psApplyColorString ~= nil and psApplyColorString ~= "") then
        self.zExecutionSelfPackage.sApplyTextCol = psApplyColorString
    end
    
    -- |[Use of Effect Prototype]|
    self.zExecutionSelfPackage.sPrototypeName = psPrototypeName
end

