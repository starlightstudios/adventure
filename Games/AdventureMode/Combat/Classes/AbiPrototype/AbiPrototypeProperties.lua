-- |[ ============================== Ability Prototype Properties ============================== ]|
--Property setting functions within the AbiPrototype class, sets things like name, JP cost, usability
-- and targets.

-- |[ =============================== AbiPrototype:fnSetSystem() =============================== ]|
--Called typically when a new ability prototype is being created, sets common properties like name, job, and
-- display name. This uses human-readable strings as much as possible to make the process easier to
-- modify even if that's a bit slower.
function AbiPrototype:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    
    -- |[Argument Check]|
    if(psJobName      == nil) then return end
    if(psSkillName    == nil) then return end
    if(psDisplayName  == nil) then return end
    if(piJPCost       == nil) then return end
    if(pbIsFreeAction == nil) then return end
    if(psBacking      == nil) then return end
    if(psFrame        == nil) then return end
    if(psIcon         == nil) then return end
    if(psResponse     == nil) then return end
    
    -- |[Special Cases]|
    --If the response case is "Unequippable", then this flag gets toggled on and switches to "Direct" for icon purposes.
    -- Unequippable skills are things like Field Abilities that get handled elsewhere.
    if(psResponse == "Unequippable") then
        psResponse = "Direct"
        self.bCannotBeEquipped = true
    end

    -- |[Naming]|
    --Display/System Variables
    self.sJobName   = psJobName
    self.sSkillName = psSkillName
    
    --Internal name is the name of the ability when it is an internal class ability, one that
    -- is auto-equipped when the player changes class. It is typically just the job name with
    -- " Internal" appended on the end.
    self.sInternalName = psJobName .. " Internal"
    
    --The display name can be "Use Skill Name" in which case it is identical to the skill name.
    if(psDisplayName == "Use Skill Name" or psDisplayName == "$SkillName") then
        self.sDisplayName = psSkillName
    
    --Otherwise, override the display name.
    else
        self.sDisplayName = psDisplayName
    end
    
    -- |[Cost and Combat Display]|
    --The JP unlock cost is an integer, usually using a constant.
    self.iJPUnlockCost = piJPCost
    
    --Mark the ability as a free action or not.
    self.bIsFreeAction = pbIsFreeAction
    
    --The backing is one of "Direct", "DoT", "Heal", "Buff", or "Debuff".
    if(self.bIsFreeAction == gbIsNotFreeAction) then
        if(psBacking == "Direct") then
            self.sIconBacking  = gsAbility_Backing_Direct
        elseif(psBacking == "DoT") then
            self.sIconBacking  = gsAbility_Backing_DoT
        elseif(psBacking == "Heal") then
            self.sIconBacking  = gsAbility_Backing_Heal
        elseif(psBacking == "Buff") then
            self.sIconBacking  = gsAbility_Backing_Buff
        elseif(psBacking == "Debuff") then
            self.sIconBacking  = gsAbility_Backing_Debuff
        end
    else
        if(psBacking == "Direct") then
            self.sIconBacking  = gsAbility_Backing_Free_Direct
        elseif(psBacking == "DoT") then
            self.sIconBacking  = gsAbility_Backing_Free_DoT
        elseif(psBacking == "Heal") then
            self.sIconBacking  = gsAbility_Backing_Free_Heal
        elseif(psBacking == "Buff") then
            self.sIconBacking  = gsAbility_Backing_Free_Buff
        elseif(psBacking == "Debuff") then
            self.sIconBacking  = gsAbility_Backing_Free_Debuff
        end
    end
    
    --The frame is one of "Active", "Passive", "Special", or "Combo".
    if(pbIsFreeAction == gbIsNotFreeAction) then
        if(psFrame == "Active") then
            self.sIconFrame = gsAbility_Frame_Active
        elseif(psFrame == "Passive") then
            self.sIconFrame = gsAbility_Frame_Passive
        elseif(psFrame == "Special") then
            self.sIconFrame = gsAbility_Frame_Special
        elseif(psFrame == "Combo") then
            self.sIconFrame = gsAbility_Frame_Combo
        end
    else
        if(psFrame == "Active") then
            self.sIconFrame = gsAbility_Frame_Free_Active
        elseif(psFrame == "Passive") then
            self.sIconFrame = gsAbility_Frame_Free_Passive
        elseif(psFrame == "Special") then
            self.sIconFrame = gsAbility_Frame_Free_Special
        elseif(psFrame == "Combo") then
            self.sIconFrame = gsAbility_Frame_Free_Combo
        end
    end

    --The icon path is the DLPath to the icon, with "Root/Images/AdventureUI/Abilities/" appended on the front.
    self.sIconPath = "Root/Images/AdventureUI/Abilities/" .. psIcon
    
    --If the icon is "Null" just set it to "Null", it won't be set later.
    if(psIcon == "Null") then
        self.sIconPath = "Null"
    end
    
    --The CP cost is set to NULL. The function fnSetAbilityUsabilityProperties() should handle changing the
    -- icon based on the usability.
    self.sCPIcon = "Null"
    
    -- |[Response Type]|
    --This determines when and how the ability script gets called. Abilities that need to activate when an attack
    -- lands are different than ones that activate only when used. Passives typically activate very little.
    --When making a unique ability, manually overriding these flags may be necessary. The presets
    -- "Direct", "Passive", "Counterattack", and "Ambush" are available.
    if(psResponse == "Direct") then
        self.iResponseType = gciAbility_ResponseStandard_DirectAction
    elseif(psResponse == "Passive") then
        self.iResponseType = gciAbility_ResponseStandard_Passive
    elseif(psResponse == "Counterattack") then
        self.iResponseType = gciAbility_ResponseStandard_Counterattack
    elseif(psResponse == "Ambush") then
        self.iResponseType = gciAbility_ResponseStandard_Ambush
    end
end


-- |[ ============================ AbiPrototype:fnSetSystemEnemy() ============================= ]|
--Same as above, but for explicitly enemy-only abilities. These abilities don't care about being
-- equipped and don't have JP costs. This is typically used in WHI where icons are ignored.
function AbiPrototype:fnSetSystemEnemy(psJobName, psSkillName, psDisplayName, pbIsFreeAction, psResponse)
    
    -- |[Argument Check]|
    if(psJobName      == nil) then return end
    if(psSkillName    == nil) then return end
    if(psDisplayName  == nil) then return end
    if(pbIsFreeAction == nil) then return end
    if(psResponse     == nil) then return end

    -- |[Naming]|
    --Display/System Variables
    self.sJobName   = psJobName
    self.sSkillName = psSkillName
    
    --Internal name is the name of the ability when it is an internal class ability, one that
    -- is auto-equipped when the player changes class. It is typically just the job name with
    -- " Internal" appended on the end.
    self.sInternalName = psJobName .. " Internal"
    
    --The display name can be "Use Skill Name" or "$SkillName" in which case it is identical to the skill name.
    if(psDisplayName == "Use Skill Name" or psDisplayName == "$SkillName") then
        self.sDisplayName = psSkillName
    
    --Otherwise, override the display name.
    else
        self.sDisplayName = psDisplayName
    end
    
    -- |[Cost and Combat Display]|
    --The JP unlock cost is an integer, usually using a constant.
    self.iJPUnlockCost = 0
    
    --Mark the ability as a free action or not.
    self.bIsFreeAction = pbIsFreeAction
    
    --Icons.
    self.sIconBacking = gsAbility_Backing_Direct
    self.sIconFrame   = gsAbility_Frame_Active
    self.sIconPath    = "Root/Images/AdventureUI/Abilities/Attack"
    self.sCPIcon      = "Null"
    
    -- |[Response Type]|
    --This determines when and how the ability script gets called. Abilities that need to activate when an attack
    -- lands are different than ones that activate only when used. Passives typically activate very little.
    --When making a unique ability, manually overriding these flags may be necessary. The presets
    -- "Direct", "Passive", "Counterattack", and "Ambush" are available.
    if(psResponse == "Direct") then
        self.iResponseType = gciAbility_ResponseStandard_DirectAction
    elseif(psResponse == "Passive") then
        self.iResponseType = gciAbility_ResponseStandard_Passive
    elseif(psResponse == "Counterattack") then
        self.iResponseType = gciAbility_ResponseStandard_Counterattack
    elseif(psResponse == "Ambush") then
        self.iResponseType = gciAbility_ResponseStandard_Ambush
    end
    
    -- |[Description]|
    --Enemy abilities do not show descriptions so set that here.
    self.sDescriptionMarkdown = "No Description"
    self.sSimpleDescMarkdown  = "No Description"
    
end

-- |[ ============================= AbiPrototype:fnSetUsability() ============================== ]|
--Called during ability setup, sets common usability properties within an ability.
function AbiPrototype:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    
    -- |[Argument Check]|
    if(piMPCost        == nil) then return end
    if(piCPCost        == nil) then return end
    if(piCooldown      == nil) then return end
    if(psTargetRoutine == nil) then return end
    if(piCPGeneration  == nil) then return end
    
    -- |[Always Available]|
    --Set this flag to false. It is typically used for debug abilities.
    self.bAlwaysAvailable = false

    -- |[Resource Costs]|
    --MP Cost. Can be zero.
    self.iRequiredMP = piMPCost
    
    --CP Cost. Can be zero.
    self.iRequiredCP = piCPCost
    
    --CP Icon setting.
    if(piCPCost < 1 or piCPCost >= 10) then
        self.sCPIcon = "Null"
    else
        self.sCPIcon = "Root/Images/AdventureUI/Abilities/Cmb" .. piCPCost
    end
    
    -- |[Cooldown and Free Action Flags]|
    --If the ability has a cooldown, set these flags. Remember: A 2-turn cooldown allows an ability to be used every-other-turn,
    -- while a 1-turn cooldown prevents a Free Action from being reused and nothing else.
    if(piCooldown > 0) then
        self.bRespectsCooldown = true
        self.iCooldown = piCooldown
    
    --No cooldown.
    else
        self.bRespectsCooldown = false
        self.iCooldown = 0
    end

    --If the ability is a free action, set these flags. Free action status should be set before this is called and be 
    -- in the self structure.
    if(self.bIsFreeAction == gbIsFreeAction) then
        self.iRequiredFreeActions = 1
        self.bRespectActionCap = true
    
    --Not a free action.
    else
        self.iRequiredFreeActions = 0
        self.bRespectActionCap = false
    end

    -- |[Targeting]|
    --Targeting routine is a human-readable string. Options are "Target Self", "Target Enemies Single", "Target Enemies All", "Target Allies Single", 
    -- "Target Allies Single Option Downed", "Target Allies Single Not Self", "Target Allies Single Downed Only", "Target Allies All", "Target All Single", 
    -- "Target Parties", "Target All", "Target Single Entity By Name Option Downed NAME1|NAME2|NAME3|", "Target Single Entity By Name NAME1|NAME2|NAME3|".
    self.sTargetMacro = psTargetRoutine

    -- |[After-Effects]|
    --Amount of CP gained from using this skill.
    self.iCPGain = piCPGeneration
end

-- |[ ============================ AbiPrototype:fnSetThreatFactor() ============================ ]|
--Applies an optional threat factor. The default is nil, if not nil then the user's attack power is
-- multiplied by the factor provided and all targets get that threat applied, even if the attack missed.
function AbiPrototype:fnSetThreatFactor(pfThreat)
    self.fThreatMultiplier = pfThreat
end

-- |[ ============================ AbiPrototype:fnSetEnemyAction() ============================= ]|
--Marks the ability as being an enemy action, showing a black flash and a title.
function AbiPrototype:fnSetEnemyAction(psOverrideTitle)
    
    -- |[Error Check]|
    --If the execution pack doesn't exist yet, do nothing.
    if(self.zExecutionAbiPackage == nil) then return end
    
    -- |[Set Common Flags]|
    --Toggle flags on.
    self.zExecutionAbiPackage.bCauseBlackFlash = true
    self.zExecutionAbiPackage.sShowTitle = self.sDisplayName
    
    --Enemy abilities do not crit.
    self.zExecutionAbiPackage.bNeverCrits = true
    
    -- |[Optional Override]|
    --This argument is optional. It can override the title shown. To disable the title, pass "" or "Null".
    if(psOverrideTitle == "") then
        self.zExecutionAbiPackage.sShowTitle = "Null"
    elseif(psOverrideTitle ~= nil) then
        self.zExecutionAbiPackage.sShowTitle = psOverrideTitle
    end
end
