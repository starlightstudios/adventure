-- |[ ================================ AbiPrototype Class File ================================= ]|
--Class file for AbiPrototype. Call this once at initialization.

--An AbiPrototype stores a version of an ability that can be quickly queried for execution. It stores
-- the name, execution types, icons, usability, costs, descriptions, and so on.

-- |[ ============ Class Members ============= ]|
-- |[System]|
AbiPrototype = {}
AbiPrototype.__index = AbiPrototype

--Display/System Variables
AbiPrototype.sJobName      = "ClassName"                                        --Class name
AbiPrototype.sSkillName    = "AbilityName"                                      --Skill name
AbiPrototype.sInternalName = AbiPrototype.sJobName .. "Internal"                --Name used to find this ability on lookups
AbiPrototype.sDisplayName  = "Display Name"                                     --Name shown when rendering on the UI
AbiPrototype.iJPUnlockCost = gciJP_Cost_Normal                                  --Integer, JP Cost
AbiPrototype.sIconBacking  = gsAbility_Backing_Direct                           --Integer indicating how the ability applies, active/DoT/heal/buff/debuff.
AbiPrototype.sIconFrame    = gsAbility_Frame_Active                             --Integer indicating if the ability is active/passive/special/combo.
AbiPrototype.sIconPath     = "Root/Images/AdventureUI/Abilities/Here"           --DLPath to the ability's icon
AbiPrototype.sCPIcon       = "Null"                                             --Numerical indicator of the CP cost of the ability, over the icon
AbiPrototype.iResponseType = gciAbility_ResponseStandard_DirectAction           --How the ability replies when called on by the C++ program.
AbiPrototype.bCannotBeEquipped = false                                          --If true, ability cannot be equipped on the skills UI
AbiPrototype.sUnlockScript = "Null"                                             --Script called when this ability is unlocked on the skills UI

-- |[Description]|
AbiPrototype.sDescriptionMarkdown = "Description"                               --Basic description string, populated with [tags] which can contain information.
AbiPrototype.sDescription = ""                                                  --Final description, created from the markdown.
AbiPrototype.saImages = {}                                                      --Images used in the description, usually icons like Attack Power or Protection.

-- |[Simplified Description]|
AbiPrototype.sSimpleDescMarkdown = "Simplified Description"
AbiPrototype.sSimpleDesc = ""
AbiPrototype.saSimpleImages = {}

-- |[Usability Variables]|
--Requirements.
AbiPrototype.bAlwaysAvailable     = false                                       --Set to true to ignore all requirements.
AbiPrototype.iRequiredMP          = 0                                           --MP required to use the ability.
AbiPrototype.iRequiredCP          = 0                                           --CP required to use the ability.
AbiPrototype.bRespectsCooldown    = true                                        --If this ability has a cooldown, decrements it.
AbiPrototype.iCooldown            = 0                                           --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
AbiPrototype.bIsFreeAction        = false                                       --Flags as a free action.
AbiPrototype.iRequiredFreeActions = 0                                           --Free actions set this as 1, everything else as 0.
AbiPrototype.bRespectActionCap    = false                                       --Characters with multiple free actions have a hard action cap. This flag checks that cap.
AbiPrototype.sTargetMacro         = "Target Enemies Single"                     --Preset macro, like "Target Self", used by C++ code to resolve targets during painting
AbiPrototype.iCPGain = 0                                                        --CP gained for using this ability
AbiPrototype.zaTags = {}                                                        --Tags. These are on the "ability" itself, when it is equipped. They can affect stats and properties for equipped skills.
                                                                                --{{"TagA", iQuantity}, {"TagB", iQuantity}, etc}

-- |[Packages]|
AbiPrototype.zPredictionPackage    = nil                                        --See fnCreatePredictionPack() for a listing of variables.
AbiPrototype.zExecutionAbiPackage  = nil                                        --See fnConstructDefaultAbilityPackage() for a listing of variables.
AbiPrototype.zExecutionEffPackage  = nil                                        --Optional. See fnConstructDefaultEffectPackage() for a listing of variables.
AbiPrototype.zAllyEffectPackage    = nil                                        --Optional. See fnConstructDefaultEffectPackage() for a listing of variables.
AbiPrototype.zExecutionSelfPackage = nil                                        --Optional. See fnConstructDefaultEffectPackage() for a listing of variables.

-- |[Other Properties]|
AbiPrototype.fThreatMultiplier = nil                                            --If not nil, applies an extra attackpower factor of threat to all targets, even if the ability misses.

-- |[ ============ Class Methods ============= ]|
--System
function AbiPrototype:new() end

--Effects
function AbiPrototype:fnAddEffect(piStrNormal, piStrCrit, piApplyType, psApplyText, psCritApplyText, psApplyColorString, psaApplyBonus, psaApplyMalus, psaSeverityBonus, psaSeverityMalus) end
function AbiPrototype:fnAddBuff(psPrototypeName, psApplyText, psApplyColorString, psApplyAnim, psApplySound)                                                                               end
function AbiPrototype:fnAddSelfEffect(psPrototypeName, psApplyText, psApplyColorString, psApplyAnim, psApplySound)                                                                         end

--Markdown
function AbiPrototype:fnRunDescriptionMarkdowns() end

--Prediction
function AbiPrototype:fnCreatePrediction() end

--Processing
function AbiPrototype:fnCreateAbiPack(psType) end
function AbiPrototype:fnFinalize()            end

--Properties
function AbiPrototype:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse) end
function AbiPrototype:fnSetSystemEnemy(psJobName, psSkillName, psDisplayName, pbIsFreeAction, psResponse)                                  end
function AbiPrototype:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)                                      end
function AbiPrototype:fnSetThreatFactor(pfThreat)                                                                                          end
function AbiPrototype:fnSetEnemyAction(psOverrideTitle)                                                                                    end

-- |[ ========== Class Constructor =========== ]|
--No arguments are required.
function AbiPrototype:new()
    
    -- |[ ========= Setup ========== ]|
    -- |[Create]|
    local zObject = {}
    setmetatable(zObject, AbiPrototype)
    
    -- |[ ======== Members ========= ]|
    --Reset local lists so they don't use the global class listings.
    zObject.saImages       = {}
    zObject.saSimpleImages = {}
    zObject.zaTags         = {}
    
    --Run default constructions.
    zObject.zPredictionPackage   = fnCreatePredictionPack()
    zObject.zExecutionAbiPackage = AbiExecPack:new("Default")

    -- |[ ======= Finish Up ======== ]|
    return zObject
end

-- |[ ========= Call Implementations ========= ]|
LM_ExecuteScript(fnResolvePath() .. "AbiPrototypeEffects.lua")
LM_ExecuteScript(fnResolvePath() .. "AbiPrototypeMarkdown.lua")
LM_ExecuteScript(fnResolvePath() .. "AbiPrototypePrediction.lua")
LM_ExecuteScript(fnResolvePath() .. "AbiPrototypeProcessing.lua")
LM_ExecuteScript(fnResolvePath() .. "AbiPrototypeProperties.lua")
