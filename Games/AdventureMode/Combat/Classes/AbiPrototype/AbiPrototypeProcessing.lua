-- |[ ============================== Ability Prototype Processing ============================== ]|
--Once setup is complete, some functions get called to handle markdowns. Those are encapsulated here.

-- |[ ============================= AbiPrototype:fnCreateAbiPack() ============================= ]|
function AbiPrototype:fnCreateAbiPack(psType)
    self.zExecutionAbiPackage = AbiExecPack:new(psType)
end

-- |[ =============================== AbiPrototype:fnFinalize() ================================ ]|
--Handles building predictions and running markdowns on descriptions.
function AbiPrototype:fnFinalize()
    self:fnCreatePrediction()
    self:fnRunDescriptionMarkdowns()
end
