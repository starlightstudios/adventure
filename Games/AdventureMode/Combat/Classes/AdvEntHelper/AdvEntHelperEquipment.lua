-- |[ ================================= AdvEntHelper Equipment ================================= ]|
--Handlers for equipment calls.

-- |[ ============================ AdvEntHelper:fnCreateEquipSlot() ============================ ]|
--Does the generalized work of creating an equipment slot with various flags. Because this function
-- may be called from multiple locations and may change if new slots are added to the game, it
-- will print a warning if any of its arguments are nil.
function AdvEntHelper:fnCreateEquipSlot(psSlotName, psDisplayName, pbAffectsStats, pbAffectsTags, pbCanBeEmpty, pbIsWeaponDamage, pbIsAltWeapon, pbNoGems)
    
    -- |[Argument Check]|
    local bPrintWarning = false
    if(psSlotName       == nil) then bPrintWarning = true end
    if(psDisplayName    == nil) then bPrintWarning = true end
    if(pbAffectsStats   == nil) then bPrintWarning = true end
    if(pbAffectsTags    == nil) then bPrintWarning = true end
    if(pbCanBeEmpty     == nil) then bPrintWarning = true end
    if(pbIsWeaponDamage == nil) then bPrintWarning = true end
    if(pbIsAltWeapon    == nil) then bPrintWarning = true end
    if(pbNoGems         == nil) then bPrintWarning = true end
    
    --Diagnostic handler.
    if(bPrintWarning == true) then
        SysDiagnostics:fnPrintArgError("AdvEntHelper:fnCreateEquipSlot()")
        return
    end
    
    -- |[Execution]|
    --Create the equipment slot.
    AdvCombatEntity_SetProperty("Create Equipment Slot", psSlotName)
    
    --Display name. Use "$Default" to use the slot name.
    AdvCombatEntity_SetProperty("Set Equipment Slot Display Name", psSlotName, psDisplayName)
    
    --Flags.
    AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", psSlotName, pbAffectsStats)
    AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Tags",       psSlotName, pbAffectsTags)
    AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty",           psSlotName, pbCanBeEmpty)
    AdvCombatEntity_SetProperty("Set Equipment Slot Is Alt Weapon",          psSlotName, pbIsAltWeapon)
    AdvCombatEntity_SetProperty("Set Equipment Slot No Gems",                psSlotName, pbNoGems)
    
    --If this slot is used for weapon damage, mark it:
    if(pbIsWeaponDamage == true) then
        AdvCombatEntity_SetProperty("Set Equipment Slot Used For Weapon Damage", psSlotName)
    end
end

-- |[ =========================== AdvEntHelper:fnScanActiveForItem() =========================== ]|
--For the active AdvCombatEntity, scans its equipment slots for the named item. Returns a table
-- that contains a set of pairs, { {iSlotIndexA, sSlotNameA}, {iSlotIndexB, sSlotNameB}, ... } 
-- that contain the index of the slot and its name. It is possible for an item to be found in 
-- multiple slots legally, and if the item is not found then a list is returned that has no entries.
function AdvEntHelper:fnScanActiveForItem(psItemName)
    
    -- |[Argument Check]|
    if(psItemName == nil) then return {} end
    
    -- |[Execution]|
    --List.
    local zaList = {}
    
    --Scan across the equipment slots.
    local iEquipSlotsTotal = AdvCombatEntity_GetProperty("Total Equipment Slots")
    for i = 0, iEquipSlotsTotal-1, 1 do

        --Name of item in slot. If it matches, this is the item that needs to be replaced.
        local sEquippedItemName = AdvCombatEntity_GetProperty("Equipment In Slot I", i)
        if(sEquippedItemName == psItemName) then

            --Variables.
            local sSlotName = AdvCombatEntity_GetProperty("Name of Equipment Slot", i)
            
            --Assemble into a pack and add to the list.
            table.insert(zaList, {i, sSlotName})
            break
        end
    end
    
    -- |[Finish]|
    --Return the list.
    return zaList
end