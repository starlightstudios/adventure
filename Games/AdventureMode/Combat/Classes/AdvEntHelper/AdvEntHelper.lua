-- |[ ================================ AdvEntHelper Class File ================================= ]|
--Class file for AdvEntHelper. Call this once at initialization.

--This class contains functions meant to automate or obfuscate common tasks, such as setting up
-- equipment slots with fewer script calls or batching together common statistics.

-- |[ =========== Globals/Statics ============ ]|
-- |[ ============ Class Members ============= ]|
-- |[System]|
AdvEntHelper = {}
AdvEntHelper.__index = AdvEntHelper 

-- |[ ============ Class Methods ============= ]|
--System
--Equipment
function AdvEntHelper:fnCreateEquipSlot(psSlotName, pbAffectsStats, pbAffectsTags, pbCanBeEmpty, pbIsWeaponDamage, pbIsAltWeapon, pbNoGems) end
function AdvEntHelper:fnScanActiveForItem(psItemName)                                                                                       end

-- |[ ========== Class Constructor =========== ]|
--Singleton, class should not be instantiated.

-- |[ ========= Call Implementations ========= ]|
LM_ExecuteScript(fnResolvePath() .. "AdvEntHelperEquipment.lua")
