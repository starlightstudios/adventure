-- |[ ================================== Combat Class Routing ================================== ]|
--Calls all combat class files.

-- |[Normal]|
LM_ExecuteScript(gsRoot .. "Combat/Classes/AbiExecPack/AbiExecPack.lua")
LM_ExecuteScript(gsRoot .. "Combat/Classes/AbiPrototype/AbiPrototype.lua")
LM_ExecuteScript(gsRoot .. "Combat/Classes/AdvEntHelper/AdvEntHelper.lua")
LM_ExecuteScript(gsRoot .. "Combat/Classes/AIString/AIString.lua")
LM_ExecuteScript(gsRoot .. "Combat/Classes/EffectList/EffectList.lua")
LM_ExecuteScript(gsRoot .. "Combat/Classes/EffectPack/EffectPack.lua")
LM_ExecuteScript(gsRoot .. "Combat/Classes/EnemyData/EnemyData.lua")
LM_ExecuteScript(gsRoot .. "Combat/Classes/ExecStatPack/ExecStatPack.lua")
LM_ExecuteScript(gsRoot .. "Combat/Classes/TurnResettable/TurnResettable.lua")