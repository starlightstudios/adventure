-- |[ =============================== TurnResettable Class File ================================ ]|
--Class file for TurnResettable. Call this once at initialization.

--A TurnResettable is a simple class that monitor variables stored in the DataLibrary. At the 
-- start of a turn, all variables gets reset to their default value. New entries can be registered
-- and AIStrings can query them.
--Typically, one of these exists at any given time and the class clears itself whenever combat begins.

-- |[ ====== Class Structures ====== ]|
--An entry on the class table zaEntries has the following format:
--zEntry.sDLPath                --DataLibrary path, the path "Root/Variables/Combat/ResetAtTurnStart/" is provided explicitly for this class.
--zEntry.sType                  --Must be "N" or "S"
--zEntry.zDefaultValue          --Typically 0 or "Null"

-- |[ ======== Class Globals ======= ]|
--Global singleton of the TurnResettable.
gzTurnResettable = nil

-- |[ ====== Class Parameters ====== ]|
--Meta-information.
TurnResettable = {}
TurnResettable.__index = TurnResettable

--Primitives.
--Lists.
TurnResettable.zaEntries = {}

--System
function TurnResettable:new() end
function TurnResettable:Register(psPath, psType, pzDefault) end

-- |[ === Direct Implementations === ]|
--Creates the class. Should be run at combat start.
function TurnResettable:new()
    
    -- |[Argument Check]|
    -- |[Create]|
    local zObject = {}
    setmetatable(zObject, TurnResettable)
    
    -- |[Initialize]|
    zObject.zaEntries = {}
    
    -- |[Return]|
    return zObject
end

--Registers a new entry to the object.
function TurnResettable:Register(psPath, psType, pzDefault)
    
    -- |[Argument Check]|
    if(psPath    == nil) then return end
    if(psType    == nil) then return end
    if(pzDefault == nil) then return end
    
    -- |[Duplicate Check]|
    --Check if the entry already exists. If so, do nothing.
    for i = 1, #self.zaEntries, 1 do
        if(self.zaEntries[i].sDLPath == psPath) then
            if(Debug_GetFlag("TurnResettable: Explicit")) then
                Debug_ForcePrint("TurnResettable:Register() - Entry " .. psPath .. " rejected as duplicate.\n")
            end
            return
        end
    end
    
    -- |[Type Check]|
    if(psType ~= "N" and psType ~= "S") then
        io.write("TurnResettable:Register() - Warning, type " .. psType .. " is not handled. Must be N or S.\n")
        return
    end
    
    -- |[Create and Register]|
    local zNewEntry = {}
    zNewEntry.sDLPath = psPath
    zNewEntry.sType = psType
    zNewEntry.zDefaultValue = pzDefault
    table.insert(self.zaEntries, zNewEntry)
    
    --Also sets the variable in the datalibrary in case it didn't exist yet.
    VM_SetVar(psPath, psType, pzDefault)
    
    -- |[Diagnostics]|
    if(Debug_GetFlag("TurnResettable: General") or Debug_GetFlag("TurnResettable: Explicit")) then
        Debug_ForcePrint("TurnResettable:Register() - Registered entry " .. psPath .. " with type " .. psType .. " and default " .. pzDefault .. "\n")
    end
end

--Orders all objects to reset to their default values.
function TurnResettable:Run()
    
    if(Debug_GetFlag("TurnResettable: All") or Debug_GetFlag("TurnResettable: Explicit")) then
        Debug_ForcePrint("TurnResettable:Run() - Executing.\n")
    end
    
    for i = 1, #self.zaEntries, 1 do
        if(Debug_GetFlag("TurnResettable: Explicit")) then
            Debug_ForcePrint(" Reset " .. self.zaEntries[i].sDLPath .. " to " .. self.zaEntries[i].zDefaultValue .. "\n")
        end
        VM_SetVar(self.zaEntries[i].sDLPath, self.zaEntries[i].sType, self.zaEntries[i].zDefaultValue)
    end
end

-- |[ ==== Call Implementations ==== ]|
--LM_ExecuteScript(fnResolvePath() .. "AIStringConditions.lua")
