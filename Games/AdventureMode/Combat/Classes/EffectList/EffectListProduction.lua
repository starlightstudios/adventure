-- |[ ================================= EffectList Production ================================== ]|
--Functions that will both create and then register a prototype using some inputs.

-- |[ =========================== EffectList:fnCreateBuffPrototype() =========================== ]|
--Given a set of values, makes and registers a statmod effect prototype. This is meant for explicit buffs
-- and uses specific strength values for predictions that indicate it always applies.
function EffectList:fnCreateBuffPrototype(psName, psDisplayName, psIcon, piTurns, psStatString, psaDescription, pzaTagList)
    
    -- |[Argument Check]|
    if(psName          == nil) then return end
    if(psDisplayName   == nil) then return end
    if(psIcon          == nil) then return end
    if(piTurns         == nil) then return end
    if(psStatString    == nil) then return end
    if(psaDescription  == nil) then return end

    -- |[Tag Checking]|
    --Setup.
    local zaUseTagList = pzaTagList
    if(zaUseTagList == nil) then zaUseTagList = {} end
    
    --Scan the tag list. If "Is Positive" is not found on a buff, add it.
    fnAppendTag(zaUseTagList, "Is Positive")

    -- |[Normal Version]|
    --This effect uses a standardized StatMod script.
    local zPrototype = fnCreateStatmodEffectPrototype(psDisplayName, piTurns, gbIsBuff, "Root/Images/AdventureUI/Abilities/" .. psIcon, psStatString, psaDescription, true)
    
    --Buffs by default are considered to always apply. There is, by default, no tag bonus or severity bonus.
    zPrototype.iEffectStr         = 1000
    zPrototype.iEffectType        = gciDamageType_Slashing
    zPrototype.saApplyTagBonus    = {}
    zPrototype.saApplyTagMalus    = {}
    zPrototype.saSeverityTagBonus = {}
    zPrototype.saSeverityTagMalus = {}

    --Tags.
    if(pzaTagList == nil) then
        zPrototype.zaTagList = {}
    else
        zPrototype.zaTagList = pzaTagList
    end

    -- |[Register]|
    self:fnRegisterEffect(psName, gciEffect_Is_StatMod, zPrototype)
    
end

-- |[ ============================ EffectList:fnCreateDoTPrototype() =========================== ]|
--Creates a damage-over-time effect prototype and registers it. Damage-over-time effects deal a specific
-- type of damage for a certain number of turns. In addition, a critical-strike version can optionally
-- be added which typically lasts extra turns.
function EffectList:fnCreateDoTPrototype(psName, piTurns, piApplyStrength, pfDamPerTurn, piDamageType, psDisplayName, psIcon, piCritTurns, piCritApplyStrength)
    
    -- |[Argument Check]|
    if(psName          == nil) then return end
    if(piTurns         == nil) then return end
    if(piApplyStrength == nil) then return end
    if(pfDamPerTurn    == nil) then return end
    if(piDamageType    == nil) then return end
    if(psDisplayName   == nil) then return end
    if(psIcon          == nil) then return end

    -- |[Setup]|
    --Compute the total damage.
    local fDamage = pfDamPerTurn * piTurns

    -- |[Normal Version]|
    --This effect uses a standardized DoT script.
    local zPrototype = fnCreateDoTEffectPrototype(piDamageType, piTurns, fDamage)
    zPrototype.sDisplayName = psDisplayName
    zPrototype.sIcon = psIcon
    
    --Information needed for prediction.
    zPrototype.iEffectStr = piApplyStrength

    --Register.
    self:fnRegisterEffect(psName, gciEffect_Is_DoT, zPrototype)
    
    -- |[Critical Strike Version]|
    --If the parameter piCritTurnBonus is nil, no critical strike version exists so stop here.
    if(piCritTurns == nil) then return end
        
    --Adds turns to the damage length and recompute the damage.
    fDamage = pfDamPerTurn * piCritTurns
    
    --Create effect.
    local zPrototypeCrit = fnCreateDoTEffectPrototype(piDamageType, piCritTurns, fDamage)
    zPrototypeCrit.sDisplayName = psDisplayName
    zPrototypeCrit.sIcon = psIcon
    
    --Information needed for prediction.
    zPrototypeCrit.iEffectStr = piCritApplyStrength
    
    --Register.
    self:fnRegisterEffect(psName .. ".Crit", gciEffect_Is_DoT, zPrototypeCrit)
end

-- |[ ============================ EffectList:fnCreateHoTPrototype() =========================== ]|
--Creates a heal-over-time effect prototype and registers it. Heal-over-time effects restore a specific
-- amount of HP each turn.
function EffectList:fnCreateHoTPrototype(psName, psDisplayName, psIcon, pfHealBase, pfHealFactor, piTurns)
    
    -- |[Argument Check]|
    if(psName        == nil) then return end
    if(psDisplayName == nil) then return end
    if(psIcon        == nil) then return end
    if(pfHealBase    == nil) then return end
    if(pfHealFactor  == nil) then return end
    if(piTurns       == nil) then return end

    -- |[Setup]|
    --Create HoT structure.
    local zStruct = {}
    zStruct.sDisplayName = psDisplayName
    zStruct.sIcon = psIcon
    zStruct.sAnimation = "Healing"
    zStruct.fHealBase = pfHealBase
    zStruct.fHealFactor = pfHealFactor
    zStruct.iDuration = piTurns

    -- |[Register]|
    self:fnRegisterEffect(psName, gciEffect_Is_HoT, zStruct)
end

-- |[ ========================== EffectList:fnCreatePassivePrototype() ========================= ]|
--Given a set of values, makes and registers a passive statmod prototype. This is similar to a buff
-- except these don't get removed if the character is KO'd and later revived.
function EffectList:fnCreatePassivePrototype(psName, psDisplayName, psIcon, psStatString, psaDescription, pzaTagList)
    
    -- |[Argument Check]|
    if(psName          == nil) then return end
    if(psDisplayName   == nil) then return end
    if(psIcon          == nil) then return end
    if(psStatString    == nil) then return end
    if(psaDescription  == nil) then return end

    -- |[Tag Checking]|
    --Setup.
    local zaUseTagList = pzaTagList
    if(zaUseTagList == nil) then zaUseTagList = {} end
    
    --Scan the tag list. If "Is Positive" is not found on a buff, add it.
    fnAppendTag(zaUseTagList, "Is Positive")

    -- |[Normal Version]|
    --This effect uses a standardized StatMod script.
    local zPrototype = fnCreateStatmodEffectPrototype(psDisplayName, -1, gbIsBuff, "Root/Images/AdventureUI/Abilities/" .. psIcon, psStatString, psaDescription, true)
    
    --Buffs by default are considered to always apply. There is, by default, no tag bonus or severity bonus.
    zPrototype.iEffectStr         = 1000
    zPrototype.iEffectType        = gciDamageType_Slashing
    zPrototype.saApplyTagBonus    = {}
    zPrototype.saApplyTagMalus    = {}
    zPrototype.saSeverityTagBonus = {}
    zPrototype.saSeverityTagMalus = {}
    
    --Passive, not removed on KO, uses different frame.
    zPrototype.sFrame      = gsAbility_Frame_Passive
    zPrototype.bRemoveOnKO = false

    --Tags.
    if(pzaTagList == nil) then
        zPrototype.zaTagList = {}
    else
        zPrototype.zaTagList = pzaTagList
    end

    -- |[Register]|
    self:fnRegisterEffect(psName, gciEffect_Is_StatMod, zPrototype)
end

-- |[ =========================== EffectList:fnCreateStatPrototype() =========================== ]|
--Given a set of values, makes and registers a statmod effect prototype. Unlike the DoT version, the
-- statmod needs to have a more specific description provided for the effect.
--If the parameters piCritTurns and beyond are nil, no critical version will be registered. In addition,
-- pzaTagList is optional and can be nil.
function EffectList:fnCreateStatPrototype(pzEffectpackage, psName, pbIsBuff, psDisplayName, psIcon, piTurns, piApplyStrength, psStatString, psaDescription, pzaTagList, piCritTurns, piCritApplyStrength)
    
    -- |[Argument Check]|
    if(pzEffectpackage == nil) then return end
    if(psName          == nil) then return end
    if(pbIsBuff        == nil) then return end
    if(psDisplayName   == nil) then return end
    if(psIcon          == nil) then return end
    if(piTurns         == nil) then return end
    if(piApplyStrength == nil) then return end
    if(psStatString    == nil) then return end
    if(psaDescription  == nil) then return end

    -- |[Tag Checking]|
    --Setup.
    local zaUseTagList = pzaTagList
    if(zaUseTagList == nil) then zaUseTagList = {} end
    
    --Scan the tag list. If "Is Positive" is not found on a buff, add it.
    if(pbIsBuff == true) then
        fnAppendTag(zaUseTagList, "Is Positive")
    
    --If "Is Negative" is not found on a debuff, add it.
    else
        fnAppendTag(zaUseTagList, "Is Negative")
    end

    -- |[Normal Version]|
    --Run the standardized builder.
    local zPrototype = fnCreateStatmodEffectPrototype(psDisplayName, piTurns, pbIsBuff, "Root/Images/AdventureUI/Abilities/" .. psIcon, psStatString, psaDescription, true)
    fnGetApplicationDataFromEffectPackage(zPrototype, pzEffectpackage)

    --Tags.
    if(pzaTagList == nil) then
        zPrototype.zaTagList = {}
    else
        zPrototype.zaTagList = pzaTagList
    end

    -- |[Register]|
    self:fnRegisterEffect(psName, gciEffect_Is_StatMod, zPrototype)

    -- |[Critical Version]|
    --If this value is nil, no crit version exists.
    if(piCritTurns == nil) then return end

    --Run the standardized builder.
    local zPrototypeCrit = fnCreateStatmodEffectPrototype(psDisplayName, piCritTurns, pbIsBuff, "Root/Images/AdventureUI/Abilities/" .. psIcon, psStatString, psaDescription, true)
    
    --Tags.
    if(pzaTagList == nil) then
        zPrototype.zaTagList = {}
    else
        zPrototype.zaTagList = pzaTagList
    end
    
    --Register.
    self:fnRegisterEffect(psName .. ".Crit", gciEffect_Is_StatMod, zPrototypeCrit)
    
end