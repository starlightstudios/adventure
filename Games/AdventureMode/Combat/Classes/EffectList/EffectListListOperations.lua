-- |[ =============================== EffectList List Operations =============================== ]|
--Functions related to list queries like finding entries, adding them, or getting the list size.

-- |[ =============================== EffectList:fnEntryExists() =============================== ]|
--Returns a boolean true if the given name is on the list, false if not.
function EffectList:fnEntryExists(psUniqueName)
    
    -- |[Argument Check]|
    if(psUniqueName == nil) then return false end
    
    -- |[Scan List]|
    for i = 1, #self.zaList, 1 do
        if(self.zaList[i].sPrototypeName == psUniqueName) then
            return true
        end
    end
    
    -- |[Not Found]|
    return false
end

-- |[ =============================== EffectList:fnLocateEntry() =============================== ]|
--Returns the type/prototype/index associated with the given name, if it exists. Returns nil if not found. 
-- Fails silently, it is the caller's job to check for nil.
function EffectList:fnLocateEntry(psUniqueName)
    
    -- |[Argument Check]|
    if(psUniqueName == nil) then 
        io.write("EffectList:fnLocateEntry() - Warning, attempted to pass name that was nil. Called: " .. LM_GetCallStack(0) .. "\n")
        return nil
    end
    
    -- |[Scan]|
    for i = 1, #self.zaList, 1 do
        if(self.zaList[i].sPrototypeName == psUniqueName) then
            return self.zaList[i].iType, self.zaList[i].zPrototype, i
        end
    end

    -- |[Not Found]|
    return nil
end

-- |[ ============================== EffectList:fnRegisterEffect() ============================= ]|
--Registers a new effect prototype to the global list. Barks an error if the name is already in use 
-- and does not register.
function EffectList:fnRegisterEffect(psName, piType, pzPrototype) 
    
    -- |[Argument Check]|
    if(psName       == nil) then return end
    if(piType       == nil) then return end
    if(pzPrototype  == nil) then return end
    
    -- |[Unique Name Check]|
    --Run the locate function. If it returns a prototype, bark a warning and stop.
    local iCheckType, zCheckPrototype = self:fnLocateEntry(psName)
    if(iCheckType ~= nil) then
        io.write("EffectList:fnRegisterEffect() -  Warning, prototype name " .. psName .. " is already in use. Failing.\n")
        return
    end
    
    -- |[Register]|
    --Create a capsule.
    local zCapsule = {}
    zCapsule.sPrototypeName = psName
    zCapsule.iType = piType
    zCapsule.zPrototype = pzPrototype
    
    --Register the capsule.
    table.insert(self.zaList, zCapsule)
end
