-- |[ ============================= EffectList List Miscellaneous ============================== ]|
--Minor operations like changing icons after the fact.

-- |[ =============================== EffectList:fnChangeIcon() ================================ ]|
--Used to change the icon of an effect already registered. Used by Witch Hunter Izana, which uses
-- a different icon format than RoP does.
function EffectList:fnChangeIcon(psName, psIcon)
    
    -- |[ ========= Setup ========== ]|
    -- |[Argument Check]|
    if(psName == nil) then return end
    if(psIcon == nil) then return end
    
    -- |[ ===== Basic Version ====== ]|
    -- |[Locate]|
    --Override the icon. WHI uses different icon formats than RoP.
    local iType, zPrototype, iSlot = fnLocateEffectPrototype(psName)
    if(iType == nil) then
        io.write("EffectList:fnChangeIcon() - Warning. Unable to locate effect " .. psName .. " to change icon to " .. psIcon .. "\n")
        return
    end
    
    -- |[Override]|
    zPrototype.sFrontImg = psIcon
    zPrototype.sFrame    = "Null"
    zPrototype.sBacking  = "Null"
    
    --Warning if image is not found.
    if(DL_Exists(zPrototype.sFrontImg) == false and zPrototype.sFrontImg ~= "Null") then
        io.write("EffectList:fnChangeIcon() - Warning. Effect " .. psName .. " changed icon to " .. psIcon .. " which was not found. Failing.\n")
        return
    end
    
    -- |[ ====== Crit Version ====== ]|
    --As above, but attempts to locate the .Crit version of the same ability. If not found, this does not print a warning since
    -- there is no guarantee a crit version exists for all effects.
    
    -- |[Locate]|
    --Override the icon. WHI uses different icon formats than RoP.
    iType, zPrototype, iSlot = fnLocateEffectPrototype(psName .. ".Crit")
    if(iType == nil) then
        return
    end
    
    -- |[Override]|
    zPrototype.sFrontImg = psIcon
    zPrototype.sFrame    = "Null"
    zPrototype.sBacking  = "Null"
end
