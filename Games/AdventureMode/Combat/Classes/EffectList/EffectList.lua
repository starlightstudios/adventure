-- |[ ================================== EffectList Class File ================================= ]|
--Class file for EffectList. Call this once at initialization.

--Stores all effect prototypes on a global list, with queries available.

-- |[ ========= Globals and Constants ======== ]|
--Internal Constants
gciEffect_Is_StatMod = 0
gciEffect_Is_DoT = 1
gciEffect_Is_HoT = 2

-- |[Entry Prototype]|
--local zEffectListContainer = {}
--zEffectListContainer.sPrototypeName = "Dogs Are Great" --Must be a unique name
--zEffectListContainer.iType = gciEffect_Is_StatMod      --Uses the internal constants listed above
--zEffectListContainer.zPrototype = nil                  --In most effect scripts, this is what is compiled into gzaStatModStruct

-- |[ ============ Class Members ============= ]|
-- |[System]|
EffectList = {}
EffectList.__index = EffectList

-- |[Entries]|
EffectList.zaList = {}
    
-- |[ ============ Class Methods ============= ]|
--System

--List Operations
function EffectList:fnEntryExists(psUniqueName)                   end
function EffectList:fnLocateEntry(psUniqueName)                   end
function EffectList:fnRegisterEffect(psName, piType, pzPrototype) end

--Misc
function EffectList:fnChangeIcon(psName, psIcon) end

--Production
function EffectList:fnCreateBuffPrototype(psName, psDisplayName, psIcon, piTurns, psStatString, psaDescription, pzaTagList)                                                                               end
function EffectList:fnCreateDoTPrototype(psName, piTurns, piApplyStrength, pfDamPerTurn, piDamageType, psDisplayName, psIcon, piCritTurns, piCritApplyStrength)                                           end
function EffectList:fnCreateHoTPrototype(psName, psDisplayName, psIcon, pfHealBase, pfHealFactor, piTurns)                                                                                                end
function EffectList:fnCreatePassivePrototype(psName, psDisplayName, psIcon, psStatString, psaDescription, pzaTagList)                                                                                     end
function EffectList:fnCreateStatPrototype(pzEffectpackage, psName, pbIsBuff, psDisplayName, psIcon, piTurns, piApplyStrength, psStatString, psaDescription, pzaTagList, piCritTurns, piCritApplyStrength) end

--Quickset Functions
function EffectList:fnExtraDurationForOwner()        end
function EffectList:fnReduceDurationIfAlreadyActed() end

-- |[ ========= Call Implementations ========= ]|
LM_ExecuteScript(fnResolvePath() .. "EffectListListOperations.lua")
LM_ExecuteScript(fnResolvePath() .. "EffectListMisc.lua")
LM_ExecuteScript(fnResolvePath() .. "EffectListProduction.lua")
LM_ExecuteScript(fnResolvePath() .. "EffectListQuickset.lua")

