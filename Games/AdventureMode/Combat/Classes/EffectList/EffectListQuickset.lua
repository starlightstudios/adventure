-- |[ ============================== EffectList Quickset Functions ============================= ]|
--Functions that quickly set a single variable inside an effect, bundling together the search
-- and existence checks. These are meant to be used inside an ability file to edit a prototyped
-- effect with very common flag changes.

-- |[ ========================== EffectList:fnExtraDurationForOwner() ========================== ]|
--When an effect can apply to multiple targets and is spawned by non-free action, the acting entity
-- will immediately tick the effect duration by one since non-DoT effects tick at the end of the turn.
-- So if an effect lasted 3 turns, using it on an ally lets them act 3 times with the effect applied,
-- but the originator could only act 2 times with it applied.
--Setting the flag bExtraDurationForOwner on an effect causes it to gain +1 duration if the originator
-- of the effect is the target of the effect.
--If you're creating a self-only effect, such as Mei's Quick Strike, you can just add 1 to the duration
-- since we always know the originator is the target of the effect. If an effect is created by a
-- free-action then there is no need to call this as free actions don't tick effects.
function EffectList:fnExtraDurationForOwner(psPrototypeName)
    
    -- |[Argument Check]|
    if(psPrototypeName == nil) then return end
    
    -- |[Retrieve]|
    local iType, zPrototype, iSlot = EffectList:fnLocateEntry(psPrototypeName)
    
    -- |[Not Found]|
    if(zPrototype == nil) then
        io.write("EffectList:fnExtraDurationForOwner() - Warning, no prototype " .. psPrototypeName .. " found.\n")
        return
    end
    
    -- |[Set Flag]|
    zPrototype.bExtraDurationForOwner = true
end

-- |[ ====================== EffectList:fnReduceDurationIfAlreadyActed() ======================= ]|
--When an effect, such as a "Slow" or "Fast" effect, is applied, it ticks the turn after the entity
-- acts. However, Slow/Fast effects affect the turn order, meaning they will apply once each time
-- a turn elapses. Therefore, if a Slow is applied on an entity who has not acted yet this turn, they
-- will act once and tick the effect down by one, whereas if they have not acted, they will not, and
-- the effect will last an extra turn for them.
--Set this flag, and the duration will check if the entity has already acted and decrement it by 1
-- if they have, leveling the playing field for Slow/Fast effects.
function EffectList:fnReduceDurationIfAlreadyActed(psPrototypeName)
    
    -- |[Argument Check]|
    if(psPrototypeName == nil) then return end
    
    -- |[Retrieve]|
    local iType, zPrototype, iSlot = EffectList:fnLocateEntry(psPrototypeName)
    
    -- |[Not Found]|
    if(zPrototype == nil) then
        io.write("EffectList:fnReduceDurationIfAlreadyActed() - Warning, no prototype " .. psPrototypeName .. " found.\n")
        return
    end
    
    -- |[Set Flag]|
    zPrototype.bLowerDurationIfAlreadyActed = true
end