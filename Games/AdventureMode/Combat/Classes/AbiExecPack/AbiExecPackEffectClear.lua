-- |[ ================================ AbiExecPack Effect Clear ================================ ]|
--Function for removing effects after ability execution. Effects are removed by tag. This needs
-- to be incorporated more directly into automation, for now it is a static method.

-- |[ ============================ AbiExecPack:fnListEffectsByTag() ============================ ]|
--Scans across all effects the reference a given target by its ID. The provided tag array should 
-- be an array of strings with the tag names in each slot.
--Returns a list of IDs of all effects found. Can legally return an empty list.
function AbiExecPack:fnListEffectsByTag(piTargetID, psaTagNames)
    
    -- |[Argument Check]|
    if(piTargetID     == nil) then return {} end
    if(psaTagNames    == nil) then return {} end
    
    --If the tag list has no entries then stop.
    if(#psaTagNames < 1) then return {} end
    
    -- |[Setup]|
    --Create a list of the IDs of all effects that match the tags.
    local iaEffectIDList = {}
    
    -- |[Initial Scan]|
    --"Store Effects Referencing" will create an internal list in the C++ state that can be queried.
    -- Iterate across them and place their IDs on the effect list.
    local iEffectsTotal = AdvCombat_SetProperty("Store Effects Referencing", piTargetID)
    for i = 0, iEffectsTotal-1, 1 do
        AdvCombat_SetProperty("Push Temp Effects", i)
        
            --Scan across the tag list. If any one tag matches, flag it.
            for p = 1, #psaTagNames, 1 do
        
                --Get how many instances of the tag exist.
                local iCount = AdvCombatEffect_GetProperty("Get Tag Count", psaTagNames[p])
        
                --If any one exists, add this to the list and stop checking tags.
                if(iCount > 0) then
                    table.insert(iaEffectIDList, RO_GetID())
                    break
                end
            end
        DL_PopActiveObject()
    end
    
    -- |[Finish Up]|
    --Clean up, removing the effects from the C++ state.
    AdvCombat_SetProperty("Clear Temp Effects")
    
    --Return the list.
    return iaEffectIDList
end

-- |[ ========================= AbiExecPack:fnRemoveEffectsSilently() ========================== ]|
--Given a list of effect IDs, removes them with an application package silently with no animations.
function AbiExecPack:fnRemoveEffectsSilently(piOriginatorID, piTargetID, piaEffectIDs)
    
    -- |[Argument Check]|
    if(piTargetID   == nil) then return end
    if(piaEffectIDs == nil) then return end
    
    --If the list has no entries then stop.
    if(#piaEffectIDs < 1) then return end
    
    -- |[Removal]|
    --Iterate across the list and remove all of the tags.
    for i = 1, #piaEffectIDs, 1 do
        AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, giStoredTimer, "Remove Effect|" .. piaEffectIDs[i])
    end
    
    --Do not advance the timer as this is a silent removal.
end

-- |[ ========================= AbiExecPack:fnRemoveEffectsNormally() ========================== ]|
--Given a list of effect IDs, removes them with an application package with a standard animation.
function AbiExecPack:fnRemoveEffectsNormally(piOriginatorID, piTargetID, piaEffectIDs, psSoundEffect)
    
    -- |[Argument Check]|
    if(piTargetID   == nil) then return end
    if(piaEffectIDs == nil) then return end
    
    --If the list has no entries then stop.
    if(#piaEffectIDs < 1) then return end
    
    -- |[Removal]|
    --Play the healing animation and sound effect.
    local iAnimTicks = fnGetAnimationTiming("Healing")
    AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, giStoredTimer, "Play Sound|" .. psSoundEffect)
    AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, giStoredTimer, "Create Animation|Healing|AttackAnim0")
    giStoredTimer = giStoredTimer + iAnimTicks
    
    --Iterate across the list and remove all of the tags.
    AdvCombat_SetProperty("Register Application Pack", piOriginatorID, iTargetUniqueID, giStoredTimer, "Text|Cured!|Color:Green")
    for i = 1, #piaEffectIDs, 1 do
        AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, giStoredTimer, "Remove Effect|" .. piaEffectIDs[i])
    end
    giStoredTimer = giStoredTimer + gciApplication_TextTicks
    
end

-- |[ =========================== AbiExecPack:fnAutoRemoveEffects() ============================ ]|
--Standard effect removal function. Removes effects that match the given tag list, and plays the normal
-- healing sound and animations.
function AbiExecPack:fnAutoRemoveEffects(piOriginatorID, piTargetID, psaTagNames)
    
    -- |[Argument Check]|
    if(piOriginatorID == nil) then return end
    if(piTargetID     == nil) then return end
    if(psaTagNames    == nil) then return end
    
    -- |[Call]|
    --Create a list of matching effect IDs.
    local iaEffectIDs = self:fnListEffectsByTag(piTargetID, psaTagNames)
    io.write("Removing " .. #iaEffectIDs .. "\n")
    
    --Remove the effects with sound/animation.
    self:fnRemoveEffectsNormally(piOriginatorID, piTargetID, iaEffectIDs, "Combat\\|Heal")
end