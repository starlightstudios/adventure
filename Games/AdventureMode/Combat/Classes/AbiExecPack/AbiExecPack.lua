-- |[ =============================== AbiExecPack Class File ============================== ]|
--Class file for AbiExecPack. Call this once at initialization.

--An AbiExecPack stores execution variables for an ability, such as what damage it does, how
-- accurate it is, animations, healing, and so on.

-- |[ ============ Class Members ============= ]|
-- |[System]|
AbiExecPack = {}
AbiExecPack.__index = AbiExecPack
AbiExecPack.iOriginatorID = 0                             --Sets when execution to track who is using the ability. Changes if shared.

-- |[Special]|
AbiExecPack.bCauseBlackFlash = false                      --Causes a black flash. Used when non-player entities attack, or player entities counter-attack.
AbiExecPack.sShowTitle = "Null"                           --Animates a title. Typically used by enemies to indicate they are using a special attack. "Null" shows nothing.
AbiExecPack.bRunIfNotEquipped = false                     --Ability runs even if not on the equipped ability grid.
AbiExecPack.bOnlyRunIfUnlocked = false                    --If bRunIfNotEquipped is true, then this only runs if the ability is unlocked. Used for Permanent Passives.

-- |[Flags]|
--Normal flags.
AbiExecPack.bDoesNoDamage = false                         --Does not compute damage.
AbiExecPack.bMandateDamage = false                        --Uses a script-resolved damage value and bypasses all resistence checks.
AbiExecPack.bNeverGlances = false                         --Will hit or miss only, no glances.
AbiExecPack.bDoesNoStun = false                           --Does not inflict stun, even if stun is set or on a crit.
AbiExecPack.bDoesNotAnimate = false                       --Does not show combat animation when firing.
AbiExecPack.bDoesNotAnimateDamage = false                 --Does not show damage numbers, glances, play SFX.
AbiExecPack.bAlwaysHits = false                           --Always hits regardless of accuracy roll.
AbiExecPack.bAlwaysCrits = false                          --Always crits regardless of accuracy roll.
AbiExecPack.bNeverCrits = false                           --Does not crit regardless of accuracy roll.
AbiExecPack.bEffectAlwaysApplies = false                  --Effect bypasses roll and always applies. Used for buffs.
AbiExecPack.bTargetCanBeKOd = false                       --Allows target to be KOd, otherwise does nothing

--Miss/Crit Threshold.
AbiExecPack.iMissThreshold = 0                            --Accuracy roll failed to miss
AbiExecPack.iCritThreshold = 100                          --Accuracy roll needed to crit

-- |[Damage Flags]|
--Damage, damage type, scatter range.
AbiExecPack.bUseWeapon = false                            --If true, uses the attacker's weapon to select damage type.
AbiExecPack.bDealsOneDamageMinimum = true                 --If true, and damage computes to 0, deals 1 damage.
AbiExecPack.iDamageType = gciDamageType_Slashing          --Damage type.
AbiExecPack.fDamageFactor = 1.00                          --Factor multiplied by power to deal damage.
AbiExecPack.bBypassProtection = false                     --Set to true to ignore protection altogether.
AbiExecPack.iPenetration = 0                              --Reduces protection by this value, cannot go below zero.
AbiExecPack.fCriticalFactor = 1.25                        --Multiplies by damage factor on a crit.
AbiExecPack.iCriticalStunDamage = 25                      --How much stun is inflicted on a crit.
AbiExecPack.iScatterRange = 15                            --Percentage variance in both directions to final damage.
AbiExecPack.bDamageBenefitsFromItemPower = false          --Adds item power tags to damage factor.

--Only used if bMandateDamage is true
AbiExecPack.iMandatedDamage = 0                           --Amount of damage to deal, if bMandateDamage is true. Can be zero!

-- |[Healing Flags]|
--Factor, percent, base.
AbiExecPack.iHealingFinal   = 0                           --Internal
AbiExecPack.iHealingBase    = 0                           --Fixed healing value
AbiExecPack.fHealingFactor  = 0.0                         --Multiplied by attack power
AbiExecPack.fHealingPercent = 0.0                         --Percent of max HP
AbiExecPack.bHealingBenefitsFromItemPower = false         --Adds item power tags to healing factor.

-- |[Self-Healing Flags]|
--Heals the user, not the target. If target and user are the same, adds them.
AbiExecPack.iSelfHealingFinal = 0                         --Internal
AbiExecPack.iSelfHealingBase = 0                          --Fixed self-healing value
AbiExecPack.fSelfHealingPercent = 0.0                     --Percent of user's max health
AbiExecPack.fSelfHealingFactor = 0.0                      --Percent of user's attack power

-- |[Shields Flags]|
--By default, overwrites other shields. Note that shields are not always effects.
AbiExecPack.iShieldsFinal  = 0                            --Internal
AbiExecPack.iShieldsBase   = 0                            --Fixed shield value
AbiExecPack.fShieldsFactor = 0.0                          --Multiplied by attack power

-- |[MP Generation]|
--Adds to user's MP pool.
AbiExecPack.iMPGeneration = 0                             --MP added to target's pool when ability is used.

--MP added to target's pool.
AbiExecPack.iMPRestore = 0                                --MP added to user's pool when ability is used.

-- |[Lifesteal]|
--Adds to self-healing if both are present. Attack must deal damage.
AbiExecPack.fLifestealFactor = 0.0                        --Percentage of damage dealt healed to caller.

-- |[Stun]|
--How much stun is inflicted.
AbiExecPack.iBaseStunDamage = 0                           --Amount of stun damage inflicted on a hit/glance/crit.

-- |[Job Change]|
--String indicating job change on use.
AbiExecPack.sChangeJobTo = "Null"                         --If not "Null", changes the user's job.
AbiExecPack.sCannotChangeJobIfInForm = "Null"             --If not "Null", then blocks changing the user's job if they are in this job.
AbiExecPack.sJobChangeCheckVariable = "Null"              --DLPath of the variable queried for the above redundancy check.

-- |[Chaser Modules]|
--Variable number of chaser modules that activate when certain DoT tags present.
AbiExecPack.bHasChaserModules = false
AbiExecPack.zaChaserModules = {}
AbiExecPack.iChaserModulesTotal = 0
--AbiExecPack.zaChaserModules[1] = {}
--AbiExecPack.zaChaserModules[1].bConsumeEffect = true           --If true, DoT expires when attack lands
--AbiExecPack.zaChaserModules[1].sDoTTag = "Slashing DoT"        --What tag to look for on effects.
--AbiExecPack.zaChaserModules[1].fDamageFactorChangePerDoT = 1.0 --Added to the damage module's factor for each effect present.
--AbiExecPack.zaChaserModules[1].fDamageRemainingFactor = 1.0    --Percentage of remaining DoT damage to be added.

-- |[Tags]|
--Tag storage. Tags are formatted {{"TagA", iTagNumber}, {"TagB", iTagNumber}, etc}. These tags can be used to adjust execution behavior.
AbiExecPack.zaTags = {}

-- |[Summoning]|
AbiExecPack.saSummonHandlerScripts = {}
AbiExecPack.saSummonHandlerNames = {}

-- |[Additional Paths]|
AbiExecPack.saExecPaths = {}
AbiExecPack.iaExecCodes = {}

-- |[Execution Functions]|
--These functions are defined below. Replace them with a new function to change ability behavior.
AbiExecPack.fnAbilityAnimate   = fnDefaultAbilityAnimate
AbiExecPack.fnDamageAnimate    = fnDefaultDamageAnimate
AbiExecPack.fnInfluenceAnimate = fnDefaultInfluenceAnimate
AbiExecPack.fnHealingAnimate   = fnDefaultHealingAnimate
AbiExecPack.fnShieldsAnimate   = fnDefaultShieldsAnimate
AbiExecPack.fnMPGainAnimate    = fnDefaultMPAnimate
AbiExecPack.fnStunAnimate      = fnDefaultStunAnimate
AbiExecPack.fnJobAnimate       = fnDefaultJobAnimate

--Additional Animations. List may be nil by default.
AbiExecPack.zaAdditionalAnimations = {}

-- |[Default Animations]|
--If no matching type pack is found, the defaults are used.
AbiExecPack.sAttackAnimation = gsDefaultAttackAnimation
AbiExecPack.sAttackSound     = gsDefaultAttackSound
AbiExecPack.sCriticalSound   = gsDefaultCriticalSound

--Used if/when voice scripts are in play.
AbiExecPack.sVoiceData = nil

-- |[Timer Modifications]|
--Note: These get copied into the gzAbiPack (type ExecStatPack) when an ability is executed, and those copies
-- should be edited, not the ones in the original package. This is because a script may fire and modify the
-- timer factors during execution. The basic values should remain as they are once set in the prototype.
AbiExecPack.zaTimerFactors = {}
AbiExecPack.zaTimerFactors.fStaggerFactor      = 1.0
AbiExecPack.zaTimerFactors.fBlackFlash         = 1.0
AbiExecPack.zaTimerFactors.fVoiceCalloutPre    = 1.0
AbiExecPack.zaTimerFactors.fAnimation          = 1.0
AbiExecPack.zaTimerFactors.fVoiceCalloutPost   = 1.0
AbiExecPack.zaTimerFactors.fText               = 1.0
AbiExecPack.zaTimerFactors.fDamage             = 1.0
AbiExecPack.zaTimerFactors.fVoiceCalloutDamage = 1.0
AbiExecPack.zaTimerFactors.fHealing            = 1.0
AbiExecPack.zaTimerFactors.fShields            = 1.0
AbiExecPack.zaTimerFactors.fMPGeneration       = 1.0
AbiExecPack.zaTimerFactors.fSelfHealing        = 1.0
AbiExecPack.zaTimerFactors.fMPRestore          = 1.0
AbiExecPack.zaTimerFactors.fStun               = 1.0
AbiExecPack.zaTimerFactors.fEffect             = 1.0
AbiExecPack.zaTimerFactors.fJobChange          = 1.0
    
-- |[ ============ Class Methods ============= ]|
--System
function AbiExecPack:new(psType) end

--Effect Clear
function AbiExecPack:fnListEffectsByTag(piTargetID, psaTagNames)                                      end
function AbiExecPack:fnRemoveEffectsSilently(piOriginatorID, piTargetID, piaEffectIDs)                end
function AbiExecPack:fnRemoveEffectsNormally(piOriginatorID, piTargetID, piaEffectIDs, psSoundEffect) end
function AbiExecPack:fnAutoRemoveEffects(piOriginatorID, piTargetID, psaTagNames)                     end

--Major Setters
function AbiExecPack:fnSetAsBuff()                                                            end
function AbiExecPack:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)             end
function AbiExecPack:fnSetAsDebuff()                                                          end
function AbiExecPack:fnSetAsHealing(piHealingBase, pfHealingScaleFactor, pfHealingPctOfHPMax) end
function AbiExecPack:fnSetAsJobChange(psJobName, psExclusionName, psVariablePath)             end
function AbiExecPack:fnSetAsMPRestore(piMPRestore)                                            end
function AbiExecPack:fnSetAsShield(piShieldBase, pfShieldScaleFactor)                         end
function AbiExecPack:fnMarkEnemySkill(psOverrideName)                                         end

--Minor Setters
function AbiExecPack:fnAddStun(piStun)                                                                   end
function AbiExecPack:fnAddLifesteal(pfStealPercent)                                                      end
function AbiExecPack:fnAddSelfHeal(piHealingBase, pfHealingScaleFactor, pfHealPctOfMaxHP)                end
function AbiExecPack:fnAddAnimation(psAnimationName, piTimerOffset, pfXOffset, pfYOffset)                end
function AbiExecPack:fnAddChaser(pbConsumeEffect, psDoTTag, pfDamageChangePerDoT, pfDamageConsumeFactor) end
function AbiExecPack:fnAddSummon(psEnemyName, psOverrideAutoHandler)                                     end
function AbiExecPack:fnAlwaysHit()                                                                       end
function AbiExecPack:fnAlwaysCrit()                                                                      end

-- |[ ========== Class Constructor =========== ]|
--Expects the rough "type" for animations to be passed in. Animations are things like Pierce, Bleed, 
-- Terrify, Protect, etc. If nothing is passed in, no override is set.
--"Weapon" is a special case that flags the ability to resolve the weapon type.
function AbiExecPack:new(psType)
    
    -- |[ ========= Setup ========== ]|
    -- |[Argument Check]|
    if(psType == nil) then psType = "Default" end
    
    -- |[Create]|
    local zObject = {}
    setmetatable(zObject, AbiExecPack)
    
    -- |[ ======== Members ========= ]|
    --Reset local lists so they don't use the global class listings.
    zObject.zaChaserModules        = {}
    zObject.zaTags                 = {}
    zObject.saSummonHandlerScripts = {}
    zObject.saSummonHandlerNames   = {}
    zObject.saExecPaths            = {}
    zObject.iaExecCodes            = {}
    zObject.zaAdditionalAnimations = {}
    
    --Re-hash the functions as they may have changed since the class was initialized.
    zObject.fnAbilityAnimate   = fnDefaultAbilityAnimate
    zObject.fnDamageAnimate    = fnDefaultDamageAnimate
    zObject.fnInfluenceAnimate = fnDefaultInfluenceAnimate
    zObject.fnHealingAnimate   = fnDefaultHealingAnimate
    zObject.fnShieldsAnimate   = fnDefaultShieldsAnimate
    zObject.fnMPGainAnimate    = fnDefaultMPAnimate
    zObject.fnStunAnimate      = fnDefaultStunAnimate
    zObject.fnJobAnimate       = fnDefaultJobAnimate
    
    --Timers.
    zObject.zaTimerFactors = {}
    zObject.zaTimerFactors.fStaggerFactor      = 1.0
    zObject.zaTimerFactors.fBlackFlash         = 1.0
    zObject.zaTimerFactors.fVoiceCalloutPre    = 1.0
    zObject.zaTimerFactors.fAnimation          = 1.0
    zObject.zaTimerFactors.fVoiceCalloutPost   = 1.0
    zObject.zaTimerFactors.fText               = 1.0
    zObject.zaTimerFactors.fDamage             = 1.0
    zObject.zaTimerFactors.fVoiceCalloutDamage = 1.0
    zObject.zaTimerFactors.fHealing            = 1.0
    zObject.zaTimerFactors.fShields            = 1.0
    zObject.zaTimerFactors.fMPGeneration       = 1.0
    zObject.zaTimerFactors.fSelfHealing        = 1.0
    zObject.zaTimerFactors.fMPRestore          = 1.0
    zObject.zaTimerFactors.fStun               = 1.0
    zObject.zaTimerFactors.fEffect             = 1.0
    zObject.zaTimerFactors.fJobChange          = 1.0

    -- |[ ======= Processing ======= ]|
    --Resolve which type of animation we're using, if any. If it's "Default" do nothing.
    if(psType == "Default") then
        return zObject
    end
    
    --Does not populate since the weapon's type will override this later.
    if(psType == "Weapon") then
        zObject.bUseWeapon = true
        return zObject
    end
    
    --If the type is "Null" then no animation is present.
    if(psType == "Null") then
        zObject.sAttackAnimation = "Null"
        zObject.sAttackSound     = "Null"
        zObject.sCriticalSound   = "Null"
        return zObject
    end
    
    --Otherwise, iterate across the array of types. If the type is found, set it.
    for i = 1, #gzaAbiPacks, 1 do
        if(gzaAbiPacks[i].sLookup == psType) then
            zObject.sAttackAnimation = gzaAbiPacks[i].sAttackAnim
            zObject.sAttackSound     = gzaAbiPacks[i].sAttackSound
            zObject.sCriticalSound   = gzaAbiPacks[i].sCriticalSound
            return zObject
        end
    end
    
    --Error case: Unable to find type.
    io.write("AbiExecPack:new() - Warning, unable to find type " .. psType .. " in animation list. Animation list has " .. #gzaAbiPacks .. "entries.\n")
    return zObject
end

-- |[ ========= Call Implementations ========= ]|
LM_ExecuteScript(fnResolvePath() .. "AbiExecPackAnimations.lua")
LM_ExecuteScript(fnResolvePath() .. "AbiExecPackEffectClear.lua")
LM_ExecuteScript(fnResolvePath() .. "AbiExecPackMajorSetter.lua")
LM_ExecuteScript(fnResolvePath() .. "AbiExecPackMinorSetter.lua")




-- |[ ============= Sample Code ============== ]|
-- Sample
--[=[

--Function
fnAddAnimationPackage(zAbiStruct.zExecutionAbiPackage, "Claw Slash", 0, 0.0, 0.0)

--Adding even more using standardized offsets.
fnAddAnimationPackageAuto(zAbiStruct.zExecutionAbiPackage, "Buff Defense", 1)
fnAddAnimationPackageAuto(zAbiStruct.zExecutionAbiPackage, "Buff Resists", 2)
fnAddAnimationPackageAuto(zAbiStruct.zExecutionAbiPackage, "Buff Power",   3)

--Direct Addition
zaPackage.zaAdditionalAnimations = {}
zaPackage.zaAdditionalAnimations[1] = {}
zaPackage.zaAdditionalAnimations[1].iTimerOffset = 0
zaPackage.zaAdditionalAnimations[1].sAnimName = "Accuracy"
zaPackage.zaAdditionalAnimations[1].fXOffset = 0.0
zaPackage.zaAdditionalAnimations[1].fYOffset = 0.0
]=]
