-- |[ ================================= AbiExecPack Animations ================================= ]|
--Because the hasher does not re-resolve functions on metatables, these need to be declared before
-- they are put in the Ability Prototype metatable. At some point these will be put directly on the
-- metatable

-- |[ =============================== fnCreateAnimationPackage() =============================== ]|
--Creates and returns an animation package.
function fnCreateAnimationPackage(psAnimationName, piTimerOffset, pfXOffset, pfYOffset)
    
    -- |[Argument Check]|
    if(psAnimationName == nil) then return end
    if(piTimerOffset   == nil) then return end
    if(pfXOffset       == nil) then pfXOffset = 0.0 end
    if(pfYOffset       == nil) then pfYOffset = 0.0 end
    
    -- |[Create]|
    local zAnimationStruct = {}
    zAnimationStruct.iTimerOffset = piTimerOffset
    zAnimationStruct.sAnimName    = psAnimationName
    zAnimationStruct.fXOffset     = pfXOffset
    zAnimationStruct.fYOffset     = pfYOffset
    
    -- |[Finish Up]|
    return zAnimationStruct
end

-- |[ ============================= fnCreateAnimationPackageAuto() ============================= ]|
--Overload of above, using the standardized gfaAnimOffsets array to specify the position and offsets. The zeroth index is zeroes for all fields.
function fnCreateAnimationPackageAuto(psAnimationName, piIndex)

    -- |[Argument Check]|
    if(psAnimationName == nil) then return end
    if(piIndex         == nil) then return end
    
    -- |[Zero Index]|
    --Use zero for all offsets.
    if(piIndex < 1) then
        return fnCreateAnimationPackage(psAnimationName, 0, 0.0, 0.0)
    end

    -- |[In Range]|
    --Index is within the range of presets.
    if(piIndex <= #gfaAnimOffsets) then
        return fnCreateAnimationPackage(psAnimationName, gfaAnimOffsets[piIndex].iTiming, gfaAnimOffsets[piIndex].fX, gfaAnimOffsets[piIndex].fY)
    end
    
    -- |[Out of Range]|
    --Returns all zeroes for safety.
    return fnCreateAnimationPackage(psAnimationName, 0, 0.0, 0.0)
end

-- |[ ================================= fnAddAnimationPackage() ================================ ]|
--Creates and adds an animation package to the provided ability package.
function fnAddAnimationPackage(pzAbiPackage, psAnimationName, piTimerOffset, pfXOffset, pfYOffset)

    -- |[Argument Check]|
    if(pzAbiPackage    == nil) then return end
    if(psAnimationName == nil) then return end
    if(piTimerOffset   == nil) then return end
    if(pfXOffset       == nil) then pfXOffset = 0.0 end
    if(pfYOffset       == nil) then pfYOffset = 0.0 end
    
    -- |[Create]|
    local zAnimationStruct = fnCreateAnimationPackage(psAnimationName, piTimerOffset, pfXOffset, pfYOffset)
    
    -- |[Finish Up]|
    table.insert(pzAbiPackage.zaAdditionalAnimations, zAnimationStruct)

end

-- |[ =============================== fnAddAnimationPackageAuto() ============================== ]|
--Overload of above, using the standardized gfaAnimOffsets array to specify the position and offsets. The zeroth index is zeroes for all fields.
function fnAddAnimationPackageAuto(pzAbiPackage, psAnimationName, piIndex)

    -- |[Argument Check]|
    if(pzAbiPackage    == nil) then return end
    if(psAnimationName == nil) then return end
    if(piIndex         == nil) then return end

    -- |[Zero Index]|
    --Use zero for all offsets.
    if(piIndex < 1) then
        fnAddAnimationPackage(pzAbiPackage, psAnimationName, 0, 0.0, 0.0)
        return
    end

    -- |[In Range]|
    --Index is within the range of presets.
    if(piIndex <= #gfaAnimOffsets) then
        fnAddAnimationPackage(pzAbiPackage, psAnimationName, gfaAnimOffsets[piIndex].iTiming, gfaAnimOffsets[piIndex].fX, gfaAnimOffsets[piIndex].fY)
        return
    end
    
    -- |[Error]|
    --Use zeroes.
    fnAddAnimationPackage(pzAbiPackage, psAnimationName, 0, 0.0, 0.0)
end

-- |[ ================================= Animation Application ================================== ]|
function fnDefaultAbilityAnimate(piTargetID, piTimer, pzaAbilityPackage, pbIsCritical)
    
    --Critical strikes change the sound effect.
    if(pbIsCritical == false) then
        if( pzaAbilityPackage.sAttackSound~= nil and  pzaAbilityPackage.sAttackSound ~= "Null") then
            AdvCombat_SetProperty("Register Application Pack", pzaAbilityPackage.iOriginatorID, piTargetID, piTimer, "Play Sound|" .. pzaAbilityPackage.sAttackSound)
        end
    else
        if(pzaAbilityPackage.sCriticalSound ~= nil and pzaAbilityPackage.sCriticalSound ~= "Null") then
            AdvCombat_SetProperty("Register Application Pack", pzaAbilityPackage.iOriginatorID, piTargetID, piTimer, "Play Sound|" .. pzaAbilityPackage.sCriticalSound)
        end
    end
    
    --Common.
    local iAnimTicks = 0
    if(pzaAbilityPackage.sAttackAnimation~= nil and pzaAbilityPackage.sAttackAnimation ~= "Null") then
        AdvCombat_SetProperty("Register Application Pack", pzaAbilityPackage.iOriginatorID, piTargetID, piTimer, "Create Animation|" .. pzaAbilityPackage.sAttackAnimation .. "|AttackAnim0")
        iAnimTicks = fnGetAnimationTiming(pzaAbilityPackage.sAttackAnimation)
    end
    
    --Handle timer.
    piTimer = piTimer + iAnimTicks
    return piTimer
end

-- |[ =================================== Damage Application =================================== ]|
function fnDefaultDamageAnimate(piOriginatorID, piTargetID, piTimer, iDamage, sOptionalDamageArgs)
    
    --sOptionalDamageArgs can legally be nil.
    local sDamageString = "Damage|" .. iDamage
    if(sOptionalDamageArgs ~= nil) then
        sDamageString = sDamageString .. "|" .. sOptionalDamageArgs
    end
    
    --If the damage was zero, show text instead:
    if(iDamage < 1) then
        AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, piTimer, "Text|0")
        
    --Deal damage.
    else
        AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, piTimer, sDamageString)
    end
    piTimer = piTimer + gciApplication_TextTicks
    
    return piTimer
end

-- |[ ================================= Influence Application ================================== ]|
function fnDefaultInfluenceAnimate(piOriginatorID, piTargetID, piTimer, piInfluence, sOptionalInfluenceArgs)
    
    --sOptionalDamageArgs can legally be nil.
    local sString = "Influence|" .. piInfluence
    if(sOptionalInfluenceArgs ~= nil) then
        sString = sDamageString .. "|" .. sOptionalInfluenceArgs
    end
    
    --If the influence was zero, do nothing:
    if(piInfluence < 1) then
        
    --Inflict.
    else
        AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, piTimer, sString)
    end
    piTimer = piTimer + gciApplication_TextTicks
    
    return piTimer
end

-- |[ ================================== Healing Application =================================== ]|
function fnDefaultHealingAnimate(piOriginatorID, piTargetID, piTimer, piHealing)
    
    --sOptionalDamageArgs can legally be nil.
    local sHealingString = "Healing|" .. piHealing
    
    --Apply.
    AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, piTimer, sHealingString)
    piTimer = piTimer + gciApplication_TextTicks
    
    return piTimer
end

-- |[ =================================== Shield Application =================================== ]|
function fnDefaultShieldsAnimate(piOriginatorID, piTargetID, piTimer, piShields)
    
    --sOptionalDamageArgs can legally be nil.
    local sShieldsString = "Shields|" .. piShields
    
    --Apply.
    AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, piTimer, sShieldsString)
    piTimer = piTimer + gciApplication_TextTicks

    return piTimer
end

-- |[ ===================================== MP Application ===================================== ]|
function fnDefaultMPAnimate(piOriginatorID, piTargetID, piTimer, piMPToGen)
    
    --sOptionalDamageArgs can legally be nil.
    local sMPGainString = "MPGain|" .. piMPToGen
    
    --Apply.
    AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, piTimer, sMPGainString)
    piTimer = piTimer + gciApplication_TextTicks
    
    return piTimer
end

-- |[ ==================================== Stun Application ==================================== ]|
--Note: The stun damage is not the amount received, it's the final value. Clamp it before you send it here.
-- Stun does not require time to animate.
--The actual application of stun takes no time, but the text does. If the start and end values are the same,
-- no animation will occur.
function fnDefaultStunAnimate(piOriginatorID, piTargetID, piTimer, piStunFinal)
    
    --Check the targets current stun. If it's the same as the destination stun, do nothing.
    AdvCombat_SetProperty("Push Entity By ID", piTargetID)
        local iStunCur = AdvCombatEntity_GetProperty("Stun")
    DL_PopActiveObject()
    if(iStunCur == piStunFinal) then return piTimer end
    
    --Immediately apply the stun change to the entity, but use a function that does not order it to change its display value.
    AdvCombat_SetProperty("Push Entity By ID", gzAbiPack.iTargetID)
        AdvCombatEntity_SetProperty("Stun No Display", gzAbiPack.iFinalStun)
    DL_PopActiveObject()
        
    --Now apply the stun display value change, so the value increments in time with the action.
    AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, piTimer, "Stun Display")
    AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, piTimer, "Text|Stun +" .. piStunFinal-iStunCur .. "|Color:Yellow")
    piTimer = piTimer + gciApplication_TextTicks
    return piTimer
end

-- |[ ================================= Job Change Application ================================= ]|
function fnDefaultJobAnimate(piOriginatorID, piTargetID, piTimer, sJobToChangeTo)
    
    --Flash to white.
    AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, piTimer, "FlashWhite")
    piTimer = piTimer + gciApplication_FlashWhiteTicks + gciApplication_FlashWhiteHold
    
    --Change jobs.
    AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, piTimer, "JobChange|" .. sJobToChangeTo)
    piTimer = piTimer + gciApplication_FlashWhiteHold
    
    --Undo the flash.
    AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, piTimer, "UnflashWhite")
    piTimer = piTimer + gciApplication_FlashWhiteTicks
    return piTimer
end

