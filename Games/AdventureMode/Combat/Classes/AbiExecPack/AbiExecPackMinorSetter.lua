-- |[ ================================ AbiExecPack Minor Setter ================================ ]|
--A "Minor Setter" is a function that adds properties to an already defined ability.


-- |[ ================================ AbiExecPack:fnAddStun() ================================= ]|
--Adds stun damage to whatever target is impacted.
function AbiExecPack:fnAddStun(piStun)
    self.iBaseStunDamage = piStun
end

-- |[ ============================== AbiExecPack:fnAddLifesteal() ============================== ]|
--Adds a lifesteal factor. The amount of damage dealt, multiplied by this factor, is healed.
function AbiExecPack:fnAddLifesteal(pfStealPercent)
    self.fLifestealFactor = pfStealPercent
end
    
-- |[ ============================== AbiExecPack:fnAddSelfHeal() =============================== ]|
--Sets an ability to self-heal. Can be used in addition to other calls, as self-healing can stack
-- with directed healing or directed damage.
function AbiExecPack:fnAddSelfHeal(piHealingBase, pfHealingScaleFactor, pfHealPctOfMaxHP)
    self.iSelfHealingBase = piHealingBase
    self.fSelfHealingPercent = pfHealPctOfMaxHP
    self.fSelfHealingFactor = pfHealingScaleFactor
end

-- |[ ============================== AbiExecPack:fnAddAnimation() ============================== ]|
--Adds an animation that plays after the primary animation, with offsets. X/Y offset are optional.
function AbiExecPack:fnAddAnimation(psAnimationName, piTimerOffset, pfXOffset, pfYOffset)

    -- |[Argument Check]|
    if(psAnimationName == nil) then return end
    if(piTimerOffset   == nil) then return end
    if(pfXOffset       == nil) then pfXOffset = 0.0 end
    if(pfYOffset       == nil) then pfYOffset = 0.0 end
    
    -- |[Create]|
    local zAnimationStruct = fnCreateAnimationPackage(psAnimationName, piTimerOffset, pfXOffset, pfYOffset)
    
    -- |[Finish Up]|
    table.insert(self.zaAdditionalAnimations, zAnimationStruct)
end

-- |[ =============================== AbiExecPack:fnAddChaser() ================================ ]|
--Adds a chaser package, which allows an ability to interact with damage-over-time effects. This can
-- include finishing them and dealing their damage, dealing more damage when one is present, and more!
function AbiExecPack:fnAddChaser(pbConsumeEffect, psDoTTag, pfDamageChangePerDoT, pfDamageConsumeFactor)
    
    -- |[Argument Check]|
    if(pbConsumeEffect       == nil) then return end
    if(psDoTTag              == nil) then return end
    if(pfDamageChangePerDoT  == nil) then return end
    if(pfDamageConsumeFactor == nil) then return end
    
    -- |[Flag]|
    --Toggle this flag on.
    self.bHasChaserModules = true
    
    -- |[Create]|
    --Create a chaser module and set its properties.
    local zChaser = {}
    zChaser.bConsumeEffect            = pbConsumeEffect
    zChaser.sDoTTag                   = psDoTTag
    zChaser.fDamageFactorChangePerDoT = pfDamageChangePerDoT
    zChaser.fDamageRemainingFactor    = pfDamageConsumeFactor
    
    --Register the module.
    table.insert(self.zaChaserModules, zChaser)
    
    --Mark how many chaser modules this ability has.
    self.iChaserModulesTotal = #self.zaChaserModules

end

-- |[ =============================== AbiExecPack:fnAddSummon() ================================ ]|
--Adds a summon to the ability, causing the named entity to spawn on the enemy team.
function AbiExecPack:fnAddSummon(psEnemyName, psOverrideAutoHandler)
    
    -- |[Argument Check]|
    if(psEnemyName == nil) then return end

    -- |[Append Enemy Name]|
    --Note: The enemy name may not actually be used if the auto-handler is overridden.
    table.insert(self.saSummonHandlerNames, psEnemyName)

    -- |[Use Auto]|
    --If an enemy handler script is not provided, use the auto-handler.
    if(psOverrideAutoHandler == nil) then
        table.insert(self.saSummonHandlerScripts, AdvCombat_GetProperty("Enemy Auto Handler"))
    
    --An explicit script is being called, which may or may not require the enemy name to be passed to it.
    else
        table.insert(self.saSummonHandlerScripts, psOverrideAutoHandler)
    end
end

-- |[ =============================== AbiExecPack:fnAlwaysHit() ================================ ]|
--Ability always hits and does not glance.
function AbiExecPack:fnAlwaysHit()
    self.bAlwaysHits   = true
    self.bNeverGlances = true
end

-- |[ =============================== AbiExecPack:fnAlwaysCrit() =============================== ]|
--Ability always hits, always crits, and implicitly does not glance.
function AbiExecPack:fnAlwaysCrit()
    self.bAlwaysCrits  = true
    self.bAlwaysHits   = true
    self.bNeverGlances = true
end
