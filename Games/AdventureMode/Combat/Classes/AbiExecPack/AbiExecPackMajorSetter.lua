-- |[ ================================ AbiExecPack Major Setter ================================ ]|
--A "Major Setter" is a function that determines the overall shape of an ability prototype, such
-- as being a Buff, Debuff, DoT, Damaging Attack, etc.

-- |[ =============================== AbiExecPack:fnSetAsBuff() ================================ ]|
--Sets flags such that this ability becomes a buff. Doesn't deal damage, effect always applies, etc.
function AbiExecPack:fnSetAsBuff()
    self.bDoesNoDamage        = true
    self.bDoesNoStun          = true
    self.bAlwaysHits          = true
    self.bNeverCrits          = true
    self.bEffectAlwaysApplies = true
end

-- |[ ============================== AbiExecPack:fnSetAsDamage() =============================== ]|
--Function called during ability setup. Sets flags normally associated with basic damage, such as
-- damage type, whether the weapon should be used, miss rate, and damage factor.
function AbiExecPack:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)

    -- |[Argument Check]|
    if(piDamageType    == nil) then return end
    if(piMissthreshold == nil) then return end
    if(pfDamageFactor  == nil) then return end

    -- |[Damage Type]|
    --If the user passes in gciDamageType_UseWeapon then toggle on the weapon damage flag and default
    -- the damage to Slashing. This will get overridden later.
    if(piDamageType == gciDamageType_UseWeapon) then
        self.bUseWeapon  = true
        self.iDamageType = gciDamageType_Slashing
    
    --User passed in the damage type directly.
    else
        self.bUseWeapon  = false
        self.iDamageType = piDamageType
    end

    -- |[Other Flags]|
    self.iMissThreshold = piMissthreshold
    self.fDamageFactor  = pfDamageFactor

end

-- |[ ============================== AbiExecPack:fnSetAsDebuff() =============================== ]|
--Function called during ability setup. Sets flags normally associated with debuffs. This is a 'strict'
-- handling of a debuff, in that it is expected to deal no damage. This also can't crit but you can
-- override that afterwards if desired.
function AbiExecPack:fnSetAsDebuff()
    self.bDoesNoDamage = true
    self.bNeverGlances = true
    self.bDoesNoStun = true
    self.bNeverCrits = true
end

-- |[ ============================== AbiExecPack:fnSetAsHealing() ============================== ]|
--Function called during ability setup. Sets flags normally associated with healing, including
-- base value and scaling.
function AbiExecPack:fnSetAsHealing(piHealingBase, pfHealingScaleFactor, pfHealingPctOfHPMax)
    
    -- |[Argument Check]|
    if(piHealingBase        == nil) then return end
    if(pfHealingScaleFactor == nil) then return end
    if(pfHealingPctOfHPMax  == nil) then return end
    
    -- |[Set Common Flags]|
    --All healing abilities toggle these flags on.
    self.bDoesNoDamage = true
    self.bDoesNoStun   = true
    self.bAlwaysHits   = true
    self.bNeverCrits   = true
    
    -- |[Set Variables]|
    --These variables determine how much the heal actually restores.
    self.iHealingBase    = piHealingBase
    self.fHealingFactor  = pfHealingScaleFactor
    self.fHealingPercent = pfHealingPctOfHPMax
end

-- |[ ============================= AbiExecPack:fnSetAsJobChange() ============================= ]|
--Function called during ability setup, flags this as a job-change ability. These change the user's
-- job and typically not a lot else.
function AbiExecPack:fnSetAsJobChange(psJobName, psExclusionName, psVariablePath)
    
    -- |[Argument Check]|
    if(psJobName == nil) then return end
    
    -- |[Common Flags]|
    self.bDoesNoDamage        = true
    self.bDoesNoStun          = true
    self.bDoesNotAnimate      = true
    self.bNeverGlances        = true
    self.bAlwaysHits          = true
    self.bNeverCrits          = true
    self.bEffectAlwaysApplies = true
    
    -- |[Job Destination]|
    self.sChangeJobTo = psJobName
    self.sCannotChangeJobIfInForm = "Null"
    self.sJobChangeCheckVariable = "Null"
    
    --If the exclusion name and variable are not nil, set them.
    if(psExclusionName ~= nil and psVariablePath ~= nil) then
        self.sCannotChangeJobIfInForm = psExclusionName
        self.sJobChangeCheckVariable = psVariablePath
    end
end

-- |[ ============================= AbiExecPack:fnSetAsMPRestore() ============================= ]|
--Sets an ability to restore MP.
function AbiExecPack:fnSetAsMPRestore(piMPRestore)
    
    -- |[Argument Check]|
    if(piMPRestore == nil) then return end
    
    -- |[Set Common Flags]|
    --All healing abilities toggle these flags on.
    self.bDoesNoDamage = true
    self.bDoesNoStun   = true
    self.bAlwaysHits   = true
    self.bNeverCrits   = true
    
    -- |[Set Variables]|
    --Variable for MP restore.
    self.iMPGeneration = piMPRestore
end

-- |[ ============================== AbiExecPack:fnSetAsShield() =============================== ]|
--Causes an ability to apply a shield.
function AbiExecPack:fnSetAsShield(piShieldBase, pfShieldScaleFactor)
    
    -- |[Argument Check]|
    if(piShieldBase        == nil) then return end
    if(pfShieldScaleFactor == nil) then return end
    
    -- |[Set Common Flags]|
    --All healing abilities toggle these flags on.
    self.bDoesNoDamage = true
    self.bDoesNoStun   = true
    self.bAlwaysHits   = true
    self.bNeverCrits   = true
    
    -- |[Set Variables]|
    --These variables determine how much the shield applies for.
    self.iShieldsBase   = piShieldBase
    self.fShieldsFactor = pfShieldScaleFactor
end

-- |[ ============================== AbiExecPack:fnAddSelfHeal() =============================== ]|
--Sets an ability to self-heal. Can be used in addition to other calls, as self-healing can stack
-- with directed healing or directed damage.
function AbiExecPack:fnAddSelfHeal(piHealingBase, pfHealingScaleFactor, pfHealPctOfMaxHP)
    self.iSelfHealingBase = piHealingBase
    self.fSelfHealingPercent = pfHealPctOfMaxHP
    self.fSelfHealingFactor = pfHealingScaleFactor
end

-- |[ ============================= AbiExecPack:fnMarkEnemySkill() ============================= ]|
--Marks this as an "Enemy" skill. Enemy skills cause a black flash in their user and show a title
-- so the player knows a special attack is being used. They also don't crit by default, as part of
-- a design decision in RoP.
function AbiExecPack:fnMarkEnemySkill(psOverrideName)
    
    -- |[Set Common Flags]|
    --Toggle flags on.
    self.bCauseBlackFlash = true
    self.sShowTitle = pzAbiStruct.sDisplayName
    
    --Enemy abilities do not crit.
    self.bNeverCrits = true
    
    -- |[Optional Override]|
    --This argument is optional. It can override the title shown. To disable the title, pass "" or "Null".
    if(psOverrideName == "") then
        self.sShowTitle = "Null"
    
    elseif(psOverrideName ~= nil) then
        self.sShowTitle = psOverrideName
    end
end
