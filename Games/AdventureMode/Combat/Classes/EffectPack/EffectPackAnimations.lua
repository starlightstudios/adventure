-- |[ ================================= EffectPack Animations ================================== ]|
--Animation functions for effects. Animations play typically when an effect is applied or resisted,
-- and can be anything but are usually text indicators.

-- |[ ============================== EffectPack:fnRegisterAnim() =============================== ]|
--Registers an animation to the animation list.
function EffectPack:fnRegisterAnim(psAnimName, piSlot)
    
    -- |[Argument Check]|
    if(psAnimName == nil) then return end
    if(piSlot     == nil) then return end
    
    -- |[Append]|
    table.insert(self.zaApplyAddAnims, fnCreateAnimationPackageAuto(psAnimName, piSlot))
end

-- |[ ============================ EffectPack:fnRegisterCritAnim() ============================= ]|
--Registers an animation to the critical animation list. As a convenience feature, clears the list if the
-- zeroth element was "NORMAL".
function EffectPack:fnRegisterCritAnim(psAnimName, piSlot)
    
    -- |[Argument Check]|
    if(psAnimName == nil) then return end
    if(piSlot     == nil) then return end
    
    -- |[Normal Check]|
    --"NORMAL" will clear the list.
    if(self.zaCritApplyAddAnims[1] == "NORMAL") then
        self.zaCritApplyAddAnims = {}
    end
    
    -- |[Append]|
    table.insert(sekf.zaCritApplyAddAnims, fnCreateAnimationPackageAuto(psAnimName, piSlot))
end

-- |[ ================================ fnDefaultEffectAnimate() ================================ ]|
--Applies the effect.
function fnDefaultEffectAnimate(piOriginatorID, piTargetID, piTimer, pzaEffectPackage, pbIsCritical)

    -- |[Argument Check]|
    if(piOriginatorID   == nil) then return end
    if(piTargetID       == nil) then return end
    if(piTimer          == nil) then return end
    if(pzaEffectPackage == nil) then return end
    if(pbIsCritical     == nil) then return end

    -- |[Basic Effect String]|
    --All information is passed via a string that stores the effect information, using "|" to denote optional arguments.
    local sEffectString = "Effect|"

    --Determine if we should use the effect, or the crit effect. Note that these can be the same path.
    if(pbIsCritical == false) then
        sEffectString = sEffectString .. pzaEffectPackage.sEffectPath
    else
        sEffectString = sEffectString .. pzaEffectPackage.sEffectCritPath
    end

    -- |[Optional Arguments]|
    --Add originator and effect severity.
    sEffectString = sEffectString .. "|Originator:" .. piOriginatorID
    sEffectString = sEffectString .. "|Severity:" .. pzaEffectPackage.fTagSeverityBonus
    
    --If the effect package uses a prototype, specify it here.
    if(pzaEffectPackage.sPrototypeName ~= nil) then
        sEffectString = sEffectString .. "|Prototype:" .. pzaEffectPackage.sPrototypeName
    end
    
    --Specify if this is a crit. This is only used when using prototypes, and if no crit version
    -- of the prototype is found then the base version is used.
    if(pbIsCritical == true) then
        sEffectString = sEffectString .. "|Critical"
    end

    -- |[Register the Application Pack]|
    --Send this to the C++ code.
    AdvCombat_SetProperty("Register Application Pack", pzaEffectPackage.iOriginatorID, piTargetID, piTimer, sEffectString)

    -- |[Resolve Animation Data]|
    --How many ticks to elapse. If none of the animations or text are set, no time is required.
    local iAnimTicks = 0

    --Resolve which set of strings to use. Crits can optionally use a different set of animations/sounds/etc.
    local sUseAnimation = "Null"
    local sUseSound     = "Null"
    local sUseText      = "Null"
    local sUseTextCol   = "Null"
    local zaUseAddAnims = {}

    --Normal effect.
    if(pbIsCritical == false) then
        sUseAnimation = pzaEffectPackage.sApplyAnimation
        sUseSound     = pzaEffectPackage.sApplySound
        sUseText      = pzaEffectPackage.sApplyText
        sUseTextCol   = pzaEffectPackage.sApplyTextCol
        zaUseAddAnims = pzaEffectPackage.zaApplyAddAnims

    --Critical strike.
    else

        --Critical. Resolve them normally:
        sUseAnimation = pzaEffectPackage.sCritApplyAnimation
        sUseSound     = pzaEffectPackage.sCritApplySound
        sUseText      = pzaEffectPackage.sCritApplyText
        sUseTextCol   = pzaEffectPackage.sCritApplyTextCol
        zaUseAddAnims = pzaEffectPackage.zaCritApplyAddAnims

        --If any of these are "NORMAL" then use the non-crit version.
        if(sUseAnimation == "NORMAL") then sUseAnimation = pzaEffectPackage.sApplyAnimation end
        if(sUseSound     == "NORMAL") then sUseSound     = pzaEffectPackage.sApplySound     end
        if(sUseText      == "NORMAL") then sUseText      = pzaEffectPackage.sApplyText      end
        if(sUseTextCol   == "NORMAL") then sUseTextCol   = pzaEffectPackage.sApplyTextCol   end
        
        --For the additional application set, if the zeroth entry is "NORMAL" then use the normal set.
        if(zaUseAddAnims ~= nil and zaUseAddAnims[1] == "NORMAL") then zaUseAddAnims = pzaEffectPackage.zaApplyAddAnims end

    end
    
    -- |[Animation]|
    --Animation.
    if(sUseAnimation ~= "Null") then
        AdvCombat_SetProperty("Register Application Pack", pzaEffectPackage.iOriginatorID, piTargetID, piTimer, "Create Animation|" .. sUseAnimation .. "|EffectAnim0")
        iAnimTicks = gciApplication_TextTicks
    end
    
    --Sound effect.
    if(sUseSound ~= "Null") then
        AdvCombat_SetProperty("Register Application Pack", pzaEffectPackage.iOriginatorID, piTargetID, piTimer, "Play Sound|" .. sUseSound)
    end
    
    --Text.
    if(sUseText ~= "Null" and sUseText ~= "") then
        AdvCombat_SetProperty("Register Application Pack", pzaEffectPackage.iOriginatorID, piTargetID, piTimer, "Text|" .. sUseText .. "|" .. sUseTextCol)
        iAnimTicks = gciApplication_TextTicks
    end
    
    --Additional animations. An optional list of animations, timer offsets, and position offsets. The timing is relative to the starting time of the animation.
    if(zaUseAddAnims ~= nil) then
        
        --Iterate across the animations and apply them.
        for i = 1, #zaUseAddAnims, 1 do
            
            --Variables
            local iTimerOffset = zaUseAddAnims[i].iTimerOffset
            local sAnimName    = zaUseAddAnims[i].sAnimName
            local fXOffset     = zaUseAddAnims[i].fXOffset
            local fYOffset     = zaUseAddAnims[i].fYOffset
            
            --String construction
            local sString = "Create Animation|" .. sAnimName .. "|AttackAnim" .. i
            
            --Optional argument:
            if(fXOffset ~= 0.0) then
                sString = sString .. "|OffX:" .. fXOffset
            end
            if(fYOffset ~= 0.0) then
                sString = sString .. "|OffY:" .. fYOffset
            end
            
            --Upload:
            AdvCombat_SetProperty("Register Application Pack", pzaEffectPackage.iOriginatorID, piTargetID, iAnimTicks + iTimerOffset, sString)
        end
    end
    
    -- |[Finish Up]|
    --Pass back the results.
    piTimer = piTimer + iAnimTicks
    return piTimer
end

-- |[ ================================ fnDefaultResistAnimate() ================================ ]|
--Resists the effect.
function fnDefaultResistAnimate(piOriginatorID, piTargetID, piTimer, pzaEffectPackage)

    --How many ticks to elapse. If none of the animations or text are set, no time is required.
    local iAnimTicks = 0
        
    --Animation.
    if(pzaEffectPackage.sResistAnimation ~= "Null") then
        AdvCombat_SetProperty("Register Application Pack", pzaEffectPackage.iOriginatorID, piTargetID, piTimer, "Create Animation|" .. pzaEffectPackage.sResistAnimation .. "|EffectAnim0")
        iAnimTicks = gciApplication_TextTicks
    end
    
    --Sound effect.
    if(pzaEffectPackage.sResistSound ~= "Null") then
        AdvCombat_SetProperty("Register Application Pack", pzaEffectPackage.iOriginatorID, piTargetID, piTimer, "Play Sound|" .. pzaEffectPackage.sResistSound)
    end
    
    --Text.
    if(pzaEffectPackage.sResistText ~= "Null") then
        AdvCombat_SetProperty("Register Application Pack", pzaEffectPackage.iOriginatorID, piTargetID, piTimer, "Text|" .. pzaEffectPackage.sResistText .. "|" .. pzaEffectPackage.sResistTextCol)
        iAnimTicks = gciApplication_TextTicks
    end
    
    --Finish up.
    piTimer = piTimer + iAnimTicks
    return piTimer
end

-- |[ ============================== fnDefaultApplyOrResistSwitch ============================== ]|
--Handles apply/resist for the effect.
function fnDefaultApplyOrResistSwitch(piOriginatorID, piTargetID, piTimer, pzaEffectPackage, pbApplied, pbIsCritical)
    if(pbApplied) then
        piTimer = pzaEffectPackage.fnApplyEffect(piOriginatorID, piTargetID, piTimer, pzaEffectPackage, pbIsCritical)
    else
        piTimer = pzaEffectPackage.fnResistEffect(piOriginatorID, piTargetID, piTimer, pzaEffectPackage)
    end
    return piTimer
end
