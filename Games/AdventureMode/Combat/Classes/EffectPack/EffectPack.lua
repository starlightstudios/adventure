-- |[ ================================= EffectPack Class File ================================== ]|
--Class file for EffectPack. Call this once at initialization.

--An EffectPack contains an effect that is applied, often with a strength, and having application
-- sounds, animations, and modifiers. This does not store the effect itself, which is a C++ object,
-- this is the combat application of the effect.

-- |[ ============ Class Members ============= ]|
-- |[System]|
EffectPack = {}
EffectPack.__index = EffectPack
EffectPack.iOriginatorID = 0                                 --Internal, set by program during execution.

--Name.
EffectPack.sDisplayName = "Display Name"                     --Display name in combat inspector.

--Flags.
EffectPack.bBenefitsFromItemPower = false                    --If true, the "Severity" flag will use item power tags.

--Paths.
EffectPack.sEffectPath     = "Null"                          --Effect script to apply on hit.
EffectPack.sEffectCritPath = "Null"                          --Effect script to apply on crit.

-- |[Application / Duration / Etc]|
EffectPack.bIsBuff                      = false              --If true, always applies regardless of strength.
EffectPack.iEffectStr                   = 0                  --If the ability hits, application power
EffectPack.iEffectCritStr               = 0                  --If the ability crits, application power
EffectPack.iEffectType              = gciDamageType_Slashing --Resistance type checked against for application
EffectPack.iDuration                    = -1                 --How many turns the effect lasts. -1 means it lasts until removed.
EffectPack.bRemoveOnKO                  = true               --If true, effect expires on a KO'd target. Passives often don't do this.
EffectPack.bLowerDurationIfAlreadyActed = false              --If true, -1 duration if the target of the effect has already acted this turn.
EffectPack.bExtraDurationForOwner       = false              --If true, +1 duration if the target of the effect is the acting entity. This allows self-buffs to last the correct duration.

-- |[Tags]|
EffectPack.saApplyTagBonus = {}                              --Any tags in this list apply a bonus to application chance for each tag found.
EffectPack.saApplyTagMalus = {}                              --Any tags in this list apply a penalty to application chance for each tag found.
EffectPack.fApplyFactor = 1                                  --The amount of bonus or penalty to application chance for each tag found, as a percent.
EffectPack.iTagApplyBonus = 0                                --Internal
EffectPack.saSeverityTagBonus = {}                           --Any tags in this list apply a bonus to severity for each tag found.
EffectPack.saSeverityTagMalus = {}                           --Any tags in this list apply a penalty to severity for each tag found.
EffectPack.fSeverityFactor = 0.01                            --The amount of bonus or penalty to severity for each tag found.
EffectPack.fTagSeverityBonus = 0                             --Internal

-- |[Display]|
EffectPack.sFrame    = "NULL"                                --DLPath to the image used for the frame in effect display
EffectPack.sFrontImg = "NULL"                                --DLPath to the image used for the front of the effect display
EffectPack.sBacking  = "NULL"                                --DLPath to the image used for the colored backing of the effect display

-- |[Animation]|
--Successfully Applied Effect
EffectPack.sApplyText      = ""                              --Generated text that appears. Can be left empty.
EffectPack.sApplyTextCol   = "Color:White"                   --Pregenerated color. See AdvCombatApplications.cc for a list.
EffectPack.sApplyAnimation = "Null"                          --Animation if the effect applies. Can be "Null".
EffectPack.sApplySound     = "Null"                          --Sound effect if the effect applies. Can be "Null". Example: "Combat\\|Impact_Debuff", "Combat\\|Buff"
EffectPack.zaApplyAddAnims = {}                              --Additional animations if the effect applies. See below for samples.

--Critical Applied Effect
EffectPack.sCritApplyText      = "NORMAL"                    --Generated text that appears on a critical application. Can be "NORMAL" which causes sApplyText to be used.
EffectPack.sCritApplyTextCol   = "NORMAL"                    --Pregenerated color. See AdvCombatApplications.cc for a list. Can be "NORMAL" which causes sApplyTextCol to be used.
EffectPack.sCritApplyAnimation = "NORMAL"                    --Animation if the effect applies. Can be "Null". Can also be "NORMAL" which causes the sApplyAnimation to be used.
EffectPack.sCritApplySound     = "NORMAL"                    --Sound effect if the effect applies. Can be "Null". Can also be "NORMAL" which causes the sApplySound to be used. 
EffectPack.zaCritApplyAddAnims = {"NORMAL"}                  --Additional animations if the effect crit-applies. See below for samples. If the zeroth entry is "NORMAL", uses the base list.

--Resisted Effect
EffectPack.sResistText      = "Resisted"                     --Generated text if the effect fails to apply. Can be left empty.
EffectPack.sResistTextCol   = "Color:White"                  --See above.
EffectPack.sResistAnimation = "Null"                         --See above.
EffectPack.sResistSound     = "Combat\\|AttackMiss"          --See above.
EffectPack.zaResistAddAnims = {}                             --See above.

-- |[Statmod Only]|
EffectPack.sStatString = {}                                  --A string of format "Stat1|Type|Amount Stat2|Type|Amount" that is parsed to get how this effect modifies stats.
EffectPack.saStrings = {}                                    --Strings for combat inspector. [1] is the header's left side, [2] is the header's right side, all other lines are description box.
EffectPack.saStoredDescription = {}                          --Copy of description strings stored internally, used to recompute values if they change over the course of the effect.
EffectPack.sShortText = "Null"                               --Text that appears on the enemy in combat, can include icons.
EffectPack.zaTagList = {}                                    --List of tags that apply to the effect and, therefore, the target.

-- |[DoT Only]|
EffectPack.sIcon = "Unknown"                                 --Icon path of the ability that caused this DoT.
EffectPack.sDisplayOnApply = "No Effect String"              --Text that is displayed on effect application, such as "Bleeding!".
EffectPack.sAnimation = "Sword Slash"                        --Animation that plays when the target takes damage on the start of their turn.
EffectPack.sDamageTypeIcon = "Slashing"                      --Icon used for damage-over-time type indication.
EffectPack.sTypeTag = "Null"                                 --Indicates the type in a fashion that Chaser abilities can use to deal extra damage or consume effects.
EffectPack.iTypeAmount = 1                                   --How many of the sTypeTag tags to create, usually 1.
EffectPack.iDamageResistFlag = gciStatIndex_Resist_Slash     --The stat index used to check resistance to this DoT
EffectPack.iDamageFactorDescriptionFlag = gciDoTEven         --Flag that indicates how damage is distributed each turn, such as "even" or "back heavy".
EffectPack.faTurnFactors = {1.0}                             --Array that specifies how much damage occurs each turn multiplied by attack power.

-- |[Function Pointers]|
--Functions called by this effect on application/resist/etc. You can override these to change the application behavior.
-- The default versions are below in this file.
EffectPack.fnApplyEffect  = fnDefaultEffectAnimate
EffectPack.fnResistEffect = fnDefaultResistAnimate
EffectPack.fnHandler      = fnDefaultApplyOrResistSwitch

-- |[Prototype Usage]|
EffectPack.sPrototypeName = nil

-- |[ ============ Class Methods ============= ]|
--System
function EffectPack:new() end

--Animation
function EffectPack:fnRegisterAnim(psAnimName, piSlot)                                                                end
function EffectPack:fnRegisterCritAnim(psAnimName, piSlot)                                                            end
function fnDefaultEffectAnimate(piOriginatorID, piTargetID, piTimer, pzaEffectPackage, pbIsCritical)                  end
function fnDefaultResistAnimate(piOriginatorID, piTargetID, piTimer, pzaEffectPackage)                                end
function fnDefaultApplyOrResistSwitch(piOriginatorID, piTargetID, piTimer, pzaEffectPackage, pbApplied, pbIsCritical) end

-- |[ ========== Class Constructor =========== ]|
function EffectPack:new()
    
    -- |[ ========= Setup ========== ]|
    -- |[Create]|
    local zObject = {}
    setmetatable(zObject, EffectPack)
    
    -- |[ ======== Members ========= ]|
    --Reset local lists so they don't use the global class listings.
    zObject.saApplyTagBonus = {}
    zObject.saApplyTagMalus = {}
    zObject.saSeverityTagBonus = {}
    zObject.saSeverityTagMalus = {}
    zObject.zaApplyAddAnims = {}
    zObject.zaCritApplyAddAnims = {"NORMAL"}
    zObject.zaResistAddAnims = {}
        
    --Re-hash functions in case they changed between prototype and implementation.
    zObject.fnApplyEffect  = fnDefaultEffectAnimate
    zObject.fnResistEffect = fnDefaultResistAnimate
    zObject.fnHandler      = fnDefaultApplyOrResistSwitch
    
    -- |[ ======= Finish Up ======== ]|
    return zObject
end

-- |[ ========= Call Implementations ========= ]|
LM_ExecuteScript(fnResolvePath() .. "EffectPackAnimations.lua")


-- |[Additional Animation Samples]|
-- Sample additions to the animation list. Follows the same format as that used by ability packages.
--[=[

--Automated adder.
zAbiStruct.zExecutionEffPackage:fnRegisterAnim("Buff Defense", 0)

--Direct Addition.
zAbiStruct.zExecutionEffPackage.zaApplyAddAnims = {}
zAbiStruct.zExecutionEffPackage.zaApplyAddAnims[1] = {}
zAbiStruct.zExecutionEffPackage.zaApplyAddAnims[1].iTimerOffset = 0
zAbiStruct.zExecutionEffPackage.zaApplyAddAnims[1].sAnimName = "Accuracy"
zAbiStruct.zExecutionEffPackage.zaApplyAddAnims[1].fXOffset = 0.0
zAbiStruct.zExecutionEffPackage.zaApplyAddAnims[1].fYOffset = 0.0
]=]
