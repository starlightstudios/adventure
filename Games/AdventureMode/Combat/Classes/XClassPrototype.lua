--Replace [CLASSPROTOTYPE] with the name of the class
--Balance the headers
--Add variables and descriptions
--Make sure any local tables are reinitialized in the constructor
--Create the constructor
--Make sure to call the function scripts!
--Delete these instructions

-- |[ =============================== [CLASSPROTOTYPE] Class File ============================== ]|
--Class file for [CLASSPROTOTYPE]. Call this once at initialization.

--[DESCRIPTION HERE]

-- |[ ========== Globals/Constants =========== ]|
-- |[ ============ Class Members ============= ]|
-- |[System]|
[CLASSPROTOTYPE] = {}
[CLASSPROTOTYPE].__index = [CLASSPROTOTYPE]

-- |[Header]|
[CLASSPROTOTYPE].bVariable = false                          --Description of variable's purpose
    
-- |[ ============ Class Methods ============= ]|
--System
function [CLASSPROTOTYPE]:new(psType) end

--Name Of File
function [CLASSPROTOTYPE]:fnFunctionName() end

-- |[ ========== Class Constructor =========== ]|
--Expects the rough "type" for animations to be passed in. Animations are things like Pierce, Bleed, 
-- Terrify, Protect, etc. If nothing is passed in, no override is set.
--"Weapon" is a special case that flags the ability to resolve the weapon type.
function [CLASSPROTOTYPE]:new()
    
    -- |[ ========= Setup ========== ]|
    -- |[Argument Check]|
    -- |[Create]|
    local zObject = {}
    setmetatable(zObject, [CLASSPROTOTYPE])
    
    -- |[ ======== Members ========= ]|
    --Reset local lists so they don't use the global class listings.
    --[CLASSPROTOTYPE].zaTable = {}

    -- |[ ======= Processing ======= ]|
    --Any additional processing for arguments is done here.
    return zObject
end

-- |[ ========= Call Implementations ========= ]|
LM_ExecuteScript(fnResolvePath() .. "ClassPrototypeFunctions.lua")
