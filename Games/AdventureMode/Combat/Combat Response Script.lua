-- |[ ================================= Combat Response Script ================================= ]|
--This is a catch-all script called at various points during combat in order to modify variables.
-- For example, it is responsible for resetting variables to their defaults at the start of a turn.
--The script uses an argument to determine what the call type is.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Common Flags]|
local bDiagnostics = Debug_GetFlag("Combat: Response Script")

-- |[ ====================================== Reinitialize ====================================== ]|
--Called after combat has called Reinitialize() in the C++ code. After all C++ variables are reset
-- to their defaults, this is called to reset any Lua variables.
if(iSwitchType == gciCombatResponse_BeginCombat) then
    
    -- |[Diagnostics]|
    Debug_PushPrint(bDiagnostics, "Combat Response Script: Begin Combat: Start.\n")
    
    -- |[Order Global Resets]|
    --The AIString runs this function to clear its list of registered variables. This is used only
    -- for diagnostics.
    AIString:fnRunCombatStart()
    
    --The TurnResettable reinitializes to clear leftover variables.
    gzTurnResettable = TurnResettable:new()
    
    --Initialize other variables.
    VM_SetVar("Root/Variables/Combat/AIGlobals/sTransformThisTurn", "S", "Null")
    
    -- |[Finish Up]|
    Debug_PopPrint("Combat Response Script: Begin Combat: Finish.\n")

-- |[ ============================ Turn Begin: Pre-Initiative Call ============================= ]|
--This is called at the start of a turn, after enemies have reinforced but before turn order is rolled.
-- This is typically where per-turn variables reset.
elseif(iSwitchType == gciCombatResponse_BeginTurnPreInitiative) then
    
    -- |[Diagnostics]|
    Debug_PushPrint(bDiagnostics, "Combat Response Script: Pre-Initiative: Start.\n")

    -- |[Reset Variables]|
    --Execute this to reset all registered TurnResettable entries to their initial values.
    gzTurnResettable:Run()
    
    -- |[Finish Up]|
    Debug_PopPrint("Combat Response Script: Pre-Initiative: Finish.\n")

-- |[ ============================ Turn Begin: Post-Initiative Call ============================ ]|
--This is called at the start of a turn, after initiative is rolled but before all entities run
-- their start-of-turn scripts.
elseif(iSwitchType == gciCombatResponse_BeginTurnPostInitiative) then
    
    -- |[Diagnostics]|
    Debug_PushPrint(bDiagnostics, "Combat Response Script: Post-Initiative: Start.\n")
    
    -- |[Finish Up]|
    Debug_PopPrint("Combat Response Script: Pre-Initiative: Finish.\n")

-- |[ ============================= Turn Begin: Post-Response Call ============================= ]|
--This is called at the start of a turn, after all entities have called their response scripts.
elseif(iSwitchType == gciCombatResponse_BeginTurnPostResponses) then
    
    -- |[Diagnostics]|
    Debug_PushPrint(bDiagnostics, "Combat Response Script: Post-Response: Start.\n")
    
    -- |[Finish Up]|
    Debug_PopPrint("Combat Response Script: Post-Response: Finish.\n")

-- |[ =============================== Action Begin: General Call =============================== ]|
--Called at the start of an action. The character in the 0th turn order slot is the acting entity.
elseif(iSwitchType == gciCombatResponse_BeginAction) then

    --Locate the character in the 0th slot.
    local iActingEntityID = AdvCombat_GetProperty("ID of Acting Entity")

    --If it's in the player's party, set this global cursor value. It is used for character switching.
    local iPartyOfID = AdvCombat_GetProperty("Party Of ID", iActingEntityID)
    if(iPartyOfID == gciACPartyGroup_Party) then
        
        --Get the character's name.
        AdvCombat_SetProperty("Push Entity By ID", iActingEntityID)
            local sInternalName = AdvCombatEntity_GetProperty("Internal Name")
        DL_PopActiveObject()
        
        --Get what slot it's in.
        local iActiveSlot = AdvCombat_GetProperty("Slot of Member In Active Party", sInternalName)
        
        --Store that in the global.
        VM_SetVar("Root/Variables/Combat/AIGlobals/iSwitchSlot", "N", iActiveSlot)
    else
        VM_SetVar("Root/Variables/Combat/AIGlobals/iSwitchSlot", "N", -1)
    end
end
