-- |[ ======================================= My Pretties ====================================== ]|
-- |[Description]|
--Heals all allied scraprats for 50 HP, buffs their attack power by 25% for 1 turn.
--Valid allies will have the tag "Is Scraprat".

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--These variables are used to auto-generate the properties of the ability when the ability is a
-- "standard" case. If you want to deviate from the standard, set things manually.
--Because the struct is global, it needs to have a unique name.
if(gzEnemyPackmasterMyPretties == nil) then
    
    -- |[System]|
    --Setup.
    local zAbiStruct = {}

    --Display/System Variables
    zAbiStruct.sJobName      = "Enemy"
    zAbiStruct.sSkillName    = "My Pretties"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Attack"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "Buff scraprat attack power, heal scraprats."
    zAbiStruct.sDescription = ""
    zAbiStruct.saImages = {}
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Buff scraprat attack power, heal scraprats."
    zAbiStruct.sSimpleDesc = ""
    zAbiStruct.saSimpleImages = {}

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = true --Set to true to ignore all requirements.
    zAbiStruct.sTargetMacro = "Target Allies All"
    
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Buff")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.bDoesNoDamage        = true
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun          = true
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits          = true
    zAbiStruct.zExecutionAbiPackage.bEffectAlwaysApplies = true
    zAbiStruct.zExecutionAbiPackage.iHealingBase         = 50
    
    --Non-Player-Character Flags
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    
    -- |[Execution Effect Package]|
    --Create an effect package and populate it.
    zAbiStruct.zExecutionEffPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides.
    zAbiStruct.zExecutionEffPackage.sEffectPath     = fnResolvePath() .. "../../Effects/Chapter 5 Enemies/My Pretties.lua"
    zAbiStruct.zExecutionEffPackage.sEffectCritPath = fnResolvePath() .. "../../Effects/Chapter 5 Enemies/My Pretties.lua"
    zAbiStruct.zExecutionEffPackage.sApplyText      = "Attack Up!"
    zAbiStruct.zExecutionEffPackage.sApplyTextCol   = "Color:Blue"
    
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzEnemyPackmasterMyPretties = zAbiStruct
end

--Because this is a prototype, we set the gzRefAbility to point to the unique structure. This way,
-- all the activity below points to the gzRefAbility's values and we don't need to change the structure's
-- name to create unique names for everything.
--The gsStandardAbilityPath handler also operates on gzRefAbility if it is called. You can use manual
-- overrides to make unique abilities.
gzRefAbility = gzEnemyPackmasterMyPretties

-- |[ ============================ Combat: Character Action Begins ============================= ]|
--Called when the character that possesses this ability begins their turn. This is where the ability
-- should enable or disable itself based on the character's MP/State/etc.
if(iSwitchType == gciAbility_BeginAction) then

    -- |[Setup]|
    --Flag false.
    AdvCombatAbility_SetProperty("Usable", false)
    
    -- |[Scan Allies]|
    --The caller must have at least one scraprat in their party or the ability is unusable.
    local iOwnerID = AdvCombatAbility_GetProperty("Owner ID")
    local iOwnerParty = AdvCombat_GetProperty("Party Of ID", iOwnerID)
    
    --Set strings. This allows the same code to be used between enemy and player parties.
    local sPartySizeString = "Combat Party Size"
    local sIDString = "Combat Party ID"
    if(iOwnerParty == gciACPartyGroup_Enemy) then
        sPartySizeString = "Enemy Party Size"
        sIDString = "Enemy ID"
    end
    
    --Scan.
    local iPartySize = AdvCombat_GetProperty(sPartySizeString)
    for i = 0, iPartySize-1, 1 do
        
        --Get the ID of the party member.
        local iPartyMemberID = AdvCombat_GetProperty(sIDString, i)
        
        --Entity cannot be the caller as the ability does not buff its user. Even if they were a scraprat somehow.
        if(iPartyMemberID ~= iOwnerID) then
        
            --Push the entity to check for the tag.
            AdvCombat_SetProperty("Push Entity By ID", iPartyMemberID)
                local iTagCount = AdvCombatEntity_GetProperty("Tag Count", "Is Scraprat")
            DL_PopActiveObject()
            
            --If at least one entity has the tag, the ability can be used.
            if(iTagCount > 0) then
                AdvCombatAbility_SetProperty("Usable", true)
                return
            end
        end
    end

-- |[ ============================= Combat: Character Free Action ============================== ]|
--Called after a character executes a free action. The ability should recompute whether or not it is
-- enabled based on the updated character state, including expended free actions.
elseif(iSwitchType == gciAbility_BeginFreeAction) then

    -- |[Setup]|
    --Flag false.
    AdvCombatAbility_SetProperty("Usable", false)
    
    -- |[Scan Allies]|
    --The caller must have at least one scraprat in their party or the ability is unusable.
    local iOwnerID = AdvCombatAbility_GetProperty("Owner ID")
    local iOwnerParty = AdvCombat_GetProperty("Party Of ID", iOwnerID)
    
    --Set strings. This allows the same code to be used between enemy and player parties.
    local sPartySizeString = "Combat Party Size"
    local sIDString = "Combat Party ID"
    if(iOwnerParty == gciACPartyGroup_Enemy) then
        sPartySizeString = "Enemy Party Size"
        sIDString = "Enemy ID"
    end
    
    --Scan.
    local iPartySize = AdvCombat_GetProperty(sPartySizeString)
    for i = 0, iPartySize-1, 1 do
        
        --Get the ID of the party member.
        local iPartyMemberID = AdvCombat_GetProperty(sIDString, i)
        
        --Entity cannot be the caller as the ability does not buff its user. Even if they were a scraprat somehow.
        if(iPartyMemberID ~= iOwnerID) then
        
            --Push the entity to check for the tag.
            AdvCombat_SetProperty("Push Entity By ID", iPartyMemberID)
                local iTagCount = AdvCombatEntity_GetProperty("Tag Count", "Is Scraprat")
            DL_PopActiveObject()
            
            --If at least one entity has the tag, the ability can be used.
            if(iTagCount > 0) then
                AdvCombatAbility_SetProperty("Usable", true)
                return
            end
        end
    end

-- |[ ================================= Combat: Paint Targets ================================== ]|
--For player-controlled characters, selects which targets can be used by this ability. AI-controlled
-- characters may optionally bypass this and select targets directly.
--Previous targets are implicitly cleared before this is called.
--If no targets are painted, the UI will play a fail sound and disable target selection.
elseif(iSwitchType == gciAbility_PaintTargets) then

    -- |[Setup]|
    --Create a target cluster.
    AdvCombat_SetProperty("Create Target Cluster", "Scraprat Cluster", "Scraprats")

    -- |[Scan All Allies]|
    --Set strings. This allows the same code to be used between enemy and player parties.
    local sPartySizeString = "Combat Party Size"
    local sIDString = "Combat Party ID"
    local iOwnerID = AdvCombatAbility_GetProperty("Owner ID")
    local iOwnerParty = AdvCombat_GetProperty("Party Of ID", iOwnerID)
    if(iOwnerParty == gciACPartyGroup_Enemy) then
        sPartySizeString = "Enemy Party Size"
        sIDString = "Enemy ID"
    end
    
    --Scan.
    local iPartySize = AdvCombat_GetProperty(sPartySizeString)
    for i = 0, iPartySize-1, 1 do
        
        --Get the ID of the party member.
        local iPartyMemberID = AdvCombat_GetProperty(sIDString, i)
        
        --Entity cannot be the caller as the ability does not buff its user. Even if they were a scraprat somehow.
        if(iPartyMemberID ~= iOwnerID) then
        
            --Push the entity to check for the tag.
            AdvCombat_SetProperty("Push Entity By ID", iPartyMemberID)
                local iTagCount = AdvCombatEntity_GetProperty("Tag Count", "Is Scraprat")
            DL_PopActiveObject()
            
            --Add this entity to the target cluster.
            if(iTagCount > 0) then
                AdvCombat_SetProperty("Add Target To Cluster", "Scraprat Cluster", iPartyMemberID)
            end
        end
    end


-- |[ ==================================== Standard Handler ==================================== ]|
--Call the standard handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
