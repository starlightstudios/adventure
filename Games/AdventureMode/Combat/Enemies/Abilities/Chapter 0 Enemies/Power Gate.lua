-- |[ ======================================= Power Gate ======================================= ]|
-- |[Description]|
--Passive. Applies a power gate to the owner.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Enemies.Ch0.PowerGate == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Enemy", "Power Gate", "$SkillName", gciJP_Cost_Passive, gbIsNotFreeAction, "Buff", "Passive", "GenBuff", "Passive")

    -- |[Passive Effect]|
    zAbiStruct.bNeverAvailable = true --Passive abilities have no usability requirements.
    zAbiStruct.sPassivePath    = fnResolvePath() .. "../../Effects/Chapter 0 Enemies/Power Gate.lua"

    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Enemies.Ch0.PowerGate = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Enemies.Ch0.PowerGate
