-- |[ ===================================== Obscure Attack ===================================== ]|
-- |[Description]|
--Standard attack using shadow damage.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Enemies.Ch0.ObscureAttack == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Enemy", "Obscure Attack", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "GenPoison", "Direct")
    
    -- |[Usability Variables]|
    --Enemies do not query usability.
    zAbiStruct.bAlwaysAvailable = true
    zAbiStruct.sTargetMacro = "Target Enemies Single"
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Shadow C")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Obscuring, 5, 1.00)
  --zAbiStruct:fnSetEnemyAction($psOverrideTitle)
    zAbiStruct:fnSetEnemyAction("Null")
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Enemies.Ch0.ObscureAttack = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Enemies.Ch0.ObscureAttack

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
