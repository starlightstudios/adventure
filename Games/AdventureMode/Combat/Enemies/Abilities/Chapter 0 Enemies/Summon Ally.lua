-- |[ ======================================= Summon Ally ====================================== ]|
-- |[Description]|
--Calls an ally to battle.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Ability Prototype =================================== ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Enemies.Ch0.SummonAlly == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, pbIsFreeAction, psResponse)
    zAbiStruct:fnSetSystem("Enemy", "Summon Ally", "Use Skill Name", gbIsNotFreeAction, "Direct")

    -- |[Usability Variables]|
    --Enemies do not query usability.
    zAbiStruct.bAlwaysAvailable = true
    zAbiStruct.sTargetMacro = "Target Self"
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Buff")
  --zAbiStruct:fnSetEnemyAction($psOverrideTitle)
    zAbiStruct:fnSetEnemyAction()
    
    --Summon handling.
    zAbiStruct.zExecutionAbiPackage:fnAddSummon("Training Dummy")
    
    --Does not animate.
    zaPackage.bDoesNotAnimate = true
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Prediction Builder]|
    zAbiStruct.zPredictionPackage = fnCreatePredictionFromAbility(zAbiStruct)
    
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzPrototypes.Combat.Enemies.Ch0.SummonAlly = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Enemies.Ch0.SummonAlly

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
