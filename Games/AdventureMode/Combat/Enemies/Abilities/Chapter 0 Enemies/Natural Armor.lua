-- |[ ===================================== Natural Armor ====================================== ]|
-- |[Description]|
--Passive. Applies a buff to the owner.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Enemies.Ch0.NaturalArmor == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Enemy", "Natural Armor", "$SkillName", gciJP_Cost_Passive, gbIsNotFreeAction, "Buff", "Passive", "GenBuff", "Passive")

    -- |[Passive Effect]|
    zAbiStruct.bNeverAvailable   = true
    zAbiStruct.sPassivePrototype = "Enemies.Ch0.NaturalArmor"
    zAbiStruct.bHasGUIEffects    = false

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    if(EffectList:fnEntryExists(zAbiStruct.sPassivePrototype) == false) then
        local saDescription = {}
        saDescription[1] = "Heavy carapace or hide provides natural"
        saDescription[2] = "protection. Increases [Prt](Defense)."
      --EffectList:fnCreatePassivePrototype(psName, psDisplayName, psIcon, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreatePassivePrototype(zAbiStruct.sPassivePrototype, "Natural Armor", "GenBuff", "Prt|Flat|8", saDescription)
    end

    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Enemies.Ch0.NaturalArmor = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Enemies.Ch0.NaturalArmor
