-- |[ ======================================= Obliterate ======================================= ]|
-- |[Description]|
--Massive terrify damage, inflicts slow, -evd, -acc, and -atk. Costs CP.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--These variables are used to auto-generate the properties of the ability when the ability is a
-- "standard" case. If you want to deviate from the standard, set things manually.
--Because the struct is global, it needs to have a unique name.
if(gzEvilHandmaidenObliterate == nil) then
    
    -- |[System]|
    --Setup.
    local zAbiStruct = {}

    --Display/System Variables
    zAbiStruct.sJobName      = "Handmaiden"
    zAbiStruct.sSkillName    = "Obliterate"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Advanced
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Christine|Obliterate"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "Inflict [[DamFactor]x [Atk]] as [DamageType].\n[HEffect][Str9][Tfy] [Slow] -20[Evd] -20[Acc] -25%%[Atk] / 3 [Turns].\n\n\n\n[CPCost][CPIco]. One Enemy."
    zAbiStruct.sDescription = ""
    zAbiStruct.saImages = {}
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Erase them.\nDeals massive [Tfy](Terrify) damage. Inflicts an\neffect reducing [Acc][Evd][Atk] and causing [Slow].\n\n\nCosts [CPCost][CPIco](CP)."
    zAbiStruct.sSimpleDesc = ""
    zAbiStruct.saSimpleImages = {}

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false  --Set to true to ignore all requirements.
    zAbiStruct.iRequiredMP = 0           --MP required to use the ability.
    zAbiStruct.iRequiredCP = 4           --CP required to use the ability.
    zAbiStruct.bRespectsCooldown = true  --If this ability has a cooldown, decrements it.
    zAbiStruct.iCooldown = 0             --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
    zAbiStruct.bIsFreeAction = false     --Flags as a free action.
    zAbiStruct.iRequiredFreeActions = 0  --Free actions set this as 1, everything else as 0.
    zAbiStruct.bRespectActionCap = false --Characters with multiple free actions have a hard action cap. This flag checks that cap.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies Single"

    --Effects
    zAbiStruct.iCPGain = 0
    
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Hit-Rate Module
    zAbiStruct.zPredictionPackage.bHasHitRateModule = true
    zAbiStruct.zPredictionPackage.zHitRateModule.iMissRate = 5
    
    --Damage Module
    zAbiStruct.zPredictionPackage.bHasDamageModule = true
    zAbiStruct.zPredictionPackage.zDamageModule.bUseWeaponType = false
    zAbiStruct.zPredictionPackage.zDamageModule.fDamageFactor = 2.75
    zAbiStruct.zPredictionPackage.zDamageModule.iScatterRange = 15
    
    --Crit Module
    zAbiStruct.zPredictionPackage.bHasCritModule = true
    
    --Create an Effect module
    zAbiStruct.zPredictionPackage.zaEffectModules[1] = fnCreateEffectModule()
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iEffectType = gciDamageType_Terrifying
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iEffectStrength = 9
    zAbiStruct.zPredictionPackage.zaEffectModules[1].sResultScript = "-20[IMG0] -20[IMG1] -25%%[IMG2] [Slow] / 2[IMG3]"
    zAbiStruct.zPredictionPackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/StatisticIcons/Accuracy", "Root/Images/AdventureUI/StatisticIcons/Evade", "Root/Images/AdventureUI/StatisticIcons/Attack", "Root/Images/AdventureUI/DebuffIcons/Clock"}
    
    -- |[Execution Ability Package]|
    --Basic package.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Terrify")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.bNeverCrits = true
    zAbiStruct.zExecutionAbiPackage.iMissThreshold = 0
    zAbiStruct.zExecutionAbiPackage.bUseWeapon = false
    zAbiStruct.zExecutionAbiPackage.iDamageType = gciDamageType_Terrifying
    zAbiStruct.zExecutionAbiPackage.fDamageFactor  = 2.75
    
    --Non-Player-Character Flags
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    
    -- |[Execution Effect Package]|
    --Basic package.
    zAbiStruct.zExecutionEffPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides.
    zAbiStruct.zExecutionEffPackage.iEffectStr      = 9
    zAbiStruct.zExecutionEffPackage.iEffectCritStr  = 13
    zAbiStruct.zExecutionEffPackage.iEffectType     = gciDamageType_Terrifying
    zAbiStruct.zExecutionEffPackage.sEffectPath     = fnResolvePath() .. "../../Effects/Chapter 5 Bosses/Obliterate.lua" 
    zAbiStruct.zExecutionEffPackage.sEffectCritPath = fnResolvePath() .. "../../Effects/Chapter 5 Bosses/Obliterate.lua" 
    zAbiStruct.zExecutionEffPackage.sApplyText      = "Crushed!"
    zAbiStruct.zExecutionEffPackage.sApplyTextCol   = "Color:Purple"
    zAbiStruct.zExecutionEffPackage.sResistText     = "Resisted!"
    zAbiStruct.zExecutionEffPackage.sResistTextCol  = "Color:White"
    
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzEvilHandmaidenObliterate = zAbiStruct
end

--Because this is a prototype, we set the gzRefAbility to point to the unique structure. This way,
-- all the activity below points to the gzRefAbility's values and we don't need to change the structure's
-- name to create unique names for everything.
--The gsStandardAbilityPath handler also operates on gzRefAbility if it is called. You can use manual
-- overrides to make unique abilities.
gzRefAbility = gzEvilHandmaidenObliterate

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
