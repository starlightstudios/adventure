-- |[ ========================================= Pause ========================================== ]|
-- |[Description]|
--Passes turn.

-- |[Notes]|
--For Code gciAbility_Create, the AdvCombatAbility is active, as this is a special system ability.
--For all other codes, the active object is the AdvCombatAbility itself. The acting character
-- can be pushed if needed.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ======================================== Creation ======================================== ]|
--Create the ability.
if(iSwitchType == gciAbility_Create) then
    AdvCombatEntity_SetProperty("Create Ability", "Enemy|Pause")
    
        --System
        AdvCombatAbility_SetProperty("Script Path", LM_GetCallStack(0))
        
        --Special:
        AdvCombatAbility_SetProperty("Requires Target", false)
    
        --Display
        AdvCombatAbility_SetProperty("Display Name", "Cower")
        AdvCombatAbility_SetProperty("Icon Back",    gsAbility_Backing_Direct)
        AdvCombatAbility_SetProperty("Icon",         "Root/Images/AdventureUI/Abilities/Attack")
        
        --Description
        AdvCombatAbility_SetProperty("Description", "Pause for a turn.")
        AdvCombatAbility_SetProperty("Crossload Description Images")
        
        --Response Flags
        fnSetAbilityResponseFlags(gciAbility_ResponseStandard_DirectAction)
        
    DL_PopActiveObject()

-- |[ ======================================== Job Taken ======================================= ]|
--Called when the character in question activates the job that contains this ability. This does not
-- occur outside of combat (a different script handles that), this is the in-combat variant. If an
-- ability does something when the owner changes Jobs, that should be done here.
--This is called after the job change has occurred.
elseif(iSwitchType == gciAbility_AssumeJob) then
    --This is never called.

-- |[ ================================== Combat: Combat Begins ================================= ]|
--Called when combat begins, to set up any local variables. Note that the Turn Begins code will be
-- called next, *after* turn order is resolved.
elseif(iSwitchType == gciAbility_BeginCombat) then

-- |[ =================================== Combat: Turn Begins ================================== ]|
--Called when the turn begins after turn order is resolved.
elseif(iSwitchType == gciAbility_BeginTurn) then

-- |[ ============================= Combat: Character Action Begins ============================ ]|
--Called when the character that possesses this ability begins their turn. This is where the ability
-- should enable or disable itself based on the character's MP/State/etc.
elseif(iSwitchType == gciAbility_BeginAction) then
    AdvCombatAbility_SetProperty("Usable", true)

-- |[ ============================== Combat: Character Free Action ============================= ]|
--Called after a character executes a free action. The ability should recompute whether or not it is
-- enabled based on the updated character state, including expended free actions.
elseif(iSwitchType == gciAbility_BeginFreeAction) then
    AdvCombatAbility_SetProperty("Usable", true)

-- |[ ============================== Combat: Character Action Ends ============================= ]|
--Called after a character executes any action. Used for passives or post-reaction abilities. Is not
-- called if the action was a free action (that's above).
elseif(iSwitchType == gciAbility_PostAction) then

-- |[ ================================== Combat: Paint Targets ================================= ]|
--For player-controlled characters, selects which targets can be used by this ability. AI-controlled
-- characters may optionally bypass this and select targets directly.
--If no targets are painted, the UI will play a fail sound and disable target selection.
elseif(iSwitchType == gciAbility_PaintTargets) then
    --Never gets called for this ability.

-- |[ =================================== Combat: Can Execute ================================== ]|
--Flags whether or not the event can actually run. If the originator got KOd by a DoT or a counterattack
-- or any other event, this will mark as false, and the event will stop here.
--This is only called for the ability in question.
elseif(iSwitchType == gciAbility_QueryCanRun) then
    AdvCombat_SetProperty("Push Event Originator")
        local bCanAct = AdvCombatEntity_GetProperty("Can Act")
    DL_PopActiveObject()
    if(bCanAct) then AdvCombat_SetProperty("Set Event Can Run", true) end

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

    AdvCombat_SetProperty("Push Acting Entity")
        local iActingID = RO_GetID()
    DL_PopActiveObject()
    AdvCombat_SetProperty("Register Application Pack", iActingID, iActingID, 0, "Flash Black")
    AdvCombat_SetProperty("Register Application Pack", iActingID, iActingID, 0, "Title|Pause")
    
    AdvCombat_SetProperty("Event Timer", gciCombatTicks_StandardActionMinimum)

-- |[ ==================================== Combat: Turn Ends =================================== ]|
--Called after the turn ends, before turn order for the next turn is resolved. The ability does not
-- need to have been used for this to be called.
elseif(iSwitchType == gciAbility_TurnEnds) then

-- |[ =================================== Combat: Combat Ends ================================== ]|
--Called after combat ends. The passed in flag dictates if Victory/Defeat occurred. This occurs
-- before Doctor Bag/XP/JP etc is awarded.
elseif(iSwitchType == gciAbility_CombatEnds) then

end
