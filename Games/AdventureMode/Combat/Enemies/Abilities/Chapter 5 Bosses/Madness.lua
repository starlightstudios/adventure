-- |[ ========================================= Madness ======================================== ]|
-- |[Description]|
--Deals damage of a random type, determined at runtime. Can sometimes deal a DoT, again at random.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--These variables are used to auto-generate the properties of the ability when the ability is a
-- "standard" case. If you want to deviate from the standard, set things manually.
--Because the struct is global, it needs to have a unique name.
if(gzCH5BossMadness == nil) then
    
    -- |[System]|
    --Setup.
    local zAbiStruct = {}

    --Display/System Variables
    zAbiStruct.sJobName      = "Enemy"
    zAbiStruct.sSkillName    = "Madness"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Attack"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "Inflict [[DamFactor]x [Atk]] as a random damage type.\nMay inflict a DoT, may not.\n\n\nOne Enemy or All Enemies."
    zAbiStruct.sDescription = ""
    zAbiStruct.saImages = {}
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Attack with a random damage type.\nMay inflict a random DoT.\nHits randomly.\n\n"
    zAbiStruct.sSimpleDesc = ""
    zAbiStruct.saSimpleImages = {}

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = true   --Set to true to ignore all requirements.
    zAbiStruct.iRequiredMP = 0           --MP required to use the ability.
    zAbiStruct.iRequiredCP = 0           --CP required to use the ability.
    zAbiStruct.bRespectsCooldown = true  --If this ability has a cooldown, decrements it.
    zAbiStruct.iCooldown = 0             --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
    zAbiStruct.bIsFreeAction = false     --Flags as a free action.
    zAbiStruct.iRequiredFreeActions = 0  --Free actions set this as 1, everything else as 0.
    zAbiStruct.bRespectActionCap = false --Characters with multiple free actions have a hard action cap. This flag checks that cap.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies Single"

    --Effects
    zAbiStruct.iCPGain = 0
    
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Hit-Rate Module
    zAbiStruct.zPredictionPackage.bHasHitRateModule = true
    zAbiStruct.zPredictionPackage.zHitRateModule.iMissRate = 5
    
    --Damage Module
    zAbiStruct.zPredictionPackage.bHasDamageModule = true
    zAbiStruct.zPredictionPackage.zDamageModule.bUseWeaponType = false
    zAbiStruct.zPredictionPackage.zDamageModule.iDamageType = gciDamageType_Slashing
    zAbiStruct.zPredictionPackage.zDamageModule.fDamageFactor = 1.00
    zAbiStruct.zPredictionPackage.zDamageModule.iScatterRange = 15
    
    -- |[Execution Ability Package]|
    --Basic package.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Sword Slash")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    zAbiStruct.zExecutionAbiPackage.bNeverCrits = true
    zAbiStruct.zExecutionAbiPackage.iMissThreshold = 5
    zAbiStruct.zExecutionAbiPackage.bUseWeapon = false
    zAbiStruct.zExecutionAbiPackage.iDamageType = gciDamageType_Slashing
    zAbiStruct.zExecutionAbiPackage.fDamageFactor  = 1.00
    
    --Non-Player-Character Flags
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzCH5BossMadness = zAbiStruct
end

--Because this is a prototype, we set the gzRefAbility to point to the unique structure. This way,
-- all the activity below points to the gzRefAbility's values and we don't need to change the structure's
-- name to create unique names for everything.
--The gsStandardAbilityPath handler also operates on gzRefAbility if it is called. You can use manual
-- overrides to make unique abilities.
gzRefAbility = gzCH5BossMadness


-- |[ ================================= Combat: Paint Targets ================================== ]|
--For player-controlled characters, selects which targets can be used by this ability. AI-controlled
-- characters may optionally bypass this and select targets directly.
--If no targets are painted, the UI will play a fail sound and disable target selection.
if(iSwitchType == gciAbility_PaintTargets) then
    
    --25% chance to hit all enemies:
    local iRoll = LM_GetRandomNumber(1, 100)
    if(iRoll <= 25) then
        AdvCombat_SetProperty("Run Target Macro", "Target Enemies All", AdvCombatAbility_GetProperty("Owner ID"))
    else
        AdvCombat_SetProperty("Run Target Macro", "Target Enemies Single", AdvCombatAbility_GetProperty("Owner ID"))
    end
    

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[ ================ Originator Statistics ================= ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
    DL_PopActiveObject()
    
    -- |[ ==================== Ability Package =================== ]|
    --Set ability package to randomize damage type.
    local zAbilityPackage = nil
    
    --Scatter damage type.
    local iTypeRoll = LM_GetRandomNumber(gciDamageType_Slashing, gciDamageType_Terrifying)
    
    --Use the lookup tables to get the matching animation.
    zAbilityPackage = fnConstructDefaultAbilityPackage(gzDamageTypes[iTypeRoll].sAnimation)

    --Common.
    zAbilityPackage.iOriginatorID = iOriginatorID
    zAbilityPackage.bCauseBlackFlash = true
    zAbilityPackage.bNeverCrits = true
    zAbilityPackage.iMissThreshold = 5
    zAbilityPackage.bUseWeapon = false
    zAbilityPackage.iDamageType = iTypeRoll
    zAbilityPackage.fDamageFactor  = 1.00
    
    -- |[ ==================== Effect Package ==================== ]|
    --25% chance to make this a DoT of a random type. Not always the same type as the primary attack.
    local zEffectPackage = nil
    local iEffectRoll = LM_GetRandomNumber(1, 100)
    if(iEffectRoll <= 25) then
        
        --Setup.
        zEffectPackage = fnConstructDefaultEffectPackage()
        zEffectPackage.iOriginatorID = iOriginatorID
        local iEffTypeRoll = LM_GetRandomNumber(gciDamageType_Slashing, gciDamageType_Terrifying)
        
        --Use the lookup table to get the display word.
        local sDisplayWord = gzDamageTypes[iEffTypeRoll].sDotText
        
        --Apply overrides.
        zEffectPackage.iEffectStr      = 6
        zEffectPackage.iEffectCritStr  = 9
        zEffectPackage.iEffectType     = iEffTypeRoll
        zEffectPackage.sEffectPath     = fnResolvePath() .. "../../Effects/Chapter 5 Bosses/Madness.lua"
        zEffectPackage.sEffectCritPath = fnResolvePath() .. "../../Effects/Chapter 5 Bosses/Madness.lua"
        zEffectPackage.sApplyText      = sDisplayWord
        zEffectPackage.sApplyTextCol   = "Color:Red"
        zEffectPackage.sResistText     = "Resisted!"
        zEffectPackage.sResistTextCol  = "Color:White"
        
        --Special: Needed for the effect.
        giMadnessEffectType = iEffTypeRoll
    end
    
    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        fnStandardExecution(iOriginatorID, i-1, zAbilityPackage, zEffectPackage)
    end
    
-- |[ ==================================== Standard Handler ==================================== ]|
--Call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
