-- |[ ==================================== Enveloping Spores =================================== ]|
-- |[Description]|
--Greatly increases Mycela's evasion for 2 turns, making her almost unhittable.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Ability Prototype =================================== ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Enemies.Ch1.EnvelopingSpores == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Boss", "Enveloping Spores", "$SkillName", gciJP_Cost_Normal, gbIsFreeAction, "Buff", "Active", "GenBuff", "Direct")

    -- |[Usability Variables]|
    --Enemies do not query usability.
    zAbiStruct.bAlwaysAvailable = true
    zAbiStruct.sTargetMacro = "Target Self"
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Buff")
    zAbiStruct.zExecutionAbiPackage:fnSetAsBuff()
  --zAbiStruct.zExecutionAbiPackage:fnAddAnimation(psAnimationName, piTimerOffset, $pfXOffset, $pfYOffset)
    zAbiStruct.zExecutionAbiPackage:fnAddAnimation("Buff Evade", 0)
  --zAbiStruct:fnSetEnemyAction($psOverrideTitle)
    zAbiStruct:fnSetEnemyAction()
    
    -- |[Effect Package]|
  --zAbiStruct:fnAddBuff(psPrototypeName, $psApplyText, $psApplyColorString, $psApplyAnim, $psApplySound)
    zAbiStruct:fnAddBuff("Enemies.Ch1.EnvelopingSpores", "Enveloped!", "Color:Blue")

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
        local saDescription = {}
        saDescription[1] = "Covered in a cloud of spores!"
        saDescription[2] = "Increases [Evd](Evade) for 3 turns."
      --EffectList:fnCreateBuffPrototype(psName, psDisplayName, psIcon, piTurns, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreateBuffPrototype(sLocalPrototypeName, "Enveloping Spores", "GenBuff", 4, "Evd|Flat|300", saDescription)
    end

    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Enemies.Ch1.EnvelopingSpores = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Enemies.Ch1.EnvelopingSpores

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
