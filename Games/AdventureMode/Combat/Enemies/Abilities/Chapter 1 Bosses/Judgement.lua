-- |[ ======================================= Judgement ======================================== ]|
-- |[Description]|
--Striking attack used by the Warden boss. Deals an enormous amount of damage to one target. Used
-- once every three turns.
--If the target is afflicted by the bleed 'Guilt', they take double damage.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Enemies.Ch1.Judgement == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Boss", "Judgement", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "Attack", "Direct")
    
    -- |[Usability Variables]|
    --Enemies do not query usability.
    zAbiStruct.bAlwaysAvailable = true
    zAbiStruct.sTargetMacro = "Target Enemies Single"
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Strike")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Striking, 5, 1.00)
  --zAbiStruct:fnSetEnemyAction($psOverrideTitle)
    zAbiStruct:fnSetEnemyAction()
    
    --Always hits.
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits = true
    
    -- |[Execution Effect Package]|
  --zAbiStruct:fnAddEffect(piStrNormal, piStrCrit, piApplyType, psApplyText, psCritApplyText, psApplyColorString, psaApplyBonus, psaApplyMalus, psaSeverityBonus, psaSeverityMalus)
    zAbiStruct:fnAddEffect(8, 8, gciDamageType_Bleeding, "Bleeding!", "Badly Bleeding!", "Color:Red", {"Bleed Standard"})
    
    -- |[Use of Effect Prototype]|
    zAbiStruct.zExecutionEffPackage.sPrototypeName = "Enemies.Ch1.Guilt"

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
      --EffectList:fnCreateDoTPrototype(psName, piTurns, piApplyStrength, pfDamPerTurn, piDamageType, psDisplayName, psIcon, $piCritTurns, $piCritApplyStrength)
        EffectList:fnCreateDoTPrototype(sLocalPrototypeName, 4, 8, 0.10, gciDamageType_Bleeding, "Guilt", "GenBleed")
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Enemies.Ch1.Judgement = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Enemies.Ch1.Judgement

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
if(iSwitchType == gciAbility_Execute) then
    
    -- |[ ====================== Originator Statistics ===================== ]|
    --Get originator.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
    DL_PopActiveObject()
    
    --Store the originator ID.
    gzRefAbility.zExecutionAbiPackage.iOriginatorID = iOriginatorID
    
    --Reset to 1.0x damage.
    gzRefAbility.zExecutionAbiPackage.fDamageFactor = 1.00
    
    -- |[ ========================= For Each Target ======================== ]|
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        
        --Get how many bleeds the target has.
        local iTargetID = AdvCombat_GetProperty("ID Of Target", i-1)
        AdvCombat_SetProperty("Push Entity By ID", iTargetID)
            local iBleedEffects = AdvCombatEntity_GetProperty("Tag Count", "Bleeding DoT")
        DL_PopActiveObject()
        
        --If the bleed is applied, double the damage.
        if(iBleedEffects > 0) then
            gzRefAbility.zExecutionAbiPackage.fDamageFactor = 2.00
        end
    
        --Run.
        fnStandardExecution(iOriginatorID, i-1, gzRefAbility.zExecutionAbiPackage, nil)
    end

-- |[ ==================================== Standard Handler ==================================== ]|
--Standard handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
