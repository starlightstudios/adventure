-- |[ ====================================== Summon Zombee ===================================== ]|
-- |[Description]|
--Calls a new Zombee ally for the boss.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--These variables are used to auto-generate the properties of the ability when the ability is a
-- "standard" case. If you want to deviate from the standard, set things manually.
--Because the struct is global, it needs to have a unique name.
if(gzEnemyBossSummonZombee == nil) then

    -- |[System]|
    --Setup.
    local zAbiStruct = {}

    --Display/System Variables
    zAbiStruct.sJobName      = "Boss"
    zAbiStruct.sSkillName    = "Summon Zombee"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Attack"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "Default description."
    zAbiStruct.sDescription = ""
    zAbiStruct.saImages = {}
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Default description."
    zAbiStruct.sSimpleDesc = ""
    zAbiStruct.saSimpleImages = {}
    
    -- |[Usability Variables]|
    --Availability.
    zAbiStruct.bAlwaysAvailable = true --Enemies don't care about stats.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Self"
    
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Pierce")
    
    --Non-Player-Character Flags
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzEnemyBossSummonZombee = zAbiStruct
end

--Because this is a prototype, we set the gzRefAbility to point to the unique structure. This way,
-- all the activity below points to the gzRefAbility's values and we don't need to change the structure's
-- name to create unique names for everything.
--The gsStandardAbilityPath handler also operates on gzRefAbility if it is called. You can use manual
-- overrides to make unique abilities.
gzRefAbility = gzEnemyBossSummonZombee

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
if(iSwitchType == gciAbility_Execute) then

    -- |[ ================ Originator Statistics ================= ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
    DL_PopActiveObject()
    
    -- |[ =================== Ability Package ==================== ]|
    --Run the script to create the allied bee. The value gsLastEnemyName will be populated with the unique name.
    LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/000 Scripted Enemies/Zombee Special.lua")
    if(giLastEnemyID == nil) then return end
    
    --Order the entity to move to the right side.
    local iTimer = 0
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, giLastEnemyID, iTimer, "Position|" .. gciACPoscode_Enemy)
    iTimer = iTimer + gciAC_StandardMoveTicks
    
    --Dialogue.
    local sScriptPath = LM_GetCallStack(0)
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, giLastEnemyID, iTimer, "Run Script|" .. sScriptPath .. "|N:-1000")
    iTimer = iTimer + 15
    
    --End
    AdvCombat_SetProperty("Event Timer", iTimer)
    
    --Flag reset.
    giTargetID = giLastEnemyID
    giLastEnemyID = nil
    
-- |[ ======================================== Cutscene ======================================== ]|
--Special
elseif(iSwitchType == -1000) then

    -- |[ ====================== Error Check ===================== ]|
    --Make sure the target exists.
    if(giTargetID == nil) then return end
    
    -- |[ =================== Cutscene Handling ================== ]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "CultistF", "Neutral") ]])
    fnCutscene([[ Append("Cultist:[E|Neutral] Protect me, drones![P] To my side!") ]])
    fnCutsceneBlocker()
    fnCutscene([[ AdvCombat_SetProperty("Position Enemies Normally") ]])
    fnCutsceneBlocker()
    
    --Clean up.
    giTargetID = nil

-- |[ ==================================== Standard Handler ==================================== ]|
--Standard handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
