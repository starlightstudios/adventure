-- |[ ========================================= Infect ========================================= ]|
-- |[Description]|
--Hits for 50% as piercing, deals another 100% as poison over 3 turns.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Enemies.Ch1.Infect == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Boss", "Infect", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "DoT", "Active", "Attack", "Direct")
    
    -- |[Usability Variables]|
    --Enemies do not query usability.
    zAbiStruct.bAlwaysAvailable = true
    zAbiStruct.sTargetMacro = "Target Enemies Single"
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Pierce")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Piercing, 5, 0.50)
  --zAbiStruct.zExecutionAbiPackage:fnAddAnimation(psAnimationName, piTimerOffset, $pfXOffset, $pfYOffset)
    zAbiStruct.zExecutionAbiPackage:fnAddAnimation("Poison", 15)
  --zAbiStruct:fnSetEnemyAction($psOverrideTitle)
    zAbiStruct:fnSetEnemyAction()
    
    --Ability always hits.
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits = true
    
    -- |[Execution Effect Package]|
  --zAbiStruct:fnAddEffect(piStrNormal, piStrCrit, piApplyType, psApplyText, psCritApplyText, psApplyColorString, psaApplyBonus, psaApplyMalus, psaSeverityBonus, psaSeverityMalus)
    zAbiStruct:fnAddEffect(8, 8, gciDamageType_Poisoning, "Poisoned!", "Badly Poisoned!", "Color:Red", {"Poisoned Standard"})
    
    -- |[Use of Effect Prototype]|
    zAbiStruct.zExecutionEffPackage.sPrototypeName = "Enemies.Ch1.Infect"

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
      --EffectList:fnCreateDoTPrototype(psName, piTurns, piApplyStrength, pfDamPerTurn, piDamageType, psDisplayName, psIcon, $piCritTurns, $piCritApplyStrength)
        EffectList:fnCreateDoTPrototype(sLocalPrototypeName, 3, 8, 0.20, gciDamageType_Poisoning, "Poisoned", "GenPoison")
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Enemies.Ch1.Infect = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Enemies.Ch1.Infect

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
