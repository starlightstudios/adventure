-- |[ ==================================== Bee Buddy Attack ==================================== ]|
-- |[Description]|
--Standard sword-slash attack. Since it's used by an ally, this ability can strike critically.
-- It is otherwise identical to other generic attacks.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Allies.BeeBuddy.Attack == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Ally", "Bee Buddy Attack", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "Attack", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Inflict [[DamFactor]x [Atk]] as [DamageType].\n\nBase Hit Rate: [HitRate]%%.\n\n\nSingle enemy."
    zAbiStruct.sSimpleDescMarkdown  = "Normal attack. Can strike critically."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, giNoCPCost, giNoCooldown, "Target Enemies Single", giNoCPGeneration)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Sword Slash")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Slashing, 5, 1.20)
    
    --Causes a black flash so the player knows the ally is attacking.
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Allies.BeeBuddy.Attack = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Allies.BeeBuddy.Attack

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
