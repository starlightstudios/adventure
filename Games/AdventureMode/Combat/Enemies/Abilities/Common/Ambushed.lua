-- |[ ======================================== Ambushed ======================================== ]|
-- |[Description]|
--System ability, used when an enemy is ambushed. Passes their turn, but allows DoTs and Effects
-- to function normally.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Standard.Ambushed == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()
  --fnSetAbilitySystemPropertiesEnemy(pzAbility, psJobName, psSkillName, psDisplayName, pbIsFreeAction, psResponse)
    fnSetAbilitySystemPropertiesEnemy(zAbiStruct, "System", "Ambushed", "Use Skill Name", gbIsNotFreeAction, "Direct")
    
    -- |[Usability Variables]|
    --Enemies do not query usability.
    zAbiStruct.bAlwaysAvailable = true
    zAbiStruct.sTargetMacro = "Target Self"
    
    -- |[ ========= Execution Package ======== ]|
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Buff")
    
    --Triggers black flash and shows skill name when using.
  --fnSetAbilityExecutionEnemy(pzAbiStruct, &psOverrideName)
    fnSetAbilityExecutionEnemy(zAbiStruct, "Ambushed!")
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Prediction Builder]|
    zAbiStruct.zPredictionPackage = fnCreatePredictionFromAbility(zAbiStruct)

    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    gzPrototypes.Combat.Standard.Ambushed = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Standard.Ambushed

-- |[ ============================= Creation ============================ ]|
--Needs to set a special target flag.
if(iSwitchType == gciAbility_Create) then
    
    --Call standard.
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
    
    --Set the ref ability, it is cleared by the standard call.
    gzRefAbility = gzPrototypes.Combat.Standard.Ambushed
    
    --Call.
    AdvCombatEntity_SetProperty("Push Ability S", gzRefAbility.sJobName .. "|" .. gzRefAbility.sSkillName)
        AdvCombatAbility_SetProperty("Requires Target", false)
    DL_PopActiveObject()
    
    --Clean.
    gzRefAbility = nil
    
-- |[ ============================= Combat: Character Action Begins ============================ ]|
--Always available.
elseif(iSwitchType == gciAbility_BeginAction) then
    AdvCombatAbility_SetProperty("Usable", true)

-- |[ ============================== Combat: Character Free Action ============================= ]|
--Always available.
elseif(iSwitchType == gciAbility_BeginFreeAction) then
    AdvCombatAbility_SetProperty("Usable", true)

-- |[ =================================== Combat: Can Execute ================================== ]|
--Always can execute if the entity can act.
elseif(iSwitchType == gciAbility_QueryCanRun) then
    AdvCombat_SetProperty("Push Event Originator")
        local bCanAct = AdvCombatEntity_GetProperty("Can Act")
    DL_PopActiveObject()
    if(bCanAct) then AdvCombat_SetProperty("Set Event Can Run", true) end
    
-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

    --Get variables.
    AdvCombat_SetProperty("Push Acting Entity")
        local iActingID = RO_GetID()
    DL_PopActiveObject()
    
    --Activate.
    AdvCombat_SetProperty("Register Application Pack", iActingID, iActingID, 0, "Flash Black")
    AdvCombat_SetProperty("Register Application Pack", iActingID, iActingID, 0, "Text|Ambushed!|Color:White")
    AdvCombat_SetProperty("Event Timer", gciCombatTicks_StandardActionMinimum)


-- |[ ======================================== Standard ======================================== ]|
--Call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
