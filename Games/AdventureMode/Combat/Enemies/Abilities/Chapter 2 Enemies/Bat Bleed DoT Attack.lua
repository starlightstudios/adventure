-- |[ ================================== Bat DoT Bleed Attack ================================== ]|
-- |[Description]|
--Hits for 40% as bleeding, attempts to leave 60% as a bleeding DoT. Identical to the base version
-- but can inflict werebat on Sanya.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzEnemyBatBleedDoTAttack == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Enemy"
    zAbiStruct.sSkillName    = "Bat Bleed DoT"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_DoT
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/GenBleed"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "Inflict [[DamFactor]x [Atk]] as [DamageType].\n[HEffect][Str5] DoT of [1.20x [Atk]] [Bld] over 3[Turns].\nSingle enemy."
    zAbiStruct.sDescription = ""
    zAbiStruct.saImages = {}
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Attack one enemy, dealing [Bld] and leaving a [Bld] DoT."
    zAbiStruct.sSimpleDesc = ""
    zAbiStruct.saSimpleImages = {}
    
    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = true

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies Single"

    --Effects
    zAbiStruct.iCPGain = 0
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Hit-Rate Module
    zAbiStruct.zPredictionPackage.bHasHitRateModule = true
    zAbiStruct.zPredictionPackage.zHitRateModule.iMissRate = 5
    
    --Crit Module
    zAbiStruct.zPredictionPackage.bHasCritModule = true
    zAbiStruct.zPredictionPackage.zCritModule.iCritThreshold = 100
    
    --Damage Module
    zAbiStruct.zPredictionPackage.bHasDamageModule = true
    zAbiStruct.zPredictionPackage.zDamageModule.iDamageType = gciDamageType_Bleeding
    zAbiStruct.zPredictionPackage.zDamageModule.fDamageFactor = 0.40
    
    --Create an Effect module
    zAbiStruct.zPredictionPackage.zaEffectModules[1] = fnCreateEffectModule()
    zAbiStruct.zPredictionPackage.zaEffectModules[1].bIsDamageOverTime = true
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iEffectType = gciDamageType_Bleeding
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iDoTDamageType = gciDamageType_Bleeding
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iEffectStrength = 7
    zAbiStruct.zPredictionPackage.zaEffectModules[1].fDotAttackFactor = 0.20
    zAbiStruct.zPredictionPackage.zaEffectModules[1].sResultScript = "[DAM][IMG1] for 3[IMG2]"
    zAbiStruct.zPredictionPackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/DamageTypeIcons/Bleeding", "Root/Images/AdventureUI/DebuffIcons/Clock"}
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Claw Slash")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.bNeverCrits      = true
    zAbiStruct.zExecutionAbiPackage.iDamageType    = gciDamageType_Bleeding
    zAbiStruct.zExecutionAbiPackage.iMissThreshold = 5
    zAbiStruct.zExecutionAbiPackage.fDamageFactor  = 0.40
    
    --Non-Player-Character Flags
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    
    --Additional Animations.
    fnAddAnimationPackage(zAbiStruct.zExecutionAbiPackage, "Bleed", 0)
    
    -- |[Execution Effect Package]|
    --Create an effect package and populate it.
    zAbiStruct.zExecutionEffPackage = fnConstructDefaultEffectPackage()
    
    --Overrides.
    zAbiStruct.zExecutionEffPackage.iEffectStr      = 7
    zAbiStruct.zExecutionEffPackage.iEffectCritStr  = 11
    zAbiStruct.zExecutionEffPackage.iEffectType     = gciDamageType_Bleeding
    zAbiStruct.zExecutionEffPackage.sEffectPath     = fnResolvePath() .. "../../Effects/Chapter 0 Enemies/DoT Bleed.lua"
    zAbiStruct.zExecutionEffPackage.sEffectCritPath = fnResolvePath() .. "../../Effects/Chapter 0 Enemies/DoT Bleed.lua"
    zAbiStruct.zExecutionEffPackage.sApplyText      = "Bleeding!"
    zAbiStruct.zExecutionEffPackage.sApplyTextCol   = "Color:Red"
    zAbiStruct.zExecutionEffPackage.sResistText     = "Resisted!"
    zAbiStruct.zExecutionEffPackage.sResistTextCol  = "Color:White"
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzEnemyBatBleedDoTAttack = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzEnemyBatBleedDoTAttack

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
if(iSwitchType == gciAbility_Execute) then
    
    -- |[Standard Execution]|
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
    
    -- |[Miss Check]|
    --If the attack missed, stop here. First, get the ID of the attack user.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
    DL_PopActiveObject()
    
    --Scan backwards through the abilities used to find the one used by our owner. It should be the
    -- last one, but a counterattack might take that slot.
    local iCurrentTurn = gzaCombatAbilityStatistics.iTurnsElapsed
    for i = #gzaCombatAbilityStatistics.zaStatisticsPacks, 1, -1 do

        --Get the package.
        local zStatsPack = gzaCombatAbilityStatistics.zaStatisticsPacks[i]
        
        --Error checks:
        if(zStatsPack == nil) then
            --io.write("  Error, out of range.\n")
        elseif(zStatsPack.iTurnElapsed ~= iCurrentTurn) then
            --io.write("  Error, incorrect turn " .. zStatsPack.iTurnElapsed .. " vs. " .. iCurrentTurn .. "\n")
        elseif(zStatsPack.iActorID ~= iOriginatorID) then
            --io.write("  Error, not action by owner " .. zStatsPack.iActorID .. " vs. " .. iOwnerID .. "\n")
        
        --Checks passed:
        else
            
            --Only one target is supposed to be attacked. If it was a miss, stop here.
            for p = 1, #zStatsPack.zaTargetInfo, 1 do
                if(zStatsPack.zaTargetInfo[p].bWasMiss) then return end
            end
        end
    end
    
    -- |[Check Targets for Tag]|
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        
        --If the target has this tag, it's Sanya in human form.
        AdvCombat_SetProperty("Push Target", i-1)
            local iTagCount = AdvCombatEntity_GetProperty("Tag Count", "Can Werebat Curse")
        DL_PopActiveObject()
        if(iTagCount > 0) then
        
            --Check this variable. If it's lower than "Infected", infect the target.
            local iWerebatStage = VM_GetVar("Root/Variables/Chapter2/WerebatTF/iWerebatStage", "N")
            if(iWerebatStage < gciWerebatTF_Infected) then
                VM_SetVar("Root/Variables/Chapter2/WerebatTF/iWerebatStage", "N", gciWerebatTF_Infected)
                return
            end
        end
    end

-- |[ ===================================== All Other Cases ==================================== ]|
--Call standard handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
