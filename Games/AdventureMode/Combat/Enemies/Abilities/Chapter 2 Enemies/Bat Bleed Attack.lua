-- |[ ==================================== Bat Bleed Attack ==================================== ]|
-- |[Description]|
--Identical to the basic bat bleed attack, but can inflict the werebat curse on Sanya.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzEnemyBatBleedAttack == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Enemy"
    zAbiStruct.sSkillName    = "Bat Bleed Attack"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/GenBleed"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "Inflict [[DamFactor]x [Atk]] as [DamageType].\nOne enemy."
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Basic attack, deals [DamageType] damage."

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = true  --Set to true to ignore all requirements.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies Single"

    --Effects
    zAbiStruct.iCPGain = 0
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Hit-Rate Module
    zAbiStruct.zPredictionPackage.bHasHitRateModule = true
    zAbiStruct.zPredictionPackage.zHitRateModule.iMissRate = 5
    
    --Crit Module
    zAbiStruct.zPredictionPackage.bHasCritModule = true
    zAbiStruct.zPredictionPackage.zCritModule.iCritThreshold = 100
    
    --Damage Module
    zAbiStruct.zPredictionPackage.bHasDamageModule = true
    zAbiStruct.zPredictionPackage.zDamageModule.iDamageType = gciDamageType_Bleeding
    zAbiStruct.zPredictionPackage.zDamageModule.fDamageFactor = 1.00
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Claw Slash")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.bNeverCrits      = true
    zAbiStruct.zExecutionAbiPackage.iDamageType      = gciDamageType_Bleeding
    zAbiStruct.zExecutionAbiPackage.iMissThreshold   = 5
    zAbiStruct.zExecutionAbiPackage.fDamageFactor    = 1.00
    
    --Non-Player-Character Flags
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    
    --Additional Animations.
    fnAddAnimationPackage(zAbiStruct.zExecutionAbiPackage, "Bleed", 0)
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzEnemyBatBleedAttack = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzEnemyBatBleedAttack

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
if(iSwitchType == gciAbility_Execute) then
    
    -- |[Standard Execution]|
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
    
    -- |[Miss Check]|
    --If the attack missed, stop here. First, get the ID of the attack user.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
    DL_PopActiveObject()
    
    --Scan backwards through the abilities used to find the one used by our owner. It should be the
    -- last one, but a counterattack might take that slot.
    local iCurrentTurn = gzaCombatAbilityStatistics.iTurnsElapsed
    for i = #gzaCombatAbilityStatistics.zaStatisticsPacks, 1, -1 do

        --Get the package.
        local zStatsPack = gzaCombatAbilityStatistics.zaStatisticsPacks[i]
        
        --Error checks:
        if(zStatsPack == nil) then
            --io.write("  Error, out of range.\n")
        elseif(zStatsPack.iTurnElapsed ~= iCurrentTurn) then
            --io.write("  Error, incorrect turn " .. zStatsPack.iTurnElapsed .. " vs. " .. iCurrentTurn .. "\n")
        elseif(zStatsPack.iActorID ~= iOriginatorID) then
            --io.write("  Error, not action by owner " .. zStatsPack.iActorID .. " vs. " .. iOwnerID .. "\n")
        
        --Checks passed:
        else
            
            --Only one target is supposed to be attacked. If it was a miss, stop here.
            for p = 1, #zStatsPack.zaTargetInfo, 1 do
                if(zStatsPack.zaTargetInfo[p].bWasMiss) then return end
            end
        end
    end
    
    -- |[Check Targets for Tag]|
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        
        --If the target has this tag, it's Sanya in human form.
        AdvCombat_SetProperty("Push Target", i-1)
            local iTagCount = AdvCombatEntity_GetProperty("Tag Count", "Can Werebat Curse")
        DL_PopActiveObject()
        if(iTagCount > 0) then
        
            --Check this variable. If it's lower than "Infected", infect the target.
            local iWerebatStage = VM_GetVar("Root/Variables/Chapter2/WerebatTF/iWerebatStage", "N")
            if(iWerebatStage < gciWerebatTF_Infected) then
                VM_SetVar("Root/Variables/Chapter2/WerebatTF/iWerebatStage", "N", gciWerebatTF_Infected)
                return
            end
        end
    end

-- |[ ===================================== All Other Cases ==================================== ]|
--Call standard handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
