-- |[ ======================================= Caustic Cloud ====================================== ]|
-- |[Description]|
--Deals 30% of corrode damage over 3 turns to the entity party. Used every turn by Grub Nests.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzEnemyCausticCloud == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Enemy"
    zAbiStruct.sSkillName    = "Caustic Cloud"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Attack"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "Default description."
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Default description."
    
    -- |[Usability Variables]|
    --Availability.
    zAbiStruct.bAlwaysAvailable = true --Enemies don't care about stats.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies All"
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Corrode")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.iDamageType      = gciDamageType_Corroding
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    zAbiStruct.zExecutionAbiPackage.bNeverCrits      = true
    zAbiStruct.zExecutionAbiPackage.bNeverMisses     = true
    zAbiStruct.zExecutionAbiPackage.fDamageFactor    = 0.05
    
    --Non-Player-Character Flags
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    zAbiStruct.zExecutionAbiPackage.sShowTitle = zAbiStruct.sDisplayName
    
    -- |[Execution Effect Package]|
    --Create an effect package and populate it.
    zAbiStruct.zExecutionEffPackage = fnConstructDefaultEffectPackage()
    zAbiStruct.zExecutionEffPackage.iEffectStr      = 7
    zAbiStruct.zExecutionEffPackage.iEffectCritStr  = 10
    zAbiStruct.zExecutionEffPackage.iEffectType     = gciDamageType_Corroding
    zAbiStruct.zExecutionEffPackage.sEffectPath     = fnResolvePath() .. "../../Effects/Chapter 2 Enemies/Caustic Cloud DoT.lua"
    zAbiStruct.zExecutionEffPackage.sEffectCritPath = fnResolvePath() .. "../../Effects/Chapter 2 Enemies/Caustic Cloud DoT.lua"
    zAbiStruct.zExecutionEffPackage.sApplyText      = "Corroded!"
    zAbiStruct.zExecutionEffPackage.sApplyTextCol   = "Color:Red"
    zAbiStruct.zExecutionEffPackage.sResistText     = "Resisted!"
    zAbiStruct.zExecutionEffPackage.sResistTextCol  = "Color:White"
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzEnemyCausticCloud = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzEnemyCausticCloud

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
