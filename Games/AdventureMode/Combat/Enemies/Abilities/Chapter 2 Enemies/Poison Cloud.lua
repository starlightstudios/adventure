-- |[ ====================================== Poison Cloud ====================================== ]|
-- |[Description]|
--Deals 50% of poison damage over 3 turns to the entity party, and debuffs their attack power
-- and accuracy. The AI of the Poison Vine is specially coded to use this every 5 turns.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzEnemyPoisonCloud == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Enemy"
    zAbiStruct.sSkillName    = "Poison Cloud"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Attack"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "Default description."
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Default description."
    
    -- |[Usability Variables]|
    --Availability.
    zAbiStruct.bAlwaysAvailable = true --Enemies don't care about stats.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies All"
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Poison")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.iDamageType      = gciDamageType_Poisoning
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    zAbiStruct.zExecutionAbiPackage.bNeverCrits      = true
    zAbiStruct.zExecutionAbiPackage.bNeverMisses     = true
    zAbiStruct.zExecutionAbiPackage.fDamageFactor    = 0.10
    
    --Non-Player-Character Flags
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    zAbiStruct.zExecutionAbiPackage.sShowTitle = zAbiStruct.sDisplayName
    
    -- |[Execution Effect Package]|
    --Create an effect package and populate it.
    zAbiStruct.zExecutionEffPackage = fnConstructDefaultEffectPackage()
    zAbiStruct.zExecutionEffPackage.iEffectStr      = 6
    zAbiStruct.zExecutionEffPackage.iEffectCritStr  = 9
    zAbiStruct.zExecutionEffPackage.iEffectType     = gciDamageType_Poisoning
    zAbiStruct.zExecutionEffPackage.sEffectPath     = fnResolvePath() .. "../../Effects/Chapter 2 Enemies/Poison Cloud DoT.lua"
    zAbiStruct.zExecutionEffPackage.sEffectCritPath = fnResolvePath() .. "../../Effects/Chapter 2 Enemies/Poison Cloud DoT.lua"
    zAbiStruct.zExecutionEffPackage.sApplyText      = "Poisoned!"
    zAbiStruct.zExecutionEffPackage.sApplyTextCol   = "Color:Red"
    zAbiStruct.zExecutionEffPackage.sResistText     = "Resisted!"
    zAbiStruct.zExecutionEffPackage.sResistTextCol  = "Color:White"
    
    -- |[Execution Secondary Effect Package]|
    --Create an effect package and populate it.
    zAbiStruct.zExecutionEffPackageB = fnConstructDefaultEffectPackage()
    zAbiStruct.zExecutionEffPackageB.iEffectStr      = 6
    zAbiStruct.zExecutionEffPackageB.iEffectCritStr  = 9
    zAbiStruct.zExecutionEffPackageB.iEffectType     = gciDamageType_Poisoning
    zAbiStruct.zExecutionEffPackageB.sEffectPath     = fnResolvePath() .. "../../Effects/Chapter 2 Enemies/Poison Cloud Stat.lua"
    zAbiStruct.zExecutionEffPackageB.sEffectCritPath = fnResolvePath() .. "../../Effects/Chapter 2 Enemies/Poison Cloud Stat.lua"
    zAbiStruct.zExecutionEffPackageB.sApplyText      = "Gasping!"
    zAbiStruct.zExecutionEffPackageB.sApplyTextCol   = "Color:Red"
    zAbiStruct.zExecutionEffPackageB.sResistText     = "Resisted!"
    zAbiStruct.zExecutionEffPackageB.sResistTextCol  = "Color:White"
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzEnemyPoisonCloud = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzEnemyPoisonCloud

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
