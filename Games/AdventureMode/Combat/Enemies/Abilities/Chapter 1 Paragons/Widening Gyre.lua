-- |[ ======================================== Widening Gyre ======================================= ]|
-- |[Description]|
--Deal 100% of normal claw attack. Increases attack by 50 and evasion by 50 for 6 turns. Designed for use with the skitterer paragon.


-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzEnemyWideningGyre == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Enemy"
    zAbiStruct.sSkillName    = "Widening Gyre"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Attack"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "Default description."
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Default description."
    
    -- |[Usability Variables]|
    --Availability.
    zAbiStruct.bAlwaysAvailable = true --Enemies don't care about stats.

    --Targeting
     zAbiStruct.sTargetMacro = "Target Enemies Single"
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Claw Slash")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.iDamageType      = gciDamageType_Slashing
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun      = true
    zAbiStruct.zExecutionAbiPackage.iMissThreshold   = 5
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    zAbiStruct.zExecutionAbiPackage.fDamageFactor    = 1.00
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun      = true
    
    --Non-Player-Character Flags
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    zAbiStruct.zExecutionAbiPackage.sShowTitle = zAbiStruct.sDisplayName
    
         -- |[Execution Self-Effect Package]|  
    --Create an effect package and populate it.
    zAbiStruct.zExecutionSelfPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides.
    zAbiStruct.zExecutionSelfPackage.sApplyAnimation = "Null"
    zAbiStruct.zExecutionSelfPackage.sApplySound     = "Null"
    zAbiStruct.zExecutionSelfPackage.sEffectPath     = fnResolvePath() .. "../../Effects/Chapter 1 Paragons/Widening Gyre Self.lua"
    zAbiStruct.zExecutionSelfPackage.sEffectCritPath = fnResolvePath() .. "../../Effects/Chapter 1 Paragons/Widening Gyre Self.lua"
    zAbiStruct.zExecutionSelfPackage.sApplyText      = "Speed!"
    zAbiStruct.zExecutionSelfPackage.sApplyTextCol   = "Color:Blue"
     
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzEnemyWideningGyre = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzEnemyWideningGyre

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

