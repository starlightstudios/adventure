-- |[ ======================================== Monster of the Forest ======================================= ]|
-- |[Description]|
--A group poison attack that gives a healing effect. Meant to be consumed for massive damage. Designed for use with the Alrune Keeper Paragon.
--Revised for red tomb. Now reduces armor.
--Really catchy name.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzEnemyMonsterOfTheForest == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Enemy"
    zAbiStruct.sSkillName    = "Monster of the Forest"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Attack"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "Default description."
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Default description."
    
    -- |[Usability Variables]|
    --Availability.
    zAbiStruct.bAlwaysAvailable = true --Enemies don't care about stats.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies All"
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Poison")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.iDamageType      = gciDamageType_Poisoning    
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun      = true
    zAbiStruct.zExecutionAbiPackage.iMissThreshold   = 5
    zAbiStruct.zExecutionAbiPackage.fDamageFactor    = 0.50
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    
    --Non-Player-Character Flags
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    zAbiStruct.zExecutionAbiPackage.sShowTitle = zAbiStruct.sDisplayName
    
       -- |[Execution Effect Package]|
    --Create an effect package and populate it.
    zAbiStruct.zExecutionEffPackage = fnConstructDefaultEffectPackage()
    zAbiStruct.zExecutionEffPackage.iEffectStr      = 6
    zAbiStruct.zExecutionEffPackage.iEffectCritStr  = 9
    zAbiStruct.zExecutionEffPackage.iEffectType     = gciDamageType_Poisoning
    zAbiStruct.zExecutionEffPackage.sEffectPath     = fnResolvePath() .. "../../Effects/Chapter 1 Paragons/Nature's Punishment DoT.lua"
    zAbiStruct.zExecutionEffPackage.sEffectCritPath = fnResolvePath() .. "../../Effects/Chapter 1 Paragons/Nature's Punishment Crit DoT.lua"
    zAbiStruct.zExecutionEffPackage.sApplyText      = "Poisoned!"
    zAbiStruct.zExecutionEffPackage.sApplyTextCol   = "Color:Green"
    --zAbiStruct.zExecutionEffPackage.sCritApplyText  = "Super Slime!"
    zAbiStruct.zExecutionEffPackage.sResistText     = "Resisted!"
    zAbiStruct.zExecutionEffPackage.sResistTextCol  = "Color:White"
    
    
    -- |[Execution Secondary Effect Package]|
    --Create an effect package and populate it.
    zAbiStruct.zExecutionEffPackageB = fnConstructDefaultEffectPackage()
    zAbiStruct.zExecutionEffPackageB.iEffectStr      = 7
    zAbiStruct.zExecutionEffPackageB.iEffectCritStr  = 10
    zAbiStruct.zExecutionEffPackageB.iEffectType     = gciDamageType_Corroding
    zAbiStruct.zExecutionEffPackageB.sEffectPath     = fnResolvePath() .. "../../Effects/Chapter 1 Paragons/Nature's Punishment Stat.lua"
    zAbiStruct.zExecutionEffPackageB.sEffectCritPath = fnResolvePath() .. "../../Effects/Chapter 1 Paragons/Nature's Punishment Stat.lua"
    zAbiStruct.zExecutionEffPackageB.sApplyText      = "Weakened!"
    zAbiStruct.zExecutionEffPackageB.sApplyTextCol   = "Color:Green"
    --zAbiStruct.zExecutionEffPackageB.sCritApplyText  = "Super Slime!"
    zAbiStruct.zExecutionEffPackageB.sResistText     = "Resisted!"
    zAbiStruct.zExecutionEffPackageB.sResistTextCol  = "Color:White"
    
    
    --[=[
    -- |[Execution Effect Package]|
    --Create an effect package and populate it.
    zAbiStruct.zExecutionEffPackage = fnConstructDefaultEffectPackage()
    zAbiStruct.zExecutionEffPackage.iEffectStr      = 7
    zAbiStruct.zExecutionEffPackage.iEffectCritStr  = 10
    zAbiStruct.zExecutionEffPackage.iEffectType     = gciDamageType_Poisoning
    zAbiStruct.zExecutionEffPackage.sEffectPath     = fnResolvePath() .. "../../Effects/Chapter 1 Paragons/Nature's Punishment HOT.lua"
    zAbiStruct.zExecutionEffPackage.sEffectCritPath = fnResolvePath() .. "../../Effects/Chapter 1 Paragons/Nature's Punishment HOT.lua"
    zAbiStruct.zExecutionEffPackage.sApplyText      = "Flourishing!"
    zAbiStruct.zExecutionEffPackage.sApplyTextCol   = "Color:Green"
    zAbiStruct.zExecutionEffPackage.sResistText     = "Resisted!"
    zAbiStruct.zExecutionEffPackage.sResistTextCol  = "Color:White"
     ]=]
     
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzEnemyMonsterOfTheForest = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzEnemyMonsterOfTheForest

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

