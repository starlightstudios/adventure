-- |[ ======================================== Slime Balloon ======================================= ]|
-- |[Description]|
--Group attack inflicting 50% corrode damage and 50% additional poison damage over 2 turns with stat debuff. Designed for use with Corrosive slime Paragon. 
--Based off silly things Meryl said.


-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzEnemySlimeBalloon == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Enemy"
    zAbiStruct.sSkillName    = "Slime Balloon"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Attack"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "Default description."
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Default description."
    
    -- |[Usability Variables]|
    --Availability.
    zAbiStruct.bAlwaysAvailable = true --Enemies don't care about stats.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies All"
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Corrode")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.iDamageType      = gciDamageType_Corroding
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun      = true
    zAbiStruct.zExecutionAbiPackage.iMissThreshold   = 5
    zAbiStruct.zExecutionAbiPackage.fDamageFactor    = 0.50
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    
    --Non-Player-Character Flags
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    zAbiStruct.zExecutionAbiPackage.sShowTitle = zAbiStruct.sDisplayName
     
         --Additional animations.
    fnAddAnimationPackage(zAbiStruct.zExecutionAbiPackage, "Corrode", 5)
     
    -- |[Execution Effect Package]|
    --Create an effect package and populate it.
    zAbiStruct.zExecutionEffPackage = fnConstructDefaultEffectPackage()
    zAbiStruct.zExecutionEffPackage.iEffectStr      = 5
    zAbiStruct.zExecutionEffPackage.iEffectCritStr  = 8
    zAbiStruct.zExecutionEffPackage.iEffectType     = gciDamageType_Corroding
    zAbiStruct.zExecutionEffPackage.sEffectPath     = fnResolvePath() .. "../../Effects/Chapter 1 Paragons/Slime Balloon DoT.lua"
    zAbiStruct.zExecutionEffPackage.sEffectCritPath = fnResolvePath() .. "../../Effects/Chapter 1 Paragons/Slime Balloon Crit DoT.lua"
    zAbiStruct.zExecutionEffPackage.sApplyText      = "Slimed!"
    zAbiStruct.zExecutionEffPackage.sApplyTextCol   = "Color:Green"
    --zAbiStruct.zExecutionEffPackage.sCritApplyText  = "Super Slime!"
    zAbiStruct.zExecutionEffPackage.sResistText     = "Resisted!"
    zAbiStruct.zExecutionEffPackage.sResistTextCol  = "Color:White"
    
    
    -- |[Execution Secondary Effect Package]|
    --Create an effect package and populate it.
    zAbiStruct.zExecutionEffPackageB = fnConstructDefaultEffectPackage()
    zAbiStruct.zExecutionEffPackageB.iEffectStr      = 6
    zAbiStruct.zExecutionEffPackageB.iEffectCritStr  = 9
    zAbiStruct.zExecutionEffPackageB.iEffectType     = gciDamageType_Poisoning
    zAbiStruct.zExecutionEffPackageB.sEffectPath     = fnResolvePath() .. "../../Effects/Chapter 1 Paragons/Slime Balloon Stat.lua"
    zAbiStruct.zExecutionEffPackageB.sEffectCritPath = fnResolvePath() .. "../../Effects/Chapter 1 Paragons/Slime Balloon Stat.lua"
    zAbiStruct.zExecutionEffPackageB.sApplyText      = "Slimed!"
    zAbiStruct.zExecutionEffPackageB.sApplyTextCol   = "Color:Green"
    --zAbiStruct.zExecutionEffPackageB.sCritApplyText  = "Super Slime!"
    zAbiStruct.zExecutionEffPackageB.sResistText     = "Resisted!"
    zAbiStruct.zExecutionEffPackageB.sResistTextCol  = "Color:White"
     
     
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzEnemySlimeBalloon = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzEnemySlimeBalloon

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

