-- |[ ======================================= Adept F SA ====================================== ]|
-- |[Description]|
--Adept F paragon version of the Summon Ally ability. Calls an ally to battle.
--Summons an Adept M.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Ability Prototype =================================== ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Enemies.Ch1.AdeptFSummonAlly == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Enemy", "Adept F SA", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "GenSlash", "Direct")

    -- |[Usability Variables]|
    --Enemies do not query usability.
    zAbiStruct.bAlwaysAvailable = true
    zAbiStruct.sTargetMacro = "Target Self"
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Buff")
  --zAbiStruct:fnSetEnemyAction($psOverrideTitle)
    zAbiStruct:fnSetEnemyAction("Summon Ally")
    
    --Summon handling.
    zAbiStruct.zExecutionAbiPackage:fnSetAsBuff()
    zAbiStruct.zExecutionAbiPackage:fnAddSummon("Adept M")
    zAbiStruct.zExecutionAbiPackage.bDoesNotAnimate = true
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Prediction Builder]|
    zAbiStruct.zPredictionPackage = fnCreatePredictionFromAbility(zAbiStruct)
    
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzPrototypes.Combat.Enemies.Ch1.AdeptFSummonAlly = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Enemies.Ch1.AdeptFSummonAlly

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
