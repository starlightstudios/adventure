-- |[ ======================================== Slime Heart Blast ======================================= ]|
-- |[Description]|
--Hits single for 120% Crusading damage. Applies an Initative Debuff. Designed for use with the Bubbly Slime paragon


-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzEnemySlimeHeartBlast == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Enemy"
    zAbiStruct.sSkillName    = "Slime Heart Blast"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = "Slime Heart Blast!"
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Attack"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "Default description."
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Default description."
    
    -- |[Usability Variables]|
    --Availability.
    zAbiStruct.bAlwaysAvailable = true --Enemies don't care about stats.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies Single"
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Light")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.iDamageType    = gciDamageType_Crusading
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun      = true
    zAbiStruct.zExecutionAbiPackage.iMissThreshold   = 5
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    zAbiStruct.zExecutionAbiPackage.fDamageFactor    = 1.2
    
    --Non-Player-Character Flags
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    zAbiStruct.zExecutionAbiPackage.sShowTitle = zAbiStruct.sDisplayName
    
        -- |[Execution Effect Package]|
    --Create an effect package and populate it.
    zAbiStruct.zExecutionEffPackage = fnConstructDefaultEffectPackage()
    zAbiStruct.zExecutionEffPackage.iEffectStr      = 6
    zAbiStruct.zExecutionEffPackage.iEffectCritStr  = 9
    zAbiStruct.zExecutionEffPackage.iEffectType     = gciDamageType_Bleeding
    zAbiStruct.zExecutionEffPackage.sEffectPath     = fnResolvePath() .. "../../Effects/Chapter 1 Paragons/Slime Heart Blast Stat.lua"
    zAbiStruct.zExecutionEffPackage.sEffectCritPath = fnResolvePath() .. "../../Effects/Chapter 1 Paragons/Slime Heart Blast Stat.lua"
    zAbiStruct.zExecutionEffPackage.sApplyText      = "Blasted!"
    zAbiStruct.zExecutionEffPackage.sApplyTextCol   = "Color:Red"
    zAbiStruct.zExecutionEffPackage.sResistText     = "Resisted!"
    zAbiStruct.zExecutionEffPackage.sResistTextCol  = "Color:White"
    
    
     
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzEnemySlimeHeartBlast = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzEnemySlimeHeartBlast

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

