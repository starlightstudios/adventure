-- |[ ======================================== Roses Riposte ======================================= ]|
-- |[Description]|
--Deals 100% regular damage, 200% if consuming poison DOT. Designed for use with Alrune paragon family

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Enemies.Ch1.RosesRiposte == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Enemy", "Roses Riposte", "Rose's Riposte", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "GenSlash", "Direct")
    
    -- |[Usability Variables]|
    --Enemies do not query usability.
    zAbiStruct.bAlwaysAvailable = true
    zAbiStruct.sTargetMacro = "Target Enemies Single"
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Sword Slash")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Slash, 5, 1.0)
  --zAbiStruct:fnSetEnemyAction($psOverrideTitle)
    zAbiStruct:fnSetEnemyAction()
    
    --Chaser Modules. Ability has one for each damage type.
  --zAbiStruct.zExecutionAbiPackage:fnAddChaser(pbConsumeEffect, psDoTTag, pfDamageChangePerDoT, pfDamageConsumeFactor)
    zAbiStruct.zExecutionAbiPackage:fnAddChaser(gbConsumeEffects, "Poisoning DoT",  0.50, 0.00)
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Enemies.Ch1.RosesRiposte = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Enemies.Ch1.RosesRiposte

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
