-- |[ ===================================== Counterattack Bee ====================================== ]|
-- |[Description]|
--Gives the enemy a 90% chance to counterattack when attacked.
--Inflicts a evasion debuff, minimally powered. Designed for use with Ultimate Bee paragon.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzEnemyCounterattackBee == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Enemy"
    zAbiStruct.sSkillName    = "Counterattack Bee"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = "Wrath of the Hivemind"
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Passive
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Passive
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Attack"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_Counterattack
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "Description."
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Description"

    -- |[Usability Variables]|
    zAbiStruct.bNeverAvailable = true    --Passive abilities have no usability requirements.
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Prediction Package]|
    --Passives don't set anything for prediction packages.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    --Execution is used when counterattacking.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Shadow B")
    zAbiStruct.zExecutionAbiPackage.iDamageType      = gciDamageType_Obscuring
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun      = true
    zAbiStruct.zExecutionAbiPackage.iMissThreshold   = -10
    zAbiStruct.zExecutionAbiPackage.fDamageFactor    = 0.01
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    
        --Non-Player-Character Flags
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    zAbiStruct.zExecutionAbiPackage.sShowTitle = zAbiStruct.sDisplayName
    
        -- |[Execution Effect Package]|
    --This applies the given effect to the target(s), if it hits and they fail the resist check.  
    zAbiStruct.zExecutionEffPackage = fnConstructDefaultEffectPackage()
    zAbiStruct.zExecutionEffPackage.iEffectStr      = 6
    zAbiStruct.zExecutionEffPackage.iEffectCritStr  = 9
    zAbiStruct.zExecutionEffPackage.iEffectType     = gciDamageType_Poisoning
    zAbiStruct.zExecutionEffPackage.sEffectPath     = fnResolvePath() .. "../../Effects/Chapter 1 Paragons/Wrath Hive Stat.lua"
    zAbiStruct.zExecutionEffPackage.sEffectCritPath = fnResolvePath() .. "../../Effects/Chapter 1 Paragons/Wrath Hive Stat.lua"
    zAbiStruct.zExecutionEffPackage.sApplyText      = "Stunned!"
    zAbiStruct.zExecutionEffPackage.sApplyTextCol   = "Color:Purple"
    zAbiStruct.zExecutionEffPackage.sResistText     = "Resisted!"
    zAbiStruct.zExecutionEffPackage.sResistTextCol  = "Color:White"
    
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzEnemyCounterattackBee  = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzEnemyCounterattackBee 

-- |[ ================================== Combat: Combat Begins ================================= ]|
--This ability needs to store its target ID and create a variable marking whether or not it has
-- executed this turn.
if(iSwitchType == gciAbility_BeginCombat) then
    local iUniqueID = RO_GetID()
    DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "/")
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iExecThisTurn", "N", 0.0)
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iTargetID", "N", 0.0)

    --Get the owner for their ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOriginatorID = RO_GetID()
    DL_PopActiveObject()
    
    --Spawn the effect.
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 0, "Effect|" .. gzRefAbility.sPassivePath .. "|Originator:" .. iOriginatorID)
    
-- |[ =================================== Combat: Turn Begins ================================== ]|
--When the turn begins, reset the execution variable.
elseif(iSwitchType == gciAbility_BeginTurn) then
    local iUniqueID = RO_GetID()
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iExecThisTurn", "N", 0.0)
    
-- |[ ============================= Combat: Paint Targets Response ============================= ]|
--Marks the attacks for targeting.
elseif(iSwitchType == gciAbility_PaintTargetsResponse) then
    local iUniqueID = RO_GetID()
    local iTargetID = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iTargetID", "N")
    AdvCombat_SetProperty("Create Target Cluster", "Counter Cluster", "Attacker")
    AdvCombat_SetProperty("Add Target To Cluster", "Counter Cluster", iTargetID)
    
-- |[ =================================== Combat: Can Execute ================================== ]|
--Flags whether or not the event can actually run. If the originator got KOd by a DoT or a counterattack
-- or any other event, this will mark as false, and the event will stop here.
--This is only called for the ability in question.
elseif(iSwitchType == gciAbility_QueryCanRun) then
    AdvCombat_SetProperty("Push Event Originator")
        local bCanAct = AdvCombatEntity_GetProperty("Can Act")
    DL_PopActiveObject()
    if(bCanAct) then AdvCombat_SetProperty("Set Event Can Run", true) end
    
-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[ ================= Originator Statistics ================ ]|
    --Get originator.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
    DL_PopActiveObject()
    gzRefAbility.zExecutionAbiPackage.iOriginatorID = iOriginatorID
    
    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them. Call the standard
    -- execution subroutine.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        fnStandardExecution(iOriginatorID, i-1, gzRefAbility.zExecutionAbiPackage, gzRefAbility.zExecutionEffPackage)
    end
    
-- |[ ================================== Combat: Event Queued ================================== ]|
--Fired whenever a "main" event is queued, which is when the player or an AI decides which action to
-- perform. All abilities equipped by all entities then fire a response script. They can then enqueue
-- their own events before or after the main one.
--This occurs before the ability calls with gciAbility_Execute.
elseif(iSwitchType == gciAbility_EventQueued) then

    --Check if the ability already executed this turn:
    local iUniqueID = RO_GetID()
    local iExecThisTurn = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iExecThisTurn", "N")
    if(iExecThisTurn == 1.0) then
        return
    end
    
    --Get the ID of the owner of this ability.
    local iOwnerID = AdvCombatAbility_GetProperty("Owner ID")
    local iOwnerGroup = AdvCombat_GetProperty("Party Of ID", iOwnerID)
    
    --Get the originator, and check if it's a hostile entity.
    local iOriginatorID = AdvCombat_GetProperty("Query Originator ID")
    local iOriginatorGroup = AdvCombat_GetProperty("Party Of ID", iOriginatorID)
    
    --To be valid, the owner must be in a different party than the originator, and both must be in either
    -- the player's active party or the enemy's active party.
    if(iOwnerGroup == iOriginatorGroup) then
        return
    end
    if(iOwnerGroup ~= gciACPartyGroup_Party and iOwnerGroup ~= gciACPartyGroup_Enemy) then
        return
    end
    if(iOriginatorGroup ~= gciACPartyGroup_Party and iOriginatorGroup ~= gciACPartyGroup_Enemy) then
        return
    end
    
    --Check the target cluster. If the owner is found in the target cluster, we can counterattack.
    local bIsInTargetCluster = false
    local iTargetsTotal = AdvCombat_GetProperty("Query Targets Total")
    for i = 1, iTargetsTotal, 1 do
        local iTargetID = AdvCombat_GetProperty("Query Targets ID", i-1)
        if(iTargetID == iOwnerID) then
            bIsInTargetCluster = true
            break
        end
    end
    
    if(bIsInTargetCluster == false) then
        return
    end
    
    --Lastly, roll. This ability has a 90% chance to fire.
    local iRoll = LM_GetRandomNumber(1, 100)
    
    --Set this to true to make Precognition always activate. Use for debug.
    if(iRoll <= 90) then
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iExecThisTurn", "N", 1.0)
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iTargetID", "N", iOriginatorID)
        AdvCombatAbility_SetProperty("Enqueue This Ability As Event", 1)
    end
    
-- |[ ===================================== All Other Cases ==================================== ]|
--Standard handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
