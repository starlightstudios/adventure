-- |[ ======================================== Melty Hug ======================================= ]|
-- |[Description]|
--Hits for 150% corroding, but zeroes evade on self for 1 turn. Designed for use with Corroding slime Paragon.


-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzEnemyMeltyHug == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Enemy"
    zAbiStruct.sSkillName    = "Melty Hug"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Attack"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "Default description."
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Default description."
    
    -- |[Usability Variables]|
    --Availability.
    zAbiStruct.bAlwaysAvailable = true --Enemies don't care about stats.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies Single"
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Corrode")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.iDamageType      = gciDamageType_Corroding
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun      = true
    zAbiStruct.zExecutionAbiPackage.iMissThreshold   = -5
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    zAbiStruct.zExecutionAbiPackage.fDamageFactor    = 1.50
    
    --Non-Player-Character Flags
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    zAbiStruct.zExecutionAbiPackage.sShowTitle = zAbiStruct.sDisplayName
     
     
         -- |[Execution Self-Effect Package]|
    --Create an effect package and populate it.
    zAbiStruct.zExecutionSelfPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides.
    zAbiStruct.zExecutionSelfPackage.sApplyAnimation = "Null"
    zAbiStruct.zExecutionSelfPackage.sApplySound     = "Null"
    zAbiStruct.zExecutionSelfPackage.sEffectPath     = fnResolvePath() .. "../../Effects/Chapter 1 Paragons/Melty Hug Self.lua"
    zAbiStruct.zExecutionSelfPackage.sEffectCritPath = fnResolvePath() .. "../../Effects/Chapter 1 Paragons/Melty Hug Self.lua"
    zAbiStruct.zExecutionSelfPackage.sApplyText      = "Occupied"
    zAbiStruct.zExecutionSelfPackage.sApplyTextCol   = "Color:Blue"
     
     
     
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzEnemyMeltyHug = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzEnemyMeltyHug

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

