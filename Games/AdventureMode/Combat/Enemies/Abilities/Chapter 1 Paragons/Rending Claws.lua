-- |[ ======================================== Rending Claws ======================================= ]|
-- |[Description]|
--Hits for 100% regular damage slashing. Adds small bleed DOT which increases damage and will help player use ghost heal skill. 
--Designed for use with Zombee Paragon.
--Rip and tear!


-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzEnemyRendingClaws == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Enemy"
    zAbiStruct.sSkillName    = "Rending Claws"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Attack"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "Default description."
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Default description."
    
    -- |[Usability Variables]|
    --Availability.
    zAbiStruct.bAlwaysAvailable = true --Enemies don't care about stats.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies Single"
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Claw Slash")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.iDamageType      = gciDamageType_Slashing
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun      = true
    zAbiStruct.zExecutionAbiPackage.iMissThreshold   = 5
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    
    --Non-Player-Character Flags
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    zAbiStruct.zExecutionAbiPackage.sShowTitle = zAbiStruct.sDisplayName
    
        -- |[Execution Effect Package]|
    --This applies the given effect to the target(s), if it hits and they fail the resist check.  
    zAbiStruct.zExecutionEffPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides.
    zAbiStruct.zExecutionEffPackage.iEffectStr      = 8
    zAbiStruct.zExecutionEffPackage.iEffectCritStr  = 11
    zAbiStruct.zExecutionEffPackage.iEffectType     = gciDamageType_Bleeding
    zAbiStruct.zExecutionEffPackage.sEffectPath     = fnResolvePath() .. "../../Effects/Chapter 1 Paragons/Rending Claws DoT.lua"
    zAbiStruct.zExecutionEffPackage.sEffectCritPath = fnResolvePath() .. "../../Effects/Chapter 1 Paragons/Rending Claws Crit DoT.lua"
    zAbiStruct.zExecutionEffPackage.sApplyText      = "Bleeding!"
    zAbiStruct.zExecutionEffPackage.sApplyTextCol   = "Color:Red"
    zAbiStruct.zExecutionEffPackage.sResistText     = "Resisted!"
    zAbiStruct.zExecutionEffPackage.sResistTextCol  = "Color:White"
     
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzEnemyRendingClaws = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzEnemyRendingClaws

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

