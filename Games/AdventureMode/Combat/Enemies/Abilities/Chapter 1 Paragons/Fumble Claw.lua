-- |[ ======================================== Fumble (Claw) ======================================= ]|
-- |[Description]|
--Deal 50% of normal rubberine attack. Reduces Evasion and Iniative by 20. Designed for use with Rubber family of paragons.


-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzEnemyFumbleClaw == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Enemy"
    zAbiStruct.sSkillName    = "Fumble Claw"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = "Fumble"
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Attack"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "Default description."
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Default description."
    
    -- |[Usability Variables]|
    --Availability.
    zAbiStruct.bAlwaysAvailable = true --Enemies don't care about stats.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies Single"
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Claw Slash")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.iDamageType      = gciDamageType_Slashing
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun      = true
    zAbiStruct.zExecutionAbiPackage.iMissThreshold   = 10
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    zAbiStruct.zExecutionAbiPackage.fDamageFactor    = 0.50
    
    --Non-Player-Character Flags
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    zAbiStruct.zExecutionAbiPackage.sShowTitle = zAbiStruct.sDisplayName
     
     
         -- |[Execution Self-Effect Package]|
    --Create an effect package and populate it.
    zAbiStruct.zExecutionSelfPackage = fnConstructDefaultEffectPackage()
    
    --Setup randomness roll
    local iEffTypeRoll = LM_GetRandomNumber(gciDamageType_Slashing, gciDamageType_Terrifying)
    --io.write("Fumble successfully rolled random damage type.\n")
    
    --Apply overrides.
    zAbiStruct.zExecutionSelfPackage.sApplyAnimation = "Null"
    zAbiStruct.zExecutionSelfPackage.sApplySound     = "Null"
    zAbiStruct.zExecutionSelfPackage.sEffectPath     = fnResolvePath() .. "../../Effects/Chapter 1 Paragons/Fumble Self.lua"
    zAbiStruct.zExecutionSelfPackage.sEffectCritPath = fnResolvePath() .. "../../Effects/Chapter 1 Paragons/Fumble Self.lua"
    zAbiStruct.zExecutionSelfPackage.sApplyText      = "Fumble!"
    zAbiStruct.zExecutionSelfPackage.sApplyTextCol   = "Color:Blue"
     
    --Special: Needed for the effect, stores the rolled effect.
    --  giFumbleEffectType = iEffTypeRoll
    --  io.write("Fumble successfully stored random damage type.\n")
     
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzEnemyFumbleClaw = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzEnemyFumbleClaw

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

