-- |[ ======================================== Be Our Guest ======================================= ]|
-- |[Description]|
-- Buffs damage and evasion for all enemies. Designed for use with the Eye Drawer paragon.
--not used.


-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzEnemyBeOurGuest == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Enemy"
    zAbiStruct.sSkillName    = "Be Our Guest"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Attack"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "Default description."
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Default description."
    
    -- |[Usability Variables]|
    --Availability.
    zAbiStruct.bAlwaysAvailable = true --Enemies don't care about stats.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies All"
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
   -- |[ ========= Execution Package ======== ]|
   --experimental
   
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Shadow B")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.iDamageType      = gciDamageType_Obscuring
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun      = true
    zAbiStruct.zExecutionAbiPackage.iMissThreshold   = 5
    zAbiStruct.zExecutionAbiPackage.fDamageFactor    = 0.50
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    
    --Non-Player-Character Flags
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    zAbiStruct.zExecutionAbiPackage.sShowTitle = zAbiStruct.sDisplayName
   
   
   -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Buff")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.bDoesNoDamage = true
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun = true
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits = true
    zAbiStruct.zExecutionAbiPackage.bEffectAlwaysApplies = true
    
    --Non-Player-Character Flags
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    
    -- |[Execution Effect Package]|
    --Create an effect package and populate it.
    zAbiStruct.zExecutionEffPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides.
    zAbiStruct.zExecutionEffPackage.sEffectPath     = fnResolvePath() .. "../../Effects/Chapter 1 Paragons/Be Our Guest Buff.lua"
    zAbiStruct.zExecutionEffPackage.sEffectCritPath = fnResolvePath() .. "../../Effects/Chapter 1 Paragons/Be Our Guest Buff.lua"
    zAbiStruct.zExecutionEffPackage.sApplyText      = "Singing!"
    zAbiStruct.zExecutionEffPackage.sApplyTextCol   = "Color:Blue"
     
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzEnemyBeOurGuest = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzEnemyBeOurGuest

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

