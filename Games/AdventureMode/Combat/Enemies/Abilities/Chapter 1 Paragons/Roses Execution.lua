-- |[ ======================================== Roses Execution ======================================= ]|
-- |[Description]|
--Deals 80% of regular damage, 300% if consuming poison DOT (Nature's Punishment). 
--Designed for use with ultimate Alrune paragon.


-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzEnemyRosesExecution == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Enemy"
    zAbiStruct.sSkillName    = "Roses Execution"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Attack"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "Default description."
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Default description."
    
    -- |[Usability Variables]|
    --Availability.
    zAbiStruct.bAlwaysAvailable = true --Enemies don't care about stats.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies Single"
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Sword Slash")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.iDamageType      = gciDamageType_Slashing
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun      = true
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    zAbiStruct.zExecutionAbiPackage.fDamageFactor    = 0.80
    zAbiStruct.zExecutionAbiPackage.iMissThreshold   = 5
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun      = true
    
    --Non-Player-Character Flags
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    zAbiStruct.zExecutionAbiPackage.sShowTitle = zAbiStruct.sDisplayName
     
     --Chaser Modules
    zAbiStruct.zExecutionAbiPackage.bHasChaserModules = true
    zAbiStruct.zExecutionAbiPackage.iChaserModulesTotal = 1
    zAbiStruct.zExecutionAbiPackage.zaChaserModules[1] = {}
    zAbiStruct.zExecutionAbiPackage.zaChaserModules[1].bConsumeEffect = true           --If true, DoT expires when attack lands
    zAbiStruct.zExecutionAbiPackage.zaChaserModules[1].sDoTTag = "Poisoning DoT"        --What tag to look for on effects.
    zAbiStruct.zExecutionAbiPackage.zaChaserModules[1].fDamageFactorChangePerDoT = 2.20 --Added to the damage module's factor for each effect present.
    zAbiStruct.zExecutionAbiPackage.zaChaserModules[1].fDamageRemainingFactor = 0.0    --Percentage of remaining DoT damage to be added.
     
      --Non-Player-Character Flags
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    zAbiStruct.zExecutionAbiPackage.sShowTitle = zAbiStruct.sDisplayName
     
         --Additional animations.
    fnAddAnimationPackage(zAbiStruct.zExecutionAbiPackage, "Poison", 5)
    
     
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzEnemyRosesExecution = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzEnemyRosesExecution

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

