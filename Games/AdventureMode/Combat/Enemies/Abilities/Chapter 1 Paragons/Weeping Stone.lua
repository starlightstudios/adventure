-- |[ ===================================== Weeping Stone ===================================== ]|
-- |[Description]|
--Signalling buff for the Gravemarker paragon AI, also removes the stacks of Groaning slab
--Passes turn

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzEnemyWeepingStone == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Enemy"
    zAbiStruct.sSkillName    = "Weeping Stone"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Attack"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "Default description."
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Default description."
    
    -- |[Usability Variables]|
    --Availability.
    zAbiStruct.bAlwaysAvailable = true --Enemies don't care about stats.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Self"
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Buff")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.bDoesNoDamage = true
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun = true
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits = true
    zAbiStruct.zExecutionAbiPackage.bEffectAlwaysApplies = true
    
            --Chaser Modules
    zAbiStruct.zExecutionAbiPackage.bHasChaserModules = true
    zAbiStruct.zExecutionAbiPackage.iChaserModulesTotal = 1
    zAbiStruct.zExecutionAbiPackage.zaChaserModules[1] = {}
    zAbiStruct.zExecutionAbiPackage.zaChaserModules[1].bConsumeEffect = true           --If true, DoT expires when attack lands
    zAbiStruct.zExecutionAbiPackage.zaChaserModules[1].sDoTTag = "Crusading DoT"        --What tag to look for on effects.
    zAbiStruct.zExecutionAbiPackage.zaChaserModules[1].fDamageFactorChangePerDoT = 0.00 --Added to the damage module's factor for each effect present.
    zAbiStruct.zExecutionAbiPackage.zaChaserModules[1].fDamageRemainingFactor = 0.0    --Percentage of remaining DoT damage to be added.
    
    
    --Non-Player-Character Flags
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    zAbiStruct.zExecutionAbiPackage.sShowTitle = zAbiStruct.sDisplayName
    
            --Additional Paths
    zAbiStruct.zExecutionAbiPackage.saExecPaths = {LM_GetCallStack(0)}
    zAbiStruct.zExecutionAbiPackage.iaExecCodes = {gciAbility_SpecialStart}
    
    -- |[Execution Effect Package]|
    --Create an effect package and populate it.
    zAbiStruct.zExecutionEffPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides.
    zAbiStruct.zExecutionEffPackage.sEffectPath     = fnResolvePath() .. "../../Effects/Chapter 1 Paragons/Weeping Stone Self.lua"
    zAbiStruct.zExecutionEffPackage.sEffectCritPath = fnResolvePath() .. "../../Effects/Chapter 1 Paragons/Weeping Stone Self.lua"
    zAbiStruct.zExecutionEffPackage.sApplyText      = "weeps"
    zAbiStruct.zExecutionEffPackage.sApplyTextCol   = "Color:Blue"
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzEnemyWeepingStone = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzEnemyWeepingStone

-- |[ ============================ Combat: Special Negative Effects ============================ ]|
--All negative effects found with the poison/corrode/obscure/terrify effects are removed.
if(iSwitchType == gciAbility_SpecialStart) then

    --Argument handling.
    if(iArgumentsTotal < 3) then return end
    local iOriginatorID   = LM_GetScriptArgument(1, "N")
    local iTargetUniqueID = LM_GetScriptArgument(2, "N")
    
    --Subroutine needs a specially made list. Any of the matching elemental types must occur along with an
    -- "Is Negative" tag.
    local zaTagStruct = {}
    table.insert(zaTagStruct, {"Crusading DoT"})
    table.insert(zaTagStruct, {"Crusading Effect"})
    
    --Run the subroutine.
    local iaEffectIDList = fnCreateListOfEffectsWithTagStruct(iTargetUniqueID, zaTagStruct)
    
    --If there was at least one effect on the list to remove, add application packs for the removal.
    
    if(#iaEffectIDList > 0) then
    
        local iAnimTicks = fnGetAnimationTiming("Healing")
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, giStoredTimer, "Play Sound|Combat\\|Heal")
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, giStoredTimer, "Create Animation|Healing|AttackAnim0")
        giStoredTimer = giStoredTimer + iAnimTicks
    
        --AdvCombat_SetProperty("Register Application Pack", piOriginatorID, iTargetUniqueID, giStoredTimer, "Text|Cured!|Color:Green")
        for i = 1, #iaEffectIDList, 1 do
            AdvCombat_SetProperty("Register Application Pack", piOriginatorID, iTargetUniqueID, giStoredTimer, "Remove Effect|" .. iaEffectIDList[i])
        end
        giStoredTimer = giStoredTimer + gciApplication_TextTicks
    end
    

-- |[ ==================================== Standard Handler ==================================== ]|
--Call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end



--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
