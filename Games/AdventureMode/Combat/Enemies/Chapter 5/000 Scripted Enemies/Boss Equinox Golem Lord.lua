-- |[ =================================== Equinox Golem Lord =================================== ]|
--Golem Lord boss in Equinox. Vulnerable to blinds, has a protection buff when not blind.

-- |[ ===================================== Topic Handling ===================================== ]|
--Regardless of mugging or fighting an enemy, any topics created should be handled here.
--WD_SetProperty("Unlock Topic", "NameOfTopic", 1)

-- |[ =================================== Common Statistics ==================================== ]|
--Setup common values regardless of functionality required from this file.
local iPlatina = 100
local iExperience = 180
local iJobPoints = 40

--Item Listing.
local zaItemList = {}

-- |[ ====================================== Mug Handling ====================================== ]|
--Cannot be mugged.
if(TA_GetProperty("Is Mugging Check") == true) then
    return
end

-- |[ ==================================== Combat Encounter ==================================== ]|
-- |[Setup]|
local iEnemyID = AdvCombat_GetProperty("Generate Unique ID")
local sEnemyUniqueName = string.format("Autogen|%02i", iEnemyID)
AdvCombat_SetProperty("Register Enemy", sEnemyUniqueName, 0)

    -- |[System]|
    --Name
    AdvCombatEntity_SetProperty("Display Name", "Proud Golem")

    --DataLibrary Variables
    local iUniqueID = RO_GetID()
    DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "/")
    
    --Inspector.
    AdvCombatEntity_SetProperty("Inspector Show Stats", 0)
    AdvCombatEntity_SetProperty("Inspector Show Resists", 0)
        
    --Never show abilities on the inspector.
    AdvCombatEntity_SetProperty("Inspector Show Abilities", 1)
    
    --During dialogue, which actor represents this character.
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/sDialogueActor", "S", "Null")
    
    --AI
    AdvCombatEntity_SetProperty("AI Script", gsRoot .. "Combat/AIs/500 Boss Golem Lord.lua")

    -- |[Display]|
    --Images
    AdvCombatEntity_SetProperty("Combat Portrait", "Root/Images/Portraits/Combat/GolemLord")
    AdvCombatEntity_SetProperty("Turn Icon", "Root/Images/AdventureUI/TurnPortraits/GolemLord")
    AdvCombatEntity_SetProperty("Override Y", -1.0)
    
    --UI Positions
    AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,      299, 116)
    AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,      -37, -26)
    AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 341, 111)
    AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Enemy,       0, 106)

    -- |[Stats and Resistances]|
    --Statistics
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_HPMax,     800)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_MPMax,     100)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_CPMax,       0)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Attack,    120)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Initiative, 10)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Accuracy,   25)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Evade,       0)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_StunCap,  1000)
    
    --Resistances
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Protection,     0)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Slash,   gciNormalResist+2)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Strike,  gciNormalResist-2)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Pierce,  gciNormalResist+2)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Flame,   gciNormalResist+3)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Freeze,  gciNormalResist+3)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Shock,   gciNormalResist-5)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Crusade, gciNormalResist+2)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Obscure, gciNormalResist+2)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Bleed,   gciNormalResist+5)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Poison,  gciNormalResist+5)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Corrode, gciNormalResist-8)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Terrify, gciNormalResist-3)
    
    --Sum.
    AdvCombatEntity_SetProperty("Recompute Stats")
    AdvCombatEntity_SetProperty("Health Percent", 1.00)
    
    -- |[Tags]|
    --Special tags.
    AdvCombatEntity_SetProperty("Add Tag", "Immune To Influence", 1)
    AdvCombatEntity_SetProperty("Is Stunnable", false)

    -- |[Abilities]|
    --System Abilities
    fnEnemyStandardSystemAbilities()

    --Register abilities.
    local sJobAbilityPath = gsRoot .. "Combat/Enemies/Abilities/Chapter 0 Enemies/"
    LM_ExecuteScript(sJobAbilityPath .. "Slash Sword Attack.lua",  gciAbility_Create)
    LM_ExecuteScript(sJobAbilityPath .. "Strike Attack.lua", gciAbility_Create)
    
    --Set ability slots.
    AdvCombatEntity_SetProperty("Set Ability Slot", 0, 0, "Enemy|Sword Attack")
    AdvCombatEntity_SetProperty("Set Ability Slot Probability", 0, 0, 100)
    AdvCombatEntity_SetProperty("Set Ability Slot", 1, 0, "Enemy|Strike Attack")
    AdvCombatEntity_SetProperty("Set Ability Slot Probability", 1, 0, 100)
    
    --Passives.
    sJobAbilityPath = gsRoot .. "Combat/Enemies/Abilities/Chapter 0 Enemies/"
    LM_ExecuteScript(sJobAbilityPath .. "Unstunnable.lua", gciAbility_Create)
    
    -- |[Rewards Handling]|
    --Factor for mugging.
    local fXPFactor = 1.0
    local fJPFactor = 1.0
    local fPLFactor = 1.0
    if(TA_GetProperty("Was Mugged") == true) then
        fXPFactor = 1.0 - gcfMugExperienceRate
        fJPFactor = 1.0 - gcfMugPlatinaRate
        fPLFactor = 1.0 - gcfMugJPRate
    end
    
    --Rewards
    AdvCombatEntity_SetProperty("Reward XP",      math.floor(iExperience * fXPFactor))
    AdvCombatEntity_SetProperty("Reward JP",      math.floor(iJobPoints  * fJPFactor))
    AdvCombatEntity_SetProperty("Reward Platina", math.floor(iPlatina    * fPLFactor))
    AdvCombatEntity_SetProperty("Reward Doctor", -1)
    
    --Item rewards. These only apply if the entity was not mugged.
    if(TA_GetProperty("Was Mugged") == false) then
        local iRoll = LM_GetRandomNumber(1, 100)
        for i = 1, #zaItemList, 1 do
            if(iRoll >= zaItemList[i][1] and iRoll <= zaItemList[i][2]) then
                AdvCombatEntity_SetProperty("Reward Item", zaItemList[i][3])
            end
        end
    end
    
DL_PopActiveObject()
