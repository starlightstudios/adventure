-- |[ ================================= Enemy Statistics Chart ================================= ]|
--This chart lists off all enemy properties in one handy spot. Complicated enemies may need additional
-- setup, which is why they have their own files.
gzStatArray = {}
gzPortraitArray = {}

-- |[ ====================================== Enemy Listing ===================================== ]|
--Notes: Resistances are difference from standard, not actual values. The standard resistance is gciNormalResist.
-- Extra abilities and tags are added later.
--The "Attack" column is the basic attack this entity uses. Place "Null" to have no attacks or to manually specify
-- additional attacks later.

-- |[Chart]|
--     Actual Name         | Display Name        | Attack ||||| Lv | Health | Attack | Ini | Acc | Evd | Stun ||||| Pr | Sl | St | Pc | Fl | Fr | Sh | Cr | Ob | Bl | Ps | Cr | Tr ||||| Plt | Exp | JP |  Drop             | Chnc
fnAdd("Scraprat Scrounger", "Scrounger",          "Claw",       1,      35,      12,   30,    0,   10,    70,     0,   1,  -2,   3,   1,   3,  -5,   0,   0,   5,   5,  -8,  -3,      5,    3,  25, "Null",                 0)
fnAdd("Wrecked Bot",        "Wrecked Bot",        "Sword",      1,      42,      12,   30,    0,   10,    70,     0,   1,  -2,   3,   1,   3,  -5,   0,   0,   5,   5,  -8,2000,      8,    3,  25, "Null",                 0)
fnAdd("Motilvac",           "Motilvac",           "Sword",      3,     150,      68,   40,   10,   10,   120,     0,   3,   0,  -3,   1,   0,  -5,   0,   0,   5,   5,  -8,  -2,     10,    8,  25, "Null",                 0)
fnAdd("Mr. Wipey",          "Mr. Wipey",          "Sword",      3,     100,      58,   40,   10,   10,    70,     0,   3,  -2,   3,   1,  -4,  -5,   0,   0,   5,   5,  -8,  -2,     12,    6,  25, "Null",                 0)
fnAdd("Secrebot",           "Secrebot",           "Sword",      3,     130,      80,   40,   10,   10,   100,     0,   2,   2,  -3,   2,   2,  -5,  -2,  -2,   5,   5,  -8,2000,     14,    8,  25, "Null",                 0)
fnAdd("Scraprat Forager",   "Forager",            "Claw",       4,      85,      60,   50,   10,   10,    70,     0,   1,  -2,   3,   1,   3,  -5,   0,   0,   5,   5,  -8,  -3,      8,   12,  25, "Null",                 0)
fnAdd("Subverted Bot",      "Subverted Bot",      "Sword",      4,     185,      75,   50,   10,   10,    70,     0,   1,  -2,   3,   1,   3,  -5,   0,   0,   5,   5,  -8,2000,      8,   14,  25, "Null",                 0)
fnAdd("Void Rift",          "Void Rift",          "Claw",       7,     370,      94,   60,   30,   30,   150,     0,   7,   7,   1,  -5,  -5,   2,  -6,  -6,2000,2000,2000,2000,     24,   20,  25, "Null",                 0)
fnAdd("Waylighter",         "Waylighter",         "Claw",       7,     225,      76,   60,   30,   30,    70,     0,   5,   5,  -3,  -5,  -5,   3,  -6,  -6,  -2,  -5,   4,2000,     18,   18,  25, "Null",                 0)
fnAdd("Latex Drone",        "Latex Drone",        "Sword",      6,     355,      70,   60,   30,   30,   100,     0,   2,   2,  -5,   3,   3,   7,   0,   0,   2,   2,   7,  -5,     12,   22,  25, "Null",                 0)
fnAdd("Darkmatter",         "Darkmatter",         "Sword",      6,     400,     140,   70,   30,   40,    70,     0,   4,   4,   4,   4,   4,   4,   6,   6,   4,   4,   4, -10,      8,   20,  25, "Null",                 0)
fnAdd("Mad Worker",         "Mad Worker",         "Sword",      6,     500,      72,   40,   30,   10,   150,     0,   2,  -2,   2,   3,   3,  -5,  -2,  -2,   5,   5,  -8,2000,     15,   22,  25, "Null",                 0)
fnAdd("Mad Lord",           "Mad Lord",           "Sword",      6,     425,      88,   40,   30,   10,   150,     0,   2,  -2,   2,   3,   3,  -5,  -2,  -2,   5,   5,  -8,2000,     50,   24,  25, "Null",                 0)
fnAdd("Mad Doll",           "Mad Doll",           "Sword",      6,     220,     132,   80,   30,   40,    70,     0,   3,  -1,   3,   4,   4,  -1,  -4,  -4,   6,   6,  -5,2000,     14,   26,  25, "Null",                 0)
fnAdd("Autovac",            "Autovac",            "Sword",      7,     190,      90,   60,   30,   30,   120,     0,   3,   0,  -3,   1,   0,  -5,   0,   0,   5,   5,  -8,  -2,      3,   14,  25, "Null",                 0)
fnAdd("Mr. Wipesalot",      "Mr. Wipesalot",      "Sword",      7,     140,      70,   80,   30,   30,    70,     0,   3,  -2,   3,   1,  -4,  -5,   0,   0,   5,   5,  -8,  -2,      3,   12,  25, "Null",                 0)
fnAdd("Attendebot",         "Attendebot",         "Sword",      7,     160,     115,   60,   30,   30,   100,     0,   2,   2,  -3,   2,   2,  -5,  -2,  -2,   5,   5,  -8,2000,     16,   14,  25, "Null",                 0)
fnAdd("Scraprat Bomber",    "Bomber",             "Claw",      10,     200,       1,  100,   50,   50,    70,     0,   1,  -2,   3,   1,   3,  -5,   0,   0,   5,   5,  -8,  -3,      9,   36,  25, "Null",                 0)
fnAdd("Compromised Bot",    "Compromised Bot",    "Sword",     11,     320,     135,   60,   50,   50,    70,     0,   1,  -2,   3,   1,   3,  -5,   0,   0,   5,   5,  -8,2000,     21,   40,  25, "Null",                 0)
fnAdd("Spatial Tear",       "Spatial Tear",       "Claw",      11,     540,     124,   60,   50,   50,   150,     0,   7,   7,   1,  -5,  -5,   2,  -6,  -6,2000,2000,2000,2000,     25,   46,  25, "Null",                 0)
fnAdd("Lightsman",          "Lightsman",          "Claw",      11,     315,     106,   60,   50,   50,    70,     0,   5,   5,  -3,  -5,  -5,   3,  -6,  -6,  -2,  -5,   4,2000,     30,   42,  25, "Null",                 0)
fnAdd("Latex Packmaster",   "Latex Packmaster",   "Sword",     11,     300,      94,   60,   50,   50,   100,     0,   2,   2,  -5,   3,   3,   7,   0,   0,   2,   2,   7,  -5,     25,   48,  25, "Null",                 0)
fnAdd("Starseer",           "Starseer",           "Sword",     11,     900,      68,   80,   50,   60,    70,     0,   4,   4,   4,   4,   4,   4,   6,   6,   4,   4,   4, -10,     30,   42,  25, "Null",                 0)
fnAdd("Golem Security",     "Golem Security",     "Sword",     12,     700,     100,   50,   50,   50,   150,     0,   2,  -2,   2,   3,   3,  -5,  -2,  -2,   5,   5,  -8,   0,     24,   48,  25, "Null",                 0)
fnAdd("Security Lord",      "Security Lord",      "Sword",     12,     540,     118,   50,   50,   50,   150,     0,   2,  -2,   2,   3,   3,  -5,  -2,  -2,   5,   5,  -8,  -5,    100,   50,  25, "Null",                 0)
fnAdd("Security Doll",      "Security Doll",      "Sword",     12,     450,     185,  100,   50,   50,    70,     0,   3,  -1,   3,   4,   4,  -1,  -4,  -4,   6,   6,  -5,   0,     24,   56,  25, "Null",                 0)
fnAdd("Intellivac",         "Intellivac",         "Sword",     12,     580,     114,   70,   50,   40,   120,     0,   3,   0,  -3,   1,   0,  -5,   0,   0,   5,   5,  -8,  -2,     12,   40,  25, "Null",                 0)
fnAdd("Der Viper",          "Der Viper",          "Sword",     12,     460,      96,   70,   50,   40,    70,     0,   3,  -2,   3,   1,  -4,  -5,   0,   0,   5,   5,  -8,  -2,     22,   36,  25, "Null",                 0)
fnAdd("Plannerbot",         "Plannerbot",         "Sword",     12,     500,     148,   70,   50,   40,   100,     0,   2,   2,  -3,   2,   2,  -5,  -2,  -2,   5,   5,  -8,2000,     38,   40,  25, "Null",                 0)
fnAdd("Billowing Maw",      "Billowing Maw",      "Claw",      15,     800,     158,   70,   60,   40,   150,     0,   7,   7,   1,  -5,  -5,   2,  -6,  -6,2000,2000,2000,2000,    115,  215,  25, "Null",                 0)
fnAdd("Blind Seer",         "Blind Seer",         "Claw",      15,     620,     134,   70,   60,   40,    70,     0,   5,   5,  -3,  -5,  -5,   3,  -6,  -6,  -2,  -5,   4,2000,    120,  220,  25, "Null",                 0)
fnAdd("Latex Security",     "Latex Security",     "Sword",     15,     680,     120,   70,   60,   50,   100,     0,   2,   2,  -5,   3,   3,   7,   0,   0,   2,   2,   7,  -5,    150,  240,  25, "Null",                 0)
fnAdd("Nebula Sight",       "Nebula Sight",       "Sword",     15,    1300,      86,  100,   60,   70,    70,     0,   4,   4,   4,   4,   4,   4,   6,   6,   4,   4,   4, -10,     80,  230,  25, "Null",                 0)
fnAdd("Elite Security",     "Elite Security",     "Sword",     15,     925,     128,   70,   60,   50,   150,     0,   2,  -2,   2,   3,   3,  -5,  -2,  -2,   5,   5,  -8,   0,    100,  240,  25, "Null",                 0)
fnAdd("Elite Lord",         "Elite Lord",         "Sword",     15,     800,     150,   70,   60,   40,   150,     0,   2,  -2,   2,   3,   3,  -5,  -2,  -2,   5,   5,  -8,  -5,    400,  260,  25, "Null",                 0)
fnAdd("Security Chief",     "Security Chief",     "Sword",     15,     515,     235,  120,   60,   70,    70,     0,   3,  -1,   3,   4,   4,  -1,  -4,  -4,   6,   6,  -5,   0,    120,  285,  25, "Null",                 0)
fnAdd("Geisha",             "Geisha",             "Claw",      20,    1300,     390,   60,   60,   30,   100,     0,  -2,   3,  -1,  -6,  -2,   2,  -6,  -6,  -3,  -3,   5,   2,    120,  220,  25, "Null",                 0)
fnAdd("Mummy Imp",          "Mummy Imp",          "Claw",      20,     925,     250,  100,   60,   80,    70,     0,  -2,   3,  -1,  -6,   2,   4,  -6,  -6,  -3,  -3,   5,   2,    160,  200,  25, "Null",                 0)
fnAdd("Hoodie",             "Hoodie",             "Sword",     20,    1100,     292,   60,   60,   60,   120,     0,  -2,   3,  -1,  -6,  -2,   2,  -6,  -6,  -3,  -3,   5,   2,     80,  220,  25, "Null",                 0)
fnAdd("Raibie",             "Raibie",             "Claw",      20,    1600,     680,  120,   60,   80,   150,     0,   3,   3,   3,  -4,   4,  20,  -1,  -1,  -5,  -1,   6,2000,    420, 1600,  25, "Null",                 0)
fnAdd("Dreamer",            "Dreamer",            "Claw",      20,    2000,     700,    0,   60,   60,   200,     0,   5,   5,   5,   5,   5,   5,  -6,  -6,   5,   5,   5,2000,      0, 2000,  25, "Null",                 0)

--Mines Only, same stats as above variants but different name for lore reasons.
fnAdd("Insane Golem",       "Insane Golem",       "Sword",     12,     700,     100,   50,   50,   50,   150,     0,   2,  -2,   2,   3,   3,  -5,  -2,  -2,   5,   5,  -8,   0,     24,   48,  25, "Null",                 0)
fnAdd("Insane Lord",        "Insane Lord",        "Sword",     12,     540,     118,   50,   50,   50,   150,     0,   2,  -2,   2,   3,   3,  -5,  -2,  -2,   5,   5,  -8,  -5,    100,   50,  25, "Null",                 0)
fnAdd("Insane Doll",        "Insane Doll",        "Sword",     12,     450,     185,  100,   50,   50,    70,     0,   3,  -1,   3,   4,   4,  -1,  -4,  -4,   6,   6,  -5,   0,     24,   56,  25, "Null",                 0)

--Placeholder.
fnAdd("RaibieChristine",    "Raibie Christine",   "Claw",      20,    2500,    3000,  400,  100,  100,   600,     0,   3,   3,   3,  -4,   4,  20,  -1,  -1,  -5,  -1,   6,2000,    420, 1600,  25, "Null",                 0)

-- |[KO Count Modifiers]|
--None yet.

-- |[ =============================== Special Abilities and Tags =============================== ]|
--Enemies that have more than the standard attack or have special tags get them added here.

-- |[ ====================== Tag Application ===================== ]|
fnTag("Scraprat Scrounger", {{"Is Scraprat", 1}})
fnTag("Wrecked Bot",        {})
fnTag("Motilvac",           {})
fnTag("Mr. Wipey",          {})
fnTag("Secrebot",           {})
fnTag("Scraprat Forager",   {{"Is Scraprat", 1}})
fnTag("Subverted Bot",      {})
fnTag("Void Rift",          {})
fnTag("Waylighter",         {})
fnTag("Latex Drone",        {})
fnTag("Darkmatter",         {})
fnTag("Mad Worker",         {})
fnTag("Mad Lord",           {})
fnTag("Mad Doll",           {})
fnTag("Autovac",            {})
fnTag("Mr. Wipesalot",      {})
fnTag("Attendebot",         {})
fnTag("Scraprat Bomber",    {{"Is Scraprat", 1}})
fnTag("Compromised Bot",    {})
fnTag("Spatial Tear",       {})
fnTag("Lightsman",          {})
fnTag("Latex Packmaster",   {})
fnTag("Starseer",           {})
fnTag("Golem Security",     {})
fnTag("Security Lord",      {})
fnTag("Security Doll",      {})
fnTag("Intellivac",         {})
fnTag("Der Viper",          {})
fnTag("Plannerbot",         {})
fnTag("Billowing Maw",      {})
fnTag("Blind Seer",         {})
fnTag("Latex Security",     {})
fnTag("Nebula Sight",       {})
fnTag("Elite Security",     {})
fnTag("Elite Lord",         {})
fnTag("Security Chief",     {})
fnTag("Geisha",             {})
fnTag("Mummy Imp",          {})
fnTag("Hoodie",             {})
fnTag("Raibie",             {})
fnTag("Dreamer",            {{"Immune To Influence", 1}})
fnTag("RaibieChristine",    {{"Immune To Influence", 1}})

-- |[ ========================= Abilities ======================== ]|
-- |[Common Abilities]|
--Strike
--Pierce
--Flame
--Freeze
--Shock
fnAbility("Raibie", {"Enemy|Shock Attack", 100})

--Crusade
--Obscure
--Bleed
--Poison

--Corrode
fnAbility("Secrebot",   {"Enemy|Corrode Attack", 100})
fnAbility("Attendebot", {"Enemy|Corrode Attack", 100})
fnAbility("Plannerbot", {"Enemy|Corrode Attack", 100})

--Terrify

-- |[Chapter-Specific Abilities]|
--Mr Wipey Series
fnAbility("Mr. Wipey",     {"Enemy|Clean Receptors", 100})
fnAbility("Mr. Wipesalot", {"Enemy|Clean Receptors", 100})
fnAbility("Der Viper",     {"Enemy|Clean Receptors", 100})

--Scraprats
fnAbility("Scraprat Forager", {"Enemy|Scrap Sweep", 100})
fnAbility("Scraprat Bomber", {})

--Security Robot
fnAbility("Subverted Bot",   {"Enemy|Piercing Laser", 100})
fnAbility("Compromised Bot", {"Enemy|Piercing Laser", 100})

--Waylighter
fnAbility("Waylighter", {"Enemy|Groaning Light", 100})
fnAbility("Lightsman",  {"Enemy|Groaning Light", 100})
fnAbility("Blind Seer", {"Enemy|Groaning Light", 100})

--Latex Packmaster
fnAbility("Latex Packmaster", {"Enemy|My Pretties", 100})

-- |[ ======================== Apply AIs ========================= ]|
--AI Builder
LM_ExecuteScript(fnResolvePath() .. "AI Strings.lua")

--AI Application
fnApplyAIString("Latex Packmaster", "Latex Packmaster")

-- |[ ========================================= Topics ========================================= ]|
fnTopic("Scraprat Scrounger",{})
fnTopic("Wrecked Bot", {})
fnTopic("Motilvac", {})
fnTopic("Mr. Wipey", {})
fnTopic("Secrebot", {})
fnTopic("Scraprat Forager", {})
fnTopic("Subverted Bot", {})
fnTopic("Void Rift", {})
fnTopic("Waylighter", {})
fnTopic("Latex Drone", {"LatexDrones", "Inhibitors"})
fnTopic("Darkmatter", {})
fnTopic("Mad Worker", {})
fnTopic("Mad Lord", {})
fnTopic("Mad Doll", {})
fnTopic("Autovac", {})
fnTopic("Mr. Wipesalot", {})
fnTopic("Attendebot", {})
fnTopic("Scraprat Bomber", {})
fnTopic("Compromised Bot", {})
fnTopic("Spatial Tear", {})
fnTopic("Lightsman", {})
fnTopic("Latex Packmaster", {"LatexDrones", "Inhibitors"})
fnTopic("Starseer", {})
fnTopic("Golem Security", {})
fnTopic("Security Lord", {})
fnTopic("Security Doll", {})
fnTopic("Intellivac", {})
fnTopic("Der Viper", {})
fnTopic("Plannerbot", {})
fnTopic("Billowing Maw", {})
fnTopic("Blind Seer", {})
fnTopic("Latex Security", {"LatexDrones", "Inhibitors"})
fnTopic("Nebula Sight", {})
fnTopic("Elite Security", {})
fnTopic("Elite Lord", {})
fnTopic("Security Chief", {})
fnTopic("Geisha", {})
fnTopic("Mummy Imp", {})
fnTopic("Hoodie", {})
fnTopic("Raibie", {})
fnTopic("Dreamer", {})
fnTopic("RaibieChristine", {})

-- |[ ================================= Portrait Offset Lookup ================================= ]|
-- |[Listing]|
--             Portrait Path                                  | Turn Portrait Path                                         | Actor 
fnAddPortrait("Root/Images/Portraits/Combat/Scraprat",         "Root/Images/AdventureUI/TurnPortraits/Scraprat",            "Null")
fnAddPortrait("Root/Images/Portraits/Combat/WreckedBot",       "Root/Images/AdventureUI/TurnPortraits/SecurityWrecked",     "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Motilvac",         "Root/Images/AdventureUI/TurnPortraits/Motilvac",            "Null")
fnAddPortrait("Root/Images/Portraits/Combat/MrWipey",          "Root/Images/AdventureUI/TurnPortraits/MrWipey",             "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Secrebot",         "Root/Images/AdventureUI/TurnPortraits/Secrebot",            "Null")
fnAddPortrait("Root/Images/Portraits/Combat/VoidRift",         "Root/Images/AdventureUI/TurnPortraits/VoidRift",            "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Horrible",         "Root/Images/AdventureUI/TurnPortraits/Horrible",            "Null")
fnAddPortrait("Root/Images/Portraits/Combat/LatexDrone",       "Root/Images/AdventureUI/TurnPortraits/LatexDrone",          "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Plannerbot",       "Root/Images/AdventureUI/TurnPortraits/Plannerbot",          "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Secrebot",         "Root/Images/AdventureUI/TurnPortraits/Secrebot",            "Null")
fnAddPortrait("Root/Images/Portraits/Combat/DarkmatterGirl",   "Root/Images/AdventureUI/TurnPortraits/DarkmatterGirl",      "Null")
fnAddPortrait("Root/Images/Portraits/Combat/GolemSlave",       "Root/Images/AdventureUI/TurnPortraits/Golem",               "Null")
fnAddPortrait("Root/Images/Portraits/Combat/GolemLord",        "Root/Images/AdventureUI/TurnPortraits/GolemLord",           "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Doll",             "Root/Images/AdventureUI/TurnPortraits/Doll",                "Null")
fnAddPortrait("Root/Images/Portraits/Combat/InnGeisha",        "Root/Images/AdventureUI/TurnPortraits/InnGeisha",           "Null")
fnAddPortrait("Root/Images/Portraits/Combat/BandageGoblin",    "Root/Images/AdventureUI/TurnPortraits/BandageGoblin",       "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Hoodie",           "Root/Images/AdventureUI/TurnPortraits/Hoodie",              "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Raibie",           "Root/Images/AdventureUI/TurnPortraits/Raibie",              "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Dreamer",          "Root/Images/AdventureUI/TurnPortraits/Dreamer",             "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Christine_Raibie", "Root/Images/AdventureUI/TurnPortraits/EnemyChristineRaibie","Null")
        
-- |[Mapping]|
fnSetRemap("Scraprat Scrounger", "Root/Images/Portraits/Combat/Scraprat")
fnSetRemap("Wrecked Bot",        "Root/Images/Portraits/Combat/WreckedBot")
fnSetRemap("Motilvac",           "Root/Images/Portraits/Combat/Motilvac")
fnSetRemap("Mr. Wipey",          "Root/Images/Portraits/Combat/MrWipey")
fnSetRemap("Secrebot",           "Root/Images/Portraits/Combat/Secrebot")
fnSetRemap("Scraprat Forager",   "Root/Images/Portraits/Combat/Scraprat")
fnSetRemap("Subverted Bot",      "Root/Images/Portraits/Combat/WreckedBot")
fnSetRemap("Void Rift",          "Root/Images/Portraits/Combat/VoidRift")
fnSetRemap("Waylighter",         "Root/Images/Portraits/Combat/Horrible")
fnSetRemap("Latex Drone",        "Root/Images/Portraits/Combat/LatexDrone")
fnSetRemap("Darkmatter",         "Root/Images/Portraits/Combat/DarkmatterGirl")
fnSetRemap("Mad Worker",         "Root/Images/Portraits/Combat/GolemSlave")
fnSetRemap("Mad Lord",           "Root/Images/Portraits/Combat/GolemLord")
fnSetRemap("Mad Doll",           "Root/Images/Portraits/Combat/Doll")
fnSetRemap("Autovac",            "Root/Images/Portraits/Combat/Motilvac")
fnSetRemap("Mr. Wipesalot",      "Root/Images/Portraits/Combat/MrWipey")
fnSetRemap("Attendebot",         "Root/Images/Portraits/Combat/Secrebot")
fnSetRemap("Scraprat Bomber",    "Root/Images/Portraits/Combat/Scraprat")
fnSetRemap("Compromised Bot",    "Root/Images/Portraits/Combat/WreckedBot")
fnSetRemap("Spatial Tear",       "Root/Images/Portraits/Combat/VoidRift")
fnSetRemap("Lightsman",          "Root/Images/Portraits/Combat/Horrible")
fnSetRemap("Latex Packmaster",   "Root/Images/Portraits/Combat/LatexDrone")
fnSetRemap("Starseer",           "Root/Images/Portraits/Combat/DarkmatterGirl")
fnSetRemap("Golem Security",     "Root/Images/Portraits/Combat/GolemSlave")
fnSetRemap("Security Lord",      "Root/Images/Portraits/Combat/GolemLord")
fnSetRemap("Security Doll",      "Root/Images/Portraits/Combat/Doll")
fnSetRemap("Intellivac",         "Root/Images/Portraits/Combat/Motilvac")
fnSetRemap("Der Viper",          "Root/Images/Portraits/Combat/MrWipey")
fnSetRemap("Plannerbot",         "Root/Images/Portraits/Combat/Plannerbot")
fnSetRemap("Billowing Maw",      "Root/Images/Portraits/Combat/VoidRift")
fnSetRemap("Blind Seer",         "Root/Images/Portraits/Combat/Horrible")
fnSetRemap("Latex Security",     "Root/Images/Portraits/Combat/LatexDrone")
fnSetRemap("Nebula Sight",       "Root/Images/Portraits/Combat/DarkmatterGirl")
fnSetRemap("Elite Security",     "Root/Images/Portraits/Combat/GolemSlave")
fnSetRemap("Elite Lord",         "Root/Images/Portraits/Combat/GolemLord")
fnSetRemap("Security Chief",     "Root/Images/Portraits/Combat/Doll")
fnSetRemap("Geisha",             "Root/Images/Portraits/Combat/InnGeisha")
fnSetRemap("Mummy Imp",          "Root/Images/Portraits/Combat/BandageGoblin")
fnSetRemap("Hoodie",             "Root/Images/Portraits/Combat/Hoodie")
fnSetRemap("Raibie",             "Root/Images/Portraits/Combat/Raibie")
fnSetRemap("Dreamer",            "Root/Images/Portraits/Combat/Dreamer")
fnSetRemap("Insane Golem",       "Root/Images/Portraits/Combat/GolemSlave")
fnSetRemap("Insane Lord",        "Root/Images/Portraits/Combat/GolemLord")
fnSetRemap("Insane Doll",        "Root/Images/Portraits/Combat/Doll")
fnSetRemap("RaibieChristine",    "Root/Images/Portraits/Combat/Christine_Raibie")

-- |[ ======================================== Paragons ======================================== ]|
--Paragons have the same display abilities as the base case, but 10x the stats and 10x the rewards, and +3 all resistances.
-- Their setup must be done after all other entries are added.
local iTotal = #gzStatArray
for i = 1, iTotal, 1 do
    fnCreateParagon(i)
end

-- |[ ========================================== Debug ========================================= ]|
--Store all the data in chapter-local arrays.
gczaChapter5Stats = gzStatArray
gczaChapter5PortraitLookups = gzPortraitArray

--Print all stats to the console.
if(false) then
    io.write("Listing Enemy information:\n")
    for i = 1, #gczaChapter5Stats, 1 do
        io.write(string.format("%2i: %s - %i %i %i %i %i\n", i, gczaChapter5Stats[i].sActualName, gczaChapter5Stats[i].iHealth, gczaChapter5Stats[i].iAtk, gczaChapter5Stats[i].iIni, gczaChapter5Stats[i].iAcc, gczaChapter5Stats[i].iEvd))
    end
end
