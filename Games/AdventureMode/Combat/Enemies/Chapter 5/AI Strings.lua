-- |[ ================================ Build AI String Lookups ================================= ]|
--Called once at program start, builds a list of AIString sets that can be used by enemies by
-- having tags. A set of constants is provided to make them easier to organize.
--Calling AIString:new() will register the string to the global list.

-- |[Repeat Check]|
gbHasBuiltCh5AILookups = nil
if(gbHasBuiltCh5AILookups ~= nil) then return end
gbHasBuiltCh5AILookups = true
local sString = nil

-- |[Latex Packmaster]|
--If nearby scraprat enemies exist, uses "My Pretties" on them every other turn. Otherwise, attacks.
sString = 
"[GVAR,iHasRunOnce,N:==:0]{SVAR:iHasRunOnce:N:1}{SVAR:iMyPrettiesCD:N:Rand,1,2} || " ..
"{TOZERO:iMyPrettiesCD:1} || " ..
"{List:New List:Friendly}{List:Remove Without Tag:'Is Scraprat'} || " ..
"[GVAR,iMyPrettiesCD,N:<=:0][ListSize:>=:1]{SVAR:iMyPrettiesCD:N:2}{Prime:Enemy|My Pretties}{Target:Random}{Finish} || " ..
"{Prime:Enemy|Sword Attack}{Target:Threat}{Finish}"
AIString:new("Latex Packmaster", sString)
