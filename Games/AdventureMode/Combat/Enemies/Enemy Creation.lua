-- |[ ================================ Enemy Creation Functions ================================ ]|
--These creation functions are used by the enemy stat charts in the various chapters.
if(fnStandardEnemyHandler ~= nil) then return end
function fnStandardEnemyHandler(psActualName)
    
    -- |[ =========== Error Check ============ ]|
    --Make sure the entry actually exists.
    zEntry = nil
    if(psActualName == nil) then return 0 end
    
    --Locate the index on the stat chart. Check all the charts, if they exist.
    if(gczaChapter1Stats ~= nil) then
        for i = 1, #gczaChapter1Stats, 1 do
            if(gczaChapter1Stats[i].sActualName == psActualName) then
                zEntry = gczaChapter1Stats[i]
            end
        end
    end
    if(gczaChapter2Stats ~= nil) then
        for i = 1, #gczaChapter2Stats, 1 do
            if(gczaChapter2Stats[i].sActualName == psActualName) then
                zEntry = gczaChapter2Stats[i]
            end
        end
    end
    if(gczaChapter5Stats ~= nil) then
        for i = 1, #gczaChapter5Stats, 1 do
            if(gczaChapter5Stats[i].sActualName == psActualName) then
                zEntry = gczaChapter5Stats[i]
            end
        end
    end
    
    -- |[Scan Mods]|
    --Check if any of the mods respond by populating zEntry.
    fnExecModScript("Combat/Enemies/Enemy List/Create Response.lua", psActualName)
    
    --Error check.
    if(zEntry == nil) then
        io.write("fnStandardEnemyHandler() : - Warning, no listing for " .. psActualName .. " was found.\n")
        return 0
    end

    -- |[ ============== Topics ============== ]|
    --Regardless of mugging or fighting an enemy, any topics created should be handled here.
    for p = 1, #zEntry.saTopics, 1 do
        WD_SetProperty("Unlock Topic", zEntry.saTopics[p], 1)
    end
    
    -- |[ ============ Mug Handler =========== ]|
    --If being mugged, provide a percentage of the base values immediately. Ends execution.
    if(TA_GetProperty("Is Mugging Check") == true) then
        
        --Cash, XP, JP.
        TA_SetProperty("Mug Platina",    math.floor(zEntry.iPlatina * gcfMugPlatinaRate))
        TA_SetProperty("Mug Experience", math.floor(zEntry.iExp     * gcfMugExperienceRate))
        TA_SetProperty("Mug Job Points", math.floor(zEntry.iJP      * gcfMugJPRate))
        
        --Item Roller
        local iItemRoll = LM_GetRandomNumber(1, 100)
        for p = 1, #zEntry.saDrops, 1 do
            if(iItemRoll >= zEntry.saDrops[p][2] and iItemRoll <= zEntry.saDrops[p][3]) then
                TA_SetProperty("Mug Item", zEntry.saDrops[p][1])
            end
        end

        --Stop execution.
        zEntry = nil
        return 0
    end

    -- |[ ========= Combat Encounter ========= ]|
    --Add this entity to the combat roster.
    local iEnemyID = AdvCombat_GetProperty("Generate Unique ID")
    local sEnemyUniqueName = string.format("Autogen|%02i", iEnemyID)
    
    --Create.
    AdvCombat_SetProperty("Register Enemy", sEnemyUniqueName, giReinforcementTurns)

        -- |[System]|
        --Name
        AdvCombatEntity_SetProperty("Display Name", zEntry.sDisplayName)
        AdvCombatEntity_SetProperty("Cluster Name", psActualName)
        
        --Store mug level for auto-win.
        AdvCombatEntity_SetProperty("Level For Mug Auto Win", zEntry.iLevel)
        
        --Get inspector properties.
        local sKOTrackerPath = VM_GetVar("Root/Variables/Global/Combat/sKOTrackerPath", "S")
        local sClusterName = AdvCombatEntity_GetProperty("Cluster Name")
        local iKOsSoFar = VM_GetVar(sKOTrackerPath .. psActualName, "I")
        
        --Never show abilities on the inspector.
        AdvCombatEntity_SetProperty("Inspector Show Abilities", 1)
        
        -- |[Inspector Show Stats]|
        --If this debug flag is set, show the stats.
        local iAlwaysShowStats = VM_GetVar("Root/Variables/Global/Debug/iAlwaysShowStats", "I")
        if(iAlwaysShowStats == 1.0) then
            AdvCombatEntity_SetProperty("Inspector Show Stats", 0)
        
        --If -1, never show stats:
        elseif(zEntry.iKOForStats == -1) then
        
        --Otherwise, compute and set.
        else
            local iKOsNeeded = zEntry.iKOForStats - iKOsSoFar
            if(iKOsNeeded < 1) then iKOsNeeded = 0 end
            AdvCombatEntity_SetProperty("Inspector Show Stats", iKOsNeeded)
        end
        
        -- |[Inspector Show Resistances]|
        --Same variable as above.
        if(iAlwaysShowStats == 1.0) then
            AdvCombatEntity_SetProperty("Inspector Show Resists", 0)
            
        --If -1, never show resists:
        elseif(zEntry.iKOForResists == -1) then
        
        --Otherwise, compute and set.
        else
        
            local iKOsNeeded = zEntry.iKOForResists - iKOsSoFar
            if(iKOsNeeded < 1) then iKOsNeeded = 0 end
            AdvCombatEntity_SetProperty("Inspector Show Resists", iKOsNeeded)
        end

        -- |[Data Library]|
        --DataLibrary Variables
        local iUniqueID = RO_GetID()
        giLastEnemyID = iUniqueID
        DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "/")
        
        --During dialogue, which actor represents this character.
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/sDialogueActor", "S", zEntry.sDialogueActor)
        
        -- |[AI]|
        AdvCombatEntity_SetProperty("AI Script", zEntry.sAIPath)

        -- |[Display]|
        --Images
        AdvCombatEntity_SetProperty("Combat Portrait", zEntry.sPortraitPath)
        AdvCombatEntity_SetProperty("Turn Icon", zEntry.sTurnPortraitPath)
        
        --UI Positions. Call the subscript.
        LM_ExecuteScript(gsEnemyPortraitRouting, zEntry.sPortraitPath)
    
        -- |[Stats and Resistances]|
        --Special: If the debug flag "Paragons Suck" is on, and this is a paragon, give them severe stat penalties.
        local sSuffix = string.sub(psActualName, -7)
        if(ADebug_GetProperty("Do Paragons Suck") == true and sSuffix == "Paragon") then
        
            --Statistics
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_HPMax,      1)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_MPMax,      zEntry.iMPMax)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_CPMax,      zEntry.iCPMax)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Attack,     1)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Initiative, 0)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Accuracy,   0)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Evade,      0)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_StunCap,    zEntry.iStun)
            
            --Resistances
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Protection,     0)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Slash,   0)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Strike,  0)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Pierce,  0)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Flame,   0)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Freeze,  0)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Shock,   0)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Crusade, 0)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Obscure, 0)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Bleed,   0)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Poison,  0)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Corrode, 0)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Terrify, 0)
        
        --Normal case:
        else
        
            --Statistics
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_HPMax,      zEntry.iHealth)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_MPMax,      zEntry.iMPMax)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_CPMax,      zEntry.iCPMax)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Attack,     zEntry.iAtk)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Initiative, zEntry.iIni)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Accuracy,   zEntry.iAcc)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Evade,      zEntry.iEvd)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_StunCap,    zEntry.iStun)
            
            --Resistances
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Protection,     zEntry.iPrt)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Slash,   zEntry.iSls)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Strike,  zEntry.iStk)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Pierce,  zEntry.iPrc)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Flame,   zEntry.iFlm)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Freeze,  zEntry.iFrz)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Shock,   zEntry.iShk)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Crusade, zEntry.iCru)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Obscure, zEntry.iObs)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Bleed,   zEntry.iBld)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Poison,  zEntry.iPsn)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Corrode, zEntry.iCrd)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Terrify, zEntry.iTrf)
        end
        
        --Special: If the flag gbEnemiesSuck is true, enemies get 1 HP, 0 Ini, 0 Evd.
        if(gbEnemiesSuck == true) then
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_HPMax,      1)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Initiative, 0)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Evade,      0)
        end
        
        --Sum.
        AdvCombatEntity_SetProperty("Recompute Stats")
        AdvCombatEntity_SetProperty("Health Percent", 1.00)
        
        -- |[Tag Backup]|
        --Create a backup of the tags listing. This is because some tags get removed before being added to the final entity.
        local zaTagListing = {}
        for i = 1, #zEntry.zaTags, 1 do
            zaTagListing[i] = {}
            zaTagListing[i][1] = zEntry.zaTags[i][1]
            zaTagListing[i][2] = zEntry.zaTags[i][2]
        end
        
        --Run the handler to spawn effects.
        fnHandleTagEffects(iUniqueID, zaTagListing)
        
        --Remove tags with certain patterns.
        local i = 1
        local iCap = #zaTagListing
        while(i <= iCap) do
            
            --Debug.
            --io.write(" Tag is " .. zaTagListing[i][1] .. " with count " .. zaTagListing[i][2] .. "\n")
            
            --Check for a match.
            if(string.sub(zaTagListing[i][1], 1, 13) == "Spawn Effect|") then
                --io.write(" Removed.\n")
                table.remove(zaTagListing, i)
                i = i - 1
                iCap = iCap - 1
            end
            
            --Next.
            i = i + 1
        end
        
        -- |[Tags]|
        --Tags are non-standard properties that are used by the scripts to compute bonuses that cannot be easily
        -- communicated via resistances, or other special properties like cutscene stuff.
        --The tags array is formatted {{"TagA", iCount}, {"TagB", iCount}, etc}
        for p = 1, #zEntry.zaTags, 1 do
            AdvCombatEntity_SetProperty("Add Tag", zEntry.zaTags[p][1], zEntry.zaTags[p][2])
        end

        -- |[Abilities]|
        --System Abilities
        fnEnemyStandardSystemAbilities()

        --Register abilities.
        local iRegX = 0
        local iRegY = 0
        for p = 1, #zEntry.saAbilityList, 1 do

            --Set.
            LM_ExecuteScript(zEntry.saAbilityList[p][2], gciAbility_Create)
            AdvCombatEntity_SetProperty("Set Ability Slot",             iRegX, iRegY, zEntry.saAbilityList[p][1])
            AdvCombatEntity_SetProperty("Set Ability Slot Probability", iRegX, iRegY, zEntry.saAbilityList[p][3])

            --Increment. Check if we need to move the line down.
            iRegX = iRegX + 1
            if(iRegX >= gciAbilityGrid_XSize) then
                iRegX = 0
                iRegY = iRegY + 1
                if(iRegY >= gciAbilityGrid_YSize) then 
                    break
                end
            end
        end

        -- |[JP Level Scaling]|
        --The enemy has a level value which dictates how much JP is gained for besting them. Only the party leader
        -- is queried. For each level the party leader is over the enemy, -5 JP is subtracted, until a minimum of 5.
        --Fighting enemies above your level gives bonus JP, up to 50.
        local iLeaderLevel = 1
        local iJPValue = zEntry.iJP
        AdvCombat_SetProperty("Push Party Member By Slot", 0)
            iLeaderLevel = AdvCombatEntity_GetProperty("Level")
        DL_PopActiveObject()
        local iLevelDif = (zEntry.iLevel - iLeaderLevel) * 5
        iJPValue = iJPValue + iLevelDif
        if(iJPValue <  5) then iJPValue =  5 end
        if(iJPValue > 50) then iJPValue = 50 end

        -- |[Rewards Handling]|
        --Factor for mugging.
        local fXPFactor = 1.0
        local fJPFactor = 1.0
        local fPLFactor = 1.0
        if(TA_GetProperty("Was Mugged") == true) then
            fXPFactor = 1.0 - gcfMugExperienceRate
            fJPFactor = 1.0 - gcfMugPlatinaRate
            fPLFactor = 1.0 - gcfMugJPRate
        end
        
        --Rewards
        AdvCombatEntity_SetProperty("Reward XP",      math.floor(zEntry.iExp     * fXPFactor))
        AdvCombatEntity_SetProperty("Reward JP",      math.floor(iJPValue        * fJPFactor))
        AdvCombatEntity_SetProperty("Reward Platina", math.floor(zEntry.iPlatina * fPLFactor))
        AdvCombatEntity_SetProperty("Reward Doctor", -1)
        
        --Item rewards. These only apply if the entity was not mugged.
        if(TA_GetProperty("Was Mugged") == false) then
            
            --Roll per-enemy.
            local iItemRoll = LM_GetRandomNumber(1, 100)
            
            --Scan drops.
            for p = 1, #zEntry.saDrops, 1 do
                if(iItemRoll >= zEntry.saDrops[p][2] and iItemRoll <= zEntry.saDrops[p][3]) then
                    AdvCombatEntity_SetProperty("Reward Item", zEntry.saDrops[p][1])
                end
            end
        end
        
    DL_PopActiveObject()
    
    --Return the generated ID.
    zEntry = nil
    return iUniqueID
end

--Scan across the tags that start with "Spawn Effect|". These create an effect that may have properties built into the
-- tag name. These get removed before being added to the entity.
function fnHandleTagEffects(piEntityID, pzaTagListing)
    
    -- |[Argument Check]|
    if(piEntityID    == nil) then return end
    if(pzaTagListing == nil) then return end

    -- |[Scan]|
    --io.write("Scanning tag list for effect spawning. There are " .. #pzaTagListing .. " tags.\n")
    for i = 1, #pzaTagListing, 1 do
        
        --Debug.
        --io.write(" Tag is " .. pzaTagListing[i][1] .. " with count " .. pzaTagListing[i][2] .. "\n")
        
        --Check for a match.
        if(string.sub(pzaTagListing[i][1], 1, 13) == "Spawn Effect|") then
        
            --Debug.
            --io.write(" Matches Spawn Effect.\n")
        
            --Break the string down by the delimiter | to get the arguments.
            local saWordList = fnSubdivide(pzaTagListing[i][1], "|")
        
            --The second string is the name of the effect prototype.
            local sPrototypeName = saWordList[2]
            --io.write(" Prototype name: " .. saWordList[2] .. "\n")
                
            --Get the prototype.
            local iEffectType, zEffect, x = fnLocateEffectPrototype(saWordList[2])
            if(iEffectType == nil) then
                io.write("fnHandleTagEffects(): Warning, no effect prototype " .. saWordList[2] .. " was found.\n")
            else
                --Build effect string.
                local sEffectString = "Effect|" .. gsGlobalEffectPrototypePath .. "|Prototype:" .. saWordList[2]
                
                --Provide additional arguments.
                for q = 3, #saWordList, 1 do
                    local sArgString = "|Arg" .. math.floor(q-3) .. ":" .. saWordList[q]
                    sEffectString = sEffectString .. sArgString
                end
                
                --Run effect.
                AdvCombat_SetProperty("Register Application Pack", piEntityID, piEntityID, 0, sEffectString)
            end
        end
    end
end
