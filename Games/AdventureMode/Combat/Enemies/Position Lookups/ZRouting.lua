-- |[ ================================ Position Lookups, Routing =============================== ]|
--During enemy creation, this script is called with the DLPath to the portrait passed in. It then
-- runs across all the subscripts to locate the UI positions for that portrait.
gbFoundPortrait = false
local sBasePath = fnResolvePath()

-- |[Arguments]|
if(fnArgCheck(1) == false) then return end
local sDLPath = LM_GetScriptArgument(0)

-- |[Execute subscripts]|
--If gbFoundPortrait is set to true, we're done.
LM_ExecuteScript(sBasePath .. "Position Lookups Ch1.lua", sDLPath)
if(gbFoundPortrait) then return end
LM_ExecuteScript(sBasePath .. "Position Lookups Ch2.lua", sDLPath)
if(gbFoundPortrait) then return end
LM_ExecuteScript(sBasePath .. "Position Lookups Ch5.lua", sDLPath)
if(gbFoundPortrait) then return end

-- |[Mods]|
--Scan the mods and see if any of them reply.
gbFoundPortrait = fnExecModScript("Combat/Enemies/Position Lookups/Position Lookups.lua", sDLPath)
if(gbFoundPortrait) then
    return 
end

-- |[Failed]|
--If we got this far, none of the subscripts had portrait information. Set everything to zeroes
-- and print a warning.
io.write("Warning: No position lookup found for: " .. sDLPath .. "\n")
AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu,         0, 0)
AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,      0, 0)
AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,      0, 0)
AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 0, 0)
AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equipment,        0, 0)
AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Skills,           0, 0)
AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status,           0, 0)
AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Vendor,           0, 0)
AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Victory,   0, 0)
AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Enemy,     0, 0)
