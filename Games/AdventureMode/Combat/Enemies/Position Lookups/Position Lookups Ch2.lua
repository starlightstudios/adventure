-- |[ ============================== Position Lookups, Chapter 2 =============================== ]|
--This file is called with a path to an image. That information is then used to set the UI positions
-- for that image. This file is for enemies specific to chapter 2.

-- |[Arguments]|
if(fnArgCheck(1) == false) then return end
local sDLPath = LM_GetScriptArgument(0)

--Set flag.
gbFoundPortrait = true

-- |[Strip]|
--Start at the end of the path and count backwards until a / is found. Use only the last part.
-- This allows paragons to use the same alignments.
local i = 0
while true do
    local p = string.find(sDLPath, "/", i+1)
    if p == nil then break end
    i = p
end

--Get the ending string.
sDLPath = string.sub(sDLPath, i+1)

-- |[ ========= Chart Setup ======== ]|
if(gzaCh2PosLookups == nil) then
    
    -- |[Setup]|
    local zaChart = {}
    
    --Function.
    local function fnAddPos(psName, pzBase, pzAlly, pzInspector, pzEnemy)
        local zEntry = {}
        zEntry.sName = psName
        zEntry.zBase = pzBase
        zEntry.zAlly = pzAlly
        zEntry.zInsp = pzInspector
        zEntry.zEnmy = pzEnemy
        table.insert(zaChart, zEntry)
    end
    
    -- |[Chart]|
    --       Name             Base          Ally          Inspector     Enemy
    fnAddPos("Bat",           { 228,   71}, {-105, -150}, { 249,  104}, {   0,  100})
    fnAddPos("Brimhog",       {  50,  109}, { -42, -158}, { 282,   22}, {   0,   89})
    fnAddPos("Buffodil",      {   6,   92}, {-327, -106}, {  62,  133}, {   0,   92})
    fnAddPos("Bunny",         { 100,   96}, {-240,  -57}, { 131,   97}, {   0,   90})
    fnAddPos("Butterbomber",  {  79,   16}, {-225, -227}, { 136,   39}, {   0,   57})
    fnAddPos("CaveCrawler",   { 142,   95}, { -29, -240}, { 227,  103}, {   0,  119})
    fnAddPos("Enforcer",      { 100,   96}, {-240,  -57}, { 131,   97}, {   0,   90})
    fnAddPos("HarpyOfficer",  { 256,  120}, { -77,  -37}, { 299,   32}, {   0,  102})
    fnAddPos("HarpyRecruit",  { 256,  120}, { -77,  -37}, { 299,   32}, {   0,  102})
    fnAddPos("Kitsune",       { 275,  222}, { -56,  -16}, { 330,  119}, {   0,  186})
    fnAddPos("Omnigoose",     { 290,  128}, {  13,   -6}, { 337,  100}, {   0,   94})
    fnAddPos("PoisonVine",    { 213,   43}, { -97, -146}, { 267,   52}, {   0,  119})
    fnAddPos("Sevavi",        { 220,  106}, {-112,  -26}, { 271,  107}, {   0,   80})
    fnAddPos("Treant",        {  23,   -7}, {-305, -157}, {  67,   -9}, {   0,  -33})
    fnAddPos("Turkerus",      { 277,  133}, {  28,    5}, { 315,  194}, {   0,  137})
    fnAddPos("Unielk",        {  79,   19}, {  12,  -99}, { 251,   16}, {   0,   17})
    fnAddPos("BunEye",        {  308, 110}, {  13, -134}, { 350,  116}, {   0,   90})
    fnAddPos("BanditRed",     {  183,  87}, {-125,  -48}, { 247,   84}, {   0,   76})
    fnAddPos("BanditGreen",   {  183,  87}, {-125,  -48}, { 247,   84}, {   0,   76})
    fnAddPos("FrostCrawler",  {  212, 200}, {-117, -116}, { 255,  200}, {   0,  184})
    fnAddPos("Grub",          {  267, 215}, { -13,  -31}, { 263,  193}, {   0,  197})
    fnAddPos("GrubNest",      {  142,  -7}, {-162, -227}, { 204,   16}, {   0,   -8})
    fnAddPos("Pseudogriffon", {  292, 218}, {  15,  -21}, { 328,  225}, {   0,  211})
    fnAddPos("RedCap",        {  116,   7}, {  -1, -287}, { 237,   23}, {   0,   13})
    fnAddPos("SuckFly",       {  231, 129}, { -32,  -71}, { 305,  116}, {   0,  128})
    fnAddPos("Toxishroom",    {  266, 157}, { -56,  -18}, { 324,  127}, {   0,  184})
    fnAddPos("WispHag",       {   47,  35}, {-274, -200}, {  93,   42}, {   0,   48})
    
    -- |[Finish Up]|
    gzaCh2PosLookups = zaChart
end

-- |[ ======= Chart Scanning ======= ]|
for i = 1, #gzaCh2PosLookups, 1 do
    if(sDLPath == gzaCh2PosLookups[i].sName) then
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,      gzaCh2PosLookups[i].zBase[1],  gzaCh2PosLookups[i].zBase[2])
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,      gzaCh2PosLookups[i].zAlly[1],  gzaCh2PosLookups[i].zAlly[2])
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, gzaCh2PosLookups[i].zInsp[1],  gzaCh2PosLookups[i].zInsp[2])
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Enemy,     gzaCh2PosLookups[i].zEnmy[1],  gzaCh2PosLookups[i].zEnmy[2])
        return
    end
end

-- |[Not Found]|
--Set the found portrait flag to false.
gbFoundPortrait = false
