-- |[ ======================================= Power Gate ======================================= ]|
-- |[Description]|
--Effect that applies attack power bonuses such that the target never has below 30% of their base
-- attack power.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Local Variables ===================================== ]|
-- |[System]|
--This effect uses a standardized StatMod script.
local sDisplayName = "Power Gate"
local iDuration = -1
local bIsBuff = true
local sIcon = "Root/Images/AdventureUI/Abilities/GenBuff"
local sStatString = " "

--Description can be a variable number of lines.
local saDescription = {}
saDescription[1] = "This enemy will never have below 30%% of its base"
saDescription[2] = "attack power regardless of other effects."

--Run the standardized builder.
gzaStatModStruct            = fnCreateStatmodEffectPrototype(sDisplayName, iDuration, bIsBuff, sIcon, sStatString)
gzaStatModStruct.saStrings  = fnStatmodBuildInspectorStrings(gzaStatModStruct, saDescription)
gzaStatModStruct.sShortText = fnStatmodBuildShortText(gzaStatModStruct)

-- |[Overrides]|
--None Yet.

-- |[ ======================================== Creation ======================================== ]|
--Standard call when effect is initialized.
if(iSwitchType == gciEffect_Create) then
    LM_ExecuteScript(gsStandardStatPath, gciEffect_Create)
    
-- |[ ================================ Character Action Begins ================================= ]|
--When the owner begins their action, checks all other effects and computes the required bonus.
elseif(iSwitchType == gciEffect_BeginAction) then
    
    -- |[Variables]|
    --ID of Effect.
    local iUniqueID = RO_GetID()

    --Push the owner.
    local iTargetID = AdvCombatEffect_GetProperty("Get ID Of Target", 0)
    AdvCombat_SetProperty("Push Entity By ID", iTargetID)
        local sTargetName = AdvCombatEntity_GetProperty("Display Name")
        local iAttackPowerBase      = AdvCombatEntity_GetProperty("Statistic", gciStatGroup_Base,        gciStatIndex_Attack)
        local iAttackPowerPerma     = AdvCombatEntity_GetProperty("Statistic", gciStatGroup_PermaEffect, gciStatIndex_Attack)
        local iAttackPowerTemp      = AdvCombatEntity_GetProperty("Statistic", gciStatGroup_TempEffect,  gciStatIndex_Attack)
        local iAttackPowerFinal     = AdvCombatEntity_GetProperty("Statistic", gciStatGroup_Final,       gciStatIndex_Attack)
    DL_PopActiveEntity()
    
    --Computations.
    local iMaxReductionAllowed = math.floor(iAttackPowerBase * -0.30)
    
    --Debug.
    if(false) then
        io.write("Power Gate Report:\n")
        io.write(" ID of Owner/Target is " .. iTargetID .. "\n")
        io.write(" Target name: " .. sTargetName .. "\n")
        io.write("  Power Base: " .. iAttackPowerBase .. "\n")
        io.write("  Power Perm: " .. iAttackPowerPerma .. "\n")
        io.write("  Power Temp: " .. iAttackPowerTemp .. "\n")
        io.write("  Power Finl: " .. iAttackPowerFinal .. "\n")
        io.write(" Maximum allowed reduction: " .. iMaxReductionAllowed .. "\n")
    end
    
    --Check if the final power is below 30% of the base power. If so, provide a buff for the difference.
    if(iMaxReductionAllowed > iAttackPowerTemp) then
        
        --Compute.
        local iBonus = iMaxReductionAllowed - iAttackPowerTemp
        
        --Debug.
        io.write(" Detected power penalty! Modifying bonus.\n")
        io.write("  Computed bonus: " .. iBonus .. "\n")
        
        --Set it on the "Perma" stat instead of the temp stat. This is so the gate doesn't conflict
        -- with itself.
        AdvCombat_SetProperty("Push Entity By ID", iTargetID)
            AdvCombatEntity_SetProperty("Statistic", gciStatGroup_PermaEffect, gciStatIndex_Attack, iBonus)
            AdvCombatEntity_SetProperty("Recompute Stats")
        DL_PopActiveEntity()
    end
    
-- |[ ================================== Character Action Ends ================================= ]|
--Called after a character action ends to decrement the turn counter.
elseif(iSwitchType == gciEffect_PostAction) then
end
