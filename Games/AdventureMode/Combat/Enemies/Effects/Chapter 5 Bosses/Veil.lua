-- |[ ========================================== Veil ========================================== ]|
-- |[Description]|
--Deals obscuring damage equal to 1.50x attack power over 3 turns.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--Setup, apply overrides.
if(gzEffectCH5BossVeil == nil) then
    
    --Base
    local zEffectStruct = fnCreateDoTEffectPrototype(gciDamageType_Obscuring, 3, 1.50)
    
    --System/Display
    zEffectStruct.sDisplayName = "Veil"
    zEffectStruct.sIcon = "GenDebuff"
    zEffectStruct.sAnimation = "Shadow B"
    
    --Store
    gzEffectCH5BossVeil = zEffectStruct
    
end

--Standardized Naming
gzRefEffect = gzEffectCH5BossVeil

--Call the standard handler. It will nil off the gzRefEffect.
LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType)