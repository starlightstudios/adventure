-- |[ ======================================== Irradiate ======================================= ]|
-- |[Description]|
--Deals corroding damage equal to 3x attack power over 5 turns.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--Setup, apply overrides.
if(gzEffectCH5BossIrradiate == nil) then
    
    --Base
    local zEffectStruct = fnCreateDoTEffectPrototype(gciDamageType_Corroding, 5, 3.00)
    
    --System/Display
    zEffectStruct.sDisplayName = "Irradiate"
    zEffectStruct.sIcon = "GenDebuff"
    zEffectStruct.sDisplayOnApply = "Radioactive!"

    --Tags
    zEffectStruct.zaTagList = {{"Is Negative", 1}, {"Corrode DoT", 1}}
    
    --Store
    gzEffectCH5BossIrradiate = zEffectStruct
    
end

--Standardized Naming
gzRefEffect = gzEffectCH5BossIrradiate

--Call the standard handler. It will nil off the gzRefEffect.
LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType)