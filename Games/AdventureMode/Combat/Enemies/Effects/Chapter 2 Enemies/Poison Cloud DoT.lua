-- |[ ==================================== Poison Cloud DoT ==================================== ]|
-- |[Description]|
--Deals poisoning damage equal to 0.50 of user's attack power over 5 turns. 

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--Setup, apply overrides.
if(gzEffectEnemyPoisonCloudDoT == nil) then
    
    --Base, overrides.
    local zEffectStruct = fnCreateDoTEffectPrototype(gciDamageType_Poisoning, 5, 0.50)
    zEffectStruct.sDisplayName = "Poison Cloud (DoT)"
    zEffectStruct.sIcon = "GenPoison"
    
    --Tags
    zEffectStruct.zaTagList = {{"Is Negative", 1}, {"Poisoning DoT", 1}}
    
    --Store
    gzEffectEnemyPoisonCloudDoT = zEffectStruct
    
end

-- |[Set and Execute]|
--Standardized Naming
gzRefEffect = gzEffectEnemyPoisonCloudDoT

--Call the standard handler. It will nil off the gzRefEffect.
LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType)
