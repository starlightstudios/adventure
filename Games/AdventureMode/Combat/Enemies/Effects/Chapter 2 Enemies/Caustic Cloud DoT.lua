-- |[ =================================== Caustic Cloud DoT ==================================== ]|
-- |[Description]|
--Deals corroding damage equal to 0.30 of user's attack power over 3 turns. 

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--Setup, apply overrides.
if(gzEffectEnemyCausticCloudDoT == nil) then
    
    --Base, overrides.
    local zEffectStruct = fnCreateDoTEffectPrototype(gciDamageType_Poisoning, 3, 1.00)
    zEffectStruct.sDisplayName = "Caustic Cloud"
    zEffectStruct.sIcon = "GenCorrode"
    
    --Tags
    zEffectStruct.zaTagList = {{"Is Negative", 1}, {"Corroding DoT", 1}}
    
    --Store
    gzEffectEnemyCausticCloudDoT = zEffectStruct
    
end

-- |[Set and Execute]|
--Standardized Naming
gzRefEffect = gzEffectEnemyCausticCloudDoT

--Call the standard handler. It will nil off the gzRefEffect.
LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType)
