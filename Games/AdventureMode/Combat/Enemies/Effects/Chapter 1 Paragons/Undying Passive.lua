-- |[ ========================================= Undying ======================================== ]|
-- |[Description]|
--Increases all resistances by 3 when HP is under 50%.
--Not used

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Local Variables ===================================== ]|
-- |[System]|
--This effect uses a standardized StatMod script.
local sDisplayName = "Undying"
local iDuration = -1
local bIsBuff = true
local sIcon = "Root/Images/AdventureUI/Abilities/Mei|Undying"
local sStatString = ""

--Description can be a variable number of lines.
local saDescription = {}
saDescription[1] = "Death is nothing to those who embrace it."
saDescription[2] = "+3 all resistances when under 50%% health."

--Run the standardized builder.
gzaStatModStruct            = fnCreateStatmodEffectPrototype(sDisplayName, iDuration, bIsBuff, sIcon, sStatString)
gzaStatModStruct.saStrings  = fnStatmodBuildInspectorStrings(gzaStatModStruct, saDescription)
gzaStatModStruct.sShortText = fnStatmodBuildShortText(gzaStatModStruct)

-- |[Overrides]|
--Passive, not removed on KO, uses different frame.
gzaStatModStruct.sFrame      = gsAbility_Frame_Passive
gzaStatModStruct.bRemoveOnKO = false

--Manually specify description.
gzaStatModStruct.saStrings[1] = "[Buff]Undying"
gzaStatModStruct.saStrings[2] = "[UpN]+Resists"
gzaStatModStruct.saStrings[3] = saDescription[1]
gzaStatModStruct.saStrings[4] = saDescription[2]
gzaStatModStruct.saStrings[5] = ""
gzaStatModStruct.saStrings[6] = ""
gzaStatModStruct.saStrings[7] = ""
gzaStatModStruct.saStrings[8] = "[Buff] +All Resists"
gzaStatModStruct.sShortText = ""

-- |[ ======================================== Creation ======================================== ]|
--Special: Marks this effect as not being applied yet.
if(iSwitchType == gciEffect_Create) then
    LM_ExecuteScript(gsStandardStatPath, gciEffect_Create)
    
    --Track whether or not the effect is currently applying.
    local iUniqueID = RO_GetID()
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iIsApplied", "N", 0.0)
    AdvCombatEffect_SetProperty("Set Hidden On UI", true)
    
-- |[ ======================================= Apply Stats ====================================== ]|
--Special code, when checking to un-apply bonuses, only does so if the effect is actually applied.
elseif((iSwitchType == gciEffect_ApplyStats or iSwitchType == gciEffect_UnApplyStats) and iArgumentsTotal >= 2) then

    -- |[Setup]|
    --Get ID of this Effect.
    local iUniqueID = RO_GetID()
    
    --Get ID of target.
    local iTargetID = math.floor(tonumber(LM_GetScriptArgument(1, "N")))
    if(iTargetID < 1) then return end
    
    -- |[Display]|
    --Run subroutines to upload the description strings.
    AdvCombatEffect_SetProperty("Allocate Description Strings", iTargetID, #gzaStatModStruct.saStrings)
    LM_ExecuteScript(gsStandardStatPath, gciEffect_StatMod_SetInspectorTitle, gzaStatModStruct.saStrings[1], gzaStatModStruct.saStrings[2], iTargetID)
    for i = 3, #gzaStatModStruct.saStrings, 1 do
        LM_ExecuteScript(gsStandardStatPath, gciEffect_StatMod_SetDescription, iTargetID, i-1, gzaStatModStruct.saStrings[i])
    end

    -- |[Control]|
    --Apply stats never actually fires. This section only is needed for removing the effect when combat ends.
    if(iSwitchType == gciEffect_ApplyStats) then return end

    --If the effect hasn't applied yet, do nothing. Can't remove what wasn't applied.
    local iIsApplied = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iIsApplied", "N")
    if(iIsApplied == 0.0) then return end

    --Push, remove stats.
    AdvCombat_SetProperty("Push Entity By ID", iTargetID)
        for i = gciStatIndex_Resist_Start, gciStatIndex_Resist_End, 1 do
            fnApplyStatBonus(i, -3)
        end
    DL_PopActiveObject()

-- |[ ============================= Combat: Character Action Begins ============================ ]|
--Called when the character that possesses this Effect begins their turn. Checks if they need to
-- actually apply the stats or not based on their HP.
elseif(iSwitchType == gciAbility_BeginAction) then

    --Variables.
    local bIsHidden = true
    local iUniqueID = RO_GetID()
    local iIsApplied = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iIsApplied", "N")
    
    --Get the target's ID.
    local iTargetID = AdvCombatEffect_GetProperty("Get ID Of Target", 0)
    AdvCombat_SetProperty("Push Entity By ID", iTargetID)

        --Get HP.
        local iHP    = AdvCombatEntity_GetProperty("Health")
        local iHPMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)

        --Over 50%.
        if(iHP >= iHPMax * 0.50) then
            bIsHidden = true

            --If the ability is not applied, do nothing:
            if(iIsApplied == 0.0) then
            
            --Remove it.
            else
                
                --Set flag.
                VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iIsApplied", "N", 0)
                
                --Remove resistance.
                for i = gciStatIndex_Resist_Start, gciStatIndex_Resist_End, 1 do
                    fnApplyStatBonus(i, -3)
                end
            end

        --Under 50%.
        else
            bIsHidden = false

            --If the ability is not applied, apply it:
            if(iIsApplied == 0.0) then
                
                --Set flag.
                VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iIsApplied", "N", 1)
                
                --Apply resistance.
                for i = gciStatIndex_Resist_Start, gciStatIndex_Resist_End, 1 do
                    fnApplyStatBonus(i, 3)
                end
            
            --Do nothing.
            else
                
            end
        end
    DL_PopActiveObject()
    
    --Modify visibility.
    AdvCombatEffect_SetProperty("Set Hidden On UI", bIsHidden)

end
