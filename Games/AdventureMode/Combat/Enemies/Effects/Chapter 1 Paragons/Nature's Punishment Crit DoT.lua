-- |[ ==================================== Nature's Punishment Crit DoT ==================================== ]|
-- |[Description]|
--Deals poisoning damage equal to 0.60 of user's attack power over 3 turns. Meant to be consumed by Roses Riposte used by alrune paragon family.
--identical to roses poison but with a cooler name

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--Setup, apply overrides.
if(gzEffectNaturesPunishmentCritDoT == nil) then
    
    --Base, overrides.
    local zEffectStruct = fnCreateDoTEffectPrototype(gciDamageType_Poisoning, 3, 0.60)
    zEffectStruct.sDisplayName = "Nature's Punishment (DoT)"
    zEffectStruct.sIcon = "GenPoison"
    
    --Tags
    zEffectStruct.zaTagList = {{"Is Negative", 1}, {"Poisoning DoT", 1}}
    
    --Store
    gzEffectEnemyNaturesPunishmentCritDoT = zEffectStruct
    
end

-- |[Set and Execute]|
--Standardized Naming
gzRefEffect = gzEffectEnemyNaturesPunishmentCritDoT

--Call the standard handler. It will nil off the gzRefEffect.
LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType)

