-- |[ ==================================== Fumble Self ==================================== ]|
-- |[Description]|
--Reduces evasion and iniative slightly for 5 turns. 
--Designed for use with the rubber family of paragons.
--Originally meant to randomly decrease two resistances.


-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Local Variables ===================================== ]|
-- |[System]|
--This effect uses a standardized StatMod script.
local sDisplayName = "Fumble"
local iDuration = 5
local bIsBuff = true
local sIcon = "Root/Images/AdventureUI/Abilities/GenDebuff"
local sStatString = "Evd|Flat|-20 Ini|Flat|-20"

local saDescription = {}
saDescription[1] = "Those bappy hands really aren't"
saDescription[2] = "good for anything."
saDescription[3] = "Reduces [Evd] and [Ini]."

--Run the standardized builder.
gzaStatModStruct            = fnCreateStatmodEffectPrototype(sDisplayName, iDuration, bIsBuff, sIcon, sStatString)
gzaStatModStruct.saStrings  = fnStatmodBuildInspectorStrings(gzaStatModStruct, saDescription)
gzaStatModStruct.sShortText = fnStatmodBuildShortText(gzaStatModStruct)

-- |[Overrides]|
--Tags.

-- |[ ======================================== Creation ======================================== ]|
--Standard call when effect is initialized.
if(iSwitchType == gciEffect_Create) then
    LM_ExecuteScript(gsStandardStatPath, gciEffect_Create, fSeverity)

-- |[ ================================== Apply/Unapply Stats =================================== ]|
--Calls the apply/unapply stat routines.
elseif((iSwitchType == gciEffect_ApplyStats or iSwitchType == gciEffect_UnApplyStats) and iArgumentsTotal >= 2) then

    --Arguments.
    local iTargetID = math.floor(tonumber(LM_GetScriptArgument(1, "N")))
    
    --Set the stat string to its default.
    gzaStatModStruct.sStatString = sStatString

    --Execute standard handler.
    LM_ExecuteScript(gsStandardStatPath, iSwitchType, iTargetID)

-- |[ ================================== Character Action Ends ================================= ]|
--Called after a character action ends to decrement the turn counter.
elseif(iSwitchType == gciEffect_PostAction) then
    LM_ExecuteScript(gsStandardStatPath, gciEffect_PostAction)
end
