-- |[ ==================================== Slime Balloon Crit DoT ==================================== ]|
-- |[Description]|
--Deals poisoning damage equal to 60% of user's attack power over 3 turns. Designed for use with Corrosive Slime paragon and based on silly things meryl said.
--Was originally meant to be corroding but player cannot clear corrode dot in chapter 1.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--Setup, apply overrides.
if(gzEffectEnemySlimeRendDoT == nil) then
    
    --Base, overrides.
    local zEffectStruct = fnCreateDoTEffectPrototype(gciDamageType_Corroding, 3, 1.20)
    --local zEffectStruct = fnCreateDoTEffectPrototype(gciDamageType_Poisoning, 3, 0.60)
    zEffectStruct.sDisplayName = "Slime Rend (DoT)"
    --zEffectStruct.sIcon = "GenCorrode"
    zEffectStruct.sIcon = "GenCorrode"
    
    --Tags
    zEffectStruct.zaTagList = {{"Is Negative", 1}, {"Corroding DoT", 1}}
    
    --Store
    gzEffectEnemySlimeRendDoT = zEffectStruct
    
end

-- |[Set and Execute]|
--Standardized Naming
gzRefEffect = gzEffectEnemySlimeRendDoT

--Call the standard handler. It will nil off the gzRefEffect.
LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType)

