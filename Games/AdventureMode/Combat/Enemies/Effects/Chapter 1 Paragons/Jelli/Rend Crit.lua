-- |[ ========================================== Rend Crit Dot ========================================== ]|
-- |[Description]|
--Deals bleeding damage equal to 2.00 of user's attack power over 5 turns.
--Designed for use with the Jelli paragon and adapted from Mei's skills

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--Setup, apply overrides.
if(gzEffectFencerRendCrit == nil) then
    
    --Base, overrides.
    local zEffectStruct = fnCreateDoTEffectPrototype(gciDamageType_Bleeding, 5, 2.00)
    zEffectStruct.sDisplayName = "Rend"
    zEffectStruct.sIcon = "Mei|Rend"
    
    --Store
    gzEffectFencerRendCrit = zEffectStruct
    
end

-- |[Set and Execute]|
--Standardized Naming
gzRefEffect = gzEffectFencerRendCrit

--Call the standard handler. It will nil off the gzRefEffect.
LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType)
