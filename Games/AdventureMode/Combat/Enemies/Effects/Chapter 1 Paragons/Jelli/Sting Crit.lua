-- |[ ========================================== Sting Crit Dot ========================================= ]|
-- |[Description]|
--Deals poisoning damage equal to 2.00 of user's attack power over 5 turns. Crit adds 2 turns.
--Designed for use with the Jelli paragon and adapted from Mei's skills

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--Setup, apply overrides.
if(gzEffectHiveScoutStingCrit == nil) then
    
    --Base, overrides.
    local zEffectStruct = fnCreateDoTEffectPrototype(gciDamageType_Poisoning, 5, 2.00)
    zEffectStruct.sDisplayName = "String"
    zEffectStruct.sIcon = "Mei|Sting"
    
    --Store
    gzEffectHiveScoutStingCrit = zEffectStruct
    
end

-- |[Set and Execute]|
--Standardized Naming
gzRefEffect = gzEffectHiveScoutStingCrit

--Call the standard handler. It will nil off the gzRefEffect.
LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType)
