-- |[ ======================================== Goopshot Crit Dot ======================================== ]|
-- |[Description]|
--Deals poisoning damage equal to 1.25 of user's attack power over 5 turns.
--Designed for use with the Jelli paragon and adapted from Mei's skills

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--Setup, apply overrides.
if(gzEffectSmartySageGoopshotCrit == nil) then
    
    --Base, overrides.
    local zEffectStruct = fnCreateDoTEffectPrototype(gciDamageType_Corroding, 5, 1.25)
    zEffectStruct.sDisplayName = "Goopshot"
    zEffectStruct.sIcon = "Mei|Goopshot"
    
    --Store
    gzEffectSmartySageGoopshotCrit = zEffectStruct
    
end

-- |[Set and Execute]|
--Standardized Naming
gzRefEffect = gzEffectSmartySageGoopshotCrit

--Call the standard handler. It will nil off the gzRefEffect.
LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType)
