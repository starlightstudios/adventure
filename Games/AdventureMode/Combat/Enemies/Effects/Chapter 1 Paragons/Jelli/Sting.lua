-- |[ ========================================== Sting ========================================= ]|
-- |[Description]|
--Deals poisoning damage equal to 1.20 of user's attack power over 3 turns.
--Designed for use with the Jelli paragon and adapted from Mei's skills

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--Setup, apply overrides.
if(gzEffectHiveScoutSting == nil) then
    
    --Base, overrides.
    local zEffectStruct = fnCreateDoTEffectPrototype(gciDamageType_Poisoning, 3, 1.20)
    zEffectStruct.sDisplayName = "String"
    zEffectStruct.sIcon = "Mei|Sting"
    
    --Store
    gzEffectHiveScoutSting = zEffectStruct
    
end

-- |[Set and Execute]|
--Standardized Naming
gzRefEffect = gzEffectHiveScoutSting

--Call the standard handler. It will nil off the gzRefEffect.
LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType)
