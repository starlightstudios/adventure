-- |[ ======================================== Goopshot ======================================== ]|
-- |[Description]|
--Deals corroding damage equal to 0.75 of user's attack power over 3 turns.
--Designed for use with the Jelli paragon and adapted from Mei's skills

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--Setup, apply overrides.
if(gzEffectSmartySageGoopshot == nil) then
    
    --Base, overrides.
    local zEffectStruct = fnCreateDoTEffectPrototype(gciDamageType_Corroding, 3, 0.75)
    zEffectStruct.sDisplayName = "Goopshot"
    zEffectStruct.sIcon = "Mei|Goopshot"
    
    --Store
    gzEffectSmartySageGoopshot = zEffectStruct
    
end

-- |[Set and Execute]|
--Standardized Naming
gzRefEffect = gzEffectSmartySageGoopshot

--Call the standard handler. It will nil off the gzRefEffect.
LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType)
