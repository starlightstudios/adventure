-- |[ ==================================== Seed Spitter DoT ==================================== ]|
-- |[Description]|
--Deals bleeding damage equal to 0.5 of user's attack power over 2 turns. Designed for use with the sprout pecker paragon.
--Originally designed to have life steal effect.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--Setup, apply overrides.
if(gzEffectEnemySeedSpitterDoT == nil) then
    
    --Base, overrides.
    local zEffectStruct = fnCreateDoTEffectPrototype(gciDamageType_Bleeding, 2, 0.50)
    zEffectStruct.sDisplayName = "Seed Spitter (DoT)"
    zEffectStruct.sIcon = "GenBleed"
    
    --Tags
    zEffectStruct.zaTagList = {{"Is Negative", 1}, {"Bleeding DoT", 1}}
    
    --Store
    gzEffectEnemySeedSpitterDoT = zEffectStruct
    
end

-- |[Set and Execute]|
--Standardized Naming
gzRefEffect = gzEffectEnemySeedSpitterDoT

--Call the standard handler. It will nil off the gzRefEffect.
LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType)

