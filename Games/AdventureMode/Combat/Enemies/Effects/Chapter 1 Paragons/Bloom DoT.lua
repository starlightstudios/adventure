-- |[ ==================================== Bloom DoT ==================================== ]|
-- |[Description]|
--Deals poisoning damage equal to 0.50 of user's attack power over 2 turns. Meant to be consumed by Wither. 
--Designed for use with the Sproutling Paragon.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--Setup, apply overrides.
if(gzEffectEnemyBloomDoT == nil) then
    
    --Base, overrides.
    local zEffectStruct = fnCreateDoTEffectPrototype(gciDamageType_Poisoning, 2, 0.50)
    zEffectStruct.sDisplayName = "Bloom (DoT)"
    zEffectStruct.sIcon = "GenPoison"
    
    --Tags
    zEffectStruct.zaTagList = {{"Is Negative", 1}, {"Poisoning DoT", 1}}
    
    --Store
    gzEffectEnemyBloomDoT = zEffectStruct
    
end

-- |[Set and Execute]|
--Standardized Naming
gzRefEffect = gzEffectEnemyBloomDoT

--Call the standard handler. It will nil off the gzRefAbility.
LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType)

