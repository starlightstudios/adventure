-- |[ ==================================== Widening Gyre Stat ==================================== ]|
-- |[Description]|
--Deal 100% of normal claw attack. Increases attack by 50 and evasion by 50 for 6 turns. Designed for use with Skitter paragon.


-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Local Variables ===================================== ]|
-- |[System]|
--This effect uses a standardized StatMod script.
local sDisplayName = "Widening Gyre"
local iDuration = 6
local bIsBuff = true
local sIcon = "Root/Images/AdventureUI/Abilities/GenBuff"
local sStatString = "Evd|Flat|50 Atk|Flat|50"

--Description can be a variable number of lines.
local saDescription = {}
saDescription[1] = "Things fall apart; the center cannot hold."
saDescription[2] = "Increases [Atk] and [Evd]."

--Run the standardized builder.
gzaStatModStruct            = fnCreateStatmodEffectPrototype(sDisplayName, iDuration, bIsBuff, sIcon, sStatString)
gzaStatModStruct.saStrings  = fnStatmodBuildInspectorStrings(gzaStatModStruct, saDescription)
gzaStatModStruct.sShortText = fnStatmodBuildShortText(gzaStatModStruct)

-- |[Overrides]|
--Tags.

-- |[ ======================================== Creation ======================================== ]|
--Standard call when effect is initialized.
if(iSwitchType == gciEffect_Create) then
    LM_ExecuteScript(gsStandardStatPath, gciEffect_Create, fSeverity)

-- |[ ================================== Apply/Unapply Stats =================================== ]|
--Calls the apply/unapply stat routines.
elseif((iSwitchType == gciEffect_ApplyStats or iSwitchType == gciEffect_UnApplyStats) and iArgumentsTotal >= 2) then

    --Arguments.
    local iTargetID = math.floor(tonumber(LM_GetScriptArgument(1, "N")))
    
    --Set the stat string to its default.
    gzaStatModStruct.sStatString = sStatString

    --Execute standard handler.
    LM_ExecuteScript(gsStandardStatPath, iSwitchType, iTargetID)

-- |[ ================================== Character Action Ends ================================= ]|
--Called after a character action ends to decrement the turn counter.
elseif(iSwitchType == gciEffect_PostAction) then
    LM_ExecuteScript(gsStandardStatPath, gciEffect_PostAction)
end
