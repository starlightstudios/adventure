-- |[ ==================================== Rending Claws Crit DoT ==================================== ]|
-- |[Description]|
--Deals Bleeding damage equal to 0.20 of user's attack power over 2 turns. 

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--Setup, apply overrides.
if(gzEffectEnemyRendingClawsDoT == nil) then
    
    --Base, overrides.
    local zEffectStruct = fnCreateDoTEffectPrototype(gciDamageType_Bleeding, 2, 0.20)
    zEffectStruct.sDisplayName = "Rending Claws (DoT)"
    zEffectStruct.sIcon = "GenBleed"
    
    --Tags
    zEffectStruct.zaTagList = {{"Is Negative", 1}, {"Bleed DoT", 1}}
    
    --Store
    gzEffectEnemyRendingClawsDoT = zEffectStruct
    
end

-- |[Set and Execute]|
--Standardized Naming
gzRefEffect = gzEffectEnemyRendingClawsDoT

--Call the standard handler. It will nil off the gzRefEffect.
LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType)
