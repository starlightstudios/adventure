-- |[ ================================ Common Effect Prototypes ================================ ]|
--Effect prototypes used by many enemies over multiple chapters. Built at startup.
if(gzPrototypes.Combat.bMadeEffectPrototypes ~= nil) then return end
gzPrototypes.Combat.bMadeEffectPrototypes = true

--Variables.
local sDisplayName = ""
local iDuration = -1
local bIsBuff = false
local sIcon = "Null"
local sStatString = ""
local saDescription = {}
local zPrototype = nil

-- |[ ================== Bleed Effects ================= ]|
-- |[Bleed Vulnerable]|
--This effect uses a standardized StatMod script.
sDisplayName = "Bleed Vulnerable"
iDuration = -1
bIsBuff = false
sIcon = "Root/Images/AdventureUI/Abilities/GenBleed"
sStatString = ""

--Description can be a variable number of lines.
saDescription = {}
saDescription[1] = "This enemy is vulnerable to bleeding effects."
saDescription[2] = "Bleeds hit with ARG2 additional strength, and"
saDescription[3] = "bleed DoTs are ARG1%% more damaging."

--Run the standardized builder.
zPrototype = fnCreateStatmodEffectPrototype(sDisplayName, iDuration, bIsBuff, sIcon, sStatString, saDescription, gcbBuildShortText)

--Tags.
zPrototype.zaTagList = {{"Bleed Damage Taken +", -1}, {"Bleed Apply +", -2}}
fnRegisterEffectPrototype("Enemy.Stock.BleedVulnerable", gciEffect_Is_StatMod, zPrototype)

-- |[Bleed Resistant]|
--This effect uses a standardized StatMod script.
sDisplayName = "Bleed Resistant"
iDuration = -1
bIsBuff = false
sIcon = "Root/Images/AdventureUI/Abilities/GenBleed"
sStatString = ""

--Description can be a variable number of lines.
saDescription = {}
saDescription[1] = "This enemy is resistant to bleeding effects."
saDescription[2] = "Blinds hit with ARG2 reduced strength, and"
saDescription[3] = "blind DoTs are ARG1%% less damaging."

--Run the standardized builder.
zPrototype = fnCreateStatmodEffectPrototype(sDisplayName, iDuration, bIsBuff, sIcon, sStatString, saDescription, gcbBuildShortText)

--Tags.
zPrototype.zaTagList = {{"Bleed Damage Taken -", -1}, {"Bleed Apply -", -2}}
fnRegisterEffectPrototype("Enemy.Stock.BleedResistant", gciEffect_Is_StatMod, zPrototype)

-- |[ ================== Blind Effects ================= ]|
-- |[Blind Vulnerable]|
--This effect uses a standardized StatMod script.
sDisplayName = "Blind Vulnerable"
iDuration = -1
bIsBuff = false
sIcon = "Root/Images/AdventureUI/Abilities/GenBlind"
sStatString = ""

--Description can be a variable number of lines.
saDescription = {}
saDescription[1] = "This enemy is vulnerable to blinding effects."
saDescription[2] = "Blinds hit with ARG2 additional strength, and"
saDescription[3] = "blind effects are ARG1%% more potent."

--Run the standardized builder.
zPrototype = fnCreateStatmodEffectPrototype(sDisplayName, iDuration, bIsBuff, sIcon, sStatString, saDescription, gcbBuildShortText)

--Tags.
zPrototype.zaTagList = {{"Blind Effect +", -1}, {"Blind Apply +", -2}}

fnRegisterEffectPrototype("Enemy.Stock.BlindVulnerable", gciEffect_Is_StatMod, zPrototype)

-- |[Blind Resistant]|
--This effect uses a standardized StatMod script.
sDisplayName = "Blind Resistant"
iDuration = -1
bIsBuff = false
sIcon = "Root/Images/AdventureUI/Abilities/GenBlindRes"
sStatString = ""

--Description can be a variable number of lines.
saDescription = {}
saDescription[1] = "This enemy is resistant to blinding effects."
saDescription[2] = "Blinds hit with ARG2 reduced strength, and"
saDescription[3] = "blind effects are ARG1%% less potent."

--Run the standardized builder.
zPrototype = fnCreateStatmodEffectPrototype(sDisplayName, iDuration, bIsBuff, sIcon, sStatString, saDescription, gcbBuildShortText)

--Tags.
zPrototype.zaTagList = {{"Blind Effect -", -1}, {"Blind Apply -", -2}}
fnRegisterEffectPrototype("Enemy.Stock.BlindResistant", gciEffect_Is_StatMod, zPrototype)
