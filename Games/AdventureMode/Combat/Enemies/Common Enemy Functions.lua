-- |[ ================================= Common Enemy Functions ================================= ]|
--Functions used to standardize enemy construction.

-- |[fnEnemyStandardSystemAbilities]|
--Gives a newly created enemy their stunned/ambushed reaction abilities. Almost every enemy uses
-- these as their first ability slots.
fnEnemyStandardSystemAbilities = function()
    
    --The 0th ability is always the stun ability. Replacing this means changing enemy behavior when they get stunned!
    LM_ExecuteScript(gsRoot .. "Combat/Enemies/Abilities/Common/Stunned.lua", gciAbility_Create)
    
    --1st ability is the Ambush handler. Change this to change how the enemy reacts when ambushed.
    LM_ExecuteScript(gsRoot .. "Combat/Enemies/Abilities/Common/Ambushed.lua", gciAbility_Create)
    
end

-- |[ ===================================== Adder Function ===================================== ]|
--Sets basic stats. This function operates on whichever stat array is specified.
gzStatArray = nil
gzPortraitArray = nil

--Adder function.
function fnAdd(psActualName, psDisplayName, psAttackName, piLevel, piHealth, piAtk, piIni, piAcc, piEvd, piStun, piPrt, piSls, piStk, piPrc, piFlm, piFrz, piShk, piCru, piObs, piBld, piPsn, piCrd, piTrf, piPlt, piExp, piJP, psDropName, piDropChance)
    
    -- |[Error Check]|
    --Make sure the enemy array exists.
    if(gzStatArray == nil) then return end
    
    -- |[Determine Slot]|
    --If the name is already taken, overwrite. Otherwise, add a new slot.
    local bIsOverwrite = false
    local i = -1
    for q = 1, #gzStatArray, 1 do
        if(psActualName == gzStatArray[q].sActualName) then
            bIsOverwrite = true
            i = q
            --io.write("Replacing enemy data in slot " .. i .. "\n")
            break
        end
    end
    
    --If i was not modified, add a new space onto the end.
    if(i == -1) then
        i = #gzStatArray + 1
        gzStatArray[i] = {}
    end
    
    -- |[System Data]|
    --Names.
    gzStatArray[i].sActualName  = psActualName
    gzStatArray[i].sDisplayName = psDisplayName
    
    --KOs needed to show stats/resists. Can be overridden.
    gzStatArray[i].iKOForStats   = gciAI_KOsForStats
    gzStatArray[i].iKOForResists = gciAI_KOsForResists
    
    --Arrays. Only reset these when not in overwrite.
    if(bIsOverwrite == false) then
        
        --Set base arrays.
        gzStatArray[i].saAbilityList = {}
        gzStatArray[i].zaTags = {}
        gzStatArray[i].saDrops = {}
        gzStatArray[i].saTopics = {}
    
        --If there is an attack name, add it. Note there is a remap to keep the chart tight.
        if(psAttackName ~= "Null") then
            if(psAttackName == "Sword") then
                gzStatArray[i].saAbilityList[1] = {"Enemy|Sword Attack", gsRoot .. "Combat/Enemies/Abilities/Chapter 0 Enemies/Slash Sword Attack.lua", 100}
            elseif(psAttackName == "Claw") then
                gzStatArray[i].saAbilityList[1] = {"Enemy|Claw Attack",  gsRoot .. "Combat/Enemies/Abilities/Chapter 0 Enemies/Slash Claw Attack.lua",  100}
            elseif(psAttackName == "Gun") then
                gzStatArray[i].saAbilityList[1] = {"Enemy|Shot Attack",  gsRoot .. "Combat/Enemies/Abilities/Chapter 0 Enemies/Pierce Shot Attack.lua", 100}
            elseif(psAttackName == "Null") then
                --Override later.
            else
                gzStatArray[i].saAbilityList[1] = {"Enemy|"..psAttackName, psAttackName, 100}
            end
        end
    end
    
    --Statistics.
    gzStatArray[i].iLevel = piLevel
    gzStatArray[i].iHealth = piHealth
    gzStatArray[i].iAtk = piAtk
    gzStatArray[i].iIni = piIni
    gzStatArray[i].iAcc = piAcc
    gzStatArray[i].iEvd = piEvd
    gzStatArray[i].iStun = piStun
    gzStatArray[i].iPrt = piPrt
    gzStatArray[i].iSls = piSls + gciNormalResist
    gzStatArray[i].iStk = piStk + gciNormalResist
    gzStatArray[i].iPrc = piPrc + gciNormalResist
    gzStatArray[i].iFlm = piFlm + gciNormalResist
    gzStatArray[i].iFrz = piFrz + gciNormalResist
    gzStatArray[i].iShk = piShk + gciNormalResist
    gzStatArray[i].iCru = piCru + gciNormalResist
    gzStatArray[i].iObs = piObs + gciNormalResist
    gzStatArray[i].iBld = piBld + gciNormalResist
    gzStatArray[i].iPsn = piPsn + gciNormalResist
    gzStatArray[i].iCrd = piCrd + gciNormalResist
    gzStatArray[i].iTrf = piTrf + gciNormalResist
    
    --Statistics not set by this function.
    gzStatArray[i].iMPMax = 100
    gzStatArray[i].iCPMax = 0
    
    --AI.
    if(bIsOverwrite == false) then
        gzStatArray[i].sAIPath = gsRoot .. "Combat/AIs/000 Normal AI.lua"
    end
    
    --Rewards
    gzStatArray[i].iPlatina = piPlt
    gzStatArray[i].iExp = piExp
    gzStatArray[i].iJP = piJP
    
    --Drops. If it's "Null", drops nothing.
    if(psDropName ~= "Null" and piDropChance > 0) then
        gzStatArray[i].saDrops[1] = {psDropName, 1, piDropChance}
    end
    
    --Display Information.
    if(bIsOverwrite == false) then
        gzStatArray[i].sDialogueActor = "Null"
        gzStatArray[i].sPortraitPath = "Null"
        gzStatArray[i].sTurnPortraitPath = "Null"
    end
end

-- |[Paragon Adder]|
--Calls fnAdd() above, but adds the paragon name properties to the entity. This is used to avoid
-- making the enemy start chart any wider than it already is.
function fnPara(psActualName, psDisplayName, psAttackName, piLevel, piHealth, piAtk, piIni, piAcc, piEvd, piStun, piPrt, piSls, piStk, piPrc, piFlm, piFrz, piShk, piCru, piObs, piBld, piPsn, piCrd, piTrf, piPlt, piExp, piJP, psDropName, piDropChance)
    fnAdd(psActualName .. " Paragon", psDisplayName .. " Paragon", psAttackName, piLevel, piHealth, piAtk, piIni, piAcc, piEvd, piStun, piPrt, piSls, piStk, piPrc, piFlm, piFrz, piShk, piCru, piObs, piBld, piPsn, piCrd, piTrf, piPlt, piExp, piJP, psDropName, piDropChance)
end

-- |[Paragon Function]|
--Creates a paragon copy of the entity in the slot, and places it in the last slot.
function fnCreateParagon(p)

    -- |[Check for Duplicates]|
    --Scan the existing stat array, looking for an existing paragon. If it exists, then the paragon
    -- was manually defined and this call stops.
    for o = 1, #gzStatArray, 1 do
        if(gzStatArray[o].sActualName == gzStatArray[p].sActualName  .. " Paragon") then
            --io.write("Paragon duplicate of " .. gzStatArray[p].sActualName .. " already exists.\n")
            return
        end
    end

    -- |[Create]|
    --Add space.
    local i = #gzStatArray + 1
    gzStatArray[i] = {}
    
    -- |[Set Properties]|
    --Names.
    gzStatArray[i].sActualName  = gzStatArray[p].sActualName  .. " Paragon"
    gzStatArray[i].sDisplayName = gzStatArray[p].sDisplayName .. " Paragon"
    
    --KOs needed to show stats/resists. Can be overridden.
    gzStatArray[i].iKOForStats   = 0
    gzStatArray[i].iKOForResists = 0
    
    --Arrays.
    gzStatArray[i].saAbilityList = gzStatArray[p].saAbilityList
    gzStatArray[i].zaTags        = gzStatArray[p].zaTags
    gzStatArray[i].saDrops       = gzStatArray[p].saDrops
    gzStatArray[i].saTopics      = gzStatArray[p].saTopics
    
    --Statistics.
    local iParagonStatFactor = 5
    local iParagonHitBonus = 100
    local iParagonResistBonus = 2
    gzStatArray[i].iLevel  = gzStatArray[p].iLevel + 15
    gzStatArray[i].iHealth = gzStatArray[p].iHealth * iParagonStatFactor
    gzStatArray[i].iAtk    = gzStatArray[p].iAtk * iParagonStatFactor
    gzStatArray[i].iIni    = gzStatArray[p].iIni + iParagonHitBonus
    gzStatArray[i].iAcc    = gzStatArray[p].iAcc + iParagonHitBonus
    gzStatArray[i].iEvd    = gzStatArray[p].iEvd + iParagonHitBonus
    gzStatArray[i].iStun   = gzStatArray[p].iStun
    gzStatArray[i].iPrt    = gzStatArray[p].iPrt
    gzStatArray[i].iSls    = gzStatArray[p].iSls + iParagonResistBonus
    gzStatArray[i].iStk    = gzStatArray[p].iStk + iParagonResistBonus
    gzStatArray[i].iPrc    = gzStatArray[p].iPrc + iParagonResistBonus
    gzStatArray[i].iFlm    = gzStatArray[p].iFlm + iParagonResistBonus
    gzStatArray[i].iFrz    = gzStatArray[p].iFrz + iParagonResistBonus
    gzStatArray[i].iShk    = gzStatArray[p].iShk + iParagonResistBonus
    gzStatArray[i].iCru    = gzStatArray[p].iCru + iParagonResistBonus
    gzStatArray[i].iObs    = gzStatArray[p].iObs + iParagonResistBonus
    gzStatArray[i].iBld    = gzStatArray[p].iBld + iParagonResistBonus
    gzStatArray[i].iPsn    = gzStatArray[p].iPsn + iParagonResistBonus
    gzStatArray[i].iCrd    = gzStatArray[p].iCrd + iParagonResistBonus
    gzStatArray[i].iTrf    = gzStatArray[p].iTrf + iParagonResistBonus
    
    --Clamp stats!
    if(gzStatArray[i].iIni < 0) then gzStatArray[i].iIni = 0 end
    if(gzStatArray[i].iAcc < 0) then gzStatArray[i].iAcc = 0 end
    if(gzStatArray[i].iEvd < 0) then gzStatArray[i].iEvd = 0 end
    
    --Statistics not set by this function.
    gzStatArray[i].iMPMax = gzStatArray[p].iMPMax
    gzStatArray[i].iCPMax = gzStatArray[p].iCPMax
    
    --AI.
    gzStatArray[i].sAIPath = gzStatArray[p].sAIPath
    
    --Rewards
    local iParagonRewardFactor = 10
    local iParagonJPFactor = 3
    gzStatArray[i].iPlatina = gzStatArray[p].iPlatina * iParagonRewardFactor
    gzStatArray[i].iExp     = gzStatArray[p].iExp     * iParagonRewardFactor
    gzStatArray[i].iJP      = gzStatArray[p].iJP      * iParagonJPFactor
    
    --Display Information.
    gzStatArray[i].sDialogueActor    = gzStatArray[p].sDialogueActor
    gzStatArray[i].sPortraitPath     = gzStatArray[p].sPortraitPath
    gzStatArray[i].sTurnPortraitPath = gzStatArray[p].sTurnPortraitPath
    
    --Paragons use different portraits and turn-order icons.
    local sToSlashCombat = fnGetStringAfterLastSlash(gzStatArray[p].sPortraitPath)
    gzStatArray[i].sPortraitPath = "Root/Images/Portraits/Paragon/" .. sToSlashCombat
    
    --To-slash for turn portraits.
    local sToSlashTurn = fnGetStringAfterLastSlash(gzStatArray[p].sTurnPortraitPath)
    gzStatArray[i].sTurnPortraitPath = "Root/Images/AdventureUI/TurnPortraitsParagon/" .. sToSlashTurn
    
    -- |[Debug]|
    if(false) then
        local bExists = DL_Exists(gzStatArray[i].sTurnPortraitPath)
        if(bExists == true) then
        else
            io.write("Portrait " .. gzStatArray[i].sTurnPortraitPath .. " does not exist.\n")
        end
    end
end

-- |[Locator Function]|
--Locates the slot a named listing is in, or -1 if not found.
function fnLocate(psActualName)
    for p = 1, #gzStatArray, 1 do
        if(gzStatArray[p].sActualName == psActualName) then
            return p
        end
    end
    return -1
end

-- |[Tags Function]|
--Adds tags. Tags are always in pairs of {{"TagA", iTagCount}, {"TagB", iTagCount}}
-- This deliberately overwrites the tags and makes a clone.
function fnTag(psActualName, pzaTags)
    
    --Locate.
    local i = fnLocate(psActualName)
    if(i == -1) then return end
    
    --Populate.
    gzStatArray[i].zaTags = {}
    for p = 1, #pzaTags, 1 do
        gzStatArray[i].zaTags[p] = {pzaTags[p][1], pzaTags[p][2]}
    end
end

--Appends tags.
function fnAppendEnemyTag(psActualName, pzaTags)
    
    --Locate.
    local i = fnLocate(psActualName)
    if(i == -1) then return end
    
    --Append.
    for p = 1, #pzaTags, 1 do
        
        --If a tag already exists with that name, add them.
        local bAppended = false
        for q = 1, #gzStatArray[i].zaTags, 1 do
            if(gzStatArray[i].zaTags[q][1] == pzaTags[p][1]) then
                bAppended = true
                gzStatArray[i].zaTags[q][2] = gzStatArray[i].zaTags[q][2] + pzaTags[p][2]
            end
        end
        
        --Did not find a match, add a new entry.
        if(bAppended == false) then
            table.insert(gzStatArray[i].zaTags, {pzaTags[p][1], pzaTags[p][2]})
        end
    end
end

-- |[Abilities Function]|
--Adds abilities, in addition to the basic attack. The first element of the array will be compared against the lookup table gczaEnemyAbilityLookups
-- to locate the path to that ability. Both the name and its path are stored on the list.
--Ability List Format: {sAbilityName0, iChanceToUse0, sAbilityName1, iChanceToUse1, ... }
function fnAbility(psActualName, psAbilityList)
    
    --Locate.
    local i = fnLocate(psActualName)
    if(i == -1) then return end
    
    --Run across the ability list, adding each ability to the ability listing.
    for o = 1, #psAbilityList, 2 do
        
        --Variables.
        local sAbilityName = psAbilityList[o+0]
        local sAbilityPath = "Null"
        local iChanceToUse = psAbilityList[o+1]
        
        --Locate the path.
        for x = 1, #gczaEnemyAbilityLookups, 1 do
            if(gczaEnemyAbilityLookups[x][1] == sAbilityName) then
                sAbilityPath = gczaEnemyAbilityLookups[x][2]
                break
            end
        end
        
        --Print a warning:
        if(sAbilityPath == "Null") then
            io.write("Warning: Entity " .. psActualName .. " in slot " .. i .. " cannot find ability " .. sAbilityName .. "\n")
        
        --Add to the array.
        else
            local p = #gzStatArray[i].saAbilityList + 1
            gzStatArray[i].saAbilityList[p] = {sAbilityName, sAbilityPath, iChanceToUse}
        end
    end
end

-- |[Drops Function]|
--If an enemy drops something using more complicated logic, or has more than one drop, it gets added here.
function fnDrop(psActualName, psDropName, piDropLo, piDropHi)
    
    --Locate.
    local i = fnLocate(psActualName)
    if(i == -1) then return end
    
    --Set.
    local p = #gzStatArray[i].saDrops + 1
    if(psDropName ~= "Null") then
        gzStatArray[i].saDrops[p] = {psDropName, piDropLo, piDropHi}
    end
end

-- |[Topics Function]|
--Mugging or fighting the enemy always unlocks these topics.
function fnTopic(psActualName, psaTopics)
    
    --Locate.
    local i = fnLocate(psActualName)
    if(i == -1) then return end
    
    --Topics.
    gzStatArray[i].saTopics = psaTopics
end

-- |[KO Counter Function]|
--Changes how many times the enemy needs to be KOd to show stats/resists.
function fnKOCount(psActualName, iStatCount, iResistCount)
    
    --Locate.
    local i = fnLocate(psActualName)
    if(i == -1) then return end
    
    --Set.
    gzStatArray[i].iKOForStats   = iStatCount
    gzStatArray[i].iKOForResists = iResistCount
end

-- |[AI Override Function]|
--Changes which AI this entity is using.
function fnAI(psActualName, psAIPath)
    
    --Locate.
    local i = fnLocate(psActualName)
    if(i == -1) then return end
    
    --Set.
    gzStatArray[i].sAIPath = psAIPath
end

-- |[ ================================ Display Setting Functions =============================== ]|
--Every entity has a portrait, but not all portraits are unique. This lookup table indicates where
-- a portrait should be offset so as to appear on the UI correctly. Negative values are legal.
--If an enemy does not use the override values, pass -1000 for them.
function fnAddPortrait(psPath, psTurnPortrait, psDialogueActor)
    
    --Add space.
    local i = #gzPortraitArray + 1
    gzPortraitArray[i] = {}
    
    --Set.
    gzPortraitArray[i].sDialogueActor   = psDialogueActor
    gzPortraitArray[i].sPath            = psPath
    gzPortraitArray[i].sTurnPortrait    = psTurnPortrait
end

--Maps enemies to which portrait set they are expected to use.
function fnSetRemap(psActualName, psPortraitPath)
    
    --Locate the entry.
    local i = fnLocate(psActualName)
    if(i == -1) then return end
    
    --Locate the portrait set.
    local p = -1
    for x = 1, #gzPortraitArray, 1 do
        if(gzPortraitArray[x].sPath == psPortraitPath) then
            p = x
            break
        end
    end
    if(p == -1) then return end
    
    --Display Information.
    gzStatArray[i].sDialogueActor    = gzPortraitArray[p].sDialogueActor
    gzStatArray[i].sPortraitPath     = gzPortraitArray[p].sPath
    gzStatArray[i].sTurnPortraitPath = gzPortraitArray[p].sTurnPortrait
end

--Same as above, but also modifies the portrait and turn portrait path to 
function fnSetRemapParagon(psActualName, psPortraitPath)
    
    -- |[Base Call]|
    --Locate the entry.
    local i = fnLocate(psActualName)
    if(i == -1) then return end
    
    --Locate the portrait set.
    local p = -1
    for x = 1, #gzPortraitArray, 1 do
        if(gzPortraitArray[x].sPath == psPortraitPath) then
            p = x
            break
        end
    end
    if(p == -1) then return end
    
    --Display Information.
    gzStatArray[i].sDialogueActor    = gzPortraitArray[p].sDialogueActor
    gzStatArray[i].sPortraitPath     = gzPortraitArray[p].sPath
    gzStatArray[i].sTurnPortraitPath = gzPortraitArray[p].sTurnPortrait
    
    -- |[Remap]|
    --Modify combat portrait.
    local sToSlashCombat = fnGetStringAfterLastSlash(gzStatArray[i].sPortraitPath)
    gzStatArray[i].sPortraitPath = "Root/Images/Portraits/Paragon/" .. sToSlashCombat
    
    --Modify turn-order portrait.
    local sToSlashTurn = fnGetStringAfterLastSlash(gzStatArray[i].sTurnPortraitPath)
    gzStatArray[i].sTurnPortraitPath = "Root/Images/AdventureUI/TurnPortraitsParagon/" .. sToSlashTurn
end