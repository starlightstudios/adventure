-- |[ ===================================== Ability Lookups ==================================== ]|
--Called at startup, creates a lookup of ability paths versus their names.
local sAbilityRoot = gsRoot .. "Combat/Enemies/Abilities/"
gczaEnemyAbilityLookups = {}

-- |[ =============== Chapter 0 Abilities ============== ]|
local sChapter0Root = sAbilityRoot .. "Chapter 0 Enemies/"

-- |[Typed Attacks]|
table.insert(gczaEnemyAbilityLookups, {"Enemy|Bleed Attack",   sChapter0Root .. "Bleed Attack.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Claw Attack",    sChapter0Root .. "Slash Claw Attack.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Corrode Attack", sChapter0Root .. "Corrode Attack.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Crusade Attack", sChapter0Root .. "Crusade Attack.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Freeze Attack",  sChapter0Root .. "Freeze Attack.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Flame Attack",   sChapter0Root .. "Flame Attack.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Obscure Attack", sChapter0Root .. "Obscure Attack.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Pierce Attack",  sChapter0Root .. "Pierce Attack.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Poison Attack",  sChapter0Root .. "Poison Attack.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Shock Attack",   sChapter0Root .. "Shock Attack.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Shot Attack",    sChapter0Root .. "Pierce Shot Attack.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Strike Attack",  sChapter0Root .. "Strike Attack.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Sword Attack",   sChapter0Root .. "Slash Sword Attack.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Terrify Attack", sChapter0Root .. "Terrify Attack.lua"})

-- |[Typed DoTs]|
table.insert(gczaEnemyAbilityLookups, {"Enemy|Bleed DoT",   sChapter0Root .. "Bleed DoT Attack.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Corrode DoT", sChapter0Root .. "Corrode DoT Attack.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Poison DoT",  sChapter0Root .. "Poison DoT Attack.lua"})

-- |[Passives/Other]|
table.insert(gczaEnemyAbilityLookups, {"Enemy|Counterattack", sChapter0Root .. "Counterattack.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Power Gate",    sChapter0Root .. "Power Gate.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Natural Armor", sChapter0Root .. "Natural Armor.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Summon Ally",   sChapter0Root .. "Summon Ally.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Unstunnable",   sChapter0Root .. "Unstunnable.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Unpredictable", sChapter0Root .. "Unpredictable.lua"})

-- |[ ============== Chapter 1 Abilities =============== ]|
local sChapter1Root = sAbilityRoot .. "Chapter 1 Enemies/"

-- |[General]|
table.insert(gczaEnemyAbilityLookups, {"Enemy|Burning Word",     sChapter1Root .. "Burning Word.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Chanting",         sChapter1Root .. "Chanting.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Frenzy",           sChapter1Root .. "Frenzy.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Knife Sweep",      sChapter1Root .. "Knife Sweep.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Peck",             sChapter1Root .. "Peck.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Powder Bomb",      sChapter1Root .. "Powder Bomb.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Sugar Rush",       sChapter1Root .. "Sugar Rush.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Threats",          sChapter1Root .. "Threats.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Torrent Of Flame", sChapter1Root .. "Torrent Of Flame.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Wild Swing",       sChapter1Root .. "Wild Swing.lua"})

table.insert(gczaEnemyAbilityLookups, {"Enemy|Mirror's Drain",   sChapter1Root .. "Mirror Drain.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Tear Wound",       sChapter1Root .. "Tear Wound.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Best Friend SA",   sChapter1Root .. "Best Friend Summon Ally.lua"})

-- |[Boss]|
sChapter1Root = sAbilityRoot .. "Chapter 1 Bosses/"
table.insert(gczaEnemyAbilityLookups, {"Boss|Victoria|Homing Needles", sChapter1Root .. "Homing Needles.lua"})
table.insert(gczaEnemyAbilityLookups, {"Boss|Victoria|Flashburn",      sChapter1Root .. "Flashburn.lua"})
table.insert(gczaEnemyAbilityLookups, {"Boss|Victoria|Poison Mist",    sChapter1Root .. "Poison Mist.lua"})
table.insert(gczaEnemyAbilityLookups, {"Boss|Victoria|Rapier Slice",   sChapter1Root .. "Rapier Slice.lua"})

-- |[ =============== Chapter 1 Paragons =============== ]|
local sChapter1Para = sAbilityRoot .. "Chapter 1 Paragons/"

-- |[General]|
table.insert(gczaEnemyAbilityLookups, {"Enemy|Angry Honey",             sChapter1Para .. "Angry Honey.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Armor Steal",             sChapter1Para .. "Armor Steal.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Be Our Guest",            sChapter1Para .. "Be Our Guest.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Bee Sting",               sChapter1Para .. "Bee Sting.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Bleeding Hand F",         sChapter1Para .. "Bleeding Hand F.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Bleeding Hand M",         sChapter1Para .. "Bleeding Hand M.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Bloom",                   sChapter1Para .. "Bloom.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Blow Up Claw",            sChapter1Para .. "Blow Up Claw.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Blow Up Sword",           sChapter1Para .. "Blow Up Sword.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Cat Nap",                 sChapter1Para .. "Cat Nap.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Cats Claw",               sChapter1Para .. "Cats Claw.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Corruptive Cloud",        sChapter1Para .. "Corruptive Cloud.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Counterattack Base",      sChapter1Para .. "Counterattack Base.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Counterattack Bee",       sChapter1Para .. "Counterattack Bee.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Counterattack Mirror",    sChapter1Para .. "Counterattack Mirror.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Counterattack Prowler",   sChapter1Para .. "Counterattack Prowler.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Counterattack Rubber",    sChapter1Para .. "Counterattack Rubber.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Counterattack Thief",     sChapter1Para .. "Counterattack Thief.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Cute Look",               sChapter1Para .. "Cute Look.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Fatal Sting",             sChapter1Para .. "Fatal Sting.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Fireball",                sChapter1Para .. "Fireball.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Fumble Claw",             sChapter1Para .. "Fumble Claw.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Fumble Sword",            sChapter1Para .. "Fumble Sword.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Groaning Slab",           sChapter1Para .. "Groaning Slab.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Ice Ray",                 sChapter1Para .. "Ice Ray.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Lovely Slime Lightning",  sChapter1Para .. "Lovely Slime Lightning.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Magical Cutie Transform", sChapter1Para .. "Magical Cutie Transform.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Melty Hug",               sChapter1Para .. "Melty Hug.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Monster of the Forest",   sChapter1Para .. "Monster of the Forest.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Pea Shooter",             sChapter1Para .. "Pea Shooter.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Pollen Rend",             sChapter1Para .. "Pollen Rend.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Protective Grip F",       sChapter1Para .. "Protective Grip F.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Protective Grip M",       sChapter1Para .. "Protective Grip M.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Rending Claws",           sChapter1Para .. "Rending Claws.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Roses Execution",         sChapter1Para .. "Roses Execution.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Roses Poison",            sChapter1Para .. "Roses Poison.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Roses Riposte",           sChapter1Para .. "Roses Riposte.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Seal Of Approval",        sChapter1Para .. "Seal Of Approval.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Badge Of Approval",       sChapter1Para .. "Badge Of Approval.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Seed Spitter",            sChapter1Para .. "Seed Spitter.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Slime Balloon",           sChapter1Para .. "Slime Balloon.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Slime Heart Blast",       sChapter1Para .. "Slime Heart Blast.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Slime Rend",              sChapter1Para .. "Slime Rend.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|The Struggle Within",     sChapter1Para .. "The Struggle Within.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Trumpet Sounds",          sChapter1Para .. "Trumpet Sounds.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Undying",                 sChapter1Para .. "Undying.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Vivimancys Flail",        sChapter1Para .. "Vivimancys Flail.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Weeping Stone",           sChapter1Para .. "Weeping Stone.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Widening Gyre",           sChapter1Para .. "Widening Gyre.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Wild Swing Modified",     sChapter1Para .. "Wild Swing Modified.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Wither",                  sChapter1Para .. "Wither.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Withering Touch",         sChapter1Para .. "Withering Touch.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Wrath Of The Hive",       sChapter1Para .. "Wrath Of The Hive.lua"})

-- |[Jelli Skills]|
table.insert(gczaEnemyAbilityLookups, {"Enemy|Bodyslam",  sChapter1Para .. "Jelli/Bodyslam.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Crush",     sChapter1Para .. "Jelli/Crush.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Goopshot",  sChapter1Para .. "Jelli/Goopshot.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Icy Hand",  sChapter1Para .. "Jelli/Icy Hand.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Rend",      sChapter1Para .. "Jelli/Rend.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Sting",     sChapter1Para .. "Jelli/Sting.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Swipe",     sChapter1Para .. "Jelli/Swipe.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Vine Lash", sChapter1Para .. "Jelli/VineLash.lua"})

-- |[Summon-Ally Skills]|
table.insert(gczaEnemyAbilityLookups, {"Enemy|Adept F SA",      sChapter1Para .. "Summon Ally/Adept F SA.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Adept M SA",      sChapter1Para .. "Summon Ally/Adept M SA.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Bee Buddy SA",    sChapter1Para .. "Summon Ally/Bee Buddy SA.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Bee Sniffer SA",  sChapter1Para .. "Summon Ally/Bee Sniffer SA.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Bee Tracker SA",  sChapter1Para .. "Summon Ally/Bee Tracker SA.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Candle Haunt SA", sChapter1Para .. "Summon Ally/Candle Haunt SA.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Eye Drawer SA",   sChapter1Para .. "Summon Ally/Eye Drawer SA.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Initiate F SA",   sChapter1Para .. "Summon Ally/Initiate F SA.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Initiate M SA",   sChapter1Para .. "Summon Ally/Initiate M SA.lua"})

-- |[ ============== Chapter 2 Abilities =============== ]|
local sChapter2Root = sAbilityRoot .. "Chapter 2 Enemies/"

-- |[General]|
table.insert(gczaEnemyAbilityLookups, {"Enemy|Armor Rot",           sChapter2Root .. "Armor Rot.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Battle Orders",       sChapter2Root .. "Battle Orders.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Burst Of Speed",      sChapter2Root .. "Burst Of Speed.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Caustic Cloud",       sChapter2Root .. "Caustic Cloud.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Lariat",              sChapter2Root .. "Lariat.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Overhead Smash",      sChapter2Root .. "Overhead Smash.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Poison Cloud",        sChapter2Root .. "Poison Cloud.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Poison Spine",        sChapter2Root .. "Poison Spine.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Suck Blood No Curse", sChapter2Root .. "Suck Blood No Curse.lua"})

-- |[Werebat]|
--These are the same as the normal abilities, but can turn Sanya into a werebat.
table.insert(gczaEnemyAbilityLookups, {"Enemy|Bat Bleed Attack", sChapter2Root .. "Bat Bleed Attack.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Bat Bleed DoT",    sChapter2Root .. "Bat Bleed DoT Attack.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Suck Blood",       sChapter2Root .. "Suck Blood.lua"})

-- |[ ============== Chapter 5 Abilities =============== ]|
local sChapter5Root = sAbilityRoot .. "Chapter 5 Enemies/"

-- |[General]|
table.insert(gczaEnemyAbilityLookups, {"Enemy|Groaning Light",  sChapter5Root .. "Groaning Light.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Scrap Sweep",     sChapter5Root .. "Scrap Sweep.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Clean Receptors", sChapter5Root .. "Clean Receptors.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|My Pretties",     sChapter5Root .. "My Pretties.lua"})
table.insert(gczaEnemyAbilityLookups, {"Enemy|Piercing Laser",  sChapter5Root .. "Piercing Laser.lua"})

