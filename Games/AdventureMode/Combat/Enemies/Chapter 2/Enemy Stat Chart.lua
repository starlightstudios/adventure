-- |[ ================================= Enemy Statistics Chart ================================= ]|
--This chart lists off all enemy properties in one handy spot. Complicated enemies may need additional
-- setup, which is why they have their own files.
gzStatArray = {}
gzPortraitArray = {}

-- |[ ====================================== Enemy Listing ===================================== ]|
--Notes: Resistances are difference from standard, not actual values. The standard resistance is gciNormalResist.
-- Extra abilities and tags are added later.
--The "Attack" column is the basic attack this entity uses. Place "Null" to have no attacks or to manually specify
-- additional attacks later.

-- |[ ========= Northwoods and Intro ========= ]|
--     Actual Name         | Display Name        | Attack ||||| Lv | Health | Attack | Ini | Acc | Evd | Stun ||||| Pr | Sl | St | Pc | Fl | Fr | Sh | Cr | Ob | Bl | Ps | Cr | Tr ||||| Plt | Exp | JP |  Drop             | Chnc
fnAdd("Bat Seeker",         "Seeker Bat",         "Claw",       1,     145,      62,  150,   20,   90,    70,    -2,  -2,   0,   0,  -4,   2,   1,   5,   5,  -5,  -2,   1,   4,      3,    4,  25, "Light Leather",       50)
fnAdd("Quiet Butterbomber", "Quiet Butterbomber", "Claw",       1,     127,      50,   80,   30,   60,    70,     0,  -4,   4,  -4,   1,   7,   5,   0,   0,   4,   4,   4,  -5,      5,    5,  25, "Gossamer",            50)
fnAdd("Cave Muncher",       "CaveMuncher",        "Claw",       1,     180,      52,   30,   30,   30,   100,     0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,      4,    6,  25, "Light Carapace",      50)
fnAdd("Poison Vine",        "Poison Vine",        "Sword",      1,     189,      87,   30,   50,   50,   100,     0,   0,   0,   0,  -4,  -4,   3,   5,  -2,   2,  10,   3,  -5,      2,   12,  25, "Light Fiber",         50)
fnAdd("Buffodil",           "Buffodil",           "Claw",       1,     321,     180,   10,    0,   40,   180,     4,   1,   1,   1,  -4,  -4,   3,   5,  -2,   2,   2,   3,  -5,      2,   12,  25, "Light Fiber",         50)
fnAdd("Bunny Smuggler",     "Bunny Smuggler",     "Sword",      1,     141,      40,  120,  120,  120,   100,     0,  -3,   0,  -3,  -2,   7,  -2,   0,   0,  -3,  -3,   3, -10,     22,   13,  25, "Null",                 0)
fnAdd("Harpy Recruit",      "Harpy Recruit",      "Sword",      3,     356,      68,   50,    0,   50,   100,     2,   1,   1,   1,   3,   3,   3,   2,   2,  -5,  -5,  -5,   5,      4,   10,  25, "Null",                 0)
fnAdd("Treant Wanderer",    "Treant Wanderer",    "Sword",      5,     560,     180,    0,   70,    0,   130,     8,   3,  -6,   4,  -8,   8,   8,   1,   1,   8,   8,   8,   8,      6,   35,  25, "Hardened Bark",       50)
fnAdd("Harpy Officer",      "Harpy Officer",      "Sword",      5,     267,      78,   55,   55,   55,   100,     2,   1,   1,   1,   3,   3,   3,   2,   2,  -5,  -5,  -5,   5,      4,   32,  25, "Null",                 0)
fnAdd("Kitsune Warrior",    "Kitsune Warrior",    "Claw",       5,     786,     105,   80,   60,  120,    70,     2,   1,   1,   1,  -3,   3,   3,   5,   5,  -3,  -3,  -3,  10,      1,   35,  25, "Null",                 0)
fnAdd("Sevavi Guardian",    "Guardian",           "Claw",       7,    1355,     215,    0,   80,    0,   130,     8,   5,  -6,   5,   3,   5,  10,   0,   0,   5,   5,  -8,  -3,      1,   60,  25, "Null",                 0)

--Hunting.
fnAdd("Puissant Omnigoose", "Puissant Omnigoose", "Claw",       1,     127,     130,   40,  100,   40,   100,     0,  -4,   4,  -4,   1,   7,   5,   0,   0,   4,   4,   4,1000,      0,    7,  25, "Bruised Meat",       100)
fnAdd("Clever Turkerus",    "Clever Turkerus",    "Claw",       1,      98,      35,   40,  120,  120,   100,     0,  -3,   2,  -3,  -4,   4,  -2,   0,   0,  -3,  -3,   3,  -5,      0,   11,  25, "Bruised Meat",       100)
fnAdd("Rowdy Brimhog",      "Rowdy Brimhog",      "Claw",       1,     220,      62,   70,   50,   50,   110,     4,   1,   0,   4,  -2,   4,   2,   0,   0,  -3,  -3,   2,  -5,      0,   24,  25, "Bruised Meat",       100)
fnAdd("Unielk Grazer",      "Unielk Grazer",      "Claw",       3,    1259,      87,   40,   30,   60,   130,     6,   1,   0,   4,  -2,   4,   2,   0,   0,  -3,  -3,   1,  -5,      0,   29,  25, "Bruised Meat",       100)

-- |[ =========== The Old Capital ============ ]|
--     Actual Name         | Display Name        | Attack ||||| Lv | Health | Attack | Ini | Acc | Evd | Stun ||||| Pr | Sl | St | Pc | Fl | Fr | Sh | Cr | Ob | Bl | Ps | Cr | Tr ||||| Plt | Exp | JP |  Drop             | Chnc
fnAdd("Cave Devourer",      "Cave Devourer",      "Claw",       1,     150,      70,   70,   10,   10,   100,     2,   5,  -5,  -3,  -4,   5,   0,   0,   0,   0,   4,   2,   0,     10,   10,  25, "Tough Carapace",     20)
fnAdd("Bat Hunter",         "Bat Hunter",         "Claw",       1,      80,      80,   80,   80,   80,    80,     0,   3,   3,   3,   0,   0,   0, -10,   0,   0,   0,   0,   0,     10,   10,  25, "Tough Leather",      20)

-- |[ =============== Westwoods ============== ]|
--     Actual Name         | Display Name        | Attack ||||| Lv | Health | Attack | Ini | Acc | Evd | Stun ||||| Pr | Sl | St | Pc | Fl | Fr | Sh | Cr | Ob | Bl | Ps | Cr | Tr ||||| Plt | Exp | JP |  Drop             | Chnc
fnAdd("Bunny Enforcer",     "Bunny Enforcer",     "Gun",        5,     300,     120,  100,  100,  100,   100,     0,  -3,   0,  -3,  -2,   7,  -2,   0,   0,  -3,  -3,   3, -10,     54,   41,  25, "Null",                0)
fnAdd("Bandit Stalker",     "Bandit Stalker",     "Sword",      5,     200,     100,   65,   75,   90,    60,     0,   1,   0,   0,   0,   3,   0,  -2,   2,  -3,   0,   0,  -6,     60,   36,  25, "Null",                0)
fnAdd("Bandit Killer",      "Bandit Killer",      "Sword",      5,     250,     150,   20,   35,   20,    80,     2,   1,   0,   0,   0,   3,   0,  -2,   2,  -3,   0,   0,  -6,     60,   36,  25, "Null",                0)
fnAdd("Frost Crawler",      "Frost Crawler",      "Null",       5,     100,      75,   80,   25,   10,   100,     3,   8,   2,   8,  -8,  10,  -2,   0,   0,  10,  10,  -2,   8,      3,   24,  25, "Unmelting Snow",     50)
fnAdd("Caustic Grub",       "Caustic Grub",       "Null",       5,     150,      50,   10,    0,    0,    60,     0,   0,  -5,  -1,   4,   0,   2,   0,   0, -10,   8,  10,   0,      2,   24,  25, "Crystallized Acid",  20)
fnAdd("Grub Nest",          "Grub Nest",          "Null",       5,     500,     120,    0,  100, -100,   200,     5,   5,  -5,   5,   5,   0,   5,   0,   0,  10,  10,  10,   0,      5,    5,  25, "Crystallized Acid", 100)
fnAdd("Brutal Redcap",      "Brutal Redcap",      "Claw",       7,     300,     500,    0,  100,   20,   140,     3,  -2,   4,   0,  -4,   0,   0,  -8,   0,  -2,   2,   0,  10,      0,   60,  25, "Tough Leather",      10)
fnAdd("Hungry Suckfly",     "Hungry Suckfly",     "Null",       5,     140,      80,   75,   35,   60,    80,     0,   0,   0,   0,  -4,   0,   0,   0,   0,   0,   0,   0,   0,      7,   25,  25, "Tough Carapace",     10)
fnAdd("Neutral Toxishroom", "Neutral Toxishroom", "Null",       5,     200,     160,    0,   50,    0,   100,     1,   0,   6,   0,  -5,   6,   6,   0,   0,   0,  10,   8,   0,      8,   25,  25, "Tough Fiber",        10)
fnAdd("Lurking Swamphag",   "Lurking Swamphag",   "Null",       9,     800,     200,   10,   10,   10,   100,     7,   3,   3,   3,   3,   7,  -4,  -7,  10,  -8,   5,   0,   7,    100,   80,  25, "Null",                0)

--Hunting.
fnAdd("Evasive Buneye",     "Evasive Buneye",      "Claw",      5,     170,      45,   80,   10,  150,    80,     0,   0,   0,   0,   0,   8,   0,   0,   0,   0,   0,   0, -10,      0,   28,  25, "Bruised Meat",       100)
fnAdd("Pseudogriffon Hen",  "Pseudogriffon Hen",   "Claw",      5,     255,      10,   10,   10,   10,   100,     4,   4,   1,   3,   0,   0,   0,   0,   0,   0,   0,   0,   3,      0,   55,  25, "Bruised Meat",       100)
fnAdd("Macho Brimhog",      "Macho Brimhog",       "Claw",      5,     650,     162,   70,   80,   70,   130,     4,   1,   0,   4,  -2,   4,   2,   0,   0,  -3,  -3,   2,  -5,      0,   75,  25, "Bruised Meat",       100)
fnAdd("Unielk Slatherer",   "Unielk Slatherer",    "Claw",      5,    2230,     147,   60,   40,   60,   130,     6,   1,   0,   4,  -2,   4,   2,   0,   0,  -3,  -3,   1,  -5,      0,  100,  25, "Bruised Meat",       100)

-- |[ =============== Debug Etc ============== ]|
--Debug only.
fnAdd("Training Dummy",     "Training Dummy",     "Claw",       1,    3500,      1,   30,    0,   10,    70,      0,   1,  -2,   3,   1,   3,  -5,   0,   0,   5,   5,  -8,  -3,      5,    3,  25, "Null",                 0)
fnAdd("Tag Switch Dummy",   "Tag Switch Dummy",   "Claw",       1,    3500,     10,   30,    0,   10,    70,      0,   1,  -2,   3,   1,   3,  -5,   0,   0,   5,   5,  -8,  -3,      5,    3,  25, "Null",                 0)

-- |[KO Count Modifiers]|
--None yet.

-- |[ ==================================== Additional Drops ==================================== ]|
--Harpies will drop some of the items of all the other huntables in the area.
fnDrop("Harpy Recruit", "Light Leather",   1, 10)
fnDrop("Harpy Recruit", "Light Carapace", 11, 20)
fnDrop("Harpy Recruit", "Light Fiber",    21, 30)
fnDrop("Harpy Recruit", "Hardened Bark",  31, 40)
fnDrop("Harpy Recruit", "Bruised Meat",   41, 50)

fnDrop("Harpy Officer", "Light Leather",   1, 10)
fnDrop("Harpy Officer", "Light Carapace", 11, 20)
fnDrop("Harpy Officer", "Light Fiber",    21, 30)
fnDrop("Harpy Officer", "Hardened Bark",  31, 40)
fnDrop("Harpy Officer", "Bruised Meat",   41, 50)

--Bandits drop huntables from the locals in Westwoods.
fnDrop("Bandit Stalker", "Bruised Meat",       1, 11)
fnDrop("Bandit Stalker", "Unmelting Snow",    12, 16)
fnDrop("Bandit Stalker", "Crystallized Acid", 17, 21)
fnDrop("Bandit Stalker", "Buneye Fluff",      22, 26)
fnDrop("Bandit Stalker", "Griffon Egg",       27, 31)
fnDrop("Bandit Stalker", "Bonesaw Hog Meat",  32, 36)
fnDrop("Bandit Stalker", "Caustic Vension",   37, 41)
fnDrop("Bandit Stalker", "Tough Leather",     42, 46)
fnDrop("Bandit Stalker", "Tough Fiber",       47, 51)
fnDrop("Bandit Stalker", "Tough Carapace",    52, 56)

fnDrop("Bandit Killer", "Bruised Meat",       1, 11)
fnDrop("Bandit Killer", "Unmelting Snow",    12, 16)
fnDrop("Bandit Killer", "Crystallized Acid", 17, 21)
fnDrop("Bandit Killer", "Buneye Fluff",      22, 26)
fnDrop("Bandit Killer", "Griffon Egg",       27, 31)
fnDrop("Bandit Killer", "Bonesaw Hog Meat",  32, 36)
fnDrop("Bandit Killer", "Caustic Vension",   37, 41)
fnDrop("Bandit Killer", "Tough Leather",     42, 46)
fnDrop("Bandit Killer", "Tough Fiber",       47, 51)
fnDrop("Bandit Killer", "Tough Carapace",    52, 56)

-- |[ =============================== Special Abilities and Tags =============================== ]|
--Enemies that have more than the standard attack or have special tags get them added here.

-- |[ ====================== Tag Application ===================== ]|
--Harpy Recruits have this tag indicating they can receive Battle Orders
fnTag("Harpy Recruit", {{"Can Take Battle Orders", 1}})

--Diagnostic effect.
fnTag("Training Dummy", {{"Passive: Protection Test", 1}})

-- |[ ========================= Abilities ======================== ]|
-- |[ ========= Northwoods ========= ]|
-- |[Bat]|
fnAbility("Bat Seeker", {"Enemy|Bat Bleed Attack", 100})
fnAbility("Bat Seeker", {"Enemy|Bat Bleed DoT",    100})
fnAbility("Bat Seeker", {"Enemy|Suck Blood",       100})
fnAbility("Bat Hunter", {"Enemy|Bat Bleed Attack", 100})
fnAbility("Bat Hunter", {"Enemy|Bat Bleed DoT",    100})
fnAbility("Bat Hunter", {"Enemy|Suck Blood",       100})

-- |[Bunny]|
fnAbility("Bunny Smuggler", {"Enemy|Burst Of Speed", 100})
fnAbility("Bunny Enforcer", {"Enemy|Burst Of Speed", 100})

-- |[Cave Crawler]|
fnAbility("Cave Muncher",  {"Enemy|Corrode Attack", 100})
fnAbility("Cave Muncher",  {"Enemy|Armor Rot",      100})
fnAbility("Cave Muncher",  {"Enemy|Natural Armor",    0})
fnAbility("Cave Devourer", {"Enemy|Corrode Attack", 100})
fnAbility("Cave Devourer", {"Enemy|Armor Rot",      100})
fnAbility("Cave Devourer", {"Enemy|Natural Armor",    0})

-- |[Harpy Officer]|
fnAbility("Harpy Officer", {"Enemy|Battle Orders", 100})

-- |[Poison Vine]|
fnAbility("Poison Vine", {"Enemy|Poison Attack", 100})
fnAbility("Poison Vine", {"Enemy|Poison Spine",  100})
fnAbility("Poison Vine", {"Enemy|Poison Cloud",  100})

-- |[Sevavi]|
fnAbility("Sevavi Guardian", {"Enemy|Lariat",         100})
fnAbility("Sevavi Guardian", {"Enemy|Overhead Smash", 50})

-- |[ ========== Westwoods ========= ]|
-- |[Bandits]|
fnAbility("Bandit Stalker", {"Enemy|Bleed Attack",   70})
fnAbility("Bandit Stalker", {"Enemy|Bleed DoT",     130})
fnAbility("Bandit Stalker", {"Enemy|Poison DoT",     60})
fnAbility("Bandit Killer",  {"Enemy|Flame Attack",  100})
fnAbility("Bandit Killer",  {"Enemy|Strike Attack", 100})

-- |[Wildlife]|
fnAbility("Frost Crawler",      {"Enemy|Freeze Attack",       100})
fnAbility("Caustic Grub",       {"Enemy|Corrode Attack",      100})
fnAbility("Caustic Grub",       {"Enemy|Corrode DoT",          50})
fnAbility("Grub Nest",          {"Enemy|Caustic Cloud",       100})
fnAbility("Hungry Suckfly",     {"Enemy|Suck Blood No Curse", 100})
fnAbility("Neutral Toxishroom", {"Enemy|Poison Attack",       100})
fnAbility("Neutral Toxishroom", {"Enemy|Corrode Attack",      100})
fnAbility("Lurking Swamphag",   {"Enemy|Obscure Attack",      100})

-- |[ ======= Training Dummy ======= ]|
--Basic training dummy.
fnAbility("Training Dummy", {"Enemy|Bleed Attack",   50})
fnAbility("Training Dummy", {"Enemy|Poison Attack",  50})
fnAbility("Training Dummy", {"Enemy|Corrode Attack", 50})
fnAbility("Training Dummy", {"Enemy|Lariat",         50})

--Tag-Switch AI training dummy.
fnAbility("Tag Switch Dummy", {"Enemy|Bleed Attack",   50})
fnAbility("Tag Switch Dummy", {"Enemy|Poison Attack",  50})
fnAbility("Tag Switch Dummy", {"Enemy|Power Gate",      0})
fnAbility("Tag Switch Dummy", {"Enemy|Counterattack",   0})
fnAbility("Tag Switch Dummy", {"Enemy|Summon Ally",     0})

-- |[ ======================================= AI Scripts ======================================= ]|
--Change the scripts used by certain enemies.
fnAI("Poison Vine",    gsRoot .. "Combat/AIs/Chapter 2/Poison Vine.lua")
fnAI("Cave Muncher",   gsRoot .. "Combat/AIs/Chapter 2/Cave Muncher.lua")
fnAI("Harpy Officer",  gsRoot .. "Combat/AIs/Chapter 2/Harpy Officer.lua")
fnAI("Bunny Smuggler", gsRoot .. "Combat/AIs/Chapter 2/Bunny Smuggler.lua")
fnAI("Bunny Enforcer", gsRoot .. "Combat/AIs/Chapter 2/Bunny Enforcer.lua")

--The dummy is set to use these skills as a sampler.
fnAI("Training Dummy",   gsRoot .. "Combat/AIs/Chapter 2/Turn Counter Sample.lua")
fnAI("Tag Switch Dummy", gsRoot .. "Combat/AIs/Chapter 2/Tag Check Sample.lua")

-- |[ ========================================= Topics ========================================= ]|
fnTopic("Sevavi Guardian",{})

-- |[ ================================= Portrait Offset Lookup ================================= ]|
-- |[Listing]|
--             Portrait Path                                  | Turn Portrait Path                                      | Actor 
fnAddPortrait("Root/Images/Portraits/Combat/Bat",               "Root/Images/AdventureUI/TurnPortraits/Bat",             "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Brimhog",           "Root/Images/AdventureUI/TurnPortraits/Brimhog",         "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Buffodil",          "Root/Images/AdventureUI/TurnPortraits/Buffodil",        "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Bunny",             "Root/Images/AdventureUI/TurnPortraits/Bunny",           "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Butterbomber",      "Root/Images/AdventureUI/TurnPortraits/Butterbomber",    "Null")
fnAddPortrait("Root/Images/Portraits/Combat/CaveCrawler",       "Root/Images/AdventureUI/TurnPortraits/CaveCrawler",     "Null")
fnAddPortrait("Root/Images/Portraits/Combat/HarpyOfficer",      "Root/Images/AdventureUI/TurnPortraits/HarpyOfficer",    "Null")
fnAddPortrait("Root/Images/Portraits/Combat/HarpyRecruit",      "Root/Images/AdventureUI/TurnPortraits/HarpyRecruit",    "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Kitsune",           "Root/Images/AdventureUI/TurnPortraits/Kitsune",         "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Omnigoose",         "Root/Images/AdventureUI/TurnPortraits/Omnigoose",       "Null")
fnAddPortrait("Root/Images/Portraits/Combat/PoisonVine",        "Root/Images/AdventureUI/TurnPortraits/PoisonVine",      "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Sevavi",            "Root/Images/AdventureUI/TurnPortraits/Sevavi",          "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Treant",            "Root/Images/AdventureUI/TurnPortraits/Treant",          "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Turkerus",          "Root/Images/AdventureUI/TurnPortraits/Turkerus",        "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Unielk",            "Root/Images/AdventureUI/TurnPortraits/Unielk",          "Null")

--Westwoods
fnAddPortrait("Root/Images/Portraits/Combat/BunEye",            "Root/Images/AdventureUI/TurnPortraits/BunEye",          "Null")
fnAddPortrait("Root/Images/Portraits/Combat/BanditRed",         "Root/Images/AdventureUI/TurnPortraits/BanditR",         "Null")
fnAddPortrait("Root/Images/Portraits/Combat/BanditGreen",       "Root/Images/AdventureUI/TurnPortraits/BanditG",         "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Enforcer",          "Root/Images/AdventureUI/TurnPortraits/Enforcer",        "Null")
fnAddPortrait("Root/Images/Portraits/Combat/FrostCrawler",      "Root/Images/AdventureUI/TurnPortraits/FrostCrawler",    "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Grub",              "Root/Images/AdventureUI/TurnPortraits/Grub",            "Null")
fnAddPortrait("Root/Images/Portraits/Combat/GrubNest",          "Root/Images/AdventureUI/TurnPortraits/GrubNest",        "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Mushmup",           "Root/Images/AdventureUI/TurnPortraits/Mushmup",         "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Pseudogriffon",     "Root/Images/AdventureUI/TurnPortraits/Pseudogriffon",   "Null")
fnAddPortrait("Root/Images/Portraits/Combat/RedCap",            "Root/Images/AdventureUI/TurnPortraits/RedCap",          "Null")
fnAddPortrait("Root/Images/Portraits/Combat/SuckFly",           "Root/Images/AdventureUI/TurnPortraits/SuckFly",         "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Toxishroom",        "Root/Images/AdventureUI/TurnPortraits/Toxishroom",      "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Wisphag",           "Root/Images/AdventureUI/TurnPortraits/Wisphag",         "Null")
            

-- |[Map To Enemies, Northwoods]|
--Mainline
fnSetRemap("Bat Seeker",          "Root/Images/Portraits/Combat/Bat")
fnSetRemap("Quiet Butterbomber",  "Root/Images/Portraits/Combat/Butterbomber")
fnSetRemap("Buffodil",            "Root/Images/Portraits/Combat/Buffodil")
fnSetRemap("Cave Muncher",        "Root/Images/Portraits/Combat/CaveCrawler")
fnSetRemap("Poison Vine",         "Root/Images/Portraits/Combat/PoisonVine")
fnSetRemap("Bunny Smuggler",      "Root/Images/Portraits/Combat/Bunny")
fnSetRemap("Harpy Recruit",       "Root/Images/Portraits/Combat/HarpyRecruit")
fnSetRemap("Treant Wanderer",     "Root/Images/Portraits/Combat/Treant")
fnSetRemap("Harpy Officer",       "Root/Images/Portraits/Combat/HarpyOfficer")
fnSetRemap("Kitsune Warrior",     "Root/Images/Portraits/Combat/Kitsune")
fnSetRemap("Sevavi Guardian",     "Root/Images/Portraits/Combat/Sevavi")

--Hunting
fnSetRemap("Puissant Omnigoose", "Root/Images/Portraits/Combat/Omnigoose")
fnSetRemap("Clever Turkerus",    "Root/Images/Portraits/Combat/Turkerus")
fnSetRemap("Rowdy Brimhog",      "Root/Images/Portraits/Combat/Brimhog")
fnSetRemap("Unielk Grazer",      "Root/Images/Portraits/Combat/Unielk")

-- |[Map To Enemies, Westwoods]|
--Mainline
fnSetRemap("Bunny Enforcer",     "Root/Images/Portraits/Combat/Enforcer")
fnSetRemap("Bandit Stalker",     "Root/Images/Portraits/Combat/BanditRed")
fnSetRemap("Bandit Killer",      "Root/Images/Portraits/Combat/BanditGreen")
fnSetRemap("Frost Crawler",      "Root/Images/Portraits/Combat/FrostCrawler")
fnSetRemap("Caustic Grub",       "Root/Images/Portraits/Combat/Grub")
fnSetRemap("Grub Nest",          "Root/Images/Portraits/Combat/GrubNest")
fnSetRemap("Brutal Redcap",      "Root/Images/Portraits/Combat/RedCap")
fnSetRemap("Hungry Suckfly",     "Root/Images/Portraits/Combat/SuckFly")
fnSetRemap("Neutral Toxishroom", "Root/Images/Portraits/Combat/Toxishroom")
fnSetRemap("Lurking Swamphag",   "Root/Images/Portraits/Combat/Wisphag")
fnSetRemap("Cave Devourer",      "Root/Images/Portraits/Combat/CaveCrawler")
fnSetRemap("Bat Hunter",         "Root/Images/Portraits/Combat/Bat")

--Huntables.
fnSetRemap("Evasive Buneye",    "Root/Images/Portraits/Combat/BunEye")
fnSetRemap("Pseudogriffon Hen", "Root/Images/Portraits/Combat/Pseudogriffon")
fnSetRemap("Macho Brimhog",     "Root/Images/Portraits/Combat/Brimhog")
fnSetRemap("Unielk Slatherer",  "Root/Images/Portraits/Combat/Unielk")

-- |[Debug]|
fnSetRemap("Training Dummy", "Root/Images/Portraits/Combat/Sevavi")

-- |[ ======================================== Paragons ======================================== ]|
--Paragons have the same display abilities as the base case, but 10x the stats and 10x the rewards, and +3 all resistances.
-- Their setup must be done after all other entries are added.
local iTotal = #gzStatArray
for i = 1, iTotal, 1 do
    fnCreateParagon(i)
end

-- |[ ========================================== Debug ========================================= ]|
--Store all the data in chapter-local arrays.
gczaChapter2Stats = gzStatArray
gczaChapter2PortraitLookups = gzPortraitArray

--Print all stats to the console.
if(false) then
    io.write("Listing Enemy information:\n")
    for i = 1, #gczaChapter2Stats, 1 do
        io.write(string.format("%2i: %s - %i %i %i %i %i\n", i, gczaChapter5Stats[i].sActualName, gczaChapter5Stats[i].iHealth, gczaChapter5Stats[i].iAtk, gczaChapter5Stats[i].iIni, gczaChapter5Stats[i].iAcc, gczaChapter5Stats[i].iEvd))
    end
end
