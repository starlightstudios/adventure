-- |[ =================================== Post Combat Handler ================================== ]|
--Script called by combat whenever it ends. It passes in the combat state (victory, defeat, retreat, surrender)
-- and the name of the enemy in question.
--This is primarily used to track kills for paragon reasons.
if(fnArgCheck(1) == false) then return end
local iEndingType = LM_GetScriptArgument(0, "N")

--Debug.
Debug_PushPrint(false, "=========== Running post-combat handler ==========\n")

-- |[Victory]|
if(iEndingType == gciAC_Resolution_Victory) then
    
    --Scan the graveyard and track all the groupings of the defeated enemies.
    local iGraveyardSize = AdvCombat_GetProperty("Entities in Graveyard")
    Debug_Print("Enemies in graveyard: " .. iGraveyardSize .. "\n")
    
    --Iterate.
    for i = 0, iGraveyardSize-1, 1 do
        
        --Get ID and push.
        local iEntityID = AdvCombat_GetProperty("ID of Graveyard Entity", i)
        AdvCombat_SetProperty("Push Entity By ID", iEntityID)
        
            --Get the associated paragon grouping.
            local sParagonGrouping = AdvCombatEntity_GetProperty("Paragon Grouping")
            
            --If the grouping starts with ALWAYS, this is the unique paragon.
            local bIsUniqueParagon = false
            if(string.sub(sParagonGrouping, 1, 6) == "ALWAYS") then
                bIsUniqueParagon = true
                sParagonGrouping = string.sub(sParagonGrouping, 8)
            end
            
            --Locate the grouping and get its values.
            local iKillCount          = VM_GetVar("Root/Variables/Paragons/"..sParagonGrouping.."/iKillCount", "N")
            local iHasDefeatedParagon = VM_GetVar("Root/Variables/Paragons/"..sParagonGrouping.."/iHasDefeatedParagon", "N")
            
            --Increment kills by 1.
            VM_SetVar("Root/Variables/Paragons/"..sParagonGrouping.."/iKillCount", "N", iKillCount + 1)
        
            --If this was the unique paragon, mark it as killed.
            if(bIsUniqueParagon) then
                VM_SetVar("Root/Variables/Paragons/"..sParagonGrouping.."/iHasDefeatedParagon", "N", 1.0)
            end
        
            --Statistics:
            Debug_Print("==> Enemy ID: " .. iEntityID .. "\n")
            Debug_Print("    Enemy Display Name: " .. AdvCombatEntity_GetProperty("Display Name") .. "\n")
            Debug_Print("    Paragon Grouping: " .. sParagonGrouping .. "\n")
            Debug_Print("    Kills in this group: " .. iKillCount + 1 .. "\n")
        
        DL_PopActiveObject()
    end
end

-- |[Debug]|
Debug_PopPrint("============ Post Combat Handler Ends ============\n")
