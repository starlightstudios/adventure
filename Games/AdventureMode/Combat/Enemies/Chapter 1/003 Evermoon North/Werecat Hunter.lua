-- |[ ================================= Werecat Hunter, Normal ================================= ]|
--Rank 0 werecat found in the forest.
local sActualName = "Werecat Hunter"
local iUniqueID = fnStandardEnemyHandler(sActualName)

-- |[Additional Setup]|
--If this is not a combat check (Mugging, Auto-Win Pulse) then don't run cutscenes.
if(TA_GetProperty("Is Non Combat Check") == true) then return end

--If the entity needs more setup, like modifying AI or conditional cutscenes, do it here.
if(iUniqueID ~= 0) then
    
    --Seeing a partirhuman for the first time.
    local sMeiSeenPartirhuman = VM_GetVar("Root/Variables/Chapter1/Counts/sMeiSeenPartirhuman", "S")
    if(sMeiSeenPartirhuman == "Nothing") then
        VM_SetVar("Root/Variables/Chapter1/Counts/sMeiSeenPartirhuman", "S", "Werecat")
        AdvCombat_SetProperty("Add Script Execution", gsRoot .. "Combat/Cutscenes/Chapter 1/Meeting Werecat.lua")
    end
    
    --Increment the Cassandra werecat encounter number, if the event is taking place. Does not occur during muggings.
    local iStartedCassandraEvent = VM_GetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N")
    if(iStartedCassandraEvent == 1.0 and TA_GetProperty("Is Mugging Check") == false) then
        
        --Set the override for the next encounter.
        AdvCombat_SetProperty("Next Combat Music", "TimeSensitive", -1.0)
        
        --Increment.
        local iCassandraEncounters = VM_GetVar("Root/Variables/Chapter1/Scenes/iCassandraEncounters", "N")
        VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraEncounters", "N", iCassandraEncounters + 1.0)
        
        --Set the time of day.
        fnSetCassandraTime()
    end
end
