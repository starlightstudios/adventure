-- |[ ================================== Cultist Male, Normal ================================== ]|
--Found in the cultist hideout under the Dimensional Trap. One of the first enemies in the game.
local sActualName = "Initiate M Paragon"
local iUniqueID = fnStandardEnemyHandler(sActualName)

-- |[Additional Setup]|
--If the entity needs more setup, like modifying AI or conditional cutscenes, do it here.
