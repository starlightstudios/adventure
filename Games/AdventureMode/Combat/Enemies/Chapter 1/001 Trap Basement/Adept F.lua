-- |[ ================================== Adept Female, Normal ================================== ]|
--Tougher cultist found in the dimensional trap.
local sActualName = "Adept F"
local iUniqueID = fnStandardEnemyHandler(sActualName)

-- |[Additional Setup]|
--If the entity needs more setup, like modifying AI or conditional cutscenes, do it here.
