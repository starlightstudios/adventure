-- |[ =================================== Enemy Auto Handler =================================== ]|
--If an enemy is being created with no special properties, this script gets called. During alias
-- building, pass the path "AUTO".
--The name of the enemy will be passed in.
if(fnArgCheck(1) == false) then return end

--Get actual name.
local sActualName = LM_GetScriptArgument(0)

--Create it.
local iUniqueID = fnStandardEnemyHandler(sActualName)
