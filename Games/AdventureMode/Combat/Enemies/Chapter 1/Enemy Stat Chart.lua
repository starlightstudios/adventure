-- |[ ================================= Enemy Statistics Chart ================================= ]|
--This chart lists off all enemy properties in one handy spot. Complicated enemies may need additional
-- setup, which is why they have their own files.
gzStatArray = {}
gzPortraitArray = {}

-- |[Diagnostics]|
local bDiagnostics = Debug_GetFlag("Enemy Stat Chart: All")
Debug_PushPrint(bDiagnostics, "Running Chapter 1 Enemy Stat Chart.\n")

-- |[ ====================================== Enemy Listing ===================================== ]|
--Notes: Resistances are difference from standard, not actual values. The standard resistance is gciNormalResist.
-- Extra abilities and tags are added later.
--The "Attack" column is the basic attack this entity uses. Place "Null" to have no attacks or to manually specify
-- additional attacks later.
Debug_Print("Building basic enemy listing.\n")

-- |[Chart]|
--     Actual Name        | Display Name       | Attack ||||| Lv | Health | Attack | Ini | Acc | Evd | Stun ||||| Pr | Sl | St | Pc | Fl | Fr | Sh | Cr | Ob | Bl | Ps | Cr | Tr ||||| Plt | Exp | JP |  Drop               | Chnc
fnAdd("Initiate F",        "Initiate",          "Sword",      1,      39,      32,   20,  -25,   10,   100,     0,   0,   0,   0,   0,   0,   0,   0,   0,  -5,   0,   0,   0,      5,   13,  25, "Wallet x10",           10)
fnAdd("Initiate M",        "Initiate",          "Sword",      1,      41,      31,   19,  -20,   13,   100,     0,   1,   0,   0,   0,   0,   0,   0,   0,  -5,   0,   0,   0,      8,   13,  25, "Tip Book x5",          10)
fnAdd("Adept F",           "Adept",             "Sword",      3,      79,      42,   40,    0,   20,   100,     0,   0,   0,   0,   0,   0,   0,   0,   0,  -3,   0,   0,   0,     12,   16,  25, "Adamantite Powder x1", 10)
fnAdd("Adept M",           "Adept",             "Sword",      3,     124,      51,   39,    0,   13,   100,     0,   1,   0,   0,   0,   0,   0,   0,   0,  -5,   0,   0,   0,     13,   16,  25, "Adamantite Powder x1", 10)
fnAdd("Alraune Petal",     "Alraune Petal",     "Sword",      5,      85,      40,   60,    0,   15,   100,     0,  -1,   1,   1,  -6,  -2,   4,   0,   0,   4,   3,   0,   0,     16,   26,  25, "Palliative",            5)
fnAdd("Bee Buddy",         "Bee Buddy",         "Claw",       3,      23,      18,  100,   25,   10,    50,     0,   0,   0,   1,  -2,  -2,   0,   0,   0,   0,   3,   2,  -5,      3,    9,  25, "Null",                  0)
fnAdd("Bee Scout",         "Bee Scout",         "Claw",       5,     100,      56,   45,   25,   20,   100,     0,   0,   0,   2,  -2,  -2,   0,   0,   0,  -1,   3,   2,   2,      5,   19,  25, "Rubose Gem",            5)
fnAdd("Bubbly Slimegirl",  "Bubbly Slimegirl",  "Claw",       5,     125,      48,   20,   20,   10,   100,     0,   0,   4,   3,   0,  -5,  -2,   0,   0,   8,   0,   0,  -2,     20,   21,  25, "Yemite Gem",            5)
fnAdd("Chilly Slimegirl",  "Chilly Slimegirl",  "Null",       5,     130,      48,   20,   20,   10,   100,     0,   0,   4,   3,  -5,   5,   0,   0,   0,   8,   0,   0,  -2,     25,   27,  25, "Nockrion Gem",          5)
fnAdd("Inky Slimegirl",    "Inky Slimegirl",    "Claw",       5,     160,      48,   20,   20,   10,   150,     0,   3,   4,   3,   0,  -5,   0,   0,   0,   8,   8,   8,  -2,     25,   22,  25, "Piorose Gem",           5)
fnAdd("Jelli",             "Jelli",             "Null",       3,      50,      30,   40,   20,   10,    50,     0,   3,   4,   3,   0,  -5,   0,   0,   0,   8,   8,   8,  -2,      7,    8,  25, "Null",                  0)
fnAdd("Sprout Pecker",     "Sprout Pecker",     "Claw",       5,     120,      44,   60,    0,   20,   100,     0,  -1,   1,   1,  -5,   2,   4,   0,   0,   3,   3,   3,  -4,     20,   17,  25, "Mordreen Gem",          5)
fnAdd("Sproutling",        "Sproutling",        "Claw",       3,      20,      35,   70,   30,   20,    50,     0,  -1,   1,   1,  -5,   2,   4,   0,   0,   3,   3,   3,  -4,      5,   12,  25, "Null",                  0)
fnAdd("Toxic Slimegirl",   "Toxic Slimegirl",   "Claw",       5,     115,      48,   20,   20,   10,   100,     0,   0,   4,   3,   0,  -5,   0,   0,   0,   8,   8,   8,  -2,     22,   22,  25, "Adamantite Powder x1", 10)
fnAdd("Alraune Stem",      "Alraune Stem",      "Sword",      8,     160,      64,   70,   20,   45,   100,     0,  -1,   1,   1,  -6,  -2,   4,   0,   0,   4,   3,   0,   0,     41,   75,  25, "Adamantite Powder x1", 20)
fnAdd("Bee Gatherer",      "Bee Gatherer",      "Claw",       8,     175,      68,   65,   25,   40,   100,     0,   0,   0,   2,  -2,  -2,   0,   0,   0,  -1,   3,   2,   2,     28,   64,  25, "Phonophage Gem",        5)
fnAdd("Bee Sniffer",       "Bee Sniffer",       "Claw",       8,      80,      32,  100,   65,   40,    50,     0,   0,   0,   1,  -2,  -2,   0,   0,   0,   0,   3,   2,  -5,     15,   15,  25, "Null",                  0)
fnAdd("Werecat Hunter",    "Werecat Hunter",    "Claw",       8,      90,      92,  100,   35,   70,   100,     0,   0,   0,   0,  -3,   2,   2,   0,   0,  -3,   0,   0,  -4,     32,   67,  25, "Tip Book x10",         10)
fnAdd("Best Friend",       "Best Friend",       "Claw",      12,     175,      95,   70,   45,   85,    50,     1,   0,  -2,  -2,  -5,   3,   0, -10, -10,   0,   0,   0,  12,      9,   62,  25, "Wallet x30",           10)
fnAdd("Bloody Mirror",     "Bloody Mirror",     "Sword",      8,      50,      52,   70,   30,   30,    50,     0,   3,  -5,   5,   0,   0,   2,   0,   0,   8,   8,  -5,  10,      8,   45,  25, "Null",                  0)
fnAdd("Candle Haunt",      "Candle Haunt",      "Null",       8,      70,      47,   10,  -10,    0,    50,     0,   3,   0,   0,   0,  -5,   0,   0,   0,   8,   8,  -5,  10,      8,   35,  25, "Null",                  0)
fnAdd("Eye Drawer",        "Eye Drawer",        "Null",      12,     310,      80,   10,   30,   20,   140,     0,   3,   3,   3, -10,   3,   5,   0,   0,   8,   8,   8,  10,    100,  140,  25, "Wallet x50",           10)
fnAdd("Ghost Maid",        "Ghost Maid",        "Sword",     12,     180,      79,   30,   30,   30,   100,     0,   3,   3,   3,  -5,   5,   0,   0,   0,   8,   8,   8,  10,     57,   96,  25, "Adamantite Powder x1", 10)
fnAdd("Mirror Image",      "Mirror Image",      "Claw",      12,     255,     140,   70,   30,  130,   100,     1,   1,  -1,   1,   0,   0,  -5,  -8,  -8,   0,   0,  -2,   8,     42,   87,  25, "Adamantite Powder x1", 10)
fnAdd("Skitterer",         "Skitterer",         "Claw",       8,      91,      65,   70,   30,   80,    50,     0,   0, -10,   0,   0,   0,  -2, -10, -10,   0,   0,   0,  10,     12,   38,  25, "Null",                  0)
fnAdd("Wisphag",           "Wisphag",           "Null",      12,     350,     130,   10,   10,   10,   100,     4,   2,   2,   2,   3,   7,  -3,  -6,   9,  -7,   4,   0,   6,    100,   80,  25, "Null",                  0)
fnAdd("Mushraune",         "Mushraune",         "Null",      12,     222,      90,   20,   60,   50,   150,     0,  -1,   1,   1,  -6,  -2,   4,   0,   0,   4,   3,   3,   3,     15,   76,  25, "Glintsteel Gem",        5)
fnAdd("Werecat Prowler",   "Werecat Prowler",   "Claw",      12,     122,     120,   70,   60,  120,   100,     0,   0,   0,   0,  -3,   2,   2,   0,   0,  -3,   0,   0,  -4,     70,   95,  25, "Tip Book x10",         10)
fnAdd("Rubberraune",       "Rubberraune",       "Sword",     13,     330,     130,   30,   60,   55,   150,     0,   5,  -5,   5, -12,  10,  10,   0,   0,  10,  10,  10,  10,     78,  180,  25, "Wallet x100",          10)
fnAdd("Rubbercat",         "Rubbercat",         "Claw",      13,     220,     120,   40,   60,   80,   150,     0,   5,  -5,   5, -12,  10,  10,   0,   0,  10,  10,  10,  10,     78,  180,  25, "Nockrion Gem",         10)
fnAdd("RubbercatThief",    "Rubbercat Thief",   "Claw",      13,     210,     145,   50,   70,  130,   100,     0,   5,  -5,   5, -12,  10,  10,   0,   0,  10,  10,  10,  10,     78,  180,  25, "Rubose Gem",           10)
fnAdd("Rubberbee",         "Rubberbee",         "Claw",      13,     370,     111,   25,   65,   40,   150,     0,   5,  -5,   5, -12,  10,  10,   0,   0,  10,  10,  10,  10,     78,  180,  25, "Tip Book x10",         10)
fnAdd("Bandit Scrub",      "Bandit Scrub",      "Sword",     12,     250,     120,   40,   60,   60,   100,     0,   0,   0,   1,   0,   0,   0,   1,   0,  -5,   3,   0,  -1,    121,  140,  25, "Tip Book x10",         10)
fnAdd("Bandit Goon",       "Bandit Goon",       "Sword",     12,     300,     130,   40,   70,   70,   100,     0,   1,   0,   1,   0,   0,   0,   1,   0,  -5,   3,   0,  -1,    121,  140,  25, "Tip Book x10",         10)
fnAdd("Demonic Mercenary", "Demonic Mercenary", "Sword",     12,     396,     120,   30,   60,   70,   110,     0,   2,   2,   2,   2,   0,   0, -10,  10,  -3,   3,   0,   2,    220,  140,  25, "Tip Book x10",         10)
fnAdd("Bandit Boss",       "Bandit Boss",       "Sword",     12,     415,     150,   45,   85,  100,   100,     0,   1,   1,   1,   2,   0,   0, -10,  10,  -5,   3,   0,   2,    300,  140,  25, "Tip Book x10",         10)
fnAdd("Mannequin Scrub",   "Bandit Scrub",      "Sword",     14,     330,     140,   50,   80,   75,    80,     1,   1,  -2,   1,  -1,   2,   0,   0,   0,   3,   3,  -1,   2,      0,  220,  25, "Tip Book x10",         10)
fnAdd("Mannequin Goon",    "Bandit Goon",       "Sword",     14,     330,     140,   50,   80,   75,    80,     1,   1,  -2,   1,  -1,   2,   0,   0,   0,   3,   3,  -1,   2,      0,  220,  25, "Tip Book x10",         10) 
fnAdd("Alraune Keeper",    "Alraune Keeper",    "Sword",     14,     230,     100,   60,   60,   55,   100,     0,  -1,   1,   1,  -6,  -2,   4,   0,   0,   4,   3,   0,   0,     49,  128,  25, "Healing Tincture",     10)
fnAdd("Sprout Thrasher",   "Sprout Thrasher",   "Claw",      14,     210,      67,   60,   30,   50,   100,     0,  -1,   1,   1,  -5,   2,   4,   0,   0,   3,   3,   3,  -4,     20,   17,  25, "Healing Tincture",     10)
fnAdd("Bee Skirmisher",    "Bee Skirmisher",    "Claw",      14,     270,     111,   55,   65,   40,   100,     0,   0,   0,   2,  -2,  -2,   0,   0,   0,  -1,   3,   2,   2,     62,  135,  25, "Smelling Salts",       10)
fnAdd("Bee Tracker",       "Bee Tracker",       "Claw",      14,     152,      42,  100,   65,   90,    50,     0,   0,   0,   1,  -2,  -2,   0,   0,   0,   0,   3,   2,  -5,     35,   45,  25, "Null",                  0)
fnAdd("Werecat Thief",     "Werecat Thief",     "Claw",      14,     110,     145,   80,   70,  130,   100,     0,   0,   0,   0,  -3,   2,   2,   0,   0,  -3,   0,   0,  -4,    220,  115,  25, "Wallet x100",          20)
fnAdd("Zombee",            "Zombee",            "Claw",      15,     400,     130,   10,   70,   50,    70,     2,   0,   0,   2,  -5,  -5,   0, -10, -10,  -2,   8,   0,2000,     32,  155,  25, "Adamantite Flakes x1", 20)
fnAdd("Finger",            "Finger",            "Sword",     15,     350,     120,   50,   50,   50,   100,     0,   0,   0,   0,   0,   0,   0,   0,   0,  -3,   0,   0,   0,     65,  137,  25, "Leather Greatcoat",    10)
fnAdd("Palm",              "Palm",              "Sword",     15,     550,     110,   50,   50,   50,   100,     0,   1,   0,   0,   0,   0,   0,   0,   0,  -5,   0,   0,   0,     70,  127,  25, "Plated Leather Vest",  10)
fnAdd("Gravemarker",       "Gravemarker",       "Sword",     15,     520,     232,   50,   85,   50,   150,     0,   3,  -4,   3,   3,   3,   6,   5,  -5,   8,   8,  -8,2000,    115,  260,  25, "Null",                  0)
fnAdd("Mushthrall",        "Mushthrall",        "Null",      15,     370,     124,   30,   50,   20,   150,     0,  -1,   1,   1,  -6,  -2,   4,   0,   0,   4,   3,   3,   3,     78,  180,  25, "Poison Spore Cap",     10)

-- |[ =============================== Special Abilities and Tags =============================== ]|
--Enemies that have more than the standard attack or have special tags get them added here.

-- |[ =============== Resistant/Vulnerable Effects =============== ]|
--Certain enemies are vulnerable to specific damage types in ways not represented by their resistances. These effects spawn at creation
-- and are visible in the combat inspector so the player can easily see the target is resistant/vulnerable.
Debug_Print("Placing vulnerable/resistance tags.\n")
fnTag("Initiate F",        { {"Spawn Effect|Enemy.Stock.BlindVulnerable|50|50|5", 1}, {"Spawn Effect|Enemy.Stock.BleedVulnerable|30|50|3", 1} })
fnTag("Initiate M",        { {"Spawn Effect|Enemy.Stock.BlindVulnerable|70|70|7", 1}, {"Spawn Effect|Enemy.Stock.BleedVulnerable|30|50|3", 1} })
fnTag("Adept F",           { {"Spawn Effect|Enemy.Stock.BlindVulnerable|50|50|5", 1}, {"Spawn Effect|Enemy.Stock.BleedVulnerable|30|50|3", 1} })
fnTag("Adept M",           { {"Spawn Effect|Enemy.Stock.BlindVulnerable|70|70|7", 1}, {"Spawn Effect|Enemy.Stock.BleedVulnerable|30|50|3", 1} })
fnTag("Bee Buddy",         { {"Spawn Effect|Enemy.Stock.BlindResistant|50|50|5",  1} })
fnTag("Bee Scout",         { {"Spawn Effect|Enemy.Stock.BlindResistant|50|50|5",  1} })
fnTag("Sprout Pecker",     { {"Spawn Effect|Enemy.Stock.BlindVulnerable|50|50|5", 1} })
fnTag("Bee Gatherer",      { {"Spawn Effect|Enemy.Stock.BlindResistant|50|50|5",  1} })
fnTag("Bee Sniffer",       { {"Spawn Effect|Enemy.Stock.BlindResistant|50|50|5",  1} })
fnTag("Bloody Mirror",     { {"Spawn Effect|Enemy.Stock.BlindResistant|50|50|5",  1} })
fnTag("Candle Haunt",      { {"Spawn Effect|Enemy.Stock.BlindResistant|50|50|5",  1} })
fnTag("Ghost Maid",        { {"Spawn Effect|Enemy.Stock.BlindResistant|50|50|5",  1} })
fnTag("Sprout Thrasher",   { {"Spawn Effect|Enemy.Stock.BlindVulnerable|50|50|5", 1} })
fnTag("Bee Skirmisher",    { {"Spawn Effect|Enemy.Stock.BlindResistant|50|50|5",  1} })
fnTag("Bee Tracker",       { {"Spawn Effect|Enemy.Stock.BlindResistant|50|50|5",  1} })
fnTag("Zombee",            { {"Spawn Effect|Enemy.Stock.BlindResistant|50|50|5",  1} })
fnTag("Finger",            { {"Spawn Effect|Enemy.Stock.BlindVulnerable|50|50|5", 1}, {"Spawn Effect|Enemy.Stock.BleedVulnerable|30|25|3", 1} })
fnTag("Palm",              { {"Spawn Effect|Enemy.Stock.BlindVulnerable|70|70|7", 1}, {"Spawn Effect|Enemy.Stock.BleedVulnerable|30|25|3", 1} })
fnTag("Gravemarker",       { {"Spawn Effect|Enemy.Stock.BlindResistant|50|50|5",  1} })

-- |[ ================== KO Stat/Resist Setting ================== ]|
--By default, the player needs a specific number of KOs, 5 for stats, 10 for resists, to see extra information on the combat inspector.
-- Some 'lesser' enemies have a lower count needed.
Debug_Print("Placing KO/Resist tags.\n")
fnAppendEnemyTag("Bee Buddy",         { {"KO Tag Stats", 3}, {"KO Tag Resists", 5} })
fnAppendEnemyTag("Jelli",             { {"KO Tag Stats", 3}, {"KO Tag Resists", 5} })
fnAppendEnemyTag("Sprout Pecker",     { {"KO Tag Stats", 3}, {"KO Tag Resists", 5} })
fnAppendEnemyTag("Sproutling",        { {"KO Tag Stats", 3}, {"KO Tag Resists", 5} })
fnAppendEnemyTag("Bee Sniffer",       { {"KO Tag Stats", 3}, {"KO Tag Resists", 5} })
fnAppendEnemyTag("Bloody Mirror",     { {"KO Tag Stats", 3}, {"KO Tag Resists", 5} })
fnAppendEnemyTag("Candle Haunt",      { {"KO Tag Stats", 3}, {"KO Tag Resists", 5} })
fnAppendEnemyTag("Eye Drawer",        { {"KO Tag Stats", 3}, {"KO Tag Resists", 5} })
fnAppendEnemyTag("Sprout Thrasher",   { {"KO Tag Stats", 3}, {"KO Tag Resists", 5} })
fnAppendEnemyTag("Bee Tracker",       { {"KO Tag Stats", 3}, {"KO Tag Resists", 5} })
fnAppendEnemyTag("Gravemarker",       { {"KO Tag Stats", 3}, {"KO Tag Resists", 5} })

-- |[ ==================== Rubberable Enemies ==================== ]|
--All enemies on this list gain the "Is Rubberable" tag, meaning they can be afflicted during the rubber sequence.
Debug_Print("Placing rubberable tags.\n")
local saRubberList = {"Alraune Petal", "Bee Scout", "Alraune Stem", "Bee Gatherer", "Werecat Hunter", "Werecat Prowler", "Alraune Keeper", "Bee Skirmisher", "Werecat Thief"}
for i = 1, #saRubberList, 1 do
    fnAppendEnemyTag(saRubberList[i], {{"Is Rubberable", 1}})
end

-- |[ ================= Un-Strangleable Enemies ================== ]|
--All enemies on this list gain the "Cannot Strangle" tag. The Stalker/Lurker ability "Strangle" cannot target them.
Debug_Print("Placing stangle tags.\n")
local saNoStrangleList = {"Bubbly Slimegirl", "Chilly Slimegirl", "Inky Slimegirl", "Jelli", "Toxic Slimegirl", "Best Friend", "Bloody Mirror", "Candle Haunt", "Eye Drawer", "Mirror Image", "Gravemarker", 
                          "Rubberraune", "Rubbercat", "RubbercatThief", "Rubberbee", "Mannequin Scrub", "Mannequin Goon"}
for i = 1, #saNoStrangleList, 1 do
    fnAppendEnemyTag(saNoStrangleList[i], {{"Cannot Strangle", 1}})
end

-- |[ ====================== AI Application ====================== ]|
--AIs are applied as tags, using constants in the AIString lookups. These are technically a tag.
Debug_Print("Applying AIStrings.\n")

--fnApplyAIString(sEnemyName, sAIStringName)
fnApplyAIString("Demonic Mercenary", "Demon Mercenary")
fnApplyAIString("Bandit Scrub",      "Bandit Scrub")
fnApplyAIString("Bandit Goon",       "Bandit Goon")
fnApplyAIString("Bandit Boss",       "Bandit Boss")
fnApplyAIString("Mannequin Scrub",   "Bandit Scrub")
fnApplyAIString("Mannequin Goon",    "Bandit Goon")
fnApplyAIString("Mirror Image",      "Mirror Image")
fnApplyAIString("Best Friend",       "Best Friend")

-- |[ ========================= Abilities ======================== ]|
--Abilities are given a weight value which gives the default AI the chance to use the ability. Enemies that use
-- an AIScript above don't need to have their abilities defined this way.
Debug_Print("Marking abilities.\n")

-- |[Generic Abilities]|
--Strike.
fnAbility("Werecat Thief", {"Enemy|Strike Attack", 100})
fnAbility("Gravemarker",   {"Enemy|Strike Attack", 100})
fnAbility("Eye Drawer",    {"Enemy|Strike Attack", 100})

--Pierce.
fnAbility("Alraune Petal",  {"Enemy|Pierce Attack", 100})
fnAbility("Alraune Stem",   {"Enemy|Pierce Attack", 100})
fnAbility("Alraune Keeper", {"Enemy|Pierce Attack", 100})
fnAbility("Mushraune",      {"Enemy|Pierce Attack", 100})
fnAbility("Mushthrall",     {"Enemy|Pierce Attack", 100})

--Ice.
fnAbility("Ghost Maid",       {"Enemy|Freeze Attack", 100})
fnAbility("Chilly Slimegirl", {"Enemy|Freeze Attack", 100})

--Fire.
fnAbility("Candle Haunt", {"Enemy|Flame Attack", 100})

--Poison.
fnAbility("Bubbly Slimegirl", {"Enemy|Poison Attack", 100})
fnAbility("Toxic Slimegirl",  {"Enemy|Poison DoT",    100, "Enemy|Poison Attack", 100})
fnAbility("Jelli",            {"Enemy|Poison DoT",    100})

--Bleed.
fnAbility("Werecat Hunter",  {"Enemy|Bleed DoT", 100})
fnAbility("Werecat Prowler", {"Enemy|Bleed DoT", 100})

--Crusade
fnAbility("Wisphag", {"Enemy|Crusade Attack", 100})

--Obscure
fnAbility("Wisphag", {"Enemy|Obscure Attack", 100})

-- |[Chapter Specific Abilities]|
--Bee Abilities.
fnAbility("Bee Scout",      {"Enemy|Sugar Rush", 33})
fnAbility("Bee Gatherer",   {"Enemy|Sugar Rush", 33})
fnAbility("Bee Skirmisher", {"Enemy|Sugar Rush", 33})

--Sproutpecker.
fnAbility("Sprout Pecker",   {"Enemy|Peck", 50})
fnAbility("Sprout Thrasher", {"Enemy|Peck", 50})

--Powerful Cultists.
fnAbility("Finger", {"Enemy|Wild Swing", 33})
fnAbility("Palm",   {"Enemy|Frenzy",     33})

-- |[ ========================================= Topics ========================================= ]|
--Getting into a battle with the enemy in question will immediately unlock the topics provided.
Debug_Print("Setting topics.\n")
fnTopic("Initiate F",       {"Cultists"})
fnTopic("Initiate M",       {"Cultists"})
fnTopic("Adept F",          {"Cultists"})
fnTopic("Adept M",          {"Cultists"})
fnTopic("Alraune Petal",    {"Alraunes"})
fnTopic("Bee Buddy",        {"Bees"})
fnTopic("Bee Scout",        {"Bees"})
fnTopic("Bubbly Slimegirl", {"Slimes"})
fnTopic("Chilly Slimegirl", {"Slimes"})
fnTopic("Inky Slimegirl",   {"Slimes"})
fnTopic("Jelli",            {"Slimes"})
fnTopic("Sprout Pecker",    {"Alraunes"})
fnTopic("Sproutling",       {"Alraunes"})
fnTopic("Toxic Slimegirl",  {"Slimes"})
fnTopic("Alraune Stem",     {"Alraunes"})
fnTopic("Bee Gatherer",     {"Bees"})
fnTopic("Bee Sniffer",      {"Bees"})
fnTopic("Werecat Hunter",   {"Werecats"})
fnTopic("Bloody Mirror",    {"Ghosts"})
fnTopic("Candle Haunt",     {"Ghosts"})
fnTopic("Eye Drawer",       {"Ghosts"})
fnTopic("Ghost Maid",       {"Ghosts"})
fnTopic("Mushraune",        {"Mushraunes"})
fnTopic("Werecat Prowler",  {"Werecats"})
fnTopic("Alraune Keeper",   {"Alraunes"})
fnTopic("Sprout Thrasher",  {"Alraunes"})
fnTopic("Bee Skirmisher",   {"Bees"})
fnTopic("Bee Tracker",      {"Bees"})
fnTopic("Werecat Burglar",  {"Werecats"})
fnTopic("Finger",           {"Cultists"})
fnTopic("Palm",             {"Cultists"})
fnTopic("Gravemarker",      {"Gravemarkers"})
fnTopic("Mushthrall",       {"Mushraunes"})
fnTopic("Rubberraune",      {"Rubberines"})
fnTopic("Rubbercat",        {"Rubberines"})
fnTopic("RubbercatThief",   {"Rubberines"})
fnTopic("Rubberbee",        {"Rubberines"})
fnTopic("Bandit Scrub",     {"Bandits"})
fnTopic("Bandit Goon",      {"Bandits"})
fnTopic("Demonic Mercenary",{"Bandits"})
fnTopic("Bandit Boss",      {"Bandits"})

-- |[ ================================= Portrait Offset Lookup ================================= ]|
Debug_Print("Building portrait lookup table.\n")

-- |[Listing]|
--            Portrait Path                                 | Turn Portrait Path                                   | Actor 
fnAddPortrait("Root/Images/Portraits/Combat/CultistF",        "Root/Images/AdventureUI/TurnPortraits/CultistF",     "CultistF")
fnAddPortrait("Root/Images/Portraits/Combat/CultistM",        "Root/Images/AdventureUI/TurnPortraits/CultistM",     "CultistM")
fnAddPortrait("Root/Images/Portraits/Combat/Alraune",         "Root/Images/AdventureUI/TurnPortraits/Alraune",      "Alraune")
fnAddPortrait("Root/Images/Portraits/Combat/BeeBuddy",        "Root/Images/AdventureUI/TurnPortraits/BeeBuddy",     "Null")
fnAddPortrait("Root/Images/Portraits/Combat/BeeGirl",         "Root/Images/AdventureUI/TurnPortraits/Bee",          "Bee")
fnAddPortrait("Root/Images/Portraits/Combat/Slime",           "Root/Images/AdventureUI/TurnPortraits/Slime",        "Slime")
fnAddPortrait("Root/Images/Portraits/Combat/SlimeB",          "Root/Images/AdventureUI/TurnPortraits/SlimeB",       "Slime")
fnAddPortrait("Root/Images/Portraits/Combat/Ink_Slime",       "Root/Images/AdventureUI/TurnPortraits/InkSlime",     "Slime")
fnAddPortrait("Root/Images/Portraits/Combat/Jelli",           "Root/Images/AdventureUI/TurnPortraits/Jelli",        "Null")
fnAddPortrait("Root/Images/Portraits/Combat/SproutPecker",    "Root/Images/AdventureUI/TurnPortraits/Sproutpecker", "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Sproutling",      "Root/Images/AdventureUI/TurnPortraits/Sproutling",   "Null")
fnAddPortrait("Root/Images/Portraits/Combat/SlimeG",          "Root/Images/AdventureUI/TurnPortraits/SlimeG",       "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Werecat",         "Root/Images/AdventureUI/TurnPortraits/Werecat",      "Werecat")
fnAddPortrait("Root/Images/Portraits/Combat/BestFriend",      "Root/Images/AdventureUI/TurnPortraits/BestFriend",   "Null")
fnAddPortrait("Root/Images/Portraits/Combat/BloodyMirror",    "Root/Images/AdventureUI/TurnPortraits/BloodyMirror", "Null")
fnAddPortrait("Root/Images/Portraits/Combat/CandleHaunt",     "Root/Images/AdventureUI/TurnPortraits/CandleHaunt",  "Null")
fnAddPortrait("Root/Images/Portraits/Combat/EyeDrawer",       "Root/Images/AdventureUI/TurnPortraits/EyeDrawer",    "Null")
fnAddPortrait("Root/Images/Portraits/Combat/MaidGhost",       "Root/Images/AdventureUI/TurnPortraits/Ghost",        "Ghost")
fnAddPortrait("Root/Images/Portraits/Combat/MirrorImage",     "Root/Images/AdventureUI/TurnPortraits/MirrorImage",  "Null")
fnAddPortrait("Root/Images/Portraits/Combat/SkullCrawler",    "Root/Images/AdventureUI/TurnPortraits/SkullCrawler", "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Wisphag",         "Root/Images/AdventureUI/TurnPortraits/WispHag",      "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Mushraune",       "Root/Images/AdventureUI/TurnPortraits/Mushraune",    "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Werecat_Burglar", "Root/Images/AdventureUI/TurnPortraits/WerecatThief", "WerecatThief")
fnAddPortrait("Root/Images/Portraits/Combat/Zombee",          "Root/Images/AdventureUI/TurnPortraits/Zombee",       "Zombee")
fnAddPortrait("Root/Images/Portraits/Combat/Gravemarker",     "Root/Images/AdventureUI/TurnPortraits/Gravemarker",  "Null")
fnAddPortrait("Root/Images/Portraits/Combat/BanditCatBlack",  "Root/Images/AdventureUI/TurnPortraits/BanditCatB",   "Null")
fnAddPortrait("Root/Images/Portraits/Combat/BanditF",         "Root/Images/AdventureUI/TurnPortraits/BanditF",      "Null")
fnAddPortrait("Root/Images/Portraits/Combat/BanditM",         "Root/Images/AdventureUI/TurnPortraits/BanditM",      "Null")
fnAddPortrait("Root/Images/Portraits/Combat/DemonMerc",       "Root/Images/AdventureUI/TurnPortraits/MercDemon",    "Null")
fnAddPortrait("Root/Images/Portraits/Combat/MannBanditF",     "Root/Images/AdventureUI/TurnPortraits/MannBanditF",  "Null")
fnAddPortrait("Root/Images/Portraits/Combat/MannBanditM",     "Root/Images/AdventureUI/TurnPortraits/MannBanditM",  "Null")

--Rubber
fnAddPortrait("Root/Images/Portraits/Combat/WerecatRubber",        "Root/Images/AdventureUI/TurnPortraits/WerecatR",      "RubberWerecat",   -192,    -119,    -132,     452,       -1000,              -1000)
fnAddPortrait("Root/Images/Portraits/Combat/WerecatBurglarRubber", "Root/Images/AdventureUI/TurnPortraits/WerecatThiefR", "RubberWerecatThief", 4,     -70,      73,     497,       -1000,              -1000)
fnAddPortrait("Root/Images/Portraits/Combat/AlrauneRubber",        "Root/Images/AdventureUI/TurnPortraits/AlrauneR",      "RubberAlraune",    -27,     -47,      41,     507,       -1000,              -1000)
fnAddPortrait("Root/Images/Portraits/Combat/BeeGirlRubber",        "Root/Images/AdventureUI/TurnPortraits/BeeR",          "RubberBee",       -185,     -81,    -111,     478,       -1000,              -1000)

-- |[Mapping]|
Debug_Print("Mapping portraits.\n")
fnSetRemap("Initiate F",        "Root/Images/Portraits/Combat/CultistF")
fnSetRemap("Initiate M",        "Root/Images/Portraits/Combat/CultistM")
fnSetRemap("Adept F",           "Root/Images/Portraits/Combat/CultistF")
fnSetRemap("Adept M",           "Root/Images/Portraits/Combat/CultistM")
fnSetRemap("Alraune Petal",     "Root/Images/Portraits/Combat/Alraune")
fnSetRemap("Bee Buddy",         "Root/Images/Portraits/Combat/BeeBuddy")
fnSetRemap("Bee Scout",         "Root/Images/Portraits/Combat/BeeGirl")
fnSetRemap("Bubbly Slimegirl",  "Root/Images/Portraits/Combat/Slime")
fnSetRemap("Chilly Slimegirl",  "Root/Images/Portraits/Combat/SlimeB")
fnSetRemap("Inky Slimegirl",    "Root/Images/Portraits/Combat/Ink_Slime")
fnSetRemap("Jelli",             "Root/Images/Portraits/Combat/Jelli")
fnSetRemap("Sprout Pecker",     "Root/Images/Portraits/Combat/SproutPecker")
fnSetRemap("Sproutling",        "Root/Images/Portraits/Combat/Sproutling")
fnSetRemap("Toxic Slimegirl",   "Root/Images/Portraits/Combat/SlimeG")
fnSetRemap("Alraune Stem",      "Root/Images/Portraits/Combat/Alraune")
fnSetRemap("Bee Gatherer",      "Root/Images/Portraits/Combat/BeeGirl")
fnSetRemap("Bee Sniffer",       "Root/Images/Portraits/Combat/BeeBuddy")
fnSetRemap("Werecat Hunter",    "Root/Images/Portraits/Combat/Werecat")
fnSetRemap("Best Friend",       "Root/Images/Portraits/Combat/BestFriend")
fnSetRemap("Bloody Mirror",     "Root/Images/Portraits/Combat/BloodyMirror")
fnSetRemap("Candle Haunt",      "Root/Images/Portraits/Combat/CandleHaunt")
fnSetRemap("Eye Drawer",        "Root/Images/Portraits/Combat/EyeDrawer")
fnSetRemap("Ghost Maid",        "Root/Images/Portraits/Combat/MaidGhost")
fnSetRemap("Mirror Image",      "Root/Images/Portraits/Combat/MirrorImage")
fnSetRemap("Skitterer",         "Root/Images/Portraits/Combat/SkullCrawler")
fnSetRemap("Wisphag",           "Root/Images/Portraits/Combat/Wisphag")
fnSetRemap("Mushraune",         "Root/Images/Portraits/Combat/Mushraune")
fnSetRemap("Werecat Prowler",   "Root/Images/Portraits/Combat/Werecat")
fnSetRemap("Bandit Scrub",      "Root/Images/Portraits/Combat/BanditF")
fnSetRemap("Bandit Goon",       "Root/Images/Portraits/Combat/BanditM")
fnSetRemap("Demonic Mercenary", "Root/Images/Portraits/Combat/DemonMerc")
fnSetRemap("Bandit Boss",       "Root/Images/Portraits/Combat/BanditCatBlack")
fnSetRemap("Mannequin Scrub",   "Root/Images/Portraits/Combat/MannBanditF")
fnSetRemap("Mannequin Goon",    "Root/Images/Portraits/Combat/MannBanditM")
fnSetRemap("Alraune Keeper",    "Root/Images/Portraits/Combat/Alraune")
fnSetRemap("Sprout Thrasher",   "Root/Images/Portraits/Combat/SproutPecker")
fnSetRemap("Bee Skirmisher",    "Root/Images/Portraits/Combat/BeeGirl")
fnSetRemap("Bee Tracker",       "Root/Images/Portraits/Combat/BeeBuddy")
fnSetRemap("Werecat Thief",     "Root/Images/Portraits/Combat/Werecat_Burglar")
fnSetRemap("Zombee",            "Root/Images/Portraits/Combat/Zombee")
fnSetRemap("Finger",            "Root/Images/Portraits/Combat/CultistF")
fnSetRemap("Palm",              "Root/Images/Portraits/Combat/CultistM")
fnSetRemap("Gravemarker",       "Root/Images/Portraits/Combat/Gravemarker")
fnSetRemap("Mushthrall",        "Root/Images/Portraits/Combat/Mushraune")
fnSetRemap("Rubberraune",       "Root/Images/Portraits/Combat/AlrauneRubber")
fnSetRemap("Rubbercat",         "Root/Images/Portraits/Combat/WerecatRubber")
fnSetRemap("RubbercatThief",    "Root/Images/Portraits/Combat/WerecatBurglarRubber")
fnSetRemap("Rubberbee",         "Root/Images/Portraits/Combat/BeeGirlRubber")

-- |[ ======================================== Paragons ======================================== ]|
--Paragons have the same display abilities as the base case, but 10x the stats and 10x the rewards, and +3 all resistances.
-- Their setup must be done after all other entries are added.
--To allow manual creation of paragons, this script is called. Any paragons remaining are created
-- by fnCreateParagon(), which will fail if the paragon is already found.
Debug_Print("Running paragon builder.\n")
local iTotal = #gzStatArray
LM_ExecuteScript(fnResolvePath() .. "Paragon Stat Chart.lua")

--Run across all non-paragons and attempt to create them.
Debug_Print("Creating unaccounted-for paragons.\n")
for i = 1, iTotal, 1 do
    fnCreateParagon(i)
end

-- |[ ================================== Tag Post-Processing =================================== ]|
--In order to cut down on the different number of functions needed for each enemy, tags can handle post-processing work
-- automatically. This is where that happens.
Debug_Print("Running tag post-processing.\n")
for i = 1, #gzStatArray, 1 do
    
    -- |[Iterate across tags:]|
    --These tags can modify how many kills are needed to see things in the inspector. Some may get removed from
    -- the final tag listing.
    local p = 1
    local iTagCount = #gzStatArray[i].zaTags
    while(p <= iTagCount) do
        
        --How many kills are needed to see the stats of an enemy in the combat inspector.
        if(gzStatArray[i].zaTags[p][1] == "KO Tag Stats") then
            gzStatArray[i].iKOForStats = gzStatArray[i].zaTags[p][2]
            table.remove(gzStatArray[i].zaTags, p)
            p = p - 1
            iTagCount = iTagCount - 1
    
        --How many kills are needed to see the resistances of an enemy in the combat inspector.
        elseif(gzStatArray[i].zaTags[p][1] == "KO Tag Resists") then
            gzStatArray[i].iKOForResists = gzStatArray[i].zaTags[p][2]
            table.remove(gzStatArray[i].zaTags, p)
            p = p - 1
            iTagCount = iTagCount - 1
        end
        
        --Next.
        p = p + 1
    end
end
        
        
-- |[ ========================================== Debug ========================================= ]|
--Store all the data in chapter-local arrays.
gczaChapter1Stats = gzStatArray
gczaChapter1PortraitLookups = gzPortraitArray

--Print all stats to the console.
if(false) then
    io.write("Listing Enemy information:\n")
    for i = 1, #gzStatArray, 1 do
        io.write(string.format("%2i: %s - %i %i %i %i %i\n", i, gzStatArray[i].sActualName, gzStatArray[i].iHealth, gzStatArray[i].iAtk, gzStatArray[i].iIni, gzStatArray[i].iAcc, gzStatArray[i].iEvd))
    end
end

--Check position lookups and report any that are missing.
if(false) then
    
    --Push Mei to receive the portrait positions.
    AdvCombat_SetProperty("Push Party Member By Slot", 0)
    
    for i = 1, #gzStatArray, 1 do
        LM_ExecuteScript(gsEnemyPortraitRouting, gzStatArray[i].sPortraitPath)
    end

    --Clean.
    DL_PopActiveObject()
end

-- |[Diagnostics]|
Debug_PopPrint("Finished.\n")
