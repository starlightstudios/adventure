-- |[ ===================================== Zombee Special ===================================== ]|
--Zombee found in the corrupted beehive. This version is summoned as a reinforcement during the 
-- boss battle, and has the same stats, but gives no XP/Loot.

-- |[ ===================================== Topic Handling ===================================== ]|
--No topics here.

-- |[ =================================== Common Statistics ==================================== ]|
--Setup common values regardless of functionality required from this file.
local iPlatina = 0
local iExperience = 0
local iJobPoints = 0

--Item Listing.
local zaItemList = {}

-- |[ ====================================== Mug Handling ====================================== ]|
--Cannot be mugged.
if(TA_GetProperty("Is Mugging Check") == true) then
    return
end

-- |[ ==================================== Combat Encounter ==================================== ]|
--Add this entity to the combat roster.

-- |[Setup]|
local iEnemyID = AdvCombat_GetProperty("Generate Unique ID")
local sEnemyUniqueName = string.format("Autogen|%02i", iEnemyID)
AdvCombat_SetProperty("Register Enemy", sEnemyUniqueName, 0)

    -- |[System]|
    --ID
    giLastEnemyID = RO_GetID()
    
    --Name
    AdvCombatEntity_SetProperty("Display Name", "Zombee")
    AdvCombatEntity_SetProperty("Cluster Name", "Zombee")

    --DataLibrary Variables
    local iUniqueID = RO_GetID()
    DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "/")
        
    --Get inspector properties.
    local sKOTrackerPath = VM_GetVar("Root/Variables/Global/Combat/sKOTrackerPath", "S")
    local iKOsSoFar = VM_GetVar(sKOTrackerPath .. "Zombee", "N")
        
    --Never show abilities on the inspector.
    AdvCombatEntity_SetProperty("Inspector Show Abilities", 1)
        
    -- |[Inspector Show Stats]|
    local iKOsNeeded = 5 - iKOsSoFar
    if(iKOsNeeded < 1) then iKOsNeeded = 0 end
    AdvCombatEntity_SetProperty("Inspector Show Stats", iKOsNeeded)
    
    -- |[Inspector Show Resistances]|
    iKOsNeeded = 10 - iKOsSoFar
    if(iKOsNeeded < 1) then iKOsNeeded = 0 end
    AdvCombatEntity_SetProperty("Inspector Show Resists", iKOsNeeded)
    
    -- |[DataLibrary]|
    --During dialogue, which actor represents this character.
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/sDialogueActor", "S", "Zombee")
    
    --AI
    AdvCombatEntity_SetProperty("AI Script", gsRoot .. "Combat/AIs/000 Normal AI.lua")

    -- |[Display]|
    --Images
    AdvCombatEntity_SetProperty("Combat Portrait", "Root/Images/Portraits/Combat/Zombee")
    AdvCombatEntity_SetProperty("Turn Icon", "Root/Images/AdventureUI/TurnPortraits/Zombee")
    
    --UI Positions
    AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,      282,  96)
    AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,      -17, -45)
    AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 299,  75)
    AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Enemy,       0,  77)

    -- |[Stats and Resistances]|
    --Statistics
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_HPMax,     300)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_MPMax,     100)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_CPMax,       0)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Attack,    130)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Initiative, 10)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Accuracy,   70)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Evade,      10)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_StunCap,    70)
    
    --Resistances
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Protection,     2)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Slash,   gciNormalResist+0)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Strike,  gciNormalResist+0)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Pierce,  gciNormalResist+2)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Flame,   gciNormalResist-5)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Freeze,  gciNormalResist-5)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Shock,   gciNormalResist+0)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Crusade, gciNormalResist-10)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Obscure, gciNormalResist-10)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Bleed,   gciNormalResist-2)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Poison,  gciNormalResist+8)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Corrode, gciNormalResist+0)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Terrify, gciResistanceForImmunity)
    
    --Sum.
    AdvCombatEntity_SetProperty("Recompute Stats")
    AdvCombatEntity_SetProperty("Health Percent", 1.00)
    
    -- |[Tags]|
    --No tagged resistances.

    -- |[Abilities]|
    --System Abilities
    fnEnemyStandardSystemAbilities()

    --Register abilities.
    local sJobAbilityPath = gsRoot .. "Combat/Enemies/Abilities/Chapter 0 Enemies/"
    LM_ExecuteScript(sJobAbilityPath .. "Slash Claw Attack.lua", gciAbility_Create)
    
    --Set ability slots.
    AdvCombatEntity_SetProperty("Set Ability Slot",             0, 0, "Enemy|Claw Attack")
    AdvCombatEntity_SetProperty("Set Ability Slot Probability", 0, 0, 100)
    
    -- |[Rewards Handling]|
    --Factor for mugging.
    local fXPFactor = 1.0
    local fJPFactor = 1.0
    local fPLFactor = 1.0
    if(TA_GetProperty("Was Mugged") == true) then
        fXPFactor = 1.0 - gcfMugExperienceRate
        fJPFactor = 1.0 - gcfMugPlatinaRate
        fPLFactor = 1.0 - gcfMugJPRate
    end
    
    --Rewards
    AdvCombatEntity_SetProperty("Reward XP",      math.floor(iExperience * fXPFactor))
    AdvCombatEntity_SetProperty("Reward JP",      math.floor(iJobPoints  * fJPFactor))
    AdvCombatEntity_SetProperty("Reward Platina", math.floor(iPlatina    * fPLFactor))
    AdvCombatEntity_SetProperty("Reward Doctor", -1)
    
    --Item rewards. These only apply if the entity was not mugged.
    if(TA_GetProperty("Was Mugged") == false) then
        local iRoll = LM_GetRandomNumber(1, 100)
        for i = 1, #zaItemList, 1 do
            if(iRoll >= zaItemList[i][1] and iRoll <= zaItemList[i][2]) then
                AdvCombatEntity_SetProperty("Reward Item", zaItemList[i][3])
            end
        end
    end
    
DL_PopActiveObject()
