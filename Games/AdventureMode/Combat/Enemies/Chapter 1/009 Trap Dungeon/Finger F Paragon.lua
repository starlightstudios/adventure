-- |[ ================================== Finger Female, Normal ================================= ]|
--Found in the Dimensional Trap Dungeon. Tougher than the basic cultists.
local sActualName = "Finger Paragon"
local iUniqueID = fnStandardEnemyHandler(sActualName)

-- |[Additional Setup]|
--If the entity needs more setup, like modifying AI or conditional cutscenes, do it here.
if(iUniqueID ~= 0) then
end
