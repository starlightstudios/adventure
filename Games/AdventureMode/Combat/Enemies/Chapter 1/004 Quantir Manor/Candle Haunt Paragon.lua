-- |[ ================================== Candle Haunt, Normal ================================== ]|
--Is the candle floating, or is it held by something unseen?
local sActualName = "Candle Haunt Paragon"
local iUniqueID = fnStandardEnemyHandler(sActualName)

-- |[Additional Setup]|
--If the entity needs more setup, like modifying AI or conditional cutscenes, do it here.
if(iUniqueID ~= 0) then
end
