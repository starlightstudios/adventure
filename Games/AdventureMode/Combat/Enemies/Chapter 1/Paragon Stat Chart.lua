-- |[ ================================ Paragon Statistics Chart ================================ ]|
--Chart and related function calls to build Paragon information. Nominally, paragons are "tough" 
-- versions of existing enemies. If the paragon is not explicitly defined in this chart, then a
-- set of bonuses is applied to the base enemy. This is obviously not very balanced.

--The function fnPara() is used in place of fnAdd(). This function just adds "Paragon" onto the
-- end of the enemy's name.

-- |[ ==================================== Paragon Listing ===================================== ]|
--     Actual Name       | Display Name      | Attack ||||| Lv | Health | Attack | Ini | Acc | Evd | Stun ||||| Pr | Sl | St | Pc | Fl | Fr | Sh | Cr | Ob | Bl | Ps | Cr | Tr ||||| Plt | Exp | JP | Drop                 | Chnc
fnPara("Initiate F",       "Initiate",         "Sword",     1,     195,     160,  120,   80,   75,   100,     0,   2,   2,   2,   2,   2,   2,   2,   2,  -3,   2,   2,   2,    105,  113,  25,  "Wallet x10",           10)
fnPara("Initiate M",       "Initiate",         "Sword",     1,     255,     165,  119,   85,   70,   100,     2,   3,   2,   2,   2,   2,   2,   2,   2,  -3,   2,   2,   2,    108,  113,  25,  "Tip Book x5",          10)
fnPara("Adept F",          "Adept",            "Sword",     3,     395,     210,  140,  100,  100,   100,     2,   2,   2,   2,   2,   2,   2,   2,   2,  -1,   2,   2,   2,    112,  116,  25,  "Adamantite Powder x1", 10)
fnPara("Adept M",          "Adept",            "Sword",     3,     620,     255,  139,  100,   90,   100,     2,   3,   2,   2,   2,   2,   2,   2,   2,  -3,   2,   2,   2,    113,  116,  25,  "Adamantite Powder x1", 10)
fnPara("Alraune Petal",    "Alraune Petal",    "Sword",     5,     500,     220,  160,  100,  120,   100,     2,  -1,   3,   3,  -4,   0,   6,   2,   2,   6,   5,   2,   4,    116,  126,  25,  "Palliative",            5)
fnPara("Bee Buddy",        "Bee Buddy",        "Claw",      3,     150,      90,  100,  125,  130,    50,     2,   2,   2,   3,   0,   0,   2,   2,   2,   2,   5,   4,  -1,    103,  109,  25,  "Null",                  0)
fnPara("Bee Scout",        "Bee Scout",        "Claw",      5,     530,     300,  145,  125,  130,   100,     2,   2,   2,   2,   0,   0,   2,   2,   2,   1,   5,   4,   6,    105,  119,  25,  "Rubose Gem",            5)
fnPara("Bubbly Slimegirl", "Bubbly Slimegirl", "Claw",      5,     735,     240,  120,  120,  110,   100,     2,   2,   6,   5,   2,  -3,   0,   2,   2,  10,   2,   2,   2,    120,  121,  25,  "Yemite Gem",            5)
fnPara("Chilly Slimegirl", "Chilly Slimegirl", "Null",      5,     750,     240,  120,  120,  110,   100,     2,   2,   6,   5,  -3,   7,   2,   2,   2,  10,   2,   2,   2,    125,  127,  25,  "Nockrion Gem",          5)
fnPara("Inky Slimegirl",   "Inky Slimegirl",   "Claw",      5,     800,     240,  120,  120,  110,   150,     2,   5,   6,   5,   2,  -3,   2,   2,   2,  10,  10,  10,   2,    125,  122,  25,  "Piorose Gem",           5)
fnPara("Jelli",            "Jelli",            "Null",      3,     250,     175,  140,  120,  110,    50,     2,   5,   6,   5,   2,  -3,   2,   2,   2,  10,  10,  10,   2,    107,  108,  25,  "Null",                  0)
fnPara("Sprout Pecker",    "Sprout Pecker",    "Claw",      5,     600,     220,  160,  100,  120,   100,     2,   1,   3,   3,  -3,   4,   6,   2,   2,   5,   5,   5,   0,    120,  117,  25,  "Mordreen Gem",          5)
fnPara("Sproutling",       "Sproutling",       "Claw",      3,     150,     175,  170,  130,  130,    50,     2,   1,   3,   3,  -3,   4,   6,   2,   2,   5,   5,   5,   0,    105,  112,  25,  "Null",                  0)
fnPara("Toxic Slimegirl",  "Toxic Slimegirl",  "Claw",      5,     675,     260,  120,  120,  110,   100,     2,   2,   6,   5,   2,  -3,   2,   2,   2,  10,  10,  10,   2,    122,  122,  25,  "Adamantite Powder x1", 10)
fnPara("Alraune Stem",     "Alraune Stem",     "Sword",     8,     800,     320,  170,  120,  145,   100,     2,   1,   3,   3,  -4,   0,   6,   2,   2,   6,   5,   2,   4,    141,  175,  25,  "Adamantite Powder x1", 20)
fnPara("Bee Gatherer",     "Bee Gatherer",     "Claw",      8,     875,     340,  165,  125,  140,   100,     2,   2,   2,   4,   0,   0,   2,   2,   2,   1,   5,   4,   6,    128,  164,  25,  "Phonophage Gem",        5)
fnPara("Bee Sniffer",      "Bee Sniffer",      "Claw",      8,     400,     160,  200,  165,  140,    50,     2,   2,   2,   3,   0,   0,   2,   2,   2,   2,   5,   4,  -1,    115,  115,  25,  "Null",                  0)
fnPara("Werecat Hunter",   "Werecat Hunter",   "Claw",      8,     450,     460,  200,  135,  170,   100,     2,   2,   2,   2,  -1,   4,   4,   2,   2,  -1,   2,   2,   0,    132,  167,  25,  "Tip Book x10",         10)
fnPara("Best Friend",      "Best Friend",      "Claw",     12,     725,     425,  140,  145,  135,    50,     2,   2,   0,   0,  -3,   5,   2,  -8,  -8,   2,   2,   2,  14,    109,  162,  25,  "Wallet x30",           10)
fnPara("Bloody Mirror",    "Bloody Mirror",    "Sword",     8,     275,     260,  170,  130,  130,    50,     2,   5,  -3,   7,   2,   2,   4,   2,   2,   10,  10, -3,  14,    108,  145,  25,  "Null",                  0)
fnPara("Candle Haunt",     "Candle Haunt",     "Null",      8,     350,     235,  110,  100,  100,    50,     2,   5,   2,   2,   2,  -3,   2,   2,   2,   10,  10, -3,  14,    108,  135,  25,  "Null",                  0)
fnPara("Eye Drawer",       "Eye Drawer",       "Null",     12,    1550,     400,  110,  130,  120,   140,     2,   5,   5,   5,  -8,   5,   7,   2,   2,   10,  10, 10,  14,    200,  140,  25,  "Wallet x50",           10)
fnPara("Ghost Maid",       "Ghost Maid",       "Sword",    12,     950,     395,  130,  130,  130,   100,     2,   5,   5,   5,  -3,   7,   2,   2,   2,   10,  10, 10,  14,    157,  196,  25,  "Adamantite Powder x1", 10)
fnPara("Mirror Image",     "Mirror Image",     "Claw",     12,    1275,     330,  150,  130,  110,   100,     2,   2,   2,  -3,   2,   2,  -3,  -8,  -8,    2,   2,  2,  14,    142,  187,  25,  "Adamantite Powder x1", 10)
fnPara("Skitterer",        "Skitterer",        "Claw",      8,     485,     350,  170,  130,  180,    50,     2,   2,  -8,   2,   2,   2,   0,  -8,  -8,    2,   2,  2,  14,    112,  138,  25,  "Null",                  0)
fnPara("Mushraune",        "Mushraune",        "Null",     12,    1110,     450,  120,  160,  150,   150,     2,   1,   3,   3,  -4,   0,   6,   2,   2,    6,   5,  5,   7,    117,  176,  25,  "Glintsteel Gem",        5)
fnPara("Werecat Prowler",  "Werecat Prowler",  "Claw",     12,     610,     600,  170,  160,  220,   100,     2,   2,   2,   2,  -1,   4,   4,   2,   2,   -1,   2,  2,   0,    170,  195,  25,  "Tip Book x10",         10)
fnPara("Rubberraune",      "Rubberraune",      "Sword",    13,    1500,     650,  130,  160,  155,   150,     2,   7,  -3,   7, -10,   8,   8,   2,   2,   12,  12, 12,  12,    178,  280,  25,  "Wallet x100",          10)
fnPara("Rubbercat",        "Rubbercat",        "Claw",     13,    1100,     600,  140,  160,  180,   150,     2,   7,  -3,   7, -10,  12,  12,   2,   2,   12,  12, 12,  12,    178,  280,  25,  "Nockrion Gem",         10)
fnPara("RubbercatThief",   "Rubbercat Thief",  "Claw",     13,    1050,     725,  150,  170,  210,   100,     2,   7,  -3,   3, -10,  12,  12,   2,   2,   12,  12, 12,  12,    178,  280,  25,  "Rubose Gem",           10)
fnPara("Rubberbee",        "Rubberbee",        "Claw",     13,    1850,     555,  125,  165,  140,   150,     2,   7,  -3,   7, -10,  12,  12,   2,   2,   12,  12, 12,  12,    178,  280,  25,  "Tip Book x10",         10)
fnPara("Alraune Keeper",   "Alraune Keeper",   "Sword",    14,    1150,     500,  160,  160,  175,   100,     2,   1,   3,   3,  -4,   0,   6,   2,   2,    6,   5,  2,   4,    149,  228,  25,  "Healing Tincture",     10)
fnPara("Sprout Thrasher",  "Sprout Thrasher",  "Claw",     14,    1050,     355,  160,  130,  150,   100,     2,   1,   3,   3,  -3,   4,   6,   2,   2,    5,   5,  5,   0,    120,  117,  25,  "Healing Tincture",     10)
fnPara("Bee Skirmisher",   "Bee Skirmisher",   "Claw",     14,    1400,     585,  155,  165,  140,   100,     2,   2,   2,   4,   0,   0,   2,   2,   2,    1,   5,  4,   6,    162,  235,  25,  "Smelling Salts",       10)
fnPara("Bee Tracker",      "Bee Tracker",      "Claw",     14,     760,     230,  200,  165,  195,    50,     2,   2,   2,   3,   0,   0,   2,   2,   2,    2,   5,  4,  -1,    135,  145,  25,  "Null",                  0)
fnPara("Werecat Thief",    "Werecat Thief",    "Claw",     14,     550,     725,  180,  170,  230,   100,     2,   2,   2,   2,  -1,   4,   4,   2,   2,   -1,   2,  2,   0,    320,  215,  25,  "Wallet x100",          20)
fnPara("Zombee",           "Zombee",           "Claw",     15,    2000,     650,  110,  170,  150,    70,     4,   2,   2,   4,  -3,  -3,   2, - 8,  -8,    0,  10,  0,2004,    132,  255,  25,  "Adamantite Flakes x1", 20)
fnPara("Finger",           "Finger",           "Sword",    15,    1750,     600,  150,  150,  150,   100,     2,   2,   2,   2,   2,   2,   2,   2,   2,   -1,   2,  2,   4,    165,  237,  25,  "Leather Greatcoat",    10)
fnPara("Palm",             "Palm",             "Sword",    15,    2750,     550,  150,  150,  150,   100,     2,   3,   2,   2,   2,   2,   2,   2,   2,   -3,   2,  2,   4,    170,  227,  25,  "Plated Leather Vest",  10)
fnPara("Gravemarker",      "Gravemarker",      "Sword",    15,    2600,     600,  150,  185,  150,   150,     2,   5,  -2,   5,   5,   5,   8,   7,  -3,   10,  10, -6,2004,    215,  360,  25,  "Null",                  0)
fnPara("Mushthrall",       "Mushthrall",       "Null",     15,    1850,     620,  130,  150,  120,   150,     2,   1,   3,   3,  -4,   0,   6,   2,   2,   6,   5,   5,   7,    178,  280,  25,  "Poison Spore Cap",     10)


-- |[ =============== Resistant/Vulnerable Effects =============== ]|
--Certain enemies are vulnerable to specific damage types in ways not represented by their resistances. These effects spawn at creation
-- and are visible in the combat inspector so the player can easily see the target is resistant/vulnerable.
fnTag("Initiate F Paragon",        { {"Spawn Effect|Enemy.Stock.BlindVulnerable|50|50|5", 1}, {"Spawn Effect|Enemy.Stock.BleedVulnerable|30|50|3", 1} })
fnTag("Initiate M Paragon",        { {"Spawn Effect|Enemy.Stock.BlindVulnerable|70|70|7", 1}, {"Spawn Effect|Enemy.Stock.BleedVulnerable|30|50|3", 1} })
fnTag("Adept F Paragon",           { {"Spawn Effect|Enemy.Stock.BlindVulnerable|50|50|5", 1}, {"Spawn Effect|Enemy.Stock.BleedVulnerable|30|50|3", 1} })
fnTag("Adept M Paragon",           { {"Spawn Effect|Enemy.Stock.BlindVulnerable|70|70|7", 1}, {"Spawn Effect|Enemy.Stock.BleedVulnerable|30|50|3", 1} })
fnTag("Bee Buddy Paragon",         { {"Spawn Effect|Enemy.Stock.BlindResistant|50|50|5",  1} })
fnTag("Bee Scout Paragon",         { {"Spawn Effect|Enemy.Stock.BlindResistant|50|50|5",  1} })
fnTag("Sprout Pecker Paragon",     { {"Spawn Effect|Enemy.Stock.BlindVulnerable|50|50|5", 1} })
fnTag("Bee Gatherer Paragon",      { {"Spawn Effect|Enemy.Stock.BlindResistant|50|50|5",  1} })
fnTag("Bee Sniffer Paragon",       { {"Spawn Effect|Enemy.Stock.BlindResistant|50|50|5",  1} })
fnTag("Bloody Mirror Paragon",     { {"Spawn Effect|Enemy.Stock.BlindResistant|50|50|5",  1} })
fnTag("Candle Haunt Paragon",      { {"Spawn Effect|Enemy.Stock.BlindResistant|50|50|5",  1} })
fnTag("Ghost Maid Paragon",        { {"Spawn Effect|Enemy.Stock.BlindResistant|50|50|5",  1} })
fnTag("Sprout Thrasher Paragon",   { {"Spawn Effect|Enemy.Stock.BlindVulnerable|50|50|5", 1} })
fnTag("Bee Skirmisher Paragon",    { {"Spawn Effect|Enemy.Stock.BlindResistant|50|50|5",  1} })
fnTag("Bee Tracker Paragon",       { {"Spawn Effect|Enemy.Stock.BlindResistant|50|50|5",  1} })
fnTag("Zombee Paragon",            { {"Spawn Effect|Enemy.Stock.BlindResistant|50|50|5",  1} })
fnTag("Finger Paragon",            { {"Spawn Effect|Enemy.Stock.BlindVulnerable|50|50|5", 1}, {"Spawn Effect|Enemy.Stock.BleedVulnerable|30|25|3", 1} })
fnTag("Palm Paragon",              { {"Spawn Effect|Enemy.Stock.BlindVulnerable|70|70|7", 1}, {"Spawn Effect|Enemy.Stock.BleedVulnerable|30|25|3", 1} })
fnTag("Gravemarker Paragon",       { {"Spawn Effect|Enemy.Stock.BlindResistant|50|50|5",  1} })

-- |[ ================== KO Stat/Resist Setting ================== ]|
--By default, the player needs a specific number of KOs, 5 for stats, 10 for resists, to see extra information on the combat inspector.
-- Some 'lesser' enemies have a lower count needed.
fnAppendEnemyTag("Bee Buddy Paragon",         { {"KO Tag Stats", 3}, {"KO Tag Resists", 5} })
fnAppendEnemyTag("Jelli Paragon",             { {"KO Tag Stats", 3}, {"KO Tag Resists", 5} })
fnAppendEnemyTag("Sprout Pecker Paragon",     { {"KO Tag Stats", 3}, {"KO Tag Resists", 5} })
fnAppendEnemyTag("Sproutling Paragon",        { {"KO Tag Stats", 3}, {"KO Tag Resists", 5} })
fnAppendEnemyTag("Bee Sniffer Paragon",       { {"KO Tag Stats", 3}, {"KO Tag Resists", 5} })
fnAppendEnemyTag("Bloody Mirror Paragon",     { {"KO Tag Stats", 3}, {"KO Tag Resists", 5} })
fnAppendEnemyTag("Candle Haunt Paragon",      { {"KO Tag Stats", 3}, {"KO Tag Resists", 5} })
fnAppendEnemyTag("Eye Drawer Paragon",        { {"KO Tag Stats", 3}, {"KO Tag Resists", 5} })
fnAppendEnemyTag("Sprout Thrasher Paragon",   { {"KO Tag Stats", 3}, {"KO Tag Resists", 5} })
fnAppendEnemyTag("Bee Tracker Paragon",       { {"KO Tag Stats", 3}, {"KO Tag Resists", 5} })
fnAppendEnemyTag("Gravemarker Paragon",       { {"KO Tag Stats", 3}, {"KO Tag Resists", 5} })

-- |[ ==================== Rubberable Enemies ==================== ]|
--All enemies on this list gain the "Is Rubberable" tag, meaning they can be afflicted during the rubber sequence.
local saRubberList = {"Alraune Petal", "Bee Scout", "Alraune Stem", "Bee Gatherer", "Werecat Hunter", "Werecat Prowler", "Alraune Keeper", "Bee Skirmisher", "Werecat Thief"}
for i = 1, #saRubberList, 1 do
    fnAppendEnemyTag(saRubberList[i] .. " Paragon", {{"Is Rubberable", 1}})
end

-- |[ ================= Un-Strangleable Enemies ================== ]|
--All enemies on this list gain the "Cannot Strangle" tag. The Stalker/Lurker ability "Strangle" cannot target them.
local saNoStrangleList = {"Bubbly Slimegirl", "Chilly Slimegirl", "Inky Slimegirl", "Jelli", "Toxic Slimegirl", "Best Friend", "Bloody Mirror", "Candle Haunt", "Eye Drawer", "Mirror Image", "Gravemarker", 
                          "Rubberraune", "Rubbercat", "RubbercatThief", "Rubberbee"}
for i = 1, #saNoStrangleList, 1 do
    fnAppendEnemyTag(saNoStrangleList[i] .. " Paragon", {{"Cannot Strangle", 1}})
end

-- |[ ========================= Abilities ======================== ]|
-- |[Generic Abilities]|
--Strike.
fnAbility("Werecat Thief Paragon", {"Enemy|Strike Attack", 100})
fnAbility("Gravemarker Paragon",   {"Enemy|Strike Attack", 100})
fnAbility("Eye Drawer Paragon",    {"Enemy|Strike Attack", 100})

--Pierce.
fnAbility("Alraune Petal Paragon",  {"Enemy|Pierce Attack", 100})
fnAbility("Alraune Stem Paragon",   {"Enemy|Pierce Attack", 100})
fnAbility("Alraune Keeper Paragon", {"Enemy|Pierce Attack", 100})
fnAbility("Mushraune Paragon",      {"Enemy|Pierce Attack", 100})
fnAbility("Mushthrall Paragon",     {"Enemy|Pierce Attack", 100})

--Ice.
fnAbility("Ghost Maid Paragon",       {"Enemy|Freeze Attack", 100})
fnAbility("Chilly Slimegirl Paragon", {"Enemy|Freeze Attack", 100})

--Fire.
fnAbility("Candle Haunt Paragon", {"Enemy|Flame Attack", 100})

--Poison.
fnAbility("Toxic Slimegirl Paragon",  {"Enemy|Poison DoT", 100, "Enemy|Poison Attack", 100})
fnAbility("Jelli Paragon",            {"Enemy|Poison DoT", 100})

--Bleed.
fnAbility("Werecat Hunter Paragon",  {"Enemy|Bleed DoT", 100})
fnAbility("Werecat Prowler Paragon", {"Enemy|Bleed DoT", 300})

-- |[Chapter Specific Abilities]|
--Bee Abilities.
fnAbility("Bee Scout Paragon",      {"Enemy|Sugar Rush", 33})
fnAbility("Bee Gatherer Paragon",   {"Enemy|Sugar Rush", 33})
fnAbility("Bee Skirmisher Paragon", {"Enemy|Sugar Rush", 33})

--Sproutpecker.
fnAbility("Sprout Pecker Paragon",   {"Enemy|Peck", 0})
fnAbility("Sprout Thrasher Paragon", {"Enemy|Peck", 100})

--Powerful Cultists.
fnAbility("Finger Paragon", {"Enemy|Wild Swing", 33})
fnAbility("Palm Paragon",   {"Enemy|Frenzy",     33})

-- |[Initiate and Adept Paragon]|
fnAbility("Initiate F Paragon", {"Enemy|Initiate F SA", 0})
fnAbility("Initiate M Paragon", {"Enemy|Initiate M SA", 0})
fnAbility("Adept F Paragon",    {"Enemy|Adept F SA", 0})
fnAbility("Adept M Paragon",    {"Enemy|Adept M SA", 0})

-- |[Chilly Slime Paragon]|
fnAbility("Chilly Slimegirl Paragon", {"Enemy|Ice Ray", 80})

-- |[Bubbly Slime Paragon]|
fnAbility("Bubbly Slimegirl Paragon", {"Enemy|Slime Heart Blast",       100})
fnAbility("Bubbly Slimegirl Paragon", {"Enemy|Magical Cutie Transform", 100})
fnAbility("Bubbly Slimegirl Paragon", {"Enemy|Lovely Slime Lightning",  100})
fnAbility("Bubbly Slimegirl Paragon", {"Enemy|Vivimancys Flail",        100})

-- |[Zombee Paragon]|
fnAbility("Zombee Paragon", {"Enemy|Rending Claws", 100})

-- |[Sprout Pecker Paragon]|
fnAbility("Sprout Pecker Paragon",   {"Enemy|Seed Spitter", 100})
fnAbility("Sprout Thrasher Paragon", {"Enemy|Seed Spitter",  50})

-- |[Sprout Thrasher Paragon]|
fnAbility("Sprout Thrasher Paragon", {"Enemy|Pea Shooter", 100})

-- |[Sproutling Paragon]|
fnAbility("Sproutling Paragon", {"Enemy|Wither", 0})
fnAbility("Sproutling Paragon", {"Enemy|Bloom",  0})

-- |[Alrune Petal Paragon]|
fnAbility("Alraune Petal Paragon", {"Enemy|Roses Poison",  0})
fnAbility("Alraune Petal Paragon", {"Enemy|Roses Riposte", 0})

-- |[Alrune Stem Paragon]|
fnAbility("Alraune Stem Paragon", {"Enemy|Pollen Rend", 0})

-- |[Alrune Keeper Paragon]|
fnAbility("Alraune Keeper Paragon", {"Enemy|Monster of the Forest", 0})
fnAbility("Alraune Keeper Paragon", {"Enemy|Roses Execution",       0})

-- |[Mushraune and Mushthrall Paragon]|
fnAbility("Mushraune Paragon",  {"Enemy|Corruptive Cloud",    0})
fnAbility("Mushthrall Paragon", {"Enemy|Corruptive Cloud",    0})
fnAbility("Mushraune Paragon",  {"Enemy|The Struggle Within", 0})
fnAbility("Mushthrall Paragon", {"Enemy|The Struggle Within", 0})

-- |[Toxic Slimegirl Paragon]|
fnAbility("Toxic Slimegirl Paragon", {"Enemy|Melty Hug",     0})
fnAbility("Toxic Slimegirl Paragon", {"Enemy|Slime Rend",    0})
fnAbility("Toxic Slimegirl Paragon", {"Enemy|Slime Balloon", 0})

-- |[Bee Family Paragons]|
fnAbility("Bee Scout Paragon",      {"Enemy|Bee Sting",         0})
fnAbility("Bee Gatherer Paragon",   {"Enemy|Bee Sting",         0})
fnAbility("Bee Sniffer Paragon",    {"Enemy|Bee Sting",         0})
fnAbility("Bee Skirmisher Paragon", {"Enemy|Bee Sting",         0})
fnAbility("Bee Gatherer Paragon",   {"Enemy|Angry Honey",       0})
fnAbility("Bee Skirmisher Paragon", {"Enemy|Angry Honey",       0})
fnAbility("Bee Skirmisher Paragon", {"Enemy|Counterattack Bee", 0})

-- |[Rubber Paragons]|
fnAbility("Rubberraune Paragon", {"Enemy|Blow Up Sword", 80})
fnAbility("Rubberraune Paragon", {"Enemy|Fumble Sword",  20})

fnAbility("Rubbercat Paragon",       {"Enemy|Blow Up Claw", 75})
fnAbility("RubbercatThief Paragon",  {"Enemy|Blow Up Claw", 75})
fnAbility("Rubberbee Paragon",       {"Enemy|Blow Up Claw", 75})
fnAbility("Rubbercat Paragon",       {"Enemy|Fumble Claw",  25})
fnAbility("RubbercatThief Paragon",  {"Enemy|Fumble Claw",  25})
fnAbility("Rubberbee Paragon",       {"Enemy|Fumble Claw",  25})

fnAbility("Rubbercat Paragon",      {"Enemy|Counterattack Rubber", 0})
fnAbility("RubbercatThief Paragon", {"Enemy|Counterattack Rubber", 0})
fnAbility("Rubberbee Paragon",      {"Enemy|Counterattack Rubber", 0})
fnAbility("Rubberraune Paragon",    {"Enemy|Counterattack Rubber", 0})

-- |[Ghost Maid Paragon]|
fnAbility("Ghost Maid Paragon", {"Enemy|Withering Touch", 100})

-- |[Skitterer Paragon]|
fnAbility("Skitterer Paragon", {"Enemy|Widening Gyre", 0})

-- |[Inky Slimegirl Paragon]|
fnAbility("Inky Slimegirl Paragon", {"Enemy|Armor Steal", 0})

-- |[Jelli Paragon]|
fnAbility("Jelli Paragon", {"Enemy|Rend",      0})
fnAbility("Jelli Paragon", {"Enemy|Vine Lash", 0})
fnAbility("Jelli Paragon", {"Enemy|Bodyslam",  0})
fnAbility("Jelli Paragon", {"Enemy|Swipe",     0})
fnAbility("Jelli Paragon", {"Enemy|Crush",     0})
fnAbility("Jelli Paragon", {"Enemy|Sting",     0})
fnAbility("Jelli Paragon", {"Enemy|Icy Hand",  0})
fnAbility("Jelli Paragon", {"Enemy|Goopshot",  0})

-- |[Bee Buddy Family Paragons]|
fnAbility("Bee Buddy Paragon",   {"Enemy|Cute Look",      0})
fnAbility("Bee Sniffer Paragon", {"Enemy|Cute Look",      0})
fnAbility("Bee Tracker Paragon", {"Enemy|Cute Look",      0})
fnAbility("Bee Buddy Paragon",   {"Enemy|Bee Buddy SA",   0})
fnAbility("Bee Sniffer Paragon", {"Enemy|Bee Sniffer SA", 0})
fnAbility("Bee Tracker Paragon", {"Enemy|Bee Tracker SA", 0})
fnAbility("Bee Tracker Paragon", {"Enemy|Fatal Sting",    0})

-- |[Candle Haunt Paragon]|
fnAbility("Candle Haunt Paragon", {"Enemy|Fireball",        0})
fnAbility("Candle Haunt Paragon", {"Enemy|Candle Haunt SA", 0})

-- |[Eye Drawer Paragon]|
fnAbility("Eye Drawer Paragon", {"Enemy|Be Our Guest", 0})
fnAbility("Eye Drawer Paragon", {"Enemy|Eye Drawer SA", 0})

-- |[Werecat Paragon Family]|
fnAbility("Werecat Hunter Paragon",  {"Enemy|Counterattack Base",    0})
fnAbility("Werecat Prowler Paragon", {"Enemy|Counterattack Prowler", 0})

-- |[Bloody Mirror Paragon]|
fnAbility("Bloody Mirror Paragon", {"Enemy|Counterattack Mirror", 0})
fnAbility("Bloody Mirror Paragon", {"Enemy|Candle Haunt SA",      0})

-- |[Palm and Finger Paragons]|
fnAbility("Palm Paragon",   {"Enemy|Bleeding Hand M",     0})
fnAbility("Palm Paragon",   {"Enemy|Protective Grip M",   0})
fnAbility("Finger Paragon", {"Enemy|Bleeding Hand F",     0})
fnAbility("Finger Paragon", {"Enemy|Protective Grip F",   0})
fnAbility("Finger Paragon", {"Enemy|Wild Swing Modified", 0})

-- |[Werecat Thief Paragon]|
fnAbility("Werecat Thief Paragon", {"Enemy|Cat Nap",            20})
fnAbility("Werecat Thief Paragon", {"Enemy|Cats Claw",          80})
fnAbility("Werecat Thief Paragon", {"Enemy|Counterattack Thief", 0})

-- |[Gravemarker Paragon]|
fnAbility("Gravemarker Paragon", {"Enemy|Groaning Slab", 0})
fnAbility("Gravemarker Paragon", {"Enemy|Weeping Stone", 0})
fnAbility("Gravemarker Paragon", {"Enemy|Trumpet Sounds", 1000})

-- |[Seal Of Approval]|
fnAbility("Initiate F Paragon",       {"Enemy|Seal Of Approval", 0})
fnAbility("Initiate M Paragon",       {"Enemy|Seal Of Approval", 0})
fnAbility("Adept F Paragon",          {"Enemy|Seal Of Approval", 0})
fnAbility("Adept M Paragon",          {"Enemy|Seal Of Approval", 0})
fnAbility("Alraune Petal Paragon",    {"Enemy|Seal Of Approval", 0})
fnAbility("Bee Buddy Paragon",        {"Enemy|Seal Of Approval", 0})
fnAbility("Bee Scout Paragon",        {"Enemy|Seal Of Approval", 0})
fnAbility("Bubbly Slimegirl Paragon", {"Enemy|Seal Of Approval", 0})
fnAbility("Chilly Slimegirl Paragon", {"Enemy|Seal Of Approval", 0})
fnAbility("Inky Slimegirl Paragon",   {"Enemy|Seal Of Approval", 0})
fnAbility("Jelli Paragon",            {"Enemy|Seal Of Approval", 0})
fnAbility("Sprout Pecker Paragon",    {"Enemy|Seal Of Approval", 0})
fnAbility("Sproutling Paragon",       {"Enemy|Seal Of Approval", 0})
fnAbility("Toxic Slimegirl Paragon",  {"Enemy|Seal Of Approval", 0})
fnAbility("Alraune Stem Paragon",     {"Enemy|Seal Of Approval", 0})
fnAbility("Bee Gatherer Paragon",     {"Enemy|Seal Of Approval", 0})
fnAbility("Bee Sniffer Paragon",      {"Enemy|Seal Of Approval", 0})
fnAbility("Werecat Hunter Paragon",   {"Enemy|Seal Of Approval", 0})
fnAbility("Best Friend Paragon",      {"Enemy|Seal Of Approval", 0})
fnAbility("Bloody Mirror Paragon",    {"Enemy|Seal Of Approval", 0})
fnAbility("Candle Haunt Paragon",     {"Enemy|Seal Of Approval", 0})
fnAbility("Eye Drawer Paragon",       {"Enemy|Seal Of Approval", 0})
fnAbility("Ghost Maid Paragon",       {"Enemy|Seal Of Approval", 0})
fnAbility("Mirror Image Paragon",     {"Enemy|Seal Of Approval", 0})
fnAbility("Skitterer Paragon",        {"Enemy|Seal Of Approval", 0})
fnAbility("Mushraune Paragon",        {"Enemy|Seal Of Approval", 0})
fnAbility("Werecat Prowler Paragon",  {"Enemy|Seal Of Approval", 0})
fnAbility("Alraune Keeper Paragon",   {"Enemy|Seal Of Approval", 0})
fnAbility("Sprout Thrasher Paragon",  {"Enemy|Seal Of Approval", 0})
fnAbility("Bee Skirmisher Paragon",   {"Enemy|Seal Of Approval", 0})
fnAbility("Bee Tracker Paragon",      {"Enemy|Seal Of Approval", 0})
fnAbility("Werecat Thief Paragon",    {"Enemy|Seal Of Approval", 0})
fnAbility("Zombee Paragon",           {"Enemy|Seal Of Approval", 0})
fnAbility("Finger Paragon",           {"Enemy|Seal Of Approval", 0})
fnAbility("Palm Paragon",             {"Enemy|Seal Of Approval", 0})
fnAbility("Gravemarker Paragon",      {"Enemy|Badge Of Approval",0})
fnAbility("Mushthrall Paragon",       {"Enemy|Seal Of Approval", 0})
fnAbility("Rubberraune Paragon",      {"Enemy|Seal Of Approval", 0})
fnAbility("Rubbercat Paragon",        {"Enemy|Seal Of Approval", 0})
fnAbility("RubbercatThief Paragon",   {"Enemy|Seal Of Approval", 0})
fnAbility("Rubberbee Paragon",        {"Enemy|Seal Of Approval", 0})

-- |[ ======================================= AI Setting ======================================= ]|
local sChapter1ParagonAIPath = gsRoot .. "Combat/AIs/Chapter 1/"
fnAI("Sproutling Paragon",       sChapter1ParagonAIPath .. "Sproutling Paragon AI.lua")
fnAI("Alraune Petal Paragon",    sChapter1ParagonAIPath .. "Alraune Petal Paragon AI.lua")
fnAI("Alraune Stem Paragon",     sChapter1ParagonAIPath .. "Alraune Stem Paragon AI.lua")
fnAI("Alraune Keeper Paragon",   sChapter1ParagonAIPath .. "Alraune Keeper Paragon AI.lua")
fnAI("Mushraune Paragon",        sChapter1ParagonAIPath .. "Mushraune Paragon AI.lua")
fnAI("Toxic Slimegirl Paragon",  sChapter1ParagonAIPath .. "Toxic Slimegirl Paragon AI.lua")
fnAI("Bee Scout Paragon",        sChapter1ParagonAIPath .. "Bee Scout Paragon AI.lua")
fnAI("Bee Gatherer Paragon",     sChapter1ParagonAIPath .. "Bee Gatherer Paragon AI.lua")
fnAI("Bee Skirmisher Paragon",   sChapter1ParagonAIPath .. "Bee Skirmisher Paragon AI.lua")
fnAI("Mushthrall Paragon",       sChapter1ParagonAIPath .. "Mushthrall Paragon AI.lua")
fnAI("Skitterer Paragon",        sChapter1ParagonAIPath .. "Skitterer Paragon AI.lua")
fnAI("Inky Slimegirl Paragon",   sChapter1ParagonAIPath .. "Inky Slimegirl Paragon AI.lua")
fnAI("Jelli Paragon",            sChapter1ParagonAIPath .. "Jelli Paragon AI.lua")
fnAI("Initiate F Paragon",       sChapter1ParagonAIPath .. "Initiate F Paragon AI.lua")
fnAI("Initiate M Paragon",       sChapter1ParagonAIPath .. "Initiate M Paragon AI.lua")
fnAI("Adept F Paragon",          sChapter1ParagonAIPath .. "Adept F Paragon AI.lua")
fnAI("Adept M Paragon",          sChapter1ParagonAIPath .. "Adept M Paragon AI.lua")
fnAI("Bee Buddy Paragon",        sChapter1ParagonAIPath .. "Bee Buddy Paragon AI.lua")
fnAI("Bee Sniffer Paragon",      sChapter1ParagonAIPath .. "Bee Sniffer Paragon AI.lua")
fnAI("Bee Tracker Paragon",      sChapter1ParagonAIPath .. "Bee Tracker Paragon AI.lua")
fnAI("Candle Haunt Paragon",     sChapter1ParagonAIPath .. "Candle Haunt Paragon AI.lua")
fnAI("Bloody Mirror Paragon",    sChapter1ParagonAIPath .. "Bloody Mirror Paragon AI.lua")
fnAI("Eye Drawer Paragon",       sChapter1ParagonAIPath .. "Eye Drawer Paragon AI.lua")
fnAI("Palm Paragon",             sChapter1ParagonAIPath .. "Palm Paragon AI.lua")
fnAI("Finger Paragon",           sChapter1ParagonAIPath .. "Finger Paragon AI.lua")
fnAI("Gravemarker Paragon",      sChapter1ParagonAIPath .. "Gravemarker Paragon AI.lua")
fnAI("Bubbly Slimegirl Paragon", sChapter1ParagonAIPath .. "Bubbly Slimegirl Paragon AI.lua")

-- |[ ================================= Portrait Offset Lookup ================================= ]|
--Nominally, paragons should use the same portraits as their base versions, except of a different
-- color palette. To simplify this, the heading "Paragons" is used instead of "Combat".
--However, paragons also store their turn-order portraits by associating them with this lookup.
-- Therefore, the function fnSetRemapParagon() is used, which will remap the paragon lookups
-- as required.

-- |[Mapping]|
fnSetRemapParagon("Initiate F Paragon",        "Root/Images/Portraits/Combat/CultistF")
fnSetRemapParagon("Initiate M Paragon",        "Root/Images/Portraits/Combat/CultistM")
fnSetRemapParagon("Adept F Paragon",           "Root/Images/Portraits/Combat/CultistF")
fnSetRemapParagon("Adept M Paragon",           "Root/Images/Portraits/Combat/CultistM")
fnSetRemapParagon("Alraune Petal Paragon",     "Root/Images/Portraits/Combat/Alraune")
fnSetRemapParagon("Bee Buddy Paragon",         "Root/Images/Portraits/Combat/BeeBuddy")
fnSetRemapParagon("Bee Scout Paragon",         "Root/Images/Portraits/Combat/BeeGirl")
fnSetRemapParagon("Bubbly Slimegirl Paragon",  "Root/Images/Portraits/Combat/Slime")
fnSetRemapParagon("Chilly Slimegirl Paragon",  "Root/Images/Portraits/Combat/SlimeB")
fnSetRemapParagon("Inky Slimegirl Paragon",    "Root/Images/Portraits/Combat/Ink_Slime")
fnSetRemapParagon("Jelli Paragon",             "Root/Images/Portraits/Combat/Jelli")
fnSetRemapParagon("Sprout Pecker Paragon",     "Root/Images/Portraits/Combat/SproutPecker")
fnSetRemapParagon("Sproutling Paragon",        "Root/Images/Portraits/Combat/Sproutling")
fnSetRemapParagon("Toxic Slimegirl Paragon",   "Root/Images/Portraits/Combat/SlimeG")
fnSetRemapParagon("Alraune Stem Paragon",      "Root/Images/Portraits/Combat/Alraune")
fnSetRemapParagon("Bee Gatherer Paragon",      "Root/Images/Portraits/Combat/BeeGirl")
fnSetRemapParagon("Bee Sniffer Paragon",       "Root/Images/Portraits/Combat/BeeBuddy")
fnSetRemapParagon("Werecat Hunter Paragon",    "Root/Images/Portraits/Combat/Werecat")
fnSetRemapParagon("Best Friend Paragon",       "Root/Images/Portraits/Combat/BestFriend")
fnSetRemapParagon("Bloody Mirror Paragon",     "Root/Images/Portraits/Combat/BloodyMirror")
fnSetRemapParagon("Candle Haunt Paragon",      "Root/Images/Portraits/Combat/CandleHaunt")
fnSetRemapParagon("Eye Drawer Paragon",        "Root/Images/Portraits/Combat/EyeDrawer")
fnSetRemapParagon("Ghost Maid Paragon",        "Root/Images/Portraits/Combat/MaidGhost")
fnSetRemapParagon("Mirror Image Paragon",      "Root/Images/Portraits/Combat/MirrorImage")
fnSetRemapParagon("Skitterer Paragon",         "Root/Images/Portraits/Combat/SkullCrawler")
fnSetRemapParagon("Mushraune Paragon",         "Root/Images/Portraits/Combat/Mushraune")
fnSetRemapParagon("Werecat Prowler Paragon",   "Root/Images/Portraits/Combat/Werecat")
fnSetRemapParagon("Bandit Scrub Paragon",      "Root/Images/Portraits/Combat/BanditF")
fnSetRemapParagon("Bandit Goon Paragon",       "Root/Images/Portraits/Combat/BanditM")
fnSetRemapParagon("Demonic Mercenary Paragon", "Root/Images/Portraits/Combat/DemonMerc")
fnSetRemapParagon("Bandit Boss Paragon",       "Root/Images/Portraits/Combat/BanditCatBlack")
fnSetRemapParagon("Mannequin Scrub Paragon",   "Root/Images/Portraits/Combat/MannBanditF")
fnSetRemapParagon("Mannequin Goon Paragon",    "Root/Images/Portraits/Combat/MannBanditM")
fnSetRemapParagon("Alraune Keeper Paragon",    "Root/Images/Portraits/Combat/Alraune")
fnSetRemapParagon("Sprout Thrasher Paragon",   "Root/Images/Portraits/Combat/SproutPecker")
fnSetRemapParagon("Bee Skirmisher Paragon",    "Root/Images/Portraits/Combat/BeeGirl")
fnSetRemapParagon("Bee Tracker Paragon",       "Root/Images/Portraits/Combat/BeeBuddy")
fnSetRemapParagon("Werecat Thief Paragon",     "Root/Images/Portraits/Combat/Werecat_Burglar")
fnSetRemapParagon("Zombee Paragon",            "Root/Images/Portraits/Combat/Zombee")
fnSetRemapParagon("Finger Paragon",            "Root/Images/Portraits/Combat/CultistF")
fnSetRemapParagon("Palm Paragon",              "Root/Images/Portraits/Combat/CultistM")
fnSetRemapParagon("Gravemarker Paragon",       "Root/Images/Portraits/Combat/Gravemarker")
fnSetRemapParagon("Mushthrall Paragon",        "Root/Images/Portraits/Combat/Mushraune")
fnSetRemapParagon("Rubberraune Paragon",       "Root/Images/Portraits/Combat/AlrauneRubber")
fnSetRemapParagon("Rubbercat Paragon",         "Root/Images/Portraits/Combat/WerecatRubber")
fnSetRemapParagon("RubbercatThief Paragon",    "Root/Images/Portraits/Combat/WerecatBurglarRubber")
fnSetRemapParagon("Rubberbee Paragon",         "Root/Images/Portraits/Combat/BeeGirlRubber")
