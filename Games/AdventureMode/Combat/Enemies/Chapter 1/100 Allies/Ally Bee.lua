-- |[ ======================================== Ally Bee ======================================== ]|
--Allied bee that spawns with 'Call For Help' ability.

-- |[ =================================== Common Statistics ==================================== ]|
--Setup common values regardless of functionality required from this file.
local iPlatina = 0
local iExperience = 0
local iJobPoints = 0

--Item Listing.
local zaItemList = {}

-- |[ ====================================== Mug Handling ====================================== ]|
--If being mugged, provide a percentage of the base values immediately.
if(TA_GetProperty("Is Mugging Check") == true) then
    return
end

-- |[ ==================================== Combat Encounter ==================================== ]|
--Add this entity to the combat roster.
    
-- |[Setup]|
local iEnemyID = AdvCombat_GetProperty("Generate Unique ID")
local sEnemyUniqueName = string.format("Autogen|%02i", iEnemyID)
AdvCombat_SetProperty("Register Enemy", sEnemyUniqueName, 0)

    -- |[System]|
    --ID
    giLastEnemyID = RO_GetID()
    
    --Name
    AdvCombatEntity_SetProperty("Display Name", "Bee Buddy")
    
    --AI
    AdvCombatEntity_SetProperty("AI Script", gsRoot .. "Combat/AIs/010 Buddy AI.lua")
        
    --Never show abilities on the inspector.
    AdvCombatEntity_SetProperty("Inspector Show Abilities", 1)

    -- |[Display]|
    --Images
    AdvCombatEntity_SetProperty("Combat Portrait", "Root/Images/Portraits/Combat/BeeGirl")
    AdvCombatEntity_SetProperty("Turn Icon", "Root/Images/AdventureUI/TurnPortraits/BeeAlly")
    
    --UI Positions
    AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,      142,  89)
    AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,      -28, -57)
    AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 320,  78)
    AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Enemy,       0,  63)

    -- |[Stats and Resistances]|
    --Compute scaling bonuses off Mei's level
    AdvCombat_SetProperty("Push Party Member", "Mei")
        local iLevel = AdvCombatEntity_GetProperty("Level")
    DL_PopActiveObject()
    local iHPScaler =  6.0
    local iAtkScaler = 4.0
    local iIniScaler = 1.0
    local iAccScaler = 3.0
    local iEvdScaler = 3.0
    
    --Catalyst Bonuses
    local iHpCatalysts  = AdInv_GetProperty("Catalyst Count", gciCatalyst_Health)
    local iAtkCatalysts = AdInv_GetProperty("Catalyst Count", gciCatalyst_Attack)
    local iAccCatalysts = AdInv_GetProperty("Catalyst Count", gciCatalyst_Accuracy)
    local iEvdCatalysts = AdInv_GetProperty("Catalyst Count", gciCatalyst_Evade)
    local iIniCatalysts = AdInv_GetProperty("Catalyst Count", gciCatalyst_Initiative)
    local iHPBonus  = (math.floor(iHpCatalysts  / gciCatalyst_Health_Needed)     * gciCatalyst_Health_Bonus)
    local iAtkBonus = (math.floor(iAtkCatalysts / gciCatalyst_Attack_Needed)     * gciCatalyst_Attack_Bonus)
    local iAccBonus = (math.floor(iAccCatalysts / gciCatalyst_Accuracy_Needed)   * gciCatalyst_Accuracy_Bonus)
    local iEvdBonus = (math.floor(iEvdCatalysts / gciCatalyst_Evade_Needed)      * gciCatalyst_Evade_Bonus)
    local iIniBonus = (math.floor(iIniCatalysts / gciCatalyst_Initiative_Needed) * gciCatalyst_Initiative_Bonus)
    
    --Statistics
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_HPMax,     175 + math.floor(iHPScaler * iLevel) + iHPBonus)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_MPMax,     100)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_CPMax,       0)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Attack,     68 + math.floor(iAtkScaler * iLevel) + iAtkBonus)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Initiative, 65 + math.floor(iIniScaler * iLevel) + iIniBonus)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Accuracy,   45 + math.floor(iAccScaler * iLevel) + iAccBonus)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Evade,      30 + math.floor(iEvdScaler * iLevel) + iEvdBonus)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_StunCap,   100)
    
    --Resistances
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Protection,     0)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Slash,   gciNormalResist+0)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Strike,  gciNormalResist+0)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Pierce,  gciNormalResist+2)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Flame,   gciNormalResist-2)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Freeze,  gciNormalResist-2)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Shock,   gciNormalResist+0)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Crusade, gciNormalResist+0)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Obscure, gciNormalResist+0)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Bleed,   gciNormalResist-1)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Poison,  gciNormalResist+3)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Corrode, gciNormalResist+2)
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Terrify, gciNormalResist+2)

    --Sum.
    AdvCombatEntity_SetProperty("Recompute Stats")
    AdvCombatEntity_SetProperty("Health Percent", 1.00)
    
    -- |[Tags]|
    --Vulnerable to bleeds and blinds.
    AdvCombatEntity_SetProperty("Add Tag", "Blind Effect -", 3)
    AdvCombatEntity_SetProperty("Add Tag", "Blind Apply -", 5)

    -- |[Abilities]|
    --System Abilities
    fnEnemyStandardSystemAbilities()

    --Register abilities.
    local sJobAbilityPath = gsRoot .. "Combat/Enemies/Abilities/Chapter 0 Allies/"
    LM_ExecuteScript(sJobAbilityPath .. "Bee Buddy Attack.lua", gciAbility_Create)
    
    --Set ability slots.
    AdvCombatEntity_SetProperty("Set Ability Slot", 0, 0, "Ally|Bee Buddy Attack")
    
    -- |[Rewards Handling]|
    --Factor for mugging.
    local fXPFactor = 1.0
    local fJPFactor = 1.0
    local fPLFactor = 1.0
    if(TA_GetProperty("Was Mugged") == true) then
        fXPFactor = 1.0 - gcfMugExperienceRate
        fJPFactor = 1.0 - gcfMugPlatinaRate
        fPLFactor = 1.0 - gcfMugJPRate
    end
    
    --Rewards
    AdvCombatEntity_SetProperty("Reward XP",      math.floor(iExperience * fXPFactor))
    AdvCombatEntity_SetProperty("Reward JP",      math.floor(iJobPoints  * fJPFactor))
    AdvCombatEntity_SetProperty("Reward Platina", math.floor(iPlatina    * fPLFactor))
    AdvCombatEntity_SetProperty("Reward Doctor", 0)
    
    --Item rewards. These only apply if the entity was not mugged.
    if(TA_GetProperty("Was Mugged") == false) then
        local iRoll = LM_GetRandomNumber(1, 100)
        for i = 1, #zaItemList, 1 do
            if(iRoll >= zaItemList[i][1] and iRoll <= zaItemList[i][2]) then
                AdvCombatEntity_SetProperty("Reward Item", zaItemList[i][3])
            end
        end
    end
    
DL_PopActiveObject()
