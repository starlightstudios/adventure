-- |[ ================================ Chapter 1 Enemy Aliases ================================= ]|
--Called at chapter boot. Creates a set of aliases for enemy names.
local sBasePath = fnResolvePath()
AdvCombat_SetProperty("Clear Enemy Aliases")

--Run the stat chart algorithm. This populates gczaChapter1Stats with all enemy properties.
-- If you need more specialized setup, that can be done in the enemy script.
LM_ExecuteScript(sBasePath .. "Enemy Stat Chart.lua")

-- |[ =================================== Creation Function ==================================== ]|
local function fnCreateAlias(psSLFName, psAliasName, bHasParagon, psPath)
    AdvCombat_SetProperty("Create Enemy Path", psAliasName, psPath .. ".lua")
    AdvCombat_SetProperty("Create Enemy Alias", psSLFName, psAliasName)
    
    if(bHasParagon) then
        AdvCombat_SetProperty("Create Enemy Path", psAliasName .. " Paragon", psPath .. " Paragon.lua")
        AdvCombat_SetProperty("Create Enemy Alias", psSLFName .. " Paragon", psAliasName .. " Paragon")
    end
end

-- |[ ====================================== Alias Listing ===================================== ]|
-- |[Trap Basement]|
fnCreateAlias("CultistF", "Cultist Initiate F", true, sBasePath .. "001 Trap Basement/Initiate F")
fnCreateAlias("CultistM", "Cultist Initiate M", true, sBasePath .. "001 Trap Basement/Initiate M")
fnCreateAlias("AdeptF",   "Cultist Adept F",    true, sBasePath .. "001 Trap Basement/Adept F")
fnCreateAlias("AdeptM",   "Cultist Adept M",    true, sBasePath .. "001 Trap Basement/Adept M")

-- |[Evermoon South]|
fnCreateAlias("Alraune",      "Alraune Petal",    true, sBasePath .. "002 Evermoon South/Alraune Petal")
fnCreateAlias("BeeBuddy",     "Bee Buddy",        true, sBasePath .. "002 Evermoon South/Bee Buddy")
fnCreateAlias("Bee",          "Bee Scout",        true, sBasePath .. "002 Evermoon South/Bee Scout")
fnCreateAlias("Jelli",        "Jelli",            true, sBasePath .. "002 Evermoon South/Jelli")
fnCreateAlias("InkSlime",     "InkSlime",         true, sBasePath .. "002 Evermoon South/Inky Slimegirl")
fnCreateAlias("Slime",        "Bubbly Slimegirl", true, sBasePath .. "002 Evermoon South/Bubbly Slimegirl")
fnCreateAlias("SlimeB",       "Chilly Slimegirl", true, sBasePath .. "002 Evermoon South/Chilly Slimegirl")
fnCreateAlias("Sproutpecker", "Sproutpecker",     true, sBasePath .. "002 Evermoon South/Sprout Pecker")
fnCreateAlias("Sproutling",   "Sproutling",       true, sBasePath .. "002 Evermoon South/Sproutling")
fnCreateAlias("SlimeG",       "Toxic Slimegirl",  true, sBasePath .. "002 Evermoon South/Toxic Slimegirl")

-- |[Evermoon North]|
fnCreateAlias("AlrauneStem", "Alraune Stem",   true, sBasePath .. "003 Evermoon North/Alraune Stem")
fnCreateAlias("BeeGatherer", "Bee Gatherer",   true, sBasePath .. "003 Evermoon North/Bee Gatherer")
fnCreateAlias("BeeSniffer",  "Bee Sniffer",    true, sBasePath .. "003 Evermoon North/Bee Sniffer")
fnCreateAlias("Werecat",     "Werecat Hunter", true, sBasePath .. "003 Evermoon North/Werecat Hunter")

-- |[Quantir Manor]|
fnCreateAlias("BestFriend",   "Best Friend",   true, sBasePath .. "004 Quantir Manor/Best Friend")
fnCreateAlias("BloodyMirror", "Bloody Mirror", true, sBasePath .. "004 Quantir Manor/Bloody Mirror")
fnCreateAlias("CandleHaunt",  "Candle Haunt",  true, sBasePath .. "004 Quantir Manor/Candle Haunt")
fnCreateAlias("EyeDrawer",    "Eye Drawer",    true, sBasePath .. "004 Quantir Manor/Eye Drawer")
fnCreateAlias("Ghost",        "Ghost Maid",    true, sBasePath .. "004 Quantir Manor/Ghost Maid")
fnCreateAlias("MirrorImage",  "Mirror Image",  true, sBasePath .. "004 Quantir Manor/Mirror Image")
fnCreateAlias("SkullCrawler", "Skitterer",     true, sBasePath .. "004 Quantir Manor/Skitterer")

-- |[Evermoon Swamps]|
fnCreateAlias("Mushraune",      "Mushraune",       true, sBasePath .. "005 Evermoon Swamps/Mushraune")
fnCreateAlias("Wisphag",        "Wisphag",         true, sBasePath .. "005 Evermoon Swamps/Wisphag")
fnCreateAlias("WerecatProwler", "Werecat Prowler", true, sBasePath .. "005 Evermoon Swamps/Werecat Prowler")

-- |[Evermoon River Wilds]|
fnCreateAlias("AlrauneKeeper",  "Alraune Keeper",  true, sBasePath .. "006 Evermoon River Wilds/Alraune Keeper")
fnCreateAlias("Sproutthrasher", "Sprout Thrasher", true, sBasePath .. "006 Evermoon River Wilds/Sprout Thrasher")
fnCreateAlias("BeeSkirmisher",  "Bee Skirmisher",  true, sBasePath .. "006 Evermoon River Wilds/Bee Skirmisher")
fnCreateAlias("BeeTracker",     "Bee Tracker",     true, sBasePath .. "006 Evermoon River Wilds/Bee Tracker")
fnCreateAlias("WerecatThief",   "Werecat Thief",   true, sBasePath .. "006 Evermoon River Wilds/Werecat Thief")

-- |[Rubber Wilds]|
fnCreateAlias("Rubberraune",    "Rubberraune",    true, sBasePath .. "007 Rubber Wilds/Rubberraune")
fnCreateAlias("Rubberbee",      "Rubberbee",      true, sBasePath .. "007 Rubber Wilds/Rubberbee")
fnCreateAlias("Rubbercat",      "Rubbercat",      true, sBasePath .. "007 Rubber Wilds/Rubbercat")
fnCreateAlias("RubbercatThief", "RubbercatThief", true, sBasePath .. "007 Rubber Wilds/RubbercatThief")

-- |[Zombee Hive]|
fnCreateAlias("Zombee", "Zombee", true, sBasePath .. "008 Zombee Hive/Zombee")

-- |[Trap Dungeon]|
fnCreateAlias("FingerF", "Finger Female", true, sBasePath .. "009 Trap Dungeon/Finger F")
fnCreateAlias("PalmM",   "Palm Male",     true, sBasePath .. "009 Trap Dungeon/Palm M")

-- |[St Fora's Church]|
fnCreateAlias("Gravemarker", "Gravemarker", true, sBasePath .. "010 St Foras/Gravemarker")
fnCreateAlias("Mushthrall",  "Mushthrall",  true, sBasePath .. "010 St Foras/Mushthrall")

-- |[ ================================= Auto Creation Function ================================= ]|
function fnCreateAlias(psSLFName, psAliasName, bHasParagon, psPath)
    
    --If the path is "AUTO", store that directly.
    local sUsePath = psPath .. ".lua"
    if(psPath == "AUTO") then sUsePath = "AUTO" end
    AdvCombat_SetProperty("Create Enemy Path", psAliasName, sUsePath)
    AdvCombat_SetProperty("Create Enemy Alias", psSLFName, psAliasName)
    
    --If needed, make a paragon version.
    if(bHasParagon) then
        
        --If the path is "AUTO", use that without additions:
        local sUsePath = psPath .. " Paragon.lua"
        if(psPath == "AUTO") then sUsePath = "AUTO" end
        
        AdvCombat_SetProperty("Create Enemy Path", psAliasName .. " Paragon", sUsePath)
        AdvCombat_SetProperty("Create Enemy Alias", psSLFName .. " Paragon", psAliasName .. " Paragon")
    end
end

-- |[Auto-Handler List]|
--Every enemy on this list uses the auto-handler.
local saAutoList = {"Bandit Boss", "Bandit Scrub", "Bandit Goon", "Demonic Mercenary", "Mannequin Scrub", "Mannequin Goon", "Bee Scout Paragon", "Bee Buddy Paragon", "Alraune Paragon", "Sproutpecker Paragon", 
                    "Skitterer Paragon", "Jelli Paragon", "Ghost Maid Paragon", "Toxic Slimegirl Paragon", "Bubbly Slimegirl Paragon", "Eye Drawer Paragon", "Bloody Mirror Paragon", "Candle Haunt Paragon",
                    "Werecat Hunter Paragon", "Werecat Prowler Paragon", "Bee Gatherer Paragon", "Bee Sniffer Paragon", "Bee Tracker Paragon", "Chilly Slimegirl Paragon", "Alraune Stem Paragon",
                    "Alraune Petal Paragon", "Sprout Thrasher Paragon", "Rubbercat Paragon", "Rubberbee Paragon", "Rubberraune Paragon", "Werecat Thief Paragon", "Mushthrall Paragon", "Finger Paragon"}
for i = 1, #saAutoList, 1 do
    fnCreateAlias(saAutoList[i], saAutoList[i], true, "AUTO")
end

-- |[Clean]|
fnCreateAlias = nil
