-- |[ ====================================== Jelli, Normal ===================================== ]|
--Adorable slime who has not taken human form.
local sActualName = "Jelli Paragon"
local iUniqueID = fnStandardEnemyHandler(sActualName)

-- |[Additional Setup]|
--If the entity needs more setup, like modifying AI or conditional cutscenes, do it here.