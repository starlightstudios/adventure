-- |[ ==================================== Bee Buddy, Normal =================================== ]|
--Adorable bee that beegirls use to find nectar.
local sActualName = "Bee Buddy"
local iUniqueID = fnStandardEnemyHandler(sActualName)

-- |[Additional Setup]|
--If the entity needs more setup, like modifying AI or conditional cutscenes, do it here.
