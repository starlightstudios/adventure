-- |[ ================================= Inky Slimegirl, Normal ================================= ]|
--A poet perhaps?
local sActualName = "Inky Slimegirl"
local iUniqueID = fnStandardEnemyHandler(sActualName)

-- |[Additional Setup]|
--If this is not a combat check (Mugging, Auto-Win Pulse) then don't run cutscenes.
if(TA_GetProperty("Is Non Combat Check") == true) then return end

--If the entity needs more setup, like modifying AI or conditional cutscenes, do it here.
if(iUniqueID ~= 0) then
    local sMeiSeenPartirhuman = VM_GetVar("Root/Variables/Chapter1/Counts/sMeiSeenPartirhuman", "S")
    if(sMeiSeenPartirhuman == "Nothing") then
        VM_SetVar("Root/Variables/Chapter1/Counts/sMeiSeenPartirhuman", "S", "Slime")
        AdvCombat_SetProperty("Add Script Execution", gsRoot .. "Combat/Cutscenes/Chapter 1/Meeting Inky Slime.lua")
    end
end
