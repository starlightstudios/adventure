-- |[ ================================= Sprout Pecker, Normal ================================== ]|
--Carnivorous bird-like plant, eats insects. Adorable!
local sActualName = "Sprout Pecker Paragon"
local iUniqueID = fnStandardEnemyHandler(sActualName)

-- |[Additional Setup]|
--If the entity needs more setup, like modifying AI or conditional cutscenes, do it here.