-- |[ ================================= Alraune Keeper, Normal ================================= ]|
--Tough grovekeeper alraune.
local sActualName = "Alraune Keeper"
local iUniqueID = fnStandardEnemyHandler(sActualName)

-- |[Additional Setup]|
--If this is not a combat check (Mugging, Auto-Win Pulse) then don't run cutscenes.
if(TA_GetProperty("Is Non Combat Check") == true) then return end

--If the entity needs more setup, like modifying AI or conditional cutscenes, do it here.
if(iUniqueID ~= 0) then
    local sMeiSeenPartirhuman = VM_GetVar("Root/Variables/Chapter1/Counts/sMeiSeenPartirhuman", "S")
    if(sMeiSeenPartirhuman == "Nothing") then
        VM_SetVar("Root/Variables/Chapter1/Counts/sMeiSeenPartirhuman", "S", "Alraune")
    end
end
