-- |[ ======================================== Infirm AI ======================================= ]|
--Poisons, bleeds, and sweeps in alternation.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ===================================== Combat Begins ====================================== ]|
--Called when combat begins, but before turn order is resolved for the 0th turn. If this AI was
-- arriving as reinforcements, this will be called at the start of the turn when they arrive before
-- turn order is resolved.
--If arriving in the middle of combat, the AI can determine they are a reinforcement by checking 
-- the turn counter. If it's 0, they were not a reinforcement.
if(iSwitchType == gciAI_CombatStart) then
    
    --Create a DataLibrary entry for this ID.
    local iUniqueID = RO_GetID()
    DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "_AI/")
    
    --Turn counter.
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/iTurn", "N", 0.0)

-- |[ ====================================== Turn Begins ======================================= ]|
--Called when the turn begins, after turn order is resolved.
elseif(iSwitchType == gciAI_TurnStart) then

-- |[ ==================================== AI Begins Action ==================================== ]|
--Called when a new Action begins, *not necessarily the action of the owning entity*. If it is the
-- action of the owning entity, we need to decide what action to perform. Otherwise, set variables
-- if the AI requires counting actions.
elseif(iSwitchType == gciAI_ActionBegin) then

-- |[ ==================================== AI Ends Action ====================================== ]|
--Called after an action ends, before the turn order is popped.
elseif(iSwitchType == gciAI_ActionEnd) then

-- |[ ======================================= Turn Ends ======================================== ]|
--Called after the turn ends but before turn order is resolved for the next turn or reinforcements
-- arrive, if any do.
elseif(iSwitchType == gciAI_TurnEnd) then

-- |[ ==================================== AI is Defeated ====================================== ]|
--Called when this entity is KO'd. Note that an enemy can be revived (in theory) which can cause
-- this to execute each time they are KO'd.
elseif(iSwitchType == gciAI_KnockedOut) then

-- |[ ====================================== Combat Ends ======================================= ]|
--Called when combat ends but before XP/JP/etc are awarded.
elseif(iSwitchType == gciAI_CombatEnds) then

-- |[ ================================ Application Pack Before ================================= ]|
--Called right before an application pack is processed, allowing querying and modification.
elseif(iSwitchType == gciAI_Application_Start) then

-- |[ ================================ Application Pack After ================================== ]|
--Called right after an application pack is processed, allowing querying.
elseif(iSwitchType == gciAI_Application_End) then

    --Boss AI does not care about threat.
    
-- |[ ====================================== Decide Action ===================================== ]|
--Decide which attack to use. Standard handling.
elseif(iSwitchType == gciAI_Decide_Action) then

    -- |[ ==================== Pre-Exec Setup ==================== ]|
    -- |[Events]|
    --Make sure the AI needs to spawn events. This flag is only true when the AI needs to spawn events.
    local bCanSpawnEvent = AdvCombat_GetProperty("Is AI Spawning Events")
    if(bCanSpawnEvent == false) then return end
    
    -- |[AI Routing]|
    --The boss can never be confused or change its script routing.

    -- |[Description]|
    --On turn 0, the AI selects a party member and points at them. On turn 3, it then uses
    -- Shatter on them, and resets back to turn 0. KO'd party members cannot be selected.

    -- |[Check Turn]|
    --Compare the ID of the owner with the ID of the acting entity.
    local iOwnerID = RO_GetID()
    local iActingID = AdvCombat_GetProperty("ID of Acting Entity")
    if(iOwnerID ~= iActingID) then return end

    -- |[Stun]|
    --Boss can be stunned.
    local bIsStunned = fnStunHandleActionBegin()
    if(bIsStunned == true) then

        --Animation.
        AdvCombat_SetProperty("Set Ability As Active", gciAI_StunAbilitySlot)

        --Stop action here.
        AdvCombat_SetProperty("Mark Handled Action")
        AdvCombat_SetProperty("Run Active Ability On Active Targets")
        return
    end

    -- |[ ======== Ability Selection ========= ]|
    --Get what turn we're on.
    local iTurn = VM_GetVar("Root/Variables/Combat/" .. iOwnerID .. "_AI/iTurn", "I")
    
    -- |[Turn Zero]|
    --Attack Mei with a poison attack. If Mei is down, attack Florentina.
    if(iTurn == 0.0) then
        
        --Turn flag.
        VM_SetVar("Root/Variables/Combat/" .. iOwnerID .. "_AI/iTurn", "N", 1.0)
        
        --Set ability.
        AdvCombat_SetProperty("Set Ability As Active", gciAI_AbilityRollStart+0)
        
        --Paint targets.
        AdvCombat_SetProperty("Run Active Ability Target Script")
        local iTargetCluster = fnPreferentialTarget("Mei")
        AdvCombat_SetProperty("Set Target Cluster As Active", iTargetCluster)
    
    -- |[Turn One]|
    --Attack Florentina with a bleed attack. If Florentina is down, attack Mei.
    elseif(iTurn == 1.0) then
        
        --Turn flag.
        VM_SetVar("Root/Variables/Combat/" .. iOwnerID .. "_AI/iTurn", "N", 2.0)
        
        --Set ability.
        AdvCombat_SetProperty("Set Ability As Active", gciAI_AbilityRollStart+1)
        
        --Paint targets.
        AdvCombat_SetProperty("Run Active Ability Target Script")
        local iTargetCluster = fnPreferentialTarget("Florentina")
        AdvCombat_SetProperty("Set Target Cluster As Active", iTargetCluster)
        io.write("Attacking target cluster: " .. iTargetCluster .. "\n")
    
    -- |[Turn Two]|
    --Hit all party members.
    elseif(iTurn == 2.0) then
        
        --Turn flag.
        VM_SetVar("Root/Variables/Combat/" .. iOwnerID .. "_AI/iTurn", "N", 0.0)
        
        --Set ability.
        AdvCombat_SetProperty("Set Ability As Active", gciAI_AbilityRollStart+2)
        AdvCombat_SetProperty("Run Active Ability Target Script")
        AdvCombat_SetProperty("Set Target Cluster As Active", 0)
    end
    
    --Run the ability.
    AdvCombat_SetProperty("Mark Handled Action")
    AdvCombat_SetProperty("Run Active Ability On Active Targets")
    
-- |[ ===================================== Threat Update ====================================== ]|
--Call this whenever you need to update the threat values for this AI.
elseif(iSwitchType == gciAI_UpdateThreat) then

    --Boss AI does not care about threat.
end
