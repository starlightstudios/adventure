-- |[ ==================================== Golem Chemist AI ==================================== ]|
--Standard AI, but uses the "Melt" attack on any target that has 3 stacks of "Searing Splash".

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ===================================== Combat Begins ====================================== ]|
--Called when combat begins, but before turn order is resolved for the 0th turn. If this AI was
-- arriving as reinforcements, this will be called at the start of the turn when they arrive before
-- turn order is resolved.
--If arriving in the middle of combat, the AI can determine they are a reinforcement by checking 
-- the turn counter. If it's 0, they were not a reinforcement.
if(iSwitchType == gciAI_CombatStart) then
    
    --Create a DataLibrary entry for this ID.
    local iUniqueID = RO_GetID()
    DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "_AI/")

-- |[ ====================================== Turn Begins ======================================= ]|
--Called when the turn begins, after turn order is resolved.
elseif(iSwitchType == gciAI_TurnStart) then

-- |[ ==================================== AI Begins Action ==================================== ]|
--Called when a new Action begins, *not necessarily the action of the owning entity*. If it is the
-- action of the owning entity, we need to decide what action to perform. Otherwise, set variables
-- if the AI requires counting actions.
elseif(iSwitchType == gciAI_ActionBegin) then
    
    -- |[ ======================== Threat ======================== ]|
    --Even if this is not the AI's turn, update its threat effects. This is to be sure the player
    -- has accurate values.
    LM_ExecuteScript(LM_GetCallStack(0), gciAI_UpdateThreat)

-- |[ ==================================== AI Ends Action ====================================== ]|
--Called after an action ends, before the turn order is popped.
elseif(iSwitchType == gciAI_ActionEnd) then

-- |[ ======================================= Turn Ends ======================================== ]|
--Called after the turn ends but before turn order is resolved for the next turn or reinforcements
-- arrive, if any do.
elseif(iSwitchType == gciAI_TurnEnd) then

-- |[ ==================================== AI is Defeated ====================================== ]|
--Called when this entity is KO'd. Note that an enemy can be revived (in theory) which can cause
-- this to execute each time they are KO'd.
elseif(iSwitchType == gciAI_KnockedOut) then

-- |[ ====================================== Combat Ends ======================================= ]|
--Called when combat ends but before XP/JP/etc are awarded.
elseif(iSwitchType == gciAI_CombatEnds) then

-- |[ ================================ Application Pack Before ================================= ]|
--Called right before an application pack is processed, allowing querying and modification.
elseif(iSwitchType == gciAI_Application_Start) then

-- |[ ================================ Application Pack After ================================== ]|
--Called right after an application pack is processed, allowing querying.
elseif(iSwitchType == gciAI_Application_End) then

    --Boss AI does not care about threat.
    
-- |[ ====================================== Decide Action ===================================== ]|
--Decide which attack to use. Standard handling.
elseif(iSwitchType == gciAI_Decide_Action) then

    -- |[ ==================== AI Description ==================== ]|
    --Checks if any target has 3 stacks of Searing Splash. If so, use Melt on them. Otherwise,
    -- attack by threat and probability.

    -- |[ ==================== Pre-Exec Setup ==================== ]|
    -- |[Events]|
    --Make sure the AI needs to spawn events. This flag is only true when the AI needs to spawn events.
    local bCanSpawnEvent = AdvCombat_GetProperty("Is AI Spawning Events")
    if(bCanSpawnEvent == false) then return end
    
    -- |[Setup]|
    --Get variables.
    local iOwnerID = RO_GetID()
    local iActingID = AdvCombat_GetProperty("ID of Acting Entity")
    
    -- |[AI Routing]|
    --The boss can never be confused or change its script routing.

    -- |[Check Turn]|
    --Compare the ID of the owner with the ID of the acting entity.
    if(iOwnerID ~= iActingID) then return end

    -- |[Stun]|
    --Boss cannot be stunned.
    fnStunHandleActionBegin()

    --Get what turn we're on.
    local iTurn = VM_GetVar("Root/Variables/Combat/" .. iOwnerID .. "_AI/iTurn", "I")
    
    -- |[ ================== Target Population =================== ]|
    -- |[Populate Target by Threat]|
    --Paint targets using the zeroth ability. Populate target clusters for later checking.
    local iMasterSlot = AdvCombatEntity_GetProperty("Slot of Ability In Master List", "Enemy|Acid Splash")
    AdvCombat_SetProperty("Set Ability As Active", iMasterSlot)
    AdvCombat_SetProperty("Run Active Ability Target Script")
    
    --One all target clusters are populated, check if any of them have 3 stacks of the DoT on them.
    -- Any [Corroding DoT] is fine.
    local iaPossibleTargets = {}
    local iTotalTargetClusters = AdvCombat_GetProperty("Total Target Clusters")
    for i = 1, iTotalTargetClusters, 1 do
        
        --Should be only one target per cluster.
        local iTotalTargets = AdvCombat_GetProperty("Total Targets In Cluster", i-1)
        for p = 1, iTotalTargets, 1 do
        
            --Get this target's ID.
            local iTargetID = AdvCombat_GetProperty("Target ID In Cluster", i-1, p-1)
            
            --Check if they have 3 [Corroding Dot] stacks.
            AdvCombat_SetProperty("Push Entity By ID", iTargetID)
                local iTagCount = AdvCombatEntity_GetProperty("Tag Count", "Corroding DoT")
            DL_PopActiveObject()
            
            --If they do, add them to the list.
            if(iTagCount >= 3) then
                local iSize = #iaPossibleTargets + 1
                iaPossibleTargets[iSize] = iTargetID
            end
        end
    end
    
    -- |[ ======================= Melt Them ====================== ]|
    --If there are any targets on the melt list, pick one at random and melt them.
    if(#iaPossibleTargets > 0) then
    
        --Select a target.
        local iFinalTargetID = iaPossibleTargets[1]
        if(#iaPossibleTargets > 1) then
            local iUseSlot = LM_GetRandomNumber(1, #iaPossibleTargets)
            iFinalTargetID = iaPossibleTargets[iUseSlot]
        end
        
        --Run the target script.
        iMasterSlot = AdvCombatEntity_GetProperty("Slot of Ability In Master List", "Enemy|Melt")
        AdvCombat_SetProperty("Set Ability As Active", iMasterSlot)
        AdvCombat_SetProperty("Run Active Ability Target Script")
        
        --Locate the cluster that has the target ID in it.
        local iMarkedCluster = 0
        local iTotalTargetClusters = AdvCombat_GetProperty("Total Target Clusters")
        for i = 0, iTotalTargetClusters-1, 1 do
            local iTotalTargets = AdvCombat_GetProperty("Total Targets In Cluster", i)
            for p = 0, iTotalTargets-1, 1 do
                
                --ID Match, target this cluster.
                local iTargetID = AdvCombat_GetProperty("Target ID In Cluster", i, p)
                if(iTargetID == iFinalTargetID) then
                    iMarkedCluster = i
                end
            end
        end
        
        --Mark target.
        AdvCombat_SetProperty("Set Target Cluster As Active", iMarkedCluster)
        AdvCombat_SetProperty("Mark Handled Action")
        AdvCombat_SetProperty("Run Active Ability On Active Targets")
        return
    end
    
    -- |[ ================== Standard Handling =================== ]|
    --There were no melt-able targets, so pick one of the other abilities and use it.
    fnPickAbilityByRoll(true)
    fnPickTargetByThreat(true, true)
    
-- |[ ===================================== Threat Update ====================================== ]|
--Call this whenever you need to update the threat values for this AI.
elseif(iSwitchType == gciAI_UpdateThreat) then

    --Boss AI does not care about threat.
end
