-- |[ ======================================= Palm Paragon AI ======================================== ]|
--An AI that has a specific series of skills it uses, in order. If its turn is skipped for any reason
-- (Ambush, Stun) then it also skips using an ability.
--Gravemarker has the most complicated AI. It uses Groaning slab, buffing itself each time. These buffs count up to 5. 
--At 5 it uses "Weeping stone" which passes it's turn, and gives itself a signalling buff.
--when it has the signalling buff it uses "Trumpet sounds" which resets the counters.
--Groaning slab buff: Crusading
--Weeping stone: Obscuring

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ===================================== Combat Begins ====================================== ]|
--At combat stat, reset global variables for all Poison Vines. They share a cooldown on the cloud
-- skill so they don't all use it at once.
if(iSwitchType == gciAI_CombatStart) then
    
    -- |[Standard]|
    --Call the standard for this object.
    LM_ExecuteScript(gsRoot .. "Combat/AIs/000 Normal AI.lua", iSwitchType)
    
    -- |[Global]|
    --DataLibrary entry.
    local iUniqueID = RO_GetID()
    DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "_AI/")
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/iTurn", "N", 0.0)
    
    -- |[Debug]|
    --io.write("Palm Paragon AI initialized.\n")
    
-- |[ ====================================== Decide Action ===================================== ]|
--Decide which attack to use. Standard handling.
elseif(iSwitchType == gciAI_Decide_Action) then

    -- |[ =================== Common Variables =================== ]|
    local iUniqueID = RO_GetID()
    local iActingID = AdvCombat_GetProperty("ID of Acting Entity")
    local iCurrentTurn = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/iTurn", "N")
        
    --Check if this entity was the one acting. If so, run the turn counter. This happens even if the
    -- entity was stunned or ambushed.
    if(iUniqueID == iActingID) then
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/iTurn", "N", iCurrentTurn + 1)
    end
    
    -- |[ =============== Standard Action Handlers =============== ]|
    --This subroutine will do all the normal work of checking if this entity should handle its action,
    -- if it's ambushed, stunned, etc. If it returns false, don't call the below code.
    if(fnStandardDecideActionSetup() == false) then
        --io.write("Palm AI failed to act on turn " .. math.floor(iCurrentTurn) .. ".\n")
        return
    end
    
    -- |[Debug]|
    --io.write("Palm AI is selecting action on turn " .. math.floor(iCurrentTurn) .. ".\n")
    
    -- |[ ================== Ability Execution =================== ]|
    --Get current job
    --local sCurrentJob = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
    
    -- |[Get Tag count]|
    --The AI uses the presence of bleeding tags to decide what to do. (If bleeding tags expire will that mess with the AI?)
    
            --AdvCombat_SetProperty("Push Target", piTargetIndex)
            --this line needs to be targeting the entity, how do I make that happen?
            --local iTargetID = RO_GetID()
           -- local iTagCount = AdvCombatEntity_GetProperty("Tag Count", "Bleeding DoT")
            --iTagCount should now reflect the number of bleeding tags active this turn.
    
    
    -- |[Ability Selection]|
    --The AI uses its abilities in sequence, skipping abilities if it got stunned or otherwise
    -- was unable to act.
    --This sample uses a cycle of 5 turns. If something goes wrong, Bleed Attack is used by default.
    local sToUseAttack = "Enemy|Bleed Attack"
    local iModulusTurn = iCurrentTurn % 1
    local iCrusadeTagCount = AdvCombatEntity_GetProperty("Tag Count", "Crusading DoT")
    local iObscureTagCount = AdvCombatEntity_GetProperty("Tag Count", "Obscuring DoT")
    
    --io.write("Palm AI is using turn modulus " .. iModulusTurn .. ".\n")
    
    --If the Crusade count reaches 5:
   if(iCrusadeTagCount >= 5) then
      local saAbilityNames = {}
      table.insert(saAbilityNames, "Enemy|Weeping Stone")
      sToUseAttack = saAbilityNames[iModulusTurn+1]
    
    --If the obscure tag exists:
    elseif(iObscureTagCount > 0) then
      local saAbilityNames = {}
      table.insert(saAbilityNames, "Enemy|Trumpet Sounds")
      sToUseAttack = saAbilityNames[iModulusTurn+1]
    
    --In all other cases, use Groaning Slab
    else
      local saAbilityNames = {}
      table.insert(saAbilityNames, "Enemy|Groaning Slab")
      sToUseAttack = saAbilityNames[iModulusTurn+1]
    end
    
    --Resolve the slot and set it as active.
    local iMasterSlot = AdvCombatEntity_GetProperty("Slot of Ability In Master List", sToUseAttack)
    AdvCombat_SetProperty("Set Ability As Active", iMasterSlot)
    --io.write("Palm AI activates ability in slot " .. iMasterSlot .. ".\n")
    
    -- |[Paint Targets, Execute]|
    --Function paints targets and selects a cluster by threat. If flagged, also executes the ability.
    fnPickTargetByThreat(true, true)
    
    -- |[Clear Effects]|
    --After using Trumpet Sounds, clear all signal effects.
    if(sToUseAttack == "Enemy|Trumpet Sounds") then
    
        --Setup.
        local iEffectCount = 0
        local iaEffectIDList = {}
        
        --Get a list of all effects referencing the target. Remove any with the matching tag.
        local iEffectsTotal = AdvCombat_SetProperty("Store Effects Referencing", iUniqueID)
        for i = 0, iEffectsTotal-1, 1 do
            AdvCombat_SetProperty("Push Temp Effects", i)
            
                --Tag is present:
                local iCount = AdvCombatEffect_GetProperty("Get Tag Count", "Obscuring DoT")
                if(iCount > 0) then
                    iEffectCount = iEffectCount + 1
                    iaEffectIDList[iEffectCount] = RO_GetID()
                end
            DL_PopActiveObject()
        end
        
        --Clean up.
        AdvCombat_SetProperty("Clear Temp Effects")
        
        --If there was at least one effect on the list to remove, add application packs for the removal.
        if(iEffectCount > 0) then
            for i = 1, #iaEffectIDList, 1 do
                AdvCombat_SetProperty("Register Application Pack", iUniqueID, iUniqueID, 0, "Remove Effect|" .. iaEffectIDList[i])
            end
        end
    end
    
-- |[ ===================================== Default Cases ====================================== ]|
--If not otherwise specified, use the default AI.
else
    LM_ExecuteScript(gsRoot .. "Combat/AIs/000 Normal AI.lua", iSwitchType)
end