-- |[ =================================== AI Script Routing ==================================== ]|
--Calls AI scripts within this folder with appropriate diagnostics flags.
local sBasePath = fnResolvePath()

-- |[Repeat Check]|
if(gbHasBuiltAIStringsCh1 ~= nil) then return end
gbHasBuiltAIStringsCh1 = true

-- |[Call]|
LM_ExecuteScript(sBasePath .. "000 Bandit AIs.lua")
LM_ExecuteScript(sBasePath .. "001 Mausoleum AIs.lua")