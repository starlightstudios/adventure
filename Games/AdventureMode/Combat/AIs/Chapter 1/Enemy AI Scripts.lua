-- |[ ================================== Chapter 1 AI Strings ================================== ]|
--Called once at program start, builds a list of AIString sets that can be used by enemies by
-- having tags. A set of constants is provided to make them easier to organize.
--Calling AIString:new() will register the string to the global list.

-- |[Repeat Check]|
if(gbHasBuiltAIStringsCh1 ~= nil) then return end
gbHasBuiltAIStringsCh1 = true

--Setup.
local sString = ""

-- |[ ==================================== Bandit Grouping ===================================== ]|
-- |[ ====== Demon Mercenary ======= ]|
--Uses a sword attack 60% of the time, Burning Word 40% of the time. On the 2nd, 5th, 8th, etc turn,
-- uses Chanting to charge up Torrent of Flame.
sString = 
"[GVAR,iHasRunOnce,N:==:0]{SVAR:iHasRunOnce:N:1}{SVAR:iChantCooldown:N:2} || " ..
"{TOZERO:iChantCooldown:1} || " ..
"[GVAR,iChantCooldown,N:<=:0]{SVAR:iChantCooldown:N:3}{Prime:Enemy|Chanting}{Target:Self}{Finish} || " ..
"[GVAR,iAttackRoll,N:<:40]{Prime:Enemy|Burning Word}{Target:Threat}{Finish} || "..
"{Prime:Enemy|Sword Attack}{Target:Threat}{Finish}"
AIString:new("Demon Mercenary", sString)

-- |[ ======== Bandit Scrub ======== ]|
--Has a regular attack and a sweep attack.
sString = 
"[GVAR,iAttackRoll,N:<:60]{Prime:Enemy|Sword Attack}{Target:Threat}{Finish} || "..
"{Prime:Enemy|Knife Sweep}{Target:Threat}{Finish}"
AIString:new("Bandit Scrub", sString)

-- |[ ======== Bandit Goon ========= ]|
--Throws a powder bomb for fire damage, 3 turn cooldown, starts on cooldown.
sString = 
"[GVAR,iHasRunOnce,N:==:0]{SVAR:iHasRunOnce:N:1}{SVAR:iBombCooldown:N:Rand,2,3} || " ..
"{TOZERO:iBombCooldown:1} || " ..
"[GVAR,iBombCooldown,N:<=:0]{SVAR:iBombCooldown:N:3}{Prime:Enemy|Powder Bomb}{Target:Threat}{Finish} || " ..
"{Prime:Enemy|Sword Attack}{Target:Threat}{Finish}"
AIString:new("Bandit Goon", sString)

-- |[ ======== Bandit Boss ========= ]|
--Uses the buffing ability "Threats" on a 3-turn cooldown if there are allies nearby.
sString =
"[GVAR,iHasRunOnce,N:==:0]{SVAR:iHasRunOnce:N:1}{SVAR:iThreatsCooldown:N:0} || " ..
"{TOZERO:iThreatsCooldown:1} || " ..
"[SizeOfParty,Friendly:>=:2][GVAR,iThreatsCooldown,N:<=:0]{SVAR:iThreatsCooldown:N:3}{Prime:Enemy|Threats}{Target:Random}{Finish} || " ..
"[GVAR,iAttackRoll,N:<:40]{Prime:Enemy|Bleed Attack}{Target:Threat}{Finish} || "..
"{Prime:Enemy|Sword Attack}{Target:Threat}{Finish}"
AIString:new("Bandit Boss", sString)

-- |[ ========== Victoria ========== ]|
--Boss encounter AI. Switches tactics below 40% HP, which is just the chance to use certain attacks.
sString =
"[GVAR,iHasRunOnce,N:==:0]{SVAR:iHasRunOnce:N:1}{SVAR:iFlamesCooldown:N:0} || " ..
"{TOZERO:iFlamesCooldown:1} || " ..
"[LowestHPPercent,Enemy:>=:40][GVAR,iAttackRoll,N:<:30]{Prime:Boss|Victoria|Poison Mist}{Target:Threat}{Finish} || "..
"[LowestHPPercent,Enemy:>=:40][GVAR,iAttackRoll,N:<:60]{Prime:Boss|Victoria|Homing Needles}{Target:Threat}{Finish} || " .. 
"[LowestHPPercent,Enemy:>=:40]{Prime:Boss|Victoria|Rapier Slice}{Target:Threat}{Finish} || " ..
"[GVAR,iFlamesCooldown,N:<=:0][GVAR,iAttackRoll,N:<:30]{SVAR:iFlamesCooldown:N:2}{Prime:Boss|Victoria|Flashburn}{Target:Threat}{Finish} || " ..
"[GVAR,iAttackRoll,N:<:50]{Prime:Boss|Victoria|Rapier Slice}{Target:Threat}{Finish} || " ..
"{Prime:Boss|Victoria|Homing Needles}{Target:Threat}{Finish}"
AIString:new("Victoria", sString)

-- |[ ======================================= Mausoleum ======================================== ]|
-- |[ ========== Best Friend ========== ]|
--Has an equal chance to use bleed dot/claw attack, has a summon ally on three turn cooldown. On use sets global cooldown so groups won't fire them all at once.
sString =
--Initialize
"[GVAR,iHasRunOnce,N:==:0]{SVAR:iHasRunOnce:N:1}{InitResettable:iHasRunBestFriendSACDThisTurn:N:0}{SetGlobal:iGlobalBestFriendSACD:N:1} || " .. 
--Once-Per-Turn-Global
"[GetResettable,iHasRunBestFriendSACDThisTurn,N:==:0]{SetResettable:iHasRunBestFriendSACDThisTurn:N:1}{SetGlobal:iGlobalBestFriendSACD:N:GetGlobal,iGlobalBestFriendSACD,N - 1} || " .. 
--Summon Ally is on cooldown. 50 Bleed, 50 Claw
"[GetGlobal,iGlobalBestFriendSACD,N:>=:1][GVAR,iAttackRoll,N:<:50]{Prime:Enemy|Bleed DoT}{Target:Threat}{Finish} || " ..
"[GetGlobal,iGlobalBestFriendSACD,N:>=:1]{Prime:Enemy|Claw Attack}{Target:Threat}{Finish} || " ..
--Summon Ally is available. 50 Summon, 25 Bleed, 25 Claw
"[GVAR,iAttackRoll,N:<:50]{SetGlobal:iGlobalBestFriendSACD:N:3}{Prime:Enemy|Best Friend SA}{Target:Random}{Finish} || " ..
"[GVAR,iSACooldown,N:<=:0][GVAR,iAttackRoll,N:<:75]{Prime:Enemy|Bleed DoT}{Target:Threat}{Finish} || " ..
"{Prime:Enemy|Claw Attack}{Target:Threat}{Finish}"
AIString:new("Best Friend", sString)

-- |[ ========== Mirror Image ========== ]|
--Chance to use a chaser if the enemy is bleeding, otherwise, uses bleed DoT or a lifeleech.
sString =
"[GetPartySlot,Hostile,WithTag,'Bleeding DoT':>=:0][GVAR,iAttackRoll,N:<:50]{Prime:Enemy|Mirror's Drain}{Target:Prefer Tag:'Bleeding DoT'}{Finish}  || " ..
"[GetPartySlot,Hostile,WithTag,'Bleeding DoT':>=:0][GVAR,iAttackRoll,N:<:75]{Prime:Enemy|Bleed DoT}{Target:Threat}{Finish}  || " ..
"[GVAR,iAttackRoll,N:<:33]{Prime:Enemy|Tear Wound}{Target:Threat}{Finish}  || " ..
"[GVAR,iAttackRoll,N:<:66]{Prime:Enemy|Bleed DoT}{Target:Threat}{Finish}  || " ..
"{Prime:Enemy|Sword Attack}{Target:Threat}{Finish}"
AIString:new("Mirror Image", sString)

