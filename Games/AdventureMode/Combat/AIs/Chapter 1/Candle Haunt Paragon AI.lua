-- |[ ======================================= Candle Haunt Paragon AI ======================================== ]|
--An AI that has a specific series of skills it uses, in order. If its turn is skipped for any reason
-- (Ambush, Stun) then it also skips using an ability.
--Candle Haunt paragon follows a pattern of 1. Fireball 2. Fireball 3. Call for Help

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ===================================== Combat Begins ====================================== ]|
--At combat stat, reset global variables for all Poison Vines. They share a cooldown on the cloud
-- skill so they don't all use it at once.
if(iSwitchType == gciAI_CombatStart) then
    
    -- |[Standard]|
    --Call the standard for this object.
    LM_ExecuteScript(gsRoot .. "Combat/AIs/000 Normal AI.lua", iSwitchType)
    
    -- |[Global]|
    --DataLibrary entry.
    local iUniqueID = RO_GetID()
    DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "_AI/")
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/iTurn", "N", 0.0)
    
    -- |[Debug]|
    --io.write("Candle Haunt AI initialized.\n")
    
-- |[ ====================================== Decide Action ===================================== ]|
--Decide which attack to use. Standard handling.
elseif(iSwitchType == gciAI_Decide_Action) then

    -- |[ =================== Common Variables =================== ]|
    local iUniqueID = RO_GetID()
    local iActingID = AdvCombat_GetProperty("ID of Acting Entity")
    local iCurrentTurn = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/iTurn", "N")
        
    --Check if this entity was the one acting. If so, run the turn counter. This happens even if the
    -- entity was stunned or ambushed.
    if(iUniqueID == iActingID) then
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/iTurn", "N", iCurrentTurn + 1)
    end
    
    -- |[ =============== Standard Action Handlers =============== ]|
    --This subroutine will do all the normal work of checking if this entity should handle its action,
    -- if it's ambushed, stunned, etc. If it returns false, don't call the below code.
    if(fnStandardDecideActionSetup() == false) then
        --io.write("Candle Haunt AI failed to act on turn " .. math.floor(iCurrentTurn) .. ".\n")
        return
    end
    
    -- |[Debug]|
    --io.write("Candle Haunt AI is selecting action on turn " .. math.floor(iCurrentTurn) .. ".\n")
    
    -- |[ ================== Ability Execution =================== ]|
    -- |[Ability Selection]|
    --The AI uses its abilities in sequence, skipping abilities if it got stunned or otherwise
    -- was unable to act.
    --This sample uses a cycle of 5 turns. If something goes wrong, Bleed Attack is used by default.
    local sToUseAttack = "Enemy|Bleed Attack"
    local iModulusTurn = iCurrentTurn % 3
    --io.write("Candle Haunt AI is using turn modulus " .. iModulusTurn .. ".\n")
    
    --Create an array of ability names.
    local saAbilityNames = {}
    table.insert(saAbilityNames, "Enemy|Fireball")
    table.insert(saAbilityNames, "Enemy|Fireball")
    table.insert(saAbilityNames, "Enemy|Candle Haunt SA")
    
    --Select.
    sToUseAttack = saAbilityNames[iModulusTurn+1]
    --io.write("Candle Haunt AI selects ability " .. sToUseAttack .. ".\n")
    
    --Resolve the slot and set it as active.
    local iMasterSlot = AdvCombatEntity_GetProperty("Slot of Ability In Master List", sToUseAttack)
    AdvCombat_SetProperty("Set Ability As Active", iMasterSlot)
    --io.write("Candle Haunt AI activates ability in slot " .. iMasterSlot .. ".\n")
    
    -- |[Paint Targets, Execute]|
    --Function paints targets and selects a cluster by threat. If flagged, also executes the ability.
    fnPickTargetByThreat(true, true)
    
-- |[ ===================================== Default Cases ====================================== ]|
--If not otherwise specified, use the default AI.
else
    LM_ExecuteScript(gsRoot .. "Combat/AIs/000 Normal AI.lua", iSwitchType)
end