-- |[ ======================================= Mausoleum ======================================== ]|
--AIs used by enemies found in the mausoleum but not paragons.
local sString = ""

-- |[ ========== Best Friend ========== ]|
--Has an equal chance to use bleed dot/claw attack, has a summon ally on three turn cooldown. On use sets global cooldown so groups won't fire them all at once.
sString =
--Initialize
"[GVAR,iHasRunOnce,N:==:0]{SVAR:iHasRunOnce:N:1}{InitResettable:iHasRunBestFriendSACDThisTurn:N:0}{SetGlobal:iGlobalBestFriendSACD:N:1} || " .. 
--Once-Per-Turn-Global
"[GetResettable,iHasRunBestFriendSACDThisTurn,N:==:0]{SetResettable:iHasRunBestFriendSACDThisTurn:N:1}{SetGlobal:iGlobalBestFriendSACD:N:GetGlobal,iGlobalBestFriendSACD,N - 1} || " .. 
--Summon Ally is on cooldown. 50 Bleed, 50 Claw
"[GetGlobal,iGlobalBestFriendSACD,N:>=:1][GVAR,iAttackRoll,N:<:50]{Prime:Enemy|Bleed DoT}{Target:Threat}{Finish} || " ..
"[GetGlobal,iGlobalBestFriendSACD,N:>=:1]{Prime:Enemy|Claw Attack}{Target:Threat}{Finish} || " ..
--Summon Ally is available. 50 Summon, 25 Bleed, 25 Claw
"[GVAR,iAttackRoll,N:<:50]{SetGlobal:iGlobalBestFriendSACD:N:3}{Prime:Enemy|Best Friend SA}{Target:Random}{Finish} || " ..
"[GVAR,iSACooldown,N:<=:0][GVAR,iAttackRoll,N:<:75]{Prime:Enemy|Bleed DoT}{Target:Threat}{Finish} || " ..
"{Prime:Enemy|Claw Attack}{Target:Threat}{Finish}"
AIString:new("Best Friend", sString)

-- |[ ========== Mirror Image ========== ]|
--Chance to use a chaser if the enemy is bleeding, otherwise, uses bleed DoT or a lifeleech.
sString =
"[GetPartySlot,Hostile,WithTag,'Bleeding DoT':>=:0][GVAR,iAttackRoll,N:<:50]{Prime:Enemy|Mirror's Drain}{Target:Prefer Tag:'Bleeding DoT'}{Finish}  || " ..
"[GetPartySlot,Hostile,WithTag,'Bleeding DoT':>=:0][GVAR,iAttackRoll,N:<:75]{Prime:Enemy|Bleed DoT}{Target:Threat}{Finish}  || " ..
"[GVAR,iAttackRoll,N:<:33]{Prime:Enemy|Tear Wound}{Target:Threat}{Finish}  || " ..
"[GVAR,iAttackRoll,N:<:66]{Prime:Enemy|Bleed DoT}{Target:Threat}{Finish}  || " ..
"{Prime:Enemy|Sword Attack}{Target:Threat}{Finish}"
AIString:new("Mirror Image", sString)

--[=[
--Rotating attack AI.
sString =
"[GVAR,iTurn,N % 2:==:0]{Prime:Enemy|Tear Wound}{Target:Threat}{Finish}  || " ..
"[GVAR,iTurn,N % 2:==:1]{Prime:Enemy|Bleed DoT}{Target:Threat}{Finish}  || " ..
"{Prime:Enemy|Sword Attack}{Target:Threat}{Finish}"
AIString:new("Mirror Image", sString)
]=]