-- |[ ==================================== Bandit Grouping ===================================== ]|
--AIs used by the bandit enemies in and around the fishing village.
local sString = ""

-- |[ ====== Demon Mercenary ======= ]|
--Uses a sword attack 60% of the time, Burning Word 40% of the time. On the 2nd, 5th, 8th, etc turn,
-- uses Chanting to charge up Torrent of Flame.
sString = 
"[GVAR,iHasRunOnce,N:==:0]{SVAR:iHasRunOnce:N:1}{SVAR:iChantCooldown:N:2} || " ..
"{TOZERO:iChantCooldown:1} || " ..
"[GVAR,iChantCooldown,N:<=:0]{SVAR:iChantCooldown:N:3}{Prime:Enemy|Chanting}{Target:Self}{Finish} || " ..
"[GVAR,iAttackRoll,N:<:40]{Prime:Enemy|Burning Word}{Target:Threat}{Finish} || "..
"{Prime:Enemy|Sword Attack}{Target:Threat}{Finish}"
AIString:new("Demon Mercenary", sString)

-- |[ ======== Bandit Scrub ======== ]|
--Has a regular attack and a sweep attack.
sString = 
"[GVAR,iAttackRoll,N:<:60]{Prime:Enemy|Sword Attack}{Target:Threat}{Finish} || "..
"{Prime:Enemy|Knife Sweep}{Target:Threat}{Finish}"
AIString:new("Bandit Scrub", sString)

-- |[ ======== Bandit Goon ========= ]|
--Throws a powder bomb for fire damage, 3 turn cooldown, starts on cooldown.
sString = 
"[GVAR,iHasRunOnce,N:==:0]{SVAR:iHasRunOnce:N:1}{SVAR:iBombCooldown:N:Rand,2,3} || " ..
"{TOZERO:iBombCooldown:1} || " ..
"[GVAR,iBombCooldown,N:<=:0]{SVAR:iBombCooldown:N:3}{Prime:Enemy|Powder Bomb}{Target:Threat}{Finish} || " ..
"{Prime:Enemy|Sword Attack}{Target:Threat}{Finish}"
AIString:new("Bandit Goon", sString)

-- |[ ======== Bandit Boss ========= ]|
--Uses the buffing ability "Threats" on a 3-turn cooldown if there are allies nearby.
sString =
"[GVAR,iHasRunOnce,N:==:0]{SVAR:iHasRunOnce:N:1}{SVAR:iThreatsCooldown:N:0} || " ..
"{TOZERO:iThreatsCooldown:1} || " ..
"[SizeOfParty,Friendly:>=:2][GVAR,iThreatsCooldown,N:<=:0]{SVAR:iThreatsCooldown:N:3}{Prime:Enemy|Threats}{Target:Random}{Finish} || " ..
"[GVAR,iAttackRoll,N:<:40]{Prime:Enemy|Bleed Attack}{Target:Threat}{Finish} || "..
"{Prime:Enemy|Sword Attack}{Target:Threat}{Finish}"
AIString:new("Bandit Boss", sString)

-- |[ ========== Victoria ========== ]|
--Boss encounter AI. Switches tactics below 40% HP, which is just the chance to use certain attacks.
sString =
"[GVAR,iHasRunOnce,N:==:0]{SVAR:iHasRunOnce:N:1}{SVAR:iFlamesCooldown:N:0} || " ..
"{TOZERO:iFlamesCooldown:1} || " ..
"[LowestHPPercent,Enemy:>=:40][GVAR,iAttackRoll,N:<:30]{Prime:Boss|Victoria|Poison Mist}{Target:Threat}{Finish} || "..
"[LowestHPPercent,Enemy:>=:40][GVAR,iAttackRoll,N:<:60]{Prime:Boss|Victoria|Homing Needles}{Target:Threat}{Finish} || " .. 
"[LowestHPPercent,Enemy:>=:40]{Prime:Boss|Victoria|Rapier Slice}{Target:Threat}{Finish} || " ..
"[GVAR,iFlamesCooldown,N:<=:0][GVAR,iAttackRoll,N:<:30]{SVAR:iFlamesCooldown:N:2}{Prime:Boss|Victoria|Flashburn}{Target:Threat}{Finish} || " ..
"[GVAR,iAttackRoll,N:<:50]{Prime:Boss|Victoria|Rapier Slice}{Target:Threat}{Finish} || " ..
"{Prime:Boss|Victoria|Homing Needles}{Target:Threat}{Finish}"
AIString:new("Victoria", sString)
