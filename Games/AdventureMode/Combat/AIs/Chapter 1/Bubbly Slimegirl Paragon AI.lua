-- |[ ============================== Bubbly SlimeGirl Paragon AI =============================== ]|
--An AI that has a specific series of skills it uses, in order. If its turn is skipped for any reason
-- (Ambush, Stun) then it also skips using an ability.
--Bubbly slime girl attempts to do a magical transformation sequence via "Magical Cutie Transform" and
--then uses the magical girl skills. This is signalled with a "Magical Cutie Transform" tag.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ===================================== Combat Begins ====================================== ]|
--At combat stat, reset global variables for all Poison Vines. They share a cooldown on the cloud
-- skill so they don't all use it at once.
if(iSwitchType == gciAI_CombatStart) then
    
    -- |[Standard]|
    --Call the standard for this object.
    LM_ExecuteScript(gsRoot .. "Combat/AIs/000 Normal AI.lua", iSwitchType)
    
    -- |[Global]|
    --DataLibrary entry.
    local iUniqueID = RO_GetID()
    DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "_AI/")
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/iTurn", "N", 0.0)
    
    -- |[Debug]|
    --io.write("Palm Paragon AI initialized.\n")
    
-- |[ ====================================== Decide Action ===================================== ]|
--Decide which attack to use. Standard handling.
elseif(iSwitchType == gciAI_Decide_Action) then

    -- |[ =================== Common Variables =================== ]|
    local iUniqueID = RO_GetID()
    local iActingID = AdvCombat_GetProperty("ID of Acting Entity")
    local iCurrentTurn = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/iTurn", "N")
        
    --Check if this entity was the one acting. If so, run the turn counter. This happens even if the
    -- entity was stunned or ambushed.
    if(iUniqueID == iActingID) then
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/iTurn", "N", iCurrentTurn + 1)
    end
    
    -- |[ =============== Standard Action Handlers =============== ]|
    --This subroutine will do all the normal work of checking if this entity should handle its action,
    -- if it's ambushed, stunned, etc. If it returns false, don't call the below code.
    if(fnStandardDecideActionSetup() == false) then
        --io.write("Palm AI failed to act on turn " .. math.floor(iCurrentTurn) .. ".\n")
        return
    end
    
    -- |[Debug]|
    --io.write("Palm AI is selecting action on turn " .. math.floor(iCurrentTurn) .. ".\n")
    
    -- |[ ================== Ability Execution =================== ]|
    --Get current job
    --local sCurrentJob = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
    
    -- |[Get Tag count]|
    --The AI uses the presence of bleeding tags to decide what to do. (If bleeding tags expire will that mess with the AI?)
    
            --AdvCombat_SetProperty("Push Target", piTargetIndex)
            --this line needs to be targeting the entity, how do I make that happen?
            --local iTargetID = RO_GetID()
           -- local iTagCount = AdvCombatEntity_GetProperty("Tag Count", "Bleeding DoT")
            --iTagCount should now reflect the number of bleeding tags active this turn.
    
    
    -- |[Ability Selection]|
    --The AI uses its abilities in sequence, skipping abilities if it got stunned or otherwise
    -- was unable to act.
    --This sample uses a cycle of 5 turns. If something goes wrong, Bleed Attack is used by default.
    local sToUseAttack = "Enemy|Bleed Attack"
    local iModulusTurn = iCurrentTurn % 4
    local iCutieTagCount = AdvCombatEntity_GetProperty("Tag Count", "Magical Cutie Transform")
    
    --Create an ability listing for the case of not having the tag.
    if(iCutieTagCount < 1) then
        local saAbilityNames = {}
        table.insert(saAbilityNames, "Enemy|Vivimancys Flail")
        table.insert(saAbilityNames, "Enemy|Magical Cutie Transform")
        table.insert(saAbilityNames, "Enemy|Vivimancys Flail")
        table.insert(saAbilityNames, "Enemy|Magical Cutie Transform")
        sToUseAttack = saAbilityNames[iModulusTurn+1]
    
    --Has the tag, use offense abilities.
    else
        local saAbilityNames = {}
        table.insert(saAbilityNames, "Enemy|Slime Heart Blast")
        table.insert(saAbilityNames, "Enemy|Slime Heart Blast")
        table.insert(saAbilityNames, "Enemy|Lovely Slime Lightning")
        table.insert(saAbilityNames, "Enemy|Slime Heart Blast")
        sToUseAttack = saAbilityNames[iModulusTurn+1]
    end
    
    --Resolve the slot and set it as active.
    local iMasterSlot = AdvCombatEntity_GetProperty("Slot of Ability In Master List", sToUseAttack)
    AdvCombat_SetProperty("Set Ability As Active", iMasterSlot)
    --io.write("Palm AI activates ability in slot " .. iMasterSlot .. ".\n")
    
    -- |[Paint Targets, Execute]|
    --Function paints targets and selects a cluster by threat. If flagged, also executes the ability.
    fnPickTargetByThreat(true, true)
    
-- |[ ===================================== Default Cases ====================================== ]|
--If not otherwise specified, use the default AI.
else
    LM_ExecuteScript(gsRoot .. "Combat/AIs/000 Normal AI.lua", iSwitchType)
end