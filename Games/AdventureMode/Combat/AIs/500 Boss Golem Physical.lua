-- |[ =================================== Golem Physical AI ==================================== ]|
--Boss selects attack based on the target's resistances, picking the one they are weakest to.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ===================================== Combat Begins ====================================== ]|
--Called when combat begins, but before turn order is resolved for the 0th turn. If this AI was
-- arriving as reinforcements, this will be called at the start of the turn when they arrive before
-- turn order is resolved.
--If arriving in the middle of combat, the AI can determine they are a reinforcement by checking 
-- the turn counter. If it's 0, they were not a reinforcement.
if(iSwitchType == gciAI_CombatStart) then
    
    --Create a DataLibrary entry for this ID.
    local iUniqueID = RO_GetID()
    DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "_AI/")

-- |[ ====================================== Turn Begins ======================================= ]|
--Called when the turn begins, after turn order is resolved.
elseif(iSwitchType == gciAI_TurnStart) then

-- |[ ==================================== AI Begins Action ==================================== ]|
--Called when a new Action begins, *not necessarily the action of the owning entity*. If it is the
-- action of the owning entity, we need to decide what action to perform. Otherwise, set variables
-- if the AI requires counting actions.
elseif(iSwitchType == gciAI_ActionBegin) then

-- |[ ==================================== AI Ends Action ====================================== ]|
--Called after an action ends, before the turn order is popped.
elseif(iSwitchType == gciAI_ActionEnd) then

-- |[ ======================================= Turn Ends ======================================== ]|
--Called after the turn ends but before turn order is resolved for the next turn or reinforcements
-- arrive, if any do.
elseif(iSwitchType == gciAI_TurnEnd) then

-- |[ ==================================== AI is Defeated ====================================== ]|
--Called when this entity is KO'd. Note that an enemy can be revived (in theory) which can cause
-- this to execute each time they are KO'd.
elseif(iSwitchType == gciAI_KnockedOut) then

-- |[ ====================================== Combat Ends ======================================= ]|
--Called when combat ends but before XP/JP/etc are awarded.
elseif(iSwitchType == gciAI_CombatEnds) then

-- |[ ================================ Application Pack Before ================================= ]|
--Called right before an application pack is processed, allowing querying and modification.
elseif(iSwitchType == gciAI_Application_Start) then

-- |[ ================================ Application Pack After ================================== ]|
--Called right after an application pack is processed, allowing querying.
elseif(iSwitchType == gciAI_Application_End) then

    --Boss AI does not care about threat.
    
-- |[ ====================================== Decide Action ===================================== ]|
--Decide which attack to use. Standard handling.
elseif(iSwitchType == gciAI_Decide_Action) then

    -- |[ ==================== AI Description ==================== ]|
    --When selecting an action, pick whichever ability hits the lowest resist on the target.
    -- Follows normal threat values.
    
    -- |[ ======================== Setup ========================= ]|
    -- |[Events]|
    --Make sure the AI needs to spawn events. This script is called at the start of a turn in case
    -- the AI needs to execute effects and whatnot, but this flag is true if it needs to perform
    -- an action.
    local bCanSpawnEvent = AdvCombat_GetProperty("Is AI Spawning Events")
    if(bCanSpawnEvent == false) then return end
    
    -- |[Setup]|
    --Get variables.
    local iOwnerID = RO_GetID()
    local iActingID = AdvCombat_GetProperty("ID of Acting Entity")
    
    -- |[AI Routing]|
    --The boss can never be confused or change its script routing.

    -- |[Stun]|
    --Boss cannot be stunned.
    fnStunHandleActionBegin()

    --Get what turn we're on.
    local iTurn = VM_GetVar("Root/Variables/Combat/" .. iOwnerID .. "_AI/iTurn", "I")
    
    -- |[ ================== Target Population =================== ]|
    -- |[Populate Target by Threat]|
    --Select the zeroth ability to paint targets with. We need to pick a target and then an ability.
    local iMasterSlot = AdvCombatEntity_GetProperty("Slot of Ability In Master List", "Enemy|Sword Attack")
    AdvCombat_SetProperty("Set Ability As Active", iMasterSlot)
    local iMarkedCluster = fnPickTargetByThreat(true, false)
    
    --Get the ID of the target. Each cluster should have one entity in it.
    local iTargetID = AdvCombat_GetProperty("Target ID In Cluster", iMarkedCluster, 0)
    
    -- |[Resolve Resistances]|
    --Get this target's resistances and pick the lowest one of the available.
    local zaTable = {}
    AdvCombat_SetProperty("Push Entity By ID", iTargetID)
        zaTable[1] = {"Enemy|Sword Attack",  AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Resist_Slash)}
        zaTable[2] = {"Enemy|Strike Attack", AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Resist_Strike)}
        zaTable[3] = {"Enemy|Pierce Attack", AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Resist_Pierce)}
        zaTable[4] = {"Enemy|Flame Attack",  AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Resist_Flame)}
        zaTable[5] = {"Enemy|Freeze Attack", AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Resist_Freeze)}
    DL_PopActiveObject()
    
    --Scan for the lowest resist. Ties break towards lower indices.
    local iLowestResist = 1000
    local sAbilityToUse = "Enemy|Sword Attack"
    for i = 1, #zaTable, 1 do
        if(zaTable[i][2] < iLowestResist) then
            sAbilityToUse = zaTable[i][1]
            iLowestResist = zaTable[i][2]
        end
    end
    
    --Debug.
    --io.write("Selected attack: " .. sAbilityToUse .. " on resistance: " .. iLowestResist .. "\n")
    
    -- |[Execute Ability]|
    --Set this ability as the active one, execute it.
    iMasterSlot = AdvCombatEntity_GetProperty("Slot of Ability In Master List", sAbilityToUse)
    AdvCombat_SetProperty("Set Ability As Active", iMasterSlot)
    AdvCombat_SetProperty("Run Active Ability Target Script")
    AdvCombat_SetProperty("Set Target Cluster As Active", iMarkedCluster)
    AdvCombat_SetProperty("Mark Handled Action")
    AdvCombat_SetProperty("Run Active Ability On Active Targets")
    
-- |[ ===================================== Threat Update ====================================== ]|
--Call this whenever you need to update the threat values for this AI.
elseif(iSwitchType == gciAI_UpdateThreat) then

    --Boss AI does not care about threat.
end
