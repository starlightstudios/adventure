-- |[ ==================================== Arachnophelia AI ==================================== ]|
--This boss will mark a target and then strike them with the Shatter ability three turns later.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ===================================== Combat Begins ====================================== ]|
--Called when combat begins, but before turn order is resolved for the 0th turn. If this AI was
-- arriving as reinforcements, this will be called at the start of the turn when they arrive before
-- turn order is resolved.
--If arriving in the middle of combat, the AI can determine they are a reinforcement by checking 
-- the turn counter. If it's 0, they were not a reinforcement.
if(iSwitchType == gciAI_CombatStart) then
    
    --Create a DataLibrary entry for this ID.
    local iUniqueID = RO_GetID()
    DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "_AI/")
    
    --Turn counter.
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/iTurn", "N", 0.0)
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/sTarget", "N", "Mei")

-- |[ ====================================== Turn Begins ======================================= ]|
--Called when the turn begins, after turn order is resolved.
elseif(iSwitchType == gciAI_TurnStart) then

-- |[ ==================================== AI Begins Action ==================================== ]|
--Called when a new Action begins, *not necessarily the action of the owning entity*. If it is the
-- action of the owning entity, we need to decide what action to perform. Otherwise, set variables
-- if the AI requires counting actions.
elseif(iSwitchType == gciAI_ActionBegin) then

-- |[ ==================================== AI Ends Action ====================================== ]|
--Called after an action ends, before the turn order is popped.
elseif(iSwitchType == gciAI_ActionEnd) then

-- |[ ======================================= Turn Ends ======================================== ]|
--Called after the turn ends but before turn order is resolved for the next turn or reinforcements
-- arrive, if any do.
elseif(iSwitchType == gciAI_TurnEnd) then

-- |[ ==================================== AI is Defeated ====================================== ]|
--Called when this entity is KO'd. Note that an enemy can be revived (in theory) which can cause
-- this to execute each time they are KO'd.
elseif(iSwitchType == gciAI_KnockedOut) then

-- |[ ====================================== Combat Ends ======================================= ]|
--Called when combat ends but before XP/JP/etc are awarded.
elseif(iSwitchType == gciAI_CombatEnds) then

-- |[ ================================ Application Pack Before ================================= ]|
--Called right before an application pack is processed, allowing querying and modification.
elseif(iSwitchType == gciAI_Application_Start) then

-- |[ ================================ Application Pack After ================================== ]|
--Called right after an application pack is processed, allowing querying.
elseif(iSwitchType == gciAI_Application_End) then

    --Boss AI does not care about threat.

-- |[ ====================================== Decide Action ===================================== ]|
--Decide which attack to use. Standard handling.
elseif(iSwitchType == gciAI_Decide_Action) then

    -- |[ ============================== Setup ============================= ]|
    -- |[Events]|
    --Make sure the AI needs to spawn events. This flag will be true if so.
    local bCanSpawnEvent = AdvCombat_GetProperty("Is AI Spawning Events")
    if(bCanSpawnEvent == false) then return end
    
    -- |[AI Routing]|
    --The boss can never be confused or change its script routing.

    -- |[Description]|
    --On turn 0, the AI selects a party member and points at them. On turn 3, it then uses
    -- Shatter on them, and resets back to turn 0. KO'd party members cannot be selected.

    -- |[Check Turn]|
    --Compare the ID of the owner with the ID of the acting entity.
    local iOwnerID = RO_GetID()
    local iActingID = AdvCombat_GetProperty("ID of Acting Entity")
    if(iOwnerID ~= iActingID) then return end

    -- |[Stun]|
    --Boss cannot be stunned.
    fnStunHandleActionBegin()

    --Get what turn we're on.
    local iTurn = VM_GetVar("Root/Variables/Combat/" .. iOwnerID .. "_AI/iTurn", "I")
    
    -- |[ =========================== Turn Zero ============================ ]|
    --Mark a target, apply an effect to them.
    if(iTurn == 0.0) then
        
        --Turn flag.
        VM_SetVar("Root/Variables/Combat/" .. iOwnerID .. "_AI/iTurn", "N", 1.0)
    
        --Setup an array of possible targets.
        local iaIDArray     = {}
        local saTargetArray = {}
        local saNamesArray  = {}
        
        --Scan all party members for validity.
        local iCombatPartySize = AdvCombat_GetProperty("Combat Party Size")
        for i = 0, iCombatPartySize-1, 1 do
            AdvCombat_SetProperty("Push Combat Party Member By Slot", i)
                local iTargetID     = RO_GetID()
                local sInternalName = AdvCombatEntity_GetProperty("Internal Name")
                local sDisplayName  = AdvCombatEntity_GetProperty("Display Name")
                local iHealth       = AdvCombatEntity_GetProperty("Health")
            DL_PopActiveObject()
            
            --If HP is over zero, it's a valid target.
            if(iHealth > 0) then
                local p = #saTargetArray + 1
                iaIDArray[p]     = iTargetID
                saTargetArray[p] = sInternalName
                saNamesArray[p]  = sDisplayName
            end
        end
        
        --Select the target.
        local iUseTarget = -1
        if(#saTargetArray == 1) then
            iUseTarget = iaIDArray[1]
            VM_SetVar("Root/Variables/Combat/" .. iOwnerID .. "_AI/sTarget",     "S", saTargetArray[1])
            VM_SetVar("Root/Variables/Combat/" .. iOwnerID .. "_AI/sTargetName", "S", saNamesArray[1])
        
        --Random roll:
        else
            local iRoll = LM_GetRandomNumber(1, #saTargetArray)
            iUseTarget = iaIDArray[iRoll]
            VM_SetVar("Root/Variables/Combat/" .. iOwnerID .. "_AI/sTarget",     "S", saTargetArray[iRoll])
            VM_SetVar("Root/Variables/Combat/" .. iOwnerID .. "_AI/sTargetName", "S", saNamesArray[iRoll])
        end
    
        --Debug.
        local sTargetName = VM_GetVar("Root/Variables/Combat/" .. iOwnerID .. "_AI/sTargetName", "S")
        
        --Display.
        WD_SetProperty("Show")
        Append("[VOICE|Narrator] The creature points at " .. sTargetName .. "...") 
        
        --Get the ID of the boss' ability, Judgement.
        
        
        --Apply the effect 'Guilt' to the target.
        local sEffectString = "Effect|" .. gsGlobalEffectPrototypePath .. "|Originator:" .. iOwnerID .. "|Prototype:Enemies.Ch1.Guilt"
        AdvCombat_SetProperty("Register Application Pack", iOwnerID, iUseTarget, 0, sEffectString)
        
        --Mark the pause ability for us.
        AdvCombat_SetProperty("Set Ability As Active", gciAI_AbilityRollStart+1)
    
    -- |[ ============================ Turn One ============================ ]|
    --Displays a notice.
    elseif(iTurn == 1.0) then
        VM_SetVar("Root/Variables/Combat/" .. iOwnerID .. "_AI/iTurn", "N", 2.0)
        
        --Display.
        WD_SetProperty("Show")
        Append("[VOICE|Narrator] The creature unwinds its limbs...") 
        AdvCombat_SetProperty("Set Ability As Active", gciAI_AbilityRollStart+1)
    
    -- |[ ============================ Turn Two ============================ ]|
    --Displays a notice.
    elseif(iTurn == 2.0) then
        VM_SetVar("Root/Variables/Combat/" .. iOwnerID .. "_AI/iTurn", "N", 3.0)
        
        --Display.
        WD_SetProperty("Show")
        Append("[VOICE|Narrator] The creature prepares to strike!") 
        AdvCombat_SetProperty("Set Ability As Active", gciAI_AbilityRollStart+1)
    
    -- |[ =========================== Turn Three =========================== ]|
    --Use the 'Judgement' attack.
    elseif(iTurn == 3.0) then
    
        --Reset flag.
        VM_SetVar("Root/Variables/Combat/" .. iOwnerID .. "_AI/iTurn", "N", 0.0)
        
        --Get target.
        local sTarget = VM_GetVar("Root/Variables/Combat/" .. iOwnerID .. "_AI/sTarget", "S")
        
        --Set ability.
        AdvCombat_SetProperty("Set Ability As Active", gciAI_AbilityRollStart)
        
        --Paint targets.
        AdvCombat_SetProperty("Run Active Ability Target Script")
        
        --Get target clusters.
        local iMarkedCluster = -1
        local iTotalTargetClusters = AdvCombat_GetProperty("Total Target Clusters")
        
        --Scan all the painted clusters:
        for i = 0, iTotalTargetClusters-1, 1 do
            
            --For each target in cluster (should be one):
            local iTotalTargets = AdvCombat_GetProperty("Total Targets In Cluster", i)
            for p = 0, iTotalTargets-1, 1 do
                
                --Get the ID. Get the name. Compare.
                local iTargetID = AdvCombat_GetProperty("Target ID In Cluster", i, p)
                AdvCombat_SetProperty("Push Entity By ID", iTargetID)
                    local sInternalName = AdvCombatEntity_GetProperty("Internal Name")
                DL_PopActiveObject()
                
                --Match. Target this cluster.
                if(sInternalName == sTarget) then
                    iMarkedCluster = i
                    AdvCombat_SetProperty("Set Target Cluster As Active", iMarkedCluster)
                    break
                end
            end
            
            --If the cluster was marked, break.
            if(iMarkedCluster ~= -1) then break end
        end
    end
    
    --Run the ability.
    AdvCombat_SetProperty("Mark Handled Action")
    AdvCombat_SetProperty("Run Active Ability On Active Targets")
    
-- |[ ===================================== Threat Update ====================================== ]|
--Call this whenever you need to update the threat values for this AI.
elseif(iSwitchType == gciAI_UpdateThreat) then

    --Boss AI does not care about threat.
end
