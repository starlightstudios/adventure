-- |[ ======================================= Poison Vine AI ======================================== ]|
--Same as the normal AI, except it makes a set of decisions in battle about which skills to use when.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ===================================== Combat Begins ====================================== ]|
--At combat stat, reset global variables for all Poison Vines. They share a cooldown on the cloud
-- skill so they don't all use it at once.
if(iSwitchType == gciAI_CombatStart) then
    
    -- |[Standard]|
    --Call the standard for this object.
    LM_ExecuteScript(gsRoot .. "Combat/AIs/000 Normal AI.lua", iSwitchType)
    
    -- |[Global]|
    --If this entry doesn't exist, create it. Otherwise, reset it to 0.
    DL_AddPath("Root/Variables/Combat/PoisonVineGlobalAI/")
    VM_SetVar("Root/Variables/Combat/PoisonVineGlobalAI/iGlobalCloudLastUsed", "N", -100.0)
    
    -- |[Debug]|
    --io.write("Poison Vine AI initialized.\n")

-- |[ ====================================== Decide Action ===================================== ]|
--Decide which attack to use. Standard handling.
elseif(iSwitchType == gciAI_Decide_Action) then

    -- |[ =============== Standard Action Handlers =============== ]|
    --This subroutine will do all the normal work of checking if this entity should handle its action,
    -- if it's ambushed, stunned, etc. If it returns false, don't call the below code.
    if(fnStandardDecideActionSetup() == false) then return end
    
    -- |[Debug]|
    --io.write("Poison Vine AI is selecting action.\n")
    
    -- |[ ================== Ability Execution =================== ]|
    -- |[Ability Selection]|
    --Poison vines will always use their cloud ability when it is off its 5-turn global cooldown. Otherwise, they will
    -- attack using their basic poison attack, or a poison spine.
    --Check if the cloud is available for use.
    local iGlobalCloudLastUsed = VM_GetVar("Root/Variables/Combat/PoisonVineGlobalAI/iGlobalCloudLastUsed", "N")
    local iCurrentTurn = AdvCombat_GetProperty("Current Turn")
    local ciCooldown = 5
    if(iCurrentTurn - iGlobalCloudLastUsed >= ciCooldown) then
    
        --Get, set, activate.
        local iMasterSlot = AdvCombatEntity_GetProperty("Slot of Ability In Master List", "Enemy|Poison Cloud")
        AdvCombat_SetProperty("Set Ability As Active", iMasterSlot)
        
        --Set the cooldown back to 5.
        VM_SetVar("Root/Variables/Combat/PoisonVineGlobalAI/iGlobalCloudLastUsed", "N", iCurrentTurn)
    
    -- Otherwise, pick poison spine or poison attack at random.
    else
    
        --Roll for which to pick.
        local iRoll = LM_GetRandomNumber(1, 150)
        if(iRoll < 100) then
            local iMasterSlot = AdvCombatEntity_GetProperty("Slot of Ability In Master List", "Enemy|Poison Attack")
            AdvCombat_SetProperty("Set Ability As Active", iMasterSlot)
        else
            local iMasterSlot = AdvCombatEntity_GetProperty("Slot of Ability In Master List", "Enemy|Poison Spine")
            AdvCombat_SetProperty("Set Ability As Active", iMasterSlot)
        end
    end
    
    -- |[Paint Targets, Execute]|
    --Function paints targets and selects a cluster by threat. If flagged, also executes the ability.
    fnPickTargetByThreat(true, true)
    
-- |[ ===================================== Default Cases ====================================== ]|
--If not otherwise specified, use the default AI.
else
    LM_ExecuteScript(gsRoot .. "Combat/AIs/000 Normal AI.lua", iSwitchType)
end
