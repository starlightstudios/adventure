-- |[ ====================================== Harpy Officer ===================================== ]|
--Same as the normal AI, except it makes a set of decisions in battle about which skills to use when.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ===================================== Combat Begins ====================================== ]|
--At combat stat, reset global variables for all Poison Vines. They share a cooldown on the cloud
-- skill so they don't all use it at once.
if(iSwitchType == gciAI_CombatStart) then
    
    -- |[Standard]|
    --Call the standard for this object.
    LM_ExecuteScript(gsRoot .. "Combat/AIs/000 Normal AI.lua", iSwitchType)
    
-- |[ ====================================== Decide Action ===================================== ]|
--Decide which attack to use. Standard handling.
elseif(iSwitchType == gciAI_Decide_Action) then

    -- |[ =============== Standard Action Handlers =============== ]|
    --This subroutine will do all the normal work of checking if this entity should handle its action,
    -- if it's ambushed, stunned, etc. If it returns false, don't call the below code.
    if(fnStandardDecideActionSetup() == false) then return end
    
    -- |[ ================== Ability Execution =================== ]|
    -- |[Setup]|
    local iUniqueID = RO_GetID()
    
    -- |[Ability Selection]|
    --Harpy Officers have a 90% chance to use Battle Orders on a random Harpy Recruit ally if one is
    -- present and does not already have Battle Orders on them. Otherwise they use their basic attack.
    local iaBattleOrdersCandidates = {}
    
    -- |[List Potential Buff Targets]|
    --Create a list of candidates for Battle Orders.
    local iTotalEnemies = AdvCombat_GetProperty("Enemy Party Size")
    for i = 0, iTotalEnemies-1, 1 do
        
        --Get the ID of this enemy.
        local iCurrentID = AdvCombat_GetProperty("Enemy ID", i)
        
        --Push the enemy.
        AdvCombat_SetProperty("Push Entity By ID", iCurrentID)
        
            --Check if they have the tag that lets them take a Battle Orders buff.
            local iCanTakeOrdersTag = AdvCombatEntity_GetProperty("Tag Count", "Can Take Battle Orders")

            --Also check if they already have the Battle Orders effect.
            local iHasOrdersTag = AdvCombatEntity_GetProperty("Tag Count", "Battle Orders")

        DL_PopActiveObject()
        
        --If they have the Can Take Orders tag, and no Has Orders tag, then add them to the list.
        if(iCanTakeOrdersTag > 0 and iHasOrdersTag < 1) then
            table.insert(iaBattleOrdersCandidates, iCurrentID)
        end
    end
    
    -- |[No Entries]|
    --If the list contains no entries, perform the normal attack.
    if(#iaBattleOrdersCandidates < 1) then
        AdvCombat_SetProperty("Set Ability As Active Name", "Enemy|Sword Attack")
        fnPickTargetByThreat(true, true)
        return
    
    -- |[Valid Entries]|
    --Otherwise, there's a 90% chance to use Battle Orders on a valid target.
    else
    
        --Roll.
        local iRoll = LM_GetRandomNumber(1, 100)
    
        --Use the normal attack anyway.
        if(iRoll < 10) then
            AdvCombat_SetProperty("Set Ability As Active Name", "Enemy|Sword Attack")
            fnPickTargetByThreat(true, true)
            return
    
        --Pick a Battle Orders target and use it on them.
        else
        
            --Activate and run targeting.
            AdvCombat_SetProperty("Set Ability As Active Name", "Enemy|Battle Orders")
            AdvCombat_SetProperty("Run Active Ability Target Script")
            
            --Select one of the IDs that is valid.
            local iSlot = LM_GetRandomNumber(1, #iaBattleOrdersCandidates)
            local iTargetID = iaBattleOrdersCandidates[iSlot]
            
            --Get the cluster it is in.
            local iCluster = fnPreferentialID(iTargetID)
            AdvCombat_SetProperty("Set Target Cluster As Active", iCluster)
        
            --Run the ability.
            AdvCombat_SetProperty("Mark Handled Action")
            AdvCombat_SetProperty("Run Active Ability On Active Targets")
        end
    end
    
-- |[ ===================================== Default Cases ====================================== ]|
--If not otherwise specified, use the default AI.
else
    LM_ExecuteScript(gsRoot .. "Combat/AIs/000 Normal AI.lua", iSwitchType)
end
