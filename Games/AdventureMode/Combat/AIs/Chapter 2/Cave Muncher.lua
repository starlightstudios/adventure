-- |[ ==================================== Cave Muncher AI ===================================== ]|
--Same as the normal AI, except it makes a set of decisions in battle about which skills to use when.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ===================================== Combat Begins ====================================== ]|
--At combat stat, reset global variables for all Poison Vines. They share a cooldown on the cloud
-- skill so they don't all use it at once.
if(iSwitchType == gciAI_CombatStart) then
    
    -- |[Standard]|
    --Call the standard for this object.
    LM_ExecuteScript(gsRoot .. "Combat/AIs/000 Normal AI.lua", iSwitchType)
    
    -- |[Variables]|
    local iUniqueID = RO_GetID()
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/iHasUsedArmorRot", "N", 0)
    
-- |[ ====================================== Decide Action ===================================== ]|
--Decide which attack to use. Standard handling.
elseif(iSwitchType == gciAI_Decide_Action) then

    -- |[ =============== Standard Action Handlers =============== ]|
    --This subroutine will do all the normal work of checking if this entity should handle its action,
    -- if it's ambushed, stunned, etc. If it returns false, don't call the below code.
    if(fnStandardDecideActionSetup() == false) then return end
    
    -- |[ ================== Ability Execution =================== ]|
    -- |[Setup]|
    local iUniqueID = RO_GetID()
    
    -- |[Ability Selection]|
    --Cave Munchers have a chance to use Armor Rot, but can only use it once per battle. After that they
    -- only use their corroding attack.
    local iHasUsedArmorRot = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/iHasUsedArmorRot", "N")
    if(iHasUsedArmorRot == 0.0) then
        
        --Roll.
        local iRoll = LM_GetRandomNumber(1, 100)
        if(iRoll < 50) then
            AdvCombat_SetProperty("Set Ability As Active Name", "Enemy|Armor Rot")
            VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/iHasUsedArmorRot", "N", 1.0)
        else
            AdvCombat_SetProperty("Set Ability As Active Name", "Enemy|Corrode Attack")
        end
    
    --Already used, just use the corrode attack.
    else
        AdvCombat_SetProperty("Set Ability As Active Name", "Enemy|Corrode Attack")
    end
    
    -- |[Paint Targets, Execute]|
    --Function paints targets and selects a cluster by threat. If flagged, also executes the ability.
    fnPickTargetByThreat(true, true)
    
-- |[ ===================================== Default Cases ====================================== ]|
--If not otherwise specified, use the default AI.
else
    LM_ExecuteScript(gsRoot .. "Combat/AIs/000 Normal AI.lua", iSwitchType)
end
