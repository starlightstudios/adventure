-- |[ ===================================== Bunny Smuggler ===================================== ]|
--Same as the normal AI, except it makes a set of decisions in battle about which skills to use when.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ===================================== Combat Begins ====================================== ]|
--At combat stat, reset global variables for all Poison Vines. They share a cooldown on the cloud
-- skill so they don't all use it at once.
if(iSwitchType == gciAI_CombatStart) then
    
    -- |[Standard]|
    --Call the standard for this object.
    LM_ExecuteScript(gsRoot .. "Combat/AIs/000 Normal AI.lua", iSwitchType)
    
    -- |[Variables]|
    local iUniqueID = RO_GetID()
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/iHasUsedBurstOfSpeed", "N", 0)
    
-- |[ ====================================== Decide Action ===================================== ]|
--Decide which attack to use. Standard handling.
elseif(iSwitchType == gciAI_Decide_Action) then

    -- |[ =============== Standard Action Handlers =============== ]|
    --This subroutine will do all the normal work of checking if this entity should handle its action,
    -- if it's ambushed, stunned, etc. If it returns false, don't call the below code.
    if(fnStandardDecideActionSetup() == false) then return end
    
    -- |[ ================== Ability Execution =================== ]|
    -- |[Setup]|
    local iUniqueID = RO_GetID()
    
    -- |[Ability Selection]|
    --The Smuggler starts with a 20% chance to use Burst of Speed. It can be used once per battle.
    -- The chance increases linearly to 90% as their HP drops.
    local iHasUsedBurstOfSpeed = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/iHasUsedBurstOfSpeed", "N")
    
    --If we've already used the skill, just use a normal attack.
    if(iHasUsedBurstOfSpeed == 1.0) then
        AdvCombat_SetProperty("Set Ability As Active Name", "Enemy|Sword Attack")
        fnPickTargetByThreat(true, true)
        return
    end
    
    -- |[Burst Roll]|
    --Compute the chance to use Burst of Speed.
    local iHealth    = AdvCombatEntity_GetProperty("Health")
    local iHealthMax = AdvCombatEntity_GetProperty("Health Max")
    local fHPPct = iHealth / iHealthMax
    local iBurstRoll = math.floor(20 + (80 * (1.0 - fHPPct)))
    
    --Roll to use burst:
    local iRoll = LM_GetRandomNumber(1, 100)
    if(iRoll < iBurstRoll) then
        
        --Set.
        AdvCombat_SetProperty("Set Ability As Active Name", "Enemy|Burst Of Speed")
        fnPickTargetByThreat(true, true)
    
        --Set flag so the ability can't be used again.
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/iHasUsedBurstOfSpeed", "N", 1.0)
    
    --Normal attack:
    else
        AdvCombat_SetProperty("Set Ability As Active Name", "Enemy|Sword Attack")
        fnPickTargetByThreat(true, true)
        return
    end
    
-- |[ ===================================== Default Cases ====================================== ]|
--If not otherwise specified, use the default AI.
else
    LM_ExecuteScript(gsRoot .. "Combat/AIs/000 Normal AI.lua", iSwitchType)
end
