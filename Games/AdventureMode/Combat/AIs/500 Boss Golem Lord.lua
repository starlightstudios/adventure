-- |[ ====================================== Golem Lord AI ===================================== ]|
--Standard AI, but adds/removes effects based on combat situation.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ===================================== Combat Begins ====================================== ]|
--Called when combat begins, but before turn order is resolved for the 0th turn. If this AI was
-- arriving as reinforcements, this will be called at the start of the turn when they arrive before
-- turn order is resolved.
--If arriving in the middle of combat, the AI can determine they are a reinforcement by checking 
-- the turn counter. If it's 0, they were not a reinforcement.
if(iSwitchType == gciAI_CombatStart) then
    
    --Create a DataLibrary entry for this ID.
    local iUniqueID = RO_GetID()
    DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "_AI/")

-- |[ ====================================== Turn Begins ======================================= ]|
--Called when the turn begins, after turn order is resolved.
elseif(iSwitchType == gciAI_TurnStart) then

-- |[ ==================================== AI Begins Action ==================================== ]|
--This AI toggles on and off debuffs based on whether or not she is blinded.
elseif(iSwitchType == gciAI_ActionBegin) then
    
    -- |[ ========================= Setup ======================== ]|
    --Get variables.
    local iOwnerID = RO_GetID()
    local iActingID = AdvCombat_GetProperty("ID of Acting Entity")
    
    -- |[ ======================== Threat ======================== ]|
    --Even if this is not the AI's turn, update its threat effects. This is to be sure the player
    -- has accurate values.
    LM_ExecuteScript(LM_GetCallStack(0), gciAI_UpdateThreat)
    
    -- |[ ==================== Effect Spawning =================== ]|
    -- |[Delicate Optics]|
    --This effect is applied once at combat start.
    AdvCombat_SetProperty("Push Entity By ID", iOwnerID)
        local iOpticsTagCount = AdvCombatEntity_GetProperty("Tag Count", "Delicate Optics Effect")
    DL_PopActiveObject()
    if(iOpticsTagCount < 1) then
        local sEffectPath = gsRoot .. "Combat/Enemies/Effects/Chapter 5 Bosses/Delicate Optics.lua"
        AdvCombat_SetProperty("Register Application Pack", iOwnerID, iOwnerID, 0, "Effect|" .. sEffectPath .. "|Originator:" .. iOwnerID)
    end
    
    -- |[Steady]|
    --Check if we are under the buff effect. This is not only done on the owner's turn.
    AdvCombat_SetProperty("Push Entity By ID", iOwnerID)
        local iBuffTagCount = AdvCombatEntity_GetProperty("Tag Count", "Boss Steady Buff")
    DL_PopActiveObject()
    
    --Under buff effect:
    if(iBuffTagCount > 0) then
        
        --Check if we're blinded.
        AdvCombat_SetProperty("Push Entity By ID", iOwnerID)
            local iAccuracyMod = AdvCombatEntity_GetProperty("Statistic", gciStatGroup_TempEffect, gciStatIndex_Accuracy)
        DL_PopActiveObject()

        --If the accuracy mod is lower than 0, a blind is in effect. Remove the effect.
        if(iAccuracyMod < 0) then
            
            --Create a list of all matching effects. Should be one.
            local iaEffectIDList = {}
            local iTotalEffects = AdvCombatEntity_GetProperty("Effects With Tag", "Boss Steady Buff")
            for i = 0, iTotalEffects-1, 1 do
                local iEffectID = AdvCombatEntity_GetProperty("Effect ID With Tag", "Boss Steady Buff", i)
                iaEffectIDList[i+1] = iEffectID
            end
            
            --Remove them all at once to preserve list stability.
            for i = 1, #iaEffectIDList, 1 do
                AdvCombat_SetProperty("Remove Effect", iaEffectIDList[i])
            end
        end

    --Not under buff effect:
    else
        
        --Check if we're blinded.
        AdvCombat_SetProperty("Push Entity By ID", iOwnerID)
            local iAccuracyMod = AdvCombatEntity_GetProperty("Statistic", gciStatGroup_TempEffect, gciStatIndex_Accuracy)
        DL_PopActiveObject()
        
        --If not blinded, register the effect.
        if(iAccuracyMod >= 0) then
            local sEffectPath = gsRoot .. "Combat/Enemies/Effects/Chapter 5 Bosses/Steady.lua"
            AdvCombat_SetProperty("Register Application Pack", iOwnerID, iOwnerID, 0, "Effect|" .. sEffectPath .. "|Originator:" .. iOwnerID)
        end
    end

-- |[ ==================================== AI Ends Action ====================================== ]|
--Called after an action ends, before the turn order is popped.
elseif(iSwitchType == gciAI_ActionEnd) then

-- |[ ======================================= Turn Ends ======================================== ]|
--Called after the turn ends but before turn order is resolved for the next turn or reinforcements
-- arrive, if any do.
elseif(iSwitchType == gciAI_TurnEnd) then

-- |[ ==================================== AI is Defeated ====================================== ]|
--Called when this entity is KO'd. Note that an enemy can be revived (in theory) which can cause
-- this to execute each time they are KO'd.
elseif(iSwitchType == gciAI_KnockedOut) then

-- |[ ====================================== Combat Ends ======================================= ]|
--Called when combat ends but before XP/JP/etc are awarded.
elseif(iSwitchType == gciAI_CombatEnds) then

-- |[ ================================ Application Pack Before ================================= ]|
--Called right before an application pack is processed, allowing querying and modification.
elseif(iSwitchType == gciAI_Application_Start) then

-- |[ ================================ Application Pack After ================================== ]|
--Called right after an application pack is processed, allowing querying.
elseif(iSwitchType == gciAI_Application_End) then

    --Boss AI does not care about threat.
    
-- |[ ====================================== Decide Action ===================================== ]|
--Decide which attack to use. Standard handling.
elseif(iSwitchType == gciAI_Decide_Action) then

    -- |[ ==================== AI Description ==================== ]|
    --Attacks by usual threat rolling.

    -- |[ ==================== Pre-Exec Setup ==================== ]|
    -- |[Events]|
    --Make sure the AI needs to spawn events. This flag is only true when the AI needs to spawn events.
    local bCanSpawnEvent = AdvCombat_GetProperty("Is AI Spawning Events")
    if(bCanSpawnEvent == false) then return end
    
    -- |[Setup]|
    --Get variables.
    local iOwnerID = RO_GetID()
    local iActingID = AdvCombat_GetProperty("ID of Acting Entity")
    
    -- |[Roll]|
    --Select.
    fnPickAbilityByRoll(true)
    fnPickTargetByThreat(true, true)
    
-- |[ ===================================== Threat Update ====================================== ]|
--Call this whenever you need to update the threat values for this AI.
elseif(iSwitchType == gciAI_UpdateThreat) then

    --Boss AI does not care about threat.
end
