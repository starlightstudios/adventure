-- |[ ======================================= Normal AI ======================================== ]|
--The Normal AI, selects abilities at random based on the weight of the ability. It then selects
-- targets at random based on their internal "Threat" rating. This AI is not specific to a given
-- enemy or chapter.
--This AI also has built-in capabilities to become Fascinated/Confused/etc. This is handled via tags.
--The AdvCombatEntity that owns the AI is the active object when executing this script.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")
    
-- |[ ===================================== Combat Begins ====================================== ]|
--Called when combat begins, but before turn order is resolved for the 0th turn. If this AI was
-- arriving as reinforcements, this will be called at the start of the turn when they arrive before
-- turn order is resolved.
--If arriving in the middle of combat, the AI can determine they are a reinforcement by checking 
-- the turn counter. If it's 0, they were not a reinforcement.
if(iSwitchType == gciAI_CombatStart) then
    
    --Create a DataLibrary entry for this ID.
    local iUniqueID = RO_GetID()
    DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "_AI/")
    
    --By default, all AIs have the internal iTurn and iAction variables. iTurn is incremented at
    -- the start of the AI's turn, iAction is incremented if the AI takes an action (not if stunned).
    --Note that these start at -1, they tick to 0 right before the AI makes its action.
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/iTurn",   "N", -1.0)
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/iAction", "N", -1.0)
    
    --Upload animation lookup if tagged.
    local iAnimTagCount = AdvCombatEntity_GetProperty("Tag Count", "Animation Tag")
    if(gsAIAnimationScripts[iAnimTagCount] ~= nil) then
        LM_ExecuteScript(gsAIAnimationScripts[iAnimTagCount], gciAIAnimUpdate_Initialize)
    end

-- |[ ====================================== Turn Begins ======================================= ]|
--Called when the turn begins, after turn order is resolved.
elseif(iSwitchType == gciAI_TurnStart) then

-- |[ ==================================== AI Begins Action ==================================== ]|
--Called when a new Action begins, *not necessarily the action of the owning entity*. If it is the
-- action of the owning entity, we need to decide what action to perform. Otherwise, set variables
-- if the AI requires counting actions.
elseif(iSwitchType == gciAI_ActionBegin or iSwitchType == gciAI_FreeActionBegin) then

    -- |[Threat Update]|
    --Even if this is not the AI's turn, update its threat effects. This is to be sure the player
    -- has accurate values.
    LM_ExecuteScript(LM_GetCallStack(0), gciAI_UpdateThreat)
    
    -- |[Passive Effects]|
    --Make sure all passives that should effect this entity are present.
    LM_ExecuteScript(fnResolvePath() .. "099 Passive Effects.lua", RO_GetID())

-- |[ ==================================== AI Ends Action ====================================== ]|
--Called after an action ends, before the turn order is popped.
elseif(iSwitchType == gciAI_ActionEnd) then

-- |[ ======================================= Turn Ends ======================================== ]|
--Called after the turn ends but before turn order is resolved for the next turn or reinforcements
-- arrive, if any do.
elseif(iSwitchType == gciAI_TurnEnd) then

-- |[ ==================================== AI is Defeated ====================================== ]|
--Called when this entity is KO'd. Note that an enemy can be revived (in theory) which can cause
-- this to execute each time they are KO'd.
elseif(iSwitchType == gciAI_KnockedOut) then

-- |[ ====================================== Combat Ends ======================================= ]|
--Called when combat ends but before XP/JP/etc are awarded.
elseif(iSwitchType == gciAI_CombatEnds) then

    --Handler.
    local iResolutionCode = AdvCombat_GetProperty("Get Resolution Type")
    if(iResolutionCode ~= gciAC_Resolution_Victory) then return end

    --Variables.
    local sKOTrackerPath = VM_GetVar("Root/Variables/Global/Combat/sKOTrackerPath", "S")
    local sInternalName = AdvCombatEntity_GetProperty("Internal Name")
    local sClusterName = AdvCombatEntity_GetProperty("Cluster Name")
    local iKOsSoFar = VM_GetVar(sKOTrackerPath .. sClusterName, "I")
    
    --Debug.
    if(false) then
        io.write("AI runs combat end.\n")
        io.write(" Resolution Type: " .. iResolutionCode .. "\n")
        io.write(" Internal Name: " .. sInternalName .. "\n")
        io.write(" Cluster Name: " .. sClusterName .. "\n")
        io.write(" Tracker Path: " .. sKOTrackerPath .. sClusterName .. "\n")
        io.write(" KOs So Far: " .. iKOsSoFar .. "\n")
    end
    
    --Add one KO.
    VM_SetVar(sKOTrackerPath .. sClusterName, "N", iKOsSoFar + 1)
    
    --Special: Award achievements for specific enemy names.
    fnCheckDefeatedEnemyAchievements(sClusterName)

-- |[ ================================ Application Pack Before ================================= ]|
--Called right before an application pack is processed, allowing querying and modification.
elseif(iSwitchType == gciAI_Application_Start) then

-- |[ ================================ Application Pack After ================================== ]|
--Called right after an application pack is processed, allowing querying.
elseif(iSwitchType == gciAI_Application_End) then

    -- |[ ======= AI Routing ======= ]|
    --This subroutine will return which AI should control the AI. It will return "Normal" if there
    -- are no tags present or the roll failed.
    local sAIResult = fnCheckAITags()

    --Switch AIs.
    if(sAIResult == "Fascinate") then
    elseif(sAIResult == "Confound") then
    elseif(sAIResult == "Berserk") then
    elseif(sAIResult == "Confuse") then
        LM_ExecuteScript(gsRoot .. "Combat/AIs/001 Confused AI.lua", gciAI_Application_End)
        return
    elseif(sAIResult == "Charm") then
    end

    -- |[Description]|
    --After an application ends, this AI computes how much threat the application generated and
    -- applies it to the relevant entities. In general, the AI provides higher threat when targeted
    -- itself and generates less threat when an ally is hit.

    -- |[ ========== Setup ========== ]|
    --Get Variables.
    local iUniqueID = RO_GetID()
    local iOriginatorID = AdvCombat_GetProperty("Application Get Originator ID")
    local iTargetID     = AdvCombat_GetProperty("Application Get Target ID")
    local sAppString    = AdvCombat_GetProperty("Application Get String")
    
    --Get the party we're in, versus the originator party.
    local iAIParty         = AdvCombat_GetProperty("Party Of ID", iUniqueID)
    local iTargetParty     = AdvCombat_GetProperty("Party Of ID", iTargetID)
    local iOriginatorParty = AdvCombat_GetProperty("Party Of ID", iOriginatorID)
    if(iOriginatorID == 0) then return end
    
    -- |[Threat Multiplier]|
    --Push the entity. We need their threat multiplier.
    AdvCombat_SetProperty("Push Entity By ID", iOriginatorID)
        local iThreatMultiplier = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_ThreatMultiplier)
    DL_PopActiveObject()
        
    --If the threat multiplier is 0, do nothing.
    if(iThreatMultiplier <= 0.0) then return end
    
    -- |[Subdivision]|
    --Break the string into chunks. This is the same logic used by the C++ code.
    local saArray = fnSubdivide(sAppString, "|")
    if(saArray[1] == nil or saArray[1] == "") then return end
    
    --Debug:
    --io.write("Printing AI threat application string: ")
    --for i = 1, #saArray, 1 do
    --    io.write(saArray[i] .. " ")
    --end
    --io.write("\n")
    
    -- |[ ========== We Are The Target ========== ]|
    --If the target is this AI's entity:
    if(iTargetID == iUniqueID) then
    
        --If the action came from the other party:
        if(iAIParty ~= iOriginatorParty) then
                
            --Push the entity. We need their threat multiplier.
            AdvCombat_SetProperty("Push Entity By ID", iOriginatorID)
                local iThreatMultiplier = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_ThreatMultiplier)
            DL_PopActiveObject()
                
            --If the threat multiplier is 0, do nothing.
            if(iThreatMultiplier <= 0.0) then return end
    
            --Is this a damage-type application:
            if(saArray[1] == "Damage") then
                
                --Resolve the threat value to inflict. First, get the damage.
                local iThreat = tonumber(saArray[2])
                
                --Multiply by the threat multiplier. Multiplier is a percent.
                iThreat = iThreat * (iThreatMultiplier / 100.0) * gcfThreat_DamageMe
                
                --Get the current threat. It comes back zero if this is the first time it was tracked.
                local iOriginatorThreat = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/" .. iOriginatorID.. "_Threat", "I")
                iOriginatorThreat = iOriginatorThreat + iThreat
                
                --Store.
                VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/" .. iOriginatorID.. "_Threat", "N", iOriginatorThreat)
            
            --Application of an effect:
            elseif(saArray[1] == "Effect") then
            
                --Get the originator's attack power. Effects are considered to emulate that amount of "damage".
                AdvCombat_SetProperty("Push Entity By ID", iOriginatorID)
                    local iThreat = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
                DL_PopActiveObject()
            
                --Multiply by the threat multiplier. Multiplier is a percent.
                iThreat = iThreat * (iThreatMultiplier / 100.0) * gcfThreat_EffectMe
                
                --Get the current threat. It comes back zero if this is the first time it was tracked.
                local iOriginatorThreat = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/" .. iOriginatorID.. "_Threat", "I")
                iOriginatorThreat = iOriginatorThreat + iThreat
                
                --Store.
                VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/" .. iOriginatorID.. "_Threat", "N", iOriginatorThreat)
            
            --Special:
            elseif(saArray[1] == "AI_APPLICATION" and #saArray >= 2) then
            
                --If the second slot is "Threat", this is an application pack that just applies threat and nothing else.
                if(saArray[2] == "Threat" and #saArray >= 3) then
            
                    --Third argument is a flat amount of threat to apply. If a multiplier is desired it needs to be applied before this point.
                    local iThreatApply = tonumber(saArray[3])
                    if(iThreatApply > 0.0) then
                        
                        --Get the current threat. It comes back zero if this is the first time it was tracked.
                        local iOriginatorThreat = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/" .. iOriginatorID.. "_Threat", "I")
                        iOriginatorThreat = iOriginatorThreat + iThreatApply
                        
                        --Store.
                        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/" .. iOriginatorID.. "_Threat", "N", iOriginatorThreat)
                    end
                end
            end
    
        --Came from the same party:
        else
    
        end

    -- |[ ========== We Are Not The Target ========== ]|
    --If the target is not this entity:
    else
        
        --Debug.
        --io.write("We are not the target.\n")
        --io.write(" Our party: " .. iAIParty .. "\n")
        --io.write(" Target party: " .. iTargetParty .. "\n")
        --io.write(" Originator party: " .. iOriginatorParty .. "\n")
        
        -- |[Action Against Ally by Enemy]|
        --If the target is in the same party as us, and the originator is not:
        if(iAIParty == iTargetParty and iAIParty ~= iOriginatorParty) then

            --Is this a damage-type application:
            if(saArray[1] == "Damage") then
                
                --Resolve the threat value to inflict. First, get the damage.
                local iThreat = tonumber(saArray[2])
                
                --Multiply by the threat multiplier. Multiplier is a percent.
                iThreat = iThreat * (iThreatMultiplier / 100.0) * gcfThreat_DamageAlly
                
                --Get the current threat. It comes back zero if this is the first time it was tracked.
                local iOriginatorThreat = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/" .. iOriginatorID.. "_Threat", "I")
                iOriginatorThreat = iOriginatorThreat + iThreat
                
                --Store.
                VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/" .. iOriginatorID.. "_Threat", "N", iOriginatorThreat)
            
            --Application of an effect:
            elseif(saArray[1] == "Effect") then
            
                --Get the originator's attack power. Effects are considered to emulate that amount of "damage".
                AdvCombat_SetProperty("Push Entity By ID", iOriginatorID)
                    local iThreat = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
                DL_PopActiveObject()
            
                --Multiply by the threat multiplier. Multiplier is a percent.
                iThreat = iThreat * (iThreatMultiplier / 100.0) * gcfThreat_EffectAlly
                
                --Get the current threat. It comes back zero if this is the first time it was tracked.
                local iOriginatorThreat = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/" .. iOriginatorID.. "_Threat", "I")
                iOriginatorThreat = iOriginatorThreat + iThreat
                
                --Store.
                VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/" .. iOriginatorID.. "_Threat", "N", iOriginatorThreat)
            end

        -- |[Action Against Ally by Ally]|
        --If the target is in the same party as us, and the originator is as well:
        elseif(iAIParty == iTargetParty and iAIParty == iOriginatorParty) then

        -- |[Action Against Enemy by Enemy]|
        --If the target is not in the same party as us, and the originator is not:
        elseif(iAIParty ~= iTargetParty and iAIParty ~= iOriginatorParty) then

            --Is this a healing-type application:
            if(saArray[1] == "Healing") then
                
                --Resolve the threat value to inflict. First, get the healing.
                local iHealing = tonumber(saArray[2])
                --io.write("  Healing case. Healing value: " .. iHealing .. "\n")
                
                --Multiply by the threat multiplier. Multiplier is a percent.
                iHealing = iHealing * (iThreatMultiplier / 100.0) * gcfThreat_Heal
                --io.write("  Healing after threat multiplier: " .. iHealing .. "\n")
                
                --Get the current threat. It comes back zero if this is the first time it was tracked.
                local iOriginatorThreat = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/" .. iOriginatorID.. "_Threat", "I")
                --io.write("  Threat before healing: " .. iOriginatorThreat .. "\n")
                iOriginatorThreat = iOriginatorThreat + iHealing
                
                --Store.
                VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/" .. iOriginatorID.. "_Threat", "N", iOriginatorThreat)
                
                --Debug.
                iOriginatorThreat = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/" .. iOriginatorID.. "_Threat", "I")
                --io.write("  Threat after healing: " .. iOriginatorThreat .. "\n")
            end

        -- |[Action Against Enemy by Ally]|
        --If the target is not in the same party as us, and the originator is:
        else

        end
    end
    
-- |[ ====================================== Decide Action ===================================== ]|
--Decide which attack to use. Standard handling.
elseif(iSwitchType == gciAI_Decide_Action) then

    -- |[ ==================== Pre-Exec Setup ==================== ]|
    -- |[Diagnostics]|
    local bDiagnostics = Debug_GetFlag("AIGeneral: All") or Debug_GetFlag("AIGeneral: Decide Action")
    Debug_PushPrint(bDiagnostics, "AI script runs, deciding action.\n")
    
    -- |[Events]|
    --Make sure the AI needs to spawn events. This flag is only true when the AI needs to spawn events.
    local bCanSpawnEvent = AdvCombat_GetProperty("Is AI Spawning Events")
    if(bCanSpawnEvent == false) then
        Debug_PopPrint("AI is not spawning events.\n")
        return
    end
    local iUniqueID = RO_GetID()
    
    -- |[AI Routing]|
    --This subroutine will return which AI should control the AI. It will return "Normal" if there
    -- are no tags present or the roll failed.
    local sAIResult = fnCheckAITags()
    
    --AI is Ambushed. Causes them to waste a turn.
    if(AdvCombatEntity_GetProperty("Is Ambushed") == true) then
        AdvCombatEntity_SetProperty("Ambushed", false)
        AdvCombat_SetProperty("Set Ability As Active", gciAI_AmbushAbilitySlot)
        AdvCombat_SetProperty("Mark Handled Action")
        AdvCombat_SetProperty("Run Active Ability On Active Targets")
        Debug_PopPrint("AI is ambushed. Running ambush ability.\n")
        return
    end

    --Switch AIs.
    if(sAIResult == "Fascinate") then
    elseif(sAIResult == "Confound") then
    elseif(sAIResult == "Berserk") then
    elseif(sAIResult == "Confuse") then
        LM_ExecuteScript(gsRoot .. "Combat/AIs/001 Confused AI.lua", gciAI_Decide_Action)
        Debug_PopPrint("AI is confused. AI ran confuse handler.\n")
        return
    elseif(sAIResult == "Charm") then

    end

    -- |[ ===================== Turn Checking ==================== ]|
    -- |[Check Turn]|
    --At this point, check if the AI's owner is the one who is actually acting. Anything above this point is status
    -- modifiers, everything below this point is attempting to execute an ability.
    local iOwnerID = RO_GetID()
    local iActingID = AdvCombat_GetProperty("ID of Acting Entity")
    Debug_Print("Owner ID: " .. iOwnerID .. "\n")
    Debug_Print("Acting ID: " .. iActingID .. "\n")
    if(iOwnerID ~= iActingID) then
        Debug_PopPrint("AI is not the acting entity.\n")
        return
    end
    
    --Get name for diagnostics.
    AdvCombat_SetProperty("Push Entity By ID", iActingID)
        local sAIOwnerName = AdvCombatEntity_GetProperty("Display Name")
    DL_PopActiveObject()
    
    --Diagnostics.
    gzDiagnostics.fnAIExec(gciAI_Diagnostic_SkillRoll, "AI for " .. sAIOwnerName .. " is deciding its action.\n")
    
    --Increment the turn counter. This happens even if the AI is stunned.
    local iTurn = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/iTurn", "N")
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/iTurn", "N", iTurn + 1)

    -- |[ ===================== Stun Handling ==================== ]|
    -- |[Stun]|
    --Standard stun handler. Will return true if this entity is stunned.
    local bIsStunned = fnStunHandleActionBegin()
    if(bIsStunned == true) then

        --Diagnostics.
        gzDiagnostics.fnAIExec(gciAI_Diagnostic_SkillRoll, " Stunned. Ending routine.\n")

        --Animation.
        AdvCombat_SetProperty("Set Ability As Active", gciAI_StunAbilitySlot)

        --Stop action here.
        AdvCombat_SetProperty("Mark Handled Action")
        AdvCombat_SetProperty("Run Active Ability On Active Targets")
        Debug_PopPrint("AI is stunned. Passing.\n")
        return
    end
    
    --Increment the action counter. This only happens when the AI gets to actually make a decision, as opposed to being stunned.
    -- Mandated attacks via tags still count as actions.
    local iAction = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/iAction", "N")
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/iAction", "N", iAction + 1)
    
    -- |[ ====================== Tag Mandate ===================== ]|
    --If an effect exists prefixed with "MANDATE|" then it requires the AI to perform a specific attack this round.
    -- This overrides normal rolling percentages.
    local iMandateAbilitySlot = -1
    AdvCombat_SetProperty("Push Entity By ID", iOwnerID)
    
        --Check for the tag and get its full name.
        local sFullTagName = AdvCombatEntity_GetProperty("Get First Tag Name Matching", "MANDATE|")
        
        --If the tag name doesn't come back "NULL" then at least one tag exists.
        if(sFullTagName ~= "NULL") then
            
            --Diagnostics.
            Debug_Print("AI detects a mandate tag: " .. sFullTagName .. "\n")
            
            --Get the ability name mandated.
            local sAbilityName = string.sub(sFullTagName, 9)
        
            --Check if we actually have this ability. If not, register it now.
            local iAbilitySlot = AdvCombatEntity_GetProperty("Slot of Ability In Master List", sAbilityName)
            if(iAbilitySlot == -1) then
                
                --Scan list. If found, add the ability to the entity now.
                local bFoundAbility = false
                for i = 1, #gczaEnemyAbilityLookups, 1 do
                    if(gczaEnemyAbilityLookups[i][1] == sAbilityName) then
                        LM_ExecuteScript(gczaEnemyAbilityLookups[i][2], gciAbility_Create)
                        bFoundAbility = true
                        break
                    end
                end
                
                --Print a warning if the ability is not found in the lookups.
                if(bFoundAbility == false) then
                    io.write("Warning, attempted to prime ability " .. sAbilityName .. " for enemy, but that ability was not found.\n")
                else
                    iAbilitySlot = AdvCombatEntity_GetProperty("Slot of Ability In Master List", sAbilityName)
                end
            end
            
            --Set the ability.
            iMandateAbilitySlot = iAbilitySlot
            Debug_Print("Mandated ability slot: " .. iMandateAbilitySlot .. "\n")
            Debug_Print("A mandate of -1 means no skill could be resolved. Mandates bypass AIStrings.\n")
        end
    DL_PopActiveObject()
    
    -- |[ ================= AI String Execution ================== ]|
    --Check if this tag is present. If it is, there may be an AIString this entity should use. This executes AFTER mandate tags!
    local iAITagCount = AdvCombatEntity_GetProperty("Tag Count", "AIString")
    if(iAITagCount > 0 and iMandateAbilitySlot == -1) then
        
        --Debug.
        Debug_Print("Enemy is using an AI String to run its AI. Slot is " .. iAITagCount .. ", total is " .. #gzAIStringList .. "\n")
        
        --This number is always rolled between 1 and 100 for use in AIStrings.
        giRunningAttackRoll = 0
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/iAttackRoll", "N", LM_GetRandomNumber(1, 100))
        
        --Range check.
        if(iAITagCount > #gzAIStringList) then
            io.write("Enemy Standard AI: Warning, AIString tag count " .. iAITagCount .. " is out of range.\n")
            
        --Valid.
        else
            gzAIStringList[iAITagCount]:fnExecute()
            local iActionCounter = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/iActionCounter", "N")
            VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/iActionCounter", "N", iActionCounter + 1)
            Debug_PopPrint("AI completes action by running an AI String.\n")
            return
        end
    end

    -- |[ ================== Ability Execution =================== ]|
    -- |[Ability Selection]|
    --Function handles this.
    if(iMandateAbilitySlot == -1) then
        gzDiagnostics.fnAIExec(gciAI_Diagnostic_SkillRoll, " Picking ability by roll.\n")
        Debug_Print("Picking ability by roll.\n")
        fnPickAbilityByRoll(true)
    
    --Override.
    else
        AdvCombat_SetProperty("Set Ability As Active", iMandateAbilitySlot)
    end
    
    -- |[Paint Targets, Execute]|
    --Function paints targets and selects a cluster by threat. If flagged, also executes the ability.
    gzDiagnostics.fnAIExec(gciAI_Diagnostic_SkillRoll, " Picking target by threat.\n")
    Debug_Print("Picking targets by threat.\n")
    fnPickTargetByThreat(true, true)
    
    --Diagnostics.
    Debug_Print("Executing skill.\n")
    gzDiagnostics.fnAIExec(gciAI_Diagnostic_SkillRoll, " AI completed normally.\n")
    
    -- |[Diagnostics]|
    Debug_PopPrint("AI completes action normally.\n")
    
-- |[ ===================================== Threat Update ====================================== ]|
--Call this whenever you need to update the threat values for this AI.
elseif(iSwitchType == gciAI_UpdateThreat) then

    -- |[Setup]|
    --Variables
    local iOwnerID = RO_GetID()

    -- |[Threat Scanner]|
    --Scan the player's party and create effects to handle threat vs. each entity. These are not
    -- the actual threat values, just an effect displaying them.
    local iCombatPartySize = AdvCombat_GetProperty("Combat Party Size")
    for i = 0, iCombatPartySize-1, 1 do

        --Push the party member for checking.
        AdvCombat_SetProperty("Push Combat Party Member By Slot", i)

            --Get the ID of the party member in question.
            local iPartyID = RO_GetID()

            --Check if the effect exists. A variable in its DataLibrary section will store the ID
            -- of its target.
            local bEffectExists = false
            local iEffectsTotal = AdvCombat_SetProperty("Store Effects Referencing", iOwnerID)
            for p = 0, iEffectsTotal-1, 1 do
                AdvCombat_SetProperty("Push Temp Effects", p)
                    local iEffectID = RO_GetID()
                    local iThreatTarget = VM_GetVar("Root/Variables/Combat/" .. iEffectID .. "/iThreatTarget", "I")
                    
                    --If this effect is the ThreatVs for this target, then update its display with the threat value.
                    if(iThreatTarget == iPartyID) then
                        bEffectExists = true
                        local iThreat = VM_GetVar("Root/Variables/Combat/" .. iOwnerID .. "_AI/" .. iPartyID.. "_Threat", "I")
                        LM_ExecuteScript(gsThreatEffectPath, gciEffect_ThreatVs_SetValues, iThreat)
                    end
                DL_PopActiveObject()
                if(bEffectExists) then break end
            end
            
            --Clean up temp effects list.
            AdvCombat_SetProperty("Clear Temp Effects")
        
            --Create an effect if none exists.
            if(bEffectExists == false) then
                AdvCombat_SetProperty("Create Effect", iOwnerID)
                
                    --Variables.
                    local iEffectID = RO_GetID()
                    
                    --Run the setup scripts.
                    local iThreat = VM_GetVar("Root/Variables/Combat/" .. iOwnerID .. "_AI/" .. iPartyID.. "_Threat", "I")
                    LM_ExecuteScript(gsThreatEffectPath, gciEffect_Create, iOwnerID, iPartyID)
                    LM_ExecuteScript(gsThreatEffectPath, gciEffect_ThreatVs_SetValues, iThreat)
                    
                DL_PopActiveObject()
            end
        
        DL_PopActiveObject()
    end
    
    -- |[Clear Unused Player Entities]|
    --If an entity is removed from the player's party, it loses all threat. Scan for threat effects and remove them
    -- if the entity is gone.
    local iaEffectIDList = {}
    
    --Iterate across all effects.
    local iEffectsTotal = AdvCombat_SetProperty("Store Effects Referencing", iOwnerID)
    for p = 0, iEffectsTotal-1, 1 do
        AdvCombat_SetProperty("Push Temp Effects", p)
        
            --Make sure this is a threat effect.
            local iEffectID = RO_GetID()
            if(VM_Exists("Root/Variables/Combat/" .. iEffectID .. "/iThreatTarget") == true) then
        
                --Get the ID of the target of this effect.
                local iThreatTarget = VM_GetVar("Root/Variables/Combat/" .. iEffectID .. "/iThreatTarget", "I")
                
                --If the entity is not in the player's party, remove this effect.
                local iPartyOfID = AdvCombat_GetProperty("Party Of ID", iThreatTarget)
                if(iPartyOfID ~= gciACPartyGroup_Party) then
                    table.insert(iaEffectIDList, iEffectID)
                end
            end
        DL_PopActiveObject()
    end
            
    --Remove them all at once to preserve list stability.
    for i = 1, #iaEffectIDList, 1 do
        AdvCombat_SetProperty("Remove Effect", iaEffectIDList[i])
    end

    --Clean up temp effects list.
    AdvCombat_SetProperty("Clear Temp Effects")
end
