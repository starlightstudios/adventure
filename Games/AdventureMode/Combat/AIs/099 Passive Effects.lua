-- |[ ================================ Passive Effect Handling ================================= ]|
--Called during the AI update gciAI_ActionBegin, this script will check the entity for any passive
-- effects they are supposed to always have, and will apply them. This is done via tags.
--The tags don't inherently know the path to the requisite effect, so this script resolves them.
-- Some tags may also have specific logics attached so this script isn't just a lookup table.
--The ID of the calling entity should be passed in.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iOwnerID = LM_GetScriptArgument(0, "N")

-- |[Protection Test]|
--Effect used to test the Flank Tactics skill.
AdvCombat_SetProperty("Push Entity By ID", iOwnerID)
    local iProtectionTestPassive = AdvCombatEntity_GetProperty("Tag Count", "Passive: Protection Test")
    local iProtectionTestExists  = AdvCombatEntity_GetProperty("Tag Count", "Exists: Protection Test")
DL_PopActiveObject()

--If the passive is needed but does not exist:
if(iProtectionTestPassive > 0 and iProtectionTestExists < 1) then
    local sEffectPath = gsRoot .. "Combat/Enemies/Effects/Chapter 0 Enemies/Protection Test.lua"
    AdvCombat_SetProperty("Register Application Pack", iOwnerID, iOwnerID, 0, "Effect|" .. sEffectPath .. "|Originator:" .. iOwnerID)
end
