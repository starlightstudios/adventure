-- |[ ====================================== Meeting Slime ===================================== ]|
--Combat script, runs if Mei meets a slime in combat as her first monstergirl. This is the green variant.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Green") ]])
fnCutscene([[ Append("Mei:[E|Surprise] Oh my![P] Are you a -[P] slime?[P] Are you alive?[B][C]") ]])
fnCutscene([[ Append("Slime: *The slime gurgles and moves forward*[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] H-[P]hey![P] Back off![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Blush] (My goodness, she's [P]*huge*![P] I wish I...)[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] (Snap out of it, Mei![P] Here she comes!)") ]])
fnCutsceneBlocker()
