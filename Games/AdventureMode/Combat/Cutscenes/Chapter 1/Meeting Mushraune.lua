-- |[ =================================== Meeting Mushraune ==================================== ]|
--Combat script, runs if Mei meets a mushraune in combat as her first monstergirl.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Mushraune", "Neutral") ]])
fnCutscene([[ Append("Mei:[E|Surprise] Uh, excuse me?[P] Hello?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] (She looks like an actual, real-life mushroom girl![P] Is this real?)[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] (She looks sick.[P] I hope she's okay.)[B][C]") ]])
fnCutscene([[ Append("Mushraune: Hurr, yeah, kiss.[P] Kiss.[P] Kiss.[P] Come to me.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Surprise] Woah, back off![P] Hey!") ]])
fnCutsceneBlocker()
