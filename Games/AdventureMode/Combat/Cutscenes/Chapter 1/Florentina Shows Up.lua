-- |[ =================================== Florentina Shows Up ================================== ]|
--Test.
local iArgumentsTotal = LM_GetNumOfArgs()
io.write("Received " .. iArgumentsTotal .. " arguments.\n")
for i = 1, iArgumentsTotal, 1 do
    io.write(" " .. i .. ": " .. LM_GetScriptArgument(i-1) .. "\n")
end

--Debug script.
WD_SetProperty("Show")
WD_SetProperty("Major Sequence", true)
WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") 
WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral")
Append("Florentina:[E|Neutral] You rang?[B][C]")
Append("Mei:[E|Neutral] It's a sample cutscene.")
