-- |[ ==================================== Meeting Beegirl ===================================== ]|
--Combat script, runs if Mei meets a bee in combat as her first monstergirl.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Bee", "Neutral") ]])
fnCutscene([[ Append("Mei:[E|Surprise] Uh, hello?[P] Are you friendly?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] (I must be dreaming![P] She's flying with those wings and her legs...[P] she's a real bee girl!)[B][C]") ]])
fnCutscene([[ Append("Bee: Bzzz bzz![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] Hey, don't get too friendly now, we just met![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] B-[P]back off![P] Hey!") ]])
fnCutsceneBlocker()
