-- |[ =============================== Standard Damage Over Time ================================ ]|
-- |[Description]|
--Standard DoT script. Just call with appropriate arguments!

-- |[Notes]|
--None yet.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ======================================== Creation ======================================== ]|
--LM_ExecuteScript(gsStandardDoTPath, gciEffect_Create, sDisplayName, iUniqueID, iDuration, sAbilityIcon, sTagA, iTagACnt, ...)
if(iSwitchType == gciEffect_Create and iArgumentsTotal >= 5) then
    
    --Arguments.
    local sDisplayName = LM_GetScriptArgument(1)
    local iUniqueID    = LM_GetScriptArgument(2, "I")
    local iDuration    = LM_GetScriptArgument(3, "I")
    local sAbilityIcon = LM_GetScriptArgument(4)
    
    --Create a DataLibrary entry for this, specify the duration.
    DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "/")
    VM_SetVar("Root/Variables/Combat/"  .. iUniqueID .. "/sDisplayName", "S", sDisplayName)
    VM_SetVar("Root/Variables/Combat/"  .. iUniqueID .. "/iCurTurn", "N", 0)
    VM_SetVar("Root/Variables/Combat/"  .. iUniqueID .. "/iDuration", "N", iDuration)
    VM_SetVar("Root/Variables/Combat/"  .. iUniqueID .. "/iMaxDuration", "N", iDuration)
    VM_SetVar("Root/Variables/Combat/"  .. iUniqueID .. "/fTagBonus", "N", 0.0)
    
    --Turn factors. These can be overridden later if desired, but for now they are 1/turns.
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iFactorsTotal", "N", iDuration)
    for i = 1, iDuration, 1 do
        local sVarName = string.format("fFactor%02i", i-1)
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/" .. sVarName, "N", 1 / iDuration)
    end
    
    --Display Name
    AdvCombatEffect_SetProperty("Display Name", sDisplayName)
    
    --Images.
    AdvCombatEffect_SetProperty("Back Image",  gsAbility_Backing_DoT)
    AdvCombatEffect_SetProperty("Frame Image", gsAbility_Frame_Active)
    AdvCombatEffect_SetProperty("Front Image", "Root/Images/AdventureUI/Abilities/" .. sAbilityIcon)
    
    --Effect priority.
    AdvCombatEffect_SetProperty("Priority", gciEffectPriorityDot)
    
    --Tags.
    for i = 5, iArgumentsTotal-1, 2 do
        AdvCombatEffect_SetProperty("Add Tag", LM_GetScriptArgument(i+0), LM_GetScriptArgument(i+1, "I"))
    end
    
    --Get the attack power of the originator at time of application.
    AdvCombatEffect_SetProperty("Push Originator")
        local iAttackPower = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iOriginatorID", "N", RO_GetID())
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iOriginatorAttackPower", "N", iAttackPower)
    DL_PopActiveObject()
    
-- |[ ====================================== Apply Stats ======================================= ]|
--LM_ExecuteScript(gsStandardDoTPath, gciEffect_ApplyStats or gciEffect_UnApplyStats, iTargetID, iResistIndex, sTypeIndicator)
elseif((iSwitchType == gciEffect_ApplyStats or iSwitchType == gciEffect_UnApplyStats) and iArgumentsTotal >= 4) then

    -- |[ ========================= Setup ======================== ]|
    -- |[Arguments]|
    local iTargetID      = LM_GetScriptArgument(1, "I")
    local iResistIndex   = LM_GetScriptArgument(2, "I")
    local sTypeIndicator = LM_GetScriptArgument(3)
    
    --Target ID out of range.
    if(iTargetID < 1) then return end
    
    --Tag Structure
    local zaTags = fnCreateTagStruct()
    
    --ID of this effect.
    local iUniqueID = RO_GetID()

    -- |[Target Stats]|
    --Push, get stats.
    AdvCombat_SetProperty("Push Entity By ID", iTargetID)
        local iResistance = AdvCombatEntity_GetProperty("Statistic", iResistIndex)
        zaTags.iBleedDamageTakenUpTags = AdvCombatEntity_GetProperty("Tag Count", "Bleed Damage Taken +")
        zaTags.iBleedDamageTakenDnTags = AdvCombatEntity_GetProperty("Tag Count", "Bleed Damage Taken -")
    DL_PopActiveObject()
    
    --Store the target ID and resistance.
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iTargetID", "N", iTargetID)
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iResistIndex", "N", iResistIndex)
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iTargetResistance", "N", iResistance)
    
    -- |[Originator Stats]|
    --Push, get stats.
    local iOriginatorID = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iOriginatorID", "I")
    AdvCombat_SetProperty("Push Entity By ID", iOriginatorID)
        zaTags.iBleedDamageDealtUpTags = AdvCombatEntity_GetProperty("Tag Count", "Bleed Damage Dealt +")
        zaTags.iBleedDamageDealtDnTags = AdvCombatEntity_GetProperty("Tag Count", "Bleed Damage Dealt -")
    DL_PopActiveObject()
    
    -- |[ ================== Damage Computation ================== ]|
    -- |[Variables]|
    local iOriginatorAttackPower = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iOriginatorAttackPower", "I")
    local iBenefitsFromItemPower = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iBenefitsFromItemPower", "I")
    
    -- |[Item Power]|
    --If benefitting from item power, the attack power of the originator is upped by their tag bonus.
    local fItemPower = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/fItemPowerFactor", "N")
    iOriginatorAttackPower = math.floor(iOriginatorAttackPower * fItemPower)
    
    -- |[Damage Computation]|
    --Get the 0th damage factor.
    local iDamageThisTurn = fnResolveDoTDamageForTurn(iUniqueID, 0, iOriginatorAttackPower, iResistance)
    
    --Damage bonus by type.
    local fTagBonus = 0.0
    if(iResistIndex == gciStatIndex_Resist_Bleed) then
        fTagBonus = 0.01 * (zaTags.iBleedDamageTakenUpTags + zaTags.iBleedDamageDealtUpTags - zaTags.iBleedDamageTakenDnTags - zaTags.iBleedDamageDealtDnTags)
        iDamageThisTurn = math.floor(iDamageThisTurn * (1.0 + fTagBonus))
        VM_SetVar("Root/Variables/Combat/"  .. iUniqueID .. "/fTagBonus", "N", fTagBonus)
    end
    
    --Debug.
    if(false) then
        io.write("DoT Applies:\n")
        io.write(" Attack Power: " .. iOriginatorAttackPower .. "\n")
        io.write(" Resistance: "   .. iResistance .. "\n")
        io.write(" Damage: "       .. iDamageThisTurn .. "\n")
        io.write(" Tag Bonus: "    .. fTagBonus .. "\n")
    end

    -- |[ ======================= Notifiers ======================= ]|
    -- |[Variables]|
    local iDuration = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iDuration", "I")
    
    -- |[HP Notifier]|
    --Place a notifier in the entity's HP display. This only applies to enemies.
    AdvCombatEffect_SetProperty("Short Text", iTargetID, iDamageThisTurn .. " [IMG0]/turn (" .. iDuration .. "[IMG1])")
    AdvCombatEffect_SetProperty("Allocate Short Text Images",  iTargetID, 2)
    AdvCombatEffect_SetProperty("Short Text Image",            iTargetID, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DamageTypeIconsLg/" .. sTypeIndicator)
    AdvCombatEffect_SetProperty("Short Text Image",            iTargetID, 1, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DamageTypeIconsLg/Clock")
    AdvCombatEffect_SetProperty("Crossload Short Text Images", iTargetID)

    -- |[Inspector Notifier Lft]|
    --Allocate.
    local iDescriptionFlag = VM_GetVar("Root/Variables/Combat/"  .. iUniqueID .. "/iDescriptionFlag", "I")
    if(iDescriptionFlag == gciDoTEven) then
        AdvCombatEffect_SetProperty("Allocate Description Strings", iTargetID, 3)
    else
        AdvCombatEffect_SetProperty("Allocate Description Strings", iTargetID, 5)
    end
    
    --Set the description text and populate any remaps.
    local sDisplayName = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/sDisplayName", "S")
    AdvCombatEffect_SetProperty("Description Text",             iTargetID, 0, "[IMG0]" .. sDisplayName)
    AdvCombatEffect_SetProperty("Allocate Description Images",  iTargetID, 0, 1)
    AdvCombatEffect_SetProperty("Description Image",            iTargetID, 0, 0, gcfAbilityImgOffsetY+2, "Root/Images/AdventureUI/DamageTypeIconsLg/Debuff")
    AdvCombatEffect_SetProperty("Crossload Description Images", iTargetID, 0)
    
    -- |[Inspector Notifier Rgt]|
    --Set the description text and populate any remaps.
    AdvCombatEffect_SetProperty("Description Text",             iTargetID, 1, iDamageThisTurn .. " [IMG0]/turn (" .. iDuration .. "[IMG1])")
    AdvCombatEffect_SetProperty("Allocate Description Images",  iTargetID, 1, 2)
    AdvCombatEffect_SetProperty("Description Image",            iTargetID, 1, 0, gcfAbilityImgOffsetY+3, "Root/Images/AdventureUI/DamageTypeIcons/" .. sTypeIndicator)
    AdvCombatEffect_SetProperty("Description Image",            iTargetID, 1, 1, gcfAbilityImgOffsetY+3, "Root/Images/AdventureUI/DamageTypeIcons/Clock")
    AdvCombatEffect_SetProperty("Crossload Description Images", iTargetID, 1)
    
    -- |[ ===================== Description ====================== ]|
    --Shows the description in the combat inspector.
    
    -- |[Even Damage]|
    --Even-damage on all turns:
    if(iDescriptionFlag == gciDoTEven) then
        AdvCombatEffect_SetProperty("Description Text",             iTargetID, 2, "Damage-over-time. Deals " .. iDamageThisTurn .. "/turn as [IMG0], " .. iDuration .. "[IMG1].")
        AdvCombatEffect_SetProperty("Allocate Description Images",  iTargetID, 2, 2)
        AdvCombatEffect_SetProperty("Description Image",            iTargetID, 2, 0, gcfAbilityImgOffsetLgY, "Root/Images/AdventureUI/DamageTypeIconsLg/" .. sTypeIndicator)
        AdvCombatEffect_SetProperty("Description Image",            iTargetID, 2, 1, gcfAbilityImgOffsetLgY, "Root/Images/AdventureUI/DamageTypeIconsLg/Clock")
        AdvCombatEffect_SetProperty("Crossload Description Images", iTargetID, 2)
    
    -- |[Backloaded]|
    --Backloaded DoT:
    elseif(iDescriptionFlag == gciDoTBackload) then
        AdvCombatEffect_SetProperty("Description Text",             iTargetID, 2, "Backloaded damage-over-time.")
        AdvCombatEffect_SetProperty("Allocate Description Images",  iTargetID, 2, 0)
        AdvCombatEffect_SetProperty("Crossload Description Images", iTargetID, 2)
        
        AdvCombatEffect_SetProperty("Description Text",             iTargetID, 3, "Deals " .. iDamageThisTurn .. "/turn as [IMG0], " .. iDuration .. "[IMG1].")
        AdvCombatEffect_SetProperty("Allocate Description Images",  iTargetID, 3, 2)
        AdvCombatEffect_SetProperty("Description Image",            iTargetID, 3, 0, gcfAbilityImgOffsetLgY, "Root/Images/AdventureUI/DamageTypeIconsLg/" .. sTypeIndicator)
        AdvCombatEffect_SetProperty("Description Image",            iTargetID, 3, 1, gcfAbilityImgOffsetLgY, "Root/Images/AdventureUI/DamageTypeIconsLg/Clock")
        AdvCombatEffect_SetProperty("Crossload Description Images", iTargetID, 3)
        
        AdvCombatEffect_SetProperty("Description Text",             iTargetID, 4, "Increases damage at the end of its lifetime.")
        AdvCombatEffect_SetProperty("Allocate Description Images",  iTargetID, 4, 0)
        AdvCombatEffect_SetProperty("Crossload Description Images", iTargetID, 4)
    
    -- |[Frontloaded]|
    --Frontloaded DoT:
    elseif(iDescriptionFlag == gciDoTFrontload) then
        AdvCombatEffect_SetProperty("Description Text",             iTargetID, 2, "Frontloaded damage-over-time.")
        AdvCombatEffect_SetProperty("Allocate Description Images",  iTargetID, 2, 0)
        AdvCombatEffect_SetProperty("Crossload Description Images", iTargetID, 2)
        
        AdvCombatEffect_SetProperty("Description Text",             iTargetID, 3, "Deals " .. iDamageThisTurn .. "/turn as [IMG0], " .. iDuration .. "[IMG1].")
        AdvCombatEffect_SetProperty("Allocate Description Images",  iTargetID, 3, 2)
        AdvCombatEffect_SetProperty("Description Image",            iTargetID, 3, 0, gcfAbilityImgOffsetLgY, "Root/Images/AdventureUI/DamageTypeIconsLg/" .. sTypeIndicator)
        AdvCombatEffect_SetProperty("Description Image",            iTargetID, 3, 1, gcfAbilityImgOffsetLgY, "Root/Images/AdventureUI/DamageTypeIconsLg/Clock")
        AdvCombatEffect_SetProperty("Crossload Description Images", iTargetID, 3)
        
        AdvCombatEffect_SetProperty("Description Text",             iTargetID, 4, "Greatly decreases damage after the first few turns.")
        AdvCombatEffect_SetProperty("Allocate Description Images",  iTargetID, 4, 0)
        AdvCombatEffect_SetProperty("Crossload Description Images", iTargetID, 4)
    end
    
-- |[ ================================== Combat: Turn Begins =================================== ]|
elseif(iSwitchType == gciAbility_BeginTurn) then

-- |[ ============================ Combat: Character Action Begins ============================= ]|
--LM_ExecuteScript(gsStandardDoTPath, gciAbility_BeginAction, sTypeIndicator, sAnimation)
elseif(iSwitchType == gciAbility_BeginAction and iArgumentsTotal >= 3) then

    -- |[ ========================= Setup ======================== ]|
    -- |[Arguments]|
    --Arguments.
    local sTypeIndicator = LM_GetScriptArgument(1)
    local sAnimation     = LM_GetScriptArgument(2)

    -- |[Duration Decrement]|
    --Get the ID of the acting entity.
    AdvCombat_SetProperty("Push Acting Entity")
        local iActingEntityID = RO_GetID()
    DL_PopActiveObject()
    
    --If the ID is not on the target list, we don't care.
    if(AdvCombatEffect_GetProperty("Is ID on Target List", iActingEntityID) == false) then
        return
    end
    
    --Decrement the duration counter.
    local iUniqueID = RO_GetID()
    local iDuration = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iDuration", "I")
    iDuration = iDuration - 1
    
    -- |[Current Turn Increment]|
    --Increment the current turn.
    local iCurTurn = VM_GetVar("Root/Variables/Combat/"  .. iUniqueID .. "/iCurTurn", "I")
    VM_SetVar("Root/Variables/Combat/"  .. iUniqueID .. "/iCurTurn", "N", iCurTurn + 1)
    
    -- |[Ending Case]|
    --Duration zeroes out:
    if(iDuration < 1) then
        AdvCombatEffect_SetProperty("Flag Remove Now")
    
    --Otherwise, store the decrement.
    else
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iDuration", "N", iDuration)
    end

    -- |[ ================== Damage Computation ================== ]|
    -- |[Apply Damage]|
    --Get variables.
    local iOriginatorID          = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iOriginatorID", "I")
    local iOriginatorAttackPower = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iOriginatorAttackPower", "I")
    local iTargetResistance      = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iTargetResistance", "I")
    local fTagBonus              = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/fTagBonus", "N")
    
    --Compute.
    local iDamageThisTurn = fnResolveDoTDamageForTurn(iUniqueID, iCurTurn, iOriginatorAttackPower, iTargetResistance)
    if(false) then
        io.write("DoT Ticks:\n")
        io.write(" Attack Power: " .. iOriginatorAttackPower .. "\n")
        io.write(" Resistance: "   .. iTargetResistance .. "\n")
        io.write(" Damage: "       .. iDamageThisTurn .. "\n")
        io.write(" Tag Bonus: "    .. fTagBonus .. "\n")
    end
    
    --Tag bonus.
    iDamageThisTurn = math.floor(iDamageThisTurn * (1.0 + fTagBonus))

    -- |[ ======================= Notifiers ======================= ]|
    -- |[HP Bar Notifier]|
    --Place a notifier in the entity's HP display. This only applies to enemies.
    AdvCombatEffect_SetProperty("Short Text", iActingEntityID, iDamageThisTurn .. " [IMG0]/turn (" .. iDuration .. "[IMG1])")
    AdvCombatEffect_SetProperty("Allocate Short Text Images", iActingEntityID, 2)
    AdvCombatEffect_SetProperty("Short Text Image", iActingEntityID, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DamageTypeIconsLg/" .. sTypeIndicator)
    AdvCombatEffect_SetProperty("Short Text Image", iActingEntityID, 1, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DamageTypeIconsLg/Clock")
    AdvCombatEffect_SetProperty("Crossload Short Text Images", iActingEntityID)
    
    -- |[Inspector Notifier Rgt]|
    --Set the description text and populate any remaps.
    AdvCombatEffect_SetProperty("Description Text",             iActingEntityID, 1, iDamageThisTurn .. " [IMG0]/turn (" .. iDuration .. "[IMG1])")
    AdvCombatEffect_SetProperty("Allocate Description Images",  iActingEntityID, 1, 2)
    AdvCombatEffect_SetProperty("Description Image",            iActingEntityID, 1, 0, gcfAbilityImgOffsetY+3, "Root/Images/AdventureUI/DamageTypeIcons/" .. sTypeIndicator)
    AdvCombatEffect_SetProperty("Description Image",            iActingEntityID, 1, 1, gcfAbilityImgOffsetY+3, "Root/Images/AdventureUI/DebuffIcons/Clock")
    AdvCombatEffect_SetProperty("Crossload Description Images", iActingEntityID, 1)
    
    -- |[ ==================== Implementation ==================== ]|
    -- |[Event Creation]|
    --Default package.
    local zaAbilityPackage = fnConstructDefaultAbilityPackage(sAnimation)
    zaAbilityPackage.iOriginatorID = iOriginatorID
    
    -- |[Application Sequence]|
    --Setup.
    local iTimer = 0
    
    --DoTs bypass shields by default. Add this as an extra parameter to the damage function.
    local sDamageParams = "PrioritySh:0"
    
    --Ability strikes, displays the hit, applies/resist effect, finishes up.
    iTimer = zaAbilityPackage.fnAbilityAnimate(iActingEntityID, iTimer, zaAbilityPackage, false)
    if(giLastDoTTick < iTimer) then
        giLastDoTTick = iTimer
    end
    iTimer = zaAbilityPackage.fnDamageAnimate(iOriginatorID, iActingEntityID, iTimer, iDamageThisTurn, sDamageParams)
    fnDefaultEndOfApplications(iTimer)

-- |[ ============================= Combat: Character Free Action ============================== ]|
--Called after a character executes a free action.
elseif(iSwitchType == gciEffect_BeginFreeAction) then

-- |[ ============================= Combat: Character Action Ends ============================== ]|
--Called after a character executes any action. Is not called if the action was a free action 
-- (that's gciEffect_BeginFreeAction above).
elseif(iSwitchType == gciEffect_PostAction) then

    -- |[Host KO]|
    --If the host is KO'd, remove the DoT effect.
    local bRemove = false
    local iUniqueID = RO_GetID()
    local iTargetID = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iTargetID", "I")
    AdvCombat_SetProperty("Push Entity By ID", iTargetID)
    
        --Check HP.
        local iHPCur = AdvCombatEntity_GetProperty("Health")
        if(iHPCur < 1) then
            bRemove = true
        end
    
    DL_PopActiveObject()

    --If flagged, remove.
    if(bRemove) then
        AdvCombatEffect_SetProperty("Flag Remove Now")
    end

-- |[ =================================== Combat: Turn Ends ==================================== ]|
--Called after the turn ends, before turn order for the next turn is resolved.
elseif(iSwitchType == gciEffect_TurnEnds) then

-- |[ ================================== Combat: Combat Ends =================================== ]|
--Called after combat ends. The passed in flag dictates if Victory/Defeat occurred. This occurs
-- before Doctor Bag/XP/JP etc is awarded.
elseif(iSwitchType == gciEffect_CombatEnds) then

-- |[ ================================ Compute Remaining Damage ================================ ]|
--Used by DoTs, this computes how much damage is remaining on the DoT and populates a global with it.
--LM_ExecuteScript(gsStandardDoTPath, gciEffect_DotComputeDamage, iEffectID)
elseif(iSwitchType == gciEffect_DotComputeDamage) then

    -- |[Setup]|
    --Get arguments.
    local iEffectID = LM_GetScriptArgument(1, "I")
    
    --Set global to 0. Caller should do this anyway.
    giEffect_LastComputedDotDamage = 0
    
    -- |[Variables]|
    --Get variables.
    local iCurTurn               = VM_GetVar("Root/Variables/Combat/" .. iEffectID .. "/iCurTurn", "I")
    local iMaxDuration           = VM_GetVar("Root/Variables/Combat/" .. iEffectID .. "/iMaxDuration", "I")
    local iOriginatorAttackPower = VM_GetVar("Root/Variables/Combat/" .. iEffectID .. "/iOriginatorAttackPower", "I")
    local iTargetResistance      = VM_GetVar("Root/Variables/Combat/" .. iEffectID .. "/iTargetResistance", "I")
    
    -- |[Computation]|
    --For each remaining turn:
    for i = iCurTurn, iMaxDuration-1, 1 do
        local iDamageThisTurn = fnResolveDoTDamageForTurn(iEffectID, iCurTurn, iOriginatorAttackPower, iTargetResistance)
        giEffect_LastComputedDotDamage = giEffect_LastComputedDotDamage + iDamageThisTurn
    end
end
