-- |[ ===================================== Threat Versus ====================================== ]|
-- |[Description]|
--Shows the threat the holder has against the target.

-- |[Notes]|
--None yet.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ========================================== Creation ========================================= ]|
--Called when the Effect is initialized. This may be at combat start, or it may be when the matching
-- ability is used. This is *not* called for a passive effect for the Status UI!
--Local variables, if any, should be set up here. The Active Object is the newly created AdvCombatEffect.
if(iSwitchType == gciEffect_Create and iArgumentsTotal >= 3) then

    --Target ID, additional argument passed in.
    local iOwnerID  = LM_GetScriptArgument(1, "I")
    local iTargetID = LM_GetScriptArgument(2, "I")
    
    --Get the target's name.
    AdvCombat_SetProperty("Push Entity By ID", iTargetID)
        local sTargetName = AdvCombatEntity_GetProperty("Display Name")
    DL_PopActiveObject()

    --System
    AdvCombatEffect_SetProperty("Display Name", "Threat Vs. " .. sTargetName)
    AdvCombatEffect_SetProperty("Script", LM_GetCallStack(0))
    AdvCombatEffect_SetProperty("Set Hidden On Quick UI", true)
    
    --Get the unique ID. Create a DataLibrary path to store variables for this effect. These are
    -- cleared when combat ends.
    local iUniqueID = RO_GetID()
    DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "/")
    
    --Common variables.
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iOwnerID", "N", iOwnerID)
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iThreatTarget", "N", iTargetID)
    
    --Images.
    AdvCombatEffect_SetProperty("Front Image", "Root/Images/AdventureUI/Abilities/Threat")
    
    --Effect Description, doesn't change:
    AdvCombatEffect_SetProperty("Allocate Description Strings", iOwnerID, 6)
    AdvCombatEffect_SetProperty("Description Text",             iOwnerID, 2, "Threat ([IMG0])determines which party member an")
    AdvCombatEffect_SetProperty("Allocate Description Images",  iOwnerID, 2, 1)
    AdvCombatEffect_SetProperty("Description Image",            iOwnerID, 2, 0, gcfAbilityImgOffsetLgY, "Root/Images/AdventureUI/DamageTypeIconsLg/Threat")
    AdvCombatEffect_SetProperty("Crossload Description Images", iOwnerID, 2)
    AdvCombatEffect_SetProperty("Description Text",             iOwnerID, 3, "enemy is most likely to attack.")
    AdvCombatEffect_SetProperty("Allocate Description Images",  iOwnerID, 3, 0)
    AdvCombatEffect_SetProperty("Description Text",             iOwnerID, 4, "Dealing damage or healing allies increases the")
    AdvCombatEffect_SetProperty("Allocate Description Images",  iOwnerID, 4, 0)
    AdvCombatEffect_SetProperty("Description Text",             iOwnerID, 5, "value, being attacked lowers it.")
    AdvCombatEffect_SetProperty("Allocate Description Images",  iOwnerID, 5, 0)
    
    --Effect Left, doesn't change
    AdvCombatEffect_SetProperty("Description Text",            iOwnerID, 0, "Threat vs. " .. sTargetName)
    AdvCombatEffect_SetProperty("Allocate Description Images", iOwnerID, 0, 0)
    
    --Effect Right, shows the value.
    AdvCombatEffect_SetProperty("Description Text",            iOwnerID, 1, "0")
    AdvCombatEffect_SetProperty("Allocate Description Images", iOwnerID, 1, 0)

-- |[ ====================================== Set Display ======================================= ]|
--Special value that updates the threat value vs the provided target. Requires an additional argument,
-- assumes the Active Object is the AdvCombatEffect.
elseif(iSwitchType == gciEffect_ThreatVs_SetValues) then

    -- |[Variables]|
    local iUniqueID     = RO_GetID()
    local iThreatValue  = LM_GetScriptArgument(1, "I")
    local iOwnerID      = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iOwnerID", "I")
    
    -- |[Upload Right]|
    --Set the description text and populate any remaps.
    AdvCombatEffect_SetProperty("Description Text",             iOwnerID, 1, iThreatValue)
    AdvCombatEffect_SetProperty("Allocate Description Images",  iOwnerID, 1, 0)
    AdvCombatEffect_SetProperty("Crossload Description Images", iOwnerID, 1)

end
