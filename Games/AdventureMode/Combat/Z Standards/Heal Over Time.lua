-- |[ ================================= Standard Heal Over Time ================================ ]|
-- |[Description]|
--Standard HoT script. Just call with appropriate arguments!

-- |[Notes]|
--None yet.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ======================================== Creation ======================================== ]|
--LM_ExecuteScript(gsStandardDoTPath, gciEffect_Create, sDisplayName, iUniqueID, iDuration, sAbilityIcon, sTagA, iTagACnt, ...)
if(iSwitchType == gciEffect_Create and iArgumentsTotal >= 5) then
    
    --Arguments.
    local sDisplayName = LM_GetScriptArgument(1)
    local iUniqueID    = LM_GetScriptArgument(2, "I")
    local iDuration    = LM_GetScriptArgument(3, "I")
    local sAbilityIcon = LM_GetScriptArgument(4)
    
    --Create a DataLibrary entry for this, specify the duration.
    DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "/")
    VM_SetVar("Root/Variables/Combat/"  .. iUniqueID .. "/sDisplayName", "S", sDisplayName)
    VM_SetVar("Root/Variables/Combat/"  .. iUniqueID .. "/iDuration", "N", iDuration)
    
    --Display Name
    AdvCombatEffect_SetProperty("Display Name", sDisplayName)
    
    --Images.
    AdvCombatEffect_SetProperty("Back Image",  gsAbility_Backing_Heal)
    AdvCombatEffect_SetProperty("Frame Image", gsAbility_Frame_Active)
    AdvCombatEffect_SetProperty("Front Image", "Root/Images/AdventureUI/Abilities/" .. sAbilityIcon)
    
    --Effect priority.
    AdvCombatEffect_SetProperty("Priority", gciEffectPriorityHot)
    
    --Tags.
    for i = 5, iArgumentsTotal-1, 2 do
        AdvCombatEffect_SetProperty("Add Tag", LM_GetScriptArgument(i+0), LM_GetScriptArgument(i+1, "N"))
    end
    
    --Get the attack power of the originator at time of application.
    AdvCombatEffect_SetProperty("Push Originator")
        local iAttackPower = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iOriginatorID", "N", RO_GetID())
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iOriginatorAttackPower", "N", iAttackPower)
    DL_PopActiveObject()
    
-- |[ ====================================== Apply Stats ======================================= ]|
--LM_ExecuteScript(gsStandardDoTPath, gciEffect_ApplyStats or gciEffect_UnApplyStats, iTargetID, fPowerFactor, iHealingFixed)
elseif((iSwitchType == gciEffect_ApplyStats or iSwitchType == gciEffect_UnApplyStats) and iArgumentsTotal >= 4) then

    --Arguments.
    local iTargetID      = LM_GetScriptArgument(1, "I")
    local fHealingFactor = LM_GetScriptArgument(2, "N")
    local iHealingFixed  = LM_GetScriptArgument(3, "I")
    
    --Target ID out of range.
    if(iTargetID < 1) then return end
    
    --Tag Structure
    local zaTags = fnCreateTagStruct()

    --Push. Get stats/tags.
    AdvCombat_SetProperty("Push Entity By ID", iTargetID)
        --TODO
    DL_PopActiveObject()

    --Get variables.
    local iUniqueID = RO_GetID()
    local iOriginatorID          = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iOriginatorID", "I")
    local iDuration              = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iDuration", "I")
    local iOriginatorAttackPower = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iOriginatorAttackPower", "I")
    
    --Save ID of target.
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iTargetID", "N", iTargetID)
    
    --Tags from originator.
    AdvCombat_SetProperty("Push Entity By ID", iOriginatorID)
        --TODO
    DL_PopActiveObject()
    
    --Compute the healing-per-turn.
    local iHealingTotal = (iHealingFixed + (fHealingFactor * iOriginatorAttackPower))
    local iHealingPerTurn = math.floor(iHealingTotal / iDuration)
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iHealingPerTurn", "N", iHealingPerTurn)
    
    --Debug.
    if(false) then
        io.write("Healing Fixed: " .. iHealingFixed .. "\n")
        io.write("Healing Factor: " .. fHealingFactor .. "\n")
        io.write("Healing Power: " .. iOriginatorAttackPower .. "\n")
        io.write("Healing Total: " .. iHealingTotal .. "\n")
        io.write("Duration: " .. iDuration.. "\n")
        io.write("Healing Per Turn: " .. iHealingPerTurn .. "\n")
    end

    -- |[HP Notifier]|
    --Place a notifier in the entity's HP display. This only applies to enemies.
    AdvCombatEffect_SetProperty("Short Text", iTargetID, iHealingPerTurn .. " [IMG0]/turn (" .. iDuration .. "[IMG1])")
    AdvCombatEffect_SetProperty("Allocate Short Text Images",  iTargetID, 2)
    AdvCombatEffect_SetProperty("Short Text Image",            iTargetID, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DamageTypeIconsLg/Health")
    AdvCombatEffect_SetProperty("Short Text Image",            iTargetID, 1, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DamageTypeIconsLg/Clock")
    AdvCombatEffect_SetProperty("Crossload Short Text Images", iTargetID)

    -- |[Inspector Notifier Lft]|
    --Allocate.
    AdvCombatEffect_SetProperty("Allocate Description Strings", iTargetID, 3)
    
    --Set the description text and populate any remaps.
    local sDisplayName = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/sDisplayName", "S")
    AdvCombatEffect_SetProperty("Description Text",             iTargetID, 0, "[IMG0]" .. sDisplayName)
    AdvCombatEffect_SetProperty("Allocate Description Images",  iTargetID, 0, 1)
    AdvCombatEffect_SetProperty("Description Image",            iTargetID, 0, 0, gcfAbilityImgOffsetY+2, "Root/Images/AdventureUI/DamageTypeIconsLg/Health")
    AdvCombatEffect_SetProperty("Crossload Description Images", iTargetID, 0)
    
    -- |[Inspector Notifier Rgt]|
    --Set the description text and populate any remaps.
    AdvCombatEffect_SetProperty("Description Text",             iTargetID, 1, iHealingPerTurn .. " [IMG0]/turn (" .. iDuration .. "[IMG1])")
    AdvCombatEffect_SetProperty("Allocate Description Images",  iTargetID, 1, 2)
    AdvCombatEffect_SetProperty("Description Image",            iTargetID, 1, 0, gcfAbilityImgOffsetY+3, "Root/Images/AdventureUI/DamageTypeIcons/Health")
    AdvCombatEffect_SetProperty("Description Image",            iTargetID, 1, 1, gcfAbilityImgOffsetY+3, "Root/Images/AdventureUI/DebuffIcons/Clock")
    AdvCombatEffect_SetProperty("Crossload Description Images", iTargetID, 1)
    
    -- |[Description]|
    AdvCombatEffect_SetProperty("Description Text",             iTargetID, 2, "Heal-over-time. Heals " .. iHealingPerTurn .. "[IMG0]/turn, " .. iDuration .. "[IMG1].")
    AdvCombatEffect_SetProperty("Allocate Description Images",  iTargetID, 2, 2)
    AdvCombatEffect_SetProperty("Description Image",            iTargetID, 2, 0, gcfAbilityImgOffsetLgY, "Root/Images/AdventureUI/DamageTypeIconsLg/Health")
    AdvCombatEffect_SetProperty("Description Image",            iTargetID, 2, 1, gcfAbilityImgOffsetLgY, "Root/Images/AdventureUI/DamageTypeIconsLg/Clock")
    AdvCombatEffect_SetProperty("Crossload Description Images", iTargetID, 2)
    
-- |[ ================================== Combat: Turn Begins =================================== ]|
elseif(iSwitchType == gciAbility_BeginTurn) then

-- |[ ============================ Combat: Character Action Begins ============================= ]|
--LM_ExecuteScript(gsStandardDoTPath, gciAbility_BeginAction, sAnimation)
elseif(iSwitchType == gciAbility_BeginAction and iArgumentsTotal >= 2) then

    --Arguments.
    local sAnimation = LM_GetScriptArgument(1)

    -- |[Duration Decrement]|
    --Get the ID of the acting entity.
    AdvCombat_SetProperty("Push Acting Entity")
        local iActingEntityID = RO_GetID()
    DL_PopActiveObject()
    
    --If the ID is not on the target list, we don't care.
    if(AdvCombatEffect_GetProperty("Is ID on Target List", iActingEntityID) == false) then
        return
    end
    
    --Decrement the duration counter.
    local iUniqueID = RO_GetID()
    local iDuration = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iDuration", "I")
    iDuration = iDuration - 1
    
    -- |[Ending Case]|
    --Duration zeroes out:
    if(iDuration < 1) then
        AdvCombatEffect_SetProperty("Flag Remove Now")
    
    --Otherwise, store the decrement.
    else
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iDuration", "N", iDuration)
    end

    -- |[Apply Damage]|
    --Get variables.
    local iOriginatorID          = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iOriginatorID", "I")
    local iOriginatorAttackPower = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iOriginatorAttackPower", "I")
    local iHealingPerTurn        = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iHealingPerTurn", "I")

    -- |[HP Bar Notifier]|
    --Place a notifier in the entity's HP display. This only applies to enemies.
    AdvCombatEffect_SetProperty("Short Text", iActingEntityID, iHealingPerTurn .. " [IMG0]/turn (" .. iDuration .. "[IMG1])")
    AdvCombatEffect_SetProperty("Allocate Short Text Images", iActingEntityID, 2)
    AdvCombatEffect_SetProperty("Short Text Image", iActingEntityID, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Health")
    AdvCombatEffect_SetProperty("Short Text Image", iActingEntityID, 1, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DebuffIcons/Clock")
    AdvCombatEffect_SetProperty("Crossload Short Text Images", iActingEntityID)
    
    -- |[Inspector Notifier Rgt]|
    --Set the description text and populate any remaps.
    AdvCombatEffect_SetProperty("Description Text",             iActingEntityID, 1, iHealingPerTurn .. " [IMG0]/turn (" .. iDuration .. "[IMG1])")
    AdvCombatEffect_SetProperty("Allocate Description Images",  iActingEntityID, 1, 2)
    AdvCombatEffect_SetProperty("Description Image",            iActingEntityID, 1, 0, gcfAbilityImgOffsetY+3, "Root/Images/AdventureUI/DamageTypeIcons/Health")
    AdvCombatEffect_SetProperty("Description Image",            iActingEntityID, 1, 1, gcfAbilityImgOffsetY+3, "Root/Images/AdventureUI/DebuffIcons/Clock")
    AdvCombatEffect_SetProperty("Crossload Description Images", iActingEntityID, 1)
    
    -- |[Event Creation]|
    --Default package.
    local zaAbilityPackage = fnConstructDefaultAbilityPackage(sAnimation)
    zaAbilityPackage.iOriginatorID = iOriginatorID
    
    -- |[Application Sequence]|
    --Setup.
    local iTimer = 0
    
    --If there is a DoT tick going on, add that value so the HoT runs afterwards.
    if(giLastDoTTick > 0) then
        iTimer = giLastDoTTick + 1
    end
    
    --Ability strikes, displays the hit, applies/resist effect, finishes up.
    iTimer = zaAbilityPackage.fnHealingAnimate(iOriginatorID, iActingEntityID, iTimer, iHealingPerTurn)
    fnDefaultEndOfApplications(iTimer)

-- |[ ============================= Combat: Character Free Action ============================== ]|
--Called after a character executes a free action.
elseif(iSwitchType == gciEffect_BeginFreeAction) then

-- |[ ============================= Combat: Character Action Ends ============================== ]|
--Called after a character executes any action. Is not called if the action was a free action 
-- (that's gciEffect_BeginFreeAction above).
elseif(iSwitchType == gciEffect_PostAction) then

    -- |[Host KO]|
    --If the host is KO'd, remove the HoT effect.
    local bRemove = false
    local iUniqueID = RO_GetID()
    local iTargetID = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iTargetID", "I")
    AdvCombat_SetProperty("Push Entity By ID", iTargetID)
    
        --Check HP.
        local iHPCur = AdvCombatEntity_GetProperty("Health")
        if(iHPCur < 1) then
            bRemove = true
        end
    
    DL_PopActiveObject()

    --If flagged, remove.
    if(bRemove) then
        AdvCombatEffect_SetProperty("Flag Remove Now")
    end

-- |[ =================================== Combat: Turn Ends ==================================== ]|
--Called after the turn ends, before turn order for the next turn is resolved.
elseif(iSwitchType == gciEffect_TurnEnds) then

-- |[ ================================== Combat: Combat Ends =================================== ]|
--Called after combat ends. The passed in flag dictates if Victory/Defeat occurred. This occurs
-- before Doctor Bag/XP/JP etc is awarded.
elseif(iSwitchType == gciEffect_CombatEnds) then

end
