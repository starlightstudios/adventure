-- |[ ============================== Ability Standard - Jobchange ============================== ]|
--There are a lot of job-change abilities in the game that all use a slight variation on the standard
-- job call, so they call this instead.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Global Check]|
if(gzRefAbility == nil) then return end

-- |[ ============================ Combat: Character Action Begins ============================= ]|
--This jobchange is available as long as it is not the current job.
if(iSwitchType == gciAbility_BeginAction or iSwitchType == gciAbility_BeginFreeAction) then

    --Do nothing if the owner is not acting:
    if(AdvCombatAbility_GetProperty("Is Owner Acting") == false) then return end
    
    --If this variable is not "Null", we need to check the owner's job to see if they can change.
    -- The ability is unusable if the user is already in the job in question.
    if(gzRefAbility.zExecutionAbiPackage.sCannotChangeJobIfInForm ~= "Null") then
    
        --Get the associated variable.
        local sCurrentJob = VM_GetVar(gzRefAbility.zExecutionAbiPackage.sJobChangeCheckVariable, "S")
        if(sCurrentJob == gzRefAbility.zExecutionAbiPackage.sCannotChangeJobIfInForm) then
            AdvCombatAbility_SetProperty("Usable", false)
            return
        end
    end
    
    --Call standard handler.
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Job Change abilities need to pulse all their class bar abilities and get them to remove any
-- passive effects they have. Otherwise, they run the same standard behavior as the default.
elseif(iSwitchType == gciAbility_Execute) then
    
    -- |[Purge Passive Effects]|
    fnPurgeJobEffects()
    
    -- |[Run Standard Handler]|
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
    
-- |[ ==================================== Standard Handler ==================================== ]|
--Standard handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
