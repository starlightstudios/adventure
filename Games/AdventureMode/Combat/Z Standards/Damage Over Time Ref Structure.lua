-- |[ ===================================== Effect Standard ==================================== ]|
-- |[Description]|
--Standard Effect script, meant to be used alongside a version of the prototype. The prototype
-- should be created by fnCreateDoTEffectPrototype() and then overrides applied.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ======================================== Creation ======================================== ]|
--Called when the Effect is initialized. We have an effect structure in memory stored in gzRefEffect.
-- This structure goes out of scope when the call ends, so any variables relevant to the effect
-- in the structure need to be stored.
if(iSwitchType == gciEffect_Create) then
    
    --ID.
    local iUniqueID = RO_GetID()
    
    --Execute.
    AdvCombatEffect_SetProperty("Script", LM_GetCallStack(0))
    LM_ExecuteScript(gsStandardDoTPath, gciEffect_Create, gzRefEffect.sDisplayName, iUniqueID, gzRefEffect.iDuration, gzRefEffect.sIcon, gzRefEffect.sTypeTag, gzRefEffect.iTypeAmount)
    
    --Store information from the gzRefEffect. The structure is nil'd after this execution.
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iDamageResistFlag", "N", gzRefEffect.iDamageResistFlag)
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/sDisplayOnApply",   "S", gzRefEffect.sDisplayOnApply)
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/sDamageTypeIcon",   "S", gzRefEffect.sDamageTypeIcon)
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/sAnimation",        "S", gzRefEffect.sAnimation)
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iDescriptionFlag",  "N", gzRefEffect.iDamageFactorDescriptionFlag)
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/fItemPowerFactor",  "N", 1.00)
    
    --Store the damage-factors-per-turn.
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iFactorsTotal", "N", #gzRefEffect.faTurnFactors)
    for i = 1, #gzRefEffect.faTurnFactors, 1 do
        local sVarName = string.format("fFactor%02i", i-1)
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/" .. sVarName, "N", gzRefEffect.faTurnFactors[i])
    end
    
    --Record the item power factor if flagged.
    if(gzRefEffect.bBenefitsFromItemPower) then
        
        --Get power factor.
        AdvCombatEffect_SetProperty("Push Originator")
            local iItemPowerBonus = AdvCombatEntity_GetProperty("Tag Count", "Item Power +")
            local iItemPowerMalus = AdvCombatEntity_GetProperty("Tag Count", "Item Power -")
            local fItemPowerFactor = 1.00 + (0.01 * (iItemPowerBonus - iItemPowerMalus))
        DL_PopActiveObject()
        
        --Store it.
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/fItemPowerFactor", "N", fItemPowerFactor)
    end
    
    --Additional tags.
    if(gzRefEffect.zaTagList ~= nil) then
        for i = 1, #gzRefEffect.zaTagList, 1 do
            AdvCombatEffect_SetProperty("Add Tag", gzRefEffect.zaTagList[i][1], gzRefEffect.zaTagList[i][2])
        end
    end
    
-- |[ ====================================== Apply Stats ======================================= ]|
--Called when the given Effect needs to apply its stats to a CombatStatistics package. This may be
-- when it is created, when the status UI needs it, or any number of other locations. If the flag is
-- gciEffect_UnApplyStats, then the effects are being removed because the effect expired.
--The AdvCombatEffect is the Active Effect. The second argument passed in will be the ID of the target.
elseif((iSwitchType == gciEffect_ApplyStats or iSwitchType == gciEffect_UnApplyStats) and iArgumentsTotal >= 2) then
    
    --ID.
    local iUniqueID = RO_GetID()
    
    --Get ID of target.
    local iTargetID = LM_GetScriptArgument(1, "I")
    if(iTargetID < 1) then return end
    
    --Variables.
    local iDamageResistFlag    = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iDamageResistFlag", "I")
    local sDamageTypeIcon      = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/sDamageTypeIcon",   "S")
    
    --Subroutine.
    LM_ExecuteScript(gsStandardDoTPath, iSwitchType, iTargetID, iDamageResistFlag, sDamageTypeIcon)
    gzRefEffect = nil

-- |[ ================================== Combat: Turn Begins =================================== ]|
--Called when the turn begins after turn order is resolved. This is called after Abilities have
-- run their updates. If an Effect is created by an ability during its update, it does *not* run its
-- turn-begin code until the next turn.
elseif(iSwitchType == gciAbility_BeginTurn) then

-- |[ ============================ Combat: Character Action Begins ============================= ]|
--Called when the character that possesses this Effect begins their turn.
elseif(iSwitchType == gciAbility_BeginAction) then
    
    --ID.
    local iUniqueID = RO_GetID()
    
    --Variables.
    local sDamageTypeIcon = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/sDamageTypeIcon", "S")
    local sAnimation      = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/sAnimation", "S")
    
    --Execute.
    LM_ExecuteScript(gsStandardDoTPath, gciAbility_BeginAction, sDamageTypeIcon, sAnimation)

-- |[ ============================= Combat: Character Free Action ============================== ]|
--Called after a character executes a free action.
elseif(iSwitchType == gciEffect_BeginFreeAction) then

-- |[ ============================= Combat: Character Action Ends ============================== ]|
--Called after a character executes any action. Is not called if the action was a free action 
-- (that's gciEffect_BeginFreeAction above).
elseif(iSwitchType == gciEffect_PostAction) then
    LM_ExecuteScript(gsStandardDoTPath, gciEffect_PostAction)

-- |[ =================================== Combat: Turn Ends ==================================== ]|
--Called after the turn ends, before turn order for the next turn is resolved.
elseif(iSwitchType == gciEffect_TurnEnds) then

-- |[ ================================== Combat: Combat Ends =================================== ]|
--Called after combat ends. The passed in flag dictates if Victory/Defeat occurred. This occurs
-- before Doctor Bag/XP/JP etc is awarded.
elseif(iSwitchType == gciEffect_CombatEnds) then

-- |[ ================================ Compute Remaining Damage ================================ ]|
--Used by DoTs, this computes how much damage is remaining on the DoT and populates a global with it.
-- Requires the ID to be passed in alongside.
elseif(iSwitchType == gciEffect_DotComputeDamage) then
    local iEffectID = LM_GetScriptArgument(1, "I")
    LM_ExecuteScript(gsStandardDoTPath, gciEffect_DotComputeDamage, iEffectID)
end
