-- |[ ====================================== Standard Job ====================================== ]|
--Standard implementation of a job script, using a prefabricated table that stores the information
-- needed to call various functions.

-- |[Important Note]|
--For gciJob_Create, the AdvCombatEntity that will own the job is the active object. For all other
-- calls, the AdvCombatJob is active and can push the owning entity.

-- |[ ============= Table Format ============= ]|
--The table zJobEntry needs to exist and be populated before this script is called. A stack called
-- gzJobCallStack should be pushed right before this call, and popped after it.

-- |[Character Variabes]|
--zJobEntry.sCharacterFormVarPath      --DLPath to the variable the character uses to track their form. (eg: "Root/Variables/Global/Mei/sForm")
--zJobEntry.sSkillbookVarPath          --DLPath to the variable the character uses to track their skillbooks. (eg: "Root/Variables/Global/Mei/iSkillbookTotal")
--zJobEntry.zaJobChart                 --Reference to the job table this character uses to determine stat increases.
--zJobEntry.sStatProfilePath           --Path to the stat profile script this character uses at levelup. (eg: gsRoot .. "Combat/Party/Mei/400 Common Stat Profile.lua")
--zJobEntry.sSecondaryMenuPath         --Path to the secondary menu builder script for this character. (eg: gsRoot .. "Combat/Party/Mei/200 Build Secondary Menu.lua")
--zJobEntry.sMasterBonusPath           --Path to the mastery bonus handler for this character. (eg: gsRoot .. "Combat/Party/Mei/401 Mastery Bonus Handler.lua")

-- |[Class Variables]|
--zJobEntry.sClassName                 --Name of the class. Must be unique. (eg: "Fencer", "Prowler")
--zJobEntry.sFormName                  --Form associated with the class. (eg: "Human", "Werecat")
--zJobEntry.sCostumeName               --Character_Form pairing used to resolve costumes. (eg: "Mei_Human", "Mei_Werecat")
--zJobEntry.fFaceIndexX                --Which slot on the CharacterFaces sheet this class uses for rendering inventory icons.
--zJobEntry.fFaceIndexY                --Which slot on the CharacterFaces sheet this class uses for rendering inventory icons.

-- |[Job Ability Variables]|
--zJobEntry.saExternalAbilities        --List of strings that are all the abilities this class can purchase and have an external copy. (eg: {"Rend", "Blind", "Pommel Strike"})
--zJobEntry.saInternalAbilities        --List of strings that are all the abilities this class has equipped by default. Typically exactly 7 of these. (eg: {"Rend", "Blind", "Pommel Strike"})
--zJobEntry.saPurchaseAbilities        --List of strings that are the abilities this class has available for purchase, in order on the UI. (eg: {"Fencer|Rend", "Fencer|Blind"})

-- |[Skillbar Variables]|
--zJobEntry.zaSkillbarAbilities        --Skills that always appear on the skillbar. Format: {sAbilityName, iXPos, iYPos, iSkillbooks} (eg: {"Common|Attack", 0, 0, 0}, {"Common|Parry", 1, 0, 0})

-- |[ ========== Argument Handling =========== ]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ =========== Variable Resolve =========== ]|
--The job entry will be the last entry of the job call stack.
local zJobEntry = gzJobCallStack[#gzJobCallStack]

--Fail if the global entry is nil.
if(zJobEntry == nil) then
    Debug_ForcePrint("Error: Standard Job Script called with invalid global job entry. Calling script: " .. LM_GetCallStack(1) .. "\n")
    return
end

-- |[ ====================================== Job Creation ====================================== ]|
--Called when the chapter is initialized, the job registers itself to its owner and populates its
-- abilities. This is only called once.
--The owner should be the active object.
if(iSwitchType == gciJob_Create) then
    
    -- |[Diagnostics]|
    local bJobDiagnostics = Debug_GetFlag("Standard Job: Creation")
    Debug_PushPrint(bJobDiagnostics, "Beginning standard job script creation.\n")
    Debug_Print("Caller: " .. LM_GetCallStack(1) .. "\n")
    Debug_Print("Job name: " .. zJobEntry.sClassName .. "\n")
    Debug_Print("Abilities External: " .. #zJobEntry.saExternalAbilities .. "\n")
    Debug_Print("Abilities Internal: " .. #zJobEntry.saInternalAbilities .. "\n")
    
    -- |[Basic Class Creation]|
    --Create the job and set its stats.
    AdvCombatEntity_SetProperty("Create Job", zJobEntry.sClassName)
    
        --Name as it appears in the UI.
        AdvCombatJob_SetProperty("Display Name", zJobEntry.sClassName)
        AdvCombatJob_SetProperty("Appears on Skills UI", false)
    
        --Script path to this job
        AdvCombatJob_SetProperty("Script Path", LM_GetCallStack(1))
        
        --Responses.
        AdvCombatJob_SetProperty("Script Response", gciJob_SwitchTo,             true)
        AdvCombatJob_SetProperty("Script Response", gciJob_Level,                true)
        AdvCombatJob_SetProperty("Script Response", gciJob_Master,               true)
        AdvCombatJob_SetProperty("Script Response", gciJob_JobAssembleSkillList, true)
        AdvCombatJob_SetProperty("Script Response", gciJob_BeginCombat,          true)
        
        --Display
        local fLft = (zJobEntry.fFaceIndexX+0) * gci_FaceTable_Size
        local fTop = (zJobEntry.fFaceIndexY+0) * gci_FaceTable_Size
        local fRgt = (zJobEntry.fFaceIndexX+1) * gci_FaceTable_Size
        local fBot = (zJobEntry.fFaceIndexY+1) * gci_FaceTable_Size
        
        --If the face table is not specified, use the basic table.
        local sUseTable = "Root/Images/AdventureUI/ItemIcons/CharacterFaces"
        if(zJobEntry.sFaceTable ~= nil) then sUseTable = zJobEntry.sFaceTable end
        AdvCombatJob_SetProperty("Face Table Data", fLft, fTop, fRgt, fBot, sUseTable)

    DL_PopActiveObject()
    
    -- |[Ability Handling]|
    --Setup. Locate the path of the caller.
    local sCallerPath = fnResolvePathFrom(LM_GetCallStack(1))
    local sJobAbilityPath = sCallerPath .. "Abilities/"
    
    --Register external abilities:
    for i = 1, #zJobEntry.saExternalAbilities, 1 do
        Debug_Print(" Creating external ability: " .. zJobEntry.saExternalAbilities[i] .. "\n")
        local sAbilityPath = sJobAbilityPath .. zJobEntry.saExternalAbilities[i] .. ".lua"
        LM_ExecuteScript(sAbilityPath, gciAbility_Create)
    end
    
    --Register internal abilities:
    for i = 1, #zJobEntry.saInternalAbilities, 1 do
        Debug_Print(" Creating internal ability: " .. zJobEntry.saInternalAbilities[i] .. "\n")
        local sAbilityPath = sJobAbilityPath .. zJobEntry.saInternalAbilities[i] .. ".lua"
        LM_ExecuteScript(sAbilityPath, gciAbility_CreateSpecial)
    end
    
    -- |[Debug]|
    Debug_PopPrint("Finished standard job creation.\n")
   
-- |[ ===================================== Job Assumption ===================================== ]|
--Called when the player switches to the class. Clears any existing job properties, sets graphics
-- and stats as needed.
--This can be called during combat, as some characters can switch jobs in-battle.
--The calling AdvCombatEntity is expected to be the active object.
elseif(iSwitchType == gciJob_SwitchTo) then

    --Change images.
    VM_SetVar(zJobEntry.sCharacterFormVarPath, "S", zJobEntry.sFormName)
    LM_ExecuteScript(gsCostumeAutoresolve, zJobEntry.sCostumeName)
    
    --Unlock on UI.
    AdvCombatJob_SetProperty("Appears on Skills UI", true)

    --Push the owning entity.
    AdvCombatJob_SetProperty("Push Owner")

        -- |[Rebuild Visibility List]|
        AdvCombatEntity_SetProperty("Rebuild Job Skills UI")

        -- |[Statistics]|
        --Get health percentage.
        local fHPPercent = AdvCombatEntity_GetProperty("Health") / AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
        
        --Set job statistics.
        AdvCombatEntity_SetProperty("Compute Level Statistics", -1)
        
        --Apply change in max HP.
        AdvCombatEntity_SetProperty("Health Percent", fHPPercent)
        
        --Clear all relevant ability slots.
        fnClearJobSlots()
        
        -- |[Set Actions Bar]|
        --Get current skillbook count.
        local iSkillbookTotal = VM_GetVar(zJobEntry.sSkillbookVarPath, "N") 
        
        --Iterate across all skill entries and place them, or the "Common|Locked" ability if not enough skillbooks are present.
        for i = 1, #zJobEntry.zaSkillbarAbilities, 1 do
            
            --Fast-access pointer.
            local zSkill = zJobEntry.zaSkillbarAbilities[i]
            
            --Not enough skillbooks:
            if(zSkill[4] > iSkillbookTotal) then
                AdvCombatEntity_SetProperty("Set Ability Slot", zSkill[2], zSkill[3], "Common|Locked")
            
            --Has enough skillbooks:
            else
                AdvCombatEntity_SetProperty("Set Ability Slot", zSkill[2], zSkill[3], zSkill[1])
            end
        end
        
        -- |[Standard Abilities]|
        --Items, Retreat, Surrender, Etc.
        fnPlaceJobCommonSkillsNoItems()
        
        --Secondary menu handles special abilities like job changing.
        if(zJobEntry.sSecondaryMenuPath ~= "Null") then
            LM_ExecuteScript(zJobEntry.sSecondaryMenuPath)
        end
        
    DL_PopActiveObject()

-- |[ ====================================== Job Level Up ====================================== ]|
--Called when the job reaches a level. The second argument is the level in question. Remember that
-- level 1 on the UI is level 0 in the scripts.
--When assuming the job, this is called by the program to reset stats. There is no need to call it
-- during the Job Assumption part of this script.
elseif(iSwitchType == gciJob_Level) then

    -- |[Setup]|
    --Argument check.
    if(iArgumentsTotal < 2) then 
        Debug_ForcePrint("Error: No level specified for Job Level Up. " .. LM_GetCallStack(1) .. "\n") 
        return
    end

    --First argument is what level we reached.
    local iLevelReached = LM_GetScriptArgument(1, "N")
    
    --Common script.
    fnChartJobStatHandler(zJobEntry.zaJobChart, zJobEntry.sClassName, iLevelReached, zJobEntry.sStatProfilePath)
    
-- |[ ====================================== Job Mastered ====================================== ]|
--Called when the player purchases any ability used by this job in the skills UI. This call should
-- re-evaluate if the class is mastered, and possibly apply a permanent bonus. This is called AFTER
-- the job has already internally checked if it is mastered.
elseif(iSwitchType == gciJob_Master) then
    if(AdvCombatJob_GetProperty("Is Mastered") == true) then
    
        --Call the mastery bonus handler, if provided.
        if(zJobEntry.sMasterBonusPath ~= nil) then
            LM_ExecuteScript(zJobEntry.sMasterBonusPath, "Prestige Unlock")
        end
    end
    
-- |[ =================================== Assemble Skill List ================================== ]|
--Once all abilities are registered to the parent entity, builds a list of which skills are associated
-- with this job. These skills show up on the UI and can be purchased by the player. Note that a job
-- can contain the same ability as another job. Purchasing it in either will unlock both versions.
-- This is not explicitly a problem, it means the same ability can be bought with JP from multiple
-- jobs. It is not part of the standard for Adventure Mode though.
elseif(iSwitchType == gciJob_JobAssembleSkillList) then
    
    --Register abilities in the order they should appear in the skills menu.
    for i = 1, #zJobEntry.saPurchaseAbilities, 1 do
        AdvCombatEntity_SetProperty("Register Ability To Job", zJobEntry.saPurchaseAbilities[i], zJobEntry.sClassName)
    end

-- |[ ====================================== Combat Start ====================================== ]|
--Called when combat begins. The job should populate passive abilities here. The AdvCombatEntity
-- will be atop the activity stack.
elseif(iSwitchType == gciJob_BeginCombat) then
    AdvCombatJob_SetProperty("Push Owner")
        fnPlaceJobCommonSkills()
        fnPlaceJobCommonSkillsCombatStart()
    DL_PopActiveObject()
    
    --Call the mastery bonus handler, if provided. This will recheck which passive skills are available
    -- for the Prestige class.
    if(zJobEntry.sMasterBonusPath ~= nil) then
        LM_ExecuteScript(zJobEntry.sMasterBonusPath, "Resolve Skill Visibility")
    end

-- |[ ==================================== Combat: New Turn ==================================== ]|
--Called when turn order is sorted and a new turn begins.
elseif(iSwitchType == gciJob_BeginTurn) then

-- |[ =================================== Combat: New Action =================================== ]|
--Called when an action begins. Note that the action is not necessarily that of the active entity.
elseif(iSwitchType == gciJob_BeginAction) then
    
-- |[ ========================= Combat: New Action (After Free Action) ========================= ]|
--Called when an action begins after a Free Action has expired.
elseif(iSwitchType == gciJob_BeginFreeAction) then

-- |[ =================================== Combat: End Action =================================== ]|
--Called when an action ends. As above, not necessarily that of the active entity.
elseif(iSwitchType == gciJob_EndAction) then

-- |[ ==================================== Combat: End Turn ==================================== ]|
--Called when a turn ends, before turn order is determined for the next turn.
elseif(iSwitchType == gciJob_EndTurn) then

-- |[ =================================== Combat: End Combat =================================== ]|
--Called when combat ends for any reason. This includes, victory, defeat, retreat, or other.
elseif(iSwitchType == gciJob_EndCombat) then

-- |[ ================================== Combat: Event Queued ================================== ]|
--Called when a main event is enqueued. An entity typically performs one event per action. This is
-- called before after Entity but before Abilities respond for this character.
elseif(iSwitchType == gciJob_EventQueued) then

end
