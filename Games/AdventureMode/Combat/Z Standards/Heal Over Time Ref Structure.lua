-- |[ ===================================== Effect Standard ==================================== ]|
-- |[Description]|
--Standard Effect script, meant to be used alongside a version of the prototype. This is for HoTs.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ======================================== Creation ======================================== ]|
--Called when the Effect is initialized. This may be at combat start, or it may be when the matching
-- ability is used. This is *not* called for a passive effect for the Status UI!
--Local variables, if any, should be set up here. The Active Object is the newly created AdvCombatEffect.
if(iSwitchType == gciEffect_Create) then
    
    --Get ID.
    local iUniqueID = RO_GetID()
    
    --Execute.
    AdvCombatEffect_SetProperty("Script", LM_GetCallStack(0))
    LM_ExecuteScript(gsStandardHoTPath, gciEffect_Create, gzRefEffect.sDisplayName, iUniqueID, gzRefEffect.iDuration, gzRefEffect.sIcon)
    
    --Store values from the ref ability. The ref ability will be nil next time this runs.
    VM_SetVar("Root/Variables/Combat/"  .. iUniqueID .. "/fHealBase",   "N", gzRefEffect.fHealBase)
    VM_SetVar("Root/Variables/Combat/"  .. iUniqueID .. "/fHealFactor", "N", gzRefEffect.fHealFactor)
    VM_SetVar("Root/Variables/Combat/"  .. iUniqueID .. "/sAnimation",  "S", gzRefEffect.sAnimation)
    gzRefEffect = nil
    
-- |[ ====================================== Apply Stats ======================================= ]|
--Called when the given Effect needs to apply its stats to a CombatStatistics package. This may be
-- when it is created, when the status UI needs it, or any number of other locations. If the flag is
-- gciEffect_UnApplyStats, then the effects are being removed because the effect expired.
--The AdvCombatEffect is the Active Effect. The second argument passed in will be the ID of the target.
elseif((iSwitchType == gciEffect_ApplyStats or iSwitchType == gciEffect_UnApplyStats) and iArgumentsTotal >= 2) then
    
    --ID of effect.
    local iUniqueID = RO_GetID()
    
    --Get ID of target.
    local iTargetID = LM_GetScriptArgument(1, "I")
    if(iTargetID < 1) then return end
    
    --Get variables.
    local fHealBase   = VM_GetVar("Root/Variables/Combat/"  .. iUniqueID .. "/fHealBase", "N")
    local fHealFactor = VM_GetVar("Root/Variables/Combat/"  .. iUniqueID .. "/fHealFactor", "N")
    
    --Subroutine.
    LM_ExecuteScript(gsStandardHoTPath, iSwitchType, iTargetID, fHealFactor, fHealBase)

-- |[ ================================== Combat: Turn Begins =================================== ]|
--Called when the turn begins after turn order is resolved. This is called after Abilities have
-- run their updates. If an Effect is created by an ability during its update, it does *not* run its
-- turn-begin code until the next turn.
elseif(iSwitchType == gciAbility_BeginTurn) then

-- |[ ============================ Combat: Character Action Begins ============================= ]|
--Called when the character that possesses this Effect begins their turn.
elseif(iSwitchType == gciAbility_BeginAction) then

    --Variables.
    local iUniqueID = RO_GetID()
    local sAnimation = VM_GetVar("Root/Variables/Combat/"  .. iUniqueID .. "/sAnimation", "S")

    --Execute.
    LM_ExecuteScript(gsStandardHoTPath, gciAbility_BeginAction, sAnimation)

-- |[ ============================= Combat: Character Free Action ============================== ]|
--Called after a character executes a free action.
elseif(iSwitchType == gciEffect_BeginFreeAction) then

-- |[ ============================= Combat: Character Action Ends ============================== ]|
--Called after a character executes any action. Is not called if the action was a free action 
-- (that's gciEffect_BeginFreeAction above).
elseif(iSwitchType == gciEffect_PostAction) then
    LM_ExecuteScript(gsStandardHoTPath, gciEffect_PostAction)

-- |[ =================================== Combat: Turn Ends ==================================== ]|
--Called after the turn ends, before turn order for the next turn is resolved.
elseif(iSwitchType == gciEffect_TurnEnds) then

-- |[ ================================== Combat: Combat Ends =================================== ]|
--Called after combat ends. The passed in flag dictates if Victory/Defeat occurred. This occurs
-- before Doctor Bag/XP/JP etc is awarded.
elseif(iSwitchType == gciEffect_CombatEnds) then

end