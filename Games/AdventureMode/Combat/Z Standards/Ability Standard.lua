-- |[ ==================================== Ability Standard ==================================== ]|
-- |[Description]|
--A set of handlers that are the "Standard" behavior given the global gzRefAbility. Simply set the
-- global to the package required and call this script with the correct firing code.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Global Check]|
if(gzRefAbility == nil) then return end

-- |[ ==================================== Creation Handler ==================================== ]|
--Called when the ability is created and added to the job prototype. This shows the ability's name
-- and icon, to inform the player what abilities are on the job.
if(iSwitchType == gciAbility_Create or iSwitchType == gciAbility_CreateSpecial) then

    -- |[ ============== Setup =============== ]|
    --Special: If the ability has an internal version, the name changes. This is used for abilities
    -- that can both be purchased for JP and exist on the job skill bar by default.
    local sUseJobName = gzRefAbility.sJobName
    if(iSwitchType == gciAbility_CreateSpecial) then sUseJobName = gzRefAbility.sInternalName end
    
    --Special: If a second argument is provided, then the name of the ability is being overrode. This
    -- is used by item creation scripts since items have fixed names.
    local sAbilityNameOverride = "NULL"
    if(iArgumentsTotal >= 2) then
        sAbilityNameOverride = LM_GetScriptArgument(1)
    end
    
    -- |[Translation]|
    if(gbBuildAbilityTranslation == true) then
        gfOutfile = io.open("out.txt", "a")
    end
    
    -- |[ ============= Creation ============= ]|
    local sAbilityName = sUseJobName .. "|" .. gzRefAbility.sSkillName
    if(sAbilityNameOverride ~= "NULL") then sAbilityName = sAbilityNameOverride end
    AdvCombatEntity_SetProperty("Create Ability", sAbilityName)
    
        -- |[System]|
        AdvCombatAbility_SetProperty("Script Path", LM_GetCallStack(1)) --Use calling script.
        AdvCombatAbility_SetProperty("JP Cost", gzRefAbility.iJPUnlockCost)
        AdvCombatAbility_SetProperty("CP Cost", gzRefAbility.iRequiredCP)
        AdvCombatAbility_SetProperty("MP Cost", gzRefAbility.iRequiredMP)
        
        -- |[Special Flags]|
        --If this flag is true, the ability cannot be equipped. The flag can legally be nil.
        if(gzRefAbility.bCannotBeEquipped == true) then
            AdvCombatAbility_SetProperty("Equippable", false)
        end
        
        -- If not "Null", this script calls its calling script whenever it is unlocked.
        if(gzRefAbility.sUnlockScript ~= nil and gzRefAbility.sUnlockScript ~= "Null") then
            AdvCombatAbility_SetProperty("Unlock Exec Script", LM_GetCallStack(1))
        end
        
        --If this flag is set, the ability executes its script calls even if not on the equipped ability grid.
        if(gzRefAbility.bRunIfNotEquipped) then
            AdvCombatAbility_SetProperty("Runs When Not Equipped", true)
        end
        
        --If this flag is set, the ability executes the script call above when not equipped if and only if it
        -- is unlocked in the skills UI. Used for Permanent Passives.
        if(gzRefAbility.bOnlyRunIfUnlocked) then
            AdvCombatAbility_SetProperty("Only Run When Unlocked", true)
        end
        
        -- |[Tags]|
        --The structure may be nil.
        if(gzRefAbility.zaTags ~= nil) then
            for i = 1, #gzRefAbility.zaTags, 1 do
                AdvCombatAbility_SetProperty("Add Tag", gzRefAbility.zaTags[i][1], gzRefAbility.zaTags[i][2])
            end
        end
    
        -- |[Display]|
        AdvCombatAbility_SetProperty("Display Name", gzRefAbility.sSkillName)
        AdvCombatAbility_SetProperty("Icon Back",    gzRefAbility.sIconBacking)
        AdvCombatAbility_SetProperty("Icon Frame",   gzRefAbility.sIconFrame)
        if(gzRefAbility.sIconPath ~= "Null") then
            AdvCombatAbility_SetProperty("Icon", gzRefAbility.sIconPath)
        end
        if(gzRefAbility.sCPIcon ~= "Null") then
            AdvCombatAbility_SetProperty("CP Icon", gzRefAbility.sCPIcon)
        end
        
        --Name Override. If the field sDisplayName isn't nil, then the skill name and the display name aren't the same.
        if(gzRefAbility.sDisplayName ~= nil) then
            gzRefAbility.sDisplayName = Translate(gsTranslationSkills, gzRefAbility.sDisplayName)
            AdvCombatAbility_SetProperty("Display Name", gzRefAbility.sDisplayName)
            --fnWriteInfo(gzRefAbility.sDisplayName)
        else
            --fnWriteInfo(gzRefAbility.sSkillName)
        end
        
        -- |[Description Translation]|
        --Send translations to the builder file.
        --fnWriteInfo(gzRefAbility.sDescription)
        if(gzRefAbility.sSimpleDesc ~= nil) then
            --fnWriteInfo(gzRefAbility.sSimpleDesc)
        end
        
        --If a translation exists, run it here.
        local sUseDescription = Translate(gsTranslationSkills, gzRefAbility.sDescription)
        local sUseSimpleDesc = nil
        if(gzRefAbility.sSimpleDesc ~= nil) then
            sUseSimpleDesc = Translate(gsTranslationSkills, gzRefAbility.sSimpleDesc)
        end
        
        --Replace all [BR] tags with \n.
        sUseDescription = string.gsub(sUseDescription, "%[BR%]", "\n")
        if(sUseSimpleDesc ~= nil) then
            sUseSimpleDesc  = string.gsub(sUseSimpleDesc, "%[BR%]", "\n")
        end
        
        --Replace all [PCT] tags with %%
        sUseDescription = string.gsub(sUseDescription, "%[PCT%]", "%%%%")
        if(sUseSimpleDesc ~= nil) then
            sUseSimpleDesc  = string.gsub(sUseSimpleDesc, "%[PCT%]", "%%%%")
        end
        
        -- |[Complex Description]|
        AdvCombatAbility_SetProperty("Description", sUseDescription)
        AdvCombatAbility_SetProperty("Allocate Description Images", #gzRefAbility.saImages)
        for i = 1, #gzRefAbility.saImages, 1 do
            AdvCombatAbility_SetProperty("Description Image", i-1, gcfAbilityImgOffsetY, gzRefAbility.saImages[i])
        end
        AdvCombatAbility_SetProperty("Crossload Description Images")
        
        -- |[Simplified Description]|
        if(gzRefAbility.sSimpleDesc ~= nil) then
            AdvCombatAbility_SetProperty("Simplified Description", sUseSimpleDesc)
            AdvCombatAbility_SetProperty("Allocate Simplified Description Images", #gzRefAbility.saSimpleImages)
            for i = 1, #gzRefAbility.saSimpleImages, 1 do
                AdvCombatAbility_SetProperty("Simplified Description Image", i-1, gcfAbilityImgOffsetLgY, gzRefAbility.saSimpleImages[i])
            end
            AdvCombatAbility_SetProperty("Crossload Simplified Description Images")
        end
        
        -- |[Response Flags]|
        fnSetAbilityResponseFlags(gzRefAbility.iResponseType)
        
    DL_PopActiveObject()

    -- |[Clean Up]|
    if(gbBuildAbilityTranslation == true) then
        io.close(gfOutfile)
        gfOutfile = nil
    end

-- |[ ======================================= Job Taken ======================================== ]|
--Called when the character in question activates the job that contains this ability. The ability is
-- stored in the AdvCombatEntity.
elseif(iSwitchType == gciAbility_AssumeJob) then

-- |[ ================================= Combat: Combat Begins ================================== ]|
--Called when combat begins, to set up any local variables. Note that the Turn Begins code will be
-- called next, *after* turn order is resolved.
elseif(iSwitchType == gciAbility_BeginCombat) then

    --Common.
    local iUniqueID = RO_GetID()

    --If there is a passive applied by this ability, do that here:
    if(gzRefAbility.sPassivePath ~= nil or gzRefAbility.sPassivePrototype ~= nil) then

        --Get the owner for their ID.
        AdvCombatAbility_SetProperty("Push Owner")
            local iOriginatorID = RO_GetID()
        DL_PopActiveObject()
        
        --Script version.
        if(gzRefAbility.sPassivePath ~= nil) then
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 0, "Effect|" .. gzRefAbility.sPassivePath .. "|Originator:" .. iOriginatorID .. "|StoreID:" .. iUniqueID)
        
        --Prototype version.
        else
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 0, "Effect|" .. gsGlobalEffectPrototypePath .. "|Originator:" .. iOriginatorID .. "|StoreID:" .. 
                                  iUniqueID .. "|Prototype:" .. gzRefAbility.sPassivePrototype)
        end
        
        --Passive: Mark that we are tracking an effect.
        DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "/")
        VM_SetVar ("Root/Variables/Combat/" .. iUniqueID .. "/iIsManagingEffect", "N", 1.0)
        VM_SetVar ("Root/Variables/Combat/" .. iUniqueID .. "/iManagedEffectID", "N", 0.0) --Will be populated when the effect is created.
    end
    
    --If the charges max is nonzero, create charge variables.
    if(gzRefAbility.iChargesMax ~= nil and gzRefAbility.iChargesMax > 0) then
        local iUniqueID = RO_GetID()
        DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "/")
        VM_SetVar ("Root/Variables/Combat/" .. iUniqueID .. "/iChargesLeft", "N", gzRefAbility.iChargesMax)
        VM_SetVar ("Root/Variables/Combat/" .. iUniqueID .. "/iChargesMax",  "N", gzRefAbility.iChargesMax)
    end
    
    --Zero off the cooldown value.
    AdvCombatAbility_SetProperty("Cooldown", 0)

-- |[ ================================== Combat: Turn Begins =================================== ]|
--Called when the turn begins after turn order is resolved.
elseif(iSwitchType == gciAbility_BeginTurn) then

-- |[ ============================ Combat: Character Action Begins ============================= ]|
--Called when the character that possesses this ability begins their turn. This is where the ability
-- should enable or disable itself based on the character's MP/State/etc.
elseif(iSwitchType == gciAbility_BeginAction or iSwitchType == gciAbility_BeginFreeAction) then

    -- |[Passives]|
    --A passive ability will check to see if it is still on the character's jobs bar. If it is not, it must
    -- remove itself. If it is, it must activate itself.
    if(gzRefAbility.sPassivePath ~= nil or gzRefAbility.sPassivePrototype ~= nil) then

        --Get the ability ID.
        local iUniqueID = RO_GetID()

        --Get the owner for their ID. Check if this ability is still equipped.
        AdvCombatAbility_SetProperty("Push Owner")
            local iOriginatorID = RO_GetID()
            local bIsAbilityEquipped = AdvCombatEntity_GetProperty("Is Ability Equipped ID", iUniqueID)
        DL_PopActiveObject()
        
        --Check the effect ID. Passives are always managing an effect, but if the ID comes back zero, the effect does not exist.
        local iManagedEffectID  = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iManagedEffectID", "I") 
        
        --Check if the effect exists.
        local bEffectExists = AdvCombat_GetProperty("Does Effect Exist", iManagedEffectID)
        
        --If the effect does not exist, but we are managing an effect, spawn a new effect.
        if(bEffectExists == false) then
            
            --Path-based version:
            if(gzRefAbility.sPassivePath ~= nil) then
                AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 0, "Effect|" .. gzRefAbility.sPassivePath .. "|Originator:" .. iOriginatorID .. "|StoreID:" .. iUniqueID)
            
            --Prototype-based version:
            else
                AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 0, "Effect|" .. gsGlobalEffectPrototypePath .. "|Originator:" .. iOriginatorID .. "|StoreID:" .. 
                                      iUniqueID .. "|Prototype:" .. gzRefAbility.sPassivePrototype)
            end
            
            --Make sure the datalibrary variables exist.
            DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "/")
            VM_SetVar ("Root/Variables/Combat/" .. iUniqueID .. "/iIsManagingEffect", "N", 1.0)
            VM_SetVar ("Root/Variables/Combat/" .. iUniqueID .. "/iManagedEffectID", "N", 0.0)
        end
    end

    -- |[Default Setting]|
    --Default setting.
    if(gzRefAbility.bRespectsCooldown    == nil) then gzRefAbility.bRespectsCooldown    = false end
    if(gzRefAbility.iRequiredFreeActions == nil) then gzRefAbility.iRequiredFreeActions = 1     end
    if(gzRefAbility.bRespectActionCap    == nil) then gzRefAbility.bRespectActionCap    = false end
        
    -- |[Not Owner, Stop]|
    --Do nothing if the owner is not acting:
    if(AdvCombatAbility_GetProperty("Is Owner Acting") == false) then return end

    -- |[Always Usable]|
    --If this is set to true, always usable:
    if(gzRefAbility.bAlwaysAvailable == true) then
        AdvCombatAbility_SetProperty("Usable", true)
    
    -- |[Never Usable]|
    --Never available. Often used for passives.
    elseif(gzRefAbility.bNeverAvailable == true) then
        AdvCombatAbility_SetProperty("Usable", false)

    -- |[Character Action Begin]|
    --Execution for the start of an action:
    elseif(iSwitchType == gciAbility_BeginAction) then
    
        --Basic check.
        local bIsUsable = fnAbilityStandardBeginAction(gzRefAbility.iRequiredMP, gzRefAbility.bRespectsCooldown, gzRefAbility.iRequiredFreeActions, gzRefAbility.bRespectActionCap)
    
        --Has a CP requirement:
        if(bIsUsable and gzRefAbility.iRequiredCP > 0) then
            AdvCombatAbility_SetProperty("Usable", fnCheckCP(gzRefAbility.iRequiredCP))
        end
    
        --Has a fixed count of charges.
        if(gzRefAbility.iChargesMax ~= nil and gzRefAbility.iChargesMax > 0) then
        
            --At least one charge must be available.
            local iUniqueID = RO_GetID()
            local iChargesLeft = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iChargesLeft", "I")
            if(iChargesLeft < 1) then
                AdvCombatAbility_SetProperty("Usable", false)
                AdvCombatAbility_SetProperty("Charges Icon", "Root/Images/AdventureUI/Abilities/Cmb0")
            else
                fnAbilityStandardBeginAction(0, true, 1, true)
                
                local iUseCharges = iChargesLeft
                if(iChargesLeft > 9) then iUseCharges = 9 end
                AdvCombatAbility_SetProperty("Charges Icon", "Root/Images/AdventureUI/Abilities/Cmb" .. iUseCharges)
            end
        end
    
    -- |[Free Action Begin]|
    --Execution for after a free-action:
    else
    
        --Basic check.
        local bIsUsable = fnAbilityStandardBeginFreeAction(gzRefAbility.iRequiredMP, gzRefAbility.bRespectsCooldown, gzRefAbility.iRequiredFreeActions, gzRefAbility.bRespectActionCap)
    
        --Has a CP requirement:
        if(bIsUsable and gzRefAbility.iRequiredCP > 0) then
            AdvCombatAbility_SetProperty("Usable", fnCheckCP(gzRefAbility.iRequiredCP))
        end
    
        --Has a fixed count of charges.
        if(gzRefAbility.iChargesMax ~= nil and gzRefAbility.iChargesMax > 0) then
        
            --At least one charge must be available.
            local iUniqueID = RO_GetID()
            local iChargesLeft = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iChargesLeft", "I")
            if(iChargesLeft < 1) then
                AdvCombatAbility_SetProperty("Usable", false)
                AdvCombatAbility_SetProperty("Charges Icon", "Root/Images/AdventureUI/Abilities/Cmb0")
            else
                fnAbilityStandardBeginAction(0, true, 1, true)
                
                local iUseCharges = iChargesLeft
                if(iChargesLeft > 9) then iUseCharges = 9 end
                AdvCombatAbility_SetProperty("Charges Icon", "Root/Images/AdventureUI/Abilities/Cmb" .. iUseCharges)
            end
        end
    end

-- |[ ============================= Combat: Character Action Ends ============================== ]|
--Called after a character executes any action. Used for passives or post-reaction abilities. Is not
-- called if the action was a free action (that's above).
elseif(iSwitchType == gciAbility_PostAction) then

-- |[ ================================= Combat: Paint Targets ================================== ]|
--For player-controlled characters, selects which targets can be used by this ability. AI-controlled
-- characters may optionally bypass this and select targets directly.
--If no targets are painted, the UI will play a fail sound and disable target selection.
elseif(iSwitchType == gciAbility_PaintTargets) then
    AdvCombat_SetProperty("Run Target Macro", gzRefAbility.sTargetMacro, AdvCombatAbility_GetProperty("Owner ID"))
    
-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--When the player is selecting targets, a little box appears above the targets. This is the prediction
-- box. This script is called once for each target cluster created when this ability is selected, and
-- prediction boxes can be made for entities in that target cluster.
--The AdvCombatAbility will be the active object.
elseif(iSwitchType == gciAbility_BuildPredictionBox) then

    -- |[ ===================== Basic Module ===================== ]|
    --Setup the basic prediction package.
    local zUsePredictionPack = gzRefAbility.zPredictionPackage
    
    --Diagnostics.
    local bDiagnostics = Debug_GetFlag("Predictions: All") or Debug_GetFlag("Predictions: Execution")
    Debug_PushPrint(bDiagnostics, "Ability Standard, Build Prediction Box - Beginning.\n")
    Debug_Print("Skill name: " .. gzRefAbility.sSkillName .. "\n")
    Debug_Print("Job name: " .. gzRefAbility.sJobName .. "\n")
        
    -- |[ ===================== Other Values ===================== ]|
    --Get originator ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
    DL_PopActiveObject()
    
    --Get information about the cluster. This is needed to give each box a unique ID.
    local iClusterID = AdvCombat_GetProperty("Prediction Get Cluster ID")
    
    --Diagnostics.
    Debug_Print("Owner ID: " .. iOwnerID .. "\n")
    Debug_Print("Cluster ID: " .. iClusterID .. "\n")
    
    -- |[ =================== For Each Target ==================== ]|
    --Diagnostics.
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    Debug_Print("Running prediction builder on " .. iClusterTargetsTotal .. " targets.\n")
    
    --For each cluster:
    for i = 0, iClusterTargetsTotal-1, 1 do
        
        --Diagnostics.
        local iTargetID = AdvCombat_GetProperty("Prediction Get Targets ID", i)
        Debug_Print(" Target ID: " .. iTargetID .. "\n")
        
        --Build basic prediction.
        fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, gzRefAbility.zPredictionPackage)
    end
        
    --If an ability has a self-effect, this handles it.
    if(gzRefAbility.zSelfPredictionPack ~= nil) then
        Debug_Print("Adding self-effect prediction.\n")
        fnBuildPredictionBox(iOwnerID, iOwnerID, iClusterID, gzRefAbility.zSelfPredictionPack)
    end
    
    -- |[ =================== Self-Healing Case ================== ]|
    if(gzRefAbility.zPredictionPackage.bHasSelfHealingModule) then
        
        --Diagnostics.
        Debug_Print("Adding self-healing prediction.\n")
        
        --Quick-access.
        local zModule = gzRefAbility.zPredictionPackage.zSelfHealingModule
        
        --Get power
        AdvCombat_SetProperty("Push Entity By ID", iOwnerID)
            local iAttackPower = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
            local iHPMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
        DL_PopActiveObject()
        
        --Compute self-heal.
        zModule.iHealingFinal = math.floor((zModule.fHealingFactor * iAttackPower) + (zModule.fHealingPercent * iHPMax) + zModule.iHealingFixed)
        
        local sBoxName = "Prd" .. 0 .. "x" .. iOwnerID .. iClusterID
        AdvCombat_SetProperty("Prediction Create Box",              sBoxName)
        AdvCombat_SetProperty("Prediction Set Host",                sBoxName, iOwnerID)
        AdvCombat_SetProperty("Prediction Set Offsets",             sBoxName, 0, -100)
        AdvCombat_SetProperty("Prediction Allocate Strings",        sBoxName, 1)
        AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, 0, "Self-Heal: " .. zModule.iHealingFinal .. "[IMG0]")
        AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, 0, 1)
        AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, 0, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Health")
    end
    
    --Diagnostics.
    Debug_PopPrint("Ability Standard, Build Prediction Box - Finished normally.\n")

-- |[ ================================== Combat: Can Execute =================================== ]|
--Flags whether or not the event can actually run. If the originator got KOd by a DoT or a counterattack
-- or any other event, this will mark as false, and the event will stop here.
--This is only called for the ability in question.
elseif(iSwitchType == gciAbility_QueryCanRun) then
    AdvCombat_SetProperty("Push Event Originator")
        local bCanAct = AdvCombatEntity_GetProperty("Can Act")
    DL_PopActiveObject()
    if(bCanAct) then AdvCombat_SetProperty("Set Event Can Run", true) end

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[ ======================= Defaults ======================= ]|
    --Default set.
    if(gzRefAbility.bIsFreeAction == nil) then gzRefAbility.bIsFreeAction = false end
    
    -- |[ ================ Originator Statistics ================= ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
    
        --Get ID.
        local iOriginatorID = RO_GetID()
        
        --CP handler.
        if(gzRefAbility.iCPGain ~= nil and gzRefAbility.iRequiredCP ~= nil) then
            fnModifyCP(gzRefAbility.iCPGain - gzRefAbility.iRequiredCP)
        end
        
        --MP handler.
        if(gzRefAbility.iRequiredMP ~= nil) then
            fnModifyMP(gzRefAbility.iRequiredMP * -1)
        end
    
    DL_PopActiveObject()
    
    -- |[ ======================= Charges ======================== ]|
    --If the ability has charges, remove them here.
    if(gzRefAbility.iChargesMax ~= nil and gzRefAbility.iChargesMax > 0) then
        local iUniqueID = RO_GetID()
        local iChargesLeft = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iChargesLeft", "I")
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iChargesLeft", "N", iChargesLeft - 1)
    end
    
    -- |[ ==================== Ability Package =================== ]|
    --Create an ability package. This package comes with the default hit/miss values, animations, etc.
    local zExecPackage = gzRefAbility.zExecutionAbiPackage
    zExecPackage.iOriginatorID = iOriginatorID
    
    --Handling of free actions:
    if(gzRefAbility.bIsFreeAction) then
        
        --If the ability is a free action, but does not respect the action cap or consume any free actions,
        -- it is an "Effortless" action and can be used as many times as the player wants.
        if(gzRefAbility.iRequiredFreeActions == 0 and gzRefAbility.bRespectActionCap == false) then
            AdvCombat_SetProperty("Set As Effortless Action")
        
        --Normal free action.
        else
            AdvCombat_SetProperty("Set As Free Action")
        end
    end
    
    --Handling of cooldowns:
    if(gzRefAbility.iCooldown ~= nil and gzRefAbility.iCooldown > 0) then
        AdvCombatAbility_SetProperty("Cooldown", gzRefAbility.iCooldown)
    end
    
    -- |[ ==================== Effect Packages ==================== ]|
    --Create a local effect package. If they exist, populate them. The pattern is A/B/C, eventually
    -- this will move to using a list.
    local zEffectPackage = gzRefAbility.zExecutionEffPackage
    if(zEffectPackage ~= nil) then
        zEffectPackage.iOriginatorID = iOriginatorID
    end
    
    --B package.
    local zEffectPackageB = gzRefAbility.zExecutionEffPackageB
    if(zEffectPackageB ~= nil) then
        zEffectPackageB.iOriginatorID = iOriginatorID
    end
    
    --C package.
    local zEffectPackageC = gzRefAbility.zExecutionEffPackageC
    if(zEffectPackageC ~= nil) then
        zEffectPackageC.iOriginatorID = iOriginatorID
    end
    
    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        
        --Debug:
        if(zExecPackage ~= nil) then
            zExecPackage.sAbilityName = gzRefAbility.sSkillName
        end
        
        --Call standard function.
        local zOldRefAbility = gzRefAbility
        fnStandardExecution(iOriginatorID, i-1, zExecPackage, zEffectPackage, zEffectPackageB, zEffectPackageC)
        gzRefAbility = zOldRefAbility
        
        --If the ref ability has a threat multiplier, set it here. This applies even if the ability misses.
        --Note that the threat multiplier can be zero, or negative.
        if(gzRefAbility.fThreatMultiplier ~= nil) then
        
            --Get target ID.
            AdvCombat_SetProperty("Push Target", i-1)
                local iTargetID = RO_GetID()
            DL_PopActiveObject()
            
            --Get threat values from the originator.
            AdvCombat_SetProperty("Push Event Originator")
                local iOriginatorAttackPower = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
                local fThreatMultiplier = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_ThreatMultiplier)
            DL_PopActiveObject()
        
            --Compute.
            local iThreatToApply = math.floor(iOriginatorAttackPower * (fThreatMultiplier / 100.0) * gzRefAbility.fThreatMultiplier)
            if(iThreatToApply > 0) then
                AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetID, 0, "AI_APPLICATION|Threat|" .. iThreatToApply)
            end
        end
    end
    
    -- |[ ===================== Ally Effects ===================== ]|
    --If set, this effect applies to all allies. Note that the allies should not be the targets. This also applies to 
    -- the user, who is considered an ally.
    if(gzRefAbility.zAllyEffectPackage ~= nil) then
        
        --Determine the party of the originator.
        local iOriginatorParty = AdvCombat_GetProperty("Party Of ID", iOriginatorID)
        
        --Player's party:
        if(iOriginatorParty == gciACPartyGroup_Party) then
            local iPartySize = AdvCombat_GetProperty("Combat Party Size")
            for i = 0, iPartySize-1, 1 do
                local iPartyID = AdvCombat_GetProperty("Combat Party ID", i)
                gzRefAbility.zAllyEffectPackage.fnHandler(iOriginatorID, iPartyID, 0, gzRefAbility.zAllyEffectPackage, true, false)
            end
        
        --Enemy party:
        elseif(iOriginatorParty == gciACPartyGroup_Enemy) then
            local iPartySize = AdvCombat_GetProperty("Enemy Party Size")
            for i = 0, iPartySize-1, 1 do
                local iEnemyID = AdvCombat_GetProperty("Enemy ID", i)
                gzRefAbility.zAllyEffectPackage.fnHandler(iOriginatorID, iEnemyID, 0, gzRefAbility.zAllyEffectPackage, true, false)
            end
        end
    end
    
    -- |[ ===================== Self-Effects ===================== ]|
    if(gzRefAbility.zExecutionSelfPackage ~= nil) then
        gzRefAbility.zExecutionSelfPackage.fnHandler(iOriginatorID, iOriginatorID, 0, gzRefAbility.zExecutionSelfPackage, true, false)
    end

-- |[ =================================== Combat: Turn Ends ==================================== ]|
--Called after the turn ends, before turn order for the next turn is resolved. The ability does not
-- need to have been used for this to be called.
elseif(iSwitchType == gciAbility_TurnEnds) then

-- |[ ================================== Combat: Combat Ends =================================== ]|
--Called after combat ends. The passed in flag dictates if Victory/Defeat occurred. This occurs
-- before Doctor Bag/XP/JP etc is awarded.
elseif(iSwitchType == gciAbility_CombatEnds) then

-- |[ ================================== Combat: Event Queued ================================== ]|
--Fired whenever a "main" event is queued, which is when the player or an AI decides which action to
-- perform. All abilities equipped by all entities then fire a response script. They can then enqueue
-- their own events before or after the main one.
--This occurs before the ability calls with gciAbility_Execute.
elseif(iSwitchType == gciAbility_EventQueued) then

-- |[ ==================================== GUI: Apply Effect =================================== ]|
--When showing a character's statistics, all equipped abilities are queried. This is not reflected
-- in combat, this is GUI only.
elseif(iSwitchType == gciAbility_GUIApplyEffect) then

-- |[ ================================= Special: Clear Effects ================================= ]|
--When called, the ability will remove any effects it is managing. This is used by passives that are
-- class specific, such that when the character changes classes, they lose any lingering passives.
elseif(iSwitchType == gciAbility_SpecialClearEffects) then

    --Get the ability ID.
    local iUniqueID = RO_GetID()
    local iIsManagingEffect = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iIsManagingEffect", "I")
    local iManagedEffectID  = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iManagedEffectID", "I")
    
    --If the ability is not managing an effect, stop here.
    if(iIsManagingEffect == 0.0 or iManagedEffectID == 0.0) then return end
    
    --Order the effect to expire.
    AdvCombat_SetProperty("Remove Effect", iManagedEffectID)
    
    --Mark the ID as zero.
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iManagedEffectID", "N", 0.0)
    
    --Return out so we don't clear the gzRefAbility.
    return
end

-- |[ ========================================= Clean ========================================== ]|
--Clear off the ability so it doesn't cause any weird conflicts on syntax error.
gzRefAbility = nil
