-- |[ ================================== JX-101 Initialization ================================= ]|
--Creates and stores the named character in the party roster. If the character already exists, pushes
-- the character and modifies their properties.
local sCharacterName = "JX-101"

-- |[ ======================================= Lua Globals ====================================== ]|
-- |[Ability Sectioning]|
--These globals are reset when the game restarts to avoid possible overlaps. These store the ability
-- prototypes for each class.
gzPrototypes.Combat.JX101 = {}
gzPrototypes.Combat.JX101.Common    = {}
gzPrototypes.Combat.JX101.Tactician = {}

-- |[ =================================== Character Handling =================================== ]|
-- |[Register or Push]|
--Register if character does not exist.
local bIsCreating = false
if(AdvCombat_GetProperty("Does Party Member Exist", sCharacterName) == false)  then
    bIsCreating = true
    AdvCombat_SetProperty("Register Party Member", sCharacterName)

--Push character.
else
    AdvCombat_SetProperty("Push Party Member", sCharacterName)
end

-- |[Call Script]|
AdvCombatEntity_SetProperty("Response Script", fnResolvePath() .. "100 Combat Script.lua")

-- |[Base Statistics]|
--Persistent Stats
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_HPMax,   100)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_MPMax,   100)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_MPRegen,  10)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_CPMax,    10)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_StunCap, 100)

--Free-Action Handlers
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_FreeActionMax, 3)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_FreeActionGen, 1)

--Combat Stats
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Initiative, 40)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Attack,     35)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Accuracy,   30)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Evade,      20)

--Base resistances.
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Protection,     3)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Slash,   gciNormalResist+0)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Strike,  gciNormalResist+0)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Pierce,  gciNormalResist+0)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Flame,   gciNormalResist+0)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Freeze,  gciNormalResist+0)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Shock,   gciNormalResist+0)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Crusade, gciNormalResist+0)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Obscure, gciNormalResist+0)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Bleed,   gciNormalResist+0)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Poison,  gciNormalResist+0)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Corrode, gciNormalResist+0)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Terrify, gciResistanceForImmunity)

--Threat Multiplier. Default 100 for all entities.
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_ThreatMultiplier, 100)

-- |[ ===================================== Equipment Slots ==================================== ]|
--Primary Weapon
AdvCombatEntity_SetProperty("Create Equipment Slot", "Weapon")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Weapon", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Weapon", false)

--Uses primary weapon to compute damage type. Can be modified by gems.
AdvCombatEntity_SetProperty("Set Equipment Slot Used For Weapon Damage", "Weapon")

--Swappable Weapons
AdvCombatEntity_SetProperty("Create Equipment Slot", "Weapon Backup A")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Weapon Backup A", false)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Weapon Backup A", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Is Alt Weapon", "Weapon Backup A", true)
AdvCombatEntity_SetProperty("Create Equipment Slot", "Weapon Backup B")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Weapon Backup B", false)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Weapon Backup B", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Is Alt Weapon", "Weapon Backup B", true)

--Body Armor
AdvCombatEntity_SetProperty("Create Equipment Slot", "Body")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Body", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Body", true)

--Accessory Slots
AdvCombatEntity_SetProperty("Create Equipment Slot", "Accessory A")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Accessory A", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Accessory A", true)
AdvCombatEntity_SetProperty("Create Equipment Slot", "Accessory B")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Accessory B", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Accessory B", true)

--Equippable Items
AdvCombatEntity_SetProperty("Create Equipment Slot", "Item A")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Item A", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Item A", true)
AdvCombatEntity_SetProperty("Create Equipment Slot", "Item B")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Item B", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Item B", true)

-- |[ ====================================== Job Handling ====================================== ]|
--Create JX-101's common abilities, shared by many classes.
local sJobAbilityPath = gsRoot .. "Combat/Party/JX-101/Jobs/Common/Abilities/"
LM_ExecuteScript(sJobAbilityPath .. "Attack.lua", gciAbility_Create)
LM_ExecuteScript(sJobAbilityPath .. "Guard.lua",  gciAbility_Create)
LM_ExecuteScript(sJobAbilityPath .. "Locked.lua", gciAbility_Create)

--Job Change Abilities
--LM_ExecuteScript(sJobAbilityPath .. "Jobchange_Fencer.lua", gciAbility_Create)

--Equipment Change Abilities
LM_ExecuteScript(gsRoot .. "Combat/Party/Common/Abilities/Swap Weapon A.lua", gciAbility_Create)
LM_ExecuteScript(gsRoot .. "Combat/Party/Common/Abilities/Swap Weapon B.lua", gciAbility_Create)

--Call all of JX-101's job subscripts.
local sCharacterJobPath = gsRoot .. "Combat/Party/JX-101/Jobs/"
LM_ExecuteScript(sCharacterJobPath .. "Tactician/000 Job Script.lua", gciJob_Create)
AdvCombatEntity_SetProperty("Rebuild Job Skills UI")

--Set JX-101 to her default job.
AdvCombatEntity_SetProperty("Active Job", "Tactician")

--Once all abilities are built across all jobs, order jobs to associate with abilities. Any abilities
-- associated with a job can be purchased in the skills UI using that job's JP, and equipped from
-- that job's submenu. If an ability is not associated with any job, and is not auto-equipped by
-- any jobs when the job is assumed, the ability is not usable by the player!
LM_ExecuteScript(sCharacterJobPath .. "Tactician/000 Job Script.lua", gciJob_JobAssembleSkillList)

-- |[ ==================================== Creation Callback =================================== ]|
--If this was a creation call of the character, fire its script callback.
if(bIsCreating) then
    local sResponsePath = AdvCombatEntity_GetProperty("Response Path")
    LM_ExecuteScript(sResponsePath, gciCombatEntity_Create)
end

-- |[ ======================================== Clean Up ======================================== ]|
--Pop the activity stack.
DL_PopActiveObject()
