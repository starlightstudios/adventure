-- |[ ========================================== Guard ========================================= ]|
-- |[Description]|
--Increases protection for 1 turn, extra CP generation.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Ability Prototype =================================== ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.JX101.Common.Guard == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Common", "Guard", "$SkillName", gciJP_Cost_NoCost, gbIsNotFreeAction, "Direct", "Active", "Defend", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "+10 [Prt] for 1 turn.\n\n\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Take cover and reduce damage.\n\n\n\nIncreases [Prt](Protection) greatly.\nGenerates extra [CPIco](CP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, giNoCPCost, giNoCooldown, "Target Self", 2)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Protect")
    zAbiStruct.zExecutionAbiPackage:fnSetAsBuff()
  --zAbiStruct.zExecutionAbiPackage:fnAddAnimation(psAnimationName, piTimerOffset, $pfXOffset, $pfYOffset)
    zAbiStruct.zExecutionAbiPackage:fnAddAnimation("Buff Defense", 0)
    
    -- |[Effect Package]|
  --zAbiStruct:fnAddBuff(psPrototypeName, $psApplyText, $psApplyColorString, $psApplyAnim, $psApplySound)
    zAbiStruct:fnAddBuff("JX101.Common.Guard", "Guarding!", "Color:Purple")
    
    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
	local saDescription = {}
	saDescription[1] = "Protect yourself."
	saDescription[2] = "Increases [Prt](Protection)."
      --EffectList:fnCreateBuffPrototype(psName, psDisplayName, psIcon, piTurns, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreateBuffPrototype(sLocalPrototypeName, "Guard", "Defend", 2, "Prt|Flat|10", saDescription)
    end

    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.JX101.Common.Guard = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.JX101.Common.Guard

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
