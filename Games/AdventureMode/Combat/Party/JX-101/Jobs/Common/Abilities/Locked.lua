-- |[ ========================================= Locked ========================================= ]|
-- |[Description]|
--Cannot be used.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Ability Prototype =================================== ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.JX101.Common.Locked == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Common", "Locked", "$SkillName", gciJP_Cost_NoCost, gbIsNotFreeAction, "Direct", "Active", "Lock", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "This slot is locked. Find skillbooks in the world to unlock\nit and gain JP!"
    zAbiStruct.sSimpleDescMarkdown  = "This slot is locked. Find skillbooks in the world to\nunlock it and gain JP!"

    -- |[Usability Variables]|
    zAbiStruct.bNeverAvailable = true
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.JX101.Common.Locked = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.JX101.Common.Locked

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
