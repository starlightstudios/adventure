-- |[ ======================================== Scavenge ======================================== ]|
-- |[Description]|
--Hits enemy for normal damage, recovers 50 HP.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.JX101.Tactician.Scavenge == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Tactician", "Scavenge", "$SkillName", gciJP_Cost_NoCost, gbIsNotFreeAction, "Direct", "Active", "SX-399|Scavenge", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\nRestores 50 [Hlt], self.\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Damage the enemy and... borrow... some parts.\nDeals increased weapon damage, restores\n50 HP.\n\n\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(20, giNoCPCost, giNoCooldown, "Target Enemies Single", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Weapon")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_UseWeapon, 5, 1.30)
    
    -- Adding self-heal
  --zAbiStruct.zExecutionAbiPackage:fnAddSelfHeal(piHealingBase, pfHealingScaleFactor, pfHealPctOfMaxHP)
    zAbiStruct.zExecutionAbiPackage:fnAddSelfHeal(50, 0.00, 0.00)
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "JX-101|Offense|General:Swing|Ability:Scavenge"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.JX101.Tactician.Scavenge = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.JX101.Tactician.Scavenge

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
