-- |[ ================================== Years of Experience =================================== ]|
-- |[Description]|
--Increases all stats.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.JX101.Tactician.YearsOfExperience == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Tactician", "Years Of Experience", "$SkillName", gciJP_Cost_NoCost, gbIsNotFreeAction, "Buff", "Passive", "SX-399|YearsOfExperience", "Passive")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "+10%% all stats.\n\n\n\n\n\nPassive."
    zAbiStruct.sSimpleDescMarkdown  = "You've learned a thing or two over the\ndecades.\nIncreases all stats by 10%%.\n\n\nPassive."
    
    -- |[Passive Effect]|
    zAbiStruct.bNeverAvailable   = true --Passive abilities have no usability requirements.
    zAbiStruct.sPassivePrototype = "JX101.Tactician.YearsOfExperience"
    zAbiStruct.bHasGUIEffects    = true

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    if(EffectList:fnEntryExists(zAbiStruct.sPassivePrototype) == false) then
	local saDescription = {}
	saDescription[1] = "The wisdom gained from decades of warfare."
	saDescription[2] = "Increases all stats. Passive."
      --EffectList:fnCreatePassivePrototype(psName, psDisplayName, psIcon, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreatePassivePrototype(zAbiStruct.sPassivePrototype, "Years Of Experience", "SX-399|YearsOfExperience", "HPMax|Pct|0.10 Atk|Pct|0.10 Ini|Pct|0.10 Evd|Pct|0.10 Acc|Pct|0.10", saDescription, {{"Is Positive", 1}, {"Is Passive", 1}})
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.JX101.Tactician.YearsOfExperience = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.JX101.Tactician.YearsOfExperience

-- |[ ===================================== All Other Cases ==================================== ]|
if(iSwitchType ~= gciAbility_GUIApplyEffect) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

-- |[ ==================================== GUI: Apply Effect =================================== ]|
--When showing a character's statistics, all equipped abilities are queried. This is not reflected
-- in combat, this is GUI only.
--The standard does not handle this.
elseif(iSwitchType == gciAbility_GUIApplyEffect) then
    AdvCombatAbility_SetProperty("Push Owner")
        fnApplyStatBonusPct(gciStatIndex_HPMax, 0.10)
        fnApplyStatBonusPct(gciStatIndex_Attack, 0.10)
        fnApplyStatBonusPct(gciStatIndex_Initiative, 0.10)
        fnApplyStatBonusPct(gciStatIndex_Accuracy, 0.10)
        fnApplyStatBonusPct(gciStatIndex_Evade, 0.10)
    DL_PopActiveObject()
    gzRefAbility = nil
end
