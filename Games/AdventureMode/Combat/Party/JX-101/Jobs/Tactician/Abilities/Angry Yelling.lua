-- |[ ====================================== Angry Yelling ===================================== ]|
-- |[Description]|
--Deals Terrify damage, reduces protection.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.JX101.Tactician.AngryYelling == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Tactician", "Angry Yelling", "$SkillName", gciJP_Cost_NoCost, gbIsNotFreeAction, "Direct", "Active", "SX-399|AngryYelling", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\n[HEffect][Str6][Tfy] -5[Prt] / 5 [Turns].\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Shout and make a real stink to scare the\nenemies!\nDeals reduced [Tfy](Terrify) damage,\nreduces enemy [Prt](Protection) for 5 turns.\n\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(20, giNoCPCost, giNoCooldown, "Target Enemies All", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Terrify")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Terrifying, 5, 0.60)
    
    -- |[Execution Effect Package]|
  --zAbiStruct:fnAddEffect(piStrNormal, piStrCrit, piApplyType, psApplyText, psCritApplyText, psApplyColorString, psaApplyBonus, psaApplyMalus, psaSeverityBonus, psaSeverityMalus)
    zAbiStruct:fnAddEffect(6, 9, gciDamageType_Terrifying, "Cowering!", "Badly Cowering!", "Color:Purple")
    
    -- |[Use of Effect Prototype]|
    zAbiStruct.zExecutionEffPackage.sPrototypeName = "JX101.Tactician.AngryYelling"
    
    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
	local saDescription = {}
	saDescription[1] = "Get out of her way, she's on the warpath!"
	saDescription[2] = "Reduces [Prt](Protection)."
      --EffectList:fnCreateStatPrototype(pzEffectpackage, psName, pbIsBuff, psDisplayName, psIcon, piTurns, piApplyStrength, psStatString, psaDescription, $pzaTagList, $piCritTurns, $piCritApplyStrength)
        EffectList:fnCreateStatPrototype(zAbiStruct.zExecutionEffPackage, sLocalPrototypeName, gbIsDebuff, "Angry Yelling", "SX-399|AngryYelling", 5, 6, "Prt|Flat|-5", saDescription, {{"Is Negative", 1}}, 7, 9)
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "JX-101|Ability:AngryYelling"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.JX101.Tactician.AngryYelling = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.JX101.Tactician.AngryYelling

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
