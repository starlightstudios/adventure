-- |[ ================================== Build Secondary Menu ================================== ]|
--Sanya can change classes in combat. This subroutine determines the current class and populates
-- what classes are available.
local sSanyaForm = VM_GetVar("Root/Variables/Global/Sanya/sForm", "S")

-- |[ ==================================== List Construction =================================== ]|
--If not done already, construct a list of all job changes available.
if(gzaSanyaSecondaryMenu == nil) then

    --Adder function.
    local zaList = {}
    local function fnAdd(psVarName, psExclusiveName, piXPos, piYPos, psAbilityName)
        local i = #zaList + 1
        zaList[i] = {}
        zaList[i].sVariable = psVarName
        zaList[i].sExclusiveName = psExclusiveName
        zaList[i].iXPos = piXPos
        zaList[i].iYPos = piYPos
        zaList[i].sAbilityName = psAbilityName
    end

    --List.
    fnAdd("TRUE",                                        "Human",   0, 0, "JobChange|Job Change - Hunter")
    fnAdd("Root/Variables/Global/Sanya/iHasSevaviForm",  "Sevavi",  1, 0, "JobChange|Job Change - Agontea")
    fnAdd("Root/Variables/Global/Sanya/iHasWerebatForm", "Werebat", 2, 0, "JobChange|Job Change - Skystriker")
    fnAdd("Root/Variables/Global/Sanya/iHasHarpyForm",   "Harpy",   3, 0, "JobChange|Job Change - Ranger")
    fnAdd("Root/Variables/Global/Sanya/iHasBunnyForm",   "Bunny",   4, 0, "JobChange|Job Change - Scofflaw")

    --Store.
    gzaSanyaSecondaryMenu = zaList
end

-- |[ ==================================== Menu Construction =================================== ]|
--Build the menu layout.
for i = 1, #gzaSanyaSecondaryMenu, 1 do
    
    --Compute position.
    local iX = gciAbility_Tactics_FormStartX + gzaSanyaSecondaryMenu[i].iXPos
    local iY = gciAbility_Tactics_FormStartY + gzaSanyaSecondaryMenu[i].iYPos
    
    --If the variable name is "TRUE" then always place this.
    if(gzaSanyaSecondaryMenu[i].sVariable == "TRUE") then
        AdvCombatEntity_SetProperty("Set Ability Slot", iX, iY, gzaSanyaSecondaryMenu[i].sAbilityName)
    
    --Otherwise, the variable must be 1.0.
    elseif(VM_GetVar(gzaSanyaSecondaryMenu[i].sVariable, "N") == 1.0) then
        AdvCombatEntity_SetProperty("Set Ability Slot", iX, iY, gzaSanyaSecondaryMenu[i].sAbilityName)
    end
end