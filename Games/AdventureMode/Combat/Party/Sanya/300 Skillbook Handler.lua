-- |[ ================================= Sanya Skillbook Handler ================================ ]|
--Call this file with the number of the skillbook in question, and JP and unlocks will be awarded as needed.
if(fnArgCheck(1) == false) then return end
local iLevel = LM_GetScriptArgument(0, "I")

-- |[Sanya is Not In the Party]|
if(AdvCombat_GetProperty("Is Member In Active Party", "Sanya") == false) then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Stalker's Scribblings.[P] Doesn't seem relevant for anyone in the party.)") ]])
    fnCutsceneBlocker()
    return
end

-- |[Activate Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)

-- |[Common Code]|
--Check if this skillbook has been read already. If not, award JP and handle unlocks.
local iCheckVar = VM_GetVar("Root/Variables/Global/Sanya/iSkillbook" .. iLevel, "N")
if(iCheckVar == 0.0) then
    
    --Mark the skillbook.
    VM_SetVar("Root/Variables/Global/Sanya/iSkillbook" .. iLevel, "N", 1.0)
    
    --Increment the skillbooks total.
    local iSkillbookTotal = VM_GetVar("Root/Variables/Global/Sanya/iSkillbookTotal", "N") + 1
    VM_SetVar("Root/Variables/Global/Sanya/iSkillbookTotal", "N", iSkillbookTotal)
    
    --Award JP/Abilities
    AdvCombat_SetProperty("Push Party Member", "Sanya")
    
        --Common strings.
        local sString = "WD_SetProperty(\"Append\", \"Sanya:[E|Smirk] (Skillbook found:: Stalker's Scribblings, Volume " .. iLevel .. ".)[B][C]\")"
        fnCutscene(sString)
    
        --JP.
        local iGlobalJP = AdvCombatEntity_GetProperty("Global JP")
        AdvCombatEntity_SetProperty("Current JP", iGlobalJP + 100)
        
        --Check if this unlocked a class ability.
        if(iSkillbookTotal >= 1.0 and iSkillbookTotal <= 4.0) then
            fnCutscene([[ Append("Sanya:[E|Smirk] (Gained 100 JP for Sanya!)[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Smirk] (Unlocked a new job ability slot for Sanya!)") ]])
        else
            fnCutscene([[ Append("Sanya:[E|Smirk] (Gained 100 JP for Sanya!)") ]])
        end

        --Execute the current job's SwitchTo script.
        AdvCombatEntity_SetProperty("Push Job S", "Active")
            AdvCombatJob_SetProperty("Fire Script", gciJob_SwitchTo)
        DL_PopActiveObject()
        
    DL_PopActiveObject()

--Already found.
else
    local sString = "WD_SetProperty(\"Append\", \"Sanya:[E|Smirk] (Stalker's Scribblings, Volume " .. iLevel .. ". You have already read this skillbook.)\")"
    fnCutscene(sString)
end