-- |[ ======================================= Aimed Shot ======================================= ]|
-- |[Description]|
--Immediately ends the player's turn and places an AI on them to fire a powerful shot at the start
-- of their next turn.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzSanyaHunterAimedShot == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Hunter"
    zAbiStruct.sSkillName    = "Aimed Shot"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Sanya|Hunter|AimedShot"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\nRequires a turn to aim.\nFires automatically at start of next action.\nInflicts 'Slow' while aiming.\nCannot miss.\n\n[Costs]"
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Aim carefully and fire a powerful shot.\nTakes a turn to aim and fires at the start of\nyour next action.\nInflicts 'Slow' while aiming.\n\n"

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false
    zAbiStruct.iRequiredMP = 20
    zAbiStruct.iRequiredCP = 0
    zAbiStruct.bRespectsCooldown = true
    zAbiStruct.iCooldown = 0
    zAbiStruct.bIsFreeAction = false
    zAbiStruct.iRequiredFreeActions = 0
    zAbiStruct.bRespectActionCap = false

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies Single"

    --Effects
    zAbiStruct.iCPGain = 1
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Prediction Package]|
    --Package used to display predictions about the ability based on enemy properties.
    --All the modules are optional, but at least one should exist to display *something*.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Crit Module
    zAbiStruct.zPredictionPackage.bHasCritModule = true
    zAbiStruct.zPredictionPackage.zCritModule.iCritThreshold = 100
    zAbiStruct.zPredictionPackage.zCritModule.fCritBonus     = 1.25
    zAbiStruct.zPredictionPackage.zCritModule.iCritStun      = 25
    
    --Damage Module
    zAbiStruct.zPredictionPackage.bHasDamageModule = true
    zAbiStruct.zPredictionPackage.zDamageModule.bUseWeaponType = true
    zAbiStruct.zPredictionPackage.zDamageModule.iDamageType = gciDamageType_Piercing
    zAbiStruct.zPredictionPackage.zDamageModule.fDamageFactor = 2.50
    zAbiStruct.zPredictionPackage.zDamageModule.iScatterRange = 15
    
    --Additional lines. These are plain text/symbols that do not have special properties.
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "Requires a turn to aim."
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    --Basic package.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Weapon")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits   = true 
    zAbiStruct.zExecutionAbiPackage.fDamageFactor = 2.50
    zAbiStruct.zExecutionAbiPackage.zaTags = {{"Is Attack", 1}}
    
    -- |[Execution Self-Effect Package]|
    --This applies Slow to the user. It is used when the AI initializes.
    zAbiStruct.zExecutionSelfPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides.
    zAbiStruct.zExecutionSelfPackage.sEffectPath     = fnResolvePath() .. "../Effects/Aimed Shot.lua"
    zAbiStruct.zExecutionSelfPackage.sEffectCritPath = fnResolvePath() .. "../Effects/Aimed Shot.lua"
    zAbiStruct.zExecutionSelfPackage.sApplyText = "Aiming..."
    zAbiStruct.zExecutionSelfPackage.sApplyTextCol = "Color:White"
    zAbiStruct.zExecutionSelfPackage.sApplyAnimation = "Null"
    zAbiStruct.zExecutionSelfPackage.sApplySound = "Combat\\|Impact_Buff"
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzSanyaHunterAimedShot = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzSanyaHunterAimedShot

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
if(iSwitchType == gciAbility_Execute) then
    
    -- |[Setup]|
    --Check if the user has the "Aimed Shot" effect. This is done via a tag.
    AdvCombat_SetProperty("Push Event Originator")
    
        --Store the ID.
        local iOriginatorID = RO_GetID()
        
        --Store how many times the tag "Aimed Shot Effect" is on the entity. It only needs to be once.
        local iAimedShotTag = AdvCombatEntity_GetProperty("Tag Count", "Aimed Shot Effect")
    
    DL_PopActiveObject()
    
    -- |[ =================== Tag Not Present ==================== ]|
    --If the tag is not present, it's because the player is selecting the ability. In that case, we 
    -- apply the effect and the AI that will auto-execute it.
    if(iAimedShotTag < 1) then
    
        -- |[Apply AI]|
        --Set the AI but don't run it.
        AdvCombat_SetProperty("Push Event Originator")
            AdvCombatEntity_SetProperty("AI Script", fnResolvePath() .. "../AIs/Aimed Shot.lua")
        DL_PopActiveObject()
        
        --Get the target the player selected.
        AdvCombat_SetProperty("Push Target", 0)
            local iZeroTargetID = RO_GetID()
        DL_PopActiveObject()
        
        --Get the name of the executing ability.
        local sAbilityInternalName = AdvCombatAbility_GetProperty("Internal Name")
        
        --Populate the AI's variables it will need.
        DL_AddPath("Root/Variables/Combat/" .. iOriginatorID .. "_AI/")
        VM_SetVar("Root/Variables/Combat/"  .. iOriginatorID .. "_AI/iAimedShotTargetID", "N", iZeroTargetID)
        VM_SetVar("Root/Variables/Combat/"  .. iOriginatorID .. "_AI/sMasterNameOfAbility", "S", sAbilityInternalName)
    
        -- |[Originator Statistics]|
        --Gain CP, lose MP.
        AdvCombat_SetProperty("Push Event Originator")
            
            --CP handler.
            if(gzRefAbility.iCPGain ~= nil and gzRefAbility.iRequiredCP ~= nil) then
                fnModifyCP(gzRefAbility.iCPGain - gzRefAbility.iRequiredCP)
            end
            
            --MP handler.
            if(gzRefAbility.iRequiredMP ~= nil) then
                fnModifyMP(gzRefAbility.iRequiredMP * -1)
            end
        DL_PopActiveObject()
        
        -- |[Apply Effect To Self]|
        --This will tell the AI to fire the shot.
        gzRefAbility.zExecutionSelfPackage.fnHandler(iOriginatorID, iOriginatorID, 0, gzRefAbility.zExecutionSelfPackage, true, false)
    
    -- |[ ==================== Tag Is Present ==================== ]|
    --The tag is present. Fire the ability and remove the tag. The AI should remove itself when
    -- this happens and return to the zero state.
    else
    
        -- |[Execution]|
        --Get how many targets were painted by this ability, iterate across them.
        local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
        for i = 1, iTargetsTotal, 1 do
            fnStandardExecution(iOriginatorID, i-1, gzRefAbility.zExecutionAbiPackage, nil)
        end
    
        -- |[Effect Removal]|
        --Create a list of all offending effects. We must get all of them before removing any as each removal
        -- will re-order the list.
        local iaTagTable = {}
        AdvCombat_SetProperty("Push Event Originator")
        
            --Iterate across effects.
            local iTotalEffectsToRemove = AdvCombatEntity_GetProperty("Effects With Tag", "Aimed Shot Effect")
            for i = 0, iTotalEffectsToRemove-1, 1 do
                local iIDOfEffect = AdvCombatEntity_GetProperty("Effect ID With Tag", "Aimed Shot Effect", i)
                table.insert(iaTagTable, iIDOfEffect)
            end
        
            --Order the program to remove all effects.
            for i = 1, #iaTagTable, 1 do
                AdvCombat_SetProperty("Remove Effect", iaTagTable[i])
            end
        DL_PopActiveObject()
    end

-- |[ ==================================== Standard Handler ==================================== ]|
--Activate normally.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
