-- |[ ==================================== Penetrator Round ==================================== ]|
-- |[Description]|
--Major damage, hits all targets but in a random order, decreasing damage with each hit.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzSanyaHunterPenetratorRound == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Hunter"
    zAbiStruct.sSkillName    = "Penetrator Round"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Special
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Sanya|Hunter|PenetratorRound"
    zAbiStruct.sCPIcon       = "Root/Images/AdventureUI/Abilities/Cmb4"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. Always hits.\nStrikes all enemies. First enemy takes full damage.\nAfter than, hits in a random order, each target taking\n30%% less damage.\nMinimum 50%% damage.\n\n[Costs]"
    zAbiStruct.sDescription = ""
    zAbiStruct.saImages = {}
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Fire a round through all the enemies. Deals\nmassive damage to the first enemy hit, then hits\nall other enemies in random order dealing less\ndamage as it goes.\n\n"..
                                     "Costs [CPCost][CPIco](CP)."
    zAbiStruct.sSimpleDesc = ""
    zAbiStruct.saSimpleImages = {}

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false  --Set to true to ignore all requirements.
    zAbiStruct.iRequiredMP = 0           --MP required to use the ability.
    zAbiStruct.iRequiredCP = 4           --CP required to use the ability.
    zAbiStruct.bRespectsCooldown = true  --If this ability has a cooldown, decrements it.
    zAbiStruct.iCooldown = 0             --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
    zAbiStruct.bIsFreeAction = false     --Flags as a free action.
    zAbiStruct.iRequiredFreeActions = 0  --Free actions set this as 1, everything else as 0.
    zAbiStruct.bRespectActionCap = false --Characters with multiple free actions have a hard action cap. This flag checks that cap.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies Single"

    --Effects
    zAbiStruct.iCPGain = 0
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Crit Module
    zAbiStruct.zPredictionPackage.bHasCritModule = true
    zAbiStruct.zPredictionPackage.zCritModule.iCritThreshold = 100
    
    --Damage Module
    zAbiStruct.zPredictionPackage.bHasDamageModule = true
    zAbiStruct.zPredictionPackage.zDamageModule.bUseWeaponType = true
    zAbiStruct.zPredictionPackage.zDamageModule.fDamageFactor = 3.00
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Weapon")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.fDamageFactor = 3.00
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits   = true
    zAbiStruct.zExecutionAbiPackage.zaTags = {{"Is Attack", 1}}
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzSanyaHunterPenetratorRound = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzSanyaHunterPenetratorRound

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
if(iSwitchType == gciAbility_Execute) then

    -- |[ ======================= Resetting ====================== ]|
    --Reset the attack power back to maximum.
    gzRefAbility.zExecutionAbiPackage.fDamageFactor = 3.00
    
    -- |[ ================ Originator Statistics ================= ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
    
        --Get ID.
        local iOriginatorID = RO_GetID()
        
        --CP handler.
        if(gzRefAbility.iCPGain ~= nil and gzRefAbility.iRequiredCP ~= nil) then
            fnModifyCP(gzRefAbility.iCPGain - gzRefAbility.iRequiredCP)
        end
        
        --MP handler.
        if(gzRefAbility.iRequiredMP ~= nil) then
            fnModifyMP(gzRefAbility.iRequiredMP * -1)
        end
    DL_PopActiveObject()
    
    -- |[ ==================== Ability Package =================== ]|
    --Create an ability package. This package comes with the default hit/miss values, animations, etc.
    local zAbilityPackage = gzRefAbility.zExecutionAbiPackage
    zAbilityPackage.iOriginatorID = iOriginatorID
    
    -- |[ ==================== Effect Package ==================== ]|
    --Create an effect package and populate it.
    local zEffectPackage = gzRefAbility.zExecutionEffPackage
    if(zEffectPackage ~= nil) then
        zEffectPackage.iOriginatorID = iOriginatorID
    end
    
    -- |[ ===================== First Target ===================== ]|
    --Get how many targets were painted by this ability, iterate across them.
    local iZeroID = AdvCombat_GetProperty("ID Of Target", 0)
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        AdvCombat_SetProperty("Push Target", i-1)
            local iTargetID = RO_GetID()
        DL_PopActiveObject()
        fnStandardExecution(iOriginatorID, i-1, zAbilityPackage, zEffectPackage)
    end

    -- |[ ================== Subsequent Targets ================== ]|
    --All other targets also get hit by the attack, but in a random order. First, run the target macro to hit all targets.
    local iTotalEnemies = AdvCombat_GetProperty("Enemy Party Size")
    local iaTargetList = {}
    for i = 0, iTotalEnemies-1, 1 do
        local iNewTargetID = AdvCombat_GetProperty("Enemy ID", i)
        if(iZeroID ~= iNewTargetID) then
            table.insert(iaTargetList, iNewTargetID)
        end
    end
    
    --Now run across the table in a random order.
    local iTargetCount = 1
    while(#iaTargetList > 0) do
    
        --Roll an ID.
        local iRoll = LM_GetRandomNumber(1, #iaTargetList)
        local iUseID = iaTargetList[iRoll]
        
        --Decrease damage:
        gzRefAbility.zExecutionAbiPackage.fDamageFactor = gzRefAbility.zExecutionAbiPackage.fDamageFactor * 0.70
        
        --Minimum:
        if(gzRefAbility.zExecutionAbiPackage.fDamageFactor < 0.50) then gzRefAbility.zExecutionAbiPackage.fDamageFactor = 0.50 end
    
        --Execute on that target.
        fnAbilityHandler(iOriginatorID, iUseID, iTargetCount, zAbilityPackage, nil)
        
        --Remove that target from the list.
        table.remove(iaTargetList, iRoll)
        
        --Increment the target count which spreads the attack out.
        iTargetCount = iTargetCount + 1
    end
    
    -- |[Clean]|
    --Reset back to max for future calls, in case the UI needs to be rebuilt.
    gzRefAbility.zExecutionAbiPackage.fDamageFactor = 3.00
    
-- |[ ===================================== Standard Cases ===================================== ]|
--Run the standard script.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
