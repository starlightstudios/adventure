-- |[ ==================================== Protective Rage ===================================== ]|
-- |[Description]|
--Deals normal damage but increases damage based on where Sanya is on the target's threat list.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzSanyaHunterProtectiveRage == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Hunter"
    zAbiStruct.sSkillName    = "Protective Rage"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Sanya|Hunter|ProtectiveRage"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\nIncreases damage if Sanya is not the highest threat.\nCauses double threat.\n\n\n[Costs]"
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "What do you mean you hate her more!?\nDeals increased damage if Sanya is not #1 on the\nenemy's threat list.\nCauses double threat.\n\nCosts [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false  --Set to true to ignore all requirements.
    zAbiStruct.iRequiredMP = 20          --MP required to use the ability.
    zAbiStruct.iRequiredCP = 0           --CP required to use the ability.
    zAbiStruct.bRespectsCooldown = true  --If this ability has a cooldown, decrements it.
    zAbiStruct.iCooldown = 0             --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
    zAbiStruct.bIsFreeAction = false     --Flags as a free action.
    zAbiStruct.iRequiredFreeActions = 0  --Free actions set this as 1, everything else as 0.
    zAbiStruct.bRespectActionCap = false --Characters with multiple free actions have a hard action cap. This flag checks that cap.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies Single"

    --Effects
    zAbiStruct.iCPGain = 1
    zAbiStruct.fThreatMultiplier = 2.0
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Hit-Rate Module
    zAbiStruct.zPredictionPackage.bHasHitRateModule = true
    zAbiStruct.zPredictionPackage.zHitRateModule.iMissRate = 5
    
    --Crit Module
    zAbiStruct.zPredictionPackage.bHasCritModule = true
    zAbiStruct.zPredictionPackage.zCritModule.iCritThreshold = 100
    
    --Damage Module
    zAbiStruct.zPredictionPackage.bHasDamageModule = true
    zAbiStruct.zPredictionPackage.zDamageModule.bUseWeaponType = true
    zAbiStruct.zPredictionPackage.zDamageModule.iDamageType = gciDamageType_Striking
    zAbiStruct.zPredictionPackage.zDamageModule.fDamageFactor = 1.00
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Weapon")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.bUseWeapon        = true
    zAbiStruct.zExecutionAbiPackage.iDamageType       = gciDamageType_Striking
    zAbiStruct.zExecutionAbiPackage.iMissThreshold    = 5
    zAbiStruct.zExecutionAbiPackage.fDamageFactor     = 1.00
    zAbiStruct.zExecutionAbiPackage.zaTags = {{"Is Attack", 1}}
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzSanyaHunterProtectiveRage = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzSanyaHunterProtectiveRage

-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--Check the target's threat list and modify the damage amount accordingly.
if(iSwitchType == gciAbility_BuildPredictionBox) then

    -- |[ ===================== Basic Module ===================== ]|
    --Setup the basic prediction package.
    local zUsePredictionPack = gzRefAbility.zPredictionPackage
        
    -- |[ ===================== Other Values ===================== ]|
    --Get originator ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
    DL_PopActiveObject()
    
    --Get information about the cluster. This is needed to give each box a unique ID.
    local iClusterID = AdvCombat_GetProperty("Prediction Get Cluster ID")
    
    -- |[ =================== For Each Target ==================== ]|
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    for i = 0, iClusterTargetsTotal-1, 1 do
        
        --Get ID of target.
        local iTargetID = AdvCombat_GetProperty("Prediction Get Targets ID", i)
        
        --Get threat they have against the owner.
        local iHigherThreats = 0
        local iThreatVsOwner = VM_GetVar("Root/Variables/Combat/" .. iTargetID .. "_AI/" .. iOwnerID.. "_Threat", "N")
        
        --Scan across all party members and build a list of threats.
        local iPartySize = AdvCombat_GetProperty("Active Party Size")
        for p = 0, iPartySize-1, 1 do
            
            --Get this member's ID.
            AdvCombat_SetProperty("Push Active Party Member By Slot", p)
                local iMemberID = RO_GetID()
            DL_PopActiveObject()
        
            --If this member is the owner, skip:
            if(iMemberID == iOwnerID) then
            
            --Get the threat, and compare it to the owner's threat.
            else
                local iThreatVsMember = VM_GetVar("Root/Variables/Combat/" .. iTargetID .. "_AI/" .. iMemberID.. "_Threat", "N")
                
                --Greater or equal, add one to the counter.
                if(iThreatVsMember >= iThreatVsOwner) then
                    iHigherThreats = iHigherThreats + 1
                end
        
            end
        end
        
        --Attack power factor is computed by how many entries are ahead of the owner.
        if(iHigherThreats < 1) then
            gzRefAbility.zPredictionPackage.zDamageModule.fDamageFactor = 1.00
        elseif(iHigherThreats == 1) then
            gzRefAbility.zPredictionPackage.zDamageModule.fDamageFactor = 1.25
        elseif(iHigherThreats == 2) then
            gzRefAbility.zPredictionPackage.zDamageModule.fDamageFactor = 1.40
        else
            gzRefAbility.zPredictionPackage.zDamageModule.fDamageFactor = 1.50
        end
        
        --Execute.
        fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, gzRefAbility.zPredictionPackage)
        
        --Reset power.
        gzRefAbility.zPredictionPackage.zDamageModule.fDamageFactor = 1.00
        
    end

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--As above, sets power based on threat order.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[ ======================= Defaults ======================= ]|
    --Default set.
    if(gzRefAbility.bIsFreeAction == nil) then gzRefAbility.bIsFreeAction = false end
    
    -- |[ ================ Originator Statistics ================= ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
    
        --Get ID.
        local iOriginatorID = RO_GetID()
        
        --CP handler.
        if(gzRefAbility.iCPGain ~= nil and gzRefAbility.iRequiredCP ~= nil) then
            fnModifyCP(gzRefAbility.iCPGain - gzRefAbility.iRequiredCP)
        end
        
        --MP handler.
        if(gzRefAbility.iRequiredMP ~= nil) then
            fnModifyMP(gzRefAbility.iRequiredMP * -1)
        end
    DL_PopActiveObject()
    
    -- |[ ==================== Ability Package =================== ]|
    --Create an ability package. This package comes with the default hit/miss values, animations, etc.
    local zAbilityPackage = gzRefAbility.zExecutionAbiPackage
    zAbilityPackage.iOriginatorID = iOriginatorID
    
    -- |[ ==================== Effect Package ==================== ]|
    --Create an effect package and populate it.
    local zEffectPackage = gzRefAbility.zExecutionEffPackage
    if(zEffectPackage ~= nil) then
        zEffectPackage.iOriginatorID = iOriginatorID
    end
    
    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        
        --Get ID of target.
        AdvCombat_SetProperty("Push Target", i-1)
            local iTargetID = RO_GetID()
        DL_PopActiveObject()
        
        --Get threat they have against the owner.
        local iHigherThreats = 0
        local iThreatVsOwner = VM_GetVar("Root/Variables/Combat/" .. iTargetID .. "_AI/" .. iOriginatorID.. "_Threat", "N")
        
        --Scan across all party members and build a list of threats.
        local iPartySize = AdvCombat_GetProperty("Active Party Size")
        for p = 0, iPartySize-1, 1 do
            
            --Get this member's ID.
            AdvCombat_SetProperty("Push Active Party Member By Slot", p)
                local iMemberID = RO_GetID()
            DL_PopActiveObject()
        
            --If this member is the owner, skip:
            if(iMemberID == iOriginatorID) then
            
            --Get the threat, and compare it to the owner's threat.
            else
                local iThreatVsMember = VM_GetVar("Root/Variables/Combat/" .. iTargetID .. "_AI/" .. iMemberID.. "_Threat", "N")
                
                --Greater or equal, add one to the counter.
                if(iThreatVsMember >= iThreatVsOwner) then
                    iHigherThreats = iHigherThreats + 1
                end
        
            end
        end
        
        --Attack power factor is computed by how many entries are ahead of the owner.
        if(iHigherThreats < 1) then
            gzRefAbility.zExecutionAbiPackage.fDamageFactor = 1.00
        elseif(iHigherThreats == 1) then
            gzRefAbility.zExecutionAbiPackage.fDamageFactor = 1.25
        elseif(iHigherThreats == 2) then
            gzRefAbility.zExecutionAbiPackage.fDamageFactor = 1.40
        else
            gzRefAbility.zExecutionAbiPackage.fDamageFactor = 1.50
        end
        
        --Call standard function.
        fnStandardExecution(iOriginatorID, i-1, zAbilityPackage, zEffectPackage)
        
        --Reset.
        gzRefAbility.zExecutionAbiPackage.fDamageFactor = 1.0
        
        --If the ref ability has a threat multiplier, set it here. This applies even if the ability misses.
        --Note that the threat multiplier can be zero, or negative.
        if(gzRefAbility.fThreatMultiplier ~= nil) then
        
            --Get target ID.
            AdvCombat_SetProperty("Push Target", i-1)
                local iTargetID = RO_GetID()
            DL_PopActiveObject()
            
            --Get threat values from the originator.
            AdvCombat_SetProperty("Push Event Originator")
                local iOriginatorAttackPower = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
                local fThreatMultiplier = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_ThreatMultiplier)
            DL_PopActiveObject()
        
            --Compute.
            local iThreatToApply = math.floor(iOriginatorAttackPower * (fThreatMultiplier / 100.0) * gzRefAbility.fThreatMultiplier)
            if(iThreatToApply > 0) then
                AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetID, 0, "AI_APPLICATION|Threat|" .. iThreatToApply)
            end
        end
        
    end
    
-- |[ ==================================== Standard Handler ==================================== ]|
--Call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
