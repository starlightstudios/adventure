-- |[ ======================================= Stalk Prey ======================================= ]|
-- |[Description]|
--Increases attack power by 50% until end of turn. Auto-activates on first turn of battle if the
-- player ambushed the enemy.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--Create a prefabricated copy the first time this script is run.
if(gzSanyaHunterStalkPrey == nil) then
    
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Hunter"
    zAbiStruct.sSkillName    = "Stalk Prey"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Buff
    zAbiStruct.sIconFrame    = gsAbility_Frame_Passive
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Sanya|Hunter|StalkPrey"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "+50%% [Atk] on first turn when ambushing enemies.\n\n\n\n\n\nPassive."
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Gain a major attack bonus when ambushing\nyour prey.\nActive on the first turn when ambushing an\nenemy.\n\nPassive."

    -- |[Usability Variables]|
    zAbiStruct.bNeverAvailable = true    --Passive abilities have no usability requirements.
    
    -- |[Passive Effect]|
    --Apply this effect when combat starts.
    zAbiStruct.sPassivePath = fnResolvePath() .. "../Effects/Stalk Prey.lua"
    
    --GUI Stat Bonuses
    zAbiStruct.bHasGUIEffects = true
    
    -- |[Prediction Package]|
    --Passives don't set anything for prediction packages.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()

    -- |[Execution Ability Package]|
    --Passives don't set anything for execution packages.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Buff")
    
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzSanyaHunterStalkPrey = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzSanyaHunterStalkPrey

-- |[ ============================ Combat: Character Action Begins ============================= ]|
--Unlike other abilities, does not re-apply itself, ever.
if(iSwitchType == gciAbility_BeginAction or iSwitchType == gciAbility_BeginFreeAction) then

    -- |[Error Checks]|
    --Error check.
    if(gzRefAbility.sPassivePath == nil) then return end
    
    --Do nothing if the owner is not acting:
    if(AdvCombatAbility_GetProperty("Is Owner Acting") == false) then return end
    
    -- |[Setup]|
    --Get the effect ID.
    local iUniqueID = RO_GetID()

    --Get the owner for their ID. Check if this ability is still equipped.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOriginatorID = RO_GetID()
        local bIsAbilityEquipped = AdvCombatEntity_GetProperty("Is Ability Equipped ID", iUniqueID)
    DL_PopActiveObject()
    
    --Check the effect ID. Passives are always managing an effect, but if the ID comes back zero, the effect does not exist.
    local iManagedEffectID  = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iManagedEffectID", "I") 
    
    --Check if the effect exists.
    local bEffectExists = AdvCombat_GetProperty("Does Effect Exist", iManagedEffectID)
    
    --Get the turn count.
    local iCurrentTurn = AdvCombat_GetProperty("Current Turn")
    if(AdvCombat_GetProperty("Does Player Have Initiative") == false) then iCurrentTurn = 1000 end
    
    -- |[Apply]|
    --If the effect does not exist, but we are managing an effect, spawn a new effect.
    if(bEffectExists == false and iCurrentTurn == 0) then
        
        --Issue spawn order.
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 0, "Effect|" .. gzRefAbility.sPassivePath .. "|Originator:" .. iOriginatorID .. "|StoreID:" .. iUniqueID)
        
        --Make sure the datalibrary variables exist.
        DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "/")
        VM_SetVar ("Root/Variables/Combat/" .. iUniqueID .. "/iIsManagingEffect", "N", 1.0)
        VM_SetVar ("Root/Variables/Combat/" .. iUniqueID .. "/iManagedEffectID", "N", 0.0)
    end

-- |[ ==================================== Standard Handler ==================================== ]|
--Call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
