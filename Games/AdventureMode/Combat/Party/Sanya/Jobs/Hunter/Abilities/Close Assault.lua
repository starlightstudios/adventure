-- |[ ===================================== Close Assault ====================================== ]|
-- |[Description]|
--Attack multiple times for 0.40x damage each time. Max number of attacks scales with current percentage
-- damage boost user has.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzSanyaHunterCloseAssault == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Hunter"
    zAbiStruct.sSkillName    = "Close Assault"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Advanced
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Sanya|Hunter|CloseAssault"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\nStrikes 2-3 times, max roll increases with [Atk] buffs.\n+1 max roll per 25%% [Atk] boost.\n\n\n[Costs]"
    zAbiStruct.sDescription = ""
    zAbiStruct.saImages = {}
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Fight in close quarters, striking multiple times.\nNumber of attacks increases as [Atk](Power) is\nbuffed by other effects.\n\n\nCosts [MPCost][MPIco](MP)."
    zAbiStruct.sSimpleDesc = ""
    zAbiStruct.saSimpleImages = {}

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false  --Set to true to ignore all requirements.
    zAbiStruct.iRequiredMP = 30          --MP required to use the ability.
    zAbiStruct.iRequiredCP = 0           --CP required to use the ability.
    zAbiStruct.bRespectsCooldown = true  --If this ability has a cooldown, decrements it.
    zAbiStruct.iCooldown = 0             --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
    zAbiStruct.bIsFreeAction = false     --Flags as a free action.
    zAbiStruct.iRequiredFreeActions = 0  --Free actions set this as 1, everything else as 0.
    zAbiStruct.bRespectActionCap = false --Characters with multiple free actions have a hard action cap. This flag checks that cap.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies Single"

    --Effects
    zAbiStruct.iCPGain = 1
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Hit-Rate Module
    zAbiStruct.zPredictionPackage.bHasHitRateModule = true
    zAbiStruct.zPredictionPackage.zHitRateModule.iMissRate = 5
    
    --Extra display lines.
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "0.40 x[IMG0] 2+ times. More attacks with [IMG0] buffs."
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {"Root/Images/AdventureUI/StatisticIcons/Attack"}
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    --Basic package.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Strike")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.fDamageFactor  = 0.40
    zAbiStruct.zExecutionAbiPackage.iMissThreshold = 5
    zAbiStruct.zExecutionAbiPackage.zaTags = {{"Is Attack", 1}}
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzSanyaHunterCloseAssault = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzSanyaHunterCloseAssault

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
if(iSwitchType == gciAbility_Execute) then
    
    -- |[ ================ Originator Statistics ================= ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
    
        --Get ID.
        local iOriginatorID = RO_GetID()
        
        --CP handler.
        if(gzRefAbility.iCPGain ~= nil and gzRefAbility.iRequiredCP ~= nil) then
            fnModifyCP(gzRefAbility.iCPGain - gzRefAbility.iRequiredCP)
        end
        
        --MP handler.
        if(gzRefAbility.iRequiredMP ~= nil) then
            fnModifyMP(gzRefAbility.iRequiredMP * -1)
        end

        --Attack power storage.
        local iBasePower  = AdvCombatEntity_GetProperty("Statistic", gciStatGroup_Base,      gciStatIndex_Attack)
        local iJobBuff    = AdvCombatEntity_GetProperty("Statistic", gciStatGroup_Job,       gciStatIndex_Attack)
        local iEquipBuff  = AdvCombatEntity_GetProperty("Statistic", gciStatGroup_Equipment, gciStatIndex_Attack)
        local iFinalPower = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
        
    DL_PopActiveObject()
    
    -- |[ ==================== Ability Package =================== ]|
    --Create an ability package. This package comes with the default hit/miss values, animations, etc.
    local zAbilityPackage = gzRefAbility.zExecutionAbiPackage
    zAbilityPackage.iOriginatorID = iOriginatorID
    
    -- |[ ================= Compute Attack Count ================= ]|
    --Base + Job + Equip is the power that is used as a baseline.
    local iUseBasePower = iBasePower + iJobBuff + iEquipBuff
    
    --Divide the final power by the baseline to get the bonus factor.
    local fBonusFactor = iFinalPower / iUseBasePower
    
    --Compute number of attacks.
    local iMinRoll = 2
    local iMaxRoll = 3 + math.floor(((fBonusFactor - 1.0) / 0.25))
    if(iMaxRoll < 3) then iMaxRoll = 3 end
    local iAttackCount = LM_GetRandomNumber(iMinRoll, iMaxRoll)
    
    --Debug.
    if(false) then
        io.write("Base Power: " .. iUseBasePower .. "\n")
        io.write("Final Power: " .. iFinalPower .. "\n")
        io.write("Damage bonus factor: " .. fBonusFactor .. "\n")
        io.write("Rolled " .. iAttackCount .. " from " .. iMinRoll .. " to " .. iMaxRoll .."\n")
    end
    
    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        
        --Call standard function.
        for p = 1, iAttackCount, 1 do
            fnStandardExecution(iOriginatorID, i-1, zAbilityPackage, nil)
        end
    end

-- |[ ==================================== Standard Handler ==================================== ]|
--All other cases, call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

end