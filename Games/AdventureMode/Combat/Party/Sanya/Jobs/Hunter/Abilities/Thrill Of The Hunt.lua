-- |[ =================================== Thrill Of The Hunt =================================== ]|
-- |[Description]|
--Restores 10 MP after a critical strike lands. The effect applied does nothing except indicate
-- that an effect is happening, the ability handles it all.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--Create a prefabricated copy the first time this script is run.
if(gzSanyaHunterThrillOfTheHunt == nil) then
    
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Hunter"
    zAbiStruct.sSkillName    = "Thrill Of The Hunt"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Passive
    zAbiStruct.sIconBacking  = gsAbility_Backing_Buff
    zAbiStruct.sIconFrame    = gsAbility_Frame_Passive
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Sanya|Hunter|ThrillOfTheHunt"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_Passive
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "Restores 10MP after every critical strike.\n\n\n\n\n\nPassive."
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Get a rush when you land that powerful blow!\nRecover 10MP after every critical.\n\n\n\nPassive."

    -- |[Usability Variables]|
    zAbiStruct.bNeverAvailable = true    --Passive abilities have no usability requirements.
    
    -- |[Passive Effect]|
    --Apply this effect when combat starts.
    zAbiStruct.sPassivePath = fnResolvePath() .. "../Effects/Thrill Of The Hunt.lua"
    
    --GUI Stat Bonuses
    zAbiStruct.bHasGUIEffects = true
    
    -- |[Prediction Package]|
    --Passives don't set anything for prediction packages.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()

    -- |[Execution Ability Package]|
    --Passives don't set anything for execution packages.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Buff")
    
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzSanyaHunterThrillOfTheHunt = zAbiStruct
end

--Because this is a prototype, we set the gzRefAbility to point to the unique structure. This way,
-- all the activity below points to the gzRefAbility's values and we don't need to change the structure's
-- name to create unique names for everything.
--The gsStandardAbilityPath handler also operates on gzRefAbility if it is called. You can use manual
-- overrides to make unique abilities.
gzRefAbility = gzSanyaHunterThrillOfTheHunt

-- |[ ==================================== Creation Handler ==================================== ]|
--Same as the standard creation, except adds the action-ends response.
if(iSwitchType == gciAbility_Create or iSwitchType == gciAbility_CreateSpecial) then
    
    -- |[Setup]|
    --Special: If the ability has an internal version, the name changes. This is used for abilities
    -- that can both be purchased for JP and exist on the job skill bar by default.
    local sUseJobName = gzRefAbility.sJobName
    if(iSwitchType == gciAbility_CreateSpecial) then sUseJobName = gzRefAbility.sInternalName end
    
    --Special: If a second argument is provided, then the name of the ability is being overrode. This
    -- is used by item creation scripts since items have fixed names.
    local sAbilityNameOverride = "NULL"
    if(iArgumentsTotal >= 2) then
        sAbilityNameOverride = LM_GetScriptArgument(1)
    end
    
    -- |[Creation]|
    local sAbilityName = sUseJobName .. "|" .. gzRefAbility.sSkillName
    if(sAbilityNameOverride ~= "NULL") then sAbilityName = sAbilityNameOverride end
    AdvCombatEntity_SetProperty("Create Ability", sAbilityName)
    
        --System
        AdvCombatAbility_SetProperty("Script Path", LM_GetCallStack(0)) --Use this script.
        AdvCombatAbility_SetProperty("JP Cost", gzRefAbility.iJPUnlockCost)
        AdvCombatAbility_SetProperty("CP Cost", gzRefAbility.iRequiredCP)
        
        --Tags. The structure may be nil.
        if(gzRefAbility.zaTags ~= nil) then
            for i = 1, #gzRefAbility.zaTags, 1 do
                AdvCombatAbility_SetProperty("Add Tag", gzRefAbility.zaTags[i][1], gzRefAbility.zaTags[i][2])
            end
        end
    
        --Display
        AdvCombatAbility_SetProperty("Display Name", gzRefAbility.sSkillName)
        AdvCombatAbility_SetProperty("Icon Back",    gzRefAbility.sIconBacking)
        AdvCombatAbility_SetProperty("Icon Frame",   gzRefAbility.sIconFrame)
        AdvCombatAbility_SetProperty("Icon",         gzRefAbility.sIconPath)
        if(gzRefAbility.sCPIcon ~= "Null") then
            AdvCombatAbility_SetProperty("CP Icon", gzRefAbility.sCPIcon)
        end
        
        --Special: Name Override. If the field sDisplayName isn't nil, then the skill name and the display name aren't the same.
        if(gzRefAbility.sDisplayName ~= nil) then
            AdvCombatAbility_SetProperty("Display Name", gzRefAbility.sDisplayName)
        end
        
        --Description
        AdvCombatAbility_SetProperty("Description", gzRefAbility.sDescription)
        AdvCombatAbility_SetProperty("Allocate Description Images", #gzRefAbility.saImages)
        for i = 1, #gzRefAbility.saImages, 1 do
            AdvCombatAbility_SetProperty("Description Image", i-1, gcfAbilityImgOffsetY, gzRefAbility.saImages[i])
        end
        AdvCombatAbility_SetProperty("Crossload Description Images")
        
        --Simplified Description
        if(gzRefAbility.sSimpleDesc ~= nil) then
            AdvCombatAbility_SetProperty("Simplified Description", gzRefAbility.sSimpleDesc)
            AdvCombatAbility_SetProperty("Allocate Simplified Description Images", #gzRefAbility.saSimpleImages)
            for i = 1, #gzRefAbility.saSimpleImages, 1 do
                AdvCombatAbility_SetProperty("Simplified Description Image", i-1, gcfAbilityImgOffsetLgY, gzRefAbility.saSimpleImages[i])
            end
            AdvCombatAbility_SetProperty("Crossload Simplified Description Images")
        end
        
        --Response Flags
        fnSetAbilityResponseFlags(gzRefAbility.iResponseType)
        
        --Additional response.
        AdvCombatAbility_SetProperty("Script Response", gciAbility_PostAction, true)
        
    DL_PopActiveObject()

-- |[ ============================= Combat: Character Action Ends ============================== ]|
--After the action ends, check if the owner acted, attacked, and got a crit.
elseif(iSwitchType == gciAbility_PostAction) then

    --If the owner is not acting, fail.
    if(AdvCombatAbility_GetProperty("Is Owner Acting") == false) then return end
    
    --Get owner ID.
    local iOwnerID = AdvCombatAbility_GetProperty("Owner ID")
    
    --Scan backwards through the abilities used to find the one used by our owner. It should be the
    -- last one, but a counterattack might take that slot.
    local iCurrentTurn = gzaCombatAbilityStatistics.iTurnsElapsed
    for i = #gzaCombatAbilityStatistics.zaStatisticsPacks, 1, -1 do

        --Get the package.
        local zStatsPack = gzaCombatAbilityStatistics.zaStatisticsPacks[i]
        
        --Error checks:
        if(zStatsPack == nil) then
            --io.write("  Error, out of range.\n")
        elseif(zStatsPack.iTurnElapsed ~= iCurrentTurn) then
            --io.write("  Error, incorrect turn " .. zStatsPack.iTurnElapsed .. " vs. " .. iCurrentTurn .. "\n")
        elseif(zStatsPack.iActorID ~= iOwnerID) then
            --io.write("  Error, not action by owner " .. zStatsPack.iActorID .. " vs. " .. iOwnerID .. "\n")
        
        --Checks passed:
        else
            
            --Scan all targets hit. If any of these were crits, log that.
            local iTotalDamageDealt = 0
            local bAnyCrits = false
            for p = 1, #zStatsPack.zaTargetInfo, 1 do
                iTotalDamageDealt = iTotalDamageDealt + zStatsPack.zaTargetInfo[p].iDamageDealt
                if(zStatsPack.zaTargetInfo[p].bWasCritical) then bAnyCrits = true end
            end
            
            --Any crits: Award 10 MP.
            if(bAnyCrits) then
                AdvCombat_SetProperty("Push Entity By ID", iOwnerID)
                    fnModifyMP(10)
                DL_PopActiveObject()
            end
            
            --Stop iteration.
            return
        end
    end

-- |[ ==================================== Standard Handler ==================================== ]|
--Call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
