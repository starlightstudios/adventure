-- |[ ====================================== Lead The Shot ===================================== ]|
-- |[Description]|
--Deals reduced damage, but increases based on the difference between Sanya's accuracy and the target's Evade.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--Create a prefabricated copy the first time this script is run.
if(gzSanyaHunterLeadTheShot == nil) then
    
    -- |[System]|
    --Setup.
    local zAbiStruct = {}

    --Display/System Variables
    zAbiStruct.sJobName      = "Hunter"
    zAbiStruct.sSkillName    = "Lead The Shot"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Sanya|Hunter|LeadTheShot"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n[Atk] +3%% per 5 [Evd] on target.\nCannot miss, cannot crit.\n\n\n[Costs]"
    zAbiStruct.sDescription = ""
    zAbiStruct.saImages = {}
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Carefully aim ahead to guarantee a hit.\nDeals more damage against more evasive\nenemies.\nCannot miss or crit.\n\nCosts [MPCost][MPIco](MP)."
    zAbiStruct.sSimpleDesc = ""
    zAbiStruct.saSimpleImages = {}

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false  --Set to true to ignore all requirements.
    zAbiStruct.iRequiredMP = 20          --MP required to use the ability.
    zAbiStruct.iRequiredCP = 0           --CP required to use the ability.
    zAbiStruct.bRespectsCooldown = true  --If this ability has a cooldown, decrements it.
    zAbiStruct.iCooldown = 0             --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
    zAbiStruct.bIsFreeAction = false     --Flags as a free action.
    zAbiStruct.iRequiredFreeActions = 0  --Free actions set this as 1, everything else as 0.
    zAbiStruct.bRespectActionCap = false --Characters with multiple free actions have a hard action cap. This flag checks that cap.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies Single"

    --Effects
    zAbiStruct.iCPGain = 1
    
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Damage Module
    zAbiStruct.zPredictionPackage.bHasDamageModule = true
    zAbiStruct.zPredictionPackage.zDamageModule.bUseWeaponType = true
    zAbiStruct.zPredictionPackage.zDamageModule.iDamageType = gciDamageType_Striking
    zAbiStruct.zPredictionPackage.zDamageModule.fDamageFactor = 0.80
    
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Weapon")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.bUseWeapon        = true
    zAbiStruct.zExecutionAbiPackage.iDamageType       = gciDamageType_Striking
    zAbiStruct.zExecutionAbiPackage.iMissThreshold    = 5
    zAbiStruct.zExecutionAbiPackage.fDamageFactor     = 0.80
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits       = true
    zAbiStruct.zExecutionAbiPackage.bNeverCrits       = true
    zAbiStruct.zExecutionAbiPackage.zaTags = {{"Is Attack", 1}}
    
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzSanyaHunterLeadTheShot = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzSanyaHunterLeadTheShot

-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--Always recompute the damage factor when building a prediction box. Otherwise same as standard.
if(iSwitchType == gciAbility_BuildPredictionBox) then

    -- |[ ===================== Basic Module ===================== ]|
    --Setup the basic prediction package.
    local zUsePredictionPack = gzRefAbility.zPredictionPackage
        
    -- |[ ===================== Other Values ===================== ]|
    --Get originator ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
    DL_PopActiveObject()
    
    --Get information about the cluster. This is needed to give each box a unique ID.
    local iClusterID = AdvCombat_GetProperty("Prediction Get Cluster ID")
    
    -- |[ =================== For Each Target ==================== ]|
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    for i = 0, iClusterTargetsTotal-1, 1 do
        
        --Get target stats.
        local iTargetID = AdvCombat_GetProperty("Prediction Get Targets ID", i)
        AdvCombat_SetProperty("Push Entity By ID", iTargetID)
            local iTargetEvade = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Evade)
        DL_PopActiveObject()
        
        --Modify the prediction package's base damage factor based on this target.
        --Each 5 points of evade increases the damage factor by 3%.
        local fBasePower = 0.80
        local fPowerPerEvade = 0.03 / 5.0
        gzRefAbility.zPredictionPackage.zDamageModule.fDamageFactor = fBasePower + math.floor(fPowerPerEvade * iTargetEvade)
        
        --If the power goes beneath the base, use the base.
        if(gzRefAbility.zPredictionPackage.zDamageModule.fDamageFactor < fBasePower) then gzRefAbility.zPredictionPackage.zDamageModule.fDamageFactor = fBasePower end
        
        --Run the prediction box.
        fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, gzRefAbility.zPredictionPackage)
        
        --Reset to base power.
        gzRefAbility.zPredictionPackage.zDamageModule.fDamageFactor = fBasePower
    end

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Recomputes attack power on execution based on the target's evade. Otherwise same as standard.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[ ======================= Defaults ======================= ]|
    --Default set.
    if(gzRefAbility.bIsFreeAction == nil) then gzRefAbility.bIsFreeAction = false end
    
    -- |[ ================ Originator Statistics ================= ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
    
        --Get ID.
        local iOriginatorID = RO_GetID()
        
        --CP handler.
        if(gzRefAbility.iCPGain ~= nil and gzRefAbility.iRequiredCP ~= nil) then
            fnModifyCP(gzRefAbility.iCPGain - gzRefAbility.iRequiredCP)
        end
        
        --MP handler.
        if(gzRefAbility.iRequiredMP ~= nil) then
            fnModifyMP(gzRefAbility.iRequiredMP * -1)
        end
    DL_PopActiveObject()
    
    -- |[ ======================= Charges ======================== ]|
    --If the ability has charges, remove them here.
    if(gzRefAbility.iChargesMax ~= nil and gzRefAbility.iChargesMax > 0) then
        local iUniqueID = RO_GetID()
        local iChargesLeft = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iChargesLeft", "I")
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iChargesLeft", "N", iChargesLeft - 1)
    end
    
    -- |[ ==================== Ability Package =================== ]|
    --Create an ability package. This package comes with the default hit/miss values, animations, etc.
    local zAbilityPackage = gzRefAbility.zExecutionAbiPackage
    zAbilityPackage.iOriginatorID = iOriginatorID
    
    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        
        -- |[Target Checking]|
        --Get the target's ID.
        local iTargetID = 0
        AdvCombat_SetProperty("Push Target", i-1)
            iTargetID = RO_GetID()
        DL_PopActiveObject()
        
        --Check for a redirect.
        iTargetID = fnGetTargetRedirect(iTargetID)
        
        --Now push the final target. It might be the original one, or not.
        AdvCombat_SetProperty("Push Entity By ID", iTargetID)
            local iTargetEvade = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Evade)
        DL_PopActiveObject()
        
        -- |[Recompute]|
        --Recompute attack power factor. Each 5 points of evade increases the damage factor by 3%.
        local fBasePower = 0.80
        local fPowerPerEvade = 0.03 / 5.0
        gzRefAbility.zExecutionAbiPackage.fDamageFactor = fBasePower + math.floor(fPowerPerEvade * iTargetEvade)
        
        --If the power goes beneath the base, use the base.
        if(gzRefAbility.zExecutionAbiPackage.fDamageFactor < fBasePower) then gzRefAbility.zExecutionAbiPackage.fDamageFactor = fBasePower end
        
        -- |[Execute]|
        --Call standard function.
        fnStandardExecution(iOriginatorID, i-1, zAbilityPackage, zEffectPackage)
    end

-- |[ ==================================== Standard Handler ==================================== ]|
--Call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
