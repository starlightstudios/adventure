-- |[ ======================================== Killshot ======================================== ]|
-- |[Description]|
--4CP high-power attack with weapon damage, serves as a bleed attack chaser.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzSanyaSkystrikerKillshot == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Skystriker"
    zAbiStruct.sSkillName    = "Killshot"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Advanced
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Sanya|Werebat|Killshot"
    zAbiStruct.sCPIcon       = "Root/Images/AdventureUI/Abilities/Cmb4"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\n[Bld] Chaser, +50%% [Atk] if [Bld] is present.\n\n\n[Costs]"
    zAbiStruct.sDescription = ""
    zAbiStruct.saImages = {}
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Hit the enemy for major weapon damage, and\ndeals extra damage if they have a [Bld](Bleed) DoT.\nConsumes all [Bld]DoTs and deals their damage\nimmediately.\n\nCosts [CPCost][CPIco](CP)."
    zAbiStruct.sSimpleDesc = ""
    zAbiStruct.saSimpleImages = {}

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false  --Set to true to ignore all requirements.
    zAbiStruct.iRequiredMP = 0           --MP required to use the ability.
    zAbiStruct.iRequiredCP = 4           --CP required to use the ability.
    zAbiStruct.bRespectsCooldown = true  --If this ability has a cooldown, decrements it.
    zAbiStruct.iCooldown = 0             --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
    zAbiStruct.bIsFreeAction = false     --Flags as a free action.
    zAbiStruct.iRequiredFreeActions = 0  --Free actions set this as 1, everything else as 0.
    zAbiStruct.bRespectActionCap = false --Characters with multiple free actions have a hard action cap. This flag checks that cap.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies Single"

    --Effects
    zAbiStruct.iCPGain = 0
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Hit-Rate Module
    zAbiStruct.zPredictionPackage.bHasHitRateModule = true
    zAbiStruct.zPredictionPackage.zHitRateModule.iMissRate = -25
    
    --Damage Module
    zAbiStruct.zPredictionPackage.bHasDamageModule = true
    zAbiStruct.zPredictionPackage.zDamageModule.bUseWeaponType = true
    zAbiStruct.zPredictionPackage.zDamageModule.iDamageType = gciDamageType_Bleeding
    zAbiStruct.zPredictionPackage.zDamageModule.fDamageFactor = 3.00
    zAbiStruct.zPredictionPackage.zDamageModule.iScatterRange = 15
    
    --Crit Module
    zAbiStruct.zPredictionPackage.bHasCritModule = true
    
    --Chaser Modules
    zAbiStruct.zPredictionPackage.bHasChaserModules = true
    zAbiStruct.zPredictionPackage.iChaserModulesTotal = 1
    zAbiStruct.zPredictionPackage.zaChaserModules[1] = {}
    zAbiStruct.zPredictionPackage.zaChaserModules[1].bConsumeEffect = true                   --If true, DoT expires when attack lands
    zAbiStruct.zPredictionPackage.zaChaserModules[1].sDoTTag = "Bleeding DoT"                --What tag to look for on effects.
    zAbiStruct.zPredictionPackage.zaChaserModules[1].fDamageFactorChangePerDoT = 0.50        --Added to the damage module's factor for each effect present.
    zAbiStruct.zPredictionPackage.zaChaserModules[1].fDamageRemainingFactor = 1.0            --Percentage of remaining DoT damage to be added.
    zAbiStruct.zPredictionPackage.zaChaserModules[1].sConsumeString = "Consumes [IMG0] DoTs" --Description to appear IF and ONLY IF the chaser consumes DoTs
    zAbiStruct.zPredictionPackage.zaChaserModules[1].iConsumeImgCount = 1
    zAbiStruct.zPredictionPackage.zaChaserModules[1].saConsumeImgPath = {}
    zAbiStruct.zPredictionPackage.zaChaserModules[1].saConsumeImgPath[1] = "Root/Images/AdventureUI/DamageTypeIcons/Bleeding"
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    --Basic package.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Bleed")
    zAbiStruct.zExecutionAbiPackage.iMissThreshold = -25
    zAbiStruct.zExecutionAbiPackage.fDamageFactor  = 3.00
    
    --Chaser Modules
    zAbiStruct.zExecutionAbiPackage.bHasChaserModules = true
    zAbiStruct.zExecutionAbiPackage.iChaserModulesTotal = 1
    zAbiStruct.zExecutionAbiPackage.zaChaserModules[1] = {}
    zAbiStruct.zExecutionAbiPackage.zaChaserModules[1].bConsumeEffect = true           --If true, DoT expires when attack lands
    zAbiStruct.zExecutionAbiPackage.zaChaserModules[1].sDoTTag = "Bleeding DoT"        --What tag to look for on effects.
    zAbiStruct.zExecutionAbiPackage.zaChaserModules[1].fDamageFactorChangePerDoT = 0.50--Added to the damage module's factor for each effect present.
    zAbiStruct.zExecutionAbiPackage.zaChaserModules[1].fDamageRemainingFactor = 1.0    --Percentage of remaining DoT damage to be added.
    zAbiStruct.zExecutionAbiPackage.zaTags = {{"Is Attack", 1}}
    
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzSanyaSkystrikerKillshot = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzSanyaSkystrikerKillshot

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
