-- |[ ====================================== Night Terror ====================================== ]|
-- |[Description]|
--Buff. Next attack will apply the debuff 'Terrified' to any hit targets. This debuff reduces
-- their terrify resistance.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Local Variables ===================================== ]|
-- |[System]|
--This effect uses a standardized StatMod script.
local sDisplayName = "Night Terror"
local iDuration = -1
local bIsBuff = true
local sIcon = "Root/Images/AdventureUI/Abilities/Tiffany|ConductiveSpray"
local sStatString = ""

--Description can be a variable number of lines.
local saDescription = {}
saDescription[1] = "Haunt their dreams!"
saDescription[2] = "Next attack reduces enemy [Tfy](Terrify) resistance."

--Run the standardized builder.
gzaStatModStruct            = fnCreateStatmodEffectPrototype(sDisplayName, iDuration, bIsBuff, sIcon, sStatString)
gzaStatModStruct.saStrings  = fnStatmodBuildInspectorStrings(gzaStatModStruct, saDescription)
gzaStatModStruct.sShortText = fnStatmodBuildShortText(gzaStatModStruct)

-- |[Overrides]|
gzaStatModStruct.zaTagList = {{"Generate Effect Hostile", 1}, {"Is Positive", 1}}

-- |[ ======================================== Creation ======================================== ]|
--Standard call when effect is initialized.
if(iSwitchType == gciEffect_Create) then
    local fSeverity = 1.0
    if(iArgumentsTotal >= 2) then fSeverity = LM_GetScriptArgument(1, "N") end
    LM_ExecuteScript(gsStandardStatPath, gciEffect_Create, fSeverity)
    
-- |[ ================================== Apply/Unapply Stats =================================== ]|
--Calls the apply/unapply stat routines.
elseif((iSwitchType == gciEffect_ApplyStats or iSwitchType == gciEffect_UnApplyStats) and iArgumentsTotal >= 2) then
    local iTargetID = math.floor(tonumber(LM_GetScriptArgument(1, "N")))
    LM_ExecuteScript(gsStandardStatPath, iSwitchType, iTargetID)

-- |[ ================================== Character Action Ends ================================= ]|
--Called after a character action ends to decrement the turn counter.
elseif(iSwitchType == gciEffect_PostAction) then
    LM_ExecuteScript(gsStandardStatPath, gciEffect_PostAction)

-- |[ ================================== User Attacks A Hostile ================================= ]|
--While affected by this Effect, user is attacking a hostile entity. Resolves whether or not to apply
-- a debuff to the affected target.
--The Effect is the active object.
elseif(iSwitchType == gciEffect_OnApply_Hostile) then

    io.write("Generating hostile effect.\n")

    --The effect always generates.
    local zNewEffect = fnConstructDefaultEffectPackage()
    zNewEffect.iEffectStr      = 9
    zNewEffect.iEffectCritStr  = 14
    zNewEffect.iEffectType     = gciDamageType_Terrifying
    zNewEffect.sEffectPath     = fnResolvePath() .. "Terrified.lua"
    zNewEffect.sEffectCritPath = fnResolvePath() .. "Terrified.lua"
    zNewEffect.sApplyText      = "Frightened!"
    zNewEffect.sApplyTextCol   = "Color:Red"
    zNewEffect.sResistText     = "Resisted!"
    zNewEffect.sResistTextCol  = "Color:White"

    --Add it to the global list.
    table.insert(gzaTagEffectsHostile, zNewEffect)
    
    --This effect now expires.
    AdvCombatEffect_SetProperty("Flag Remove Now")

end
