-- |[ ==================================== Full Power Shot ===================================== ]|
-- |[Description]|
--Deals 200% damage but costs 30% of user HP, or 15% when a Sevavi.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzSanyaAgonteaFullPowerShot == nil) then
    
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Agontea"
    zAbiStruct.sSkillName    = "Full Power Shot"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Sanya|Sevavi|FullPowerShot"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\nCosts 30%% of user [Hlt]. Only 15%% [Hlt] if a Sevavi.\n\n\n[Costs]"
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Pour all your power into the shot!\nDeals massive weapon damage, but injures you\nfrom the blowback.\nHurts less if you are a Sevavi.\n\nCosts [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false  --Set to true to ignore all requirements.
    zAbiStruct.iRequiredMP = 30          --MP required to use the ability.
    zAbiStruct.iRequiredCP = 0           --CP required to use the ability.
    zAbiStruct.bRespectsCooldown = true  --If this ability has a cooldown, decrements it.
    zAbiStruct.iCooldown = 0             --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
    zAbiStruct.bIsFreeAction = false     --Flags as a free action.
    zAbiStruct.iRequiredFreeActions = 0  --Free actions set this as 1, everything else as 0.
    zAbiStruct.bRespectActionCap = false --Characters with multiple free actions have a hard action cap. This flag checks that cap.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies Single"

    --Effects
    zAbiStruct.iCPGain = 1
    
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Hit-Rate Module
    zAbiStruct.zPredictionPackage.bHasHitRateModule = true
    zAbiStruct.zPredictionPackage.zHitRateModule.iMissRate = 5
    
    --Crit Module
    zAbiStruct.zPredictionPackage.bHasCritModule = true
    zAbiStruct.zPredictionPackage.zCritModule.iCritThreshold = 100
    
    --Damage Module
    zAbiStruct.zPredictionPackage.bHasDamageModule = true
    zAbiStruct.zPredictionPackage.zDamageModule.bUseWeaponType = true
    zAbiStruct.zPredictionPackage.zDamageModule.iDamageType = gciDamageType_Striking
    zAbiStruct.zPredictionPackage.zDamageModule.fDamageFactor = 2.00
    
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Weapon")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.bUseWeapon     = true
    zAbiStruct.zExecutionAbiPackage.iDamageType    = gciDamageType_Striking
    zAbiStruct.zExecutionAbiPackage.iMissThreshold = 5
    zAbiStruct.zExecutionAbiPackage.fDamageFactor  = 2.00
    zAbiStruct.zExecutionAbiPackage.zaTags = {{"Is Attack", 1}}
    
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzSanyaAgonteaFullPowerShot = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzSanyaAgonteaFullPowerShot

-- |[ ============================= Combat: Character Action Begins ============================ ]|
--Make sure we have enough HP.
if(iSwitchType == gciAbility_BeginAction or iSwitchType == gciAbility_BeginFreeAction) then
    
    -- |[Standard Usability]|
    --Setup.
    local bIsUsable = false

    --Execution for the start of an action:
    if(iSwitchType == gciAbility_BeginAction) then
        bIsUsable = fnAbilityStandardBeginAction(gzRefAbility.iRequiredMP, gzRefAbility.bRespectsCooldown, gzRefAbility.iRequiredFreeActions, gzRefAbility.bRespectActionCap)
        
    --Execution for after a free-action:
    else
        bIsUsable = fnAbilityStandardBeginFreeAction(gzRefAbility.iRequiredMP, gzRefAbility.bRespectsCooldown, gzRefAbility.iRequiredFreeActions, gzRefAbility.bRespectActionCap)
    end

    -- |[Health Check]|
    --Owner must have 21% or more HP available.
    if(AdvCombatAbility_GetProperty("Is Owner Acting") == true and bIsUsable) then

        --Get owner data:
        local iOwnerID = AdvCombatAbility_GetProperty("Owner ID")
        AdvCombat_SetProperty("Push Entity By ID", iOwnerID)
            local iHealthCur = AdvCombatEntity_GetProperty("Health")
            local iHealthMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
            local fHealthPct = iHealthCur / iHealthMax
            local iFullPowerShotResistTags = AdvCombatEntity_GetProperty("Tag Count", "Full Power Shot Resist")
        DL_PopActiveObject()
        
        --Set the HP requirement
        local fHPRequirement = 0.30
        if(iFullPowerShotResistTags > 0) then fHPRequirement = 0.15 end
        
        --Set usable:
        if(fHealthPct > fHPRequirement) then
            bIsUsable = true
        else
            bIsUsable = false
        end
    end
    
    --Finalize:
    AdvCombatAbility_SetProperty("Usable", bIsUsable)
    
-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--Set the prediction line to indicate self-damage.
elseif(iSwitchType == gciAbility_BuildPredictionBox) then

    -- |[ ===================== Other Values ===================== ]|
    --Get originator ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
        local iHealthCur = AdvCombatEntity_GetProperty("Health")
        local iHealthMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
        local iFullPowerShotResistTags = AdvCombatEntity_GetProperty("Tag Count", "Full Power Shot Resist")
    DL_PopActiveObject()
    
    --Get information about the cluster. This is needed to give each box a unique ID.
    local iClusterID = AdvCombat_GetProperty("Prediction Get Cluster ID")
        
    --Set the HP requirement
    local iHPRequirement = math.floor(0.30 * iHealthMax)
    if(iFullPowerShotResistTags > 0) then iHPRequirement = math.floor(0.15 * iHealthMax) end
    
    -- |[ ==================== For Each Target =================== ]|
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    for i = 0, iClusterTargetsTotal-1, 1 do
        
        --Create.
        local iTargetID = AdvCombat_GetProperty("Prediction Get Targets ID", i)
        fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, gzRefAbility.zPredictionPackage)
    end
    
    -- |[ =============== Originator Health Drain ================ ]|
    --Compute damage. Cannot KO self!
    local iSelfDamage = iHPRequirement
    if(iSelfDamage >= iHealthCur) then iSelfDamage = iHealthCur - 1 end
    
    --Set lines if self-damage is over 0. If for some reason the HPMAX is 4 or less, no cost!
    if(iSelfDamage > 0) then
        local sBoxName = "Prd" .. 0 .. "x" .. iOwnerID .. iClusterID
        AdvCombat_SetProperty("Prediction Create Box",              sBoxName)
        AdvCombat_SetProperty("Prediction Set Host",                sBoxName, iOwnerID)
        AdvCombat_SetProperty("Prediction Set Offsets",             sBoxName, 0, -100)
        AdvCombat_SetProperty("Prediction Allocate Strings",        sBoxName, 1)
        AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, 0, "Damage: " .. iSelfDamage .. "[IMG0]")
        AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, 0, 1)
        AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, 0, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Health")
    end

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Execute on targets, damage-self.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[ ================ Originator Statistics ================= ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        local iHealthCur = AdvCombatEntity_GetProperty("Health")
        local iHealthMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
        local iFullPowerShotResistTags = AdvCombatEntity_GetProperty("Tag Count", "Full Power Shot Resist")
        
        --CP handler.
        if(gzRefAbility.iCPGain ~= nil and gzRefAbility.iRequiredCP ~= nil) then
            fnModifyCP(gzRefAbility.iCPGain - gzRefAbility.iRequiredCP)
        end
        
        --MP handler.
        if(gzRefAbility.iRequiredMP ~= nil) then
            fnModifyMP(gzRefAbility.iRequiredMP * -1)
        end
        
    DL_PopActiveObject()
        
    --Get how much HP is needed.
    local iHPRequirement = math.floor(0.30 * iHealthMax)
    if(iFullPowerShotResistTags > 0) then iHPRequirement = math.floor(0.15 * iHealthMax) end
    
    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        fnStandardExecution(iOriginatorID, i-1, gzRefAbility.zExecutionAbiPackage, nil)
    end
    
    -- |[ ====================== Self-Damage ===================== ]|
    --Compute damage. Cannot KO self!
    local iSelfDamage = iHPRequirement
    if(iSelfDamage >= iHealthCur) then iSelfDamage = iHealthCur - 1 end
    
    --Set lines if self-damage is over 0. If for some reason the HPMAX is 4 or less, no cost!
    if(iSelfDamage > 0) then
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 0, "Play Sound|" .. gzRefAbility.zExecutionAbiPackage.sAttackSound)
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 0, "Create Animation|Shock|AttackAnim0")
        iAnimTicks = fnGetAnimationTiming("Shock")
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, iAnimTicks, "Damage|" .. iSelfDamage)
    end
    
-- |[ ==================================== Standard Handler ==================================== ]|
--Call standard handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end