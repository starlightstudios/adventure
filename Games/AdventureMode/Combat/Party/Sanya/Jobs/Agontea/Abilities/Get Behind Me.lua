-- |[ ====================================== Get Behind Me ====================================== ]|
-- |[Description]|
--Increases user's attack power, all allies' protection.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzSanyaAgonteaGetBehindMe == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Agontea"
    zAbiStruct.sSkillName    = "Get Behind Me"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Advanced
    zAbiStruct.sIconBacking  = gsAbility_Backing_Free_Buff
    zAbiStruct.sIconFrame    = gsAbility_Frame_Free_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Sanya|Sevavi|GetBehindMe"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "[FEffect]Self, +35%% [Atk] / 3 [Turns].\n[FEffect]Allies, +5[Prt] / 3 [Turns]\n\n\nFree Action.\n3 [Turns] cooldown.\n[Costs]"
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Give cover for your allies by taking the front!\nIncreases your power and their defense.\n3 turn cooldown.\n\nFree Action.\nCosts [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false  --Set to true to ignore all requirements.
    zAbiStruct.iRequiredMP = 10          --MP required to use the ability.
    zAbiStruct.iRequiredCP = 0           --CP required to use the ability.
    zAbiStruct.bRespectsCooldown = true  --If this ability has a cooldown, decrements it.
    zAbiStruct.iCooldown = 4             --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
    zAbiStruct.bIsFreeAction = true      --Flags as a free action.
    zAbiStruct.iRequiredFreeActions = 1  --Free actions set this as 1, everything else as 0.
    zAbiStruct.bRespectActionCap = false --Characters with multiple free actions have a hard action cap. This flag checks that cap.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Allies All"

    --Effects
    zAbiStruct.iCPGain = 0
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Self Package]|
    --Self package shows attack power.
    zAbiStruct.zSelfPredictionPackage = fnCreatePredictionPack()
    zAbiStruct.zSelfPredictionPackage.zaEffectModules[1] = fnCreateEffectModule()
    zAbiStruct.zSelfPredictionPackage.zaEffectModules[1].bIsBuff = true
    zAbiStruct.zSelfPredictionPackage.zaEffectModules[1].sResultScript = "+[SEV] [IMG0]%% / 3 [IMG1]"
    zAbiStruct.zSelfPredictionPackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/StatisticIcons/Attack", "Root/Images/AdventureUI/DebuffIcons/Clock"}
    zAbiStruct.zSelfPredictionPackage.zaEffectModules[1].iSeverity = 25
    
    -- |[Allied Package]|
    --Ally package shows protection.
    zAbiStruct.zAllyPredictionPackage = fnCreatePredictionPack()
    zAbiStruct.zAllyPredictionPackage.zaEffectModules[1] = fnCreateEffectModule()
    zAbiStruct.zAllyPredictionPackage.zaEffectModules[1].bIsBuff = true
    zAbiStruct.zAllyPredictionPackage.zaEffectModules[1].sResultScript = "+[SEV] [IMG0] / 3 [IMG1]"
    zAbiStruct.zAllyPredictionPackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/StatisticIcons/Protection", "Root/Images/AdventureUI/DebuffIcons/Clock"}
    zAbiStruct.zAllyPredictionPackage.zaEffectModules[1].iSeverity = 5
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Self Package]|
    --Attack power version.
    zAbiStruct.zSelfExecutionAbiPackage = fnConstructDefaultAbilityPackage("Buff")
    zAbiStruct.zSelfExecutionAbiPackage.bDoesNoDamage = true
    zAbiStruct.zSelfExecutionAbiPackage.bDoesNoStun = true
    zAbiStruct.zSelfExecutionAbiPackage.bAlwaysHits = true
    zAbiStruct.zSelfExecutionAbiPackage.bEffectAlwaysApplies = true
    
    --Additional Animations.
    fnAddAnimationPackage(zAbiStruct.zSelfExecutionAbiPackage, "Buff Power", 0)
    
    --Effect package.
    zAbiStruct.zSelfExecutionEffPackage = fnConstructDefaultEffectPackage()
    zAbiStruct.zSelfExecutionEffPackage.sEffectPath     = fnResolvePath() .. "../Effects/Get Behind Me Self.lua" 
    zAbiStruct.zSelfExecutionEffPackage.sEffectCritPath = fnResolvePath() .. "../Effects/Get Behind Me Self.lua" 
    
    -- |[Allied Package]|
    --Protection version.
    zAbiStruct.zAllyExecutionAbiPackage = fnConstructDefaultAbilityPackage("Buff")
    zAbiStruct.zAllyExecutionAbiPackage.bDoesNoDamage = true
    zAbiStruct.zAllyExecutionAbiPackage.bDoesNoStun = true
    zAbiStruct.zAllyExecutionAbiPackage.bAlwaysHits = true
    zAbiStruct.zAllyExecutionAbiPackage.bEffectAlwaysApplies = true
    
    --Additional Animations.
    fnAddAnimationPackage(zAbiStruct.zAllyExecutionAbiPackage, "Buff Defense", 0)
    
    --Effect package.
    zAbiStruct.zAllyExecutionEffPackage = fnConstructDefaultEffectPackage()
    zAbiStruct.zAllyExecutionEffPackage.sEffectPath     = fnResolvePath() .. "../Effects/Get Behind Me Ally.lua" 
    zAbiStruct.zAllyExecutionEffPackage.sEffectCritPath = fnResolvePath() .. "../Effects/Get Behind Me Ally.lua" 
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzSanyaAgonteaGetBehindMe = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzSanyaAgonteaGetBehindMe

-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--Builds prediction boxes using the prediction packs based on whether it's an ally or user.
if(iSwitchType == gciAbility_BuildPredictionBox) then
        
    -- |[ ===================== Other Values ===================== ]|
    --Get originator ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
    DL_PopActiveObject()
    
    --Get information about the cluster. This is needed to give each box a unique ID.
    local iClusterID = AdvCombat_GetProperty("Prediction Get Cluster ID")
    
    -- |[ =================== For Each Target ==================== ]|
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    for i = 0, iClusterTargetsTotal-1, 1 do
        
        --Get ID.
        local iTargetID = AdvCombat_GetProperty("Prediction Get Targets ID", i)
        
        --Target is user:
        if(iTargetID == iOwnerID) then
            fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, gzRefAbility.zSelfPredictionPackage)
    
        --Target is ally:
        else
            fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, gzRefAbility.zAllyPredictionPackage)
        end
    end

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[ ======================= Defaults ======================= ]|
    --Default set.
    if(gzRefAbility.bIsFreeAction == nil) then gzRefAbility.bIsFreeAction = false end
    
    -- |[ ================ Originator Statistics ================= ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
    
        --Get ID.
        local iOriginatorID = RO_GetID()
        
        --CP handler.
        if(gzRefAbility.iCPGain ~= nil and gzRefAbility.iRequiredCP ~= nil) then
            fnModifyCP(gzRefAbility.iCPGain - gzRefAbility.iRequiredCP)
        end
        
        --MP handler.
        if(gzRefAbility.iRequiredMP ~= nil) then
            fnModifyMP(gzRefAbility.iRequiredMP * -1)
        end

    DL_PopActiveObject()
    
    -- |[ ==================== Ability Package =================== ]|
    --Create an ability package. This package comes with the default hit/miss values, animations, etc.
    gzRefAbility.zSelfExecutionAbiPackage.iOriginatorID = iOriginatorID
    gzRefAbility.zAllyExecutionAbiPackage.iOriginatorID = iOriginatorID
    
    --Handling of free actions:
    if(gzRefAbility.bIsFreeAction) then
        
        --If the ability is a free action, but does not respect the action cap or consume any free actions,
        -- it is an "Effortless" action and can be used as many times as the player wants.
        if(gzRefAbility.iRequiredFreeActions == 0 and gzRefAbility.bRespectActionCap == false) then
            AdvCombat_SetProperty("Set As Effortless Action")
        
        --Normal free action.
        else
            AdvCombat_SetProperty("Set As Free Action")
        end
    end
    
    --Handling of cooldowns:
    if(gzRefAbility.iCooldown ~= nil and gzRefAbility.iCooldown > 0) then
        AdvCombatAbility_SetProperty("Cooldown", gzRefAbility.iCooldown)
    end
    
    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        
        --Get target ID.
        AdvCombat_SetProperty("Push Target", i-1)
            local iTargetID = RO_GetID()
        DL_PopActiveObject()
        
        --Self:
        if(iTargetID == iOriginatorID) then
            fnStandardExecution(iOriginatorID, i-1, gzRefAbility.zSelfExecutionAbiPackage, gzRefAbility.zSelfExecutionEffPackage)
        
        --Ally:
        else
            fnStandardExecution(iOriginatorID, i-1, gzRefAbility.zAllyExecutionAbiPackage, gzRefAbility.zAllyExecutionEffPackage)
        end
    end
    
-- |[ ==================================== Standard Handler ==================================== ]|
--Call standard handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
