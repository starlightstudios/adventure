-- |[ ==================================== Seductive Dance ===================================== ]|
-- |[Description]|
--6 CP skill that hits all enemies for double their stun threshold. If the enemy cannot be stunned,
-- target takes an additional 100% damage as Terrify.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzSanyaAgonteaSeductiveDance == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Agontea"
    zAbiStruct.sSkillName    = "Seductive Dance"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Combo
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Sanya|Sevavi|SeductiveDance"
    zAbiStruct.sCPIcon       = "Root/Images/AdventureUI/Abilities/Cmb6"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "Inflicts 2.50x[Atk] as [Tfy]. 2x [Stun] cap. All Enemies.\nCannot miss.\nUnstunnable enemies take additional 1.00x[Atk].\n\n\n\n[Costs]"
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "A dance that leaves them stunned. Inflicts double\nthe target's stun threshold of stun damage.\nHits all enemies. Cannot miss.\n\n\nCosts [CPCost][CPIco](CP)."

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false
    zAbiStruct.iRequiredMP = 0
    zAbiStruct.iRequiredCP = 6
    zAbiStruct.bRespectsCooldown = true
    zAbiStruct.iCooldown = 0
    zAbiStruct.bIsFreeAction = false
    zAbiStruct.iRequiredFreeActions = 0
    zAbiStruct.bRespectActionCap = false

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies All"

    --Effects
    zAbiStruct.iCPGain = 0
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Stunnable Prediction Package]|
    --This skill holds two prediction packages. One for unstunnable enemies, and one for stunnable enemies.
    zAbiStruct.zStunPredictionPackage = fnCreatePredictionPack()
    
    --Crit Module
    zAbiStruct.zStunPredictionPackage.bHasCritModule = true
    zAbiStruct.zStunPredictionPackage.zCritModule.iCritThreshold = 100
    zAbiStruct.zStunPredictionPackage.zCritModule.fCritBonus     = 1.25
    zAbiStruct.zStunPredictionPackage.zCritModule.iCritStun      = 25
    
    --Damage Module
    zAbiStruct.zStunPredictionPackage.bHasDamageModule = true
    zAbiStruct.zStunPredictionPackage.zDamageModule.bUseWeaponType = false
    zAbiStruct.zStunPredictionPackage.zDamageModule.iDamageType = gciDamageType_Terrifying
    zAbiStruct.zStunPredictionPackage.zDamageModule.fDamageFactor = 2.50
    zAbiStruct.zStunPredictionPackage.zDamageModule.iScatterRange = 15
    
    --Stun Module
    zAbiStruct.zStunPredictionPackage.bHasStunModule = true
    zAbiStruct.zStunPredictionPackage.zStunModule.iStunBase = 0
    
    -- |[Unstunnable Prediction Package]|
    zAbiStruct.zNoStunPredictionPackage = fnCreatePredictionPack()
    
    --Crit Module
    zAbiStruct.zNoStunPredictionPackage.bHasCritModule = true
    zAbiStruct.zNoStunPredictionPackage.zCritModule.iCritThreshold = 100
    zAbiStruct.zNoStunPredictionPackage.zCritModule.fCritBonus     = 1.25
    zAbiStruct.zNoStunPredictionPackage.zCritModule.iCritStun      = 25
    
    --Damage Module
    zAbiStruct.zNoStunPredictionPackage.bHasDamageModule = true
    zAbiStruct.zNoStunPredictionPackage.zDamageModule.bUseWeaponType = false
    zAbiStruct.zNoStunPredictionPackage.zDamageModule.iDamageType = gciDamageType_Terrifying
    zAbiStruct.zNoStunPredictionPackage.zDamageModule.fDamageFactor = 3.50
    zAbiStruct.zNoStunPredictionPackage.zDamageModule.iScatterRange = 15
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Stunnable Package]|
    --As above, has a stunnable and unstunnable execution variant.
    zAbiStruct.zStunExecutionAbiPackage = fnConstructDefaultAbilityPackage("Terrify")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zStunExecutionAbiPackage.bAlwaysHits     = true
    zAbiStruct.zStunExecutionAbiPackage.bNeverCrits     = true
    zAbiStruct.zStunExecutionAbiPackage.iBaseStunDamage = 100 --Gets recalculated when stun is computer.
    zAbiStruct.zStunExecutionAbiPackage.iDamageType     = gciDamageType_Terrifying
    zAbiStruct.zStunExecutionAbiPackage.fDamageFactor   = 2.50
    zAbiStruct.zStunExecutionAbiPackage.zaTags = {{"Is Attack", 1}}
    
    -- |[Unstunnable Package]|
    --As above, has a stunnable and unstunnable execution variant.
    zAbiStruct.zNoStunExecutionAbiPackage = fnConstructDefaultAbilityPackage("Terrify")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zNoStunExecutionAbiPackage.bAlwaysHits    = true
    zAbiStruct.zNoStunExecutionAbiPackage.bNeverCrits    = true
    zAbiStruct.zNoStunExecutionAbiPackage.iDamageType    = gciDamageType_Terrifying
    zAbiStruct.zNoStunExecutionAbiPackage.fDamageFactor  = 3.50
    zAbiStruct.zNoStunExecutionAbiPackage.zaTags = {{"Is Attack", 1}}
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzSanyaAgonteaSeductiveDance = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzSanyaAgonteaSeductiveDance

-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--Prediction package swaps between packages based on whether the target can be stunned or not.
if(iSwitchType == gciAbility_BuildPredictionBox) then
        
    -- |[ ===================== Other Values ===================== ]|
    --Get originator ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
    DL_PopActiveObject()
    
    --Get information about the cluster. This is needed to give each box a unique ID.
    local iClusterID = AdvCombat_GetProperty("Prediction Get Cluster ID")
    
    -- |[ =================== For Each Target ==================== ]|
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    for i = 0, iClusterTargetsTotal-1, 1 do
        
        --Get target ID.
        local iTargetID = AdvCombat_GetProperty("Prediction Get Targets ID", i)
        
        --Check if target can be stunned.
        AdvCombat_SetProperty("Push Entity By ID", iTargetID)
            local bCanBeStunned = AdvCombatEntity_GetProperty("Is Stunnable")
            local iStunCap = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_StunCap)
        DL_PopActiveObject()
        
        --Target can be stunned:
        if(bCanBeStunned) then
            gzRefAbility.zStunPredictionPackage.zStunModule.iStunBase = iStunCap * 2
            fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, gzRefAbility.zStunPredictionPackage)
        
        --Cannot be stunned:
        else
            fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, gzRefAbility.zNoStunPredictionPackage)
        end
    end

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Execution package swaps between packages based on whether the target can be stunned or not.
elseif(iSwitchType == gciAbility_Execute) then
    
    -- |[ ================ Originator Statistics ================= ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
    
        --Get ID.
        local iOriginatorID = RO_GetID()
        
        --CP handler.
        if(gzRefAbility.iCPGain ~= nil and gzRefAbility.iRequiredCP ~= nil) then
            fnModifyCP(gzRefAbility.iCPGain - gzRefAbility.iRequiredCP)
        end
        
        --MP handler.
        if(gzRefAbility.iRequiredMP ~= nil) then
            fnModifyMP(gzRefAbility.iRequiredMP * -1)
        end

    DL_PopActiveObject()
    
    -- |[ ==================== Ability Package =================== ]|
    --Create an ability package. This package comes with the default hit/miss values, animations, etc.
    gzRefAbility.zStunExecutionAbiPackage.iOriginatorID = iOriginatorID
    gzRefAbility.zNoStunExecutionAbiPackage.iOriginatorID = iOriginatorID
    
    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        
        --Get target properties.
        AdvCombat_SetProperty("Push Target", i-1)
            local bCanBeStunned = AdvCombatEntity_GetProperty("Is Stunnable")
            local iStunCap = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_StunCap)
        DL_PopActiveObject()
        
        --Target can be stunned:
        if(bCanBeStunned) then
            gzRefAbility.zStunExecutionAbiPackage.iBaseStunDamage = iStunCap * 2
            fnStandardExecution(iOriginatorID, i-1, gzRefAbility.zStunExecutionAbiPackage, nil)
        
        --Cannot be stunned:
        else
            fnStandardExecution(iOriginatorID, i-1, gzRefAbility.zNoStunExecutionAbiPackage, nil)
        end
    end
    
-- |[ ==================================== Standard Handler ==================================== ]|
--Call standard handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
