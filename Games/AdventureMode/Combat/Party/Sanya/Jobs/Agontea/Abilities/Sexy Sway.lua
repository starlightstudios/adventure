-- |[ ======================================== Sexy Sway ======================================= ]|
-- |[Description]|
--130% as Terrify. Reduces enemy Atk by 25% for 3 turns.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzSanyaAgonteaSexySway == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Agontea"
    zAbiStruct.sSkillName    = "Sexy Sway"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Debuff
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Sanya|Sevavi|SexySway"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\nAlways hits. Cannot strike critically.\n\n[HEffect][Str6][Tfy] -25%%[Atk] / 3[Turns].\n\n\n[Costs]"
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "The right assets can sap their will to fight.\nDeals increased [Tfy](Terrify) damage, always hits.\nReduces enemy [Atk](Power) for 3 turns.\n\n\nCosts [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false  --Set to true to ignore all requirements.
    zAbiStruct.iRequiredMP = 20          --MP required to use the ability.
    zAbiStruct.iRequiredCP = 0           --CP required to use the ability.
    zAbiStruct.bRespectsCooldown = true  --If this ability has a cooldown, decrements it.
    zAbiStruct.iCooldown = 0             --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
    zAbiStruct.bIsFreeAction = false     --Flags as a free action.
    zAbiStruct.iRequiredFreeActions = 0  --Free actions set this as 1, everything else as 0.
    zAbiStruct.bRespectActionCap = false --Characters with multiple free actions have a hard action cap. This flag checks that cap.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies Single"

    --Effects
    zAbiStruct.iCPGain = 1
    
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Damage Module
    zAbiStruct.zPredictionPackage.bHasDamageModule = true
    zAbiStruct.zPredictionPackage.zDamageModule.iDamageType = gciDamageType_Terrifying
    zAbiStruct.zPredictionPackage.zDamageModule.fDamageFactor = 1.30
    
    --Create an Effect module
    zAbiStruct.zPredictionPackage.zaEffectModules[1] = fnCreateEffectModule()
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iEffectType = gciDamageType_Terrifying
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iEffectStrength = 6
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iSeverity = 25
    zAbiStruct.zPredictionPackage.zaEffectModules[1].sResultScript = "-[SEV] [IMG1] / 3[IMG2]"
    zAbiStruct.zPredictionPackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/StatisticIcons/Attack", "Root/Images/AdventureUI/DebuffIcons/Clock"}
    zAbiStruct.zPredictionPackage.zaEffectModules[1].saApplyTagBonus = {}
    zAbiStruct.zPredictionPackage.zaEffectModules[1].saApplyTagMalus = {}
    zAbiStruct.zPredictionPackage.zaEffectModules[1].saSeverityTagBonus = {}
    zAbiStruct.zPredictionPackage.zaEffectModules[1].saSeverityTagMalus = {}
    
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Terrify")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.iDamageType   = gciDamageType_Terrifying
    zAbiStruct.zExecutionAbiPackage.fDamageFactor = 1.30
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits   = true
    zAbiStruct.zExecutionAbiPackage.bNeverCrits   = true
    zAbiStruct.zExecutionAbiPackage.zaTags = {{"Is Attack", 1}}
    
    -- |[Execution Effect Package]|
    --Create an effect package and populate it.
    zAbiStruct.zExecutionEffPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides.
    zAbiStruct.zExecutionEffPackage.iEffectStr      = 6
    zAbiStruct.zExecutionEffPackage.iEffectCritStr  = 9
    zAbiStruct.zExecutionEffPackage.iEffectType     = gciDamageType_Terrifying
    zAbiStruct.zExecutionEffPackage.sEffectPath     = fnResolvePath() .. "../Effects/Sexy Sway.lua"
    zAbiStruct.zExecutionEffPackage.sEffectCritPath = fnResolvePath() .. "../Effects/Sexy Sway.lua"
    zAbiStruct.zExecutionEffPackage.sApplyText      = "Distracted!"
    zAbiStruct.zExecutionEffPackage.sApplyTextCol   = "Color:Purple"
    zAbiStruct.zExecutionEffPackage.sResistText     = "Resisted!"
    zAbiStruct.zExecutionEffPackage.sResistTextCol  = "Color:White"
    
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzSanyaAgonteaSexySway = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzSanyaAgonteaSexySway

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
