-- |[ ======================================= Lift Skirt ======================================= ]|
-- |[Description]|
--50% as Terrify, all enemies. Rolls a chance to immediately stun them based on their Terrify resist.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzSanyaAgonteaLiftSkirt == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Agontea"
    zAbiStruct.sSkillName    = "Lift Skirt"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Sanya|Sevavi|LiftSkirt"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\nAlways hits. Cannot strike critically.\n\n[HEffect][Str7][Tfy] Immediate stun.\n\n\n[Costs]"
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Those legs go all the way up.\nDeals reduced [Tfy](Terrify) damage, all enemies.\nChance to immediately stun (based on [Tfy] resist).\n\n\nCosts [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false  --Set to true to ignore all requirements.
    zAbiStruct.iRequiredMP = 25          --MP required to use the ability.
    zAbiStruct.iRequiredCP = 0           --CP required to use the ability.
    zAbiStruct.bRespectsCooldown = true  --If this ability has a cooldown, decrements it.
    zAbiStruct.iCooldown = 0             --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
    zAbiStruct.bIsFreeAction = false     --Flags as a free action.
    zAbiStruct.iRequiredFreeActions = 0  --Free actions set this as 1, everything else as 0.
    zAbiStruct.bRespectActionCap = false --Characters with multiple free actions have a hard action cap. This flag checks that cap.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies All"

    --Effects
    zAbiStruct.iCPGain = 1
    
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Damage Module
    zAbiStruct.zPredictionPackage.bHasDamageModule = true
    zAbiStruct.zPredictionPackage.zDamageModule.iDamageType = gciDamageType_Terrifying
    zAbiStruct.zPredictionPackage.zDamageModule.fDamageFactor = 0.50
    
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Terrify")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.iDamageType   = gciDamageType_Terrifying
    zAbiStruct.zExecutionAbiPackage.fDamageFactor = 0.50
    zAbiStruct.zExecutionAbiPackage.iBaseStunDamage = 100
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits   = true
    zAbiStruct.zExecutionAbiPackage.bNeverCrits   = true
    zAbiStruct.zExecutionAbiPackage.zaTags = {{"Is Attack", 1}}
    
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzSanyaAgonteaLiftSkirt = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzSanyaAgonteaLiftSkirt
    
-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--Computation of chance for instant stun must be done manually.
if(iSwitchType == gciAbility_BuildPredictionBox) then

    -- |[ ===================== Basic Module ===================== ]|
    --Setup the basic prediction package.
    local zUsePredictionPack = gzRefAbility.zPredictionPackage
        
    -- |[ ===================== Other Values ===================== ]|
    --Get originator ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
    DL_PopActiveObject()
    
    --Get information about the cluster. This is needed to give each box a unique ID.
    local iClusterID = AdvCombat_GetProperty("Prediction Get Cluster ID")
    
    -- |[ =================== For Each Target ==================== ]|
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    for i = 0, iClusterTargetsTotal-1, 1 do
        
        --Get target info.
        local iTargetID = AdvCombat_GetProperty("Prediction Get Targets ID", i)
        AdvCombat_SetProperty("Push Entity By ID", iTargetID)
            local bIsStunnable = AdvCombatEntity_GetProperty("Is Stunnable")
        DL_PopActiveObject()
        
        --Target is unstunnable.
        if(bIsStunnable == false) then
            
            --Clear extra strings.
            gzRefAbility.zPredictionPackage.zaAdditionalLines[1] = nil
            
            --Create.
            fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, gzRefAbility.zPredictionPackage)
        
        --Target is stunnable.
        else
        
            --Compute chance to apply.
            local iApplyRate = fnComputeEffectApplyRateID(iTargetID, 7, gciDamageType_Terrifying)
        
            --Append.
            gzRefAbility.zPredictionPackage.zaAdditionalLines[1] = {}
            gzRefAbility.zPredictionPackage.zaAdditionalLines[1].sString   = iApplyRate .. "%% Instant Stun"
            gzRefAbility.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
        
            --Create.
            fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, gzRefAbility.zPredictionPackage)
        end
        
    end
    
-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then
    
    -- |[ ================ Originator Statistics ================= ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
    
        --Get ID.
        local iOriginatorID = RO_GetID()
        
        --CP handler.
        if(gzRefAbility.iCPGain ~= nil and gzRefAbility.iRequiredCP ~= nil) then
            fnModifyCP(gzRefAbility.iCPGain - gzRefAbility.iRequiredCP)
        end
        
        --MP handler.
        if(gzRefAbility.iRequiredMP ~= nil) then
            fnModifyMP(gzRefAbility.iRequiredMP * -1)
        end
    
    DL_PopActiveObject()
    
    -- |[ ==================== Ability Package =================== ]|
    --Create an ability package. This package comes with the default hit/miss values, animations, etc.
    local zAbilityPackage = gzRefAbility.zExecutionAbiPackage
    zAbilityPackage.iOriginatorID = iOriginatorID
    
    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        
        --Get target info.
        AdvCombat_SetProperty("Push Target", piTargetIndex)
            local iTargetID = RO_GetID()
            local bIsStunnable = AdvCombatEntity_GetProperty("Is Stunnable")
            local iStunCap = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_StunCap)
        DL_PopActiveObject()
        
        --Target is unstunnable.
        if(bIsStunnable == false) then
            gzRefAbility.zExecutionAbiPackage.iBaseStunDamage = 0
        
        --Target is stunnable.
        else
        
            --Do a roll to see if the stun goes through.
            local bApplyStun = fnStandardEffectApply(iTargetID, 7, gciDamageType_Terrifying, 0)
        
            --Stun applies:
            if(bApplyStun) then
                gzRefAbility.zExecutionAbiPackage.iBaseStunDamage = iStunCap
        
            --Stun fails:
            else
                gzRefAbility.zExecutionAbiPackage.iBaseStunDamage = 0
            end
        end
        
        --Call standard function.
        fnStandardExecution(iOriginatorID, i-1, gzRefAbility.zExecutionAbiPackage, nil)
        
    end
    
-- |[ ==================================== Standard Handler ==================================== ]|
--Call standard handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
