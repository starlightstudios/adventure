-- |[ ======================================== Withstand ======================================= ]|
-- |[Description]|
--Places an AI on the player. Executes next turn, restoring 50% of missing HP. Massively increases
-- protection until the next turn.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabricated copy the first time this script is run.
if(gzSanyaAgonteaWithstand == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Agontea"
    zAbiStruct.sSkillName    = "Withstand"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Heal
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Sanya|Sevavi|Withstand"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "Restores half of missing [Hlt] to self, activates next turn.\n[Prt] +500 until next turn.\n'Slow' while charging.\n\n\n\n[Costs]"
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Regenerate yourself from within.\nRequires a turn to focus, massively increasing\ndefense.\nApplies 'Slow' while charging.\n\nCosts [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false
    zAbiStruct.iRequiredMP = 20
    zAbiStruct.iRequiredCP = 0
    zAbiStruct.bRespectsCooldown = true
    zAbiStruct.iCooldown = 0
    zAbiStruct.bIsFreeAction = false
    zAbiStruct.iRequiredFreeActions = 0
    zAbiStruct.bRespectActionCap = false

    --Targeting
    zAbiStruct.sTargetMacro = "Target Self"

    --Effects
    zAbiStruct.iCPGain = 1
    
    -- |[ ======== Prediction Package ======== ]|
    --Prediction package will populate healing value at time of execution.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()

    --Additional lines. These are plain text/symbols that do not have special properties.
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "Restores half of missing HP."
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[2] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[2].sString   = "Requires a turn to charge."
    zAbiStruct.zPredictionPackage.zaAdditionalLines[2].saSymbols = {}
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    --Basic package.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Healing")
    
    --Apply overrides. Healing is computed at time of execution.
    zAbiStruct.zExecutionAbiPackage.bDoesNoDamage = true
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun = true
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits = true
    zAbiStruct.zExecutionAbiPackage.bNeverCrits = true
    zAbiStruct.zExecutionAbiPackage.bEffectAlwaysApplies = true
    zAbiStruct.zExecutionAbiPackage.iHealingBase = 0
    
    -- |[Execution Self-Effect Package]|
    --This applies Slow to the user. It is used when the AI initializes.
    zAbiStruct.zExecutionSelfPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides.
    zAbiStruct.zExecutionSelfPackage.sEffectPath     = fnResolvePath() .. "../Effects/Withstand.lua"
    zAbiStruct.zExecutionSelfPackage.sEffectCritPath = fnResolvePath() .. "../Effects/Withstand.lua"
    zAbiStruct.zExecutionSelfPackage.sApplyText = "Focusing..."
    zAbiStruct.zExecutionSelfPackage.sApplyTextCol = "Color:White"
    zAbiStruct.zExecutionSelfPackage.sApplyAnimation = "Null"
    zAbiStruct.zExecutionSelfPackage.sApplySound = "Combat\\|Impact_Buff"
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzSanyaAgonteaWithstand = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzSanyaAgonteaWithstand

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
if(iSwitchType == gciAbility_Execute) then
    
    -- |[Setup]|
    --Check if the user has the Withstand effect. This is done via a tag.
    AdvCombat_SetProperty("Push Event Originator")
    
        --Store the ID.
        local iOriginatorID = RO_GetID()
        
        --Store how many times the tag "Withstand Effect" is on the entity. It only needs to be once.
        local iWithstandTag = AdvCombatEntity_GetProperty("Tag Count", "Withstand Effect")
    
    DL_PopActiveObject()
    
    -- |[ =================== Tag Not Present ==================== ]|
    --If the tag is not present, it's because the player is selecting the ability. In that case, we 
    -- apply the effect and the AI that will auto-execute it.
    if(iWithstandTag < 1) then
    
        -- |[Apply AI]|
        --Set the AI but don't run it.
        AdvCombat_SetProperty("Push Event Originator")
            AdvCombatEntity_SetProperty("AI Script", fnResolvePath() .. "../AIs/Withstand.lua")
        DL_PopActiveObject()
        
        --Get the name of the executing ability.
        local sAbilityInternalName = AdvCombatAbility_GetProperty("Internal Name")
        
        --Populate the AI's variables it will need.
        DL_AddPath("Root/Variables/Combat/" .. iOriginatorID .. "_AI/")
        VM_SetVar("Root/Variables/Combat/"  .. iOriginatorID .. "_AI/sMasterNameOfAbility", "S", sAbilityInternalName)
    
        -- |[Originator Statistics]|
        --Gain CP, lose MP.
        AdvCombat_SetProperty("Push Event Originator")
            
            --CP handler.
            if(gzRefAbility.iCPGain ~= nil and gzRefAbility.iRequiredCP ~= nil) then
                fnModifyCP(gzRefAbility.iCPGain - gzRefAbility.iRequiredCP)
            end
            
            --MP handler.
            if(gzRefAbility.iRequiredMP ~= nil) then
                fnModifyMP(gzRefAbility.iRequiredMP * -1)
            end
        DL_PopActiveObject()
        
        -- |[Apply Effect To Self]|
        --This will tell the AI to fire the shot.
        gzRefAbility.zExecutionSelfPackage.fnHandler(iOriginatorID, iOriginatorID, 0, gzRefAbility.zExecutionSelfPackage, true, false)
    
    -- |[ ==================== Tag Is Present ==================== ]|
    --The tag is present. Fire the ability and remove the tag. The AI should remove itself when
    -- this happens and return to the zero state.
    else
    
        -- |[Health]|
        --The ability restores half of the missing HP.
        AdvCombat_SetProperty("Push Event Originator")
            local iCurrentHealth = AdvCombatEntity_GetProperty("Health")
            local iMaxHealth     = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
            local iHealthRestore = math.floor((iMaxHealth - iCurrentHealth) / 2)
        
            --Always restore at least 1 HP.
            if(iHealthRestore < 1) then iHealthRestore = 1.0 end
        
            --Set.
            gzRefAbility.zExecutionAbiPackage.iHealingBase = iHealthRestore
        
        DL_PopActiveObject()
    
        -- |[Execution]|
        --Get how many targets were painted by this ability, iterate across them.
        local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
        for i = 1, iTargetsTotal, 1 do
            fnStandardExecution(iOriginatorID, i-1, gzRefAbility.zExecutionAbiPackage, nil)
        end
    
        -- |[Effect Removal]|
        --Create a list of all offending effects. We must get all of them before removing any as each removal
        -- will re-order the list.
        local iaTagTable = {}
        AdvCombat_SetProperty("Push Event Originator")
        
            --Iterate across effects.
            local iTotalEffectsToRemove = AdvCombatEntity_GetProperty("Effects With Tag", "Withstand Effect")
            for i = 0, iTotalEffectsToRemove-1, 1 do
                local iIDOfEffect = AdvCombatEntity_GetProperty("Effect ID With Tag", "Withstand Effect", i)
                table.insert(iaTagTable, iIDOfEffect)
            end
        
            --Order the program to remove all effects.
            for i = 1, #iaTagTable, 1 do
                AdvCombat_SetProperty("Remove Effect", iaTagTable[i])
            end
        DL_PopActiveObject()
    end

-- |[ ==================================== Standard Handler ==================================== ]|
--Activate normally.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
