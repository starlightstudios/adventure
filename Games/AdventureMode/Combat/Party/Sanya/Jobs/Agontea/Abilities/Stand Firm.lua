-- |[ ======================================= Stand Firm ======================================= ]|
-- |[Description]|
--Increases resistances but greatly reduces evade.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzSanyaAgonteaStandFirm == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Agontea"
    zAbiStruct.sSkillName    = "Stand Firm"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Passive
    zAbiStruct.sIconBacking  = gsAbility_Backing_Buff
    zAbiStruct.sIconFrame    = gsAbility_Frame_Passive
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Sanya|Sevavi|StandFirm"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_Passive
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "-100% [Evd]. +6[Prt]. +3 all resists except [Tfy] and [Stk].\n\n\n\n\n\nPassive."
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Anchor yourself, statues don't dodge.\nZeroes evade, but increases protection and most\nresistances.\n\n\nPassive."

    -- |[Usability Variables]|
    zAbiStruct.bNeverAvailable = true    --Passive abilities have no usability requirements.
    
    -- |[Passive Effect]|
    --Apply this effect when combat starts.
    zAbiStruct.sPassivePath = fnResolvePath() .. "../Effects/Stand Firm.lua"
    
    --GUI Stat Bonuses
    zAbiStruct.bHasGUIEffects = true
    
    -- |[Prediction Package]|
    --Passives don't set anything for prediction packages.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()

    -- |[Execution Ability Package]|
    --Passives don't set anything for execution packages.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Buff")
    
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzSanyaAgonteaStandFirm = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzSanyaAgonteaStandFirm

-- |[ ===================================== All Other Cases ==================================== ]|
if(iSwitchType ~= gciAbility_GUIApplyEffect) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

-- |[ ==================================== GUI: Apply Effect =================================== ]|
--When showing a character's statistics, all equipped abilities are queried. This is not reflected
-- in combat, this is GUI only.
--The standard does not handle this.
elseif(iSwitchType == gciAbility_GUIApplyEffect) then
    AdvCombatAbility_SetProperty("Push Owner")
        fnApplyStatBonus(gciStatIndex_Resist_Slash, 3)
        fnApplyStatBonus(gciStatIndex_Resist_Pierce, 3)
        fnApplyStatBonus(gciStatIndex_Resist_Flame, 3)
        fnApplyStatBonus(gciStatIndex_Resist_Freeze, 3)
        fnApplyStatBonus(gciStatIndex_Resist_Shock, 3)
        fnApplyStatBonus(gciStatIndex_Resist_Crusade, 3)
        fnApplyStatBonus(gciStatIndex_Resist_Obscure, 3)
        fnApplyStatBonus(gciStatIndex_Resist_Bleed, 3)
        fnApplyStatBonus(gciStatIndex_Resist_Poison, 3)
        fnApplyStatBonus(gciStatIndex_Resist_Corrode, 3)
    DL_PopActiveObject()
    gzRefAbility = nil
end
