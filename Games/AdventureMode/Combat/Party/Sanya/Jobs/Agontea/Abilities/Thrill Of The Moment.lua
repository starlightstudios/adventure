-- |[ ================================== Thrill Of The Moment ================================== ]|
-- |[Description]|
--Increases Terrify damage and resistance.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzSanyaAgonteaThrillOfTheMoment == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Agontea"
    zAbiStruct.sSkillName    = "Thrill Of The Moment"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Passive
    zAbiStruct.sIconBacking  = gsAbility_Backing_Buff
    zAbiStruct.sIconFrame    = gsAbility_Frame_Passive
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Sanya|Sevavi|ThrillOfTheMoment"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_Passive
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "+3[Tfy] resistance. +20%% [Tfy] attack power.\n\n\n\n\n\nPassive."
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Get wild, because you only live once!\nIncreases [Tfy](Terrify) resistance and damage\ndealt.\n\n\nPassive."

    -- |[Usability Variables]|
    zAbiStruct.bNeverAvailable = true    --Passive abilities have no usability requirements.
    
    -- |[Passive Effect]|
    --Apply this effect when combat starts.
    zAbiStruct.sPassivePath = fnResolvePath() .. "../Effects/Thrill Of The Moment.lua"
    
    --GUI Stat Bonuses
    zAbiStruct.bHasGUIEffects = true

    --Tags.
    zAbiStruct.zaTags = {{"Terrify Damage Dealt +", 20}}
    
    -- |[Prediction Package]|
    --Passives don't set anything for prediction packages.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()

    -- |[Execution Ability Package]|
    --Passives don't set anything for execution packages.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Buff")
    
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzSanyaAgonteaThrillOfTheMoment = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzSanyaAgonteaThrillOfTheMoment

-- |[ ===================================== All Other Cases ==================================== ]|
if(iSwitchType ~= gciAbility_GUIApplyEffect) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

-- |[ ==================================== GUI: Apply Effect =================================== ]|
--When showing a character's statistics, all equipped abilities are queried. This is not reflected
-- in combat, this is GUI only.
--The standard does not handle this.
elseif(iSwitchType == gciAbility_GUIApplyEffect) then
    AdvCombatAbility_SetProperty("Push Owner")
        fnApplyStatBonus(gciStatIndex_Resist_Terrify, 3)
    DL_PopActiveObject()
    gzRefAbility = nil
end
