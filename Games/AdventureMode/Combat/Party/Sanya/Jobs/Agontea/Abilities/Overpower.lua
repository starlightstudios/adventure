-- |[ ======================================= Overpower ======================================== ]|
-- |[Description]|
--Deals 170% damage to all enemies under 40% health. Cannot crit, will not affect targets over 40% health.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzSanyaAgonteaOverpower == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Agontea"
    zAbiStruct.sSkillName    = "Overpower"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Sanya|Sevavi|Overpower"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit]. Cannot strike critically.\n\nOnly affects enemies under 40%% [Hlt].\n\n\n[Costs]"
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Kick them while they're down, it's the best!\nHits enemies under 40%% HP for major damage.\nDoes nothing to enemies above 40%% HP.\n\n\nCosts [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false
    zAbiStruct.iRequiredMP = 30
    zAbiStruct.iRequiredCP = 0
    zAbiStruct.bRespectsCooldown = true
    zAbiStruct.iCooldown = 0
    zAbiStruct.bIsFreeAction = false
    zAbiStruct.iRequiredFreeActions = 0
    zAbiStruct.bRespectActionCap = false

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies All"

    --Effects
    zAbiStruct.iCPGain = 1
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Standard]|
    --Package used for enemies at or below 40% health.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Hit-Rate Module
    zAbiStruct.zPredictionPackage.bHasHitRateModule = true
    zAbiStruct.zPredictionPackage.zHitRateModule.iMissRate = 5
    
    --Damage Module
    zAbiStruct.zPredictionPackage.bHasDamageModule = true
    zAbiStruct.zPredictionPackage.zDamageModule.bUseWeaponType = true
    zAbiStruct.zPredictionPackage.zDamageModule.iDamageType = gciDamageType_Striking
    zAbiStruct.zPredictionPackage.zDamageModule.fDamageFactor = 1.70
    zAbiStruct.zPredictionPackage.zDamageModule.iScatterRange = 15
    
    -- |[Too Much HP]|
    --Package used for enemies above 40% health.
    zAbiStruct.zHealthyPredictionPackage = fnCreatePredictionPack()
    zAbiStruct.zHealthyPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zHealthyPredictionPackage.zaAdditionalLines[1].sString   = "No effect."
    zAbiStruct.zHealthyPredictionPackage.zaAdditionalLines[1].saSymbols = {}
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Basic Execution]|
    --Basic package.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Weapon")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.bUseWeapon     = true
    zAbiStruct.zExecutionAbiPackage.iDamageType    = gciDamageType_Striking
    zAbiStruct.zExecutionAbiPackage.iMissThreshold = 5
    zAbiStruct.zExecutionAbiPackage.fDamageFactor  = 1.70
    zAbiStruct.zExecutionAbiPackage.bNeverCrits    = true
    zAbiStruct.zExecutionAbiPackage.zaTags = {{"Is Attack", 1}}
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzSanyaAgonteaOverpower = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzSanyaAgonteaOverpower

-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--Switches prediction packages for health vs unhealthy enemies.
if(iSwitchType == gciAbility_BuildPredictionBox) then
        
    -- |[ ===================== Other Values ===================== ]|
    --Get originator ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
    DL_PopActiveObject()
    
    --Get information about the cluster. This is needed to give each box a unique ID.
    local iClusterID = AdvCombat_GetProperty("Prediction Get Cluster ID")
    
    -- |[ =================== For Each Target ==================== ]|
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    for i = 0, iClusterTargetsTotal-1, 1 do
        
        --Get target ID.
        local iTargetID = AdvCombat_GetProperty("Prediction Get Targets ID", i)
        
        --Check if target can be stunned.
        AdvCombat_SetProperty("Push Entity By ID", iTargetID)
            local iHealth    = AdvCombatEntity_GetProperty("Health")
            local iHealthMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
            local fHPPct = iHealth / iHealthMax
        DL_PopActiveObject()
        
        --Target is under 40% health:
        if(fHPPct <= 0.40) then
            fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, gzRefAbility.zPredictionPackage)
        
        --Above 40% health:
        else
            fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, gzRefAbility.zHealthyPredictionPackage)
        end
    end

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Doesn't do anything to targets above 40% health.
elseif(iSwitchType == gciAbility_Execute) then
    
    -- |[ ================ Originator Statistics ================= ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
    
        --Get ID.
        local iOriginatorID = RO_GetID()
        
        --CP handler.
        if(gzRefAbility.iCPGain ~= nil and gzRefAbility.iRequiredCP ~= nil) then
            fnModifyCP(gzRefAbility.iCPGain - gzRefAbility.iRequiredCP)
        end
        
        --MP handler.
        if(gzRefAbility.iRequiredMP ~= nil) then
            fnModifyMP(gzRefAbility.iRequiredMP * -1)
        end

    DL_PopActiveObject()
    
    -- |[ ==================== Ability Package =================== ]|
    --Create an ability package. This package comes with the default hit/miss values, animations, etc.
    gzRefAbility.zExecutionAbiPackage.iOriginatorID = iOriginatorID
    
    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        
        --Get target properties.
        AdvCombat_SetProperty("Push Target", i-1)
            local iHealth    = AdvCombatEntity_GetProperty("Health")
            local iHealthMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
            local fHPPct = iHealth / iHealthMax
        DL_PopActiveObject()
        
        --Target is under 40% health:
        if(fHPPct <= 0.40) then
            fnStandardExecution(iOriginatorID, i-1, gzRefAbility.zExecutionAbiPackage, nil)
        end
    end
    
-- |[ ==================================== Standard Handler ==================================== ]|
--Call standard handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
