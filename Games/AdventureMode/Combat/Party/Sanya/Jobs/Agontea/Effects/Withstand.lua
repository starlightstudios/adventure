-- |[ ======================================== Withstand ======================================= ]|
-- |[Description]|
--Inflicts [Slow] while aiming. Also has a tag to indicate to the AI that it should execute on
-- the next turn.
--When the ability fires, the effect is removed regardless of its remaining timer.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Local Variables ===================================== ]|
-- |[System]|
--This effect uses a standardized StatMod script.
local sDisplayName = "Aimed Shot"
local iDuration = 2
local bIsBuff = false
local sIcon = "Root/Images/AdventureUI/Abilities/Christine|Ram"
local sStatString = "Prt|Flat|500"

--Description can be a variable number of lines.
local saDescription = {}
saDescription[1] = "Focusing within..."
saDescription[2] = "[Slow], acts last next turn."
saDescription[3] = "+500 [Prt]"
saDescription[4] = ""
saDescription[5] = ""
saDescription[6] = ""
saDescription[7] = ""
saDescription[8] = ""

--Run the standardized builder.
gzaStatModStruct            = fnCreateStatmodEffectPrototype(sDisplayName, iDuration, bIsBuff, sIcon, sStatString)
gzaStatModStruct.saStrings  = fnStatmodBuildInspectorStrings(gzaStatModStruct, saDescription)
gzaStatModStruct.sShortText = fnStatmodBuildShortText(gzaStatModStruct)

-- |[Overrides]|
--Subtract one duration if entity has acted this turn, as this affects turn order.
gzaStatModStruct.bLowerDurationIfAlreadyActed = true

--Tags
gzaStatModStruct.zaTagList = {{"Slow", 1}, {"Withstand Effect", 1}, {"Is Positive", 1}}

-- |[ ======================================== Creation ======================================== ]|
--Standard call when effect is initialized.
if(iSwitchType == gciEffect_Create) then
    local fSeverity = 1.0
    if(iArgumentsTotal >= 2) then fSeverity = LM_GetScriptArgument(1, "N") end
    LM_ExecuteScript(gsStandardStatPath, gciEffect_Create, fSeverity)
    
-- |[ ================================== Apply/Unapply Stats =================================== ]|
--Calls the apply/unapply stat routines.
elseif((iSwitchType == gciEffect_ApplyStats or iSwitchType == gciEffect_UnApplyStats) and iArgumentsTotal >= 2) then
    local iTargetID = math.floor(tonumber(LM_GetScriptArgument(1, "N")))
    LM_ExecuteScript(gsStandardStatPath, iSwitchType, iTargetID)

-- |[ ================================== Character Action Ends ================================= ]|
--Called after a character action ends to decrement the turn counter.
elseif(iSwitchType == gciEffect_PostAction) then
    LM_ExecuteScript(gsStandardStatPath, gciEffect_PostAction)
end
