-- |[ ================================== Get Behind Me - Self ================================== ]|
-- |[Description]|
--+35% Atk for 3 turns.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Local Variables ===================================== ]|
-- |[System]|
--This effect uses a standardized StatMod script.
local sDisplayName = "Get Behind Me!"
local iDuration = 3
local bIsBuff = true
local sIcon = "Root/Images/AdventureUI/Abilities/Item|Bomb"
local sStatString = "Atk|Pct|0.35"

--Description can be a variable number of lines.
local saDescription = {}
saDescription[1] = "Taking the lead!"
saDescription[2] = "Increases [Atk](Power)."

--Run the standardized builder.
gzaStatModStruct            = fnCreateStatmodEffectPrototype(sDisplayName, iDuration, bIsBuff, sIcon, sStatString)
gzaStatModStruct.saStrings  = fnStatmodBuildInspectorStrings(gzaStatModStruct, saDescription)
gzaStatModStruct.sShortText = fnStatmodBuildShortText(gzaStatModStruct)

-- |[Overrides]|
--None Yet.

-- |[Tags]|
gzaStatModStruct.zaTagList = {{"Is Positive", 1}}

-- |[ ======================================== Creation ======================================== ]|
--Standard call when effect is initialized.
if(iSwitchType == gciEffect_Create) then
    local fSeverity = 1.0
    if(iArgumentsTotal >= 2) then fSeverity = LM_GetScriptArgument(1, "N") end
    LM_ExecuteScript(gsStandardStatPath, gciEffect_Create, fSeverity)
    
-- |[ ================================== Apply/Unapply Stats =================================== ]|
--Calls the apply/unapply stat routines.
elseif((iSwitchType == gciEffect_ApplyStats or iSwitchType == gciEffect_UnApplyStats) and iArgumentsTotal >= 2) then
    local iTargetID = math.floor(tonumber(LM_GetScriptArgument(1, "N")))
    LM_ExecuteScript(gsStandardStatPath, iSwitchType, iTargetID)

-- |[ ================================== Character Action Ends ================================= ]|
--Called after a character action ends to decrement the turn counter.
elseif(iSwitchType == gciEffect_PostAction) then
    LM_ExecuteScript(gsStandardStatPath, gciEffect_PostAction)
end
