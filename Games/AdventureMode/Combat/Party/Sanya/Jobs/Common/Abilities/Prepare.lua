-- |[ ========================================= Prepare ========================================== ]|
-- |[Description]|
--Boosts user's protection. Increases attack power for next turn.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzSanyaCommonPrepare == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Common"
    zAbiStruct.sSkillName    = "Prepare"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. "Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Buff
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Sanya|Common|Prepare"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "Increases [Prt] by 3 for 1 turn.\nIncreases [Atk] for next turn by 100%%.\n\nGenerates 1[CPIco]."
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Defend yourself and plan your next shot.\nOffensive buff can stack.\n\n\nIncreases [Prt](Protection) slightly.\nGreatly Increases [Atk](Power) for the next action."

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false  --Set to true to ignore all requirements.
    zAbiStruct.iRequiredMP = 0           --MP required to use the ability.
    zAbiStruct.iRequiredCP = 0           --CP required to use the ability.
    zAbiStruct.bRespectsCooldown = true  --If this ability has a cooldown, decrements it.
    zAbiStruct.iCooldown = 0             --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
    zAbiStruct.bIsFreeAction = false     --Flags as a free action.
    zAbiStruct.iRequiredFreeActions = 0  --Free actions set this as 1, everything else as 0.
    zAbiStruct.bRespectActionCap = false --Characters with multiple free actions have a hard action cap. This flag checks that cap.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Self"

    --Effects
    zAbiStruct.iCPGain = 1
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Create an Effect module
    zAbiStruct.zPredictionPackage.zaEffectModules[1] = fnCreateEffectModule()
    zAbiStruct.zPredictionPackage.zaEffectModules[1].bIsBuff = true
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iEffectType = gciDamageType_Piercing
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iEffectStrength = 1000
    zAbiStruct.zPredictionPackage.zaEffectModules[1].sResultScript = "+3 [IMG1] +100%%[IMG2] / 1[IMG3]"
    zAbiStruct.zPredictionPackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/StatisticIcons/Protection", "Root/Images/AdventureUI/StatisticIcons/Attack", "Root/Images/AdventureUI/DebuffIcons/Clock"}
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Buff")
    zAbiStruct.zExecutionAbiPackage.bDoesNoDamage = true
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun = false
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits = true
    zAbiStruct.zExecutionAbiPackage.bEffectAlwaysApplies = true
    
    --Additional Animations.
    fnAddAnimationPackageAuto(zAbiStruct.zExecutionAbiPackage, "Buff Defense", 0)
    fnAddAnimationPackageAuto(zAbiStruct.zExecutionAbiPackage, "Buff Power",   1)
    
    --Does not benefit from the runestone.
    zAbiStruct.zExecutionAbiPackage.zaTags = {{"Ignore User Attack Crits", 1}, {"Ignore User Attack Crits Consume", 1}}
    
    -- |[Execution Effect Package]|
    --Protection package. Increases defense, lasts one turn.
    zAbiStruct.zExecutionEffPackage = fnConstructDefaultEffectPackage()
    zAbiStruct.zExecutionEffPackage.sEffectPath     = fnResolvePath() .. "../Effects/Prepare Protection.lua"
    zAbiStruct.zExecutionEffPackage.sEffectCritPath = fnResolvePath() .. "../Effects/Prepare Protection.lua"
    zAbiStruct.zExecutionEffPackage.sApplySound     = "Combat\\|Impact_Buff"
    
    --Attack Power package. Increases attack power, lasts until Sanya's next attack.
    zAbiStruct.zExecutionEffPackageB = fnConstructDefaultEffectPackage()
    zAbiStruct.zExecutionEffPackageB.sEffectPath     = fnResolvePath() .. "../Effects/Prepare Attack.lua"
    zAbiStruct.zExecutionEffPackageB.sEffectCritPath = fnResolvePath() .. "../Effects/Prepare Attack.lua"
    zAbiStruct.zExecutionEffPackageB.sApplySound     = "Null"
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzSanyaCommonPrepare = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzSanyaCommonPrepare

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
