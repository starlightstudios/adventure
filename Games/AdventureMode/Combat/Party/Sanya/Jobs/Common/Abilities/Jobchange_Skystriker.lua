-- |[ ==================================== Jobchange: Hunter =================================== ]|
-- |[Description]|
--Change jobs to Hunter. Costs CP, Free Action.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--These variables are used to auto-generate the properties of the ability when the ability is a
-- "standard" case. If you want to deviate from the standard, set things manually.
--Because the struct is global, it needs to have a unique name.
if(gzSanyaCommon_JobchangeSkystriker == nil) then
    
    -- |[System]|
    --Setup.
    local zAbiStruct = {}

    --Display/System Variables
    zAbiStruct.sJobName      = "JobChange"
    zAbiStruct.sSkillName    = "Job Change - Skystriker"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.iJPUnlockCost = gciJP_Cost_NoCost
    zAbiStruct.sIconBacking  = gsAbility_Backing_Free_Buff
    zAbiStruct.sIconFrame    = gsAbility_Frame_Free_Combo
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Sanya|Werebat|ThrillOfTheNight"
    zAbiStruct.sCPIcon       = "Root/Images/AdventureUI/Abilities/Cmb2"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "Transform into a Werebat Skystriker.\nCosts [CPCost][CPIco].\nFree Action."
    zAbiStruct.sDescription = ""
    zAbiStruct.saImages = {}
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Transform into a Werebat Skystriker.\nCosts [CPCost][CPIco](CP).\nFree Action.\n\n\n\n"
    zAbiStruct.sSimpleDesc = ""
    zAbiStruct.saSimpleImages = {}

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false  --Set to true to ignore all requirements.
    zAbiStruct.iRequiredMP = 0           --MP required to use the ability.
    zAbiStruct.iRequiredCP = 2           --CP required to use the ability.
    zAbiStruct.bRespectsCooldown = true  --If this ability has a cooldown, decrements it.
    zAbiStruct.iCooldown = 0             --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
    zAbiStruct.bIsFreeAction = true      --Flags as a free action.
    zAbiStruct.iRequiredFreeActions = 1  --Free actions set this as 1, everything else as 0.
    zAbiStruct.bRespectActionCap = true  --Characters with multiple free actions have a hard action cap. This flag checks that cap.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Self"

    --Effects
    zAbiStruct.iCPGain = 0
    
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Additional lines.
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "Transform to Werebat Skystriker"
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
    
    -- |[Execution Ability Package]|
    --Basic package.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("JobChange")
    
    --Overrides.
    zAbiStruct.zExecutionAbiPackage.bDoesNoDamage        = true
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun          = true
    zAbiStruct.zExecutionAbiPackage.bDoesNotAnimate      = true
    zAbiStruct.zExecutionAbiPackage.bNeverGlances        = true
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits          = true
    zAbiStruct.zExecutionAbiPackage.bNeverCrits          = true
    zAbiStruct.zExecutionAbiPackage.bEffectAlwaysApplies = true
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.sChangeJobTo = "Skystriker"
    
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzSanyaCommon_JobchangeSkystriker = zAbiStruct
end

--Because this is a prototype, we set the gzRefAbility to point to the unique structure. This way,
-- all the activity below points to the gzRefAbility's values and we don't need to change the structure's
-- name to create unique names for everything.
--The gsStandardAbilityPath handler also operates on gzRefAbility if it is called. You can use manual
-- overrides to make unique abilities.
gzRefAbility = gzSanyaCommon_JobchangeSkystriker

-- |[ ============================ Combat: Character Action Begins ============================= ]|
--This jobchange is available as long as it is not the current job.
if(iSwitchType == gciAbility_BeginAction or iSwitchType == gciAbility_BeginFreeAction) then

    --Do nothing if the owner is not acting:
    if(AdvCombatAbility_GetProperty("Is Owner Acting") == false) then return end
    
    --Check Job of owner.
    local sCurrentJob = VM_GetVar("Root/Variables/Global/Sanya/sForm", "S")
    if(sCurrentJob == "Werebat") then
        AdvCombatAbility_SetProperty("Usable", false)
        return
    end
    
    --Call standard handler.
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Job Change abilities need to pulse all their class bar abilities and get them to remove any
-- passive effects they have. Otherwise, they run the same standard behavior as the default.
elseif(iSwitchType == gciAbility_Execute) then
    
    -- |[Purge Passive Effects]|
    fnPurgeJobEffects()
    
    -- |[Run Standard Handler]|
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
    
-- |[ ==================================== Standard Handler ==================================== ]|
--Standard handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
