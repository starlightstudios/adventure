-- |[ ====================================== Flank Tactics ===================================== ]|
-- |[Description]|
--Passive, +100% damage to enemies with a [Prt] buff.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--Create a prefabricated copy the first time this script is run.
if(gzSanyaRangerFlankTactics == nil) then
    
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Ranger"
    zAbiStruct.sSkillName    = "Flank Tactics"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Passive
    zAbiStruct.sIconBacking  = gsAbility_Backing_Buff
    zAbiStruct.sIconFrame    = gsAbility_Frame_Passive
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Sanya|Harpy|FlankTactics"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_Passive
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "+100%% [Atk] against enemies with [Prt] buffs.\n\n\n\n\n\nPassive."
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Get behind enemies with tough defenses.\nIncreases [Atk](Power) when attacking enemies\nwith [Prt](Defense) buffs.\n\n\nPassive."

    -- |[Usability Variables]|
    zAbiStruct.bNeverAvailable = true    --Passive abilities have no usability requirements.
    
    -- |[Passive Effect]|
    --Apply this effect when combat starts.
    zAbiStruct.sPassivePath = fnResolvePath() .. "../Effects/Flank Tactics.lua"
    
    --GUI Stat Bonuses
    zAbiStruct.bHasGUIEffects = false
    
    -- |[Prediction Package]|
    --Passives don't set anything for prediction packages.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()

    -- |[Execution Ability Package]|
    --Passives don't set anything for execution packages.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Buff")
    
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzSanyaRangerFlankTactics = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzSanyaRangerFlankTactics

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
