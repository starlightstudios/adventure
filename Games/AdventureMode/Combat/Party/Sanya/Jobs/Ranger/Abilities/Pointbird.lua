-- |[ ======================================== Pointbird ======================================= ]|
-- |[Description]|
--Increases Sanya's threat by 3x her current HP. Free action.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzSanyaRangerPointbird == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Ranger"
    zAbiStruct.sSkillName    = "Pointbird"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Cheap
    zAbiStruct.sIconBacking  = gsAbility_Backing_Free_Buff
    zAbiStruct.sIconFrame    = gsAbility_Frame_Free_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Sanya|Harpy|Pointbird"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "[Threat] equal to 3x current [Hlt].\n\n\n\n\n\nFree Action."
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Take point in the flock, increasing threat.\nThreat increases based on your current HP.\n\n\n\nFree action."

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false  --Set to true to ignore all requirements.
    zAbiStruct.iRequiredMP = 0           --MP required to use the ability.
    zAbiStruct.iRequiredCP = 0           --CP required to use the ability.
    zAbiStruct.bRespectsCooldown = true  --If this ability has a cooldown, decrements it.
    zAbiStruct.iCooldown = 0             --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
    zAbiStruct.bIsFreeAction = true      --Flags as a free action.
    zAbiStruct.iRequiredFreeActions = 1  --Free actions set this as 1, everything else as 0.
    zAbiStruct.bRespectActionCap = true  --Characters with multiple free actions have a hard action cap. This flag checks that cap.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies All"

    --Effects
    zAbiStruct.iCPGain = 0
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Create an Effect module
    zAbiStruct.zPredictionPackage.zaEffectModules[1] = fnCreateEffectModule()
    zAbiStruct.zPredictionPackage.zaEffectModules[1].bIsBuff = true
    zAbiStruct.zPredictionPackage.zaEffectModules[1].sResultScript = "+Threat"
    zAbiStruct.zPredictionPackage.zaEffectModules[1].saResultImages = {}
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    --Basic package.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Terrify")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.bDoesNoDamage = true
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun = true
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits = true
    zAbiStruct.zExecutionAbiPackage.bEffectAlwaysApplies = true
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzSanyaRangerPointbird = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzSanyaRangerPointbird

-- |[ ==================================== Standard Handler ==================================== ]|
--Call the standardized handler.
if(iSwitchType ~= gciAbility_Execute) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--This ability does no damage but does apply a lot of threat. The threat is a flat 1000 regardless
-- of other factors. This may need to be adjusted later.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[========== Originator Statistics ========== ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        local iCurHP = AdvCombatEntity_GetProperty("Health")
    DL_PopActiveObject()
    
    -- |[========== Ability Package ========== ]|
    --Create an ability package. This package comes with the default hit/miss values, animations, etc.
    local zaAbilityPackage = fnConstructDefaultAbilityPackage("Terrify")
    zaAbilityPackage.iOriginatorID = iOriginatorID
    
    --Apply overrides. We only edit parts that may not be standard.
    zaAbilityPackage.bDoesNoDamage = true
    zaAbilityPackage.bDoesNoStun = true
    zaAbilityPackage.bAlwaysHits = true
    zaAbilityPackage.bEffectAlwaysApplies = true
    
    --Handling of free actions:
    if(gzRefAbility.bIsFreeAction) then
        
        --If the ability is a free action, but does not respect the action cap or consume any free actions,
        -- it is an "Effortless" action and can be used as many times as the player wants.
        if(gzRefAbility.iRequiredFreeActions == 0 and gzRefAbility.bRespectActionCap == false) then
            AdvCombat_SetProperty("Set As Effortless Action")
        
        --Normal free action.
        else
            AdvCombat_SetProperty("Set As Free Action")
        end
    end
    
    --Handling of cooldowns:
    if(gzRefAbility.iCooldown > 0) then
        AdvCombatAbility_SetProperty("Cooldown", gzRefAbility.iCooldown)
    end
    
    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        fnStandardExecution(iOriginatorID, i-1, zaAbilityPackage, nil)
        
        --Non-standard. Apply a "Threat" to the AI. The threat scales with attack power. Note that this applies even if the attack misses.
        AdvCombat_SetProperty("Push Target", i-1)
            local iTargetID = RO_GetID()
        DL_PopActiveObject()
        
        --Compute.
        local sApplyString = math.floor(iCurHP * 3)
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetID, 0, "AI_APPLICATION|Threat|" .. sApplyString)
    end
end
