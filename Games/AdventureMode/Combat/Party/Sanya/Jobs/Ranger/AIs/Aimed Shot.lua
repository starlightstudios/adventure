-- |[ ======================================= Aimed Shot ======================================= ]|
--A simple AI that is used for Sanya's Aimed Shot ability. The AI takes over for one turn and shoots
-- the target specified immediately, then expires.
--If the target specified is not available, hits a random target instead.

-- |[Special Notes]|
--Once the player selects the ability to use, they should create the following variables, where the 
-- uniqueID is the ID of the acting character. Be sure to create the DLPath with DL_AddPath().
--VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/iAimedShotTargetID", "N", 0.0)
--VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "_AI/sMasterNameOfAbility", "S", "Null")

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")
    
-- |[ ===================================== Combat Begins ====================================== ]|
--Called when combat begins, but before turn order is resolved for the 0th turn. If this AI was
-- arriving as reinforcements, this will be called at the start of the turn when they arrive before
-- turn order is resolved.
--If arriving in the middle of combat, the AI can determine they are a reinforcement by checking 
-- the turn counter. If it's 0, they were not a reinforcement.
if(iSwitchType == gciAI_CombatStart) then

-- |[ ====================================== Turn Begins ======================================= ]|
--Called when the turn begins, after turn order is resolved.
elseif(iSwitchType == gciAI_TurnStart) then

-- |[ ==================================== AI Begins Action ==================================== ]|
--Called when a new Action begins, *not necessarily the action of the owning entity*. If it is the
-- action of the owning entity, we need to decide what action to perform. Otherwise, set variables
-- if the AI requires counting actions.
elseif(iSwitchType == gciAI_ActionBegin or iSwitchType == gciAI_FreeActionBegin) then

-- |[ ==================================== AI Ends Action ====================================== ]|
--Called after an action ends, before the turn order is popped.
elseif(iSwitchType == gciAI_ActionEnd) then

-- |[ ======================================= Turn Ends ======================================== ]|
--Called after the turn ends but before turn order is resolved for the next turn or reinforcements
-- arrive, if any do.
elseif(iSwitchType == gciAI_TurnEnd) then

-- |[ ==================================== AI is Defeated ====================================== ]|
--Called when this entity is KO'd. Note that an enemy can be revived (in theory) which can cause
-- this to execute each time they are KO'd.
elseif(iSwitchType == gciAI_KnockedOut) then

-- |[ ====================================== Combat Ends ======================================= ]|
--Called when combat ends but before XP/JP/etc are awarded.
elseif(iSwitchType == gciAI_CombatEnds) then

-- |[ ================================ Application Pack Before ================================= ]|
--Called right before an application pack is processed, allowing querying and modification.
elseif(iSwitchType == gciAI_Application_Start) then

-- |[ ================================ Application Pack After ================================== ]|
--Called right after an application pack is processed, allowing querying.
elseif(iSwitchType == gciAI_Application_End) then
    
-- |[ ====================================== Decide Action ===================================== ]|
--Decide which attack to use. Standard handling.
elseif(iSwitchType == gciAI_Decide_Action) then

    -- |[ ==================== Pre-Exec Setup ==================== ]|
    -- |[Events]|
    --Make sure the AI needs to spawn events. This flag is only true when the AI needs to spawn events.
    local bCanSpawnEvent = AdvCombat_GetProperty("Is AI Spawning Events")
    if(bCanSpawnEvent == false) then return end

    -- |[ ===================== Turn Checking ==================== ]|
    -- |[Check Turn]|
    --At this point, check if the AI's owner is the one who is actually acting. Anything above this point is status
    -- modifiers, everything below this point is attempting to execute an ability.
    local iOwnerID = RO_GetID()
    local iActingID = AdvCombat_GetProperty("ID of Acting Entity")
    if(iOwnerID ~= iActingID) then return end

    -- |[ ================== Ability Execution =================== ]|
    --Get the master name of the ability.
    local sMasterName = VM_GetVar("Root/Variables/Combat/" .. iOwnerID .. "_AI/sMasterNameOfAbility", "S")
    local iMasterSlot = AdvCombatEntity_GetProperty("Slot of Ability In Master List", sMasterName)
    AdvCombat_SetProperty("Set Ability As Active", iMasterSlot)
    
    -- |[Clear AI]|
    --AI unsets itself to return to normal control.
    AdvCombatEntity_SetProperty("AI Script", "Null")
    
    -- |[Paint Targets, Execute]|
    --Paint available targets.
    AdvCombat_SetProperty("Run Active Ability Target Script")
    
    --Scan target clusters. Find the one that has the selected target's ID in it.
    local bDone = false
    local iTrueTargetID = VM_GetVar("Root/Variables/Combat/" .. iOwnerID .. "_AI/iAimedShotTargetID", "I")
    local iTotalTargetClusters = AdvCombat_GetProperty("Total Target Clusters")
    for i = 0, iTotalTargetClusters-1, 1 do
        
        --Get how many targets are in the cluster.
        local iTotalTargets = AdvCombat_GetProperty("Total Targets In Cluster", i)
        for p = 0, iTotalTargets-1, 1 do
        
            --Get this target's ID.
            local iTargetID = AdvCombat_GetProperty("Target ID In Cluster", i, p)
        
            --Match. Set this as the target.
            if(iTargetID == iTrueTargetID) then
                bDone = true
                AdvCombat_SetProperty("Set Target Cluster As Active", i)
                AdvCombat_SetProperty("Mark Handled Action")
                AdvCombat_SetProperty("Run Active Ability On Active Targets")
                return
            end
        end
    end
    
    -- |[Target Is Not Present]|
    --If we got this far, then the target the player originally picked is gone. This may be due to them
    -- being defeated, retreating, switching parties, etc. In any case, pick a random target.
    local iRandomCluster = LM_GetRandomNumber(0, iTotalTargetClusters-1)
    AdvCombat_SetProperty("Set Target Cluster As Active", iRandomCluster)
    AdvCombat_SetProperty("Mark Handled Action")
    AdvCombat_SetProperty("Run Active Ability On Active Targets")
    
-- |[ ===================================== Threat Update ====================================== ]|
--Call this whenever you need to update the threat values for this AI.
elseif(iSwitchType == gciAI_UpdateThreat) then

end
