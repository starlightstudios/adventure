-- |[ =================================== Croatian Handshake =================================== ]|
-- |[Description]|
--Inflict 50% bleed damage and 100% additional bleed damage over 3 turns.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzSanyaScofflawCroatianHandshake == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Scofflaw"
    zAbiStruct.sSkillName    = "Croatian Handshake"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_DoT
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Sanya|Bunny|CroatianHandshake"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\n[DoT].\n\n\n[Costs]"
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "They think you're making peace, until you\nseriously cut them!\nBecause it's actually a broken bottle.\nCauses a painful [Bld](Bleed) DoT.\n\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false  --Set to true to ignore all requirements.
    zAbiStruct.iRequiredMP = 20          --MP required to use the ability.
    zAbiStruct.iRequiredCP = 0           --CP required to use the ability.
    zAbiStruct.bRespectsCooldown = true  --If this ability has a cooldown, decrements it.
    zAbiStruct.iCooldown = 0             --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
    zAbiStruct.bIsFreeAction = false     --Flags as a free action.
    zAbiStruct.iRequiredFreeActions = 0  --Free actions set this as 1, everything else as 0.
    zAbiStruct.bRespectActionCap = false --Characters with multiple free actions have a hard action cap. This flag checks that cap.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies Single"

    --Effects
    zAbiStruct.iCPGain = 1
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Hit-Rate Module
    zAbiStruct.zPredictionPackage.bHasHitRateModule = true
    zAbiStruct.zPredictionPackage.zHitRateModule.iMissRate = 5
    
    --Crit Module
    zAbiStruct.zPredictionPackage.bHasCritModule = true
    zAbiStruct.zPredictionPackage.zCritModule.iCritThreshold = 100
    
    --Damage Module
    zAbiStruct.zPredictionPackage.bHasDamageModule = true
    zAbiStruct.zPredictionPackage.zDamageModule.iDamageType = gciDamageType_Bleeding
    zAbiStruct.zPredictionPackage.zDamageModule.fDamageFactor = 0.50
    
    --Create an Effect module
    zAbiStruct.zPredictionPackage.zaEffectModules[1] = fnCreateEffectModule()
    zAbiStruct.zPredictionPackage.zaEffectModules[1].bIsDamageOverTime = true
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iEffectType = gciDamageType_Bleeding
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iDotDuration = 3
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iDoTDamageType = gciDamageType_Bleeding
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iEffectStrength = 5
    zAbiStruct.zPredictionPackage.zaEffectModules[1].fDotAttackFactor = 0.50
    zAbiStruct.zPredictionPackage.zaEffectModules[1].sResultScript = "[DAM][IMG1] for 3[IMG2]"
    zAbiStruct.zPredictionPackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/DamageTypeIcons/Bleeding", "Root/Images/AdventureUI/DebuffIcons/Clock"}
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Claw Slash")
    zAbiStruct.zExecutionAbiPackage.iDamageType    = gciDamageType_Bleeding
    zAbiStruct.zExecutionAbiPackage.iMissThreshold = 5
    zAbiStruct.zExecutionAbiPackage.fDamageFactor  = 0.50
    zAbiStruct.zExecutionAbiPackage.zaTags = {{"Is Attack", 1}}
    
    --Additional Animation
    fnAddAnimationPackage(zAbiStruct.zExecutionAbiPackage, "Bleed", 10)
    
    -- |[Execution Effect Package]|
    --Create an effect package and populate it.
    zAbiStruct.zExecutionEffPackage = fnConstructDefaultEffectPackage()
    zAbiStruct.zExecutionEffPackage.iEffectStr      = 5
    zAbiStruct.zExecutionEffPackage.iEffectCritStr  = 8
    zAbiStruct.zExecutionEffPackage.iEffectType     = gciDamageType_Bleeding
    zAbiStruct.zExecutionEffPackage.sEffectPath     = fnResolvePath() .. "../Effects/Croatian Handshake.lua"
    zAbiStruct.zExecutionEffPackage.sEffectCritPath = fnResolvePath() .. "../Effects/Croatian Handshake Crit.lua"
    zAbiStruct.zExecutionEffPackage.sApplyText      = "Bleeding!"
    zAbiStruct.zExecutionEffPackage.sApplyTextCol   = "Color:Red"
    zAbiStruct.zExecutionEffPackage.sCritApplyText  = "Badly Bleeding!"
    zAbiStruct.zExecutionEffPackage.sResistText     = "Resisted!"
    zAbiStruct.zExecutionEffPackage.sResistTextCol  = "Color:White"
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzSanyaScofflawCroatianHandshake = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzSanyaScofflawCroatianHandshake

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
