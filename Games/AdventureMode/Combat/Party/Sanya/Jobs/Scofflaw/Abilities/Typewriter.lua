-- |[ ======================================= Typewriter ======================================= ]|
-- |[Description]|
--Performs three normal attacks on a single target.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzSanyaScofflawTypewriter == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Scofflaw"
    zAbiStruct.sSkillName    = "Typewriter"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_DoT
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Sanya|Bunny|Typewriter"
    zAbiStruct.sCPIcon       = "Root/Images/AdventureUI/Abilities/Cmb4"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n3x attacks.\n\n\n\n[Costs]"
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Keep the change, ya filthy monstergirl.\nPerforms 3 basic attacks against a target.\nEach attack hits/crits independently.\n\n\nCosts [CPCost][CPIco](CP)."
    
    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false  --Set to true to ignore all requirements.
    zAbiStruct.iRequiredMP = 0           --MP required to use the ability.
    zAbiStruct.iRequiredCP = 4           --CP required to use the ability.
    zAbiStruct.bRespectsCooldown = true  --If this ability has a cooldown, decrements it.
    zAbiStruct.iCooldown = 0             --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
    zAbiStruct.bIsFreeAction = false     --Flags as a free action.
    zAbiStruct.iRequiredFreeActions = 0  --Free actions set this as 1, everything else as 0.
    zAbiStruct.bRespectActionCap = false --Characters with multiple free actions have a hard action cap. This flag checks that cap.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies Single"

    --Effects
    zAbiStruct.iCPGain = 1
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Crit Module
    zAbiStruct.zPredictionPackage.bHasCritModule = true
    zAbiStruct.zPredictionPackage.zCritModule.iCritThreshold = 100
    
    --Damage Module
    zAbiStruct.zPredictionPackage.bHasDamageModule = true
    zAbiStruct.zPredictionPackage.zDamageModule.bUseWeaponType = true
    zAbiStruct.zPredictionPackage.zDamageModule.iDamageType = gciDamageType_Striking
    zAbiStruct.zPredictionPackage.zDamageModule.fDamageFactor = 1.00
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Weapon")
    zAbiStruct.zExecutionAbiPackage.bUseWeapon = true
    zAbiStruct.zExecutionAbiPackage.iDamageType    = gciDamageType_Striking
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits    = true
    zAbiStruct.zExecutionAbiPackage.fDamageFactor  = 1.25
    zAbiStruct.zExecutionAbiPackage.zaTags = {{"Is Attack", 1}}
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzSanyaScofflawTypewriter = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzSanyaScofflawTypewriter

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
if(iSwitchType == gciAbility_Execute) then

    -- |[ ========== Originator Statistics ========== ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        fnModifyCP(gzRefAbility.iCPGain - gzRefAbility.iRequiredCP)
        fnModifyMP(gzRefAbility.iRequiredMP * -1)
    DL_PopActiveObject()
    
    -- |[ ========== Ability Package ========== ]|
    --Fire three times at the target.
    for i = 1, 3, 1 do
    
        --Common.
        gzRefAbility.zExecutionAbiPackage.iOriginatorID  = iOriginatorID
    
        --Set the combat timer offset. This allows multiple hits to go through, but makes them not simultaneous.
        giCombatTimerOffset = 15 * (i-1)
        
        --Run.
        fnStandardExecution(iOriginatorID, 0, gzRefAbility.zExecutionAbiPackage, nil)
    end

-- |[ ==================================== Standard Handler ==================================== ]|
--All other cases, call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
