-- |[ ==================================== Thrill Of Crime ===================================== ]|
-- |[Description]|
--+1 CP when scoring a crit.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzSanyaScofflawThrillOfCrime == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Scofflaw"
    zAbiStruct.sSkillName    = "Thrill Of Crime"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Passive
    zAbiStruct.sIconBacking  = gsAbility_Backing_Buff
    zAbiStruct.sIconFrame    = gsAbility_Frame_Passive
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Sanya|Bunny|ThrillOfCrime"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_Passive
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "+1CP when landing a critical.\n\n\n\n\n\nPassive."
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Breaking the law is seductive and dangerous.\n+1 CP generated when scoring a critical hit.\n\n\n\nPassive."

    -- |[Usability Variables]|
    zAbiStruct.bNeverAvailable = true    --Passive abilities have no usability requirements.
    
    -- |[Passive Effect]|
    --Apply this effect when combat starts.
    zAbiStruct.sPassivePath = fnResolvePath() .. "../Effects/Thrill Of Crime.lua"
    
    --GUI Stat Bonuses
    zAbiStruct.bHasGUIEffects = true

    --Tags.
    zAbiStruct.zaTags = {{"+CP On Crit", 1}}
    
    -- |[Prediction Package]|
    --Passives don't set anything for prediction packages.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()

    -- |[Execution Ability Package]|
    --Passives don't set anything for execution packages.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Buff")
    
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzSanyaScofflawThrillOfCrime = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzSanyaScofflawThrillOfCrime

--Call standard handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
