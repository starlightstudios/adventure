-- |[ ======================================= Spicy Rum ======================================== ]|
-- |[Description]|
--Deals burning damage equal to 0.30 of user's attack power over 3 turns.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--Setup, apply overrides.
if(gzEffectScofflawSpicyRum == nil) then
    
    --Base, overrides.
    local zEffectStruct = fnCreateDoTEffectPrototype(gciDamageType_Flaming, 3, 0.30)
    zEffectStruct.sDisplayName = "Spicy Rum"
    zEffectStruct.sIcon = "Sanya|Bunny|SpicyRum"
    
    --Tags
    zEffectStruct.zaTagList = {{"Flaming DoT", 1}, {"Is Negative", 1}}
    
    --Store
    gzEffectScofflawSpicyRum = zEffectStruct
    
end

-- |[Set and Execute]|
--Standardized Naming
gzRefEffect = gzEffectScofflawSpicyRum

--Call the standard handler. It will nil off the gzRefEffect.
LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType)
