-- |[ ================================= Croatian Handshake Crit ================================= ]|
-- |[Description]|
--Deals bleeding damage equal to 1.00 of user's attack power over 3 turns.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--Setup, apply overrides.
if(gzEffectScofflawCroatianHandshakeCrit == nil) then
    
    --Base, overrides.
    local zEffectStruct = fnCreateDoTEffectPrototype(gciDamageType_Bleeding, 5, 1.66)
    zEffectStruct.sDisplayName = "Croatian Handshake Crit"
    zEffectStruct.sIcon = "Sanya|Bunny|CroatianHandshake"
    
    --Tags
    zEffectStruct.zaTagList = {{"Bleeding DoT", 1}, {"Is Negative", 1}}
    
    --Store
    gzEffectScofflawCroatianHandshakeCrit = zEffectStruct
    
end

-- |[Set and Execute]|
--Standardized Naming
gzRefEffect = gzEffectScofflawCroatianHandshakeCrit

--Call the standard handler. It will nil off the gzRefEffect.
LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType)
