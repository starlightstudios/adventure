-- |[ ==================================== Spicy Rum Crit ====================================== ]|
-- |[Description]|
--Deals burning damage equal to 0.50 of user's attack power over 5 turns.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--Setup, apply overrides.
if(gzEffectScofflawSpicyRumCrit == nil) then
    
    --Base, overrides.
    local zEffectStruct = fnCreateDoTEffectPrototype(gciDamageType_Flaming, 5, 0.50)
    zEffectStruct.sDisplayName = "Spicy Rum Crit"
    zEffectStruct.sIcon = "Sanya|Bunny|SpicyRum"
    
    --Tags
    zEffectStruct.zaTagList = {{"Flaming DoT", 1}, {"Is Negative", 1}}
    
    --Store
    gzEffectScofflawSpicyRumCrit = zEffectStruct
    
end

-- |[Set and Execute]|
--Standardized Naming
gzRefEffect = gzEffectScofflawSpicyRumCrit

--Call the standard handler. It will nil off the gzRefEffect.
LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType)
