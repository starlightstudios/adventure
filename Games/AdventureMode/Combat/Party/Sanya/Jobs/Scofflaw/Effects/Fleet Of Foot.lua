-- |[ ====================================== Fleet Of Foot ===================================== ]|
-- |[Description]|
--Increases Ini bt 50, Evd by 25.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Local Variables ===================================== ]|
-- |[System]|
--This effect uses a standardized StatMod script.
local sDisplayName = "Fleet Of Foot"
local iDuration = -1
local bIsBuff = true
local sIcon = "Root/Images/AdventureUI/Abilities/Sanya|Bunny|FleetOfFoot"
local sStatString = "Ini|Flat|50 Evd|Flat|25"

--Description can be a variable number of lines.
local saDescription = {}
saDescription[1] = "Faster than a speeding bullet!"
saDescription[2] = "Increases [Ini] and [Evd]."

--Run the standardized builder.
gzaStatModStruct            = fnCreateStatmodEffectPrototype(sDisplayName, iDuration, bIsBuff, sIcon, sStatString)
gzaStatModStruct.saStrings  = fnStatmodBuildInspectorStrings(gzaStatModStruct, saDescription)
gzaStatModStruct.sShortText = fnStatmodBuildShortText(gzaStatModStruct)

-- |[Overrides]|
--Passive, not removed on KO, uses different frame.
gzaStatModStruct.sFrame      = gsAbility_Frame_Passive
gzaStatModStruct.bRemoveOnKO = false

-- |[Tags]|
gzaStatModStruct.zaTagList = {{"Is Positive", 1}, {"Is Passive", 1}}

-- |[ ======================================== Creation ======================================== ]|
--Standard call when effect is initialized.
if(iSwitchType == gciEffect_Create) then
    LM_ExecuteScript(gsStandardStatPath, gciEffect_Create)
    
-- |[ ================================== Apply/Unapply Stats =================================== ]|
--Calls the apply/unapply stat routines.
elseif((iSwitchType == gciEffect_ApplyStats or iSwitchType == gciEffect_UnApplyStats) and iArgumentsTotal >= 2) then
    local iTargetID = math.floor(tonumber(LM_GetScriptArgument(1, "N")))
    LM_ExecuteScript(gsStandardStatPath, iSwitchType, iTargetID)

-- |[ ================================== Character Action Ends ================================= ]|
--Called after a character action ends to decrement the turn counter.
elseif(iSwitchType == gciEffect_PostAction) then
    LM_ExecuteScript(gsStandardStatPath, gciEffect_PostAction)
end
