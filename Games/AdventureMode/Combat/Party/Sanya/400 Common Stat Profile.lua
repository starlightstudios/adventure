-- |[ ====================================== Stat Profile ====================================== ]|
--Unless otherwise noted, this character's jobs all share the same growth curve. Each class simply
-- provides a set of bonuses.
if(fnArgCheck(1) == false) then return end

--Slot.
local iSlot  = LM_GetScriptArgument(0, "N")

--Zero off the first level.
giaHPArray  = {}
giaAtkArray = {}
giaAccArray = {}
giaEvdArray = {}
giaIniArray = {}

giaHPArray[0] = 0
giaAtkArray[0] = 0
giaAccArray[0] = 0
giaEvdArray[0] = 0
giaIniArray[0] = 0

-- |[Description]|
--HP:  HI
--Atk: AV
--Acc: AV
--Evd: BA
--Ini: BA

-- |[ ========================================= Max HP ========================================= ]|
fnRunRegime(giaHPArray,  1, 22,  7, 11, gciCurveRevCubic)
fnRunRegime(giaHPArray, 23, 68,  5,  9, gciCurveCubic)
fnRunRegime(giaHPArray, 69, 99,  0,  2, gciCurveRevLinear)

-- |[ ====================================== Attack Power ====================================== ]|
fnRunRegime(giaAtkArray,  1, 16,  5, 7, gciCurveSquare)
fnRunRegime(giaAtkArray, 17, 35,  3, 5, gciCurveSquare)
fnRunRegime(giaAtkArray, 36, 67,  2, 4, gciCurveSquare)
fnRunRegime(giaAtkArray, 68, 99,  0, 1, gciCurveRevSquare)

-- |[ ======================================== Accuracy ======================================== ]|
fnRunRegime(giaAccArray,  1, 14, 2.4, 3.4, gciCurveSquare)
fnRunRegime(giaAccArray, 15, 34, 1.2, 1.9, gciCurveSquare)
fnRunRegime(giaAccArray, 35, 62, 1.4, 2.3, gciCurveCubic)
fnRunRegime(giaAccArray, 63, 72, 1.2, 1.3, gciCurveRevCubic)
fnRunRegime(giaAccArray, 73, 99, 0.2, 0.8, gciCurveRevCubic)

-- |[ ========================================= Evade ========================================== ]|
fnRunRegime(giaEvdArray,  1, 35, 1.2, 1.6, gciCurveRevSquare)
fnRunRegime(giaEvdArray, 36, 99, 0.9, 1.4, gciCurveSquare)

-- |[ ======================================= Initiative ======================================= ]|
fnRunRegime(giaIniArray,  1, 99, 0.3, 0.5, gciCurveRevSquare)

-- |[ ========================================= Report ========================================= ]|
--If the slot value is -1, this is being used to compute job statistics and not debug. Stop.
if(iSlot == -1) then return end

--Resolve the character and class name.
local iSlashNumber = 0
local sPathName = fnResolvePath()
local iPathLen = string.len(sPathName)
local sCharBuf = ""
for i = iPathLen, 1, -1 do
    local sLetter = string.sub(sPathName, i, i)
    if(sLetter == "/" or sLetter == "\\") then
        iSlashNumber = iSlashNumber + 1
    elseif(iSlashNumber == 1)then
        sCharBuf = sLetter .. sCharBuf
    end
end

--Write.
io.write(sCharBuf .. ". Stat Report:\n")
io.write(" Max HP:     " .. math.floor(giaHPArray[99])  .. "\n")
io.write(" Attack:     " .. math.floor(giaAtkArray[99]) .. "\n")
io.write(" Accuracy:   " .. math.floor(giaAccArray[99]) .. "\n")
io.write(" Evade:      " .. math.floor(giaEvdArray[99]) .. "\n")
io.write(" Initiative: " .. math.floor(giaIniArray[99]) .. "\n")

-- |[ ====================================== Data Upload ======================================= ]|
for i = 0, 99, 1 do
    ADebug_SetProperty("Set Profile Stats", iSlot, i, math.floor(giaHPArray[i]), math.floor(giaAtkArray[i]), math.floor(giaAccArray[i]), math.floor(giaEvdArray[i]), math.floor(giaIniArray[i]))
end
