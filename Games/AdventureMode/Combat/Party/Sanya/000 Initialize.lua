-- |[ ================================== Sanya Initialization ================================== ]|
--Creates and stores the named character in the party roster. If the character already exists, pushes
-- the character and modifies their properties.
local sCharacterName = "Sanya"

-- |[ ==================================== Unique Functions ==================================== ]|
-- |[ =================================== Character Handling =================================== ]|
-- |[Register or Push]|
--Register if character does not exist.
local bIsCreating = false
if(AdvCombat_GetProperty("Does Party Member Exist", sCharacterName) == false)  then
    bIsCreating = true
    AdvCombat_SetProperty("Register Party Member", sCharacterName)

--Push character.
else
    AdvCombat_SetProperty("Push Party Member", sCharacterName)
end

-- |[Call Script]|
AdvCombatEntity_SetProperty("Response Script", fnResolvePath() .. "100 Combat Script.lua")

-- |[Base Statistics]|
--Persistent Stats
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_HPMax,   121)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_MPMax,   100)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_MPRegen,  10)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_CPMax,    10)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_StunCap, 100)

--Free-Action Handlers
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_FreeActionMax, 3)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_FreeActionGen, 1)

--Combat Stats
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Initiative, 40)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Attack,     38)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Accuracy,   30)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Evade,      20)

--Base resistances.
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Protection,     3)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Slash,   gciNormalResist-2)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Strike,  gciNormalResist-2)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Pierce,  gciNormalResist-2)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Flame,   gciNormalResist+2)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Freeze,  gciNormalResist+5)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Shock,   gciNormalResist+0)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Crusade, gciNormalResist-1)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Obscure, gciNormalResist-1)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Bleed,   gciNormalResist-4)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Poison,  gciNormalResist-1)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Corrode, gciNormalResist-2)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Terrify, gciNormalResist-4)

--Threat Multiplier. Default 100 for all entities.
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_ThreatMultiplier, 100)

-- |[ ================================== Job Statistics Chart ================================== ]|
--All jobs store their statistics here, in a big ungainly chart!
if(gzaSanyaJobChart == nil) then

    -- |[Creation Function]|
    --Adds elements to the reference chart.
    local gzaRefChart = {}
    local function fnAdd(psName, piHltConstant, pfHltPercent, piIniConstant, pfIniPercent, piAtkConstant, pfAtkPercent, piAccConstant, pfAccPercent, piEvdConstant, pfEvdPercent, 
                         piProtection, piSlash, piStrike, piPierce, piFlame, piFreeze, piShock, piCrusade, piObscure, piBleed, piPoison, piCorrode, piTerrify)
        
        --Setup.
        local i = #gzaRefChart + 1
        gzaRefChart[i] = {}
        
        --System.
        gzaRefChart[i].sName = psName
        
        --Stats.
        gzaRefChart[i].iHlt_Constant = piHltConstant
        gzaRefChart[i].fHlt_Percent  = pfHltPercent
        gzaRefChart[i].iIni_Constant = piIniConstant
        gzaRefChart[i].fIni_Percent  = pfIniPercent
        gzaRefChart[i].iAtk_Constant = piAtkConstant
        gzaRefChart[i].fAtk_Percent  = pfAtkPercent
        gzaRefChart[i].iAcc_Constant = piAccConstant
        gzaRefChart[i].fAcc_Percent  = pfAccPercent
        gzaRefChart[i].iEvd_Constant = piEvdConstant
        gzaRefChart[i].fEvd_Percent  = pfEvdPercent
        
        --Resistances.
        gzaRefChart[i].iBonus_Resist_Protection = piProtection
        gzaRefChart[i].iBonus_Resist_Slash = piSlash
        gzaRefChart[i].iBonus_Resist_Pierce = piPierce
        gzaRefChart[i].iBonus_Resist_Strike = piStrike
        gzaRefChart[i].iBonus_Resist_Flame = piFlame
        gzaRefChart[i].iBonus_Resist_Freeze = piFreeze
        gzaRefChart[i].iBonus_Resist_Shock = piShock
        gzaRefChart[i].iBonus_Resist_Crusade = piCrusade
        gzaRefChart[i].iBonus_Resist_Obscure = piObscure
        gzaRefChart[i].iBonus_Resist_Bleed = piBleed
        gzaRefChart[i].iBonus_Resist_Poison = piPoison
        gzaRefChart[i].iBonus_Resist_Corrode = piCorrode
        gzaRefChart[i].iBonus_Resist_Terrify = piTerrify
    end

    --     Actual Name ||||| HltC | HltP | IniC | IniP | AtkC | AtkP | AccC | AccP | EvdC | EvdP  ||||| Pr | Sl | St | Pc | Fl | Fr | Sh | Cr | Ob | Bl | Ps | Cr | Tr
    fnAdd("Scofflaw",         -5, -0.10,    50,  0.10,    10,  0.06,     5,  0.10,    13,  0.08,      0,   0,   1,   1,  -6,   8,   4,   0,   0,  -5,  -2,   5,  -5)
    fnAdd("Ranger",          -10, -0.10,    20,  0.10,    15,  0.08,    15,  0.15,     3,  0.01,      0,   0,   1,   1,   4,   6,   4,   0,   0,  -5,  -2,   1,   5)
    fnAdd("Hunter",            9,  0.00,     0,  0.00,    10,  0.06,     5,  0.10,     3,  0.01,      0,   0,   0,   0,  -1,  -1,   1,   1,   1,  -5,  -2,   1,   0)
    fnAdd("Agontea",          40,  0.10,   -10,  0.00,    10,  0.06,     3,  0.06,    -6, -0.10,      2,   2,  -8,   2,   5,   5,   5,   0,   0,   5,   5, -10,   0)
    fnAdd("Skystriker",      -12, -0.03,    10,  0.05,    10,  0.06,     8,  0.03,     3,  0.10,      0,   0,   1,  -1,  -3,  -3,   3,   5,   5,  -3,   1,   2,  10)

    --Save.
    gzaSanyaJobChart = gzaRefChart
end

-- |[ ===================================== Equipment Slots ==================================== ]|
--Primary Weapon
AdvCombatEntity_SetProperty("Create Equipment Slot", "Weapon")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Weapon", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Weapon", false)

--Ammo. This is what affects the damage type and can be changed.
AdvCombatEntity_SetProperty("Create Equipment Slot", "Ammo")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Ammo", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty",           "Ammo", false)
AdvCombatEntity_SetProperty("Set Equipment Slot Used For Weapon Damage", "Ammo")

--Swappable Ammunition
AdvCombatEntity_SetProperty("Create Equipment Slot", "Ammo Backup")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Ammo Backup", false)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty",           "Ammo Backup", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Is Alt Weapon",          "Ammo Backup", true)

--Body Armor
AdvCombatEntity_SetProperty("Create Equipment Slot", "Body")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Body", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Body", true)

--Accessory Slots
AdvCombatEntity_SetProperty("Create Equipment Slot", "Accessory A")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Accessory A", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Accessory A", true)
AdvCombatEntity_SetProperty("Create Equipment Slot", "Accessory B")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Accessory B", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Accessory B", true)

--Equippable Items
AdvCombatEntity_SetProperty("Create Equipment Slot", "Item A")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Item A", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Item A", true)
AdvCombatEntity_SetProperty("Create Equipment Slot", "Item B")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Item B", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Item B", true)

-- |[ ====================================== Job Handling ====================================== ]|
--Create Sanya's common abilities, shared by many classes.
local sJobAbilityPath = gsRoot .. "Combat/Party/Sanya/Jobs/Common/Abilities/"
LM_ExecuteScript(sJobAbilityPath .. "Attack.lua",  gciAbility_Create)
LM_ExecuteScript(sJobAbilityPath .. "Prepare.lua", gciAbility_Create)
LM_ExecuteScript(sJobAbilityPath .. "Locked.lua",  gciAbility_Create)

--Job Change Abilities
LM_ExecuteScript(sJobAbilityPath .. "Jobchange_Scofflaw.lua",   gciAbility_Create)
LM_ExecuteScript(sJobAbilityPath .. "Jobchange_Ranger.lua",     gciAbility_Create)
LM_ExecuteScript(sJobAbilityPath .. "Jobchange_Hunter.lua",     gciAbility_Create)
LM_ExecuteScript(sJobAbilityPath .. "Jobchange_Agontea.lua",    gciAbility_Create)
LM_ExecuteScript(sJobAbilityPath .. "Jobchange_Skystriker.lua", gciAbility_Create)

--Ammo Change Ability
LM_ExecuteScript(gsRoot .. "Combat/Party/Common/Abilities/Swap Ammo.lua", gciAbility_Create)

--Call all of Sanya's job subscripts.
local sCharacterJobPath = gsRoot .. "Combat/Party/Sanya/Jobs/"
LM_ExecuteScript(sCharacterJobPath .. "Scofflaw/"   .. "000 Job Script.lua", gciJob_Create)
LM_ExecuteScript(sCharacterJobPath .. "Ranger/"     .. "000 Job Script.lua", gciJob_Create)
LM_ExecuteScript(sCharacterJobPath .. "Hunter/"     .. "000 Job Script.lua", gciJob_Create)
LM_ExecuteScript(sCharacterJobPath .. "Agontea/"    .. "000 Job Script.lua", gciJob_Create)
LM_ExecuteScript(sCharacterJobPath .. "Skystriker/" .. "000 Job Script.lua", gciJob_Create)
AdvCombatEntity_SetProperty("Rebuild Job Skills UI")

--Set Sanya to her default job.
AdvCombatEntity_SetProperty("Active Job", "Hunter")

--Once all abilities are built across all jobs, order jobs to associate with abilities. Any abilities
-- associated with a job can be purchased in the skills UI using that job's JP, and equipped from
-- that job's submenu. If an ability is not associated with any job, and is not auto-equipped by
-- any jobs when the job is assumed, the ability is not usable by the player!
LM_ExecuteScript(sCharacterJobPath .. "Scofflaw/"   .. "000 Job Script.lua", gciJob_JobAssembleSkillList)
LM_ExecuteScript(sCharacterJobPath .. "Ranger/"     .. "000 Job Script.lua", gciJob_JobAssembleSkillList)
LM_ExecuteScript(sCharacterJobPath .. "Hunter/"     .. "000 Job Script.lua", gciJob_JobAssembleSkillList)
LM_ExecuteScript(sCharacterJobPath .. "Agontea/"    .. "000 Job Script.lua", gciJob_JobAssembleSkillList)
LM_ExecuteScript(sCharacterJobPath .. "Skystriker/" .. "000 Job Script.lua", gciJob_JobAssembleSkillList)
    
-- |[ ==================================== Creation Callback =================================== ]|
--If this was a creation call of the character, fire its script callback.
if(bIsCreating) then
    local sResponsePath = AdvCombatEntity_GetProperty("Response Path")
    LM_ExecuteScript(sResponsePath, gciCombatEntity_Create)
end

-- |[ ======================================== Clean Up ======================================== ]|
--Pop the activity stack.
DL_PopActiveObject()
