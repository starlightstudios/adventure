-- |[ ====================================== User Manuals ====================================== ]|
-- |[Description]|
--Increases effect of usable combat items by 50%.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Tiffany.Support.UserManuals == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Support", "User Manuals", "$SkillName", gciJP_Cost_Passive, gbIsNotFreeAction, "Buff", "Passive", "Tiffany|UserManuals", "Passive")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Increases effect of equipped items by 50%%.\n\n\n\n\n\nPassive."
    zAbiStruct.sSimpleDescMarkdown  = "Download every user manual you can think of!\nIncreases combat item effectiveness by 50%%.\n\n\n\nPassive."
    
    -- |[Passive Effect]|
    zAbiStruct.bNeverAvailable   = true --Passive abilities have no usability requirements.
    zAbiStruct.sPassivePath = fnResolvePath() .. "../Effects/User Manuals.lua"
    zAbiStruct.bHasGUIEffects    = true

    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.Tiffany.Support.UserManuals = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Tiffany.Support.UserManuals

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
