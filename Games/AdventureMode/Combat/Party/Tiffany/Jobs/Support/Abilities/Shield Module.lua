-- |[ ===================================== Shield Module ====================================== ]|
-- |[Description]|
--Applies shields to one ally. Clears other shield effects.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Tiffany.Support.ShieldModule == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Support", "Shield Module", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Heal", "Active", "Tiffany|ShieldModule", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Applies [50 + [Atk] x 0.25] [Shld]. [Target].\n\n\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Apply a short-term shield to one ally.\nShield durability increases with [Atk](Power).\nClears all other shields.\n\n\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(20, giNoCPCost, giNoCooldown, "Target Allies Single", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Healing")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsShield(piShieldBase, pfShieldScaleFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsShield(50, 0.25)
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Tiffany|Ability:ShieldModule"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.Tiffany.Support.ShieldModule = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Tiffany.Support.ShieldModule

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
