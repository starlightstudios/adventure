-- |[ ======================================= Target Legs ======================================= ]|
-- |[Description]|
--Hit an enemy for weapon damage, reduces initiative. Chance to inflict stun.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Tiffany.Support.TargetLegs == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Support", "Target Legs", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "Tiffany|TargetLegs", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\n20%% chance for 100 [Stun].\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Aim at their legs, or whatever they use to\nmove.\n20%% chance to inflict 100 stun.\n\n\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(20, giNoCPCost, giNoCooldown, "Target Enemies Single", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Weapon")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_UseWeapon, 5, 1.25)
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Tiffany|Offense|General:Swing|Ability:TargetLegs"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Adding custom stun prediction
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "20%% chance: 100 [IMG0]"
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {"Root/Images/AdventureUI/StatisticIcons/Stun"}
    
    --Set prototype.
    gzPrototypes.Combat.Tiffany.Support.TargetLegs = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Tiffany.Support.TargetLegs

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--When executing, there is a 20% chance to deal stun damage.
if(iSwitchType == gciAbility_Execute) then
    
    -- |[ ========== Originator Statistics ========== ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID  = RO_GetID()
        fnModifyMP(gzRefAbility.iRequiredMP * -1)
        fnModifyCP(gzRefAbility.iCPGain)
    DL_PopActiveObject()
    
    -- |[ ========== Ability Package ========== ]|
    --Use the existing ability package, but toggle the stun damage on or off based on chance.
    local iRoll = LM_GetRandomNumber(1, 100)
    if(iRoll <= 20) then
        gzRefAbility.zExecutionAbiPackage.iBaseStunDamage = 100
    else
        gzRefAbility.zExecutionAbiPackage.iBaseStunDamage = 0
    end
    
    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        fnStandardExecution(iOriginatorID, i-1, gzRefAbility.zExecutionAbiPackage, nil)
    end
    
-- |[ ==================================== Standard Handler ==================================== ]|
--All other cases call the standard.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
