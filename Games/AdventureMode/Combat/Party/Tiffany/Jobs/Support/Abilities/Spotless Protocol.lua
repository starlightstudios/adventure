-- |[ =================================== Spotless Protocol ==================================== ]|
-- |[Description]|
--Removes all poisons and corrode effects, all allies.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Tiffany.Support.SpotlessProtocol == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Support", "Spotless Protocol", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "Tiffany|SpotlessProtocol", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Remove all [Psn] and [Crd] effects. [Target].\n\n\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Quickly clean your party's chassis of harmful\ncontaminants.\nCures [Psn](Poison) and [Crd](Corrode) effects.\n\n\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(10, giNoCPCost, giNoCooldown, "Target Allies All", 1)

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false  --Set to true to ignore all requirements.
    zAbiStruct.iRequiredMP = 10          --MP required to use the ability.
    zAbiStruct.iRequiredCP = 0           --CP required to use the ability.
    zAbiStruct.bRespectsCooldown = false --If this ability has a cooldown, decrements it.
    zAbiStruct.iCooldown = 0             --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
    zAbiStruct.bIsFreeAction = false     --Flags as a free action.
    zAbiStruct.iRequiredFreeActions = 0  --Free actions set this as 1, everything else as 0.
    zAbiStruct.bRespectActionCap = false --Characters with multiple free actions have a hard action cap. This flag checks that cap.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Allies All"

    --Effects
    zAbiStruct.iCPGain = 1
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Buff")
    zAbiStruct.zExecutionAbiPackage:fnSetAsBuff()
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Tiffany|Ability:SpotlessProtocol"
    
    -- |[Finalize]|
    --Running description markdown manually to avoid calling standard prediction creation
    zAbiStruct:fnRunDescriptionMarkdowns()
    
    --Custom prediction
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "Cure poison and corrode effects."
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {} 
    
    --Set prototype.
    gzPrototypes.Combat.Tiffany.Support.SpotlessProtocol = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Tiffany.Support.SpotlessProtocol

-- |[ ================================== Standardized Handler ================================== ]|
--Most executions use the standard.
if(iSwitchType ~= gciAbility_SpecialStart) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

-- |[ ==================== Combat: Special Remove Poison and Corrode Effects =================== ]|
--After the effect runs, remove DoTs and negative effects from anyone hit by the ability.
elseif(iSwitchType == gciAbility_SpecialStart) then

    --Argument handling.
    if(iArgumentsTotal < 3) then return end
    local iOriginatorID   = LM_GetScriptArgument(1, "N")
    local iTargetUniqueID = LM_GetScriptArgument(2, "N")
    
    --Setup.
    local iEffectCount = 0
    local iaEffectIDList = {}
    
    --Get a list of all effects referencing the target. Remove any with the matching tag.
    local iEffectsTotal = AdvCombat_SetProperty("Store Effects Referencing", iTargetUniqueID)
    for i = 0, iEffectsTotal-1, 1 do
        AdvCombat_SetProperty("Push Temp Effects", i)
        
            --Tag is present:
            local iCount =    AdvCombatEffect_GetProperty("Get Tag Count", "Poisoning DoT") + AdvCombatEffect_GetProperty("Get Tag Count", "Corroding DoT")
            iCount = iCount + AdvCombatEffect_GetProperty("Get Tag Count", "Poison Effect") + AdvCombatEffect_GetProperty("Get Tag Count", "Corrode Effect")
            if(iCount > 0) then
                iEffectCount = iEffectCount + 1
                iaEffectIDList[iEffectCount] = RO_GetID()
            end
        DL_PopActiveObject()
    end
    
    --Clean up.
    AdvCombat_SetProperty("Clear Temp Effects")
    
    --If there was at least one effect on the list to remove, add application packs for the removal.
    if(iEffectCount > 0) then
    
        local iAnimTicks = fnGetAnimationTiming("Healing")
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, giStoredTimer, "Play Sound|Combat\\|Heal")
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, giStoredTimer, "Create Animation|Healing|AttackAnim0")
        giStoredTimer = giStoredTimer + iAnimTicks
    
        AdvCombat_SetProperty("Register Application Pack", piOriginatorID, iTargetUniqueID, giStoredTimer, "Text|Cured!|Color:Green")
        for i = 1, iEffectCount, 1 do
            AdvCombat_SetProperty("Register Application Pack", piOriginatorID, iTargetUniqueID, giStoredTimer, "Remove Effect|" .. iaEffectIDList[i])
        end
        giStoredTimer = giStoredTimer + gciApplication_TextTicks
    end
end
