-- |[ =================================== Implosion Grenade ==================================== ]|
-- |[Description]|
--Hits all enemies for 75% base damage, increasing by 15% for each enemy.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Tiffany.Support.ImplosionGrenade == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Support", "Implosion Grenade", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "Tiffany|ImplosionGrenade", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\nCannot miss, cannot strike critically.\n+0.15[Atk] per target on field.\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Throw a nano-singularity bomb that sucks\nenemies into one another.\nDeals reduced [Stk](Striking) damage, but\nincreases for each hit enemy.\nAlways hits, cannot strike critically.\nCosts [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(25, giNoCPCost, giNoCooldown, "Target Enemies All", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Strike")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Striking, 5, 0.60)
    
    --Ensuring it always hits but never crits
    zAbiStruct.zExecutionAbiPackage:fnAlwaysHit()
    zAbiStruct.zExecutionAbiPackage.bNeverCrits = true
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Tiffany|Ability:ImplosionGrenade"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.Tiffany.Support.ImplosionGrenade = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Tiffany.Support.ImplosionGrenade

-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--When the player is selecting targets, a little box appears above the targets. This is the prediction
-- box. This script is called once for each target cluster created when this ability is selected, and
-- prediction boxes can be made for entities in that target cluster.
--The AdvCombatAbility will be the active object.
if(iSwitchType == gciAbility_BuildPredictionBox) then

    -- |[ ========================= Setup ======================== ]|
    --The attack power scales against how many enemies are present.
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    gzRefAbility.zPredictionPackage.zDamageModule.fDamageFactor = 0.60 + (0.15 * iClusterTargetsTotal)

    -- |[ ===================== Other Values ===================== ]|
    --Get originator ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
    DL_PopActiveObject()
    
    --Get information about the cluster. This is needed to give each box a unique ID.
    local iClusterID = AdvCombat_GetProperty("Prediction Get Cluster ID")
    
    -- |[ ==================== For Each Target =================== ]|
    for i = 0, iClusterTargetsTotal-1, 1 do
        local iTargetID = AdvCombat_GetProperty("Prediction Get Targets ID", i)
        fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, gzRefAbility.zPredictionPackage)
    end

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[ ================= Originator Statistics ================ ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        fnModifyCP(gzRefAbility.iCPGain)
        fnModifyMP(gzRefAbility.iRequiredMP * -1)
    DL_PopActiveObject()
    
    -- |[ ==================== Ability Package =================== ]|
    --Set the attack power to scale by number of targets. All other values are left as-is.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    gzRefAbility.zExecutionAbiPackage.fDamageFactor = 0.60 + (0.15 * iTargetsTotal)
    
    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them. Call the standard
    -- execution subroutine.
    for i = 1, iTargetsTotal, 1 do
        fnStandardExecution(iOriginatorID, i-1, gzRefAbility.zExecutionAbiPackage, nil)
    end

-- |[ ===================================== All Other Cases ==================================== ]|
--Call the standard handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end

--Clean.
gzRefAbility = nil
