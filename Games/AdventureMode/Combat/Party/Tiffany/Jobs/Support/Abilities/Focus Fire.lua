-- |[ ======================================= Focus Fire ======================================= ]|
-- |[Description]|
--Increase all allies attack power.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Tiffany.Support.FocusFire == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Support", "Focus Fire", "$SkillName", gciJP_Cost_Normal, gbIsFreeAction, "Buff", "Active", "Tiffany|FocusFire", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "+25%% [Atk] / 3 [Turns]. [Target].\n\n\n\n\nFree Action. 3 [Turns] cooldown.\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Order your team to focus on an enemy's weak\npoint.\nBoosts party attack power for 3 turns.\n3 turn cooldown.\n\nFree Action."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, giNoCPCost, 3, "Target Allies All", giNoCPGeneration)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Buff")
    zAbiStruct.zExecutionAbiPackage:fnSetAsBuff()

    -- |[Effect Package]|
  --zAbiStruct:fnAddBuff(psPrototypeName, $psApplyText, $psApplyColorString, $psApplyAnim, $psApplySound)
    zAbiStruct:fnAddBuff("Tiffany.Support.FocusFire", "Attack Up!", "Color:Purple")
    
    --Custom resist text
    zAbiStruct.zExecutionEffPackage.sResistText = "Failed!"
    
    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName

    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
        local saDescription = {}
        saDescription[1] = "I want that target down, on the double!"
        saDescription[2] = "Increases [Atk](Power)."
      --EffectList:fnCreateBuffPrototype(psName, psDisplayName, psIcon, piTurns, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreateBuffPrototype(sLocalPrototypeName, "Focus Fire", "Tiffany|FocusFire", 3, "Atk|Pct|0.25", saDescription, {{"Is Positive", 1}})
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Tiffany|Ability:FocusFire"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.Tiffany.Support.FocusFire = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Tiffany.Support.FocusFire

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
