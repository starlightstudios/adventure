-- |[ ===================================== Covering Fire ====================================== ]|
-- |[Description]|
--30% chance to attack an enemy before they attack, max of once per turn.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Tiffany.Support.CoveringFire == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Support", "Covering Fire", "$SkillName", gciJP_Cost_Passive, gbIsNotFreeAction, "Buff", "Passive", "Tiffany|CoveringFire", "Counterattack")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "30%% chance to counterattack an enemy before they act.\nActivates once per turn max.\n\n\n\n\nPassive."
    zAbiStruct.sSimpleDescMarkdown  = "Take a shot when the enemy is open.\n30%% chance to counterattack before an\nenemy action.\nMax once per turn.\n\nPassive."
    
    -- |[Passive Effect]|
    zAbiStruct.bNeverAvailable   = true --Passive abilities have no usability requirements.

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    --Execution is used when counterattacking.
    zAbiStruct:fnCreateAbiPack("Weapon")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_UseWeapon, 5, 1.00)
    
    --Special: Counterattacks cause a black flash to appear.
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    
    --Reduces timer procession.
    zAbiStruct.zExecutionAbiPackage.zaTimerFactors.fStaggerFactor      = 1.0
    zAbiStruct.zExecutionAbiPackage.zaTimerFactors.fBlackFlash         = 1.0
    zAbiStruct.zExecutionAbiPackage.zaTimerFactors.fVoiceCalloutPre    = 1.0
    zAbiStruct.zExecutionAbiPackage.zaTimerFactors.fAnimation          = 0.35
    zAbiStruct.zExecutionAbiPackage.zaTimerFactors.fVoiceCalloutPost   = 0.0
    zAbiStruct.zExecutionAbiPackage.zaTimerFactors.fText               = 1.0
    zAbiStruct.zExecutionAbiPackage.zaTimerFactors.fDamage             = 1.0
    zAbiStruct.zExecutionAbiPackage.zaTimerFactors.fVoiceCalloutDamage = 0.0
    zAbiStruct.zExecutionAbiPackage.zaTimerFactors.fHealing            = 0.0
    zAbiStruct.zExecutionAbiPackage.zaTimerFactors.fShields            = 0.0
    zAbiStruct.zExecutionAbiPackage.zaTimerFactors.fMPGeneration       = 0.0
    zAbiStruct.zExecutionAbiPackage.zaTimerFactors.fSelfHealing        = 0.0
    zAbiStruct.zExecutionAbiPackage.zaTimerFactors.fMPRestore          = 0.0
    zAbiStruct.zExecutionAbiPackage.zaTimerFactors.fStun               = 0.0
    zAbiStruct.zExecutionAbiPackage.zaTimerFactors.fEffect             = 0.0
    zAbiStruct.zExecutionAbiPackage.zaTimerFactors.fJobChange          = 0.0
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Tiffany|Ability:CoveringFire"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.Tiffany.Support.CoveringFire = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Tiffany.Support.CoveringFire

-- |[ ================================== Combat: Combat Begins ================================= ]|
--Ability needs to track whether it has activated each turn. Give it variables.
if(iSwitchType == gciAbility_BeginCombat) then
    local iUniqueID = RO_GetID()
    DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "/")
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iExecThisTurn", "N", 0.0)
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iTargetID", "N", 0.0)

-- |[ =================================== Combat: Turn Begins ================================== ]|
--Reset activity when a new turn begins.
elseif(iSwitchType == gciAbility_BeginTurn) then
    local iUniqueID = RO_GetID()
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iExecThisTurn", "N", 0.0)
    
-- |[ ============================= Combat: Paint Targets Response ============================= ]|
--Ability attempts to attack whoever is acting if hostile.
elseif(iSwitchType == gciAbility_PaintTargetsResponse) then
    local iUniqueID = RO_GetID()
    local iTargetID = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iTargetID", "N")
    AdvCombat_SetProperty("Create Target Cluster", "Precog Cluster", "Attacker")
    AdvCombat_SetProperty("Add Target To Cluster", "Precog Cluster", iTargetID)

-- |[ ================================== Combat: Can Execute =================================== ]|
--Ability needs to be sure the target is still alive.
elseif(iSwitchType == gciAbility_QueryCanRun) then

    -- |[Variables]|
    --Get variables.
    local iUniqueID = RO_GetID()
    local iOwnerID  = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iOwnerID", "N")
    local iTargetID = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iTargetID", "N")

    -- |[Originator Alive]|
    AdvCombat_SetProperty("Push Event Originator")
        local bCanAct = AdvCombatEntity_GetProperty("Can Act")
    DL_PopActiveObject()
    if(bCanAct == false) then return end
    
    -- |[Target Alive Check]|
    --The target has to be, y'know, alive. Otherwise, don't attack it.
    AdvCombat_SetProperty("Push Entity By ID", iTargetID)
        local iHealth = AdvCombatEntity_GetProperty("Health")
    DL_PopActiveObject()
    if(iHealth < 1) then return end
    
    -- |[All Checks Passed]|
    AdvCombat_SetProperty("Set Event Can Run", true) 

-- |[ ================================== Combat: Event Queued ================================== ]|
--When an enemy activates an ability, we need to enqueue ourselves ahead of them if they are an enemy.
-- That code is done here.
elseif(iSwitchType == gciAbility_EventQueued) then

    -- |[Duplication Check]|
    --Check if the ability already executed this turn:
    local iUniqueID = RO_GetID()
    local iExecThisTurn = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iExecThisTurn", "N")
    if(iExecThisTurn == 1.0) then
        return
    end
    --io.write("Executing for " .. iUniqueID .. "\n")
    
    -- |[Hostility Check]|
    --Get the ID of the owner of this ability.
    local iOwnerID = AdvCombatAbility_GetProperty("Owner ID")
    local iOwnerGroup = AdvCombat_GetProperty("Party Of ID", iOwnerID)
    
    --Get the originator, and check if it's a hostile entity.
    local iOriginatorID = AdvCombat_GetProperty("Query Originator ID")
    local iOriginatorGroup = AdvCombat_GetProperty("Party Of ID", iOriginatorID)
    
    --To be valid, the owner must be in a different party than the originator, and both must be in either
    -- the player's active party or the enemy's active party.
    if(iOwnerGroup == iOriginatorGroup) then
        return
    end
    if(iOwnerGroup ~= gciACPartyGroup_Party and iOwnerGroup ~= gciACPartyGroup_Enemy) then
        return
    end
    if(iOriginatorGroup ~= gciACPartyGroup_Party and iOriginatorGroup ~= gciACPartyGroup_Enemy) then
        return
    end
    
    -- |[Target Check]|
    --Note: This ability doesn't care if the enemy is attacking its owner or not. It only cares that the
    -- target is in the same party. If any one target is in the same party, it can be countered.
    local bIsInTargetCluster = false
    
    --Scan targets.
    local iTargetsTotal = AdvCombat_GetProperty("Query Targets Total")
    for i = 1, iTargetsTotal, 1 do
        
        --Get ID of target.
        local iTargetID = AdvCombat_GetProperty("Query Targets ID", i-1)
        
        --Check its party. If it matches the owner's party group, it's valid.
        local iPartyOfID = AdvCombat_GetProperty("Party Of ID", iTargetID)
        if(iPartyOfID == iOwnerGroup) then
            bIsInTargetCluster = true
            break
        end
    end
    
    if(bIsInTargetCluster == false) then
        return
    end
    
    -- |[Random Chance]|
    --Lastly, roll. This ability has a 30% chance to fire.
    local iRoll = LM_GetRandomNumber(1, 100)
    
    --Collect tags applying to the owner.
    AdvCombat_SetProperty("Push Entity By ID", iOwnerID)
        local iAdditive    = AdvCombatEntity_GetProperty("Tag Count", "Counterattack +")
        local iSubtractive = AdvCombatEntity_GetProperty("Tag Count", "Counterattack -")
    DL_PopActiveObject()
    
    --The additive tags subtract from the roll since lower is better.
    iRoll = iRoll - iAdditive + iSubtractive
    
    -- |[Execution]|
    --Set this to true to make the ability always activate for debug.
    if(true) then
        iRoll = 0
    end
    if(iRoll <= 30) then
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iExecThisTurn", "N", 1.0)
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iOwnerID", "N", iOwnerID)
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iTargetID", "N", iOriginatorID)
        AdvCombatAbility_SetProperty("Enqueue This Ability As Event", -1)
    end
    
-- |[ ===================================== Standard Cases ===================================== ]|
--All other cases use the standard.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
    
