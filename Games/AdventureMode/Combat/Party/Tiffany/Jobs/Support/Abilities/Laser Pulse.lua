-- |[ ====================================== Laser Pulse ======================================= ]|
-- |[Description]|
--Inflicts flame damage, gives a temporary accuracy buff.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Tiffany.Support.LaserPulse == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Support", "Laser Pulse", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "Tiffany|LaserPulse", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\nCannot miss.\n\n[FEffect] +35 [Acc] / 3 [Turns].\n\n\n[Costs] "
    zAbiStruct.sSimpleDescMarkdown  = "Reconfigure your weapon to fire a\nshort-duration laser pulse.\nDeals increased [Flm](Flaming) damage, cannot\nmiss.\nBoosts your [Acc](Accuracy) for 3 turns.\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(30, giNoCPCost, giNoCooldown, "Target Enemies Single", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Flames")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Flaming, 5, 1.30)
    
    --Ensuring it always hits
    zAbiStruct.zExecutionAbiPackage:fnAlwaysHit()
    
    -- |[Execution Self-Effect Package]|
  --zAbiStruct:fnAddSelfEffect(psPrototypeName, $psApplyText, $psApplyColorString, $psApplyAnim, $psApplySound)
    zAbiStruct:fnAddSelfEffect("Tiffany.Support.LaserPulse", "Accuracy Up!", "Color:White", "", "Combat\\|Impact_Buff")
    
    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionSelfPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
	local saDescription = {}
	saDescription[1] = "Focus with laser-intensity!"
	saDescription[2] = "Increases [Acc](Accuracy)."
      --EffectList:fnCreateBuffPrototype(psName, psDisplayName, psIcon, piTurns, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreateBuffPrototype(sLocalPrototypeName, "Laser Pulse", "Tiffany|LaserPulse", 3, "Acc|Flat|35", saDescription, {{"Is Positive", 1}})
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Tiffany|Ability:LaserPulse"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    -- |[Self Effect Prediction Package]|
    zAbiStruct.zSelfPredictionPack = fnCreatePredictionPack()
    zAbiStruct.zSelfPredictionPack.zaAdditionalLines[1] = {}
    zAbiStruct.zSelfPredictionPack.zaAdditionalLines[1].sString   = "+35 [IMG0] / 3 [IMG1]"
    zAbiStruct.zSelfPredictionPack.zaAdditionalLines[1].saSymbols = {"Root/Images/AdventureUI/StatisticIcons/Accuracy", "Root/Images/AdventureUI/DebuffIcons/Clock"}
    
    --Set prototype.
    gzPrototypes.Combat.Tiffany.Support.LaserPulse = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Tiffany.Support.LaserPulse

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
