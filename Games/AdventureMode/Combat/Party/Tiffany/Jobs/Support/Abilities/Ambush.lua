-- |[ ========================================= Ambush ========================================= ]|
-- |[Description]|
--Attack all enemies for 50% weapon damage at the start of combat.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Tiffany.Support.Ambush == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Support", "Ambush", "$SkillName", gciJP_Cost_Passive, gbIsNotFreeAction, "Buff", "Passive", "Tiffany|Ambush", "Ambush")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\nActivates for free at battle start.\n\n\nPassive."
    zAbiStruct.sSimpleDescMarkdown  = "Control the battlefield to control the battle.\nAt battle start, hit all enemies for half\nweapon damage.\n\n\nPassive."
    
    -- |[Passive Effect]|
    zAbiStruct.bNeverAvailable   = true --Passive abilities have no usability requirements.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies All"
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    --Execution is used when counterattacking.
    zAbiStruct:fnCreateAbiPack("Weapon")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_UseWeapon, 5, 0.50)
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Tiffany|Ability:Ambush"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.Tiffany.Support.Ambush = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Tiffany.Support.Ambush

-- |[ ============================ Combat: Character Action Begins ============================= ]|
--Passive abilities can never be used.
if(iSwitchType == gciAbility_BeginAction or iSwitchType == gciAbility_BeginFreeAction) then
    AdvCombatAbility_SetProperty("Usable", false)
    
-- |[ ============================= Combat: Paint Targets Response ============================= ]|
--Paint targets by the macro.
elseif(iSwitchType == gciAbility_PaintTargetsResponse) then
    AdvCombat_SetProperty("Run Target Macro", gzRefAbility.sTargetMacro, AdvCombatAbility_GetProperty("Owner ID"))
    
-- |[ =================================== Combat: Can Execute ================================== ]|
--Always runs since Tiffany cannot start the battle KO'd.
elseif(iSwitchType == gciAbility_QueryCanRun) then
    AdvCombat_SetProperty("Set Event Can Run", true)
    
-- |[ ================================== Combat: Combat Begins ================================= ]|
--When combat begins, this ability gets enqueued.
elseif(iSwitchType == gciAbility_BeginCombat) then
    AdvCombatAbility_SetProperty("Enqueue This Ability As Event", -1)

-- |[ ==================================== Standard Handlers =================================== ]|
--All other cases work as normal.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
