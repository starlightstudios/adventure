-- |[ ==================================== Rubber Slug Shot ==================================== ]|
-- |[Description]|
--Reduced striking damage, causes a stun.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Tiffany.Assault.RubberSlugShot == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Assault", "Rubber Slug Shot", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "Tiffany|RubberSlugShot", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\nCannot miss.\n\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Fires a special rubber-slug to stun a target.\nInflicts reduced striking damage.\nCannot miss.\n\n\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(20, giNoCPCost, giNoCooldown, "Target Enemies Single", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Strike")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Striking, 5, 0.40)
    
    --Adding stun damage
    zAbiStruct.zExecutionAbiPackage:fnAddStun(80)
    
    --Ensuring it always hits
    zAbiStruct.zExecutionAbiPackage:fnAlwaysHit()
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Tiffany|Offense|General:Swing|Ability:RubberSlugShot"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.Tiffany.Assault.RubberSlugShot = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Tiffany.Assault.RubberSlugShot

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
