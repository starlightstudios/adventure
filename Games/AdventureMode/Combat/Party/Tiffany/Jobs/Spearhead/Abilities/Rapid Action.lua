-- |[ ====================================== Rapid Action ====================================== ]|
-- |[Description]|
--Hits one enemy, gives Fast buff on self until next turn.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Tiffany.Spearhead.RapidAction == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Spearhead", "Rapid Action", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "Tiffany|RapidAction", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\n[FEffect] 'Fast' / 1 [Turns]. Self.\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Fire your weapon and work the action at\nlightning speed.\nGives [Fast] buff, giving you the first move\nnext turn.\n\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(10, giNoCPCost, giNoCooldown, "Target Enemies Single", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Weapon")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_UseWeapon, 5, 1.10)
    
    -- |[Execution Self-Effect Package]|
  --AbiPrototype:fnAddSelfEffect(psPrototypeName, $psApplyText, $psApplyColorString, $psApplyAnim, $psApplySound)
    zAbiStruct:fnAddSelfEffect("Tiffany.Spearhead.RapidAction", "Fast!")
    
    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionSelfPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
	local saDescription = {}
	saDescription[1] = "Ready to act first next round!"
	saDescription[2] = "Provides [Fast], causing first action next turn."
      --EffectList:fnCreateBuffPrototype(psName, psDisplayName, psIcon, piTurns, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreateBuffPrototype(sLocalPrototypeName, "Rapid Action", "Tiffany|RapidAction", 2, "", saDescription, {{"Is Positive", 1}, {"Fast", 1}})
	
        --Manually specify description.
        local iType, zPrototype, iSlot = EffectList:fnLocateEntry(sLocalPrototypeName)
        if(zPrototype ~= nil) then
            zPrototype.bDontRebuildDescription = true
            zPrototype.saStrings[1] = "[Buff]Rapid Action"
            zPrototype.saStrings[2] = "[UpN][Fast] /[Dur][Turns]"
            zPrototype.saStrings[3] = "Ready to act first next round!"
            zPrototype.saStrings[4] = ""
            zPrototype.saStrings[5] = ""
            zPrototype.saStrings[6] = ""
            zPrototype.saStrings[7] = ""
            zPrototype.saStrings[8] = "[Buff] [Fast] ([Dur][Turns])"
            zPrototype.sShortText = "[Buff] [Fast] ([Dur][Turns])"
        end
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Tiffany|Offense|General:Swing|Ability:RapidAction"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Add [Fast] status to prediction
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "[Fast] until next turn."
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
    
    --Set prototype.
    gzPrototypes.Combat.Tiffany.Spearhead.RapidAction = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Tiffany.Spearhead.RapidAction

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
