-- |[ ==================================== Cold Calculation ==================================== ]|
-- |[Description]|
--Chance to instakill all enemies based on enemy Max HP. Does normal weapon attack on failure.
-- Enemies with the tag "No Instakill" cannot be instakilled.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Tiffany.Spearhead.ColdCalculation == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Spearhead", "Cold Calculation", "$SkillName", gciJP_Cost_Advanced, gbIsNotFreeAction, "Direct", "Combo", "Tiffany|ColdCalculation", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\nCannot miss.\n\nChance of instant KO, increases as target HP decreases.\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Attack all enemies and attempt a total system\nshutdown.\nDeals normal damage, but has a chance of\ninstantly-KOing the target.\nChance increases as enemy HP decreases.\nAttack cannot miss. Costs [CPCost][CPIco](CP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, 4, giNoCooldown, "Target Enemies All", giNoCPGeneration)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Weapon")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_UseWeapon, 5, 1.00)
    
    --Ensuring it always hits
    zAbiStruct.zExecutionAbiPackage:fnAlwaysHit()
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Tiffany|Special|Ability:ColdCalculation"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.Tiffany.Spearhead.ColdCalculation = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Tiffany.Spearhead.ColdCalculation
    
-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--Set the prediction line to indicate self-damage.
if(iSwitchType == gciAbility_BuildPredictionBox) then

    -- |[ ===================== Other Values ===================== ]|
    --Get originator ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
    DL_PopActiveObject()
    
    --Get information about the cluster. This is needed to give each box a unique ID.
    local iClusterID = AdvCombat_GetProperty("Prediction Get Cluster ID")
    
    -- |[ ==================== For Each Target =================== ]|
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    for i = 0, iClusterTargetsTotal-1, 1 do
        
        --Compute chance of instant-kill.
        local iTargetID = AdvCombat_GetProperty("Prediction Get Targets ID", i)
        AdvCombat_SetProperty("Push Entity By ID", iTargetID)
        
            --KO-Resist tags.
            local iKOResistTags = AdvCombatEntity_GetProperty("Tag Count", "No Instakill")
            
            --Health percentage.
            local iHealthCur = AdvCombatEntity_GetProperty("Health")
            local iHealthMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
            local fHealthPct = iHealthCur / iHealthMax
        
        DL_PopActiveObject()
        
        --No instakill allowed:
        if(iKOResistTags > 0) then
            zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
            zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "Immune to Instant KO!"
            zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
        
        --Compute instant KO:
        else
        
            --Chance starts at 20% and goes to 100% at 1% HP. It can actually go down if the enemy has adrenaline over 100% HP!
            --However, the chance cannot drop below 10%.
            local iInstantKOChance = 21 + math.floor((80 * (1.0 - fHealthPct)))
            if(iInstantKOChance <  10) then iInstantKOChance =  10 end
            if(iInstantKOChance > 100) then iInstantKOChance = 100 end
            
            --Set lines.
            gzRefAbility.zPredictionPackage.zaAdditionalLines[1] = {}
            gzRefAbility.zPredictionPackage.zaAdditionalLines[1].sString   = "Instant KO: " .. iInstantKOChance .. "%% chance."
            gzRefAbility.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
        
        end
        
        --Create box.
        fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, gzRefAbility.zPredictionPackage)
    end

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Execute on targets, trigger instant KO.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[ ================ Originator Statistics ================= ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        fnModifyCP(gzRefAbility.iRequiredCP * -1)
    DL_PopActiveObject()
    
    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        
        --Get target statistics.
        AdvCombat_SetProperty("Push Target", i-1)
        
            --ID.
            local iTargetID = RO_GetID()
        
            --KO-Resist tags.
            local iKOResistTags = AdvCombatEntity_GetProperty("Tag Count", "No Instakill")
            
            --Health percentage.
            local iHealthCur = AdvCombatEntity_GetProperty("Health")
            local iHealthMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
            local fHealthPct = iHealthCur / iHealthMax
        
        DL_PopActiveObject()
        
        --No instakill allowed, execute normally:
        if(iKOResistTags > 0) then
            fnStandardExecution(iOriginatorID, i-1, gzRefAbility.zExecutionAbiPackage, nil)
        
        --Compute instant KO:
        else
        
            --Chance starts at 20% and goes to 100% at 1% HP. It can actually go down if the enemy has adrenaline over 100% HP!
            --However, the chance cannot drop below 10%.
            local iInstantKOChance = 21 + math.floor((80 * (1.0 - fHealthPct)))
            if(iInstantKOChance <  10) then iInstantKOChance =  10 end
            if(iInstantKOChance > 100) then iInstantKOChance = 100 end
            
            --Roll.
            local iRoll = LM_GetRandomNumber(1, 100)
            
            --Instant KO!
            if(iRoll < iInstantKOChance) then
                
                --Deal damage equal to the target's HP.
                local iTimer = 0
                iTimer = iTimer + gzRefAbility.zExecutionAbiPackage.fnAbilityAnimate(iTargetID, iTimer, gzRefAbility.zExecutionAbiPackage, false)
                AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetID, iTimer, "Text|Instant KO!")
                iTimer = iTimer + gciApplication_TextTicks
                iTimer = iTimer + gzRefAbility.zExecutionAbiPackage.fnDamageAnimate(iOriginatorID, iTargetID, iTimer, iHealthCur, nil)
                fnDefaultEndOfApplications(iTimer)
                
            --No KO, normal damage:
            else
                fnStandardExecution(iOriginatorID, i-1, gzRefAbility.zExecutionAbiPackage, nil)
            end
        end
    end

-- |[ ==================================== Standard Handler ==================================== ]|
--All other cases covered by the standard.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

end
