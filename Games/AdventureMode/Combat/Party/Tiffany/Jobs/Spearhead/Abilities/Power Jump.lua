-- |[ ======================================= Power Jump ======================================= ]|
-- |[Description]|
--Generates 30 MP for the target, costs 10% of Max HP.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Tiffany.Spearhead.PowerJump == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Spearhead", "Power Jump", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Buff", "Active", "Tiffany|PowerJump", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "+30 [MPIco]. [Target].\n\n\n\n\nCosts 10%% of user's Max [Hlt].\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Transfer energy to an ally.\nGenerate 30 [MPIco](MP).\nCosts 10%% of Max HP. Can be used on yourself.\n\n\n"
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, giNoCPCost, giNoCooldown, "Target Allies Single", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Buff")
    zAbiStruct.zExecutionAbiPackage:fnSetAsMPRestore(30)
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Tiffany|Ability:PowerJump"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.Tiffany.Spearhead.PowerJump = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Tiffany.Spearhead.PowerJump

-- |[ ============================= Combat: Character Action Begins ============================ ]|
--Make sure we have enough HP.
if(iSwitchType == gciAbility_BeginAction or iSwitchType == gciAbility_BeginFreeAction) then

    --Owner must have 11% or more HP available.
    AdvCombatAbility_SetProperty("Usable", false)
    if(AdvCombatAbility_GetProperty("Is Owner Acting") == true) then

        --Get owner data:
        local iOwnerID = AdvCombatAbility_GetProperty("Owner ID")
        AdvCombat_SetProperty("Push Entity By ID", iOwnerID)
            local iHealthCur = AdvCombatEntity_GetProperty("Health")
            local iHealthMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
            local fHealthPct = iHealthCur / iHealthMax
        DL_PopActiveObject()
        
        --Set usable:
        if(fHealthPct > 0.10) then
            AdvCombatAbility_SetProperty("Usable", true)
        end
    end
    
-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--Set the prediction line to indicate self-damage.
elseif(iSwitchType == gciAbility_BuildPredictionBox) then

    -- |[ ===================== Other Values ===================== ]|
    --Get originator ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
        local iHealthCur = AdvCombatEntity_GetProperty("Health")
        local iHealthMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
    DL_PopActiveObject()
    
    --Get information about the cluster. This is needed to give each box a unique ID.
    local iClusterID = AdvCombat_GetProperty("Prediction Get Cluster ID")
    
    --Compute self-damage.
    local iSelfDamage = math.floor(iHealthMax * 0.10)
    if(iSelfDamage >= iHealthCur) then iSelfDamage = iHealthCur - 1 end
    
    -- |[ ==================== For Each Target =================== ]|
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    for i = 0, iClusterTargetsTotal-1, 1 do
        
        --Check if the target is the originator.
        local iTargetID = AdvCombat_GetProperty("Prediction Get Targets ID", i)
        if(iTargetID == iOwnerID) then
    
            --Set line to add self-damage.
            gzRefAbility.zPredictionPackage.zaAdditionalLines[1] = {}
            gzRefAbility.zPredictionPackage.zaAdditionalLines[1].sString   = "Damage: " .. iSelfDamage .. "[IMG0]"
            gzRefAbility.zPredictionPackage.zaAdditionalLines[1].saSymbols = {"Root/Images/AdventureUI/StatisticIcons/Health"}
        
            --Create box.
            fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, gzRefAbility.zPredictionPackage)
        
        --Target is not originator, so create a second prediction box to show self-damage.
        else
    
            --Remove self-damage line.
            gzRefAbility.zPredictionPackage.zaAdditionalLines[1] = nil
        
            --Create box.
            fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, gzRefAbility.zPredictionPackage)
    
            --Create second box.
            local sBoxName = "Prd" .. 0 .. "x" .. iOwnerID .. iClusterID
            AdvCombat_SetProperty("Prediction Create Box",              sBoxName)
            AdvCombat_SetProperty("Prediction Set Host",                sBoxName, iOwnerID)
            AdvCombat_SetProperty("Prediction Set Offsets",             sBoxName, 0, -100)
            AdvCombat_SetProperty("Prediction Allocate Strings",        sBoxName, 1)
            AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, 0, "Damage: " .. iSelfDamage .. "[IMG0]")
            AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, 0, 1)
            AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, 0, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Health")
        end
    end

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Execute on targets, damage-self.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[ ================ Originator Statistics ================= ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        local iHealthCur = AdvCombatEntity_GetProperty("Health")
        local iHealthMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
        fnModifyMP(gzRefAbility.iRequiredMP * -1)
        fnModifyCP(gzRefAbility.iCPGain)
    DL_PopActiveObject()
    
    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        fnStandardExecution(iOriginatorID, i-1, gzRefAbility.zExecutionAbiPackage, nil)
    end
    
    -- |[ ====================== Self-Damage ===================== ]|
    --Compute damage. Cannot KO self!
    local iSelfDamage = math.floor(iHealthMax * 0.10)
    if(iSelfDamage >= iHealthCur) then iSelfDamage = iHealthCur - 1 end
    
    --Set lines if self-damage is over 0. If for some reason the HPMAX is 4 or less, no cost!
    if(iSelfDamage > 0) then
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 0, "Play Sound|" .. gzRefAbility.zExecutionAbiPackage.sAttackSound)
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 0, "Create Animation|Shock|AttackAnim0")
        iAnimTicks = fnGetAnimationTiming("Shock")
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, iAnimTicks, "Damage|" .. iSelfDamage)
    end

-- |[ ==================================== Standard Handler ==================================== ]|
--All other cases covered by the standard.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
