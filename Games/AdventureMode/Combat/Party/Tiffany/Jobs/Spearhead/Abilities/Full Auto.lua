-- |[ ======================================== Full Auto ======================================= ]|
-- |[Description]|
--Deals greatly increased weapon damage. Costs CP. If not enough CP is available, drains HP.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Tiffany.Spearhead.FullAuto == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Spearhead", "Full Auto", "$SkillName", gciJP_Cost_Advanced, gbIsNotFreeAction, "Direct", "Combo", "Tiffany|FullAuto", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\nCannot miss.\n\n\n\nIf insufficient CP, damage self 10%% Max [Hlt] per CP.\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Go full auto with your weapon, dealing extra\ndamage.\nIf not enough [CPIco](CP) is available, drain 10%% of\nyour Max HP per missing CP, and fire anyway!\nCannot miss.\nCosts [CPCost][CPIco](CP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, 4, 4, "Target Enemies Single", giNoCPGeneration)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Weapon")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_UseWeapon, 5, 3.00)
    
    --Ensuring it always hits
    zAbiStruct.zExecutionAbiPackage:fnAlwaysHit()

    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Tiffany|Offense|General:Swing|Ability:FullAuto"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.Tiffany.Spearhead.FullAuto = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Tiffany.Spearhead.FullAuto

-- |[ ============================= Combat: Character Action Begins ============================ ]|
--Called when the character that possesses this ability begins their turn. This is where the ability
-- should enable or disable itself based on the character's MP/State/etc.
if(iSwitchType == gciAbility_BeginAction or iSwitchType == gciAbility_BeginFreeAction) then

    --Owner must have 4CP or enough HP to actually burn the CP.
    AdvCombatAbility_SetProperty("Usable", false)
    if(AdvCombatAbility_GetProperty("Is Owner Acting") == true) then

        --Get owner data:
        local iOwnerID = AdvCombatAbility_GetProperty("Owner ID")
        AdvCombat_SetProperty("Push Entity By ID", iOwnerID)
            local iHealthCur = AdvCombatEntity_GetProperty("Health")
            local iHealthMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
            local fHealthPct = iHealthCur / iHealthMax
            local iComboPnts = AdvCombatEntity_GetProperty("Combo Points")
        DL_PopActiveObject()
        
        --Get how much HP is needed.
        local fHPNeeded = (4 - iComboPnts) * 0.10
        if(fHPNeeded < 0.0) then fHPNeeded = 0.0 end
        
        --Set usable:
        if(fHealthPct > fHPNeeded) then
            AdvCombatAbility_SetProperty("Usable", true)
        end
    end
    
-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--Set the prediction line to indicate self-damage.
elseif(iSwitchType == gciAbility_BuildPredictionBox) then

    -- |[ ===================== Other Values ===================== ]|
    --Get originator ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
        local iHealthCur = AdvCombatEntity_GetProperty("Health")
        local iHealthMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
        local iComboPnts = AdvCombatEntity_GetProperty("Combo Points")
    DL_PopActiveObject()
    
    --Get information about the cluster. This is needed to give each box a unique ID.
    local iClusterID = AdvCombat_GetProperty("Prediction Get Cluster ID")
        
    --Get how much HP is needed.
    local fHPNeeded = (4 - iComboPnts) * 0.10
    if(fHPNeeded < 0.0) then fHPNeeded = 0.0 end
    
    -- |[ ==================== For Each Target =================== ]|
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    for i = 0, iClusterTargetsTotal-1, 1 do
        local iTargetID = AdvCombat_GetProperty("Prediction Get Targets ID", i)
        fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, gzRefAbility.zPredictionPackage)
    end
    
    -- |[ =============== Originator Health Drain ================ ]|
    --Compute damage. Cannot KO self! Can be 0 if user has enough CP.
    local iSelfDamage = math.floor(iHealthMax * fHPNeeded)
    if(iSelfDamage >= iHealthCur) then iSelfDamage = iHealthCur - 1 end
    
    --Set lines if self-damage is over 0. If for some reason the HPMAX is 4 or less, no cost!
    if(iSelfDamage > 0) then
        local sBoxName = "Prd" .. 0 .. "x" .. iOwnerID .. iClusterID
        AdvCombat_SetProperty("Prediction Create Box",              sBoxName)
        AdvCombat_SetProperty("Prediction Set Host",                sBoxName, iOwnerID)
        AdvCombat_SetProperty("Prediction Set Offsets",             sBoxName, 0, -100)
        AdvCombat_SetProperty("Prediction Allocate Strings",        sBoxName, 1)
        AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, 0, "Damage: " .. iSelfDamage .. "[IMG0]")
        AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, 0, 1)
        AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, 0, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Health")
    end

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Execute on targets, damage-self.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[ ================ Originator Statistics ================= ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        local iHealthCur = AdvCombatEntity_GetProperty("Health")
        local iHealthMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
        local iComboPnts = AdvCombatEntity_GetProperty("Combo Points")
        
        --Subtract combo points as needed.
        if(iComboPnts <= 4) then
            fnModifyCP(iComboPnts * -1)
        else
            fnModifyCP(-4)
        end
    DL_PopActiveObject()
        
    --Get how much HP is needed.
    local fHPNeeded = (4 - iComboPnts) * 0.10
    if(fHPNeeded < 0.0) then fHPNeeded = 0.0 end
    
    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        fnStandardExecution(iOriginatorID, i-1, gzRefAbility.zExecutionAbiPackage, nil)
    end
    
    -- |[ ====================== Self-Damage ===================== ]|
    --Compute damage. Cannot KO self!
    local iSelfDamage = math.floor(iHealthMax * fHPNeeded)
    if(iSelfDamage >= iHealthCur) then iSelfDamage = iHealthCur - 1 end
    
    --Set lines if self-damage is over 0. If for some reason the HPMAX is 4 or less, no cost!
    if(iSelfDamage > 0) then
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 0, "Play Sound|" .. gzRefAbility.zExecutionAbiPackage.sAttackSound)
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 0, "Create Animation|Shock|AttackAnim0")
        iAnimTicks = fnGetAnimationTiming("Shock")
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, iAnimTicks, "Damage|" .. iSelfDamage)
    end

-- |[ ==================================== Standard Handler ==================================== ]|
--All other cases covered by the standard.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
