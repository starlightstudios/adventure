-- |[ ==================================== Conductive Spray ==================================== ]|
-- |[Description]|
--Reduces enemy shock resist by 10 for 3 turns.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Tiffany.Spearhead.ConductiveSpray == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Spearhead", "Conductive Spray", "$SkillName", gciJP_Cost_Normal, gbIsFreeAction, "Direct", "Active", "Tiffany|ConductiveSpray", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[HEffect][Str7][Slsh] -10[Shk] Resist, 3 [Turns]. [Target].\nCannot miss.\n\n\n\nFree Action.\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Spritz the enemy with a gel that conducts\nelectricity. Reduces one enemy's [Shk](Shock)\nresist. Cannot miss.\nMakes the enemy vulnerable to Christine's\nelectrospear. Free Action.\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(10, giNoCPCost, 1, "Target Enemies Single", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Corrode")
    zAbiStruct.zExecutionAbiPackage:fnSetAsDebuff()
    
    --Re-enabling crits
    zAbiStruct.zExecutionAbiPackage.bNeverCrits = false
    
    --Ensuring it always hits
    zAbiStruct.zExecutionAbiPackage:fnAlwaysHit()
    
    -- |[Execution Effect Package]|
  --zAbiStruct:fnAddEffect(piStrNormal, piStrCrit, piApplyType, psApplyText, psCritApplyText, psApplyColorString, psaApplyBonus, psaApplyMalus, psaSeverityBonus, psaSeverityMalus)
    zAbiStruct:fnAddEffect(7, 10, gciDamageType_Corroding, "Coated!", "Badly Coated!", "Color:Purple")
    
    -- |[Use of Effect Prototype]|
    zAbiStruct.zExecutionEffPackage.sPrototypeName = "Tiffany.Spearhead.ConductiveSpray"
    
    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
        local saDescription = {}
        saDescription[1] = "Coated in conductive gel!"
        saDescription[2] = "Reduces [Shk](Shock) resistance."
      --EffectList:fnCreateStatPrototype(pzEffectpackage, psName, pbIsBuff, psDisplayName, psIcon, piTurns, piApplyStrength, psStatString, psaDescription, $pzaTagList, $piCritTurns, $piCritApplyStrength)
        EffectList:fnCreateStatPrototype(zAbiStruct.zExecutionEffPackage, sLocalPrototypeName, gbIsDebuff, "Conductive Spray", "Tiffany|ConductiveSpray", 3, 7, "ResShk|Flat|-10", saDescription, {{"Is Negative", 1}}, 5, 10)
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Tiffany|Offense|General:Swing|Ability:ConductiveSpray"
    
    -- |[Finalize]|
    --Running description markdown manually to avoid calling standard prediction creation
    zAbiStruct:fnRunDescriptionMarkdowns()
    
    --Manually creating an Effect module
    zAbiStruct.zPredictionPackage.zaEffectModules[1] = fnCreateEffectModule()
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iEffectType = gciDamageType_Corroding
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iEffectStrength = 7
    zAbiStruct.zPredictionPackage.zaEffectModules[1].sResultScript = "-10[IMG1]Resist / 3[IMG2]"
    zAbiStruct.zPredictionPackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/DamageTypeIcons/Shocking", "Root/Images/AdventureUI/DebuffIcons/Clock"}
    
    --Set prototype.
    gzPrototypes.Combat.Tiffany.Spearhead.ConductiveSpray = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Tiffany.Spearhead.ConductiveSpray

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
