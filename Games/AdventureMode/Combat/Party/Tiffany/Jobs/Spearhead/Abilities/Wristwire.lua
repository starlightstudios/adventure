-- |[ ======================================== Wristwire ======================================= ]|
-- |[Description]|
--Deals shock damage, damage scales with user's HP. Costs 20% of max HP.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Tiffany.Spearhead.Wristwire == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Spearhead", "Wristwire", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "Tiffany|Wristwire", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Inflict [1.50x to 2.50x [Atk]] as [DamageType]. [Target].\n[BaseHit].\nDamage scales by user's current HP.\n\n\n\nCosts 20%% of user's Max [Hlt]."
    zAbiStruct.sSimpleDescMarkdown  = "Shock the enemy with your own power supply.\nDeals extra [Shk](Shock) damage, scales with\nuser HP.\n\n\nCosts 20%% of user max HP."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, giNoCPCost, giNoCooldown, "Target Enemies Single", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Shock")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Shocking, 5, 1.50)
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Tiffany|Offense|General:Swing|Ability:Wristwire"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.Tiffany.Spearhead.Wristwire = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Tiffany.Spearhead.Wristwire

-- |[ ============================= Combat: Character Action Begins ============================ ]|
--Called when the character that possesses this ability begins their turn. This is where the ability
-- should enable or disable itself based on the character's MP/State/etc.
if(iSwitchType == gciAbility_BeginAction or iSwitchType == gciAbility_BeginFreeAction) then

    --Owner must have 21% or more HP available.
    AdvCombatAbility_SetProperty("Usable", false)
    if(AdvCombatAbility_GetProperty("Is Owner Acting") == true) then

        --Get owner data:
        local iOwnerID = AdvCombatAbility_GetProperty("Owner ID")
        AdvCombat_SetProperty("Push Entity By ID", iOwnerID)
            local iHealthCur = AdvCombatEntity_GetProperty("Health")
            local iHealthMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
            local fHealthPct = iHealthCur / iHealthMax
        DL_PopActiveObject()
        
        --Set usable:
        if(fHealthPct > 0.20) then
            AdvCombatAbility_SetProperty("Usable", true)
        end
    end
    
-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--Set the prediction line to indicate self-damage.
elseif(iSwitchType == gciAbility_BuildPredictionBox) then

    -- |[ ===================== Other Values ===================== ]|
    --Get originator ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
        local iHealthCur = AdvCombatEntity_GetProperty("Health")
        local iHealthMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
    DL_PopActiveObject()
    
    --Get information about the cluster. This is needed to give each box a unique ID.
    local iClusterID = AdvCombat_GetProperty("Prediction Get Cluster ID")
    
    -- |[ ==================== Power Scaling ===================== ]|
    --The damage scales linearly down to 20% of max HP.
    local fHealthPct = iHealthCur / iHealthMax
    gzRefAbility.zPredictionPackage.zDamageModule.fDamageFactor = 1.50 + (((fHealthPct-0.20)/0.80) * 1.00)
    
    -- |[ ==================== For Each Target =================== ]|
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    for i = 0, iClusterTargetsTotal-1, 1 do
        local iTargetID = AdvCombat_GetProperty("Prediction Get Targets ID", i)
        fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, gzRefAbility.zPredictionPackage)
    end
    
    -- |[ =============== Originator Health Drain ================ ]|
    --Compute damage. Cannot KO self!
    local iSelfDamage = math.floor(iHealthMax * 0.20)
    if(iSelfDamage >= iHealthCur) then iSelfDamage = iHealthCur - 1 end
    
    --Set lines if self-damage is over 0. If for some reason the HPMAX is 4 or less, no cost!
    if(iSelfDamage > 0) then
        local sBoxName = "Prd" .. 0 .. "x" .. iOwnerID .. iClusterID
        AdvCombat_SetProperty("Prediction Create Box",              sBoxName)
        AdvCombat_SetProperty("Prediction Set Host",                sBoxName, iOwnerID)
        AdvCombat_SetProperty("Prediction Set Offsets",             sBoxName, 0, -100)
        AdvCombat_SetProperty("Prediction Allocate Strings",        sBoxName, 1)
        AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, 0, "Damage: " .. iSelfDamage .. "[IMG0]")
        AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, 0, 1)
        AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, 0, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Health")
    end

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Execute on targets, damage-self.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[ ================ Originator Statistics ================= ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        local iHealthCur = AdvCombatEntity_GetProperty("Health")
        local iHealthMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
        fnModifyMP(gzRefAbility.iRequiredMP * -1)
        fnModifyCP(gzRefAbility.iCPGain)
    DL_PopActiveObject()
    
    -- |[ ==================== Power Scaling ===================== ]|
    --The damage scales linearly down to 20% of max HP.
    local fHealthPct = iHealthCur / iHealthMax
    gzRefAbility.zExecutionAbiPackage.fDamageFactor = 1.50 + (((fHealthPct-0.20)/0.80) * 1.00)
    
    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        fnStandardExecution(iOriginatorID, i-1, gzRefAbility.zExecutionAbiPackage, nil)
    end
    
    -- |[ ====================== Self-Damage ===================== ]|
    --Compute damage. Cannot KO self!
    local iSelfDamage = math.floor(iHealthMax * 0.20)
    if(iSelfDamage >= iHealthCur) then iSelfDamage = iHealthCur - 1 end
    
    --Set lines if self-damage is over 0. If for some reason the HPMAX is 4 or less, no cost!
    if(iSelfDamage > 0) then
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 0, "Play Sound|" .. gzRefAbility.zExecutionAbiPackage.sAttackSound)
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 0, "Create Animation|Shock|AttackAnim0")
        iAnimTicks = fnGetAnimationTiming("Shock")
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, iAnimTicks, "Damage|" .. iSelfDamage)
    end

-- |[ ==================================== Standard Handler ==================================== ]|
--All other cases covered by the standard.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

end
