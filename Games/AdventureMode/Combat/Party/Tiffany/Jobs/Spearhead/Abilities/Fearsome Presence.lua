-- |[ =================================== Fearsome Presence ==================================== ]|
-- |[Description]|
--Terrify damage, -Atk, -Ini, all enemies. Causes extra threat.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Tiffany.Spearhead.FearsomePresence == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Spearhead", "Fearsome Presence", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "Tiffany|FearsomePresence", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\n[HEffect][Str7][Tfy] -25%%[Atk], -35[Ini], 3 [Turns].\n+100%% [Threat].\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Your reputation and scowl precede you. Hits all\nenemies.\nDeals [Tfy](Terrify) damage, reduces enemy\n[Atk](Power) and [Ini](Initiative).\nGenerates extra threat.\nCosts [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(30, giNoCPCost, giNoCooldown, "Target Enemies All", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Terrify")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Terrifying, 5, 0.60)
    
    --Manual override for threat multiplier.
    zAbiStruct.fThreatMultiplier = 2.0
    
    -- |[Execution Effect Package]|
  --zAbiStruct:fnAddEffect(piStrNormal, piStrCrit, piApplyType, psApplyText, psCritApplyText, psApplyColorString, psaApplyBonus, psaApplyMalus, psaSeverityBonus, psaSeverityMalus)
    zAbiStruct:fnAddEffect(7, 10, gciDamageType_Terrifying, "Terrified!", "Badly Terrified!", "Color:Purple")
    
    -- |[Use of Effect Prototype]|
    zAbiStruct.zExecutionEffPackage.sPrototypeName = "Tiffany.Spearhead.FearsomePresence"
    
    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
	local saDescription = {}
	saDescription[1] = "Terrified of crossing 55!"
	saDescription[2] = "Reduces [Atk](Power) and [Ini](Initiative)."
      --EffectList:fnCreateStatPrototype(pzEffectpackage, psName, pbIsBuff, psDisplayName, psIcon, piTurns, piApplyStrength, psStatString, psaDescription, $pzaTagList, $piCritTurns, $piCritApplyStrength)
        EffectList:fnCreateStatPrototype(zAbiStruct.zExecutionEffPackage, sLocalPrototypeName, gbIsDebuff, "Fearsome Presence", "Tiffany|FearsomePresence", 3, 7, "Atk|Pct|-0.25 Ini|Flat|-35", saDescription, {{"Is Negative", 1}}, 5, 10)
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Tiffany|Ability:FearsomePresence"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.Tiffany.Spearhead.FearsomePresence = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Tiffany.Spearhead.FearsomePresence

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Extra threat needs to be applied for this ability.
if(iSwitchType == gciAbility_Execute) then

    -- |[ ========== Originator Statistics ========== ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        fnModifyCP(1)
        fnModifyMP(-30)
    DL_PopActiveObject()
    
    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        
        --Execute.
        fnStandardExecution(iOriginatorID, i-1, gzRefAbility.zExecutionAbiPackage, gzRefAbility.zExecutionEffPackage)
        
        --Apply a bonus 100% of attack power as threat.
        AdvCombat_SetProperty("Push Target", i-1)
            local iTargetID = RO_GetID()
        DL_PopActiveObject()
        AdvCombat_SetProperty("Push Event Originator")
            local iOriginatorAttackPower = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
            local fThreatMultiplier = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_ThreatMultiplier)
        DL_PopActiveObject()
        
        --Compute.
        local iThreatToApply = math.floor(iOriginatorAttackPower * fThreatMultiplier / 100.0)
        if(iThreatToApply > 0) then
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetID, 0, "AI_APPLICATION|Threat|" .. iThreatToApply)
        end
    end
    
-- |[ ==================================== Standard Handler ==================================== ]|
--Run standard handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
