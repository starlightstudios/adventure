-- |[ ====================================== Into The Fray ===================================== ]|
-- |[Description]|
--Hits all enemies for striking damage, dealing more as enemy count goes up, caps at 5 enemies.
-- Inflicts -Protection debuff on user. Costs 15% max HP.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Tiffany.Spearhead.IntoTheFray == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Spearhead", "Into The Fray", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "Tiffany|IntoTheFray", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit]. +0.20x[Atk] per enemy on field.\n\n[FEffect] -10[Prt] / 2[Turns], Self.\n\nCosts 15%% of user's Max [Hlt].\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Rush into enemy lines and attack in melee!\nDeals more [Stk](Strike) damage as number of\nenemies increases.\nCosts 15%% of Max HP, reduces your\n[Prt](Protection) for 2 turns.\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(20, giNoCPCost, giNoCooldown, "Target Enemies All", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Strike")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Striking, 5, 1.00)
    
    -- |[Execution Self-Effect Package]|
  --zAbiStruct:fnAddSelfEffect(psPrototypeName, $psApplyText, $psApplyColorString, $psApplyAnim, $psApplySound)
    zAbiStruct:fnAddSelfEffect("Tiffany.Spearhead.IntoTheFray", "Vulnerable!", "Color:White", "", "Combat\\|Impact_Debuff")
    
    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionSelfPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
	local saDescription = {}
	saDescription[1] = "Left exposed from attacking in close-combat."
	saDescription[2] = "Reduces [Prt](Protection)."
      --EffectList:fnCreateBuffPrototype(psName, psDisplayName, psIcon, piTurns, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreateBuffPrototype(sLocalPrototypeName, "Into The Fray", "Tiffany|IntoTheFray", 3, "Prt|Flat|-10", saDescription, {{"Is Negative", 1}})
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Tiffany|Offense|General:Swing|Ability:IntoTheFray"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.Tiffany.Spearhead.IntoTheFray = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Tiffany.Spearhead.IntoTheFray

-- |[ ============================= Combat: Character Action Begins ============================ ]|
--Called when the character that possesses this ability begins their turn. This is where the ability
-- should enable or disable itself based on the character's MP/State/etc.
if(iSwitchType == gciAbility_BeginAction or iSwitchType == gciAbility_BeginFreeAction) then

    --Owner must have 16% or more HP available.
    AdvCombatAbility_SetProperty("Usable", false)
    if(AdvCombatAbility_GetProperty("Is Owner Acting") == true) then

        --Get owner data:
        local iOwnerID = AdvCombatAbility_GetProperty("Owner ID")
        AdvCombat_SetProperty("Push Entity By ID", iOwnerID)
            local iHealthCur = AdvCombatEntity_GetProperty("Health")
            local iHealthMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
            local fHealthPct = iHealthCur / iHealthMax
        DL_PopActiveObject()
        
        --Set usable:
        if(fHealthPct > 0.15) then
            AdvCombatAbility_SetProperty("Usable", true)
        end
    end
    
-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--Set the prediction line to indicate self-damage.
elseif(iSwitchType == gciAbility_BuildPredictionBox) then

    -- |[ ===================== Other Values ===================== ]|
    --Get originator ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
        local iHealthCur = AdvCombatEntity_GetProperty("Health")
        local iHealthMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
    DL_PopActiveObject()
    
    --Get information about the cluster. This is needed to give each box a unique ID.
    local iClusterID = AdvCombat_GetProperty("Prediction Get Cluster ID")
    
    -- |[ ==================== For Each Target =================== ]|
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    for i = 0, iClusterTargetsTotal-1, 1 do
        
        --Scale damage by number of targets.
        local iTargetNum = iClusterTargetsTotal
        if(iTargetNum > 5) then iTargetNum = 5 end
        gzRefAbility.zPredictionPackage.zDamageModule.fDamageFactor = 0.80 + (iTargetNum * 0.20)
        
        --Create.
        local iTargetID = AdvCombat_GetProperty("Prediction Get Targets ID", i)
        fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, gzRefAbility.zPredictionPackage)
    end
    
    -- |[ =============== Originator Health Drain ================ ]|
    --Compute damage. Cannot KO self!
    local iSelfDamage = math.floor(iHealthMax * 0.15)
    if(iSelfDamage >= iHealthCur) then iSelfDamage = iHealthCur - 1 end
    
    --Set lines if self-damage is over 0. If for some reason the HPMAX is 4 or less, no cost!
    if(iSelfDamage > 0) then
        local sBoxName = "Prd" .. 0 .. "x" .. iOwnerID .. iClusterID
        AdvCombat_SetProperty("Prediction Create Box",              sBoxName)
        AdvCombat_SetProperty("Prediction Set Host",                sBoxName, iOwnerID)
        AdvCombat_SetProperty("Prediction Set Offsets",             sBoxName, 0, -100)
        AdvCombat_SetProperty("Prediction Allocate Strings",        sBoxName, 2)
        AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, 0, "Damage: " .. iSelfDamage .. "[IMG0]")
        AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, 0, 1)
        AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, 0, 0, gcfAbilityImgOffsetPredY+1, "Root/Images/AdventureUI/StatisticIcons/Health")
        AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, 1, "-10 [IMG0] / 2 [IMG1]")
        AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, 1, 2)
        AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, 1, 0, gcfAbilityImgOffsetPredY+1, "Root/Images/AdventureUI/StatisticIcons/Protection")
        AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, 1, 1, gcfAbilityImgOffsetPredY+1, "Root/Images/AdventureUI/StatisticIcons/Turns")
    
    --Self debuff.
    else
        local sBoxName = "Prd" .. 0 .. "x" .. iOwnerID .. iClusterID
        AdvCombat_SetProperty("Prediction Create Box",              sBoxName)
        AdvCombat_SetProperty("Prediction Set Host",                sBoxName, iOwnerID)
        AdvCombat_SetProperty("Prediction Set Offsets",             sBoxName, 0, -100)
        AdvCombat_SetProperty("Prediction Allocate Strings",        sBoxName, 1)
        AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, 0, "-10 [IMG0] / 2 [IMG1]")
        AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, 0, 2)
        AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, 0, 0, gcfAbilityImgOffsetPredY+1, "Root/Images/AdventureUI/StatisticIcons/Protection")
        AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, 0, 1, gcfAbilityImgOffsetPredY+1, "Root/Images/AdventureUI/StatisticIcons/Turns")
    end

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Execute on targets, damage-self.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[ ================ Originator Statistics ================= ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        local iHealthCur = AdvCombatEntity_GetProperty("Health")
        local iHealthMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
        fnModifyMP(gzRefAbility.iRequiredMP * -1)
        fnModifyCP(gzRefAbility.iCPGain)
    DL_PopActiveObject()
    
    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        
        --Scale damage by number of targets.
        local iTargetNum = iTargetsTotal
        if(iTargetNum > 5) then iTargetNum = 5 end
        gzRefAbility.zExecutionAbiPackage.fDamageFactor = 0.80 + (iTargetNum * 0.20)
        
        fnStandardExecution(iOriginatorID, i-1, gzRefAbility.zExecutionAbiPackage, nil)
    end
    
    -- |[ ===================== Self-Effects ===================== ]|
    if(gzRefAbility.zExecutionSelfPackage ~= nil) then
	gzRefAbility.zExecutionSelfPackage.fnHandler(iOriginatorID, iOriginatorID, 0, gzRefAbility.zExecutionSelfPackage, true, false)
    end
    
    -- |[ ====================== Self-Damage ===================== ]|
    --Compute damage. Cannot KO self!
    local iSelfDamage = math.floor(iHealthMax * 0.15)
    if(iSelfDamage >= iHealthCur) then iSelfDamage = iHealthCur - 1 end
    
    --Set lines if self-damage is over 0. If for some reason the HPMAX is 4 or less, no cost!
    if(iSelfDamage > 0) then
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 0, "Effect|" .. gzRefAbility.zExecutionSelfPackage.sEffectPath)
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 0, "Play Sound|" .. gzRefAbility.zExecutionAbiPackage.sAttackSound)
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 0, "Create Animation|Shock|AttackAnim0")
        iAnimTicks = fnGetAnimationTiming("Shock")
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, iAnimTicks, "Damage|" .. iSelfDamage)
    end

-- |[ ==================================== Standard Handler ==================================== ]|
--All other cases covered by the standard.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end

