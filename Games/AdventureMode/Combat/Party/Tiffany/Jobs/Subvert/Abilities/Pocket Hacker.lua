-- |[ ===================================== Pocket Hacker ====================================== ]|
-- |[Description]|
--Start combat with 1 CP. 10% chance for a free CP every turn.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Tiffany.Subvert.PocketHacker == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Subvert", "Pocket Hacker", "$SkillName", gciJP_Cost_Passive, gbIsNotFreeAction, "Buff", "Passive", "Tiffany|PocketHacker", "Passive")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Start combat with +1 [CPIco].\n10%% chance to generate 1 [CPIco] each turn.\n\n\n\n\nPassive."
    zAbiStruct.sSimpleDescMarkdown  = "Integrate a frequency cycler into your PDU.\nStart combat with +1 [CPIco](CP) and 10%% chance\nto get a free CP each turn.\n\n\nPassive."
    
    -- |[Passive Effect]|
    zAbiStruct.bNeverAvailable   = true --Passive abilities have no usability requirements.
    zAbiStruct.sPassivePath = fnResolvePath() .. "../Effects/Pocket Hacker.lua"
    zAbiStruct.bHasGUIEffects    = true

    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.Tiffany.Subvert.PocketHacker = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Tiffany.Subvert.PocketHacker

LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
