-- |[ ======================================= Comm Jammer ====================================== ]|
-- |[Description]|
--Attempts to confuse one enemy.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Tiffany.Subvert.CommJammer == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Subvert", "Comm Jammer", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "Tiffany|CommJammer", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\n[HEffect][Str6][Shk] [Confuse] for 3 turns.\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Interrupt friend/foe signals, causing an enemy\nto attack its allies.\n[Confuse] can be broken by damage.\n\n\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(20, giNoCPCost, giNoCooldown, "Target Enemies Single", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Shock")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_UseWeapon, 5, 0.80)
    
    -- |[Execution Effect Package]|
  --zAbiStruct:fnAddEffect(piStrNormal, piStrCrit, piApplyType, psApplyText, psCritApplyText, psApplyColorString, psaApplyBonus, psaApplyMalus, psaSeverityBonus, psaSeverityMalus)
    zAbiStruct:fnAddEffect(6, 9, gciDamageType_Shocking, "Confused!", "Badly Confused!", "Color:Purple")
    
    -- |[Use of Effect Prototype]|
    zAbiStruct.zExecutionEffPackage.sPrototypeName = "Tiffany.Subvert.CommJammer"
    
    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
	local saDescription = {}
	saDescription[1] = "Jam the enemy friend/foe comm frequency."
	saDescription[2] = "Causes confuse, causing enemies to attack other"
	saDescription[3] = "enemies as often as your party."
      --EffectList:fnCreateStatPrototype(pzEffectpackage, psName, pbIsBuff, psDisplayName, psIcon, piTurns, piApplyStrength, psStatString, psaDescription, $pzaTagList, $piCritTurns, $piCritApplyStrength)
        EffectList:fnCreateStatPrototype(zAbiStruct.zExecutionEffPackage, sLocalPrototypeName, gbIsDebuff, "Comm Jammer", "Tiffany|CommJammer", 3, 6, "", saDescription, {{"Is Negative", 1}, {"Confuse", 1}}, 5, 9)
    
	local iType, zPrototype, iSlot = EffectList:fnLocateEntry(sLocalPrototypeName)
        if(zPrototype ~= nil) then
            zPrototype.bDontRebuildDescription = true
            zPrototype.saStrings[1] = "[Debuff]Comm Jammer"
            zPrototype.saStrings[2] = "Confuse /[Dur][Turns]"
            zPrototype.saStrings[3] = saDescription[1]
            zPrototype.saStrings[4] = saDescription[2]
            zPrototype.saStrings[5] = saDescription[3]
            zPrototype.saStrings[6] = ""
            zPrototype.saStrings[7] = ""
            zPrototype.saStrings[8] = ""
            zPrototype.sShortText = "[Debuff]Confused! ([Dur][Turns])"
        end
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Tiffany|Ability:CommJammer"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Override effect portion of prediction
    zAbiStruct.zPredictionPackage.zaEffectModules[1] = fnCreateEffectModule()
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iEffectType = gciDamageType_Shocking
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iEffectStrength = 6
    zAbiStruct.zPredictionPackage.zaEffectModules[1].sResultScript = "[Confuse] for 3 [IMG0]"
    zAbiStruct.zPredictionPackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/DebuffIcons/Clock"}
    
    --Set prototype.
    gzPrototypes.Combat.Tiffany.Subvert.CommJammer = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Tiffany.Subvert.CommJammer

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
