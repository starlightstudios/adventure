-- |[ ====================================== Bag of Tricks ===================================== ]|
-- |[Description]|
--Randomly strikes twice at two different targets (or the same target) with two random damage types.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Tiffany.Subvert.BagOfTricks == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Subvert", "Bag of Tricks", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "Tiffany|BagOfTricks", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Inflict [0.75x [Atk]], random type. Two enemies, random.\n[BaseHit]\n\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Hits two random targets for random damage\ntypes.\nDeals more total damage than a normal\nattack, costs no MP.\n\n\n\n"
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, giNoCPCost, giNoCooldown, "Target Enemies All", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Bleed")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Slashing, 5, 0.75)
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Tiffany|Ability:BagOfTricks"
    
    -- |[Finalize]|
    --Running description markdown manually to avoid calling standard prediction creation
    zAbiStruct:fnRunDescriptionMarkdowns()
    
    --Custom prediction for parity with original
    zAbiStruct.zPredictionPackage.bHasHitRateModule = true
    zAbiStruct.zPredictionPackage.zHitRateModule.iMissRate = 5
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "Random Damage Type, 0.75 x[IMG0]"
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {"Root/Images/AdventureUI/StatisticIcons/Attack"}
    
    --Set prototype.
    gzPrototypes.Combat.Tiffany.Subvert.BagOfTricks = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Tiffany.Subvert.BagOfTricks

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
if(iSwitchType == gciAbility_Execute) then

    -- |[ ========== Originator Statistics ========== ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        fnModifyCP(1)
    DL_PopActiveObject()
    
    -- |[ ========== Ability Package ========== ]|
    --We need to create two different ability packages. They can be the same by chance.
    local zaPackages = {}
    for i = 1, 2, 1 do
        
        --Roll.
        local iRoll = LM_GetRandomNumber(gciDamageType_Slashing, gciDamageType_Obscuring)
        
        --Create.
        if(iRoll == gciDamageType_Slashing) then
            zaPackages[i] = fnConstructDefaultAbilityPackage("Sword Slash")
            zaPackages[i].iDamageType = gciDamageType_Slashing
            
        elseif(iRoll == gciDamageType_Striking) then
            zaPackages[i] = fnConstructDefaultAbilityPackage("Strike")
            zaPackages[i].iDamageType = gciDamageType_Striking
            
        elseif(iRoll == gciDamageType_Piercing) then
            zaPackages[i] = fnConstructDefaultAbilityPackage("Pierce")
            zaPackages[i].iDamageType = gciDamageType_Piercing
            
        elseif(iRoll == gciDamageType_Flaming) then
            zaPackages[i] = fnConstructDefaultAbilityPackage("Flames")
            zaPackages[i].iDamageType = gciDamageType_Flaming
            
        elseif(iRoll == gciDamageType_Freezing) then
            zaPackages[i] = fnConstructDefaultAbilityPackage("Freeze")
            zaPackages[i].iDamageType = gciDamageType_Freezing
            
        elseif(iRoll == gciDamageType_Shocking) then
            zaPackages[i] = fnConstructDefaultAbilityPackage("Shock")
            zaPackages[i].iDamageType = gciDamageType_Shocking
            
        elseif(iRoll == gciDamageType_Crusading) then
            zaPackages[i] = fnConstructDefaultAbilityPackage("Light")
            zaPackages[i].iDamageType = gciDamageType_Crusading
            
        else
            zaPackages[i] = fnConstructDefaultAbilityPackage("Shadow B")
            zaPackages[i].iDamageType = gciDamageType_Obscuring
        end
    
        --Common.
        zaPackages[i].iOriginatorID  = iOriginatorID
        zaPackages[i].iMissThreshold = 5
        zaPackages[i].fDamageFactor  = 0.75
    
        --Pick one of the painted targets at random. It can be the same target twice.
        local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
        local iTargetSlot = LM_GetRandomNumber(0, iTargetsTotal-1)
    
        --Set the combat timer offset. This allows multiple hits to go through, but makes them not simultaneous.
        giCombatTimerOffset = 15 * (i-1)
        
        --Run.
        fnStandardExecution(iOriginatorID, iTargetSlot, zaPackages[i], nil)
    end

-- |[ ==================================== Standard Handler ==================================== ]|
--All other cases, call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

end
