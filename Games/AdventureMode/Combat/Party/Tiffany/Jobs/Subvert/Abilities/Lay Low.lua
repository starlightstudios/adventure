-- |[ ========================================= Lay Low ======================================== ]|
-- |[Description]|
--Reduces all threat by 25%.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Tiffany.Subvert.LayLow == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Subvert", "Lay Low", "$SkillName", gciJP_Cost_Passive, gbIsNotFreeAction, "Buff", "Passive", "Tiffany|LayLow", "Passive")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "-25%% [Threat].\n\n\n\n\n\nPassive."
    zAbiStruct.sSimpleDescMarkdown  = "Sometimes you just keep your head down.\nReduces threat, making enemies less likely to\ntarget you.\n\n\nPassive."
    
    -- |[Passive Effect]|
    zAbiStruct.bNeverAvailable   = true --Passive abilities have no usability requirements.
    zAbiStruct.sPassivePath = fnResolvePath() .. "../Effects/Lay Low.lua"
    zAbiStruct.bHasGUIEffects    = true

    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.Tiffany.Subvert.LayLow = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Tiffany.Subvert.LayLow

LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
