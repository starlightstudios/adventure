-- |[ ===================================== Pocket Hacker ====================================== ]|
-- |[Description]|
--+1 CP on combat start, 10% chance for +1 CP each turn.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Local Variables ===================================== ]|
-- |[System]|
--This effect uses a standardized StatMod script.
local sDisplayName = "Pocket Hacker"
local iDuration = -1
local bIsBuff = true
local sIcon = "Root/Images/AdventureUI/Abilities/Tiffany|PocketHacker"
local sStatString = ""

--Description can be a variable number of lines.
local saDescription = {}
saDescription[1] = "Generate free CP!"
saDescription[2] = "10%% chance to generate a CP each turn. Passive."
saDescription[3] = ""
saDescription[4] = ""
saDescription[5] = ""
saDescription[6] = ""
saDescription[7] = ""
saDescription[8] = ""

--Run the standardized builder.
gzaStatModStruct            = fnCreateStatmodEffectPrototype(sDisplayName, iDuration, bIsBuff, sIcon, sStatString)
gzaStatModStruct.saStrings  = fnStatmodBuildInspectorStrings(gzaStatModStruct, saDescription)
gzaStatModStruct.sShortText = fnStatmodBuildShortText(gzaStatModStruct)

-- |[Overrides]|
--Passive, not removed on KO, uses different frame.
gzaStatModStruct.sFrame      = gsAbility_Frame_Passive
gzaStatModStruct.bRemoveOnKO = false

--Manually specify description.
gzaStatModStruct.saStrings[1] = "[Buff]Pocket Hacker"
gzaStatModStruct.saStrings[2] = "[UpN]CP/Turn"
gzaStatModStruct.saStrings[3] = saDescription[1]
gzaStatModStruct.saStrings[4] = saDescription[2]
gzaStatModStruct.saStrings[5] = ""
gzaStatModStruct.saStrings[6] = ""
gzaStatModStruct.saStrings[7] = ""
gzaStatModStruct.saStrings[8] = "[Buff] +CP/Turn"
gzaStatModStruct.sShortText = ""

-- |[Tags]|
gzaStatModStruct.zaTagList = {{"Is Positive", 1}, {"Is Passive", 1}}

-- |[ ======================================== Creation ======================================== ]|
--Standard call when effect is initialized.
if(iSwitchType == gciEffect_Create) then
    local fSeverity = 1.0
    if(iArgumentsTotal >= 2) then fSeverity = LM_GetScriptArgument(1, "N") end
    LM_ExecuteScript(gsStandardStatPath, gciEffect_Create, fSeverity)
    
    --Give the owner a CP point.
    local iOwnerID = AdvCombatEffect_GetProperty("ID of Originator")
    AdvCombat_SetProperty("Push Entity By ID", iOwnerID)
        local iCurrentCP = AdvCombatEntity_GetProperty("Combo Points")
        AdvCombatEntity_SetProperty("Combo Points", iCurrentCP + 1)
    DL_PopActiveObject()
    
-- |[ ================================== Apply/Unapply Stats =================================== ]|
--Calls the apply/unapply stat routines.
elseif((iSwitchType == gciEffect_ApplyStats or iSwitchType == gciEffect_UnApplyStats) and iArgumentsTotal >= 2) then
    local iTargetID = math.floor(tonumber(LM_GetScriptArgument(1, "N")))
    LM_ExecuteScript(gsStandardStatPath, iSwitchType, iTargetID)
    
-- |[ ================================== Combat: Turn Begins =================================== ]|
--Special code: Generates a CP at the start of a turn.
elseif(iSwitchType == gciAbility_BeginTurn) then

    --Roll.
    local iRoll = LM_GetRandomNumber(1, 100)
    if(iRoll > 10) then return end

    --Give effect owner a CP.
    local iOwnerID = AdvCombatEffect_GetProperty("ID of Originator")
    AdvCombat_SetProperty("Push Entity By ID", iOwnerID)
        local iCurrentCP = AdvCombatEntity_GetProperty("Combo Points")
        AdvCombatEntity_SetProperty("Combo Points", iCurrentCP + 1)
    DL_PopActiveObject()

-- |[ ================================== Character Action Ends ================================= ]|
--Called after a character action ends to decrement the turn counter.
elseif(iSwitchType == gciEffect_PostAction) then
    LM_ExecuteScript(gsStandardStatPath, gciEffect_PostAction)
end
