-- |[ =================================== Jobchange: Assault =================================== ]|
-- |[Description]|
--Change jobs to Assault. Does *not* cost CP or even a free action!

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Ability Prototype =================================== ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Tiffany.Common.JobchangeAssault == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("JobChange", "Job Change - Assault", "$SkillName", gciJP_Cost_NoCost, gbIsFreeAction, "Buff", "Active", "Tiffany|Assault", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Change algorithm to Assault.\nEffortless, requires no CP and no actions."
    zAbiStruct.sSimpleDescMarkdown  = "Change algorithm to Assault.\nDoes not require any CP or a Free Action.\n\n\n\n\n"
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, giNoCPCost, giNoCooldown, "Target Self", giNoCPGeneration)
    
    --Manually override so it does not take a free action and can continuously switch
    zAbiStruct.iRequiredFreeActions = 0
    zAbiStruct.bRespectActionCap = false
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = AbiExecPack:new("JobChange")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsJobChange(psJobName, $psExclusionName, $psVariablePath)
    zAbiStruct.zExecutionAbiPackage:fnSetAsJobChange("Assault", "Assault", "Root/Variables/Global/Tiffany/sCurrentJob")
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Additional lines.
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "Change to Assault Algorithm"
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
    
    --Set prototype.
    gzPrototypes.Combat.Tiffany.Common.JobchangeAssault = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Tiffany.Common.JobchangeAssault

--Job-change abilities have a standard path.
if(iSwitchType == gciAbility_Create or iSwitchType == gciAbility_CreateSpecial) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
else
    LM_ExecuteScript(gsStandardAbilityJobchangePath, iSwitchType)
end

