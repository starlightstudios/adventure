-- |[ ================================== Build Secondary Menu ================================== ]|
--Tiffany's algorithm listing. Can be used in combat.
local sCurrentJob = VM_GetVar("Root/Variables/Global/Tiffany/sCurrentJob", "S")

-- |[ ==================================== List Construction =================================== ]|
--If not done already, construct a list of all job changes available.
if(gzaTiffanySecondaryMenu == nil) then

    --Adder function.
    local zaList = {}
    local function fnAdd(psVarName, psExclusiveName, piXPos, piYPos, psAbilityName)
        local i = #zaList + 1
        zaList[i] = {}
        zaList[i].sVariable = psVarName
        zaList[i].sExclusiveName = psExclusiveName
        zaList[i].iXPos = piXPos
        zaList[i].iYPos = piYPos
        zaList[i].sAbilityName = psAbilityName
    end

    --List.
    fnAdd("TRUE",                                            "Assault",   0, 0, "JobChange|Job Change - Assault")
    fnAdd("TRUE",                                            "Support",   1, 0, "JobChange|Job Change - Support")
    fnAdd("TRUE",                                            "Subvert",   2, 0, "JobChange|Job Change - Subvert")
    fnAdd("Root/Variables/Global/Tiffany/iHasJob_Spearhead", "Spearhead", 3, 0, "JobChange|Job Change - Spearhead")
    
    --Store.
    gzaTiffanySecondaryMenu = zaList
end

-- |[ ==================================== Menu Construction =================================== ]|
--Build the menu layout.
for i = 1, #gzaTiffanySecondaryMenu, 1 do

    --Compute position.
    local iX = gciAbility_Tactics_FormStartX + gzaTiffanySecondaryMenu[i].iXPos
    local iY = gciAbility_Tactics_FormStartY + gzaTiffanySecondaryMenu[i].iYPos
    
    --If the variable name is "TRUE" then always place this.
    if(gzaTiffanySecondaryMenu[i].sVariable == "TRUE") then
        AdvCombatEntity_SetProperty("Set Ability Slot", iX, iY, gzaTiffanySecondaryMenu[i].sAbilityName)
    
    --Otherwise, the variable must be 1.0.
    elseif(VM_GetVar(gzaTiffanySecondaryMenu[i].sVariable, "N") == 1.0) then
        AdvCombatEntity_SetProperty("Set Ability Slot", iX, iY, gzaTiffanySecondaryMenu[i].sAbilityName)
    end
end
