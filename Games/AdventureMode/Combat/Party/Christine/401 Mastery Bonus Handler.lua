-- |[ ================================= Mastery Bonus Handler ================================== ]|
--Called whenever this character masters a job or class. Unlocks the prestige class and any prestige
-- passives appropriate given what jobs are mastered.
--Typically, the AdvCombatJob is the active object. The script is designed to bypass this and 
-- gets the character directly.
local bAnyMasteries    = false
local sCharacterName   = "Christine"
local sPrestigeJobName = "Prestige"

-- |[ ========== Argument Handling =========== ]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local sSwitchType = LM_GetScriptArgument(0)

-- |[ ============================== Unlocking Prestige Passives =============================== ]|
if(sSwitchType == "Prestige Unlock") then

    -- |[Existence Check]|
    --Do nothing if the character in question doesn't exist.
    if(AdvCombat_GetProperty("Does Party Member Exist", sCharacterName) == false) then return end
    AdvCombat_SetProperty("Push Party Member", sCharacterName)
    
    -- |[ ============ Unlock Prestige Passives ============ ]|
    --These are character specific, and placed in a table.
    local saSets = {{"Brass Brawler", "Prestige|Master Brawler"}, {"Commissar", "Prestige|Master Commissar"}, {"Drone", "Prestige|Master Drone"}, {"Handmaiden", "Prestige|Master Handmaiden"}, 
                    {"Lancer", "Prestige|Master Lancer"}, {"Live Wire", "Prestige|Master Live Wire"}, {"Menialist", "Prestige|Master Menialist"}, {"Repair Unit", "Prestige|Master Repair Unit"}, 
                    {"Starseer", "Prestige|Master Starseer"}, {"Thunder Rat", "Prestige|Master Thunder Rat"}}

    --Iterate across the list.
    for i = 1, #saSets, 1 do
        
        --Get whether or not the job in question is mastered.
        AdvCombatEntity_SetProperty("Push Job S", saSets[i][1])
            local bIsMastered = AdvCombatJob_GetProperty("Is Mastered")
        DL_PopActiveObject()
        
        --If the job is mastered, unlock the matching skill in the Prestige class.
        if(bIsMastered) then
        
            --Mark that at least one job is mastered.
            bAnyMasteries = true
        
            --Unlock skill.
            AdvCombatEntity_SetProperty("Push Job S", sPrestigeJobName)
                AdvCombatJob_SetProperty("Ability Unlocked", saSets[i][2], true)
            DL_PopActiveObject()
        end
    end

    -- |[ =================== Finish Up ==================== ]|
    --Reset whether the Prestige job is visible based on whether or not any jobs are mastered. This is 
    -- typically used when loading the game and rechecking which permanent passives are unlocked.
    AdvCombatEntity_SetProperty("Push Job S", sPrestigeJobName)
        AdvCombatJob_SetProperty("Appears on Skills UI", bAnyMasteries)
    DL_PopActiveObject()
    AdvCombatEntity_SetProperty("Rebuild Job Skills UI")

    --Pop the active character.
    DL_PopActiveObject()

-- |[ ============================== Unlocking Prestige Passives =============================== ]|
--Handles checking which abilities are visible in the Prestige class. Only passives used by classes
-- that the player has unlocked are shown.
elseif(sSwitchType == "Resolve Skill Visibility") then

    -- |[Existence Check]|
    --Do nothing if the character in question doesn't exist.
    if(AdvCombat_GetProperty("Does Party Member Exist", sCharacterName) == false) then return end
    AdvCombat_SetProperty("Push Party Member", sCharacterName)

    --Sets.
    local saSets = {{"Brass Brawler", "Prestige|Master Brawler"}, {"Commissar", "Prestige|Master Commissar"}, {"Drone", "Prestige|Master Drone"}, {"Handmaiden", "Prestige|Master Handmaiden"}, 
                    {"Lancer", "Prestige|Master Lancer"}, {"Live Wire", "Prestige|Master Live Wire"}, {"Menialist", "Prestige|Master Menialist"}, {"Repair Unit", "Prestige|Master Repair Unit"}, 
                    {"Starseer", "Prestige|Master Starseer"}, {"Thunder Rat", "Prestige|Master Thunder Rat"}}

    --Iterate across the list.
    for i = 1, #saSets, 1 do
        
        --Get whether or not the job in question is visible. Remove the named ability.
        AdvCombatEntity_SetProperty("Unregister Ability From Job", saSets[i][2], sPrestigeJobName)
        AdvCombatEntity_SetProperty("Push Job S", saSets[i][1])
            local bIsOnSkillsUI = AdvCombatJob_GetProperty("Is Visible On Skills UI")
        DL_PopActiveObject()
        
        --If the job is present, add the ability.
        if(bIsOnSkillsUI == true) then
            AdvCombatEntity_SetProperty("Register Ability To Job", saSets[i][2], sPrestigeJobName)
        end
    end

    --Pop the active character.
    DL_PopActiveObject()
end
