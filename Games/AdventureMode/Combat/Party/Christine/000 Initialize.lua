-- |[ ================================ Christine Initialization ================================ ]|
--Creates and stores the named character in the party roster. If the character already exists, pushes
-- the character and modifies their properties.
local sCharacterName = "Christine"

-- |[ ======================================= Lua Globals ====================================== ]|
-- |[Ability Sectioning]|
--These globals are reset when the game restarts to avoid possible overlaps. These store the ability
-- prototypes for each class.
gzPrototypes.Combat.Christine = {}
gzPrototypes.Combat.Christine.BrassBrawler     = {}
gzPrototypes.Combat.Christine.Commissar        = {}
gzPrototypes.Combat.Christine.Common           = {}
gzPrototypes.Combat.Christine.Drone            = {}
gzPrototypes.Combat.Christine.Handmaiden       = {}
gzPrototypes.Combat.Christine.Lancer           = {}
gzPrototypes.Combat.Christine.LiveWire         = {}
gzPrototypes.Combat.Christine.Menialist        = {}
gzPrototypes.Combat.Christine.RageRat          = {}
gzPrototypes.Combat.Christine.RepairUnit       = {}
gzPrototypes.Combat.Christine.Starseer         = {}
gzPrototypes.Combat.Christine.Teacher          = {}
gzPrototypes.Combat.Christine.ThunderRat       = {}
gzPrototypes.Combat.Christine.Prestige         = {}

-- |[ ==================================== Unique Functions ==================================== ]|
--This function figures out the secondary damage type of Christine's equipped weapon and returns it.
-- Christine (AdvCombatEntity) needs to be atop the activity stack.
--Returns an integer specifying the type, and a string to be used for picking attack animations.
function fnResolveChristineWeaponSecondary()
    
    --Weapon slot's name:
    local sWeaponSlotName = AdvCombatEntity_GetProperty("Slot For Weapon Damage")
    AdvCombatEntity_SetProperty("Push Item In Slot S", sWeaponSlotName)
    
        --Get all the different types. Tag count does not matter, we just check if they are nonzero.
        local iShockDischarge  = AdItem_GetProperty("Get Tag Count", "Christine Secondary Shock")
        local iFlameDischarge  = AdItem_GetProperty("Get Tag Count", "Christine Secondary Flame")
        local iFreezeDischarge = AdItem_GetProperty("Get Tag Count", "Christine Secondary Freeze")
    DL_PopActiveObject()
    
    --Shock:
    if(iShockDischarge > 0) then
        return gciDamageType_Shocking, "Shock"
    
    --Flame:
    elseif(iFlameDischarge > 0) then
        return gciDamageType_Flaming, "Flames"
    
    --Freeze:
    elseif(iFreezeDischarge > 0) then
        return gciDamageType_Freezing, "Freeze"
    end

    --Error, none found.
    return gciDamageType_Slashing, "Sword Slash"
end

-- |[ =================================== Character Handling =================================== ]|
-- |[Register or Push]|
--Register if character does not exist.
local bIsCreating = false
if(AdvCombat_GetProperty("Does Party Member Exist", sCharacterName) == false)  then
    bIsCreating = true
    AdvCombat_SetProperty("Register Party Member", sCharacterName)

--Push character.
else
    AdvCombat_SetProperty("Push Party Member", sCharacterName)
end

-- |[Call Script]|
AdvCombatEntity_SetProperty("Response Script", fnResolvePath() .. "100 Combat Script.lua")

-- |[Base Statistics]|
--Persistent Stats
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_HPMax,   120)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_MPMax,   100)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_MPRegen,  10)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_CPMax,    10)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_StunCap, 100)

--Free-Action Handlers
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_FreeActionMax, 3)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_FreeActionGen, 1)

--Combat Stats
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Initiative, 40)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Attack,     15)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Accuracy,    0)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Evade,      20)

--Base resistances.
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Protection,     3)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Slash,   gciNormalResist+0)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Strike,  gciNormalResist+0)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Pierce,  gciNormalResist+0)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Flame,   gciNormalResist+0)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Freeze,  gciNormalResist+0)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Shock,   gciNormalResist+0)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Crusade, gciNormalResist+0)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Obscure, gciNormalResist+0)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Bleed,   gciNormalResist+0)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Poison,  gciNormalResist+0)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Corrode, gciNormalResist+0)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Terrify, gciNormalResist+0)

--Threat Multiplier. Default 100 for all entities.
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_ThreatMultiplier, 100)

-- |[ ================================== Job Statistics Chart ================================== ]|
--All jobs store their statistics here, in a big ungainly chart!
if(gzaChristineJobChart == nil) then

    -- |[Creation Function]|
    --Adds elements to the reference chart.
    local gzaRefChart = {}
    local function fnAdd(psName, piHltConstant, pfHltPercent, piIniConstant, pfIniPercent, piAtkConstant, pfAtkPercent, piAccConstant, pfAccPercent, piEvdConstant, pfEvdPercent, 
                         piProtection, piSlash, piStrike, piPierce, piFlame, piFreeze, piShock, piCrusade, piObscure, piBleed, piPoison, piCorrode, piTerrify)
        
        --Setup.
        local i = #gzaRefChart + 1
        gzaRefChart[i] = {}
        
        --System.
        gzaRefChart[i].sName = psName
        
        --Stats.
        gzaRefChart[i].iHlt_Constant = piHltConstant
        gzaRefChart[i].fHlt_Percent  = pfHltPercent
        gzaRefChart[i].iIni_Constant = piIniConstant
        gzaRefChart[i].fIni_Percent  = pfIniPercent
        gzaRefChart[i].iAtk_Constant = piAtkConstant
        gzaRefChart[i].fAtk_Percent  = pfAtkPercent
        gzaRefChart[i].iAcc_Constant = piAccConstant
        gzaRefChart[i].fAcc_Percent  = pfAccPercent
        gzaRefChart[i].iEvd_Constant = piEvdConstant
        gzaRefChart[i].fEvd_Percent  = pfEvdPercent
        
        --Resistances.
        gzaRefChart[i].iBonus_Resist_Protection = piProtection
        gzaRefChart[i].iBonus_Resist_Slash = piSlash
        gzaRefChart[i].iBonus_Resist_Pierce = piPierce
        gzaRefChart[i].iBonus_Resist_Strike = piStrike
        gzaRefChart[i].iBonus_Resist_Flame = piFlame
        gzaRefChart[i].iBonus_Resist_Freeze = piFreeze
        gzaRefChart[i].iBonus_Resist_Shock = piShock
        gzaRefChart[i].iBonus_Resist_Crusade = piCrusade
        gzaRefChart[i].iBonus_Resist_Obscure = piObscure
        gzaRefChart[i].iBonus_Resist_Bleed = piBleed
        gzaRefChart[i].iBonus_Resist_Poison = piPoison
        gzaRefChart[i].iBonus_Resist_Corrode = piCorrode
        gzaRefChart[i].iBonus_Resist_Terrify = piTerrify
    end

    --     Actual Name ||||| HltC | HltP | IniC | IniP | AtkC | AtkP | AccC | AccP | EvdC | EvdP  ||||| Pr | Sl | St | Pc | Fl | Fr | Sh | Cr | Ob | Bl | Ps | Cr | Tr
    fnAdd("Brass Brawler",    17,  0.05,   -10, -0.05,    15,  0.07,    -5, -0.03,    -8, -0.03,      1,   0,  -1,   1,   6,  -2,  -1,   1,   1,   5,   5,  -8,   8)
    fnAdd("Commissar",       -23, -0.10,    10,  0.05,   -10, -0.09,    20,  0.10,    17,  0.06,     -1,   3,  -1,   3,   4,   4,  -1,  -4,  -4,   6,   6,  -5,   0)
    fnAdd("Drone",            -8, -0.05,     0,  0.00,   -10, -0.10,     0,  0.00,     3,  0.01,      0,   2,   2,  -5,   3,   3,   7,   0,   0,   2,   2,   7,  -5)
    fnAdd("Handmaiden",        9,  0.15,   -10, -0.05,     0,  0.05,    20,  0.06,    22,  0.06,      0,   5,   5,   5,   5,   5,   5,  -6,  -6,   5,   5,   5,   3)
    fnAdd("Lancer",            9,  0.00,     0,  0.00,    10,  0.06,     5,  0.03,     3,  0.01,      0,   0,   0,   0,  -1,  -1,   1,   1,   1,  -5,   0,   0,   0)
    fnAdd("Live Wire",        -8, -0.15,    20,  0.10,    10,  0.10,     8,  0.04,     6,  0.04,      0,  -2,   2,  -2,   3,  -3,  10,   3,  -3,   3,   3,   3,   3)
    fnAdd("Menialist",        10,  0.05,    10,  0.10,    25,  0.07,    25,  0.20,    17,  0.08,      0,   3,  -2,   3,   3,   3,  -5,   0,   0,   5,   5,  -8,   4)
    fnAdd("Rage Rat",         75,  0.30,    20,  0.10,    50,  0.20,   -10, -0.10,   -10, -0.08,      0,   1,   1,   1,  -4,   2,  10,   3,  -3,  -3,  12,  12,   2)
    fnAdd("Repair Unit",      60,  0.20,   -20, -0.10,    15,  0.05,     5,  0.02,     7,  0.02,      1,   2,  -2,   2,   3,   3,  -5,  -2,  -2,   5,   5,  -8,   3)
    fnAdd("Starseer",        -12, -0.10,    10,  0.05,   -10, -0.08,    30,  0.13,    27,  0.16,      2,   4,   4,   3,   3,   2,   5,   6,   6,   2,   2,   4,  -5)
    fnAdd("Thunder Rat",     -23, -0.10,    20,  0.10,    15,  0.11,     5,  0.02,     2,  0.02,      0,   1,   1,   1,  -4,   2,  10,   3,  -3,  -3,  -3,   2,   2)

    --Teacher has fixed statistics and does not use this chart. It's included for completeness.
    fnAdd("Teacher",           0,  0.00,     0,  0.00,     0,  0.00,     0,  0.00,     0,  0.00,      0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0)

    --Save.
    gzaChristineJobChart = gzaRefChart
end

-- |[ ===================================== Equipment Slots ==================================== ]|
--Primary Weapon
AdvCombatEntity_SetProperty("Create Equipment Slot", "Weapon")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Weapon", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Weapon", false)

--Uses primary weapon to compute damage type. Can be modified by gems.
AdvCombatEntity_SetProperty("Set Equipment Slot Used For Weapon Damage", "Weapon")

--Swappable Weapons
AdvCombatEntity_SetProperty("Create Equipment Slot", "Weapon Backup A")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Weapon Backup A", false)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Weapon Backup A", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Is Alt Weapon", "Weapon Backup A", true)
AdvCombatEntity_SetProperty("Create Equipment Slot", "Weapon Backup B")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Weapon Backup B", false)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Weapon Backup B", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Is Alt Weapon", "Weapon Backup B", true)

--Body Armor
AdvCombatEntity_SetProperty("Create Equipment Slot", "Body")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Body", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Body", true)

--Accessory Slots
AdvCombatEntity_SetProperty("Create Equipment Slot", "Accessory A")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Accessory A", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Accessory A", true)
AdvCombatEntity_SetProperty("Create Equipment Slot", "Accessory B")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Accessory B", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Accessory B", true)

--Equippable Items
AdvCombatEntity_SetProperty("Create Equipment Slot", "Item A")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Item A", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Item A", true)
AdvCombatEntity_SetProperty("Create Equipment Slot", "Item B")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Item B", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Item B", true)

--Badge Slot
AdvCombatEntity_SetProperty("Create Equipment Slot", "Badge")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Badge", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Badge", true)

-- |[ ====================================== Job Handling ====================================== ]|
--Create Christine's common abilities, shared by many classes.
local sJobAbilityPath = gsRoot .. "Combat/Party/Christine/Jobs/Common/Abilities/"
LM_ExecuteScript(sJobAbilityPath .. "Attack.lua",         gciAbility_Create)
LM_ExecuteScript(sJobAbilityPath .. "AttackChris.lua",    gciAbility_Create)
LM_ExecuteScript(sJobAbilityPath .. "Discharge.lua",      gciAbility_Create)
LM_ExecuteScript(sJobAbilityPath .. "DischargeChris.lua", gciAbility_Create)
LM_ExecuteScript(sJobAbilityPath .. "Locked.lua",         gciAbility_Create)

--Job Change Abilities
LM_ExecuteScript(sJobAbilityPath .. "Jobchange_BrassBrawler.lua", gciAbility_Create)
LM_ExecuteScript(sJobAbilityPath .. "Jobchange_Commissar.lua",    gciAbility_Create)
LM_ExecuteScript(sJobAbilityPath .. "Jobchange_Drone.lua",        gciAbility_Create)
LM_ExecuteScript(sJobAbilityPath .. "Jobchange_Handmaiden.lua",   gciAbility_Create)
LM_ExecuteScript(sJobAbilityPath .. "Jobchange_Lancer.lua",       gciAbility_Create)
LM_ExecuteScript(sJobAbilityPath .. "Jobchange_LiveWire.lua",     gciAbility_Create)
LM_ExecuteScript(sJobAbilityPath .. "Jobchange_Menialist.lua",    gciAbility_Create)
LM_ExecuteScript(sJobAbilityPath .. "Jobchange_RageRat.lua",      gciAbility_Create)
LM_ExecuteScript(sJobAbilityPath .. "Jobchange_RepairUnit.lua",   gciAbility_Create)
LM_ExecuteScript(sJobAbilityPath .. "Jobchange_Starseer.lua",     gciAbility_Create)
LM_ExecuteScript(sJobAbilityPath .. "Jobchange_ThunderRat.lua",   gciAbility_Create)

--Equipment Change Abilities
LM_ExecuteScript(gsRoot .. "Combat/Party/Common/Abilities/Swap Weapon A.lua", gciAbility_Create)
LM_ExecuteScript(gsRoot .. "Combat/Party/Common/Abilities/Swap Weapon B.lua", gciAbility_Create)

--Call all of Christine's job subscripts.
local sCharacterJobPath = gsRoot .. "Combat/Party/Christine/Jobs/"
LM_ExecuteScript(sCharacterJobPath .. "Lancer/000 Job Script.lua",        gciJob_Create)
LM_ExecuteScript(sCharacterJobPath .. "Repair Unit/000 Job Script.lua",   gciJob_Create)
LM_ExecuteScript(sCharacterJobPath .. "Brass Brawler/000 Job Script.lua", gciJob_Create)
LM_ExecuteScript(sCharacterJobPath .. "Drone/000 Job Script.lua",         gciJob_Create)
LM_ExecuteScript(sCharacterJobPath .. "Starseer/000 Job Script.lua",      gciJob_Create)
LM_ExecuteScript(sCharacterJobPath .. "Handmaiden/000 Job Script.lua",    gciJob_Create)
LM_ExecuteScript(sCharacterJobPath .. "Live Wire/000 Job Script.lua",     gciJob_Create)
LM_ExecuteScript(sCharacterJobPath .. "Thunder Rat/000 Job Script.lua",   gciJob_Create)
LM_ExecuteScript(sCharacterJobPath .. "Rage Rat/000 Job Script.lua",      gciJob_Create)
LM_ExecuteScript(sCharacterJobPath .. "Commissar/000 Job Script.lua",     gciJob_Create)
LM_ExecuteScript(sCharacterJobPath .. "Menialist/000 Job Script.lua",     gciJob_Create)
LM_ExecuteScript(sCharacterJobPath .. "Teacher/000 Job Script.lua",       gciJob_Create)
LM_ExecuteScript(sCharacterJobPath .. "Prestige/000 Job Script.lua",      gciJob_Create)
AdvCombatEntity_SetProperty("Rebuild Job Skills UI")

--Set Christine to her default job.
AdvCombatEntity_SetProperty("Active Job", "Teacher")

--Once all abilities are built across all jobs, order jobs to associate with abilities. Any abilities
-- associated with a job can be purchased in the skills UI using that job's JP, and equipped from
-- that job's submenu. If an ability is not associated with any job, and is not auto-equipped by
-- any jobs when the job is assumed, the ability is not usable by the player!
LM_ExecuteScript(sCharacterJobPath .. "Lancer/000 Job Script.lua",        gciJob_JobAssembleSkillList)
LM_ExecuteScript(sCharacterJobPath .. "Brass Brawler/000 Job Script.lua", gciJob_JobAssembleSkillList)
LM_ExecuteScript(sCharacterJobPath .. "Repair Unit/000 Job Script.lua",   gciJob_JobAssembleSkillList)
LM_ExecuteScript(sCharacterJobPath .. "Drone/000 Job Script.lua",         gciJob_JobAssembleSkillList)
LM_ExecuteScript(sCharacterJobPath .. "Starseer/000 Job Script.lua",      gciJob_JobAssembleSkillList)
LM_ExecuteScript(sCharacterJobPath .. "Handmaiden/000 Job Script.lua",    gciJob_JobAssembleSkillList)
LM_ExecuteScript(sCharacterJobPath .. "Live Wire/000 Job Script.lua",     gciJob_JobAssembleSkillList)
LM_ExecuteScript(sCharacterJobPath .. "Thunder Rat/000 Job Script.lua",   gciJob_JobAssembleSkillList)
LM_ExecuteScript(sCharacterJobPath .. "Commissar/000 Job Script.lua",     gciJob_JobAssembleSkillList)
LM_ExecuteScript(sCharacterJobPath .. "Menialist/000 Job Script.lua",     gciJob_JobAssembleSkillList)
LM_ExecuteScript(sCharacterJobPath .. "Teacher/000 Job Script.lua",       gciJob_JobAssembleSkillList)

--Prestige class, only contains special passives and cannot be entered.
LM_ExecuteScript(sCharacterJobPath .. "Prestige/000 Job Script.lua",     gciJob_JobAssembleSkillList)
    
-- |[ ==================================== Creation Callback =================================== ]|
--If this was a creation call of the character, fire its script callback.
if(bIsCreating) then
    local sResponsePath = AdvCombatEntity_GetProperty("Response Path")
    LM_ExecuteScript(sResponsePath, gciCombatEntity_Create)
end

-- |[ ======================================== Clean Up ======================================== ]|
--Pop the activity stack.
DL_PopActiveObject()
