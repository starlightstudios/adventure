-- |[ =============================== Christine's Combat Script ================================ ]|
-- |[Description]|
--Christine regenerates MP normally.

-- |[Notes]|
--For all Codes, the AdvCombatEntity is the active object.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ======================================== Creation ======================================== ]|
--Called when the entity is created. Can be used to set up global variables. This callback is performed
-- after all other setup is completed. The entity will know its jobs, abilities, stats, etc.
--Note: The creation call always fires regardless of whether or not it was flagged to.
if(iSwitchType == gciCombatEntity_Create) then
    
    --Response cases.
    AdvCombatEntity_SetProperty("Response Code", gciCombatEntity_BeginCombat,     true)
    AdvCombatEntity_SetProperty("Response Code", gciCombatEntity_BeginTurn,       true)
    AdvCombatEntity_SetProperty("Response Code", gciCombatEntity_BeginAction,     true)
    AdvCombatEntity_SetProperty("Response Code", gciCombatEntity_BeginFreeAction, true)
    AdvCombatEntity_SetProperty("Response Code", gciCombatEntity_EndAction,       true)
    AdvCombatEntity_SetProperty("Response Code", gciCombatEntity_EndCombat,       true)
    AdvCombatEntity_SetProperty("Response Code", gciCombatEntity_EventQueued,     true)

-- |[ ====================================== Combat: Begin ===================================== ]|
--Called when combat begins. This is called before Ability/Job update calls, and before turn-order
-- resolution.
elseif(iSwitchType == gciCombatEntity_BeginCombat) then
    
    --DataLibrary Variables
    local iUniqueID = RO_GetID()
    DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "/")
    
    --During dialogue, which actor represents this character.
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/sDialogueActor", "S", "Christine")
    
    --Reset combo points to zero.
    AdvCombatEntity_SetProperty("Combo Points", 0)
    
-- |[ ==================================== Combat: New Turn ==================================== ]|
--Called when turn order is sorted and a new turn begins.
elseif(iSwitchType == gciCombatEntity_BeginTurn) then

    --At the start of the 0th turn, after all abilities have run, we need to set MP up.
    local iTurn = AdvCombat_GetProperty("Current Turn")
    if(iTurn > 0) then return end
    
    --Get Max MP
    local iMPMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_MPMax)

    --Set Current MP
    local iMPCur = math.floor(iMPMax * 0.50)
    AdvCombatEntity_SetProperty("Magic", iMPCur)

-- |[ =================================== Combat: New Action =================================== ]|
--Called when an action begins. Note that the action is not necessarily that of the active entity.
elseif(iSwitchType == gciCombatEntity_BeginAction) then

    --At the start of this character's action, regenerate MP. Does nothing if it's not this character's
    -- action. MP clamps against the max. Does nothing on the 0th turn.
    local iTurn = AdvCombat_GetProperty("Current Turn")
    if(iTurn == 0) then return end
    
    --Make sure it's the owner's action.
    local iCallerID = RO_GetID()
    local iActingEntityID = AdvCombat_GetProperty("ID of Acting Entity")
    if(iCallerID ~= iActingEntityID) then return end
    
    --It's our action and it's not turn 0. Generate MP!
    local iMPCur = AdvCombatEntity_GetProperty("Magic")
    local iMPMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_MPMax)
    local iMPGen = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_MPRegen)
    
    --Add, clamp, set.
    iMPCur = iMPCur + iMPGen
    if(iMPCur > iMPMax) then iMPCur = iMPMax end
    AdvCombatEntity_SetProperty("Magic", iMPCur)
    
    --Reset this flag.
    giLastDoTTick = 0
    
-- |[ ========================= Combat: New Action (After Free Action) ========================= ]|
--Called when an action begins after a Free Action has expired.
elseif(iSwitchType == gciCombatEntity_BeginFreeAction) then

-- |[ =================================== Combat: End Action =================================== ]|
--Called when an action ends. As above, not necessarily that of the active entity.
elseif(iSwitchType == gciCombatEntity_EndAction) then

-- |[ ==================================== Combat: End Turn ==================================== ]|
--Called when a turn ends, before turn order is determined for the next turn.
elseif(iSwitchType == gciCombatEntity_EndTurn) then

-- |[ =================================== Combat: End Combat =================================== ]|
--Called when combat ends for any reason. This includes, victory, defeat, retreat, or other.
elseif(iSwitchType == gciCombatEntity_EndCombat) then

    --If HP is less than 1, set it to 1.
    local iHPCur = AdvCombatEntity_GetProperty("Health")
    if(iHPCur < 1.0) then
        AdvCombatEntity_SetProperty("Health", 1)
    end

-- |[ ================================== Combat: Event Queued ================================== ]|
--Called when a main event is enqueued. An entity typically performs one event per action. This is
-- called before Abilities respond for this character.
elseif(iSwitchType == gciCombatEntity_EventQueued) then

end
