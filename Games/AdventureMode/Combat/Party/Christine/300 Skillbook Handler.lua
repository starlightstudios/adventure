-- |[ ============================== Christine Skillbook Handler =============================== ]|
--Call this file with the number of the skillbook in question, and JP and unlocks will be awarded as needed.
if(fnArgCheck(1) == false) then return end
local iLevel = LM_GetScriptArgument(0, "I")

-- |[Christine is Not In the Party]|
if(AdvCombat_GetProperty("Is Member In Active Party", "Christine") == false) then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Lieutenant's Logs.[P] Doesn't seem useful for anyone in the party right now.)") ]])
    fnCutsceneBlocker()
    return
end

-- |[Activate Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)

-- |[Common Code]|
--Check if this skillbook has been read already. If not, award JP and handle unlocks.
local iCheckVar = VM_GetVar("Root/Variables/Global/Christine/iSkillbook" .. iLevel, "N")
if(iCheckVar == 0.0) then
    
    --Mark the skillbook.
    VM_SetVar("Root/Variables/Global/Christine/iSkillbook" .. iLevel, "N", 1.0)
    
    --Increment the skillbooks total.
    local iSkillbookTotal = VM_GetVar("Root/Variables/Global/Christine/iSkillbookTotal", "N") + 1
    VM_SetVar("Root/Variables/Global/Christine/iSkillbookTotal", "N", iSkillbookTotal)
    
    --Award JP/Abilities
    AdvCombat_SetProperty("Push Party Member", "Christine")
    
        --Common strings.
        local sString = "WD_SetProperty(\"Append\", \"Christine:[E|Smirk] (Skillbook found:: Lieutenant's Logs, Volume " .. iLevel .. ".)[B][C]\")"
        fnCutscene(sString)
    
        --JP.
        local iGlobalJP = AdvCombatEntity_GetProperty("Global JP")
        AdvCombatEntity_SetProperty("Current JP", iGlobalJP + 100)
        
        --Check if this unlocked a class ability.
        if(iSkillbookTotal >= 1.0 and iSkillbookTotal <= 4.0) then
            fnCutscene([[ Append("Christine:[E|Smirk] (Gained 100 JP for Christine!)[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] (Unlocked a new job ability slot for Christine!)") ]])
        else
            fnCutscene([[ Append("Christine:[E|Smirk] (Gained 100 JP for Christine!)") ]])
        end

        --Execute the current job's SwitchTo script.
        AdvCombatEntity_SetProperty("Push Job S", "Active")
            AdvCombatJob_SetProperty("Fire Script", gciJob_SwitchTo)
        DL_PopActiveObject()
        
    DL_PopActiveObject()

--Already found.
else
    local sString = "WD_SetProperty(\"Append\", \"Christine:[E|Smirk] (Lieutenant's Logs, Volume " .. iLevel .. ". You have already read this skillbook.)\")"
    fnCutscene(sString)
end