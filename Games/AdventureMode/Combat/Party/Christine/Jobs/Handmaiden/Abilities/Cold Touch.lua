-- |[ ======================================= Cold Touch ======================================= ]|
-- |[Description]|
--Hits all enemies for 60% of base damage as freezing. Triggers Spearguard.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.Handmaiden.ColdTouch == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Handmaiden", "Cold Touch", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "Christine|ColdTouch", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\n\n\nTriggers Spearguard.\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Your spear is as cold as your dead flesh.\nDeals reduced [Frz](Freeze) damage to all\nenemies.\n\nTriggers Spearguard.\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(25, giNoCPCost, giNoCooldown, "Target Enemies All", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Freeze")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Freezing, 5, 0.60)
    
    -- |[Execution Self-Effect Package - Spearguard]|
    --Basic package.
    zAbiStruct.zExecutionSelfPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides for Spearguard.
    zAbiStruct.zExecutionSelfPackage.sEffectPath     = fnResolvePath() .. "../../Common/Effects/Spearguard.lua"
    zAbiStruct.zExecutionSelfPackage.sEffectCritPath = fnResolvePath() .. "../../Common/Effects/Spearguard.lua"
    zAbiStruct.zExecutionSelfPackage.sApplyText = "Spearguard"
    zAbiStruct.zExecutionSelfPackage.sApplyTextCol = "Color:White"
    zAbiStruct.zExecutionSelfPackage.sApplyAnimation = "Null"
    zAbiStruct.zExecutionSelfPackage.sApplySound = "Combat\\|Impact_Buff"
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Ability:ColdTouch"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Add a manual line to the prediction to reflect Spearguard.
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "Triggers Spearguard"
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
    
    --Set prototype.
    gzPrototypes.Combat.Christine.Handmaiden.ColdTouch = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.Handmaiden.ColdTouch

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
