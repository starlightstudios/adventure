-- |[ ======================================= Obliterate ======================================= ]|
-- |[Description]|
--Massive terrify damage, inflicts slow, -evd, -acc, and -atk. Costs CP.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.Handmaiden.Obliterate == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Handmaiden", "Obliterate", "$SkillName", gciJP_Cost_Advanced, gbIsNotFreeAction, "Direct", "Combo", "Christine|Obliterate", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\n[HEffect][Str9][Tfy] [Slow] -20[Evd] -20[Acc] -25%%[Atk] / 3 [Turns].\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Erase them.\nDeals massive [Tfy](Terrify) damage. Inflicts an\neffect reducing [Acc][Evd][Atk] and causing [Slow].\n\n\nCosts [CPCost][CPIco](CP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, 4, giNoCooldown, "Target Enemies Single", giNoCPGeneration)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Terrify")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Terrifying, 5, 2.75)
    
    -- |[Execution Effect Package]|
  --zAbiStruct:fnAddEffect(piStrNormal, piStrCrit, piApplyType, psApplyText, psCritApplyText, psApplyColorString, psaApplyBonus, psaApplyMalus, psaSeverityBonus, psaSeverityMalus)
    zAbiStruct:fnAddEffect(9, 13, gciDamageType_Terrifying, "Crushed!", "Badly Crushed!", "Color:Purple")
    
    -- |[Use of Effect Prototype]|
    zAbiStruct.zExecutionEffPackage.sPrototypeName = "Christine.Handmaiden.Obliterate"
    
    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
        local saDescription = {}
        saDescription[1] = "Crumble to dust..."
        saDescription[2] = "Reduces [Acc][Evd][Atk], inflicts [Slow]."
      --EffectList:fnCreateStatPrototype(pzEffectpackage, psName, pbIsBuff, psDisplayName, psIcon, piTurns, piApplyStrength, psStatString, psaDescription, $pzaTagList, $piCritTurns, $piCritApplyStrength)
        EffectList:fnCreateStatPrototype(zAbiStruct.zExecutionEffPackage, sLocalPrototypeName, gbIsDebuff, "Obliterate", "Christine|Obliterate", 3, 9, "Acc|Flat|-20 Evd|Flat|-20 Atk|Pct|-0.25", saDescription, {{"Slow", 1}, {"Is Negative", 1}}, 5, 13)
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Ability:Obliterate"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Manually adjusting prediction to include Slow
    zAbiStruct.zPredictionPackage.zaEffectModules[1].sResultScript = "-20[IMG1] -20[IMG2] -25%%[IMG3] [Slow] / 3[IMG4]"
    zAbiStruct.zPredictionPackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/StatisticIcons/Accuracy", "Root/Images/AdventureUI/StatisticIcons/Evade", "Root/Images/AdventureUI/StatisticIcons/Attack", "Root/Images/AdventureUI/DebuffIcons/Clock"}
    
    --Set prototype.
    gzPrototypes.Combat.Christine.Handmaiden.Obliterate = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.Handmaiden.Obliterate

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
