-- |[ ======================================== Influence ======================================= ]|
-- |[Description]|
--Tries to create a Terrify effect on the enemy. If successful, the enemy joins your party.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.Handmaiden.Influence == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Handmaiden", "Influence", "$SkillName", gciJP_Cost_Unique, gbIsNotFreeAction, "Direct", "Active", "Christine|Influence", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[HEffect][Str4][Tfy] Force target to join your party. [Target].\n\n\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Reach in and twist until the material complies.\nLow chance to force an enemy to join your\nteam.\nDoes not work on some enemies.\n\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(35, giNoCPCost, giNoCooldown, "Target Enemies Single", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Shadow C")

    --Manual override due to being a unique skill
    zAbiStruct.zExecutionAbiPackage.bDoesNoDamage = true
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun = true
    zAbiStruct.zExecutionAbiPackage.bNeverCrits = false
    
    -- |[Execution Effect Package]|
  --zAbiStruct:fnAddEffect(piStrNormal, piStrCrit, piApplyType, psApplyText, psCritApplyText, psApplyColorString, psaApplyBonus, psaApplyMalus, psaSeverityBonus, psaSeverityMalus)
    zAbiStruct:fnAddEffect(4, 6, gciDamageType_Terrifying, "Influenced...", "Influenced...", "Color:Purple")
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Ability:Influence"
    
    -- |[Finalize]|
    --Running description markdown manually to avoid calling standard prediction creation
    zAbiStruct:fnRunDescriptionMarkdowns()
    
    --Special: Tourist mode doesn't modify effect hit chance.
    zAbiStruct.zPredictionPackage.bTouristDoesntAffectEffects = true
    
    --Creating custom prediction
    zAbiStruct.zPredictionPackage.zaEffectModules[1] = fnCreateEffectModule()
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iEffectType = gciDamageType_Terrifying
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iDoTDamageType = gciDamageType_Terrifying
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iEffectStrength = 4
    zAbiStruct.zPredictionPackage.zaEffectModules[1].sResultScript = "Team swap until end of combat"
    zAbiStruct.zPredictionPackage.zaEffectModules[1].saResultImages = {}
    
    --Set prototype.
    gzPrototypes.Combat.Christine.Handmaiden.Influence = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.Handmaiden.Influence
    
-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--Shows chance to apply, or indicates the target is immune.
if(iSwitchType == gciAbility_BuildPredictionBox) then

    -- |[ ===================== Owner Values ===================== ]|
    --Get originator ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
    DL_PopActiveObject()
    
    --Get information about the cluster. This is needed to give each box a unique ID.
    local iClusterID = AdvCombat_GetProperty("Prediction Get Cluster ID")
    
    -- |[ ==================== For Each Target =================== ]|
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    for i = 0, iClusterTargetsTotal-1, 1 do
        
        --Target ID.
        local iTargetID = AdvCombat_GetProperty("Prediction Get Targets ID", i)
        
        --Check if the target is immune.
        AdvCombat_SetProperty("Push Entity By ID", iTargetID)
            local iImmuneTags = AdvCombatEntity_GetProperty("Tag Count", "Immune To Influence")
        DL_PopActiveObject()
        
        --Target is immune:
        if(iImmuneTags > 0) then
            local sBoxName = "Prd" .. iClusterID .. "x" .. iTargetID
            AdvCombat_SetProperty("Prediction Create Box",  sBoxName)
            AdvCombat_SetProperty("Prediction Set Host",    sBoxName, iTargetID)
            AdvCombat_SetProperty("Prediction Set Offsets", sBoxName, 0, -50)
            AdvCombat_SetProperty("Prediction Allocate Strings",        sBoxName, 1)
            AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, 0, "Immune")
            AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, 0, 0)
        
        --Build prediction box as normal:
        else
            fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, gzRefAbility.zPredictionPackage)
        end
    end

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[ ================ Originator Statistics ================= ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        fnModifyCP(gzRefAbility.iCPGain - gzRefAbility.iRequiredCP)
        fnModifyMP(gzRefAbility.iRequiredMP * -1)
    DL_PopActiveObject()
    
    -- |[Effect Power Bonus]|
    --Each level the character gains buffs application chance.
    local iEffectBonus = 0
    if(iPartyOfOriginator == gciACPartyGroup_Party) then
        AdvCombat_SetProperty("Push Entity By ID", piOriginatorID)
            local iLevel = AdvCombatEntity_GetProperty("Level")
            iEffectBonus = math.floor(iLevel * gciEffectPowerPerLevel)
        DL_PopActiveObject()
    end

    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them. Call the standard
    -- execution subroutine.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        
        -- |[Target Properties]|
        --Target ID.
        local iTimer = (gciAbility_TicksOffsetPerTarget * (i-1))
        local iTargetID = AdvCombat_GetProperty("ID Of Target", i-1)
        
        --Check target stacks.
        AdvCombat_SetProperty("Push Target", i-1) 
            local iTargetUniqueID = RO_GetID()
            local iImmuneTags = AdvCombatEntity_GetProperty("Tag Count", "Immune To Influence")
        DL_PopActiveObject()
        
        -- |[Target Roll]|
        --Check if the ability applies. Note that Tourist Mode does not factor in here.
        local iEffectStr = gzRefAbility.zExecutionEffPackage.iEffectStr
        local bRollSucceeds = fnStandardEffectApply(iTargetUniqueID, iEffectStr, gzRefAbility.zExecutionEffPackage.iEffectType, iEffectBonus)
        
        -- |[Target is Immune]|
        if(iImmuneTags > 0) then
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, iTimer, "Play Sound|Combat\\|AttackMiss")
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, iTimer, "Text|Immune!|Color:White")
            iTimer = iTimer + gciApplication_TextTicks
            AdvCombat_SetProperty("Event Timer", iTimer)
        
        -- |[Roll Failed]|
        elseif(bRollSucceeds == false) then
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, iTimer, "Play Sound|Combat\\|AttackMiss")
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, iTimer, "Text|Failed!|Color:White")
            iTimer = iTimer + gciApplication_TextTicks
            AdvCombat_SetProperty("Event Timer", iTimer)
        
        -- |[Roll Succeeds]|
        else
        
            --Store the ID of the entity.
            giTargetID = iTargetUniqueID
        
            --Change parties.
            local iTimer = (i-1) * 15
            --AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, iTimer, "Play Sound|Rubber\\|ChangeA") --We need a sound effect for this eventually.
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetID, iTimer, "Change Party|" .. gciACPartyGroup_Party)
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetID, iTimer, "Position|" .. gciACPoscode_Party)
            iTimer = iTimer + gciAC_StandardMoveTicks
            
            --End
            AdvCombat_SetProperty("Event Timer", iTimer)
            
            --Flag the enemy as no longer ambushed.
            AdvCombat_SetProperty("Push Target", i-1) 
                AdvCombatEntity_SetProperty("Ambushed", false)
            DL_PopActiveObject()
        
        end
        
    end

-- |[ ==================================== Standard Handler ==================================== ]|
--Call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end