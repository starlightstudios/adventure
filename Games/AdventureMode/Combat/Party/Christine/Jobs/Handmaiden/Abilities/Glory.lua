-- |[ ========================================= Glory ========================================== ]|
-- |[Description]|
--Generates 20 MP for free instantly. Free action.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.Handmaiden.Glory == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Handmaiden", "Glory", "$SkillName", gciJP_Cost_Normal, gbIsFreeAction, "Heal", "Active", "Christine|Glory", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "+20 [Man]. [Target].\n\n\n\n\n\nFree Action."
    zAbiStruct.sSimpleDescMarkdown  = "Bask in hidden light only the wise can see.\nGenerates 20 [MPIco](MP).\n\n\n\nFree Action."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, giNoCPCost, 1, "Target Self", giNoCPGeneration)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Healing")
    zAbiStruct.zExecutionAbiPackage:fnSetAsMPRestore(20)
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Ability:Glory"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.Christine.Handmaiden.Glory = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.Handmaiden.Glory

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
