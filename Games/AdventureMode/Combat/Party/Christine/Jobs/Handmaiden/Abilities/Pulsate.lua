-- |[ ======================================== Pulsate ========================================= ]|
-- |[Description]|
--HoT on one ally.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.Handmaiden.Pulsate == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Handmaiden", "Pulsate", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Heal", "Active", "Christine|Pulsate", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Heals [70 + [Atk] x 0.70] [Hlt] total over 5 turns. [Target].\n\n\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "The material is made to remember its old self.\nHeals wounds over 5 turns.\nHealing increases with attack power.\n\n\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(20, giNoCPCost, giNoCooldown, "Target Allies Single", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Healing")
    zAbiStruct.zExecutionAbiPackage:fnSetAsBuff()
    
    --Cannot crit.
    zAbiStruct.zExecutionAbiPackage.bNeverCrits = true
    
    -- |[Execution Effect Package]|
  --zAbiStruct:fnAddBuff(psPrototypeName, $psApplyText, $psApplyColorString, $psApplyAnim, $psApplySound)
    zAbiStruct:fnAddBuff("Christine.Handmaiden.Pulsate", "Healing!", "Color:Green", "", "Combat\\|Impact_Buff")

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
      --EffectList:fnCreateHoTPrototype(psName, psDisplayName, psIcon, pfHealBase, pfHealFactor, piTurns)
        EffectList:fnCreateHoTPrototype(sLocalPrototypeName, "Pulsate", "Christine|Pulsate", 70, 0.70, 5)
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Ability:Pulsate"

    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.Christine.Handmaiden.Pulsate = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.Handmaiden.Pulsate

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
