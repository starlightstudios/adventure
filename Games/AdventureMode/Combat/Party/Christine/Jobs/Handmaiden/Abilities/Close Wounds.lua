-- |[ ====================================== Close Wounds ====================================== ]|
-- |[Description]|
--Passive, regenerate 5% of max HP each turn.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
if(gzPrototypes.Combat.Christine.Handmaiden.CloseWounds == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Handmaiden", "Close Wounds", "$SkillName", gciJP_Cost_Passive, gbIsNotFreeAction, "Buff", "Passive", "Christine|CloseWounds", "Passive")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Regenerate 5%% of Max [Hlt] at the start of your turn.[BR][BR][BR][BR][BR][BR]Passive."
    zAbiStruct.sSimpleDescMarkdown  = "Regenerate some health every turn.[BR]Passive."
    
    -- |[Passive Effect]|
    zAbiStruct.bNeverAvailable   = true                                             --Passive abilities have no usability requirements.
    zAbiStruct.sPassivePath    = fnResolvePath() .. "../Effects/Close Wounds.lua"   --Effect script handles the HP recovery.
    zAbiStruct.bHasGUIEffects    = true
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.Christine.Handmaiden.CloseWounds = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.Handmaiden.CloseWounds

-- |[ ===================================== All Other Cases ==================================== ]|
if(iSwitchType ~= gciAbility_GUIApplyEffect) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

-- |[ ==================================== GUI: Apply Effect =================================== ]|
--When showing a character's statistics, all equipped abilities are queried. This is not reflected
-- in combat, this is GUI only.
--The standard does not handle this.
elseif(iSwitchType == gciAbility_GUIApplyEffect) then
    AdvCombatAbility_SetProperty("Push Owner")
        fnApplyStatBonus(gciStatIndex_Evade, 15)
        fnApplyStatBonus(gciStatIndex_Attack, 20)
    DL_PopActiveObject()
    gzRefAbility = nil
end

