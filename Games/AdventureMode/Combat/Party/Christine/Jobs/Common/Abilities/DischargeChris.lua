-- |[ =================================== Discharge - Chris ==================================== ]|
-- |[Description]|
--Discharge with the equipped weapon, using the secondary damage type.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Ability Prototype =================================== ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.Common.DischargeChris == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Common", "DischargeChris", "Discharge", gciJP_Cost_NoCost, gbIsNotFreeAction, "Direct", "Active", "Christine|DischargeMale", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Inflict [DamFactor]x [Atk] as [Shk]. [Target].\n[BaseHit].\n\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Attack the enemy with your tazer's discharge.\nDeals [Shk](Shock) damage.\nEffective against machines."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, giNoCPCost, giNoCooldown, "Target Enemies Single", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    --Damage type resolved in custom execution below.  Slashing type only used as placeholder.
    zAbiStruct:fnCreateAbiPack()
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage("gciDamageType_Slashing", 5, 1.00)

    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Offense|General:Swing|Ability:Discharge"

    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    gzPrototypes.Combat.Christine.Common.DischargeChris = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.Common.DischargeChris

-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--When the player is selecting targets, a little box appears above the targets. This is the prediction
-- box. This script is called once for each target cluster created when this ability is selected, and
-- prediction boxes can be made for entities in that target cluster.
--The AdvCombatAbility will be the active object.
if(iSwitchType == gciAbility_BuildPredictionBox) then

    --Figure out what the secondary damage type is. This can change from moment to moment if the weapon
    -- changes, and must be re-called each time.
    AdvCombatAbility_SetProperty("Push Owner")
        local iSecondaryDamageType, sAnimString = fnResolveChristineWeaponSecondary()
    DL_PopActiveObject()
    gzRefAbility.zPredictionPackage.zDamageModule.iDamageType = iSecondaryDamageType
    gzRefAbility.zPredictionPackage.zDamageModule.fDamageFactor = 1.00

    --Execute the standard.
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

    --Figure out what the secondary damage type is. This can change from moment to moment if the weapon
    -- changes, and must be re-called each time.
    AdvCombatAbility_SetProperty("Push Owner")
        local iSecondaryDamageType, sAnimString = fnResolveChristineWeaponSecondary()
    DL_PopActiveObject()
    
    --Set damage type.
    gzRefAbility.zExecutionAbiPackage.iDamageType = iSecondaryDamageType
    
    --Set package properties.
    if(sAnimString == "Weapon") then
        gzRefAbility.zExecutionAbiPackage.bUseWeapon = true
    
    --Otherwise, iterate across the array of types.
    else
        for i = 1, #gzaAbiPacks, 1 do
            if(gzaAbiPacks[i].sLookup == sAnimString) then
                gzRefAbility.zExecutionAbiPackage.bUseWeapon = false
                gzRefAbility.zExecutionAbiPackage.sAttackAnimation = gzaAbiPacks[i].sAttackAnim
                gzRefAbility.zExecutionAbiPackage.sAttackSound     = gzaAbiPacks[i].sAttackSound
                gzRefAbility.zExecutionAbiPackage.sCriticalSound   = gzaAbiPacks[i].sCriticalSound
                break
            end
        end
    end
    
    --Call the standard.
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
    
-- |[ ======================================== Standard ======================================== ]|
--Call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
