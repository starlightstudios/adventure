-- |[ ======================================== Discharge ======================================= ]|
-- |[Description]|
--Discharge with the equipped weapon, using the secondary damage type. Triggers Spearguard.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.Common.Discharge == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Common", "Discharge", "$SkillName", gciJP_Cost_NoCost, gbIsNotFreeAction, "Direct", "Active", "Christine|DischargeGen", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Inflict [DamFactor]x [Atk] as weapon secondary. [Target].\n[BaseHit].\n\n\n\nTriggers Spearguard.\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Attack the enemy with your spear's secondary\nattack.\nDamage type is based on the weapon.\nTriggers Spearguard."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, giNoCPCost, giNoCooldown, "Target Enemies Single", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Weapon")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_UseWeapon, 5, 1.00)
    
    -- |[Execution Self-Effect Package - Spearguard]|
    --Basic package.
    zAbiStruct.zExecutionSelfPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides for Spearguard.
    zAbiStruct.zExecutionSelfPackage.sEffectPath     = fnResolvePath() .. "../../Common/Effects/Spearguard.lua"
    zAbiStruct.zExecutionSelfPackage.sEffectCritPath = fnResolvePath() .. "../../Common/Effects/Spearguard.lua"
    zAbiStruct.zExecutionSelfPackage.sApplyText = "Spearguard"
    zAbiStruct.zExecutionSelfPackage.sApplyTextCol = "Color:White"
    zAbiStruct.zExecutionSelfPackage.sApplyAnimation = "Null"
    zAbiStruct.zExecutionSelfPackage.sApplySound = "Combat\\|Impact_Buff"
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Offense|General:Swing"
    
    -- |[Finalize]|
    --Running description markdown manually to avoid calling standard prediction creation
    zAbiStruct:fnRunDescriptionMarkdowns()
    
    --Manually creating prediction package as the one created by the standard call will not get modified in the custom prediction section
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Hit-Rate Module
    zAbiStruct.zPredictionPackage.bHasHitRateModule = true
    zAbiStruct.zPredictionPackage.zHitRateModule.iMissRate = 5
    
    --Crit Module
    zAbiStruct.zPredictionPackage.bHasCritModule = true
    zAbiStruct.zPredictionPackage.zCritModule.iCritThreshold = 100
    
    --Damage Module
    zAbiStruct.zPredictionPackage.bHasDamageModule = true
    
    --Add a manual line to the prediction to reflect Spearguard.
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "Triggers Spearguard"
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
    
    --Set prototype.
    gzPrototypes.Combat.Christine.Common.Discharge = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.Common.Discharge

-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--When the player is selecting targets, a little box appears above the targets. This is the prediction
-- box. This script is called once for each target cluster created when this ability is selected, and
-- prediction boxes can be made for entities in that target cluster.
--The AdvCombatAbility will be the active object.
if(iSwitchType == gciAbility_BuildPredictionBox) then

    --Figure out what the secondary damage type is. This can change from moment to moment if the weapon
    -- changes, and must be re-called each time.
    AdvCombatAbility_SetProperty("Push Owner")
        local iSecondaryDamageType, sAnimString = fnResolveChristineWeaponSecondary()
    DL_PopActiveObject()
    gzRefAbility.zPredictionPackage.zDamageModule.iDamageType = iSecondaryDamageType
    gzRefAbility.zPredictionPackage.zDamageModule.fDamageFactor = 1.00

    --Execute the standard.
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

    --Figure out what the secondary damage type is. This can change from moment to moment if the weapon
    -- changes, and must be re-called each time.
    AdvCombatAbility_SetProperty("Push Owner")
        local iSecondaryDamageType, sAnimString = fnResolveChristineWeaponSecondary()
    DL_PopActiveObject()
    
    --Set damage type.
    gzRefAbility.zExecutionAbiPackage.iDamageType = iSecondaryDamageType
    
    --Set package properties.
    if(sAnimString == "Weapon") then
        gzRefAbility.zExecutionAbiPackage.bUseWeapon = true
    
    --Otherwise, iterate across the array of types.
    else
        for i = 1, #gzaAbiPacks, 1 do
            if(gzaAbiPacks[i].sLookup == sAnimString) then
                gzRefAbility.zExecutionAbiPackage.bUseWeapon = false
                gzRefAbility.zExecutionAbiPackage.sAttackAnimation = gzaAbiPacks[i].sAttackAnim
                gzRefAbility.zExecutionAbiPackage.sAttackSound     = gzaAbiPacks[i].sAttackSound
                gzRefAbility.zExecutionAbiPackage.sCriticalSound   = gzaAbiPacks[i].sCriticalSound
                break
            end
        end
    end
    
    --Call the standard.
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
    
-- |[ ======================================== Standard ======================================== ]|
--Call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
