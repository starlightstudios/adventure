-- |[ ========================================= Attack ========================================= ]|
-- |[Description]|
--Strike with the equipped weapon. Christine's basic attacks trigger 'Spearguard'.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.Common.Attack == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Common", "Attack", "$SkillName", gciJP_Cost_NoCost, gbIsNotFreeAction, "Direct", "Active", "Attack", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\n\n\nTriggers Spearguard.\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Attack the enemy with your spear.\nTriggers Spearguard."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, giNoCPCost, giNoCooldown, "Target Enemies Single", 1)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Weapon")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_UseWeapon, 5, 1.00)
    
    -- |[Execution Self-Effect Package - Spearguard]|
    --Basic package.
    zAbiStruct.zExecutionSelfPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides for Spearguard.
    zAbiStruct.zExecutionSelfPackage.sEffectPath     = fnResolvePath() .. "../../Common/Effects/Spearguard.lua"
    zAbiStruct.zExecutionSelfPackage.sEffectCritPath = fnResolvePath() .. "../../Common/Effects/Spearguard.lua"
    zAbiStruct.zExecutionSelfPackage.sApplyText = "Spearguard"
    zAbiStruct.zExecutionSelfPackage.sApplyTextCol = "Color:White"
    zAbiStruct.zExecutionSelfPackage.sApplyAnimation = "Null"
    zAbiStruct.zExecutionSelfPackage.sApplySound = "Combat\\|Impact_Buff"
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Offense|General:Swing"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Add a manual line to the prediction to reflect Spearguard.
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "Triggers Spearguard"
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
    
    --Set prototype.
    gzPrototypes.Combat.Christine.Common.Attack = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.Common.Attack

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)