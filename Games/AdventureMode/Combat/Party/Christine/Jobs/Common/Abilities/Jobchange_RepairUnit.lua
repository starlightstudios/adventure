-- |[ ================================= Jobchange: Repair Unit ================================= ]|
-- |[Description]|
--Change jobs to Lancer. Costs CP, Free Action.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Ability Prototype =================================== ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.Common.JobchangeRepairUnit == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("JobChange", "Job Change - Repair Unit", "$SkillName", gciJP_Cost_NoCost, gbIsFreeAction, "Buff", "Combo", "Christine|InspiringFortitude", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Transform into a Golem Repair Unit.\n\n\n\n\nFree Action.\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Transform into a Golem Repair Unit.\nCosts [CPCost][CPIco](CP).\nFree Action.\n\n\n\n"
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, 2, giNoCooldown, "Target Self", giNoCPGeneration)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = AbiExecPack:new("JobChange")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsJobChange(psJobName, $psExclusionName, $psVariablePath)
    zAbiStruct.zExecutionAbiPackage:fnSetAsJobChange("Repair Unit", "Golem", "Root/Variables/Global/Christine/sForm")
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Additional lines.
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "Transform to Golem Repair Unit"
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
    
    --Set prototype.
    gzPrototypes.Combat.Christine.Common.JobchangeRepairUnit = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.Common.JobchangeRepairUnit

--Job-change abilities have a standard path.
if(iSwitchType == gciAbility_Create or iSwitchType == gciAbility_CreateSpecial) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
else
    LM_ExecuteScript(gsStandardAbilityJobchangePath, iSwitchType)
end
