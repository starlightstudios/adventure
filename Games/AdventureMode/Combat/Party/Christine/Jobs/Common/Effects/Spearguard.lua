-- |[ ======================================= Spearguard ======================================= ]|
-- |[Description]|
--Increases protection by 5 for 1 turn.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Local Variables ===================================== ]|
-- |[System]|
--This effect uses a standardized StatMod script.
local sDisplayName = "Spearguard"
local iDuration = 2
local bIsBuff = true
local sIcon = "Root/Images/AdventureUI/Abilities/Defend"
local sStatString = "Prt|Flat|5"

--Description can be a variable number of lines.
local saDescription = {}
saDescription[1] = "Block incoming attacks with your spear."
saDescription[2] = "Activated by any spear attack. Increases [Prt]."

--Run the standardized builder.
gzaStatModStruct            = fnCreateStatmodEffectPrototype(sDisplayName, iDuration, bIsBuff, sIcon, sStatString)
gzaStatModStruct.saStrings  = fnStatmodBuildInspectorStrings(gzaStatModStruct, saDescription)
gzaStatModStruct.sShortText = fnStatmodBuildShortText(gzaStatModStruct)

-- |[Overrides]|
--Tags.
gzaStatModStruct.zaTagList = {{"Spearguard", 1}, {"Is Positive", 1}}

-- |[ ======================================== Creation ======================================== ]|
--Standard call when effect is initialized.
if(iSwitchType == gciEffect_Create) then
    
    -- |[Argument Check]|
    --Arguments.
    local fSeverity = 1.0
    if(iArgumentsTotal >= 2) then fSeverity = LM_GetScriptArgument(1, "N") end
    
    -- |[Repeat Check]|
    --Special: Check if Spearguard is already present. If so, order this to expire, and refresh the duration on the existing spearguard.
    AdvCombatEffect_SetProperty("Push Originator")
        local iTotalSpearguards = AdvCombatEntity_GetProperty("Effects With Tag", "Spearguard")
        local iaSpearguardIDs = {}
        for i = 1, iTotalSpearguards, 1 do
            iaSpearguardIDs[i] = AdvCombatEntity_GetProperty("Effect ID With Tag", "Spearguard", i-1)
        end
    DL_PopActiveObject()
    
    -- |[Creation]|
    --Execute creation handler.
    LM_ExecuteScript(gsStandardStatPath, gciEffect_Create, fSeverity)
    
    -- |[Deletion]|
    --If there were any other spearguards, remove them.
    for i = 1, iTotalSpearguards, 1 do
        AdvCombat_SetProperty("Remove Effect", iaSpearguardIDs[i])
    end
    
-- |[ ================================== Apply/Unapply Stats =================================== ]|
--Calls the apply/unapply stat routines.
elseif((iSwitchType == gciEffect_ApplyStats or iSwitchType == gciEffect_UnApplyStats) and iArgumentsTotal >= 2) then

    --Arguments.
    local iTargetID = math.floor(tonumber(LM_GetScriptArgument(1, "N")))
    
    --Set the stat string to its default.
    gzaStatModStruct.sStatString = sStatString
    
    --Check if Frontline Leader is present. If it is, it modifies the stat string.
    AdvCombatEffect_SetProperty("Push Originator")
        local bIsAbilityEquipped = AdvCombatEntity_GetProperty("Is Ability Equipped", "Commissar|Frontline Leader")
    DL_PopActiveObject()
    if(bIsAbilityEquipped == true) then
        gzaStatModStruct.sStatString = gzaStatModStruct.sStatString .. " Atk|Pct|0.10"
        gzaStatModStruct.saStrings  = fnStatmodBuildInspectorStrings(gzaStatModStruct, saDescription)
        gzaStatModStruct.sShortText = fnStatmodBuildShortText(gzaStatModStruct)
    end
    
    --Execute standard handler.
    LM_ExecuteScript(gsStandardStatPath, iSwitchType, iTargetID)

-- |[ ================================== Character Action Ends ================================= ]|
--Called after a character action ends to decrement the turn counter.
elseif(iSwitchType == gciEffect_PostAction) then
    LM_ExecuteScript(gsStandardStatPath, gciEffect_PostAction)
end
