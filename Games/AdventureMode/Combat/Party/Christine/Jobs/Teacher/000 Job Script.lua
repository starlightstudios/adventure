-- |[ ========================================= Teacher ======================================== ]|
--Christine's human male class for chapter 5. Only playable at the start.

-- |[Notes]|
--For gciJob_Create, the AdvCombatEntity that will own the job is the active object. For all other
-- calls, the AdvCombatJob is active and can push the owning entity.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ====================================== Job Creation ====================================== ]|
--Called when the chapter is initialized, the job registers itself to its owner and populates its
-- abilities. This is only called once.
--The owner should be the active object.
if(iSwitchType == gciJob_Create) then
    
    --Create the job and set its stats.
    AdvCombatEntity_SetProperty("Create Job", "Teacher")
    
        --Name as it appears in the UI.
        AdvCombatJob_SetProperty("Display Name", "Teacher")
        AdvCombatJob_SetProperty("Appears on Skills UI", false)
    
        --Script path to this job
        AdvCombatJob_SetProperty("Script Path", LM_GetCallStack(0))
        
        --Responses.
        AdvCombatJob_SetProperty("Script Response", gciJob_SwitchTo, true)
        AdvCombatJob_SetProperty("Script Response", gciJob_Level, true)
        AdvCombatJob_SetProperty("Script Response", gciJob_Master, true)
        AdvCombatJob_SetProperty("Script Response", gciJob_JobAssembleSkillList, true)
        AdvCombatJob_SetProperty("Script Response", gciJob_BeginCombat, true)
        
        --Display
        local fIndexX = 30
        local fIndexY = 4
        local fLft = (fIndexX+0) * gci_FaceTable_Size
        local fTop = (fIndexY+0) * gci_FaceTable_Size
        local fRgt = (fIndexX+1) * gci_FaceTable_Size
        local fBot = (fIndexY+1) * gci_FaceTable_Size
        AdvCombatJob_SetProperty("Face Table Data", fLft, fTop, fRgt, fBot, "Root/Images/AdventureUI/ItemIcons/CharacterFaces")

    DL_PopActiveObject()
    
    --No abilities to register.
   
-- |[ ===================================== Job Assumption ===================================== ]|
--Called when the player switches to the class. Clears any existing job properties, sets graphics
-- and stats as needed.
--This can be called during combat, as some characters can switch jobs in-battle.
--The calling AdvCombatEntity is expected to be the active object.
elseif(iSwitchType == gciJob_SwitchTo) then

    --Change images.
    VM_SetVar("Root/Variables/Global/Christine/sForm", "S", "Male")
    LM_ExecuteScript(gsCostumeAutoresolve, "Christine_Male")
    
    --Flag to appear on UI.
    AdvCombatJob_SetProperty("Appears on Skills UI", true)

    --Push the owning entity.
    AdvCombatJob_SetProperty("Push Owner")

        -- |[Display Name]|
        --Change the display name to Chris when in this class.
        AdvCombatEntity_SetProperty("Display Name", "Chris")

        -- |[Rebuild Visibility List]|
        AdvCombatEntity_SetProperty("Rebuild Job Skills UI")

        -- |[Statistics]|
        --Get health percentage.
        local fHPPercent = AdvCombatEntity_GetProperty("Health") / AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
        
        --Set job statistics.
        AdvCombatEntity_SetProperty("Compute Level Statistics", -1)
        
        --Apply change in max HP.
        AdvCombatEntity_SetProperty("Health Percent", fHPPercent)
        
        --Clear all relevant ability slots.
        fnClearJobSlots()
        
        -- |[Top Bar]|
        --Set basic actions bar.
        AdvCombatEntity_SetProperty("Set Ability Slot", 0, 0, "Common|AttackChris")
        AdvCombatEntity_SetProperty("Set Ability Slot", 1, 0, "Common|DischargeChris")
        
        --4-CP ability.
        AdvCombatEntity_SetProperty("Set Ability Slot", 2, 0, "Null")
        
        --Items, Retreat, Surrender, Etc.
        fnPlaceJobCommonSkillsNoItems()
        
        -- |[Class Bar]|
        --This class has no abilities! Chris sucks!
        AdvCombatEntity_SetProperty("Set Ability Slot", 0, 1, "Null")
        AdvCombatEntity_SetProperty("Set Ability Slot", 1, 1, "Null")
        AdvCombatEntity_SetProperty("Set Ability Slot", 2, 1, "Null")
        AdvCombatEntity_SetProperty("Set Ability Slot", 0, 2, "Null")
        AdvCombatEntity_SetProperty("Set Ability Slot", 1, 2, "Null")
        AdvCombatEntity_SetProperty("Set Ability Slot", 2, 2, "Null")
        
        -- |[Job Changes]|
        LM_ExecuteScript(gsRoot .. "Combat/Party/Christine/200 Build Secondary Menu.lua")
        
    DL_PopActiveObject()

-- |[ ====================================== Job Level Up ====================================== ]|
--Called when the job reaches a level. The second argument is the level in question. Remember that
-- level 1 on the UI is level 0 in the scripts.
--When assuming the job, this is called by the program to reset stats. There is no need to call it
-- during the Job Assumption part of this script.
elseif(iSwitchType == gciJob_Level) then

    -- |[Setup]|
    --Argument check.
    if(iArgumentsTotal < 2) then 
        Debug_ForcePrint("Error: No level specified for Job Level Up. " .. LM_GetCallStack(0) .. "\n") 
        return
    end

    --First argument is what level we reached.
    local iLevelReached = LM_GetScriptArgument(1, "N")
    
    -- |[Sum And Upload]|
    --Iterate across to the given level.
    local iHPBonus  = iLevelReached - 50
    local iAtkBonus = iLevelReached
    local iAccBonus = iLevelReached
    local iEvdBonus = iLevelReached
    local iIniBonus = iLevelReached
    
    -- |[Catalysts]|
    --Add catalyst bonuses.
    local iHpCatalysts  = AdInv_GetProperty("Catalyst Count", gciCatalyst_Health)
    local iAtkCatalysts = AdInv_GetProperty("Catalyst Count", gciCatalyst_Attack)
    local iAccCatalysts = AdInv_GetProperty("Catalyst Count", gciCatalyst_Accuracy)
    local iEvdCatalysts = AdInv_GetProperty("Catalyst Count", gciCatalyst_Evade)
    local iIniCatalysts = AdInv_GetProperty("Catalyst Count", gciCatalyst_Initiative)
    iHPBonus  = iHPBonus  + (math.floor(iHpCatalysts  / gciCatalyst_Health_Needed)     * gciCatalyst_Health_Bonus)
    iAtkBonus = iAtkBonus + (math.floor(iAtkCatalysts / gciCatalyst_Attack_Needed)     * gciCatalyst_Attack_Bonus)
    iAccBonus = iAccBonus + (math.floor(iAccCatalysts / gciCatalyst_Accuracy_Needed)   * gciCatalyst_Accuracy_Bonus)
    iEvdBonus = iEvdBonus + (math.floor(iEvdCatalysts / gciCatalyst_Evade_Needed)      * gciCatalyst_Evade_Bonus)
    iIniBonus = iIniBonus + (math.floor(iIniCatalysts / gciCatalyst_Initiative_Needed) * gciCatalyst_Initiative_Bonus)

    -- |[Upload]|
    --Final tally. Upload to the temp structure.
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_HPMax,      math.floor(iHPBonus))
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Attack,     math.floor(iAtkBonus))
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Accuracy,   math.floor(iAccBonus))
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Evade,      math.floor(iEvdBonus))
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Initiative, math.floor(iIniBonus))
    
    -- |[Resistances]|
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Protection,     0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Slash,   0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Strike,  0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Pierce,  0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Flame,   0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Freeze,  0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Shock,   0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Crusade, 0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Obscure, 0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Bleed,   0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Poison,  0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Corrode, 0)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Terrify, 0)
    
-- |[ ====================================== Job Mastered ====================================== ]|
--Called when the player masters the class and gets the mastery bonus applied. Also called when the
-- character changes class and has the mastery bonus applied to that class.
elseif(iSwitchType == gciJob_Master) then
    
-- |[ =================================== Assemble Skill List ================================== ]|
--Once all abilities are registered to the parent entity, builds a list of which skills are associated
-- with this job. These skills show up on the UI and can be purchased by the player. Note that a job
-- can contain the same ability as another job. Purchasing it in either will unlock both versions.
-- This is not explicitly a problem, it means the same ability can be bought with JP from multiple
-- jobs. It is not part of the standard for Adventure Mode though.
elseif(iSwitchType == gciJob_JobAssembleSkillList) then

    --Setup.
    local sJobName = "Teacher"
    
    --Register abilities in the order they should appear in the skills menu.
    --AdvCombatEntity_SetProperty("Register Ability To Job", "Repair Unit Internal|Sweep", sJobName)

-- |[ ====================================== Combat Start ====================================== ]|
--Called when combat begins. The job should populate passive abilities here. The AdvCombatEntity
-- will be atop the activity stack.
elseif(iSwitchType == gciJob_BeginCombat) then
    AdvCombatJob_SetProperty("Push Owner")
        fnPlaceJobCommonSkills()
        fnPlaceJobCommonSkillsCombatStart()
    DL_PopActiveObject()
    
-- |[ ==================================== Combat: New Turn ==================================== ]|
--Called when turn order is sorted and a new turn begins.
elseif(iSwitchType == gciJob_BeginTurn) then

-- |[ =================================== Combat: New Action =================================== ]|
--Called when an action begins. Note that the action is not necessarily that of the active entity.
elseif(iSwitchType == gciJob_BeginAction) then
    
-- |[ ========================= Combat: New Action (After Free Action) ========================= ]|
--Called when an action begins after a Free Action has expired.
elseif(iSwitchType == gciJob_BeginFreeAction) then

-- |[ =================================== Combat: End Action =================================== ]|
--Called when an action ends. As above, not necessarily that of the active entity.
elseif(iSwitchType == gciJob_EndAction) then

-- |[ ==================================== Combat: End Turn ==================================== ]|
--Called when a turn ends, before turn order is determined for the next turn.
elseif(iSwitchType == gciJob_EndTurn) then

-- |[ =================================== Combat: End Combat =================================== ]|
--Called when combat ends for any reason. This includes, victory, defeat, retreat, or other.
elseif(iSwitchType == gciJob_EndCombat) then

-- |[ ================================== Combat: Event Queued ================================== ]|
--Called when a main event is enqueued. An entity typically performs one event per action. This is
-- called before after Entity but before Abilities respond for this character.
elseif(iSwitchType == gciJob_EventQueued) then

end
