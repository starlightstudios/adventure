-- |[ =========================================== Ram ========================================== ]|
-- |[Description]|
--Extra striking damage to one target.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.RepairUnit.Ram == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Repair Unit", "Ram", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "Christine|Ram", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\n[HEffect][Str6][Stk]'Slow' / 1 [Turns].\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Ram into one enemy for major striking damage.\nInflicts 'Slow' for one turn.\nIncreased chance to hit.\n\n\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(25, giNoCPCost, giNoCooldown, "Target Enemies Single", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Strike")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Striking, 0, 1.60)
    
    -- |[Execution Effect Package]|
  --zAbiStruct:fnAddEffect(piStrNormal, piStrCrit, piApplyType, psApplyText, psCritApplyText, psApplyColorString, psaApplyBonus, psaApplyMalus, psaSeverityBonus, psaSeverityMalus)
    zAbiStruct:fnAddEffect(6, 9, gciDamageType_Striking, "Slowed!", "Badly Slowed!", "Color:Purple")
    
    -- |[Use of Effect Prototype]|
    zAbiStruct.zExecutionEffPackage.sPrototypeName = "Christine.RepairUnit.Ram"
    
    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
        local saDescription = {}
        saDescription[1] = "Knocked off balance!"
        saDescription[2] = "[Slow], acts last next turn."
      --EffectList:fnCreateStatPrototype(pzEffectpackage, psName, pbIsBuff, psDisplayName, psIcon, piTurns, piApplyStrength, psStatString, psaDescription, $pzaTagList, $piCritTurns, $piCritApplyStrength)
        EffectList:fnCreateStatPrototype(zAbiStruct.zExecutionEffPackage, sLocalPrototypeName, gbIsDebuff, "Ram", "Christine|Ram", 2, 6, "", saDescription, {{"Slow", 1}, {"Is Negative", 1}}, 4, 9)
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Offense|Ability:Ram"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Adjust prediction to show slow status
    zAbiStruct.zPredictionPackage.zaEffectModules[1].sResultScript = "Slow / 1[IMG2]"
    zAbiStruct.zPredictionPackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/DebuffIcons/Clock"}
    
    --Set prototype.
    gzPrototypes.Combat.Christine.RepairUnit.Ram = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.RepairUnit.Ram

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)