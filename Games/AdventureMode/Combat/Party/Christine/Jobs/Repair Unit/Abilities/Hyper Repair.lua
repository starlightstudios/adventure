-- |[ ====================================== Hyper Repair ====================================== ]|
-- |[Description]|
--Heals all allies, buffs attack power by 50% for 3 turns.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.RepairUnit.HyperRepair == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Repair Unit", "Hyper Repair", "$SkillName", gciJP_Cost_Advanced, gbIsNotFreeAction, "Heal", "Combo", "Christine|HyperRepair", "Direct")
  
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Restore]. [Target].\n\n[FEffect] +25%%[Atk] / 3 [Turns].\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Fix up the entire party and give them a\ntune-up to boot!\nHeals and increases attack power for 3 turns.\n\n\nCosts [CPCost][CPIco](CP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, 4, giNoCooldown, "Target Allies All", giNoCPGeneration)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Healing")
    --Initial healing
  --zAbiStruct.zExecutionAbiPackage:fnSetAsHealing(piHealingBase, pfHealingScaleFactor, pfHealingPctOfHPMax)
    zAbiStruct.zExecutionAbiPackage:fnSetAsHealing(50, 0.50, 0)
    
    --Setting up attack buff
    zAbiStruct.zExecutionAbiPackage:fnSetAsBuff()
    
    -- |[Effect Package]|
  --zAbiStruct:fnAddBuff(psPrototypeName, $psApplyText, $psApplyColorString, $psApplyAnim, $psApplySound)
    zAbiStruct:fnAddBuff("Christine.RepairUnit.HyperRepair", "Optimized!", "Color:Purple")
    zAbiStruct.zExecutionEffPackage.sResistText = "Failed!"
    
    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName

    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
        local saDescription = {}
        saDescription[1] = "Let the power flow!"
        saDescription[2] = "Increases [Atk](Power)."
      --EffectList:fnCreateBuffPrototype(psName, psDisplayName, psIcon, piTurns, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreateBuffPrototype(sLocalPrototypeName, "Hyper Repair", "Christine|HyperRepair", 3, "Atk|Pct|0.25", saDescription, {{"Is Positive", 1}})
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Ability:HyperRepair"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.Christine.RepairUnit.HyperRepair = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.RepairUnit.HyperRepair

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
