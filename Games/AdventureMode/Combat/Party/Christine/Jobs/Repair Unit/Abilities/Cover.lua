-- |[ ========================================= Cover ========================================== ]|
-- |[Description]|
--Redirects damage from the targeted entity to the user. Triggers Spearguard.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.RepairUnit.Cover == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Repair Unit", "Cover", "$SkillName", gciJP_Cost_Cheap, gbIsNotFreeAction, "Buff", "Active", "Christine|Cover", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Absorb hits in place of an ally.\n+5 [Prt] / 1 [Turns].\n\n\n\nTriggers Spearguard.\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Leap in front of an ally and take hits in their\nplace.\nLasts until your next turn.\nIncreases Protection.\nTriggers Spearguard.\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(10, giNoCPCost, giNoCooldown, "Target Allies Single Not Self", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Buff")
    zAbiStruct.zExecutionAbiPackage:fnSetAsBuff()
    
    -- |[Execution Self-Effect Package]|
    --Basic package.  Does not use a prototype because of custom effect create.
    zAbiStruct.zExecutionSelfPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides.
    zAbiStruct.zExecutionSelfPackage.sEffectPath     = fnResolvePath() .. "../Effects/Cover.lua"
    zAbiStruct.zExecutionSelfPackage.sEffectCritPath = fnResolvePath() .. "../Effects/Cover.lua"
    zAbiStruct.zExecutionSelfPackage.sApplyText = "Covering!"
    zAbiStruct.zExecutionSelfPackage.sApplyTextCol = "Color:White"
    zAbiStruct.zExecutionSelfPackage.sApplyAnimation = "Null"
    zAbiStruct.zExecutionSelfPackage.sApplySound = "Combat\\|Impact_Buff"
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Ability:Cover"
    
    -- |[Finalize]|
    --Running description markdown manually to avoid calling standard prediction creation
    zAbiStruct:fnRunDescriptionMarkdowns()
    
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Create an Effect module
    zAbiStruct.zPredictionPackage.zaEffectModules[1] = fnCreateEffectModule()
    zAbiStruct.zPredictionPackage.zaEffectModules[1].bIsBuff = true
    zAbiStruct.zPredictionPackage.zaEffectModules[1].sResultScript = "Covered / 1[IMG1]"
    zAbiStruct.zPredictionPackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/DebuffIcons/Clock"}
    
    --Add "Triggers Spearguard" package.
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "Triggers Spearguard"
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
    
    --Set prototype.
    gzPrototypes.Combat.Christine.RepairUnit.Cover = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.RepairUnit.Cover

-- |[ ============================ All Executions Except on Targets ============================ ]|
--Call the standard handler if the case is NOT execution:
if(iSwitchType ~= gciAbility_Execute) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
    
-- |[ =============================== Combat: Execute on Targets =============================== ]|
--This is a copy of the usual execution, except the self-effect of Cover needs more information to
-- be provided via globals.
elseif(iSwitchType == gciAbility_Execute) then
    
    -- |[ ============ Originator Statistics =========== ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        fnModifyCP(gzRefAbility.iCPGain - gzRefAbility.iRequiredCP)
        fnModifyMP(gzRefAbility.iRequiredMP * -1)
    DL_PopActiveObject()
    
    -- |[ =============== Ability Package ============== ]|
    --Create an ability package. This package comes with the default hit/miss values, animations, etc.
    local zAbilityPackage = gzRefAbility.zExecutionAbiPackage
    zAbilityPackage.iOriginatorID = iOriginatorID
    
    --Handling of free actions:
    if(gzRefAbility.bIsFreeAction) then
        AdvCombat_SetProperty("Set As Free Action")
    end
    
    --Handling of cooldowns:
    if(gzRefAbility.iCooldown > 0) then
        AdvCombatAbility_SetProperty("Cooldown", gzRefAbility.iCooldown)
    end
    
    -- |[ =============== Effect Package =============== ]|
    --Create an effect package and populate it.
    local zEffectPackage = gzRefAbility.zExecutionEffPackage
    if(zEffectPackage ~= nil) then
        zEffectPackage.iOriginatorID = iOriginatorID
    end
    
    -- |[ =============== For Each Target ============== ]|
    --Get how many targets were painted by this ability, iterate across them.
    giStoredCoverTargetID = 0
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        
        --Provide the cover ID.
        AdvCombat_SetProperty("Push Target", i-1)
            giStoredCoverTargetID = RO_GetID()
        DL_PopActiveObject()
        
        --Execute.
        fnStandardExecution(iOriginatorID, i-1, zAbilityPackage, zEffectPackage)
    end
    
    -- |[ ================ Self-Effects ================ ]|
    --Cover Effect.
    if(gzRefAbility.zExecutionSelfPackage ~= nil) then
        gzRefAbility.zExecutionSelfPackage.fnHandler(iOriginatorID, iOriginatorID, 0, gzRefAbility.zExecutionSelfPackage, true, false)
    end
    
    --Needs a Spearguard effect too.
    local zSpearguardPackage = fnConstructDefaultEffectPackage()
    zSpearguardPackage.sEffectPath     = fnResolvePath() .. "../../Common/Effects/Spearguard.lua"
    zSpearguardPackage.sEffectCritPath = fnResolvePath() .. "../../Common/Effects/Spearguard.lua"
    zSpearguardPackage.sApplyText = "Spearguard"
    zSpearguardPackage.sApplyTextCol = "Color:White"
    zSpearguardPackage.sApplyAnimation = "Null"
    zSpearguardPackage.sApplySound = "Combat\\|Impact_Buff"
    gzRefAbility.zExecutionSelfPackage.fnHandler(iOriginatorID, iOriginatorID, 20, zSpearguardPackage, true, false)
    
    --Clean.
    gzRefAbility = nil
    
end

