-- |[ ==================================== Singular Strike ===================================== ]|
-- |[Description]|
--Increases critical strike damage factor by 0.50. Passive.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
if(gzPrototypes.Combat.Christine.RepairUnit.SingularStrike == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Repair Unit", "Singular Strike", "$SkillName", gciJP_Cost_Passive, gbIsNotFreeAction, "Buff", "Passive", "Christine|SingularStrike", "Passive")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Increases critical strike bonus by 50%%.\n\n\n\n\n\nPassive."
    zAbiStruct.sSimpleDescMarkdown  = "Puts more oomph behind your crits.\nIncreases critical strike damage boost from\n1.25x to 1.75x.\n\n\nPassive."

    -- |[Passive Effect]|
    zAbiStruct.bNeverAvailable   = true --Passive abilities have no usability requirements.
    zAbiStruct.sPassivePrototype = "Christine.RepairUnit.SingularStrike"
    zAbiStruct.bHasGUIEffects    = true    

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    if(EffectList:fnEntryExists(zAbiStruct.sPassivePrototype) == false) then
        local saDescription = {}
        saDescription[1] = "Make them all count."
        saDescription[2] = "Increases critical strike damage. Passive."
      --EffectList:fnCreatePassivePrototype(psName, psDisplayName, psIcon, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreatePassivePrototype(zAbiStruct.sPassivePrototype, "Singular Strike", "Christine|SingularStrike", "", saDescription, {{"Is Positive", 1}, {"Is Passive", 1}, {"Critical Damage Bonus", 50}})
        
        --Manually specify description.
        local iType, zPrototype, iSlot = EffectList:fnLocateEntry(zAbiStruct.sPassivePrototype)
        if(zPrototype ~= nil) then
            zPrototype.bDontRebuildDescription = true
            zPrototype.saStrings[1] = "[Buff]Singular Strike"
            zPrototype.saStrings[2] = "[UpN]Crit Power"
            zPrototype.saStrings[3] = saDescription[1]
            zPrototype.saStrings[4] = saDescription[2]
            zPrototype.saStrings[5] = ""
            zPrototype.saStrings[6] = ""
            zPrototype.saStrings[7] = ""
            zPrototype.saStrings[8] = "[Buff] +Crit Damage"
            zPrototype.sShortText = ""
        end
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.Christine.RepairUnit.SingularStrike = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.RepairUnit.SingularStrike

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
