-- |[ ======================================== Prestige ======================================== ]|
--Prestige class. Cannot be used, contains passives that only unlock when all skills are purchased
-- from base classes and do not need to be equipped.

-- |[ ========== Argument Handling =========== ]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--1st argument is only used for level-up calls.
local iLevelReached = 0
if(iArgumentsTotal >= 2) then
    iLevelReached = LM_GetScriptArgument(1, "N")
end

-- |[ ============ Variable Setup ============ ]|
-- |[Enumerations]|
local gciNoSkillbooks = 0

-- |[Encapsulation]|
local zEntry = {}

-- |[Basic Variables]|
--Character Variables.
zEntry.sCharacterFormVarPath = "Root/Variables/Global/Christine/sForm"
zEntry.sSkillbookVarPath     = "Root/Variables/Global/Christine/iSkillbookTotal"
zEntry.zaJobChart            = gzaChristineJobChart
zEntry.sStatProfilePath      = gsRoot .. "Combat/Party/Christine/400 Common Stat Profile.lua"
zEntry.sSecondaryMenuPath    = gsRoot .. "Combat/Party/Christine/200 Build Secondary Menu.lua"

--Class Variables.
zEntry.sClassName   = "Prestige"
zEntry.sFormName    = "Human"
zEntry.sCostumeName = "Christine_Human"
zEntry.fFaceIndexX  = 0
zEntry.fFaceIndexY  = 4

-- |[Ability Variables]|
--Purchaseable abilities.
zEntry.saExternalAbilities = {}
table.insert(zEntry.saExternalAbilities, "Master Brawler")
table.insert(zEntry.saExternalAbilities, "Master Commissar")
table.insert(zEntry.saExternalAbilities, "Master Drone")
table.insert(zEntry.saExternalAbilities, "Master Handmaiden")
table.insert(zEntry.saExternalAbilities, "Master Lancer")
table.insert(zEntry.saExternalAbilities, "Master Live Wire")
table.insert(zEntry.saExternalAbilities, "Master Menialist")
table.insert(zEntry.saExternalAbilities, "Master Repair Unit")
table.insert(zEntry.saExternalAbilities, "Master Starseer")
table.insert(zEntry.saExternalAbilities, "Master Thunder Rat")

--Class-equipped abilities.
zEntry.saInternalAbilities = {}

--Purchaseable abilities, in the order of purchase.
zEntry.saPurchaseAbilities = {}
table.insert(zEntry.saPurchaseAbilities, "Prestige|Master Brawler")
table.insert(zEntry.saPurchaseAbilities, "Prestige|Master Commissar")
table.insert(zEntry.saPurchaseAbilities, "Prestige|Master Drone")
table.insert(zEntry.saPurchaseAbilities, "Prestige|Master Handmaiden")
table.insert(zEntry.saPurchaseAbilities, "Prestige|Master Lancer")
table.insert(zEntry.saPurchaseAbilities, "Prestige|Master Live Wire")
table.insert(zEntry.saPurchaseAbilities, "Prestige|Master Menialist")
table.insert(zEntry.saPurchaseAbilities, "Prestige|Master Repair Unit")
table.insert(zEntry.saPurchaseAbilities, "Prestige|Master Starseer")
table.insert(zEntry.saPurchaseAbilities, "Prestige|Master Thunder Rat")

-- |[Skillbar Variables]|
--Skills that always appear on the skillbar. Format: {sAbilityName, iXPos, iYPos, iSkillbooks} (eg: {"Common|Attack", 0, 0, 0}, {"Common|Parry", 1, 0, 0} {})
zEntry.zaSkillbarAbilities = {}

-- |[ ============ Call Standard ============= ]|
--Add to the global stack. There may be multiple executions of this script.
table.insert(gzJobCallStack, zEntry)

--Call.
LM_ExecuteScript(gsStandardJobPath, iSwitchType, iLevelReached)

--Pop the global stack.
table.remove(gzJobCallStack)
