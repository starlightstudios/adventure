-- |[ ======================================= Bend Space ======================================= ]|
-- |[Description]|
--Field ability, disables enemy viewcones for a few seconds.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Ability Prototype =================================== ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.Starseer.BendSpace == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Starseer", "Bend Space", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Special", "Christine|Eclipse", "Unequippable")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Unlocks the Bend Space field ability, disabling enemy sight[BR]for a short time."
    zAbiStruct.sSimpleDescMarkdown  = "Unlocks the Bend Space field ability.[BR]Enemies will be unable to see you at all.[BR][BR][BR]Field abilities can be seen in the top left corner[BR]when out of battle."

    -- |[Usability Variables]|
    zAbiStruct.bNeverAvailable = true --Field abilities have no usability requirements.
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Christine.Starseer.BendSpace = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.Starseer.BendSpace

--Call standard.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
