-- |[ ======================================= Supernova ======================================== ]|
-- |[Description]|
--Hits all enemies for big fire damage. Costs CP.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.Starseer.Pincushion == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Starseer", "Supernova", "$SkillName", gciJP_Cost_Advanced, gbIsNotFreeAction, "Direct", "Combo", "Christine|Supernova", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\nCannot miss.\n\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Blast the enemy with the fury of a dying star.\nInflicts massive flame damage, all enemies.\n\n\nCannot miss.\nCosts [CPCost][CPIco](CP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, 6, giNoCooldown, "Target Enemies All", giNoCPGeneration)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Flames")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Flaming, 5, 4.20)
    
    --Ensuring it always hits
    zAbiStruct.zExecutionAbiPackage:fnAlwaysHit()
        
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Special|Ability:Supernova"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.Christine.Starseer.Pincushion = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.Starseer.Pincushion

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
