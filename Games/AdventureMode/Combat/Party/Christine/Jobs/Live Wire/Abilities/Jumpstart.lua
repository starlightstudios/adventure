-- |[ ======================================= Jumpstart ======================================== ]|
-- |[Description]|
--Heals one ally to full, gives them "Fast" until the next turn, revives from KO. Positive charge.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.LiveWire.Jumpstart == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Live Wire", "Jumpstart", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Heal", "Active", "Christine|Jumpstart", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Restore]. [Target].\n\n[FEffect]'Fast' until next turn.\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Give them a jump if they're frozen up!\nMajorly heals one ally, and can revive\nfrom KO.\nHealing increases with attack power.\nAlso gives them [Fast] until the next turn.\nCosts [CPCost][CPIco](CP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, 3, giNoCooldown, "Target Allies Single Option Downed", 0)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Healing")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsHealing(piHealingBase, pfHealingScaleFactor, pfHealingPctOfHPMax)
    zAbiStruct.zExecutionAbiPackage:fnSetAsHealing(50, 2.50, 0)
    
    -- |[Effect Package]|
  --zAbiStruct:fnAddBuff(psPrototypeName, $psApplyText, $psApplyColorString, $psApplyAnim, $psApplySound)
    zAbiStruct:fnAddBuff("Christine.LiveWire.Jumpstart", "Fast!", "Color:Purple")
    
    --Custom resist text
    zAbiStruct.zExecutionEffPackage.sResistText = "Failed!"
    
    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName

    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
        local saDescription = {}
        saDescription[1] = "It kicks like a mule!"
        saDescription[2] = "[Fast], act first next round."
      --EffectList:fnCreateBuffPrototype(psName, psDisplayName, psIcon, piTurns, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreateBuffPrototype(sLocalPrototypeName, "Jumpstart", "Christine|Jumpstart", 1, "", saDescription, {{"Fast", 1}, {"Is Positive", 1}})
    end

    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Ability:Jumpstart"
    
    -- |[Finalize]|
    --Running description markdown manually to avoid calling standard prediction creation
    zAbiStruct:fnRunDescriptionMarkdowns()
    
    --Healing Module for prediction
    zAbiStruct.zPredictionPackage.bHasHealingModule = true
    zAbiStruct.zPredictionPackage.zHealingModule.iHealingFixed = 50
    zAbiStruct.zPredictionPackage.zHealingModule.fHealingFactor = 2.50
    
    --Create a 'Fast' effect module for prediction
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "[IMG0] Fast / 1[IMG1]"
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {"Root/Images/AdventureUI/DebuffIcons/EffectFriendly", "Root/Images/AdventureUI/DebuffIcons/Clock"}
    
    --Add charge indicator to prediction
    zAbiStruct.zPredictionPackage.zaAdditionalLines[2] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[2].sString   = "Creates Positive Charge"
    zAbiStruct.zPredictionPackage.zaAdditionalLines[2].saSymbols = {}

    
    --Set prototype.
    gzPrototypes.Combat.Christine.LiveWire.Jumpstart = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.LiveWire.Jumpstart

--Electric charge abilities use this handler instead of calling the standard script.
fnApplyPositiveCharge(iSwitchType)
