-- |[ ======================================== Grounded ======================================== ]|
-- |[Description]|
--Increases shock resist by 10 for one ally. Negative charge.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.LiveWire.Grounded == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Live Wire", "Grounded", "$SkillName", gciJP_Cost_Normal, gbIsFreeAction, "Buff", "Active", "Christine|Grounded", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "+10 [Shk] resistance / 3 [Turns].\n\n\n\n\nFree Action. Negative Charge.\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Protect one ally from harmful shocks.\nBuffs [Shk](Shock) Resist for 3 turns.\nNegative Charge.\n\nFree Action.\nCosts [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(15, giNoCPCost, giNoCooldown, "Target Allies Single", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Buff")
    zAbiStruct.zExecutionAbiPackage:fnSetAsBuff()
    
    -- |[Effect Package]|
  --zAbiStruct:fnAddBuff(psPrototypeName, $psApplyText, $psApplyColorString, $psApplyAnim, $psApplySound)
    zAbiStruct:fnAddBuff("Christine.LiveWire.Grounded", "Grounded!", "Color:Purple")
    
    --Custom resist text
    zAbiStruct.zExecutionEffPackage.sResistText = "Failed!"
    
    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName

    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
        local saDescription = {}
        saDescription[1] = "Protected from high voltage."
        saDescription[2] = "Increases [Shk](Shock) resistance."
      --EffectList:fnCreateBuffPrototype(psName, psDisplayName, psIcon, piTurns, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreateBuffPrototype(sLocalPrototypeName, "Grounded", "Christine|Grounded", 3, "ResShk|Flat|10", saDescription, {{"Is Positive", 1}})
    end

    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Ability:Grounded"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Adjusting prediction to keep parity with previous version
    zAbiStruct.zPredictionPackage.zaEffectModules[1].sResultScript = "+10 [IMG1] Res / 3 [IMG2]"
    zAbiStruct.zPredictionPackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/StatisticIcons/Shocking", "Root/Images/AdventureUI/DebuffIcons/Clock"}
    
    --Add charge indicator to prediction
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "Creates Negative Charge"
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
    
    --Set prototype.
    gzPrototypes.Combat.Christine.LiveWire.Grounded = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.LiveWire.Grounded

--Electric charge abilities use this handler instead of calling the standard script.
fnApplyNegativeCharge(iSwitchType)
