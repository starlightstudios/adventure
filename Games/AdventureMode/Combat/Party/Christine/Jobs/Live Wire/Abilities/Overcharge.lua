-- |[ ======================================= Overcharge ======================================= ]|
-- |[Description]|
--Increases attack power by 25% for 3 turns, one ally. Positive charge.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.LiveWire.Overcharge == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Live Wire", "Overcharge", "$SkillName", gciJP_Cost_Normal, gbIsFreeAction, "Buff", "Active", "Christine|Overcharge", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "+25%% [Atk] / 3 [Turns]. [Target].\n\n\n\n\nFree Action. Positive Charge.\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Give your teammates a jolt!\nBuffs attack for one ally, low [MPIco](MP) cost.\nPositive Charge.\n\nFree Action.\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(5, giNoCPCost, 1, "Target Allies Single", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Buff")
    zAbiStruct.zExecutionAbiPackage:fnSetAsBuff()
    
    -- |[Effect Package]|
  --zAbiStruct:fnAddBuff(psPrototypeName, $psApplyText, $psApplyColorString, $psApplyAnim, $psApplySound)
    zAbiStruct:fnAddBuff("Christine.LiveWire.Overcharge", "Energized!", "Color:Purple")
    
    --Custom resist text
    zAbiStruct.zExecutionEffPackage.sResistText = "Failed!"
    
    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName

    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
        local saDescription = {}
        saDescription[1] = "Brimming with energy!"
        saDescription[2] = "Increases [Atk](Power)."
      --EffectList:fnCreateBuffPrototype(psName, psDisplayName, psIcon, piTurns, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreateBuffPrototype(sLocalPrototypeName, "Overcharge", "Christine|Overcharge", 3, "Atk|Pct|0.25", saDescription, {{"Is Positive", 1}})
    end
    
    --Create an Effect module
    --zAbiStruct.zPredictionPackage.zaEffectModules[1] = fnCreateEffectModule()
    --zAbiStruct.zPredictionPackage.zaEffectModules[1].bIsBuff = true
    --zAbiStruct.zPredictionPackage.zaEffectModules[1].sResultScript = "+25%% [IMG1] / 3 [IMG2]"
    --zAbiStruct.zPredictionPackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/StatisticIcons/Attack", "Root/Images/AdventureUI/DebuffIcons/Clock"}
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Ability:Overcharge"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Add charge indicator to prediction
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "Creates Positive Charge"
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
    
    --Set prototype.
    gzPrototypes.Combat.Christine.LiveWire.Overcharge = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.LiveWire.Overcharge

--Electric charge abilities use this handler instead of calling the standard script.
fnApplyPositiveCharge(iSwitchType)
