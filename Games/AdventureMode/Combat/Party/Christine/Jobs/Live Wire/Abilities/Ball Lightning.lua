-- |[ ===================================== Ball Lightning ===================================== ]|
-- |[Description]|
--Field ability, shoots a ball of lightning that goes ZAP and mugs enemies.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Ability Prototype =================================== ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.LiveWire.BallLightning == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Live Wire", "Ball Lightning", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Special", "Christine|BatteryDrainer", "Unequippable")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Unlocks the Ball Lightning field ability, blasting enemies in the[BR]field and mugging them."
    zAbiStruct.sSimpleDescMarkdown  = "Unlocks the Ball Lightning field ability. Zaps[BR]enemies in front of you![BR][BR][BR]Field abilities can be seen in the top left corner[BR]when out of battle."

    -- |[Usability Variables]|
    zAbiStruct.bNeverAvailable = true --Field abilities have no usability requirements.
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Christine.LiveWire.BallLightning = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.LiveWire.BallLightning

--Call standard.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
