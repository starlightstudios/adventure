-- |[ ====================================== Mend Chassis ====================================== ]|
-- |[Description]|
--Heals one ally, HoT.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.Drone.MendChassis == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Drone", "Mend Chassis", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Heal", "Active", "Christine|MendChassis", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Restore]. [Target].\n\n\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Regenerative nanites heal one ally over 3\nturns.\nHealing increases with attack power.\n\n\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(20, giNoCPCost, giNoCooldown, "Target Allies Single", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Healing")
    
    --Initial Healing
  --zAbiStruct.zExecutionAbiPackage:fnSetAsHealing(piHealingBase, pfHealingScaleFactor, pfHealingPctOfHPMax)
    zAbiStruct.zExecutionAbiPackage:fnSetAsHealing(20, 0.70, 0)
    
    --Setting up HoT
    zAbiStruct.zExecutionAbiPackage:fnSetAsBuff()
    
    --Cannot crit.
    zAbiStruct.zExecutionAbiPackage.bNeverCrits = true
    
    -- |[Execution Effect Package]|
  --zAbiStruct:fnAddBuff(psPrototypeName, $psApplyText, $psApplyColorString, $psApplyAnim, $psApplySound)
    zAbiStruct:fnAddBuff("Christine.Drone.MendChassis", "Healing!", "Color:Green", "", "Combat\\|Impact_Buff")

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
      --EffectList:fnCreateHoTPrototype(psName, psDisplayName, psIcon, pfHealBase, pfHealFactor, piTurns)
        EffectList:fnCreateHoTPrototype(sLocalPrototypeName, "Mend Chassis", "Christine|MendChassis", 20, 0.70, 3)
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Restore|Ability:MendChassis"

    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.Christine.Drone.MendChassis = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.Drone.MendChassis

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
