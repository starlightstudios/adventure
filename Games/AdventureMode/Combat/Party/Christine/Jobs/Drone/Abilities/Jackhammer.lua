-- |[ ======================================= Jackhammer ======================================= ]|
-- |[Description]|
--Randomly strikes three times for .60x damage, total 1.80x. Base accuracy is 60%, +10% per enemy
-- on the field. Damage goes up by 0.05x per enemy on the field. Triggers Spearguard.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.Drone.Jackhammer == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Drone", "Jackhammer", "$SkillName", gciJP_Cost_Advanced, gbIsNotFreeAction, "Direct", "Active", "Christine|Jackhammer", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. 3 random targets.\n[BaseHit], +10 [Acc] per enemy on field.\n+5%% [Atk] per enemy on field.\n\n\nTriggers Spearguard.\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Attack three times at reduced damage. Damage\nand accuracy increase the bigger the enemy\nparty is.\n\nTriggers Spearguard.\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(20, giNoCPCost, giNoCooldown, "Target Enemies All", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Weapon")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_UseWeapon, 40, 0.60)
    
    -- |[Execution Self-Effect Package - Spearguard]|
    --Basic package.
    zAbiStruct.zExecutionSelfPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides for Spearguard.
    zAbiStruct.zExecutionSelfPackage.sEffectPath     = fnResolvePath() .. "../../Common/Effects/Spearguard.lua"
    zAbiStruct.zExecutionSelfPackage.sEffectCritPath = fnResolvePath() .. "../../Common/Effects/Spearguard.lua"
    zAbiStruct.zExecutionSelfPackage.sApplyText = "Spearguard"
    zAbiStruct.zExecutionSelfPackage.sApplyTextCol = "Color:White"
    zAbiStruct.zExecutionSelfPackage.sApplyAnimation = "Null"
    zAbiStruct.zExecutionSelfPackage.sApplySound = "Combat\\|Impact_Buff"
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Offense|General:Swing|Ability:Jackhammer"
    
    -- |[Finalize]|
    --Running description markdown manually to avoid calling standard prediction creation
    zAbiStruct:fnRunDescriptionMarkdowns()
    
    --Custom prediction for parity with original
    zAbiStruct.zPredictionPackage.bHasHitRateModule = true
    zAbiStruct.zPredictionPackage.zHitRateModule.iMissRate = 40
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "Random target, 0.60 x[IMG0] 3 times. +Dam/Acc per enemy party size."
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {"Root/Images/AdventureUI/StatisticIcons/Attack"}
    
    --Add a manual line to the prediction to reflect Spearguard.
    zAbiStruct.zPredictionPackage.zaAdditionalLines[2] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[2].sString   = "Triggers Spearguard"
    zAbiStruct.zPredictionPackage.zaAdditionalLines[2].saSymbols = {}
    
    --Set prototype.
    gzPrototypes.Combat.Christine.Drone.Jackhammer = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.Drone.Jackhammer

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
if(iSwitchType == gciAbility_Execute) then

    -- |[ ========== Originator Statistics ========== ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        fnModifyCP(gzRefAbility.iCPGain - gzRefAbility.iRequiredCP)
        fnModifyMP(gzRefAbility.iRequiredMP * -1)
    DL_PopActiveObject()
    
    -- |[ ========== Ability Package ========== ]|
    --Modify the damage and miss thresholds based on how many enemies there are.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    gzRefAbility.zExecutionAbiPackage.iMissThreshold = 40   - (10   * (iTargetsTotal - 1))
    gzRefAbility.zExecutionAbiPackage.fDamageFactor  = 0.60 + (0.10 * (iTargetsTotal - 1))
    
    --Execute the same attack three times.
    for i = 1, 3, 1 do
    
        --Common.
        gzRefAbility.zExecutionAbiPackage.iOriginatorID  = iOriginatorID
    
        --Pick one of the painted targets at random. It can be the same target twice.
        local iTargetSlot = LM_GetRandomNumber(0, iTargetsTotal-1)
    
        --Set the combat timer offset. This allows multiple hits to go through, but makes them not simultaneous.
        giCombatTimerOffset = 15 * (i-1)
        
        --Run.
        fnStandardExecution(iOriginatorID, iTargetSlot, gzRefAbility.zExecutionAbiPackage, nil)
    end

    --Clean up.
    gzRefAbility.zExecutionAbiPackage.iMissThreshold = 40
    gzRefAbility.zExecutionAbiPackage.fDamageFactor  = 0.60
    
    --Copied self-effect execution code from standard call to ensure Spearguard triggers
    if(gzRefAbility.zExecutionSelfPackage ~= nil) then
        gzRefAbility.zExecutionSelfPackage.fnHandler(iOriginatorID, iOriginatorID, 0, gzRefAbility.zExecutionSelfPackage, true, false)
    end

-- |[ ==================================== Standard Handler ==================================== ]|
--All other cases, call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end