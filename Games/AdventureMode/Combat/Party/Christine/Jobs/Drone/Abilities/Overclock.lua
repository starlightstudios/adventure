-- |[ ======================================= Overclock ======================================== ]|
-- |[Description]|
--Greatly increases self attack power at the cost of MP regeneration.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.Drone.Overclock == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Drone", "Overclock", "$SkillName", gciJP_Cost_Normal, gbIsFreeAction, "Buff", "Active", "Christine|Overclock", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "+80%% [Atk], -15 [MPT] for 3 [Turns]. [Target].\n\n\n\n\nFree Action.\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Focus everything into pure power!\nGreatly buffs attack power, but handicaps\n[MPIco](MP) generation.\nSelf-target.\n\nFree Action."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, giNoCPCost, giNoCooldown, "Target Self", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Buff")
    zAbiStruct.zExecutionAbiPackage:fnSetAsBuff()
    
    -- |[Effect Package]|
  --zAbiStruct:fnAddBuff(psPrototypeName, $psApplyText, $psApplyColorString, $psApplyAnim, $psApplySound)
    zAbiStruct:fnAddBuff("Christine.Drone.Overclock", "Overclocked!", "Color:Purple")
    
    --Manual override for custom resist text
    zAbiStruct.zExecutionEffPackage.sResistText = "Failed!"
    
        -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName

    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
        local saDescription = {}
        saDescription[1] = "Focus your processor on power!"
        saDescription[2] = "Increases [Atk](Power) but decreases"
        saDescription[3] = "[Man](MP) generation."
      --EffectList:fnCreateBuffPrototype(psName, psDisplayName, psIcon, piTurns, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreateBuffPrototype(sLocalPrototypeName, "Overclock", "Christine|Overclock", 3, "Atk|Pct|0.80 MPReg|Flat|-15", saDescription, {{"Is Positive", 1}})
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Ability:Overclock"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.Christine.Drone.Overclock = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.Drone.Overclock

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
