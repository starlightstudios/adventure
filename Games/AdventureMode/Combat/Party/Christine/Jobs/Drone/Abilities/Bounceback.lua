-- |[ ======================================= Bounceback ======================================= ]|
-- |[Description]|
--Counterattacks once per turn if Spearguard is active.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.Drone.Bounceback == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Drone", "Bounceback", "$SkillName", gciJP_Cost_Passive, gbIsNotFreeAction, "Buff", "Passive", "Christine|Bounceback", "Counterattack")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "If Spearguard is active, counterattack up to 1 enemy[BR]per turn.[BR][BR][BR][BR][BR]Passive."
    zAbiStruct.sSimpleDescMarkdown  = "Rubber can bounce back to its original shape![BR]Counterattacks up to one enemy per turn if[BR]Spearguard is active.[BR][BR][BR]Passive."
    
    -- |[Passive Effect]|
    zAbiStruct.bNeverAvailable   = true     --Passive abilities have no usability requirements.
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Weapon")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Ability:Bounceback"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.Christine.Drone.Bounceback = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.Drone.Bounceback

-- |[ ================================== Combat: Combat Begins ================================= ]|
--Setup variables.
if(iSwitchType == gciAbility_BeginCombat) then

    --Variables.
    local iAbilityID = RO_GetID()
    DL_AddPath("Root/Variables/Combat/" .. iAbilityID .. "/")
    VM_SetVar("Root/Variables/Combat/" .. iAbilityID .. "/iExecThisTurn", "N", 0.0)
    VM_SetVar("Root/Variables/Combat/" .. iAbilityID .. "/iTargetID", "N", 0.0)

-- |[ =================================== Combat: Turn Begins ================================== ]|
--Reset execution at the start of each turn.
elseif(iSwitchType == gciAbility_BeginTurn) then
    local iAbilityID = RO_GetID()
    VM_SetVar("Root/Variables/Combat/" .. iAbilityID .. "/iExecThisTurn", "N", 0.0)
    --io.write("->Bounceback resets for new turn. Ability ID is: " .. iAbilityID .. "\n")
    
-- |[ ============================= Combat: Paint Targets Response ============================= ]|
--When attacked, store the attacker's ID.
elseif(iSwitchType == gciAbility_PaintTargetsResponse) then
    local iAbilityID = RO_GetID()
    local iTargetID  = VM_GetVar("Root/Variables/Combat/" .. iAbilityID .. "/iTargetID", "N")
    AdvCombat_SetProperty("Create Target Cluster", "Bounceback Cluster", "Attacker")
    AdvCombat_SetProperty("Add Target To Cluster", "Bounceback Cluster", iTargetID)

-- |[ ================================== Combat: Can Execute =================================== ]|
--Ability needs to be sure the target is still alive.
elseif(iSwitchType == gciAbility_QueryCanRun) then

    -- |[Variables]|
    --Get variables.
    local iAbilityID = RO_GetID()
    local iOwnerID  = VM_GetVar("Root/Variables/Combat/" .. iAbilityID .. "/iOwnerID", "N")
    local iTargetID = VM_GetVar("Root/Variables/Combat/" .. iAbilityID .. "/iTargetID", "N")

    -- |[Originator Alive]|
    AdvCombat_SetProperty("Push Event Originator")
        local bCanAct = AdvCombatEntity_GetProperty("Can Act")
    DL_PopActiveObject()
    if(bCanAct == false) then return end
    
    -- |[Target Alive Check]|
    --The target has to be, y'know, alive. Otherwise, don't attack it.
    AdvCombat_SetProperty("Push Entity By ID", iTargetID)
        local iHealth = AdvCombatEntity_GetProperty("Health")
    DL_PopActiveObject()
    if(iHealth < 1) then return end
    
    -- |[All Checks Passed]|
    AdvCombat_SetProperty("Set Event Can Run", true)
    
    --Flag the execution.
    VM_SetVar("Root/Variables/Combat/" .. iAbilityID .. "/iExecThisTurn", "N", 1.0)
    
-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Counterattack the enemy who attacked the owner.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[ ========== Originator Statistics ========== ]|
    --Ability's internal ID.
    local iAbilityID = RO_GetID()
    
    --Get originator.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
    DL_PopActiveObject()
    
    --Set the weapon animation values by using the weapon values. If any overrides are found, use those.
    local sAttackAnimation, sAttackSound, sCriticalSound = fnStandardWeaponAnimResolve(iOriginatorID)
    
    -- |[ ========== Ability Package ========== ]|
    --Store the ID of the user.
    gzRefAbility.zExecutionAbiPackage.iOriginatorID = iOriginatorID
    
    -- |[ ==================== For Each Target =================== ]|
    --Attack painted targets.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        fnStandardExecution(iOriginatorID, i-1, gzRefAbility.zExecutionAbiPackage, nil)
    end
    
-- |[ ================================== Combat: Event Queued ================================== ]|
--Whenever an ability is enqueued, this fires to check if we need to counterattack it.
elseif(iSwitchType == gciAbility_EventQueued) then

    -- |[Execution Checks]|
    --Check if the ability already executed this turn. This ability can fire once per turn.
    local iAbilityID = RO_GetID()
    local iExecThisTurn = VM_GetVar("Root/Variables/Combat/" .. iAbilityID .. "/iExecThisTurn", "N")
    if(iExecThisTurn == 1.0) then
        return
    end
    
    --Debug.
    --io.write("Running bounceback check.\n")
    
    --Check the ability owner to see if they have a [Spearguard] tag active.
    local iOwnerID = AdvCombatAbility_GetProperty("Owner ID")
    AdvCombat_SetProperty("Push Entity By ID", iOwnerID)
        local iTagCount = AdvCombatEntity_GetProperty("Tag Count", "Spearguard")
    DL_PopActiveObject()
    if(iTagCount < 1) then 
        return 
    end
    
    --Debug.
    --io.write(" Spearguard tag found on owner.\n")
    --io.write(" Owner ID: " .. iOwnerID .. "\n")
    
    -- |[Variables]|
    --Get the ID of the owner of this ability.
    local iOwnerGroup = AdvCombat_GetProperty("Party Of ID", iOwnerID)
    
    --Get the originator, and check if it's a hostile entity.
    local iOriginatorID = AdvCombat_GetProperty("Query Originator ID")
    local iOriginatorGroup = AdvCombat_GetProperty("Party Of ID", iOriginatorID)
    
    -- |[Party Check]|
    --To be valid, the owner must be in a different party than the originator, and both must be in either
    -- the player's active party or the enemy's active party.
    if(iOwnerGroup == iOriginatorGroup) then
        return
    end
    if(iOwnerGroup ~= gciACPartyGroup_Party and iOwnerGroup ~= gciACPartyGroup_Enemy) then
        return
    end
    if(iOriginatorGroup ~= gciACPartyGroup_Party and iOriginatorGroup ~= gciACPartyGroup_Enemy) then
        return
    end
    
    --Debug.
    --io.write(" Owner and actor are opposed.\n")
    
    -- |[Target Cluster Check]|
    --Check the target cluster. If the owner is found in the target cluster, we can counterattack.
    local bIsInTargetCluster = false
    local iTargetsTotal = AdvCombat_GetProperty("Query Targets Total")
    --io.write(" Targets in cluster: " .. iTargetsTotal .. "\n")
    for i = 1, iTargetsTotal, 1 do
        
        --Get the target ID.
        local iTargetID = AdvCombat_GetProperty("Query Targets ID", i-1)
        --io.write("  ID: " .. iTargetID .. "\n")
        
        --If we're being targeted, we can respond immediately.
        if(iTargetID == iOwnerID) then
            bIsInTargetCluster = true
            break
        end
        
        --Alternately, if the target in question is being covered by us, respond.
        if(fnIsTargetCoveredBy(iTargetID, iOwnerID)) then
            bIsInTargetCluster = true
            break
        end
    end
    
    --Not in target cluster, fail.
    if(bIsInTargetCluster == false) then
        return
    end
    
    --Debug.
    --io.write(" Owner was in target cluster.\n")
    
    -- |[Enqueue]|
    VM_SetVar("Root/Variables/Combat/" .. iAbilityID .. "/iTargetID", "N", iOriginatorID)
    AdvCombatAbility_SetProperty("Enqueue This Ability As Event", 1)
    --io.write("Enqueued bounceback as event.\n")
    
    --Debug.
    --io.write(" Counterattack queued.\n")

-- |[ ==================================== Standard Handler ==================================== ]|
--All other cases, call the standard handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end

