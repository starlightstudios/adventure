-- |[ ========================================== Drone ========================================= ]|
--Christine's latex drone class for chapter 5.

-- |[Notes]|
--For gciJob_Create, the AdvCombatEntity that will own the job is the active object. For all other
-- calls, the AdvCombatJob is active and can push the owning entity.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ====================================== Job Creation ====================================== ]|
--Called when the chapter is initialized, the job registers itself to its owner and populates its
-- abilities. This is only called once.
--The owner should be the active object.
if(iSwitchType == gciJob_Create) then
    
    --Create the job and set its stats.
    AdvCombatEntity_SetProperty("Create Job", "Drone")
    
        --Name as it appears in the UI.
        AdvCombatJob_SetProperty("Display Name", "Drone")
        AdvCombatJob_SetProperty("Appears on Skills UI", false)
    
        --Script path to this job
        AdvCombatJob_SetProperty("Script Path", LM_GetCallStack(0))
        
        --Responses.
        AdvCombatJob_SetProperty("Script Response", gciJob_SwitchTo, true)
        AdvCombatJob_SetProperty("Script Response", gciJob_Level, true)
        AdvCombatJob_SetProperty("Script Response", gciJob_Master, true)
        AdvCombatJob_SetProperty("Script Response", gciJob_JobAssembleSkillList, true)
        AdvCombatJob_SetProperty("Script Response", gciJob_BeginCombat, true)
        
        --Display
        local fIndexX = 11
        local fIndexY = 4
        local fLft = (fIndexX+0) * gci_FaceTable_Size
        local fTop = (fIndexY+0) * gci_FaceTable_Size
        local fRgt = (fIndexX+1) * gci_FaceTable_Size
        local fBot = (fIndexY+1) * gci_FaceTable_Size
        AdvCombatJob_SetProperty("Face Table Data", fLft, fTop, fRgt, fBot, "Root/Images/AdventureUI/ItemIcons/CharacterFaces")

    DL_PopActiveObject()
    
    --Register the job's abilities to the entity. Abilities with "JOBNAMEINTERNAL|ABILITYNAME" are abilities
    -- used by the job itself, while "JOBNAME|ABILITYNAME" are those the player can purchase.
    --Even if two such abilities share the same name, they don't have to be the same ability.
    local sJobAbilityPath = fnResolvePath() .. "Abilities/"
    LM_ExecuteScript(sJobAbilityPath .. "Auto Repair.lua",       gciAbility_Create)
    LM_ExecuteScript(sJobAbilityPath .. "Bounceback.lua",        gciAbility_Create)
    LM_ExecuteScript(sJobAbilityPath .. "Encased.lua",           gciAbility_Create)
    LM_ExecuteScript(sJobAbilityPath .. "Inspiring Loyalty.lua", gciAbility_Create)
    LM_ExecuteScript(sJobAbilityPath .. "Jackhammer.lua",        gciAbility_Create)
    LM_ExecuteScript(sJobAbilityPath .. "Mend Chassis.lua",      gciAbility_Create)
    LM_ExecuteScript(sJobAbilityPath .. "Overclock.lua",         gciAbility_Create)
    LM_ExecuteScript(sJobAbilityPath .. "Quickdry Splash.lua",   gciAbility_Create)
    LM_ExecuteScript(sJobAbilityPath .. "Roundhouse Kick.lua",   gciAbility_Create)
    
    --Internal versions.
    LM_ExecuteScript(sJobAbilityPath .. "Roundhouse Kick.lua", gciAbility_CreateSpecial)
    LM_ExecuteScript(sJobAbilityPath .. "Overclock.lua",       gciAbility_CreateSpecial)
    LM_ExecuteScript(sJobAbilityPath .. "Mend Chassis.lua",    gciAbility_CreateSpecial)
    LM_ExecuteScript(sJobAbilityPath .. "Quickdry Splash.lua", gciAbility_CreateSpecial)
    LM_ExecuteScript(sJobAbilityPath .. "Encased.lua",         gciAbility_CreateSpecial)
    LM_ExecuteScript(sJobAbilityPath .. "Jackhammer.lua",      gciAbility_CreateSpecial)
    
    LM_ExecuteScript(sJobAbilityPath .. "Auto Repair.lua",     gciAbility_CreateSpecial)
   
-- |[ ===================================== Job Assumption ===================================== ]|
--Called when the player switches to the class. Clears any existing job properties, sets graphics
-- and stats as needed.
--This can be called during combat, as some characters can switch jobs in-battle.
--The calling AdvCombatEntity is expected to be the active object.
elseif(iSwitchType == gciJob_SwitchTo) then

    --Change images.
    VM_SetVar("Root/Variables/Global/Christine/sForm", "S", "LatexDrone")
    LM_ExecuteScript(gsCostumeAutoresolve, "Christine_LatexDrone")
    
    --Unlock on UI.
    AdvCombatJob_SetProperty("Appears on Skills UI", true)

    --Push the owning entity.
    AdvCombatJob_SetProperty("Push Owner")

        -- |[Display Name]|
        --Use normal display name.
        AdvCombatEntity_SetProperty("Display Name", "DEFAULT")

        -- |[Rebuild Visibility List]|
        AdvCombatEntity_SetProperty("Rebuild Job Skills UI")

        -- |[Statistics]|
        --Get health percentage.
        local fHPPercent = AdvCombatEntity_GetProperty("Health") / AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
        
        --Set job statistics.
        AdvCombatEntity_SetProperty("Compute Level Statistics", -1)
        
        --Apply change in max HP.
        AdvCombatEntity_SetProperty("Health Percent", fHPPercent)
        
        --Clear all relevant ability slots.
        fnClearJobSlots()
        
        -- |[Top Bar]|
        --Set basic actions bar.
        AdvCombatEntity_SetProperty("Set Ability Slot", 0, 0, "Common|Attack")
        AdvCombatEntity_SetProperty("Set Ability Slot", 1, 0, "Common|Discharge")
    
        --4-CP ability.
        AdvCombatEntity_SetProperty("Set Ability Slot", 2, 0, "Drone Internal|Auto Repair")
        
        --Items, Retreat, Surrender, Etc.
        fnPlaceJobCommonSkillsNoItems()
        
        -- |[Class Bar]|
        --Set class-specific bar.
        local iSkillbookTotal = VM_GetVar("Root/Variables/Global/Christine/iSkillbookTotal", "N") 
        AdvCombatEntity_SetProperty("Set Ability Slot", 0, 1, "Drone Internal|Roundhouse Kick")
        AdvCombatEntity_SetProperty("Set Ability Slot", 1, 1, "Drone Internal|Overclock")
        AdvCombatEntity_SetProperty("Set Ability Slot", 2, 1, "Common|Locked")
        AdvCombatEntity_SetProperty("Set Ability Slot", 0, 2, "Common|Locked")
        AdvCombatEntity_SetProperty("Set Ability Slot", 1, 2, "Common|Locked")
        AdvCombatEntity_SetProperty("Set Ability Slot", 2, 2, "Common|Locked")
    
        --Unlocked abilities.
        if(iSkillbookTotal >= 1.0) then
            AdvCombatEntity_SetProperty("Set Ability Slot", 2, 1, "Drone Internal|Mend Chassis")
        end
        if(iSkillbookTotal >= 2.0) then
            AdvCombatEntity_SetProperty("Set Ability Slot", 0, 2, "Drone Internal|Quickdry Splash")
        end
        if(iSkillbookTotal >= 3.0) then
            AdvCombatEntity_SetProperty("Set Ability Slot", 1, 2, "Drone Internal|Encased")
        end
        if(iSkillbookTotal >= 4.0) then
            AdvCombatEntity_SetProperty("Set Ability Slot", 2, 2, "Drone Internal|Jackhammer")
        end
        
        -- |[Job Changes]|
        LM_ExecuteScript(gsRoot .. "Combat/Party/Christine/200 Build Secondary Menu.lua")
        
    DL_PopActiveObject()

-- |[ ====================================== Job Level Up ====================================== ]|
--Called when the job reaches a level. The second argument is the level in question. Remember that
-- level 1 on the UI is level 0 in the scripts.
--When assuming the job, this is called by the program to reset stats. There is no need to call it
-- during the Job Assumption part of this script.
elseif(iSwitchType == gciJob_Level) then

    -- |[Setup]|
    --Argument check.
    if(iArgumentsTotal < 2) then 
        Debug_ForcePrint("Error: No level specified for Job Level Up. " .. LM_GetCallStack(0) .. "\n") 
        return
    end

    --First argument is what level we reached.
    local iLevelReached = LM_GetScriptArgument(1, "N")
    
    --Common script.
    fnChartJobStatHandler(gzaChristineJobChart, "Drone", iLevelReached, gsRoot .. "Combat/Party/Christine/400 Common Stat Profile.lua")
    
-- |[ ====================================== Job Mastered ====================================== ]|
--Called when the player masters the class and gets the mastery bonus applied. Also called when the
-- character changes class and has the mastery bonus applied to that class.
elseif(iSwitchType == gciJob_Master) then
    if(AdvCombatJob_GetProperty("Is Mastered") == true) then
        local sMasteryPath = gsRoot .. "Combat/Party/Christine/401 Mastery Bonus Handler.lua"
        LM_ExecuteScript(sMasteryPath, "Prestige Unlock")
    end
    
-- |[ =================================== Assemble Skill List ================================== ]|
--Once all abilities are registered to the parent entity, builds a list of which skills are associated
-- with this job. These skills show up on the UI and can be purchased by the player. Note that a job
-- can contain the same ability as another job. Purchasing it in either will unlock both versions.
-- This is not explicitly a problem, it means the same ability can be bought with JP from multiple
-- jobs. It is not part of the standard for Adventure Mode though.
elseif(iSwitchType == gciJob_JobAssembleSkillList) then

    --Setup.
    local sJobName = "Drone"
    
    --Register abilities in the order they should appear in the skills menu.
    AdvCombatEntity_SetProperty("Register Ability To Job", "Drone|Roundhouse Kick",   sJobName)
    AdvCombatEntity_SetProperty("Register Ability To Job", "Drone|Quickdry Splash",   sJobName)
    AdvCombatEntity_SetProperty("Register Ability To Job", "Drone|Jackhammer",        sJobName)
    AdvCombatEntity_SetProperty("Register Ability To Job", "Drone|Mend Chassis",      sJobName)
    AdvCombatEntity_SetProperty("Register Ability To Job", "Drone|Overclock",         sJobName)
    AdvCombatEntity_SetProperty("Register Ability To Job", "Drone|Auto Repair",       sJobName)
    AdvCombatEntity_SetProperty("Register Ability To Job", "Drone|Bounceback",        sJobName)
    AdvCombatEntity_SetProperty("Register Ability To Job", "Drone|Encased",           sJobName)
    AdvCombatEntity_SetProperty("Register Ability To Job", "Drone|Inspiring Loyalty", sJobName)

-- |[ ====================================== Combat Start ====================================== ]|
--Called when combat begins. The job should populate passive abilities here. The AdvCombatEntity
-- will be atop the activity stack.
elseif(iSwitchType == gciJob_BeginCombat) then
    AdvCombatJob_SetProperty("Push Owner")
        fnPlaceJobCommonSkills()
        fnPlaceJobCommonSkillsCombatStart()
    DL_PopActiveObject()
    
-- |[ ==================================== Combat: New Turn ==================================== ]|
--Called when turn order is sorted and a new turn begins.
elseif(iSwitchType == gciJob_BeginTurn) then

-- |[ =================================== Combat: New Action =================================== ]|
--Called when an action begins. Note that the action is not necessarily that of the active entity.
elseif(iSwitchType == gciJob_BeginAction) then
    
-- |[ ========================= Combat: New Action (After Free Action) ========================= ]|
--Called when an action begins after a Free Action has expired.
elseif(iSwitchType == gciJob_BeginFreeAction) then

-- |[ =================================== Combat: End Action =================================== ]|
--Called when an action ends. As above, not necessarily that of the active entity.
elseif(iSwitchType == gciJob_EndAction) then

-- |[ ==================================== Combat: End Turn ==================================== ]|
--Called when a turn ends, before turn order is determined for the next turn.
elseif(iSwitchType == gciJob_EndTurn) then

-- |[ =================================== Combat: End Combat =================================== ]|
--Called when combat ends for any reason. This includes, victory, defeat, retreat, or other.
elseif(iSwitchType == gciJob_EndCombat) then

-- |[ ================================== Combat: Event Queued ================================== ]|
--Called when a main event is enqueued. An entity typically performs one event per action. This is
-- called before after Entity but before Abilities respond for this character.
elseif(iSwitchType == gciJob_EventQueued) then

end
