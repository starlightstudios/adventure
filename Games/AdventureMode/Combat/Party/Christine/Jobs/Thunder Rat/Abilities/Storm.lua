-- |[ ========================================== Storm ========================================= ]|
-- |[Description]|
--Randomly strikes five times with 35% power. Negative charge.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.ThunderRat.Storm == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Thunder Rat", "Storm", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "Christine|Storm", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. Up to 5 random targets.\n[BaseHit].\n\nNumber of strikes increases with enemy count.\n\nNegative charge.\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Hits up to five targets at random with\nlightning for [Shk](Shock) damage.\nDeals reduced damage per strike, strikes\nmore times the more targets there are.\nNegative charge.\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(20, giNoCPCost, giNoCooldown, "Target Enemies All", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Shock")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Shocking, 5, 0.35)

    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Ability:Storm"
    
    -- |[Finalize]|
    --Running description markdown manually to avoid calling standard prediction creation
    zAbiStruct:fnRunDescriptionMarkdowns()
    
    --Custom prediction for parity with original
    zAbiStruct.zPredictionPackage.bHasHitRateModule = true
    zAbiStruct.zPredictionPackage.zHitRateModule.iMissRate = 5
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "[IMG0] damages randomly, 0.35 x[IMG1]"
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {"Root/Images/AdventureUI/DamageTypeIcons/Shocking", "Root/Images/AdventureUI/StatisticIcons/Attack"}
    
    --Add charge indicator to prediction
    zAbiStruct.zPredictionPackage.zaAdditionalLines[2] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[2].sString   = "Creates Negative Charge"
    zAbiStruct.zPredictionPackage.zaAdditionalLines[2].saSymbols = {}
    
    --Set prototype.
    gzPrototypes.Combat.Christine.ThunderRat.Storm = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.ThunderRat.Storm

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
if(iSwitchType == gciAbility_Execute) then

    -- |[ ========== Originator Statistics ========== ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        
        --Get properties.
        local iOriginatorID = RO_GetID()
        local iPositiveTags = AdvCombatEntity_GetProperty("Tag Count", "Positive Charge")
        local iNegativeTags = AdvCombatEntity_GetProperty("Tag Count", "Negative Charge")
        
        --Set stat changes.
        fnModifyCP(gzRefAbility.iCPGain)
        fnModifyMP(gzRefAbility.iRequiredMP * -1)
        fnModifyCP(gzRefAbility.iRequiredCP * -1)

    DL_PopActiveObject()
    
    -- |[ ========== Ability Package ========== ]|
    --Determine how many times to shock. The minimum is 2. The roll needed for each strike decreases
    -- with each enemy on the field, but the roll is always out of 100.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    local iAmtPerStrike = 30
    if(iTargetsTotal == 1) then
        
    elseif(iTargetsTotal == 2) then
        iAmtPerStrike = 25
    elseif(iTargetsTotal == 3) then
        iAmtPerStrike = 20
    elseif(iTargetsTotal == 4) then
        iAmtPerStrike = 12
    elseif(iTargetsTotal == 5) then
        iAmtPerStrike = 8
    else
        iAmtPerStrike = 5
    end
    
    --Perform roll.
    local iRoll = LM_GetRandomNumber(1, 100)
    local iStrikes = 2 + math.floor(iRoll / iAmtPerStrike)
    
    --Clamp.
    if(iStrikes < 2) then iStrikes = 2 end
    if(iStrikes > 5) then iStrikes = 5 end
    
    
    -- |[ ==================== For Each Strike =================== ]|
    --Store max number of spent ticks.
    local iMaxTicks = 0
    
    --Execute.
    local zaPackages = {}
    for i = 1, iStrikes, 1 do
    
        --Pick one of the painted targets at random. It can be the same target twice.
        local iTargetSlot = LM_GetRandomNumber(0, iTargetsTotal-1)
    
        --Set the combat timer offset. This allows multiple hits to go through, but makes them not simultaneous.
        giCombatTimerOffset = 15 * (i-1)
        
        --Run.
        local iTicksExpended = fnStandardExecution(iOriginatorID, iTargetSlot, gzRefAbility.zExecutionAbiPackage, nil)
        if(iTicksExpended > iMaxTicks) then iMaxTicks = iTicksExpended end
    end

    --Timer setup.
    local iStartTicks = iMaxTicks

    -- |[ ================ Apply Negative Charge ================= ]|
    --First, check if the owner has any positive charges. If so, issue a discharge.
    if(iPositiveTags > 0) then

        --Find all effects with the [Positive Charge] tag and remove them.
        AdvCombat_SetProperty("Push Event Originator")
            local iTotalChargeEffects = AdvCombatEntity_GetProperty("Effects With Tag", "Positive Charge")
            for i = 0, iTotalChargeEffects-1, 1 do
                local iEffectID = AdvCombatEntity_GetProperty("Effect ID With Tag", "Positive Charge", i)
                AdvCombat_SetProperty("Remove Effect", iEffectID)
            end
        DL_PopActiveObject()
    
        --Find all combat entities on the opposite team as the user.
        local iPartyAffiliation = AdvCombat_GetProperty("Party Of ID", iOriginatorID)
        local sSizeFunction = "Combat Party Size"
        local sIDFunction = "Combat Party ID"
        if(iPartyAffiliation == gciACPartyGroup_Party) then
            sSizeFunction = "Enemy Party Size"
            sIDFunction = "Enemy ID"
        end

        --Iterate.
        local iPartySize = AdvCombat_GetProperty(sSizeFunction)
        for i = 0, iPartySize-1, 1 do
            
            --Get ID.
            local iPartyID = AdvCombat_GetProperty(sIDFunction, i)
            
            --Compute damage versus target.
            fnStandardDamageType(iOriginatorID, iPartyID, gciDamageType_Shocking, 15, false, 0)
            giStandardDamage = math.floor(giStandardDamage * 0.15)
    
            --Damage is tripled in tourist mode, though it could still be 0 in theory.
            if(bIsTouristMode) then
                giStandardDamage = giStandardDamage * 3.0
            end
            
            --Create a shock attack.
            local iCurTimer = iStartTicks
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iPartyID, iCurTimer, "Play Sound|Combat\\|Impact_Slash")
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iPartyID, iCurTimer, "Create Animation|Shock|AttackAnim"..i)
            iCurTimer = iCurTimer + fnGetAnimationTiming("Shock")
            
            --Damage issuer.
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iPartyID, iCurTimer, "Damage|" .. giStandardDamage)
            iCurTimer = iCurTimer + gciApplication_TextTicks
            
            --Max ticks.
            if(iCurTimer > iMaxTicks) then iMaxTicks = iCurTimer end
            
            --At the end of each segment, buffer the starting timer.
            iStartTicks = iStartTicks + 15
        end
    
        --Timer.
        fnDefaultEndOfApplications(iMaxTicks)
    
    --No positive tags. If we have less than 3 negative charges, issue a new charge.
    elseif(iNegativeTags < 3) then
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, iMaxTicks, "Effect|"..gsRoot.."Combat/Party/Christine/Jobs/Common/Effects/Negative Charge.lua")
    
    --No negatives and no space for new positives.
    else
        --Do nothing
    end

-- |[ ==================================== Standard Handler ==================================== ]|
--All other cases, call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
