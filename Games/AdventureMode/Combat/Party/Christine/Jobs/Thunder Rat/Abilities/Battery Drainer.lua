-- |[ ==================================== Battery Drainer ===================================== ]|
-- |[Description]|
--Striking damage to one target, [Shocking DoT] chaser. Negative charge.  Triggers Spearguard.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.ThunderRat.BatteryDrainer == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Thunder Rat", "Battery Drainer", "$SkillName", gciJP_Cost_Advanced, gbIsNotFreeAction, "Direct", "Active", "Christine|BatteryDrainer", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\n[Shk] Chaser, +25%% [Atk] if [Shk] is present.\n\nTriggers Spearguard. Positive Charge.\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Hit the enemy for [Stk](Strike) damage, and deals\nextra damage if they have a [Shk](Shock) DoT.\nConsumes all [Shk]DoTs and deals their damage\nimmediately.\nTriggers Spearguard, positive charge.\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(25, giNoCPCost, giNoCooldown, "Target Enemies Single", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Strike")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Striking, 5, 1.20)
    
    --[Shocking DoT] chaser implementation
  --zAbiStruct.zExecutionAbiPackage:fnAddChaser(pbConsumeEffect, psDoTTag, pfDamageChangePerDoT, pfDamageConsumeFactor)
    zAbiStruct.zExecutionAbiPackage:fnAddChaser(gbConsumeEffects, "Shocking DoT", 0.10, 1.0)
    
    -- |[Execution Self-Effect Package - Spearguard]|
    --Basic package.
    zAbiStruct.zExecutionSelfPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides for Spearguard.
    zAbiStruct.zExecutionSelfPackage.sEffectPath     = fnResolvePath() .. "../../Common/Effects/Spearguard.lua"
    zAbiStruct.zExecutionSelfPackage.sEffectCritPath = fnResolvePath() .. "../../Common/Effects/Spearguard.lua"
    zAbiStruct.zExecutionSelfPackage.sApplyText = "Spearguard"
    zAbiStruct.zExecutionSelfPackage.sApplyTextCol = "Color:White"
    zAbiStruct.zExecutionSelfPackage.sApplyAnimation = "Null"
    zAbiStruct.zExecutionSelfPackage.sApplySound = "Combat\\|Impact_Buff"
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Offense|General:Swing|Ability:BatteryDrainer"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Add a manual line to the prediction to reflect Spearguard.
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "Triggers Spearguard"
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
    
    --Add charge indicator to prediction
    zAbiStruct.zPredictionPackage.zaAdditionalLines[2] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[2].sString   = "Creates Positive Charge"
    zAbiStruct.zPredictionPackage.zaAdditionalLines[2].saSymbols = {}
    
    --Set prototype.
    gzPrototypes.Combat.Christine.ThunderRat.BatteryDrainer = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.ThunderRat.BatteryDrainer

--Electric charge abilities use this handler instead of calling the standard script.
fnApplyPositiveCharge(iSwitchType)
