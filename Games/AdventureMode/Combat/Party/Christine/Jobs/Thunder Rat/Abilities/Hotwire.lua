-- |[ ======================================== Hotwire ========================================= ]|
-- |[Description]|
--Hits one enemy for piercing damage, inflicts stun.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.ThunderRat.Hotwire == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Thunder Rat", "Hotwire", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "Christine|Hotwire", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\n\n\nPositive Charge.\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "What harm can a crossed wire cause?\nDeals reduced [Prc](Pierce) damage, inflicts stun.\nBuilds a positive charge.\n\n\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(20, giNoCPCost, giNoCooldown, "Target Enemies Single", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Pierce")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Piercing, 5, 0.75)
    
    --Adding stun damage
    zAbiStruct.zExecutionAbiPackage:fnAddStun(75)
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Offense|General:Swing|Ability:Hotwire"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Add charge indicator to prediction
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "Creates Positive Charge"
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
    
    --Set prototype.
    gzPrototypes.Combat.Christine.ThunderRat.Hotwire = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.ThunderRat.Hotwire

--Electric charge abilities use this handler instead of calling the standard script.
fnApplyPositiveCharge(iSwitchType)
