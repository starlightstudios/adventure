-- |[ ========================================== Bluff ========================================= ]|
-- |[Description]|
--Increases evade by 100 until the next turn, self target.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.Lancer.Bluff == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Lancer", "Bluff", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Buff", "Active", "Christine|Bluff", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "+100 [Evd] for 1 [Turns]. [Target].\n\n\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Deceptively try to dodge the next hit.\nGreatly boosts your evade for 1 turn.\n\n\n\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(20, giNoCPCost, giNoCooldown, "Target Self", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Buff")
    zAbiStruct.zExecutionAbiPackage:fnSetAsBuff()
    
    -- |[Effect Package]|
  --zAbiStruct:fnAddBuff(psPrototypeName, $psApplyText, $psApplyColorString, $psApplyAnim, $psApplySound)
    zAbiStruct:fnAddBuff("Christine.Lancer.Bluff", "Bluffing!", "Color:Purple")
    
    --Unique resist text
    zAbiStruct.zExecutionEffPackage.sResistText = "Failed!"
    
    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName

    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
        local saDescription = {}
        saDescription[1] = "Try to block, then dodge!"
        saDescription[2] = "Increases [Evd](Evade) until your next turn."
      --EffectList:fnCreateBuffPrototype(psName, psDisplayName, psIcon, piTurns, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreateBuffPrototype(sLocalPrototypeName, "Bluff", "Christine|Bluff", 2, "Evd|Flat|100", saDescription, {{"Is Positive", 1}})
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Ability:Bluff"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Adjusting prediction to keep parity with previous version
    zAbiStruct.zPredictionPackage.zaEffectModules[1].sResultScript = "+100 [IMG1] / 1 [IMG2]"
    zAbiStruct.zPredictionPackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/StatisticIcons/Evade", "Root/Images/AdventureUI/DebuffIcons/Clock"}
    
    --Set prototype.
    gzPrototypes.Combat.Christine.Lancer.Bluff = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.Lancer.Bluff

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)