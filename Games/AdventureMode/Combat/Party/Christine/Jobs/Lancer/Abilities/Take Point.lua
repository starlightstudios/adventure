-- |[ ======================================= Take Point ======================================= ]|
-- |[Description]|
--Increases Christine's threat by 1000 to all enemies, free action, usable once every 10 turns.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.Lancer.TakePoint == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Lancer", "Take Point", "$SkillName", gciJP_Cost_Cheap, gbIsFreeAction, "Buff", "Active", "Christine|TakePoint", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "+1000 [Threat]\n\n\n\n\nFree Action.\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Put yourself at the head of the team.\nGreatly increases your chance of being\nattacked.\n\n\nFree action, 10 turn cooldown."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, giNoCPCost, 10, "Target Enemies All", giNoCPGeneration)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Terrify")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.bDoesNoDamage = true
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun = true
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits = true
    zAbiStruct.zExecutionAbiPackage.bEffectAlwaysApplies = true
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    --Running description markdown manually to avoid calling standard prediction creation
    zAbiStruct:fnRunDescriptionMarkdowns()
    
    --Creating custom prediction
    zAbiStruct.zPredictionPackage.zaEffectModules[1] = fnCreateEffectModule()
    zAbiStruct.zPredictionPackage.zaEffectModules[1].bIsBuff = true
    zAbiStruct.zPredictionPackage.zaEffectModules[1].sResultScript = "+1000 Threat"
    zAbiStruct.zPredictionPackage.zaEffectModules[1].saResultImages = {}
    
    --Set prototype.
    gzPrototypes.Combat.Christine.Lancer.TakePoint = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.Lancer.TakePoint

-- |[ ==================================== Standard Handler ==================================== ]|
--Call the standardized handler.
if(iSwitchType ~= gciAbility_Execute) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--This ability does no damage but does apply a lot of threat. The threat is a flat 1000 regardless
-- of other factors. This may need to be adjusted later.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[========== Originator Statistics ========== ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
    DL_PopActiveObject()
    
    -- |[========== Ability Package ========== ]|
    --Create an ability package. This package comes with the default hit/miss values, animations, etc.
    local zaAbilityPackage = fnConstructDefaultAbilityPackage("Terrify")
    zaAbilityPackage.iOriginatorID = iOriginatorID
    
    --Apply overrides. We only edit parts that may not be standard.
    zaAbilityPackage.bDoesNoDamage = true
    zaAbilityPackage.bDoesNoStun = true
    zaAbilityPackage.bAlwaysHits = true
    zaAbilityPackage.bEffectAlwaysApplies = true
    
    zaAbilityPackage.sVoiceData = "Christine|Ability:TakePoint"
    
    --Handling of free actions:
    if(gzRefAbility.bIsFreeAction) then
        
        --If the ability is a free action, but does not respect the action cap or consume any free actions,
        -- it is an "Effortless" action and can be used as many times as the player wants.
        if(gzRefAbility.iRequiredFreeActions == 0 and gzRefAbility.bRespectActionCap == false) then
            AdvCombat_SetProperty("Set As Effortless Action")
        
        --Normal free action.
        else
            AdvCombat_SetProperty("Set As Free Action")
        end
    end
    
    --Handling of cooldowns:
    if(gzRefAbility.iCooldown > 0) then
        AdvCombatAbility_SetProperty("Cooldown", gzRefAbility.iCooldown)
    end
    
    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        fnStandardExecution(iOriginatorID, i-1, zaAbilityPackage, nil)
        
        --Non-standard. Apply a "Threat" to the AI. The threat scales with attack power. Note that this applies even if the attack misses.
        AdvCombat_SetProperty("Push Target", i-1)
            local iTargetID = RO_GetID()
        DL_PopActiveObject()
        
        --Compute.
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetID, 0, "AI_APPLICATION|Threat|1000")
    end
end
