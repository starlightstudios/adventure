-- |[ ======================================== Inspire ========================================= ]|
-- |[Description]|
--Performs a guaranteed critical strike and buffs ally attack power. Has a long cooldown. Triggers Spearguard.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.Lancer.Inspire == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Lancer", "Inspire", "$SkillName", gciJP_Cost_Advanced, gbIsNotFreeAction, "Direct", "Active", "Christine|Inspire", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\nAlways hits. Always critically strikes.\n\n+25%% [Atk] All Allies / 3 [Turns].\nTriggers Spearguard.\n5 [Turns] cooldown.\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Set an example with a powerful blow!\nAlways crits, buffs ally attack power.\n5 turn cooldown.\n\nTriggers Spearguard.\n"
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, giNoCPCost, 6, "Target Enemies Single", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Weapon")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_UseWeapon, 0, 1.00)
    
    --Ensuring it always crits
    zAbiStruct.zExecutionAbiPackage:fnAlwaysCrit()
    
    --Optional threat multiplier.
    zAbiStruct.fThreatMultiplier = 3.0
    
    -- |[Execution Ally Effect Package]|
    --Basic package.  Does not use a prototype as the target of the ability is an enemy, not an ally.
    zAbiStruct.zAllyEffectPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides.
    zAbiStruct.zAllyEffectPackage.sEffectPath     = fnResolvePath() .. "../Effects/Inspire.lua"
    zAbiStruct.zAllyEffectPackage.sEffectCritPath = fnResolvePath() .. "../Effects/Inspire.lua"
    zAbiStruct.zAllyEffectPackage.sApplyText = "Inspired!"
    zAbiStruct.zAllyEffectPackage.sApplyTextCol = "Color:White"
    zAbiStruct.zAllyEffectPackage.sApplyAnimation = "Null"
    zAbiStruct.zAllyEffectPackage.sApplySound = "Combat\\|Impact_Buff"
    
    -- |[Execution Self-Effect Package - Spearguard]|
    --Basic package.
    zAbiStruct.zExecutionSelfPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides for Spearguard.
    zAbiStruct.zExecutionSelfPackage.sEffectPath     = fnResolvePath() .. "../../Common/Effects/Spearguard.lua"
    zAbiStruct.zExecutionSelfPackage.sEffectCritPath = fnResolvePath() .. "../../Common/Effects/Spearguard.lua"
    zAbiStruct.zExecutionSelfPackage.sApplyText = "Spearguard"
    zAbiStruct.zExecutionSelfPackage.sApplyTextCol = "Color:White"
    zAbiStruct.zExecutionSelfPackage.sApplyAnimation = "Null"
    zAbiStruct.zExecutionSelfPackage.sApplySound = "Combat\\|Impact_Buff"
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Offense|General:Swing|Ability:Inspire"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Adjusting crit module, adding Spearguard, and adding ally effect lines
    zAbiStruct.zPredictionPackage.zCritModule.iCritThreshold = -1000
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "Triggers Spearguard"
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[2] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[2].sString   = "+25%% [IMG0] All Allies"
    zAbiStruct.zPredictionPackage.zaAdditionalLines[2].saSymbols = {"Root/Images/AdventureUI/StatisticIcons/Attack"}
    
    --Set prototype.
    gzPrototypes.Combat.Christine.Lancer.Inspire = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.Lancer.Inspire

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
