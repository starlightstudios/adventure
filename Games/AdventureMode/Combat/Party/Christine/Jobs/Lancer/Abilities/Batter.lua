-- |[ ========================================= Batter ========================================= ]|
-- |[Description]|
--Major striking damage and stun to one target, costs CP. Triggers Spearguard.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.Lancer.Batter == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Lancer", "Batter", "$SkillName", gciJP_Cost_Advanced, gbIsNotFreeAction, "Direct", "Combo", "Christine|Batter", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\n\n\nTriggers Spearguard.\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Crush one enemy for major strike damage.\nInflicts a lot of stun.\n\n\nTriggers Spearguard.\nCosts [CPCost][CPIco](CP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, 4, giNoCooldown, "Target Enemies Single", giNoCPGeneration)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Strike")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Striking, 5, 2.50)
  --zAbiStruct.zExecutionAbiPackage:fnAddStun(piStun)
    zAbiStruct.zExecutionAbiPackage:fnAddStun(120)
    
    -- |[Execution Self-Effect Package - Spearguard]|
    --Basic package.
    zAbiStruct.zExecutionSelfPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides for Spearguard.
    zAbiStruct.zExecutionSelfPackage.sEffectPath     = fnResolvePath() .. "../../Common/Effects/Spearguard.lua"
    zAbiStruct.zExecutionSelfPackage.sEffectCritPath = fnResolvePath() .. "../../Common/Effects/Spearguard.lua"
    zAbiStruct.zExecutionSelfPackage.sApplyText = "Spearguard"
    zAbiStruct.zExecutionSelfPackage.sApplyTextCol = "Color:White"
    zAbiStruct.zExecutionSelfPackage.sApplyAnimation = "Null"
    zAbiStruct.zExecutionSelfPackage.sApplySound = "Combat\\|Impact_Buff"
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Special|Ability:Batter"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Add a manual line to the prediction to reflect Spearguard.
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "Triggers Spearguard"
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
    
    --Set prototype.
    gzPrototypes.Combat.Christine.Lancer.Batter = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.Lancer.Batter

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)