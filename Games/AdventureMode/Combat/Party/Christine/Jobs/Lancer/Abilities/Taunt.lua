-- |[ ========================================= Taunt ========================================== ]|
-- |[Description]|
--Hits all enemies for 60% of base damage as Terrifying. Inflicts tripled threat.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.Lancer.Taunt == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Lancer", "Taunt", "$SkillName", gciJP_Cost_Cheap, gbIsNotFreeAction, "Direct", "Active", "Christine|Taunt", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit]. +200%% [Threat]\n\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Mock all enemies to inflict reduced Terrify\ndamage.\nCauses greatly increased threat.\n\n\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(15, giNoCPCost, giNoCooldown, "Target Enemies All", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Terrify")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Terrifying, 5, 0.60)
    
    --Optional threat multiplier.
    zAbiStruct.fThreatMultiplier = 3.0
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Ability:Taunt"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.Christine.Lancer.Taunt = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.Lancer.Taunt

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)