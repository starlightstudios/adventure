-- |[ ======================================= Bloodboil ======================================== ]|
-- |[Description]|
--Increases attack power for 3 turns, self target.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.BrassBrawler.Bloodboil == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Brass Brawler", "Bloodboil", "$SkillName", gciJP_Cost_Normal, gbIsFreeAction, "Buff", "Active", "Christine|Bloodboil", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "+45%% [Atk] for 3[Turns]. [Target].\n\n\n\n\nFree Action.\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "The advantage of steam blood is the hotter\nyou run, the stronger you are.\nBuffs your [Atk](Power).\n\nFree Action.\nCosts [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(10, giNoCPCost, giNoCooldown, "Target Self", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Buff")
    zAbiStruct.zExecutionAbiPackage:fnSetAsBuff()

    -- |[Effect Package]|
  --zAbiStruct:fnAddBuff(psPrototypeName, $psApplyText, $psApplyColorString, $psApplyAnim, $psApplySound)
    zAbiStruct:fnAddBuff("Christine.BrassBrawler.Bloodboil", "Running Hot!", "Color:Purple")
    
    --Custom resist text
    zAbiStruct.zExecutionEffPackage.sResistText = "Failed!"

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName

    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
        local saDescription = {}
        saDescription[1] = "Run your blood hot!"
        saDescription[2] = "Increases [Atk](Power) for 3 turns."
      --EffectList:fnCreateBuffPrototype(psName, psDisplayName, psIcon, piTurns, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreateBuffPrototype(sLocalPrototypeName, "Blood Boil", "Christine|Bloodboil", 3, "Atk|Pct|0.45", saDescription, {{"Is Positive", 1}})
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Ability:Bloodboil"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.Christine.BrassBrawler.Bloodboil = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.BrassBrawler.Bloodboil

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
