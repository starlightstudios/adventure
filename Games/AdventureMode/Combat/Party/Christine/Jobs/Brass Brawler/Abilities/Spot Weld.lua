-- |[ ======================================= Spot Weld ======================================== ]|
-- |[Description]|
--Field ability, heals the party for half their HP, one use per rest.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Ability Prototype =================================== ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.BrassBrawler.SpotWeld == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Brass Brawler", "Spot Weld", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Special", "Christine|PercussiveMaintenance", "Unequippable")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Unlocks the Spot Weld field ability, restoring 50%% of the[BR]party's HP once per rest action."
    zAbiStruct.sSimpleDescMarkdown  = "Unlocks the Spot Weld field ability.[BR]Restores 50%% of the party's HP, one use per[BR]rest.[BR][BR]Field abilities can be seen in the top left corner[BR]when out of battle."

    -- |[Usability Variables]|
    zAbiStruct.bNeverAvailable = true --Field abilities have no usability requirements.
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Christine.BrassBrawler.SpotWeld = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.BrassBrawler.SpotWeld

--Call standard.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
