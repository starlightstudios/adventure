-- |[ ================================= Percussive Maintenance ================================= ]|
-- |[Description]|
--Heals one ally, costs no MP.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.BrassBrawler.PercussiveMaintenance == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Brass Brawler", "Percussive Maintenance", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Heal", "Active", "Christine|PercussiveMaintenance", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Restore]. [Target].\n\n\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Sometimes it just takes a whack and it works!\nHeals one ally.\nHealing increases with attack power.\n\n\nCosts no [MPIco](MP)!"
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, giNoCPCost, giNoCooldown, "Target Allies Single", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Healing")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsHealing(piHealingBase, pfHealingScaleFactor, pfHealingPctOfHPMax)
    zAbiStruct.zExecutionAbiPackage:fnSetAsHealing(30, 0.30, 0)
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Ability:PercussiveMaintenance"

    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.Christine.BrassBrawler.PercussiveMaintenance = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.BrassBrawler.PercussiveMaintenance

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
