-- |[ ======================================= Improvise ======================================== ]|
-- |[Description]|
--Generates 3 CP immediately at the cost of your turn.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.BrassBrawler.Improvise == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Brass Brawler", "Improvise", "$SkillName", gciJP_Cost_Advanced, gbIsNotFreeAction, "Heal", "Active", "Christine|Improvise", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Immediately generates 3[CPIco]. [Target].[BR][BR][BR][BR][BR]"
    zAbiStruct.sSimpleDescMarkdown  = "Think of something - anything![BR]Generates 3 [CPIco](CP) at the cost of your turn.[BR][BR][BR][BR]"
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, giNoCPCost, giNoCooldown, "Target Self", 3)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Buff")
    zAbiStruct.zExecutionAbiPackage:fnSetAsBuff()

    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Ability:Improvise"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Add additional lines to show CP generation in prediction
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "Generate 3 CP."
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
    
    --Set prototype.
    gzPrototypes.Combat.Christine.BrassBrawler.Improvise = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.BrassBrawler.Improvise

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
