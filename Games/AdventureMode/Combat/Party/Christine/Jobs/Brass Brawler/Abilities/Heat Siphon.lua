-- |[ ==================================== Heat Siphon ========================================= ]|
-- |[Description]|
--Get a little re-up! Siphons heat for moderate Freeze damage.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.BrassBrawler.HeatSiphon == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Brass Brawler", "Heat Siphon", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "Christine|HeatSiphon", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\n\n\n3 [Turns] cooldown.\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Get a little re-up on your steam!\nDeals moderate [Frz](Freeze) damage \nwith a large margin of error.\n3 turn cooldown.\n\nCosts [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(15, giNoCPCost, 3, "Target Enemies Single", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Freeze")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Freezing, 5, 1.25)
    
    --Manual override for custom scatter range
    zAbiStruct.zExecutionAbiPackage.iScatterRange = 40
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Ability:HeatSiphon"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Adjust prediction to reflect scatter range
    zAbiStruct.zPredictionPackage.zDamageModule.iScatterRange = 40
    
    --Set prototype.
    gzPrototypes.Combat.Christine.BrassBrawler.HeatSiphon = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.BrassBrawler.HeatSiphon

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
