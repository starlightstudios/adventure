-- |[ ======================================= Pincushion ======================================= ]|
-- |[Description]|
--Randomly strikes five times for .70x damage, total 3.50x. Costs CP.  Triggers Spearguard.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.BrassBrawler.Pincushion == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Brass Brawler", "Pincushion", "$SkillName", gciJP_Cost_Advanced, gbIsNotFreeAction, "Direct", "Combo", "Christine|Pincushion", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. 5 random targets.\n[BaseHit].\n\n\n\nTriggers Spearguard.\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Rapid spear strikes turn the enemy into\npincushions!\nAttack five times at random targets, deals\nmajor damage overall.\nTriggers Spearguard.\nCosts 4[CPIco](CP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, 4, giNoCooldown, "Target Enemies All", giNoCPGeneration)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Weapon")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_UseWeapon, 5, 0.60)
    
    -- |[Execution Self-Effect Package - Spearguard]|
    --Basic package.
    zAbiStruct.zExecutionSelfPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides for Spearguard.
    zAbiStruct.zExecutionSelfPackage.sEffectPath     = fnResolvePath() .. "../../Common/Effects/Spearguard.lua"
    zAbiStruct.zExecutionSelfPackage.sEffectCritPath = fnResolvePath() .. "../../Common/Effects/Spearguard.lua"
    zAbiStruct.zExecutionSelfPackage.sApplyText = "Spearguard"
    zAbiStruct.zExecutionSelfPackage.sApplyTextCol = "Color:White"
    zAbiStruct.zExecutionSelfPackage.sApplyAnimation = "Null"
    zAbiStruct.zExecutionSelfPackage.sApplySound = "Combat\\|Impact_Buff"
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Offense|General:Swing|Ability:Pincushion"
    
    -- |[Finalize]|
    --Running description markdown manually to avoid calling standard prediction creation
    zAbiStruct:fnRunDescriptionMarkdowns()
    
    --Custom prediction for parity with original
    zAbiStruct.zPredictionPackage.bHasHitRateModule = true
    zAbiStruct.zPredictionPackage.zHitRateModule.iMissRate = 5
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "Random target, 0.60 x[IMG0] 5 times."
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {"Root/Images/AdventureUI/StatisticIcons/Attack"}
    
    --Add a manual line to the prediction to reflect Spearguard.
    zAbiStruct.zPredictionPackage.zaAdditionalLines[2] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[2].sString   = "Triggers Spearguard"
    zAbiStruct.zPredictionPackage.zaAdditionalLines[2].saSymbols = {}
    
    --Set prototype.
    gzPrototypes.Combat.Christine.BrassBrawler.Pincushion = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.BrassBrawler.Pincushion

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
if(iSwitchType == gciAbility_Execute) then

    -- |[ ========== Originator Statistics ========== ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        fnModifyCP(-4)
    DL_PopActiveObject()
    
    -- |[ ========== Ability Package ========== ]|
    --Execute the same attack five times.
    for i = 1, 5, 1 do
    
        --Common.
        gzRefAbility.zExecutionAbiPackage.iOriginatorID  = iOriginatorID
        gzRefAbility.zExecutionAbiPackage.iMissThreshold = 5
        gzRefAbility.zExecutionAbiPackage.fDamageFactor  = 0.60
    
        --Pick one of the painted targets at random. It can be the same target twice.
        local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
        local iTargetSlot = LM_GetRandomNumber(0, iTargetsTotal-1)
    
        --Set the combat timer offset. This allows multiple hits to go through, but makes them not simultaneous.
        giCombatTimerOffset = 15 * (i-1)
        
        --Run.
        fnStandardExecution(iOriginatorID, iTargetSlot, gzRefAbility.zExecutionAbiPackage, nil)
    end
    
    --Copied self-effect execution code from standard call to ensure Spearguard triggers
    if(gzRefAbility.zExecutionSelfPackage ~= nil) then
        gzRefAbility.zExecutionSelfPackage.fnHandler(iOriginatorID, iOriginatorID, 0, gzRefAbility.zExecutionSelfPackage, true, false)
    end


-- |[ ==================================== Standard Handler ==================================== ]|
--All other cases, call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end