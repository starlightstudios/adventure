-- |[ ========================================= Bash =========================================== ]|
-- |[Description]|
--Smack 'em with a wrench. Deals stun damage.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.BrassBrawler.Bash == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Brass Brawler", "Bash", "$SkillName", gciJP_Cost_Advanced, gbIsNotFreeAction, "Direct", "Combo", "Christine|Bash", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Pull out a huge brass spanner and hit 'em\nwhere it hurts!\nInflicts a huge amount of stun, and deals\nmoderate [Stk](Strike) damage.\n\nCosts [CPCost][CPIco](CP)."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, 3, giNoCooldown, "Target Enemies Single", giNoCPGeneration)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Strike")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Striking, 8, 2.00)
    
    --Adding stun damage
    zAbiStruct.zExecutionAbiPackage:fnAddStun(160)
    
    --Manual override for custom scatter range
    zAbiStruct.zExecutionAbiPackage.iScatterRange = 10

    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Special|Ability:Bash"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Adjust prediction to reflect scatter range
    zAbiStruct.zPredictionPackage.zDamageModule.iScatterRange = 10
    
    --Set prototype.
    gzPrototypes.Combat.Christine.BrassBrawler.Bash = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.BrassBrawler.Bash

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
