-- |[ ========================================= Bash ========================================= ]|
-- |[Description]|
--They've gotta have SOMETHING loose. Steals platina from a target, -3 protection, free action.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.BrassBrawler.Scavenge == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Brass Brawler", "Scavenge", "$SkillName", gciJP_Cost_Normal, gbIsFreeAction, "Direct", "Active", "Christine|Scavenge", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Steals platina equal to 0.50x [Atk].\n\n[HEffect][Str6][Stk] -3[Prt] / 3[Turns].\nCan steal once per target.\n\nFree action.\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "They've gotta have SOMETHING loose!\nSteals some Platina from a single target, \nas well as some armor- lowering\ntheir [Prt](Protection).\n\nFree Action."
 
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, giNoCPCost, giNoCooldown, "Target Enemies Single", giNoCPGeneration)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Strike")
    zAbiStruct.zExecutionAbiPackage:fnSetAsDebuff()
    
    -- |[Execution Effect Package]|
  --zAbiStruct:fnAddEffect(piStrNormal, piStrCrit, piApplyType, psApplyText, psCritApplyText, psApplyColorString, psaApplyBonus, psaApplyMalus, psaSeverityBonus, psaSeverityMalus)
    zAbiStruct:fnAddEffect(5, 5, gciDamageType_Corroding, "Armor Torn!", "Armor Badly Torn!", "Color:Purple")
    
    -- |[Use of Effect Prototype]|
    zAbiStruct.zExecutionEffPackage.sPrototypeName = "Christine.BrassBrawler.Scavenge"
    
    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
	local saDescription = {}
	saDescription[1] = "Defenses disrupted."
	saDescription[2] = "Reduces [Prt](Protection)."
      --EffectList:fnCreateStatPrototype(pzEffectpackage, psName, pbIsBuff, psDisplayName, psIcon, piTurns, piApplyStrength, psStatString, psaDescription, $pzaTagList, $piCritTurns, $piCritApplyStrength)
        EffectList:fnCreateStatPrototype(zAbiStruct.zExecutionEffPackage, sLocalPrototypeName, gbIsDebuff, "Scavenge", "Christine|Scavenge", 3, 5, "Prt|Flat|-3", saDescription, {{"Is Negative", 1}}, 3, 5)
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Ability:Scavenge"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.Christine.BrassBrawler.Scavenge = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.BrassBrawler.Scavenge

-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--Normal prediction box work, but adds a platina steal line.
if(iSwitchType == gciAbility_BuildPredictionBox) then

    -- |[ ===================== Other Values ===================== ]|
    --Get originator ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
        local iAttackPower = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
    DL_PopActiveObject()
    
    --Get information about the cluster. This is needed to give each box a unique ID.
    local iClusterID = AdvCombat_GetProperty("Prediction Get Cluster ID")
    
    -- |[ ==================== For Each Target =================== ]|
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    for i = 0, iClusterTargetsTotal-1, 1 do
        
        --Target ID.
        local iTargetID = AdvCombat_GetProperty("Prediction Get Targets ID", i)
        
        --Clear the additional lines.
        gzRefAbility.zPredictionPackage.zaAdditionalLines = {}
        
        --Check if we already pickpocketed the enemy.
        local iHasPickpocketed = VM_GetVar("Root/Variables/Combat/" .. iTargetID .. "/iWasPickpocketed", "N")
        if(iHasPickpocketed == 0.0) then
            
            --Determine amount stolen.
            local iPickPocketPower = math.floor(iAttackPower * 0.50)
            if(iPickPocketPower < 1) then iPickPocketPower = 1 end
            
            --Display.
            gzRefAbility.zPredictionPackage.zaAdditionalLines[1] = {}
            gzRefAbility.zPredictionPackage.zaAdditionalLines[1].sString = ""
            gzRefAbility.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
            gzRefAbility.zPredictionPackage.zaAdditionalLines[2] = {}
            gzRefAbility.zPredictionPackage.zaAdditionalLines[2].sString = "Steal " .. iPickPocketPower .. " platina."
            gzRefAbility.zPredictionPackage.zaAdditionalLines[2].saSymbols = {}
            
        --Already pickpocketed.
        else
            gzRefAbility.zPredictionPackage.zaAdditionalLines[1] = {}
            gzRefAbility.zPredictionPackage.zaAdditionalLines[1].sString = "Already pickpocketed!"
            gzRefAbility.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
        end
        
        fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, gzRefAbility.zPredictionPackage)
    end

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Standard execution, but steals platina.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[ ================ Originator Statistics ================= ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        local iAttackPower = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
    DL_PopActiveObject()
    
    -- |[ =================== Ability Package ==================== ]|
    --Apply cooldown for this ability. Mark the action as free.
    AdvCombatAbility_SetProperty("Cooldown", 1)
    AdvCombat_SetProperty("Set As Free Action")
    
    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        
        --Standard execution handler.
        fnStandardExecution(iOriginatorID, i-1, gzRefAbility.zExecutionAbiPackage, gzRefAbility.zExecutionEffPackage)
        
        --Get if the target was pickpocketed:
        AdvCombat_SetProperty("Push Target", i-1) 
            local iTargetUniqueID = RO_GetID()
        DL_PopActiveObject()
        
        --Check if we already pickpocketed the enemy.
        local iHasPickpocketed = VM_GetVar("Root/Variables/Combat/" .. iTargetUniqueID .. "/iWasPickpocketed", "N")
        if(iHasPickpocketed == 0.0 and gbAttackHit) then
            
            --Flag.
            VM_SetVar("Root/Variables/Combat/" .. iTargetUniqueID .. "/iWasPickpocketed", "N", 1.0)
            
            --Determine amount stolen.
            local iPickPocketPower = math.floor(iAttackPower * 0.50)
            if(iPickPocketPower < 1) then iPickPocketPower = 1 end
            
            --Store.
            DL_AddPath("Root/Variables/Combat/Default/")
            VM_SetVar("Root/Variables/Combat/Default/iPickPocket", "N", iPickPocketPower)
            
            --Report the steal value.
            local sScriptPath = LM_GetCallStack(0)
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, 45, "Run Script|" .. sScriptPath .. "|N:-1000")
        end
    end
    
-- |[ ======================================== Cutscene ======================================== ]|
--Special
elseif(iSwitchType == -1000) then

    --Stole string.
    local iStoleValue = VM_GetVar("Root/Variables/Combat/Default/iPickPocket", "I")
    local sString = "WD_SetProperty(\"Append\", \"Christine: Stole " .. iStoleValue .. " platina!\")"

    --Base.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene(sString)
    fnCutsceneBlocker()
    
    --Platina calculation
    AdInv_SetProperty("Add Platina", iStoleValue)
    
-- |[ ==================================== Standard Handler ==================================== ]|
--Standard handler for all other cases.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
