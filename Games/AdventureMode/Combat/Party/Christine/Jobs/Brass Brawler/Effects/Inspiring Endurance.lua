-- |[ ================================== Inspiring Endurance =================================== ]|
-- |[Description]|
--Increase flame resist and accuracy.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Local Variables ===================================== ]|
-- |[System]|
--This effect uses a standardized StatMod script.
local sDisplayName = "Inspiring Endurance"
local iDuration = -1
local bIsBuff = true
local sIcon = "Root/Images/AdventureUI/Abilities/Christine|InspiringEndurance"
local sStatString = "ResFlm|Flat|5 Acc|Flat|15"

--Description can be a variable number of lines.
local saDescription = {}
saDescription[1] = "Never give up!"
saDescription[2] = "Increases [Acc](Accuracy) and [Flm](Flame) resistance."
saDescription[3] = "Passive Aura."

--Run the standardized builder.
gzaStatModStruct            = fnCreateStatmodEffectPrototype(sDisplayName, iDuration, bIsBuff, sIcon, sStatString)
gzaStatModStruct.saStrings  = fnStatmodBuildInspectorStrings(gzaStatModStruct, saDescription)
gzaStatModStruct.sShortText = fnStatmodBuildShortText(gzaStatModStruct)

-- |[Overrides]|
--Passive, not removed on KO, uses different frame.
gzaStatModStruct.sFrame      = gsAbility_Frame_Passive
gzaStatModStruct.bRemoveOnKO = false

-- |[Tags]|
gzaStatModStruct.zaTagList = {{"Is Positive", 1}, {"Is Passive", 1}}

-- |[ ======================================== Creation ======================================== ]|
--Standard call when effect is initialized.
if(iSwitchType == gciEffect_Create) then
    
    --Get severity.
    local fSeverity = 1.0
    if(iArgumentsTotal >= 2) then fSeverity = LM_GetScriptArgument(1, "N") end
    
    --Tag list needs to be modified with the owner's ID.
    local iOriginatorID = AdvCombatEffect_GetProperty("ID of Originator")
    gzaStatModStruct.zaTagList = {{"Is Positive", 1}, {"Is Passive", 1}, {"Inspiring Endurance " .. iOriginatorID, 1}}
    
    --Call.
    LM_ExecuteScript(gsStandardStatPath, gciEffect_Create, fSeverity)
    
-- |[ ================================== Apply/Unapply Stats =================================== ]|
--Calls the apply/unapply stat routines.
elseif((iSwitchType == gciEffect_ApplyStats or iSwitchType == gciEffect_UnApplyStats) and iArgumentsTotal >= 2) then
    local iTargetID = math.floor(tonumber(LM_GetScriptArgument(1, "N")))
    LM_ExecuteScript(gsStandardStatPath, iSwitchType, iTargetID)

-- |[ ================================== Character Action Ends ================================= ]|
--Called after a character action ends to decrement the turn counter.
elseif(iSwitchType == gciEffect_PostAction) then
    LM_ExecuteScript(gsStandardStatPath, gciEffect_PostAction)
end
