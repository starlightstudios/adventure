-- |[ ======================================= Menialist ======================================== ]|
--The Secrebot class.

-- |[ ========== Argument Handling =========== ]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--1st argument is only used for level-up calls.
local iLevelReached = 0
if(iArgumentsTotal >= 2) then
    iLevelReached = LM_GetScriptArgument(1, "N")
end

-- |[ ============ Variable Setup ============ ]|
-- |[Enumerations]|
local gciNoSkillbooks = 0

-- |[Encapsulation]|
local zEntry = {}

-- |[Basic Variables]|
--Character Variables.
zEntry.sCharacterFormVarPath = "Root/Variables/Global/Christine/sForm"
zEntry.sSkillbookVarPath     = "Root/Variables/Global/Christine/iSkillbookTotal"
zEntry.zaJobChart            = gzaChristineJobChart
zEntry.sStatProfilePath      = gsRoot .. "Combat/Party/Christine/400 Common Stat Profile.lua"
zEntry.sSecondaryMenuPath    = gsRoot .. "Combat/Party/Christine/200 Build Secondary Menu.lua"
zEntry.sMasterBonusPath      = gsRoot .. "Combat/Party/Christine/401 Mastery Bonus Handler.lua"

--Class Variables.
zEntry.sClassName   = "Menialist"
zEntry.sFormName    = "Secrebot"
zEntry.sCostumeName = "Christine_Secrebot"
zEntry.fFaceIndexX  = 15
zEntry.fFaceIndexY  = 4

-- |[Ability Variables]|
--Purchaseable abilities.
zEntry.saExternalAbilities = {}
table.insert(zEntry.saExternalAbilities, "Forced Reschedule")
table.insert(zEntry.saExternalAbilities, "Hold My Calls")
table.insert(zEntry.saExternalAbilities, "Impromptu Organization")
table.insert(zEntry.saExternalAbilities, "Inspiring Fashion")
table.insert(zEntry.saExternalAbilities, "Mighty Pen")
table.insert(zEntry.saExternalAbilities, "Oil Run")
table.insert(zEntry.saExternalAbilities, "Proofreading")
table.insert(zEntry.saExternalAbilities, "Stress Release")
table.insert(zEntry.saExternalAbilities, "Take Notes")

--Class-equipped abilities.
zEntry.saInternalAbilities = {}
table.insert(zEntry.saInternalAbilities, "Impromptu Organization")
table.insert(zEntry.saInternalAbilities, "Hold My Calls")
table.insert(zEntry.saInternalAbilities, "Oil Run")
table.insert(zEntry.saInternalAbilities, "Mighty Pen")
table.insert(zEntry.saInternalAbilities, "Forced Reschedule")
table.insert(zEntry.saInternalAbilities, "Take Notes")
table.insert(zEntry.saInternalAbilities, "Stress Release")

--Purchaseable abilities, in the order of purchase.
zEntry.saPurchaseAbilities = {}
table.insert(zEntry.saPurchaseAbilities, "Menialist|Mighty Pen")
table.insert(zEntry.saPurchaseAbilities, "Menialist|Hold My Calls")
table.insert(zEntry.saPurchaseAbilities, "Menialist|Forced Reschedule")
table.insert(zEntry.saPurchaseAbilities, "Menialist|Oil Run")
table.insert(zEntry.saPurchaseAbilities, "Menialist|Impromptu Organization")
table.insert(zEntry.saPurchaseAbilities, "Menialist|Take Notes")
table.insert(zEntry.saPurchaseAbilities, "Menialist|Stress Release")
table.insert(zEntry.saPurchaseAbilities, "Menialist|Proofreading")
table.insert(zEntry.saPurchaseAbilities, "Menialist|Inspiring Fashion")

-- |[Skillbar Variables]|
--Skills that always appear on the skillbar. Format: {sAbilityName, iXPos, iYPos, iSkillbooks} (eg: {"Common|Attack", 0, 0, 0}, {"Common|Parry", 1, 0, 0} {})
zEntry.zaSkillbarAbilities = {}
table.insert(zEntry.zaSkillbarAbilities, {"Common|Attack",                             0, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Common|Discharge",                          1, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Menialist Internal|Stress Release",         2, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Menialist Internal|Hold My Calls",          0, 1, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Menialist Internal|Oil Run",                1, 1, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Menialist Internal|Mighty Pen",             2, 1, 1})
table.insert(zEntry.zaSkillbarAbilities, {"Menialist Internal|Forced Reschedule",      0, 2, 2})
table.insert(zEntry.zaSkillbarAbilities, {"Menialist Internal|Take Notes",             1, 2, 3})
table.insert(zEntry.zaSkillbarAbilities, {"Menialist Internal|Impromptu Organization", 2, 2, 4})
        
-- |[ ============ Call Standard ============= ]|
--Add to the global stack. There may be multiple executions of this script.
table.insert(gzJobCallStack, zEntry)

--Call.
LM_ExecuteScript(gsStandardJobPath, iSwitchType, iLevelReached)

--Pop the global stack.
table.remove(gzJobCallStack)

-- |[Name Handling]|
if(iSwitchType == gciJob_SwitchTo) then

    --Variables.
    local i254Transformed    = VM_GetVar("Root/Variables/Chapter5/Scenes/i254Transformed", "N")
    local i254FixedChristine = VM_GetVar("Root/Variables/Chapter5/Scenes/i254FixedChristine", "N")

    --Push job owner.
    AdvCombatJob_SetProperty("Push Owner")

        --Player is on the secrebot quest and has not fixed Christine.
        if(i254Transformed == 1.0 and i254FixedChristine == 0.0) then
            AdvCombatEntity_SetProperty("Display Name", "CR-1-16")
        
        --All other cases.
        else
            AdvCombatEntity_SetProperty("Display Name", "DEFAULT")
        end

    --Finish.
    DL_PopActiveObject()
end