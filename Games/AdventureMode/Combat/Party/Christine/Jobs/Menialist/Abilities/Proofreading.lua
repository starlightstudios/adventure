-- |[ ====================================== Proofreading ====================================== ]|
-- |[Description]|
--Passive, increases accuracy.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.Menialist.Proofreading == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Menialist", "Proofreading", "$SkillName", gciJP_Cost_Passive, gbIsNotFreeAction, "Buff", "Passive", "Christine|Proofreading", "Passive")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "+25[Acc].[BR][BR][BR][BR][BR][BR]Passive."
    zAbiStruct.sSimpleDescMarkdown  = "A little attention to detail goes a long way.[BR]Increases [Acc](Accuracy).[BR][BR][BR][BR]Passive."
    
    -- |[Passive Effect]|
    zAbiStruct.bNeverAvailable   = true --Passive abilities have no usability requirements.
    zAbiStruct.sPassivePrototype = "Christine.Menialist.Proofreading"
    zAbiStruct.bHasGUIEffects    = true

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    if(EffectList:fnEntryExists(zAbiStruct.sPassivePrototype) == false) then
        local saDescription = {}
        saDescription[1] = "Impressive attention to detail increases accuracy."
        saDescription[2] = "Increases [Acc](Accuracy)"
      --EffectList:fnCreatePassivePrototype(psName, psDisplayName, psIcon, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreatePassivePrototype(zAbiStruct.sPassivePrototype, "Proofreading", "Christine|Proofreading", "Acc|Flat|25", saDescription)
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Christine.Menialist.Proofreading = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.Menialist.Proofreading

-- |[ ===================================== All Other Cases ==================================== ]|
if(iSwitchType ~= gciAbility_GUIApplyEffect) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

-- |[ ==================================== GUI: Apply Effect =================================== ]|
--When showing a character's statistics, all equipped abilities are queried. This is not reflected
-- in combat, this is GUI only.
--The standard does not handle this.
elseif(iSwitchType == gciAbility_GUIApplyEffect) then
    AdvCombatAbility_SetProperty("Push Owner")
        fnApplyStatBonus(gciStatIndex_Accuracy, 25)
    DL_PopActiveObject()
    gzRefAbility = nil
end
