-- |[ ================================= Impromptu Organization ================================= ]|
-- |[Description]|
--Gives 'Fast' to one party member, lasts 3 turns, 3 turn cooldown.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Ability Prototype =================================== ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.Menialist.ImpromptuOrganization == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Menialist", "Impromptu Organization", "$SkillName", gciJP_Cost_Normal, gbIsFreeAction, "Buff", "Active", "Christine|ImpromptuOrganization", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[FEffect] 'Fast' for 3[Turns].[BR]One ally.[BR][BR][BR][BR]Free Action.[BR][Costs]"
    zAbiStruct.sSimpleDescMarkdown = "Reorganize on the go![BR]Guarantees one party member acts first for 3[BR]turns.[BR][BR]Free Action.[BR]Costs [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(20, giNoCPCost, 3, "Target Allies Single", giNoCPGeneration)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Buff")
    zAbiStruct.zExecutionAbiPackage:fnSetAsBuff()
    
    -- |[Effect Package]|
  --zAbiStruct:fnAddBuff(psPrototypeName, $psApplyText, $psApplyColorString, $psApplyAnim, $psApplySound)
    zAbiStruct:fnAddBuff("Christine.Menialist.ImpromptuOrganization", "Fast!")

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
        local saDescription = {}
        saDescription[1] = "Incredible organizational skills let you act first!"
        saDescription[2] = "Provides 'Fast', making you act first each turn."
      --EffectList:fnCreateBuffPrototype(psName, psDisplayName, psIcon, piTurns, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreateBuffPrototype(sLocalPrototypeName, "Impromptu Organization", "Christine|ImpromptuOrganization", 3, "", saDescription, {{"Fast", 1}})

        --Manually specify description.
        local iType, zPrototype, iSlot = EffectList:fnLocateEntry(sLocalPrototypeName)
        if(zPrototype ~= nil) then
            zPrototype.bDontRebuildDescription = true
            zPrototype.saStrings[1] = "[Buff]Impromptu Organization"
            zPrototype.saStrings[2] = "[UpN][Fast] /[Dur][Turns]"
            zPrototype.saStrings[3] = "Ready to act first next round!"
            zPrototype.saStrings[4] = ""
            zPrototype.saStrings[5] = ""
            zPrototype.saStrings[6] = ""
            zPrototype.saStrings[7] = ""
            zPrototype.saStrings[8] = "[Buff] [Fast] ([Dur][Turns])"
            zPrototype.sShortText = "[Buff] [Fast] ([Dur][Turns])"
        end
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Ability:ImpromptuOrganization"

    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Christine.Menialist.ImpromptuOrganization = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.Menialist.ImpromptuOrganization

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
