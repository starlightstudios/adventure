-- |[ =================================== Forced Reschedule ==================================== ]|
-- |[Description]|
--Inflict 45% shock damage over 3 turns, free action.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.Menialist.ForcedReschedule == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Menialist", "Forced Reschedule", "$SkillName", gciJP_Cost_Normal, gbIsFreeAction, "DoT", "Active", "Christine|ForcedReschedule", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Inflicts minimal damage. [Target].[BR][BaseHit].[BR][BR][DoT].[BR][BR][BR][Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Really screw with everyone's plans by changing[BR]the meeting start time![BR]Deals reduced shock damage over 3 turns.[BR]Free Action.[BR][BR]Costs [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(10, giNoCPCost, 1, "Target Enemies Single", giNoCPGeneration)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Shock")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Shocking, 5, 0.01)
    
    -- |[Execution Effect Package]|
  --zAbiStruct:fnAddEffect(piStrNormal, piStrCrit, piApplyType, psApplyText, psCritApplyText, psApplyColorString, psaApplyBonus, psaApplyMalus, psaSeverityBonus, psaSeverityMalus)
    zAbiStruct:fnAddEffect(5, 8, gciDamageType_Shocking, "Shocked!", "Badly Shocked!", "Color:Red", {"Shock Standard"})
    
    -- |[Use of Effect Prototype]|
    zAbiStruct.zExecutionEffPackage.sPrototypeName = "Christine.Menialist.ForcedRescheduling"

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
      --EffectList:fnCreateDoTPrototype(psName, piTurns, piApplyStrength, pfDamPerTurn, piDamageType, psDisplayName, psIcon, $piCritTurns, $piCritApplyStrength)
        EffectList:fnCreateDoTPrototype(sLocalPrototypeName, 3, 5, 0.16, gciDamageType_Shocking, "Forced Rescheduling", "Christine|ForcedReschedule", 5, 8)
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|General:Swing|Offense|Ability:Scheduling"

    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Christine.Menialist.ForcedReschedule = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.Menialist.ForcedReschedule

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
