-- |[ ====================================== Slavedriver ======================================= ]|
-- |[Description]|
--Hits all enemies for 70% of base damage. Terrify effect to reduce attack power.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.Commissar.Slavedriver == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Commissar", "Slavedriver", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "Christine|Slavedriver", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].[BR][BaseHit].[BR][BR][HEffect][Str7][Shk] -30%%[Atk] / 3[Turns].[BR][BR][BR][Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Use your discharge cord as a whip to strike[BR]all enemies.[BR]Deals reduced [Shk](Shock) damage.[BR]Reduces [Atk](Power) for 3 turns.[BR][BR]Costs [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(20, giNoCPCost, giNoCooldown, "Target Enemies All", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Shock")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Shocking, 5, 0.70)
    
    -- |[Execution Effect Package]|
  --zAbiStruct:fnAddEffect(piStrNormal, piStrCrit, piApplyType, psApplyText, psCritApplyText, psApplyColorString, psaApplyBonus, psaApplyMalus, psaSeverityBonus, psaSeverityMalus)
    zAbiStruct:fnAddEffect(7, 11, gciDamageType_Shocking, "Frightened!", "Badly Frightened!", "Color:Purple")
    
    -- |[Use of Effect Prototype]|
    zAbiStruct.zExecutionEffPackage.sPrototypeName = "Christine.Commissar.Slavedriver"
    
    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
        local saDescription = {}
        saDescription[1] = "Command Units were made to inspire obedience."
        saDescription[2] = "Decreases [Atk](Power)."
      --EffectList:fnCreateStatPrototype(pzEffectpackage, psName, pbIsBuff, psDisplayName, psIcon, piTurns, piApplyStrength, psStatString, psaDescription, $pzaTagList, $piCritTurns, $piCritApplyStrength)
        EffectList:fnCreateStatPrototype(zAbiStruct.zExecutionEffPackage, sLocalPrototypeName, gbIsDebuff, "Slavedriver", "Christine|Slavedriver", 3, 7, "Atk|Pct|-0.30", saDescription, {{"Is Negative", 1}}, 5, 11)
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Ability:SlaveDriver"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.Christine.Commissar.Slavedriver = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.Commissar.Slavedriver

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

