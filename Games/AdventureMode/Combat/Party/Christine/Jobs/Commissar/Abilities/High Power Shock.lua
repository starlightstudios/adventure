-- |[ ==================================== High Power Shock ==================================== ]|
-- |[Description]|
--Deals greatly increased weapon-secondary damage. Costs CP. Triggers Spearguard.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.Commissar.HighPowerShock == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Commissar", "High Power Shock", "$SkillName", gciJP_Cost_Advanced, gbIsNotFreeAction, "Direct", "Combo", "Christine|HighPowerShock", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Inflict [DamFactor]x [Atk] as weapon secondary. [Target].\nCannot miss.\n\n\n\nTriggers Spearguard.\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Supercharge your spear, dealing massive\ndamage.\nUses your weapon's secondary damage type.\n\nTriggers Spearguard.\nCosts [CPCost][CPIco](CP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, 6, giNoCooldown, "Target Enemies Single", giNoCPGeneration)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Weapon")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Slashing, 5, 4.50)
    
    --Ensuring the skill always hits.
    zAbiStruct.zExecutionAbiPackage:fnAlwaysHit()
    
    -- |[Execution Self-Effect Package - Spearguard]|
    --Basic package.
    zAbiStruct.zExecutionSelfPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides for Spearguard.
    zAbiStruct.zExecutionSelfPackage.sEffectPath     = fnResolvePath() .. "../../Common/Effects/Spearguard.lua"
    zAbiStruct.zExecutionSelfPackage.sEffectCritPath = fnResolvePath() .. "../../Common/Effects/Spearguard.lua"
    zAbiStruct.zExecutionSelfPackage.sApplyText = "Spearguard"
    zAbiStruct.zExecutionSelfPackage.sApplyTextCol = "Color:White"
    zAbiStruct.zExecutionSelfPackage.sApplyAnimation = "Null"
    zAbiStruct.zExecutionSelfPackage.sApplySound = "Combat\\|Impact_Buff"
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Special|Ability:HighPowerShock"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Add a manual line to the prediction to reflect Spearguard.
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "Triggers Spearguard"
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
    
    --Set prototype.
    gzPrototypes.Combat.Christine.Commissar.HighPowerShock = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.Commissar.HighPowerShock

-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--Updates the damage type with the owner's weapon info.
if(iSwitchType == gciAbility_BuildPredictionBox) then

    -- |[Get Damage Type]|
    --Update the ref ability with the damage type.
    AdvCombatAbility_SetProperty("Push Owner")
        local iSecondaryDamageType, sAnimString = fnResolveChristineWeaponSecondary()
    DL_PopActiveObject()
    gzRefAbility.zPredictionPackage.zDamageModule.iDamageType = iSecondaryDamageType
    
    -- |[Call]|
    --Call standard handler.
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Updates the damage type with the owner's weapon info.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[Get Damage Type]|
    --Update the ref ability with the damage type.
    AdvCombatAbility_SetProperty("Push Owner")
        local iSecondaryDamageType, sAnimString = fnResolveChristineWeaponSecondary()
    DL_PopActiveObject()
    gzRefAbility.zExecutionAbiPackage.iDamageType = iSecondaryDamageType
    
    -- |[Animation]|
    for i = 1, #gzaAbiPacks, 1 do
        if(gzaAbiPacks[i].sLookup == iSecondaryDamageType) then
            gzRefAbility.zExecutionAbiPackage.sAttackAnimation = gzaAbiPacks[i].sAttackAnim
            gzRefAbility.zExecutionAbiPackage.sAttackSound     = gzaAbiPacks[i].sAttackSound
            gzRefAbility.zExecutionAbiPackage.sCriticalSound   = gzaAbiPacks[i].sCriticalSound
            break
        end
    end
    
    -- |[Call]|
    --Call standard handler.
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
    
-- |[ ======================================== Standard ======================================== ]|
--Call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
