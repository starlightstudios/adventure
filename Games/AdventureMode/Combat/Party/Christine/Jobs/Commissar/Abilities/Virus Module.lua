-- |[ ====================================== Virus Module ====================================== ]|
-- |[Description]|
--Inflicts 100 stun. Cannot miss. does no damage.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Christine.Commissar.VirusModule == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Commissar", "Virus Module", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "Tiffany|LockdownVirus", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Inflicts 100 [Stun]. [Target].[BR]Cannot miss.[BR][BR][BR][BR]1 [Turns] cooldown.[BR][Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Inject a virus to disable the enemy.[BR]Does no damage, inflicts stun.[BR]Cannot miss.[BR]1 turn cooldown.[BR][BR]Costs [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(30, giNoCPCost, 2, "Target Enemies Single", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Strike")
    --zAbiStruct.zExecutionAbiPackage:fnAddStun(piStun)
    zAbiStruct.zExecutionAbiPackage:fnAddStun(100)
    
    --Applying overrides to ensure it always hits and deals no damage.
    zAbiStruct.zExecutionAbiPackage:fnAlwaysHit()
    zAbiStruct.zExecutionAbiPackage.bDoesNoDamage  = true
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Christine|Ability:VirusModule"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.Christine.Commissar.VirusModule = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Christine.Commissar.VirusModule

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
