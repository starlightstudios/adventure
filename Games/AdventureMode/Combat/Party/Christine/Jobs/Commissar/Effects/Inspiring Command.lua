-- |[ =================================== Inspiring Command ==================================== ]|
-- |[Description]|
--Increases critical strike damage bonus by 0.50x

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Local Variables ===================================== ]|
-- |[System]|
--This effect uses a standardized StatMod script.
local sDisplayName = "Inspiring Command"
local iDuration = -1
local bIsBuff = true
local sIcon = "Root/Images/AdventureUI/Abilities/Christine|InspiringCommand"
local sStatString = ""

--Description can be a variable number of lines.
local saDescription = {}
saDescription[1] = "The command unit knows what's best!"
saDescription[2] = "Increases critical strike damage. Passive."

--Run the standardized builder.
gzaStatModStruct            = fnCreateStatmodEffectPrototype(sDisplayName, iDuration, bIsBuff, sIcon, sStatString)
gzaStatModStruct.saStrings  = fnStatmodBuildInspectorStrings(gzaStatModStruct, saDescription)
gzaStatModStruct.sShortText = fnStatmodBuildShortText(gzaStatModStruct)

-- |[Overrides]|
--Tags.
gzaStatModStruct.zaTagList = {{"Is Positive", 1}, {"Critical Damage Bonus", 50}, {"Is Passive", 1}}

--Passive, not removed on KO, uses different frame.
gzaStatModStruct.sFrame      = gsAbility_Frame_Passive
gzaStatModStruct.bRemoveOnKO = false

--Manually specify description.
gzaStatModStruct.saStrings[1] = "[Buff]Inspiring Command"
gzaStatModStruct.saStrings[2] = "[UpN]Crit Power"
gzaStatModStruct.saStrings[3] = saDescription[1]
gzaStatModStruct.saStrings[4] = saDescription[2]
gzaStatModStruct.saStrings[5] = ""
gzaStatModStruct.saStrings[6] = ""
gzaStatModStruct.saStrings[7] = ""
gzaStatModStruct.saStrings[8] = "[Buff] +Crit Damage"
gzaStatModStruct.sShortText = ""

-- |[ ======================================== Creation ======================================== ]|
--Standard call when effect is initialized.
if(iSwitchType == gciEffect_Create) then
    
    --Argument handling.
    local fSeverity = 1.0
    if(iArgumentsTotal >= 2) then fSeverity = LM_GetScriptArgument(1, "N") end
    
    --Tag list needs to be modified with the owner's ID.
    local iOriginatorID = AdvCombatEffect_GetProperty("ID of Originator")
    gzaStatModStruct.zaTagList = {{"Is Positive", 1}, {"Critical Damage Bonus", 50}, {"Inspiring Command " .. iOriginatorID, 1}, {"Is Passive", 1}}
    
    --Stats.
    LM_ExecuteScript(gsStandardStatPath, gciEffect_Create, fSeverity)
    
-- |[ ================================== Apply/Unapply Stats =================================== ]|
--Calls the apply/unapply stat routines.
elseif((iSwitchType == gciEffect_ApplyStats or iSwitchType == gciEffect_UnApplyStats) and iArgumentsTotal >= 2) then
    local iTargetID = math.floor(tonumber(LM_GetScriptArgument(1, "N")))
    LM_ExecuteScript(gsStandardStatPath, iSwitchType, iTargetID)

-- |[ ================================== Character Action Ends ================================= ]|
--Called after a character action ends to decrement the turn counter.
elseif(iSwitchType == gciEffect_PostAction) then
    LM_ExecuteScript(gsStandardStatPath, gciEffect_PostAction)
end