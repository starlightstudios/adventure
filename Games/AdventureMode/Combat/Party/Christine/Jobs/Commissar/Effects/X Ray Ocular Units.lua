-- |[ =================================== X Ray Ocular Units =================================== ]|
-- |[Description]|
--Zeroes enemy protection.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Local Variables ===================================== ]|
-- |[System]|
--This effect uses a standardized StatMod script.
local sDisplayName = "X Ray Ocular Units"
local iDuration = 3
local bIsBuff = false
local sIcon = "Root/Images/AdventureUI/Abilities/Christine|XRayOcularUnits"
local sStatString = ""

--Description can be a variable number of lines.
local saDescription = {}
saDescription[1] = "Command Units know your weaknesses."
saDescription[2] = "Zeroes [Prt](Protection)."
saDescription[4] = ""
saDescription[5] = ""
saDescription[6] = ""
saDescription[7] = ""
saDescription[8] = ""

--Run the standardized builder.
gzaStatModStruct            = fnCreateStatmodEffectPrototype(sDisplayName, iDuration, bIsBuff, sIcon, sStatString)
gzaStatModStruct.saStrings  = fnStatmodBuildInspectorStrings(gzaStatModStruct, saDescription)
gzaStatModStruct.sShortText = fnStatmodBuildShortText(gzaStatModStruct)

-- |[Overrides]|
--None Yet.

-- |[Tags]|
gzaStatModStruct.zaTagList = {{"Is Positive", 1}}

-- |[ ======================================== Creation ======================================== ]|
--Standard call when effect is initialized.
if(iSwitchType == gciEffect_Create) then
    LM_ExecuteScript(gsStandardStatPath, gciEffect_Create)
    
-- |[ ================================== Apply/Unapply Stats =================================== ]|
--Calls the apply/unapply stat routines.
elseif((iSwitchType == gciEffect_ApplyStats or iSwitchType == gciEffect_UnApplyStats) and iArgumentsTotal >= 2) then
    
    -- |[Variables]|
    --ID of Effect.
    local iUniqueID = RO_GetID()

    --Get the target's ID.
    local iTargetID = math.floor(tonumber(LM_GetScriptArgument(1, "N")))
    
    -- |[Applying]|
    --Determine their protection value when applying.
    if(iSwitchType == gciEffect_ApplyStats) then
        
        --Get value.
        AdvCombat_SetProperty("Push Entity By ID", iTargetID)
            local iProtection = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Protection)
        DL_PopActiveObject()
    
        --If the value is 5 or lower, then the protect penalty is 5. This ability always reduces protection
        -- by at least 5.
        if(iProtection < 5) then iProtection = 5 end
    
        --Store it.
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iProtection", "N", iProtection)
    
        --Set the stat string.
        gzaStatModStruct.sStatString = "Prt|Flat|-" .. iProtection
    
    -- |[Removing]|
    --If removing, get the protection value out of storage.
    else
    
        --Retrieve.
        local iProtection = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iProtection", "N")
    
        --Set the stat string.
        gzaStatModStruct.sStatString = "Prt|Flat|-" .. iProtection
    
    end

    -- |[Execute Standard]|
    --Run.
    LM_ExecuteScript(gsStandardStatPath, iSwitchType, iTargetID)
    
    -- |[Clean]|
    --Clean up.
    gzaStatModStruct.sStatString = ""

-- |[ ================================== Character Action Ends ================================= ]|
--Called after a character action ends to decrement the turn counter.
elseif(iSwitchType == gciEffect_PostAction) then
    LM_ExecuteScript(gsStandardStatPath, gciEffect_PostAction)
end
