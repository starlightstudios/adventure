-- |[ ================================== Build Secondary Menu ================================== ]|
--Christine can change classes in combat. This subroutine determines the current class and populates
-- what classes are available.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

-- |[Special]|
--The menu has no entries if Christine has not met 55 in the basement of Regulus City. When this
-- happens, she can transform for the rest of the chapter.
local iMet55InBasement = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InBasement", "N")
if(iMet55InBasement == 0.0) then return end

-- |[ ==================================== List Construction =================================== ]|
--If not done already, construct a list of all job changes available.
gzaChristineSecondaryMenu = nil
if(gzaChristineSecondaryMenu == nil) then

    --Adder function.
    local zaList = {}
    local function fnAdd(psVarName, psExclusiveName, piXPos, piYPos, psAbilityName)
        local i = #zaList + 1
        zaList[i] = {}
        zaList[i].sVariable = psVarName
        zaList[i].sExclusiveName = psExclusiveName
        zaList[i].iXPos = piXPos
        zaList[i].iYPos = piYPos
        zaList[i].sAbilityName = psAbilityName
    end

    --List.
    fnAdd("TRUE",                                                  "Human",      0, 0, "JobChange|Job Change - Lancer")
    fnAdd("Root/Variables/Global/Christine/iHasGolemForm",         "Golem",      1, 0, "JobChange|Job Change - Repair Unit")
    fnAdd("Root/Variables/Global/Christine/iHasLatexForm",         "LatexDrone", 2, 0, "JobChange|Job Change - Drone")
    fnAdd("Root/Variables/Global/Christine/iHasDarkmatterForm",    "Darkmatter", 3, 0, "JobChange|Job Change - Starseer")
    fnAdd("Root/Variables/Global/Christine/iHasEldritchForm",      "Eldritch",   4, 0, "JobChange|Job Change - Handmaiden")
    fnAdd("Root/Variables/Global/Christine/iHasElectrospriteForm", "Electro",    5, 0, "JobChange|Job Change - Live Wire")
    fnAdd("Root/Variables/Global/Christine/iHasRaijuForm",         "Raiju",      6, 0, "JobChange|Job Change - Thunder Rat")
    fnAdd("Root/Variables/Global/Christine/iHasDollForm",          "Doll",       0, 1, "JobChange|Job Change - Commissar")
    fnAdd("Root/Variables/Global/Christine/iHasSteamDroidForm",    "SteamDroid", 1, 1, "JobChange|Job Change - Brass Brawler")
    fnAdd("Root/Variables/Global/Christine/iHasSecrebotForm",      "Secrebot",   2, 1, "JobChange|Job Change - Menialist")
    fnAdd("Root/Variables/Global/Christine/iHasRaibieForm",        "Raibie",     3, 1, "JobChange|Job Change - Rage Rat")
    
    --Store.
    gzaChristineSecondaryMenu = zaList
end

-- |[ ==================================== Menu Construction =================================== ]|
--Build the menu layout.
for i = 1, #gzaChristineSecondaryMenu, 1 do

    --Compute position.
    local iX = gciAbility_Tactics_FormStartX + gzaChristineSecondaryMenu[i].iXPos
    local iY = gciAbility_Tactics_FormStartY + gzaChristineSecondaryMenu[i].iYPos
    
    --If the variable name is "TRUE" then always place this.
    if(gzaChristineSecondaryMenu[i].sVariable == "TRUE") then
        AdvCombatEntity_SetProperty("Set Ability Slot", iX, iY, gzaChristineSecondaryMenu[i].sAbilityName)
    
    --Otherwise, the variable must be 1.0.
    elseif(VM_GetVar(gzaChristineSecondaryMenu[i].sVariable, "N") == 1.0) then
        AdvCombatEntity_SetProperty("Set Ability Slot", iX, iY, gzaChristineSecondaryMenu[i].sAbilityName)
    end
end