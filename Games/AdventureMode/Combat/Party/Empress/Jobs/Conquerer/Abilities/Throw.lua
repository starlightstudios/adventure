-- |[ ========================================= Throw ========================================== ]|
-- |[Description]|
--Hits one selected and one random enemy for strike damage. If only one enemy exists, only hits
-- the targeted enemy.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzEmpressConquererThrow == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Conquerer"
    zAbiStruct.sSkillName    = "Throw"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Empress|Conquerer|Throw"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\n\n\n\n[Costs]"
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Hits two bird with one stone when one of them\n*is* the stone!\nDeals strike damage to the target and another\nrandom enemy.\n\nCosts [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false
    zAbiStruct.iRequiredMP = 25
    zAbiStruct.iRequiredCP = 0
    zAbiStruct.bRespectsCooldown = true
    zAbiStruct.iCooldown = 0
    zAbiStruct.bIsFreeAction = false
    zAbiStruct.iRequiredFreeActions = 0
    zAbiStruct.bRespectActionCap = false

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies Single"

    --Effects
    zAbiStruct.iCPGain = 1
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Primary Package]|
    --Package used to display predictions about the ability based on enemy properties.
    --All the modules are optional, but at least one should exist to display *something*.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Hit-Rate Module
    zAbiStruct.zPredictionPackage.bHasHitRateModule = true
    zAbiStruct.zPredictionPackage.zHitRateModule.iMissRate = 5
    
    --Crit Module
    zAbiStruct.zPredictionPackage.bHasCritModule = true
    zAbiStruct.zPredictionPackage.zCritModule.iCritThreshold = 100
    zAbiStruct.zPredictionPackage.zCritModule.fCritBonus     = 1.25
    zAbiStruct.zPredictionPackage.zCritModule.iCritStun      = 25
    
    --Damage Module
    zAbiStruct.zPredictionPackage.bHasDamageModule = true
    zAbiStruct.zPredictionPackage.zDamageModule.bUseWeaponType = false
    zAbiStruct.zPredictionPackage.zDamageModule.iDamageType = gciDamageType_Striking
    zAbiStruct.zPredictionPackage.zDamageModule.fDamageFactor = 1.20
    zAbiStruct.zPredictionPackage.zDamageModule.iScatterRange = 15
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Basic Execution]|
    --Basic package.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Strike")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.iDamageType    = gciDamageType_Striking
    zAbiStruct.zExecutionAbiPackage.iMissThreshold = 5
    zAbiStruct.zExecutionAbiPackage.fDamageFactor  = 1.20
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits    = true
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzEmpressConquererThrow = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzEmpressConquererThrow

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
if(iSwitchType == gciAbility_Execute) then

    -- |[ ====================== One Target ====================== ]|
    --If there is exactly one enemy, then just run a normal attack against that enemy. Done!
    local iEnemyPartySize = AdvCombat_GetProperty("Enemy Party Size")
    if(iEnemyPartySize == 1) then
        LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
        return
    end

    -- |[ ================ Originator Statistics ================= ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
    
        --Get ID.
        local iOriginatorID = RO_GetID()
        
        --CP handler.
        if(gzRefAbility.iCPGain ~= nil and gzRefAbility.iRequiredCP ~= nil) then
            fnModifyCP(gzRefAbility.iCPGain - gzRefAbility.iRequiredCP)
        end
        
        --MP handler.
        if(gzRefAbility.iRequiredMP ~= nil) then
            fnModifyMP(gzRefAbility.iRequiredMP * -1)
        end
    
    DL_PopActiveObject()
    
    -- |[ ==================== Ability Package =================== ]|
    --Create an ability package. This package comes with the default hit/miss values, animations, etc.
    local zAbilityPackage = gzRefAbility.zExecutionAbiPackage
    zAbilityPackage.iOriginatorID = iOriginatorID
    
    -- |[ ==================== For Each Target =================== ]|
    --Storage.
    local iMainTargetID = 0
    
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        
        --Get target ID. We need to store the main target ID here.
        AdvCombat_SetProperty("Push Target", i-1)
            local iTargetID = RO_GetID()
            iMainTargetID = iTargetID
        DL_PopActiveObject()
        
        --Call standard function.
        local zOldRefAbility = gzRefAbility
        fnStandardExecution(iOriginatorID, i-1, zAbilityPackage, zEffectPackage, zEffectPackageB, zEffectPackageC)
        gzRefAbility = zOldRefAbility
        
        --If the ref ability has a threat multiplier, set it here. This applies even if the ability misses.
        --Note that the threat multiplier can be zero, or negative.
        if(gzRefAbility.fThreatMultiplier ~= nil) then
            
            --Get threat values from the originator.
            AdvCombat_SetProperty("Push Event Originator")
                local iOriginatorAttackPower = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
                local fThreatMultiplier = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_ThreatMultiplier)
            DL_PopActiveObject()
        
            --Compute.
            local iThreatToApply = math.floor(iOriginatorAttackPower * (fThreatMultiplier / 100.0) * gzRefAbility.fThreatMultiplier)
            if(iThreatToApply > 0) then
                AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetID, 0, "AI_APPLICATION|Threat|" .. iThreatToApply)
            end
        end
    end
    
    -- |[ =================== Secondary Impact =================== ]|
    --Select another target from the enemy party, excluding the one we already hit.
    local iaEnemyIDs = {}
    for i = 0, iEnemyPartySize-1, 1 do
        
        --Get ID.
        local iThisEnemyID = AdvCombat_GetProperty("Enemy ID", i)
    
        --If it's not the one we initially targeted, add it.
        if(iThisEnemyID ~= iMainTargetID) then
            table.insert(iaEnemyIDs, iThisEnemyID)
        end
    end
    
    --Select one of the IDs.
    local iRoll = LM_GetRandomNumber(1, #iaEnemyIDs)
    local iSecondaryTargetID = iaEnemyIDs[iRoll]
    
    --Perform the attack against them, as well.
    fnAbilityHandler(iOriginatorID, iSecondaryTargetID, 1, zAbilityPackage, nil)
    
-- |[ ==================================== Standard Handler ==================================== ]|
--Call standard handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
