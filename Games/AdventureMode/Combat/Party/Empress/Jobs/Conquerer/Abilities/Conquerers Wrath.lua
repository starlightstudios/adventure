-- |[ =================================== Conquerer's Wrath ==================================== ]|
-- |[Description]|
--Aura effect for Empress, increases freeze and strike damage dealt by everyone in the party.

-- |[Notes]|
--For Code gciAbility_Create, the AdvCombatEntity that will own the ability is active.
--For Code gciAbility_AssumeJob, the AdvCombatEntity is active.
--For all other codes, the active object is the AdvCombatAbility itself. The owning AdvCombatEntity
-- and AdvCombatJob can be pushed with functions.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ======================================== Creation ======================================== ]|
--Called when the ability is created and added to the master list of abilities for the character.
-- It is not equipped at this point.
if(iSwitchType == gciAbility_Create or iSwitchType == gciAbility_CreateSpecial) then
    
    --Variables
    local sJobName = "Conquerer"
    local sSkillName = "Conquerer's Wrath"
    
    --Special: If the ability has an internal version, the name changes. This is used for abilities
    -- that can both be purchased for JP and exist on the job skill bar by default.
    if(iSwitchType == gciAbility_CreateSpecial) then sJobName = "Conquerer Internal" end
    
    --Create.
    AdvCombatEntity_SetProperty("Create Ability", sJobName .. "|" .. sSkillName)
    
        --System
        AdvCombatAbility_SetProperty("Script Path", LM_GetCallStack(0))
        AdvCombatAbility_SetProperty("JP Cost", gciJP_Cost_Passive)
    
        --Display
        AdvCombatAbility_SetProperty("Display Name", sSkillName)
        AdvCombatAbility_SetProperty("Icon Back",    gsAbility_Backing_Buff)
        AdvCombatAbility_SetProperty("Icon Frame",   gsAbility_Frame_Passive)
        AdvCombatAbility_SetProperty("Icon",         "Root/Images/AdventureUI/Abilities/Empress|Conquerer|ConquerersWrath")
        
        --Description
        AdvCombatAbility_SetProperty("Description", "Increases all [IMG0]/[IMG1] damage dealt by the party by 10%%.\nEffect only appears in battle.\n\n\n\n\nPassive Aura.")
        AdvCombatAbility_SetProperty("Allocate Description Images", 2)
        AdvCombatAbility_SetProperty("Description Image", 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DamageTypeIcons/Striking")
        AdvCombatAbility_SetProperty("Description Image", 1, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/DamageTypeIcons/Freezing")
        AdvCombatAbility_SetProperty("Crossload Description Images")
        
        --Simplified Description
        AdvCombatAbility_SetProperty("Simplified Description", "Your fury is the stuff of legend.\nIncreases party freeze/strike damage.\n\n\nEffect only appears in battle.\nPassive.")
        AdvCombatAbility_SetProperty("Allocate Simplified Description Images", 0)
        AdvCombatAbility_SetProperty("Crossload Simplified Description Images")
        
        --Response Flags
        AdvCombatAbility_SetProperty("Script Response", gciAbility_BeginAction, true)
        
    DL_PopActiveObject()

-- |[ ======================================== Job Taken ======================================= ]|
--Called when the character in question activates the job that contains this ability. This does not
-- occur outside of combat (a different script handles that), this is the in-combat variant. If an
-- ability does something when the owner changes Jobs, that should be done here.
--This is called after the job change has occurred.
elseif(iSwitchType == gciAbility_AssumeJob) then

-- |[ ================================== Combat: Combat Begins ================================= ]|
--Called when combat begins, to set up any local variables. Note that the Turn Begins code will be
-- called next, *after* turn order is resolved.
elseif(iSwitchType == gciAbility_BeginCombat) then

-- |[ =================================== Combat: Turn Begins ================================== ]|
--Called when the turn begins after turn order is resolved.
elseif(iSwitchType == gciAbility_BeginTurn) then

-- |[ ============================= Combat: Character Action Begins ============================ ]|
--Called when the character that possesses this ability begins their turn. This is where the ability
-- should enable or disable itself based on the character's MP/State/etc.
elseif(iSwitchType == gciAbility_BeginAction) then

    -- |[Documentation and Setup]|
    --Note: Does not bother checking if this is the owner's action. This is meant to handle party-switching
    -- cases, so the ability runs every single action.
    
    --Effect path.
    local sEffectPath = fnResolvePath() .. "../Effects/Conquerers Wrath.lua"

    --Get the party of the owner.
    local iOwnerID = AdvCombatAbility_GetProperty("Owner ID")
    local iOwnerParty = AdvCombat_GetProperty("Party Of ID", iOwnerID)
    
    -- |[Enemy Party]|
    --Scan all entities in the enemy party. This refers to the party the player does not control,
    -- it may be the party of the ability owner.
    local iEnemiesTotal = AdvCombat_GetProperty("Enemy Party Size")
    for i = 0, iEnemiesTotal-1, 1 do
        
        --Get ID and party.
        local iEnemyID = AdvCombat_GetProperty("Enemy ID", i)
        local iEnemyParty = AdvCombat_GetProperty("Party Of ID", iEnemyID)
        
        --Get the effect that this ability applies, as it may already exist on the target.
        AdvCombat_SetProperty("Push Entity By ID", iEnemyID)
            local iIDOfEffectWithTag = AdvCombatEntity_GetProperty("Effect ID With Tag", "Conquerers Wrath " .. iOwnerID, 0)
        DL_PopActiveObject()
        
        --If the parties match:
        if(iOwnerParty == iEnemyParty) then
        
            --If the effect is not applied, we need to apply it.
            if(iIDOfEffectWithTag == 0) then
                AdvCombat_SetProperty("Register Application Pack", iOwnerID, iEnemyID, 0, "Effect|" .. sEffectPath .. "|Originator:" .. iOwnerID)
            end
        
        --If the parties differ:
        else
        
            --If the effect is applied, remove it.
            if(iIDOfEffectWithTag ~= 0) then
                AdvCombat_SetProperty("Remove Effect", iIDOfEffectWithTag)
            end
        end
    end
    
    -- |[Player Party]|
    --Scan all entities in the player party. Do the same as above.
    local iPartyTotal = AdvCombat_GetProperty("Combat Party Size")
    for i = 0, iPartyTotal-1, 1 do
        
        --Get ID and party.
        local iPartyID = AdvCombat_GetProperty("Combat Party ID", i)
        local iPartyParty = AdvCombat_GetProperty("Party Of ID", iPartyID)
        
        --Get the effect that this ability applies, as it may already exist on the target.
        AdvCombat_SetProperty("Push Entity By ID", iPartyID)
            local iIDOfEffectWithTag = AdvCombatEntity_GetProperty("Effect ID With Tag", "Conquerers Wrath " .. iOwnerID, 0)
        DL_PopActiveObject()
        
        --If the parties match:
        if(iOwnerParty == iPartyParty) then
        
            --If the effect is not applied, we need to apply it.
            if(iIDOfEffectWithTag == 0) then
                AdvCombat_SetProperty("Register Application Pack", iOwnerID, iPartyID, 0, "Effect|" .. sEffectPath .. "|Originator:" .. iOwnerID)
            end
        
        --If the parties differ:
        else
        
            --If the effect is applied, remove it.
            if(iIDOfEffectWithTag ~= 0) then
                AdvCombat_SetProperty("Remove Effect", iIDOfEffectWithTag)
            end
        end
    end

-- |[ ============================== Combat: Character Free Action ============================= ]|
--Called after a character executes a free action. The ability should recompute whether or not it is
-- enabled based on the updated character state, including expended free actions.
elseif(iSwitchType == gciAbility_BeginFreeAction) then

-- |[ ============================== Combat: Character Action Ends ============================= ]|
--Called after a character executes any action. Used for passives or post-reaction abilities. Is not
-- called if the action was a free action (that's above).
elseif(iSwitchType == gciAbility_PostAction) then

-- |[ ================================== Combat: Paint Targets ================================= ]|
--For player-controlled characters, selects which targets can be used by this ability. AI-controlled
-- characters may optionally bypass this and select targets directly.
--Previous targets are implicitly cleared before this is called.
--If no targets are painted, the UI will play a fail sound and disable target selection.
elseif(iSwitchType == gciAbility_PaintTargets) then
    
-- |[ ============================= Combat: Paint Targets Response ============================= ]|
--If this ability fires as a response to another ability being enqueued, this section gets called to
-- paint targets. The zeroth target cluster is the one that gets used if more than one exists.
elseif(iSwitchType == gciAbility_PaintTargetsResponse) then
    
-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--When the player is selecting targets, a little box appears above the targets. This is the prediction
-- box. This script is called once for each target cluster created when this ability is selected, and
-- prediction boxes can be made for entities in that target cluster.
--The AdvCombatAbility will be the active object.
elseif(iSwitchType == gciAbility_BuildPredictionBox) then

-- |[ =================================== Combat: Can Execute ================================== ]|
--Flags whether or not the event can actually run. If the originator got KOd by a DoT or a counterattack
-- or any other event, this will mark as false, and the event will stop here.
--This is only called for the ability in question.
elseif(iSwitchType == gciAbility_QueryCanRun) then

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

-- |[ ==================================== Combat: Turn Ends =================================== ]|
--Called after the turn ends, before turn order for the next turn is resolved. The ability does not
-- need to have been used for this to be called.
elseif(iSwitchType == gciAbility_TurnEnds) then

-- |[ =================================== Combat: Combat Ends ================================== ]|
--Called after combat ends. The passed in flag dictates if Victory/Defeat occurred. This occurs
-- before Doctor Bag/XP/JP etc is awarded.
elseif(iSwitchType == gciAbility_CombatEnds) then

end
