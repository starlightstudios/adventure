-- |[ ====================================== Canecussion ======================================= ]|
-- |[Description]|
--Hits one enemy for 1.0x freeze damage and 100 stun.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzEmpressConquererCanecussion == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Conquerer"
    zAbiStruct.sSkillName    = "Canecussion"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = "Cane-cussion"
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Empress|Conquerer|Canecussion"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\n\n\n\n[Costs]"
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Bash them with your icy cane!\nDeals [Frz](Freeze) damage and inflicts stun.\n\n\n\nCosts [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false
    zAbiStruct.iRequiredMP = 30
    zAbiStruct.iRequiredCP = 0
    zAbiStruct.bRespectsCooldown = true
    zAbiStruct.iCooldown = 0
    zAbiStruct.bIsFreeAction = false
    zAbiStruct.iRequiredFreeActions = 0
    zAbiStruct.bRespectActionCap = false

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies Single"

    --Effects
    zAbiStruct.iCPGain = 1
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Primary Package]|
    --Package used to display predictions about the ability based on enemy properties.
    --All the modules are optional, but at least one should exist to display *something*.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Hit-Rate Module
    zAbiStruct.zPredictionPackage.bHasHitRateModule = true
    zAbiStruct.zPredictionPackage.zHitRateModule.iMissRate = 5
    
    --Stun Module
    zAbiStruct.zPredictionPackage.bHasStunModule = true
    zAbiStruct.zPredictionPackage.zStunModule.iStunBase = 100
    
    --Crit Module
    zAbiStruct.zPredictionPackage.bHasCritModule = true
    zAbiStruct.zPredictionPackage.zCritModule.iCritThreshold = 100
    zAbiStruct.zPredictionPackage.zCritModule.fCritBonus     = 1.25
    zAbiStruct.zPredictionPackage.zCritModule.iCritStun      = 25
    
    --Damage Module
    zAbiStruct.zPredictionPackage.bHasDamageModule = true
    zAbiStruct.zPredictionPackage.zDamageModule.bUseWeaponType = false
    zAbiStruct.zPredictionPackage.zDamageModule.iDamageType = gciDamageType_Freezing
    zAbiStruct.zPredictionPackage.zDamageModule.fDamageFactor = 1.00
    zAbiStruct.zPredictionPackage.zDamageModule.iScatterRange = 15
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Basic Execution]|
    --Basic package.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Freeze")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.iDamageType     = gciDamageType_Freezing
    zAbiStruct.zExecutionAbiPackage.iMissThreshold  = 5
    zAbiStruct.zExecutionAbiPackage.fDamageFactor   = 1.00
    zAbiStruct.zExecutionAbiPackage.iBaseStunDamage = 100
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzEmpressConquererCanecussion = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzEmpressConquererCanecussion

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
