-- |[ ==================================== Freezing Breath ===================================== ]|
-- |[Description]|
--Hits for 1.20x freezing damage, +.35x if the enemy has at least one bleed effect.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzEmpressConquererFreezingBreath == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Conquerer"
    zAbiStruct.sSkillName    = "Freezing Breath"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Empress|Conquerer|FreezingBreath"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n+35%% damage if enemy has any [Bld] effects.\n\n\n\n[Costs]"
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Blast the enemy with freezing dragon breath.\nIncreases damage if the enemy is bleeding.\nHigh hit rate.\n\n\nCosts [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false
    zAbiStruct.iRequiredMP = 20
    zAbiStruct.iRequiredCP = 0
    zAbiStruct.bRespectsCooldown = true
    zAbiStruct.iCooldown = 0
    zAbiStruct.bIsFreeAction = false
    zAbiStruct.iRequiredFreeActions = 0
    zAbiStruct.bRespectActionCap = false

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies Single"

    --Effects
    zAbiStruct.iCPGain = 1
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Primary Package]|
    --Package used to display predictions about the ability based on enemy properties.
    --All the modules are optional, but at least one should exist to display *something*.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Hit-Rate Module
    zAbiStruct.zPredictionPackage.bHasHitRateModule = true
    zAbiStruct.zPredictionPackage.zHitRateModule.iMissRate = -20
    
    --Crit Module
    zAbiStruct.zPredictionPackage.bHasCritModule = true
    zAbiStruct.zPredictionPackage.zCritModule.iCritThreshold = 100
    zAbiStruct.zPredictionPackage.zCritModule.fCritBonus     = 1.25
    zAbiStruct.zPredictionPackage.zCritModule.iCritStun      = 25
    
    --Damage Module
    zAbiStruct.zPredictionPackage.bHasDamageModule = true
    zAbiStruct.zPredictionPackage.zDamageModule.bUseWeaponType = false
    zAbiStruct.zPredictionPackage.zDamageModule.iDamageType = gciDamageType_Freezing
    zAbiStruct.zPredictionPackage.zDamageModule.fDamageFactor = 1.20
    zAbiStruct.zPredictionPackage.zDamageModule.iScatterRange = 15
    
    -- |[Chaser]|
    zAbiStruct.zPredictionPackage.bHasChaserModules = true
    zAbiStruct.zPredictionPackage.zaChaserModules = {}
    zAbiStruct.zPredictionPackage.iChaserModulesTotal = 1
    zAbiStruct.zPredictionPackage.zaChaserModules[1] = {}
    zAbiStruct.zPredictionPackage.zaChaserModules[1].bConsumeEffect = false
    zAbiStruct.zPredictionPackage.zaChaserModules[1].sDoTTag = "Bleeding DoT"
    zAbiStruct.zPredictionPackage.zaChaserModules[1].fDamageFactorChangePerDoT = 0.35
    zAbiStruct.zPredictionPackage.zaChaserModules[1].fDamageRemainingFactor = 0.0
    zAbiStruct.zPredictionPackage.zaChaserModules[1].sConsumeString = "Consumes [IMG0] DoTs"
    zAbiStruct.zPredictionPackage.zaChaserModules[1].iConsumeImgCount = 1
    zAbiStruct.zPredictionPackage.zaChaserModules[1].saConsumeImgPath = {}
    zAbiStruct.zPredictionPackage.zaChaserModules[1].saConsumeImgPath[1] = "Root/Images/AdventureUI/DamageTypeIcons/Bleeding"
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Basic Execution]|
    --Basic package.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Freeze")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.iDamageType    = gciDamageType_Freezing
    zAbiStruct.zExecutionAbiPackage.iMissThreshold = -20
    zAbiStruct.zExecutionAbiPackage.fDamageFactor  = 1.20
    
    --Chaser module.
    zAbiStruct.zExecutionAbiPackage.bHasChaserModules = true
    zAbiStruct.zExecutionAbiPackage.zaChaserModules = {}
    zAbiStruct.zExecutionAbiPackage.iChaserModulesTotal = 1
    zAbiStruct.zExecutionAbiPackage.zaChaserModules[1] = {}
    zAbiStruct.zExecutionAbiPackage.zaChaserModules[1].bConsumeEffect = false
    zAbiStruct.zExecutionAbiPackage.zaChaserModules[1].sDoTTag = "Bleeding DoT"
    zAbiStruct.zExecutionAbiPackage.zaChaserModules[1].fDamageFactorChangePerDoT = 0.35
    zAbiStruct.zExecutionAbiPackage.zaChaserModules[1].fDamageRemainingFactor = 0.0
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzEmpressConquererFreezingBreath = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzEmpressConquererFreezingBreath

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
