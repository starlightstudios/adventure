-- |[ ======================================== Incoming ======================================== ]|
-- |[Description]|
--Buffs all protection. Increases Empress' threat to all enemies.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzEmpressConquererIncoming == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Conquerer"
    zAbiStruct.sSkillName    = "Incoming"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = "Incoming!"
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Cheap
    zAbiStruct.sIconBacking  = gsAbility_Backing_Heal
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Empress|Conquerer|Incoming"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "[Target].\n\n[FEffect] +10[Prt] / 3[Turns].\n+800 [Threat] vs All Enemies\n\n\n[Costs]"
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Tell your team to hit the dirt!\nBuffs all ally [Prt](Protection).\nCauses a lot of threat to all enemies.\n\n\nCosts [CPCost][CPIco](CP)."

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false  --Set to true to ignore all requirements.
    zAbiStruct.iRequiredMP = 0          --MP required to use the ability.
    zAbiStruct.iRequiredCP = 1           --CP required to use the ability.
    zAbiStruct.bRespectsCooldown = true  --If this ability has a cooldown, decrements it.
    zAbiStruct.iCooldown = 0             --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
    zAbiStruct.bIsFreeAction = false     --Flags as a free action.
    zAbiStruct.iRequiredFreeActions = 0  --Free actions set this as 1, everything else as 0.
    zAbiStruct.bRespectActionCap = false --Characters with multiple free actions have a hard action cap. This flag checks that cap.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Allies All"

    --Effects
    zAbiStruct.iCPGain = 0
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Create an Effect module
    zAbiStruct.zPredictionPackage.zaEffectModules[1] = fnCreateEffectModule()
    zAbiStruct.zPredictionPackage.zaEffectModules[1].bIsBuff = true
    zAbiStruct.zPredictionPackage.zaEffectModules[1].sResultScript = "+10 [IMG1] / 3 [IMG2]"
    zAbiStruct.zPredictionPackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/StatisticIcons/Protection", "Root/Images/AdventureUI/DebuffIcons/Clock"}

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    --Basic package.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Buff")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.bDoesNoDamage = true
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun = true
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits = true
    zAbiStruct.zExecutionAbiPackage.bNeverCrits = true
    zAbiStruct.zExecutionAbiPackage.bEffectAlwaysApplies = true
    
    --Additional Animations.
    fnAddAnimationPackage(zAbiStruct.zExecutionAbiPackage, "Buff Defense", 0)
    
    -- |[Execution Effect Package]|
    --Basic package.
    zAbiStruct.zExecutionEffPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides.
    zAbiStruct.zExecutionEffPackage.sEffectPath     = fnResolvePath() .. "../Effects/Move Move.lua" 
    zAbiStruct.zExecutionEffPackage.sEffectCritPath = fnResolvePath() .. "../Effects/Move Move.lua" 
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzEmpressConquererIncoming = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzEmpressConquererIncoming

-- |[ ==================================== Standard Handler ==================================== ]|
--Call the standardized handler.
if(iSwitchType ~= gciAbility_Execute) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--This ability applies 800 threat to all enemies.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[ ========== Originator Statistics ========== ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
    DL_PopActiveObject()
    
    --Standard execution. Handles the part where it gives allies a buff.
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
    
    -- |[ ============= Threat Application ============= ]|
    --Iterate across all enemies.
    local iEnemyPartySize = AdvCombat_GetProperty("Enemy Party Size")
    for i = 0, iEnemyPartySize - 1, 1 do

        -- |[Setup]|
        --Get ID:
        local iEnemyID = AdvCombat_GetProperty("Enemy ID", i)
        
        -- |[Apply Threat]|
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iEnemyID, 0, "AI_APPLICATION|Threat|1000")
        
        -- |[Animation]|
        --String construction
        local sString = "Create Animation|Terrify|AttackAnim" .. i
        
        --Upload:
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iEnemyID, i * 15, sString)
    end
end

