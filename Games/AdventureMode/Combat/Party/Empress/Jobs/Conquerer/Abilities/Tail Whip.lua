-- |[ ======================================= Tail Whip ======================================== ]|
-- |[Description]|
--Cannot miss, deals slash damage to one enemy.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzEmpressConquererTailWhip == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Conquerer"
    zAbiStruct.sSkillName    = "Tail Whip"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Empress|Conquerer|TailWhip"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n\n\n\n\n\n[Costs]"
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Whip the enemy with your tail!\nDeals increased slashing damage.\nCannot miss.\n\n\nCosts [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false
    zAbiStruct.iRequiredMP = 10
    zAbiStruct.iRequiredCP = 0
    zAbiStruct.bRespectsCooldown = true
    zAbiStruct.iCooldown = 0
    zAbiStruct.bIsFreeAction = false
    zAbiStruct.iRequiredFreeActions = 0
    zAbiStruct.bRespectActionCap = false

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies Single"

    --Effects
    zAbiStruct.iCPGain = 1
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Primary Package]|
    --Package used to display predictions about the ability based on enemy properties.
    --All the modules are optional, but at least one should exist to display *something*.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Crit Module
    zAbiStruct.zPredictionPackage.bHasCritModule = true
    zAbiStruct.zPredictionPackage.zCritModule.iCritThreshold = 100
    zAbiStruct.zPredictionPackage.zCritModule.fCritBonus     = 1.25
    zAbiStruct.zPredictionPackage.zCritModule.iCritStun      = 25
    
    --Damage Module
    zAbiStruct.zPredictionPackage.bHasDamageModule = true
    zAbiStruct.zPredictionPackage.zDamageModule.bUseWeaponType = false
    zAbiStruct.zPredictionPackage.zDamageModule.iDamageType = gciDamageType_Slashing
    zAbiStruct.zPredictionPackage.zDamageModule.fDamageFactor = 1.20
    zAbiStruct.zPredictionPackage.zDamageModule.iScatterRange = 15
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Basic Execution]|
    --Basic package.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Claw Slash")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.iDamageType    = gciDamageType_Slashing
    zAbiStruct.zExecutionAbiPackage.fDamageFactor  = 1.20
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits    = true
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzEmpressConquererTailWhip = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzEmpressConquererTailWhip

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
