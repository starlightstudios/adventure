-- |[ =============================== Empress Skillbook Handler ================================ ]|
--Call this file with the number of the skillbook in question, and JP and unlocks will be awarded as needed.
if(fnArgCheck(1) == false) then return end
local iLevel = LM_GetScriptArgument(0, "I")

-- |[Empress is Not In the Party]|
if(AdvCombat_GetProperty("Is Member In Active Party", "Empress") == false) then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Chronicles of the Dragon Empire.[P] Doesn't seem relevant for anyone in the party.)") ]])
    fnCutsceneBlocker()
    return
end

-- |[Activate Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)

-- |[Common Code]|
--Check if this skillbook has been read already. If not, award JP and handle unlocks.
local iCheckVar = VM_GetVar("Root/Variables/Global/Empress/iSkillbook" .. iLevel, "N")
if(iCheckVar == 0.0) then
    
    --Mark the skillbook.
    VM_SetVar("Root/Variables/Global/Empress/iSkillbook" .. iLevel, "N", 1.0)
    
    --Increment the skillbooks total.
    local iSkillbookTotal = VM_GetVar("Root/Variables/Global/Empress/iSkillbookTotal", "N") + 1
    VM_SetVar("Root/Variables/Global/Empress/iSkillbookTotal", "N", iSkillbookTotal)
    
    --Award JP/Abilities
    AdvCombat_SetProperty("Push Party Member", "Empress")
    
        --Common strings.
        local sString = "WD_SetProperty(\"Append\", \"Empress:[E|Neutral] (Skillbook found:: Chronicles of the Dragon Empire, Volume " .. iLevel .. ".)[B][C]\")"
        fnCutscene(sString)
    
        --JP.
        local iGlobalJP = AdvCombatEntity_GetProperty("Global JP")
        AdvCombatEntity_SetProperty("Current JP", iGlobalJP + 100)
        
        --Check if this unlocked a class ability.
        if(iSkillbookTotal >= 1.0 and iSkillbookTotal <= 4.0) then
            fnCutscene([[ Append("Empress:[E|Neutral] (Gained 100 JP for Empress!)[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] (Unlocked a new job ability slot for Empress!)") ]])
        else
            fnCutscene([[ Append("Empress:[E|Neutral] (Gained 100 JP for Empress!)") ]])
        end

        --Execute the current job's SwitchTo script.
        AdvCombatEntity_SetProperty("Push Job S", "Active")
            AdvCombatJob_SetProperty("Fire Script", gciJob_SwitchTo)
        DL_PopActiveObject()
        
    DL_PopActiveObject()

--Already found.
else
    local sString = "WD_SetProperty(\"Append\", \"Empress:[E|Neutral] (Chronicles of the Dragon Empire, Volume " .. iLevel .. ". You have already read this skillbook.)\")"
    fnCutscene(sString)
end