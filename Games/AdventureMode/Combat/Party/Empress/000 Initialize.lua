-- |[ ================================= Empress Initialization ================================= ]|
--Creates and stores the named character in the party roster. If the character already exists, pushes
-- the character and modifies their properties.
local sCharacterName = "Empress"

-- |[ ==================================== Unique Functions ==================================== ]|
-- |[ =================================== Character Handling =================================== ]|
-- |[Register or Push]|
--Register if character does not exist.
local bIsCreating = false
if(AdvCombat_GetProperty("Does Party Member Exist", sCharacterName) == false)  then
    bIsCreating = true
    AdvCombat_SetProperty("Register Party Member", sCharacterName)

--Push character.
else
    AdvCombat_SetProperty("Push Party Member", sCharacterName)
end

-- |[Call Script]|
AdvCombatEntity_SetProperty("Response Script", fnResolvePath() .. "100 Combat Script.lua")

-- |[Base Statistics]|
--Persistent Stats
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_HPMax,   180)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_MPMax,   100)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_MPRegen,  10)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_CPMax,    10)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_StunCap, 100)

--Free-Action Handlers
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_FreeActionMax, 3)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_FreeActionGen, 1)

--Combat Stats
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Initiative, 40)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Attack,     47)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Accuracy,   30)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Evade,      30)

--Base resistances.
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Protection,     3)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Slash,   gciNormalResist+2)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Strike,  gciNormalResist+2)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Pierce,  gciNormalResist+2)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Flame,   gciNormalResist+0)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Freeze,  gciNormalResist+1000)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Shock,   gciNormalResist+1)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Crusade, gciNormalResist+1)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Obscure, gciNormalResist+1)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Bleed,   gciNormalResist+2)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Poison,  gciNormalResist+2)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Corrode, gciNormalResist+1)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Terrify, gciNormalResist+8)

--Threat Multiplier. Default 100 for all entities.
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_ThreatMultiplier, 100)

-- |[ ================================== Job Statistics Chart ================================== ]|
--All jobs store their statistics here, in a big ungainly chart!
if(gzaEmpressJobChart == nil) then

    -- |[Creation Function]|
    --Adds elements to the reference chart.
    local gzaRefChart = {}
    local function fnAdd(psName, piHltConstant, pfHltPercent, piIniConstant, pfIniPercent, piAtkConstant, pfAtkPercent, piAccConstant, pfAccPercent, piEvdConstant, pfEvdPercent, 
                         piProtection, piSlash, piStrike, piPierce, piFlame, piFreeze, piShock, piCrusade, piObscure, piBleed, piPoison, piCorrode, piTerrify)
        
        --Setup.
        local i = #gzaRefChart + 1
        gzaRefChart[i] = {}
        
        --System.
        gzaRefChart[i].sName = psName
        
        --Stats.
        gzaRefChart[i].iHlt_Constant = piHltConstant
        gzaRefChart[i].fHlt_Percent  = pfHltPercent
        gzaRefChart[i].iIni_Constant = piIniConstant
        gzaRefChart[i].fIni_Percent  = pfIniPercent
        gzaRefChart[i].iAtk_Constant = piAtkConstant
        gzaRefChart[i].fAtk_Percent  = pfAtkPercent
        gzaRefChart[i].iAcc_Constant = piAccConstant
        gzaRefChart[i].fAcc_Percent  = pfAccPercent
        gzaRefChart[i].iEvd_Constant = piEvdConstant
        gzaRefChart[i].fEvd_Percent  = pfEvdPercent
        
        --Resistances.
        gzaRefChart[i].iBonus_Resist_Protection = piProtection
        gzaRefChart[i].iBonus_Resist_Slash = piSlash
        gzaRefChart[i].iBonus_Resist_Pierce = piPierce
        gzaRefChart[i].iBonus_Resist_Strike = piStrike
        gzaRefChart[i].iBonus_Resist_Flame = piFlame
        gzaRefChart[i].iBonus_Resist_Freeze = piFreeze
        gzaRefChart[i].iBonus_Resist_Shock = piShock
        gzaRefChart[i].iBonus_Resist_Crusade = piCrusade
        gzaRefChart[i].iBonus_Resist_Obscure = piObscure
        gzaRefChart[i].iBonus_Resist_Bleed = piBleed
        gzaRefChart[i].iBonus_Resist_Poison = piPoison
        gzaRefChart[i].iBonus_Resist_Corrode = piCorrode
        gzaRefChart[i].iBonus_Resist_Terrify = piTerrify
    end

    --     Actual Name ||||| HltC | HltP | IniC | IniP | AtkC | AtkP | AccC | AccP | EvdC | EvdP  ||||| Pr | Sl | St | Pc | Fl | Fr | Sh | Cr | Ob | Bl | Ps | Cr | Tr
    fnAdd("Conquerer",        9,  0.00,     0,  0.00,    10,  0.06,     5,  0.03,     3,  0.01,      0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0)

    --Save.
    gzaEmpressJobChart = gzaRefChart
end

-- |[ ===================================== Equipment Slots ==================================== ]|
--Primary Weapon
AdvCombatEntity_SetProperty("Create Equipment Slot", "Weapon")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Weapon", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Weapon", false)

--Uses primary weapon to compute damage type. Can be modified by gems.
AdvCombatEntity_SetProperty("Set Equipment Slot Used For Weapon Damage", "Weapon")

--Swappable Weapons
AdvCombatEntity_SetProperty("Create Equipment Slot", "Weapon Backup A")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Weapon Backup A", false)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Weapon Backup A", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Is Alt Weapon", "Weapon Backup A", true)
AdvCombatEntity_SetProperty("Create Equipment Slot", "Weapon Backup B")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Weapon Backup B", false)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Weapon Backup B", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Is Alt Weapon", "Weapon Backup B", true)

--Body Armor
AdvCombatEntity_SetProperty("Create Equipment Slot", "Body")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Body", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Body", true)

--Accessory Slots
AdvCombatEntity_SetProperty("Create Equipment Slot", "Accessory A")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Accessory A", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Accessory A", true)
AdvCombatEntity_SetProperty("Create Equipment Slot", "Accessory B")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Accessory B", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Accessory B", true)

--Equippable Items
AdvCombatEntity_SetProperty("Create Equipment Slot", "Item A")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Item A", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Item A", true)
AdvCombatEntity_SetProperty("Create Equipment Slot", "Item B")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Item B", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Item B", true)

-- |[ ====================================== Job Handling ====================================== ]|
--Create Empress's common abilities, shared by many classes.
local sJobAbilityPath = gsRoot .. "Combat/Party/Empress/Jobs/Common/Abilities/"
LM_ExecuteScript(sJobAbilityPath .. "Attack.lua", gciAbility_Create)
LM_ExecuteScript(sJobAbilityPath .. "Block.lua",  gciAbility_Create)
LM_ExecuteScript(sJobAbilityPath .. "Locked.lua", gciAbility_Create)

--Job Change Abilities
--LM_ExecuteScript(sJobAbilityPath .. "Jobchange_Hunter.lua", gciAbility_Create)

--Call all of Empress's job subscripts.
local sCharacterJobPath = gsRoot .. "Combat/Party/Empress/Jobs/"
LM_ExecuteScript(sCharacterJobPath .. "Conquerer/000 Job Script.lua",  gciJob_Create)
AdvCombatEntity_SetProperty("Rebuild Job Skills UI")

--Set Empress to her default job.
AdvCombatEntity_SetProperty("Active Job", "Conquerer")
VM_SetVar("Root/Variables/Global/Empress/sCurrentJob", "S", "Conquerer")

--Once all abilities are built across all jobs, order jobs to associate with abilities. Any abilities
-- associated with a job can be purchased in the skills UI using that job's JP, and equipped from
-- that job's submenu. If an ability is not associated with any job, and is not auto-equipped by
-- any jobs when the job is assumed, the ability is not usable by the player!
LM_ExecuteScript(sCharacterJobPath .. "Conquerer/000 Job Script.lua", gciJob_JobAssembleSkillList)
    
-- |[ ==================================== Creation Callback =================================== ]|
--If this was a creation call of the character, fire its script callback.
if(bIsCreating) then
    local sResponsePath = AdvCombatEntity_GetProperty("Response Path")
    LM_ExecuteScript(sResponsePath, gciCombatEntity_Create)
end

-- |[ ======================================== Clean Up ======================================== ]|
--Pop the activity stack.
DL_PopActiveObject()
