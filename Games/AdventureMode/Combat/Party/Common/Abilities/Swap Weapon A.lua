-- |[ ====================================== Swap Weapon A ===================================== ]|
-- |[Description]|
--Swaps to the weapon in the alternate weapon slot A.

-- |[Notes]|
--For Code gciAbility_Create, the AdvCombatEntity that will own the ability is active.
--For Code gciAbility_AssumeJob, the AdvCombatEntity is active.
--For all other codes, the active object is the AdvCombatAbility itself. The owning AdvCombatEntity
-- and AdvCombatJob can be pushed with functions.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ======================================== Creation ======================================== ]|
--Called when the ability is created and added to the master list of abilities for the character.
-- It is not equipped at this point.
if(iSwitchType == gciAbility_Create or iSwitchType == gciAbility_CreateSpecial) then
    
    --Create.
    AdvCombatEntity_SetProperty("Create Ability", "Common|WeaponSwapA")
    
        --System
        AdvCombatAbility_SetProperty("Script Path", LM_GetCallStack(0))
    
        --Display
        AdvCombatAbility_SetProperty("Display Name", "Swap to Backup Weapon A")
        AdvCombatAbility_SetProperty("Icon Back",    gsAbility_Backing_Free_Direct)
        AdvCombatAbility_SetProperty("Icon Frame",   gsAbility_Frame_Active)
        AdvCombatAbility_SetProperty("Icon",         "Root/Images/AdventureUI/Abilities/WeaponLft")
        
        --Description
        AdvCombatAbility_SetProperty("Description", "Immediately switch to the weapon in Backup Slot A.\nFree Action.")
        AdvCombatAbility_SetProperty("Allocate Description Images", 0)
        AdvCombatAbility_SetProperty("Crossload Description Images")
    
        --Simple Description.
        AdvCombatAbility_SetProperty("Simplified Description", "Switch to the weapon in Backup Slot A.\nFree Action.")
        AdvCombatAbility_SetProperty("Allocate Simplified Description Images", 0)
        AdvCombatAbility_SetProperty("Crossload Simplified Description Images")
        
        --Response Flags
        fnSetAbilityResponseFlags(gciAbility_ResponseStandard_DirectAction)
        
    DL_PopActiveObject()

-- |[ ======================================== Job Taken ======================================= ]|
--Called when the character in question activates the job that contains this ability. This does not
-- occur outside of combat (a different script handles that), this is the in-combat variant. If an
-- ability does something when the owner changes Jobs, that should be done here.
--This is called after the job change has occurred.
elseif(iSwitchType == gciAbility_AssumeJob) then

-- |[ ================================== Combat: Combat Begins ================================= ]|
--Called when combat begins, to set up any local variables. Note that the Turn Begins code will be
-- called next, *after* turn order is resolved.
elseif(iSwitchType == gciAbility_BeginCombat) then

-- |[ =================================== Combat: Turn Begins ================================== ]|
--Called when the turn begins after turn order is resolved.
elseif(iSwitchType == gciAbility_BeginTurn) then

-- |[ ============================= Combat: Character Action Begins ============================ ]|
--Called when the character that possesses this ability begins their turn. This is where the ability
-- should enable or disable itself based on the character's MP/State/etc.
elseif(iSwitchType == gciAbility_BeginAction) then
    if(AdvCombatAbility_GetProperty("Is Owner Acting") == true) then
        fnAbilityStandardBeginAction(0, true, 1, true)
    end

-- |[ ============================== Combat: Character Free Action ============================= ]|
--Called after a character executes a free action. The ability should recompute whether or not it is
-- enabled based on the updated character state, including expended free actions.
elseif(iSwitchType == gciAbility_BeginFreeAction) then
    if(AdvCombatAbility_GetProperty("Is Owner Acting") == true) then
        fnAbilityStandardBeginFreeAction(0, true, 1, true)
    end

-- |[ ============================== Combat: Character Action Ends ============================= ]|
--Called after a character executes any action. Used for passives or post-reaction abilities. Is not
-- called if the action was a free action (that's above).
elseif(iSwitchType == gciAbility_PostAction) then

-- |[ ================================== Combat: Paint Targets ================================= ]|
--For player-controlled characters, selects which targets can be used by this ability. AI-controlled
-- characters may optionally bypass this and select targets directly.
--Previous targets are implicitly cleared before this is called.
--If no targets are painted, the UI will play a fail sound and disable target selection.
elseif(iSwitchType == gciAbility_PaintTargets) then
    AdvCombat_SetProperty("Run Target Macro", "Target Self", AdvCombatAbility_GetProperty("Owner ID"))
    
-- |[ ============================= Combat: Paint Targets Response ============================= ]|
--If this ability fires as a response to another ability being enqueued, this section gets called to
-- paint targets. The zeroth target cluster is the one that gets used if more than one exists.
elseif(iSwitchType == gciAbility_PaintTargetsResponse) then
    
-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--When the player is selecting targets, a little box appears above the targets. This is the prediction
-- box. This script is called once for each target cluster created when this ability is selected, and
-- prediction boxes can be made for entities in that target cluster.
--The AdvCombatAbility will be the active object.
elseif(iSwitchType == gciAbility_BuildPredictionBox) then

    -- |[Get Statistics]|
    --Get equipment statistics.
    AdvCombatAbility_SetProperty("Push Owner")
    
        --Stats for equipped weapon.
        AdvCombatEntity_SetProperty("Push Item In Slot S", "Weapon")
            local sOrgName = AdItem_GetProperty("Name")
            local iOrgAtk = AdItem_GetProperty("Final Statistic", gciStatIndex_Attack)
            local iOrgIni = AdItem_GetProperty("Final Statistic", gciStatIndex_Initiative)
            local iOrgAcc = AdItem_GetProperty("Final Statistic", gciStatIndex_Accuracy)
            local iOrgEvd = AdItem_GetProperty("Final Statistic", gciStatIndex_Evade)
            local iOrgPrt = AdItem_GetProperty("Final Statistic", gciStatIndex_Protection)
        DL_PopActiveObject()
        
        --Stats for new weapon.
        AdvCombatEntity_SetProperty("Push Item In Slot S", "Weapon Backup A")
            local sNewName = AdItem_GetProperty("Name")
            local iNewAtk = AdItem_GetProperty("Final Statistic", gciStatIndex_Attack)
            local iNewIni = AdItem_GetProperty("Final Statistic", gciStatIndex_Initiative)
            local iNewAcc = AdItem_GetProperty("Final Statistic", gciStatIndex_Accuracy)
            local iNewEvd = AdItem_GetProperty("Final Statistic", gciStatIndex_Evade)
            local iNewPrt = AdItem_GetProperty("Final Statistic", gciStatIndex_Protection)
            local iHighestDamageType = gciDamageType_Slashing
            local fHighestDamageVal = 0.0
            
            --Check base damage type.
            for i = 0, gciDamageType_Total-1, 1 do
                local fDamageVal = AdItem_GetProperty("Damage Type", gciEquipment_DamageType_Base, i+gciDamageOffsetToResistances)
                if(fDamageVal > fHighestDamageVal) then
                    iHighestDamageType = i
                    fHighestDamageVal = fDamageVal
                end
            end
            
            --Check override damage type. The first override found does the override for all.
            for i = 0, gciDamageType_Total-1, 1 do
                local fDamageVal = AdItem_GetProperty("Damage Type", gciEquipment_DamageType_Override, i+gciDamageOffsetToResistances)
                if(fDamageVal > 0.0) then
                    iHighestDamageType = i
                    fHighestDamageVal = fDamageVal
                    break
                end
            end
            
        DL_PopActiveObject()
    
    DL_PopActiveObject()

    -- |[Description]|
    --Construct an array of strings. These have item icons.
    local saStringList = {}
    local saSymbolsList = {}
    
    --Setup.
    local iLine = 1
    local sBoxName = "PredictionBox"
    AdvCombat_SetProperty("Prediction Create Box",  sBoxName)
    AdvCombat_SetProperty("Prediction Set Offsets", sBoxName, 368, 140)
    
    --Heading.
    saStringList [iLine] = sOrgName .. " -> " .. sNewName
    saSymbolsList[iLine] = {}
    iLine = iLine + 1
    
    --Attack power. Always appears.
    saStringList [iLine] = "[IMG0] " .. iOrgAtk .. " -> " .. iNewAtk
    saSymbolsList[iLine] = {"Root/Images/AdventureUI/StatisticIcons/Attack"}
    iLine = iLine + 1
    
    --Initiative. Only appears if the value changes.
    if(iNewIni ~= iOrgIni) then
        saStringList [iLine] = "[IMG0] " .. iOrgIni .. " -> " .. iNewIni
        saSymbolsList[iLine] = {"Root/Images/AdventureUI/StatisticIcons/Initiative"}
        iLine = iLine + 1
    end
    
    --Accuracy. Always appears.
    saStringList [iLine] = "[IMG0] " .. iOrgAcc .. " -> " .. iNewAcc
    saSymbolsList[iLine] = {"Root/Images/AdventureUI/StatisticIcons/Accuracy"}
    iLine = iLine + 1
    
    --Evade. Always appears.
    saStringList [iLine] = "[IMG0] " .. iOrgEvd .. " -> " .. iNewEvd
    saSymbolsList[iLine] = {"Root/Images/AdventureUI/StatisticIcons/Evade"}
    iLine = iLine + 1
    
    --Damage Type.
    saStringList [iLine] = "Damage Type: [IMG0]"
    if(iHighestDamageType == gciDamageType_Slashing) then
        saSymbolsList[iLine] = {"Root/Images/AdventureUI/DamageTypeIcons/Slashing"}
    elseif(iHighestDamageType == gciDamageType_Striking) then
        saSymbolsList[iLine] = {"Root/Images/AdventureUI/DamageTypeIcons/Striking"}
    elseif(iHighestDamageType == gciDamageType_Piercing) then
        saSymbolsList[iLine] = {"Root/Images/AdventureUI/DamageTypeIcons/Piercing"}
    elseif(iHighestDamageType == gciDamageType_Flaming) then
        saSymbolsList[iLine] = {"Root/Images/AdventureUI/DamageTypeIcons/Flaming"}
    elseif(iHighestDamageType == gciDamageType_Freezing) then
        saSymbolsList[iLine] = {"Root/Images/AdventureUI/DamageTypeIcons/Freezing"}
    elseif(iHighestDamageType == gciDamageType_Shocking) then
        saSymbolsList[iLine] = {"Root/Images/AdventureUI/DamageTypeIcons/Shocking"}
    elseif(iHighestDamageType == gciDamageType_Crusading) then
        saSymbolsList[iLine] = {"Root/Images/AdventureUI/DamageTypeIcons/Crusading"}
    elseif(iHighestDamageType == gciDamageType_Obscuring) then
        saSymbolsList[iLine] = {"Root/Images/AdventureUI/DamageTypeIcons/Obscuring"}
    elseif(iHighestDamageType == gciDamageType_Bleeding) then
        saSymbolsList[iLine] = {"Root/Images/AdventureUI/DamageTypeIcons/Bleeding"}
    elseif(iHighestDamageType == gciDamageType_Poisoning) then
        saSymbolsList[iLine] = {"Root/Images/AdventureUI/DamageTypeIcons/Poisoning"}
    elseif(iHighestDamageType == gciDamageType_Corroding) then
        saSymbolsList[iLine] = {"Root/Images/AdventureUI/DamageTypeIcons/Corroding"}
    elseif(iHighestDamageType == gciDamageType_Terrifying) then
        saSymbolsList[iLine] = {"Root/Images/AdventureUI/DamageTypeIcons/Terrifying"}
    end
    iLine = iLine + 1
    
    -- |[Upload]|
    --Allocate strings, populate.
    AdvCombat_SetProperty("Prediction Allocate Strings", sBoxName, #saStringList)
    for i = 1, #saStringList, 1 do
        AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, i-1, saStringList[i])
        AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, i-1, #saSymbolsList[i])
        for p = 1, #saSymbolsList, 1 do
            AdvCombat_SetProperty("Prediction Set String Image", sBoxName, i-1, p-1, gcfAbilityImgOffsetY, saSymbolsList[i][p])
        end
    end
    
-- |[ =================================== Combat: Can Execute ================================== ]|
--Flags whether or not the event can actually run. If the originator got KOd by a DoT or a counterattack
-- or any other event, this will mark as false, and the event will stop here.
--This is only called for the ability in question.
elseif(iSwitchType == gciAbility_QueryCanRun) then
    AdvCombat_SetProperty("Push Event Originator")
        local bCanAct = AdvCombatEntity_GetProperty("Can Act")
    DL_PopActiveObject()
    if(bCanAct) then AdvCombat_SetProperty("Set Event Can Run", true) end

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[ ============ Originator Statistics =========== ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        AdvCombatEntity_SetProperty("Swap Equipment In Slots S", "Weapon", "Weapon Backup A")
    DL_PopActiveObject()
    
    -- |[ =============== Ability Package ============== ]|
    --Apply cooldown for this ability. Mark the action as free.
    AdvCombat_SetProperty("Set As Free Action")
    local iTimer = 0
    AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piOriginatorID, iTimer, "Text|Switched Weapons!")
    AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piOriginatorID, iTimer, "Play Sound|World|TakeWeapon")
    iTimer = iTimer + gciAbility_BlackFlashTicks
    fnDefaultEndOfApplications(iTimer)

-- |[ ==================================== Combat: Turn Ends =================================== ]|
--Called after the turn ends, before turn order for the next turn is resolved. The ability does not
-- need to have been used for this to be called.
elseif(iSwitchType == gciAbility_TurnEnds) then

-- |[ =================================== Combat: Combat Ends ================================== ]|
--Called after combat ends. The passed in flag dictates if Victory/Defeat occurred. This occurs
-- before Doctor Bag/XP/JP etc is awarded.
elseif(iSwitchType == gciAbility_CombatEnds) then

end
