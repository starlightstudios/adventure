-- |[ ==================================== Build Stat Profiles ==================================== ]|
--Used for the debug menu to compute stat profiles. This builds a list of which characters exist
-- and should have profiles available for their stats. It is called each time the profile menu is
-- opened, so you can add new characters on the fly.
local sBasePath = fnResolvePath()

-- |[Descriptions]|
--Individual jobs for individual characters have varying stats versus an average. The final of those
-- stats is what is usually compared between the two, as two jobs may have the same final value but
-- a different growth curve.
--The stats are scored according to this set of values
--==Lowest== Very Low, Low, Fairly Low, Below Average, Average, Above Average, Fairly High, High, Very High ==Highest==

--Most jobs "sum" to the same value of having average across the five varying stats, but not all do.
-- Some jobs may have lower stats but better resistances, or use abilities to make up for it.
--In addition, some characters have lower stats across the board for various reasons (like Jeanne).
--These are the comparison values for the stats:
--==Lowest==   VL|  LO|  FL|  BA| AVG|  AA|  FH|  HI|   VH ==Highest==
--Max HP    | 300| 400| 500| 575| 650| 725| 800| 950|1000+
--Attack    | 100| 130| 150| 170| 190| 220| 250| 280| 310+
--Accuracy  |  70|  80|  90| 110| 120| 130| 145| 160| 170+
--Evade     |  70|  80|  90| 110| 120| 130| 145| 160| 170+
--Initiative|  10|  15|  20|  25|  30|  35|  40|  45|  50

-- |[Lists]|
--Build profile lists to make it easy to edit them.
gsaProfiles = {}
fnAddProfile = function(psName, psPath)
    local i = #gsaProfiles
    gsaProfiles[i+1] = {}
    gsaProfiles[i+1].sName = psName
    gsaProfiles[i+1].sPath = psPath
end

-- |[Single Profile Characters]|
--These characters all use a single profile and apply percentage and constant bonuses by job.
fnAddProfile("Mei",        gsRoot .. "Combat/Party/Mei/"        .. "400 Common Stat Profile.lua")
fnAddProfile("Florentina", gsRoot .. "Combat/Party/Florentina/" .. "400 Common Stat Profile.lua")
fnAddProfile("Christine",  gsRoot .. "Combat/Party/Christine/"  .. "400 Common Stat Profile.lua")
fnAddProfile("Tiffany",    gsRoot .. "Combat/Party/Tiffany/"    .. "400 Common Stat Profile.lua")
fnAddProfile("SX-399",     gsRoot .. "Combat/Party/SX-399/"     .. "400 Common Stat Profile.lua")
fnAddProfile("Sanya",      gsRoot .. "Combat/Party/Sanya/"      .. "400 Common Stat Profile.lua")
fnAddProfile("Izuna",      gsRoot .. "Combat/Party/Izuna/"      .. "400 Common Stat Profile.lua")
fnAddProfile("Zeke",       gsRoot .. "Combat/Party/Zeke/"       .. "400 Common Stat Profile.lua")

-- |[Modded Profile Characters]|
fnExecModScript("Combat/Party/Stat Profile Builder.lua")

-- |[ ======================================= Execution ======================================== ]|
--Allocate.
ADebug_SetProperty("Allocate Profiles", #gsaProfiles)

--Iterate and populate.
for i = 1, #gsaProfiles, 1 do
    ADebug_SetProperty("Set Profile Base", i-1, gsaProfiles[i].sName, gsaProfiles[i].sPath)
end

--Clean up.
gsaProfiles = nil
fnAddProfile = nil
