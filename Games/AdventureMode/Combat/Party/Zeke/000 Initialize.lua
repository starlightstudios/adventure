-- |[ =================================== Zeke Initialization ================================== ]|
--Creates and stores the named character in the party roster. If the character already exists, pushes
-- the character and modifies their properties.
local sCharacterName = "Zeke"

-- |[ ==================================== Unique Functions ==================================== ]|
--Zeke's base statistics and growth rates. Used for his repurchaseable skills.
gzaZekeStats = {}
gzaZekeStats.iHPMax_Base = 140
gzaZekeStats.iHPMax_Grow = 6
gzaZekeStats.iAtk_Base = 34
gzaZekeStats.iAtk_Grow = 3
gzaZekeStats.iAcc_Base = 30
gzaZekeStats.iAcc_Grow = 3
gzaZekeStats.iEvd_Base = 30
gzaZekeStats.iEvd_Grow = 3
gzaZekeStats.iIni_Base = 60
gzaZekeStats.iIni_Grow = 5

--Tag Growth
gzaZekeStats.iItemTagsPerLevel = 2
gzaZekeStats.iStrikeTagsPerLevel = 2
gzaZekeStats.iBleedTagsPerLevel = 2

--Costs in JP
gzaZekeStats.iJPCostBase = 50
gzaZekeStats.iJPCostIncrease = 10

-- |[Tags]|
--Zeke has these tags to boost his various ability powers. They get modified directly by the repurchaseable
-- abilities. These are sample tags.
--AdvCombatEntity_SetProperty("Add Tag", "Item Power +", 0)
--AdvCombatEntity_SetProperty("Add Tag", "Strike Damage Dealt +", 0)
--AdvCombatEntity_SetProperty("Add Tag", "Bleed Damage Dealt +", 0)

-- |[ =================================== Character Handling =================================== ]|
-- |[Register or Push]|
--Register if character does not exist.
local bIsCreating = false
if(AdvCombat_GetProperty("Does Party Member Exist", sCharacterName) == false)  then
    bIsCreating = true
    AdvCombat_SetProperty("Register Party Member", sCharacterName)

--Push character.
else
    AdvCombat_SetProperty("Push Party Member", sCharacterName)
end

-- |[Call Script]|
AdvCombatEntity_SetProperty("Response Script", fnResolvePath() .. "100 Combat Script.lua")

-- |[Base Statistics]|
--Persistent Stats
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_HPMax,   gzaZekeStats.iHPMax_Base)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_MPMax,   100)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_MPRegen,  10)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_CPMax,    10)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_StunCap, 100)

--Free-Action Handlers
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_FreeActionMax, 3)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_FreeActionGen, 1)

--Combat Stats
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Initiative, gzaZekeStats.iIni_Base)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Attack,     gzaZekeStats.iAtk_Base)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Accuracy,   gzaZekeStats.iAcc_Base)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Evade,      gzaZekeStats.iEvd_Base)

--Base resistances.
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Protection,     3)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Slash,   gciNormalResist+2)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Strike,  gciNormalResist+2)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Pierce,  gciNormalResist+2)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Flame,   gciNormalResist-5)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Freeze,  gciNormalResist+3)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Shock,   gciNormalResist+3)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Crusade, gciNormalResist+8)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Obscure, gciNormalResist+8)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Bleed,   gciNormalResist-3)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Poison,  gciNormalResist-3)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Corrode, gciNormalResist+2)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Terrify, gciNormalResist-4)

--Threat Multiplier. Default 100 for all entities.
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_ThreatMultiplier, 100)

-- |[ ================================== Job Statistics Chart ================================== ]|
--All jobs store their statistics here, in a big ungainly chart!
if(gzaZekeJobChart == nil) then

    -- |[Creation Function]|
    --Adds elements to the reference chart.
    local gzaRefChart = {}
    local function fnAdd(psName, piHltConstant, pfHltPercent, piIniConstant, pfIniPercent, piAtkConstant, pfAtkPercent, piAccConstant, pfAccPercent, piEvdConstant, pfEvdPercent, 
                         piProtection, piSlash, piStrike, piPierce, piFlame, piFreeze, piShock, piCrusade, piObscure, piBleed, piPoison, piCorrode, piTerrify)
        
        --Setup.
        local i = #gzaRefChart + 1
        gzaRefChart[i] = {}
        
        --System.
        gzaRefChart[i].sName = psName
        
        --Stats.
        gzaRefChart[i].iHlt_Constant = piHltConstant
        gzaRefChart[i].fHlt_Percent  = pfHltPercent
        gzaRefChart[i].iIni_Constant = piIniConstant
        gzaRefChart[i].fIni_Percent  = pfIniPercent
        gzaRefChart[i].iAtk_Constant = piAtkConstant
        gzaRefChart[i].fAtk_Percent  = pfAtkPercent
        gzaRefChart[i].iAcc_Constant = piAccConstant
        gzaRefChart[i].fAcc_Percent  = pfAccPercent
        gzaRefChart[i].iEvd_Constant = piEvdConstant
        gzaRefChart[i].fEvd_Percent  = pfEvdPercent
        
        --Resistances.
        gzaRefChart[i].iBonus_Resist_Protection = piProtection
        gzaRefChart[i].iBonus_Resist_Slash = piSlash
        gzaRefChart[i].iBonus_Resist_Pierce = piPierce
        gzaRefChart[i].iBonus_Resist_Strike = piStrike
        gzaRefChart[i].iBonus_Resist_Flame = piFlame
        gzaRefChart[i].iBonus_Resist_Freeze = piFreeze
        gzaRefChart[i].iBonus_Resist_Shock = piShock
        gzaRefChart[i].iBonus_Resist_Crusade = piCrusade
        gzaRefChart[i].iBonus_Resist_Obscure = piObscure
        gzaRefChart[i].iBonus_Resist_Bleed = piBleed
        gzaRefChart[i].iBonus_Resist_Poison = piPoison
        gzaRefChart[i].iBonus_Resist_Corrode = piCorrode
        gzaRefChart[i].iBonus_Resist_Terrify = piTerrify
    end

    --     Actual Name ||||| HltC | HltP | IniC | IniP | AtkC | AtkP | AccC | AccP | EvdC | EvdP  ||||| Pr | Sl | St | Pc | Fl | Fr | Sh | Cr | Ob | Bl | Ps | Cr | Tr
    fnAdd("Goat",             9,  0.00,     0,  0.00,    10,  0.06,     5,  0.03,     3,  0.01,      0,   0,   0,   0,  -1,  -1,   1,   1,   1,  -5,   0,   0,   0)

    --Save.
    gzaZekeJobChart = gzaRefChart
end

-- |[ ===================================== Equipment Slots ==================================== ]|
--Collar. It's kinda like body armor!
AdvCombatEntity_SetProperty("Create Equipment Slot", "Collar")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Collar", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Collar", true)

--Accessory Slots
AdvCombatEntity_SetProperty("Create Equipment Slot", "Accessory A")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Accessory A", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Accessory A", true)
AdvCombatEntity_SetProperty("Create Equipment Slot", "Accessory B")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Accessory B", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Accessory B", true)

--Equippable Items
AdvCombatEntity_SetProperty("Create Equipment Slot", "Item A")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Item A", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Item A", true)
AdvCombatEntity_SetProperty("Create Equipment Slot", "Item B")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Item B", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Item B", true)
AdvCombatEntity_SetProperty("Create Equipment Slot", "Item C")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Item C", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Item C", true)
AdvCombatEntity_SetProperty("Create Equipment Slot", "Item D")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Item D", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Item D", true)

-- |[ ====================================== Job Handling ====================================== ]|
--Create Zeke's common abilities, shared by many classes.
local sJobAbilityPath = gsRoot .. "Combat/Party/Zeke/Jobs/Common/Abilities/"
LM_ExecuteScript(sJobAbilityPath .. "Attack.lua", gciAbility_Create)
LM_ExecuteScript(sJobAbilityPath .. "Block.lua",  gciAbility_Create)
LM_ExecuteScript(sJobAbilityPath .. "Locked.lua", gciAbility_Create)

--Job Change Abilities
--LM_ExecuteScript(sJobAbilityPath .. "Jobchange_Hunter.lua", gciAbility_Create)

--Call all of Zeke's job subscripts.
local sCharacterJobPath = gsRoot .. "Combat/Party/Zeke/Jobs/"
LM_ExecuteScript(sCharacterJobPath .. "Goat/000 Job Script.lua",  gciJob_Create)
AdvCombatEntity_SetProperty("Rebuild Job Skills UI")

--Set Zeke to his default job.
AdvCombatEntity_SetProperty("Active Job", "Goat")
VM_SetVar("Root/Variables/Global/Zeke/sCurrentJob", "S", "Goat")

--Once all abilities are built across all jobs, order jobs to associate with abilities. Any abilities
-- associated with a job can be purchased in the skills UI using that job's JP, and equipped from
-- that job's submenu. If an ability is not associated with any job, and is not auto-equipped by
-- any jobs when the job is assumed, the ability is not usable by the player!
LM_ExecuteScript(sCharacterJobPath .. "Goat/000 Job Script.lua", gciJob_JobAssembleSkillList)
    
-- |[ ==================================== Creation Callback =================================== ]|
--If this was a creation call of the character, fire its script callback.
if(bIsCreating) then
    local sResponsePath = AdvCombatEntity_GetProperty("Response Path")
    LM_ExecuteScript(sResponsePath, gciCombatEntity_Create)
end

-- |[ ======================================== Clean Up ======================================== ]|
--Pop the activity stack.
DL_PopActiveObject()
