-- |[ ========================================= Block ========================================== ]|
-- |[Description]|
--Exactly what you'd think. Increases protection.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzZekeCommonBlock == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Common"
    zAbiStruct.sSkillName    = "Block"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. "Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Buff
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Defend"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "Increases [Prt] by 10 for one turn.\n\n\n"
    zAbiStruct.sDescription = ""
    zAbiStruct.saImages = {}
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Protect yourself. Increases [Prt](Protection).\n\n\n\n"
    zAbiStruct.sSimpleDesc = ""
    zAbiStruct.saSimpleImages = {}

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false  --Set to true to ignore all requirements.
    zAbiStruct.iRequiredMP = 0           --MP required to use the ability.
    zAbiStruct.iRequiredCP = 0           --CP required to use the ability.
    zAbiStruct.bRespectsCooldown = true  --If this ability has a cooldown, decrements it.
    zAbiStruct.iCooldown = 0             --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
    zAbiStruct.bIsFreeAction = false     --Flags as a free action.
    zAbiStruct.iRequiredFreeActions = 0  --Free actions set this as 1, everything else as 0.
    zAbiStruct.bRespectActionCap = false --Characters with multiple free actions have a hard action cap. This flag checks that cap.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Self"

    --Effects
    zAbiStruct.iCPGain = 1
    
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Create an Effect module
    zAbiStruct.zPredictionPackage.zaEffectModules[1] = fnCreateEffectModule()
    zAbiStruct.zPredictionPackage.zaEffectModules[1].bIsBuff = true
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iEffectType = gciDamageType_Piercing
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iEffectStrength = 1000
    zAbiStruct.zPredictionPackage.zaEffectModules[1].sResultScript = "+10 [IMG1] / 1[IMG2]"
    zAbiStruct.zPredictionPackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/StatisticIcons/Protection", "Root/Images/AdventureUI/DebuffIcons/Clock"}
    
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Buff")
    zAbiStruct.zExecutionAbiPackage.bDoesNoDamage = true
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun = false
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits = true
    zAbiStruct.zExecutionAbiPackage.bEffectAlwaysApplies = true
    
    --Additional Animations.
    fnAddAnimationPackage(zAbiStruct.zExecutionAbiPackage, "Buff Defense", 0)
    
    -- |[Execution Effect Package]|
    --Basic package.
    zAbiStruct.zExecutionEffPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides.
    zAbiStruct.zExecutionEffPackage.sEffectPath     = fnResolvePath() .. "../Effects/Block.lua"
    zAbiStruct.zExecutionEffPackage.sEffectCritPath = fnResolvePath() .. "../Effects/Block.lua"
    zAbiStruct.zExecutionEffPackage.sApplySound     = "Combat\\|Impact_Buff"
    
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzZekeCommonBlock = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzZekeCommonBlock

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
