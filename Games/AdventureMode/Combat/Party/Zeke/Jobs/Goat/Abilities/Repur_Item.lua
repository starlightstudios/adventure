-- |[ =========================== Repurchaseable Item Power Upgrade ============================ ]|
-- |[Description]|
--Skill that can be purchased repeatedly to upgrade Zeke's item power.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzZekeRepurchaseableItem == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Goat"
    zAbiStruct.sSkillName    = "Item Boost"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gzaZekeStats.iJPCostBase
    zAbiStruct.sIconBacking  = gsAbility_Backing_Buff
    zAbiStruct.sIconFrame    = gsAbility_Frame_Passive
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Zeke|StatUp|ItemPower"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    local iRepurchaseItemPower = math.floor(VM_GetVar("Root/Variables/Global/Zeke/iRepurchaseItemPower", "N"))
    zAbiStruct.sDescriptionMarkdown = "+2%% Item Power per level.\n\n\n\nPassive.\nDoes not need to be equipped.\nCurrent Level: " .. iRepurchaseItemPower
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Permanently increases Zeke's Item Power\nby 2%% each time it is purchased.\nThe JP cost increases each time you buy any\nstat skill.\n\nCurrent Level: " .. iRepurchaseItemPower

    -- |[Usability Variables]|
    --Ability cannot be equipped or used.
    zAbiStruct.bCannotBeEquipped = true
    
    --Calls this script when purchased.
    zAbiStruct.sUnlockScript = LM_GetCallStack(0)
    
    -- |[ ======== Prediction Package ======== ]|
    --This ability never executes.
    
    -- |[ ========= Execution Package ======== ]|
    --This ability never executes.
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzZekeRepurchaseableItem = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzZekeRepurchaseableItem

-- |[ ================================= Unlocked in Skills UI ================================== ]|
--Whenever the player purchases this ability, this script gets called with this switch code. The ability
-- should be on the activity stack. This script handles changing Zeke's stats and resetting the skill.
if(iSwitchType == gciAbility_UIPurchased) then

    -- |[Debug]|
    --io.write("Running item power unlock handler!\n")

    -- |[Unset Unlocked]|
    --The ability continues to be available for purchase.
    AdvCombatAbility_SetProperty("Override Unlocked", false)

    -- |[Stat Modification]|
    --Add one to the total and associated variable.
    local iRepurchaseTotal     = VM_GetVar("Root/Variables/Global/Zeke/iRepurchaseTotal", "N")
    local iRepurchaseItemPower = VM_GetVar("Root/Variables/Global/Zeke/iRepurchaseItemPower", "N")
    VM_SetVar("Root/Variables/Global/Zeke/iRepurchaseTotal",     "N", iRepurchaseTotal     + 1)
    VM_SetVar("Root/Variables/Global/Zeke/iRepurchaseItemPower", "N", iRepurchaseItemPower + 1)
    
    -- |[Order Recompution]|
    LM_ExecuteScript(LM_GetCallStack(0), gciAbility_UIRecompute)
    
    -- |[Debug]|
    --io.write("Finished item power unlock handler.\n")

-- |[ ======================================== Recompute ======================================= ]|
--Recomputes the costs and effects of the ability given its current unlock count. Used whenever the 
-- player purchases the ability, or when the game loads.
elseif(iSwitchType == gciAbility_UIRecompute) then

    -- |[Stat Modification]|
    --Add one to the total and associated variable.
    local iRepurchaseTotal     = VM_GetVar("Root/Variables/Global/Zeke/iRepurchaseTotal", "N")
    local iRepurchaseItemPower = VM_GetVar("Root/Variables/Global/Zeke/iRepurchaseItemPower", "N")
    
    --Add to Zeke's tags.
    AdvCombat_SetProperty("Push Party Member", "Zeke")
        local iCurrentBaseTags = AdvCombatEntity_GetProperty("Base Tag Count", "Item Power +")
        local iExpectedTags    = gzaZekeStats.iItemTagsPerLevel * (iRepurchaseItemPower + 1)
        AdvCombatEntity_SetProperty("Add Tag", "Item Power +", (iExpectedTags - iCurrentBaseTags))
        
        --Debug.
        --io.write("After upgrade, item power tags: " .. AdvCombatEntity_GetProperty("Base Tag Count", "Item Power +") .. "\n")
        
    DL_PopActiveObject()
    
    -- |[Cost Recompute]|
    --Call this function to recompute the costs of all of Zeke's repurchaseable skills.
    fnRecomputeZekeCosts()
    
    -- |[Description Update]|
    --Modifies the description to show the current upgrade level.
    gzRefAbility.sDescriptionMarkdown = "+2%% Item Power per level. Passive.\nDoes not need to be equipped.\n\n\n\n\nCurrent Level: " .. math.floor(iRepurchaseItemPower)
    gzRefAbility.sSimpleDescMarkdown  = "Permanently increases Zeke's Item Power\nby 2%% each time it is purchased.\nThe JP cost increases each time you buy any\nstat skill." .. 
                                        "\n\nCurrent Level: " .. math.floor(iRepurchaseItemPower)
    
    --Re-run the markdown handler.
    fnMarkdownHandlerAbility(gzRefAbility)
    
    --Re-run the description uploader.
    AdvCombatAbility_SetProperty("Description", gzRefAbility.sDescription)
    AdvCombatAbility_SetProperty("Simplified Description", gzRefAbility.sSimpleDesc)

-- |[ ==================================== Standard Handler ==================================== ]|
--Call standard handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
