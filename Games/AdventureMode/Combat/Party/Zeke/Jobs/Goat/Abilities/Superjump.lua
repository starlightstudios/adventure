-- |[ ======================================= Superjump ======================================== ]|
-- |[Description]|
--Inflict 30% strike damage and 120% additional strike damage over 3 turns. Inflicts user with Slow.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzZekeGoatSuperjump == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Goat"
    zAbiStruct.sSkillName    = "Superjump"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_DoT
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Zeke|Goat|Superjump"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\n[DoT].\n\n\n[Costs]"
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Jump on the enemy's head and inflict a\n[Stk](Strike) DoT.\nDeals increased damage over 3 turns.\nInflicts you with 'Slow' for a turn.\n\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false  --Set to true to ignore all requirements.
    zAbiStruct.iRequiredMP = 20          --MP required to use the ability.
    zAbiStruct.iRequiredCP = 0           --CP required to use the ability.
    zAbiStruct.bRespectsCooldown = true  --If this ability has a cooldown, decrements it.
    zAbiStruct.iCooldown = 0             --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
    zAbiStruct.bIsFreeAction = false     --Flags as a free action.
    zAbiStruct.iRequiredFreeActions = 0  --Free actions set this as 1, everything else as 0.
    zAbiStruct.bRespectActionCap = false --Characters with multiple free actions have a hard action cap. This flag checks that cap.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies Single"

    --Effects
    zAbiStruct.iCPGain = 1
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Hit-Rate Module
    zAbiStruct.zPredictionPackage.bHasHitRateModule = true
    zAbiStruct.zPredictionPackage.zHitRateModule.iMissRate = 5
    
    --Crit Module
    zAbiStruct.zPredictionPackage.bHasCritModule = true
    zAbiStruct.zPredictionPackage.zCritModule.iCritThreshold = 100
    
    --Damage Module
    zAbiStruct.zPredictionPackage.bHasDamageModule = true
    zAbiStruct.zPredictionPackage.zDamageModule.iDamageType = gciDamageType_Striking
    zAbiStruct.zPredictionPackage.zDamageModule.fDamageFactor = 0.30
    
    --Create an Effect module
    zAbiStruct.zPredictionPackage.zaEffectModules[1] = fnCreateEffectModule()
    zAbiStruct.zPredictionPackage.zaEffectModules[1].bIsDamageOverTime = true
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iEffectType = gciDamageType_Striking
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iDotDuration = 3
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iDoTDamageType = gciDamageType_Striking
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iEffectStrength = 5
    zAbiStruct.zPredictionPackage.zaEffectModules[1].fDotAttackFactor = 0.50
    zAbiStruct.zPredictionPackage.zaEffectModules[1].sResultScript = "[DAM][IMG1] for 3[IMG2]"
    zAbiStruct.zPredictionPackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/DamageTypeIcons/Striking", "Root/Images/AdventureUI/DebuffIcons/Clock"}
    
    -- |[Self-Prediction]|
    --Inflicts slow on self.
    zAbiStruct.zSelfPredictionPack = fnCreatePredictionPack()
    zAbiStruct.zSelfPredictionPack.zaAdditionalLines[1] = {}
    zAbiStruct.zSelfPredictionPack.zaAdditionalLines[1].sString   = "'Slow' / 1 [IMG1]"
    zAbiStruct.zSelfPredictionPack.zaAdditionalLines[1].saSymbols = {"Root/Images/AdventureUI/DebuffIcons/Clock"}
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Strike")
    zAbiStruct.zExecutionAbiPackage.iDamageType    = gciDamageType_Striking
    zAbiStruct.zExecutionAbiPackage.iMissThreshold = 5
    zAbiStruct.zExecutionAbiPackage.fDamageFactor  = 0.30
    
    -- |[Execution Effect Package]|
    --Create an effect package and populate it.
    zAbiStruct.zExecutionEffPackage = fnConstructDefaultEffectPackage()
    zAbiStruct.zExecutionEffPackage.iEffectStr      = 5
    zAbiStruct.zExecutionEffPackage.iEffectCritStr  = 8
    zAbiStruct.zExecutionEffPackage.iEffectType     = gciDamageType_Striking
    zAbiStruct.zExecutionEffPackage.sEffectPath     = fnResolvePath() .. "../Effects/Superjump.lua"
    zAbiStruct.zExecutionEffPackage.sEffectCritPath = fnResolvePath() .. "../Effects/Superjump Crit.lua"
    zAbiStruct.zExecutionEffPackage.sApplyText      = "Shaking!"
    zAbiStruct.zExecutionEffPackage.sApplyTextCol   = "Color:Red"
    zAbiStruct.zExecutionEffPackage.sCritApplyText  = "Badly Shaking!"
    zAbiStruct.zExecutionEffPackage.sResistText     = "Resisted!"
    zAbiStruct.zExecutionEffPackage.sResistTextCol  = "Color:White"
    
    -- |[Execution Self-Effect Package]|
    --This applies the effect to the user of the ability.
    zAbiStruct.zExecutionSelfPackage = fnConstructDefaultEffectPackage()
    zAbiStruct.zExecutionSelfPackage.sEffectPath     = fnResolvePath() .. "../Effects/Superjump Self.lua"
    zAbiStruct.zExecutionSelfPackage.sEffectCritPath = fnResolvePath() .. "../Effects/Superjump Self.lua"
    zAbiStruct.zExecutionSelfPackage.sApplyText = "Slowed!"
    zAbiStruct.zExecutionSelfPackage.sApplyTextCol = "Color:White"
    zAbiStruct.zExecutionSelfPackage.sApplyAnimation = "Null"
    zAbiStruct.zExecutionSelfPackage.sApplySound = "Combat\\|Impact_Debuff"
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzZekeGoatSuperjump = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzZekeGoatSuperjump

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
