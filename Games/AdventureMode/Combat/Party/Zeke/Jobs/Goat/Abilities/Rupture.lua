-- |[ ========================================= Rupture ======================================== ]|
-- |[Description]|
--Bleeding attack, [Bleeding DoT] chaser.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzZekeGoatRupture == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Goat"
    zAbiStruct.sSkillName    = "Rupture"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Advanced
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Zeke|Goat|Rupture"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\n[Bld] Chaser, +25%% [Atk] if [Bld] is present.\n\n\n[Costs]"
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Hit the enemy for [Bld](Bleed) damage, and deals\nextra damage if they have a [Bld](Bleed) DoT.\nConsumes all [Bld]DoTs and deals their damage\nimmediately.\n\nCosts [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false  --Set to true to ignore all requirements.
    zAbiStruct.iRequiredMP = 25           --MP required to use the ability.
    zAbiStruct.iRequiredCP = 0           --CP required to use the ability.
    zAbiStruct.bRespectsCooldown = true  --If this ability has a cooldown, decrements it.
    zAbiStruct.iCooldown = 0             --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
    zAbiStruct.bIsFreeAction = false     --Flags as a free action.
    zAbiStruct.iRequiredFreeActions = 0  --Free actions set this as 1, everything else as 0.
    zAbiStruct.bRespectActionCap = false --Characters with multiple free actions have a hard action cap. This flag checks that cap.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies Single"

    --Effects
    zAbiStruct.iCPGain = 1
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Hit-Rate Module
    zAbiStruct.zPredictionPackage.bHasHitRateModule = true
    zAbiStruct.zPredictionPackage.zHitRateModule.iMissRate = 5
    
    --Damage Module
    zAbiStruct.zPredictionPackage.bHasDamageModule = true
    zAbiStruct.zPredictionPackage.zDamageModule.bUseWeaponType = false
    zAbiStruct.zPredictionPackage.zDamageModule.iDamageType = gciDamageType_Bleeding
    zAbiStruct.zPredictionPackage.zDamageModule.fDamageFactor = 0.75
    zAbiStruct.zPredictionPackage.zDamageModule.iScatterRange = 15
    
    --Crit Module
    zAbiStruct.zPredictionPackage.bHasCritModule = true
    
    --Chaser Modules
    zAbiStruct.zPredictionPackage.bHasChaserModules = true
    zAbiStruct.zPredictionPackage.iChaserModulesTotal = 1
    zAbiStruct.zPredictionPackage.zaChaserModules[1] = {}
    zAbiStruct.zPredictionPackage.zaChaserModules[1].bConsumeEffect = true                   --If true, DoT expires when attack lands
    zAbiStruct.zPredictionPackage.zaChaserModules[1].sDoTTag = "Bleeding DoT"                --What tag to look for on effects.
    zAbiStruct.zPredictionPackage.zaChaserModules[1].fDamageFactorChangePerDoT = 0.25        --Added to the damage module's factor for each effect present.
    zAbiStruct.zPredictionPackage.zaChaserModules[1].fDamageRemainingFactor = 1.0            --Percentage of remaining DoT damage to be added.
    zAbiStruct.zPredictionPackage.zaChaserModules[1].sConsumeString = "Consumes [IMG0] DoTs" --Description to appear IF and ONLY IF the chaser consumes DoTs
    zAbiStruct.zPredictionPackage.zaChaserModules[1].iConsumeImgCount = 1
    zAbiStruct.zPredictionPackage.zaChaserModules[1].saConsumeImgPath = {}
    zAbiStruct.zPredictionPackage.zaChaserModules[1].saConsumeImgPath[1] = "Root/Images/AdventureUI/DamageTypeIcons/Bleeding"
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    --Basic package.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Bleed")
    zAbiStruct.zExecutionAbiPackage.iMissThreshold = 5
    zAbiStruct.zExecutionAbiPackage.fDamageFactor  = 0.75
    
    --Chaser Modules
    zAbiStruct.zExecutionAbiPackage.bHasChaserModules = true
    zAbiStruct.zExecutionAbiPackage.iChaserModulesTotal = 1
    zAbiStruct.zExecutionAbiPackage.zaChaserModules[1] = {}
    zAbiStruct.zExecutionAbiPackage.zaChaserModules[1].bConsumeEffect = true           --If true, DoT expires when attack lands
    zAbiStruct.zExecutionAbiPackage.zaChaserModules[1].sDoTTag = "Bleeding DoT"        --What tag to look for on effects.
    zAbiStruct.zExecutionAbiPackage.zaChaserModules[1].fDamageFactorChangePerDoT = 0.25--Added to the damage module's factor for each effect present.
    zAbiStruct.zExecutionAbiPackage.zaChaserModules[1].fDamageRemainingFactor = 1.0    --Percentage of remaining DoT damage to be added.
    
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzZekeGoatRupture = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzZekeGoatRupture

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
