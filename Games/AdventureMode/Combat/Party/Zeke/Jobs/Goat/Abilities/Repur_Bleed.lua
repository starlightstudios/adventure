-- |[ =========================== Repurchaseable Bleed Power Upgrade =========================== ]|
-- |[Description]|
--Skill that can be purchased repeatedly to upgrade Zeke's bleed damage.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzZekeRepurchaseableBleed == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Goat"
    zAbiStruct.sSkillName    = "Bleed Boost"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gzaZekeStats.iJPCostBase
    zAbiStruct.sIconBacking  = gsAbility_Backing_Buff
    zAbiStruct.sIconFrame    = gsAbility_Frame_Passive
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Zeke|StatUp|BleedDam"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    local iRepurchaseBleedDamage = math.floor(VM_GetVar("Root/Variables/Global/Zeke/iRepurchaseBleedDamage", "N"))
    zAbiStruct.sDescriptionMarkdown = "+2%% Bleed Damage per level.\n\n\n\nPassive.\nDoes not need to be equipped.\nCurrent Level: " .. iRepurchaseBleedDamage
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Permanently increases Zeke's [Bld](Bleed) damage\nby 2%% each time it is purchased.\nThe JP cost increases each time you buy any\nstat skill.\n\nCurrent Level: " .. iRepurchaseBleedDamage

    -- |[Usability Variables]|
    --Ability cannot be equipped or used.
    zAbiStruct.bCannotBeEquipped = true
    
    --Calls this script when purchased.
    zAbiStruct.sUnlockScript = LM_GetCallStack(0)
    
    -- |[ ======== Prediction Package ======== ]|
    --This ability never executes.
    
    -- |[ ========= Execution Package ======== ]|
    --This ability never executes.
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzZekeRepurchaseableBleed = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzZekeRepurchaseableBleed

-- |[ ================================= Unlocked in Skills UI ================================== ]|
--Whenever the player purchases this ability, this script gets called with this switch code. The ability
-- should be on the activity stack. This script handles changing Zeke's stats and resetting the skill.
if(iSwitchType == gciAbility_UIPurchased) then

    -- |[Debug]|
    --io.write("Running bleed power unlock handler!\n")

    -- |[Unset Unlocked]|
    --The ability continues to be available for purchase.
    AdvCombatAbility_SetProperty("Override Unlocked", false)

    -- |[Stat Modification]|
    --Add one to the total and associated variable.
    local iRepurchaseTotal       = VM_GetVar("Root/Variables/Global/Zeke/iRepurchaseTotal", "N")
    local iRepurchaseBleedDamage = VM_GetVar("Root/Variables/Global/Zeke/iRepurchaseBleedDamage", "N")
    VM_SetVar("Root/Variables/Global/Zeke/iRepurchaseTotal",       "N", iRepurchaseTotal       + 1)
    VM_SetVar("Root/Variables/Global/Zeke/iRepurchaseBleedDamage", "N", iRepurchaseBleedDamage + 1)
    
    -- |[Order Recompution]|
    LM_ExecuteScript(LM_GetCallStack(0), gciAbility_UIRecompute)
    
    -- |[Debug]|
    --io.write("Finished bleed power unlock handler.\n")

-- |[ ======================================== Recompute ======================================= ]|
--Recomputes the costs and effects of the ability given its current unlock count. Used whenever the 
-- player purchases the ability, or when the game loads.
elseif(iSwitchType == gciAbility_UIRecompute) then

    -- |[Stat Modification]|
    --Add one to the total and associated variable.
    local iRepurchaseTotal       = VM_GetVar("Root/Variables/Global/Zeke/iRepurchaseTotal", "N")
    local iRepurchaseBleedDamage = VM_GetVar("Root/Variables/Global/Zeke/iRepurchaseBleedDamage", "N")
    
    --Add to Zeke's tags.
    AdvCombat_SetProperty("Push Party Member", "Zeke")
        local iCurrentBaseTags = AdvCombatEntity_GetProperty("Base Tag Count", "Bleed Damage Dealt +")
        local iExpectedTags    = gzaZekeStats.iBleedTagsPerLevel * (iRepurchaseBleedDamage + 1)
        AdvCombatEntity_SetProperty("Add Tag", "Bleed Damage Dealt +", (iExpectedTags - iCurrentBaseTags))
    DL_PopActiveObject()
    
    -- |[Cost Recompute]|
    --Call this function to recompute the costs of all of Zeke's repurchaseable skills.
    fnRecomputeZekeCosts()
    
    -- |[Description Update]|
    --Modifies the description to show the current upgrade level.
    gzRefAbility.sDescriptionMarkdown = "+2%% Bleed Damage per level. Passive.\nDoes not need to be equipped.\n\n\n\n\nCurrent Level: " .. math.floor(iRepurchaseBleedDamage)
    gzRefAbility.sSimpleDescMarkdown  = "Permanently increases Zeke's [Bld](Bleed) damage\nby 2%% each time it is purchased.\nThe JP cost increases each time you buy any\nstat skill." .. 
                                        "\n\nCurrent Level: " .. math.floor(iRepurchaseBleedDamage+1)
    
    --Re-run the markdown handler.
    fnMarkdownHandlerAbility(gzRefAbility)
    
    --Re-run the description uploader.
    AdvCombatAbility_SetProperty("Description", gzRefAbility.sDescription)
    AdvCombatAbility_SetProperty("Allocate Description Images", #gzRefAbility.saImages)
    for i = 1, #gzRefAbility.saImages, 1 do
        AdvCombatAbility_SetProperty("Description Image", i-1, gcfAbilityImgOffsetY, gzRefAbility.saImages[i])
    end
    AdvCombatAbility_SetProperty("Crossload Description Images")
    
    --Simplified Description
    if(gzRefAbility.sSimpleDesc ~= nil) then
        AdvCombatAbility_SetProperty("Simplified Description", gzRefAbility.sSimpleDesc)
        AdvCombatAbility_SetProperty("Allocate Simplified Description Images", #gzRefAbility.saSimpleImages)
        for i = 1, #gzRefAbility.saSimpleImages, 1 do
            AdvCombatAbility_SetProperty("Simplified Description Image", i-1, gcfAbilityImgOffsetLgY, gzRefAbility.saSimpleImages[i])
        end
        AdvCombatAbility_SetProperty("Crossload Simplified Description Images")
    end

-- |[ ==================================== Standard Handler ==================================== ]|
--Call standard handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
