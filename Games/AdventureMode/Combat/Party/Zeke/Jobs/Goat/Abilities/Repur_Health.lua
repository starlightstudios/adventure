-- |[ ============================= Repurchaseable Health Upgrade ============================== ]|
-- |[Description]|
--Skill that can be purchased repeatedly to upgrade Zeke's HP.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzZekeRepurchaseableHealth == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Goat"
    zAbiStruct.sSkillName    = "Health Boost"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gzaZekeStats.iJPCostBase
    zAbiStruct.sIconBacking  = gsAbility_Backing_Buff
    zAbiStruct.sIconFrame    = gsAbility_Frame_Passive
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Zeke|StatUp|Health"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    local iRepurchaseHealth = math.floor(VM_GetVar("Root/Variables/Global/Zeke/iRepurchaseHealth", "N"))
    zAbiStruct.sDescriptionMarkdown = "+6[Hlt] per level.\n\n\n\nPassive.\nDoes not need to be equipped.\nCurrent Level: " .. iRepurchaseHealth
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Permanently increases Zeke's [Hlt](Health) by 6\neach time it is purchased.\nThe JP cost increases each time you buy any\nstat skill.\n\nCurrent Level: " .. iRepurchaseHealth

    -- |[Usability Variables]|
    --Ability cannot be equipped or used.
    zAbiStruct.bCannotBeEquipped = true
    
    --Calls this script when purchased.
    zAbiStruct.sUnlockScript = LM_GetCallStack(0)
    
    -- |[ ======== Prediction Package ======== ]|
    --This ability never executes.
    
    -- |[ ========= Execution Package ======== ]|
    --This ability never executes.
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzZekeRepurchaseableHealth = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzZekeRepurchaseableHealth

-- |[ ================================= Unlocked in Skills UI ================================== ]|
--Whenever the player purchases this ability, this script gets called with this switch code. The ability
-- should be on the activity stack. This script handles changing Zeke's stats and resetting the skill.
if(iSwitchType == gciAbility_UIPurchased) then

    -- |[Debug]|
    --io.write("Running health unlock handler!\n")

    -- |[Unset Unlocked]|
    --The ability continues to be available for purchase.
    AdvCombatAbility_SetProperty("Override Unlocked", false)

    -- |[Stat Modification]|
    --Add one to the total and associated variable.
    local iRepurchaseTotal  = VM_GetVar("Root/Variables/Global/Zeke/iRepurchaseTotal",  "N")
    local iRepurchaseHealth = VM_GetVar("Root/Variables/Global/Zeke/iRepurchaseHealth", "N")
    VM_SetVar("Root/Variables/Global/Zeke/iRepurchaseTotal",  "N", iRepurchaseTotal  + 1)
    VM_SetVar("Root/Variables/Global/Zeke/iRepurchaseHealth", "N", iRepurchaseHealth + 1)
    
    -- |[Order Recompution]|
    LM_ExecuteScript(LM_GetCallStack(0), gciAbility_UIRecompute)
    
    -- |[Debug]|
    --io.write("Finished health unlock handler.\n")

-- |[ ======================================== Recompute ======================================= ]|
--Recomputes the costs and effects of the ability given its current unlock count. Used whenever the 
-- player purchases the ability, or when the game loads.
elseif(iSwitchType == gciAbility_UIRecompute) then

    -- |[Stat Modification]|
    --Add one to the total and associated variable.
    local iRepurchaseTotal  = VM_GetVar("Root/Variables/Global/Zeke/iRepurchaseTotal",  "N")
    local iRepurchaseHealth = VM_GetVar("Root/Variables/Global/Zeke/iRepurchaseHealth", "N")
    
    --Add to Zeke's base HP.
    AdvCombat_SetProperty("Push Party Member", "Zeke")
    
        --Track Zeke's current health percent and keep it updated when his HP goes up.
        local iCurHp = AdvCombatEntity_GetProperty("Health")
        local iMaxHp = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
        local fHpPct = iCurHp / iMaxHp
    
        --Compute HP.
        local iBaseHP = gzaZekeStats.iHPMax_Base
        local iBonus  = gzaZekeStats.iHPMax_Grow * iRepurchaseHealth
        local iTotalBaseHP = iBaseHP + iBonus
        
        --Set.
        AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_HPMax, iTotalBaseHP)
        
        --Update UI.
        AdvCombatEntity_SetProperty("Recompute Stats")
        AdvCombatEntity_SetProperty("Refresh Stats for UI")
        
        --Set the percentage.
        AdvCombatEntity_SetProperty("Health Percent", fHpPct)
    DL_PopActiveObject()
    
    -- |[Cost Recompute]|
    --Call this function to recompute the costs of all of Zeke's repurchaseable skills.
    fnRecomputeZekeCosts()
    
    -- |[Description Update]|
    --Modifies the description to show the current upgrade level.
    gzRefAbility.sDescriptionMarkdown = "+5[Hlt] per level. Passive.\nDoes not need to be equipped.\n\n\n\n\nCurrent Level: " .. math.floor(iRepurchaseHealth)
    gzRefAbility.sSimpleDescMarkdown = "Permanently increases Zeke's [Hlt](Health) by 5\neach time it is purchased.\nThe JP cost increases each time you buy any\nstat skill.\n\nCurrent Level: " .. math.floor(iRepurchaseHealth)
    
    --Re-run the markdown handler.
    fnMarkdownHandlerAbility(gzRefAbility)
    
    --Re-run the description uploader.
    AdvCombatAbility_SetProperty("Description", gzRefAbility.sDescription)
    AdvCombatAbility_SetProperty("Allocate Description Images", #gzRefAbility.saImages)
    for i = 1, #gzRefAbility.saImages, 1 do
        AdvCombatAbility_SetProperty("Description Image", i-1, gcfAbilityImgOffsetY, gzRefAbility.saImages[i])
    end
    AdvCombatAbility_SetProperty("Crossload Description Images")
    
    --Simplified Description
    if(gzRefAbility.sSimpleDesc ~= nil) then
        AdvCombatAbility_SetProperty("Simplified Description", gzRefAbility.sSimpleDesc)
        AdvCombatAbility_SetProperty("Allocate Simplified Description Images", #gzRefAbility.saSimpleImages)
        for i = 1, #gzRefAbility.saSimpleImages, 1 do
            AdvCombatAbility_SetProperty("Simplified Description Image", i-1, gcfAbilityImgOffsetLgY, gzRefAbility.saSimpleImages[i])
        end
        AdvCombatAbility_SetProperty("Crossload Simplified Description Images")
    end

-- |[ ==================================== Standard Handler ==================================== ]|
--Call standard handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
