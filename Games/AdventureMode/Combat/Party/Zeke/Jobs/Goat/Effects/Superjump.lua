-- |[ ======================================= Superjump ======================================== ]|
-- |[Description]|
--Deals striking damage equal to 1.20 of user's attack power over 3 turns.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--Setup, apply overrides.
if(gzEffectGoatSuperjump == nil) then
    
    --Base, overrides.
    local zEffectStruct = fnCreateDoTEffectPrototype(gciDamageType_Striking, 3, 1.50)
    zEffectStruct.sDisplayName = "Superjump"
    zEffectStruct.sIcon = "Mei|Rend"
    
    --Tags
    zEffectStruct.zaTagList = {{"Striking DoT", 1}, {"Is Negative", 1}}
    
    --Store
    gzEffectGoatSuperjump = zEffectStruct
    
end

-- |[Set and Execute]|
--Standardized Naming
gzRefEffect = gzEffectGoatSuperjump

--Call the standard handler. It will nil off the gzRefEffect.
LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType)
