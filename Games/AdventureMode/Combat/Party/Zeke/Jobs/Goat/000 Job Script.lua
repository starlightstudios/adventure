-- |[ ========================================== Goat ========================================== ]|
--Zeke's basic class for chapter 2. And only class! Goats aren't smart enough to change class!

-- |[Notes]|
--For gciJob_Create, the AdvCombatEntity that will own the job is the active object. For all other
-- calls, the AdvCombatJob is active and can push the owning entity.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ====================================== Job Creation ====================================== ]|
--Called when the chapter is initialized, the job registers itself to its owner and populates its
-- abilities. This is only called once.
--The owner should be the active object.
if(iSwitchType == gciJob_Create) then
    
    --Create the job and set its stats.
    AdvCombatEntity_SetProperty("Create Job", "Goat")
    
        --Name as it appears in the UI.
        AdvCombatJob_SetProperty("Display Name", "Goat")
        AdvCombatJob_SetProperty("Appears on Skills UI", false)
    
        --Script path to this job
        AdvCombatJob_SetProperty("Script Path", LM_GetCallStack(0))
        AdvCombatJob_SetProperty("Is Always Mastered", true)
        
        --Responses.
        AdvCombatJob_SetProperty("Script Response", gciJob_SwitchTo, true)
        AdvCombatJob_SetProperty("Script Response", gciJob_Level, true)
        AdvCombatJob_SetProperty("Script Response", gciJob_Master, true)
        AdvCombatJob_SetProperty("Script Response", gciJob_JobAssembleSkillList, true)
        AdvCombatJob_SetProperty("Script Response", gciJob_BeginCombat, true)
        AdvCombatJob_SetProperty("Script Response", gciJob_EndAction, true)
        
        --Display
        local fIndexX = 5
        local fIndexY = 6
        local fLft = (fIndexX+0) * gci_FaceTable_Size
        local fTop = (fIndexY+0) * gci_FaceTable_Size
        local fRgt = (fIndexX+1) * gci_FaceTable_Size
        local fBot = (fIndexY+1) * gci_FaceTable_Size
        AdvCombatJob_SetProperty("Face Table Data", fLft, fTop, fRgt, fBot, "Root/Images/AdventureUI/ItemIcons/CharacterFaces")

    DL_PopActiveObject()
    
    --All of Zeke's abilities are pre-set and he has no skillbooks. JP is instead spent on repurchaseable skills
    -- that give him stat upgrades.
    local sJobAbilityPath = fnResolvePath() .. "Abilities/"
    LM_ExecuteScript(sJobAbilityPath .. "Headbutt.lua",   gciAbility_CreateSpecial)
    LM_ExecuteScript(sJobAbilityPath .. "Horn Slash.lua", gciAbility_CreateSpecial)
    LM_ExecuteScript(sJobAbilityPath .. "Nuzzle.lua",     gciAbility_CreateSpecial)
    LM_ExecuteScript(sJobAbilityPath .. "Rupture.lua",    gciAbility_CreateSpecial)
    LM_ExecuteScript(sJobAbilityPath .. "Spit.lua",       gciAbility_CreateSpecial)
    LM_ExecuteScript(sJobAbilityPath .. "Goat Quake.lua", gciAbility_CreateSpecial)
    LM_ExecuteScript(sJobAbilityPath .. "Superjump.lua",  gciAbility_CreateSpecial)
    
    --Repurchaseable skills.
    LM_ExecuteScript(sJobAbilityPath .. "Repur_Health.lua",     gciAbility_CreateSpecial)
    LM_ExecuteScript(sJobAbilityPath .. "Repur_Power.lua",      gciAbility_CreateSpecial)
    LM_ExecuteScript(sJobAbilityPath .. "Repur_Accuracy.lua",   gciAbility_CreateSpecial)
    LM_ExecuteScript(sJobAbilityPath .. "Repur_Evade.lua",      gciAbility_CreateSpecial)
    LM_ExecuteScript(sJobAbilityPath .. "Repur_Initiative.lua", gciAbility_CreateSpecial)
    LM_ExecuteScript(sJobAbilityPath .. "Repur_Item.lua",       gciAbility_CreateSpecial)
    LM_ExecuteScript(sJobAbilityPath .. "Repur_Bleed.lua",      gciAbility_CreateSpecial)
    LM_ExecuteScript(sJobAbilityPath .. "Repur_Strike.lua",     gciAbility_CreateSpecial)
   
-- |[ ===================================== Job Assumption ===================================== ]|
--Called when the player switches to the class. Clears any existing job properties, sets graphics
-- and stats as needed.
--This can be called during combat, as some characters can switch jobs in-battle.
--The calling AdvCombatEntity is expected to be the active object.
elseif(iSwitchType == gciJob_SwitchTo) then

    --Change images.
    VM_SetVar("Root/Variables/Global/Zeke/sCurrentJob", "S", "Goat")
    LM_ExecuteScript(gsCostumeAutoresolve, "Zeke_Goat")
    
    --Unlock on UI.
    AdvCombatJob_SetProperty("Appears on Skills UI", true)

    --Push the owning entity.
    AdvCombatJob_SetProperty("Push Owner")

        -- |[Display Name]|
        --Use normal display name.
        AdvCombatEntity_SetProperty("Display Name", "DEFAULT")

        -- |[Rebuild Visibility List]|
        AdvCombatEntity_SetProperty("Rebuild Job Skills UI")

        -- |[Statistics]|
        --Get health percentage.
        local fHPPercent = AdvCombatEntity_GetProperty("Health") / AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
        
        --Set job statistics.
        AdvCombatEntity_SetProperty("Compute Level Statistics", -1)
        
        --Apply change in max HP.
        AdvCombatEntity_SetProperty("Health Percent", fHPPercent)
        
        --Clear all relevant ability slots.
        fnClearJobSlots()
        
        -- |[Top Bar]|
        --Set basic actions bar.
        AdvCombatEntity_SetProperty("Set Ability Slot", 0, 0, "Common|Attack")
        AdvCombatEntity_SetProperty("Set Ability Slot", 1, 0, "Common|Block")
    
        --4-CP ability.
        AdvCombatEntity_SetProperty("Set Ability Slot", 2, 0, "Goat Internal|Goat Quake")
        
        --Items, Retreat, Surrender, Etc.
        fnPlaceJobCommonSkillsNoItems()
        
        -- |[Class Bar]|
        --Set class-specific bar.
        local iSkillbookTotal = VM_GetVar("Root/Variables/Global/Sanya/iSkillbookTotal", "N") 
        AdvCombatEntity_SetProperty("Set Ability Slot", 0, 1, "Goat Internal|Horn Slash")
        AdvCombatEntity_SetProperty("Set Ability Slot", 1, 1, "Goat Internal|Rupture")
        AdvCombatEntity_SetProperty("Set Ability Slot", 2, 1, "Goat Internal|Headbutt")
        AdvCombatEntity_SetProperty("Set Ability Slot", 0, 2, "Goat Internal|Spit")
        AdvCombatEntity_SetProperty("Set Ability Slot", 1, 2, "Goat Internal|Superjump")
        AdvCombatEntity_SetProperty("Set Ability Slot", 2, 2, "Goat Internal|Nuzzle")
        
        --Note: Zeke does not use skillbooks. All his abilities are unlocked by default.
        
        -- |[Job Changes]|
        LM_ExecuteScript(gsRoot .. "Combat/Party/Zeke/200 Build Secondary Menu.lua")
        
    DL_PopActiveObject()

-- |[ ====================================== Job Level Up ====================================== ]|
--Called when the job reaches a level. The second argument is the level in question. Remember that
-- level 1 on the UI is level 0 in the scripts.
--When assuming the job, this is called by the program to reset stats. There is no need to call it
-- during the Job Assumption part of this script.
elseif(iSwitchType == gciJob_Level) then

    -- |[Setup]|
    --Argument check.
    if(iArgumentsTotal < 2) then 
        Debug_ForcePrint("Error: No level specified for Job Level Up. " .. LM_GetCallStack(0) .. "\n") 
        return
    end

    --First argument is what level we reached.
    local iLevelReached = LM_GetScriptArgument(1, "N")
    
    --Common script.
    fnChartJobStatHandler(gzaZekeJobChart, "Goat", iLevelReached, gsRoot .. "Combat/Party/Zeke/400 Common Stat Profile.lua")
    
-- |[ ====================================== Job Mastered ====================================== ]|
--Called when the player masters the class and gets the mastery bonus applied. Also called when the
-- character changes class and has the mastery bonus applied to that class.
elseif(iSwitchType == gciJob_Master) then
    
-- |[ =================================== Assemble Skill List ================================== ]|
--Once all abilities are registered to the parent entity, builds a list of which skills are associated
-- with this job. These skills show up on the UI and can be purchased by the player. Note that a job
-- can contain the same ability as another job. Purchasing it in either will unlock both versions.
-- This is not explicitly a problem, it means the same ability can be bought with JP from multiple
-- jobs. It is not part of the standard for Adventure Mode though.
elseif(iSwitchType == gciJob_JobAssembleSkillList) then

    --Setup.
    local sJobName = "Goat"
    
    --Register Zeke's repurchaseable abilities.
    AdvCombatEntity_SetProperty("Register Ability To Job", "Goat Internal|Health Boost",     sJobName)
    AdvCombatEntity_SetProperty("Register Ability To Job", "Goat Internal|Power Boost",      sJobName)
    AdvCombatEntity_SetProperty("Register Ability To Job", "Goat Internal|Accuracy Boost",   sJobName)
    AdvCombatEntity_SetProperty("Register Ability To Job", "Goat Internal|Evade Boost",      sJobName)
    AdvCombatEntity_SetProperty("Register Ability To Job", "Goat Internal|Initiative Boost", sJobName)
    AdvCombatEntity_SetProperty("Register Ability To Job", "Goat Internal|Item Boost",       sJobName)
    AdvCombatEntity_SetProperty("Register Ability To Job", "Goat Internal|Bleed Boost",      sJobName)
    AdvCombatEntity_SetProperty("Register Ability To Job", "Goat Internal|Strike Boost",     sJobName)
    
-- |[ ====================================== Combat Start ====================================== ]|
--Called when combat begins. The job should populate passive abilities here. The AdvCombatEntity
-- will be atop the activity stack.
--Special: Zeke has 4 item slots, not 2. Therefore we have special item placement code. He also
-- can't switch weapons.
elseif(iSwitchType == gciJob_BeginCombat) then
    AdvCombatJob_SetProperty("Push Owner")
        fnPlaceJobCommonSkills()
        fnPlaceJobCommonSkillsCombatStart()
    DL_PopActiveObject()
    
-- |[ ==================================== Combat: New Turn ==================================== ]|
--Called when turn order is sorted and a new turn begins.
elseif(iSwitchType == gciJob_BeginTurn) then

-- |[ =================================== Combat: New Action =================================== ]|
--Called when an action begins. Note that the action is not necessarily that of the active entity.
elseif(iSwitchType == gciJob_BeginAction) then
    
-- |[ ========================= Combat: New Action (After Free Action) ========================= ]|
--Called when an action begins after a Free Action has expired.
elseif(iSwitchType == gciJob_BeginFreeAction) then

-- |[ =================================== Combat: End Action =================================== ]|
--Called when an action ends. As above, not necessarily that of the active entity.
elseif(iSwitchType == gciJob_EndAction) then

    -- |[Debug]|
    --io.write("Calling Zeke's End of Action job script.\n")
    
    -- |[Check Acting]|
    --We need to find out if Zeke just ended his action.
    local iActingEntityID = AdvCombat_GetProperty("ID of Acting Entity")
    
    --Get Zeke's ID.
    AdvCombat_SetProperty("Push Party Member", "Zeke")
        local iZekesID = RO_GetID()
    DL_PopActiveObject()
    
    --Debug.
    --io.write("Zeke's ID: " .. iZekesID .. " versus acting ID: " .. iActingEntityID .. "\n")
    
    --If the owner is not acting, fail.
    if(iActingEntityID ~= iZekesID) then return end
    
    -- |[Scan]|
    --Setup.
    local bHitAnyone = false
    --io.write("Zeke acted.\n")
    
    --Scan backwards through the abilities used to find the one used by our owner. It should be the
    -- last one, but a counterattack might take that slot.
    local iCurrentTurn = gzaCombatAbilityStatistics.iTurnsElapsed
    for i = #gzaCombatAbilityStatistics.zaStatisticsPacks, 1, -1 do

        --Get the package.
        local zStatsPack = gzaCombatAbilityStatistics.zaStatisticsPacks[i]
        
        --Error checks:
        if(zStatsPack == nil) then
            --io.write("  Error, out of range.\n")
        elseif(zStatsPack.iTurnElapsed ~= iCurrentTurn) then
            --io.write("  Error, incorrect turn " .. zStatsPack.iTurnElapsed .. " vs. " .. iCurrentTurn .. "\n")
        elseif(zStatsPack.iActorID ~= iZekesID) then
            --io.write("  Error, not action by owner " .. zStatsPack.iActorID .. " vs. " .. iZekesID .. "\n")
        
        --Checks passed:
        else
            
            --Scan all targets hit. If any attacks hit, stop here.
            for p = 1, #zStatsPack.zaTargetInfo, 1 do
                if(zStatsPack.zaTargetInfo[p].bWasMiss == false) then
                    --io.write("Got a hit, exiting.\n")
                    return
                end
            end
        end
    end
    
    -- |[No Hits]|
    --If not a single target was hit, provide the Stubborn buff. If we got this far, all the targets
    -- were missed, otherwise the routine would have exited.
    --io.write("No hits.\n")
    
    --Apply the buff immediately.
    local zExecutionEffPackage = fnConstructDefaultEffectPackage()
    zExecutionEffPackage.sEffectPath     = fnResolvePath() .. "Effects/Stubborn.lua" 
    zExecutionEffPackage.sEffectCritPath = fnResolvePath() .. "Effects/Stubborn.lua" 
    zExecutionEffPackage.sApplyText      = "Stubborn!"
    zExecutionEffPackage.sApplyTextCol   = "Color:Purple"
    zExecutionEffPackage.sResistText     = "Failed!"
    zExecutionEffPackage.sResistTextCol  = "Color:White"
    zExecutionEffPackage.fnHandler(iZekesID, iZekesID, 0, zExecutionEffPackage, true, false)
    
    
-- |[ ==================================== Combat: End Turn ==================================== ]|
--Called when a turn ends, before turn order is determined for the next turn.
elseif(iSwitchType == gciJob_EndTurn) then

-- |[ =================================== Combat: End Combat =================================== ]|
--Called when combat ends for any reason. This includes, victory, defeat, retreat, or other.
elseif(iSwitchType == gciJob_EndCombat) then

-- |[ ================================== Combat: Event Queued ================================== ]|
--Called when a main event is enqueued. An entity typically performs one event per action. This is
-- called before after Entity but before Abilities respond for this character.
elseif(iSwitchType == gciJob_EventQueued) then

end
