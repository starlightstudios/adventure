-- |[ ======================================= Smarty Sage ====================================== ]|
--The Slime class.

-- |[ ========== Argument Handling =========== ]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--1st argument is only used for level-up calls.
local iLevelReached = 0
if(iArgumentsTotal >= 2) then
    iLevelReached = LM_GetScriptArgument(1, "N")
end

-- |[ ============ Variable Setup ============ ]|
-- |[Enumerations]|
local gciNoSkillbooks = 0

-- |[Encapsulation]|
zEntry = {}

-- |[Basic Variables]|
--Character Variables.
zEntry.sCharacterFormVarPath = "Root/Variables/Global/Mei/sForm"
zEntry.sSkillbookVarPath     = "Root/Variables/Global/Mei/iSkillbookTotal"
zEntry.zaJobChart            = gzaMeiJobChart
zEntry.sStatProfilePath      = gsRoot .. "Combat/Party/Mei/400 Common Stat Profile.lua"
zEntry.sSecondaryMenuPath    = gsRoot .. "Combat/Party/Mei/200 Build Secondary Menu.lua"
zEntry.sMasterBonusPath      = gsRoot .. "Combat/Party/Mei/401 Mastery Bonus Handler.lua"

--Class Variables.
zEntry.sClassName   = "Smarty Sage"
zEntry.sFormName    = "Slime"
zEntry.sCostumeName = "Mei_Slime"
zEntry.fFaceIndexX  = 4
zEntry.fFaceIndexY  = 0

-- |[Ability Variables]|
--Purchaseable abilities.
zEntry.saExternalAbilities = {}
table.insert(zEntry.saExternalAbilities, "Absorb")
table.insert(zEntry.saExternalAbilities, "Big Dumb Grin")
table.insert(zEntry.saExternalAbilities, "Firm Up")
table.insert(zEntry.saExternalAbilities, "Goopshot")
table.insert(zEntry.saExternalAbilities, "Jiggly Chest")
table.insert(zEntry.saExternalAbilities, "Splash")
table.insert(zEntry.saExternalAbilities, "Entangle")
table.insert(zEntry.saExternalAbilities, "Reserves")
table.insert(zEntry.saExternalAbilities, "Slimy Strike")
table.insert(zEntry.saExternalAbilities, "Way Of The Slime")

--Class-equipped abilities.
zEntry.saInternalAbilities = {}
table.insert(zEntry.saInternalAbilities, "Splash")
table.insert(zEntry.saInternalAbilities, "Goopshot")
table.insert(zEntry.saInternalAbilities, "Absorb")
table.insert(zEntry.saInternalAbilities, "Firm Up")
table.insert(zEntry.saInternalAbilities, "Jiggly Chest")
table.insert(zEntry.saInternalAbilities, "Big Dumb Grin")
table.insert(zEntry.saInternalAbilities, "Slimy Strike")

--Purchaseable abilities, in the order of purchase.
zEntry.saPurchaseAbilities = {}
table.insert(zEntry.saPurchaseAbilities, "Smarty Sage|Splash")
table.insert(zEntry.saPurchaseAbilities, "Smarty Sage|Goopshot")
table.insert(zEntry.saPurchaseAbilities, "Smarty Sage|Absorb")
table.insert(zEntry.saPurchaseAbilities, "Smarty Sage|Firm Up")
table.insert(zEntry.saPurchaseAbilities, "Smarty Sage|Jiggly Chest")
table.insert(zEntry.saPurchaseAbilities, "Smarty Sage|Entangle")
table.insert(zEntry.saPurchaseAbilities, "Smarty Sage|Reserves")
table.insert(zEntry.saPurchaseAbilities, "Smarty Sage|Slimy Strike")
table.insert(zEntry.saPurchaseAbilities, "Smarty Sage|Big Dumb Grin")
table.insert(zEntry.saPurchaseAbilities, "Smarty Sage|Way Of The Slime")


-- |[Skillbar Variables]|
--Skills that always appear on the skillbar. Format: {sAbilityName, iXPos, iYPos, iSkillbooks} (eg: {"Common|Attack", 0, 0, 0}, {"Common|Parry", 1, 0, 0} {})
zEntry.zaSkillbarAbilities = {}
table.insert(zEntry.zaSkillbarAbilities, {"Common|Attack",                      0, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Common|Parry",                       1, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Smarty Sage Internal|Slimy Strike",  2, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Smarty Sage Internal|Splash",        0, 1, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Smarty Sage Internal|Goopshot",      1, 1, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Smarty Sage Internal|Absorb",        2, 1, 1})
table.insert(zEntry.zaSkillbarAbilities, {"Smarty Sage Internal|Firm Up",       0, 2, 2})
table.insert(zEntry.zaSkillbarAbilities, {"Smarty Sage Internal|Jiggly Chest",  1, 2, 3})
table.insert(zEntry.zaSkillbarAbilities, {"Smarty Sage Internal|Big Dumb Grin", 2, 2, 4})

-- |[ ============ Call Standard ============= ]|
--Add to the global stack. There may be multiple executions of this script.
table.insert(gzJobCallStack, zEntry)

--Call.
LM_ExecuteScript(gsStandardJobPath, iSwitchType, iLevelReached)

--Pop the global stack.
table.remove(gzJobCallStack)
