-- |[ ====================================== Slimy Strike ====================================== ]|
-- |[Description]|
--Deals extra weapon damage. Costs CP.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.SmartySage.SlimyStrike == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Smarty Sage", "Slimy Strike", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Combo", "Mei|Fencer|PowerfulStrike", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\n\n\nIt's like getting hit with a wet sock.\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "A powerful finisher with a big slimy tendril.\nDeals copious amounts of [Stk](Strike) damage.\n\n\nLike getting hit with a wet sock.\nCosts 4[CPIco](CP)."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, 4, giNoCooldown, "Target Enemies Single", giNoCPGeneration)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Strike")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Striking, 5, 2.20)
    zAbiStruct.zExecutionAbiPackage:fnAddStun(50)
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Mei|Special"

    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.SmartySage.SlimyStrike = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.SmartySage.SlimyStrike

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
