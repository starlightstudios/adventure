-- |[ ==================================== Way Of The Slime ==================================== ]|
-- |[Description]|
--+2% HP per turn, +10% max HP, passive.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.SmartySage.WayOfTheSlime == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Smarty Sage", "Way Of The Slime", "Way of the Slime", gciJP_Cost_Passive, gbIsNotFreeAction, "Buff", "Passive", "Mei|WayOfTheSlime", "Passive")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Regen 2%%[Hlt]/turn, +10%% Max[Hlt].\n\n\n\n\n\nPassive."
    zAbiStruct.sSimpleDescMarkdown  = "Regenerate a small amount of health each turn.\nIncreases max health.\n\n\n\nPassive."

    -- |[Passive Effect]|
    zAbiStruct.bNeverAvailable   = true --Passive abilities have no usability requirements.
    zAbiStruct.sPassivePrototype = "Mei.SmartySage.WayOfTheSlime"
    zAbiStruct.bHasGUIEffects    = true
    
    -- |[Healing Effect]|
    --This is used by the proc, the ability is not activated.
    zAbiStruct:fnCreateAbiPack("Healing")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsHealing(piHealingBase, pfHealingScaleFactor, pfHealingPctOfHPMax)
    zAbiStruct.zExecutionAbiPackage:fnSetAsHealing(0, 0.00, 0.02)

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    if(EffectList:fnEntryExists(zAbiStruct.sPassivePrototype) == false) then
        local saDescription = {}
        saDescription[1] = "Can a slime ever truly be wounded?"
        saDescription[2] = "Increase Max HP, Regenerate 2%% HP per turn."
      --EffectList:fnCreatePassivePrototype(psName, psDisplayName, psIcon, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreatePassivePrototype(zAbiStruct.sPassivePrototype, "Way of the Slime", "Mei|WayOfTheSlime", "HPMax|Pct|0.10", saDescription)

        --Manually specify description.
        local iType, zPrototype, iSlot = EffectList:fnLocateEntry(zAbiStruct.sPassivePrototype)
        if(zPrototype ~= nil) then
            zPrototype.bDontRebuildDescription = true
            zPrototype.saStrings[1] = "[Buff]Way of the Slime"
            zPrototype.saStrings[2] = "[UpN][Hlt] [UpN][Hlt]/[Turns]"
            zPrototype.saStrings[3] = saDescription[1]
            zPrototype.saStrings[4] = saDescription[2]
            zPrototype.saStrings[5] = ""
            zPrototype.saStrings[6] = ""
            zPrototype.saStrings[7] = ""
            zPrototype.saStrings[8] = "[Buff] +10%%[Hlt] +2[Hlt]/[Turns]"
            zPrototype.sShortText = ""
        end
    end

    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.SmartySage.WayOfTheSlime = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.SmartySage.WayOfTheSlime

-- |[ ============================ Combat: Character Action Begins ============================= ]|
--Regenerate 2% of user's HP, in addition to the usual stuff.
if(iSwitchType == gciAbility_BeginAction) then

    -- |[Standard Call]|
    --Call standard.
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
    
    --Do nothing if the owner is not acting:
    if(AdvCombatAbility_GetProperty("Is Owner Acting") == false) then return end
    
    -- |[Generate HP]|
    --Reset prototype.
    gzRefAbility = gzPrototypes.Combat.Mei.SmartySage.WayOfTheSlime
    
    --Get owner ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOriginatorID = RO_GetID()
        local iHPMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
    DL_PopActiveObject()
    
    --Compute.
    local iHPRestore = iHPMax * 0.02
    if(iHPRestore < 1) then iHPRestore = 1 end
    
    --Ability executes.
    iTimer = gzRefAbility.zExecutionAbiPackage.fnAbilityAnimate(iOriginatorID, 0, gzRefAbility.zExecutionAbiPackage, false)
    iTimer = gzRefAbility.zExecutionAbiPackage.fnHealingAnimate(iOriginatorID, iOriginatorID, 0, iHPRestore)
    fnDefaultEndOfApplications(iTimer)

    --Clear prototype.
    gzRefAbility = nil

-- |[ ==================================== GUI: Apply Effect =================================== ]|
--When showing a character's statistics, all equipped abilities are queried. This is not reflected
-- in combat, this is GUI only.
--The standard does not handle this.
elseif(iSwitchType == gciAbility_GUIApplyEffect) then
    AdvCombatAbility_SetProperty("Push Owner")
        fnApplyStatBonusPct(gciStatIndex_HPMax, 0.10)
    DL_PopActiveObject()
    gzRefAbility = nil

-- |[ ===================================== All Other Cases ==================================== ]|
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
