-- |[ ========================================= Absorb ========================================= ]|
-- |[Description]|
--Deals 150% corroding damage, heals self for 20% damage dealt.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.SmartySage.Absorb == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Smarty Sage", "Absorb", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "Mei|Absorb", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\nHeal 20%% damage dealt as [Hlt].\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "A slimy attack that saps some HP.\nInflicts increased [Crd](Corrode) damage.\nHeals you for 20%% of damage dealt.\n\n\nCosts [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(30, giNoCPCost, giNoCooldown, "Target Enemies Single", 1)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Corrode")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Corroding, 5, 1.50)
    zAbiStruct.zExecutionAbiPackage:fnAddLifesteal(0.20)
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Mei|Ability:Absorb"

    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.SmartySage.Absorb = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.SmartySage.Absorb

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
