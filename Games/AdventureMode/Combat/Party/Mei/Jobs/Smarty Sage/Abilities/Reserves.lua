-- |[ ======================================== Reserves ======================================== ]|
-- |[Description]|
--Generates 20 MP.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Ability Prototype =================================== ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.SmartySage.Reserves == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Smarty Sage", "Reserves", "$SkillName", gciJP_Cost_Advanced, gbIsNotFreeAction, "Heal", "Active", "Mei|Reserves", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "+20 [Man], ends your turn.\n\n\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Summon your slimy reserves. Immediately\ngenerate 20[Man](MP).\n\n\n\nNo cost, ends your turn."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, giNoCPCost, giNoCooldown, "Target Self", giNoCPGeneration)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Healing")
    zAbiStruct.zExecutionAbiPackage:fnSetAsBuff()
    zAbiStruct.zExecutionAbiPackage:fnSetAsMPRestore(20)
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    --zAbiStruct.zExecutionAbiPackage.sVoiceData = "Mei|Ability:Whirl|General:Swing|General:Bleed|Offense"

    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.SmartySage.Reserves = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.SmartySage.Reserves

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
