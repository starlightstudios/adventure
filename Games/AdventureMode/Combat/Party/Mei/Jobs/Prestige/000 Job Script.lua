-- |[ ======================================== Prestige ======================================== ]|
--Prestige class. Cannot be used, contains passives that only unlock when all skills are purchased
-- from base classes and do not need to be equipped.

-- |[ ========== Argument Handling =========== ]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--1st argument is only used for level-up calls.
local iLevelReached = 0
if(iArgumentsTotal >= 2) then
    iLevelReached = LM_GetScriptArgument(1, "N")
end

-- |[ ============ Variable Setup ============ ]|
-- |[Enumerations]|
local gciNoSkillbooks = 0

-- |[Encapsulation]|
local zEntry = {}

-- |[Basic Variables]|
--Character Variables.
zEntry.sCharacterFormVarPath = "Root/Variables/Global/Mei/sForm"
zEntry.sSkillbookVarPath     = "Root/Variables/Global/Mei/iSkillbookTotal"
zEntry.zaJobChart            = gzaMeiJobChart
zEntry.sStatProfilePath      = gsRoot .. "Combat/Party/Mei/400 Common Stat Profile.lua"
zEntry.sSecondaryMenuPath    = gsRoot .. "Combat/Party/Mei/200 Build Secondary Menu.lua"

--Class Variables.
zEntry.sClassName   = "Prestige"
zEntry.sFormName    = "Human"
zEntry.sCostumeName = "Mei_Human"
zEntry.fFaceIndexX  = 0
zEntry.fFaceIndexY  = 0

-- |[Ability Variables]|
--Purchaseable abilities.
zEntry.saExternalAbilities = {}
table.insert(zEntry.saExternalAbilities, "Master Fencer")
table.insert(zEntry.saExternalAbilities, "Master Nightshade")
table.insert(zEntry.saExternalAbilities, "Master Maid")
table.insert(zEntry.saExternalAbilities, "Master Hive Scout")
table.insert(zEntry.saExternalAbilities, "Master Prowler")
table.insert(zEntry.saExternalAbilities, "Master Petraian")
table.insert(zEntry.saExternalAbilities, "Master Smarty Sage")
table.insert(zEntry.saExternalAbilities, "Master Soulherd")
table.insert(zEntry.saExternalAbilities, "Master Stalker")

--Class-equipped abilities.
zEntry.saInternalAbilities = {}

--Purchaseable abilities, in the order of purchase.
zEntry.saPurchaseAbilities = {}
table.insert(zEntry.saPurchaseAbilities, "Prestige|Master Fencer")
table.insert(zEntry.saPurchaseAbilities, "Prestige|Master Nightshade")
table.insert(zEntry.saPurchaseAbilities, "Prestige|Master Maid")
table.insert(zEntry.saPurchaseAbilities, "Prestige|Master Hive Scout")
table.insert(zEntry.saPurchaseAbilities, "Prestige|Master Prowler")
table.insert(zEntry.saPurchaseAbilities, "Prestige|Master Petraian")
table.insert(zEntry.saPurchaseAbilities, "Prestige|Master Smarty Sage")
table.insert(zEntry.saPurchaseAbilities, "Prestige|Master Soulherd")
table.insert(zEntry.saPurchaseAbilities, "Prestige|Master Stalker")

-- |[Skillbar Variables]|
--Skills that always appear on the skillbar. Format: {sAbilityName, iXPos, iYPos, iSkillbooks} (eg: {"Common|Attack", 0, 0, 0}, {"Common|Parry", 1, 0, 0} {})
zEntry.zaSkillbarAbilities = {}

-- |[ ============ Call Standard ============= ]|
--Add to the global stack. There may be multiple executions of this script.
table.insert(gzJobCallStack, zEntry)

--Call.
LM_ExecuteScript(gsStandardJobPath, iSwitchType, iLevelReached)

--Pop the global stack.
table.remove(gzJobCallStack)
