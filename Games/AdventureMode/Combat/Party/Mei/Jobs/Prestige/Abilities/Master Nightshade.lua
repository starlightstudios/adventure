-- |[ =================================== Master Nightshade ==================================== ]|
-- |[Description]|
--Passive, increases Poison damage by 5%. Does not need to be equipped, requires all other Nightshade
-- skills to be purchased before it unlocks automatically.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.Prestige.MasterNightshade == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Prestige", "Master Nightshade", "$SkillName", gciJP_Cost_Locked, gbIsNotFreeAction, "Buff", "Passive", "Mei|WayOfTheDruid", "Passive")
    
    --Runs even if not equipped, but must be unlocked.
    zAbiStruct.bRunIfNotEquipped = true
    zAbiStruct.bOnlyRunIfUnlocked = true
    
    --Permanent passives can never be equipped.
    zAbiStruct.bCannotBeEquipped = true
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "+5[PCT] [Psn] damage.\n\n\n\n\n\nPermanent passive."
    zAbiStruct.sSimpleDescMarkdown  = "Increases [Psn](Poison) damage permanently by 5[PCT].\nDoes not need to be equipped.\n\nYou must master the Nightshade class to unlock\nthis ability.\nPassive."
    
    -- |[Passive Effect]|
    zAbiStruct.bNeverAvailable   = true --Passive abilities have no usability requirements.
    zAbiStruct.sPassivePrototype = "Mei.Prestige.MasterNightshade"
    zAbiStruct.bHasGUIEffects    = true
    
    -- |[Tags]|
    --Apply tags to increase slashing damage.
    zAbiStruct.zaTags = {{"Poison Damage Dealt +", 5}}

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    -- Note: The effect doesn't increase slash damage, the ability itself does.
    if(EffectList:fnEntryExists(zAbiStruct.sPassivePrototype) == false) then

        -- |[Creation]|
        --This effect uses a standardized StatMod script.
        local sDisplayName = "Master Nightshade"
        local iDuration = -1
        local bIsBuff = true
        local sIcon = "Root/Images/AdventureUI/Abilities/Mei|WayOfTheDruid"
        local sStatString = ""

        --Needs to manually set the description to account for the tag damage.
        local saDescription = {}
        saDescription[1] = "You have mastered the Nightshade class. Increases your"
        saDescription[2] = "[Psn](Poison) damage permanently."
        saDescription[3] = ""
        saDescription[4] = ""
        saDescription[5] = ""
        saDescription[6] = "[Buff] [Psn]+5[PCT]Dam"
        saDescription[7] = ""

        --Run the standardized builder.
        local zPrototype = fnCreateStatmodEffectPrototype(sDisplayName, iDuration, bIsBuff, sIcon, sStatString, saDescription, true)

        -- |[Overrides]|
        --Passive, not removed on KO, uses different frame.
        zPrototype.sFrame      = gsAbility_Frame_Passive
        zPrototype.bRemoveOnKO = false
    
        -- |[Register]|
        fnRegisterEffectPrototype(zAbiStruct.sPassivePrototype, gciEffect_Is_StatMod, zPrototype)
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.Prestige.MasterNightshade = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.Prestige.MasterNightshade

--Call standard.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
