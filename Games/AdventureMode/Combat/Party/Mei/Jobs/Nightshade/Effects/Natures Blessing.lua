-- |[ =================================== Nature's Blessing ==================================== ]|
-- |[Description]|
--Regen 4% of HP per turn.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Local Variables ===================================== ]|
-- |[System]|
--This effect uses a standardized StatMod script.
local sDisplayName = "Nature's Blessing"
local iDuration = -1
local bIsBuff = true
local sIcon = "Root/Images/AdventureUI/Abilities/Mei|NaturesBlessing"
local sStatString = ""

--Description can be a variable number of lines.
local saDescription = {}
saDescription[1] = "Regrow yourself a little."
saDescription[2] = "Regenerate 4%% HP per turn."

--Run the standardized builder.
gzaStatModStruct            = fnCreateStatmodEffectPrototype(sDisplayName, iDuration, bIsBuff, sIcon, sStatString)
gzaStatModStruct.saStrings  = fnStatmodBuildInspectorStrings(gzaStatModStruct, saDescription)
gzaStatModStruct.sShortText = fnStatmodBuildShortText(gzaStatModStruct)

-- |[Overrides]|
--Passive, not removed on KO, uses different frame.
gzaStatModStruct.sFrame      = gsAbility_Frame_Passive
gzaStatModStruct.bRemoveOnKO = false

--Specify description manually.
gzaStatModStruct.saStrings = {}
gzaStatModStruct.saStrings[1] = "[Buff]Nature's Blessing"
gzaStatModStruct.saStrings[2] = "[UpN][Hlt]/[Turns]"
gzaStatModStruct.saStrings[3] = saDescription[1]
gzaStatModStruct.saStrings[4] = saDescription[2]
gzaStatModStruct.saStrings[5] = ""
gzaStatModStruct.saStrings[6] = ""
gzaStatModStruct.saStrings[7] = ""
gzaStatModStruct.saStrings[8] = "[Buff] +4%%[Hlt]/[Turns]"
gzaStatModStruct.sShortText = ""

-- |[ ======================================== Creation ======================================== ]|
--Standard call when effect is initialized.
if(iSwitchType == gciEffect_Create) then
    LM_ExecuteScript(gsStandardStatPath, gciEffect_Create)
    
-- |[ ================================== Apply/Unapply Stats =================================== ]|
--Calls the apply/unapply stat routines.
elseif((iSwitchType == gciEffect_ApplyStats or iSwitchType == gciEffect_UnApplyStats) and iArgumentsTotal >= 2) then
    local iTargetID = math.floor(tonumber(LM_GetScriptArgument(1, "N")))
    LM_ExecuteScript(gsStandardStatPath, iSwitchType, iTargetID)

-- |[ ================================== Character Action Ends ================================= ]|
--Called after a character action ends to decrement the turn counter.
elseif(iSwitchType == gciEffect_PostAction) then

    --Description:
    --Non-standard effect code. Computes 4%% of max HP and enqueues an event to heal that much HP.

    --Get ID.
    AdvCombat_SetProperty("Push Acting Entity")
        local iActingEntityID = RO_GetID()
        local iHPMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
    DL_PopActiveObject()
    
    --If the ID is not on the target list, we don't care.
    if(AdvCombatEffect_GetProperty("Is ID on Target List", iActingEntityID) == false) then
        return
    end
    
    --Default package.
    local zaAbilityPackage = fnConstructDefaultAbilityPackage("Healing")
    zaAbilityPackage.iOriginatorID = iOriginatorID
    zaAbilityPackage.bDoesNoDamage = true
    zaAbilityPackage.bDoesNoStun = true
    zaAbilityPackage.bAlwaysHits = true
    zaAbilityPackage.fHealingPercent = 0.04
    
    --Setup.
    local iTimer = 0
    local iHealing = math.floor(iHPMax * 0.04)
    if(iHealing < 1) then iHealing = 1 end
    
    --Ability executes.
    iTimer = zaAbilityPackage.fnAbilityAnimate(iActingEntityID, iTimer, zaAbilityPackage, false)
    iTimer = zaAbilityPackage.fnHealingAnimate(iActingEntityID, iActingEntityID, iTimer, iHealing)
    fnDefaultEndOfApplications(iTimer)

end
