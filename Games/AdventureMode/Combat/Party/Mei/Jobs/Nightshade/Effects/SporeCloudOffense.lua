-- |[ ================================== Spore Cloud  Offense ================================== ]|
-- |[Description]|
--Deals poison damage equal to 0.75 of user's attack power over 3 turns.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--Setup, apply overrides.
if(gzEffectNightshadeSporeCloudOffense == nil) then
    
    --Base, overrides.
    local zEffectStruct = fnCreateDoTEffectPrototype(gciDamageType_Poisoning, 3, 0.75)
    zEffectStruct.sDisplayName = "Spore Cloud (DoT)"
    zEffectStruct.sIcon = "Mei|SporeCloud"
    
    --Store
    gzEffectNightshadeSporeCloudOffense = zEffectStruct
    
end

-- |[Set and Execute]|
--Standardized Naming
gzRefEffect = gzEffectNightshadeSporeCloudOffense

--Call the standard handler. It will nil off the gzRefEffect.
LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType)
