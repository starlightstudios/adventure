-- |[ ==================================== Way of the Druid ==================================== ]|
-- |[Description]|
--Passive, increases MP regen by 5/turn.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.Nightshade.WayOfTheDruid == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Nightshade", "Way Of The Druid", "Way of the Druid", gciJP_Cost_Passive, gbIsNotFreeAction, "Buff", "Passive", "Mei|WayOfTheDruid", "Passive")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "+5[Man] per turn.\n\n\n\n\n\nPassive."
    zAbiStruct.sSimpleDescMarkdown  = "Gain 5 more [Man](MP) per turn.\n\n\n\n\nPassive."
    
    -- |[Passive Effect]|
    zAbiStruct.bNeverAvailable   = true --Passive abilities have no usability requirements.
    zAbiStruct.sPassivePrototype = "Mei.Nightshade.WayOfTheDruid"
    zAbiStruct.bHasGUIEffects    = false

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    if(EffectList:fnEntryExists(zAbiStruct.sPassivePrototype) == false) then
        local saDescription = {}
        saDescription[1] = "In tune with nature, the alraune commands "
        saDescription[2] = "natural magic."
        saDescription[3] = "Increases [Man](MP) regen per turn by 5."
      --EffectList:fnCreatePassivePrototype(psName, psDisplayName, psIcon, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreatePassivePrototype(zAbiStruct.sPassivePrototype, "Way of the Druid", "Mei|WayOfTheDruid", "MPReg|Flat|5", saDescription)
    end

    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.Nightshade.WayOfTheDruid = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.Nightshade.WayOfTheDruid

--Call standard.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
