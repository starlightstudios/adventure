-- |[ ==================================== Nature's Blessing =================================== ]|
-- |[Description]|
--Regenerate 4% of max HP per turn.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.Nightshade.NaturesBlessing == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Nightshade", "Natures Blessing", "Nature's Blessing", gciJP_Cost_Passive, gbIsNotFreeAction, "Buff", "Passive", "Mei|NaturesBlessing", "Passive")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Regenerate 4%% of Max [Hlt] at the start of your turn.\n\n\n\n\n\nPassive."
    zAbiStruct.sSimpleDescMarkdown  = "Regenerate some health every turn.\n\n\n\n\nPassive."
    
    -- |[Passive Effect]|
    zAbiStruct.bNeverAvailable = true                                                   --Passive abilities have no usability requirements.
    zAbiStruct.sPassivePath    = fnResolvePath() .. "../Effects/Natures Blessing.lua"   --Effect script handles the HP recovery.
    zAbiStruct.bHasGUIEffects  = false

    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.Nightshade.NaturesBlessing = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.Nightshade.NaturesBlessing

-- |[ ===================================== All Other Cases ==================================== ]|
if(iSwitchType ~= gciAbility_GUIApplyEffect) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

-- |[ ==================================== GUI: Apply Effect =================================== ]|
--When showing a character's statistics, all equipped abilities are queried. This is not reflected
-- in combat, this is GUI only.
--The standard does not handle this.
elseif(iSwitchType == gciAbility_GUIApplyEffect) then
    AdvCombatAbility_SetProperty("Push Owner")
        fnApplyStatBonus(gciStatIndex_Evade, 15)
        fnApplyStatBonus(gciStatIndex_Attack, 20)
    DL_PopActiveObject()
    gzRefAbility = nil
end
