-- |[ ======================================= Spore Cloud ====================================== ]|
-- |[Description]|
--All party members have evade increased, all enemies are afflicted with a poison DoT.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.Nightshade.SporeCloud == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Nightshade", "Spore Cloud", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "Mei|SporeCloud", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[HEffect][Str5][Psn] DoT of [0.75x [Atk]] [Psn] over 3[Turns], all enemies.\n[FEffect]+25[Evd] for 3[Turns], all allies.\n3 [Turns] cooldown.\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Release a cloud of spores over the battlefield.\nAll enemies take reduced [Psn](Poison) damage.\nAll allies gain [Evd](Evade).\nLasts 3 turns.\n\nCosts [MPCost][MPIco](MP). "..
                                      "3 turn cooldown."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(30, giNoCPCost, 3, "Target All", 1)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Poison")
    zAbiStruct.zExecutionAbiPackage.bDoesNoDamage = true
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun   = true
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits   = true
    zAbiStruct.zExecutionAbiPackage.bNeverCrits   = true
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Mei|General:Poison|Offense"

    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.Nightshade.SporeCloud = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.Nightshade.SporeCloud

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--Prediction changes based on hostile/ally party.
if(iSwitchType == gciAbility_BuildPredictionBox) then

    -- |[ ========================= Setup ======================== ]|
    --This ability targets everyone, and therefore has two prediction box routines. One for allies, one for enemies.
    -- First, get values.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
    DL_PopActiveObject()
    local iOwnerParty = AdvCombat_GetProperty("Party Of ID", iOwnerID)
    
    -- |[ =================== Offensive Package ================== ]|
    --Used for the damaging part.
    local zaOffensePackage = fnCreatePredictionPack()
    
    --Create an Effect module
    zaOffensePackage.zaEffectModules[1] = fnCreateEffectModule()
    zaOffensePackage.zaEffectModules[1].bIsDamageOverTime = true
    zaOffensePackage.zaEffectModules[1].iEffectType = gciDamageType_Poisoning
    zaOffensePackage.zaEffectModules[1].iDoTDamageType = gciDamageType_Poisoning
    zaOffensePackage.zaEffectModules[1].iEffectStrength = 5
    zaOffensePackage.zaEffectModules[1].fDotAttackFactor = 0.25
    zaOffensePackage.zaEffectModules[1].sResultScript = "[DAM][IMG1] for 3[IMG2]"
    zaOffensePackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/DamageTypeIcons/Poisoning", "Root/Images/AdventureUI/DebuffIcons/Clock"}

    -- |[ ==================== Support Package =================== ]|
    --Used for allies.
    local zaSupportPackage = fnCreatePredictionPack()
    
    --Create an Effect module
    zaSupportPackage.zaEffectModules[1] = fnCreateEffectModule()
    zaSupportPackage.zaEffectModules[1].bIsBuff = true
    zaSupportPackage.zaEffectModules[1].iEffectType = gciDamageType_Piercing
    zaSupportPackage.zaEffectModules[1].iEffectStrength = 1000
    zaSupportPackage.zaEffectModules[1].sResultScript = "+25[IMG1] / 3[IMG2]"
    zaSupportPackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/StatisticIcons/Evade", "Root/Images/AdventureUI/DebuffIcons/Clock"}

    -- |[ ========================= Apply ======================== ]|
    --Get information about the cluster. This is needed to give each box a unique ID.
    local iClusterID = AdvCombat_GetProperty("Prediction Get Cluster ID")
    
    --Iterate.
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    for i = 0, iClusterTargetsTotal-1, 1 do
        
        --Get the ID of the target.
        local iTargetID = AdvCombat_GetProperty("Prediction Get Targets ID", i)
        local iTargetParty = AdvCombat_GetProperty("Party Of ID", iTargetID)
        
        --Is it in the same party as the owner?
        if(iTargetParty == iOwnerParty) then
            fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, zaSupportPackage)
        --Hostile.
        else
            fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, zaOffensePackage)
        end
    end

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[ ================= Originator Statistics ================ ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        fnModifyCP(1)
        fnModifyMP(-30)
    DL_PopActiveObject()
    AdvCombatAbility_SetProperty("Cooldown", 3)
    
    -- |[ ================== Offensive Package =================== ]|
    --Create an ability package to be applied to unfriendly entities.
    local zaOffensePackage = fnConstructDefaultAbilityPackage("Null")
    zaOffensePackage.bDoesNoDamage = true
    zaOffensePackage.bDoesNoStun = true
    zaOffensePackage.bDoesNotAnimate = true
    zaOffensePackage.bAlwaysHits = true
    zaOffensePackage.bAlwaysCrits = false
    zaOffensePackage.bNeverCrits = true
    zaOffensePackage.bEffectAlwaysApplies = false
    
    --Offensive Effect Package
    local zaOffenseEffectPackage = fnConstructDefaultEffectPackage()
    zaOffenseEffectPackage.iOriginatorID = iOriginatorID
    
    --Apply overrides.
    zaOffenseEffectPackage.iEffectStr      = 5
    zaOffenseEffectPackage.iEffectCritStr  = 5
    zaOffenseEffectPackage.iEffectType     = gciDamageType_Poisoning
    zaOffenseEffectPackage.sEffectPath     = fnResolvePath() .. "../Effects/SporeCloudOffense.lua"
    zaOffenseEffectPackage.sEffectCritPath = fnResolvePath() .. "../Effects/SporeCloudOffense.lua"
    zaOffenseEffectPackage.sApplyText      = "Poisoned!"
    zaOffenseEffectPackage.sApplyTextCol   = "Color:Red"
    zaOffenseEffectPackage.sResistText     = "Resisted!"
    zaOffenseEffectPackage.sResistTextCol  = "Color:White"
    
    --Tags.
    zaOffenseEffectPackage.saApplyTagBonus    = {"Poison Apply +"}
    zaOffenseEffectPackage.saApplyTagMalus    = {"Poison Apply -"}
    zaOffenseEffectPackage.saSeverityTagBonus = {"Poison Effect +"}
    zaOffenseEffectPackage.saSeverityTagMalus = {"Poison Effect -"}
    zaOffenseEffectPackage.zaTags = {{"Florentina No Called Shot", 1}}
    
    -- |[ =================== Support Package ==================== ]|
    --For friendlies, use this package.
    local zaSupportPackage = fnConstructDefaultAbilityPackage("Buff")
    zaSupportPackage.bDoesNoDamage = true
    zaSupportPackage.bDoesNoStun = true
    --zaSupportPackage.bDoesNotAnimate = true
    zaSupportPackage.bAlwaysHits = true
    zaSupportPackage.bAlwaysCrits = false
    zaSupportPackage.bNeverCrits = true
    zaSupportPackage.bEffectAlwaysApplies = true
    
    --Additional Animations.
    fnAddAnimationPackage(zaSupportPackage, "Buff Evade", 0)
    
    --Support Effect Package
    local zaSupportEffectPackage = fnConstructDefaultEffectPackage()
    zaSupportEffectPackage.iOriginatorID = iOriginatorID
    
    --Apply overrides.
    zaSupportEffectPackage.iEffectType     = gciDamageType_Poisoning
    zaSupportEffectPackage.sEffectPath     = fnResolvePath() .. "../Effects/SporeCloudSupport.lua"
    zaSupportEffectPackage.sEffectCritPath = fnResolvePath() .. "../Effects/SporeCloudSupport.lua"
    zaSupportEffectPackage.sApplySound     = "Combat\\|Impact_Buff"
    zaSupportEffectPackage.sApplyText      = ""
    zaSupportEffectPackage.sApplyTextCol   = "Color:Green"
    zaSupportEffectPackage.sResistText     = ""
    zaSupportEffectPackage.sResistTextCol  = "Color:White"
    
    -- |[ ======================= Animation ====================== ]|
    --Spawn several poison effects at various spots on the field.
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID,  0, "Create Animation|Poison|AttackAnim0|UseX:150|UseY:300")
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID,  0, "Play Sound|Combat\\|Impact_Bleed")
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 20, "Create Animation|Poison|AttackAnim0|UseX:750|UseY:300")
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 20, "Play Sound|Combat\\|Impact_Bleed")
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 40, "Create Animation|Poison|AttackAnim0|UseX:350|UseY:300")
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 40, "Play Sound|Combat\\|Impact_Bleed")
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 60, "Create Animation|Poison|AttackAnim0|UseX:550|UseY:300")
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 60, "Play Sound|Combat\\|Impact_Bleed")
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 80, "Create Animation|Poison|AttackAnim0|UseX:950|UseY:300")
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 80, "Play Sound|Combat\\|Impact_Bleed")
    
    -- |[ ==================== For Each Target =================== ]|
    --Party of the originator.
    local iOriginatorParty = AdvCombat_GetProperty("Party Of ID", iOriginatorID)
    
    --Compute how large an offset is needed. Each action takes gciApplication_TextTicks, so multiply
    -- that by the number of targets.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    local iOffset = 65 - (gciApplication_TextTicks * iTargetsTotal)
    if(iOffset < 0) then iOffset = 0 end
    
    --Get how many targets were painted by this ability, iterate across them.
    for i = 1, iTargetsTotal, 1 do
        
        --Get party of target.
        AdvCombat_SetProperty("Push Target", i-1) 
            local iTargetUniqueID = RO_GetID()
        DL_PopActiveObject()
        local iTargetParty = AdvCombat_GetProperty("Party Of ID", iTargetUniqueID)
        
        --Timer offset to allow animations to play. Gets reset after each call of fnStandardExecution().
        giCombatTimerOffset = iOffset
        
        --Friendly:
        if(iOriginatorParty == iTargetParty) then
            fnStandardExecution(iOriginatorID, i-1, zaSupportPackage, zaSupportEffectPackage)
            
        --Hostile:
        else
            fnStandardExecution(iOriginatorID, i-1, zaOffensePackage, zaOffenseEffectPackage)
        end
    end


-- |[ ==================================== Standard Handler ==================================== ]|
--Call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
