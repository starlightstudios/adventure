-- |[ ======================================= Nightshade ======================================= ]|
--The Alraune class.

-- |[ ========== Argument Handling =========== ]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--1st argument is only used for level-up calls.
local iLevelReached = 0
if(iArgumentsTotal >= 2) then
    iLevelReached = LM_GetScriptArgument(1, "N")
end

-- |[ ============ Variable Setup ============ ]|
-- |[Enumerations]|
local gciNoSkillbooks = 0

-- |[Encapsulation]|
zEntry = {}

-- |[Basic Variables]|
--Character Variables.
zEntry.sCharacterFormVarPath = "Root/Variables/Global/Mei/sForm"
zEntry.sSkillbookVarPath     = "Root/Variables/Global/Mei/iSkillbookTotal"
zEntry.zaJobChart            = gzaMeiJobChart
zEntry.sStatProfilePath      = gsRoot .. "Combat/Party/Mei/400 Common Stat Profile.lua"
zEntry.sSecondaryMenuPath    = gsRoot .. "Combat/Party/Mei/200 Build Secondary Menu.lua"
zEntry.sMasterBonusPath      = gsRoot .. "Combat/Party/Mei/401 Mastery Bonus Handler.lua"

--Class Variables.
zEntry.sClassName   = "Nightshade"
zEntry.sFormName    = "Alraune"
zEntry.sCostumeName = "Mei_Alraune"
zEntry.fFaceIndexX  = 1
zEntry.fFaceIndexY  = 0

-- |[Ability Variables]|
--Purchaseable abilities.
zEntry.saExternalAbilities = {}
table.insert(zEntry.saExternalAbilities, "PollenRend")
table.insert(zEntry.saExternalAbilities, "VineLash")
table.insert(zEntry.saExternalAbilities, "SporeCloud")
table.insert(zEntry.saExternalAbilities, "NaturesBlessing")
table.insert(zEntry.saExternalAbilities, "Ripple")
table.insert(zEntry.saExternalAbilities, "FanOfLeaves")
table.insert(zEntry.saExternalAbilities, "Mend")
table.insert(zEntry.saExternalAbilities, "Regrowth")
table.insert(zEntry.saExternalAbilities, "AcidBlood")
table.insert(zEntry.saExternalAbilities, "WayOfTheDruid")

--Class-equipped abilities.
zEntry.saInternalAbilities = {}
table.insert(zEntry.saInternalAbilities, "PollenRend")
table.insert(zEntry.saInternalAbilities, "VineLash")
table.insert(zEntry.saInternalAbilities, "SporeCloud")
table.insert(zEntry.saInternalAbilities, "Ripple")
table.insert(zEntry.saInternalAbilities, "FanOfLeaves")
table.insert(zEntry.saInternalAbilities, "NaturesBlessing")
table.insert(zEntry.saInternalAbilities, "Regrowth")

--Purchaseable abilities, in the order of purchase.
zEntry.saPurchaseAbilities = {}
table.insert(zEntry.saPurchaseAbilities, "Nightshade|Vine Lash")
table.insert(zEntry.saPurchaseAbilities, "Nightshade|Fan Of Leaves")
table.insert(zEntry.saPurchaseAbilities, "Nightshade|Ripple")
table.insert(zEntry.saPurchaseAbilities, "Nightshade|Pollen Rend")
table.insert(zEntry.saPurchaseAbilities, "Nightshade|Spore Cloud")
table.insert(zEntry.saPurchaseAbilities, "Nightshade|Mend")
table.insert(zEntry.saPurchaseAbilities, "Nightshade|Regrowth")
table.insert(zEntry.saPurchaseAbilities, "Nightshade|Natures Blessing")
table.insert(zEntry.saPurchaseAbilities, "Nightshade|Acid Blood")
table.insert(zEntry.saPurchaseAbilities, "Nightshade|Way Of The Druid")
                                          
-- |[Skillbar Variables]|
--Skills that always appear on the skillbar. Format: {sAbilityName, iXPos, iYPos, iSkillbooks} (eg: {"Common|Attack", 0, 0, 0}, {"Common|Parry", 1, 0, 0} {})
zEntry.zaSkillbarAbilities = {}
table.insert(zEntry.zaSkillbarAbilities, {"Common|Attack",                        0, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Common|Parry",                         1, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Nightshade Internal|Regrowth",         2, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Nightshade Internal|Pollen Rend",      0, 1, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Nightshade Internal|Vine Lash",        1, 1, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Nightshade Internal|Spore Cloud",      2, 1, 1})
table.insert(zEntry.zaSkillbarAbilities, {"Nightshade Internal|Ripple",           0, 2, 2})
table.insert(zEntry.zaSkillbarAbilities, {"Nightshade Internal|Fan Of Leaves",    1, 2, 3})
table.insert(zEntry.zaSkillbarAbilities, {"Nightshade Internal|Natures Blessing", 2, 2, 4})

-- |[ ============ Call Standard ============= ]|
--Add to the global stack. There may be multiple executions of this script.
table.insert(gzJobCallStack, zEntry)

--Call.
LM_ExecuteScript(gsStandardJobPath, iSwitchType, iLevelReached)

--Pop the global stack.
table.remove(gzJobCallStack)
