-- |[ ======================================= Hive Scout ======================================= ]|
--Mei's bee class. Big stinger, hive mind, wings, this class has it all!

-- |[ ========== Argument Handling =========== ]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--1st argument is only used for level-up calls.
local iLevelReached = 0
if(iArgumentsTotal >= 2) then
    iLevelReached = LM_GetScriptArgument(1, "N")
end

-- |[ ============ Variable Setup ============ ]|
-- |[Enumerations]|
local gciNoSkillbooks = 0

-- |[Encapsulation]|
local zEntry = {}

-- |[Basic Variables]|
--Character Variables.
zEntry.sCharacterFormVarPath = "Root/Variables/Global/Mei/sForm"
zEntry.sSkillbookVarPath     = "Root/Variables/Global/Mei/iSkillbookTotal"
zEntry.zaJobChart            = gzaMeiJobChart
zEntry.sStatProfilePath      = gsRoot .. "Combat/Party/Mei/400 Common Stat Profile.lua"
zEntry.sSecondaryMenuPath    = gsRoot .. "Combat/Party/Mei/200 Build Secondary Menu.lua"
zEntry.sMasterBonusPath      = gsRoot .. "Combat/Party/Mei/401 Mastery Bonus Handler.lua"

--Class Variables.
zEntry.sClassName   = "Hive Scout"
zEntry.sFormName    = "Bee"
zEntry.sCostumeName = "Mei_Bee"
zEntry.fFaceIndexX  = 2
zEntry.fFaceIndexY  = 0

-- |[Ability Variables]|
--Purchaseable abilities.
zEntry.saExternalAbilities = {}
table.insert(zEntry.saExternalAbilities, "Call For Help") 
table.insert(zEntry.saExternalAbilities, "Claw Kick")      
table.insert(zEntry.saExternalAbilities, "Coordinate")    
table.insert(zEntry.saExternalAbilities, "Flap Jump")     
table.insert(zEntry.saExternalAbilities, "Scouts Honor") 
table.insert(zEntry.saExternalAbilities, "Sting")          
table.insert(zEntry.saExternalAbilities, "Hive Locus")  
table.insert(zEntry.saExternalAbilities, "Wax Armor")    
table.insert(zEntry.saExternalAbilities, "Way Of The Hive")
table.insert(zEntry.saExternalAbilities, "Scout Sight")  
    
--Class-equipped abilities.
zEntry.saInternalAbilities = {}
table.insert(zEntry.saInternalAbilities, "Sting")      
table.insert(zEntry.saInternalAbilities, "Claw Kick")   
table.insert(zEntry.saInternalAbilities, "Wax Armor") 
table.insert(zEntry.saInternalAbilities, "Coordinate")
table.insert(zEntry.saInternalAbilities, "Flap Jump") 
table.insert(zEntry.saInternalAbilities, "Scouts Honor")
table.insert(zEntry.saInternalAbilities, "Call For Help")

--Purchaseable abilities, in the order of purchase.
zEntry.saPurchaseAbilities = {}
table.insert(zEntry.saPurchaseAbilities, "Hive Scout|Flap Jump")
table.insert(zEntry.saPurchaseAbilities, "Hive Scout|Sting")
table.insert(zEntry.saPurchaseAbilities, "Hive Scout|Claw Kick")
table.insert(zEntry.saPurchaseAbilities, "Hive Scout|Coordinate")
table.insert(zEntry.saPurchaseAbilities, "Hive Scout|Hive Locus")
table.insert(zEntry.saPurchaseAbilities, "Hive Scout|Wax Armor")
table.insert(zEntry.saPurchaseAbilities, "Hive Scout|Call For Help")
table.insert(zEntry.saPurchaseAbilities, "Hive Scout|Scouts Honor")
table.insert(zEntry.saPurchaseAbilities, "Hive Scout|Way Of The Hive")
table.insert(zEntry.saPurchaseAbilities, "Hive Scout|Scout Sight")

-- |[Skillbar Variables]|
--Skills that always appear on the skillbar. Format: {sAbilityName, iXPos, iYPos, iSkillbooks} (eg: {"Common|Attack", 0, 0, 0}, {"Common|Parry", 1, 0, 0} {})
zEntry.zaSkillbarAbilities = {}
table.insert(zEntry.zaSkillbarAbilities, {"Common|Attack",                     0, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Common|Parry",                      1, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Hive Scout Internal|Call For Help", 2, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Hive Scout Internal|Sting",         0, 1, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Hive Scout Internal|Claw Kick",     1, 1, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Hive Scout Internal|Wax Armor",     2, 1, 1})
table.insert(zEntry.zaSkillbarAbilities, {"Hive Scout Internal|Coordinate",    0, 2, 2})
table.insert(zEntry.zaSkillbarAbilities, {"Hive Scout Internal|Flap Jump",     1, 2, 3})
table.insert(zEntry.zaSkillbarAbilities, {"Hive Scout Internal|Scouts Honor",  2, 2, 4})

-- |[ ============ Call Standard ============= ]|
--Add to the global stack. There may be multiple executions of this script.
table.insert(gzJobCallStack, zEntry)

--Call.
LM_ExecuteScript(gsStandardJobPath, iSwitchType, iLevelReached)

--Pop the global stack.
table.remove(gzJobCallStack)
