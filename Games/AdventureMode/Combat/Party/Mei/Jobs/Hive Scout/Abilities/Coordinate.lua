-- |[ ======================================= Coordinate ======================================= ]|
-- |[Description]|
--Next attack is guaranteed to hit.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Ability Prototype =================================== ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.HiveScout.Coordinate == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Hive Scout", "Coordinate", "$SkillName", gciJP_Cost_Normal, gbIsFreeAction, "Buff", "Active", "Mei|Coordinate", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Next attack is guaranteed to hit. [Target].\n\n\n\n\nFree action.\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Allow the hive mind to guide your blade.\nNext attack always hits.\n\n\nFree action.\nCosts [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(30, giNoCPCost, 1, "Target Self", giNoCPGeneration)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Buff")
    zAbiStruct.zExecutionAbiPackage:fnSetAsBuff()
  --zAbiStruct.zExecutionAbiPackage:fnAddAnimation(psAnimationName, piTimerOffset, $pfXOffset, $pfYOffset)
    zAbiStruct.zExecutionAbiPackage:fnAddAnimation("Buff Accuracy", 0)
    
    -- |[Effect Package]|
  --zAbiStruct:fnAddBuff(psPrototypeName, $psApplyText, $psApplyColorString, $psApplyAnim, $psApplySound)
    zAbiStruct:fnAddBuff("Mei.HiveScout.Coordinate")

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
        local saDescription = {}
        saDescription[1] = "Use the hivemind to help you plan your attacks!"
        saDescription[2] = "Causes the next attack to always hit."
      --EffectList:fnCreateBuffPrototype(psName, psDisplayName, psIcon, piTurns, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreateBuffPrototype(sLocalPrototypeName, "Coordinate", "Mei|Coordinate", 1, "", saDescription, {{"Always Hit", 1}})

        --Manually specify description.
        local iType, zPrototype, iSlot = EffectList:fnLocateEntry(sLocalPrototypeName)
        if(zPrototype ~= nil) then
            zPrototype.bDontRebuildDescription = true
            zPrototype.saStrings[1] = "[Buff]Coordinate"
            zPrototype.saStrings[2] = "+[Always Hit] /[Dur][Turns]"
            zPrototype.saStrings[3] = saDescription[1]
            zPrototype.saStrings[4] = saDescription[2]
            zPrototype.saStrings[5] = ""
            zPrototype.saStrings[6] = ""
            zPrototype.saStrings[7] = ""
            zPrototype.saStrings[8] = "[Buff] [Always Hit] ([Dur][Turns])"
            zPrototype.sShortText = "[Buff] [Always Hit] ([Dur][Turns])"
        end
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Mei|Ability:Coordinate"

    -- |[Finalize]|
    --Autobuild.
    zAbiStruct:fnFinalize()
    
    --Prevent automatic effects from showing. We do this manually.
    zAbiStruct.zPredictionPackage.zaEffectModules = {}
    
    --Add a manual line to the prediction.
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "Guaranteed Hit, 1[IMG0]"
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {"Root/Images/AdventureUI/DebuffIcons/Clock"}
    
    --Set ref.
    gzPrototypes.Combat.Mei.HiveScout.Coordinate = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.HiveScout.Coordinate

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
