-- |[ ======================================= Hive Locus ======================================= ]|
-- |[Description]|
--Immediately generates 40 MP, but leaves a -100Acc debuff for 1 turn. Free action.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Ability Prototype =================================== ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.HiveScout.HiveLocus == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Hive Scout", "Hive Locus", "$SkillName", gciJP_Cost_Advanced, gbIsFreeAction, "Heal", "Active", "Mei|HiveLocus", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Generate 40[Man].\n[HEffect] -100[Acc] to self for 1 turn.\n\n\n\n\nFree action."
    zAbiStruct.sSimpleDescMarkdown  = "Generate 40[Man](MP), but exhaust the hive mind.\nGreatly reduces [Acc](Accuracy) until your next\nturn.\n\n\nFree action."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, giNoCPCost, giNoCooldown, "Target Self", giNoCPGeneration)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Healing")
    zAbiStruct.zExecutionAbiPackage:fnSetAsBuff()
    zAbiStruct.zExecutionAbiPackage:fnSetAsMPRestore(40)
    
    -- |[Effect Package]|
  --zAbiStruct:fnAddBuff(psPrototypeName, $psApplyText, $psApplyColorString, $psApplyAnim, $psApplySound)
    zAbiStruct:fnAddBuff("Mei.HiveScout.HiveLocus", "Overtaxed!", "Color:Purple", "Debuff", nil)

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
        local saDescription = {}
        saDescription[1] = "The hive is overtaxed from generating mana."
        saDescription[2] = "Greatly decreases [Acc](Accuracy) for your next"
        saDescription[3] = "attack."
      --EffectList:fnCreateBuffPrototype(psName, psDisplayName, psIcon, piTurns, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreateBuffPrototype(sLocalPrototypeName, "Hive Locus", "Mei|HiveLocus", 1, "Acc|Flat|-100", saDescription)
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    --zAbiStruct.zExecutionAbiPackage.sVoiceData = "Mei|Ability:Whirl|General:Swing|General:Bleed|Offense"

    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.HiveScout.HiveLocus = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.HiveScout.HiveLocus

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
