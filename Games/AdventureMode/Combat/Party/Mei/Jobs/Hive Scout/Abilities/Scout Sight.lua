-- |[ ======================================= Scout Sight ====================================== ]|
-- |[Description]|
--Unlocks the Scout Sight field ability. Cannot be equipped.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Ability Prototype =================================== ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.HiveScout.ScoutSight == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Hive Scout", "Scout Sight", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Special", "Mei|ScoutsHonor", "Unequippable")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Unlocks the Scout Sight field ability, allowing you to\nsee more area around you."
    zAbiStruct.sSimpleDescMarkdown  = "Unlocks the Scout Sight field ability.\nThis ability allows you to see further\non the overworld.\n\nField abilities can be seen in the top left corner\nwhen out of battle."

    -- |[Usability Variables]|
    zAbiStruct.bNeverAvailable = true --Field abilities have no usability requirements.
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.HiveScout.ScoutSight = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.HiveScout.ScoutSight

--Call standard.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)