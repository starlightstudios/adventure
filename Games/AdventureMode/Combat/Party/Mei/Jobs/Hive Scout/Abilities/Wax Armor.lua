-- |[ ======================================== Wax Armor ======================================= ]|
-- |[Description]|
--Increases protection by 5 for 3 turns, free action.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Ability Prototype =================================== ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.HiveScout.WaxArmor == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Hive Scout", "Wax Armor", "$SkillName", gciJP_Cost_Normal, gbIsFreeAction, "Buff", "Active", "Mei|WaxArmor", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "+5 [Prt] for 3[Turns]. [Target].\n+50 Shield.\n\n\n\nFree Action.\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Creates temporary armor from wax on an ally.\nIncreases [Prt](Protection) for 3 turns.\nAdds 50 points of Shield.\n\nFree action.\nCosts [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(20, giNoCPCost, 1, "Target Allies Single", giNoCPGeneration)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Buff")
    zAbiStruct.zExecutionAbiPackage:fnSetAsBuff()
  --zAbiStruct.zExecutionAbiPackage:fnAddAnimation(psAnimationName, piTimerOffset, $pfXOffset, $pfYOffset)
    zAbiStruct.zExecutionAbiPackage:fnAddAnimation("Buff Defense", 0)
  --zAbiStruct.zExecutionAbiPackage:fnSetAsShield(piShieldBase, pfShieldScaleFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsShield(50, 0.0)
    
    -- |[Effect Package]|
  --zAbiStruct:fnAddBuff(psPrototypeName, $psApplyText, $psApplyColorString, $psApplyAnim, $psApplySound)
    zAbiStruct:fnAddBuff("Mei.HiveScout.WaxArmor")

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName

    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
        local saDescription = {}
        saDescription[1] = "Quickly molded armor made from beeswax will "
        saDescription[2] = "stop a few hits. Increases [Prt](Protection)."
      --EffectList:fnCreateBuffPrototype(psName, psDisplayName, psIcon, piTurns, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreateBuffPrototype(sLocalPrototypeName, "Wax Armor", "Mei|WaxArmor", 3, "Prt|Flat|5", saDescription)
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    --zAbiStruct.zExecutionAbiPackage.sVoiceData = "Mei|Ability:Whirl|General:Swing|General:Bleed|Offense"

    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.HiveScout.WaxArmor = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.HiveScout.WaxArmor

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
