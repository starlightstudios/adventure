-- |[ ====================================== Scout's Honor ===================================== ]|
-- |[Description]|
--Chance to counter-attack an enemy that targets any ally, once per turn.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.HiveScout.ScoutsHonor == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Hive Scout", "Scouts Honor", "Scout's Honor", gciJP_Cost_Passive, gbIsNotFreeAction, "Direct", "Passive", "Mei|ScoutsHonor", "Counterattack")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "15%% chance to attack an enemy that targets an ally.\nAttack lands before enemy attack begins.\nOnly activates once per turn.\nAttack deals [1.0x [Atk]] as weapon-damage.\n\n\nPassive."
    zAbiStruct.sSimpleDescMarkdown  = "Gives you a chance to counter an enemy who\ntargets an ally.\nYou strike before their attack.\n\n\nPassive."

    -- |[Passive Effect]|
    zAbiStruct.bNeverAvailable   = true --Passive abilities have no usability requirements.
    zAbiStruct.sPassivePrototype = "Mei.HiveScout.ScoutsHonor"
    zAbiStruct.bHasGUIEffects    = true

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    --Execution is used when counterattacking.
    zAbiStruct:fnCreateAbiPack("Weapon")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_UseWeapon, 5, 1.00)
    
    --Special: Counterattacks cause a black flash to appear.
    zAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    if(EffectList:fnEntryExists(zAbiStruct.sPassivePrototype) == false) then
        local saDescription = {}
        saDescription[1] = "15%% chance to counterattack an enemy before "
        saDescription[2] = "they attack your allies."
        saDescription[3] = "Benefits from other counterattack skills."
      --EffectList:fnCreatePassivePrototype(psName, psDisplayName, psIcon, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreatePassivePrototype(zAbiStruct.sPassivePrototype, "Scout's Honor", "Mei|ScoutsHonor", "", saDescription)

        --Manually specify description.
        local iType, zPrototype, iSlot = EffectList:fnLocateEntry(zAbiStruct.sPassivePrototype)
        if(zPrototype ~= nil) then
            zPrototype.bDontRebuildDescription = true
            zPrototype.saStrings[1] = "[Buff]Scout's Honor"
            zPrototype.saStrings[2] = "Counterattack"
            zPrototype.saStrings[3] = saDescription[1]
            zPrototype.saStrings[4] = saDescription[2]
            zPrototype.saStrings[5] = saDescription[3]
            zPrototype.sShortText = ""
        end
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.HiveScout.ScoutsHonor = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.HiveScout.ScoutsHonor

-- |[ ================================== Combat: Combat Begins ================================= ]|
--This ability needs to store its target ID and create a variable marking whether or not it has
-- executed this turn.
if(iSwitchType == gciAbility_BeginCombat) then
    local iUniqueID = RO_GetID()
    DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "/")
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iExecThisTurn", "N", 0.0)
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iTargetID", "N", 0.0)

    --Get the owner for their ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOriginatorID = RO_GetID()
    DL_PopActiveObject()
    
    --Spawn the effect.
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 0, "Effect|" .. gsGlobalEffectPrototypePath .. "|Originator:" .. iOriginatorID .. "|Prototype:" .. gzRefAbility.sPassivePrototype)
    
-- |[ =================================== Combat: Turn Begins ================================== ]|
--When the turn begins, reset the execution variable.
elseif(iSwitchType == gciAbility_BeginTurn) then
    local iUniqueID = RO_GetID()
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iExecThisTurn", "N", 0.0)
    
-- |[ ============================= Combat: Paint Targets Response ============================= ]|
--Marks the attacks for targeting.
elseif(iSwitchType == gciAbility_PaintTargetsResponse) then
    local iUniqueID = RO_GetID()
    local iTargetID = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iTargetID", "N")
    AdvCombat_SetProperty("Create Target Cluster", "Precog Cluster", "Attacker")
    AdvCombat_SetProperty("Add Target To Cluster", "Precog Cluster", iTargetID)

-- |[ =================================== Combat: Can Execute ================================== ]|
--Flags whether or not the event can actually run. If the originator got KOd by a DoT or a counterattack
-- or any other event, this will mark as false, and the event will stop here.
--This is only called for the ability in question.
elseif(iSwitchType == gciAbility_QueryCanRun) then
    AdvCombat_SetProperty("Push Event Originator")
        local bCanAct = AdvCombatEntity_GetProperty("Can Act")
    DL_PopActiveObject()
    if(bCanAct) then AdvCombat_SetProperty("Set Event Can Run", true) end
    
-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[ ================ Originator Statistics ================= ]|
    --Get originator.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID    = RO_GetID()
    DL_PopActiveObject()
    gzRefAbility.zExecutionAbiPackage.iOriginatorID = iOriginatorID
    
    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them. Call the standard
    -- execution subroutine.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        fnStandardExecution(iOriginatorID, i-1, gzRefAbility.zExecutionAbiPackage, nil)
    end
    
-- |[ ================================== Combat: Event Queued ================================== ]|
--Fired whenever a "main" event is queued, which is when the player or an AI decides which action to
-- perform. All abilities equipped by all entities then fire a response script. They can then enqueue
-- their own events before or after the main one.
--This occurs before the ability calls with gciAbility_Execute.
elseif(iSwitchType == gciAbility_EventQueued) then

    --Check if the ability already executed this turn:
    local iUniqueID = RO_GetID()
    local iExecThisTurn = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iExecThisTurn", "N")
    if(iExecThisTurn == 1.0) then
        return
    end
    
    --Get the ID of the owner of this ability.
    local iOwnerID = AdvCombatAbility_GetProperty("Owner ID")
    local iOwnerGroup = AdvCombat_GetProperty("Party Of ID", iOwnerID)
    
    --Get the originator, and check if it's a hostile entity.
    local iOriginatorID = AdvCombat_GetProperty("Query Originator ID")
    local iOriginatorGroup = AdvCombat_GetProperty("Party Of ID", iOriginatorID)
    
    --To be valid, the owner must be in a different party than the originator, and both must be in either
    -- the player's active party or the enemy's active party.
    if(iOwnerGroup == iOriginatorGroup) then
        return
    end
    if(iOwnerGroup ~= gciACPartyGroup_Party and iOwnerGroup ~= gciACPartyGroup_Enemy) then
        return
    end
    if(iOriginatorGroup ~= gciACPartyGroup_Party and iOriginatorGroup ~= gciACPartyGroup_Enemy) then
        return
    end
    
    --Check the target cluster. If a party member who is not the owner is found, we can counterattack.
    local bIsInTargetCluster = false
    local iTargetsTotal = AdvCombat_GetProperty("Query Targets Total")
    for i = 1, iTargetsTotal, 1 do
        local iTargetID = AdvCombat_GetProperty("Query Targets ID", i-1)
        local iTargetParty = AdvCombat_GetProperty("Party Of ID", iTargetID)
        
        --Same as owner: Skip.
        if(iTargetID == iOwnerID) then
        
        --Same party as owner: Can counterattack:
        elseif(iTargetParty == iOwnerGroup) then
            bIsInTargetCluster = true
        end
    end
    
    if(bIsInTargetCluster == false) then
        return
    end
    
    --Lastly, roll. This ability has a 15% chance to fire.
    local iRoll = LM_GetRandomNumber(1, 100)
    
    --Tags that modify counterattack chance.
    AdvCombat_SetProperty("Push Entity By ID", iOwnerID)
        local iAdditive    = AdvCombatEntity_GetProperty("Tag Count", "Counterattack +")
        local iSubtractive = AdvCombatEntity_GetProperty("Tag Count", "Counterattack -")
    DL_PopActiveObject()
    
    --The additive tags subtract from the roll since lower is better.
    iRoll = iRoll - iAdditive + iSubtractive
    
    --Final check.
    if(false) then
        iRoll = 0
    end
    if(iRoll <= 15) then
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iExecThisTurn", "N", 1.0)
        VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iTargetID", "N", iOriginatorID)
        AdvCombatAbility_SetProperty("Enqueue This Ability As Event", -1)
    end
    
-- |[ ===================================== All Other Cases ==================================== ]|
--Standard handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
