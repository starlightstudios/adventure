-- |[ ====================================== Call For Help ===================================== ]|
-- |[Description]|
--Calls a Bee ally for the rest of the battle! Costs CP.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Ability Prototype =================================== ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.HiveScout.CallForHelp == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Hive Scout", "Call For Help", "Use Skill Name", gciJP_Cost_Normal, gbIsFreeAction, "Buff", "Combo", "Mei|CallForHelp", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Calls a friendly bee girl to help!\nUsable once per battle.\n\n\n\nFree Action.\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Calls a friendly bee girl to help!\nCan be used once per battle.\nFree Action.\n\n\nCosts [CPCost][CPIco](CP)."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, 3, giNoCooldown, "Target Self", giNoCPGeneration)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Buff")
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Mei|Ability:CallForHelp"

    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.HiveScout.CallForHelp = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.HiveScout.CallForHelp
        
-- |[ ==================================== Creation Handler ==================================== ]|
--Called when the ability is created and added to the job prototype. This shows the ability's name
-- and icon, to inform the player what abilities are on the job.
if(iSwitchType == gciAbility_Create or iSwitchType == gciAbility_CreateSpecial) then
    
    -- |[Call Base]|
    --Check for second argument. If found, pass it along.
    local sAbilityNameOverride = "Null"
    if(iArgumentsTotal >= 2) then
        sAbilityNameOverride = LM_GetScriptArgument(1)
        LM_ExecuteScript(gsStandardAbilityPath, iSwitchType, sAbilityNameOverride)
    
    --Normal case:
    else
        LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
    end
    
    -- |[Special Call Case]|
    --Reset gzRefAbility pointer (it gets cleared by the standard)
    gzRefAbility = gzPrototypes.Combat.Mei.HiveScout.CallForHelp
    
    --This ability is also run at combat start. Re-resolve the name and push the ability so we can add the response flag.
    local sUseJobName = gzRefAbility.sJobName
    if(iSwitchType == gciAbility_CreateSpecial) then sUseJobName = gzRefAbility.sInternalName end

    --Resolve ability name.
    local sAbilityName = sUseJobName .. "|" .. gzRefAbility.sSkillName
    if(sAbilityNameOverride ~= "Null") then sAbilityName = sAbilityNameOverride end
    
    --Add flag.
    AdvCombatEntity_SetProperty("Push Ability S", sAbilityName)
        AdvCombatAbility_SetProperty("Script Response", gciAbility_BeginCombat, true)
    DL_PopActiveObject()
    
    --Clean.
    gzRefAbility = nil

-- |[ ================================== Combat: Combat Begins ================================= ]|
--This ability may only be used once per battle.
elseif(iSwitchType == gciAbility_BeginCombat) then
    local iUniqueID = RO_GetID()
    DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "/")
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iHasUsed", "N", 0.0)
    
-- |[ ============================= Combat: Character Action Begins ============================ ]|
--Resolve whether this ability can be used. In chapter 1 during the rubber sequence, it cannot.
elseif(iSwitchType == gciAbility_BeginAction) then

    -- |[Call Standard]|
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
    
    -- |[Once Per Battle]|
    --If this flag is set, this ability cannot be used.
    local iUniqueID = RO_GetID()
    local iHasUsed = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iHasUsed", "N")
    if(iHasUsed == 1.0) then
        AdvCombatAbility_SetProperty("Usable", false)
        return
    end

    -- |[Party Size Cap]|
    --A max of 6 characters may be in the combat party for any reason.
    if(AdvCombat_GetProperty("Combat Party Size") >= 6) then
        AdvCombatAbility_SetProperty("Usable", false)
        return
    end
    
-- |[ ============================== Combat: Character Free Action ============================= ]|
--Cannot be used after a free action.
elseif(iSwitchType == gciAbility_BeginFreeAction) then
    AdvCombatAbility_SetProperty("Usable", false)
    
-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--Creates a specific prediction with a string on it.
elseif(iSwitchType == gciAbility_BuildPredictionBox) then
    
    --Get originator ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
    DL_PopActiveObject()
    
    --Box.
    local sBoxName = "Prd0x" .. iOwnerID
    AdvCombat_SetProperty("Prediction Create Box",              sBoxName)
    AdvCombat_SetProperty("Prediction Set Host",                sBoxName, iOwnerID)
    AdvCombat_SetProperty("Prediction Set Offsets",             sBoxName, 0, -100)
    AdvCombat_SetProperty("Prediction Allocate Strings",        sBoxName, 1)
    AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, 0, "Summon a bee ally to help you!")
    
-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[ ================ Originator Statistics ================= ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        fnModifyCP(-4)
    DL_PopActiveObject()
    
    --Mark the ability as used.
    local iAbilityID = RO_GetID()
    VM_SetVar("Root/Variables/Combat/" .. iAbilityID .. "/iHasUsed", "N", 1.0)
    
    -- |[ =================== Ability Package ==================== ]|
    --Apply cooldown for this ability. Mark the action as free.
    --AdvCombatAbility_SetProperty("Cooldown", 1)
    AdvCombat_SetProperty("Set As Free Action")
    
    --Run the script to create the allied bee. The value gsLastEnemyName will be populated with the unique name.
    local iIsRubberMode = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
    if(iIsRubberMode == 1.0) then
      LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/007 Rubber Wilds/Rubberbee.lua")
    else
      LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/100 Allies/Ally Bee.lua")
    end
    if(giLastEnemyID == nil) then return end
    
    --Begin
    local iTimer = 0
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, giLastEnemyID, iTimer, "Change Party|" .. gciACPartyGroup_Party)
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, giLastEnemyID, iTimer, "Position|" .. gciACPoscode_Party)
    iTimer = iTimer + gciAC_StandardMoveTicks
    
    --If Rubber, change skills.
    if(iIsRubberMode == 1.0) then
      AdvCombat_SetProperty("Push Entity By ID", giLastEnemyID)
      
          --Clear AI and existing abilities.
          AdvCombatEntity_SetProperty("AI Script", "Null")
          fnClearAllSlots()
          
          --Provide abilities. These are identical to Mei's.
          LM_ExecuteScript(gsRoot .. "Combat/Party/Mei/Jobs/Squeaky Thrall/Abilities/Spread Rubber.lua", gciAbility_Create)
          LM_ExecuteScript(gsRoot .. "Combat/Party/Mei/Jobs/Squeaky Thrall/Abilities/Permagrin.lua",     gciAbility_Create)
          
          --Place abilities.
          AdvCombatEntity_SetProperty("Set Ability Slot", 0, 0, "Squeaky Thrall|Spread Rubber")
          AdvCombatEntity_SetProperty("Set Ability Slot", 1, 0, "Squeaky Thrall|Permagrin")
      DL_PopActiveObject()
    end
    
    --Dialogue.
    local sScriptPath = LM_GetCallStack(0)
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, giLastEnemyID, iTimer, "Run Script|" .. sScriptPath .. "|N:-1000")
    iTimer = iTimer + 15
    
    --End
    AdvCombat_SetProperty("Event Timer", iTimer)
    
    --Flag reset.
    giTargetID = giLastEnemyID
    giLastEnemyID = nil
            
-- |[ ======================================== Cutscene ======================================== ]|
--Special
elseif(iSwitchType == -1000) then

    -- |[ ====================== Error Check ===================== ]|
    --Make sure the target exists.
    if(giTargetID == nil) then return end
    
    -- |[ =================== Cutscene Handling ================== ]|
    local iIsRubberMode = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
    
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetCombatPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Bee", "Neutral") ]])
    
    --If Rubber mode.
    if(iIsRubberMode == 1.0) then
      fnCutscene([[ Append("Bee:[E|Neutral] Bzzz bzz?[P] Bzzzz![B][C]") ]])
      fnCutscene([[ Append("Mei:[E|Neutral] *Squeak*[P] (Spread...)[B][C]") ]])
      fnCutscene([[ Append("Bee:[E|Neutral] [SOUND|Rubber|ChangeA]Zzzzz!") ]])
      fnCutsceneBlocker()
      
      
      fnCutscene([[ WD_SetProperty("Show") ]])
      fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
      LM_ExecuteScript(gzFunctionPaths.sSetCombatPartyDialogue)
      fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "RubberBee", "Neutral") ]])
      fnCutscene([[ Append("Bee:[E|Neutral] [SOUND|Rubber|ChangeC]*Squeak*[P] (Spread...)[B][C]") ]])
      fnCutscene([[ Append("Mei:[E|Neutral] (Spread...)") ]])
      
    --Normal.
    else
    --If Mei is a bee:
      local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
      if(sMeiForm == "Bee") then
          fnCutscene([[ Append("Bee:[E|Neutral] Bzzz bzz? (You called?)[B][C]") ]])
          fnCutscene([[ Append("Mei:[E|Neutral] Bzzz! BzzZzzZZz! (Yes! Thank you for coming, please help me!)[B][C]") ]])
          fnCutscene([[ Append("Bee:[E|Neutral] zzzzZZ! (For the hive!)") ]])
      else
          fnCutscene([[ Append("Bee:[E|Neutral] Bzzz bzz?[B][C]") ]])
          fnCutscene([[ Append("Mei:[E|Neutral] Uhhhh... bzzz! Bbzz? I think I said that wrong...[B][C]") ]])
          fnCutscene([[ Append("Bee:[E|Neutral] zzzzZZ!") ]])
      end
    end
    fnCutsceneBlocker()
    fnCutscene([[ AdvCombat_SetProperty("Position Party Normally") ]])
    fnCutsceneBlocker()
    
    --Clean up.
    giTargetID = nil
    
-- |[ ======================================== Standard ======================================== ]|
--All other calls follow the standard.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
