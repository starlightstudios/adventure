-- |[ ========================================= Fencer ========================================= ]|
--Mei's starting class in chapter 1. Armed with a light katana sword, this is also her human job.
-- Therefore, she has common human resistances.
--Notably, Mei is always immune to terrify damage and effects.

-- |[ ========== Argument Handling =========== ]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--1st argument is only used for level-up calls.
local iLevelReached = 0
if(iArgumentsTotal >= 2) then
    iLevelReached = LM_GetScriptArgument(1, "N")
end

-- |[ ============ Variable Setup ============ ]|
-- |[Enumerations]|
local gciNoSkillbooks = 0

-- |[Encapsulation]|
local zEntry = {}

-- |[Basic Variables]|
--Character Variables.
zEntry.sCharacterFormVarPath = "Root/Variables/Global/Mei/sForm"
zEntry.sSkillbookVarPath     = "Root/Variables/Global/Mei/iSkillbookTotal"
zEntry.zaJobChart            = gzaMeiJobChart
zEntry.sStatProfilePath      = gsRoot .. "Combat/Party/Mei/400 Common Stat Profile.lua"
zEntry.sSecondaryMenuPath    = gsRoot .. "Combat/Party/Mei/200 Build Secondary Menu.lua"
zEntry.sMasterBonusPath      = gsRoot .. "Combat/Party/Mei/401 Mastery Bonus Handler.lua"

--Class Variables.
zEntry.sClassName   = "Fencer"
zEntry.sFormName    = "Human"
zEntry.sCostumeName = "Mei_Human"
zEntry.fFaceIndexX  = 0
zEntry.fFaceIndexY  = 0

-- |[Ability Variables]|
--Purchaseable abilities.
zEntry.saExternalAbilities = {}
table.insert(zEntry.saExternalAbilities, "Rend")
table.insert(zEntry.saExternalAbilities, "Blind")
table.insert(zEntry.saExternalAbilities, "Quick Strike")
table.insert(zEntry.saExternalAbilities, "Pommel Bash")
table.insert(zEntry.saExternalAbilities, "Taunt")
table.insert(zEntry.saExternalAbilities, "Trip")
table.insert(zEntry.saExternalAbilities, "Whirl")
table.insert(zEntry.saExternalAbilities, "Concentrate")
table.insert(zEntry.saExternalAbilities, "Fast Reflexes")
table.insert(zEntry.saExternalAbilities, "Precognition")
table.insert(zEntry.saExternalAbilities, "Way Of The Blade")
table.insert(zEntry.saExternalAbilities, "Powerful Strike")
table.insert(zEntry.saExternalAbilities, "Blade Dance")
    
--Class-equipped abilities.
zEntry.saInternalAbilities = {}
table.insert(zEntry.saInternalAbilities, "Rend")
table.insert(zEntry.saInternalAbilities, "Blind")
table.insert(zEntry.saInternalAbilities, "Quick Strike")
table.insert(zEntry.saInternalAbilities, "Taunt")
table.insert(zEntry.saInternalAbilities, "Blade Dance")
table.insert(zEntry.saInternalAbilities, "Precognition")
table.insert(zEntry.saInternalAbilities, "Powerful Strike")

--Purchaseable abilities, in the order of purchase.
zEntry.saPurchaseAbilities = {}
table.insert(zEntry.saPurchaseAbilities, "Fencer|Quick Strike")
table.insert(zEntry.saPurchaseAbilities, "Fencer|Pommel Bash")
table.insert(zEntry.saPurchaseAbilities, "Fencer|Rend")
table.insert(zEntry.saPurchaseAbilities, "Fencer|Whirl")
table.insert(zEntry.saPurchaseAbilities, "Fencer|Blind")
table.insert(zEntry.saPurchaseAbilities, "Fencer|Taunt")
table.insert(zEntry.saPurchaseAbilities, "Fencer|Trip")
table.insert(zEntry.saPurchaseAbilities, "Fencer|Powerful Strike")
table.insert(zEntry.saPurchaseAbilities, "Fencer|Blade Dance")
table.insert(zEntry.saPurchaseAbilities, "Fencer|Concentrate")
table.insert(zEntry.saPurchaseAbilities, "Fencer|Fast Reflexes")
table.insert(zEntry.saPurchaseAbilities, "Fencer|Precognition")
table.insert(zEntry.saPurchaseAbilities, "Fencer|Way Of The Blade")

-- |[Skillbar Variables]|
--Skills that always appear on the skillbar. Format: {sAbilityName, iXPos, iYPos, iSkillbooks} (eg: {"Common|Attack", 0, 0, 0}, {"Common|Parry", 1, 0, 0} {})
zEntry.zaSkillbarAbilities = {}
table.insert(zEntry.zaSkillbarAbilities, {"Common|Attack",                    0, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Common|Parry",                     1, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Fencer Internal|Powerful Strike",  2, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Fencer Internal|Rend",             0, 1, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Fencer Internal|Blind",            1, 1, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Fencer Internal|Quick Strike",     2, 1, 1})
table.insert(zEntry.zaSkillbarAbilities, {"Fencer Internal|Taunt",            0, 2, 2})
table.insert(zEntry.zaSkillbarAbilities, {"Fencer Internal|Blade Dance",      1, 2, 3})
table.insert(zEntry.zaSkillbarAbilities, {"Fencer Internal|Precognition",     2, 2, 4})

-- |[ ============ Call Standard ============= ]|
--Add to the global stack. There may be multiple executions of this script.
table.insert(gzJobCallStack, zEntry)

--Call.
LM_ExecuteScript(gsStandardJobPath, iSwitchType, iLevelReached)

--Pop the global stack.
table.remove(gzJobCallStack)
