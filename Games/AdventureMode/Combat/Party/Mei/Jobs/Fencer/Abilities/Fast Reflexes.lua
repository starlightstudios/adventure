-- |[ ====================================== Fast Reflexes ===================================== ]|
-- |[Description]|
--Passive, increases Mei's evade and attack power when equipped.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.Fencer.FastReflexes == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Fencer", "Fast Reflexes", "$SkillName", gciJP_Cost_Passive, gbIsNotFreeAction, "Buff", "Passive", "Mei|Fencer|FastReflexes", "Passive")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "+15[Evd] +20[Atk].\n\n\n\n\n\nPassive."
    zAbiStruct.sSimpleDescMarkdown  = "Increases [Evd](Evade) and [Atk](Power).\n\n\n\n\nPassive."
    
    -- |[Passive Effect]|
    zAbiStruct.bNeverAvailable   = true --Passive abilities have no usability requirements.
    zAbiStruct.sPassivePrototype = "Mei.Fencer.FastReflexes"
    zAbiStruct.bHasGUIEffects    = true

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    if(EffectList:fnEntryExists(zAbiStruct.sPassivePrototype) == false) then
        local saDescription = {}
        saDescription[1] = "Quick reflexes increase your chance to dodge, and attack power."
        saDescription[2] = "Increases [Evd](Evade) and [Atk](Power)"
      --EffectList:fnCreatePassivePrototype(psName, psDisplayName, psIcon, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreatePassivePrototype(zAbiStruct.sPassivePrototype, "Fast Reflexes", "Mei|Fencer|FastReflexes", "Evd|Flat|15 Atk|Flat|20", saDescription)
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.Fencer.FastReflexes = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.Fencer.FastReflexes

-- |[ ===================================== All Other Cases ==================================== ]|
if(iSwitchType ~= gciAbility_GUIApplyEffect) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

-- |[ ==================================== GUI: Apply Effect =================================== ]|
--When showing a character's statistics, all equipped abilities are queried. This is not reflected
-- in combat, this is GUI only.
--The standard does not handle this.
elseif(iSwitchType == gciAbility_GUIApplyEffect) then
    AdvCombatAbility_SetProperty("Push Owner")
        fnApplyStatBonus(gciStatIndex_Evade, 15)
        fnApplyStatBonus(gciStatIndex_Attack, 20)
    DL_PopActiveObject()
    gzRefAbility = nil
end
