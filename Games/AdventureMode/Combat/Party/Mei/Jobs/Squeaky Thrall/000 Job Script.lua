-- |[ ===================================== Squeaky Thrall ===================================== ]|
--Mei's special rubber class.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ====================================== Job Creation ====================================== ]|
--Called when the chapter is initialized, the job registers itself to its owner and populates its
-- abilities. This is only called once.
--The owner should be the active object.
if(iSwitchType == gciJob_Create) then
    
    --Create the job and set its stats.
    AdvCombatEntity_SetProperty("Create Job", "Squeaky Thrall")
    
        --Name as it appears in the UI.
        AdvCombatJob_SetProperty("Display Name", "Squeaky Thrall")
        AdvCombatJob_SetProperty("Appears on Skills UI", false)
    
        --Script path to this job
        AdvCombatJob_SetProperty("Script Path", LM_GetCallStack(0))
        
        --Responses.
        AdvCombatJob_SetProperty("Script Response", gciJob_SwitchTo, true)
        AdvCombatJob_SetProperty("Script Response", gciJob_Level, true)
        AdvCombatJob_SetProperty("Script Response", gciJob_Master, true)
        AdvCombatJob_SetProperty("Script Response", gciJob_JobAssembleSkillList, true)
        AdvCombatJob_SetProperty("Script Response", gciJob_BeginCombat, true)
        
        --Display
        local fIndexX = 7
        local fIndexY = 0
        local fLft = (fIndexX+0) * gci_FaceTable_Size
        local fTop = (fIndexY+0) * gci_FaceTable_Size
        local fRgt = (fIndexX+1) * gci_FaceTable_Size
        local fBot = (fIndexY+1) * gci_FaceTable_Size
        AdvCombatJob_SetProperty("Face Table Data", fLft, fTop, fRgt, fBot, "Root/Images/AdventureUI/ItemIcons/CharacterFaces")
        
    DL_PopActiveObject()
    
    --Register the job's abilities to the entity. Abilities with "JOBNAME|ABILITYNAME" are abilities
    -- used by the job itself, while "UNLOCK-JOBNAME|ABILITYNAME" are those the player can purchase.
    --Even if two such abilities share the same name, they don't have to be the same ability.
    local sJobAbilityPath = fnResolvePath() .. "Abilities/"
    LM_ExecuteScript(sJobAbilityPath .. "Spread Rubber.lua",  gciAbility_Create)
    LM_ExecuteScript(sJobAbilityPath .. "Super Spread.lua",   gciAbility_Create)
    LM_ExecuteScript(sJobAbilityPath .. "Permagrin.lua",      gciAbility_Create)
    LM_ExecuteScript(sJobAbilityPath .. "Summon Thralls.lua", gciAbility_Create)
   
-- |[ ===================================== Job Assumption ===================================== ]|
--Called when the player switches to the class. Clears any existing job properties, sets graphics
-- and stats as needed.
--This can be called during combat, as some characters can switch jobs in-battle.
--The calling AdvCombatEntity is expected to be the active object.
elseif(iSwitchType == gciJob_SwitchTo) then

    --Change images.
    VM_SetVar("Root/Variables/Global/Mei/sForm", "S", "Rubber")
    LM_ExecuteScript(gsCostumeAutoresolve, "Mei_Rubber")
    
    --Push the owning entity.
    AdvCombatJob_SetProperty("Push Owner")

        -- |[Statistics]|
        --Get health percentage.
        local fHPPercent = AdvCombatEntity_GetProperty("Health") / AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
        
        --Set job statistics.
        AdvCombatEntity_SetProperty("Compute Level Statistics", -1)
        
        --Apply change in max HP.
        AdvCombatEntity_SetProperty("Health Percent", fHPPercent)
        
        --Clear ability slots.
        fnClearAllSlots()
        
        -- |[Top Bar]|
        --Set basic actions bar.
        AdvCombatEntity_SetProperty("Set Ability Slot", 0, 0, "Squeaky Thrall|Spread Rubber")
        AdvCombatEntity_SetProperty("Set Ability Slot", 1, 0, "Squeaky Thrall|Permagrin")
        AdvCombatEntity_SetProperty("Set Ability Slot", 2, 0, "Squeaky Thrall|Super Spread")
        
        --Other. This form can't retreat but can surrender.
        AdvCombatEntity_SetProperty("Set Ability Slot", gciAbility_Tactics_Retreat_X,   gciAbility_Tactics_Retreat_Y,   "Null")
        AdvCombatEntity_SetProperty("Set Ability Slot", gciAbility_Tactics_Surrender_X, gciAbility_Tactics_Surrender_Y, "System|Surrender")
        
        -- |[Thralls Summoner]|
        local iHasThralls = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iHasThralls", "N")
        if(iHasThralls == 1.0) then
            AdvCombatEntity_SetProperty("Set Ability Slot", 0, 1, "Squeaky Thrall|Summon Thralls")
        end

    DL_PopActiveObject()

-- |[ ====================================== Job Level Up ====================================== ]|
--Called when the job reaches a level. The second argument is the level in question. Remember that
-- level 1 on the UI is level 0 in the scripts.
--When assuming the job, this is called by the program to reset stats. There is no need to call it
-- during the Job Assumption part of this script.
elseif(iSwitchType == gciJob_Level) then

    -- |[Setup]|
    --Argument check.
    if(iArgumentsTotal < 2) then 
        Debug_ForcePrint("Error: No level specified for Job Level Up. " .. LM_GetCallStack(0) .. "\n") 
        return
    end

    --First argument is what level we reached.
    local iLevelReached = LM_GetScriptArgument(1, "N")
    
    --Common script.
    fnChartJobStatHandler(gzaMeiJobChart, "Squeaky Thrall", iLevelReached, gsRoot .. "Combat/Party/Mei/400 Common Stat Profile.lua")
    
-- |[ ====================================== Job Mastered ====================================== ]|
--Called when the player masters the class and gets the mastery bonus applied. Also called when the
-- character changes class and has the mastery bonus applied to that class.
elseif(iSwitchType == gciJob_Master) then
    
-- |[ =================================== Assemble Skill List ================================== ]|
--Once all abilities are registered to the parent entity, builds a list of which skills are associated
-- with this job. These skills show up on the UI and can be purchased by the player. Note that a job
-- can contain the same ability as another job. Purchasing it in either will unlock both versions.
-- This is not explicitly a problem, it means the same ability can be bought with JP from multiple
-- jobs. It is not part of the standard for Adventure Mode though.
elseif(iSwitchType == gciJob_JobAssembleSkillList) then

-- |[ ====================================== Combat: Begin ===================================== ]|
--Called when combat begins. This is called after entity and before Ability update calls, and before
-- turn-order resolution.
elseif(iSwitchType == gciJob_BeginCombat) then
    
-- |[ ==================================== Combat: New Turn ==================================== ]|
--Called when turn order is sorted and a new turn begins.
elseif(iSwitchType == gciJob_BeginTurn) then

-- |[ =================================== Combat: New Action =================================== ]|
--Called when an action begins. Note that the action is not necessarily that of the active entity.
elseif(iSwitchType == gciJob_BeginAction) then
    
-- |[ ========================= Combat: New Action (After Free Action) ========================= ]|
--Called when an action begins after a Free Action has expired.
elseif(iSwitchType == gciJob_BeginFreeAction) then

-- |[ =================================== Combat: End Action =================================== ]|
--Called when an action ends. As above, not necessarily that of the active entity.
elseif(iSwitchType == gciJob_EndAction) then

-- |[ ==================================== Combat: End Turn ==================================== ]|
--Called when a turn ends, before turn order is determined for the next turn.
elseif(iSwitchType == gciJob_EndTurn) then

-- |[ =================================== Combat: End Combat =================================== ]|
--Called when combat ends for any reason. This includes, victory, defeat, retreat, or other.
elseif(iSwitchType == gciJob_EndCombat) then

-- |[ ================================== Combat: Event Queued ================================== ]|
--Called when a main event is enqueued. An entity typically performs one event per action. This is
-- called before after Entity but before Abilities respond for this character.
elseif(iSwitchType == gciJob_EventQueued) then
    
end
