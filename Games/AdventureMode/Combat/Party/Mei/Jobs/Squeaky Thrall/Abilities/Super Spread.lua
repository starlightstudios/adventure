-- |[ ====================================== Super Spread ====================================== ]|
-- |[Description]|
--Applies a rubber effect twice to all enemies. Does not work on certain targets.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.SqueakyThrall.SuperSpread == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Squeaky Thrall", "Super Spread", "Use Skill Name", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Combo", "Mei|SpreadRubber", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Spreads twice as much rubber to all enemies.\nDoes no damage.\nDoes not work on some enemies.\n\n\n\nCosts [CPCost][CPIco]CP."
    zAbiStruct.sSimpleDescMarkdown  = "Spreads twice as much rubber to all enemies.\nDoes no damage.\nDoes not work on some enemies.\n\n\nCosts [CPCost][CPIco]CP."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, 1, giNoCooldown, "Target Enemies All", giNoCPGeneration)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Pierce")
    zAbiStruct.zExecutionAbiPackage.bDoesNoDamage = true
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun   = true
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits   = true
    zAbiStruct.zExecutionAbiPackage.bNeverCrits   = true
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.SqueakyThrall.SuperSpread = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.SqueakyThrall.SuperSpread

-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--Custom prediction box based on if the target can be Rubbered. Thanks to fnTrimTargetsWithoutTag in Paint Targets, the target must already be Rubberable.
if(iSwitchType == gciAbility_BuildPredictionBox) then
    
    -- |[ ===================== Other Values ===================== ]|
    --Get originator ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
    DL_PopActiveObject()
    
    --Get information about the cluster. This is needed to give each box a unique ID.
    local iClusterID = AdvCombat_GetProperty("Prediction Get Cluster ID")
    
    -- |[ ==================== For Each Target =================== ]|
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    for i = 0, iClusterTargetsTotal-1, 1 do
        
        --Target ID.
        local iTargetID = AdvCombat_GetProperty("Prediction Get Targets ID", i)
        
        --Get how many times Rubber tags appear on the entity.
        AdvCombat_SetProperty("Push Entity By ID", iTargetID)
            local iRubberifiedStacks = AdvCombatEntity_GetProperty("Tag Count", "Rubberified")
        DL_PopActiveObject()
        
        --Not enough Rubber.
          if(iRubberifiedStacks < 3) then
            gzRefAbility.zPredictionPackage.zaAdditionalLines[1] = {}
            gzRefAbility.zPredictionPackage.zaAdditionalLines[1].sString = "Apply Rubber!"
            gzRefAbility.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
            
          --Too much Rubber.
          else
            gzRefAbility.zPredictionPackage.zaAdditionalLines[1] = {}
            gzRefAbility.zPredictionPackage.zaAdditionalLines[1].sString = "Already covered in Rubber!"
            gzRefAbility.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
          end
          
        --Build the box!
        fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, gzRefAbility.zPredictionPackage)
    end

-- |[ ================================= Combat: Paint Targets ================================== ]|
--For player-controlled characters, selects which targets can be used by this ability. AI-controlled
-- characters may optionally bypass this and select targets directly.
--If no targets are painted, the UI will play a fail sound and disable target selection.
elseif(iSwitchType == gciAbility_PaintTargets) then
    
    --Run macro.
    AdvCombat_SetProperty("Run Target Macro", gzRefAbility.sTargetMacro, AdvCombatAbility_GetProperty("Owner ID"))

    --Scan all clusters for "Is Rubberable" tag. Remove all targets/clusters that don't have it.
    fnTrimTargetsWithoutTag({"Is Rubberable"})
    
    
-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[ ================ Originator Statistics ================= ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        fnModifyCP(gzRefAbility.iCPGain - gzRefAbility.iRequiredCP)
    DL_PopActiveObject()
    
    
    -- |[ ====================== Provide IDs ===================== ]|
    --Provide IDs to all packages.
    gzRefAbility.zExecutionAbiPackage.iOriginatorID = iOriginatorID
    
    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        
        --Target Variables:
        AdvCombat_SetProperty("Push Target", i-1) 
            local iTargetUniqueID = RO_GetID()
            local iRubberifiedStacks = AdvCombatEntity_GetProperty("Tag Count", "Rubberified")
        DL_PopActiveObject()
    
        --Setup
        local iTimer = (gciAbility_TicksOffsetPerTarget * (i-1))
        
        --Sound effect:
        local sSound = "Play Sound|Rubber\\|ChangeA"
        local iRoll = LM_GetRandomNumber(1, 3)
        if(iRoll == 2) then 
            sSound = "Play Sound|Rubber\\|ChangeB"
        elseif(iRoll == 3) then 
            sSound = "Play Sound|Rubber\\|ChangeC" 
        end
      
        --Common.
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, iTimer, sSound)
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, iTimer, "Create Animation|Shadow B|AttackAnim0")
        iAnimTicks = fnGetAnimationTiming("Debuff")
        iTimer = iTimer + iAnimTicks
        
        -- |[ ================== Can be Rubber'd ================= ]|
        if(iRubberifiedStacks < 3) then
            local sEffectPath = fnResolvePath() .. "../Effects/Rubber.lua"
        
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, iTimer, "Effect|" .. sEffectPath)
            if(iRubberifiedStacks ~= 2) then
              AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, iTimer, "Effect|" .. sEffectPath)
            end
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, iTimer, "Text|Rubber Spreads...|Color:Blue")
            iTimer = iTimer + gciApplication_TextTicks
            
        -- |[ ================= Too much rubber! ================= ]|
        else
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, iTimer, "Text|Already Rubber!|Color:Red")
            iTimer = iTimer + gciApplication_TextTicks
        end
        
        --Finish up.
        fnDefaultEndOfApplications(iTimer)
    end
      
-- |[ ==================================== All Other Cases ===================================== ]|
--Call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
