-- |[ ===================================== Summon Thralls ===================================== ]|
-- |[Description]|
--At the start of battle, summons two rubber allies.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.SqueakyThrall.SummonThralls == nil) then
    
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Squeaky Thrall", "Summon Thralls", "Summon Thralls", gciJP_Cost_Passive, gbIsNotFreeAction, "Buff", "Passive", "Mei|RubberThralls", "Passive")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Two thralls aid you in battle.\n\n\n\n\n\nPassive."
    zAbiStruct.sSimpleDescMarkdown  = "Two thralls aid you in battle.\n\n\n\n\nPassive."
    
    -- |[Passive Effect]|
    zAbiStruct.bNeverAvailable   = true --Passive abilities have no usability requirements.
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.SqueakyThrall.SummonThralls = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.SqueakyThrall.SummonThralls

-- |[ ==================================== Creation Handler ==================================== ]|
--Called when the ability is created and added to the job prototype. This shows the ability's name
-- and icon, to inform the player what abilities are on the job.
if(iSwitchType == gciAbility_Create or iSwitchType == gciAbility_CreateSpecial) then
    
    -- |[Setup]|
    --Special: If the ability has an internal version, the name changes. This is used for abilities
    -- that can both be purchased for JP and exist on the job skill bar by default.
    local sUseJobName = gzRefAbility.sJobName
    if(iSwitchType == gciAbility_CreateSpecial) then sUseJobName = gzRefAbility.sInternalName end
    
    --Special: If a second argument is provided, then the name of the ability is being overrode. This
    -- is used by item creation scripts since items have fixed names.
    local sAbilityNameOverride = "NULL"
    if(iArgumentsTotal >= 2) then
        sAbilityNameOverride = LM_GetScriptArgument(1)
    end
    
    -- |[Creation]|
    local sAbilityName = sUseJobName .. "|" .. gzRefAbility.sSkillName
    if(sAbilityNameOverride ~= "NULL") then sAbilityName = sAbilityNameOverride end
    AdvCombatEntity_SetProperty("Create Ability", sAbilityName)
    
        --System
        AdvCombatAbility_SetProperty("Script Path", LM_GetCallStack(0)) --Use calling script.
        AdvCombatAbility_SetProperty("JP Cost", gzRefAbility.iJPUnlockCost)
    
        --Display
        AdvCombatAbility_SetProperty("Display Name", gzRefAbility.sSkillName)
        AdvCombatAbility_SetProperty("Icon Back",    gzRefAbility.sIconBacking)
        AdvCombatAbility_SetProperty("Icon Frame",   gzRefAbility.sIconFrame)
        AdvCombatAbility_SetProperty("Icon",         gzRefAbility.sIconPath)
        if(gzRefAbility.sCPIcon ~= "Null") then
            AdvCombatAbility_SetProperty("CP Icon", gzRefAbility.sCPIcon)
        end
        
        --Description
        AdvCombatAbility_SetProperty("Description", gzRefAbility.sDescription)
        AdvCombatAbility_SetProperty("Allocate Description Images", #gzRefAbility.saImages)
        for i = 1, #gzRefAbility.saImages, 1 do
            AdvCombatAbility_SetProperty("Description Image", i-1, gcfAbilityImgOffsetY, gzRefAbility.saImages[i])
        end
        AdvCombatAbility_SetProperty("Crossload Description Images")
        
        --Simplified Description
        if(gzRefAbility.sSimpleDesc ~= nil) then
            AdvCombatAbility_SetProperty("Simplified Description", gzRefAbility.sSimpleDesc)
            AdvCombatAbility_SetProperty("Allocate Simplified Description Images", #gzRefAbility.saSimpleImages)
            for i = 1, #gzRefAbility.saSimpleImages, 1 do
                AdvCombatAbility_SetProperty("Simplified Description Image", i-1, gcfAbilityImgOffsetY, gzRefAbility.saSimpleImages[i])
            end
            AdvCombatAbility_SetProperty("Crossload Simplified Description Images")
        end
        
        --Response Flags
        AdvCombatAbility_SetProperty("Script Response", gciAbility_BeginCombat, true)
        
    DL_PopActiveObject()

-- |[ ================================= Combat: Combat Begins ================================== ]|
--Called when combat begins, to set up any local variables. Note that the Turn Begins code will be
-- called next, *after* turn order is resolved.
elseif(iSwitchType == gciAbility_BeginCombat) then

    -- |[Creation]|
    --Create a rubberraune.
    LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/007 Rubber Wilds/Rubberraune.lua")
    local iAlrauneID = giLastEnemyID
    LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/007 Rubber Wilds/Rubberbee.lua")
    local iBeeID = giLastEnemyID
    if(iAlrauneID == nil or iBeeID == nil) then return end
    
    -- |[Team Swap]|
    --Begin
    AdvCombat_SetProperty("Move Entity To Party", iAlrauneID)
    AdvCombat_SetProperty("Move Entity To Party", iBeeID)
    
    -- |[Abilities]|
    --Change these enemies to use the same ability set as Rubber Mei.
    local sJobAbilityPath = fnResolvePath()
    
    --Alraune
    AdvCombat_SetProperty("Push Entity By ID", iAlrauneID)
    
        --Clear AI and existing abilities.
        AdvCombatEntity_SetProperty("AI Script", "Null")
        fnClearAllSlots()
        
        --Provide abilities. These are identical to Mei's.
        LM_ExecuteScript(sJobAbilityPath .. "Spread Rubber.lua", gciAbility_Create)
        LM_ExecuteScript(sJobAbilityPath .. "Permagrin.lua",     gciAbility_Create)
        
        --Place abilities.
        AdvCombatEntity_SetProperty("Set Ability Slot", 0, 0, "Squeaky Thrall|Spread Rubber")
        AdvCombatEntity_SetProperty("Set Ability Slot", 1, 0, "Squeaky Thrall|Permagrin")
    DL_PopActiveObject()
    
    --Bee
    AdvCombat_SetProperty("Push Entity By ID", iBeeID)
    
        --Clear AI and existing abilities.
        AdvCombatEntity_SetProperty("AI Script", "Null")
        fnClearAllSlots()
        
        --Provide abilities. These are identical to Mei's.
        LM_ExecuteScript(sJobAbilityPath .. "Spread Rubber.lua", gciAbility_Create)
        LM_ExecuteScript(sJobAbilityPath .. "Permagrin.lua",     gciAbility_Create)
        
        --Place abilities.
        AdvCombatEntity_SetProperty("Set Ability Slot", 0, 0, "Squeaky Thrall|Spread Rubber")
        AdvCombatEntity_SetProperty("Set Ability Slot", 1, 0, "Squeaky Thrall|Permagrin")
    DL_PopActiveObject()
    
    -- |[Clean]|
    --Flag reset.
    giTargetID = iAlrauneID
    giLastEnemyID = nil

-- |[ ==================================== Standard Handler ==================================== ]|
--Call the standardized handler.
elseif(iSwitchType == gciAbility_CombatEnds) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
