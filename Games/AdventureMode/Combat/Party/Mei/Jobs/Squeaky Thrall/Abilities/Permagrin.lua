-- |[ ======================================== Permagrin ======================================== ]|
-- |[Description]|
--Enemies that have 3 stacks of [Rubberified] are turned into rubber permagrins and join the party.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.SqueakyThrall.Permagrin == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Squeaky Thrall", "Permagrin", "Use Skill Name", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "Mei|Permagrin", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Turns an enemy with 3 stacks of [Rubbered!]\ninto a squeaky permagrin ally."
    zAbiStruct.sSimpleDescMarkdown  = "Turns an enemy with 3 stacks of [Rubbered!]\ninto a squeaky permagrin ally."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, giNoCPCost, giNoCooldown, "Target Enemies Single", giNoCPGeneration)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Pierce")
    zAbiStruct.zExecutionAbiPackage.bDoesNoDamage = true
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun   = true
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits   = true
    zAbiStruct.zExecutionAbiPackage.bNeverCrits   = true
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.SqueakyThrall.Permagrin = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.SqueakyThrall.Permagrin

-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--Custom prediction box based on if the target can be Rubbered. Thanks to fnTrimTargetsWithoutTag in Paint Targets, the target must already be Rubberable.
if(iSwitchType == gciAbility_BuildPredictionBox) then
    
    -- |[ ===================== Other Values ===================== ]|
    --Get originator ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
    DL_PopActiveObject()
    
    --Get information about the cluster. This is needed to give each box a unique ID.
    local iClusterID = AdvCombat_GetProperty("Prediction Get Cluster ID")
    
    -- |[ ==================== For Each Target =================== ]|
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    for i = 0, iClusterTargetsTotal-1, 1 do
        
        --Target ID.
        local iTargetID = AdvCombat_GetProperty("Prediction Get Targets ID", i)
        
        --Get how many times Rubber tags appear on the entity.
        AdvCombat_SetProperty("Push Entity By ID", iTargetID)
            local iRubberifiedStacks = AdvCombatEntity_GetProperty("Tag Count", "Rubberified")
        DL_PopActiveObject()
        
        --Not enough Rubber.
        if(iRubberifiedStacks == 3) then
          gzRefAbility.zPredictionPackage.zaAdditionalLines[1] = {}
          gzRefAbility.zPredictionPackage.zaAdditionalLines[1].sString = "Transform to a Rubber thrall!"
          gzRefAbility.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
          
        --Insufficient Rubber.
        else
          gzRefAbility.zPredictionPackage.zaAdditionalLines[1] = {}
          gzRefAbility.zPredictionPackage.zaAdditionalLines[1].sString = "Not enough Rubber!"
          gzRefAbility.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
        end
        
        --Build the box!
        fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, gzRefAbility.zPredictionPackage)
    end
    
-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[ ================ Originator Statistics ================= ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
    DL_PopActiveObject()
    
    
    -- |[ ====================== Provide IDs ===================== ]|
    --Provide IDs to all packages.
    gzRefAbility.zExecutionAbiPackage.iOriginatorID = iOriginatorID
    
    
    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        
        --Target ID.
        local iTimer = (gciAbility_TicksOffsetPerTarget * (i-1))
        local iTargetID = AdvCombat_GetProperty("ID Of Target", i-1)
        
        --Check target stacks.
        AdvCombat_SetProperty("Push Target", i-1) 
            local iTargetUniqueID = RO_GetID()
            local iRubberifiedStacks = AdvCombatEntity_GetProperty("Tag Count", "Rubberified")
        DL_PopActiveObject()
        
        -- |[Not Enough Stacks]|
        if(iRubberifiedStacks < 3) then
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, iTimer, "Play Sound|Combat\\|AttackMiss")
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, iTimer, "Text|Failed!|Color:White")
            iTimer = iTimer + gciApplication_TextTicks
            AdvCombat_SetProperty("Event Timer", iTimer)
        
        -- |[Rubberified!]|
        else
        
            --Store the ID of the entity.
            giTargetID = iTargetUniqueID
        
            --Change parties.
            local iTimer = (i-1) * 15
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, iTimer, "Play Sound|Rubber\\|ChangeA")
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetID, iTimer, "Change Party|" .. gciACPartyGroup_Party)
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetID, iTimer, "Position|" .. gciACPoscode_Party)
            iTimer = iTimer + gciAC_StandardMoveTicks
            
            --Dialogue.
            local sScriptPath = LM_GetCallStack(0)
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetID, iTimer, "Run Script|" .. sScriptPath .. "|N:-1000")
            iTimer = iTimer + 15
            
            --End
            AdvCombat_SetProperty("Event Timer", iTimer)
        
        end
        
    end
    
-- |[ =================================== Permagrin Cutscene =================================== ]|
--Special
elseif(iSwitchType == -1000) then

    -- |[ ============ Error Check =========== ]|
    --Make sure the target exists.
    if(giTargetID == nil) then return end
    
    -- |[ ========= AI Modifications ========= ]|
    --Switch the AI of the target to make it player-controlled. Change its abilities.
    AdvCombat_SetProperty("Push Entity By ID", giTargetID)
    
        --Clear AI and existing abilities.
        AdvCombatEntity_SetProperty("AI Script", "Null")
        fnClearAllSlots()
        
        --Provide abilities. These are identical to Mei's.
        local sJobAbilityPath = fnResolvePath()
        LM_ExecuteScript(sJobAbilityPath .. "Spread Rubber.lua", gciAbility_Create)
        LM_ExecuteScript(sJobAbilityPath .. "Permagrin.lua",     gciAbility_Create)
        
        --Place abilities.
        AdvCombatEntity_SetProperty("Set Ability Slot", 0, 0, "Squeaky Thrall|Spread Rubber")
        AdvCombatEntity_SetProperty("Set Ability Slot", 1, 0, "Squeaky Thrall|Permagrin")
        
        --Cancel effects.
        local iEffectsTotal = AdvCombat_SetProperty("Store Effects Referencing", giTargetID)
        for i = 1, iEffectsTotal, 1 do
            AdvCombat_SetProperty("Push Temp Effects", i-1)
                AdvCombatEffect_SetProperty("Flag Remove Now")
            DL_PopActiveObject()
        end
        
        
    DL_PopActiveObject()
    
    -- |[ ========= Cutscene Handling ======== ]|
    --Setup.
    local saActorList = {}
    local saSlots = {}
    for i = 0, 6, 1 do
        saActorList[i] = "Null"
        saSlots[i] = "Null"
    end
    
    --Scan across the player's party. Store the dialogue actors associated with the characters.
    local iCombatPartySize = AdvCombat_GetProperty("Combat Party Size")
    for i = 0, iCombatPartySize-1, 1 do
        AdvCombat_SetProperty("Push Combat Party Member By Slot", i)
            local iUniqueID = RO_GetID()
            saActorList[i] = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/sDialogueActor", "S")
        DL_PopActiveObject()
    end
    
    --Determine which cutscene we need to run.
    local sDialogueActor = VM_GetVar("Root/Variables/Combat/" .. giTargetID .. "/sDialogueActor", "S")
    
    -- |[ ============= Cultist F ============ ]|
    if(sDialogueActor == "CultistF") then

        --Change properties:
        VM_SetVar("Root/Variables/Combat/" .. giTargetID .. "/sDialogueActor", "S", "RubberCultistF")
        AdvCombat_SetProperty("Push Entity By ID", giTargetID)
            AdvCombatEntity_SetProperty("Combat Portrait", "Root/Images/Portraits/Combat/CultistFRubber")
        DL_PopActiveObject()

        --Base.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "CultistF", "Neutral") ]])
        fnCutscene([[ Append("Cultist:[E|Neutral] [SOUND|Rubber|ChangeA]What the hell is this stuff![P] Aaaack![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] *Squeak*[P] (Spread...[P] Spread...[P] Spread...)[B][C]") ]])
        fnCutscene([[ Append("Cultist:[E|Neutral] [SOUND|Rubber|ChangeB]Get it -[P] off...[P] Spread...") ]])
        fnCutsceneBlocker()
        
        --Update the party.
        for i = 0, 6, 1 do 
            saSlots[i] = "Null"
        end
        if(iCombatPartySize == 2) then
            saSlots[0] = "RubberCultistF"
            saSlots[2] = saActorList[0]
        elseif(iCombatPartySize == 3) then
            saSlots[0] = "RubberCultistF"
            saSlots[1] = saActorList[1]
            saSlots[2] = saActorList[0]
        elseif(iCombatPartySize == 4) then
            saSlots[6] = "RubberCultistF"
            saSlots[0] = saActorList[2]
            saSlots[1] = saActorList[1]
            saSlots[2] = saActorList[0]
        end
        
        --Enemy has joined the party.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "RubberCultistF", "Neutral") ]])
        fnCutscene([[ Append("Cultist:[E|Neutral] [SOUND|Rubber|ChangeC]*Squeak*[P] (Spread...)[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] (Spread...)") ]])
        fnCutsceneBlocker()
    
    -- |[ ============== Alraune ============= ]|
    elseif(sDialogueActor == "Alraune") then

        --Change properties:
        VM_SetVar("Root/Variables/Combat/" .. giTargetID .. "/sDialogueActor", "S", "RubberAlraune")
        AdvCombat_SetProperty("Push Entity By ID", giTargetID)
            AdvCombatEntity_SetProperty("Combat Portrait", "Root/Images/Portraits/Combat/AlrauneRubber")
        DL_PopActiveObject()

        --Base.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
        fnCutscene([[ Append("Alraune:[E|Neutral] [SOUND|Rubber|ChangeA]No![P] It's on me![P] Get it off![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] *Squeak*[P] (Spread...[P] Spread...[P] Spread...)[B][C]") ]])
        fnCutscene([[ Append("Alraune:[E|Neutral] [SOUND|Rubber|ChangeB]Help! Help me...[P] spread it...") ]])
        fnCutsceneBlocker()
        
        
        --Enemy has joined the party.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "RubberAlraune", "Neutral") ]])
        fnCutscene([[ Append("Alraune:[E|Neutral] [SOUND|Rubber|ChangeC]*Squeak*[P] (Spread...)[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] (Spread...)") ]])
        fnCutsceneBlocker()
    
    -- |[ ================ Bee =============== ]|
    elseif(sDialogueActor == "Bee") then

        --Change properties:
        VM_SetVar("Root/Variables/Combat/" .. giTargetID .. "/sDialogueActor", "S", "RubberBee")
        AdvCombat_SetProperty("Push Entity By ID", giTargetID)
            AdvCombatEntity_SetProperty("Combat Portrait", "Root/Images/Portraits/Combat/BeeGirlRubber")
        DL_PopActiveObject()

        --Base.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Bee", "Neutral") ]])
        fnCutscene([[ Append("Bee:[E|Neutral] [SOUND|Rubber|ChangeC]Zzzzz![P] ZZzzzz![P] Bzzz!!![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] *Squeak*[P] (Spread...)[B][C]") ]])
        fnCutscene([[ Append("Bee:[E|Neutral] [SOUND|Rubber|ChangeA]...[P] zzz...") ]])
        fnCutsceneBlocker()
        
        --Enemy has joined the party.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "RubberBee", "Neutral") ]])
        fnCutscene([[ Append("Bee:[E|Neutral] [SOUND|Rubber|ChangeB]*Squeak*[P] (Spread...)[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] (Spread...)") ]])
        fnCutsceneBlocker()
    
    -- |[ ================ Werecat =============== ]|
    elseif(sDialogueActor == "Werecat") then

        --Change properties:
        VM_SetVar("Root/Variables/Combat/" .. giTargetID .. "/sDialogueActor", "S", "RubberWerecat")
        AdvCombat_SetProperty("Push Entity By ID", giTargetID)
            AdvCombatEntity_SetProperty("Combat Portrait", "Root/Images/Portraits/Combat/WerecatRubber")
        DL_PopActiveObject()

        --Base.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Werecat", "Neutral") ]])
        fnCutscene([[ Append("Werecat:[E|Neutral] [SOUND|Rubber|ChangeB]It's got me![P] Aaaaaagggghhh!!![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] *Squeak*[P] (Spread...)[B][C]") ]])
        fnCutscene([[ Append("Werecat:[E|Neutral] [SOUND|Rubber|ChangeC]It feels...[P] so good...[P] to spread...") ]])
        fnCutsceneBlocker()
        
        --Enemy has joined the party.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "RubberWerecat", "Neutral") ]])
        fnCutscene([[ Append("Werecat:[E|Neutral] [SOUND|Rubber|ChangeA]*Squeak*[P] (Spread...)[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] (Spread...)") ]])
        fnCutsceneBlocker()
        
    -- |[ ========== Werecat Thief =========== ]|
    elseif(sDialogueActor == "WerecatThief") then

        --Change properties:
        VM_SetVar("Root/Variables/Combat/" .. giTargetID .. "/sDialogueActor", "S", "RubberWerecatThief")
        AdvCombat_SetProperty("Push Entity By ID", giTargetID)
            AdvCombatEntity_SetProperty("Combat Portrait", "Root/Images/Portraits/Combat/WerecatBurglarRubber")
        DL_PopActiveObject()

        --Base.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "WerecatThief", "Neutral") ]])
        fnCutscene([[ Append("Werecat:[E|Neutral] [SOUND|Rubber|ChangeB]Someone help![P] I regret all the bad things I did!!![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] *Squeak*[P] (Spread...)[B][C]") ]])
        fnCutscene([[ Append("Werecat:[E|Neutral] [SOUND|Rubber|ChangeC]What?[P] Spread...[P] gotta spread...") ]])
        fnCutsceneBlocker()
        
        --Enemy has joined the party.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "RubberWerecatThief", "Neutral") ]])
        fnCutscene([[ Append("Werecat:[E|Neutral] [SOUND|Rubber|ChangeA]*Squeak*[P] (Spread...)[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] (Spread...)") ]])
        fnCutsceneBlocker()
    end
    
    --Clean up.
    giTargetID = nil

-- |[ ==================================== Standard Handler ==================================== ]|
--Call the standard handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end