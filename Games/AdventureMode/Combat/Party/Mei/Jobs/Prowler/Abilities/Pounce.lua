-- |[ ========================================= Pounce ========================================= ]|
-- |[Description]|
--Hits for 150% strike damage, but can only be used on a stunned target.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.Prowler.Pounce == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Prowler", "Pounce", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "Mei|Pounce", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\nCan only be used on stunned enemies.\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Jump on your helpless prey! Can only target\nstunned enemies.\nDeals increased [Stk](Strike) damage.\n\n\nCosts [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(20, giNoCPCost, giNoCooldown, "Target Enemies Single", 1)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Strike")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Striking, 5, 1.50)
    zAbiStruct.zExecutionAbiPackage:fnAddStun(50)
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Mei|General:Swing|Offense"

    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.Prowler.Pounce = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.Prowler.Pounce

-- |[ ================================== Combat: Paint Targets ================================= ]|
--Paints targets normally, then trims all targets who are not stunned.
if(iSwitchType == gciAbility_PaintTargets) then

    --Run macro.
    AdvCombat_SetProperty("Run Target Macro", "Target Enemies Single", AdvCombatAbility_GetProperty("Owner ID"))
    
    --Setup
    local iTotalTargetClusters = AdvCombat_GetProperty("Total Target Clusters")
    local saClustersToRemove = {}
    
    --Trim all targets that aren't stunned.
    for i = 0, iTotalTargetClusters-1, 1 do
        
        --Name of this cluster:
        local sClusterName = AdvCombat_GetProperty("Name of Target Cluster", i)
        
        --Make a list of targets to remove:
        local iaTargetsToRemove = {}
        
        --For each target in the cluster:
        local iTargetsInCluster = AdvCombat_GetProperty("Total Targets In Cluster", i)
        for p = 0, iTargetsInCluster-1, 1 do
            
            --Get the ID of the target. Check validity.
            local iTargetID = AdvCombat_GetProperty("Target ID In Cluster", i, p)
            if(iTargetID == 0) then
            
            else
                AdvCombat_SetProperty("Push Entity By ID", iTargetID)
                    local bIsStunned = AdvCombatEntity_GetProperty("Is Stunned")
                DL_PopActiveObject()
            
                if(bIsStunned == false) then
                    local iSize = #iaTargetsToRemove
                    iaTargetsToRemove[iSize+1] = iTargetID
                end
            end
        end
        
        --Once all targets are scanned, remove the target IDs from the given cluster. Note that this
        -- is done *after* checking all targets to preserve list integrity.
        for p = 1, #iaTargetsToRemove, 1 do
            AdvCombat_SetProperty("Remove Target From Cluster", i, iaTargetsToRemove[p])
        end
        
        --After removing targets, check if the cluster is empty. If so, remove the cluster.
        iTargetsInCluster = AdvCombat_GetProperty("Total Targets In Cluster", i)
        if(iTargetsInCluster < 1) then
            local iSize = #saClustersToRemove
            saClustersToRemove[iSize+1] = sClusterName
        end
    end
    
    --Remove pending clusters.
    for i = 1, #saClustersToRemove, 1 do
        AdvCombat_SetProperty("Remove Target Cluster By Name", saClustersToRemove[i])
    end
    
-- |[ ==================================== Standard Handler ==================================== ]|
--Call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
    