-- |[ ======================================== Jump Kick ======================================= ]|
-- |[Description]|
--Hits with Striking damage, deals more damage at full health.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.Prowler.JumpKick == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Prowler", "Jump Kick", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "Mei|JumpKick", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "==> Mei at less than full HP:\nInflicts [0.80x [Atk]] as [Stk], generate 25[Adrn].\n==> Mei at full HP:\nInflicts [1.20x [Atk]] as [Stk].\n\n[BaseHit]. [Target].\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "The karate classic, always aim for the face.\nIf you are below full health, you generate\n25 [Adrn](Adrenaline).\nAt full health, inflict extra damage.\n\nCosts [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(20, giNoCPCost, giNoCooldown, "Target Enemies Single", 1)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Strike")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Striking, 5, 0.80)
    zAbiStruct.zExecutionAbiPackage:fnAddStun(50)
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Mei|General:Swing|Offense"

    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.Prowler.JumpKick = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.Prowler.JumpKick

-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--Changes prediction based on whether or not the originator has full health.
if(iSwitchType == gciAbility_BuildPredictionBox) then

    -- |[ ===================== Other Values ===================== ]|
    --Get originator ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
        local iHP = AdvCombatEntity_GetProperty("Health")
        local iHPMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
    DL_PopActiveObject()
    
    -- |[ ========================= Setup ======================== ]|
    --Check if Mei is at full HP. If so, change the damage factor.
    gzRefAbility.zPredictionPackage.zDamageModule.fDamageFactor = 0.80
    if(iHP >= iHPMax) then
        gzRefAbility.zPredictionPackage.zDamageModule.fDamageFactor = 1.20
    end
    
    --Get information about the cluster. This is needed to give each box a unique ID.
    local iClusterID = AdvCombat_GetProperty("Prediction Get Cluster ID")
    
    -- |[ ==================== For Each Target =================== ]|
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    for i = 0, iClusterTargetsTotal-1, 1 do
        
        --Basic prediction box.
        local iTargetID = AdvCombat_GetProperty("Prediction Get Targets ID", i)
        fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, gzRefAbility.zPredictionPackage)

        --If Mei would get Adrenaline for this:
        if(iHP < iHPMax) then
            local sBoxName = "MeiBox" .. iOwnerID .. iTargetID
            AdvCombat_SetProperty("Prediction Create Box",              sBoxName)
            AdvCombat_SetProperty("Prediction Set Host",                sBoxName, iOwnerID)
            AdvCombat_SetProperty("Prediction Set Offsets",             sBoxName, 0, -100)
            AdvCombat_SetProperty("Prediction Allocate Strings",        sBoxName, 1)
            AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, 0, "+25[IMG0]")
            AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, 0, 1)
            AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, 0, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Adrenaline")
        end
    end
        
-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[ ================ Originator Statistics ================= ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        local iHP = AdvCombatEntity_GetProperty("Health")
        local iHPMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
        fnModifyCP(1)
        fnModifyMP(-20)
    DL_PopActiveObject()
    
    -- |[ =================== Ability Package ==================== ]|
    --Check if Mei is at full HP. If so, change the damage factor.
    gzRefAbility.zExecutionAbiPackage.fDamageFactor = 0.80
    if(iHP >= iHPMax) then
        gzRefAbility.zExecutionAbiPackage.fDamageFactor = 1.20
    end
    
    -- |[ ================== Give Mei Adrenaline ================= ]|
    --If Mei would get Adrenaline for this:
    if(iHP < iHPMax) then
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 15, "Adrenaline|"..25)
    end
    
    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them. Call the standard
    -- execution subroutine.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        fnStandardExecution(iOriginatorID, i-1, gzRefAbility.zExecutionAbiPackage, nil)
    end
    
-- |[ ==================================== Standard Handler ==================================== ]|
--Call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
