-- |[ ======================================= Deadly Jump ====================================== ]|
-- |[Description]|
--Unlocks the Deadly Jump field ability. Cannot be equipped.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Ability Prototype =================================== ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.Prowler.DeadlyJump == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Prowler", "Deadly Jump", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Special", "Mei|Pounce", "Unequippable")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Unlocks the Deadly Jump field ability, which allows you\nto stun enemies by leaping on them.\n10 Second Cooldown."
    zAbiStruct.sSimpleDescMarkdown  = "Unlocks the Deadly Jump field ability.\nThis ability allows you to ambush enemies from\nthe front on the overworld.\n\nField abilities can be seen in the top left corner\n"..
                                      "when out of battle."

    -- |[Usability Variables]|
    zAbiStruct.bNeverAvailable = true --Field abilities have no usability requirements.
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.Prowler.DeadlyJump = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.Prowler.DeadlyJump

--Call standard.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
