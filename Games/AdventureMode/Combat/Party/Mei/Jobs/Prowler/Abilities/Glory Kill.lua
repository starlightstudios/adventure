-- |[ ======================================= Glory Kill ======================================= ]|
-- |[Description]|
--Normal attack, provides adrenaline if the target is KOd from it.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.Prowler.GloryKill == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Prowler", "Glory Kill", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "Mei|GloryKill", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\nGenerates 10%%[Hlt] as [Adrn] if target is KOd by attack.\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Attack the enemy as normal. If you KO them,\nyou recover [Adrn](Adrenaline).\n\n\n\nCosts [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(20, giNoCPCost, giNoCooldown, "Target Enemies Single", 1)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Weapon")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_UseWeapon, 5, 1.10)
    zAbiStruct.zExecutionAbiPackage:fnAddStun(50)
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Mei|Ability:GloryKill"

    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.Prowler.GloryKill = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.Prowler.GloryKill

-- |[ ==================================== Creation Handler ==================================== ]|
--Called when the ability is created and added to the job prototype. This shows the ability's name
-- and icon, to inform the player what abilities are on the job.
if(iSwitchType == gciAbility_Create or iSwitchType == gciAbility_CreateSpecial) then
    
    -- |[Setup]|
    --Special: If the ability has an internal version, the name changes. This is used for abilities
    -- that can both be purchased for JP and exist on the job skill bar by default.
    local sUseJobName = gzRefAbility.sJobName
    if(iSwitchType == gciAbility_CreateSpecial) then sUseJobName = gzRefAbility.sInternalName end
    
    --Special: If a second argument is provided, then the name of the ability is being overrode. This
    -- is used by item creation scripts since items have fixed names.
    local sAbilityNameOverride = "NULL"
    if(iArgumentsTotal >= 2) then
        sAbilityNameOverride = LM_GetScriptArgument(1)
    end
    
    -- |[Creation]|
    local sAbilityName = sUseJobName .. "|" .. gzRefAbility.sSkillName
    if(sAbilityNameOverride ~= "NULL") then sAbilityName = sAbilityNameOverride end
    AdvCombatEntity_SetProperty("Create Ability", sAbilityName)
    
        --System
        AdvCombatAbility_SetProperty("Script Path", LM_GetCallStack(0)) --Use calling script.
        AdvCombatAbility_SetProperty("JP Cost", gzRefAbility.iJPUnlockCost)
    
        --Display
        AdvCombatAbility_SetProperty("Display Name", gzRefAbility.sSkillName)
        AdvCombatAbility_SetProperty("Icon Back",    gzRefAbility.sIconBacking)
        AdvCombatAbility_SetProperty("Icon Frame",   gzRefAbility.sIconFrame)
        AdvCombatAbility_SetProperty("Icon",         gzRefAbility.sIconPath)
        if(gzRefAbility.sCPIcon ~= "Null") then
            AdvCombatAbility_SetProperty("CP Icon", gzRefAbility.sCPIcon)
        end
        
        --Description
        AdvCombatAbility_SetProperty("Description", gzRefAbility.sDescription)
        AdvCombatAbility_SetProperty("Allocate Description Images", #gzRefAbility.saImages)
        for i = 1, #gzRefAbility.saImages, 1 do
            AdvCombatAbility_SetProperty("Description Image", i-1, gcfAbilityImgOffsetY, gzRefAbility.saImages[i])
        end
        AdvCombatAbility_SetProperty("Crossload Description Images")
        
        --Simplified Description
        if(gzRefAbility.sSimpleDesc ~= nil) then
            AdvCombatAbility_SetProperty("Simplified Description", gzRefAbility.sSimpleDesc)
            AdvCombatAbility_SetProperty("Allocate Simplified Description Images", #gzRefAbility.saSimpleImages)
            for i = 1, #gzRefAbility.saSimpleImages, 1 do
                AdvCombatAbility_SetProperty("Simplified Description Image", i-1, gcfAbilityImgOffsetY, gzRefAbility.saSimpleImages[i])
            end
            AdvCombatAbility_SetProperty("Crossload Simplified Description Images")
        end
        
        --Response Flags
        fnSetAbilityResponseFlags(gzRefAbility.iResponseType)
        AdvCombatAbility_SetProperty("Script Response", gciAbility_PostAction, true) --Because we need to check if the target was KO'd.
        AdvCombatAbility_SetProperty("Script Response", gciAbility_CombatEnds, true) --Because we need to check if the target was KO'd.
        
    DL_PopActiveObject()

-- |[ ============================= Combat: Character Action Begins ============================ ]|
--Called when the character that possesses this ability begins their turn. This is where the ability
-- should enable or disable itself based on the character's MP/State/etc.
elseif(iSwitchType == gciAbility_BeginAction) then

    --Ability owner is acting, run normal begin handler.
    if(AdvCombatAbility_GetProperty("Is Owner Acting") == true) then
        fnAbilityStandardBeginAction(20, false, 0, false)
    
    --Ability owner is not acting. If flagged, check if the target got KO'd and award adrenaline.
    else

        --Check if the ID set was tripped.
        local iTargetID     = VM_GetVar("Root/Variables/Combat/WerecatGloryKill/iTargetID", "N")
        local iOriginatorID = VM_GetVar("Root/Variables/Combat/WerecatGloryKill/iOriginatorID", "N")
        
        --Reset.
        VM_SetVar("Root/Variables/Combat/WerecatGloryKill/iTargetID", "N", 0)
        VM_SetVar("Root/Variables/Combat/WerecatGloryKill/iOriginatorID", "N", 0)
        if(iTargetID == 0) then return end
        
        --Check if the target is on the graveyard.
        if(AdvCombat_GetProperty("Party Of ID", iTargetID) == gciACPartyGroup_Graveyard) then
            
            --Award Adrenaline to originator.
            AdvCombat_SetProperty("Push Entity By ID", iOriginatorID)
                local iHPMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
            DL_PopActiveObject()
            
            --Application pack.
            local iAward = math.floor(iHPMax * 0.10)
            if(iAward < 1) then iAward = 1 end
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 15, "Adrenaline|"..iAward)
            
        end
    end

-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--Creates a box indicating that the originator gets adrenaline if they KO.
elseif(iSwitchType == gciAbility_BuildPredictionBox) then

    -- |[ ===================== Other Values ===================== ]|
    --Get originator ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
        local iHPMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
    DL_PopActiveObject()
    
    -- |[ ========================= Setup ======================== ]|
    --Get information about the cluster. This is needed to give each box a unique ID.
    local iClusterID = AdvCombat_GetProperty("Prediction Get Cluster ID")
    
    -- |[ ==================== For Each Target =================== ]|
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    for i = 0, iClusterTargetsTotal-1, 1 do
        local iTargetID = AdvCombat_GetProperty("Prediction Get Targets ID", i)
        fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, gzRefAbility.zPredictionPackage)
    end
    
    -- |[ ======================= For Owner ====================== ]|
    local iAdrenaline = math.floor(iHPMax * 0.10)
    local sBoxName = "Prd" .. 0 .. "x" .. iOwnerID
    AdvCombat_SetProperty("Prediction Create Box",  sBoxName)
    AdvCombat_SetProperty("Prediction Set Host",    sBoxName, iOwnerID)
    AdvCombat_SetProperty("Prediction Set Offsets", sBoxName, 0, -100)
    AdvCombat_SetProperty("Prediction Allocate Strings",        sBoxName, 1)
    AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, 0, "On enemy KO: +" .. iAdrenaline .. "[IMG0]")
    AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, 0, 1)
    AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, 0, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Adrenaline")

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[ ================ Originator Statistics ================= ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        local iHPMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
        fnModifyCP(1)
        fnModifyMP(-20)
    DL_PopActiveObject()
    
    -- |[ =================== Ability Package ==================== ]|
    --Give the ability the originator's ID.
    gzRefAbility.zExecutionAbiPackage.iOriginatorID = iOriginatorID
    
    --Store the originator ID.
    DL_AddPath("Root/Variables/Combat/WerecatGloryKill/")
    VM_SetVar("Root/Variables/Combat/WerecatGloryKill/iOriginatorID", "N", iOriginatorID)
    
    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them. Call the standard
    -- execution subroutine.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        
        --Normal execution.
        fnStandardExecution(iOriginatorID, i-1, gzRefAbility.zExecutionAbiPackage, nil)
        
        --Mark the target's ID.
        AdvCombat_SetProperty("Push Target", i-1) 
            VM_SetVar("Root/Variables/Combat/WerecatGloryKill/iTargetID", "N", RO_GetID())
        DL_PopActiveObject()
    end

-- |[ =================================== Combat: Combat Ends ================================== ]|
--Just like the next-turn code, called if combat ends on the last hit.
elseif(iSwitchType == gciAbility_CombatEnds) then

    --Check if the ID set was tripped.
    local iTargetID     = VM_GetVar("Root/Variables/Combat/WerecatGloryKill/iTargetID", "N")
    local iOriginatorID = VM_GetVar("Root/Variables/Combat/WerecatGloryKill/iOriginatorID", "N")
    
    --Reset.
    VM_SetVar("Root/Variables/Combat/WerecatGloryKill/iTargetID", "N", 0)
    VM_SetVar("Root/Variables/Combat/WerecatGloryKill/iOriginatorID", "N", 0)
    if(iTargetID == 0) then return end
    
    --Check if the target is on the graveyard.
    if(AdvCombat_GetProperty("Party Of ID", iTargetID) == gciACPartyGroup_Graveyard) then
        
        --Award Adrenaline to originator.
        AdvCombat_SetProperty("Push Entity By ID", iOriginatorID)
        
            --Get stats.
            local iHPMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
            local iAdrenaline = AdvCombatEntity_GetProperty("Adrenaline")
            
            --Application pack.
            local iAward = math.floor(iHPMax * 0.10)
            if(iAward < 1) then iAward = 1 end
            
            --Set.
            AdvCombatEntity_SetProperty("Adrenaline", iAdrenaline + iAward)
        DL_PopActiveObject()
    end

-- |[ ==================================== Standard Handler ==================================== ]|
--Call the standard handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
