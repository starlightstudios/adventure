-- |[ ========================================= Prowler ======================================== ]|
--The Werecat class.

-- |[ ========== Argument Handling =========== ]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--1st argument is only used for level-up calls.
local iLevelReached = 0
if(iArgumentsTotal >= 2) then
    iLevelReached = LM_GetScriptArgument(1, "N")
end

-- |[ ============ Variable Setup ============ ]|
-- |[Enumerations]|
local gciNoSkillbooks = 0

-- |[Encapsulation]|
zEntry = {}

-- |[Basic Variables]|
--Character Variables.
zEntry.sCharacterFormVarPath = "Root/Variables/Global/Mei/sForm"
zEntry.sSkillbookVarPath     = "Root/Variables/Global/Mei/iSkillbookTotal"
zEntry.zaJobChart            = gzaMeiJobChart
zEntry.sStatProfilePath      = gsRoot .. "Combat/Party/Mei/400 Common Stat Profile.lua"
zEntry.sSecondaryMenuPath    = gsRoot .. "Combat/Party/Mei/200 Build Secondary Menu.lua"
zEntry.sMasterBonusPath      = gsRoot .. "Combat/Party/Mei/401 Mastery Bonus Handler.lua"

--Class Variables.
zEntry.sClassName   = "Prowler"
zEntry.sFormName    = "Werecat"
zEntry.sCostumeName = "Mei_Werecat"
zEntry.fFaceIndexX  = 5
zEntry.fFaceIndexY  = 0

-- |[Ability Variables]|
--Purchaseable abilities.
zEntry.saExternalAbilities = {}
table.insert(zEntry.saExternalAbilities, "Jab")
table.insert(zEntry.saExternalAbilities, "Pounce")
table.insert(zEntry.saExternalAbilities, "Jump Kick")
table.insert(zEntry.saExternalAbilities, "Garrote")
table.insert(zEntry.saExternalAbilities, "Glory Kill") 
table.insert(zEntry.saExternalAbilities, "Lunge")       
table.insert(zEntry.saExternalAbilities, "Swipe")    
table.insert(zEntry.saExternalAbilities, "Hunters Instinct")
table.insert(zEntry.saExternalAbilities, "Way Of The Claw")
table.insert(zEntry.saExternalAbilities, "Deadly Jump")

--Class-equipped abilities.
zEntry.saInternalAbilities = {}
table.insert(zEntry.saInternalAbilities, "Pounce")
table.insert(zEntry.saInternalAbilities, "Jump Kick")
table.insert(zEntry.saInternalAbilities, "Garrote")
table.insert(zEntry.saInternalAbilities, "Swipe")
table.insert(zEntry.saInternalAbilities, "Glory Kill")
table.insert(zEntry.saInternalAbilities, "Hunters Instinct")
table.insert(zEntry.saInternalAbilities, "Lunge")

--Purchaseable abilities, in the order of purchase.
zEntry.saPurchaseAbilities = {}
table.insert(zEntry.saPurchaseAbilities, "Prowler|Pounce")
table.insert(zEntry.saPurchaseAbilities, "Prowler|Jump Kick")
table.insert(zEntry.saPurchaseAbilities, "Prowler|Garrote")
table.insert(zEntry.saPurchaseAbilities, "Prowler|Glory Kill")
table.insert(zEntry.saPurchaseAbilities, "Prowler|Jab")
table.insert(zEntry.saPurchaseAbilities, "Prowler|Lunge")
table.insert(zEntry.saPurchaseAbilities, "Prowler|Swipe")
table.insert(zEntry.saPurchaseAbilities, "Prowler|Hunters Instinct")
table.insert(zEntry.saPurchaseAbilities, "Prowler|Way Of The Claw")
table.insert(zEntry.saPurchaseAbilities, "Prowler|Deadly Jump")
                                          
-- |[Skillbar Variables]|
--Skills that always appear on the skillbar. Format: {sAbilityName, iXPos, iYPos, iSkillbooks} (eg: {"Common|Attack", 0, 0, 0}, {"Common|Parry", 1, 0, 0} {})
zEntry.zaSkillbarAbilities = {}
table.insert(zEntry.zaSkillbarAbilities, {"Common|Attack",                     0, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Common|Parry",                      1, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Prowler Internal|Lunge",            2, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Prowler Internal|Pounce",           0, 1, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Prowler Internal|Jump Kick",        1, 1, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Prowler Internal|Garrote",          2, 1, 1})
table.insert(zEntry.zaSkillbarAbilities, {"Prowler Internal|Swipe",            0, 2, 2})
table.insert(zEntry.zaSkillbarAbilities, {"Prowler Internal|Glory Kill",       1, 2, 3})
table.insert(zEntry.zaSkillbarAbilities, {"Prowler Internal|Hunters Instinct", 2, 2, 4})

-- |[ ============ Call Standard ============= ]|
--Add to the global stack. There may be multiple executions of this script.
table.insert(gzJobCallStack, zEntry)

--Call.
LM_ExecuteScript(gsStandardJobPath, iSwitchType, iLevelReached)

--Pop the global stack.
table.remove(gzJobCallStack)
