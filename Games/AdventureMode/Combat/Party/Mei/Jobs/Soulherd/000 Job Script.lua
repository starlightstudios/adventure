-- |[ ======================================== Soulherd ======================================== ]|
--The Wisphag class.

-- |[ ========== Argument Handling =========== ]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--1st argument is only used for level-up calls.
local iLevelReached = 0
if(iArgumentsTotal >= 2) then
    iLevelReached = LM_GetScriptArgument(1, "N")
end

-- |[ ============ Variable Setup ============ ]|
-- |[Enumerations]|
local gciNoSkillbooks = 0

-- |[Encapsulation]|
local zEntry = {}

-- |[Basic Variables]|
--Character Variables.
zEntry.sCharacterFormVarPath = "Root/Variables/Global/Mei/sForm"
zEntry.sSkillbookVarPath     = "Root/Variables/Global/Mei/iSkillbookTotal"
zEntry.zaJobChart            = gzaMeiJobChart
zEntry.sStatProfilePath      = gsRoot .. "Combat/Party/Mei/400 Common Stat Profile.lua"
zEntry.sSecondaryMenuPath    = gsRoot .. "Combat/Party/Mei/200 Build Secondary Menu.lua"
zEntry.sMasterBonusPath      = gsRoot .. "Combat/Party/Mei/401 Mastery Bonus Handler.lua"

--Class Variables.
zEntry.sClassName   = "Soulherd"
zEntry.sFormName    = "Wisphag"
zEntry.sCostumeName = "Mei_Wisphag"
zEntry.fFaceIndexX  = 8
zEntry.fFaceIndexY  = 0

-- |[Ability Variables]|
--Purchaseable abilities.
zEntry.saExternalAbilities = {}
table.insert(zEntry.saExternalAbilities, "Burning Soul")
table.insert(zEntry.saExternalAbilities, "Claw Slash")
table.insert(zEntry.saExternalAbilities, "Creepy Giggle")
table.insert(zEntry.saExternalAbilities, "Drag Beneath")
table.insert(zEntry.saExternalAbilities, "Lightburst")
table.insert(zEntry.saExternalAbilities, "Mend Spirit")
table.insert(zEntry.saExternalAbilities, "Shadowburst")
table.insert(zEntry.saExternalAbilities, "Vicious Strike")
table.insert(zEntry.saExternalAbilities, "Way Of The Wisps")

--Class-equipped abilities.
zEntry.saInternalAbilities = {}
table.insert(zEntry.saInternalAbilities, "Claw Slash")
table.insert(zEntry.saInternalAbilities, "Mend Spirit")
table.insert(zEntry.saInternalAbilities, "Lightburst")
table.insert(zEntry.saInternalAbilities, "Shadowburst")
table.insert(zEntry.saInternalAbilities, "Vicious Strike")
table.insert(zEntry.saInternalAbilities, "Drag Beneath")
table.insert(zEntry.saInternalAbilities, "Burning Soul")

--Purchaseable abilities, in the order of purchase.
zEntry.saPurchaseAbilities = {}
table.insert(zEntry.saPurchaseAbilities, "Soulherd|Drag Beneath")
table.insert(zEntry.saPurchaseAbilities, "Soulherd|Vicious Strike")
table.insert(zEntry.saPurchaseAbilities, "Soulherd|Claw Slash")
table.insert(zEntry.saPurchaseAbilities, "Soulherd|Lightburst")
table.insert(zEntry.saPurchaseAbilities, "Soulherd|Shadowburst")
table.insert(zEntry.saPurchaseAbilities, "Soulherd|Creepy Giggle")
table.insert(zEntry.saPurchaseAbilities, "Soulherd|Mend Spirit")
table.insert(zEntry.saPurchaseAbilities, "Soulherd|Burning Soul")
table.insert(zEntry.saPurchaseAbilities, "Soulherd|Way Of The Wisps")

-- |[Skillbar Variables]|
--Skills that always appear on the skillbar. Format: {sAbilityName, iXPos, iYPos, iSkillbooks} (eg: {"Common|Attack", 0, 0, 0}, {"Common|Parry", 1, 0, 0} {})
zEntry.zaSkillbarAbilities = {}
table.insert(zEntry.zaSkillbarAbilities, {"Common|Attack",                    0, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Common|Parry",                     1, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Soulherd Internal|Burning Soul",   2, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Soulherd Internal|Claw Slash",     0, 1, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Soulherd Internal|Mend Spirit",    1, 1, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Soulherd Internal|Lightburst",     2, 1, 1})
table.insert(zEntry.zaSkillbarAbilities, {"Soulherd Internal|Shadowburst",    0, 2, 2})
table.insert(zEntry.zaSkillbarAbilities, {"Soulherd Internal|Vicious Strike", 1, 2, 3})
table.insert(zEntry.zaSkillbarAbilities, {"Soulherd Internal|Drag Beneath",   2, 2, 4})

-- |[ ============ Call Standard ============= ]|
--Add to the global stack. There may be multiple executions of this script.
table.insert(gzJobCallStack, zEntry)

--Call.
LM_ExecuteScript(gsStandardJobPath, iSwitchType, iLevelReached)

--Pop the global stack.
table.remove(gzJobCallStack)
