-- |[ ==================================== Way of the Wisps ==================================== ]|
-- |[Description]|
--Applies a buff after scoring a critical hit that increases accuracy by 30 for 3 turns. The buff
-- can stack if multiple crits are landed!

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.Soulherd.WayOfTheWisps == nil) then
    
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    fnSetAbilitySystemProperties(zAbiStruct, "Soulherd", "Way Of The Wisps", "Way of the Wisps", gciJP_Cost_Passive, gbIsNotFreeAction, "Buff", "Passive", "Mei|Soulherd|WayOfTheWisps", "Passive")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Gain a stacking buff increasing [Acc] by 30 for each time a\ncritical strike lands.\n\n\n\n\nPassive."
    zAbiStruct.sSimpleDescMarkdown  = "Fill with power each time a critical hit lands.\n+30[Acc](Accuracy) after each critical strike. Stacks.\nLasts 3 turns.\n\n\nPassive."
    
    -- |[Passive Effect]|
    zAbiStruct.bNeverAvailable   = true --Passive abilities have no usability requirements.
    zAbiStruct.sPassivePrototype = "Mei.Soulherd.WayOfTheWisps"
    zAbiStruct.bHasGUIEffects    = false

    -- |[ ===== Passive Effect Prototype ===== ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    if(EffectList:fnEntryExists(zAbiStruct.sPassivePrototype) == false) then
        local saDescription = {}
        saDescription[1] = "The souls of the wayward empower you when you strike"
        saDescription[2] = "critically. After a critical hit, +30[Acc] for 3 turns,"
        saDescription[3] = "stacking."
      --EffectList:fnCreatePassivePrototype(psName, psDisplayName, psIcon, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreatePassivePrototype(zAbiStruct.sPassivePrototype, "Way of the Wisps", "Mei|Soulherd|WayOfTheWisps", "", saDescription)
    end
    
    -- |[ ====== Active Effect Prototype ===== ]|
    --This is the effect that gets applied when an attack crits.
    if(EffectList:fnEntryExists("Mei.Soulherd.SightOfTheWisps") == false) then
        local saDescription = {}
        saDescription[1] = "Guided by the souls of the wayward!"
        saDescription[2] = "Increases [Acc](Accuracy)."
      --EffectList:fnCreateBuffPrototype(psName, psDisplayName, psIcon, piTurns, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreateBuffPrototype("Mei.Soulherd.SightOfTheWisps", "Sight of the Wisps", "Mei|Soulherd|WayOfTheWisps", 3, "Acc|Flat|30", saDescription)
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.Soulherd.WayOfTheWisps = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.Soulherd.WayOfTheWisps

-- |[ ==================================== Creation Handler ==================================== ]|
--Same as the standard creation, except adds the action-ends response.
if(iSwitchType == gciAbility_Create or iSwitchType == gciAbility_CreateSpecial) then
    
    -- |[Setup]|
    --Special: If the ability has an internal version, the name changes. This is used for abilities
    -- that can both be purchased for JP and exist on the job skill bar by default.
    local sUseJobName = gzRefAbility.sJobName
    if(iSwitchType == gciAbility_CreateSpecial) then sUseJobName = gzRefAbility.sInternalName end
    
    --Special: If a second argument is provided, then the name of the ability is being overrode. This
    -- is used by item creation scripts since items have fixed names.
    local sAbilityNameOverride = "NULL"
    if(iArgumentsTotal >= 2) then
        sAbilityNameOverride = LM_GetScriptArgument(1)
    end
    
    -- |[Creation]|
    local sAbilityName = sUseJobName .. "|" .. gzRefAbility.sSkillName
    if(sAbilityNameOverride ~= "NULL") then sAbilityName = sAbilityNameOverride end
    AdvCombatEntity_SetProperty("Create Ability", sAbilityName)
    
        --System
        AdvCombatAbility_SetProperty("Script Path", LM_GetCallStack(0)) --Use this script.
        AdvCombatAbility_SetProperty("JP Cost", gzRefAbility.iJPUnlockCost)
        AdvCombatAbility_SetProperty("CP Cost", gzRefAbility.iRequiredCP)
        
        --Tags. The structure may be nil.
        if(gzRefAbility.zaTags ~= nil) then
            for i = 1, #gzRefAbility.zaTags, 1 do
                AdvCombatAbility_SetProperty("Add Tag", gzRefAbility.zaTags[i][1], gzRefAbility.zaTags[i][2])
            end
        end
    
        --Display
        AdvCombatAbility_SetProperty("Display Name", gzRefAbility.sSkillName)
        AdvCombatAbility_SetProperty("Icon Back",    gzRefAbility.sIconBacking)
        AdvCombatAbility_SetProperty("Icon Frame",   gzRefAbility.sIconFrame)
        AdvCombatAbility_SetProperty("Icon",         gzRefAbility.sIconPath)
        if(gzRefAbility.sCPIcon ~= "Null") then
            AdvCombatAbility_SetProperty("CP Icon", gzRefAbility.sCPIcon)
        end
        
        --Special: Name Override. If the field sDisplayName isn't nil, then the skill name and the display name aren't the same.
        if(gzRefAbility.sDisplayName ~= nil) then
            AdvCombatAbility_SetProperty("Display Name", gzRefAbility.sDisplayName)
        end
        
        --Description
        AdvCombatAbility_SetProperty("Description", gzRefAbility.sDescription)
        AdvCombatAbility_SetProperty("Allocate Description Images", #gzRefAbility.saImages)
        for i = 1, #gzRefAbility.saImages, 1 do
            AdvCombatAbility_SetProperty("Description Image", i-1, gcfAbilityImgOffsetY, gzRefAbility.saImages[i])
        end
        AdvCombatAbility_SetProperty("Crossload Description Images")
        
        --Simplified Description
        if(gzRefAbility.sSimpleDesc ~= nil) then
            AdvCombatAbility_SetProperty("Simplified Description", gzRefAbility.sSimpleDesc)
            AdvCombatAbility_SetProperty("Allocate Simplified Description Images", #gzRefAbility.saSimpleImages)
            for i = 1, #gzRefAbility.saSimpleImages, 1 do
                AdvCombatAbility_SetProperty("Simplified Description Image", i-1, gcfAbilityImgOffsetLgY, gzRefAbility.saSimpleImages[i])
            end
            AdvCombatAbility_SetProperty("Crossload Simplified Description Images")
        end
        
        --Response Flags
        fnSetAbilityResponseFlags(gzRefAbility.iResponseType)
        
        --Additional response.
        AdvCombatAbility_SetProperty("Script Response", gciAbility_PostAction, true)
        
    DL_PopActiveObject()

-- |[ ============================= Combat: Character Action Ends ============================== ]|
--After the action ends, check if the owner acted, attacked, and got a crit.
elseif(iSwitchType == gciAbility_PostAction) then

    -- |[Activity Check]|
    --If the owner is not acting, fail.
    if(AdvCombatAbility_GetProperty("Is Owner Acting") == false) then return end
    
    -- |[Setup]|
    --Variables.
    local bGotAnyCrits = false
    
    --Get needed IDs.
    local iAbilityID = RO_GetID()
    local iOwnerID = AdvCombatAbility_GetProperty("Owner ID")
    
    -- |[Scan Actions]|
    --Scan backwards through the abilities used to find the one used by our owner. It should be the
    -- last one, but a counterattack might take that slot.
    local iCurrentTurn = gzaCombatAbilityStatistics.iTurnsElapsed
    for i = #gzaCombatAbilityStatistics.zaStatisticsPacks, 1, -1 do

        --Get the package.
        local zStatsPack = gzaCombatAbilityStatistics.zaStatisticsPacks[i]
        
        --Error checks:
        if(zStatsPack == nil) then
            --io.write("  Error, out of range.\n")
        elseif(zStatsPack.iTurnElapsed ~= iCurrentTurn) then
            --io.write("  Error, incorrect turn " .. zStatsPack.iTurnElapsed .. " vs. " .. iCurrentTurn .. "\n")
        elseif(zStatsPack.iActorID ~= iOwnerID) then
            --io.write("  Error, not action by owner " .. zStatsPack.iActorID .. " vs. " .. iOwnerID .. "\n")
        
        --Checks passed:
        else
            
            --Scan all targets hit. If any of these were crits, log that.
            local iTotalDamageDealt = 0
            for p = 1, #zStatsPack.zaTargetInfo, 1 do
                iTotalDamageDealt = iTotalDamageDealt + zStatsPack.zaTargetInfo[p].iDamageDealt
                if(zStatsPack.zaTargetInfo[p].bWasCritical) then bGotAnyCrits = true end
            end
            
            --Stop iteration.
            break
        end
    end
    
    -- |[Apply Effect]|
    --If a crit landed, apply the effect. Attacks that hit multiple targets only apply one buff.
    if(bGotAnyCrits == false) then return end
    AdvCombat_SetProperty("Register Application Pack", iOwnerID, iOwnerID, 0, "Effect|" .. gsGlobalEffectPrototypePath .. "|Originator:" .. iOwnerID .. "|StoreID:" .. 
                                                       iAbilityID .. "|Prototype:Mei.Soulherd.SightOfTheWisps")

-- |[ ==================================== Standard Handler ==================================== ]|
--Call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
