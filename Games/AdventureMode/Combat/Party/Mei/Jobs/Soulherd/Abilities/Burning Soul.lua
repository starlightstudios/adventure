-- |[ ====================================== Burning Soul ====================================== ]|
-- |[Description]|
--Increases attack power by 100% for 3 turns. Free action. Costs 4 CP.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Ability Prototype =================================== ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.Soulherd.BurningSoul == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Soulherd", "Burning Soul", "Use Skill Name", gciJP_Cost_Advanced, gbIsFreeAction, "Buff", "Combo", "Mei|Soulherd|BurningSoul", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[FEffect] +100[PCT][Atk] for 3[Turns].\n\n\n\n\nFree Action.\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown = "Enflame your soul with the ancient power of the[BR]wisps!\nGreatly increases [Atk](Power) for 3 turns.\n\nFree Action.\nCosts [CPCost][CPIco](CP)."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, 4, giNoCooldown, "Target Self", giNoCPGeneration)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Buff")
    zAbiStruct.zExecutionAbiPackage:fnSetAsBuff()
  --zAbiStruct.zExecutionAbiPackage:fnAddAnimation(psAnimationName, piTimerOffset, $pfXOffset, $pfYOffset)
    zAbiStruct.zExecutionAbiPackage:fnAddAnimation("Buff Power", 0)
    
    -- |[Effect Package]|
  --zAbiStruct:fnAddBuff(psPrototypeName, $psApplyText, $psApplyColorString, $psApplyAnim, $psApplySound)
    zAbiStruct:fnAddBuff("Mei.Soulherd.BurningSoul")

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
        local saDescription = {}
        saDescription[1] = "The wisps enflame your soul with power!"
        saDescription[2] = "Increases [Atk](Power)."
      --EffectList:fnCreateBuffPrototype(psName, psDisplayName, psIcon, piTurns, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreateBuffPrototype(sLocalPrototypeName, "Burning Soul", "Mei|Soulherd|BurningSoul", 3, "Atk|Pct|1.0", saDescription)
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Mei|Ability:BurningSoul"

    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.Soulherd.BurningSoul = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.Soulherd.BurningSoul

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
