-- |[ ===================================== Creepy Giggle ====================================== ]|
-- |[Description]|
--Increases threat, deals terrify damage to all targets, reduces enemy attack. Stronger than Taunt
-- but costs more.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.Soulherd.CreepyGiggle == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Soulherd", "Creepy Giggle", "Use Skill Name", gciJP_Cost_Normal, gbIsNotFreeAction, "Debuff", "Active", "Mei|Soulherd|CreepyGiggle", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\n[HEffect][Str5][Tfy] -15[PCT][Atk], 5[Turns].\n+300%%[Atk] as [Threat].\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Nothing voids the bowels like a soft laugh in the[BR]pitch darkness.\nReduces enemy [Atk](Power), increases your\n[Threat](Threat).\nDeals reduced damage as [Tfy](Terrify).\n"..
                                      "Costs [MPCost][MPIco]."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(35, giNoCPCost, giNoCooldown, "Target Enemies All", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Terrify")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Terrifying, 5, 0.90)
    
    --Threat modifier. Applies even if the attack misses.
    zAbiStruct:fnSetThreatFactor(3.0)
    
    -- |[Execution Effect Package]|
  --zAbiStruct:fnAddEffect(piStrNormal, piStrCrit, piApplyType, psApplyText, psCritApplyText, psApplyColorString, psaApplyBonus, psaApplyMalus, psaSeverityBonus, psaSeverityMalus)
    zAbiStruct:fnAddEffect(5, 8, gciDamageType_Terrifying, "Frightened!", "Frightened!", "Color:Red")
    
    -- |[Use of Effect Prototype]|
    zAbiStruct.zExecutionEffPackage.sPrototypeName = "Mei.Soulherd.CreepyGiggle"

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
        local saDescription = {}
        saDescription[1] = "Hard to attack when your hands are shaking!"
        saDescription[2] = "Reduces [Atk](Power)."
      --EffectList:fnCreateStatPrototype(pzEffectpackage, psName, pbIsBuff, psDisplayName, psIcon, piTurns, piApplyStrength, psStatString, psaDescription, $pzaTagList, $piCritTurns, $piCritApplyStrength)
        EffectList:fnCreateStatPrototype(zAbiStruct.zExecutionEffPackage, sLocalPrototypeName, gbIsDebuff, "Creepy Giggle", "Mei|Soulherd|CreepyGiggle", 5, 5, "Atk|Pct|-0.15", saDescription)
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Mei|Ability:CreepyGiggle"

    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.Soulherd.CreepyGiggle = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.Soulherd.CreepyGiggle

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
