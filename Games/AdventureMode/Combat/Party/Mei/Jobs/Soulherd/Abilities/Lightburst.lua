-- |[ ======================================= Lightburst ======================================= ]|
-- |[Description]|
--Inflict -50 Acc for 3 turns, strength 6, and Crusading damage.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.Soulherd.Lightburst == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Soulherd", "Lightburst", "Use Skill Name", gciJP_Cost_Normal, gbIsNotFreeAction, "Debuff", "Active", "Mei|Soulherd|Lightburst", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\n[HEffect][Str6][Cru] -50[Acc] for 3[Turns].\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Bring forth a mighty burst of light, blinding all[BR]enemies and inflicting [Cru](Crusade) damage.\nBlinded enemies have reduced [Acc](Accuracy).\n\n\nCosts [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(35, giNoCPCost, giNoCooldown, "Target Enemies All", 1)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Light")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Crusading, -25, 0.75)
    
    -- |[Execution Effect Package]|
  --zAbiStruct:fnAddEffect(piStrNormal, piStrCrit, piApplyType, psApplyText, psCritApplyText, psApplyColorString, psaApplyBonus, psaApplyMalus, psaSeverityBonus, psaSeverityMalus)
    zAbiStruct:fnAddEffect(6, 9, gciDamageType_Crusading, "Blinded!", "Badly Blinded!", "Color:Purple", {"Blind Standard"})
    
    -- |[Use of Effect Prototype]|
    zAbiStruct.zExecutionEffPackage.sPrototypeName = "Mei.Soulherd.Lightburst"

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
        local saDescription = {}
        saDescription[1] = "Blinded by a powerful light!"
        saDescription[2] = "Reduces [Acc](Accuracy)."
      --EffectList:fnCreateStatPrototype(pzEffectpackage, psName, pbIsBuff, psDisplayName, psIcon, piTurns, piApplyStrength, psStatString, psaDescription, pzaTagList, piCritTurns, piCritApplyStrength)
        EffectList:fnCreateStatPrototype(zAbiStruct.zExecutionEffPackage, sLocalPrototypeName, gbIsDebuff, "Lightburst", "Mei|Soulherd|Lightburst", 3, 6, "Acc|FlatSev|-50", saDescription, {}, 5, 9)
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Mei|Offense"

    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.Soulherd.Lightburst = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.Soulherd.Lightburst

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
