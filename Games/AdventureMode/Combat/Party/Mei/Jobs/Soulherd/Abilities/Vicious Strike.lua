-- |[ ===================================== Vicious Strike ===================================== ]|
-- |[Description]|
--Inflicts 1.20x damage, plus 0.20x damage for each DoT affecting the target.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.Soulherd.ViciousStrike == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Soulherd", "Vicious Strike", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "Mei|Soulherd|ViciousStrike", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n+0.20x [Atk] per DoT (any type).\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Rip into the enemy with your earthen claws.\nDeals extra damage for each DoT affecting the[BR]target.\n\n\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(20, giNoCPCost, giNoCooldown, "Target Enemies Single", 1)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Claw Slash")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Slashing, 5, 1.20)
    
    --Chaser Modules. Ability has one for each damage type.
  --zAbiStruct.zExecutionAbiPackage:fnAddChaser(pbConsumeEffect, psDoTTag, pfDamageChangePerDoT, pfDamageConsumeFactor)
    zAbiStruct.zExecutionAbiPackage:fnAddChaser(gbDoNotConsumeEffects, "Slashing DoT",   0.20, 0.0)
    zAbiStruct.zExecutionAbiPackage:fnAddChaser(gbDoNotConsumeEffects, "Striking DoT",   0.20, 0.0)
    zAbiStruct.zExecutionAbiPackage:fnAddChaser(gbDoNotConsumeEffects, "Piercing DoT",   0.20, 0.0)
    zAbiStruct.zExecutionAbiPackage:fnAddChaser(gbDoNotConsumeEffects, "Flaming DoT",    0.20, 0.0)
    zAbiStruct.zExecutionAbiPackage:fnAddChaser(gbDoNotConsumeEffects, "Freezing DoT",   0.20, 0.0)
    zAbiStruct.zExecutionAbiPackage:fnAddChaser(gbDoNotConsumeEffects, "Shocking DoT",   0.20, 0.0)
    zAbiStruct.zExecutionAbiPackage:fnAddChaser(gbDoNotConsumeEffects, "Crusading DoT",  0.20, 0.0)
    zAbiStruct.zExecutionAbiPackage:fnAddChaser(gbDoNotConsumeEffects, "Obscuring DoT",  0.20, 0.0)
    zAbiStruct.zExecutionAbiPackage:fnAddChaser(gbDoNotConsumeEffects, "Bleeding DoT",   0.20, 0.0)
    zAbiStruct.zExecutionAbiPackage:fnAddChaser(gbDoNotConsumeEffects, "Poisoning DoT",  0.20, 0.0)
    zAbiStruct.zExecutionAbiPackage:fnAddChaser(gbDoNotConsumeEffects, "Corroding DoT",  0.20, 0.0)
    zAbiStruct.zExecutionAbiPackage:fnAddChaser(gbDoNotConsumeEffects, "Terrifying DoT", 0.20, 0.0)
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Mei|General:Swing|Offense"

    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.Soulherd.ViciousStrike = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.Soulherd.ViciousStrike

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
