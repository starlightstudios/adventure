-- |[ ========================================= Tidy Up ======================================== ]|
-- |[Description]|
--Removes all poisons and corrode effects, adds Fast to the party.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Ability Prototype =================================== ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.Maid.TidyUp == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Maid", "Tidy Up", "$SkillName", gciJP_Cost_Normal, gbIsFreeAction, "Buff", "Active", "Mei|TidyUp", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Removes all [Psn] and [Crd] DoTs. [Target].\n\n[FEffect]'Fast' for 1[Turns].\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Cleans all [Psn](Poison) and [Crd](Corrode) DoTs\nfrom your party.\nAlso causes you to act first next turn.\n\n\nCosts [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(20, giNoCPCost, 1, "Target Allies All", giNoCPGeneration)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Haste")
    zAbiStruct.zExecutionAbiPackage:fnSetAsBuff()
    
    --Extra calls:
    zAbiStruct.zExecutionAbiPackage.saExecPaths = {LM_GetCallStack(0)}
    zAbiStruct.zExecutionAbiPackage.iaExecCodes = {gciAbility_SpecialStart}
    
    -- |[Effect Package]|
  --zAbiStruct:fnAddBuff(psPrototypeName, $psApplyText, $psApplyColorString, $psApplyAnim, $psApplySound)
    zAbiStruct:fnAddBuff("Mei.Maid.TidyUp", "Organized!", "Color:Blue")

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
        local saDescription = {}
        saDescription[1] = "An organized party is an efficient party!"
        saDescription[2] = "Provides [Fast] until your next turn."
      --EffectList:fnCreateBuffPrototype(psName, psDisplayName, psIcon, piTurns, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreateBuffPrototype(sLocalPrototypeName, "Tidy Up", "Mei|TidyUp", 1, "", saDescription, {{"Fast", 1}})

        --Manually specify description.
        local iType, zPrototype, iSlot = EffectList:fnLocateEntry(sLocalPrototypeName)
        if(zPrototype ~= nil) then
            zPrototype.bDontRebuildDescription = true
            zPrototype.saStrings[1] = "[Buff]Tidy Up"
            zPrototype.saStrings[2] = "+1[Fast] /[Dur][Turns]"
            zPrototype.saStrings[3] = saDescription[1]
            zPrototype.saStrings[4] = saDescription[2]
            zPrototype.saStrings[5] = ""
            zPrototype.saStrings[6] = ""
            zPrototype.saStrings[7] = ""
            zPrototype.saStrings[8] = "[Buff] [Fast] ([Dur][Turns])"
            zPrototype.sShortText = "[Buff] [Fast] ([Dur][Turns])"
            
            --Specify an extra turn of duration for the owner of the skill.
            zPrototype.bExtraDurationForOwner = true
        end
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Mei|Restore"

    -- |[Finalize]|
    --Autobuild.
    zAbiStruct:fnFinalize()
    
    --Prevent automatic effects from showing. We do this manually.
    zAbiStruct.zPredictionPackage.zaEffectModules = {}
    
    --Manually add prediction effect.
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "Remove all [IMG0][IMG1] DoTs."
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {"Root/Images/AdventureUI/DamageTypeIcons/Poisoning", "Root/Images/AdventureUI/DamageTypeIcons/Corroding"}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[2] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[2].sString   = "[IMG0]Act first next turn."
    zAbiStruct.zPredictionPackage.zaAdditionalLines[2].saSymbols = {"Root/Images/AdventureUI/EffectIndicator/EffectFriendly"}
    
    --Set reference.
    gzPrototypes.Combat.Mei.Maid.TidyUp = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.Maid.TidyUp

-- |[ ==================== Combat: Special - Remove Poison and Corrode DoTs ==================== ]|
if(iSwitchType == gciAbility_SpecialStart) then

    --Argument handling.
    if(iArgumentsTotal < 3) then return end
    local iOriginatorID   = LM_GetScriptArgument(1, "N")
    local iTargetUniqueID = LM_GetScriptArgument(2, "N")
    
    --Run subroutine.
    AbiExecPack:fnAutoRemoveEffects(iOriginatorID, iTargetUniqueID, {"Poisoning DoT", "Corroding DoT"})
    
    --[=[
    --Setup.
    local iEffectCount = 0
    local iaEffectIDList = {}
    
    --Get a list of all effects referencing the target. Remove any with the matching tag.
    local iEffectsTotal = AdvCombat_SetProperty("Store Effects Referencing", iTargetUniqueID)
    for i = 0, iEffectsTotal-1, 1 do
        AdvCombat_SetProperty("Push Temp Effects", i)
        
            --Tag is present:
            local iCount = AdvCombatEffect_GetProperty("Get Tag Count", "Poisoning DoT") + AdvCombatEffect_GetProperty("Get Tag Count", "Corroding DoT")
            if(iCount > 0) then
                iEffectCount = iEffectCount + 1
                iaEffectIDList[iEffectCount] = RO_GetID()
            end
        DL_PopActiveObject()
    end
    
    --Clean up.
    AdvCombat_SetProperty("Clear Temp Effects")
    
    --If there was at least one effect on the list to remove, add application packs for the removal.
    if(iEffectCount > 0) then
    
        local iAnimTicks = fnGetAnimationTiming("Healing")
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, giStoredTimer, "Play Sound|" .. gzRefAbility.zExecutionAbiPackage.sAttackSound)
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, giStoredTimer, "Create Animation|Healing|AttackAnim0")
        giStoredTimer = giStoredTimer + iAnimTicks
    
        AdvCombat_SetProperty("Register Application Pack", piOriginatorID, iTargetUniqueID, giStoredTimer, "Text|Cured!|Color:Green")
        for i = 1, iEffectCount, 1 do
            AdvCombat_SetProperty("Register Application Pack", piOriginatorID, iTargetUniqueID, giStoredTimer, "Remove Effect|" .. iaEffectIDList[i])
        end
        giStoredTimer = giStoredTimer + gciApplication_TextTicks
    end]=]

-- |[ ==================================== Standard Handler ==================================== ]|
--Call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
