-- |[ ==================================== Translucent Smile =================================== ]|
-- |[Description]|
--Inflicts Terrify damage to enemies, buffs allies (but not the user) with ATP.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.Maid.TranslucentSmile == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Maid", "Translucent Smile", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Debuff", "Active", "Mei|TranslucentSmile", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. All Enemies.\n[BaseHit].\n\n[FEffect]+10%%[Atk]/3[Turns], All Allies.\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Terrify your enemy with a ghostly grin!\nInflicts reduced [Tfy](Terrify) damage to\nthe enemy party.\nBuffs all allies [Atk](Power) for 3 turns.\n\nCosts [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(30, giNoCPCost, giNoCooldown, "Target All", 1)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Terrify")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Terrifying, 5, 0.75)
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Mei|Ability:TranslucentSmile"

    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.Maid.TranslucentSmile = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.Maid.TranslucentSmile

-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--When the player is selecting targets, a little box appears above the targets. This is the prediction
-- box. This script is called once for each target cluster created when this ability is selected, and
-- prediction boxes can be made for entities in that target cluster.
--The AdvCombatAbility will be the active object.
if(iSwitchType == gciAbility_BuildPredictionBox) then

    -- |[ ========================= Setup ======================== ]|
    --This routine create a package that contains all the default values needed to build a Prediction Box. We
    -- then change any values that aren't default.
    local sBoxName = "PredictionBox"
    AdvCombat_SetProperty("Prediction Create Box",              sBoxName)
    AdvCombat_SetProperty("Prediction Set Offsets",             sBoxName, 368, 0)
    AdvCombat_SetProperty("Prediction Allocate Strings",        sBoxName, 2)
    AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, 0, "To Enemies: [0.75x [IMG0]] as [IMG1]")
    AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, 0, 2)
    AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, 0, 0, gcfAbilityImgOffsetPredY, "Root/Images/AdventureUI/StatisticIcons/Attack")
    AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, 0, 1, gcfAbilityImgOffsetPredY, "Root/Images/AdventureUI/DamageTypeIcons/Terrifying")
    AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, 1, "To Allies: [IMG0]+10%%[IMG1] for 3[IMG2].")
    AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, 1, 3)
    AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, 1, 0, gcfAbilityImgOffsetPredY, "Root/Images/AdventureUI/EffectIndicator/EffectFriendly")
    AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, 1, 1, gcfAbilityImgOffsetPredY, "Root/Images/AdventureUI/StatisticIcons/Attack")
    AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, 1, 2, gcfAbilityImgOffsetPredY, "Root/Images/AdventureUI/StatisticIcons/Turns")

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[ ================= Originator Statistics ================ ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        fnModifyCP(1)
        fnModifyMP(-30)
    DL_PopActiveObject()
    
    -- |[ =================== Friendly Package =================== ]|
    --Create an ability package. This package comes with the default hit/miss values, animations, etc.
    local zaSupportPackage = fnConstructDefaultAbilityPackage("Buff")
    zaSupportPackage.bDoesNoDamage = true
    zaSupportPackage.bDoesNoStun = true
    zaSupportPackage.bDoesNotAnimate = false
    zaSupportPackage.bAlwaysHits = true
    zaSupportPackage.bAlwaysCrits = false
    zaSupportPackage.bNeverCrits = true
    zaSupportPackage.bEffectAlwaysApplies = true
    
    --Support Effect Package
    local zaSupportEffectPackage = fnConstructDefaultEffectPackage()
    zaSupportEffectPackage.iOriginatorID   = iOriginatorID
    zaSupportEffectPackage.iEffectType     = gciDamageType_Terrifying
    zaSupportEffectPackage.sEffectPath     = fnResolvePath() .. "../Effects/Translucent Smile.lua"
    zaSupportEffectPackage.sEffectCritPath = fnResolvePath() .. "../Effects/Translucent Smile.lua"
    zaSupportEffectPackage.sApplySound     = "Combat\\|Impact_Buff"
    zaSupportEffectPackage.sApplyText      = "Attack Up!"
    zaSupportEffectPackage.sApplyTextCol   = "Color:Blue"
    zaSupportEffectPackage.sResistText     = "Resisted!"
    zaSupportEffectPackage.sResistTextCol  = "Color:White"
    
    -- |[ ==================== For Each Target =================== ]|
    --Party of the originator.
    local iOriginatorParty = AdvCombat_GetProperty("Party Of ID", iOriginatorID)
    
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        
        --Get party of target.
        AdvCombat_SetProperty("Push Target", i-1) 
            local iTargetUniqueID = RO_GetID()
        DL_PopActiveObject()
        local iTargetParty = AdvCombat_GetProperty("Party Of ID", iTargetUniqueID)
        
        --Originator. Is ignored.
        if(iTargetUniqueID == iOriginatorID) then
            fnStandardExecution(iOriginatorID, i-1, zaSupportPackage, zaSupportEffectPackage)
        
        --Friendly:
        elseif(iOriginatorParty == iTargetParty) then
            fnStandardExecution(iOriginatorID, i-1, zaSupportPackage, zaSupportEffectPackage)
            
        --Hostile:
        else
            fnStandardExecution(iOriginatorID, i-1, gzRefAbility.zExecutionAbiPackage, nil)
        end
    end

-- |[ ==================================== Standard Handler ==================================== ]|
--Standard handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
