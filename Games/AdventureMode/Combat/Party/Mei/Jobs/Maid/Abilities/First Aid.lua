-- |[ ======================================== First Aid ======================================= ]|
-- |[Description]|
--Restores HP based on ATP, +25% if target is bleeding and removes bleeds.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.Maid.FirstAid == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Maid", "First Aid", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Heal", "Active", "Mei|FirstAid", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Restores [0.50x[Atk]][Hlt]. [Target].\nAdditional [0.25x [Atk]][Hlt] per [Bld]DoT on target.\nRemoves all [Bld]DoTs.\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Heals a single ally, removes [Bld](Bleed) DoTs.\nHeals more for each DoT removed.\n\n\n\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(10, giNoCPCost, giNoCooldown, "Target Allies Single", 1)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    --Note: Ability computes healing based on number of bleeds present, so these values are zeroes on purpose.
    zAbiStruct:fnCreateAbiPack("Healing")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsHealing(piHealingBase, pfHealingScaleFactor, pfHealingPctOfHPMax)
    zAbiStruct.zExecutionAbiPackage:fnSetAsHealing(1, 0, 0)
    
    --Additional Paths
    zAbiStruct.zExecutionAbiPackage.saExecPaths = {LM_GetCallStack(0)}
    zAbiStruct.zExecutionAbiPackage.iaExecCodes = {gciAbility_SpecialStart}
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Mei|Restore"

    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.Maid.FirstAid = zAbiStruct
end
    
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.Maid.FirstAid

-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--Prediction box updates based on bleed count.
if(iSwitchType == gciAbility_BuildPredictionBox) then
    
    -- |[ ===================== Other Values ===================== ]|
    --Get originator ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
        local iAttackPower = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
    DL_PopActiveObject()
    
    --Get information about the cluster. This is needed to give each box a unique ID.
    local iClusterID = AdvCombat_GetProperty("Prediction Get Cluster ID")
    
    -- |[ ==================== For Each Target =================== ]|
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    for i = 0, iClusterTargetsTotal-1, 1 do
        
        --Target ID.
        local iTargetID = AdvCombat_GetProperty("Prediction Get Targets ID", i)
        
        --Get how many times the [Bleeding DoT] tag appears on the entity.
        AdvCombat_SetProperty("Push Entity By ID", iTargetID)
            local iTagCount = AdvCombatEntity_GetProperty("Tag Count", "Bleeding DoT")
        DL_PopActiveObject()
        
        --Set the power of the module.
        gzRefAbility.zPredictionPackage.zHealingModule.iHealingFixed = math.floor((0.50 + (0.25 * iTagCount)) * iAttackPower)
        
        --Build the box!
        fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, gzRefAbility.zPredictionPackage)
    end

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability. Updates ability power based on bleed count.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[ ================ Originator Statistics ================= ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        local iAttackPower = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
        fnModifyCP(1)
        fnModifyMP(-10)
    DL_PopActiveObject()
    
    -- |[ =================== Ability Package ==================== ]|
    --Store the originator ID.
    gzRefAbility.zExecutionAbiPackage.iOriginatorID = iOriginatorID
    
    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        
        --Target ID. Get how many times the [Bleeding DoT] tag appears on the entity.
        AdvCombat_SetProperty("Push Target", piTargetIndex) 
            local iTargetID = RO_GetID()
            local iTagCount = AdvCombatEntity_GetProperty("Tag Count", "Bleeding DoT")
        DL_PopActiveObject()
        
        --Set the power of the module.
        gzRefAbility.zExecutionAbiPackage.iHealingBase = math.floor((0.50 + (0.25 * iTagCount)) * iAttackPower)
        
        --Execute.
        fnStandardExecution(iOriginatorID, i-1, gzRefAbility.zExecutionAbiPackage, nil)
    end

-- |[ ========================== Combat: Special Remove Bleeding DoTs ========================== ]|
--Clear any DoTs that have the matching tag on the target.
elseif(iSwitchType == gciAbility_SpecialStart) then

    --Argument handling.
    if(iArgumentsTotal < 3) then return end
    local iOriginatorID   = LM_GetScriptArgument(1, "N")
    local iTargetUniqueID = LM_GetScriptArgument(2, "N")
    
    --Setup.
    local iEffectCount = 0
    local iaEffectIDList = {}
    
    --Get a list of all effects referencing the target. Remove any with the matching tag.
    local iEffectsTotal = AdvCombat_SetProperty("Store Effects Referencing", iTargetUniqueID)
    for i = 0, iEffectsTotal-1, 1 do
        AdvCombat_SetProperty("Push Temp Effects", i)
        
            --Tag is present:
            local iCount = AdvCombatEffect_GetProperty("Get Tag Count", "Bleeding DoT")
            if(iCount > 0) then
                iEffectCount = iEffectCount + 1
                iaEffectIDList[iEffectCount] = RO_GetID()
            end
        DL_PopActiveObject()
    end
    
    --Clean up.
    AdvCombat_SetProperty("Clear Temp Effects")
    
    --If there was at least one effect on the list to remove, add application packs for the removal.
    if(iEffectCount > 0) then
    
        local iAnimTicks = fnGetAnimationTiming("Healing")
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, giStoredTimer, "Play Sound|Combat\\|Heal")
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, giStoredTimer, "Create Animation|Healing|AttackAnim0")
        giStoredTimer = giStoredTimer + iAnimTicks
    
        AdvCombat_SetProperty("Register Application Pack", piOriginatorID, iTargetUniqueID, giStoredTimer, "Text|Cured!|Color:Green")
        for i = 1, iEffectCount, 1 do
            AdvCombat_SetProperty("Register Application Pack", piOriginatorID, iTargetUniqueID, giStoredTimer, "Remove Effect|" .. iaEffectIDList[i])
        end
        giStoredTimer = giStoredTimer + gciApplication_TextTicks
    end
    
-- |[ ==================================== Standard Handler ==================================== ]|
--Call standard handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
