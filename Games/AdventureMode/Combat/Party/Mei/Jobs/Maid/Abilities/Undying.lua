-- |[ ========================================= Undying ======================================== ]|
-- |[Description]|
--Passive, adds +3 to all resistances when below 50% HP.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.Maid.Undying == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Maid", "Undying", "$SkillName", gciJP_Cost_Passive, gbIsNotFreeAction, "Buff", "Passive", "Mei|Undying", "Passive")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "+3 All Resists when under 50%%[Hlt]\n\n\n\n\n\nPassive."
    zAbiStruct.sSimpleDescMarkdown  = "When below half health, all resists increase.\n\n\n\n\nPassive."
    
    -- |[Passive Effect]|
    --Does not use a prototype, as the effect script modifies the effect dynamically.
    zAbiStruct.bNeverAvailable   = true --Passive abilities have no usability requirements.
    zAbiStruct.sPassivePath      = fnResolvePath() .. "../Effects/Undying.lua"
    zAbiStruct.bHasGUIEffects    = true
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.Maid.Undying = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.Maid.Undying

--Call standard.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
