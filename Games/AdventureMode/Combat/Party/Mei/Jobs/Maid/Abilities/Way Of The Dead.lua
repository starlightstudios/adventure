-- |[ ===================================== Way of the Dead ==================================== ]|
-- |[Description]|
--Passive, increases Slashing/Piercing/Striking resist by 3.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.Maid.WayOfTheDead == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Maid", "Way Of The Dead", "Way of the Dead", gciJP_Cost_Passive, gbIsNotFreeAction, "Buff", "Passive", "Mei|WayOfTheDead", "Passive")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "+3 [Prc]/[Slsh]/[Stk] resistance.\n\n\n\n\n\nPassive."
    zAbiStruct.sSimpleDescMarkdown  = "Increases resistance to physical damage types.\n[Prc](Pierce), [Slsh](Slash), and [Stk](Strike).\n\n\n\nPassive."
    
    -- |[Passive Effect]|
    zAbiStruct.bNeverAvailable   = true --Passive abilities have no usability requirements.
    zAbiStruct.sPassivePrototype = "Mei.Maid.WayOfTheDead"
    zAbiStruct.bHasGUIEffects    = true

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    if(EffectList:fnEntryExists(zAbiStruct.sPassivePrototype) == false) then
        local saDescription = {}
        saDescription[1] = "Your dissociated ghostly body is resistant to"
        saDescription[2] = "physical attacks. Increased [Stk][Slsh][Prc] resist."
      --EffectList:fnCreatePassivePrototype(psName, psDisplayName, psIcon, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreatePassivePrototype(zAbiStruct.sPassivePrototype, "Way of the Dead", "Mei|WayOfTheDead", "ResSls|Flat|3 ResStk|Flat|3 ResPrc|Flat|3", saDescription)
    end

    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.Maid.WayOfTheDead = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.Maid.WayOfTheDead

-- |[ ===================================== All Other Cases ==================================== ]|
if(iSwitchType ~= gciAbility_GUIApplyEffect) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

-- |[ ==================================== GUI: Apply Effect =================================== ]|
--When showing a character's statistics, all equipped abilities are queried. This is not reflected
-- in combat, this is GUI only.
--The standard does not handle this.
elseif(iSwitchType == gciAbility_GUIApplyEffect) then
    AdvCombatAbility_SetProperty("Push Owner")
        fnApplyStatBonus(gciStatIndex_Resist_Slash,  3)
        fnApplyStatBonus(gciStatIndex_Resist_Strike, 3)
        fnApplyStatBonus(gciStatIndex_Resist_Pierce, 3)
    DL_PopActiveObject()
    gzRefAbility = nil
end
