-- |[ ======================================= Wraithform ======================================= ]|
-- |[Description]|
--Field ability, makes the party invisible for a while.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Ability Prototype =================================== ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.Maid.Wraithform == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Maid", "Wraithform", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Special", "Mei|Undying", "Unequippable")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Unlocks the Wraithform field ability, turning you invisible\nto enemies for a short time."
    zAbiStruct.sSimpleDescMarkdown  = "Unlocks the Wraithform field ability.\nThis ability turns you invisible. Enemies will\nignore you.\n\nField abilities can be seen in the top left corner\nwhen out of battle."

    -- |[Usability Variables]|
    zAbiStruct.bNeverAvailable = true --Field abilities have no usability requirements.
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.Maid.Wraithform = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.Maid.Wraithform

--Call standard.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
