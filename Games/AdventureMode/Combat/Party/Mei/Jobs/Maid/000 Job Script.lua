-- |[ ========================================== Maid ========================================== ]|
--The Ghost class.

-- |[ ========== Argument Handling =========== ]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--1st argument is only used for level-up calls.
local iLevelReached = 0
if(iArgumentsTotal >= 2) then
    iLevelReached = LM_GetScriptArgument(1, "N")
end

-- |[ ============ Variable Setup ============ ]|
-- |[Enumerations]|
local gciNoSkillbooks = 0

-- |[Encapsulation]|
zEntry = {}

-- |[Basic Variables]|
--Character Variables.
zEntry.sCharacterFormVarPath = "Root/Variables/Global/Mei/sForm"
zEntry.sSkillbookVarPath     = "Root/Variables/Global/Mei/iSkillbookTotal"
zEntry.zaJobChart            = gzaMeiJobChart
zEntry.sStatProfilePath      = gsRoot .. "Combat/Party/Mei/400 Common Stat Profile.lua"
zEntry.sSecondaryMenuPath    = gsRoot .. "Combat/Party/Mei/200 Build Secondary Menu.lua"
zEntry.sMasterBonusPath      = gsRoot .. "Combat/Party/Mei/401 Mastery Bonus Handler.lua"

--Class Variables.
zEntry.sClassName   = "Maid"
zEntry.sFormName    = "Ghost"
zEntry.sCostumeName = "Mei_Ghost"
zEntry.fFaceIndexX  = 3
zEntry.fFaceIndexY  = 0

-- |[Ability Variables]|
--Purchaseable abilities.
zEntry.saExternalAbilities = {}
table.insert(zEntry.saExternalAbilities, "Duster Blast")
table.insert(zEntry.saExternalAbilities, "First Aid")
table.insert(zEntry.saExternalAbilities, "Icy Hand")
table.insert(zEntry.saExternalAbilities, "Tidy Up")
table.insert(zEntry.saExternalAbilities, "Translucent Smile")
table.insert(zEntry.saExternalAbilities, "Undying")
table.insert(zEntry.saExternalAbilities, "Way Of The Dead")
table.insert(zEntry.saExternalAbilities, "Freezing Blade")
table.insert(zEntry.saExternalAbilities, "Wraithform")

--Class-equipped abilities.
zEntry.saInternalAbilities = {}
table.insert(zEntry.saInternalAbilities, "Duster Blast")
table.insert(zEntry.saInternalAbilities, "Icy Hand")
table.insert(zEntry.saInternalAbilities, "First Aid")
table.insert(zEntry.saInternalAbilities, "Tidy Up")
table.insert(zEntry.saInternalAbilities, "Translucent Smile")
table.insert(zEntry.saInternalAbilities, "Undying")
table.insert(zEntry.saInternalAbilities, "Freezing Blade")

--Purchaseable abilities, in the order of purchase.
zEntry.saPurchaseAbilities = {}
table.insert(zEntry.saPurchaseAbilities, "Maid|Icy Hand")
table.insert(zEntry.saPurchaseAbilities, "Maid|Translucent Smile")
table.insert(zEntry.saPurchaseAbilities, "Maid|Duster Blast")
table.insert(zEntry.saPurchaseAbilities, "Maid|First Aid")
table.insert(zEntry.saPurchaseAbilities, "Maid|Tidy Up")
table.insert(zEntry.saPurchaseAbilities, "Maid|Freezing Blade")
table.insert(zEntry.saPurchaseAbilities, "Maid|Undying")
table.insert(zEntry.saPurchaseAbilities, "Maid|Way Of The Dead")
table.insert(zEntry.saPurchaseAbilities, "Maid|Wraithform")

-- |[Skillbar Variables]|
--Skills that always appear on the skillbar. Format: {sAbilityName, iXPos, iYPos, iSkillbooks} (eg: {"Common|Attack", 0, 0, 0}, {"Common|Parry", 1, 0, 0} {})
zEntry.zaSkillbarAbilities = {}
table.insert(zEntry.zaSkillbarAbilities, {"Common|Attack",                   0, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Common|Parry",                    1, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Maid Internal|Freezing Blade",    2, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Maid Internal|Duster Blast",      0, 1, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Maid Internal|Icy Hand",          1, 1, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Maid Internal|First Aid",         2, 1, 1})
table.insert(zEntry.zaSkillbarAbilities, {"Maid Internal|Tidy Up",           0, 2, 2})
table.insert(zEntry.zaSkillbarAbilities, {"Maid Internal|Translucent Smile", 1, 2, 3})
table.insert(zEntry.zaSkillbarAbilities, {"Maid Internal|Undying",           2, 2, 4})

-- |[ ============ Call Standard ============= ]|
--Add to the global stack. There may be multiple executions of this script.
table.insert(gzJobCallStack, zEntry)

--Call.
LM_ExecuteScript(gsStandardJobPath, iSwitchType, iLevelReached)

--Pop the global stack.
table.remove(gzJobCallStack)
