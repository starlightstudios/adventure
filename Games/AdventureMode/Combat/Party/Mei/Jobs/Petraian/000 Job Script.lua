-- |[ ======================================== Petraian ======================================== ]|
--The Gravemarker class.

-- |[ ========== Argument Handling =========== ]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--1st argument is only used for level-up calls.
local iLevelReached = 0
if(iArgumentsTotal >= 2) then
    iLevelReached = LM_GetScriptArgument(1, "N")
end

-- |[ ============ Variable Setup ============ ]|
-- |[Enumerations]|
local gciNoSkillbooks = 0

-- |[Encapsulation]|
zEntry = {}

-- |[Basic Variables]|
--Character Variables.
zEntry.sCharacterFormVarPath = "Root/Variables/Global/Mei/sForm"
zEntry.sSkillbookVarPath     = "Root/Variables/Global/Mei/iSkillbookTotal"
zEntry.zaJobChart            = gzaMeiJobChart
zEntry.sStatProfilePath      = gsRoot .. "Combat/Party/Mei/400 Common Stat Profile.lua"
zEntry.sSecondaryMenuPath    = gsRoot .. "Combat/Party/Mei/200 Build Secondary Menu.lua"
zEntry.sMasterBonusPath      = gsRoot .. "Combat/Party/Mei/401 Mastery Bonus Handler.lua"

--Class Variables.
zEntry.sClassName   = "Petraian"
zEntry.sFormName    = "Gravemarker"
zEntry.sCostumeName = "Mei_Gravemarker"
zEntry.fFaceIndexX  = 6
zEntry.fFaceIndexY  = 0

-- |[Ability Variables]|
--Purchaseable abilities.
zEntry.saExternalAbilities = {}
table.insert(zEntry.saExternalAbilities, "Bodyslam")
table.insert(zEntry.saExternalAbilities, "Flutter")
table.insert(zEntry.saExternalAbilities, "Holy Flame")
table.insert(zEntry.saExternalAbilities, "Piercing Light")
table.insert(zEntry.saExternalAbilities, "Soothing Light")
table.insert(zEntry.saExternalAbilities, "Indomitable")
table.insert(zEntry.saExternalAbilities, "Stoneskin")
table.insert(zEntry.saExternalAbilities, "Holy Inferno")
table.insert(zEntry.saExternalAbilities, "Way Of The Holy")

--Class-equipped abilities.
zEntry.saInternalAbilities = {}
table.insert(zEntry.saInternalAbilities, "Bodyslam")
table.insert(zEntry.saInternalAbilities, "Holy Flame")
table.insert(zEntry.saInternalAbilities, "Soothing Light")
table.insert(zEntry.saInternalAbilities, "Piercing Light")
table.insert(zEntry.saInternalAbilities, "Flutter")
table.insert(zEntry.saInternalAbilities, "Stoneskin")
table.insert(zEntry.saInternalAbilities, "Holy Inferno")

--Purchaseable abilities, in the order of purchase.
zEntry.saPurchaseAbilities = {}
table.insert(zEntry.saPurchaseAbilities, "Petraian|Bodyslam")
table.insert(zEntry.saPurchaseAbilities, "Petraian|Holy Flame")
table.insert(zEntry.saPurchaseAbilities, "Petraian|Piercing Light")
table.insert(zEntry.saPurchaseAbilities, "Petraian|Soothing Light")
table.insert(zEntry.saPurchaseAbilities, "Petraian|Flutter")
table.insert(zEntry.saPurchaseAbilities, "Petraian|Holy Inferno")
table.insert(zEntry.saPurchaseAbilities, "Petraian|Indomitable")
table.insert(zEntry.saPurchaseAbilities, "Petraian|Stoneskin")
table.insert(zEntry.saPurchaseAbilities, "Petraian|Way Of The Holy")

-- |[Skillbar Variables]|
--Skills that always appear on the skillbar. Format: {sAbilityName, iXPos, iYPos, iSkillbooks} (eg: {"Common|Attack", 0, 0, 0}, {"Common|Parry", 1, 0, 0} {})
zEntry.zaSkillbarAbilities = {}
table.insert(zEntry.zaSkillbarAbilities, {"Common|Attack",                    0, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Common|Parry",                     1, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Petraian Internal|Holy Inferno",   2, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Petraian Internal|Bodyslam",       0, 1, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Petraian Internal|Holy Flame",     1, 1, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Petraian Internal|Soothing Light", 2, 1, 1})
table.insert(zEntry.zaSkillbarAbilities, {"Petraian Internal|Piercing Light", 0, 2, 2})
table.insert(zEntry.zaSkillbarAbilities, {"Petraian Internal|Flutter",        1, 2, 3})
table.insert(zEntry.zaSkillbarAbilities, {"Petraian Internal|Stoneskin",      2, 2, 4})

-- |[ ============ Call Standard ============= ]|
--Add to the global stack. There may be multiple executions of this script.
table.insert(gzJobCallStack, zEntry)

--Call.
LM_ExecuteScript(gsStandardJobPath, iSwitchType, iLevelReached)

--Pop the global stack.
table.remove(gzJobCallStack)
