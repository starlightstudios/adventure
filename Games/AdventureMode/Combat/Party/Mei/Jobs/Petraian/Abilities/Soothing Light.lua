-- |[ ===================================== Soothing Light ===================================== ]|
-- |[Description]|
--Performs a heal-over-time to one ally.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.Petraian.SoothingLight == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Petraian", "Soothing Light", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Heal", "Active", "Mei|SoothingLight", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Restores [0.20x [Atk] + 30][Hlt] over 3 turns. [Target].\n\n\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Treat your injured allies with your pure soul.\nHeals over 3 turns, increase by [Atk](Power).\n\n\n\nCosts [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(20, giNoCPCost, giNoCooldown, "Target Allies All", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Healing")
    zAbiStruct.zExecutionAbiPackage:fnSetAsBuff()
    
    --Cannot crit.
    zAbiStruct.zExecutionAbiPackage.bNeverCrits = true
    
    -- |[Execution Effect Package]|
  --zAbiStruct:fnAddBuff(psPrototypeName, $psApplyText, $psApplyColorString, $psApplyAnim, $psApplySound)
    zAbiStruct:fnAddBuff("Mei.Petraian.SoothingLight")

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
      --EffectList:fnCreateHoTPrototype(psName, psDisplayName, psIcon, pfHealBase, pfHealFactor, piTurns)
        EffectList:fnCreateHoTPrototype(sLocalPrototypeName, "Soothing Light", "Mei|SoothingLight", 30, 0.20, 3)
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Mei|Restore"

    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.Petraian.SoothingLight = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.Petraian.SoothingLight

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
