-- |[ ========================================= Stalker ======================================== ]|
--The Mannequin class.

-- |[ ========== Argument Handling =========== ]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--1st argument is only used for level-up calls.
local iLevelReached = 0
if(iArgumentsTotal >= 2) then
    iLevelReached = LM_GetScriptArgument(1, "N")
end

-- |[ ============ Variable Setup ============ ]|
-- |[Enumerations]|
local gciNoSkillbooks = 0

-- |[Encapsulation]|
local zEntry = {}

-- |[Basic Variables]|
--Character Variables.
zEntry.sCharacterFormVarPath = "Root/Variables/Global/Mei/sForm"
zEntry.sSkillbookVarPath     = "Root/Variables/Global/Mei/iSkillbookTotal"
zEntry.zaJobChart            = gzaMeiJobChart
zEntry.sStatProfilePath      = gsRoot .. "Combat/Party/Mei/400 Common Stat Profile.lua"
zEntry.sSecondaryMenuPath    = gsRoot .. "Combat/Party/Mei/200 Build Secondary Menu.lua"
zEntry.sMasterBonusPath      = gsRoot .. "Combat/Party/Mei/401 Mastery Bonus Handler.lua"

--Class Variables.
zEntry.sClassName   = "Stalker"
zEntry.sFormName    = "Mannequin"
zEntry.sCostumeName = "Mei_Mannequin"
zEntry.fFaceIndexX  = 9
zEntry.fFaceIndexY  = 0

-- |[Ability Variables]|
--Purchaseable abilities.
zEntry.saExternalAbilities = {}
table.insert(zEntry.saExternalAbilities, "Appear Lifeless")
table.insert(zEntry.saExternalAbilities, "No Restraints")
table.insert(zEntry.saExternalAbilities, "Relentless")
table.insert(zEntry.saExternalAbilities, "Resin Skin")
table.insert(zEntry.saExternalAbilities, "Strangle")
table.insert(zEntry.saExternalAbilities, "Unliving Grasp")
table.insert(zEntry.saExternalAbilities, "Unsettling Moves")
table.insert(zEntry.saExternalAbilities, "Way Of The Faceless")
table.insert(zEntry.saExternalAbilities, "You Must Leave")

--Class-equipped abilities.
zEntry.saInternalAbilities = {}
table.insert(zEntry.saInternalAbilities, "Appear Lifeless")
table.insert(zEntry.saInternalAbilities, "No Restraints")
table.insert(zEntry.saInternalAbilities, "Relentless")
table.insert(zEntry.saInternalAbilities, "Strangle")
table.insert(zEntry.saInternalAbilities, "Unliving Grasp")
table.insert(zEntry.saInternalAbilities, "Unsettling Moves")
table.insert(zEntry.saInternalAbilities, "You Must Leave")

--Purchaseable abilities, in the order of purchase.
zEntry.saPurchaseAbilities = {}
table.insert(zEntry.saPurchaseAbilities, "Stalker|Strangle")
table.insert(zEntry.saPurchaseAbilities, "Stalker|You Must Leave")
table.insert(zEntry.saPurchaseAbilities, "Stalker|Unliving Grasp")
table.insert(zEntry.saPurchaseAbilities, "Stalker|Relentless")
table.insert(zEntry.saPurchaseAbilities, "Stalker|Appear Lifeless")
table.insert(zEntry.saPurchaseAbilities, "Stalker|Unsettling Moves")
table.insert(zEntry.saPurchaseAbilities, "Stalker|No Restraints")
table.insert(zEntry.saPurchaseAbilities, "Stalker|Resin Skin")
table.insert(zEntry.saPurchaseAbilities, "Stalker|Way Of The Faceless")

-- |[Skillbar Variables]|
--Skills that always appear on the skillbar. Format: {sAbilityName, iXPos, iYPos, iSkillbooks} (eg: {"Common|Attack", 0, 0, 0}, {"Common|Parry", 1, 0, 0} {})
zEntry.zaSkillbarAbilities = {}
table.insert(zEntry.zaSkillbarAbilities, {"Common|Attack",                     0, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Common|Parry",                      1, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Stalker Internal|No Restraints",    2, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Stalker Internal|Strangle",         0, 1, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Stalker Internal|Relentless",       1, 1, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Stalker Internal|Unliving Grasp",   2, 1, 1})
table.insert(zEntry.zaSkillbarAbilities, {"Stalker Internal|You Must Leave",   0, 2, 2})
table.insert(zEntry.zaSkillbarAbilities, {"Stalker Internal|Appear Lifeless",  1, 2, 3})
table.insert(zEntry.zaSkillbarAbilities, {"Stalker Internal|Unsettling Moves", 2, 2, 4})

-- |[ ============ Call Standard ============= ]|
--Add to the global stack. There may be multiple executions of this script.
table.insert(gzJobCallStack, zEntry)

--Call.
LM_ExecuteScript(gsStandardJobPath, iSwitchType, iLevelReached)

--Pop the global stack.
table.remove(gzJobCallStack)
