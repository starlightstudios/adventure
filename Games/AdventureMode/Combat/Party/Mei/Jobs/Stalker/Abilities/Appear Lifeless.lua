-- |[ ==================================== Appear Lifeless ===================================== ]|
-- |[Description]|
--Zeroes threat values against user, free action.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.Stalker.AppearLifeless == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Stalker", "Appear Lifeless", "Use Skill Name", gciJP_Cost_Normal, gbIsFreeAction, "Buff", "Active", "Mei|Stalker|AppearLifeless", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Zeroes all threat against user.\n\n\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Mimic a real mannequin and become utterly[BR]lifeless.\nEnemies will lose all threat against you.\nFree Action.\n\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(10, giNoCPCost, giNoCooldown, "Target Self", giNoCPGeneration)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Buff")
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.Stalker.AppearLifeless = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.Stalker.AppearLifeless

-- |[ ===================================== Combat: Execute ==================================== ]|
--During execution, scans all threat effects for this entity and zeroes the associated value.
if(iSwitchType == gciAbility_Execute) then
    
    --Call the standard.
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
    
    --Get user ID.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
    DL_PopActiveObject()
    
    --For each enemy in battle:
    local iEnemyPartySize = AdvCombat_GetProperty("Enemy Party Size")
    for i = 0, iEnemyPartySize-1, 1 do
        
        --Get the enemy ID.
        local iEnemyID = AdvCombat_GetProperty("Enemy ID", i)
        
        --Check if this enemy has a threat variable focused on the ability user. If so, zero it.
        local iCurThreat = VM_GetVar("Root/Variables/Combat/" .. iEnemyID .. "_AI/" .. iOriginatorID.. "_Threat", "N")
        if(iCurThreat > 0) then
            VM_SetVar("Root/Variables/Combat/" .. iEnemyID .. "_AI/" .. iOriginatorID.. "_Threat", "N", 0)
        end
    end

-- |[ ==================================== All Other Cases ===================================== ]|
--Call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
