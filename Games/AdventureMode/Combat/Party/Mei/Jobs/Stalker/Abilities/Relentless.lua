-- |[ ======================================= Relentless ======================================= ]|
-- |[Description]|
--Does normal damage and costs 5 MP, but restores some HP.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.Stalker.Relentless == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Stalker", "Relentless", "Use Skill Name", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "Mei|Stalker|Relentless", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\nRecover 10[PCT][Hlt] on use.\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Press forward, shrugging off anything they[BR]throw at you.\nDeals normal weapon damage, but recovers 10[PCT][BR]of your [Hlt].\n\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(5, giNoCPCost, giNoCooldown, "Target Enemies Single", 1)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Weapon")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_UseWeapon, 5, 1.00)
  --zAbiStruct.zExecutionAbiPackage:fnAddSelfHeal(piHealingBase, pfHealingScaleFactor, pfHealPctOfMaxHP)
    zAbiStruct.zExecutionAbiPackage:fnAddSelfHeal(0, 0, 0.10)
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.Stalker.Relentless = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.Stalker.Relentless

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
