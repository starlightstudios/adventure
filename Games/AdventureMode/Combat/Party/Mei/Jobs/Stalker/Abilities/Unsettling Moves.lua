-- |[ ==================================== Unsettling Moves ==================================== ]|
-- |[Description]|
--Increases evade by 100 for next turn. Free action. 3 turn cooldown.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ =================================== Ability Prototype ==================================== ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.Stalker.UnsettlingMoves == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Stalker", "Unsettling Moves", "Use Skill Name", gciJP_Cost_Normal, gbIsFreeAction, "Buff", "Active", "Mei|Stalker|UnsettlingMoves", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[FEffect] +100[Evd] for 3[Turns].\n3[Turns] cooldown.\n\n\n\nFree Action.\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "If they blink, you can move anywhere. Even[BR]behind them.\nGreatly increase [Evd](Evade) for 3 turns.\n3 turncooldown.\nFree Action.\nCosts [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(20, giNoCPCost, 4, "Target Self", giNoCPGeneration)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Buff")
    zAbiStruct.zExecutionAbiPackage:fnSetAsBuff()
  --zAbiStruct.zExecutionAbiPackage:fnAddAnimation(psAnimationName, piTimerOffset, $pfXOffset, $pfYOffset)
    zAbiStruct.zExecutionAbiPackage:fnAddAnimation("Buff Evade", 0)
    
    -- |[Effect Package]|
  --zAbiStruct:fnAddBuff(psPrototypeName, $psApplyText, $psApplyColorString, $psApplyAnim, $psApplySound)
    zAbiStruct:fnAddBuff("Mei.Stalker.UnsettlingMoves")

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
        local saDescription = {}
        saDescription[1] = "When they blink, move at an alarming speed."
        saDescription[2] = "Greatly increases [Evd](Evade)."
      --EffectList:fnCreateBuffPrototype(psName, psDisplayName, psIcon, piTurns, psStatString, psaDescription, pzaTagList)
        EffectList:fnCreateBuffPrototype(sLocalPrototypeName, "Unsettling Moves", "Mei|Stalker|UnsettlingMoves", 3, "Evd|Flat|100", saDescription)
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.Stalker.UnsettlingMoves = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.Stalker.UnsettlingMoves

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
