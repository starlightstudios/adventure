-- |[ ======================================== Strangle ======================================== ]|
-- |[Description]|
--Does a lot of strike damage, but can't be used on certain enemies that don't have a neck, or
-- don't breathe.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.Stalker.Strangle == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Stalker", "Strangle", "Use Skill Name", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "Mei|Stalker|Strangle", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\nDoes not work on certain enemies.\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Mercilessly choke the life out of an enemy.\nDeals major striking damage, but certain enemies[BR]cannot be strangled.\n\n\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(20, giNoCPCost, giNoCooldown, "Target Enemies Single", 1)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Strike")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Striking, 5, 2.00)
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.Stalker.Strangle = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.Stalker.Strangle

-- |[ ================================= Combat: Paint Targets ================================== ]|
--For player-controlled characters, selects which targets can be used by this ability. AI-controlled
-- characters may optionally bypass this and select targets directly.
--If no targets are painted, the UI will play a fail sound and disable target selection.
if(iSwitchType == gciAbility_PaintTargets) then
    
    --Run macro.
    AdvCombat_SetProperty("Run Target Macro", gzRefAbility.sTargetMacro, AdvCombatAbility_GetProperty("Owner ID"))

    --Scan all clusters for "Cannot Strangle" tag. Remove all targets/clusters that have it.
    fnTrimTargetsWithTag({"Cannot Strangle"})

-- |[ ==================================== All Other Cases ===================================== ]|
--Call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
