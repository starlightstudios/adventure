-- |[ ================================== Way of the Faceless =================================== ]|
-- |[Description]|
--Passive, increases Bleed and Terrify damage by 15%.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.Stalker.WayOfTheFaceless == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Stalker", "Way Of The Faceless", "Way of the Faceless", gciJP_Cost_Passive, gbIsNotFreeAction, "Buff", "Passive", "Mei|Stalker|WayOfTheFaceless", "Passive")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "+15[PCT][Bld], +15[PCT][Tfy] damage.\n\n\n\n\n\nPassive."
    zAbiStruct.sSimpleDescMarkdown  = "Increases [Bld](Bleed) and [Tfy](Terrify) damage[BR]by 15[PCT].\n\n\n\nPassive."
    
    -- |[Passive Effect]|
    zAbiStruct.bNeverAvailable   = true --Passive abilities have no usability requirements.
    zAbiStruct.sPassivePrototype = "Mei.Stalker.WayOfTheFaceless"
    zAbiStruct.bHasGUIEffects    = true
    
    -- |[Tags]|
    --Apply tags to increase bleed/terrify damage.
    zAbiStruct.zaTags = {{"Bleed Damage Dealt +", 15}, {"Terrify Damage Dealt +", 15}}

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    -- Note: The effect doesn't increase damage, the ability itself does.
    if(EffectList:fnEntryExists(zAbiStruct.sPassivePrototype) == false) then
        local saDescription = {}
        saDescription[1] = "Embrace your inhumanity, your lifeless visage."
        saDescription[2] = "Increases [Bld][Tfy] damage by 15[PCT]."
      --EffectList:fnCreatePassivePrototype(psName, psDisplayName, psIcon, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreatePassivePrototype(zAbiStruct.sPassivePrototype, "Way of the Faceless", "Mei|Stalker|WayOfTheFaceless", "", saDescription)
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.Stalker.WayOfTheFaceless = zAbiStruct
end

-- |[ ======================================= Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.Stalker.WayOfTheFaceless

--Call.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
