-- |[ ===================================== No Restraints ====================================== ]|
-- |[Description]|
--Inflict 1.60x damage to all enemies, leaves an additional 0.10x DoT for 3 turns for both bleed
-- and terrify damage. Total is 2.20x to all enemies.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.Stalker.NoRestraints == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Stalker", "No Restraints", "Use Skill Name", gciJP_Cost_Advanced, gbIsNotFreeAction, "DoT", "Combo", "Mei|Stalker|NoRestraints", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\n0.10x [Bld] and 0.10x [Tfy] DoT for 3 turns.\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Nothing holds you back. Obliterate them.\nDeals major weapon damage to all enemies,[BR]leaves a [Bld](Bleed) and [Tfy](Terrify) DoT.\n\n\nCosts [CPCost][CPIco](CP)."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, 6, giNoCooldown, "Target Enemies All", giNoCPGeneration)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Sword Slash")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_UseWeapon, 5, 1.60)
    
    -- |[Execution Effect Package]|
  --zAbiStruct:fnAddEffect(piStrNormal, piStrCrit, piApplyType, psApplyText, psCritApplyText, psApplyColorString, psaApplyBonus, psaApplyMalus, psaSeverityBonus, psaSeverityMalus)
    zAbiStruct:fnAddEffect(6, 9, gciDamageType_Bleeding,   "Bleeding!", "Badly Bleeding!", "Color:Red", {"Bleed Standard"})
    zAbiStruct:fnAddEffect(6, 9, gciDamageType_Terrifying, "Shaking!",  "Badly Shaking!",  "Color:Red", {"Terrify Standard"})
    
    -- |[Use of Effect Prototype]|
    zAbiStruct.zExecutionEffPackage.sPrototypeName  = "Mei.Stalker.NoRestraintsBleed"
    zAbiStruct.zExecutionEffPackageB.sPrototypeName = "Mei.Stalker.NoRestraintsTerrify"

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    if(EffectList:fnEntryExists(zAbiStruct.zExecutionEffPackage.sPrototypeName) == false) then
      --fnMakeAndRegisterDoTPrototype(psName, piTurns, piApplyStrength, pfDamPerTurn, piDamageType, psDisplayName, psIcon, piCritTurns, piCritApplyStrength)
        EffectList:fnCreateDoTPrototype("Mei.Stalker.NoRestraintsBleed",   3, 6, 0.10, gciDamageType_Bleeding,   "No Restraints (Bld)", "Mei|Stalker|NoRestraints", 5, 9)
        EffectList:fnCreateDoTPrototype("Mei.Stalker.NoRestraintsTerrify", 3, 6, 0.10, gciDamageType_Terrifying, "No Restraints (Tfy)", "Mei|Stalker|NoRestraints", 5, 9)
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.Stalker.NoRestraints = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.Stalker.NoRestraints

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
