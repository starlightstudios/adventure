-- |[ ===================================== Jobchange Maid ===================================== ]|
-- |[Description]|
--Changes jobs in combat to Maid.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Ability Prototype =================================== ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.Common.JobchangeMaid == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("JobChange", "Job Change - Maid", "$SkillName", gciJP_Cost_NoCost, gbIsFreeAction, "Buff", "Combo", "Mei|WayOfTheDead", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Transform into a Ghost Maid.\n\n\n\n\nFree Action.\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Transform into a Ghost Maid.\nCosts [CPCost][CPIco](CP).\nFree Action.\n\n\n\n"

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, 2, giNoCooldown, "Target Self", giNoCPGeneration)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = AbiExecPack:new("JobChange")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsJobChange(psJobName, $psExclusionName, $psVariablePath)
    zAbiStruct.zExecutionAbiPackage:fnSetAsJobChange("Maid", "Ghost", "Root/Variables/Global/Mei/sForm")
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Add a manual line to the prediction.
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "Transform to Ghost Maid."
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
    
    --Set prototype.
    gzPrototypes.Combat.Mei.Common.JobchangeMaid = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.Common.JobchangeMaid

--Job-change abilities have a standard path.
if(iSwitchType == gciAbility_Create or iSwitchType == gciAbility_CreateSpecial) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
else
    LM_ExecuteScript(gsStandardAbilityJobchangePath, iSwitchType)
end
