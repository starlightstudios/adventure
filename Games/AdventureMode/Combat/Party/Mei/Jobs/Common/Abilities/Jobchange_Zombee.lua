-- |[ ==================================== Jobchange  Zombee =================================== ]|
-- |[Description]|
--Changes jobs in combat to Zombee.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Ability Prototype =================================== ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.Common.JobchangeZombee == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("JobChange", "Job Change - Zombee", "$SkillName", gciJP_Cost_NoCost, gbIsFreeAction, "Buff", "Combo", "Mei|WayOfTheZombee", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Transform into a Zombee.[BR][BR][BR][BR][BR]Free Action.[BR][Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Transform into a Zombee.[BR]This powerful form can only be used in combat.[BR]Costs [CPCost][CPIco](CP).[BR]Free Action.[BR][BR][BR]"

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, 6, giNoCooldown, "Target Self", giNoCPGeneration)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = AbiExecPack:new("JobChange")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsJobChange(psJobName, $psExclusionName, $psVariablePath)
    zAbiStruct.zExecutionAbiPackage:fnSetAsJobChange("Zombee", "Zombee", "Root/Variables/Global/Mei/sForm")
    
    --Additional Paths
    zAbiStruct.zExecutionAbiPackage.saExecPaths = {LM_GetCallStack(0)}
    zAbiStruct.zExecutionAbiPackage.iaExecCodes = {gciAbility_SpecialLastExec}
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Add a manual line to the prediction.
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "Transform to Zombee."
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
    
    --Set prototype.
    gzPrototypes.Combat.Mei.Common.JobchangeZombee = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.Common.JobchangeZombee
    
-- |[ ============================ Combat: Possible Zombee Cutscene ============================ ]|
--If an enemy tagged with "Zombee Subvert" is present, fires a special cutscene.
if(iSwitchType == gciAbility_SpecialLastExec) then

    -- |[Scan For Tag]|
    --Scan all enemies for the tag "Zombee Subvert". If it is not found, do nothing. If it is found,
    -- fire a cutscene which kicks the party back to the last save point.
    local bGotHit = false

    --Scan all present enemies.
    local iEnemyPartySize = AdvCombat_GetProperty("Enemy Party Size")
    for i = 0, iEnemyPartySize-1, 1 do
        
        --Get ID.
        local iEnemyID = AdvCombat_GetProperty("Enemy ID", i)
        
        --Push.
        AdvCombat_SetProperty("Push Entity By ID", iEnemyID)
            local iTagCount = AdvCombatEntity_GetProperty("Tag Count", "Zombee Subvert")
        DL_PopActiveObject()
        
        --If tag is present, stop iterating.
        if(iTagCount > 0) then
            bGotHit = true
            break
        end
    end

    -- |[Tag Check]|
    --No tags found, stop.
    if(bGotHit == false) then return end
    
    -- |[Scene Execution]|
    --If we got this far, the tag was present. Fire this cutscene.
    LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Zombee Subversion/Scene.lua")

-- |[ ==================================== Standard Handler ==================================== ]|
--Creation calls the standard as it relies on the call stack to reference this script.
elseif(iSwitchType == gciAbility_Create or iSwitchType == gciAbility_CreateSpecial) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
    
--Job-change abilities have a standard path.
else
    LM_ExecuteScript(gsStandardAbilityJobchangePath, iSwitchType)
end

