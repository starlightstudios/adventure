-- |[ ====================================== Corrupt Honey ===================================== ]|
-- |[Description]|
--Restores 20% of max HP. Free Action.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Mei.Zombee.CorruptHoney == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Zombee", "Corrupt Honey", "$SkillName", gciJP_Cost_Normal, gbIsFreeAction, "Heal", "Active", "Mei|CorruptHoney", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Heals 20%% of Max [Hlt], Self.\n\n\n\nFree Action.\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "The corrupted honey nourishes you.\nHeals you, free action.\nCosts [MPCost][MPIco]."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(20, giNoCPCost, giNoCooldown, "Target Self", giNoCPGeneration)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Healing")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsHealing(piHealingBase, pfHealingScaleFactor, pfHealingPctOfHPMax)
    zAbiStruct.zExecutionAbiPackage:fnSetAsHealing(0, 0.00, 0.20)
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Mei|Ability:CorruptedHoney"

    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Mei.Zombee.CorruptHoney = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Mei.Zombee.CorruptHoney

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
