-- |[ ================================== Mei Skillbook Handler ================================== ]|
--Call this file with the number of the skillbook in question, and JP and unlocks will be awarded as needed.
if(fnArgCheck(1) == false) then return end
local iLevel = LM_GetScriptArgument(0, "I")

-- |[Mei is Not In the Party]|
if(AdvCombat_GetProperty("Is Member In Active Party", "Mei") == false) then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The Fencer's Friend.[P] Doesn't seem useful for anyone in the party right now.)") ]])
    fnCutsceneBlocker()
    return
end

-- |[Activate Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)

-- |[Common Code]|
--Check if this skillbook has been read already. If not, award JP and handle unlocks.
local iCheckVar = VM_GetVar("Root/Variables/Global/Mei/iSkillbook" .. iLevel, "N")
if(iCheckVar == 0.0) then
    
    --Mark the skillbook.
    VM_SetVar("Root/Variables/Global/Mei/iSkillbook" .. iLevel, "N", 1.0)
    
    --Increment the skillbooks total.
    local iSkillbookTotal = VM_GetVar("Root/Variables/Global/Mei/iSkillbookTotal", "N") + 1
    VM_SetVar("Root/Variables/Global/Mei/iSkillbookTotal", "N", iSkillbookTotal)
    
    --Award JP/Abilities
    AdvCombat_SetProperty("Push Party Member", "Mei")
    
        --JP.
        local iGlobalJP = AdvCombatEntity_GetProperty("Global JP")
        AdvCombatEntity_SetProperty("Current JP", iGlobalJP + 100)
        fnCutscene([[ Append("Mei:[E|Smirk] (Gained 100 JP for Mei!)[B][C]") ]])
        
        --Check if this unlocked a class ability.
        if(iSkillbookTotal >= 1.0 and iSkillbookTotal <= 4.0) then
            fnCutscene([[ Append("Mei:[E|Smirk] (Unlocked a new job ability slot for Mei!)[B][C]") ]])
        end

        --Execute the current job's SwitchTo script.
        AdvCombatEntity_SetProperty("Push Job S", "Active")
            AdvCombatJob_SetProperty("Fire Script", gciJob_SwitchTo)
        DL_PopActiveObject()
        
    DL_PopActiveObject()
    
end

-- |[ ======================================== Dialogue ======================================== ]|
--Each skillbook has some useful tips!
if(iLevel == 0) then
    fnCutscene([[ Append("Mei:[E|Neutral] The Fencer's Friend, Volume 1.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] \"Your MP always replenishes at the start of a battle, and between turns.[P] Don't conserve it, use it!\"[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] \"When facing tougher enemies, consider how much MP you gain between rounds versus the cost, so as not to run out.[P] Weaker enemies?[P] Go all out!\"") ]])
    
elseif(iLevel == 1) then
    fnCutscene([[ Append("Mei:[E|Neutral] The Fencer's Friend, Volume 2.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] \"The doctor bag gains charges faster the faster you defeat your enemies. Don't spend time healing - take the enemy down and use the doctor bag.\"[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] \"If you see a floating plus sign, touch it to immediately refill your doctor bag to full. These respawn when you rest.\"") ]])
    
elseif(iLevel == 2) then
    fnCutscene([[ Append("Mei:[E|Neutral] The Fencer's Friend, Volume 3.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] \"Enemies that are vulnerable to bleeding damage are both vulnerable to damage-over-times and direct damage.\"[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] \"Pay attention to the icon next to the ability's damage. This is its type, and some abilities deal immediate bleed or poison damage.\"") ]])
    
elseif(iLevel == 3) then
    fnCutscene([[ Append("Mei:[E|Neutral] The Fencer's Friend, Volume 4.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] \"If you max out a given job's abilities, any spare JP will be placed in a global pool all jobs can buy with.\"[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] \"If you really like a job, you can safely use it and use the global JP to buy abilities out of other jobs!\"") ]])
    
elseif(iLevel == 4) then
    fnCutscene([[ Append("Mei:[E|Neutral] The Fencer's Friend, Volume 5.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] \"Some characters may have a second menu of abilities. Look for an indicator above the player combat panel.\"[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] \"You may have special abilities there, including the ability to change jobs in mid-battle!\"") ]])
    
elseif(iLevel == 5) then
    fnCutscene([[ Append("Mei:[E|Neutral] The Fencer's Friend, Volume 6.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] \"Free-Actions are abilities that do not end your turn. By default, you may use one free action per turn.\"[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] \"Most Free-Actions are buffs, but a few deal damage, allowing you to really put the hurt on your enemies! Don't forget them!\"") ]])
    
elseif(iLevel == 6) then
    fnCutscene([[ Append("Mei:[E|Neutral] The Fencer's Friend, Volume 7.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] \"The [Fast] buff means your character will act before the enemy does, regardless of Initiative score.\"[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] \"The [Slow] debuff does the opposite. And if you have both, they cancel each other out!\"[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] \"If two characters both have [Fast], then they use Initiative as normal.\"") ]])
    
end
fnCutsceneBlocker()
