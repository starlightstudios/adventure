-- |[ ================================== Build Secondary Menu ================================== ]|
--Mei can change classes in combat. This subroutine determines the current class and populates
-- what classes are available.
local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")

-- |[ ==================================== List Construction =================================== ]|
--If not done already, construct a list of all job changes available.
if(gzaMeiSecondaryMenu == nil) then

    --Adder function.
    local zaList = {}
    local function fnAdd(psVarName, psExclusiveName, piXPos, piYPos, psAbilityName)
        local i = #zaList + 1
        zaList[i] = {}
        zaList[i].sVariable = psVarName
        zaList[i].sExclusiveName = psExclusiveName
        zaList[i].iXPos = piXPos
        zaList[i].iYPos = piYPos
        zaList[i].sAbilityName = psAbilityName
    end

    --List.
    fnAdd("TRUE",                                          "Human",       0, 0, "JobChange|Job Change - Fencer")
    fnAdd("Root/Variables/Global/Mei/iHasAlrauneForm",     "Alraune",     1, 0, "JobChange|Job Change - Nightshade")
    fnAdd("Root/Variables/Global/Mei/iHasBeeForm",         "Bee",         2, 0, "JobChange|Job Change - Hive Scout")
    fnAdd("Root/Variables/Global/Mei/iHasSlimeForm",       "Slime",       3, 0, "JobChange|Job Change - Smarty Sage")
    fnAdd("Root/Variables/Global/Mei/iHasWerecatForm",     "Werecat",     4, 0, "JobChange|Job Change - Prowler")
    fnAdd("Root/Variables/Global/Mei/iHasGhostForm",       "Ghost",       5, 0, "JobChange|Job Change - Maid")
    fnAdd("Root/Variables/Global/Mei/iHasGravemarkerForm", "Gravemarker", 6, 0, "JobChange|Job Change - Petraian")
    fnAdd("Root/Variables/Global/Mei/iHasMannequinForm",   "Mannequin",   0, 1, "JobChange|Job Change - Stalker")
    fnAdd("Root/Variables/Global/Mei/iHasWisphagForm",     "Wisphag",     1, 1, "JobChange|Job Change - Soulherd")
    fnAdd("Root/Variables/Global/Mei/iHasZombeeForm",      "Zombee",      2, 1, "JobChange|Job Change - Zombee")
    
    --Store.
    gzaMeiSecondaryMenu = zaList
end

-- |[ ==================================== Menu Construction =================================== ]|
--Build the menu layout.
for i = 1, #gzaMeiSecondaryMenu, 1 do
    
    --Compute position.
    local iX = gciAbility_Tactics_FormStartX + gzaMeiSecondaryMenu[i].iXPos
    local iY = gciAbility_Tactics_FormStartY + gzaMeiSecondaryMenu[i].iYPos
    
    --If the variable name is "TRUE" then always place this.
    if(gzaMeiSecondaryMenu[i].sVariable == "TRUE") then
        AdvCombatEntity_SetProperty("Set Ability Slot", iX, iY, gzaMeiSecondaryMenu[i].sAbilityName)
    
    --Otherwise, the variable must be 1.0.
    elseif(VM_GetVar(gzaMeiSecondaryMenu[i].sVariable, "N") == 1.0) then
        AdvCombatEntity_SetProperty("Set Ability Slot", iX, iY, gzaMeiSecondaryMenu[i].sAbilityName)
    end
end
