-- |[ =================================== Discipline Of Mind =================================== ]|
-- |[Description]|
--Passive, -10% Atk but +2 all resistances. Also starts battle with shields and extra MP.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzIzunaMikoDisplineOfMind == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Miko"
    zAbiStruct.sSkillName    = "Discipline Of Mind"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Passive
    zAbiStruct.sIconBacking  = gsAbility_Backing_Buff
    zAbiStruct.sIconFrame    = gsAbility_Frame_Passive
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Izuna|Miko|DisciplineOfMind"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_Passive
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "Start battle with +25[MPIco] and +50[Shld].\n+2 All Resists.\n-10%% [Atk].\n\n\n\nPassive."
    zAbiStruct.sDescription = ""
    zAbiStruct.saImages = {}
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Your mental focus increases your starting MP\nand gives 50 shields.\nAll resists +2, but decreases [Atk](Power) slightly.\n\n\nPassive."
    zAbiStruct.sSimpleDesc = ""
    zAbiStruct.saSimpleImages = {}

    -- |[Usability Variables]|
    zAbiStruct.bNeverAvailable = true    --Passive abilities have no usability requirements.
    
    -- |[Passive Effect]|
    --Apply this effect when combat starts.
    zAbiStruct.sPassivePath = fnResolvePath() .. "../Effects/Discipline Of Mind.lua"
    
    --GUI Stat Bonuses
    zAbiStruct.bHasGUIEffects = true
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Prediction Package]|
    --Passives don't set anything for prediction packages.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    --Passives don't set anything for execution packages.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Buff")
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzIzunaMikoDisplineOfMind = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzIzunaMikoDisplineOfMind


-- |[ ================================= Combat: Combat Begins ================================== ]|
--Apply the passive, MP, and shield.
if(iSwitchType == gciAbility_BeginCombat) then

    -- |[Setup]|
    --Common.
    local iUniqueID = RO_GetID()

    -- |[Apply Passive Effect]|
    --If there is a passive applied by this ability, do that here:
    if(gzRefAbility.sPassivePath ~= nil) then

        --Get the owner for their ID.
        AdvCombatAbility_SetProperty("Push Owner")
            local iOriginatorID = RO_GetID()
        DL_PopActiveObject()
        
        --Spawn the effect.
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 0, "Effect|" .. gzRefAbility.sPassivePath .. "|Originator:" .. iOriginatorID .. "|StoreID:" .. iUniqueID)
        
        --Passive: Mark that we are tracking an effect.
        if(gzRefAbility.sPassivePath ~= nil) then
            DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "/")
            VM_SetVar ("Root/Variables/Combat/" .. iUniqueID .. "/iIsManagingEffect", "N", 1.0)
            VM_SetVar ("Root/Variables/Combat/" .. iUniqueID .. "/iManagedEffectID", "N", 0.0) --Will be populated when the effect is created.
        end
    end
    
    -- |[Apply Shields and MP]|
    AdvCombatAbility_SetProperty("Push Owner")
    
        --Start with a shield. Don't overwrite a bigger shield.
        local iCurrentShield = AdvCombatEntity_GetProperty("Shields")
        if(iCurrentShield < 50) then
            AdvCombatEntity_SetProperty("Shields", 50)
        end
    
        --Extra MP.
        local iCurrentMP = AdvCombatEntity_GetProperty("Magic")
        AdvCombatEntity_SetProperty("Magic", iCurrentMP + 25)
    DL_PopActiveObject()
    
-- |[ ===================================== All Other Cases ==================================== ]|
elseif(iSwitchType ~= gciAbility_GUIApplyEffect) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

-- |[ ==================================== GUI: Apply Effect =================================== ]|
--When showing a character's statistics, all equipped abilities are queried. This is not reflected
-- in combat, this is GUI only.
--The standard does not handle this.
elseif(iSwitchType == gciAbility_GUIApplyEffect) then
    AdvCombatAbility_SetProperty("Push Owner")
        fnApplyStatBonusPct(gciStatIndex_Attack,     -0.10)
        fnApplyStatBonus(gciStatIndex_Resist_Slash,   2)
        fnApplyStatBonus(gciStatIndex_Resist_Strike,  2)
        fnApplyStatBonus(gciStatIndex_Resist_Pierce,  2)
        fnApplyStatBonus(gciStatIndex_Resist_Flame,   2)
        fnApplyStatBonus(gciStatIndex_Resist_Freeze,  2)
        fnApplyStatBonus(gciStatIndex_Resist_Shock,   2)
        fnApplyStatBonus(gciStatIndex_Resist_Crusade, 2)
        fnApplyStatBonus(gciStatIndex_Resist_Obscure, 2)
        fnApplyStatBonus(gciStatIndex_Resist_Bleed,   2)
        fnApplyStatBonus(gciStatIndex_Resist_Poison,  2)
        fnApplyStatBonus(gciStatIndex_Resist_Corrode, 2)
        fnApplyStatBonus(gciStatIndex_Resist_Terrify, 2)
    DL_PopActiveObject()
    gzRefAbility = nil
end
