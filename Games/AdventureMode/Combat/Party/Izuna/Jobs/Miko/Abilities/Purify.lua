-- |[ ========================================= Purify ========================================= ]|
-- |[Description]|
--Removes all Corrode, Poison, Obscure, and Terrify DoTs/Effects.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzIzunaMikoPurify == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Miko"
    zAbiStruct.sSkillName    = "Purify"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Cheap
    zAbiStruct.sIconBacking  = gsAbility_Backing_Free_Heal
    zAbiStruct.sIconFrame    = gsAbility_Frame_Free_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Izuna|Miko|Purify"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "Removes all negative [Crd][Psn][Obs][Tfy] effects. [Target].\n\n\n\n\nFree Action.\n[Costs]"
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Removes all negative [Psn](Poison), [Tfy] (Terrify),\n[Obs](Obscure), and [Crd](Corrode) effects.\nAffects the whole party.\n\nFree Action.\nCosts [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false  --Set to true to ignore all requirements.
    zAbiStruct.iRequiredMP = 10          --MP required to use the ability.
    zAbiStruct.iRequiredCP = 0           --CP required to use the ability.
    zAbiStruct.bRespectsCooldown = false --If this ability has a cooldown, decrements it.
    zAbiStruct.iCooldown = 0             --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
    zAbiStruct.bIsFreeAction = true      --Flags as a free action.
    zAbiStruct.iRequiredFreeActions = 1  --Free actions set this as 1, everything else as 0.
    zAbiStruct.bRespectActionCap = true  --Characters with multiple free actions have a hard action cap. This flag checks that cap.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Allies All"

    --Effects
    zAbiStruct.iCPGain = 0
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Create an Effect module
    local sPath = "Root/Images/AdventureUI/DamageTypeIcons/"
    zAbiStruct.zPredictionPackage.zaEffectModules[1] = fnCreateEffectModule()
    zAbiStruct.zPredictionPackage.zaEffectModules[1].bIsBuff = true
    zAbiStruct.zPredictionPackage.zaEffectModules[1].sResultScript = "Clear [IMG1]/[IMG2]/[IMG3]/[IMG4] DoTs."
    zAbiStruct.zPredictionPackage.zaEffectModules[1].saResultImages = {sPath.."Corroding", sPath.."Poisoning", sPath.."Obscuring", sPath.."Terrifying"}

    -- |[ ========= Execution Package ======== ]|
    --Basic package.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Healing")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.bDoesNoDamage = true
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun = true
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits = true
    zAbiStruct.zExecutionAbiPackage.bNeverCrits = true
    zAbiStruct.zExecutionAbiPackage.bEffectAlwaysApplies = true
    
    --Additional Paths
    zAbiStruct.zExecutionAbiPackage.saExecPaths = {LM_GetCallStack(0)}
    zAbiStruct.zExecutionAbiPackage.iaExecCodes = {gciAbility_SpecialStart}
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzIzunaMikoPurify = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzIzunaMikoPurify

-- |[ ============================ Combat: Special Negative Effects ============================ ]|
--All negative effects found with the poison/corrode/obscure/terrify effects are removed.
if(iSwitchType == gciAbility_SpecialStart) then

    --Argument handling.
    if(iArgumentsTotal < 3) then return end
    local iOriginatorID   = LM_GetScriptArgument(1, "N")
    local iTargetUniqueID = LM_GetScriptArgument(2, "N")
    
    --Subroutine needs a specially made list. Any of the matching elemental types must occur along with an
    -- "Is Negative" tag.
    local zaTagStruct = {}
    table.insert(zaTagStruct, {"Is Negative", "Poisoning DoT"})
    table.insert(zaTagStruct, {"Is Negative", "Corroding DoT"})
    table.insert(zaTagStruct, {"Is Negative", "Obscuring DoT"})
    table.insert(zaTagStruct, {"Is Negative", "Terrifying DoT"})
    table.insert(zaTagStruct, {"Is Negative", "Poisoning Effect"})
    table.insert(zaTagStruct, {"Is Negative", "Corroding Effect"})
    table.insert(zaTagStruct, {"Is Negative", "Obscuring Effect"})
    table.insert(zaTagStruct, {"Is Negative", "Terrifying Effect"})
    
    --Run the subroutine.
    local iaEffectIDList = fnCreateListOfEffectsWithTagStruct(iTargetUniqueID, zaTagStruct)
    
    --If there was at least one effect on the list to remove, add application packs for the removal.
    if(#iaEffectIDList > 0) then
    
        local iAnimTicks = fnGetAnimationTiming("Healing")
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, giStoredTimer, "Play Sound|Combat\\|Heal")
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, giStoredTimer, "Create Animation|Healing|AttackAnim0")
        giStoredTimer = giStoredTimer + iAnimTicks
    
        AdvCombat_SetProperty("Register Application Pack", piOriginatorID, iTargetUniqueID, giStoredTimer, "Text|Cured!|Color:Green")
        for i = 1, #iaEffectIDList, 1 do
            AdvCombat_SetProperty("Register Application Pack", piOriginatorID, iTargetUniqueID, giStoredTimer, "Remove Effect|" .. iaEffectIDList[i])
        end
        giStoredTimer = giStoredTimer + gciApplication_TextTicks
    end
    
    -- |[Werebat TF Remover]|
    --If Purify is used, it resets the werebat variable back to zero, unless the TF already finished.
    local iWerebatStage = VM_GetVar("Root/Variables/Chapter2/WerebatTF/iWerebatStage", "N")
    if(iWerebatStage < gciWerebatTF_FullyTFd) then
        
        --Reset the variable.
        VM_SetVar("Root/Variables/Chapter2/WerebatTF/iWerebatStage", "N", gciWerebatTF_Uninfected)
        
        --Reset the costume.
        VM_SetVar("Root/Variables/Costumes/Sanya/sCostumeHuman", "S", "Rifle")
        LM_ExecuteScript(gsCharacterAutoresolve, "Sanya")
    end

-- |[ ==================================== Standard Handler ==================================== ]|
--Call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
