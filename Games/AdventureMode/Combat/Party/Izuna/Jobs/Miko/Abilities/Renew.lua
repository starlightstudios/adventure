-- |[ ========================================== Renew ========================================= ]|
-- |[Description]|
--4CP ability that heals, removes negative effects, and applys a HoT.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzIzunaMikoRenew == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Miko"
    zAbiStruct.sSkillName    = "Renew"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Advanced
    zAbiStruct.sIconBacking  = gsAbility_Backing_Heal
    zAbiStruct.sIconFrame    = gsAbility_Frame_Combo
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Izuna|Miko|Renew"
    zAbiStruct.sCPIcon       = "Root/Images/AdventureUI/Abilities/Cmb4"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "[Restore]. [Target].\n[FEffect] Heals [0.35x [Atk] + 30][Hlt] over 3 turns.\nRemoves all negative effects.\n\n\n\n[Costs]"
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Heals an ally and removes all negative effects.\nAlso provides a heal-over-time.\n\n\n\nCosts [CPCost][CPIco](CP)."

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false  --Set to true to ignore all requirements.
    zAbiStruct.iRequiredMP = 0          --MP required to use the ability.
    zAbiStruct.iRequiredCP = 4           --CP required to use the ability.
    zAbiStruct.bRespectsCooldown = true  --If this ability has a cooldown, decrements it.
    zAbiStruct.iCooldown = 0             --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
    zAbiStruct.bIsFreeAction = false     --Flags as a free action.
    zAbiStruct.iRequiredFreeActions = 0  --Free actions set this as 1, everything else as 0.
    zAbiStruct.bRespectActionCap = false --Characters with multiple free actions have a hard action cap. This flag checks that cap.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Allies Single"

    --Effects
    zAbiStruct.iCPGain = 0
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Healing Module
    zAbiStruct.zPredictionPackage.bHasHealingModule = true
    zAbiStruct.zPredictionPackage.zHealingModule.iHealingFixed = 30
    zAbiStruct.zPredictionPackage.zHealingModule.fHealingFactor = 0.35
    
    --Create an Effect module
    zAbiStruct.zPredictionPackage.zaEffectModules[1] = fnCreateEffectModule()
    zAbiStruct.zPredictionPackage.zaEffectModules[1].bIsHealOverTime = true
    zAbiStruct.zPredictionPackage.zaEffectModules[1].fHotAttackFactor = 0.35
    zAbiStruct.zPredictionPackage.zaEffectModules[1].fHotFixedValue = 30
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iHotDuration = 3

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    --Basic package.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Healing")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.bDoesNoDamage = true
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun = true
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits = true
    zAbiStruct.zExecutionAbiPackage.bNeverCrits = true
    zAbiStruct.zExecutionAbiPackage.bEffectAlwaysApplies = true
    zAbiStruct.zExecutionAbiPackage.iHealingBase = 30
    zAbiStruct.zExecutionAbiPackage.fHealingFactor = 0.35
    
    -- |[Execution Effect Package]|
    --Basic package.
    zAbiStruct.zExecutionEffPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides.
    zAbiStruct.zExecutionEffPackage.sEffectPath     = fnResolvePath() .. "../Effects/Renew.lua" 
    zAbiStruct.zExecutionEffPackage.sEffectCritPath = fnResolvePath() .. "../Effects/Renew.lua" 
    zAbiStruct.zExecutionEffPackage.sApplyText      = "Renewed!"
    zAbiStruct.zExecutionEffPackage.sApplyTextCol   = "Color:Purple"
    zAbiStruct.zExecutionEffPackage.sResistText     = "Failed!"
    zAbiStruct.zExecutionEffPackage.sResistTextCol  = "Color:White"
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzIzunaMikoRenew = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzIzunaMikoRenew

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Removes all negative effects. All negative effects should have the tag "Is Negative".
if(iSwitchType == gciAbility_Execute) then

    -- |[ ======================= Defaults ======================= ]|
    --Default set.
    if(gzRefAbility.bIsFreeAction == nil) then gzRefAbility.bIsFreeAction = false end
    
    -- |[ ================ Originator Statistics ================= ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
    
        --Get ID.
        local iOriginatorID = RO_GetID()
        
        --CP handler.
        if(gzRefAbility.iCPGain ~= nil and gzRefAbility.iRequiredCP ~= nil) then
            fnModifyCP(gzRefAbility.iCPGain - gzRefAbility.iRequiredCP)
        end
        
        --MP handler.
        if(gzRefAbility.iRequiredMP ~= nil) then
            fnModifyMP(gzRefAbility.iRequiredMP * -1)
        end
    
    DL_PopActiveObject()
    
    -- |[ ==================== Ability Package =================== ]|
    --Create an ability package. This package comes with the default hit/miss values, animations, etc.
    local zAbilityPackage = gzRefAbility.zExecutionAbiPackage
    zAbilityPackage.iOriginatorID = iOriginatorID
    
    -- |[ ==================== Effect Package ==================== ]|
    --Create an effect package and populate it.
    local zEffectPackage = gzRefAbility.zExecutionEffPackage
    if(zEffectPackage ~= nil) then
        zEffectPackage.iOriginatorID = iOriginatorID
    end
    
    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        
        --Call standard function.
        fnStandardExecution(iOriginatorID, i-1, zAbilityPackage, zEffectPackage)
        
        --Get the target ID using the target index.
        AdvCombat_SetProperty("Push Target", piTargetIndex)
            local iTargetID = RO_GetID()
        DL_PopActiveObject()
        
        --Check for negative effects. Type doesn't matter.
        local iaEffectIDList = fnCreateListOfEffectsWithTag(iTargetID, "Is Negative")
    
        --If there was at least one effect on the list to remove, add application packs for the removal.
        if(#iaEffectIDList > 0) then
        
            local iAnimTicks = fnGetAnimationTiming("Healing")
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetID, giStoredTimer, "Play Sound|Combat\\|Heal")
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetID, giStoredTimer, "Create Animation|Healing|AttackAnim0")
            giStoredTimer = giStoredTimer + iAnimTicks
        
            AdvCombat_SetProperty("Register Application Pack", piOriginatorID, iTargetID, giStoredTimer, "Text|Cured!|Color:Green")
            for i = 1, #iaEffectIDList, 1 do
                AdvCombat_SetProperty("Register Application Pack", piOriginatorID, iTargetID, giStoredTimer, "Remove Effect|" .. iaEffectIDList[i])
            end
            giStoredTimer = giStoredTimer + gciApplication_TextTicks
        end
    end

-- |[ ==================================== Standard Handler ==================================== ]|
--Call standard handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
