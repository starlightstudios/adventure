-- |[ ====================================== Sealing Ward ====================================== ]|
-- |[Description]|
--Crusading effect that lowers all an enemy's stats, but does no damage.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzIzunaMikoSealingWard == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = {}

    --Display/System Variables
    zAbiStruct.sJobName      = "Miko"
    zAbiStruct.sSkillName    = "Sealing Ward"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Izuna|Miko|SealingWard"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "[HEffect][Cru][Str8] -15%%[Atk][Acc][Evd][Ini] / 3 [Turns].\n\n\n\n\n\n[Costs]"
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Place a ward that seals the enemy's strength.\nReduces all enemy stats.\n\n\n\nCosts [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false  --Set to true to ignore all requirements.
    zAbiStruct.iRequiredMP = 10          --MP required to use the ability.
    zAbiStruct.iRequiredCP = 0           --CP required to use the ability.
    zAbiStruct.bRespectsCooldown = true  --If this ability has a cooldown, decrements it.
    zAbiStruct.iCooldown = 0             --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
    zAbiStruct.bIsFreeAction = false     --Flags as a free action.
    zAbiStruct.iRequiredFreeActions = 0  --Free actions set this as 1, everything else as 0.
    zAbiStruct.bRespectActionCap = false --Characters with multiple free actions have a hard action cap. This flag checks that cap.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies Single"

    --Effects
    zAbiStruct.iCPGain = 1
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Create an Effect module
    zAbiStruct.zPredictionPackage.zaEffectModules[1] = fnCreateEffectModule()
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iEffectType = gciDamageType_Crusading
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iEffectStrength = 8
    zAbiStruct.zPredictionPackage.zaEffectModules[1].sResultScript = "-15%% [IMG1][IMG2][IMG3][IMG4], / 3[IMG5]"
    zAbiStruct.zPredictionPackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/StatisticIcons/Attack", "Root/Images/AdventureUI/StatisticIcons/Accuracy", "Root/Images/AdventureUI/StatisticIcons/Evade",
                                                                       "Root/Images/AdventureUI/StatisticIcons/Initiative", "Root/Images/AdventureUI/DebuffIcons/Clock"}
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Light")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.bDoesNoDamage  = true
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun    = true
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits    = true
    
    -- |[Execution Effect Package]|
    --Create an effect package and populate it.
    zAbiStruct.zExecutionEffPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides.
    zAbiStruct.zExecutionEffPackage.iEffectStr     = 8
    zAbiStruct.zExecutionEffPackage.iEffectCritStr = 11
    zAbiStruct.zExecutionEffPackage.iEffectType    = gciDamageType_Crusading
    zAbiStruct.zExecutionEffPackage.sApplyAnimation = "Null"
    zAbiStruct.zExecutionEffPackage.sApplySound     = "Null"
    zAbiStruct.zExecutionEffPackage.sEffectPath     = fnResolvePath() .. "../Effects/Sealing Ward.lua"
    zAbiStruct.zExecutionEffPackage.sEffectCritPath = fnResolvePath() .. "../Effects/Sealing Ward.lua"
    zAbiStruct.zExecutionEffPackage.sApplyText      = "Sealed!"
    zAbiStruct.zExecutionEffPackage.sApplyTextCol   = "Color:Purple"
    zAbiStruct.zExecutionEffPackage.sResistText     = "Resisted!"
    zAbiStruct.zExecutionEffPackage.sResistTextCol  = "Color:White"
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzIzunaMikoSealingWard = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzIzunaMikoSealingWard

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
