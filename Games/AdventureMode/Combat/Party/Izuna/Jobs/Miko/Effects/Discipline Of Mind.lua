-- |[ =================================== Discipline Of Mind =================================== ]|
-- |[Description]|
--All resists +2, -10% Atk.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Local Variables ===================================== ]|
-- |[System]|
--This effect uses a standardized StatMod script.
local sDisplayName = "Discipline Of Mind"
local iDuration = -1
local bIsBuff = true
local sIcon = "Root/Images/AdventureUI/Abilities/Izuna|Miko|DisciplineOfMind"
local sStatString = "ResSls|Flat|2 ResStk|Flat|2 ResPrc|Flat|2 ResFlm|Flat|2 ResFrz|Flat|2 ResShk|Flat|2 ResCru|Flat|2 ResObs|Flat|2 ResBld|Flat|2 ResPsn|Flat|2 ResCrd|Flat|2 ResTfy|Flat|2 Atk|Pct|-0.10"

--Description can be a variable number of lines.
local saDescription = {}
saDescription[1] = "Your focus makes you more durable."
saDescription[2] = ""
saDescription[3] = ""
saDescription[4] = ""
saDescription[5] = ""
saDescription[6] = "+2 All Resists. -10%% [Atk]."
saDescription[7] = ""

--Run the standardized builder.
gzaStatModStruct            = fnCreateStatmodEffectPrototype(sDisplayName, iDuration, bIsBuff, sIcon, sStatString)
gzaStatModStruct.saStrings  = fnStatmodBuildInspectorStrings(gzaStatModStruct, saDescription)
gzaStatModStruct.sShortText = fnStatmodBuildShortText(gzaStatModStruct)

-- |[Overrides]|
--Passive, not removed on KO, uses different frame.
gzaStatModStruct.sFrame      = gsAbility_Frame_Passive
gzaStatModStruct.bRemoveOnKO = false

--Right string is obscenely long. Modify it.
gzaStatModStruct.saStrings[2] = "+2 All Resists, -10%% [Atk]"

-- |[Tags]|
gzaStatModStruct.zaTagList = {{"Is Positive", 1}, {"Is Passive", 1}}

-- |[ ======================================== Creation ======================================== ]|
--Standard call when effect is initialized.
if(iSwitchType == gciEffect_Create) then
    LM_ExecuteScript(gsStandardStatPath, gciEffect_Create)
    
-- |[ ================================== Apply/Unapply Stats =================================== ]|
--Calls the apply/unapply stat routines.
elseif((iSwitchType == gciEffect_ApplyStats or iSwitchType == gciEffect_UnApplyStats) and iArgumentsTotal >= 2) then
    local iTargetID = math.floor(tonumber(LM_GetScriptArgument(1, "N")))
    LM_ExecuteScript(gsStandardStatPath, iSwitchType, iTargetID)

-- |[ ================================== Character Action Ends ================================= ]|
--Called after a character action ends to decrement the turn counter.
elseif(iSwitchType == gciEffect_PostAction) then
    LM_ExecuteScript(gsStandardStatPath, gciEffect_PostAction)
end
