-- |[ ====================================== Rending Ward ====================================== ]|
-- |[Description]|
--Deals bleeding damage equal to 1.15 of user's attack power over 5 turns.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--Setup, apply overrides.
if(gzEffectIzunaMikoRendingWardCrit == nil) then
    
    --Base, overrides.
    local zEffectStruct = fnCreateDoTEffectPrototype(gciDamageType_Bleeding, 5, 1.15)
    zEffectStruct.sDisplayName = "Rending Ward"
    zEffectStruct.sIcon = "Mei|Rend"
    
    --Tags
    zEffectStruct.zaTagList = {{"Striking DoT", 1}, {"Is Negative", 1}}
    
    --Store
    gzEffectFencerRendCrit = zEffectStruct
    
end

-- |[Set and Execute]|
--Standardized Naming
gzRefEffect = gzEffectFencerRendCrit

--Call the standard handler. It will nil off the gzRefEffect.
LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType)
