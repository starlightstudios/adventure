-- |[ ================================== Build Secondary Menu ================================== ]|
--Sanya can change classes in combat. This subroutine determines the current class and populates
-- what classes are available.
local sSanyaForm = VM_GetVar("Root/Variables/Global/Izuna/sCurrentJob", "S")

-- |[ ==================================== List Construction =================================== ]|
--If not done already, construct a list of all job changes available.
if(gzaIzunaSecondaryMenu == nil) then

    --Adder function.
    local zaList = {}
    local function fnAdd(psVarName, psExclusiveName, piXPos, piYPos, psAbilityName)
        local i = #zaList + 1
        zaList[i] = {}
        zaList[i].sVariable = psVarName
        zaList[i].sExclusiveName = psExclusiveName
        zaList[i].iXPos = piXPos
        zaList[i].iYPos = piYPos
        zaList[i].sAbilityName = psAbilityName
    end

    --List.
    --fnAdd("TRUE",                                                  "Human",      0, 0, "JobChange|Job Change - Hunter")
    
    --Store.
    gzaIzunaSecondaryMenu = zaList
end

-- |[ ==================================== Menu Construction =================================== ]|
--Build the menu layout.
for i = 1, #gzaIzunaSecondaryMenu, 1 do
    
    --If the variable name is "TRUE" then always place this.
    if(gzaIzunaSecondaryMenu[i].sVariable == "TRUE") then
        AdvCombatEntity_SetProperty("Set Secondary Slot", gzaSanyaSecondaryMenu[i].iXPos, gzaSanyaSecondaryMenu[i].iYPos, gzaSanyaSecondaryMenu[i].sAbilityName)
    
    --Otherwise, the variable must be 1.0.
    elseif(VM_GetVar(gzaIzunaSecondaryMenu[i].sVariable, "N") == 1.0) then
        AdvCombatEntity_SetProperty("Set Secondary Slot", gzaSanyaSecondaryMenu[i].iXPos, gzaSanyaSecondaryMenu[i].iYPos, gzaSanyaSecondaryMenu[i].sAbilityName)
    end
end