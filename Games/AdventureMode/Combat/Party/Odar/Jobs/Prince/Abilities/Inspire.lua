-- |[ ======================================== Inspire ========================================= ]|
-- |[Description]|
--Performs a guaranteed critical strike and buffs ally attack power. Has a long cooldown.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--These variables are used to auto-generate the properties of the ability when the ability is a
-- "standard" case. If you want to deviate from the standard, set things manually.
--Because the struct is global, it needs to have a unique name.
if(gzLancerInspire == nil) then
    
    -- |[System]|
    --Setup.
    local zAbiStruct = {}

    --Display/System Variables
    zAbiStruct.sJobName      = "Lancer"
    zAbiStruct.sSkillName    = "Inspire"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Advanced
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Christine|Inspire"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "Inflict [[DamFactor]x [Atk]] as [DamageType], always crits.\nBuffs ally [Atk] by 25%% / 3 turns.\nGenerates [CPGen][CPIco].\n\nOne Enemy.\n5 turn cooldown."
    zAbiStruct.sDescription = ""
    zAbiStruct.saImages = {}
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Set an example with a powerful blow!\nAlways crits, buffs ally attack power.\n5 turn cooldown.\n\nTriggers Spearguard.\n"
    zAbiStruct.sSimpleDesc = ""
    zAbiStruct.saSimpleImages = {}

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false  --Set to true to ignore all requirements.
    zAbiStruct.iRequiredMP = 0           --MP required to use the ability.
    zAbiStruct.iRequiredCP = 0           --CP required to use the ability.
    zAbiStruct.bRespectsCooldown = true  --If this ability has a cooldown, decrements it.
    zAbiStruct.iCooldown = 6             --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
    zAbiStruct.bIsFreeAction = false     --Flags as a free action.
    zAbiStruct.iRequiredFreeActions = 0  --Free actions set this as 1, everything else as 0.
    zAbiStruct.bRespectActionCap = false --Characters with multiple free actions have a hard action cap. This flag checks that cap.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies Single"

    --Effects
    zAbiStruct.iCPGain = 1
    
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Damage Module
    zAbiStruct.zPredictionPackage.bHasDamageModule = true
    zAbiStruct.zPredictionPackage.zDamageModule.bUseWeaponType = true
    zAbiStruct.zPredictionPackage.zDamageModule.fDamageFactor = 1.00
    zAbiStruct.zPredictionPackage.zDamageModule.iScatterRange = 15
    
    --Crit Module
    zAbiStruct.zPredictionPackage.bHasCritModule = true
    zAbiStruct.zPredictionPackage.zCritModule.iCritThreshold = -1000
    
    --Add "Triggers Spearguard" package.
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "Triggers Spearguard"
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
    
    -- |[Execution Ability Package]|
    --Optional threat multiplier.
    zAbiStruct.fThreatMultiplier = 3.0
    
    --Basic package.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Weapon")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.fDamageFactor  = 1.00
    zAbiStruct.zExecutionAbiPackage.bNeverGlances = true
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits = true
    zAbiStruct.zExecutionAbiPackage.bAlwaysCrits = true
    
    -- |[Execution Ally Effect Package]|
    --Basic package.
    zAbiStruct.zAllyEffectPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides.
    zAbiStruct.zAllyEffectPackage.sEffectPath     = fnResolvePath() .. "../Effects/Inspire.lua"
    zAbiStruct.zAllyEffectPackage.sEffectCritPath = fnResolvePath() .. "../Effects/Inspire.lua"
    zAbiStruct.zAllyEffectPackage.sApplyText = "Inspired!"
    zAbiStruct.zAllyEffectPackage.sApplyTextCol = "Color:White"
    zAbiStruct.zAllyEffectPackage.sApplyAnimation = "Null"
    zAbiStruct.zAllyEffectPackage.sApplySound = "Combat\\|Impact_Buff"
    
    -- |[Execution Self-Effect Package]|
    --Basic package.
    zAbiStruct.zExecutionSelfPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides.
    zAbiStruct.zExecutionSelfPackage.sEffectPath     = fnResolvePath() .. "../../Common/Effects/Spearguard.lua"
    zAbiStruct.zExecutionSelfPackage.sEffectCritPath = fnResolvePath() .. "../../Common/Effects/Spearguard.lua"
    zAbiStruct.zExecutionSelfPackage.sApplyText = "Spearguard"
    zAbiStruct.zExecutionSelfPackage.sApplyTextCol = "Color:White"
    zAbiStruct.zExecutionSelfPackage.sApplyAnimation = "Null"
    zAbiStruct.zExecutionSelfPackage.sApplySound = "Combat\\|Impact_Buff"
    
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzLancerInspire = zAbiStruct
end

--Because this is a prototype, we set the gzRefAbility to point to the unique structure. This way,
-- all the activity below points to the gzRefAbility's values and we don't need to change the structure's
-- name to create unique names for everything.
--The gsStandardAbilityPath handler also operates on gzRefAbility if it is called. You can use manual
-- overrides to make unique abilities.
gzRefAbility = gzLancerInspire

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
