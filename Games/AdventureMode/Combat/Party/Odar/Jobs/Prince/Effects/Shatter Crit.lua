-- |[ ====================================== Shatter Crit ====================================== ]|
-- |[Description]|
--Deals striking damage equal to 2.00x attack power over 5 turns.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--Setup, apply overrides.
if(gzEffectLancerShatterCrit == nil) then
    
    --Base
    local zEffectStruct = fnCreateDoTEffectPrototype(gciDamageType_Striking, 5, 2.00)
    
    --System/Display
    zEffectStruct.sDisplayName = "Shatter"
    zEffectStruct.sIcon = "Christine|Shatter"
    
    --Store
    gzEffectLancerShatterCrit = zEffectStruct
    
end

--Standardized Naming
gzRefEffect = gzEffectLancerShatterCrit

--Call the standard handler. It will nil off the gzRefEffect.
LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType)