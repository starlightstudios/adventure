-- |[ ================================ Florentina Initialization =============================== ]|
--Creates and stores the named character in the party roster. If the character already exists, pushes
-- the character and modifies their properties.
local sCharacterName = "Florentina"

-- |[ ======================================= Lua Globals ====================================== ]|
-- |[Ability Sectioning]|
--These globals are reset when the game restarts to avoid possible overlaps. These store the ability
-- prototypes for each class.
gzPrototypes.Combat.Florentina = {}
gzPrototypes.Combat.Florentina.Common         = {}
gzPrototypes.Combat.Florentina.Merchant       = {}
gzPrototypes.Combat.Florentina.Mediator       = {}
gzPrototypes.Combat.Florentina.TreasureHunter = {}
gzPrototypes.Combat.Florentina.Agarist        = {}
gzPrototypes.Combat.Florentina.Lurker         = {}
gzPrototypes.Combat.Florentina.Prestige       = {}

-- |[Costume Listing]|
--If the costume list doesn't exist for this character, build it. This is used by YCharacterAutoresolve.lua
-- when figuring out what costume a character is in.
local iSlotOfCharacter = fnGetSlotOnCostumeList(sCharacterName)

--If no entry exists, add one.
if(gzCostumeResolveList ~= nil and iSlotOfCharacter == -1) then
    
    --Create.
    local zEntry = {}
    zEntry.sCharacterName = sCharacterName
    zEntry.saJobListing   = {"Merchant", "Mediator", "Agarist", "Lurker", "Treasure Hunter", "TreasureHunter"}
    zEntry.saFormListing  = {"Merchant", "Mediator", "Agarist", "Lurker", "TreasureHunter",  "TreasureHunter"}
    
    --Add to list.
    table.insert(gzCostumeResolveList, zEntry)
    
    --Debug.
    --io.write("==> Florentina adds costume autoresolve entry.\n")
end

-- |[ =================================== Character Handling =================================== ]|
-- |[Register or Push]|
--Register if character does not exist.
local bIsCreating = false
if(AdvCombat_GetProperty("Does Party Member Exist", sCharacterName) == false)  then
    bIsCreating = true
    AdvCombat_SetProperty("Register Party Member", sCharacterName)

--Push character.
else
    AdvCombat_SetProperty("Push Party Member", sCharacterName)
end

-- |[Call Script]|
AdvCombatEntity_SetProperty("Response Script", fnResolvePath() .. "100 Combat Script.lua")

-- |[Base Statistics]|
--Persistent Stats
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_HPMax,    86)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_MPMax,   100)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_MPRegen,  10)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_CPMax,    10)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_StunCap, 100)

--Free-Action Handlers
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_FreeActionMax, 3)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_FreeActionGen, 1)

--Combat Stats
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Initiative, 50)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Attack,     12)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Accuracy,    4)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Evade,      14)

--Base resistances.
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Protection,     3)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Slash,   gciNormalResist-2)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Strike,  gciNormalResist+0)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Pierce,  gciNormalResist+1)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Flame,   gciNormalResist-3)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Freeze,  gciNormalResist+1)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Shock,   gciNormalResist+2)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Crusade, gciNormalResist+6)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Obscure, gciNormalResist-4)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Bleed,   gciNormalResist-1)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Poison,  gciNormalResist+3)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Corrode, gciNormalResist+3)
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_Resist_Terrify, gciNormalResist+0)

--Threat Multiplier. Default 100 for all entities.
AdvCombatEntity_SetProperty("Statistic", gciStatGroup_Base, gciStatIndex_ThreatMultiplier, 100)

-- |[ ================================== Job Statistics Chart ================================== ]|
--All jobs store their statistics here, in a big ungainly chart!
if(gzaFlorentinaJobChart == nil) then

    -- |[Creation Function]|
    --Adds elements to the reference chart.
    local gzaRefChart = {}
    local function fnAdd(psName, piHltConstant, pfHltPercent, piIniConstant, pfIniPercent, piAtkConstant, pfAtkPercent, piAccConstant, pfAccPercent, piEvdConstant, pfEvdPercent, 
                         piProtection, piSlash, piStrike, piPierce, piFlame, piFreeze, piShock, piCrusade, piObscure, piBleed, piPoison, piCorrode, piTerrify)
        
        --Setup.
        local i = #gzaRefChart + 1
        gzaRefChart[i] = {}
        
        --System.
        gzaRefChart[i].sName = psName
        
        --Stats.
        gzaRefChart[i].iHlt_Constant = piHltConstant
        gzaRefChart[i].fHlt_Percent  = pfHltPercent
        gzaRefChart[i].iIni_Constant = piIniConstant
        gzaRefChart[i].fIni_Percent  = pfIniPercent
        gzaRefChart[i].iAtk_Constant = piAtkConstant
        gzaRefChart[i].fAtk_Percent  = pfAtkPercent
        gzaRefChart[i].iAcc_Constant = piAccConstant
        gzaRefChart[i].fAcc_Percent  = pfAccPercent
        gzaRefChart[i].iEvd_Constant = piEvdConstant
        gzaRefChart[i].fEvd_Percent  = pfEvdPercent
        
        --Resistances.
        gzaRefChart[i].iBonus_Resist_Protection = piProtection
        gzaRefChart[i].iBonus_Resist_Slash = piSlash
        gzaRefChart[i].iBonus_Resist_Pierce = piPierce
        gzaRefChart[i].iBonus_Resist_Strike = piStrike
        gzaRefChart[i].iBonus_Resist_Flame = piFlame
        gzaRefChart[i].iBonus_Resist_Freeze = piFreeze
        gzaRefChart[i].iBonus_Resist_Shock = piShock
        gzaRefChart[i].iBonus_Resist_Crusade = piCrusade
        gzaRefChart[i].iBonus_Resist_Obscure = piObscure
        gzaRefChart[i].iBonus_Resist_Bleed = piBleed
        gzaRefChart[i].iBonus_Resist_Poison = piPoison
        gzaRefChart[i].iBonus_Resist_Corrode = piCorrode
        gzaRefChart[i].iBonus_Resist_Terrify = piTerrify
    end

    --     Actual Name ||||| HltC | HltP | IniC | IniP | AtkC | AtkP | AccC | AccP | EvdC | EvdP  ||||| Pr | Sl | St | Pc | Fl | Fr | Sh | Cr | Ob | Bl | Ps | Cr | Tr
    fnAdd("Merchant",         0,  0.00,     0,  0.00,     0,  0.00,     0,  0.00,     0,  0.00,      0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0)
    fnAdd("Mediator",       -12, -0.05,     3,  0.05,    -2, -0.02,     8,  0.03,    -2, -0.02,      0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   2)
    fnAdd("Treasure Hunter", 18,  0.08,     0,  0.00,     5,  0.03,     4,  0.03,    -2, -0.02,      0,   1,   1,   1,   0,   0,   0,   0,   0,   0,   0,   0,   0)
    fnAdd("Agarist",         10,  0.12,    -5, -0.10,     0,  0.00,    -3, -0.02,    -3, -0.04,      0,  -1,   1,   1,  -6,  -2,   4,   0,   0,   4,   3,   3,   3)
    fnAdd("Lurker",          20,  0.22,    10,  0.10,    15,  0.12,     0,  0.00,     0,  0.00,      0,   3,  -3,   3,  -7,   7,   7,   0,   0,   5,   5,  -7,   4)

    --Save.
    gzaFlorentinaJobChart = gzaRefChart
end

-- |[ ===================================== Equipment Slots ==================================== ]|
--Primary Weapon
AdvCombatEntity_SetProperty("Create Equipment Slot", "Weapon")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Weapon", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Weapon", false)

--Uses primary weapon to compute damage type. Can be modified by gems.
AdvCombatEntity_SetProperty("Set Equipment Slot Used For Weapon Damage", "Weapon")

--Swappable Weapons
AdvCombatEntity_SetProperty("Create Equipment Slot", "Weapon Backup A")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Weapon Backup A", false)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Weapon Backup A", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Is Alt Weapon", "Weapon Backup A", true)
AdvCombatEntity_SetProperty("Create Equipment Slot", "Weapon Backup B")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Weapon Backup B", false)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Weapon Backup B", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Is Alt Weapon", "Weapon Backup B", true)

--Body Armor
AdvCombatEntity_SetProperty("Create Equipment Slot", "Body")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Body", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Body", true)

--Accessory Slots
AdvCombatEntity_SetProperty("Create Equipment Slot", "Accessory A")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Accessory A", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Accessory A", true)
AdvCombatEntity_SetProperty("Create Equipment Slot", "Accessory B")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Accessory B", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Accessory B", true)

--Equippable Items
AdvCombatEntity_SetProperty("Create Equipment Slot", "Item A")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Item A", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Item A", true)
AdvCombatEntity_SetProperty("Create Equipment Slot", "Item B")
AdvCombatEntity_SetProperty("Set Equipment Slot Is Used For Statistics", "Item B", true)
AdvCombatEntity_SetProperty("Set Equipment Slot Can Be Empty", "Item B", true)

-- |[ ====================================== Job Handling ====================================== ]|
--Create Florentina's common abilities, shared by many classes.
local sJobAbilityPath = gsRoot .. "Combat/Party/Florentina/Jobs/Common/Abilities/"
LM_ExecuteScript(sJobAbilityPath .. "Attack.lua", gciAbility_Create)
LM_ExecuteScript(sJobAbilityPath .. "Guard.lua",  gciAbility_Create)
LM_ExecuteScript(sJobAbilityPath .. "Locked.lua", gciAbility_Create)

--Call all of Florentina's job subscripts.
local sCharacterJobPath = gsRoot .. "Combat/Party/Florentina/Jobs/"
LM_ExecuteScript(sCharacterJobPath .. "Merchant/000 Job Script.lua",        gciJob_Create)
LM_ExecuteScript(sCharacterJobPath .. "Treasure Hunter/000 Job Script.lua", gciJob_Create)
LM_ExecuteScript(sCharacterJobPath .. "Mediator/000 Job Script.lua",        gciJob_Create)
LM_ExecuteScript(sCharacterJobPath .. "Agarist/000 Job Script.lua",         gciJob_Create)
LM_ExecuteScript(sCharacterJobPath .. "Lurker/000 Job Script.lua",          gciJob_Create)
LM_ExecuteScript(sCharacterJobPath .. "Prestige/000 Job Script.lua",        gciJob_Create)
AdvCombatEntity_SetProperty("Rebuild Job Skills UI")

--Set Florentina to her default job.
AdvCombatEntity_SetProperty("Active Job", "Merchant")
VM_SetVar("Root/Variables/Global/Florentina/sCurrentJob", "S", "Merchant")

--Equipment Change Abilities
LM_ExecuteScript(gsRoot .. "Combat/Party/Common/Abilities/Swap Weapon A.lua", gciAbility_Create)
LM_ExecuteScript(gsRoot .. "Combat/Party/Common/Abilities/Swap Weapon B.lua", gciAbility_Create)

--Once all abilities are built across all jobs, order jobs to associate with abilities. Any abilities
-- associated with a job can be purchased in the skills UI using that job's JP, and equipped from
-- that job's submenu. If an ability is not associated with any job, and is not auto-equipped by
-- any jobs when the job is assumed, the ability is not usable by the player!
LM_ExecuteScript(sCharacterJobPath .. "Merchant/000 Job Script.lua",        gciJob_JobAssembleSkillList)
LM_ExecuteScript(sCharacterJobPath .. "Treasure Hunter/000 Job Script.lua", gciJob_JobAssembleSkillList)
LM_ExecuteScript(sCharacterJobPath .. "Mediator/000 Job Script.lua",        gciJob_JobAssembleSkillList)
LM_ExecuteScript(sCharacterJobPath .. "Agarist/000 Job Script.lua",         gciJob_JobAssembleSkillList)
LM_ExecuteScript(sCharacterJobPath .. "Lurker/000 Job Script.lua",          gciJob_JobAssembleSkillList)

--Prestige class, only contains special passives and cannot be entered.
LM_ExecuteScript(sCharacterJobPath .. "Prestige/000 Job Script.lua",     gciJob_JobAssembleSkillList)
 
-- |[ ==================================== Creation Callback =================================== ]|
--If this was a creation call of the character, fire its script callback.
if(bIsCreating) then
    local sResponsePath = AdvCombatEntity_GetProperty("Response Path")
    LM_ExecuteScript(sResponsePath, gciCombatEntity_Create)
end

-- |[ ======================================== Clean Up ======================================== ]|
--Pop the activity stack.
DL_PopActiveObject()
