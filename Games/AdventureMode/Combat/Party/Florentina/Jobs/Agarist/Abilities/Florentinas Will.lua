-- |[ =================================== Florentina's Will ==================================== ]|
-- |[Description]|
--Passive, increases corrode damage by 5% and MP generation by 5.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Florentina.Agarist.FlorentinasWill == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Agarist", "Florentina's Will", "Use Skill Name", gciJP_Cost_Passive, gbIsNotFreeAction, "Buff", "Passive", "Florentina|Agarist|FlorentinasWill", "Passive")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "+5[PCT][Crd] damage. +5[Man] / Turn.\n\n\n\n\n\nPassive."
    zAbiStruct.sSimpleDescMarkdown  = "Willpower allows for command of the fungus at[BR]a cellular level, and integration provides benefits.[BR]Increases [Crd](Corrode) damage and MP generation.\n\n\nPassive."

    -- |[Passive Effect]|
    zAbiStruct.bNeverAvailable   = true --Passive abilities have no usability requirements.
    zAbiStruct.sPassivePrototype = "Florentina.Agarist.FlorentinasWill"
    zAbiStruct.bHasGUIEffects    = true
    
    -- |[Tags]|
    --Apply tags to increase slashing damage.
    zAbiStruct.zaTags = {{"Corrode Damage Dealt +", 5}}

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    -- Note: The effect doesn't increase corrode damage, the ability itself does.
    if(EffectList:fnEntryExists(zAbiStruct.sPassivePrototype) == false) then
        local saDescription = {}
        saDescription[1] = "Mental mastery allows integration with a fungus"
        saDescription[2] = "on a cellular level, improving mana generation"
        saDescription[3] = "and corrode damage."
        saDescription[4] = ""
        saDescription[5] = ""
        saDescription[6] = "[Buff] [Man]+5/Turn +5[Crd] Dam"
        saDescription[7] = ""
      --EffectList:fnCreatePassivePrototype(psName, psDisplayName, psIcon, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreatePassivePrototype("Florentina.Agarist.FlorentinasWill", "Florentina's Will", "Florentina|Agarist|FlorentinasWill", "MPReg|Flat|5", saDescription)

        --Manually specify description.
        local iType, zPrototype, iSlot = EffectList:fnLocateEntry(zAbiStruct.sPassivePrototype)
        if(zPrototype ~= nil) then
            zPrototype.bDontRebuildDescription = true
            zPrototype.saStrings[1] = "[Buff]Florentina's Will"
            zPrototype.saStrings[2] = "[UpN][Man][Crd]"
            zPrototype.saStrings[3] = saDescription[1]
            zPrototype.saStrings[4] = saDescription[2]
            zPrototype.saStrings[5] = saDescription[3]
            zPrototype.saStrings[6] = saDescription[4]
            zPrototype.saStrings[7] = saDescription[5]
            zPrototype.saStrings[8] = saDescription[6]
            zPrototype.sShortText = ""
        end
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzPrototypes.Combat.Florentina.Agarist.FlorentinasWill = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Florentina.Agarist.FlorentinasWill

-- |[ ===================================== All Other Cases ==================================== ]|
if(iSwitchType ~= gciAbility_GUIApplyEffect) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

-- |[ ==================================== GUI: Apply Effect =================================== ]|
--When showing a character's statistics, all equipped abilities are queried. This is not reflected
-- in combat, this is GUI only.
--The standard does not handle this.
elseif(iSwitchType == gciAbility_GUIApplyEffect) then
    AdvCombatAbility_SetProperty("Push Owner")
        fnApplyStatBonus(gciStatIndex_MPRegen, 5)
    DL_PopActiveObject()
    gzRefAbility = nil
end
