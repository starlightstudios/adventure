-- |[ =================================== Might Of Mycelium ==================================== ]|
-- |[Description]|
--Inflict 150% corrode damage and a DoT that deals another 50% corrode damage over 3 turns, 6CP.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Florentina.Agarist.MightOfMycelium == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Agarist", "Might Of Mycelium", "Might of Mycelium", gciJP_Cost_Advanced, gbIsNotFreeAction, "Direct", "Combo", "Florentina|Agarist|MightOfMycelium", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\n[DoT].\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Unleash the full might of your fungal fury,[BR]dealing increased corrode damage to all enemies.\nAlso inflicts a corrode DoT on all enemies.\n\n\nCosts [CPCost][CPIco](CP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, 6, giNoCooldown, "Target Enemies All", 0)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Corrode")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(iDamageType, iMissRate, fDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Corroding, 5, 1.50)
    
    -- |[Execution Effect Package]|
  --zAbiStruct:fnAddEffect(piStrNormal, piStrCrit, piApplyType, psApplyText, psCritApplyText, psApplyColorString, psaApplyBonus, psaApplyMalus, psaSeverityBonus, psaSeverityMalus)
    zAbiStruct:fnAddEffect(7, 11, gciDamageType_Corroding, "Corroded!", "Badly Corroded!", "Color:Red", {"Corrode Standard"})
    
    -- |[Use of Effect Prototype]|
    zAbiStruct.zExecutionEffPackage.sPrototypeName = "Florentina.Agarist.MightOfMycelium"

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
      --EffectList:fnCreateDoTPrototype(psName, piTurns, piApplyStrength, pfDamPerTurn, piDamageType, psDisplayName, psIcon, piCritTurns, piCritApplyStrength)
        EffectList:fnCreateDoTPrototype(sLocalPrototypeName, 3, 7, 0.13, gciDamageType_Corroding, "Might of Mycelium", "Florentina|Agarist|MightOfMycelium", 5, 11)
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Florentina|Special"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Florentina.Agarist.MightOfMycelium = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Florentina.Agarist.MightOfMycelium

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
