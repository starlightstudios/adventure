-- |[ ======================================== Dissolve ======================================== ]|
-- |[Description]|
--Chaser attack for Corrode attacks. Deals normal damage as Corrode otherwise. Enemy must have
-- a Corrode DoT to be targeted.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Florentina.Agarist.Dissolve == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Agarist", "Dissolve", "Use Skill Name", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Active", "Florentina|Agarist|Dissolve", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n[Crd] Chaser, target must have a [Crd] DoT.\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Chaser attack that consumes and finishes all[BR][Crd](Corrode) DoTs.\nDeals [Crd](Corrode) damage.\n\n\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(20, giNoCPCost, giNoCooldown, "Target Enemies Single", 1)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Corrode")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(iDamageType, iMissRate, fDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Corroding, 5, 1.00)
    
    --Chaser Modules
  --zAbiStruct.zExecutionAbiPackage:fnAddChaser(pbConsumeEffect, psDoTTag, pfDamageChangePerDoT, pfDamageConsumeFactor)
    zAbiStruct.zExecutionAbiPackage:fnAddChaser(gbConsumeEffects, "Corroding DoT",  0.00, 1.00)
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Florentina|Ability:Dissolve|General:Swing|Offense"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Florentina.Agarist.Dissolve = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Florentina.Agarist.Dissolve 

-- |[ ================================= Combat: Paint Targets ================================== ]|
--This ability can only hit targets afflicted with a Corrode DoT.
if(iSwitchType == gciAbility_PaintTargets) then
    
    --Build initial target listing.
    AdvCombat_SetProperty("Run Target Macro", gzRefAbility.sTargetMacro, AdvCombatAbility_GetProperty("Owner ID"))
    
    --Trim target listing. Target must have the listing effect tag or it cannot be attacked.
    fnTrimTargetsWithoutTag({"Corroding DoT"})
    
-- |[ ==================================== Standard Handler ==================================== ]|
--Call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
