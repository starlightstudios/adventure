-- |[ ========================================= Agarist ======================================== ]|
--Florentina's made a pact with a mushroom and it's time to kick some ass!

-- |[ ========== Argument Handling =========== ]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--1st argument is only used for level-up calls.
local iLevelReached = 0
if(iArgumentsTotal >= 2) then
    iLevelReached = LM_GetScriptArgument(1, "N")
end

-- |[ ============ Variable Setup ============ ]|
-- |[Enumerations]|
local gciNoSkillbooks = 0

-- |[Encapsulation]|
local zEntry = {}

-- |[Basic Variables]|
--Character Variables.
zEntry.sCharacterFormVarPath = "Root/Variables/Global/Florentina/sCurrentJob"
zEntry.sSkillbookVarPath     = "Root/Variables/Global/Florentina/iSkillbookTotal"
zEntry.zaJobChart            = gzaFlorentinaJobChart
zEntry.sStatProfilePath      = gsRoot .. "Combat/Party/Florentina/400 Common Stat Profile.lua"
zEntry.sSecondaryMenuPath    = "Null"
zEntry.sMasterBonusPath      = gsRoot .. "Combat/Party/Florentina/401 Mastery Bonus Handler.lua"

--Class Variables.
zEntry.sClassName   = "Agarist"
zEntry.sFormName    = "Agarist"
zEntry.sCostumeName = "Florentina_Agarist"
zEntry.fFaceIndexX  = 0
zEntry.fFaceIndexY  = 9

-- |[Ability Variables]|
--Purchaseable abilities.
zEntry.saExternalAbilities = {}
table.insert(zEntry.saExternalAbilities, "Acid Wave")
table.insert(zEntry.saExternalAbilities, "Acidic Burst")
table.insert(zEntry.saExternalAbilities, "Dissolve")
table.insert(zEntry.saExternalAbilities, "Feeding Fury")
table.insert(zEntry.saExternalAbilities, "Florentinas Will")
table.insert(zEntry.saExternalAbilities, "Lichen This")
table.insert(zEntry.saExternalAbilities, "Might Of Mycelium")
table.insert(zEntry.saExternalAbilities, "Parasitize")
table.insert(zEntry.saExternalAbilities, "Symbiosis")
    
--Class-equipped abilities.
zEntry.saInternalAbilities = {}
table.insert(zEntry.saInternalAbilities, "Acid Wave")
table.insert(zEntry.saInternalAbilities, "Acidic Burst")
table.insert(zEntry.saInternalAbilities, "Dissolve")
table.insert(zEntry.saInternalAbilities, "Feeding Fury")
table.insert(zEntry.saInternalAbilities, "Might Of Mycelium")
table.insert(zEntry.saInternalAbilities, "Parasitize")
table.insert(zEntry.saInternalAbilities, "Symbiosis")

--Purchaseable abilities, in the order of purchase.
zEntry.saPurchaseAbilities = {}
table.insert(zEntry.saPurchaseAbilities, "Agarist|Acid Wave")
table.insert(zEntry.saPurchaseAbilities, "Agarist|Dissolve")
table.insert(zEntry.saPurchaseAbilities, "Agarist|Acidic Burst")
table.insert(zEntry.saPurchaseAbilities, "Agarist|Feeding Fury")
table.insert(zEntry.saPurchaseAbilities, "Agarist|Parasitize")
table.insert(zEntry.saPurchaseAbilities, "Agarist|Symbiosis")
table.insert(zEntry.saPurchaseAbilities, "Agarist|Might Of Mycelium")
table.insert(zEntry.saPurchaseAbilities, "Agarist|Lichen This")
table.insert(zEntry.saPurchaseAbilities, "Agarist|Florentina's Will")
    
-- |[Skillbar Variables]|
--Skills that always appear on the skillbar. Format: {sAbilityName, iXPos, iYPos, iSkillbooks} (eg: {"Common|Attack", 0, 0, 0}, {"Common|Parry", 1, 0, 0} {})
zEntry.zaSkillbarAbilities = {}
table.insert(zEntry.zaSkillbarAbilities, {"Common|Attack",                        0, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Common|Guard",                         1, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Agarist Internal|Might Of Mycelium",   2, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Agarist Internal|Acidic Burst",        0, 1, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Agarist Internal|Feeding Fury",        1, 1, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Agarist Internal|Dissolve",            2, 1, 1})
table.insert(zEntry.zaSkillbarAbilities, {"Agarist Internal|Acid Wave",           0, 2, 2})
table.insert(zEntry.zaSkillbarAbilities, {"Agarist Internal|Parasitize",          1, 2, 3})
table.insert(zEntry.zaSkillbarAbilities, {"Agarist Internal|Symbiosis",           2, 2, 4})

-- |[ ============ Call Standard ============= ]|
--Add to the global stack. There may be multiple executions of this script.
table.insert(gzJobCallStack, zEntry)

--Call.
LM_ExecuteScript(gsStandardJobPath, iSwitchType, iLevelReached)

--Pop the global stack.
table.remove(gzJobCallStack)
