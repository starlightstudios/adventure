-- |[ ========================================== Guard ========================================= ]|
-- |[Description]|
--Boosts Florentina's protection and evade. Asks for the enemy's passport.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Ability Prototype =================================== ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Florentina.Common.Guard == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Common", "Guard", "$SkillName", gciJP_Cost_NoCost, gbIsNotFreeAction, "Buff", "Active", "Defend", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "+5 [Prt] +45 [Evd] for 1[Turns].\n+30%% [Atk] for 1[Turns].\n\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Defend yourself with your knife and careful\nfootwork.\nIncreases [Prt](Protection) and [Evd](Evade).\nIncreases [Atk](Power) for your next turn."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, giNoCPCost, giNoCooldown, "Target Self", 1)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Protect")
    zAbiStruct.zExecutionAbiPackage:fnSetAsBuff()
  --zAbiStruct.zExecutionAbiPackage:fnAddAnimation(psAnimationName, piTimerOffset, $pfXOffset, $pfYOffset)
    zAbiStruct.zExecutionAbiPackage:fnAddAnimation("Buff Defense", 0)
    
    -- |[Effect Package]|
  --zAbiStruct:fnAddBuff(psPrototypeName, $psApplyText, $psApplyColorString, $psApplyAnim, $psApplySound)
    zAbiStruct:fnAddBuff("Florentina.Common.Guard")

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
        local saDescription = {}
        saDescription[1] = "Protect yourself, increasing [Prt](Protection)"
        saDescription[2] = "and [Evd](Evade)."
        saDescription[3] = "Also increases [Atk](Power) for your next turn."
      --EffectList:fnCreateBuffPrototype(psName, psDisplayName, psIcon, piTurns, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreateBuffPrototype(sLocalPrototypeName, "Guard", "Defend", 2, "Prt|Flat|5 Evd|Flat|45 Atk|Pct|0.30", saDescription)
    end

    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Florentina.Common.Guard = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Florentina.Common.Guard

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
