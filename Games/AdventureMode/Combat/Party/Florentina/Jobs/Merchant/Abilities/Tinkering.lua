-- |[ ======================================== Tinkering ======================================= ]|
-- |[Description]|
--Refills charges on combat items. Costs CP.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Florentina.Merchant.Tinkering == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Merchant", "Tinkering", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Combo", "Florentina|Tinkering", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Refill all item charges to full.\n\n\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Refills the charges on your equipped items.\n\n\n\n\nCosts 2[CPIco](CP)."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, 2, giNoCooldown, "Target Self", giNoCPGeneration)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Healing")
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Florentina|Ability:Tinker"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Add a manual line to the prediction.
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "Refills all item charges to full."
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
    
    --Set prototype.
    gzPrototypes.Combat.Florentina.Merchant.Tinkering = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Florentina.Merchant.Tinkering

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
if(iSwitchType == gciAbility_Execute) then
    
    -- |[ ====== Originator Statistics ======= ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID  = RO_GetID()
        fnModifyCP(-2)
    DL_PopActiveObject()
    
    -- |[ ============= Animation ============ ]|
    --Animate a buff.
    local iTimer = 0
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, iTimer, "Play Sound|Combat\\|Impact_Buff")
    AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, iTimer, "Create Animation|Buff|AttackAnim0")
    iAnimTicks = fnGetAnimationTiming("Buff")
    iTimer = iTimer + iAnimTicks
    fnDefaultEndOfApplications(iTimer)
    
    -- |[ ============== Effect ============== ]|
    --Push user.
    AdvCombat_SetProperty("Push Entity By ID", iOriginatorID)
    
        --Check ability in the Item A slot.
        local sAbilityAName = AdvCombatEntity_GetProperty("Ability In Slot", 8, 0)
        if(sAbilityAName ~= "Null") then
            
            --Fetch ability. Get and set max charges.
            AdvCombatEntity_SetProperty("Push Ability In Slot", 8, 0)
                local iUniqueID = RO_GetID()
                local iChargesMax = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iChargesMax", "N")
                VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iChargesLeft", "N", iChargesMax)
            DL_PopActiveObject()
        end
        
        --Check ability in Item B slot.
        sAbilityAName = AdvCombatEntity_GetProperty("Ability In Slot", 9, 0)
        if(sAbilityAName ~= "Null") then
            
            --Fetch ability. Get and set max charges.
            AdvCombatEntity_SetProperty("Push Ability In Slot", 9, 0)
                local iUniqueID = RO_GetID()
                local iChargesMax = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iChargesMax", "N")
                VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iChargesLeft", "N", iChargesMax)
            DL_PopActiveObject()
        end
    
    DL_PopActiveObject()

-- |[ ==================================== Standard Handler ==================================== ]|
--Call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
