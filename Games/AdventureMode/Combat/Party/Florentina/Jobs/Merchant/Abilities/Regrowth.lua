-- |[ ======================================== Regrowth ======================================== ]|
-- |[Description]|
--Restores 70% of max HP, costs 4 CP.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Florentina.Merchant.Regrowth == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Merchant", "Regrowth", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Heal", "Combo", "Florentina|Regrowth", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Restore 70%% of Max [Hlt]. Self.\n\n\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Regrow yourself, healing a very large amount\nof your health.\nSelf-target only.\n\n\nCosts 4[CPIco](CP)."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, 4, giNoCooldown, "Target Self", giNoCPGeneration)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Healing")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsHealing(piHealingBase, pfHealingScaleFactor, pfHealingPctOfHPMax)
    zAbiStruct.zExecutionAbiPackage:fnSetAsHealing(0, 0.00, 0.70)
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Florentina|Ability:Regrowth"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Florentina.Merchant.Regrowth = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Florentina.Merchant.Regrowth

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
