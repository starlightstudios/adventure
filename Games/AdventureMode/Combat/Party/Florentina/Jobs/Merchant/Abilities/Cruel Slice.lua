-- |[ ======================================= Cruel Slice ====================================== ]|
-- |[Description]|
--Inflict 30% bleed damage and 130% additional bleed damage over 3 turns.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Florentina.Merchant.CruelSlice == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Merchant", "Cruel Slice", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "DoT", "Active", "Florentina|CruelSlice", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\n[HEffect][Str6] DoT of [0.80x [Atk]] [Bld] over 3[Turns].\n[HEffect][Str6] DoT of [0.80x [Atk]] [Psn] over 3[Turns].\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "A deep cut that inflicts increased [Bld](Bleed)\nand [Psn](Poison) damage over 3 turns.\n\n\n\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(30, giNoCPCost, giNoCooldown, "Target Enemies Single", 1)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Sword Slash")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_UseWeapon, 5, 0.70)
    
    -- |[Execution Effect Package]|
  --zAbiStruct:fnAddEffect(piStrNormal, piStrCrit, piApplyType, psApplyText, psCritApplyText, psApplyColorString, psaApplyBonus, psaApplyMalus, psaSeverityBonus, psaSeverityMalus)
    zAbiStruct:fnAddEffect(6, 9, gciDamageType_Bleeding,  "Bleeding!", "Badly Bleeding!", "Color:Red", {"Bleed Standard"})
    zAbiStruct:fnAddEffect(6, 9, gciDamageType_Poisoning, "Poisoned!", "Badly Poisoned!", "Color:Red", {"Poison Standard"})
    
    -- |[Use of Effect Prototype]|
    zAbiStruct.zExecutionEffPackage.sPrototypeName  = "Florentina.Merchant.CruelSliceBleed"
    zAbiStruct.zExecutionEffPackageB.sPrototypeName = "Florentina.Merchant.CruelSlicePoison"

    -- |[ ========= Effect Prototype ========= ]|
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists("Florentina.Merchant.CruelSliceBleed") == false) then
      --EffectList:fnCreateDoTPrototype(psName, piTurns, piApplyStrength, pfDamPerTurn, piDamageType, psDisplayName, psIcon, piCritTurns, piCritApplyStrength)
        EffectList:fnCreateDoTPrototype("Florentina.Merchant.CruelSliceBleed", 3, 6, 0.27, gciDamageType_Bleeding, "Cruel Slice Bld", "Florentina|CruelSlice", 5, 9)
    end
    if(EffectList:fnEntryExists("Florentina.Merchant.CruelSlicePoison") == false) then
      --EffectList:fnCreateDoTPrototype(psName, piTurns, piApplyStrength, pfDamPerTurn, piDamageType, psDisplayName, psIcon, piCritTurns, piCritApplyStrength)
        EffectList:fnCreateDoTPrototype("Florentina.Merchant.CruelSlicePoison", 3, 6, 0.27, gciDamageType_Poisoning, "Cruel Slice Psn", "Florentina|CruelSlice", 5, 9)
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Florentina|General:Swing|Offense"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Florentina.Merchant.CruelSlice = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Florentina.Merchant.CruelSlice

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
