-- |[ ======================================= Intimidate ======================================= ]|
-- |[Description]|
--Increases threat, deals terrify damage to all targets, reduces enemy accuracy.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Florentina.Merchant.Intimidate == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Merchant", "Intimidate", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Debuff", "Active", "Florentina|Intimidate", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\n[HEffect][Str7][Tfy] -50%%[Atk] / 3[Turns].\n+100%%[Atk] as [Threat].\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Put the fear of Florentina into 'em!\nInflicts reduced [Tfy](Terrify) damage, all\nenemies.\nReduces [Atk](Power) for 5 turns.\nIncreases chance of being targeted.\n"..
                                      "Costs [MPCost][MPIco](MP)."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(20, giNoCPCost, giNoCooldown, "Target Enemies All", 1)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Terrify")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Terrifying, 5, 0.50)
    
    --Threat modifier. Applies even if the attack misses.
    zAbiStruct:fnSetThreatFactor(2.0)
    
    -- |[Execution Effect Package]|
  --zAbiStruct:fnAddEffect(piStrNormal, piStrCrit, piApplyType, psApplyText, psCritApplyText, psApplyColorString, psaApplyBonus, psaApplyMalus, psaSeverityBonus, psaSeverityMalus)
    zAbiStruct:fnAddEffect(7, 11, gciDamageType_Terrifying, "Frightened!", "Badly Frightened!", "Color:Purple")
    
    -- |[Use of Effect Prototype]|
    zAbiStruct.zExecutionEffPackage.sPrototypeName = "Florentina.Merchant.Intimidate"

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
        local saDescription = {}
        saDescription[1] = "She's small, but she'll kick your ass."
        saDescription[2] = "Cower in fear!"
        saDescription[3] = "Greatly decreases [Atk](Power)."
      --EffectList:fnCreateStatPrototype(pzEffectpackage, psName, pbIsBuff, psDisplayName, psIcon, piTurns, piApplyStrength, psStatString, psaDescription, $pzaTagList, $piCritTurns, $piCritApplyStrength)
        EffectList:fnCreateStatPrototype(zAbiStruct.zExecutionEffPackage, sLocalPrototypeName, gbIsDebuff, "Intimidate", "Florentina|Intimidate", 3, 7, "Atk|Pct|-0.50", saDescription)
    end

    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Florentina|Ability:Intimidate"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Florentina.Merchant.Intimidate = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Florentina.Merchant.Intimidate

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
