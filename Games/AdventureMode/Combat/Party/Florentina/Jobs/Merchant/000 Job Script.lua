-- |[ ======================================== Merchant ======================================== ]|
--Florentina's starting class in chapter 1. Armed with a hunting knife, she has several violence
-- abilities and several vocal abilities.

-- |[ ========== Argument Handling =========== ]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--1st argument is only used for level-up calls.
local iLevelReached = 0
if(iArgumentsTotal >= 2) then
    iLevelReached = LM_GetScriptArgument(1, "N")
end

-- |[ ============ Variable Setup ============ ]|
-- |[Enumerations]|
local gciNoSkillbooks = 0

-- |[Encapsulation]|
local zEntry = {}

-- |[Basic Variables]|
--Character Variables.
zEntry.sCharacterFormVarPath = "Root/Variables/Global/Florentina/sCurrentJob"
zEntry.sSkillbookVarPath     = "Root/Variables/Global/Florentina/iSkillbookTotal"
zEntry.zaJobChart            = gzaFlorentinaJobChart
zEntry.sStatProfilePath      = gsRoot .. "Combat/Party/Florentina/400 Common Stat Profile.lua"
zEntry.sSecondaryMenuPath    = "Null"
zEntry.sMasterBonusPath      = gsRoot .. "Combat/Party/Florentina/401 Mastery Bonus Handler.lua"

--Class Variables.
zEntry.sClassName   = "Merchant"
zEntry.sFormName    = "Merchant"
zEntry.sCostumeName = "Florentina_Merchant"
zEntry.fFaceIndexX  = 0
zEntry.fFaceIndexY  = 6

-- |[Ability Variables]|
--Purchaseable abilities.
zEntry.saExternalAbilities = {}
table.insert(zEntry.saExternalAbilities, "Dripping Blade")
table.insert(zEntry.saExternalAbilities, "Vine Wrap")
table.insert(zEntry.saExternalAbilities, "Intimidate")
table.insert(zEntry.saExternalAbilities, "Drain Vitality")
table.insert(zEntry.saExternalAbilities, "Cruel Slice")
table.insert(zEntry.saExternalAbilities, "Regrowth")
table.insert(zEntry.saExternalAbilities, "Critical Stab")
table.insert(zEntry.saExternalAbilities, "Tinkering")
table.insert(zEntry.saExternalAbilities, "Florentinas Guile")
    
--Class-equipped abilities.
zEntry.saInternalAbilities = {}
table.insert(zEntry.saInternalAbilities, "Dripping Blade")
table.insert(zEntry.saInternalAbilities, "Vine Wrap")
table.insert(zEntry.saInternalAbilities, "Intimidate")
table.insert(zEntry.saInternalAbilities, "Drain Vitality")
table.insert(zEntry.saInternalAbilities, "Cruel Slice")
table.insert(zEntry.saInternalAbilities, "Florentinas Guile")
table.insert(zEntry.saInternalAbilities, "Critical Stab")

--Purchaseable abilities, in the order of purchase.
zEntry.saPurchaseAbilities = {}
table.insert(zEntry.saPurchaseAbilities, "Merchant|Dripping Blade")
table.insert(zEntry.saPurchaseAbilities, "Merchant|Cruel Slice")
table.insert(zEntry.saPurchaseAbilities, "Merchant|Drain Vitality")
table.insert(zEntry.saPurchaseAbilities, "Merchant|Vine Wrap")
table.insert(zEntry.saPurchaseAbilities, "Merchant|Intimidate")
table.insert(zEntry.saPurchaseAbilities, "Merchant|Regrowth")
table.insert(zEntry.saPurchaseAbilities, "Merchant|Tinkering")
table.insert(zEntry.saPurchaseAbilities, "Merchant|Critical Stab")
table.insert(zEntry.saPurchaseAbilities, "Merchant|Florentina's Guile")
    
-- |[Skillbar Variables]|
--Skills that always appear on the skillbar. Format: {sAbilityName, iXPos, iYPos, iSkillbooks} (eg: {"Common|Attack", 0, 0, 0}, {"Common|Parry", 1, 0, 0} {})
zEntry.zaSkillbarAbilities = {}
table.insert(zEntry.zaSkillbarAbilities, {"Common|Attack",                        0, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Common|Guard",                         1, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Merchant Internal|Critical Stab",      2, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Merchant Internal|Dripping Blade",     0, 1, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Merchant Internal|Vine Wrap",          1, 1, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Merchant Internal|Intimidate",         2, 1, 1})
table.insert(zEntry.zaSkillbarAbilities, {"Merchant Internal|Drain Vitality",     0, 2, 2})
table.insert(zEntry.zaSkillbarAbilities, {"Merchant Internal|Cruel Slice",        1, 2, 3})
table.insert(zEntry.zaSkillbarAbilities, {"Merchant Internal|Florentina's Guile", 2, 2, 4})

-- |[ ============ Call Standard ============= ]|
--Add to the global stack. There may be multiple executions of this script.
table.insert(gzJobCallStack, zEntry)

--Call.
LM_ExecuteScript(gsStandardJobPath, iSwitchType, iLevelReached)

--Pop the global stack.
table.remove(gzJobCallStack)
