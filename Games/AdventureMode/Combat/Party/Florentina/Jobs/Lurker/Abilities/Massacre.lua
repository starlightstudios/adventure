-- |[ ======================================== Massacre ======================================== ]|
-- |[Description]|
--Chaser CP finisher. Deals major pierce damage, extra if enemy is bleeding.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Florentina.Lurker.Massacre == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Lurker", "Massacre", "Use Skill Name", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Combo", "Florentina|Lurker|Massacre", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\nExtra +25[PCT] damage if enemy has [Bld] DoT.\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "A singly powerful attack that deals extra[BR]damage if the enemy is bleeding.\nDeals [Prc](Pierce) damage.\n\n[BR]Costs [CPCost][CPIco](CP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, 4, giNoCooldown, "Target Enemies Single", giNoCPGeneration)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Pierce")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(iDamageType, iMissRate, fDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Piercing, 5, 1.70)
  --zAbiStruct.zExecutionAbiPackage:fnAddChaser(pbConsumeEffect, psDoTTag, pfDamageChangePerDoT, pfDamageConsumeFactor)
    zAbiStruct.zExecutionAbiPackage:fnAddChaser(gbDoNotConsumeEffects, "Bleeding DoT", 0.425, 0.0)
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Florentina|Special"
    
    -- |[Prediction Builder]|
    zAbiStruct.zPredictionPackage = fnCreatePredictionFromAbility(zAbiStruct)

    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzPrototypes.Combat.Florentina.Lurker.Massacre = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Florentina.Lurker.Massacre

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
