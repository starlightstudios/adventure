-- |[ ======================================= Resin Skin ======================================= ]|
-- |[Description]|
--Passive, increases Florentina's pierce, poison, and bleed resist by 2.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Florentina.Lurker.ResinSkin == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Lurker", "Resin Skin", "$SkillName", gciJP_Cost_Passive, gbIsNotFreeAction, "Buff", "Passive", "Florentina|Lurker|ResinSkin", "Passive")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "+2 [Prc][Psn][Bld] resist.\n\n\n\n\n\nPassive."
    zAbiStruct.sSimpleDescMarkdown  = "Increases [Prc](Pierce), [Psn](Poison), and [Bld](Bleed)[BR]resistance.\n\n\n\nPassive."

    -- |[Passive Effect]|
    zAbiStruct.bNeverAvailable   = true --Passive abilities have no usability requirements.
    zAbiStruct.sPassivePrototype = "Florentina.Lurker.ResinSkin"
    zAbiStruct.bHasGUIEffects    = true

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    if(EffectList:fnEntryExists(zAbiStruct.sPassivePrototype) == false) then
        local saDescription = {}
        saDescription[1] = "Your skin incorporates mannequin resin."
        saDescription[2] = "Increases [Prc][Psn][Bld] resist by 2."
      --EffectList:fnCreatePassivePrototype(psName, psDisplayName, psIcon, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreatePassivePrototype(zAbiStruct.sPassivePrototype, "Resin Skin", "Florentina|Lurker|ResinSkin", "ResPrc|Flat|2 ResPsn|Flat|2 ResBld|Flat|2", saDescription)
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Florentina.Lurker.ResinSkin = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Florentina.Lurker.ResinSkin

-- |[ ===================================== All Other Cases ==================================== ]|
if(iSwitchType ~= gciAbility_GUIApplyEffect) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

-- |[ ==================================== GUI: Apply Effect =================================== ]|
--When showing a character's statistics, all equipped abilities are queried. This is not reflected
-- in combat, this is GUI only.
--The standard does not handle this.
elseif(iSwitchType == gciAbility_GUIApplyEffect) then
    AdvCombatAbility_SetProperty("Push Owner")
        fnApplyStatBonus(gciStatIndex_Resist_Pierce, 2)
        fnApplyStatBonus(gciStatIndex_Resist_Bleed, 2)
        fnApplyStatBonus(gciStatIndex_Resist_Poison, 2)
    DL_PopActiveObject()
    gzRefAbility = nil
end
