-- |[ ==================================== Relieve Burdens ===================================== ]|
-- |[Description]|
--Inflicts -5 protection for 3 turns, and steals platina.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Florentina.Lurker.RelieveBurdens == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Lurker", "Relieve Burdens", "Use Skill Name", gciJP_Cost_Normal, gbIsFreeAction, "Debuff", "Active", "Florentina|Lurker|RelieveBurdens", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Steals platina equal to 0.50x [Atk].\n\n[HEffect][Str5][Prt] -5[Prt] / 1[Turns].\nCan steal once per target.\n\nFree action.\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Take the opportunity of being a lifeless,[BR]unmoving thing to help yourself to some money.\nReduces [Prt](Protection) for one turn.\nSteals platina based on [Atk](Power).\n"..
                                      "Free Action.\nCosts no MP!"

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, giNoCPCost, 1, "Target Enemies Single", giNoCPGeneration)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Pierce")
    fnSetAbilityExecutionDebuff(zAbiStruct)
    
    --No benefit from the "Called Shot" ability.
    zAbiStruct.zExecutionAbiPackage.zaTags = {{"Florentina No Called Shot", 1}}
    
    -- |[Execution Effect Package]|
  --zAbiStruct:fnAddEffect(piStrNormal, piStrCrit, piApplyType, psApplyText, psCritApplyText, psApplyColorString, psaApplyBonus, psaApplyMalus, psaSeverityBonus, psaSeverityMalus)
    zAbiStruct:fnAddEffect(5, 8, gciDamageType_Piercing, "Vulnerable!", "Really Vulnerable!", "Color:Purple")
    
    -- |[Use of Effect Prototype]|
    zAbiStruct.zExecutionEffPackage.sPrototypeName = "Florentina.Lurker.RelieveBurdens"

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
        local saDescription = {}
        saDescription[1] = "The enemy just noticed their money is"
        saDescription[2] = "gone, attack!."
        saDescription[3] = "Reduced [Prt](Protection) until the next action."
      --EffectList:fnCreateStatPrototype(pzEffectpackage, psName, pbIsBuff, psDisplayName, psIcon, piTurns, piApplyStrength, psStatString, psaDescription, pzaTagList, piCritTurns, piCritApplyStrength)
        EffectList:fnCreateStatPrototype(zAbiStruct.zExecutionEffPackage, "Florentina.Lurker.RelieveBurdens", gbIsDebuff, "Relieve Burdens", "Florentina|Lurker|RelieveBurdens", 1, 5, "Prt|Flat|-5", saDescription)
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Florentina|Ability:Pickpocket"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Florentina.Lurker.RelieveBurdens = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Florentina.Lurker.RelieveBurdens

-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--Specify how much platina gets stolen.
if(iSwitchType == gciAbility_BuildPredictionBox) then

    -- |[ ===================== Other Values ===================== ]|
    --Get originator ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
        local iAttackPower = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
        local bIsStickyFingersEquipped = AdvCombatEntity_GetProperty("Is Ability Equipped", "TreasureHunter|Sticky Fingers")
    DL_PopActiveObject()
    
    --Get information about the cluster. This is needed to give each box a unique ID.
    local iClusterID = AdvCombat_GetProperty("Prediction Get Cluster ID")
    
    -- |[ ==================== For Each Target =================== ]|
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    for i = 0, iClusterTargetsTotal-1, 1 do
        
        --Target ID.
        local iTargetID = AdvCombat_GetProperty("Prediction Get Targets ID", i)
        
        --Check if we already pickpocketed the enemy.
        local iHasPickpocketed = VM_GetVar("Root/Variables/Combat/" .. iTargetID .. "/iWasPickpocketed", "N")
        if(iHasPickpocketed == 0.0) then
            
            --Determine amount stolen.
            local iPickPocketPower = math.floor(iAttackPower * 0.50)
            if(bIsStickyFingersEquipped) then iPickPocketPower = math.floor(iPickPocketPower * 1.15) end
            if(iPickPocketPower < 1) then iPickPocketPower = 1 end
            
            --Display.
            gzRefAbility.zPredictionPackage.zaAdditionalLines[1] = {}
            gzRefAbility.zPredictionPackage.zaAdditionalLines[1].sString = ""
            gzRefAbility.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
            gzRefAbility.zPredictionPackage.zaAdditionalLines[2] = {}
            gzRefAbility.zPredictionPackage.zaAdditionalLines[2].sString = "Steal " .. iPickPocketPower .. " platina."
            gzRefAbility.zPredictionPackage.zaAdditionalLines[2].saSymbols = {}
            if(bIsStickyFingersEquipped) then
                gzRefAbility.zPredictionPackage.zaAdditionalLines[3] = {}
                gzRefAbility.zPredictionPackage.zaAdditionalLines[3].sString = "+15%% bonus from Sticky Fingers!"
                gzRefAbility.zPredictionPackage.zaAdditionalLines[3].saSymbols = {}
            end
        
        --Already pickpocketed.
        else
            gzRefAbility.zPredictionPackage.zaAdditionalLines[1] = {}
            gzRefAbility.zPredictionPackage.zaAdditionalLines[1].sString = "Already pickpocketed!"
            gzRefAbility.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
        end
        
        fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, gzRefAbility.zPredictionPackage)
    end

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[ ================ Originator Statistics ================= ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        local iAttackPower = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
    DL_PopActiveObject()
    
    -- |[ ===================== Set Cooldown ===================== ]|
    --Apply cooldown for this ability. Mark the action as free.
    AdvCombatAbility_SetProperty("Cooldown", 1)
    AdvCombat_SetProperty("Set As Free Action")
    
    -- |[ ====================== Provide IDs ===================== ]|
    --Provide IDs to all packages.
    gzRefAbility.zExecutionAbiPackage.iOriginatorID = iOriginatorID
    gzRefAbility.zExecutionEffPackage.iOriginatorID = iOriginatorID
    
    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        fnStandardExecution(iOriginatorID, i-1, gzRefAbility.zExecutionAbiPackage, gzRefAbility.zExecutionEffPackage)
        
        --Get if the target was pickpocketed:
        AdvCombat_SetProperty("Push Target", i-1) 
            local iTargetUniqueID = RO_GetID()
        DL_PopActiveObject()
        
        --Check if we already pickpocketed the enemy.
        local iHasPickpocketed = VM_GetVar("Root/Variables/Combat/" .. iTargetUniqueID .. "/iWasPickpocketed", "N")
        if(iHasPickpocketed == 0.0 and gbAttackHit) then
            
            --Flag.
            VM_SetVar("Root/Variables/Combat/" .. iTargetUniqueID .. "/iWasPickpocketed", "N", 1.0)
            
            --Determine amount stolen.
            local iPickPocketPower = math.floor(iAttackPower * 0.50)
            if(iPickPocketPower < 1) then iPickPocketPower = 1 end
            
            --Store.
            DL_AddPath("Root/Variables/Combat/Default/")
            VM_SetVar("Root/Variables/Combat/Default/iPickPocket", "N", iPickPocketPower)
            
            --Report the steal value.
            local sScriptPath = LM_GetCallStack(0)
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, 45, "Run Script|" .. sScriptPath .. "|N:-1000")
        end
    end
    
-- |[ ======================================== Cutscene ======================================== ]|
--Special
elseif(iSwitchType == -1000) then

    --Stole string.
    local iStoleValue = math.floor(VM_GetVar("Root/Variables/Combat/Default/iPickPocket", "N"))
    local sString = "WD_SetProperty(\"Append\", \"Florentina: Stole " .. iStoleValue .. " platina!\")"

    --Base.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene(sString)
    fnCutsceneBlocker()
        
-- |[ ==================================== Standard Handler ==================================== ]|
--Call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
