-- |[ =================================== Florentina's Terror ================================== ]|
-- |[Description]|
--Passive, increases Bleed and Poison damage by 15%.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Florentina.Lurker.FlorentinasTerror == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Lurker", "Florentina's Terror", "Florentina's Terror", gciJP_Cost_Passive, gbIsNotFreeAction, "Buff", "Passive", "Florentina|Lurker|FlorentinasTerror", "Passive")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "+15[PCT][Bld], +15[PCT][Psn] damage.\n\n\n\n\n\nPassive."
    zAbiStruct.sSimpleDescMarkdown  = "Increases [Bld](Bleed) and [Psn](Poison) damage[BR]by 15[PCT].\n\n\n\nPassive."

    -- |[Passive Effect]|
    zAbiStruct.bNeverAvailable   = true --Passive abilities have no usability requirements.
    zAbiStruct.sPassivePrototype = "Florentina.Lurker.FlorentinasTerror"
    zAbiStruct.bHasGUIEffects    = false
    
    -- |[Tags]|
    --Apply tags to increase slashing damage.
    zAbiStruct.zaTags = {{"Bleed Damage Dealt +", 15}, {"Poison Damage Dealt +", 15}}

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    -- Note: The effect doesn't increase damage, the ability itself does.
    if(EffectList:fnEntryExists(zAbiStruct.sPassivePrototype) == false) then
        local saDescription = {}
        saDescription[1] = "Florentina is just as scary but now resin."
        saDescription[2] = "Increases [Bld][Psn] damage by 15[PCT]."
      --EffectList:fnCreatePassivePrototype(psName, psDisplayName, psIcon, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreatePassivePrototype(zAbiStruct.sPassivePrototype, "Florentina's Terror", "Florentina|Lurker|FlorentinasTerror", "", saDescription)

        --Manually specify description.
        local iType, zPrototype, iSlot = EffectList:fnLocateEntry(zAbiStruct.sPassivePrototype)
        if(zPrototype ~= nil) then
            zPrototype.bDontRebuildDescription = true
            zPrototype.saStrings[1] = "[Buff]Florentina's Terror"
            zPrototype.saStrings[2] = "[UpN][Bld][Psn]"
            zPrototype.saStrings[3] = saDescription[1]
            zPrototype.saStrings[4] = saDescription[2]
            zPrototype.saStrings[5] = saDescription[3]
            zPrototype.sShortText = ""
        end
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Florentina.Lurker.FlorentinasTerror = zAbiStruct
end

-- |[ ======================================= Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Florentina.Lurker.FlorentinasTerror

--Call.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
