-- |[ ========================================= Lurker ========================================= ]|
--Fools! Florentina being transformed into a mannequin is actually worse for you! Now you have no chance!

-- |[ ========== Argument Handling =========== ]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--1st argument is only used for level-up calls.
local iLevelReached = 0
if(iArgumentsTotal >= 2) then
    iLevelReached = LM_GetScriptArgument(1, "N")
end

-- |[ ============ Variable Setup ============ ]|
-- |[Enumerations]|
local gciNoSkillbooks = 0

-- |[Encapsulation]|
local zEntry = {}

-- |[Basic Variables]|
--Character Variables.
zEntry.sCharacterFormVarPath = "Root/Variables/Global/Florentina/sCurrentJob"
zEntry.sSkillbookVarPath     = "Root/Variables/Global/Florentina/iSkillbookTotal"
zEntry.zaJobChart            = gzaFlorentinaJobChart
zEntry.sStatProfilePath      = gsRoot .. "Combat/Party/Florentina/400 Common Stat Profile.lua"
zEntry.sSecondaryMenuPath    = "Null"
zEntry.sMasterBonusPath      = gsRoot .. "Combat/Party/Florentina/401 Mastery Bonus Handler.lua"

--Class Variables.
zEntry.sClassName   = "Lurker"
zEntry.sFormName    = "Lurker"
zEntry.sCostumeName = "Florentina_Lurker"
zEntry.fFaceIndexX  = 0
zEntry.fFaceIndexY  = 10

-- |[Ability Variables]|
--Purchaseable abilities.
zEntry.saExternalAbilities = {}
table.insert(zEntry.saExternalAbilities, "Appear Lifeless")
table.insert(zEntry.saExternalAbilities, "Dont Blink")
table.insert(zEntry.saExternalAbilities, "Florentinas Terror")
table.insert(zEntry.saExternalAbilities, "Massacre")
table.insert(zEntry.saExternalAbilities, "Relentless")
table.insert(zEntry.saExternalAbilities, "Relieve Burdens")
table.insert(zEntry.saExternalAbilities, "Resin Skin")
table.insert(zEntry.saExternalAbilities, "Strangle")
table.insert(zEntry.saExternalAbilities, "Unliving Grasp")
    
--Class-equipped abilities.
zEntry.saInternalAbilities = {}
table.insert(zEntry.saInternalAbilities, "Appear Lifeless")
table.insert(zEntry.saInternalAbilities, "Dont Blink")
table.insert(zEntry.saInternalAbilities, "Massacre")
table.insert(zEntry.saInternalAbilities, "Relentless")
table.insert(zEntry.saInternalAbilities, "Relieve Burdens")
table.insert(zEntry.saInternalAbilities, "Strangle")
table.insert(zEntry.saInternalAbilities, "Unliving Grasp")

--Purchaseable abilities, in the order of purchase.
zEntry.saPurchaseAbilities = {}
table.insert(zEntry.saPurchaseAbilities, "Lurker|Strangle")
table.insert(zEntry.saPurchaseAbilities, "Lurker|Unliving Grasp")
table.insert(zEntry.saPurchaseAbilities, "Lurker|Relentless")
table.insert(zEntry.saPurchaseAbilities, "Lurker|Don't Blink")
table.insert(zEntry.saPurchaseAbilities, "Lurker|Appear Lifeless")
table.insert(zEntry.saPurchaseAbilities, "Lurker|Relieve Burdens")
table.insert(zEntry.saPurchaseAbilities, "Lurker|Massacre")
table.insert(zEntry.saPurchaseAbilities, "Lurker|Resin Skin")
table.insert(zEntry.saPurchaseAbilities, "Lurker|Florentina's Terror")
    
-- |[Skillbar Variables]|
--Skills that always appear on the skillbar. Format: {sAbilityName, iXPos, iYPos, iSkillbooks} (eg: {"Common|Attack", 0, 0, 0}, {"Common|Parry", 1, 0, 0} {})
zEntry.zaSkillbarAbilities = {}
table.insert(zEntry.zaSkillbarAbilities, {"Common|Attack",                   0, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Common|Guard",                    1, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Lurker Internal|Massacre",        2, 0, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Lurker Internal|Strangle",        0, 1, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Lurker Internal|Relentless",      1, 1, gciNoSkillbooks})
table.insert(zEntry.zaSkillbarAbilities, {"Lurker Internal|Unliving Grasp",  2, 1, 1})
table.insert(zEntry.zaSkillbarAbilities, {"Lurker Internal|Relieve Burdens", 0, 2, 2})
table.insert(zEntry.zaSkillbarAbilities, {"Lurker Internal|Appear Lifeless", 1, 2, 3})
table.insert(zEntry.zaSkillbarAbilities, {"Lurker Internal|Don't Blink",     2, 2, 4})

-- |[ ============ Call Standard ============= ]|
--Add to the global stack. There may be multiple executions of this script.
table.insert(gzJobCallStack, zEntry)

--Call.
LM_ExecuteScript(gsStandardJobPath, iSwitchType, iLevelReached)

--Pop the global stack.
table.remove(gzJobCallStack)
