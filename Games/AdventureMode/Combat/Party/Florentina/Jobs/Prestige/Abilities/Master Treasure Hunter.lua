-- |[ ================================= Master Treasure Hunter ================================= ]|
-- |[Description]|
--Passive, increases Bleed and Poison damage by 5%. Does not need to be equipped, requires all 
-- other Treasure Hunter skills to be purchased before it unlocks automatically.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Florentina.Prestige.MasterTreasureHunter == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Prestige", "Master Treasure Hunter", "$SkillName", gciJP_Cost_Locked, gbIsNotFreeAction, "Buff", "Passive", "Florentina|FlorentinasInstinct", "Passive")
    
    --Runs even if not equipped, but must be unlocked.
    zAbiStruct.bRunIfNotEquipped = true
    zAbiStruct.bOnlyRunIfUnlocked = true
    
    --Permanent passives can never be equipped.
    zAbiStruct.bCannotBeEquipped = true
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "+5[PCT] [Bld][Psn] damage.\n\n\n\n\n\nPermanent passive."
    zAbiStruct.sSimpleDescMarkdown  = "Increases [Bld](Bleed) and [Psn](Poison) damage\npermanently by 5[PCT].\nDoes not need to be equipped.\nYou must master the Treasure Hunter class to\nunlock this ability.\nPassive."
    
    -- |[Passive Effect]|
    zAbiStruct.bNeverAvailable   = true --Passive abilities have no usability requirements.
    zAbiStruct.sPassivePrototype = "Mei.Prestige.MasterTreasureHunter"
    zAbiStruct.bHasGUIEffects    = true
    
    -- |[Tags]|
    --Apply tags to increase slashing damage.
    zAbiStruct.zaTags = {{"Bleed Damage Dealt +", 5}, {"Poison Damage Dealt +", 5}}

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    -- Note: The effect doesn't increase slash damage, the ability itself does.
    if(EffectList:fnEntryExists(zAbiStruct.sPassivePrototype) == false) then
        local saDescription = {}
        saDescription[1] = "You have mastered the Treasure Hunter class."
        saDescription[2] = "Increases Bleed and Poison damage. Passive."
      --EffectList:fnCreatePassivePrototype(psName, psDisplayName, psIcon, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreatePassivePrototype(zAbiStruct.sPassivePrototype, "Master Treasure Hunter", "Florentina|FlorentinasInstinct", "", saDescription)
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Florentina.Prestige.MasterTreasureHunter = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Florentina.Prestige.MasterTreasureHunter

-- |[ ===================================== All Other Cases ==================================== ]|
if(iSwitchType ~= gciAbility_GUIApplyEffect) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

-- |[ ==================================== GUI: Apply Effect =================================== ]|
--When showing a character's statistics, all equipped abilities are queried. This is not reflected
-- in combat, this is GUI only.
--The standard does not handle this.
elseif(iSwitchType == gciAbility_GUIApplyEffect) then
    --AdvCombatAbility_SetProperty("Push Owner")
    --    fnApplyStatBonusPct(gciStatIndex_HPMax, 0.05)
    --DL_PopActiveObject()
    gzRefAbility = nil
end
