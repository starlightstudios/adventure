-- |[ ===================================== Master Agarist ===================================== ]|
-- |[Description]|
--Passive, increases protection by 2, evade by 7. Does not need to be equipped, requires all other
-- Agarist skills to be purchased before it unlocks automatically.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Florentina.Prestige.MasterAgarist == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Prestige", "Master Agarist", "$SkillName", gciJP_Cost_Locked, gbIsNotFreeAction, "Buff", "Passive", "Florentina|Agarist|FlorentinasWill", "Passive")
    
    --Runs even if not equipped, but must be unlocked.
    zAbiStruct.bRunIfNotEquipped = true
    zAbiStruct.bOnlyRunIfUnlocked = true
    
    --Permanent passives can never be equipped.
    zAbiStruct.bCannotBeEquipped = true
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "+2[Prt], +7[Evd].\n\n\n\n\n\nPermanent passive."
    zAbiStruct.sSimpleDescMarkdown  = "Increases Protection by 2, Evade by 7.\nDoes not need to be equipped.\n\nYou must master the Agarist class to unlock\nthis ability.\nPassive."
    
    -- |[Passive Effect]|
    zAbiStruct.bNeverAvailable   = true --Passive abilities have no usability requirements.
    zAbiStruct.sPassivePrototype = "Mei.Prestige.MasterAgarist"
    zAbiStruct.bHasGUIEffects    = true
    
    -- |[Tags]|
    --Apply tags to increase slashing damage.
    --zAbiStruct.zaTags = {{"Bleed Damage Dealt +", 5}, {"Poison Damage Dealt +", 5}}

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    -- Note: The effect doesn't increase slash damage, the ability itself does.
    if(EffectList:fnEntryExists(zAbiStruct.sPassivePrototype) == false) then
        local saDescription = {}
        saDescription[1] = "You have mastered the Agarist class."
        saDescription[2] = "Increases protection and evade. Passive."
      --EffectList:fnCreatePassivePrototype(psName, psDisplayName, psIcon, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreatePassivePrototype(zAbiStruct.sPassivePrototype, "Master Agarist", "Florentina|Agarist|FlorentinasWill", "Prt|Flat|2 Evd|Flat|7", saDescription)
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Florentina.Prestige.MasterAgarist = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Florentina.Prestige.MasterAgarist

-- |[ ===================================== All Other Cases ==================================== ]|
if(iSwitchType ~= gciAbility_GUIApplyEffect) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

-- |[ ==================================== GUI: Apply Effect =================================== ]|
--When showing a character's statistics, all equipped abilities are queried. This is not reflected
-- in combat, this is GUI only.
--The standard does not handle this.
elseif(iSwitchType == gciAbility_GUIApplyEffect) then
    AdvCombatAbility_SetProperty("Push Owner")
        fnApplyStatBonus(gciStatIndex_Protection, 2)
        fnApplyStatBonus(gciStatIndex_Evade, 7)
    DL_PopActiveObject()
    gzRefAbility = nil
end
