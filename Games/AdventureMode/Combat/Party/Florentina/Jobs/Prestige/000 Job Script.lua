-- |[ ======================================== Prestige ======================================== ]|
--Prestige class. Cannot be used, contains passives that only unlock when all skills are purchased
-- from base classes and do not need to be equipped.

-- |[ ========== Argument Handling =========== ]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

--1st argument is only used for level-up calls.
local iLevelReached = 0
if(iArgumentsTotal >= 2) then
    iLevelReached = LM_GetScriptArgument(1, "N")
end

-- |[ ============ Variable Setup ============ ]|
-- |[Enumerations]|
local gciNoSkillbooks = 0

-- |[Encapsulation]|
local zEntry = {}

-- |[Basic Variables]|
--Character Variables.
zEntry.sCharacterFormVarPath = "Root/Variables/Global/Florentina/sCurrentJob"
zEntry.sSkillbookVarPath     = "Root/Variables/Global/Florentina/iSkillbookTotal"
zEntry.zaJobChart            = gzaFlorentinaJobChart
zEntry.sStatProfilePath      = gsRoot .. "Combat/Party/Florentina/400 Common Stat Profile.lua"
zEntry.sSecondaryMenuPath    = "Null"
zEntry.sMasterBonusPath      = gsRoot .. "Combat/Party/Florentina/401 Mastery Bonus Handler.lua"

--Class Variables.
zEntry.sClassName   = "Prestige"
zEntry.sFormName    = "Merchant"
zEntry.sCostumeName = "Florentina_Merchant"
zEntry.fFaceIndexX  = 0
zEntry.fFaceIndexY  = 6

-- |[Ability Variables]|
--Purchaseable abilities.
zEntry.saExternalAbilities = {}
table.insert(zEntry.saExternalAbilities, "Master Merchant")
table.insert(zEntry.saExternalAbilities, "Master Mediator")
table.insert(zEntry.saExternalAbilities, "Master Treasure Hunter")
table.insert(zEntry.saExternalAbilities, "Master Lurker")
table.insert(zEntry.saExternalAbilities, "Master Agarist")

--Class-equipped abilities.
zEntry.saInternalAbilities = {}

--Purchaseable abilities, in the order of purchase.
zEntry.saPurchaseAbilities = {}
table.insert(zEntry.saPurchaseAbilities, "Prestige|Master Merchant")
table.insert(zEntry.saPurchaseAbilities, "Prestige|Master Mediator")
table.insert(zEntry.saPurchaseAbilities, "Prestige|Master Treasure Hunter")
table.insert(zEntry.saPurchaseAbilities, "Prestige|Master Lurker")
table.insert(zEntry.saPurchaseAbilities, "Prestige|Master Agarist")

-- |[Skillbar Variables]|
--Skills that always appear on the skillbar. Format: {sAbilityName, iXPos, iYPos, iSkillbooks} (eg: {"Common|Attack", 0, 0, 0}, {"Common|Parry", 1, 0, 0} {})
zEntry.zaSkillbarAbilities = {}

-- |[ ============ Call Standard ============= ]|
--Add to the global stack. There may be multiple executions of this script.
table.insert(gzJobCallStack, zEntry)

--Call.
LM_ExecuteScript(gsStandardJobPath, iSwitchType, iLevelReached)

--Pop the global stack.
table.remove(gzJobCallStack)
