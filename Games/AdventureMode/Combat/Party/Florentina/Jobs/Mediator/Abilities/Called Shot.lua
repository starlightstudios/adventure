-- |[ ======================================= Called Shot ====================================== ]|
-- |[Description]|
--Next attack is guaranteed to hit/crit on the target. Costs 2 CP.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Florentina.Mediator.CalledShot == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Mediator", "Called Shot", "$SkillName", gciJP_Cost_Normal, gbIsFreeAction, "Direct", "Combo", "Florentina|CalledShot", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Next attack against the marked target is a critical hit.\n\n\n\n\nFree Action.\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Mark a target's weak spot. The next attack\nagainst it always hits and crits.\n\n\nFree Action.\nCosts 2[CPIco](CP)."

    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, 2, 1, "Target Enemies Single", giNoCPGeneration)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Terrify")
  --zAbiStruct:fnAddBuff(psPrototypeName, $psApplyText, $psApplyColorString, $psApplyAnim, $psApplySound)
    zAbiStruct:fnAddBuff("Florentina.Mediator.CalledShot", "Called Shot!", "Color:Purple")
    
    --Called Shot is not a debuff or attack, it always applies and has no type.
    zAbiStruct.zExecutionAbiPackage.bDoesNoDamage        = true
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun          = true
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits          = true
    zAbiStruct.zExecutionAbiPackage.bNeverCrits          = true
    zAbiStruct.zExecutionAbiPackage.bEffectAlwaysApplies = true

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
        local saDescription = {}
        saDescription[1] = "The enemy's weakness has been marked, now"
        saDescription[2] = "exploit it!"
        saDescription[3] = "Next attack against this enemy will hit and crit."
      --EffectList:fnCreateBuffPrototype(psName, psDisplayName, psIcon, piTurns, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreateBuffPrototype(sLocalPrototypeName, "Coordinate", "Florentina|CalledShot", -1, "", saDescription, {{"Florentina Called Shot", 1}, {"Is Negative", 1}})

        --Manually specify description.
        local iType, zPrototype, iSlot = EffectList:fnLocateEntry(sLocalPrototypeName)
        if(zPrototype ~= nil) then
            zPrototype.bDontRebuildDescription = true
            zPrototype.saStrings[1] = "[Debuff]Called Shot"
            zPrototype.saStrings[2] = " "
            zPrototype.saStrings[3] = saDescription[1]
            zPrototype.saStrings[4] = saDescription[2]
            zPrototype.saStrings[5] = nil
            zPrototype.sShortText = "[Debuff]Next Hit Crits"
        end
    end

    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Florentina|Ability:CalledShot"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Add a manual line to the prediction.
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1] = {}
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].sString   = "Next Attack Crits"
    zAbiStruct.zPredictionPackage.zaAdditionalLines[1].saSymbols = {}
    
    --Set prototype.
    gzPrototypes.Combat.Florentina.Mediator.CalledShot = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Florentina.Mediator.CalledShot

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
