-- |[ ===================================== Sticky Fingers ===================================== ]|
-- |[Description]|
--Increases platina gain from combat by 15%. Passive.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Florentina.TreasureHunter.StickyFingers == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Treasure Hunter", "Sticky Fingers", "$SkillName", gciJP_Cost_Passive, gbIsNotFreeAction, "Buff", "Passive", "Florentina|StickyFingers", "Passive")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Increases Platina gained from combat by 15%%.\n\n\n\n\n\nPassive."
    zAbiStruct.sSimpleDescMarkdown  = "Increases Platina gained from battles by 15%%.\n\n\n\n\nPassive."

    -- |[Passive Effect]|
    zAbiStruct.bNeverAvailable = true --Passive abilities have no usability requirements.

    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Florentina.TreasureHunter.StickyFingers = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Florentina.TreasureHunter.StickyFingers

-- |[ ===================================== All Other Cases ==================================== ]|
if(iSwitchType ~= gciAbility_CombatEnds) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

-- |[ =================================== Combat: Combat Ends ================================== ]|
--Handles giving the platina bonus.
elseif(iSwitchType == gciAbility_CombatEnds) then

    --Get the platina gained via combat:
    local iPlatinaGained = AdvCombat_GetProperty("Victory Platina")
    local iCurrentBonus = AdvCombat_GetProperty("Bonus Platina")
    
    --Bonus:
    iCurrentBonus = iCurrentBonus + math.floor((iPlatinaGained * 0.15))
    AdvCombat_SetProperty("Bonus Platina", iCurrentBonus)
end
