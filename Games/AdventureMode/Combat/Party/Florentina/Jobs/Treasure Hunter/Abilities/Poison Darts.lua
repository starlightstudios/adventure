-- |[ ====================================== Poison Darts ====================================== ]|
-- |[Description]|
--Hits all targets for piercing, causes a Poison DoT, reduces INI.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Florentina.TreasureHunter.PoisonDarts == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Treasure Hunter", "Poison Darts", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "DoT", "Active", "Florentina|PoisonDarts", "Direct")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\n[DoT].\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Hurl poison darts at all enemies.\nDeals increased [Psn](Poison) damage over 5 turns.\n\n\n\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(30, giNoCPCost, giNoCooldown, "Target Enemies All", 1)
    
    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Pierce")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Piercing, 5, 0.45)
    
    -- |[Execution Effect Package]|
  --zAbiStruct:fnAddEffect(piStrNormal, piStrCrit, piApplyType, psApplyText, psCritApplyText, psApplyColorString, psaApplyBonus, psaApplyMalus, psaSeverityBonus, psaSeverityMalus)
    zAbiStruct:fnAddEffect(8, 11, gciDamageType_Piercing, "Poisoned!", "Badly Poisoned!", "Color:Red", {"Poison Standard"})
    
    -- |[Use of Effect Prototype]|
    zAbiStruct.zExecutionEffPackage.sPrototypeName = "Florentina.TreasureHunter.PoisonDarts"

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName
    
    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
      --EffectList:fnCreateDoTPrototype(psName, piTurns, piApplyStrength, pfDamPerTurn, piDamageType, psDisplayName, psIcon, piCritTurns, piCritApplyStrength)
        EffectList:fnCreateDoTPrototype(sLocalPrototypeName, 5, 8, 0.15, gciDamageType_Poisoning, "Poison Darts", "Florentina|PoisonDarts", 7, 11)
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Florentina|Ability:PoisonDarts"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Florentina.TreasureHunter.PoisonDarts = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Florentina.TreasureHunter.PoisonDarts

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
