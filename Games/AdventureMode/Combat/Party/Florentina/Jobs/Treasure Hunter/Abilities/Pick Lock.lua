-- |[ ======================================== Pick Lock ======================================= ]|
-- |[Description]|
--Field ability, opens certain doors.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Ability Prototype =================================== ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Florentina.TreasureHunter.PickLock == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Treasure Hunter", "Pick Lock", "$SkillName", gciJP_Cost_Normal, gbIsNotFreeAction, "Direct", "Special", "Florentina|Botany", "Unequippable")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Unlocks the Pick Lock field ability, which can open certain\ndoors. Equip from the field ability menu."
    zAbiStruct.sSimpleDescMarkdown  = "Unlocks the Pick Lock field ability.\nThis ability allows you to open certain\ndoors in the field.\n\nField abilities can be seen in the top left corner\nwhen out of battle."

    -- |[Usability Variables]|
    zAbiStruct.bNeverAvailable = true --Field abilities have no usability requirements.
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Florentina.TreasureHunter.PickLock = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Florentina.TreasureHunter.PickLock

--Call standard.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
