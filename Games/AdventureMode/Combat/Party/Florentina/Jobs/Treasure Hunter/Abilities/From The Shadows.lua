-- |[ ==================================== From The Shadows ==================================== ]|
-- |[Description]|
--40% chance to perform a normal attack on the first turn of combat for free.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.Florentina.TreasureHunter.FromTheShadows == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Treasure Hunter", "From The Shadows", "From the Shadows", gciJP_Cost_Passive, gbIsNotFreeAction, "Buff", "Passive", "Florentina|FromTheShadows", "Counterattack")
    
    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "100%% chance to strike a random target when combat begins.\nDoes not trigger counterattacks.\n\n\n\n\nPassive."
    zAbiStruct.sSimpleDescMarkdown  = "Strikes a random enemy when combat begins.\n\n\n\n\nPassive."

    -- |[Passive Effect]|
    zAbiStruct.bNeverAvailable = true --Passive abilities have no usability requirements.
    
    -- |[Healing Effect]|
    --This is used by the proc, the ability is not activated.
    zAbiStruct:fnCreateAbiPack("Weapon")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_UseWeapon, 5, 1.00)

    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "Florentina|Ability:FromTheShadows|Playchance:33" --33% chance to play
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    gzPrototypes.Combat.Florentina.TreasureHunter.FromTheShadows = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzPrototypes.Combat.Florentina.TreasureHunter.FromTheShadows

-- |[ ================================== Combat: Combat Begins ================================= ]|
--Called when combat begins, to set up any local variables. Note that the Turn Begins code will be
-- called next, *after* turn order is resolved.
if(iSwitchType == gciAbility_BeginCombat) then
    
    AdvCombatAbility_SetProperty("Enqueue This Ability As Event", -1)
    
-- |[ ============================= Combat: Paint Targets Response ============================= ]|
--If this ability fires as a response to another ability being enqueued, this section gets called to
-- paint targets. The zeroth target cluster is the one that gets used if more than one exists.
elseif(iSwitchType == gciAbility_PaintTargetsResponse) then

    --Target macro targets all enemies. Once execution begins, one target is selected.
    local iOwnerID = AdvCombatAbility_GetProperty("Owner ID")
    AdvCombat_SetProperty("Run Target Macro", "Target Enemies All", iOwnerID)
    
-- |[ =================================== Combat: Can Execute ================================== ]|
--Flags whether or not the event can actually run. If the originator got KOd by a DoT or a counterattack
-- or any other event, this will mark as false, and the event will stop here.
--This is only called for the ability in question.
elseif(iSwitchType == gciAbility_QueryCanRun) then
    AdvCombat_SetProperty("Set Event Can Run", true)

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[ ================ Originator Statistics ================= ]|
    --Get originator.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
    DL_PopActiveObject()
    
    --Give the ID to the execution package.
    gzRefAbility.zExecutionAbiPackage.iOriginatorID = iOriginatorID
    
    -- |[ ==================== For Each Target =================== ]|
    --Check all the targets. We only target someone with more than zero HP. This is because, if this ability
    -- is equipped more than once, or another pre-emptive attack is, and it already KO'd an enemy, we don't
    -- want to accidentally target an enemy that is already down.
    local iaValidSlots = {}
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 0, iTargetsTotal-1, 1 do
        
        --Get target properties.
        AdvCombat_SetProperty("Push Target", i)
            local iHealth = AdvCombatEntity_GetProperty("Health")
        DL_PopActiveObject()
        
        --If the health is over zero, this is a valid target. We store the slot, not the ID.
        if(iHealth > 0) then
            local p = #iaValidSlots + 1
            iaValidSlots[p] = i
        end
    end
    
    --If there are no valid slots, stop here.
    if(#iaValidSlots < 1) then return end
    
    --Pick one target at random.
    local iSlotFromArray = LM_GetRandomNumber(1, #iaValidSlots)
    local iTargetToUse = iaValidSlots[iSlotFromArray]
    
    --Attack that target.
    fnStandardExecution(iOriginatorID, iTargetToUse, gzRefAbility.zExecutionAbiPackage, nil)
    
-- |[ ==================================== Standard Handler ==================================== ]|
--Call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
