-- |[ ============================== Florentina Skillbook Handler ============================== ]|
--Call this file with the number of the skillbook in question, and JP and unlocks will be awarded as needed.
if(fnArgCheck(1) == false) then return end
local iLevel = LM_GetScriptArgument(0, "I")

-- |[Florentina is Not In the Party]|
if(AdvCombat_GetProperty("Is Member In Active Party", "Florentina") == false) then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Murderer's Monthly.[P] Doesn't seem useful for anyone in the party right now.)") ]])
    fnCutsceneBlocker()
    return
end

-- |[Activate Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)

-- |[Common Code]|
--Check if this skillbook has been read already. If not, award JP and handle unlocks.
local iCheckVar = VM_GetVar("Root/Variables/Global/Florentina/iSkillbook" .. iLevel, "N")
if(iCheckVar == 0.0) then
    
    --Mark the skillbook.
    VM_SetVar("Root/Variables/Global/Florentina/iSkillbook" .. iLevel, "N", 1.0)
    
    --Increment the skillbooks total.
    local iSkillbookTotal = VM_GetVar("Root/Variables/Global/Florentina/iSkillbookTotal", "N") + 1
    VM_SetVar("Root/Variables/Global/Florentina/iSkillbookTotal", "N", iSkillbookTotal)
    
    --Award JP/Abilities
    AdvCombat_SetProperty("Push Party Member", "Florentina")
    
        --JP.
        local iGlobalJP = AdvCombatEntity_GetProperty("Global JP")
        AdvCombatEntity_SetProperty("Current JP", iGlobalJP + 100)
        fnCutscene([[ Append("Florentina:[E|Neutral] (Gained 100 JP for Florentina!)[B][C]") ]])
        
        --Check if this unlocked a class ability.
        if(iSkillbookTotal >= 1.0 and iSkillbookTotal <= 4.0) then
            fnCutscene([[ Append("Florentina:[E|Neutral] (Unlocked a new job ability slot for Florentina!)[B][C]") ]])
        end

        --Execute the current job's SwitchTo script.
        AdvCombatEntity_SetProperty("Push Job S", "Active")
            AdvCombatJob_SetProperty("Fire Script", gciJob_SwitchTo)
        DL_PopActiveObject()
        
    DL_PopActiveObject()
    
end

-- |[ ======================================== Dialogue ======================================== ]|
--Each skillbook has some useful tips!
if(iLevel == 0) then
    fnCutscene([[ Append("Florentina:[E|Neutral] Murderer's Monthly, Volume 1.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] \"Some weapons deal damage other than simple physical damage. These are listed on the weapon's description.\"[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] \"You can switch weapons in battle to your backup weapons A and B. Use this to maximize damage and be ready for anything!\"") ]])
    
elseif(iLevel == 1) then
    fnCutscene([[ Append("Florentina:[E|Neutral] Murderer's Monthly, Volume 2.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] \"Catalysts apply across chapters. You can see how many you have for each chapter you're playing.\"[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] \"The bonuses are small, but they add up. Find them all!\"") ]])
    
elseif(iLevel == 2) then
    fnCutscene([[ Append("Florentina:[E|Neutral] Murderer's Monthly, Volume 3.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] \"Determine what enemies you are up against, and tailor your abilities to deal with them.\"[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] \"You can change what skills you have equipped in the pause menu, you don't need to return to a save point.\"") ]])
    
elseif(iLevel == 3) then
    fnCutscene([[ Append("Florentina:[E|Neutral] Murderer's Monthly, Volume 4.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] \"The higher forms of adamantite might seem useful, but be on the lookout for powder. Every gem cut needs powder!\"[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] \"You'll go through it very quickly, so stock up.\"") ]])
    
elseif(iLevel == 4) then
    fnCutscene([[ Append("Florentina:[E|Neutral] Murderer's Monthly, Volume 5.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] \"Armor comes in light, medium, and heavy. Some armors are character-unique, but most have a weight class.\"[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] \"Heavy armor reduces initiative but provides extra protection. Characters wearing heavy armor can wear lighter armors if they need to move faster.\"") ]])
    
elseif(iLevel == 5) then
    fnCutscene([[ Append("Florentina:[E|Neutral] Murderer's Monthly, Volume 6.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] \"Keep your eyes open for secrets, as catalysts are often hidden. Secrets may be passages through walls, marked by floors looking different.\"") ]])
    
elseif(iLevel == 6) then
    fnCutscene([[ Append("Florentina:[E|Neutral] Murderer's Monthly, Volume 7.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] \"Critical strikes deal extra damage, but also inflict stun damage. Crits can be used to stun enemies when your other abilities don't deal enough.\"[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] \"Every point of accuracy over 100 increases your critical strike chance. You can always use more accuracy.\"") ]])
    
end
fnCutsceneBlocker()
