-- |[ =============================== Controversial Sugar Cookies ============================== ]|
-- |[Description]|
--Restores 35HP, removes poisons.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--These variables are used to auto-generate the properties of the ability when the ability is a
-- "standard" case. If you want to deviate from the standard, set things manually.
--Because the struct is global, it needs to have a unique name.
if(gzItemControversialSugarCookies == nil) then
    
    -- |[System]|
    --Setup.
    local zAbiStruct = {}

    --Display/System Variables
    zAbiStruct.sJobName      = "Item"
    zAbiStruct.sSkillName    = "Controversial Sugar Cookies"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.iJPUnlockCost = gciJP_Cost_NoCost
    zAbiStruct.sIconBacking  = gsAbility_Backing_Free_Heal
    zAbiStruct.sIconFrame    = gsAbility_Frame_Free_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Item|Cookies"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "Removes all [Psn]/[Bld] effects and restores 35[Hlt]. [Target].\n\n\n\n\nFree Action.\n[Uses]"
    zAbiStruct.sDescription = ""
    zAbiStruct.saImages = {}
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Restores some health, removes [Psn](Poison)\nand [Bld](Bleed) effects.\n\n\nFree Action.\n1 use per battle."
    zAbiStruct.sSimpleDesc = ""
    zAbiStruct.saSimpleImages = {}

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false  --Set to true to ignore all requirements.
    zAbiStruct.iRequiredMP = 0           --MP required to use the ability.
    zAbiStruct.iRequiredCP = 0           --CP required to use the ability.
    zAbiStruct.bRespectsCooldown = false --If this ability has a cooldown, decrements it.
    zAbiStruct.iCooldown = 0             --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
    zAbiStruct.bIsFreeAction = true      --Flags as a free action.
    zAbiStruct.iRequiredFreeActions = 1  --Free actions set this as 1, everything else as 0.
    zAbiStruct.bRespectActionCap = true  --Characters with multiple free actions have a hard action cap. This flag checks that cap.

    --Charges.
    zAbiStruct.iChargesMax = 1

    --Targeting
    zAbiStruct.sTargetMacro = "Target Allies Single"

    --Effects
    zAbiStruct.iCPGain = 0
    
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Healing Module
    zAbiStruct.zPredictionPackage.bHasHealingModule = true
    zAbiStruct.zPredictionPackage.zHealingModule.iHealingFixed = 35
    zAbiStruct.zPredictionPackage.zHealingModule.bHealingBenefitsFromItemPower = true
    
    --Create an Effect module
    zAbiStruct.zPredictionPackage.zaEffectModules[1] = fnCreateEffectModule()
    zAbiStruct.zPredictionPackage.zaEffectModules[1].bIsBuff = true
    zAbiStruct.zPredictionPackage.zaEffectModules[1].sResultScript = "Clear [IMG1]/[IMG2] DoTs."
    zAbiStruct.zPredictionPackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/DamageTypeIcons/Poisoning", "Root/Images/AdventureUI/DamageTypeIcons/Bleeding"}

    -- |[Execution Ability Package]|
    --Basic package.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Healing")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.bDoesNoDamage = true
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun = true
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits = true
    zAbiStruct.zExecutionAbiPackage.bNeverCrits = true
    zAbiStruct.zExecutionAbiPackage.bEffectAlwaysApplies = true
    zAbiStruct.zExecutionAbiPackage.iHealingBase = 35
    zAbiStruct.zExecutionAbiPackage.bHealingBenefitsFromItemPower = true
    
    --Additional Paths
    zAbiStruct.zExecutionAbiPackage.saExecPaths = {LM_GetCallStack(0)}
    zAbiStruct.zExecutionAbiPackage.iaExecCodes = {gciAbility_SpecialStart}
    
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzItemControversialSugarCookies = zAbiStruct
end

--Because this is a prototype, we set the gzRefAbility to point to the unique structure. This way,
-- all the activity below points to the gzRefAbility's values and we don't need to change the structure's
-- name to create unique names for everything.
--The gsStandardAbilityPath handler also operates on gzRefAbility if it is called. You can use manual
-- overrides to make unique abilities.
gzRefAbility = gzItemControversialSugarCookies

-- |[ ==================================== Creation Handler ==================================== ]|
--Needs to pass the second argument, which is the internal name of this item.
if(iSwitchType == gciAbility_Create or iSwitchType == gciAbility_CreateSpecial) then
    local sInternalName = LM_GetScriptArgument(1)
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType, sInternalName)

-- |[ ====================== Combat: Special Remove Poison and Bleed DoTs ====================== ]|
elseif(iSwitchType == gciAbility_SpecialStart) then

    --Argument handling.
    if(iArgumentsTotal < 3) then return end
    local iOriginatorID   = LM_GetScriptArgument(1, "N")
    local iTargetUniqueID = LM_GetScriptArgument(2, "N")
    
    --Setup.
    local iEffectCount = 0
    local iaEffectIDList = {}
    
    --Get a list of all effects referencing the target. Remove any with the matching tag.
    local iEffectsTotal = AdvCombat_SetProperty("Store Effects Referencing", iTargetUniqueID)
    for i = 0, iEffectsTotal-1, 1 do
        AdvCombat_SetProperty("Push Temp Effects", i)
        
            --Tag is present:
            local iCount =    AdvCombatEffect_GetProperty("Get Tag Count", "Poisoning DoT")
            iCount = iCount + AdvCombatEffect_GetProperty("Get Tag Count", "Bleeding DoT")
            if(iCount > 0) then
                iEffectCount = iEffectCount + 1
                iaEffectIDList[iEffectCount] = RO_GetID()
            end
            
        DL_PopActiveObject()
    end
    
    --Clean up.
    AdvCombat_SetProperty("Clear Temp Effects")
    
    --If there was at least one effect on the list to remove, add application packs for the removal.
    if(iEffectCount > 0) then
    
        local iAnimTicks = fnGetAnimationTiming("Healing")
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, giStoredTimer, "Play Sound|Combat\\|Heal")
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iTargetUniqueID, giStoredTimer, "Create Animation|Healing|AttackAnim0")
        giStoredTimer = giStoredTimer + iAnimTicks
    
        AdvCombat_SetProperty("Register Application Pack", piOriginatorID, iTargetUniqueID, giStoredTimer, "Text|Cured!|Color:Green")
        for i = 1, iEffectCount, 1 do
            AdvCombat_SetProperty("Register Application Pack", piOriginatorID, iTargetUniqueID, giStoredTimer, "Remove Effect|" .. iaEffectIDList[i])
        end
        giStoredTimer = giStoredTimer + gciApplication_TextTicks
    end

-- |[ ==================================== Standard Handler ==================================== ]|
--Call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
