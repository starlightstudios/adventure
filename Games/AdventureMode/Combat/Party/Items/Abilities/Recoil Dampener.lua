-- |[ ==================================== Recoil Dampener ===================================== ]|
-- |[Description]|
--Increases damage for next attack.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--These variables are used to auto-generate the properties of the ability when the ability is a
-- "standard" case. If you want to deviate from the standard, set things manually.
--Because the struct is global, it needs to have a unique name.
if(gzItemRecoilDampener == nil) then
    
    -- |[System]|
    --Setup.
    local zAbiStruct = {}

    --Display/System Variables
    zAbiStruct.sJobName      = "Item"
    zAbiStruct.sSkillName    = "Recoil Dampener"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.iJPUnlockCost = gciJP_Cost_NoCost
    zAbiStruct.sIconBacking  = gsAbility_Backing_Free_Buff
    zAbiStruct.sIconFrame    = gsAbility_Frame_Free_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Item|Gun"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "+20%% [Atk] for 1 [Turns]. [Target].\n\n\n\n\nFree Action.\n[Uses]"
    zAbiStruct.sDescription = ""
    zAbiStruct.saImages = {}
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Deploy a one-use gel that absorbs recoil.\nBuffs [Atk](Attack) on self for next attack.\n\n\n\nFree Action."
    zAbiStruct.sSimpleDesc = ""
    zAbiStruct.saSimpleImages = {}

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false  --Set to true to ignore all requirements.
    zAbiStruct.iRequiredMP = 0           --MP required to use the ability.
    zAbiStruct.iRequiredCP = 0           --CP required to use the ability.
    zAbiStruct.bRespectsCooldown = true  --If this ability has a cooldown, decrements it.
    zAbiStruct.iCooldown = 0             --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
    zAbiStruct.bIsFreeAction = true      --Flags as a free action.
    zAbiStruct.iRequiredFreeActions = 1  --Free actions set this as 1, everything else as 0.
    zAbiStruct.bRespectActionCap = true  --Characters with multiple free actions have a hard action cap. This flag checks that cap.

    --Charges.
    zAbiStruct.iChargesMax = 1

    --Targeting
    zAbiStruct.sTargetMacro = "Target Self"

    --Effects
    zAbiStruct.iCPGain = 0
    
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Create an Effect module
    zAbiStruct.zPredictionPackage.zaEffectModules[1] = fnCreateEffectModule()
    zAbiStruct.zPredictionPackage.zaEffectModules[1].bIsBuff = true
    zAbiStruct.zPredictionPackage.zaEffectModules[1].sResultScript = "+[SEV]%% [IMG0] / 3 [IMG1]"
    zAbiStruct.zPredictionPackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/StatisticIcons/Attack", "Root/Images/AdventureUI/DebuffIcons/Clock"}
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iSeverity = 20
    zAbiStruct.zPredictionPackage.zaEffectModules[1].bBenefitsFromItemPower = true
    
    -- |[Execution Ability Package]|
    --Basic package.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Buff")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.bDoesNoDamage = true
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun = true
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits = true
    zAbiStruct.zExecutionAbiPackage.bEffectAlwaysApplies = true
    
    -- |[Execution Effect Package]|
    --Basic package.
    zAbiStruct.zExecutionEffPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides.
    zAbiStruct.zExecutionEffPackage.sEffectPath     = fnResolvePath() .. "../Effects/Recoil Dampener.lua" 
    zAbiStruct.zExecutionEffPackage.sEffectCritPath = fnResolvePath() .. "../Effects/Recoil Dampener.lua" 
    zAbiStruct.zExecutionEffPackage.sApplyText      = "Powered Up!"
    zAbiStruct.zExecutionEffPackage.sApplyTextCol   = "Color:Purple"
    zAbiStruct.zExecutionEffPackage.sResistText     = "Failed!"
    zAbiStruct.zExecutionEffPackage.sResistTextCol  = "Color:White"
    zAbiStruct.zExecutionEffPackage.bBenefitsFromItemPower = true
    
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzItemRecoilDampener = zAbiStruct
end

--Because this is a prototype, we set the gzRefAbility to point to the unique structure. This way,
-- all the activity below points to the gzRefAbility's values and we don't need to change the structure's
-- name to create unique names for everything.
--The gsStandardAbilityPath handler also operates on gzRefAbility if it is called. You can use manual
-- overrides to make unique abilities.
gzRefAbility = gzItemRecoilDampener

-- |[ ==================================== Creation Handler ==================================== ]|
--Needs to pass the second argument, which is the internal name of this item.
if(iSwitchType == gciAbility_Create or iSwitchType == gciAbility_CreateSpecial) then
    local sInternalName = LM_GetScriptArgument(1)
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType, sInternalName)

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Once target selection is complete, executes the ability.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[ ================ Originator Statistics ================= ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        local iItemPowerBonus = AdvCombatEntity_GetProperty("Tag Count", "Item Power +")
        local iItemPowerMalus = AdvCombatEntity_GetProperty("Tag Count", "Item Power -")
    DL_PopActiveObject()
    
    --Remove charges.
    local iUniqueID = RO_GetID()
    local iChargesLeft = VM_GetVar("Root/Variables/Combat/" .. iUniqueID .. "/iChargesLeft", "N")
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/iChargesLeft", "N", iChargesLeft - 1)
    
    -- |[ =================== Ability Package ==================== ]|
    --Fast-access pointers.
    local zaPackage = gzRefAbility.zExecutionAbiPackage
    
    --Store temp information.
    zaPackage.iOriginatorID = iOriginatorID
    
    --Handling of free actions:
    if(gzRefAbility.bIsFreeAction) then
        
        --If the ability is a free action, but does not respect the action cap or consume any free actions,
        -- it is an "Effortless" action and can be used as many times as the player wants.
        if(gzRefAbility.iRequiredFreeActions == 0 and gzRefAbility.bRespectActionCap == false) then
            AdvCombat_SetProperty("Set As Effortless Action")
        
        --Normal free action.
        else
            AdvCombat_SetProperty("Set As Free Action")
        end
    end
    
    -- |[ =================== For Each Target ==================== ]|
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        fnStandardExecution(iOriginatorID, i-1, zaPackage, gzRefAbility.zExecutionEffPackage)
    end

-- |[ ==================================== Standard Handler ==================================== ]|
--Call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
