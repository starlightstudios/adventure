-- |[ =================================== Explication Spike ==================================== ]|
-- |[Description]|
--Boosts accuracy for one turn on self.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--These variables are used to auto-generate the properties of the ability when the ability is a
-- "standard" case. If you want to deviate from the standard, set things manually.
--Because the struct is global, it needs to have a unique name.
if(gzItemExplicationSpike == nil) then
    
    -- |[System]|
    --Setup.
    local zAbiStruct = {}

    --Display/System Variables
    zAbiStruct.sJobName      = "Item"
    zAbiStruct.sSkillName    = "Explication Spike"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.iJPUnlockCost = gciJP_Cost_NoCost
    zAbiStruct.sIconBacking  = gsAbility_Backing_Free_Heal
    zAbiStruct.sIconFrame    = gsAbility_Frame_Free_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Item|Injection"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "+40 [Acc] for 1 [Turns]. [Target].\n\n\n\n\nFree Action.\n[Uses]"
    zAbiStruct.sDescription = ""
    zAbiStruct.saImages = {}
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Causes a temporary performance boost in the\noptical processors.\nGreatly boosts [Acc](Accuracy) for your next action.\n\n\nFree Action."
    zAbiStruct.sSimpleDesc = ""
    zAbiStruct.saSimpleImages = {}

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false  --Set to true to ignore all requirements.
    zAbiStruct.iRequiredMP = 0           --MP required to use the ability.
    zAbiStruct.iRequiredCP = 0           --CP required to use the ability.
    zAbiStruct.bRespectsCooldown = true  --If this ability has a cooldown, decrements it.
    zAbiStruct.iCooldown = 1             --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
    zAbiStruct.bIsFreeAction = true      --Flags as a free action.
    zAbiStruct.iRequiredFreeActions = 1  --Free actions set this as 1, everything else as 0.
    zAbiStruct.bRespectActionCap = true  --Characters with multiple free actions have a hard action cap. This flag checks that cap.
 
    --Charges.
    zAbiStruct.iChargesMax = 1

    --Targeting
    zAbiStruct.sTargetMacro = "Target Self"

    --Effects
    zAbiStruct.iCPGain = 0
    
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Create an Effect module
    zAbiStruct.zPredictionPackage.zaEffectModules[1] = fnCreateEffectModule()
    zAbiStruct.zPredictionPackage.zaEffectModules[1].bIsBuff = true
    zAbiStruct.zPredictionPackage.zaEffectModules[1].sResultScript = "+[SEV] [IMG0]/ 3 [IMG2]"
    zAbiStruct.zPredictionPackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/StatisticIcons/Accuracy", "Root/Images/AdventureUI/DebuffIcons/Clock"}
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iSeverity = 40
    zAbiStruct.zPredictionPackage.zaEffectModules[1].bBenefitsFromItemPower = true

    -- |[Execution Ability Package]|
    --Basic package.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Healing")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.bDoesNoDamage = true
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun = true
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits = true
    zAbiStruct.zExecutionAbiPackage.bNeverCrits = true
    zAbiStruct.zExecutionAbiPackage.bEffectAlwaysApplies = true
    
    -- |[Execution Effect Package]|
    --Basic package.
    zAbiStruct.zExecutionEffPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides.
    zAbiStruct.zExecutionEffPackage.sEffectPath     = fnResolvePath() .. "../Effects/Explication Spike.lua" 
    zAbiStruct.zExecutionEffPackage.sEffectCritPath = fnResolvePath() .. "../Effects/Explication Spike.lua" 
    zAbiStruct.zExecutionEffPackage.sApplyText      = "Focused!"
    zAbiStruct.zExecutionEffPackage.sApplyTextCol   = "Color:Purple"
    zAbiStruct.zExecutionEffPackage.sResistText     = "Failed!"
    zAbiStruct.zExecutionEffPackage.sResistTextCol  = "Color:White"
    
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzItemExplicationSpike = zAbiStruct
end

--Because this is a prototype, we set the gzRefAbility to point to the unique structure. This way,
-- all the activity below points to the gzRefAbility's values and we don't need to change the structure's
-- name to create unique names for everything.
--The gsStandardAbilityPath handler also operates on gzRefAbility if it is called. You can use manual
-- overrides to make unique abilities.
gzRefAbility = gzItemExplicationSpike

-- |[ ==================================== Creation Handler ==================================== ]|
--Needs to pass the second argument, which is the internal name of this item.
if(iSwitchType == gciAbility_Create or iSwitchType == gciAbility_CreateSpecial) then
    local sInternalName = LM_GetScriptArgument(1)
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType, sInternalName)
    
-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--Prediction box needs to account for item power tags.
elseif(iSwitchType == gciAbility_BuildPredictionBox) then
    
    -- |[ ========================= Setup ======================== ]|
    --Fast-access pointers.
    local zPackage = gzRefAbility.zPredictionPackage
    
    -- |[ ===================== Tag Handling ===================== ]|
    --The user can be affected by the tags "Item Power +" and "Item Power -". Each tag is a 1% change.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
        local iItemPowerBonus = AdvCombatEntity_GetProperty("Tag Count", "Item Power +")
        local iItemPowerMalus = AdvCombatEntity_GetProperty("Tag Count", "Item Power -")
    DL_PopActiveObject()
    
    --Modify the effect module by the power. This only changes the string.
    local sOrigString = zPackage.zaEffectModules[1].sResultScript
    local fPowerFactor = 1.0 + ((iItemPowerBonus - iItemPowerMalus) * 0.01)
    local iPower = math.floor(40 * fPowerFactor)
    zPackage.zaEffectModules[1].sResultScript = "+" .. iPower .. " [IMG0]/ 3 [IMG2]"

    -- |[ ===================== Other Values ===================== ]|
    --Get information about the cluster. This is needed to give each box a unique ID.
    local iClusterID = AdvCombat_GetProperty("Prediction Get Cluster ID")
    
    -- |[ =================== For Each Target ==================== ]|
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    for i = 0, iClusterTargetsTotal-1, 1 do
        local iTargetID = AdvCombat_GetProperty("Prediction Get Targets ID", i)
        fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, zPackage)
    end
    
    --Revert to original.
    zPackage.zaEffectModules[1].sResultScript = "+40 [IMG0]/ 3 [IMG2]"

-- |[ ==================================== Standard Handler ==================================== ]|
--Call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
