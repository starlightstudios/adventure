-- |[ ===================================== Gourmet Lunch ====================================== ]|
-- |[Description]|
--Restores 100 HP, 10 MP, and buffs evade by 50 for 1 turn.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabricated copy the first time this script is run.
if(gzItemGourmetLunch == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    --Setup.
    local zAbiStruct = fnCreateAbilityPrototype()

    --Display/System Variables
    zAbiStruct.sJobName      = "Item"
    zAbiStruct.sSkillName    = "Gourmet Lunch"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.sDisplayName  = zAbiStruct.sSkillName
    zAbiStruct.iJPUnlockCost = gciJP_Cost_NoCost
    zAbiStruct.sIconBacking  = gsAbility_Backing_Free_Heal
    zAbiStruct.sIconFrame    = gsAbility_Frame_Free_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Item|GenericPotionRed"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "Restore 100[Hlt], 10[MPIco]. [Target].\n[FEffect]+45 [Evd] for 1[Turns].\n\n\n\nFree Action.\n[Uses]"
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "It's got sandwiches, chips, a drink, and dessert!\nRestores 100 HP and 10 MP.\nIncreases [Evd](Evade) for next turn.\n\n1 use per battle.\nFree Action."

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false  --Set to true to ignore all requirements.
    zAbiStruct.iRequiredMP = 0           --MP required to use the ability.
    zAbiStruct.iRequiredCP = 0           --CP required to use the ability.
    zAbiStruct.bRespectsCooldown = true  --If this ability has a cooldown, decrements it.
    zAbiStruct.iCooldown = 1             --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
    zAbiStruct.bIsFreeAction = true      --Flags as a free action.
    zAbiStruct.iRequiredFreeActions = 1  --Free actions set this as 1, everything else as 0.
    zAbiStruct.bRespectActionCap = true  --Characters with multiple free actions have a hard action cap. This flag checks that cap.

    --Charges.
    zAbiStruct.iChargesMax = 1

    --Targeting
    zAbiStruct.sTargetMacro = "Target Allies Single"

    --Effects
    zAbiStruct.iCPGain = 0
    
    -- |[ ======== Prediction Package ======== ]|
    -- |[Primary Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Healing Module
    zAbiStruct.zPredictionPackage.bHasHealingModule = true
    zAbiStruct.zPredictionPackage.zHealingModule.iHealingFixed = 100
    zAbiStruct.zPredictionPackage.zHealingModule.bHealingBenefitsFromItemPower = true
    
    --MP Generation Module
    zAbiStruct.zPredictionPackage.bHasMPGenerationModule = true
    zAbiStruct.zPredictionPackage.zMPGenerationModule.iMPGeneration = 10
    
    --Create an Effect module
    zAbiStruct.zPredictionPackage.zaEffectModules[1] = fnCreateEffectModule()
    zAbiStruct.zPredictionPackage.zaEffectModules[1].bIsBuff = true
    zAbiStruct.zPredictionPackage.zaEffectModules[1].sResultScript = "+50 [IMG1] / 1[IMG2]"
    zAbiStruct.zPredictionPackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/StatisticIcons/Evade", "Root/Images/AdventureUI/DebuffIcons/Clock"}

    -- |[ ========= Execution Package ======== ]|
    -- |[Basic Execution]|
    --Basic package.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Healing")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.bDoesNoDamage = true
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun = true
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits = true
    zAbiStruct.zExecutionAbiPackage.bNeverCrits = true
    zAbiStruct.zExecutionAbiPackage.bEffectAlwaysApplies = true
    zAbiStruct.zExecutionAbiPackage.iHealingBase = 100
    zAbiStruct.zExecutionAbiPackage.iMPGeneration = 10
    
    -- |[Execution Effect Package]|
    --Basic package.
    zAbiStruct.zExecutionEffPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides.
    zAbiStruct.zExecutionEffPackage.sApplyAnimation = "Null"
    zAbiStruct.zExecutionEffPackage.sApplySound     = "Null"
    zAbiStruct.zExecutionEffPackage.sEffectPath     = fnResolvePath() .. "../Effects/Gourmet Lunch.lua"
    zAbiStruct.zExecutionEffPackage.sEffectCritPath = fnResolvePath() .. "../Effects/Gourmet Lunch.lua"
    zAbiStruct.zExecutionEffPackage.sApplyText      = "+Evade"
    zAbiStruct.zExecutionEffPackage.sApplyTextCol   = "Color:Blue"
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzItemGourmetLunch = zAbiStruct
end

--Activate prototype.
gzRefAbility = gzItemGourmetLunch

-- |[ ==================================== Creation Handler ==================================== ]|
--Needs to pass the second argument, which is the internal name of this item.
if(iSwitchType == gciAbility_Create or iSwitchType == gciAbility_CreateSpecial) then
    local sInternalName = LM_GetScriptArgument(1)
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType, sInternalName)

-- |[ ==================================== Standard Handler ==================================== ]|
--Call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
