-- |[ ================================== Runestone Christine =================================== ]|
-- |[Description]|
--Christine's basic runestone. Heals her a bit.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--These variables are used to auto-generate the properties of the ability when the ability is a
-- "standard" case. If you want to deviate from the standard, set things manually.
--Because the struct is global, it needs to have a unique name.
if(gzItemVioletRunestone == nil) then
    
    -- |[System]|
    --Setup.
    local zAbiStruct = {}

    --Display/System Variables
    zAbiStruct.sJobName      = "Item"
    zAbiStruct.sSkillName    = "Violet Runestone"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. " Internal"
    zAbiStruct.iJPUnlockCost = gciJP_Cost_NoCost
    zAbiStruct.sIconBacking  = gsAbility_Backing_Free_Heal
    zAbiStruct.sIconFrame    = gsAbility_Frame_Free_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Item|VioletRunestone"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "Restore 20%% of Max [Hlt]. [Target]\n\n\n\n\nFree Action.\n[Uses]"
    zAbiStruct.sDescription = ""
    zAbiStruct.saImages = {}
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Heal yourself for 20%% of your max health.\n\n\n\nFree Action.\nRecharges every 2 turns, unlimited uses."
    zAbiStruct.sSimpleDesc = ""
    zAbiStruct.saSimpleImages = {}

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false  --Set to true to ignore all requirements.
    zAbiStruct.iRequiredMP = 0           --MP required to use the ability.
    zAbiStruct.iRequiredCP = 0           --CP required to use the ability.
    zAbiStruct.bRespectsCooldown = true  --If this ability has a cooldown, decrements it.
    zAbiStruct.iCooldown = 2             --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
    zAbiStruct.bIsFreeAction = true      --Flags as a free action.
    zAbiStruct.iRequiredFreeActions = 1  --Free actions set this as 1, everything else as 0.
    zAbiStruct.bRespectActionCap = true  --Characters with multiple free actions have a hard action cap. This flag checks that cap.

    --Charges.
    zAbiStruct.iChargesMax = nil

    --Targeting
    zAbiStruct.sTargetMacro = "Target Self"

    --Effects
    zAbiStruct.iCPGain = 0
    
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Healing Module
    zAbiStruct.zPredictionPackage.bHasHealingModule = true
    zAbiStruct.zPredictionPackage.zHealingModule.fHealingPercent = 0.20
    zAbiStruct.zPredictionPackage.zHealingModule.bHealingBenefitsFromItemPower = true

    -- |[Execution Ability Package]|
    --Basic package.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Healing")
    
    --Apply overrides. We only edit parts that may not be standard.
    zAbiStruct.zExecutionAbiPackage.bDoesNoDamage = true
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun = true
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits = true
    zAbiStruct.zExecutionAbiPackage.bNeverCrits = true
    zAbiStruct.zExecutionAbiPackage.bEffectAlwaysApplies = true
    zAbiStruct.zExecutionAbiPackage.fHealingPercent = 0.20
    zAbiStruct.zExecutionAbiPackage.bHealingBenefitsFromItemPower = true
    
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzItemVioletRunestone = zAbiStruct
end

--Because this is a prototype, we set the gzRefAbility to point to the unique structure. This way,
-- all the activity below points to the gzRefAbility's values and we don't need to change the structure's
-- name to create unique names for everything.
--The gsStandardAbilityPath handler also operates on gzRefAbility if it is called. You can use manual
-- overrides to make unique abilities.
gzRefAbility = gzItemVioletRunestone

-- |[ ==================================== Creation Handler ==================================== ]|
--Needs to pass the second argument, which is the internal name of this item.
if(iSwitchType == gciAbility_Create or iSwitchType == gciAbility_CreateSpecial) then
    local sInternalName = LM_GetScriptArgument(1)
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType, sInternalName)
    
-- |[ ==================================== Standard Handler ==================================== ]|
--Call the standardized handler.
else
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
end
