-- |[ ==================================== Poison Spore Cap ==================================== ]|
-- |[Description]|
--Deals .51 attack power poison damage over 3 turns.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--Setup, apply overrides.
if(gzEffectItemPoisonSporeCap == nil) then
    
    --Base, overrides.
    local zEffectStruct = fnCreateDoTEffectPrototype(gciDamageType_Poisoning, 3, 0.51)
    zEffectStruct.sDisplayName = "Poison Spore Cap"
    zEffectStruct.sIcon = "Florentina|DrippingBlade"
    zEffectStruct.bBenefitsFromItemPower = true
    
    --Store
    gzEffectItemPoisonSporeCap = zEffectStruct
    
end

-- |[Set and Execute]|
--Standardized Naming
gzRefEffect = gzEffectItemPoisonSporeCap

--Call the standard handler. It will nil off the gzRefEffect.
LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType)
