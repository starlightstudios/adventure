-- |[ =================================== Crimson Runestone ==================================== ]|
-- |[Description]|
--Next attack against used with this Effect is a guaranteed hit/crit. Effect is consumed
-- upon use. Does not modify any stats.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Local Variables ===================================== ]|
-- |[System]|
--This effect uses a standardized StatMod script.
local sDisplayName = "Crimson Runestone"
local iDuration = -1
local bIsBuff = false
local sIcon = "Root/Images/AdventureUI/Abilities/Item|CrimsonRunestone"
local sStatString = ""

--Description can be a variable number of lines.
local saDescription = {}
saDescription[1] = "Sanya's rune powers her up!"
saDescription[2] = "Next attack will hit and crit."

--Run the standardized builder.
gzaStatModStruct            = fnCreateStatmodEffectPrototype(sDisplayName, iDuration, bIsBuff, sIcon, sStatString)
gzaStatModStruct.saStrings  = fnStatmodBuildInspectorStrings(gzaStatModStruct, saDescription)
gzaStatModStruct.sShortText = fnStatmodBuildShortText(gzaStatModStruct)

-- |[Overrides]|
--Tags.
gzaStatModStruct.zaTagList = {{"User Attack Crits Consume", 1}, {"Is Positive", 1}}

--Manually specify description since there is no stat modification.
gzaStatModStruct.saStrings = {}
gzaStatModStruct.saStrings[1] = "[Buff]Crimson Runestone"
gzaStatModStruct.saStrings[2] = " "
gzaStatModStruct.saStrings[3] = saDescription[1]
gzaStatModStruct.saStrings[4] = saDescription[2]
gzaStatModStruct.saStrings[5] = nil
gzaStatModStruct.sShortText = "[Buff]Next Hit Crits"

-- |[ ======================================== Creation ======================================== ]|
--Standard call when effect is initialized.
if(iSwitchType == gciEffect_Create) then
    LM_ExecuteScript(gsStandardStatPath, gciEffect_Create)
    
-- |[ ================================== Apply/Unapply Stats =================================== ]|
--Calls the apply/unapply stat routines.
elseif((iSwitchType == gciEffect_ApplyStats or iSwitchType == gciEffect_UnApplyStats) and iArgumentsTotal >= 2) then
    local iTargetID = math.floor(tonumber(LM_GetScriptArgument(1, "N")))
    LM_ExecuteScript(gsStandardStatPath, iSwitchType, iTargetID)

-- |[ ================================== Character Action Ends ================================= ]|
--Called after a character action ends to decrement the turn counter.
elseif(iSwitchType == gciEffect_PostAction) then
    LM_ExecuteScript(gsStandardStatPath, gciEffect_PostAction)
end
