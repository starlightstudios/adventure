-- |[ ================================ SX-399 Skillbook Handler ================================ ]|
--Call this file with the number of the skillbook in question, and JP and unlocks will be awarded as needed.
if(fnArgCheck(1) == false) then return end
local iLevel = LM_GetScriptArgument(0, "I")

-- |[Not In the Party]|
if(AdvCombat_GetProperty("Is Member In Active Party", "SX-399") == false) then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The Poor-Bot's Power Weapons.[P] Has diagrams of makeshift explosives and weapons.[P] Doesn't seem useful to anyone in the party right now.)") ]])
    fnCutsceneBlocker()
    return
end

-- |[Activate Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)

-- |[Common Code]|
--Check if this skillbook has been read already. If not, award JP and handle unlocks.
local iCheckVar = VM_GetVar("Root/Variables/Global/SX-399/iSkillbook" .. iLevel, "N")
if(iCheckVar == 0.0) then
    
    --Mark the skillbook.
    VM_SetVar("Root/Variables/Global/SX-399/iSkillbook" .. iLevel, "N", 1.0)
    
    --Increment the skillbooks total.
    local iSkillbookTotal = VM_GetVar("Root/Variables/Global/SX-399/iSkillbookTotal", "N") + 1
    VM_SetVar("Root/Variables/Global/SX-399/iSkillbookTotal", "N", iSkillbookTotal)
    
    --Award JP/Abilities
    AdvCombat_SetProperty("Push Party Member", "SX-399")
    
        --Common strings.
        local sString = "WD_SetProperty(\"Append\", \"SX-399:[E|Flirt] (Skillbook found:: The Poor-Bot's Power Weapons, Volume " .. iLevel .. ".)[B][C]\")"
        fnCutscene(sString)
    
        --JP.
        local iGlobalJP = AdvCombatEntity_GetProperty("Global JP")
        AdvCombatEntity_SetProperty("Current JP", iGlobalJP + 100)
        
        --Check if this unlocked a class ability.
        if(iSkillbookTotal >= 1.0 and iSkillbookTotal <= 4.0) then
            fnCutscene([[ Append("SX-399:[E|Flirt] (Gained 100 JP for SX-399!)[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] (Unlocked a new job ability slot for SX-399!)") ]])
        else
            fnCutscene([[ Append("SX-399:[E|Flirt] (Gained 100 JP for SX-399!)") ]])
        end
    
        --Execute the current job's SwitchTo script.
        AdvCombatEntity_SetProperty("Push Job S", "Active")
            AdvCombatJob_SetProperty("Fire Script", gciJob_SwitchTo)
        DL_PopActiveObject()
        
    DL_PopActiveObject()

--Already found.
else
    local sString = "WD_SetProperty(\"Append\", \"SX-399:[E|Neutral] (The Poor-Bot's Power Weapons, Volume " .. iLevel .. ". You have already read this skillbook.)\")"
    fnCutscene(sString)
    
end