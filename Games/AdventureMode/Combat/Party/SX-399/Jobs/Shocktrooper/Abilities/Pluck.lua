-- |[ ========================================== Pluck ========================================= ]|
-- |[Description]|
--Increases protection by 3.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.SX399.Shocktrooper.Pluck == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Shocktrooper", "Pluck", "$SkillName", gciJP_Cost_Passive, gbIsNotFreeAction, "Buff", "Passive", "SX-399|Pluck", "Passive")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "+3 [Prt].\n\n\n\n\n\nPassive."
    zAbiStruct.sSimpleDescMarkdown  = "Not knowing when to quit is the same as being\ndetermined.\nIncreases [Prt](Protection).\n\n\nPassive."
    
    -- |[Passive Effect]|
    zAbiStruct.bNeverAvailable   = true --Passive abilities have no usability requirements.
    zAbiStruct.sPassivePrototype = "SX399.Shocktrooper.Pluck"
    zAbiStruct.bHasGUIEffects    = true

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    if(EffectList:fnEntryExists(zAbiStruct.sPassivePrototype) == false) then
	local saDescription = {}
	saDescription[1] = "Not knowing when to give up is the same as"
	saDescription[2] = "being determined. Increases [Prt](Protection)."
      --EffectList:fnCreatePassivePrototype(psName, psDisplayName, psIcon, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreatePassivePrototype(zAbiStruct.sPassivePrototype, "Pluck", "SX-399|Pluck", "Prt|Flat|3", saDescription, {{"Is Positive", 1}, {"Is Passive", 1}})
    end

    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.SX399.Shocktrooper.Pluck = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.SX399.Shocktrooper.Pluck

-- |[ ===================================== All Other Cases ==================================== ]|
if(iSwitchType ~= gciAbility_GUIApplyEffect) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

-- |[ ==================================== GUI: Apply Effect =================================== ]|
--When showing a character's statistics, all equipped abilities are queried. This is not reflected
-- in combat, this is GUI only.
--The standard does not handle this.
elseif(iSwitchType == gciAbility_GUIApplyEffect) then
    AdvCombatAbility_SetProperty("Push Owner")
        fnApplyStatBonus(gciStatIndex_Protection, 3)
    DL_PopActiveObject()
    gzRefAbility = nil
end
