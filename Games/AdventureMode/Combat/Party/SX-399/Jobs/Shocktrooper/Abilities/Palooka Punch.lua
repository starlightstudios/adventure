-- |[ ===================================== Palooka Punch ====================================== ]|
-- |[Description]|
--Give them one in the jaw.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.SX399.Shocktrooper.PalookaPunch == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Shocktrooper", "Palooka Punch", "$SkillName", gciJP_Cost_Advanced, gbIsNotFreeAction, "Direct", "Active", "SX-399|PalookaPunch", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "They think you're going to shoot, and then you\nsock them in the jaw!\nDeals reduced [DamageType]([DamTypeString]) damage, inflicts stun.\n\n\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(20, giNoCPCost, giNoCooldown, "Target Enemies Single", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Strike")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_Striking, 5, 0.80)
    
    --Adding stun damage
    zAbiStruct.zExecutionAbiPackage:fnAddStun(60)
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "SX-399|General:Swing|Offense|Ability:PalookaPunch"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.SX399.Shocktrooper.PalookaPunch = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.SX399.Shocktrooper.PalookaPunch

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
