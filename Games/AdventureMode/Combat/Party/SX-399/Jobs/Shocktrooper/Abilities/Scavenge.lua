-- |[ ======================================== Scavenge ======================================== ]|
-- |[Description]|
--Hits one enemy, restores health.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.SX399.Shocktrooper.Scavenge == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Shocktrooper", "Scavenge", "$SkillName", gciJP_Cost_Advanced, gbIsNotFreeAction, "Direct", "Active", "SX-399|Scavenge", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\n[BaseHit].\n\nRestore 50 [Hlt], self.\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Swipe a few bits as you take them off, right?\nDeals normal weapon damage, heals you a bit.\n\n\n\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(20, giNoCPCost, giNoCooldown, "Target Enemies Single", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Weapon")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_UseWeapon, 5, 1.10)
    
    -- Adding self-heal
  --zAbiStruct.zExecutionAbiPackage:fnAddSelfHeal(piHealingBase, pfHealingScaleFactor, pfHealPctOfMaxHP)
    zAbiStruct.zExecutionAbiPackage:fnAddSelfHeal(50, 0.00, 0.00)
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "SX-399|General:Swing|Offense|Ability:Scavenge"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.SX399.Shocktrooper.Scavenge = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.SX399.Shocktrooper.Scavenge

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
