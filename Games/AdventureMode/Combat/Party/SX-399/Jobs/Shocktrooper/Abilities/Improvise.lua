-- |[ ======================================= Improvise ======================================== ]|
-- |[Description]|
--Damage yourself, but next attack always crits.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.SX399.Shocktrooper.Improvise == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Shocktrooper", "Improvise", "$SkillName", gciJP_Cost_Advanced, gbIsFreeAction, "Direct", "Active", "SX-399|Improvise", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "-10%% Max [Hlt], next attack crits.[BR][BR][BR][BR][BR]Free Action.[BR][Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "With a bit of ingenuity, your health is your[BR]weapon.[BR]Lose 10%% of your health, guaranteed crit for[BR]remainder of turn.[BR][BR]Free action."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(giNoMPCost, giNoCPCost, giNoCooldown, "Target Self", giNoCPGeneration)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Buff")
    zAbiStruct.zExecutionAbiPackage:fnSetAsBuff()
    
    -- |[Effect Package]|
  --zAbiStruct:fnAddBuff(psPrototypeName, $psApplyText, $psApplyColorString, $psApplyAnim, $psApplySound)
    zAbiStruct:fnAddBuff("SX399.Shocktrooper.Improvise", "Motivated!", "Color:Purple")
    
    --Custom resist text
    zAbiStruct.zExecutionEffPackage.sResistText = "Failed!"

    -- |[ ========= Effect Prototype ========= ]|
    --This ability has an associated effect prototype. Create and store it in the global listing.
    local sLocalPrototypeName = zAbiStruct.zExecutionEffPackage.sPrototypeName

    --If not created yet, create and register it.
    if(EffectList:fnEntryExists(sLocalPrototypeName) == false) then
	local saDescription = {}
	saDescription[1] = "Take a piece of you to take a piece of them!"
	saDescription[2] = "Next attack crits."
      --EffectList:fnCreateBuffPrototype(psName, psDisplayName, psIcon, piTurns, psStatString, psaDescription, $pzaTagList)
        EffectList:fnCreateBuffPrototype(sLocalPrototypeName, "Improvise", "SX-399|Improvise", 1, "Acc|Flat|1000", saDescription, {{"Is Positive", 1}})
    end
    
    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "SX-399|Ability:Improvise"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.SX399.Shocktrooper.Improvise = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.SX399.Shocktrooper.Improvise

-- |[ ==================================== Standard Handler ==================================== ]|
--Call the standardized handler.
if(iSwitchType ~= gciAbility_Execute) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Special execution is required.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[ ========== Originator Statistics ========== ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        local iCurrentHP    = AdvCombatEntity_GetProperty("Health")
        local iMaxHP        = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
    DL_PopActiveObject()
    
    -- |[ ================= Ability Package ================== ]|
    --Create an ability package. This package comes with the default hit/miss values, animations, etc.
    local zaAbilityPackage = gzRefAbility.zExecutionAbiPackage
    local zaEffectPackage = gzRefAbility.zExecutionEffPackage
    zaAbilityPackage.iOriginatorID = iOriginatorID
    
    --Handling of free actions:
    if(gzRefAbility.bIsFreeAction) then
	
	--If the ability is a free action, but does not respect the action cap or consume any free actions,
	-- it is an "Effortless" action and can be used as many times as the player wants.
	if(gzRefAbility.iRequiredFreeActions == 0 and gzRefAbility.bRespectActionCap == false) then
	    AdvCombat_SetProperty("Set As Effortless Action")
	
	--Normal free action.
	else
	    AdvCombat_SetProperty("Set As Free Action")
	end
    end
    
    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        
        --Normal execution.
        fnStandardExecution(iOriginatorID, i-1, gzRefAbility.zExecutionAbiPackage, gzRefAbility.zExecutionEffPackage)
        
        --Compute damage to the user.
        local iDamageToDeal = math.floor(iMaxHP * 0.10)
        if(iDamageToDeal > iCurrentHP) then iDamageToDeal = iCurrentHP - 1 end
        
        --Execute.
        AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, 0, "Damage|" .. iDamageToDeal)
        
    end
    
    --Clean.
    gzRefAbility = nil
end
