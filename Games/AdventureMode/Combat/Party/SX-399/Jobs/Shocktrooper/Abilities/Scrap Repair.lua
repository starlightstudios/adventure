-- |[ ====================================== Scrap Repair ====================================== ]|
-- |[Description]|
--Heals one ally for half of their missing HP.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.SX399.Shocktrooper.ScrapRepair == nil) then

    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Shocktrooper", "Scrap Repair", "$SkillName", gciJP_Cost_Advanced, gbIsNotFreeAction, "Heal", "Active", "SX-399|ScrapRepair", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "Restore half of all missing [Hlt]. [Target].\n\n\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Spot-weld a wounded ally.\nHeals half their missing HP.\n\n\n\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(25, giNoCPCost, giNoCooldown, "Target Allies Single", 1)
    
    -- |[ ========= Execution Package ======== ]|
    --Handled manually below.

    -- |[ ============= Finish Up ============ ]|
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.SX399.Shocktrooper.ScrapRepair = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.SX399.Shocktrooper.ScrapRepair

-- |[ ==================================== Standard Handler ==================================== ]|
--Call the standardized handler.
if(iSwitchType ~= gciAbility_BuildPredictionBox and iSwitchType ~= gciAbility_Execute) then
    LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)

-- |[ ============================== Combat: Build Prediction Box ============================== ]|
--This ability has a unique formula and computes healing based on the target's max HP.
elseif(iSwitchType == gciAbility_BuildPredictionBox) then

    -- |[ ========================= Setup ======================== ]|
    --This routine create a package that contains all the default values needed to build a Prediction Box. We
    -- then change any values that aren't default.
    local zPackage = fnCreatePredictionPack()
    
    --Healing.
    zPackage.bHasHealingModule = true
    zPackage.zHealingModule.iHealingFixed = 0

    -- |[ ===================== Other Values ===================== ]|
    --Get originator ID.
    AdvCombatAbility_SetProperty("Push Owner")
        local iOwnerID = RO_GetID()
    DL_PopActiveObject()
    
    --Get information about the cluster. This is needed to give each box a unique ID.
    local iClusterID = AdvCombat_GetProperty("Prediction Get Cluster ID")
    
    -- |[ ==================== For Each Target =================== ]|
    --There should only be one target. Get their Max and Current HP.
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    for i = 0, iClusterTargetsTotal-1, 1 do
        
        --Target ID.
        local iTargetID = AdvCombat_GetProperty("Prediction Get Targets ID", i)
        
        --Properties.
        AdvCombat_SetProperty("Push Entity By ID", iTargetID)
            local iCurrentHP = AdvCombatEntity_GetProperty("Health")
            local iMaxHP     = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
        DL_PopActiveObject()
        
        --Set the power of the module.
        zPackage.zHealingModule.iHealingFixed = math.floor((iMaxHP - iCurrentHP) * 0.50)
        if(zPackage.zHealingModule.iHealingFixed < 1) then
            zPackage.zaAdditionalLines[1] = {}
            zPackage.zaAdditionalLines[1].sString   = "No Healing (Max HP)"
            zPackage.zaAdditionalLines[1].saSymbols = {}
        else
            zPackage.zaAdditionalLines[1] = nil
        end
        
        --Build the box!
        fnBuildPredictionBox(iOwnerID, iTargetID, iClusterID, zPackage)
    end
    
    --Clean.
    gzRefAbility = nil

-- |[ =============================== Combat: Execute on Targets =============================== ]|
--Unique formula, cannot use standard.
elseif(iSwitchType == gciAbility_Execute) then

    -- |[ ========== Originator Statistics ========== ]|
    --Get originator variables, modify resources.
    AdvCombat_SetProperty("Push Event Originator")
        local iOriginatorID = RO_GetID()
        fnModifyCP(gzRefAbility.iCPGain)
        fnModifyMP(gzRefAbility.iRequiredMP * -1)
    DL_PopActiveObject()
    
    -- |[ ========== Ability Package ========== ]|
    --Create an ability package. This package comes with the default hit/miss values, animations, etc.
    local zaAbilityPackage = fnConstructDefaultAbilityPackage("Healing")
    zaAbilityPackage.iOriginatorID = iOriginatorID
    
    --Apply overrides. We only edit parts that may not be standard.
    zaAbilityPackage.bDoesNoDamage = true
    zaAbilityPackage.bDoesNoStun = true
    zaAbilityPackage.bAlwaysHits = true
    
    --Additional Paths
    zaAbilityPackage.saExecPaths = {LM_GetCallStack(0)}
    zaAbilityPackage.iaExecCodes = {gciAbility_SpecialStart}
    
    --Voice.
    zaAbilityPackage.sVoiceData = "SX-399|General:Swing|Offense|Ability:ScrapRepair"
    
    -- |[ ==================== For Each Target =================== ]|
    --Get how many targets were painted by this ability, iterate across them.
    local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
    for i = 1, iTargetsTotal, 1 do
        
        --Target ID.
        AdvCombat_SetProperty("Push Target", piTargetIndex) 
            local iTargetID = RO_GetID()
            local iCurrentHP = AdvCombatEntity_GetProperty("Health")
            local iMaxHP     = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
        DL_PopActiveObject()
        
        --Set the power of the module.
        zaAbilityPackage.iHealingBase = math.floor((iMaxHP - iCurrentHP) * 0.50)
        
        --Execute.
        fnStandardExecution(iOriginatorID, i-1, zaAbilityPackage, nil)
    end
    
    --Clean.
    gzRefAbility = nil
end
