-- |[ ======================================= Flak Round ======================================= ]|
-- |[Description]|
--Hits for piercing damage, all enemies.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================ Prefabrication Variables ================================ ]|
--Create a prefabrication on first execution to speed up subsequent executions.
if(gzPrototypes.Combat.SX399.Shocktrooper.FlakRound == nil) then
    
    -- |[ ========= Basic Properties ========= ]|
    -- |[System]|
    local zAbiStruct = AbiPrototype:new()
  --zAbiStruct:fnSetSystem(psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    zAbiStruct:fnSetSystem("Shocktrooper", "Flak Round", "$SkillName", gciJP_Cost_Advanced, gbIsNotFreeAction, "Direct", "Active", "SX-399|FlakRound", "Direct")

    -- |[Descriptions]|
    zAbiStruct.sDescriptionMarkdown = "[Inflict]. [Target].\nCannot miss.\n\n\n\n\n[Costs]"
    zAbiStruct.sSimpleDescMarkdown  = "Fires a scattering flak round, hitting all\nenemies.\nDeals reduced weapon damage.\nCannot miss.\n\nCosts [MPCost][MPIco](MP)."
    
    -- |[Usability Variables]|
  --zAbiStruct:fnSetUsability(piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    zAbiStruct:fnSetUsability(20, giNoCPCost, giNoCooldown, "Target Enemies All", 1)

    -- |[ ========= Execution Package ======== ]|
    -- |[Execution Ability Package]|
    zAbiStruct:fnCreateAbiPack("Weapon")
  --zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(piDamageType, piMissthreshold, pfDamageFactor)
    zAbiStruct.zExecutionAbiPackage:fnSetAsDamage(gciDamageType_UseWeapon, 5, 0.70)
    
    --Override to ensure it never misses
    zAbiStruct.zExecutionAbiPackage:fnAlwaysHit()

    -- |[ ============= Finish Up ============ ]|
    -- |[Voice]|
    zAbiStruct.zExecutionAbiPackage.sVoiceData = "SX-399|General:Swing|Offense|Ability:FlakRound"
    
    -- |[Finalize]|
    zAbiStruct:fnFinalize()
    
    --Set prototype.
    gzPrototypes.Combat.SX399.Shocktrooper.FlakRound = zAbiStruct
end

-- |[ ======================================== Execution ======================================= ]|
--Activate prototype.
gzRefAbility = gzPrototypes.Combat.SX399.Shocktrooper.FlakRound

--Call the standardized handler.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
