-- |[ =============================== fnResolveDoTFactorForTurn() ============================== ]|
--Given the UniqueID of a DoT effect, resolves its damage factor for the requested turn.
-- Can legally return 0.00, but will bark a warning of the turn is out of range.
--Turns start counting at 0.
function fnResolveDoTFactorForTurn(piUniqueID, piTurn)
    
    --Error check:
    if(piUniqueID == nil) then return 0.0 end
    if(piTurn     == nil) then return 0.0 end
    
    --Check if the number is out of range:
    local iFactorsTotal = VM_GetVar("Root/Variables/Combat/"  .. piUniqueID .. "/iFactorsTotal", "I")
    if(piTurn < 0 or piTurn >= iFactorsTotal) then
        io.write("fnResolveDoTDamageForTurn: Error, turn " .. piTurn .. " out of range: " .. iFactorsTotal .. "\n")
        return 0.0
    end
    
    --Resolve the name of the variables.
    local sVarName = string.format("fFactor%02i", piTurn)
    
    --Get the factor.
    local fDamageFactor = VM_GetVar("Root/Variables/Combat/"  .. piUniqueID .. "/" .. sVarName, "N")
    return fDamageFactor
    
end
