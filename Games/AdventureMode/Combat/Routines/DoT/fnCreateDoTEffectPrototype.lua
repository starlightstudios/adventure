-- |[ ============================== fnCreateDoTEffectPrototype() ============================== ]|
--When a DoT effect is created, this function creates a pre-filled structure with the default values
-- and returns it. The calling script should then override the default values.
function fnCreateDoTEffectPrototype(piDamageType, piDuration, pfTotalDamage)
    
    -- |[Creation]|
    --Base
    local zEffectStruct = {}
    
    --System/Display
    zEffectStruct.sDisplayName = "New Effect"
    zEffectStruct.sIcon = "Unknown"
    zEffectStruct.sDisplayOnApply = "No Effect String"
    zEffectStruct.sAnimation = "Sword Slash"
    zEffectStruct.sDamageTypeIcon = "Slashing"
    
    --Tags
    zEffectStruct.sTypeTag = "Null"
    zEffectStruct.iTypeAmount = 1
    
    --Other Tags
    zEffectStruct.zaTagList = {}
    
    --Stats
    zEffectStruct.bBenefitsFromItemPower = false
    zEffectStruct.iDamageResistFlag = gciStatIndex_Resist_Slash
    zEffectStruct.iDuration = piDuration
    zEffectStruct.iDamageFactorDescriptionFlag = gciDoTEven
    
    --Damage-Per-Turn-Table. Default is to make the damage factors sum to pfTotalDamage.
    zEffectStruct.faTurnFactors = {}
    for i = 1, piDuration, 1 do
        zEffectStruct.faTurnFactors[i] = pfTotalDamage / piDuration
    end
    
    -- |[Damage Type Handling]|
    --If the damage type passed in is out of range, print an error:
    if(piDamageType < gciDamageType_Begin or piDamageType >= gciDamageType_Total) then
        io.write("fnCreateDoTEffectPrototype: Error, illegal damage type: " .. piDamageType .. "\n")
    
    --Otherwise, use the damage type presets.
    else
        zEffectStruct.sDisplayOnApply   = gzDamageTypes[piDamageType].sDotText
        zEffectStruct.sAnimation        = gzDamageTypes[piDamageType].sAnimation
        zEffectStruct.sDamageTypeIcon   = gzDamageTypes[piDamageType].sDamageTypeIcon
        zEffectStruct.sTypeTag          = gzDamageTypes[piDamageType].sTypeTag
        zEffectStruct.iDamageResistFlag = piDamageType + gciStatIndex_Resist_Start
    end
    
    -- |[Finish Up]|
    return zEffectStruct
end
