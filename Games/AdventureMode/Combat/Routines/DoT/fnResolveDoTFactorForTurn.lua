-- |[ =============================== fnResolveDoTDamageForTurn() ============================== ]|
--Given the UniqueID of a DoT effect, resolves its damage dealt for the requested turn.
-- Same rules as above function.
function fnResolveDoTDamageForTurn(piUniqueID, piTurn, piAttackerAttackPower, piDefenderResistance)
    
    --Error check:
    if(piUniqueID            == nil) then return 0.0 end
    if(piTurn                == nil) then return 0.0 end
    if(piAttackerAttackPower == nil) then return 0.0 end
    if(piDefenderResistance  == nil) then return 0.0 end
    
    --Check if the number is out of range:
    local iFactorsTotal = VM_GetVar("Root/Variables/Combat/"  .. piUniqueID .. "/iFactorsTotal", "I")
    if(piTurn < 0 or piTurn >= iFactorsTotal) then
        io.write("fnResolveDoTDamageForTurn: Error, turn " .. piTurn .. " out of range: " .. iFactorsTotal .. "\n")
        return 0
    end
    
    --Resolve the name of the variables.
    local sVarName = string.format("fFactor%02i", piTurn)
    local fDamageFactor = VM_GetVar("Root/Variables/Combat/"  .. piUniqueID .. "/" .. sVarName, "N")
    --io.write("Damage factor for turn " .. piTurn .. " was " .. fDamageFactor .. "\n")
    
    --Run damage computation.
    local iDamageThisTurn = fnComputeCombatDamage(piAttackerAttackPower * fDamageFactor, 0, piDefenderResistance)
    
    --Pass it back.
    return iDamageThisTurn
    
end
