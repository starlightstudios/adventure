-- |[ ================================ fnGetLastStatisticPack() ================================ ]|
--Returns the last statistic pack used by the given entity, with their ID passed in. This is used
-- for figuring out how much damage the last attack did or if it missed, etc.
--Returns nil on error. Otherwise, returns the statistic pack.
function fnGetLastStatisticPack(piOriginatorID)
    
    -- |[Argument Check]|
    if(piOriginatorID == nil) then return nil end
    
    -- |[Scan]|
    --Scan backwards through the abilities used to find the one used by our owner. It should be the
    -- last one, but a counterattack might take that slot.
    local iCurrentTurn = gzaCombatAbilityStatistics.iTurnsElapsed
    for i = #gzaCombatAbilityStatistics.zaStatisticsPacks, 1, -1 do

        --Get the package.
        local zStatsPack = gzaCombatAbilityStatistics.zaStatisticsPacks[i]
        
        --Error checks:
        if(zStatsPack == nil) then
            --io.write("  Error, out of range.\n")
        elseif(zStatsPack.iActorID ~= piOriginatorID) then
            --io.write("  Error, not action by owner " .. zStatsPack.iActorID .. " vs. " .. iOwnerID .. "\n")
        
        --Checks passed:
        else
            return zStatsPack
        end
    end
    
    --Not found.
    return nil
end
