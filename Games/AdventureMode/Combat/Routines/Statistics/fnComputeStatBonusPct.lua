-- |[ ================================ fnComputeStatBonusPct() ================================= ]|
--Version of fnApplyStatBonusPct() that computes and returns the value, but does not modify it.
function fnComputeStatBonusPct(piStatIndex, pfPercent)
    
    --Base value.
    local iBaseVal =      AdvCombatEntity_GetProperty("Statistic", gciStatGroup_Base,      piStatIndex)
    iBaseVal = iBaseVal + AdvCombatEntity_GetProperty("Statistic", gciStatGroup_Job,       piStatIndex)
    iBaseVal = iBaseVal + AdvCombatEntity_GetProperty("Statistic", gciStatGroup_Equipment, piStatIndex)
    
    --Bonus as percentage of base value.
    local iBonus = math.floor(iBaseVal * pfPercent)
    return iBonus
end
