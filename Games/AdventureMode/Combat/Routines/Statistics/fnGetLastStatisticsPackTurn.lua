-- |[ ============================= fnGetLastStatisticsPackTurn() ============================== ]|
--Returns the most recent statistics pack matching the passed in criteria. Can legally return nil 
-- if no pack was found matching the criteria. This prioritizes the last created pack in order to 
-- allow abilities to respond to actions that just happened, such as awarding extra healing.
--Same as fnGetLastStatisticsPack() but has the additional turn requirement.
function fnGetLastStatisticsPackTurn(piTurn, piOwnerID)
    
    -- |[Argument Check]|
    if(piTurn    == nil) then return nil end
    if(piOwnerID == nil) then return nil end
    
    -- |[Scan]|
    --Scan backwards through the abilities used to find the one used by our owner. It should be the
    -- last one, but a counterattack might take that slot.
    local iCurrentTurn = gzaCombatAbilityStatistics.iTurnsElapsed
    for i = #gzaCombatAbilityStatistics.zaStatisticsPacks, 1, -1 do

        --Get the package.
        local zStatsPack = gzaCombatAbilityStatistics.zaStatisticsPacks[i]
        
        --Error checks:
        if(zStatsPack == nil) then
            --io.write("  Error, out of range.\n")
        elseif(zStatsPack.iTurnElapsed ~= piTurn) then
            --io.write("  Error, incorrect turn " .. zStatsPack.iTurnElapsed .. " vs. " .. iCurrentTurn .. "\n")
        elseif(zStatsPack.iActorID ~= piOwnerID) then
            --io.write("  Error, not action by owner " .. zStatsPack.iActorID .. " vs. " .. iOwnerID .. "\n")
        
        --Checks passed:
        else
            return gzaCombatAbilityStatistics.zaStatisticsPacks[i]
        end
    end
    
    -- |[No Matches]|
    --Return nil to indicate there's nothing here.
    return nil
end