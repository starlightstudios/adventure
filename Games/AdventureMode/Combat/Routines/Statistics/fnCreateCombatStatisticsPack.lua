-- |[ ============================= fnCreateCombatStatisticsPack() ============================= ]|
--Creates and returns a combat statistics pack with all the default values filled in. These packages
-- are stored in gzaCombatAbilityStatistics which is reset at the start of every battle.
--The caller should take the returned pack and fill everything in as needed.
function fnCreateCombatStatisticsPack()
    
    -- |[Setup]|
    local zPackage = {}
    
    -- |[Global Information]|
    zPackage.iTurnElapsed = 0
    
    -- |[Actor Information]|
    zPackage.iActorID = 0
    zPackage.sActorInternalName = "Null"
    zPackage.sActorDisplayName = "Null"
    
    -- |[Ability Information]|
    zPackage.sAbilityMasterListName = "Null"
    
    -- |[Target Information]|
    zPackage.zaTargetInfo = {} --Made of zTargetInfo packs, below.
    
    -- |[Finish Up]|
    return zPackage
end

--Used for each individual target of an ability above.
function fnCreateStatisticsTargetPack()
    
    -- |[Setup]|
    local zTargetInfo = {}
    
    -- |[Target Info]|
    zTargetInfo.iTargetID = 0
    
    -- |[Statistics]|
    zTargetInfo.iDamageDealt = 0
    zTargetInfo.iInfluenceDealt = 0
    zTargetInfo.iHealingDealt = 0
    zTargetInfo.bWasMiss = false
    zTargetInfo.bWasGlance = false
    zTargetInfo.bWasCritical = false
    zTargetInfo.bEffectApplied = true
    
    -- |[Finish Up]|
    return zTargetInfo
end