-- |[ ==================================== fnGetBaseStat() ===================================== ]|
--When an effect provides a percentage buff to a stat, the "base" value is what is being used as
-- the percentage base. This is Base + Job + Equipment.
function fnGetBaseStat(piStatIndex)
    local iBaseVal =      AdvCombatEntity_GetProperty("Statistic", gciStatGroup_Base,      piStatIndex)
    iBaseVal = iBaseVal + AdvCombatEntity_GetProperty("Statistic", gciStatGroup_Job,       piStatIndex)
    iBaseVal = iBaseVal + AdvCombatEntity_GetProperty("Statistic", gciStatGroup_Equipment, piStatIndex)
    return iBaseVal
end
