-- |[ =================================== fnMarkdownToArrays =================================== ]|
--This function takes a statistic markdown, of format "Acc|Flat|1 Evd|Pct|0.10" and splits it into
-- a stat array and a tag array, which it then returns.
--The format of the stat array is {StatIndex0, StatAmount0, StatIndex1, StatAmount1, etc}
--The format of the tag array is {{"Tag0", TagAmount0}, {"Tag1", TagAmount1}, etc}

--Note: If the second argument is missing, it is assumed to be "Flat". In addition, shortenings
-- of "F" -> "Flat" and "P" -> "Pct" are usable.
function fnMarkdownToArrays(psStatString)
    
    -- |[ ============ Returnables =========== ]|
    --By default, the arrays are empty. It is legal for them to be returned empty.
    local iaStatArray = {}
    local zaTagArray = {}
    
    -- |[Argument Check]|
    if(psStatString == nil) then return iaStatArray, zaTagArray end
    if(psStatString == "")  then return iaStatArray, zaTagArray end
    
    -- |[Setup]|
    --Break the markdown into parts, using " " as a delimiter.
    local saStatStrings = fnSubdivide(psStatString, " ")
    
    -- |[ ========== Stat Iteration ========== ]|
    --For Each Stat:
    for i = 1, #saStatStrings, 1 do
        
        -- |[Basics]|
        --Variables.
        local bGotMatch = false
        
        --Get the string.
        local sBaseString = saStatStrings[i]
        
        --Subdivide it. We expect 3 parts. '|' is the delimiter.
        local saSubstrings = fnSubdivide(sBaseString, "|")
        
        --Error check.
        if(#saSubstrings < 2 or #saSubstrings > 3) then
            io.write("fnMarkdownToArrays() - Warning. Stat string " .. psStatString .. " substring " .. sBaseString .. " (substring #" .. i .. ") was malformed.\n")
            io.write(" Markdowns require 2 or 3 entries, this has " .. #saSubstrings .. ". Listing:\n")
            for p = 1, #saSubstrings, 1 do
                io.write(" " .. saSubstrings[p] .. "\n")
            end
            return iaStatArray, zaTagArray
        end
        
        -- |[Argument Resolve]|
        --The first argument is the type, which can be a stat or a tag.
        local sTypeArg = saSubstrings[1]
        
        --The second argument is the modifier type. It is optional, and defaults to "Flat" if not provided.
        local sModifier = "Flat"
        if(#saSubstrings == 3) then
            
            --Base.
            sModifier = saSubstrings[2]
            
            --Shorthand:
            if(sModifier == "F") then
                sModifier = "Flat"
            elseif(sModifier == "FS") then
                sModifier = "FlatSev"
            elseif(sModifier == "P") then
                sModifier = "Pct"
            elseif(sModifier == "PS") then
                sModifier = "PctSev"
            end
        end
        
        --The third part is an amount. It is nominally floating-point, but must be an integer.
        -- This is because the markdowns don't handle percentages in this format. Tags and stats are integers.
        -- If only two substrings are provided, the second string is the amount instead.
        local iAmount = 0
        if(#saSubstrings == 2) then
            iAmount = math.floor(tonumber(saSubstrings[2]))
        else
            iAmount = math.floor(tonumber(saSubstrings[3]))
        end
    
        -- |[Statistic Modifiers]|
        --Iterate across the stat modifiers.
        if(bGotMatch == false) then
            for p = 1, giModTableTotal, 1 do
        
                --Match?
                if(sTypeArg == gzaModTable[p][1]) then
                    
                    --Get the associated stat number. Add it to the stat array, along with its count.
                    table.insert(iaStatArray, gzaModTable[p][2])
                    table.insert(iaStatArray, iAmount)
                    
                    --Stop execution in this type.
                    bGotMatch = true
                    break
                end
            end
        end
        
        -- |[Tag Modifiers]|
        --If the modifier wasn't found, this might be a tagged stat. Check those.
        if(bGotMatch == false) then
            for p = 1, giTagModTableTotal, 1 do
    
                --Match?
                if(sTypeArg == gzaTagModTable[p][1]) then
            
                    --Add it, along with its count.
                    table.insert(zaTagArray, {gzaTagModTable[p][2], iAmount})
                
                    --Stop execution in this type.
                    bGotMatch = true
                    break
                end
            end
        end
        
        -- |[Error]|
        --Warn that the stat/tag wasn't found.
        if(bGotMatch == false) then
            io.write("Warning: Unable to find markdown " .. sBaseString .. "\n")
        end
    end
    
    -- |[ ============= Finish Up ============ ]|
    return iaStatArray, zaTagArray
    
end
