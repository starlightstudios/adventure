-- |[ =================================== fnApplyStatBonus() =================================== ]|
--Shorthand to quickly apply a stat bonus to a given stat. Frequently used in effect scripts.
function fnApplyStatBonus(piStatIndex, piAmount)
    
    --Get health percent in case the health max changes.
    local fHealthPct = AdvCombatEntity_GetProperty("Health") / AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
    
    --Compute and apply bonus.
    local iBonus = AdvCombatEntity_GetProperty("Statistic", gciStatGroup_TempEffect, piStatIndex)
    iBonus = iBonus + piAmount
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_TempEffect, piStatIndex, iBonus)
    AdvCombatEntity_SetProperty("Recompute Stats")
    
    --Set health percent.
    if(piStatIndex == gciStatIndex_HPMax) then
        AdvCombatEntity_SetProperty("Health Percent", fHealthPct)
    end
end
