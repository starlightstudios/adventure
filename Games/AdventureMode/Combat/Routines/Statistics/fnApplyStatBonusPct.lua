-- |[ ================================= fnApplyStatBonusPct() ================================== ]|
--Version of fnApplyStatBonus() that applies a stat bonus as a percentage of a base. Returns the applied bonus.
function fnApplyStatBonusPct(piStatIndex, pfPercent)
    
    --Base value.
    local iBaseVal =      AdvCombatEntity_GetProperty("Statistic", gciStatGroup_Base,      piStatIndex)
    iBaseVal = iBaseVal + AdvCombatEntity_GetProperty("Statistic", gciStatGroup_Job,       piStatIndex)
    iBaseVal = iBaseVal + AdvCombatEntity_GetProperty("Statistic", gciStatGroup_Equipment, piStatIndex)
    
    --Get health percent in case the health max changes.
    local fHealthPct = AdvCombatEntity_GetProperty("Health") / AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
    
    --Bonus as percentage of base value.
    local iBonus = math.floor(iBaseVal * pfPercent)
    
    --Current value.
    local iCurrentVal = AdvCombatEntity_GetProperty("Statistic", gciStatGroup_TempEffect, piStatIndex)
    iCurrentVal = iCurrentVal + iBonus
    AdvCombatEntity_SetProperty("Statistic", gciStatGroup_TempEffect, piStatIndex, iCurrentVal)
    
    --Recompute stats, return the bonus value.
    AdvCombatEntity_SetProperty("Recompute Stats")
    local iFinalValue = AdvCombatEntity_GetProperty("Statistic", piStatIndex)
    
    --Set health percent.
    if(piStatIndex == gciStatIndex_HPMax) then
        AdvCombatEntity_SetProperty("Health Percent", fHealthPct)
    end
    return iBonus
end
