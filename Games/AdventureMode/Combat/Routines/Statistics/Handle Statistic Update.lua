-- |[ ================================= Handle Statistic Update ================================ ]|
--Update script called by the combat routine at various times. This is used to track statistical
-- information about combat.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "I")
--io.write("Statistics Update: " .. iSwitchType .. "\n")

-- |[ ====================================== Combat Begins ===================================== ]|
--Reset the combat statistics back to baseline.
if(iSwitchType == giCombatStats_Begin) then

    --Debug.
    --io.write("Statistic Update: Combat Begins.\n")

    --Reset everything. Note that the turn is set to -1. This is because the new turn order will
    -- increment it to 0 on the 0th turn.
    gzaCombatAbilityStatistics = {}
    gzaCombatAbilityStatistics.iTurnsElapsed = -1
    gzaCombatAbilityStatistics.zaStatisticsPacks = {}

-- |[ ======================================= Turn Begins ====================================== ]|
--Increments the turn counter.
elseif(iSwitchType == giCombatStats_NewTurn) then

    --Debug.
    --io.write("Statistic Update: New turn begins.\n")
    
    --Increment.
    gzaCombatAbilityStatistics.iTurnsElapsed = gzaCombatAbilityStatistics.iTurnsElapsed + 1
    
    --Report.
    --io.write(" Turn is: " .. gzaCombatAbilityStatistics.iTurnsElapsed .. "\n")

-- |[ ===================================== Action Begins ====================================== ]|
--A new acting entity takes the stage. Creates a new statistics pack for their action.
elseif(iSwitchType == giCombatStats_NewAction) then

    --Debug.
    --io.write("Statistic Update: New action begins.\n")
    
    --Create the package and put it at the end of the list.
    local zStatsPack = fnCreateCombatStatisticsPack()
    table.insert(gzaCombatAbilityStatistics.zaStatisticsPacks, zStatsPack)
    
    --Record what turn we're on.
    zStatsPack.iTurnElapsed = gzaCombatAbilityStatistics.iTurnsElapsed
    
end