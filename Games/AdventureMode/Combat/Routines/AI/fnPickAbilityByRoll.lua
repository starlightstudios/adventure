-- |[ =================================== fnPickAbilityByRoll ================================== ]|
--Selects a random ability from the enemy's ability list by probability.
--Returns the ability selected's master slot.
function fnPickAbilityByRoll(pbActivateAbility)
    
    -- |[Argument Check]|
    if(pbActivateAbility == nil) then return end
    
    -- |[Setup]|
    --Diagnostics.
    gzDiagnostics.fnAIExec(gciAI_Diagnostic_SkillRoll, "AI runs fnPickAbilityByRoll().\n")
    
    --Get how many abilities are available.
    local iAbilitiesTotal = AdvCombatEntity_GetProperty("Abilities Total")
    
    --Diagnostics, print available ability list.
    if((giAI_Diagnostic_Level & gciAI_Diagnostic_SkillRoll) > 0) then
        io.write(" Ability listing. Total abilities: " .. iAbilitiesTotal .. "\n")
        for x = 0, gciAbilityGrid_XSize-1, 1 do
            for y = 0, gciAbilityGrid_YSize-1, 1 do
                
                --Get.
                local sName   = AdvCombatEntity_GetProperty("Ability In Slot", x, y)
                local iChance = AdvCombatEntity_GetProperty("Probability In Slot", x, y)
                
                --Print.
                if(sName ~= "Null" and iChance > 0) then
                    io.write("  " .. sName .. ": " .. iChance .. "\n") 
                end
            end
        end
    end
    
    --If it's zero, pass turn:
    if(iAbilitiesTotal == 0) then
        gzDiagnostics.fnAIExec(gciAI_Diagnostic_SkillRoll, " Failed to select ability. None present.\n")
        return -1
    end
    
    -- |[Sum Probabilities]|
    --Run across abilities. Sum up the probabilities available.
    local iTotalRoll = 0
    local saNames = {}
    for x = 0, gciAbilityGrid_XSize-1, 1 do
        for y = 0, gciAbilityGrid_YSize-1, 1 do
            
            --Ignore zeroes.
            local iAddRoll = AdvCombatEntity_GetProperty("Probability In Slot", x, y)
            if(iAddRoll > 0) then
                
                --Add to total.
                iTotalRoll = iTotalRoll + iAddRoll
                
                --Place entry in list.
                local p = #saNames + 1
                saNames[p] = {AdvCombatEntity_GetProperty("Ability In Slot", x, y), iAddRoll}
            end
        end
    end
    
    --Debug.
    gzDiagnostics.fnAIExec(gciAI_Diagnostic_SkillRoll, " Total Probability: " .. iTotalRoll .. "\n")
    
    --Range check.
    if(iTotalRoll == 0) then
        gzDiagnostics.fnAIExec(gciAI_Diagnostic_SkillRoll, " Failed, probability was zero.\n")
        return -1
    end
    
    -- |[Roll]|
    --Roll.
    local iAbilityRoll = LM_GetRandomNumber(0, iTotalRoll-1)

    -- |[Select Ability]|
    --Iterate across the list, subtracting values until we go under zero. When we do, that's the ability to use.
    local iMasterSlot = -1
    for i = 1, #saNames, 1 do
        local sAbilityName = saNames[i][1]
        local iWeight = saNames[i][2]
        iAbilityRoll = iAbilityRoll - iWeight
        
        --If the roll value goes below zero, select this ability:
        if(iAbilityRoll < 0) then
            iMasterSlot = AdvCombatEntity_GetProperty("Slot of Ability In Master List", sAbilityName)
            if(pbActivateAbility) then
                AdvCombat_SetProperty("Set Ability As Active", iMasterSlot)
            end
            break
        end
    end
    
    --Return the ability slot.
    gzDiagnostics.fnAIExec(gciAI_Diagnostic_SkillRoll, " AI selects ability in slot: " .. iMasterSlot .. "\n")
    return iMasterSlot
end
