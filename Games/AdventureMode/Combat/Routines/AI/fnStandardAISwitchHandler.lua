-- |[ =============================== fnStandardAISwitchHandler ================================ ]|
--Called by an AI to determine if that AI is currently affected by a status effect that changes
-- the AI, such as "Fascinate" or "Confuse". 
--If affected, returns a script path to be called. Can return "Handled" as well, indicating no
-- script needs to be called, but the action is handled in any case. Returns "Null" if no
-- AI switch is affecting the AI in question.
--Note that Stun is not handled here.
function fnStandardAISwitchHandler()

    -- |[Setup]|
    --This subroutine will return which AI should control the AI. It will return "Normal" if there
    -- are no tags present or the roll failed.
    local sAIResult = fnCheckAITags()
    
    -- |[Ambush]|
    --AI is Ambushed. Causes them to waste a turn.
    if(AdvCombatEntity_GetProperty("Is Ambushed") == true) then
        AdvCombatEntity_SetProperty("Ambushed", false)
        AdvCombat_SetProperty("Set Ability As Active", gciAI_AmbushAbilitySlot)
        AdvCombat_SetProperty("Mark Handled Action")
        AdvCombat_SetProperty("Run Active Ability On Active Targets")
        return "Handled"
    end

    -- |[Switching AIs]|
    if(sAIResult == "Fascinate") then
    elseif(sAIResult == "Confound") then
    elseif(sAIResult == "Berserk") then
    elseif(sAIResult == "Confuse") then
        return gsRoot .. "Combat/AIs/001 Confused AI.lua"
    elseif(sAIResult == "Charm") then
    end

    -- |[No Switch]|
    return "Null"
end
