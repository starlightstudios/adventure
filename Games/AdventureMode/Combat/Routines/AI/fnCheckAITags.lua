-- |[ ====================================== Check AI Tags ===================================== ]|
--When an AdvCombatEntity is on the activity stack, checks the tags that can modify their AI and
-- returns which one is in control. Returns "Normal" if no special AI is in control. Uses a random
-- 1-100 roll to determine control patterns.
function fnCheckAITags()
    
    -- |[Get Values]|
    local iTagFascinate = AdvCombatEntity_GetProperty("Tag Count", "Fascinate")
    local iTagConfound  = AdvCombatEntity_GetProperty("Tag Count", "Confound")
    local iTagBerserk   = AdvCombatEntity_GetProperty("Tag Count", "Berserk")
    local iTagConfuse   = AdvCombatEntity_GetProperty("Tag Count", "Confuse")
    local iTagCharm     = AdvCombatEntity_GetProperty("Tag Count", "Charm")
    local iTagTotal = iTagFascinate + iTagConfound + iTagBerserk + iTagConfuse + iTagCharm
    
    -- |[Checks]|
    --If the tags total to zero, no need to continue.
    if(iTagTotal < 1) then return "Normal" end
    
    --If the tags total over 100, we don't need to bother rolling.
    if(iTagTotal >= 100) then
    
    --Otherwise, we need to roll to see if the AI is modified this turn.
    else
        local iRoll = LM_GetRandomNumber(1, 100)
        if(iRoll > iTagTotal) then return "Normal" end
    end
    
    --If we made it this far, then one of the tags is in control. Find the tag that has the highest count.
    -- It is possible the tags might be tied!
    local zaValues = {{iTagFascinate, "Fascinate"}, {iTagConfound, "Confound"}, {iTagBerserk, "Berserk"}, {iTagConfuse, "Confuse"}, {iTagCharm, "Charm"}}
    local iHighestCount = -1
    local saHighestNames = {}
    for i = 1, #zaValues, 1 do
        if(zaValues[i][1] > iHighestCount) then
            iHighestCount = zaValues[i][1]
            saHighestNames = {zaValues[i][2]}
        elseif(zaValues[i][1] == iHighestCount) then
            local iEntries = #saHighestNames
            saHighestNames[iEntries+1] = zaValues[i][2]
        end
    end
    
    --If there are somehow no entries on the highest names list, a bug occurred.
    if(#saHighestNames < 1) then return "Normal" end
    
    --Return the zeroth entry on the highest names list, since the effects are in order of priority.
    return saHighestNames[1]
end
