-- |[ ============================= fnPickAbilityByManualWeight() ============================== ]|
--First, this function creates an element that can be added to a list. It contains the name of the
-- skill and its associated weight. Optional elements can be added afterwards.
function fnCreateSkillWeightPack(psSkillName, piWeight)
    
    -- |[Argument Check]|
    if(psSkillName == nil) then return end
    if(piWeight    == nil) then return end
    
    -- |[Create]|
    local zPack = {}
    zPack[1] = psSkillName
    zPack[2] = piWeight
    
    -- |[Optional Arguments]|
    --Put them here.
    
    -- |[Finish Up]|
    return zPack
end

--This function will select a skill from a table assembled out of the above packages. It will
-- return the slot of the selected skill in the table, in addition to activating it.
--On error, returns -1.
function fnPickAbilityByManualWeight(pzaTable)
    
    -- |[Argument Check]|
    if(pzaTable == nil) then return -1 end
    
    -- |[Sum Weights]|
    --Sum.
    local iMaxRoll = 0
    for i = 1, #pzaTable, 1 do
        iMaxRoll = iMaxRoll + pzaTable[i][2]
    end
    
    --If the max roll is less than one, that's an error.
    if(iMaxRoll < 1) then return -1 end
    
    -- |[Select Skill]|
    --Roll.
    local iSkillRoll = LM_GetRandomNumber(1, iMaxRoll)
    
    --Resolve skill.
    local iSelectedSlotInTable = -1
    for i = 1, #pzaTable, 1 do
        
        --Decrement.
        iSkillRoll = iSkillRoll - pzaTable[i][2]
        
        --If the value goes below 1, this is the skill in question.
        if(iSkillRoll <= 0) then
            iSelectedSlotInTable = i
            break
        end
    end

    --Error check.
    if(iSelectedSlotInTable == -1) then return -1 end

    -- |[Skill Activation]|
    --Prime the skill.
    local iActivatedSkillSlotInCharacter = AdvCombatEntity_GetProperty("Slot of Ability In Master List", pzaTable[iSelectedSlotInTable][1])
    AdvCombat_SetProperty("Set Ability As Active", iActivatedSkillSlotInCharacter)
    
    -- |[Finish Up]|
    return iSelectedSlotInTable
end