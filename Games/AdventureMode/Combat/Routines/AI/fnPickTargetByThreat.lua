-- |[ ================================= fnPickTargetByThreat() ================================= ]|
--When an AI is using threat to select targets, the highest threat has a 70% chance to be targeted,
-- the second highest a 20%, and all others split the remaining 10% chance. This function performs
-- that operation, and reduces the selected target's threat.
--Exceptions: If the highest threat is 5x the next nearest target, that target is selected 100% of 
-- the time.
--An ability must be selected at this time.
--Returns which target cluster was selected.
function fnPickTargetByThreat(pbReduceThreat, pbRunAbility)
    
    -- |[ ============== Setup =============== ]|
    -- |[Variables]|
    local iOwnerID = RO_GetID()
    --io.write("fnPickTargetByThreat() executes.\n")
    --io.write(" Owner ID: " .. iOwnerID .. "\n")
    
    -- |[Paint Targets]|
    --Run the ability script to paint active targets.
    AdvCombat_SetProperty("Run Active Ability Target Script")
    
    --Get target clusters.
    local iMarkedCluster = 0
    local iTotalTargetClusters = AdvCombat_GetProperty("Total Target Clusters")
    
    -- |[ ========= Simple Handling ========== ]|
    -- |[Zero Target Clusters]|
    --Nobody to use ability on, pass turn.
    if(iTotalTargetClusters == 0) then
        --io.write(" Target routine completes. No targets, passing turn.\n")
        return
    
    -- |[One Target Cluster]|
    --If there is exactly one cluster, mark that cluster.
    elseif(iTotalTargetClusters == 1) then
        --io.write(" Exactly one target cluster available: " .. iMarkedCluster .. "\n")
        AdvCombat_SetProperty("Set Target Cluster As Active", iMarkedCluster)
    
    -- |[ ===== Multiple Target Clusters ===== ]|
    --Multiple clusters, determine threat and roll.
    else
    
        -- |[Debug]|
        --io.write(" Target clusters after scan: " .. iTotalTargetClusters .. "\n")
    
        -- |[Threat Scanning]|
        --We need to check threat for each cluster. Build a list of average threats for each
        -- element in each cluster.
        local iLowestThreat = 100000
        local iHighestThreat = -1000
        local iSecondHighestThreat = -1000
        local iHighestThreatSlot = 1
        local iSecondHighestThreatSlot = 1
        local iaThreats = {}
        for i = 1, iTotalTargetClusters, 1 do
            
            --Zero base threat.
            iaThreats[i] = {0, i}
            
            --For each target:
            local iTotalTargets = AdvCombat_GetProperty("Total Targets In Cluster", i-1)
            for p = 1, iTotalTargets, 1 do
            
                --Get this target's ID.
                local iTargetID = AdvCombat_GetProperty("Target ID In Cluster", i-1, p-1)
            
                --Get the threat associated with the target. It can be 0.
                local iThreat = VM_GetVar("Root/Variables/Combat/" .. iOwnerID .. "_AI/" .. iTargetID.. "_Threat", "I")
                iaThreats[i][1] = iaThreats[i][1] + (iThreat / iTotalTargets)
            
            end
        
            --Check if this is the lowest threat. This is to check if all targets have the same threat.
            if(iaThreats[i][1] < iLowestThreat) then iLowestThreat = iaThreats[i][1] end
        
            --Check if this is the highest threat. If it is, the previous highest becomes the second highest.
            if(iaThreats[i][1] >= iHighestThreat) then
                iSecondHighestThreat = iHighestThreat
                iSecondHighestThreatSlot = iHighestThreatSlot
                iHighestThreat = iaThreats[i][1]
                iHighestThreatSlot = i
                
            --Check if this is the second highest threat.
            elseif(iaThreats[i][1] >= iSecondHighestThreat) then
                iSecondHighestThreat = iaThreats[i][1]
                iSecondHighestThreatSlot = i
            end
        end
        
        -- |[Debug]|
        --io.write(" Listing threat averages in clusters:\n")
        --for i = 1, iTotalTargetClusters, 1 do
        --    io.write("  " .. i .. ": " .. iaThreats[i][1] .. "\n")
        --end
        --io.write(" Highest threat: " .. iHighestThreat .. " in cluster " .. iHighestThreatSlot .. "\n")
        --io.write(" Second threat: " .. iSecondHighestThreat .. " in cluster " .. iSecondHighestThreatSlot .. "\n")

        -- |[Equal Threats]|
        --If everyone has an equal threat rating, then pick a target totally at random.
        if(iLowestThreat == iHighestThreat) then
            iMarkedCluster = LM_GetRandomNumber(1, iTotalTargetClusters) - 1
            AdvCombat_SetProperty("Set Target Cluster As Active", iMarkedCluster)
        
        -- |[Target Selection]|
        --All target clusters have been populated with their average threat values. Perform a roll.
        else
            local iSlotRoll = LM_GetRandomNumber(1, 100)
            --io.write(" Slot roll: " .. iSlotRoll .. "\n")
            
            --Calculate the threat needed for a 100% pick chance.
            local cfThreatFloorForMandateFactor = 5.0
            local iSecondMulti = iSecondHighestThreatSlot * cfThreatFloorForMandateFactor
            
            --If the highest threat is 5x the next highest, then that is always selected.
            if(iHighestThreat >= iSecondMulti) then
                iMarkedCluster = iHighestThreatSlot - 1
                AdvCombat_SetProperty("Set Target Cluster As Active", iMarkedCluster)
                --io.write(" 5x Autoselect.\n")
        
            --There is a 70% chance to pick the cluster with the highest threat:
            elseif(iSlotRoll <= 70) then
                iMarkedCluster = iHighestThreatSlot - 1
                AdvCombat_SetProperty("Set Target Cluster As Active", iMarkedCluster)
                --io.write(" Rolled highest cluster.\n")
                
            --There is a 20% chance to pick the cluster with the second highest threat:
            elseif(iSlotRoll <= 90 or iTotalTargetClusters == 2) then
                iMarkedCluster = iSecondHighestThreatSlot - 1
                AdvCombat_SetProperty("Set Target Cluster As Active", iMarkedCluster)
                --io.write(" Rolled second highest cluster.\n")
                
            --All remaining clusters are equally divided into the last 10%.
            else
            
                --Debug
                --io.write(" Rolled random other cluster.\n")
            
                --Assemble a new list that does not contain the two highest threats and pick one element at random.
                local iNewThreatsTotal = 0
                local iaNewThreats = {}
                for i = 1, iTotalTargetClusters, 1 do
                    if(iaThreats[i][2] ~= iHighestThreatSlot and iaThreats[i][2] ~= iSecondHighestThreatSlot) then
                        iNewThreatsTotal = iNewThreatsTotal + 1
                        iaNewThreats[iNewThreatsTotal] = iaThreats[i][2]
                    end
                end
                
                --Roll.
                local iSlot = LM_GetRandomNumber(1, iNewThreatsTotal)
                iMarkedCluster = iaNewThreats[iSlot] - 1
                AdvCombat_SetProperty("Set Target Cluster As Active", iMarkedCluster)
            end
        end
    end
    
    -- |[ ===== Post Selection ===== ]|
    -- |[Threat Reduction]|
    --Each target in the selected cluster loses 40% of its threat. This keeps the threat values cycling.
    if(pbReduceThreat) then
        local iTotalTargets = AdvCombat_GetProperty("Total Targets In Cluster", iMarkedCluster)
        for p = 1, iTotalTargets, 1 do
        
            --Get this target's ID.
            local iTargetID = AdvCombat_GetProperty("Target ID In Cluster", iMarkedCluster, p-1)
        
            --Get the threat associated with the target. It can be 0.
            local iThreat = VM_GetVar("Root/Variables/Combat/" .. iOwnerID .. "_AI/" .. iTargetID.. "_Threat", "I")
            iThreat = iThreat * 0.60
            VM_SetVar("Root/Variables/Combat/" .. iOwnerID .. "_AI/" .. iTargetID.. "_Threat", "N", iThreat)
        end
    end
    
    -- |[Execute Ability]|
    --Run the ability.
    if(pbRunAbility) then
        AdvCombat_SetProperty("Mark Handled Action")
        AdvCombat_SetProperty("Run Active Ability On Active Targets")
    end
    
    -- |[Return]|
    --Returns the marked target cluster.
    --io.write(" Target routine completes. Cluster: " .. iMarkedCluster .. "\n")
    return iMarkedCluster
end
