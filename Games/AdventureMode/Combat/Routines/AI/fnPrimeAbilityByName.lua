-- |[ ================================= fnPrimeAbilityByName() ================================= ]|
--Given the name of an ability, primes it. Optionally returns the slot of the ability found, or -1
-- if the ability was not found. Also paints targets unless the optional flag pbDontPaintTargets is
-- set to true.
function fnPrimeAbilityByName(psAbilityName, pbDontPaintTargets)
    
    -- |[Argument Check]|
    if(psAbilityName      == nil) then return -1 end
    if(pbDontPaintTargets == nil) then pbDontPaintTargets = false end
    
    -- |[Check Slot]|
    --If it doesn't exist, print an error.
    local iMasterSlot = AdvCombatEntity_GetProperty("Slot of Ability In Master List", psAbilityName)
    if(iMasterSlot == -1) then
        io.write("Warning: fnPrimeAbilityByName() could not find ability named " .. psAbilityName .. "\n")
        return iMasterSlot
    end
    
    --Execute.
    AdvCombat_SetProperty("Set Ability As Active", iMasterSlot)
    if(pbDontPaintTargets == false) then AdvCombat_SetProperty("Run Active Ability Target Script") end
    return iMasterSlot
end