-- |[ =================================== fnPreferentialID() =================================== ]|
--Returns a target cluster containing the given ID, if possible. Otherwise returns a random cluster.
-- The optional flag pbExecuteOnTarget will execute the ability as well.
function fnPreferentialID(piTargetID, pbExecuteOnTarget)
    
    -- |[Arg Check]|
    if(piTargetID        == nil) then return 0 end
    if(pbExecuteOnTarget == nil) then pbExecuteOnTarget = false end
    
    -- |[Setup]|
    local iTargetCluster = -1

    -- |[Get and Scan]|
    --Get target clusters. If there's only one, stop here.
    local iTotalTargetClusters = AdvCombat_GetProperty("Total Target Clusters")
    if(iTotalTargetClusters <= 1) then return 0 end
    
    --Scan all the painted clusters:
    for i = 0, iTotalTargetClusters-1, 1 do
        
        --For each target in cluster:
        local iTotalTargets = AdvCombat_GetProperty("Total Targets In Cluster", i)
        for p = 0, iTotalTargets-1, 1 do
            
            --Get the ID.
            local iTargetID = AdvCombat_GetProperty("Target ID In Cluster", i, p)
            
            --Match. Target this cluster.
            if(iTargetID == piTargetID) then
                iTargetCluster = i
                break
            end
        end
        
        --Stop iterating if a target was found.
        if(iTargetCluster ~= -1) then break end
    end
    
    -- |[No Preferential Target, Pick Random]|
    --If no cluster was marked, pick a random cluster.
    if(iTargetCluster == -1) then
        iTargetCluster = LM_GetRandomNumber(0, iTotalTargetClusters-1)
    end
    
    -- |[Optional: Execute]|
    --If flagged, execute the ability on the selected target.
    if(pbExecuteOnTarget == true) then
        AdvCombat_SetProperty("Set Target Cluster As Active", iTargetCluster)
        AdvCombat_SetProperty("Mark Handled Action")
        AdvCombat_SetProperty("Run Active Ability On Active Targets")
    end
    
    -- |[Finish Up]|
    return iTargetCluster
end