-- |[ ============================= fnStandardDecideActionSetup() ============================== ]|
--Used as a convenience function for AIs. If an AI is doing nothing special except specifying which
-- attack to use, then this can be called to do all the additional checks such as Stun, Ambush,
-- AI switch, "Is it my turn yet", etc.
--Returns true if the AI should proceed with its actions, false otherwise.
function fnStandardDecideActionSetup()
    
    -- |[ ==================== Pre-Exec Setup ==================== ]|
    -- |[Events]|
    --Make sure the AI needs to spawn events. This flag is only true when the AI needs to spawn events.
    local bCanSpawnEvent = AdvCombat_GetProperty("Is AI Spawning Events")
    if(bCanSpawnEvent == false) then return false end
    
    -- |[AI Routing]|
    --A subroutine will check if this AI is being redirected. If so, it will return "Handled" or the script
    -- to be called. If it returns "Null" then no redirection is in place.
    local sIsAIRedirected = fnStandardAISwitchHandler()
    
    -- |[Handled]|
    if(sIsAIRedirected == "Handled") then return false end
    
    -- |[Call Script]|
    if(sIsAIRedirected ~= "Null") then
        LM_ExecuteScript(sIsAIRedirected, gciAI_Decide_Action)
        return false
    end
    
    -- |[ ===================== Turn Checking ==================== ]|
    -- |[Check Turn]|
    --At this point, check if the AI's owner is the one who is actually acting. Anything above this point is status
    -- modifiers, everything below this point is attempting to execute an ability.
    local iOwnerID = RO_GetID()
    local iActingID = AdvCombat_GetProperty("ID of Acting Entity")
    if(iOwnerID ~= iActingID) then return false end

    -- |[ ===================== Stun Handler ===================== ]|
    --Standard stun handler. Will return true if this entity is stunned.
    local bIsStunned = fnStunHandleActionBegin()
    if(bIsStunned == true) then

        --Animation.
        AdvCombat_SetProperty("Set Ability As Active", gciAI_StunAbilitySlot)

        --Stop action here.
        AdvCombat_SetProperty("Mark Handled Action")
        AdvCombat_SetProperty("Run Active Ability On Active Targets")
        return false
    end
    
    -- |[ ================== All Checks Passed =================== ]|
    --Return true to proceed with action.
    return true
end
