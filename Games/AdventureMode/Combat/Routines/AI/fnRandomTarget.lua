-- |[ ===================================== fnRandomTarget ===================================== ]|
--Returns a random target cluster.
function fnRandomTarget()
    
    --If there's one target, return that.
    local iTotalTargetClusters = AdvCombat_GetProperty("Total Target Clusters")
    if(iTotalTargetClusters <= 1) then return 0 end
    
    --Roll.
    local iRoll = LM_GetRandomNumber(0, iTotalTargetClusters-1)
    return iRoll
end
