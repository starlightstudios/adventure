-- |[ =================================== fnApplyAIString() ==================================== ]|
--Applies the named AIString to the named Enemy, by modifying its tag count to use the named string's
-- index. This can remove tags if overriding an existing tag.
function fnApplyAIString(psEnemyName, psAIStringName)
    
    -- |[Argument Check]|
    if(psEnemyName    == nil) then return end
    if(psAIStringName == nil) then return end
    
    -- |[Locate Prototype]|
    local i = fnLocate(psEnemyName)
    if(i == -1) then return end
    
    -- |[Locate String]|
    for p = 1, #gzAIStringList, 1 do
    
        --Name match:
        if(gzAIStringList[p].sName == psAIStringName) then
    
            --Search tag array for AIString. If it already exists, override it.
            local bFoundMatch = false
            for q = 1, #gzStatArray[i].zaTags, 1 do
                if(gzStatArray[i].zaTags[q][1] == "AIString") then
                    bFoundMatch = true
                    gzStatArray[i].zaTags[q][2] = p
                    break
                end
            end

            --Apply tags if one wasn't found.
            if(bFoundMatch == false) then
                table.insert(gzStatArray[i].zaTags, {"AIString", p})
            end
            return
        end
    end
    
    -- |[Error]|
    io.write("fnApplyAIString(): Warning, no AIString named " .. psAIStringName .. " was found.\n")
end