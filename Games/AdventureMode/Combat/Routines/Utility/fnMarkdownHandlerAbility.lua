-- |[ =============================== fnMarkdownHandlerAbility() =============================== ]|
--Markdown handler used for StarlightStrings that have images and text next to each other.
-- Replaces special [tags] with numbers or variables.
--Markdown handlers often use global lists like gsaStatIcons to make a standard reusable set of
-- aliases. Make sure you're using the correct function!

--Markdown handler used for ability descriptions, particularly the ability prototype versions.
-- The format used is the same one as gsStandardAbilityPath, that's the ability structure passed in.
--Make sure the ability has finished setting up before calling this so it has all the variables.
--These use the Large 24x24 icons as opposed to the 19x19 icons used elsewhere.
function fnMarkdownHandlerAbility(pzStructure)
    
    -- |[Setup]|
    --Error check.
    if(pzStructure == nil) then return end
    
    --This table will store the remaps needed.
    local iRemapsTotal = 0
    local saRemaps = {}
    
    --Final version of the string.
    local sFinalString = ""
    
    --Used for skipping letters when a tag gets replaced.
    local iSkips = 0
    
    -- |[Weapon Damage Lookup Table]|
    local saDamageLookups = {}
    saDamageLookups[ 1] = {gciDamageType_Slashing,   "Root/Images/AdventureUI/DamageTypeIconsLg/Slashing"}
    saDamageLookups[ 2] = {gciDamageType_Piercing,   "Root/Images/AdventureUI/DamageTypeIconsLg/Piercing"}
    saDamageLookups[ 3] = {gciDamageType_Striking,   "Root/Images/AdventureUI/DamageTypeIconsLg/Striking"}
    saDamageLookups[ 4] = {gciDamageType_Flaming,    "Root/Images/AdventureUI/DamageTypeIconsLg/Flaming"}
    saDamageLookups[ 5] = {gciDamageType_Freezing,   "Root/Images/AdventureUI/DamageTypeIconsLg/Freezing"}
    saDamageLookups[ 6] = {gciDamageType_Shocking,   "Root/Images/AdventureUI/DamageTypeIconsLg/Shocking"}
    saDamageLookups[ 7] = {gciDamageType_Crusading,  "Root/Images/AdventureUI/DamageTypeIconsLg/Crusading"}
    saDamageLookups[ 8] = {gciDamageType_Obscuring,  "Root/Images/AdventureUI/DamageTypeIconsLg/Obscuring"}
    saDamageLookups[ 9] = {gciDamageType_Terrifying, "Root/Images/AdventureUI/DamageTypeIconsLg/Terrifying"}
    saDamageLookups[10] = {gciDamageType_Bleeding,   "Root/Images/AdventureUI/DamageTypeIconsLg/Bleeding"}
    saDamageLookups[11] = {gciDamageType_Poisoning,  "Root/Images/AdventureUI/DamageTypeIconsLg/Poisoning"}
    saDamageLookups[12] = {gciDamageType_Corroding,  "Root/Images/AdventureUI/DamageTypeIconsLg/Corroding"}

    -- |[Damage Names Lookup]|
    local saDamageNameLookups = {}
    saDamageNameLookups[ 1] = {gciDamageType_Slashing,   "Slash"}
    saDamageNameLookups[ 2] = {gciDamageType_Piercing,   "Pierce"}
    saDamageNameLookups[ 3] = {gciDamageType_Striking,   "Strike"}
    saDamageNameLookups[ 4] = {gciDamageType_Flaming,    "Flame"}
    saDamageNameLookups[ 5] = {gciDamageType_Freezing,   "Freeze"}
    saDamageNameLookups[ 6] = {gciDamageType_Shocking,   "Shock"}
    saDamageNameLookups[ 7] = {gciDamageType_Crusading,  "Crusade"}
    saDamageNameLookups[ 8] = {gciDamageType_Obscuring,  "Obscure"}
    saDamageNameLookups[ 9] = {gciDamageType_Terrifying, "Terrify"}
    saDamageNameLookups[10] = {gciDamageType_Bleeding,   "Bleed"}
    saDamageNameLookups[11] = {gciDamageType_Poisoning,  "Poison"}
    saDamageNameLookups[12] = {gciDamageType_Corroding,  "Corrode"}
    
    -- |[ ====================== Complex Description ======================= ]|
    --Iterate across the string and build a list of images. These will need to be replaced. Duplicates are not a huge
    -- issue since these are small and temporary objects.
    local sUseString = pzStructure.sDescriptionMarkdown
    local iLen = string.len(sUseString)
    for i = 1, iLen, 1 do
        
        --Store.
        local sLetter = string.sub(sUseString, i, i)
        
        -- |[Skip]|
        --Skip this letter.
        if(iSkips > 0) then
            iSkips = iSkips - 1
        
        -- |[ ============= Simple Parts =============== ]|
        -- |[Variables]|
        --Populate with a variable, expected to be a number.
        elseif(string.sub(sUseString, i, i+5) == "[VARN:") then
            
            --Scan ahead to locate the terminating brace.
            for p = i+6, iLen, 1 do
                local sSubLetter = string.sub(sUseString, p, p)
                if(sSubLetter == "]") then
                    
                    --Get the variable name.
                    local sVarName = string.sub(sUseString, i+6, p-1)
        
                    --Now get the variable from the DataLibrary.
                    local iValue = VM_GetVar("Root/Variables/Combat/" .. piEffectID .. "/" .. sVarName, "N")
                    sFinalString = sFinalString .. iValue
                    iSkips = p-i
                    break
                end
            end
        
        -- |[Damage Type Icon]|
        --Damage type. Automatically selects the damage type from the structure.
        elseif(string.sub(sUseString, i, i+11) == "[DamageType]") then
        
            --Skip.
            iSkips = 11
        
            --Weapon damage:
            if(pzStructure.zExecutionAbiPackage.bUseWeapon == true) then
                bSearch = true
                sFinalString = sFinalString .. Translate(gsTranslationCombat, "Weapon Damage")
        
            --Specific damage types:
            else
                for p = 1, #saDamageLookups, 1 do
                    if(pzStructure.zExecutionAbiPackage.iDamageType == saDamageLookups[p][1]) then
                        iRemapsTotal = iRemapsTotal + 1
                        sFinalString = sFinalString .. "[IMG" .. iRemapsTotal-1 .. "]"
                        saRemaps[iRemapsTotal] = saDamageLookups[p][2]
                        
                        --Next. Skip the length of the tag.
                        break
                    end
                end
            end
            
        -- |[Damage Type String]|
        --Damage type, displayed as a string.
        elseif(string.sub(sUseString, i, i+14) == "[DamTypeString]") then
        
            --Skip.
            iSkips = 14
        
            --Weapon damage:
            if(pzStructure.zExecutionAbiPackage.bUseWeapon == true) then
                bSearch = true
                sFinalString = sFinalString .. Translate(gsTranslationCombat, "Weapon")
        
            --Specific damage types:
            else
                for p = 1, #saDamageNameLookups, 1 do
                    if(pzStructure.zExecutionAbiPackage.iDamageType == saDamageNameLookups[p][1]) then
                        sFinalString = sFinalString .. saDamageNameLookups[p][2]
                        
                        --Next. Skip the length of the tag.
                        break
                    end
                end
            end
        
        -- |[Damage Factor]|
        --Percentage of attack power.
        elseif(string.sub(sUseString, i, i+10) == "[DamFactor]") then
            iSkips = 10
            local fPower = pzStructure.zExecutionAbiPackage.fDamageFactor
            local sPowerString = string.format("%.2f", fPower)
            sFinalString = sFinalString .. sPowerString
        
        -- |[Hit Rate]|
        --Base chance to hit.
        elseif(string.sub(sUseString, i, i+8) == "[HitRate]") then
            iSkips = 8
            local iHitRate = 100 - pzStructure.zExecutionAbiPackage.iMissThreshold
            sFinalString = sFinalString .. iHitRate
        
        -- |[CP Generation]|
        elseif(string.sub(sUseString, i, i+6) == "[CPGen]") then
            iSkips = 6
            sFinalString = sFinalString .. pzStructure.iCPGain
        
        -- |[CP Cost]|
        elseif(string.sub(sUseString, i, i+7) == "[CPCost]") then
            iSkips = 7
            sFinalString = sFinalString .. pzStructure.iRequiredCP
        
        -- |[MP Cost]|
        elseif(string.sub(sUseString, i, i+7) == "[MPCost]") then
            iSkips = 7
            sFinalString = sFinalString .. pzStructure.iRequiredMP
    
        -- |[ =========== Advanced Macros ============== ]|
        -- |[Inflict]|
        --Most abilities use the pattern "Inflict [DamFactor]x [Atk] as [DamType]". This standardizes that string.
        elseif(string.sub(sUseString, i, i+8) == "[Inflict]") then
            iSkips = 8
            
            --Lead-in.
            sFinalString = sFinalString .. Translate(gsTranslationCombat, "Inflicts [")
            
            --Damage factor.
            local fPower = pzStructure.zExecutionAbiPackage.fDamageFactor
            local sPowerString = string.format("%.2f", fPower)
            sFinalString = sFinalString .. sPowerString
            
            --Multiplier symbol.
            sFinalString = sFinalString .. "x "
            
            --Attack Symbol.
            iRemapsTotal = iRemapsTotal + 1
            sFinalString = sFinalString .. "[IMG" .. iRemapsTotal-1 .. "]"
            saRemaps[iRemapsTotal] = "Root/Images/AdventureUI/DamageTypeIconsLg/Attack"
            
            --As.
            sFinalString = sFinalString .. Translate(gsTranslationCombat, "] as ")
            
            --Damage type icon.
            if(pzStructure.zExecutionAbiPackage.bUseWeapon == true) then
                bSearch = true
                sFinalString = sFinalString .. Translate(gsTranslationCombat, "Weapon Damage")
        
            --Specific damage types:
            else
                for p = 1, #saDamageLookups, 1 do
                    if(pzStructure.zExecutionAbiPackage.iDamageType == saDamageLookups[p][1]) then
                        iRemapsTotal = iRemapsTotal + 1
                        sFinalString = sFinalString .. "[IMG" .. iRemapsTotal-1 .. "]"
                        saRemaps[iRemapsTotal] = saDamageLookups[p][2]
                        
                        --Next. Skip the length of the tag.
                        break
                    end
                end
            end
            
            --Stun. Optional.
            if(pzStructure.zExecutionAbiPackage.iBaseStunDamage > 0) then
                sFinalString = sFinalString .. "  +" .. pzStructure.zExecutionAbiPackage.iBaseStunDamage
                iRemapsTotal = iRemapsTotal + 1
                sFinalString = sFinalString .. "[IMG" .. iRemapsTotal-1 .. "]"
                saRemaps[iRemapsTotal] = "Root/Images/AdventureUI/DamageTypeIconsLg/Stun"
            end
        
        -- |[Restore]|
        --Used for healing abilities, lists out the healing and its factor.
        elseif(string.sub(sUseString, i, i+8) == "[Restore]") then
            iSkips = 8
            
            --Lead-in.
            sFinalString = sFinalString .. Translate(gsTranslationCombat, "Restores [")
            
            --Damage factor.
            local fPower = pzStructure.zExecutionAbiPackage.fHealingFactor
            local sPowerString = string.format("%.2f", fPower)
            sFinalString = sFinalString .. sPowerString
            
            --Multiplier symbol.
            sFinalString = sFinalString .. "x "
            
            --Attack Symbol.
            iRemapsTotal = iRemapsTotal + 1
            sFinalString = sFinalString .. "[IMG" .. iRemapsTotal-1 .. "]"
            saRemaps[iRemapsTotal] = "Root/Images/AdventureUI/DamageTypeIconsLg/Attack"
            
            --As.
            sFinalString = sFinalString .. "]"
            
            --Base healing, if present:
            if(pzStructure.zExecutionAbiPackage.iHealingBase > 0) then
                sFinalString = sFinalString .. " + " ..  pzStructure.zExecutionAbiPackage.iHealingBase .. " "
            end
            
            --Health Symbol.
            iRemapsTotal = iRemapsTotal + 1
            sFinalString = sFinalString .. "[IMG" .. iRemapsTotal-1 .. "]"
            saRemaps[iRemapsTotal] = "Root/Images/AdventureUI/DamageTypeIconsLg/Health"
        
        -- |[DoT Properties]|
        --Standardized damage-over-time properties.
        elseif(string.sub(sUseString, i, i+4) == "[DoT]") then
            iSkips = 4
        
            --Verify that there is a DoT. We use the prediction package since the effect package doesn't actually
            -- store the needed coefficients.
            if(pzStructure.zPredictionPackage.zaEffectModules[1] ~= nil) then
            
                --HEffect tag.
                iRemapsTotal = iRemapsTotal + 1
                sFinalString = sFinalString .. "[IMG" .. iRemapsTotal-1 .. "]"
                saRemaps[iRemapsTotal] = "Root/Images/AdventureUI/DamageTypeIconsLg/EffectHostile"
            
                --Strength tag.
                local iUseStrength = pzStructure.zPredictionPackage.zaEffectModules[1].iEffectStrength
                if(iUseStrength > 9) then iUseStrength = 9 end
                iRemapsTotal = iRemapsTotal + 1
                sFinalString = sFinalString .. "[IMG" .. iRemapsTotal-1 .. "]"
                saRemaps[iRemapsTotal] = "Root/Images/AdventureUI/DamageTypeIconsLg/Str" .. iUseStrength
                
                --Effect type.
                for p = 1, #saDamageLookups, 1 do
                    if(pzStructure.zPredictionPackage.zaEffectModules[1].iEffectType == saDamageLookups[p][1]) then
                        iRemapsTotal = iRemapsTotal + 1
                        sFinalString = sFinalString .. "[IMG" .. iRemapsTotal-1 .. "]"
                        saRemaps[iRemapsTotal] = saDamageLookups[p][2]
                        
                        --Next. Skip the length of the tag.
                        break
                    end
                end
            
                --Text.
                sFinalString = sFinalString .. " DoT of ["
            
                --Attack power factor.
                local iDuration = pzStructure.zPredictionPackage.zaEffectModules[1].iDotDuration
                local fPower = (pzStructure.zPredictionPackage.zaEffectModules[1].fDotAttackFactor * iDuration)
                local sPowerString = string.format("%.2f", fPower)
                sFinalString = sFinalString .. sPowerString
                
                --Multiplier symbol.
                sFinalString = sFinalString .. "x "
            
                --Attack Symbol.
                iRemapsTotal = iRemapsTotal + 1
                sFinalString = sFinalString .. "[IMG" .. iRemapsTotal-1 .. "]"
                saRemaps[iRemapsTotal] = "Root/Images/AdventureUI/DamageTypeIconsLg/Attack"
            
                --Space
                sFinalString = sFinalString .. "] "
            
                --Damage type.
                for p = 1, #saDamageLookups, 1 do
                    if(pzStructure.zPredictionPackage.zaEffectModules[1].iDoTDamageType == saDamageLookups[p][1]) then
                        iRemapsTotal = iRemapsTotal + 1
                        sFinalString = sFinalString .. "[IMG" .. iRemapsTotal-1 .. "]"
                        saRemaps[iRemapsTotal] = saDamageLookups[p][2]
                        
                        --Next. Skip the length of the tag.
                        break
                    end
                end
            
                --Duration.
                sFinalString = sFinalString .. Translate(gsTranslationCombat, " over ") .. iDuration
                
                --Turns icon.
                iRemapsTotal = iRemapsTotal + 1
                sFinalString = sFinalString .. "[IMG" .. iRemapsTotal-1 .. "]"
                saRemaps[iRemapsTotal] = "Root/Images/AdventureUI/DamageTypeIconsLg/Clock"
            
            end
        
        -- |[Target String]|
        --Target. Selects target type from the structure.
        elseif(string.sub(sUseString, i, i+7) == "[Target]") then
        
            --Skip.
            iSkips = 7
        
            --Translate.
            if(pzStructure.sTargetMacro == "Target Self") then
                sFinalString = sFinalString .. Translate(gsTranslationCombat, "Self")
            
            elseif(pzStructure.sTargetMacro == "Target Enemies Single") then
                sFinalString = sFinalString .. Translate(gsTranslationCombat, "Single Enemy")
                
            elseif(pzStructure.sTargetMacro == "Target Enemies All") then
                sFinalString = sFinalString .. Translate(gsTranslationCombat, "All Enemies")
                
            elseif(pzStructure.sTargetMacro == "Target Allies Single") then
                sFinalString = sFinalString .. Translate(gsTranslationCombat, "Single Ally")
                
            elseif(pzStructure.sTargetMacro == "Target Allies Single Option Downed") then
                sFinalString = sFinalString .. Translate(gsTranslationCombat, "Single Ally (May be Downed)")
                
            elseif(pzStructure.sTargetMacro == "Target Allies Single Not Self") then
                sFinalString = sFinalString .. Translate(gsTranslationCombat, "Single Ally (Not User)")
                
            elseif(pzStructure.sTargetMacro == "Target Allies Single Downed Only") then
                sFinalString = sFinalString .. Translate(gsTranslationCombat, "Single Downed Ally")
                
            elseif(pzStructure.sTargetMacro == "Target Allies All") then
                sFinalString = sFinalString .. Translate(gsTranslationCombat, "All Allies")
                
            elseif(pzStructure.sTargetMacro == "Target All Single") then
                sFinalString = sFinalString .. Translate(gsTranslationCombat, "Single Ally or Enemy")
                
            elseif(pzStructure.sTargetMacro == "Target Parties") then
                sFinalString = sFinalString .. Translate(gsTranslationCombat, "Ally Party or Enemy Party")
                
            elseif(pzStructure.sTargetMacro == "Target All") then
                sFinalString = sFinalString .. Translate(gsTranslationCombat, "All Allies and Enemies")
        
            else
                sFinalString = sFinalString .. Translate(gsTranslationCombat, "Unhandled Target String")
            end
        
        -- |[BaseHit]|
        --Same as [Hit Rate] but displays "Base Hit Rate: " as well.
        elseif(string.sub(sUseString, i, i+8) == "[BaseHit]") then
            iSkips = 8
            local iHitRate = 100 - pzStructure.zExecutionAbiPackage.iMissThreshold
            sFinalString = sFinalString .. Translate(gsTranslationCombat, "Base Hit Rate: ") .. iHitRate .. "%%"
        
        -- |[Costs]|
        --Shows all costs. Usually goes on the final line.
        elseif(string.sub(sUseString, i, i+6) == "[Costs]") then
            iSkips = 6
            
            --First, check if the costs are all zero. If so, print this.
            if(pzStructure.iRequiredMP < 1 and pzStructure.iRequiredCP < 1) then
                sFinalString = sFinalString .. Translate(gsTranslationCombat, "Lose: Nothing")
            
            --Otherwise, render costs.
            else
                sFinalString = sFinalString .. Translate(gsTranslationCombat, "Lose: ")
                
                --Mana:
                if(pzStructure.iRequiredMP > 0) then
                    sFinalString = sFinalString .. "-" .. pzStructure.iRequiredMP
                    iRemapsTotal = iRemapsTotal + 1
                    sFinalString = sFinalString .. "[IMG" .. iRemapsTotal-1 .. "]"
                    saRemaps[iRemapsTotal] = "Root/Images/AdventureUI/DamageTypeIconsLg/Mana"
                end
                
                --Combo Points:
                if(pzStructure.iRequiredCP > 0) then
                    sFinalString = sFinalString .. "-" .. pzStructure.iRequiredCP
                    iRemapsTotal = iRemapsTotal + 1
                    sFinalString = sFinalString .. "[IMG" .. iRemapsTotal-1 .. "]"
                    saRemaps[iRemapsTotal] = "Root/Images/AdventureUI/DamageTypeIconsLg/CmbPnt"
                end
            end
            
            --Gains.
            sFinalString = sFinalString .. "[MOV200]"
            
            --If the gains are all zero, print this.
            if(pzStructure.iCPGain < 1) then
                sFinalString = sFinalString .. Translate(gsTranslationCombat, "Gain: Nothing")
            
            --Otherwise, render gains.
            else
                sFinalString = sFinalString .. Translate(gsTranslationCombat, "Gain: ")
                
                --Combo Points:
                if(pzStructure.iCPGain > 0) then
                    sFinalString = sFinalString .. "+" .. pzStructure.iCPGain
                    iRemapsTotal = iRemapsTotal + 1
                    sFinalString = sFinalString .. "[IMG" .. iRemapsTotal-1 .. "]"
                    saRemaps[iRemapsTotal] = "Root/Images/AdventureUI/DamageTypeIconsLg/CmbPnt"
                end
            end
        
        -- |[Uses]|
        --Shows how many times the item can be used or it's cooldown. Typically used for combat items.
        elseif(string.sub(sUseString, i, i+5) == "[Uses]") then
            iSkips = 5
        
            --Check for a cooldown. If it's 1 or 0, don't list it.
            if(pzStructure.iCooldown > 1) then
                
                --Number.
                sFinalString = sFinalString .. (pzStructure.iCooldown-1)
                
                --Symbol.
                iRemapsTotal = iRemapsTotal + 1
                sFinalString = sFinalString .. "[IMG" .. iRemapsTotal-1 .. "]"
                saRemaps[iRemapsTotal] = "Root/Images/AdventureUI/DamageTypeIconsLg/Clock"
            
                --Cooldown text.
                sFinalString = sFinalString .. Translate(gsTranslationCombat, " cooldown. ")
            end
            
            --No charge limit.
            if(pzStructure.iChargesMax == nil or pzStructure.iChargesMax < 1) then
                sFinalString = sFinalString .. Translate(gsTranslationCombat, "No charge limit.")
            
            --Has a limit. Exactly one, no plural.
            elseif(pzStructure.iChargesMax == 1) then
                sFinalString = sFinalString .. Translate(gsTranslationCombat, "1 use per battle.")
            
            --More than one charge.
            else
                sFinalString = sFinalString .. pzStructure.iChargesMax .. Translate(gsTranslationCombat, " uses per battle.")
            end

        -- |[ ============= All Stat Remaps ============ ]|
        --Check for a stat remap.
        elseif(sLetter == "[") then
            
            --Iterate across the stat icons and look for a match.
            local bMatch = false
            for p = 1, giStatIconsTotal, 1 do
                local iRemapLen = string.len(gsaStatIconsLg[p][1]) - 1
                local sSubString = string.sub(sUseString, i, i+iRemapLen)
                if(sSubString == gsaStatIconsLg[p][1]) then
                    iRemapsTotal = iRemapsTotal + 1
                    sFinalString = sFinalString .. "[IMG" .. iRemapsTotal-1 .. "]"
                    saRemaps[iRemapsTotal] = gsaStatIconsLg[p][2]
                    
                    --Next. Skip the length of the tag.
                    iSkips = iRemapLen
                    bMatch = true
                    break
                end
            end
            
            --No match. Append the brace.
            if(bMatch == false) then
                sFinalString = sFinalString .. sLetter
            end
        -- |[ ========== No Special Cases ============== ]|
        --Copy the letter.
        else
            sFinalString = sFinalString .. sLetter
        end
    end

    -- |[Finish Up]|
    --Populate the structure's description information with the new data.
    pzStructure.sDescription = sFinalString
    pzStructure.saImages = saRemaps
    
    -- |[ ===================== Simplified Description ===================== ]|
    --Same as above, but handles the simplified description. The same markdown and patterns are used.
    -- If no simplified string is provided, the complex one will be used.
    if(pzStructure.sSimpleDescMarkdown == nil) then return end
    
    --Reset variables.
    iRemapsTotal = 0
    saRemaps = {}
    sFinalString = ""
    
    --Iterate.
    sUseString = pzStructure.sSimpleDescMarkdown
    iLen = string.len(sUseString)
    for i = 1, iLen, 1 do
        
        --Store.
        local sLetter = string.sub(sUseString, i, i)
        
        -- |[Skip]|
        --Skip this letter.
        if(iSkips > 0) then
            iSkips = iSkips - 1
        
        -- |[Variables]|
        --Populate with a variable, expected to be a number.
        elseif(string.sub(sUseString, i, i+5) == "[VARN:") then
            
            --Scan ahead to locate the terminating brace.
            for p = i+6, iLen, 1 do
                local sSubLetter = string.sub(sUseString, p, p)
                if(sSubLetter == "]") then
                    
                    --Get the variable name.
                    local sVarName = string.sub(sUseString, i+6, p-1)
        
                    --Now get the variable from the DataLibrary.
                    local iValue = VM_GetVar("Root/Variables/Combat/" .. piEffectID .. "/" .. sVarName, "N")
                    sFinalString = sFinalString .. iValue
                    iSkips = p-i
                    break
                end
            end
        
        -- |[Damage Type Icon]|
        --Damage type. Automatically selects the damage type from the structure.
        elseif(string.sub(sUseString, i, i+11) == "[DamageType]") then
        
            --Skip.
            iSkips = 11
        
            --Weapon damage:
            if(pzStructure.zExecutionAbiPackage.bUseWeapon == true) then
                bSearch = true
                sFinalString = sFinalString .. Translate(gsTranslationCombat, "Weapon Damage")
        
            --Specific damage types:
            else
                for p = 1, #saDamageLookups, 1 do
                    if(pzStructure.zExecutionAbiPackage.iDamageType == saDamageLookups[p][1]) then
                        iRemapsTotal = iRemapsTotal + 1
                        sFinalString = sFinalString .. "[IMG" .. iRemapsTotal-1 .. "]"
                        saRemaps[iRemapsTotal] = saDamageLookups[p][2]
                        
                        --Next. Skip the length of the tag.
                        break
                    end
                end
            end
            
        -- |[Damage Type String]|
        --Damage type, displayed as a string.
        elseif(string.sub(sUseString, i, i+14) == "[DamTypeString]") then
        
            --Skip.
            iSkips = 14
        
            --Weapon damage:
            if(pzStructure.zExecutionAbiPackage.bUseWeapon == true) then
                bSearch = true
                sFinalString = sFinalString .. Translate(gsTranslationCombat, "Weapon")
        
            --Specific damage types:
            else
                for p = 1, #saDamageLookups, 1 do
                    if(pzStructure.zExecutionAbiPackage.iDamageType == saDamageNameLookups[p][1]) then
                        sFinalString = sFinalString .. saDamageNameLookups[p][2]
                        
                        --Next. Skip the length of the tag.
                        break
                    end
                end
            end
        
        -- |[Damage Factor]|
        --Percentage of attack power.
        elseif(string.sub(sUseString, i, i+10) == "[DamFactor]") then
            iSkips = 10
            local fPower = pzStructure.zPredictionPackage.zDamageModule.fDamageFactor
            local sPowerString = string.format("%.2f", fPower)
            sFinalString = sFinalString .. sPowerString
        
        -- |[Hit Rate]|
        --Base chance to hit.
        elseif(string.sub(sUseString, i, i+8) == "[HitRate]") then
            iSkips = 8
            local iHitRate = 100 - pzStructure.zExecutionAbiPackage.iMissThreshold
            sFinalString = sFinalString .. iHitRate
        
        -- |[CP Generation]|
        elseif(string.sub(sUseString, i, i+6) == "[CPGen]") then
            iSkips = 6
            sFinalString = sFinalString .. pzStructure.iCPGain
        
        -- |[CP Cost]|
        elseif(string.sub(sUseString, i, i+7) == "[CPCost]") then
            iSkips = 7
            sFinalString = sFinalString .. pzStructure.iRequiredCP
        
        -- |[MP Cost]|
        elseif(string.sub(sUseString, i, i+7) == "[MPCost]") then
            iSkips = 7
            sFinalString = sFinalString .. pzStructure.iRequiredMP
        
        -- |[Standard Stat Remaps]|
        --Check for a stat remap.
        elseif(sLetter == "[") then
            
            --Iterate across the stat icons and look for a match.
            local bMatch = false
            for p = 1, giStatIconsTotal, 1 do
                local iRemapLen = string.len(gsaStatIconsLg[p][1]) - 1
                local sSubString = string.sub(sUseString, i, i+iRemapLen)
                if(sSubString == gsaStatIconsLg[p][1]) then
                    iRemapsTotal = iRemapsTotal + 1
                    sFinalString = sFinalString .. "[IMG" .. iRemapsTotal-1 .. "]"
                    saRemaps[iRemapsTotal] = gsaStatIconsLg[p][2]
                    
                    --Next. Skip the length of the tag.
                    iSkips = iRemapLen
                    bMatch = true
                    break
                end
            end
            
            --No match. Append the brace.
            if(bMatch == false) then
                sFinalString = sFinalString .. sLetter
            end
    
        -- |[No Special Cases]|
        --Copy the letter.
        else
            sFinalString = sFinalString .. sLetter
        end
    end

    -- |[Finish Up]|
    --Populate the structure's description information with the new data.
    pzStructure.sSimpleDesc = sFinalString
    pzStructure.saSimpleImages = saRemaps
end
