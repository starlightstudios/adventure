-- |[ =================================== fnApplyWeaponTypeOverrides() =================================== ]|
--Given an ID, figures out if their weapon type was overridden. If so, changes the attack animation and
-- sound to match, and returns them. Otherwise, returns the originals.
function fnApplyWeaponTypeOverrides(piEntityID, psAttackAnim, psAttackSound, psCriticalSound)
    
    --Setup.
    local iOverrideSlot = -1
    local fHighestOverride = 0.0
    
    --Determine the highest damage factor. We only care about overrides.
    AdvCombat_SetProperty("Push Entity By ID", piEntityID)
        
        --Make sure there's a weapon slot.
        local sWeaponSlot = AdvCombatEntity_GetProperty("Slot For Weapon Damage")
        if(sWeaponSlot == "Null") then
            DL_PopActiveObject()
            return psAttackAnim, psAttackSound, psCriticalSound
        end
    
        --See if the slot has something in it.
        local sEquipmentName = AdvCombatEntity_GetProperty("Equipment In Slot S", sWeaponSlot)
        if(sEquipmentName == "Null") then
            DL_PopActiveObject()
            return psAttackAnim, psAttackSound, psCriticalSound
        end
    
        --Push the weapon.
        AdvCombatEntity_SetProperty("Push Item In Slot S", sWeaponSlot)
        
            --Override storage.
            for i = 0, gciDamageType_Total-1, 1 do
                local fFactor = AdItem_GetProperty("Damage Type", gciEquipment_DamageType_Override, i+gciDamageOffsetToResistances)
                if(fFactor > fHighestOverride) then
                    iOverrideSlot = i
                    fHighestOverride = fFactor
                end
            end
        DL_PopActiveObject()
    DL_PopActiveObject()
    
    --If the override slot is not -1, we need to change the type:
    if(iOverrideSlot > -1) then
    
        --Translate.
        local sPackSlot = "Null"
        if(iOverrideSlot == gciDamageType_Slashing) then sPackSlot = "Sword Slash" end
        if(iOverrideSlot == gciDamageType_Striking) then sPackSlot = "Strike" end
        if(iOverrideSlot == gciDamageType_Piercing) then sPackSlot = "Pierce" end
        if(iOverrideSlot == gciDamageType_Flaming) then sPackSlot = "Flames" end
        if(iOverrideSlot == gciDamageType_Freezing) then sPackSlot = "Freeze" end
        if(iOverrideSlot == gciDamageType_Shocking) then sPackSlot = "Shock" end
        if(iOverrideSlot == gciDamageType_Crusading) then sPackSlot = "Light" end
        if(iOverrideSlot == gciDamageType_Obscuring) then sPackSlot = "Shadow A" end
        if(iOverrideSlot == gciDamageType_Bleeding) then sPackSlot = "Bleed" end
        if(iOverrideSlot == gciDamageType_Poisoning) then sPackSlot = "Poison" end
        if(iOverrideSlot == gciDamageType_Corroding) then sPackSlot = "Corrode" end
    
        for i = 1, #gzaAbiPacks, 1 do
            if(sPackSlot == gzaAbiPacks[i].sLookup) then
                psAttackAnim = gzaAbiPacks[i].sAttackAnim
                psAttackSound = gzaAbiPacks[i].sAttackSound
                psCriticalSound = gzaAbiPacks[i].sCriticalSound
                break
            end
        end
    end
    
    --Pass them back.
    return psAttackAnim, psAttackSound, psCriticalSound
end

-- |[ ============================= fnStandardWeaponAnimResolve() ============================== ]|
--Standard handler. Given an entity ID, figures out their animation and sounds, and passes them back.
function fnStandardWeaponAnimResolve(piEntityID)
    
    --Get values from the entity.
    AdvCombat_SetProperty("Push Entity By ID", piEntityID)
        local sAttackAnimation = AdvCombatEntity_GetProperty("Weapon Attack Animation")
        local sAttackSound     = AdvCombatEntity_GetProperty("Weapon Attack Sound")
        local sCriticalSound   = AdvCombatEntity_GetProperty("Weapon Critical Sound")
    DL_PopActiveObject()
    
    --Switch the weapon values to use defaults if any were in error. This can occur if the equipped
    -- weapon was configured incorrectly or did not exist.
    if(sAttackAnimation == "Null") then sAttackAnimation = gsDefaultAttackAnimation end
    if(sAttackSound     == "Null") then sAttackSound     = gsDefaultAttackSound     end
    if(sCriticalSound   == "Null") then sCriticalSound   = gsDefaultCriticalSound   end
    
    --Get overrides.
    sAttackAnimation, sAttackSound, sCriticalSound = fnApplyWeaponTypeOverrides(piEntityID, sAttackAnimation, sAttackSound, sCriticalSound)
    return sAttackAnimation, sAttackSound, sCriticalSound
end

