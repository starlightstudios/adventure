-- |[ ============================== fnDefaultEndOfApplications() ============================== ]|
--Finishes a set of applications, sending the final event timer to the C++ and range-checking it as needed.
function fnDefaultEndOfApplications(piTimer)
    
    -- |[Argument Check]|
    if(piTimer == nil) then piTimer = 1 end
    
    -- |[Padding]|
    --Add padding.
    piTimer = piTimer + gciApplication_EventPaddingTicks
    
    -- |[Range Checks]|
    --Clamp. There is a minimum number of ticks every Event must occupy.
    if(piTimer < gciCombatTicks_StandardActionMinimum) then piTimer = gciCombatTicks_StandardActionMinimum end
    
    -- |[Upload]|
    --Send to C++ state.
    AdvCombat_SetProperty("Event Timer", piTimer)
end
