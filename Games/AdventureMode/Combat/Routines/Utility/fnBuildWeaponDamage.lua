-- |[ ================================= fnBuildWeaponDamage() ================================== ]|
--Abilities that deal weapon damage need to query the acting character's weapon and get the damage
-- types it deals. Weapons may deal more than one type of damage, and gems may change the listed
-- type of damage.
--The function will return an array of gciDamageType_Total entries, which are floating point values
-- ranging from 0.0 to 1.0. The sum (should) be at least 1.0 across all damage types, but some
-- equipment may apply bonuses or not sum to 100%.
--If an error occurs, the array populates 100% slashing damage. This can occur if a character has
-- no weapon damage type slot set, or if the slot is unequipped.
--The Active Object should be the entity whose weapon we are querying.
function fnBuildWeaponDamage()
    
    -- |[Setup]|
    --Build a factor array.
    local fSum = 0.0
    local faDamageFactors = {}
    for i = 0, gciDamageType_Total-1, 1 do
        faDamageFactors[i] = 0.0
    end
    
    --Default slashing to 100% damage.
    faDamageFactors[gciDamageType_Slashing] = 1.0
    
    -- |[Check for Weapon Slot]|
    --Get which slot is used for weapon damage.
    local sWeaponSlot = AdvCombatEntity_GetProperty("Slot For Weapon Damage")
    if(sWeaponSlot == "Null") then
        return faDamageFactors
    end
    
    --See if the slot has something in it.
    local sEquipmentName = AdvCombatEntity_GetProperty("Equipment In Slot S", sWeaponSlot)
    if(sEquipmentName == "Null") then
        return faDamageFactors
    end
    
    -- |[Weapon Queries]|
    --If we got this far, we have a weapon slot active and it has something in it. Next, push it and query it.
    AdvCombatEntity_SetProperty("Push Item In Slot S", sWeaponSlot)
    
        -- |[Scan Overrides]|
        --Check the override damages. If the sum is nonzero, we're using the overrides.
        local fOverrideSum = 0.0
        local faOverrideFactors = {}
        for i = 0, gciDamageType_Total-1, 1 do
            faOverrideFactors[i] = AdItem_GetProperty("Damage Type", gciEquipment_DamageType_Override, i+gciDamageOffsetToResistances)
            fOverrideSum = fOverrideSum + faOverrideFactors[i]
        end
        
        -- |[Overrides Accepted]|
        --Override was found, populate.
        if(fOverrideSum > 0.0) then
            fSum = fOverrideSum
            for i = 0, gciDamageType_Total-1, 1 do
                faDamageFactors[i] = faOverrideFactors[i]
            end
        
        -- |[Use Base Values]|
        --If the sum was zero, there is no override in place. Use the base values.
        else
            
            --Scan base values.
            local fBaseSum = 0.0
            local faBaseFactors = {}
            for i = 0, gciDamageType_Total-1, 1 do
                faBaseFactors[i] = AdItem_GetProperty("Damage Type", gciEquipment_DamageType_Base, i+gciDamageOffsetToResistances)
                fBaseSum = fBaseSum + faBaseFactors[i]
            end
            
            --If the sum is still zero, the weapon was configured incorrectly. Return the default.
            if(fBaseSum <= 0.0) then
                DL_PopActiveObject()
                return faDamageFactors
            end
            
            --Otherwise, populate.
            fSum = fBaseSum
            for i = 0, gciDamageType_Total-1, 1 do
                faDamageFactors[i] = faBaseFactors[i]
            end
        end
        
        -- |[Renormalize]|
        --If the sum of the array is not 1.0, then renormalize the array to 1.0. This is used to make sure gems changing damage type
        -- all work together in harmony.
        if(fSum ~= 1.0) then
        
            --For safety, check for a zero-sum. Shouldn't be logically possible.
            if(fSum <= 0.0) then
                DL_PopActiveObject()
                return faDamageFactors
            end
            
            --Divide all values by the total value.
            for i = 0, gciDamageType_Total-1, 1 do
                faDamageFactors[i] = faDamageFactors[i] / fSum
            end

        end
    
        -- |[Add Bonuses]|
        --Gems and weapons may provide a bonus. This is tacked on *after* override/base is renormalized, and can therefore
        -- allow a weapon to do more than 100% damage.
        for i = 0, gciDamageType_Total-1, 1 do
            faDamageFactors[i] = faDamageFactors[i] + AdItem_GetProperty("Damage Type", gciEquipment_DamageType_Bonus, i+gciDamageOffsetToResistances)
        end
    
    --Clean.
    DL_PopActiveObject()
    
    --Return the final array.
    return faDamageFactors
end

-- |[ =============================== fnBuildWeaponDamageIcons() =============================== ]|
--Builds a list of icons of the weapon damage, ordered from most to least important.
function fnBuildWeaponDamageIcons(piEntityID)
    
    --Get weapon damage listing.
    if(piEntityID == nil) then return {} end
    AdvCombat_SetProperty("Push Entity By ID", piEntityID)
        local faDamageFactors = fnBuildWeaponDamage()
    DL_PopActiveObject()

    --Build a table of key-value pairs.
    local zaImgTable = {}
    zaImgTable[gciDamageType_Slashing]   = {faDamageFactors[gciDamageType_Slashing],   "Root/Images/AdventureUI/DamageTypeIcons/Slashing"}
    zaImgTable[gciDamageType_Piercing]   = {faDamageFactors[gciDamageType_Piercing],   "Root/Images/AdventureUI/DamageTypeIcons/Piercing"}
    zaImgTable[gciDamageType_Striking]   = {faDamageFactors[gciDamageType_Striking],   "Root/Images/AdventureUI/DamageTypeIcons/Striking"}
    zaImgTable[gciDamageType_Flaming]    = {faDamageFactors[gciDamageType_Flaming],    "Root/Images/AdventureUI/DamageTypeIcons/Flaming"}
    zaImgTable[gciDamageType_Freezing]   = {faDamageFactors[gciDamageType_Freezing],   "Root/Images/AdventureUI/DamageTypeIcons/Freezing"}
    zaImgTable[gciDamageType_Shocking]   = {faDamageFactors[gciDamageType_Shocking],   "Root/Images/AdventureUI/DamageTypeIcons/Shocking"}
    zaImgTable[gciDamageType_Crusading]  = {faDamageFactors[gciDamageType_Crusading],  "Root/Images/AdventureUI/DamageTypeIcons/Crusading"}
    zaImgTable[gciDamageType_Obscuring]  = {faDamageFactors[gciDamageType_Obscuring],  "Root/Images/AdventureUI/DamageTypeIcons/Obscuring"}
    zaImgTable[gciDamageType_Bleeding]   = {faDamageFactors[gciDamageType_Bleeding],   "Root/Images/AdventureUI/DamageTypeIcons/Bleeding"}
    zaImgTable[gciDamageType_Poisoning]  = {faDamageFactors[gciDamageType_Poisoning],  "Root/Images/AdventureUI/DamageTypeIcons/Poisoning"}
    zaImgTable[gciDamageType_Corroding]  = {faDamageFactors[gciDamageType_Corroding],  "Root/Images/AdventureUI/DamageTypeIcons/Corroding"}
    zaImgTable[gciDamageType_Terrifying] = {faDamageFactors[gciDamageType_Terrifying], "Root/Images/AdventureUI/DamageTypeIcons/Terrifying"}
    
    --Sort the table from highest to lowest by running across it and placing the highest nonzero value in another array.
    local saFinalTable = {}
    while(true) do
        local iHighestSlot = -1
        local fHighest = 0.0
        for i = 0, gciDamageType_Total-1, 1 do
            if(zaImgTable[i][1] > fHighest) then
                iHighestSlot = i
                fHighest = zaImgTable[i][1]
            end
        end
        
        --Highest is -1, we're done.
        if(iHighestSlot == -1) then break end
        
        --Zero the slot, put it in the final table.
        local iLen = #saFinalTable + 1
        saFinalTable[iLen] = zaImgTable[iHighestSlot][2]
        zaImgTable[iHighestSlot][1] = -1.0
    end
    
    --Pass the final table back.
    return saFinalTable
end
