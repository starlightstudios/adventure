-- |[ ======================================== CP Check ======================================== ]|
--Function that checks the owner's CP. Meant to be called during gciAbility_BeginAction/gciAbility_BeginFreeAction
-- to determine if the entity has enough CP to use an ability.
--An AdvCombatAbility should be atop the activity stack.
--Returns true if the entity has enough CP, false if not.
function fnCheckCP(piCPRequired)
    
    --Arg check.
    if(piCPRequired == nil) then return true end
    
    --Get variables.
    AdvCombatAbility_SetProperty("Push Owner")
        local iCPCur = AdvCombatEntity_GetProperty("Combo Points")
    DL_PopActiveObject()

    --If we pass the CP requirements, flag as usable.
    if(iCPCur >= piCPRequired) then
        return true
    end
    return false
end

--Modifies the amount of CP an entity has, automatically clamping it against maximums.
--An AdvCombatEntity should be atop the activity stack.
function fnModifyCP(piAmount)
    
    --Arg check.
    if(piAmount == nil) then return end
    
    --Get values.
    local iCPCur = AdvCombatEntity_GetProperty("Combo Points")
    local iCPMin = 0
    local iCPMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_CPMax)
    
    --Modify.
    iCPCur = iCPCur + piAmount
    
    --Clamp.
    if(iCPCur < iCPMin) then iCPCur = iCPMin end
    if(iCPCur > iCPMax) then iCPCur = iCPMax end
    
    --Set.
    AdvCombatEntity_SetProperty("Combo Points", iCPCur)
end
