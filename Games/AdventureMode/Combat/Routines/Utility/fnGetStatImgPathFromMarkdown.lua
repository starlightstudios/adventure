-- |[ ============================= fnGetStatImgPathFromMarkdown() ============================= ]|
--Given a markdown on the gsaStatIcons table, such as [Acc], locates and returns the matching
-- image path ("Root/Images/AdventureUI/StatisticIcons/Accuracy" in this case).
--If no markdown is found, returns "Null".
function fnGetStatImgPathFromMarkdown(psMarkdown)
    
    -- |[Argument Check]|
    if(psMarkdown == nil) then return "Null" end
    
    -- |[Scan]|
    for i = 1, #gsaStatIcons, 1 do
        if(gsaStatIcons[i][1] == psMarkdown) then
            return gsaStatIcons[i][2]
        end
    end

    -- |[Error]|
    return "Null"
end
