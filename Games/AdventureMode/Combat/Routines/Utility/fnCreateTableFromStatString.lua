-- |[ ============================== fnCreateTableFromStatString() ============================= ]|
--Given a stat string such as "Acc|Flat|10 Atk|Flat|20", splits up the values into a table. The 
-- format of entries on the table is given below. This is meant to allow constructing more complex
-- prediction strings with images based on the compressed statstring data.
--It is legal for the table to have 0 entries.
function fnCreateTableFromStatString(psStatString)
    
    -- |[Format]|
    --zaStatTable.iModTable --Index of the stat (in gzaModTable) that is modified. For example, index 7 is Attack Power.
    --zaStatTable.sIco      --Markdown of the stat type. Example: [Atk]
    --zaStatTable.fAmount   --The amount the given stat is changed. Can be positive, negative, and/or a percent.
    --zaStatTable.sType     --Flat or Percent, indicates how fAmount is applied to the statistic.
    --zaStatTable.sBuffIco  --Icon, [DnN] or [UpN], depending on if the value is positive or negative.
    
    -- |[Argument Check]|
    if(psStatString == nil) then return {} end
    
    -- |[Run Subdivide]|
    --This splits the string into sets broken by " ". In the example above, the table saStatStrings
    -- will contain {"Acc|Flat|10", "Atk|Flat|20"}
    local saStatStrings = fnSubdivide(psStatString, " ")
    
    -- |[Iterate]|
    --Create return table.
    local zaStatTable = {}
    
    --For Each Stat:
    for i = 1, #saStatStrings, 1 do
    
        --Get the string, break it into 3 parts, and then store the amount. In the example above,
        -- the first string becomes {"Acc", "Flat", "10"}.
        local sBaseString = saStatStrings[i]
        local saSubstrings = fnSubdivide(sBaseString, "|")
    
        --Iterate across the modifiers. This is a global table that maps a markdown, such as "Acc",
        -- to a constant index in a stat table (gciStatIndex_Accuracy in this case).
        -- We also track the amount and "type", such as percentage bonus or flag bonus.
        for p = 1, giModTableTotal, 1 do
    
            --Match?
            if(saSubstrings[1] == gzaModTable[p][1]) then
    
                --Create a new entry on the stat table.
                local o = #zaStatTable + 1
                zaStatTable[o] = {}
                zaStatTable[o].iModTable = p
                zaStatTable[o].sIco = gzaModTable[p][3]
                zaStatTable[o].fAmount = tonumber(saSubstrings[3])
                zaStatTable[o].sType = saSubstrings[2]
                
                --Buff/Debuff:
                if(zaStatTable[o].fAmount < 0.00) then
                    zaStatTable[o].sBuffIco = "[DnN]"
                else
                    zaStatTable[o].sBuffIco = "[UpN]"
                end
                
                --Stop execution in this type.
                break
            end
        end
    end
    
    -- |[Finish Up]|
    return zaStatTable
end
