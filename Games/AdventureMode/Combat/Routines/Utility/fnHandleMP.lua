-- |[ ======================================== MP Check ======================================== ]|
--Function that checks the owner's MP. Meant to be called during gciAbility_BeginAction/gciAbility_BeginFreeAction
-- to determine if the entity has enough MP to use an ability.
--An AdvCombatAbility should be atop the activity stack.
--Returns true if the entity has enough MP, false if not.
function fnCheckMP(piMPRequired)
    
    --Arg check.
    if(piMPRequired == nil) then return true end
    
    --Get variables.
    AdvCombatAbility_SetProperty("Push Owner")
        local iMPCur = AdvCombatEntity_GetProperty("Magic")
    DL_PopActiveObject()

    --If we pass the MP requirements, flag as usable.
    if(iMPCur >= piMPRequired) then
        return true
    end
    return false
end

--Modifies the amount of MP an entity has, automatically clamping it against maximums.
-- Note that MP does not have a zero clamp. This is because overcasting is possible for some
-- characters. If a minimum is desired, it needs to be done manually.
--An AdvCombatEntity should be atop the activity stack.
function fnModifyMP(piAmount)
    
    --Arg check.
    if(piAmount == nil) then return end
    
    --Get values.
    local iMPCur = AdvCombatEntity_GetProperty("Magic")
    local iMPMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_MPMax)
    
    --Modify.
    iMPCur = iMPCur + piAmount
    
    --Clamp.
    if(iMPCur > iMPMax) then iMPCur = iMPMax end
    
    --Set.
    AdvCombatEntity_SetProperty("Magic", iMPCur)
end
