-- |[ ============================= Is Entity Immune To Damage Type ============================ ]|
--Returns true if the target atop the activity stack is immune to the given damage type. An entity
-- becomes immune to damage of a type if ANY of its resistance groups are 1000 or higher, not the sum!
--An AdvCombatEntity should be on top of the Activity Stack.
--The type should be one of the gciStatIndex_Resist_[X] series.
fnIsEntityImmuneToDamageType = function(piType)
    
    --Arg check.
    if(piType == nil) then return false end
    
    --Run across the stat groupings.
    for i = gciStatGroup_Base, gciStatGroup_TempEffect, 1 do
        local iResistValue = AdvCombatEntity_GetProperty("Statistic", i, piType+gciStatIndex_Resist_Start)
        if(iResistValue >= gciResistanceForImmunity) then
            return true
        end
    end
    
    --We got this far but no grouping was immune.
    return false
end
