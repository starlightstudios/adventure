-- |[ ================================ fnStatmodBuildShortText() =============================== ]|
--The short text appears above the enemy stat bar when afflicted by the effect. Doesn't appear anywhere
-- when the character is in the player's party.
--By default, this uses the existing pzaStatModStruct.saStrings table. This is because the last string
-- of the description is always the short text.
function fnStatmodBuildShortText(pzaStatModStruct)
    
    --Error check.
    if(pzaStatModStruct == nil) then return "" end
    
    --Check if there is a strings table.
    if(pzaStatModStruct.saStrings == nil) then return "" end
    if(pzaStatModStruct.saStrings[1] == nil) then return "" end
    
    --Get the last entry on the string table. This is the auto-generated stat value.
    local iTotalStrings = #pzaStatModStruct.saStrings
    local sShortText = pzaStatModStruct.saStrings[iTotalStrings]
    
    --Add the duration onto the end.
    sShortText = sShortText .. "/[Dur][Turns]"
    
    --Finish up.
    return sShortText
    
end