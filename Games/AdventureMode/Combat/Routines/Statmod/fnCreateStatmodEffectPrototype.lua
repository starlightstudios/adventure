-- |[ ============================ fnCreateStatmodEffectPrototype() ============================ ]|
--Statmod effects use the gzaStatModStruct global to communicate information between the standard
-- handlers and the calling script. This function initializes that structure.
--The resulting structure is returned.

-- |[Statmod Structure Prototype]|
--zStruct.sDisplayName                      Display name in combat inspector.
--zStruct.bIsBuff                           If true, always applies regardless of strength.
--zStruct.iDuration                         How many turns the effect lasts. -1 means it lasts until removed.
--zStruct.sFrame                            DLPath to the image used for the frame in effect display
--zStruct.sFrontImg                         DLPath to the image used for the front of the effect display
--zStruct.sBacking                          DLPath to the image used for the colored backing of the effect display
--zStruct.bRemoveOnKO                       If true, effect expires on a KO'd target. Passives often don't do this.
--zStruct.bLowerDurationIfAlreadyActed      If true, -1 duration if the target of the effect has already acted this turn.
--zStruct.bExtraDurationForOwner            If true, +1 duration if the target of the effect is the acting entity. This allows self-buffs to last the correct duration.
--zStruct.sStatString                       A string of format "Stat1|Type|Amount Stat2|Type|Amount" that is parsed to get how this effect modifies stats.
--zStruct.bDontRebuildDescription           If true, the combat inspector strings will not rebuild to include severity. Used for fixed-property effects that rely on tags.
--zStruct.saStrings                         Strings for combat inspector. [1] is the header's left side, [2] is the header's right side, all other lines are description box.
--zStruct.sShortText                        Text that appears on the enemy in combat, can include icons.
--zStruct.zaTagList                         List of tags that apply to the effect and, therefore, the target.

-- |[Function]|
function fnCreateStatmodEffectPrototype(psDisplayName, piDuration, pbIsBuff, psFrontImg, psStatString, psaDescription, pbBuildShortText)
    
    -- |[Creation]|
    --Base.
    local zaStatModStruct = {}
    
    --Error check:
    if(psDisplayName         == nil) then return zaStatModStruct end
    if(piDuration            == nil) then return zaStatModStruct end
    if(pbIsBuff              == nil) then return zaStatModStruct end
    if(psStatString          == nil) then return zaStatModStruct end
    if(psFrontImg            == nil) then return zaStatModStruct end
    --psaDescription is optional
    --pbBuildShortText is optional
    
    -- |[Display / System]|
    --Display and basic variables.
    zaStatModStruct.sDisplayName = psDisplayName
    zaStatModStruct.bIsBuff      = pbIsBuff
    zaStatModStruct.iDuration    = piDuration
    zaStatModStruct.sFrame       = gsAbility_Frame_Active
    zaStatModStruct.sFrontImg    = psFrontImg
    zaStatModStruct.bRemoveOnKO  = true
    
    --Buff/Debuff backing.
    if(pbIsBuff) then
        zaStatModStruct.sBacking = gsAbility_Backing_Buff
    else
        zaStatModStruct.sBacking = gsAbility_Backing_Debuff
    end
    
    -- |[Turn Flags]|
    --If the target of the effect has already acted this turn, decrements duration by one.
    zaStatModStruct.bLowerDurationIfAlreadyActed = false
    
    --If the target of the effect is the acting entity, increments duration by one.
    zaStatModStruct.bExtraDurationForOwner = false

    -- |[Stat Effects]|
    --This string stores the actual stat modifiers.
    zaStatModStruct.sStatString = psStatString

    -- |[Strings for Combat Inspector]|
    --The first two strings are the header left and right side.
    zaStatModStruct.bDontRebuildDescription = false
    zaStatModStruct.saStrings = {}

    -- |[Short Text]|
    --Appears above the enemy portrait in combat. Not used for friendlies, but allies can switch sides.
    zaStatModStruct.sShortText = ""
    
    -- |[Tags]|
    --Tags that apply to the target(s) when afflicted by the effect.
    zaStatModStruct.zaTagList = {}
    
    -- |[Optional Handling]|
    --If the description is provided, build that here.
    if(psaDescription ~= nil) then
        zaStatModStruct.saStrings = fnStatmodBuildInspectorStrings(zaStatModStruct, psaDescription)
    end
    
    --If this flag is provided, build the short text as well.
    if(pbBuildShortText == true) then
        zaStatModStruct.sShortText = fnStatmodBuildShortText(zaStatModStruct)
    end
    
    -- |[Finish Up]|
    return zaStatModStruct
end
