-- |[ ======================== fnStatmodBuildInspectorStringsInBattle() ======================== ]|
--Returns a set of strings to be used in the combat inspector that show the actual values of the
-- effect rather than the precomputed values. This takes into account severity, which can change
-- over the course of a battle.
function fnStatmodBuildInspectorStringsInBattle(pzaStatModStruct, psaDescriptionStrings, pfSeverity)
    
    -- |[ ============ Error Check =========== ]|
    local saStrings = {}
    if(pzaStatModStruct      == nil) then return saStrings end
    if(psaDescriptionStrings == nil) then return saStrings end
    if(pfSeverity            == nil) then return saStrings end
    
    --If this flag is set, don't rebuild.
    if(pzaStatModStruct.bDontRebuildDescription == true) then return pzaStatModStruct.saStrings end
    
    -- |[ ========= Argument Resolve ========= ]|
    --Pull these variables out of the stat mod structure.
    local sDisplayName = pzaStatModStruct.sDisplayName
    local sStatString = pzaStatModStruct.sStatString
    
    -- |[ ========== Stat Resolver =========== ]|
    --Figures out how each stat is affected and stores it in a table.
    local zaStatTable = {}
    local saStatStrings = fnSubdivide(sStatString, " ")
    
    --For Each Stat:
    for i = 1, #saStatStrings, 1 do
    
        --Get the string, break it into 3 parts, and then store the amount.
        local sBaseString = saStatStrings[i]
        local saSubstrings = fnSubdivide(sBaseString, "|")
    
        --Iterate across the modifiers.
        for p = 1, giModTableTotal, 1 do
    
            --Match?
            if(saSubstrings[1] == gzaModTable[p][1]) then
    
                --Create a new entry on the stat table.
                local o = #zaStatTable + 1
                zaStatTable[o] = {}
                zaStatTable[o].iModTable = p
                zaStatTable[o].sIco = gzaModTable[p][3]
                zaStatTable[o].fAmount = tonumber(saSubstrings[3])
                zaStatTable[o].sType = saSubstrings[2]
                
                --Buff/Debuff:
                if(zaStatTable[o].fAmount < 0.00) then
                    zaStatTable[o].sBuffIco = "[DnN]"
                else
                    zaStatTable[o].sBuffIco = "[UpN]"
                end
                
                --Stop execution in this type.
                break
            end
        end
    end
    
    -- |[ =========== Left String ============ ]|
    --Appears on the left side of the inspector. Shows the name and buff/debuff.
    if(pzaStatModStruct.bIsBuff) then
        saStrings[1] = "[Buff]" .. sDisplayName
    else
        saStrings[1] = "[Debuff]" .. sDisplayName
    end
    
    -- |[ =========== Right String =========== ]|
    --Shows which stats are affected and how, and how long the effect will last.
    saStrings[2] = ""
    for i = 1, #zaStatTable, 1 do
        saStrings[2] = saStrings[2] .. zaStatTable[i].sBuffIco .. zaStatTable[i].sIco
    end
    
    --Append the duration onto the end. If the duration is -1, don't bother.
    if(pzaStatModStruct.iDuration > -1) then
        saStrings[2] = saStrings[2] .. " /[Dur][Turns]"
    end

    -- |[ =========== Description ============ ]|
    -- |[Base]|
    --These are description strings. First, append whatever the base script calls for.
    local i = 3
    for p = 1, #psaDescriptionStrings, 1 do
        saStrings[i] = string.gsub(psaDescriptionStrings[p], "%[PCT%]", "%%%%")
        i = i + 1
    end
    
    -- |[Padding]|
    --Get what our string count is. Pad until we're on the last line.
    for p = i, gciMaxEffectDescriptionLines, 1 do
        saStrings[i] = ""
        i = i + 1
    end
    
    -- |[Block Autogeneration]|
    --If there are no extra lines available, don't place the autogenerated lines.
    if(#psaDescriptionStrings >= gciMaxEffectDescriptionLines) then
        saStrings[gciMaxEffectDescriptionLines+3] = nil
        return saStrings
    end
    
    -- |[Effect Autogeneration]|
    --Next, add the auto-generated values. First, if it's a buff or debuff.
    saStrings[i] = ""
    if(pzaStatModStruct.bIsBuff) then
        saStrings[i] = "[Buff] "
    else
        saStrings[i] = "[Debuff] "
    end
    
    --Next, iterate across the bonuses. These are the computed values.
    for p = 1, #zaStatTable, 1 do
        
        --Get the associated value. Increase it by the severity.
        local fValue = math.floor(zaStatTable[p].fAmount)
        
        --Human-readable display value. If percent, multiply by 100.
        if(zaStatTable[p].sType == "Pct" or zaStatTable[p].sType == "PctSev") then
            fValue = math.floor(zaStatTable[p].fAmount * 100.0)
        end
        
        --Bonus from severity, if applicable.
        if(zaStatTable[p].sType == "PctSev" or zaStatTable[p].sType == "FlatSev") then
            fValue = math.floor(fValue * pfSeverity)
        end
        
        --If the value is positive, show a plus sign.
        if(zaStatTable[p].fAmount > 0.00) then
            saStrings[i] = saStrings[i] .. "+"
        end
        
        --Add the value.
        saStrings[i] = saStrings[i] .. fValue 
        
        --If this is a percentage, add a percent indicator.
        if(zaStatTable[p].sType == "Pct" or zaStatTable[p].sType == "PctSev" or zaStatTable[p].sIco == "[Threat]") then
            saStrings[i] = saStrings[i] .. "%%"
        end
    
        --Add the icon indicator.
        saStrings[i] = saStrings[i] .. " " .. zaStatTable[p].sIco
        
        --Add a space on the end.
        saStrings[i] = saStrings[i] .. " "
    end

    -- |[ ============= Finish Up ============ ]|
    --Nil last string. This indicates an endpoint for the scripts.
    saStrings[i+1] = nil
    
    --Return it.
    return saStrings
end