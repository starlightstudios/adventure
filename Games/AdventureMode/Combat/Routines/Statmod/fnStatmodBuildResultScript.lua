-- |[ ============================== fnStatmodBuildResultScript() ============================== ]|
--This appears in the prediction window, and the values sResultScript and saResultImages are built
-- from a statmod structure.
function fnStatmodBuildResultScript(pzaStatModStruct)
    
    -- |[Argument Check]|
    if(pzaStatModStruct == nil) then return "", {} end
    
    --Setup.
    local sResultScript = ""
    local saResultImages = {}
    
    --Diagnostics.
    local bDiagnostics = Debug_GetFlag("Statmod: All") or Debug_GetFlag("Statmod: Build Result Script")
    Debug_PushPrint(bDiagnostics, "fnStatmodBuildResultScript() - Begin.\n")
    Debug_Print("Base String: " .. pzaStatModStruct.sStatString .. "\n")
    
    -- |[Argument Resolve]|
    --Pull these variables out of the stat mod structure.
    local sStatString = pzaStatModStruct.sStatString
    
    -- |[Stat Resolver]|
    --Figures out how each stat is affected and stores it in a table.
    local zaStatTable = fnCreateTableFromStatString(sStatString)
    
    -- |[Assemble]|
    --Iterate across the stat table and append the results. Remember that the image count starts at 1
    -- because FEffect or HEffect is the 0th entry.
    local iTotalImageCount = 1
    for i = 1, #zaStatTable, 1 do
        
        --Flat values: Append the literal amount.
        if(zaStatTable[i].sType == "Flat" or zaStatTable[i].sType == "FlatSev") then
            sResultScript = sResultScript .. math.floor(zaStatTable[i].fAmount) .. "[IMG" .. math.floor(iTotalImageCount) .. "] "
        
        --Percentages. Append a percentage indicator.
        else
        
            --Positive. Add a plus to indicate an increase.
            if(zaStatTable[i].fAmount >= 0) then
                sResultScript = sResultScript .. "+" .. math.floor(zaStatTable[i].fAmount * 100) .. "%% [IMG" .. math.floor(iTotalImageCount) .. "] "
        
            --Negative. Already has a negative sign, append no extra characters.
            else
                sResultScript = sResultScript .. math.floor(zaStatTable[i].fAmount * 100) .. "%% [IMG" .. math.floor(iTotalImageCount) .. "] "
            end
        end
        
        --Locate the stat mod icon associated with the markdown.
        local sImgPath = fnGetStatImgPathFromMarkdown(zaStatTable[i].sIco)
        table.insert(saResultImages, sImgPath)
        
        --Add an image count.
        iTotalImageCount = iTotalImageCount + 1
    end
    
    --Append the duration onto the end. If the duration is -1, don't bother.
    if(pzaStatModStruct.iDuration > -1) then
        sResultScript = sResultScript .. "/" .. pzaStatModStruct.iDuration ..  "[IMG" .. math.floor(iTotalImageCount) .. "]"
        table.insert(saResultImages, "Root/Images/AdventureUI/DebuffIcons/Clock")
        iTotalImageCount = iTotalImageCount + 1
    end
    
    -- |[Finish Up]|
    --Diagnostics.
    Debug_PopPrint("fnStatmodBuildResultScript() - Completed normally.\n")
    
    --Pass back.
    return sResultScript, saResultImages
end
