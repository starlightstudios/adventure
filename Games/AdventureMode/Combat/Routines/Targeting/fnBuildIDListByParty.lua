-- |[ ================================= fnBuildIDListByParty() ================================= ]|
--Given a party type from the list "Combat", "Active", "Friendly", "Hostile", "Enemy", and, if using
-- "Friendly" or "Hostile", an ID, assembles a list of IDs of all entries in that party.
--"Combat": The player's combat party. That is, characters on the field.
--"Active": The player's field party. Active party becomes Combat party when the battle starts, but
--          summons or transformation can change the combat party. Active does not change.
--"Enemy": The enemy party that the player does not control.
--"Friendly": All entities in the same party as the provided ID, including the ID.
--"Friendly Not Self": All entities in the same party as the provided ID, excluding the ID.
--"Hostile": All entities in the party opposing the provided ID.

--Returns a list of IDs. The list can be empty.
function fnBuildIDListByParty(psPartyType, piUserID, pbTrimDisabledCharacters)
    
    -- |[Argument Check]|
    if(psPartyType == nil) then return {} end
    
    -- |[Setup]|
    local bAttemptTrim = false
    local sIDString   = "Combat Party ID"
    local sSizeString = "Combat Party Size"
    
    -- |[Combat Party]|
    if(psPartyType == "Combat") then
        bAttemptTrim = true
        sIDString   = "Combat Party ID"
        sSizeString = "Combat Party Size"
        
    -- |[Active Party]|
    elseif(psPartyType == "Active") then
        sIDString   = "Active Party ID"
        sSizeString = "Active Party Size"
    
    -- |[Enemy Party]|
    elseif(psPartyType == "Enemy") then
        sIDString   = "Enemy ID"
        sSizeString = "Enemy Party Size"
    
    -- |[Opposed Party To ID]|
    elseif(psPartyType == "Hostile") then
        
        --No ID provided:
        if(piUserID == nil) then
            io.write("fnBuildIDListByParty() - Warning, attempted to use party type " .. psPartyType .. " but did not provide an ID. This type requires an ID to resolve which party is which.\n")
            return {}
        end
        
        --Get associated party.
        local iPartyOfID = AdvCombat_GetProperty("Party Of ID", piUserID)
        
        --Player party, use Enemy:
        if(iPartyOfID == gciACPartyGroup_Party) then
            sIDString   = "Enemy ID"
            sSizeString = "Enemy Party Size"
        else
            bAttemptTrim = true
            sIDString   = "Combat Party ID"
            sSizeString = "Combat Party Size"
        end
        
    -- |[Same Party As ID]|
    elseif(psPartyType == "Friendly") then
        
        --No ID provided:
        if(piUserID == nil) then
            io.write("fnBuildIDListByParty() - Warning, attempted to use party type " .. psPartyType .. " but did not provide an ID. This type requires an ID to resolve which party is which.\n")
            return {}
        end
        
        --Get associated party.
        local iPartyOfID = AdvCombat_GetProperty("Party Of ID", piUserID)
        
        --Player party, use Enemy:
        if(iPartyOfID == gciACPartyGroup_Party) then
            bAttemptTrim = true
            sIDString   = "Combat Party ID"
            sSizeString = "Combat Party Size"
        else
            sIDString   = "Enemy ID"
            sSizeString = "Enemy Party Size"
        end
    
    -- |[Unhandled Type]|
    else
        io.write("fnBuildIDListByParty() - Warning, attempted to use unhandled party type " .. psPartyType .. ".\n")
        return {}
    end
    
    -- |[Assemble List]|
    --List to return.
    local iaList = {}
    
    --If not using abyss combat, always disable trimming.
    if(AdvCombat_GetProperty("Is Abyss Combat") == false) then bAttemptTrim = false end
    
    --If pbTrimDisabledCharacters is not provided, or false, then trimming is disallowed.
    if(pbTrimDisabledCharacters ~= true) then bAttemptTrim = false end
    
    --Get size and iterate.
    local iPartySize = AdvCombat_GetProperty(sSizeString)
    for i = 0, iPartySize-1, 1 do
        
        --If we are flagged to trim a character that is disabled (such as being transformed in Witch Hunter Izana) then
        -- perform that here. Note that only AbyssCombat classes can do this, and it only works on the Combat Party.
        if(bAttemptTrim) then
            if(AbyCombat_GetProperty("Is Party Slot Disabled", i) == false) then
                table.insert(iaList, AdvCombat_GetProperty(sIDString, i))
            end
        
        --Always append.
        else
            table.insert(iaList, AdvCombat_GetProperty(sIDString, i))
        end
    end
    
    --Finish.
    return iaList
end