-- |[ ================================== fnGetTargetRedirect =================================== ]|
--When called, returns the ID that should be targeted by an attack directed at a given target ID.
-- If the attack should not redirect, then the ID returned will be the same as the one given.
function fnGetTargetRedirect(piTargetID)
    
    -- |[Arg Check]|
    if(piTargetID == 0 or piTargetID == nil) then return 0 end
    
    -- |[Setup]|
    --Scan all entities currently in combat, and make a list of all IDs that are currently covering the target ID.
    local iaCoveringList = {}
    
    --This is what the tag needs to be to count.
    local sCoverTagName = "Is Covering " .. piTargetID
    
    -- |[Enemy Party]|
    --Scan all entities in the enemy party. This refers to the party the player does not control,
    -- it may be the party of the ability owner.
    local iEnemiesTotal = AdvCombat_GetProperty("Enemy Party Size")
    for i = 0, iEnemiesTotal-1, 1 do
        local iEntityID = AdvCombat_GetProperty("Enemy ID", i)
        AdvCombat_SetProperty("Push Entity By ID", iEntityID)
            local iTagCount = AdvCombatEntity_GetProperty("Tag Count", sCoverTagName)
            if(iTagCount > 0) then
                local iSlot = #iaCoveringList + 1
                iaCoveringList[iSlot] = iEntityID
            end
        DL_PopActiveObject()
    end
    
    -- |[Player Party]|
    --Scan all entities in the player party. Do the same as above.
    local iPartyTotal = AdvCombat_GetProperty("Combat Party Size")
    for i = 0, iPartyTotal-1, 1 do
        local iEntityID = AdvCombat_GetProperty("Combat Party ID", i)
        AdvCombat_SetProperty("Push Entity By ID", iEntityID)
            local iTagCount = AdvCombatEntity_GetProperty("Tag Count", sCoverTagName)
            if(iTagCount > 0) then
                local iSlot = #iaCoveringList + 1
                iaCoveringList[iSlot] = iEntityID
            end
        DL_PopActiveObject()
    end
    
    -- |[Target Redirect]|
    --If there is one entry on the list, redirect to that target.
    if(#iaCoveringList == 1) then
        return iaCoveringList[1]
    
    --Otherwise, randomly roll for which target to hit.
    elseif(#iaCoveringList > 1) then
        local iUseSlot = LM_GetRandomNumber(1, #iaCoveringList)
        return iaCoveringList[iUseSlot]
    end
    
    -- |[No Redirection]|
    --No entries on the covering list, attack is not redirected.
    return piTargetID
    
end
