-- |[ ================================== Is Target Covered By ================================== ]|
--Function that returns true if the given ID is being covered by the second ID. This checks for the 
-- tag "Is Covering [FirstID]" on the second ID.
function fnIsTargetCoveredBy(piTargetID, piCoveringID)
    
    --Argument check.
    if(piTargetID   == nil) then return false end
    if(piCoveringID == nil) then return false end
    
    --This is the tag to check for.
    local sCoverTagName = "Is Covering " .. piTargetID
    
    --Push the covering entity's ID:
    AdvCombat_SetProperty("Push Entity By ID", piCoveringID)
        local iTagCount = AdvCombatEntity_GetProperty("Tag Count", sCoverTagName)
    DL_PopActiveObject()
    
    --If the tag is present, return true.
    if(iTagCount > 0) then return true end
    
    --Failed.
    return false
end