-- |[ =============================== fnTrimTargetsWithoutTag() ================================ ]|
--Should be called after targets have been painted by an ability. Iterates across all targets that
-- were painted and removes target clusters where none of the entities within have any of the tags
-- in the provided tag list.
--For example, if you wanted to make an ability only able to target enemies that have a Bleed DoT, 
-- you would call this with {"Bleeding DoT"}. All painted targets with no Bleeding DoT tags will
-- be removed. Tags can be from effects, innate, or from abilities.
--This function uses a Logical OR basis. If ANY of the tags are present on the target, it is viable.
-- Only if NONE of the tags are present is the target culled.
function fnTrimTargetsWithoutTag(psaTagListing)
    
    -- |[Argument Check]|
    if(psaTagListing == nil) then return end
    
    -- |[Setup]|
    --Variables.
    local iTotalTargetClusters = AdvCombat_GetProperty("Total Target Clusters")
    local saClustersToRemove = {}
    
    -- |[Build Removal List]|
    --Scan all target clusters. Any clusters that are emptied in the loop get removed
    -- at the end of the loop.
    for i = 0, iTotalTargetClusters-1, 1 do
        
        --Name of this cluster:
        local sClusterName = AdvCombat_GetProperty("Name of Target Cluster", i)
        
        --Make a list of targets to remove:
        local iaTargetsToRemove = {}
        
        --For each target in the cluster:
        local iTargetsInCluster = AdvCombat_GetProperty("Total Targets In Cluster", i)
        for p = 0, iTargetsInCluster-1, 1 do
            
            --Get the ID of the target. Check validity.
            local iTargetID = AdvCombat_GetProperty("Target ID In Cluster", i, p)
            if(iTargetID ~= 0) then
                
                --Push the entity and check to see if it has any of the tags. Having any tag is enough.
                local bHasAnyTag = fnTargetHasAnyTags(psaTagListing, iTargetID)
            
                --If no tag was found, this entity needs to be removed from the target cluster.
                if(bHasAnyTag == false) then
                    table.insert(iaTargetsToRemove, iTargetID)
                end
            end
        end
        
        --Once all targets are scanned, remove the target IDs from the given cluster. Note that this
        -- is done *after* checking all targets to preserve list integrity.
        for p = 1, #iaTargetsToRemove, 1 do
            AdvCombat_SetProperty("Remove Target From Cluster", i, iaTargetsToRemove[p])
        end
        
        --After removing targets, check if the cluster is empty. If so, remove the cluster.
        iTargetsInCluster = AdvCombat_GetProperty("Total Targets In Cluster", i)
        if(iTargetsInCluster < 1) then
            table.insert(saClustersToRemove, sClusterName)
        end
    end
    
    -- |[Removal]|
    --Remove pending clusters.
    for i = 1, #saClustersToRemove, 1 do
        AdvCombat_SetProperty("Remove Target Cluster By Name", saClustersToRemove[i])
    end
end

