-- |[ ================================= fnGetAnimationTiming() ================================= ]|
--Returns how many ticks long the requested animation is. Returns 0 on error.
function fnGetAnimationTiming(psAnimName)
    
    -- |[Argument Check]|
    if(psAnimName == nil) then return 0 end
    
    -- |[Scan Animations]|
    for i = 1, giAnimationTimingsTotal, 1 do
        if(gzaAnimationTimings[i].sName == psAnimName) then
            return gzaAnimationTimings[i].iTicks
        end
    end
    
    -- |[Not Found]|
    return 0
end
