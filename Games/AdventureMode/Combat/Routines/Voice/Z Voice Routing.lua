-- |[ ===================================== Voice Routing ====================================== ]|
--Script used to generate vocal callouts during combat. This is called during gzaAbiHandlerFuncs.fnApplicationSequence()
-- so all the needed variables are in gzAbiPack.
local iTimer = math.floor(LM_GetScriptArgument(0, "N"))
local iFlag  = math.floor(LM_GetScriptArgument(1, "N"))
--io.write("Ran voice callout handler " .. iTimer .. " with flag " .. iFlag .. ".\n")

-- |[ ===== Pain Callout ===== ]|
--In the case of a pain callout, the target is the one who is checked, not the originator.
if(iFlag == gciPainCallout) then

    --Diagnostics.
    local bDiagnostics = Debug_GetFlag("Voice: All") or Debug_GetFlag("Voice: All Toggle") or Debug_GetFlag("Voice: Pain")
    Debug_PushPrint(bDiagnostics, "Voice Routing: Pain - Begin.\n")

    --Find out who is getting hit.
    if(gzAbiPack.zAbilityPack == nil or gzAbiPack.iTargetID == 0) then
        Debug_PopPrint("Voice routing has no ability or no target, stopping.\n")
        return
    end
    
    --Scan the combat party. The internal name of the struck character is the name of the voice pack we need to find.
    Debug_Print("Calling pain. Target is " .. gzAbiPack.iTargetID .. "\n")
    local iCombatPartySize = AdvCombat_GetProperty("Combat Party Size")
    for i = 0, iCombatPartySize-1, 1 do
        
        --Compare IDs.
        local iCombatPartyID = AdvCombat_GetProperty("Combat Party ID", i)
        if(iCombatPartyID == gzAbiPack.iTargetID) then
        
            --Debug.
            Debug_Print("Pain reply for ID " .. iCombatPartyID .. " in slot " .. i .. "\n")
        
            --Get the internal name.
            AdvCombat_SetProperty("Push Entity By ID", iCombatPartyID)
                local sInternalName = AdvCombatEntity_GetProperty("Internal Name")
                local sCurrentJob = AdvCombatEntity_GetProperty("Current Job")
            DL_PopActiveObject()
            Debug_Print("Character name: " .. sInternalName .. "\n")
            
            --Special: Christine uses the "Chris" voice lines during the first part of chapter 5. Override that here.
            if(sInternalName == "Christine" and sCurrentJob == "Teacher") then
                sInternalName = "Chris"
                Debug_Print("Character name changed to: " .. sInternalName .. "\n")
            end
        
            --Try to find a matching voice pack. If found, call the pain handler.
            local zVoiceDataPack = VoiceDataList:fnLocateEntry(sInternalName)
            if(zVoiceDataPack ~= nil) then
                Debug_Print("Character has voice pack. Calling fnHandlePain().\n")
                zVoiceDataPack:fnHandlePain(iTimer, iCombatPartyID)
            end
            
            Debug_PopPrint("Pain voice routing finished.\n")
            return
        end
    end
    
    --Diagnostics.
    Debug_PopPrint("Pain voice routing finished with no party match.\n")

-- |[ ===== Action Callouts ===== ]|
else

    -- |[Error Check]|
    --If the associated voice data is nil, do nothing.
    if(gzAbiPack.zAbilityPack == nil or gzAbiPack.zAbilityPack.sVoiceData == nil) then return end
    
    -- |[Resolve Package]|
    --Execute subdivision. The first section is the name of the voice pack in question. If the
    -- pack exists, order it to call with the appropriate flag.
    local sUseData = gzAbiPack.zAbilityPack.sVoiceData
    local saSections = fnSubdivide(gzAbiPack.zAbilityPack.sVoiceData, "|")
            
    --Special: Christine uses the "Chris" voice lines during the first part of chapter 5. Override that here.
    if(saSections[1] == "Christine") then
        
        --Get Christine's current job. If it's "Teacher", this is the opening of chapter 5.
        AdvCombat_SetProperty("Push Party Member", "Christine")
            local sCurrentJob = AdvCombatEntity_GetProperty("Current Job")
        DL_PopActiveObject()
        if(sCurrentJob == "Teacher") then
            
            --Reset the section.
            saSections[1] = "Chris"
            
            --Reset the voice data package.
            sUseData = "Chris|"
            for i = 2, #saSections, 1 do
                sUseData = sUseData .. saSections[i] .. "|"
            end
        end
    end
    
    --Locate.
    local zVoiceDataPack = VoiceDataList:fnLocateEntry(saSections[1])

    --Pack not found. Stop.
    if(zVoiceDataPack == nil) then return end
    --io.write("Final package: " .. sUseData .. "\n")
    
    -- |[Switch Callout Type]|
    --Pre-anim, plays before the attack lands. Can set internal flags so a post-animation callout is used.
    if(iFlag == gciPreAnimCallout) then
        zVoiceDataPack:fnHandlePreAction(sUseData, iTimer)
    
    --Post-anim. Used for some abilities that play a sound after the impact.
    elseif(iFlag == gciPostAnimCallout) then
        zVoiceDataPack:fnHandlePostAction(sUseData, iTimer)
    end
end