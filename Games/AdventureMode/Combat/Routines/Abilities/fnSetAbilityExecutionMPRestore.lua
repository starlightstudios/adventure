-- |[ ============================ fnSetAbilityExecutionMPRestore() ============================ ]|
--Function called during ability setup. Sets flags for MP restore.
function fnSetAbilityExecutionMPRestore(pzAbiStruct, piMPRestore)
    
    -- |[Argument Check]|
    if(pzAbiStruct == nil) then return end
    if(piMPRestore == nil) then return end
    
    -- |[Set Common Flags]|
    --All healing abilities toggle these flags on.
    pzAbiStruct.zExecutionAbiPackage.bDoesNoDamage = true
    pzAbiStruct.zExecutionAbiPackage.bDoesNoStun   = true
    pzAbiStruct.zExecutionAbiPackage.bAlwaysHits   = true
    pzAbiStruct.zExecutionAbiPackage.bNeverCrits   = true
    
    -- |[Set Variables]|
    --Variable for MP restore.
    pzAbiStruct.zExecutionAbiPackage.iMPGeneration = piMPRestore
end
