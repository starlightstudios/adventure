-- |[ ============================ fnMakeAndRegisterBuffPrototype() ============================ ]|
--Given a set of values, makes and registers a statmod effect prototype. This is meant for explicit buffs
-- and uses specific strength values for predictions that indicate it always applies.
--Deprecated.
function fnMakeAndRegisterBuffPrototype(psName, psDisplayName, psIcon, piTurns, psStatString, psaDescription, pzaTagList)
    EffectList:fnCreateBuffPrototype(psName, psDisplayName, psIcon, piTurns, psStatString, psaDescription, pzaTagList)
    --[=[
    
    -- |[Argument Check]|
    if(pzEffectpackage == nil) then return end
    if(psName          == nil) then return end
    if(psDisplayName   == nil) then return end
    if(psIcon          == nil) then return end
    if(piTurns         == nil) then return end
    if(psStatString    == nil) then return end
    if(psaDescription  == nil) then return end

    -- |[Tag Checking]|
    --Setup.
    local zaUseTagList = pzaTagList
    if(zaUseTagList == nil) then zaUseTagList = {} end
    
    --Scan the tag list. If "Is Positive" is not found on a buff, add it.
    fnAppendTag(zaUseTagList, "Is Positive")

    -- |[Normal Version]|
    --This effect uses a standardized StatMod script.

    --Run the standardized builder.
    local zPrototype = fnCreateStatmodEffectPrototype(psDisplayName, piTurns, gbIsBuff, "Root/Images/AdventureUI/Abilities/" .. psIcon, psStatString, psaDescription, true)
    fnGetApplicationDataFromEffectPackage(zPrototype, pzEffectpackage)

    --Tags.
    if(pzaTagList == nil) then
        zPrototype.zaTagList = {}
    else
        zPrototype.zaTagList = pzaTagList
    end

    -- |[Register]|
    fnRegisterEffectPrototype(psName, gciEffect_Is_StatMod, zPrototype)
    ]=]
end