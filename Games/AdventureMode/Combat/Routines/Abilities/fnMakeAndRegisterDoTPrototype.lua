-- |[ ============================ fnMakeAndRegisterDoTPrototype() ============================= ]|
--Given a set of values, makes and registers a DoT prototype to the global listing. Can optionally
-- add a critical hit version as well.
--If the parameters piCritTurnBonus and beyond are nil, no critical version will be registered.
function fnMakeAndRegisterDoTPrototype(psName, piTurns, piApplyStrength, pfDamPerTurn, piDamageType, psDisplayName, psIcon, piCritTurns, piCritApplyStrength)
    EffectList:fnCreateDoTPrototype(psName, piTurns, piApplyStrength, pfDamPerTurn, piDamageType, psDisplayName, psIcon, piCritTurns, piCritApplyStrength)
    --[=[
    
    -- |[Argument Check]|
    if(psName          == nil) then return end
    if(piTurns         == nil) then return end
    if(piApplyStrength == nil) then return end
    if(pfDamPerTurn    == nil) then return end
    if(piDamageType    == nil) then return end
    if(psDisplayName   == nil) then return end
    if(psIcon          == nil) then return end

    -- |[Setup]|
    --Compute the total damage.
    local fDamage = pfDamPerTurn * piTurns

    -- |[Normal Version]|
    --This effect uses a standardized DoT script.
    local zPrototype = fnCreateDoTEffectPrototype(piDamageType, piTurns, fDamage)
    zPrototype.sDisplayName = psDisplayName
    zPrototype.sIcon = psIcon
    
    --Information needed for prediction.
    zPrototype.iEffectStr = piApplyStrength

    --Register.
    fnRegisterEffectPrototype(psName, gciEffect_Is_DoT, zPrototype)
    
    -- |[Critical Strike Version]|
    --If the parameter piCritTurnBonus is nil, no critical strike version exists so stop here.
    if(piCritTurns == nil) then return end
        
    --Adds turns to the damage length and recompute the damage.
    fDamage = pfDamPerTurn * piCritTurns
    
    --Create effect.
    local zPrototypeCrit = fnCreateDoTEffectPrototype(piDamageType, piCritTurns, fDamage)
    zPrototypeCrit.sDisplayName = psDisplayName
    zPrototypeCrit.sIcon = psIcon
    
    --Information needed for prediction.
    zPrototype.iEffectStr = piCritApplyStrength
    
    --Register.
    fnRegisterEffectPrototype(psName .. ".Crit", gciEffect_Is_DoT, zPrototypeCrit)
    ]=]
end