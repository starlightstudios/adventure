-- |[ ============================== fnSetAbilityExecutionEnemy() ============================== ]|
--Function called during ability setup. Sets flags so the action is one typically used by an enemy,
-- causing a black flash and printing a title. Optional flags can disable the title or override it.
function fnSetAbilityExecutionEnemy(pzAbiStruct, psOverrideName)
    
    -- |[Argument Check]|
    if(pzAbiStruct == nil) then return end
    
    -- |[Set Common Flags]|
    --Toggle flags on.
    pzAbiStruct.zExecutionAbiPackage.bCauseBlackFlash = true
    pzAbiStruct.zExecutionAbiPackage.sShowTitle = pzAbiStruct.sDisplayName
    
    --Enemy abilities do not crit.
    pzAbiStruct.zExecutionAbiPackage.bNeverCrits = true
    
    -- |[Optional Override]|
    --This argument is optional. It can override the title shown. To disable the title, pass "" or "Null".
    if(psOverrideName == "") then
        pzAbiStruct.zExecutionAbiPackage.sShowTitle = "Null"
    
    elseif(psOverrideName ~= nil) then
        pzAbiStruct.zExecutionAbiPackage.sShowTitle = psOverrideName
    end
end
