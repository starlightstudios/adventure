-- |[ ============================= fnSetAbilityExecutionDamage() ============================== ]|
--Function called during ability setup. Sets flags normally associated with basic damage, such as
-- damage type, whether the weapon should be used, miss rate, and damage factor.
function fnSetAbilityExecutionDamage(pzAbiStruct, piDamageType, piMissthreshold, pfDamageFactor)

    -- |[Argument Check]|
    if(pzAbiStruct     == nil) then return end
    if(piDamageType    == nil) then return end
    if(piMissthreshold == nil) then return end
    if(pfDamageFactor  == nil) then return end

    -- |[Damage Type]|
    --If the user passes in gciDamageType_UseWeapon then toggle on the weapon damage flag and default
    -- the damage to Slashing. This will get overridden later.
    if(piDamageType == gciDamageType_UseWeapon) then
        pzAbiStruct.zExecutionAbiPackage.bUseWeapon  = true
        pzAbiStruct.zExecutionAbiPackage.iDamageType = gciDamageType_Slashing
    
    --User passed in the damage type directly.
    else
        pzAbiStruct.zExecutionAbiPackage.bUseWeapon  = false
        pzAbiStruct.zExecutionAbiPackage.iDamageType = piDamageType
    end

    -- |[Other Flags]|
    pzAbiStruct.zExecutionAbiPackage.iMissThreshold = piMissthreshold
    pzAbiStruct.zExecutionAbiPackage.fDamageFactor  = pfDamageFactor

end
