-- |[ =============================== fnSetAbilityExecutionBuff() ============================== ]|
--Function called during ability setup. Sets flags normally associated with buffs. Buffs always apply
-- but otherwise are mostly handled by the effect code.
function fnSetAbilityExecutionBuff(pzAbiStruct)
    
    -- |[Argument Check]|
    if(pzAbiStruct == nil) then return end
    
    -- |[Set Common Flags]|
    --All buffs toggle these on.
    pzAbiStruct.zExecutionAbiPackage.bDoesNoDamage        = true
    pzAbiStruct.zExecutionAbiPackage.bDoesNoStun          = true
    pzAbiStruct.zExecutionAbiPackage.bAlwaysHits          = true
    pzAbiStruct.zExecutionAbiPackage.bNeverCrits          = true
    pzAbiStruct.zExecutionAbiPackage.bEffectAlwaysApplies = true
end
