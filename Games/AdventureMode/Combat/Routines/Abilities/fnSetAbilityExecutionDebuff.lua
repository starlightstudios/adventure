-- |[ ============================= fnSetAbilityExecutionDebuff() ============================== ]|
--Function called during ability setup. Sets flags normally associated with debuffs. This is a 'strict'
-- handling of a debuff, in that it is expected to deal no damage. This also can't crit but you can
-- override that afterwards if desired.
function fnSetAbilityExecutionDebuff(pzAbiStruct)
    
    -- |[Argument Check]|
    if(pzAbiStruct == nil) then return end
    
    -- |[Set Common Flags]|
    --All buffs toggle these on.
    pzAbiStruct.zExecutionAbiPackage.bDoesNoDamage = true
    pzAbiStruct.zExecutionAbiPackage.bNeverGlances = true
    pzAbiStruct.zExecutionAbiPackage.bDoesNoStun = true
    pzAbiStruct.zExecutionAbiPackage.bNeverCrits = true
end
