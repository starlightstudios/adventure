-- |[ ============================= fnSetAbilityExecutionHealing() ============================= ]|
--Function called during ability setup. Sets flags normally associated with healing, including
-- base value and scaling.
function fnSetAbilityExecutionHealing(pzAbiStruct, piHealingBase, pfHealingScaleFactor, pfHealingPctOfHPMax)
    
    -- |[Argument Check]|
    if(pzAbiStruct          == nil) then return end
    if(piHealingBase        == nil) then return end
    if(pfHealingScaleFactor == nil) then return end
    if(pfHealingPctOfHPMax  == nil) then return end
    
    -- |[Set Common Flags]|
    --All healing abilities toggle these flags on.
    pzAbiStruct.zExecutionAbiPackage.bDoesNoDamage = true
    pzAbiStruct.zExecutionAbiPackage.bDoesNoStun   = true
    pzAbiStruct.zExecutionAbiPackage.bAlwaysHits   = true
    pzAbiStruct.zExecutionAbiPackage.bNeverCrits   = true
    
    -- |[Set Variables]|
    --These variables determine how much the heal actually restores.
    pzAbiStruct.zExecutionAbiPackage.iHealingBase    = piHealingBase
    pzAbiStruct.zExecutionAbiPackage.fHealingFactor  = pfHealingScaleFactor
    pzAbiStruct.zExecutionAbiPackage.fHealingPercent = pfHealingPctOfHPMax
end
