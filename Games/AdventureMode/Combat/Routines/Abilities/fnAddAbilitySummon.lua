-- |[ ================================== fnAddAbilitySummon() ================================== ]|
function fnAddAbilitySummon(pzExecPackage, psEnemyName, psOverrideAutoHandler)
    
    -- |[Argument Check]|
    if(pzExecPackage == nil) then return end
    if(psEnemyName   == nil) then return end

    -- |[Append Enemy Name]|
    --Note: The enemy name may not actually be used if the auto-handler is overridden.
    table.insert(pzExecPackage.saSummonHandlerNames, psEnemyName)

    -- |[Use Auto]|
    --If an enemy handler script is not provided, use the auto-handler.
    if(psOverrideAutoHandler == nil) then
        table.insert(pzExecPackage.saSummonHandlerScripts, AdvCombat_GetProperty("Enemy Auto Handler"))
    
    --An explicit script is being called, which may or may not require the enemy name to be passed to it.
    else
        table.insert(pzExecPackage.saSummonHandlerScripts, psOverrideAutoHandler)
    end
end