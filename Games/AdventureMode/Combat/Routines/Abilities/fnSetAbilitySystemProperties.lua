-- |[ ============================= fnSetAbilitySystemProperties() ============================= ]|
--Called typically when a new ability prototype is being created, sets common properties like name, job, and
-- display name. This uses human-readable strings as much as possible to make the process easier to
-- modify even if that's a bit slower.
--Expects the ability structure to be passed in.
function fnSetAbilitySystemProperties(pzAbility, psJobName, psSkillName, psDisplayName, piJPCost, pbIsFreeAction, psBacking, psFrame, psIcon, psResponse)
    
    -- |[Argument Check]|
    if(pzAbility      == nil) then return end
    if(psJobName      == nil) then return end
    if(psSkillName    == nil) then return end
    if(psDisplayName  == nil) then return end
    if(piJPCost       == nil) then return end
    if(pbIsFreeAction == nil) then return end
    if(psBacking      == nil) then return end
    if(psFrame        == nil) then return end
    if(psIcon         == nil) then return end
    if(psResponse     == nil) then return end

    -- |[Naming]|
    --Display/System Variables
    pzAbility.sJobName   = psJobName
    pzAbility.sSkillName = psSkillName
    
    --Internal name is the name of the ability when it is an internal class ability, one that
    -- is auto-equipped when the player changes class. It is typically just the job name with
    -- " Internal" appended on the end.
    pzAbility.sInternalName = psJobName .. " Internal"
    
    --The display name can be "Use Skill Name" in which case it is identical to the skill name.
    if(psDisplayName == "Use Skill Name" or psDisplayName == "$SkillName") then
        pzAbility.sDisplayName = psSkillName
    
    --Otherwise, override the display name.
    else
        pzAbility.sDisplayName = psDisplayName
    end
    
    -- |[Cost and Combat Display]|
    --The JP unlock cost is an integer, usually using a constant.
    pzAbility.iJPUnlockCost = piJPCost
    
    --Mark the ability as a free action or not.
    pzAbility.bIsFreeAction = pbIsFreeAction
    
    --The backing is one of "Direct", "DoT", "Heal", "Buff", or "Debuff".
    if(pzAbility.bIsFreeAction == gbIsNotFreeAction) then
        if(psBacking == "Direct") then
            pzAbility.sIconBacking  = gsAbility_Backing_Direct
        elseif(psBacking == "DoT") then
            pzAbility.sIconBacking  = gsAbility_Backing_DoT
        elseif(psBacking == "Heal") then
            pzAbility.sIconBacking  = gsAbility_Backing_Heal
        elseif(psBacking == "Buff") then
            pzAbility.sIconBacking  = gsAbility_Backing_Buff
        elseif(psBacking == "Debuff") then
            pzAbility.sIconBacking  = gsAbility_Backing_Debuff
        end
    else
        if(psBacking == "Direct") then
            pzAbility.sIconBacking  = gsAbility_Backing_Free_Direct
        elseif(psBacking == "DoT") then
            pzAbility.sIconBacking  = gsAbility_Backing_Free_DoT
        elseif(psBacking == "Heal") then
            pzAbility.sIconBacking  = gsAbility_Backing_Free_Heal
        elseif(psBacking == "Buff") then
            pzAbility.sIconBacking  = gsAbility_Backing_Free_Buff
        elseif(psBacking == "Debuff") then
            pzAbility.sIconBacking  = gsAbility_Backing_Free_Debuff
        end
    end
    
    --The frame is one of "Active", "Passive", "Special", or "Combo".
    if(pbIsFreeAction == gbIsNotFreeAction) then
        if(psFrame == "Active") then
            pzAbility.sIconFrame = gsAbility_Frame_Active
        elseif(psFrame == "Passive") then
            pzAbility.sIconFrame = gsAbility_Frame_Passive
        elseif(psFrame == "Special") then
            pzAbility.sIconFrame = gsAbility_Frame_Special
        elseif(psFrame == "Combo") then
            pzAbility.sIconFrame = gsAbility_Frame_Combo
        end
    else
        if(psFrame == "Active") then
            pzAbility.sIconFrame = gsAbility_Frame_Free_Active
        elseif(psFrame == "Passive") then
            pzAbility.sIconFrame = gsAbility_Frame_Free_Passive
        elseif(psFrame == "Special") then
            pzAbility.sIconFrame = gsAbility_Frame_Free_Special
        elseif(psFrame == "Combo") then
            pzAbility.sIconFrame = gsAbility_Frame_Free_Combo
        end
    end

    --The icon path is the DLPath to the icon, with "Root/Images/AdventureUI/Abilities/" appended on the front.
    pzAbility.sIconPath = "Root/Images/AdventureUI/Abilities/" .. psIcon
    
    --The CP cost is set to NULL. The function fnSetAbilityUsabilityProperties() should handle changing the
    -- icon based on the usability.
    pzAbility.sCPIcon = "Null"
    
    -- |[Response Type]|
    --This determines when and how the ability script gets called. Abilities that need to activate when an attack
    -- lands are different than ones that activate only when used. Passives typically activate very little.
    --When making a unique ability, manually overriding these flags may be necessary. The presets
    -- "Direct", "Passive", "Counterattack", and "Ambush" are available.
    if(psResponse == "Direct") then
        pzAbility.iResponseType = gciAbility_ResponseStandard_DirectAction
    elseif(psResponse == "Passive") then
        pzAbility.iResponseType = gciAbility_ResponseStandard_Passive
    elseif(psResponse == "Counterattack") then
        pzAbility.iResponseType = gciAbility_ResponseStandard_Counterattack
    elseif(psResponse == "Ambush") then
        pzAbility.iResponseType = gciAbility_ResponseStandard_Ambush
    end
    
end