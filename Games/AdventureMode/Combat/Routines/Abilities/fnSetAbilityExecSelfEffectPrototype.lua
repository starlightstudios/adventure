-- |[ ========================= fnSetAbilityExecSelfEffectPrototype() ========================== ]|
--Constructs and returns a default effect package modified to use an effect prototype. The apply
-- animation and sound are optional arguments.
--This applies to the user of the ability, not the target.
function fnSetAbilityExecSelfEffectPrototype(pzAbiStruct, psPrototypeName, psApplyText, psApplyColorString, psApplyAnim, psApplySound)
    
    -- |[Argument Check]|
    if(pzAbiStruct     == nil) then return end
    if(psPrototypeName == nil) then return end
    
    -- |[Construct]|
    --Builds the basic effect package with all its values at zero.
    pzAbiStruct.zExecutionSelfPackage = fnConstructDefaultEffectPackage()
    
    -- |[Overrides]|
    --The effect path and crit path are always the prototype file.
    pzAbiStruct.zExecutionSelfPackage.sEffectPath     = gsGlobalEffectPrototypePath
    pzAbiStruct.zExecutionSelfPackage.sEffectCritPath = gsGlobalEffectPrototypePath
    
    --If an apply animation is provided, set it here. The default is "Null".
    if(psApplyAnim ~= nil) then
        pzAbiStruct.zExecutionSelfPackage.sApplyAnimation = psApplyAnim
    end
    
    --If an apply sound is provided, set it here. The default is "Null".
    if(psApplySound ~= nil) then
        pzAbiStruct.zExecutionSelfPackage.sApplySound = psApplySound
    end
    
    --On-apply, what string is printed and what color is it.
    if(psApplyText ~= nil) then
        pzAbiStruct.zExecutionSelfPackage.sApplyText     = psApplyText
        pzAbiStruct.zExecutionSelfPackage.sCritApplyText = psApplyText
    end
    if(psApplyColorString ~= nil) then
        pzAbiStruct.zExecutionSelfPackage.sApplyTextCol = psApplyColorString
    end
    
    -- |[Use of Effect Prototype]|
    pzAbiStruct.zExecutionSelfPackage.sPrototypeName = psPrototypeName
end

