-- |[ ================================== fnAddAbilityChaser() ================================== ]|
--Given an ability structure, creates a chaser for the named DoT type. Ex: "Corroding DoT", "Bleeding DoT".
function fnAddAbilityChaser(pzAbiStruct, pbConsumeEffect, psDoTTag, pfDamageChangePerDoT, pfDamageConsumeFactor)
    
    -- |[Argument Check]|
    if(pzAbiStruct           == nil) then return end
    if(pbConsumeEffect       == nil) then return end
    if(psDoTTag              == nil) then return end
    if(pfDamageChangePerDoT  == nil) then return end
    if(pfDamageConsumeFactor == nil) then return end
    
    -- |[Package Check]|
    --The execution package needs to have been created.
    if(pzAbiStruct.zExecutionAbiPackage == nil) then return end
    
    -- |[Flag]|
    --Toggle this flag on.
    pzAbiStruct.zExecutionAbiPackage.bHasChaserModules = true
    
    -- |[Create]|
    --Create a chaser module and set its properties.
    local zChaser = {}
    zChaser.bConsumeEffect            = pbConsumeEffect
    zChaser.sDoTTag                   = psDoTTag
    zChaser.fDamageFactorChangePerDoT = pfDamageChangePerDoT
    zChaser.fDamageRemainingFactor    = pfDamageConsumeFactor
    
    --Register the module.
    table.insert(pzAbiStruct.zExecutionAbiPackage.zaChaserModules, zChaser)
    
    --Mark how many chaser modules this ability has.
    pzAbiStruct.zExecutionAbiPackage.iChaserModulesTotal = #pzAbiStruct.zExecutionAbiPackage.zaChaserModules
end
