-- |[ =========================== fnSetAbilityExecEffectPrototype() ============================ ]|
--Constructs and returns a default effect package modified to use an effect prototype. The apply
-- animation and sound are optional arguments.
function fnSetAbilityExecEffectPrototype(pzAbiStruct, psPrototypeName, psApplyAnim, psApplySound)
    
    -- |[Argument Check]|
    if(pzAbiStruct     == nil) then return end
    if(psPrototypeName == nil) then return end
    
    -- |[Construct]|
    --Builds the basic effect package with all its values at zero.
    pzAbiStruct.zExecutionEffPackage = fnConstructDefaultEffectPackage()
    
    -- |[Overrides]|
    --The effect path and crit path are always the prototype file.
    pzAbiStruct.zExecutionEffPackage.sEffectPath     = gsGlobalEffectPrototypePath
    pzAbiStruct.zExecutionEffPackage.sEffectCritPath = gsGlobalEffectPrototypePath
    
    --If an apply animation is provided, set it here. The default is "Null".
    if(psApplyAnim ~= nil) then
        pzAbiStruct.zExecutionEffPackage.sApplyAnimation = psApplyAnim
    end
    
    --If an apply sound is provided, set it here. The default is "Null".
    if(psApplySound ~= nil) then
        pzAbiStruct.zExecutionEffPackage.sApplySound = psApplySound
    end
    
    -- |[Use of Effect Prototype]|
    pzAbiStruct.zExecutionEffPackage.sPrototypeName = psPrototypeName
end

