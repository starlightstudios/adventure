-- |[ ============================ fnMakeAndRegisterStatPrototype() ============================ ]|
--Given a set of values, makes and registers a statmod effect prototype. Unlike the DoT version, the
-- statmod needs to have a more specific description provided for the effect.
--If the parameters piCritTurns and beyond are nil, no critical version will be registered. In addition,
-- pzaTagList is optional and can be nil.
--Deprecated.
function fnMakeAndRegisterStatPrototype(pzEffectpackage, psName, pbIsBuff, psDisplayName, psIcon, piTurns, piApplyStrength, psStatString, psaDescription, pzaTagList, piCritTurns, piCritApplyStrength)

    --Deprecated Warning
    if(gbPrintDeprecatedCallWarnings == true) then
        io.write("Warning: Calling fnMakeAndRegisterStatPrototype() is deprecated. Call from: " .. LM_GetCallStack(0) .. "\n")
    end
    
    EffectList:fnCreateStatPrototype(pzEffectpackage, psName, pbIsBuff, psDisplayName, psIcon, piTurns, piApplyStrength, psStatString, psaDescription, pzaTagList, piCritTurns, piCritApplyStrength)
    --[=[
    
    -- |[Argument Check]|
    if(pzEffectpackage == nil) then return end
    if(psName          == nil) then return end
    if(pbIsBuff        == nil) then return end
    if(psDisplayName   == nil) then return end
    if(psIcon          == nil) then return end
    if(piTurns         == nil) then return end
    if(piApplyStrength == nil) then return end
    if(psStatString    == nil) then return end
    if(psaDescription  == nil) then return end

    -- |[Tag Checking]|
    --Setup.
    local zaUseTagList = pzaTagList
    if(zaUseTagList == nil) then zaUseTagList = {} end
    
    --Scan the tag list. If "Is Positive" is not found on a buff, add it.
    if(pbIsBuff == true) then
        fnAppendTag(zaUseTagList, "Is Positive")
    
    --If "Is Negative" is not found on a debuff, add it.
    else
        fnAppendTag(zaUseTagList, "Is Negative")
    end

    -- |[Normal Version]|
    --This effect uses a standardized StatMod script.

    --Run the standardized builder.
    local zPrototype = fnCreateStatmodEffectPrototype(psDisplayName, piTurns, pbIsBuff, "Root/Images/AdventureUI/Abilities/" .. psIcon, psStatString, psaDescription, true)
    fnGetApplicationDataFromEffectPackage(zPrototype, pzEffectpackage)

    --Tags.
    if(pzaTagList == nil) then
        zPrototype.zaTagList = {}
    else
        zPrototype.zaTagList = pzaTagList
    end

    -- |[Register]|
    fnRegisterEffectPrototype(psName, gciEffect_Is_StatMod, zPrototype)

    -- |[Critical Version]|
    --If this value is nil, no crit version exists.
    if(piCritTurns == nil) then return end

    --Run the standardized builder.
    local zPrototypeCrit = fnCreateStatmodEffectPrototype(psDisplayName, piCritTurns, pbIsBuff, "Root/Images/AdventureUI/Abilities/" .. psIcon, psStatString, psaDescription, true)
    
    --Tags.
    if(pzaTagList == nil) then
        zPrototype.zaTagList = {}
    else
        zPrototype.zaTagList = pzaTagList
    end
    
    --Register.
    fnRegisterEffectPrototype(psName .. ".Crit", gciEffect_Is_StatMod, zPrototypeCrit)
    ]=]
end