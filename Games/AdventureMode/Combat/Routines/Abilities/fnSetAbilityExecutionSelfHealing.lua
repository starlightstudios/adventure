-- |[ =========================== fnSetAbilityExecutionSelfHealing() =========================== ]|
--Function called during ability setup. Sets flags normally associated with self-healing, including
-- base value and scaling.
function fnSetAbilityExecutionSelfHealing(pzAbiStruct, piHealingBase, pfHealingScaleFactor, pfHealPctOfMaxHP)
    
    -- |[Argument Check]|
    if(pzAbiStruct == nil) then return end

    -- |[Set Variables]|
    --These variables determine how much the heal actually restores.
    pzAbiStruct.zExecutionAbiPackage.iSelfHealingBase = piHealingBase
    pzAbiStruct.zExecutionAbiPackage.fSelfHealingPercent = pfHealPctOfMaxHP
    pzAbiStruct.zExecutionAbiPackage.fSelfHealingFactor = pfHealingScaleFactor
end
