-- |[ ============================= fnSetAbilityExecutionShield() ============================= ]|
--Function called during ability setup. Sets flags normally associated with adding a shield.
function fnSetAbilityExecutionShield(pzAbiStruct, piShieldBase, pfShieldScaleFactor)
    
    -- |[Argument Check]|
    if(pzAbiStruct         == nil) then return end
    if(piShieldBase        == nil) then return end
    if(pfShieldScaleFactor == nil) then return end
    
    -- |[Set Common Flags]|
    --All healing abilities toggle these flags on.
    pzAbiStruct.zExecutionAbiPackage.bDoesNoDamage = true
    pzAbiStruct.zExecutionAbiPackage.bDoesNoStun   = true
    pzAbiStruct.zExecutionAbiPackage.bAlwaysHits   = true
    pzAbiStruct.zExecutionAbiPackage.bNeverCrits   = true
    
    -- |[Set Variables]|
    --These variables determine how much the shield applies for.
    pzAbiStruct.zExecutionAbiPackage.iShieldsBase   = piShieldBase
    pzAbiStruct.zExecutionAbiPackage.fShieldsFactor = pfShieldScaleFactor
end
