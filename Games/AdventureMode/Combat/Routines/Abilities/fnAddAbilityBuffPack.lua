-- |[ ================================= fnAddAbilityBuffPack() ================================= ]|
--A variant of fnAddAbilityEffectPack() that explicitly applies a buff, and thus doesn't need to worry
-- about application strength, type, or crits.
function fnAddAbilityBuffPack(pzAbiStruct, psPrototypeName, psApplyText, psApplyColorString, psApplyAnim, psApplySound)
    
    -- |[Argument Check]|
    if(pzAbiStruct     == nil) then return nil end
    if(psPrototypeName == nil) then return nil end
    
    -- |[Create Package]|
    --Construct the package with defaults.
    local zPack = fnConstructDefaultEffectPackage()
    
    --Strength and Application Type. Not actually used.
    zPack.iEffectStr      = 1000
    zPack.iEffectCritStr  = 1000
    zPack.iEffectType     = gciDamageType_Physical
    
    --This assumes that a global effect is going to be created.
    zPack.sEffectPath     = gsGlobalEffectPrototypePath
    zPack.sEffectCritPath = gsGlobalEffectPrototypePath
    
    --If an apply animation is provided, set it here. The default is "Null".
    if(psApplyAnim ~= nil) then
        zPack.sApplyAnimation = psApplyAnim
    end
    
    --If an apply sound is provided, set it here. The default is "Null".
    if(psApplySound ~= nil) then
        zPack.sApplySound = psApplySound
    end
    
    --On-apply, what string is printed and what color is it.
    if(psApplyText ~= nil) then
        zPack.sApplyText     = psApplyText
        zPack.sCritApplyText = psApplyText
    end
    if(psApplyColorString ~= nil) then
        zPack.sApplyTextCol = psApplyColorString
    end
    
    --Default resist case.
    zPack.sResistText     = "Resisted!"
    zPack.sResistTextCol  = "Color:White"
    
    -- |[Tag Handling]|
    --Initialize the tag arrays. These are completely optional to pass in, not all effects need tags.
    zPack.saApplyTagBonus    = {}
    zPack.saApplyTagMalus    = {}
    zPack.saSeverityTagBonus = {}
    zPack.saSeverityTagMalus = {}
    
    -- |[Prototype]|
    zPack.sPrototypeName = psPrototypeName
    
    -- |[Apply Package]|
    --No packages yet, put it in the A slot.
    if(pzAbiStruct.zExecutionEffPackage == nil) then
        pzAbiStruct.zExecutionEffPackage = zPack
        return pzAbiStruct.zExecutionEffPackage
    
    --B slot.
    elseif(pzAbiStruct.zExecutionEffPackageB == nil) then
        pzAbiStruct.zExecutionEffPackageB = zPack
        return pzAbiStruct.zExecutionEffPackageB
    
    --C slot.
    elseif(pzAbiStruct.zExecutionEffPackageC == nil) then
        pzAbiStruct.zExecutionEffPackageC = zPack
        return pzAbiStruct.zExecutionEffPackageC
    end
    
    -- |[Error]|
    --Print a warning.
    io.write("fnAddAbilityBuffPack(): Warning, already has 3 effect packages on ability. Caller: " .. LM_GetCallStack(0) .. "\n")
    return nil
end