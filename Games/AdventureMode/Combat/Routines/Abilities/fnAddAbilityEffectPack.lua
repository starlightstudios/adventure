-- |[ ================================ fnAddAbilityEffectPack() ================================ ]|
--Appends an ability effect package to the provided ability structure. This is specifically designed to use
-- the global effect prototype path, so you'll need to override it if you want to have local effect scripts.
--Returns the applied package if further overrides are needed, but inherently applies it to the structure.
function fnAddAbilityEffectPack(pzAbiStruct, piStrNormal, piStrCrit, piApplyType, psApplyText, psCritApplyText, psApplyColorString, psaApplyBonus, psaApplyMalus, psaSeverityBonus, psaSeverityMalus)
    
    -- |[Argument Check]|
    if(pzAbiStruct        == nil) then return nil end
    if(piStrNormal        == nil) then return nil end
    if(piStrCrit          == nil) then return nil end
    if(piApplyType        == nil) then return nil end
    if(psApplyText        == nil) then return nil end
    if(psCritApplyText    == nil) then return nil end
    if(psApplyColorString == nil) then return nil end
    
    --Note: The apply bonus/malus/etc arrays are optional.
    
    -- |[Create Package]|
    --Construct the package with defaults.
    local zPack = fnConstructDefaultEffectPackage()
    
    --Strength and Application Type. Note that weapon type *cannot* be used here.
    zPack.iEffectStr      = piStrNormal
    zPack.iEffectCritStr  = piStrCrit
    zPack.iEffectType     = piApplyType
    
    --This assumes that a global effect is going to be created.
    zPack.sEffectPath     = gsGlobalEffectPrototypePath
    zPack.sEffectCritPath = gsGlobalEffectPrototypePath
    
    --On-apply, what string is printed and what color is it.
    zPack.sApplyText      = psApplyText
    zPack.sCritApplyText  = psCritApplyText
    zPack.sApplyTextCol   = psApplyColorString
    
    --Default resist case.
    zPack.sResistText     = "Resisted!"
    zPack.sResistTextCol  = "Color:White"
    
    -- |[Tag Handling]|
    --Initialize the tag arrays. These are completely optional to pass in, not all effects need tags.
    zPack.saApplyTagBonus    = {}
    zPack.saApplyTagMalus    = {}
    zPack.saSeverityTagBonus = {}
    zPack.saSeverityTagMalus = {}
    
    --Check the psaApplyBonus array for special strings. Only the first array is checked for these.
    if(psaApplyBonus ~= nil) then
    
        --Iterate across the array.
        for i = 1, #psaApplyBonus, 1 do
            
            --Check the standard set. This set is built in 000 Variables.lua at game start. The file
            -- that holds the lookups is Root/Lookups/Ability Standard Effect Tags.lua
            local bFoundStandard = false
            for p = 1, #gzaEffectTagModifierList, 1 do
                
                --Match: Append.
                if(psaApplyBonus[i] == gzaEffectTagModifierList[p].sTagName) then
                    bFoundStandard = true
                    table.insert(zPack.saApplyTagBonus,    gzaEffectTagModifierList[p].saApplyTagBonus)
                    table.insert(zPack.saApplyTagMalus,    gzaEffectTagModifierList[p].saApplyTagMalus)
                    table.insert(zPack.saSeverityTagBonus, gzaEffectTagModifierList[p].saSeverityTagBonus)
                    table.insert(zPack.saSeverityTagMalus, gzaEffectTagModifierList[p].saSeverityTagMalus)
                    break
                end
            end
                
            --No special cases, append.
            if(bFoundStandard == false) then
                table.insert(zPack.saApplyTagBonus, psaApplyBonus[i])
            end
        end
    end
    
    --All other tag arrays just append to their respective arrays.
    if(psaApplyMalus ~= nil) then
        for i = 1, #psaApplyMalus, 1 do
            table.insert(zPack.saApplyTagMalus, psaApplyMalus[i])
        end
    end
    if(saSeverityTagBonus ~= nil) then
        for i = 1, #saSeverityTagBonus, 1 do
            table.insert(zPack.saSeverityTagBonus, saSeverityTagBonus[i])
        end
    end
    if(saSeverityTagMalus ~= nil) then
        for i = 1, #saSeverityTagMalus, 1 do
            table.insert(zPack.saSeverityTagMalus, saSeverityTagMalus[i])
        end
    end
    
    -- |[Apply Package]|
    --No packages yet, put it in the A slot.
    if(pzAbiStruct.zExecutionEffPackage == nil) then
        pzAbiStruct.zExecutionEffPackage = zPack
        return pzAbiStruct.zExecutionEffPackage
    
    --B slot.
    elseif(pzAbiStruct.zExecutionEffPackageB == nil) then
        pzAbiStruct.zExecutionEffPackageB = zPack
        return pzAbiStruct.zExecutionEffPackageB
    
    --C slot.
    elseif(pzAbiStruct.zExecutionEffPackageC == nil) then
        pzAbiStruct.zExecutionEffPackageC = zPack
        return pzAbiStruct.zExecutionEffPackageC
    end
    
    -- |[Error]|
    --Print a warning.
    io.write("fnAddAbilityEffectPack(): Warning, already has 3 effect packages on ability. Caller: " .. LM_GetCallStack(0) .. "\n")
    return nil
end