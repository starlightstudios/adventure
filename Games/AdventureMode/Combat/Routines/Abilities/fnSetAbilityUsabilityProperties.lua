-- |[ =========================== fnSetAbilityUsabilityProperties() ============================ ]|
--Called during ability setup, sets common usability properties within an ability.
function fnSetAbilityUsabilityProperties(pzAbility, piMPCost, piCPCost, piCooldown, psTargetRoutine, piCPGeneration)
    
    -- |[Argument Check]|
    if(pzAbility       == nil) then return end
    if(piMPCost        == nil) then return end
    if(piCPCost        == nil) then return end
    if(piCooldown      == nil) then return end
    if(psTargetRoutine == nil) then return end
    if(piCPGeneration  == nil) then return end
    
    -- |[Always Available]|
    --Set this flag to false. It is typically used for debug abilities.
    pzAbility.bAlwaysAvailable = false

    -- |[Resource Costs]|
    --MP Cost. Can be zero.
    pzAbility.iRequiredMP = piMPCost
    
    --CP Cost. Can be zero.
    pzAbility.iRequiredCP = piCPCost
    
    --CP Icon setting.
    if(piCPCost < 1 or piCPCost >= 10) then
        pzAbility.sCPIcon = "Null"
    else
        pzAbility.sCPIcon = "Root/Images/AdventureUI/Abilities/Cmb" .. piCPCost
    end
    
    -- |[Cooldown and Free Action Flags]|
    --If the ability has a cooldown, set these flags. Remember: A 2-turn cooldown allows an ability to be used every-other-turn,
    -- while a 1-turn cooldown prevents a Free Action from being reused and nothing else.
    if(piCooldown > 0) then
        pzAbility.bRespectsCooldown = true
        pzAbility.iCooldown = piCooldown
    
    --No cooldown.
    else
        pzAbility.bRespectsCooldown = false
        pzAbility.iCooldown = 0
    end

    --If the ability is a free action, set these flags. Free action status should be set before this is called and be 
    -- in the pzAbility structure.
    if(pzAbility.bIsFreeAction == gbIsFreeAction) then
        pzAbility.iRequiredFreeActions = 1
        pzAbility.bRespectActionCap = true
    
    --Not a free action.
    else
        pzAbility.iRequiredFreeActions = 0
        pzAbility.bRespectActionCap = false
    end

    -- |[Targeting]|
    --Targeting routine is a human-readable string. Options are "Target Self", "Target Enemies Single", "Target Enemies All", "Target Allies Single", 
    -- "Target Allies Single Option Downed", "Target Allies Single Not Self", "Target Allies Single Downed Only", "Target Allies All", "Target All Single", 
    -- "Target Parties", "Target All", "Target Single Entity By Name Option Downed NAME1|NAME2|NAME3|", "Target Single Entity By Name NAME1|NAME2|NAME3|".
    pzAbility.sTargetMacro = psTargetRoutine

    -- |[After-Effects]|
    --Amount of CP gained from using this skill.
    pzAbility.iCPGain = piCPGeneration
end
