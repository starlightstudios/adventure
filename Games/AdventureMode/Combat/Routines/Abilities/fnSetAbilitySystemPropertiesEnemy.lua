-- |[ ========================== fnSetAbilitySystemPropertiesEnemy() =========================== ]|
--Called typically when a new ability prototype is being created, sets common properties like name, job, and
-- display name. This is used for Enemy abilities, which typically do not care about icons and the like.
-- Note that enemy abilities intended to be used with enemies who can be made to switch teams should
-- still have icons and the like, as the player can theoretically see those. Games like Witch Hunter Izana
-- never do this and can thus skip some arguments.
function fnSetAbilitySystemPropertiesEnemy(pzAbility, psJobName, psSkillName, psDisplayName, pbIsFreeAction, psResponse)
    
    -- |[Argument Check]|
    if(pzAbility      == nil) then return end
    if(psJobName      == nil) then return end
    if(psSkillName    == nil) then return end
    if(psDisplayName  == nil) then return end
    if(pbIsFreeAction == nil) then return end
    if(psResponse     == nil) then return end

    -- |[Naming]|
    --Display/System Variables
    pzAbility.sJobName   = psJobName
    pzAbility.sSkillName = psSkillName
    
    --Internal name is the name of the ability when it is an internal class ability, one that
    -- is auto-equipped when the player changes class. It is typically just the job name with
    -- " Internal" appended on the end.
    pzAbility.sInternalName = psJobName .. " Internal"
    
    --The display name can be "Use Skill Name" in which case it is identical to the skill name.
    if(psDisplayName == "Use Skill Name") then
        pzAbility.sDisplayName = psSkillName
    
    --Otherwise, override the display name.
    else
        pzAbility.sDisplayName = psDisplayName
    end
    
    -- |[Cost and Combat Display]|
    --The JP unlock cost is an integer, usually using a constant.
    pzAbility.iJPUnlockCost = 0
    
    --Mark the ability as a free action or not.
    pzAbility.bIsFreeAction = pbIsFreeAction
    
    --Icons.
    pzAbility.sIconBacking = gsAbility_Backing_Direct
    pzAbility.sIconFrame   = gsAbility_Frame_Active
    pzAbility.sIconPath    = "Root/Images/AdventureUI/Abilities/Attack"
    pzAbility.sCPIcon      = "Null"
    
    -- |[Response Type]|
    --This determines when and how the ability script gets called. Abilities that need to activate when an attack
    -- lands are different than ones that activate only when used. Passives typically activate very little.
    --When making a unique ability, manually overriding these flags may be necessary. The presets
    -- "Direct", "Passive", "Counterattack", and "Ambush" are available.
    if(psResponse == "Direct") then
        pzAbility.iResponseType = gciAbility_ResponseStandard_DirectAction
    elseif(psResponse == "Passive") then
        pzAbility.iResponseType = gciAbility_ResponseStandard_Passive
    elseif(psResponse == "Counterattack") then
        pzAbility.iResponseType = gciAbility_ResponseStandard_Counterattack
    elseif(psResponse == "Ambush") then
        pzAbility.iResponseType = gciAbility_ResponseStandard_Ambush
    end
    
    -- |[Description]|
    --Enemy abilities do not show descriptions so set that here.
    pzAbility.sDescriptionMarkdown = "No Description"
    pzAbility.sSimpleDescMarkdown = "No Description"
    
end