-- |[ =========================== fnMakeAndRegisterPassivePrototype() ========================== ]|
--Given a set of values, makes and registers a passive statmod prototype. This is similar to a buff
-- except these don't get removed if the character is KO'd and later revived.
--Deprecated.
function fnMakeAndRegisterPassivePrototype(psName, psDisplayName, psIcon, psStatString, psaDescription, pzaTagList)
    EffectList:fnCreatePassivePrototype(psName, psDisplayName, psIcon, psStatString, psaDescription, pzaTagList)
    --[=[
    
    -- |[Argument Check]|
    if(pzEffectpackage == nil) then return end
    if(psName          == nil) then return end
    if(psDisplayName   == nil) then return end
    if(psIcon          == nil) then return end
    if(psStatString    == nil) then return end
    if(psaDescription  == nil) then return end

    -- |[Tag Checking]|
    --Setup.
    local zaUseTagList = pzaTagList
    if(zaUseTagList == nil) then zaUseTagList = {} end
    
    --Scan the tag list. If "Is Positive" is not found on a buff, add it.
    fnAppendTag(zaUseTagList, "Is Positive")

    -- |[Normal Version]|
    --This effect uses a standardized StatMod script.

    --Run the standardized builder.
    local zPrototype = fnCreateStatmodEffectPrototype(psDisplayName, -1, gbIsBuff, "Root/Images/AdventureUI/Abilities/" .. psIcon, psStatString, psaDescription, true)
    fnGetApplicationDataFromEffectPackage(zPrototype, pzEffectpackage)
    
    --Passive, not removed on KO, uses different frame.
    zPrototype.sFrame      = gsAbility_Frame_Passive
    zPrototype.bRemoveOnKO = false

    --Tags.
    if(pzaTagList == nil) then
        zPrototype.zaTagList = {}
    else
        zPrototype.zaTagList = pzaTagList
    end

    -- |[Register]|
    fnRegisterEffectPrototype(psName, gciEffect_Is_StatMod, zPrototype)
    ]=]
end