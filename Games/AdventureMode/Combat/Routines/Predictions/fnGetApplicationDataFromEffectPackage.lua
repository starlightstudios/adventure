-- |[ ======================== fnGetApplicationDataFromEffectPackage() ========================= ]|
--Given an effect prototype to be placed on the global list, and an execution effect package, takes
-- the application change from the package and gives it to the prototype. This is only used if the
-- ability that owns the execution package is going to use fnCreatePredictionFromAbility() to
-- generate a prediction box, and thus needs to know the chance-to-apply for the effect.
function fnGetApplicationDataFromEffectPackage(pzPrototype, pzExecutionEffPackage)
    
    -- |[Argument Check]|
    if(pzPrototype           == nil) then return end
    if(pzExecutionEffPackage == nil) then return end
    
    -- |[Set Flags]|
    pzPrototype.iEffectStr  = pzExecutionEffPackage.iEffectStr  --Strength of effect. Higher = better application change.
    pzPrototype.iEffectType = pzExecutionEffPackage.iEffectType --Type of the effect application, such as bleed, pierce, etc.
    
    -- |[Tags]|
    --Basic tables.
    pzPrototype.saApplyTagBonus    = pzExecutionEffPackage.saApplyTagBonus
    pzPrototype.saApplyTagMalus    = pzExecutionEffPackage.saApplyTagMalus  
    pzPrototype.saSeverityTagBonus = pzExecutionEffPackage.saSeverityTagBonus
    pzPrototype.saSeverityTagMalus = pzExecutionEffPackage.saSeverityTagMalus

end