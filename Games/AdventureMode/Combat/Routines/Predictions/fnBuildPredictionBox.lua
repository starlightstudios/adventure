-- |[ ================================= fnBuildPredictionBox() ================================= ]|
--Builds a prediction box for the provided package.
function fnBuildPredictionBox(piOriginatorID, piTargetID, piClusterID, pzPredictionPack)
    
    -- |[ ==================== Argument Check ==================== ]|
    if(piOriginatorID   == nil) then return end
    if(piTargetID       == nil) then return end
    if(piClusterID      == nil) then return end
    if(pzPredictionPack == nil) then return end
    
    --Get variables.
    local iEffectModules = #pzPredictionPack.zaEffectModules
    
    --Setup
    local iAlwaysHitTags  = 0
    local iNeverHitTags   = 0
    local iAlwaysCritTags = 0
    local iNeverCritTags  = 0
    local iCritDamageBonusTags = 0
    
    --Tag Structure
    local zaTags = fnCreateTagStruct()
    
    --Determine if Tourist Mode is active. Tourist mode makes all attacks hit, all effects apply,
    -- and damage is tripled.
    local bIsTouristMode     = AdvCombat_GetProperty("Is Tourist Mode")
    local iPartyOfOriginator = AdvCombat_GetProperty("Party Of ID", piOriginatorID)
    local iPartyOfTarget     = AdvCombat_GetProperty("Party Of ID", piTargetID)
    if(iPartyOfOriginator ~= gciACPartyGroup_Party or iPartyOfTarget ~= gciACPartyGroup_Enemy) then bIsTouristMode = false end
    
    --Diagnostics.
    local bDiagnostics = Debug_GetFlag("Predictions: All") or Debug_GetFlag("Predictions: Build Prediction Box")
    Debug_PushPrint(bDiagnostics, "fnBuildPredictionBox() - Beginning.\n")
    Debug_Print("System data report: \n")
    Debug_Print(" Originator ID:       " .. piOriginatorID .. "\n")
    Debug_Print(" Target ID:           " .. piTargetID .. "\n")
    Debug_Print(" Cluster ID:          " .. piClusterID .. "\n")
    Debug_Print(" Party of Originator: " .. iPartyOfOriginator .. "\n")
    Debug_Print(" Party of Target:     " .. iPartyOfTarget .. "\n")
    
    -- |[ ================= Originator Variables ================= ]|
    --Diagnostics.
    Debug_Print("Originator Report:\n")
    
    --Push.
    AdvCombat_SetProperty("Push Entity By ID", piOriginatorID)
    
        -- |[General Stats]|
        --These stats are used by multiple modules.
        local iOriginatorPower = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
        Debug_Print(" Power: " .. iOriginatorPower .. "\n")
    
        -- |[Tag Handling]|
        --Special tags. Often ability specific.
        AdvCombat_SetProperty("Push Entity By ID", piTargetID) 
        
            --Florentina ability tags. Target is always-hit, always-crit. Counts as one always-hit and always-crit.
            local iCalledShotCount = AdvCombatEntity_GetProperty("Tag Count", "Florentina Called Shot")
            local iCalledShotCounter = fnGetTagCountInTable("Florentina No Called Shot", pzPredictionPack.zaTags)
            if(iCalledShotCount > 0 and iCalledShotCounter == 0) then
                iAlwaysHitTags  = iAlwaysHitTags  + iCalledShotCount
                iAlwaysCritTags = iAlwaysCritTags + iCalledShotCount
            end
            
            --Generic Tags
            iAlwaysHitTags  = iAlwaysHitTags  + AdvCombatEntity_GetProperty("Tag Count", "Always Hit") + AdvCombatEntity_GetProperty("Tag Count", "Always Hit Consume")
            iNeverHitTags   = iNeverHitTags   + AdvCombatEntity_GetProperty("Tag Count", "Never Hit")
            iAlwaysCritTags = iAlwaysCritTags + AdvCombatEntity_GetProperty("Tag Count", "Always Crit")
            iNeverCritTags  = iNeverCritTags  + AdvCombatEntity_GetProperty("Tag Count", "Never Crit")
            
        DL_PopActiveObject()
    
        -- |[Hit-rate Module]|
        if(pzPredictionPack.bHasHitRateModule) then
            
            --Flags.
            pzPredictionPack.zHitRateModule.iAccuracyBonus = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Accuracy)
            iAlwaysHitTags  = iAlwaysHitTags  + AdvCombatEntity_GetProperty("Tag Count", "User Always Hit")
            iNeverHitTags   = iNeverHitTags   + AdvCombatEntity_GetProperty("Tag Count", "User Never Hit")
            iAlwaysCritTags = iAlwaysCritTags + AdvCombatEntity_GetProperty("Tag Count", "User Always Crit") + AdvCombatEntity_GetProperty("Tag Count", "User Attack Crits") + AdvCombatEntity_GetProperty("Tag Count", "User Attack Crits Consume")
            iNeverCritTags  = iNeverCritTags  + AdvCombatEntity_GetProperty("Tag Count", "User Never Crit")
            
            --Diagnostics.
            Debug_Print(" Acc Bonus: " .. pzPredictionPack.zHitRateModule.iAccuracyBonus .. "\n")
            Debug_Print(" Always-Hit Tags: " .. iAlwaysHitTags .. "\n")
            Debug_Print(" Never-Hit Tags: " .. iNeverHitTags .. "\n")
            Debug_Print(" Always-Crit Tags: " .. iAlwaysCritTags .. "\n")
            Debug_Print(" Never-Crit Tags: " .. iNeverCritTags .. "\n")
        end
        
        -- |[Damage Module]|
        if(pzPredictionPack.bHasDamageModule) then
            zaTags.iBleedDamageDealtUpTags = AdvCombatEntity_GetProperty("Tag Count", "Bleed Damage Dealt +")
            zaTags.iBleedDamageDealtDnTags = AdvCombatEntity_GetProperty("Tag Count", "Bleed Damage Dealt -")
        end
    
        -- |[Critical Hit Module]|
        if(pzPredictionPack.bHasCritModule) then
            pzPredictionPack.zCritModule.iAccuracyBonus = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Accuracy)
            iCritDamageBonusTags = iCritDamageBonusTags + AdvCombatEntity_GetProperty("Tag Count", "Critical Damage Bonus")
            Debug_Print(" Acc Bonus (Crit): " .. pzPredictionPack.zCritModule.iAccuracyBonus .. "\n")
            Debug_Print(" Crit Damage Bonus: " .. iCritDamageBonusTags .. "\n")
        end
    
        -- |[Effect Modules]|
        --Diagnostics.
        Debug_Print(" Effect Modules: " .. iEffectModules .. "\n")
        
        --Effect Modules. DoTs need to store the user's attack power.
        for i = 1, iEffectModules, 1 do
            Debug_Print("  Module: " .. i .. "\n")
            if(pzPredictionPack.zaEffectModules[i].bIsDamageOverTime) then
                Debug_Print("  Is a Damage-Over-Time.\n")
                pzPredictionPack.zaEffectModules[i].iUserAttackPower = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
            end
            if(pzPredictionPack.zaEffectModules[i].bIsHealOverTime) then
                Debug_Print("  Is a Heal-Over-Time.\n")
                pzPredictionPack.zaEffectModules[i].fHotComputed = pzPredictionPack.zaEffectModules[i].fHotFixedValue + (pzPredictionPack.zaEffectModules[i].fHotAttackFactor * AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack))
            end
            
            --Collect bonuses for item power.
            pzPredictionPack.zaEffectModules[i].fItemPowerBonus = 1.0
            if(pzPredictionPack.zaEffectModules[i].bBenefitsFromItemPower == true) then
                Debug_Print("  Benefits from item power.\n")
                local iItemPowerBonus = AdvCombatEntity_GetProperty("Tag Count", "Item Power +")
                local iItemPowerMalus = AdvCombatEntity_GetProperty("Tag Count", "Item Power -")
                pzPredictionPack.zaEffectModules[i].fItemPowerBonus = 1.00 + (0.01 * (iItemPowerBonus - iItemPowerMalus))
                Debug_Print("  Item power bonus: " .. iItemPowerBonus .. "\n")
                Debug_Print("  Item power malus: " .. iItemPowerMalus .. "\n")
                Debug_Print("  Item power final: " .. pzPredictionPack.zaEffectModules[i].fItemPowerBonus  .. "\n")
            end
        end
    DL_PopActiveObject()
    
    -- |[ =================== Target Variables =================== ]|
    --Diagnostics.
    Debug_Print("Originator Report:\n")
    
    --Push.
    AdvCombat_SetProperty("Push Entity By ID", piTargetID)
    
        -- |[General]|
        --If this target is stunned, store that here.
        local bIsStunned = AdvCombatEntity_GetProperty("Is Stunned")
        if(bIsStunned) then
            Debug_Print(" Target is stunned.\n")
        end
    
        -- |[Hit-Rate Module]|
        if(pzPredictionPack.bHasHitRateModule) then
            pzPredictionPack.zHitRateModule.iAccuracyMalus = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Evade)
            Debug_Print(" Evade Rate: " .. pzPredictionPack.zHitRateModule.iAccuracyMalus .. "\n")
        end
        
        -- |[Damage Module]|
        if(pzPredictionPack.bHasDamageModule) then
            zaTags.iBleedDamageTakenUpTags = AdvCombatEntity_GetProperty("Tag Count", "Bleed Damage Taken +")
            zaTags.iBleedDamageTakenDnTags = AdvCombatEntity_GetProperty("Tag Count", "Bleed Damage Taken -")
        end
        
        -- |[Stun Module]|
        if(pzPredictionPack.bHasStunModule) then
            --Tags here
        end
    
        -- |[Healing Module]|
        if(pzPredictionPack.bHasHealingModule) then
            
            --Fast-access.
            local zModule = pzPredictionPack.zHealingModule
            
            --Get HP/Power
            local iHPMax = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
            
            --Compute healing.
            zModule.iHealingFinal = zModule.iHealingFixed + (iHPMax * zModule.fHealingPercent) + (iOriginatorPower * zModule.fHealingFactor)
            zModule.iHealingFinal = math.floor(zModule.iHealingFinal)
            
            --Gains a bonus from healing power:
            if(zModule.bHealingBenefitsFromItemPower) then
                AdvCombat_SetProperty("Push Entity By ID", piOriginatorID)
                    local iItemPowerBonus = AdvCombatEntity_GetProperty("Tag Count", "Item Power +")
                    local iItemPowerMalus = AdvCombatEntity_GetProperty("Tag Count", "Item Power -")
                    local fHealingFactor = 1.00 + (0.01 * (iItemPowerBonus - iItemPowerMalus))
                    zModule.iHealingFinal = math.floor(zModule.iHealingFinal * fHealingFactor)
                DL_PopActiveObject()
            end
            
            --Report.
            Debug_Print(" Final healing: " .. zModule.iHealingFinal .. "\n")
        end
        
        -- |[Shields Module]|
        if(pzPredictionPack.bHasShieldModule) then
            pzPredictionPack.zShieldModule.iShieldFinal = pzPredictionPack.zShieldModule.iShieldFixed + (pzPredictionPack.zShieldModule.iShieldFactor * iOriginatorPower)
            pzPredictionPack.zShieldModule.iShieldFinal = math.floor(pzPredictionPack.zShieldModule.iShieldFinal)
            Debug_Print(" Final shields: " .. pzPredictionPack.zShieldModule.iShieldFinal .. "\n")
        end
        
        -- |[Critical Hit Module]|
        if(pzPredictionPack.bHasCritModule) then
            pzPredictionPack.zCritModule.iAccuracyMalus = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Evade)
            Debug_Print(" Evade (Crit): " .. pzPredictionPack.zCritModule.iAccuracyMalus .. "\n")
        end
        
        -- |[Effect Modules]|
        --Diagnostics.
        Debug_Print(" Effect Modules: " .. iEffectModules .. "\n")
        
        --Iterate.
        for i = 1, iEffectModules, 1 do
        
            --DoTs can compute damage with the target's resistance.
            if(pzPredictionPack.zaEffectModules[i].bIsDamageOverTime) then
                
                --If flagged to use an item bonus, get that here.
                local fItemPowerFactor = 1.00
                if(pzPredictionPack.zaEffectModules[i].bBenefitsFromItemPower) then
                    AdvCombat_SetProperty("Push Entity By ID", piOriginatorID)
                        local iItemPowerBonus = AdvCombatEntity_GetProperty("Tag Count", "Item Power +")
                        local iItemPowerMalus = AdvCombatEntity_GetProperty("Tag Count", "Item Power -")
                        fItemPowerFactor = 1.00 + (0.01 * (iItemPowerBonus - iItemPowerMalus))
                    DL_PopActiveObject()
                end
                
                --Get resistance, compute total damage, store it.
                local iResistance = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Resist_Start + pzPredictionPack.zaEffectModules[i].iDoTDamageType)
                local iAttackPower = pzPredictionPack.zaEffectModules[i].iUserAttackPower * fItemPowerFactor
                local iBaseDamageTotal = math.floor(fnComputeCombatDamage(iAttackPower, 0, iResistance))
                
                --Modify by bleed bonus.
                local iBaseDamageAfterBonuses = iBaseDamageTotal
                if(pzPredictionPack.zaEffectModules[i].iDoTDamageType == gciDamageType_Bleeding) then
                    local fBonusPct = 0.01 * (zaTags.iBleedDamageTakenUpTags + zaTags.iBleedDamageDealtUpTags - zaTags.iBleedDamageTakenDnTags - zaTags.iBleedDamageDealtDnTags)
                    iBaseDamageAfterBonuses = math.floor(iBaseDamageTotal * (1.0 + fBonusPct))
                end
                
                --Compute final damage-per-turn.
                pzPredictionPack.zaEffectModules[i].iDamageTotal = math.floor(iBaseDamageAfterBonuses * pzPredictionPack.zaEffectModules[i].fDotAttackFactor)
                
                --Debug.
                if(false) then
                    io.write("Begin DoT Prediction.\n")
                    io.write(" Item power: "   .. fItemPowerFactor .. "\n")
                    io.write(" Resistance: "   .. iResistance .. "\n")
                    io.write(" Attack Power: " .. iAttackPower .. "\n")
                    io.write(" Factor: "       .. pzPredictionPack.zaEffectModules[i].fDotAttackFactor .. "\n")
                    io.write(" Damage Base: "  .. iBaseDamageTotal .. "\n")
                    io.write(" Damage After Bonuses: " .. iBaseDamageAfterBonuses .. "\n")
                    io.write(" Damage per turn: " .. pzPredictionPack.zaEffectModules[i].iDamageTotal .. "\n")
                end
            end
            
            --Collect tag bonuses for chance-to-apply.
            local fApplyBonus = fnComputeTagBonus(pzPredictionPack.zaEffectModules[i].saApplyTagBonus, pzPredictionPack.zaEffectModules[i].fApplyFactor)
            local fApplyMalus = fnComputeTagBonus(pzPredictionPack.zaEffectModules[i].saApplyTagMalus, pzPredictionPack.zaEffectModules[i].fApplyFactor)
            pzPredictionPack.zaEffectModules[i].fApplyBonus = fApplyBonus - fApplyMalus
            
            --Collect tag bonuses for severity.
            local fSeverityBonus = fnComputeTagBonus(pzPredictionPack.zaEffectModules[i].saSeverityTagBonus, pzPredictionPack.zaEffectModules[i].fSeverityFactor)
            local fSeverityMalus = fnComputeTagBonus(pzPredictionPack.zaEffectModules[i].saSeverityTagMalus, pzPredictionPack.zaEffectModules[i].fSeverityFactor)
            pzPredictionPack.zaEffectModules[i].fSeverityBonus = 1.0 + fSeverityBonus - fSeverityMalus
        
            --Report.
            Debug_Print("  Module: " .. i .. "\n")
            Debug_Print("   Apply Bonus: " .. fApplyBonus .. "\n")
            Debug_Print("   Apply Malus: " .. fApplyMalus .. "\n")
            Debug_Print("   Apply Final: " .. pzPredictionPack.zaEffectModules[i].fApplyBonus .. "\n")
            Debug_Print("   Severity Bonus: " .. fSeverityBonus .. "\n")
            Debug_Print("   Severity Malus: " .. fSeverityMalus .. "\n")
            Debug_Print("   Severity Final: " .. pzPredictionPack.zaEffectModules[i].fSeverityBonus .. "\n")
        
        end
        
        -- |[Chaser Modules]|
        --Damage bonus.
        local iDamageBonusFromChasers = 0.0
        local fDamageFactorFromChasers = 0.0
        
        --If there are no chasers, skip this.
        if(pzPredictionPack.bHasChaserModules == true) then
            
            --Diagnostics.
            Debug_Print(" Chaser Modules: " .. pzPredictionPack.iChaserModulesTotal .. "\n")
            
            --Iterate across all chasers.
            for i = 1, pzPredictionPack.iChaserModulesTotal, 1 do
            
                --Shorthand.
                local zModule = pzPredictionPack.zaChaserModules[i]
            
                --Check how many times the dot tag appears on the enemy.
                local iDotTagCount = AdvCombatEntity_GetProperty("Tag Count", zModule.sDoTTag)
            
                --Modify the damage factor used by the damage module for each tag found.
                fDamageFactorFromChasers = fDamageFactorFromChasers + (zModule.fDamageFactorChangePerDoT * iDotTagCount)
            
                --If the damage remaining factor is not zero, add a part of the damage remaining to the damage output.
                if(zModule.fDamageRemainingFactor ~= 0.0) then
                    
                    --Iterate across all effects:
                    local iEffectsTotal = AdvCombatEntity_GetProperty("Effects With Tag", zModule.sDoTTag)
                    for p = 0, iEffectsTotal-1, 1 do
                    
                        --Get the script this effect uses.
                        local iEffectID = AdvCombatEntity_GetProperty("Effect ID With Tag", zModule.sDoTTag, p)
                        AdvCombat_SetProperty("Push Effect", iEffectID)
                            local sEffectScript = AdvCombatEffect_GetProperty("Script")
                            local sEffectPrototypeName = AdvCombatEffect_GetProperty("Global Prototype Name")
                        DL_PopActiveObject()
                        
                        --If the effect prototype exists, mark that.
                        if(sEffectPrototypeName ~= "Null") then
                            AdvCombatEffect_SetProperty("Static Prototype Name", sEffectPrototypeName)
                        end
                        
                        --Call it with the compute DoT code.
                        giEffect_LastComputedDotDamage = 0
                        LM_ExecuteScript(sEffectScript, gciEffect_DotComputeDamage, iEffectID)
                        
                        --Add the damage by its factor.
                        iDamageBonusFromChasers = iDamageBonusFromChasers + math.floor(giEffect_LastComputedDotDamage * zModule.fDamageRemainingFactor)
                        
                        --Clean.
                        AdvCombatEffect_SetProperty("Static Prototype Name", "Null")
                    end
                end
            end
        end
    DL_PopActiveObject()
    
    -- |[ ======================= Creation ======================= ]|
    --Create a box for this target.
    local sBoxName = "Prd" .. piClusterID .. "x" .. piTargetID
    AdvCombat_SetProperty("Prediction Create Box",  sBoxName)
    AdvCombat_SetProperty("Prediction Set Host",    sBoxName, piTargetID)
    
    --Determine which slot this entity is in in combat.
    local bIsEven = false
    if(iPartyOfTarget == gciACPartyGroup_Party) then
    
        --Scan.
        local iPartyTotal = AdvCombat_GetProperty("Combat Party Size")
        for i = 0, iPartyTotal-1, 1 do
            
            --Found match.
            local iPartyID = AdvCombat_GetProperty("Combat Party ID", i)
            if(iPartyID == piTargetID) then
                if(i % 2 == 0) then
                    bIsEven = true
                end
                break
            end
        end
        
    --Enemy party.
    elseif(iPartyOfTarget == gciACPartyGroup_Enemy) then
    
        --Scan.
        local iEnemiesTotal = AdvCombat_GetProperty("Enemy Party Size")
        for i = 0, iEnemiesTotal-1, 1 do
            
            --Found match.
            local iEnemyID = AdvCombat_GetProperty("Enemy ID", i)
            if(iEnemyID == piTargetID) then
                if(i % 2 == 0) then
                    bIsEven = true
                end
                break
            end
        end
    end
    
    --Diagnostics.
    Debug_Print("Box Creation Report:\n")
    Debug_Print(" Prediction box name: " .. sBoxName .. "\n")
    
    -- |[ ============ Line Counting And Computations ============ ]|
    --Each module needs a line. Count them out here.
    local iTotalLines = 0
    local fHitMultiplier = 1.0
    
    --Diagnostics.
    Debug_Print("Begin line counting:\n")
    
    -- |[ ======== Hit-Rate ======== ]|
    --Compute hit-rate if needed.
    if(pzPredictionPack.bHasHitRateModule == true) then
        
        --Report.
        Debug_Print(" Hit-rate module.\n")
        
        --Compute.
        iTotalLines = iTotalLines + 1
        pzPredictionPack.zHitRateModule.iHitRate = fnComputeHitRate(pzPredictionPack.zHitRateModule.iAccuracyBonus, pzPredictionPack.zHitRateModule.iAccuracyMalus, pzPredictionPack.zHitRateModule.iMissRate)
        
        --Tag Handling.
        if(iAlwaysHitTags > iNeverHitTags) then
            pzPredictionPack.zHitRateModule.iHitRate = 100
        elseif(iNeverHitTags > iAlwaysHitTags) then
            pzPredictionPack.zHitRateModule.iHitRate = 0
        end
        
        --An always-crit guarantees a hit as well.
        if(iAlwaysCritTags > iNeverCritTags) then
            pzPredictionPack.zHitRateModule.iHitRate = 100
        end
        
        --Tourist mode causes an attack to always hit.
        if(bIsTouristMode) then
            pzPredictionPack.zHitRateModule.iHitRate = 100
        end
        
        --A stunned target has a 100% hit rate, even if the never-hit tags are in play.
        if(bIsStunned) then
            pzPredictionPack.zHitRateModule.iHitRate = 100
        end
        
        --Flag.
        fHitMultiplier = pzPredictionPack.zHitRateModule.iHitRate / 100.0
        if(fHitMultiplier < 0.0) then fHitMultiplier = 0.0 end
        if(fHitMultiplier > 1.0) then fHitMultiplier = 1.0 end
        
        --Special: Hit rate is between 0 and -25. Glances only.
        if(pzPredictionPack.zHitRateModule.iHitRate < 1 and pzPredictionPack.zHitRateModule.iHitRate > -25) then
            AdvCombat_SetProperty("Prediction Allocate Strings",        sBoxName, 1)
            AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, 0, Translate(gsTranslationCombat, "[IMG0]Accuracy: Glancing Blows Only"))
            AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, 0, 1)
            AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, 0, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Accuracy")
            Debug_PopPrint("fnBuildPredictionBox() - Completed, glance-only case.\n")
            return
        
        --Guaranteed miss:
        elseif(pzPredictionPack.zHitRateModule.iHitRate <= -25) then
            AdvCombat_SetProperty("Prediction Allocate Strings",        sBoxName, 1)
            AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, 0, Translate(gsTranslationCombat, "[IMG0]Accuracy: Guaranteed Miss"))
            AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, 0, 1)
            AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, 0, 0, gcfAbilityImgOffsetY, "Root/Images/AdventureUI/StatisticIcons/Accuracy")
            Debug_PopPrint("fnBuildPredictionBox() - Completed, guaranteed miss case.\n")
            return
        end
    end
    
    -- |[ ========= Damage ========= ]|
    --Show damage if needed. Note that tags are not used for this computation. The subroutines already handle basic tags.
    -- The tag bonuses are only needed for Effect damage computation.
    if(pzPredictionPack.bHasDamageModule == true) then
        
        -- |[Setup]|
        --Line add.
        iTotalLines = iTotalLines + 1
        
        --Report.
        Debug_Print(" Damage module.\n")
        
        --Variables.
        local fDamageFactor = pzPredictionPack.zDamageModule.fDamageFactor
        
        -- |[Weapon-Damage]|
        --If we're flagged to use the weapon:
        if(pzPredictionPack.zDamageModule.bUseWeaponType == true) then
            pzPredictionPack.zDamageModule.sDamageIco = fnBuildWeaponDamageIcons(piOriginatorID)
            local iLoDamage, iHiDamage = fnComputeDamageRangeWeapon(piOriginatorID, piTargetID, pzPredictionPack.zDamageModule.iScatterRange, pzPredictionPack.zDamageModule.bBypassProtection, pzPredictionPack.zDamageModule.iPenetration)
            pzPredictionPack.zDamageModule.iDamageLo = iLoDamage
            pzPredictionPack.zDamageModule.iDamageHi = iHiDamage
        
        -- |[Preset Damage Type]|
        --Otherwise, single damage type:
        else
            pzPredictionPack.zDamageModule.sDamageIco[1] = fnGetIconByDamageType(pzPredictionPack.zDamageModule.iDamageType)
            local iLoDamage, iHiDamage = fnComputeDamageRangeTypeID(piOriginatorID, piTargetID, pzPredictionPack.zDamageModule.iDamageType, pzPredictionPack.zDamageModule.iScatterRange, pzPredictionPack.zDamageModule.bBypassProtection, pzPredictionPack.zDamageModule.iPenetration)
            pzPredictionPack.zDamageModule.iDamageLo = iLoDamage
            pzPredictionPack.zDamageModule.iDamageHi = iHiDamage
        end
    
        -- |[Item Power Boost]|
        --If the item receives a bonus from items, compute this and add it to fItemPowerFactor.
        if(pzPredictionPack.zDamageModule.bBenefitsFromItemPower) then
            
            --Get the item power factor.
            AdvCombat_SetProperty("Push Entity By ID", piOriginatorID)
                local iItemPowerBonus = AdvCombatEntity_GetProperty("Tag Count", "Item Power +")
                local iItemPowerMalus = AdvCombatEntity_GetProperty("Tag Count", "Item Power -")
                local fItemPowerFactor =  1.00 + (0.01 * (iItemPowerBonus - iItemPowerMalus))
            DL_PopActiveObject()
            
            --Multiply the damage factor by the item power factor.
            fDamageFactor = fDamageFactor * fItemPowerFactor
        end
        
        -- |[Scale By Damage Factor]|
        --Modify by damage percentage.
        local iUseFactor = fDamageFactor + fDamageFactorFromChasers
        pzPredictionPack.zDamageModule.iDamageLo = math.floor(pzPredictionPack.zDamageModule.iDamageLo * iUseFactor)
        pzPredictionPack.zDamageModule.iDamageHi = math.floor(pzPredictionPack.zDamageModule.iDamageHi * iUseFactor)
        
        -- |[Tourist Mode]|
        --Tourist mode triples damage output.
        if(bIsTouristMode) then
            pzPredictionPack.zDamageModule.iDamageLo = pzPredictionPack.zDamageModule.iDamageLo * 3.0
            pzPredictionPack.zDamageModule.iDamageHi = pzPredictionPack.zDamageModule.iDamageHi * 3.0
        end
        
        -- |[Chasers]|
        --Append damage bonus from chasers.
        pzPredictionPack.zDamageModule.iDamageLo = pzPredictionPack.zDamageModule.iDamageLo + iDamageBonusFromChasers
        pzPredictionPack.zDamageModule.iDamageHi = pzPredictionPack.zDamageModule.iDamageHi + iDamageBonusFromChasers
    end

    -- |[ ===== Minor Effects ====== ]|
    -- |[Stun Module]|
    if(pzPredictionPack.bHasStunModule) then
        iTotalLines = iTotalLines + 1
        Debug_Print(" Stun module.\n")
    end
    
    -- |[Healing]|
    if(pzPredictionPack.bHasHealingModule == true) then
        
        --Must be at least one point of healing:
        if(pzPredictionPack.zHealingModule.iHealingFinal > 0) then
            iTotalLines = iTotalLines + 1
        end
        
        --Report.
        Debug_Print(" Healing module.\n")
    end
    
    -- |[Shields]|
    if(pzPredictionPack.bHasShieldModule == true) then
        
        --Must be at least one point of shields:
        if(pzPredictionPack.zShieldModule.iShieldFinal > 0) then
            iTotalLines = iTotalLines + 1
        end
        
        --Report.
        Debug_Print(" Shield module.\n")
    end
    
    -- |[MP Generation]|
    if(pzPredictionPack.bHasMPGenerationModule == true) then
        iTotalLines = iTotalLines + 1
        
        --Report.
        Debug_Print(" MP-Generation module.\n")
    end
    
    -- |[ ===== Critical Hits ====== ]|
    --Compute critical strike if needed.
    local bLockoutCritModule = false
    if(pzPredictionPack.bHasCritModule == true) then
        
        --Report.
        Debug_Print(" Critical-hit module.\n")
        
        --Compute.
        iTotalLines = iTotalLines + 1
        pzPredictionPack.zCritModule.iCritRate = fnComputeCritRate(pzPredictionPack.zCritModule.iAccuracyBonus, pzPredictionPack.zCritModule.iAccuracyMalus, pzPredictionPack.zCritModule.iCritThreshold)
        
        --Tag Handling.
        if(iAlwaysCritTags > iNeverCritTags) then
            pzPredictionPack.zCritModule.iCritRate = 100
        elseif(iNeverCritTags > iAlwaysCritTags) then
            pzPredictionPack.zCritModule.iCritRate = 0
        end
        
        --If the crit rate comes back as zero, this package doesn't register.
        if(pzPredictionPack.zCritModule.iCritRate < 1) then
            iTotalLines = iTotalLines - 1
            bLockoutCritModule = true
        end
    end
    
    -- |[ ======== Effects ========= ]|
    -- |[Effect Level Bonus]|
    --Compute the bonus imparted to player-party-only effect application chance.
    local iEffectBonus = 0
    if(iPartyOfOriginator == gciACPartyGroup_Party) then
        AdvCombat_SetProperty("Push Entity By ID", piOriginatorID)
            local iLevel = AdvCombatEntity_GetProperty("Level")
            iEffectBonus = math.floor(iLevel * gciEffectPowerPerLevel)
        DL_PopActiveObject()
    end
    
    -- |[Effect Modules]|
    --For each effect module...
    for i = 1, iEffectModules, 1 do
        
        iTotalLines = iTotalLines + 1
        pzPredictionPack.zaEffectModules[i].iEffectApplyRate = fnComputeEffectApplyRateID(piTargetID, pzPredictionPack.zaEffectModules[i].iEffectStrength, pzPredictionPack.zaEffectModules[i].iEffectType)
        pzPredictionPack.zaEffectModules[i].iEffectApplyRate = pzPredictionPack.zaEffectModules[i].iEffectApplyRate + pzPredictionPack.zaEffectModules[i].fApplyBonus
        
        --Add the bonus, which applies only to party members.
        pzPredictionPack.zaEffectModules[i].iEffectApplyRate = pzPredictionPack.zaEffectModules[i].iEffectApplyRate + iEffectBonus
        
        --Apply rate is multiplied by hit rate as well.
        pzPredictionPack.zaEffectModules[i].iEffectApplyRate = pzPredictionPack.zaEffectModules[i].iEffectApplyRate * fHitMultiplier
        
        --Clamp:
        if(pzPredictionPack.zaEffectModules[i].iEffectApplyRate > 100) then pzPredictionPack.zaEffectModules[i].iEffectApplyRate = 100 end
        if(pzPredictionPack.zaEffectModules[i].iEffectApplyRate <   0) then pzPredictionPack.zaEffectModules[i].iEffectApplyRate =   0 end
        
        --Application rates in tourist mode are 100%, even against an immune target.
        if(bIsTouristMode and pzPredictionPack.bTouristDoesntAffectEffects == false) then 
            pzPredictionPack.zaEffectModules[i].iEffectApplyRate = 100
        end
        
        --HoTs always hit.
        if(pzPredictionPack.zaEffectModules[i].bIsHealOverTime) then
            pzPredictionPack.zaEffectModules[i].iEffectApplyRate = 100
        end
        
        --If flagged, always hits.
        if(pzPredictionPack.zaEffectModules[i].bIsBuff) then
            pzPredictionPack.zaEffectModules[i].iEffectApplyRate = 100
        end
        
        --If the apply rate is zero, remove the line.
        if(pzPredictionPack.zaEffectModules[i].iEffectApplyRate < 1) then
        
        --If the effect is a damage-over-time, and the damage came out as zero, skip the line.
        elseif(pzPredictionPack.zaEffectModules[i].bIsDamageOverTime and pzPredictionPack.zaEffectModules[i].iDamageTotal < 1) then
            pzPredictionPack.zaEffectModules[i].iEffectApplyRate = 0
            iTotalLines = iTotalLines - 1
        end
    end
        
    -- |[ ======== Chasers ========= ]|
    --Add one line for each chaser module, even if there are no matching tags. This only happens if the module
    -- is flagged to consume the effects in question.
    if(pzPredictionPack.bHasChaserModules == true) then
        
        --Iterate across all chasers.
        for i = 1, pzPredictionPack.iChaserModulesTotal, 1 do
        
            --Shorthand.
            local zModule = pzPredictionPack.zaChaserModules[i]
            
            --If this chaser consumes effects:
            if(zModule.bConsumeEffect == true) then
                iTotalLines = iTotalLines + 1
            end
        end
    end
    
    -- |[Additional Lines]|
    iTotalLines = iTotalLines + #pzPredictionPack.zaAdditionalLines
    
    --Diagnostics.
    Debug_Print(" Expected lines: " .. iTotalLines .. "\n")
    
    -- |[ ===================== Line Assembly ==================== ]|
    -- |[Allocate Lines]|
    if(iTotalLines < 1) then
        
        --If there are zero lines, and there is at least one effect module, the target is immune.
        if(iEffectModules > 0) then
            AdvCombat_SetProperty("Prediction Allocate Strings",        sBoxName, 1)
            AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, 0, Translate(gsTranslationCombat, "Immune!"))
            AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, 0, 0)
        end
        
        --Debug.
        Debug_PopPrint("fnBuildPredictionBox() - Completed, zero-line case.\n")
        return 
    end
    
    --Diagnostics.
    Debug_Print("Begin line assembly:\n")
    
    --Setup.
    local i = 0
    AdvCombat_SetProperty("Prediction Allocate Strings", sBoxName, iTotalLines)
    
    --Box offset. Reset the box height based on our best-guess height for the box, assuming all targets have the same height.
    -- If there is only one target, don't bother.
    local iClusterTargetsTotal = AdvCombat_GetProperty("Prediction Get Targets Total")
    if(bIsEven == false and iClusterTargetsTotal > 1) then
        AdvCombat_SetProperty("Prediction Set Offsets", sBoxName, 0, ((iTotalLines * 21) + 15) * -1)
        giPredictionBoxIsEven = true
    else
        AdvCombat_SetProperty("Prediction Set Offsets", sBoxName, 0, 0)
        giPredictionBoxIsEven = false
    end
    
    -- |[Hit Rate]|
    --Show the hit chance.
    if(pzPredictionPack.bHasHitRateModule == true) then
        AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, i, Translate(gsTranslationCombat, "[IMG0][MOV25]Accuracy: ") .. pzPredictionPack.zHitRateModule.iHitRate .. "%%")
        AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, i, 1)
        AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, i, 0, gcfAbilityImgOffsetPredY, "Root/Images/AdventureUI/StatisticIcons/Accuracy")
        i = i + 1
    end
    
    -- |[Damage]|
    --Show damage if needed.
    if(pzPredictionPack.bHasDamageModule == true) then
        
        --Build a string.
        local iMembers = #pzPredictionPack.zDamageModule.sDamageIco
        local sString = Translate(gsTranslationCombat, "[IMG0][MOV26]Damage: ") .. math.floor(pzPredictionPack.zDamageModule.iDamageLo) .. "-" .. math.floor(pzPredictionPack.zDamageModule.iDamageHi)
        
        --Totally immune to damage:
        if(pzPredictionPack.zDamageModule.iDamageLo == 0.0 and pzPredictionPack.zDamageModule.iDamageHi == 0.0) then
            sString = Translate(gsTranslationCombat, "[IMG0][MOV26]Immune to: ")
        end
            
            --Add on the icon spots for each member of the array.
            for p = 1, iMembers, 1 do
                sString = sString .. "[IMG" .. p .. "]"
            end
            
            --Now place the string and build the icons.
            AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, i, sString)
            AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, i, 1+iMembers)
            AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, i, 0, gcfAbilityImgOffsetPredY, "Root/Images/AdventureUI/StatisticIcons/Attack")
            for p = 1, iMembers, 1 do
                AdvCombat_SetProperty("Prediction Set String Image", sBoxName, i, p, gcfAbilityImgOffsetPredY, pzPredictionPack.zDamageModule.sDamageIco[p])
            end
        
        --Next.
        i = i + 1
    end

    -- |[Stun]|
    --Stun Module
    if(pzPredictionPack.bHasStunModule) then
    
        --Compute expected stun damage.
        local iExpectedStun = fnComputeStunID(pzPredictionPack.zStunModule.iStunBase, piTargetID)
    
        --Lines.
        AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, i, Translate(gsTranslationCombat, "[IMG0]Stun: ") .. iExpectedStun)
        AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, i, 1)
        AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, i, 0, gcfAbilityImgOffsetPredY, "Root/Images/AdventureUI/DebuffIcons/Stun")
    
        --Next.
        i = i + 1
    end
    
    -- |[Healing]|
    --Show healing if needed.
    if(pzPredictionPack.bHasHealingModule == true) then
        
        --Must be at least one point of healing:
        if(pzPredictionPack.zHealingModule.iHealingFinal > 0) then
        
            --Lines.
            AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, i, Translate(gsTranslationCombat, "[IMG0]Healing: ") .. pzPredictionPack.zHealingModule.iHealingFinal)
            AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, i, 1)
            AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, i, 0, gcfAbilityImgOffsetPredY+2, "Root/Images/AdventureUI/StatisticIcons/Health")
        
            --Next.
            i = i + 1
        end
    end
    
    -- |[Shields]|
    --Show shields applied to target.
    if(pzPredictionPack.bHasShieldModule == true) then
        
        --Must be at least one point of shields:
        if(pzPredictionPack.zShieldModule.iShieldFinal > 0) then
        
            --Lines.
            AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, i, Translate(gsTranslationCombat, "[IMG0]Shields: ") .. pzPredictionPack.zShieldModule.iShieldFinal)
            AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, i, 1)
            AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, i, 0, gcfAbilityImgOffsetPredY, "Root/Images/AdventureUI/StatisticIcons/Shields")
        
            --Next.
            i = i + 1
        end
    end
    
    -- |[MP Generation]|
    --Shows how much MP the ability generates for its user.
    if(pzPredictionPack.bHasMPGenerationModule == true) then
        
        --Lines.
        AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, i, Translate(gsTranslationCombat, "[IMG0]Mana: +") .. pzPredictionPack.zMPGenerationModule.iMPGeneration)
        AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, i, 1)
        AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, i, 0, gcfAbilityImgOffsetPredY, "Root/Images/AdventureUI/StatisticIcons/Mana")
    
        --Next.
        i = i + 1
    end
    
    -- |[Lifesteal]|
    if(pzPredictionPack.bHasLifestealModule and pzPredictionPack.bHasDamageModule == true) then
        pzPredictionPack.zLifestealModule.iLifestealLo = math.floor(pzPredictionPack.zDamageModule.iDamageLo * pzPredictionPack.zLifestealModule.fStealPercent)
        pzPredictionPack.zLifestealModule.iLifestealHi = math.floor(pzPredictionPack.zDamageModule.iDamageHi * pzPredictionPack.zLifestealModule.fStealPercent)
    end
    
    -- |[Critical Strike]|
    --Show critical strike if it's over zero.
    if(pzPredictionPack.bHasCritModule == true and bLockoutCritModule == false) then
        
        --Common variables.
        local iCritRate  = pzPredictionPack.zCritModule.iCritRate
        local fCritBonus = pzPredictionPack.zCritModule.fCritBonus
        local iCritStun  = pzPredictionPack.zCritModule.iCritStun
        Debug_Print(" Crit Rate: " .. iCritRate .. "\n")
        Debug_Print(" Crit Bonus before tags: " .. fCritBonus .. "\n")
        Debug_Print(" Crit Stun: " .. iCritStun .. "\n")
        
        --Bonus applied to crits from the owner's tags.
        fCritBonus = fCritBonus + (iCritDamageBonusTags * 0.01)
        Debug_Print(" Crit Bonus after tags: " .. fCritBonus .. "\n")
                
        --Critical strike has no stun bonus:
        if(iCritStun < 1) then
            
            --Critical strike, but no damage base:
            if(pzPredictionPack.bHasDamageModule == false) then
            
            --Critical strike, has a damage module.
            else
                
                --Compute new damage range.
                local iLo = math.floor(pzPredictionPack.zDamageModule.iDamageLo * fCritBonus)
                local iHi = math.floor(pzPredictionPack.zDamageModule.iDamageHi * fCritBonus)
            
                --Set strings.
                local sCritical = Translate(gsTranslationCombat, "Critical")
                AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, i, iCritRate .. "%% "..sCritical..". [IMG0]" .. iLo .. "-" .. iHi)
                AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, i, 1)
                AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, i, 0, gcfAbilityImgOffsetPredY, "Root/Images/AdventureUI/StatisticIcons/Attack")
            end
        
        --Critical strike applies stun.
        else
            
            --Critical strike, but no damage base:
            if(pzPredictionPack.bHasDamageModule == false) then
            
            --Critical strike, has a damage module.
            else
                
                --Compute new damage range.
                local iLo = math.floor(pzPredictionPack.zDamageModule.iDamageLo * fCritBonus)
                local iHi = math.floor(pzPredictionPack.zDamageModule.iDamageHi * fCritBonus)
                
                --How much crit stun to apply.
                local iExpectedStun = fnComputeStunID(iCritStun, piTargetID)
                
                --If there is no base stun:
                local sCritical = Translate(gsTranslationCombat, "Critical")
                if(pzPredictionPack.bHasStunModule == false) then
                    AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, i, math.floor(iCritRate) .. "%% "..sCritical..". [IMG0]" .. iLo .. "-" .. iHi .. ". " .. iExpectedStun .. "[IMG1].")
                    AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, i, 2)
                    AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, i, 0, gcfAbilityImgOffsetPredY, "Root/Images/AdventureUI/StatisticIcons/Attack")
                    AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, i, 1, gcfAbilityImgOffsetPredY, "Root/Images/AdventureUI/DebuffIcons/Stun")
                
                --Has a base stun, so "add" stun.
                else
                    AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, i, math.floor(iCritRate) .. "%% "..sCritical..". [IMG0]" .. iLo .. "-" .. iHi .. ". +" .. iExpectedStun .. "[IMG1].")
                    AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, i, 2)
                    AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, i, 0, gcfAbilityImgOffsetPredY, "Root/Images/AdventureUI/StatisticIcons/Attack")
                    AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, i, 1, gcfAbilityImgOffsetPredY, "Root/Images/AdventureUI/DebuffIcons/Stun")
                end
            end
        end
        i = i + 1
    end
    
    -- |[Effect Modules]|
    --Diagnostics.
    Debug_Print(" Effect modules:\n")
    
    --For each effect module...
    for p = 1, iEffectModules, 1 do
        
        --Diagnostics.
        Debug_Print(" Module " .. i .. ":\n")
        
        -- |[Heal Over Time]|
        --Heal-Over-Time effects always hit.
        if(pzPredictionPack.zaEffectModules[p].bIsHealOverTime == true) then
    
            --Diagnostics.
            Debug_Print("  Heal-Over-Time module.\n")
    
            --Final = Fixed + AttackPower
            local iHeal = math.floor(pzPredictionPack.zaEffectModules[p].fHotComputed)
            local iDuration = pzPredictionPack.zaEffectModules[p].iHotDuration
            local iHealPerTurn = math.floor(iHeal / iDuration)
            
            --Debug.
            if(false) then
                io.write("Running Heal-Over-Time Line assembler.\n")
                io.write("Computed HoT: " .. pzPredictionPack.zaEffectModules[p].fHotComputed .. "\n")
                io.write("Healing: " .. iHeal .. "\n")
                io.write("Duration: " .. iDuration .. "\n")
                io.write("Heal-per-turn: " .. iHealPerTurn .. "\n")
            end
            
            --Set lines.
            AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, i, Translate(gsTranslationCombat, "Restores ") .. iHealPerTurn .. Translate(gsTranslationCombat, "[IMG0]/[IMG1] for ") .. iDuration .. "[IMG1].")
            AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, i, 2)
            AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, i, 0, gcfAbilityImgOffsetPredY+2, "Root/Images/AdventureUI/StatisticIcons/Health")
            AdvCombat_SetProperty("Prediction Set String Image",        sBoxName, i, 1, gcfAbilityImgOffsetPredY,   "Root/Images/AdventureUI/DebuffIcons/Clock")
            i = i + 1
        
        -- |[Debuffs and DoTs]|
        --If the module has a non-zero application rate...
        elseif(pzPredictionPack.zaEffectModules[p].iEffectApplyRate > 0) then
            
            --Setup.
            local sUseString = pzPredictionPack.zaEffectModules[p].sResultScript
    
            --Diagnostics.
            Debug_Print("  DoT/Buff/Debuff module.\n")
            Debug_Print("  Operating String: " .. sUseString .. "\n")
            
            -- |[Damage Tag]|
            --Scan the string for the special sequence "[DAM]". If it is found, it needs to be replaced with the damage,
            -- even if the damage value is zero.
            --The %'s are escape sequences.
            local iIndexOfSequence, iDummy = string.find(sUseString, "[DAM]", 1, true)
            while(iIndexOfSequence ~= nil) do
                
                --Get the letters before and after the sequence.
                local sStringBefore = string.sub(sUseString, 1, iIndexOfSequence-1)
                local sStringAfter = string.sub(sUseString, iIndexOfSequence + 5)
                
                --Replace.
                sUseString = sStringBefore .. math.floor(pzPredictionPack.zaEffectModules[p].iDamageTotal) .. sStringAfter
                
                --Check if there's another sequence.
                iIndexOfSequence = string.find(sUseString, "[DAM]", 1, true)
            end
            
            -- |[Severity Tag]|
            --Scan the string for the sequence "[SEV]". If it found, it needs to be replaced with the severity, even if the value is zero.
            local iUseSeverity = pzPredictionPack.zaEffectModules[p].iSeverity * pzPredictionPack.zaEffectModules[p].fSeverityBonus
            
            --If flagged, multiply the use-severity by the item power factor.
            if(pzPredictionPack.zaEffectModules[p].bBenefitsFromItemPower) then
                iUseSeverity = iUseSeverity * pzPredictionPack.zaEffectModules[p].fItemPowerBonus
            end
            
            --Round the severity down.
            iUseSeverity = math.floor(iUseSeverity)
            
            iIndexOfSequence, iDummy = string.find(sUseString, "[SEV]", 1, true)
            while(iIndexOfSequence ~= nil) do
                
                --Get the letters before and after the sequence.
                local sStringBefore = string.sub(sUseString, 1, iIndexOfSequence-1)
                local sStringAfter = string.sub(sUseString, iIndexOfSequence + 5)
                
                --Replace.
                sUseString = sStringBefore .. iUseSeverity .. sStringAfter
                
                --Check if there's another sequence.
                iIndexOfSequence = string.find(sUseString, "[SEV]", 1, true)
            end
            
            -- |[Images]|
            --Common. Set images. The 0th image is always the EffectHostile/EffectFriendly image.
            local iImagesTotal = #pzPredictionPack.zaEffectModules[p].saResultImages
            AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, i, 1 + iImagesTotal)
            for x = 1, iImagesTotal, 1 do
                
                --Offset. Some icons are a bit higher and need to be lowered.
                local fUseOffset = gcfAbilityImgOffsetPredY
                if(pzPredictionPack.zaEffectModules[p].saResultImages[x] == "Root/Images/AdventureUI/StatisticIcons/Protection") then
                    fUseOffset = gcfAbilityImgOffsetPredY+2
                end
                
                --Set.
                AdvCombat_SetProperty("Prediction Set String Image", sBoxName, i, x, fUseOffset, pzPredictionPack.zaEffectModules[p].saResultImages[x])
            end
            
            --Negative Effect / Debuff. Has a listed chance to apply.
            if(pzPredictionPack.zaEffectModules[p].bIsBuff == false) then
                AdvCombat_SetProperty("Prediction Set String Text",  sBoxName, i, "[IMG0]" .. math.floor(pzPredictionPack.zaEffectModules[p].iEffectApplyRate) .. "%% - " .. sUseString)
                AdvCombat_SetProperty("Prediction Set String Image", sBoxName, i, 0, gcfAbilityImgOffsetPredY+1, "Root/Images/AdventureUI/DebuffIcons/EffectHostile")
                
                --Report.
                Debug_Print("  Negative effect. Chance to apply.\n")
                Debug_Print("  Line: " .. "[IMG0]" .. math.floor(pzPredictionPack.zaEffectModules[p].iEffectApplyRate) .. "%% - " .. sUseString .. "\n")
            
            --Positive Effect / Buff. Just shows the effect indicator, always applies.
            else
                AdvCombat_SetProperty("Prediction Set String Text",  sBoxName, i, "[IMG0]" .. sUseString)
                AdvCombat_SetProperty("Prediction Set String Image", sBoxName, i, 0, gcfAbilityImgOffsetPredY+1, "Root/Images/AdventureUI/DebuffIcons/EffectFriendly")
                
                --Report.
                Debug_Print("  Positive effect. Always applies.\n")
                Debug_Print("  Line: " .. "[IMG0]" .. sUseString .. "\n")
            end
            
            --Next.
            i = i + 1
        
        -- |[DoT/Debuff but always misses]|
        else
            AdvCombat_SetProperty("Prediction Set String Text",  sBoxName, i, Translate(gsTranslationCombat, "Effect always resisted."))
            i = i + 1
        end
    end
    
    -- |[Chasers]|
    --Add a line indicating if the effect will be consumed. Only happens if the flag is set.
    if(pzPredictionPack.bHasChaserModules == true) then
        
        --Iterate across all chasers.
        for o = 1, pzPredictionPack.iChaserModulesTotal, 1 do
        
            --Shorthand.
            local zModule = pzPredictionPack.zaChaserModules[o]
            
            --If this chaser consumes effects:
            if(zModule.bConsumeEffect == true) then
                AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, i, zModule.sConsumeString)
                AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, i, zModule.iConsumeImgCount)
                for p = 1, zModule.iConsumeImgCount, 1 do
                    AdvCombat_SetProperty("Prediction Set String Image", sBoxName, i, p-1, gcfAbilityImgOffsetPredY, zModule.saConsumeImgPath[p])
                end
            end
            i = i + 1
        end
    end
    
    -- |[Additional Lines]|
    --Optional extra lines added manually.
    for p = 1, #pzPredictionPack.zaAdditionalLines, 1 do
        
        local iImageCount = #pzPredictionPack.zaAdditionalLines[p].saSymbols
        
        AdvCombat_SetProperty("Prediction Set String Text",         sBoxName, i, pzPredictionPack.zaAdditionalLines[p].sString)
        AdvCombat_SetProperty("Prediction Set String Images Total", sBoxName, i, iImageCount)
        for o = 1, iImageCount, 1 do
            AdvCombat_SetProperty("Prediction Set String Image", sBoxName, i, o-1, gcfAbilityImgOffsetPredY, pzPredictionPack.zaAdditionalLines[p].saSymbols[o])
        end
        i = i + 1
    end
    
    -- |[Finish Up]|
    Debug_PopPrint("fnBuildPredictionBox() - Completed, normally.\n")
end
