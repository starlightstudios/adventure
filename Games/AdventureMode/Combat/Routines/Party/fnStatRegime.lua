-- |[ ===================================== fnStatRegime() ===================================== ]|
--Stat Regimes are curves of character stat growth rates. These functions handle building them from
-- base values. They are only used in the debug menu.

--Curve types.
gciCurveLinear = 0
gciCurveSquare = 1
gciCurveCubic = 2
gciCurveRevLinear = 3
gciCurveRevSquare = 4
gciCurveRevCubic = 5

--Function that is used for computing exactly what the value of a statistic is at a given level.
function fnRunRegime(piaArray, piLevStart, piLevEnd, piGrowLo, piGrowHi, piCurveType)
    
    for i = piLevStart, piLevEnd, 1 do
        local fPercent = (i-piLevStart)/(piLevEnd-piLevStart)
        local iGrowRange = (piGrowHi - piGrowLo)
        local iGrowRate = 0
        
        --Resolve curve type.
        if(piCurveType == gciCurveLinear) then
            iGrowRate = piGrowLo + (iGrowRange * fPercent)
        elseif(piCurveType == gciCurveSquare) then
            iGrowRate = piGrowLo + (iGrowRange * fPercent * fPercent)
        elseif(piCurveType == gciCurveCubic) then
            iGrowRate = piGrowLo + (iGrowRange * fPercent * fPercent * fPercent)
        elseif(piCurveType == gciCurveRevLinear) then
            fPercent = 1.0 - fPercent
            iGrowRate = piGrowLo + (iGrowRange * fPercent)
        elseif(piCurveType == gciCurveRevSquare) then
            fPercent = 1.0 - fPercent
            iGrowRate = piGrowLo + (iGrowRange * fPercent * fPercent)
        elseif(piCurveType == gciCurveRevCubic) then
            fPercent = 1.0 - fPercent
            iGrowRate = piGrowLo + (iGrowRange * fPercent * fPercent * fPercent)
        end
        
        piaArray[i] = piaArray[i-1] + iGrowRate
    end
end

--Function that does standardized regime handling for a job. Uses a stat profile script.
function fnStandardJobStatHandler(psScriptPath, piLevel)
    
    -- |[Argument Check]|
    if(psScriptPath == nil) then return end
    if(piLevel      == nil) then return end
    
    -- |[Profile Script]|
    --Call this script to populate the global arrays.
    LM_ExecuteScript(psScriptPath, -1)
    
    -- |[Sum And Upload]|
    --Iterate across to the given level.
    local iHPBonus  = giaHPArray[piLevel]
    local iAtkBonus = giaAtkArray[piLevel]
    local iAccBonus = giaAccArray[piLevel]
    local iEvdBonus = giaEvdArray[piLevel]
    local iIniBonus = giaIniArray[piLevel]
    
    -- |[Catalysts]|
    --Add catalyst bonuses.
    local iHpCatalysts  = AdInv_GetProperty("Catalyst Count", gciCatalyst_Health)
    local iAtkCatalysts = AdInv_GetProperty("Catalyst Count", gciCatalyst_Attack)
    local iAccCatalysts = AdInv_GetProperty("Catalyst Count", gciCatalyst_Accuracy)
    local iEvdCatalysts = AdInv_GetProperty("Catalyst Count", gciCatalyst_Evade)
    local iIniCatalysts = AdInv_GetProperty("Catalyst Count", gciCatalyst_Initiative)
    iHPBonus  = iHPBonus  + (math.floor(iHpCatalysts  / gciCatalyst_Health_Needed)     * gciCatalyst_Health_Bonus)
    iAtkBonus = iAtkBonus + (math.floor(iAtkCatalysts / gciCatalyst_Attack_Needed)     * gciCatalyst_Attack_Bonus)
    iAccBonus = iAccBonus + (math.floor(iAccCatalysts / gciCatalyst_Accuracy_Needed)   * gciCatalyst_Accuracy_Bonus)
    iEvdBonus = iEvdBonus + (math.floor(iEvdCatalysts / gciCatalyst_Evade_Needed)      * gciCatalyst_Evade_Bonus)
    iIniBonus = iIniBonus + (math.floor(iIniCatalysts / gciCatalyst_Initiative_Needed) * gciCatalyst_Initiative_Bonus)

    -- |[Upload]|
    --Final tally. Upload to the temp structure.
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_HPMax,      math.floor(iHPBonus))
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Attack,     math.floor(iAtkBonus))
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Accuracy,   math.floor(iAccBonus))
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Evade,      math.floor(iEvdBonus))
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Initiative, math.floor(iIniBonus))
    
end

-- |[ ==================================== Chart Job Handler =================================== ]|
--Function that uses a common stat profile and common character job array.
function fnChartJobStatHandler(pzaJobChart, psJobName, piLevel, psCharacterProfile)
    
    -- |[Argument Check]|
    --Verify.
    if(pzaJobChart == nil) then return end
    if(psJobName   == nil) then return end
    if(piLevel     == nil) then return end
    
    -- |[Locate Job Package]|
    --Find out which slot in the chart the job is in.
    local iJobSlot = -1
    for p = 1, #pzaJobChart, 1 do
        if(pzaJobChart[p].sName == psJobName) then
            iJobSlot = p
            break
        end
    end
    
    --Error check:
    if(iJobSlot == -1) then 
        Debug_ForcePrint("Error: No job " .. psJobName .. " found in chart.\n")
        return 
    end
    
    -- |[Call Universal Stat Profile]|
    --Unless otherwise noted, this character's jobs all use the same growth rate and apply bonuses afterwards.
    LM_ExecuteScript(psCharacterProfile, -1)
    
    -- |[Compute Bonuses]|
    --Use the value at the given level.
    local iHPBonus  = pzaJobChart[iJobSlot].iHlt_Constant + (giaHPArray[piLevel]  * (1.0 + pzaJobChart[iJobSlot].fHlt_Percent))
    local iAtkBonus = pzaJobChart[iJobSlot].iAtk_Constant + (giaAtkArray[piLevel] * (1.0 + pzaJobChart[iJobSlot].fAtk_Percent))
    local iAccBonus = pzaJobChart[iJobSlot].iAcc_Constant + (giaAccArray[piLevel] * (1.0 + pzaJobChart[iJobSlot].fAcc_Percent))
    local iEvdBonus = pzaJobChart[iJobSlot].iEvd_Constant + (giaEvdArray[piLevel] * (1.0 + pzaJobChart[iJobSlot].fEvd_Percent))
    local iIniBonus = pzaJobChart[iJobSlot].iIni_Constant + (giaIniArray[piLevel] * (1.0 + pzaJobChart[iJobSlot].fEvd_Percent))
        
    -- |[Catalysts]|
    --Add catalyst bonuses.
    local iHpCatalysts  = AdInv_GetProperty("Catalyst Count", gciCatalyst_Health)
    local iAtkCatalysts = AdInv_GetProperty("Catalyst Count", gciCatalyst_Attack)
    local iAccCatalysts = AdInv_GetProperty("Catalyst Count", gciCatalyst_Accuracy)
    local iEvdCatalysts = AdInv_GetProperty("Catalyst Count", gciCatalyst_Evade)
    local iIniCatalysts = AdInv_GetProperty("Catalyst Count", gciCatalyst_Initiative)
    iHPBonus  = iHPBonus  + (math.floor(iHpCatalysts  / gciCatalyst_Health_Needed)     * gciCatalyst_Health_Bonus)
    iAtkBonus = iAtkBonus + (math.floor(iAtkCatalysts / gciCatalyst_Attack_Needed)     * gciCatalyst_Attack_Bonus)
    iAccBonus = iAccBonus + (math.floor(iAccCatalysts / gciCatalyst_Accuracy_Needed)   * gciCatalyst_Accuracy_Bonus)
    iEvdBonus = iEvdBonus + (math.floor(iEvdCatalysts / gciCatalyst_Evade_Needed)      * gciCatalyst_Evade_Bonus)
    iIniBonus = iIniBonus + (math.floor(iIniCatalysts / gciCatalyst_Initiative_Needed) * gciCatalyst_Initiative_Bonus)

    -- |[Upload]|
    --Final tally. Upload to the temp structure.
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_HPMax,      math.floor(iHPBonus))
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Attack,     math.floor(iAtkBonus))
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Accuracy,   math.floor(iAccBonus))
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Evade,      math.floor(iEvdBonus))
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Initiative, math.floor(iIniBonus))
    
    -- |[Resistances]|
    --Flat values.
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Protection,     pzaJobChart[iJobSlot].iBonus_Resist_Protection)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Slash,   pzaJobChart[iJobSlot].iBonus_Resist_Slash)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Strike,  pzaJobChart[iJobSlot].iBonus_Resist_Strike)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Pierce,  pzaJobChart[iJobSlot].iBonus_Resist_Pierce)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Flame,   pzaJobChart[iJobSlot].iBonus_Resist_Flame)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Freeze,  pzaJobChart[iJobSlot].iBonus_Resist_Freeze)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Shock,   pzaJobChart[iJobSlot].iBonus_Resist_Shock)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Crusade, pzaJobChart[iJobSlot].iBonus_Resist_Crusade)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Obscure, pzaJobChart[iJobSlot].iBonus_Resist_Obscure)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Bleed,   pzaJobChart[iJobSlot].iBonus_Resist_Bleed)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Poison,  pzaJobChart[iJobSlot].iBonus_Resist_Poison)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Corrode, pzaJobChart[iJobSlot].iBonus_Resist_Corrode)
    AdvCombat_SetProperty("Set Level Up Struct Stat", gciStatIndex_Resist_Terrify, pzaJobChart[iJobSlot].iBonus_Resist_Terrify)
    
end

