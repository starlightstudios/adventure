-- |[ ================================= fnApplyNegativeCharge() ================================ ]|
--This file contains functions pertaining to electric charge effects. These are called when an 
-- ability that builds positive or negative charges is used.
--Positive charges build to 3. When a negative charge ability is used, the positive charges buff
-- all allied accuracy by 15 per charge.
--Negative charges build to 3. When a positive charge ability is used, the negative charges deal
-- shock damage to all enemies equal to 0.15x user's attack power per charge.
--This file handles the negative charge version.
function fnApplyNegativeCharge(piSwitchType)

    -- |[ ============================= Combat: Execute on Targets ============================= ]|
    --Once target selection is complete, executes the ability.
    if(piSwitchType == gciAbility_Execute) then

        -- |[ ============== Originator Statistics =============== ]|
        --Get originator variables, modify resources.
        AdvCombat_SetProperty("Push Event Originator")
        
            --Get properties.
            local iOriginatorID = RO_GetID()
            local iPositiveTags = AdvCombatEntity_GetProperty("Tag Count", "Positive Charge")
            local iNegativeTags = AdvCombatEntity_GetProperty("Tag Count", "Negative Charge")
            
            --Set stat changes.
            fnModifyCP(gzRefAbility.iCPGain)
            fnModifyMP(gzRefAbility.iRequiredMP * -1)
            fnModifyCP(gzRefAbility.iRequiredCP * -1)
        DL_PopActiveObject()
        
        -- |[ ================= Ability Package ================== ]|
        --Create an ability package. This package comes with the default hit/miss values, animations, etc.
        local zaAbilityPackage = gzRefAbility.zExecutionAbiPackage
        local zaEffectPackage = gzRefAbility.zExecutionEffPackage
        zaAbilityPackage.iOriginatorID = iOriginatorID
    
        --Handling of free actions:
        if(gzRefAbility.bIsFreeAction) then
            
            --If the ability is a free action, but does not respect the action cap or consume any free actions,
            -- it is an "Effortless" action and can be used as many times as the player wants.
            if(gzRefAbility.iRequiredFreeActions == 0 and gzRefAbility.bRespectActionCap == false) then
                AdvCombat_SetProperty("Set As Effortless Action")
            
            --Normal free action.
            else
                AdvCombat_SetProperty("Set As Free Action")
            end
        end
        
        -- |[ ================== For Each Target ================= ]|
        --Store max number of spent ticks.
        local iMaxTicks = 0
        
        --Get how many targets were painted by this ability, iterate across them.
        local iTargetsTotal = AdvCombat_GetProperty("Active Event Targets")
        for i = 1, iTargetsTotal, 1 do
            local iTicksExpended = fnStandardExecution(iOriginatorID, i-1, zaAbilityPackage, zaEffectPackage)
            if(iTicksExpended > iMaxTicks) then iMaxTicks = iTicksExpended end
        end
        
        --Buffer.
        iMaxTicks = iMaxTicks + 15
        local iStartTicks = iMaxTicks
	
	-- |[ ===================== Ally Effects ===================== ]|
	--If set, this effect applies to all allies. Note that the allies should not be the targets. This also applies to 
	-- the user, who is considered an ally.
	if(gzRefAbility.zAllyEffectPackage ~= nil) then
	    
	    --Determine the party of the originator.
	    local iOriginatorParty = AdvCombat_GetProperty("Party Of ID", iOriginatorID)
	    
	    --Player's party:
	    if(iOriginatorParty == gciACPartyGroup_Party) then
		local iPartySize = AdvCombat_GetProperty("Combat Party Size")
		for i = 0, iPartySize-1, 1 do
		    local iPartyID = AdvCombat_GetProperty("Combat Party ID", i)
		    gzRefAbility.zAllyEffectPackage.fnHandler(iOriginatorID, iPartyID, 0, gzRefAbility.zAllyEffectPackage, true, false)
		end
	    
	    --Enemy party:
	    elseif(iOriginatorParty == gciACPartyGroup_Enemy) then
		local iPartySize = AdvCombat_GetProperty("Enemy Party Size")
		for i = 0, iPartySize-1, 1 do
		    local iEnemyID = AdvCombat_GetProperty("Enemy ID", i)
		    gzRefAbility.zAllyEffectPackage.fnHandler(iOriginatorID, iEnemyID, 0, gzRefAbility.zAllyEffectPackage, true, false)
		end
	    end
	end
	
	-- |[ ===================== Self-Effects ===================== ]|
	if(gzRefAbility.zExecutionSelfPackage ~= nil) then
	    gzRefAbility.zExecutionSelfPackage.fnHandler(iOriginatorID, iOriginatorID, 0, gzRefAbility.zExecutionSelfPackage, true, false)
	end
        
        -- |[ ============== Apply Negative Charge =============== ]|
        --First, check if the owner has any positive charges. If so, issue a discharge.
        if(iPositiveTags > 0) then
        
            --Find all effects with the [Positive Charge] tag and remove them.
            AdvCombat_SetProperty("Push Event Originator")
                local iTotalChargeEffects = AdvCombatEntity_GetProperty("Effects With Tag", "Positive Charge")
                for i = 0, iTotalChargeEffects-1, 1 do
                    local iEffectID = AdvCombatEntity_GetProperty("Effect ID With Tag", "Positive Charge", 0)
                    AdvCombat_SetProperty("Remove Effect", iEffectID)
                end
            DL_PopActiveObject()
        
            --Find all combat entities on the opposite team as the user.
            local iPartyAffiliation = AdvCombat_GetProperty("Party Of ID", iOriginatorID)
            local sSizeFunction = "Combat Party Size"
            local sIDFunction = "Combat Party ID"
            if(iPartyAffiliation == gciACPartyGroup_Party) then
                sSizeFunction = "Enemy Party Size"
                sIDFunction = "Enemy ID"
            end
            
            --Iterate.
            local iPartySize = AdvCombat_GetProperty(sSizeFunction)
            for i = 0, iPartySize-1, 1 do
                
                --Get ID.
                local iPartyID = AdvCombat_GetProperty(sIDFunction, i)
                
                --Compute damage versus target.
                fnStandardDamageType(iOriginatorID, iPartyID, gciDamageType_Shocking, 15, false, 0)
                giStandardDamage = math.floor(giStandardDamage * 0.15)
        
                --Damage is tripled in tourist mode, though it could still be 0 in theory.
                if(bIsTouristMode) then
                    giStandardDamage = giStandardDamage * 3.0
                end
                
                --Create a shock attack.
                local iCurTimer = iStartTicks
                AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iPartyID, iCurTimer, "Play Sound|Combat\\|Impact_Slash")
                AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iPartyID, iCurTimer, "Create Animation|Shock|AttackAnim"..i)
                iCurTimer = iCurTimer + fnGetAnimationTiming("Shock")
                
                --Damage issuer.
                AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iPartyID, iCurTimer, "Damage|" .. giStandardDamage)
                iCurTimer = iCurTimer + gciApplication_TextTicks
                
                --Max ticks.
                if(iCurTimer > iMaxTicks) then iMaxTicks = iCurTimer end
                
                --At the end of each segment, buffer the starting timer.
                iStartTicks = iStartTicks + 15
            end
        
            --Timer.
            fnDefaultEndOfApplications(iMaxTicks)
        
        --No positive tags. If we have less than 3 negative charges, issue a new charge.
        elseif(iNegativeTags < 3) then
            AdvCombat_SetProperty("Register Application Pack", iOriginatorID, iOriginatorID, iMaxTicks, "Effect|"..gsRoot.."Combat/Party/Christine/Jobs/Common/Effects/Negative Charge.lua")
        
        --No negatives and no space for new positives.
        else
            --Do nothing
        end
        
    -- |[ ================================== Standard Handler ================================== ]|
    --All other cases, call the standard handler.
    else
        LM_ExecuteScript(gsStandardAbilityPath, piSwitchType)
    end
end
