-- |[ ================================ Resolve Doctor Bag Value ================================ ]|
--When an enemy has a -1 for their doctor bag charges gained, this script is run. It receives the
-- turn the enemy was KO'd on and resolves the number of charges accordingly.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Turn the entity was defeated on.
local iDefeatedTurn = LM_GetScriptArgument(0, "I")

--Resolver.
if(iDefeatedTurn == 0) then
    AdvCombat_SetProperty("Set Doctor Resolve Value", 10)
elseif(iDefeatedTurn == 1) then
    AdvCombat_SetProperty("Set Doctor Resolve Value", 6)
elseif(iDefeatedTurn == 2) then
    AdvCombat_SetProperty("Set Doctor Resolve Value", 3)
elseif(iDefeatedTurn == 3) then
    AdvCombat_SetProperty("Set Doctor Resolve Value", 1)
else
    AdvCombat_SetProperty("Set Doctor Resolve Value", 0)
end
