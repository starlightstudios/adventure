-- |[ ================================= Place Job Common Skills ================================ ]|
--Functions related to placing jobs on the active entity's skill bar during combat.


-- |[ ================================ fnPlaceJobCommonSkills() ================================ ]|
--Common code to replace two slots on the player interface with the equipped items, as well as 
-- Surrender/Retreat.
--Most characters have two equippable items and keep everything in the same slots. The AdvCombatJob
-- should be the active object.
--This should only be called at combat start, otherwise it will replace item abilities with new
-- copies of themselves, which makes them lose their charge counts.
function fnPlaceJobCommonSkills()
    
    -- |[Items]|
    --Under normal conditions, there are two items. This is variable. Item slots are always
    -- to follow the pattern "Item A", "Item B", "Item C" etc. As soon as a slot does not exist,
    -- the iteration stops.
    --The tactics grid is only 9 entries wide. Any additional items would be lost with this algorithm.
    local iCurrentItemSlot = 1
    for i = 0, 8, 1 do
    
        --Byte value.
        local iByteValue = string.byte("A") + i
        local sEquipSlot = "Item " .. string.char(iByteValue)
        local sAbilityName = "Item|" .. string.char(iByteValue)
    
        --If the slot doesn't exist at all, stop iterating.
        if(AdvCombatEntity_GetProperty("Does Slot Exist", sEquipSlot) == false) then
            break
        end
        
        --Resolve the position to place the item.
        local iPlaceX = gciaAbility_Tactics_Items_X[iCurrentItemSlot]
        local iPlaceY = gciaAbility_Tactics_Items_Y[iCurrentItemSlot]
        if(iPlaceX == nil) then iPlaceX = gciaAbility_Tactics_Items_X[1] end
        if(iPlaceY == nil) then iPlaceY = gciaAbility_Tactics_Items_Y[1] end
    
        --Slot exists. If it has an item in it, we need to slot its ability. First clear the slot.
        AdvCombatEntity_SetProperty("Remove Ability", sAbilityName)
        
        --If there's a piece of equipment in the slot, gets its ability and place it.
        if(AdvCombatEntity_GetProperty("Equipment In Slot S", sEquipSlot) ~= "Null") then
            AdvCombatEntity_SetProperty("Push Item In Slot S", sEquipSlot)
                local sAbilityPath = AdItem_GetProperty("Ability Path")
            DL_PopActiveObject()
            if(sAbilityPath ~= "Null") then
                LM_ExecuteScript(sAbilityPath, gciAbility_Create, sAbilityName)
                AdvCombatEntity_SetProperty("Set Ability Slot",iPlaceX, iPlaceY, sAbilityName)
            end
        end
        
        --In all cases, increment the item slot.
        iCurrentItemSlot = iCurrentItemSlot + 1
    
    end
    
    -- |[Weapon Swap]|
    --Equipment Change. Remove abilities, make sure there's a weapon in the named slot.
    if(AdvCombatEntity_GetProperty("Does Slot Exist", "Weapon Backup A") == true) then
        if(AdvCombatEntity_GetProperty("Equipment In Slot S", "Weapon Backup A") ~= "Null") then
            AdvCombatEntity_SetProperty("Set Ability Slot", gciAbility_Tactics_WeaponSwapA_X, gciAbility_Tactics_WeaponSwapA_Y, "Common|WeaponSwapA")
        else
            AdvCombatEntity_SetProperty("Set Ability Slot", gciAbility_Tactics_WeaponSwapA_X, gciAbility_Tactics_WeaponSwapA_Y, "Null")
        end
    end
    
    --Weapon Slot B.
    if(AdvCombatEntity_GetProperty("Does Slot Exist", "Weapon Backup B") == true) then
        if(AdvCombatEntity_GetProperty("Equipment In Slot S", "Weapon Backup B") ~= "Null") then
            AdvCombatEntity_SetProperty("Set Ability Slot", gciAbility_Tactics_WeaponSwapB_X, gciAbility_Tactics_WeaponSwapB_Y, "Common|WeaponSwapB")
        else
            AdvCombatEntity_SetProperty("Set Ability Slot", gciAbility_Tactics_WeaponSwapB_X, gciAbility_Tactics_WeaponSwapB_Y, "Null")
        end
    end
    
    -- |[Ammo Swap]|
    --Similar to Weapon Swap A. Sanya uses alternate ammo types.
    if(AdvCombatEntity_GetProperty("Does Slot Exist", "Ammo Backup") == true) then
        if(AdvCombatEntity_GetProperty("Equipment In Slot S", "Ammo Backup") ~= "Null") then
            AdvCombatEntity_SetProperty("Set Ability Slot", gciAbility_Tactics_WeaponSwapA_X, gciAbility_Tactics_WeaponSwapA_Y, "Common|AmmoSwap")
        else
            AdvCombatEntity_SetProperty("Set Ability Slot", gciAbility_Tactics_WeaponSwapA_X, gciAbility_Tactics_WeaponSwapA_Y, "Null")
        end
    end
    
    -- |[System]|
    --These abilities are always present, and use fixed grid positions.
    AdvCombatEntity_SetProperty("Set Ability Slot", gciAbility_Tactics_Retreat_X, gciAbility_Tactics_Retreat_Y, "System|Retreat")
    if(gbIsVolunteer == false) then
        AdvCombatEntity_SetProperty("Set Ability Slot", gciAbility_Tactics_Surrender_X, gciAbility_Tactics_Surrender_Y, "System|Surrender")
    else
        AdvCombatEntity_SetProperty("Set Ability Slot", gciAbility_Tactics_Surrender_X, gciAbility_Tactics_Surrender_Y, "System|Volunteer")
    end
end

-- |[ ============================= fnPlaceJobCommonSkillsNoItems() ============================ ]|
--Variant of fnPlaceJobCommonSkills(), this is used explicitly when changing jobs during combat.
-- It does not replace the combat item abilities, which allows them to maintain their charge counts.
function fnPlaceJobCommonSkillsNoItems()
    
    -- |[Weapon Swap]|
    --Equipment Change. Use fixed slots.
    if(AdvCombatEntity_GetProperty("Does Slot Exist", "Weapon Backup A") == true) then
        if(AdvCombatEntity_GetProperty("Equipment In Slot S", "Weapon Backup A") ~= "Null") then
            AdvCombatEntity_SetProperty("Set Ability Slot", gciAbility_Tactics_WeaponSwapA_X, gciAbility_Tactics_WeaponSwapA_Y, "Common|WeaponSwapA")
        else
            AdvCombatEntity_SetProperty("Set Ability Slot", gciAbility_Tactics_WeaponSwapA_X, gciAbility_Tactics_WeaponSwapA_Y, "Null")
        end
    end
    
    --Weapon Slot B.
    if(AdvCombatEntity_GetProperty("Does Slot Exist", "Weapon Backup B") == true) then
        if(AdvCombatEntity_GetProperty("Equipment In Slot S", "Weapon Backup B") ~= "Null") then
            AdvCombatEntity_SetProperty("Set Ability Slot", gciAbility_Tactics_WeaponSwapB_X, gciAbility_Tactics_WeaponSwapB_Y, "Common|WeaponSwapB")
        else
            AdvCombatEntity_SetProperty("Set Ability Slot", gciAbility_Tactics_WeaponSwapB_X, gciAbility_Tactics_WeaponSwapB_Y, "Null")
        end
    end
    
    -- |[Ammo Swap]|
    --Similar to Weapon Swap A. Sanya uses alternate ammo types.
    if(AdvCombatEntity_GetProperty("Does Slot Exist", "Ammo Backup") == true) then
        if(AdvCombatEntity_GetProperty("Equipment In Slot S", "Ammo Backup") ~= "Null") then
            AdvCombatEntity_SetProperty("Set Ability Slot", gciAbility_Tactics_WeaponSwapA_X, gciAbility_Tactics_WeaponSwapA_Y, "Common|AmmoSwap")
        else
            AdvCombatEntity_SetProperty("Set Ability Slot", gciAbility_Tactics_WeaponSwapA_X, gciAbility_Tactics_WeaponSwapA_Y, "Null")
        end
    end
    
    -- |[System]|
    --These abilities are always present, and use fixed grid positions.
    AdvCombatEntity_SetProperty("Set Ability Slot", gciAbility_Tactics_Retreat_X, gciAbility_Tactics_Retreat_Y, "System|Retreat")
    if(gbIsVolunteer == false) then
        AdvCombatEntity_SetProperty("Set Ability Slot", gciAbility_Tactics_Surrender_X, gciAbility_Tactics_Surrender_Y, "System|Surrender")
    else
        AdvCombatEntity_SetProperty("Set Ability Slot", gciAbility_Tactics_Surrender_X, gciAbility_Tactics_Surrender_Y, "System|Volunteer")
    end
    
end

-- |[ ========================== fnPlaceJobCommonSkillsCombatStart() =========================== ]|
--At combat start, after the abilities have been placed on the ability bar, runs their combat-start
-- boot functions. Items need these to compute their charges.
function fnPlaceJobCommonSkillsCombatStart()
    
    --Setup.
    local iCurrentItemSlot = 1
    
    --Iterate.
    for i = 0, 8, 1 do
    
        --Byte value.
        local iByteValue = string.byte("A") + i
        local sEquipSlot = "Item " .. string.char(iByteValue)
        local sAbilityName = "Item|" .. string.char(iByteValue)
    
        --If the slot doesn't exist at all, stop iterating.
        if(AdvCombatEntity_GetProperty("Does Slot Exist", sEquipSlot) == false) then
            break
        end
        
        --Resolve the position to place the item.
        local iPlaceX = gciaAbility_Tactics_Items_X[iCurrentItemSlot]
        local iPlaceY = gciaAbility_Tactics_Items_Y[iCurrentItemSlot]
        if(iPlaceX == nil) then iPlaceX = gciaAbility_Tactics_Items_X[1] end
        if(iPlaceY == nil) then iPlaceY = gciaAbility_Tactics_Items_Y[1] end
        
        --Push the item to get its path.
        if(AdvCombatEntity_GetProperty("Equipment In Slot S", sEquipSlot) ~= "Null") then
            AdvCombatEntity_SetProperty("Push Item In Slot S", sEquipSlot)
                local sAbilityPath = AdItem_GetProperty("Ability Path")
            DL_PopActiveObject()
            
            --Path was not null. Push the ability spawned from the item and execute its startup.
            if(sAbilityPath ~= "Null") then
                AdvCombatEntity_SetProperty("Push Ability In Slot", iPlaceX, iPlaceY)
                    LM_ExecuteScript(sAbilityPath, gciAbility_BeginCombat)
                DL_PopActiveObject()
            end
        end
        
        --In all cases, increment the item slot.
        iCurrentItemSlot = iCurrentItemSlot + 1
    
    end
end

-- |[ ==================================== fnClearJobSlots() =================================== ]|
--Clears the ability slots used by the job and its standard slots.
function fnClearJobSlots()
        
    --Clear 'standard' slots. Deliberately skips the item slots.
    for x = gciAbility_Class_X0, gciAbility_Class_X1, 1 do
        for y = gciAbility_Class_Y0, gciAbility_Class_Y1, 1 do
            AdvCombatEntity_SetProperty("Set Ability Slot", x, 0, "Null")
        end
    end
    
end

-- |[ =================================== fnClearAllSlots() ==================================== ]|
--Clears all skill slots. Usually used for special TFs or enemies.
function fnClearAllSlots()
    for x = 0, gciAbilityGrid_XSize-1, 1 do
        for y = 0, gciAbilityGrid_YSize-1, 1 do
            AdvCombatEntity_SetProperty("Set Ability Slot", x, y, "Null")
        end
    end
end
