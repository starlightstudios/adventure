-- |[ ==================================== fnComputeEffects ==================================== ]|
--Computes whether or not any effects attached to the ability apply. Effects are usually all-or-nothing
-- cases. Every level the user has gives an application bonus.
function gzaAbiHandlerFuncs.fnComputeEffects()
    
    -- |[ ========= Effect Application Chance ========== ]|
    -- |[Effect Application]|
    --Run across the list of effects. Check if they applied.
    for i = 1, #gzAbiPack.zaEffectPacks, 1 do
        gzAbiPack.baEffectsApplied[i] = fnCheckEffectApplied(gzAbiPack.zaEffectPacks[i], gzAbiPack.iOriginatorID, gzAbiPack.iTargetID, gzAbiPack.zAbilityPack.bEffectAlwaysApplies)
        gzAbiPack.zTargetInfo.bEffectApplied = gzAbiPack.baEffectsApplied[i]
    end
    
    -- |[ ========== Effect Generation By Tags ========= ]|
    --Check the attack's user for tags. Tags that can generate new effects are checked here and added to a list.
    -- Effects generated in this fashion are assumed to always apply.
    fnGenerateEffectsFromTags(gzAbiPack.iOriginatorID, gzAbiPack.iTargetID, gzAbiPack.zAbilityPack)
    
    --Store the generated effects in the ability.
    gzAbiPack.zaTagEffectsHostile = gzaTagEffectsHostile
end