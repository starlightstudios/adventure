-- |[ =================================== Standard Execution =================================== ]|
--Routing function, uses the target index for an already run target macro. The execution index is
-- assumed to be the same as the target index.
function fnStandardExecution(piOriginatorID, piTargetIndex, pzExecPack, pzEffectPack, pzEffectPackB, pzEffectPackC)

    --Get the target ID using the target index.
    AdvCombat_SetProperty("Push Target", piTargetIndex)
        local iTargetID = RO_GetID()
    DL_PopActiveObject()
    
    --Assemble effects into a list.
    local zaEffectList = {}
    zaEffectList[1] = pzEffectPack
    zaEffectList[2] = pzEffectPackB
    zaEffectList[3] = pzEffectPackC
    
    --Call the handler.
    return fnAbilityHandler(piOriginatorID, iTargetID, piTargetIndex, pzExecPack, zaEffectList)
end