-- |[ ===================================== fnHandleMiss() ===================================== ]|
--If the attack misses, this handler is called and operations end. The function returns the timer
-- value done by this execution.
function gzaAbiHandlerFuncs.fnHandleMiss()
    
    -- |[Statistics]|
    --Store in the stats pack that this ability missed.
    gzAbiPack.zTargetInfo.bWasMiss = true
    
    -- |[Setup]|
    local iTimer = (gciAbility_TicksOffsetPerTarget * gzAbiPack.iExecIndex)
    local zaMissPackage = fnConstructDefaultMissPackage()
    zaMissPackage.iOriginatorID = gzAbiPack.iOriginatorID

    -- |[Black Flash]|
    --Causes the entity using the skill to flash black. Only happens if this is the zeroth ability used.
    if(gzAbiPack.zAbilityPack.bCauseBlackFlash == true and gzAbiPack.iExecIndex == 0) then
        AdvCombat_SetProperty("Register Application Pack", gzAbiPack.iOriginatorID, gzAbiPack.iOriginatorID, iTimer, "Flash Black")
        iTimer = iTimer + gciAbility_BlackFlashTicks
    end
    
    -- |[Title]|
    --Optional, shows the name of the ability. Can be nil or "Null" to do nothing. Enemies often
    -- use these to indicate a special attack.
    if(gzAbiPack.zAbilityPack.sShowTitle ~= nil and gzAbiPack.zAbilityPack.sShowTitle ~= "Null") then
        AdvCombat_SetProperty("Register Application Pack", gzAbiPack.iOriginatorID, gzAbiPack.iOriginatorID, iTimer, "Title|" .. gzAbiPack.zAbilityPack.sShowTitle)
    end
    
    -- |[Animation]|
    --Ability strikes, then misses, then finishes up.
    iTimer = zaMissPackage.fnMissAnimate(gzAbiPack.iTargetID, iTimer, gzAbiPack.zAbilityPack, zaMissPackage)
    fnDefaultEndOfApplications(iTimer)
    return iTimer
end
