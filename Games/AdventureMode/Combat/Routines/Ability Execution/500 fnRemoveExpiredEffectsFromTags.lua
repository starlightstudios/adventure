-- |[ ============================ fnRemoveExpiredEffectsFromTags() ============================ ]|
--Removes any effects that should expire due to tags. This is done after execution of the ability
-- as those tags may affect attack power and whatnot.
function gzaAbiHandlerFuncs.fnRemoveExpiredEffectsFromTags()
    
    -- |[ ========== Execution Tags ========== ]|
    --Debug.
    --io.write("Removing expired effects from tags.\n")
    --if(gzAbiPack.zAbilityPack.sAbilityName ~= nil) then
    --    io.write(" Ability used: " .. gzAbiPack.zAbilityPack.sAbilityName .. "\n")
    --else
    --    io.write(" Ability used: [No Name]\n")
    --end
    
    --Setup.
    local zaExecTags = gzAbiPack.zAbilityPack.zaTags
    
    --List of IDs to remove.
    local iaRemoveIDs = {}
    
    --These tags can adjust behaviors caused by other tags.
    local iIsAttack  = fnGetTagCountInTable("Is Attack", zaExecTags)
    
    -- |[ =========== User Effects =========== ]|
    --Effects on the ability's user.
    AdvCombat_SetProperty("Push Entity By ID", gzAbiPack.iOriginatorID)
    
        -- |[Attack Removal]|
        --Effects can be consumed after the next attack. Remove those here.
        if(iIsAttack > 0) then
            
            --Iterate.
            local iAttackRemovalEffectsTotal = AdvCombatEntity_GetProperty("Effects With Tag", "Consume Next Attack")
            for i = 0, iAttackRemovalEffectsTotal-1, 1 do
                
                --Get ID:
                local iEffectID = AdvCombatEntity_GetProperty("Effect ID With Tag", "Consume Next Attack", i)
                
                --Store on the global list.
                table.insert(iaRemoveIDs, iEffectID)
                
            end
        end
    
    DL_PopActiveObject()
    
    -- |[ ========== Target Effects ========== ]|
    --Tags that apply to the target.
    AdvCombat_SetProperty("Push Entity By ID", gzAbiPack.iTargetID)
        
    DL_PopActiveObject()
    
    -- |[ ============== Removal ============= ]|
    --Once a list of all effects has been assembled, remove them. This is done because effects are often
    -- referenced by index, and removing effects when querying will change their indices. Instead they are
    -- all removed at once.
    for i = 1, #iaRemoveIDs, 1 do
        AdvCombat_SetProperty("Remove Effect", iaRemoveIDs[i])
    end
end
