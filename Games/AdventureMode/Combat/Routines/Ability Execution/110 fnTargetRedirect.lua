-- |[ =================================== fnTargetRedirect() =================================== ]|
--Handles target redirection via tags or abilities. Everything is in the global packages.
function gzaAbiHandlerFuncs.fnTargetRedirect()
    
    -- |[Setup]|
    --Store the original target ID in case it changes.
    gzAbiPack.iOrigTargetID = gzAbiPack.iTargetID
    
    --Fast-access pointers.
    local zAbilityPack = gzAbiPack.zAbilityPack

    -- |[Error Check]|
    --Target redirection does not occur if this flag is set. Buffs and heals will therefore not redirect.
    if(zAbilityPack.bDoesNoDamage == false) then

        --Run routine.
        local iNewTargetID = fnGetTargetRedirect(gzAbiPack.iTargetID)
        
        --If the new target ID is not the same as the old one, we need to make sure the new target
        -- is on stage to take the hit.
        if(iNewTargetID ~= gzAbiPack.iTargetID) then
            AdvCombat_SetProperty("Stage Entity By ID", iNewTargetID)
            gzAbiPack.iTargetID = iNewTargetID
        end
    end
    
    --Once the target is resolved, create a new target pack.
    gzAbiPack.zTargetInfo = fnCreateStatisticsTargetPack()
    table.insert(gzAbiPack.zStatPack.zaTargetInfo, gzAbiPack.zTargetInfo)
    gzAbiPack.zTargetInfo.iTargetID = gzAbiPack.iTargetID
    
end
