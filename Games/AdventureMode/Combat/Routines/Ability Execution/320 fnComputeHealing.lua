-- |[ =================================== fnComputeHealing() =================================== ]|
--Computes how much healing the ability applies. Note that this may conflate multiple healing sources,
-- such as a self-heal ability stacking with lifesteal to simplify application.
function gzaAbiHandlerFuncs.fnComputeHealing()

    -- |[ ============= Variables ============ ]|
    --User variables.
    AdvCombat_SetProperty("Push Entity By ID", gzAbiPack.iOriginatorID)
        local iMaxHPUser  = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
        local iAttackUser = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
    DL_PopActiveObject()
    
    --Target variables. Note: Target may be user.
    AdvCombat_SetProperty("Push Entity By ID", gzAbiPack.iTargetID)
        local iMaxHPTarget = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
    DL_PopActiveObject()
    
    -- |[ ========= Stack Application ======== ]|
    --If the target is the same as the user, then self-healing adds to the healing component.
    local bAddSelfHealing = false
    if(gzAbiPack.iTargetID == gzAbiPack.iOriginatorID) then
        bAddSelfHealing = true
    
    --If the target and user are not the same, then self-healing compute here.
    else
    
        --Set. Unlike healing, there is no clamp for self-healing, it can be zero.
        local iFinalSelfHeal = gzAbiPack.zAbilityPack.iSelfHealingBase
        iFinalSelfHeal = iFinalSelfHeal + math.floor(gzAbiPack.zAbilityPack.fSelfHealingFactor * iAttackUser)
        iFinalSelfHeal = iFinalSelfHeal + math.floor(gzAbiPack.zAbilityPack.fSelfHealingPercent * iMaxHPUser)
        iFinalSelfHeal = iFinalSelfHeal + gzAbiPack.iLifestealVal
        
        --Store.
        gzAbiPack.zAbilityPack.iSelfHealingFinal = iFinalSelfHeal
    end
    
    -- |[ =========== Computations =========== ]|
    --Healing component. Calculate the final healing value.
    if(                     (gzAbiPack.zAbilityPack.iHealingBase     ~= 0 or gzAbiPack.zAbilityPack.fHealingFactor     ~= 0.0 or gzAbiPack.zAbilityPack.fHealingPercent     ~= 0.0) or 
       (bAddSelfHealing and (gzAbiPack.zAbilityPack.iSelfHealingBase ~= 0 or gzAbiPack.zAbilityPack.fSelfHealingFactor ~= 0.0 or gzAbiPack.zAbilityPack.fSelfHealingPercent ~= 0.0))) then
        
        -- |[Formula Variables]|
        local iUseBase    = gzAbiPack.zAbilityPack.iHealingBase
        local fUseFactor  = gzAbiPack.zAbilityPack.fHealingFactor
        local fUsePercent = gzAbiPack.zAbilityPack.fHealingPercent
        
        -- |[Item Power Boost]|
        --If flagged, multiply the base healing by the item power factor.
        if(gzAbiPack.zAbilityPack.bHealingBenefitsFromItemPower) then
            AdvCombat_SetProperty("Push Entity By ID", gzAbiPack.iOriginatorID)
                local iItemPowerBonus = AdvCombatEntity_GetProperty("Tag Count", "Item Power +")
                local iItemPowerMalus = AdvCombatEntity_GetProperty("Tag Count", "Item Power -")
                local fPowerFactor = 1.00 + (0.01 * (iItemPowerBonus - iItemPowerMalus))
                iUseBase = iUseBase * fPowerFactor
            DL_PopActiveObject()
        end
        
        -- |[Self-Healing Mod]|
        --If the target is the same as the originator, the self-healing appends itself to the healing.
        if(bAddSelfHealing) then
            iUseBase    = iUseBase    + gzAbiPack.zAbilityPack.iSelfHealingBase
            fUseFactor  = fUseFactor  + gzAbiPack.zAbilityPack.fSelfHealingFactor
            fUsePercent = fUsePercent + gzAbiPack.zAbilityPack.fSelfHealingPercent
        
        end
        
        -- |[Compute]|
        --Calculate.
        gzAbiPack.zAbilityPack.iHealingFinal = iUseBase + math.floor(fUseFactor * iAttackUser) + math.floor(iMaxHPTarget * fUsePercent)
        
        --Clamp.
        if(gzAbiPack.zAbilityPack.iHealingFinal < 1) then gzAbiPack.zAbilityPack.iHealingFinal = 1 end
    end
end
