-- |[ ====================================== fnComputeStun ===================================== ]|
--Computes how much stun to apply to the target, if any.
function gzaAbiHandlerFuncs.fnComputeStun()

    -- |[Application Check]|
    --Ability applies no stun. Do nothing.
    if(gzAbiPack.zAbilityPack.bDoesNoStun == true) then return end

    --Target can't be stunned anyway.
    if(gzAbiPack.bIsTargetStunnable == false) then return end

    -- |[Compute and Store]|
    --Base value.
    local iStunToApply = gzAbiPack.zAbilityPack.iBaseStunDamage
    
    --Critical strikes can apply extra crit. This value may be zero.
    if(gbAttackCrit == true) then 
        iStunToApply = iStunToApply + gzAbiPack.zAbilityPack.iCriticalStunDamage 
    end
    
    --Apply the stun immediately. The value gzAbiPack.iFinalStun is stored for later animation.
    gzAbiPack.iFinalStun = fnApplyStunID(iStunToApply, gzAbiPack.iTargetID)
end