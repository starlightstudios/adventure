-- |[ ==================================== fnHandleAccuracy ==================================== ]|
--Handles the accuracy computations for the given ability. Populates the globals gbAttackHit and gbAttackCrit
-- with the results.
function gzaAbiHandlerFuncs.fnHandleAccuracy()
    
    -- |[Normal Accuracy]|
    --Ability doesn't have an "Always Hit" property. Run the standard accuracy.
    if(gzAbiPack.zAbilityPack.bAlwaysHits == false and gzAbiPack.bAlwaysHit == false) then
        
        --Run standard routine.
        fnStandardAccuracy(gzAbiPack.iOriginatorID, gzAbiPack.iTargetID, gzAbiPack.zAbilityPack.iMissThreshold, gzAbiPack.zAbilityPack.iCritThreshold)
        
        --Modify crit by ability properties.
        if(gzAbiPack.zAbilityPack.bAlwaysCrits) then gbAttackCrit = true end
        if(gzAbiPack.bAlwaysCrit)               then gbAttackCrit = true end
        if(gzAbiPack.zAbilityPack.bNeverCrits)  then gbAttackCrit = false end
        
        --In tourist mode, the attack always hits and never glances.
        if(gbAttackHit == false and gzAbiPack.bIsTouristMode) then
            gbAttackHit = true
        end
        
        --If the target was stunned, the attack always hits.
        if(gbAttackHit == false and gzAbiPack.bIsTargetStunned) then
            gbAttackHit = true
        end
        
        --If the attack was a glancing blow, but the ability never glances, it becomes a miss.
        if(gzAbiPack.zAbilityPack.bNeverGlances and gbAttackGlances) then
            gbAttackHit = false
            gbAttackCrit = false
        
        --If the attack was a glancing blow, and it can glance, disable crits. If the always-crit flag is active, the attack still crits.
        elseif(gbAttackGlances) then
            gbAttackCrit = false
            if(gzAbiPack.zAbilityPack.bAlwaysCrits) then gbAttackCrit = true end
        end
    
    -- |[Always Hits]|
    --Attack always hits, save some time and don't run the subroutine. Ignoring accuracy means the ability only crits
    -- if explicitly flagged to.
    else
    
        --Basic hit flag.
        gbAttackHit = true
        gbAttackCrit = false
        gbAttackGlances = false
        
        --Crit can be caused by the ability always critting, or a tag causing a crit. It can be denied by the never-crit flag.
        if(gzAbiPack.zAbilityPack.bAlwaysCrits) then gbAttackCrit = true  end
        if(gzAbiPack.bAlwaysCrit)               then gbAttackCrit = true  end
        if(gzAbiPack.zAbilityPack.bNeverCrits)  then gbAttackCrit = false end
    end
end