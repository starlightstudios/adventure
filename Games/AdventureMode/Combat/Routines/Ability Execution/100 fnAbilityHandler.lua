-- |[ ===================================== Ability Handler ==================================== ]|
--"Standard" implementation of the gciAbility_Execute code for abilities. Requires the index of
-- the target, an ability package, and optional effect packages. The effect packages can be nil.
--Returns how many ticks were needed for everything to run.
--This algorithm is broken into multiple sub-handlers as it is quite complicated. To make things
-- easier, a single global object stores the temporary variables.
function fnAbilityHandler(piOriginatorID, piTargetID, piExecIndex, pzExecPack, pzaEffectPacks)
    
    -- |[ ==================== Variable Reset ==================== ]|
    --Diagnostics.
    gzDiagnostics.fnCombatExec(gciCombatExec_Diagnostic_Announce, "[===== Running fnAbilityHandler() =====]\n")
    
    --Argument Diagnostics.
    if((giCombatExec_Diagnostic_Level & gciCombatExec_Diagnostic_Arguments) > 0) then
        
        --Get names.
        AdvCombat_SetProperty("Push Entity By ID", piOriginatorID)
            local sOriginatorName = AdvCombatEntity_GetProperty("Display Name")
        DL_PopActiveObject()
        AdvCombat_SetProperty("Push Entity By ID", piTargetID)
            local sTargetName = AdvCombatEntity_GetProperty("Display Name")
        DL_PopActiveObject()
        
        io.write(" Originator: " .. piOriginatorID .. " (" .. sOriginatorName .. ")\n")
        io.write(" Target ID:  " .. piTargetID .. " (" .. sTargetName .. ")\n")
        io.write(" Exec Index: " .. piExecIndex .. "\n")
    end
    
    --Reset all variables in the ability pack.
    pzExecPack.iHealingFinal     = 0
    pzExecPack.iShieldsFinal     = 0
    pzExecPack.iSelfHealingFinal = 0
    
    --Create a single global package to store all local variables. This can be accessed by execution subroutines.
    fnInitializeAbilityHandlerPack()
    
    --Initialize the values from the arguments.
    gzAbiPack.iTargetID     = piTargetID
    gzAbiPack.iOriginatorID = piOriginatorID
    gzAbiPack.iExecIndex    = piExecIndex
    gzAbiPack.zAbilityPack  = pzExecPack
    gzAbiPack.zaEffectPacks = pzaEffectPacks
    
    --Clone timers from the ability package into the gzAbiPack base, allowing scripts to modify them during execution.ExecStatPack.zaTimerFactors.fStaggerFactor      = 1.0
    gzAbiPack.zaTimerFactors.fStaggerFactor      = pzExecPack.zaTimerFactors.fStaggerFactor     
    gzAbiPack.zaTimerFactors.fBlackFlash         = pzExecPack.zaTimerFactors.fBlackFlash        
    gzAbiPack.zaTimerFactors.fVoiceCalloutPre    = pzExecPack.zaTimerFactors.fVoiceCalloutPre   
    gzAbiPack.zaTimerFactors.fAnimation          = pzExecPack.zaTimerFactors.fAnimation         
    gzAbiPack.zaTimerFactors.fVoiceCalloutPost   = pzExecPack.zaTimerFactors.fVoiceCalloutPost  
    gzAbiPack.zaTimerFactors.fText               = pzExecPack.zaTimerFactors.fText              
    gzAbiPack.zaTimerFactors.fDamage             = pzExecPack.zaTimerFactors.fDamage            
    gzAbiPack.zaTimerFactors.fVoiceCalloutDamage = pzExecPack.zaTimerFactors.fVoiceCalloutDamage
    gzAbiPack.zaTimerFactors.fHealing            = pzExecPack.zaTimerFactors.fHealing           
    gzAbiPack.zaTimerFactors.fShields            = pzExecPack.zaTimerFactors.fShields           
    gzAbiPack.zaTimerFactors.fMPGeneration       = pzExecPack.zaTimerFactors.fMPGeneration      
    gzAbiPack.zaTimerFactors.fSelfHealing        = pzExecPack.zaTimerFactors.fSelfHealing       
    gzAbiPack.zaTimerFactors.fMPRestore          = pzExecPack.zaTimerFactors.fMPRestore         
    gzAbiPack.zaTimerFactors.fStun               = pzExecPack.zaTimerFactors.fStun              
    gzAbiPack.zaTimerFactors.fEffect             = pzExecPack.zaTimerFactors.fEffect            
    gzAbiPack.zaTimerFactors.fJobChange          = pzExecPack.zaTimerFactors.fJobChange
    
    --Reset the global effect lists
    gzaTagEffectsHostile = {}
    
    --If the effect pack list is nil (no effects provided) make it an empty list.
    if(gzAbiPack.zaEffectPacks == nil) then gzAbiPack.zaEffectPacks = {} end
    
    -- |[ ================== Statistics Storage ================== ]|
    --Diagnostics.
    gzDiagnostics.fnCombatExec(gciCombatExec_Diagnostic_Announce, " Storing statistics.\n")
    
    --Get the last-added statistics package. All statistics get stored in that package. Do not assume
    -- that this is the first or only time that package is getting updated.
    local iTotalPacks = #gzaCombatAbilityStatistics.zaStatisticsPacks
    gzAbiPack.zStatPack = gzaCombatAbilityStatistics.zaStatisticsPacks[iTotalPacks]
    
    --If no stat pack exists, create one here. This may happen with abilities that trigger before combat
    -- initializes normally, such as Ambush or From The Shadows.
    if(gzAbiPack.zStatPack == nil or iTotalPacks == 0) then
        gzAbiPack.zStatPack = fnCreateCombatStatisticsPack()
        table.insert(gzaCombatAbilityStatistics.zaStatisticsPacks, gzAbiPack.zStatPack)
        gzAbiPack.zStatPack.iTurnElapsed = gzaCombatAbilityStatistics.iTurnsElapsed
    end
    
    --Store user info.
    gzAbiPack.zStatPack.iActorID = gzAbiPack.iOriginatorID
    AdvCombat_SetProperty("Push Entity By ID", gzAbiPack.iOriginatorID)
        gzAbiPack.zStatPack.sActorInternalName = AdvCombatEntity_GetProperty("Internal Name")
        gzAbiPack.zStatPack.sActorDisplayName  = AdvCombatEntity_GetProperty("Display Name")
    DL_PopActiveObject()
    
    -- |[ ================ Information Gathering ================= ]|
    -- |[ ======== Target Redirection ======== ]|
    --Check tags/skills/etc to see if anything forces the target to change. For example, "Cover" can cause a character
    -- to take a hit in place of an ally.
    gzDiagnostics.fnCombatExec(gciCombatExec_Diagnostic_Announce, " Running fnTargetRedirect().\n")
    gzaAbiHandlerFuncs.fnTargetRedirect()
    
    -- |[ ======= Weapon Info Resolve ======== ]|
    --If the ability is flagged to use weapon damage, resolve animations here.
    gzDiagnostics.fnCombatExec(gciCombatExec_Diagnostic_Announce, " Running fnWeaponInfoResolve().\n")
    gzaAbiHandlerFuncs.fnWeaponInfoResolve()

    -- |[ ============ Target Info =========== ]|
    --From here, it is assumed the target is the stored ID, regardless of being redirected. This function resolves
    -- remaining information from the target and stores it in the global structure.
    gzDiagnostics.fnCombatExec(gciCombatExec_Diagnostic_Announce, " Running fnTargetInfoResolve().\n")
    gzaAbiHandlerFuncs.fnTargetInfoResolve()
    
    -- |[ =========== Tag Handling =========== ]|
    --Tags may edit the properties of the ability, causing it to deal extra damage, always hit, always crit, etc.
    -- This function checks tags affecting the target/originator and edits properties accordingly.
    gzDiagnostics.fnCombatExec(gciCombatExec_Diagnostic_Announce, " Running fnHandleTags().\n")
    gzaAbiHandlerFuncs.fnHandleTags()
    
    -- |[ ============= Accuracy ============= ]|
    --Function call will populate the globals gbAttackHit and gbAttackCrit with their values, applying special
    -- properties like "Always Hit" and "Always Crit" as needed.
    gzDiagnostics.fnCombatExec(gciCombatExec_Diagnostic_Announce, " Running fnHandleAccuracy().\n")
    gzaAbiHandlerFuncs.fnHandleAccuracy()
    
    --Special tag: If the attack crits, this can generate extra CP for the user.
    AdvCombat_SetProperty("Push Entity By ID", gzAbiPack.iOriginatorID)
        local iCPBonusTags = AdvCombatEntity_GetProperty("Tag Count", "+CP On Crit")
        if(gbAttackCrit == true and iCPBonusTags > 0) then
            fnModifyCP(iCPBonusTags)
        end
    DL_PopActiveObject()
    
    -- |[ ======================== Missed ======================== ]|
    --Attack missed. This ends execution.
    if(gbAttackHit == false) then
        gzDiagnostics.fnCombatExec(gciCombatExec_Diagnostic_Announce, " Running fnHandleMiss().\n")
        return gzaAbiHandlerFuncs.fnHandleMiss()
    end

    -- |[ ===================== Computations ===================== ]|
    --The ability hit its target, either because it passed the accuracy roll or never misses. In any case, go
    -- through all the modules and calculate damage, healing, lifesteal, etc.

    --Damage.
    gzDiagnostics.fnCombatExec(gciCombatExec_Diagnostic_Announce, " Running fnComputeDamage().\n")
    gzaAbiHandlerFuncs.fnComputeDamage()

    --Lifesteal. Must be computed after damage.
    gzDiagnostics.fnCombatExec(gciCombatExec_Diagnostic_Announce, " Running fnComputeLifesteal().\n")
    gzaAbiHandlerFuncs.fnComputeLifesteal()

    --Healing.
    gzDiagnostics.fnCombatExec(gciCombatExec_Diagnostic_Announce, " Running fnComputeHealing().\n")
    gzaAbiHandlerFuncs.fnComputeHealing()

    --Shields.
    gzDiagnostics.fnCombatExec(gciCombatExec_Diagnostic_Announce, " Running fnComputeShields().\n")
    gzaAbiHandlerFuncs.fnComputeShields()

    --Stun.
    gzDiagnostics.fnCombatExec(gciCombatExec_Diagnostic_Announce, " Running fnComputeStun().\n")
    gzaAbiHandlerFuncs.fnComputeStun()

    --Effects. Includes generating effects from tags.
    gzDiagnostics.fnCombatExec(gciCombatExec_Diagnostic_Announce, " Running fnComputeEffects().\n")
    gzaAbiHandlerFuncs.fnComputeEffects()

    -- |[ ================= Application Sequence ================= ]|
    --Finally, apply everything and run the associated animations.
    gzDiagnostics.fnCombatExec(gciCombatExec_Diagnostic_Announce, " Running fnApplicationSequence().\n")
    local iTicksElapsed = gzaAbiHandlerFuncs.fnApplicationSequence()
    
    -- |[ ====================== Finish Up ======================= ]|
    --Remove expired effects as marked by tags.
    gzDiagnostics.fnCombatExec(gciCombatExec_Diagnostic_Announce, " Running fnRemoveExpiredEffectsFromTags().\n")
    gzaAbiHandlerFuncs.fnRemoveExpiredEffectsFromTags()
    
    --This algorithm notifies the combat handler that we're done wtih execution at the given number of ticks.
    gzDiagnostics.fnCombatExec(gciCombatExec_Diagnostic_Announce, " Running fnDefaultEndOfApplications().\n")
    fnDefaultEndOfApplications(iTicksElapsed)
    return iTicksElapsed
end
