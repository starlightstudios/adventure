-- |[ ================================ fnApplicationSequence() ================================= ]|
--Once all computations are done, applies everything and causes animations where necessary.
--Returns how many ticks elapsed.
function gzaAbiHandlerFuncs.fnApplicationSequence()

    -- |[ ==================== Setup =================== ]|
    --The timer tracks when the application goes off. If multiple packs are executing, each execution
    -- is staggered behind the previous one.
    local iTimer = math.floor(gciAbility_TicksOffsetPerTarget * gzAbiPack.iExecIndex * gzAbiPack.zaTimerFactors.fStaggerFactor) + gzAbiPack.iTimerOffset
    
    -- |[ ================= Black Flash ================ ]|
    -- Only occurs if flagged and on the 0th target. Enemies use this to indicate they are acting.
    if(gzAbiPack.zAbilityPack.bCauseBlackFlash == true and gzAbiPack.iExecIndex == 0) then
        AdvCombat_SetProperty("Register Application Pack", gzAbiPack.iOriginatorID, gzAbiPack.iOriginatorID, piTimer, "Flash Black")
        iTimer = ExecStatPack:fnAdvanceTimer(iTimer, gciAbility_BlackFlashTicks, gzAbiPack.zaTimerFactors.fBlackFlash)
    end
    
    -- |[ =================== Title ==================== ]|
    --Optional, shows the name of the ability. Can be nil or "Null" to do nothing. Enemies often
    -- use these to indicate a special attack.
    if(gzAbiPack.zAbilityPack.sShowTitle ~= nil and gzAbiPack.zAbilityPack.sShowTitle ~= "Null") then
        AdvCombat_SetProperty("Register Application Pack", gzAbiPack.iOriginatorID, gzAbiPack.iOriginatorID, iTimer, "Title|" .. gzAbiPack.zAbilityPack.sShowTitle)
    end

    -- |[ =============== Voice Callout ================ ]|
    --If this path is not nil, run it to resolve a voice callout. This callout should register an application pack to play a sound, but nominally
    -- it can do whatever. Just don't crash the damn engine.
    --This is the "pre-animation" callout. Another one runs after the animation.
    giAdjustActionTimerForCallout = 0
    if(gsGlobalVoicePath ~= nil) then
        LM_ExecuteScript(gsGlobalVoicePath, iTimer, gciPreAnimCallout)
    end
    
    --If this value was set, adjust the timer by it.
    iTimer = ExecStatPack:fnAdvanceTimer(iTimer, giAdjustActionTimerForCallout, gzAbiPack.zaTimerFactors.fVoiceCalloutPre)
    
    -- |[ ================ Animation(s) ================ ]|
    --Animate the ability, if it exists.
    if(gzAbiPack.zAbilityPack.bDoesNotAnimate == false) then
        
        --Starting timer.
        local iStartingTimer = iTimer
        
        --Primary animation. Advances the timer.
        local iNewTimer = gzAbiPack.zAbilityPack.fnAbilityAnimate(gzAbiPack.iTargetID, iTimer, gzAbiPack.zAbilityPack, gbAttackCrit)
        iTimer = ExecStatPack:fnDiffTimer(iTimer, iNewTimer, gzAbiPack.zaTimerFactors.fAnimation)
        
        --Any secondary animations (there can be multiple) do not advance the timer.
        if(gzAbiPack.zAbilityPack.zaAdditionalAnimations ~= nil) then
            
            --Iterate across the animations and apply them.
            for i = 1, #gzAbiPack.zAbilityPack.zaAdditionalAnimations, 1 do
                
                --Variables
                local iTimerOffset = gzAbiPack.zAbilityPack.zaAdditionalAnimations[i].iTimerOffset
                local sAnimName    = gzAbiPack.zAbilityPack.zaAdditionalAnimations[i].sAnimName
                local fXOffset     = gzAbiPack.zAbilityPack.zaAdditionalAnimations[i].fXOffset
                local fYOffset     = gzAbiPack.zAbilityPack.zaAdditionalAnimations[i].fYOffset
                
                --String construction
                local sString = "Create Animation|" .. sAnimName .. "|AttackAnim" .. i
                
                --Optional argument:
                if(fXOffset ~= 0.0) then
                    sString = sString .. "|OffX:" .. fXOffset
                end
                if(fYOffset ~= 0.0) then
                    sString = sString .. "|OffY:" .. fYOffset
                end
                
                --Upload:
                AdvCombat_SetProperty("Register Application Pack", gzAbiPack.iOriginatorID, gzAbiPack.iTargetID, iStartingTimer + iTimerOffset, sString)
            end
        end
    end

    -- |[ =============== Voice Callout ================ ]|
    --Post animation callout, goes with a different code.
    giAdjustActionTimerForCallout = 0
    if(gsGlobalVoicePath ~= nil) then
        LM_ExecuteScript(gsGlobalVoicePath, iTimer, gciPostAnimCallout)
    end
    
    --If this value was set, adjust the timer by it.
    iTimer = ExecStatPack:fnAdvanceTimer(iTimer, giAdjustActionTimerForCallout, gzAbiPack.zaTimerFactors.fVoiceCalloutPost)
    giAdjustActionTimerForCallout = 0
    
    -- |[ ================= Summoning ================== ]|
    --Summons entities into battle.
    local bSummonedAnything = false
    for i = 1, #gzAbiPack.zAbilityPack.saSummonHandlerScripts, 1 do
        bSummonedAnything = true
        local sEnemyString = "LM_ExecuteScript(\"" .. gzAbiPack.zAbilityPack.saSummonHandlerScripts[i] .. "\", \"" .. gzAbiPack.zAbilityPack.saSummonHandlerNames[i] .. "\")"
        fnCutscene(sEnemyString)
    end
    
    --If anything was summoned, reorganize entities here.
    if(bSummonedAnything) then
        AdvCombat_SetProperty("Position Enemies Normally")
    end
    
    -- |[ =================== Damage =================== ]|
    --If the ability deals damage, handle that here.
    if(gzAbiPack.zAbilityPack.bDoesNoDamage == false) then
        
        --If the attack did zero damage, indicate immunity.
        if(giStandardDamage == 0) then
            if(gzAbiPack.zAbilityPack.bDoesNotAnimateDamage == false) then
                AdvCombat_SetProperty("Register Application Pack", gzAbiPack.iOriginatorID, gzAbiPack.iTargetID, iTimer, "Play Sound|Combat\\|Glance")
                AdvCombat_SetProperty("Register Application Pack", gzAbiPack.iOriginatorID, gzAbiPack.iTargetID, iTimer, "Text|Immune!")
                iTimer = ExecStatPack:fnAdvanceTimer(iTimer, gciApplication_TextTicks, gzAbiPack.zaTimerFactors.fText)
            end
        
        --Normal case:
        else
        
            --Glance/Critical handlers.
            local sExtraString = nil
            if(gbAttackGlances) then sExtraString = "Glance" end
            if(gbAttackCrit) then sExtraString = "Critical" end
            
            --Execute.
            local iNewTimer = gzAbiPack.zAbilityPack.fnDamageAnimate(gzAbiPack.iOriginatorID, gzAbiPack.iTargetID, iTimer, giStandardDamage, sExtraString)
            iTimer = ExecStatPack:fnDiffTimer(iTimer, iNewTimer, gzAbiPack.zaTimerFactors.fDamage)
            
            --Store damage in statistics pack.
            gzAbiPack.zTargetInfo.iDamageDealt = gzAbiPack.zTargetInfo.iDamageDealt + giStandardDamage
            
            --Pain SFX.
            giAdjustActionTimerForCallout = 0
            if(gsGlobalVoicePath ~= nil) then
                LM_ExecuteScript(gsGlobalVoicePath, iTimer, gciPainCallout)
            end
            
            --If this value was set, adjust the timer by it.
            iTimer = ExecStatPack:fnAdvanceTimer(iTimer, giAdjustActionTimerForCallout, gzAbiPack.zaTimerFactors.fVoiceCalloutDamage)
        end
    end
    
    -- |[ ================= Influence ================== ]|
    --If the attack inflicts Influence, handle that here.
    if(gzAbiPack.zAbilityPack.iInfluenceDamage ~= nil and gzAbiPack.zAbilityPack.iInfluenceDamage > 0) then
            
        --Execute.
        iTimer = gzAbiPack.zAbilityPack.fnInfluenceAnimate(gzAbiPack.iOriginatorID, gzAbiPack.iTargetID, iTimer, gzAbiPack.zAbilityPack.iInfluenceDamage)
        gzAbiPack.zTargetInfo.iInfluenceDealt = gzAbiPack.zTargetInfo.iInfluenceDealt + gzAbiPack.zAbilityPack.iInfluenceDamage
        
        --io.write("Apply Influence " .. gzAbiPack.zAbilityPack.iInfluenceDamage .. "\n")
        
        --Pain SFX.
        --giAdjustActionTimerForCallout = 0
        --if(gsGlobalVoicePath ~= nil) then
        --    LM_ExecuteScript(gsGlobalVoicePath, iTimer, gciPainCallout)
        --end
        
        --If this value was set, adjust the timer by it.
        --iTimer = iTimer + giAdjustActionTimerForCallout

    end
    
    -- |[ ====== Healing / Shields / MP Generation ===== ]|
    --If the ability has healing, handle that here.
    if(gzAbiPack.zAbilityPack.iHealingFinal > 0) then
        local iNewTimer = gzAbiPack.zAbilityPack.fnHealingAnimate(gzAbiPack.iOriginatorID, gzAbiPack.iTargetID, iTimer, gzAbiPack.zAbilityPack.iHealingFinal)
        iTimer = ExecStatPack:fnDiffTimer(iTimer, iNewTimer, gzAbiPack.zaTimerFactors.fHealing)
        gzAbiPack.zTargetInfo.iHealingDealt = gzAbiPack.zTargetInfo.iHealingDealt + gzAbiPack.zAbilityPack.iHealingFinal
    end
    
    --If the ability applies shields, handle that here.
    if(gzAbiPack.zAbilityPack.iShieldsFinal > 0) then
        local iNewTimer = gzAbiPack.zAbilityPack.fnShieldsAnimate(gzAbiPack.iOriginatorID, gzAbiPack.iTargetID, iTimer, gzAbiPack.zAbilityPack.iShieldsFinal)
        iTimer = ExecStatPack:fnDiffTimer(iTimer, iNewTimer, gzAbiPack.zaTimerFactors.fShields)
    end
    
    --If the ability generates MP for the target, handle that here.
    if(gzAbiPack.zAbilityPack.iMPGeneration > 0) then
        local iNewTimer = gzAbiPack.zAbilityPack.fnMPGainAnimate(gzAbiPack.iOriginatorID, gzAbiPack.iTargetID, iTimer, gzAbiPack.zAbilityPack.iMPGeneration)
        iTimer = ExecStatPack:fnDiffTimer(iTimer, iNewTimer, gzAbiPack.zaTimerFactors.fMPGeneration)
    end
    
    --If the ability has self-healing, handle that here.
    if(gzAbiPack.zAbilityPack.iSelfHealingFinal > 0) then
        local iNewTimer = gzAbiPack.zAbilityPack.fnHealingAnimate(gzAbiPack.iOriginatorID, gzAbiPack.iOriginatorID, iTimer, gzAbiPack.zAbilityPack.iSelfHealingFinal)
        iTimer = ExecStatPack:fnDiffTimer(iTimer, iNewTimer, gzAbiPack.zaTimerFactors.fSelfHealing)
    end
    
    --If the ability restores MP, handle that here.
    if(gzAbiPack.zAbilityPack.iMPRestore > 0) then
        local iNewTimer = gzAbiPack.zAbilityPack.fnMPGainAnimate(gzAbiPack.iOriginatorID, gzAbiPack.iOriginatorID, iTimer, gzAbiPack.zAbilityPack.iMPRestore)
        iTimer = ExecStatPack:fnDiffTimer(iTimer, iNewTimer, gzAbiPack.zaTimerFactors.fMPRestore)
    end
    
    -- |[ ==================== Stun ==================== ]|
    --If the ability applies stun, do that here. Note that an ability that stuns on crit but has a base 0 will not
    -- animate stun unless the value was nonzero.
    --Unstunnable targets also never animate.
    if(gzAbiPack.zAbilityPack.bDoesNoStun == false and gzAbiPack.bIsTargetStunnable == true) then
        local iNewTimer = gzAbiPack.zAbilityPack.fnStunAnimate(gzAbiPack.iOriginatorID, gzAbiPack.iTargetID, iTimer, gzAbiPack.iFinalStun)
        iTimer = ExecStatPack:fnDiffTimer(iTimer, iNewTimer, gzAbiPack.zaTimerFactors.fStun)
    end
    
    -- |[ =================== Effects ================== ]|
    --If any effects applied (or failed to apply), handle that here.
    for i = 1, #gzAbiPack.zaEffectPacks, 1 do
        local iNewTimer = gzAbiPack.zaEffectPacks[i].fnHandler(gzAbiPack.iOriginatorID, gzAbiPack.iTargetID, iTimer, gzAbiPack.zaEffectPacks[i], gzAbiPack.baEffectsApplied[i], gbAttackCrit)
        iTimer = ExecStatPack:fnDiffTimer(iTimer, iNewTimer, gzAbiPack.zaTimerFactors.fEffect)
    end
    
    --If any generated effects exist, apply them.
    for i = 1, #gzAbiPack.zaTagEffectsHostile, 1 do
        local iNewTimer = gzAbiPack.zaTagEffectsHostile[i].fnHandler(gzAbiPack.iOriginatorID, gzAbiPack.iTargetID, iTimer, gzAbiPack.zaTagEffectsHostile[i], true, gbAttackCrit)
        iTimer = ExecStatPack:fnDiffTimer(iTimer, iNewTimer, gzAbiPack.zaTimerFactors.fEffect)
    end
    
    -- |[ ================ Job Changing ================ ]|
    --Job Change
    if(gzAbiPack.zAbilityPack.sChangeJobTo ~= "Null") then
        local iNewTimer = gzAbiPack.zAbilityPack.fnJobAnimate(gzAbiPack.iOriginatorID, gzAbiPack.iTargetID, iTimer, gzAbiPack.zAbilityPack.sChangeJobTo)
        iTimer = ExecStatPack:fnDiffTimer(iTimer, iNewTimer, gzAbiPack.zaTimerFactors.fJobChange)
    end
    
    -- |[ ============= Special Execution ============== ]|
    --Miscellaneous properties are done via special scripts. Execute those here.
    local iTotal = #gzAbiPack.zAbilityPack.saExecPaths
    for i = 1, iTotal, 1 do
        
        --Resolve the code. If it's nil, it defaults to gciAbility_SpecialStart.
        local iCode = gzAbiPack.zAbilityPack.iaExecCodes[i]
        if(iCode == nil) then iCode = gciAbility_SpecialStart end
        
        --Immediate codes:
        if(iCode ~= gciAbility_SpecialLastExec) then
            giStoredTimer = iTimer
            LM_ExecuteScript(gzAbiPack.zAbilityPack.saExecPaths[i], iCode, gzAbiPack.iOriginatorID, gzAbiPack.iTargetID)
            iTimer = giStoredTimer
        
        --These execute internally when the timer reaches the given value.
        else
            giStoredTimer = iTimer
            AdvCombat_SetProperty("Register Application Pack", gzAbiPack.iOriginatorID, gzAbiPack.iTargetID, iTimer, "Run Script|" .. gzAbiPack.zAbilityPack.saExecPaths[i] .. "|N:1002")
            iTimer = giStoredTimer
        
        end
    end

    -- |[ ================== Finish Up ================= ]|
    return iTimer
end
