-- |[ =================================== fnComputeDamage() ==================================== ]|
--Once the ability lands, check if it does any damage and compute how much it did. Application is not
-- immediate, the value is stored for later.
function gzaAbiHandlerFuncs.fnComputeDamage()
    
    -- |[ ============================= Setup ============================== ]|
    -- |[Diagnostics]|
    gzDiagnostics.fnCombatExec(gciCombatExec_Diagnostic_Damage, "fnComputeDamage() - Executing.\n")
    
    -- |[Application Check]|
    --If the ability does no damage, stop here.
    if(gzAbiPack.zAbilityPack.bDoesNoDamage == true) then
        gzDiagnostics.fnCombatExec(gciCombatExec_Diagnostic_Damage, "Completed. Ability deals no damage.\n")
        return
    end
    
    -- |[Mandate Damage]|
    --Ability does a specific amount of damage determined elsewhere.
    if(gzAbiPack.zAbilityPack.bMandateDamage == true) then
        giStandardDamage = gzAbiPack.zAbilityPack.iMandatedDamage
        gzDiagnostics.fnCombatExec(gciCombatExec_Diagnostic_Damage, "Completed. Ability is dealing mandated damage.\n")
        gzDiagnostics.fnCombatExec(gciCombatExec_Diagnostic_Damage, "Mandated damage: " .. gzAbiPack.zAbilityPack.iMandatedDamage .. "\n")
        return
    end

    -- |[Type Resolve]|
    --Weapon-type Damage vs Specific Damage. These functions will compute the damage and place it in a global storage for later.
    if(gzAbiPack.zAbilityPack.bUseWeapon) then
        fnStandardAttack(gzAbiPack.iOriginatorID, gzAbiPack.iTargetID, gzAbiPack.zAbilityPack.iScatterRange, gzAbiPack.zAbilityPack.bBypassProtection, gzAbiPack.zAbilityPack.iPenetration)
    else
        fnStandardDamageType(gzAbiPack.iOriginatorID, gzAbiPack.iTargetID, gzAbiPack.zAbilityPack.iDamageType, gzAbiPack.zAbilityPack.iScatterRange, gzAbiPack.zAbilityPack.bBypassProtection, gzAbiPack.zAbilityPack.iPenetration)
    end
    
    --Report.
    gzDiagnostics.fnCombatExec(gciCombatExec_Diagnostic_Damage, "Executed damage type resolver.\n")
    gzDiagnostics.fnCombatExec(gciCombatExec_Diagnostic_Damage, "Damage factor: " .. gzAbiPack.zAbilityPack.fDamageFactor .. "\n")
    
    -- |[Damage Factor]|
    --Base damage factor.
    local fDamageFactor = gzAbiPack.zAbilityPack.fDamageFactor
    
    --Special tags.
    for i = 1, #gzAbiPack.zaLocalEffectsFriendly, 1 do
        if(gzAbiPack.zaLocalEffectsFriendly[i][1] == "Double Power") then
            fDamageFactor = fDamageFactor * 2.0
        end
    end
    
    -- |[Item Power Bonus]|
    --If flagged, the damage factor gets a bonus from item power tags.
    if(gzAbiPack.zAbilityPack.bDamageBenefitsFromItemPower == true) then
        
        --Get the item power factor.
        AdvCombat_SetProperty("Push Entity By ID", gzAbiPack.iOriginatorID)
            local iItemPowerBonus = AdvCombatEntity_GetProperty("Tag Count", "Item Power +")
            local iItemPowerMalus = AdvCombatEntity_GetProperty("Tag Count", "Item Power -")
            local fItemPowerFactor =  1.00 + (0.01 * (iItemPowerBonus - iItemPowerMalus))
        DL_PopActiveObject()
        
        --Multiply the damage factor by the item power factor.
        fDamageFactor = fDamageFactor * fItemPowerFactor
        
        --Report.
        gzDiagnostics.fnCombatExec(gciCombatExec_Diagnostic_Damage, "Ran item power bonus.\n")
    end

    -- |[Chasers]|
    --Damage bonus.
    local iDamageBonusFromChasers = 0.0
    local fDamageFactorFromChasers = 0.0
    
    --Check if there are any chaser effects in play.
    if(gzAbiPack.zAbilityPack.bHasChaserModules == true) then
        
        --Report.
        gzDiagnostics.fnCombatExec(gciCombatExec_Diagnostic_Damage, "Checking chasers.\n")
        
        --Push the target as the active object.
        AdvCombat_SetProperty("Push Entity By ID", gzAbiPack.iTargetID)

            --Iterate across chasers.
            for i = 1, gzAbiPack.zAbilityPack.iChaserModulesTotal, 1 do
            
                --Shorthand.
                local zModule = gzAbiPack.zAbilityPack.zaChaserModules[i]
                
                --Check how many times the dot tag appears on the target.
                local iDotTagCount = AdvCombatEntity_GetProperty("Tag Count", zModule.sDoTTag)
                --io.write("Checked tag " .. zModule.sDoTTag .. " and found " .. iDotTagCount .. " matching tags.\n")
            
                --Modify the damage factor used by the damage module for each tag found.
                fDamageFactorFromChasers = fDamageFactorFromChasers + (zModule.fDamageFactorChangePerDoT * iDotTagCount)
            
                --If the damage remaining factor is not zero, add a part of the damage remaining to the damage output.
                if(zModule.fDamageRemainingFactor ~= 0.0) then
                    
                    --Iterate across all effects:
                    local iEffectsTotal = AdvCombatEntity_GetProperty("Effects With Tag", zModule.sDoTTag)
                    for p = 0, iEffectsTotal-1, 1 do
                    
                        --Get the script this effect uses.
                        local iEffectID = AdvCombatEntity_GetProperty("Effect ID With Tag", zModule.sDoTTag, p)
                        AdvCombat_SetProperty("Push Effect", iEffectID)
                            local sEffectScript = AdvCombatEffect_GetProperty("Script")
                            local sEffectPrototypeName = AdvCombatEffect_GetProperty("Global Prototype Name")
                        DL_PopActiveObject()
                        
                        --If the effect prototype exists, mark that.
                        if(sEffectPrototypeName ~= "Null") then
                            AdvCombatEffect_SetProperty("Static Prototype Name", sEffectPrototypeName)
                        end
                        
                        --Call it with the compute DoT code.
                        giEffect_LastComputedDotDamage = 0
                        LM_ExecuteScript(sEffectScript, gciEffect_DotComputeDamage, iEffectID)
                        
                        --Add the damage by its factor.
                        iDamageBonusFromChasers = iDamageBonusFromChasers + math.floor(giEffect_LastComputedDotDamage * zModule.fDamageRemainingFactor)
                        
                        --Clean.
                        AdvCombatEffect_SetProperty("Static Prototype Name", "Null")
                    end
                end
            
                --If flagged, remove all effects with the matching tag.
                if(zModule.bConsumeEffect == true) then
                    
                    --Iterate across all effects, storing the IDs that need to be removed.
                    local iaEffectRemoveList = {}
                    local iEffectsTotal = AdvCombatEntity_GetProperty("Effects With Tag", zModule.sDoTTag)
                    for p = 0, iEffectsTotal-1, 1 do
                        local iEffectID = AdvCombatEntity_GetProperty("Effect ID With Tag", zModule.sDoTTag, p)
                        table.insert(iaEffectRemoveList, iEffectID)
                    end
                    
                    for p = 1, #iaEffectRemoveList, 1 do
                        AdvCombat_SetProperty("Remove Effect", iaEffectRemoveList[p])
                    end
                end
            end
        DL_PopActiveObject()
    end
    
    --Compute damage with finalized factor.
    fDamageFactor = fDamageFactor + fDamageFactorFromChasers
    giStandardDamage = giStandardDamage * fDamageFactor
    
    --Damage is tripled in tourist mode, though it could still be 0 in theory.
    if(gzAbiPack.bIsTouristMode) then
        giStandardDamage = giStandardDamage * 3.0
    end
    
    --Add damage bonus from chasers.
    giStandardDamage = giStandardDamage + iDamageBonusFromChasers
    
    --Report.
    gzDiagnostics.fnCombatExec(gciCombatExec_Diagnostic_Damage, "Computed damage after all steps: " .. giStandardDamage .. "\n")
    
    -- |[Clamping]|
    --If the damage came back zero, and we're flagged to deal at least one damage, AND the target was not immune to at least
    -- one damage type used:
    if(giStandardDamage == 0 and gzAbiPack.zAbilityPack.bDealsOneDamageMinimum and gbStandardImmune == false) then
        giStandardDamage = 1
    end
    
    --Report.
    gzDiagnostics.fnCombatExec(gciCombatExec_Diagnostic_Damage, "After clamps: " .. giStandardDamage .. "\n")

    -- |[Glancing Blows]|
    --If the ability glanced, it does half damage.
    if(gbAttackGlances == true and giStandardDamage > 0) then
        giStandardDamage = math.floor(giStandardDamage * 0.5)
        if(giStandardDamage < 1) then giStandardDamage = 1 end
        
        --Store glance in the statistics.
        gzAbiPack.zTargetInfo.bWasGlance = true
    
        --Report.
        gzDiagnostics.fnCombatExec(gciCombatExec_Diagnostic_Damage, "Glanced. Damage is: " .. giStandardDamage .. "\n")

    -- |[Critical Strikes]|
    --If the ability critted, multiply damage by the critical factor.
    elseif(gbAttackCrit == true and giStandardDamage > 0) then
    
        --Tags can boost critical damage factor.
        AdvCombat_SetProperty("Push Entity By ID", gzAbiPack.iOriginatorID)
            local fCritDamageBonus = AdvCombatEntity_GetProperty("Tag Count", "Critical Damage Bonus") * 0.01
            local fCritDamageMalus = AdvCombatEntity_GetProperty("Tag Count", "Critical Damage Malus") * 0.01
        DL_PopActiveObject()
        
        --Compute bonus.
        local fTotalBonus = gzAbiPack.zAbilityPack.fCriticalFactor + fCritDamageBonus - fCritDamageMalus
        if(fTotalBonus < 1.0) then fTotalBonus = 1.0 end
    
        --Apply.
        giStandardDamage = math.floor(giStandardDamage * fTotalBonus)
        
        --Store crit in the statistics.
        gzAbiPack.zTargetInfo.bWasCritical = true
    
        --Report.
        gzDiagnostics.fnCombatExec(gciCombatExec_Diagnostic_Damage, "Critical strike. Damage is: " .. giStandardDamage .. "\n")
    end
end
