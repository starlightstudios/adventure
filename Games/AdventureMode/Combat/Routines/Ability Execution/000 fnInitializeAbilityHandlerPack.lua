-- |[ ============================ fnInitializeAbilityHandlerPack() ============================ ]|
--Offloaded to a class with better documentation.
function fnInitializeAbilityHandlerPack()
    gzAbiPack = ExecStatPack:new()
    return gzAbiPack
end