-- |[ ================================== fnWeaponInfoResolve =================================== ]|
--If an ability derives any properties from the attacker's weapon, this routine resolve them and
-- populates the global structure with the needed data.
function gzaAbiHandlerFuncs.fnWeaponInfoResolve()
    
    -- |[Error Check]|
    --Do nothing if the ability pack doesn't use weapon info.
    if(gzAbiPack.zAbilityPack.bUseWeapon == false) then return end
    
    -- |[Fast-Access Pointers]|
    local zPack = gzAbiPack.zAbilityPack
    
    -- |[Entity Info]|
    --Push acting entity.
    AdvCombat_SetProperty("Push Entity By ID", gzAbiPack.iOriginatorID)

        --Get which slot is used for weapon damage.
        local sWeaponSlot = AdvCombatEntity_GetProperty("Slot For Weapon Damage")
        if(sWeaponSlot ~= "Null") then
        
            --See if the slot has something in it.
            local sEquipmentName = AdvCombatEntity_GetProperty("Equipment In Slot S", sWeaponSlot)
            if(sEquipmentName ~= "Null") then
    
                --If we got this far, we have a weapon slot active and it has something in it. Next, push it and query it.
                AdvCombatEntity_SetProperty("Push Item In Slot S", sWeaponSlot)
                    zPack.sAttackAnimation = AdItem_GetProperty("Attack Animation")
                    zPack.sAttackSound     = AdItem_GetProperty("Attack Sound")
                    zPack.sCriticalSound   = AdItem_GetProperty("Critical Sound")
                DL_PopActiveObject()

                --If a damage-type override exists, use that animation instead.
                zPack.sAttackAnimation, zPack.sAttackSound, zPack.sCriticalSound = fnApplyWeaponTypeOverrides(gzAbiPack.iOriginatorID, zPack.sAttackAnimation, zPack.sAttackSound, zPack.sCriticalSound)

            end
        end
    DL_PopActiveObject()
    
end
