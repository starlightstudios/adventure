-- |[ ==================================== fnComputeShields ==================================== ]|
--Computes how much shield the ability applies, if any, to the target.
function gzaAbiHandlerFuncs.fnComputeShields()
    
    -- |[Application Check]|
    --Ability applies no shields, do nothing.
    if(gzAbiPack.zAbilityPack.iShieldsBase < 1 and gzAbiPack.zAbilityPack.fShieldsFactor <= 0.0) then return end
    
    -- |[Variables]|
    --Get power.
    AdvCombat_SetProperty("Push Entity By ID", gzAbiPack.iOriginatorID)
        local iPower = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_Attack)
    DL_PopActiveObject()
    
    -- |[Compute and Set]|
    --Compute.
    gzAbiPack.zAbilityPack.iShieldsFinal = gzAbiPack.zAbilityPack.iShieldsBase
    if(gzAbiPack.zAbilityPack.fShieldsFactor > 0.0) then
        gzAbiPack.zAbilityPack.iShieldsFinal = gzAbiPack.zAbilityPack.iShieldsFinal + (iPower * gzAbiPack.zAbilityPack.fShieldsFactor)
    end
    
    --Remove decimals.
    gzAbiPack.zAbilityPack.iShieldsFinal = math.floor(gzAbiPack.zAbilityPack.iShieldsFinal)
end