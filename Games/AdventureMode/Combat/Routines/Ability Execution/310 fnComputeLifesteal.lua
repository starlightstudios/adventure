-- |[ ================================== fnComputeLifesteal() ================================== ]|
--After damage is computed, computes the lifesteal value. Abilities can be flagged to do no lifesteal.
function gzaAbiHandlerFuncs.fnComputeLifesteal()
    
    -- |[Application Check]|
    --Ability does no damage, so it obviously doesn't steal life.
    if(gzAbiPack.zAbilityPack.bDoesNoDamage == true) then return end
    
    --Ability's lifesteal factor is zero. Do nothing.
    if(gzAbiPack.zAbilityPack.fLifestealFactor <= 0.0) then return end
    
    -- |[Set Lifesteal]|
    --The calculation is really simple.
    gzAbiPack.iLifestealVal = math.floor(giStandardDamage * gzAbiPack.zAbilityPack.fLifestealFactor)
    
end