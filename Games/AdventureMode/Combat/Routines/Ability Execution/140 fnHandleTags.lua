-- |[ ===================================== fnHandleTags() ===================================== ]|
--Changes flags based on tags affecting the target and originator.
function gzaAbiHandlerFuncs.fnHandleTags()
    
    -- |[ ========== Execution Tags ========== ]|
    --Setup.
    local zaExecTags = gzAbiPack.zAbilityPack.zaTags
    
    --These tags can adjust behaviors caused by other tags.
    local iNoCalledShot                 = fnGetTagCountInTable("Florentina No Called Shot", zaExecTags)
    local iIgnoreUserAttackCrits        = fnGetTagCountInTable("Ignore User Attack Crits", zaExecTags)
    local iIgnoreUserAttackCritsConsume = fnGetTagCountInTable("Ignore User Attack Crits Consume", zaExecTags)
    
    -- |[ ============= User Tags ============ ]|
    --Tags that apply to the user.
    AdvCombat_SetProperty("Push Entity By ID", gzAbiPack.iOriginatorID)
        
        -- |[Flank Tactics]|
        --If found, doubles attack power if the enemy has a Protection increasing effect present.
        local iUserFlankTacticsCount = AdvCombatEntity_GetProperty("Tag Count", "Flank Tactics Effect")
        
        -- |[User Attack Crits]|
        --Friendly effect on the user. Causes attacks to always hit/crit. Does not remove the holding effect.
        local iUserAttackCritsCount = AdvCombatEntity_GetProperty("Tag Count", "User Attack Crits")
        if(iUserAttackCritsCount > 0 and iIgnoreUserAttackCrits == 0) then
            gzAbiPack.bAlwaysHit = true
            gzAbiPack.bAlwaysCrit = true
        end
        
        -- |[User Attack Crits Consume]|
        --This tag is a friendly effect applied on the user. If present, the attack hits and crits, and the effect holding
        -- the tag is removed.
        local iUserAttackCritsConsumeCount = AdvCombatEntity_GetProperty("Tag Count", "User Attack Crits Consume")
        if(iUserAttackCritsConsumeCount > 0 and iIgnoreUserAttackCritsConsume == 0) then
            
            --Flags.
            gzAbiPack.bAlwaysHit = true
            gzAbiPack.bAlwaysCrit = true
            
            --Consume the first effect found with the tag ID.
            fnRemoveEffectWithTag(iUserAttackCritsConsumeCount, "User Attack Crits Consume")
        end
    DL_PopActiveObject()
    
    -- |[ =========== Target Tags ============ ]|
    --Tags that apply to the target.
    AdvCombat_SetProperty("Push Entity By ID", gzAbiPack.iTargetID)
    
        -- |[Flank Tactics]|
        --If the user count was over 0 for "Flank Tactics Effect", check if any effects increase protection.
        if(iUserFlankTacticsCount > 0) then
            
            --Check the stats array in the Permanent and Temporary effect set. If either is positive, this effect triggers.
            local iPermaBuff = AdvCombatEntity_GetProperty("Statistic", gciStatGroup_PermaEffect, gciStatIndex_Protection)
            local iTempBuff  = AdvCombatEntity_GetProperty("Statistic", gciStatGroup_TempEffect,  gciStatIndex_Protection)
            
            --Check:
            if(iPermaBuff > 0 or iTempBuff > 0) then
                table.insert(gzAbiPack.zaLocalEffectsFriendly, {"Double Power", 1})
            end
        end
    
        -- |[Called Shot]|
        --Florentina ability. Target is always-hit, always-crit. Effect that applied the tag will be removed.
        -- Abilities marked with "Florentina No Called Shot" ignore this.
        local iCalledShotCount = AdvCombatEntity_GetProperty("Tag Count", "Florentina Called Shot")
        if(iCalledShotCount > 0 and iNoCalledShot == 0) then
            
            --Set flags.
            gzAbiPack.bAlwaysHit = true
            gzAbiPack.bAlwaysCrit = true
            
            --Consume the first effect found with the tag ID.
            fnRemoveEffectWithTag(iCalledShotCount, "Florentina Called Shot")
        end
        
        -- |[Always Hit]|
        --Generic tag. When active, the effect expires but guarantees a hit.
        local iAlwaysHitCount = AdvCombatEntity_GetProperty("Tag Count", "Always Hit")
        if(iAlwaysHitCount > 0) then
            
            --Apply.
            gzAbiPack.bAlwaysHit = true
            
            --Consume the first effect found with the tag ID.
            fnRemoveEffectWithTag(iAlwaysHitConsumeCount, "Always Hit")
        end
        
        -- |[Always Hit, Consume]|
        --Generic tag. When active, the effect expires but guarantees a hit. Is consumed after applying.
        local iAlwaysHitConsumeCount = AdvCombatEntity_GetProperty("Tag Count", "Always Hit Consume")
        if(iAlwaysHitConsumeCount > 0) then
            
            --Apply.
            gzAbiPack.bAlwaysHit = true
            
            --Consume the first effect found with the tag ID.
            fnRemoveEffectWithTag(iAlwaysHitConsumeCount, "Always Hit Consume")
        end
        
    DL_PopActiveObject()
end
