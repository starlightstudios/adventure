-- |[ ================================== fnTargetInfoResolve() ================================= ]|
--Resolve and store information about the target in the global package.
function gzaAbiHandlerFuncs.fnTargetInfoResolve()
    
    -- |[Basic Info]|
    AdvCombat_SetProperty("Push Entity By ID", gzAbiPack.iTargetID)
        gzAbiPack.bIsNormalTarget    = AdvCombatEntity_GetProperty("Is Normal Target")
        gzAbiPack.bIsTargetStunnable = AdvCombatEntity_GetProperty("Is Stunnable")
        gzAbiPack.bIsTargetStunned   = AdvCombatEntity_GetProperty("Is Stunned")
    DL_PopActiveObject()
    
    -- |[Timer Offset]|
    gzAbiPack.iTimerOffset = giCombatTimerOffset
    giCombatTimerOffset = 0
    
    -- |[Not-Normal Target Properties]|
    --Not a normal target. This can happen if the target was hit by another event before we get to this execution
    -- section, in which case they might be time-stopped or KO'd or somesuch.
    if(gzAbiPack.bIsNormalTarget == false) then
        
        --Target can be a non-normal target.
        if(gzAbiPack.zAbilityPack.bTargetCanBeKOd == true) then
        
        --Checks failed, cannot execute.
        else
            return 0
        end
    end
    
    -- |[Tourist Mode Handling]|
    --Determine if Tourist Mode is active. Tourist mode makes all attacks hit, all effects apply,
    -- and damage is tripled.
    --Tourist mode does not apply to abilities used on allies or neutrals.
    gzAbiPack.bIsTouristMode     = AdvCombat_GetProperty("Is Tourist Mode")
    gzAbiPack.iPartyOfOriginator = AdvCombat_GetProperty("Party Of ID", gzAbiPack.iOriginatorID)
    gzAbiPack.iPartyOfTarget     = AdvCombat_GetProperty("Party Of ID", gzAbiPack.iTargetID)
    if(gzAbiPack.iPartyOfOriginator ~= gciACPartyGroup_Party or gzAbiPack.iPartyOfTarget ~= gciACPartyGroup_Enemy) then gzAbiPack.bIsTouristMode = false end
    
end
