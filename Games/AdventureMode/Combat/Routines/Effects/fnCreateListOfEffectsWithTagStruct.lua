-- |[ =========================== fnCreateListOfEffectsWithTagStruct =========================== ]|
--Given a tag structure, creates a list of all effects which have all of the tags inside each entry in the structure
-- that reference the given ID.
--The tag structure is as follows:
-- {{"Tag1", "Tag2", "Tag3"}, {"Tag4"}, {"Tag5, "Tag6"} ... }
--Any combination of tags is allowed, but to be valid the effect must contain at least one tag from every entry inside one cluster.
function fnCreateListOfEffectsWithTagStruct(piTargetID, pzaTagStruct)
    
    -- |[Argument Check]|
    if(piTargetID   == nil or piTargetID == 0) then return end
    if(pzaTagStruct == nil) then return end
    
    --Cast to integer.
    piTargetID = math.floor(piTargetID)
    
    -- |[Setup]|
    --Variables.
    local iaEffectIDList = {}
    
    --Run this routine to create an internal list of all effects referencing this ID.
    local iEffectsTotal = AdvCombat_SetProperty("Store Effects Referencing", piTargetID)
    
    -- |[Iterate]|
    --Check all relevant effects for the tag.
    for i = 0, iEffectsTotal-1, 1 do
        AdvCombat_SetProperty("Push Temp Effects", i)
        
            --Iterate across the tag structure.
            for p = 1, #pzaTagStruct, 1 do
        
                --Setup.
                local bHasAllEntries = true
                
                --For each tag:
                for o = 1, #pzaTagStruct[p], 1 do
                    local iCount = AdvCombatEffect_GetProperty("Get Tag Count", pzaTagStruct[p][o])
                    if(iCount < 1) then 
                        bHasAllEntries = false
                        break
                    end
                end
                
                --If this cluster has all of its entries represented, the effect is used.
                if(bHasAllEntries) then
                    table.insert(iaEffectIDList, RO_GetID())
                    break
                end
            end
            
        DL_PopActiveObject()
    end
    
    --Clean up.
    AdvCombat_SetProperty("Clear Temp Effects")
    
    -- |[Finish Up]|
    return iaEffectIDList
    
end