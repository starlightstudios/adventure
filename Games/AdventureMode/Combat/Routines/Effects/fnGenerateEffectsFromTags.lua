-- |[ =============================== fnGenerateEffectsFromTags ================================ ]|
--Part of the standard execution handler, scans the acting entity for tags that generate effects.
-- If any are found, runs the effect scripts to see if they spawn any effects on the target.
function fnGenerateEffectsFromTags(piOriginatorID, piTargetID, pzAbilityPack)
    
    -- |[ =============== Setup ============== ]|
    -- |[Argument Check]|
    if(piOriginatorID == nil) then return end
    if(piTargetID     == nil) then return end
    if(pzAbilityPack  == nil) then return end
    
    -- |[Get Party States]|
    local iPartyOfOriginator = AdvCombat_GetProperty("Party Of ID", piOriginatorID)
    local iPartyOfTarget     = AdvCombat_GetProperty("Party Of ID", iTargetUniqueID)
    
    -- |[ ========= Friendly Target ========== ]|
    if(iPartyOfOriginator == iPartyOfTarget) then
        return

    -- |[ ========== Hostile Target ========== ]|
    else
    
        -- |[Setup]|
        --Storage list.
        local zaEffectFireList = {}
    
        -- |[Resolve All Relevant Effects]|
        --Push acting entity.
        AdvCombat_SetProperty("Push Entity By ID", piOriginatorID)
        
            --Look for all effects that have the tag "Generate Effect" in one of several variants.
            local iTotalEffects = AdvCombatEntity_GetProperty("Effects With Tag", "Generate Effect Hostile")
        
            --Iterate across the effects.
            for i = 0, iTotalEffects-1, 1 do
                
                --Push the effect.
                local iEffectID = AdvCombatEntity_GetProperty("Effect ID With Tag", "Generate Effect Hostile", i)
                AdvCombat_SetProperty("Push Effect", iEffectID)
                
                    --Store the effect's information in an array.
                    local zEffectInfo = {}
                    zEffectInfo.iUniqueID = iEffectID
                    zEffectInfo.sScript = AdvCombatEffect_GetProperty("Script")
                    table.insert(zaEffectFireList, zEffectInfo)

                DL_PopActiveObject()
            end
        DL_PopActiveObject()
    
        -- |[Fire Generation Scripts]|
        --Iterate across the stored effects, firing their scripts to handle whether or not they should create an effect on the given target.
        for i = 1, #zaEffectFireList, 1 do
            AdvCombat_SetProperty("Push Effect", zaEffectFireList[i].iUniqueID)
                LM_ExecuteScript(zaEffectFireList[i].sScript, gciEffect_OnApply_Hostile)
            DL_PopActiveObject()
        end
        
        --Debug.
        --io.write(" Fired generation script. Got " .. #gzaTagEffectsHostile .. " total effects to check application.\n")
        
        -- |[Attempt Application]|
        --Create a list to store effects that actually applied.
        local zaFinalFireList = {}
        
        --Run the application sequence code to the effects in the global list, if any.
        for i = 1, #gzaTagEffectsHostile, 1 do
            local bThisEffectApplied = fnCheckEffectApplied(gzaTagEffectsHostile[i], piOriginatorID, piTargetID, false)
            if(bThisEffectApplied) then
                table.insert(zaFinalFireList, gzaTagEffectsHostile[i])
            end
        end
        
        --Once all effects are checked, the final fire list becomes the global list. This just saves a bit of memory.
        gzaTagEffectsHostile = zaFinalFireList
        
        --Debug.
        --io.write(" Total effects applied: " .. #gzaTagEffectsHostile .. "\n")
    end
    
end
