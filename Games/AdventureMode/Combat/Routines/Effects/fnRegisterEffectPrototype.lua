-- |[ ============================== fnRegisterEffectPrototype() =============================== ]|
--Registers an effect prototype to the global effect list. Will bark a warning if the effect name
-- is already in use.
function fnRegisterEffectPrototype(psUniqueName, piType, pzPrototype)
    EffectList:fnRegisterEffect(psUniqueName, piType, pzPrototype)
end
