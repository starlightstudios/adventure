-- |[ ==================================== fnPurgeJobEffects =================================== ]|
--When a character changes jobs in battle, any passives on their jobs bar also change. This code
-- orders those abilities on the jobs bar to purge their associated passive effects, if any.
--This function should be called during gciAbility_Execute in the ability's script.
function fnPurgeJobEffects()
    
    --Originator.
    AdvCombat_SetProperty("Push Event Originator")
    
        --Iterate across the class bar.
        for x = gciAbility_Class_X0, gciAbility_Class_X1, 1 do
            for y = gciAbility_Class_Y0, gciAbility_Class_Y1, 1 do
            
                --Get ability name.
                local sName = AdvCombatEntity_GetProperty("Ability In Slot", x, y)
                
                --Push the ability. Run the ability standard script for this ability with a special code to purge effects.
                AdvCombatEntity_SetProperty("Push Ability In Slot", x, gciAbilityGrid_Class_Y)
                    LM_ExecuteScript(gsStandardAbilityPath, gciAbility_SpecialClearEffects)
                DL_PopActiveObject()
            end
        end
    
    --Clean.
    DL_PopActiveObject()
    
end
