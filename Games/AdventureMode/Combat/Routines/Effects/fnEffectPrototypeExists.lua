-- |[ =============================== fnEffectPrototypeExists() ================================ ]|
--Returns true if the given prototype name is registered, false otherwise.
function fnEffectPrototypeExists(psUniqueName)
    return EffectList:fnEntryExists(psUniqueName)
    --[=[
    
    -- |[Argument Check]|
    if(psUniqueName == nil) then return false end
    
    -- |[Scan List]|
    for i = 1, #gzaEffectPrototypeList, 1 do
        if(gzaEffectPrototypeList[i].sPrototypeName == psUniqueName) then
            return true
        end
    end
    
    -- |[Not Found]|
    return false]=]
end