-- |[ ================================ fnLocateEffectPrototype() =============================== ]|
--Locates and returns the type, prototype structure, and index, with the given name, in that order. If 
-- no prototype is found, fails silently and returns nothing. It is the caller's job to report errors.
function fnLocateEffectPrototype(psUniqueName)
    local iType, zPrototype, iIndex = EffectList:fnLocateEntry(psUniqueName)
    return iType, zPrototype, iIndex
end