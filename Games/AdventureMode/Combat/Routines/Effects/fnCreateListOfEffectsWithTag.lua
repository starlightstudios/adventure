-- |[ ============================== fnCreateListOfEffectsWithTag ============================== ]|
--Given a tag, creates and returns a list of all effects referencing a given target ID. The list may be empty.
-- The effect list returned is a list of all IDs.
function fnCreateListOfEffectsWithTag(piTargetID, psTagToFind)
    
    -- |[Argument Check]|
    if(piTargetID  == nil or piTargetID == 0) then return end
    if(psTagToFind == nil) then return end
    
    -- |[Setup]|
    --Variables.
    local iaEffectIDList = {}
    
    --Run this routine to create an internal list of all effects referencing this ID.
    local iEffectsTotal = AdvCombat_SetProperty("Store Effects Referencing", piTargetID)
    
    -- |[Iterate]|
    --Check all relevant effects for the tag.
    for i = 0, iEffectsTotal-1, 1 do
        AdvCombat_SetProperty("Push Temp Effects", i)
        
            --Check if there is at least one tag found.
            local iCount = AdvCombatEffect_GetProperty("Get Tag Count", psTagToFind)
            
            --Add to the list.
            if(iCount > 0) then
                table.insert(iaEffectIDList, RO_GetID())
            end
            
        DL_PopActiveObject()
    end
    
    --Clean up.
    AdvCombat_SetProperty("Clear Temp Effects")
    
    -- |[Finish Up]|
    return iaEffectIDList
    
end