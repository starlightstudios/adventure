-- |[ ================================== fnCheckEffectApplied ================================== ]|
--Returns whether or not the given effect applies to the given target. Used by the standard execution
-- routine to see which effects stick and which fail.
function fnCheckEffectApplied(pzEffectPack, piOriginatorID, piTargetID, pbAlwaysApplies)
    
    -- |[Argument Check]|
    if(pzEffectPack    == nil) then return false end
    if(piOriginatorID  == nil) then return false end
    if(piTargetID      == nil) then return false end
    if(pbAlwaysApplies == nil) then return false end
    
    -- |[Setup]|
    --Resolve variables.
    local bIsTouristMode     = AdvCombat_GetProperty("Is Tourist Mode")
    local iPartyOfOriginator = AdvCombat_GetProperty("Party Of ID", piOriginatorID)
    
    -- |[Effect Level Bonus]|
    --Compute the bonus imparted to player-party-only effect application chance.
    local iEffectBonus = 0
    
    --If in the party, provide the bonus.
    if(iPartyOfOriginator == gciACPartyGroup_Party) then
        AdvCombat_SetProperty("Push Entity By ID", piOriginatorID)
            local iLevel = AdvCombatEntity_GetProperty("Level")
            iEffectBonus = math.floor(iLevel * gciEffectPowerPerLevel)
        DL_PopActiveObject()
    end
    
    -- |[Compute Tag Bonus]|
    --Bonus from tags
    AdvCombat_SetProperty("Push Entity By ID", piTargetID)
        
        --Collect tag bonuses for chance-to-apply.
        local fApplyBonus = fnComputeTagBonus(pzEffectPack.saApplyTagBonus, pzEffectPack.fApplyFactor)
        local fApplyMalus = fnComputeTagBonus(pzEffectPack.saApplyTagMalus, pzEffectPack.fApplyFactor)
        pzEffectPack.iTagApplyBonus = fApplyBonus - fApplyMalus
        
        --Collect tag bonuses for severity.
        local fSeverityBonus = fnComputeTagBonus(pzEffectPack.saSeverityTagBonus, pzEffectPack.fSeverityFactor)
        local fSeverityMalus = fnComputeTagBonus(pzEffectPack.saSeverityTagMalus, pzEffectPack.fSeverityFactor)
        pzEffectPack.fTagSeverityBonus = 1.0 + fSeverityBonus - fSeverityMalus
        
        --If flagged, also collect the item power bonus and add it.
        if(pzEffectPack.bBenefitsFromItemPower) then
            AdvCombat_SetProperty("Push Entity By ID", piOriginatorID)
                local iItemPowerBonus = AdvCombatEntity_GetProperty("Tag Count", "Item Power +")
                local iItemPowerMalus = AdvCombatEntity_GetProperty("Tag Count", "Item Power -")
                local fItemSeverityBonus = (0.01 * (iItemPowerBonus - iItemPowerMalus))
                pzEffectPack.fTagSeverityBonus = pzEffectPack.fTagSeverityBonus + fItemSeverityBonus
            DL_PopActiveObject()
        end
    
    DL_PopActiveObject()
    
    -- |[Final Tally]|
    --In Tourist Mode, or when flagged, the effect always applied.
    if(bIsTouristMode == true or pbAlwaysApplies == true) then
        return true
    end
        
    --Base
    local iEffectStr = pzEffectPack.iEffectStr
    if(gbAttackCrit) then iEffectStr = pzEffectPack.iEffectCritStr end
    
    --Check.
    return fnStandardEffectApply(piTargetID, iEffectStr + pzEffectPack.iTagApplyBonus, pzEffectPack.iEffectType, iEffectBonus)

end