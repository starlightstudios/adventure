-- |[ ============================= Global Prototype Effect Script ============================= ]|
--This script is used when a prefabricated effect exists on the global effect list.
--The effect's prototype name is stored in a global register in the C++ code. There is a global 
-- list of effect prototypes and each one has a unique string idenfitier. This list is searched 
-- whenever the effect runs and the resulting prototype is used to get constant data for the 
-- effect. This prototype is provided when creating an effect, otherwise it is stored inside 
-- the C++ data structure and passed when that structure calls an update.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Locate Prototype]|
--Retrieve the static prototype name.
local sPrototypeName = AdvCombatEffect_GetProperty("Static Prototype Name")
if(sPrototypeName == "Null") then
    io.write("Warning: Standard Effect Script was called but no static prototype name was set.\n")
    io.write(" ID of effect: " .. RO_GetID() .. "\n")
    io.write(" Calling switch type: " .. iSwitchType .. "\n")
    return
end

--Now that the prototype name is resolved, reset it to "Null". This prevents an erroneous effect
-- from reusing it later.
if(false) then
    io.write("Resolve prototype " .. sPrototypeName .. " for switch type " .. iSwitchType .. "\n")
end
AdvCombatEffect_SetProperty("Static Prototype Name", "Null")

--Scan the global list for the prototype.
local iEffectType, zEffectPrototype = fnLocateEffectPrototype(sPrototypeName)
if(iEffectType == nil) then
    
    --Check if the last five letters were ".Crit". This happens if a critical strike occurs. If the
    -- prototype is not found, remove the .Crit and try to find the base version of the effect.
    local sLastFive = string.sub(sPrototypeName, -5)
    if(string.len(sPrototypeName) > 5 and sLastFive == ".Crit") then
        sPrototypeName = string.sub(sPrototypeName, 1, -6)
        iEffectType, zEffectPrototype = fnLocateEffectPrototype(sPrototypeName)
    end
    
    --No base version, or this wasn't a crit.
    if(iEffectType == nil) then
        io.write("Warning: Standard Effect Script was called but effect prototype " .. sPrototypeName .. " was not found.\n")
        io.write(" ID of effect: " .. RO_GetID() .. "\n")
        io.write(" Calling switch type: " .. iSwitchType .. "\n")
        return
    end
end

-- |[Effect ID]|
--Resolve effect ID if this is a type that uses those.
local iEffectID = 0
if((iEffectType == gciEffect_Is_DoT or iEffectType == gciEffect_Is_HoT) and iArgumentsTotal >= 2) then
    iEffectID = LM_GetScriptArgument(1, "I")
end

--Set the gzaStatModStruct structure to reference the prototype.
gzaStatModStruct = zEffectPrototype

-- |[ ======================================== Creation ======================================== ]|
--Standard call when effect is initialized.
if(iSwitchType == gciEffect_Create) then
    
    -- |[Stat Mod]|
    if(iEffectType == gciEffect_Is_StatMod) then
        local fSeverity = 1.0
        if(iArgumentsTotal >= 2) then fSeverity = LM_GetScriptArgument(1, "N") end
        LM_ExecuteScript(gsStandardStatPath, gciEffect_Create, fSeverity)
    
    -- |[DoT]|
    elseif(iEffectType == gciEffect_Is_DoT) then
        gzRefEffect = zEffectPrototype
        LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType)
    
    -- |[HoT]|
    elseif(iEffectType == gciEffect_Is_HoT) then
        gzRefEffect = zEffectPrototype
        LM_ExecuteScript(gsStandardEffectHotPath, iSwitchType)
    end
    
    -- |[Upload Extra Data]|
    --When creating an effect via prototype, its unique name gets stored in the C++ code.
    AdvCombatEffect_SetProperty("Local Prototype Name", sPrototypeName)
    
    --Modify the call script to be this one.
    AdvCombatEffect_SetProperty("Script", LM_GetCallStack(0))
    
    -- |[Debug]|
    if(false) then
        io.write("Registered new effect of ID " .. RO_GetID() .. " prototype " .. sPrototypeName .. "\n")
    end
    
-- |[ ================================== Apply/Unapply Stats =================================== ]|
--Calls the apply/unapply stat routines.
elseif((iSwitchType == gciEffect_ApplyStats or iSwitchType == gciEffect_UnApplyStats) and iArgumentsTotal >= 2) then

    -- |[Stat Mod]|
    if(iEffectType == gciEffect_Is_StatMod) then
        local iTargetID = math.floor(tonumber(LM_GetScriptArgument(1, "N")))
        LM_ExecuteScript(gsStandardStatPath, iSwitchType, iTargetID)
    
    -- |[DoT]|
    elseif(iEffectType == gciEffect_Is_DoT) then
        gzRefEffect = zEffectPrototype
        LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType, iEffectID)
    
    -- |[HoT]|
    elseif(iEffectType == gciEffect_Is_HoT) then
        gzRefEffect = zEffectPrototype
        LM_ExecuteScript(gsStandardEffectHotPath, iSwitchType, iEffectID)
    end
    
    -- |[Debug]|
    --if(true) then
    --    io.write("Ran Apply/Unapply Stats call for " .. RO_GetID() .. " prototype .. " .. sPrototypeName .. "\n")
    --end

-- |[ ============================ Combat: Character Action Begins ============================= ]|
--Called when the character that possesses this Effect begins their turn.
elseif(iSwitchType == gciAbility_BeginAction) then

    -- |[Stat Mod]|
    if(iEffectType == gciEffect_Is_StatMod) then
        --Does nothing.
    
    -- |[DoT]|
    elseif(iEffectType == gciEffect_Is_DoT) then
        gzRefEffect = zEffectPrototype
        LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType, iEffectID)
    
    -- |[HoT]|
    elseif(iEffectType == gciEffect_Is_HoT) then
        gzRefEffect = zEffectPrototype
        LM_ExecuteScript(gsStandardEffectHotPath, iSwitchType, iEffectID)
    end
    
    -- |[Debug]|
    --if(true) then
    --    io.write("Ran Action Begins " .. RO_GetID() .. " prototype .. " .. sPrototypeName .. "\n")
    --end

-- |[ ================================== Character Action Ends ================================= ]|
--Called after a character action ends to decrement the turn counter.
elseif(iSwitchType == gciEffect_PostAction) then

    -- |[Stat Mod]|
    if(iEffectType == gciEffect_Is_StatMod) then
        LM_ExecuteScript(gsStandardStatPath, gciEffect_PostAction)
    
    -- |[DoT]|
    elseif(iEffectType == gciEffect_Is_DoT) then
        gzRefEffect = zEffectPrototype
        LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType, iEffectID)
    
    -- |[HoT]|
    elseif(iEffectType == gciEffect_Is_HoT) then
        gzRefEffect = zEffectPrototype
        LM_ExecuteScript(gsStandardEffectHotPath, iSwitchType, iEffectID)
    end
    
    -- |[Debug]|
    --if(true) then
    --    io.write("Ran Action Ends " .. RO_GetID() .. " prototype .. " .. sPrototypeName .. "\n")
    --end

-- |[ ================================ Compute Remaining Damage ================================ ]|
--Used by DoTs, this computes how much damage is remaining on the DoT and populates a global with it.
-- Requires the ID to be passed in alongside.
elseif(iSwitchType == gciEffect_DotComputeDamage) then

    -- |[Stat Mod]|
    if(iEffectType == gciEffect_Is_StatMod) then
        --Does nothing.
    
    -- |[DoT]|
    elseif(iEffectType == gciEffect_Is_DoT) then
        gzRefEffect = zEffectPrototype
        LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType, iEffectID)
    
    -- |[HoT]|
    elseif(iEffectType == gciEffect_Is_HoT) then
        --Does nothing.
    end
    
    -- |[Debug]|
    --if(true) then
    --    io.write("Ran Compute Remaining Damage " .. RO_GetID() .. " prototype .. " .. sPrototypeName .. "\n")
    --end
end