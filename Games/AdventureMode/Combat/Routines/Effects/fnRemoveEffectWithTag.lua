-- |[ ================================ fnRemoveEffectWithTag() ================================ ]|
--Given a tag, removes the zeroth effect on the active entity with that tag. Also removes the tags themselves
-- from the user if they were present on the base.
--This is used for fnHandleTags() during the ability execution code, to remove effects that are 
-- consumed on use.
function fnRemoveEffectWithTag(piTagCount, psTagName)
    
    -- |[Argument Check]|
    if(piTagCount == nil) then return end
    if(psTagName  == nil) then return end
    
    -- |[Removal]|
    --Remove from the entity, if applicable.
    AdvCombatEntity_SetProperty("Remove Tag", psTagName, piTagCount)
    
    --Remove the given effect, if one is found with the tag.
    local iZerothID = AdvCombatEntity_GetProperty("Effect ID With Tag", psTagName, 0)
    if(iZerothID ~= 0) then
        AdvCombat_SetProperty("Remove Effect", iZerothID)
    end
end
