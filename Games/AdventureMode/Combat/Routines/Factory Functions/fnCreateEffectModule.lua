-- |[ ================================= fnCreateEffectModule() ================================= ]|
--An effect module used for the Prediction Box. Note that this is not the same as the ability's execution
-- package, this is a module used for predictions only.
--An arbitrary number of modules may exist in a given prediction box.
function fnCreateEffectModule()
    
    -- |[Setup]|
    zModule = {}
    
    -- |[Resolved Variables]|
    --These will be populated by fnBuildPredictionBox() and should be ignored.
    zModule.iEffectApplyRate = 0
    zModule.iUserAttackPower = 0
    zModule.iDamageTotal = 0
    
    -- |[Chance-To-Apply]|
    zModule.bIsBuff = false                         --If true, doesn't compute chance-to-apply.
    zModule.iEffectType = gciDamageType_Slashing    --Resistance type to compute against
    zModule.iEffectStrength = 0                     --Higher effect strength gives an application bonus
    
    -- |[DoT]|
    zModule.bIsDamageOverTime = false               --If true, computes damage per turn
    zModule.iDotDuration = 3                        --How many turns the DoT runs for
    zModule.fDotAttackFactor = 1.0                  --Amount of the user's attack power to use for computing damage
    zModule.iDoTDamageType = gciDamageType_Slashing --Resistance type to compute damage against
    
    -- |[HoT]|
    zModule.bIsHealOverTime = false                 --If true, computes healing per turn
    zModule.fHotAttackFactor = 1.0                  --Amount of user's attack power to use for computing healing
    zModule.fHotFixedValue = 0.0                    --Always heals this amount
    zModule.fHotComputed = 0.0                      --Derived
    zModule.iHotDuration = 0                        --How many turns the HoT runs for
    
    -- |[Severity]|
    zModule.iSeverity = 0                           --Value of an effect if the effect scales with power. Used with the [SEV] tag.
    zModule.bBenefitsFromItemPower = false          --If true, the user's item power affects the iSeverity value.
    zModule.fItemPowerBonus = 1.00                  --Amount of user's item power to use.
    
    -- |[Display]|
    --Note: Results script counts images starting at 1. The 0th image is the [HEffect]/[FEffect] image.
    zModule.sResultScript = ""                      --Text when showing the results of an effect. Ex: "-[SEV] [IMG1] / 3[IMG2]"
    zModule.saResultImages = {}                     --Images for above. Ex: {"Root/Images/AdventureUI/StatisticIcons/Accuracy", "Root/Images/AdventureUI/DebuffIcons/Clock"}
    
    -- |[Tag Handling]|
    zModule.saApplyTagBonus = {}                    --Any tags in this array boost chance of effect to apply
    zModule.saApplyTagMalus = {}                    --Any tags in this array decrease chance of effect to apply
    zModule.fApplyFactor = 1                        --Each tag found adds this amount to the chance to apply, divided by 100.
    zModule.fApplyBonus = 0                         --Derived
    zModule.saSeverityTagBonus = {}                 --As above, but increase the effect severity.
    zModule.saSeverityTagMalus = {}                 --As above, but decrease the effect severity.
    zModule.fSeverityFactor = 0.01                  --As above, how much the severity increases per tag.
    zModule.fSeverityBonus = 1.00                   --Derived
    
    -- |[Finish]|
    return zModule
end