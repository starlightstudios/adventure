-- |[ ============================ fnCreatePredictionFromAbility() ============================= ]|
--Given an ability structure, takes information from elsewhere in the structure and builds prediction
-- information, then puts it in the structure. This effectively automates prediction setup.
--Note: To correctly build effect information, any effects caused by the ability need to be in
-- the global effect list.
--Returns the prediction pack created.
function fnCreatePredictionFromAbility(pzAbiStruct)
    
    -- |[Argument Check]|
    if(pzAbiStruct == nil) then return end
    
    -- |[Basic Setup]|
    --Create an empty package.
    local zPredictionPackage = fnCreatePredictionPack()
    
    --Fast-access pointer.
    local zExecPack = pzAbiStruct.zExecutionAbiPackage
    
    -- |[Hit Rate Module]|
    if(zExecPack.bAlwaysHits == false and zExecPack.bAlwaysCrits == false) then
        zPredictionPackage.bHasHitRateModule = true
        zPredictionPackage.zHitRateModule.iMissRate = zExecPack.iMissThreshold
    end
    
    -- |[Crit Rate Module]|
    --Ability must not have the no-crit case. Note than an ability that does 0 damage still has a crit package (for effects).
    if(zExecPack.bNeverCrits == false) then
        zPredictionPackage.bHasCritModule = true
        zPredictionPackage.zCritModule.iCritThreshold = zExecPack.iCritThreshold
        zPredictionPackage.zCritModule.fCritBonus     = zExecPack.fCriticalFactor
    end
    
    -- |[Damage Module]|
    if(zExecPack.bDoesNoDamage == false) then
        zPredictionPackage.bHasDamageModule = true
        zPredictionPackage.zDamageModule.iDamageType   = zExecPack.iDamageType
        zPredictionPackage.zDamageModule.fDamageFactor = zExecPack.fDamageFactor
    end
    
    -- |[Shield Module]|
    if(zExecPack.iShieldsBase > 0 or zExecPack.fShieldsFactor > 0.0) then
        zPredictionPackage.bHasShieldModule = true
        zPredictionPackage.zShieldModule.iShieldFixed  = zExecPack.iShieldsBase
        zPredictionPackage.zShieldModule.iShieldFactor = zExecPack.fShieldsFactor
    end
    
    -- |[Effect Packages]|
    --For each effect package on the ability, add lines.
    local iEffectModule = 0
    if(pzAbiStruct.zExecutionEffPackage ~= nil and pzAbiStruct.zExecutionEffPackage.sPrototypeName ~= nil) then
        iEffectModule = iEffectModule + 1
        zPredictionPackage.zaEffectModules[iEffectModule] = fnCreatePredictionEffectModuleFromPrototype(pzAbiStruct.zExecutionEffPackage.sPrototypeName)
    end
    
    -- |[Finish Up]|
    return zPredictionPackage
end
