-- |[ ====================== fnCreatePredictionEffectModuleFromPrototype() ======================= ]|
--Creates and returns an effect module based on the provided prototype. If the prototype is not found,
-- prints an error and returns nothing.
function fnCreatePredictionEffectModuleFromPrototype(psPrototypeName)
    
    -- |[Argument Check]|
    if(psPrototypeName == nil) then return end
    
    --Diagnostics.
    local bDiagnostics = Debug_GetFlag("Predictions: All") or Debug_GetFlag("Predictions: Effect Module")
    Debug_PushPrint(bDiagnostics, "fnCreatePredictionEffectModuleFromPrototype() - Beginning.\n")
    Debug_Print("Prototype: " .. psPrototypeName .. "\n")
    
    -- |[Locate Prototype]|
    --Make sure the prototype exists.
    local iType, zPrototype = fnLocateEffectPrototype(psPrototypeName)
    if(iType == nil) then
        io.write("fnCreatePredictionEffectModuleFromPrototype(): Error in script: " .. LM_GetCallStack(0) .. "\n")
        if(psPrototypeName == nil) then
            io.write(" Warning, prototype name was nil.\n")
        else
            io.write(" Warning, unable to locate effect prototype " .. psPrototypeName .. "\n")
        end
        Debug_PopPrint("fnCreatePredictionEffectModuleFromPrototype() - Finished, no prototype exists.\n")
        return
    end
    
    --Create the effect module.
    local zEffectModule = fnCreateEffectModule()
    
    -- |[Stat Mod]|
    --A buff or debuff effect.
    if(iType == gciEffect_Is_StatMod) then

        --Diagnostics.
        Debug_Print("Building a statmod prediction package.\n")

        --Buff: Always applies, so the type/strength are ignored.
        if(zPrototype.bIsBuff == true) then
            zEffectModule.bIsBuff = true
            Debug_Print(" Is a buff.\n")

        --Debuff: Has to handle chance-to-apply.
        else
            zEffectModule.bIsBuff            = false
            zEffectModule.iEffectStrength    = zPrototype.iEffectStr
            zEffectModule.iEffectType        = zPrototype.iEffectType
            zEffectModule.saApplyTagBonus    = zPrototype.saApplyTagBonus   
            zEffectModule.saApplyTagMalus    = zPrototype.saApplyTagMalus   
            zEffectModule.saSeverityTagBonus = zPrototype.saSeverityTagBonus
            zEffectModule.saSeverityTagMalus = zPrototype.saSeverityTagMalus
            Debug_Print(" Is a debuff.\n")
        end
        
        --Description section, built from the stat string.
        Debug_Print(" Running result script auto builder.\n")
        zEffectModule.sResultScript, zEffectModule.saResultImages = fnStatmodBuildResultScript(zPrototype)

        --Debug.
        if(bDiagnostics) then
            Debug_Print(" Result script is: " .. zEffectModule.sResultScript .. "\n")
            for i = 1, #zEffectModule.saResultImages, 1 do
                Debug_Print(" " .. zEffectModule.saResultImages[i] .. "\n")
            end
        end

    -- |[DoT]|
    --Damage-over-time.
    elseif(iType == gciEffect_Is_DoT) then
    
        --Diagnostics.
        Debug_Print("Building a DoT prediction package.\n")
        
        --Flags.
        zEffectModule.bIsDamageOverTime = true
        zEffectModule.iEffectType      = zPrototype.iDamageResistFlag - gciStatIndex_Resist_Start
        zEffectModule.iDotDuration     = zPrototype.iDuration
        zEffectModule.iDoTDamageType   = zPrototype.iDamageResistFlag - gciStatIndex_Resist_Start
        zEffectModule.iEffectStrength  = zPrototype.iEffectStr
        zEffectModule.fDotAttackFactor = zPrototype.faTurnFactors[1]
        zEffectModule.sResultScript    = "[DAM][IMG1] for " .. zEffectModule.iDotDuration .. "[IMG2]"
        zEffectModule.saResultImages   = {"Root/Images/AdventureUI/DamageTypeIcons/" .. zPrototype.sDamageTypeIcon, "Root/Images/AdventureUI/DebuffIcons/Clock"}

        --Results.
        Debug_Print("Dot effect type: " .. zEffectModule.iEffectType .. "\n")
        Debug_Print("Dot duration: " .. zEffectModule.iDotDuration .. "\n")
        Debug_Print("Dot damage type: " .. zEffectModule.iDoTDamageType .. "\n")
        Debug_Print("Dot strength: " .. zEffectModule.iEffectStrength .. "\n")

    -- |[HoT]|
    --Heal-over-time.
    elseif(iType == gciEffect_Is_HoT) then
    
        --Diagnostics.
        Debug_Print("Building a HoT prediction package.\n")
        
        --Flags.
        zEffectModule.bIsHealOverTime  = true
        zEffectModule.fHotAttackFactor = zPrototype.fHealFactor
        zEffectModule.fHotFixedValue   = zPrototype.fHealBase
        zEffectModule.iHotDuration     = zPrototype.iDuration
    
    -- |[Unhandled Type]|
    else
        io.write("fnCreatePredictionEffectModuleFromPrototype(): Warning, prototype found but effect code " .. iType .. " is unhandled.\n")
        Debug_PopPrint("fnCreatePredictionEffectModuleFromPrototype() - Finished with an unhandled type.\n")
        return
    end

    -- |[Finish Up]|
    --Diagnostics.
    Debug_PopPrint("fnCreatePredictionEffectModuleFromPrototype() - Finished normally.\n")
    
    --Pass back the effect module.
    return zEffectModule
end
