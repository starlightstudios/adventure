-- |[ =============================== Ability Package Structures =============================== ]|
--Lookup package used for fnConstructDefaultAbilityPackage(). Maps a human-readable name to a set of
-- effects and sounds. Should be run at program startup.
gzaAbiPacks = {}
local function fnAddPack(psLookup, psAttackAnim, psAttackSound, psCriticalSound)
    local i = #gzaAbiPacks + 1
    gzaAbiPacks[i] = {}
    gzaAbiPacks[i].sLookup = psLookup
    gzaAbiPacks[i].sAttackAnim = psAttackAnim
    gzaAbiPacks[i].sAttackSound = psAttackSound
    gzaAbiPacks[i].sCriticalSound = psCriticalSound
end

--Build list.
fnAddPack("Ankh",        "Ankh",        "Combat\\|Impact_Corrosive",  "Combat\\|Impact_Corrosive_Crit")
fnAddPack("ArrowM",      "ArrowM",      "Combat\\|Impact_Pierce",     "Combat\\|Impact_Pierce_Crit")
fnAddPack("ArrowS",      "ArrowS",      "Combat\\|Impact_Pierce",     "Combat\\|Impact_Pierce_Crit")
fnAddPack("Bleed",       "Bleed",       "Combat\\|Impact_Bleed",      "Combat\\|Impact_Bleed_Crit")
fnAddPack("Blind",       "Blind",       "Combat\\|Impact_Pierce",     "Combat\\|Impact_Pierce_Crit")
fnAddPack("Buff",        "Buff",        "Combat\\|Impact_Buff",       "Combat\\|Impact_Buff")
fnAddPack("Claw Slash",  "Claw Slash",  "Combat\\|Impact_Slash",      "Combat\\|Impact_Slash_Crit")
fnAddPack("Corrode",     "Corrode",     "Combat\\|Impact_Corrosive",  "Combat\\|Impact_Corrosive_Crit")
fnAddPack("Debuff",      "Debuff",      "Combat\\|Impact_Debuff",     "Combat\\|Impact_Debuff")
fnAddPack("Flamewall",   "Flamewall",   "Combat\\|Impact_Flames",     "Combat\\|Impact_Flames_Crit")
fnAddPack("Flames",      "Flames",      "Combat\\|Impact_Flames",     "Combat\\|Impact_Flames_Crit")
fnAddPack("Freeze",      "Freeze",      "Combat\\|Impact_Ice",        "Combat\\|Impact_Ice_Crit")
fnAddPack("Gun Shot",    "Gun Shot",    "Combat\\|Impact_Rifle",      "Combat\\|Impact_Rifle_Crit")
fnAddPack("Haste",       "Haste",       "Combat\\|Impact_Buff",       "Combat\\|Impact_Buff")
fnAddPack("Healing",     "Healing",     "Combat\\|Heal",              "Combat\\|Heal")
fnAddPack("Laser Shot",  "Laser Shot",  "Combat\\|Impact_Laser",      "Combat\\|Impact_Laser_Crit")
fnAddPack("Light",       "Light",       "Combat\\|Impact_Laser",      "Combat\\|Impact_Laser_Crit")
fnAddPack("Music",       "Music",       "Combat\\|Impact_Pierce",     "Combat\\|Impact_Pierce_Crit")
fnAddPack("Pierce",      "Pierce",      "Combat\\|Impact_Pierce",     "Combat\\|Impact_Pierce_Crit")
fnAddPack("PierceF",     "PierceF",     "Combat\\|Impact_Pierce",     "Combat\\|Impact_Pierce_Crit")
fnAddPack("Poison",      "Poison",      "Combat\\|Impact_Bleed",      "Combat\\|Impact_Bleed_Crit")
fnAddPack("Protect",     "Protect",     "Combat\\|Protect",           "Combat\\|Protect")
fnAddPack("Shadow A",    "Shadow A",    "Combat\\|Impact_Corrosive",  "Combat\\|Impact_Corrosive_Crit")
fnAddPack("Shadow B",    "Shadow B",    "Combat\\|Impact_Corrosive",  "Combat\\|Impact_Corrosive_Crit")
fnAddPack("Shadow C",    "Shadow C",    "Combat\\|Impact_Corrosive",  "Combat\\|Impact_Corrosive_Crit")
fnAddPack("Shock",       "Shock",       "Combat\\|Impact_Slash",      "Combat\\|Impact_Slash_Crit")
fnAddPack("Slash Lava",  "Slash Lava",  "Combat\\|Impact_Flames",     "Combat\\|Impact_Flames")
fnAddPack("Slow",        "Slow",        "Combat\\|Impact_Debuff",     "Combat\\|Impact_Debuff")
fnAddPack("Strike",      "Strike",      "Combat\\|Impact_Strike",     "Combat\\|Impact_Strike_Crit")
fnAddPack("Sword Slash", "Sword Slash", "Combat\\|Impact_CrossSlash", "Combat\\|Impact_CrossSlash_Crit")
fnAddPack("Terrify",     "Terrify",     "Combat\\|Impact_Slash",      "Combat\\|Impact_Slash_Crit")
fnAddPack("Whip",        "Whip",        "Combat\\|Impact_Slash",      "Combat\\|Impact_Slash_Crit")
fnAddPack("JobChange",   "Null",        "Null",                       "Null")