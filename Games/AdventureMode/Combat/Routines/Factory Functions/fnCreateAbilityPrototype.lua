-- |[ =============================== fnCreateAbilityPrototype() =============================== ]|
--Creates and returns an ability prototype. Serves as a listing for all the possible fields this type can have.
function fnCreateAbilityPrototype()
    return AbiPrototype:new()
    --[=[
    
    -- |[System]|
    --Create.
    local zAbiStruct = {}

    --Display/System Variables
    zAbiStruct.sJobName      = "ClassName"
    zAbiStruct.sSkillName    = "AbilityName"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. "Internal"
    zAbiStruct.sDisplayName  = "Display Name"
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Direct
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Here"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    --Special flag: If true, ability cannot be equipped.
    zAbiStruct.bCannotBeEquipped = false
    
    --Special flag: If not "Null", ability calls its handler script whenever it is unlocked.
    zAbiStruct.sUnlockScript = "Null"
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "Description"
    zAbiStruct.sDescription = ""
    zAbiStruct.saImages = {}
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Simplified Description"
    zAbiStruct.sSimpleDesc = ""
    zAbiStruct.saSimpleImages = {}

    -- |[Usability Variables]|
    --Requirements.
    zAbiStruct.bAlwaysAvailable     = false --Set to true to ignore all requirements.
    zAbiStruct.iRequiredMP          = 0     --MP required to use the ability.
    zAbiStruct.iRequiredCP          = 0     --CP required to use the ability.
    zAbiStruct.bRespectsCooldown    = true  --If this ability has a cooldown, decrements it.
    zAbiStruct.iCooldown            = 0     --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
    zAbiStruct.bIsFreeAction        = false --Flags as a free action.
    zAbiStruct.iRequiredFreeActions = 0     --Free actions set this as 1, everything else as 0.
    zAbiStruct.bRespectActionCap    = false --Characters with multiple free actions have a hard action cap. This flag checks that cap.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Enemies Single"

    --Effects
    zAbiStruct.iCPGain = 0
    
    --Tags. These are on the "ability" itself, when it is equipped. They can affect stats and properties for equipped skills.
    zAbiStruct.zaTags = {} --{{"TagA", iQuantity}, {"TagB", iQuantity}, etc}
    
    -- |[Prediction Package]|
    --See fnCreatePredictionPack() for a listing of variables.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    -- |[Execution Ability Package]|
    --See fnConstructDefaultAbilityPackage() for a listing of variables.
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Strike")
    
    -- |[Execution Effect Package]|
    --Optional. See fnConstructDefaultEffectPackage() for a listing of variables.
    zAbiStruct.zExecutionEffPackage = nil
    
    -- |[Execution Ally Effect Package]|
    --Optional. See fnConstructDefaultEffectPackage() for a listing of variables.
    zAbiStruct.zAllyEffectPackage = nil
    
    -- |[Execution Self-Effect Package]|
    --Optional. See fnConstructDefaultEffectPackage() for a listing of variables.
    zAbiStruct.zExecutionSelfPackage = nil
    
    -- |[Finish Up]|
    return zAbiStruct
    ]=]
end
