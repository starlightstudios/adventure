-- |[ ====================================== Miss Package ====================================== ]|
--Package that contains instructions when an attack misses.

-- |[Structure]|
function fnConstructDefaultMissPackage()
    zaPackage = {}
    zaPackage.iOriginatorID = 0
    zaPackage.sText = "Missed!"
    zaPackage.sSound = "Combat\\|AttackMiss"
    zaPackage.fnMissAnimate = fnDefaultMiss
    return zaPackage
end

-- |[Application]|
function fnDefaultMiss(piTargetID, piTimer, pzaAbilityPackage, pzaMissPackage)
    
    -- |[Ability Package]|
    --Run the ability package, but change the sound effect.
    if(pzaAbilityPackage.sAttackSound ~= "Null") then
        --AdvCombat_SetProperty("Register Application Pack", pzaAbilityPackage.iOriginatorID, piTargetID, piTimer, "Play Sound|" .. pzaAbilityPackage.sAttackSound)
    end
    
    --Play the normal attack animation at the same time.
    local iAnimTicks = 0
    if(pzaAbilityPackage.sAttackAnimation ~= nil and pzaAbilityPackage.sAttackAnimation ~= "Null") then
        AdvCombat_SetProperty("Register Application Pack", pzaAbilityPackage.iOriginatorID, piTargetID, piTimer, "Create Animation|" .. pzaAbilityPackage.sAttackAnimation .. "|AttackAnim0")
        iAnimTicks = fnGetAnimationTiming(pzaAbilityPackage.sAttackAnimation)
    end
    
    --Handle timer.
    piTimer = piTimer + (iAnimTicks / 2)
    
    --Display "Missed!" alongside.
    AdvCombat_SetProperty("Register Application Pack", pzaMissPackage.iOriginatorID, piTargetID, piTimer, "Text|" .. pzaMissPackage.sText)
    AdvCombat_SetProperty("Register Application Pack", pzaMissPackage.iOriginatorID, piTargetID, piTimer, "Play Sound|" .. pzaMissPackage.sSound)
    piTimer = piTimer + gciApplication_TextTicks
    return piTimer
end
