-- |[ =========================== fnConstructDefaultEffectPackage() ============================ ]|
-- |[Structure]|
--Used for an effect that can be applied with a strength.
function fnConstructDefaultEffectPackage()
    return EffectPack:new()
    --[=[
    
    -- |[Setup]|
    zPackage = {}
    
    -- |[System]|
    --System.
    zPackage.iOriginatorID = 0                                 --Internal, set by program during execution.
    
    --Flags.
    zPackage.bBenefitsFromItemPower = false                    --If true, the "Severity" flag will use item power tags.
    
    --Paths.
    zPackage.sEffectPath     = "Null"                          --Effect script to apply on hit.
    zPackage.sEffectCritPath = "Null"                          --Effect script to apply on crit.
    
    -- |[Application]|
    zPackage.iEffectStr     = 0                                --If the ability hits, application power
    zPackage.iEffectCritStr = 0                                --If the ability crits, application power
    zPackage.iEffectType    = gciDamageType_Slashing           --Resistance type checked against for application
    
    -- |[Tags]|
    zPackage.saApplyTagBonus = {}                              --Any tags in this list apply a bonus to application chance for each tag found.
    zPackage.saApplyTagMalus = {}                              --Any tags in this list apply a penalty to application chance for each tag found.
    zPackage.fApplyFactor = 1                                  --The amount of bonus or penalty to application chance for each tag found, as a percent.
    zPackage.iTagApplyBonus = 0                                --Internal
    zPackage.saSeverityTagBonus = {}                           --Any tags in this list apply a bonus to severity for each tag found.
    zPackage.saSeverityTagMalus = {}                           --Any tags in this list apply a penalty to severity for each tag found.
    zPackage.fSeverityFactor = 0.01                            --The amount of bonus or penalty to severity for each tag found.
    zPackage.fTagSeverityBonus = 0                             --Internal

    -- |[Animation]|
    --Successfully Applied Effect
    zPackage.sApplyText      = ""                              --Generated text that appears. Can be left empty.
    zPackage.sApplyTextCol   = "Color:White"                   --Pregenerated color. See AdvCombatApplications.cc for a list.
    zPackage.sApplyAnimation = "Null"                          --Animation if the effect applies. Can be "Null".
    zPackage.sApplySound     = "Null"                          --Sound effect if the effect applies. Can be "Null". Example: "Combat\\|Impact_Debuff", "Combat\\|Buff"
    zPackage.zaApplyAddAnims = {}                              --Additional animations if the effect applies. See below for samples.
    
    --Critical Applied Effect
    zPackage.sCritApplyText      = "NORMAL"                    --Generated text that appears on a critical application. Can be "NORMAL" which causes sApplyText to be used.
    zPackage.sCritApplyTextCol   = "NORMAL"                    --Pregenerated color. See AdvCombatApplications.cc for a list. Can be "NORMAL" which causes sApplyTextCol to be used.
    zPackage.sCritApplyAnimation = "NORMAL"                    --Animation if the effect applies. Can be "Null". Can also be "NORMAL" which causes the sApplyAnimation to be used.
    zPackage.sCritApplySound     = "NORMAL"                    --Sound effect if the effect applies. Can be "Null". Can also be "NORMAL" which causes the sApplySound to be used. 
    zPackage.zaCritApplyAddAnims = {"NORMAL"}                  --Additional animations if the effect crit-applies. See below for samples. If the zeroth entry is "NORMAL", uses the base list.
    
    --Resisted Effect
    zPackage.sResistText      = "Resisted"                     --Generated text if the effect fails to apply. Can be left empty.
    zPackage.sResistTextCol   = "Color:White"                  --See above.
    zPackage.sResistAnimation = "Null"                         --See above.
    zPackage.sResistSound     = "Combat\\|AttackMiss"          --See above.
    zPackage.zaResistAddAnims = {}                             --See above.
    
    -- |[Function Pointers]|
    --Functions called by this effect on application/resist/etc. You can override these to change the application behavior.
    -- The default versions are below in this file.
    zPackage.fnApplyEffect  = fnDefaultEffectAnimate
    zPackage.fnResistEffect = fnDefaultResistAnimate
    zPackage.fnHandler      = fnDefaultApplyOrResistSwitch
    
    -- |[Local Functions]|
    --Registers an animation to the animation list.
    function zPackage:fnRegisterAnim(psAnimName, piSlot)
        table.insert(self.zaApplyAddAnims, fnCreateAnimationPackageAuto(psAnimName, piSlot))
    end
    
    --Registers an animation to the critical animation list. As a convenience feature, clears the list if the
    -- zeroth element was "NORMAL".
    function zPackage:fnRegisterCritAnim(psAnimName, piSlot)
        if(self.zaCritApplyAddAnims[1] == "NORMAL") then
            self.zaCritApplyAddAnims = {}
        end
        
        table.insert(sekf.zaCritApplyAddAnims, fnCreateAnimationPackageAuto(psAnimName, piSlot))
    end
    
    -- |[Prototype Usage]|
    zPackage.sPrototypeName = nil
    ]=]
    
    -- |[Additional Animation Samples]|
    -- Sample additions to the animation list. Follows the same format as that used by ability packages.
    --[=[
    
    --Automated adder.
    zAbiStruct.zExecutionEffPackage:fnRegisterAnim("Buff Defense", 0)
    
    --Direct Addition.
    zAbiStruct.zExecutionEffPackage.zaApplyAddAnims = {}
    zAbiStruct.zExecutionEffPackage.zaApplyAddAnims[1] = {}
    zAbiStruct.zExecutionEffPackage.zaApplyAddAnims[1].iTimerOffset = 0
    zAbiStruct.zExecutionEffPackage.zaApplyAddAnims[1].sAnimName = "Accuracy"
    zAbiStruct.zExecutionEffPackage.zaApplyAddAnims[1].fXOffset = 0.0
    zAbiStruct.zExecutionEffPackage.zaApplyAddAnims[1].fYOffset = 0.0
    ]=]
    
    -- |[Finish Up]|
    --return zPackage
end