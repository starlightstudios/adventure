-- |[ ================================= fnCreatePredictionPack ================================= ]|
--Creates a package for the prediction box with default values and passes it back. The package is split into
-- a set of modules. If the module's associated variable is false, it does not add a line to the prediction
-- box and is ignored.
function fnCreatePredictionPack()
    
    -- |[Setup]|
    local zPackage = {}
    
    -- |[Hit-Rate Module]|
    zPackage.bHasHitRateModule = false
    zPackage.zHitRateModule = {}
    zPackage.zHitRateModule.iHitRate = 0
    zPackage.zHitRateModule.iAccuracyBonus = 0
    zPackage.zHitRateModule.iAccuracyMalus = 0
    zPackage.zHitRateModule.iMissRate = 0
    
    -- |[Damage Module]|
    zPackage.bHasDamageModule = false
    zPackage.zDamageModule = {}
    zPackage.zDamageModule.iDamageLo = 0
    zPackage.zDamageModule.iDamageHi = 0
    zPackage.zDamageModule.sDamageIco = {}
    zPackage.zDamageModule.bUseWeaponType = false
    zPackage.zDamageModule.iDamageType = gciDamageType_Slashing
    zPackage.zDamageModule.fDamageFactor = 1.00
    zPackage.zDamageModule.bBypassProtection = false
    zPackage.zDamageModule.iPenetration = 0
    zPackage.zDamageModule.iScatterRange = 15
    zPackage.zDamageModule.bBenefitsFromItemPower = false
    
    -- |[Stun Module]|
    zPackage.bHasStunModule = false
    zPackage.zStunModule = {}
    zPackage.zStunModule.iStunBase = 0
    
    -- |[Healing Module]|
    zPackage.bHasHealingModule = false
    zPackage.zHealingModule = {}
    zPackage.zHealingModule.iHealingFinal = 0 --Internal
    zPackage.zHealingModule.iHealingFixed = 0
    zPackage.zHealingModule.fHealingPercent = 0.0
    zPackage.zHealingModule.fHealingFactor = 0.0
    zPackage.zHealingModule.bHealingBenefitsFromItemPower = false
    
    -- |[Shield Module]|
    zPackage.bHasShieldModule = false
    zPackage.zShieldModule = {}
    zPackage.zShieldModule.iShieldFinal = 0 --Internal
    zPackage.zShieldModule.iShieldFixed = 0
    zPackage.zShieldModule.iShieldFactor = 0.0
    
    -- |[Self-Healing Module]|
    zPackage.bHasSelfHealingModule = false
    zPackage.zSelfHealingModule = {}
    zPackage.zSelfHealingModule.iHealingFinal   = 0   --Internal
    zPackage.zSelfHealingModule.iHealingFixed   = 0   --Constant healing
    zPackage.zSelfHealingModule.fHealingPercent = 0.0 --Percent of target's HP
    zPackage.zSelfHealingModule.fHealingFactor  = 0.0 --Percent of user's attack power
    
    -- |[MP Generation Module]|
    zPackage.bHasMPGenerationModule = false
    zPackage.zMPGenerationModule = {}
    zPackage.zMPGenerationModule.iMPGeneration = 0
    
    -- |[Lifesteal Module]|
    zPackage.bHasLifestealModule = false
    zPackage.zLifestealModule = {}
    zPackage.zLifestealModule.fStealPercent = 0.0 --Percentage of damage dealt returned to user
    zPackage.zLifestealModule.iLifestealLo = 0.0  --Internal
    zPackage.zLifestealModule.iLifestealHi = 0.0  --Internal
    
    -- |[Crit Module]|
    zPackage.bHasCritModule = false
    zPackage.zCritModule = {}
    zPackage.zCritModule.iCritRate = 0
    zPackage.zCritModule.iAccuracyBonus = 0
    zPackage.zCritModule.iAccuracyMalus = 0
    zPackage.zCritModule.iCritThreshold = 100
    zPackage.zCritModule.fCritBonus = 1.25
    zPackage.zCritModule.iCritStun = 25
    
    -- |[Chaser Modules]|
    zPackage.bHasChaserModules = false
    zPackage.zaChaserModules = {}
    zPackage.iChaserModulesTotal = 0
    zPackage.zaChaserModules[1] = {}
    zPackage.zaChaserModules[1].bConsumeEffect = true                   --If true, DoT expires when attack lands
    zPackage.zaChaserModules[1].sDoTTag = "Slashing DoT"                --What tag to look for on effects.
    zPackage.zaChaserModules[1].fDamageFactorChangePerDoT = 1.0         --Added to the damage module's factor for each effect present.
    zPackage.zaChaserModules[1].fDamageRemainingFactor = 1.0            --Percentage of remaining DoT damage to be added.
    zPackage.zaChaserModules[1].sConsumeString = "Consumes [IMG0] DoTs" --Description to appear IF and ONLY IF the chaser consumes DoTs
    zPackage.zaChaserModules[1].iConsumeImgCount = 1
    zPackage.zaChaserModules[1].saConsumeImgPath = {}
    zPackage.zaChaserModules[1].saConsumeImgPath[1] = "Root/Images/AdventureUI/DamageTypeIcons/Bleeding"
    
    -- |[Effects]|
    --Effect modules should be created by fnCreateEffectModule().
    zPackage.bTouristDoesntAffectEffects = false
    zPackage.zaEffectModules = {}
    
    -- |[Tags]|
    --Tags are of the format {{"TagA", iTagCount}, {"TagB", iTagCount}, etc}
    zPackage.zaTags = {}
    
    -- |[Additional Lines]|
    --Additional text lines appended to the prediction box. Format:
    --zPackage.zaAdditionalLines[1].sString   = "Plaintext [IMG0]"
    --zPackage.zaAdditionalLines[1].saSymbols = {"Root/Images/AdventureUI/DamageTypeIcons/Bleeding"}
    zPackage.zaAdditionalLines = {}
    
    -- |[Functions]|
    function zPackage:fnAddLine(psLine, psaSymbolTable)
        
        if(psLine == nil) then return end
        
        local zNewLine = {}
        zNewLine.sString   = psLine
        zNewLine.saSymbols = {}
        if(psaSymbolTable ~= nil) then
            zNewLine.saSymbols = psaSymbolTable
        end
        
        table.insert(self.zaAdditionalLines, zNewLine)
    end
    
    -- |[Finish]|
    return zPackage
end