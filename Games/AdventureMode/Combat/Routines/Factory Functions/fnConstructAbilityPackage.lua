-- |[ =========================== fnConstructDefaultAbilityPackage() =========================== ]|
--Constructs and returns an ability package. See 000 Structures.lua for the associated between psType
-- and the image/sounds of the ability.
function fnConstructDefaultAbilityPackage(psType)
    return AbiExecPack:new(psType)
    
    --[=[
    
    -- |[Setup]|
    zaPackage = {}
    
    -- |[System]|
    zaPackage.iOriginatorID = 0                             --Sets when execution to track who is using the ability. Changes if shared.
    
    -- |[Special]|
    zaPackage.bCauseBlackFlash = false                      --Causes a black flash. Used when non-player entities attack, or player entities counter-attack.
    zaPackage.sShowTitle = "Null"                           --Animates a title. Typically used by enemies to indicate they are using a special attack. "Null" shows nothing.
    zaPackage.bRunIfNotEquipped = false                     --Ability runs even if not on the equipped ability grid.
    
    -- |[Flags]|
    --Normal flags.
    zaPackage.bDoesNoDamage = false                         --Does not compute damage.
    zaPackage.bMandateDamage = false                        --Uses a script-resolved damage value and bypasses all resistence checks.
    zaPackage.bNeverGlances = false                         --Will hit or miss only, no glances.
    zaPackage.bDoesNoStun = false                           --Does not inflict stun, even if stun is set or on a crit.
    zaPackage.bDoesNotAnimate = false                       --Does not show combat animation when firing.
    zaPackage.bDoesNotAnimateDamage = false                 --Does not show damage numbers, glances, play SFX.
    zaPackage.bAlwaysHits = false                           --Always hits regardless of accuracy roll.
    zaPackage.bAlwaysCrits = false                          --Always crits regardless of accuracy roll.
    zaPackage.bNeverCrits = false                           --Does not crit regardless of accuracy roll.
    zaPackage.bEffectAlwaysApplies = false                  --Effect bypasses roll and always applies. Used for buffs.
    zaPackage.bTargetCanBeKOd = false                       --Allows target to be KOd, otherwise does nothing
    
    --Miss/Crit Threshold.
    zaPackage.iMissThreshold = 0                            --Accuracy roll failed to miss
    zaPackage.iCritThreshold = 100                          --Accuracy roll needed to crit
    
    -- |[Damage Flags]|
    --Damage, damage type, scatter range.
    zaPackage.bUseWeapon = false                            --If true, uses the attacker's weapon to select damage type.
    zaPackage.bDealsOneDamageMinimum = true                 --If true, and damage computes to 0, deals 1 damage.
    zaPackage.iDamageType = gciDamageType_Slashing          --Damage type.
    zaPackage.fDamageFactor = 1.00                          --Factor multiplied by power to deal damage.
    zaPackage.bBypassProtection = false                     --Set to true to ignore protection altogether.
    zaPackage.iPenetration = 0                              --Reduces protection by this value, cannot go below zero.
    zaPackage.fCriticalFactor = 1.25                        --Multiplies by damage factor on a crit.
    zaPackage.iCriticalStunDamage = 25                      --How much stun is inflicted on a crit.
    zaPackage.iScatterRange = 15                            --Percentage variance in both directions to final damage.
    zaPackage.bDamageBenefitsFromItemPower = false          --Adds item power tags to damage factor.
    
    --Only used if bMandateDamage is true
    zaPackage.iMandatedDamage = 0                           --Amount of damage to deal, if bMandateDamage is true. Can be zero!
    
    -- |[Healing Flags]|
    --Factor, percent, base.
    zaPackage.iHealingFinal   = 0                           --Internal
    zaPackage.iHealingBase    = 0                           --Fixed healing value
    zaPackage.fHealingFactor  = 0.0                         --Multiplied by attack power
    zaPackage.fHealingPercent = 0.0                         --Percent of max HP
    zaPackage.bHealingBenefitsFromItemPower = false         --Adds item power tags to healing factor.
    
    -- |[Self-Healing Flags]|
    --Heals the user, not the target. If target and user are the same, adds them.
    zaPackage.iSelfHealingFinal = 0                         --Internal
    zaPackage.iSelfHealingBase = 0                          --Fixed self-healing value
    zaPackage.fSelfHealingPercent = 0.0                     --Percent of user's max health
    zaPackage.fSelfHealingFactor = 0.0                      --Percent of user's attack power
    
    -- |[Shields Flags]|
    --By default, overwrites other shields. Note that shields are not always effects.
    zaPackage.iShieldsFinal  = 0                            --Internal
    zaPackage.iShieldsBase   = 0                            --Fixed shield value
    zaPackage.fShieldsFactor = 0.0                          --Multiplied by attack power
    
    -- |[MP Generation]|
    --Adds to user's MP pool.
    zaPackage.iMPGeneration = 0                             --MP added to user's pool when ability is used.
    
    --MP added to target's pool.
    zaPackage.iMPRestore = 0                                --MP added to target's pool when ability is used.
    
    -- |[Lifesteal]|
    --Adds to self-healing if both are present. Attack must deal damage.
    zaPackage.fLifestealFactor = 0.0                        --Percentage of damage dealt healed to caller.
    
    -- |[Stun]|
    --How much stun is inflicted.
    zaPackage.iBaseStunDamage = 0                           --Amount of stun damage inflicted on a hit/glance/crit.
    
    -- |[Job Change]|
    --String indicating job change on use.
    zaPackage.sChangeJobTo = "Null"                         --If not "Null", changes the user's job.
    
    -- |[Chaser Modules]|
    --Variable number of chaser modules that activate when certain DoT tags present.
    zaPackage.bHasChaserModules = false
    zaPackage.zaChaserModules = {}
    zaPackage.iChaserModulesTotal = 0
    zaPackage.zaChaserModules[1] = {}
    zaPackage.zaChaserModules[1].bConsumeEffect = true           --If true, DoT expires when attack lands
    zaPackage.zaChaserModules[1].sDoTTag = "Slashing DoT"        --What tag to look for on effects.
    zaPackage.zaChaserModules[1].fDamageFactorChangePerDoT = 1.0 --Added to the damage module's factor for each effect present.
    zaPackage.zaChaserModules[1].fDamageRemainingFactor = 1.0    --Percentage of remaining DoT damage to be added.
    
    -- |[Tags]|
    --Tag storage. Tags are formatted {{"TagA", iTagNumber}, {"TagB", iTagNumber}, etc}. These tags can be used to adjust execution behavior.
    zaPackage.zaTags = {}
    
    -- |[Summoning]|
    zaPackage.saSummonHandlerScripts = {}
    zaPackage.saSummonHandlerNames = {}
    
    -- |[Additional Paths]|
    zaPackage.saExecPaths = {}
    zaPackage.iaExecCodes = {}
    
    -- |[Execution Functions]|
    --These functions are defined below. Replace them with a new function to change ability behavior.
    zaPackage.fnAbilityAnimate   = fnDefaultAbilityAnimate
    zaPackage.fnDamageAnimate    = fnDefaultDamageAnimate
    zaPackage.fnInfluenceAnimate = fnDefaultInfluenceAnimate
    zaPackage.fnHealingAnimate   = fnDefaultHealingAnimate
    zaPackage.fnShieldsAnimate   = fnDefaultShieldsAnimate
    zaPackage.fnMPGainAnimate    = fnDefaultMPAnimate
    zaPackage.fnStunAnimate      = fnDefaultStunAnimate
    zaPackage.fnJobAnimate       = fnDefaultJobAnimate
    
    --Additional Animations. List may be nil by default.
    zaPackage.zaAdditionalAnimations = {}

    -- Sample
    --[=[
    
    --Function
    fnAddAnimationPackage(zAbiStruct.zExecutionAbiPackage, "Claw Slash", 0, 0.0, 0.0)
    
    --Adding even more using standardized offsets.
    fnAddAnimationPackageAuto(zAbiStruct.zExecutionAbiPackage, "Buff Defense", 1)
    fnAddAnimationPackageAuto(zAbiStruct.zExecutionAbiPackage, "Buff Resists", 2)
    fnAddAnimationPackageAuto(zAbiStruct.zExecutionAbiPackage, "Buff Power",   3)
    
    --Direct Addition
    zaPackage.zaAdditionalAnimations = {}
    zaPackage.zaAdditionalAnimations[1] = {}
    zaPackage.zaAdditionalAnimations[1].iTimerOffset = 0
    zaPackage.zaAdditionalAnimations[1].sAnimName = "Accuracy"
    zaPackage.zaAdditionalAnimations[1].fXOffset = 0.0
    zaPackage.zaAdditionalAnimations[1].fYOffset = 0.0
    ]=]
    --[=[
    
    -- |[Default Animations]|
    --If no matching type pack is found, the defaults are used.
    zaPackage.sAttackAnimation = gsDefaultAttackAnimation
    zaPackage.sAttackSound     = gsDefaultAttackSound
    zaPackage.sCriticalSound   = gsDefaultCriticalSound
    
    --Used if/when voice scripts are in play.
    zaPackage.sVoiceData = nil
    
    -- |[Weapon Type]|
    --Does not populate since the weapon's type will override this later.
    if(psType == "Weapon") then
        zaPackage.bUseWeapon = true
    
    -- |[Specified Type]|
    --Otherwise, iterate across the array of types.
    else
        for i = 1, #gzaAbiPacks, 1 do
            if(gzaAbiPacks[i].sLookup == psType) then
                zaPackage.sAttackAnimation = gzaAbiPacks[i].sAttackAnim
                zaPackage.sAttackSound     = gzaAbiPacks[i].sAttackSound
                zaPackage.sCriticalSound   = gzaAbiPacks[i].sCriticalSound
                break
            end
        end
    end
    
    -- |[Finish Up]|
    return zaPackage
    ]=]
end
--[=[
-- |[ =============================== fnCreateAnimationPackage() =============================== ]|
--Creates and returns an animation package.
function fnCreateAnimationPackage(psAnimationName, piTimerOffset, pfXOffset, pfYOffset)
    
    -- |[Argument Check]|
    if(psAnimationName == nil) then return end
    if(piTimerOffset   == nil) then return end
    if(pfXOffset       == nil) then pfXOffset = 0.0 end
    if(pfYOffset       == nil) then pfYOffset = 0.0 end
    
    -- |[Create]|
    local zAnimationStruct = {}
    zAnimationStruct.iTimerOffset = piTimerOffset
    zAnimationStruct.sAnimName    = psAnimationName
    zAnimationStruct.fXOffset     = pfXOffset
    zAnimationStruct.fYOffset     = pfYOffset
    
    -- |[Finish Up]|
    return zAnimationStruct
end

-- |[ ============================= fnCreateAnimationPackageAuto() ============================= ]|
--Overload of above, using the standardized gfaAnimOffsets array to specify the position and offsets. The zeroth index is zeroes for all fields.
function fnCreateAnimationPackageAuto(psAnimationName, piIndex)

    -- |[Argument Check]|
    if(psAnimationName == nil) then return end
    if(piIndex         == nil) then return end
    
    -- |[Zero Index]|
    --Use zero for all offsets.
    if(piIndex < 1) then
        return fnCreateAnimationPackage(psAnimationName, 0, 0.0, 0.0)
    end

    -- |[In Range]|
    --Index is within the range of presets.
    if(piIndex <= #gfaAnimOffsets) then
        return fnCreateAnimationPackage(psAnimationName, gfaAnimOffsets[piIndex].iTiming, gfaAnimOffsets[piIndex].fX, gfaAnimOffsets[piIndex].fY)
    end
    
    -- |[Out of Range]|
    --Returns all zeroes for safety.
    return fnCreateAnimationPackage(psAnimationName, 0, 0.0, 0.0)
end

-- |[ ================================= fnAddAnimationPackage() ================================ ]|
--Creates and adds an animation package to the provided ability package.
function fnAddAnimationPackage(pzAbiPackage, psAnimationName, piTimerOffset, pfXOffset, pfYOffset)

    -- |[Argument Check]|
    if(pzAbiPackage    == nil) then return end
    if(psAnimationName == nil) then return end
    if(piTimerOffset   == nil) then return end
    if(pfXOffset       == nil) then pfXOffset = 0.0 end
    if(pfYOffset       == nil) then pfYOffset = 0.0 end
    
    -- |[Create]|
    local zAnimationStruct = fnCreateAnimationPackage(psAnimationName, piTimerOffset, pfXOffset, pfYOffset)
    
    -- |[Finish Up]|
    table.insert(pzAbiPackage.zaAdditionalAnimations, zAnimationStruct)

end

-- |[ =============================== fnAddAnimationPackageAuto() ============================== ]|
--Overload of above, using the standardized gfaAnimOffsets array to specify the position and offsets. The zeroth index is zeroes for all fields.
function fnAddAnimationPackageAuto(pzAbiPackage, psAnimationName, piIndex)

    -- |[Argument Check]|
    if(pzAbiPackage    == nil) then return end
    if(psAnimationName == nil) then return end
    if(piIndex         == nil) then return end

    -- |[Zero Index]|
    --Use zero for all offsets.
    if(piIndex < 1) then
        fnAddAnimationPackage(pzAbiPackage, psAnimationName, 0, 0.0, 0.0)
        return
    end

    -- |[In Range]|
    --Index is within the range of presets.
    if(piIndex <= #gfaAnimOffsets) then
        fnAddAnimationPackage(pzAbiPackage, psAnimationName, gfaAnimOffsets[piIndex].iTiming, gfaAnimOffsets[piIndex].fX, gfaAnimOffsets[piIndex].fY)
        return
    end
    
    -- |[Error]|
    --Use zeroes.
    fnAddAnimationPackage(pzAbiPackage, psAnimationName, 0, 0.0, 0.0)
end

-- |[ ================================= Animation Application ================================== ]|
function fnDefaultAbilityAnimate(piTargetID, piTimer, pzaAbilityPackage, pbIsCritical)
    
    --Critical strikes change the sound effect.
    if(pbIsCritical == false) then
        if( pzaAbilityPackage.sAttackSound~= nil and  pzaAbilityPackage.sAttackSound ~= "Null") then
            AdvCombat_SetProperty("Register Application Pack", pzaAbilityPackage.iOriginatorID, piTargetID, piTimer, "Play Sound|" .. pzaAbilityPackage.sAttackSound)
        end
    else
        if(pzaAbilityPackage.sCriticalSound ~= nil and pzaAbilityPackage.sCriticalSound ~= "Null") then
            AdvCombat_SetProperty("Register Application Pack", pzaAbilityPackage.iOriginatorID, piTargetID, piTimer, "Play Sound|" .. pzaAbilityPackage.sCriticalSound)
        end
    end
    
    --Common.
    local iAnimTicks = 0
    if(pzaAbilityPackage.sAttackAnimation~= nil and pzaAbilityPackage.sAttackAnimation ~= "Null") then
        AdvCombat_SetProperty("Register Application Pack", pzaAbilityPackage.iOriginatorID, piTargetID, piTimer, "Create Animation|" .. pzaAbilityPackage.sAttackAnimation .. "|AttackAnim0")
        iAnimTicks = fnGetAnimationTiming(pzaAbilityPackage.sAttackAnimation)
    end
    
    --Handle timer.
    piTimer = piTimer + iAnimTicks
    return piTimer
end

-- |[ =================================== Damage Application =================================== ]|
function fnDefaultDamageAnimate(piOriginatorID, piTargetID, piTimer, iDamage, sOptionalDamageArgs)
    
    --sOptionalDamageArgs can legally be nil.
    local sDamageString = "Damage|" .. iDamage
    if(sOptionalDamageArgs ~= nil) then
        sDamageString = sDamageString .. "|" .. sOptionalDamageArgs
    end
    
    --If the damage was zero, show text instead:
    if(iDamage < 1) then
        AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, piTimer, "Text|0")
        
    --Deal damage.
    else
        AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, piTimer, sDamageString)
    end
    piTimer = piTimer + gciApplication_TextTicks
    
    return piTimer
end

-- |[ ================================= Influence Application ================================== ]|
function fnDefaultInfluenceAnimate(piOriginatorID, piTargetID, piTimer, piInfluence, sOptionalInfluenceArgs)
    
    --sOptionalDamageArgs can legally be nil.
    local sString = "Influence|" .. piInfluence
    if(sOptionalInfluenceArgs ~= nil) then
        sString = sDamageString .. "|" .. sOptionalInfluenceArgs
    end
    
    --If the influence was zero, do nothing:
    if(piInfluence < 1) then
        
    --Inflict.
    else
        AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, piTimer, sString)
    end
    piTimer = piTimer + gciApplication_TextTicks
    
    return piTimer
end

-- |[ ================================== Healing Application =================================== ]|
function fnDefaultHealingAnimate(piOriginatorID, piTargetID, piTimer, piHealing)
    
    --sOptionalDamageArgs can legally be nil.
    local sHealingString = "Healing|" .. piHealing
    
    --Apply.
    AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, piTimer, sHealingString)
    piTimer = piTimer + gciApplication_TextTicks
    
    return piTimer
end

-- |[ =================================== Shield Application =================================== ]|
function fnDefaultShieldsAnimate(piOriginatorID, piTargetID, piTimer, piShields)
    
    --sOptionalDamageArgs can legally be nil.
    local sShieldsString = "Shields|" .. piShields
    
    --Apply.
    AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, piTimer, sShieldsString)
    piTimer = piTimer + gciApplication_TextTicks

    return piTimer
end

-- |[ ===================================== MP Application ===================================== ]|
function fnDefaultMPAnimate(piOriginatorID, piTargetID, piTimer, piMPToGen)
    
    --sOptionalDamageArgs can legally be nil.
    local sMPGainString = "MPGain|" .. piMPToGen
    
    --Apply.
    AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, piTimer, sMPGainString)
    piTimer = piTimer + gciApplication_TextTicks
    
    return piTimer
end

-- |[ ==================================== Stun Application ==================================== ]|
--Note: The stun damage is not the amount received, it's the final value. Clamp it before you send it here.
-- Stun does not require time to animate.
--The actual application of stun takes no time, but the text does. If the start and end values are the same,
-- no animation will occur.
function fnDefaultStunAnimate(piOriginatorID, piTargetID, piTimer, piStunFinal)
    
    --Check the targets current stun. If it's the same as the destination stun, do nothing.
    AdvCombat_SetProperty("Push Entity By ID", piTargetID)
        local iStunCur = AdvCombatEntity_GetProperty("Stun")
    DL_PopActiveObject()
    if(iStunCur == piStunFinal) then return piTimer end
    
    --Otherwise, apply the stun.
    AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, piTimer, "Stun|" .. piStunFinal)
    AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, piTimer, "Text|Stun +" .. piStunFinal-iStunCur .. "|Color:Yellow")
    piTimer = piTimer + gciApplication_TextTicks
    return piTimer
end

-- |[ ================================= Job Change Application ================================= ]|
function fnDefaultJobAnimate(piOriginatorID, piTargetID, piTimer, sJobToChangeTo)
    
    --Flash to white.
    AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, piTimer, "FlashWhite")
    piTimer = piTimer + gciApplication_FlashWhiteTicks + gciApplication_FlashWhiteHold
    
    --Change jobs.
    AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, piTimer, "JobChange|" .. sJobToChangeTo)
    piTimer = piTimer + gciApplication_FlashWhiteHold
    
    --Undo the flash.
    AdvCombat_SetProperty("Register Application Pack", piOriginatorID, piTargetID, piTimer, "UnflashWhite")
    piTimer = piTimer + gciApplication_FlashWhiteTicks
    return piTimer
end
]=]
