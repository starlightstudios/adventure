-- |[ ==================================== Create Tag Struct =================================== ]|
--Creates a table which holds an integer for every type of tag in the game. Modify it and pass it
-- between functions.
function fnCreateTagStruct()
    local zaTagStruct = {}
    zaTagStruct.iBleedDamageTakenUpTags = 0
    zaTagStruct.iBleedDamageTakenDnTags = 0
    zaTagStruct.iBleedDamageDealtUpTags = 0
    zaTagStruct.iBleedDamageDealtDnTags = 0
    return zaTagStruct
end
