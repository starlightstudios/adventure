-- |[ ===================================== Build Functions ==================================== ]|
--Builds the functions found in this folder.
local sFunctionPath = gsRoot .. "Combat/Routines/"
local sFormulaPath  = gsRoot .. "Combat/Formulas/"

-- |[ ================================ Structures and Formulas ================================= ]|
-- |[Debug]|
if(gbAdventureBootDebug) then io.write("  Combat Formulas\n") end

-- |[Structures]|
LM_ExecuteScript(sFunctionPath .. "Factory Functions/000 Structures.lua")

-- |[Formulas]|
LM_ExecuteScript(sFormulaPath .. "fnCombatDamageFormula.lua")
LM_ExecuteScript(sFormulaPath .. "fnComputeCritRate.lua")
LM_ExecuteScript(sFormulaPath .. "fnComputeEffectApplyRate.lua")
LM_ExecuteScript(sFormulaPath .. "fnComputeHitRate.lua")
LM_ExecuteScript(sFormulaPath .. "fnStandardAttack.lua")
LM_ExecuteScript(sFormulaPath .. "fnStandardDamageType.lua")
LM_ExecuteScript(sFormulaPath .. "fnStandardEffectApply.lua")
LM_ExecuteScript(sFormulaPath .. "fnSumBaseStats.lua")
LM_ExecuteScript(sFormulaPath .. "fnStandardAccuracy.lua")
LM_ExecuteScript(sFormulaPath .. "fnComputeDamageRange.lua")

-- |[ =================================== Function Execution =================================== ]|
-- |[Debug]|
if(gbAdventureBootDebug) then io.write("  Combat Functions\n") end

-- |[Base Folder]|

-- |[Abilities]|
LM_ExecuteScript(sFunctionPath .. "Abilities/fnAddAbilityBuffPack.lua")
LM_ExecuteScript(sFunctionPath .. "Abilities/fnAddAbilityChaser.lua")
LM_ExecuteScript(sFunctionPath .. "Abilities/fnAddAbilityEffectPack.lua")
LM_ExecuteScript(sFunctionPath .. "Abilities/fnAddAbilitySummon.lua")
LM_ExecuteScript(sFunctionPath .. "Abilities/fnMakeAndRegisterBuffPrototype.lua")
LM_ExecuteScript(sFunctionPath .. "Abilities/fnMakeAndRegisterStatPrototype.lua")
LM_ExecuteScript(sFunctionPath .. "Abilities/fnMakeAndRegisterDoTPrototype.lua")
LM_ExecuteScript(sFunctionPath .. "Abilities/fnMakeAndRegisterPassivePrototype.lua")
LM_ExecuteScript(sFunctionPath .. "Abilities/fnSetAbilityExecEffectPrototype.lua")
LM_ExecuteScript(sFunctionPath .. "Abilities/fnSetAbilityExecSelfEffectPrototype.lua")
LM_ExecuteScript(sFunctionPath .. "Abilities/fnSetAbilityExecutionBuff.lua")
LM_ExecuteScript(sFunctionPath .. "Abilities/fnSetAbilityExecutionDamage.lua")
LM_ExecuteScript(sFunctionPath .. "Abilities/fnSetAbilityExecutionDebuff.lua")
LM_ExecuteScript(sFunctionPath .. "Abilities/fnSetAbilityExecutionEnemy.lua")
LM_ExecuteScript(sFunctionPath .. "Abilities/fnSetAbilityExecutionHealing.lua")
LM_ExecuteScript(sFunctionPath .. "Abilities/fnSetAbilityExecutionMPRestore.lua")
LM_ExecuteScript(sFunctionPath .. "Abilities/fnSetAbilityExecutionSelfHealing.lua")
LM_ExecuteScript(sFunctionPath .. "Abilities/fnSetAbilityExecutionShield.lua")
LM_ExecuteScript(sFunctionPath .. "Abilities/fnSetAbilitySystemProperties.lua")
LM_ExecuteScript(sFunctionPath .. "Abilities/fnSetAbilitySystemPropertiesEnemy.lua")
LM_ExecuteScript(sFunctionPath .. "Abilities/fnSetAbilityUsabilityProperties.lua")

-- |[AIs]|
LM_ExecuteScript(sFunctionPath .. "AI/fnApplyAIString.lua")
LM_ExecuteScript(sFunctionPath .. "AI/fnCheckAITags.lua")
LM_ExecuteScript(sFunctionPath .. "AI/fnPickAbilityByManualWeight.lua")
LM_ExecuteScript(sFunctionPath .. "AI/fnPickAbilityByRoll.lua")
LM_ExecuteScript(sFunctionPath .. "AI/fnPickTargetByThreat.lua")
LM_ExecuteScript(sFunctionPath .. "AI/fnPreferentialID.lua")
LM_ExecuteScript(sFunctionPath .. "AI/fnPreferentialTarget.lua")
LM_ExecuteScript(sFunctionPath .. "AI/fnPrimeAbilityByName.lua")
LM_ExecuteScript(sFunctionPath .. "AI/fnRandomTarget.lua")
LM_ExecuteScript(sFunctionPath .. "AI/fnStandardAISwitchHandler.lua")
LM_ExecuteScript(sFunctionPath .. "AI/fnStandardDecideActionSetup.lua")

-- |[Animations]|
LM_ExecuteScript(sFunctionPath .. "Animation/fnGetAnimationTiming.lua")

-- |[DoT]|
--Any functions that rely on tables built in this script are called here.
LM_ExecuteScript(sFunctionPath .. "DoT/fnCreateDoTEffectPrototype.lua")
LM_ExecuteScript(sFunctionPath .. "DoT/fnResolveDoTDamageForTurn.lua")
LM_ExecuteScript(sFunctionPath .. "DoT/fnResolveDoTFactorForTurn.lua")

-- |[Effects]|
LM_ExecuteScript(sFunctionPath .. "Effects/fnCheckEffectApplied.lua")
LM_ExecuteScript(sFunctionPath .. "Effects/fnCreateListOfEffectsWithTag.lua")
LM_ExecuteScript(sFunctionPath .. "Effects/fnCreateListOfEffectsWithTagStruct.lua")
LM_ExecuteScript(sFunctionPath .. "Effects/fnEffectPrototypeExists.lua")
LM_ExecuteScript(sFunctionPath .. "Effects/fnGenerateEffectsFromTags.lua")
LM_ExecuteScript(sFunctionPath .. "Effects/fnLocateEffectPrototype.lua")
LM_ExecuteScript(sFunctionPath .. "Effects/fnPurgeClassEffects.lua")
LM_ExecuteScript(sFunctionPath .. "Effects/fnRegisterEffectPrototype.lua")
LM_ExecuteScript(sFunctionPath .. "Effects/fnRemoveEffectWithTag.lua")

-- |[Factory Functions]|
LM_ExecuteScript(sFunctionPath .. "Factory Functions/fnConstructAbilityPackage.lua")
LM_ExecuteScript(sFunctionPath .. "Factory Functions/fnConstructEffectPackage.lua")
LM_ExecuteScript(sFunctionPath .. "Factory Functions/fnConstructMissPackage.lua")
LM_ExecuteScript(sFunctionPath .. "Factory Functions/fnCreateAbilityPrototype.lua")
LM_ExecuteScript(sFunctionPath .. "Factory Functions/fnCreateEffectModule.lua")
LM_ExecuteScript(sFunctionPath .. "Factory Functions/fnCreatePredictionEffectModuleFromPrototype.lua")
LM_ExecuteScript(sFunctionPath .. "Factory Functions/fnCreatePredictionFromAbility.lua")
LM_ExecuteScript(sFunctionPath .. "Factory Functions/fnCreatePredictionPack.lua")
LM_ExecuteScript(sFunctionPath .. "Factory Functions/fnCreateTagStruct.lua")

-- |[Party]|
LM_ExecuteScript(sFunctionPath .. "Party/fnApplyNegativeCharge.lua")
LM_ExecuteScript(sFunctionPath .. "Party/fnApplyPositiveCharge.lua")
LM_ExecuteScript(sFunctionPath .. "Party/fnStatRegime.lua")
LM_ExecuteScript(sFunctionPath .. "Party/fnPlaceJobCommonSkills.lua")

-- |[Prediction]|
LM_ExecuteScript(sFunctionPath .. "Predictions/fnBuildPredictionBox.lua")
LM_ExecuteScript(sFunctionPath .. "Predictions/fnGetApplicationDataFromEffectPackage.lua")

-- |[Statistics]|
LM_ExecuteScript(sFunctionPath .. "Statistics/fnApplyStatBonus.lua")
LM_ExecuteScript(sFunctionPath .. "Statistics/fnApplyStatBonusPct.lua")
LM_ExecuteScript(sFunctionPath .. "Statistics/fnComputeStatBonusPct.lua")
LM_ExecuteScript(sFunctionPath .. "Statistics/fnCreateCombatStatisticsPack.lua")
LM_ExecuteScript(sFunctionPath .. "Statistics/fnGetBaseStat.lua")
LM_ExecuteScript(sFunctionPath .. "Statistics/fnGetLastStatisticPack.lua")
LM_ExecuteScript(sFunctionPath .. "Statistics/fnGetLastStatisticsPackTurn.lua")
LM_ExecuteScript(sFunctionPath .. "Statistics/fnMarkdownToArrays.lua")

-- |[Statmod]|
LM_ExecuteScript(sFunctionPath .. "Statmod/fnCreateStatmodEffectPrototype.lua")
LM_ExecuteScript(sFunctionPath .. "Statmod/fnStatmodBuildInspectorStrings.lua")
LM_ExecuteScript(sFunctionPath .. "Statmod/fnStatmodBuildInspectorStringsInBattle.lua")
LM_ExecuteScript(sFunctionPath .. "Statmod/fnStatmodBuildResultScript.lua")
LM_ExecuteScript(sFunctionPath .. "Statmod/fnStatmodBuildShortText.lua")

-- |[Tags]|
LM_ExecuteScript(sFunctionPath .. "Tags/fnAppendTag.lua")
LM_ExecuteScript(sFunctionPath .. "Tags/fnGetTagCountInTable.lua")
LM_ExecuteScript(sFunctionPath .. "Tags/fnTagCalculator.lua")
LM_ExecuteScript(sFunctionPath .. "Tags/fnTargetHasAnyTags.lua")

-- |[Targets and Target Redirection]|
LM_ExecuteScript(sFunctionPath .. "Targeting/fnBuildIDListByParty.lua")
LM_ExecuteScript(sFunctionPath .. "Targeting/fnGetTargetRedirect.lua")
LM_ExecuteScript(sFunctionPath .. "Targeting/fnIsTargetCoveredBy.lua")
LM_ExecuteScript(sFunctionPath .. "Targeting/fnTrimTargetsWithoutTag.lua")
LM_ExecuteScript(sFunctionPath .. "Targeting/fnTrimTargetsWithTag.lua")

-- |[Utility]|
LM_ExecuteScript(sFunctionPath .. "Utility/fnApplyWeaponTypeOverrides.lua")
LM_ExecuteScript(sFunctionPath .. "Utility/fnBuildWeaponDamage.lua")
LM_ExecuteScript(sFunctionPath .. "Utility/fnCreateTableFromStatString.lua")
LM_ExecuteScript(sFunctionPath .. "Utility/fnDefaultEndOfApplications.lua")
LM_ExecuteScript(sFunctionPath .. "Utility/fnGetIconByDamageType.lua")
LM_ExecuteScript(sFunctionPath .. "Utility/fnGetStatImgPathFromMarkdown.lua")
LM_ExecuteScript(sFunctionPath .. "Utility/fnHandleCooldown.lua")
LM_ExecuteScript(sFunctionPath .. "Utility/fnHandleCP.lua")
LM_ExecuteScript(sFunctionPath .. "Utility/fnHandleMP.lua")
LM_ExecuteScript(sFunctionPath .. "Utility/fnIsEntityImmuneToDamageType.lua")
LM_ExecuteScript(sFunctionPath .. "Utility/fnMarkdownHandlerAbility.lua")
LM_ExecuteScript(sFunctionPath .. "Utility/fnMarkdownHandlerStats.lua")
LM_ExecuteScript(sFunctionPath .. "Utility/fnSetAbilityResponseFlags.lua")
LM_ExecuteScript(sFunctionPath .. "Utility/fnStandardBeginAction.lua") 
LM_ExecuteScript(sFunctionPath .. "Utility/fnStunHandler.lua")

-- |[ =================================== External Functions =================================== ]|
-- |[Debug]|
if(gbAdventureBootDebug) then io.write("  Enemy Functions\n") end

-- |[Enemy Functions]|
LM_ExecuteScript(gsRoot .. "Combat/Enemies/Common Enemy Functions.lua")
LM_ExecuteScript(gsRoot .. "Combat/Enemies/Enemy Creation.lua")

-- |[ =============================== Ability Execution Sequence =============================== ]|
-- |[Debug]|
if(gbAdventureBootDebug) then io.write("  Ability Execution Functions\n") end

-- |[Ability Execution Routines]|
local sAbiExecutionPath = sFunctionPath .. "Ability Execution/"
LM_ExecuteScript(sAbiExecutionPath .. "000 fnInitializeAbilityHandlerPack.lua")
LM_ExecuteScript(sAbiExecutionPath .. "001 fnStandardExecution.lua")
LM_ExecuteScript(sAbiExecutionPath .. "100 fnAbilityHandler.lua")
LM_ExecuteScript(sAbiExecutionPath .. "110 fnTargetRedirect.lua")
LM_ExecuteScript(sAbiExecutionPath .. "120 fnWeaponInfoResolve.lua")
LM_ExecuteScript(sAbiExecutionPath .. "130 fnTargetInfoResolve.lua")
LM_ExecuteScript(sAbiExecutionPath .. "140 fnHandleTags.lua")
LM_ExecuteScript(sAbiExecutionPath .. "150 fnHandleAccuracy.lua")
LM_ExecuteScript(sAbiExecutionPath .. "200 fnHandleMiss.lua")
LM_ExecuteScript(sAbiExecutionPath .. "300 fnComputeDamage.lua")
LM_ExecuteScript(sAbiExecutionPath .. "310 fnComputeLifesteal.lua")
LM_ExecuteScript(sAbiExecutionPath .. "320 fnComputeHealing.lua")
LM_ExecuteScript(sAbiExecutionPath .. "330 fnComputeShields.lua")
LM_ExecuteScript(sAbiExecutionPath .. "340 fnComputeStun.lua")
LM_ExecuteScript(sAbiExecutionPath .. "350 fnComputeEffects.lua")
LM_ExecuteScript(sAbiExecutionPath .. "400 fnApplicationSequence.lua")
LM_ExecuteScript(sAbiExecutionPath .. "500 fnRemoveExpiredEffectsFromTags.lua")
