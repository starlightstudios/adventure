-- |[ ================================== fnTargetHasAnyTags() ================================== ]|
--Given a list of tags and the ID of a target, checks if that target has any of the tags in the list
-- affecting it. If it does, then returns true. Returns false on error or if none of the tags found
-- are affecting the target.
function fnTargetHasAnyTags(psaTagListing, piTargetID)
    
    -- |[Argument Check]|
    if(psaTagListing == nil) then return false end
    if(piTargetID    == nil) then return false end
    
    -- |[Scan For Tags]|
    --Push entity.
    AdvCombat_SetProperty("Push Entity By ID", piTargetID)
    
        --Iterate across the tags.
        for i = 1, #psaTagListing, 1 do
            
            --Get the tag count. If it's over 0, then we can return true without scanning the rest of the list.
            local iTagCount = AdvCombatEntity_GetProperty("Tag Count", psaTagListing[i])
            if(iTagCount > 0) then
                DL_PopActiveObject()
                return true
            end
        end
    DL_PopActiveObject()
    
    -- |[No Tag Matches]|
    return false
end
