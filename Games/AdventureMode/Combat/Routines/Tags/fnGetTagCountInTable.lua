-- |[ ================================= fnGetTagCountInTable() ================================= ]|
--Given a structure of form {{"TagA", iCount}, {"TagB", iCount}, etc}, returns how many times the
-- given tag appears. Returns 0 if none found.
function fnGetTagCountInTable(psTag, pzaTagTable)
    
    -- |[Argument Check]|
    if(psTag       == nil) then return 0 end
    if(pzaTagTable == nil) then return 0 end
    
    -- |[Scan]|
    local iTagCount = 0
    for i = 1, #pzaTagTable, 1 do
        
        --Match:
        if(pzaTagTable[i][1] == psTag) then
            iTagCount = iTagCount + pzaTagTable[i][2]
        end
    end
    
    -- |[Finish Up]|
    return iTagCount
end