-- |[ ===================================== fnAppendTag() ====================================== ]|
--Given an array of tags of format {{"Tag A", iCount}, {"Tag B", iCount}}, appends the given tag
-- to the array with a count of 1 if it does not exist. If it does exist, the function will by
-- default do nothing, but if pbIncrementIfPresent is provided and true, it will add 1 to the
-- count for the matching tag.
function fnAppendTag(pzaTagArray, psTag, pbIncrementIfPresent)
    
    -- |[Argument Check]|
    if(pzaTagArray == nil) then return end
    if(psTag       == nil) then return end
    
    -- |[Scan]|
    --Check if we have the entry in the array.
    local bFoundTag = false
    for i = 1, #pzaTagArray, 1 do
        
        --Match:
        if(pzaTagArray[i][1] == psTag) then
            
            --If this flag is present, add 1 to the count.
            if(pbIncrementIfPresent == true) then
                pzaTagArray[i][2] = pzaTagArray[i][2] + 1
            end
            
            --In all cases, stop here.
            return
        end
    end

    -- |[Not Found, Append]|
    --If we got this far, add the missing tag to the listing.
    table.insert(pzaTagArray, {psTag, 1})
end
