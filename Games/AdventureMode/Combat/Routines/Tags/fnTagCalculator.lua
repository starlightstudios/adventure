-- |[ ===================================== Tag Calculator ===================================== ]|
--Functions for handling tag lists. Assumes the AdvCombatEntity being queried is the active object.
function fnComputeTagBonus(psaList, pfBonusPerElement)
    if(psaList           == nil) then return 0.0 end
    if(pfBonusPerElement == nil) then return 0.0 end
    
    --Setup.
    local fTotalBonus = 0.0
    local iListSize = #psaList
    
    --Iterate.
    for p = 1, iListSize, 1 do
        local sTagName = psaList[p]
        local iTagCount = AdvCombatEntity_GetProperty("Tag Count", sTagName)
        fTotalBonus = fTotalBonus + (pfBonusPerElement * iTagCount)
    end
    
    --Done.
    return fTotalBonus
end
