-- |[ ======================================== PierceF ========================================= ]|
--Pierce animation, but now it's on fire.
AdvCombat_SetProperty("Create Animation", "PierceF")

    AdvCombatAnimation_SetProperty("Offsets", -192, 0)
    AdvCombatAnimation_SetProperty("Ticks Per Frame", 1.0)
    AdvCombatAnimation_SetProperty("Allocate Frames", 20)
    for i = 0, 20-1, 1 do
		local sIndex = string.format("%02i", i)
        AdvCombatAnimation_SetProperty("Set Frame", i, "Root/Images/CombatAnimations/PierceF/" .. sIndex)
    end

DL_PopActiveObject()

--Add an entry to the timing list.
giAnimationTimingsTotal = giAnimationTimingsTotal + 1
gzaAnimationTimings[giAnimationTimingsTotal] = {}
gzaAnimationTimings[giAnimationTimingsTotal].sName = "PierceF"
gzaAnimationTimings[giAnimationTimingsTotal].iTicks = 20
