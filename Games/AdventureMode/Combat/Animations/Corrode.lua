-- |[ ========================================= Corrode ======================================== ]|
--Corrode animation, no special properties.
AdvCombat_SetProperty("Create Animation", "Corrode")

    AdvCombatAnimation_SetProperty("Offsets", -192, 0)
    AdvCombatAnimation_SetProperty("Ticks Per Frame", 1.0)
    AdvCombatAnimation_SetProperty("Allocate Frames", 41)
    for i = 0, 24-1, 1 do
		local sIndex = string.format("%02i", i)
        AdvCombatAnimation_SetProperty("Set Frame", i, "Root/Images/CombatAnimations/Corrode/" .. sIndex)
    end

DL_PopActiveObject()

--Add an entry to the timing list.
giAnimationTimingsTotal = giAnimationTimingsTotal + 1
gzaAnimationTimings[giAnimationTimingsTotal] = {}
gzaAnimationTimings[giAnimationTimingsTotal].sName = "Corrode"
gzaAnimationTimings[giAnimationTimingsTotal].iTicks = 41
