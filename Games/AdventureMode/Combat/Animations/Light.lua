-- |[ ========================================== Light ========================================= ]|
--Light animation, no special properties.
AdvCombat_SetProperty("Create Animation", "Light")

    AdvCombatAnimation_SetProperty("Offsets", -192, 0)
    AdvCombatAnimation_SetProperty("Ticks Per Frame", 1.0)
    AdvCombatAnimation_SetProperty("Allocate Frames", 30)
    for i = 0, 30-1, 1 do
		local sIndex = string.format("%02i", i)
        AdvCombatAnimation_SetProperty("Set Frame", i, "Root/Images/CombatAnimations/Light/" .. sIndex)
    end

DL_PopActiveObject()

--Add an entry to the timing list.
giAnimationTimingsTotal = giAnimationTimingsTotal + 1
gzaAnimationTimings[giAnimationTimingsTotal] = {}
gzaAnimationTimings[giAnimationTimingsTotal].sName = "Light"
gzaAnimationTimings[giAnimationTimingsTotal].iTicks = 30
