-- |[ ===================================== Arrow (Single) ===================================== ]|
--Arrow (Single) animation, no special properties.
local sAnimName = "ArrowS"
local sAnimPath = "ArrowS"
local iAnimFrames = 11
local fTPF = 1

--Create, set.
AdvCombat_SetProperty("Create Animation", sAnimName)
    AdvCombatAnimation_SetProperty("Offsets", -192, 0)
    AdvCombatAnimation_SetProperty("Ticks Per Frame", fTPF)
    AdvCombatAnimation_SetProperty("Allocate Frames", iAnimFrames)
    for i = 0, iAnimFrames-1, 1 do
		local sIndex = string.format("%02i", i)
        AdvCombatAnimation_SetProperty("Set Frame", i, "Root/Images/CombatAnimations/" .. sAnimPath .. "/" .. sIndex)
    end
DL_PopActiveObject()

--Add an entry to the timing list.
giAnimationTimingsTotal = giAnimationTimingsTotal + 1
gzaAnimationTimings[giAnimationTimingsTotal] = {}
gzaAnimationTimings[giAnimationTimingsTotal].sName = sAnimName
gzaAnimationTimings[giAnimationTimingsTotal].iTicks = iAnimFrames * fTPF
