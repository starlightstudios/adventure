-- |[ ========================================== Power ========================================= ]|
--Power Buff. Meant to be placed over a buff animation.
AdvCombat_SetProperty("Create Animation", "Buff Power")

    AdvCombatAnimation_SetProperty("Offsets", -192, 32)
    AdvCombatAnimation_SetProperty("Ticks Per Frame", 1.0)
    AdvCombatAnimation_SetProperty("Allocate Frames", 1)
    AdvCombatAnimation_SetProperty("Scroll Up Mode", 60)
    AdvCombatAnimation_SetProperty("Set Frame", 0, "Root/Images/CombatAnimations/BuffText/Power")

DL_PopActiveObject()

--Add an entry to the timing list.
giAnimationTimingsTotal = giAnimationTimingsTotal + 1
gzaAnimationTimings[giAnimationTimingsTotal] = {}
gzaAnimationTimings[giAnimationTimingsTotal].sName = "Buff Power"
gzaAnimationTimings[giAnimationTimingsTotal].iTicks = 60
