-- |[ ========================================= ShadowA ======================================== ]|
--ShadowA animation, no special properties.
AdvCombat_SetProperty("Create Animation", "Shadow A")

    AdvCombatAnimation_SetProperty("Offsets", -192, 0)
    AdvCombatAnimation_SetProperty("Ticks Per Frame", 1.0)
    AdvCombatAnimation_SetProperty("Allocate Frames", 40)
    for i = 0, 40-1, 1 do
		local sIndex = string.format("%02i", i)
        AdvCombatAnimation_SetProperty("Set Frame", i, "Root/Images/CombatAnimations/ShadowA/" .. sIndex)
    end

DL_PopActiveObject()

--Add an entry to the timing list.
giAnimationTimingsTotal = giAnimationTimingsTotal + 1
gzaAnimationTimings[giAnimationTimingsTotal] = {}
gzaAnimationTimings[giAnimationTimingsTotal].sName = "Shadow A"
gzaAnimationTimings[giAnimationTimingsTotal].iTicks = 40
