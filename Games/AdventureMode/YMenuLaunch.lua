-- |[ ====================================== Menu Launcher ===================================== ]|
--Script called when this game is selected from the main menu.
local sGameName = "Adventure Mode"

--Make sure the entry exists.
local zGameEntry = SysPaths:fnGetGameEntry(sGameName)
if(zGameEntry == nil) then
	io.write("Unable to launch game " .. sGameName.. ", no game entry was found.\n")
	return
end

--Switch to loading mode.
MapM_PushMenuStackHead()
	FlexMenu_ActivateLoading()
DL_PopActiveObject()