-- |[ ======================================== Game Info ======================================= ]|
--This file is executed at program startup to specify that this is a game the engine can load.
local sBasePath       = fnResolvePath()
local sGameName       = "Adventure Mode"
local sGamePath       = "Root/Paths/System/Startup/sAdventurePath" --Must exist in the C++ listing.
local sButtonText     = "Play Runes of Pandemonium"
local iPriority       = gciAdventure_Set_Start + 0
local sLauncherScript = "YMenuLaunch.lua" --File in the same folder as this file that will launch the program.
local iNegativeLen    = -12 --Indicates length of launcher name. ZLaunch.lua is -12.

--Local variables.
local zLaunchPath = sBasePath .. "ZLaunch.lua"

-- |[Compatibility Check]|
if(LM_IsGameRegistered("Pandemonium") == false) then
    if(SysPaths.bShowFailedRegistrations) then
        io.write("Not registering Pandemonium. Engine is incompatible.\n")
    end
    return
end

-- |[Registration]|
--Add the game entry. Does nothing if it already exists.
SysPaths:fnAddGameEntry(sGameName, sGamePath, sButtonText, iPriority, sLauncherScript, iNegativeLen)

--Add search path.
SysPaths:fnAddSearchPathToEntry(sGameName, zLaunchPath)
