-- |[ =============================== Miso General: Improvements =============================== ]|
--Miso activates vendor mode, with a special flag to instead activate unlocking mode.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Dialogue]|
--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
    --Dialogue.
    fnCutscene([[ Append("Miso:[E|Neutral] Here are the things I can make, if you get me the materials.") ]])
    fnCutsceneBlocker()

    --Run the shop.
    local sBasePath = fnResolvePath()
    local sString = "AM_SetShopProperty(\"Show\", \"Miso\", \"" .. sBasePath .. "Unlocking.lua\", \"Null\")"
    fnCutscene(sString)
    fnCutsceneBlocker()
    
end
