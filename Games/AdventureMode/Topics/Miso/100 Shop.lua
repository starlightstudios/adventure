-- |[ =================================== Miso General: Shop =================================== ]|
--Miso activates vendor mode.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Dialogue]|
--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
    --Dialogue.
    fnCutscene([[ Append("Miso:[E|Neutral] Here's what I have available.[P] All proceeds go to the temple, of course!") ]])
    fnCutsceneBlocker()

    --Run the shop.
    local sBasePath = fnResolvePath()
    local sString = "AM_SetShopProperty(\"Show\", \"Miso\", \"" .. sBasePath .. "Shopping.lua\", \"Null\")"
    fnCutscene(sString)
    fnCutsceneBlocker()
    
end
