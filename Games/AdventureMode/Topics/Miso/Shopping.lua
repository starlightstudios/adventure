-- |[ ===================================== Miso Shop Setup ==================================== ]|
--Miso's inventory. Carries items from Northwoods, Tier 1.

-- |[ =================================== Item Stock Listing =================================== ]|
-- |[Common Items]|
--Adamantite. Always sells powder.
AM_SetShopProperty("Add Item", "Adamantite Powder", gciNoPriceOverride, gciNoQuantityLimit)

--A few other basic items.
AM_SetShopProperty("Add Item", "Healing Tincture",            gciNoPriceOverride, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Controversial Sugar Cookies", gciNoPriceOverride, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Infused Rye Bread",           gciNoPriceOverride, gciNoQuantityLimit)

-- |[Weapons]|
AM_SetShopProperty("Add Item", "Musket Rounds",          gciNoPriceOverride, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Enchanted Cartridges",   gciNoPriceOverride, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Argentum Straightsword", gciNoPriceOverride, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Thick Collar",           gciNoPriceOverride, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Bruiser Gauntlets",      gciNoPriceOverride, gciNoQuantityLimit)

-- |[Armors]|
AM_SetShopProperty("Add Item", "Warrior's Pelt",         gciNoPriceOverride, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Infused Miko Robes",     gciNoPriceOverride, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Ancient Cuirass",        gciNoPriceOverride, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Leather Greatcoat",      gciNoPriceOverride, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Lined Miko Robes",       gciNoPriceOverride, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Light Leather Vest",     gciNoPriceOverride, gciNoQuantityLimit)

-- |[Accessories]|
AM_SetShopProperty("Add Item", "Ammo Bag",       gciNoPriceOverride, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Silk Pouch",     gciNoPriceOverride, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Glove Liners",   gciNoPriceOverride, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Wooden Greaves", gciNoPriceOverride, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Rush Bracers",   gciNoPriceOverride, gciNoQuantityLimit)

-- |[Combat Items]|
AM_SetShopProperty("Add Item", "Hearty Soup",     gciNoPriceOverride, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Brutal Sandwich", gciNoPriceOverride, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Lamp Oil",        gciNoPriceOverride, gciNoQuantityLimit)

