-- |[ =================================== Miso Unlock Setup ==================================== ]|
--Unlocking of items interface.
local sBasePath = fnResolvePath()

-- |[Unlock Mode]|
AM_SetShopProperty("Set Unlock Mode")

-- |[ ===================================== Shorthand Names ==================================== ]|
local saShorthandTable = {}

-- |[Northwoods]|
table.insert(saShorthandTable, {"Gossamer",           "Goss"})
table.insert(saShorthandTable, {"Hardened Bark",      "HBark"})
table.insert(saShorthandTable, {"Slippery Boar Meat", "BoarMt"})
table.insert(saShorthandTable, {"Mighty Venison",     "Vnsn"})
table.insert(saShorthandTable, {"Bird Meat",          "BirdMt"})

-- |[Westwoods]|
table.insert(saShorthandTable, {"Unmelting Snow",     "UnmltS"})
table.insert(saShorthandTable, {"Crystallized Acid",  "CryAcd"})
table.insert(saShorthandTable, {"Buneye Fluff",       "BunFluf"})
table.insert(saShorthandTable, {"Griffon Egg",        "GrifEgg"})
table.insert(saShorthandTable, {"Bonesaw Hog Meat",   "BoneHog"})
table.insert(saShorthandTable, {"Caustic Vension",    "CausVen"})
    
-- |[ ==================================== List of Materials =================================== ]|
--Run across the list of materials and upload them. This is used to show the total crafting materials
-- in the inventory, even if none of the unlockables use a particular item.
for i = 1, #saShorthandTable, 1 do
    AM_SetShopProperty("Add Unlock Material", saShorthandTable[i][1], saShorthandTable[i][2])
end

-- |[ ======================================== Functions ======================================= ]|
--List.
local zaUnlockableList = {}

--Given a name, variable, and set of requirements, places them on the table for later addition.
local function fnAddUnlockable(psName, pzaRequirementsTable)
    
    -- |[Argument Check]|
    if(psName               == nil) then return end
    if(pzaRequirementsTable == nil) then return end
    
    -- |[Creation]|
    local zEntry = {}
    zEntry.sName               = psName
    zEntry.zaRequirementsTable = pzaRequirementsTable
    table.insert(zaUnlockableList, zEntry)
end

-- |[ ================================== List of Unlockables =================================== ]|
--Constants
local sMisoPath = "Root/Variables/Chapter2/Miso/"

-- |[ ================ Tier 1 ================ ]|
-- |[Weapons]|
fnAddUnlockable("Enchanted Cartridges +",   { {"Enchanted Cartridges",   1}, {"Gossamer",      2}, {"Slippery Boar Meat", 1}, {"Slippery Boar Meat", 1} })
fnAddUnlockable("Argentum Straightsword +", { {"Argentum Straightsword", 1}, {"Hardened Bark", 2}, {"Mighty Venison",     1} })
fnAddUnlockable("Thick Collar +",           { {"Thick Collar",           1}, {"Gossamer",      2}, {"Bonesaw Hog Meat",   1} })
fnAddUnlockable("Bruiser Gauntlets +",      { {"Bruiser Gauntlets",      1}, {"Gossamer",      2}, {"Bird Meat",          1} })

-- |[Armors]|
fnAddUnlockable("Warrior's Pelt +",     { {"Warrior's Pelt",     1}, {"Hardened Bark", 1} })
fnAddUnlockable("Infused Miko Robes +", { {"Infused Miko Robes", 1}, {"Gossamer",      1} })
fnAddUnlockable("Ancient Cuirass +",    { {"Ancient Cuirass",    1}, {"Hardened Bark", 1} })
    
-- |[ =============================== Apply List of Unlockables ================================ ]|
--Run across the unlockables list and unlock everything.
for i = 1, #zaUnlockableList, 1 do
    
    -- |[Basics]|
    AM_SetShopProperty("Add Item", zaUnlockableList[i].sName, 0, 0)
    AM_SetShopProperty("Set Unlock Execute Script", zaUnlockableList[i].sName, sBasePath .. "Unlock Reply.lua")
    
    -- |[Requirement Setting]|
    --Fast-access pointer.
    local zaReqs = zaUnlockableList[i].zaRequirementsTable
    
    --Run across the table.
    for p = 1, #zaReqs, 1 do
        
        --Fast-access pointers.
        local sReqName     = zaReqs[p][1]
        local iReqQuantity = zaReqs[p][2]
        
        --In case the shorthand is not found, it defaults to the requirement name.
        local sShorthandName = sReqName
        
        --Check the lookup table for the shorthand name.
        for o = 1, #saShorthandTable, 1 do
            if(sReqName == saShorthandTable[o][1]) then
                sShorthandName = saShorthandTable[o][2]
                break
            end
        end
        
        --Add it to the list.
        AM_SetShopProperty("Add Unlock Requirement", zaUnlockableList[i].sName, sReqName, sShorthandName, iReqQuantity)
    
    end
end
