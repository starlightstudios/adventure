-- |[ ===================================== Build Listing ====================================== ]|
--Builds a list of topics this NPC will respond to, and at what levels.
local sNPCName = "Miso"
gsActiveNPCName = "Miso"

--              ||||| Unlocked At Start |||  Filename      |||  Display Name (Optional)
fnConstructTopicStd(              true, "Explanation",   "Explanation")
fnConstructTopicStd(              true, "Improvements",  "Improvements")
fnConstructTopicStd(              true, "Shop",          "Shop")
