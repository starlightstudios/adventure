-- |[ ================================ Miso General: Explanation =============================== ]|
--Miso explains how to unlock items.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Miso")

-- |[Dialogue]|
--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
    --Dialogue.
    fnCutscene([[ Append("Sanya:[E|Neutral] Miso, can you give me a once-over on unlocking items?[B][C]") ]])
    fnCutscene([[ Append("Miso:[E|Neutral] Certainly![P] In fact, it's very simple.[B][C]") ]])
    fnCutscene([[ Append("Miso:[E|Neutral] The glacier has many unique creatures and items.[P] I can use a bit of magic, alchemy, and good old know-how to improve those items.[B][C]") ]])
    fnCutscene([[ Append("Miso:[E|Neutral] Creatures that you hunt will provide special materials.[P] Items you can buy can be improved with those materials.[P] Items bought in a region tend to need the materials from that region.[B][C]") ]])
    fnCutscene([[ Append("Miso:[E|Neutral] Ask me about 'Improvements' to see the improvement, or just do some shopping.[P] I have some odds and ends, but you'll find better things at shops in Poterup or elsewhere.[B][C]") ]])
    
end
