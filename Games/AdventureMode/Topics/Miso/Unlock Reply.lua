-- |[ ================================== Unlock Reply Script =================================== ]|
--This script is called as soon as the player unlocks an item in the interface. The item is not
-- automatically created, rather this script is called to handle that. There are special cases
-- where an item is meant to upgrade an existing item which need to be handled.
--The first argument passed is the name of the item that is to be unlocked.
local iArgsTotal = LM_GetNumOfArgs()
local sItemName = LM_GetScriptArgument(0)

--Retrieve this string, it is used to pass extra information such as upgrade-in-place cases.
-- If there is no special info, it will be "Null".
local sSpecialInfoString = AM_GetProperty("Unlock Replace Info")

--Diagnostics.
local bDiagnostics = fnAutoCheckDebugFlag("Unlock: Reply")

--Diagnostics.
Debug_PushPrint(bDiagnostics, "Running Miso's Unlock Reply script.\n")
Debug_Print("Unlocked item name: " .. sItemName .. "\n")

-- |[ ============ Item Creation ============= ]|
--Create the item by calling the usual item handler.
LM_ExecuteScript(gsItemListing, sItemName)

--Diagnostics.
Debug_Print("Called item creation script.\n")

--If the sSpecialInfoString is not "Null" then it needs to be handled here.
if(sSpecialInfoString == "Null") then 
	Debug_PopPrint("No special info strings. Finishing.\n")
	return
end

-- |[ ============ Special Cases ============= ]|
--Diagnostics.
Debug_Print("Has special info string. Handling.\n")
Debug_Print("String: " .. sSpecialInfoString .. "\n")

--Subdivide it by | to split commands up.
local saMajorCommands = fnSubdivide(sSpecialInfoString, "|")
for i = 1, #saMajorCommands, 1 do
	
	--Further subdivide the command into sub-arguments. It is possible there is only one. The first
	-- argument is always the command.
	local saArguments = fnSubdivide(saMajorCommands[i], ":")
	
	--Upgrade-in-place. The first argument is the name of the item that is being replaced, but this
	-- item is equipped on a party member. We need to locate that item, remove it, and replace it
	-- with the one just created. Gems should be handled by the internal equipping routine.
	if(saArguments[1] == "UpgradeInPlace" and #saArguments >= 2) then
	
		--Fast-access variables.
		local bFoundMatch = false
		local sReplaceItemName = saArguments[2]
	
		--Diagnostics.
		Debug_Print("Has upgrade-in-place order.\n")
		Debug_Print(" Name of item to replace: " .. sReplaceItemName .. "\n")
	
		--Scan the active party. If multiple characters have the same item equipped, then
		-- the first one found is upgraded.
		local iActivePartySize = AdvCombat_GetProperty("Active Party Size")
		for c = 0, iActivePartySize-1, 1 do
			
			AdvCombat_SetProperty("Push Active Party Member By Slot", c)
			
				--Diagnostics.
				local sCharacterName = AdvCombatEntity_GetProperty("Internal Name")
				Debug_Print(" Scanning " .. sCharacterName .. "\n")
			
				--Call subroutine, which will return a list containing any matches.
				local zaList = AdvEntHelper:fnScanActiveForItem(sReplaceItemName)
				
				--If the list has any elements, the first one is used.
				if(#zaList > 0) then
			
					--Variables.
					local iSlotIndex = zaList[1][1]
					local sSlotName  = zaList[1][2]
		
					--Diagnostics.
					Debug_Print("  Found a match. Item in slot " .. iSlotIndex .. " is the replacement item.\n")
					Debug_Print("  Slot in question is: " .. sSlotName .. "\n")
				
					--Equip.
					bFoundMatch = true
					AdvCombatEntity_SetProperty("Equip Item To Slot", sSlotName, sItemName)
				end
			DL_PopActiveObject()
			
			--If a match was found, stop.
			if(bFoundMatch) then break end
		end
		
		--If we found and replaced an item, we need to remove the item from the inventory.
		if(bFoundMatch) then
			AdInv_SetProperty("Remove Item", sReplaceItemName, 1)
		
		--If we did not replace an item, that's an error. The C++ should not send a replacement order
		-- if the item wasn't equipped.
		else
			io.write("Warning. Ordered to upgrade-in-place item " .. sReplaceItemName .. " but it was not found on any active characters.\n")
		end
	end
end

--Diagnostics.
Debug_PopPrint("Finished Unlock Reply after special handler.\n")
