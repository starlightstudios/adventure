-- |[Salted Fish Recipes]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "EvermoonNWBookshelf", "Leave")

--Standard.
Append("[VOICE|Mei](A book about how to make salted fish palatable.[P] The back cover promises to finally reveal the secret.)[B][C]")
Append("[VOICE|Mei](The first page just says [SPOOKY]\"There is no way, you DINK.\"[NOSPOOKY])[B][C]")
Append("[VOICE|Mei](The rest of the book is empty...)[B][C]")
Append("[VOICE|Mei](Wait, are these water stains on the first page?[P] Was someone crying?)[B][C]")
