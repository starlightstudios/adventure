-- |[Foreign Languages]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "EvermoonNWBookshelf", "Leave")

--Standard.
Append("[VOICE|Mei](Maybe this is a book about translating foreign languages?)[B][C]")
Append("[VOICE|Mei](No...[P] it's a book about two girls who speak different languages meeting and teaching each other.)[B][C]")
Append("[VOICE|Mei](Then they have sex a lot, but it takes twenty chapters to get that far.)[B][C]")
Append("[VOICE|Mei](At first I thought this was a story, but then it just became smut...)[B][C]")
