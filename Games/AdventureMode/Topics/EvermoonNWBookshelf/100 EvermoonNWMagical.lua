-- |[Magical Settlement]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "EvermoonNWBookshelf", "Leave")

--Standard.
Append("[VOICE|Mei](Seems to be part of a long series about a bunch of magical girls.)[B][C]")
Append("[VOICE|Mei](The main character was a man at the start of the series, but has slowly been turned into a magical girl due to a series of mishaps.)[B][C]")
Append("[VOICE|Mei](In this one, he/she gets into a drinking contest with the camp leader.)[B][C]")
Append("[VOICE|Mei](...)[B][C]")
Append("[VOICE|Mei](Wow!)[B][C]")
Append("[VOICE|Mei](...)[B][C]")
Append("[VOICE|Mei](That was close!)[B][C]")
Append("[VOICE|Mei](...)[B][C]")
Append("[VOICE|Mei](Is that all for this novella?[P] I want to see what happens next![P] Argh!)[B][C]")
