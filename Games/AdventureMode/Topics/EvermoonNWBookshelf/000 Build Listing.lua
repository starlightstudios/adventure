-- |[Build Listing]|
--Builds a list of topics this NPC will respond to, and at what levels. Called at Adventure Mode startup.
local sNPCName = "EvermoonNWBookshelf"

--Topic listing.
fnConstructTopic("EvermoonNWChemistry", "Chemical Interest",         1, sNPCName, 0)
fnConstructTopic("EvermoonNWLanguage",  "Foreign Languages",         1, sNPCName, 0)
fnConstructTopic("EvermoonNWSalted",    "Salted Fish Recipes",       1, sNPCName, 0)
fnConstructTopic("EvermoonNWMagical",   "Magical Settlement",        1, sNPCName, 0)
