-- |[Topic Script]|
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "TiffanyBiolabs")

--This changes a lot if the player has done the runestone upgrade quest.
local iRuneUpgradeCompleted = VM_GetVar("Root/Variables/Chapter5/Scenes/iRuneUpgradeCompleted", "N")

--Has not done it:
if(iRuneUpgradeCompleted == 0.0) then
    Append("55:[E|Neutral] Unit 771852, do you believe there are other runestones like yours?[B][C]")
    Append("Christine:[E|Neutral] I know for certain there are others like it.[P] Five others.[B][C]")
    Append("55:[E|Neutral] Interesting.[B][C]")
    Append("Christine:[E|Smirk] You seemed to know about them when you ambushed me under sector 96.[P] Did you know of the others?[B][C]")
    Append("55:[E|Neutral] Yes, Regulus City has collected data suggesting there are at least two more.[P] These are based on reports from the surface and memories from abducted humans that were converted.[B][C]")
    Append("55:[E|Neutral] The reports did not seem to understand the true gravity of what the runestones may mean, considering your interactions with Vivify.[B][C]")
    Append("Christine:[E|Neutral] You're right.[P] I wonder if those other five are going through what we're going through.[B][C]")
    Append("Christine:[E|Neutral] Do you know any specifics about the two bearers?[B][C]")
    Append("55:[E|Neutral] I found an unecrypted report on one of them, named 'Jeanne'. Mostly a physical description, short, quiet, shy, and good with magic.[B][C]")
    Append("55:[E|Neutral] The report mentioned transformative abilities, everything else I found on the individual was censored.[B][C]")
    Append("Christine:[E|Smirk] And the other?[B][C]")
    Append("55:[E|Neutral] A team captured videograph footage of her while filming for an action movie, however, they did not know what the runestone was.[B][C]")
    Append("55:[E|Neutral] No, I was not able to locate the videograph itself. It is redacted.[B][C]")
    Append("55:[E|Neutral] As you may expect, both are targets for conversion by the authorities, and both have completely disappeared from the limited sight of abductions agents.[B][C]")
    Append("Christine:[E|Blush] Heh...[B][C]")
    Append("Christine:[E|Smirk] It'd be nice to meet another unit from Earth, maybe swap stories, reminisce on where we came from.[B][C]")
    Append("Christine:[E|Blush] And plot our eventual return to bring mechanical perfection to the people of Earth...[B][C]")
    Append("55:[E|Neutral] My assessment was correct.[B][C]")
    Append("55:[E|Neutral] How are you certain there are five others, though?[B][C]")
    Append("Christine:[E|Neutral] Same way I knew I could transform myself, somehow.[P] It's just something I know, like I always knew it.[B][C]")
    Append("55:[E|Neutral] Interesting.[P] If we are successful here, we may wish to find these other five bearers and coordinate with them.[P] There may be other threats you are uniquely suited for like Project Vivify.[B][C]")
    Append("Christine:[E|Smirk] Agreed![B][C]")
else
    Append("55:[E|Neutral] Unit 771852, do you know how many other runestones there are?[B][C]")
    Append("Christine:[E|Neutral] Five others.[P] I am totally certain.[B][C]")
    Append("55:[E|Neutral] Interesting.[B][C]")
    Append("Christine:[E|Smirk] You seemed to know about them when you ambushed me under sector 96.[P] What did you know, then?[B][C]")
    Append("55:[E|Neutral] I looked up what I could on the network when I arrived in the city.[P] I found reports of two, though neither resemble yours.[B][C]")
    Append("55:[E|Neutral] I found an unecrypted report on one of them, named 'Jeanne'.[P] Mostly a physical description, short, quiet, shy, and good with magic.[B][C]")
    Append("55:[E|Neutral] The report mentioned transformative abilities, everything else I found on the individual was censored.[B][C]")
    Append("Christine:[E|Smirk] And the other?[B][C]")
    Append("55:[E|Neutral] A team captured videograph footage of her while filming for an action movie, however, they did not know what the runestone was.[B][C]")
    Append("55:[E|Neutral] No, I was not able to locate the videograph itself.[P] It is redacted.[B][C]")
    Append("55:[E|Neutral] As you may expect, both are targets for conversion by the authorities, and both have completely disappeared from the limited sight of abductions agents.[B][C]")
    Append("Christine:[E|Smirk] What about the one that we saw punching an orc?[B][C]")
    Append("55:[E|Smirk] You enjoy that image?[P] Yes, but that was redacted and I did not know of her until we decrypted the academic information.[B][C]")
    Append("55:[E|Neutral] There are also rumours about a person named 'Mei' in the Trannadar region of Arulenta, however, no eyewitnesses or images.[P] Only rumours about a foreign magician who could transform.[B][C]")
    Append("Christine:[E|Neutral] And the administration has absolutely nothing about the last one?[B][C]")
    Append("55:[E|Neutral] Nothing at all.[P] Until you mentioned five others, I did not, either.[B][C]")
    Append("Christine:[E|Neutral] It's...[P] something I just know.[P] Like how I knew I could transform, I know there are others.[B][C]")
    Append("Christine:[E|Smirk] I bet they'd love to be golems, too![P] So we should make a point of finding them.[B][C]")
    Append("55:[E|Neutral] If we are successful here, we may wish to find these other five bearers and coordinate with them.[P] There may be other threats you are uniquely suited for like Project Vivify.[B][C]")
    Append("Christine:[E|Smirk] Agreed![B][C]")
end