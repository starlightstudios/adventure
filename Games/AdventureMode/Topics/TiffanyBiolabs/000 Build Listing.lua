-- |[Build Listing]|
--Builds a list of topics this NPC will respond to, and at what levels. Called at Adventure Mode startup.
local sNPCName = "TiffanyBiolabs"
gsActiveNPCName = sNPCName

--Topic listing.
fnConstructTopicStd(true,  "Biolabs_NextMove",    "Our Next Move")
fnConstructTopicStd(false, "Biolabs_2856",        "2856")
fnConstructTopicStd(false, "Biolabs_Alraune",     "Alraunes")
fnConstructTopicStd(false, "Biolabs_Biolabs",     "Biolabs")
fnConstructTopicStd(false, "Biolabs_Biology",     "Biology")
fnConstructTopicStd(false, "Biolabs_Cassandra",   "Cassandra")
fnConstructTopicStd(false, "Biolabs_Darkmatters", "Darkmatters")
fnConstructTopicStd(false, "Biolabs_Games",       "Games")
fnConstructTopicStd(false, "Biolabs_Heroics",     "Heroics")
fnConstructTopicStd(false, "Biolabs_Magic",       "Magic")
fnConstructTopicStd(false, "Biolabs_Monsters",    "Monsters")
fnConstructTopicStd(false, "Biolabs_Processors",  "Processors")
fnConstructTopicStd(false, "Biolabs_Rebellion",   "Rebellion")
fnConstructTopicStd(false, "Biolabs_Runestones",  "Runestones")
