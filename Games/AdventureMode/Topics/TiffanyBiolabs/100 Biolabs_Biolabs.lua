-- |[Topic Script]|
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "TiffanyBiolabs")

Append("55:[E|Neutral] Christine, what is your opinion on the biolabs themselves?[B][C]")
Append("Christine:[E|Smirk] Reminds me of jolly old England.[P] If there were fewer monsters trying to kill us, it'd be like a September day in Whitley Park.[B][C]")
Append("55:[E|Neutral] Does this remind you of Earth?[B][C]")
Append("Christine:[E|Smirk] Green fields, lots of trees, they somehow have a blue sky in the biolabs despite it being a dome...[B][C]")
Append("55:[E|Neutral] It is part of the simulation of natural sunlight.[B][C]")
Append("Christine:[E|Laugh] Well then that makes it nothing like home![B][C]")
Append("Christine:[E|Smirk] ...[P] The joke being how much it rains in England.[P] It's an island so we get a lot of rainy days.[B][C]")
Append("Christine:[E|Sad] ...[P] Oh...[B][C]")
Append("55:[E|Neutral] What is it?[B][C]")
Append("Christine:[E|Sad] I just said 'home', didn't I?[B][C]")
Append("Christine:[E|Sad] Sector 96 is my home, that's where I live.[P] That's where I want to be, fixing things and being with Sophie.[B][C]")
Append("Christine:[E|Sad] But I guess Earth is still my home, too.[B][C]")
Append("55:[E|Neutral] Does it upset you to have multiple homes?[B][C]")
Append("Christine:[E|Neutral] When you put it that way, no.[P] I suppose two is better than one, right?[B][C]")
Append("Christine:[E|Sad] But will I ever be able to visit?[P] Will I have anything to visit about?[P] I'm very different now, would they accept me?[B][C]")
Append("55:[E|Neutral] I have no answers to these questions.[B][C]")
Append("Christine:[E|Neutral] Sorry, 55.[P] You're probably not the best person to ask.[B][C]")
Append("Christine:[E|Smirk] In a way, the biolabs are like England, so I like them, but mostly they aren't because of many little differences.[P] That answers your question.[B][C]")
Append("55:[E|Neutral] It does, thank you.[B][C]")
