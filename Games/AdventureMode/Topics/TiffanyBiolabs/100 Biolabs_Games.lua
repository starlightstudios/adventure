-- |[Topic Script]|
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "TiffanyBiolabs")

Append("55:[E|Neutral] 771852, you have stated that you had video games on Earth.[P] How do they compare to the games on Regulus?[B][C]")
Append("Christine:[E|Neutral] The production values are actually higher in Earth games even if the technology is nowhere near as advanced.[B][C]")
Append("Christine:[E|Offended] But the heart just isn't in them as much.[P] All of the best video games on Earth were made by people who really cared and had the talent and resources to bring their vision to life.[B][C]")
Append("Christine:[E|Offended] Independent developers made many of the best games before I left, but most people who played games only played the trash triple-A games because they were popular.[B][C]")
Append("55:[E|Neutral] Are these your opinions, or those of someone else?[B][C]")
Append("Christine:[E|Neutral] Obviously, these are my opinions.[P] Who else's could they be?[B][C]")
Append("Christine:[E|Smirk] Anyway, here on Regulus, all of the video games are passion projects made by a unit with some spare time and a dream.[P] Our technology is just good enough to make that possible.[B][C]")
Append("Christine:[E|Smirk] We may not have the budget to get a famous unit in the game, or have big rendered cutscenes, but you care about what happens because they took the time to make you care.[B][C]")
Append("Christine:[E|Smirk] So I guess I would say that games on Regulus are similar to independent games on Earth if you vastly increased their budgets.[B][C]")
Append("Christine:[E|Neutral] It's not that I hold it against the people of Earth for playing games devoid of artistic integrity.[P] Do what you like to do, not everything has to be a masterpiece.[B][C]")
Append("Christine:[E|Neutral] Just expand your horizons a bit, you will enjoy the things that are out there that aren't brown and grey military shooters.[B][C]")
Append("55:[E|Neutral] I see.[P] Thank you.[B][C]")
