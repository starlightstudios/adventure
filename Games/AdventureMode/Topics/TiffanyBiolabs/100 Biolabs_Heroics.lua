-- |[Topic Script]|
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "TiffanyBiolabs")

Append("55:[E|Neutral] Christine, I would like to criticize your tactics for a moment.[B][C]")
Append("Christine:[E|Neutral] You don't need my permission to criticize me, 55.[B][C]")
Append("55:[E|Neutral] I do.[P] In the past, my criticisms have upset others.[P] I have elected to preface them with a permission request.[B][C]")
Append("Christine:[E|Sad] Well that's an improvement, but why do your criticisms upset people?[B][C]")
Append("55:[E|Neutral] You have a tendency towards dangerous heroics.[P] You may compromise others with your actions.[B][C]")
Append("Christine:[E|Offended] Question answered.[B][C]")
Append("55:[E|Neutral] Why do you behave this way when you know it is dangerous, and you know the statistics on heroic actions?[B][C]")
Append("55:[E|Neutral] No doubt you are aware they tend to result in the retirement of the unit enacting them.[P] I have sent you the data multiple times.[B][C]")
Append("Christine:[E|Offended] Don't worry, I read it.[P] My PDU notifies me every time.[B][C]")
Append("Christine:[E|Neutral] 55, you need to understand that I do what I think is right, and what I think is right is shouldering burdens on myself rather than others.[B][C]")
Append("Christine:[E|Sad] I guess I get it from my father.[P] He was a horrible man, but he never once asked something of his friends he wouldn't do himself.[B][C]")
Append("55:[E|Neutral] This has triggered a negative emotional response.[B][C]")
Append("Christine:[E|Sad] I'm a lot more like him than I thought.[P] I always tried consciously to take after my mother, but I can't escape that he is half of me.[B][C]")
Append("Christine:[E|Sad] He possessed neutral virtues.[P] He was brave, but if his cause is evil, his bravery is virtuous but not good.[B][C]")
Append("Christine:[E|Neutral] I'm different because of my cause, not my bravery.[P] I guess I owe him that much, too.[P] Would I be who I am if I didn't reject his ideology?[B][C]")
Append("55:[E|Neutral] Your flair for theatrics is genetic, then?[B][C]")
Append("Christine:[E|Neutral] Yeah, and you know what?[P] I think your calm, rational demeanor is genetic, too.[B][C]")
Append("Christine:[E|Smirk] Your sister has the rational part at any rate.[P] Calm, less-so.[B][C]")
Append("55:[E|Neutral] I see.[P] The idea of neutral virtues is illuminating.[B][C]")
