-- |[Topic Script]|
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "TiffanyBiolabs")

Append("55:[E|Neutral] Unit 771852, I need to ask you about something personal.[B][C]")
Append("Christine:[E|Neutral] Your sister?[B][C]")
Append("55:[E|Neutral] Yes.[P] How did you know?[B][C]")
Append("Christine:[E|Smirk] The list of things that are 'personal' with Unit 2855 is pretty short, and a small number cause her not to smirk.[B][C]")
Append("55:[E|Neutral] I see.[P] That is good.[B][C]")
Append("55:[E|Neutral] You appear to understand how to read my emotions very well.[P] Can we use this knowledge against my identical twin?[B][C]")
Append("Christine:[E|Offended] No, unfortunately.[P] Believe me, I want to.[B][C]")
Append("Christine:[E|Neutral] Despite your being twins, you actually don't have as much in common as you might think.[P] She has several lifetimes of experience being 2856, you have little of being 2855.[B][C]")
Append("Christine:[E|Neutral] Because I have known you for almost your entire life, I have seen how you have changed with it.[P] I know how you will progress, because I have known people like you.[B][C]")
Append("55:[E|Neutral] You did not mention any of Earth's robotic war criminals in the past.[B][C]")
Append("Christine:[E|Laugh] Ha ha ha![P] Not what I meant![B][C]")
Append("55:[E|Neutral] (She laughed at that...)[B][C]")
Append("55:[E|Smirk] Thank you for laughing at my joke.[B][C]")
Append("Christine:[E|Neutral] No, 55, what I mean is the little girls I used to teach.[P] Everyone is different, but people are usually products of their environments, and there are certain paths they tend to go down.[B][C]")
Append("Christine:[E|Neutral] I've seen yours, and I've seen hers.[P] She is...[P] a control freak.[B][C]")
Append("Christine:[E|Offended] Always in charge, always gets her way, abuses those who do anything she doesn't approve of.[P] Has to have control over the situation.[B][C]")
Append("55:[E|Neutral] I see.[P] So your obstinacy when speaking with her?[B][C]")
Append("Christine:[E|Offended] You can really set off a control freak by just not listening to them.[B][C]")
Append("Christine:[E|Neutral] It's probably not productive, but at the bare minimum it may anger her and cause her to make mistakes we can exploit.[B][C]")
Append("55:[E|Neutral] So when dealing with her, you are being obtuse [P]*deliberately*.[B][C]")
Append("55:[E|Neutral] I will take that into consideration for our future conversations.[B][C]")
Append("Christine:[E|Angry] Unit 2855, what are you implying?[B][C]")
Append("55:[E|Smug] Nothing at all.[P] Let us discuss another topic.[B][C]")
