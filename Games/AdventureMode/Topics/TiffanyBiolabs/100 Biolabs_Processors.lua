-- |[Topic Script]|
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
local iHasSteamDroidForm = VM_GetVar("Root/Variables/Global/Christine/iHasSteamDroidForm", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "TiffanyBiolabs")

Append("55:[E|Neutral] Christine, I would like to discuss 'All My Processors'.[B][C]")
Append("Christine:[E|Smirk] Oh that drama show that's always on the RVDs?[P] What of it?[B][C]")
Append("Christine:[E|Happy] What's your favourite episode?[P] Mine's series 451, episode 12, when Daring hangs upside down to trick Drakon and then asks Drakon where 'name' is, and we don't find out Name is, if anyone, or where, and then asks again and she just says 'Drakon' because it was all a part of a plot to trick Jobel into helping Patricia![B][C]")
Append("Christine:[E|Happy] And then Daring buys an entire city by turning all the furniture in the inn into gold![B][C]")
Append("55:[E|Neutral] I do not have a favourite episode.[P] I often listen to the show while I am researching or attempting to access restricted areas.[B][C]")
Append("Christine:[E|Smirk] Oh?[P] Why?[B][C]")
Append("55:[E|Neutral] The harmonics of the show interface well with my processor.[P] I do not know why.[P] That is, in fact, an area of active research.[B][C]")
Append("Christine:[E|Smirk] So what did you want to talk about it for?[B][C]")
Append("55:[E|Neutral] I recently discovered the show is written by a primitive artificial intelligence under development at the Arcane University.[B][C]")
Append("55:[E|Neutral] The staff writers take the AI's work and translate it into scripts for the actors.[B][C]")
Append("Christine:[E|Happy] Wow![P] Maybe someday soon, AIs will be as smart as partirhumans![B][C]")
Append("55:[E|Neutral] It would require either many advances in synthetic intelligence, or else enormous amounts of redundancies and an incredible amount of processing power.[B][C]")
Append("55:[E|Neutral] It is theoretically possible, but Regulus City does not yet have the infrastructure for it.[B][C]")
Append("55:[E|Neutral] But, assuming that were the case.[P] I...[B][C]")
Append("55:[E|Down] How does one distinguish an AI from a partirhuman, like us?[B][C]")
Append("55:[E|Down] What if I, for example, had never been a human, but instead was an AI placed inside a command unit's body?[B][C]")
Append("Christine:[E|Neutral] Our processors can't be generated synthetically, 55.[P] They are created from the human brain during the transformation process.[B][C]")
Append("55:[E|Neutral] True, but there is no physical difference.[P] In the future, the creation of an intelligence like ours will be possible.[P] How will we know when the AI becomes a person?[B][C]")
Append("Christine:[E|Smirk] While that's probably a really tough philosophical question, I do have an answer.[B][C]")
Append("Christine:[E|Smirk] Art.[B][C]")
Append("55:[E|Neutral] ...[P] Art?[B][C]")
Append("Christine:[E|Smirk] When the machine is capable of producing works of art, it is now a person.[B][C]")
Append("55:[E|Neutral] But 'All My Processors' is a work of art, isn't it?[B][C]")
Append("Christine:[E|Smirk] No, not from the perspective of the AI.[P] To the AI there is no distinction between anything.[P] It creates what it creates.[B][C]")
Append("Christine:[E|Smirk] It is us, the observer, who are creating the work of art because we interpret it as such.[P] So, the AI must be capable of perceiving art, struggling with it, and learning from it.[P] Then, creating it.[P] Then it will be a person.[B][C]")
Append("55:[E|Neutral] I have little appreciation of art, Christine.[B][C]")
if(iHasSteamDroidForm == 1.0) then
    Append("Christine:[E|Smirk] SX-399.[B][C]")
    Append("55:[E|Upset] What about her?[B][C]")
    Append("Christine:[E|Smirk] Talk about her a bit.[B][C]")
    Append("55:[E|Neutral] Her ocular receptors are...[P] round.[B][C]")
    Append("55:[E|Blush] Her hair is red and soft.[P] Her lips are...[P] soft...[B][C]")
    Append("Christine:[E|Laugh] You appreciate art, 55.[P] You're not all-the-way robot![P] Ha ha![B][C]")
    Append("55:[E|Blush] (Yes, SX-399 is truly a work of art...)[B][C]")
else
    Append("Christine:[E|Smirk] Then maybe I'll take you on a tour of the city when all this is over.[B][C]")
    Append("Christine:[E|Smirk] The architecture of Regulus City is a work of art, isn't it?[P] Tiny towers thrusting defiantly at infinity, promising it that we will not be limited by the void of space.[B][C]")
    Append("Christine:[E|Smirk] Our little home of light in a sea of black.[P] That's art, isn't it?[B][C]")
    Append("55:[E|Smirk] I suppose it is.[P] Thank you, Christine.[B][C]")
end
