-- |[Topic Script]|
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm           = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
local iSpokeTo2856Biolabs      = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeTo2856Biolabs", "N")
local iSawMusicComments        = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMusicComments", "N")
local iSawRaijuIntro           = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawRaijuIntro", "N")
local iGotBreachingTools       = VM_GetVar("Root/Variables/Chapter5/Scenes/iGotBreachingTools", "N")
local iBreachedDoor            = VM_GetVar("Root/Variables/Chapter5/Scenes/iBreachedDoor", "N")
local iMetVivify               = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetVivify", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "TiffanyBiolabs")

Append("55:[E|Neutral] We should reassess our current objective.[B][C]")
Append("Christine:[E|Neutral] Affirmative, Unit 2855.[B][C]")

--Player needs to make it to the Raiju Ranch.
if(iSawRaijuIntro == 0.0) then
    
    --Hasn't seen the music comments:
    if(iSawMusicComments == 0.0) then
        
        --Didn't speak to 2856 yet:
        if(iSpokeTo2856Biolabs == 0.0) then
            Append("Christine:[E|Neutral] Considering our temporary alliance, it may be best to coordinate with Unit 2856.[P] She's over there at the hydrologic uplink terminal.[B][C]")
            Append("Christine:[E|Laugh] Or we could not, and that'd really set her off![B][C]")
            Append("55:[E|Neutral] Please focus on the mission assessment, Unit 771852.[B][C]")
            Append("Christine:[E|Neutral] Affirmative.[P] Assessment is to speak with Unit 2856.[B][C]")
            Append("55:[E|Neutral] Mission assessment acceptable.[P] Thank you, Unit 771852.[B][C]")
        
        
        --Spoke to her:
        else
            Append("Christine:[E|Neutral] Our current objective is the Raiju Ranch, to link up with what little security presence remains and figure out how we're going to stop Vivify.[B][C]")
            Append("55:[E|Neutral] Secondary objectives.[B][C]")
            Append("Christine:[E|Neutral] Assist the revolutionary cause in whatever way possible, including helping units in trouble or frustrating the activity of Vivify's followers.[B][C]")
            Append("55:[E|Neutral] Mission assessment acceptable.[P] Thank you, Unit 771852.[B][C]")
        end
    
    --Knows they need to get to the Raiju Ranch:
    else
        Append("Christine:[E|Neutral] Our current objective is the Raiju Ranch, to link up with what little security presence remains and figure out how we're going to stop Vivify.[B][C]")
        Append("55:[E|Neutral] Secondary objectives.[B][C]")
        Append("Christine:[E|Neutral] Assist the revolutionary cause in whatever way possible, including helping units in trouble or frustrating the activity of Vivify's followers.[B][C]")
        Append("55:[E|Neutral] Mission assessment acceptable.[P] Thank you, Unit 771852.[B][C]")
    end

--Player has reached the Raiju Ranch, needs the breaching tools:
elseif(iGotBreachingTools == 0.0) then
    Append("Christine:[E|Neutral] In order to access the Epsilon labs, we're going to need some tools to cut through the military-grade fortifications there.[B][C]")
    Append("Christine:[E|Neutral] These tools are stored in a shed on the eastern side of the Raiju Ranch.[B][C]")
    Append("55:[E|Neutral] Secondary objectives.[B][C]")
    Append("Christine:[E|Neutral] Assist the revolutionary cause in whatever way possible, including helping units in trouble or frustrating the activity of Vivify's followers.[B][C]")
    Append("55:[E|Neutral] Mission assessment acceptable.[P] Thank you, Unit 771852.[B][C]")

--Player has the breaching tools:
elseif(iBreachedDoor == 0.0) then
    Append("Christine:[E|Neutral] In order to access the Epsilon labs, we'll need to move to the Beta-Gamma security checkpoint and check the Epsilon access in the northeastern area.[B][C]")
    Append("Christine:[E|Neutral] I can then cut the walls out and we can access the labs.[B][C]")
    Append("55:[E|Neutral] Secondary objectives.[B][C]")
    Append("Christine:[E|Neutral] Assist the revolutionary cause in whatever way possible, including helping units in trouble or frustrating the activity of Vivify's followers.[B][C]")
    Append("55:[E|Neutral] Mission assessment acceptable.[P] Thank you, Unit 771852.[B][C]")

--Player cut the door out:
elseif(iMetVivify == 0.0) then
    Append("Christine:[E|Neutral] Our objective is to confront Project Vivify and neutralize her as a threat to Regulus City.[B][C]")
    Append("Christine:[E|Sad] And possibly everything else in existence...[B][C]")
    Append("55:[E|Neutral] Do you have a plan for neutralizing her?[B][C]")
    Append("Christine:[E|Sad] No.[P] It might be a fight, it might not.[P] I simply can't be sure...[B][C]")
    Append("55:[E|Neutral] Very well.[P] Secondary objectives.[B][C]")
    Append("Christine:[E|Neutral] Assist the revolutionary cause in whatever way possible, including helping units in trouble or frustrating the activity of Vivify's followers.[B][C]")
    Append("55:[E|Neutral] Mission assessment acceptable.[P] Thank you, Unit 771852.[B][C]")

--Player has confronted Vivify:
else
    Append("Christine:[E|Neutral] With Project Vivify neutralized, we should return to the Raiju Ranch and, if possible, neutralize Unit 2856 before she has a chance to call for backup.[B][C]")
    Append("Christine:[E|Smirk] With her out of the way, our revolution will have a much better chance of success.[B][C]")
    Append("55:[E|Neutral] Correct.[P] Thank for the assessment.[P] Move out.[B][C]")
end
