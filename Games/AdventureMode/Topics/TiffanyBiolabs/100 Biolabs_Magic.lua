-- |[Topic Script]|
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "TiffanyBiolabs")

Append("Christine:[E|Smirk] Considering we've been through the Arcane University now, 55, are you thinking of maybe taking a few classes on magic?[B][C]")
Append("55:[E|Neutral] What are you referring to?[B][C]")
Append("Christine:[E|Laugh] Oh cheer up, 55![B][C]")
Append("Christine:[E|Smirk] You're a believer in the capability of science.[P] Militarily, what about magic?[B][C]")
Append("Christine:[E|Smirk] I use a bit of it in my attacks, I think, but I'm just an amateur.[P] You, however, don't use it at all.[B][C]")
Append("55:[E|Neutral] I do not have any magical talent that I am aware of.[P] I could learn, but magic cannot be programmed the way combat routines can.[B][C]")
Append("55:[E|Neutral] My understanding is that magic is a personal relationship, not a material one.[P] Everyone who interacts with magic does so according to their own terms.[B][C]")
Append("Christine:[E|Smirk] So you have researched it?[B][C]")
Append("55:[E|Neutral] For strategic reasons, yes.[P] Some of our mavericks will likely have undeveloped magical abilities that have been suppressed by a life of hard labour.[B][C]")
Append("55:[E|Neutral] Units who show exemplary arcane capability are usually reassigned to research in the university.[B][C]")
Append("Christine:[E|Neutral] Can we use it to help win the civil war?[B][C]")
Append("55:[E|Neutral] Unlikely.[P] Magic requires years of training as well as natural aptitude.[P] It'd take five years to teach a golem to hurl a fireball, or fifteen seconds to teach them how to aim a pulse rifle.[B][C]")
Append("55:[E|Neutral] Not to mention the fact that combat subroutines can teach a golem to reload and maintain the rifle in nanoseconds.[B][C]")
Append("Christine:[E|Neutral] Oh, too bad.[P] But if we find someone who has amazing magical talent?[B][C]")
Append("55:[E|Neutral] By all means, it may be easier for some to help with their magic rather than their weapons.[P] At the same time, someone who shows aptitude for repairs should be a repair unit.[B][C]")
Append("55:[E|Neutral] Magic is just another skill.[P] It has acquired its legendary reputation because of its greatest practitioners, not its average ones.[B][C]")
Append("Christine:[E|Neutral] Okay, we'll keep an eye out then![B][C]")
