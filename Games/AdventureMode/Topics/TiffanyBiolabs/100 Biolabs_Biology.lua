-- |[Topic Script]|
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "TiffanyBiolabs")

Append("55:[E|Neutral] I would like your opinion on biology and the study of organics.[B][C]")
Append("Christine:[E|Smirk] The robot in me says it's a waste of time.[P] We've already transcended organic material![B][C]")
Append("Christine:[E|Neutral] But I know the natural world has so many wonders in store for us that it'd be arrogant to assume we know the best way to do things just because we are machines.[B][C]")
Append("55:[E|Neutral] Interesting.[B][C]")
Append("Christine:[E|Smirk] Not that I want to study it myself.[P] I was rubbish at biology.[B][C]")
Append("Christine:[E|Smirk] I just think all topics are equally valid.[P] There is no practical argument for knowledge of one type over another.[B][C]")
Append("55:[E|Neutral] Should not our resources be focused on topics that will provide engineering advantages to our society?[B][C]")
Append("Christine:[E|Laugh] Ha ha![P] Of course not, my dear![B][C]")
Append("Christine:[E|Smirk] You know what it means to focus on a topic?[P] It just means the researchers will find ways to make their grant proposals line up with whatever you claim to be focusing on.[B][C]")
Append("Christine:[E|Smirk] Whatever it is the researcher is passionate about researching is what they will research.[P] Science is, in many ways, an art with specific rules.[B][C]")
Append("55:[E|Neutral] But our society has specific problems it must research answers to.[B][C]")
Append("Christine:[E|Smirk] Then there is no need to guide your scientists![P] That's what they'll want to research.[B][C]")
Append("Christine:[E|Smirk] Scientists love to solve problems, it's why they got into the field in the first place.[B][C]")
Append("Christine:[E|Smirk] Your perspective is that of a military commander, your motivation is simple, direct, and efficient.[P] You believe everyone else is motivated in the same way.[B][C]")
Append("Christine:[E|Smirk] Well, not everyone is.[P] Some units find and solve problems because that's what they like to do.[B][C]")
Append("55:[E|Neutral] I see.[P] Thank you.[B][C]")
