-- |[Topic Script]|
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm          = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
local iResolvedCassandraQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iResolvedCassandraQuest", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "TiffanyBiolabs")

--Cassandra became a golem:
if(iResolvedCassandraQuest == 1.0 or iResolvedCassandraQuest == 3.0) then
    Append("Christine:[E|Smirk] 55, I've been meaning to ask.[P] Have you heard from Cassandra at all?[B][C]")
    Append("55:[E|Neutral] Unit 771853, Logistics, Sector 15?[B][C]")
    Append("Christine:[E|Smirk] If you want to use her primary designation that's fine with me.[B][C]")
    Append("55:[E|Neutral] Cassandra has provided undercover support for us.[P] Sector 15 had many materials we could use that were not correctly documented.[P] As part of her duties, she disposed of them by giving them to us.[B][C]")
    Append("55:[E|Neutral] While she is sympathetic, however, she does not wish to participate directly.[B][C]")
    Append("Christine:[E|Neutral] Oh?[P] Why not?[P] She seems like she's pretty tough.[B][C]")
    Append("55:[E|Neutral] She is, but she declined nonetheless.[P] She has still provided us with useful equipment and recruiting information, so I consider her a useful asset for the revolution.[B][C]")
    Append("Christine:[E|Smirk] Splendid![P] I hope she comes around![B][C]")
    
--Cassandra is a human:
elseif(iResolvedCassandraQuest == 2.0 or iResolvedCassandraQuest == 4.0) then
    Append("Christine:[E|Smirk] 55, I've been meaning to ask.[P] Have you heard from Cassandra at all?[B][C]")
    Append("55:[E|Neutral] Yes.[P] She made contact with me a few days ago.[B][C]")
    Append("55:[E|Neutral] She had taken up residence with the steam droids of Steamston, and wanted to speak with me personally.[P] However, I was unable to do so, so we communicated via mail.[B][C]")
    Append("55:[E|Neutral] She is 'fine', as you would say.[B][C]")
    Append("Christine:[E|Smirk] Great![P] We've been so busy, I didn't have time to check up on her.[P] Did she fit in with the steam droids well?[B][C]")
    Append("55:[E|Neutral] Her activities have generated some goodwill with the residents of Steamston.[P] She is apparently good with a stubgun, and assists during their scavenging operations.[B][C]")
    Append("55:[E|Neutral] She was not interested in joining our movement, however.[B][C]")
    Append("Christine:[E|Neutral] It'd be pretty dangerous for an organic...[B][C]")
    Append("55:[E|Neutral] I am sure you will have time to reconnect with her in the future.[B][C]")
end
