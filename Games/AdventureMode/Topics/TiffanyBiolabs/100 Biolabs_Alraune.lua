-- |[Topic Script]|
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "TiffanyBiolabs")

Append("55:[E|Neutral] Christine, I would like you to transform into an alraune at a later date.[B][C]")
Append("Christine:[E|Neutral] I need to be transformed into one first, 55.[B][C]")
Append("55:[E|Neutral] That is what I meant.[P] I apologize if I was unclear.[B][C]")
Append("55:[E|Neutral] Having access to new forms can be useful for tactical reasons.[P] You should acquire as many as you can.[B][C]")
Append("Christine:[E|Smirk] I agree![P] I'd be like a plant repair unit![B][C]")
Append("Christine:[E|Smirk] My job in Maintenance and Repair would be so much easier if I could just ask a broken radio-transcoder which part was burnt out.[B][C]")
Append("55:[E|Upset] Tactical reasons, Christine.[P] Alraunes can regenerate and speak to plants for intelligence information on enemy movements.[B][C]")
Append("Christine:[E|Neutral] True, but I don't think they'd transform me if we asked.[P] They seem to be rather choosy about new alraunes.[B][C]")
Append("Christine:[E|Neutral] Maybe they wouldn't appreciate having their form be considered as a tactical decision rather than a philosophical one.[B][C]")
Append("55:[E|Neutral] You mean they would only transform those who agree with them?[P] That is uncommon behavior among partirhumans.[B][C]")
Append("Christine:[E|Smirk] You say that, but is it really?[P] Reports indicate a great deal of happiness among those who are transformed.[B][C]")
Append("Christine:[E|Smirk] Maybe it is causal the other way?[P] Maybe the partirhuman magic seeks out those who agree with it already, and transforms them?[B][C]")
Append("55:[E|Neutral] An interesting hypothesis.[P] I admit I am not familiar with the research literature, so I cannot provide an argument for or against.[B][C]")
Append("Christine:[E|Smirk] Nevertheless, we'll have to worry about the here and now.[P] If the chance comes up, I'd like to be a plant, but let's not force the issue.[B][C]")
