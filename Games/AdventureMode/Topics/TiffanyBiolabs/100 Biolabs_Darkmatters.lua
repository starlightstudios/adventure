-- |[Topic Script]|
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
local iHasDarkmatterForm = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasDarkmatterForm", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "TiffanyBiolabs")

--Christine has darkmatter form:
if(iHasDarkmatterForm == 1.0) then
    Append("55:[E|Neutral] 771852, diplomatic assessment of the darkmatters.[B][C]")
    Append("Christine:[E|Neutral] Affirmative.[P] The darkmatters, I believe, are mistaken about me and the future of Regulus.[P] They've seen what I've seen, and they are wrong.[B][C]")
    Append("Christine:[E|Neutral] I've been one, I [P]*am*[P] one.[P] I know how they view the universe and time.[P] It is not as unflinching as they think, because the interface point of reality was mere moments ago.[B][C]")
    Append("55:[E|Neutral] Please elaborate on this interface point.[B][C]")
    Append("Christine:[E|Neutral] Time flows in fixed, circular patterns.[P] It doesn't change, but it does repeat.[P] The darkmatters are aware of this, as well as Vivify, I think.[B][C]")
    Append("Christine:[E|Neutral] They are aware of it but cannot change it, so they enjoy the ride.[P] But they only exist in one particular instance, not all simultaneously.[B][C]")
    Append("Christine:[E|Neutral] But something odd happened recently, and time is now malleable.[P] I think my runestone is related to that.[B][C]")
    Append("55:[E|Neutral] How?[B][C]")
    Append("Christine:[E|Sad] That...[P] I don't know.[P] I don't know why anything is different this time, but I do know it is.[B][C]")
    Append("Christine:[E|Sad] If I can communicate that to them, convince them, maybe I can at least get them to stop attacking us.[B][C]")
    Append("Christine:[E|Neutral] If they do get in our way, don't hurt them anymore than is necessary.[P] I know we can find a solution.[B][C]")
    Append("55:[E|Neutral] And why do you believe this?[B][C]")
    Append("Christine:[E|Smirk] Because nobody on Regulus seems to have any idea how to negotiate, so you're all really bad at it.[B][C]")
    Append("Christine:[E|Smirk] The Administration simply takes what they want from the workers.[P] The darkmatters understand one another and are generous.[P] Nobody knows how to not get what they want.[B][C]")
    Append("Christine:[E|Smirk] If we just are firm but understanding, we can win them over.[P] I know it.[B][C]")
    Append("55:[E|Neutral] Very well.[B][C]")

--Christine does not have darkmatter form:
else
    Append("55:[E|Neutral] 771852, diplomatic assessment of the darkmatters.[B][C]")
    Append("Christine:[E|Neutral] Affirmative.[P] The darkmatters, I believe, are mistaken about me and the future of Regulus.[P] They've seen what I've seen, and they are wrong.[B][C]")
    Append("55:[E|Neutral] I do not have the understanding you do, so I will defer to your judgement.[B][C]")
    Append("55:[E|Neutral] However, the darkmatters are dangerous enemies and could strike at any moment.[P] We must at least return them to neutrality.[B][C]")
    Append("Christine:[E|Neutral] I think if we can stop Vivify, we can prove to them that they were wrong.[P] If we show them her future isn't set...[B][C]")
    Append("Christine:[E|Sad] Wow, this is getting a little complicated...[B][C]")
    Append("Christine:[E|Neutral] If they do get in our way, don't hurt them anymore than is necessary.[P] I know we can find a solution.[B][C]")
    Append("55:[E|Neutral] And why do you believe this?[B][C]")
    Append("Christine:[E|Smirk] Because nobody on Regulus seems to have any idea how to negotiate, so you're all really bad at it.[B][C]")
    Append("Christine:[E|Smirk] The Administration simply takes what they want from the workers.[P] The darkmatters understand one another and are generous.[P] Nobody knows how to not get what they want.[B][C]")
    Append("Christine:[E|Smirk] If we just are firm but understanding, we can win them over.[P] I know it.[B][C]")
    Append("55:[E|Neutral] Very well.[B][C]")
end
