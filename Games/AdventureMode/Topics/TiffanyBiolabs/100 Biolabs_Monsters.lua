-- |[Topic Script]|
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "TiffanyBiolabs")

Append("55:[E|Neutral] 771852, you have knowledge of such matters.[P] Do you know where the creatures in the biolabs are coming from?[B][C]")
Append("Christine:[E|Neutral] If you're asking about their true origins, I really don't know.[P] They were not here, and now they are.[B][C]")
Append("55:[E|Neutral] Interesting, but I meant their infiltration vector.[B][C]")
Append("Christine:[E|Neutral] They're followers of 20, that unit who organized the gala.[P] I expect they entered through the university basement and spread out through the domes.[B][C]")
Append("55:[E|Neutral] That means their retreat vector would be limited to that, assuming the organic growth has not compromised the biolabs yet.[B][C]")
Append("Christine:[E|Sad] It hasn't.[P] Yet.[B][C]")
Append("55:[E|Neutral] That is good news for us.[P] Final question.[B][C]")
Append("55:[E|Neutral] Can we use them against the administration?[B][C]")
Append("Christine:[E|Offended] No.[B][C]")
Append("55:[E|Neutral] Please do not become emotional.[P] I am asking this question for due diligence purposes.[B][C]")
Append("Christine:[E|Neutral] Sorry, it's just -[P] that's something that the Administration would do.[P] After all, that's one of their reasons for researching Project Vivify.[B][C]")
Append("55:[E|Neutral] I am aware of that.[B][C]")
Append("Christine:[E|Neutral] Good.[P] We have to be better than them.[B][C]")
Append("55:[E|Neutral] They will not hesitate to weaponize them against the rebels if they are able to.[P] Are you prepared to give up the rebellion if we cannot win without their help?[B][C]")
Append("Christine:[E|Sad] I think we can win without resorting to such crimes, but...[B][C]")
Append("Christine:[E|Sad] 55, you're the tactician.[P] I give you permission to do it on the condition that you prove it is tactically vital to do so.[B][C]")
Append("Christine:[E|Sad] And that you, and I, are prepared to be punished for those crimes should we win.[B][C]")
Append("55:[E|Neutral] Freedom trumps moral concerns?[B][C]")
Append("Christine:[E|Neutral] Yes.[P] We want to keep our hands clean because that's the right thing to do, but the rightest thing to do is bring freedom to the people of Regulus.[B][C]")
Append("Christine:[E|Neutral] If we lose when we could have won, then that's the larger moral failure.[P] But this fact does not excuse us.[P] We must face justice for what we will do.[B][C]")
Append("Christine:[E|Neutral] Some people act like self-defense is justified because it's a necessary use of violence.[P] What it actually is, is a tragedy where there are no good choices.[B][C]")
Append("55:[E|Neutral] I see.[P] It is not enough to win most efficiently, we must win the best moral victory possible.[B][C]")
Append("55:[E|Neutral] How can we weigh the losses we will take versus the justifications?[P] When does someone's life become worth less than moral purity?[B][C]")
Append("Christine:[E|Neutral] That one is a bit easier.[P] It is never just the choice between those two things.[P] If you commit war crimes to win, you normalize war crimes.[P] In the future, you will do more.[B][C]")
Append("Christine:[E|Neutral] If you lionize victory, make war seem like a good thing, there will be more wars.[P] We are responsible for that future, too.[B][C]")
Append("55:[E|Down] This is extremely complicated.[B][C]")
Append("Christine:[E|Smirk] We'll get through it together, 55.[B][C]")
