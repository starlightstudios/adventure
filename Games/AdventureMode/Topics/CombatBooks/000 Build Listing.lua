-- |[Build Listing]|
--Builds a list of topics this NPC will respond to, and at what levels. Called at Adventure Mode startup.
local sNPCName = "CombatBooks"

--Topic listing.
fnConstructTopic("Blinding",       "Blinding and Accuracy", 1, sNPCName, 0)
fnConstructTopic("Reinforcements", "Reinforcements",        1, sNPCName, 0)
fnConstructTopic("Tips",           "Monstergirl Tips",      1, sNPCName, 0)
