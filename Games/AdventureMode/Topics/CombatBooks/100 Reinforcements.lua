-- |[Reinforcements]|
--Dialogue script.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "CombatBooks", "Leave")

--Common.
WD_SetProperty("Major Sequence Fast", true)

Append("[VOICE|Mei](When engaging enemies, they will chase you a certain distance if they spot you.[P] If another enemy is nearby, they will chase you, and attempt to join the battle.)[B][C]")
Append("[VOICE|Mei](If extremely close, they may join the battle immediately.[P] Otherwise, a number will appear above their heads, indicating how many turns they are from joining.)[B][C]")
Append("[VOICE|Mei](Having another enemy join the battle can make it far more difficult, but it can also make it easier since you can build up buffs or combo-points to unleash.)[B][C]")
Append("[VOICE|Mei](When possible, try to pick enemies off one at a time.[P] Isolate them and lead them away, then take them out!)[B][C]")
Append("[VOICE|Mei](Some enemies may move in formation or patrol around, though, which can make isolating them difficult.[P] In these cases, avoid them or be ready for a big battle!)[B][C]")
Append("[VOICE|Mei](You can also sneak up behind enemies and rapidly press the activate key to mug them.[P] Not only does this disable them, it also breaks up groups so they can't reinforce each other.)[B][C]")
	
