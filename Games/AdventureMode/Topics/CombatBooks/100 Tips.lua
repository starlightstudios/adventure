-- |[Tips]|
--Dialogue script.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "CombatBooks", "Leave")

--Common.
WD_SetProperty("Major Sequence Fast", true)

Append("[VOICE|Mei](Slimes are vulnerable to different damage types based on their color, but they are all fairly resistant to bleeds.)[B][C]")
Append("[VOICE|Mei](Alraunes, being plants, are vulnerable to fire.[P] They are also slightly vulnerable to freezing.)[B][C]")
Append("[VOICE|Mei](Bee girls are difficult to blind, but vulnerable to bleeds and fire.)[B][C]")
Append("[VOICE|Mei](Werecats are very fast, so the use of a fast ability like Quick Strike will help.[P] They scare easily, just like housecats, so try Terrifying attacks on them.)[B][C]")
Append("[VOICE|Mei](And if you're unlucky enough to run into the undead -[P] they dislike fire.[P] A lot.[P] And they don't have much to bleed, so don't bother trying to bleed or poison them.)[B][C]")
	
