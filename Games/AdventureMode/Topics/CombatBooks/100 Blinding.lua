-- |[Blinding]|
--Dialogue script.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "CombatBooks", "Leave")

--Common.
WD_SetProperty("Major Sequence Fast", true)

Append("[VOICE|Mei](Blinding attacks will reduce their target's accuracy.[P] Blinds stack together, and can make an enemy unable to land any hits.)[B][C]")
Append("[VOICE|Mei](Blinding effects may be more effective against some enemies, and less against others.[P] Bee Girls are resistant -[P] since they can sense through their feelers.[P] The blind will still take effect but be reduced in impact.)[B][C]")
Append("[VOICE|Mei](If you get blinded, you may want to try a high-accuracy attack like Quick Strike if you are unable to remove the blinding effect.)[B][C]")
Append("[VOICE|Mei](You will be able to tell if an enemy is particularly vulnerable to a blind by checking the prediction box in combat.)[B][C]")
	
