-- |[Topic Script]|
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "SX399")

Append("SX-399:[E|Neutral] Okay, this is a bit of a personal question, boss.[P] You're a bit better at this than I am.[B][C]")
Append("Christine:[E|Smirk] Go ahead.[B][C]")
Append("SX-399:[E|Neutral] So uh, how do I...[P] initiate?[B][C]")
Append("Christine:[E|Smirk] With 55?[B][C]")
Append("SX-399:[E|Blush] Y-[P]yeah...[B][C]")
Append("SX-399:[E|Blush] I'm kind of new...[P] at this...[B][C]")
Append("SX-399:[E|Happy] I get so pumped up when I talk to her and then I try to make a move and -[P] phew![B][C]")
Append("SX-399:[E|Neutral] But I don't know if I'm doing it wrong because she gets upset when I try to hold her hand and she never kisses me.[B][C]")
Append("Christine:[E|Neutral] Oh, all right.[P] I see the problem.[P] You're actually doing things pretty much right.[B][C]")
Append("Christine:[E|Smirk] Hold her hand, talk to her.[P] That's a good starting point.[B][C]")
Append("Christine:[E|Smirk] Then, you look at her and state,[P] 'I would like to kiss you now.'[B][C]")
Append("SX-399:[E|Neutral] Not very hot, Christine.[B][C]")
Append("Christine:[E|Neutral] She literally doesn't know what to do, so it feels awkward for her.[P] After doing this a few times, she'll figure out what follows from what.[B][C]")
Append("Christine:[E|Neutral] Once you've got her kissing you without you needing to state it, I'm going to say get her to fondle your torsal chassis.[B][C]")
Append("SX-399:[E|Neutral] But does she want to?[B][C]")
Append("Christine:[E|Blush] She probably wants to touch you all over, but is too scared.[B][C]")
Append("SX-399:[E|Blush] Me too![B][C]")
Append("Christine:[E|Smirk] Work up to it, adding to your routine each time.[P] She wants to kiss you, but she wants to kiss you right.[P] Teach her how to kiss you right.[B][C]")
Append("Christine:[E|Smirk] Don't be in a hurry, you'll get to the good parts soon enough.[B][C]")
Append("SX-399:[E|Blush] Kissing is the good part![B][C]")
Append("Christine:[E|Blush] Oh it gets so much better...[B][C]")
