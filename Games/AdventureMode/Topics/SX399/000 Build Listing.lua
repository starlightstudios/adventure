-- |[Build Listing]|
--Builds a list of topics this NPC will respond to, and at what levels. Called at Adventure Mode startup.
local sNPCName = "SX399"
gsActiveNPCName = sNPCName

--Topic listing.
fnConstructTopicStd(false, "Biolabs_Attention",   "'Attention'")
fnConstructTopicStd(false, "Biolabs_Hydrophobia", "Hydrophobia")
fnConstructTopicStd(false, "Biolabs_Latex",       "Latex")
fnConstructTopicStd(false, "Biolabs_Mercenaries", "Mercenaries")
fnConstructTopicStd(true,  "Biolabs_Oxygen",      "Oxygen")
fnConstructTopicStd(false, "Biolabs_Past",        "Your Past")
fnConstructTopicStd(false, "Biolabs_Name",        "Name")
fnConstructTopicStd(false, "Biolabs_Poison",      "Poisons")
