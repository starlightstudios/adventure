-- |[Topic Script]|
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "SX399")

Append("SX-399:[E|Neutral] Hey Christine, question for you.[P] You know how organic matter works, right?[B][C]")
Append("Christine:[E|Neutral] You don't?[B][C]")
Append("SX-399:[E|Neutral] I haven't been organic for over a century now.[B][C]")
Append("Christine:[E|Neutral] I guess there's that...[B][C]")
Append("SX-399:[E|Smirk] So plants and grass and stuff -[P] they ignite, right?[P] I could set this whole place on fire?[P] There's enough oxygen for that?[B][C]")
Append("Christine:[E|Sad] Why are you smiling when you say that?[B][C]")
Append("SX-399:[E|Neutral] Sorry, it was just the idea...[B][C]")
Append("SX-399:[E|Happy] Torch the whole place and let the creepy-crawlies figure out how fire works![P] That'll show those dunderheads![B][C]")
Append("Christine:[E|Offended] It'll also show all the innocent organic wildlife.[B][C]")
Append("Christine:[E|Neutral] Besides, I'm pretty sure the biolabs have automatic fire suppression systems.[P] Whether or not they'll work with all the staff evacuated and the power systems broken, I don't know.[B][C]")
Append("SX-399:[E|Neutral] I got carried away.[P] I'll watch my background and make sure not to toast any innocent critters.[B][C]")
Append("SX-399:[E|Smirk] On the topic -[P] look at the birds and the squirrels![P] Aren't they so keen?[B][C]")
Append("Christine:[E|Smirk] It's the one thing I miss working in sector 96.[P] Cute animals are the best.[B][C]")
Append("Christine:[E|Blush] Maybe we'll find a way to mechanize cute animals someday?[B][C]")
Append("SX-399:[E|Neutral] I think their not being robots is part of the appeal.[B][C]")
Append("Christine:[E|Smirk] True enough.[B][C]")
