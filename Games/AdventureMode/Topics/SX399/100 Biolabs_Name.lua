-- |[Topic Script]|
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "SX399")

Append("Christine:[E|Neutral] Hey, SX.[P] What's your name?[B][C]")
Append("SX-399:[E|Neutral] SX-399.[P] You knew that so I assume you have a real question to ask.[B][C]")
Append("Christine:[E|Smirk] Well I - [B][C]")
Append("SX-399:[E|Neutral] Saro Xalthier.[P] Saro means 'hero' in...[P] whatever the old dialect was called.[P] I guess it's called Regulan?[B][C]")
Append("SX-399:[E|Neutral] I don't know, apparently it's not a language spoken on Pandemonium.[P] Doesn't matter.[B][C]")
Append("Christine:[E|Neutral] So why do you go by SX-399?[B][C]")
Append("SX-399:[E|Smirk] Habit, I suppose.[P] You get called by your designation a hundred times a day but your 'real name' maybe once, and only if mom is really mad.[B][C]")
Append("SX-399:[E|Neutral] You have to type it into keypads and punch cards and stuff all the time to be let into areas.[P] That's how it was back when I worked as a cleaner.[P] Now all the doors are automated.[B][C]")
Append("SX-399:[E|Neutral] What about you?[P] Why'd you go with Christine?[P] Do golems pick their secondary designations?[P] You don't pick your unit number, right?[B][C]")
Append("Christine:[E|Smirk] Christine just sort of fit me, I like it.[P] We get to pick our secondary designations but apparently a lot of units don't know where they got theirs from.[B][C]")
Append("Christine:[E|Sad] Sophie told me once she didn't really remember who she was, but Sophie just leapt out at her, so she went with that.[B][C]")
Append("SX-399:[E|Neutral] And Unit 771852?[B][C]")
Append("Christine:[E|Neutral] Unit designations are apparently assigned to golem cores, so that was the designation of the core I had put in me.[P] I have a few spare cores, up to Unit 771858.[B][C]")
Append("SX-399:[E|Blush] What would you need those for?[B][C]")
Append("Christine:[E|Serious] Assimilation.[B][C]")
Append("Christine:[E|Neutral] But seriously, if a human is exposed to vacuum or radiation, transformation may save their life.[P] I always keep some cores on me just in case.[B][C]")
Append("SX-399:[E|Smirk] Ever considered changing your name?[P] You could be anything you want.[B][C]")
Append("Christine:[E|Smirk] I have thought of it, but...[P] there's no real reason.[P] Christine is fine.[P] I like who I am, I like being Christine.[B][C]")
