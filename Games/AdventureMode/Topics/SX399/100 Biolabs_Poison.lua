-- |[Topic Script]|
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "SX399")

Append("SX-399:[E|Neutral] Christine, why are they storing this dangerous waste in the biolabs where it's clearly killing things?[B][C]")
Append("SX-399:[E|Neutral] It's not like there's a shortage of places on Regulus you could put this stuff and have it not get into the soil.[B][C]")
Append("Christine:[E|Sad] Oh.[P] That's just them lying.[B][C]")
Append("Christine:[E|Neutral] Political lesson time -[P] people who want to do awful things always lie about their reasons because the truth would spark a riot.[B][C]")
Append("Christine:[E|Neutral] So if you want to poison the soil to test out killing plants and people, you act like you're doing it for a bureaucratic error.[B][C]")
Append("Christine:[E|Offended] Then everyone fights it, so you keep lying and tell them to follow the proper channels.[P] The end result is everyone gets poisoned and nothing gets done.[B][C]")
Append("Christine:[E|Offended] The barrels of dioxin being there were the goal, not an accident.[P] They just want everyone to stop complaining about it.[B][C]")
Append("SX-399:[E|Neutral] The administrative types aren't usually the sort that cares what people think.[B][C]")
Append("Christine:[E|Neutral] The biolabs are a little different.[P] We've tried to form alliances with the people here, they have a lot more independence because of the reduced reach of the surveillance.[B][C]")
Append("Christine:[E|Neutral] Microphones and cameras in the biolabs don't work well because organic species keep on growing on them and gunking them up, and they require constant maintenance.[B][C]")
Append("Christine:[E|Neutral] We have some sympathizers here, but most of them are surprised about the awful condition of Regulus City.[B][C]")
Append("SX-399:[E|Neutral] So they protest the actions of the administrators?[B][C]")
Append("Christine:[E|Smirk] Yep, and that really sets them off.[B][C]")
Append("Christine:[E|Offended] I wouldn't put it past them that these poisons are a disciplinary tool.[P] Maybe they're sending a message about Biological Services getting too uppity.[B][C]")
Append("Christine:[E|Offended] That, or 2856 just wants to kill people on Pandemonium and destroy the forests there for her own reasons.[P] Hell, maybe it's to save budget.[P] I'm sure whatever awful reason she has will come to light when we publish all her research records after we win our freedom.[B][C]")
Append("SX-399:[E|Neutral] If anything, mother downplayed how evil they are...[B][C]")
