-- |[Topic Script]|
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "SX399")

Append("SX-399:[E|Neutral] Tactical question, Christine.[P] Would you happen to know the melting point of latex?[B][C]")
Append("Christine:[E|Neutral] 180C, I believe.[P] Though keep in mind the drone units aren't actually latex, they're ceramoweave with a rubbery coating.[B][C]")
Append("SX-399:[E|Neutral] Okay, so melting point of that?[B][C]")
Append("Christine:[E|Neutral] I think it's around 1200C, though its structural integrity drops precipitously at 800C.[P] Why?[B][C]")
Append("SX-399:[E|Smirk] No reason.[B][C]")
Append("Christine:[E|Offended] I didn't think you'd be so casual about the violence inherent in melting a drone unit, SX-399.[B][C]")
Append("SX-399:[E|Blush] Wh-[P]what?[P] You've got me all wrong, Christine![B][C]")
Append("SX-399:[E|Smirk] I don't want to melt *myself*.[P] I want to make sure I'd be able to withstand the latent heat from my weapons.[B][C]")
Append("Christine:[E|Neutral] Excuse me?[B][C]")
Append("SX-399:[E|Smirk] I've been looking into part interoperability due to my upgrade.[P] Obviously making more steam lords exactly like me will be a huge help to the war effort.[B][C]")
Append("SX-399:[E|Neutral] Unfortunately it seems like I'm going to be the only one exactly like me.[P] Some of the stuff Sophie did isn't going to scale up.[B][C]")
Append("SX-399:[E|Smirk] But then I found out I'm compatible with drone unit parts, and I could actually become one someday if we found a way to rework my power core![B][C]")
Append("SX-399:[E|Happy] I could be a bubbly steam latex drone![B][C]")
Append("Christine:[E|Neutral] I am understanding the words you're saying but I'm still confused.[B][C]")
Append("SX-399:[E|Flirt] 55 said it'd be a great idea.[P] I asked her why and she started talking about tactics and stuff.[P] That's how you know it's personal with her.[B][C]")
Append("Christine:[E|Smirk] Being rubbery is pretty great, I admit.[P] It's nice to be encased and...[B][C]")
Append("Christine:[E|Blush] Simplified...[B][C]")
Append("SX-399:[E|Smirk] Speaking of, if you have any advice on getting her...[P] attention...[B][C]")

WD_SetProperty("Unlock Topic", "Biolabs_Attention", 1)
