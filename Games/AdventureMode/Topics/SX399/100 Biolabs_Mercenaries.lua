-- |[Topic Script]|
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "SX399")

Append("SX-399:[E|Neutral] Hey, out of curiosity, how do mercenaries work?[B][C]")
Append("Christine:[E|Neutral] Mercenaries are soldiers for hire.[P] They fight wars when paid to, or do other things soldiers do.[P] Why?[B][C]")
Append("SX-399:[E|Neutral] I heard some of the steam droid groups mother is talking to were 'mercenaries' but I didn't ask what she meant by that.[B][C]")
Append("Christine:[E|Neutral] I guess some of the steam droids want to be paid to fight for their freedom.[B][C]")
Append("Christine:[E|Offended] Time to be frank.[P] Mercenaries are scum.[P] They kill people for money.[B][C]")
Append("Christine:[E|Offended] On Regulus, things are terrible and everyone except a select few are suffering.[P] To be able to fight to end that, and demand payment for it, is unconscionable.[B][C]")
Append("SX-399:[E|Angry] But why?[P] How can they act like everything is fine in the mines?[B][C]")
Append("Christine:[E|Neutral] Oh, that's easy.[P] They know we're desperate, and they know we'll pay.[P] And I've spoken to your mother -[P] we'll pay.[B][C]")
Append("SX-399:[E|Angry] You'd pay them?[B][C]")
Append("Christine:[E|Neutral] The steam droids you're talking about are some of the best equipped soldiers we'll have.[P] They bring their own weapons and have decades of experience in the mines.[B][C]")
Append("Christine:[E|Offended] The motivation for freedom does a lot, but having a gun and knowing how to duck does a lot more.[B][C]")
Append("SX-399:[E|Neutral] ...[P] My own people would do that...[B][C]")
Append("Christine:[E|Neutral] If we can capture the teleportarium complex, we could even try hiring mercenaries from Pandemonium.[P] Not sure how well they'd do, but we could do it.[B][C]")
Append("Christine:[E|Smirk] 55 said the people of Pandemonium have just figured out repeater rifles, and firearms are expensive.[P] We could probably buy a mercenary company for three pulse rifles and a wink.[B][C]")
Append("SX-399:[E|Neutral] Can they survive in a vacuum?[P] Won't they get gunned down by the Administration's forces?[B][C]")
Append("Christine:[E|Smirk] The one advantage of mercenaries is that you don't have to care if they die.[P] They're here for money.[P] Screw 'em.[B][C]")
Append("Christine:[E|Smirk] Besides, wouldn't it be so fun to have demons or spidergirls here on Regulus?[B][C]")
Append("SX-399:[E|Smirk] Are demons actually real?[B][C]")
Append("Christine:[E|Happy] I hope so![P] Then I can turn into one![B][C]")
