-- |[Topic Script]|
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Unlock Topic", "Biolabs_Name", 1)
WD_SetProperty("Activate Topics After Dialogue", "SX399")

Append("Christine:[E|Neutral] SX-399, can you tell me a bit more about your past?[P] Before we met?[B][C]")
Append("SX-399:[E|Neutral] You mean the really awful parts?[B][C]")
Append("Christine:[E|Sad] I'm sorry if it's painful for you...[B][C]")
Append("SX-399:[E|Smirk] It's behind me now, so it's all right.[B][C]")
Append("SX-399:[E|Smirk] [EMOTION|Christine|Neutral]I don't remember too much about my early childhood, you'd have to ask mother about that.[P] I think my earliest memory is of me getting lost in the habitation block and trying to find my father.[B][C]")
Append("SX-399:[E|Smirk] Apparently I just held on to some other person's pant leg and wouldn't let go.[P] Turns out the fellow had the same pants that my father had worn and I hadn't figured out how to look up yet.[B][C]")
Append("Christine:[E|Laugh] Ha ha![P] Too true![B][C]")
Append("SX-399:[E|Neutral] Back then, we lived in big buildings with special magical insulation on them.[P] It was really cramped and people were always coughing and getting sick, it was pretty awful.[B][C]")
Append("SX-399:[E|Neutral] Mother worked as a housekeeper for all the people who worked at the university, which was the big thing.[P] If you didn't work there, you probably worked for someone who did.[B][C]")
Append("SX-399:[E|Neutral] Not sure what father did.[P] I never asked, but I assume it was something at the university.[B][C]")
Append("Christine:[E|Neutral] Would JX-101 know?[B][C]")
Append("SX-399:[E|Smirk] Maybe I should ask her one of these days.[B][C]")
Append("SX-399:[E|Smirk] When I was fourteen, they started rolling out this partirhuman thing.[P] We called them steam droids, the name stuck.[P] There's a technical name for it but I don't know what it is.[B][C]")
Append("SX-399:[E|Neutral] Mother was actually selected for the pilot program, I got in two years later.[P] So the body I had was me at sixteen, a robot.[P] I was one of the younger droids, actually, but you sort of get locked into your appearance.[B][C]")
Append("SX-399:[E|Smirk] Actually my hips got a bit wider over the years.[P] Maybe I did have some growing still to do?[B][C]")
Append("Christine:[E|Smirk] Or your parts just degraded...[B][C]")
Append("SX-399:[E|Smirk] If we were rich we could have just bought new parts, actually.[B][C]")
Append("SX-399:[E|Neutral] But we weren't rich.[B][C]")
Append("Christine:[E|Neutral] What happened to your father?[B][C]")
Append("SX-399:[E|Neutral] He stayed human right up to the end.[P] I got a job clearing blockages from the heating system and spent days at a time not seeing him or mother because our schedules didn't overlap.[B][C]")
Append("SX-399:[E|Neutral] I thought it was part of growing up.[P] I had never even seen Pandemonium, I had no idea how people live there.[P] This was just how it was on Regulus.[B][C]")
Append("SX-399:[E|Neutral] One day, he said he had to go somewhere.[P] Mother said he'd be gone a long time.[P] I went to work, never saw him after that.[B][C]")
Append("Christine:[E|Sad] Oh...[B][C]")
Append("SX-399:[E|Neutral] Mother told me later they had decided to remove all the humans who refused to be upgraded.[P] They were going to be rolling out this new 'golem' thing and the humans were problems to be removed.[B][C]")
Append("SX-399:[E|Neutral] He was getting old and couldn't keep up with a steam droid on his best days anyway.[P] They said they were sending him and all the other holdouts to Pandemonium.[B][C]")
Append("SX-399:[E|Neutral] They probably had him shot.[B][C]")
Append("Christine:[E|Sad] I don't doubt it.[B][C]")
Append("SX-399:[E|Neutral] I had started to have problems with my power system.[P] Then they told us they weren't going to be paying for repairs anymore, and we couldn't be upgraded.[P] There were riots over that, a lot of the other droids got retired.[P] Big revolt, lots of fighting.[B][C]")
Append("SX-399:[E|Neutral] The new prototype golems crushed it.[P] Mother took me into the mines where a lot of the rebels were hiding out.[B][C]")
Append("SX-399:[E|Neutral] My power system had been faulty and at that point, replacing it was something only a rich person would be able to do.[P] As the years went on, it became just more and more impossible.[B][C]")
Append("SX-399:[E|Neutral] I'm a lot older than you, Christine, but I haven't lived as long.[B][C]")
Append("SX-399:[E|Smirk] Well now, I intend to make up for lost time.[B][C]")
Append("Christine:[E|Sad] Sorry I brought it up.[B][C]")
Append("SX-399:[E|Smirk] Don't be, it's actually really encouraging![B][C]")
Append("Christine:[E|Neutral] How so?[B][C]")
Append("SX-399:[E|Smirk] We've got so many problems in this world, but they're actually all the same problem.[P] It's been that way forever, you fix that one thing and everything else falls into place.[B][C]")
Append("SX-399:[E|Smirk] All we have to do is make sure nobody is 'better' than someone else, whether it's wealth or power or smarts.[P] You can be smarter, but not better.[P] You don't deserve to be able to hurt others just to stay comfortable.[B][C]")
Append("Christine:[E|Smirk] That's what I think, too, SX.[P] We'll get through this.[B][C]")



