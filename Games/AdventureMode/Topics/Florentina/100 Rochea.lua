-- |[Rochea]|
--Dialogue script. The player must have met Rochea for it to even be available, so Mei implicitly has Alraune form.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local iKnowsMeiHasAlraune       = VM_GetVar("Root/Variables/Chapter1/Florentina/iKnowsMeiHasAlraune", "N")
local iFlorentinaKnowsAboutRune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")
local iFlorentinaSpecialAlraune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaSpecialAlraune", "N")

--If Florentina doesn't know Mei is an Alraune, which is possible if Mei becomes an Alraune, switches back, then meets Florentina.
if(iKnowsMeiHasAlraune == 0.0) then
	
	--Dialogue.
	fnCutscene([[ Append("Mei:[E|Neutral] Do you know who Rochea is?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] How do you know that name?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Well, you see...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I'm an Alraune too.[B][C]") ]])
	
	--Set variable.
	VM_SetVar("Root/Variables/Chapter1/Florentina/iKnowsMeiHasAlraune", "N", 1.0)
	
	--If Florentina knows about the runestone...
	if(iFlorentinaKnowsAboutRune == 1.0) then

		--Flag to go back to the topics listing when this is over.
		WD_SetProperty("Activate Topics After Dialogue", "Florentina")
		
		--Dialogue.
		fnCutscene([[ Append("Florentina:[E|Confused] You are?[P] No, let me guess.[P] The runestone?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] Yep.[P] I don't think you've seen me that way, yet.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Feh.[P] I'm not in a position to doubt any claim you make, am I?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Were you perchance royalty back on Earth?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I'm a waitress, actually.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] But if you're an Alraune, and you just arrived in Pandemonium...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] ...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Rochea joined you, didn't she.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] That was her.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] ... she's beautiful, isn't she?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] And so gentle...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] I'm not on good terms with her.[P] I said some things I shouldn't have.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] We probably wouldn't be on good terms anyway, but I don't think she deserved it.[P] Sometimes you're just angry at the whole world.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Would an apology help?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] I'll think about it, Mei.[P] I'll think about it.[B][C]") ]])
		fnCutsceneBlocker()
	
	--If not, a demonstration is in order.
	else
	
		--Flags.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaSpecialAlraune", "N", 1.0)
	
		--Dialogue.
		fnCutscene([[ Append("Florentina:[E|Neutral] You are?[P] Because you don't look like one.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Surprise] Huh?[P] Really?[BLOCK] Freakin' really?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] The little ones are backing you up here.[P] What gives?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I could show you...") ]])
		fnCutsceneBlocker()
		
		--This script will take over once the dialogue closes.
		LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/100 Transform/Transform_MeiToAlraune/Scene_Begin.lua")
	end

--Otherwise, she knows.
else
	--Flag to go back to the topics listing when this is over.
	WD_SetProperty("Activate Topics After Dialogue", "Florentina")
	
	--Normal dialogue:
	if(iFlorentinaSpecialAlraune == 0.0) then
		Append("Florentina:[E|Neutral] Mei, when you were joined...[B][C]")
		Append("Mei:[E|Neutral] Yes?[B][C]")
		Append("Florentina:[E|Confused] Was it Rochea?[B][C]")
		Append("Mei:[E|Blush] That was her.[P] She's the one who joined me.[P] She's so beautiful, and gentle.[B][C]")
		Append("Mei:[E|Neutral] Did she join you too?[P][CLEAR]")
		Append("Florentina:[E|Neutral] No, I was joined a long way from here.[B][C]")
		Append("Florentina:[E|Confused] I said some things to her last time we crossed paths.[P] Not all of them good.[B][C]")
		Append("Mei:[E|Happy] No surprise there.[B][C]")
		Append("Florentina:[E|Offended] Funny.[B][C]")
		Append("Florentina:[E|Neutral] Seeing you...[P] I was thinking maybe I should go and apologize to her.[B][C]")
		Append("Mei:[E|Neutral] I'm a little shocked to hear this coming from you.[B][C]")
		Append("Florentina:[E|Neutral] People aren't of a piece, Mei.[P] I don't like her, but I don't think she deserved what I did either.[B][C]")
		Append("Mei:[E|Neutral] She's still there.[P] You could go see her.[B][C]")
		Append("Florentina:[E|Neutral] If we go that way...[P] I don't know.[P] I'll figure it out.[B][C]")
	
	--Special dialogue:
	else
		Append("Florentina:[E|Happy] Hey, Mei?[P] Thanks.[B][C]")
		Append("Mei:[E|Neutral] For what?[B][C]")
		Append("Florentina:[E|Confused] It's been a long time since I really thought about life outside of business.[B][C]")
		Append("Florentina:[E|Confused] I try to keep myself busy, and people at the trading post are always coming and going.[B][C]")
		Append("Florentina:[E|Confused] But, I should probably at least try to make nice with Rochea and her covenant.[B][C]")
		Append("Florentina:[E|Neutral] Maybe not right away.[P] I need to think of what I'm going to say.[P] But I should say it, you know?[B][C]")
		Append("Mei:[E|Neutral] I understand you.[B][C]")
		Append("Florentina:[E|Happy] Of course, if you tell anyone I've been acting all sappy, I will deny it up and down.[P] You hear me?[B][C]")
		Append("Mei:[E|Laugh] Oh, of course.[B][C]")
	
	end
end
