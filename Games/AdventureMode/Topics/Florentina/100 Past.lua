-- |[Florentina's Past]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Topic.
local sTopic = LM_GetScriptArgument(0)

--Opener:
if(sTopic == "Hello") then

	--Dialogue.
	fnCutscene([[ Append("Mei:[E|Neutral] So, would you say you know me well enough to talk about your past, yet?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] It's a really long story.[P] Do you really want to hear it?[BLOCK]") ]])

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Hear it\", " .. sDecisionScript .. ", \"HearIt\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Maybe not\",  " .. sDecisionScript .. ", \"Nope\") ")
	fnCutsceneBlocker()

--Hear the story:
elseif(sTopic == "HearIt") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
    
    --Flag.
    VM_SetVar("Root/Variables/Global/Florentina/iMeiKnowsPast", "N", 1.0)
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])

	fnCutscene([[ Append("Mei:[E|Neutral] I really do, Florentina.[P] Please.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] Are you ever going to just give up?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Oh wait, no.[P] No you're not.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] Look, if you really don't want to talk about it, I'll leave it.[P] But you don't look angry whenever I ask.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] It means dredging up old stuff that I'm supposed to have moved on from.[P] I thought I had, but then you showed up.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] I guess I didn't really move on from it...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] You can trust me.[P] I think I've demonstrated that.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Smirk] And if not -[P] I do intend to go back to Earth.[P] No way to keep your secret safer![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] It's not really a secret.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] ...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] Okay, I may as well tell you.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] I'm from a city-state far to the southeast of here.[P] It's called Sturnheim, if that means anything to you.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Nope...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] Heh.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] I was going to school to be a lawyer.[P] Down there, it's a six-year study, but you're pretty much guaranteed to get a good job.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] I don't like taking orders from anyone, and lawyers are generally in charge of legal offices.[P] It's a lot more tiered than in Trannadar.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] My -[P] my love...[P][CLEAR]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Florentina?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] My fiance and I -[P] oh how I miss her...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Ahem.[P] Sorry.[P] I -[P] get a little tear-eyed about her.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Sad] Oh...[P] did she pass away?[P] I'm sorry...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] No...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] We were going to move into a new house I had commissioned with my first year's salary.[P] She had always wanted a big house with her own garden.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] She loved growing vegetables in the public gardens and selling them.[P] That's how she helped put me through school, you see.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] We had a big house, good jobs, lots of money, respect...") ]])
	fnCutsceneBlocker()
	
	--Disable music.
	fnCutscene([[ AudioManager_PlayMusic("Null") ]])
	fnCutscene([[ WD_SetProperty("FastShow") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Sad") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
	fnCutscene([[ Append("Florentina:[E|Facepalm] ...[P] I did everything right, damn it![P] I worked hard![P] I made sacrifices for my beloved Cathelina![P] She sacrificed for me![P] Isn't that what love is?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Facepalm] It was all going so well...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Facepalm] ...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I'm sorry to bring this up...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] I got sick, Mei.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] I got sick and nobody knew what the cause was.[P] One night I just woke up and had spit blood all over the bedsheets.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] I would feel weak one minute and then strong as a horse the next.[P] Then, I'd get a fever that would last a day and be gone by midnight.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] We went to every doctor we could find.[P] We got a different diagnosis from each one, and none of their treatments worked.[P] Quacks, all of them![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] I had done everything right, and this is what I got for it?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Facepalm] Cathelina -[P] she was so strong.[P] She kept finding new doctors and wizards who could help.[P] She never broke her calm facade.[P] She just kept going like this was a problem and we could solve it.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Facepalm] But I was going to die, Mei.[P] I was going to die and leave her all alone in that big house.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Sad] ...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] There was one treatment I didn't rule out.[P] I didn't tell my beloved about it, of course.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Sad] The Alraunes...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] She had cried herself to sleep, so I let myself out one night when I was feeling strong.[P] I went into the forest and I called to them.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] They took me to their joining pool.[P] At first I walked, but they had to carry me after I had a spell of weakness.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] They put me in, and...[P] here I am.[P] Cured.[P] Healthy.[P] A plant.[P] Forever.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] I didn't give a crap about the cleansing fungus, so I told them to stuff it and went to see Cathelina.[P] She met me at the door.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] She -[P] spit on me.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Surprise] ..![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] She said I was a grotesque monster, a mockery of the one she loved.[P] She said I should have died rather than become this.[P] She called me a coward.[P] She said I was worthless.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] And, I had spent our money searching for a cure.[P] She was poor.[P] All we had was the house, and she would have to sell it to cover my debts.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] So I left.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Sad] Oh, Florentina...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] Well, now you know.[P] Now you know why I am what I am.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Sad] I'm really sorry...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] It's ancient history, Mei.[P] It's been forty years since then.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] But you still have debt collectors coming after you?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] Heh.[P] More bounty hunters than anyone else.[P] I assume Cathelina sold the house and put the money into a bounty on my head.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Maybe she convinced herself that I killed her beloved and tried to take his place?[P] I don't know what could be going through her head.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] I don't blame her.[P] I did this to her.[P] Maybe it really would have been better if I had had the courage to just die quickly.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] What do you think?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I'm not going to judge you.[P] Either of you.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Fair enough.") ]])
	fnCutsceneBlocker()
	
	--Enable music.
	fnCutscene([[ AudioManager_PlayMusic("FlorentinasTheme") ]])
	fnCutscene([[ WD_SetProperty("FastShow") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Florentina") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] It actually feels nice to talk about it with someone who understands.[P] I feel a little better about it now.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] Have you ever considered going back?[P] Finding her?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] ...[P] Every day.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] But she's an old woman by now, Mei.[P] She's probably remarried, had kids, grandkids...[P] moved on with her life.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] If I really love her, I'm not going to re-insert myself into that.[P] I'll let her be happy.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Well.[P] I guess I get it now.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Nothing to do but keep going.[P] Until we don't anymore.[B][C]") ]])

--Hear the story:
elseif(sTopic == "Nope") then

	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Florentina") ]])
	
	fnCutscene([[ Append("Mei:[E|Neutral] If it's really long, it might be best to do it later.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Fine with me...[B][C]") ]])

end
