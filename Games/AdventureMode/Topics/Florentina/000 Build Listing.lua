-- |[Build Listing]|
--Builds a list of topics this NPC will respond to, and at what levels. Called at Adventure Mode startup.
local sNPCName = "Florentina"

--Topic listing.
fnConstructTopic("NextMove",          "Our Next Move",     1, sNPCName, 0)
fnConstructTopic("Alraunes",          "Alraunes",         -1, sNPCName, 0)
fnConstructTopic("Mushraunes",        "Mushraunes",       -1, sNPCName, 0)
fnConstructTopic("Gravemarkers",      "Gravemarkers",     -1, sNPCName, 0)
fnConstructTopic("Bees",              "Bees",             -1, sNPCName, 0)
fnConstructTopic("Breanne",           "Breanne",          -1, sNPCName, 0)
fnConstructTopic("Cell Phone",        "Cell Phone",       -1, sNPCName, 0)
fnConstructTopic("Claudia",           "Claudia",          -1, sNPCName, 0)
fnConstructTopic("Countess Quantir",  "Countess Quantir", -1, sNPCName, 0)
fnConstructTopic("Eyepatch",          "Eyepatch",         -1, sNPCName, 0)
fnConstructTopic("Ghosts",            "Ghosts",           -1, sNPCName, 0)
fnConstructTopic("Job",               "Job",              -1, sNPCName, 0)
fnConstructTopic("Nadia",             "Nadia",            -1, sNPCName, 0)
fnConstructTopic("Name",              "Name",             -1, sNPCName, 0)
fnConstructTopic("OutlandFarm",       "Outland Farm",     -1, sNPCName, 0)
fnConstructTopic("Pandemonium",       "Pandemonium",      -1, sNPCName, 0)
fnConstructTopic("Past",              "Past",             -1, sNPCName, 0)
fnConstructTopic("Quantir",           "Quantir",          -1, sNPCName, 0)
fnConstructTopic("Rilmani",           "Rilmani",          -1, sNPCName, 0)
fnConstructTopic("Rochea",            "Rochea",           -1, sNPCName, 0)
fnConstructTopic("Slimes",            "Slimes",           -1, sNPCName, 0)
fnConstructTopic("Trading Post",      "Trading Post",     -1, sNPCName, 0)
fnConstructTopic("Warden",            "Warden",           -1, sNPCName, 0)
fnConstructTopic("Werecats",          "Werecats",         -1, sNPCName, 0)
fnConstructTopic("Rubberines",        "Rubberines",       -1, sNPCName, 0)
fnConstructTopic("Victoria",          "Victoria",         -1, sNPCName, 0)

--Special:
fnConstructTopic("Pepper Pie",      "Pepper Pie",     -1, sNPCName, 0)
