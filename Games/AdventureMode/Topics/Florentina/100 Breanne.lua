-- |[Claudia]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Variables.
local iMetMeiWithFlorentina = VM_GetVar("Root/Variables/Chapter1/Breanne/iMetMeiWithFlorentina", "N")

--If Mei has seen Florentina and Breanne meet.
if(iMetMeiWithFlorentina == 1.0) then
	Append("Mei:[E|Neutral] Do you and Breanne get along well?[B][C]")
	Append("Florentina:[E|Confused] You saw it.[P] Was it not clear?[B][C]")
	Append("Mei:[E|Happy] Brusque.[B][C]")
	Append("Florentina:[E|Confused] Then why are you asking?[B][C]")
	Append("Mei:[E|Neutral] One does not study a lake by merely staring at its surface.[B][C]")
	Append("Florentina:[E|Confused]...[P] Come again?[B][C]")
	Append("Mei:[E|Neutral] I'm just trying to learn about a person who I have to fight alongside.[B][C]")
	Append("Florentina:[E|Happy] I had you pegged for a sucker, Mei.[P] Keep it up and you might yet prove me wrong.[B][C]")

--If Mei has not met Breanne with Florentina present.
else
	Append("Mei:[E|Neutral] Do you and Breanne get along well?[B][C]")
	Append("Florentina:[E|Confused] I've had dealings with her in the past. She worries too much about mannerisms and not enough about results.[B][C]")
	Append("Mei:[E|Neutral] The two aren't incompatible...[B][C]")
	Append("Florentina:[E|Confused] She seems to think we're friends, if that's what you wanted to know.[B][C]")
	Append("Mei:[E|Neutral] Are you?[B][C]")
	Append("Florentina:[E|Neutral] No.[B][C]")
	Append("Mei:[E|Happy] Naturally.[B][C]")
end
