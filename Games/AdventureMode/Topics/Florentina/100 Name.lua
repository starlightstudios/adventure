-- |[Name]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Variables.
local sMeiForm        = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local iHasAlrauneForm = VM_GetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N")
local iMeiKnowsPast   = VM_GetVar("Root/Variables/Global/Florentina/iMeiKnowsPast", "N")

--Standard. Mei hasn't seen the Alraune scene and doesn't know they change their names.
if(iMeiKnowsPast == 1.0) then
	Append("Mei:[E|Neutral] Um, is it prying if I ask about your beloved?[B][C]")
	Append("Florentina:[E|Neutral] No.[P] Talk about water under the bridge, Mei.[B][C]")
	Append("Mei:[E|Smirk] Were you as brusque to her as you were to me?[B][C]")
	Append("Florentina:[E|Happy] Oh, you better believe it.[P] I think that's why she liked me so much.[B][C]")
	Append("Florentina:[E|Happy] When you first hear someone constantly insult you and take cheap shots, you probably think, 'Oh what a sorhole'.[B][C]")
	Append("Florentina:[E|Happy] But when you hear them make up a special insult just for you...[B][C]")
	Append("Florentina:[E|Blush] Or when you see them suckerpunch someone for leering at you...[B][C]")
	Append("Mei:[E|Angry] Florentina, you're not supposed to hit people![B][C]")
	Append("Florentina:[E|Neutral] And just what do we do on an hourly basis?[B][C]")
	Append("Mei:[E|Offended] You're not supposed to hit people who aren't trying to kill or rob you![B][C]")
	Append("Florentina:[E|Neutral] Now try being a young hothead and have someone insult the light of your life.[B][C]")
	Append("Florentina:[E|Neutral] Your blood practically boils...[B][C]")
	Append("Florentina:[E|Happy] Sheesh, I think I've mellowed out over the years.[B][C]")
	Append("Mei:[E|Offended] (I can't believe I'm talking to the *calm, reasonable* version of Florentina...)[B][C]")
    
--Standard. Mei hasn't seen the Alraune scene and doesn't know they change their names.
elseif(iHasAlrauneForm == 0.0) then
	Append("Mei:[E|Neutral] Could you tell me a little more about yourself?[B][C]")
	Append("Florentina:[E|Neutral] No.[P] I don't like to waste time, and you don't need to know anything.[B][C]")
	Append("Mei:[E|Happy] This is a rest stop.[P] Relax a little.[B][C]")
	Append("Florentina:[E|Offended] Tch.[P][E|Confused] What do you want to know?[B][C]")
	Append("Mei:[E|Neutral] I don't know...[P] Where is the name Florentina from?[B][C]")
	Append("Florentina:[E|Happy] Me.[P] I came up with it.[P] Sounded flowery.[B][C]")
	Append("Mei:[E|Neutral] Well that's nice...[B][C]")

--Mei has Alraune form, so she knows they change their names.
else
	Append("Mei:[E|Neutral] Could you tell me a little more about yourself?[B][C]")
	Append("Florentina:[E|Confused] No.[P] I don't like to waste time, and you don't need to know anything.[B][C]")
	Append("Mei:[E|Happy] This is a rest stop.[P] Relax a little.[B][C]")
	Append("Florentina:[E|Offended] Tch.[P][E|Confused] What do you want to know?[B][C]")
	Append("Mei:[E|Neutral] Was Florentina your name before you became an Alraune?[B][C]")
	Append("Florentina:[E|Confused] No.[B][C]")
	Append("Mei:[E|Neutral] Ah.[P] I see.[P] I was under the impression you didn't breathe the cleansing fungus.[B][C]")
	Append("Florentina:[E|Confused] I didn't.[P] I'm not the nature-loving type.[B][C]")
	Append("Mei:[E|Neutral] Then why change your name?[B][C]")
	Append("Florentina:[E|Confused] My business, not yours.[B][C]")
	Append("Mei:[E|Sad] Okay then...[B][C]")
end
