-- |[Outland Farm]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Standard.
Append("Mei:[E|Neutral] What can you tell me about Outland Farm?[B][C]")
Append("Florentina:[E|Neutral] It's one of the small farms on the plains north of the trading post, and the closest one to the main road.[B][C]")
Append("Florentina:[E|Happy] The farmers sell me their goods, and I sell them equipment.[B][C]")
Append("Mei:[E|Neutral] Do you visit them often?[B][C]")
Append("Florentina:[E|Happy] I have people for that, Mei.[B][C]")
Append("Mei:[E|Happy] Just asking.[B][C]")
