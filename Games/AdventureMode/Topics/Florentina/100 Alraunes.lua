-- |[Bees]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sMeiForm        = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local iHasAlrauneForm = VM_GetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N")
VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaToldFungus", "N", 1.0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Mei is in Alraune form, or has access to it.
if(sMeiForm == "Alraune" or iHasAlrauneForm == 1.0) then
	Append("Mei:[E|Neutral] Florentina, I don't mean to offend, but...[P][CLEAR]")
	Append("Florentina:[E|Confused] A sure indication you're about to offend me.[B][C]")
	Append("Mei:[E|Neutral] You don't act like the other Alraunes.[P] Why?[B][C]")
	Append("Florentina:[E|Neutral] It's complicated.[B][C]")
	Append("Mei:[E|Sad] But you hear the little ones whisper.[P] Don't you care about them?[B][C]")
	Append("Florentina:[E|Neutral] As much as I care about anyone else, really.[B][C]")
	Append("Florentina:[E|Confused] The other Alraunes?[P] When they wiped their memories, I guess all that was left was peace and love.[P] That's called naivete.[B][C]")
	Append("Mei:[E|Offended] Yeah, I don't think you'd change at all if you partook of the fungus.[B][C]")
	Append("Florentina:[E|Neutral] Feh, who knows?[P] I'm not keen to find out.[P] Neither are you, it seems.[B][C]")
	Append("Mei:[E|Blush] I haven't ruled it out.[B][C]")
	Append("Florentina:[E|Confused] I thought you wanted to go back to Earth?[B][C]")
	Append("Mei:[E|Neutral] I do, but...[P] I don't know if I would actually stay.[P] I would want to tell my family not to worry about me...[B][C]")
	Append("Florentina:[E|Neutral] How sweet.[P] So the job at hand hasn't changed.[B][C]")

--Normal case.
else
	Append("Mei:[E|Neutral] You seem a lot different than the other Alraunes.[B][C]")
	Append("Florentina:[E|Offended] The so-called \"wild\" ones?[P] Yeah.[P] What tipped you off?[B][C]")
	Append("Mei:[E|Offended] No need to be sarcastic.[B][C]")
	Append("Florentina:[E|Confused] Mei, when Alraunes join a new leaf-sister, they make them soak up this stuff they call the \"Cleansing Fungus\".[B][C]")
	Append("Florentina:[E|Confused] It makes you forget everything.[P] I mean, everything.[P] Your name, your home, your family, all of it -[P] gone.[B][C]")
	Append("Mei:[E|Neutral] But it's voluntary, right?[P] You didn't go for it?[B][C]")
	Append("Florentina:[E|Confused] Yeah, and they'll treat you like an outsider until you do.[P] For most Alraunes, it's not much of a choice.[B][C]")
	Append("Florentina:[E|Happy] Me?[P] I had other things I needed to do.[B][C]")
	Append("Mei:[E|Neutral] Oh?[B][C]")
	Append("Florentina:[E|Confused] ...[P] I don't want to talk about it right now.[B][C]")
	Append("Florentina:[E|Neutral] Can we just find you a way home?[B][C]")
end
