-- |[Slimes]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sMeiForm  = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Mei is not slime form.
if(sMeiForm ~= "Slime") then
	Append("Mei:[E|Neutral] Florentina, what do you know about slimes?[B][C]")
	Append("Florentina:[E|Neutral] I know enough not to fool with them.[B][C]")
	Append("Florentina:[E|Neutral] Slimes are minor predators and carrion feeders.[P] They eat whatever they can catch.[P] That includes you, if you're dense.[B][C]")
	Append("Mei:[E|Neutral] They'd attack one of us?[B][C]")
	Append("Florentina:[E|Happy] No, more like slowly slide up to us and nudge us.[P] We're too big, and it'd take days to digest us.[B][C]")
	Append("Florentina:[E|Neutral] You'd have to be pretty badly wounded for a slime to try to take you on.[B][C]")
	Append("Mei:[E|Neutral] But the slimes here - [P][CLEAR]")
	Append("Florentina:[E|Happy] Breeding season.[P] They take on human form when they're looking to reproduce.[B][C]")
	Append("Florentina:[E|Neutral] They're pretty dumb, so they'll attack bees, Alraunes, werecats, whatever.[P] But, they're looking for stray humans.[B][C]")
	Append("Mei:[E|Neutral] Oh...[B][C]")

--Mei is not in slime form.
else
	Append("Mei:[E|Neutral] Hey, Florentina...[B][C]")
	Append("Florentina:[E|Neutral] Huh?[B][C]")
	Append("Mei:[E|Blush] I keep catching you staring at my chest.[B][C]")
	Append("Florentina:[E|Blush] ...[P] and?[P] You're not exactly hiding it.[B][C]")
	Append("Mei:[E|Blush] Are you attracted to women, perchance?[B][C]")
	Append("Florentina:[E|Blush] Yeah.[P] All monstergirls are.[P] It's just how it is.[B][C]")
	Append("Florentina:[E|Neutral] ...[P] oh, you don't have monstergirls on Earth, do you?[B][C]")
	Append("Mei:[E|Neutral] Nope.[B][C]")
	Append("Florentina:[E|Neutral] Well, monstergirls are girls for a reason.[P] No men.[P] We're all attracted to the female form.[B][C]")
	Append("Florentina:[E|Neutral] Not that I don't appreciate a good man, but I know what I like.[B][C]")
	Append("Mei:[E|Blush] Should I make them bigger?[B][C]")
	Append("Florentina:[E|Blush] They're the right size.[P] Keep them right there...[B][C]")
	Append("Florentina:[E|Surprise] Hey![P] Don't manipulate my libido![B][C]")
	Append("Mei:[E|Neutral] Hee hee...[B][C]")
end
