-- |[Gravemarkers]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Mei is not a gravemarker:
if(sMeiForm ~= "Gravemarker") then
	Append("Mei:[E|Neutral] Have you ever seen a monstergirl like those gravemarkers?[B][C]")
	Append("Florentina:[E|Neutral] Let me think...[B][C]")
	Append("Florentina:[E|Neutral] I've probably seen them before, but they weren't moving at the time.[P] I thought they were statues.[B][C]")
	Append("Florentina:[E|Confused] Well they were, but you know what I mean.[B][C]")
	Append("Mei:[E|Neutral] And where was that?[B][C]")
	Append("Florentina:[E|Neutral] Business trip to a big church east of Jeffespeir.[P] It's huge, congregations in the thousands.[P] There's a whole town around it.[B][C]")
	Append("Florentina:[E|Neutral] Those things are positioned around the building.[P] I guess it keeps the thieves the hell out.[B][C]")
	Append("Mei:[E|Blush] They look like angels.[B][C]")
	Append("Florentina:[E|Happy] They probably are.[P] I wouldn't put it past the angels to use living statues as security guards.[B][C]")
	Append("Florentina:[E|Neutral] That means St. Fora's is an old angelic convent.[P] Or they beat up whoever's convent it was and took over.[B][C]")
	Append("Mei:[E|Neutral] Beat them up?[P] Why?[B][C]")
	Append("Florentina:[E|Neutral] It's a whole thing.[P] They'll find a religious convent, ask them to worship the holy light.[B][C]")
	Append("Florentina:[E|Neutral] If they say no, they bust in, kill or transform everyone, and put up their own statues and altars of worship.[P] Then they leave.[B][C]")
	Append("Florentina:[E|Confused] Free advice.[P] Don't piss off an angel, even if it seems like the right thing to do.[P] They're not worth it.[B][C]")

--Mei is a gravemarker:
else
	Append("Florentina:[E|Neutral] Mei, can I ask you about being a gravemarker?[B][C]")
	Append("Mei:[E|Neutral] Go ahead.[B][C]")
	Append("Florentina:[E|Neutral] They stung you with the holy light, right?[P] You're an angel under there?[B][C]")
	Append("Mei:[E|Blush] They 'stung' me?[P] Is that what you call total enlightenment, in more ways than one?[B][C]")
	Append("Florentina:[E|Happy] So that's a yes, then.[B][C]")
	Append("Florentina:[E|Neutral] What's it like?[B][C]")
	Append("Mei:[E|Neutral] Imagine taking a bath in sunlight.[P] Pure happiness, love, acceptance.[P] You believe you know everything in that moment.[B][C]")
	Append("Mei:[E|Neutral] And even if you don't, you know it's okay, because the light does.[P] It gets underneath you, below your conscious mind.[B][C]")
	Append("Mei:[E|Blush] If I hadn't grabbed my runestone when I got control over my body, I'd be down in that basement.[P] I'd be so happy...[B][C]")
	Append("Florentina:[E|Surprise] Wow.[B][C]")
	Append("Mei:[E|Offended] But as Miss Polaris said, it's a fake happiness.[P] I know that, because the light isn't in me anymore.[B][C]")
	Append("Mei:[E|Offended] They made my body pliable, and locked me in stone.[P] They want a guard dog.[B][C]")
	Append("Mei:[E|Blush] But, if I didn't know better, I'd be so... so happy...[B][C]")
	Append("Florentina:[E|Neutral] Huh.[P] Good to know.[B][C]")
	Append("Florentina:[E|Neutral] I've asked angels about it before, but those guys are liars.[B][C]")
	Append("Mei:[E|Offended] *Angels never lie!!!*[B][C]")
	Append("Florentina:[E|Offended] Get a hold of yourself, kid.[B][C]")
	Append("Mei:[E|Sad] Sorry...[P] Can we change the subject?[B][C]")
	Append("Florentina:[E|Neutral] Sure.[B][C]")
end
