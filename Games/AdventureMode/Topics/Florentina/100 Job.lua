-- |[Job]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Standard.
Append("Mei:[E|Neutral] Do you own the general store, or just manage it?[B][C]")
Append("Florentina:[E|Confused] ...[P] what?[B][C]")
Append("Mei:[E|Neutral] Well, I mean, it's your store.[P] Is it owned by someone else?[B][C]")
Append("Florentina:[E|Confused] You -[P] huh?[P] You can't own something without managing it.[B][C]")
Append("Mei:[E|Surprise] That's not how we do it on Earth.[B][C]")
Append("Florentina:[E|Happy] It is here.[P] In Trannadar, if you're not using a property then you had best have a good reason for your absence.[B][C]")
Append("Florentina:[E|Happy] Hypatia and I are co-owners.[P] I've got two years seniority, so that means I make most of the decisions.[B][C]")
Append("Mei:[E|Neutral] I had heard some Japanese companies are like that, but...[B][C]")
Append("Florentina:[E|Happy] Listen, Mei.[P] People go missing fairly often here.[P] If you're gone and don't come back, the rest of us can't afford to wait.[B][C]")
Append("Florentina:[E|Neutral] It hurts a lot when someone you care about...[P][E|Confused] is...[P] gone...[B][C]")
Append("Mei:[E|Neutral] Florentina?[B][C]")
Append("Florentina:[E|Neutral] Don't interrupt me.[B][C]")
Append("Florentina:[E|Happy] Anyway, ownership of property passes to whoever is making good use of it.[P] If nobody contests a claim for three months, it passes to the claimant.[B][C]")
Append("Mei:[E|Neutral] I saw some abandoned cabins in the forest.[P] Are those up for grabs?[B][C]")
Append("Florentina:[E|Happy] Sure, if nobody else is using them.[P] Blythe keeps a claims log in his office.[P] You looking to settle down?[B][C]")
Append("Mei:[E|Blush] Not at all.[P][E|Neutral] It's just so different from back home.[B][C]")
Append("Florentina:[E|Neutral] I'm starting to think this Earth place is somehow worse than Pandemonium.[B][C]")
Append("Mei:[E|Neutral] Huh...[B][C]")
