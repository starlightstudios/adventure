-- |[Our Next Move]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local iMetClaudia                  = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetClaudia", "N")
local iExaminedMirror              = VM_GetVar("Root/Variables/Chapter1/Scenes/iExaminedMirror", "N")
local iMeiKnowsRilmani             = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiKnowsRilmani", "N")
local iHasBreanneMetMei            = VM_GetVar("Root/Variables/Chapter1/Breanne/iHasMetMei", "N")
local iHasFoundOutlandAcolyte      = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasFoundOutlandAcolyte", "N")
local iBreanneMetMeiWithFlorentina = VM_GetVar("Root/Variables/Chapter1/Breanne/iMetMeiWithFlorentina", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--If you examined the mirror, this dialogue pops up:
if(iExaminedMirror == 1.0) then
	Append("Mei:[E|Sad] Florentina...[P] I'm gonna miss you...[B][C]")
	Append("Florentina:[E|Happy] Heh.[P] We're not done yet.[P] Let's go bust some heads.[P] You know, that can be your going-away present![B][C]")
	Append("Mei:[E|Neutral] Heh.[P] Okay![B][C]")
	Append("Florentina:[E|Happy] But, whenever we're done, sooner or later, you're going to have to go through that mirror.[B][C]")
	Append("Mei:[E|Neutral] Yeah.[P] But, I'll enjoy our time together.[B][C]")
	Append("Florentina:[E|Happy] Likewise.[B][C]")
	
--If Mei knows Rilmani:
elseif(iMeiKnowsRilmani == 1.0) then
	Append("Mei:[E|Neutral] Okay, so this runestone has a Rilmani symbol on it.[P] Any ideas?[B][C]")
	Append("Florentina:[E|Happy] Well, legend has it that the big mansion east of the Trading Post has something to do with the Rilmani.[P] It's called the Dimensional Trap, after all.[B][C]")
	Append("Mei:[E|Neutral] So we should check the stuff in there?[B][C]")
	Append("Florentina:[E|Neutral] Yeah, look for anything that might have a Rilmani symbol on it.[P] That's probably our lead.[B][C]")

--If Mei met Claudia:
elseif(iMetClaudia == 1.0) then
	Append("Mei:[E|Neutral] Claudia mentioned that her journal could help us, and that it's in the Quantir estate.[B][C]")
	Append("Florentina:[E|Neutral] She didn't give you particular directions?[B][C]")
	Append("Mei:[E|Neutral] She said it was a few weeks ago.[P] Someone might have moved it.[P] We'll just have to go in there and look for it.[B][C]")
	Append("Florentina:[E|Happy] And if we happen to find anything valuable, I'm sure nobody will mind if we take it.[B][C]")
	Append("Mei:[E|Happy] I knew you'd be excited![B][C]")


--Has met Karina but not talked to Breanne:
elseif(iHasFoundOutlandAcolyte == 1.0 and iBreanneMetMeiWithFlorentina == 0.0) then

	Append("Mei:[E|Neutral] So, where should we go next to find Claudia?[B][C]")
	Append("Florentina:[E|Neutral] That Karina character said she was up in Quantir.[P] It's not far from here.[B][C]")
	Append("Florentina:[E|Neutral] If memory serves, the old Quantir Estate should be northeast of the trading post.[P] Run down, hasn't been used in a long time.[P] We should start there.[B][C]")
	Append("Mei:[E|Neutral] What happened to it?[B][C]")
	Append("Florentina:[E|Neutral] Dunno.[P] The caravans sometimes go by it, but they're not dumb enough to go in it.[B][C]")
	Append("Mei:[E|Neutral] Is it dangerous?[B][C]")
	Append("Florentina:[E|Happy] In Quantir, it's a dangerous business going out your front door.[B][C]")
	Append("Florentina:[E|Happy] Like Trannadar, it's an 'unincorporated territory'.[P] That means, no aristocrats, no royals, no laws.[B][C]")
	Append("Mei:[E|Neutral] Well, danger is relative.[B][C]")
	Append("Florentina:[E|Neutral] We might also stop in at Breanne's place if you're feeling lucky.[P] She might have a clue, since Claudia probably went by her place.[B][C]")
	Append("Mei:[E|Happy] No time like the present. Let's go see what we can find![B][C]")

--Has spoken with Breanne but not Karina:
elseif(iHasFoundOutlandAcolyte == 0.0 and iBreanneMetMeiWithFlorentina == 1.0) then

	Append("Mei:[E|Neutral] So, where should we go next to find Claudia?[B][C]")
	Append("Florentina:[E|Neutral] Definitely we should check out Outland Farm.[P] Breanne said one of Claudia's followers was over there.[B][C]")
	Append("Florentina:[E|Neutral] It's just north of the trading post, across the river.[B][C]")
	Append("Mei:[E|Happy] All right![B][C]")

--Has spoken with Breanne and Karina:
elseif(iHasFoundOutlandAcolyte == 1.0 and iBreanneMetMeiWithFlorentina == 1.0) then

	Append("Mei:[E|Neutral] So, where should we go next to find Claudia?[B][C]")
	Append("Florentina:[E|Neutral] That Karina character said she was up in Quantir.[P] It's not far from here.[B][C]")
	Append("Florentina:[E|Neutral] If memory serves, the old Quantir Estate should be northeast of the trading post.[P] Run down, hasn't been used in a long time.[P] We should start there.[B][C]")
	Append("Mei:[E|Neutral] What happened to it?[B][C]")
	Append("Florentina:[E|Neutral] Dunno.[P] The caravans sometimes go by it, but they're not dumb enough to go in it.[B][C]")
	Append("Mei:[E|Neutral] Is it dangerous?[B][C]")
	Append("Florentina:[E|Happy] In Quantir, it's a dangerous business going out your front door.[B][C]")
	Append("Florentina:[E|Happy] Like Trannadar, it's an 'unincorporated territory'.[P] That means, no aristocrats, no royals, no laws.[B][C]")
	Append("Mei:[E|Happy] Danger is relative.[P] I'm sure we can handle it.[B][C]")
	Append("Florentina:[E|Happy] That's the right attitude.[B][C]")
	
--Has not met Karina and did not talk to Breanne about it:
else

	Append("Mei:[E|Neutral] So, where should we go next to find Claudia?[B][C]")
	Append("Florentina:[E|Neutral] Two possibilities, actually.[P] Last I heard, Claudia and her little troupe were up north, at the Outland Farm.[B][C]")
	Append("Florentina:[E|Neutral] I don't know if they're still there, but maybe we can find someone who saw them.[B][C]")
	Append("Mei:[E|Neutral] And the other possibility?[B][C]")
	Append("Florentina:[E|Neutral] There's an excessively nosy lady named Breanne who lives on the north end of the lake.[P] Has a place she calls the Pit Stop.[B][C]")
	
	--If Mei hasn't met Breanne:
	if(iHasBreanneMetMei == 0.0) then
		Append("Florentina:[E|Happy] Dumb name, but there it is.[B][C]")
		Append("Mei:[E|Offended] I don't think that's a dumb name...[B][C]")
		Append("Florentina:[E|Neutral] A lot of visitors to this area stop there since she has spare beds.[P] Might be a good place to look.[B][C]")
		Append("Mei:[E|Happy] All right![P] Let's go see what we can find![B][C]")
	
	--If Mei has met Breanne:
	else
		Append("Mei:[E|Happy] Breanne![P] She suggested I come visit you![B][C]")
		Append("Florentina:[E|Neutral] Huh.[P] Well, couldn't hurt to go see what she knows.[B][C]")
		Append("Mei:[E|Happy] All right![P] Let's go see what we can find![B][C]")
	end
end
