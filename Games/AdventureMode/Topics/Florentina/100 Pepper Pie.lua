-- |[Pepper Pie]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local iTakenPieJob = VM_GetVar("Root/Variables/Chapter1/Breanne/iTakenPieJob", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Already completed the pie job:
if(iTakenPieJob == 2.0) then
	Append("Mei:[E|Laugh] Mmmm![P] You weren't kidding that this pie is good![B][C]")
	Append("Florentina:[E|Happy] Ooooohhhhh yeah.[B][C]")
	Append("Florentina:[E|Happy] It regenerates slowly, so it can only be used three times per battle.[P] But, what a treat![B][C]")
	Append("Mei:[E|Neutral] Well then don't eat it all now, silly![B][C]")
	Append("Florentina:[E|Happy] Just a nibble...[B][C]")

--On the pie job:
else

	--Variables.
	local iMossCount      = AdInv_GetProperty("Item Count", "Gaardian Cave Moss")
	local iPaperCount     = AdInv_GetProperty("Item Count", "Booped Paper")
	local iNectarCount    = AdInv_GetProperty("Item Count", "Decayed Bee Nectar")
	local iFlowerCount    = AdInv_GetProperty("Item Count", "Hypnotic Flower Petals")
	local iSalamiCount    = AdInv_GetProperty("Item Count", "Translucent Quantirian Salami")
	local iKokayaneeCount = AdInv_GetProperty("Item Count", "Kokayanee")
	
	--Special:
	local iHasMetAdina           = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iHasMetAdina", "N")
	local iMeiLovesAdina         = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiLovesAdina", "N")
	local iAdinaExtendedMistress = VM_GetVar("Root/Variables/Chapter1/Scenes/iAdinaExtendedMistress", "N")

	--Common:
	Append("Mei:[E|Neutral] Okay, what's on the pie list?[B][C]")
	Append("Florentina:[E|Neutral] Let's see here...[B][C]")
	
	--Special case:
	if(iMossCount > 0 and iPaperCount > 0 and iNectarCount > 0 and iFlowerCount > 0 and iSalamiCount > 0 and iKokayaneeCount > 0) then
		Append("Florentina:[E|Neutral] Actually, we have everything we need.[P] Let's go get us a pie![B][C]")
		Append("Mei:[E|Neutral] Cool![P] I can't wait![P] I call first dibs on the taste-test![B][C]")
	
	--Otherwise:
	else

		--Moss:
		if(iMossCount < 1) then
			Append("Florentina:[E|Neutral] Gaardian Cave Moss.[P] I know this stuff -[P] nasty, but vital to the recipe.[P] Grows in damp, stony underground areas.[B][C]")
			Append("Florentina:[E|Neutral] Look for green mossy buildups.[P] I'll be able to tell which ones have the right stuff.[B][C]")
			Append("Mei:[E|Neutral] I've got an idea of where to search...[B][C]")
		end
		
		--Paper:
		if(iPaperCount < 1) then
			Append("Florentina:[E|Neutral] Canine Boop Paper.[P] Uhhh...[B][C]")
			Append("Mei:[E|Surprise] Boop paper?[B][C]")
			Append("Florentina:[E|Neutral] Paper that has been booped against a dog's nose.[P] Any breed of dog will do.[B][C]")
			Append("Mei:[E|Smirk] Why does it need to be booped?[P] What would that do?[B][C]")
			Append("Florentina:[E|Confused] Are you a mage?[P] Do you know how alchemical reactions work?[P] No?[P] Didn't think so![B][C]")
			Append("Mei:[E|Neutral] Okay, okay.[P] Next?[B][C]")
		end
		
		--Nectar:
		if(iNectarCount < 1) then
			Append("Florentina:[E|Neutral] Decayed Bee Nectar.[B][C]")
			Append("Mei:[E|Neutral] Well that one sounds easy.[B][C]")
			Append("Florentina:[E|Confused] No, it has to be decayed.[P] It won't be in a beehive, it'll be someplace else.[P] It decays if the bees don't process it in time.[B][C]")
			Append("Florentina:[E|Neutral] It'll be someplace near a lot of flowers where the bees over-harvested and had to discard some.[B][C]")
		end
		
		--Flower:
		if(iFlowerCount < 1) then
			Append("Florentina:[E|Neutral] Hypnotic Flower Petals.[P] There's a couple candidate species, but none of them are native to Trannadar.[P] This one might be tough.[B][C]")
			
			--Mei has met Adina and smelled the hypnotic flower.
			if(iHasMetAdina >= 10.0) then
				
				--Mei not in love with Adina:
				if(iMeiLovesAdina == 0.0) then
					Append("Mei:[E|Smirk] Oh, I think I have an idea.[P] Let's check the salt flats down south.[B][C]")
					Append("Florentina:[E|Neutral] Yeah, sure.[P] Might be something there.[B][C]")
				
				--Mei is in love with Adina:
				else
					Append("Mei:[E|Blush] Oh this one is easy![P] Mistress Adina grows those on the salt flats![B][C]")
					
					if(iAdinaExtendedMistress == 0.0) then
						Append("Florentina:[E|Surprise] ...[P] Mistress?[B][C]")
						Append("Florentina:[E|Neutral] Nevermind.[P] Let's go find the flower.[B][C]")
                        VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsMistress", "N", 1.0)
					else
						Append("Florentina:[E|Surprise] Mistress?[B][C]")
						Append("Florentina:[E|Neutral] Oh yeah, you guys are freaks.[P] Whatever.[P] Let's go find that flower.[B][C]")
					end
				end
			
			--Mei did not smell the flower:
			else
				Append("Mei:[E|Neutral] I have no idea.[P] Guess we'll have to do some searching.[B][C]")
			end
		end
		
		--Salami:
		if(iSalamiCount < 1) then
			Append("Florentina:[E|Neutral] Quantirian Salami.[P] That might be a bit tough.[B][C]")
			Append("Mei:[E|Neutral] Why would that be?[B][C]")
			Append("Florentina:[E|Neutral] Quantir's total population is less than a hundred.[P] I don't know if any of them even know how to make Quantirian Salami.[B][C]")
			Append("Florentina:[E|Happy] The stuff is so salted that it'd take centuries to decay, though.[P] We might be able to find some leftover if we look.[B][C]")
			Append("Mei:[E|Smirk] All right![P] Next?[B][C]")
		end
		
		--Kokaynee
		if(iKokayaneeCount < 1) then
			Append("Florentina:[E|Happy] Kokayanee.[P] Hmmm...[P] that one hits a bit close to home.[B][C]")
			Append("Mei:[E|Neutral] Huh?[B][C]")
			Append("Florentina:[E|Neutral] Blythe is a real stickler about the stuff.[P] He keeps the mercs clean.[B][C]")
			Append("Mei:[E|Neutral] It's a drug?[B][C]")
			Append("Florentina:[E|Happy] One of the knock-you-on-your-sor drugs, actually.[P] Doesn't work on Alraunes, so I don't care for it.[B][C]")
			Append("Florentina:[E|Neutral] Someone will probably have a stash, but I bet they won't want to part with it.[B][C]")
			Append("Mei:[E|Smirk] So we'll have to ask around, and definitely don't ask any of the mercs.[P] Got it.[P] What's next?[B][C]")
		end
		
		--Final case.
		Append("Florentina:[E|Neutral] That's it.[P] Shall we get back to it, then?[B][C]")
	end
end
