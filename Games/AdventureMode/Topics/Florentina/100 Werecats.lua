-- |[Werecats]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sMeiForm        = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local iHasWerecatForm = VM_GetVar("Root/Variables/Global/Mei/iHasWerecatForm", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Mei is in werecat form.
if(sMeiForm == "Werecat" or iHasWerecatForm == 1.0) then
	Append("Mei:[E|Neutral] Do you have many dealings with the kinfangs of this region?[B][C]")
	Append("Florentina:[E|Neutral] You mean werecats, right?[P] Yeah, sort of.[B][C]")
	Append("Florentina:[E|Neutral] They've got everything they want in the forest.[P] They come to buy medicine sometimes, or to put a hit out.[B][C]")
	Append("Mei:[E|Surprise] A -[P] hit?[B][C]")
	Append("Florentina:[E|Neutral] Slang for assassination.[B][C]")
	Append("Mei:[E|Offended] I know what it means![B][C]")
	Append("Mei:[E|Neutral] Why would a kinfang do that?[B][C]")
	Append("Florentina:[E|Neutral] Search me.[P] I don't ask too many questions.[B][C]")
	Append("Mei:[E|Offended] Lazy![P] If you want someone dead, do it yourself![B][C]")
	Append("Florentina:[E|Blush] You're my best friend right at this second.[B][C]")
	Append("Mei:[E|Sad] I just don't get it.[P] There's got to be a reason...[B][C]")
	Append("Florentina:[E|Neutral] Some of the werecats prefer scavenging to hunting.[P] Maybe they're not good at violence but want another cat dead?[B][C]")
	Append("Mei:[E|Neutral] Hmmm...[B][C]")

--Normal case.
else
	Append("Mei:[E|Neutral] Do you have many dealings with the werecats of this region?[B][C]")
	Append("Florentina:[E|Neutral] Sort of.[P] They've got everything they want in the forest.[P] They come to buy medicine sometimes, or to put a hit out.[B][C]")
	Append("Mei:[E|Surprise] A -[P] hit?[B][C]")
	Append("Florentina:[E|Neutral] Slang for assassination.[B][C]")
	Append("Mei:[E|Offended] I know what it means![B][C]")
	Append("Mei:[E|Sad] Do the werecats -[P] do they kill each other?[B][C]")
	Append("Florentina:[E|Happy] Obviously not.[P] They hire mercenaries to do it.[B][C]")
	Append("Mei:[E|Sad] Maybe there's some rule against killing another werecat?[B][C]")
	Append("Florentina:[E|Happy] Gee, can you think of any other kind of society where killing another citizen is illegal?[B][C]")
	Append("Mei:[E|Offended] Okay, jeez, lay off![B][C]")
	Append("Florentina:[E|Happy] Ha ha![P] Oh you're fun to tease![B][C]")

end
