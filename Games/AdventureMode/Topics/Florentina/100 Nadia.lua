-- |[Nadia]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Standard.
Append("Mei:[E|Neutral] What do you think of Nadia?[P] Are you two friends?[B][C]")
Append("Florentina:[E|Neutral] I wondered how long it'd be before you brought up that nutjob.[B][C]")
Append("Mei:[E|Laugh] Nutjob?[P] Well -[B][C]")
Append("Florentina:[E|Confused] If you make a pun I'm going to whack you.[B][C]")
Append("Mei:[E|Offended] Fine, jeez.[B][C]")
Append("Florentina:[E|Neutral] Nadia's okay.[P] Irritating, but useful.[B][C]")
Append("Mei:[E|Neutral] And would I qualify as irritating but useful?[B][C]")
Append("Florentina:[E|Neutral] Yes.[B][C]")
Append("Mei:[E|Happy] So that means you and Nadia are good friends.[P] Case closed.[B][C]")
Append("Florentina:[E|Neutral] It's all relative...[B][C]")
