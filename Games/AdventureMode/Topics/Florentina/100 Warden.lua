-- |[Warden]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local iHasGhostForm            = VM_GetVar("Root/Variables/Global/Mei/iHasGhostForm", "N")
local iCompletedQuantirMansion = VM_GetVar("Root/Variables/Chapter1/Scenes/iCompletedQuantirMansion", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Has not completed the mansion events:
if(iCompletedQuantirMansion == 0.0) then
	Append("Mei:[E|Neutral] The warden...[P] do we even know what her name was?[B][C]")
	Append("Florentina:[E|Neutral] I don't think so.[P] Did any of those journal pages mention it?[B][C]")
	Append("Mei:[E|Neutral] I guess not.[B][C]")
	
	if(iHasGhostForm == 1.0) then
		Append("Mei:[E|Neutral] Seems Natalie doesn't know either.[P] There were a lot of maids doing burials, so I'm not sure.[B][C]")
	end
	Append("Florentina:[E|Confused] Considering everything we know, reasoning with her isn't going to help.[B][C]")
	Append("Mei:[E|Neutral] So how are we going to defeat her?[B][C]")
	Append("Florentina:[E|Confused] Okay, so my memory is a little hazy, what with the hard hit to the head and all...[B][C]")
	Append("Florentina:[E|Neutral] But it seemed that she was going for you first, for some reason.[B][C]")
	Append("Florentina:[E|Neutral] If you were ready for it, you could probably try to block the hit.[B][C]")
	Append("Mei:[E|Neutral] Would that be enough?[P] It was like getting hit by a train...[B][C]")
	Append("Florentina:[E|Neutral] There are other ways to soften a hit in battle.[P] I'll see what I can do to help.[B][C]")
	Append("Florentina:[E|Neutral] It might also be a good idea to look for combat books.[P] There are a bunch of publishers who put them out on a monthly basis.[B][C]")
	Append("Florentina:[E|Neutral] No matter how many scraps you've been in, you can always learn something new.[B][C]")
	Append("Mei:[E|Sad] *sigh*...[B][C]")
	Append("Mei:[E|Sad] Is violence really the only way forward?[B][C]")
	Append("Florentina:[E|Confused] This isn't a fight, Mei.[P] This is a mercy-kill.[P] They're just going to suffer more if we dither.[B][C]")
	Append("Mei:[E|Sad] Yeah...[B][C]")

--Normal case.
else
	Append("Mei:[E|Neutral] The warden...[P] we didn't even know her name...[B][C]")
	Append("Florentina:[E|Neutral] Good riddance to bad rubbish.[B][C]")
	Append("Mei:[E|Sad] But Florentina - [P][CLEAR]")
	Append("Florentina:[E|Confused] Mei, that thing wasn't human.[P] It was pure anger.[B][C]")
	Append("Florentina:[E|Confused] No matter what her intentions may have been, she caused a lot of people to suffer.[P] She's just as bad as Quantir was.[B][C]")
	Append("Florentina:[E|Confused] The right thing to do in those cases is to wash your hands and move on.[P] There's no happy ending here.[B][C]")
	Append("Mei:[E|Neutral] Yeah, I guess you're right.[P] We'll just have to make sure they're not forgotten.[B][C]")
	Append("Mei:[E|Neutral] Will you ask Blythe to record what happened here?[P] Tell the historians?[B][C]")
	Append("Florentina:[E|Neutral] If it'll get you to shut up, sure.[B][C]")
	Append("Mei:[E|Neutral] So what will happen to the ghosts now?[B][C]")
	Append("Florentina:[E|Neutral] Dunno.[P] Maybe they'll fade away, maybe they'll remember who they were.[B][C]")
	Append("Florentina:[E|Happy] If any of the tireless dead need a job cleaning, Hypatia's pretty lousy at it.[P] I could hire one.[B][C]")
	Append("Mei:[E|Surprise] R-[P]really?[B][C]")
	Append("Florentina:[E|Happy] Think about it -[P] no sick leave![B][C]")
	Append("Mei:[E|Neutral] ...[B][C]")
	Append("Mei:[E|Happy] Only you could put a positive spin on undeath.[B][C]")
	Append("Florentina:[E|Neutral] There's a time to mourn, and a time to move on.[P] Shall we get back to finding a way home for you?[B][C]")

end
