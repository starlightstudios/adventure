-- |[Trading Post]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Standard.
Append("Mei:[E|Neutral] Do you know how long the trading post has been there?[B][C]")
Append("Florentina:[E|Neutral] This particular instance?[P] I think Blythe mentioned he took over for someone else eight years ago, so at least that long.[B][C]")
Append("Mei:[E|Neutral] But beyond that?[B][C]")
Append("Florentina:[E|Happy] Mei, that spot has probably been a settlement for a long time.[P] It's in a good spot where rivers and paths intersect.[B][C]")
Append("Florentina:[E|Happy] If that kind of spot gets abandoned, someone will get it in their head sooner or later to re-establish it.[P] Why do you ask?[B][C]")
Append("Mei:[E|Happy] Comparing it to home.[P] People have been living in the Hong Kong area for some thirty-five thousand years.[P] It's a very old city.[B][C]")
Append("Florentina:[E|Surprise] Thirty-five...[P] thousand?[P] Is the world even that old?[B][C]")
Append("Mei:[E|Neutral] Earth is billions of years old.[P] I would figure Pandemonium is too.[B][C]")
Append("Florentina:[E|Confused] Yeah, you're making that up.[B][C]")
Append("Mei:[E|Neutral] (I guess they haven't figured out archaeology here yet...)[B][C]")
