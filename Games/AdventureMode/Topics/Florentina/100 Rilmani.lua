-- |[Rilmani]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Dialogue.
Append("Mei:[E|Smirk] So, Rilmani.[P] Do you know anything about them that might help?[B][C]")
Append("Florentina:[E|Neutral] Nope, sorry.[P] I didn't even think they existed.[B][C]")
Append("Mei:[E|Smirk] The book said they're a local legend.[B][C]")
Append("Florentina:[E|Neutral] Yeah, there's legends about them almost everywhere you go.[P] Usually someone knew someone who knew someone who saw one.[B][C]")
Append("Florentina:[E|Happy] ...[P] Alcohol is usually involved...[B][C]")
Append("Mei:[E|Smirk] What do the legends say?[B][C]")
Append("Florentina:[E|Neutral] Well, they're four meters tall, can walk through walls, have short necks and legs, and they love to eat disobedient children.[B][C]")
Append("Mei:[E|Neutral] Yeah, that's not useful...[B][C]")
Append("Florentina:[E|Neutral] Sometimes I get people who are looking for Rilmani artifacts.[P] Usually they're procurement types.[B][C]")
Append("Mei:[E|Neutral] What does that mean?[B][C]")
Append("Florentina:[E|Neutral] If you're an alchemist and you need something exotic, or a mage who wants a catalyst, you can't exactly find them for sale in a store.[B][C]")
Append("Florentina:[E|Neutral] So they'll hire a procurement agent to find what they need.[B][C]")
Append("Mei:[E|Smirk] So they're like treasure hunters?[B][C]")
Append("Florentina:[E|Blush] Oooh, you're making me excited.[P] Treasure is such a sexy word...[B][C]")
Append("Florentina:[E|Happy] Anyway, I've never met one who actually found a Rilmani artifact, much less a Rilmani.[P] Either they're rare, or they clean up after themselves.[B][C]")
Append("Mei:[E|Happy] You've also never met someone from Earth![B][C]")
Append("Florentina:[E|Happy] And you have a runestone which is probably one such artifact.[P] Suddenly I like our chances.[B][C]")
Append("Florentina:[E|Neutral] The legends say they're found in that mansion near the lake.[P] It's called the Dimensional Trap sometimes.[P] We should look there.[B][C]")
Append("Mei:[E|Smirk] All right![B][C]")
