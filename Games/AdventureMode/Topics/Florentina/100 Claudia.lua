-- |[Claudia]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local iSavedClaudia = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedClaudia", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Standard.
if(iSavedClaudia == 0.0) then
	Append("Mei:[E|Neutral] Do you remember anything about Claudia that might help find her?[B][C]")
	Append("Florentina:[E|Confused] I remember that I didn't like her, that's for sure.[B][C]")
	Append("Mei:[E|Laugh] You don't seem to like anyone.[B][C]")
	Append("Florentina:[E|Happy] Excellent choice of words.[B][C]")
	Append("Florentina:[E|Neutral] It was mostly Hypatia who did the bulk of the dealings.[P] I ran into her when she tried to break up a card game I was running.[B][C]")
	Append("Mei:[E|Neutral] Card game?[P] Like, gambling?[B][C]")
	Append("Florentina:[E|Happy] Ever heard of Six-Step Shuffle?[B][C]")
	Append("Mei:[E|Neutral] No, never.[B][C]")
	Append("Florentina:[E|Happy] You bet on who's got the best hand of cards, and there's several rounds of betting.[B][C]")
	Append("Florentina:[E|Happy] The catch is that you can't bet on yourself.[P] So if you've got the best hand, you have to find a way to ruin it, or improve someone else's.[B][C]")
	Append("Florentina:[E|Happy] It's all about bluffing, guessing motives, and lying.[B][C]")
	Append("Florentina:[E|Confused] Apparently, her religion thinks that me taking money from dumb people is immoral.[B][C]")
	Append("Florentina:[E|Confused] She got really riled up, and Blythe had to get involved.[P] That's why I let Hypatia do the dealing.[B][C]")
	Append("Mei:[E|Neutral] I see.[B][C]")
	Append("Florentina:[E|Neutral] She's a real goodie-goodie.[P] Kind of like you, except she was blonde and shorter.[B][C]")
	Append("Mei:[E|Happy] I'll consider that a compliment.[B][C]")

--Claudia has already been rescued:
else
	Append("Mei:[E|Neutral] Claudia is kind of a saccharine person, isn't she?[B][C]")
	Append("Florentina:[E|Happy] Absolutely.[P] She was a lot of fun to mock.[B][C]")
	Append("Mei:[E|Offended] Florentina![B][C]")
	Append("Florentina:[E|Neutral] What?[P] She would torture my words until they were compliments.[P] It was genuinely endearing to watch.[B][C]")
	Append("Mei:[E|Smirk] So you made fun of her, but failed.[P] She was too nice to even insult.[B][C]")
	Append("Florentina:[E|Confused] I didn't say that...[B][C]")
	Append("Mei:[E|Smirk] Does that irk you?[P] That you couldn't get to somebody?[B][C]")
	Append("Florentina:[E|Neutral] In my younger days, it probably would have.[P] Now?[P] Meh.[B][C]")
	Append("Florentina:[E|Confused] I'm kind of surprised nobody has taken advantage of her charitable attitude yet.[P] Then again I can say the same about you.[B][C]")
	Append("Mei:[E|Offended] You're not doing people a favour by being a jerk to them.[B][C]")
	Append("Florentina:[E|Neutral] You're also not doing them a favour by being nice.[P] Think about it.[B][C]")

end
