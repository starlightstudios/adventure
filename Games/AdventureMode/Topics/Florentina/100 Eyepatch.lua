-- |[Werecats]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local iReadAlraunes = VM_GetVar("Root/Variables/Chapter1/Scenes/iReadAlraunes", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Mei does not know Alraunes can regenerate:
if(iReadAlraunes == 0.0) then
	Append("Mei:[E|Smirk] So how'd you lose an eye, anyway?[B][C]")
	Append("Florentina:[E|Neutral] Now let's see...[P] which story are you most likely to believe?[B][C]")
	Append("Florentina:[E|Happy] Knife fight is obvious.[P] Failed assassination is also possible, you'd go for that.[B][C]")
	Append("Mei:[E|Neutral] You're not going to tell me, are you?[B][C]")
	Append("Florentina:[E|Neutral] I have my reasons for my privacy, Mei.[P] I don't like you prying.[B][C]")
	Append("Mei:[E|Sad] I just want to know you better...[B][C]")
	Append("Florentina:[E|Neutral] Buck up, kid.[P] I'm not trying to sour you, I just don't think it's something you need to know.[B][C]")
	Append("Florentina:[E|Happy] Do you know how old I am?[B][C]")
	Append("Mei:[E|Neutral] Well, I don't mean to be impolite, but...[B][C]")
	Append("Florentina:[E|Happy] Sixty-two.[B][C]")
	Append("Mei:[E|Surprise] ..![P] Really?[B][C]")
	Append("Florentina:[E|Happy] See, that one you're willing to believe.[P] Alraunes don't age like humans, we age like trees.[B][C]")
	Append("Florentina:[E|Happy] We could spend days talking about all the awesome stuff I've done.[P] But, we have a job to do.[B][C]")
	Append("Mei:[E|Laugh] So are you really in your sixties?[B][C]")
	Append("Florentina:[E|Confused] It's like talking to a brick wall...[B][C]")

--Mei knows Florentina can regrow her eye:
else
	Append("Mei:[E|Smirk] So how'd you lose an eye, anyway?[B][C]")
	Append("Florentina:[E|Neutral] Now let's see...[P] which story are you most likely to believe?[B][C]")
	Append("Florentina:[E|Happy] Knife fight is obvious.[P] Failed assassination is also possible, you'd go for that.[B][C]")
	Append("Mei:[E|Neutral] You're not going to tell me, are you?[B][C]")
	Append("Florentina:[E|Neutral] I have my reasons for my privacy, Mei. I don't like you prying.[B][C]")
	Append("Mei:[E|Sad] I just want to know you better...[B][C]")
	Append("Florentina:[E|Neutral] Buck up, kid.[P] I'm not trying to sour you, I just don't think it's something you need to know.[B][C]")
	Append("Florentina:[E|Happy] Do you know how old I am?[B][C]")
	Append("Mei:[E|Neutral] Well, I don't mean to be impolite, but...[B][C]")
	Append("Florentina:[E|Neutral] Sixty-two.[B][C]")
	Append("Mei:[E|Surprise] ..![P] Really?[B][C]")
	Append("Florentina:[E|Happy] See, that one you're willing to believe.[P] Alraunes don't age like humans, we age like trees.[B][C]")
	Append("Florentina:[E|Happy] We could spend days talking about all the awesome stuff I've done.[P] But, we have a job to do.[B][C]")
	Append("Mei:[E|Offended] Hey, wait a second![P] That Heavenly Doves log said that Alraunes regrow lost body parts![B][C]")
	Append("Florentina:[E|Neutral] Well yes, we do.[P] It takes a while, though.[P] Trees take years to regrow branches, after all.[B][C]")
	Append("Mei:[E|Smirk] So why don't you just regrow your eye?[B][C]")
	Append("Florentina:[E|Neutral] Mei, when you met me, which eye was my patch over?[P] Left, or right?[B][C]")
	Append("Mei:[E|Neutral] Well, uhh....[B][C]")
	Append("Florentina:[E|Neutral] How do you know I didn't switch it when you weren't looking?[B][C]")
	Append("Florentina:[E|Neutral] More importantly, if you were a bounty hunter who's never seen me before, and you were told I had an eyepatch...[B][C]")
	Append("Mei:[E|Happy] And then you took it off...[B][C]")
	Append("Florentina:[E|Happy] Now you're getting it.[B][C]")
	Append("Mei:[E|Laugh] So -[P] are you actually missing an eye?[B][C]")
	Append("Florentina:[E|Confused] It's like talking to a brick wall...[B][C]")

end
