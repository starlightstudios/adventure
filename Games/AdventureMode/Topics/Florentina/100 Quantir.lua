-- |[Quantir]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

Append("Mei:[E|Smirk] So what exactly is Quantir?[B][C]")
Append("Florentina:[E|Neutral] It's usually called the Quantir High Wastes.[P] It's an arid province east of Evermoon Forest.[B][C]")
Append("Florentina:[E|Neutral] Something about the mountains prevents rainclouds from reaching it, or something.[B][C]")
Append("Mei:[E|Smirk] Are there many people there?[B][C]")
Append("Florentina:[E|Neutral] Not a lot of humans that I know about.[P] There's a few small settlements, but it's bad land for farming.[B][C]")
Append("Florentina:[E|Neutral] Nobody administers the land, really.[P] None of the city states care about it since it's not good for much.[B][C]")
Append("Florentina:[E|Happy] There apparently was a gold rush a few decades back that I missed out on.[P] Lots of abandoned buildings in former mining communities.[B][C]")
Append("Mei:[E|Neutral] Gold rushes...[P] we had those on Earth, too.[B][C]")
Append("Florentina:[E|Happy] When people hear they can get rich, they'll come from a continent away for even the smallest chance -[P] and damn the consequences.[B][C]")
Append("Florentina:[E|Neutral] It's a good place to go if you need to get lost.[P] If you ever get a price on your head, there's all kinds of caves to hide out in.[B][C]")
Append("Mei:[E|Smirk] Spoken from experience?[B][C]")
Append("Florentina:[E|Surprise] You'd impugn my honor?[B][C]")
Append("Florentina:[E|Happy] Yeah okay, I maaaay have spent a few weeks hiding out a few years back...[B][C]")
