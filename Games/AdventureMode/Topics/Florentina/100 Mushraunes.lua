-- |[Mushraunes]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local iMetMycela      = VM_GetVar("Root/Variables/Chapter1/Sharelock/iMetMycela", "N")
local iDefeatedMycela = VM_GetVar("Root/Variables/Chapter1/Sharelock/iDefeatedMycela", "N")
local sCurrentJob     = VM_GetVar("Root/Variables/Global/Florentina/sCurrentJob", "S")
local iHasJob_Agarist = VM_GetVar("Root/Variables/Global/Florentina/iHasJob_Agarist", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

-- |[Mushraune Florentina Cases]|
if(sCurrentJob == "Agarist") then
	Append("Mei:[E|Neutral] I'd like to ask about your mushy friend there.[B][C]")
	Append("Florentina:[E|Neutral] What's up?[B][C]")
	Append("Mei:[E|Happy] Does he have a name or anything?[B][C]")
	Append("Florentina:[E|Happy] You can't name him in any way that matters.[P] Fungi have names as long as a book.[B][C]")
	Append("Florentina:[E|Neutral] Forogito, descendent of Memil, Irosh, the seer of Nind, eye before - [B][C]")
	Append("Mei:[E|Neutral] Figures mushrooms have a weird naming scheme.[B][C]")
	Append("Florentina:[E|Neutral] Yeah, I just call him buddy, and he's been calling me buddy.[P] He also calls you buddy, which doesn't help.[B][C]")
	Append("Florentina:[E|Neutral] He thinks you're really cool, by the way.[P] Loves the whole good-guy shtick.[B][C]")
	Append("Mei:[E|Happy] Thanks, buddy![B][C]")
    
elseif(sCurrentJob == "Lurker") then
	Append("Mei:[E|Neutral] Hey, how's your mushy buddy handle the whole mannequin thing?[B][C]")
	Append("Florentina:[E|Neutral] He's still here, he's just also a mannequin, incorporated into my skin.[B][C]")
	Append("Florentina:[E|Neutral] We both think it's plenty strange but the feeling isn't exactly uncomfortable.[P] Kinda tingles, but it feels like alcohol on your skin in the breeze.[P] You know that feeling?[B][C]")
	Append("Mei:[E|Neutral] I guess so?[P] As long as you're okay with it.[B][C]")
	Append("Florentina:[E|Neutral] I don't want to run into an undead fungus, that'd be a bit much.[P] Gives us both the creeps.[B][C]")
    
elseif(iHasJob_Agarist == 1.0) then
	Append("Mei:[E|Neutral] What's it like, sharing your head with someone?[B][C]")
	Append("Florentina:[E|Neutral] It's not all that new of a feeling, really. It's like having a little one tagging along, and a bit louder.[P] He's 'here' even when I don't have my mushy stuff on display, he can hear you.[B][C]")
	Append("Mei:[E|Neutral] And you can call upon his powers?[P] How come a mushroom has powers, anyway?[B][C]")
	Append("Florentina:[E|Happy] I guess mushrooms can use magic, same as anyone else.[P] Plants don't do that, other than alraunes of course.[P] I've heard there are animal wizards but never had the chance to meet one.[B][C]")
	Append("Florentina:[E|Neutral] I wouldn't be surprised at this point if there was an entire fungal civilization under the ground.[P] In fact, I'd send trade caravans over.[B][C]")
	Append("Florentina:[E|Happy] We're going to corner the market on fungal services and luxuries![B][C]")

-- |[No Mushraune Florentina]|
--Hasn't met Mycela:
elseif(iMetMycela == 0.0) then
	Append("Mei:[E|Neutral] Do you know anything about these mushraunes we've been seeing?[B][C]")
	Append("Florentina:[E|Neutral] Not really.[P] There's a million kinds of monstergirl out there.[P] They must've migrated in recently.[B][C]")
	Append("Florentina:[E|Neutral] They do resemble alraunes, but alraunes don't grow mushrooms.[B][C]")
	Append("Mei:[E|Neutral] Are you sure?[B][C]")
	Append("Florentina:[E|Neutral] Positive.[P] Mushrooms don't allow the little ones within them.[P] In fact, fungus is a lot closer to animal than plant.[B][C]")
	Append("Florentina:[E|Neutral] I've tried talking to mushrooms before.[P] All you hear is an odd echo, like your own words coming to you from long ago, distorted.[B][C]")
	Append("Florentina:[E|Happy] It's kinda funny to tell a new alraune to talk to a mushroom, though.[P] They freak out for a bit.[B][C]")
	Append("Mei:[E|Surprise] Did you do that to Nadia?[B][C]")
	Append("Florentina:[E|Blush] Uh well...[P] hey, let's talk about something else![B][C]")

--Has seen Mycela:
elseif(iMetMycela == 1.0 and iDefeatedMycela == 0.0) then
	Append("Mei:[E|Neutral] You think that alraune we saw in the basement is responsible for the mushraunes?[B][C]")
	Append("Florentina:[E|Offended] No, I'd say the mushrooms themselves are.[P] She's a conduit.[B][C]")
	Append("Florentina:[E|Offended] I don't know how willing she is in this whole thing, but it's clear that pool is the source of this.[B][C]")
	Append("Mei:[E|Sad] I hope we can help all those poor mushraunes...[B][C]")
	Append("Florentina:[E|Confused] I've never seen mushrooms do this to someone, though.[P] I don't like it at all.[B][C]")
    
--Defeated Mycela:
else
	Append("Mei:[E|Neutral] You think that alraune we saw in the basement was responsible for the mushraunes?[B][C]")
	Append("Florentina:[E|Offended] No, I'd say the mushrooms themselves were.[P] She was a conduit.[B][C]")
	Append("Florentina:[E|Offended] Something got into that spawning pool.[P] The mushrooms were using anyone she dunked as a host.[B][C]")
	Append("Mei:[E|Sad] I hope Rochea can help all those poor mushraunes...[B][C]")
	Append("Florentina:[E|Confused] If she's good for anything, it's this.[P] Better left in her care than a weird, horny mushroom host.[B][C]")
end
