-- |[Pandemonium]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Standard.
Append("Florentina:[E|Neutral] You know, I had figured you would be asking me non-stop about Pandemonium.[B][C]")
Append("Mei:[E|Offended] I'm trying to stay focused.[P] Every time I see something new we could probably spend ten minutes discussing it.[B][C]")
Append("Florentina:[E|Neutral] Probably.[P] I was trying to say you're less annoying on this count than some others.[B][C]")
Append("Mei:[E|Angry] Oh, okay![P] So, tell me about freakin' Pandemonium![B][C]")
Append("Florentina:[E|Neutral] Teasing you is a lot of fun.[B][C]")
Append("Mei:[E|Offended] Mrgrgr...[B][C]")
Append("Florentina:[E|Neutral] Ha![B][C]")
