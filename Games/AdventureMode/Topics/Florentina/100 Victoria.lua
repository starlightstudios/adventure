-- |[Victoria]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sMeiForm  = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")
Append("Mei:[E|Neutral] Florentina, about Victoria...[B][C]")
Append("Florentina:[E|Confused] What about her?[B][C]")
Append("Mei:[E|Blush] Did you like it?[P] The flirting part?[B][C]")
Append("Florentina:[E|Blush] I'll just say it's a shame that a body like that was wasted on petty self-interest and revenge.[B][C]")
Append("Florentina:[E|Neutral] If she would put aside her ego and worship me instead, we could get along![B][C]")
Append("Mei:[E|Sad] But she was turning people into objects, and using them.[P] Can someone like that be redeemed?[B][C]")
Append("Florentina:[E|Neutral] Probably not, but redemption is overrated.[P] You just sort of keep stumbling ahead because going to wizard jail isn't the end of her life.[B][C]")
Append("Florentina:[E|Happy] She's got ability, I could make her useful.[P] If her debt comes up, I might just buy it.[P] I'd need to shift some investments but I think I could cover it.[B][C]")
Append("Mei:[E|Neutral] What does that mean, buy her debt?[B][C]")
Append("Florentina:[E|Neutral] A lot of places will sell you a prisoner for money, and they can work off their sentence for a private person instead of doing labour at a work camp.[B][C]")
Append("Florentina:[E|Neutral] You can also just dismiss them if you want, but most people don't do that.[B][C]")
Append("Mei:[E|Offended] How is that any different than what Victoria was doing to those bandits!?[B][C]")
Append("Florentina:[E|Happy] For the most part?[P] It isn't, except the king or the state take a cut and call it legal.[P] Me, I don't care either way.[B][C]")
Append("Florentina:[E|Neutral] As long as you aren't too harsh, you won't get bounty hunters coming after you.[P] They have bigger problems.[B][C]")
Append("Mei:[E|Offended] Sheesh.[P] Earth is better in a lot of ways, but...[B][C]")
Append("Florentina:[E|Neutral] Let me guess, you've still got slavery there, too?[B][C]")
Append("Mei:[E|Sad] Yeah...[B][C]")
Append("Florentina:[E|Neutral] Crushing someone under your heel is the best way to look taller.[P] That's an old saying out east.[P] Oh, and then 'cut down the tallest poppy' as a response to that.[B][C]")
Append("Florentina:[E|Neutral] It means conquerers can kill the richest citizens and liberate the public to gain their support against the local oligarchs.[B][C]")
Append("Mei:[E|Neutral] We're good, but we're not going to end a milennia-long cycle of slavery and liberation on our own.[B][C]")
Append("Florentina:[E|Happy] No I think we'd need at least six or seven people to pull that off.[B][C]")