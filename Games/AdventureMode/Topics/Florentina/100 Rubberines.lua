-- |[Rubberines]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Variables.
local iRubberVictory    = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iRubberVictory", "N")
local iPolarisGavePhial = VM_GetVar("Root/Variables/Chapter1/Scenes/iPolarisGavePhial", "N")
local iReturnedPhial    = VM_GetVar("Root/Variables/Chapter1/Scenes/iReturnedPhial", "N")

--No rubber victory.
if(iRubberVictory == 0.0) then
	Append("Mei:[E|Neutral] What do you think of these rubber...[P] people?[B][C]")
	Append("Florentina:[E|Neutral] It's been a weird couple of days with you, Mei, but this isn't that unusual for Pandemonium.[B][C]")
	Append("Florentina:[E|Neutral] Okay, maybe a little.[P] Usually, it isn't this [P]*dense*[P] with weirdness.[B][C]")
	Append("Florentina:[E|Neutral] I once ran into someone trying to sell gold statues they made out of people.[P] He got the tar beaten out of him by vigilantes.[B][C]")
	Append("Mei:[E|Sad] But these rubber monsters seem so...[P] different...[B][C]")
	Append("Florentina:[E|Confused] Severe, sure, but not that unusual.[B][C]")
	Append("Florentina:[E|Neutral] There's always a perfect reasonable explanation, even if it's difficult to find.[B][C]")
	Append("Mei:[E|Sad] If you say so.[B][C]")
    if(iPolarisGavePhial == 0.0) then
        Append("Florentina:[E|Neutral] Might be a good idea to ask Polaris about it.[P] I think she mentioned she's dealt with things like this.[B][C]")
    elseif(iPolarisGavePhial == 1.0 and iReturnedPhial == 0.0) then
        Append("Florentina:[E|Neutral] If we help Polaris out, I'm sure it'll be fine.[P] There, my good deed for the year.[B][C]")
    else
        Append("Florentina:[E|Neutral] Just leave it in Polaris' hands.[P] If anyone can get to the bottom of this, it's her.[P] And then you don't need to worry about it.[B][C]")
    end

--Rubber victory.
else
	Append("Mei:[E|Neutral] What do you think of these rubber people?[B][C]")
	Append("Florentina:[E|Neutral] I think...[B][C]")
	Append("Florentina:[E|Neutral] Huh, I'm just sort of blanking.[B][C]")
	Append("Mei:[E|Neutral] The master wants us to leave the tower alone.[P] It is not time to spread.[B][C]")
	Append("Florentina:[E|Neutral] ...[B][C]")
	Append("Florentina:[E|Confused] Did you say something just now?[B][C]")
	Append("Mei:[E|Smirk] Did I?[P] I just sort of forgot this whole thing.[B][C]")

end
