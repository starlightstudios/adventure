-- |[Bees]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sMeiForm    = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local iHasBeeForm = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Mei is in bee form.
if(sMeiForm == "Bee") then
	Append("Mei:[E|Blush] Florentina, you are looking...[P] delicious today...[B][C]")
	Append("Florentina:[E|Surprise] Mei, if you're doing what I think you're doing...[B][C]")
	Append("Mei:[E|Neutral] Paying a compliment?[B][C]")
	Append("Florentina:[E|Happy] Subtlety is not your strong suit.[B][C]")
	Append("Mei:[E|Blush] I can smell your nectar - [P][CLEAR]")
	Append("Florentina:[E|Blush] Too forward, kid.[P] I do find it flattering, but not right now.[B][C]")
	Append("Mei:[E|Blush] I'll just enjoy your company...[B][C]")

--Mei is not in bee form, but has access to bee form.
elseif(iHasBeeForm == 1.0) then
	Append("Mei:[E|Blush] So, Florentina.[P] Have you ever been...[P] intimate...[P] with a bee?[B][C]")
	Append("Florentina:[E|Neutral] Why don't you ask your little hive friends?[B][C]")
	Append("Mei:[E|Neutral] I can't without my antennae.[B][C]")
	Append("Florentina:[E|Happy] You're hitting on me, and you're really bad at it.[B][C]")
	Append("Mei:[E|Blush] I wasn't -[P][CLEAR]")
	Append("Florentina:[E|Happy] You absolutely were.[B][C]")
	Append("Mei:[E|Blush] I can't help it...[P] I just want to drink your delicious nectar...[B][C]")
	Append("Florentina:[E|Happy] I'm flattered.[P] Really.[P] But I'm not in the mood, okay?[B][C]")
	Append("Mei:[E|Neutral] Okay, sure.[B][C]")
	Append("Mei:[E|Blush] (Just, gotta make sure she doesn't catch me drooling...)[B][C]")

--Normal case.
else
	Append("Mei:[E|Neutral] Where I'm from, we don't have bees this big.[B][C]")
	Append("Florentina:[E|Neutral] Count your blessings, then.[P] The only places on Pandemonium that bees *aren't* are...[B][C]")
	Append("Florentina:[E|Confused] ...[P] glaciers, I guess.[P] That's all I can think of.[P] I've heard that they can survive in deserts, badlands, jungles...[B][C]")
	Append("Mei:[E|Happy] I'm detecting a slight displeasure in your voice.[B][C]")
	Append("Florentina:[E|Neutral] In general, I've got nothing against them.[P] They just tend to pester us Alraunes when we're busy.[B][C]")
	Append("Mei:[E|Neutral] How so?[B][C]")
	Append("Florentina:[E|Blush] Do you know what bees do to plants?[B][C]")
	Append("Mei:[E|Neutral] Well they -[P][P][E|Surprise] Oh![P][E|Neutral] I get you.[B][C]")
	Append("Florentina:[E|Blush] They can be a lot of fun, but there's a lot of bees and they're always in the mood.[B][C]")
	Append("Mei:[E|Laugh] Reminds me of the guys back home![B][C]")
	Append("Florentina:[E|Neutral] Some things are universal.[P] If you're cute, someone's going to be bothering you.[B][C]")
	Append("Mei:[E|Neutral] Hear hear![B][C]")
end
