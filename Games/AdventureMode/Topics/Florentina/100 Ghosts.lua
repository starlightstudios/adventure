-- |[Ghosts]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sMeiForm      = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local iHasGhostForm = VM_GetVar("Root/Variables/Global/Mei/iHasGhostForm", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

--Mei is in ghost form.
if(sMeiForm == "Ghost" or iHasGhostForm == 1.0) then
	Append("Mei:[E|Neutral] Florentina -[P] have you ever seen a ghost before?[B][C]")
	Append("Florentina:[E|Neutral] In your usually unsubtle way, you're implying that Earth doesn't have these.[B][C]")
	Append("Mei:[E|Neutral] Oh -[P] no![P] We absolutely have ghosts![P] I've seen Youku videos that show them, absolutely![B][C]")
	Append("Mei:[E|Neutral] I was just wondering if this was your first time.[B][C]")
	Append("Florentina:[E|Neutral] Not really, no.[P] There are parts of Pandemonium just lousy with the undead.[B][C]")
	Append("Florentina:[E|Neutral] If you're looking for an exercise in futility, go spend some time trying to kill them.[P] You'll get really tired and probably depressed.[B][C]")
	Append("Mei:[E|Neutral] This kind of suffering is widespread?[B][C]")
	Append("Florentina:[E|Neutral] It's no different than being an Alraune, really.[B][C]")
	Append("Florentina:[E|Neutral] But Quantir.[P] Different.[P] That's all I can say.[B][C]")
	Append("Mei:[E|Neutral] ...[B][C]")

--Normal case.
else
	Append("Mei:[E|Neutral] Florentina -[P] have you ever seen a ghost before?[B][C]")
	Append("Florentina:[E|Neutral] In your usually unsubtle way, you're implying that Earth doesn't have these.[B][C]")
	Append("Mei:[E|Neutral] Oh -[P] no![P] We absolutely have ghosts![P] I've seen Youku videos that show them, absolutely![B][C]")
	Append("Mei:[E|Neutral] I was just wondering if this was your first time.[B][C]")
	Append("Florentina:[E|Neutral] Not really, no.[P] There are parts of Pandemonium just lousy with the undead.[B][C]")
	Append("Florentina:[E|Neutral] If you're looking for an exercise in futility, go spend some time trying to kill them.[P] You'll get really tired and probably depressed.[B][C]")
	Append("Mei:[E|Neutral] Surely there is something we can do?[P] We can't just let them suffer for eternity...[B][C]")
	Append("Florentina:[E|Neutral] Actually, the Draugr aren't really suffering.[P] Insufferable, sure, but suffering?[B][C]")
	Append("Florentina:[E|Neutral] Now -[P] Quantir.[P] Different.[P] That's all I can say.[B][C]")
	Append("Mei:[E|Neutral] I see...[B][C]")

end
