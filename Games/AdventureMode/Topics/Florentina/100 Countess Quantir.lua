-- |[Countess Quantir]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Florentina")

Append("Mei:[E|Neutral] Do you know when Countess Quantir was alive?[B][C]")
Append("Florentina:[E|Neutral] I'm not much of a historian, but I can safely say it was well before my time.[B][C]")
Append("Florentina:[E|Neutral] Quantir has always been an abandoned wasteland.[P] I guess she was responsible for that.[B][C]")
Append("Florentina:[E|Neutral] We're looking at centuries, at least.[B][C]")
Append("Mei:[E|Sad] She suffered in that room for centuries...[B][C]")
Append("Florentina:[E|Confused] Well deserved, too.[B][C]")
Append("Mei:[E|Sad] Yeah...[B][C]")
