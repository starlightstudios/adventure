-- |[ ===================================== Build Listing ====================================== ]|
--Builds a list of topics this NPC will respond to, and at what levels.
local sNPCName = "Izuna"
gsActiveNPCName = "Izuna"

--              ||||| Unlocked At Start |||  Filename      |||  Display Name (Optional)
fnConstructTopicStd(              true, "Sweetheart",    "Sweetheart")
fnConstructTopicStd(              true, "Journalism",    "Journalism")
fnConstructTopicStd(              true, "Lesbianism",    "Lesbianism")
fnConstructTopicStd(              true, "Miko",          "Miko")
fnConstructTopicStd(              true, "Tails",         "Tails")
