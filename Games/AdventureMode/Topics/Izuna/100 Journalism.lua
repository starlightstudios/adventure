-- |[ ==================================== Izuna: Journalism =================================== ]|
--Like, writing stuff down? Lame!

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Izuna")

--Common.
WD_SetProperty("Major Sequence Fast", true)

--Dialogue.
Append("Sanya:[E|Smirk] What's it mean to be a journalist here?[B][C]")
Append("Izuna:[E|Neutral] Do you not have journalists on Earth?[B][C]")
Append("Sanya:[E|Sad] We do, but they suck.[P] Sometimes you get a good one like Hersh or Jennings, but most of the time they are more motivated by politics than the truth.[B][C]")
Append("Sanya:[E|Neutral] I don't much like politics and stuff, and I just assume everyone is lying.[P] But everyone now and then...[B][C]")
Append("Izuna:[E|Neutral] Unfortunately, it seems you do have them on Earth, and they are the same as here.[B][C]")
Append("Izuna:[E|Explain] But not me![P] The truth is all that matters, and we kitsunes aim to change how people get their information![B][C]")
Append("Sanya:[E|Neutral] So what do you do, then?[P] How are you different?[B][C]")
Append("Izuna:[E|Smirk] It's not the person, it's the things around them.[P] A person who is motivated by prestige will write in a way that gains them prestige.[P] A person who is motivated by money will write in a way that gains them money.[P] This is the very basis of the kitsune teachings.[B][C]")
Append("Izuna:[E|Neutral] People are their environments.[P] Humans can adapt to anything, and that includes partirhumans like me.[B][C]")
Append("Zeke:[E|Neutral] Bah![B][C]")
Append("Izuna:[E|Smirk] No, Zeke.[P] Goats are completely neutral but you cannot be journalists because you can't write things down.[P] You need thumbs.[B][C]")
Append("Zeke:[E|Neutral] Bah...[B][C]")
Append("Izuna:[E|Smirk] We kitsunes are aware of our deficiencies and strive to remove them.[P] I do not need to worry about food or shelter, or even my own safety, so I can do my best to get to the truth.[B][C]")
Append("Sanya:[E|Offended] Safety!?[P] Do people threaten you for writing about them!?[B][C]")
Append("Izuna:[E|Explain] Of course![P] It's the best![B][C]")
Append("Izuna:[E|Explain] As soon as some shady jerk starts telling you not to dig, that's exactly when you know it's time to dig![B][C]")
Append("Sanya:[E|Blush] I love it![P] *smooch*[B][C]")
Append("Izuna:[E|Happy] Mmm![P] So the pursuit of truth makes you horny?[B][C]")
Append("Sanya:[E|Blush] Yeah![P] Wrecking bad people is my thing![P] Usually I do it by hitting them but you've got a cool way of doing it![B][C]")
Append("Sanya:[E|Neutral] It's odd, though.[P] A lot of people tell me that violence is always wrong.[P] You know, morally wrong.[B][C]")
Append("Izuna:[E|Smirk] Yes, that is another of the teachings.[P] And it's true, violence is wrong, but only if it is the first response, and not the last.[B][C]")
Append("Izuna:[E|Smirk] Violence is, ultimately, a part of existence.[P] So long as there are faces and fists, then those two things can be put together.[P] Like all things, we must use our cleverness to make sure the right fists are put into the right faces.[B][C]")
Append("Sanya:[E|Sad] Yeah...[B][C]")
Append("Izuna:[E|Cry] That sounds like something you would agree with.[P] Why are you sad?[B][C]")
Append("Sanya:[E|Sad] I didn't really think about who I hit, you know?[P] Like if I saw a kid bullying another kid I'd beat their ass.[B][C]")
Append("Sanya:[E|Sad] Uh, in this case I was like five years older, by the way.[P] I got yelled at a lot.[P] But kids didn't bully each other when I was around.[P] Did I make things worse?[B][C]")
Append("Izuna:[E|Neutral] Maybe you did, and maybe you didn't.[P] It's up to researchers and journalists, like me, to find out.[B][C]")
Append("Izuna:[E|Smirk] If you could limit your power by the wisdom of the kitsunes, then we could make the world a much better place, Sanya![P] Maybe you can contribute to that wisdom someday, too![B][C]")
Append("Sanya:[E|Happy] Hell no![P] I suck at writing and stuff![B][C]")
Append("Sanya:[E|Happy] I'll just help you chase down bad guys![P] I don't need to be the center of every story, right?[B][C]")
Append("Izuna:[E|Smirk] I suppose not.[P] An odd thing for the hero of legend to say, though.[B][C]")
Append("Sanya:[E|Smirk] Yeah but once I give Perfect Hatred a swirlie or whatever, I'm back to being Sanya.[P] I'd like to have something to do after that.[B][C]")
Append("Izuna:[E|Smirk] Great![P] Because believe me, there's a lot of bad guys out there who have power that doesn't involve force![P] Let's go take them all down![B][C]")