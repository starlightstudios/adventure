-- |[ =================================== Izuna: Lesbianism ==================================== ]|
--There are details needed.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Izuna")

--Common.
WD_SetProperty("Major Sequence Fast", true)

--Dialogue.
Append("Sanya:[E|Blush] Quick question for you.[P] What are girls who like girls supposed to like about girls?[B][C]")
Append("Izuna:[E|Ugh] Uhhhh...[B][C]")
Append("Izuna:[E|Ugh] What are girls who like boys supposed to like about boys?[B][C]")
Append("Izuna:[E|Smirk] Don't actually answer that, silly![P] What you like about someone is just what you like about them.[P] Their laugh, their smile, their hair, their dancing.[B][C]")
Append("Izuna:[E|Smirk] Their personality, their wit, their fluffy tail...[B][C]")
Append("Sanya:[E|Blush] Definitely yes on the tail.[B][C]")
Append("Izuna:[E|Happy] Mwehehe![P] It's fun to snuggle with at night.[P] Mine is just big enough![B][C]")
Append("Sanya:[E|Smirk] I ask because I'm new at this.[P] I've never seen a woman I wanted to be with.[P] Touch.[P] You know?[B][C]")
Append("Sanya:[E|Sad] I don't want to do it wrong.[B][C]")
Append("Izuna:[E|Smirk] I don't think you can do it wrong.[P] You just be honest and considerate, and work it out.[B][C]")
Append("Sanya:[E|Blush] Then, what do you like about me?[B][C]")
Append("Izuna:[E|Blush] You're so strong and firm, and I know I'm safe when I'm with you.[P] I like that.[B][C]")
Append("Sanya:[E|Neutral] Even when we're being chased by monsters, you're safe with me?[B][C]")
Append("Izuna:[E|Smirk] Adversity is inevitable.[P] I'd rather be chased by monsters with you, Sanya, than chased by monsters with anyone else.[B][C]")
Append("Sanya:[E|Happy] Me too, Izuna![P] Let's go get chased by monsters, together![B][C]")