-- |[ ======================================= Izuna: Miko ====================================== ]|
--All about healing.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Izuna")

--Common.
WD_SetProperty("Major Sequence Fast", true)

--Dialogue.
Append("Sanya:[E|Happy] Let's talk about fightin![B][C]")
Append("Izuna:[E|Smirk] Your favourite subject?[B][C]")
Append("Sanya:[E|Happy] One of them![B][C]")
Append("Sanya:[E|Smirk] But really.[P] What is a miko?[P] How do you fight?[P] What should I know about your skills?[B][C]")
Append("Izuna:[E|Smirk] The mikos are one of the orders of kitsunes.[P] I actually was a warrior once, but it just wasn't for me.[B][C]")
Append("Izuna:[E|Explain] Mikos are the healers of the kitsunes.[P] We tend to the sick, make medicines to cure or at least treat diseases, and practice magic that encourages good health.[B][C]")
Append("Izuna:[E|Explain] - As you can probably guess from my healing coma, which repaired my broken bones in just a month![P] We heal the wounded in battle.[B][C]")
Append("Sanya:[E|Happy] Great![P] Because making a cast is all I know about medicine![B][C]")
Append("Sanya:[E|Smirk] I took a first aid course once, and learned the rest from my grandma.[P] They couldn't afford doctors back in her day.[B][C]")
Append("Sanya:[E|Smirk] So you'll keep Zeke and I refreshed in battle?[B][C]")
Append("Izuna:[E|Smirk] Yep![P] I can also increase your stats, remove many negative effects like poisons, and place wards on enemies to seal their strength away.[B][C]")
Append("Sanya:[E|Smirk] What about actual damage infliction?[P] That sword of yours looks pretty blunt.[B][C]")
Append("Izuna:[E|Neutral] This is called a focusing sword.[P] It has a core of silver, and it focuses magical energy so I can release it.[B][C]")
Append("Izuna:[E|Neutral] Every miko has one, we use it to cast magic.[P] It's not meant to actually slash people, and I'm not much of a fighter anyway.[B][C]")
Append("Izuna:[E|Smirk] My ability to hurt the enemy is pretty minimal, it's just not what mikos train to do.[P] But I can help you and Zeke do that![B][C]")
    
