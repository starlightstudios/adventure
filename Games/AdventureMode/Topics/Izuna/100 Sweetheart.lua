-- |[ ==================================== Izuna: Sweetheart =================================== ]|
--Awww!

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Izuna")

--Common.
WD_SetProperty("Major Sequence Fast", true)

--Dialogue.
Append("Sanya:[E|Blush] So, we're going with sweetheart are we?[B][C]")
Append("Izuna:[E|Blush] Do you like it?[B][C]")
Append("Sanya:[E|Blush] You want to hear something my grandpa told me about once?[B][C]")
Append("Sanya:[E|Neutral] Back on Earth, the Indians have a story about a dude named Indra.[P] Very powerful god.[P] My grandpa loved eastern mythology.[B][C]")
Append("Sanya:[E|Neutral] He said if it wasn't for the wars, he wanted to go to school and learn more about that.[P] But the world tore itself apart.[B][C]")
Append("Izuna:[E|Smirk] He had a thirst for knowledge?[B][C]")
Append("Sanya:[E|Happy] I inherited his eyesight, not his booksmarts![B][C]")
Append("Sanya:[E|Smirk] But in the story, Indra gets mad at a king and turns him into a woman for something involving a fertility ritual.[B][C]")
Append("Sanya:[E|Smirk] And the king, now a woman, later gets the chance to turn back, but refuses.[B][C]")
Append("Sanya:[E|Smirk] She says it's because of the connection, emotionally, that women share is more whole and fulfilling than any other.[P] And also women enjoy fucking more.[B][C]")
Append("Izuna:[E|Neutral] Really?[P] Why don't men connect emotionally?[B][C]")
Append("Sanya:[E|Smirk] Maybe it's an Earth thing, but being a man means being tough.[P] Strong.[P] Not weak, and showing emotion is weak.[B][C]")
Append("Izuna:[E|Ugh] I see.[P] That is very common among the nobles, but it's not gender-aligned.[B][C]")
Append("Izuna:[E|Neutral] We don't have nobility here in Trafal, but the warrior kitsunes speak of the feudal kingdoms to the north and south.[B][C]")
Append("Izuna:[E|Neutral] Showing any sign of weakness makes the other lords and ladies pounce.[P] They fight all the time.[P] You have to be tough.[B][C]")
Append("Sanya:[E|Neutral] I'm plenty tough.[P] What I was getting at was, I like how you're so open.[P] Up-front.[P] Vulnerable with me.[B][C]")
Append("Sanya:[E|Sad] The men in my life never have been.[P] We treated one another like conquests.[P] It's obvious now that I meet someone who doesn't do it.[B][C]")
Append("Izuna:[E|Ugh] Maybe you just didn't meet the right man?[P] There are probably lots of dumb women out there, too.[B][C]")
Append("Sanya:[E|Sad] Maybe.[B][C]")
Append("Sanya:[E|Blush] So it's just a coincidence that I take a walk on the other side of the tracks and meet someone like you the first time out?[B][C]")
Append("Izuna:[E|Happy] The moniker of sweetheart could not be more appropriate![B][C]")
    
