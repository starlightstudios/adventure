-- |[ ====================================== Izuna: Tails ====================================== ]|
--Fluffy tail.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Izuna")

--Common.
WD_SetProperty("Major Sequence Fast", true)

--Dialogue.
Append("Sanya:[E|Neutral] Where I'm from, kitsunes have nine tails.[P] You have one.[B][C]")
Append("Izuna:[E|Neutral] Wait, you said you don't even have monstergirls on Earth.[P] But you have kitsunes?[B][C]")
Append("Sanya:[E|Neutral] Nah.[P] They're a mythical creature.[P] Japanese legend.[P] Not a real thing.[B][C]")
Append("Izuna:[E|Jot] Interesting.[P] We exist, but only as myths.[P] And they all have nine tails?[P] Oh, uh, mind if I write this down?[B][C]")
Append("Sanya:[E|Neutral] Go ahead.[P] Also they're spirits, not physical, but they can possess people.[B][C]")
Append("Izuna:[E|Ugh] Oh my![P] We certainly don't do that![B][C]")
Append("Izuna:[E|Blush] (Would that be hot?[P] Taking over Sanya's body and...)[B][C]")
Append("Izuna:[E|Neutral] *cough* No, a kitsune begins with one tail, like the one I have.[B][C]")
Append("Izuna:[E|Explain] A kitsune gains a new tail when the Fox Goddess bestows one upon them for learning a great truth.[B][C]")
Append("Sanya:[E|Neutral] A great truth?[P] That's kinda vague.[B][C]")
Append("Izuna:[E|Neutral] Because it's a very personal thing.[P] Every kitsune gets their tails differently, there is no right or wrong way to do it.[B][C]")
Append("Izuna:[E|Smirk] It is how the Fox Goddess shows us favour, tells us we are following her ways.[B][C]")
Append("Izuna:[E|Explain] A researcher might toil for years on a problem, only to solve it and not gain a new tail.[P] Then they think carefully about it.[B][C]")
Append("Izuna:[E|Explain] Were they researching to get that tail, or were they researching to help others?[P] And when they decide to pick up and start on the next big problem...[B][C]")
Append("Izuna:[E|Explain] There it is![P] Because the desire to help others is what is the real, important thing.[P] That is what the Fox Goddess teaches.[B][C]")
Append("Sanya:[E|Neutral] But are you trying to gain tails for some reason?[B][C]")
Append("Izuna:[E|Explain] It is said that a kitsune who has all nine tails will gain a personal audience with the Fox Goddess, where they may ask her a question.[P] Anything.[B][C]")
Append("Izuna:[E|Explain] The meaning of life.[P] The cure for all illnesses.[P] The best way to organize society.[P] Or maybe just a smoothie recipe that will blow your mind![P] Anything![B][C]")
Append("Sanya:[E|Happy] I'd ask her to armwrestle![P] Raaaa![B][C]")
Append("Izuna:[E|Smirk] Mwehehe![B][C]")
Append("Izuna:[E|Smirk] But no kitsune has ever had all nine of their tails, perhaps because that would require wisdom that only the Goddess herself possesses.[B][C]")
Append("Sanya:[E|Sad] Or maybe you only get the last tail...[P] in the afterlife.[B][C]")
Append("Izuna:[E|Smirk] You'd make a great kitsune, Sanya![B][C]")
