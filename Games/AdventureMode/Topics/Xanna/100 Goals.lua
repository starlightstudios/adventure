-- |[Goals]|
--Topic!
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Xanna")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
    Append("Florentina:[E|Neutral] I've always wondered.[P] Once you have the bad honey taken care of, what goal does the hive have?[B][C]")
    Append("Mei:[E|Offended] You could have just asked me that.[P] Do you think I don't know?[B][C]")
    Append("Florentina:[E|Neutral] Okay.[P] Mei, once you have - [B][C]")
    Append("Mei:[E|Offended] Funny.[P] Now you shall be punished.[B][C]")
    Append("Xanna:[E|BeeNeutral] Bzzz.[B][C]")
    Append("Mei:[E|Neutral] Bzzzz.[P] Bz.[B][C]")
    Append("Florentina:[E|Neutral] I take it you are answering the question.[B][C]")
    Append("Mei:[E|Laugh] Not so easy without me translating, is it?[B][C]")
    Append("Florentina:[E|Happy] Fine, fine, you win.[P] Now are you going to answer?[B][C]")
    Append("Xanna:[E|BeeNeutral] (The goal of every hive is to grow until we are large enough to be stable, defend ourselves, and establish a queen.)[B][C]")
    Append("Xanna:[E|BeeNeutral] (The queen is capable of independent speech, and can broker agreements with nearby settlements.[P] Then we can live in peace with them.)[B][C]")
    Append("Xanna:[E|BeeNeutral] (And maybe, eventually, we become large enough to splinter and send a group to establish another hive elsewhere.)[B][C]")
    Append("Florentina:[E|Neutral] So that explains it.[P] I've heard rumours of bee queens.[B][C]")
    Append("Mei:[E|Neutral] It requires hundreds, maybe thousands, of bees to support a queen.[P] It is quite a strain on the hive mind.[B][C]")
    Append("Florentina:[E|Neutral] Is the bee queen a leader?[P] Can she command the hive to do things?[B][C]")
    Append("Xanna:[E|BeeNeutral] (No.[P] Much like our smaller cousins, the queen works for the hive even though we support her.[P] It is just necessary to call her a queen so the monarchical humans accept her.)[B][C]")
    Append("Mei:[E|Laugh] On Earth, she might be a bee prime minister![B][C]")
    Append("Florentina:[E|Happy] Maybe, just maybe, I could do business with a bee queen...[P] Yes, this could be a big deal....[B][C]")
end
