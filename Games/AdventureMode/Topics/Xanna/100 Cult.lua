-- |[The Cult]|
--Topic!
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Xanna")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
    Append("Florentina:[E|Neutral] What exactly is the cult that has taken up residence in that decaying hulk on the lake?[B][C]")
    Append("Xanna:[E|BeeNeutral] (We, or rather they, pay homage to the great sodden peat.[P] Though really, it was just a group that collects rejects.)[B][C]")
    Append("Xanna:[E|BeeUpset] (Like me...)[B][C]")
    Append("Mei:[E|Smirk] You're not a reject.[B][C]")
    Append("Xanna:[E|BeeNeutral] (That is how they recruit new members.[P] People who are just not good at anything, they take them in.[P] Give them a home.)[B][C]")
    Append("Xanna:[E|BeeNeutral] (I did pay attention to the religious texts and sermons, but they were optional.[P] What goals the sister superior had, I don't know.)[B][C]")
    Append("Xanna:[E|BeeNeutral] (I know we gathered food, built beds, tended the gardens.[P] I learned a bit of magic, too.)[B][C]")
    Append("Florentina:[E|Neutral] Yeah, grunts don't need to know what the higher-ups plan to do with their work.[P] In fact, it's a bad idea for them to know.[P] They talk.[B][C]")
    Append("Florentina:[E|Neutral] They look slipshod, but the operation is run pretty well.[B][C]")
    Append("Mei:[E|Smirk] You have a new family now.[P] You're accepted for who you are.[B][C]")
    Append("Xanna:[E|BeeNeutral] (A drone?)[B][C]")
    Append("Mei:[E|Happy] A drone![B][C]")
    Append("Xanna:[E|BeeNeutral] Bzzz![B][C]")
end
