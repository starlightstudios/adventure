-- |[Build Listing]|
--Builds a list of topics this NPC will respond to, and at what levels. Called at Adventure Mode startup.
DiaHelper:fnSetActiveTopicNPCName("Xanna")

--                          ||||| Unlocked At Start |||  Filename      |||  Display Name (Optional)
DiaHelper:fnAutoConstructTopic(               true, "Zombees",       "Zombees")
DiaHelper:fnAutoConstructTopic(               true, "Cult",          "Cult")
DiaHelper:fnAutoConstructTopic(               true, "Goals",         "Goals")
DiaHelper:fnAutoConstructTopic(               true, "Pollination",   "Pollination")
