-- |[Pollination]|
--Topic!
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Xanna")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
    Append("Florentina:[E|Neutral] Just what does the hive think about pollinating.[P] You know, with an alraune.[B][C]")
    Append("Mei:[E|Smirk] Are you asking us out?[B][C]")
    Append("Florentina:[E|Blush] Hey now, don't get the wrong idea.[B][C]")
    Append("Xanna:[E|BeeNeutral] (You date one of us, you date all of us.[P] It's just how it works.)[B][C]")
    Append("Xanna:[E|BeeNeutral] (I'd like to do it, but my injuries need to heal first.[P] Besides, I've already experienced it, several times.)[B][C]")
    Append("Xanna:[E|BeeNeutral] (Whenever one of my sisters does it, I'm there.[P] It's so good...)[B][C]")
    Append("Florentina:[E|Blush] Heh, heh...[B][C]")
end
