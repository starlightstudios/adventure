-- |[Zombees]|
--Topic!
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Xanna")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
    Append("Xanna:[E|BeeNeutral] (Ah, I apologize again.[P] Florentina, I know Mei forgives me but...)[B][C]")
    Append("Florentina:[E|Neutral] Don't beat yourself up about it.[P] I've seen scarier stuff.[B][C]")
    Append("Mei:[E|Offended] Have you now?[B][C]")
    Append("Florentina:[E|Confused] Yes, actually.[P] Do not underestimate the evil that can be found in everyday banalities.[B][C]")
    Append("Florentina:[E|Neutral] However, I need to ask the hive, and you specifically.[P] What was the point of the zombees?[B][C]")
    Append("Xanna:[E|BeeNeutral] (To create an obedient slave workforce.[P] The bees use the hive mind to remain productive and do not question orders.[P] We are ideal workers.)[B][C]")
    Append("Xanna:[E|BeeNeutral] (I was instructed to hijack that power and use it to the sister superior's ends.)[B][C]")
    Append("Florentina:[E|Confused] And those ends were?[B][C]")
    Append("Xanna:[E|BeeUpset] (I am sorry.[P] I was not told why.)[B][C]")
    Append("Mei:[E|Smirk] I can confirm she isn't lying.[B][C]")
    Append("Xanna:[E|BeeNeutral] (It was my personal goal to gain the favour of the sister superior.[P] It was my idea to create the corruptive honey using a technique she showed me.)[B][C]")
    Append("Xanna:[E|BeeNeutral] (It had the intended effect.[P] When I brought her a hive of loyal drones, I would have been elevated for certain.)[B][C]")
    Append("Xanna:[E|BeeNeutral] (It was petty, small-minded, and vain.[P] Individuals are too concerned with status, I see that now.)[B][C]")
    Append("Florentina:[E|Confused] I'm going to ignore that little slight.[B][C]")
    Append("Florentina:[E|Neutral] Okay, Mei.[P] So the puzzle is now why the cultists need a workforce like that.[P] Any ideas?[B][C]")
    Append("Mei:[E|Sad] None.[B][C]")
    Append("Mei:[E|Neutral] It can't be good, whatever it is.[B][C]")
    Append("Xanna:[E|BeeNeutral] (The hive wants to keep its distance from the Dimensional Trap, but my small voice desires to return and convert all my former friends.)[B][C]")
    Append("Xanna:[E|BeeNeutral] (The sister superior uses them to her own ends.[P] I think they would be happier as bees.)[B][C]")
    Append("Mei:[E|Happy] As do I![B][C]")
    Append("Mei:[E|Smirk] Oh, maybe I just think that because you're thinking it so strongly![B][C]")
end
