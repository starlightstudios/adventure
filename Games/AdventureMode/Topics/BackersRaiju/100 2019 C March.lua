-- |[Backers]|
--Backer terminal.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "BackersRaiju")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Dialogue.
	Append("Christine:[E|Neutral] So...[P] Unit #901157 'Klaysee' and Unit #902399 'Kumquat' had a public debate.[P] The topic was 'Slimes or Drones::[P] Which is dumber?'[B][C]")
	Append("Christine:[E|Neutral] It seems that Klaysee made some good points about slime anatomy while Kumquat argued that drones are stupid on purpose and could be made stupider if necessary.[B][C]")
	Append("Christine:[E|Neutral] Unit #907723 'Dyamonde' proposed that slimes adapt their environments while drones are manufactured...[B][C]")
	Append("Christine:[E|Neutral] Unit #900102 'RepeatedMeme' proposed a competition to resolve the issue, pitting slimes against drones in a series of contests, mostly involving wrestling...[B][C]")
	Append("Christine:[E|Neutral] Unit #894551 'James Upton' volunteered to film the competition, as did Unit #917114 'MarioneTTe'.[B][C]")
	Append("Christine:[E|Neutral] Unit #901922 'Austin Durbin', who works in abductions, reported that collecting the slimes for the wrestling match would be easy...[B][C]")
	Append("Christine:[E|Neutral] Unit #966403 'Gaming Chocobro' even offered to do the collecting personally...[B][C]")
	Append("Christine:[E|Neutral] Unit #993012 'Abrissgurke' volunteered to train the team of drone units...[B][C]")
	Append("Christine:[E|Neutral] Unit #911102 'Taedas' offered to referee, and Unit #882301 'Sudoku' began writing a program to track score...[B][C]")
	Append("Christine:[E|Neutral] And then Unit 870563 'Goop Sinpai' reminded everyone that this wasn't about a wrestling match at all.[B][C]")
	Append("Christine:[E|Laugh] ...[P] So it seems there will be a slime/latex wrestling match to look forward to![B][C]")
end
