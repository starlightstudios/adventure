-- |[Backers]|
--Backer terminal.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "BackersRaiju")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Dialogue.
	Append("Christine:[E|Neutral] Okay.[P] This month, the slime/latex drone wrestling match took place.[P] The teams were 'Team Klaysee' and 'Team Kumquat'.[B][C]")
	Append("Christine:[E|Neutral] The team members had been given names based on who was sponsoring them.[P] On the slime team, there was 'Dyamonde', 'Upton', 'Durbin', and 'Namapo'.[B][C]")
	Append("Christine:[E|Neutral] On the Drone Unit team, there was 'RepeatedMeme', 'Sudoku', 'Taedas', and 'Abrissgurke'.[B][C]")
	Append("Christine:[E|Neutral] There was also 'Team Sinpai' because of confusion as to the rules.[P] Team Sinpai consisted of a very confused Alraune from Biological Services and a Slave Golem from Sector 44.[B][C]")
	Append("Christine:[E|Neutral] After seventeen rounds of wrestling, sliming, and general chaos, the winner was...[B][C]")
	Append("Christine:[E|Smirk] Team Sinpai![P] Despite not actually participating.[B][C]")
	Append("Christine:[E|Laugh] ...[P] Because both team names started with 'K' and the scorekeeper logged points to the wrong team.[P] So Team Sinpai won by default.[B][C]")
	Append("Christine:[E|Smirk] A total mess, but none of the audience members logged a single complaint, so well done everyone![B][C]")
end
