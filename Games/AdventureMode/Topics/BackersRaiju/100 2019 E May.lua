-- |[Backers]|
--Backer terminal.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "BackersRaiju")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Dialogue.
	Append("Christine:[E|Neutral] In May, the annual Biolabs Treasure Hunt took place.[P] Let's see...[B][C]")
	Append("Christine:[E|Neutral] Unit #901157 'Klaysee' took third place.[P] She hadn't been informed of the treasure hunt but kept finding the caches as she was working on her personal garden.[B][C]")
	Append("Christine:[E|Neutral] Unit #902399 'Kumquat' was arrested by security services after a misunderstanding involving the wine barrels at the distillery and the rules of the treasure hunt.[B][C]")
	Append("Christine:[E|Neutral] Unit #882301 'Sudoku' took first place when she outsmarted one of the other contestants and...[P] 'borrowed' her treasure caches.[B][C]")
	Append("Christine:[E|Neutral] Unit #894551 'James Upton' found over six-hundred caches only to be later informed they were, in fact, chicken eggs.[B][C]")
	Append("Christine:[E|Neutral] Unit #901922 'Austin Durbin' made a very large number of omelettes which were very popular with the breeding program humans.[B][C]")
	Append("Christine:[E|Neutral] Unit #870563 'Goop Sinpai' got two Drone Units to do most of the work for her, only to lose everything when someone else asked the Drone Units to give her the treasure caches.[B][C]")
	Append("Christine:[E|Neutral] Unit #993012 'Abrissgurke' realized the real treasure was the friends she made along the way.[B][C]")
	Append("Christine:[E|Neutral] Unit #874536 'Namapo' realized the real treasure was the treasure, which is why she scored better than Abrissgurke...[B][C]")
	Append("Christine:[E|Neutral] Unit #911102 'Taedas' built an elaborate detector array and knew the exact position of every treasure cache, just in time for the contest to end.[B][C]")
	Append("Christine:[E|Neutral] Unit #900102 'RepeatedMeme' couldn't remember where the caches were despite having been assigned to hiding them, and took fourth place despite a massively unfair advantage.[B][C]")
	Append("Christine:[E|Neutral] And Unit #907723 'Dyamonde' took third place by eating three whole watermelons by herself without using her hands.[P] She didn't find any treasure but the judges declared that so awesome that it counted.[B][C]")
	Append("Christine:[E|Smirk] ...[P] Yes that all really happened...[B][C]")
end
