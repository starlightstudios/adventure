-- |[Backers]|
--Backer terminal.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "BackersRaiju")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Dialogue.
	Append("Christine:[E|Smirk] Well look at this::[P] They held a fun fair in February![B][C]")
	Append("Christine:[E|Smirk] They even invited slave units![P] Apparently, Unit #901157 'Klaysee' and Unit #907723 'Dyamonde' organized it with help from #900102 'RepeatedMeme'.[B][C]")
	Append("Christine:[E|Smirk] There was a footrace, and Unit #894551 'James Upton' came in first.[P] Dead last was #917114 'MarioneTTe'.[B][C]")
	Append("Christine:[E|Smirk] And apparently #901922 'Austin Durbin' won a lot of work credits by betting against Unit 917114...[B][C]")
	Append("Christine:[E|Smirk] #966403 'Gaming Chocobro' won the caber toss (and apparently always does...)[B][C]")
	Append("Christine:[E|Smirk] Unit #993012 'Abrissgurke' spiked the punch and got all the humans drunks (and apparently always does...)[B][C]")
	Append("Christine:[E|Smirk] #912792 'Christian Gross' managed to eat 150 units of cake at the cake-eating contest (and apparently always does...)[B][C]")
	Append("Christine:[E|Smirk] And there was a concert and a dance off, with Team Raiju winning by a landslide![P] Wow![P] I'm sad I missed it![B][C]")
end
