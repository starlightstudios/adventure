-- |[Backers]|
--Backer terminal.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "BackersRaiju")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Dialogue.
	Append("Christine:[E|Neutral] As of June...[P] Doctor Maisie had to fix up a certain unit due to a sialolith forming in her salivary glands followed by a nasty infection...[B][C]")
	Append("Christine:[E|Neutral] In solidarity, Unit #901157 'Klaysee' decided to hold a fundraiser.[P] The theme was 'Dance Until you Drop'.[B][C]")
	Append("Christine:[E|Neutral] Unit #902399 'Kumquat' did the 'Electropop Boogie Swing' for fifteen minutes until being forcibly removed from the stage because the electrical discharges were shocking nearby viewers.[B][C]")
	Append("Christine:[E|Neutral] Unit #907723 'Dyamonde' did 'The Magpie', which is a dance involving taking things out of other units' pockets.[P] She executed it so flawlessly that nobody saw her for weeks afterwards to congratulate her.[B][C]")
	Append("Christine:[E|Neutral] Unit #894551 'James Upton' didn't know how to dance but gave twenty work credits to everyone else who did.[P] What a hero![B][C]")
	Append("Christine:[E|Neutral] Unit #882301 'Sudoku' performed a profound version of 'Sit There and State Angrily', rumoured to be one of the most difficult dances to perform.[P] The audience was moved.[B][C]")
	Append("Christine:[E|Neutral] Unit #870563 'Goop Sinpai' did the walk.[P] She did the walk all night.[B][C]")
	Append("Christine:[E|Neutral] Unit #993012 'Abrissgurke' formed a pyramid with several other Lord Golems.[P] It wasn't technically dancing but lots of work credits were still raised.[B][C]")
	Append("Christine:[E|Neutral] Unit #901922 'Austin Durbin' spun around for twenty minutes, performing an incredible show of breakdancing, before it was determined that her leg motivators had malfunctioned and she couldn't stop herself.[B][C]")
	Append("Christine:[E|Neutral] Unit #911102 'Taedas' fell on her face and got really hurt.[P] However, this was the elaborate dance 'Ow ow oops oh no' and was in fact carefully rehearsed.[P] The audience burst into applause.[B][C]")
	Append("Christine:[E|Neutral] Unit #874536 'Namapo' posed magnificently, causing onlookers to swoon.[B][C]")
	Append("Christine:[E|Neutral] Unit #900102 'RepeatedMeme' spent most of the contest cleaning the floor of the mangled remains of those who tried to dance outside their competencies.[P] She was the real hero.[B][C]")
	Append("Christine:[E|Smirk] After the fundraiser, Klaysee gave all of the funds to the clinic here in the Raiju Ranch. Hopefully some renovations will give them more space soon![B][C]")
end
