-- |[ =================================== Angelface: Goodbye =================================== ]|
--Script that fires when the player says goodbye to end the dialogue.
WD_SetProperty("Major Sequence Fast", true)

--Standard.
fnCutscene([[ Append("Angelface:[E|Neutral] Yeah, I was done with you anyway.") ]])

--Restore the music.
AL_SetProperty("Music", "Choking Clouds")
