-- |[ ================================= Angelface: Bunny Boys ================================== ]|
--Asking about the monastery.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Angelface")
WD_SetProperty("Unlock Topic", "Smugglers", 1)
WD_SetProperty("Unlock Topic", "Bunny Story", 1)

-- |[Dialogue]|
--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
    --Dialogue.
    fnCutscene([[ Append("Izuna:[E|Jot] Miss Angelface, what gave you the idea to have a harem of bunny boys?[B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] Am I givin' an interview now?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Blush] ...[P] Not if you don't want to.[B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] It's funny.[P] Nobody ever asked me a question like that.[P] Maybe the appeal is obvious?[B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] They're my boys.[P] All of 'em.[P] You harm a hair on their fuzzy heads, I'll kill you.[P] I'm possessive.[B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] I guess making them bunnies is my way of marking my turf.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Jot] I suppose so.[P] But the idea of using transformative magic specifically in this way?[B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] It ain't a big idea of mine.[P] I find it hard to believe nobody else has done this.[P] Then again, nobody else likes lean, taut, bunny boys.[B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] I got to getting with a particularly cute guy some years ago, we did the tango.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Jot] ...[P] tango...[P] euphemism...[B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] He starting turning, but we finish right quick.[P] Very quick.[P] He's ashamed, but he's also got bunny ears.[P] So we go again.[B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] Turns out it's not the finishing or anything, it really does take a certain amount of magic.[P] You hold the magic back, or you direct it elsewhere, and bam.[P] No transformation.[P] You can stop it.[B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] So we keep going, because if you've got the stamina, so do I.[P] He's still a bunny boy after a few hours.[P] I decided I like that.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Jot] Amazing.[P] I wonder if other monstergirls can do something similar?[B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] Never asked.[P] You'd have to find out on your own.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] I never knew journalism had so many questions about fucking.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Blush] ...[P] It's at least a quarter of my job, honestly.[P] Luckily Mia doesn't mind doing all the editing around it...[B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] What, you're pulling your punches on the horny stuff?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] Not quite![P] We just have an internal policy.[P] No more than ten percent of our stories can be about screwing![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] ...[P] Because the first few pamphlets were entirely that.[P] It probably didn't help our reputation.[B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] That's how the world is.[P] Warts and all.[B][C]") ]])
    
end
