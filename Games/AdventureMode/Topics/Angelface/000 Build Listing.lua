-- |[ ===================================== Build Listing ====================================== ]|
--Builds a list of topics this NPC will respond to, and at what levels.
local sNPCName = "Angelface"

--Constants
local gciAvailableAtStart = 1
local gciMustBeUnlocked = -1

--Topic listing.
fnConstructTopic("Bunny Boys",  "Bunny Boys",  gciAvailableAtStart, sNPCName, 0)
fnConstructTopic("Harpy Offer", "Harpy Offer", gciMustBeUnlocked,   sNPCName, 0)
