-- |[ ================================= Angelface: Harpy Offer ================================= ]|
--Esmerelda's trade deal.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Dialogue]|
--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Variables.
    local iGaveBunnyClearance = VM_GetVar("Root/Variables/Chapter2/BunnyWarrens/iGaveBunnyClearance", "N")
    local sSanyaForm          = VM_GetVar("Root/Variables/Global/Sanya/sForm", "S")
    local iEmpressJoined      = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
    
    --First time:
    if(iGaveBunnyClearance == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter2/BunnyWarrens/iGaveBunnyClearance", "N", 1.0)
        
        --Dialogue.
        fnCutscene([[ Append("Angelface:[E|Neutral] So what did the featherbrains say?[B][C]") ]])
        if(sSanyaForm == "Harpy") then
            fnCutscene([[ Append("Sanya:[E|Offended] Featherbrains!?[B][C]") ]])
            fnCutscene([[ Append("Angelface:[E|Neutral] Yes, that is what I said.[P] Are you going to do something about it?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Offended] Your feet pads look dumb![B][C]") ]])
            fnCutscene([[ Append("Angelface:[E|Neutral] Probably.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Offended] Damn it![P] React by getting sad![P] Take offense![B][C]") ]])
            fnCutscene([[ Append("Angelface:[E|Neutral] No.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Neutral] This one has mastered her inner turmoil.[P] The kitsunes strive to be like you, Angelface.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] Criminy, now I gotta master my inner turmoil...[P] Being a hero sucks![B][C]") ]])
            fnCutscene([[ Append("Angelface:[E|Neutral] Yeah, I bet it does.[P] So what'd the harpy's have to say?[B][C]") ]])
        else
            fnCutscene([[ Append("Sanya:[E|Offended] Don't call them featherbrains, they're cool as hell![B][C]") ]])
            fnCutscene([[ Append("Angelface:[E|Neutral] Whatever.[P] What'd they say?[B][C]") ]])
        end    
        fnCutscene([[ Append("Izuna:[E|Jot] I wrote it down.[P] Here.[B][C]") ]])
        fnCutscene([[ Append("Angelface:[E|Neutral] No weapon smuggling, consent to searches when moving south...[P] You told them we're not running weapons, right?[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Explain] They didn't believe you![B][C]") ]])
        fnCutscene([[ Append("Angelface:[E|Neutral] I guess it doesn't matter.[P] Fine, they can search the boys going south if they want.[P] If they catch any of them with weapons, that's on them.[B][C]") ]])
        fnCutscene([[ Append("Angelface:[E|Neutral] Hell, I'd ask her to let me know whose running firearms and swords without me knowing.[P] I'd slap them around myself.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Happy] We've got a deal!?[B][C]") ]])
        fnCutscene([[ Append("Angelface:[E|Neutral] Yeah, sure.[P] I guess you actually came through for me.[P] Maybe you're worth a good god damn.[B][C]") ]])
        fnCutscene([[ Append("Angelface:[E|Neutral] You still can't join my gang, but I guess I ought to bunny you.[P] If just to keep you the hell off my property.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Happy] So what's the next step?[B][C]") ]])
        fnCutscene([[ Append("Angelface:[E|Neutral] The rest of you, especially the goat, get lost.[P] You.[P] Drop your trousers.") ]])
        fnCutsceneBlocker()

        -- |[Call Subscene]|
        fnCutsceneWait(45)
        fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
        fnCutsceneBlocker()
        
        --While blacked out, call the harpy volunteer scene.
        LM_ExecuteScript(gsRoot .. "Chapter 2/Scenes/400 Volunteer/Bunny/Scene_Begin.lua")
        
        --Switch to Harpy form.
        fnCutsceneCall(gsRoot .. "FormHandlers/Sanya/Form_Bunny.lua")
    
        -- |[Movement]|
        fnCutsceneTeleport("Sanya", 57.25, 11.50)
        fnCutsceneFace("Sanya", -1, 0)
        fnCutsceneTeleport("Angelface", 56.25, 11.50)
        fnCutsceneFace("Angelface", 1, 0)
        fnCutsceneTeleport("Izuna", 58.25, 15.50)
        fnCutsceneFace("Izuna", 1, 0)
        fnCutsceneTeleport("Zeke", 59.25, 15.50)
        fnCutsceneFace("Zeke", -1, 0)
        fnCutsceneTeleport("CrowbarChan", 58.25, 16.50)
        fnCutsceneFace("CrowbarChan", 0, -1)
        if(iEmpressJoined == 1.0) then
            fnCutsceneTeleport("Empress", 59.25, 16.50)
            fnCutsceneFace("Empress", -1, 0)
        end
        
        --Wait a bit.
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        
        --Fade in.
        fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
        fnCutsceneBlocker()
        
        -- |[Dialogue]|
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Angelface", "Neutral") ]])
        fnCutscene([[ Append("Angelface:[E|Neutral] Job done. Now beat it.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Laugh] What, no aftercare?[B][C]") ]])
        fnCutscene([[ Append("Angelface:[E|Neutral] Me telling you to beat it [P]*was*[P] the aftercare.[B][C]") ]])
        fnCutscene([[ Append("Angelface:[E|Neutral] Now I will say I enjoyed that a little.[P] Wasn't as bad as I thought.[P] But you are already crimping my style.[B][C]") ]])
        fnCutscene([[ Append("Angelface:[E|Neutral] If any of the boys start talking up this other bunnygirl they saw, I'll break your legs.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Surprised] Come on, can we at least come to a gentlebuns agreement?[B][C]") ]])
        fnCutscene([[ Append("Angelface:[E|Neutral] And what are you proposing?[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] You can have all the bunny boys...[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Happy] But any bunnygirls?[P] I get those![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Blush] (Embrace the gay, I suppose...)[B][C]") ]])
        fnCutscene([[ Append("Angelface:[E|Neutral] Deal.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Great.[P] See you later.[B][C]") ]])
        fnCutscene([[ Append("Angelface:[E|Neutral] I hope not.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        
        -- |[Movement]|
        fnCutsceneMove("Sanya", 57.25, 15.50)
        fnCutsceneFace("Sanya", 1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("Izuna", -1, 0)
        
        -- |[Dialogue]|
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "CrowbarChanMob", "Neutral") ]])
        fnCutscene([[ Append("Sanya:[E|Sad] Man, she's cold.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Happy] S-[P]Sanya![P] Goddess' whiskers, you're so -[P] cute![P][B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Explain] But just a tiny bit dangerous![P] A little rough around the edges, with a heart of gold![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Blush] Thanks, babe.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Sad] It's just a shame all my Bugs Bunny jokes are going to land like a Korean airliner in a Gladwell book.[B][C]") ]])
        fnCutscene([[ Append("CC:[E|Neutral] I got that joke![P] I got that one![B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Happy] I didn't at all![B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
        if(iEmpressJoined == 1.0) then
            fnCutscene([[ Append("Empress:[E|Neutral] It sounded crass, which was the point.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Smirk] You know it.[P] Anyway, let's roll.[P] CC?[B][C]") ]])
        else
            fnCutscene([[ Append("Sanya:[E|Smirk] Anyway, let's roll.[P] CC?[B][C]") ]])
        end
        fnCutscene([[ Append("CC:[E|Neutral] What's up?[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Blush] It was great seeing you again, but do text a girl every now and then, okay?[B][C]") ]])
        fnCutscene([[ Append("CC:[E|Neutral] ...[P] You lost your phone.[P] Remember?[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Angry] ...[P] FUCK I FORGOT.[B][C]") ]])
        fnCutscene([[ Append("CC:[E|Neutral] Oh ho ho ho![P] Go get 'em you big lug!") ]])
        fnCutsceneBlocker()
    
        -- |[Finish Up]|
        fnAutoFoldParty()
        fnCutsceneBlocker()
        
        -- |[Music]|
        fnCutscene([[ AL_SetProperty("Music", "Choking Clouds") ]])
    
    --Repeats.
    else
        WD_SetProperty("Activate Topics After Dialogue", "Angelface")   
        fnCutscene([[ Append("Angelface:[E|Neutral] Let's hope the harpies keep their end of the bargain.[P] I should go over there and flesh out the details.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Neutral] You don't think they will?[B][C]") ]])
        fnCutscene([[ Append("Angelface:[E|Neutral] No, I don't think they have to.[P] Armies can do whatever they want, that's the problem.[B][C]") ]])
        fnCutscene([[ Append("Angelface:[E|Neutral] A smart businessbunny like myself knows how to keep out of their way.[P] Unfortunately, I happen to be in it by circumstance.[B][C]") ]])
        fnCutscene([[ Append("Angelface:[E|Neutral] So I'm making sure I get out of it.[P] I don't owe the bandits but two things -[P] Jack and shit.[P] And Jack left town.[B][C]") ]])
        fnCutscene([[ Append("Angelface:[E|Neutral] So I make a deal, sure, but what do I do if they don't keep it?[P] Fight them?[P] Cut them off?[P] You can buy booze anywhere, but I can't hire a lot of soldiers.[B][C]") ]])
        fnCutscene([[ Append("Angelface:[E|Neutral] You can't believe someone who doesn't need to be believed, and power always goes to someone's head.[P] The best strategy is to be unimpressive.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Neutral] It is an old kitsune saying.[P] 'The tall grass gets cut'.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Yeah but the kitsunes aren't smuggling.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Smirk] To a king, the ideas we bring are more dangerous than whatever weapons Angelface could procure.[B][C]") ]])
        fnCutscene([[ Append("Angelface:[E|Neutral] I can't sell the kitsunes texts, so I'll stick to booze, drugs, and fancy cakes.[B][C]") ]])
    end
end
