-- |[Cleansing Fungus]|
--Concerns the fungus that Alraunes use to forget their old lives.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Rochea")

--Variables.
local iIsMeiForeigner = VM_GetVar("Root/Variables/Chapter1/Alraunes/iIsMeiForeigner", "N")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	WD_SetProperty("Major Sequence Fast", true)
	Append("Mei: Could you tell me a little more about the cleansing fungus?[B][C]")
	Append("Rochea: Ah, yes, the fungus.[P] It is a gift that nature has given her followers.[B][C]")
    if(iIsMeiForeigner == 0.0) then
        Append("Rochea: When you are ready, we will prepare a special chamber where the fungus will grow.[P] You will stay in it for three full days.[B][C]")
        Append("Rochea: The spores will absorb your hatred, your malice, the things which make up your humanity.[P] When you breathe them in, they will purify you.[B][C]")
        Append("Mei:[E|Blush] My memories? Will they be gone too?[B][C]")
        Append("Rochea: ...[P] Yes.[P] You have left your humanity behind physically, but not mentally.[B][C]")
        Append("Rochea:[EMOTION|Mei|Neutral] If you are not ready yet, we will wait until you are.[B][C]")
    else
        Append("Rochea: I must profess confusion.[P] Are you not aware of the fungus?[B][C]")
        Append("Mei:[E|Neutral] I've heard of it, but never took it myself.[P] It's...[P] not a thing in Hong Kong.[B][C]")
        Append("Rochea: Oh.[P] I have heard there are places it cannot grow, as it is a symbiotic fungus.[P] Is Hong Kong a very hot place?[B][C]")
        Append("Mei:[E|Laugh] It gets pretty hot in summer![P] It's right by the ocean, too.[B][C]")
        Append("Rochea: Maybe that is the cause.[P] Well...[B][C]")
        Append("Rochea: The cleansing fungus has the effect on an alraune where it absorbs your hatred, your malice, and all other things that make you a human.[B][C]")
        Append("Rochea: After three days, you do not remember your name, your memories, or anything else that made you an animal.[P] You are truly a plant, a daughter of nature.[B][C]")
        Append("Rochea: Some who choose not to partake in the fungus are still alraunes, and we still love them as our leaf-sisters, but they are not truly of us.[B][C]")
        Append("Mei:[E|Neutral] You don't accept them in your coven?[B][C]")
        Append("Rochea: We do, if you're interested![P] But I meant, their judgement is always soured by their lingering attachment to their humanity.[P] Their former families, and lives.[B][C]")
        Append("Rochea: Thus, their council is not called for when we must make a decision.[P] It seems harsh, but it is for the best.[B][C]")
        Append("Mei:[E|Neutral] I see.[P] I suppose it is your way, and I should accept our differences.[B][C]")
        Append("Mei:[E|Neutral] If it's all the same to you, I'll pass on the fungus.[P] We can still be friends.[B][C]")
        Append("Rochea: Of course.[P] If you change your mind and wish to stay with us, we would be honored.[B][C]")
    end
end
