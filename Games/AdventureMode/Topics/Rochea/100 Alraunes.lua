-- |[Alraunes]|
--Asking this NPC about Alraunes in general.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Rochea")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Variables:
	local iMeiVolunteeredToAlraune = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiVolunteeredToAlraune", "N")
	
	WD_SetProperty("Major Sequence Fast", true)
	Append("Mei: What does it mean to be an Alraune?[P] What is our purpose?[B][C]")
	Append("Rochea: A good question![P] When you were a human, were you certain of your purpose?[B][C]")
	Append("Mei:[E|Blush] Ah, well, I suppose not.[B][C]")
	Append("Rochea: I'm afraid I can give no answers on such deep subjects.[P] I can tell you that our way is one of peace and harmony.[B][C]")
	
	if(iMeiVolunteeredToAlraune == 0.0) then
		Append("Mei:[E|Neutral] Is that so?[P] I seem to recall you attacking me.[B][C]")
		Append("Mei:[E|Happy] Not that it's a problem.[P] I hold no grudges.[P] You were in the right.[B][C]")
		Append("Rochea: Unfortunately, there are many humans who view us and nature herself with utter contempt.[P] Reasoning with them failed, so we resort to violence now.[P] I apologize that it came to that.[B][C]")
		Append("Mei:[E|Neutral] I understand.[P] If all humans became leaf-sisters, there would be no need for violence.[B][C]")
		Append("Rochea: Then you understand our philosophy with clarity.[P] Good.[B][C]")
	else
		Append("Mei:[E|Sad] If only the humans agreed...[B][C]")
		Append("Rochea: It is a shame that so many humans view us with contempt.[P] Reasoning with them has failed, so we resort to violence now.[B][C]")
		Append("Mei:[E|Neutral] I understand completely.[P] I would join every one of them if I could.[P] There would be no need for violence, then.[B][C]")
		Append("Mei:[E|Blush] Perhaps, someday, we will reach that unity...[B][C]")
		Append("Rochea: Then you understand our philosophy with clarity.[P] Good.[B][C]")
	end
end
