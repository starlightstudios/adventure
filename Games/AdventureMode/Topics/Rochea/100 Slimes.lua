-- |[Slimes]|
--Asking Rochea about slimes.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Rochea")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Standard.
	WD_SetProperty("Major Sequence Fast", true)
	Append("Mei:[E|Neutral] The slimes I've seen in the forest.[P] Are they like us?[B][C]")
	Append("Rochea: No, they are closer to animals than plants in most ways.[P] They hunt for wounded or unsuspecting creatures and consume them.[B][C]")
	Append("Rochea: They are more like carrion feeders in most cases, and not much of a threat.[B][C]")
	Append("Mei:[E|Neutral] What of the ones that are humanoids?[P] They are certainly dangerous.[B][C]")
	Append("Rochea: Oh, I see you've encountered one in heat.[P] I suppose we are nearing mating season for them.[B][C]")
	Append("Mei:[E|Blush] In h-h-heat?[P] Really?[B][C]")
	Append("Rochea: Yes, slimes take on human form when they seek to reproduce.[P] They then look for humans to convert into new slimes, like them.[P] Otherwise, they rarely attack humans.[B][C]")
	Append("Rochea: Slimes are not particularly intelligent, and may confuse us for humans sometimes.[P] Do not worry, merely give them a few raps on the membrane and they should leave you be.[B][C]")
	
	--Dialogue changes slightly if Mei has the slime TF.
	local iHasSlimeForm = VM_GetVar("Root/Variables/Global/Mei/iHasSlimeForm", "N")
	if(iHasSlimeForm == 0) then
		Append("Mei:[E|Happy] I'll try to be gentle.[B][C]")
	else
		Append("Mei:[E|Blush] (It sought to mate with me...[P]  mmmm...)[B][C]")
		Append("Rochea: Are you all right, leaf-sister?[B][C]")
		Append("Mei:[E|Neutral] Hm?[P] Oh, yes.[P] Just, lost in thought.[B][C]")
	end
end
