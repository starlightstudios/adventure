-- |[Name]|
--Asking this NPC about their name. A lot of NPCs have this. Sometimes it's asking them what their name is, sometimes
-- it's asking them what their name means.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Rochea")

--Variables.
local iIsMeiForeigner = VM_GetVar("Root/Variables/Chapter1/Alraunes/iIsMeiForeigner", "N")

--Dialogue.
WD_SetProperty("Major Sequence Fast", true)
Append("Mei: Your name, Rochea.[P] Was it yours before you became a leaf-sister?[B][C]")
Append("Rochea: No, I do not remember my name before then.[P] It is a name I took afterwards.[B][C]")
if(iIsMeiForeigner == 0.0) then
    Append("Mei:[E|Sad] When I consume the cleansing fungus...[B][C]")
    Append("Rochea: Your own name will be as foreign to you as mine was to me.[P] We will choose a new name as befits you.[B][C]")
    Append("Rochea: Apprehension is common.[P] If it comforts you, know that all of us felt it before we consumed the fungus.[B][C]")
    Append("Mei:[E|Neutral] It does help, thank you.[B][C]")
else
    Append("Mei:[E|Smirk] You know, I never took the cleansing fungus.[P] Mei is my original name.[B][C]")
    Append("Rochea: That is quite clear from your mannerisms.[P] No offense.[B][C]")
    Append("Mei:[E|Sad] Oh dear.[P] Am I being rude again?[B][C]")
    Append("Rochea: No, no.[P] When we take the cleansing fungus, we forget ourselves.[P] Our identities, our names.[B][C]")
    Append("Rochea: The first thing we do is call out, and the little ones answer.[P] So from the first moment, they are our companions and guides.[B][C]")
    Append("Rochea: One, like you, who does not take it, sees them as a new friend, rather than their oldest friend.[B][C]")
    Append("Mei:[E|Smirk] Ah, I see what you mean.[B][C]")
    Append("Rochea: It is how Thistle and I managed to sneak up on you![P] If your first instinct was to speak to them, you'd have heard us coming far off.[B][C]")
    Append("Mei:[E|Smirk] Heh.[P] You're right about that.[P] Thank you for answering my question.[B][C]")
end
