-- |[Mycela]|
--Dialogue script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Rochea")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	WD_SetProperty("Major Sequence Fast", true)
	Append("Mei:[E|Neutral] Leaf-sister, what has become of that alraune we found with the mushroom on her?[B][C]")
	Append("Rochea: She is...[P] in need of much learning.[B][C]")
	Append("Rochea: We have asked her to tell us where she strayed from the path.[P] She had no idea she was in the wrong.[B][C]")
	Append("Mei:[E|Neutral] Does she know her own name?[B][C]")
	Append("Rochea: No.[P] We have named her Mycela.[P] It seems to suit her.[B][C]")
	Append("Rochea: She said she awoke alone in a dark cave, the one you told me of.[B][C]")
	Append("Mei:[E|Sad] An abandoned alraune joining pool?[B][C]")
	Append("Mei:[E|Sad] She must have been so scared...[B][C]")
	Append("Rochea: Perhaps she fell in when the ground collapsed, or threw herself in deliberately.[P] We cannot say.[B][C]")
	Append("Rochea: The mushrooms around her had absorbed some foul substance, and whispered lies to her.[P] They did what our cleansing fungus does, and replaced her memories with untruths.[B][C]")
	Append("Rochea: Since then, she has been following their orders.[P] Kidnapping alraunes and subjecting them to the same treatment.[B][C]")
	Append("Rochea: The mushrooms were slowly devouring her...[B][C]")
	Append("Mei:[E|Sad] Was any permanent damage done?[B][C]")
	Append("Rochea: Only to her psyche.[P] The alraune body heals with time, she merely needs proper sunlight and nutrition.[P] Not that false light the mushrooms gave off.[B][C]")
	Append("Mei:[E|Neutral] But why were the mushrooms telling her lies?[B][C]")
	Append("Rochea: Normally, fungus does not host the little ones.[P] But there was something within these mushrooms.[P] I heard it myself, faintly.[B][C]")
	Append("Rochea: Whatever it was, it was not the spirits of life.[B][C]")
	Append("Mei:[E|Sad] This bodes ill for our forest...[B][C]")
	Append("Rochea: We know there is an enemy, now we will defeat it.[P] We will rescue our wayward sisters, expose this foulness, and destroy it.[B][C]")
	Append("Rochea: If you wish to remain with us, you can help.[P] Otherwise, we wish you success in finding your way home.[P] You have already proved your dedication to nature, Leaf-sister Mei.[B][C]")
	Append("Mei:[E|Smirk] Thanks, Leaf-Sister.[B][C]")
end
