-- |[Bees]|
--Asking this NPC about Bee Girls in general.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Rochea")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	WD_SetProperty("Major Sequence Fast", true)
	Append("Mei: The bees seem to pay us a great deal of attention more than the humans.[P] Is there a reason for that?[B][C]")
	Append("Rochea: Ah, the bees.[P] We are one of the sweetest sources of nectar for them.[B][C]")
	Append("Mei:[E|Offended] They want my nectar?[B][C]")
	Append("Mei:[E|Surprise] ... Is that bad?[B][C]")
	Append("Rochea: It's part of the natural cycle.[P] They are not normally aggressive, but in lean times the bees will take nectar when they cannot find it.[B][C]")
	Append("Rochea: We can find no more fault with them than we find fault with a storm on the horizon.[P] Nature bears no malice.[B][C]")
	Append("Mei:[E|Neutral] I see.[B][C]")
	Append("Rochea: Even so, be on your guard.[B][C]")
end
