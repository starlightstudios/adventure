-- |[Rochea]|
--Asking Rochea about herself.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Rochea")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	WD_SetProperty("Major Sequence Fast", true)
	Append("Mei:[E|Neutral] Are you the leader here, Rochea?[B][C]")
	Append("Rochea: Mmm, leaf-sister, we have no leaders.[B][C]")
	Append("Mei:[E|Surprise] My apologies.[P] I did not mean to offend.[B][C]")
	Append("Rochea: It is your lingering humanity that assumes all beings fit into some hierarchy.[P] Is a tree superior to a blade of grass?[P] Shall we measure the worth of a vine by its length?[B][C]")
	Append("Mei:[E|Laugh] No plant need have its worth measured![B][C]")
	Append("Rochea: Then you understand why your question was silly.[P] You are young yet, and we are patient.[B][C]")
	Append("Rochea: Though, in a manner, I am what a human would call a leader.[P] I am in charge of joining new leaf-sisters, because I happen to be very skilled at it.[B][C]")
	Append("Rochea: I do my best to make the process as painless and relaxing as possible.[B][C]")
    if(iIsMeiForeigner == 0.0) then
        Append("Mei:[E|Blush] It was wonderful.[P] It's a shame I can only experience it once.[B][C]")
        Append("Rochea: Perhaps later I can guide you through your first joining.[P] You will need to bring a human, of course.[B][C]")
        Append("Mei:[E|Neutral] That will have to wait.[P] Thank you for the offer, though.[B][C]")
    else
        Append("Mei:[E|Blush] I - I don't doubt that. I feel like I know you're an expert at it.[B][C]")
        Append("Rochea: Expert? I am not so certain about that. In any case, if you find a human you would like to join, we would love to help you.[B][C]")
        Append("Rochea: Even if you are a foreigner, you are among friends here.[B][C]")
        Append("Mei:[E|Neutral] That will have to wait.[P] Thank you for the offer, though.[B][C]")
    end
end
