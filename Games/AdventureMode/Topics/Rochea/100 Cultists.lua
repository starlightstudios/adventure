-- |[Rochea]|
--Asking Rochea about herself.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Rochea")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	local iCompletedTrapDungeon        = VM_GetVar("Root/Variables/Chapter1/Scenes/iCompletedTrapDungeon", "N")
	local iSeenAlrauneBattleIntroScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iSeenAlrauneBattleIntroScene", "N")
	
	--Initial case:
	if(iSeenAlrauneBattleIntroScene == 0.0) then
        
        --Rochea has not begun her assault:
        local iInformedOfDungeon = VM_GetVar("Root/Variables/Chapter1/Scenes/iInformedOfDungeon", "N")
        if(iInformedOfDungeon == 0.0) then
            Append("Mei:[E|Neutral] What do you know of the cultists, and that mansion near here?[B][C]")
            Append("Rochea: The mansion has cultists within it?[P] Human cultists?[B][C]")
            Append("Mei:[E|Surprise] Is that news? [P]You did not know?[B][C]")
            Append("Rochea: I'm afraid not.[P] I have not heard whispers from the little ones near there, I had assumed they decided to play a prank.[B][C]")
            Append("Rochea: Did they have any markings that might distinguish them?[B][C]")
            Append("Mei:[E|Offended] They were thugs who wore tattered cloaks and rusty chains.[P] The cloaks had hand imprints crudely scrawled on them.[B][C]")
            Append("Rochea: I've never heard of such markings...[B][C]")
            Append("Rochea: I will consult with the other sisters and grandfathers.[P] I'm afraid I cannot help you more than that.[B][C]")
	
        else
            Append("Mei:[E|Neutral] What do you know of the cultists, and that mansion near here?[B][C]")
            Append("Rochea: They are conspiring to do something foul.[P] We have felt the emanations through the soil.[B][C]")
            Append("Rochea: There is an entry point for you on the southern end.[P] Look for a small shed entrance.[B][C]")
            Append("Rochea: Meet my leaf-sisters within.[P] Your aid is most vital.[B][C]")
            Append("Mei:[E|Neutral] All right.[P] How will I know I'm going the right way?[B][C]")
            Append("Rochea: There is a locked door.[P] We would help, but we risk alerting the cultists further.[P] You'll have to get it open on your own.[B][C]")
        end
    
	--Met the Alraunes but haven't completed the dungeon yet:
	elseif(iSeenAlrauneBattleIntroScene == 1.0 and iCompletedTrapDungeon == 0.0) then
		Append("Mei:[E|Neutral] Has there been any update on the front?[B][C]")
		Append("Rochea: The daughters of the wild are holding the cult at bay.[B][C]")
		Append("Mei:[E|Neutral] Florentina and I are on the job, don't worry.[P] We just needed some more supplies.[B][C]")
		Append("Rochea: With the help of the little ones, we can hold the cult back, but not forever.[P] Please, get what you need and stop the ritual.[B][C]")
		Append("Mei:[E|Offended] You can count on us, leaf-sister Rochea![B][C]")
	
	--Met the Alraunes and completed the dungeon:
	else
		Append("Mei:[E|Neutral] Leaf-sister Rochea...[P] have you reconsidered?[P] Will you talk to Blythe about an alliance?[B][C]")
		Append("Rochea: I am sure we can defeat the cultists without resorting to aid from a human.[P] They will use the first opportunity to betray us.[B][C]")
		Append("Mei:[E|Sad] ...[P] I can't say you're not right.[P] I think you are.[B][C]")
		Append("Mei:[E|Neutral] Perhaps...[P] perhaps you could do the same in reverse?[B][C]")
		Append("Rochea: What are you proposing?[B][C]")
		Append("Mei:[E|Offended] Betray the humans before they betray us.[P] Simple.[P] Use them in fighting the cult.[P] When they are weak, strike quickly.[P] Forcibly join them.[P] Then they will help us forever.[B][C]")
		Append("Rochea: To be honest, I had considered that.[P] But, isn't it a human thing to betray one's allies?[B][C]")
		Append("Mei:[E|Happy] When there are no humans, there will be no betrayal.[B][C]")
		Append("Rochea: Wise.[B][C]")
		Append("Rochea: I will consult with the other leaf-sisters.[P] Your proposal is indeed a valid option.[B][C]")
		Append("Mei:[E|Neutral] Glad to help.[P] Nature will triumph in the end.[B][C]")
	
	end
end
