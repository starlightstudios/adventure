-- |[Nadia]|
--Dialogue.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Rochea")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	WD_SetProperty("Major Sequence Fast", true)
	Append("Mei:[E|Neutral] What is your opinion of Nadia?[B][C]")
	Append("Rochea: She is a terrible influence on the little ones![P] Just yesterday, they told me the worst joke...[B][C]")
	Append("Mei:[E|Laugh] Uh oh, here we go.[B][C]")
	Append("Rochea: It is too horrible to utter here, but it involved celery and following someone without their knowledge.[B][C]")
	Append("Rochea: Other than that, I fear she is misguided.[P] She does not merely tolerate humans, but seems to trust them.[B][C]")
	Append("Rochea: Our experience shows that to be a path leading to needless destruction.[P] They will turn on her, it is their nature.[B][C]")
	Append("Rochea: I only hope she comes to her senses before that happens and rejoins our union.[B][C]")
	Append("Mei:[E|Surprise] You really think Blythe would betray her?[B][C]")
	Append("Rochea: I know he will, for certain.[P] The hearts of men are dark indeed.[B][C]")
end
