-- |[Florentina]|
--Dialogue.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Rochea")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	WD_SetProperty("Major Sequence Fast", true)
	Append("Mei:[E|Neutral] About Florentina...[B][C]")
	Append("Rochea: Hmm, a name that is uttered rarely here.[B][C]")
	Append("Mei:[E|Neutral] I mean no offense.[B][C]")
	Append("Rochea: Of course.[P] I know her, and she knows me.[P] Her tongue is barbed and her form disfigured.[B][C]")
	Append("Rochea: These things reflect her soul.[P] She hides great pain beneath her stern facade.[B][C]")
	Append("Rochea: I suspect she has not cleansed herself, and thus she will not be one of us until she does.[P] She must let go of that pain.[B][C]")
	Append("Mei:[E|Neutral] I haven't been cleansed, either, but you still tolerate me.[B][C]")
	Append("Rochea: True, but your soul yearns for peace.[P] I don't think hers does.[B][C]")
	Append("Rochea: To bind herself to ephemeral relationships is what keeps her humanity intact.[P] That is not the way of the Alraune.[B][C]")
	Append("Rochea: ...[P] I am being too judgemental.[P] She will find her way in time.[B][C]")
end
