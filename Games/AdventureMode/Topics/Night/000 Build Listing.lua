-- |[ ===================================== Build Listing ====================================== ]|
--Builds a list of topics this NPC will respond to, and at what levels.
local sNPCName = "Night"
gsActiveNPCName = "Night"

--              ||||| Unlocked At Start |||  Filename      |||  Display Name (Optional)
fnConstructTopicStd(              true, "About You",     "About You")
fnConstructTopicStd(              true, "Date",          "Date")
fnConstructTopicStd(              true, "Secrebots",     "Secrebots")
