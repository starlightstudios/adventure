-- |[ ==================================== Night: Secrebots ==================================== ]|
--About the other secrebots.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Night")

--Common.
WD_SetProperty("Major Sequence Fast", true)

--Dialogue.
Append("Christine:[E|Smirk] Hey, did you and our mutual friend find out more about the other secrebots?[B][C]")
Append("Night:[E|Secrebot] What?[P] Oh, oh yes![P] Sorry.[B][C]")
Append("Night:[E|SecrebotSad] We had to 'exfiltrate' some of them, and the others...[P] we couldn't rescue.[B][C]")
Append("Night:[E|Secrebot] We've got my sisters in a safe place.[P] When the time comes, they'll be ready.[B][C]")
Append("Christine:[E|Laugh] Glad to hear it![P] Just don't tell me where.[P] Don't want any more liabilities.[B][C]")
Append("Night:[E|Secrebot] My vocal synthesizer is sealed, don't you worry.[P] To freedom.[B][C]")