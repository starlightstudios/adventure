-- |[ ==================================== Night: About You ==================================== ]|
--Let's hear your story.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Night")

--Common.
WD_SetProperty("Major Sequence Fast", true)

--Dialogue.
Append("Christine:[E|Smirk] It occurs to me I never really asked you about you.[B][C]")
Append("Night:[E|SecrebotHappy] What, you mean like my past, or my hobbies?[B][C]")
Append("Night:[E|SecrebotHappy] I like to collect labels.[P] Lubricant, cleaners, clothing, just cut the tag off and add it to my collection![B][C]")
Append("Sophie:[E|Smirk] I think she wanted to know about your past but I am suddenly interested in this collection.[B][C]")
Append("Night:[E|SecrebotSad] Oh, sorry to say.[P] I remember some of it but...[B][C]")
Append("Christine:[E|Neutral] It's common for conversion to scramble memories.[P] Given your rather unusual conversion...[B][C]")
Append("Night:[E|SecrebotSad] I remember some things and...[P] you know what?[B][C]")
Append("Night:[E|Secrebot] *Memory file deleted*.[B][C]")
Append("Night:[E|SecrebotHappy] That's better![P] Goodbye, you drunk jerk![P] I'll not even remember who you were![B][C]")
Append("Christine:[E|Sad] Umm, should I ask?[B][C]")
Append("Night:[E|SecrebotHappy] Can't![P] I deleted my memories just now, so I don't even know who you're asking about.[B][C]")
Append("Night:[E|SecrebotHappy] Being a robot is really nifty![P] But...[B][C]")
Append("Night:[E|Secrebot] I grew up on a farm just outside Jeffespeir.[P] Eight siblings, I'm the second youngest.[P] I was out in the fields after a yelling argument and a group of golems grabbed me.[B][C]")
Append("Night:[E|SecrebotHappy] I won't be missed, and I won't miss them.[B][C]")
Append("Sophie:[E|Neutral] So being turned into a secrebot is a good thing?[B][C]")
Append("Night:[E|Secrebot] My oldest sister got sold into indentured servitude to pay the family's debts off, and it was piling up again.[P] One of us was going to go.[B][C]")
Append("Night:[E|Secrebot] Slavery is supposed to be illegal but...[P] there's always someone who has a way.[B][C]")
Append("Christine:[E|Smirk] There's always parts of our past we'd rather not remember though I don't think you should go deleting memory files without making backups.[B][C]")
Append("Night:[E|Secrebot] I made some on the terminal in the repair bay, since I don't have my own quarters as a secrebot.[P] I won't be downloading them, though.[P] I made sure to remember that I don't want to.[B][C]")
Append("Night:[E|SecrebotHappy] The past is past, let's talk about something else![B][C]")