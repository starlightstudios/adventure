-- |[ ====================================== Night: Date ======================================= ]|
--Night asks a question!

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Night")

--Common.
WD_SetProperty("Major Sequence Fast", true)

--Dialogue.
Append("Night:[E|SecrebotHappy] Are you on a date?[P] That's so sweet![P] I heard units are not to mix their model variants...[B][C]")
Append("Sophie:[E|Smirk] We're not big on caring what other units think.[B][C]")
Append("Christine:[E|Smirk] Besides, even if I wanted to break up with her, I love her so much that I couldn't![B][C]")
Append("Night:[E|SecrebotSad] I'm happy for you, really.[B][C]")
Append("Sophie:[E|Neutral] You don't look happy.[B][C]")
Append("Night:[E|SecrebotSad] My cover is that I'm a refusbished secrebot.[P] Not a sentient unit.[P] And therefore...[B][C]")
Append("Christine:[E|Scared] Beautiful, but undateable![B][C]")
Append("Night:[E|SecrebotSad] And I get looks, too![P] I don't even know if my -[P] parts -[P] would work for that![B][C]")
Append("Sophie:[E|Offended] They do.[B][C]")
Append("Christine:[E|Offended] Sophie how do you know that?[B][C]")
Append("Sophie:[E|Offended] Look, Night?[P] Do you know how secrebots like yourself used to be, before they were obsoleted?[B][C]")
Append("Night:[E|Secrebot] Not really.[P] I did a bit of research for my cover but all I know is how to limit my speech patterns.[B][C]")
Append("Sophie:[E|Neutral] Secrebots were prized as status symbols by lord golems.[P] They could customize them to their liking, make them do basic tasks, and dress them how they wanted.[B][C]")
Append("Night:[E|Secrebot] Oh.[B][C]")
Append("Night:[E|SecrebotSad] Oh![B][C]")
Append("Christine:[E|Blush] Ohhhh...[B][C]")
Append("Sophie:[E|Offended] And whenever they wore out, guess who got to fix them?[B][C]")
Append("Night:[E|Secrebot] It's not really a guess.[B][C]")
Append("Christine:[E|Blush] Night, uh, CR-1-15, uh...[P] I know there's definitely some units who would be into a secrebot.[B][C]")
Append("Christine:[E|Blush] There's none in this sector but I could introduce you to some on the network.[P] Don't worry, they're units I trust.[B][C]")
Append("Night:[E|Secrebot] Uh...[B][C]")
Append("Night:[E|SecrebotSad] I don't know what to say, but...[P] I don't think I'm ready.[P] Yet.[B][C]")
Append("Night:[E|SecrebotHappy] I mean I would like you to introduce me but not right now.[P] I gotta process this.[P] Wow![B][C]")
Append("Sophie:[E|Blush] Oh I can think of a few of my friends who'd love to date a secrebot.[P] Especially a really pretty one![B][C]")
Append("Night:[E|SecrebotHappy] Thanks, you two.[P] I don't think I could ask for better friends.[B][C]")