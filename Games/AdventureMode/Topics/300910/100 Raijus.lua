-- |[Raijus]|
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "300910")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Flags.
	VM_SetVar("Root/Variables/Global/Christine/iKnowsAboutBreedingProgram", "N", 1.0)
	
	WD_SetProperty("Major Sequence Fast", true)
	Append("Christine:[E|Neutral] How do you generate power here without any Raijus?[B][C]")
	Append("300910:[E|Neutral] We have an underground cable that goes to the LRT Facility.[P] They've got a non-baryonic conversion reactor over there.[P] Raijus are really only used in Regulus City.[B][C]")
	Append("300910:[E|Neutral] Plus if we had any Raijus here, I'd never get any work done.[B][C]")
	Append("Christine:[E|Neutral] Because?[B][C]")
	Append("300910:[E|Neutral] Obviously I'd be snuggling them all the time![P] They're so fuzzy and huggable![B][C]")
	Append("300910:[E|Neutral] If you're ever in the biolabs, you should definitely go and play with them.[P] Units from the breeding program with exceptionally affectionate personalities are selected to become Raijus.[B][C]")
	Append("300910:[E|Neutral] Oh, but be careful not to zap yourself.[P] They tend to zap when they get excited.[P] Ha ha![B][C]")
	
end
