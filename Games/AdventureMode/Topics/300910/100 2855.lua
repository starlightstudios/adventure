-- |[Unit 2855]|
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "300910")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	WD_SetProperty("Major Sequence Fast", true)
	Append("Christine:[E|Neutral] So you knew 55 before she wiped her memory, right?[P] What was she like?[B][C]")
	Append("300910:[E|Neutral] Unit 771852, it's not polite to speak about someone when they're present.[B][C]")
	Append("55:[E|Neutral] As far as I'm concerned, who I was then is gone.[P] I do not remember who I was.[B][C]")
	Append("300910:[E|Neutral] I understand...[B][C]")
	Append("300910:[E|Neutral] I'll say she hasn't changed too much.[P] Brusque, not talkative, and very focused.[B][C]")
	Append("300910:[E|Neutral] But these aren't bad things.[P] When she wants to help you, she will get things done and in a hurry.[B][C]")
	Append("Christine:[E|Neutral] Yeah, that's the 55 I know.[B][C]")
	Append("55:[E|Neutral] It is - [P]comforting, I would venture.[B][C]")
	Append("Christine:[E|Neutral] A memory wipe won't change who you are.[B][C]")
	Append("55:[E|Neutral] You sound certain, but you should not be.[B][C]")
	Append("300910:[E|Neutral] Uh, you've said that before.[P] To me, in a different context, but you've said exactly that before.[B][C]")
	Append("Christine:[E|Neutral] Oh ho ho![B][C]")
	Append("55:[E|Neutral] ...[B][C]")
	Append("55:[E|Neutral] Unit 300910, were you aware I am currently wanted for questioning by Central Administration?[B][C]")
	Append("300910:[E|Neutral] Yes, I was.[P] It was on my briefing a while back.[B][C]")
	Append("55:[E|Neutral] But you did not act on that information.[P] Doubtless they would reward you for turning me in.[B][C]")
	Append("300910:[E|Neutral] Never![B][C]")
	Append("300910:[E|Neutral] Besides, I just assumed it was petty politicking.[P] Command Units at Central are always jockeying for position.[B][C]")
	Append("300910:[E|Neutral] Glad I'm not part of that mess, and I don't want to be.[P] If you're wanted for something, I'm sure you'll work it out eventually.[B][C]")
	Append("55:[E|Neutral] Does it not concern you?[P] Do you know why I am wanted for questioning?[B][C]")
	Append("300910:[E|Neutral] No, and I'm not going to ask.[P] In fact, you were never here, and I'll make sure the other units don't report you.[B][C]")
	Append("300910:[E|Neutral] I hope you get things sorted out, but I know you'll do it on your own terms.[B][C]")
	Append("Christine:[E|Neutral] That's as good an answer as we're ever going to hear, 55.[B][C]")

end
