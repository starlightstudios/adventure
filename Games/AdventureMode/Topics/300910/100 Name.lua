-- |[Name]|
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "300910")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Variables.
	local iKnowsAboutBreedingProgram = VM_GetVar("Root/Variables/Global/Christine/iKnowsAboutBreedingProgram", "N")
	
	--Flags.
	VM_SetVar("Root/Variables/Global/Christine/iKnowsAboutBreedingProgram", "N", 1.0)
	
	WD_SetProperty("Major Sequence Fast", true)
	Append("Christine:[E|Neutral] Can you tell me a bit more about yourself?[P] Why do you have such a high designation as a Command Unit?[B][C]")
	Append("300910:[E|Neutral] You know, I never really thought about it that way.[P] Most of the other Command Units have pretty low designations.[P] Guess that means they were converted well before me.[B][C]")
	Append("300910:[E|Neutral] I grew up in the breeding program.[P] Regulus Citizen, born and raised![B][C]")
	if(iKnowsAboutBreedingProgram == 0.0) then
		Append("Christine:[E|Neutral] Breeding program?[B][C]")
		Append("300910:[E|Neutral] ...[P] 771852?[P] You must be a fairly new unit...[B][C]")
		Append("300910:[E|Neutral] The Biological Research Labs have an enormous artificial habitat.[P] They keep specimens there, and we breed humans for conversion.[B][C]")
		Append("300910:[E|Neutral] I've heard humans actually evolved on Pandemonium, but I've never been there myself.[B][C]")
		Append("Christine:[E|Neutral] Does that bother you?[B][C]")
		Append("300910:[E|Neutral] When I was human, I was fed, watered, educated, and always had a nice bed to defr-[P] sleep in.[P] The Slave Units were always very kind to me, and I got to play with the Raijus every day after fitness routines.[P] Could you ask for anything more?[B][C]")
		Append("Christine:[E|Neutral] So were you picked to become a Command Unit, then?[B][C]")
		Append("300910:[E|Neutral] I suppose so.[P] When I turned 17, I had to take an aptitude test and some other things.[P] I don't know how the other girls did, but I guess they liked my answers.[B][C]")
		Append("300910:[E|Neutral] There was a very strange test afterwards...[P] I'd rather not talk about it...[B][C]")
		Append("300910:[E|Neutral] And then I was converted, and here I am.[B][C]")
		Append("Christine:[E|Neutral] Huh.[P] I had sort of assumed they didn't make new Command Units.[B][C]")
		Append("300910:[E|Neutral] I'd guess I was a replacement, or maybe we needed to expand.[P] After all, I do oversee this observatory, which was under construction when I was converted.[P] We'll need new Command Units sooner or later.[B][C]")
	else
		Append("Christine:[E|Neutral] I'm a little jealous![P] You're a true Regulan![B][C]")
		Append("300910:[E|Neutral] Are you from Pandemonium?[B][C]")
		Append("Christine:[E|Neutral] No, but -[P] it's -[P] it's not important.[B][C]")
		Append("Christine:[E|Neutral] So why did you become a Command Unit and not a Lord Unit?[B][C]")
		Append("300910:[E|Neutral] When I turned 17, I had to take an aptitude test and some other things.[P] I don't know how the other girls did, but I guess they liked my answers.[B][C]")
		Append("300910:[E|Neutral] There was a very strange test afterwards...[P] I'd rather not talk about it...[B][C]")
		Append("300910:[E|Neutral] And then I was converted, and here I am.[B][C]")
		Append("Christine:[E|Neutral] Huh.[P] I had sort of assumed they didn't make new Command Units.[B][C]")
		Append("300910:[E|Neutral] I'd guess I was a replacement, or maybe we needed to expand.[P] After all, I do oversee this observatory, which was under construction when I was converted.[P] We'll need new Command Units sooner or later.[B][C]")
	end
	Append("300910:[E|Neutral] Now it's funny you ask, because I've been asked why I'm different from the other Command Units.[B][C]")
	Append("55:[E|Neutral] I was about to point that out.[B][C]")
	Append("300910:[E|Neutral] Maybe it's because I'm from the breeding program?[P] Where are you from, 2855?[B][C]")
	Append("55:[E|Neutral] ...[B][C]")
	Append("300910:[E|Neutral] Ah, right.[P] Memories.[P] Sorry.[B][C]")
	Append("300910:[E|Neutral] Was there anything else I could help you with?[B][C]")
	
	--Topic unlocks.
	WD_SetProperty("Unlock Topic", "BreedingProgram", 1)
end
