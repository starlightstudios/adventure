-- |[Academic Code]|
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "300910")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
    
    --Variables:
    local iRuneUpgradeSerenity  = VM_GetVar("Root/Variables/Chapter5/Scenes/iRuneUpgradeSerenity", "N")
	
    if(iRuneUpgradeSerenity == 0.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iRuneUpgradeSerenity", "N", 1.0)
        WD_SetProperty("Major Sequence Fast", true)
        Append("55:[E|Neutral] Unit 300910, I need to ask a favour.[B][C]")
        Append("300910:[E|Neutral] Certainly![B][C]")
        Append("55:[E|Neutral] Do you have an academic decryption engine of specification PR-1100m, second series?[B][C]")
        Append("300910:[E|Neutral] ...[P] Yes, or at least we did.[P] How did you know that?[B][C]")
        Append("55:[E|Neutral] I have traced the use of the decryption engine to several locations.[P] May I take a look at what you have?[B][C]")
        Append("300910:[E|Neutral] I'm sorry, but standard procedure is to delete it when you're done with it.[P] It was not in my possession, actually, it was used by some surveyors who were using the observatory briefly.[B][C]")
        Append("300910:[E|Neutral] I do have part of it, though.[P] They deleted it but did not fully wipe the drive.[P] What a shame.[B][C]")
        Append("55:[E|Smirk] Indeed.[P] I am attempting to piece the decryption engine together, this will help.[B][C]")
        Append("55:[E|Neutral] [SOUND|World|TakeItem] (Got part of the academic decryption engine!)[B][C]")
        Append("300910:[E|Neutral] Should I ask what you need it for?[B][C]")
        Append("55:[E|Neutral] Is this idle curiosity?[B][C]")
        Append("300910:[E|Neutral] Yes.[P] If you don't want to answer...[B][C]")
        Append("Christine:[E|Neutral] It might be best not to.[P] For your own safety.[B][C]")
        Append("300910:[E|Neutral] I see.[P] Though it is not difficult to piece together what it concerns.[P] The surveyors were working in the crater a while ago, before all this started.[P] It's linked.[B][C]")
        Append("300910:[E|Neutral] I hope I helped![B][C]")
    else
        WD_SetProperty("Major Sequence Fast", true)
        Append("300910:[E|Neutral] All I can offer for background is that a survey team was using the observatory for a while when working down in the crater.[P] Shortly afterwards, the facility down there was under construction.[B][C]")
        Append("300910:[E|Neutral] Didn't see them again after that.[P] I'm smart enough to put 10 and 10 together.[B][C]")
    end
end
