-- |[Breeding Program]|
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "300910")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Variables.
	local iKnowsAboutBreedingProgram = VM_GetVar("Root/Variables/Global/Christine/iKnowsAboutBreedingProgram", "N")
	
	--Flags.
	VM_SetVar("Root/Variables/Global/Christine/iKnowsAboutBreedingProgram", "N", 1.0)
	
	WD_SetProperty("Major Sequence Fast", true)
	Append("Christine:[E|Neutral] What can you tell me about the breeding program?[B][C]")
	Append("300910:[E|Neutral] Ahhhh, those were the days.[P] I really should go back and see how things are going over there next time I'm in for review.[B][C]")
	Append("300910:[E|Neutral] Oh, and I should send a message to Unit 300917![P] She was my best friend, and I haven't seen her in months.[B][C]")
	Append("Christine:[E|Neutral] Did you have secondary designations in the program?[B][C]")
	Append("300910:[E|Neutral] Yes, but we had to write our primary designations on everything in the education curriculum.[P] We only used secondary designations when we were by ourselves.[B][C]")
	Append("300910:[E|Neutral] Unit 278282 was very strict about that, and would give you shearing duty if she overheard you.[B][C]")
	Append("Christine:[E|Neutral] What did you do while you were there?[B][C]")
	Append("300910:[E|Neutral] Well, you wake up,[P] attend morning fitness,[P] education classes in the afternoon,[P] afternoon fitness,[P] and then you can do whatever you want after that.[B][C]")
	Append("300910:[E|Neutral] I would usually go play with the Raijus, or hang out with the younger units.[B][C]")
	Append("Christine:[E|Neutral] Younger units?[B][C]")
	Append("300910:[E|Neutral] Uh, yes.[P] Is that odd?[B][C]")
	Append("Christine:[E|Neutral] You mean, younger children?[B][C]")
	Append("300910:[E|Neutral] That's what I said,[P] isn't it?[B][C]")
	Append("Christine:[E|Neutral] We used to call each other kids where I'm from.[B][C]")
	Append("300910:[E|Neutral] Like baby goats?[P] I think I'll stick to units if it's all the same to you.[B][C]")
	Append("300910:[E|Neutral] Anyway, every fifth day was free of education classes, and we'd sometimes go visit Regulus City if we could convince our overseers to give us day passes.[B][C]")
	Append("300910:[E|Neutral] I remember scaring the daylights out of Unit 199230 once![P] She screamed[P] 'Rogue human! Rogue human! Security, help!'[P] when I showed up in the cafeteria.[B][C]")
	Append("300910:[E|Neutral] Even when I showed her my badge and told her I was Unit 300910, she didn't calm down until security took me away.[B][C]")
	Append("Christine:[E|Neutral] I haven't seen any human children in Sector 96.[B][C]")
	Append("300910:[E|Neutral] Oh, you'd probably only see them in Sector 3.[P] That's the only place we could get day passes to.[B][C]")
	Append("300910:[E|Neutral] You're bringing back a lot of memories, 771852.[P] I really miss the old habitat.[B][C]")
	Append("300910:[E|Neutral] But I have responsibilities to attend to.[P] Still, I really want to go play with the Raijus now...[B][C]")

	--Topic unlocks.
	WD_SetProperty("Unlock Topic", "Raijus", 1)

end
