-- |[Topic Script]|
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local iRecalibratesDone = VM_GetVar("Root/Variables/Chapter5/Scenes/iRecalibratesDone", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "300910")

--No recalibrations yet:
if(iRecalibratesDone < 1.0) then
    Append("300910:[E|Neutral] So, to reiterate::[P] The sensors we have set up in the crater shelf need to be recalibrated.[B][C]")
    Append("300910:[E|Neutral] I can't do it remotely, it has to be done by hand.[P] Can you help us with that?[B][C]")
    Append("Christine:[E|Smirk] Shouldn't be a problem.[P] What do I need to do?[B][C]")
    Append("300910:[E|Neutral] There's a waystation partway down the crater shelf.[P] All the sensors connect with it via a heavy-duty cable.[B][C]")
    Append("300910:[E|Neutral] An intercom in the waystation should allow us to keep in contact.[P] Head down there and call me when you do.[P] The calibration codes are in that waystation.[B][C]")

--Started but not completed.
elseif(iRecalibratesDone < 6.0) then
    Append("300910:[E|Neutral] I see you've got some of our sensors recalibrated.[P] Is everything all right?[B][C]")
    Append("Christine:[E|Neutral] We got a little side-tracked.[P] There's six sensors, right?[B][C]")
    Append("300910:[E|Neutral] Correct.[P] Thank you for this, Christine.[P] You're serving the Cause of Science very directly here.[B][C]")
    Append("Christine:[E|Smirk] We're always happy to help.[P] You can count on us.[B][C]")

--Completed. Turning the quest in.
elseif(iRecalibratesDone == 6.0) then

    --Variables.
    local iHasDarkmatterForm = VM_GetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N")
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
    
    --Flags.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iRecalibratesDone", "N", 1000)
    VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + 200)

    --Dialogue.
    Append("300910:[E|Neutral] Well, I figured you'd have called me on the intercom, but this is nice too.[B][C]")
    Append("Christine:[E|Smirk] I prefer the personal touch.[B][C]")
    Append("Christine:[E|Smirk] All your sensors are reset and ready to go.[B][C]")
    Append("300910:[E|Neutral] Quite right![P] We're already collecting data again, and nobody had to get hurt![B][C]")
    Append("300910:[E|Neutral] Well done, Christine.[P] I've sent 200 Work Credits to your account.[B][C]")
    Append("Christine:[E|Smirk] Happy to help![P] Anything else we can do?[B][C]")
    
    --If Christine does not have Darkmatter form, there's more quests to do:
    if(iHasDarkmatterForm == 0.0) then
        Append("300910:[E|Neutral] I presume you have not fully investigated the facility at the base of the crater.[B][C]")
        Append("300910:[E|Neutral] I have my suspicions that it is related in some way to the recent events on the shelf, but I do not have the resources to find out.[B][C]")
        Append("300910:[E|Neutral] The entrance point is to the west of the waystation you found the sensors at.[P] There should be a ladder down to the staging area.[B][C]")
        Append("Christine:[E|Neutral] We'll be heading there next, then.[P] Thank you.[B][C]")
    
    --No that's enough.
    else
        Append("300910:[E|Neutral] You've already helped the observatory far in excess of what I could have expected.[P] Thank you.[B][C]")
        Append("300910:[E|Neutral] We owe you a great debt.[P] All we can do is try to prepare for what comes next.[B][C]")
    end

--If it's at 1000
else
    Append("300910:[E|Neutral] Hmm, the volumetric scanners went out of phase all at once, and there was a ripple felt further out.[P] It was like an earthquake.[B][C]")
    Append("Christine:[E|Neutral] Aren't earthquakes rather common around here?[B][C]")
    Append("300910:[E|Neutral] They are, but the energy travels through rock, not three-dimensional space.[B][C]")
    Append("300910:[E|Neutral] Yet if these numbers are correct, the phase disruption was volumetric.[P] As if space itself was shaken, and not just the rock...[B][C]")

end
