-- |[ =================================== Esmerelda: Joining =================================== ]|
--Appears to allow Sanya to join the harpies. If she already did, some talking about that.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[ =============== Dialogue =============== ]|
--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --New harpy time!
    local iHasHarpyForm = VM_GetVar("Root/Variables/Global/Sanya/iHasHarpyForm", "N")
    if(iHasHarpyForm == 0.0) then
    
        --Dialogue.
        Append("Esmerelda:[E|Neutral] Have you given some more thought to my proposal?[P] We'd love to have you in our flock.[B][C]")
        Append("Esmerelda:[E|Neutral] Even if you choose not to join us as a soldier, I'm sure the form of a harpy will help on your quest.[B][C]")
        Append("Sanya:[E|Neutral] (Should I become a harpy?)[B]")

        --Decision script is this script.
        WD_SetProperty("Activate Decisions")
        WD_SetProperty("Add Decision", "Yes",                LM_GetCallStack(0) , "Yes")
        WD_SetProperty("Add Decision", "Yes (Feet Version)", LM_GetCallStack(0) , "Yes (Feet Version)")
        WD_SetProperty("Add Decision", "No",                 LM_GetCallStack(0) , "No")
        fnCutsceneBlocker()
    
    --Already has the form:
    else
        WD_SetProperty("Activate Topics After Dialogue", "Esmerelda")
        Append("Esmerelda:[E|Neutral] Did the transformation process alleviate your fears?[B][C]")
        Append("Sanya:[E|Neutral] I had a think about it, both before and after.[P] I think I made the right call.[B][C]")
        Append("Sanya:[E|Neutral] People sometimes decide they're going to learn a new language.[P] The weebs can't get enough of Japanese culture.[P] Those are Earth things, by the way.[B][C]")
        Append("Esmerelda:[E|Neutral] Weebs?[B][C]")
        Append("Sanya:[E|Neutral] Short for weeaboos.[B][C]")
        Append("Esmerelda:[E|Neutral] Oh well that explains it. (The Weeaboos must be like a culture of people, right?)[B][C]")
        Append("Sanya:[E|Neutral] Wouldn't it be incredible if every culture were this way?[P] There's people who would kill for this kind of chance.[P] I should take advantage of it.[B][C]")
        Append("Sanya:[E|Neutral] It's not like I'm changing myself, it's like I'm gaining a new part.[P] I'm better than ever![P] And so is the flock for having me, too.[B][C]")
        Append("Esmerelda:[E|Neutral] I hope that you understand why I think we can bring the world together in peace.[P] Each new culture becomes part of a great tapestry, not lost within it.[B][C]")
        Append("Esmerelda:[E|Neutral] It's quite a thing to jump from that to world peace, but the fastest way to fail is to not try.[B][C]")
        Append("Sanya:[E|Neutral] We're all pulling for you, chief.[B][C]")
    end

-- |[ ================= Agree ================ ]|
elseif(sTopicName == "Yes") then
    
    -- |[Variables]|
    local iEmpressJoined = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
    
    -- |[Dialogue]|
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Esmerelda", "Neutral") ]])
	fnCutscene([[ Append("Sanya:[E|Happy] Bird me up, boss![B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] I knew you'd see things our way.[P] I need to call some people![B][C]") ]])
	fnCutscene([[ Append("Sanya:[E|Neutral] Is it a complex process?[B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] No, well, you'll see.[P] Just wait outside.[B][C]") ]])
	fnCutscene([[ Append("Izuna:[E|Happy] I hope it's fun, sweetie![B][C]") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ Append("Empress:[E|Neutral] I am sure it will be enjoyable.[B][C]") ]])
    end
	fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh.") ]])
    
    -- |[Call Subscene]|
    fnCutsceneWait(45)
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --While blacked out, call the harpy volunteer scene.
    LM_ExecuteScript(gsRoot .. "Chapter 2/Scenes/400 Volunteer/Harpy/Scene_Begin.lua")
    
    --Switch to Harpy form.
    fnCutsceneCall(gsRoot .. "FormHandlers/Sanya/Form_Harpy.lua")
    
    -- |[Movement]|
    fnCutsceneTeleport("Sanya", 42.25, 39.50)
    fnCutsceneFace("Sanya", 1, 0)
    fnCutsceneTeleport("Esmerelda", 43.25, 39.50)
    fnCutsceneFace("Esmerelda", -1, 0)
    fnCutsceneTeleport("Izuna", 42.25, 32.50)
    fnCutsceneFace("Izuna", 0, 1)
    fnCutsceneTeleport("Zeke", 43.25, 32.50)
    fnCutsceneFace("Zeke", 0, 1)
    if(iEmpressJoined == 1.0) then
        fnCutsceneTeleport("Empress", 41.25, 32.50)
        fnCutsceneFace("Empress", 0, 1)
    end
    
    --Wait a bit.
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Esmerelda", "Neutral") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] So, does everything fit?[P] Will your uniform survive your transformations?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] My clothes usually do, don't see any reason why these wouldn't.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] So this imprinting thing didn't tell me how to salute.[P] Do we have one?[B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] Place your left wing in front of you, hold it for a second, then bow your head.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] Cool![B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] I take it you'd like to join the harpy army, then?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] If you guys are saving the world, count me in.[P] I'll help keep the people of Trafal safe.[B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] Splendid![P] But for now, I can't attach you to a unit.[P] Your quest comes first.[B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] The situation with the bandits has not changed.[P] We can always use your help in those matters.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Laugh] So really, go forth and be nice to people?[B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] Be the person the prophecy foretold.[P] Let the legend come to life![B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] And if we can help in your mission in any way, just come to me.[P] We're in this together.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] Birds of a feather.[P] All right!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("Sanya", 42.25, 33.50)
    fnCutsceneFace("Esmerelda", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Sanya:[E|Neutral] All right, we've got a world to save.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Jot] Wait, wait![P] I have a few questions![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] I'm going to limit you to one.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Jot] Uhh, how do harpy wings work?[P] Can you fly and hold things?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] No.[P] You know flexing?[P] It's kind of like that.[P] If I flex the right way, my arm turns into a wing.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] The hands and like, hidden?[P] Magic stuff.[P] When I do that, I can fly.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Jot] Oh this is good...[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Bah?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] I said one question, Zeke.[P] Izuna already used it up.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Jot] I guess I can ask follow-ups later.[B][C]") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ Append("Empress:[E|Neutral] There is something important I need to know.[P] Is the harpy intention genuine?[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] As far as the song I heard goes, yes.[P] They really believe this.[P] They really are here to protect Trafal.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Sad] Trust me when I tell you, it's not something you could ever hide.[P] The song is personal.[P] There's more in there than the culture, traditions, symbols.[P] There's stories of pain, too.[P] Loss.[P] Grieving.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] I see.[P] That is worthy of consideration.[B][C]") ]])
    end
    fnCutscene([[ Append("Sanya:[E|Smirk] Okay, form up.[P] Let's go!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Finish Up]|
    fnAutoFoldParty()
    fnCutsceneBlocker()
    
-- |[ ================= Agree ================ ]|
elseif(sTopicName == "Yes (Feet Version)") then
    
    -- |[Variables]|
    local iEmpressJoined = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter2/HarpyBase/iDidHarpyFeet", "N", 1.0)
    
    -- |[Dialogue]|
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Esmerelda", "Neutral") ]])
	fnCutscene([[ Append("Sanya:[E|Happy] Bird me up, boss![B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] I knew you'd see things our way.[P] I need to call some people![B][C]") ]])
	fnCutscene([[ Append("Sanya:[E|Neutral] Is it a complex process?[B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] No, well, you'll see.[P] Just wait outside.[B][C]") ]])
	fnCutscene([[ Append("Izuna:[E|Happy] I hope it's fun, sweetie![B][C]") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ Append("Empress:[E|Neutral] I am sure it will be enjoyable.[B][C]") ]])
    end
	fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh.") ]])
    
    -- |[Call Subscene]|
    fnCutsceneWait(45)
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --While blacked out, call the harpy volunteer scene.
    LM_ExecuteScript(gsRoot .. "Chapter 2/Scenes/400 Volunteer/HarpyFeet/Scene_Begin.lua")
    
    --Switch to Harpy form.
    fnCutsceneCall(gsRoot .. "FormHandlers/Sanya/Form_Harpy.lua")
    
    -- |[Movement]|
    fnCutsceneTeleport("Sanya", 42.25, 39.50)
    fnCutsceneFace("Sanya", 1, 0)
    fnCutsceneTeleport("Esmerelda", 43.25, 39.50)
    fnCutsceneFace("Esmerelda", -1, 0)
    fnCutsceneTeleport("Izuna", 42.25, 32.50)
    fnCutsceneFace("Izuna", 0, 1)
    fnCutsceneTeleport("Zeke", 43.25, 32.50)
    fnCutsceneFace("Zeke", 0, 1)
    if(iEmpressJoined == 1.0) then
        fnCutsceneTeleport("Empress", 41.25, 32.50)
        fnCutsceneFace("Empress", 0, 1)
    end
    
    --Wait a bit.
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Esmerelda", "Neutral") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] So, does everything fit?[P] Will your uniform survive your transformations?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] My clothes usually do, don't see any reason why these wouldn't.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] So this imprinting thing didn't tell me how to salute.[P] Do we have one?[B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] Place your left wing in front of you, hold it for a second, then bow your head.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] Cool![B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] I take it you'd like to join the harpy army, then?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] If you guys are saving the world, count me in.[P] I'll help keep the people of Trafal safe.[B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] Splendid![P] But for now, I can't attach you to a unit.[P] Your quest comes first.[B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] The situation with the bandits has not changed.[P] We can always use your help in those matters.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Laugh] So really, go forth and be nice to people?[B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] Be the person the prophecy foretold.[P] Let the legend come to life![B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] And if we can help in your mission in any way, just come to me.[P] We're in this together.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] Birds of a feather.[P] All right!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("Sanya", 42.25, 33.50)
    fnCutsceneFace("Esmerelda", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Sanya:[E|Neutral] All right, we've got a world to save.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Jot] Wait, wait![P] I have a few questions![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] I'm going to limit you to one.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Jot] Uhh, how do harpy wings work?[P] Can you fly and hold things?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] No.[P] You know flexing?[P] It's kind of like that.[P] If I flex the right way, my arm turns into a wing.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] The hands and like, hidden?[P] Magic stuff.[P] When I do that, I can fly.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Jot] Oh this is good...[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Bah?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] I said one question, Zeke.[P] Izuna already used it up.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Jot] I guess I can ask follow-ups later.[B][C]") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ Append("Empress:[E|Neutral] There is something important I need to know.[P] Is the harpy intention genuine?[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] As far as the song I heard goes, yes.[P] They really believe this.[P] They really are here to protect Trafal.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Sad] Trust me when I tell you, it's not something you could ever hide.[P] The song is personal.[P] There's more in there than the culture, traditions, symbols.[P] There's stories of pain, too.[P] Loss.[P] Grieving.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] I see.[P] That is worthy of consideration.[B][C]") ]])
    end
    fnCutscene([[ Append("Sanya:[E|Smirk] Okay, form up.[P] Let's go!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Finish Up]|
    fnAutoFoldParty()
    fnCutsceneBlocker()

-- |[ ============== No Thanks =============== ]|
elseif(sTopicName == "No") then
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Esmerelda") ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Esmerelda", "Neutral") ]])
	fnCutscene([[ Append("Sanya:[E|Sad] I'm still thinking it over.[P] I'll let you know if I decide I want to.[B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] I understand.[P] Take all the time you need.[B][C]") ]])
end
