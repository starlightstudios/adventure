-- |[ =================================== Esmerelda: Goodbye =================================== ]|
--Script that fires when the player says goodbye to end the dialogue.
WD_SetProperty("Major Sequence Fast", true)

--Standard.
Append("Esmerelda:[E|Neutral] Good luck on the road!")