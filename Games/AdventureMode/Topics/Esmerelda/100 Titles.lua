-- |[ ==================================== Esmerelda: Titles =================================== ]|
--What's up with titles anyway?
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Esmerelda")

-- |[Dialogue]|
--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Variables.
    local iEmpressJoined = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")

    Append("Sanya:[E|Neutral] Hey chief, I'm wondering why you seemed put off by me calling you...[P] chief.[B][C]")
    Append("Esmerelda:[E|Neutral] Did I come off as put off?[P] Oh my.[B][C]")
    Append("Sanya:[E|Neutral] I ain't trying to offend or anything.[P] I tend to do that.[P] A lot.[B][C]")
    Append("Izuna:[E|Explain] It's one of her core traits![P] She's extremely forward![B][C]")
    Append("Esmerelda:[E|Neutral] No, it's not offensive.[P] I just have a, shall we say, complicated relationship with titles.[B][C]")
    Append("Esmerelda:[E|Neutral] I know what purpose they serve, but I have seen firsthand what they do to someone.[B][C]")
    Append("Sanya:[E|Laugh] I have no idea what you're talking about![B][C]")
    Append("Esmerelda:[E|Neutral] Do you not have titles on Earth?[B][C]")
    Append("Sanya:[E|Smirk] Still do, but the nobles are long gone.[P] But you gotta call the person who owns something, something.[P] Titles are everywhere.[B][C]")
    Append("Esmerelda:[E|Neutral] Hm.[P] Allow me to illustrate this to you.[B][C]")
    Append("Izuna:[E|Jot] (Ooh![P] This might be a scoop!)[B][C]")
    Append("Esmerelda:[E|Neutral] Let us say you are about to challenge someone to a fight.[B][C]")
    Append("Sanya:[E|Smirk] Back home, I called that 'Saturday'.[B][C]")
    Append("Zeke:[E|Neutral] Bah![B][C]")
    if(iEmpressJoined == 1.0) then
        Append("Empress:[E|Neutral] Ah, this parable.[B][C]")
        Append("Esmerelda:[E|Neutral] You know of it?[B][C]")
        Append("Empress:[E|Neutral] I wrote it.[B][C]")
    end
    Append("Esmerelda:[E|Neutral] Let us say your enemy has several skulls in their possession.[P] Human skulls.[P] What does this tell you about them?[B][C]")
    Append("Sanya:[E|Neutral] That they're a men's rights activist?[B][C]")
    Append("Esmerelda:[E|Neutral] ...[B][C]")
    Append("Izuna:[E|Neutral] ...[B][C]")
    Append("Zeke:[E|Neutral] BAH BAH![P] BAH![B][C]")
    Append("Sanya:[E|Laugh] Earth thing![P] Earth thing![B][C]")
    Append("Esmerelda:[E|Neutral] Well, if you were on Pandemonium...[B][C]")
    Append("Sanya:[E|Laugh] That the person in question is dangerous.[P] You know, because they killed people, probably.[B][C]")
    Append("Esmerelda:[E|Neutral] Right.[P] The title is the skulls.[P] It indicates you should treat a person a certain way, even if you don't know them.[B][C]")
    Append("Esmerelda:[E|Neutral] The army officers in Jeffespeir, the nobles with their titles, all of these demand respect and deference.[P] A person who has earned respect ought to get it, even from people who don't know them.[P] That is what the title is for.[B][C]")
    Append("Esmerelda:[E|Neutral] If you encounter a harpy officer, I would hope you would respect them.[P] They are doing a difficult and dangerous job.[P] They have chosen to lead, they cannot simply go home like the soldiers can.[P] Theirs is life on the battlefield, on patrol, on duty.[B][C]")
    Append("Sanya:[E|Neutral] Yeah, I get that.[B][C]")
    Append("Esmerelda:[E|Neutral] But to the nobles, they get the title first.[P] They believe their deference is innate.[P] The title becomes their obsession, not the duty.[B][C]")
    Append("Esmerelda:[E|Neutral] Soon, the title means nothing, or its opposite.[P] If you see an officer in the Jefferspeirian army, most likely they are not worthy of your respect![B][C]")
    Append("Sanya:[E|Neutral] Yeah, the title is politics.[B][C]")
    Append("Esmerelda:[E|Neutral] Precisely.[P] So you see why I am hesitant to let people call me anything other than my name.[B][C]")
    Append("Sanya:[E|Neutral] You don't think you've earned it?[P] To be called chief?[B][C]")
    Append("Esmerelda:[E|Neutral] Maybe a little...[P] But mostly, I am worried that I will fall into the same trap as them.[B][C]")
    Append("Esmerelda:[E|Neutral] If not me, then the soldiers under my command.[P] How can a title be made to do its job?[P] How can I keep those who strive for the title itself apart from those who seek to earn them through good works?[B][C]")
    if(iEmpressJoined == 1.0) then
        Append("Empress:[E|Neutral] You cannot.[B][C]")
        Append("Empress:[E|Neutral] Sooner or later, politics wins out over merit.[P] But the person who creates the title must win it by merit.[P] The first one to hold it is usually genuine.[B][C]")
        Append("Esmerelda:[E|Neutral] And thus why you saw succession as your key advantage.[B][C]")
        Append("Empress:[E|Neutral] Precisely.[P] How you will solve it, I do not know.[B][C]")
    else
        Append("Sanya:[E|Sad] Yeah, sounds like Earth, all right.[B][C]")
        Append("Esmerelda:[E|Neutral] How did Earth solve this problem?[B][C]")
        Append("Sanya:[E|Neutral] The incompetent morons who inherited their titles sparked a war that killed millions, and their empires fell to revolution when the people became sick of it.[B][C]")
        Append("Sanya:[E|Laugh] Not so much 'solved' the problem, but there we are.[B][C]")
        Append("Sanya:[E|Neutral] And just so you know, I come from one of the countries that was in the thick of it.[B][C]")
    end
    Append("Esmerelda:[E|Neutral] Well, it is a problem that haunts me.[P] Now I hope you can see why.[B][C]")
    Append("Sanya:[E|Neutral] Yeah, I get it.[P] But I'm still going to call you chief.[B][C]")
    Append("Esmerelda:[E|Neutral] I appreciate that.[P] I hope I live up to that name.[B][C]")
end
