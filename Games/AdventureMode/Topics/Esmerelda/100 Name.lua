-- |[ ================================== Esmerelda: Your Name ================================== ]|
--Asking about her name. "It's a long story".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Esmerelda")

-- |[Dialogue]|
--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Variables.
    local iEmpressJoined = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
	
    --Dialogue.
    Append("Sanya:[E|Smirk] So what's up with your name, de Artezzi?[P] You said it was a long story?[B][C]")
    Append("Izuna:[E|Explain] That's one of the four great houses in Jeffespeir![P] Didn't you say you were from there?[B][C]")
    Append("Esmerelda:[E|Neutral] Indeed.[P] You are well read.[P] Yes, I am descended from the great House Artezzi.[B][C]")
    Append("Sanya:[E|Smirk] Cool.[P] Does that mean you're a knight or something?[B][C]")
    Append("Esmerelda:[E|Neutral] No.[P] While I am a blood descendent, I lost my title when I became a harpy.[P] That is one of the laws there.[B][C]")
    Append("Esmerelda:[E|Neutral] Only humans can sit on the city council.[P] Not that it matters much right now.[P] The city was conquered just after I left.[B][C]")
    Append("Sanya:[E|Offended] Bastards![B][C]")
    Append("Zeke:[E|Neutral] Bah![B][C]")
    Append("Esmerelda:[E|Neutral] You don't know anything about the conquerers.[B][C]")
    Append("Sanya:[E|Neutral] No, but you have to yell 'Bastards!' every now and then to keep the blood flowing.[B][C]")
    Append("Zeke:[E|Neutral] Bah![B][C]")
    Append("Sanya:[E|Neutral] See.[B][C]")
    Append("Izuna:[E|Explain] I believe House Artezzi is often called 'House Green' because of their emblem's colours.[B][C]")
    Append("Esmerelda:[E|Neutral] It's an easy way to keep us straight for outsiders.[B][C]")
    Append("Sanya:[E|Neutral] You said it was a long story.[P] That seems pretty short to me.[B][C]")
    Append("Esmerelda:[E|Neutral] The long part of the story is how I left the city to tour the world, only to find after I left that the city had been invaded and conquered.[B][C]")
    Append("Esmerelda:[E|Neutral] I didn't really care for Jeffespeir's politics, to be honest.[P] It's a collection of back-biting noble families who care only for their own power.[B][C]")
    if(iEmpressJoined == 1.0) then
        Append("Esmerelda:[E|Neutral] Coincidentally, we happen to be standing next to someone who also conquered Jeffespeir, once.[B][C]")
        Append("Empress:[E|Neutral] I was wondering if you knew about that.[B][C]")
        Append("Sanya:[E|Happy] Cool![P] Way to go, Big E![B][C]")
        Append("Empress:[E|Neutral] The city council of Jeffespeir has a 'Speaker' position.[P] They possess executive powers.[P] Each term, the Speaker is selected from a name in a hat to govern.[B][C]")
        Append("Empress:[E|Neutral] The nobles no doubt try to rig the process, but it has to be done in public with many witnesses.[P] Anyway, I conquered the city, and declared myself Speaker in perpetuity.[B][C]")
        Append("Esmerelda:[E|Neutral] The plains riders did the same thing when they conquered the city.[B][C]")
        Append("Empress:[E|Neutral] It's a good way to let the petty politics burn themselves out with a lack of power.[P] Anything I needed to do in the city, I simply abrogated the matching power to the position of Speaker.[P] If I needed a law removed, I removed it.[P] My army made sure everyone cooperated.[B][C]")
        Append("Esmerelda:[E|Neutral] The city hasn't changed much since then.[P] The brickwork is nicer, but the corruption is evergreen.[B][C]")
    end
    Append("Esmerelda:[E|Neutral] I saw that the politicking was strangling the city, which isn't hard to see.[P] Self-dealing is rife, inequality, endemic.[P] We have homes to house people, but they go empty because the planning council only gives houses for favours.[B][C]")
    Append("Esmerelda:[E|Neutral] There is food enough for everyone, but selling food makes important money for House Insiati, so people go hungry while food rots.[P] It's disgusting.[B][C]")
    Append("Sanya:[E|Smirk] Man, do I have bad news about Earth.[B][C]")
    Append("Izuna:[E|Neutral] Is that why you became a harpy?[B][C]")
    Append("Esmerelda:[E|Neutral] Yes.[P] As Sanya said, this is a problem everywhere.[P] Greed and power over people.[P] I spent a lot of time with the harpies from the tropical islands west of here.[B][C]")
    Append("Esmerelda:[E|Neutral] Everyone was a friend in the flock.[P] We shared what we could.[P] The federation of flocks worked together like old friends.[B][C]")
    Append("Sanya:[E|Smirk] And you think that kind of camaraderie can scale up?[B][C]")
    Append("Esmerelda:[E|Neutral] I think so.[P] The ruling class of people will be harpies who must see the people as their flock.[P] There have been other empires with worse foundations.[B][C]")
    Append("Esmerelda:[E|Neutral] Of course, I had to convince the harpies to join me.[P] I had to train them to fight, and teach them the soldier's code of conduct.[B][C]")
    Append("Esmerelda:[E|Neutral] I couldn't have done it all alone.[P] The harpies you see here are my friends, my flock.[P] They share my vision for a peaceful future.[B][C]")
    Append("Sanya:[E|Smirk] Not the worst backstory.[P] Glad I asked.[B][C]")
end
