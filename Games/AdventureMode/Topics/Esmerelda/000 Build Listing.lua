-- |[ ===================================== Build Listing ====================================== ]|
--Builds a list of topics this NPC will respond to, and at what levels.
local sNPCName = "Esmerelda"

--Constants
local gciAvailableAtStart = 1
local gciMustBeUnlocked = -1

--Topic listing.
fnConstructTopic("Name",      "Name",      gciAvailableAtStart, sNPCName, 0)
fnConstructTopic("Join",      "Join",      gciAvailableAtStart, sNPCName, 0)
fnConstructTopic("Smugglers", "Smugglers", gciMustBeUnlocked,   sNPCName, 0)
fnConstructTopic("Titles",    "Titles",    gciMustBeUnlocked,   sNPCName, 0)
