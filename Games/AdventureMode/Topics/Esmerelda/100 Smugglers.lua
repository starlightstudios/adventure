-- |[ ================================= Esmerelda: Smugglers! ================================== ]|
--Asking about letting the bunnies through.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Esmerelda")

-- |[Dialogue]|
--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Variables.
    local iHasHarpyForm       = VM_GetVar("Root/Variables/Global/Sanya/iHasHarpyForm", "N")
    local iEmpressJoined      = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
    local iGotBunnyClearance  = VM_GetVar("Root/Variables/Chapter2/BunnyWarrens/iGotBunnyClearance", "N")
    local iGaveBunnyClearance = VM_GetVar("Root/Variables/Chapter2/BunnyWarrens/iGaveBunnyClearance", "N")
	
    -- |[First Pass]|
    if(iGotBunnyClearance == 0.0) then
    
        --Flag.
        VM_SetVar("Root/Variables/Chapter2/BunnyWarrens/iGotBunnyClearance", "N", 1.0)
        WD_SetProperty("Unlock Topic", "Harpy Offer", 1)
    
        --Dialogue.
        Append("Sanya:[E|Smirk] Hey chief, I've got a favour to ask.[B][C]")
        Append("Esmerelda:[E|Neutral] Oh?[P] What is it?[B][C]")
        Append("Sanya:[E|Neutral] I need you to let the bunny smugglers through the gate in...[B][C]")
        Append("Izuna:[E|Neutral] Granvire pass.[B][C]")
        Append("Sanya:[E|Neutral] Right, that one.[B][C]")
        Append("Esmerelda:[E|Neutral] Ah yes, the bunnies.[P] They don't ask questions and will do anything for money.[B][C]")
        Append("Esmerelda:[E|Neutral] The problem is, they have no allegiances.[P] The bandits are happy to purchase from them as much as we are.[P] I cannot allow them free access to Westwoods.[B][C]")
        Append("Esmerelda:[E|Neutral] It's the primary approach to Trafal from the south, and to the trade routes to Trannadar in the north.[P] If weaponry finds its way to the bandits, it will cost us lives.[B][C]")
        Append("Sanya:[E|Neutral] That's the thing.[P] Their boss, Angelface, says she's not running weapons.[B][C]")
        Append("Esmerelda:[E|Neutral] And you believed her?[B][C]")
        Append("Izuna:[E|Neutral] She's right, Sanya.[P] Why should we believe what Angelface said?[B][C]")
        Append("Esmerelda:[E|Neutral] And why do you need this favour?[B][C]")
        Append("Sanya:[E|Sad] Uhhh...[B][C]")
        if(iEmpressJoined == 1.0) then
            Append("Empress:[E|Neutral] Do not hide your intentions if you wish to have a long-term alliance with the harpies.[P] Tell her the truth.[B][C]")
            Append("Izuna:[E|Explain] Honesty is the best policy![B][C]")
            Append("Sanya:[E|Sad] Yeah, but what if she says no?[B][C]")
            Append("Izuna:[E|Neutral] That's...[P] the whole point.[B][C]")
            Append("Sanya:[E|Neutral] It's a joke, Izuna.[P] Sheesh.[B][C]")
        else
            Append("Izuna:[E|Explain] Just tell her right out![P] The elder says to always be honest![B][C]")
            Append("Zeke:[E|Neutral] Bah![B][C]")
            Append("Sanya:[E|Neutral] Okay, okay.[B][C]")
        end
        Append("Sanya:[E|Neutral] Angelface said she won't turn me into a bunny if I don't.[P] Also, all the other bunnies are dudes.[B][C]")
        Append("Esmerelda:[E|Neutral] Dudes?[B][C]")
        Append("Sanya:[E|Neutral] Bros.[P] Fellas.[P] Guys.[B][C]")
        Append("Izuna:[E|Neutral] She partially transforms them for her own reasons.[P] Since they are not actually bunny partirhumans, they can't transform Sanya.[P] Only Angelface can.[B][C]")
        Append("Esmerelda:[E|Neutral] I see.[P] So she requested this favour in exchange for a new form from you.[B][C]")
        Append("Esmerelda:[E|Neutral] I will offer a compromise.[P] The bunnies are free to move to the north through the gate.[P] If they wish to move south, they will need to be searched.[B][C]")
        Append("Esmerelda:[E|Neutral] We will only confiscate weapons.[P] The bunnies are allowed one personal defense weapon per bunny.[P] If they have a problem with that, they can find another way.[B][C]")
        Append("Esmerelda:[E|Neutral] I don't particularly like having to maintain an outpost there for this purpose, so you must realize it is a serious issue.[P] Once the bandits are quelled, I will leave the bunnies to their devices.[B][C]")
        Append("Izuna:[E|Jot] I wrote it down.[P] What do we do if she rejects the offer?[B][C]")
        Append("Sanya:[E|Neutral] Tell her she can do the negotiations herself, is my opinion.[B][C]")
        Append("Esmerelda:[E|Neutral] I would be willing to meet in a neutral location or through another mediator.[B][C]")
        if(iEmpressJoined == 1.0) then
            Append("Empress:[E|Neutral] Smugglers are concerned with their bottom line, not the morals of the situation.[P] Make it clear this is a business cost.[B][C]")
            Append("Esmerelda:[E|Neutral] Good advice.[B][C]")
        end
        Append("Sanya:[E|Happy] Okay, we'll let Angelface know.[P] Next time you see me, I'll be a lot fuzzier![B][C]")
        Append("Zeke:[E|Neutral] Nyeh![B][C]")
        Append("Sanya:[E|Smirk] Don't worry, buddy.[P] You're still my number one fuzzball.[B][C]")
    
    -- |[Repeats]|
    else
    
        -- |[Hasn't Told Angelface]|
        if(iGaveBunnyClearance == 0.0) then
            Append("Esmerelda:[E|Neutral] I am hoping the bunnies accept my offer.[P] If anything, it is for their sakes, too.[B][C]")
            if(iEmpressJoined == 1.0) then
                Append("Empress:[E|Neutral] Bandits do not hew to their words.[P] If this roving gang turned army is as you say, the smugglers will be dispatched when they are no longer useful.[B][C]")
                Append("Esmerelda:[E|Neutral] Exactly what I was thinking.[P] I don't think she'd let that happen, but they'd have to move their operations.[B][C]")
            end
            Append("Esmerelda:[E|Neutral] Please deliver my offer as soon as possible, Sanya.[B][C]")
            Append("Sanya:[E|Smirk] You got it, chief.[B][C]")
    
        -- |[Has Told Angelface]|
        else
            WD_SetProperty("Unlock Topic", "Titles", 1)
            Append("Esmerelda:[E|Neutral] I received a notice that the bunnies accepted our offer.[P] Well done, Sanya.[B][C]")
            Append("Sanya:[E|Happy] No problem, chief![P] I got bunny'd from it, too![P] One more set of cool powers![B][C]")
            if(iHasHarpyForm == 0.0) then
                Append("Esmerelda:[E|Neutral] You don't need to call me chief, you know.[B][C]")
                Append("Sanya:[E|Happy] It's fun![P] Also 'Esmerelda' sounds too formal.[B][C]")
                Append("Esmerelda:[E|Neutral] I suppose so.[P] I kind of like how it sounds.[P] It's certainly shorter than 'Flock Leader'.[B][C]")
                Append("Sanya:[E|Neutral] Does everyone else call you flock leader?[B][C]")
                Append("Esmerelda:[E|Neutral] It's part of military discipline.[P] Not because the title is important, but because the person filling the role changes.[B][C]")
                Append("Esmerelda:[E|Neutral] If someone else were to become a flock leader, you'd need to give them same respect, even if you don't know them.[P] We need to reorganize to meet new threats.[B][C]")
                Append("Esmerelda:[E|Neutral] It's when people start to believe that the title, well, entitles them to respect.[P] They put the title first.[P] That's when you've got a problem.[B][C]")
                Append("Esmerelda:[E|Neutral] Perhaps I will take on the title of 'chief'.[P] It sounds much niftier.[P] Thank you![B][C]")
            else
                Append("Esmerelda:[E|Neutral] Chief, eh...[B][C]")
                Append("Esmerelda:[E|Neutral] I trust the impressive speed of the bunnies will be of use during your journey.[B][C]")
                Append("Izuna:[E|Smirk] She could also use her shocking cuteness in negotiations.[B][C]")
                Append("Sanya:[E|Smirk] We already have Zeke for that.[B][C]")
                Append("Zeke:[E|Neutral] Baaaah...[B][C]")
            end
            Append("Esmerelda:[E|Neutral] I hope our relationship with the bunnies can be productive.[P] I've already been seeing the soldiers acquiring comfort items, like medicine, clothes and alcohol, from them.[B][C]")
            Append("Esmerelda:[E|Neutral] I don't mind, of course.[P] I just don't want them to become reliant on the bunnies.[P] That'd give them leverage over us.[B][C]")
            Append("Sanya:[E|Sad] Yeah, you'd look like the fun police if you had to crack down on them.[B][C]")
            if(iEmpressJoined == 1.0) then
                Append("Empress:[E|Neutral] Angelface intends to use this against you.[P] I've seen it many times.[B][C]")
                Append("Empress:[E|Neutral] Acquire an alternate stream of supplies.[P] Smugglers always have rivals and splinter groups.[P] Make them compete.[P] Make their biggest problem each other, not you.[B][C]")
                Append("Esmerelda:[E|Neutral] Another excellent piece of advice.[B][C]")
            end
            Append("Esmerelda:[E|Neutral] In any case, I'm glad you got the bunny form.[P] I'm sure it will get you out of a tight spot.[B][C]")
        end
    end
end
