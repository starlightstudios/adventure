-- |[ ================================ Crowbar-Chan: Your Name ================================= ]|
--Asking about her name. Or rather, new name.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "CrowbarChan")

--Common.
WD_SetProperty("Major Sequence Fast", true)
	
--Dialogue.
Append("Sanya:[E|Smirk] So CC.[P] I noticed you're going by...[P] CC.[P] Whatup?[B][C]")
Append("CC:[E|Neutral] Nyuu aren't going to believe this, Sanya, but I changed my name to fit in.[P] Doesn't sound like me at all, right?[B][C]")
Append("Sanya:[E|Smirk] Are you kidding?[P] All you ever do is try to fit in.[B][C]")
Append("CC:[E|Neutral] Oh yeah, I guess so.[P] It just never works.[P] CC sounds really cool, like a mobster name, right?[B][C]")
Append("Sanya:[E|Blush] Have you considered 'Legs'?[B][C]")
Append("CC:[E|Neutral] I don't even have legs, Sanya.[B][C]")
Append("Sanya:[E|Neutral] Oh yeah, right.[B][C]")