-- |[ ============================ Crowbar-Chan: History with Sanya ============================ ]|
--Izuna has some pertinent questions.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "CrowbarChan")

--Common.
WD_SetProperty("Major Sequence Fast", true)
	
--Dialogue.
Append("Izuna:[E|Neutral] Um, this may sound odd, but how do you two know each other?[P] I was under the impression that Sanya...[P] was from Earth.[B][C]")
Append("CC:[E|Neutral] She is, I think.[P] I've never been.[B][C]")
Append("Sanya:[E|Neutral] Earth sucks.[P] Chili is good, but everything else sucks.[B][C]")
Append("CC:[E|Neutral] We met in classic mode.[P] It was actually a one-off part-time gig I got.[B][C]")
Append("Izuna:[E|Cry] Classic...[P] mode?[B][C]")
Append("CC:[E|Neutral] Yeah, it was steady work, which I really needed.[P] I'm talking about the original Inform one, but I also got the role in the remake.[B][C]")
Append("Izuna:[E|Jot] (None of it makes sense but maybe if I write it down...)[B][C]")
Append("CC:[E|Neutral] There was a poll for who went first in the big Adventure Mode thing.[P] Mei won, and Sanya and I placed at the bottom.[B][C]")
Append("Sanya:[E|Offended] I'm still pretty pissed about that![B][C]")
Append("CC:[E|Neutral] Me too, sister![P] Come on, I'm way cuter than Jeanne![B][C]")
Append("Sanya:[E|Happy] I voted for ya, comrade.[B][C]")
Append("CC:[E|Neutral] So we became friends more or less as a result of that.[P] I mean, I was 'friends' with everyone in Classic, but you know, work friends.[P] Not friends friends.[B][C]")
Append("CC:[E|Neutral] I drifted around after that, because I had bills to pay.[P] Got a couple side gigs.[P] I was in some of the Hitman games, got auditioned for Black Mesa...[B][C]")
Append("Sanya:[E|Blush] Yeah she emailed me that they basically didn't even call her back, despite her just nailing the audition.[B][C]")
Append("CC:[E|Neutral] I did Risk of Rain but not the sequel...[P] Was in some hillbilly game I can't remember...[B][C]")
Append("CC:[E|Neutral] I even auditioned for Phantom Pain but they went with Sutherland.[P] Didn't even give me a role as the prosthetic arm![P] My career was a bunch of near-misses.[B][C]")
Append("CC:[E|Neutral] So anyway, I was trying out for some shitty shampoo commercial or something.[P] 20 bucks just to show up even if they didn't use me.[P] Whatever.[B][C]")
Append("CC:[E|Neutral] I'm in the back and the craft services table is just, like, candy bars and bottled stuff.[P] But it comes in crates.[P] I get to talking with a bunny who was also trying out.[B][C]")
Append("CC:[E|Neutral] He needed a juice box, they're all gone, and I pop a crate open like it's nothing.[P] He's impressed.[P] One thing leads to another...[B][C]")
Append("CC:[E|Neutral] Turns out, the bunny smugglers around here need a crate specialist.[P] I can open 'em, close 'em, spin 'em around.[P] You want the best?[P] Here I am.[B][C]")
Append("CC:[E|Neutral] Of course I do cameos in Runes of Pandemonium, but it ain't for the money, you know?[P] I've got a damn good thing going at this gig.[B][C]")
Append("Sanya:[E|Smirk] I'm happy if you're happy, CC.[P] Oh, and none of this is canon, Izuna.[B][C]")
Append("Izuna:[E|Jot] (Just total gibberish...[P] There is no pattern to the madness.)[B][C]")