-- |[ ===================================== Build Listing ====================================== ]|
--Builds a list of topics this NPC will respond to, and at what levels.
local sNPCName = "CrowbarChan"

--Constants
local gciAvailableAtStart = 1
local gciMustBeUnlocked = -1

--Topic listing.
fnConstructTopic("Name",    "Name",    gciAvailableAtStart, sNPCName, 0)
fnConstructTopic("History", "History", gciAvailableAtStart, sNPCName, 0)
