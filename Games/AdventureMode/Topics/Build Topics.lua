-- |[ ====================================== Build Topics ====================================== ]|
--At AdventureMode startup, builds the topic listing.
local sBasePath = fnResolvePath()
local saList = {}

-- |[Chapter 1]|
table.insert(saList, "Backers")
table.insert(saList, "Blythe")
table.insert(saList, "Breanne")
table.insert(saList, "CombatBooks")
table.insert(saList, "EvermoonNWBookshelf")
table.insert(saList, "Florentina")
table.insert(saList, "Karina")
table.insert(saList, "Miho")
table.insert(saList, "Nadia")
table.insert(saList, "Rochea")
table.insert(saList, "Septima")
table.insert(saList, "Sharelock")
table.insert(saList, "Mycela")
table.insert(saList, "Polaris")
table.insert(saList, "Xanna")

-- |[Chapter 2]|
table.insert(saList, "BackersKitsune")
table.insert(saList, "CrowbarChan")
table.insert(saList, "Angelface")
table.insert(saList, "Esmerelda")
table.insert(saList, "Izuna")
table.insert(saList, "Empress")
table.insert(saList, "Mia")
table.insert(saList, "Miso")
table.insert(saList, "Yukina")
table.insert(saList, "Zeke")

-- |[Chapter 5]|
table.insert(saList, "300910")
table.insert(saList, "Backers5")
table.insert(saList, "BackersRaiju")
table.insert(saList, "BackersRepressed")
table.insert(saList, "BackersManufactory")
table.insert(saList, "ChristineTerminal")
table.insert(saList, "PipeNightmare")
table.insert(saList, "Night")
table.insert(saList, "Sophie")
table.insert(saList, "SX399")
table.insert(saList, "Tiffany")
table.insert(saList, "TiffanyBiolabs")
table.insert(saList, "TiffanyBiolabs")

-- |[Multi-Purpose]|
table.insert(saList, "ZGeneral")

--Debug: If this is not nil, a list of all unique topics will be printed.
--gsaUniqueTopics = {}

-- |[ ================================== Execute Topic Files =================================== ]|
--Now set as necessary.
local i = 1
while(saList[i] ~= nil) do
	LM_ExecuteScript(sBasePath .. saList[i] .. "/000 Build Listing.lua")
	i = i + 1
end

--Once the topic listing is done, get a list of all unique topics. Write them to the console.
if(gsaUniqueTopics ~= nil) then
	local i = 1
	while(gsaUniqueTopics[i] ~= nil) do
		io.write("Topic " .. i - 1 .. ": " .. gsaUniqueTopics[i] .. "\n")
		i = i + 1
	end
end
	
