-- |[ ===================================== Topic Launcher ===================================== ]|
--Called when the player selects a topic during dialogue. Resolves where the topic file is and
-- executes it. Also checks mods to see if any of them handled it.
--The script expects two arguments, describing the NPC name reciving the call, and the name of the selected topic.

-- |[Arguments]|
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sNPCName   = LM_GetScriptArgument(0)
local sTopicName = LM_GetScriptArgument(1)

--Debug
--io.write("Received topic call. Name: " .. sNPCName .. " Topic: " .. sTopicName .. "\n")

-- |[Base Topics]|
--Check if this topic exists in the base folder.
local sTopicPath = fnResolvePath() .. sNPCName .. "/100 " .. sTopicName .. ".lua"
--io.write(" Topic path: " .. sTopicPath .. "\n")
if(FS_Exists(sTopicPath) == true) then
    LM_ExecuteScript(sTopicPath, "Hello")
    --io.write(" Handled by base game.\n")
    return
end

-- |[Modded Topics]|
--Not in the base game? Check if any of the mods catch it. If any file is executed by the
-- routine, gbModExecutedAnything is tripped.
fnExecModScript("Topics/" .. sNPCName .. "/100 " .. sTopicName .. ".lua", "Hello")
if(gbModExecutedAnything == true) then return end

-- |[Error]|
--Could not handle topic call.
WD_SetProperty("Hide")
io.write(" Error: Failed to handle topics call.\n")
