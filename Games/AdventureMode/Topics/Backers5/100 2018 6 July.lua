-- |[Alraunes]|
--Asking this NPC about Alraunes in general.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Backers5")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	Append("Christine:[E|Smirk] The commendations for July went to #912792 'Christian Gross', #917114 'MarioneTTe', #900102 'RepeatedMeme', and Unit #903293 'Diana Nonya'. And then, out of nowhere, Unit #999000 'Marek' appeared![P] Way to go![B][C]")
end
