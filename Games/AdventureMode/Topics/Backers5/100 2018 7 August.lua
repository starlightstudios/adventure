-- |[Alraunes]|
--Asking this NPC about Alraunes in general.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Backers5")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	Append("Christine:[E|Smirk] The commendations for August went to #912792 'Christian Gross', #917114 'MarioneTTe', #900102 'RepeatedMeme', Unit #903293 'Diana Nonya' and Unit #999000 'Marek'.[B][C]")
	Append("Christine:[E|Smirk] It also heralded the triumphant return of #901922 'Austin Durbin', and the incredible Prime Command Unit Exemplary Service Award for Outstanding Productivity, Unit #901157 'Klaysee'.[P] I heard she did such a good job they even named a category of electrical phenomena after her![B][C]")
end
