-- |[Alraunes]|
--Asking this NPC about Alraunes in general.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Backers5")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	Append("Christine:[E|Smirk] In December, we had #901922 'Austin Durbin', #917114 'MarioneTTe', #900102 'RepeatedMeme'...[B][C]")
	Append("Christine:[E|Neutral] Scroll down a bit...[B][C]")
	Append("Christine:[E|Neutral] Yadda yadda yadda...[P] 'Proud of contribution'...[B][C]")
	Append("Christine:[E|Smirk] Unit 966403 'Gaming Chocobro' and Unit 993012 'Abrissgurke'[B][C]")
	Append("Christine:[E|Laugh] Unit #901157 'Klaysee' has achieved such success that she's getting repurposed![P] We are proud of our new Lord Unit sister![B][C]")
	Append("Christine:[E|Neutral] ...[B][C]")
	Append("Christine:[E|Offended] Wait - [P]Unit 901157 was flagged as a Drone Unit before that?[P] What the hay?[B][C]")
end
