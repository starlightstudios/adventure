-- |[Alraunes]|
--Asking this NPC about Alraunes in general.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Backers5")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	Append("Christine:[E|Smirk] Starting to get a bit of a rogues gallery here.[B][C]")
	Append("Christine:[E|Smirk] Longtime achievers #901922 'Austin Durbin', #912792 'Christian Gross', #917114 'MarioneTTe', #900102 'RepeatedMeme' return...[B][C]")
	Append("Christine:[E|Smirk] Unit #903293 'Diana Nonya', Unit #999000 'Marek' and Unit 966403 'Gaming Chocobro' made the cut again, well done![B][C]")
	Append("Christine:[E|Smirk] And then, once again deserving special mention is 'Klaysee', Unit #901157, who has stunned us all yet again with her research into dragon mating habits.[B][C]")
	Append("Christine:[E|Smirk] It's not every day a unit manages to bargain someone into having on-camera sex with a mountain of rock candy, so I think the reward is pretty well deserved![P] Good work![B][C]")
	Append("Christine:[E|Blush] Wait the phrasing is unclear.[P] Did she bargain using the rock candy, or did the dragon have sex with the rock candy?[P] Oh dear...[B][C]")
end
