-- |[Backers]|
--Backer terminal.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "BackersRepressed")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Dialogue.
	Append("Christine:[E|Neutral] A repressed memory of me in a role-playing session on the internet...[P] I would have been a teenager at the time...[B][C]")
	Append("Christine:[E|Neutral] I was playing as Mina 'Klaysee' Jones, with my two friends Bill 'Kumquat' Allen and Marcy 'Sudoku-head' Bright.[P] We were in the woods exploring one day when a storm hit and we were forced to find shelter.[B][C]")
	Append("Christine:[E|Neutral] We found a strange mansion that had never been there, and soon found we just couldn't leave.[P] All paths looped back to the manor.[B][C]")
	Append("Christine:[E|Neutral] Inside, we found a strange lady.[P] She told us her name was 'Aaaac', and of course we didn't believe her.[P] She didn't seem to be all there.[B][C]")
	Append("Christine:[E|Neutral] Naturally we started exploring, looking for loot and trying to solve the mystery of the manor.[B][C]")
	Append("Christine:[E|Neutral] They got my character first.[P] There were strange creatures in the halls, I failed a few rolls and got split off from the others.[B][C]")
	Append("Christine:[E|Neutral] I found a room with enormous stained-glass windows.[P] There were portraits made of the stained glass.[P] 'Taedas', 'Sinpai', 'Dyamonde'...[B][C]")
	Append("Christine:[E|Neutral] A monster grabbed me and I failed my struggle roll.[P] The monster held me in front of an empty stained glass window with no portrait, until my character began to empty out.[B][C]")
	Append("Christine:[E|Neutral] She turned clear as glass, and a portrait of her appeared in the stained glass.[P] The DM informed me that I was to find my former friends and capture them.[B][C]")
	Append("Christine:[E|Neutral] The other monsters then painted me to look like I had as a human.[P] I found my friends poring over a book written by Abrissgurke, a philosopher from the 12th century.[B][C]")
	Append("Christine:[E|Neutral] I played along.[P] The book held important information about the manor.[P] It told the story of Winston 'Upton' Smith, and how he escaped the manor.[P] They read it and figured they had a chance.[B][C]")
	Append("Christine:[E|Neutral] The book said we needed to find another, special book that held a spell.[P] I said I knew where it was.[P] I led them into an ambush, and in the confusion, I took down Kumquat and dragged him away.[B][C]")
	Append("Christine:[E|Neutral] The other monsters transformed him into a gothic doll girl, given the same mission as me.[P] At this point, the DM, RepeatedMeme, told me to drop my disguise.[P] I resumed my glass form and we began hunting Sudoku-head.[B][C]")
	Append("Christine:[E|Neutral] We broke up for a bit to get some snacks and use the bathroom, and had a couple more players join.[P] 'Lew' managed to go all of six turns before getting turned into a living statue girl, and 'Dyamonde' accidentally fell into a pit that turned him into a girl made of clay.[P] So Sudoku-head was pretty screwed at this point, with the entire server working against her.[B][C]")
	Append("Christine:[E|Neutral] Kumquat caught up to her first but lost a few rolls and got defeated.[P] Sudoku had found the magic tome, which was the diary of Austin Durbin, the creator of the manor.[B][C]")
	Append("Christine:[E|Neutral] If she could reach 'Aaaac' before the rest of us, she'd win the game.[P] All of us monstergirls had to stop her.[B][C]")
	Append("Christine:[E|Neutral] Lew, being a living statue, basically tried to guard the attic where 'Aaaac' was waiting.[P] Dyamonde disguised herself as a health potion nearby, and I concealed myself in the darkness.[B][C]")
	Append("Christine:[E|Neutral] Sudoku walked right into the trap but used a magic spell to freeze Dyamonde.[P] I got behind her and passed a grab roll with a natural 20.[P] I sleeper'd her until she passed out.[B][C]")
	Append("Christine:[E|Neutral] When she woke up, she was a pretty geisha doll.[P] The monsters won.[B][C]")
	Append("Christine:[E|Neutral] I had a nightmare a few days later about the transformation into a stained-glass girl, which is probably why I repressed the memory.[P] I had come down with a flu and fever dreams are just awful.[B][C]")
	Append("Christine:[E|Neutral] ...[P] Who would have thought I would wind up in Pandemonium where this sort of thing isn't just roleplaying?[B][C]")
end
