-- |[Build Listing]|
--Builds a list of topics this NPC will respond to, and at what levels. Called at Adventure Mode startup.
local sNPCName = "BackersRepressed"

--Chapter 5 backers.
fnConstructTopic("2019 A July",      "July 2019",      1, sNPCName, 0)
fnConstructTopic("2019 B August",    "August 2019",    1, sNPCName, 0)
fnConstructTopic("2019 C September", "September 2019", 1, sNPCName, 0)
fnConstructTopic("2019 D October",   "October 2019",   1, sNPCName, 0)
