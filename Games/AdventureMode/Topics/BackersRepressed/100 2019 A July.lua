-- |[Backers]|
--Backer terminal.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "BackersRepressed")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	Append("Christine:[E|Neutral] Let's see...[P] Well, the memory is mostly intact, but the names are all over the place...[B][C]")
	Append("Christine:[E|Neutral] I was visiting the department store...[P] I don't recall its name.[P] It's in downtown London...[P] Lew's?[P] Lew's Clothes and Such?[B][C]")
	Append("Christine:[E|Neutral] Mother had picked out some shoes and we were on our way out when we heard a commotion.[P] A lady my mother was friends with, Sinpai, with was having an argument with a customer.[B][C]")
	Append("Christine:[E|Neutral] The customer was screaming that birdseed was two pounds more expensive here than in the store three blocks over, Upton's.[P] Complaining about price in downtown London, well that's a first.[B][C]")
	Append("Christine:[E|Neutral] The lady kept ranting about the birdseed and Sinpai turned to my mother and mouthed 'Help me'.[P] The customer didn't even notice.[B][C]")
	Append("Christine:[E|Neutral] Mother shrugged to say 'What do you want me to do?'[B][C]")
	Append("Christine:[E|Neutral] We stood there in solidarity for another five minutes until eventually the customer gave up and left.[P] The store manager, Mr. Durbin, came up to console Ms. Sinpai afterwards.[P] She looked like she was about to cry.[B][C]")
	Append("Christine:[E|Neutral] As we left, the rain had let up.[P] It's London, so this literally never happens.[P] The two famous street performers, Sudoku and Abrissgurke, were outside doing their juggling act.[P] We stopped to watch them.[B][C]")
	Append("Christine:[E|Neutral] And then we heard the screaming.[B][C]")
	Append("Christine:[E|Neutral] The entire crowd turned to look at the same lady we had seen before, now screaming her head off at a traffic officer who was giving her a ticket.[B][C]")
	Append("Christine:[E|Neutral] Officer RepeatedMeme explained, slowly and patiently over the screaming, that you can't, in fact, double park.[P] Anywhere.[P] At any time.[P] Because it's illegal.[B][C]")
	Append("Christine:[E|Neutral] After about ten minutes of furtive yelling, two more police, Officer Klaysee and Officer Kumquat, showed up.[P] They also tried to patiently explain the simple laws of parking.[B][C]")
	Append("Christine:[E|Neutral] Because yelling very loudly had not worked, the lady decided to yell even louder.[P] At that point, another friend of my mother's, Taedas, came up behind the lady and socked her.[P] She dropped like a brick.[B][C]")
	Append("Christine:[E|Neutral] Taedas was later arraigned on charges of assault, but strangely, Judge Dyamonde dismissed the case when literally no witnesses could be found.[P] Somehow, all three officers and a dozen bystanders had somehow been looking the other way at the exact moment of the assault.[B][C]")
	Append("Christine:[E|Neutral] I think I repressed the memory due to the sheer volume of the shouting, but damned if that wasn't a happy ending to the story.[B][C]")
    
end
