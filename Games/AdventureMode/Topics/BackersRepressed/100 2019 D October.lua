-- |[Backers]|
--Backer terminal.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "BackersRepressed")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Dialogue.
	Append("Christine:[E|Neutral] Sheesh, this one is of a crime wave I experienced when I was a teenager.[B][C]")
	Append("Christine:[E|Neutral] Going to a private school with rich girls means nobody steals something because they need it, but to send a message.[B][C]")
	Append("Christine:[E|Neutral] It was the end of the school day.[P] My school had a bussing system set up that ran between the dorms and the school, for all the foreign students.[P] Normally I took a private car, but that day I had to take the bus.[B][C]")
	Append("Christine:[E|Neutral] I was talking with my friend Sinpai and we had some other friends on the second floor.[P] The bus would be by in a few minutes and they hadn't come downstairs yet.[B][C]")
	Append("Christine:[E|Neutral] I left my bag with Sinpai to see what was taking them so long.[P] I went upstairs and found them hanging out at the balcony.[B][C]")
	Append("Christine:[E|Neutral] Durbin, Upton, and Aaaac were just goofing off.[P] I chided them for a bit.[P] The bus was late.[B][C]")
	Append("Christine:[E|Neutral] Sinpai comes up and we start chatting.[P] After a few minutes I ask 'why aren't you watching the bags' and she just shrugged.[B][C]")
	Append("Christine:[E|Neutral] So I go back downstairs.[P] My bag is gone.[P] Just mine, nobody else's.[B][C]")
	Append("Christine:[E|Neutral] My Gamequipment 3D 'MJS' was in that bag, along with a bunch of...[P] fetishy art...[P] from my favourite artists, like Sudoku and Abrissgurke.[P] I was into some weird stuff.[B][C]")
	Append("Christine:[E|Neutral] Like handholding and legitimate affection.[P] I was such a kinky kid.[B][C]")
	Append("Christine:[E|Neutral] Years later, I had a boyfriend.[P] Kumquat Edelburg, god he was so buff...[B][C]")
	Append("Christine:[E|Neutral] He gave me a birthday present of a new Gamequipment 3D.[P] I had never replaced it, but there were so many games I had wanted to play, like Squid Squad Mobile and Rip'N'Tear 2016.[B][C]")
	Append("Christine:[E|Neutral] A week later, we broke up.[B][C]")
	Append("Christine:[E|Neutral] I found out that he thought I didn't like the gift despite the fact that I played it constantly.[B][C]")
	Append("Christine:[E|Neutral] Then my new Gamequipment 3D 'MJS' got stolen, and we all knew he had done it.[P] He tried to pretend it was Klaysee, the girl who hangs out at a cafe we would go to sometimes.[B][C]")
	Append("Christine:[E|Neutral] I had a few dates with her, actually.[P] I had a phase where I was into quiet girls.[P] I wonder what happened to her?[B][C]")
	Append("Christine:[E|Neutral] And then I had a Gamequipment Lite which got stolen three weeks later while I was at a gaming club.[P] I went to the bathroom and came back and it was gone.[B][C]")
	Append("Christine:[E|Neutral] That one I did report to the police.[P] Officer Steven, I remember he was the one who took my statement.[P] He was so buff.[P] I was a 16-year-old girl. If I had been older...[B][C]")
	Append("Christine:[E|Neutral] Okay I definitely have a thing for muscled guys.[P] That settles that.[P] Girls are better, but I will accept a built guy.[B][C]")
	Append("Christine:[E|Neutral] RepeatedMeme had just gotten me a copy of Murder Fist, and I had already put 90 hours into it, so all that progress was lost when it was stolen.[B][C]")
	Append("Christine:[E|Neutral] From then on, no mobile consoles for me.[P] I would just have Lew, Dyamonde, and Taedas over and we'd play Brawl Brothers.[P] I guess that's how I got so good at it.[B][C]")
	Append("Christine:[E|Neutral] Maybe I repressed the memory because, well, when people steal, they send a message.[P] I had a lot of enemies at that school...[B][C]")

end
