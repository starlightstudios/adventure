-- |[Backers]|
--Backer terminal.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "BackersRepressed")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Dialogue.
	Append("Christine:[E|Neutral] A repressed memory from my days as a teenager.[P] Our chef was sick and I had decided to take matters into my own hands.[B][C]")
	Append("Christine:[E|Neutral] I had some guests over to watch movies and we had some brownies left over.[P] Not being an expert on matters culinary, I didn't notice they were stale and stuck to the pan.[P] I was also a teenager, and dumb.[B][C]")
	Append("Christine:[E|Neutral] I struggled with the spatula for a while.[P] Abrissgurke came in and asked if I needed help, but I shooed her off.[P] For some reason, struggling with the brownies redoubled my determination to handle it myself.[B][C]")
	Append("Christine:[E|Neutral] Then the spatula broke off at the handle, with the brownies still firmly on the pan.[P] I checked the label.[P] 'Durbin Industries'.[P] I've had an irrational hatred of that company ever since.[B][C]")
	Append("Christine:[E|Neutral] Not to be dissuaded, I pulled out a butcher's knife and used that instead.[P] It slid cleanly under the brownies.[P] In fact it was so effective that I didn't even notice it slicing my pinky finger open until it was far too late.[B][C]")
	Append("Christine:[E|Neutral] Blood poured all over my arm and down, and I just started laughing.[P] I dropped the brownies and the pan bounced and clanged, with the brownies still firmly attached.[B][C]")
	Append("Christine:[E|Neutral] Mother began to panic, but Taedas kept a level head and called the ambulance.[B][C]")
	Append("Christine:[E|Neutral] I found out later that a representative of the government came to interview my friends about it, believing it to be child negligence.[P] Quite a charge to level against a family like mine, but Mr. Upton eventually left satisfied it was my own stupidity and not mother's.[B][C]")
	Append("Christine:[E|Neutral] I went to the hospital and didn't have to wait long before a haggard looking doctor, named Ellie Sinpai, came in to stitch me up.[B][C]")
	Append("Christine:[E|Neutral] She was clearly in a hurry and overworked.[P] If any British person ever complains about the NHS, it's because they need *more* doctors, not less![P] Pay your taxes![B][C]")
	Append("Christine:[E|Neutral] I got a shot to numb my hand and she stitched me up, but a few seconds in I feel everything.[P] I scream as the pain kicks in.[B][C]")
	Append("Christine:[E|Neutral] I get a second shot and this time it finally numbs me.[P] Dr. Sinpai finishes the stitches and runs off.[B][C]")
	Append("Christine:[E|Neutral] So I'm sitting there and my hand starts throbbing as the numbing agent wears off.[P] Another doctor, or nurse?[P] Nurse Kumquat?[P] She comes in and says they did the stitches wrong.[B][C]")
	Append("Christine:[E|Neutral] Nurse Kumquat numbs my hand -[P] again -[P] and pulls the stitches out.[P] Then Dr. Klaysee comes in, numbs my hand for the fourth time and does the stitches up.[B][C]")
	Append("Christine:[E|Neutral] Finally I get discharged with a big horrible cast on my hand that makes it hard to move.[P] So of course the next day was the day of the big Brawl Brothers tournament.[B][C]")
	Append("Christine:[E|Neutral] So obviously, Repeated Meme thinks today is his day.[P] This is the day he takes me down.[P] I play with the damned cast on.[B][C]")
	Append("Christine:[E|Neutral] RM gets knocked out in the semi-finals and I take out Lew and Sudoku to take the tournament because I will be screwed sideways before I let anyone say they're better than me at Brawl Brothers.[B][C]")
	Append("Christine:[E|Neutral] Aaaac gives me the prize of three-hundred pounds, which I don't need, and a PlumberPal limited run figurine of Dyamonde from Needlemouse Heroes.[P] I had it on my shelf until the day I came to Regulus.[B][C]")
	Append("Christine:[E|Neutral] I remain the undisputed champion of Brawl Brothers.[P] Fight me.[B][C]")
    
end
