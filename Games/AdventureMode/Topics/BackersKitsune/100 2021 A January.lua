-- |[Backers]|
--Kitsune Backers
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "BackersKitsune")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Dialogue.
	Append("Thought: Supporters for January 2021 were...[P] Vexches, Fakein, David, Damaso Simal Castro, Christian Adalbert, Reiella, Goop-Sinpai, Erractic...[B][C]")
	Append("Thought: Dyamonde, James Upton, Abrissgurke, Pandavenger, xyzzyzzyxxysym, Klaysee, Aaaac, Austin Durbin.[B][C]")
	Append("Thought: Maybe someday all of these people will be famous throughout the galaxy as the people who made a game happen that changed the world.[B][C]")
	Append("Thought: I mean.[P] Dragon empire.[P] Heh.[B][C]")

end

