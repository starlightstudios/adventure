-- |[Backers]|
--Kitsune Backers
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "BackersKitsune")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Dialogue.
	Append("Thought: Supporters for August 2021 were...[P] Abrissgurke, Kamendrago, Vexches, Fakein, David, Damaso Simal Castro, Christian Adalbert, Reiella, Goop-Sinpai, Erractic...[B][C]")
	Append("Thought: Dyamonde, Pokeball97, James Upton, Pandavenger, Meryl Mabry, Squish, xyzzyzzyxxysym, Klaysee, Aaaac, Austin Durbin.[B][C]")
	Append("Thought: I love all of them and they deserve to get free guac on the side of every order they make, forever.[B][C]")

end

