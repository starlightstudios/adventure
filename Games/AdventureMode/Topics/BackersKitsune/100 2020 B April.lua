-- |[Backers]|
--Kitsune Backers
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "BackersKitsune")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Dialogue.
	Append("Thought: Supporters for April 2020 were...[P] Oli Ku, David, Damaso Simal Castro, Christian Adalbert, Merb, Lew, Reiella...[B][C]")
    Append("Thought: Goop-Sinpai, Dyamonde, James Upton, Abrissgurke, Pandavenger, xyzzyzzyxxysym, Klaysee, Aaaac, Austin Durbin, Repeated Meme.[B][C]")
    Append("Thought: Good job all around.[P] Splendid work.[B][C]")

end
