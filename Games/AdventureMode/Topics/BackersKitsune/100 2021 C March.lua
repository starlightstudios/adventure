-- |[Backers]|
--Kitsune Backers
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "BackersKitsune")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Dialogue.
	Append("Thought: Supporters for March 2021 were...[P] Hyena-Princess, Vexches, Fakein, David, Damaso Simal Castro, Christian Adalbert, Reiella, Goop-Sinpai, Erractic...[B][C]")
	Append("Thought: Dyamonde, Pokeball97, James Upton, Abrissgurke, Pandavenger, xyzzyzzyxxysym, Klaysee, N-0002, Aaaac, Austin Durbin.[B][C]")
	Append("Thought: Starting to notice a pattern here.[P] Namely, that all the people on the list are awesome![B][C]")

end

