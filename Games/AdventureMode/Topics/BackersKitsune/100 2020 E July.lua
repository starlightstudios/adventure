-- |[Backers]|
--Kitsune Backers
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "BackersKitsune")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Dialogue.
	Append("Thought: Supporters for July 2020 were...[P] Robert DellaFave, David, Damaso Simal Castro, Christian Adalbert, Merb, Reiella, Goop-Sinpai...[B][C]")
	Append("Thought: Dyamonde, James Upton, Abrissgurke, Pandavenger, xyzzyzzyxxysym, Klaysee, Aaaac, Austin Durbin.[B][C]")
	Append("Thought: They're the ones who make all of this possible.[P] You know, the dragon empire.[P] Yeah, that's it.[B][C]")

end

