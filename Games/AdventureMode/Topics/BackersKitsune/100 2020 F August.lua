-- |[Backers]|
--Kitsune Backers
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "BackersKitsune")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Dialogue.
	Append("Thought: Supporters for August 2020 were...[P] Robert DellaFave, David, Damaso Simal Castro, Christian Adalbert, Reiella, Goop-Sinpai.[B][C]")
	Append("Thought: Dyamonde, James Upton, Abrissgurke, Pandavenger, xyzzyzzyxxysym, Klaysee, Aaaac, Austin Durbin.[B][C]")
	Append("Thought: I'd give them all a kiss if I could.[B][C]")


end

