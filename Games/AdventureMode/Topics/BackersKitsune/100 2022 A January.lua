-- |[Backers]|
--Kitsune Backers
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "BackersKitsune")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Dialogue.
	Append("Thought: Supporters for January 2022 were...[P] Wulfbeta, Abrissgurke, Kamendrago, Vexches, Fakein, David, Damaso Simal Castro, Reiella, Goop-Sinpai, Erractic...[B][C]")
	Append("Thought: Dyamonde, Pokeball97, James Upton, Pandavenger, Meryl Mabry, xyzzyzzyxxysym, Klaysee, Aaaac, Austin Durbin, Mira.[B][C]")
	Append("Thought: BEST.[P] PEOPLE.[P] EVER![B][C]")

end

