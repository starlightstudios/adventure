-- |[Backers]|
--Kitsune Backers
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "BackersKitsune")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Dialogue.
	Append("Thought: Supporters for September 2022 were...[P] Klaysee, Kumquat, Aaack, Kamendrago, James Upton, Abrissgurke, Dyamonde, Reiella, Squeaky Sin, Pandavenger, Damaso Castro, David...[B][C]")
	Append("Thought: Erratic, Christian Adalbert, Vexches, Pokeball97, Fakeine, Anthony Carlon, Wulfbeta, Chaotix Spark, Heroforlife, Padre Bombita, and Cheren Alexander.[B][C]")
end

