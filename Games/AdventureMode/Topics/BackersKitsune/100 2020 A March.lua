-- |[Backers]|
--Kitsune Backers
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "BackersKitsune")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Dialogue.
	Append("Thought: Supporters for March 2020 were...[P] Damaso Simal Castro, Christian Adalbert, t135c, Merb, Lew, taedas, Reiella... [B][C]")
	Append("Thought: Goop-Sinpai, Dyamonde, James Upton, Abrissgurke, Pandavenger, xyzzyzzyxxysym, Klaysee, Aaaac, Austin Durbin, Repeated Meme. [B][C]")
	Append("Thought: What a bunch of swell fellows.[P] I'd buy them a beer if I weren't in an alternate dimension.[P] And this weren't a history book so they're probably dead? [B][C]")

end

