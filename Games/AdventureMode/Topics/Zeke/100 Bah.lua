-- |[Bah]|
--This is still probably not the dumbest thing I've ever written.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Zeke")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
    Append("Zeke:[E|Neutral] Bah nyeh?[B][C]")
    Append("Sanya:[E|Neutral] Gonna have to disagree with you, buddy.[B][C]")
    Append("Zeke:[E|Neutral] Bah.[B][C]")
    Append("Sanya:[E|Offended] Oh really?[P] Gonna need to see your evidence on that one![B][C]")
    Append("Zeke:[E|Neutral] Nyeh. Bah.[B][C]")
    Append("Sanya:[E|Sad] Damn.[P] I'm convinced.[B][C]")
end
