-- |[Nyeh]|
--Still not the dumbest thing ever.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Zeke")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
    Append("Zeke:[E|Neutral] Nyeeeehh...[B][C]")
    Append("Sanya:[E|Blush] You think so?[B][C]")
    Append("Zeke:[E|Neutral] Bahhhhh.[B][C]")
    Append("Sanya:[E|Blush] I'm not going to argue with a compliment.[B][C]")
end
