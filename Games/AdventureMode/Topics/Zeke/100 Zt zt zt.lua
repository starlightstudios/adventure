-- |[Zt zt zt]|
--This one is the dumbest.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Zeke")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
    Append("Zeke:[E|Neutral] *Chittering noises*[B][C]")
    Append("Sanya:[E|Happy] Bah?[B][C]")
    Append("Zeke:[E|Neutral] Zt zt.[P] Baahh![P] *spit*[B][C]")
    Append("Sanya:[E|Happy] Bahhh![P] So eloquent![B][C]")
end
