-- |[Build Listing]|
--Builds a list of topics this NPC will respond to, and at what levels. Called at Adventure Mode startup.
local sNPCName = "Zeke"
gsActiveNPCName = sNPCName

--              ||||| Unlocked At Start |||  Filename      |||  Display Name (Optional)
fnConstructTopicStd(              true, "Bah",           "Bah")
fnConstructTopicStd(              true, "Nyeh",          "Nyeh")
fnConstructTopicStd(              true, "Zt zt zt",      "Zt zt zt")
