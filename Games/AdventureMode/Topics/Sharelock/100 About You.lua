-- |[ ======================================== About You ======================================= ]|
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Sharelock")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	WD_SetProperty("Major Sequence Fast", true)
	Append("Mei:[E|Neutral] Have you had any luck finding out more about what sort of monstergirl you are?[B][C]")
	Append("Share;Lock:[E|Neutral] Not as yet, but I have a lead.[B][C]")
	Append("Share;Lock:[E|Neutral] There's a clockmaker in Trafal who knows all sorts about mechanics, metals, and tension power storage.[B][C]")
	Append("Share;Lock:[E|Neutral] I may as well see what they have to say.[B][C]")
	Append("Mei:[E|Happy] Terrific![P] So you're going to Trafal next?[B][C]")
	Append("Share;Lock:[E|Neutral] Soon, soon.[P] I'm going to see if I can gather some clues from the locals here.[P] Unfortunately, nobody recognizes me.[B][C]")
	Append("Share;Lock:[E|Thinking] Clearly I am not a local to this area.[P] Unfortunate.[B][C]")
end
