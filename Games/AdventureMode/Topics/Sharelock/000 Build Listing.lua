-- |[ ===================================== Build Listing ====================================== ]|
--Builds a list of topics this NPC will respond to, and at what levels. Called at Adventure Mode startup.
local sNPCName = "Sharelock"

--Topic listing.
fnConstructTopic("About You",    "About You",    1, sNPCName, 0)
fnConstructTopic("Saint Foras",  "Saint Foras",  1, sNPCName, 0)
fnConstructTopic("Power Supply", "Power Supply", 1, sNPCName, 0)
fnConstructTopic("Clothes",      "Clothes",      1, sNPCName, 0)
