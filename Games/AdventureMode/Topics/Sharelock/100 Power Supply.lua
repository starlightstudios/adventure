-- |[ ====================================== Power Supply ====================================== ]|
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Sharelock")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	WD_SetProperty("Major Sequence Fast", true)
	Append("Mei:[E|Neutral] Share;Lock, are you worried about running out of power?[B][C]")
	Append("Share;Lock:[E|Thinking] No.[P] Should I be?[B][C]")
	Append("Mei:[E|Surprise] But what if your batteries run out and you can't move![B][C]")
	Append("Share;Lock:[E|Neutral] Oh, I don't run on batteries![P] I run on steam![B][C]")
	Append("Share;Lock:[E|Neutral] Water has a very high specific heat, so hot steam contains a lot of energy.[P] I metabolize that energy and turn it back to cool water.[B][C]")
	Append("Mei:[E|Happy] Like a steam engine![B][C]")
	Append("Share;Lock:[E|AhHa] ...[P] Stu-pendous, Mei![P] A steam engine![P] That's what I am![B][C]")
	Append("Florentina:[E|Neutral] You're thinking you're in some way related to the engines in Jeffespeir?[B][C]")
	Append("Share;Lock:[E|AhHa] Precisely![P] After the clockmaker, Jeffespeir shall be next destination![B][C]")
	Append("Share;Lock:[E|Neutral] If nothing else, someone there may know how to repair me.[B][C]")
	Append("Mei:[E|Smirk] So, what do you use to power yourself?[B][C]")
	Append("Share;Lock:[E|Neutral] I simply boil some water and inhale the steam.[P] I'm getting very efficient at bringing it to a boil without losing any.[B][C]")
	Append("Share;Lock:[E|Neutral] I've developed a simple metal flask that captures the steam from a campfire.[P] I can last over a day before I start to get 'hungry'.[B][C]")
	Append("Mei:[E|Smirk] Nifty![B][C]")
	Append("Florentina:[E|Neutral] And there's never a shortage of firewood.[P] Guess that makes you low-maintenance.[B][C]")
	Append("Share;Lock:[E|Neutral] Somewhat, but I can't compete with an alraune that consumes light from the sun.[B][C]")
	Append("Share;Lock:[E|Thinking] The only problem is, I am not sure how I know this about myself.[P] I have no memories, but I still know my own requirements...[B][C]")
end
