-- |[ ====================================== Saint Fora's ====================================== ]|
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Sharelock")

--Variables.
local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
    
    --Normal Case:
    if(sMeiForm ~= "Gravemarker") then
        WD_SetProperty("Major Sequence Fast", true)
        Append("Share;Lock:[E|Neutral] Has your investigation resulted in anything, Mei?[B][C]")
        Append("Mei:[E|Neutral] Saint Fora's is crawling with statue monsterpeople.[P] Big wings.[B][C]")
        Append("Share;Lock:[E|Thinking] Statues.[P] Hm, that'd be the rumours of inorganic partirhumans.[B][C]")
        Append("Share;Lock:[E|Thinking] Rock is not metal with gears and steam, though.[P] Seems that was not relevant to me.[B][C]")
        Append("Share;Lock:[E|Neutral] No matter.[P] We can assume whoever's campsite it was merely joined those creatures.[P] That is the way of things, after all.[B][C]")
        Append("Mei:[E|Neutral] I guess so.[P] But, if we find anything about robots, we'll let you know![B][C]")
        Append("Share;Lock:[E|Neutral] Very good![P] I'll make sure the locals are warned of the dangers of the area, as well.[B][C]")
        Append("Florentina:[E|Neutral] Oh good, that'll make lots of new statue girls.[B][C]")
        Append("Share;Lock:[E|Neutral] Hm, the risk would draw adrenaline addicts and treasure hunters, yes.[P] But what can we do?[B][C]")
        Append("Florentina:[E|Happy] I'll get someone to seal the place up later.[P] Best to let it stay buried.[B][C]")
        Append("Mei:[E|Neutral] [P]*After*[P] we loot the place?[B][C]")
        Append("Florentina:[E|Neutral] Of course![P] That goes without saying![B][C]")
    
    --Mei is a Gravemarker:
    else
        WD_SetProperty("Major Sequence Fast", true)
        Append("Share;Lock:[E|Neutral] Has your investigation resulted in anything, Mei?[B][C]")
        Append("Mei:[E|Neutral] Isn't it obvious?[B][C]")
        Append("Share;Lock:[E|Thinking] Weren't you looking for a way home?[P] Wasn't that your reason for visiting St. Fora's in the first place?[B][C]")
        Append("Mei:[E|Neutral] We found statue monstergirls in the basement.[B][C]")
        Append("Share;Lock:[E|Thinking] Oh, yes?[P] But what else?[B][C]")
        Append("Mei:[E|Neutral] Um...[P] This solves the mystery of where the missing camper went?[B][C]")
        Append("Share;Lock:[E|AhHa] Brilliant![P] Much like you, the traveller was captured and transformed into a statue![B][C]")
        Append("Share;Lock:[E|AhHa] Excellent work, my friend![B][C]")
        Append("Share;Lock:[E|Neutral] Sadly, this means there is no connection between myself and these inorganic partirhumans.[P] Statues are not robots.[B][C]")
        Append("Mei:[E|Neutral] I suppose not.[P] If we bump into a robot, we'll let you know.[B][C]")
        Append("Share;Lock:[E|Neutral] Very good![P] My search continues![B][C]")
    
    end
end
