-- |[ ========================================= Clothes ======================================== ]|
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Sharelock")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	WD_SetProperty("Major Sequence Fast", true)
	Append("Mei:[E|Neutral] Did you find yourself in that mansion wearing that outfit?[B][C]")
	Append("Share;Lock:[E|Neutral] ...[P] Not the hat, but otherwise, yes.[B][C]")
	Append("Share;Lock:[E|Neutral] No, I don't know where I got it.[P] The hat, I got from a passing trader.[B][C]")
	Append("Mei:[E|Smirk] Any idea why you may have chosen to dress like that?[B][C]")
	Append("Share;Lock:[E|Neutral] If you look at my diary, this sort of clothing was the fashion in London.[P] In fact, I suspect I lost the sleeves on this vest in a fight.[B][C]")
	Append("Share;Lock:[E|Neutral] As for the hat, I don't know![P] I saw it, I loved it, I said it was nice, and the trader just gave it to me![B][C]")
	Append("Mei:[E|Smirk] Do you think if you can find a similar outfit, you might find a clue to your origins?[B][C]")
	Append("Share;Lock:[E|Neutral] I've asked around with the knitters here, but no luck.[B][C]")
	Append("Florentina:[E|Neutral] Doesn't look like anything I've seen.[B][C]")
	Append("Share;Lock:[E|Neutral] Unfortunately, I can't tell if it's a cold-weather outfit.[P] My systems perform better when well insulated, so I likely always dress heavy.[B][C]")
	Append("Share;Lock:[E|Neutral] If you see someone else in such garb, though, let me know.[P] I'd like to question them -[P] on how splendid their taste is![B][C]")
end
