-- |[ ========================================= Goodbye ======================================== ]|
--Script that fires when the player says goodbye to end the dialogue. Not all NPCs actually do anything here! This
-- script's existence is also optional.
WD_SetProperty("Major Sequence Fast", true)

--Standard.
Append("Share;Lock: Good luck on your quest, Mei![P] I'm sure you'll succeed!")
