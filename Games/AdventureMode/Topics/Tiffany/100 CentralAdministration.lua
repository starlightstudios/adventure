-- |[Bees]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Tiffany")

Append("55:[E|Neutral] In your day to day life, what sorts of contact have you had with Central Administration?[B][C]")
Append("Christine:[E|Neutral] Hmm...[P] Not much.[P] I periodically get mails from them giving me orders, that's about it.[B][C]")
Append("Christine:[E|Smirk] Hey, you don't think they don't exist, do you?[P] Like, they're a collective hallucination?[B][C]")
Append("55:[E|Neutral] They exist.[P] I am a former member of their ranks.[P] Plus, I have accessed their unprotected communications and some video logs.[B][C]")
Append("Christine:[E|Laugh] It's called a joke, 55.[B][C]")
Append("55:[E|Smirk] Very well.[P] Why did the Lord Golem cross the road?[B][C]")
Append("Christine:[E|Smirk] Uhh...[P] I don't know?[B][C]")
Append("55:[E|Smirk] To discuss a pending revolution with a maverick Command Unit.[B][C]")
Append("55:[E|Neutral] Your failure to laugh indicates that you are also not interested in comedy.[P] Shall we proceed?[B][C]")
Append("Christine:[E|Offended] (That, or the joke wasn't funny, 55...)[B][C]")
Append("Christine:[E|Neutral] Judging from the number of units Sophie and I have done repairs on, I'd guess that the administrative units compose less than one percent of the population of Regulus City.[P] They probably have their own repair facilities at that rate.[B][C]")
Append("55:[E|Neutral] Quite likely on both counts.[P] Because of the high number of Command Units in their facilities, they likely do maintenance work in special workshops.[B][C]")
Append("55:[E|Neutral] However, none of these workshops are on the official maps of Regulus City, nor are any conversion facilities or administrative centers.[B][C]")
Append("Christine:[E|Neutral] What about the Administration Building in Sector 7?[B][C]")
Append("55:[E|Neutral] A front, I believe.[P] Encrypted network traffic does route to that building, but only in a slightly higher density than others.[P] The building itself is not central administration.[B][C]")
Append("Christine:[E|Offended] Hmm, so we don't actually know where it is...[B][C]")
Append("55:[E|Neutral] Indeed.[P] Solving that mystery will be one of our preparatory objectives.[B][C]")
