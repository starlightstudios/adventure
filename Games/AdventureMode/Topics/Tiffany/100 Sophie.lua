-- |[Topic Script]|
--Activate sequence.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
local iSXMet55 = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXMet55", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Tiffany")

Append("55:[E|Neutral] How is your relationship with Sophie progressing?[B][C]")
Append("Christine:[E|Neutral] Progressing?[B][C]")
Append("55:[E|Neutral] The standard pattern involves a period of infatuation, establishment of norms, mutualization of living spaces, and eventually social integration.[B][C]")
Append("55:[E|Smug] It's very well documented.[P] Deviations are generally in the negative direction and result in the end of the relationship.[B][C]")
Append("Christine:[E|Smirk] You don't understand us at all, do you?[B][C]")
Append("55:[E|Smirk] If my assessment was incorrect, please demonstrate the flaws.[P] I cannot improve my model if I do not know how it is erroneous.[B][C]")
Append("Christine:[E|Neutral] ...[B][C]")
Append("Christine:[E|Neutral] It's not that you got anything wrong, it's just that -[P] you missed the magic of it.[B][C]")
Append("55:[E|Neutral] The electrical signals in the brains of a golem, while encoded at various harmonics and thus difficult to parse, are by no means magical.[B][C]")
Append("Christine:[E|Neutral] Not what I meant...[B][C]")
Append("55:[E|Smug] Then demonstrate the flaws of the model.[B][C]")

if(iSXMet55 == 0.0) then
    Append("Christine:[E|Neutral] I don't think I can.[B][C]")
    Append("Christine:[E|Smirk] You'd have to be in love to understand.[B][C]")
    Append("55:[E|Neutral] I am not.[B][C]")
    Append("Christine:[E|Neutral] Then why did you ask?[B][C]")
    Append("55:[E|Neutral] Your.[P] Your.[P] Emotional well being depends on the presence of Unit 499323.[P] I wish to keep you stable.[B][C]")
    Append("Christine:[E|Neutral] And that stutter there wasn't you trying to find an excuse to ask how I was doing?[B][C]")
    Append("55:[E|Offended] A data retrieval error.[P] They happen.[B][C]")
    Append("Christine:[E|Smirk] Suuuure.[B][C]")
else
    Append("Christine:[E|Neutral] I don't think I can.[B][C]")
    Append("Christine:[E|Smirk] You'd have to be in love to understand.[B][C]")
    Append("55:[E|Neutral] I am not.[B][C]")
    Append("Christine:[E|Neutral] And what about SX-399?[B][C]")
    Append("55:[E|Offended] What about her?[B][C]")
    Append("Christine:[E|Neutral] *PDU, begin monitoring 55's cognitive cycle activity.*[B][C]")
    Append("55:[E|Neutral] She is -[P] an interesting conversationalist.[B][C]")
    Append("Christine:[E|Neutral] What about her looks?[B][C]")
    Append("55:[E|Smirk] Red.[B][C]")
    Append("Christine:[E|Neutral] What sorts of jokes does she like to tell?[B][C]")
    Append("55:[E|Smirk] 45pct knock-knock jokes,[P] 25pct puns,[P] 15pct extended stories with punch-lines - [P][CLEAR]")
    Append("Christine:[E|Neutral] Uh huh.[P] Okay.[B][C]")
    Append("Christine:[E|PDU] PDU?[B][C]")
    Append("PDU:[E|Neutral] Unit 2855's cognitive cycles increased by 1300pct above normal operations when discussing SX-399.[P] The largest increase was during the categorization of her jokes.[B][C]")
    Append("55:[E|Offended] That -[P] that should be obvious.[P] Recalling, sorting, and storing memories requires a great deal of processor cycles![B][C]")
    Append("Christine:[E|Smirk] Thank you, PDU, that will be all.[B][C]")
end
