-- |[Bees]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Tiffany")

Append("55:[E|Neutral] I would like to talk about my sister, if that is all right with you.[B][C]")
Append("Christine:[E|Smirk] Oh, yes![P] Do you think - [P][CLEAR]")
Append("55:[E|Upset] You encountered her and mistook her for me, correct?[P] What level of similarity do we share?[B][C]")
Append("Christine:[E|Neutral] Your faces and mannerisms are identical, as are your voices.[P] Pitch-perfect.[B][C]")
Append("Christine:[E|Neutral] The way you walk, the way you take charge, yeah, you're probably twins.[B][C]")
Append("Christine:[E|Neutral] As to her personality...[B][C]")
Append("55:[E|Upset] That will be all.[B][C]")
Append("Christine:[E|Sad] Did I touch a nerve?[P] I'm sorry.[B][C]")
Append("55:[E|Neutral] What occurred between you two was not recorded, and therefore is absent from my records.[P] I was asking in order to fill the picture.[B][C]")
Append("55:[E|Neutral] I have intercepted recorded communications from Unit 2856 dating back several years, though I was not aware of our familiality.[B][C]")
Append("55:[E|Neutral] She is brusque, aggressive, rude, and commanding.[B][C]")
Append("Christine:[E|Neutral] So are you.[B][C]")
Append("55:[E|Down] ...[P] I do not like to think of myself that way...[B][C]")
Append("Christine:[E|Neutral] Well, everyone else does.[B][C]")
Append("55:[E|Down] ...[B][C]")
