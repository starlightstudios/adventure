-- |[Inhibitors]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Tiffany")

Append("55:[E|Neutral] I have a question concerning inhibitor chips.[B][C]")
Append("Christine:[E|Smirk] Oh, the XL series or the RB-100 series?[P] I can get you schematics if you like.[B][C]")
Append("Christine:[E|Smirk] We repair them all the time.[B][C]")
Append("55:[E|Neutral] I believe the XL series is the one used in drone units.[P] I have looked at the specifications but I am unsure of something.[B][C]")
Append("Christine:[E|Neutral] All right, hit me.[B][C]")
Append("55:[E|Neutral] Why would a unit want one voluntarily implanted?[B][C]")
Append("Christine:[E|Neutral] Vol -[P] I am not aware of that.[P] Drone Units don't get to say no, they are installed as part of the repurposing or conversion process.[B][C]")
Append("55:[E|Neutral] I came across some unsecured mail files two units were exchanging.[P] It made me curious precisely how the chips work.[B][C]")
Append("Christine:[E|Smirk] The RB-100 chips and the new generation of 120 chips are regulation lines on a processor that prevent electrical backflow.[P] They're used on a variety of appliances to regulate voltages.[B][C]")
Append("Christine:[E|Smirk] The XL series are instead a complicated thread-sniper.[P] That's what Sophie called them, apparently all the repair units say that.[P] They watch how long a thread has been operating and cut it.[B][C]")
Append("Christine:[E|Neutral] Drone Units come across as being stupid because they literally can't spend too long on something before the heuristic thread trips the chip's limiter.[P] They are unable to go past their gut reactions to something.[P] In fact, golems have them as well, but they are set to prevent feedback locks when defragmenting and are not active normally.[B][C]")
Append("55:[E|Neutral] And that is desireable?[B][C]")
Append("Christine:[E|Offended] No, and - [B][C]")
Append("Christine:[E|Laugh] Oh. My! Goodness! 55, were you reading two units sexting one another?[B][C]")
Append("55:[E|Neutral] 'Sexting', the act of sending sexually suggesting messages through text.[B][C]")
Append("55:[E|Upset] Unlikely. One of the units was expressing a desire to have an inhibitor chip and follow orders from the other unit, who reciprocated.[B][C]")
Append("Christine:[E|Laugh] That's called being a sub, 55![B][C]")
Append("55:[E|Upset] Explain yourself.[B][C]")
Append("Christine:[E|Blush] Let's see, well. Imagine there is someone you love so much, and trust so completely, that any order they give, you follow. You don't think about it.[B][C]")
Append("Christine:[E|Blush] Because no matter what, they love you and are protecting you. Thinking for you. So you don't need to think, or worry, or be afraid. You're completely safe.[B][C]")
Append("55:[E|Down] Something about that... Something about it upsets me.[B][C]")
Append("55:[E|Neutral] I thank you for adding the context but would like to end the conversation here.[B][C]")
Append("Christine:[E|Neutral] Sure thing, 55.[B][C]")