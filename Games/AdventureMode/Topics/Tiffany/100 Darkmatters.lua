-- |[Bees]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Tiffany")

Append("55:[E|Neutral] Have you reconsidered your stance?[P] I would like to examine your Darkmatter form.[B][C]")
Append("Christine:[E|Neutral] I thought you weren't interested in science and the like.[B][C]")
Append("55:[E|Neutral] I am not.[P] Darkmatters, however, are very resistant to damage and can phase through solid material if they so wish.[B][C]")
Append("55:[E|Smirk] These are notable advantages in combat.[P] We should harness them.[B][C]")
Append("Christine:[E|Neutral] Well, sorry, but you can't.[B][C]")
Append("Christine:[E|Neutral] Even I don't understand how I work.[P] It's like there is another form of matter, not the usual hadrons and leptons.[B][C]")
Append("Christine:[E|Neutral] I think the most accurate way to put it, is if we took space and folded it so much that it became something solid.[P] It's still a vacuum, but a hard vacuum.[B][C]")
Append("55:[E|Neutral] And intelligent.[B][C]")
Append("Christine:[E|Happy] Why, thank you for the compliment, 55![P] You're always a treat to talk to.[B][C]")
Append("55:[E|Neutral] That is not what I meant.[B][C]")
Append("Christine:[E|Smirk] Take the compliment and run, I always say![B][C]")
Append("55:[E|Upset] You have never said that before.[B][C]")
Append("Christine:[E|Neutral] (It's like talking to a wet towel...)[B][C]")

