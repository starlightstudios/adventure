-- |[Build Listing]|
--Builds a list of topics this NPC will respond to, and at what levels. Called at Adventure Mode startup.
local sNPCName = "Tiffany"
gsActiveNPCName = sNPCName

--Topic listing.
fnConstructTopicStd(true,  "RegulusCity", "Regulus City")
fnConstructTopicStd(true,  "Sophie")
fnConstructTopicStd(true,  "CauseOfScience", "Cause of Science")
fnConstructTopicStd(true,  "Revolution")
fnConstructTopicStd(false, "Faith")
fnConstructTopicStd(true,  "Golems")
fnConstructTopicStd(false, "Contradictions")
fnConstructTopicStd(false, "Inhibitors", "Inhibitor Chips")
fnConstructTopicStd(false, "LatexDrones", "Latex Drones")
fnConstructTopicStd(false, "SteamDroids", "Steam Droids")
fnConstructTopicStd(false, "Manufactory")
fnConstructTopicStd(false, "Mirabelle")
fnConstructTopicStd(false, "Electrosprites")
fnConstructTopicStd(true,  "CentralAdministration", "Central Administration")
fnConstructTopicStd(true,  "2855")
fnConstructTopicStd(false, "2856")
fnConstructTopicStd(false, "Vivify")
fnConstructTopicStd(false, "Darkmatters")
fnConstructTopicStd(false, "SerenityCrater", "Serenity Crater")
fnConstructTopicStd(false, "Doll")
