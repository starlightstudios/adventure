-- |[Bees]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Tiffany")

Append("55:[E|Neutral] What is ordinary life like in Regulus City?[B][C]")
Append("Christine:[E|Smirk] Are you asking me for some specific reason?[B][C]")
Append("55:[E|Neutral] I do not interact socially with its inhabitants.[B][C]")
Append("Christine:[E|Neutral] Oh, I see.[P] I suppose you don't.[B][C]")
Append("Christine:[E|Neutral] Well, I get out of my defragmentation pod, visit with anyone who's in my elevator, head in to the maintenance bay...[B][C]")
Append("55:[E|Neutral] Are the units there unnerved by your presence?[B][C]")
Append("Christine:[E|Smirk] They were at first, but I think I've grown on them.[P] Apparently I'm the only Lord Golem who smiles.[B][C]")
Append("Christine:[E|Smirk] Unit 445823 always waves at me as I pass the work terminals, and I like to visit with Unit 702399 when she's watching the Raijus.[B][C]")
Append("55:[E|Neutral] You apparently did not understand my query.[B][C]")
Append("55:[E|Neutral] Is the sentiment of the Slave Units at the breaking point?[P] How ready are they for a revolt?[B][C]")
Append("Christine:[E|Sad] N-[P]not ready at all![B][C]")
Append("Christine:[E|Sad] You don't understand.[P] Everyday life is something we tolerate.[P] We're very tough, we can endure.[B][C]")
Append("55:[E|Neutral] We.[B][C]")
Append("Christine:[E|Offended] Yes, we![B][C]")
Append("Christine:[E|Offended] It's the petty leadership, the uncaring administration, and the little cruelties it inflicts on a daily basis.[P] Removing someone's breaks, taking away their personal items, ordering extra pointless work...[B][C]")
Append("Christine:[E|Neutral] We put up with it because the threat of violence is big and scary.[P] We have something, but in war we might lose everything.[B][C]")
Append("55:[E|Upset] That is not encouraging.[B][C]")
Append("Christine:[E|Neutral] Unfortunately, we will have to thrust war upon them.[P] When it is apparent and presented, I know they'll make the right choice.[B][C]")
Append("Christine:[E|Neutral] The limits of a people's endurance will be the limits of their oppression.[P] We golems are a very durable lot.[P] You figure out the rest.[B][C]")
