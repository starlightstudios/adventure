-- |[Latex Drones]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
local iHasLatexForm = VM_GetVar("Root/Variables/Global/Christine/iHasLatexForm", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Tiffany")

--Has the form:
if(iHasLatexForm == 1.0) then
    Append("55:[E|Neutral] As a Drone Unit, what are your physical weaknesses?[B][C]")
    Append("Christine:[E|Neutral] Come again?[B][C]")
    Append("55:[E|Neutral] Drone Units are common in the security services, as they rarely ask difficult questions.[P] Optimizing our combat routines against them is a worthwhile endeavour.[B][C]")
    if(sChristineForm == "LatexDrone") then
        Append("Christine:[E|Neutral] Well, when you put it that way...[B][C]")
        Append("Christine:[E|Blush] There's a weird undertone in my base programming about being punctured.[P] The latex synth-skin is meant to remain intact at all times.[B][C]")
    else
        Append("Christine:[E|Blush] Going from memory -[P] There's a weird undertone in my base programming about being punctured.[P] The latex synth-skin is meant to remain intact at all times.[B][C]")
    end
    Append("Christine:[E|Blush] Violations of the chassis are to be repaired immediately, no exceptions.[B][C]")
    Append("Christine:[E|Blush] Good units are held tight, bound up, thick and firm, squeezing, good units...[B][C]")
    Append("Christine:[E|Blush] I'M A GOOD UNIT.[P] GOOD UNITS ARE HELD TIGHT, BOUND UP, THICK AND FIRM - [P][CLEAR]")
    Append("Christine:[E|Blush] W-[P]woah![P] Got a little carried away there![P] J-[P]just ignore what I said![B][C]")
    Append("55: Hm.[P] I am not privy to the conversion coding methods, they are not stored on the network.[P] Perhaps I could hack the conversion tube directly...[B][C]")
    Append("Christine:[E|Offended] Don't touch her![B][C]")
    Append("55: Her?[B][C]")
    Append("Christine:[E|Sad] I -[P] don't know where that came from, honestly.[B][C]")
    Append("Christine:[E|Sad] But please don't violate the conversion tube.[B][C]")
    Append("55:[E|Neutral] Are you saying this because of the latent programming?[B][C]")
    Append("Christine:[E|Offended] I'm saying it because you shouldn't hack the conversion tube, so don't.[B][C]")
    Append("Christine:[E|Offended] Move on.[B][C]")
    Append("55:[E|Neutral] Affirmative.[P] Changing topic.[B][C]")

--Does not have the form:
else
    Append("55:[E|Neutral] Christine, please provide your detailed assessment of the Drone Units we have encountered.[B][C]")
    Append("Christine:[E|Neutral] Why are you asking me?[B][C]")
    Append("55:[E|Neutral] I have noticed your ocular receivers following their movements very closely.[P] Particularly those of their legs.[B][C]")
    Append("Christine:[E|Smirk] I -[P] I like their shoes.[B][C]")
    Append("Christine:[E|Smirk] They look pretty and I wonder what they'd look like on me.[B][C]")
    Append("55:[E|Neutral] Their appearance should not be a consideration.[B][C]")
    Append("Christine:[E|Smirk] I like your boots, too, but they don't look like they'd fit me.[B][C]")
    Append("55:[E|Upset] Please do not make this a discussion of footwear.[B][C]")
    Append("Christine:[E|Smirk] Why not?[P] Are you saying you don't like shoes?[B][C]")
    Append("Christine:[E|Smirk] I like shoes![P] Okay, they're kind of bolted to your legs as a Lord Golem, but I'm sure we can replace them with a wrench or a tire-iron.[B][C]")
    Append("Christine:[E|Smirk] ... [P]or a welder...[B][C]")
    Append("55:[E|Angry] Please stop...[B][C]")
    Append("Christine:[E|Smirk] Do all the Command Units get boots like yours, or do you just get them from the same place?[P] Do you think it'd be suspicious if I requisitioned some for me?[B][C]")
    Append("55:[E|Offended] Christine, refocus your attentions on the present.[P] The Drone Units.[B][C]")
    Append("Christine:[E|Smirk] Yes?[P] What about them?[B][C]")
    Append("Christine:[E|Smirk] I like their masks, too.[P] I've had to work on them in the maintenance bay, you know.[P] That's not really a mask, their optical receptors are behind those lenses.[B][C]")
    Append("Christine:[E|Happy] But they look cute![P] I'm sure you could find some way to style them...[B][C]")
    Append("55:[E|Neutral] Unit 771852, recalibrate topics matrix and reinitialize conversation.[B][C]")
    Append("Christine:[E|Smirk] Okay, fine, we'll talk about something else.[B][C]")
end

