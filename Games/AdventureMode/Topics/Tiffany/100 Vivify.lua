-- |[Bees]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Tiffany")

Append("55:[E|Neutral] Unit 771852, provide a threat assessment of Project Vivify.[B][C]")
Append("Christine:[E|Neutral] Uh, massive?[B][C]")
Append("Christine:[E|Neutral] She can apparently melt through thick metal, butchers security units like hogs, and can climb slick surfaces at terrifying rates.[B][C]")
Append("Christine:[E|Neutral] And I think she can somehow influence the minds of those around her, though I can't say how.[B][C]")
Append("55:[E|Neutral] Though you have been influenced by her.[B][C]")
Append("Christine:[E|Neutral] Yes, but you weren't.[B][C]")
Append("55:[E|Neutral] Before you declare some sort of resistance or immunity, remember that her full attention was focused on you.[B][C]")
Append("55:[E|Neutral] Were you not present, I doubt I would have fared as well.[B][C]")
Append("Christine:[E|Neutral] So that's good.[P] Do you think I should act as a distraction?[B][C]")
Append("55:[E|Neutral] I'd say so, you seem quite suited for the role.[B][C]")
Append("Christine:[E|Neutral] Acid is a problem, but if I became a Drone Unit I'd probably be resistant to it.[P] You...[B][C]")
Append("55:[E|Neutral] I have protocols dedicated to removing contaminants.[P] So long as I focus on support fire, I think we can succeed, should we be forced to fight.[B][C]")
Append("Christine:[E|Neutral] But we shouldn't, unless we have to.[P] She -[P] is extremely dangerous...[B][C]")
Append("55:[E|Neutral] I agree, I am merely proposing solutions to our problems.[B][C]")
