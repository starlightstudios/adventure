-- |[Bees]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Tiffany")

Append("55:[E|Neutral] Tell me, Christine. Why are you a class traitor?[B][C]")
Append("Christine:[E|Neutral] That's the second time you've used that term.[B][C]")
Append("55:[E|Smirk] Hm, you are correct.[P] Perhaps I have an affinity for it in my harmonics.[B][C]")
Append("Christine:[E|Neutral] Maybe it's an official term among Command Units.[B][C]")
Append("55:[E|Neutral] That is quite possible.[P] I will review the communications I have intercepted between them.[B][C]")
Append("Christine:[E|Neutral] Considering your memory wipe, does that mean it's a basic part of your programming?[B][C]")
Append("55:[E|Neutral] Hmm...[P] Disturbing.[B][C]")
Append("55:[E|Neutral] Now I must pause to wonder if my programming is affecting my behavior in other, unidentified ways.[B][C]")
Append("Christine:[E|Smirk] Maybe that's why I'm a class traitor.[P] I wasn't programmed normally.[B][C]")
Append("Christine:[E|Smirk] Honestly, I don't think the conversion chamber was quite sure what to do with me.[P] It really made a mess of my memories.[B][C]")
Append("55:[E|Neutral] Elaborate.[B][C]")
Append("Christine:[E|Smirk] I remember going to school and graduating with an education degree and an English degree.[P] As a Lord Golem.[P] [E|Laugh]For a language that doesn't exist on Regulus.[B][C]")
Append("55:[E|Neutral] True, but that would not cause you to become a class traitor.[P] Such paradoxes are frequent in reprogramming cases.[P] Golems usually ignore them.[B][C]")
Append("Christine:[E|Smirk] ...[P] I guess that might mean it's just how I am.[P] Sooner or later I was going to betray the Lord Golems.[P] It's part of my personality.[P] Sophie seems to think so.[B][C]")
Append("Christine:[E|Smirk] I like to think that, no matter what happens, I'd stand up for what's right.[B][C]")
Append("55:[E|Down] That is...[P] discouraging...[B][C]")
Append("Christine:[E|Sad] How so?[B][C]")
Append("55:[E|Down] I wonder if how I am now is how I was before.[P] If so, and I am not like you, will I revert to that behavior eventually?[P] How much of my behavior is due to my cognition?[B][C]")
Append("Christine:[E|Sad] I don't know, that's a big question.[P][E|Smirk] But I do know you'll be fine. I have faith in you.[B][C]")
    
--Topics
WD_SetProperty("Unlock Topic", "Faith", 1)
