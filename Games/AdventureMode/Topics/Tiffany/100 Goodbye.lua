-- |[Goodbye]|
--Script that fires when the player says goodbye to end the dialogue. Not all NPCs actually do anything here! This
-- script's existence is also optional.
WD_SetProperty("Major Sequence Fast", true)

--Standard.
Append("55: Let us resume our mission, Unit 771852.")

--Restore the music.
if(glLevelMusic ~= "Telecomm") then
    AL_SetProperty("Music", glLevelMusic)
else
    AL_SetProperty("Music", "LAYER|Telecomm")
    AL_SetProperty("Mandated Music Intensity", 0.0)
end
