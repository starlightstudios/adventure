-- |[Bees]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Tiffany")

Append("55:[E|Neutral] What is it like being a golem?[B][C]")
Append("Christine:[E|Neutral] Hm?[B][C]")
Append("55:[E|Neutral] I am not one of your models.[P] I am familiar with the technical differences, but - [P][CLEAR]")
if(sChristineForm == "Golem") then
    Append("Christine:[E|Happy] It's the best thing![P] I love every second of it![B][C]")
else
    Append("Christine:[E|Happy] It's the best thing![P] I'll be changing back first chance I get![B][C]")
end
Append("55:[E|Neutral] Interesting.[B][C]")
Append("Christine:[E|Smirk] Doesn't everyone feel this way?[B][C]")
Append("55:[E|Neutral] More or less.[P] The testimonials of recently converted humans share your sentiment.[B][C]")
Append("Christine:[E|Smirk] I feel so much more aware, more energetic, stronger, smarter...[P] My legs don't hurt after walking or standing for a long time, I'm not as bothered by cold or heat...[P] and I'm...[P][E|Blush] a woman...[B][C]")
Append("55:[E|Neutral] Indeed.[B][C]")
Append("Christine:[E|Smirk] So why is it interesting if everyone feels this way?[B][C]")
Append("55:[E|Neutral] Because, in my analysis of the other partirhuman species, I see the sentiment largely echoed elsewhere.[B][C]")
Append("55:[E|Neutral] There are Alraunes and Raijus in the habitation domes.[P] Humans assigned to those forms report a euphoria after their transformation, similar to the one you and other Units report.[B][C]")
Append("55:[E|Neutral] It makes me think that, perhaps, there is something fundamental about the non-human nature of existence.[P] Are humans a fallen state, and non-humans an elevated one?[B][C]")
Append("Christine:[E|Neutral] ...[P] Don't talk like that.[B][C]")
Append("Christine:[E|Offended] They're different, but not inferior.[P] Don't talk like you know the correct way to live one's life.[B][C]")
Append("55:[E|Neutral] I was not advancing that proposition.[B][C]")
Append("Christine:[E|Offended] Don't.[B][C]")
Append("55:[E|Neutral] Very well.[P] The vehemence behind your position is noted.[B][C]")

--Topics
WD_SetProperty("Unlock Topic", "Contradictions", 1)
