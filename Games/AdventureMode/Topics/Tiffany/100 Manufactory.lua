-- |[Manufactory]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Tiffany")

--Variables.
local iManuMetKatarina        = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuMetKatarina", "N")
local iManuTold55Plan         = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuTold55Plan", "N")
local iManuReassigned         = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuReassigned", "N")
local iManufactoryDepopulated = VM_GetVar("Root/Variables/Chapter5/Scenes/iManufactoryDepopulated", "N")
local iManuFinale             = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuFinale", "N")

if(iManuMetKatarina == 0.0) then
    Append("55:[E|Neutral] Unit 771852, what are the odds of a tram car jumping a rail and crashing?[B][C]")
    Append("Christine:[E|Neutral] Is there a question directed to a repair unit, or a statistical thing?[B][C]")
    Append("55:[E|Neutral] I have not queried the databases yet, but the trams here are remarkably reliable.[P] I would like your assessment.[B][C]")
    Append("Christine:[E|Smirk] Regulus City's transit network is a net-work of art.[P] Excuse the pun.[B][C]")
    Append("55:[E|Neutral] I will not.[B][C]")
    Append("Christine:[E|Smirk] There's several levels of failsafes that prevent crashes, and the systems are analog-automated.[P] Even computer bugs won't cause issues.[B][C]")
    Append("Christine:[E|Smirk] We maintain rail parts all the time and have exactingly high standards for usage.[P] I'd say the odds of a failure are one in a billion.[B][C]")
    Append("55:[E|Neutral] I see.[P] Rail crashes were indeed rare in my research of the city's history.[B][C]")
    Append("Christine:[E|Neutral] But they do happen, 55.[P] There are thousands of trams running hundreds of times a day.[P] Those add up.[B][C]")
    Append("55:[E|Neutral] Correct.[P] I was merely asking if there was anything worthy of concern.[B][C]")

elseif(iManuMetKatarina == 1.0 and iManuFinale == 0.0) then
    Append("55:[E|Neutral] What do you think of Lord Unit 600484?[B][C]")
    Append("Christine:[E|Neutral] Why do you always go by unit designations?[P] Just call her Katarina.[B][C]")
    Append("55:[E|Neutral] Don't avoid the question.[B][C]")
    Append("Christine:[E|Neutral] I like her, she has a good head on her shoulders.[B][C]")
    Append("Christine:[E|Offended] I doubt she's anything different from a normal lord golem when it comes to abusing her staff, but that's like getting mad at water for being wet.[B][C]")
    Append("Christine:[E|Neutral] If given a chance, or a change of scenery, I'm sure she'd be a useful ally.[B][C]")
    Append("55:[E|Neutral] You do not suspect ulterior motives?[B][C]")
    Append("Christine:[E|Neutral] No.[P] I think she's telling the truth about being stonewalled.[B][C]")
    Append("55:[E|Neutral] And do you think we can trust her?[B][C]")
    Append("Christine:[E|Offended] 55, if you have something you want to ask me, you can just ask me.[P] You don't need to dance around it.[B][C]")
    Append("55:[E|Blush] Tch.[B][C]")
    Append("55:[E|Upset] Her skills are useful.[P] She is accomplished.[P] I think she might aid us, if we explained our situation.[B][C]")
    Append("55:[E|Upset] But if we miscalculate, and she is loyal to the administration...[B][C]")
    Append("Christine:[E|Neutral] She'd rat us out?[B][C]")
    Append("55:[E|Neutral] No.[P] I'd eliminate her first.[B][C]")
    Append("Christine:[E|Sad] Yeah...[P] I guess you would.[B][C]")
    Append("55:[E|Down] I have run over the evidence, and no clear choice comes forth.[P] I cannot make a decision.[B][C]")
    Append("Christine:[E|Smirk] You don't have to right now.[P] You can gather more evidence.[B][C]")
    Append("55:[E|Down] No.[P] That is not what I meant.[B][C]")
    Append("55:[E|Down] I wanted you to make a decision.[P] For me.[B][C]")
    Append("Christine:[E|Blush] Oh you did?[P] Well that's different.[B][C]")
    Append("55:[E|Blush] Do not take this as a compliment.[P] I am avoiding a hysteresis lock.[B][C]")
    Append("Christine:[E|Smirk] Oh, are you?[B][C]")
    Append("Christine:[E|Smirk] Or are you worried about the results of whatever decision you make, and hoping for someone else to take the blame if it backfires?[B][C]")
    Append("Christine:[E|Smirk] By routing the choice past me, you can use it to justify whatever comes.[P] You become blameless.[P] Even if only in your own mind.[B][C]")
    Append("55:[E|Upset] ...[B][C]")
    Append("55:[E|Upset] ...[B][C]")
    Append("55:[E|Down] ...[B][C]")
    Append("55:[E|Neutral] Christine, how do you know how a person works like this?[B][C]")
    Append("Christine:[E|Neutral] Machines, I read the manual.[P] People, I just read them.[B][C]")
    Append("55:[E|Neutral] So what is the overall decision?[B][C]")
    Append("Christine:[E|Neutral] The cost for a failed approach is higher than the gain for a successful one.[P] We wait and gather more information before asking her anything.[B][C]")
    Append("55:[E|Neutral] That is the decision I will make, then.[P] And if there are consequences, I will accept them.[B][C]")

else
    Append("55:[E|Neutral] Christine, a moment to review what happened in the Manufactory.[B][C]")
    Append("Christine:[E|Neutral] The part where we ran our leg motivators off?[B][C]")
    Append("55:[E|Smug] The part right after.[B][C]")
    Append("55:[E|Neutral] I received the killphrase anonymously over the network.[P] But the only units in the area were you, me, Katarina, and Chip.[B][C]")
    Append("55:[E|Neutral] You and I are accounted for.[P] Katarina clearly didn't have access, and I doubt a slave unit like Chip would have it.[B][C]")
    Append("55:[E|Neutral] The sector is isolated and it was a short-range radio transmission logging into a router in sector 99.[P] That router was destroyed in the explosion.[B][C]")
    Append("Christine:[E|Neutral] So what you're saying is, we have no way to find out who sent it.[P] They covered their tracks.[B][C]")
    Append("55:[E|Neutral] And they covered their tracks in a way that only someone with further knowledge of the project would have.[B][C]")
    Append("55:[E|Neutral] It is possible there is a list of staff who knew about the project somewhere, but it is not on the network.[P] Even in restricted areas.[B][C]")
    Append("Christine:[E|Neutral] It could be on a drive somewhere in administration headquarters, and just not on the network.[B][C]")
    Append("55:[E|Neutral] But who could have known we were there, we were in trouble, and also the needed killphrase?[P] It doesn't add up.[B][C]")
    Append("Christine:[E|Smirk] A guardian angel?[P] That's my best guess.[B][C]")
    Append("Christine:[E|Neutral] There's no way it was due to luck.[P] And hey, guardian angel, if you're listening?[P] Thanks.[B][C]")
    Append("55:[E|Neutral] Could you take this seriously?[P] Nobody is listening to us, least of all some fictional angel.[B][C]")
    Append("Christine:[E|Neutral] As soon as you find a physical person to thank, you tell me.[P] Until then, my theory is as good as yours.[B][C]")

end