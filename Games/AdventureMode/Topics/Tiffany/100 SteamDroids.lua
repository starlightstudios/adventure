-- |[Bees]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Tiffany")

Append("55:[E|Neutral] Please provide your technical assessment of the capabilities of Steam Droids.[B][C]")
Append("Christine:[E|Happy] I love their color choices.[P] You'd think the green and bronze wouldn't work, but they do![B][C]")
Append("55:[E|Upset] Technical.[P] Assessment.[B][C]")
Append("55:[E|Neutral] You are a repair unit, act like one.[B][C]")
Append("Christine:[E|Smirk] Oh, of course.[P] Silly me.[B][C]")
Append("Christine:[E|Neutral] Their bodies and equipment are in poor shape.[P] I estimate their performance rates are at thirty percent or lower than optimal, even in ideal conditions.[B][C]")
Append("Christine:[E|Neutral] Poor labourers, poor soldiers, and probably inadequate for research purposes unless one is researching patchwork solutions with non-standard materials.[B][C]")
Append("Christine:[E|Neutral] Technical advisory is a major increase in repair personnel and increased allocation to materials budgeting.[B][C]")
Append("Christine:[E|Neutral] But we both know that's not going to happen.[P] Why do you ask?[B][C]")
Append("55:[E|Smirk] You are effectively confirming my evaluation that the Steam Droids would serve as expendable shock troops.[P] Why did you resist it when I made it?[B][C]")
Append("Christine:[E|Neutral] My technical assessment is the assessment of the machine, not the person within it.[P] Technically I'd expect they'd have fallen apart years ago, but their indomitable spirit or a lot of creativity are keeping them running.[B][C]")
Append("Christine:[E|Smirk] So, 55, would you prefer to have a courageous and clever ally?[P] Or one whose equipment is up to spec but untested?[B][C]")
Append("55:[E|Smug] The latter, of course.[B][C]")
Append("Christine:[E|Neutral] ...[P] of course you would.[B][C]")
