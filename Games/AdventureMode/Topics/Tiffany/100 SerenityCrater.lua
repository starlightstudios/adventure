-- |[Bees]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Tiffany")

Append("Christine:[E|Neutral] I've been meaning to ask -[P] What do you think your motives were for helping the golems at Serenity Crater Observatory?[B][C]")
Append("55:[E|Neutral] As I believe has been made clear, my memories were wiped.[P] I cannot answer that question.[B][C]")
Append("Christine:[E|Smirk] And as I believe has been made clear, I was asking what you [P]*think*[P] your motives were.[B][C]")
Append("Christine:[E|Smirk] You know yourself.[P] We know roughly what the circumstances were.[P] Why would you help them?[B][C]")
Append("55:[E|Neutral] Barring some pathetic sentimental attachment, I can only assume that there was a practical cause for it.[B][C]")
Append("55:[E|Neutral] Unless it was, as 300910 suspects, a political reason, then there would be some reason related to security for me intervening.[B][C]")
Append("55:[E|Neutral] Abandoning the observatory, for example, would pose danger as the units hauled equipment over the surface.[P] Abandoning the equipment would likewise leave it for looters.[B][C]")
Append("Christine:[E|Neutral] Looters?[P] On Regulus?[B][C]")
Append("55:[E|Neutral] The Steam Droids, typically.[B][C]")
Append("55:[E|Neutral] Considering that these problems could be overcome with additional security details, I am forced to conclude the reason was political.[B][C]")
Append("Christine:[E|Smirk] Maybe you were doing it just to annoy another Command Unit.[B][C]")
Append("55:[E|Upset] 'Annoy' is a poor choice of words.[B][C]")
Append("55:[E|Neutral] Concluding a bargain or fulfilling a favour is more likely.[B][C]")
Append("Christine:[E|Neutral] Uh huh, favours.[P] You don't seem like the type to keep them.[B][C]")
Append("55:[E|Neutral] A favour is a transaction where the traded object is vague and maintains its properties into the indefinite future.[P] Obtaining favours can be a desirable end, so long as one uses them correctly. There is nothing illogical there.[B][C]")

