-- |[Bees]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Tiffany")

Append("55:[E|Neutral] Do you agree with the Cause of Science?[B][C]")
Append("Christine:[E|Smirk] Yes.[B][C]")
Append("55:[E|Neutral] Interesting.[B][C]")
Append("Christine:[E|Neutral] You don't?[B][C]")
Append("55:[E|Neutral] It does not concern me, as it is an abstract philosophy.[P] Machines do not need such mental crutches.[B][C]")
Append("Christine:[E|Neutral] Uh huh.[P] So what are you going to replace it with?[B][C]")
Append("55:[E|Neutral] As I said, that does not concern me.[B][C]")
Append("Christine:[E|Offended] So you've thought extensively about our coming revolution, but nothing about what comes after?[P] I have.[B][C]")
Append("Christine:[E|Smirk] I think a republic would be the best way to serve the Cause.[P] We ought to distribute the gains of science amongst all workers, regardless of social status.[B][C]")
Append("55:[E|Neutral] Why bother with such proselytization?[P] I have already committed to your purposes, you do not need to sell me on it.[B][C]")
Append("Christine:[E|Neutral] But do you agree?[P] What do you think?[B][C]")
Append("55:[E|Neutral] That this conversation is pointless.[P] Machines do not need such mental crutches.[B][C]")
Append("Christine:[E|Neutral] So if this is a mental crutch, then what does one with no such handicap suppose?[B][C]")
Append("55:[E|Smug] It does not matter.[B][C]")
Append("55:[E|Neutral] What happens will happen.[P] The forces we set in motion will be larger than us, and will shape the events.[P] We are merely parts of that larger force.[P] Machines.[B][C]")
Append("Christine:[E|Neutral] But you care when those events go against you.[B][C]")
Append("55:[E|Neutral] I am doing what a machine will do to preserve itself, if self-preservation is in that machine's programming.[P] Evidently, it is.[B][C]")
Append("Christine:[E|Neutral] Self-preservation is a philosophy.[P] A simple one, but it is.[B][C]")
Append("Christine:[E|Smirk] So, I think a republic of equals will do a fine job preserving you.[P] Don't you think?[B][C]")
Append("55:[E|Smirk] I have no need for such mental crutches.[B][C]")
Append("Christine:[E|Offended] Structuralism taken to its logical extreme...[B][C]")
    
