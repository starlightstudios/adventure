-- |[Bees]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Tiffany")

Append("55:[E|Neutral] Christine, what is faith?[P] Why do you have it in me?[B][C]")
Append("Christine:[E|Smirk] Uh, is your dictionary software malfunctioning?[B][C]")
Append("55:[E|Neutral] I know its definition, context, pronunciation, etymology.[P] But I do not know what it is.[B][C]")
Append("55:[E|Neutral] You remember being a human.[P] Humans have faith.[P] Don't they?[B][C]")
Append("Christine:[E|Neutral] I think so.[P] Some pretend like they don't, but they do.[B][C]")
Append("Christine:[E|Smirk] Faith is where you believe something even if it's not certain.[P] It's a part of ordinary life, we use it to get by.[B][C]")
Append("Christine:[E|Smirk] It's not just believing in some higher power, if you do.[P] Other people are very complex, and if you don't have faith in them, you can never understand or get along with them.[B][C]")
Append("Christine:[E|Smirk] Another word for faith might be trust.[B][C]")
Append("55:[E|Neutral] It is listed as synonym.[B][C]")
Append("Christine:[E|Smirk] Most uses of faith are in relation to something which does not reciprocate it.[P] A God is the one you might think of first, but a government, an organization, a leader.[B][C]")
Append("Christine:[E|Smirk] A leader does not need to know of each of her followers, but the followers still have faith in her.[P] Faith that she will do what they want her to.[B][C]")
Append("55:[E|Down] And if I betray that trust?[B][C]")
Append("Christine:[E|Smirk] Then I will still believe in you.[P] If you walk the wrong path, you are one step away from walking the right one.[B][C]")
Append("55:[E|Neutral] Hmm.[B][C]")
Append("Christine:[E|Offended] But that doesn't mean my faith will let you escape consequences.[P] If someone were to betray my trust, and I had to...[P] retire them...[B][C]")
Append("Christine:[E|Neutral] Then in their final moments in this world I would ask them to apologize.[P] It would still matter.[B][C]")
Append("55:[E|Neutral] And if they did not?[B][C]")
Append("Christine:[E|Neutral] Then...[P] my faith in them was misplaced.[B][C]")
Append("Christine:[E|Neutral] I made a mistake.[P] But there are many who are worthy of faith.[P] I'll give them a chance to prove they are worthy of it, if they need one.[B][C]")
Append("Christine:[E|Sad] If you don't have faith, then you're on a cold, lonely walk between these dark poles.[B][C]")
Append("55:[E|Down] ...[B][C]")
