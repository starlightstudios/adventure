-- |[Bees]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Tiffany")

Append("55:[E|Neutral] I noted a contradiction in your positions.[B][C]")
Append("Christine:[E|Neutral] Hm?[B][C]")
Append("55:[E|Smirk] I have little interest in your pedantic philosophy.[P] I am a logical machine.[B][C]")
Append("55:[E|Neutral] But I do have an interest in the contradiction.[B][C]")
Append("Christine:[E|Smirk] Oh, of course.[P] I believe you.[B][C]")
Append("55:[E|Neutral] Whether or not you believe me has no bearing.[P] It is true.[B][C]")
Append("55:[E|Neutral] Now, the problem is your insistence that I not speak of partirhumans as if this is a superior state to the human one.[B][C]")
Append("55:[E|Neutral] Yet I do recall you idolizing the transformation of humans into golems.[P] Robotizing the world.[B][C]")
Append("55:[E|Neutral] How can you desire this while insisting its implementation be suppressed?[B][C]")
Append("Christine:[E|Neutral] ...[B][C]")
Append("Christine:[E|Smirk] Yes, 55, I do imagine that.[P] It makes me feel good to imagine the disparate humans of the world becoming robots.[P] That excites me.[P] Maybe a little bit more than it ought to...[B][C]")
Append("Christine:[E|Blush] I wonder if Sophie thinks the same thing...[B][C]")
Append("55:[E|Neutral] Focus.[B][C]")
Append("Christine:[E|Smirk] Right, yes.[B][C]")
Append("Christine:[E|Neutral] But...[P] People don't understand what they are excluding themselves from.[P] They would prefer to remain ignorant in a simple world where everything makes sense than be brought into a world where they truly understand what is happening, but are powerless to stop it.[B][C]")
Append("Christine:[E|Neutral] That is naivete.[P] Innocence.[P] Part of growing up is losing that innocence.[P] I suppose that transformation into something better is a part of growing up.[B][C]")
Append("55:[E|Neutral] Interesting.[P] Your justification is not one I have seen elsewhere.[B][C]")
Append("Christine:[E|Sad] But it makes me feel ashamed to have thought of it...[B][C]")
Append("55:[E|Neutral] For reference, the administration's official justification is that the humans of Pandemonium are useless as they are, and can be made useful by conversion.[B][C]")
Append("55:[E|Neutral] Internally, of course, they merely seek to augment the labour force.[P] The justification is secondary to the true goal.[B][C]")
Append("55:[E|Neutral] I am, of course, not implying that your sexual fetishization is the true goal and the maturation of a species merely a justification.[B][C]")
Append("Christine:[E|Offended] ...[B][C]")
Append("55:[E|Smug] I merely wanted to know what you thought.[P] I am satisfied.[B][C]")















