-- |[Bees]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Tiffany")

Append("55:[E|Neutral] I was present for neither the discovery nor recruitment of these 'electrosprites', as you call them.[B][C]")
Append("55:[E|Neutral] Are you certain they can be trusted?[B][C]")
Append("Christine:[E|Smirk] Positive.[P] They share a mind, temporarily, with their conduit until they can take full form.[B][C]")
Append("Christine:[E|Smirk] They'll understand the positions of the golems they merge with, and I know they'll want to help them.[P] We're in the same boat, as it were.[B][C]")
Append("55:[E|Neutral] Hm.[B][C]")
Append("Christine:[E|Neutral] Let me guess, you don't trust them?[B][C]")
Append("55:[E|Smirk] You are naive.[P] These are an entirely alien, unknown life form.[P] We have no reason to believe they are on our side.[P] Anything and everything could be a misrepresentation, intentional or otherwise.[B][C]")
Append("Christine:[E|Neutral] Fine, then.[P] Sic them on the administration.[P] Like attack dogs.[B][C]")
Append("55:[E|Smirk] That I may well do, when the time comes.[B][C]")
Append("Christine:[E|Sad] *sigh*[B][C]")

