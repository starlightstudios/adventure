-- |[Bees]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Tiffany")

Append("55:[E|Neutral] Christine, do you remember the circumstances under which we met?[B][C]")
Append("Christine:[E|Smirk] Yes.[P] Happy memories...[B][C]")
Append("Christine:[E|Sad] S-[P]sort of.[P] I remember you whacking me and then waking up...[B][C]")
Append("Christine:[E|Blush] And then my conversion...[B][C]")
Append("55:[E|Neutral] I was referring to the time I spoke to you on the intercom.[P] I consider that to be our first meeting.[B][C]")
Append("Christine:[E|Sad] Oh yeah, almost died...[B][C]")
Append("Christine:[E|Smirk] You did save my life, didn't you?[P] You repressurized the airlock, and I would have died otherwise.[B][C]")
Append("55:[E|Smirk] I had no need for a corpse.[P] It was purely pragmatic.[P] Without your aid, I would have been unable to repair myself.[B][C]")
Append("55:[E|Neutral] I was instead referring to your jittery, uncertain manner.[P] The thermal scopes suggested excessively high stress levels.[B][C]")
Append("Christine:[E|Offended] Oh, yes, how unreasonable.[P] Nothing stressful at all about wandering around dark halls, surrounded by bodies, and berserk robots trying to kill you.[B][C]")
Append("Christine:[E|Offended] However could I feel such a way, with my frail little meat body?[B][C]")
Append("55:[E|Neutral] But you no longer express such fears.[B][C]")
Append("Christine:[E|Smirk] I got reprogrammed.[P] Obviously petty organic things like fear were overwritten, right?[B][C]")
Append("Christine:[E|Smirk] I retain my robotic memories and skills even when in organic form, though I can't access them as cleanly.[B][C]")
Append("55:[E|Neutral] Your hypothesis is unlikely.[B][C]")
Append("Christine:[E|Neutral] Well,[P] you're unflappable.[P] Maybe I'm learning from you?[B][C]")
Append("55:[E|Neutral] Also unlikely, but serviceable.[P] Very well.[B][C]")
