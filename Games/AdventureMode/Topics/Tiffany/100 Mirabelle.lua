-- |[Mirabelle]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Tiffany")

--Dialogue:
Append("Christine:[E|Neutral] So, after the mess with Project Mirabelle...[B][C]")
Append("55:[E|Neutral] Indeed.[P] Apart from your performance...[B][C]")
Append("Christine:[E|Offended] Oh, lay off, will you?[P] We came out on top, didn't we?[B][C]")
Append("55:[E|Smug] Chiding you is much easier if you anticipate the criticisms.[B][C]")
Append("55:[E|Neutral] However, there is are two unresolved matters that concern me.[B][C]")
Append("Christine:[E|Neutral] Who sent that killphrase code...[B][C]")
Append("55:[E|Neutral] Yes, that is one of them.[P] I received the phrase from someone who was nearby, despite the sector being isolated.[P] They knew about our activities.[B][C]")
Append("55:[E|Neutral] Yet despite rescuing us and saving the sector, indeed, possibly the city, they have made no attempt to contact us afterwards.[B][C]")
Append("Christine:[E|Neutral] Not only that, but how did they, whoever they are, know the killphrase?[B][C]")
Append("55:[E|Neutral] Indeed.[P] I have a theory.[B][C]")
Append("55:[E|Upset] However, it is an odd one and the evidence was destroyed.[P] I cannot confirm it.[B][C]")
Append("55:[E|Upset] I believe that Project Mirabelle assimilated its own inventor, and the personality of that inventor sent us the killphrase before being subsumed again.[B][C]")
Append("Christine:[E|Neutral] I think that's better than my idea.[P] I figured there must be an infiltrator already within the area, unknown to us.[B][C]")
Append("55:[E|Neutral] Unlikely, but not impossible.[P] If I had more time, I may have been able to spoof the uplink and allow us to blend in with them.[P] Force, however, was adequate.[B][C]")
Append("Christine:[E|Neutral] Yeah, that was my concern.[P] You said there were two unresolved matters.[B][C]")
Append("55:[E|Neutral] Yes.[P] Project Mirabelle was created by the administration of the city, likely as a research project.[P] Do not act surprised when I inform you that there is no record of Project Mirabelle, at all.[B][C]")
Append("Christine:[E|Scared] What!?[P] Impossible![B][C]")
Append("55:[E|Neutral] Christine, please.[B][C]")
Append("Christine:[E|Offended] Spoilsport.[B][C]")
Append("Christine:[E|Neutral] Yeah, it'd make sense that they scrubbed everything after it went rogue.[P] But that means they still have the basic schematics somewhere.[B][C]")
Append("55:[E|Neutral] Indeed.[P] There is nothing to prevent the administration from reviving the project and fixing its obvious loyalty issue.[P] If that happens, all attempts at revolution are moot.[B][C]")
Append("55:[E|Neutral] We must succeed in our mission before that happens.[B][C]")

