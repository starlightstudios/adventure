-- |[Trading Post]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Breanne")

--Standard.
Append("Mei: Do you go to the trading post often?[B][C]")
Append("Breanne: It's been a while, since they usually deliver to me and not t'other way 'round.[B][C]")
Append("Breanne: I do make visits sometimes.[P] It's pretty much the biggest settlement in Trannadar.[B][C]")
Append("Mei: Trannadar?[B][C]")
Append("Breanne: That's the province we're in, hun.[P] Quantir is northeast of here.[B][C]")
Append("Breanne: Province is a bit of a misnomer, though.[P] More like unadministered wild territory.[B][C]")
Append("Breanne: There's probably some noble someplace who claims title to Trannadar or somesuch, but that don't amount to a hill of beans out here.[B][C]")
