-- |[Breanne Sign]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Breanne")

--Standard.
Append("Mei: What exactly does taff mean?[B][C]")
Append("Breanne: You saw my sign, eh?[P] I wrote that when I was feelin' particularly off.[B][C]")
Append("Breanne: Taff is not a very nice word.[B][C]")
Append("Mei: I could guess from the context.[P] Do you have a problem with your parents?[B][C]")
Append("Breanne: I love 'em to bits, really.[P] They just need to stop intruding and let me live my life.[B][C]")

--Topic unlock.
WD_SetProperty("Unlock Topic", "Parents", 1)
