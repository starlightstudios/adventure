-- |[Bees]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sMeiForm        = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local iHasBeeForm     = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")
local iCanSpawnJoanie = VM_GetVar("Root/Variables/Chapter1/Breanne/iCanSpawnJoanie", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Breanne")

-- |[Special]|
--If Joanie has not been unlocked and Mei is a bee, special dialogue plays.
if(iCanSpawnJoanie == 0.0 and sMeiForm == "Bee") then
	Append("Mei:[E|Neutral] Do you have a history with the bees?[B][C]")
	Append("Breanne: Everyone in these parts does.[P] You're about as likely to get mauled by a bear as dragged off by a bee.[B][C]")
	Append("Mei:[E|Neutral] Hmmm?[P] Oh?[B][C]")
	Append("Mei:[E|Happy] Oh really!?[B][C]")
	Append("Breanne: That smile is kinda unnervin', Mei.[B][C]")
	Append("Mei:[E|Happy] You have a friend who's a bee, don't you?[B][C]")
	Append("Breanne: ...[B][C]")
	Append("Breanne: Had.[P] Past tense.[B][C]")
	Append("Mei:[E|Smirk] Well she's telling me she's your friend.[B][C]")
	Append("Breanne: J-[P]Joanie?[B][C]")
	Append("Mei:[E|Smirk] Oh, that was her name?[P] We generally don't remember our names, since we don't really need them.[B][C]")
	Append("Mei:[E|Smirk] ...[P] Yep![P] Okay.[P] I'll let her know.[B][C]")
	Append("Breanne: M-[P]Mei, I'm about to cry - [B][C]")
	Append("Mei:[E|Smirk] Whatever for?[P] I let her know you'd like her to visit.[B][C]")
	Append("Breanne: *sniff*[P] Really?[B][C]")
	Append("Mei:[E|Smirk] Of course.[P] She's come by a few times but we're very busy, as you might imagine.[P] I guess you must have missed her.[B][C]")
	Append("Breanne: Oh, Mei![P] I can't believe it![B][C]")
	Append("Breanne: Thank you so much![B][C]")
	Append("Mei:[E|Laugh] Don't mention it![B][C]")
	Append("Mei:[E|Smirk] But, if you don't want to become a bee yourself...[B][C]")
	Append("Breanne: Better stay near the Pit Stop...[B][C]")
	Append("Mei:[E|Smirk] The hive thinks you'd be a good drone.[P] I happen to agree.[B][C]")
	Append("Breanne: Thanks so much, Mei.[P] *sniff*[P] This means a lot...[B][C]")

	VM_SetVar("Root/Variables/Chapter1/Breanne/iCanSpawnJoanie", "N", 1.0)

-- |[Normal]|
else

	--Standard.
	Append("Mei:[E|Neutral] Do you have a history with the bees?[B][C]")
	Append("Breanne: Everyone in these parts does.[P] You're about as likely to get mauled by a bear as dragged off by a bee.[B][C]")
	Append("Breanne: Ain't sure which is worse.[B][C]")

	--If Mei is a bee:
	if(sMeiForm == "Bee") then
		Append("Mei:[E|Neutral] We're very sorry if we've caused you any trouble.[B][C]")
		Append("Breanne: Ya don't have to be apologizin'.[P] Bees gotta survive too.[P] You can't get mad at a beaver for building a dam.[B][C]")
		Append("Breanne: Nature's real cruel like that...[B][C]")

	--If Mei is not a bee, but does have the bee transformation:
	elseif(iHasBeeForm == 1.0) then
		Append("Mei:[E|Neutral] It's not so bad.[P] It's scary at first, but then...[B][C]")
		Append("Breanne: Please don't.[B][C]")
		Append("Breanne: I know you're trying to help, but there are some things that ought to be let lie.[B][C]")

	--Normal case:
	else
		Append("Mei:[E|Neutral] Neither sounds very appealing.[B][C]")
		Append("Breanne: These'r the risks we take living out in the sticks.[B][C]")
		Append("Breanne: I know it ain't the bees' fault, they're just doing what comes natural.[P] Gotta forgive and forget, else the pain will gnaw at you.[B][C]")
	end
end
