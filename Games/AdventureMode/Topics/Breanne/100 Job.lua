-- |[Job]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Breanne")

--Flags.
VM_SetVar("Root/Variables/Chapter1/Breanne/iTalkedJob", "N", 1.0)

--Standard.
Append("Mei: Can you tell me a bit more about your daily routine?[B][C]")
Append("Breanne: Aren't you just a sweetie?[B][C]")
Append("Mei: I am?[B][C]")
Append("Breanne: I get a lot of visitors, but they ain't asking me what I do.[P] You're either real nice or real inquisitive.[B][C]")
Append("Mei: Can't it be both?[B][C]")
Append("Breanne: Sure can![B][C]")
Append("Breanne: Welp, this here Pit Stop was made by these two hands.[P] I hired a couple labourers from the trading post a few years back, but I did all the planning and a lot of the work myself.[B][C]")
Append("Breanne: Chopped the trees, built the fences, that sort of thing.[B][C]")
Append("Mei: The shed looks newer.[B][C]")
Append("Breanne: I built that last winter.[P] Unfortunately it gets real dusty, but the extra storage space is nice.[B][C]")
Append("Breanne: I also do the cooking, sewing, cleaning...[P] If the lakeside is quiet, I go fishing sometimes too.[B][C]")
Append("Breanne: Oh, and I read.[P] Travellers leave books they're done with, so I've built up a bit of a library.[B][C]")
Append("Mei: But you're the only one here?[B][C]")
Append("Breanne: Nope![P] My friend Mei is here right now.[B][C]")
Append("Mei: Aww...[B][C]")
