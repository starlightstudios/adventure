-- |[Pandemonium]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Breanne")

--Standard.
Append("Mei: I'm from Earth.[P] This is Pandemonium?[B][C]")
Append("Breanne: Yep![P] Are you sure your word for Pandemonium isn't just Earth?[P] Languages are like that.[B][C]")
Append("Mei: How many planets are there in your solar system?[B][C]")
Append("Breanne: Planets?[P] Well, there's Regulus, which is the moon.[P] There's Cenatroid, Meracoid, Ossiline, and Tenebree.[B][C]")
Append("Breanne: Did I get them all?[P] I don't have a lot of books on astronomy.[B][C]")
Append("Mei: You see, on Earth we have seven other planets.[P] Eight if you count Pluto, but only people desperate to look smart do that.[B][C]")
Append("Breanne: Huh.[P] Can't argue with that.[P] I guess you really are far from home.[B][C]")
Append("Breanne: But, hey, home is where your heart is.[P] If you can't find a way back to Earth, you're welcome to stay here.[P] Could always use another set of hands.[B][C]")
Append("Mei: I'm not giving up just yet, but thank you for the offer.[B][C]")
