-- |[Florentina]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local bIsFlorentinaPresent = AL_GetProperty("Is Character Following", "Florentina")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Breanne")

--If Florentina is not around:
if(bIsFlorentinaPresent == false) then
	Append("Mei: Who's Florentina?[P] What's she like?[B][C]")
	Append("Breanne: She's an Alraune who runs the supply shop over at the tradin' post.[P] Brusque sort.[B][C]")
	Append("Breanne: Don't let it go foolin' you.[P] She's got her reasons for bein' the way she is, but nobody's cruel like that all the time.[B][C]")
	Append("Breanne: I think somethin' bad happened to her and she ain't lookin' for companionship.[P] All of this is rumours, mind.[B][C]")
	Append("Mei: Have you had a lot of business dealings with her?[B][C]")
	Append("Breanne: Nothing consistent.[P] If I get an excess of something, she can usually sell it off.[P] If she needs something...[P] disappeared...[B][C]")
	Append("Mei: You're not doing anything illegal are you?[B][C]")
	Append("Breanne: Law ain't the same here as it is down east, stranger![B][C]")
	Append("Mei: I'm not from down east, but I get what you mean.[B][C]")
	Append("Breanne: There's a real divide between what's moral and what's legal.[P] I try to stay on the side of the former, damn the latter.[B][C]")

--If Florentina is around:
else
	Append("Mei: So, Florentina, are you and Breanne friends?[B][C]")
	Append("Florentina: No.[B][C]")
	Append("Mei: I had a feeling you'd say that.[B][C]")
	Append("Breanne: Do you believe her?[B][C]")
	Append("Mei: Not really. People who don't like each other don't come up with clever insults for one another.[B][C]")
	Append("Florentina: Astute observation, you cheese-eyed gibberer.[B][C]")
	Append("Mei: Thank you, sock-sniffing maple-butt.[B][C]")
	Append("Breanne: Aww, you two get along so well.[P] I'm a little jealous.[B][C]")

end
