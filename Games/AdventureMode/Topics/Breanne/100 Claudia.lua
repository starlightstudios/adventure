-- |[Bees]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local bIsFlorentinaPresent = AL_GetProperty("Is Character Following", "Florentina")
local iSavedClaudia = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedClaudia", "N")
local iHasFoundOutlandAcolyte = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasFoundOutlandAcolyte", "N")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Breanne")

--Standard.
if(iSavedClaudia == 0.0) then
	Append("Mei:[E|Neutral] Could you tell me anything about Sister Claudia?[P] Anything that might help find her?[B][C]")
	Append("Breanne:[E|Neutral] Cultist, totally.[B][C]")
	Append("Mei:[E|Neutral] Come again?[B][C]")
	Append("Breanne:[E|Neutral] Well that wasn't very polite of me.[B][C]")
	Append("Mei:[E|Offended] I was abducted by cultists![P] Now I'm trying to track one down?[B][C]")
	Append("Breanne:[E|Neutral] Claudia?[P] No way![P] She wouldn't hurt a fly.[P] She's the purest, goodest person in the world.[B][C]")
	Append("Breanne:[E|Neutral] It's so nauseating![B][C]")

	--If Florentina is present, this aside happens.
	if(bIsFlorentinaPresent) then
		Append("Florentina:[E|Happy] Now you know how I feel all the time.[B][C]")
		Append("Breanne:[E|Neutral] Oh shush, you.[B][C]")
		Append("Breanne:[E|Neutral] As I was saying, Claudia is all peace and love, all the time.[B][C]")
	else
		Append("Breanne:[E|Neutral] Claudia is all peace and love, all the time.[B][C]")
	end

	--Resume.
	Append("Breanne:[E|Neutral] She said she was on a holy mission to study the monster girls in Trannadar.[P] If she's on a holy mission, you better believe she's going to complete it.[B][C]")
	Append("Mei:[E|Neutral] Is she a dangerous fanatic?[B][C]")
	Append("Breanne:[E|Neutral] If you find her, she'll shower you with pamphlets.[P] Saying no doesn't work.[B][C]")
	Append("Breanne:[E|Neutral] They make good kindling...[B][C]")
	Append("Mei:[E|Neutral] Hm.[P] Does her group have a name?[B][C]")
	Append("Breanne:[E|Neutral] Uh, crud.[P] I think it was...[P] Heavenly Doves?[P] Does that ring a bell?[B][C]")
    if(iHasFoundOutlandAcolyte == 1.0) then
        Append("Mei:[E|Smirk] Yes, that's what sister Karina said their name was.[B][C]")
        Append("Mei:[E|Neutral] Maybe I didn't get the whole 'fanatic' impression from her, but she seems really nice.[B][C]")
        Append("Breanne:[E|Neutral] Yeah, that's them.[P] Sorry, that's all I know.[B][C]")
    else
        Append("Mei:[E|Neutral] No, but I wasn't expecting it to.[P] I just wanted to know in case I find something she left behind.[P] Heavenly Doves, eh?[B][C]")
        Append("Breanne:[E|Neutral] Sorry I couldn't be more help.[B][C]")
    end

	--If Florentina is present, this aside happens.
	if(bIsFlorentinaPresent) then
		Append("Florentina:[E|Neutral] Her gear has pictures of birds on it.[P] That's a pretty big tell.[B][C]")
		Append("Florentina:[E|Neutral] If we find a journal with a dove on it, that's hers.[B][C]")
		Append("Breanne:[E|Neutral] Florentina, don't you dare rob that poor Claudia![B][C]")
		Append("Florentina:[E|Happy] I was just thinking that if she had lost it, we might recognize it.[P] But now you've got me thinking...[B][C]")
		Append("Breanne:[E|Neutral] Don't![B][C]")
	end

--Claudia is rescued.
else
	Append("Breanne:[E|Neutral] Oh, Mei![P] Claudia came by again![B][C]")
	Append("Breanne:[E|Neutral] She said she has you to thank for that![B][C]")
	Append("Mei:[E|Neutral] Well I just helped her out of a bind, is all.[B][C]")
	Append("Breanne:[E|Neutral] You're making the world a better place, hun![B][C]")
	Append("Breanne:[E|Neutral] She was on her way over to Outland if you want to catch up with her.[B][C]")

end
