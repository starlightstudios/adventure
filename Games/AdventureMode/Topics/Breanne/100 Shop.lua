-- |[Let's Go Shopping!]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Dialogue.
Append("Breanne: What can I get for ya?")
fnCutsceneBlocker()

--Setup.
local sShopPath = gsRoot .. "Maps/Evermoon/BreannesPitStop/Shop Setup.lua"
local sString = "AM_SetShopProperty(\"Show\", \"Breanne's Pit Stop\", \"" .. sShopPath .. "\", \"Null\")"

--Run the shop.
fnCutscene(sString)
fnCutsceneBlocker()
