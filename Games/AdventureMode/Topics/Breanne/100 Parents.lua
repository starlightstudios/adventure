-- |[Parents]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Breanne")

--Set this flag.
VM_SetVar("Root/Variables/Chapter1/Breanne/iMeiKnowsAboutSuitors", "N", 1.0)
VM_SetVar("Root/Variables/Chapter1/Breanne/iTalkedParents", "N", 1.0)

--Standard.
Append("Mei: Your parents are overbearing, huh?[B][C]")
Append("Breanne: I don't know if I should be talkin' about that...[B][C]")
Append("Mei: I can relate.[P] My parents are always breathing down my neck.[P] Or they were, anyway.[B][C]")
Append("Breanne: Yeah![P] I'm a grown woman, I can make my own decisions![B][C]")
Append("Mei: And I don't need someone harping at me to get a promotion.[P] I wait tables for a living, I don't want to do that the rest of my life![B][C]")
Append("Breanne: Exactly![B][C]")
Append("Mei: I guess you don't live in the same town as yours.[P] Do they send you letters?[B][C]")
Append("Breanne: They send me suitors.[B][C]")
Append("Mei: Suitors? I guess that explains the sign.[P] They want you to get married?[B][C]")
Append("Breanne: Mom and Dad want me to be happy, really.[P] That also means they want their little girl to come home and have grandkids.[B][C]")
Append("Breanne: So, they send up anyone they think I would like.[P] Their ideas of what I like are way off.[B][C]")
Append("Mei: Heheh. Mine sent me job openings clipped from a newspaper.[B][C]")
Append("Breanne: It's about what they want, but they always cloaked in the language of what they want for you.[B][C]")
Append("Breanne: Can't they just leave us alone?[B][C]")
Append("Mei: It's love.[P] I don't think they're selfish, maybe they can't separate what they want for us from what they want for themselves.[B][C]")
Append("Mei: When you raise someone, you think of yourselves as one person.[P] They just can't let go.[B][C]")
Append("Breanne: ...[B][C]")
Append("Breanne: That's proper smart, Mei.[B][C]")
Append("Breanne: Doesn't change that I don't want people showing up here asking for my hand.[B][C]")
Append("Mei: But you'll put up with it, right?[B][C]")
Append("Breanne: Don't got the option.[P] At least it makes more sense now.[B][C]")
