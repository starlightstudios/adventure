-- |[Build Listing]|
--Builds a list of topics this NPC will respond to, and at what levels. Called at Adventure Mode startup.
local sNPCName = "Breanne"

--Topic listing.
fnConstructTopic("Shop",              "Shop",          1, sNPCName, 0)
fnConstructTopic("Bees",              "Bees",         -1, sNPCName, 0)
fnConstructTopic("BreanneSign",       "The Sign",     -1, sNPCName, 0)
fnConstructTopic("Claudia",           "Claudia",      -1, sNPCName, 0)
fnConstructTopic("Florentina",        "Florentina",   -1, sNPCName, 0)
fnConstructTopic("Job",               "Job",          -1, sNPCName, 0)
fnConstructTopic("Nadia",             "Nadia",        -1, sNPCName, 0)
fnConstructTopic("OutlandFarm",       "Outland Farm", -1, sNPCName, 0)
fnConstructTopic("Pandemonium",       "Pandemonium",  -1, sNPCName, 0)
fnConstructTopic("Parents",           "Parents",      -1, sNPCName, 0)
fnConstructTopic("PitStopMagicField", "Magic Field",  -1, sNPCName, 0)
fnConstructTopic("Trading Post",      "Trading Post", -1, sNPCName, 0)
