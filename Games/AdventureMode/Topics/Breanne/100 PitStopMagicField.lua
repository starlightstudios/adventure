-- |[Pit Stop Magic Field]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Breanne")

--Standard.
Append("Mei: How does the magic field here work?[B][C]")
Append("Breanne: If I knew, I'd be the greatest mage in Pandemonium.[P] I haven't the foggiest.[B][C]")
Append("Breanne: It's been here for a very, very long time.[P] Possibly as long as humans have existed, maybe longer.[B][C]")
Append("Breanne: Had a lady come through, said she was a mage.[P] She thought it was fey magic.[B][C]")
Append("Mei: Is it natural?[P] Did someone set it up?[P] I'm afraid I don't know how magic works, we don't have it on Earth.[B][C]")
Append("Breanne: Huh, you know, I never thought it might have been deliberate.[P] I always figured it was just the way it was.[B][C]")
Append("Breanne: Why would someone go to the lengths of making a magic field that scares off people who want to fight?[B][C]")
Append("Mei: Why not?[B][C]")
Append("Breanne: If you can make a magic field like that, you can probably blow up a building with a thought.[B][C]")
Append("Mei: Maybe they wanted to make a safe place to rest?[B][C]")
Append("Breanne: Hey![P] This place has been a Pit Stop for thousands of years![B][C]")
Append("Breanne: I'm continuing a grand tradition![B][C]")
Append("Mei: You don't have to go that far.[B][C]")
Append("Breanne: Shh, it's fun.[P] Let me have my fun.[B][C]")

--Topic unlock.
WD_SetProperty("Unlock Topic", "Pandemonium", 1)
WD_SetProperty("Unlock Topic", "Polaris_Magic Field", 1)
