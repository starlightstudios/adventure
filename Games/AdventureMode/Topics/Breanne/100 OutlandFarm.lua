-- |[Outland Farm]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Breanne")

--Standard.
Append("Mei: Outland Farm?[P] A farm in a place swarming with monsters?[B][C]")
Append("Breanne: You've come at a time when monster activity is a lot higher than usual.[P] This is breeding season.[B][C]")
Append("Breanne: Outland just hires a few more guards for a few months.[P] Things calm down a lot once the rains set in.[B][C]")
Append("Mei: So it's not always like this?[B][C]")
Append("Breanne: Not always, no.[P] If you *really* want to find a wild Alraune, you can.[P] Right now, they're awful agitated.[B][C]")
Append("Breanne: Dunno why, and not concerned, neither.[B][C]")
Append("Breanne: If you're looking for some spare platina, I bet Outland is hiring.[P] You and that sword look decent in a scrap.[B][C]")

--Mei is using the Rusty Katana.
if(AdInv_GetProperty("Is Item Equipped", "Mei", "Rusty Katana") == true or AdInv_GetProperty("Is Item Equipped", "Mei", "Rusty Katana (+1)") or AdInv_GetProperty("Is Item Equipped", "Mei", "Rusty Katana (+2)")) then
	Append("Mei: This rusty thing?[B][C]")
	Append("Breanne: It's the user, not the weapon. But, you should consider getting a better one.[P] I think the equipment shop at the trading post has swords for sale.[B][C]")

--Steel Katana.
else
	Append("Mei: This thing?[P] I guess it looks pretty nifty.[B][C]")
end

--Resume.
Append("Breanne: You hold it naturally.[P] Most folks need to be taught to wield one properly, but you seem like you were born with it.[B][C]")
Append("Mei: I watch some anime, I guess.[B][C]")
Append("Breanne: Ani-what?[B][C]")
Append("Mei: Uh, it's like moving pictures.[P] We have them on Earth.[B][C]")
Append("Breanne: It ain't something you learn by watching.[B][C]")
Append("Breanne: You hold yourself like a trained warrior.[P] Trust me, I know a lot of them.[P] You've got all the makings.[B][C]")
Append("Mei: Well, thanks for that.[P] I'm not looking for a job, but who knows?[B][C]")
