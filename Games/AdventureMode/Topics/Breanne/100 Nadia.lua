-- |[Nadia]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Breanne")

--Standard.
Append("Mei: So, Nadia.[B][C]")
Append("Breanne: She's an Alraune from the trading post.[P] She tells jokes.[B][C]")
Append("Breanne: Lots and lots of jokes.[P] Bad jokes.[B][C]")
Append("Mei: You don't like them?[B][C]")
Append("Breanne: Of course I like them![P] Puns are better the worse they are.[B][C]")
Append("Breanne: It's just like snoo.[B][C]")
Append("Mei: ...[B][C]")
Append("Mei: Snoo?[P] What's snoo?[B][C]")
Append("Breanne: Not much.[P] What's snoo with you?[B][C]")
Append("Mei: ...[P] What does an aneurysm feel like...[B][C]")
Append("Breanne: Haha![P] Nadia told me that one![P] It's *awful* isn't it?[B][C]")
