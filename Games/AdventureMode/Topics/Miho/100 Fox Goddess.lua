-- |[ ====================================== Fox Goddess ======================================= ]|
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Miho")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	WD_SetProperty("Major Sequence Fast", true)
	Append("Mei:[E|Neutral] Who is the Fox Goddess?[B][C]")
	Append("Miho:[E|Neutral] She is the keeper of all wisdom, the guardian of wit and cleverness.[B][C]")
	Append("Miho:[E|Neutral] Her true name is unknown to all mortal kind, though they say that when a kitsune finally claims her ninth tail, the Fox Goddess answers any one question she asks.[B][C]")
	Append("Mei:[E|Smirk] Has anyone asked her for her name?[B][C]")
	Append("Miho:[E|Neutral] No, because no kitsune has ever had a ninth tail.[B][C]")
	Append("Miho:[E|Neutral] Elder Yukina has eight.[P] She resides at the temple in Trafal where I found my way.[B][C]")
	Append("Mei:[E|Smirk] What does the Fox Goddess teach?[B][C]")
	Append("Miho:[E|Neutral] That we mortals are given our cleverness to do good by one another, and make our lives easier.[P] Be it through clever technologies or outwitting the forces of evil, wit is the basis of all that is right.[B][C]")
	Append("Miho:[E|Neutral] Are you interested in joining?[P] There's many teachings I could direct you to.[B][C]")
	Append("Mei:[E|Smirk] Maybe later, we're searching for a way home.[B][C]")
	Append("Miho:[E|Neutral] Elder Yukina has been around for a long time and knows a great many things.[P] If you cannot think of anywhere else to go, go to her.[B][C]")
	Append("Miho:[E|Neutral] And before you get any wild ideas, it's a lot of studying and self-reflection.[P] A lot of people want to join, but simply lack the patience.[B][C]")
	Append("Miho:[E|Neutral] Learning your own shortcomings is how most kitsunes get their first tail, you see.[B][C]")
	Append("Mei:[E|Happy] Thanks, Miho![B][C]")
end

--Topics.
WD_SetProperty("Unlock Topic", "Tails", 1)
