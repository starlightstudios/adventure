-- |[ ======================================= Cell Phone ======================================= ]|
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Miho")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iMihoKnowsCellPhone", "N", 1.0)
    
	WD_SetProperty("Major Sequence Fast", true)
	Append("Mei:[E|Neutral] Can you take a look at something for me?[P] A werecat said she found it near the mountains.[B][C]")
	Append("Miho:[E|Neutral] Sure.[P] What is it?[B][C]")
	Append("Mei:[E|Neutral] Does this look familiar?[P] We call them 'Cell Phones' where I'm from.[B][C]")
	Append("Miho:[E|Neutral] No, never seen anything like it.[P] You said it's from Trafal?[B][C]")
	Append("Mei:[E|Neutral] The werecat said she found it in a rockslide.[B][C]")
	Append("Miho:[E|Neutral] Might have washed down in a river.[P] She was probably panning for gold.[B][C]")
	Append("Miho:[E|Neutral] The rivers run from the mountains and carry things down here, to Trannadar.[P] Sometimes they cut through an embankment and trigger a rockslide.[B][C]")
	Append("Mei:[E|Smirk] So if we followed the rivers...?[B][C]")
	Append("Miho:[E|Neutral] You'd have to search the entire glacier.[P] It could be from a hundred places.[B][C]")
	Append("Mei:[E|Sad] Oh.[B][C]")
	Append("Miho:[E|Neutral] Why's it so important, anyway?[B][C]")
	Append("Mei:[E|Smirk] It belongs to someone called 'Sanya Pavletic'.[P] I think she might be from Earth, where I'm from![B][C]")
	Append("Miho:[E|Neutral] ...[B][C]")
	Append("Miho:[E|Neutral] What was that name again?[B][C]")
	Append("Mei:[E|Smirk] Sanya Pavletic.[P] Do you recognize it?[B][C]")
	Append("Miho:[E|Neutral] No, just.[P] No.[P] Never heard it before.[B][C]")
	Append("Miho:[E|Neutral] But, I'll be heading back to Trafal pretty soon.[P] I'll ask around, she might want it back.[B][C]")
	Append("Mei:[E|Happy] Thanks, Miho![B][C]")
end
