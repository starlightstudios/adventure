-- |[ ========================================= Goodbye ======================================== ]|
--Script that fires when the player says goodbye to end the dialogue. Not all NPCs actually do anything here! This
-- script's existence is also optional.
WD_SetProperty("Major Sequence Fast", true)

--Standard.
Append("Miho: Until we meet again.")
WD_SetProperty("Unlock Topic", "About You", 1)
WD_SetProperty("Unlock Topic", "Smoking", 1)
