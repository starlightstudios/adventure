-- |[ ===================================== Build Listing ====================================== ]|
--Builds a list of topics this NPC will respond to, and at what levels. Called at Adventure Mode startup.
local sNPCName = "Miho"

--Topic listing.
fnConstructTopic("About You",   "About You",    1, sNPCName, 0)
fnConstructTopic("Smoking",     "Smoking",      1, sNPCName, 0)
fnConstructTopic("Fox Goddess", "Fox Goddess", -1, sNPCName, 0)
fnConstructTopic("Cell Phone",  "Cell Phone",  -1, sNPCName, 0)
fnConstructTopic("Tails",       "Tails",       -1, sNPCName, 0)
