-- |[ ========================================== Tails ========================================= ]|
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Miho")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	WD_SetProperty("Major Sequence Fast", true)
	Append("Mei:[E|Neutral] Where I'm from, kitsunes are supposed to have nine tails.[B][C]")
	Append("Miho:[E|Neutral] Have you ever actually met one?[B][C]")
	Append("Mei:[E|Happy] Well no, because they were fictional demons![B][C]")
	Append("Miho:[E|Neutral] You're meeting one now, so at least some of what you know is clearly wrong.[B][C]")
	Append("Miho:[E|Neutral] Yes, I have three tails.[P] A fact I am quite proud of, mind you, though a kitsune must be humble in all things.[B][C]")
	Append("Mei:[E|Smirk] Is having three tails a big deal?[P] Do you all have three tails?[B][C]")
	Append("Miho:[E|Neutral] No.[P] A tail is bestowed upon a kitsune when they learn a great truth.[P] It is said the Fox Goddess herself judges us.[B][C]")
	Append("Miho:[E|Neutral] When a kitsune has many tails, they are very wise.[P] Elder Yukina has eight, and she knows everything.[B][C]")
	Append("Mei:[E|Smirk] How did you get your tails?[B][C]")
	Append("Miho:[E|Neutral] The first one is pretty obvious.[P] I became a kitsune.[B][C]")
	Append("Miho:[E|Neutral] We don't transform people like so many other monstergirls do, you have to do it yourself.[P] Everyone who is a kitsune, chose to be one.[P] That is the first task.[B][C]")
	Append("Mei:[E|Blush] Can you transform me, or teach me how?[B][C]")
	Append("Miho:[E|Neutral] You'd have to ask the elder, not me.[B][C]")
	Append("Mei:[E|Blush] Just asking...[B][C]")
	Append("Mei:[E|Smirk] How'd you get your second tail?[B][C]")
	Append("Miho:[E|Neutral] I'm a bounty hunter.[P] I hunt the cleverest, most dangerous, most villainous bounties.[B][C]")
	Append("Miho:[E|Neutral] A huge monster, made of pure steel and standing two stories tall, dressed in skins ripped from lions and rhinos, used to roam the savannah in Yeto.[B][C]")
	Append("Miho:[E|Neutral] It had one eye and they said its stare would paralyze you until it crushed you beneath its metal feet.[B][C]")
	Append("Mei:[E|Surprise] Oh dear![B][C]")
	Append("Miho:[E|Neutral] The creature would harass villages along the savannah.[P] It demanded every drop of alcohol from the people there.[P] And since the water isn't safe to drink, the creature caused many to die of thirst.[B][C]")
	Append("Florentina:[E|Neutral] Did you kick its ass?[B][C]")
	Append("Miho:[E|Neutral] You're darn tootin' I did![P] But that's not the whole story.[B][C]")
	Append("Miho:[E|Neutral] You see, I commissioned a supply of the most potent booze I could find, and set a trade caravan along a path.[B][C]")
	Append("Miho:[E|Neutral] I staffed it with all the angry villagers I could, and we waited for the beast to find us.[P] We gave it all the booze...[B][C]")
	Append("Miho:[E|Neutral] And followed it back to its lair.[P] We waited for it to drink...[B][C]")
	Append("Miho:[E|Angry] And set upon it as it slept in a drunken stupor![B][C]")
	Append("Miho:[E|Angry] We slew the beast by stabbing out its eye![P] It troubled the people of the savannah no more![B][C]")
	Append("Mei:[E|Happy] And you gained a tail for that?[B][C]")
	Append("Miho:[E|Neutral] No.[B][C]")
	Append("Mei:[E|Sad] No.[B][C]")
	Append("Miho:[E|Neutral] No.[P] For, you see, it was not my idea to get it drunk.[P] It was Sayam Jorgar's, one of the men I met.[B][C]")
	Append("Miho:[E|Neutral] I did not gain a tail for slaying the beast, I gained it for realizing that I could never have slain it on my own.[B][C]")
	Append("Miho:[E|Neutral] I needed to do research, listen to the people, and use my cleverness.[P] When I realized that, days later, I found my second tail had grown in when I awoke.[B][C]")
	Append("Miho:[E|Neutral] ...[P] It is a lesson Florentina has reminded me of.[B][C]")
	Append("Florentina:[E|Neutral] You're welcome.[B][C]")
	Append("Miho:[E|Neutral] I am no longer hunting your bounty, Florentina.[P] If I had done my research like the Fox Goddess taught me, I would have seen you as no villain.[B][C]")
	Append("Mei:[E|Offended] Oh, she's a villain all right.[P] Just not the booze stealing giant metal kind.[B][C]")
	Append("Florentina:[E|Blush] Oh stop, you're flattering me![B][C]")
	Append("Mei:[E|Neutral] What about the third tail?[B][C]")
	Append("Miho:[E|Neutral] Heh.[P] Maybe some other time, Mei.[P] Besides, I've got a lot of stories that don't end in a new tail.[B][C]")
	Append("Miho:[E|Neutral] Failures are an important part of learning.[P] Piece of advice -[P] learn to survive your failures, and you could be an eight-tailed kitsune someday.[B][C]")
	Append("Miho:[E|Neutral] That's what the elder told me.[P] Best advice I ever got.[B][C]")
end
