-- |[ ========================================= Smoking ======================================== ]|
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Miho")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	WD_SetProperty("Major Sequence Fast", true)
	Append("Mei:[E|Neutral] What are you smoking, anyway?[B][C]")
	Append("Florentina:[E|Neutral] Smells like wildthrush leaf.[B][C]")
	Append("Miho:[E|Neutral] It is a mixture of incenses.[P] Wildthrush is in there, yes.[P] You have a keen nose.[B][C]")
	Append("Miho:[E|Neutral] This isn't smoking tobacco, I use it to ward off insects when I am hunting a bounty.[B][C]")
	Append("Miho:[E|Neutral] Starfield Swamp isn't as bad as the jungles further to the north, but I keep a few incense sticks on me nonetheless.[B][C]")
	Append("Florentina:[E|Neutral] You wouldn't happen to know the recipe offhand would you?[B][C]")
	Append("Miho:[E|Neutral] A moment ago I was trying to capture you, and now you're making business offers?[B][C]")
	Append("Florentina:[E|Neutral] Forgive and forget, right?[B][C]")
	Append("Miho:[E|Neutral] I'll not part with the recipe quite yet, but I'll consider it.[B][C]")
end
