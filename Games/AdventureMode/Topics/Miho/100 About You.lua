-- |[ ======================================== About You ======================================= ]|
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Miho")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	WD_SetProperty("Major Sequence Fast", true)
	Append("Mei:[E|Neutral] So, Miho, where are you from?[B][C]")
	Append("Miho:[E|Neutral] While I am from Soswich, spiritually, I am from Trafal.[B][C]")
	Append("Miho:[E|Neutral] Do you know much about the Fox Goddess, Mei?[B][C]")
	Append("Mei:[E|Happy] Not a thing![B][C]")
	Append("Miho:[E|Neutral] She is the keeper of all wisdom, and her followers are we, the kitsunes.[B][C]")
	Append("Miho:[E|Neutral] So while I was born in Soswich, I did not live until I became her follower.[B][C]")
	Append("Miho:[E|Neutral] Then again, she was always there keeping watch on mortalkind, so perhaps I was always her follower.[B][C]")
	Append("Florentina:[E|Happy] Nerd.[P] You sound like Rochea.[B][C]")
	Append("Miho:[E|Angry] What do you believe in besides coin, Florentina?[B][C]")
	Append("Florentina:[E|Neutral] Woah woah woah, I believe in lots of things.[B][C]")
	Append("Florentina:[E|Neutral] Helping the poor in exchange for money, healing the sick in exchange for money, ending war in exchange for money.[B][C]")
	Append("Miho:[E|Neutral] Hmm.[P] Interesting.[B][C]")
	Append("Mei:[E|Smirk] I think more people see through Florentina than she'd admit.[B][C]")
	Append("Miho:[E|Neutral] Indeed.[P] Yet again I underestimate her.[B][C]")
	Append("Mei:[E|Smirk] Can you tell me a bit more about the Fox Goddess?[B][C]")
	Append("Miho:[E|Neutral] Well...[B][C]")
end

--Topic unlock.
WD_SetProperty("Unlock Topic", "Fox Goddess", 1)
