-- |[ ===================================== Build Listing ====================================== ]|
--Builds a list of topics this NPC will respond to, and at what levels.
local sNPCName = "Mia"
gsActiveNPCName = "Mia"

--              ||||| Unlocked At Start |||  Filename          |||  Display Name (Optional)
fnConstructTopicStd(              true, "Best Story",        "Best Story")
fnConstructTopicStd(              true, "Sarulente",         "Mt. Sarulente")
fnConstructTopicStd(             false, "Bunny Story",       "Bunnies")
fnConstructTopicStd(              true, "Missing Merchants", "Missing Merchants")
fnConstructTopicStd(              true, "Westwoods Greens",  "Westwoods Greens")
