-- |[ =================================== Mia: Mt. Sarulente =================================== ]|
--Mia explains Mt. Sarulente's rumours.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Mia")

--Common.
WD_SetProperty("Major Sequence Fast", true)

--Flag.
VM_SetVar("Root/Variables/Chapter2/MtSarulente/iMiaGaveEmpressQuest", "N", 1.0)

-- |[Variables]|
local iEmpressJoined     = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
local iTurnedInSarulente = VM_GetVar("Root/Variables/Chapter2/Mia/iTurnedInSarulente", "N")

-- |[Initial]|
if(iEmpressJoined == 0.0) then
    Append("Sanya:[E|Neutral] What has the rumour mill got in store for us?[B][C]")
    Append("Mia:[E|Neutral] So get this.[P] There's an old fella who lives over on Mount Sarulente.[B][C]")
    Append("Izuna:[E|Explain] That's on the west side of Northwoods, Sanya.[B][C]")
    Append("Mia:[E|Neutral] Yeah, one of the taller mountains on the west side of Trafal.[P] There are some ruins there.[B][C]")
    Append("Mia:[E|Neutral] The old guy is a hunter or something.[P] Nothing out of the ordinary, really.[P] But![B][C]")
    Append("Mia:[E|Neutral] There's a rumour going down that he's been meeting with a cloaked figure, in secret.[B][C]")
    Append("Sanya:[E|Happy] Yes![P] Snooping on old people is exactly what a hero should do![B][C]")
    Append("Mia:[E|Neutral] No, you dope![B][C]")
    Append("Mia:[E|Neutral] There are a lot of brigands in Westwoods and the Old Capital Grounds.[P] Word is there might be an attack coming.[B][C]")
    Append("Sanya:[E|Happy] Yes![P] Beating up old people is exactly what a hero should do![B][C]")
    Append("Izuna:[E|Neutral] I think she means we should look into this, Sanya.[P] We can't just go attacking people on hunches.[B][C]")
    Append("Sanya:[E|Neutral] Lame, but I guess you're right.[P] And if he's up to anything...[B][C]")
    Append("Sanya:[E|Offended] I'll knock his teeth down his wrinkly old neck![B][C]")
    Append("Izuna:[E|Ugh] Did you have a bad experience with an old person once?[B][C]")
    Append("Sanya:[E|Neutral] No, I'm just making it clear that being old isn't a defense.[P] I'd beat up an entire book club of old people if the principle was right.[B][C]")
    Append("Mia:[E|Neutral] I think you made that plenty clear.[B][C]")
    Append("Izuna:[E|Neutral] Anyway, I believe Mount Sarulente is the resting place of the old dragon empress.[B][C]")
    Append("Sanya:[E|Neutral] And?[B][C]")
    Append("Izuna:[E|Neutral] You know, the one that build the Shrine of the Hero?[P] The one that predicted your arrival here?[B][C]")
    Append("Sanya:[E|Neutral] And?[B][C]")
    Append("Izuna:[E|Ugh] There might be something important in her tomb, Sanya.[B][C]")
    Append("Mia:[E|Neutral] Worth a look.[P] Hell, maybe the old guy's a graverobber.[P] Get me the story, Izuna![B][C]")

-- |[Turn-In]|
elseif(iEmpressJoined == 1.0 and iTurnedInSarulente == 0.0) then

    --Flags.
    VM_SetVar("Root/Variables/Chapter2/Mia/iTurnedInSarulente", "N", 1.0)
    
    --Give item.
    LM_ExecuteScript(gsItemListing, "Pamphlet: Dragon")
    
    --Increment pamphlet count.
    local iPamphletCount = VM_GetVar("Root/Variables/Chapter2/Mia/iPamphletCount", "N")
    VM_SetVar("Root/Variables/Chapter2/Mia/iPamphletCount", "N", iPamphletCount + 1)
    
    --Dialogue.
    Append("Izuna:[E|Jot] Hoo boy have I got a story today, Mia![B][C]")
    Append("Mia:[E|Neutral] I'll say![P] She's huge![B][C]")
    Append("Empress:[E|Neutral] ...[B][C]")
    Append("Sanya:[E|Neutral] This is Empress.[P] She used to run the place a while ago.[B][C]")
    Append("Mia:[E|Neutral] She's still alive?[P] Amazing![P] Uh, is that all right to print?[B][C]")
    Append("Empress:[E|Neutral] I am not in hiding.[P] Go ahead.[B][C]")
    Append("Mia:[E|Neutral] (Goddess' Whiskers she's so freaking tall.[P] It's giving me a panic attack.)[B][C]")
    Append("Mia:[E|Neutral] Okay, let's have the story![B][C]")
    Append("Izuna:[E|Jot] Here's my notes, too.[B][C]")
    
    --First pamphlet publishing.
    if(iPamphletCount == 0.0) then
        Append("Mia:[E|Neutral] Great![P] The story will be everywhere before you know it![B][C]")
        Append("Mia:[E|Neutral] [SOUND|World|TakeItem]Here, take an extra.[P] You can use the field ability 'Extra, Extra!' to show it to people![B][C]")
        Append("Izuna:[E|Explain] Yeah![P] We can help distribution, Sanya![B][C]")
        Append("Sanya:[E|Smirk] Giving pamphlets to everyone on the glacier and hearing what they have to say?[P] Sounds like a staggering amount of work.[B][C]")
        Append("Izuna:[E|Smirk] But I think we're up to it![B][C]")
        Append("Zeke:[E|Neutral] Bah![B][C]")
        
        --Flag.
        VM_SetVar("Root/Variables/Global/Izuna/sPamphlet", "S", "Dragon")
        
        --Add 'Extra, Extra!' to the first unused Field Ability slot. If none are open, do nothing.
        local iUseSlot = -1
        for i = 0, 4, 1 do
            if(AdvCombat_GetProperty("Field Ability Name", i) == "Null") then
                iUseSlot = i
                break
            end
        end
        if(iUseSlot ~= -1) then
            AdvCombat_SetProperty("Set Field Ability", iUseSlot, "Root/Special/Combat/FieldAbilities/Extra Extra")
        end
        
    --Second pamphlet publishing.
    elseif(iPamphletCount == 1.0) then
        Append("Mia:[E|Neutral] All right, hot off the presses![B][C]")
        Append("Mia:[E|Neutral] [SOUND|World|TakeItem]Here's your extra.[P] Since you have multiple pamphlets now, you can use the field ability 'Change Pamphlet' to switch which one you're showing people.[B][C]")
        Append("Izuna:[E|Smirk] Thanks a lot, Mia![B][C]")
        
        --Add 'Change Pamphlet' to the first unused Field Ability slot. If none are open, do nothing.
        local iUseSlot = -1
        for i = 0, 4, 1 do
            if(AdvCombat_GetProperty("Field Ability Name", i) == "Null") then
                iUseSlot = i
                break
            end
        end
        if(iUseSlot ~= -1) then
            AdvCombat_SetProperty("Set Field Ability", iUseSlot, "Root/Special/Combat/FieldAbilities/Change Pamphlet")
        end
    
    --All future:
    else
        Append("Mia:[E|Neutral] I'll have these halfway to Jeffespeir by tomorrow![P] Great work![B][C]")
        Append("Mia:[E|Neutral] [SOUND|World|TakeItem]Here's your extra.[P] Keep it up, Izuna.[B][C]")
    end

-- |[Repeats]|
else
    Append("Mia:[E|Neutral] Great job on the Sarulente story, Izuna.[P] So, Empress, you've been living there for 700 years?[P] Nobody knew about you this whole time?[B][C]")
    Append("Empress:[E|Neutral] Many people have.[P] I was not hiding.[P] Though I did tour the world, seeing far off places.[P] I would be gone for years at a time.[B][C]")
    Append("Mia:[E|Neutral] So you've been around, but nobody knew.[P] Well, everyone will now![P] That's the power of the printing press![B][C]")
    Append("Empress:[E|Neutral] Quite a piece of technology.[P] We certainly could have saved the labour needed to hand write educational materials.[B][C]")
    Append("Izuna:[E|Smirk] I hope a lot of people don't bother you, now.[B][C]")
    Append("Empress:[E|Neutral] That is what the giant dragon animatronic was for.[P] It was extremely effective.[B][C]")
    Append("Mia:[E|Neutral] Uh, yeah, I left that part out of the story, Izuna.[B][C]")
    Append("Sanya:[E|Neutral] Hope your statue girls like cleaning after the freshly-evacuated bowels of tourists.[B][C]")

end