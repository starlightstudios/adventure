-- |[ ================================= Mia: Missing Merchants ================================= ]|
--Someone went missing in Westwoods!

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Mia")

--Common.
WD_SetProperty("Major Sequence Fast", true)

--Flag.
VM_SetVar("Root/Variables/Chapter2/Westwoods/iMiaGaveCartQuest", "N", 1.0)

-- |[Variables]|
local iHelpedTakahn         = VM_GetVar("Root/Variables/Chapter2/Westwoods/iHelpedTakahn", "N")
local iTurnedInMerchantCart = VM_GetVar("Root/Variables/Chapter2/Mia/iTurnedInMerchantCart", "N")

-- |[Initial]|
if(iHelpedTakahn == 0.0) then
    Append("Mia:[E|Neutral] Izuna, I heard from some of the warriors that a shipping company lost a delivery in Westwoods.[B][C]")
    Append("Izuna:[E|Neutral] People go missing there all the time.[P] That's not really a story.[B][C]")
    Append("Mia:[E|Neutral] I know it isn't.[P] Can you take your burly protector over there and see if you can find them?[B][C]")
    Append("Mia:[E|Neutral] Our obligations as journalists don't override our obligations as kitsunes![B][C]")
    Append("Izuna:[E|Neutral] What do you think, Sanya?[P] Can we hunt down some missing merchants?[B][C]")
    Append("Sanya:[E|Blush] Zeke has a nose for trouble.[B][C]")
    Append("Zeke:[E|Neutral] Nyeh nyeh.[B][C]")
    Append("Sanya:[E|Neutral] See?[P] Piece of cake.[P] Let's go find some carts.[B][C]")

-- |[Turn-In]|
elseif(iHelpedTakahn == 1.0 and iTurnedInMerchantCart == 0.0) then

    --Flags.
    VM_SetVar("Root/Variables/Chapter2/Mia/iTurnedInMerchantCart", "N", 1.0)
    
    --Give item.
    LM_ExecuteScript(gsItemListing, "Pamphlet: Bandits in Westwoods")
    
    --Increment pamphlet count.
    local iPamphletCount = VM_GetVar("Root/Variables/Chapter2/Mia/iPamphletCount", "N")
    VM_SetVar("Root/Variables/Chapter2/Mia/iPamphletCount", "N", iPamphletCount + 1)
    
    --Dialogue.
    Append("Izuna:[E|Jot] Mia, got a story here. Bandits in Westwoods![P] They attacked a merchant caravan, and will surely strike again.[B][C]")
    Append("Mia:[E|Neutral] Bandits?[P] Not wildlife?[B][C]")
    Append("Izuna:[E|Jot] Yep![P] I guess things are getting desperate down south.[P] There's apparently a lot of them, and more coming.[B][C]")
    Append("Mia:[E|Neutral] And they're attacking merchants on the imperial highway?[P] This is bad.[B][C]")
    Append("Sanya:[E|Neutral] Are bandits not a thing normally?[B][C]")
    Append("Mia:[E|Neutral] Westwoods is full of dangerous wildlife.[P] You're likely to get mauled trying to rob someone.[P] That means you're either crazy, or desperate.[B][C]")
    Append("Mia:[E|Neutral] I heard Sonoko saying there have been major crop failures lately.[P] I'm guessing it's the desperate one.[B][C]")
    Append("Sanya:[E|Offended] Crazy never hurts![B][C]")
    Append("Mia:[E|Neutral] Anyway, this is a public service pamphlet.[P] Great work getting the story, Izuna.[P] I'll get it ready to print.[B][C]")
    
    --First pamphlet publishing.
    if(iPamphletCount == 0.0) then
        Append("Mia:[E|Neutral] There we go![P] The story will be everywhere before you know it![B][C]")
        Append("Mia:[E|Neutral] [SOUND|World|TakeItem]Here, take an extra.[P] You can use the field ability 'Extra, Extra!' to show it to people![B][C]")
        Append("Izuna:[E|Explain] Yeah![P] We can help distribution, Sanya![B][C]")
        Append("Sanya:[E|Smirk] Giving pamphlets to everyone on the glacier and hearing what they have to say?[P] Sounds like a staggering amount of work.[B][C]")
        Append("Izuna:[E|Smirk] But I think we're up to it![B][C]")
        Append("Zeke:[E|Neutral] Bah![B][C]")
        
        --Flag.
        VM_SetVar("Root/Variables/Global/Izuna/sPamphlet", "S", "Bandits in Westwoods")
        
        --Add 'Extra, Extra!' to the first unused Field Ability slot. If none are open, do nothing.
        local iUseSlot = -1
        for i = 0, 4, 1 do
            if(AdvCombat_GetProperty("Field Ability Name", i) == "Null") then
                iUseSlot = i
                break
            end
        end
        if(iUseSlot ~= -1) then
            AdvCombat_SetProperty("Set Field Ability", iUseSlot, "Root/Special/Combat/FieldAbilities/Extra Extra")
        end
        
    --Second pamphlet publishing.
    elseif(iPamphletCount == 1.0) then
        Append("Mia:[E|Neutral] All right, hot off the presses![B][C]")
        Append("Mia:[E|Neutral] [SOUND|World|TakeItem]Here's your extra.[P] Since you have multiple pamphlets now, you can use the field ability 'Change Pamphlet' to switch which one you're showing people.[B][C]")
        Append("Izuna:[E|Smirk] Thanks a lot, Mia![B][C]")
        
        --Add 'Change Pamphlet' to the first unused Field Ability slot. If none are open, do nothing.
        local iUseSlot = -1
        for i = 0, 4, 1 do
            if(AdvCombat_GetProperty("Field Ability Name", i) == "Null") then
                iUseSlot = i
                break
            end
        end
        if(iUseSlot ~= -1) then
            AdvCombat_SetProperty("Set Field Ability", iUseSlot, "Root/Special/Combat/FieldAbilities/Change Pamphlet")
        end
    
    --All future:
    else
        Append("Mia:[E|Neutral] I'll have these halfway to Jeffespeir by tomorrow![P] Great work![B][C]")
        Append("Mia:[E|Neutral] [SOUND|World|TakeItem]Here's your extra.[P] Keep it up, Izuna.[B][C]")
    end

-- |[Repeats]|
else

    --Variables.
    local iEmpressJoined = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")

    --Dialogue.
    Append("Mia:[E|Neutral] I'd ask the warriors to help patrol the highway, but we've got enough going on as it is.[B][C]")
    Append("Mia:[E|Neutral] You'd have to be crazy to try to set up in Westwoods, but it really is the prime spot to hit a caravan.[P] There's not a lot of other ways across Trafal.[B][C]")
    Append("Sanya:[E|Neutral] There's no other ways in?[B][C]")
    Append("Mia:[E|Neutral] None that are paved and good for shipping, other than, you know, by sea.[P] And going all the way around the continent won't work out, you have to go overland.[B][C]")
    Append("Mia:[E|Neutral] Trafal is a choke point for the continent.[P] That's why the old dragon empire did so well.[B][C]")
    if(iEmpressJoined == 1.0) then
        Append("Empress:[E|Neutral] The mountains are a natural barrier, so control of the access points allows control of trade.[B][C]")
        Append("Empress:[E|Neutral] Taxing trade allowed us to maintain an army and upkeep the roads.[P] Even with a smaller population base than the coasts, we had the real power.[B][C]")
        Append("Empress:[E|Neutral] If Trafal loses control of its entrances, it may lose its relevance to trade.[P] These bandits must be dealt with.[B][C]")
        Append("Izuna:[E|Smirk] Did this happen in the old empire?[B][C]")
        Append("Empress:[E|Neutral] Frequently, except those bandits had some pathetic king at their head.[P] Cleaning up raiders was the daily job of the army.[B][C]")
        Append("Mia:[E|Neutral] We've done our job publishing about this matter.[P] I hope the elders take it seriously.[B][C]")
    end
end