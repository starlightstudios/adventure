-- |[ ==================================== Mia: Bunny Story ==================================== ]|
--Mia receives a report on the bunnies. This topic won't appear until the story is ready to turn in.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Mia")

--Common.
WD_SetProperty("Major Sequence Fast", true)

-- |[Variables]|
local iTurnedInBunnies = VM_GetVar("Root/Variables/Chapter2/Mia/iTurnedInBunnies", "N")

-- |[Initial]|
--None!

-- |[Turn-In]|
if(iTurnedInBunnies == 0.0) then

    --Flags.
    VM_SetVar("Root/Variables/Chapter2/Mia/iTurnedInBunnies", "N", 1.0)
    
    --Give item.
    LM_ExecuteScript(gsItemListing, "Pamphlet: Bun Runners")
    
    --Increment pamphlet count.
    local iPamphletCount = VM_GetVar("Root/Variables/Chapter2/Mia/iPamphletCount", "N")
    VM_SetVar("Root/Variables/Chapter2/Mia/iPamphletCount", "N", iPamphletCount + 1)
    
    --Dialogue.
    Append("Izuna:[E|Jot] Mia, I've got a scoop![P] An interview with Angelface, who runs the local bunny smuggling racket![B][C]")
    Append("Mia:[E|Neutral] Angelface?[P] The gangster?[P] How'd you manage that?[B][C]")
    Append("Sanya:[E|Blush] I ran into an old friend...[B][C]")
    Append("Mia:[E|Neutral] Give me the details![B][C]")
    Append("Izuna:[E|Jot] Okay, get this![P] She's got a harem of bunny boys![P] She partially transforms them and has tons and tons of sex![P] All the time![B][C]")
    Append("Mia:[E|Neutral] Wow![P] And she's not going to come after us for publishing this?[B][C]")
    Append("Izuna:[E|Explain] She wants us to.[P] She considers it advertising![B][C]")
    Append("Mia:[E|Neutral] Are you sure that's wise?[P] What if we're responsible for some impressionable young fellows getting bunnied and falling into a life of crime?[B][C]")
    Append("Izuna:[E|Smirk] The smugglers aren't actually doing anything illegal in Trafal.[P] Drugs, booze, exotic foods.[P] I don't care if some king someplace disapproves.[B][C]")
    Append("Mia:[E|Neutral] All right.[P] I guess the bunnies aren't any worse than any other group around here.[P] I'll get this ready to print.[B][C]")
    
    --First pamphlet publishing.
    if(iPamphletCount == 0.0) then
        Append("Mia:[E|Neutral] There we go![P] The story will be everywhere before you know it![B][C]")
        Append("Mia:[E|Neutral] [SOUND|World|TakeItem]Here, take an extra.[P] You can use the field ability 'Extra, Extra!' to show it to people![B][C]")
        Append("Izuna:[E|Explain] Yeah![P] We can help distribution, Sanya![B][C]")
        Append("Sanya:[E|Smirk] Giving pamphlets to everyone on the glacier and hearing what they have to say?[P] Sounds like a staggering amount of work.[B][C]")
        Append("Izuna:[E|Smirk] But I think we're up to it![B][C]")
        Append("Zeke:[E|Neutral] Bah![B][C]")
        
        --Flag.
        VM_SetVar("Root/Variables/Global/Izuna/sPamphlet", "S", "Bun Runners")
        
        --Add 'Extra, Extra!' to the first unused Field Ability slot. If none are open, do nothing.
        local iUseSlot = -1
        for i = 0, 4, 1 do
            if(AdvCombat_GetProperty("Field Ability Name", i) == "Null") then
                iUseSlot = i
                break
            end
        end
        if(iUseSlot ~= -1) then
            AdvCombat_SetProperty("Set Field Ability", iUseSlot, "Root/Special/Combat/FieldAbilities/Extra Extra")
        end
        
    --Second pamphlet publishing.
    elseif(iPamphletCount == 1.0) then
        Append("Mia:[E|Neutral] All right, hot off the presses![B][C]")
        Append("Mia:[E|Neutral] [SOUND|World|TakeItem]Here's your extra.[P] Since you have multiple pamphlets now, you can use the field ability 'Change Pamphlet' to switch which one you're showing people.[B][C]")
        Append("Izuna:[E|Smirk] Thanks a lot, Mia![B][C]")
        
        --Add 'Change Pamphlet' to the first unused Field Ability slot. If none are open, do nothing.
        local iUseSlot = -1
        for i = 0, 4, 1 do
            if(AdvCombat_GetProperty("Field Ability Name", i) == "Null") then
                iUseSlot = i
                break
            end
        end
        if(iUseSlot ~= -1) then
            AdvCombat_SetProperty("Set Field Ability", iUseSlot, "Root/Special/Combat/FieldAbilities/Change Pamphlet")
        end
    
    --All future:
    else
        Append("Mia:[E|Neutral] I'll have these halfway to Jeffespeir by tomorrow![P] Great work![B][C]")
        Append("Mia:[E|Neutral] [SOUND|World|TakeItem]Here's your extra.[P] Keep it up, Izuna.[B][C]")
    end

-- |[Repeats]|
else
    Append("Mia:[E|Neutral] This one has to be one of our steamier stories.[P] A harem of bunny boys?[P] That's practically lurid.[B][C]")
    Append("Izuna:[E|Smirk] All part of the job, Mia.[P] Any blowback on it yet?[B][C]")
    Append("Mia:[E|Neutral] None of the elders have a problem with the story.[P] I guess the 'lured into a life of crime' angle doesn't bother them.[B][C]")
    Append("Izuna:[E|Neutral] Maybe we're just overreacting.[P] Why couldn't they be a bunny courier service...[B][C]")
    Append("Mia:[E|Neutral] Smugglers would just hire a courier to move illegal goods, then.[P] At least the bunnies know about the danger.[B][C]")
    Append("Mia:[E|Neutral] What if some innocent courier was carrying something valuable, and got ambushed, shot, and dumped in a shallow grave someplace?[B][C]")
    Append("Izuna:[E|Neutral] Yeah, okay, Mia.[P] You made your point.[B][C]")
end