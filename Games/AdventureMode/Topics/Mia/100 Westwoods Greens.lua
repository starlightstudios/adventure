-- |[ ================================= Mia: Westwoods Greens ================================== ]|
--Mia explains a rumour about greenery in Westwoods.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Mia")

--Common.
WD_SetProperty("Major Sequence Fast", true)

--Flag.
VM_SetVar("Root/Variables/Chapter2/FortKirysu/iMiaGaveAlrauneQuest", "N", 1.0)

-- |[Variables]|
local iBefriendedAlraunes      = VM_GetVar("Root/Variables/Chapter2/FortKirysu/iBefriendedAlraunes", "N")
local iTurnedInWestwoodsGreens = VM_GetVar("Root/Variables/Chapter2/Mia/iTurnedInWestwoodsGreens", "N")

-- |[Initial]|
if(iBefriendedAlraunes == 0.0) then
    Append("Mia:[E|Neutral] Izuna, I've got something that might be worth looking into.[P] You know the Westwoods?[B][C]")
    Append("Izuna:[E|Neutral] Who doesn't?[B][C]")
    Append("Mia:[E|Neutral] Get this, apparently there's green grass and trees growing in there someplace![B][C]")
    Append("Izuna:[E|Happy] Really!?[B][C]")
    Append("Sanya:[E|Happy] Yeah, absolutely![P] I love green things![B][C]")
    Append("Sanya:[E|Neutral] ...[P] Is that a big deal?[B][C]")
    Append("Mia:[E|Neutral] Nothing grows there, or at least, nothing that isn't a mutated freak that will try to strangle you.[B][C]")
    Append("Mia:[E|Neutral] A couple mercs at Poterup swore they saw some green grass.[P] And it's growing fast, too.[P] They said it's spreading![B][C]")
    Append("Izuna:[E|Smirk] That's great news![P] Sanya, let's go verify it![P] This has to be a story![B][C]")
    Append("Sanya:[E|Neutral] Awesome.[P] Let's go.[B][C]")

-- |[Turn-In]|
elseif(iTurnedInWestwoodsGreens == 0.0) then

    --Flags.
    VM_SetVar("Root/Variables/Chapter2/Mia/iTurnedInWestwoodsGreens", "N", 1.0)
    
    --Give item.
    LM_ExecuteScript(gsItemListing, "Pamphlet: Green Westwoods")
    
    --Increment pamphlet count.
    local iPamphletCount = VM_GetVar("Root/Variables/Chapter2/Mia/iPamphletCount", "N")
    VM_SetVar("Root/Variables/Chapter2/Mia/iPamphletCount", "N", iPamphletCount + 1)
    
    --Dialogue.
    Append("Izuna:[E|Jot] Mia, I've got a story for you![B][C]")
    Append("Mia:[E|Neutral] Ah, the Westwoods article.[P] What's going on?[B][C]")
    Append("Izuna:[E|Jot] There's an ancient tree that has been in Fort Kirysu for who knows how long, quietly cleaning the soil and water.[B][C]")
    Append("Izuna:[E|Jot] Recently, it called a covenant of alraunes to it, and with their efforts, the greenery is spreading.[B][C]")
    Append("Mia:[E|Neutral] Really?[P] Wow, that's wonderful![P] Did you get an interview?[B][C]")
    Append("Izuna:[E|Jot] Sure did![P] Their leader is named Marigold, and they all had dreams about the tree in Westwoods?[B][C]")
    Append("Mia:[E|Neutral] And you didn't interview the tree?[B][C]")
    Append("Izuna:[E|Ugh] ...[P] I'm not an alraune, Mia.[B][C]")
    Append("Mia:[E|Neutral] Ha ha![P] Gotcha![B][C]")
    Append("Izuna:[E|Smirk] Just think of it![P] Maybe within our lifetimes, Westwoods won't be a nightmare wasteland![B][C]")
    Append("Sanya:[E|Neutral] It wasn't a pleasant place.[P] There wasn't much for Zeke to eat.[B][C]")
    Append("Zeke:[E|Neutral] BAH![B][C]")
    Append("Sanya:[E|Neutral] I know, buddy.[P] I'll take you out for something tasty later.[B][C]")
    Append("Mia:[E|Neutral] Okay, let's get to printing![B][C]")
    
    --First pamphlet publishing.
    if(iPamphletCount == 0.0) then
        Append("Mia:[E|Neutral] There we go![P] The story will be everywhere before you know it![B][C]")
        Append("Mia:[E|Neutral] [SOUND|World|TakeItem]Here, take an extra.[P] You can use the field ability 'Extra, Extra!' to show it to people![B][C]")
        Append("Izuna:[E|Explain] Yeah![P] We can help distribution, Sanya![B][C]")
        Append("Sanya:[E|Smirk] Giving pamphlets to everyone on the glacier and hearing what they have to say?[P] Sounds like a staggering amount of work.[B][C]")
        Append("Izuna:[E|Smirk] But I think we're up to it![B][C]")
        Append("Zeke:[E|Neutral] Bah![B][C]")
        
        --Flag.
        VM_SetVar("Root/Variables/Global/Izuna/sPamphlet", "S", "Green Westwoods")
        
        --Add 'Extra, Extra!' to the first unused Field Ability slot. If none are open, do nothing.
        local iUseSlot = -1
        for i = 0, 4, 1 do
            if(AdvCombat_GetProperty("Field Ability Name", i) == "Null") then
                iUseSlot = i
                break
            end
        end
        if(iUseSlot ~= -1) then
            AdvCombat_SetProperty("Set Field Ability", iUseSlot, "Root/Special/Combat/FieldAbilities/Extra Extra")
        end
        
    --Second pamphlet publishing.
    elseif(iPamphletCount == 1.0) then
        Append("Mia:[E|Neutral] All right, hot off the presses![B][C]")
        Append("Mia:[E|Neutral] [SOUND|World|TakeItem]Here's your extra.[P] Since you have multiple pamphlets now, you can use the field ability 'Change Pamphlet' to switch which one you're showing people.[B][C]")
        Append("Izuna:[E|Smirk] Thanks a lot, Mia![B][C]")
        
        --Add 'Change Pamphlet' to the first unused Field Ability slot. If none are open, do nothing.
        local iUseSlot = -1
        for i = 0, 4, 1 do
            if(AdvCombat_GetProperty("Field Ability Name", i) == "Null") then
                iUseSlot = i
                break
            end
        end
        if(iUseSlot ~= -1) then
            AdvCombat_SetProperty("Set Field Ability", iUseSlot, "Root/Special/Combat/FieldAbilities/Change Pamphlet")
        end
    
    --All future:
    else
        Append("Mia:[E|Neutral] I'll have these halfway to Jeffespeir by tomorrow![P] Great work![B][C]")
        Append("Mia:[E|Neutral] [SOUND|World|TakeItem]Here's your extra.[P] Keep it up, Izuna.[B][C]")
    end

-- |[Repeats]|
else
    Append("Mia:[E|Neutral] You think the temple should offer to help the alraunes?[P] Should I bring the matter up at the next meeting?[B][C]")
    Append("Izuna:[E|Smirk] I think that's a great idea.[P] If we all work together, I'm sure Westwoods will heal in no time at all.[B][C]")

end