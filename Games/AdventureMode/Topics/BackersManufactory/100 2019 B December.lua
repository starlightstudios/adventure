-- |[Backers]|
--Backer terminal.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "BackersManufactory")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Dialogue.
	Append("Christine:[E|Neutral] Let's see which of my dream sequences 55 was looking over...[B][C]")
	Append("Christine:[E|Neutral] 'File MEM-5, 'Abrissgurke'.[P] Three slime girls, Lew, Goop-Sinpai, and Aaaac assemble around a table and prepare to eat an organic food substance.[P] Memory files label it 'Pizza'.'[B][C]")
	Append("Christine:[E|Neutral] 'Lew halts the other slimes and produces a pineapple from nowhere, then places it on the pizza.[P] She eats a slice of pizza with pineapple on it, including the rind and leaves, in one bite.'[B][C]")
	Append("Christine:[E|Neutral] 'A group of pineapples appear from the sky and surround Lew, who has a series of hallucinatory dances.[P] A human named James Upton appears and is squished by a pineapple.'[B][C]")
	Append("Christine:[E|Neutral] 'Moments later, we return to the table.[P] Sinpai and Aaaac are horrified to see Lew has become a pineapple girl.[P] James Upton is nearby, also a pineapple girl.'[B][C]")
	Append("Christine:[E|Neutral] 'The pineapple girls place more pineapples on the pizza.[P] Horrified, Sinpai and Aaaac eat it, complimenting its flavor.[P] More pineapples appear from the sky.'[B][C]")
	Append("Christine:[E|Neutral] '2855's Notes::[P] The music that played in the dream sequence was most enjoyable.[P] Pineapple girls are not a partirhuman species.'[B][C]")
	Append("Christine:[E|Laugh] (Live just a little, 55...)[B][C]")
	Append("Christine:[E|Neutral] 'File JR-12 'Repeated Meme'.[P] Two golems are showering, cleaning biological residue off their chassis.[P] They are Pandavenger and Kumquat.'[B][C]")
	Append("Christine:[E|Neutral] 'The drain clogs and one of the golems reaches into the pipe to clear it.[P] Organic material begins bubbling out of the pipe.[P] The golems flee.'[B][C]")
	Append("Christine:[E|Neutral] 'The material bursts through the floor and grows across the walls and floors.[P] An emergency response crew headed by Austin Durbin attempts to set fire to the material, but it regenerates as fast as it burns.'[B][C]")
	Append("Christine:[E|Neutral] 'The sector is evacuated and bombarded by long-range artillery.[P] The organic material is not contained.'[B][C]")
	Append("Christine:[E|Neutral] '2855's Notes::[P] Possible future memory, though Christine herself is not present.[P] Organic material may be related to project Vivify.'[B][C]")
	Append("Christine:[E|Sad] I don't remember having this dream...[B][C]")
	Append("Christine:[E|Neutral] 'File Q19-P, 'Taedas'.[P] A lord golem and two slaves are sitting around a table.[P] The lord is named Klaysee, the others are Dyamonde and Sudoku.'[B][C]")
	Append("Christine:[E|Neutral] 'Klaysee begins playing a game with a knife, stabbing it between her fingers on the table.[P] Dyamonde and Sudoku begin to clap, with each clap matching a stab of the knife.'[B][C]")
	Append("Christine:[E|Neutral] 'The tempo slowly increases as the claps speed up until Klaysee's motions with the knife blur together.[P] Eventually, light itself begins to be pulled in.'[B][C]")
	Append("Christine:[E|Neutral] 'The dream goes dark, and it is revealed this was taking place inside of a pot in a kitchen.[P] Klaysee steps out of the pot and places spaghetti noodles in it.[P] Dyamonde and Sudoku do not mind the boiling water as golems can withstand the temperature.'[B][C]")
	Append("Christine:[E|Neutral] 'Klaysee serves the spaghetti with Dyamonde and Sudoku on it.[P] The golems eat it from within.'[B][C]")
	Append("Christine:[E|Neutral] '2855's Notes::[P] Unlikely future memory unless the spaghetti is a metaphor.'[B][C]")
	Append("Christine:[E|Smirk] Well 55, in dreams, everything is a metaphor...[B][C]")
end
