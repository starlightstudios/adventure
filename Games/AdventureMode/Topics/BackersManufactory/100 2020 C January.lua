-- |[Backers]|
--Backer terminal.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "BackersManufactory")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Variables.
    local iSXMet55 = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXMet55", "N")
    
    --Dialogue.
	Append("Christine:[E|Neutral] Let's see which of my dream sequences 55 was looking over...[B][C]")
	Append("Christine:[E|Neutral] 'File R7-D 'Abrissgurke'.[P] Long series of images of a location in Cornwall, a tourist destination.[P] Perception records disgust looking at crane game.'[B][C]")
	Append("Christine:[E|Neutral] 'Crane game filled entirely with bread loaves of varying descriptions, all pristine.[P] Wine and cigarettes can be found in a nearby vending machine.'[B][C]")
	Append("Christine:[E|Neutral] '2855's Notes::[P] Ask Christine what 'Cornwall' is and verify accuracy.'[B][C]")
	Append("Christine:[E|Sad] Well, if it's Cornwall, it's probably tacky, boorish wine.[B][C]")
	Append("Christine:[E|Smirk] I went there on vacation once, so that's got to be the genesis.[P] Maybe I ate something bad?[B][C]")
	Append("Christine:[E|Neutral] 'File P22-Q 'Sudoku'.[P] Perception is playing a card game.[P] There are two other players::[P] Memory identifies them as 'Kumquat' and 'Dyamonde'.'[B][C]")
	Append("Christine:[E|Neutral] 'These are animated characters from a TV show about giant fighting monsters.[P] Kumquat's hands are too big to hold the cards.[P] Dyamonde cheats openly but never bets anything.'[B][C]")
	Append("Christine:[E|Neutral] 'Midway through the dream, the floor falls out and the group parachutes on top of a whale who then joins the card game.[P] The player busts out and is mocked by the other players.'[B][C]")
	Append("Christine:[E|Neutral] '2855's Notes::[P] Verify fictional characters with Christine.[P] Appearances are inconsistent with recorded Earth fauna.'[B][C]")
	Append("Christine:[E|Blush] No, 55, that's what they actually look like.[P] It's a TV show I watched when I was a kid...[B][C]")
	Append("Christine:[E|Neutral] 'File INF 'Lew'.[P] A greenish steam droid unit is hiding under a tree in a biological forest environment covered with snow.'[B][C]")
	Append("Christine:[E|Neutral] 'She leaps up and fires a rifle wildly into the air for several minutes.[P] File ends.'[B][C]")
    if(iSXMet55 == 1.0) then
        Append("Christine:[E|Neutral] '2855's Notes::[P] Steam droid resembles SX-399, possible future memory.[P] Consult Christine for more.'[B][C]")
        Append("Christine:[E|Blush] I had that dream before we met SX-399.[P] I wonder if it's a future memory?[B][C]")
    else
        Append("Christine:[E|Neutral] '2855's Notes::[P] Steam droid design is highly unusual, possible future memory.[P] Consult Christine for more information.'[B][C]")
        Append("Christine:[E|Sad] I've got nothing on that one.[P] Maybe we meet them soon?[B][C]")
    end
	Append("Christine:[E|Neutral] 'File RJQ-4 'Durbin'.[P] Perception is being arrested and interrogated by Administration forces.[P] Fellow units 'Morbo', 'Taedas', and 'Klaysee' are likewise being arrested.'[B][C]")
	Append("Christine:[E|Neutral] 'Unit 2855 appears and shocked a security unit, followed by chaos.[P] Morbo is hit by stray fire, Klaysee drags her to safety.[P] Shooting and shouting.[P] Memory goes dark.'[B][C]")
	Append("Christine:[E|Neutral] '2855's Notes::[P] Dream has several inconsistencies and is likely not a memory of the future.[P] Design of pulse weapons is incorrect, perception's body appears human and not a golem.[P] Try to identify the units in question and confirm their identities.'[B][C]")
	Append("Christine:[E|Neutral] 'File BR-12 'Pandavenger'.[P] Perception sits at the throne of a superior.[P] The entire environment is frozen, including the perception.[P] A command is heard to follow the superior 'Repeated Meme' on a walk.[P] Perception stands up and reveals her body is completely frozen.'[B][C]")
	Append("Christine:[E|Neutral] 'The perception eventually stands before a frozen individual 'Goop Sinpai', and remains there for eternity, unmoving.'[B][C]")
	Append("Christine:[E|Neutral] '2855's Notes::[P] This seems to be a dream about a work of fiction.[P] Confirm with Christine.'[B][C]")
	Append("Christine:[E|Blush] Yep, absolutely.[P] That's a story I read once...[P] I really liked it...[B][C]")
	Append("Christine:[E|Neutral] 'File PRO-FI 'James Upton'.[P] Perception is watching a group of idol singers 'Aaaac', and five backup dancers.[P] They are doing the Crunch.'[B][C]")
	Append("Christine:[E|Neutral] 'The Crunch is a series of contortions that eventually result in a damaged spinal column, but the singer has no internal bone structure.[P] They remold themselves.'[B][C]")
	Append("Christine:[E|Neutral] 'The backup dancers are not so lucky, and crack their spines.[P] They die on the stage and the crowd cheers.'[B][C]")
	Append("Christine:[E|Neutral] '2855's Notes::[P] Probably not a real event and likely a dream mixing multiple events.'[B][C]")
	Append("Christine:[E|Sad] I don't know about that, 55.[P] After what I've heard about K-Pop...[B][C]")
    
end
