-- |[Backers]|
--Backer terminal.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "BackersManufactory")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Dialogue.
	Append("Christine:[E|Neutral] Let's see which of my dream sequences 55 was looking over...[B][C]")
	Append("Christine:[E|Neutral] 'File T1-0 'Klaysee'.[P] A sex dungeon filled with framed photos marked with names like 'Goop-Sinpai', 'Taedas', and 'Pandavenger' are seen along the side of a stone corridor.[P] Jail cells contain people moaning in torment.'[B][C]")
	Append("Christine:[E|Neutral] 'At the head of the room, a column of magma illuminates a wide array of torture equipment.[P] BlaDragon, the mistress of the dungeon, pushes Lew in.'[B][C]")
	Append("Christine:[E|Neutral] 'Lew comes out the other side, the magma rapidly cooling to transform her into an invincible rock girl.[P] She then becomes the prime minister.'[B][C]")
	Append("Christine:[E|Neutral] '2855's Notes::[P] Incomprehensible series of events suggests this is a normal dream.'[B][C]")
	Append("Christine:[E|Laugh] For some definition of normal, 55![B][C]")
	Append("Christine:[E|Neutral] 'File 77B-B 'Repeated Meme'.[P] A man with a beard holds a beige puppet.[P] The beige puppet speaks and moves on its own and argues vociferously with its handler.'[B][C]")
	Append("Christine:[E|Neutral] 'A picture of a mouse named Dyamonde appears.[P] He is wearing a t-shirt that bursts into flame.[P] He spins in place several times and vomits.'[B][C]")
	Append("Christine:[E|Neutral] 'At that moment, a group of police officers appear and begin arresting everyone for being too silly.[P] This is likely a memory of a Monty Python sketch.[P] Monty Python is officially public domain so we can't be sued for this.'[B][C]")
	Append("Christine:[E|Neutral] '2855's Notes::[P] Find out what is wrong with Christine's home planet.'[B][C]")
	Append("Christine:[E|Happy] Monty Python...[P] I should re-create some of their best skits here on Regulus so everyone can enjoy them![B][C]")
	Append("Christine:[E|Neutral] 'File 77B-C 'Kumquat'.[P] The aforementioned puppet and handler are being interrogated by officers Abrissgurke and Aaaac.[P] They are being held on suspicion of excess silliness.'[B][C]")
	Append("Christine:[E|Neutral] 'A lawyer named MJS appears and begins claiming the arrest is illegal and the two are to be released at once.[P] He is then attacked by the mouse named Dyamonde, who vomits on him.'[B][C]")
	Append("Christine:[E|Neutral] 'MJS then wins an award named 'Most Precarious' and is given a prize of ten thousand pounds.'[B][C]")
	Append("Christine:[E|Neutral] '2855's Notes::[P] Find out what is wrong with Christine's home planet.'[B][C]")
	Append("Christine:[E|Smirk] Come on, 55.[P] Dreams are weird and fun![B][C]")
	Append("Christine:[E|Neutral] 'File 12-R 'James Upton'.[P] Perception awakens in a room surrounded by the still bodies of golems.'[B][C]")
	Append("Christine:[E|Neutral] 'A retired command unit named Sudoku is nearby.[P] The perception rips her eye out of the socket.[P] The golem bodies stand up and begin closing around the perception.'[B][C]")
	Append("Christine:[E|Neutral] 'Another command unit named Austin appears and extracts the perception.[P] She resembles Unit 2855 but has green hair.'[B][C]")
	Append("Christine:[E|Neutral] '2855's Notes::[P] Possible future memory, mixed with memories from the Cryogenics Incident.[P] A command unit with secondary designation Sudoku works in Sector 114 and matches the description in the memory.'[B][C]")
	Append("Christine:[E|Sad] Hmm, I don't think that's a future memory.[P] I think I saw her on a videograph announcing some new initiative and she got mixed in with a dream...[B][C]")
	Append("Christine:[E|Sad] What an awful thing to have to dream about...[B][C]")
    
    
end
