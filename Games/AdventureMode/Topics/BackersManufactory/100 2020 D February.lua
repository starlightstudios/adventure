-- |[Backers]|
--Backer terminal.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "BackersManufactory")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Dialogue.
	Append("Christine:[E|Neutral] Seems 55 already went through my dreams and left some notes...[B][C]")
	Append("Christine:[E|Neutral] 'File 00rE, 'Sinpai'.[P] Two humans in military garb are standing facing one another.[P] They are discussing soap.[P] Memory files identify them as Kumquat and Klaysee.'[B][C]")
	Append("Christine:[E|Neutral] 'Kumquat produces a series of metal ingots from her hat, ranging from iron to brass to tellurium.[P] Klaysee laughs and takes them, grinding them to powder in her hands.'[B][C]")
	Append("Christine:[E|Neutral] 'She changes the ingots into a guitar.[P] A werecat named Pandavenger appears and places her hand on the guitar.[P] She says 'Solo Battle'.'[B][C]")
	Append("Christine:[E|Neutral] 'All characters in the dream bump their heads together, transporting them to a small island somewhere on Earth.[P] A passing boat, the H.M.S. Morbo, has cheering humans on it.'[B][C]")
	Append("Christine:[E|Neutral] 'Kumquat and Klaysee begin playing guitars at one another.[P] The songs that play include instruments other than guitar.'[B][C]")
	Append("Christine:[E|Neutral] 'The boat crashes into the island.'[B][C]")
	Append("Christine:[E|Neutral] '2855's Notes::[P] Unlikely future memory.[P] Otherwise, standard dream.'[B][C]")
	Append("Christine:[E|Offended] Standard?[P] How can a dream be standard?[B][C]")
	Append("Christine:[E|Neutral] 'File 2772-B, 'Sudoku'.[P] A human in a white moth costume, called the 'Peppered Moth', is beating a criminal named Aaaac.'[B][C]")
	Append("Christine:[E|Neutral] 'They are standing in Abrissgurke First National Bank, a location where humans store their valuables.[P] Aaaac is beaten unconscious.'[B][C]")
	Append("Christine:[E|Neutral] 'Peppered Moth removes her mask to reveal she is Austin Durbin, billionaire philanthropist.[P] She calls a press conference inside the bank.[P] Aaaac is stomped on by a journalist looking for a photo.'[B][C]")
	Append("Christine:[E|Neutral] 'The journalist is named Taedas.[P] After snapping the photo, she is standing on the moon, where everyone except Peppered Moth has vanished.'[B][C]")
	Append("Christine:[E|Neutral] 'Peppered Moth shoots a grappling hook gun into the sun.[P] It catches and she pulls the sun towards here.[P] The moon is swallowed in plasmatic nuclear fire.'[B][C]")
	Append("Christine:[E|Neutral] '2855's Notes::[P] This is an example of the superhero genre.[P] Similar files appear periodically in Christine's memory.'[B][C]")
	Append("Christine:[E|Blush] I'll just leave out the part where Peppered Moth is my original character...[B][C]")
	Append("Christine:[E|Neutral] 'File FIREFIRE, 'Lew'.[P] Christine is visible in third person, sitting in a doctor's office.'[B][C]")
	Append("Christine:[E|Neutral] 'Dr. Dyamonde appears with a needle larger than her body.[P] Christine recoils.'[B][C]")
	Append("Christine:[E|Neutral] 'Dyamonde attempts to puncture Christine, but the needle continually misses until Dyamonde accidentally sticks herself.'[B][C]")
	Append("Christine:[E|Neutral] 'Christine's head is abruptly replaced with a sparrow's.[P] The sparrow begins making tweeting sounds.[P] Another patient, Repeated Meme, enters the office and steals Christine.'[B][C]")
	Append("Christine:[E|Neutral] 'Dyamonde withdraws the needle.[P] Mushrooms have grown on the floor.[P] A mushroom girl appears from the floor, named James Upton.'[B][C]")
	Append("Christine:[E|Neutral] 'The mushroom girl arrests Dr. Dyamonde for practicing medicine without a licence and for selling illegal bananas.'[B][C]")
	Append("Christine:[E|Neutral] '2855's Notes::[P] Fear of medical personnel appears in other dream sequences.[P] Not likely a future memory.'[B][C]")
	Append("Christine:[E|Sad] ...[P] I guess I really don't like doctors, do I?[B][C]")

end
