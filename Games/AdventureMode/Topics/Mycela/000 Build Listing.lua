-- |[ ===================================== Build Listing ====================================== ]|
--Builds a list of topics this NPC will respond to, and at what levels.
local sNPCName = "Mycela"

--Constants
local gciAvailableAtStart = 1
local gciMustBeUnlocked = -1

--Topic listing.
fnConstructTopic("Business Offer", "Business Offer", gciMustBeUnlocked,   sNPCName, 0)
fnConstructTopic("Mushraunes",     "Mushraunes",     gciAvailableAtStart, sNPCName, 0)
fnConstructTopic("Future Plans",   "Future Plans",   gciAvailableAtStart, sNPCName, 0)
