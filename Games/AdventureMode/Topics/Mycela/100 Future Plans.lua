-- |[ ================================== Mycela: Future Plans ================================== ]|
--Topic handler.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Mycela")

-- |[Dialogue]|
WD_SetProperty("Major Sequence Fast", true)
fnCutscene([[ Append("Mei:[E|Neutral] Mycela, what are you going to do from here?[B][C]") ]])
fnCutscene([[ Append("Mycela:[E|Demushed] I -[P] haven't even thought that far ahead.[P] What do you think I should do?[B][C]") ]])
fnCutscene([[ Append("Mycela:[E|Demushed] Rochea has been very nice to me, and I have much to learn.[P] But the fungus who did this to me and the other mushraunes, the one we called our great leader.[B][C]") ]])
fnCutscene([[ Append("Mycela:[E|Demushed] He's still at large, and I am afraid I am one of the few who can stop him.[P] But...[P] I want to stay here, with my friends.[B][C]") ]])
fnCutscene([[ Append("Mycela:[E|Demushed] I'm sorry, Mei.[P] I can't tell you what I want to do, because I don't know.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Understandable.[P] You've got a lot of time to figure it out.[P] Listen to the plants, I'm sure they'll guide you.[B][C]") ]])