-- |[ ================================= Mycela: Business Offer ================================= ]|
--Topic handler.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
--WD_SetProperty("Activate Topics After Dialogue", "Mycela")

-- |[Variables]|
local sMeiForm        = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local iHasJob_Agarist = VM_GetVar("Root/Variables/Global/Florentina/iHasJob_Agarist", "N")

-- |[Getting New Job]|
if(iHasJob_Agarist == 0.0) then
    
    --Flags.
    VM_SetVar("Root/Variables/Global/Florentina/iHasJob_Agarist", "N", 1.0)
    VM_SetVar("Root/Variables/Global/Mei/iMeiKnowsFlorentinaTFs", "N", 1.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ Append("Florentina:[E|Happy] A friend of yours has a business offer for me, eh?[P] I am intrigued.[P] What could a mushraune offer me?[B][C]") ]])
    fnCutscene([[ Append("Mycela:[E|Demushed] He is not a mushraune.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Surprise] My shock is only exceeded by my curiosity.[P] A human?[B][C]") ]])
    fnCutscene([[ Append("Mycela:[E|Demushed] A mushroom.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Offended] I now don't like where this is going.[B][C]") ]])
    fnCutscene([[ Append("Mycela:[E|Demushed] He followed our leader's call when we first started our coven, but he took issue with our tactics.[B][C]") ]])
    fnCutscene([[ Append("Mycela:[E|Demushed] We had to throw him in the jail cells and make sure his spores did not escape.[P] But, with Rochea's help, I have extricated him.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] (Hello?[P] Can you hear me?)[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Woah![P] Hey![P] Mei, did you say something?[B][C]") ]])
    if(sMeiForm == "Alraune") then
        fnCutscene([[ Append("Mei:[E|Neutral] No but I definitely heard someone speak.[P] It wasn't you?[B][C]") ]])
    else
        fnCutscene([[ Append("Mei:[E|Neutral] I didn't say anything.[P] Don't look at me like that.[B][C]") ]])
    end
    fnCutscene([[ Append("Florentina:[E|Neutral] (It is working![P] Ha ha!)[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] There it is again![B][C]") ]])
    fnCutscene([[ Append("Mycela:[E|Demushed] When parasitized by a mushroom, I can understand them.[P] When they are removed, I cannot.[B][C]") ]])
    fnCutscene([[ Append("Mycela:[E|Demushed] But my friend here has spent much time observing how the little ones speak with alraunes, and he can mimic it.[P] Not particularly well, but he is learning.[B][C]") ]])
    fnCutscene([[ Append("Mycela:[E|Demushed] The mushroom on the ground there is him.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] (Hello.)[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Okay so now you want me to make a deal with a mushroom?[P] A mushroom with no money?[P] You got a bunch of platina under your cap there, buddy?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] (I have heard much about you since Mycela rescued me.[P] Rochea says there is a price on your head, yes?)[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Choose your words carefully, fungus.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] (How would you like a disguise that no bounty hunter will anticipate, that you can hide at any time?)[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] You have my attention.[P] If this is going where I think it is...[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] (I answered the leader's call because I wanted to see the world.[P] He promised me a host so I could do that.[P] Bastard double-crossed me.)[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] (If you let me symbiotically bond with you, I can get what I want, and you get my fungal disguise.[P] I can hide myself, too.)[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] And this symbiotic bond.[P] Can it be undone?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] (Mycela did it.)[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Without killing you?[B][C]") ]])
    fnCutscene([[ Append("Mycela:[E|Demushed] I was led to believe his life would not be your concern.[P] Interesting.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Don't misunderstand me, Mycela.[P] Killing your business partners is a faux-pas.[B][C]") ]])
    if(sMeiForm == "Alraune") then
        fnCutscene([[ Append("Mei:[E|Neutral] I think it's a good idea![P] Uh, not the killing part, the partnership part.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Confused] Hey chuckles, why don't you parasitize Mei over there?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Confused] (I have heard from the one named Nadia that you were the ideal candidate.[P] Is that not true?)[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Blush] Ooh, flattery.[P] I think we might just get along after all.[B][C]") ]])
    else
        fnCutscene([[ Append("Mei:[E|Neutral] I am so lost, but I can gather from half the conversation what he wants to do with you.[B][C]") ]])
    end
    fnCutscene([[ Append("Mycela:[E|Demushed] In any case, the fungal roots can be safely removed with some simple chemicals.[P] He wants to see the world, and when he has seen his fill, you drop him off in a nice damp cave.[B][C]") ]])
    fnCutscene([[ Append("Mycela:[E|Demushed] He has been a voice of reason for me.[P] I appreciate the things he has said, he is part of why I am so happy to be out of that cult.[P] To see a person from my old life encouraging me in my new life, I cannot describe it.[B][C]") ]])
    fnCutscene([[ Append("Mycela:[E|Demushed] Please, Florentina, take him with you.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Blush] Awww, you talked me into it.[P] Okay.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] (Huh.[P] Usually there's a dialogue option for this.[P] 'Do you want to let Florentina become a mushraune?'.[P] Guess it's her decision, not mine.)[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Blush] So, what do we do next?[P] You just hop on?[B][C]") ]])
    fnCutscene([[ Append("Mycela:[E|Demushed] I believe you can do the process without my help.[P] You may want some privacy, though.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Blush] (I will walk you through it.)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    -- |[Fade Out]|
    fnCutsceneFadeOut(45)
    fnCutsceneWait(65)
    fnCutsceneBlocker()

    -- |[Costume Change]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] I just put this here?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] (Take your shirt off.)[B][C]") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] Show me yours and I'll show you mine.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] (I [P]*am*[P] showing you mine![P] This is what I look like![P] The mushroom is the reproduct-)[B][C]") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] Didn't need to hear that, buddy.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] (Now take the cap and put it -[P] don't rip it!)[B][C]") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] I ripped it.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] (That'll take weeks to regrow.[P] I'll be pulling the nutrition out of your body!)[B][C]") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] Don't be a wuss, I get into a lot of fights and you're going to be scuffed.[P] Besides, I eat sunlight.[P] Lots to go around.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] (Lastly, let me grow around your legs.[P] This will take a second.)[B][C]") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] Ow![P] Ow![P] That pinches![B][C]") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] (All done.[P] Now we're host and symbiotic fungus.[P] How do you feel?)[B][C]") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] ...[P] Miserable.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] (Is that normal?)[B][C]") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] I haven't been this kind of intimate in a long time.[P] Really bringing back memories.[P] This had better pay off.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()

    --Hide mushroom layer.
    fnCutsceneLayerDisabled("Mushroom", true)

    --Change costume.
    fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Florentina/Job_Agarist.lua") ]])
    fnCutsceneBlocker()
    
    -- |[Fade In]|
    fnCutsceneFadeIn(45)
    fnCutsceneBlocker()

    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Mycela", "Demushed") ]])
    fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Mycela") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] This is...[P] something.[P] I'd say it's been a while since I had a burden like this, but that'd be selling Mei short.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Happy] That means she likes the new guy.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] (If I may...)[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Offended] YEOWWWW![B][C]") ]])
    fnCutscene([[ Append("Mycela:[E|Demushed] What happened?[P] He just spoke normally.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Facepalm] Maybe normal to you![P] My ears are still ringing.[P] Cripes![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Buddy, you need to whisper to me, and I'll relay what you said.[P] Or I go deaf.[B][C]") ]])
    fnCutscene([[ Append("Mycela:[E|Demushed] So that is why they whisper...[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] He said he's sorry but he does like the fit.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Blush] Neat![P] And it's like you can transform![P] Just hide the fungus and poof![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] Heh, I can use this for all kinds of mayhem.[P] If you wanted to see the world, buddy, we're going to see a lot.[B][C]") ]])
    fnCutscene([[ Append("Mycela:[E|Demushed] I am glad the arrangement worked out.[P] I do not know if there are any other fungi who would like such an arrangement, but...[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] New friends are always a good thing.[P] Maybe you can forge an alliance between fungi and plants![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Don't get ahead of yourself, Mei.[P] We still have to get you home.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] Right, right.[B][C]") ]])
    fnCutscene([[ Append("Mycela:[E|Demushed] I hope my friend helps to the best of his ability.[P] Good luck.[B][C]") ]])

-- |[Has Job]|
else

    --Setup.
    WD_SetProperty("Activate Topics After Dialogue", "Mycela")
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ Append("Mycela:[E|Demushed] How are you two getting along?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Terribly.[P] He's just like Mei, always being kind and thoughtful.[P] I need to teach him the killer instinct.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] It's not enough to want something, you need a plan to get it and the ruthlessness to keep it.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] But, he does have the right attitude.[P] Willing to learn.[P] I think I can make a vicious merchant out of him yet.[B][C]") ]])
    fnCutscene([[ Append("Mycela:[E|Demushed] And what does he think of it?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Surprise] He thinks he should have charged me for the privilige![P] Why I never![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] He's learning so fast![P] I'm so proud.[B][C]") ]])
    fnCutscene([[ Append("Mycela:[E|Demushed] The arrangement is working, but what have I unleashed upon the world?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Laugh] Don't worry, I'll keep them under control.[B][C]") ]])

end
