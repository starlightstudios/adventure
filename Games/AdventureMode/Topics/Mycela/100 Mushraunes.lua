-- |[ =================================== Mycela: Mushraunes =================================== ]|
--Topic handler.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Mycela")

-- |[Dialogue]|
WD_SetProperty("Major Sequence Fast", true)
fnCutscene([[ Append("Mei:[E|Neutral] Tell me about the coven of mushraunes, from the beginning.[B][C]") ]])
fnCutscene([[ Append("Mycela:[E|Demushed] All right.[P] My first memory was waking up near that pool you found me at.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Were you an alraune?[B][C]") ]])
fnCutscene([[ Append("Mycela:[E|Demushed] Yes, but I do not know if I have been for a long time or not.[P] I don't remember anything...[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] Maybe they hit you with the cleansing fungus?[P] It's a thing alraunes cultivate in their lairs, makes you forget who you were.[P] Only works on alraunes.[B][C]") ]])
fnCutscene([[ Append("Mycela:[E|Demushed] Rochea mentioned that, I will assume it is possible.[B][C]") ]])
fnCutscene([[ Append("Mycela:[E|Demushed] At first I was alone, and very scared, but soon the great leader called to me.[P] I could hear him whisper, and I rose to find him.[B][C]") ]])
fnCutscene([[ Append("Mycela:[E|Demushed] Him may be incorrect, fungus do not have genders like we do.[P] In any case, I followed his whispers to find a small blue mushroom growing in a corner.[P] But his voice was soothing and he seemed to know everything.[B][C]") ]])
fnCutscene([[ Append("Mycela:[E|Demushed] He told me what an alraune is, that we speak to the little ones.[P] He lied to me, told me the little ones were fungus like him.[P] That my duty was to serve them and they would protect me.[B][C]") ]])
fnCutscene([[ Append("Mycela:[E|Demushed] He told me that I was to take him on my body and let him take over it.[P] So I did.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] Was that the mushroom we saw you covered in?[B][C]") ]])
fnCutscene([[ Append("Mycela:[E|Demushed] No, that fellow came much later.[P] He, too, was called by the great leader.[P] Many other fungi came.[P] They floated into the cave as he called, and I helped cultivate and protect their spores.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] The mushrooms perpetuate their intelligence with spores?[P] Interesting.[B][C]") ]])
fnCutscene([[ Append("Mycela:[E|Demushed] Indeed.[P] But it soon became clear I would not be able to carry all the followers around, especially as some became larger.[P] So I had to go out and catch more alraunes.[B][C]") ]])
fnCutscene([[ Append("Mycela:[E|Demushed] I covered them in the fungus and they soon became followers, too.[P] I realize now that they were not like me.[P] The fungus was controlling them by arousing them, making them live in delirium.[P] They don't know what they're doing, the fungus tells them what to do.[B][C]") ]])
fnCutscene([[ Append("Mycela:[E|Demushed] Whenever I questioned what I was doing, the great leader calmed me.[P] He always had an answer.[P] I was the highest alraune, but beneath the mushrooms.[P] I took orders from them, let them use my body to move around and do things.[B][C]") ]])
fnCutscene([[ Append("Mycela:[E|Demushed] They whispered such dark things to me.[P] Told me of making an army and smashing the humans to the west, and turning them into alraunes to grow our numbers.[P] I wanted to help them...[B][C]") ]])
fnCutscene([[ Append("Mycela:[E|Demushed] But the great leader said he had other covens to found, and took half of our sisters away.[P] I was left with the rest, and the mushroom on me soon grew so large and heavy I could hardly walk.[B][C]") ]])
fnCutscene([[ Append("Mycela:[E|Demushed] So I sat there, blissful and empty, as he ordered the others around.[P] Then you came, and showed me there is so much more to life than that.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] A cult.[P] A cult centered around this 'great leader', lying to you and the other alraunes.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] Makes sense, he was doing it for power.[P] Or maybe sex, I don't know how mushrooms do it.[P] Sex and power are the two things that motivate a good cult leader.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] Problem is, he left, with a bunch of others.[P] So there could be more mushraunes.[B][C]") ]])
fnCutscene([[ Append("Mycela:[E|Demushed] Yes, and I do not know where they went.[P] I am sorry...[B][C]") ]])