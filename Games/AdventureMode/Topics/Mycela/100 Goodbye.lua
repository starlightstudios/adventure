-- |[ ===================================== Mycela: Goodbye ==================================== ]|
--Script that fires when the player says goodbye to end the dialogue.
WD_SetProperty("Major Sequence Fast", true)

--Standard.
fnCutscene([[ Append("Mycela:[E|Demushed] Be careful on the road, my friends.") ]])
