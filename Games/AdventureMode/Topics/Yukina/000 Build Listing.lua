-- |[ ===================================== Build Listing ====================================== ]|
--Builds a list of topics this NPC will respond to, and at what levels.
local sNPCName = "Yukina"
gsActiveNPCName = "Yukina"

--              ||||| Unlocked At Start |||  Filename      |||  Display Name (Optional)
fnConstructTopicStd(              true, "Legend",        "The Legend")
fnConstructTopicStd(              true, "Happiness",     "Happiness")
fnConstructTopicStd(             false, "Nekomata",      "Nekomata")
