-- |[ ==================================== Yukina: Happiness =================================== ]|
--But are you happy?

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Yukina")
	
--Common.
WD_SetProperty("Major Sequence Fast", true)

--Dialogue.
Append("Sanya:[E|Offended] You're really wise, Yukina, but are you happy?[B][C]")
Append("Yukina:[E|Neutral] Yes.[P] Very.[B][C]")
Append("Sanya:[E|Neutral] Oh.[P] Cool.[B][C]")
Append("Sanya:[E|Neutral] Care to explain?[B][C]")
Append("Yukina:[E|Neutral] Oh, you youngsters![P] Of course I'm very happy![P] I've lived a full life.[B][C]")
Append("Yukina:[E|Neutral] Learning.[P] Danger.[P] Good friends.[P] I have regrets, sure, but on the whole I've had a great time.[P] Wisdom brings happiness with it.[B][C]")
Append("Yukina:[E|Neutral] Whatever problems happen in the world are the world's, not mine.[P] We do what we can to solve them, but it must be taken as fact that we are small.[B][C]")
Append("Yukina:[E|Neutral] I know my place in the world.[P] It's not very big.[P] Nobody's is very big.[P] I just do right by those around me, and they do right by me, and that's enough.[P] That's real wisdom.[B][C]")
Append("Sanya:[E|Sad] But isn't there a great evil stirring?[P] Don't we have to oppose it?[B][C]")
Append("Yukina:[E|Neutral] Whenever a villain comes sowing death and discord, of course we oppose them.[P] But there just aren't enough hours in the day for one person to take on every problem.[B][C]")
Append("Yukina:[E|Neutral] Being the elder of this temple means I have more responsibilities than most, but even then, I am not personally organizing a campaign to end hunger.[P] I have hundreds of others to help me.[P] I give orders, make recommendations, but we do it together.[B][C]")
Append("Yukina:[E|Neutral] To be happy is to be with people who love you, to have what you want but to want what you have.[B][C]")
Append("Sanya:[E|Neutral] So you know much of the world's problems, and you just don't let them bother you?[B][C]")
Append("Yukina:[E|Neutral] Your mind is a part of you.[P] You can control it, if you practice.[P] You can simply tell yourself not to feel pain, sadness, anger.[P] There are times for these emotions, but if you master them, you can hear the warnings they give you and choose to be happy anyway.[B][C]")
Append("Yukina:[E|Neutral] This mastery takes work.[P] Decades of it.[P] And some people are always anxious, depressed.[P] But they can lessen it through this mastery.[B][C]")
Append("Yukina:[E|Neutral] There are mighty kings with hoards of gold, thousands of soldiers, and the most comfortable pillows you can imagine.[P] And they are miserable.[B][C]")
Append("Yukina:[E|Neutral] But with my tea, and my friends, and a good book, I am content.[B][C]")
Append("Yukina:[E|Neutral] Oh and a good roll in the hay every now and them.[B][C]")
Append("Izuna:[E|Ugh] Elder![B][C]")
Append("Yukina:[E|Neutral] I was quite a looker in my day, you know![P] Young people aren't the only ones who enjoy sex, children![B][C]")
Append("Izuna:[E|Ugh] My first test of emotional mastery shall be to not imagine that![P] Nope![P] Nope![P] Nope![B][C]")