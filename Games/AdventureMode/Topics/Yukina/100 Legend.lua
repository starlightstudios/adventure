-- |[ =================================== Yukina: The Legend =================================== ]|
--Yukina describes the legend of the hero.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Other:
local bIsEmpressPresent = fnIsCharacterPresent("Empress")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Yukina")

-- |[Dialogue]|
--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Nekomatas.
    WD_SetProperty("Unlock Topic", "Nekomata", 1)
	
    --Dialogue.
    Append("Sanya:[E|Neutral] So a lot of people have told me about the legend.[P] Can you tell me a bit more about the specifics of it?[B][C]")
    Append("Yukina:[E|Neutral] You have not heard the precise text of it?[B][C]")
    Append("Sanya:[E|Neutral] We've been a little busy.[B][C]")
    Append("Izuna:[E|Cry] I'm sorry.[P] I know it's important but time is of the essence.[P] I should have taken more time to explain it to you.[B][C]")
    Append("Yukina:[E|Neutral] It's all right, children.[P] Besides, Sanya wants to review the evidence for herself, and not merely take it on faith.[B][C]")
    Append("Yukina:[E|Neutral] That instinct will serve her very well in the future.[B][C]")
    Append("Yukina:[E|Neutral] Now, the problem here, miss Sanya, is that there is not one specific legend.[P] There are several versions.[B][C]")
    Append("Yukina:[E|Neutral] It has been seven-hundred years since the legend was born, and written copies of the original have been lost.[P] We have transcriptions.[B][C]")
    if(bIsEmpressPresent == true) then
        Append("Yukina:[E|Neutral] Chime in whenever you like, you big blue oaf.[B][C]")
        Append("Empress:[E|Neutral] I intend to.[B][C]")
        Append("Sanya:[E|Neutral] Is there a reason why you're not just telling me the original?[B][C]")
        Append("Empress:[E|Neutral] Yes.[P] Please, continue, Yukina.[B][C]")
        Append("Yukina:[E|Neutral] As I was saying.[P] Despite the gap in time, there are physical transcriptions that have survived various eras.[P] We have some in our library and produce new ones to make sure old documents are not lost.[B][C]")
    end
    Append("Sanya:[E|Neutral] You're pretty lucky to have written copies.[B][C]")
    Append("Yukina:[E|Neutral] Indeed![P] The Arulente empire prized education greatly.[P] Literacy was their primary accomplishment.[P] Most other empires cannot say the same.[B][C]")
    Append("Yukina:[E|Neutral] Now, there is a diversion among the written copies.[P] There was a fire, and the originals lost, but the kitsunes wrote what they remembered of the original.[B][C]")
    Append("Yukina:[E|Neutral] The others, the nekomata, wrote their copy.[P] It differs from ours.[B][C]")
    Append("Sanya:[E|Neutral] How does it differ?[B][C]")
    Append("Yukina:[E|Neutral] So, first, the kitsune version.[P] A great hero with power unmatched emerges to right the wrongs wrought by the foulest night, Perfect Hatred.[B][C]")
    Append("Yukina:[E|Neutral] In the story, she travels the glacier, bestowing boons on those she meets.[P] She vanquishes evil wherever she finds it.[P] Many people gather to her side.[B][C]")
    Append("Yukina:[E|Neutral] But before she can fight the great evil and vanquish it for good, she is betrayed by someone close to her.[P] She is poisoned and falls to a deep sleep.[B][C]")
    Append("Yukina:[E|Neutral] While she sleeps, the one who betrayed her stands vigil over her, wracked by guilt.[P] The great hero then perishes.[B][C]")
    Append("Sanya:[E|Surprised] WOAH HEY HOLD ON HERE![B][C]")
    if(bIsEmpressPresent == true) then
        Append("Empress:[E|Neutral] Calm down, child.[P] There is more to the story.[B][C]")
        Append("Sanya:[E|Neutral] Dude, getting freaked out about headlines is like most of Earth's culture.[P] Nobody reads the article.[B][C]")
    else
        Append("Izuna:[E|Explain] Wait for the next part![B][C]")
    end
    Append("Yukina:[E|Neutral] But then, when she has died, the betrayer weeps.[P] Her tears contain the hopes of all the people of the world, and her soul returns to her body.[P] She becomes an immortal being of light, and vanquishes Perfect Hatred.[B][C]")
    Append("Yukina:[E|Neutral] After the beast is slain, she returns to human form, for she bears a runestone that allows her to return to any form she has need of.[B][C]")
    Append("Yukina:[E|Neutral] Then she leaves Trafal to seek peace elsewhere, the world safe.[B][C]")
    Append("Sanya:[E|Smirk] You really had me in the first half, holy cow.[B][C]")
    Append("Sanya:[E|Smirk] It has its ups and downs, but what does the Nekomata version sound like?[B][C]")
    Append("Yukina:[E|Neutral] The beginning is the same.[P] A hero named Sanya appears with a runestone, but in that one, she transforms frequently as she journeys.[B][C]")
    Append("Yukina:[E|Neutral] She remains a mystery to the people she meets, gathering to her powerful allies, but keeping them distant.[P] Only a select few journey with her.[B][C]")
    Append("Yukina:[E|Neutral] She barely interacts with the common people of Trafal, fearing they will not understand her.[P] And she is not betrayed, nor does she die.[B][C]")
    Append("Yukina:[E|Neutral] She instead confronts Perfect Hatred in a battle of wits.[P] She outsmarts him, takes his power for her own, and forges a new empire.[B][C]")
    Append("Sanya:[E|Neutral] Yeah that's pretty different.[P] Why?[B][C]")
    Append("Yukina:[E|Neutral] The nekomatas prize different qualities than what we kitsunes do.[P] Change a word here or there, and the meaning of a whole paragraph changes.[P] Their scholars merely interpreted the same source material differently.[B][C]")
    Append("Sanya:[E|Sad] The Sanya in those stories...[P] I wonder if that really is me.[P] Would someone betray me?[P] Can I win a battle of wits with something like that?[B][C]")
    Append("Sanya:[E|Offended] Looking at you on the betrayal, Zeke![B][C]")
    Append("Zeke:[E|Neutral] Bah![B][C]")
    Append("Sanya:[E|Neutral] Yeah, you're right.[P] You wouldn't feel a lick of guilt![B][C]")
    if(bIsEmpressPresent == true) then
        Append("Sanya:[E|Neutral] So Empress.[P] Which is the real one?[P] What's going on here?[B][C]")
        Append("Empress:[E|Neutral] Quite interesting.[P] I had heard the kitsune one before, but not the nekomata one.[P] There are other versions besides it as well, but they are often verbally told.[B][C]")
        Append("Empress:[E|Neutral] The legend is known to the far corners of the former empire and beyond.[P] It's quite interesting to hear how it has changed over time.[B][C]")
        Append("Empress:[E|Neutral] Sanya, as Yukina said, the changes to the legend bear with them the values of the people who are retelling it.[P] Would you like to know the true original?[B][C]")
        Append("Empress:[E|Neutral] That a person named Sanya, bearing a runestone, and possessing of great power, would appear under the Bull's Horn.[P] She will gather to her trusted companions.[B][C]")
        Append("Empress:[E|Neutral] Using her runestone, she can adopt the form of the partirhumans she meets.[P] She will then venture into the prison of the fallen beast.[P] There, it ends.[B][C]")
        Append("Sanya:[E|Happy] I'm liking all of this except the part where you say it ends.[B][C]")
        Append("Sanya:[E|Smirk] Because, like, the easiest thing to predict is that I'm going to walk out of there covered in the thing's blood, possibly holding a doggy bag so I can kick its ass again at home when I'm feeling peckish.[B][C]")
        Append("Yukina:[E|Neutral] Interesting.[P] Why did you not tell this version of the story to me earlier, Empress?[P] Or any of the other kitsunes?[B][C]")
        Append("Empress:[E|Neutral] I did make it quite explicit that the outcome is unknown.[P] Perhaps you forgot that detail, but I have maintained that our fates are uncertain.[B][C]")
        Append("Empress:[E|Neutral] The arrival of the hero is what matters.[P] If I knew for sure the hero would succeed, there would be no need for the prep work we did.[B][C]")
        Append("Yukina:[E|Neutral] Hrmpf.[P] I may not like your attitude but I suppose you are right.[P] Perhaps I did not listen carefully enough.[B][C]")
        Append("Sanya:[E|Neutral] So the original story doesn't mention Zeke, or Izuna, or even you?[B][C]")
        Append("Empress:[E|Neutral] No.[P] Though I will say, there are three companions.[B][C]")
        Append("Empress:[E|Neutral] You see, Sanya, this story was not told to us.[P] It was seen, in images.[P] Those are more open to interpretation.[P] The shadows that follow behind you are indistinct.[B][C]")
        Append("Empress:[E|Neutral] But your red clothes and that rifle on your back?[P] Unmistakable.[P] Your form is very clear even as it changes.[B][C]")
        Append("Sanya:[E|Neutral] Did you see the images yourself?[B][C]")
        Append("Empress:[E|Neutral] Yes.[P] There were four of us who did.[B][C]")
        Append("Yukina:[E|Neutral] Empress.[P] Do not obfuscate any further.[P] Who were the other three who saw it?[P] You have never told this to any of us.[B][C]")
        Append("Empress:[E|Neutral] Does it matter?[B][C]")
        Append("Yukina:[E|Neutral] I have never seen you this candid.[P] Something has changed in you.[P] I don't know what it is, but if you will tell us things you kept hidden, I must hear them.[B][C]")
        Append("Izuna:[E|Jot] Don't worry, elder.[P] I'm taking notes![B][C]")
        Append("Zeke:[E|Neutral] Bah![B][C]")
        Append("Empress:[E|Neutral] My chief magister, the one who composed the spell that bound Perfect Hatred.[B][C]")
        Append("Empress:[E|Neutral] A former magister who had become a rilmani and pitied us mortals enough to help us, despite it being against their customs.[B][C]")
        Append("Empress:[E|Neutral] And Sir Annolos, my drake apprentice and one of the few people I would consider my martial equal at the time.[B][C]")
        Append("Yukina:[E|Neutral] And what happened to them?[B][C]")
        Append("Empress:[E|Neutral] Sir Annolos was sent on an expedition to gather more information after the prediction.[P] He did not return.[P] None of the drakes did.[B][C]")
        Append("Empress:[E|Neutral] My chief magister died during the battle with Perfect Hatred, and I have no idea what became of the rilmani, unfortunately.[P] For all I know, she still lives.[B][C]")
        Append("Sanya:[E|Sad] This is sounding really heavy.[B][C]")
        Append("Empress:[E|Neutral] The decline of the empire began when its leadership was decapitated before Perfect Hatred even showed his face.[P] Perhaps with my drakes, we could have stopped him without such losses.[B][C]")
        Append("Empress:[E|Neutral] The real story of the prophecy is one of hubris and failure.[B][C]")
        Append("Izuna:[E|Jot] Elder, do you want me to publish this?[B][C]")
        Append("Yukina:[E|Neutral] Eventually, but not right now.[P] I must think some more on it.[P] For one thing, we should ascertain if it's true or not.[B][C]")
        Append("Izuna:[E|Neutral] Never hurts to verify, but how?[B][C]")
        Append("Yukina:[E|Neutral] That is what I must think about.[B][C]")
        Append("Empress:[E|Neutral] Believe me or don't.[P] That's all there is to it.[P] Sanya, your victory is not assured, but you deserve to know what we do.[B][C]")
        Append("Empress:[E|Neutral] Don't make the same mistake I did.[P] Do not underestimate this challenge before you.[P] It will be a trial unlike any you have even countenanced.[B][C]")
        Append("Sanya:[E|Neutral] Yikes.[P] Okay.[P] I get it.[B][C]")
        Append("Sanya:[E|Angry] Hey wait a minute![P] How did you know my name's Sanya if it was all pictures![B][C]")
        Append("Empress:[E|Neutral] ...[P] The runestone is a rilmani word.[P] That rune is literally your name.[B][C]")
        Append("Sanya:[E|Angry] I - [B][C]")
        Append("Sanya:[E|Sad] Damn it I thought I caught you in a contradiction.[P] Oh well.[B][C]")
        Append("Sanya:[E|Neutral] I'll get you next time.[B][C]")
    
    else
        Append("Izuna:[E|Neutral] Goat jokes aside, there's a lot of divergence in the story.[P] If only we had the originals...[B][C]")
        Append("Yukina:[E|Neutral] Would they matter?[P] Do as you will, Sanya.[P] You write your destiny, not some long-dead archivists.[P] Remember that.[B][C]")
    end
end
