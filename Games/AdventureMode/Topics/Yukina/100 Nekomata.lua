-- |[ ==================================== Yukina: Nekomata ==================================== ]|
--Yukina explains the nekomatas.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Yukina")

-- |[Dialogue]|
--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
    --Dialogue.
    Append("Sanya:[E|Neutral] Can you tell me a bit about the nekomatas?[B][C]")
    Append("Yukina:[E|Neutral] Yes, but I must warn you.[P] I, and every kitsune, is biased against them.[P] They are our oldest enemy.[B][C]")
    Append("Sanya:[E|Offended] I'll kick their asses for you![B][C]")
    Append("Yukina:[E|Neutral] Please don't.[B][C]")
    Append("Sanya:[E|Sad] Man, why are you such a buzz kill...[B][C]")
    Append("Yukina:[E|Neutral] While they are our opposites, they still do not cause true, grievous harm to others.[P] Let me explain.[B][C]")
    Append("Yukina:[E|Neutral] We kitsunes believe that the Fox Goddess teaches us to use our cleverness for the benefit of everyone.[B][C]")
    Append("Yukina:[E|Neutral] Our warriors protect the weak.[P] Our mikos heal the sick.[P] Our scholars research solutions to the world's problems.[B][C]")
    Append("Yukina:[E|Neutral] Young Izuna had a most clever idea.[P] She publishes pamphlets to spread knowledge to people far and wide.[P] We do this because it is right, and we are kind.[P] We strive to be kind.[B][C]")
    Append("Yukina:[E|Neutral] The nekomatas likewise believe in the Fox Goddess.[P] They study all her teachings, follow all her precepts.[P] But the Fox Goddess does not explicitly state that cleverness is to be used for the good of all.[B][C]")
    Append("Yukina:[E|Neutral] They believe that the quick-witted are superior to the dull.[P] They must guide them, watch over them, but always be superior to them.[B][C]")
    Append("Yukina:[E|Neutral] And thus they are selfish, for the comfort of a nekomata is worth the discomfort of a hundred others.[B][C]")
    Append("Yukina:[E|Neutral] The nekomata seek to occupy a ruling caste above other people, molding them as they see fit.[P] And the weak are culled for they are of no use to them.[B][C]")
    Append("Sanya:[E|Offended] Fuckin' fascists![B][C]")
    Append("Izuna:[E|Ugh] A what now?[B][C]")
    Append("Sanya:[E|Sad] People on Earth who think they're superior to others.[P] They sound a lot like these nekomatas.[B][C]")
    Append("Sanya:[E|Neutral] But surely not every nekomata is like that?[P] Can you really tell me that every kitsune is a good person?[P] Can you really make a broad generalization about a group of people?[B][C]")
    Append("Yukina:[E|Neutral] Of course not.[P] There are kitsunes who have strayed from our doctrines.[P] They are expulsed.[B][C]")
    Append("Yukina:[E|Neutral] But to become a kitsune or nekomata is something people embark on.[P] It involves study.[P] You must be dedicated to the cause.[B][C]")
    Append("Sanya:[E|Sad] So it's not an accident, they choose to make that part of their identity.[B][C]")
    Append("Sanya:[E|Offended] But you don't want me to beat them up?[B][C]")
    Append("Yukina:[E|Neutral] No, because despite all their scheming, they have never accomplished the ends they claim to seek.[P] They're quite pathetic, actually.[B][C]")
    Append("Yukina:[E|Neutral] We kitsunes have a temple here in Trafal.[P] Most major settlements on this continent have a kitsune shrine.[P] Our teachings are heard the world over.[P] The nekomatas?[B][C]")
    Append("Yukina:[E|Neutral] They believe in the primacy of the individual.[P] They fight each other more than they fight us.[P] And...[B][C]")
    Append("Yukina:[E|Neutral] Not all of them may be beyond redemption.[P] To be transformed is simple, to be misled, trivial.[B][C]")
    Append("Yukina:[E|Neutral] Many other kitsunes do not agree with me, but I think there must be some good deep inside at least some of them.[P] Not all, maybe not even most.[P] But some.[B][C]")
    Append("Yukina:[E|Neutral] Besides, judging others by their appearance is cautioned by the kitsunes, and embraced by the nekomatas.[P] You owe it to us to at least hear them out, refute them...[B][C]")
    Append("Yukina:[E|Neutral] And then cave their skulls in.[P] To be kind in the face of adversity is real strength.[P] The nekomatas mistake the two at their peril.[B][C]")
    Append("Sanya:[E|Happy] I think I like the kitsunes, Izuna![B][C]")
    
end
