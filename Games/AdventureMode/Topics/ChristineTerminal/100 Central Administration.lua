-- |[Central Administration]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "ChristineTerminal")

--Dialogue:
Append("Thought: (Housed in the Administrative Complex at the heart of Regulus City, this is where all the important decisions that affect our daily lives are made.)[B][C]")
Append("Thought: (The Administration is staffed by Command Units and a select few Lord Units.[P] Together, they collect and analyze data from the city and steer production and research in accordance with the Cause of Science.)[B][C]")
Append("Thought: (Please note that access to the Administrative Complex is restricted.[P] Even Lord Units require special access permissions to be on its grounds, so be sure to keep your security badge on you at all times.)[B][C]")
Append("Thought: (The computer mainframes that support the Central Administrators are among the most advanced machines in the history of our solar system, and possibly the universe.[P] Every day, new breakthroughs are made.[P] It is due to Central Administration that the Cause of Science is advanced.)[B][C]")

--Topics
WD_SetProperty("Unlock Topic", "Central Administration", 1)
