-- |[They're Called Command Units, Damn It]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "ChristineTerminal")

--Dialogue:
Append("Thought: (Considered to be the pinnacle of synthetic partirhuman technology, the Command Unit serves as the leadership caste of Regulus City.[P] They, and a select few Lord Units, are responsible for the city's security and progress.)[B][C]")
Append("Thought: (The design itself is secret, largely out of security concerns.[P] Command Units are stronger, smarter, faster, and in every way superior to the Golem series, but the expense of their conversion prohibits mass production.)[B][C]")
Append("Thought: (Command Units are frequently nicknamed 'Dolls' out of a misunderstanding of their anatomy.[P] Their jointed frames are similar to the ball joints of conventional toy dolls of Pandemonium tradition.[P] These joints are actually superconducting selective-adhesion modules, but the superficial resemblance gave them the nickname.)[B][C]")
Append("Thought: (Among the Command Units are a small cadre called the Prime Command Units.[P] These Command Units are authorized to make decisions in absence of Central Administration's input, and serve as extensions of Regulus City's will.[P] Their word is absolute.)[B][C]")
Append("Thought: (If you are in the presence of a Command Unit, show her the utmost respect and deference.[P] Their jobs are extremely difficult and necessary for the success of Regulus City, and thus you must do all within your power to accommodate them.)[B][C]")

--Topics
WD_SetProperty("Unlock Topic", "Central Administration", 1)
