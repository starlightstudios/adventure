-- |[Killphrases]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "ChristineTerminal")

--Dialogue:
Append("Thought: (The Unit Subjugation Verbal and/or Radio Command, commonly called the 'Killphrase', is a concept important for lord golems to understand.[P] If under any circumstances you need to disable a slave unit, you will need to use their Killphrase.[P] Be aware of them and do not use the Killphrase accidentally, accidental Killphrasing is a level six offense.)[B][C]")
Append("Thought: (On slave units, a synaptic spike is attached to the headplate and positioned such that it reaches into the unit's CPU.[P] When the Killphrase is received audibly or by radio transmission, the spike activates and immediately sets the CPU into system standby until a repair unit can reactivate the unit.[P] This can be used to prevent a unit who is threatening a lord golem from posing a danger.)[B][C]")
Append("Thought: (Despite being in standby, the spike still has a receptor and internal battery.[P] The battery will run down within 30 seconds and render the spike inoperable until the main power line is restored.)[B][C]")
Append("Thought: (While active, if the spike receives the Killphrase a second time, it will pierce the CPU and render it inoperable.[P] The unit will be permanently rendered offline.)[B][C]")
Append("Thought: (As a lord golem, you are not authorized to use a second Killphrase.[P] Only security units putting down a revolt are authorized.[P] Using a second Killphrase without permission is a level two offense.)[B][C]")
Append("Thought: (The code on the headplate is not the actual Killphrase, it is instead a hash lookup.[P] The hash position leads to a lookup table that lord golems are programmed with that specifies the Killphrase.)[B][C]")
Append("Thought: (Remember to sync your lookup table each time you defragment.[P] Repair units may be required to edit the lookup phrase during tune-ups.[P] The lookup table is not to be shared with slave units!)[B][C]")
Append("Thought: (If a slave unit disables their killphrase for any reason other than explicit orders from a Command Unit, that is a level one offense.[P] Only during servicing in a repair bay is this allowed.[P] Report any violations to security immediately.)[B][C]")
