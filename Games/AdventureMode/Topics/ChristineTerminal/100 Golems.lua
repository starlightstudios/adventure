-- |[Anatomy of Golems]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "ChristineTerminal")

--Dialogue:
Append("Thought: (The Golem series of partirhuman consists of several subsets, including Slave Unit, Lord Unit, and Drone Unit.[P] While varying greatly in appearance and capability, they are in fact very similar internally.)[B][C]")
Append("Thought: (Based on now-lost schematics for Steam Droids, the Golem improves upon them in every way.[P] Advances in computers, semiconductor miniaturization, and materials science have produced living machines that can operate in any environment with minimal support for long periods.)[B][C]")
Append("Thought: (Lowest amongst the Golems is the Drone Unit.[P] Clad in a synthetic skin similar to rubber, these units are meant for labour in dangerous conditions.[P] They are also favoured for security units, as their chassis is easily repaired after receiving damage.[P] However, they tend to be very unintelligent.)[B][C]")
Append("Thought: (Next is the Slave Unit.[P] These are the bulk of Regulus City's workers, constituting some 95 percent of the current working population.[P] They perform tasks ranging from manufacture, repair, survey, security, and cleaning to research and development.[P] Despite their name, Slave Units are independent, intelligent, and capable machines.)[B][C]")
Append("Thought: (Atop the hierarchy is the Lord Unit.[P] These Units are primarily used for administrative duties and research, making sure Regulus City continues functioning optimally.[P] Due to the difficulties of their work, only individuals of exceptional intelligence and leadership are selected to become Lord Units.)[B][C]")
Append("Thought: (The Golem models are highly modular and can be modified to become another model with part replacement.[P] It is unlikely that another, more advanced form of partirhuman will be necessary considering the modularity of the form.)[B][C]")
Append("Thought: (Constant scientific advances have improved modern Golems far beyond their first iterations.[P] To take one example, Golem power cores are capable of uninterrupted function for 144 hours without recharging::[P] A major improvement over the original 48 hours!)[B][C]")
Append("Thought: (Despite claims otherwise, the Command Unit is not of the same series of designs as Golems, and their parts are not compatible.[P] Please see the Command Unit page for more information.)[B][C]")

--Topics
WD_SetProperty("Unlock Topic", "Dolls", 1)
