-- |[Anatomy of Steam Droids]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "ChristineTerminal")

--Dialogue:
Append("Thought: (The first synthetically generated partirhuman species, Mechanicum Gynoidium (often called the Steam Droid, somewhat erroneously) was designed and built to overcome the obvious environmental hostility present on Regulus.)[B][C]")
Append("Thought: (Many of the original documents that were involved in the production of the first Steam Droid have been lost due to simple decay or poor record keeping.[P] What is known is that the design was meant to circumvent common issues of mortality::[P] Disease, aging, physical frailty.)[B][C]")
Append("Thought: (The Steam Droid has a primitive power system, very advanced for its time, but totally obsoleted by modern power cores.[P] The Droid stores steam in high-pressure magically sealed tanks, and releases it to perform work.[P] Recharging a Steam Droid consists of refilling its steam supplies.)[B][C]")
Append("Thought: (Naturally, corrosion and leaks are serious problems with the design.[P] Despite its many flaws, the prospect of eternal life was so alluring to the mages of Regulus that the entire Arcane Academy soon converted themselves to Steam Droids.)[B][C]")
Append("Thought: (Further improvements to the design eventually led to the creation of the modern Golem design and its many variants.[P] The Command Unit 'Doll' design is a separate project and unrelated to the Steam Droid production series.)[B][C]")
Append("Thought: (The Steam Droid design is currently obsoleted and no further conversions are undertaken.[P] Older Steam Droids have largely been upgraded to the Golem model when possible.[P] Unfortunately, those who physically incompatible with the upgrade were left behind.)[B][C]")
Append("Thought: (Unable to adjust to life in Regulus City, the remaining Steam Droids rejected the Cause of Science and left.[P] Considering their state of decay, it is unlikely any have survived.)[B][C]")
Append("Thought: (If you sight a Steam Droid unit, it is ideal to report it to security for capture and study.[P] Functioning Steam Droids may have novel engineering improvements to their systems which could serve the Cause of Science.[P] Thank you for your cooperation.)[B][C]")

--Topics
WD_SetProperty("Unlock Topic", "Golems", 1)
WD_SetProperty("Unlock Topic", "Dolls", 1)
