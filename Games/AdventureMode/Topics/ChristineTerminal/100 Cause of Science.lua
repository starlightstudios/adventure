-- |[The Cause of Science]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "ChristineTerminal")

--Dialogue:
Append("Thought: (The Cause of Science has three tenets.[P] The first, and most important, is that the truth is to be held above all other concerns.[P] Ethics, Morals, Politics::[P] All are secondary to Truth.)[B][C]")
Append("Thought: (The second tenet is that those who would pursue the Truth are to be held to a higher standard than those who shun it.[P] Even those who lack the mental capacities to further the Cause can still serve it with their bodies.[P] They are to be rewarded for their service.)[B][C]")
Append("Thought: (Third and final of the tenets is that the Truth must be made available to all who would seek it.[P] Race, color, creed, none shall be denied the Truth.)[B][C]")
Append("Thought: (While we seek to further the Cause of Science, we must further recognize that sometimes we fail.[P] The Cause, however, does not fail.[P] It can only be failed.)[B][C]")
Append("Thought: (One day, in the future, when the great questions have been answered, we of Regulus shall bring the Cause to Pandemonium and usher in a new golden age of rational inquiry.[P] Until then, we remain here, isolated, and studying.)[B][C]")

--Topics
WD_SetProperty("Unlock Topic", "Cause of Science", 1)
WD_SetProperty("Unlock Topic", "Steam Droids", 1)
WD_SetProperty("Unlock Topic", "Golems", 1)
