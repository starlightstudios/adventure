-- |[Breeding Program]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
VM_SetVar("Root/Variables/Global/Christine/iKnowsAboutBreedingProgram", "N", 1.0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "ChristineTerminal")

--Dialogue:
Append("Thought: (Founded in Common Year 3362, the Breeding Program is one of the most successful initiatives undertaken in Regulus City.)[B][C]")
Append("Thought: (While abducting humans from Pandemonium for conversion is still the primary way of producing new workers and administrators for Regulus City, the genetic stock of Pandemonium is not without its flaws.)[B][C]")
Append("Thought: (Golem physical architecture is impervious to genetic disease, but mental acuity is still beyond the understanding of our computer scientists.[P] The only way to make a smarter golem is to convert a smarter human.)[B][C]")
Append("Thought: (Rather than submit the vagaries of chance, or attempt to construct an observation program to monitor the inhabitants of Pandemonium, administrators instead decided to do what ranchers have done for thousands of years, and artificially select livestock for quality.)[B][C]")
Append("Thought: (The breeding program is housed in the habitation domes attached to the Biological Research Laboratory.[P] Humans grow there until the age of 17, at which point they are organized by their aptitudes and converted as appropriate.)[B][C]")
Append("Thought: (Those humans of exceptional qualities are selected to reproduce before conversion.[P] The result has been a marked improvement in the overall efficiency of Units.)[B][C]")
Append("Thought: (Whether this can be attributed to improvements in human nutrition and education or genetic selection is still undergoing research.)[B][C]")
Append("Thought: (Please note that humans in the breeding program are afforded all of the privileges of Slave Units, but not access permissions outside of the habitation domes.)[B][C]")
Append("Thought: (If you see a human who is outside of the domes, verify their security pass.[P] Humans from the breeding program may be issued one-day passes to visit Regulus City for educational purposes, or work assignments as part of their graduation.)[B][C]")
Append("Thought: (Otherwise, treat humans in the breeding program as you would any other Unit.[P] Please do not address them by their secondary designations unless directed to.[P] Remember that they are relatively fragile and slow-witted, so afford them space to move!)[B][C]")

--Topics
WD_SetProperty("Unlock Topic", "Central Administration", 1)
