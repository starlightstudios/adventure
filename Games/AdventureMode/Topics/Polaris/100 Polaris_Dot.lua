-- |[Dot]|
--Topic script.
WD_SetProperty("Activate Topics After Dialogue", "Polaris")

WD_SetProperty("Major Sequence Fast", true)
Append("Mei:[E|Neutral] Where did you meet Dot?[B][C]")
Append("Polaris:[E|Neutral] She made the trip here with me from Yeto![B][C]")
Append("Polaris:[E|Neutral] One of my apprentices had caused a ruckus in a temple outside Yorgasville.[P] She was being manipulated by an ice spirit.[B][C]")
Append("Polaris:[E|Upset] Long story short, she wound up encasing a lot of innocent people in ice.[P] Luckily, one of my other apprentices found out and put a stop to it.[B][C]")
Append("Polaris:[E|Neutral] But when everyone was thawed out, there was this adorable corgi, and nobody claimed to be her owner.[B][C]")
Append("Mei:[E|Happy] So you decided to take care of her?[B][C]")
Append("Polaris:[E|Neutral] More like, she chose me![P] She's been with me since then.[B][C]")
Append("Polaris:[E|Neutral] She's got a sharp mind and can predict a thrown stick's trajectory, and she barks to let me know of any nearby ghosts or monsters.[B][C]")
Append("Mei:[E|Smirk] Is she as powerful a magician as you?[B][C]")
Append("Polaris:[E|Neutral] Of course![P] She has only one spell, but it's a charm spell and nobody can resist it![B][C]")
Append("Polaris:[E|Neutral] And best of all, wild monsters fear corgis.[P] It's an ancient superstition.[B][C]")
Append("Mei:[E|Smirk] Really?[P] Why corgis?[B][C]")
Append("Polaris:[E|Neutral] Nobody knows, but even writing the word 'corgi' on the ground is often enough to keep a monster away.[B][C]")
