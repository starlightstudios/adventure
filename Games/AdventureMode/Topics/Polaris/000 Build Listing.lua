-- |[Build Listing]|
--Builds a list of topics this NPC will respond to, and at what levels. Called at Adventure Mode startup.
local sNPCName = "Polaris"
gsActiveNPCName = sNPCName

--Topic listing.
fnConstructTopicStd(true,  "Polaris_AboutYou",      "About You")
fnConstructTopicStd(true,  "Polaris_Polaris",       "Your Name")
fnConstructTopicStd(true,  "Polaris_YourJob",       "Your Job")
fnConstructTopicStd(true,  "Polaris_House",         "Your House")
fnConstructTopicStd(true,  "Polaris_Petrification", "Petrification")
fnConstructTopicStd(true,  "Polaris_Monstergirls",  "Monstergirls")
fnConstructTopicStd(true,  "Polaris_Dot",           "Dot")
fnConstructTopicStd(false, "Polaris_Demons",        "Demons")
fnConstructTopicStd(false, "Polaris_Apprentices",   "Apprentices")
fnConstructTopicStd(false, "Polaris_Magic",         "Magic")
fnConstructTopicStd(false, "Polaris_Magic Field",   "Breanne's Magic Field")
fnConstructTopicStd(false, "Polaris_Mages Guild",   "Mage's Guild")
fnConstructTopicStd(false, "Polaris_LockedRuins",   "Locked Ruins")
fnConstructTopicStd(false, "Polaris_Runestone",     "Upgrade Runestone")
fnConstructTopicStd(false, "Polaris_Gravemarkers",  "Gravemarkers")
fnConstructTopicStd(false, "Rubberines",            "Rubberines")
