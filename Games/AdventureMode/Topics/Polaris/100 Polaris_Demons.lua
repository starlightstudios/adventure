-- |[About You]|
--Topic script.
WD_SetProperty("Activate Topics After Dialogue", "Polaris")

local bIsFlorentinaPresent  = AL_GetProperty("Is Character Following", "Florentina")
WD_SetProperty("Major Sequence Fast", true)
Append("Mei:[E|Neutral] You said demons would come after you if you turned people to stone?[B][C]")
Append("Polaris:[E|Neutral] Oh, yes.[P] There are groups of demons who are looking for people who enslave others.[B][C]")
Append("Polaris:[E|Neutral] Do you know much about demons?[B][C]")
Append("Mei:[E|Happy] Nothing![B][C]")
Append("Polaris:[E|Upset] I'm starting to feel like I did when my apprentices were around.[P] At least you have an excuse.[B][C]")
Append("Polaris:[E|Neutral] Most demons are radical liberty-lover types.[P] They hate slavery, mind-control, necromancy, all that stuff.[B][C]")
Append("Polaris:[E|Thinking] Not all of them, of course, but all of the major demon clans have an anti-slavery stance.[P] I suppose a demon could try to enslave someone, if they didn't mind being kicked out of their clan and hunted.[B][C]")
Append("Mei:[E|Neutral] Demon clan?[B][C]")
Append("Polaris:[E|Thinking] Hm?[P] Oh, the demons are largely organized into clans based on what their obsessions are.[P] The most well known, the succubi, are in the clan 'Hetaru' and prize lust and love.[B][C]")
Append("Polaris:[E|Thinking] While the demons of knowledge, the tengu, are in the clans 'Obsonox' and 'Light Eyes'.[P] I've met a few, they're usually pretty personable.[B][C]")
Append("Polaris:[E|Upset] While a bloodthirster from clan 'MURDER', always spelled in all capital letters, will just try to kill you if they see you.[B][C]")
Append("Mei:[E|Surprise] They really call their clan 'MURDER'?[B][C]")
Append("Polaris:[E|Upset] They're not known for their intellects.[B][C]")
Append("Mei:[E|Neutral] So, no surprise there.[B][C]")
Append("Polaris:[E|Neutral] Anyway, most demons despise slavery in all forms, and if word gets out you're forming some kind of sinister gallery, you'd have some demonic bounties on your head in short order.[B][C]")
Append("Mei:[E|Neutral] Good to know.[P] Where I'm from, demon is usually a bad word indicating a dangerous monster.[B][C]")
Append("Polaris:[E|Neutral] Most people don't know anything about demons, so they'd think the same thing.[P] Luckily, you have me, a person who has [P]*read at least one book*.[B][C]")

--Topic unlock.
WD_SetProperty("Unlock Topic", "Polaris_Apprentices", 1)
