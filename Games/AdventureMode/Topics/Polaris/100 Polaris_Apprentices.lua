-- |[Apprentices]|
--Topic script.
WD_SetProperty("Activate Topics After Dialogue", "Polaris")

local bIsFlorentinaPresent  = AL_GetProperty("Is Character Following", "Florentina")
WD_SetProperty("Major Sequence Fast", true)
Append("Mei:[E|Neutral] You said something about apprentices?[B][C]")
Append("Polaris:[E|Neutral] Oh, yes![P] My sweet, sweet girls![B][C]")
Append("Polaris:[E|Neutral] I have a couple spare beds in case they decide to visit, but they're all grown up now and having adventures of their own.[B][C]")
Append("Polaris:[E|Neutral] I wonder how they're doing...[P] They probably decided to stay together, but you never know.[P] Dorothy is pretty headstrong and might have struck off on her own.[B][C]")
Append("Mei:[E|Smirk] Did you teach them magic?[B][C]")
Append("Polaris:[E|Upset] Yes, but moreso, I had to teach them common sense.[B][C]")
Append("Polaris:[E|Upset] Like not running off to go fight a powerful monster without telling anyone where you are.[B][C]")
Append("Polaris:[E|Neutral] If you stick around long enough, I'm sure you'll get to meet them.[P] At least I hope they visit.[P] It'd be a shame to leave your master to while away her time in a swamp.[B][C]")
Append("Mei:[E|Neutral] Oh, but I need to find my way home.[P] Can't stick around.[B][C]")
Append("Polaris:[E|Neutral] If you do cross paths with them, just say you're a friend of mine.[P] They'll help you out.[B][C]")

--Topic unlock.
WD_SetProperty("Unlock Topic", "Polaris_Magic", 1)
