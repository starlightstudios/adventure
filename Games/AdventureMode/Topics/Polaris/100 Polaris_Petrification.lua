-- |[About You]|
--Topic script.
WD_SetProperty("Activate Topics After Dialogue", "Polaris")
local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")

local bIsFlorentinaPresent  = AL_GetProperty("Is Character Following", "Florentina")
WD_SetProperty("Major Sequence Fast", true)
Append("Mei:[E|Neutral] I don't know much about petrification.[P] Can you give me the basics?[B][C]")
if(sMeiForm == "Gravemarker") then
    Append("Polaris:[E|Upset] You're literally a living statue and you're asking me that?[B][C]")
    Append("Mei:[E|Smirk] Just because I am one doesn't mean I know anything about the process![B][C]")
    Append("Polaris:[E|Upset] Yeah, I can see that.[P] At least you're the one least likely to need to know public safety about petrification.[B][C]")
    Append("Polaris:[E|Upset] May as well get you caught up, though.[B][C]")
else
    Append("Polaris:[E|Neutral] Nobody taught you in school?[P] Your -[P] oh wait, you're not from -[P] what?[B][C]")
    Append("Polaris:[E|Upset] Are you not from Pandemonium, or is the public education here just so miserable?[B][C]")
    Append("Mei:[E|Smirk] I haven't been here long, but I can tell you I haven't seen anything resembling a school in this forest.[B][C]")
    Append("Polaris:[E|Neutral] Good thing I'm telling you, then![P] This is public safety stuff![P] If you don't know it, you'll probably wind up a statue in someone's collection.[B][C]")
end
Append("Polaris:[E|Neutral] Getting turned into a statue is part of a suite of what we call 'State Changes' in the magical community.[B][C]")
Append("Mei:[E|Neutral] Is that like being turned into a monstergirl?[B][C]")
Append("Polaris:[E|Thinking] No no, monstergirls, called 'Partirhumans', are a different effect.[B][C]")
Append("Polaris:[E|Upset] This is not helped by the fact that some state changes can become partirhumans.[B][C]")
Append("Mei:[E|Smirk] Oh?[B][C]")
Append("Polaris:[E|Thinking] Maybe I should try to keep this simple.[P] Let's say you get turned to stone.[B][C]")
Append("Polaris:[E|Thinking] As long as your soul remains, you can magically reattune your body back to its normal state.[P] In fact, you'll probably do it on your own without needing to be told.[B][C]")
Append("Polaris:[E|Thinking] But, if you choose to accept your new form, then you can stay that way.[P] After a while, the change 'sets in' and becomes permanent, and you become a statue partirhuman.[B][C]")
Append("Mei:[E|Smirk] A living statue?[P] Would I be able to move?[B][C]")
Append("Polaris:[E|Thinking] If you wanted to, though I presume most people who want to be statues would probably also want to stay still.[B][C]")
Append("Polaris:[E|Thinking] There are other kinds of petrification, too.[P] We call them 'hardening' as a general term.[P] Being turned to stone, ice, glass, wax, or even into a resin figurine or plastic doll.[B][C]")
Append("Polaris:[E|Thinking] Some particularly awful people have discovered techniques to manipulate the bodies of people who have been hardened.[B][C]")
Append("Polaris:[E|Upset] In fact, it's a current point of contention if it's possible to brainwash people into wanting to be hardened, so you could make a sick collection of them.[B][C]")
Append("Polaris:[E|Upset] Let me just say that the magical research community has its share of black sheep.[B][C]")
Append("Mei:[E|Offended] Well, even if that thought might be a little interesting in the abstract, I wouldn't want to see it done to anyone.[B][C]")
Append("Mei:[E|Blush] On the other hand...[B][C]")
Append("Polaris:[E|Thinking] If you tried that with someone, you'd probably find yourself on the pointy end of a demon's stick.[B][C]")
Append("Mei:[E|Neutral] Demons?[P] Those are real, too?[B][C]")

--Topic unlock.
WD_SetProperty("Unlock Topic", "Polaris_Demons", 1)
