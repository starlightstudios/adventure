-- |[About You]|
--Topic script.
WD_SetProperty("Activate Topics After Dialogue", "Polaris")

WD_SetProperty("Major Sequence Fast", true)
Append("Mei:[E|Neutral] Polaris, you seemed to be unhappy when I didn't recognize your name.[B][C]")
Append("Polaris:[E|Neutral] Please don't burden yourself about that.[P] It's just a little disheartening.[B][C]")
Append("Mei:[E|Neutral] What is?[B][C]")
Append("Polaris:[E|Neutral] I've saved whole kingdoms, sealed away evil gods -[P] you know, things you'd think would get you some name recognition.[B][C]")
Append("Mei:[E|Sad] Sorry, but I'm not from around here.[B][C]")
Append("Polaris:[E|Neutral] Heh, being anonymous isn't so bad.[P] Don't worry about it.[B][C]")
