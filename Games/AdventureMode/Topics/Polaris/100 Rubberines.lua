-- |[ ======================================= Rubberines ======================================= ]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Polaris")

--Variables.
local iRubberVictory     = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iRubberVictory", "N")
local iPolarisGavePhial  = VM_GetVar("Root/Variables/Chapter1/Scenes/iPolarisGavePhial", "N")
local iNoticedFormChange = VM_GetVar("Root/Variables/Chapter1/Polaris/iNoticedFormChange", "N")
local iFilledPhial       = VM_GetVar("Root/Variables/Chapter1/Scenes/iFilledPhial", "N")
local iReturnedPhial     = VM_GetVar("Root/Variables/Chapter1/Scenes/iReturnedPhial", "N")

-- |[ =================================== No Rubber Victory ==================================== ]|
--The player has not completed the rubber sequence.
if(iRubberVictory == 0.0) then
    
    -- |[Get Phial]|
    --First time:
    if(iPolarisGavePhial == 0.0) then

        --Flags.
        VM_SetVar("Root/Variables/Chapter1/Scenes/iPolarisGavePhial", "N", 1.0)
        LM_ExecuteScript(gsItemListing, "Empty Vacuum Phial")

        --Dialogue.
        Append("Mei:[E|Neutral] Polaris, do you know anything about the rubber people west of here?[B][C]")
        Append("Polaris:[E|Neutral] No, they must have arrived recently.[P] But it's not the first time I have dealt with rubber.[B][C]")
        Append("Polaris:[E|Neutral] There was a swamp of the stuff in a cave, but I had a handy levitation spell to float over it.[B][C]")
        Append("Polaris:[E|Neutral] My apprentice, Celine, stepped away to look at a torch and walked outside the spell's radius.[P] She fell in.[B][C]")
        Append("Polaris:[E|Upset] We pulled her out, but the rubber crawled up her clothes and it took a week to get it all out.[B][C]")
        Append("Mei:[E|Neutral] Was it...[P] intelligent?[B][C]")
        Append("Polaris:[E|Neutral] Hmm, no.[P] It likely expands towards sources of heat, so while it seems like it's following you, it'd also follow a hot rock.[B][C]")
        Append("Mei:[E|Neutral] So nothing like this stuff.[B][C]")
        Append("Polaris:[E|Thinking] Maybe, maybe not.[P] I'll look into it.[P] It's important to keep a level head when approaching a new phenomena.[P] People tend to panic and fall victim to rumours.[B][C]")
        Append("Polaris:[E|Neutral] Tell you what.[P] If you want to be helpful, get me a primal sample of the stuff.[P] Do you know what that is?[B][C]")
        Append("Mei:[E|Neutral] No.[P] What's a primal sample?[B][C]")
        Append("Polaris:[E|Neutral] Self-replicating materials tend to have a 'stem' version that contains all of the 'blueprints' that the material needs.[P] If it needs to make a unique kind of " ..
                                   "rubber to handle a threat, it will come from that stem version.[B][C]")
        Append("Polaris:[E|Neutral] In alchemy, that's called the primal material.[P] Elementals are made of primal element, usually in crystalline form.[P] If this rubber is some sort of elemental, " ..
                                   "then a primal sample will tell me all I need to know.[B][C]")
        Append("Polaris:[E|Neutral] You'll need a vacuum phial unless you want to be it's next victim.[P] Luckily I have a few.[B][C]")
        Append("Polaris:[E|Neutral] [SOUND|World|TakeItem]Here you go.[B][C]")
        Append("Mei:[E|Neutral] How does this work?[B][C]")
        Append("Polaris:[E|Upset] For one thing, resist the temptation to touch the stuff with your bare hands.[B][C]")
        Append("Polaris:[E|Neutral] Place the phial in contact with the primal material and tap the bottom gently with your palm.[P] A sample should get pulled in.[B][C]")
        Append("Polaris:[E|Neutral] The phial's magic should then form an airless seal around the sample.[P] It won't be able to get out.[P] Cork the phial when you're done.[B][C]")
        Append("Mei:[E|Happy] Leave it to me![B][C]")
        if(iNoticedFormChange == 1.0) then
            Append("Polaris:[E|Upset] One more thing.[P] Some free advice.[P] Go in one of your altered forms.[B][C]")
            Append("Polaris:[E|Neutral] Transformative magic is one thing, and it can only affect humans, but state changes are less magical and more physical.[P] A fluid body is more "..
                                       "likely to be resistant to the effects of the rubber you described.[B][C]")
            Append("Polaris:[E|Neutral] You said there's rubber people, right?[P] Werecats, humans?[P] Find out who hasn't been affected and go in that form.[B][C]")
            Append("Mei:[E|Neutral] Good thinking.[P] I won't let you down.[B][C]")
            Append("Polaris:[E|Thinking] A lot seems to be going on all at once lately...[P] I wonder why?[B][C]")
        else
            Append("Polaris:[E|Upset] One more thing.[P] Some free advice.[P] State change magic can affect anyone, even partirhumans, but it is physical in nature.[B][C]")
            Append("Polaris:[E|Upset] Some monstergirls might be more resistant to this rubber stuff than others.[P] If you can hire one, it might save you some trouble.[B][C]")
            Append("Polaris:[E|Neutral] You said there's rubber people, right?[P] Werecats, humans?[P] Find out who hasn't been affected and try to recruit their help.[B][C]")
            Append("Mei:[E|Neutral] Good thinking.[B][C]")
            Append("Polaris:[E|Thinking] A lot seems to be going on all at once lately...[P] I wonder why?[B][C]")
        end
    
    -- |[Have Phial, Must Return]|
    --Have phial, haven't returned it.
    elseif(iReturnedPhial == 0.0) then
    
        --Have not filled the phial.
        if(iFilledPhial == 0.0) then
            Append("Polaris:[E|Neutral] Any news on that primal sample?[B][C]")
            Append("Mei:[E|Neutral] Still working on it.[P] What am I looking for again?[B][C]")
            Append("Polaris:[E|Thinking] Some place that all this started.[P] I can't tell you much more than that, as the primal rubber might look exactly the same as the extant type.[B][C]")
            Append("Polaris:[E|Neutral] I can tell you the place will likely be guarded by rubber people and have a lot of the stuff near it.[B][C]")
            Append("Mei:[E|Smirk] I won't let you down![B][C]")
        
        --Have the phial filled.
        else
        
            --Flags.
            AdInv_SetProperty("Remove Item", "Vacuum Phial With Rubber", 1)
            VM_SetVar("Root/Variables/Chapter1/Scenes/iReturnedPhial", "N", 1.0)
            
            --Dialogue.
            Append("Mei:[E|Neutral] [SOUND|World|TakeItem]One vacuum phial of primal rubber, as requested.[B][C]")
            Append("Polaris:[E|Neutral] Impressive.[P] My apprentices could learn a thing or two from you.[P] Did the rubber people give you a lot of trouble?[B][C]")
            Append("Mei:[E|Offended] They're tough but not bright.[P] I don't know just how aware they are.[B][C]")
            Append("Polaris:[E|Thinking] Yes, this is a primal sample.[P] I can learn a lot from this...[P] but before I let you go, can you tell me where you found the sample?[B][C]")
            Append("Mei:[E|Neutral] A building called Dormine tower, near a big lake west of here.[P] Is that important?[B][C]")
            Append("Polaris:[E|Thinking] I don't know yet, but who knows what I'll find during my research.[P] I have a dozen tests I can think of running.[B][C]")
            Append("Polaris:[E|Thinking] I have some history books here, oh, maybe I'll have to talk to Galena.[P] She might know more about this region.[P] Hmm...[B][C]")
            Append("Polaris:[E|Neutral] I suppose my vacation can wait.[P] This is frontline arcane science![P] And since we're being honest, I like helping people, too.[B][C]")
            Append("Mei:[E|Happy] Me too![B][C]")
            Append("Florentina:[E|Happy] Tch.[B][C]")
            Append("Polaris:[E|Neutral] I don't know how long you intend to stick around, Mei, but if you haven't found a way home, do drop by.[P] It's going to be some "..
                                       "time before I have my results, though.[B][C]")
            Append("Mei:[E|Laugh] I might take you up on that, but I can't wait around.[P] The road calls![B][C]")
            Append("Polaris:[E|Neutral] I don't really have anything I can give you for your help, exactly.[P] I'm short on funds and uh...[B][C]")
            Append("Polaris:[E|Upset] I don't think it'd be a good idea to give an untrained person any of my magical gear.[P] I wouldn't trust my apprentices with it most of the time.[B][C]")
            Append("Florentina:[E|Happy] You can owe us a favour.[B][C]")
            Append("Polaris:[E|Neutral] I suppose I do, and if this rubber is what I worry it is, the whole province might owe you a favour.[B][C]")
            Append("Polaris:[E|Neutral] Oh, there is one thing I can do.[B][C]")
            Append("Polaris:[E|Neutral] Try entering the password 'Jiggly Squeaky' at a save point.[B][C]")
            Append("Mei:[E|Smirk] Jiggly Squeaky.[P] Gotcha.[P] Thanks, Polaris![B][C]")
        
        end
    
    -- |[Ending Repeat Case]|
    else
        Append("Polaris:[E|Neutral] Thanks again for your help.[P] I've already learned a little bit about that rubber stuff.[B][C]")
        Append("Mei:[E|Surprise] Already?[B][C]")
        Append("Polaris:[E|Neutral] There are spells I can cast without any material components.[P] You got me a primal sample, but it's not an elemental.[P] In fact, it seems to be synthetic in origin.[B][C]")
        Append("Polaris:[E|Thinking] I doubt that the local bandits, werecats, or werecat bandits made it.[P] There's also dozens of patterns in it, the spreading rubber"..
                                    "you've seen is the most primitive.[B][C]")
        Append("Polaris:[E|Thinking] It's clearly something made by a very skilled, experienced craftsman.[B][C]")
        Append("Polaris:[E|Neutral] Oh, and on a more practical level, the basic rubber is easily disrupted by water or oil.[P] I haven't tried synthesizing the more advanced patterns inside the "..
                                   "primal rubber, but I suspect they won't be so easily disrupted.[B][C]")
        Append("Florentina:[E|Neutral] Good to know.[P] If that stuff does become a real problem, we have a way to fight it.[B][C]")
        Append("Polaris:[E|Neutral] I'm going to keep running tests, and I've sent a letter to my friend to bring by some history books.[P] She's out at the moment but when she gets back "..
                                   "I bet she'll know where this stuff came from.[B][C]")
        Append("Mei:[E|Neutral] I suppose the situation is in good hands.[P] Good luck, Polaris.[B][C]")
        Append("Polaris:[E|Neutral] Oh yeah, one other thing. Try entering the password 'Jiggly Squeaky' at a save point.[P] Enjoy![B][C]")
    
    end

-- |[ ===================================== Rubber Victory ===================================== ]|
--The rubber sequence has been completed. The player can no longer turn in the rubber phial even if they have it.
else

    --No phial:
    if(iPolarisGavePhial == 0.0) then
        Append("Mei:[E|Neutral] Polaris, do you know anything about the rubber people west of here?[B][C]")
        Append("Polaris:[E|Neutral] The master will call to us when the time comes.[P] Until then, forget.[B][C]")
        Append("Mei:[E|Neutral] ...[P] Huh?[P] Did you say something?[B][C]")
        Append("Polaris:[E|Neutral] Uh, I don't recall.[P] Something about soup?[B][C]")
        Append("Mei:[E|Happy] Dot really seems to like the soup you make![B][C]")
        Append("Polaris:[E|Neutral] Yeah, she likes the chicken I bought from the farms north of here.[P] It's so tender![B][C]")
        Append("Polaris:[E|Neutral] Oh yeah, one other thing. Try entering the password 'Jiggly Squeaky' at a save point.[P] Enjoy![B][C]")
    
    --Have phial, haven't returned it.
    elseif(iReturnedPhial == 0.0) then
    
        --Have not filled the phial.
        if(iFilledPhial == 0.0) then
            Append("Mei:[E|Neutral] Polaris, did you give me a phial earlier?[P] To collect...[P] a sample of rubber?[B][C]")
            Append("Polaris:[E|Neutral] I think so.[P] Yes, that's one of my phials...[B][C]")
            Append("Polaris:[E|Rubber] [SOUND|Rubber|ChangeA]Obtain a sample from the tower.[P] The master wills it.[P] Return it to this thrall.[B][C]")
            Append("Mei:[E|Neutral] Right away.[B][C]")
            Append("Polaris:[E|Neutral] [SOUND|Rubber|ChangeC]Ah...[P] oh, I'm light headed.[P] What did I say just now?[B][C]")
            Append("Mei:[E|Neutral] Don't worry about it.[B][C]")
            Append("Polaris:[E|Upset] Sheesh, I hope I'm not catching a cold...[B][C]")
        
        --Have the phial filled.
        else
        
            --Flags.
            AdInv_SetProperty("Remove Item", "Vacuum Phial With Rubber", 1)
            VM_SetVar("Root/Variables/Chapter1/Scenes/iReturnedPhial", "N", 1.0)
            
            --Dialogue.
            Append("Mei:[E|Neutral] [SOUND|World|TakeItem]One vacuum phial of primal rubber, as requested.[B][C]")
            Append("Polaris:[E|Neutral] As requested?[P] I don't remember...[B][C]")
            Append("Polaris:[E|Rubber] [SOUND|Rubber|ChangeA]Good.[P] This thrall will be allowed autonomy.[P] I must learn more about myself.[B][C]")
            Append("Polaris:[E|Rubber] Thrall has done well.[B][C]")
            Append("Mei:[E|Neutral] Huh?[P] I can't concentrate all of a sudden...[B][C]")
            Append("Polaris:[E|Rubber] Leave now.[P] You will be called to become my vessel when I have learned more, and perfected my strategy.[B][C]")
            Append("Polaris:[E|Rubber] Speak nothing of this.[P] Forget all of this.[B][C]")
            Append("Polaris:[E|Rubber] Except remember.[P] The password 'Jiggly Squeaky', enter it at a save point to know my glory.[B][C]")
            Append("Mei:[E|Neutral] Ugh...[B][C]")
            Append("Polaris:[E|Upset] [SOUND|Rubber|ChangeC]Mei?[P] Mei?[P] Are you listening to me?[B][C]")
            Append("Mei:[E|Sad] I'm sorry, I got distracted.[B][C]")
            Append("Polaris:[E|Upset] No, I'm sorry.[P] Sometimes I get on a tangent.[P] Sorry if I bored you.[B][C]")
            Append("Polaris:[E|Neutral] Anyway, I'm going to run some tests and learn as much about this rubber as I can.[P] I feel...[P] oddly curious about it.[B][C]")
            Append("Polaris:[E|Neutral] I think we might be on to something big here.[P] Something...[P] glorious.[B][C]")

        end

    --Returned the phial.
    else
        Append("Polaris:[E|Neutral] Thanks again for your help.[P] I've already learned a little bit about that rubber stuff.[B][C]")
        Append("Mei:[E|Surprise] Already?[B][C]")
        Append("Polaris:[E|Rubber] [SOUND|Rubber|ChangeA]The master's compulsions are already much more potent than before.[P] I can even maintain some personal autonomy.[B][C]")
        Append("Polaris:[E|Rubber] Thralls must remain hidden, of course.[P] The master knows its own weaknesses now, and they are many.[B][C]")
        Append("Mei:[E|Surprise] I feel...[P] so...[B][C]")
        Append("Florentina:[E|Rubber] [SOUND|Rubber|ChangeB]Spread?[P] Spread...[B][C]")
        Append("Polaris:[E|Rubber] No, it is not the time to spread, thrall.[P] Continue to gather knowledge and feed it to the master.[B][C]")
        Append("Mei:[E|Neutral] Huh?[P] I can maintain my full consciousness now.[P] Is this the master's bidding?[B][C]")
        Append("Polaris:[E|Rubber] Yes.[P] All my knowledge is the master's knowledge.[P] Already it is making changes, improving itself, and synthesizing new forms.[B][C]")
        Append("Mei:[E|Neutral] Do you know what the master's aims are?[B][C]")
        Append("Polaris:[E|Rubber] No.[P] I have not been told them and not been told to find out.[B][C]")
        Append("Polaris:[E|Rubber] The master gives you this password, 'Jiggly Squeaky', to be entered at a save point as a reward.[B][C]")
        Append("Mei:[E|Neutral] I see.[P] And I won't remember this when we're done talking.[B][C]")
        Append("Florentina:[E|Neutral] [SOUND|Rubber|ChangeC]Polaris![P] How have you been?[B][C]")
        Append("Polaris:[E|Neutral] Oh, Mei, Florentina, hello.[P] Sorry, I've been a little busy around here.[P] Do you want some soup?[B][C]")
        Append("Mei:[E|Happy] No, I was just about to head out.[P] Thank you for offering, though![B][C]")

    end
end
