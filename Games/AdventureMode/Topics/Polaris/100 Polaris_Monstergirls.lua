-- |[About You]|
--Topic script.
WD_SetProperty("Activate Topics After Dialogue", "Polaris")

local bIsFlorentinaPresent  = AL_GetProperty("Is Character Following", "Florentina")
local sMeiForm              = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
WD_SetProperty("Major Sequence Fast", true)
Append("Mei:[E|Neutral] You're pretty well read.[P] What can you tell me about monstergirls?[B][C]")
if(sMeiForm ~= "Human") then
    Append("Polaris:[E|Neutral] Should I skip the transformation part, as you're familiar with that?[B][C]")
    Append("Mei:[E|Laugh] You could, but it helps to hear it from an expert![B][C]")
end

Append("Polaris:[E|Thinking] I might not be the best one to teach you about this, as it's not my field of study, but I can tell you the basics.[B][C]")
Append("Polaris:[E|Thinking] 'Partirhumans' are the scientific term for monstergirls.[P] They come in all shapes and sizes, from dragons to slimes to plants to all kinds of beast-people.[B][C]")
Append("Polaris:[E|Thinking] They can't have children the normal way, so they transform humans into monstergirls like themselves.[B][C]")
Append("Polaris:[E|Thinking] Though even that is something of a contested point, it may simply be that a partirhuman simply has a very very low chance of becoming impregnated.[B][C]")
Append("Mei:[E|Blush] You mean it might be possible for an alraune or a slime to have a child?[B][C]")
Append("Polaris:[E|Neutral] I'm saying nobody knows enough about the basic biology of it to say with certainty, but I can say nobody has ever seen it happen.[B][C]")
Append("Mei:[E|Neutral] Has anyone tried?[B][C]")
Append("Polaris:[E|Thinking] ...[B][C]")
Append("Polaris:[E|Thinking] ...[B][C]")
Append("Polaris:[E|Thinking] No, I don't think so.[P] But again, it's not my field of study.[B][C]")
Append("Polaris:[E|Neutral] And if you happen to know a man and a monstergirl who want to try, by all means.[B][C]")
Append("Polaris:[E|Neutral] No guarantees he'd stay a man for long, though.[P] Transformations are often sexual in nature.[B][C]")
Append("Polaris:[E|Thinking] Oh, speaking of, monstergirls are all female.[P] At least, as far as we know, males are turned female when transformed.[B][C]")
Append("Polaris:[E|Thinking] As to why that happens, it's anyone's guess at this point.[P] The best theory I've heard is that all souls are inherently female and are warped male at birth.[B][C]")
Append("Mei:[E|Neutral] Souls are inherently female?[B][C]")
Append("Polaris:[E|Upset] Not exactly something you can test, you see.[B][C]")
Append("Polaris:[E|Thinking] Scientific literature is as often full of falsities and legends as it is actually researched fact.[P] Simply having someone go through all the existing works and check if they're true would be several lifetimes of work.[B][C]")
Append("Mei:[E|Smirk] Ever heard of the scientific method?[B][C]")
Append("Polaris:[E|Neutral] The what now?[B][C]")
Append("Mei:[E|Smirk] We learned it in school where I'm from.[P] You create a hypothesis, devise an experiment to try to falsify the hypothesis, and record the results so it can be reproduced.[B][C]")
Append("Mei:[E|Smirk] You try to control for every possible variable in the experiment, but the real point is to either falsify the hypothesis, or fail to do so.[B][C]")
Append("Polaris:[E|Thinking] Interesting.[P] You carve out reality from negative statements, rather than making wild positive statements.[B][C]")
Append("Mei:[E|Smirk] And be sure to reproduce the results, so other people can verify you didn't just get really unlucky.[B][C]")
Append("Polaris:[E|Thinking] That'd go a long way, but getting the guild to actually adhere to it is something else entirely.[B][C]")
Append("Mei:[E|Neutral] You'll have to convince them.[P] The method has worked very well for us![B][C]")

--Topic unlock.
