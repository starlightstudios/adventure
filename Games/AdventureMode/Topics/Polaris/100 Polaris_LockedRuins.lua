-- |[Locked Ruins]|
--Topic script.

--Variables.
local bIsFlorentinaPresent  = AL_GetProperty("Is Character Following", "Florentina")
local iHasJob_TreasureHunter = VM_GetVar("Root/Variables/Global/Florentina/iHasJob_TreasureHunter", "N")

-- |[Does not have Treasure Hunter]|
if(iHasJob_TreasureHunter == 0.0) then

    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Miss Polaris, the ruins south of here has a locked gate I can't get past.[P] Do you know anything about it?[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Thinking] Locked gate?[P] Oh, I think I know which one you're talking about.[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Thinking] Those old ruins are real deathtraps.[P] The gate probably fell into place, it was open last I saw it.[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Neutral] You could probably pick the locking mechanism to open it.[P] They're not very complex.[B][C]") ]])

    if(bIsFlorentinaPresent == false) then
        fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Polaris") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Pick a lock?[P] Me? I'm not sure I have any idea how.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Me neither, actually.[P] Hm.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Maybe you could ask around at the trading post.[P] There's probably at least one blatant criminal over there.[B][C]") ]])

    else
        VM_SetVar("Root/Variables/Global/Florentina/iHasJob_TreasureHunter", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter1/Polaris/iLockpickQuestState", "N", 2.0)
        fnCutscene([[ Append("Florentina:[E|Neutral] I'd love to, but I don't have any tools for the job.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Hmm, let me see.[P] You can borrow my screwdriver, but for the pick itself...[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Actually, what if I just melted the metal and reshaped it for you?[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] ...[P] There you go, one steel pick.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Well well well, I went from no picking tools to some high-quality picking tools![P] You know what this means, Mei?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] ..........[P] no?") ]])
        fnCutsceneBlocker()

        --Florentina spins.
        local iTicks = 3
        for i = 1, 5, 1 do
            fnCutsceneFace("Florentina", 1, 0)
            fnCutsceneWait(iTicks)
            fnCutsceneBlocker()
            fnCutsceneFace("Florentina", 1, 1)
            fnCutsceneWait(iTicks)
            fnCutsceneBlocker()
            fnCutsceneFace("Florentina", 0, 1)
            fnCutsceneWait(iTicks)
            fnCutsceneBlocker()
            fnCutsceneFace("Florentina", -1, 1)
            fnCutsceneWait(iTicks)
            fnCutsceneBlocker()
            fnCutsceneFace("Florentina", -1, 0)
            fnCutsceneWait(iTicks)
            fnCutsceneBlocker()
            fnCutsceneFace("Florentina", -1, -1)
            fnCutsceneWait(iTicks)
            fnCutsceneBlocker()
            fnCutsceneFace("Florentina", 0, -1)
            fnCutsceneWait(iTicks)
            fnCutsceneBlocker()
            fnCutsceneFace("Florentina", 1, -1)
            fnCutsceneWait(iTicks)
            fnCutsceneBlocker()
        end
        iTicks = 1
        for i = 1, 10, 1 do
            fnCutsceneFace("Florentina", 1, 0)
            fnCutsceneWait(iTicks)
            fnCutsceneBlocker()
            fnCutsceneFace("Florentina", 1, 1)
            fnCutsceneWait(iTicks)
            fnCutsceneBlocker()
            fnCutsceneFace("Florentina", 0, 1)
            fnCutsceneWait(iTicks)
            fnCutsceneBlocker()
            fnCutsceneFace("Florentina", -1, 1)
            fnCutsceneWait(iTicks)
            fnCutsceneBlocker()
            fnCutsceneFace("Florentina", -1, 0)
            fnCutsceneWait(iTicks)
            fnCutsceneBlocker()
            fnCutsceneFace("Florentina", -1, -1)
            fnCutsceneWait(iTicks)
            fnCutsceneBlocker()
            fnCutsceneFace("Florentina", 0, -1)
            fnCutsceneWait(iTicks)
            fnCutsceneBlocker()
            fnCutsceneFace("Florentina", 1, -1)
            fnCutsceneWait(iTicks)
            fnCutsceneBlocker()
            if(i == 5) then
                fnCutscene([[ 
                    AdvCombat_SetProperty("Push Party Member", "Florentina")
                        AdvCombatEntity_SetProperty("Active Job", "TreasureHunter")
                    DL_PopActiveObject()
                ]])
            end
        end
        iTicks = 7
        for i = 1, 3, 1 do
            fnCutsceneFace("Florentina", 1, 0)
            fnCutsceneWait(iTicks)
            fnCutsceneBlocker()
            fnCutsceneFace("Florentina", 1, 1)
            fnCutsceneWait(iTicks)
            fnCutsceneBlocker()
            fnCutsceneFace("Florentina", 0, 1)
            fnCutsceneWait(iTicks)
            fnCutsceneBlocker()
            fnCutsceneFace("Florentina", -1, 1)
            fnCutsceneWait(iTicks)
            fnCutsceneBlocker()
            fnCutsceneFace("Florentina", -1, 0)
            fnCutsceneWait(iTicks)
            fnCutsceneBlocker()
            fnCutsceneFace("Florentina", -1, -1)
            fnCutsceneWait(iTicks)
            fnCutsceneBlocker()
            fnCutsceneFace("Florentina", 0, -1)
            fnCutsceneWait(iTicks)
            fnCutsceneBlocker()
            fnCutsceneFace("Florentina", 1, -1)
            fnCutsceneWait(iTicks)
            fnCutsceneBlocker()
        end
        fnCutsceneFaceTarget("Florentina", "Polaris")
        fnCutsceneWait(25)
        fnCutsceneBlocker()
            
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Polaris", "Neutral") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Time to dust off the ol' treasure hunting skills![P] Let's go break into places![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Laugh] Great![P] Now we can get past the lock![B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] We're just not going to ask where the hat came from?[P] Okay then.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Good luck, you two.[B][C]") ]])
        fnCutscene([[ Append("Narrator: (Florentina has gained the 'Treasure Hunter' class![P] You can change classes at any save point.)") ]])
        fnCutsceneBlocker()
    end

else
    fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Polaris") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ Append("Polaris:[E|Neutral] Good luck, and be careful when poking around in old ruins.[P] Those things can fall apart at the slightest tap.[B][C]") ]])
end
