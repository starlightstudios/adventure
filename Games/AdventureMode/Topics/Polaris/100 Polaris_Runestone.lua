-- |[Runestone Upgrade]|
--Topic script.
WD_SetProperty("Activate Topics After Dialogue", "Polaris")

WD_SetProperty("Major Sequence Fast", true)

--Variables.
local iRunestoneQuest = VM_GetVar("Root/Variables/Chapter1/Polaris/iRunestoneQuest", "N")

--Taking the quest.
if(iRunestoneQuest == 0.0) then
    VM_SetVar("Root/Variables/Chapter1/Polaris/iRunestoneQuest", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter1/Polaris/iPolarisRuneQuestState", "N", 2.0)
    Append("Polaris:[E|Neutral] I've been thinking about that runestone of yours.[P] Could I handle it for a moment?[B][C]")
    Append("Mei:[E|Neutral] Go right ahead.[B][C]")
    Append("Polaris:[E|Thinking] Oh my, this isn't good.[B][C]")
    Append("Polaris:[E|Thinking] The poor thing is covered in rust.[B][C]")
    Append("Mei:[E|Neutral] It is?[B][C]")
    Append("Polaris:[E|Neutral] Not conventional rust, but something like it.[P] I suppose nobody has actually used it for centuries.[B][C]")
    Append("Polaris:[E|Neutral] The symbol isn't supposed to be this color.[P] I think I can help, though.[B][C]")
    Append("Polaris:[E|Neutral] Not as much as I'd like to help, but I'd also like to make sure I don't damage it.[P] I'll need you to get me something, though.[B][C]")
    Append("Mei:[E|Smirk] Sure![P] What do I need to get?[B][C]")
    Append("Polaris:[E|Thinking] I have most of the things I need to make the proper cleaner with me here, but I'm going to need some greaseflower.[B][C]")
    Append("Polaris:[E|Thinking] I was speaking with a farmer a while back -[P] Jean?[P] Jacque? -[P] who lives north of the swamp.[P] I think she knows where some is.[B][C]")
    Append("Mei:[E|Smirk] So all you need me to do is find this greaseflower and bring some back?[B][C]")
    Append("Polaris:[E|Neutral] Yep, I'll get the rest ready for you.[B][C]")
    Append("Mei:[E|Smirk] I'm on the job![B][C]")
    
elseif(iRunestoneQuest == 1.0 or iRunestoneQuest == 2.0 or iRunestoneQuest == 3.0) then
    Append("Polaris:[E|Neutral] There's a farmer north of the swamp.[P] I forgot her name, but she mentioned she knows where some greaseflower grows wild.[B][C]")
    Append("Polaris:[E|Neutral] Get me a bit of greaseflower, and I'll get the rest ready for you.[B][C]")

elseif(iRunestoneQuest == 4.0) then
    VM_SetVar("Root/Variables/Chapter1/Polaris/iRunestoneQuest", "N", 5.0)
    VM_SetVar("Root/Variables/Chapter1/Polaris/iPolarisRuneQuestState", "N", 6.0)
    Append("Polaris:[E|Neutral] Do I smell a bit of greaseflower on you?[B][C]")
    Append("Mei:[E|Smirk] You do![P] Here it is![B][C]")
    Append("Polaris:[E|Neutral] All right![P] I already did the mixing and boiling, you just need to wipe the runestone with the greasy petals.[B][C]")
    Append("Mei:[E|Smirk] Okay then.[B][C]")
    Append("Mei:[E|Surprise] Somehow, they're super sticky![B][C]")
    Append("Polaris:[E|Neutral] The magic -[P] of chemistry![P] My solution makes the greasy petal become so sticky it'll rip some of that rust off.[B][C]")
    Append("Polaris:[E|Upset] That's about all I can do without knowing a lot more about the runestone, though, and I imagine you're not keen on letting me study it.[B][C]")
    Append("Mei:[E|Sad] Sorry, but I need it.[B][C]")
    Append("Polaris:[E|Neutral] No problem, you probably have something really important to do.[P] But, if you can't find a way home, I could help power it up even more.[B][C]")
    Append("Mei:[E|Happy] I'll keep that in mind![P] That's a lot, miss Polaris![B][C]")
    Append("Mei:[E|Neutral] [VOICE|Narrator](Mei's runestone has been upgraded to Mk II!)[B][C]")
    
    --Remove the greaseflower.
    AdInv_SetProperty("Remove Item", "Greaseflower Petals")
    
    -- |[NG+ Check]|
    --If there is already a Mk II runestone, don't upgrade it.
    if(AdInv_GetProperty("Item Count", "Silver Runestone Mk II") > 0) then
        return
    
    --If Mei already equipped it:
    else
    
        --Push Mei.
        AdvCombat_SetProperty("Push Party Member", "Mei")
        
            --Get slot contents.
            local sItemInSlotA = AdvCombatEntity_GetProperty("Equipment In Slot S", "Item A")
            local sItemInSlotB = AdvCombatEntity_GetProperty("Equipment In Slot S", "Item B")
    
            if(sItemInSlotA == "Silver Runestone Mk II") then
                DL_PopActiveObject()
                return
            elseif(sItemInSlotB == "Silver Runestone Mk II") then
                DL_PopActiveObject()
                return
            end
        DL_PopActiveObject()
    end
    
    -- |[Rune Upgrade]|
    --If Mei's runestone is in the inventory, remove it and replace it with the Mk II version.
    if(AdInv_GetProperty("Item Count", "Silver Runestone") > 0) then
        AdInv_SetProperty("Remove Item", "Silver Runestone")
        LM_ExecuteScript(gsItemListing, "Silver Runestone Mk II")
    
    --Otherwise, Mei must have it equipped. Figure out which slot it is in, and replace it.
    else
        --Add the item.
        LM_ExecuteScript(gsItemListing, "Silver Runestone Mk II")
    
        --Swap the new runestone in.
        AdvCombat_SetProperty("Push Party Member", "Mei")
            local sItemInSlotA = AdvCombatEntity_GetProperty("Equipment In Slot S", "Item A")
            local sItemInSlotB = AdvCombatEntity_GetProperty("Equipment In Slot S", "Item B")
    
            if(sItemInSlotA == "Silver Runestone") then
                AdvCombatEntity_SetProperty("Unequip Slot", "Item A")
                AdvCombatEntity_SetProperty("Equip Item To Slot", "Item A", "Silver Runestone Mk II")
    
            elseif(sItemInSlotB == "Silver Runestone") then
                AdvCombatEntity_SetProperty("Unequip Slot", "Item B")
                AdvCombatEntity_SetProperty("Equip Item To Slot", "Item B", "Silver Runestone Mk II")
            
            end
        DL_PopActiveObject()
        
        --Remove the old runestone.
        AdInv_SetProperty("Remove Item", "Silver Runestone")
    end

else
    Append("Polaris:[E|Neutral] I can't do much more for your runestone without spending a lot of time experimenting on it.[B][C]")
    Append("Polaris:[E|Neutral] If you do decide to stick around for longer, I could help out.[P] Otherwise, good luck.[B][C]")
end
