-- |[About You]|
--Topic script.
WD_SetProperty("Activate Topics After Dialogue", "Polaris")

local bIsFlorentinaPresent  = AL_GetProperty("Is Character Following", "Florentina")
WD_SetProperty("Major Sequence Fast", true)
Append("Mei:[E|Neutral] Did you build this house, Miss Polaris?[B][C]")
if(bIsFlorentinaPresent == true) then
    Append("Florentina:[E|Neutral] I didn't hear of anyone hiring labourers for it.[B][C]")
end
Append("Polaris:[E|Neutral] It looks like a lot, but magic helps speed things up.[B][C]")
Append("Polaris:[E|Upset] Some free advice::[P] Lift with magic, assemble with hands.[B][C]")
Append("Mei:[E|Neutral] Okay...?[B][C]")
Append("Polaris:[E|Neutral] If you're using screws and nails to hold something together, magic tends to cause electric sparks, and that causes fires.[B][C]")
Append("Polaris:[E|Neutral] Cement is fine since there's no metal involved, but don't get impatient.[P] Let the cement set naturally, speeding up the process with magic tends to lead to structural faults.[B][C]")
Append("Mei:[E|Neutral] I will...[P] keep that in mind![B][C]")
if(bIsFlorentinaPresent == true) then
    Append("Florentina:[E|Neutral] If you're up for any more building projects, I could use some bricks laid at the trading post.[B][C]")
    Append("Polaris:[E|Neutral] I spent the past month putting up this house, so maybe later.[P] When I'm not sick of it.[B][C]")
    Append("Polaris:[E|Thinking] Hmmm, or I could add a second floor...[P] but what would I need a second floor for?[P] Hmmm...[B][C]")
end

--Topic unlock.
