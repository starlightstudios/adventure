-- |[Swamp Witch]|
--Dialogue script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Nadia")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
    --Variables.
    local iMetPolaris = VM_GetVar("Root/Variables/Chapter1/Polaris/iMetPolaris", "N")
	WD_SetProperty("Major Sequence Fast", true)
	
    --Not met Polaris:
    if(iMetPolaris == 0.0) then
        Append("Mei:[E|Neutral] Nadia, Florentina said you might know something about a swamp witch?[B][C]")
        Append("Nadia: Oh yeah!-[P] Pole -[P] uh, Pol?-[P] Poledra?-[P] Polarizer?-[P] Poletrane?[B][C]")
        Append("Mei:[E|Surprise] Are you having a stroke?[B][C]")
        Append("Nadia: Polecat.-[P] Poleham.-[P] Hamface.-[P] BIG HAM!![B][C]")
        Append("Mei:[E|Neutral] Big ham?[B][C]")
        Append("Nadia: Sorry.-[P] I was trying to remember her name, but it has slipped my mind.-[P] Oh well.[B][C]")
        Append("Nadia: She moved into Starfield Swamp recently.-[P] Just go north a bit, then west past that researcher's cabin.[B][C]")
        Append("Nadia: When you get to the swamp, go west until you hit an old draconic ruin, then north.-[P] You can't miss her, she built a big house.[B][C]")
        Append("Nadia: Say hi to her doggo for me!-[P] She's so cute![B][C]")
        Append("Mei:[E|Happy] Thanks, Nadia![B][C]")
    else
        Append("Mei:[E|Neutral] Nadia, have you met Polaris?[B][C]")
        Append("Nadia: Yeah![P] Polaris![P] That's her name![P] I couldn't remember it for the longest time![B][C]")
        Append("Nadia: I ran into her when she was building her house.[B][C]")
        Append("Mei:[E|Happy] Oh, the house looked done when I was there![B][C]")
        Append("Nadia: I should give her a housewarming gift![B][C]")
        Append("Nadia: Anyway, she's really nice, and I don't think she knows anyone here yet.[P] Tell her to come visit the trading post![B][C]")
    end
end
