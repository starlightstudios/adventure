-- |[Slimes]|
--Asking this NPC about slimes.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Nadia")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--Standard.
	WD_SetProperty("Major Sequence Fast", true)
	
	--If Mei is not a slime...
	if(sMeiForm ~= "Slime") then
		Append("Mei:[E|Neutral] So if I run into a slime in the forest, what should I do?[B][C]")
		Append("Nadia: Well I'll tell you what I'd do.[P] Leg it![P] Slimes aren't very fast.[B][C]")
		Append("Mei:[E|Neutral] What if running isn't an option?[B][C]")
		Append("Nadia: That I'm less sure about.[P] I think some types of slime are poisonous.[P] They're not fast, so a big heavy strike would probably work well.[B][C]")
		Append("Nadia: You could ask the Cap'n if you want to know more, I'm sure he's got into a few scraps himself.[B][C]")
	
	--If Mei is a slime...
	else
		Append("Mei:[E|Neutral] Do you have any advice about slimes?[B][C]")
		Append("Nadia: Always pay them compliments.[P] Your membrane is very shiny, by the way.[B][C]")
		Append("Mei:[E|Neutral] Thanks![P] I think...[B][C]")
		Append("Nadia: Don't mention it![P] I think you're goo-reat![B][C]")
		Append("Mei:[E|Neutral] Sheesh.[B][C]")
	
	end
end
