-- |[Alraunes]|
--Asking this NPC about Alraunes in general.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Nadia")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--If Mei is a human...
	if(sMeiForm ~= "Alraune") then
		Append("Mei:[E|Neutral] So, you're an Alraune?[B][C]")
		Append("Nadia: Yep![P] I have been as long as I can remember![B][C]")
		Append("Mei:[E|Neutral] And you're a guard here?[B][C]")
		Append("Nadia: Oh yeah, Cap'n Blythe is just the nicest human you'll ever meet.[P] He goes to great lengths to hide it, but he's a softy on the inside.[B][C]")
		Append("Nadia: Most humans aren't very understanding, but here at Trannadar, we have a saying::[P] \"If you don't have money, go away\".[B][C]")
		Append("Mei:[E|Offended] That isn't a nice thing to say.[B][C]")
		Append("Nadia: Nice, no, but it sure isn't prejudiced.[P] Well, not if you have money.[B][C]")
	
	--If Mei is an Alraune
	else
		Append("Mei:[E|Neutral] Have you been a leaf-sister for very long?[B][C]")
		Append("Nadia: Hm, a few years now.[P] I don't remember what came before.[B][C]")
		Append("Mei:[E|Neutral] Yeah, they gave me the same offer.[P] I turned it down.[B][C]")
		Append("Nadia: To each her own.[B][C]")
		Append("Mei:[E|Sad] Aren't you curious?[P] Don't you wonder who you used to be?[B][C]")
		Append("Nadia: There's really two ways it could have gone.[P] Either I was something I'd rather leave behind, or I led a boring life and it's no great loss.[B][C]")
		Append("Mei:[E|Neutral] I take it you've thought about it a lot.[B][C]")
		Append("Nadia: Sometimes, when I'm on my own and the little ones don't feel like talking, I get to thinking.[P] Then, they start talking again.[B][C]")
		Append("Mei:[E|Happy] They're real chatterboxes aren't they?[B][C]")
	end
end
