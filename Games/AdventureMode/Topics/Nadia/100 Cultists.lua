-- |[Cultists]|
--Asking this NPC about the spooky cultists in the mansion.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Nadia")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	WD_SetProperty("Major Sequence Fast", true)
	Append("Mei:[E|Neutral] You're a guard, right?[P] Did you know about the cultists in the big mansion near here?[B][C]")
	Append("Nadia: That rotting old dump has cultists in it?[B][C]")
	Append("Mei:[E|Neutral] Yes.[P] You didn't know?[B][C]")
	Append("Nadia: I steer clear of that place.[P] Gives me the willies just looking at it.[B][C]")
	Append("Nadia: You better tell Cap'n Blythe about those cultists, they might have something to do with the strange happenings lately.[B][C]")
end
