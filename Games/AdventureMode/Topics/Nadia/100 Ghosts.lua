-- |[Ghosts]|
--Dialogue script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Nadia")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--If Mei is a non-ghost...
	if(sMeiForm ~= "Ghost") then
		Append("Mei:[E|Neutral] Nadia, do you know anything about the mansion way north of here?[B][C]")
		Append("Nadia: Oh, geez, no![P] That place creeps me out![B][C]")
		Append("Nadia: I keep telling everyone it's haunted![P] Nobody believes me![B][C]")
		Append("Mei:[E|Smirk] I've been inside it.[P] It's haunted.[P] Ghosts of long-dead maids stalk the halls...[B][C]")
		Append("Nadia: Maids?[P] G-[P]g-[P]g-[P]ghost maids?[B][C]")
		Append("Mei:[E|Neutral] Oh, I'm sorry if I scared you...[B][C]")
		Append("Nadia: That is the cutest thing ever![P] Oh, now I want to meet one![B][C]")
		Append("Nadia: But then I'd get scared solid...[B][C]")
		Append("Mei:[E|Smirk] Heh...[B][C]")
	
	--If Mei is a ghost:
	else
		Append("Mei:[E|Neutral] Nadia, do you know anything about the ghosts in the mansion north of here?[B][C]")
		Append("Nadia: Ohmygosh your outfit is soooooooo cute, Mei![B][C]")
		Append("Nadia: Do you think you have a ghostly maid getup that'd fit me?[P] I know I'm a little small...[B][C]")
		Append("Mei:[E|Neutral] Uh, I don't think that's how it works...[B][C]")
		Append("Nadia: Yeah.[P] I wouldn't want to spook myself, but I just love all the little frills and buttons and the big bow -[P] so cute![B][C]")
		Append("Mei:[E|Smirk] Thanks?[B][C]")
		Append("Nadia: What's the term for scary-cute?[P] S'cut?[P] Terror-chic?[P] Boo-tiful?[P] Spookute?[P] Frightsionable?[P] Adhorrorible?[B][C]")
		Append("Mei:[E|Neutral] Why do I bring this upon myself?[B][C]")
	end
end
