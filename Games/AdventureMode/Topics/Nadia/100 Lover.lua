-- |[Lover]|
--Dialogue script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Nadia")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Variables.
	local iMeiLovesAdina = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiLovesAdina", "N")
	
	--Standard.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Dialogue.
	Append("Mei:[E|Blush] Nadia...[P] about the other night...[B][C]")
	Append("Nadia: Oh.[P] That.[B][C]")
	Append("Mei:[E|Blush] I was -[P] I was under the influence of the werecat curse, you see, and - [P][CLEAR]")
	Append("Nadia: Don't try to walk this back.[B][C]")
	Append("Nadia: I loved it.[B][C]")
	Append("Mei:[E|Blush] Oh.[P] Well, that's good.[B][C]")
	Append("Mei:[E|Blush] I would never - [P][CLEAR]")
	Append("Nadia: I know.[P] That you would even try to apologize is proof enough.[B][C]")
	Append("Nadia: It seems you got lucky with me -[P] in more ways than one![P] Ha ha![B][C]")
	
	--Mei does not love Adina (or hasn't met her):
	if(iMeiLovesAdina == 0.0) then
		Append("Mei:[E|Blush] So...[P] are you available?[B][C]")
		Append("Nadia: Right this second, no.[P] But, if you want to be an item...[B][C]")
		Append("Nadia: I have to think about it.[P] I've never been in a real relationship before.[P] I don't know if I'm ready.[B][C]")
		Append("Mei:[E|Neutral] That's okay.[P] I don't know if I'll be able to stay on Pandemonium for long.[P] But, if you're willing, I'd have a reason to come back.[B][C]")
		Append("Nadia: Or, I could come visit you on Earth![P] Do they have Alraunes there?[B][C]")
		Append("Mei:[E|Neutral] Ah, no.[B][C]")
		Append("Nadia: Cool![P] I'd be a celebrity![P] Maybe I could go on stage with my jokes?[P] Or do you think they're too -[P] corny?[P] Ah ha ha![B][C]")
		Append("Mei:[E|Happy] (I don't think Earth could handle Nadia...)[B][C]")
	
	--Mei fell in love with Adina.
	else
		Append("Mei:[E|Blush] Nadia, I didn't mean to lead you on.[P] My heart already belongs to another.[B][C]")
		Append("Nadia: Oh, really?[P] Aww, that's too bad.[P] I really liked you![B][C]")
		Append("Mei:[E|Blush] Not that we can't have fun, but I would need Mistress Adina's permission first.[B][C]")
		Append("Nadia: Adina?[P] The Alraune who is farming the salt flats?[B][C]")
		Append("Nadia: Good for you, Mei![P] She's really nice![P] I visit on my patrols and she always has some mulch cookies for me![B][C]")
		Append("Mei:[E|Blush] Oh, Nadia, you should let her enthrall you.[P] We could be mindless slaves together...[B][C]")
		Append("Nadia: Wha?[B][C]")
		Append("Mei:[E|Neutral] ...[P] Nothing.[B][C]")
		Append("Nadia: That sounds kinda nice...[B][C]")
		Append("Nadia: But I have a job and junk.[P] I can't let Cap'n Blythe down...[B][C]")
		Append("Mei:[E|Neutral] There is so much work to do on the salt flats.[P] If you change your mind, I have no doubt she'd love to have you.[B][C]")
		Append("Nadia: You're really sweet in an odd kind of way, Mei.[P] I like that![B][C]")
		Append("Mei:[E|Blush] I'm just a humble thrall.[B][C]")
		Append("Nadia: And we can thrall-ways count on you to cheer us up![P] Ha Ha![B][C]")
		Append("Mei:[E|Neutral] (When she's enslaved, she won't tell any more jokes.[P] Or will she..?)[B][C]")
	
	end
end
