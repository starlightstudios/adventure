-- |[Werecats]|
--Dialogue script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Nadia")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local iMeiHasDoneNadia = VM_GetVar("Root/Variables/Chapter1/ScenesWerecat/iMeiHasDoneNadia", "N")
	
	--If Mei is a non-werecat...
	if(sMeiForm ~= "Werecat") then
		Append("Mei:[E|Neutral] Nadia, do you have any advice about the werecats?[B][C]")
		Append("Nadia: They're a bunch of big pussies.[B][C]")
		Append("Mei:[E|Neutral] ..![B][C]")
		Append("Nadia: Ha ha![P] It's true, though.[P] I bet you could hit them with some catnip and stone them out of their minds.[B][C]")
		Append("Mei:[E|Neutral] I meant for fighting them...[B][C]")
		Append("Nadia: Oh...[P] Well, quick strikes might help.[P] They're really fast, so you'd have to beat them at their own game.[B][C]")
		Append("Nadia: I bet the Cap'n can help you there.[P] He's tussled with a few.[B][C]")
	
	--If Mei is a werecat but didn't sex Nadia:
	elseif(iMeiHasDoneNadia == 0.0) then
		Append("Mei:[E|Neutral] Nadia, do you have any advice about fighting werecats?[P] It -[P] didn't go so well last I tried it.[B][C]")
		Append("Nadia: Well, you cats are really quick in short bursts.[P] It might be a good idea to try to beat them at their own game.[B][C]")
		Append("Mei:[E|Neutral] They're going to be so much more experienced on the hunt than I am...[B][C]")
		Append("Nadia: That's why you need to use your noodle![P] Pick the right attacks and you'll win for sure![B][C]")
		Append("Nadia: Since you use a sword, you should look for Fencer's Friend books.[P] I think there's one in the researcher's cabin north of here![B][C]")
		Append("Mei:[E|Smirk] Oh, okay.[P] Thanks, Nadia![B][C]")
	
	--Mei is a werecat and made love to Nadia:
	else
	
		Append("Mei:[E|Neutral] Nadia, do you have any advice about -[P][EMOTION|Mei|Blush] oooohhhpuurrrr....[B][C]")
		Append("Nadia: Just scratch them behind the ear and they're yours...[B][C]")
		Append("Mei:[E|Blush] Uuunnnhhhh...[B][C]")
		Append("Mei:[E|Blush] I'd tell you to stop but it's sooooo gooodpuurrr...[B][C]")
		Append("Nadia: Yeah, just like that.[B][C]")
		Append("Mei:[E|Blush] You're so good with your fingers...[B][C]")
		Append("Nadia: So are you...[B][C]")
		Append("Mei:[E|Neutral] Can we be professional for a second here?[B][C]")
		Append("Nadia: Purr-[P]fessional?[B][C]")
		Append("Mei:[E|Offended] These claws are real...[B][C]")
		Append("Nadia: Aww.[P] Okay.[B][C]")
		Append("Nadia: I'd say you need to use quick strikes to hit quick targets.[P] That's my advice.[P] There are books with tips all over the place.[B][C]")
		Append("Nadia: I saw someone up at that crazy researcher's cabin earlier today.[P] I think they have a copy of Fencer's Friend you might want to look at.[B][C]")
		Append("Mei:[E|Laugh] Purr...[P] thanks, Nadia![B][C]")
	end
end
