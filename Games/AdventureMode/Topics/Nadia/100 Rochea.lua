-- |[Rochea]|
--Asking Nadia about Rochea.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Nadia")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	WD_SetProperty("Major Sequence Fast", true)
	Append("Mei:[E|Neutral] Do you know of Rochea?[B][C]")
	Append("Nadia: Oh.[P] Her.[P] I know her.[B][C]")
	Append("Nadia: She doesn't like me very much.[B][C]")
	Append("Mei:[E|Neutral] Why not?[B][C]")
	Append("Nadia: Well, most Alraunes don't see much use for humans besides joining new sisters.[P] Not me![B][C]")
	Append("Nadia: Humans are great![P] Well, some of them are.[P] The Cap'n is really nice, and so are the other mercs here.[P] They're not all bad![B][C]")
	Append("Mei:[E|Neutral] So they see you as a traitor?[B][C]")
	Append("Nadia: More like they think I'm misguided.[P] They're really bitter about it, too.[P] The little ones don't mind, I don't know why the Alraunes have to be such jerks about it![B][C]")
end
