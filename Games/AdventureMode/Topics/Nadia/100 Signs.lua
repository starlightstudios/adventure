-- |[Signs]|
--Asking this NPC about the signs throughout Evermoon.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Nadia")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Standard.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Dialogue.
	Append("Mei:[E|Neutral] Did you make all of those signs yourself?[B][C]")
	Append("Nadia: Yep![P] Cap'n made it one of my first jobs once I started working here.[B][C]")
	Append("Nadia: Most of the critters in the forest leave us Alraunes alone.[P] So long as we don't poke in their business, anyway.[B][C]")
	Append("Nadia: That means I get a lot of the jobs outside of the trading post, like putting up signs.[B][C]")
	Append("Mei:[E|Neutral] Did he approve of the jokes?[B][C]")
	Append("Nadia: There were no jokes on those signs.[P] Not one.[B][C]")
	Append("Mei:[E|Neutral] Uhhh...[B][C]")
	Append("Nadia: Hah, gotcha![B][C]")
	Append("Nadia: I don't think he knows, or cares.[P] He's got bigger problems.[B][C]")
end
