-- |[Goodbye]|
--Script that fires when the player says goodbye to end the dialogue. Not all NPCs actually do anything here! This
-- script's existence is also optional.
WD_SetProperty("Major Sequence Fast", true)
local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")

--Mei is an Alraune.
if(sMeiForm == "Alraune") then
	Append("Nadia: Say hi to all the little ones for me, leaf-sister!")

else
	Append("Nadia: Come back soon!")
end
