-- |[Breanne]|
--Asking this NPC about Breanne.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Nadia")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Standard.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Dialogue.
	Append("Mei:[E|Neutral] Could you tell me a bit about Breanne?[P] What's she like?[B][C]")
	Append("Nadia: Well, I don't want to say anything I'm not certain of.[P] I think she's in trouble from down east.[B][C]")
	Append("Nadia: There's always human men hanging around her place.[P] She doesn't like them much, and refuses to talk about them.[B][C]")
	Append("Mei:[E|Neutral] Human men?[B][C]")
	Append("Nadia: Sorry, force of habit.[P] All men are humans.[B][C]")
	Append("Mei:[E|Neutral] That's not what I meant.[B][C]")
	Append("Nadia: Good pun![B][C]")
	Append("Mei:[E|Offended] Huh?[B][C]")
	Append("Nadia: That's not what you men-t![P] Ha ha![B][C]")
	Append("Mei:[E|Neutral] Grugh...[B][C]")
	
end
