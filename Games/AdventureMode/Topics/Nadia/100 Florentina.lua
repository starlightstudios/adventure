-- |[Florentina]|
--Asking this NPC about Florentina.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Nadia")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Variables.
	local bIsFlorentinaInParty = AL_GetProperty("Is Character Following", "Florentina")
	
	--Standard.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Dialogue.
	Append("Mei:[E|Neutral] Can you tell me anything interesting about Florentina?[B][C]")
	
	--If Florentina is not present...
	if(bIsFlorentinaInParty == false) then
		Append("Nadia: Like, gossip?[B][C]")
		Append("Nadia: She's a bit of tree bark on the outside, but she's a darn good merchant and she's always going to help if you're in trouble.[B][C]")
		Append("Nadia: The other Alraunes don't like her much, but I never asked her why.[P] She might be shy about it.[B][C]")
		
	--If Florentina is present...
	else
		Append("Nadia: I'm not gonna say anything while she's standing right there![P] How ya doin', Florry?[B][C]")
		Append("Florentina:[E|Confused] Please don't call me that.[P] That is a terrible nickname.[B][C]")
		Append("Nadia: Sure thing, Florry![B][C]")
		Append("Florentina:[E|Facepalm] ...[P] This is what I have to deal with.[B][C]")
		Append("Mei:[E|Laugh] Ha ha![B][C]")
		Append("Nadia: She's bitter,[P] like a Vernonia leaf,[P] but you can count on her.[P] Just watch your coinpurse.[B][C]")
		Append("Florentina:[E|Confused] Hey![B][C]")
		Append("Florentina:[E|Neutral] Oh, who am I kidding, she's right.[B][C]")
		Append("Mei:[E|Neutral] About the coinpurse thing?[B][C]")
		Append("Florentina:[E|Happy] A little from column A, a little from column B...[B][C]")
	end
end
