-- |[Trading Post]|
--Asking this NPC about Trannadar Trading Post.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Nadia")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Standard.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Topic unlocks.
	WD_SetProperty("Unlock Topic", "Settlements", 1)
	
	--Dialogue.
	Append("Mei:[E|Neutral] Can you tell me a little bit about this place?[B][C]")
    
    --If at the trading post:
    local sRoomName = AL_GetProperty("Name")
    if(sRoomName == "TrannadarTradingPost") then
        Append("Nadia: You're standing in front of Trannadar Trading Post.[P] That's the human name for it, anyway.[B][C]")
        Append("Nadia: It's the biggest human settlement for a month's walk.[B][C]")
        Append("Mei:[E|Neutral] Why here?[B][C]")
        Append("Nadia: Well we're on one of the main roads leading around the mountains to the south, so we get a lot of travellers.[B][C]")
        Append("Nadia: The merchants here give them meals and lodging for free, 'cause the shipping companies cover the bills.[P] There's always new people visiting, of all sorts![B][C]")
    else
        Append("Nadia: This is Breanne's Pit Stop![P] If you're a trader and want the best roasts in the province, this is where you go![B][C]")
        Append("Nadia: Turkey, duck, swan![P] Breanne's got it all![B][C]")
        Append("Mei:[E|Neutral] Swan?[P] You can do that?[B][C]")
        Append("Nadia: I've been told it's very greasy, and you will get a concussion if you're hunting one.[B][C]")
        Append("Nadia: Oh, and Breanne is the sweetest person you'll ever meet, but you probably knew that already.[B][C]")
    end
end
