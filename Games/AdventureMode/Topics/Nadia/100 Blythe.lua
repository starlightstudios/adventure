-- |[Blythe]|
--Asking this NPC about Blythe.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Nadia")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Standard.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Dialogue.
	Append("Mei:[E|Neutral] Mr. Blythe is your boss?[P] How'd you two meet?[B][C]")
	Append("Nadia: Mmm, it's kind of a sad story.[P] Are you sure you want to hear it?[B][C]")
	Append("Mei:[E|Neutral] Not if it upsets you.[B][C]")
	Append("Nadia: No, it's okay.[B][C]")
	Append("Nadia: I wasn't joined around here.[P] My spawning pool was a long way north of here, near Soswitch.[B][C]")
	Append("Nadia: Let me tell you, Soswitch isn't a very nice place.[P] The humans are all greedy and paranoid and angry and...[P][CLEAR]")
	Append("Mei:[E|Surprise] Are you okay?[B][C]")
	Append("Nadia: ...[B][C]")
	Append("Nadia: They hunted us down.[B][C]")
	Append("Nadia: I don't know if it's something we did, but one day a huge crowd of them started combing through the forest and murdering any monstergirl they found.[B][C]")
	Append("Nadia: They found my spawning pool.[P] My leaf-sisters tried to stop them, but they cut them down and burned the place to the ground.[B][C]")
	Append("Nadia: ...[P] I ran.[B][C]")
	Append("Mei:[E|Sad] I'm sorry...[B][C]")
	Append("Nadia: I ran for a long time, days, weeks with little rest.[P] They were like a wave crashing against the beach, just going further and further across the land and washing away everything they touched.[B][C]")
	Append("Nadia: Eventually they petered out and went back home, but I was so far away from my spawning pool and I didn't know how to get back.[B][C]")
	Append("Nadia: I was so angry...[P] I wanted to just kill all the humans, wipe them out like they wiped out my leaf-sisters![B][C]")
	Append("Nadia: But then I met the Cap'n and he was so different.[P] He didn't want to kill anyone, and he wasn't like the humans up north.[B][C]")
	Append("Nadia: It took me a while, but I learned that not all humans are the same.[P] Maybe if I could teach the humans that not all monstergirls are the same, my leaf-sisters won't have died for nothing.[B][C]")
	Append("Mei:[E|Neutral] Sorry to bring it up, Nadia.[P] You've been through a lot.[B][C]")
	Append("Nadia: It still hurts, but it gave me a reason to keep going.[P] You can't let your past hold you back forever.[B][C]")
	Append("Nadia: Besides, the sun still shines and juice still tastes sweet.[P] There's always something to look forward to.[B][C]")
	
end
