-- |[Bees]|
--Asking this NPC about Bee Girls in general.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Nadia")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--If Mei is a non-bee...
	if(sMeiForm ~= "Bee") then
		Append("Mei:[E|Neutral] Any tips if I run into a bee girl out in the forest?[B][C]")
		Append("Nadia: The bees?[P] Yeah, their stings are minorly poisonous.[P] It's nothing serious, but be careful.[B][C]")
		Append("Mei:[E|Neutral] Anything else?[B][C]")
		Append("Nadia: Hm, I think there was someone researching bees at the farm up north.[P] If you're curious you might want to ask around up there.[B][C]")
		Append("Mei:[E|Happy] Thanks![B][C]")
		
	--If Mei is a bee...
	else
		Append("Mei:[E|Neutral] My sisters sure do talk about you a lot.[B][C]")
		Append("Nadia: Really?[P] Cool![P] What do they say?[B][C]")
		Append("Mei:[E|Offended] They think you're really nosy.[P] You like to bother them when they're busy.[B][C]")
		Append("Nadia: H-[P]hey![P] I'm just doing my job![P] I have to keep the bees out of the area around the trading post![B][C]")
		Append("Mei:[E|Neutral] ...[P] Yeah.[P] Sure.[P] Okay.[B][C]")
		Append("Nadia: ...?[B][C]")
		Append("Mei:[E|Neutral] Oh, sorry.[P] My sisters said they didn't know.[P] We won't look for nectar near the post.[B][C]")
		Append("Nadia: Well isn't that sweet as honey of them.[B][C]")
		Append("Mei:[E|Neutral] ...[P] Yes, sure.[P] I'll tell her.[B][C]")
		Append("Nadia: What did they say?[B][C]")
		Append("Mei:[E|Smirk] Oh, nothing.[P] Nothing about your jokes, certainly.[B][C]")
	
	end
end
