-- |[Name]|
--Asking this NPC about their name. A lot of NPCs have this. Sometimes it's asking them what their name is, sometimes
-- it's asking them what their name means.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Nadia")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Dialogue.
	WD_SetProperty("Major Sequence Fast", true)
	Append("Mei:[E|Neutral] So, Nadia...[B][C]")
	Append("Nadia: Hi![B][C]")
	Append("Mei:[E|Neutral] Was that your name before you became an Alraune?[B][C]")
	Append("Nadia: I...[P] I don't think so.[P] I think one of my leaf-sisters said it suited me, so I kept it.[B][C]")
	Append("Nadia: It's a good name![P] Not as cool as Mei, but still pretty cool.[B][C]")
	Append("Mei:[E|Smirk] Hey, there's nothing cool about my name.[B][C]")
	Append("Nadia: You [P]\"Mei\"[P] not think so, but I do![P] Ha ha![B][C]")
	Append("Mei:[E|Neutral] (I guess I walked into that one...)[B][C]")
end
