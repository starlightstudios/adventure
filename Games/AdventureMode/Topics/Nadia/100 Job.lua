-- |[Name]|
--Asking this NPC about their job. A lot of NPCs have this.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Nadia")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common code.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Dialogue.
	Append("Mei:[E|Neutral] What is it that you do here, Nadia?[B][C]")
	Append("Nadia: Pretty much whatever the Cap'n says.[P] Most of the forest critters aren't interested in us Alraunes, so I can go out there without getting jumped.[B][C]")
	Append("Nadia: Cap'n taught me how to fight, too.[P] Just in case.[B][C]")
	Append("Nadia: I must inform you that these hands are deadly weapons.[B][C]")
	Append("Nadia: I'm mostly in charge of foraging up food and ingredients.[P] The little ones just love getting their berries picked.[B][C]")
	Append("Mei:[E|Neutral] But you don't need the job here, right?[P] Couldn't you just go live in the forest like the other Alraunes?[B][C]")
	Append("Nadia: I guess so, but then I wouldn't get to hang around with all my friends.[B][C]")
	Append("Nadia: Like you![B][C]")
	Append("Mei:[E|Smirk] Aww...[B][C]")
end
