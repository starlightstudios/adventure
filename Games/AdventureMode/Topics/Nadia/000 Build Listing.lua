-- |[Build Listing]|
--Builds a list of topics this NPC will respond to, and at what levels. Called at Adventure Mode startup.
local sNPCName = "Nadia"

--Topic listing.
fnConstructTopic("Alraunes",        "Alraunes",         -1, sNPCName, 0)
fnConstructTopic("Bees",            "Bees",             -1, sNPCName, 0)
fnConstructTopic("Blythe",          "Blythe",           -1, sNPCName, 0)
fnConstructTopic("Breanne",         "Breanne",          -1, sNPCName, 0)
fnConstructTopic("Cultists",        "Cultists",         -1, sNPCName, 0)
fnConstructTopic("Florentina",      "Florentina",       -1, sNPCName, 0)
fnConstructTopic("Ghosts",          "Ghosts",           -1, sNPCName, 0)
fnConstructTopic("Job",             "Job",              -1, sNPCName, 0)
fnConstructTopic("Lover",           "Lover",            -1, sNPCName, 0)
fnConstructTopic("Rochea",          "Rochea",           -1, sNPCName, 0)
fnConstructTopic("Name",            "Name",             -1, sNPCName, 0)
fnConstructTopic("Settlements",     "Settlements",      -1, sNPCName, 0)
fnConstructTopic("Signs",           "Signs",            -1, sNPCName, 0)
fnConstructTopic("Slimes",          "Slimes",           -1, sNPCName, 0)
fnConstructTopic("Swamp Witch",     "Swamp Witch",      -1, sNPCName, 0)
fnConstructTopic("TradingPost",     "Trading Post",     -1, sNPCName, 0)
fnConstructTopic("Werecats",        "Werecats",         -1, sNPCName, 0)
