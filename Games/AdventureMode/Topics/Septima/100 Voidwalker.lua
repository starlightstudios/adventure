-- |[Voidwalker]|
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Septima")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	WD_SetProperty("Major Sequence Fast", true)
	Append("Mei:[E|Neutral] Why do you keep referring to me as 'voidwalker'?[B][C]")
	Append("Septima: Because that is one of your names.[B][C]")
	Append("Mei:[E|Happy] Heh.[P][EMOTION|Mei|Neutral] No, I mean, why is it one of my names?[B][C]")
	Append("Septima: You are the first human to cross the divide between Pandemonium and The Still Plane.[P] You have crossed a void even we Rilmani cannot cross.[B][C]")
	Append("Septima: It seems I have not impressed upon you the magnitude of what you have accomplished.[B][C]")
	Append("Septima: Imagine a normal human is a creature standing on the ground, staring at the moon.[P] We Rilmani would be birds.[P] We soar easily overhead, we are above the ground, but to reach the moon would be just as impossible.[B][C]")
	Append("Septima: You are one who has performed what would be impossible to anyone else.[P] You stood on the moon, with the aid of no tools.[P] You walk across the void.[B][C]")
	Append("Mei:[E|Neutral] Not intentionally.[B][C]")
	Append("Septima: Not intentionally?[P] Incorrect.[P] A better way of putting it would be::[P] Not consciously.[B][C]")
end
