-- |[Primary Words]|
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Septima")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	WD_SetProperty("Major Sequence Fast", true)
	Append("Mei:[E|Neutral] The Rilmani language guide says that there are six primary words.[P] My runestone has one of them.[P] Why?[B][C]")
	Append("Septima: Your runestone bears your name.[P] It is part of you.[B][C]")
	Append("Septima: The Rilmani word for bravery is Mei.[B][C]")
	Append("Mei:[E|Neutral] Really?[B][C]")
	Append("Septima: Yes, it is one of several synonyms for the concept.[B][C]")
	Append("Mei:[E|Neutral] But -[P] how am I able to speak to you, then?[P] I thought you were speaking - [P][CLEAR]")
	Append("Septima: Your Rilmani tongue is flawless.[P] You speak it naturally as if you had grown up immersed by it.[P] You, obviously, did not.[B][C]")
	Append("Mei:[E|Surprise] I'm speaking Rilmani right now?[B][C]")
	Append("Septima: Indeed.[P] Your diction is excellent.[B][C]")
	Append("Septima: It is necessary for your purpose that you not struggle with our language, so you do not.[P] Did you not find it odd that you could speak to the inhabitants of Pandemonium?[B][C]")
	Append("Septima: Even if you had been able to, certainly you'd not be able to read their words.[P] Yet you did.[B][C]")
	Append("Mei:[E|Neutral] Ugh, does that mean I didn't even need to find that language guide?[B][C]")
	Append("Septima: Most amusing.[P] If only you had known that you merely needed to translate the symbols to activate the portal.[B][C]")
	Append("Mei:[E|Happy] Could you please smile when you say something like 'most amusing'?[B][C]")
	Append("Septima: To smile is most human.[P] I find that enjoyable, even if it is beyond me.[B][C]")
	Append("Mei:[E|Neutral] So what are the other primary words, then?[B][C]")
	Append("Septima: They are Mei, Sanya, Jeanne, Lotta, Christine, and Talia.[B][C]")
	Append("Mei:[E|Neutral] Those are all names from Earth...[B][C]")
	Append("Septima: There are five other bearers.[P] You are the only one we are aware of, as that was the strand of your destiny.[B][C]")
	Append("Septima: When the time comes, we have no doubt you and the other bearers will unite as one.[P] We will prepare you until that time comes.[B][C]")
end
