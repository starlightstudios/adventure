-- |[Goodbye]|
--Script that fires when the player says goodbye to end the dialogue. Not all NPCs actually do anything here! This
-- script's existence is also optional.
WD_SetProperty("Major Sequence Fast", true)

--Standard.
Append("Septima: Return when you are ready.[P] The other unborn would like to meet you, I expect, and it would encourage them greatly to know you are well.[P] Please, introduce yourself to them.")
