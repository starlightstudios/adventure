-- |[The Still Plane]|
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Septima")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	WD_SetProperty("Major Sequence Fast", true)
	Append("Mei:[E|Neutral] Just what is 'The Still Plane'?[B][C]")
	Append("Septima: How to put this...[B][C]")
	Append("Septima: It would be what you call a dimension, even though it is not, truly.[P] There is one universe, split into five regions that do not,[P] cannot,[P] touch one another.[B][C]")
	Append("Septima: This was done by folding their space, such that they are effectively five dimensions of the same universe.[B][C]")
	Append("Mei:[E|Neutral] Uh, okay...[B][C]")
	Append("Septima: Perhaps it would be easier if you thought of them as separate.[P] I cannot say.[B][C]")
	Append("Septima: The Still Plane is the furthest from the other four.[P] It is the one that contains Earth, Sol, The Milky Way...[B][C]")
	Append("Septima: Everything you have ever perceived, including the physical laws, is local only to The Still Plane.[B][C]")
	Append("Mei:[E|Neutral] So Pandemonium is its own plane?[P] Not even in the same universe?[B][C]")
	Append("Septima: Correct.[P] Nix Nedar is between the dimensional folds.[P] It consumes no space.[B][C]")
	Append("Mei:[E|Neutral] Well I guess that'll do for an answer...[B][C]")
end
