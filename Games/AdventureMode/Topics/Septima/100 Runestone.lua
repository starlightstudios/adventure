-- |[Runestone]|
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Septima")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	WD_SetProperty("Major Sequence Fast", true)
	Append("Mei:[E|Neutral] So what's this runestone?[P] Did you make it?[B][C]")
	Append("Septima: No.[P] The runestone you bear has existed since several seconds before the first Rilmani was created.[P] It is tied to your destiny, not ours.[B][C]")
	Append("Septima: Unfortunately, its true nature eludes us.[P] We do not know the words that will describe it fully.[B][C]")
	Append("Mei:[E|Neutral] But it has your symbol on it![B][C]")
	Append("Septima: You are incorrect, that is not our symbol.[P] That is *your* symbol, one that has been yours longer than I have existed.[B][C]")
	Append("Septima: The runestone is tied to you, indeed, it is part of you.[P] It is an extension of your self.[P] Its true nature is known to you and has always been so, but you may not be capable of understanding it yet.[B][C]")
	Append("Septima: It is part of the duties of the Rilmani to guide you to that understanding, though we will not be able to share it with you.[B][C]")
	Append("Mei:[E|Neutral] Well, it's come in handy before, so I guess I'll hang onto it a little longer...[B][C]")
end
