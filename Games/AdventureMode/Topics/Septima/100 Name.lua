-- |[Name]|
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Septima")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	WD_SetProperty("Major Sequence Fast", true)
	Append("Mei:[E|Neutral] Could you tell me a bit more about yourself?[P] Why are you named Septima?[B][C]")
	Append("Septima: I am the seventh Rilmani of the first hundred created.[P] There were six before me, and their names were in order as mine is.[B][C]")
	Append("Mei:[E|Neutral] But didn't you say you were the oldest?[B][C]")
	Append("Septima: I am.[P] The ones that preceded me no longer live.[B][C]")
	Append("Mei:[E|Neutral] Oh...[P] I'm sorry...[B][C]")
	Append("Septima: They fulfilled the purpose they were created for.[P] They died at the moment they were required to die.[B][C]")
	Append("Septima: But thank you for your concern, nonetheless.[B][C]")
end
