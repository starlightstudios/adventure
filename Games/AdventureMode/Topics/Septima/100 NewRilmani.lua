-- |[Language]|
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Septima")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	WD_SetProperty("Major Sequence Fast", true)
	Append("Mei:[E|Neutral] Do you Rilmani create new ones the same way as the other monster girls do?[B][C]")
	Append("Septima: Yes, we do.[P] Sometimes we perish due to events we cannot control.[P] The fabric of reality must be maintained, so we require replacements.[B][C]")
	Append("Septima: When that happens, we convert humans to our cause.[B][C]")
	Append("Mei:[E|Offended] By force?[B][C]")
	Append("Septima: If necessary, yes.[P] The candidates we select usually assent once they understand the gravity of the situation.[P] Some resist.[P] We convert them anyway.[B][C]")
	Append("Mei:[E|Offended] And you don't see a problem with that?[B][C]")
	Append("Septima: No.[B][C]")
	Append("Mei:[E|Offended] Ugh.[B][C]")
	Append("Septima: The purpose we have is higher than any trivial mortal concern could ever be.[B][C]")
	Append("Septima: When the time is right, you, too, will become Rilmani.[P] This is unavoidable.[B][C]")
	Append("Mei:[E|Offended] ...[B][C]")
	Append("Septima: That time is not now.[P] Your runestone will preserve your self because your form must change for your purpose.[P] Ours is merely one of many you will need.[B][C]")
	Append("Septima: But you [P]*will*[P] become Rilmani.[B][C]")
	Append("Mei:[E|Neutral] No point getting upset about it now, I guess...[B][C]")
end
