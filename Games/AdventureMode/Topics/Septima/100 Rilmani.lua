-- |[Rilmani]|
--Topic script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Septima")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	WD_SetProperty("Major Sequence Fast", true)
	Append("Mei:[E|Neutral] So what exactly are the Rilmani?[P] How are you different from the other partirhumans?[B][C]")
	Append("Septima: We are the unborn.[P] We were created at the moment that reality was rent in five.[P] Our duty is to maintain the fabric of the universe.[B][C]")
	Append("Septima: You, like us, are four-dimensional.[B][C]")
	Append("Mei:[E|Neutral] Me?[B][C]")
	Append("Septima: We Rilmani travel between the manifolds just as a three-dimensional being climbs a hill.[P] With effort, certainly, but no less impossible to perceive for a two-dimensional being.[B][C]")
	Append("Septima: You possess this ability as well, though you cannot perceive it.[B][C]")
	Append("Septima: Have you not noticed that, when you are defeated and in grave danger, you awaken safely near a dimensional low point?[B][C]")
	Append("Mei:[E|Neutral] The campfires...[B][C]")
	Append("Septima: There are many low points in reality.[P] You can feel them, just as you can feel certain things without knowing them directly.[B][C]")
	Append("Septima: You have much in common with us.[P] We are kindred spirits.[B][C]")
	Append("Mei:[E|Neutral] That...[P] explains a lot, but opens up so many more questions...[B][C]")
end
