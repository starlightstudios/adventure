-- |[ ===================================== Build Listing ====================================== ]|
--Builds a list of topics this NPC will respond to, and at what levels.
local sNPCName = "Empress"
gsActiveNPCName = "Empress"

--              ||||| Unlocked At Start |||  Filename      |||  Display Name (Optional)
fnConstructTopicStd(              true, "Sevavi",        "Sevavi")
