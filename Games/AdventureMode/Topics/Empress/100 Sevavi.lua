-- |[ ================================== Empress: The Sevavi =================================== ]|
--Let's hear about those statues.

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Empress")

--Common.
WD_SetProperty("Major Sequence Fast", true)

--Dialogue.
Append("Sanya:[E|Neutral] Can you tell me a bit more about the sevavi?[P] What's this about it being a prison sentence?[B][C]")
Append("Empress:[E|Neutral] It was, in fact, a way to avoid a prison sentence, and get a shorter work duration.[B][C]")
Append("Empress:[E|Neutral] You may or may not know this, but transformation into a partirhuman is one of several different transformative magics that exist.[P] The 'state change', undeath, elemental fusion...[B][C]")
Append("Empress:[E|Neutral] Unlike some of those, state changes are reversible, if done fast enough.[P] They may also afflict a partirhuman.[B][C]")
Append("Izuna:[E|Smirk] That means I could become petrified, but not a sevavi.[B][C]")
Append("Sanya:[E|Blush] As if you could become more of a stone-cold fox.[B][C]")
Append("Izuna:[E|Blush] Ohhh my heart...[B][C]")
Append("Empress:[E|Neutral] State changes can have attendant compulsions put in them.[P] Creating a magic spell with a compulsion is difficult, but not impossible.[B][C]")
Append("Empress:[E|Neutral] A criminal could have their sentence reduced in exchange for working on a project as a statue.[P] We had to keep them loyal, so we put the compulsion in.[B][C]")
Append("Empress:[E|Neutral] Prisons are a waste of resources.[P] We needed people to build roads and buildings, not sit there stewing.[B][C]")
Append("Sanya:[E|Neutral] Did the petrification set in on any of them?[P] Did any of them become sevavi?[B][C]")
Append("Empress:[E|Neutral] No, the spell was constructed specifically to simply fade away rather than do that.[P] Escapees were tracked with a collar embedded with a beacon.[B][C]")
Append("Empress:[E|Neutral] They didn't escape all that often, but sometimes the compulsion simply didn't work.[B][C]")
Append("Empress:[E|Neutral] We removed as many of the compulsions as we could, but the spell used to make the sevavi still had one in it.[P] It was load-bearing, so to speak.[B][C]")
Append("Sanya:[E|Smirk] What are they going to do once we whup Perfect Hatred?[B][C]")
Append("Empress:[E|Neutral] Their task complete, they will be free to do as they like.[P] After seven centuries, I wonder if they have any interest in returning to society.[B][C]")
Append("Empress:[E|Neutral] Perhaps, like me, they will tour the world, see the sights.[P] Perhaps they will take jobs as guards, or become tradeswomen.[P] Who can say?[B][C]")
Append("Empress:[E|Neutral] I'm sure they've given it a great deal of thought.[P] I hope they keep in touch.[B][C]")