-- |[Topic Script]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "PipeNightmare")

Append("Christine:[E|Neutral] What are my pressure goals for each colour of pipe?[B][C]")
Append("565102: The target for the Red pipes is 27psi.[B][C]")
Append("565102: The target for the Blue pipes is 16psi.[B][C]")
Append("565102: The target for the Violet pipes is 39psi.[B][C]")
Append("565102: The target for the Yellow pipes is 25psi.[B][C]")
Append("565102: You can check the pressures in all the pipes at the terminals in this area.[P] You can adjust the pipes at locations where there's a red valve.[B][C]")
Append("Christine:[E|Smirk] Thanks, good to know.[B][C]")
Append("565102: The valves are configured to have specific pressures they can work at.[P] So not every combination is possible.[B][C]")
Append("565102: And...[P] some of the pressures are negative.[P] So pay attention to that.[B][C]")
Append("Christine:[E|Smirk] Well, that'd make...[P] wait.[B][C]")
Append("Christine:[E|Offended] Why are they set for specific pressures?[B][C]")
Append("565102: I didn't build this mess, I just maintain it.[B][C]")
Append("Christine:[E|Neutral] *Sigh*[B][C]")
Append("565102: You can vocalize that sentiment again...[B][C]")
