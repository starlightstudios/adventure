-- |[Topic Script]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
local iRedAValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iRedAValue", "N")
local iRedBValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iRedBValue", "N")
local iRedCValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iRedCValue", "N")
local iRedDValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iRedDValue", "N")
local iBlueAValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iBlueAValue", "N")
local iBlueBValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iBlueBValue", "N")
local iBlueCValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iBlueCValue", "N")
local iBlueDValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iBlueDValue", "N")
local iVioletAValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iVioletAValue", "N")
local iVioletBValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iVioletBValue", "N")
local iVioletCValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iVioletCValue", "N")
local iYellowAValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iYellowAValue", "N")
local iYellowBValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iYellowBValue", "N")
local iYellowCValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iYellowCValue", "N")
local iYellowDValue = VM_GetVar("Root/Variables/Chapter5/Scenes/iYellowDValue", "N")

--Run formulas.
local iRedFinal = iRedAValue + iRedBValue + iRedCValue + iRedDValue
local iBlueFinal = (iBlueAValue + iBlueBValue) - ((iBlueDValue) - (iBlueCValue))
local iVioletFinal = (iVioletAValue) * (iVioletBValue + iVioletCValue)
local iYellowFinal = (iYellowAValue) - ((iYellowBValue) - (iYellowCValue + iYellowDValue))

--Always flag to go back to the topics listing when this is over.
Append("Christine:[E|Neutral] Okay, check the targets. How am I doing?[B][C]")

local iRights = 0
if(iRedFinal == 27.0) then
    iRights = iRights + 1
    Append("565102: The red pipes are at 27psi, so you got those done.[B][C]")
else
    Append("565102: No good, the red pipes need to be at 27psi but are at " .. string.format("%i", iRedFinal) .. ".[B][C]")
end
if(iBlueFinal == 16.0) then
    iRights = iRights + 1
    Append("565102: The blue pipes are at 16psi, good work![B][C]")
else
    Append("565102: Hmm, the blues need to be at 16psi but are at " .. string.format("%i", iBlueFinal) .. ".[B][C]")
end
if(iVioletFinal == 39.0) then
    iRights = iRights + 1
    Append("565102: Seems the violet pipes are at 39psi as expected.[B][C]")
else
    Append("565102: Oh, this is bad. Violet is at " .. string.format("%i", iVioletFinal) .. " and should be 39.[B][C]")
end
if(iYellowFinal == 25.0) then
    iRights = iRights + 1
    Append("565102: And lastly, yellow is at 25psi. So good on you.[B][C]")
else
    Append("565102: Better take a look at the yellow pipes. I'm reading " .. string.format("%i", iYellowFinal) .. " but expecting 25.[B][C]")
end
    
--Mission complete!
if(iRights == 4) then
    VM_SetVar("Root/Variables/Chapter5/Scenes/iPipeGolemState", "N", 3.0)
    Append("Christine:[E|Smirk] So that means the job is done, right?[B][C]")
    Append("565102: Hold on...[P] let me check the power transfers...[B][C]")
    Append("565102: Wow, look at that![P] Cruising right back towards nominal levels![P] You did it![B][C]")
    Append("565102: Thank you thank you thank you soooo much, Lord Unit![P] I don't know what I would have done without you![B][C]")
    Append("Christine:[E|Smirk] Just doing my job.[B][C]")
    Append("Christine:[E|Neutral] It's a good thing this was a routine repair job.[P] If I didn't know better, I'd have thought it was a puzzle.[B][C]")
    Append("565102: It isn't.[B][C]")
    Append("Christine:[E|Smirk] Good.[B][C]")
    Append("Christine:[E|Angry] Because I HATE PUZZLES.[B][C]")
    Append("565102: Woah![P] L-[P]lord Unit![B][C]")
    Append("Christine:[E|Neutral] Sorry, sorry.[B][C]")
    Append("565102: So, here you are.[P] 400 Work Credits for your department.[P] Uh, this is...[P] off the books, okay?[B][C]")
    Append("565102: I'm going to have to fudge things on the reports because...[B][C]")
    Append("Christine:[E|Smirk] I can make sure you aren't punished.[P] I'm a Lord Unit of Sector 96, after all.[B][C]")
    Append("565102: Thank you, but that's not it.[B][C]")
    Append("565102: I'm...[P] not really assigned out here.[P] You see.[B][C]")
    Append("565102: My Lord Unit sent me out here a year ago as a punishment for...[P] well, it doesn't matter.[B][C]")
    Append("565102: She seems to have forgotten I exist.[P] I'd rather not remind her.[B][C]")
    Append("Christine:[E|Neutral] Oh dear.[P] So -[P] you were worried when the pipes broke, because you didn't want anyone to find you out here and reassign you?[B][C]")
    Append("565102: Yeah...[B][C]")
    Append("565102: I'm not really lonely, I have my scraprat buddy and I can always talk to the others on the network.[B][C]")
    Append("565102: It's weird.[P] I've sort of slipped through the cracks, and I'd rather stay there.[B][C]")
    Append("Christine:[E|Sad] (You won't have to stay there for long, my friend...)[B][C]")
    Append("Christine:[E|Neutral] Don't worry, I'll edit the work order.[P] I'll say I found them during a routine check and...[EMOTION|Christine|Laugh] neglect to mention you![B][C]")
    Append("565102: Thank you very much, Lord Unit.[P] Uhh...[P] here...[B][C]")
    Append("565102: It's a set of Regeneration Mist nanites with an aerosol dispenser.[P] I found it lodged between some pipes when I got here.[P] Maybe you can use it.[B][C]")
    Append("Christine:[E|Smirk] Thanks![P] I'll make good use of it![B][C]")
    Append("565102: Thank goodness this worked out.[P] I'll get back to it, then.[B][C]")
    Append("Christine:[E|Neutral] Back to what?[B][C]")
    Append("565102: Oh, I write works of fiction sometimes and publish them on the network.[P] Under a pseudonym, of course.[B][C]")
    Append("565102: My latest one is about these adventurers who escort a caravan, but one of them gets ambushed and...[P] turned into an ant girl...[B][C]")
    Append("565102: It's not done yet, so I won't bore you with the details.[B][C]")
    Append("Christine:[E|Happy] Let me read it when you're done with it, it sounds great![P] See you later!")
    
    --Give the player the item and word credits.
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
    VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + 400)
    LM_ExecuteScript(gsItemListing, "Regeneration Mist")

--Give it anuvva go.
else
    WD_SetProperty("Activate Topics After Dialogue", "PipeNightmare")
    Append("565102: Yeah, levels are below nominal.[P] We better keep trying.[B][C]")
    Append("Christine:[E|Smirk] I'm on it, don't worry.[P] I'll figure it out before you know it.[B][C]")
    Append("Christine:[E|Sad] (Or just randomly guess, but that'd take forever...)[B][C]")

end
