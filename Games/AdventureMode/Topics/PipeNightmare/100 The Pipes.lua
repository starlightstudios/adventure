-- |[Topic Script]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "PipeNightmare")

Append("Christine:[E|Neutral] So let's hear the basics.[P] How do the pipes work?[P] Why is it so hard to fix them?[B][C]")
Append("565102: Okay, so.[P] Pipes.[P] Simple metal or plastic things, gets compressed fluids from one side to the other.[B][C]")
Append("565102: But because the engineers who built Regulus City just couldn't help themselves, they made the system needlessly complex.[B][C]")
Append("565102: In *Red* pipes, fluid pressure is *Added* together as it flows.[P] That's pretty easy.[B][C]")
Append("565102: In *Blue* or *Yellow* pipes, fluid pressure is *Added* as it flows, but at a T-junction, *Subtracted*.[B][C]")
Append("Christine:[E|Neutral] ...[P] How?[B][C]")
Append("565102: Oh goodness I don't know.[P] I've downloaded the schematics and technical papers but it's too dense for me.[B][C]")
Append("565102: My poor robot brain...[P] I wonder what sort of chance an organic would even have?[B][C]")
Append("565102: And finally, in the *Violet* pipes, it's *Added* as it flows, and *Multiplied* at a T-Junction.[B][C]")
Append("Christine:[E|Offended] But, okay, physical impossibilities aside...[B][C]")
Append("Christine:[E|Neutral] In the subtraction cases, how do the pipes determine which side gets subtracted from which?[B][C]")
Append("565102: The priority is N, E, S, W, in that order.[P] The pipes are always flowing towards the stations here, at the north end.[B][C]")
Append("565102: So if a T-junction meets two pipes, from South and East, and merges going North, the formula is East Minus South -[P] since East is higher priority.[B][C]")
Append("Christine:[E|Neutral] (This is total nonsense. Maybe I should ask her again and write it down somewhere.)[B][C]")
Append("565102: If you want to adjust the pressure in a pipe, look for the red valves.[P] They're configured to allow certain combinations.[B][C]")
Append("565102: I've rigged up the terminals in this area so they can quickly report the steam pressures.[B][C]")
Append("565102: The target for the Red pipes is 27psi.[B][C]")
Append("565102: The target for the Blue pipes is 16psi.[B][C]")
Append("565102: The target for the Violet pipes is 39psi.[B][C]")
Append("565102: The target for the Yellow pipes is 25psi.[B][C]")
Append("565102: If you think you've got it all figured out, let me know.[P] I have some work credits for you, and some other stuff you might like.[B][C]")
Append("Christine:[E|Neutral] This will test my skills, but a real repair unit never gives up![B][C]")
Append("Christine:[E|Smirk] I'll have it fixed before you know it![B][C]")

WD_SetProperty("Unlock Topic", "Properties", 1)
WD_SetProperty("Unlock Topic", "Priorities", 1)
WD_SetProperty("Unlock Topic", "Targets", 1)
WD_SetProperty("Unlock Topic", "Solution", 1)
