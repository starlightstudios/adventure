-- |[Topic Script]|
--Dialogue script.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "PipeNightmare")

Append("Christine:[E|Neutral] All right, T-junctions.[P] How do they work again?[B][C]")
Append("565102: So, the priorities are N, E, S, W, in that order.[B][C]")
Append("565102: Pipes are flowing to...[P] where we're standing, basically.[P] Just above us there, you see?[B][C]")
Append("565102: When *Blue* or *Yellow* pipes reach a T-Junction, it flows in from two directions.[P] For example, East and West merging to the North.[B][C]")
Append("565102: When this happens, East is before West in the priority list of N E S W.[P] Therefore, the result is the pressure from East minus the pressure from West.[B][C]")
Append("Christine:[E|Smirk] Okay.[P] I think I get that.[B][C]")
