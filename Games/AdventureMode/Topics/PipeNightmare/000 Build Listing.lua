-- |[Build Listing]|
--Builds a list of topics this NPC will respond to, and at what levels. Called at Adventure Mode startup.
local sNPCName = "PipeNightmare"
gsActiveNPCName = sNPCName

--Topic listing.
fnConstructTopicStd(true,  "The Pipes", "The Pipes")
fnConstructTopicStd(false, "Properties", "Pipe Properties")
fnConstructTopicStd(false, "Priorities", "Junction Priorities")
fnConstructTopicStd(false, "Targets", "Targets")
fnConstructTopicStd(false, "Solution", "Have I fixed it?")
