-- |[Bees]|
--Asking this NPC about Bee Girls in general.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Blythe")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--If Mei is a non-bee...
	if(sMeiForm ~= "Bee") then
		Append("Mei: Any tips if I run into a bee girl out in the forest?[B][C]")
		Append("Blythe: They're agile and have a weak poison in their stingers.[P] They're not very tough but they tend to attack in groups.[B][C]")
		Append("Blythe: If you can, isolate one and take it out before its friends arrive to help.[B][C]")
		
	--If Mei is a bee...
	else
		Append("Mei: My hive has a lot of respect for you, you know.[B][C]")
		Append("Blythe: They know of me?[B][C]")
		Append("Mei: Possibly.[P] All humans look the same, sometimes.[B][C]")
		Append("Mei: You're very disciplined, which is something we value.[P] If you ever wanted a change of career...[B][C]")
		Append("Blythe: I'll have to be turning down that offer.[B][C]")
		Append("Mei: We expected that, but my sisters will still try to convince you.[B][C]")
	
	end
end
