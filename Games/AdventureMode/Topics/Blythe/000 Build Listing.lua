-- |[Build Listing]|
--Builds a list of topics this NPC will respond to, and at what levels. Called at Adventure Mode startup.
local sNPCName = "Blythe"

--Topic listing.
fnConstructTopic("Alraunes",        "Alraunes",         -1, sNPCName, 0)
fnConstructTopic("Bandits",         "Bandits",          -1, sNPCName, 0)
fnConstructTopic("Bees",            "Bees",             -1, sNPCName, 0)
fnConstructTopic("Cultists",        "Cultists",         -1, sNPCName, 0)
fnConstructTopic("Dungeon",         "Dungeon",          -1, sNPCName, 0)
fnConstructTopic("Florentina",      "Florentina",       -1, sNPCName, 0)
fnConstructTopic("Ghosts",          "Ghosts",           -1, sNPCName, 0)
fnConstructTopic("Job",             "Job",              -1, sNPCName, 0)
fnConstructTopic("Nadia",           "Nadia",            -1, sNPCName, 0)
fnConstructTopic("Name",            "Name",             -1, sNPCName, 0)
fnConstructTopic("Pandemonium",     "Pandemonium",      -1, sNPCName, 0)
fnConstructTopic("Rilmani",         "Rilmani",          -1, sNPCName, 0)
fnConstructTopic("Slimes",          "Slimes",           -1, sNPCName, 0)
fnConstructTopic("Werecats",        "Werecats",         -1, sNPCName, 0)
fnConstructTopic("Swamp Witch",     "Swamp Witch",      -1, sNPCName, 0)
fnConstructTopic("Challenge",       "Challenge",        -1, sNPCName, 0)
