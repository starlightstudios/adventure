-- |[Nadia]|
--Asking this NPC about Nadia.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Blythe")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Dialogue.
	WD_SetProperty("Major Sequence Fast", true)
	Append("Mei: Nadia seems...[P] different...[P] from the other guards.[B][C]")
	Append("Blythe: I take it your comment is more substantive than her skin's tone.[B][C]")
	Append("Mei: The jokes...[B][C]")
	Append("Blythe: Yes, that is a common topic.[P] She's unprofessional, brash, and often rude.[B][C]")
	Append("Blythe: It's not her fault, though.[P] She's learning.[P] I think she forgot how to act around humans after she became an Alraune.[B][C]")
	Append("Blythe: That said, her foraging skills are unparalleled.[P] She's quite an asset here at the trading post.[B][C]")
	Append("Blythe: Mind you, her usefulness is not why she's here.[P] She's been through...[P] a lot.[B][C]")
	Append("Mei: How do you mean?[B][C]")
	Append("Blythe: Ask her about me.[P] Ask her how we met.[P] If she can bear it, she'll tell you.[B][C]")
end
