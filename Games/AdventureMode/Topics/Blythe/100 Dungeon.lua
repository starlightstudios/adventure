-- |[Dungeon]|
--Dialogue script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Blythe")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	local iHasAlrauneForm = VM_GetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N")
	
	--Dialogue.
	Append("Mei:[E|Sad] Blythe, the cultists summoned...[P] something...[P] in the basement under the Dimensional Trap.[B][C]")
	Append("Florentina:[E|Confused] It was big and extremely dangerous.[P] If they summon more, the Trading Post could be in big trouble.[B][C]")
	Append("Blythe: An animal of some sort?[B][C]")
	Append("Florentina:[E|Confused] No, that is not how I'd put it.[B][C]")
	Append("Mei:[E|Sad] It was an atrocity.[P] It had metal grafted onto its skin and its innards sloshed out of its mouth...[B][C]")
	Append("Blythe: Most distressing...[B][C]")
	Append("Florentina:[E|Confused] They can be wounded, I'll say that much.[P] Killed, I have no idea...[B][C]")
	Append("Mei:[E|Offended] You should form an alliance with the Alraunes![P] They've no love for the cultists, either![B][C]")
	Append("Blythe: I'm not sure they'd take the offer.[P] What few dealings we've had in the past usually ended with threats.[B][C]")
	Append("Mei:[E|Offended] You might not have that choice this time.[P]  Please, try.[B][C]")
	Append("Blythe: Their solution is always the same one.[P] Every disagreement can be solved by them turning everyone here into Alraunes.[B][C]")
	if(iHasAlrauneForm == 0.0) then
		Append("Florentina:[E|Neutral] Yeah, they're kind of awful at negotiating.[B][C]")
	else
		Append("Mei:[E|Blush] Well, there's a certain logic to that...[B][C]")
		Append("Florentina:[E|Neutral] Shut up,[P] Mei.[B][C]")
		Append("Mei:[E|Offended] Jeez...[B][C]")
	end
	Append("Blythe: Still, if this threat is as dangerous as you say it is, they'll be willing to talk at the very least.[P] I'll put together an envoy.[B][C]")
	Append("Florentina:[E|Happy] See?[P] This is why he gets the big bucks.[B][C]")
end
