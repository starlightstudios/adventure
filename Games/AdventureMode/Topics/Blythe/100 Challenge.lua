-- |[ ================================ Blythe: Challenge Modes ================================= ]|
--Hey you want to make your life harder!?
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Blythe")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Dialogue.
    Append("Mei:[E|Neutral] Let's say I'm looking for an extreme challenge with no reward whatsoever.[B][C]")
    Append("Blythe:[E|Neutral] There are ways.[B][C]")
    Append("Blythe:[E|Neutral] Try the password 'Broken Treadmill' at a save point.[P] That will make you unable to gain XP.[B][C]")
    Append("Blythe:[E|Neutral] Or worse, 'I love kumquat' which will set you back to level 1 and prevent XP gain.[B][C]")
    Append("Mei:[E|Smirk] Now that's what I'm looking for, needless suffering![P] But what if I want to undo that?[B][C]")
    Append("Blythe:[E|Neutral] 'Niko Repair Service' will unset the first one, 'Review WHI on Steam and Itch' will unset the second one.[P] You won't regain lost experience, though.[B][C]")
    Append("Mei:[E|Smirk] Thanks a lot![B][C]")
end
