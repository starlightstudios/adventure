-- |[Rilmani]|
--Dialogue script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Blythe")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Dialogue.
	Append("Mei:[E|Neutral] Have you ever heard of the Rilmani?[B][C]")
	Append("Blythe: The troops entertain themselves with stories of them, but I don't pay much attention to them.[B][C]")
	Append("Blythe: They're a legend that stretches across Pandemonium.[B][C]")
	Append("Blythe: While I was in the academy, I did see a mention of them in a book for officers.[P] Specifically, how to fight them.[B][C]")
	Append("Blythe: Make sure to cut their trunk off at the earliest opportunity.[P] They use them to spit acid.[B][C]")
	Append("Mei:[E|Neutral] I will...[P] keep that in mind...[B][C]")
end
