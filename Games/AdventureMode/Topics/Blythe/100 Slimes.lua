-- |[Slimes]|
--Asking this NPC about slimes.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Blythe")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--Standard.
	WD_SetProperty("Major Sequence Fast", true)
	
	--If Mei is not a slime...
	if(sMeiForm ~= "Slime") then
		Append("Mei: What should I do to...[P] ward off the advances of a slime?[B][C]")
		Append("Blythe: Slimes aren't known for their speed.[P] Or their intelligence.[P] Or much of anything, really.[B][C]")
		Append("Blythe: They'll make no effort to dodge you, so take your time and pick your strikes.[P] You should be fine if you don't get overwhelmed.[B][C]")
	
	--If Mei is a slime...
	else
		Append("Mei: Can you tell me anything about the slimes in this area?[B][C]")
		Append("Blythe: I would assume you would know better than I.[B][C]")
		Append("Mei: I haven't been a slime very long.[B][C]")
		Append("Blythe: Well, slimes aren't fast.[P] They're not intelligent, either.[P] Present company excluded, of course.[B][C]")
		Append("Blythe: I believe they're highly resistant to poisons, so that might be something to keep in mind.[B][C]")
	
	end
end
