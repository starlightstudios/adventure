-- |[Werecats]|
--Dialogue script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Blythe")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--If Mei is a non-werecat:
	if(sMeiForm ~= "Werecat") then
		Append("Mei:[E|Neutral] Do many werecats come to the outpost?[B][C]")
		Append("Blythe: Few.[P] They tend not to have much patience for us.[B][C]")
		Append("Blythe: I certainly don't have much patience for them.[P] They're needlessly aggressive.[B][C]")
		Append("Mei:[E|Neutral] But some do come by?[B][C]")
		Append("Blythe: There are a few werecats who are scavengers.[P] Anything they find that they think they can sell, they bring here.[P] Florentina usually handles them.[B][C]")
		Append("Blythe: They also buy medicine sometimes.[P] Mostly for wounds that I'd guess were inflicted by other werecats.[B][C]")
		Append("Mei:[E|Offended] They're pretty dangerous in a fight.[P] Got any advice?[B][C]")
		Append("Blythe: They're extremely fast.[P] Try using an attack that's faster, even if it's not as powerful.[P] Better to land a weak hit than miss a strong one.[B][C]")
		Append("Blythe: There are some books around that may help you.[P] The researcher's cabin just north of the trading post had a copy of the Fencer's Friend, I think.[B][C]")
		Append("Mei:[E|Neutral] Thanks.[P] I'll go ask around.[B][C]")
	
	--If Mei is a werecat:
	else
		Append("Mei:[E|Neutral] Do many werecats come to the outpost?[B][C]")
		Append("Blythe: You're the first one we've had in a few weeks.[P] They don't have much need for us.[B][C]")
		Append("Blythe: They do tend to behave themselves when they're here.[P] We don't tolerate violence, and I make it very clear what we do to offenders.[B][C]")
		Append("Mei:[E|Smirk] Hunters, not fools, they are.[P] Though I will admit that attacking one another for sport is something we do.[B][C]")
		Append("Mei:[E|Smirk] It's a good way to train the weak kinfangs to be strong, you see.[B][C]")
		Append("Blythe: We're not kinfangs.[B][C]")
		Append("Mei:[E|Smirk] Heh.[P] You would make a good night stalker, Darius.[B][C]")
		Append("Blythe: I've had a few of your kin think the same thing.[P] I rebuffed them with the edge of my spear.[B][C]")
		Append("Mei:[E|Smirk] Then that'd make you all the better.[P] Surely you see that?[B][C]")
		Append("Blythe: I appreciate the compliments, but I'm not interested.[B][C]")
		Append("Mei:[E|Neutral] A shame...[B][C]")
	
	end
end
