-- |[ ==================================== Blythe: Alraunes ==================================== ]|
--Asking this NPC about Alraunes in general.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Blythe")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--If Mei is a human...
	if(sMeiForm ~= "Alraune") then
		Append("Mei: Are Alraunes very common around here?[B][C]")
		Append("Blythe: Quite. You'll find them in most forested areas.[P] I've heard there are a different breed found in jungles, but I've never seen them myself.[B][C]")
		Append("Blythe: They claim to be non-violent, but that rule has plenty of exceptions, it seems.[B][C]")
		Append("Mei: You're fine with them working at the trading post?[B][C]")
		Append("Blythe: You're referring to Nadia and Florentina?[P] Anyone who follows the rules is welcome here.[P] We do business with the werecats, too, when they can behave themselves.[B][C]")
	
	--If Mei is an Alraune
	else
		Append("Mei: You don't seem to be uncomfortable around Alraunes.[B][C]")
		Append("Blythe: There are plenty of monsters on Pandemonium.[P] Alraunes are among the more agreeable of them.[P] Sometimes.[B][C]")
		Append("Mei: Sometimes?[B][C]")
		Append("Blythe: Hatred knows many forms.[P] Humans aren't any better or worse, but Alraunes seem to think they're above it.[B][C]")
		Append("Mei: If all humans became Alraunes...[B][C]")
		Append("Blythe: Yeah, sure.[P] And if all Alraunes just died, then humans wouldn't fight them anymore.[P] Nothing is more peaceful than a graveyard.[B][C]")
		Append("Mei: A fair point, and well made.[B][C]")
	
		--Topic unlocks.
		WD_SetProperty("Unlock Topic", "Pandemonium", 1)
	end
end
