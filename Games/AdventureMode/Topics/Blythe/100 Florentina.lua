-- |[Florentina]|
--Asking this NPC about Florentina.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Blythe")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Variables.
	local bIsFlorentinaInParty = AL_GetProperty("Is Character Following", "Florentina")
	
	--Standard.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Dialogue.
	Append("Mei: Has Florentina been here very long?[B][C]")
	
	--If Florentina is not present...
	if(bIsFlorentinaInParty == false) then
		Append("Blythe: She's been at the post for a few years now.[P] I get a lot of complaints about her.[B][C]")
		Append("Mei: Oh no.[P] What did she do?[B][C]")
		Append("Blythe: I believe she counts cards.[P] Very good at it, too.[P] Don't play a game of chance with her.[B][C]")
		Append("Mei: I'll keep that in mind.[B][C]")
		
	--If Florentina is present...
	else
		Append("Blythe: She's right there, why don't you ask her yourself?[B][C]")
		Append("Florentina: Uh, I think I've forgotten.[P] I'm not as meticulous as you are, Darius.[B][C]")
		Append("Blythe: This is why we have paperwork, Florentina.[P] Writing can remember things longer than minds.[B][C]")
		Append("Florentina: Conveniently, writing also allows nosy to people to remember things they have no business remembering.[B][C]")
		Append("Mei: Sorry if I touched a nerve.[B][C]")
		Append("Florentina: I like to be unmoored, is all.[P] I've been here at least five summers, if I recall correctly.[B][C]")
		Append("Blythe: Fortunately I have your records here.[P] You are correct, you came here five years ago.[B][C]")
		Append("Florentina: See?[P] I was right![B][C]")
		Append("Mei: You're both right.[P] Heh.[B][C]")
	end
end
