-- |[ ==================================== Blythe: Bandits ===================================== ]|
--Asking this NPC about the bandits near the fishing village.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Blythe")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Variables.
    local iSawFort           = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iSawFort", "N")
    local iCompletedSequence = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iCompletedSequence", "N")
	
    --Hasn't seen the fort:
    if(iSawFort == 0.0 and iCompletedSequence == 0.0) then
        Append("Florentina:[E|Neutral] Hey Cap'n, remember how we kept hearing about some of the merchants getting hit on their way out west?[B][C]")
        Append("Blythe: I do.[P] And we couldn't find a trace of where the crooks went.[B][C]")
        Append("Florentina:[E|Neutral] South of the fishing village, by the look of it.[P] Place was crawling with thugs.[B][C]")
        Append("Blythe: Lovely.[P] Why haven't I heard about this from any messengers?[B][C]")
        Append("Florentina:[E|Neutral] Don't know, maybe they're picky about their targets.[P] These guys are likely ex-military, so they know to leave messengers alone.[B][C]")
        Append("Blythe: Ex-military?[P] Do you know what outfit?[B][C]")
        Append("Florentina:[E|Neutral] From their clothes and accents?[P] I'd guess Dresseheim or its territories.[B][C]")
        Append("Mei:[E|Neutral] Can I ask where that is?[B][C]")
        Append("Blythe: Across the Trafal mountains to the south.[P] Dresseheim owns most of that territory, and the smaller cities there are vassal states.[B][C]")
        Append("Blythe: I've been hearing a lot of very bad rumours from down that way.[P] Bandits are often defectors who aren't getting paid, or paid enough.[B][C]")
        Append("Blythe: I suppose it's our responsibility to go deal with the bandits.[P] Thank you for the report.[B][C]")
    elseif(iSawFort == 1.0 and iCompletedSequence == 0.0) then
        Append("Florentina:[E|Neutral] Hey Cap'n, remember how we kept hearing about some of the merchants getting hit on their way out west?[B][C]")
        Append("Blythe: I do.[P] And we couldn't find a trace of where the crooks went.[B][C]")
        Append("Florentina:[E|Neutral] They rebuilt an old draconic fort that was south of the fishing village.[P] Damn fine work on the walls, too.[P] Had to be professional.[B][C]")
        Append("Blythe: Engineering corps, you're thinking?[P] Ex-military?[P] From where?[B][C]")
        Append("Florentina:[E|Neutral] From their clothes and accents?[P] I'd guess Dresseheim or its territories.[B][C]")
        Append("Mei:[E|Neutral] Can I ask where that is?[B][C]")
        Append("Blythe: Across the Trafal mountains to the south.[P] Dresseheim owns most of that territory, and the smaller cities there are vassal states.[B][C]")
        Append("Blythe: I've been hearing a lot of very bad rumours from down that way.[P] Bandits are often defectors who aren't getting paid, or paid enough.[B][C]")
        Append("Blythe: I suppose it's our responsibility to go deal with the bandits.[P] Thank you for the report.[B][C]")
        Append("Mei:[E|Neutral] Actually, I was wondering.[P] If we had to get into the fort...[B][C]")
        Append("Blythe: I'd tell you not to, the entire point of a fort is to defend against exactly that.[B][C]")
        Append("Blythe: If you believe there is a compelling reason to get in one, then you should look for a way in that the bandits will not know about.[P] Considering they reinforced the walls, that's not likely.[B][C]")
        Append("Blythe: And you ought to do it at night.[P] I don't recommend doing it as you are.[B][C]")
    elseif(iSawFort == 1.0 and iCompletedSequence == 1.0) then
        Append("Florentina:[E|Neutral] Hey Cap'n, remember how we kept hearing about some of the merchants getting hit on their way out west?[B][C]")
        Append("Blythe: Nadia has filled me in about your exploits.[P] You took down a bandit fort by yourselves.[B][C]")
        Append("Mei:[E|Happy] We had help![P] Polaris the witch helped us![B][C]")
        Append("Florentina:[E|Happy] I hope Nadia didn't mention anything else about that escapade.[B][C]")
        Append("Blythe: Cursed jewelry, so I heard.[P] If you believe the bandits have cargo belonging to the trading companies, we'll have to send a team in.[B][C]")
        Append("Blythe: As you are familiar with the area, can I trust you to lead the assessors when the team has the area cleared?[B][C]")
        Append("Florentina:[E|Neutral] Yeah, but my insurance is already paid out, so I can't claim anything.[B][C]")
        Append("Blythe: So much the better.[B][C]")
    end
end
