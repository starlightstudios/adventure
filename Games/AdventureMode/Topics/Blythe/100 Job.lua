-- |[Name]|
--Asking this NPC about their job. A lot of NPCs have this.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Blythe")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common code.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Dialogue.
	Append("Mei: What is it that you do here, Captain Blythe?[B][C]")
	Append("Blythe: Please, you can call me Darius if you like.[B][C]")
	Append("Blythe: I make sure the guards here do their jobs properly and deal with the disputes that come up whenever Florentina has to haggle with someone.[B][C]")
	Append("Blythe: I used to do logistics, but I've let the merchants take over for that.[P] It's their coin, after all.[B][C]")
	Append("Mei: Do the merchants pay you?[B][C]")
	Append("Blythe: They pay a fee, but most of our funding comes from the shipping companies that go through here.[P] They need safe roadways to make money, after all.[B][C]")
end
