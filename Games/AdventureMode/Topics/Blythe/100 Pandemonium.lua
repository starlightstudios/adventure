-- |[Pandemonium]|
--Asking this NPC about Pandemonium.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Blythe")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Dialogue.
	WD_SetProperty("Major Sequence Fast", true)
	Append("Mei: What exactly is Pandemonium?[B][C]")
	Append("Blythe: It's where you're standing.[B][C]")
	Append("Mei: So not Earth, then.[P] Am I on another planet?[P] In another galaxy?[B][C]")
	Append("Blythe: Hm, do you recognize any of the stars?[P] Perhaps you're just a long way from Earth.[P] Maybe across the ocean?[B][C]")
	Append("Mei: No, no.[P] On Earth, we have electricity, airplanes, cars, satellites...[B][C]")
	Append("Mei: Indoor plumbing...[B][C]")
	Append("Mei: Pandemonium must be another planet.[P] It has to be.[P] And that means there must be a way back to Earth...[B][C]")
	
	--Variables.
	local bIsFlorentinaInParty = AL_GetProperty("Is Character Following", "Florentina")
	
	--If Florentina is not present...
	if(bIsFlorentinaInParty == false) then
		Append("Blythe: As I said, speak to Florentina.[P] If there is a way, she will know it, or know who knows it.[B][C]")
	else
		Append("Florentina: Relax.[P] I know some people who know some people.[P] We'll get you back home, you big baby.[B][C]")
	end
end
