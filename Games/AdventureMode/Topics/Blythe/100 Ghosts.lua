-- |[Ghosts]|
--Dialogue script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Blythe")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--If Mei is a non-ghost:
	if(sMeiForm ~= "Ghost") then
		Append("Mei:[E|Neutral] Were you aware of the ghosts in the Quantir manor?[B][C]")
		Append("Blythe: No.[P] It's only recently that I've even heard the place is open.[B][C]")
		Append("Mei:[E|Neutral] It wasn't open?[P] Nobody has tried to enter it?[B][C]")
		Append("Blythe: Oh, we get our fair share of treasure hunters here.[P] They've tried, but they failed.[P] The windows wouldn't break, and tunneling in didn't work either.[B][C]")
		Append("Blythe: Of course, that just caused more treasure hunters to come.[P] They failed, too.[B][C]")
		Append("Blythe: I suppose the emergence of the ghosts is a response to the building being infiltrated.[P] I recall a religious convent that came through here being very interested in the building.[B][C]")
		Append("Mei:[E|Neutral] Hmmm...[B][C]")
	
	--If Mei is an Alraune
	else
		Append("Mei:[E|Neutral] Were you aware of the ghosts in the Quantir manor?[B][C]")
		Append("Blythe: No, but I assume you are.[P] Perhaps you could shed some light on the situation?[B][C]")
		Append("Mei:[E|Neutral] It's quite complicated.[P] The other ghosts seem to think there's a sickness in Quantir, in the present-tense.[B][C]")
		Append("Blythe: Ah, the plague.[P] I remember reading about it some time back.[P] It was over two centuries ago.[P] The exact date was lost, as the entire population of Quantir succumbed.[B][C]")
		Append("Mei:[E|Sad] We -[P] I mean, they -[P] seemed to think I was sick.[P] Until I got...[P] chained...[B][C]")
		Append("Blythe: Perhaps the presence of humans in the manor made the ghosts wake up?[P] That'd be my guess.[B][C]")
		Append("Blythe: Still, this is a security issue.[P] I'll have to have notices put up.[P] Should I have the main entrance barred?[B][C]")
		Append("Mei:[E|Neutral] You can try, but I can't say whether or not the other maids will remove them.[P] It might be a good idea to have Nadia do it, as they seem to not notice Alraunes.[B][C]")
		Append("Blythe: That's very helpful.[P] I wouldn't want to put anyone at risk.[P] Thank you, Mei.[B][C]")
	
	end
end
