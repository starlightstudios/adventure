-- |[Swamp Witch]|
--Dialogue script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Blythe")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
    --Variables.
    local iMetPolaris = VM_GetVar("Root/Variables/Chapter1/Polaris/iMetPolaris", "N")
    WD_SetProperty("Major Sequence Fast", true)
    
	--Not met Polaris.
    if(iMetPolaris == 0.0) then
        Append("Mei:[E|Neutral] Mister Blythe, do you know anything about the swamp witch?[B][C]")
        Append("Blythe: Yes, but only from what Nadia has reported.[P] She's the only one I send into the swamps.[B][C]")
        Append("Blythe: Starfield Swamp is west of here, and it's a particularly dangerous area.[P] Nadia is an alraune, so she really can't get ambushed.[B][C]")
        Append("Mei:[E|Neutral] Do you think the witch is dangerous?[B][C]")
        Append("Blythe: No, as I believe she was in the area selling something recently.[P] So, no more dangerous than any other yahoo you meet in the forest.[B][C]")
        Append("Blythe: If she comes by again, I'll let her know you were looking for her.[B][C]")
        Append("Mei:[E|Smirk] Thanks.[P] Any idea how to find her?[B][C]")
        Append("Blythe: Ask Nadia for directions.[P] She knows the terrain.[B][C]")
	else
        Append("Mei:[E|Neutral] Mister Blythe, do you know anything about Polaris?[B][C]")
        Append("Blythe: Hmm, I believe that's the name of the sorceress who moved into the swamp recently.[B][C]")
        Append("Blythe: I believe she was selling something at the trading post, but I was not there to see her in person.[B][C]")
        Append("Blythe: You may want to ask Nadia, she patrols the swamps.[B][C]")
    end
end
