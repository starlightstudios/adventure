-- |[Cultists]|
--Asking this NPC about the spooky cultists in the mansion.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Blythe")

--Variables.
local iCompletedTrapDungeon = VM_GetVar("Root/Variables/Chapter1/Scenes/iCompletedTrapDungeon", "N")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Standard.
	WD_SetProperty("Major Sequence Fast", true)
	if(iCompletedTrapDungeon == 0.0) then
		Append("Mei:[E|Neutral] Do you know anything about the cultists in the mansion near here?[B][C]")
		Append("Blythe: A scout said he saw movement in that old heap, but we never followed up on it.[B][C]")
		Append("Blythe: It's a very old building and there's a lot of myths about it.[P] I don't know if its some abandoned estate or if it really is some ancient ruin from the first age.[B][C]")
		Append("Blythe: Still, cultists very rarely are peaceful.[P] Do you know how many there were?[B][C]")
		Append("Mei:[E|Offended] Lots.[P] Dozens.[P] Probably more, I wasn't exactly counting.[B][C]")
		Append("Blythe: This does not bode well.[B][C]")
		Append("Blythe: Thank you for the report.[P] I'll make sure my patrols are briefed.[B][C]")
	
	--Completed trap dungeon.
	else
		Append("Mei:[E|Neutral] Blythe...[B][C]")
		Append("Blythe: Yes Miss Mei, I've heard about the situation at the mansion.[B][C]")
		Append("Mei:[E|Neutral] How?[B][C]")
		Append("Blythe: Nadia has been relaying reports.[P] The Alraunes are not interested in our help, but apparently the plants are.[B][C]")
		Append("Florentina:[E|Happy] Seems they have their heads on straight.[P] Metaphorically.[B][C]")
		Append("Mei:[E|Neutral] So you know what happened?[B][C]")
		Append("Blythe: I know enough.[P] If you had requested support, we'd have at least dispatched a scouting team.[B][C]")
		Append("Blythe: I'm not certain the cultists are a major threat, at least not until I can verify what I'd been hearing.[B][C]")
		Append("Mei:[E|Neutral] I've tried to talk Rochea into an alliance...[B][C]")
		Append("Blythe: It is appreciated, though I doubt it will bear fruit.[B][C]")
		Append("Blythe: She may not be willing to admit it, but she's still just as proud as any human is.[P] She won't accept help until she's backed into a corner.[B][C]")
		Append("Florentina:[E|Happy] Which is fine with me.[P] Let them fight it out.[P] The mercs here can clean up the mess.[B][C]")
		Append("Mei:[E|Neutral] You don't fully grasp the nature of what's going on here...[B][C]")
		Append("Florentina:[E|Neutral] Yeah, I don't.[P] But you do, don't you?[B][C]")
		Append("Mei:[E|Sad] ...[B][C]")
		Append("Blythe: In any case, there is potential for an alliance.[P] Yet, if the threat is of the nature you're suggesting, we will need to prepare to go it alone.[P] I will see to the preparations.[B][C]")
		Append("Blythe: Thank you for your assisstance, Mei.[B][C]")
		Append("Mei:[E|Sad] Yeah...[B][C]")
	
	end
end
