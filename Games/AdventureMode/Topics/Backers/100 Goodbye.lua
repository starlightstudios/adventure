-- |[Goodbye]|
--Script that fires when the player says goodbye to end the dialogue. Not all NPCs actually do anything here! This
-- script's existence is also optional.
WD_SetProperty("Major Sequence Fast", true)

--Standard.
Append("Mei:[E|Happy] Thanks for your support, everyone![B][C]")
Append("Mei:[E|Neutral] Wow, I really wrecked the fourth wall there...")
