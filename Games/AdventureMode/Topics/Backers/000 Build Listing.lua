-- |[Build Listing]|
--Builds a list of topics this NPC will respond to, and at what levels. Called at Adventure Mode startup.
local sNPCName = "Backers"

--Topic listing.
fnConstructTopic("2017 2 February", "February 2017", 1, sNPCName, 0)
fnConstructTopic("2017 3 March",    "March 2017",    1, sNPCName, 0)
fnConstructTopic("2017 4 April",    "April 2017",    1, sNPCName, 0)
fnConstructTopic("2017 5 May",      "May 2017",      1, sNPCName, 0)
fnConstructTopic("2017 6 June",     "June 2017",     1, sNPCName, 0)
fnConstructTopic("2017 7 July",     "July 2017",     1, sNPCName, 0)
fnConstructTopic("2017 8 August",   "August 2017",   1, sNPCName, 0)
fnConstructTopic("2017 9 September","September 2017",1, sNPCName, 0)
fnConstructTopic("2017 A October",  "October 2017",  1, sNPCName, 0)
