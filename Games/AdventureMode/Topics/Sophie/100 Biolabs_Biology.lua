-- |[Topic Script]|
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Sophie")

Append("Sophie:[E|Neutral] Hey Christine, want to hear a scary story?[B][C]")
Append("Christine:[E|Smirk] You can't scare me.[B][C]")
Append("Sophie:[E|Neutral] You won't be saying that once I'm done.[B][C]")
Append("Christine:[E|Smirk] Try me.[B][C]")
Append("Sophie:[E|Smirk] Imagine working here, in the biolabs.[P] Fixing things that break down.[B][C]")
Append("Sophie:[E|Smirk] Sand in the keyboards, organic gunk in the power sockets, rust from ambient water in the air...[B][C]")
Append("Christine:[E|Scared] Sophie, no![B][C]")
Append("Sophie:[E|Happy] Moss and fungus growing inside a terminal![P] Scrubbing and rinsing to get it all out but it gets into the little corners and never comes out![B][C]")
Append("Christine:[E|Scared] Stop![P] Stop, I give![P] It's too horrible!BLOCK][CLEAR]")
Append("Sophie:[E|Smirk] Hee hee![P] We have it easy in sector 96![B][C]")
Append("Christine:[E|Smirk] Remind me to never assume you don't mean exactly what you say...[B][C]")
