-- |[Topic Script]|
--Setup.
WD_SetProperty("Major Sequence Fast", true)

--Variables.
local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Sophie")

Append("Sophie:[E|Neutral] Christine?[P] I was wondering what you can do when you're an electrosprite.[B][C]")
Append("Christine:[E|Smirk] Oh, like zip through wires?[B][C]")
Append("Christine:[E|Blush] 55 and I tried practicing it with Psue's help, but I'm not very good at it.[P] I could get a few meters before I got stuck and she had to get me out.[B][C]")
Append("Christine:[E|Blush] I could probably figure it out but we ran out of time and had a lot of organizing to do.[B][C]")
Append("Sophie:[E|Happy] Wow![P] You can use that to sneak around and spy on units, right?[B][C]")
Append("Christine:[E|Neutral] No, actually I can't hear anything when I'm in a wire.[P] Apparently you have to listen for soundwaves disrupting the electrical waves but it was just static to me.[B][C]")
Append("Christine:[E|Smirk] Psue said she would love to help me practice so I can use it to sneak around and ambush the Administration goons, though![B][C]")
Append("Sophie:[E|Happy] Cool![B][C]")
Append("Sophie:[E|Smirk] But actually, I was meaning...[B][C]")
Append("Christine:[E|Blush] You want me to hop inside you?[B][C]")
Append("Sophie:[E|Blush] Yeah, maybe hijack my processes, make me do things...[B][C]")
Append("Christine:[E|Blush] Psue said I'm not ready for that...[B][C]")
Append("Christine:[E|Neutral] The other electrosprites tried it a few times, but you have to be very deeply in synchronicity with the unit you jump into or you'll probably just get kicked right back out.[P] That's why they usually just hop into their tandem units.[B][C]")
Append("Sophie:[E|Offended] Why would you say we're not in sync, Christine?[B][C]")
Append("Christine:[E|Laugh] Your clock speed, silly![B][C]")
Append("Sophie:[E|Happy] Oh, I get you![P] Hee hee![B][C]")
Append("Sophie:[E|Happy] You keep practicing because I want you inside me so bad![B][C]")
Append("Christine:[E|Laugh] Me too![B][C]")
