-- |[Hive Mine]|
--Karina asks you about the hive mind.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Karina")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--Bee form.
	if(sMeiForm == "Bee") then
		Append("Karina: So, how does the hive mind work?[P] Can you describe it?[B][C]")
		Append("Mei:[E|Neutral] I'm afraid you wouldn't understand it without being able to experience it.[B][C]")
		Append("Mei:[E|Blush] It truly is incomparable to human experience.[P] Not even the internet comes close, and that place is an echo chamber.[B][C]")
		Append("Karina: What's an internet?[B][C]")
		Append("Mei:[E|Neutral] Nevermind...[B][C]")
		Append("Karina: All right, then.[P] How do you interact with the other bees?[B][C]")
		Append("Mei:[E|Neutral] They...[P] we can hear one another's ideas through our antennae here, and we send our ideas to one another when we have them.[B][C]")
		Append("Mei:[E|Laugh] They're always telling me what they're seeing and hearing, and what they're thinking.[P] It's like they're my own thoughts.[B][C]")
		Append("Karina: Could you stop your thoughts from going out?[B][C]")
		Append("Mei:[E|Offended] N-[P]never![P] Why would I want to?[B][C]")
		Append("Mei:[E|Sad] Imagine you love someone so deeply that you want to share every moment of your life with them.[P] That is how I am with my drone sisters.[B][C]")
		Append("Karina: But you could, in theory?[B][C]")
		Append("Mei:[E|Cry] ...[P] Only if the hive would be better served by my absence.[B][C]")
		Append("Mei:[E|Sad] Even thinking this has caused us to shudder.[P] I am sorry, sisters.[B][C]")
	
	--Any other form:
	else
		Append("Karina: So, how does the hive mind work?[P] Can you describe it?[B][C]")
		Append("Mei:[E|Neutral] I'm afraid you wouldn't understand it without being able to experience it.[B][C]")
		Append("Mei:[E|Blush] It truly is incomparable to human experience.[P] Not even the internet comes close, and that place is an echo chamber.[B][C]")
		Append("Karina: What's an internet?[B][C]")
		Append("Mei:[E|Neutral] Nevermind...[B][C]")
		Append("Karina: All right, then.[P] How do you interact with the other bees?[B][C]")
		Append("Mei:[E|Neutral] If I had my antennae, I'd be hearing their thoughts through them.[P] We transmit what we're thinking to one another.[B][C]")
		Append("Karina: Does that mean someone could intercept them somehow?[P] Oh, or possibly listen in![B][C]")
		Append("Mei:[E|Neutral] Sorry, but you don't speak our language.[P] Every hive has its own thought language.[P] It's quite complicated, actually.[B][C]")
		Append("Karina: But would you?[P] Can you speak the language?[B][C]")
		Append("Mei:[E|Happy] Not speak, think.[P] When you're counting, you count in numbers, right?[P] Those numbers are ones you learned from your language.[B][C]")
		Append("Mei:[E|Neutral] The way we think is organized by our language.[P] As a bee, the way I think is organized by the hive mind.[B][C]")
		Append("Karina: Amazing![P] But what happens if you're cut off from the hive mind?[B][C]")
		Append("Mei:[E|Sad] Well, I'd find my way back as soon as possible![P] It'd be...[P] truly frightening to be so alone...[B][C]")
	end
end
