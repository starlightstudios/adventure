-- |[Alraunes]|
--Karina asks you about Alraunes.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Karina")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	Append("Mei:[E|Neutral] I was wondering.[P] What is a partirhuman?[B][C]")
	Append("Karina: Oh, that's the technical term for what is commonly called a monster girl.[B][C]")
	Append("Karina: Humans are very similar to animals like dogs and birds and apes, in that we can reproduce sexually.[P] In fact, the majority of macrospecies do that.[B][C]")
	Append("Karina: Partirhumans cannot do that, they instead must turn humans into their own kind.[P] Incidentally, all partirhumans are female, and male humans become female during the process.[B][C]")
	Append("Karina: They all have a different method for the conversion.[P] Nobody is quite sure how they evolved like that, but that's what my research is for![B][C]")
	Append("Mei:[E|Neutral] (Perhaps my runestone has something to do with this...)[B][C]")
end
