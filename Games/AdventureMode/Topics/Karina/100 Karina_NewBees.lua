-- |[New Bees]|
--Karina asks you about how bees make new bees.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Karina")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	Append("Karina: Can you tell me a bit more about the process of making a new bee?[P] I can't get close to the hive to observe it.[B][C]")
	Append("Mei:[E|Blush] There is a very fast way to find out...[B][C]")
	Append("Karina: You simply telling me is faster, isn't it?[B][C]")
	Append("Mei:[E|Neutral] Suit yourself.[B][C]")
	Append("Mei:[E|Neutral] We bees can turn nectar into honey with our special enzymes, much like the diminuitive bee.[B][C]")
	Append("Mei:[E|Neutral] The honey is both a long-term food source, and when specially prepared, acts as a transformation agent.[B][C]")
	Append("Karina: I see![P] So you need honey both for food and reproduction?[B][C]")
	Append("Mei:[E|Neutral] Correct.[P] It takes approximately the body weight of a human in honey to fully transform one.[B][C]")
	Append("Mei:[E|Neutral] We are limited in our recruitment by our honey income.[P] Once we need all our honey for food, we regrettably cannot convert more bees.[B][C]")
	Append("Mei:[E|Neutral] We are thus somewhat choosy in our recruitment.[P] My hive is young, so we are always on the lookout.[B][C]")
	Append("Karina: Very interesting![P] That answers quite a few questions about the spread of bee hives.[B][C]")
end
