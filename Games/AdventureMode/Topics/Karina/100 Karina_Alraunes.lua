-- |[Alraunes]|
--Karina asks you about Alraunes.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Karina")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
    local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	
    -- |[Florentina is Present]|
    if(bIsFlorentinaPresent) then
    
        --Bee form.
        if(sMeiForm == "Bee") then
            Append("Karina: So, how do bees get along with the Alraunes?[B][C]")
            Append("Mei:[E|Smirk] I'd say Florentina and I get along famously.[B][C]")
            Append("Florentina:[E|Neutral] I think the buzzing is melting her brain.[B][C]")
            Append("Mei:[E|Happy] You see?[P] Best friends.[B][C]")
            Append("Karina: But seriously...[B][C]")
            Append("Mei:[E|Neutral] They mostly leave us alone, and we leave them alone.[P] They are good sources of nectar when they want to be, but they are often obstinate and not worth the risk.[B][C]")
            Append("Karina: Wait, the Alraunes themselves?[P] You can derive nectar from them?[B][C]")
            Append("Mei:[E|Blush] Yes, we just need to su- [B][C]")
            Append("Florentina:[E|Surprise] That's enough![P] Next question![B][C]")
        
        --Any other form:
        else
            Append("Karina: So, how do bees get along with the Alraunes?[B][C]")
            Append("Mei:[E|Smirk] I'd say Florentina and I get along famously.[B][C]")
            Append("Florentina:[E|Neutral] You're not a bee right now, numbnuts.[B][C]")
            Append("Mei:[E|Sad] Do you have to rub it in?[B][C]")
            Append("Karina: But seriously...[B][C]")
            Append("Mei:[E|Neutral] They mostly leave us alone, and we leave them alone.[P] They are good sources of nectar when they want to be, but they are often obstinate and not worth the risk.[B][C]")
            Append("Karina: Wait, the Alraunes themselves?[P] You can derive nectar from them?[B][C]")
            Append("Mei:[E|Blush] Yes, we just need to su- [B][C]")
            Append("Florentina:[E|Surprise] That's enough![P] Next question![B][C]")
        end
    
    -- |[No Florentina]|
    else
        Append("Karina: So, how do bees get along with the Alraunes?[B][C]")
        Append("Mei:[E|Neutral] They mostly leave us alone, and we leave them alone.[P] They are good sources of nectar when they want to be, but they are often obstinate and not worth the risk.[B][C]")
        Append("Karina: Wait, the Alraunes themselves?[P] You can derive nectar from them?[B][C]")
        Append("Mei:[E|Blush] Yes, we just need to su- [B][C]")
        Append("Mei:[E|Blush] Uh, well, you know...[B][C]")
    
    end
end
