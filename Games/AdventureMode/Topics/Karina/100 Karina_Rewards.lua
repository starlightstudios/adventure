-- |[Rewards]|
--Karina asks you about what she could reward the hive with.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Karina")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--Bee form:
	if(sMeiForm == "Bee") then
		Append("Karina: What can I get you to act as a reward for answering my questions?[B][C]")
		Append("Mei:[E|Neutral] One moment...[B][C]")
		Append("Mei:[E|Neutral] Yes.[P] Yes.[P] Of course.[P] No.[B][C]")
		Append("Mei:[E|Neutral] I see.[B][C]")
		Append("Mei:[E|Neutral] We want for three things, human.[P] Nectar, recruits, and security.[B][C]")
		Append("Mei:[E|Neutral] Can you provide us any of those?[B][C]")
		Append("Karina: Perhaps I could broker an agreement.[P] You could gather nectar from the plants on the farm here, at certain times, and the mercs wouldn't stop you.[B][C]")
		Append("Karina: Of course, we'd have to clear out for a bit.[P] I don't trust the hive not to grab someone...[B][C]")
		Append("Mei:[E|Neutral] ...[B][C]")
		Append("Mei:[E|Neutral] Yes.[P] It is less than ideal.[P] It is better than the current standoff.[B][C]")
		Append("Mei:[E|Neutral] We are at consensus.[P] Your deal is tentatively accepted.[B][C]")
		Append("Karina: I'll see if I can talk the Lieutenant into it![B][C]")
		Append("Mei:[E|Neutral] Yes.[P] I understand.[B][C]")
		Append("Mei:[E|Neutral] It seems the hive will choose another representative.[P] My mission is of higher priority.[P] We will leave written instructions for you.[B][C]")
		Append("Karina: Can't any of the other bees speak for the hive?[B][C]")
		Append("Mei:[E|Neutral] I'm afraid not.[P] They are incapable of speech in the conventional sense, as they lack the internal conception of individuality.[P] If any of them were to speak, they would all speak in unison.[B][C]")
		Append("Mei:[E|Neutral] I can speak, because I am...[P] different...[B][C]")
	
	--Any other form.
	else
		Append("Karina: What can I get you to act as a reward for answering my questions?[B][C]")
		Append("Mei:[E|Neutral] Well, I'd need to consult with the hive.[P] But, I can't do that without my feelers.[B][C]")
		Append("Mei:[E|Neutral] So, erm, I'll go get them and get back to you.[B][C]")
	end
end
