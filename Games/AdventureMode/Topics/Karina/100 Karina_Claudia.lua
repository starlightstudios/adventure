-- |[Claudia]|
--Dialogue.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Karina")

--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
	Append("Karina: Mei![P] Sister Claudia says that she has you to thank for saving her![B][C]")
	Append("Claudia: Indeed.[P] Thank you, Mei.[B][C]")
	Append("Mei:[E|Neutral] It was nothing.[P] Really.[B][C]")
	Append("Claudia: I do apologize, though.[P] I did mention a possible reward, but I'm afraid Karina is all that is left of my convent.[B][C]")
	Append("Claudia: I'm sorry that I have nothing to offer you.[P] Except, possibly, membership.[B][C]")
	Append("Mei:[E|Neutral] Uhhh, I think I'll pass...[B][C]")
	Append("Karina: You'll always be an honorary Dove![B][C]")
	Append("Claudia: Indeed.[P] Should you encounter another of our order, merely mention the Doves and they will accommodate you.[B][C]")
	Append("Mei:[E|Neutral] Well, that's not so bad.[P] I'll keep it in mind.[B][C]")
end
