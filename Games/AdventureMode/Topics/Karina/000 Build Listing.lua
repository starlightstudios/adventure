-- |[Build Listing]|
--Builds a list of topics this NPC will respond to, and at what levels. Called at Adventure Mode startup.
local sNPCName = "Karina"

--Topic listing.
fnConstructTopic("Karina_Alraunes",    "Alraunes",    1, sNPCName, 0)
fnConstructTopic("Karina_Claudia",     "Claudia",    -1, sNPCName, 0)
fnConstructTopic("Karina_NewBees",     "New Bees",    1, sNPCName, 0)
fnConstructTopic("Karina_HiveMind",    "Hive Mind",   1, sNPCName, 0)
fnConstructTopic("Karina_Rewards",     "Reward",      1, sNPCName, 0)
fnConstructTopic("Karina_PartirHuman", "Partirhuman", 1, sNPCName, 0)
