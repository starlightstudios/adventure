-- |[ ==================================== Hunting Algorithm =================================== ]|
--While not a minigame in the sense that it has a handler routine in the C++, hunting is a part of
-- chapter 2 that is mostly optional and involves some unique mechanics.
--Hunting is done by creating a map of the rooms that construct Trafal and spawning wildlife which
-- move between those rooms, leaving tracks or other clues. These reset each time the player rests.
--This file sets up the initial set of variables at chapter start.
Debug_PushPrint(false, "Initializing Hunt minigame.\n")
local sBasePath = fnResolvePath()

-- |[Constants]|
gciHunt_PreyType_Error = -1
gciHunt_PreyType_Omnigoose = 0
gciHunt_PreyType_Turkerus = 1
gciHunt_PreyType_Brimhog = 2
gciHunt_PreyType_Unielk = 3
gciHunt_PreyType_MBrimhog = 4
gciHunt_PreyType_SUnielk = 5
gciHunt_PreyType_PesudoHen = 6
gciHunt_PreyType_Buneye = 7
gciHunt_PreyType_Total = 8

-- |[Overall Data Object]|
gzaHuntData = {}

-- |[Paths]|
gzaHuntData.sCreateHuntPath = sBasePath .. "200 Create Hunt.lua"

-- |[ ================================== Map and Construction ================================== ]|
--Variables and functions related to the map.
Debug_Print("Setting map prototypes.\n")

-- |[Variables]|
gzaHuntData.zaRoomList = {} --Made of zMapObjects

-- |[Structure Prototypes]|
--Map object, represents a room in the world. Rooms know which slot they are in.
local zMapObject = {}
zMapObject.iSlot = nil
zMapObject.sName = nil
zMapObject.zaConnections = {} --Made of zConnections

--New connection.
local zConnection = {}
zConnection.sName = nil
zConnection.iSlot = nil
zConnection.iDirection = nil --One of gci_Face_North, gci_Face_South, etc

-- |[Function Predefinitions]|
function gzaHuntData.fnGetSlotOfRoom(psRoomName)                       end
function gzaHuntData.fnIsRoomConnectedTo(psRoomNameA, psRoomNameB)     end
function gzaHuntData.fnAddRoom(psRoomName)                             end
function gzaHuntData.fnAddRoomSeries(psPrefix, psStart, psEnd)         end
function gzaHuntData.fnCreateConnection(psRoomNameA, psRoomNameB, ...) end
function gzaHuntData.fnPrintMapDebug()                                 end

-- |[Call Files]|
--These will implement the above functions.
Debug_Print("Compiling map functions.\n")
LM_ExecuteScript(sBasePath .. "001 Map Functions.lua")

-- |[ ========================================== Flags ========================================= ]|
--Set to true and the tutorial will run instead.
gzaHuntData.bIsTutorial = false

-- |[ ======================================== Prey Data ======================================= ]|
--These prototypes represent the various types of prey creatures that can be hunted.
gzaHuntData.zaPreyPrototypes = {}

-- |[Omnigoose]|
local zOmnigoose = {}
zOmnigoose.sName = "Omnigoose"
zOmnigoose.iType = gciHunt_PreyType_Omnigoose
zOmnigoose.iToughness = 0
zOmnigoose.sHuntDrop = "Bird Meat"
zOmnigoose.sSpriteName = "Omnigoose"
zOmnigoose.sCombatEnemy = "Puissant Omnigoose"
table.insert(gzaHuntData.zaPreyPrototypes, zOmnigoose)

-- |[Turkerus]|
local zTurkerus = {}
zTurkerus.sName = "Turkerus"
zTurkerus.iType = gciHunt_PreyType_Turkerus
zTurkerus.iToughness = 0
zTurkerus.sHuntDrop = "Bird Meat"
zTurkerus.sSpriteName = "Turkerus"
zTurkerus.sCombatEnemy = "Clever Turkerus"
table.insert(gzaHuntData.zaPreyPrototypes, zTurkerus)

-- |[Brimhog]|
local zBrimhog = {}
zBrimhog.sName = "Brimhog"
zBrimhog.iType = gciHunt_PreyType_Brimhog
zBrimhog.iToughness = 0
zBrimhog.sHuntDrop = "Slippery Boar Meat"
zBrimhog.sSpriteName = "Brimhog"
zBrimhog.sCombatEnemy = "Rowdy Brimhog"
table.insert(gzaHuntData.zaPreyPrototypes, zBrimhog)

-- |[Unielk]|
local zUnielk = {}
zUnielk.sName = "Unielk"
zUnielk.iType = gciHunt_PreyType_Unielk
zUnielk.iToughness = 0
zUnielk.sHuntDrop = "Mighty Venison"
zUnielk.sSpriteName = "Unielk"
zUnielk.sCombatEnemy = "Unielk Grazer"
table.insert(gzaHuntData.zaPreyPrototypes, zUnielk)

-- |[Macho Brimhog]|
local zMachoBrimhog = {}
zMachoBrimhog.sName = "Macho Brimhog"
zMachoBrimhog.iType = gciHunt_PreyType_MBrimhog
zMachoBrimhog.iToughness = 1
zMachoBrimhog.sHuntDrop = "Bonesaw Hog Meat"
zMachoBrimhog.sSpriteName = "Brimhog"
zMachoBrimhog.sCombatEnemy = "Macho Brimhog"
table.insert(gzaHuntData.zaPreyPrototypes, zMachoBrimhog)

-- |[Slathering Unielk]|
local zSlatheringUnielk = {}
zSlatheringUnielk.sName = "Slathering Unielk"
zSlatheringUnielk.iType = gciHunt_PreyType_SUnielk
zSlatheringUnielk.iToughness = 1
zSlatheringUnielk.sHuntDrop = "Caustic Vension"
zSlatheringUnielk.sSpriteName = "Unielk"
zSlatheringUnielk.sCombatEnemy = "Unielk Slatherer"
table.insert(gzaHuntData.zaPreyPrototypes, zSlatheringUnielk)

-- |[Pseudogriffon Hen]|
local zPseudohen = {}
zPseudohen.sName = "Psuedogriffon Hen"
zPseudohen.iType = gciHunt_PreyType_PesudoHen
zPseudohen.iToughness = 0
zPseudohen.sHuntDrop = "Griffon Egg"
zPseudohen.sSpriteName = "Pseudogriffon"
zPseudohen.sCombatEnemy = "Pseudogriffon Hen"
table.insert(gzaHuntData.zaPreyPrototypes, zPseudohen)

-- |[Buneye]|
local zBuneye = {}
zBuneye.sName = "Buneye"
zBuneye.iType = gciHunt_PreyType_Buneye
zBuneye.iToughness = 0
zBuneye.sHuntDrop = "Buneye Fluff"
zBuneye.sSpriteName = "BunEye"
zBuneye.sCombatEnemy = "Evasive Buneye"
table.insert(gzaHuntData.zaPreyPrototypes, zBuneye)

-- |[ ==================================== Prey and Pathing ==================================== ]|
--Variables and functions related to the prey and how they move around the map.
Debug_Print("Setting prey prototypes.\n")

-- |[Variables]|
gzaHuntData.zaPreyList = {} --Made of zPreys

-- |[Structure Prototypes]|
--Represents a creature the player can hunt.
local zPrey = {}
zPrey.bDisabled = nil
zPrey.sName = nil
zPrey.iType = nil
zPrey.sCurLocation = nil
zPrey.iCurLocation = nil
zPrey.iClueRoll = nil
zPrey.zaPathList = {} --Made of zMovements
zPrey.sLootDrop = nil

--Represents a movement made by a prey. The name is the room it moved to.
local zMovement = {}
zMovement.sName = nil
zMovement.iSlot = nil
zMovement.iDirection = nil

-- |[Function Predefinitions]|
function gzaHuntData.fnWipePrey()                                     end
function gzaHuntData.fnGetSlotOfPrey(psName)                          end
function gzaHuntData.fnGetPreyPrototype(piPrototypeCode)              end
function gzaHuntData.fnAddPrey(psName, psType, psSpawnRoom)           end
function gzaHuntData.fnPathPrey(psName, piMinDistance, piMaxDistance) end
function gzaHuntData.fnPathPreyManually(psName, psaPathNames)         end

-- |[Call Files]|
--These will implement the above functions.
Debug_Print("Compiling prey functions.\n")
LM_ExecuteScript(sBasePath .. "002 Prey Functions.lua")

-- |[ ================================= Construction Sequence ================================== ]|
--Once all variables and functions are initialized, the construction sequence will create the 
-- individual rooms and connections.
Debug_Print("Running construction routine.\n")
LM_ExecuteScript(sBasePath .. "100 Construction.lua")
LM_ExecuteScript(sBasePath .. "900 World Functions.lua")

-- |[ ========================================== Debug ========================================= ]|
Debug_PopPrint("Finished Hunt initialization.\n")
