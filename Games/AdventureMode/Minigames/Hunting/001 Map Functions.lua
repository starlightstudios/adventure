-- |[ ====================================== Map Functions ===================================== ]|
--Functions related to the map, including queries and builders.

-- |[ ==================================== Property Queries ==================================== ]|
-- |[fnGetSlotOfRoom]|
--Returns the index in the zaRoomList of the named room. Returns -1 if not found.
function gzaHuntData.fnGetSlotOfRoom(psRoomName)

    -- |[Error Check]|
    local this = gzaHuntData
    if(psRoomName == nil) then return -1 end
    
    -- |[Iterate]|
    for i = 1, #this.zaRoomList, 1 do
        if(this.zaRoomList[i].sName == psRoomName) then
            return i
        end
    end
    
    --Not found.
    return -1
end

-- |[fnIsRoomConnectedTo]|
--Returns true if there is a connection from RoomA to RoomB. Does not check if RoomB leads to RoomA.
-- Returns false if there is no connection.
function gzaHuntData.fnIsRoomConnectedTo(psRoomNameA, psRoomNameB)
    
    -- |[Error Check]|
    local this = gzaHuntData
    if(psRoomNameA == nil) then return false end
    if(psRoomNameB == nil) then return false end
    
    -- |[Locate]|
    local iSlotA = this.fnGetSlotOfRoom(psRoomNameA)
    if(iSlotA == -1) then return false end
    
    -- |[Scan]|
    for i = 1, #this.zaRoomList[iSlotA].zaConnections, 1 do
        if(this.zaRoomList[iSlotA].zaConnections[i].sName == psRoomNameB) then
            return true
        end
    end
    
    --Not found.
    return false
end

-- |[ ====================================== Manipulators ====================================== ]|
-- |[fnAddRoom]|
--Adds a new room onto the end of the list.
function gzaHuntData.fnAddRoom(psRoomName)

    -- |[Error Check]|
    local this = gzaHuntData
    if(psRoomName == nil) then return end
    
    -- |[Duplicate Check]|
    --Room names must be unique.
    local iExistingSlot = this.fnGetSlotOfRoom(psRoomName)
    if(iExistingSlot ~= -1) then return end

    -- |[Create and Register]|
    --Create.
    local zMapObject = {}
    zMapObject.iSlot = -1
    zMapObject.sName = psRoomName
    zMapObject.zaConnections = {}
    
    --Add to the end.
    table.insert(this.zaRoomList, zMapObject)
    
    --Resolve the slot the room was inserted into and store it inside the object.
    zMapObject.iSlot = this.fnGetSlotOfRoom(psRoomName)
end

-- |[fnAddRoomSeries]|
--Adds a series of rooms, starting at the first letter and iterating to the last one. For example:
-- "A" to "D" creates RoomA, RoomB, RoomC, and RoomD.
--Note that if the end letter is before the start letter, the loop will run all the way to 'Z'.
function gzaHuntData.fnAddRoomSeries(psPrefix, psStart, psEnd)

    -- |[Error Check]|
    --Verify arguments.
    local this = gzaHuntData
    if(psPrefix == nil) then return end
    if(psStart  == nil) then return end
    if(psEnd    == nil) then return end
    
	--Setup.
	local sCurrentLetter = psStart
	
	--Range check. If the letter is out of the normal range, fail.
	local iByteValue = string.byte(sCurrentLetter)
	if(iByteValue < string.byte("A") or iByteValue > string.byte("Z")) then return end
	
	--Iterate across the letters. Always stops on Z. Also always stops on non-capitals.
	while(true) do
		
		--Add the remap.
		local sLevelName = psPrefix .. sCurrentLetter
        this.fnAddRoom(sLevelName)
		
		--If this was the ending letter, or the letter 'Z', stop.
		if(sCurrentLetter == psEnd or sCurrentLetter == "Z") then break end
		
		--Move to the next letter.
		local iByteValue = string.byte(sCurrentLetter)
		sCurrentLetter = string.char(iByteValue + 1)
	end
end

-- |[fnCreateConnection]|
--Creates a connection between the two or more provided rooms. Only one is mandatory.
function gzaHuntData.fnCreateConnection(psRoomNameA, psRoomNameB, ...)
    
    -- |[Error Check]|
    local this = gzaHuntData
    if(psRoomNameA == nil) then return end
    if(psRoomNameB == nil) then return end
    
    -- |[Divide]|
    --The B room is expected to be "Roomname|Direction" where direction is N, NE, E, SE, etc.
    -- Split the strings up here.
    local iBarSlot   = string.find(psRoomNameB, "|")
    local sRoomNameB = string.sub(psRoomNameB, 1, iBarSlot-1)
    local sRoomDirB  = string.sub(psRoomNameB, iBarSlot+1)
    
    -- |[Locate]|
    local iSlotA = this.fnGetSlotOfRoom(psRoomNameA)
    local iSlotB = this.fnGetSlotOfRoom(sRoomNameB)
    
    --If either was not found, fail.
    if(iSlotA == -1 or iSlotB == -1) then return end
    
    --If they are somehow the same slot, fail.
    if(iSlotA == iSlotB) then return end
    
    -- |[A to B, B to A]|
    --Check Room A to see if it is already connected. If not, add a connection.
    if(this.fnIsRoomConnectedTo(psRoomNameA, sRoomNameB) == false) then
        
        --New connection.
        local zConnection = {}
        zConnection.sName = sRoomNameB
        zConnection.iSlot = iSlotB
        zConnection.iDirection = fnGetDirCodeFromString(sRoomDirB)
        
        --Add it.
        table.insert(this.zaRoomList[iSlotA].zaConnections, zConnection)
    end
    
    --Check Room B to see if it is already connected. If not, add a connection.
    if(this.fnIsRoomConnectedTo(sRoomNameB, psRoomNameA) == false) then
        
        --New connection.
        local zConnection = {}
        zConnection.sName = psRoomNameA
        zConnection.iSlot = iSlotA
        zConnection.iDirection = fnGetRevDirCodeFromString(sRoomDirB)
        
        --Add it.
        table.insert(this.zaRoomList[iSlotB].zaConnections, zConnection)
    end

    -- |[Additional Arguments]|
    --All additional arguments follow the same set of logic.
    for i = 1, select("#", ...), 1 do
        
        --Resolve the name.
        local sRoomBaseN = select(i, ...)
        
        --Split it.
        local iBarSlot   = string.find(sRoomBaseN, "|")
        local sRoomNameN = string.sub(sRoomBaseN, 1, iBarSlot-1)
        local sRoomDirN  = string.sub(sRoomBaseN, iBarSlot+1)
        
        --Find the slot. Make sure it exists and is not the starting room.
        local iSlotN = this.fnGetSlotOfRoom(sRoomNameN)
        if(iSlotN ~= -1 or iSlotN == iSlotA) then
        
            --Make sure the A->N is not already connected.
            if(this.fnIsRoomConnectedTo(psRoomNameA, sRoomNameN) == false) then
                
                --New connection.
                local zConnection = {}
                zConnection.sName = sRoomNameN
                zConnection.iSlot = iSlotN
                zConnection.iDirection = fnGetDirCodeFromString(sRoomDirN)
                
                --Add it.
                table.insert(this.zaRoomList[iSlotA].zaConnections, zConnection)
            end
        
            --Make sure the N->A is not already connected.
            if(this.fnIsRoomConnectedTo(sRoomNameN, psRoomNameA) == false) then
                
                --New connection.
                local zConnection = {}
                zConnection.sName = psRoomNameA
                zConnection.iSlot = iSlotA
                zConnection.iDirection = fnGetRevDirCodeFromString(sRoomDirN)
                
                --Add it.
                table.insert(this.zaRoomList[iSlotN].zaConnections, zConnection)
            end
        end
    end
end

-- |[ ====================================== Core Methods ====================================== ]|
-- |[fnPrintMapDebug]|
--Debug function, prints the map data to the console for perusal.
function gzaHuntData.fnPrintMapDebug()
    --TODO
end

