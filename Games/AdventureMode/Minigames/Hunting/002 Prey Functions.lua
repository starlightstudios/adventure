-- |[ ===================================== Prey Functions ===================================== ]|
--Functions related to the spawning and movement of prey entities.

-- |[ ==================================== System Functions ==================================== ]|
-- |[fnWipePrey]|
--Wipes all prey data.
function gzaHuntData.fnWipePrey()
    gzaHuntData.zaPreyList = {}
end

-- |[ ==================================== Property Queries ==================================== ]|
-- |[fnGetSlotOfPrey]|
--Returns the slot a named prey occupies, or -1 if not found.
function gzaHuntData.fnGetSlotOfPrey(psName)
    
    -- |[Error Check]|
    --Verify arguments.
    local this = gzaHuntData
    if(psName == nil) then return -1 end
    
    -- |[Scan]|
    for i = 1, #this.zaPreyList, 1 do
        if(this.zaPreyList[i].sName == psName) then
            return i
        end
    end
    
    --Not found.
    return -1
end

-- |[fnGetPreyPrototype]|
--Returns a prototype structure for a prey, given its type.
function gzaHuntData.fnGetPreyPrototype(piPrototypeCode)

    -- |[Error Check]|
    --Verify arguments.
    local this = gzaHuntData
    if(piPrototypeCode == nil) then return nil end

    -- |[Range Check]|
    if(piPrototypeCode <= gciHunt_PreyType_Error or piPrototypeCode >= gciHunt_PreyType_Total) then
        io.write("Warning: fnGetPreyPrototype() - Prototype " .. piPrototypeCode .. " is out of range.\n")
        return gzaHuntData.zaPreyPrototypes[1]
    end

    -- |[Scan]|
    for i = 1, #gzaHuntData.zaPreyPrototypes, 1 do
        if(gzaHuntData.zaPreyPrototypes[i].iType == piPrototypeCode) then
            return gzaHuntData.zaPreyPrototypes[i]
        end
    end

    -- |[Not Found]|
    io.write("Warning: fnGetPreyPrototype() - Prototype " .. piPrototypeCode .. " was not found.\n")
    return gzaHuntData.zaPreyPrototypes[1]
end

-- |[ ====================================== Manipulators ====================================== ]|
-- |[fnAddPrey]|
--Spawns a new prey enemy at the given location, assuming it exists.
function gzaHuntData.fnAddPrey(psName, piType, psSpawnRoom)
    
    -- |[Error Check]|
    --Verify arguments.
    local this = gzaHuntData
    if(psName      == nil) then return end
    if(piType      == nil) then return end
    if(psSpawnRoom == nil) then return end
    
    --Debug.
    Debug_Print("Spawning prey: " .. psName .. " " .. piType .. " " .. psSpawnRoom .. "\n")
    
    --Make sure the type is within range.
    if(piType <= gciHunt_PreyType_Error or piType >= gciHunt_PreyType_Total) then
        io.write("fnAddPrey(): Error, type " .. piType .. " is out of range.\n")
        return
    end
    
    -- |[Location Resolve]|
    --Check the map to make sure the named room actually exists.
    local iRoom = this.fnGetSlotOfRoom(psSpawnRoom)
    if(iRoom == -1) then
        io.write("fnAddPrey(): Error, room " .. psSpawnRoom .. " did not exist.\n")
        return
    end
    
    -- |[Prototype Resolve]|
    --Figure out which prototype this entity uses.
    local zUsePrototype = gzaHuntData.fnGetPreyPrototype(piType)
    
    -- |[Create and Register]|
    --Create.
    local zPrey = {}
    zPrey.bDisabled = false
    zPrey.sName = psName
    zPrey.iType = piType
    zPrey.sCurLocation = psSpawnRoom
    zPrey.iCurLocation = iRoom
    zPrey.iClueRoll = LM_GetRandomNumber(0, 1000)
    zPrey.zaPathList = {}
    zPrey.sLootDrop = zUsePrototype.sHuntDrop
    
    --Register.
    table.insert(this.zaPreyList, zPrey)
    Debug_Print("Finished adding a prey.\n")
    
end

-- |[fnPathPrey]|
--Causes the prey to path around the map randomly. The prey weights rolls against where it has already
-- been, but may sometimes backtrack anyway by sheer coincidence or necessity.
function gzaHuntData.fnPathPrey(psName, piMinDistance, piMaxDistance)
    
    -- |[Error Check]|
    --Verify arguments.
    local this = gzaHuntData
    if(psName        == nil) then return end
    if(piMinDistance == nil) then return end
    if(piMaxDistance == nil) then return end
    
    --Make sure the given prey exists.
    local iPreySlot = this.fnGetSlotOfPrey(psName)
    if(iPreySlot == -1) then
        io.write("fnPathPrey(): Error, no prey creature named " .. psName .. " found.\n")
        return
    end
    
    --Make sure the distance is nonzero, and that the min/max distances make sense.
    if(piMinDistance > piMaxDistance) then piMinDistance = piMaxDistance end
    if(piMaxDistance < 1) then
        io.write("fnPathPrey(): Error, invalid distance " .. piMaxDistance .. ".\n")
        return
    end
    
    -- |[Constants]|
    local ciBaseRollValue = 100
    local cfRepeatRoomFactor = 0.10
    
    -- |[Distance Roll]|
    --Figure out how far we want to path. If the min and max distances are identical, then exactly that many moves will be made.
    local iDistance = LM_GetRandomNumber(piMinDistance, piMaxDistance)
    
    -- |[Setup]|
    --Get where the prey currently is.
    local sCurLocation = this.zaPreyList[iPreySlot].sCurLocation
    local iCurLocation = this.zaPreyList[iPreySlot].iCurLocation
    
    --Debug.
    --io.write("Begin prey pathing. Start point: " .. sCurLocation .. "\n")
    --io.write(" Path distance: " .. iDistance .. "\n")
    
    -- |[Begin Pathing]|
    for i = 1, iDistance, 1 do
    
        -- |[Setup]|
        local iTotalWeight = 0
        --io.write("  =>Path Begins at  " .. sCurLocation .. "\n")
    
        -- |[Assemble List of Locations]|
        --Create a list of all locations we can go to from here, and their weights.
        local zaPathList = {}
        for p = 1, #this.zaRoomList[iCurLocation].zaConnections, 1 do

            --Create entry. Set default weight to 100.
            local zEntry = {}
            zEntry.sName = this.zaRoomList[iCurLocation].zaConnections[p].sName
            zEntry.iSlot = this.zaRoomList[iCurLocation].zaConnections[p].iSlot
            zEntry.iDir  = this.zaRoomList[iCurLocation].zaConnections[p].iDirection
            zEntry.iWeight = ciBaseRollValue

            --Default weight is 100.
            table.insert(zaPathList, zEntry)
            
            --Search across the existing path list. If this room has already been visited, then halve its weight
            -- for each time it was visited.
            for c = 1, #this.zaPreyList[iPreySlot].zaPathList, 1 do
                if(this.zaPreyList[iPreySlot].zaPathList[c].iSlot == zEntry.iSlot) then
                    zEntry.iWeight = math.floor(zEntry.iWeight * cfRepeatRoomFactor)
                end
            end
            
            --Add the final weight to the sum.
            iTotalWeight = iTotalWeight + zEntry.iWeight
        end
        
        -- |[Error]|
        --If the total weight was zero, then something went wrong.
        if(iTotalWeight < 1 or #zaPathList < 1) then return end
        
        --Debug.
        --io.write("     Locations available: " .. #zaPathList .. "\n")
        --for p = 1, #zaPathList, 1 do
            --io.write("     " .. zaPathList[p].sName .. " x " .. zaPathList[p].iWeight .. "\n")
        --end
    
        -- |[Roll By Weight]|
        --Roll by the total weight and select which room to path to.
        local iSlot = 1
        local iTotalRoll = LM_GetRandomNumber(1, iTotalWeight)
        
        --Resolve which room to enter and path there.
        while(true) do
            
            --Subtract.
            iTotalRoll = iTotalRoll - zaPathList[iSlot].iWeight
            
            --Check for ending case:
            if(iTotalRoll < 1 or zaPathList[iSlot] == nil) then
                break
            end
            
            --Otherwise, next entry.
            iSlot = iSlot + 1
        end
        
        --We should now have a valid room to path to. First, store the current room in the pathing list.
        local zMapObject = this.zaRoomList[iCurLocation]
        local zMovement = {}
        zMovement.sName = zMapObject.sName
        zMovement.iSlot = zMapObject.iSlot
        zMovement.iDirection = zaPathList[iSlot].iDir
        table.insert(this.zaPreyList[iPreySlot].zaPathList, zMovement)
        
        --Move the prey's current location to the target.
        this.zaPreyList[iPreySlot].sCurLocation = zaPathList[iSlot].sName
        this.zaPreyList[iPreySlot].iCurLocation = zaPathList[iSlot].iSlot
        sCurLocation = this.zaPreyList[iPreySlot].sCurLocation
        iCurLocation = this.zaPreyList[iPreySlot].iCurLocation
        --io.write("     Prey paths to " .. this.zaPreyList[iPreySlot].sCurLocation .. "\n")
    end
    
    --Debug.
    --io.write("End prey pathing.\n")
end

-- |[fnPathPreyNest]|
--Causes all map spaces adjacent to the starting room to show tracks pointing to the nest.
function gzaHuntData.fnPathPreyNest(psName)
    
    -- |[Error Check]|
    --Verify arguments.
    local this = gzaHuntData
    if(psName == nil) then return end
    
    --Make sure the given prey exists.
    local iPreySlot = this.fnGetSlotOfPrey(psName)
    if(iPreySlot == -1) then
        io.write("fnPathPreyNest(): Error, no prey creature named " .. psName .. " found.\n")
        return
    end
    
    -- |[Setup]|
    --Get where the prey currently is.
    local sCurLocation = this.zaPreyList[iPreySlot].sCurLocation
    local iCurLocation = this.zaPreyList[iPreySlot].iCurLocation
    
    -- |[Assemble List of Locations]|
    --Create a list of all locations we can go to from here, and their weights.
    local zaPathList = {}
    for p = 1, #this.zaRoomList[iCurLocation].zaConnections, 1 do

        --Fast-access pointer.
        local zTargetRoom = this.zaRoomList[iCurLocation].zaConnections[p]
        local iTargetSlot = zTargetRoom.iSlot
        
        --Add the prey's clue indicator to the adjacent room. Note: Direction is inverted, as this is not
        -- the prey moving from the nest, it's rooms adjacent to the nest moving to the nest.
        local zMapObject = this.zaRoomList[iCurLocation]
        local zMovement = {}
        zMovement.sName = zTargetRoom.sName
        zMovement.iSlot = zTargetRoom.iSlot
        zMovement.iDirection = fnGetReverseDirFromCode(zTargetRoom.iDirection)
        table.insert(this.zaPreyList[iPreySlot].zaPathList, zMovement)
        
    end

end

-- |[fnPathPreyManually]|
--Sets a specific path for a prey. Used for story-specific moments.
function gzaHuntData.fnPathPreyManually(psName, psaPathNames)
    
    -- |[Error Check]|
    --Verify arguments.
    local this = gzaHuntData
    if(psName       == nil) then return end
    if(psaPathNames == nil) then return end
    
    --Make sure the given prey exists.
    local iPreySlot = this.fnGetSlotOfPrey(psName)
    if(iPreySlot == -1) then
        io.write("fnPathPreyManually(): Error, no prey creature named " .. psName .. " found.\n")
        return
    end
    
    -- |[Room Check]|
    --Check every room in the provided array. They all have to exist.
    for i = 1, #psaPathNames, 1 do
        local iCheckSlot = this.fnGetSlotOfRoom(psaPathNames[i])
        if(iCheckSlot == -1) then
            io.write("fnPathPreyManually(): Error, room " .. psaPathNames[i] .. " does not exist.\n")
            return
        end
    end
    
    -- |[Setup]|
    --Get where the prey currently is.
    local sCurLocation = this.zaPreyList[iPreySlot].sCurLocation
    local iCurLocation = this.zaPreyList[iPreySlot].iCurLocation
    
    -- |[Begin Pathing]|
    for i = 1, #psaPathNames, 1 do
    
        --Get the slot of the target room.
        local iCheckSlot = this.fnGetSlotOfRoom(psaPathNames[i])
    
        --Store the current location in the path array.
        local zMapObject = this.zaRoomList[iCurLocation]
        local zMovement = {}
        zMovement.sName = zMapObject.sName
        zMovement.iSlot = zMapObject.iSlot
        zMovement.iDirection = 0
        
        --Locate the direction between the two rooms.
        for p = 1, #zMapObject.zaConnections, 1 do
            if(zMapObject.zaConnections[p].sName == psaPathNames[i]) then
                zMovement.iDirection = zMapObject.zaConnections[p].iDirection
            end
        end
        
        --Insert it.
        table.insert(this.zaPreyList[iPreySlot].zaPathList, zMovement)
        
        --Move the prey's current location to the target.
        this.zaPreyList[iPreySlot].sCurLocation = psaPathNames[i]
        this.zaPreyList[iPreySlot].iCurLocation = iCheckSlot
        sCurLocation = this.zaPreyList[iPreySlot].sCurLocation
        iCurLocation = this.zaPreyList[iPreySlot].iCurLocation
    end

end