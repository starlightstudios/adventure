-- |[ ===================================== World Functions ==================================== ]|
--Functions used when running constructors and spawning objects. All maps share the same basic set
-- of objects and calls.
local bShowConstructorDebug = false

-- |[ ===================================== World Functions ==================================== ]|
function fnGetAsciiSum(psWord)
    
    -- |[Error Checking]|
    if(psWord == nil) then return 0 end
    
    -- |[Iterate]|
    local iSum = 0
    for i = 1, string.len(psWord), 1 do
        local sLetter = string.sub(psWord, i, i)
        local iValue = string.byte(sLetter)
        iSum = iSum + iValue
    end
    
    --Finish up.
    return iSum
end

-- |[ =================================== fnHuntConstructor() ================================== ]|
--Called in the constructor of any map that handles hunts. Spawns tracks or prey if any are
-- present. Queries an already created level, so should be called in the second half of the
-- constructor near NPC spawning.
function fnHuntConstructor()
    io.write("Warning. Deprecated call to fnHuntConstructor() at " .. LM_GetCallStack(0) .. "\n")
    
    -- |[ =================== Setup ==================== ]|
    -- |[Error Checking]|
    --Debug.
    Debug_PushPrint(bShowConstructorDebug, "== Running fnHuntConstructor ==\n")
    
    -- |[Variables]|
    --Fast-access pointers and setup.
    local this = gzaHuntData
    local sRoomName = AL_GetProperty("Name")
    local zaClueList = {}
    local zaPreyList = {}
    Debug_Print(" Room name: " .. sRoomName .. "\n")
    Debug_Print(" Prey list size: " .. #this.zaPreyList .. "\n")
    
    -- |[ ================= Prey Check ================= ]|
    --Check each prey. At least one must path through this room to spawn a clue. Dead prey do
    -- not spawn anything. Create a list of all prey that path here.
    for i = 1, #this.zaPreyList, 1 do
        
        --Debug.
        Debug_Print(" Scanning prey " .. i .. "\n")
        
        --Direct match. Prey is in this room. Cannot be disabled.
        if(this.zaPreyList[i].sCurLocation == sRoomName and this.zaPreyList[i].bDisabled == false) then
            Debug_Print("  Direct match. Prey is present.\n")
            table.insert(zaPreyList, this.zaPreyList[i])
            zaPreyList.iOriginalIndex = i
            
        --Scan all of the rooms in their path. If any match, stop iterating.
        else
        
            for p = 1, #this.zaPreyList[i].zaPathList, 1 do
                Debug_Print("  Comparing: " .. this.zaPreyList[i].zaPathList[p].sName .. " to " .. sRoomName .. "\n")
                if(this.zaPreyList[i].zaPathList[p].sName == sRoomName) then
                    Debug_Print("   Match.\n")
                    table.insert(zaPreyList, this.zaPreyList[i])
                    zaPreyList.iOriginalIndex = i
                    break
                end
            end
        end
    end
    
    --No prey path in this room.
    if(#zaPreyList < 1) then
        Debug_PopPrint("No prey pathing through this room.\n")
        return
    end
    
    --Debug.
    Debug_Print(" Prey pathing through this room: " .. #zaPreyList .. "\n")
    
    -- |[ =============== Position Check =============== ]|
    --Iterate across the positions that can exist. Store the ones that do in a table.
    for i = 0, 9, 1 do
        if(AL_GetProperty("Position Exists", "Clue"..i) == true) then
            local fClueX, fClueY = AL_GetProperty("Position Location", "Clue"..i)
            local zStruct = {"Clue"..i, fClueX, fClueY}
            table.insert(zaClueList, zStruct)
        end
    end
    for i = 0, 25, 1 do
        local sName = "Clue" .. string.char(97 + i)
        if(AL_GetProperty("Position Exists", sName) == true) then
            local fClueX, fClueY = AL_GetProperty("Position Location", sName)
            local zStruct = {sName, fClueX, fClueY}
            table.insert(zaClueList, zStruct)
        end
    end
    
    --If no clues exist, the prey cannot spawn here or leave clues.
    if(#zaClueList < 1) then
        Debug_PopPrint("No clues in this room.\n")
        return
    end
    
    --Debug.
    Debug_Print(" Clues in this room: " .. #zaClueList .. "\n")
    
    -- |[ ================ Clue Spawning =============== ]|
    --For each prey that paths through this room, check if we should spawn a clue or the prey itself.
    for i = 1, #zaPreyList, 1 do
        
        -- |[Debug]|
        Debug_Print(" => Scan prey " .. i .. ": " .. zaPreyList[i].sName .. "\n")
        Debug_Print(" Prey location: " .. zaPreyList[i].sCurLocation .. "\n")
        
        -- |[Shot Check]|
        --Check the datalibrary if the prey has already been shot. If so, disable it. This can happen if the player shoots a prey,
        -- saves, loads, but doesn't rest to reset prey spawning.
        local iOriginalIndex = zaPreyList.iOriginalIndex
        local iHasBeenShot = VM_GetVar("Root/Variables/Chapter2/HuntTracker/" .. iOriginalIndex, "N")
        if(iHasBeenShot == 1.0) then
            zaPreyList[i].bDisabled = true
        end
        
        -- |[Prey is Present]|
        --Prey is in this room. Spawn them.
        if(zaPreyList[i].sCurLocation == sRoomName and zaPreyList[i].bDisabled == false) then
            Debug_Print("Prey is in the room right now.\n")
        
            --Resolve the clue index we should use. This is done by adding the clue roll to the ASCII value of the letters of the room.
            -- This makes every room use a pseudorandom clue spawn.
            Debug_Print("  Roll from structure: " .. zaPreyList[i].iClueRoll .. "\n")
            Debug_Print("  Roll from name: " .. fnGetAsciiSum(sRoomName) .. "\n")
            local iFinalRoll = zaPreyList[i].iClueRoll + fnGetAsciiSum(sRoomName)
        
            --To figure out which position to spawn the clue at, use the modulo of the total clue spawns vs the roll.
            local iSlot = math.floor((iFinalRoll % #zaClueList) + 1)
            Debug_Print("  Resolved to use the clue in slot " .. iSlot .. "\n")
            
            --Get values.
            local sClueName = zaClueList[iSlot][1]
            local fClueX    = zaClueList[iSlot][2]
            local fClueY    = zaClueList[iSlot][3]
    
            --Figure out which prototype this entity uses.
            local zUsePrototype = gzaHuntData.fnGetPreyPrototype(zaPreyList[i].iType)
            
            --Spawn.
            TA_Create(zaPreyList[i].sName)
                TA_SetProperty("Position", fClueX, fClueY)
                TA_SetProperty("Facing", gci_Face_South)
                TA_SetProperty("Clipping Flag", false)
                TA_SetProperty("Activation Script", gsRoot .. "Minigames/Hunting/901 Dialogue.lua")
                fnSetCharacterGraphics("Root/Images/Sprites/" .. zUsePrototype.sSpriteName .. "/", false)
                TA_SetProperty("Wipe Special Frames")
                TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/" .. zUsePrototype.sSpriteName .. "|Wounded")
                TA_SetProperty("Activate Enemy Mode", zaPreyList[i].sName)
                TA_SetProperty("Add Combat Enemy", zUsePrototype.sCombatEnemy)
                TA_SetProperty("Responds To Shot", true)
                TA_SetProperty("Toughness", zUsePrototype.iToughness)
                
                --Special: If this is a buyeye, give it 360 degree vision.
                if(zUsePrototype.sSpriteName == "BunEye") then
                    TA_SetProperty("Detection Angle", 361)
                end
                
            DL_PopActiveObject()

            --Remove the clue so it won't be used again.
            table.remove(zaClueList, iSlot)
        end
    
        -- |[Prey Left A Clue]|
        --Iterate across the pathing list. Each time the prey entered this room, it left a clue. Note that it is possible for a prey to leave
        -- a clue for the room it is in.
        for p = 1, #zaPreyList[i].zaPathList, 1 do
            if(zaPreyList[i].zaPathList[p].sName == sRoomName) then
                
                --Resolve the clue index we should use. This is done by adding the clue roll to the ASCII value of the letters of the room.
                -- This makes every room use a pseudorandom clue spawn.
                Debug_Print("  Roll from structure: " .. zaPreyList[i].iClueRoll .. "\n")
                Debug_Print("  Roll from name: " .. fnGetAsciiSum(sRoomName) .. "\n")
                local iFinalRoll = zaPreyList[i].iClueRoll + fnGetAsciiSum(sRoomName)
            
                --To figure out which position to spawn the clue at, use the modulo of the total clue spawns vs the roll.
                local iSlot = math.floor((iFinalRoll % #zaClueList) + 1)
                Debug_Print("  Resolved to use the clue in slot " .. iSlot .. "\n")
            
                --Get the direction the prey went.
                local iDirection = zaPreyList[i].zaPathList[p].iDirection
                Debug_Print("  Name is " .. zaPreyList[i].sName .. "\n")
                Debug_Print("  Direction is " .. iDirection .. "\n")
            
                --Spawn it.
                fnSpawnClue(zaPreyList[i].sName, iDirection, zaClueList, iSlot)
                
                --Remove the clue so it won't be used again.
                table.remove(zaClueList, iSlot)
                
            end
            
            --If we ran out of clue slots, stop iterating.
            if(#zaClueList < 1) then
                io.write("Out of clues, stopping (internal).\n")
                break
            end
        end
        
        --If we ran out of clue slots, stop iterating.
        if(#zaClueList < 1) then
            io.write("Out of clues, stopping (external).\n")
            break
        end
    end
    
    -- |[Debug]|
    Debug_PopPrint("Finished running handler.\n")
end

-- |[ ====================================== fnSpawnClue() ===================================== ]|
--Given a prey name and direction, spawns a clue in a the provided clue slot.
function fnSpawnClue(psName, piDirection, pzaClueList, piClueSlot)
    
    -- |[Error Check]|
    --Verify arguments.
    if(psName      == nil) then return end
    if(piDirection == nil) then return end
    if(pzaClueList == nil) then return end
    if(piClueSlot  == nil) then return end
    
    --Make sure the clue list has the required clue.
    if(#pzaClueList < 1 or #pzaClueList < piClueSlot) then return end
        
    -- |[Clue Info]|
    --Store the information of this clue.
    local i = piClueSlot
    local sClueName = pzaClueList[i][1]
    local fClueX    = pzaClueList[i][2]
    local fClueY    = pzaClueList[i][3]
    Debug_Print("  " .. sClueName .. ": " .. fClueX .. "x" .. fClueY .. "\n")

    --Direction string.
    local sDirString = fnGetStringFromDirCode(piDirection)
    local sUseFrame = "Root/Images/Sprites/Tracks/Tracks"..sDirString
    
    -- |[Spawn NPC]|
    --Using the information provided, spawn the clue with the data it needs. The clue is the same name as the prey.
    Debug_Print("  Spawning " .. psName .. "|" .. sDirString .. "\n")
    TA_Create(psName .. "|" .. sDirString)
        TA_SetProperty("Position", fClueX, fClueY)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        TA_SetProperty("Activation Script", gsRoot .. "Minigames/Hunting/901 Dialogue.lua")
        fnSetCharacterGraphics("Root/Images/Sprites/GenericF0/", false)
        TA_SetProperty("Wipe Special Frames")
        TA_SetProperty("Add Special Frame", "Clue", sUseFrame)
        TA_SetProperty("Set Special Frame", "Clue")
        TA_SetProperty("Rendering Depth", -0.999980)
        TA_SetProperty("Rendering Offsets", 0, 0)
        TA_SetProperty("Shadow", "Null")
    DL_PopActiveObject()
    
end
