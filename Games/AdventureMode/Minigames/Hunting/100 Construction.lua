-- |[ ================================== Construction Handler ================================== ]|
--Called after initialization, builds the list of maps and their connectivity. This file is split
-- into segments, since some parts of the map do not interact.

-- |[ ======================================= Northwoods ======================================= ]|
--The northwestern part of the glacier, where the player starts the chapter.
local sPrefix = "Northwoods"
gzaHuntData.fnAddRoomSeries(sPrefix .. "NE", "A", "E")
gzaHuntData.fnAddRoomSeries(sPrefix .. "NC", "A", "D")
gzaHuntData.fnAddRoomSeries(sPrefix .. "NW", "A", "F")
gzaHuntData.fnAddRoomSeries(sPrefix .. "SC", "A", "E")
gzaHuntData.fnAddRoomSeries(sPrefix .. "SE", "A", "D")
gzaHuntData.fnAddRoomSeries(sPrefix .. "SW", "A", "D")

-- |[NE Series]|
gzaHuntData.fnCreateConnection("NorthwoodsNEA", "NorthwoodsNEB|S", "NorthwoodsNEE|W")
gzaHuntData.fnCreateConnection("NorthwoodsNEB", "NorthwoodsNEA|N", "NorthwoodsNEC|S")
gzaHuntData.fnCreateConnection("NorthwoodsNEC", "NorthwoodsNEB|N", "NorthwoodsNED|W")
gzaHuntData.fnCreateConnection("NorthwoodsNED", "NorthwoodsNEC|E", "NorthwoodsNCA|W")
gzaHuntData.fnCreateConnection("NorthwoodsNEE", "NorthwoodsNEA|E", "NorthwoodsNWC|W")

-- |[NC Series]|
gzaHuntData.fnCreateConnection("NorthwoodsNCA", "NorthwoodsNED|E",  "NorthwoodsNCB|W",  "NorthwoodsSCA|S")
gzaHuntData.fnCreateConnection("NorthwoodsNCB", "NorthwoodsNWA|N",  "NorthwoodsNCC|W",  "NorthwoodsNCD|S")
gzaHuntData.fnCreateConnection("NorthwoodsNCC", "NorthwoodsNCB|NE", "NorthwoodsNCD|SE", "NorthwoodsSWA|S")
gzaHuntData.fnCreateConnection("NorthwoodsNCD", "NorthwoodsNCC|NW", "NorthwoodsNCB|N")

-- |[NW Series]|
gzaHuntData.fnCreateConnection("NorthwoodsNWA", "NorthwoodsNWB|N", "NorthwoodsNWD|W", "NorthwoodsNCB|S")
gzaHuntData.fnCreateConnection("NorthwoodsNWB", "NorthwoodsNWC|E", "NorthwoodsNWA|S")
gzaHuntData.fnCreateConnection("NorthwoodsNWC", "NorthwoodsNWB|W", "NorthwoodsNEE|E")
gzaHuntData.fnCreateConnection("NorthwoodsNWD", "NorthwoodsNWA|E", "NorthwoodsNWE|N", "NorthwoodsNWF|S")
gzaHuntData.fnCreateConnection("NorthwoodsNWE", "NorthwoodsNWD|S")
gzaHuntData.fnCreateConnection("NorthwoodsNWF", "NorthwoodsNWD|N")

-- |[SC Series]|
gzaHuntData.fnCreateConnection("NorthwoodsSCA", "NorthwoodsNCA|N", "NorthwoodsSCB|S", "NorthwoodsSCE|E")
gzaHuntData.fnCreateConnection("NorthwoodsSCB", "NorthwoodsSCA|N", "NorthwoodsSEA|S", "NorthwoodsSCC|W")
gzaHuntData.fnCreateConnection("NorthwoodsSCC", "NorthwoodsSCB|E", "NorthwoodsSCD|W")
gzaHuntData.fnCreateConnection("NorthwoodsSCD", "NorthwoodsSCC|E")
gzaHuntData.fnCreateConnection("NorthwoodsSCE", "NorthwoodsSCA|W")

-- |[SE Series]|
gzaHuntData.fnCreateConnection("NorthwoodsSEA", "NorthwoodsSCB|N", "NorthwoodsSWD|W", "NorthwoodsSEB|E")
gzaHuntData.fnCreateConnection("NorthwoodsSEB", "NorthwoodsSEA|W", "NorthwoodsSEC|N", "NorthwoodsSED|E")
gzaHuntData.fnCreateConnection("NorthwoodsSEC", "NorthwoodsSEB|S")
gzaHuntData.fnCreateConnection("NorthwoodsSED", "NorthwoodsSEB|W")

-- |[SW Series]|
gzaHuntData.fnCreateConnection("NorthwoodsSWA", "NorthwoodsNCC|N", "NorthwoodsSWB|S")
gzaHuntData.fnCreateConnection("NorthwoodsSWB", "NorthwoodsSWA|N", "NorthwoodsSWC|E")
gzaHuntData.fnCreateConnection("NorthwoodsSWC", "NorthwoodsSWB|W", "NorthwoodsSWD|E")
gzaHuntData.fnCreateConnection("NorthwoodsSWD", "NorthwoodsSWC|W", "NorthwoodsSEA|E")

-- |[ ======================================== Westwoods ======================================= ]|
--The southwestern part of the glacier. Not a nice place.
sPrefix = "Westwoods"
gzaHuntData.fnAddRoomSeries(sPrefix .. "N",  "A", "F")
gzaHuntData.fnAddRoomSeries(sPrefix .. "W",  "A", "C")
gzaHuntData.fnAddRoomSeries(sPrefix .. "C",  "D", "G")
gzaHuntData.fnAddRoomSeries(sPrefix .. "E",  "A", "C")
gzaHuntData.fnAddRoomSeries(sPrefix .. "E",  "F", "G")
gzaHuntData.fnAddRoomSeries(sPrefix .. "SE", "A", "A")

-- |[N Series]|
gzaHuntData.fnCreateConnection("WestwoodsNA", "WestwoodsNB|E", "WestwoodsWA|S")
gzaHuntData.fnCreateConnection("WestwoodsNB", "WestwoodsNA|W", "WestwoodsNC|E")
gzaHuntData.fnCreateConnection("WestwoodsNC", "WestwoodsNB|W", "WestwoodsND|E")
gzaHuntData.fnCreateConnection("WestwoodsND", "WestwoodsNC|W", "WestwoodsNE|E")
gzaHuntData.fnCreateConnection("WestwoodsNE", "WestwoodsND|W", "WestwoodsNF|E")
gzaHuntData.fnCreateConnection("WestwoodsNF", "WestwoodsNE|W", "WestwoodsEA|S")

-- |[W Series]|
gzaHuntData.fnCreateConnection("WestwoodsWA", "WestwoodsNA|N", "WestwoodsWB|S")
gzaHuntData.fnCreateConnection("WestwoodsWB", "WestwoodsWA|N", "WestwoodsWC|S")
gzaHuntData.fnCreateConnection("WestwoodsWC", "WestwoodsWB|N")

-- |[C Series]|
gzaHuntData.fnCreateConnection("WestwoodsCD", "WestwoodsCE|E")
gzaHuntData.fnCreateConnection("WestwoodsCE", "WestwoodsCD|W", "WestwoodsCF|S", "WestwoodsCG|E")
gzaHuntData.fnCreateConnection("WestwoodsCF", "WestwoodsCE|N")
gzaHuntData.fnCreateConnection("WestwoodsCG", "WestwoodsCE|W", "WestwoodsEB|E")

-- |[E Series]|
gzaHuntData.fnCreateConnection("WestwoodsEA", "WestwoodsNF|N", "WestwoodsEB|S")
gzaHuntData.fnCreateConnection("WestwoodsEB", "WestwoodsEA|N", "WestwoodsEG|S", "WestwoodsCG|W")
gzaHuntData.fnCreateConnection("WestwoodsEC", "WestwoodsEG|N", "WestwoodsEF|E", "WestwoodsSEA|S")
gzaHuntData.fnCreateConnection("WestwoodsEF", "WestwoodsEC|W")
gzaHuntData.fnCreateConnection("WestwoodsEG", "WestwoodsEB|N", "WestwoodsEC|S")

-- |[SE Series]|
gzaHuntData.fnCreateConnection("WestwoodsSEA", "WestwoodsEC|N")
















