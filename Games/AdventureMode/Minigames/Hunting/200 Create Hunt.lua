-- |[ ======================================= Create Hunt ====================================== ]|
--Cleans up all hunting variables and spawns a new hunt. A flag can be set to spawn the tutorial
-- hunt, which features exactly one creature in a fixed path and position.
local bShowCreateHuntDebug = false

-- |[Wipe Data]|
gzaHuntData.fnWipePrey()

-- |[ ====================================== Normal Case ======================================= ]|
if(gzaHuntData.bIsTutorial == false) then
    
    -- |[ ========== Setup ========= ]|
    -- |[Debug]|
    Debug_PushPrint(true, "== Running Hunting Spawning Routines ==\n")
    Debug_Print("Total rooms detected: " .. #gzaHuntData.zaRoomList .. "\n")
    
    -- |[ ======= Northwoods ======= ]|
    -- |[Prey Types]|
    --Create a list of all prey types that can spawn.
    local iaPreyTypes = {}
    table.insert(iaPreyTypes, gciHunt_PreyType_Omnigoose)
    table.insert(iaPreyTypes, gciHunt_PreyType_Turkerus)
    table.insert(iaPreyTypes, gciHunt_PreyType_Brimhog)
    table.insert(iaPreyTypes, gciHunt_PreyType_Unielk)
    
    -- |[Map Indices]|
    --Create a list of all maps that can spawn a nest.
    local iaHuntMapIndices = {}
    table.insert(iaHuntMapIndices, gzaHuntData.fnGetSlotOfRoom("NorthwoodsSWD"))
    table.insert(iaHuntMapIndices, gzaHuntData.fnGetSlotOfRoom("NorthwoodsNCD"))
    table.insert(iaHuntMapIndices, gzaHuntData.fnGetSlotOfRoom("NorthwoodsNWF"))
    table.insert(iaHuntMapIndices, gzaHuntData.fnGetSlotOfRoom("NorthwoodsNWC"))
    table.insert(iaHuntMapIndices, gzaHuntData.fnGetSlotOfRoom("NorthwoodsNED"))
    table.insert(iaHuntMapIndices, gzaHuntData.fnGetSlotOfRoom("NorthwoodsSCD"))
    
    -- |[How Many To Spawn]|
    --There are 3 hunts spawned per set.
    local iHuntsToSpawn = 3
    Debug_Print("Spawning " .. iHuntsToSpawn .. " hunts.\n")
    
    --Iterate.
    local iPreyCounter = 1
    for i = 1, iHuntsToSpawn, 1 do
    
        -- |[Debug]|
        Debug_Print("Spawning hunt " .. i .. "\n")
        
        --Select a prey type and a map.
        local iPreyIndex = LM_GetRandomNumber(1, #iaPreyTypes)
        local iMapIndex  = LM_GetRandomNumber(1, #iaHuntMapIndices)
        
        --Fast-access variables, remove from original table.
        local iUsePrey = iaPreyTypes[iPreyIndex]
        local iUseMap  = iaHuntMapIndices[iMapIndex]
        local zMapObject = gzaHuntData.zaRoomList[iUseMap]
        table.remove(iaPreyTypes, iPreyIndex)
        table.remove(iaHuntMapIndices, iMapIndex)
        
        --Diagnostics.
        Debug_Print(" Using prey type: " .. iUsePrey .. "\n")
        Debug_Print(" Using map index: " .. iMapIndex .. "\n")
        Debug_Print(" Name of room: " .. zMapObject.sName .. "\n")
        Debug_Print(" Connections: " .. #zMapObject.zaConnections .. "\n")
        
        -- |[Save Variable]|
        --Create a variable that indicates whether or not this prey has been shot.
        DL_AddPath("Root/Variables/Chapter2/HuntTracker/")
        VM_SetVar("Root/Variables/Chapter2/HuntTracker/" .. i, "N", 0)
        
        -- |[Run Creation]|
        --Spawn between 2-4 of this prey on the given map.
        local iNumToSpawn = LM_GetRandomNumber(2, 4)
        for p = 1, iNumToSpawn, 1 do
            
            --Spawn.
            local sPreyName = string.format("Autogen%02i", iPreyCounter)
            iPreyCounter = iPreyCounter + 1
            gzaHuntData.fnAddPrey(sPreyName, iUsePrey, zMapObject.sName)
        
            --If this is the first element, place adjacent paths.
            if(p == 1) then
                gzaHuntData.fnPathPreyNest(sPreyName)
            end
        end
    end
    
    -- |[ ======== Westwoods ======= ]|
    -- |[Prey Types]|
    --Westwoods has four prey types.
    local iWestwoodsLoRoll = gciHunt_PreyType_MBrimhog
    local iWestwoodsHiRoll = gciHunt_PreyType_Buneye
    
    -- |[Map Indices]|
    --Get the low/high values of the maps to roll.
    local iWestwoodsMapLoRoll = gzaHuntData.fnGetSlotOfRoom("WestwoodsNA")
    local iWestwoodsMapHiRoll = gzaHuntData.fnGetSlotOfRoom("WestwoodsSEA")
    
    -- |[How Many To Spawn]|
    --Spawn between 2 and 5 hunts.
    iHuntsToSpawn = LM_GetRandomNumber(2, 5)
    Debug_Print("Rolled to spawn " .. iHuntsToSpawn .. " hunts.\n")
    
    --Iterate.
    for i = 1, iHuntsToSpawn, 1 do
    
        -- |[Debug]|
        Debug_Print("Spawning hunt " .. i .. "\n")
        
        -- |[Type Selection]|
        local iTypeRoll = LM_GetRandomNumber(iWestwoodsLoRoll, iWestwoodsHiRoll)
        Debug_Print(" Rolled type " .. iTypeRoll .. "\n")
    
        -- |[Room Select]|
        local iRoomSlot = LM_GetRandomNumber(iWestwoodsMapLoRoll, iWestwoodsMapHiRoll)
        local zMapObject = gzaHuntData.zaRoomList[iRoomSlot]
        Debug_Print(" Selected slot " .. iRoomSlot .. "\n")
        Debug_Print(" Name of room: " .. zMapObject.sName .. "\n")
        Debug_Print(" Connections: " .. #zMapObject.zaConnections .. "\n")
        
        -- |[Run Creation]|
        gzaHuntData.fnAddPrey("Autogen"..i, iTypeRoll, zMapObject.sName)
        
        -- |[Run Prey Pathing]|
        gzaHuntData.fnPathPrey("Autogen"..i, 4, 9)
        Debug_Print(" Finished prey pathing.\n")
    
    end
    
    -- |[ ========== Debug ========= ]|
    Debug_PopPrint("After spawning, " .. #gzaHuntData.zaPreyList .. " hunts are present.\n")
    Debug_PopPrint("Finished creating hunt algorithm.\n")
    
-- |[ ======================================== Tutorial ======================================== ]|
else

    --Spawns one enemy and paths them along a specific path.
    gzaHuntData.fnAddPrey("The Goose", gciHunt_PreyType_Omnigoose, "NorthwoodsNCA")
    gzaHuntData.fnPathPreyManually("The Goose", {"NorthwoodsNCB", "NorthwoodsNWA", "NorthwoodsNWB", "NorthwoodsNWC"})

    --Set the spawn position flag to make the clues easier for the player to find.
    gzaHuntData.zaPreyList[1].iClueRoll = 1

end
