-- |[ ========================================= Loading ======================================== ]|
--Called after the object is received by the owning AdventureLevel. Graphics are not loaded yet.
-- The Construct() routine will be called after this lua script finishes executing.
io.write("Ran loader script.\n")

-- |[ ========================================== Setup ========================================= ]|
--Open the SLF file.
SLF_Open(gsDatafilesPath .. "PuzzleBattle.slf")

-- |[ ===================================== Field Elements ===================================== ]|
--The highlight must render without the special filters.
DL_AddPath("Root/Images/Sprites/PuzzleBattle/")
local sPrefix = "Field|"
DL_ExtractBitmap(sPrefix .. "Highlight", "Root/Images/Sprites/PuzzleBattle/" .. sPrefix .. "Highlight")

--Modify the distance filters to keep everything pixellated. All field elements are sprite-level.
local iNearest = DM_GetEnumeration("GL_NEAREST")
ALB_SetTextureProperty("MagFilter", iNearest)
ALB_SetTextureProperty("MinFilter", iNearest)
ALB_SetTextureProperty("Special Sprite Padding", true)

--List of World Pieces:
local saNames = {"World Arrow Down", "World Arrow Lft", "World Arrow Rgt", "World Arrow Up", "World Boost", "World Guard", "World Melee", "World Ranged", "World Power Up", "World Perfect Guard"}

--Load.
for i = 1, #saNames, 1 do
    DL_ExtractBitmap(sPrefix .. saNames[i], "Root/Images/Sprites/PuzzleBattle/" .. sPrefix .. saNames[i])
end

--Reset flags.
ALB_SetTextureProperty("Restore Defaults")
ALB_SetTextureProperty("Special Sprite Padding", false)

-- |[ ======================================= UI Elements ====================================== ]|
--List of UI Pieces:
sPrefix = "UI|"
saNames = {"Health Fill Lft", "Health Fill Rgt", "Health Frame Lft", "Health Frame Rgt", "Moves Box", "Time Header", "Confirm Box", "Text Top", "Text Bottom"}

--Load.
DL_AddPath("Root/Images/UI/PuzzleBattle/")
for i = 1, #saNames, 1 do
    DL_ExtractBitmap(sPrefix .. saNames[i], "Root/Images/UI/PuzzleBattle/" .. sPrefix .. saNames[i])
end

-- |[ ========================================== Clean ========================================= ]|
--Close the SLF file.
SLF_Close()
