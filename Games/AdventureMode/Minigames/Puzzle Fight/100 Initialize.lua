-- |[ ======================================= Initialize ======================================= ]|
--The minigame has now loaded and referenced all its graphics. At this point, set all variables
-- associated with the minigame, both in lua and in C++.
--io.write("Running initializer script.\n")

--Initialize all Lua-side variables.
fnInitPuzzleFight()

--If this function hasn't been built yet, do so.
local sBasePath = fnResolvePath()
LM_ExecuteScript(sBasePath .. "Puzzle Melee Attack.lua")
LM_ExecuteScript(sBasePath .. "Puzzle Ranged Attack.lua")
LM_ExecuteScript(sBasePath .. "Move Actor.lua")
LM_ExecuteScript(sBasePath .. "Enemy Attack.lua")
LM_ExecuteScript(sBasePath .. "Board Generator/000 Main.lua")
LM_ExecuteScript(sBasePath .. "Board Generator/010 Subroutines.lua")
LM_ExecuteScript(sBasePath .. "Board Generator/020 Attack Placement.lua")
LM_ExecuteScript(sBasePath .. "Board Generator/030 Arrow Builder.lua")
LM_ExecuteScript(sBasePath .. "Board Generator/040 Scrambler.lua")
LM_ExecuteScript(sBasePath .. "Board Generator/050 Powerups.lua")

--Set world scale. It zooms slightly out so the whole board is visible.
AL_SetProperty("Camera Zoom", 2.5)

-- |[ ==================================== Location Handler ==================================== ]|
--Because the puzzle battles can take place in several locations, a global script variable is
-- checked to see which room it is.
local iLocation = VM_GetVar("Root/Variables/Global/PuzzleFight/iLocation", "N")

-- |[Error Case]|
--Just print a warning.
if(iLocation == gci_PuzzleFightLocation_Invalid) then
    io.write("Warning: Puzzle Fight initialized with an invalid location.\n")
    
-- |[Sanya Mines G]|
--This area is the tutorial battle and has several related cutscenes.
elseif(iLocation == gci_PuzzleFightLocation_SanyaMinesG) then

    -- |[Setup]|
    local iBoardX = 4
    local iBoardY = 5
    local iBoardW = 7
    local iBoardH = 7
    local iTileW = 2
    local iTileH = 2
    
    -- |[Update Script]|
    PuzzleFight_SetProperty("Update Script", gsRoot .. "Minigames/Puzzle Fight/200 Update Handler.lua")
    
    -- |[Variables]|
    --These variables are used since this is the tutorial battle.
    gzaPuzzleFight.iTutorialState = gci_PuzzleFightTutorial_ExplainControls
    gzaPuzzleFight.iBoardStartX = iBoardX
    gzaPuzzleFight.iBoardStartY = iBoardY
    gzaPuzzleFight.iTileW = iTileW
    gzaPuzzleFight.iTileH = iTileH
    gzaPuzzleFight.iBoardWidth = iBoardW
    gzaPuzzleFight.iBoardHeight = iBoardH
    gzaPuzzleFight.sPostExecScript = gsRoot .. "Maps/Trafal/NorthwoodsMines/SanyaMinesG/Post Puzzle.lua"
    
    --Uncomment this to skip the tutorial.
    --gzaPuzzleFight.iTutorialState = gci_PuzzleFightTutorial_None
    
    -- |[Player]|
    PuzzleFight_SetProperty("Player Entity Name", "Sanya")
    gzaPuzzleFight.sEntityName = "Sanya"

    -- |[Board Properties]|
    PuzzleFight_SetProperty("Board Tile Position", gzaPuzzleFight.iBoardStartX, gzaPuzzleFight.iBoardStartY)
    PuzzleFight_SetProperty("Board Size", gzaPuzzleFight.iBoardWidth, gzaPuzzleFight.iBoardHeight)

    -- |[Dislocation Handling]|
    --White Board Dislocations
    AL_SetProperty("Add Dislocation", "Board10", "BoardPieces",  0,  0, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board30", "BoardPieces",  0,  2, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board50", "BoardPieces",  0,  4, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board01", "BoardPieces",  0,  6, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board21", "BoardPieces",  0,  8, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board41", "BoardPieces",  0, 10, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board61", "BoardPieces",  0, 12, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board12", "BoardPieces",  0, 14, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board32", "BoardPieces",  0, 16, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board52", "BoardPieces",  0, 18, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board03", "BoardPieces",  0, 20, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board23", "BoardPieces",  0, 22, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board43", "BoardPieces", 20,  0, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board63", "BoardPieces", 20,  2, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board14", "BoardPieces", 20,  4, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board34", "BoardPieces", 20,  6, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board54", "BoardPieces", 20,  8, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board05", "BoardPieces", 20, 10, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board25", "BoardPieces", 20, 12, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board45", "BoardPieces", 20, 14, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board65", "BoardPieces", 20, 16, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board16", "BoardPieces", 20, 18, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board36", "BoardPieces", 20, 20, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board56", "BoardPieces", 20, 22, 2, 2, 0, 0)
    
    --Brown Board Dislocations
    AL_SetProperty("Add Dislocation", "Board00", "BoardPieces",  2,  0, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board20", "BoardPieces",  4,  0, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board40", "BoardPieces",  6,  0, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board60", "BoardPieces",  8,  0, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board11", "BoardPieces", 10,  0, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board31", "BoardPieces", 12,  0, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board51", "BoardPieces", 14,  0, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board02", "BoardPieces", 16,  0, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board22", "BoardPieces", 18,  0, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board42", "BoardPieces",  2, 21, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board62", "BoardPieces",  2, 23, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board13", "BoardPieces",  2, 25, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board33", "BoardPieces",  2, 27, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board53", "BoardPieces",  0, 24, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board04", "BoardPieces",  0, 26, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board24", "BoardPieces",  4, 27, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board44", "BoardPieces",  6, 27, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board64", "BoardPieces", 17, 21, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board15", "BoardPieces", 17, 23, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board35", "BoardPieces", 17, 25, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board55", "BoardPieces", 17, 27, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board06", "BoardPieces", 13, 27, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board26", "BoardPieces", 15, 26, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board46", "BoardPieces", 19, 24, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "Board66", "BoardPieces", 19, 26, 2, 2, 0, 0)
    
    --Extras used for scrolling.
    AL_SetProperty("Add Dislocation", "BoardWht", "BoardPieces",  0, 29, 2, 2, 0, 0)
    AL_SetProperty("Add Dislocation", "BoardBrn", "BoardPieces", 20, 29, 2, 2, 0, 0)
    
    --Allocate dislocation space.
    PuzzleFight_SetProperty("Allocate Dislocations", 51) --(7x7 slots + 2 extras)
    local iSlot = 0
    for x = 0, 6, 1 do
        for y = 0, 6, 1 do
            
            --Basic.
            PuzzleFight_SetProperty("Dislocation Name", iSlot, "Board"..x..y)
            
            --If this is an odd-numbered tile, it's a white tile.
            if(iSlot % 2 == 1) then
                PuzzleFight_SetProperty("Dislocation Alt Name", iSlot, "BoardWht")
            else
                PuzzleFight_SetProperty("Dislocation Alt Name", iSlot, "BoardBrn")
            end
            
            --Position and Destination are the same at the start.
            local fXPos = (iBoardX * gciSizePerTile) + (x * iTileW * gciSizePerTile)
            local fYPos = (iBoardY * gciSizePerTile) + (y * iTileH * gciSizePerTile)
            PuzzleFight_SetProperty("Dislocation Pos", iSlot, fXPos, fYPos)
            PuzzleFight_SetProperty("Dislocation Dest", iSlot, fXPos, fYPos)
            
            --Register this dislocation to the board.
            PuzzleFight_SetProperty("Register Dislocation To Board Slot", iSlot, x, y)
            
            --Next.
            iSlot = iSlot + 1
        end
    end
    
    --Extra dislocations are left in the corner.
    PuzzleFight_SetProperty("Dislocation Name",     49, "BoardWht")
    PuzzleFight_SetProperty("Dislocation Alt Name", 49, "Null")
    PuzzleFight_SetProperty("Dislocation Pos",      49, 0, 0)
    PuzzleFight_SetProperty("Dislocation Dest",     49, 0, 0)
    PuzzleFight_SetProperty("Dislocation Name",     50, "BoardBrn")
    PuzzleFight_SetProperty("Dislocation Alt Name", 50, "Null")
    PuzzleFight_SetProperty("Dislocation Pos",      50, 0, 0)
    PuzzleFight_SetProperty("Dislocation Dest",     50, 0, 0)
    
    --Set this to indicate the object is ready for action.
    PuzzleFight_SetProperty("Take Control")

-- |[Other Error Case]|
else
    io.write("Warning: Puzzle Fight initialized with an invalid location.\n")
end

-- |[Debug]|
io.write("Done initializer script.\n")