-- |[ ============================== Puzzle Melee Attack Function ============================== ]|
--Called when the player attacks an enemy with a melee attack.
function fnPuzzleMeleeAttack(piDirection, piEnemySlot, piPlayerPosX, piPlayerPosY, piDamage)

    -- |[Error Checks]|
    --Argument check.
    if(piDirection  == nil) then return end
    if(piEnemySlot  == nil) then return end
    if(piPlayerPosX == nil) then return end
    if(piPlayerPosY == nil) then return end
    if(piDamage     == nil) then return end
    
    --Error check: Slot can't be -1.
    if(piEnemySlot == -1) then return end
    
    -- |[Setup]|
    --Constants.
    local cfMoveDist = 1.0
    local cfMoveSpeed = 1.50
    
    --Variables.
    local iEnemyHealth = PuzzleFight_GetProperty("Enemy Health")
    local sPlayerName = gzaPuzzleFight.sEntityName
    local sTargetName = gzaPuzzleFight.zaEnemies[piEnemySlot].sActorName
    local fWorldX = gzaPuzzleFight.fnGetWorldX(piPlayerPosX) / gciSizePerTile
    local fWorldY = gzaPuzzleFight.fnGetWorldY(piPlayerPosY) / gciSizePerTile
    
    -- |[Direction Resolve]|
    --Variables.
    local fTargetX = fWorldX
    local fTargetY = fWorldY
    local sAttackDir = "N"
    local sPainDir = "S"
    
    --Set by direction.
    if(piDirection == gci_Face_North) then
        fTargetY = fTargetY - cfMoveDist
        sAttackDir = "N"
        sPainDir = "S"
    elseif(piDirection == gci_Face_East) then
        fTargetX = fTargetX + cfMoveDist
        sAttackDir = "E"
        sPainDir = "W"
    elseif(piDirection == gci_Face_South) then
        fTargetY = fTargetY + cfMoveDist
        sAttackDir = "S"
        sPainDir = "N"
    elseif(piDirection == gci_Face_West) then
        fTargetX = fTargetX - cfMoveDist
        sAttackDir = "W"
        sPainDir = "E"
    end

    --Entities always start by facing each other.
    fnCutsceneFaceTarget(sPlayerName, sTargetName)
    fnCutsceneFaceTarget(sTargetName, sPlayerName)

    --Move up to them.
    fnCutsceneMove(sPlayerName, fTargetX, fTargetY, cfMoveSpeed)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Attack power up!
    if(gzaPuzzleFight.bAttackUpThisTurn == true) then
        fnCutscene([[ AudioManager_PlaySound("Puzzle|PowerUp") ]])
        local sString = "PuzzleFight_SetProperty(\"Register Floatfade Notifier\", " .. (fWorldX * gciSizePerTile) .. ", " .. (fWorldY * gciSizePerTile) .. ", \"Root/Images/Sprites/PuzzleBattle/Field|World Power Up\")"
        fnCutscene(sString)
        fnCutsceneWait(35)
        fnCutsceneBlocker()
    end
    
    --String for damage.
    local sDamageString = "PuzzleFight_SetProperty(\"Enemy Health\", " .. (iEnemyHealth - piDamage) .. ")"
    
    --Kick animation.
    fnCutsceneSetFrame(sPlayerName, "Kick" .. sAttackDir .. "0")
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutscene(sDamageString)
    fnCutscenePlaySound("Combat|Impact_Strike")
    fnCutsceneSetFrame(sPlayerName, "Kick" .. sAttackDir .. "1")
    fnCutsceneSetFrame(sTargetName, "Pain" .. sPainDir .. "0")
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneSetFrame(sTargetName, "Pain" .. sPainDir .. "1")
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    
    --Return to the starting position.
    fnCutsceneSetFrame(sPlayerName, "Null")
    fnCutsceneJumpNPC(sPlayerName, gcfStdJumpSpeed * 0.50)
    fnCutsceneMove(sPlayerName, fWorldX, fWorldY, cfMoveSpeed)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneSetFrame(sTargetName, "Null")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
end
