-- |[ ==================================== Dialogue Handler ==================================== ]|
--If dialogue is to appear during the puzzle fight minigame, this script handles it. This is because
-- it's a lot simpler to deal with decisions in their own file.
--The argument provided is a string for which block of dialogue to show.
if(fnArgCheck(1) == false) then return end

--Get arg.
local sSwitchType = LM_GetScriptArgument(0)

-- |[ ================================= Tutorial Introduction ================================== ]|
--When Odar first tells Sanya how to play, show this. It can be repeated via a decision at the end.
if(sSwitchType == "Tutorial A") then
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] Now hear this, foul hunter.[P] I'll give you the basics.[B][C]") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] You're the accepter of the challenge, which means you are in the middle of the board.[B][C]") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] I take up a position on the board, and your goal is to reach me.[B][C]") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] You can move the board with the cursor.[P] Use the arrow keys to change the cursor.[B][C]") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] The key 'UpLevel' (Q by Default) changes the cursor from row to column.[P] When you move a row or column, the entire thing moves, except for you and me of course.[B][C]") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] You can only move the board so many times.[P] Each move to a column or row costs one move.[P] You can undo moves with the 'Cancel' key (X by default).[B][C]") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] In order to move, you must follow the arrows on the board.[P] One is currently under you, facing right, so you will move right.[B][C]") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] As you can see, there is a fist next to me.[P] If you step on it, you will attack me.[B][C]") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] You should only need a single move to create a path to that fist.[P] Try it![P] I'll even give you unlimited time![B][C]") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] Did you get all that, or should I repeat the instructions?[BLOCK]") ]])

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"One More Time\",  " .. sDecisionScript .. ", \"Repeat\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"I got it\", " .. sDecisionScript .. ", \"Got It\") ")
    fnCutsceneBlocker()
    
--Confirm instructions.
elseif(sSwitchType == "Got It") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Sanya:[VOICE|Sanya] I think I got it.[B][C]") ]])
	fnCutscene([[ Append("Odar:[VOICE|Odar] Then have at you!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Title print occurs in stride.
    gzaPuzzleFight.bHasPrintedTitle = true
    fnCutscene([[ PuzzleFight_SetProperty("Title Print", "Use the Arrows!") ]])
    
--Repeats instructions with a slight variation.
elseif(sSwitchType == "Repeat") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] Very well.[P] If I have a fault, it's that I am perhaps too forgiving.[B][C]") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] You're the accepter of the challenge, which means you are in the middle of the board.[B][C]") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] I take up a position on the board, and your goal is to reach me.[B][C]") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] You can move the board with the cursor.[P] Use the arrow keys to change the cursor.[B][C]") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] The key 'UpLevel' (Q by Default) changes the cursor from row to column.[P] When you move a row or column, the entire thing moves, except for you and me of course.[B][C]") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] You can only move the board so many times.[P] Each move to a column or row costs one move.[P] You can undo moves with the 'Cancel' key (X by default).[B][C]") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] In order to move, you must follow the arrows on the board.[P] One is currently under you, facing right, so you will move right.[B][C]") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] As you can see, there is a fist next to me.[P] If you step on it, you will attack me.[B][C]") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] You should only need a single move to create a path to that fist.[P] Try it![P] I'll even give you unlimited time![B][C]") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] Did you get all that, or should I repeat the instructions?[BLOCK]") ]])

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"One More Time\",  " .. sDecisionScript .. ", \"Repeat\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"I got it\", " .. sDecisionScript .. ", \"Got It\") ")
    fnCutsceneBlocker()
    
-- |[ ============================== Tutorial Introduction Repeat ============================== ]|
--The player messed up the first move and has to retry.
elseif(sSwitchType == "Tutorial B") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] I see.[P] Perhaps I have miscommunicated.[B][C]") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] You can move the board with the cursor.[P] Use the arrow keys to change the cursor.[B][C]") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] The key 'UpLevel' (Q by Default) changes the cursor from row to column.[P] When you move a row or column, the entire thing moves, except for you and me of course.[B][C]") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] You can only move the board so many times.[P] Each move to a column or row costs one move.[P] You can undo moves with the 'Cancel' key (X by default).[B][C]") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] In order to move, you must follow the arrows on the board.[P] One is currently under you, facing right, so you will move right.[B][C]") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] As you can see, there is a fist next to me.[P] If you step on it, you will attack me.[B][C]") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] You should only need a single move to create a path to that fist.[P] Try it![P] I'll even give you unlimited time![B][C]") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] Did you get all that, or should I repeat the instructions?[BLOCK]") ]]) 
    
    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"One More Time\",  " .. sDecisionScript .. ", \"Repeat\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"I got it\", " .. sDecisionScript .. ", \"Got It\") ")
    fnCutsceneBlocker()

    
-- |[ ================================= Tutorial Ranged Combat ================================= ]|
--Learning to use guns.
elseif(sSwitchType == "Tutorial C") then
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] (Ungh...[P] This hunter hits like a siege engine...)[B][C]") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] (I think she broke something...)[B][C]") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] *Cough*[P] Well done, it seems you understand melee attacks.[P] They will strike all adjacent enemies when used.[B][C]") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] As I have returned your gun, you may as well try out ranged attacks.[P] They will strike any enemy in the same row or column as you.[B][C]") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] Keep in mind that symbols that go off one side of the board come back on the other side.[P] Don't forget that!") ]])
    fnCutsceneBlocker()
    
    --Title print occurs in stride.
    gzaPuzzleFight.bHasPrintedTitle = true
    fnCutscene([[ PuzzleFight_SetProperty("Title Print", "Use the Arrows!") ]])
    
--Learning to use guns, but more!
elseif(sSwitchType == "Tutorial D") then
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] Oh dear, was I not clear?[P] You need to be on the same row or column as me to shoot with me your firearm.[B][C]") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] Ranged attacks cannot fire diagonally, and do the same damage as melee attacks.[B][C]") ]])
    fnCutscene([[ Append("Odar:[VOICE|Odar] And keep in mind that moving a tile off one edge moves onto the other edge of the board.[P] You'll need it!") ]])
    fnCutsceneBlocker()
    
    --Title print occurs in stride.
    gzaPuzzleFight.bHasPrintedTitle = true
    fnCutscene([[ PuzzleFight_SetProperty("Title Print", "Use the Arrows!") ]])
    
-- |[ ====================================== Finishing Up ====================================== ]|
--He ain't looking so good.
elseif(sSwitchType == "Tutorial E") then

    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Odar", "Neutral") ]])
    fnCutscene([[ Append("Odar:[E|Neutral] Ha...[P] haaa...[P] well done, hunter.[P] *Ugh* but you have made a mistake.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Hey pal, you're not looking so good.[P] You need like an iced tea or something?[B][C]") ]])
    fnCutscene([[ Append("Odar:[E|Neutral] *cough*[P] We fungoids are of fey heritage.[P] Your iron bullets *ow* will not avail you.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Well that's pretty neat.[P] But those are 7.62 bullets made of lead.[P] Not iron.[B][C]") ]])
    fnCutscene([[ Append("Odar:[E|Neutral] Really?[P] Not iron?[P] Not even a bit?[P] Steel is iron you know.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Nope, lead.[B][C]") ]])
    fnCutscene([[ Append("Odar:[E|Neutral] I guess that explains all the blood.[B][C]") ]])
    fnCutscene([[ Append("Odar:[E|Neutral] Where'd you get a gun that shoots lead balls?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] They're not balls, they're shaped bullets designed to take advantage of the barrel's corkscrewing.[B][C]") ]])
    fnCutscene([[ Append("Odar:[E|Neutral] *Cough*[P] this battle is still mine, hunter![B][C]") ]])
    fnCutscene([[ Append("Odar:[E|Neutral] Obviously you have mastered the basics, so I see no need to hold back.[B][C]") ]])
    fnCutscene([[ Append("Odar:[E|Neutral] Oh one more thing.[P] There are sometimes powerups on the board.[P] These will increase your damage or reduce the damage you take.[B][C]") ]])
    fnCutscene([[ Append("Odar:[E|Neutral] And, if you have moves left over at the end of your turn, you will keep them on the next turn.[B][C]") ]])
    fnCutscene([[ Append("Odar:[E|Neutral] Now, the duel begins in earnest!") ]])
    fnCutsceneBlocker()
    
    --Title print occurs in stride.
    gzaPuzzleFight.bHasPrintedTitle = true
    fnCutscene([[ PuzzleFight_SetProperty("Title Print", "Use the Arrows!") ]])

end
