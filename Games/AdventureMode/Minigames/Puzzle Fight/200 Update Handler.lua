-- |[ ===================================== Update Handler ===================================== ]|
--Handles updates, obviously. This script gets called when the puzzle battle reaches a certain state,
-- such as each time the player moves a row, or when the objects on a board have finished spawning.
--Each update code is defined in the C++ and mirrored in 000 Variables.lua.
--The update is always and only called at the end of the update cycle. This means the board status
-- may have changed in the C++ code.
if(fnArgCheck(1) == false) then return end

--Get arg.
local iSwitchType = LM_GetScriptArgument(0, "I")

-- |[ ===================================== Begin New Turn ===================================== ]|
--Called when the board state requires a new turn to begin. This occurs once immediately after
-- initialization, and then again each time the player finishes their turn and the enemy acts.
if(iSwitchType == gciPuzzleFight_Update_BeginTurn) then
    
    -- |[End Check]|
    --Check the player and enemy HPs. End the minigame as needed. Note that a tie breaks towards the enemy.
    local iPlayerHP = PuzzleFight_GetProperty("Player Health")
    local iEnemyHP  = PuzzleFight_GetProperty("Enemy Health")
    if(iPlayerHP <= 0) then
        PuzzleFight_SetProperty("Finish")
        gzaPuzzleFight.bIsGameOver = true
        gzaPuzzleFight.bPlayerWonGame = false
        LM_ExecuteScript(gzaPuzzleFight.sPostExecScript)
        return
    elseif(iEnemyHP <= 0) then
        PuzzleFight_SetProperty("Finish")
        gzaPuzzleFight.bIsGameOver = true
        gzaPuzzleFight.bPlayerWonGame = true
        LM_ExecuteScript(gzaPuzzleFight.sPostExecScript)
        return
    end
    
    -- |[Setup]|
    --First, set the game state to the execution of the player's turn.
    PuzzleFight_SetProperty("Game State", gciPuzzleFightState_PlayersTurn_Run)
    
    --Clear the board of objects.
    PuzzleFight_SetProperty("Clear Board Objects")
    gzaPuzzleFight.fnClearEnemies()
    
    -- |[Tutorial 1]|
    --Tutorial: Explain the controls. This spawns a special set of objects.
    if(gzaPuzzleFight.iTutorialState == gci_PuzzleFightTutorial_ExplainControls or gzaPuzzleFight.iTutorialState == gci_PuzzleFightTutorial_PlayerFirstMoveFailed) then

        -- |[Actors]|
        --Odar is in a fixed position.
        gzaPuzzleFight.fnAddEnemy("Odar", 6, 5)
        
        --Subroutine moves the actors.
        fnMoveActor("Sanya", 3, 3)
        fnMoveActor("Odar",  6, 5)
    
        --Face each other.
        fnCutsceneBlocker()
        fnCutsceneFaceTarget("Sanya", "Odar")
        fnCutsceneFaceTarget("Odar", "Sanya")
        fnCutsceneBlocker()
        
        -- |[Timer]|
        --Set to -1 to give the player unlimited time.
        PuzzleFight_SetProperty("Player Ticks Left", -1)
        
        -- |[Objects]|
        --Spawn position. They start the center and radiate outwards.
        local fSpawnX = (gzaPuzzleFight.iBoardStartX * gciSizePerTile) + (3.0 * gzaPuzzleFight.iTileW * gciSizePerTile)
        local fSpawnY = (gzaPuzzleFight.iBoardStartY * gciSizePerTile) + (3.0 * gzaPuzzleFight.iTileH * gciSizePerTile)
        
        --Spawn the needed objects. When the objects finish moving, the next tutorial block is
        -- in a set of code down this handler.
        fnCutscene("PuzzleFight_SetProperty(\"Spawn Board Object\", gciPuzzleFight_Object_ArrowRt, 3, 3, " .. fSpawnX .. ", " .. fSpawnY .. ")")
        fnCutscene("PuzzleFight_SetProperty(\"Spawn Board Object\", gciPuzzleFight_Object_ArrowDn, 4, 2, " .. fSpawnX .. ", " .. fSpawnY .. ")")
        fnCutscene("PuzzleFight_SetProperty(\"Spawn Board Object\", gciPuzzleFight_Object_ArrowRt, 4, 4, " .. fSpawnX .. ", " .. fSpawnY .. ")")
        fnCutscene("PuzzleFight_SetProperty(\"Spawn Board Object\", gciPuzzleFight_Object_Melee,   5, 5, " .. fSpawnX .. ", " .. fSpawnY .. ")")
        
        --Player gets one move for this sequence.
        fnCutscene([[ PuzzleFight_SetProperty("Player Moves Left", 1) ]])
        
        --Block title printing until the dialogue handler runs.
        if(gzaPuzzleFight.bHasPrintedTitle == false) then
            gzaPuzzleFight.bHasPrintedTitle = true
        end
    
    -- |[Tutorial 2 / Repeat]|
    --Tutorial: Player hit Odar. Next up is ranged combat.
    elseif(gzaPuzzleFight.iTutorialState == gci_PuzzleFightTutorial_PlayerFirstMoveSuccess or gzaPuzzleFight.iTutorialState == gci_PuzzleFightTutorial_PlayerSecondMoveFailed) then
        
        -- |[Actors]|
        --Odar is in a fixed position.
        gzaPuzzleFight.fnAddEnemy("Odar", 6, 1)
        
        --Subroutine moves the actors.
        fnMoveActor("Sanya", 3, 3)
        fnMoveActor("Odar",  6, 1)
    
        --Face each other.
        fnCutsceneBlocker()
        fnCutsceneFaceTarget("Sanya", "Odar")
        fnCutsceneFaceTarget("Odar", "Sanya")
        fnCutsceneBlocker()
        
        -- |[Timer]|
        --Set to -1 to give the player unlimited time.
        PuzzleFight_SetProperty("Player Ticks Left", -1)
        
        -- |[Objects]|
        --Spawn position. They start the center and radiate outwards.
        local fSpawnX = (gzaPuzzleFight.iBoardStartX * gciSizePerTile) + (3.0 * gzaPuzzleFight.iTileW * gciSizePerTile)
        local fSpawnY = (gzaPuzzleFight.iBoardStartY * gciSizePerTile) + (3.0 * gzaPuzzleFight.iTileH * gciSizePerTile)
        
        --Spawn the needed objects. When the objects finish moving, the next tutorial block is
        -- in a set of code down this handler.
        fnCutscene("PuzzleFight_SetProperty(\"Spawn Board Object\", gciPuzzleFight_Object_ArrowLf, 2, 3, " .. fSpawnX .. ", " .. fSpawnY .. ")")
        fnCutscene("PuzzleFight_SetProperty(\"Spawn Board Object\", gciPuzzleFight_Object_ArrowUp, 0, 5, " .. fSpawnX .. ", " .. fSpawnY .. ")")
        fnCutscene("PuzzleFight_SetProperty(\"Spawn Board Object\", gciPuzzleFight_Object_Ranged,  6, 3, " .. fSpawnX .. ", " .. fSpawnY .. ")")
        
        --Player gets three moves for this sequence.
        fnCutscene([[ PuzzleFight_SetProperty("Player Moves Left", 3) ]])
        
        --Block title printing until the dialogue handler runs.
        if(gzaPuzzleFight.bHasPrintedTitle == false) then
            gzaPuzzleFight.bHasPrintedTitle = true
        end
    
    -- |[Standard Spawner]|
    --All other cases, use spawner logic.
    else
    
        -- |[Enemy Placement]|
        --Create a list of all places the enemy can be positioned. The middle 3x3 tiles near the player start
        -- are off-limits.
        local zaList = {}
        for x = 0, 6, 1 do
            for y = 0, 6, 1 do
                if(x >= 2 and x <= 4 and y >= 2 and y <= 4) then
                
                else
                    table.insert(zaList, {x, y})
                end
            end
        end
    
        --Place the enemy.
        local iSlot = LM_GetRandomNumber(1, #zaList)
        gzaPuzzleFight.fnAddEnemy("Odar", zaList[iSlot][1], zaList[iSlot][2])
        
        -- |[Actor Movement]|
        --Subroutine moves the actors.
        fnMoveActor("Sanya", 3, 3)
        fnMoveActor("Odar", zaList[iSlot][1], zaList[iSlot][2])
    
        --Face each other.
        fnCutsceneBlocker()
        fnCutsceneFaceTarget("Sanya", "Odar")
        fnCutsceneFaceTarget("Odar", "Sanya")
        fnCutsceneBlocker()
        
        -- |[Move and Time Handling]|
        --Get how much time was left. If it was -1, set it to 0.
        if(gzaPuzzleFight.iTicksLeft < 0) then gzaPuzzleFight.iTicksLeft = 0 end
        if(gzaPuzzleFight.iMovesLeft < 0) then gzaPuzzleFight.iMovesLeft = 0 end
        
        --Roll how many scrambles. Right now it's 2-5. Player also gets a default of 60 seconds to solve the puzzle.
        local iScrambles = LM_GetRandomNumber(2, 5)
        
        --Add the old to the new, but apply a cap.
        local iPlayerMoves = iScrambles + gzaPuzzleFight.iMovesLeft
        local iTimeLeft = (60 * 60) + gzaPuzzleFight.iTicksLeft
        
        --Clamp.
        if(iPlayerMoves >         10) then iPlayerMoves = 10         end
        if(iTimeLeft    > (60 * 180)) then iTimeLeft    = (60 * 180) end
        
        -- |[Generate!]|
        --Generate the board when the actors are in place.
        fnCutscene("fnPuzzleBoardGenerate(" .. iScrambles .. ", true)")
        
        --Upload moves/time. This is done in-stride.
        fnCutscene("PuzzleFight_SetProperty(\"Player Moves Left\", " .. iPlayerMoves .. ")")
        fnCutscene("PuzzleFight_SetProperty(\"Player Ticks Left\", " .. iTimeLeft .. ")")
    end
    
-- |[ ================================= Spawned Objects Moved ================================== ]|
--All objects that were spawned at the start of a player's turn, or otherwise during play, have
-- finished moving.
elseif(iSwitchType == gciPuzzleFight_Update_SpawnedObjectsMoved) then
    
    -- |[Tutorial Stage 1]|
    --Tutorial: Explain the controls. Once the objects are done moving, Odar explains how to play.
    -- This uses an external dialogue file since it involves a decision.
    if(gzaPuzzleFight.iTutorialState == gci_PuzzleFightTutorial_ExplainControls) then
        gzaPuzzleFight.iTutorialState = gci_PuzzleFightTutorial_PlayerFirstMove
        LM_ExecuteScript(fnResolvePath() .. "900 Dialogue Handler.lua", "Tutorial A")
    
    --Tutorial: Remind the player of the controls since they messed up the first section.
    elseif(gzaPuzzleFight.iTutorialState == gci_PuzzleFightTutorial_PlayerFirstMoveFailed) then
        gzaPuzzleFight.iTutorialState = gci_PuzzleFightTutorial_PlayerFirstMove
        LM_ExecuteScript(fnResolvePath() .. "900 Dialogue Handler.lua", "Tutorial B")
    
    -- |[Tutorial Stage 2]|
    elseif(gzaPuzzleFight.iTutorialState == gci_PuzzleFightTutorial_PlayerFirstMoveSuccess) then
        gzaPuzzleFight.iTutorialState = gci_PuzzleFightTutorial_PlayerSecondMove
        LM_ExecuteScript(fnResolvePath() .. "900 Dialogue Handler.lua", "Tutorial C")
        
    elseif(gzaPuzzleFight.iTutorialState == gci_PuzzleFightTutorial_PlayerSecondMoveFailed) then
        gzaPuzzleFight.iTutorialState = gci_PuzzleFightTutorial_PlayerSecondMove
        LM_ExecuteScript(fnResolvePath() .. "900 Dialogue Handler.lua", "Tutorial D")
    
    -- |[Post Tutorial]|
    elseif(gzaPuzzleFight.iTutorialState == gci_PuzzleFightTutorial_PlayerSecondMoveSuccess) then
        gzaPuzzleFight.iTutorialState = gci_PuzzleFightTutorial_None
        LM_ExecuteScript(fnResolvePath() .. "900 Dialogue Handler.lua", "Tutorial E")
    
    -- |[Normal]|
    --All other cases, print the title to inform the player to pay attention to the arrows.
    -- This only occurs once per turn.
    else
        if(gzaPuzzleFight.bHasPrintedTitle == false) then
            gzaPuzzleFight.bHasPrintedTitle = true
            PuzzleFight_SetProperty("Title Print", "Use the Arrows!")
        end
    end
    
-- |[ ====================================== Move Finished ===================================== ]|
elseif(iSwitchType == gciPuzzleFight_Update_Moved) then

    
-- |[ ================================== Player Begins Action ================================== ]|
--Called when the player either confirms their action is complete, or when time runs out.
elseif(iSwitchType == gciPuzzleFight_Update_ConfirmActionComplete) then
    
    -- |[UI Handling]|
    --Store how many moves and ticks were left over.
    gzaPuzzleFight.iTicksLeft = PuzzleFight_GetProperty("Player Ticks Left")
    gzaPuzzleFight.iMovesLeft = PuzzleFight_GetProperty("Player Moves Left")
    
    --Set the time to -1 so it stops displaying.
    PuzzleFight_SetProperty("Player Ticks Left", -1)
    PuzzleFight_SetProperty("Player Moves Left", -1)
    
    -- |[Tutorial Flags]|
    --Tutorial. If Sanya does not hit Odar on the first turn, we repeat the tutorial.
    if(gzaPuzzleFight.iTutorialState == gci_PuzzleFightTutorial_PlayerFirstMove) then
        gzaPuzzleFight.iTutorialState = gci_PuzzleFightTutorial_PlayerFirstMoveFailed
    elseif(gzaPuzzleFight.iTutorialState == gci_PuzzleFightTutorial_PlayerSecondMove) then
        gzaPuzzleFight.iTutorialState = gci_PuzzleFightTutorial_PlayerSecondMoveFailed
    end
    
    -- |[Call Handler]|
    --Most of the work is done in this script.
    LM_ExecuteScript(fnResolvePath() .. "300 Move Handler.lua")
    
    -- |[Enemy Turn]|
    --Run this script again using the enemy action code.
    local sThisScript = fnResolvePath() .. "200 Update Handler.lua"
    local sString = "LM_ExecuteScript(\"".. sThisScript.. "\", gciPuzzleFight_Update_EnemyTurn)"
    fnCutscene(sString)
    
-- |[ ====================================== Enemy Action ====================================== ]|
--Whenever the player's action finishes and the battle is not over, the enemy takes their turn.
elseif(iSwitchType == gciPuzzleFight_Update_EnemyTurn) then

    --Clear the board of objects.
    PuzzleFight_SetProperty("Clear Board Objects")
    gzaPuzzleFight.fnClearEnemies()
    
    -- |[End Check]|
    --Check the player and enemy HPs. End the minigame as needed. Note that a tie breaks towards the enemy.
    local iPlayerHP = PuzzleFight_GetProperty("Player Health")
    local iEnemyHP  = PuzzleFight_GetProperty("Enemy Health")
    if(iPlayerHP <= 0) then
        PuzzleFight_SetProperty("Finish")
        gzaPuzzleFight.bIsGameOver = true
        gzaPuzzleFight.bPlayerWonGame = false
        LM_ExecuteScript(gzaPuzzleFight.sPostExecScript)
        return
    elseif(iEnemyHP <= 0) then
        PuzzleFight_SetProperty("Finish")
        gzaPuzzleFight.bIsGameOver = true
        gzaPuzzleFight.bPlayerWonGame = true
        LM_ExecuteScript(gzaPuzzleFight.sPostExecScript)
        return
    end
    
    -- |[Tutorial Stage]|
    --Check if the enemy health is below max. If so, then Odar got hit and the player passes the first test.
    if(gzaPuzzleFight.iTutorialState == gci_PuzzleFightTutorial_PlayerFirstMoveFailed) then
        if(PuzzleFight_GetProperty("Enemy Health") < 100) then
            gzaPuzzleFight.iTutorialState = gci_PuzzleFightTutorial_PlayerFirstMoveSuccess
        end
    elseif(gzaPuzzleFight.iTutorialState == gci_PuzzleFightTutorial_PlayerSecondMoveFailed) then
        if(PuzzleFight_GetProperty("Enemy Health") < 80) then
            gzaPuzzleFight.iTutorialState = gci_PuzzleFightTutorial_PlayerSecondMoveSuccess
        end
    end
    
    -- |[Counterattack]|
    --If not currently doing the tutorial, the enemy will counterattack.
    if(gzaPuzzleFight.iTutorialState == gci_PuzzleFightTutorial_None) then
    
        --Subroutine moves the actors.
        fnMoveActor("Sanya", 3.0, 3.0)
        fnMoveActor("Odar",  3.5, 3.0)
        fnCutsceneBlocker()
        fnCutsceneFaceTarget("Sanya", "Odar")
        fnCutsceneFaceTarget("Odar", "Sanya")
    
        --Title.
        fnCutscene([[ PuzzleFight_SetProperty("Title Print", "Enemy's Turn!") ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Call attack handler in-stride.
        fnCutscene([[ fnEnemyAttack() ]])
        fnCutsceneBlocker()
    
    -- |[Next Turn]|
    --Otherwise, reset flags and start the player's next turn.
    else
        gzaPuzzleFight.bHasPrintedTitle = false
        fnCutscene([[ PuzzleFight_SetProperty("Game State", gciPuzzleFightState_PlayersTurn_Init) ]])
    end
    
end