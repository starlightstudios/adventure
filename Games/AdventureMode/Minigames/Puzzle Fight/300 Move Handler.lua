-- |[ ====================================== Move Handler ====================================== ]|
--The movement handling loop which controls most of the player's actions.

-- |[Setup]|
--Timing.
fnCutsceneWait(15)
fnCutsceneBlocker()

--The player starts on the middle block.
local iPlayerX = 3
local iPlayerY = 3
local iPlayerMoveDir = -1

--Other variables.
local cfPlayerMoveSpeed = 2.50
local iAttackDamageBase = 20
local iAttackDamageCurrent = iAttackDamageBase
gzaPuzzleFight.bPerfectGuardThisTurn = false
gzaPuzzleFight.bAttackUpThisTurn = false

-- |[Action Loop]|
local iActionsLeft = 200
local bBreakOut = false
while(bBreakOut == false) do

    -- |[ =================================== Setup ================================== ]|
    -- |[Error Check]|
    --It should not be possible to do 200 actions in one go.
    iActionsLeft = iActionsLeft - 1
    if(iActionsLeft < 1) then
        io.write("Puzzle Fight Minigame Error: Broke action limit.\n")
    end

    -- |[Setup]|
    --Get what object is at this position, and where the player is.
    local bKeepMoving = false
    local bHitAction = false
    local fWorldX = gzaPuzzleFight.fnGetWorldX(iPlayerX) / gciSizePerTile
    local fWorldY = gzaPuzzleFight.fnGetWorldY(iPlayerY) / gciSizePerTile
    local iObjectAtPlayer = PuzzleFight_GetProperty("Object At Board Position", iPlayerX, iPlayerY)
    
    --Disable the object immediately. This causes the object to continue rendering, but the next time
    -- the object is queried it will come back as an empty slot. This prevents infinite loops.
    PuzzleFight_SetProperty("Disable Board Object At", iPlayerX, iPlayerY)
    
    --Create a removal string in-stride.
    local sClearingString = "PuzzleFight_SetProperty(\"Clear Board Object At\", " .. iPlayerX .. ", " .. iPlayerY .. ")"
    
    -- |[ ============================== Object Resolve ============================== ]|
    -- |[Enemy Check]|
    --Player smacked into an enemy!
    local iEnemyOnSpot = gzaPuzzleFight.fnGetEnemyInSlot(iPlayerX, iPlayerY)
    if(iEnemyOnSpot ~= -1) then
        bKeepMoving = false
    
        --Move to position.
        local fTargetX = gzaPuzzleFight.fnGetWorldX(iPlayerX) / gciSizePerTile
        local fTargetY = gzaPuzzleFight.fnGetWorldY(iPlayerY) / gciSizePerTile
        fnCutsceneMove("Sanya", fTargetX, fTargetY, cfPlayerMoveSpeed)
        fnCutsceneBlocker()
    
        --Have the player character fall on their face.
        fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
        fnCutsceneSetFrame(gzaPuzzleFight.sEntityName, "Wounded")
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Sanya:[VOICE|Sanya] Ow![P] Watch where you're going![B][C]") ]])
        fnCutscene([[ Append("Odar:[VOICE|Odar] Me?[P] I was just standing here![B][C]") ]])
        fnCutscene([[ Append("Odar:[VOICE|Odar] You can't attack by ramming into your opponent you numbskull!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Stand up.
        fnCutsceneSetFrame(gzaPuzzleFight.sEntityName, "Crouch")
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneSetFrame(gzaPuzzleFight.sEntityName, "Null")
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        break
    
    -- |[Object Type]|
    --Player is standing on an up arrow:
    elseif(iObjectAtPlayer == gciPuzzleFight_Object_ArrowUp) then
        bKeepMoving = true
            
        --Move to position.
        local fTargetX = gzaPuzzleFight.fnGetWorldX(iPlayerX) / gciSizePerTile
        local fTargetY = gzaPuzzleFight.fnGetWorldY(iPlayerY) / gciSizePerTile
        fnCutsceneMove("Sanya", fTargetX, fTargetY, cfPlayerMoveSpeed)
        fnCutsceneBlocker()
        
        --Effects.
        iPlayerMoveDir = gci_Face_North
        fnCutscene([[ AudioManager_PlaySound("Puzzle|ChangeDirections") ]])
        fnCutscene(sClearingString)
    
    --Right arrow:
    elseif(iObjectAtPlayer == gciPuzzleFight_Object_ArrowRt) then
        bKeepMoving = true
            
        --Move to position.
        local fTargetX = gzaPuzzleFight.fnGetWorldX(iPlayerX) / gciSizePerTile
        local fTargetY = gzaPuzzleFight.fnGetWorldY(iPlayerY) / gciSizePerTile
        fnCutsceneMove("Sanya", fTargetX, fTargetY, cfPlayerMoveSpeed)
        fnCutsceneBlocker()
        
        --Effects.
        iPlayerMoveDir = gci_Face_East
        fnCutscene([[ AudioManager_PlaySound("Puzzle|ChangeDirections") ]])
        fnCutscene(sClearingString)
    
    --Down arrow:
    elseif(iObjectAtPlayer == gciPuzzleFight_Object_ArrowDn) then
        bKeepMoving = true
            
        --Move to position.
        local fTargetX = gzaPuzzleFight.fnGetWorldX(iPlayerX) / gciSizePerTile
        local fTargetY = gzaPuzzleFight.fnGetWorldY(iPlayerY) / gciSizePerTile
        fnCutsceneMove("Sanya", fTargetX, fTargetY, cfPlayerMoveSpeed)
        fnCutsceneBlocker()
        
        --Effects.
        iPlayerMoveDir = gci_Face_South
        fnCutscene([[ AudioManager_PlaySound("Puzzle|ChangeDirections") ]])
        fnCutscene(sClearingString)
    
    --Left arrow:
    elseif(iObjectAtPlayer == gciPuzzleFight_Object_ArrowLf) then
        bKeepMoving = true
            
        --Move to position.
        local fTargetX = gzaPuzzleFight.fnGetWorldX(iPlayerX) / gciSizePerTile
        local fTargetY = gzaPuzzleFight.fnGetWorldY(iPlayerY) / gciSizePerTile
        fnCutsceneMove("Sanya", fTargetX, fTargetY, cfPlayerMoveSpeed)
        fnCutsceneBlocker()
        
        --Effects.
        iPlayerMoveDir = gci_Face_West
        fnCutscene([[ AudioManager_PlaySound("Puzzle|ChangeDirections") ]])
        fnCutscene(sClearingString)
    
    --Boost! Keeps moving, but plays a scene.
    elseif(iObjectAtPlayer == gciPuzzleFight_Object_Boost) then
        bKeepMoving = true
        
        --Attack power bonus.
        iAttackDamageCurrent = iAttackDamageCurrent + iAttackDamageBase
        gzaPuzzleFight.bAttackUpThisTurn = true
        
        --Move to this position.
        local fTargetX = gzaPuzzleFight.fnGetWorldX(iPlayerX) / gciSizePerTile
        local fTargetY = gzaPuzzleFight.fnGetWorldY(iPlayerY) / gciSizePerTile
        fnCutsceneMove("Sanya", fTargetX, fTargetY, cfPlayerMoveSpeed)
        fnCutsceneBlocker()
        
        --Print a title in-world.
        local sString = "PuzzleFight_SetProperty(\"Register Floatfade Notifier\", " .. (fWorldX * gciSizePerTile) .. ", " .. (fWorldY * gciSizePerTile) .. ", \"Root/Images/Sprites/PuzzleBattle/Field|World Power Up\")"
        fnCutscene(sString)
        fnCutscene([[ AudioManager_PlaySound("Puzzle|PowerUp") ]])
        
        --Scene.
        fnCutsceneSetFrame(gzaPuzzleFight.sEntityName, "ArmUp")
        fnCutsceneJumpNPC(gzaPuzzleFight.sEntityName, gcfStdJumpSpeed * 1.3)
        fnCutsceneWait(35)
        fnCutsceneBlocker()
        fnCutsceneSetFrame(gzaPuzzleFight.sEntityName, "ArmUp")
        fnCutsceneJumpNPC(gzaPuzzleFight.sEntityName, gcfStdJumpSpeed * 1.3)
        fnCutsceneWait(35)
        fnCutsceneBlocker()
        fnCutsceneSetFrame(gzaPuzzleFight.sEntityName, "ArmUp")
        fnCutsceneJumpNPC(gzaPuzzleFight.sEntityName, gcfStdJumpSpeed * 1.3)
        fnCutsceneWait(35)
        fnCutsceneBlocker()
        fnCutscene(sClearingString)
        fnCutsceneSetFrame(gzaPuzzleFight.sEntityName, "Null")
    
    --Guard! Keeps moving, but plays a scene.
    elseif(iObjectAtPlayer == gciPuzzleFight_Object_Guard) then
        bKeepMoving = true
        
        --Flag.
        gzaPuzzleFight.bPerfectGuardThisTurn = true
        
        --Move to this position.
        local fTargetX = gzaPuzzleFight.fnGetWorldX(iPlayerX) / gciSizePerTile
        local fTargetY = gzaPuzzleFight.fnGetWorldY(iPlayerY) / gciSizePerTile
        fnCutsceneMove("Sanya", fTargetX, fTargetY, cfPlayerMoveSpeed)
        fnCutsceneBlocker()
        
        --Print a title in-world.
        local sString = "PuzzleFight_SetProperty(\"Register Floatfade Notifier\", " .. (fWorldX * gciSizePerTile) .. ", " .. (fWorldY * gciSizePerTile) .. ", \"Root/Images/Sprites/PuzzleBattle/Field|World Perfect Guard\")"
        fnCutscene(sString)
        fnCutscene([[ AudioManager_PlaySound("Puzzle|PowerUp") ]])
        
        --Scene.
        fnCutsceneSetFrame(gzaPuzzleFight.sEntityName, "ArmUp")
        fnCutsceneJumpNPC(gzaPuzzleFight.sEntityName, gcfStdJumpSpeed)
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        fnCutsceneSetFrame(gzaPuzzleFight.sEntityName, "ArmUp")
        fnCutsceneJumpNPC(gzaPuzzleFight.sEntityName, gcfStdJumpSpeed)
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        fnCutsceneSetFrame(gzaPuzzleFight.sEntityName, "ArmUp")
        fnCutsceneJumpNPC(gzaPuzzleFight.sEntityName, gcfStdJumpSpeed)
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        fnCutscene(sClearingString)
        fnCutsceneSetFrame(gzaPuzzleFight.sEntityName, "Null")
    
    --Melee. Stops movement.
    elseif(iObjectAtPlayer == gciPuzzleFight_Object_Melee) then
        bHitAction = true
    
    --Ranged. Stops movement.
    elseif(iObjectAtPlayer == gciPuzzleFight_Object_Ranged) then
        bHitAction = true
    
    --Empty tile.
    else
        bKeepMoving = true
    end
    
    -- |[ ============================== Failure Checks ============================== ]|
    -- |[Out of Bound Check]|
    --If the player hits an edge, run this.
    if(iPlayerX < 0 or iPlayerY < 0 or iPlayerX >= 7 or iPlayerY >= 7) then
            
        --Move to position.
        local fTargetX = gzaPuzzleFight.fnGetWorldX(iPlayerX) / gciSizePerTile
        local fTargetY = gzaPuzzleFight.fnGetWorldY(iPlayerY) / gciSizePerTile
        fnCutsceneMove("Sanya", fTargetX, fTargetY, cfPlayerMoveSpeed)
        fnCutsceneBlocker()
    
        --Have the player character fall on their face.
        fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
        fnCutsceneSetFrame(gzaPuzzleFight.sEntityName, "Wounded")
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Sanya:[VOICE|Sanya] Argh![P] I went out of bounds!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Stand up.
        fnCutsceneSetFrame(gzaPuzzleFight.sEntityName, "Crouch")
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneSetFrame(gzaPuzzleFight.sEntityName, "Null")
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        break
    end
    
    -- |[Fall Over]|
    --If the player has no movement direction, and didn't hit an action, they get a stumble.
    if(iPlayerMoveDir == -1 and bHitAction == false) then
    
        --Have the player character fall on their face.
        local fWorldX = gzaPuzzleFight.fnGetWorldX(iPlayerX) / 16
        local fWorldY = gzaPuzzleFight.fnGetWorldY(iPlayerY) / 16
        fnCutsceneMove(gzaPuzzleFight.sEntityName, fWorldX + 1.0, fWorldY, 2.00)
        fnCutsceneBlocker()
        fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
        fnCutsceneSetFrame(gzaPuzzleFight.sEntityName, "Wounded")
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Sanya:[VOICE|Sanya] Ouch![P] I forgot I need to have an arrow under me before I get to move anywhere![B][C]") ]])
        fnCutscene([[ Append("Sanya:[VOICE|Sanya] Better make that priority one when lining up tiles!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Stand up.
        fnCutsceneSetFrame(gzaPuzzleFight.sEntityName, "Crouch")
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneSetFrame(gzaPuzzleFight.sEntityName, "Null")
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Move.
        fnCutsceneMove(gzaPuzzleFight.sEntityName, fWorldX, fWorldY, 1.00)
        fnCutsceneWait(25)
        fnCutsceneBlocker()

        --Break out of the move loop.
        break
    end

    -- |[ ================================= Attacking ================================ ]|
    -- |[Attack!]|
    --If the player hit an attack icon, now is the time to strike!
    if(bHitAction) then
        -- |[Melee Attack]|
        if(iObjectAtPlayer == gciPuzzleFight_Object_Melee) then
        
            --Figure out which enemies are immediately adjacent. They all get attacked.
            local iNSlot = gzaPuzzleFight.fnGetEnemyInSlot(iPlayerX, iPlayerY - 1)
            local iESlot = gzaPuzzleFight.fnGetEnemyInSlot(iPlayerX + 1, iPlayerY)
            local iSSlot = gzaPuzzleFight.fnGetEnemyInSlot(iPlayerX, iPlayerY + 1)
            local iWSlot = gzaPuzzleFight.fnGetEnemyInSlot(iPlayerX - 1, iPlayerY)
            
            --Move to position.
            local fTargetX = gzaPuzzleFight.fnGetWorldX(iPlayerX) / gciSizePerTile
            local fTargetY = gzaPuzzleFight.fnGetWorldY(iPlayerY) / gciSizePerTile
            fnCutsceneMove("Sanya", fTargetX, fTargetY, cfPlayerMoveSpeed)
            fnCutsceneBlocker()
            
            --If all targets missed, show this scene:
            if(iNSlot == -1 and iESlot == -1 and iSSlot == -1 and iWSlot == -1) then
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ Append("Sanya:[VOICE|Sanya] ...[B][C]") ]])
                fnCutscene([[ Append("Sanya:[VOICE|Sanya] Darn it![P] There's no one in range to melee!") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(25)
                fnCutsceneBlocker()
            
            --At least one target. Show this title.
            else
                fnCutscene([[ PuzzleFight_SetProperty("Title Print", "Melee Attack!") ]])
                fnCutsceneWait(65)
                fnCutsceneBlocker()
            end
            
            --Run these. They do nothing if the slot is -1.
            fnPuzzleMeleeAttack(gci_Face_North, iNSlot, iPlayerX, iPlayerY, iAttackDamageCurrent)
            fnPuzzleMeleeAttack(gci_Face_East,  iESlot, iPlayerX, iPlayerY, iAttackDamageCurrent)
            fnPuzzleMeleeAttack(gci_Face_South, iSSlot, iPlayerX, iPlayerY, iAttackDamageCurrent)
            fnPuzzleMeleeAttack(gci_Face_West,  iWSlot, iPlayerX, iPlayerY, iAttackDamageCurrent)
        
        -- |[Ranged Attack]|
        elseif(iObjectAtPlayer == gciPuzzleFight_Object_Ranged) then
        
            --Figure out the closest enemy in each cardinal direction. They each get attacked.
            local iNSlot = gzaPuzzleFight.fnGetEnemyInLine(iPlayerX, iPlayerY,  0, -1)
            local iESlot = gzaPuzzleFight.fnGetEnemyInLine(iPlayerX, iPlayerY,  1,  0)
            local iSSlot = gzaPuzzleFight.fnGetEnemyInLine(iPlayerX, iPlayerY,  0,  1)
            local iWSlot = gzaPuzzleFight.fnGetEnemyInLine(iPlayerX, iPlayerY, -1,  0)
            
            --Move to position.
            local fTargetX = gzaPuzzleFight.fnGetWorldX(iPlayerX) / gciSizePerTile
            local fTargetY = gzaPuzzleFight.fnGetWorldY(iPlayerY) / gciSizePerTile
            fnCutsceneMove("Sanya", fTargetX, fTargetY, cfPlayerMoveSpeed)
            fnCutsceneBlocker()
            
            --If all targets missed, show this scene:
            if(iNSlot == -1 and iESlot == -1 and iSSlot == -1 and iWSlot == -1) then
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ Append("Sanya:[VOICE|Sanya] ...[B][C]") ]])
                fnCutscene([[ Append("Sanya:[VOICE|Sanya] Darn it![P] I can only shoot an enemy in the same row or column as me!") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(25)
                fnCutsceneBlocker()
            
            --At least one target. Show this title.
            else
                fnCutscene([[ PuzzleFight_SetProperty("Title Print", "Ranged Attack!") ]])
                fnCutsceneWait(65)
                fnCutsceneBlocker()
            end
        
            --Run these. They do nothing if the slot is -1.
            fnPuzzleRangedAttack(gci_Face_North, iNSlot, iPlayerX, iPlayerY, iAttackDamageCurrent)
            fnPuzzleRangedAttack(gci_Face_East,  iESlot, iPlayerX, iPlayerY, iAttackDamageCurrent)
            fnPuzzleRangedAttack(gci_Face_South, iSSlot, iPlayerX, iPlayerY, iAttackDamageCurrent)
            fnPuzzleRangedAttack(gci_Face_West,  iWSlot, iPlayerX, iPlayerY, iAttackDamageCurrent)
        end

    end

    -- |[ ================================== Moving ================================== ]|
    -- |[Movement]|
    --If this object does not stop movement, animate moving in the given direction.
    if(bKeepMoving) then
        
        --Setup.
        local iNewX = iPlayerX
        local iNewY = iPlayerY
        
        --Compute new position.
        if(iPlayerMoveDir == gci_Face_North) then
            iNewY = iNewY - 1
        elseif(iPlayerMoveDir == gci_Face_East) then
            iNewX = iNewX + 1
        elseif(iPlayerMoveDir == gci_Face_South) then
            iNewY = iNewY + 1
        elseif(iPlayerMoveDir == gci_Face_West) then
            iNewX = iNewX - 1
        end
        
        --Change the player's position for the next cycle.
        iPlayerX = iNewX
        iPlayerY = iNewY
        
    -- |[Stopped For Action]|
    --This tile performs an action so do it.
    else
        break
    end
end