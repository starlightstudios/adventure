-- |[ ============================= Puzzle Ranged Attack Function ============================== ]|
--Called when the player attacks an enemy with a ranged attack.
function fnPuzzleRangedAttack(piDirection, piEnemySlot, piPlayerPosX, piPlayerPosY, piDamage)

    -- |[Error Checks]|
    --Argument check.
    if(piDirection  == nil) then return end
    if(piEnemySlot  == nil) then return end
    if(piPlayerPosX == nil) then return end
    if(piPlayerPosY == nil) then return end
    if(piDamage     == nil) then return end
    
    --Error check: Slot can't be -1.
    if(piEnemySlot == -1) then return end
    
    -- |[Setup]|
    --Constants.
    
    --Variables.
    local iEnemyHealth = PuzzleFight_GetProperty("Enemy Health")
    local sPlayerName = gzaPuzzleFight.sEntityName
    local sTargetName = gzaPuzzleFight.zaEnemies[piEnemySlot].sActorName
    local fWorldX = gzaPuzzleFight.fnGetWorldX(piPlayerPosX) / gciSizePerTile
    local fWorldY = gzaPuzzleFight.fnGetWorldY(piPlayerPosY) / gciSizePerTile
    
    -- |[Direction Resolve]|
    --Variables.
    local sAttackDir = "N"
    local sPainDir = "S"
    
    --Set by direction.
    if(piDirection == gci_Face_North) then
        sAttackDir = "N"
        sPainDir = "S"
    elseif(piDirection == gci_Face_East) then
        sAttackDir = "E"
        sPainDir = "W"
    elseif(piDirection == gci_Face_South) then
        sAttackDir = "S"
        sPainDir = "N"
    elseif(piDirection == gci_Face_West) then
        sAttackDir = "W"
        sPainDir = "E"
    end

    --Entities always start by facing each other.
    fnCutsceneFaceTarget(sPlayerName, sTargetName)
    fnCutsceneFaceTarget(sTargetName, sPlayerName)

    --Weapon draw sequence.
    --fnCutsceneMove(sPlayerName, fTargetX, fTargetY, cfMoveSpeed)
    --fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Attack power up!
    if(gzaPuzzleFight.bAttackUpThisTurn == true) then
        fnCutscene([[ AudioManager_PlaySound("Puzzle|PowerUp") ]])
        local sString = "PuzzleFight_SetProperty(\"Register Floatfade Notifier\", " .. (fWorldX * gciSizePerTile) .. ", " .. (fWorldY * gciSizePerTile) .. ", \"Root/Images/Sprites/PuzzleBattle/Field|World Power Up\")"
        fnCutscene(sString)
        fnCutsceneWait(35)
        fnCutsceneBlocker()
    end
    
    --String for damage.
    local sDamageString = "PuzzleFight_SetProperty(\"Enemy Health\", " .. (iEnemyHealth - piDamage) .. ")"
    
    --Firing sequence.
    fnCutsceneSetFrame(sPlayerName, "Shoot" .. sAttackDir .. "0")
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneSetFrame(sPlayerName, "Shoot" .. sAttackDir .. "1")
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutscene(sDamageString)
    fnCutscenePlaySound("Firearm|RifleFire")
    fnCutsceneSetFrame(sPlayerName, "Shoot" .. sAttackDir .. "2")
    fnCutsceneSetFrame(sTargetName, "Pain" .. sPainDir .. "0")
    fnCutsceneWait(7)
    fnCutsceneBlocker()
    fnCutsceneSetFrame(sPlayerName, "Shoot" .. sAttackDir .. "3")
    fnCutsceneSetFrame(sTargetName, "Pain" .. sPainDir .. "1")
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneSetFrame(sPlayerName, "Shoot" .. sAttackDir .. "1")
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutscenePlaySound("Firearm|WorkAction")
    fnCutsceneSetFrame(sPlayerName, "Shoot" .. sAttackDir .. "0")
    fnCutsceneSetFrame(sTargetName, "Null")
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    
    --Return to the starting position.
    fnCutsceneSetFrame(sPlayerName, "Null")
    fnCutsceneSetFrame(sTargetName, "Null")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
end
