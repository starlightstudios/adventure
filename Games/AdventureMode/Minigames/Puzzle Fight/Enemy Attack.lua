-- |[ ====================================== Enemy Attack ====================================== ]|
--Function that handles enemies attacking the player. Should be called in-stride when all enemies
-- and the player have moved to their attack start positions.
function fnEnemyAttack()

    -- |[Setup]|
    --Get positions.
    local fSanyaWorldX = gzaPuzzleFight.fnGetWorldX(3.0) / gciSizePerTile
    local fSanyaWorldY = gzaPuzzleFight.fnGetWorldY(3.0) / gciSizePerTile
    local fOdarWorldX  = gzaPuzzleFight.fnGetWorldX(4.0) / gciSizePerTile

    --Player's HP.
    local iPlayerHealth = PuzzleFight_GetProperty("Player Health")
    local iPlayerHealthAfter = iPlayerHealth - 20

    -- |[Animation]|
    --Enemy slams into the player for now. Starts by moving forward slowly then bam.
    if(gzaPuzzleFight.bPerfectGuardThisTurn == false) then
        local fPercent = 0.90
        local fMidwayX = (fSanyaWorldX + ((fOdarWorldX - fSanyaWorldX) * fPercent))
        fnCutsceneMove("Odar",     fMidwayX, fSanyaWorldY, 0.40)
        fnCutsceneMove("Odar", fSanyaWorldX, fSanyaWorldY, 2.50)
        fnCutsceneBlocker()
        
        --Impact. Plays a sound and changes the player's HP.
        fnCutscene("PuzzleFight_SetProperty(\"Player Health\", " .. iPlayerHealthAfter .. ")")
        fnCutscenePlaySound("World|Thump")
        fnCutsceneSetFrame("Sanya", "Crouch")
        fnMoveActor("Odar",  3.5, 3.0)
        fnCutsceneBlocker()
        fnCutsceneWait(35)
        fnCutsceneBlocker()
        fnCutsceneSetFrame("Sanya", "Null")
        fnCutsceneBlocker()
    
    -- |[Perfect Guard!]|
    --Player takes no damage.
    else
    
        --Setup.
        local sString = "PuzzleFight_SetProperty(\"Register Floatfade Notifier\", " .. (fSanyaWorldX * gciSizePerTile) .. ", " .. (fSanyaWorldY * gciSizePerTile) .. ", \"Root/Images/Sprites/PuzzleBattle/Field|World Perfect Guard\")"
    
        --Movement.
        local fPercent = 0.90
        local fMidwayX = (fSanyaWorldX + ((fOdarWorldX - fSanyaWorldX) * fPercent))
        fnCutsceneMove("Odar",     fMidwayX, fSanyaWorldY, 0.40)
        fnCutsceneMove("Odar", fSanyaWorldX, fSanyaWorldY, 2.50)
        fnCutsceneBlocker()
    
        --Dodge!
        fnCutscenePlaySound("Combat|AttackMiss")
        fnMoveActor("Odar",  3.5, 3.0)
        fnMoveActor("Sanya", 2.5, 3.0)
        fnCutsceneBlocker()
        fnCutscene([[ AudioManager_PlaySound("Puzzle|PowerUp") ]])
        fnCutscene(sString)
        fnCutsceneWait(45)
        fnCutsceneBlocker()
    
    end
                
    -- |[Next Turn]|
    --Reset flags and start the next turn.
    gzaPuzzleFight.bHasPrintedTitle = false
    fnCutscene([[ PuzzleFight_SetProperty("Game State", gciPuzzleFightState_PlayersTurn_Init) ]])

end