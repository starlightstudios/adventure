-- |[ ======================================= Move Actors ====================================== ]|
--Moves the given actors to their starting positions. Used at the start of each round.
function fnMoveActor(psActorName, pfTargetX, pfTargetY)
    
    -- |[Error Check]|
    --Check arguments.
    if(psActorName == nil) then return end
    if(pfTargetX   == nil) then return end
    if(pfTargetY   == nil) then return end
    
    -- |[Setup]|
    --Constants
    local ciMoveTicks = 25
    
    -- |[Player's Movement]|
    --Player's destination is the center of the board. Translate board coordinates to tile coordinates.
    local fWorldDestX = gzaPuzzleFight.fnGetWorldX(pfTargetX)
    local fWorldDestY = gzaPuzzleFight.fnGetWorldY(pfTargetY)
    local fTileDestX = fWorldDestX / gciSizePerTile
    local fTileDestY = fWorldDestY / gciSizePerTile
    
    --Action.
    fnCutsceneJumpTo(psActorName, fTileDestX, fTileDestY, ciMoveTicks)
    
end
