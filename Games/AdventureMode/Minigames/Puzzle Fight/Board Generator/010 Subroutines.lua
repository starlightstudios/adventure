-- |[ ====================================== Render Board ====================================== ]|
--Renders the board to the console for diagnostics.
function fnRenderBoard(pzaBoard)
    
    --Argument check.
    if(pzaBoard == nil) then return end

    --Render.
    for y = 0, gzaPuzzleFight.iBoardHeight-1, 1 do
        io.write(" ")
        for x = 0, gzaPuzzleFight.iBoardWidth-1, 1 do
            
            --Check if an enemy is in this spot.
            local bHasEnemy = false
            for i = 1, gzaPuzzleFight.iEnemiesTotal, 1 do
                if(gzaPuzzleFight.zaEnemies[i].iX == x and gzaPuzzleFight.zaEnemies[i].iY == y) then
                    bHasEnemy = true
                end
            end
            
            --Render.
            if(x == 3 and y == 3) then
                io.write("@ ")
            elseif(bHasEnemy) then
                io.write("* ")
            elseif(pzaBoard[x][y].iCode == gciPuzzleFight_Object_Empty) then
                io.write("E ")
            elseif(pzaBoard[x][y].iCode == gciPuzzleFight_Object_ArrowUp) then
                io.write("^ ")
            elseif(pzaBoard[x][y].iCode == gciPuzzleFight_Object_ArrowRt) then
                io.write("> ")
            elseif(pzaBoard[x][y].iCode == gciPuzzleFight_Object_ArrowDn) then
                io.write("v ")
            elseif(pzaBoard[x][y].iCode == gciPuzzleFight_Object_ArrowLf) then
                io.write("< ")
            elseif(pzaBoard[x][y].iCode == gciPuzzleFight_Object_Boost) then
                io.write("B ")
            elseif(pzaBoard[x][y].iCode == gciPuzzleFight_Object_Guard) then
                io.write("G ")
            elseif(pzaBoard[x][y].iCode == gciPuzzleFight_Object_Melee) then
                io.write("M ")
            elseif(pzaBoard[x][y].iCode == gciPuzzleFight_Object_Ranged) then
                io.write("R ")
            else
                io.write("! ")
            end
        end
        io.write("\n")
    end

end

-- |[ ===================================== Random Cardinal ==================================== ]|
--Returns a random cardinal direction from the gci_Face_ series.
function fnRandomCardinal()
    local iRoll = LM_GetRandomNumber(1, 4)
    if(iRoll == 1) then return gci_Face_North end
    if(iRoll == 2) then return gci_Face_East end
    if(iRoll == 3) then return gci_Face_South end
    return gci_Face_West
end

-- |[ ============================== Modify Position By Direction ============================== ]|
--Given an X/Y position and a direction in the gci_Face_ series, modifies that position.
function fnModifyPositionByDirection(piX, piY, piDirection)
    if(piX         == nil) then return -1, -1 end
    if(piY         == nil) then return -1, -1 end
    if(piDirection == nil) then return -1, -1 end
    
    if(piDirection == gci_Face_North) then
        return piX, piY - 1
    elseif(piDirection == gci_Face_East) then
        return piX + 1, piY
    elseif(piDirection == gci_Face_South) then
        return piX, piY + 1
    elseif(piDirection == gci_Face_West) then
        return piX - 1, piY
    end
    
    --Error case.
    return piX, piY
end

-- |[ =================================== Is Point In Board ==================================== ]|
--Returns true if the given X/Y is within the board area.
function fnIsPointInBoard(piX, piY)
    if(piX == nil) then return false end
    if(piY == nil) then return false end
    if(piX < 0 or piX >= gzaPuzzleFight.iBoardWidth) then return false end
    if(piY < 0 or piY >= gzaPuzzleFight.iBoardHeight) then return false end
    return true
end

-- |[ ==================================== Is Point Occupied =================================== ]|
--Returns true if the given X/Y tile is occupied. This checks both objects and enemies. The player
-- start position is not considered. Going off the edge of the board is considered occupied.
function fnIsPointOccupied(pzaBoard, piX, piY)
    
    --Arg check.
    if(pzaBoard == nil) then return true end
    if(piX      == nil) then return true end
    if(piY      == nil) then return true end
    
    --Range check.
    if(piX < 0 or piX >= gzaPuzzleFight.iBoardWidth)  then return true end
    if(piY < 0 or piY >= gzaPuzzleFight.iBoardHeight) then return true end
    
    --Board contains an object at the listed location.
    if(pzaBoard[piX][piY].iCode ~= gciPuzzleFight_Object_Empty) then
        return true 
    end
    
    --Scan all enemies to see if we run into one.
    for i = 1, gzaPuzzleFight.iEnemiesTotal, 1 do
        if(gzaPuzzleFight.zaEnemies[i].iX == piX and gzaPuzzleFight.zaEnemies[i].iY == piY) then 
            return true 
        end
    end
    
    --All checks failed, spot is empty.
    return false
end