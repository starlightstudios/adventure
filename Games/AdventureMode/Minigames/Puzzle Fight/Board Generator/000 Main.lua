-- |[ ================================== Puzzle Board Generate ================================= ]|
--Generates a puzzle board. Can optionally have a solution for a given number of moves, or else
-- be totally randomized.
--Note: Enemies must be placed on the board before generation occurs. If no enemies are on the
-- board then the random algorithm is used.
function fnPuzzleBoardGenerate(piMoves, pbMustBeSolvable)

    -- |[ ============================== Setup ============================= ]|
    -- |[Debug]|
    --io.write("fnPuzzleBoardGenerate: Running.\n")

    -- |[Error Checks]|
    --Verify arguments.
    if(piMoves          == nil) then return end
    if(pbMustBeSolvable == nil) then return end
    
    --No enemies. Remove solvable requirement.
    if(gzaPuzzleFight.iEnemiesTotal < 1) then
        pbMustBeSolvable = false
    end
    
    -- |[Board Setup]|
    --This structure holds the board's current state.
    local zaBoard = {}
    for x = 0, gzaPuzzleFight.iBoardWidth-1, 1 do
        zaBoard[x] = {}
        for y = 0, gzaPuzzleFight.iBoardHeight-1, 1 do
            zaBoard[x][y] = {}
            zaBoard[x][y].iCode = gciPuzzleFight_Object_Empty
        end
    end
        
    -- |[ ======================= Completely Random ======================== ]|
    --Places items and arrows at random.
    if(pbMustBeSolvable == false) then
        --io.write(" Board is random.\n")
        
        
    -- |[ ============================ Solvable ============================ ]|
    --Constructs a "path" towards the target and then scrambles the board by the number of moves provided.
    else
    
        -- |[ ============= Setup ============ ]|
        --Debug.
        --io.write(" Board is solvable in " .. piMoves .. " moves.\n")
    
        --Select an enemy to create a mandated path towards. It's possible the board can be changed to hit
        -- more than one enemy, but we aren't concerned with that.
        local iEnemySlot = LM_GetRandomNumber(1, gzaPuzzleFight.iEnemiesTotal)
        local iEnemyX = gzaPuzzleFight.zaEnemies[iEnemySlot].iX
        local iEnemyY = gzaPuzzleFight.zaEnemies[iEnemySlot].iY
        --io.write(" Enemy position: " .. iEnemyX .. "x" .. iEnemyY .. "\n")
        
        --Variables.
        local iTargetX = -1
        local iTargetY = -1

        --Decide if this is a melee or ranged case.
        local bIsMelee = true
        local iAttackRoll = LM_GetRandomNumber(1, 100)
        if(iAttackRoll < 60) then
            bIsMelee = true
        else
            bIsMelee = false
        end
        
        -- |[ ======== Attack Object ========= ]|
        --Place.
        iTargetX, iTargetY = fnPlaceAttackObject(zaBoard, bIsMelee, iEnemyX, iEnemyY)
        
        -- |[ ======= Arrow Generation ======= ]|
        --Now start placing arrows to allow the player to move to the target position. How many arrows are required
        -- is randomized, with the ostensible minimum being 1 (if the target is on an unbroken line with the start point)
        -- or 2 (if not).
        --Debug.
        --io.write(" Generating movement arrows.\n")
        
        --Create a backup of the board if it has to get scrapped.
        local zaStorageBoard = {}
        for x = 0, gzaPuzzleFight.iBoardWidth-1, 1 do
            zaStorageBoard[x] = {}
            for y = 0, gzaPuzzleFight.iBoardHeight-1, 1 do
                zaStorageBoard[x][y] = {}
                zaStorageBoard[x][y].iCode = zaBoard[x][y].iCode
            end
        end
        
        --Run the path generator repeatedly until it returns a valid result.
        local iArrowAttempts = 100
        local bIsValid = false
        while(bIsValid == false) do
        
            --Run.
            iArrowAttempts = iArrowAttempts - 1
            local iArrowsPlaced = fnBuildArrowPathToTarget(3, 3, iTargetX, iTargetY, zaBoard)
            
            --If this value came back -1, then the routine failed. Restore the board to its starting state.
            if(iArrowsPlaced == -1) then
                
                --Debug:
                --io.write("Failed board. Rendering.\n")
                --fnRenderBoard(zaBoard)
                
                --Restore.
                for x = 0, gzaPuzzleFight.iBoardWidth-1, 1 do
                    for y = 0, gzaPuzzleFight.iBoardHeight-1, 1 do
                        zaBoard[x][y].iCode = zaStorageBoard[x][y].iCode
                    end
                end
            
            --Success! The board generated a valid path.
            else
                break
            end
            
            --Failure, too many attempts.
            if(iArrowAttempts < 1) then
                io.write("Generation failed. Could not build arrow path in 100 attempts.\n")
                return
            end
        end
        
        -- |[ =========== Powerups =========== ]|
        --Optionally places powerups on the board. Powerups don't always spawn.
        local iaPowerups = {}
        local iPowerupThreshold = 70
        while(true) do
        
            --Roll to spawn a powerup.
            local iSpawnRoll = LM_GetRandomNumber(1, 100)
        
            --If it's above the spawn threshold, we're done.
            if(iSpawnRoll > iPowerupThreshold) then break end
        
            --Roll which kind to spawn.
            local iKindRoll = LM_GetRandomNumber(1, 100)
            if(iKindRoll <= 60) then
                iKindRoll = gciPuzzleFight_Object_Boost
            elseif(iKindRoll <= 100) then
                iKindRoll = gciPuzzleFight_Object_Guard
            end

            --Add it to the list.
            table.insert(iaPowerups, iKindRoll)
            
            --Reduce the chance of another powerup.
            iPowerupThreshold = math.floor(iPowerupThreshold * 0.55)
            
        end
        
        --Place powerups. This does nothing if the powerup list was empty.
        fnPlacePowerups(zaBoard, iaPowerups)
        
        -- |[ =========== Scramble =========== ]|
        --Scramble the board by the requested number of moves.
        local iPreviousType = -1
        local iPreviousRowCol = -1
        local iPreviousDir = -1
        
        --Storage structure. Stores the board states. If a scramble results in the board returning to
        -- a previous state, the scramble is discarded.
        local zaBoardStorage = {}
        
        -- |[Create Backup]|
        --Create a board copy.
        local zStoredBoard = {}
        for x = 0, gzaPuzzleFight.iBoardWidth-1, 1 do
            zStoredBoard[x] = {}
            for y = 0, gzaPuzzleFight.iBoardHeight-1, 1 do
                zStoredBoard[x][y] = {}
                zStoredBoard[x][y].iCode = zaBoard[x][y].iCode
            end
        end
        table.insert(zaBoardStorage, zStoredBoard)
        
        -- |[Scramble]|
        --Begin scrambling.
        for i = 1, piMoves, 1 do
            
            -- |[Run Scrambling]|
            --Roll which kind of scramble.
            local iRoll = LM_GetRandomNumber(1, 2)
            
            --Horizontal scramble:
            if(iRoll == 1) then
                fnScrambleRow(zaBoard)
                
            --Vertical scramble:
            else
                fnScrambleColumn(zaBoard)
            end
            
            -- |[Duplicate Check]|
            --Check the board state against all stored boards.
            local bIsUnique = true
            for p = 1, #zaBoardStorage, 1 do
            
                --Fast-access pointer.
                local zStoredBoard = zaBoardStorage[p]
            
                --Scan the board against the previous version.
                local bIsIdentical = true
                for x = 0, gzaPuzzleFight.iBoardWidth-1, 1 do
                    for y = 0, gzaPuzzleFight.iBoardHeight-1, 1 do
                        
                        if(zStoredBoard[x][y].iCode ~= zaBoard[x][y].iCode) then
                            bIsIdentical = false
                            break
                        end
                    end
                    
                    if(bIsIdentical == false) then break end
                    
                end
            
                --The boards were identical. Fail out here.
                if(bIsIdentical == true) then
                    bIsUnique = false
                    break
                end
            
            end
            
            -- |[Unique Board State]|
            --Board state is unique, use it.
            if(bIsUnique == true) then
            
                --Create a board copy.
                local zStoredBoard = {}
                for x = 0, gzaPuzzleFight.iBoardWidth-1, 1 do
                    zStoredBoard[x] = {}
                    for y = 0, gzaPuzzleFight.iBoardHeight-1, 1 do
                        zStoredBoard[x][y] = {}
                        zStoredBoard[x][y].iCode = zaBoard[x][y].iCode
                    end
                end
                table.insert(zaBoardStorage, zStoredBoard)
            
            -- |[Duplicate Board State]|
            --Board is not unique. Fail, subtract 1 from the move count, redo the board.
            else
            
                --Retrieve previous board.
                local iLastBoard = #zaBoardStorage
                local zPreviousBoard = zaBoardStorage[iLastBoard]
                
                --Copy previous board to current.
                for x = 0, gzaPuzzleFight.iBoardWidth-1, 1 do
                    for y = 0, gzaPuzzleFight.iBoardHeight-1, 1 do
                        zaBoard[x][y].iCode = zPreviousBoard[x][y].iCode
                    end
                end
                
                --Subtract one from the move count.
                i = i - 1
            end
        end
        
        -- |[ ============ Upload ============ ]|
        --Debug.
        --io.write(" Uploading board state to C++.\n")
        
        --Spawn position. They start the center and radiate outwards.
        local fSpawnX = (gzaPuzzleFight.iBoardStartX * gciSizePerTile) + (3.0 * gzaPuzzleFight.iTileW * gciSizePerTile)
        local fSpawnY = (gzaPuzzleFight.iBoardStartY * gciSizePerTile) + (3.0 * gzaPuzzleFight.iTileH * gciSizePerTile)
        
        --Spawn the needed objects. When the objects finish moving, the next tutorial block is
        -- in a set of code down this handler.
        for x = 0, gzaPuzzleFight.iBoardWidth-1, 1 do
            for y = 0, gzaPuzzleFight.iBoardHeight-1, 1 do
                if(zaBoard[x][y].iCode ~= gciPuzzleFight_Object_Empty) then
                    PuzzleFight_SetProperty("Spawn Board Object", zaBoard[x][y].iCode, x, y, fSpawnX, fSpawnY)
                end
            end
        end
        
    end
    
    -- |[Debug]|
    --io.write("Board generation was successful.\n")
end