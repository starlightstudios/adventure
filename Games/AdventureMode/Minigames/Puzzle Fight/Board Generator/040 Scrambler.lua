-- |[ ================================== Row/Column Scrambler ================================== ]|
--Mirror functions that move a random row or column by 1 in either direction. First one moves a row
-- left or right, second one moves a column up or down.
--Both functions will return the index of the row/column they moved, and 0 to represent moving left
-- or up, 1 to represent right or down.
function fnScrambleRow(pzaBoard)
    
    -- |[Error Check]|
    if(pzaBoard         == nil) then return -1, -1 end

    -- |[Setup]|
    --Setup.
    local iRowToScramble = -1

    -- |[Resolve Row]|
    --Create a list of all rows. We can only scramble a row that actually has something in it.
    local zaList = {}
    for p = 0, gzaPuzzleFight.iBoardHeight-1, 1 do
        table.insert(zaList, p)
    end

    --Roll a random row. Remove it from the list.
    while(#zaList > 0) do
        local iModifyRoll = LM_GetRandomNumber(1, #zaList)
        local iRow = zaList[iModifyRoll]
        table.remove(zaList, iModifyRoll)
        
        --Check if this row contains at least one object. This excludes enemies and the player.
        local bFoundObject = false
        for x = 0, gzaPuzzleFight.iBoardWidth-1, 1 do
            if(pzaBoard[x][iRow].iCode ~= gciPuzzleFight_Object_Empty) then
                iRowToScramble = iRow
                bFoundObject = true
                break
            end
        end
        
        --Object found in row, scramble it.
        if(bFoundObject == true) then
            break
        end
    end

    --If we managed to exhaust the rows and not find one to scramble, fail.
    if(iRowToScramble == -1) then return -1, -1 end

    -- |[Scramble]|
    --Easy-access numbers.
    local y = iRowToScramble
    local r = gzaPuzzleFight.iBoardWidth-1
    
    --Roll which direction to move the row. Don't let it unmatch the previous movement.
    local iDirectionRoll = LM_GetRandomNumber(0, 1)
    
    --Move it left.
    if(iDirectionRoll == 0) then
        local iLeftmost = pzaBoard[0][y].iCode
        for x = 0, r-1, 1 do
            pzaBoard[x][y].iCode = pzaBoard[x+1][y].iCode
        end
        pzaBoard[r][y].iCode = iLeftmost
        
    --Move it right.
    else
        local iRightmost = pzaBoard[r][y].iCode
        for x = r, 1, -1 do
            pzaBoard[x][y].iCode = pzaBoard[x-1][y].iCode
        end
        pzaBoard[0][y].iCode = iRightmost

    end

    -- |[Finish Up]|
    return iRowToScramble, iDirectionRoll
end

--Same as above, but scrambles a column instead.
function fnScrambleColumn(pzaBoard)
    
    -- |[Error Check]|
    if(pzaBoard         == nil) then return -1, -1 end

    -- |[Setup]|
    --Setup.
    local iColToScramble = -1

    -- |[Resolve Row]|
    --Create a list of all columns. We can only scramble a column that actually has something in it.
    local zaList = {}
    for p = 0, gzaPuzzleFight.iBoardWidth-1, 1 do
        table.insert(zaList, p)
    end

    --Roll a random column. Remove it from the list.
    while(#zaList > 0) do
        local iModifyRoll = LM_GetRandomNumber(1, #zaList)
        local iCol = zaList[iModifyRoll]
        table.remove(zaList, iModifyRoll)
        
        --Check if this column contains at least one object. This excludes enemies and the player.
        local bFoundObject = false
        for y = 0, gzaPuzzleFight.iBoardHeight-1, 1 do
            if(pzaBoard[iCol][y].iCode ~= gciPuzzleFight_Object_Empty) then
                iColToScramble = iCol
                bFoundObject = true
                break
            end
        end
        
        --Object found in row, scramble it.
        if(bFoundObject == true) then
            break
        end
    end

    --If we managed to exhaust the columns and not find one to scramble, fail.
    if(iColToScramble == -1) then return -1, -1 end

    -- |[Scramble]|
    --Easy-access numbers.
    local x = iColToScramble
    local b = gzaPuzzleFight.iBoardHeight-1
    
    --Roll which direction to move the column. Don't let it undo the previous action.
    local iDirectionRoll = LM_GetRandomNumber(0, 1)
    
    --Move it up.
    if(iDirectionRoll == 0) then
        local iTopmost = pzaBoard[x][0].iCode
        for y = 0, b-1, 1 do
            pzaBoard[x][y].iCode = pzaBoard[x][y+1].iCode
        end
        pzaBoard[x][b].iCode = iTopmost
        
    --Move it down.
    else
        local iBottommost = pzaBoard[x][b].iCode
        for y = b, 1, -1 do
            pzaBoard[x][y].iCode = pzaBoard[x][y-1].iCode
        end
        pzaBoard[x][0].iCode = iBottommost

    end

    -- |[Finish Up]|
    return iColToScramble, iDirectionRoll
end
