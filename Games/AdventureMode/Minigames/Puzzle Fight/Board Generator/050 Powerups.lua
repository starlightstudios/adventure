-- |[ ===================================== Place Powerups ===================================== ]|
--Runs over the board and places powerups from the provided array. May periodically place the powerups
-- off the "designated" path, requiring an extra move (or sometimes not!) for the player to get.
function fnPlacePowerups(pzaBoard, piaPowerups)
    
    -- |[ =========================== Error Check ========================== ]|
    --Check arguments
    if(pzaBoard == nil) then return end
    if(piaPowerups == nil) then return end
    
    --If the powerup array has no entries, this function wouldn't do anything. Exit.
    if(#piaPowerups < 1) then return end
    
    -- |[ ========================== Initial Walk ========================== ]|
    --Setup.
    local iActionsLeft = 200
    local iPlayerX = 3
    local iPlayerY = 3
    local iPlayerMoveDir = -1
    
    --Create a backup board. The board removes arrows as the player moves across
    -- it, which we don't do yet. So recreate the backup afterwards.
    local zaStorageBoard = {}
    for x = 0, gzaPuzzleFight.iBoardWidth-1, 1 do
        zaStorageBoard[x] = {}
        for y = 0, gzaPuzzleFight.iBoardHeight-1, 1 do
            zaStorageBoard[x][y] = {}
            zaStorageBoard[x][y].iCode = pzaBoard[x][y].iCode
        end
    end
    
    --Walk along the route designated for the player. Store each tile in an array.
    --io.write("Walking board.\n")
    local zaTilesWalked = {}
    while(true) do

        -- |[ ======== Setup ======= ]|
        -- |[Error Check]|
        --It should not be possible to do 200 actions in one go.
        iActionsLeft = iActionsLeft - 1
        if(iActionsLeft < 1) then
            io.write("Puzzle Fight Minigame Error: Broke action limit.\n")
            return
        end

        -- |[Setup]|
        --Get what object is at this position, and where the player is.
        local bEmptyTile = false
        local bKeepMoving = false
        local bHitAction = false
        --io.write("XY is " .. iPlayerX .. "x" .. iPlayerY .. "\n")
        local iObjectAtPlayer = pzaBoard[iPlayerX][iPlayerY].iCode
        pzaBoard[iPlayerX][iPlayerY].iCode = gciPuzzleFight_Object_Empty
        
        -- |[ === Object Resolve === ]|
        -- |[Object Type]|
        --Player is standing on an up arrow:
        if(iObjectAtPlayer == gciPuzzleFight_Object_ArrowUp) then
            bKeepMoving = true
            iPlayerMoveDir = gci_Face_North
        
        --Right arrow:
        elseif(iObjectAtPlayer == gciPuzzleFight_Object_ArrowRt) then
            bKeepMoving = true
            iPlayerMoveDir = gci_Face_East
        
        --Down arrow:
        elseif(iObjectAtPlayer == gciPuzzleFight_Object_ArrowDn) then
            bKeepMoving = true
            iPlayerMoveDir = gci_Face_South
        
        --Left arrow:
        elseif(iObjectAtPlayer == gciPuzzleFight_Object_ArrowLf) then
            bKeepMoving = true
            iPlayerMoveDir = gci_Face_West
        
        --Boost! Doesn't stop moving.
        elseif(iObjectAtPlayer == gciPuzzleFight_Object_Boost) then
            bKeepMoving = true
        
        --Guard! Doesn't stop moving.
        elseif(iObjectAtPlayer == gciPuzzleFight_Object_Guard) then
            bKeepMoving = true
        
        --Melee. Stops movement.
        elseif(iObjectAtPlayer == gciPuzzleFight_Object_Melee) then
            bHitAction = true
        
        --Ranged. Stops movement.
        elseif(iObjectAtPlayer == gciPuzzleFight_Object_Ranged) then
            bHitAction = true
        
        --Empty tile.
        else
            bKeepMoving = true
            bEmptyTile = true
        end
        
        -- |[ === Failure Checks === ]|
        -- |[Fall Over]|
        --If the player has no movement direction, and didn't hit an action, they get a stumble.
        if(iPlayerMoveDir == -1 and bHitAction == false) then
            break
        end

        -- |[ ===== Record Tile ==== ]|
        --If flagged, this tile gets added to the list of tiles we can put an object on.
        if(bEmptyTile) then
            table.insert(zaTilesWalked, {iPlayerX, iPlayerY})
        end
        
        -- |[ ======= Moving ======= ]|
        -- |[Movement]|
        --If this object does not stop movement, update the position.
        if(bKeepMoving) then
            
            --Compute new position.
            if(iPlayerMoveDir == gci_Face_North) then
                iPlayerY = iPlayerY - 1
            elseif(iPlayerMoveDir == gci_Face_East) then
                iPlayerX = iPlayerX + 1
            elseif(iPlayerMoveDir == gci_Face_South) then
                iPlayerY = iPlayerY + 1
            elseif(iPlayerMoveDir == gci_Face_West) then
                iPlayerX = iPlayerX - 1
            end
        
            --If the player hits an edge, stop the run.
            if(iPlayerX < 0 or iPlayerY < 0 or iPlayerX >= 7 or iPlayerY >= 7) then
                break
            end
            
        -- |[Stopped For Action]|
        --This tile performs an action so do it.
        else
            break
        end
    end
    
    -- |[ ========================= Item Placement ========================= ]|
    --We now have a list of all the tiles that the player must walk over that do not have an object in them yet.
    -- Randomly place powerups in them. First, restore the backup board.
    --io.write("Resetting board.\n")
    for x = 0, gzaPuzzleFight.iBoardWidth-1, 1 do
        for y = 0, gzaPuzzleFight.iBoardHeight-1, 1 do
            pzaBoard[x][y].iCode = zaStorageBoard[x][y].iCode
        end
    end
    
    --As long as we have powerups to place, or tiles to attempt to place them:
    --io.write("Placing items.\n")
    while(#zaTilesWalked > 0 and #piaPowerups > 0) do
        
        --Select a random tile and random powerup.
        local iTileSlot = LM_GetRandomNumber(1, #zaTilesWalked)
        local iItemSlot = LM_GetRandomNumber(1, #piaPowerups)
        
        --Get the X/Y.
        local iTileX = zaTilesWalked[iTileSlot][1]
        local iTileY = zaTilesWalked[iTileSlot][2]
        
        --Remove the tile from the list in all cases.
        table.remove(zaTilesWalked, iTileSlot)
        
        --First, do this roll. There is a 10% chance that we will place the item in an adjacent slot instead of this one.
        local iSpotRoll = LM_GetRandomNumber(1, 100)
        if(iSpotRoll <= 10) then
        
            --Roll which direction to place the item.
            local iDirectionRoll = fnRandomCardinal()
            iTileX, iTileY = fnModifyPositionByDirection(iTileX, iTileY, iDirectionRoll)
        
            --Roll around the edge of the board if needed.
            if(iTileX < 0) then iTileX = gzaPuzzleFight.iBoardWidth -1 end
            if(iTileY < 0) then iTileY = gzaPuzzleFight.iBoardHeight-1 end
            if(iTileX >= gzaPuzzleFight.iBoardWidth)  then iTileX = 0 end
            if(iTileY >= gzaPuzzleFight.iBoardHeight) then iTileY = 0 end
        
        end
        
        --If this spot is occupied, which is possible due to switchbacks on the arrow track, do nothing.
        if(pzaBoard[iTileX][iTileY].iCode ~= gciPuzzleFight_Object_Empty) then
            
        --Otherwise, place the powerup and remove the powerup from the list.
        else
            pzaBoard[iTileX][iTileY].iCode = piaPowerups[iItemSlot]
            table.remove(piaPowerups, iItemSlot)
        end
    end
end

