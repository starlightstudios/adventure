-- |[ =================================== Place Attack Object ================================== ]|
--The player attacks the enemy by touching an attack object, which can be melee or ranged.
-- This routine places that object on the board in a valid position.
--Returns the X/Y of where it placed the object. Returns -1, -1 if an error occurred.
function fnPlaceAttackObject(pzaBoard, pbIsMelee, piEnemyX, piEnemyY)
    
    -- |[Argument Check]|
    --Validate the arguments.
    if(pzaBoard  == nil) then return -1, -1 end
    if(pbIsMelee == nil) then return -1, -1 end
    if(piEnemyX  == nil) then return -1, -1 end
    if(piEnemyY  == nil) then return -1, -1 end

    -- |[Melee Case]|
    --Place the melee strike object in a slot adjacent to the enemy. Check for edges.
    if(pbIsMelee) then
        
        --Debug.
        --io.write(" Generating melee position.\n")
        
        --Setup.
        local iTargetX = -1
        local iTargetY = -1
        
        --List of directions.
        local iaDirections = {gci_Face_North, gci_Face_East, gci_Face_South, gci_Face_West}
        
        --Roll until a valid position is selected.
        while(#iaDirections > 0) do
            
            --Roll a direction.
            local iSlot = LM_GetRandomNumber(1, #iaDirections)
            local iDirection = iaDirections[iSlot]
            table.remove(iaDirections, iSlot)
            
            --Get the target position.
            iTargetX, iTargetY = fnModifyPositionByDirection(piEnemyX, piEnemyY, iDirection)
            --io.write("  Query: " .. iTargetX .. "x" .. iTargetY .. "\n")

            --Verify that the position is within the board. If it is, place the object.
            if(fnIsPointInBoard(iTargetX, iTargetY)) then
                pzaBoard[iTargetX][iTargetY].iCode = gciPuzzleFight_Object_Melee
                --io.write("  Check passes.\n")
                break
            end
        end
        
        --Return target coordinates.
        return iTargetX, iTargetY

    -- |[Ranged Case]|
    --Place the ranged strike object two tiles or more away from the enemy in a straight line.
    else
        
        --Debug.
        --io.write(" Generating ranged position.\n")
        
        --Setup.
        local iTargetX = -1
        local iTargetY = -1
        
        --List of directions.
        local iMinRange = 2
        local iMaxRange = 7
        local iaDirections = {gci_Face_North, gci_Face_East, gci_Face_South, gci_Face_West}
        local zaRangeDirList = {}
        for i = iMinRange, iMaxRange, 1 do
            for p = 1, #iaDirections, 1 do
                table.insert(zaRangeDirList, {iaDirections[p], i})
            end
        end
        
        --Roll until a valid position is selected.
        while(#zaRangeDirList > 0) do
            
            --Roll a direction and how many tiles to place the icon.
            local iSlot      = LM_GetRandomNumber(1, #zaRangeDirList)
            local iDirection = zaRangeDirList[iSlot][1]
            local iRange     = zaRangeDirList[iSlot][2]
            
            --Remove from the master array.
            table.remove(zaRangeDirList, iSlot)
            
            --Get the target position.
            iTargetX, iTargetY = fnModifyPositionByDirection(piEnemyX, piEnemyY, iDirection)
            
            --The position has been modified, modify it again by the range.
            for i = 2, iRange, 1 do
                iTargetX, iTargetY = fnModifyPositionByDirection(iTargetX, iTargetY, iDirection)
            end
            --io.write("  Query: " .. iTargetX .. "x" .. iTargetY .. "\n")

            --Verify that the position is within the board. If it is, place the object.
            if(fnIsPointInBoard(iTargetX, iTargetY)) then
                pzaBoard[iTargetX][iTargetY].iCode = gciPuzzleFight_Object_Ranged
                --io.write("  Check passes.\n")
                break
            end
        end
        
        --Return target coordinates.
        return iTargetX, iTargetY
    end
    
    --Error:
    return -1, -1
    
end
