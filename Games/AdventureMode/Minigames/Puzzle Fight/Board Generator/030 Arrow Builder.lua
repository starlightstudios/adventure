-- |[ ====================================== Arrow Builder ===================================== ]|
--Creates a path of arrows for the player to follow from one position to another. Note that the
-- positions in question are not necessarily the target or player start position.
--The overall routine creates between 1 and 3 arrow sets. The subroutine attempts to connect those
-- arrow sets together by handling one corner at a time.
--This routine returns how many moves were needed if it was successful, -1 if not.
function fnBuildArrowPathToTarget(piStartX, piStartY, piEndX, piEndY, pzaBoard)
    
    -- |[Error Checking]|
    --Verify the arguments.
    --io.write(" Running arrow builder.\n")
    if(piStartX == nil) then return -1 end
    if(piStartY == nil) then return -1 end
    if(piEndX   == nil) then return -1 end
    if(piEndY   == nil) then return -1 end
    if(pzaBoard == nil) then return -1 end
    
    -- |[Setup]|
    --We start at the end position and move backwards towards the target.
    local iMovesUsed = 0
    local iMoveClamp = 6
    local iCurrentX = piEndX
    local iCurrentY = piEndY
    
    --Reset this flag.
    gzaPuzzleFight.bArrowsEndedEarly = false
    
    --Available directions and ranges.
    local iMaxRange = 6
    local iaDirections = {gci_Face_North, gci_Face_East, gci_Face_South, gci_Face_West}
    local zaRangeDirList = {}
    for i = 1, iMaxRange, 1 do
        for p = 1, #iaDirections, 1 do
            table.insert(zaRangeDirList, {iaDirections[p], i})
        end
    end
    
    -- |[Movement Loop]|
    --This loops until the destination is hit.
    --io.write(" Entering arrow loop.\n")
    while(iMoveClamp > 0) do
        
        --Decrement the clamp.
        iMoveClamp = iMoveClamp - 1
        
        --If the zaRangeDirList has no elements, then every range/direction has been attempted. Fail.
        if(#zaRangeDirList < 1) then
            break
        end
        
        --Store the starting position.
        local iStartedAtX = iCurrentX
        local iStartedAtY = iCurrentY
        
        --Roll a direction and distance.
        local iSlot = LM_GetRandomNumber(1, #zaRangeDirList)
        
        local iDirection = zaRangeDirList[iSlot][1]
        local iRange = zaRangeDirList[iSlot][2]
        --io.write(" -> Availability list has " .. #zaRangeDirList .. " entries.\n")
        --io.write("  Arrow start: " .. iCurrentX .. "x" .. iCurrentY .. "\n")
        --io.write("  Dir/Range: " .. iDirection .. " " .. iRange .. "\n")
        
        --Remove these from the availability array.
        table.remove(zaRangeDirList, iSlot)

        --Check each tile along this path.
        local iNewX, iNewY = fnArrowRunner(pzaBoard, iCurrentX, iCurrentY, iDirection, iRange, piStartX, piStartY)
        --io.write("  Arrow ran to: " .. iNewX .. "x" .. iNewY .. "\n")
        
        --Success flag is true. Place an arrow in the opposite direction at the end point.
        if(iNewX ~= iCurrentX or iNewY ~= iCurrentY) then
            
            --Update positions.
            iCurrentX = iNewX
            iCurrentY = iNewY
            
            --Place the opposing arrow.
            fnPlaceReverseArrow(iCurrentX, iCurrentY, iDirection, pzaBoard)
            --io.write(" Success, placing reverse arrow.\n")

            --Mark this as a successful move.
            iMoveClamp = iMoveClamp - 1
            iMovesUsed = iMovesUsed + 1
            
            --Rebuild the range/dir list.
            zaRangeDirList = {}
            for i = 1, iMaxRange, 1 do
                for p = 1, #iaDirections, 1 do
                    table.insert(zaRangeDirList, {iaDirections[p], i})
                end
            end
            
            --Special: Movement loop ended by hitting the player's position unexpectedly. This is a success.
            if(gzaPuzzleFight.bArrowsEndedEarly == true) then
                --io.write("  By chance hit player start. Exiting.\n")
                break
            end

        --Failed. Loop again.
        else
            --io.write(" Failed, arrow did not move.\n")
        end
    end
    
    --Debug.
    --io.write(" Exiting primary loop.\n")
    
    -- |[Exit]|
    --If by sheer chance the final position is the player's start position, we're done.
    if(iCurrentX == piStartX and iCurrentY == piStartY) then return iMovesUsed end
    
    -- |[Second-Final Joiner]|
    --Depending on where we ended up, it may be necessary for two additional arrows to be placed in order to reach the starting point.
    -- This first one is placed if the current X/Y is not at least on the same row or column as the start.
    --io.write(" Checking second-last joiner. " .. iCurrentX .. "x" .. iCurrentY .. " to " .. piStartX .. "x" .. piStartY .. "\n")
    if(iCurrentX ~= piStartX and iCurrentY ~= piStartY) then
        
        --Roll to determine if we should try to match the row or column.
        local iRoll = LM_GetRandomNumber(1, 2)
        
        --Storage.
        local iDirection = -1
        local iRange = -1
        
        --Attempt to match the column (x position)
        if(iRoll == 1) then
        
            --Need to move to the right/east.
            if(iCurrentX < piStartX) then
                iDirection = gci_Face_East
                iRange = piStartX - iCurrentX
        
            --Need to move to the left/west.
            else
                iDirection = gci_Face_West
                iRange = iCurrentX - piStartX
            end
        
        --Attempt to match the row (y position)
        else
        
            --Need to move down/south.
            if(iCurrentY < piStartY) then
                iDirection = gci_Face_South
                iRange = piStartY - iCurrentY
        
            --Need to move up/north.
            else
                iDirection = gci_Face_North
                iRange = iCurrentY - piStartY
            end
        end
        
        --Run the subroutine.
        local iNewX, iNewY = fnArrowRunner(pzaBoard, iCurrentX, iCurrentY, iDirection, iRange, piStartX, piStartY)
        
        --If we hit something, the entire routine has to fail. Try again.
        if(iNewX == iCurrentX and iNewY == iCurrentY) then
            return -1

        --Otherwise, success!
        else
            
            --Update positions.
            iCurrentX = iNewX
            iCurrentY = iNewY
            
            --Place the opposing arrow.
            fnPlaceReverseArrow(iCurrentX, iCurrentY, iDirection, pzaBoard)

            --Mark this as a successful move.
            iMovesUsed = iMovesUsed + 1
        
        end
    end
    
    -- |[Final Joiner]|
    --If the player is on the same column but not the same row:
    if(iCurrentX == piStartX) then
    
        --Storage.
        local iDirection = -1
        local iRange = -1
        
        --Need to move down/south.
        if(iCurrentY < piStartY) then
            iDirection = gci_Face_South
            iRange = piStartY - iCurrentY
    
        --Need to move up/north.
        else
            iDirection = gci_Face_North
            iRange = iCurrentY - piStartY
        end
        
        --Run the subroutine.
        local iNewX, iNewY = fnArrowRunner(pzaBoard, iCurrentX, iCurrentY, iDirection, iRange, piStartX, piStartY)
        
        --If we hit something, the entire routine has to fail. Try again.
        if(iNewX == iCurrentX and iNewY == iCurrentY) then
            return -1

        --Otherwise, success!
        else
            
            --Update positions.
            iCurrentX = iNewX
            iCurrentY = iNewY
            
            --Place the opposing arrow.
            fnPlaceReverseArrow(iCurrentX, iCurrentY, iDirection, pzaBoard)

            --Mark this as a successful move.
            iMovesUsed = iMovesUsed + 1
        
        end
    
    --If the player is on the same row but not the same column:
    elseif(iCurrentY == piStartY) then
    
        --Storage.
        local iDirection = -1
        local iRange = -1
        
        --Need to move to the right/east.
        if(iCurrentX < piStartX) then
            iDirection = gci_Face_East
            iRange = piStartX - iCurrentX
    
        --Need to move to the left/west.
        else
            iDirection = gci_Face_West
            iRange = iCurrentX - piStartX
        end
        
        --Run the subroutine.
        local iNewX, iNewY = fnArrowRunner(pzaBoard, iCurrentX, iCurrentY, iDirection, iRange, piStartX, piStartY)
        
        --If we hit something, the entire routine has to fail. Try again.
        if(iNewX == iCurrentX and iNewY == iCurrentY) then
            return -1

        --Otherwise, success!
        else
            
            --Update positions.
            iCurrentX = iNewX
            iCurrentY = iNewY
            
            --Place the opposing arrow.
            fnPlaceReverseArrow(iCurrentX, iCurrentY, iDirection, pzaBoard)

            --Mark this as a successful move.
            iMovesUsed = iMovesUsed + 1
        
        end
    end
    
    -- |[Finish Up]|
    --If we got this far, job done. Return the number of moves used.
    return iMovesUsed
end

-- |[ ================================== Place Reverse Arrow =================================== ]|
--Places an arrow in the reverse direction of the given direction, because the arrow routine
-- runs backwards.
function fnPlaceReverseArrow(piX, piY, piDirection, pzaBoard)
    
    --Argument check.
    if(piX         == nil) then return end
    if(piY         == nil) then return end
    if(piDirection == nil) then return end
    if(pzaBoard    == nil) then return end
    
    --Place the opposing arrow.
    if(piDirection == gci_Face_North) then
        pzaBoard[piX][piY].iCode = gciPuzzleFight_Object_ArrowDn
    elseif(piDirection == gci_Face_East) then
        pzaBoard[piX][piY].iCode = gciPuzzleFight_Object_ArrowLf
    elseif(piDirection == gci_Face_South) then
        pzaBoard[piX][piY].iCode = gciPuzzleFight_Object_ArrowUp
    elseif(piDirection == gci_Face_West) then
        pzaBoard[piX][piY].iCode = gciPuzzleFight_Object_ArrowRt
    end
end

-- |[ ====================================== Arrow Runner ====================================== ]|
--Actually does the work of connecting two locations together based on a direction and range.
-- Checks if there are no obstructions.
--Returns the position the routine ended at if successful. Returns the starting position on failure,
-- meaning there was an obstruction or we went off the board edge.
function fnArrowRunner(pzaBoard, piCurrentX, piCurrentY, piDirection, piDistance, piPlayerX, piPlayerY)
    
    --Argument check.
    if(pzaBoard    == nil) then return 0, 0 end
    if(piCurrentX  == nil) then return 0, 0 end
    if(piCurrentY  == nil) then return 0, 0 end
    if(piDirection == nil) then return 0, 0 end
    if(piDistance  == nil) then return 0, 0 end
    if(piPlayerX   == nil) then return 0, 0 end
    if(piPlayerY   == nil) then return 0, 0 end
    
    --Store the starting position.
    local iOriginalX = piCurrentX
    local iOriginalY = piCurrentY
    --io.write("   Arrow runner begins: " .. iOriginalX .. "x" .. iOriginalY .. "\n")
    
    --Run.
    for i = 1, piDistance, 1 do
        
        --Modify.
        piCurrentX, piCurrentY = fnModifyPositionByDirection(piCurrentX, piCurrentY, piDirection)
        --io.write("    Moves to " .. piCurrentX .. "x" .. piCurrentY .. ":")
        
        --If the point in question happens to be the player start position, then we end the routine.
        if(piCurrentX == piPlayerX and piCurrentY == piPlayerY) then
            --io.write("Hit player position.\n")
            gzaPuzzleFight.bArrowsEndedEarly = true
            return piCurrentX, piCurrentY
        end
        
        --Check. If it comes back true, the spot is occupied and the series is invalid.
        if(fnIsPointOccupied(pzaBoard, piCurrentX, piCurrentY)) then
            --io.write("Occupied\n")
            return iOriginalX, iOriginalY
        end
        --io.write("Unoccupied\n")
    end
    
    --If we got this far, there were no obstructions. Return the ending position.
    return piCurrentX, piCurrentY
end