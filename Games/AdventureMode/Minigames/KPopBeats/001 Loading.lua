-- |[ ========================================= Loading ======================================== ]|
--Loads all graphics files for the dancing minigame.
gbLoadedDanceAssets = false
if(gbLoadedDanceAssets == true) then return end
gbLoadedDanceAssets = true

--Setup.
local sBasePath = fnResolvePath()

-- |[ ========================================== Sound ========================================= ]|
--Music.
AudioManager_Register("DanceMusic", "AsMusic", "AsStream",sBasePath .. "../../Audio/Music/Dance/DanceSong.ogg")

--SFX.
AudioManager_Register("Dance|Bell0", "AsSound", "AsSample", sBasePath .. "../../Audio/WorldInteractionSFX/Dance/Bell0.ogg")
AudioManager_Register("Dance|Bell1", "AsSound", "AsSample", sBasePath .. "../../Audio/WorldInteractionSFX/Dance/Bell1.ogg")
AudioManager_Register("Dance|Bell2", "AsSound", "AsSample", sBasePath .. "../../Audio/WorldInteractionSFX/Dance/Bell2.ogg")
AudioManager_Register("Dance|Bell3", "AsSound", "AsSample", sBasePath .. "../../Audio/WorldInteractionSFX/Dance/Bell3.ogg")
AudioManager_Register("Dance|Fail",  "AsSound", "AsSample", sBasePath .. "../../Audio/WorldInteractionSFX/Dance/Fail.ogg")

-- |[ =========================================== UI =========================================== ]|
--Images used by the minigame's UI.
SLF_Open(gsDatafilesPath .. "Sprites.slf")

-- |[Arrows]|
DL_AddPath("Root/Images/KPopSprites/UI/")
DL_ExtractBitmap("Dance|ArrowTop",   "Root/Images/KPopSprites/UI/ArrowTop")
DL_ExtractBitmap("Dance|ArrowRgt",   "Root/Images/KPopSprites/UI/ArrowRgt")
DL_ExtractBitmap("Dance|ArrowBot",   "Root/Images/KPopSprites/UI/ArrowBot")
DL_ExtractBitmap("Dance|ArrowLft",   "Root/Images/KPopSprites/UI/ArrowLft")
DL_ExtractBitmap("Dance|ArrowNon",   "Root/Images/KPopSprites/UI/ArrowNon")
DL_ExtractBitmap("Dance|ArrowAct",   "Root/Images/KPopSprites/UI/ArrowAct")
DL_ExtractBitmap("Dance|ArrowCan",   "Root/Images/KPopSprites/UI/ArrowCan")
DL_ExtractBitmap("Dance|ArrowBox",   "Root/Images/KPopSprites/UI/ArrowBox")
DL_ExtractBitmap("Dance|Encourage0", "Root/Images/KPopSprites/UI/Encourage0")
DL_ExtractBitmap("Dance|Encourage1", "Root/Images/KPopSprites/UI/Encourage1")
DL_ExtractBitmap("Dance|Encourage2", "Root/Images/KPopSprites/UI/Encourage2")
DL_ExtractBitmap("Dance|DiscLate",   "Root/Images/KPopSprites/UI/DiscLate")
DL_ExtractBitmap("Dance|DiscEarly",  "Root/Images/KPopSprites/UI/DiscEarly")
DL_ExtractBitmap("Dance|DiscWrong",  "Root/Images/KPopSprites/UI/DiscWrong")

-- |[ ======================================== Sequences ======================================= ]|
--All dancers share the same sequences.
local zaSequences = {}

-- |[Izuna]|
--Build.
zaSequences[1] = {"Dance|Izuna|Idle|", "Root/Images/KPopSprites/Izuna/Idle|", 4}
zaSequences[2] = {"Dance|Izuna|ArmR|", "Root/Images/KPopSprites/Izuna/ArmR|", 6}
zaSequences[3] = {"Dance|Izuna|ArmL|", "Root/Images/KPopSprites/Izuna/ArmL|", 6}
zaSequences[4] = {"Dance|Izuna|LegL|", "Root/Images/KPopSprites/Izuna/LegL|", 6}
zaSequences[5] = {"Dance|Izuna|LegR|", "Root/Images/KPopSprites/Izuna/LegR|", 6}
zaSequences[6] = {"Dance|Izuna|Down|", "Root/Images/KPopSprites/Izuna/Down|", 6}
zaSequences[7] = {"Dance|Izuna|HipT|", "Root/Images/KPopSprites/Izuna/HipT|", 6}

--Paths.
DL_AddPath("Root/Images/KPopSprites/Izuna/")

--Set flags.
local iNearest = DM_GetEnumeration("GL_NEAREST")
ALB_SetTextureProperty("MagFilter", iNearest)
ALB_SetTextureProperty("MinFilter", iNearest)

--Run.
for i = 1, #zaSequences, 1 do
    local sRipName = zaSequences[i][1]
    local sDLPath  = zaSequences[i][2]
    local iCount   = zaSequences[i][3]
    for p = 0, iCount-1, 1 do
        DL_ExtractBitmap(sRipName .. p, sDLPath .. p)
        --DL_ReportBitmap(sDLPath .. p)
    end
end

--Clean.
ALB_SetTextureProperty("Restore Defaults")

-- |[ ======================================== Clean Up ======================================== ]|
--Close.
SLF_Close()
