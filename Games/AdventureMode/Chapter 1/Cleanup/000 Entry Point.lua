-- |[ ==================================== Chapter 1 Cleanup =================================== ]|
--This script is fired once Chapter 1 is completed. General states of Chapter 1 are stored in a special
-- DataLibrary section to be queried later.

-- |[Flag Completion]|
VM_SetVar("Root/Variables/Global/ChapterComplete/iChapter1", "N", 1.0)

-- |[ ================================ New Chapter Plus Backup ================================= ]|
--Store variables that will be needed if the player decides to do a New Chapter Plus. This includes
-- inventory, forms, money, and more!
DL_AddPath("Root/Variables/NewChapterPlus/Chapter1/")

-- |[Inventory]|
--Basic Platina count.
VM_SetVar("Root/Variables/NewChapterPlus/Chapter1/iPlatina", "N", AdInv_GetProperty("Platina"))

--Adamantite counts.
for i = gciCraft_Adamantite_Powder, gciCraft_Adamantite_Total-1, 1 do
    local iCount = AdInv_GetProperty("Crafting Count", i)
    VM_SetVar("Root/Variables/NewChapterPlus/Chapter1/iAdamantite"..i, "N", iCount)
end

--Items in the inventory, not equipped:
local iFreeItems = AdInv_GetProperty("Total Items Unequipped")
VM_SetVar("Root/Variables/NewChapterPlus/Chapter1/iItemsUnequipped", "N", iFreeItems)

--Store the items and their counts:
for i = 1, iFreeItems, 1 do
    AdInv_PushItemI(i-1)
        local sName = AdItem_GetProperty("Name")
        local iQuantity = AdItem_GetProperty("Quantity")
        VM_SetVar("Root/Variables/NewChapterPlus/Chapter1/sItem"..i.."Name", "S", sName)
        VM_SetVar("Root/Variables/NewChapterPlus/Chapter1/iItem"..i.."Quantity", "N", iQuantity)
    DL_PopActiveObject()
end

-- |[Costumes]|
--Store if a costume is unlocked.
VM_SetVar("Root/Variables/NewChapterPlus/Chapter1/iCostumeUnlocked_QueenBee", "N", VM_GetVar("Root/Variables/Costumes/Mei/iQueenBee", "N"))

-- |[Forms]|
--Store which forms are unlocked for Mei:
VM_SetVar("Root/Variables/NewChapterPlus/Chapter1/iFormUnlocked_Alraune",     "N", VM_GetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N"))
VM_SetVar("Root/Variables/NewChapterPlus/Chapter1/iFormUnlocked_Bee",         "N", VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N"))
VM_SetVar("Root/Variables/NewChapterPlus/Chapter1/iFormUnlocked_Ghost",       "N", VM_GetVar("Root/Variables/Global/Mei/iHasGhostForm", "N"))
VM_SetVar("Root/Variables/NewChapterPlus/Chapter1/iFormUnlocked_Gravemarker", "N", VM_GetVar("Root/Variables/Global/Mei/iHasGravemarkerForm", "N"))
VM_SetVar("Root/Variables/NewChapterPlus/Chapter1/iFormUnlocked_Rubber",      "N", VM_GetVar("Root/Variables/Global/Mei/iHasRubberForm", "N"))
VM_SetVar("Root/Variables/NewChapterPlus/Chapter1/iFormUnlocked_Slime",       "N", VM_GetVar("Root/Variables/Global/Mei/iHasSlimeForm", "N"))
VM_SetVar("Root/Variables/NewChapterPlus/Chapter1/iFormUnlocked_Mannequin",   "N", VM_GetVar("Root/Variables/Global/Mei/iHasMannequinForm", "N"))
VM_SetVar("Root/Variables/NewChapterPlus/Chapter1/iFormUnlocked_Werecat",     "N", VM_GetVar("Root/Variables/Global/Mei/iHasWerecatForm", "N"))
VM_SetVar("Root/Variables/NewChapterPlus/Chapter1/iFormUnlocked_Wisphag",     "N", VM_GetVar("Root/Variables/Global/Mei/iHasWisphagForm", "N"))
VM_SetVar("Root/Variables/NewChapterPlus/Chapter1/iFormUnlocked_Zombee",      "N", VM_GetVar("Root/Variables/Global/Mei/iHasZombeeForm", "N"))

--Store which jobs are unlocked for Florentina:
VM_SetVar("Root/Variables/NewChapterPlus/Chapter1/iJobUnlocked_Mediator",       "N", VM_GetVar("Root/Variables/Global/Florentina/iHasJob_Mediator", "N"))
VM_SetVar("Root/Variables/NewChapterPlus/Chapter1/iJobUnlocked_TreasureHunter", "N", VM_GetVar("Root/Variables/Global/Florentina/iHasJob_TreasureHunter", "N"))
VM_SetVar("Root/Variables/NewChapterPlus/Chapter1/iJobUnlocked_Agarist",        "N", VM_GetVar("Root/Variables/Global/Florentina/iHasJob_Agarist", "N"))
VM_SetVar("Root/Variables/NewChapterPlus/Chapter1/iJobUnlocked_Lurker",         "N", VM_GetVar("Root/Variables/Global/Florentina/iHasJob_Lurker", "N"))

-- |[Skillbooks]|
--Store the total number of skillbooks unlocked. Individual skillbooks are handled later.
VM_SetVar("Root/Variables/NewChapterPlus/Chapter1/iSkillbooksMei",        "N", VM_GetVar("Root/Variables/Global/Mei/iSkillbookTotal",        "N"))
VM_SetVar("Root/Variables/NewChapterPlus/Chapter1/iSkillbooksFlorentina", "N", VM_GetVar("Root/Variables/Global/Florentina/iSkillbookTotal", "N"))

-- |[ ================================== Storage and Takedown ================================== ]|
-- |[Major Variables]|
--Get variables for major chapter states.
local iSavedBeehive            = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedBeehive", "N")
local iCompletedTrapDungeon    = VM_GetVar("Root/Variables/Chapter1/Scenes/iCompletedTrapDungeon", "N")
local iCompletedQuantirMansion = VM_GetVar("Root/Variables/Chapter1/Scenes/iCompletedQuantirMansion", "N")

--Store dungeon completion states.
VM_SetVar("Root/Variables/Storage/Chapter1/iHiveWasSaved", "N", iSavedBeehive)
VM_SetVar("Root/Variables/Storage/Chapter1/iWardenDefeated", "N", iCompletedQuantirMansion)
VM_SetVar("Root/Variables/Storage/Chapter1/iTrapDungeonBossDefeated", "N", iCompletedTrapDungeon)

-- |[Character Opinions of Mei]|
--Adina's Opinion of Mei.
local iMeiLovesAdina = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiLovesAdina", "N")
if(iMeiLovesAdina == 0.0) then
	VM_SetVar("Root/Variables/Storage/Chapter1/iAdinasOpinionOfMei", "N", 0.0)
else
	VM_SetVar("Root/Variables/Storage/Chapter1/iAdinasOpinionOfMei", "N", 1.0)
end

--Breanne's Opinion of Mei.
local iHasMetMei     = VM_GetVar("Root/Variables/Chapter1/Breanne/iHasMetMei", "N")
local iTalkedJob     = VM_GetVar("Root/Variables/Chapter1/Breanne/iTalkedJob", "N")
local iTalkedParents = VM_GetVar("Root/Variables/Chapter1/Breanne/iTalkedParents", "N")
local iTakenPieJob   = VM_GetVar("Root/Variables/Chapter1/Breanne/iTakenPieJob", "N")
if(iHasMetMei == 0.0) then
	VM_SetVar("Root/Variables/Storage/Chapter1/iBreannesOpinionOfMei", "N", 0.0)
elseif(iTalkedJob == 1.0 and iTalkedParents == 1.0 and iTakenPieJob == 0.0) then
	VM_SetVar("Root/Variables/Storage/Chapter1/iBreannesOpinionOfMei", "N", 1.0)
elseif(iTalkedJob == 1.0 and iTalkedParents == 1.0 and iTakenPieJob == 1.0) then
	VM_SetVar("Root/Variables/Storage/Chapter1/iBreannesOpinionOfMei", "N", 2.0)
end

--Florentina's Opinion of Mei.
local iHasSeenTrannadarFlorentinaScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
local iHasSeenFlorentinaTwentyWin = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenFlorentinaTwentyWin", "N")
if(iHasSeenTrannadarFlorentinaScene == 0.0) then
	VM_SetVar("Root/Variables/Storage/Chapter1/iFlorentinasOpinionOfMei", "N", 0.0)
else
	local iCount = 1
	if(iHasSeenFlorentinaTwentyWin == 1.0) then iCount = iCount + 1 end
	if(iSavedBeehive == 1.0) then iCount = iCount + 1 end
	if(iCompletedTrapDungeon == 1.0) then iCount = iCount + 1 end
	if(iCompletedQuantirMansion == 1.0) then iCount = iCount + 1 end
	VM_SetVar("Root/Variables/Storage/Chapter1/iFlorentinasOpinionOfMei", "N", iCount)
end

--Nadia's Opinion of Mei.
local iHasSeenTrannadarFirstScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N")
local iMeiHasDoneNadia            = VM_GetVar("Root/Variables/Chapter1/ScenesWerecat/iMeiHasDoneNadia", "N")
if(iHasSeenTrannadarFirstScene == 0.0) then
	VM_SetVar("Root/Variables/Storage/Chapter1/iNadiasOpinionOfMei", "N", 0.0)
elseif(iMeiHasDoneNadia == 0.0) then
	VM_SetVar("Root/Variables/Storage/Chapter1/iNadiasOpinionOfMei", "N", 1.0)
else
	VM_SetVar("Root/Variables/Storage/Chapter1/iNadiasOpinionOfMei", "N", 2.0)
end

-- |[Claudia and the Heavenly Doves]|
--Claudia's rescue state.
local iMetClaudia   = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetClaudia", "N")
local iSavedClaudia = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedClaudia", "N")
if(iMetClaudia == 0.0) then
	VM_SetVar("Root/Variables/Storage/Chapter1/iMetClaudia", "N", 0.0)
	VM_SetVar("Root/Variables/Storage/Chapter1/iSavedClaudia", "N", 0.0)
else
	VM_SetVar("Root/Variables/Storage/Chapter1/iMetClaudia", "N", 1.0)
	VM_SetVar("Root/Variables/Storage/Chapter1/iSavedClaudia", "N", iSavedClaudia)
end

--Answered a few questions to Karina about bee-ing a bee.
local bReadAlraunes    = WD_GetProperty("Is Topic Read", "Karina_Alraunes")
local bReadNewBees     = WD_GetProperty("Is Topic Read", "Karina_NewBees")
local bReadHiveMind    = WD_GetProperty("Is Topic Read", "Karina_HiveMind")
local bReadPartirhuman = WD_GetProperty("Is Topic Read", "Karina_PartirHuman")
if(bReadAlraunes == true or bReadNewBees == true or bReadHiveMind == true or bReadPartirhuman == true) then
	VM_SetVar("Root/Variables/Storage/Chapter1/iProvidedKarinaWithBeeInfo", "N", 1.0)
else
	VM_SetVar("Root/Variables/Storage/Chapter1/iProvidedKarinaWithBeeInfo", "N", 0.0)
end

-- |[Things Mei Knows About]|
--Variables describing a few things Mei did or found.
local iTalkedWerecatVendor = VM_GetVar("Root/Variables/Chapter1/Scenes/iTalkedWerecatVendor", "N")
if(iTalkedWerecatVendor == 0.0) then
	VM_SetVar("Root/Variables/Storage/Chapter1/iMeiSawSanyasPhone", "N", 0.0)
else
	VM_SetVar("Root/Variables/Storage/Chapter1/iMeiSawSanyasPhone", "N", 1.0)
end

--Smarty Slimes!
local iMetSlimeGuide = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetSlimeGuide", "N")
if(iMetSlimeGuide == 0.0) then
	VM_SetVar("Root/Variables/Storage/Chapter1/iMeiKnowsAboutSmartySlimes", "N", 0.0)
else
	VM_SetVar("Root/Variables/Storage/Chapter1/iMeiKnowsAboutSmartySlimes", "N", 1.0)
end

--Did Mei join the pliant man?
local iMeiJoinedSuitor = VM_GetVar("Root/Variables/Chapter1/Breanne/iMeiJoinedSuitor", "N")
if(iMeiJoinedSuitor == 0.0) then
	VM_SetVar("Root/Variables/Storage/Chapter1/iMeiJoinedALeafSister", "N", 0.0)
else
	VM_SetVar("Root/Variables/Storage/Chapter1/iMeiJoinedALeafSister", "N", 1.0)
end

--Cassandra's Status
local iStartedCassandraEvent = VM_GetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N")
if(iStartedCassandraEvent < 1.0) then
	VM_SetVar("Root/Variables/Storage/Chapter1/iMeiSawCassandra", "N", 0.0)
	VM_SetVar("Root/Variables/Storage/Chapter1/iCassandraBecameWerecat", "N", 1.0)
else
	local iSavedCassandra = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedCassandra", "N")
	VM_SetVar("Root/Variables/Storage/Chapter1/iMeiSawCassandra", "N", 1.0)
	VM_SetVar("Root/Variables/Storage/Chapter1/iCassandraBecameWerecat", "N", 1.0 - iSavedCassandra)
end

--Mei and the Zombees
local iHasSeenZombeeScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenZombeeScene", "N")
if(iHasSeenZombeeScene == 0.0) then
	VM_SetVar("Root/Variables/Storage/Chapter1/iMeiBecameZombee", "N", 0.0)
else
	VM_SetVar("Root/Variables/Storage/Chapter1/iMeiBecameZombee", "N", 1.0)
end

--Mei spoke to Countess Quantir
local iSpokeToCountess = VM_GetVar("Root/Variables/Chapter1/Scenes/iSpokeToCountess", "N")
VM_SetVar("Root/Variables/Storage/Chapter1/iSpokeToCountess", "N", iSpokeToCountess)

-- |[Mei's Forms]|
--Transformation variables.
VM_SetVar("Root/Variables/Storage/Chapter1/iHasAlraune", "N", VM_GetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N"))
VM_SetVar("Root/Variables/Storage/Chapter1/iHasBee",     "N", VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm",     "N"))
VM_SetVar("Root/Variables/Storage/Chapter1/iHasSlime",   "N", VM_GetVar("Root/Variables/Global/Mei/iHasSlimeForm",   "N"))
VM_SetVar("Root/Variables/Storage/Chapter1/iHasWerecat", "N", VM_GetVar("Root/Variables/Global/Mei/iHasWerecatForm", "N"))
VM_SetVar("Root/Variables/Storage/Chapter1/iHasGhost",   "N", VM_GetVar("Root/Variables/Global/Mei/iHasGhostForm",   "N"))

--Volunteer cases.
local iMeiVolunteeredToAlraune = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiVolunteeredToAlraune", "N")
local iUseSpecialBeeOpening    = VM_GetVar("Root/Variables/Chapter1/Scenes/iUseSpecialBeeOpening",    "N")
VM_SetVar("Root/Variables/Storage/Chapter1/iVolunteeredAlraune", "N", iMeiVolunteeredToAlraune)
VM_SetVar("Root/Variables/Storage/Chapter1/iVolunteeredBee",     "N", iUseSpecialBeeOpening)

-- |[Very Important Variables]|
--Why would you annoy that poor goat?
local iGoatBotherCount = VM_GetVar("Root/Variables/Global/Goat/iGoatBotherCount", "N")
if(iGoatBotherCount < 30) then
	VM_SetVar("Root/Variables/Storage/Chapter1/iTormentedThatPoorGoat", "N", 0.0)
else
	VM_SetVar("Root/Variables/Storage/Chapter1/iTormentedThatPoorGoat", "N", 1.0)
end

-- |[Purge]|
--Now that we've stored everything we need, purge the datalibrary of the Chapter 1 variables.
--DL_Purge("Root/Variables/Chapter1/", false)
--DL_Purge("Root/Variables/Global/Mei/", false)
--DL_Purge("Root/Variables/Global/Florentina/", false)
--DL_Purge("Root/Variables/Global/Goat", false)

-- |[Catalyst Counts]|
local iCatalystH  = VM_GetVar("Root/Variables/Global/Catalysts/iHealth", "N")
local iCatalystAt = VM_GetVar("Root/Variables/Global/Catalysts/iAttack", "N")
local iCatalystI  = VM_GetVar("Root/Variables/Global/Catalysts/iInitiative", "N")
local iCatalystE  = VM_GetVar("Root/Variables/Global/Catalysts/iEvade", "N") + VM_GetVar("Root/Variables/Global/Catalysts/iDodge", "N")
local iCatalystAc = VM_GetVar("Root/Variables/Global/Catalysts/iAccuracy", "N")
local iCatalystSk = VM_GetVar("Root/Variables/Global/Catalysts/iMovement", "N") + VM_GetVar("Root/Variables/Global/Catalysts/iSkill", "N")

-- |[Inventory Handling]|
--Remove the Platinum Compass
VM_SetVar("Root/Variables/Global/CatalystTone/iHasCatalystTone", "N", 0.0)

--Change Mei to no longer be the party leader.
AL_SetProperty("Player Actor ID", 0)

--Remove Mei from the party.
AdvCombat_SetProperty("Clear Party")

--Set Cash to 0, remove crafting ingredients, drop the inventory entirely.
AdInv_SetProperty("Clear")

-- |[Reinstate Catalysts]|
--Clearing the inventory zeroes off the catalyst count, but it persists between chapters. Reset them here.
AdInv_SetProperty("Catalyst Count", gciCatalyst_Health,     iCatalystH)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Attack,     iCatalystAt)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Initiative, iCatalystI)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Evade,      iCatalystE)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Accuracy,   iCatalystAc)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Skill,      iCatalystSk)

--Call the auto-set function with "Null". Zeroes the cur/max counts for the catalysts.
AdInv_SetProperty("Resolve Catalysts By String", "Null")
