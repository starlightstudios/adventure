-- |[Variable Listing]|
--These are all the storage variables that Chapter 1 has. They get initted at program startup and are
-- always present in every chapter, even after Chapter 1 has been cleaned up.
DL_AddPath("Root/Variables/Storage/Chapter1/")

--Character opinions of Mei.
VM_SetVar("Root/Variables/Storage/Chapter1/iAdinasOpinionOfMei", "N", 0.0)
VM_SetVar("Root/Variables/Storage/Chapter1/iBreannesOpinionOfMei", "N", 0.0)
VM_SetVar("Root/Variables/Storage/Chapter1/iFlorentinasOpinionOfMei", "N", 0.0)
VM_SetVar("Root/Variables/Storage/Chapter1/iNadiasOpinionOfMei", "N", 0.0)

--Claudia and the Heavenly Doves.
VM_SetVar("Root/Variables/Storage/Chapter1/iMetClaudia", "N", 0.0)
VM_SetVar("Root/Variables/Storage/Chapter1/iSavedClaudia", "N", 0.0)
VM_SetVar("Root/Variables/Storage/Chapter1/iProvidedKarinaWithBeeInfo", "N", 0.0)

--Things Mei knows about.
VM_SetVar("Root/Variables/Storage/Chapter1/iMeiSawSanyasPhone", "N", 0.0)
VM_SetVar("Root/Variables/Storage/Chapter1/iMeiKnowsAboutSmartySlimes", "N", 0.0)
VM_SetVar("Root/Variables/Storage/Chapter1/iMeiJoinedALeafSister", "N", 0.0)

--Cassandra's Status
VM_SetVar("Root/Variables/Storage/Chapter1/iMeiSawCassandra", "N", 0.0)
VM_SetVar("Root/Variables/Storage/Chapter1/iCassandraBecameWerecat", "N", 0.0)

--Zombee and Beehive State
VM_SetVar("Root/Variables/Storage/Chapter1/iMeiBecameZombee", "N", 0.0)
VM_SetVar("Root/Variables/Storage/Chapter1/iHiveWasSaved", "N", 0.0)

--Trap Dungeon
VM_SetVar("Root/Variables/Storage/Chapter1/iTrapDungeonBossDefeated", "N", 0.0)

--Quantir Mansion State
VM_SetVar("Root/Variables/Storage/Chapter1/iWardenDefeated", "N", 0.0)
VM_SetVar("Root/Variables/Storage/Chapter1/iSpokeToCountess", "N", 0.0)

-- |[Mei's Forms]|
--These carry over into later Chapters.
VM_SetVar("Root/Variables/Storage/Chapter1/iHasAlraune", "N", 0.0)
VM_SetVar("Root/Variables/Storage/Chapter1/iHasBee", "N", 0.0)
VM_SetVar("Root/Variables/Storage/Chapter1/iHasSlime", "N", 0.0)
VM_SetVar("Root/Variables/Storage/Chapter1/iHasWerecat", "N", 0.0)
VM_SetVar("Root/Variables/Storage/Chapter1/iHasGhost", "N", 0.0)

--Note: Mei's skills are not carried over. She is assumed to learn all her Chapter 6 skills from the Rilmani.

--Volunteer cases.
VM_SetVar("Root/Variables/Storage/Chapter1/iVolunteeredAlraune", "N", 0.0)
VM_SetVar("Root/Variables/Storage/Chapter1/iVolunteeredBee", "N", 0.0)

-- |[Very Important Variables]|
--These are crucial, damn it.
VM_SetVar("Root/Variables/Storage/Chapter1/iTormentedThatPoorGoat", "N", 0.0)
