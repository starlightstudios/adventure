-- |[ ==================================== Volunteer Handler =================================== ]|
--When combat begins, this script is called to determine if the surrender option should say
-- "Volunteer" instead. This is to help players find out if they get a special scene when they
-- lose the battle in question.
gbIsVolunteer = false

--The script expects to have the defeat script passed in.
if(fnArgCheck(1) == false) then return end
local sDefeatScript = LM_GetScriptArgument(0)

-- |[Subdivision]|
local bIsOnDirectory = false
local sLastPart = ""
local sDirectory = ""
for i = string.len(sDefeatScript), 1, -1 do
    
    --Current letter.
    local c = string.sub(sDefeatScript, i, i)
    
    --Letter is a slash:
    if(c == "/" or c == "\\") then
        if(bIsOnDirectory == false) then
            bIsOnDirectory = true
        else
            break
        end
    
    --Otherwise, copy.
    else
        if(bIsOnDirectory == false) then
            sLastPart = c .. sLastPart
        else
            sDirectory =  c .. sDirectory
        end
    end
end

-- |[ ====================================== Case Listing ====================================== ]|
--Now that we know which directory this scene is in, we can figure out if a special scene will play
-- as a result.
if(sDirectory == "Defeat_Alraune") then
    local iHasAlrauneForm = VM_GetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N")
    if(iHasAlrauneForm == 0.0) then gbIsVolunteer = true end
    
elseif(sDirectory == "Defeat_Bee") then
    local iHasBeeForm = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")
    if(iHasBeeForm == 0.0) then gbIsVolunteer = true end
    
    --If Mei is in alraune form, a special scene can play out.
	local sForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
    if(sForm == "Alraune") then gbIsVolunteer = true end
    
elseif(sDirectory == "Defeat_Ghost") then
    local iHasGhostForm = VM_GetVar("Root/Variables/Global/Mei/iHasGhostForm", "N")
    if(iHasGhostForm == 0.0) then gbIsVolunteer = true end
    
elseif(sDirectory == "Defeat_Slime") then
    local iMeiHasSlimeForm = VM_GetVar("Root/Variables/Global/Mei/iHasSlimeForm", "N")
    if(iMeiHasSlimeForm == 0.0) then gbIsVolunteer = true end
    
elseif(sDirectory == "Defeat_Werecat") then
    local iHasWerecatForm = VM_GetVar("Root/Variables/Global/Mei/iHasWerecatForm", "N")
    if(iHasWerecatForm == 0.0) then gbIsVolunteer = true end
    
elseif(sDirectory == "Defeat_Wisphag") then
    local iHasWisphagForm = VM_GetVar("Root/Variables/Global/Mei/iHasWisphagForm", "N")
    if(iHasWisphagForm == 0.0) then gbIsVolunteer = true end
    
elseif(sDirectory == "Defeat_Gravemarker") then
    local iHasGravemarkerForm = VM_GetVar("Root/Variables/Global/Mei/iHasGravemarkerForm", "N")
    if(iHasGravemarkerForm == 0.0) then gbIsVolunteer = true end
    
elseif(sDirectory == "Defeat_Zombee") then
    local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
    local iHasSeenZombeeScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenZombeeScene", "N")
    if(iHasSeenZombeeScene == 0.0 and sMeiForm == "Bee") then gbIsVolunteer = true end
    
end
