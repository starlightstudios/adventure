-- |[ ====================================== Quest Handler ===================================== ]|
--Whenever the journal is opened to quest mode, generates quest entries.

-- |[Setup]|
--Create chart for storage.
gzaCh1QuestEntries = {}

--Set active globals. This reduces the number of arguments that need to be passed.
JournalEntry.zaAppendList = gzaCh1QuestEntries

-- |[Call Subscripts]|
local sBasePath = fnResolvePath() .. "Quests/"
LM_ExecuteScript(sBasePath .. "00 Find A Way Home.lua")
LM_ExecuteScript(sBasePath .. "01 Salt Flats.lua")
LM_ExecuteScript(sBasePath .. "02 Zombees.lua")
LM_ExecuteScript(sBasePath .. "03 Runestone.lua")
LM_ExecuteScript(sBasePath .. "04 Pepper Pie.lua")
LM_ExecuteScript(sBasePath .. "05 Cassandra.lua")
LM_ExecuteScript(sBasePath .. "06 Summoning.lua")
LM_ExecuteScript(sBasePath .. "07 Starfield Ruins.lua")
LM_ExecuteScript(sBasePath .. "08 Quantir Manor.lua")
LM_ExecuteScript(sBasePath .. "09 St Foras.lua")
LM_ExecuteScript(sBasePath .. "10 Dormine Tower.lua")

-- |[ ============== Finish Up =============== ]|
-- |[Assemble List]|
for i = 1, #gzaCh1QuestEntries, 1 do
    gzaCh1QuestEntries[i]:fnUploadData()
end

-- |[Clean]|
--Unsets globals.
JournalEntry:fnCleanGlobals()

--Clear the list to save memory.
gzaCh1QuestEntries = nil
