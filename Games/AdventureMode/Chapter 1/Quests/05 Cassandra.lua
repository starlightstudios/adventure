-- |[ ======================================== Cassandra ======================================= ]|
--Save Cassandra!
local sInternalName = "Save That Girl!"
local sObjective = ""
local sDescription = ""
local zQuestEntry = JournalEntry:new(sInternalName, sInternalName)
    
--Variables.
local iStartedCassandraEvent = VM_GetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N")
local iSavedCassandra      = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedCassandra",      "N")
local iCassandraWayTooLate = VM_GetVar("Root/Variables/Chapter1/Scenes/iCassandraWayTooLate", "N")

-- |[Blank Entry Check]|
if(iStartedCassandraEvent < 1.0) then
    zQuestEntry:fnBlankQuest()
    return
end

-- |[ ============================== Variants ============================== ]|
--Find her!
if(iStartedCassandraEvent == 1.0) then

    --Objective.
    sObjective = "Save that girl from the werecats!"
    
    --Description.
    sDescription = "You found a bunch of blood and heard a scream! Someone needs help![BR]"..
                   "You don't have forever to find her, so get searching![BR][BR]"..
                   "Each battle will advance time, so search the area and avoid the[BR]"..
                   "werecats, or you'll be too late!"

--Done!
elseif(iStartedCassandraEvent == 2.0) then

    --Objective.
    sObjective = "Complete"

    --Success:
    if(iSavedCassandra == 1.0) then
        sDescription = "After a fight with some werecats, you saved their prisoner in time to[BR]"..
        "prevent her from being transformed. She's a little shaken, and intends[BR]"..
        "to return to Jeffespeir soon."
    
    --Abandoned:
    elseif(iCassandraWayTooLate == 1.0) then
        sDescription = "You can't save everyone in the whole world by yourself, so you let it[BR]"..
                       "go. Hopefully, the girl enjoys her new werecat life."
    
    else
        sDescription = "The girl was transformed into a werecat and has run off into the[BR]"..
        "forest. Hopefully she's happy in her new form."
    end
end

-- |[ =============================== Upload =============================== ]|
--Upload information.
zQuestEntry:fnSetToQuest(sObjective)
zQuestEntry:fnSetDescription(sDescription)