-- |[ =================================== Prison of the Dead =================================== ]|
--Fight the Warden.
local sInternalName = "Prison of the Dead"
local sObjective = ""
local sDescription = ""
local zQuestEntry = JournalEntry:new(sInternalName, sInternalName)
    
--Variables.
local iWardenQuestState = VM_GetVar("Root/Variables/Chapter1/Scenes/iWardenQuestState", "N")
local iIsGhostTF        = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsGhostTF", "N")

-- |[Blank Entry Check]|
if(iWardenQuestState < 1.0 and iIsGhostTF ~= 1.0) then
    zQuestEntry:fnBlankQuest()
    return
end

-- |[ ============================== Variants ============================== ]|
--Ghost TF:
if(iIsGhostTF == 1.0) then

    --Objective.
    sObjective = "Do what Lydie says before you get in trouble!"
    
    --Description.
    sDescription = "You were sleeping on the job (again!) and the Countess will be[BR]"..
                   "absolutely furious if she catches you like this. Luckily, your best friend[BR]"..
                   "Lydie woke you up.[BR][BR]"..
                   "Get your chores done with Lydie's help!"

--Door not opened.
elseif(iWardenQuestState == 1.0) then

    --Objective.
    sObjective = "Find the secret of the Quantir Manor"
    
    --Description.
    sDescription = "The haunted Quantir Manor contains some sort of secret. The statues[BR]"..
                   "in the northwest hall pose a riddle. Something important must be[BR]"..
                   "past them."

--Found the kill room.
elseif(iWardenQuestState == 1.5) then

    --Objective.
    sObjective = "Find the secret of the Quantir Manor"
    
    --Description.
    sDescription = "The haunted Quantir Manor contains some sort of secret. The statues[BR]"..
                   "in the northwest hall pose a riddle. Something important must be[BR]"..
                   "past them.[BR][BR]"..
                   "A journal found in the basement indicates some hateful curse placed[BR]"..
                   "on the building. It might contain a clue."

--Met the countess.
elseif(iWardenQuestState == 2.0) then

    --Objective.
    sObjective = "Free the Countess, or Don't"
    
    --Description.
    sDescription = "Countess Josephina Quantir, or her ghost, is trapped in the manor.[BR]"..
                   "She's committed heinous crimes, and is punished by reliving her own[BR]"..
                   "murder daily.[BR][BR]"..
                   "You could free her by defeating her jailor, or leave her to her fate."

--Defeated the warden.
else

    --Objective.
    sObjective = "Complete"
    
    --Description.
    sDescription = "The Warden is dead and the curse is lifted. Countess Quantir's soul is[BR]"..
                   "freed. What will become of the ghosts? That remains to be seen.[BR][BR]"..
                   "There is nothing more you can do."

end

-- |[ =============================== Upload =============================== ]|
--Upload information.
zQuestEntry:fnSetToQuest(sObjective)
zQuestEntry:fnSetDescription(sDescription)