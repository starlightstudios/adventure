-- |[ ====================================== Dormine Tower ===================================== ]|
--Investigate the rubber tower!
local sInternalName = "Dormine Tower"
local sObjective = ""
local sDescription = ""
local zQuestEntry = JournalEntry:new(sInternalName, sInternalName)
    
--Variables.
local iRubberQuestState = VM_GetVar("Root/Variables/Chapter1/Scenes/iRubberQuestState", "N")

-- |[Blank Entry Check]|
if(iRubberQuestState < 1.0) then
    zQuestEntry:fnBlankQuest()
    return
end

-- |[ ============================== Variants ============================== ]|
--Stana mentions the plague.
if(iRubberQuestState == 1.0) then

    --Objective.
    sObjective = "Find a way across the river to check Dormine Tower"
    
    --Description.
    sDescription = "Stana mentioned a shiny plague across Dormine River. Might be[BR]"..
                   "worth looking in to.[BR]"..
                   "You'll have to go around, through Starfield Swamp, to reach[BR]"..
                   "Dormine Tower. See what you can find there."

--Nervous locals.
elseif(iRubberQuestState == 2.0) then

    --Objective.
    sObjective = "Investigate Dormine Tower"
    
    --Description.
    sDescription = "The local forest monstergirls are extremely nervous about[BR]"..
                   "a shiny plague near Dormine Tower.[BR][BR]"..
                   "See what you can find out."

--Spread...
elseif(iRubberQuestState == 3.0) then

    --Objective.
    sObjective = "Spread"
    
    --Description.
    sDescription = "You've been transformed into a rubber thrall. Spread the rubber to[BR]"..
                   "everyone in the world.[BR]"..
                   "Spread. Spread."

--Complete
elseif(iRubberQuestState == 4.0) then

    --Objective.
    sObjective = "Complete"
    
    --Description.
    sDescription = "The rubber master has achieved sentience and desires you to return to[BR]"..
                   "normal. The enemies of rubber are numerous and conquest not[BR]"..
                   "guaranteed if you are exposed. The master must learn why it is[BR]"..
                   "here and what has happened.[BR][BR]"..
                   "You will remain as a sleeper, unknowingly carrying the rubber within[BR]"..
                   "you. When the master wills it, you will reactivate and spread."

--"Failed" state.
elseif(iRubberQuestState == 10.0) then

    --Objective.
    sObjective = "Complete (Can be retried)"
    
    --Description.
    sDescription = "You got transformed into a rubber thrall, but luckily, the people[BR]"..
                   "of the forest stopped the plague before it got out of control.[BR][BR]"..
                   "(You can retry the quest by returning to Dormine Tower)."

end

-- |[ =============================== Upload =============================== ]|
--Upload information.
zQuestEntry:fnSetToQuest(sObjective)
zQuestEntry:fnSetDescription(sDescription)