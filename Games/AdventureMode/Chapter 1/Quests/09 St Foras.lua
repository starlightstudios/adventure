-- |[ ================================= Mystery of St. Fora's ================================== ]|
--Solve the mystery for Share;lock!
local sInternalName = "Mystery of St. Fora's"
local sObjective = ""
local sDescription = ""
local zQuestEntry = JournalEntry:new(sInternalName, sInternalName)
    
--Variables.
local iSharelockQuestState = VM_GetVar("Root/Variables/Chapter1/Sharelock/iSharelockQuestState", "N")

-- |[Blank Entry Check]|
if(iSharelockQuestState < 1.0) then
    zQuestEntry:fnBlankQuest()
    return
end

-- |[ ============================== Variants ============================== ]|
--Searching for stuff.
if(iSharelockQuestState == 1.0) then

    --Objective.
    sObjective = "Investigate St. Fora's"
    
    --Description.
    sDescription = "Share;Lock is looking for any sign of metallic monstergirls, and has[BR]"..
                   "encountered some oddities. There is a mystery afoot! Search the[BR]"..
                   "buildings for anything unusual. Check everything!"

--Secret switch!
elseif(iSharelockQuestState == 2.0) then

    --Objective.
    sObjective = "Investigate St. Fora's for signs of the missing campers"
    
    --Description.
    sDescription = "Share;Lock thinks there were campers using the area, but they are[BR]"..
                   "nowhere to be found. They must have vanished recently.[BR][BR]"..
                   "Find the secret of St. Fora's!"

--Complete.
else

    --Objective.
    sObjective = "Complete"
    
    --Description.
    sDescription = "With Share;Lock's help, you found the secret basement of St. Fora's.[BR]"..
                   "Unfortunately, she hurt herself in the process. She has retired to the[BR]"..
                   "nearby fishing village, so the treasure is all yours!"

end

-- |[ =============================== Upload =============================== ]|
--Upload information.
zQuestEntry:fnSetToQuest(sObjective)
zQuestEntry:fnSetDescription(sDescription)