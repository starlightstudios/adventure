-- |[ ===================================== The Summoning ====================================== ]|
--Stop those darn cultists!
local sInternalName = "The Summoning"
local sObjective = ""
local sDescription = ""
local zQuestEntry = JournalEntry:new(sInternalName, sInternalName)

--Variables.
local iInformedOfDungeon = VM_GetVar("Root/Variables/Chapter1/Scenes/iInformedOfDungeon", "N")
local iMeiUnlockedVineDoor = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiUnlockedVineDoor", "N")
local iSeenAlrauneBattleIntroScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iSeenAlrauneBattleIntroScene", "N")
local iCompletedTrapDungeon = VM_GetVar("Root/Variables/Chapter1/Scenes/iCompletedTrapDungeon", "N")

-- |[Blank Entry Check]|
if(iInformedOfDungeon < 1.0) then
    zQuestEntry:fnBlankQuest()
    return
end

-- |[ ============================== Variants ============================== ]|
--Hasn't check the door yet:
if(iMeiUnlockedVineDoor == 0.0) then

    --Objective.
    sObjective = "Search the Dimensional Trap Dungeon"
    
    --Description.
    sDescription = "The small side entrance on the west side of the Dimensional Trap hides[BR]"..
                   "a dark secret. Something bad is going on down there. Investigate,[BR]"..
                   "before it's too late!"
    
--Encountered the vine door.
elseif(iMeiUnlockedVineDoor == 2.0) then

    --Objective.
    sObjective = "Find a way to talk to the vine"
    
    --Description.
    sDescription = "The small side entrance on the west side of the Dimensional Trap hides[BR]"..
                   "a dark secret. Something bad is going on down there.[BR][BR]"..
                   "There's a vine holding the door shut, though. A nightshade alraune[BR]"..
                   "might be able to charm him into opening up."

--Opened the door.
elseif(iMeiUnlockedVineDoor == 1.0 and iCompletedTrapDungeon == 0.0) then

    --Hasn't seen the battle:
    if(iSeenAlrauneBattleIntroScene == 0.0) then

        --Objective.
        sObjective = "Investigate the dungeon"
        
        --Description.
        sDescription = "The small side entrance on the west side of the Dimensional Trap hides[BR]"..
                       "a dark secret. Something bad is going on down there.[BR][BR]"..
                       "You've made it past the vine door. Now see what's going on![BR]"

    --Has seen the battle:
    else

        --Objective.
        sObjective = "Stop the ritual"
        
        --Description.
        sDescription = "The small side entrance on the west side of the Dimensional Trap hides[BR]"..
                       "a dark secret. Something bad is going on down there.[BR][BR]"..
                       "Rochea and her covenant have made a path, but it's up to you to go[BR]"..
                       "further in and stop whatever the cult is up to."

    end

--Complete.
else

    --Objective.
    sObjective = "Complete"
    
    --Description.
    sDescription = "Whatever it was the cult was trying to do with that... thing... has[BR]"..
                   "been stopped and buried under a hundred tonnes of rock.[BR][BR]"..
                   "You get the feeling this isn't over. The creature wasn't even at a[BR]"..
                   "fraction of its full strength and was extremely dangerous."

end

-- |[ =============================== Upload =============================== ]|
--Upload information.
zQuestEntry:fnSetToQuest(sObjective)
zQuestEntry:fnSetDescription(sDescription)