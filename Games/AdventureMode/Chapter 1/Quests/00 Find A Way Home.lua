-- |[ ======================================= Main Quest ======================================= ]|
--Find a way home. Always generated.
    
-- |[ ================================ Setup =============================== ]|
--Setup.
local sInternalName = "Find A Way Home"
local sObjective = ""
local sDescription = ""
local zQuestEntry = JournalEntry:new(sInternalName, sInternalName)

--Variables.
local iMetSeptima                      = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetSeptima", "N")
local iEnteredNixNedar                 = VM_GetVar("Root/Variables/Chapter1/Scenes/iEnteredNixNedar", "N")
local iExaminedMirror                  = VM_GetVar("Root/Variables/Chapter1/Scenes/iExaminedMirror", "N")
local iMeiKnowsRilmani                 = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiKnowsRilmani", "N")
local iPolarisSuggestStForas           = VM_GetVar("Root/Variables/Chapter1/Polaris/iPolarisSuggestStForas", "N")
local iMetClaudia                      = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetClaudia", "N")
local iHasFoundOutlandAcolyte          = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasFoundOutlandAcolyte", "N")
local iHasSeenTrannadarFlorentinaScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
local iHasSeenCultistMeetingScene      = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenCultistMeetingScene", "N")

-- |[ =========================== Determine Variant ======================== ]|
--There are multiple ways to reach certain variants, so a set of codes is used for each
-- possible description block.
--The if/else structure basically constructs the variants backwards from how they are
-- encountered in the game.
local iDescriptionBlock = 0.0

-- |[Finale Block]|
--Has met the Rilmani:
if(iMetSeptima == 1.0) then
    iDescriptionBlock = 102.0
    
--Has entered Nix Nedar, not met the Rilmani:
elseif(iEnteredNixNedar == 1.0) then
    iDescriptionBlock = 101.0

--Has found, but not entered, the mirror:
elseif(iExaminedMirror == 1.0) then
    iDescriptionBlock = 100.0

-- |[Merge Block]|
--Either found the St. Fora's notes, or Heavenly Doves notes.
elseif(iMeiKnowsRilmani == 1.0) then
    iDescriptionBlock = 5.0

-- |[Secondary Track]|
--Gained after speaking to Polaris.
elseif(iPolarisSuggestStForas == 1.0) then
    iDescriptionBlock = 20.0

-- |[Primary Track]|
--Met Claudia, go find the journals.
elseif(iMetClaudia == 1.0) then
    iDescriptionBlock = 4.0

--Karina directs player to Quantir.
elseif(iHasFoundOutlandAcolyte == 1.0) then
    iDescriptionBlock = 3.0
    
--Florentina says to go to Outland Farm.
elseif(iHasSeenTrannadarFlorentinaScene == 1.0) then
    iDescriptionBlock = 2.0

--Cultists say to go to the trading post.
elseif(iHasSeenCultistMeetingScene == 1.0) then
    iDescriptionBlock = 1.0
end

-- |[ ============================ Primary Track =========================== ]|
-- |[Case: Start of Chapter]|
--Until seeing the cultist meeting scene:
if(iDescriptionBlock == 0.0) then

    --Objective.
    sObjective = "Get out of this stuffy basement."
    
    --Description.
    sDescription = "You've been kidnapped by a bunch of robed weirdos who wear chains[BR]" ..
                   "and seem pretty belligerent. Find a way out of this damp basement[BR]" ..
                   "and give any cultists a good drubbing if they get in the way."

-- |[Case: Saw Meeting, Heading to Trading Post]|
--Player has no hints other than to go to the trading post.
elseif(iDescriptionBlock == 1.0) then

    --Objective.
    sObjective = "Head to the Trading Post"
    
    --Description.
    sDescription = "You got a tip from the idiot cultists that the Trading Post is a good[BR]" ..
                   "place to check. They even gave you a map. You should head to the[BR]" ..
                   "Trading Post and see if anyone there knows how to get home."


-- |[Case: Met Florentina, Find the Doves]|
--Player has met Florentina, who advises checking Outland Farm.
elseif(iDescriptionBlock == 2.0) then

    --Objective.
    sObjective = "Search for the Doves"
    
    --Description.
    sDescription = "Florentina doesn't know what 'Earth' is, but thinks that runestone[BR]" ..
                   "you found is related in some way. She doesn't know what the symbol[BR]" ..
                   "means; luckily, she knows someone who might.[BR][BR]"..
                   "Search for Sister Claudia of the Heavenly Doves. She last heard they[BR]"..
                   "were going to Outland Farm, north of the Trading Post."

-- |[Case: Met Karina, Find the Doves]|
--Player has met Karina, who directs the player to the Quantir Estate.
elseif(iDescriptionBlock == 3.0) then

    --Objective.
    sObjective = "Search for the Doves in Quantir Estate"
    
    --Description.
    sDescription = "Sister Karina of the Heavenly Doves was studying bees at Outland[BR]" ..
                   "Farm. The rest of her convent left to search the Quantir Estate[BR]" ..
                   "for a rare monstergirl. They haven't come back.[BR][BR]"..
                   "Head to the northeast to Quantir and try to find out what happened[BR]"..
                   "to them, or at least find their research notes."

-- |[Case: Met Claudia, Find the Notes]|
--Player has met Claudia, who mentions her notes.
elseif(iDescriptionBlock == 4.0) then

    --Objective.
    sObjective = "Search for the research notes in the Quantir Estate"
    
    --Description.
    sDescription = "Sister Claudia of the Heavenly Doves did recognize the symbol, but[BR]" ..
                   "can't translate it without her notes. Those are in the Quantir Estate[BR]" ..
                   "somewhere, so you'll have to go find them.[BR][BR]"..
                   "The notes in question are in journals left on tables by the researchers."

-- |[Case: Found the Notes, Search for Rilmani Artifacts]|
--Player has found the notes, go find the mirror. Can also be done from St. Fora's.
elseif(iDescriptionBlock == 5.0) then

    --Objective.
    sObjective = "Search for the Dimensional Trap for Rilmani artifacts"
    
    --Description.
    sDescription = "You managed to translate your runestone! The language on it is[BR]" ..
                   "Rilmani, a rare monstergirl species. You might be able to find an[BR]" ..
                   "artifact of theirs, or even a live Rilmani, if you search the[BR]"..
                   "Dimensional Trap, which is rumoured to be related to them.[BR][BR]"..
                   "The Dimensional Trap has a basement, a main floor, and a roof area.[BR]"..
                   "You're looking for anything out of place. Don't mind the cultists."

-- |[ ========================== Secondary Track =========================== ]|
-- |[Case: Spoke to Polaris, Search St. Fora's]|
--Player spoke to Polaris, who mentioned St. Fora's.
elseif(iDescriptionBlock == 20.0) then

    --Objective.
    sObjective = "Search St. Fora's for information on your runestone"
    
    --Description.
    sDescription = "The swamp witch Polaris has seen the symbol on your runestone,[BR]" ..
                   "and thinks it might be draconic.[BR][BR]" ..
                   "Not a lot of people know the language any more, as it hasn't been used[BR]"..
                   "for centuries. There is a very old religious convent, St. Fora's, to the[BR]"..
                   "south of Starfield Swamp that might be ancient enough.[BR][BR]"..
                   "It might be worth searching for clues."

-- |[ =============================== Finale =============================== ]|
-- |[Case: Travel to Nix Nedar]|
--Player has found the mirror, go through.
elseif(iDescriptionBlock == 100.0) then

    --Objective.
    sObjective = "Go through the mirror"
    
    --Description.
    sDescription = "You've found the mirror in the Dimensional Trap, and hopefully, a way[BR]"..
                   "home.[BR][BR]" ..
                   "Take care of any pending affairs and head through."

-- |[Case: Explore Nix Nedar]|
--Player has reached Nix Nedar, find Septima.
elseif(iDescriptionBlock == 101.0) then

    --Objective.
    sObjective = "Explore the area"
    
    --Description.
    sDescription = "Well that was unexpected! You seem to be in between dimensions, or[BR]"..
                   "something to that effect.[BR][BR]" ..
                   "Look around and see what you can find."

-- |[Case: Finish Chapter]|
--Player has spoken to Septima, end the chapter.
elseif(iDescriptionBlock == 102.0) then

    --Objective.
    sObjective = "Speak to Septima"
    
    --Description.
    sDescription = "You've found the Rilmani! They seem to be friendly. Speak to[BR]"..
                   "Septima and determine your next move."
end

-- |[ =============================== Upload =============================== ]|
--Upload information.
zQuestEntry:fnSetToQuest(sObjective)
zQuestEntry:fnSetDescription(sDescription)
    