-- |[ ============================== Building a Better Runestone =============================== ]|
--Caused when Mei speaks to Polaris about the runestone.
local sInternalName = "Building a Better Runestone"
local sObjective = ""
local sDescription = ""
local zQuestEntry = JournalEntry:new(sInternalName, sInternalName)

--Variables.
local iPolarisRuneQuestState = VM_GetVar("Root/Variables/Chapter1/Polaris/iPolarisRuneQuestState", "N")

-- |[Blank Entry Check]|
if(iPolarisRuneQuestState < 1.0) then
    zQuestEntry:fnBlankQuest()
    return
end

-- |[ ============================== Variants ============================== ]|
--Polaris let slip she knows about the runestone.
if(iPolarisRuneQuestState == 1.0) then

    --Objective.
    sObjective = "Find out what Polaris knows about your runestone"
    
    --Description.
    sDescription = "Polaris let slip that she recognized the symbol on your runestone.[BR]"..
                   "But, she's hiding it. Ask her about it."

--Speak to Jean.
elseif(iPolarisRuneQuestState == 2.0) then

    --Objective.
    sObjective = "Speak to Jean about greaseflower"
    
    --Description.
    sDescription = "Polaris can probably clean off your runestone to make it more[BR]"..
                   "powerful, but she needs some greaseflower first. She said a farmer[BR]"..
                   "north of the swamp will know where some is.[BR][BR]"..
                   "The farmer in question is west of Outland Farm."

--Speak to Beth.
elseif(iPolarisRuneQuestState == 3.0) then

    --Objective.
    sObjective = "Speak to Beth about greaseflower"
    
    --Description.
    sDescription = "Jean said her daughter, Beth, will know where greaseflower is.[BR]"..
                   "She should be south of the Oak farm.[BR]"

--Get the greaseflower.
elseif(iPolarisRuneQuestState == 4.0) then

    --Objective.
    sObjective = "Find the Greaseflower in northwest Arbonne"
    
    --Description.
    sDescription = "Beth said some greaseflower grows in the fields northwest of[BR]"..
                   "the Oak farm, near the river.[BR]"..
                   "It's a white flower with four points."

--Return to Polaris.
elseif(iPolarisRuneQuestState == 5.0) then

    --Objective.
    sObjective = "Return to Polaris"
    
    --Description.
    sDescription = "You found the greaseflower, bring it back to Polaris.[BR]"..
                   "Ask her about your runestone again."

--Done!
elseif(iPolarisRuneQuestState == 6.0) then

    --Objective.
    sObjective = "Complete"
    
    --Description.
    sDescription = "Polaris cleaned off the runestone, and now it has been upgraded to[BR]"..
                   "Mark II! It heals for more and improves accuracy.[BR]"
end

-- |[ =============================== Upload =============================== ]|
--Upload information.
zQuestEntry:fnSetToQuest(sObjective)
zQuestEntry:fnSetDescription(sDescription)