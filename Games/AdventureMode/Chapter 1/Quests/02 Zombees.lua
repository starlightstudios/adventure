-- |[ ================================== Night of the Zombees ================================== ]|
--Caused when Mei speaks to the bees in the basement.
local sInternalName = "Night of the Zombees"
local sObjective = ""
local sDescription = ""
local zQuestEntry = JournalEntry:new(sInternalName, sInternalName)
    
--Variables.
local iZombeeQuestState = VM_GetVar("Root/Variables/Chapter1/Scenes/iZombeeQuestState", "N")
local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")

-- |[Blank Entry Check]|
if(iZombeeQuestState < 1.0) then
    zQuestEntry:fnBlankQuest()
    return
end

-- |[ ============================== Variants ============================== ]|
--Mei is a zombee!
if(sMeiForm == "Zombee") then

    --Objective.
    sObjective = "Assimilate the hive"
    
    --Description.
    sDescription = "You've been corrupted and reduced to a thoughtless drone under the[BR]"..
                   "command of an enormous, nameless force. Corrupt the other bees in[BR]"..
                   "the hive and do the same to them. Obey."

--Encountered the bees and they stopped you.
elseif(iZombeeQuestState == 1.0) then

    --Objective.
    sObjective = "Find out what's bothering the bees"
    
    --Description.
    sDescription = "The bees in the basement of the beehive seem agitated, but not hostile.[BR]"..
                   "Something seems very wrong. They won't let anyone further in, not[BR]"..
                   "even other bees.[BR]Investigate."
                
--Florentina calmed the bees down.
elseif(iZombeeQuestState == 2.0) then

    --Objective.
    sObjective = "Deal with the corruption in the hive."
    
    --Description.
    sDescription = "Something is turning the bees' stored honey purple and corrupting[BR]"..
                   "their drones! Thanks to Florentina, you've got a way in.[BR][BR]"..
                   "Find out what's causing this and put an end to it."

--The cult!
elseif(iZombeeQuestState == 3.0) then

    --Objective.
    sObjective = "Beat up the cultist corrupter!"
    
    --Description.
    sDescription = "Those cultist jerks who kidnapped you are messing with the[BR]"..
                   "bees! You've got to put a stop to it!"

--Day saved.
elseif(iZombeeQuestState == 4.0) then

    --Objective.
    sObjective = "Complete"
    
    --Description.
    sDescription = "The cultist causing the corruption in the hive has been defeated, and[BR]"..
                   "the bees will transform her to help clean things up. Hopefully they[BR]"..
                   "can take it from here."

end

-- |[ =============================== Upload =============================== ]|
--Upload information.
zQuestEntry:fnSetToQuest(sObjective)
zQuestEntry:fnSetDescription(sDescription)