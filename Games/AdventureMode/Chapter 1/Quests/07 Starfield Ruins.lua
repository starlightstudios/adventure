-- |[ ================================ Nothing You Can Possess ================================= ]|
--Pick open the lock in Starfield.
local sInternalName = "Nothing You Can Possess"
local sObjective = ""
local sDescription = ""
local zQuestEntry = JournalEntry:new(sInternalName, sInternalName)
    
--Variables.
local iLockpickQuestState = VM_GetVar("Root/Variables/Chapter1/Polaris/iLockpickQuestState", "N")

-- |[Blank Entry Check]|
if(iLockpickQuestState < 1.0) then
    zQuestEntry:fnBlankQuest()
    return
end

-- |[ ============================== Variants ============================== ]|
--Door not opened.
if(iLockpickQuestState == 1.0) then

    --Objective.
    sObjective = "Find a way to unlock the door in the Starfield ruins"
    
    --Description.
    sDescription = "A long-abandoned guard post in Starfield Swamp has a locked[BR]"..
                   "door you can't pass. Florentina can probably pick it open with[BR]"..
                   "some tools."

--Treasure Hunter get!
elseif(iLockpickQuestState == 2.0) then

    --Objective.
    sObjective = "Have Florentina pick the lock in the Starfield ruins"
    
    --Description.
    sDescription = "A long-abandoned guard post in Starfield Swamp has a locked[BR]"..
                   "door you can't pass.[BR][BR]"..
                   "Sporting a spiffy new hat, Florentina can now pick the lock.[BR]"..
                   "Equip her Pick Lock field ability and use it to get the lock open!"
                   
--Complete.
else

    --Objective.
    sObjective = "Complete"
    
    --Description.
    sDescription = "Florentina picked the lock in the Starfield ruins.[BR]"..
                   "Now let's find some treasure!"

end

-- |[ =============================== Upload =============================== ]|
--Upload information.
zQuestEntry:fnSetToQuest(sObjective)
zQuestEntry:fnSetDescription(sDescription)