-- |[ ================================= Pepper Pie Procurement ================================= ]|
--Breanne's Pepper Pie quest!
local sInternalName = "Pepper Pie Procurement"
local sObjective = ""
local sDescription = ""
local zQuestEntry = JournalEntry:new(sInternalName, sInternalName)
    
--Variables.
local iTakenPieJob = VM_GetVar("Root/Variables/Chapter1/Breanne/iTakenPieJob", "N")

-- |[Blank Entry Check]|
if(iTakenPieJob < 1.0) then
    zQuestEntry:fnBlankQuest()
    return
end

-- |[ ============================== Variants ============================== ]|
--On the job for pie parts!
if(iTakenPieJob == 1.0) then

    --Objective.
    sObjective = "Get the pie ingredients!"
    
    --Description.
    sDescription = "Breanne wants your help getting ingredients for her Pepper Pie.[BR]"..
                   "Florentina might have some hints on where to find them.[BR][BR]"
                   
    --Item Variables.
    local iMossCount      = AdInv_GetProperty("Item Count", "Gaardian Cave Moss")
    local iPaperCount     = AdInv_GetProperty("Item Count", "Booped Paper")
    local iNectarCount    = AdInv_GetProperty("Item Count", "Decayed Bee Nectar")
    local iFlowerCount    = AdInv_GetProperty("Item Count", "Hypnotic Flower Petals")
    local iSalamiCount    = AdInv_GetProperty("Item Count", "Translucent Quantirian Salami")
    local iKokayaneeCount = AdInv_GetProperty("Item Count", "Kokayanee")
    
    --Moss:
    sDescription = sDescription .. "1: Gaardian Cave Moss"
    if(iMossCount > 0) then
        sDescription = sDescription .. "(Check)[BR]"
    else
        sDescription = sDescription .. "[BR]"
    end
    
    --Booped Paper:
    sDescription = sDescription .. "2: Corgi Booped Paper"
    if(iPaperCount > 0) then
        sDescription = sDescription .. "(Check)[BR]"
    else
        sDescription = sDescription .. "[BR]"
    end
    
    --Decayed Bee Nectar:
    sDescription = sDescription .. "3: Decayed Bee Nectar"
    if(iNectarCount > 0) then
        sDescription = sDescription .. "(Check)[BR]"
    else
        sDescription = sDescription .. "[BR]"
    end
    
    --Hypnotic Flower Petals:
    sDescription = sDescription .. "4: Hypnotic Flower Petals"
    if(iFlowerCount > 0) then
        sDescription = sDescription .. "(Check)[BR]"
    else
        sDescription = sDescription .. "[BR]"
    end
    
    --Translucent Quantirian Salami:
    sDescription = sDescription .. "5: Quantirian Salami"
    if(iSalamiCount > 0) then
        sDescription = sDescription .. "(Check)[BR]"
    else
        sDescription = sDescription .. "[BR]"
    end
    
    --Kokayanee:
    sDescription = sDescription .. "6: Kokayanee"
    if(iKokayaneeCount > 0) then
        sDescription = sDescription .. "(Check)[BR]"
    else
        sDescription = sDescription .. "[BR]"
    end
                   
--Done!
elseif(iTakenPieJob == 2.0) then

    --Objective.
    sObjective = "Complete"
    
    --Description.
    sDescription = "Breanne baked a Pepper Pie! It tastes great and can be equipped to[BR]"..
                   "greatly increase your damage in battle."
end

-- |[ =============================== Upload =============================== ]|
--Upload information.
zQuestEntry:fnSetToQuest(sObjective)
zQuestEntry:fnSetDescription(sDescription)