-- |[ ================================ Adina and the Salt Flats ================================ ]|
--Generated when Mei meets Adina.
local sInternalName = "Salt Flats"
local sObjective = ""
local sDescription = ""
local zQuestEntry = JournalEntry:new(sInternalName, sInternalName)

--Variables.
local iAdinaQuestVar = VM_GetVar("Root/Variables/Chapter1/Scenes/iAdinaQuestVar", "N")

-- |[Blank Entry Check]|
if(iAdinaQuestVar < 1.0) then
    zQuestEntry:fnBlankQuest()
    return
end

-- |[ ============================== Variants ============================== ]|
--Mei has met Adina, but didn't do anything.
if(iAdinaQuestVar == 1.0) then

    --Objective.
    sObjective = "Speak to Adina"
    
    --Description.
    sDescription = "A strange alraune tends the salt flats. She seems honest enough, and[BR]"..
                   "wonders why you don't trust her.[BR]"..
                   "Maybe open up a little?"

--Mei met Adina and something (form, Florentina) cut it off.
elseif(iAdinaQuestVar == 2.0) then

    --Objective.
    sObjective = "Complete"
    
    --Description.
    sDescription = "Adina tends the salt flats. She seems nice enough."

--Mei is mind-controlled and working for Adina.
elseif(iAdinaQuestVar == 3.0) then

    --Objective.
    sObjective = "Perform tasks for your Mistress"
    
    --Description.
    sDescription = "You are enthralled by Mistress Adina. Perform tasks for her on the[BR]"..
                   "salt flats until she orders otherwise."

--Go to bed.
elseif(iAdinaQuestVar == 4.0) then

    --Objective.
    sObjective = "Rest for the night"
    
    --Description.
    sDescription = "You are enthralled by Mistress Adina. She has ordered you to attend[BR]"..
                   "to her in her bedchambers."

--Mei broke free and didn't fall in love.
elseif(iAdinaQuestVar == 5.0) then

    --Objective.
    sObjective = "Complete"
    
    --Description.
    sDescription = "Adina tried to mind control you! Luckily, you shook it off. Still,[BR]"..
                   "she was just trying to demonstrate she's trustworthy, and didn't do[BR]"..
                   "anything to actually harm you. In fact, it was kinda fun..."

--Mei broke free and did fall in love.
elseif(iAdinaQuestVar == 6.0) then

    --Objective.
    sObjective = "Complete"
    
    --Description.
    sDescription = "You fell in love with Mistress Adina and want nothing more than to[BR]"..
                   "serve her on the salt flats, but you are still compelled to find a[BR]"..
                   "way home. Hopefully, you can return to her when your affairs on Earth[BR]"..
                   "are taken care of."
end

-- |[ =============================== Upload =============================== ]|
--Upload information.
zQuestEntry:fnSetToQuest(sObjective)
zQuestEntry:fnSetDescription(sDescription)
    