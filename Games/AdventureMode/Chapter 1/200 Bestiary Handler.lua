-- |[ ==================================== Bestiary Handler ==================================== ]|
--Whenever the journal is opened to bestiary mode, run this script to populate bestiary entries.

-- |[Stat Table Check]|
if(gczaChapter1Stats == nil) then return end

-- |[Setup]|
--Access variable.
local zEntry = nil 

--Create chart for storage.
gzaCh1BestiaryEntries = {}

--Set active globals. This reduces the number of arguments that need to be passed.
JournalEntry.zaEnemyStats = gczaChapter1Stats
JournalEntry.zaAppendList = gzaCh1BestiaryEntries

-- |[Prototype]|
--zEntry = JournalEntry:new(sUniqueName, sDisplayName)
--zEntry:fnSetToChartEntry (sNameOnStatChart, bAlwaysShow, fPortraitOffsetX, fPortraitOffsetY, bCanTFPlayer)
--zEntry:fnSetDescription  (sDescription)

-- |[ =============== Listing ================ ]|
-- |[Dimensional Trap Basement]|
zEntry = JournalEntry:new("Initiate F", "Cultist Initiate (Female)")
zEntry:fnSetToChartEntry ("Initiate F", gbDoNotAlwaysShow, 750, 143, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A lowly initiate in the cult that kidnapped Mei. Not a warrior, but[BR]fanatical enough to be dangerous.[BR]Is notably vulnerable to bleed and blind effects." .. 
                          "[BR][BR][BR][BR]Commonly Found: Dimensional Trap.")

zEntry = JournalEntry:new("Initiate M", "Cultist Initiate (Male)")
zEntry:fnSetToChartEntry ("Initiate M", gbDoNotAlwaysShow, 739, 167, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A lowly initiate in the cult that kidnapped Mei. Not a warrior, but[BR]fanatical enough to be dangerous.[BR]Is notably vulnerable to bleed and blind effects." .. 
                          "[BR][BR][BR][BR]Commonly Found: Dimensional Trap")

zEntry = JournalEntry:new("Adept F", "Cultist Adept (Female)")
zEntry:fnSetToChartEntry ("Adept F", gbDoNotAlwaysShow, 750, 143, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A mid-level adept in the cult that kidnapped Mei. More experienced[BR]with the... intricacies... of dissection and sacrifice, and attendantly[BR]more dangerous." .. 
                          "[BR][BR][BR][BR]Commonly Found: Dimensional Trap")

zEntry = JournalEntry:new("Adept M", "Cultist Adept (Male)")
zEntry:fnSetToChartEntry("Adept M", gbDoNotAlwaysShow, 739, 167, gbCannotTFPlayer)
zEntry:fnSetDescription("A mid-level adept in the cult that kidnapped Mei. More experienced[BR]with the... intricacies... of dissection and sacrifice, and attendantly[BR]more dangerous." .. 
                        "[BR][BR][BR][BR]Commonly Found: Dimensional Trap")

-- |[Evermoon Forest]|
zEntry = JournalEntry:new("Sproutling", "Sproutling")
zEntry:fnSetToChartEntry ("Sproutling", gbDoNotAlwaysShow, 821, 226, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A tiny baby plant! These cute motile plants mostly stumble around[BR]as they scrounge for insects to eat. The alraunes protect them and[BR]help them grow into the " ..
                          "very helpful pest-controlling Sprout Peckers.[BR][BR][BR][BR]Commonly Found: Evermoon Forest")

zEntry = JournalEntry:new("Sprout Pecker", "Sprout Pecker")
zEntry:fnSetToChartEntry ("Sprout Pecker", gbDoNotAlwaysShow, 637, 153, gbCannotTFPlayer)
zEntry:fnSetDescription  ("In the same genus as the venus fly trap, the Sprout Pecker, for lack[BR]of a better term, is a motile plant that consumes insects near marshy[BR]locations and consumes " .. 
                          "parasites that burrow into tree bark.[BR]They are often seen with alraunes, who shepherd and protect them.[BR]This is the adult form of a Sproutling.[BR][BR]Commonly Found: Evermoon Forest")

zEntry = JournalEntry:new("Alraune Petal", "Alraune Petal")
zEntry:fnSetToChartEntry ("Alraune Petal", gbDoNotAlwaysShow, 800, 140, gbCanTFPlayer)
zEntry:fnSetDescription  ("One of the plant women who live in the forest. Many of them despise[BR]humans for their despoiling nature.[BR]Their bodies are capable of metabolizing " ..
                          "sunlight and CO2, allowing[BR]them to spend their time basking in the sunlight.[BR]Sometimes called 'Daughter of the Wilds'.[BR]Given the chance, they will " .. 
                          "transform any human they find.[BR]Commonly Found: Evermoon Forest")

zEntry = JournalEntry:new("Bee Scout", "Bee Scout")
zEntry:fnSetToChartEntry ("Bee Scout", gbDoNotAlwaysShow, 797, 152, gbCanTFPlayer)
zEntry:fnSetDescription  ("An insect girl, common in the forest, who constantly searches for[BR]plants with nectar. Capable of limited flight. Their antennae make[BR]them difficult to " ..
                          "blind due to enhanced perception.[BR]Their antennae allow a short-range hive mind to guide individual[BR]drones.[BR]They are always looking for new, capable bee " ..
                          "scouts.[BR]Commonly Found: Evermoon Forest, Beehive")

zEntry = JournalEntry:new("Bee Buddy", "Bee Buddy")
zEntry:fnSetToChartEntry ("Bee Buddy", gbDoNotAlwaysShow, 775, 168, gbCannotTFPlayer)
zEntry:fnSetDescription  ("An exceedingly large bee domesticated by the bee girls. They help[BR]them search for nectar and defend their scouts in the field.[BR]Small and fairly weak, they " ..
                          "can be defeated by splash-damage[BR]abilities.[BR][BR][BR]Commonly Found: Evermoon Forest")

zEntry = JournalEntry:new("Bubbly Slimegirl", "Bubbly Slimegirl")
zEntry:fnSetToChartEntry ("Bubbly Slimegirl", gbDoNotAlwaysShow, 660, 168, gbCanTFPlayer)
zEntry:fnSetDescription  ("Somehow, this glob of goop is alive, and shaped like a busty woman.[BR]Particularly vulnerable to freezing attacks, they have a lot of health,[BR]and are not to be taken lightly." ..
                          "[BR]They are happy to add you to their ranks, if you let them.[BR][BR][BR]Commonly Found: Evermoon Forest")

zEntry = JournalEntry:new("Chilly Slimegirl", "Chilly Slimegirl")
zEntry:fnSetToChartEntry ("Chilly Slimegirl", gbDoNotAlwaysShow, 660, 168, gbCanTFPlayer)
zEntry:fnSetDescription  ("Inexplicably, a slime that has cold attributes and is freezing to the[BR]touch. Still as durable as any other slime, but vulnerable to flames." ..
                          "[BR]They are happy to add you to their ranks, if you let them.[BR][BR][BR][BR]Commonly Found: Evermoon Forest")

zEntry = JournalEntry:new("Toxic Slimegirl", "Toxic Slimegirl")
zEntry:fnSetToChartEntry ("Toxic Slimegirl", gbDoNotAlwaysShow, 660, 168, gbCanTFPlayer)
zEntry:fnSetDescription  ("One of the many slimegirls found in the forest, this one is more toxic[BR]than normal, and is capable of poisoning you. She's also more[BR]resistant to poison. However, they are " ..
                          "still vulnerable to cold attacks[BR]like most slimegirls.[BR][BR][BR]Commonly Found: Evermoon Forest")

zEntry = JournalEntry:new("Inky Slimegirl", "Inky Slimegirl")
zEntry:fnSetToChartEntry ("Inky Slimegirl", gbDoNotAlwaysShow, 713, 156, gbCanTFPlayer)
zEntry:fnSetDescription  ("A slimegirl who has consumed enough ink to recolor her completely[BR]black. Tougher and thicker than a normal slimegirl, but just as[BR]vulnerable to cold.[BR][BR][BR]" ..
                          "If she transforms you, you'll become a regular slimegirl, unfortunately.[BR]Commonly Found: Evermoon Forest")

zEntry = JournalEntry:new("Jelli", "Jelli")
zEntry:fnSetToChartEntry ("Jelli", gbDoNotAlwaysShow, 675, 270, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A slimegirl who is not currently taking human form. Has considerably[BR]less health as a result, but retains the vulnerability to cold." ..
                          "[BR][BR][BR][BR][BR]Commonly Found: Evermoon Forest")

zEntry = JournalEntry:new("Alraune Stem", "Alraune Stem")
zEntry:fnSetToChartEntry ("Alraune Stem", gbDoNotAlwaysShow, 800, 140, gbCanTFPlayer)
zEntry:fnSetDescription  ("A stronger alraune, experienced in the role of a nature guardian and[BR]more than capable of using violence to that end." ..
                          "[BR][BR][BR][BR][BR]Commonly Found: Evermoon Forest")

zEntry = JournalEntry:new("Bee Gatherer", "Bee Gatherer")
zEntry:fnSetToChartEntry ("Bee Gatherer", gbDoNotAlwaysShow, 797, 152, gbCanTFPlayer)
zEntry:fnSetDescription  ("A more senior bee when it comes to both gathering nectar, and[BR]fighting enemies of the hive." ..
                          "[BR][BR][BR][BR][BR]Commonly Found: Evermoon Forest, Beehive")

zEntry = JournalEntry:new("Bee Sniffer", "Bee Sniffer")
zEntry:fnSetToChartEntry ("Bee Sniffer", gbDoNotAlwaysShow, 775, 168, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A domesticated bee trained to help the bee girls with their[BR]nectar searches. Better trained, the sniffer is still physically[BR]weak and easily defeated." ..
                          "[BR][BR][BR][BR]Commonly Found: Starfield Swamp")

zEntry = JournalEntry:new("Werecat Hunter", "Werecat Hunter")
zEntry:fnSetToChartEntry ("Werecat Hunter", gbDoNotAlwaysShow, 751, 162, gbCanTFPlayer)
zEntry:fnSetDescription  ("Perhaps the most dangerous creature commonly encountered in the[BR]wild, the werecat is extremely agile and capable of impressive[BR]damage. They are cunning " ..
                          "hunters known for tracking their prey[BR]solo or in small groups. They also sometimes become dedicated[BR]fishercats or scrounge for things to sell for fun.[BR]"..
                          "Strong humans are the ideal targets for conversion, so beware![BR]Commonly Found: Evermoon Forest")

-- |[Quantir Manor]|
zEntry = JournalEntry:new("Ghost Maid", "Ghost Maid")
zEntry:fnSetToChartEntry ("Ghost Maid", gbDoNotAlwaysShow, 672, 160, gbCanTFPlayer)
zEntry:fnSetDescription  ("The undead remnants of the Quantir Estate's service staff. Their[BR]ghostly form is extremely resistant to bleeding and poison as well as[BR]conventional physical attacks. " ..
                          "If left alone, they will resume their[BR]unending work of tidying the manor up.[BR][BR][BR]Commonly Found: Quantir Estate")


zEntry = JournalEntry:new("Bloody Mirror", "Bloody Mirror")
zEntry:fnSetToChartEntry ("Bloody Mirror", gbDoNotAlwaysShow, 821, 180, gbCannotTFPlayer)
zEntry:fnSetDescription  ("An ornamental hand-held mirror possessed by the spirit of the[BR]long-dead victim of an unknown disease. The mirror is cracked in[BR]several places. A bloody hand print, " ..
	                      "perhaps the last vestige of[BR]the former owner's humanity, is streaked across it.[BR]Though resiliant against cutting, smashing the mirror should be easy[BR]as long as " ..
                          "you don't mind a little misfortune.[BR]Commonly Found: Quantir Estate")

zEntry = JournalEntry:new("Candle Haunt", "Candle Haunt")
zEntry:fnSetToChartEntry ("Candle Haunt", gbDoNotAlwaysShow, 853, 175, gbCannotTFPlayer)
zEntry:fnSetDescription  ("An old lamp possessed by the spirit of someone who died long ago.[BR]An ethereal eye seems to look out from the flame, though what it[BR]seeks is " ..
	                      "unknown.[BR][BR]A bit of water would do well to extinguish the ever-watchful gaze.[BR][BR]Commonly Found: Quantir Estate")

zEntry = JournalEntry:new("Eye Drawer", "Eye Drawer")
zEntry:fnSetToChartEntry ("Eye Drawer", gbDoNotAlwaysShow, 531, 174, gbCannotTFPlayer)
zEntry:fnSetDescription  ("An ornate cupboard or small wardrobe that has been given life by[BR]a malevolent force and that has had the soul of the dead infused[BR]within its " ..
	                      "carved wood.[BR][BR]Sturdy and resiliant to most physical attacks, a bit of fire will[BR]turn this poltergeist into kindling.[BR]Commonly Found: Quantir Estate")

zEntry = JournalEntry:new("Mirror Image", "Mirror Image")
zEntry:fnSetToChartEntry ("Mirror Image", gbDoNotAlwaysShow, 802, 136, gbCannotTFPlayer)
zEntry:fnSetDescription  ("It looks like a man, but it balances gently on blades of steel or[BR]perhaps bone. What passes for a face has an odd growth of grey[BR]crystals on it, which may be part " ..
                          "of its biology, or a parasite equally[BR]as vile as the host.[BR][BR][BR]Commonly Found: Under Starfield Swamp")

-- |["Other"]|
zEntry = JournalEntry:new("Best Friend", "Best Friend")
zEntry:fnSetToChartEntry ("Best Friend", gbDoNotAlwaysShow, 680, 170, gbCannotTFPlayer)
zEntry:fnSetDescription  ("Some sort of awful amalgamation of good dogs turned bad.[BR]Whatever it is, kill it, and kill it quickly." ..
                          "[BR][BR][BR][BR][BR]Commonly Found: Under Starfield Swamp")

zEntry = JournalEntry:new("Skitterer", "Skitterer")
zEntry:fnSetToChartEntry ("Skitterer", gbDoNotAlwaysShow, 661, 245, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A loathsome creature unaccustomed to daylight, Skitterers are quick[BR]and hard to hit. They also are not native to Pandemonium, but have[BR]been appearing in greater numbers "..
                          "every year.[BR][BR][BR][BR]Commonly Found: Damp Caves")

-- |[Starfield Swamp]|
zEntry = JournalEntry:new("Mushraune", "Mushraune")
zEntry:fnSetToChartEntry ("Mushraune", gbDoNotAlwaysShow, 755, 159, gbCannotTFPlayer)
zEntry:fnSetDescription  ("An alraune that has fungus growing all over her. Her normally leafy[BR]appearance is greatly decayed.[BR][BR]She seems a lot more horny than usual. Keep your distance."..
                          "[BR][BR][BR]Commonly Found: St. Fora's Convent")

zEntry = JournalEntry:new("Wisphag", "Wisphag")
zEntry:fnSetToChartEntry ("Wisphag", gbDoNotAlwaysShow, 574, 124, gbCanTFPlayer)
zEntry:fnSetDescription  ("The enigmatic wisphags are frequently found in swampy areas and do[BR]not take kindly to intruders. Their earthen claws are sharp and they[BR]cannot be reasoned with, despite their "..
                          "humanoid appearance.[BR][BR][BR][BR]Commonly Found: Starfield Swamp")

zEntry = JournalEntry:new("Werecat Prowler", "Werecat Prowler")
zEntry:fnSetToChartEntry ("Werecat Prowler", gbDoNotAlwaysShow, 751, 162, gbCanTFPlayer)
zEntry:fnSetDescription  ("As agile and dangerous as their kin, these hunters are known for[BR]their patience as they stalk their prey and attack when presented[BR]with " ..
	                      "the most opportune chance. Whether that opportune moment is[BR]when their prey is the least defended, could offer the best[BR]fight, or something " ..
                          "more... carnal... is a choice made by each[BR]individual werecat.[BR]Commonly Found: Starfield Swamp, Evermoon Forest")

-- |[Rubber Wilds]|
zEntry = JournalEntry:new("Rubberraune", "Rubberraune")
zEntry:fnSetToChartEntry ("Rubberraune", gbDoNotAlwaysShow, 825, 159, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A rubbery substance covers the body of this alraune. She ambles[BR]about, tending to the trees and plants like normal, but does so " ..
                          "by[BR]coating them in a thin layer of rubber.[BR][BR]An eternal grin is plastered to her face, a sign of the wonders[BR]they have " ..
                          "become and a welcoming beacon that calls you to follow...[BR]Commonly Found: Dormine River Wilds")

zEntry = JournalEntry:new("Rubbercat", "Rubbercat")
zEntry:fnSetToChartEntry ("Rubbercat", gbDoNotAlwaysShow, 751, 162, gbCannotTFPlayer)
zEntry:fnSetDescription  ("This werecat's body is covered in a layer of squeaky rubber. She[BR]continues to prowl about, seemingly unconcerned with the noise " ..
	                      "she[BR]is now making.[BR][BR]A wide grin stretches across her rubbered face, welcoming those who[BR]have yet to experience the pliable " ..
                          "joys of rubberization.[BR]Found: Dormine River Wilds")

zEntry = JournalEntry:new("RubbercatThief", "Rubbercat Thief")
zEntry:fnSetToChartEntry ("RubbercatThief", gbDoNotAlwaysShow, 745, 163, gbCannotTFPlayer)
zEntry:fnSetDescription  ("This werecat's body is covered with a layer of squeaky rubber. The[BR]noise this causes does not seem to hinder her as she skulks " ..
	                      "about in[BR]the shadows, looking for purses to snatch and a body to enjoin to the[BR]wonderful joys of rubberization.[BR][BR][BR]Commonly Found: Dormine River Wilds")

zEntry = JournalEntry:new("Rubberbee", "Rubberbee")
zEntry:fnSetToChartEntry ("Rubberbee", gbDoNotAlwaysShow, 768, 136, gbCannotTFPlayer)
zEntry:fnSetDescription  ("This bee girl's body is coated in a thin layer of rubber. If the[BR]smile on her face is any indication, she is very happy with this.[BR]She " ..
	                            "appears to be harvesting nectar as normal, but after watching[BR]further, it becomes apparent she is depositing drops of rubber that[BR]then " ..
								"begin to spread.[BR][BR]Commonly Found: Dormine River Wilds")

-- |[South Vendrilstadt]|
zEntry = JournalEntry:new("Bandit Scrub", "Bandit Scrub")
zEntry:fnSetToChartEntry ("Bandit Scrub", gbDoNotAlwaysShow, 671, 153, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A common cutpurse, the type which litters the lawless roads of[BR]Arulenta. Whether ex-military, a hopeless farmer turned thief, or a[BR]professional burglar, " ..
	                      "this kind of crook is a dime a dozen." ..
	                      "[BR][BR][BR][BR]Commonly Found: South of Vendrilstadt")

zEntry = JournalEntry:new("Bandit Goon", "Bandit Goon")
zEntry:fnSetToChartEntry ("Bandit Goon", gbDoNotAlwaysShow, 745, 153, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A common cutpurse, the type which litters the lawless roads of[BR]Arulenta. Whether ex-military, a hopeless farmer turned thief, or a[BR]professional burglar, " ..
                          "this kind of crook is a dime a dozen." ..
                          "[BR][BR][BR][BR]Commonly Found: South of Vendrilstadt")

zEntry = JournalEntry:new("Demonic Mercenary", "Demonic Mercenary")
zEntry:fnSetToChartEntry ("Demonic Mercenary", gbDoNotAlwaysShow, 733, 133, gbCannotTFPlayer)
zEntry:fnSetDescription  ("Shrouded in layers of clothing with barely any features visible,[BR]demons on Pandemonium like to hide their identities from casual[BR]observers. They are " ..
                          "ever hounded by the church and many human[BR]authorities, and often keep either to their own kind, or the sort who[BR]can pay their fees." ..
	                      "[BR][BR]Commonly Found: South of Vendrilstadt")

zEntry = JournalEntry:new("Bandit Boss", "Bandit Boss")
zEntry:fnSetToChartEntry ("Bandit Boss", gbDoNotAlwaysShow, 675, 136, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A local big wig in a bandit group, usually a lieutenant of a real leader.[BR]Bosses crack the whip of their local squaddies but inspire loyalty by[BR]leading from the front." ..
	                      "[BR][BR][BR][BR]Commonly Found: South of Vendrilstadt")

zEntry = JournalEntry:new("Mannequin Scrub", "Mannequin Scrub")
zEntry:fnSetToChartEntry ("Mannequin Scrub", gbDoNotAlwaysShow, 676, 133, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A bandit cursed by the mannequin spell, they will follow the orders of[BR]whoever is responsible for spreading the curse. If that person is lost,[BR]they act on their own. " ..
                          "Who it is they truly serve is a mystery, and[BR]they are most untalkative." ..
	                      "[BR][BR][BR]Commonly Found: South of Vendrilstadt")

zEntry = JournalEntry:new("Mannequin Goon", "Mannequin Goon")
zEntry:fnSetToChartEntry ("Mannequin Goon", gbDoNotAlwaysShow, 598, 135, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A bandit cursed by the mannequin spell, they will follow the orders of[BR]whoever is responsible for spreading the curse. If that person is lost,[BR]they act on their own. " ..
                                     "Who it is they truly serve is a mystery, and[BR]they are most untalkative." ..
                                     "[BR][BR][BR]Commonly Found: South of Vendrilstadt")

-- |[Tougher Wilds Area]|
zEntry = JournalEntry:new("Alraune Keeper", "Alraune Keeper")
zEntry:fnSetToChartEntry ("Alraune Keeper", gbDoNotAlwaysShow, 800, 140, gbCanTFPlayer)
zEntry:fnSetDescription  ("One of nature's self-appointed guardians, these plant women are[BR]exalted within the alraune covens for both their dedication and[BR]success at combating " ..
	                      "the human insurgency against the natural order.[BR][BR]Among their greatest accomplishments are the myriad victims whom[BR]they have forcefully converted to " ..
                          "their cause.[BR]Commonly Found: Dormine River Wilds")

zEntry = JournalEntry:new("Sprout Thrasher", "Sprout Thrasher")
zEntry:fnSetToChartEntry ("Sprout Thrasher", gbDoNotAlwaysShow, 637, 153, gbCannotTFPlayer)
zEntry:fnSetDescription  ("The adult form of a sproutling, the Sprout Thrasher is a Sprout[BR]Pecker that the alraune have trained to aid them against the[BR]ever-encroaching population of humans. " ..
                          "Their alraune handlers are[BR]never far from these trained planthounds.[BR][BR][BR]Commonly Found: Dormine River Wilds")

zEntry = JournalEntry:new("Bee Skirmisher", "Bee Skirmisher")
zEntry:fnSetToChartEntry ("Bee Skirmisher", gbDoNotAlwaysShow, 797, 152, gbCanTFPlayer)
zEntry:fnSetDescription  ("More capable at fighting than their scout sisters, these insect[BR]girls are a regular sight in dangerous areas that still offer a[BR]bounty of nectar. "..
                          "Capable of limited flight and difficult to blind[BR]due to the enhanced percept from their antennae.[BR]As part of their duties into dangerous areas, "..
                          "they keep a watchful[BR]eye for potential new additions to their hive.[BR]Commonly Found: Dormine River Wilds")

zEntry = JournalEntry:new("Bee Tracker", "Bee Tracker")
zEntry:fnSetToChartEntry ("Bee Tracker", gbDoNotAlwaysShow, 775, 168, gbCannotTFPlayer)
zEntry:fnSetDescription  ("Tougher than the other bee buddies, these trackers accompany their[BR]beegirl handlers into more dangerous areas in search of nectar.[BR]Though stronger than their kin, they "..
                          "are still physically weak.[BR]Use splash-damage attacks.[BR][BR][BR]Commonly Found: Starfield Swamp")

zEntry = JournalEntry:new("Werecat Thief", "Werecat Thief")
zEntry:fnSetToChartEntry ("Werecat Thief", gbDoNotAlwaysShow, 740, 163, gbCanTFPlayer)
zEntry:fnSetDescription  ("These werecats prefer to use their agility for ventures far more[BR]lucrative than mere combat. It has been said that many a maiden[BR]seeking a " ..
	                      "more interesting life have dreamed of the deft graces of[BR]these werecats, but only after they've trimmed their nails.[BR][BR][BR]Commonly Found: Dormine River Wilds")

-- |[Beehive Basement]|
zEntry = JournalEntry:new("Zombee", "Zombee")
zEntry:fnSetToChartEntry ("Zombee", gbDoNotAlwaysShow, 763, 157, gbCanTFPlayer)
zEntry:fnSetDescription  ("An unknown -- and perhaps unknowable -- force has corrupted this[BR]bee girl and severed her link with the hive. Her eyes are devoid of[BR]any recognition " ..
	                      "beyond a slavish adherence to her assigned duties.[BR]Capable of limited flight and driven by a preternatural strength,[BR]she will actively attack anyone " ..
                          "who tries to prevent her from[BR]fulfilling her duties.[BR]Commonly Found: Beehive Basement")

-- |[Dimensional Trap Dungeon]|
zEntry = JournalEntry:new("Finger", "Finger")
zEntry:fnSetToChartEntry ("Finger", gbDoNotAlwaysShow, 750, 143, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A high-ranking member in the cult that kidnapped Mei. Though not a[BR]natural fighter, their fanatical devotion to their tenets drives[BR]every action " ..
                          "of their daily lives. They ruthlessly attack anyone[BR]who has not yet joined them. Truly a dangerous lunatic.[BR][BR]Still highly vulnerable to bleed " ..
                          "and blind effects.[BR]Commonly Found: Dimensional Trap")

zEntry = JournalEntry:new("Palm", "Palm")
zEntry:fnSetToChartEntry ("Palm", gbDoNotAlwaysShow, 739, 167, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A high-ranking member in the cult that kidnapped Mei. Though not a[BR]natural fighter, their fanatical devotion to their tenets drives[BR]every action " ..
                          "of their daily lives. They ruthlessly attack anyone[BR]who has not yet joined them. Truly a dangerous lunatic.[BR][BR]Still highly vulnerable to bleed " ..
                          "and blind effects.[BR]Commonly Found: Dimensional Trap")

-- |[St. Fora's]|
zEntry = JournalEntry:new("Gravemarker", "Gravemarker")
zEntry:fnSetToChartEntry ("Gravemarker", gbDoNotAlwaysShow, 607, 98, gbCanTFPlayer)
zEntry:fnSetDescription  ("A silent sentry who guards the sanctified grounds of the Holy Light.[BR]They are quick to mete punishment to those who break their " ..
	                      "laws,[BR]generally by transforming the transgressor into one of their own.[BR]They have no mouths, and will make you scream in " ..
                          "rapture.[BR]These obelisks are resiliant to most forms of physical damage, but[BR]a few solid strikes from a hammer or mace will " ..
                          "make them crumble.[BR]Commonly Found: St. Fora's Convent")

zEntry = JournalEntry:new("Mushthrall", "Mushthrall")
zEntry:fnSetToChartEntry ("Mushthrall", gbDoNotAlwaysShow, 755, 159, gbCannotTFPlayer)
zEntry:fnSetDescription  ("An alraune that has fungus growing all over her. Her normally leafy[BR]appearance is greatly decayed.[BR][BR]She seems a lot more horny than usual. Keep your distance.[BR]"..
                          "Considerably tougher than the mushraunes, but otherwise the same.[BR][BR]Commonly Found: St. Fora's Convent")

-- |[ ============== Finish Up =============== ]|
-- |[Assemble List]|
for i = 1, #gzaCh1BestiaryEntries, 1 do
    gzaCh1BestiaryEntries[i]:fnUploadData()
end

-- |[Clean]|
--Unsets globals.
JournalEntry:fnCleanGlobals()