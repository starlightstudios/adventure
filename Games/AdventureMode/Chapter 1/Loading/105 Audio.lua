-- |[ ===================================== Audio Loading ====================================== ]|
--Loads audio for this chapter.
local sBasePath = gsRoot .. "Audio/Music/"

-- |[Ambient Tracks]|
local sAmbientPath = sBasePath .. "Ambient Tracks/"
AudioManager_RegisterAdv("Cave Quiet",      sAmbientPath .. "Cave Quiet.ogg",      "Stream|Music|Loop:" ..  "3.000:" .. "120.743")
AudioManager_RegisterAdv("Mausoleum",       sAmbientPath .. "Mausoleum.ogg",       "Stream|Music|Loop:" ..  "1.000:" ..  "64.969")
AudioManager_RegisterAdv("Waves",           sAmbientPath .. "Waves.ogg",           "Stream|Music|Loop:" ..  "0.150:" ..  "42.262")

-- |[Battle Themes]|
local sBattlePath = sBasePath .. "Battle Themes/"
AudioManager_RegisterAdv("BattleThemeMei",       sBattlePath .. "Battle Theme Mei.ogg",      "Stream|Music|Loop:" ..  "5.658:" .. "102.088")
AudioManager_RegisterAdv("RottenTheme",          sBattlePath .. "ThemeOfTheRotten.ogg",      "Stream|Music|Loop:" ..  "2.000:" ..  "93.195")

-- |[Character Themes]|
local sCharThemePath = sBasePath .. "Character Themes/"
AudioManager_RegisterAdv("AdinasTheme",      sCharThemePath .. "AdinasTheme.ogg",             "Stream|Music|Loop:" ..  "2.000:" ..  "73.591")
AudioManager_RegisterAdv("BreannesTheme",    sCharThemePath .. "Breannes Theme.ogg",          "Stream|Music|Loop:" .. "13.735:" ..  "96.459")
AudioManager_RegisterAdv("FlorentinasTheme", sCharThemePath .. "Florentinas Theme.ogg",       "Stream|Music|Loop:" .. "15.557:" ..  "79.941")
AudioManager_RegisterAdv("NadiasTheme",      sCharThemePath .. "Nadias Theme.ogg",            "Stream|Music|Loop:" ..  "0.000:" ..  "39.746")

-- |[Layered Tracks]|
local sLayeredPath = sBasePath .. "Layered Tracks/"
AudioManager_RegisterAdv("CultistPassive", sLayeredPath .. "CultistPassive.ogg", "Stream|Music|Loop:" .. "0.000:" .. "113.430")
AudioManager_RegisterAdv("CultistTense",   sLayeredPath .. "CultistTense.ogg",   "Stream|Music|Loop:" .. "0.000:" .. "113.430")
AudioManager_RegisterAdv("CultistCombat",  sLayeredPath .. "CultistCombat.ogg",  "Stream|Music|Loop:" .. "0.000:" .. "113.430")

-- |[Misc Tracks]|
local sMiscPath = sBasePath .. "Misc Tracks/"
AudioManager_RegisterAdv("ABillionEyes",   sMiscPath .. "A Billion Eyes.ogg",   "Stream|Music|Loop:" ..   "0.000:" ..  "30.067")
AudioManager_RegisterAdv("ThemeOfCourage", sMiscPath .. "Theme of Courage.ogg", "Stream|Music|Loop:" ..   "3.734:" ..  "25.615")
AudioManager_RegisterAdv("TimeSensitive",  sMiscPath .. "TimeSensitive.ogg",    "Stream|Music|Loop:" .. "127.561:" .. "154.408")
AudioManager_RegisterAdv("BattleAmbience", sMiscPath .. "BattleAmbience.ogg",   "Stream|Music|Loop:" ..   "0.000:" ..  "90.000")
AudioManager_RegisterAdv("Gladiator",      sMiscPath .. "Gladiator.ogg",        "Stream|Music|Loop:" ..   "4.951:" ..  "90.015")
AudioManager_RegisterAdv("Special|War",    sMiscPath .. "Battle.ogg",           "Stream|Music")

-- |[World Themes]|
local sWorldPath = sBasePath .. "World Themes/"
AudioManager_RegisterAdv("Apprehension",         sWorldPath .. "Apprehension.ogg",           "Stream|Music|Loop:" ..  "0.264:" .. "116.143")
AudioManager_RegisterAdv("ForestTheme",          sWorldPath .. "Forest Theme.ogg",           "Stream|Music|Loop:" ..  "4.813:" .. "117.546")
AudioManager_RegisterAdv("HighlandsTheme",       sWorldPath .. "Highlands Theme.ogg",        "Stream|Music|Loop:" ..  "3.009:" ..  "61.009")
AudioManager_RegisterAdv("NixNedar",             sWorldPath .. "NixNedar.ogg",               "Stream|Music|Loop:" ..  "4.702:" ..  "97.694")
AudioManager_RegisterAdv("QuantirManseTheme",    sWorldPath .. "QuantirMansionTheme.ogg",    "Stream|Music|Loop:" .. "42.270:" .. "111.892")
AudioManager_RegisterAdv("QuantirManseThemeLow", sWorldPath .. "QuantirMansionThemeLow.ogg", "Stream|Music|Loop:" ..  "3.061:" ..  "73.285")
AudioManager_RegisterAdv("SerenityObservatory",  sWorldPath .. "SerenityObservatory.ogg",    "Stream|Music|Loop:" .. "14.983:" ..  "60.001")
AudioManager_RegisterAdv("StarfieldSwamp",       sWorldPath .. "StarfieldSwamp.ogg",         "Stream|Music|Loop:" .. "20.007:" ..  "95.299")
AudioManager_RegisterAdv("TheyKnowWeAreHere",    sWorldPath .. "They Know We Are Here.ogg",  "Stream|Music|Loop:" .. "12.983:" ..  "74.159")
AudioManager_RegisterAdv("TownTheme",            sWorldPath .. "Town Theme.ogg",             "Stream|Music|Loop:" ..  "6.404:" ..  "99.603")

-- |[Character Callouts]|
local sVoicePath = gsRoot .. "Audio/VoiceCallouts/"
LM_ExecuteScript(sVoicePath .. "Mei/Z Build Class.lua")
LM_ExecuteScript(sVoicePath .. "Florentina/Z Build Class.lua")
