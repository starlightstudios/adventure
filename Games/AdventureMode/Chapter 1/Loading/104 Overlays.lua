-- |[ =================================== Chapter 1 Overlays =================================== ]|
--All of this is done via a meta-autoloader.
local sEverythingList = "Chapter 1 Overlays"
fnCreateDelayedLoadList(sEverythingList)

--Execute autoload.
SLF_Open(gsaDatafilePaths.sChapter1MapAutoload)
fnExecAutoloadDelayed("AutoLoad|Adventure|Maps_Ch1_Immediate", sEverythingList)

--Order everything to enqueue loading.
fnLoadDelayedBitmapsFromList(sEverythingList, gciDelayedLoadLoadAtEndOfTick)
