-- |[ ==================================== Chapter 1 Scenes ==================================== ]|
--Scenes for chapter 1. This includes TF scenes and runestone flashes.

-- |[Streaming List]|
--Create an "Everything" list. All scene images are put in this list that aren't in another list,
-- and ordered to load immediately.
--Because scene images tend to be really big, this list *should* be empty in an optimized program.
local sEverythingList = "Chapter 1 Scenes"
fnCreateDelayedLoadList(sEverythingList)

-- |[ ====================================== Introduction ====================================== ]|
--Open, create.
SLF_Open(gsaDatafilePaths.sScnCh1Major)

--Cluster.
local sCluster = "Chapter 1 Introduction"
fnCreateDelayedLoadList(sCluster)

--Images.
DL_AddPath("Root/Images/Scenes/Ch1Introduction/")
fnExtractDelayedBitmapToList(sCluster, "Introduction", "Root/Images/Scenes/Ch1Introduction/Introduction")

-- |[ ==================================== Adina Sex Scene ===================================== ]|
--Cluster.
sCluster = "Chapter 1 Adina Sex Scene"
fnCreateDelayedLoadList(sCluster)

--Images.
DL_AddPath("Root/Images/Scenes/AdinaSexScene/")
fnExtractDelayedBitmapToList(sCluster, "AdinaSpecial", "Root/Images/Scenes/AdinaSexScene/AdinaSexScene")

-- |[ ================================== Rubber Meteor Scene =================================== ]|
--Cluster.
sCluster = "Chapter 1 Rubber Meteor"
fnCreateDelayedLoadList(sCluster)

--Images.
DL_AddPath("Root/Images/Scenes/RubberMeteor/")
fnExtractDelayedBitmapToList(sCluster, "RubberMeteor", "Root/Images/Scenes/RubberMeteor/RubberMeteor")

-- |[ =================================== Slime Art Gallery ==================================== ]|
--Examination images for the art gallery. Uses the same SLF file as the introduction.
local sSlimeImages = "Chapter 1 Slime Gallery"
fnCreateDelayedLoadList(sSlimeImages)

--Open, load.
DL_AddPath("Root/Images/Scenes/ArtGallery/")
fnExtractDelayedBitmapToList(sSlimeImages, "SlimeGallery|Goodiva",     "Root/Images/Scenes/ArtGallery/Goodiva")
fnExtractDelayedBitmapToList(sSlimeImages, "SlimeGallery|SlimaLisa",   "Root/Images/Scenes/ArtGallery/SlimaLisa")
fnExtractDelayedBitmapToList(sSlimeImages, "SlimeGallery|Washlimeton", "Root/Images/Scenes/ArtGallery/Washlimeton")

-- |[ =================================== Mannequin Bad End ==================================== ]|
--Cluster.
sCluster = "Chapter 1 Mannequin Bad End"
fnCreateDelayedLoadList(sCluster)

--Images.
DL_AddPath("Root/Images/Scenes/MannBadEnd/")
fnExtractDelayedBitmapToList(sCluster, "MannBadEnd0", "Root/Images/Scenes/MannBadEnd/MannBadEnd0")
fnExtractDelayedBitmapToList(sCluster, "MannBadEnd1", "Root/Images/Scenes/MannBadEnd/MannBadEnd1")

-- |[ ============================== Cassandra's Transformations =============================== ]|
--She shows up a lot. Suspicious, isn't it?
local sCassandraImages = "Chapter 1 Cassandra"
fnCreateDelayedLoadList(sCassandraImages)

--Open, load.
SLF_Open(gsaDatafilePaths.sScnCassandraTF)
DL_AddPath("Root/Images/Scenes/Cassandra/")

--Human
fnExtractDelayedBitmapToList(sCassandraImages, "Cassandra|Neutral",    "Root/Images/Scenes/Cassandra/Neutral")
fnExtractDelayedBitmapToList(sCassandraImages, "Cassandra|WerecatTF0", "Root/Images/Scenes/Cassandra/WerecatTF0")
fnExtractDelayedBitmapToList(sCassandraImages, "Cassandra|WerecatTF1", "Root/Images/Scenes/Cassandra/WerecatTF1")
fnExtractDelayedBitmapToList(sCassandraImages, "Cassandra|Werecat",    "Root/Images/Scenes/Cassandra/Werecat")

-- |[ ================================== Mei's Transformations ================================= ]|
--Chapter 1's heroine. Each TF has its own image grouping.
SLF_Open(gsaDatafilePaths.sScnMeiTF)
DL_AddPath("Root/Images/Scenes/Mei/")

--Alraune
local sTFCluster = "Chapter 1 Mei Alraune TF"
fnCreateDelayedLoadList(sTFCluster)
fnExtractDelayedBitmapToList(sTFCluster, "Mei|AlrauneTF0", "Root/Images/Scenes/Mei/AlrauneTF0")
fnExtractDelayedBitmapToList(sTFCluster, "Mei|AlrauneTF1", "Root/Images/Scenes/Mei/AlrauneTF1")

--Bee
sTFCluster = "Chapter 1 Mei Bee TF"
fnCreateDelayedLoadList(sTFCluster)
fnExtractDelayedBitmapToList(sTFCluster, "Mei|BeeTF0",     "Root/Images/Scenes/Mei/BeeTF0")
fnExtractDelayedBitmapToList(sTFCluster, "Mei|BeeTF1",     "Root/Images/Scenes/Mei/BeeTF1")
fnExtractDelayedBitmapToList(sTFCluster, "Mei|BeeTF2",     "Root/Images/Scenes/Mei/BeeTF2")

--Ghost
sTFCluster = "Chapter 1 Mei Ghost TF"
fnCreateDelayedLoadList(sTFCluster)
fnExtractDelayedBitmapToList(sTFCluster, "Mei|GhostTF0",   "Root/Images/Scenes/Mei/GhostTF0")
fnExtractDelayedBitmapToList(sTFCluster, "Mei|GhostTF1",   "Root/Images/Scenes/Mei/GhostTF1")

--Gravemarker
sTFCluster = "Chapter 1 Mei Gravemarker TF"
fnCreateDelayedLoadList(sTFCluster)
fnExtractDelayedBitmapToList(sTFCluster, "Mei|GravemarkerTF0",   "Root/Images/Scenes/Mei/GravemarkerTF0")
fnExtractDelayedBitmapToList(sTFCluster, "Mei|GravemarkerTF1",   "Root/Images/Scenes/Mei/GravemarkerTF1")
fnExtractDelayedBitmapToList(sTFCluster, "Mei|GravemarkerTF2",   "Root/Images/Scenes/Mei/GravemarkerTF2")
fnExtractDelayedBitmapToList(sTFCluster, "Mei|GravemarkerTF3",   "Root/Images/Scenes/Mei/GravemarkerTF3")
fnExtractDelayedBitmapToList(sTFCluster, "Mei|GravemarkerTF4",   "Root/Images/Scenes/Mei/GravemarkerTF4")

--Slime
sTFCluster = "Chapter 1 Mei Slime TF"
fnCreateDelayedLoadList(sTFCluster)
fnExtractDelayedBitmapToList(sTFCluster, "Mei|SlimeTF0",   "Root/Images/Scenes/Mei/SlimeTF0")
fnExtractDelayedBitmapToList(sTFCluster, "Mei|SlimeTF1",   "Root/Images/Scenes/Mei/SlimeTF1")
fnExtractDelayedBitmapToList(sTFCluster, "Mei|SlimeTF2",   "Root/Images/Scenes/Mei/SlimeTF2")

--Werecat
sTFCluster = "Chapter 1 Mei Werecat TF"
fnCreateDelayedLoadList(sTFCluster)
fnExtractDelayedBitmapToList(sTFCluster, "Mei|WerecatTF0", "Root/Images/Scenes/Mei/WerecatTF0")
fnExtractDelayedBitmapToList(sTFCluster, "Mei|WerecatTF1", "Root/Images/Scenes/Mei/WerecatTF1")

--Wisphag.
sTFCluster = "Chapter 1 Mei Wisphag TF"
fnCreateDelayedLoadList(sTFCluster)
fnExecAutoloadDelayed("AutoLoad|Adventure|Mei_Wisphag_TF", sTFCluster)

--Zombee.
sTFCluster = "Chapter 1 Mei Zombee TF"
fnCreateDelayedLoadList(sTFCluster)
fnExecAutoloadDelayed("AutoLoad|Adventure|Mei_Zombee_TF", sTFCluster)

--Mei's Rune overlay. 60 frames. Gets a TF cluster, but can appear in more than one TF.
sTFCluster = "Chapter 1 Mei Runestone"
fnCreateDelayedLoadList(sTFCluster)
SLF_Open(gsaDatafilePaths.sScnMeiRune)
for i = 0, 59, 1 do
	local sName = string.format("Mei|Rune%02i", i)
	local sPath = string.format("Root/Images/Scenes/Mei/RuneAnim%02i", i)
	fnExtractDelayedBitmapToList(sTFCluster, sName, sPath)
end

--Still image of Mei's rune is the last one in the animation sequence.
fnExtractDelayedBitmapToList(sTFCluster, "Mei|Rune59", "Root/Images/Scenes/Mei/Rune")

-- |[ ======================================== Clean Up ======================================== ]|
-- |[Load Issue]|
--Order everything to enqueue loading.
fnLoadDelayedBitmapsFromList(sEverythingList, gciDelayedLoadLoadAtEndOfTick)

-- |[Debug]|
if(false) then
    fnPrintDelayedBitmapList(sEverythingList)
end

-- |[Close]|
SLF_Close()
