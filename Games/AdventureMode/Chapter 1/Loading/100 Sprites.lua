-- |[ =================================== Chapter 1 Loading ==================================== ]|
--This file loads assets unique to chapter 1. Subfiles are used where necessary.

-- |[Common]|
--Open the datafile.
SLF_Open(gsDatafilesPath .. "Sprites.slf")

--Modify the distance filters to keep everything pixellated. This is only for sprites.
local iNearest = DM_GetEnumeration("GL_NEAREST")
ALB_SetTextureProperty("MagFilter", iNearest)
ALB_SetTextureProperty("MinFilter", iNearest)
ALB_SetTextureProperty("Special Sprite Padding", true)

-- |[ ======================================= Autoloader ======================================= ]|
--Execute chapter 1's autoloading list.
SLF_Open(gsaDatafilePaths.sChapter1SpriteAutoload)
fnExecAutoload("AutoLoad|Adventure|Sprites_Ch1")

-- |[ ===================================== Loading Lists ====================================== ]|
--Lists of standardized NPC load patterns.
DL_AddPath("Root/Images/Sprites/Special/")

-- |[ =========== Party Characters =========== ]|
--[=[
--Mei loads her many transformations. These all have Crouch and Wounded frames.
local saMeiSpriteList = {"Human", "Alraune",   "Bee",     "BeeQueen",    "Ghost",    "Gravemarker",    "Slime",    "Werecat",    "Rubber", "Alraune_MC", "Bee_MC", "Human_MC"}
local saMeiCWList     = {"Mei",   "MeiAlraune", "MeiBee", "MeiBeeQueen", "MeiGhost", "MeiGravemarker", "MeiSlime", "MeiWerecat", "MeiRubber"}
fnExtractSpriteList   (saMeiSpriteList, true, "Mei_")
fnExtractSpecialFrames(saMeiCWList, "Crouch", "Wounded")

--Some of Mei's crouch frames are named differently.
fnExtractSpecialFrames({"MeiBeeMC", "MeiMC"}, "Crouch")

--Mei's Idle Animation
DL_AddPath("Root/Images/Sprites/IdleAnim/")
for i = 0, 10-1, 1 do
    DL_ExtractBitmap("IdleAnim|Mei_Alraune|" .. i, "Root/Images/Sprites/IdleAnim/Mei_Alraune|" .. i)
end
for i = 0, 11-1, 1 do
    DL_ExtractBitmap("IdleAnim|Mei_Human|" .. i, "Root/Images/Sprites/IdleAnim/Mei_Human|" .. i)
end
for i = 0, 9-1, 1 do
    DL_ExtractBitmap("IdleAnim|Mei_Slime|" .. i, "Root/Images/Sprites/IdleAnim/Mei_Slime|" .. i)
end
for i = 0, 4-1, 1 do
    DL_ExtractBitmap("IdleAnim|Mei_Bee_MC|"      .. i, "Root/Images/Sprites/IdleAnim/Mei_Bee_MC|"      .. i)
    DL_ExtractBitmap("IdleAnim|Mei_Gravemarker|" .. i, "Root/Images/Sprites/IdleAnim/Mei_Gravemarker|" .. i)
end
for i = 0, 5-1, 1 do
    DL_ExtractBitmap("IdleAnim|Mei_Bee|" .. i, "Root/Images/Sprites/IdleAnim/Mei_Bee|" .. i)
end
for i = 0, 8-1, 1 do
    DL_ExtractBitmap("IdleAnim|Mei_BeeQueen|" .. i, "Root/Images/Sprites/IdleAnim/Mei_BeeQueen|" .. i)
    DL_ExtractBitmap("IdleAnim|Mei_Ghost|"    .. i, "Root/Images/Sprites/IdleAnim/Mei_Ghost|"    .. i)
end
for i = 0, 7-1, 1 do
    DL_ExtractBitmap("IdleAnim|Mei_Werecat|" .. i, "Root/Images/Sprites/IdleAnim/Mei_Werecat|" .. i)
end

]=]

--Florentina loads her classes. These all have Crouch and Wounded frames.
local saFlorentinaSpriteList = {"Florentina", "FlorentinaTH", "FlorentinaMed"}
fnExtractSpriteList   (saFlorentinaSpriteList, true)
fnExtractSpecialFrames(saFlorentinaSpriteList, "Crouch", "Wounded")
fnExtractSpecialFrames(saFlorentinaSpriteList, "Drink0", "Drink1")
fnExtractSpecialFrames(saFlorentinaSpriteList, "Drink2")

--Agarist and Lurker don't have the drinking animations.
saFlorentinaSpriteList = {"FlorentinaAgarist", "FlorentinaLurker"}
fnExtractSpriteList   (saFlorentinaSpriteList, true)
fnExtractSpecialFrames(saFlorentinaSpriteList, "Crouch", "Wounded")

--Other special frames for Florentina.
DL_ExtractBitmap("Spcl|Florentina|Sleep", "Root/Images/Sprites/Special/Florentina|Sleep")

--Florentina's Idle Animations
for i = 0, 15-1, 1 do
    DL_ExtractBitmap("IdleAnim|Florentina|" .. i, "Root/Images/Sprites/IdleAnim/Florentina|" .. i)
end
for i = 0, 9-1, 1 do
    DL_ExtractBitmap("IdleAnim|FlorentinaMed|" .. i, "Root/Images/Sprites/IdleAnim/FlorentinaMed|" .. i)
end
for i = 0, 11-1, 1 do
    DL_ExtractBitmap("IdleAnim|FlorentinaTH|" .. i, "Root/Images/Sprites/IdleAnim/FlorentinaTH|" .. i)
end
for i = 0, 3-1, 1 do
    DL_ExtractBitmap("IdleAnim|FlorentinaAgarist|" .. i, "Root/Images/Sprites/IdleAnim/FlorentinaAgarist|" .. i)
end
for i = 0, 3-1, 1 do
    DL_ExtractBitmap("IdleAnim|FlorentinaLurker|" .. i, "Root/Images/Sprites/IdleAnim/FlorentinaLurker|" .. i)
end

-- |[ =========== Named Characters =========== ]|
-- |[Four Direction Characters]|
--Four-direction major characters.
local saMajorNPCSpriteList = {"Adina", "Blythe", "CassandraH", "CassandraW", "Claudia", "Jackie", "Miho", "Nadia", "Polaris", "Rochea", "Sammie", "Sharelock", "Lydie"}
fnExtractSpriteList(saMajorNPCSpriteList, false)

--Rubber versions of major characters.
local saMajorRubberSpriteList = {"AdinaR", "MihoR", "PolarisR", "RocheaR"}
fnExtractSpriteList(saMajorRubberSpriteList, false)

--Wounded frames for four-direction characters.
fnExtractSpecialFrames({"CassandraH", "CassandraW"}, "Wounded")

--Other special frames.
DL_ExtractBitmap("Spcl|Nadia|Sleep",      "Root/Images/Sprites/Special/Nadia|Sleep")
DL_ExtractBitmap("Spcl|Miho|Wounded",     "Root/Images/Sprites/Special/Miho|Wounded")
DL_ExtractBitmap("Spcl|Miho|Meditate",    "Root/Images/Sprites/Special/Miho|Meditate")
DL_ExtractBitmap("Spcl|Mycela|MushKneel", "Root/Images/Sprites/Special/Mycela|MushKneel")
DL_ExtractBitmap("Spcl|Mycela|Wounded",   "Root/Images/Sprites/Special/Mycela|Wounded")

-- |[Eight Direction Characters]|
--Eight-direction major characters.
local saOtherEightSpriteList = {"Aquillia", "AquilliaNoJacket", "Breanne", "Septima"}
fnExtractSpriteList(saOtherEightSpriteList, true)

--Wounded frames for eight-direction characters.
fnExtractSpecialFrames({"Aquillia"}, "Wounded")

-- |[ ============= Generic NPCs ============= ]|
--"NPC" lists. Entities that have 4-directional movement and a "Wounded" frame, but are meant to be used for town characters.
local saGenericNPCSpriteList = {"Corgi", "GenericF0", "GenericF1", "GenericF2", "GenericF3", "GenericF4", "GenericM0", "GenericM1", "GenericM2", "GenericM3", "GenericM4", "MercF", "MercM"}
fnExtractSpriteList   (saGenericNPCSpriteList, false)
fnExtractSpecialFrames(saGenericNPCSpriteList, "Wounded")

--Special Frames for NPCs
local saDrinkList = {"GenericF0", "GenericF1", "GenericF3", "MercM", "Werecat", "Miho"}
fnExtractSpecialFrames(saDrinkList, "Drink0", "Drink1")
fnExtractSpecialFrames(saDrinkList, "Drink2")

-- |[ ================ Enemies =============== ]|
--"Enemy" Lists. This is a list of entities that have 4-directional movement and a "Wounded" frame. Any sprite following this
-- format can be used for an enemy.
local saChapter0EnemyList = {"BestFriend", "CultistF", "CultistM", "MirrorImage", "SkullCrawler", "Wisphag"}
local saChapter1EnemyList = {"Alraune", "BeeBuddy", "BeeGirl", "BeeGirlZ", "BloodyMirror", "CandleHaunt", "EyeDrawer", "Gravemarker", "InkSlime", "Jelli", "MaidGhost", "Mushraune", "Rilmani", "Slime", "SlimeB", "SlimeG", "Sproutling", "Sproutpecker", "Werecat", "WerecatThief", "AlrauneR", "WerecatR", "BeeGirlR", "WerecatThiefR"}
fnExtractSpriteList   (saChapter0EnemyList, false)
fnExtractSpecialFrames(saChapter0EnemyList, "Wounded")
fnExtractSpriteList   (saChapter1EnemyList, false)
fnExtractSpecialFrames(saChapter1EnemyList, "Wounded")

--Alraune also has a crouch frame for cutscenes.
fnExtractSpecialFrames({"Alraune"}, "Crouch")

--Paragons. These are alt-color versions of an enemy that are tougher. These just have "Paragon" tacked on the end.
local saParagon0List = {}
local saParagon1List = {}
for i = 1, #saChapter0EnemyList, 1 do
    saParagon0List[i] = saChapter0EnemyList[i] .. "Paragon"
end
for i = 1, #saChapter1EnemyList, 1 do
    saParagon1List[i] = saChapter1EnemyList[i] .. "Paragon"
end
fnExtractSpriteList   (saParagon0List, false)
fnExtractSpecialFrames(saParagon0List, "Wounded")
fnExtractSpriteList   (saParagon1List, false)
fnExtractSpecialFrames(saParagon1List, "Wounded")

--Werecat special frame.
fnExtractSpecialFrames({"Werecat"}, "Slump")

-- |[ ============ Half-Frame NPCs =========== ]|
--Half-Frame Lists. Used for some NPCs with two-directional movement, like sheep and chickens.
local saHalfFrameList = {"Sheep", "Chicken"}
fnExtractSpriteListEW(saHalfFrameList)

-- |[ ================================= Misc Character Sprites ================================= ]|
-- |[Crowbar Chan]|
--Second last!? But I'm way cuter than Jeanne!
DL_AddPath("Root/Images/Sprites/CrowbarChan/")
DL_ExtractBitmap("CrowbarChan|0", "Root/Images/Sprites/CrowbarChan/0")
DL_ExtractBitmap("CrowbarChan|1", "Root/Images/Sprites/CrowbarChan/1")
DL_ExtractBitmap("CrowbarChan|2", "Root/Images/Sprites/CrowbarChan/2")

-- |[ ======================================== Clean Up ======================================== ]|
--Reset flags.
--Bitmap_DeactivateAtlasing()
ALB_SetTextureProperty("Restore Defaults")
ALB_SetTextureProperty("Special Sprite Padding", false)

--Close the slf file.
SLF_Close()
