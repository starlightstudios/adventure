-- |[ ====================================== Actor Setup ======================================= ]|
-- |[Function Reference]|
--Helper functions are found in Games/AdventureMode/Classes/DiaHelper/
-- Function lines are provided here as reference. See Games/AdventureMode/Classes/DiaHelper/DiaHelperActor.lua
-- for more on how these work.
--DiaHelper:fnActorSetName(psName)
--DiaHelper:fnActorSetVoice(psVoice)
--DiaHelper:fnActorAddAlias(psAlias, psDisplayName)
--DiaHelper:fnActorSetAliases(psaAliasList, psaDisplayList)
--DiaHelper:fnActorAddEmotion(psEmotion, psImagePath, pbIsNormalWidth)
--DiaHelper:fnActorSetEmotions(pzaEmotionList)
--DiaHelper:fnActorUploadAndClear()
--DiaHelper:fnCreateSimpleActor(psActorName, psVoiceName, psPathPattern, pbIsNormalWidth, psaAliasList, psaEmotionList, psaAliasRemapList)

--Note: The first alias for a character should always be that character's name. This is for translations,
-- as aliases can have display names that allow the translator to change a character's name in the dialogue.
-- Even if a character has no other aliases, they should always have their name in the list.
--The exception to this rule is if the character never speaks under their internal name, which some of the
-- generic or monster NPCs do.

-- |[Common Variables]|
--Human-readable versions of special name sets.
local saNeutralIsPath       = {"NEUTRALISPATH"}		--psPathPattern will be the direct path to the emotion image, and the emotion will be "Neutral".
local saNoAliasDisplayNames = nil 					--Alias display names are typically only used for translations.

-- |[ ================================= Party/Important Actors ================================= ]|
--Party members are often subject to costumes, meaning while their emotions and aliases are typically
-- the same for the entire game duration, their portraits will change periodically.

-- |[Mei]|
--Mei! The star of the show.
local saMeiAliases         = {"Mei", "Drone", "Natalie", "Thrall"}
local saMeiEmotions        = {"Neutral", "Smirk", "Happy", "Blush", "Sad", "Surprise", "Offended", "Cry", "Laugh", "Angry", "MC"}
DiaHelper:fnCreateSimpleActor("Mei", "Voice|Mei", "Root/Images/Portraits/MeiDialogue/", gbNormalWidth, saMeiAliases,  saMeiEmotions, saNoAliasDisplayNames)

-- |[Florentina]|
--Florentina. Technically optional but can you live without her?
local saFlorentinaAliases  = {"Florentina", "Houseplant"}
local saFlorentinaEmotions = {"Neutral", "Happy", "Blush", "Facepalm", "Surprise", "Offended", "Confused"}
DiaHelper:fnCreateSimpleActor("Florentina", "Voice|Florentina", "Root/Images/Portraits/FlorentinaMerchantDialogue/", gbNormalWidth, saFlorentinaAliases, saFlorentinaEmotions, saNoAliasDisplayNames)

-- |[Crowbar-Chan]|
--Crowbar Chan. I don't care if she technically belongs in another category SHE GOES HERE.
DiaHelper:fnCreateSimpleActor("CrowbarChan", "Voice|CrowbarChan", "Root/Images/Portraits/CrowbarChan/", gbNormalWidth, {"CrowbarChan"}, {"Neutral"}, saNoAliasDisplayNames)

-- |[Aquillia]|
--Aquillia. Gets a cameo here, but she's playable in Chapter 3. Requires manual construction as she borrows the cultist talk sprite.
DiaHelper:fnActorSetName("Aquillia")
DiaHelper:fnActorSetVoice("Voice|Aquillia")
DiaHelper:fnActorAddAlias("Aquillia", nil)
DiaHelper:fnActorAddAlias("Prisoner", nil)
DiaHelper:fnActorAddAlias("Cultist",  nil)
DiaHelper:fnActorAddAlias("Lady",     nil)
DiaHelper:fnActorAddEmotion("Neutral", "Root/Images/Portraits/AquilliaDialogue/HumanNeutral", gbNormalWidth)
DiaHelper:fnActorAddEmotion("Cast",    "Root/Images/Portraits/AquilliaDialogue/HumanCast",    gbNormalWidth)
DiaHelper:fnActorAddEmotion("Cultist", "Root/Images/Portraits/Enemies/CultistF",              gbIsWide)
DiaHelper:fnActorUploadAndClear()

-- |[ ====================================== Major Actors ====================================== ]|
--Refers to actors who are important enough to usually warrant their own sprite, but not a full emotion set.
-- May also refer to actors who play a big role but only in one section of the game.

-- |[Many-Chapter]|
--Characters who appear in multiple chapters, and thus may have extra portraits never used in this chapter.
local saCassandraAliases  = {"Cassandra", "Lady", "Fang"}
local saCassandraEmotions = {"Neutral", "Werecat", "Golem"}
local saMihoEmotions      = {"Neutral", "Angry", "Drunk", "VeryDrunk", "Rubber"}
local saSharelockAlias    = {"Sharelock", "Share;Lock", "Robot"}
local saSharelockEmotions = {"Neutral", "Thinking", "AhHa", "Sheepish"}
DiaHelper:fnCreateSimpleActor("Cassandra", "Voice|GenericF13", "Root/Images/Portraits/Cassandra/", gbNormalWidth, saCassandraAliases,  saCassandraEmotions, saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("Miho",      "Voice|Miho",       "Root/Images/Portraits/Miho/",      gbNormalWidth, {"Miho", "Kitsune"}, saMihoEmotions,      saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("Sharelock", "Voice|Sharelock",  "Root/Images/Portraits/Sharelock/", gbNormalWidth, saSharelockAlias,    saSharelockEmotions, saNoAliasDisplayNames)

-- |[Chapter 1]|
--Character aliases/emotions, used to shorten lines.
local saPolarisEmotions   = {"Neutral", "Thinking", "Upset", "Rubber"}
local saMarriedAlias      = {"Marriedraunes", "Jackie", "Sammie"}
local saMarriedEmotions   = {"JNeutral", "JClimb", "JGrab", "SNeutral", "SClimb", "SGrab"}
local saVictoriaEmotions  = {"Neutral", "Apology", "Evil", "Disgust", "Concern", "Disbelief", "Mannequin"}
local saXannaEmotions     = {"Neutral", "Upset", "BeeNeutral", "BeeUpset"}

--DiaHelper:fnCreateSimpleActor(psActorName, psVoiceName, psPathPattern, pbIsNormalWidth, psaAliasList, psaEmotionList, psaAliasRemapList)
DiaHelper:fnCreateSimpleActor("Adina",         "Voice|Alraune",       "Root/Images/Portraits/Adina/",          gbNormalWidth, {"Adina", "Alraune"},  {"Neutral", "Rubber"},           saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("Blythe",        "Voice|Blythe",        "Root/Images/Portraits/Blythe/Neutral",  gbNormalWidth, {"Blythe", "Man"},     saNeutralIsPath,                 saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("Breanne",       "Voice|Breanne",       "Root/Images/Portraits/Breanne/Neutral", gbNormalWidth, {"Breanne"},           saNeutralIsPath,                 saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("Claudia",       "Voice|Nadia",         "Root/Images/Portraits/Claudia/Neutral", gbNormalWidth, {"Claudia"},           saNeutralIsPath,                 saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("Denise",        "Voice|Ghost",         "Root/Images/Portraits/Denise/",         gbNormalWidth, {"Denise"},            {"Neutral", "Smirk", "Insight"}, saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("Karina",        "Voice|GenericF03",    "Root/Images/Portraits/Karina/Neutral",  gbNormalWidth, {"Karina", "Lady"},    saNeutralIsPath,                 saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("Kona",          "Voice|Alraune",       "Root/Images/Portraits/Kona/",           gbNormalWidth, {"Kona"},              {"Neutral", "Happy", "Wink"},    saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("Marriedraunes", "Voice|MarriedJackie", "Root/Images/Portraits/Marriedraunes/",  gbNormalWidth, saMarriedAlias,        saMarriedEmotions,               saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("MarriedJackie", "Voice|MarriedJackie", "Root/Images/Portraits/Marriedraunes/",  gbNormalWidth, {"MarriedJackie"},     {"JNeutral"},                    saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("MarriedSammie", "Voice|MarriedSammie", "Root/Images/Portraits/Marriedraunes/",  gbNormalWidth, {"MarriedSammie"},     {"SNeutral"},                    saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("Mycela",        "Voice|Mycela",        "Root/Images/Portraits/Mycela/",         gbNormalWidth, {"Mycela", "Alraune"}, {"Neutral", "Demushed"},         saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("Nadia",         "Voice|Nadia",         "Root/Images/Portraits/Nadia/",          gbNormalWidth, {"Nadia", "Alraune"},  {"Neutral", "Rubber"},           saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("Polaris",       "Voice|Polaris",       "Root/Images/Portraits/Polaris/",        gbNormalWidth, {"Polaris", "Witch"},  saPolarisEmotions,               saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("Rochea",        "Voice|Alraune",       "Root/Images/Portraits/Rochea/",         gbNormalWidth, {"Rochea", "Alraune"}, {"Neutral", "Rubber"},           saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("Victoria",      "Voice|Victoria",      "Root/Images/Portraits/Victoria/",       gbNormalWidth, {"Victoria"},          saVictoriaEmotions,              saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("Xanna",         "Voice|Xanna",         "Root/Images/Portraits/Xanna/",          gbNormalWidth, {"Xanna", "Cultist"},  saXannaEmotions,                 saNoAliasDisplayNames)

-- |[ ====================================== Minor Actors ====================================== ]|
--NPCs or Enemy actors, usually play small roles many times. Some have unique dialogue sprites because their enemy field
-- variants are too big to fit on the dialogue screen.

-- |[Named Characters]|
--"Named" versions, these are characters but they use generic portraits.
DiaHelper:fnCreateSimpleActor("Hypatia",   "Voice|GenericF02", "Root/Images/Portraits/NPCs/HumanNPCF0", gbNormalWidth, {"Hypatia"},          saNeutralIsPath, saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("Alicia",    "Voice|GenericF04", "Root/Images/Portraits/NPCs/HumanNPCF0", gbNormalWidth, {"Alicia", "Lady"},   saNeutralIsPath, saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("Ginny",     "Voice|Werecat",    "Root/Images/Portraits/Enemies/Werecat", gbIsWide,      {"Ginny", "Werecat"}, saNeutralIsPath, saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("Jean",      "Voice|Werecat",    "Root/Images/Portraits/Enemies/Werecat", gbIsWide,      {"Jean", "Werecat"},  saNeutralIsPath, saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("Bartender", "Voice|GenericF02", "Root/Images/Portraits/NPCs/HumanNPCF1", gbNormalWidth, {"Bartender"},        saNeutralIsPath, saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("Laura",     "Voice|Ghost",      "Root/Images/Portraits/Enemies/Laura",   gbIsWide,      {"Laura"},            saNeutralIsPath, saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("Lydie",     "Voice|Ghost",      "Root/Images/Portraits/Enemies/Lydie",   gbIsWide,      {"Lydia"},            saNeutralIsPath, saNoAliasDisplayNames)

-- |[Unnamed Characters]|
--"Unnamed" versions. Not characters, just stand-ins for a monster/enemy type.
local saCultistFAliases = {"CultistF", "Cultist", "Female Cultist"}
local saCultistMAliases = {"CultistM", "Cultist", "Male Cultist"}
local saAlrauneAliases = {"Alraune", "Dafoda", "Reika", "Sammie", "Agapa", "Stana", "Leader", "Thistle"}
DiaHelper:fnCreateSimpleActor("CultistF",     "Voice|GenericF00", "Root/Images/Portraits/Enemies/CultistF",     gbNormalWidth, saCultistFAliases,            saNeutralIsPath, saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("CultistM",     "Voice|GenericM00", "Root/Images/Portraits/Enemies/CultistM",     gbNormalWidth, saCultistMAliases,            saNeutralIsPath, saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("Alraune",      "Voice|Alraune",    "Root/Images/Portraits/Enemies/Alraune",      gbNormalWidth, saAlrauneAliases,             saNeutralIsPath, saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("Alraune2",     "Voice|Alraune",    "Root/Images/Portraits/Enemies/Alraune",      gbNormalWidth, {"Alraune2"},                 saNeutralIsPath, saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("MercF",        "Voice|GenericF11", "Root/Images/Portraits/NPCs/MercF",           gbNormalWidth, {"MercF", "Guard"},           saNeutralIsPath, saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("MercM",        "Voice|GenericM01", "Root/Images/Portraits/NPCs/MercM",           gbNormalWidth, {"MercM", "Guard", "Suitor"}, saNeutralIsPath, saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("Bee",          "Voice|Bee",        "Root/Images/Portraits/Enemies/Bee",          gbIsWide,      {"Bee", "Drone", "Joanie"},   saNeutralIsPath, saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("Ghost",        "Voice|Ghost",      "Root/Images/Portraits/Enemies/MaidGhost",    gbIsWide,      {"Ghost"},                    saNeutralIsPath, saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("Wisphag",      "Voice|Ghost",      "Root/Images/Portraits/Enemies/MaidGhost",    gbIsWide,      {"Wisphag"},                  saNeutralIsPath, saNoAliasDisplayNames) --Not in major dialogues.
DiaHelper:fnCreateSimpleActor("Werecat",      "Voice|Werecat",    "Root/Images/Portraits/Enemies/Werecat",      gbIsWide,      {"Werecat"},                  saNeutralIsPath, saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("BanditCatB",   "Voice|Werecat",    "Root/Images/Portraits/Enemies/BanditCatB",   gbIsWide,      {"Boss"},                     saNeutralIsPath, saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("WerecatThief", "Voice|Werecat",    "Root/Images/Portraits/Enemies/WerecatThief", gbIsWide,      {"Werecat"},                  saNeutralIsPath, saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("Zombee",       "Voice|Bee",        "Root/Images/Portraits/Enemies/Zombee",       gbIsWide,      {"Zombee"},                   saNeutralIsPath, saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("Mushraune",    "Voice|Alraune",    "Root/Images/Portraits/Enemies/Mushraune",    gbIsWide,      {"Mushraune"},                saNeutralIsPath, saNoAliasDisplayNames)

-- |[Rubber Characters]|
--Rubberified NPCs. There are two NPCs for the rubberable enemies since they may stack in combat. The NPCs never speak at the same time.
DiaHelper:fnCreateSimpleActor("RubberCultistF",      "Voice|GenericF00", "Root/Images/Portraits/Enemies/CultistFRubber",     gbNormalWidth, {"Cultist", "Rubber Cultist"}, saNeutralIsPath, saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("RubberAlraune",       "Voice|Alraune",    "Root/Images/Portraits/Enemies/AlrauneRubber",      gbIsWide,      {"Alraune", "Rubber Alraune"}, saNeutralIsPath, saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("RubberAlraune2",      "Voice|Alraune",    "Root/Images/Portraits/Enemies/AlrauneRubber",      gbIsWide,      {"Alraune", "Rubber Alraune"}, saNeutralIsPath, saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("RubberWerecat",       "Voice|Werecat",    "Root/Images/Portraits/Enemies/WerecatRubber",      gbIsWide,      {"Werecat", "Rubber Werecat"}, saNeutralIsPath, saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("RubberWerecat2",      "Voice|Werecat",    "Root/Images/Portraits/Enemies/WerecatRubber",      gbIsWide,      {"Werecat", "Rubber Werecat"}, saNeutralIsPath, saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("RubberWerecatThief",  "Voice|Werecat",    "Root/Images/Portraits/Enemies/WerecatThiefRubber", gbIsWide,      {"Werecat", "Rubber Werecat"}, saNeutralIsPath, saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("RubberWerecatThief2", "Voice|Werecat",    "Root/Images/Portraits/Enemies/WerecatThiefRubber", gbIsWide,      {"Werecat", "Rubber Werecat"}, saNeutralIsPath, saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("RubberBee",           "Voice|Bee",        "Root/Images/Portraits/Enemies/BeeRubber",          gbIsWide,      {"Bee",     "Rubber Bee"},     saNeutralIsPath, saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("RubberBee2",          "Voice|Bee",        "Root/Images/Portraits/Enemies/BeeRubber",          gbIsWide,      {"Bee",     "Rubber Bee"},     saNeutralIsPath, saNoAliasDisplayNames)

-- |[Slimes!]|
--Slimes exist in five color variants. Requires manual construction due to an unusual path structure.
DiaHelper:fnActorSetName("Slime")
DiaHelper:fnActorSetVoice("Voice|GenericF00")
DiaHelper:fnActorAddAlias("Slime", nil)
DiaHelper:fnActorAddAlias("Meryl", nil)
DiaHelper:fnActorAddEmotion("Neutral", "Root/Images/Portraits/Enemies/Slime",  gbNormalWidth)
DiaHelper:fnActorAddEmotion("Green",   "Root/Images/Portraits/Enemies/SlimeG", gbNormalWidth)
DiaHelper:fnActorAddEmotion("Blue",    "Root/Images/Portraits/Enemies/SlimeB", gbNormalWidth)
DiaHelper:fnActorAddEmotion("Inky",    "Root/Images/Portraits/Enemies/SlimeI", gbNormalWidth)
DiaHelper:fnActorAddEmotion("Purple",  "Root/Images/Portraits/Meryl/Neutral",  gbNormalWidth)
DiaHelper:fnActorUploadAndClear()

-- |[ ===================================== Generic Actors ===================================== ]|
--Actors from the common images not specific to any chapter. If they already exist, the call will do nothing.
DiaHelper:fnCreateSimpleActor("HumanF0",  "Voice|GenericF03", "Root/Images/Portraits/NPCs/HumanNPCF0",  true,  {"Human"},          saNeutralIsPath, saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("HumanF1",  "Voice|GenericF02", "Root/Images/Portraits/NPCs/HumanNPCF1",  true,  {"Human", "Norah"}, saNeutralIsPath, saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("HumanM0",  "Voice|GenericM01", "Root/Images/Portraits/NPCs/HumanNPCM0",  true,  {"Human"},          saNeutralIsPath, saNoAliasDisplayNames)
DiaHelper:fnCreateSimpleActor("HumanM1",  "Voice|GenericM00", "Root/Images/Portraits/NPCs/HumanNPCM1",  true,  {"Human"},          saNeutralIsPath, saNoAliasDisplayNames)
