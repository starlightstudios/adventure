-- |[ ================================== Chapter 1 Portraits =================================== ]|
--Loads all portraits for chapter 1.
Debug_PushPrint(gbLoadDebug, "Beginning Portrait loader.\n")

-- |[All Portraits List]|
--Create an "Everything" list. All delayed images not in another list get put in this list and loaded
-- as soon as possible, but not during the load sequence.
local sEverythingList = "Chapter 1 Portraits"
fnCreateDelayedLoadList(sEverythingList)

--List used for portraits that load if gbUnloadDialoguePortraitsWhenDialogueCloses is true, and don't when it's false.
local sCh1DialogueList = "Chapter 1 Dialogue"
fnCreateDelayedLoadList(sCh1DialogueList)

-- |[Dialogue Portraits Flag]|
--Setup. Allows suppression of streaming warnings if desired for some reason.
local bShowWarningsForDialoguePortraits = true
local bShowWarningsForCombatPortraits = true

-- |[Constants]|
local sMetaAutoload = gsaDatafilePaths.sChapter1PortraitAutoload

-- |[ ================================ Meta-Autoloader Function ================================ ]|
--Used for autoloader files that contain instructions to jump to other files. This re-loads the meta
-- file, then executes the autoload to the given costume. Basically compressed two lines into one.
local function fnMetaAutoload(psAutoloadSLF, psAutoloadLump, psCostumeDestination)
    SLF_Open(psAutoloadSLF)
    fnExecAutoloadDelayed(psAutoloadLump, psCostumeDestination)
end

-- |[ ======================================= Autoloader ======================================= ]|
--Order everything on these two portrait lists to load. One is the "load immediately", the other is
-- "load as needed". This distinction is done at compression time.
SLF_Open(gsaDatafilePaths.sChapter1PortraitAutoload)
fnExecAutoloadDelayed("AutoLoad|Adventure|Portrait_Ch1_Immediate", sEverythingList)

--Re-open the autoload file. The previous call may have changed which file is open.
SLF_Open(gsaDatafilePaths.sChapter1PortraitAutoload)
fnExecAutoloadDelayed("AutoLoad|Adventure|Portrait_Ch1_AsNeeded", sCh1DialogueList)

-- |[ =================================== Costume Autoloader =================================== ]|
--Characters who have changeable costumes register their images and then let the costume handlers load
-- and unload as needed. Most playable characters and some non-playable ones use this.

-- |[ ======== Mei ======= ]|
--Loading Lists.
local sCostumePrefix        = "Mei Costume "
local sCostumeCombatSuffix  = " Combat"
local sAutoloadEmotePrefix  = "AutoLoad|Adventure|Portrait_Ch1_Emt_Mei_"
local sAutoloadCombatPrefix = "AutoLoad|Adventure|Portrait_Ch1_Cbt_Mei_"
local saBaseListing = {"Alraune", "AlrauneMC", "Bee", "BeeMC", "BeeQueen", "BeeQueenMC", "Ghost", "Human", "HumanMC", "Slime", "SlimeBlue", "SlimeInk", "SlimePink", "SlimeYellow", "Mannequin", "Werecat", 
                       "Wisphag", "Gravemarker", "Rubber", "RubberQueen"}

--Global lists, used by costumes to load/unload before changing portraits.
gsaMeiDialogueList = {}
gsaMeiCombatList = {}

--Create a dialogue list and a combat list that stores all these entries. Create delayed load sets for each entry as well.
for i = 1, #saBaseListing, 1 do
    
    --Prefab variables.
    local sCostumeNameEmt = sCostumePrefix .. saBaseListing[i]                         --Ex: "Mei Costume Alraune"
    local sCostumeNameCbt = sCostumePrefix .. saBaseListing[i] .. sCostumeCombatSuffix --Ex: "Mei Costume Alraune Combat"
    
    --Add to global listing.
    table.insert(gsaMeiDialogueList, sCostumeNameEmt)
    table.insert(gsaMeiCombatList,   sCostumeNameCbt)

    --Delayed load lists.
    fnCreateDelayedLoadList(sCostumeNameEmt)
    fnCreateDelayedLoadList(sCostumeNameCbt)
    
    --Order the meta-autoload to take place.
    fnMetaAutoload(sMetaAutoload, sAutoloadEmotePrefix  .. saBaseListing[i], sCostumeNameEmt)
    fnMetaAutoload(sMetaAutoload, sAutoloadCombatPrefix .. saBaseListing[i], sCostumeNameCbt)
    
end

-- |[ ======== Florentina ======= ]|
--Loading Lists.
sCostumePrefix        = "Florentina Costume "
sCostumeCombatSuffix  = " Combat"
sAutoloadEmotePrefix  = "AutoLoad|Adventure|Portrait_Ch1_Emt_Florentina_"
sAutoloadCombatPrefix = "AutoLoad|Adventure|Portrait_Ch1_Cbt_Florentina_"
saBaseListing         = {"Agarist", "Lurker", "Mediator", "Merchant", "Treasure Hunter"}

--Global lists, used by costumes to load/unload before changing portraits.
gsaFlorentinaDialogueList = {}
gsaFlorentinaCombatList = {}

--Create a dialogue list and a combat list that stores all these entries. Create delayed load sets for each entry as well.
for i = 1, #saBaseListing, 1 do
    
    --Prefab variables.
    local sCostumeNameEmt = sCostumePrefix .. saBaseListing[i]                         --Ex: "Mei Costume Alraune"
    local sCostumeNameCbt = sCostumePrefix .. saBaseListing[i] .. sCostumeCombatSuffix --Ex: "Mei Costume Alraune Combat"
    
    --Add to global listing.
    table.insert(gsaFlorentinaDialogueList, sCostumeNameEmt)
    table.insert(gsaFlorentinaCombatList,   sCostumeNameCbt)

    --Delayed load lists.
    fnCreateDelayedLoadList(sCostumeNameEmt)
    fnCreateDelayedLoadList(sCostumeNameCbt)
    
    --Order the meta-autoload to take place.
    fnMetaAutoload(sMetaAutoload, sAutoloadEmotePrefix  .. saBaseListing[i], sCostumeNameEmt)
    fnMetaAutoload(sMetaAutoload, sAutoloadCombatPrefix .. saBaseListing[i], sCostumeNameCbt)
    
end

--Rubber emote. Only used in one spot.
fnExtractDelayedBitmapToList("Florentina Costume Merchant", "Por|FlorentinaMerchant|Rubber", "Root/Images/Portraits/FlorentinaDialogue/MerchantRubber")

--Countermask. Merchant, Mediator, and Treasure Hunter use the same countermask.
fnExtractDelayedBitmapToList("Florentina Costume Merchant Combat",        "Party|FlorentinaCountermask", "Root/Images/Portraits/Combat/FlorentinaCountermask")
fnAppendDelayedBitmapToList ("Florentina Costume Mediator Combat",        "Party|FlorentinaCountermask", "Root/Images/Portraits/Combat/FlorentinaCountermask")
fnAppendDelayedBitmapToList ("Florentina Costume Treasure Hunter Combat", "Party|FlorentinaCountermask", "Root/Images/Portraits/Combat/FlorentinaCountermask")

-- |[ ======================================= Florentina ======================================= ]|
--[=[


-- |[Costume List Creation]|
--Create costume lists for each of Florentina's costumes.
fnCreateDelayedLoadList("Florentina Costume Merchant")
fnCreateDelayedLoadList("Florentina Costume Mediator")
fnCreateDelayedLoadList("Florentina Costume Treasure Hunter")

--Combat version.
fnCreateDelayedLoadList("Florentina Costume Merchant Combat")
fnCreateDelayedLoadList("Florentina Costume Mediator Combat")
fnCreateDelayedLoadList("Florentina Costume Treasure Hunter Combat")

-- |[Store Lists]|
--Create a list of all of Florentina's costume sets.
gsaFlorentinaDialogueList = {}
gsaFlorentinaDialogueList[1] = "Florentina Costume Merchant"
gsaFlorentinaDialogueList[2] = "Florentina Costume Mediator"
gsaFlorentinaDialogueList[3] = "Florentina Costume Treasure Hunter"

--Combat version.
gsaFlorentinaCombatList = {}
gsaFlorentinaCombatList[1] = "Florentina Costume Merchant Combat"
gsaFlorentinaCombatList[2] = "Florentina Costume Mediator Combat"
gsaFlorentinaCombatList[3] = "Florentina Costume Treasure Hunter Combat"

-- |[Flag]|
--Set portraits flag off.
Bitmap_SetNewBitmapNoHandleWarnings(bShowWarningsForDialoguePortraits)

-- |[Florentina's  Dialogue Portraits]|
--Emote
SLF_Open(gsaDatafilePaths.sFlorentinaPath)
DL_AddPath("Root/Images/Portraits/FlorentinaMerchantDialogue/")
fnExtractDelayedBitmapToList("Florentina Costume Merchant", "Por|FlorentinaMerchant|Neutral",       "Root/Images/Portraits/FlorentinaMerchantDialogue/Neutral")
fnExtractDelayedBitmapToList("Florentina Costume Merchant", "Por|FlorentinaMerchant|Happy",         "Root/Images/Portraits/FlorentinaMerchantDialogue/Happy")
fnExtractDelayedBitmapToList("Florentina Costume Merchant", "Por|FlorentinaMerchant|Blush",         "Root/Images/Portraits/FlorentinaMerchantDialogue/Blush")
fnExtractDelayedBitmapToList("Florentina Costume Merchant", "Por|FlorentinaMerchant|Facepalm",      "Root/Images/Portraits/FlorentinaMerchantDialogue/Facepalm")
fnExtractDelayedBitmapToList("Florentina Costume Merchant", "Por|FlorentinaMerchant|Surprise",      "Root/Images/Portraits/FlorentinaMerchantDialogue/Surprise")
fnExtractDelayedBitmapToList("Florentina Costume Merchant", "Por|FlorentinaMerchant|Offended",      "Root/Images/Portraits/FlorentinaMerchantDialogue/Offended")
fnExtractDelayedBitmapToList("Florentina Costume Merchant", "Por|FlorentinaMerchant|Confused",      "Root/Images/Portraits/FlorentinaMerchantDialogue/Confused")
fnExtractDelayedBitmapToList("Florentina Costume Merchant", "Por|FlorentinaMerchantRubber|Neutral", "Root/Images/Portraits/FlorentinaMerchantDialogue/Rubber")

DL_AddPath("Root/Images/Portraits/FlorentinaMediatorDialogue/")
fnExtractDelayedBitmapToList("Florentina Costume Mediator", "Por|FlorentinaMediator|Neutral",  "Root/Images/Portraits/FlorentinaMediatorDialogue/Neutral")
fnExtractDelayedBitmapToList("Florentina Costume Mediator", "Por|FlorentinaMediator|Happy",    "Root/Images/Portraits/FlorentinaMediatorDialogue/Happy")
fnExtractDelayedBitmapToList("Florentina Costume Mediator", "Por|FlorentinaMediator|Blush",    "Root/Images/Portraits/FlorentinaMediatorDialogue/Blush")
fnExtractDelayedBitmapToList("Florentina Costume Mediator", "Por|FlorentinaMediator|Facepalm", "Root/Images/Portraits/FlorentinaMediatorDialogue/Facepalm")
fnExtractDelayedBitmapToList("Florentina Costume Mediator", "Por|FlorentinaMediator|Surprise", "Root/Images/Portraits/FlorentinaMediatorDialogue/Surprise")
fnExtractDelayedBitmapToList("Florentina Costume Mediator", "Por|FlorentinaMediator|Offended", "Root/Images/Portraits/FlorentinaMediatorDialogue/Offended")
fnExtractDelayedBitmapToList("Florentina Costume Mediator", "Por|FlorentinaMediator|Confused", "Root/Images/Portraits/FlorentinaMediatorDialogue/Confused")

DL_AddPath("Root/Images/Portraits/FlorentinaTreasureHunterDialogue/")
fnExtractDelayedBitmapToList("Florentina Costume Treasure Hunter", "Por|FlorentinaTreasureHunter|Neutral",  "Root/Images/Portraits/FlorentinaTreasureHunterDialogue/Neutral")
fnExtractDelayedBitmapToList("Florentina Costume Treasure Hunter", "Por|FlorentinaTreasureHunter|Happy",    "Root/Images/Portraits/FlorentinaTreasureHunterDialogue/Happy")
fnExtractDelayedBitmapToList("Florentina Costume Treasure Hunter", "Por|FlorentinaTreasureHunter|Blush",    "Root/Images/Portraits/FlorentinaTreasureHunterDialogue/Blush")
fnExtractDelayedBitmapToList("Florentina Costume Treasure Hunter", "Por|FlorentinaTreasureHunter|Facepalm", "Root/Images/Portraits/FlorentinaTreasureHunterDialogue/Facepalm")
fnExtractDelayedBitmapToList("Florentina Costume Treasure Hunter", "Por|FlorentinaTreasureHunter|Surprise", "Root/Images/Portraits/FlorentinaTreasureHunterDialogue/Surprise")
fnExtractDelayedBitmapToList("Florentina Costume Treasure Hunter", "Por|FlorentinaTreasureHunter|Offended", "Root/Images/Portraits/FlorentinaTreasureHunterDialogue/Offended")
fnExtractDelayedBitmapToList("Florentina Costume Treasure Hunter", "Por|FlorentinaTreasureHunter|Confused", "Root/Images/Portraits/FlorentinaTreasureHunterDialogue/Confused")

-- |[Combat Portraits]|
--Set warning flag on.
Bitmap_SetNewBitmapNoHandleWarnings(bShowWarningsForCombatPortraits)

--Combat
fnExtractDelayedBitmapToList("Florentina Costume Merchant Combat",        "Party|Florentina_Merchant",       "Root/Images/Portraits/Combat/Florentina_Merchant")
fnExtractDelayedBitmapToList("Florentina Costume Mediator Combat",        "Party|Florentina_Mediator",       "Root/Images/Portraits/Combat/Florentina_Mediator")
fnExtractDelayedBitmapToList("Florentina Costume Treasure Hunter Combat", "Party|Florentina_TreasureHunter", "Root/Images/Portraits/Combat/Florentina_TreasureHunter")

--Countermask. All costumes use the same countermask.
fnExtractDelayedBitmapToList("Florentina Costume Merchant Combat",        "Party|FlorentinaCountermask", "Root/Images/Portraits/Combat/FlorentinaCountermask")
fnAppendDelayedBitmapToList ("Florentina Costume Mediator Combat",        "Party|FlorentinaCountermask", "Root/Images/Portraits/Combat/FlorentinaCountermask")
fnAppendDelayedBitmapToList ("Florentina Costume Treasure Hunter Combat", "Party|FlorentinaCountermask", "Root/Images/Portraits/Combat/FlorentinaCountermask")

--The rubber emote from Merchant applies to all costume subsets.
fnAppendDelayedBitmapToList ("Florentina Costume Mediator Combat",        "Por|FlorentinaMerchantRubber|Neutral", "Root/Images/Portraits/FlorentinaMerchantDialogue/Rubber")
fnAppendDelayedBitmapToList ("Florentina Costume Treasure Hunter Combat", "Por|FlorentinaMerchantRubber|Neutral", "Root/Images/Portraits/FlorentinaMerchantDialogue/Rubber")

]=]

-- |[ ==================================== Other Characters ==================================== ]|
-- |[Flag]|
--Set warning flag off.
Bitmap_SetNewBitmapNoHandleWarnings(bShowWarningsForDialoguePortraits)

-- |[Aquillia's Portraits]|
SLF_Open(gsaDatafilePaths.sAquilliaPath)
DL_AddPath("Root/Images/Portraits/AquilliaDialogue/")
fnExtractDelayedBitmapToList(sCh1DialogueList, "Por|Aquillia|Neutral", "Root/Images/Portraits/AquilliaDialogue/HumanNeutral")
fnExtractDelayedBitmapToList(sCh1DialogueList, "Por|Aquillia|Cast",    "Root/Images/Portraits/AquilliaDialogue/HumanCast")

-- |[Crowbar Chan]|
SLF_Open(gsaDatafilePaths.sChapter0EmotePath)
DL_AddPath("Root/Images/Portraits/CrowbarChan/")
fnExtractDelayedBitmapToList(sCh1DialogueList, "Por|CrowbarChan|Neutral",  "Root/Images/Portraits/CrowbarChan/Neutral")
    
-- |[Minor Characters]|
SLF_Open(gsaDatafilePaths.sChapter1EmotePath)

--Adina
DL_AddPath("Root/Images/Portraits/Adina/")
fnExtractDelayedBitmapToList(sCh1DialogueList, "Por|Adina|Neutral",       "Root/Images/Portraits/Adina/Neutral")
fnExtractDelayedBitmapToList(sCh1DialogueList, "Por|AdinaRubber|Neutral", "Root/Images/Portraits/Adina/Rubber")

--Blythe
DL_AddPath("Root/Images/Portraits/Blythe/")
fnExtractDelayedBitmapToList(sCh1DialogueList, "Por|Blythe|Neutral", "Root/Images/Portraits/Blythe/Neutral")

--Breanne
DL_AddPath("Root/Images/Portraits/Breanne/")
fnExtractDelayedBitmapToList(sCh1DialogueList, "Por|Breanne|Neutral", "Root/Images/Portraits/Breanne/Neutral")

--Nadia
DL_AddPath("Root/Images/Portraits/Nadia/")
fnExtractDelayedBitmapToList(sCh1DialogueList, "Por|Nadia|Neutral",       "Root/Images/Portraits/Nadia/Neutral")
fnExtractDelayedBitmapToList(sCh1DialogueList, "Por|NadiaRubber|Neutral", "Root/Images/Portraits/Nadia/Rubber")

--Claudia
DL_AddPath("Root/Images/Portraits/Claudia/")
fnExtractDelayedBitmapToList(sCh1DialogueList, "Por|Claudia|Neutral", "Root/Images/Portraits/Claudia/Neutral")

--Karina
DL_AddPath("Root/Images/Portraits/Karina/")
fnExtractDelayedBitmapToList(sCh1DialogueList, "Por|Karina|Neutral", "Root/Images/Portraits/Karina/Neutral")

--Rochea
DL_AddPath("Root/Images/Portraits/Rochea/")
fnExtractDelayedBitmapToList(sCh1DialogueList, "Por|Rochea|Neutral",       "Root/Images/Portraits/Rochea/Neutral")
fnExtractDelayedBitmapToList(sCh1DialogueList, "Por|RocheaRubber|Neutral", "Root/Images/Portraits/Rochea/Rubber")

-- |[Polaris]|
SLF_Open(gsaDatafilePaths.sPolarisPath)
DL_AddPath("Root/Images/Portraits/Polaris/")
fnExtractDelayedBitmapToList(sCh1DialogueList, "Por|PolarisHuman|Neutral",  "Root/Images/Portraits/Polaris/Neutral")
fnExtractDelayedBitmapToList(sCh1DialogueList, "Por|PolarisHuman|Thinking", "Root/Images/Portraits/Polaris/Thinking")
fnExtractDelayedBitmapToList(sCh1DialogueList, "Por|PolarisHuman|Upset",    "Root/Images/Portraits/Polaris/Upset")
fnExtractDelayedBitmapToList(sCh1DialogueList, "Por|PolarisHuman|Rubber",   "Root/Images/Portraits/Polaris/Rubber")

-- |[Miho]|
SLF_Open(gsaDatafilePaths.sMihoPath)
DL_AddPath("Root/Images/Portraits/Miho/")
fnExtractDelayedBitmapToList(sCh1DialogueList, "Por|MihoKitsune|Neutral",   "Root/Images/Portraits/Miho/Neutral")
fnExtractDelayedBitmapToList(sCh1DialogueList, "Por|MihoKitsune|Angry",     "Root/Images/Portraits/Miho/Angry")
fnExtractDelayedBitmapToList(sCh1DialogueList, "Por|MihoKitsune|Drunk",     "Root/Images/Portraits/Miho/Drunk")
fnExtractDelayedBitmapToList(sCh1DialogueList, "Por|MihoKitsune|VeryDrunk", "Root/Images/Portraits/Miho/VeryDrunk")
fnExtractDelayedBitmapToList(sCh1DialogueList, "Por|MihoKitsune|Rubber",    "Root/Images/Portraits/Miho/Rubber")

-- |[Sharelock]|
SLF_Open(gsaDatafilePaths.sSharelockPath)
DL_AddPath("Root/Images/Portraits/Sharelock/")
fnExtractDelayedBitmapToList(sCh1DialogueList, "Por|SharelockSteamdroid|Neutral",  "Root/Images/Portraits/Sharelock/Neutral")
fnExtractDelayedBitmapToList(sCh1DialogueList, "Por|SharelockSteamdroid|Thinking", "Root/Images/Portraits/Sharelock/Thinking")
fnExtractDelayedBitmapToList(sCh1DialogueList, "Por|SharelockSteamdroid|AhHa",     "Root/Images/Portraits/Sharelock/AhHa")
fnExtractDelayedBitmapToList(sCh1DialogueList, "Por|SharelockSteamdroid|Sheepish", "Root/Images/Portraits/Sharelock/Sheepish")

-- |[Marriedraunes]|
SLF_Open(gsaDatafilePaths.sMarriedraunesPath)
DL_AddPath("Root/Images/Portraits/Marriedraunes/")
fnExtractDelayedBitmapToList(sCh1DialogueList, "Por|Marriedraunes|JNeutral", "Root/Images/Portraits/Marriedraunes/JNeutral")
fnExtractDelayedBitmapToList(sCh1DialogueList, "Por|Marriedraunes|JClimb",   "Root/Images/Portraits/Marriedraunes/JClimb")
fnExtractDelayedBitmapToList(sCh1DialogueList, "Por|Marriedraunes|JGrab",    "Root/Images/Portraits/Marriedraunes/JGrab")
fnExtractDelayedBitmapToList(sCh1DialogueList, "Por|Marriedraunes|SNeutral", "Root/Images/Portraits/Marriedraunes/SNeutral")
fnExtractDelayedBitmapToList(sCh1DialogueList, "Por|Marriedraunes|SClimb",   "Root/Images/Portraits/Marriedraunes/SClimb")
fnExtractDelayedBitmapToList(sCh1DialogueList, "Por|Marriedraunes|SGrab",    "Root/Images/Portraits/Marriedraunes/SGrab")
    
-- |[Bosses]|
--None.

-- |[ ================================ Enemy Dialogue Portraits ================================ ]|
--Portraits used when an enemy type speaks to the party.
local sSystemPortraits = "System Portraits"
sEverythingList = sSystemPortraits
fnCreateDelayedLoadList(sSystemPortraits)

-- |[Loading List]|
local saChapter1DiaEnemyList = {"Alraune", "Bee", "CultistF", "CultistM", "MaidGhost", "Slime", "Werecat", "WerecatThief", "Zombee", "SlimeG", "SlimeB", "SlimeI", "BeeRubber", "WerecatThiefRubber", "WerecatRubber", "AlrauneRubber", "CultistFRubber", "Mushraune", "Laura", "Lydie"}

-- |[List Extraction]|
--Uses the auto-loader.
DL_AddPath("Root/Images/Portraits/Enemies/")
SLF_Open(gsaDatafilePaths.sChapter1EmotePath)
fnExtractList("Por|", "|Neutral", "Root/Images/Portraits/Enemies/", saChapter1DiaEnemyList)

-- |[Misc NPCs]|
--NPCs that are in this chapter even if not specific to it.
SLF_Open(gsaDatafilePaths.sChapter0EmotePath)
DL_AddPath("Root/Images/Portraits/NPCs/")

--Extract.
fnExtractDelayedBitmapToList(sSystemPortraits, "Por|HumanNPCF0|Neutral",  "Root/Images/Portraits/NPCs/HumanNPCF0")
fnExtractDelayedBitmapToList(sSystemPortraits, "Por|HumanNPCF1|Neutral",  "Root/Images/Portraits/NPCs/HumanNPCF1")
fnExtractDelayedBitmapToList(sSystemPortraits, "Por|MercF|Neutral",       "Root/Images/Portraits/NPCs/MercF")
fnExtractDelayedBitmapToList(sSystemPortraits, "Por|MercM|Neutral",       "Root/Images/Portraits/NPCs/MercM")

--Reset the everything list.
sEverythingList = "Chapter 1 Portraits"

-- |[ ================================= Enemy Combat Portraits ================================= ]|
--All combat portraits have their warning flags on.
DL_AddPath("Root/Images/Portraits/Paragon/")
Bitmap_SetNewBitmapNoHandleWarnings(bShowWarningsForCombatPortraits)

--Chapter 0 Enemies
SLF_Open(gsaDatafilePaths.sChapter0CombatPath)
local saList = {"BestFriend", "SkullCrawler", "MirrorImage"}
for i = 1, #saList, 1 do
    fnExtractDelayedBitmapToList(sEverythingList, "Enemy|"   .. saList[i], "Root/Images/Portraits/Combat/"  .. saList[i])
    fnExtractDelayedBitmapToList(sEverythingList, "Paragon|" .. saList[i], "Root/Images/Portraits/Paragon/" .. saList[i])
end

--Chapter 1 Enemies
--SLF_Open(gsaDatafilePaths.sChapter1CombatPath)
--[=[
saList = {"Alraune", "BeeBuddy", "BeeGirl", "BloodyMirror", "CandleHaunt", "CultistF", "CultistM", "EyeDrawer", "Gravemarker", "Ink_Slime", "Jelli", "MaidGhost", "Mushraune", "Slime", "SlimeB", "SlimeG", "Sproutling", "SproutPecker", "Werecat", "Werecat_Burglar", "Zombee"}
for i = 1, #saList, 1 do
    fnExtractDelayedBitmapToList(sEverythingList, "Enemy|"   .. saList[i], "Root/Images/Portraits/Combat/"  .. saList[i])
    fnExtractDelayedBitmapToList(sEverythingList, "Paragon|" .. saList[i], "Root/Images/Portraits/Paragon/" .. saList[i])
end]=]

--[=[
--Chapter 1 Rubber
local saRubber = {"Alraune", "BeeGirl", "CultistF", "CultistM", "Werecat", "WerecatBurglar"}
for i = 1, #saRubber, 1 do
    fnExtractDelayedBitmapToList(sEverythingList, "Enemy|"   .. saRubber[i] .. "Rubber", "Root/Images/Portraits/Combat/"  .. saRubber[i] .. "Rubber")
    fnExtractDelayedBitmapToList(sEverythingList, "Paragon|" .. saRubber[i] .. "Rubber", "Root/Images/Portraits/Paragon/" .. saRubber[i] .. "Rubber")
end
]=]

--Chapter 1 Boss
--fnExtractDelayedBitmapToList(sEverythingList, "Enemy|Arachnophelia", "Root/Images/Portraits/Combat/Arachnophelia")
--fnExtractDelayedBitmapToList(sEverythingList, "Enemy|Infirm",        "Root/Images/Portraits/Combat/Infirm")
--fnExtractDelayedBitmapToList(sEverythingList, "Enemy|Mycela",        "Root/Images/Portraits/Combat/Mycela")

-- |[ ======================================== Finish Up ======================================= ]|
-- |[Reset Flag]|
--Default flag state is true.
Bitmap_SetNewBitmapNoHandleWarnings(true)

-- |[Issue Asset Stream]|
--If this flag is set, order all dialogue images that aren't linked to a costume to load.
if(gbUnloadDialoguePortraitsWhenDialogueCloses == false) then
    fnLoadDelayedBitmapsFromList(sCh1DialogueList, gciDelayedLoadLoadAtEndOfTick)
end

--Order all combat and unconditional images to load.
fnLoadDelayedBitmapsFromList(sEverythingList, gciDelayedLoadLoadAtEndOfTick)

-- |[Clean]|
SLF_Close()
