-- |[Farm Variables]|
--Variables used for the salt flats farming sequence in Chapter 1. It's very complex so it gets its own file.
DL_AddPath("Root/Variables/Chapter1/SaltFlats/")

--State variables
gci_SFF_NoTending = 0
gci_SFF_Water = 1
gci_SFF_Fertilizer = 2
gci_SFF_SpecialGrass = 3
gci_SFF_SpecialPollen = 4

--Directions
gci_FDir_Water = 0
gci_FDir_Fertilizer = 2
gci_FDir_Grass = 4
gci_FDir_Pollen = 6

-- |[DL Variables]|
--State variables for all 8 plots.
VM_SetVar("Root/Variables/Chapter1/SaltFlats/iPlotState0", "N", gci_SFF_NoTending)
VM_SetVar("Root/Variables/Chapter1/SaltFlats/iPlotState1", "N", gci_SFF_NoTending)
VM_SetVar("Root/Variables/Chapter1/SaltFlats/iPlotState2", "N", gci_SFF_NoTending)
VM_SetVar("Root/Variables/Chapter1/SaltFlats/iPlotState3", "N", gci_SFF_NoTending)
VM_SetVar("Root/Variables/Chapter1/SaltFlats/iPlotState4", "N", gci_SFF_NoTending)
VM_SetVar("Root/Variables/Chapter1/SaltFlats/iPlotState5", "N", gci_SFF_NoTending)
VM_SetVar("Root/Variables/Chapter1/SaltFlats/iPlotState6", "N", gci_SFF_NoTending)
VM_SetVar("Root/Variables/Chapter1/SaltFlats/iPlotState7", "N", gci_SFF_NoTending)

--Mei's farming variables.
VM_SetVar("Root/Variables/Chapter1/SaltFlats/iPutThingsAway", "N", 0)
VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasWater", "N", 0)
VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasFertilizer", "N", 0)
VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasGrass", "N", 0)
VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasPollen", "N", 0)

--Time of day handlers. 
VM_SetVar("Root/Variables/Chapter1/SaltFlats/iTasksDone", "N", 0)
VM_SetVar("Root/Variables/Chapter1/SaltFlats/iTasksDoneTotal", "N", 0)

--Script Variables
VM_SetVar("Root/Variables/Chapter1/SaltFlats/iAwaitAdina", "N", 0)
VM_SetVar("Root/Variables/Chapter1/SaltFlats/iDaysPassed", "N", 0)
VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasMetAdina", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/SaltFlats/iIsMeiControlled", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/SaltFlats/iMeiRecontrolCount", "N", 0.0)

-- |[Function: fnSetToFarmGfx]|
--When an NPC is created, this sets them to use the farm graphics.
function fnSetToFarmGfx()
	
	for p = 1, 4, 2 do
		TA_SetProperty("Move Frame", gci_FDir_Water,      p-1, "Root/Images/Sprites/FarmIcons/Water0")
		TA_SetProperty("Move Frame", gci_FDir_Fertilizer, p-1, "Root/Images/Sprites/FarmIcons/Ferti0")
		TA_SetProperty("Move Frame", gci_FDir_Grass,      p-1, "Root/Images/Sprites/FarmIcons/Grass0")
		TA_SetProperty("Move Frame", gci_FDir_Pollen,     p-1, "Root/Images/Sprites/FarmIcons/Polln0")
	end
	for p = 2, 4, 2 do
		TA_SetProperty("Move Frame", gci_FDir_Water,      p-1, "Root/Images/Sprites/FarmIcons/Water1")
		TA_SetProperty("Move Frame", gci_FDir_Fertilizer, p-1, "Root/Images/Sprites/FarmIcons/Ferti1")
		TA_SetProperty("Move Frame", gci_FDir_Grass,      p-1, "Root/Images/Sprites/FarmIcons/Grass1")
		TA_SetProperty("Move Frame", gci_FDir_Pollen,     p-1, "Root/Images/Sprites/FarmIcons/Polln1")
	end
end

-- |[Function: fnSetMeiToWhiteout]|
--Switches Mei's sprites and variables to the whiteout frames.
function fnSetMeiToWhiteout()
    
    --Store previous.
    local sCostumeHuman   = VM_GetVar("Root/Variables/Costumes/Mei/sCostumeHuman", "S")
    local sCostumeAlraune = VM_GetVar("Root/Variables/Costumes/Mei/sCostumeAlraune", "S")
    VM_SetVar("Root/Variables/Chapter1/SaltFlats/sCostumeHuman",   "S", sCostumeHuman)
    VM_SetVar("Root/Variables/Chapter1/SaltFlats/sCostumeAlraune", "S", sCostumeAlraune)
    
    --Set new, run.
    VM_SetVar("Root/Variables/Costumes/Mei/sCostumeHuman", "S", "Mindless")
    VM_SetVar("Root/Variables/Costumes/Mei/sCostumeAlraune", "S", "Mindless")
    LM_ExecuteScript(gsCharacterAutoresolve, "Mei")
end

-- |[Function: fnClearWhiteout]|
--Clear whiteout.
function fnClearWhiteout()
    
    --Restore previous.
    local sCostumeHuman   = VM_GetVar("Root/Variables/Chapter1/SaltFlats/sCostumeHuman", "S")
    local sCostumeAlraune = VM_GetVar("Root/Variables/Chapter1/SaltFlats/sCostumeAlraune", "S")
    VM_SetVar("Root/Variables/Costumes/Mei/sCostumeHuman",   "S", sCostumeHuman)
    VM_SetVar("Root/Variables/Costumes/Mei/sCostumeAlraune", "S", sCostumeAlraune)
    
    --Run.
    LM_ExecuteScript(gsCharacterAutoresolve, "Mei")
end

-- |[Function: fnPlaceProblemIndicators]|
--Positions the indicator NPCs over the farm plots. Used when the level is re-loaded or when new problems generate.
-- bRemove is optional. If passed, the indicators will all be removed (regardless of the value).
function fnPlaceProblemIndicators(bRemove)
	
	if(bRemove == nil) then return end
	
	--Iterate.
	for i = 0, 7, 1 do
		
		--Get the variable.
		local iValue = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iPlotState" .. i, "N")

		--If there is no work needed her, move the matching NPC off the map.
		if(iValue == gci_SFF_NoTending or bRemove == true) then
			EM_PushEntity("NPCPLOT" .. i)
				TA_SetProperty("Position", -100, -100)
			DL_PopActiveObject()
	
		--If there is work needed, place the NPC and set their facing. The positions are generated in the Salt Flats map constructor.
		else
			local iXPos = ( math.floor((i+1) % 3) * 5) + 15
			local iYPos = ( math.floor((i+1) / 3) * 4) + 12
			EM_PushEntity("NPCPLOT" .. i)
				TA_SetProperty("Position", iXPos, iYPos)
				
				if(iValue == gci_SFF_Water) then
					TA_SetProperty("Facing", gci_FDir_Water)
				elseif(iValue == gci_SFF_Fertilizer) then
					TA_SetProperty("Facing", gci_FDir_Fertilizer)
				elseif(iValue == gci_SFF_SpecialGrass) then
					TA_SetProperty("Facing", gci_FDir_Grass)
				elseif(iValue == gci_SFF_SpecialPollen) then
					TA_SetProperty("Facing", gci_FDir_Pollen)
				end
				
			DL_PopActiveObject()
		end
	end
end

-- |[Function: fnGenerateFarmProblems]|
--Randomly spawns 4-7 problems on the farm plots.
function fnGenerateFarmProblems()
	
	--Array.
	local iaData = {gci_SFF_NoTending, gci_SFF_NoTending, gci_SFF_NoTending, gci_SFF_NoTending, gci_SFF_NoTending, gci_SFF_NoTending, gci_SFF_NoTending, gci_SFF_NoTending}
	
	--Generate how many problems to spawn.
	local iProblemsToGen = LM_GetRandomNumber(2, 5)
	
	--Start generating. Problems afflict a random plot and retry until a success is rolled.
	while(iProblemsToGen > 0) do
	
		--Roll a slot.
		local iSlot = LM_GetRandomNumber(1, 8)
		
		--Slot is occupied, so fail.
		if(iaData[iSlot] ~= gci_SFF_NoTending) then
	
		--Not occupied, roll a problem.
		else
			iaData[iSlot] = LM_GetRandomNumber(gci_SFF_Water, gci_SFF_SpecialPollen)
			iProblemsToGen = iProblemsToGen - 1
		end
	end
	
	--Iterate. Store in the variable of the matching name.
	for i = 0, 7, 1 do
		VM_SetVar("Root/Variables/Chapter1/SaltFlats/iPlotState" .. i, "N", iaData[i+1])
	end
	
	--Reposition the indicator NPCs.
	fnPlaceProblemIndicators(false)
end
