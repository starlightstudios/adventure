-- |[ =================================== Menu Close Handler =================================== ]|
--Whenever the player closes the pause menu, this script gets executed. This is used to allow the
-- player to equip the harpy badge and make harpies no longer target the party.
if(fnArgCheck(1) == false) then return end
local sSwitchType = LM_GetScriptArgument(0)

-- |[ ==================================== Change Equipment ==================================== ]|
--Only handle this when changing equipment.
if(sSwitchType ~= "Change Equipment") then return end

--No pulse occurs during the rubber sequence.
local iIsRubberMode = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
if(iIsRubberMode == 1.0) then return end

--Pulse.
fnStandardEnemyPulse()
