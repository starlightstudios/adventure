-- |[ ================================= Chapter Initialization ================================= ]|
--Called when the chapter is initialized. This can be either because it was loaded in the chapter select
-- or because the game was loaded with the chapter already active.
--If loading the game, the initial values created her are overwritten afterwards. These thus serve
-- as initial values. If a variable exists here but does not exist in the save file, the default
-- value is used.
local sBasePath = fnResolvePath()
gbInitializeDebug = false
Debug_PushPrint(gbInitializeDebug, "Beginning Chapter 1 Initialization.\n")

-- |[ ========================================= Loading ======================================== ]|
if(gbLoadedChapter1Assets ~= true and gbShouldLoadChapter1Assets == true) then

    --Set load handlers to chapter 1.
	if(gsMandateTitle ~= "Witch Hunter Izana") then
		LI_BootChapter1()
	end
    
    --Flag.
    Debug_Print(" Loading assets for chapter 1.\n")
    gbLoadedChapter1Assets = true
    
    --Load Sequence
    fnIssueLoadReset("AdventureModeCH1")
    LM_ExecuteScript(gsRoot .. "Chapter 1/Loading/100 Sprites.lua")
    LM_ExecuteScript(gsRoot .. "Chapter 1/Loading/101 Portraits.lua")
    LM_ExecuteScript(gsRoot .. "Chapter 1/Loading/102 Actors.lua")
    LM_ExecuteScript(gsRoot .. "Chapter 1/Loading/103 Scenes.lua")
    LM_ExecuteScript(gsRoot .. "Chapter 1/Loading/104 Overlays.lua")
    LM_ExecuteScript(gsRoot .. "Chapter 1/Loading/105 Audio.lua")
    SLF_Close()
    fnCompleteLoadSequence()  
    Debug_Print(" Loading assets done.\n")  

--Assets were already loaded. We need to call the scenes file anyway because those assets load into
-- lists at runtime.
else
    LM_ExecuteScript(gsRoot .. "Chapter 1/Loading/103 Scenes.lua")
end

-- |[ ======================================= Path Setup ======================================= ]|
--Set standard paths for the rest of the chapter.
gsStandardGameOver     = gsRoot .. "Chapter 1/Scenes/300 Standards/Defeat/Scene_Begin.lua"
gsStandardRetreat      = gsRoot .. "Chapter 1/Scenes/300 Standards/Retreat/Scene_Begin.lua"
gsStandardRevert       = gsRoot .. "Chapter 1/Scenes/300 Standards/Revert/Scene_Begin.lua"
gsStandardReliveBegin  = gsRoot .. "Chapter 1/Scenes/Z Relive Handler/Scene_Begin.lua"
gsStandardReliveEnd    = gsRoot .. "Chapter 1/Scenes/Z Relive Handler/Scene_End.lua"

--Menu Paths
AM_SetPropertyJournal("Bestiary Resolve Script", gsRoot .. "Chapter 1/200 Bestiary Handler.lua")
AM_SetPropertyJournal("Profile Resolve Script",  gsRoot .. "Chapter 1/210 Profile Handler.lua")
AM_SetPropertyJournal("Quest Resolve Script",    gsRoot .. "Chapter 1/220 Quest Handler.lua")
AM_SetPropertyJournal("Location Resolve Script", gsRoot .. "Chapter 1/230 Location Handler.lua")
AM_SetPropertyJournal("Paragon Resolve Script",  gsRoot .. "Chapter 1/240 Paragon Handler.lua")
AM_SetPropertyJournal("Combat Resolve Script",   gsRoot .. "Chapter 0/240 Combat Handler.lua")

--Autosave Icon
AL_SetProperty("Autosave Icon Path", "Root/Images/AdventureUI/Symbols22/RuneMei")

--Journal Icons
if(AM_GetPropertyJournal("Can Set Images") == true) then
    AM_SetPropertyJournal("Set Achievement Title",       "Plantchievements")
    AM_SetPropertyJournal("Set Achievement Show String", "[IMG0] Show Plantchievements")
    AM_SetPropertyJournal("Set Achievement Hide String", "[IMG0] Hide Plantchievements")
    AM_SetPropertyJournal("Set Achievement Over Image",  "Root/Images/AdventureUI/Journal/Static Parts Achievements Over Ch1")
end

-- |[ ======================================== Clearing ======================================== ]|
--Because of how the load handler works, there may be lingering equipment in the inventory if the
-- player saved and loaded in Nowhere, as characters are given their default gear at game load.
--Therefore, clear the inventory again.

-- |[Catalyst Handling]|
if(gbDontRecountCatalysts ~= true) then
    
    --Clear.
    AdInv_SetProperty("Clear")

    --Order the inventory to recompute how many catalysts we've picked up. Chapters can be done out
    -- of order or with NC+ so the current counts aren't necessarily zeroes.
    AdInv_SetProperty("Resolve Catalysts By String", "Chapter 1")
    
    --Get the counts from the inventory.
    local iHealthCatalyst     = AdInv_GetProperty("Catalyst Count", gciCatalyst_Health)
    local iAttackCatalyst     = AdInv_GetProperty("Catalyst Count", gciCatalyst_Attack)
    local iInitiativeCatalyst = AdInv_GetProperty("Catalyst Count", gciCatalyst_Initiative)
    local iEvadeCatalyst      = AdInv_GetProperty("Catalyst Count", gciCatalyst_Evade)
    local iAccuracyCatalyst   = AdInv_GetProperty("Catalyst Count", gciCatalyst_Accuracy)
    local iSkillCatalyst      = AdInv_GetProperty("Catalyst Count", gciCatalyst_Skill)

    --Set extra slots from skill catalysts.
    local iSlotCount = math.floor(iSkillCatalyst / gciCatalyst_Skill_Needed)
    AdvCombat_SetProperty("Set Skill Catalyst Slots", iSlotCount)

    --Mirror the catalyst counts into the DataLibrary.
    VM_SetVar("Root/Variables/Global/Catalysts/iHealth",     "N", iHealthCatalyst)
    VM_SetVar("Root/Variables/Global/Catalysts/iAttack",     "N", iAttackCatalyst)
    VM_SetVar("Root/Variables/Global/Catalysts/iInitiative", "N", iInitiativeCatalyst)
    VM_SetVar("Root/Variables/Global/Catalysts/iEvade",      "N", iEvadeCatalyst)
    VM_SetVar("Root/Variables/Global/Catalysts/iAccuracy",   "N", iAccuracyCatalyst)
    VM_SetVar("Root/Variables/Global/Catalysts/iSkill",      "N", iSkillCatalyst)
end

-- |[ ================================== Party Initialization ================================== ]|
-- |[ ============================ Mei =========================== ]|
--Create Mei. Her default job is "Fencer".
if(AdvCombat_GetProperty("Does Party Member Exist", "Mei") == false) then
    Debug_Print(" Creating Mei.\n")
    LM_ExecuteScript(gsRoot .. "Combat/Party/Mei/000 Initialize.lua")

    --Create a Silver Runestone and equip it to Mei.
    Debug_Print(" Creating Mei's equipment.\n")
    LM_ExecuteScript(gsItemListing, "Silver Runestone")
    LM_ExecuteScript(gsItemListing, "Mei's Work Uniform")
    AdvCombat_SetProperty("Push Party Member", "Mei")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Item A", "Silver Runestone")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Body", "Mei's Work Uniform")
    DL_PopActiveObject()
end

--Run Mei's costume routines.
LM_ExecuteScript(gsRoot .. "CostumeHandlers/Mei/ZZ Common.lua")

-- |[ ======================== Florentina ======================== ]|
--Create Florentina. Her default job is "Merchant".
if(AdvCombat_GetProperty("Does Party Member Exist", "Florentina") == false) then
    Debug_Print(" Creating Florentina.\n")
    LM_ExecuteScript(gsRoot .. "Combat/Party/Florentina/000 Initialize.lua")
end

--Run Florentina's costume routines.
LM_ExecuteScript(gsRoot .. "CostumeHandlers/Florentina/ZZ Common.lua")

-- |[ ========================================== Flags ========================================= ]|
--Mark Mei as the party leader.
gsPartyLeaderName = "Mei"

--Set the party positions. Mei is the leader, all others are empty.
Debug_Print(" Positioning party slots.\n")
AdvCombat_SetProperty("Clear Party")
AdvCombat_SetProperty("Party Slot", 0, "Mei")

--Setup the XP tables.
Debug_Print(" Booting XP table.\n")
LM_ExecuteScript(gsRoot .. "Combat/Party/XP Table Chapter 1.lua")

-- |[ ================================= Variable Initialization ================================ ]|
--Mark this as the current chapter.
Debug_Print(" Initializing chapter 1 variables.\n")
VM_SetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N", 1.0)

--Create script variables. This is done in its own file.
LM_ExecuteScript(sBasePath .. "001 Variables.lua")

--The farm variables are used in Adina's farm sequence.
LM_ExecuteScript(sBasePath .. "002 Farm Variables.lua")

--Create paths referring to enemies.
LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/Alias List.lua")

--Combat Paragon script
AdvCombat_SetProperty("Set Paragon Script",      gsRoot .. "Combat/Enemies/Chapter 1/Paragon Handler.lua")
AdvCombat_SetProperty("Set Enemy Auto Script",   gsRoot .. "Combat/Enemies/Chapter 1/Enemy Auto Handler.lua")
AdvCombat_SetProperty("Set Post Combat Handler", gsRoot .. "Combat/Enemies/Chapter 1/Post Combat Handler.lua")

--Combat Volunteer Script
AdvCombat_SetProperty("Set Volunteer Script", gsRoot .. "Chapter 1/100 Volunteer Handler.lua")

-- |[ ================================== Character Appearance ================================== ]|
--Call the costume handlers to build default appearances for the characters.
Debug_Print(" Running costume resolvers.\n")
local bSuppressErrorsOld = gbSuppressNoListErrors
gbSuppressNoListErrors = true
LM_ExecuteScript(gsCostumeAutoresolve, "Mei_Human")
LM_ExecuteScript(gsCostumeAutoresolve, "Florentina_Merchant")
gbSuppressNoListErrors = bSuppressErrorsOld

-- |[ ===================================== Doctor Bag Zero ==================================== ]|
--Mei does not start the chapter with the Doctor Bag. Zero it off here.
Debug_Print(" Setting doctor bag.\n")
VM_SetVar("Root/Variables/CrossChapter/DoctorBag/iDoctorBagChargesMax", "N", 0)
VM_SetVar("Root/Variables/CrossChapter/DoctorBag/iDoctorBagChargesCur", "N", 0)
AdInv_SetProperty("Doctor Bag Charges", 0)
AdInv_SetProperty("Doctor Bag Charges Max", 0)

-- |[ ====================================== Party Restore ===================================== ]|
--Make sure everyone is at full HP. This affects the whole party roster, not just the active party.
Debug_Print(" Restoring party.\n")
AdvCombat_SetProperty("Restore Roster")

-- |[ ====================================== Path Building ===================================== ]|
--Run this script to build cutscenes that can be accessed from the debug menu.
Debug_Print(" Building paths.\n")
LM_ExecuteScript(sBasePath .. "Scenes/Build Scene List.lua")

--Build a set of aliases. These must be used in place of hard paths for in-engine script calls.
LM_ExecuteScript(sBasePath .. "Scenes/Build Alias List.lua")

--Build a list of locations the player can warp to from the debug menu.
LM_ExecuteScript(gsRoot .. "Maps/Build Debug Warp List.lua", "Chapter 1")

--Standard combat paths.
AdvCombat_SetProperty("Standard Retreat Script", gsRoot .. "Chapter 1/Scenes/300 Standards/Retreat/Scene_Begin.lua")
AdvCombat_SetProperty("Standard Defeat Script",  gsRoot .. "Chapter 1/Scenes/300 Standards/Defeat/Scene_Begin.lua")
AdvCombat_SetProperty("Standard Victory Script", gsRoot .. "Chapter 1/Scenes/300 Standards/Victory/Scene_Begin.lua")

--UI Icons
AM_SetProperty("Set Form Icon",    "Root/Images/AdventureUI/CampfireMenuIcon/TransformMei")
AM_SetProperty("Set Relive Icon",  "Root/Images/AdventureUI/CampfireMenuIcon/ReliveMei")
AM_SetProperty("Set Costume Icon", "Root/Images/AdventureUI/CampfireMenuIcon/Costume")

-- |[ ===================================== Music and Sound ==================================== ]|
--Default combat music.
Debug_Print(" Setting combat music.\n")
AdvCombat_SetProperty("Default Combat Music", "BattleThemeMei", 93.816)

-- |[ ===================================== Field Abilities ==================================== ]|
--Purge field abilities if any exist:
local iSizeOfPath = DL_SizeOfPath("Root/Special/Combat/FieldAbilities/")
if(iSizeOfPath > 0) then
    DL_Purge("Root/Special/Combat/FieldAbilities/", false)
end

Debug_Print(" Setting field abilities.\n")
DL_AddPath("Root/Special/Combat/FieldAbilities/")
LM_ExecuteScript(gsFieldAbilityListing .. "Deadly Jump.lua", gciFieldAbility_Create)
LM_ExecuteScript(gsFieldAbilityListing .. "Wraithform.lua",  gciFieldAbility_Create)
LM_ExecuteScript(gsFieldAbilityListing .. "Scout Sight.lua", gciFieldAbility_Create)
LM_ExecuteScript(gsFieldAbilityListing .. "Pick Lock.lua",   gciFieldAbility_Create)

AdvCombat_SetProperty("Set Field Ability", 0, "NULL")
AdvCombat_SetProperty("Set Field Ability", 1, "NULL")
AdvCombat_SetProperty("Set Field Ability", 2, "NULL")
AdvCombat_SetProperty("Set Field Ability", 3, "NULL")
AdvCombat_SetProperty("Set Field Ability", 4, "NULL")

-- |[ ====================================== Intro Bypass ====================================== ]|
--The player can optionally bypass the intro. If that happens, this code executes.
if(gbBypassIntro == true) then
    
    --Debug.
    Debug_Print(" Setting intro bypass values.\n")

    --Doctor Bag starts at its normal charges. Set flags to upload these to the inventory.
    gbAutoSetDoctorBagProperties = true
    gbAutoSetDoctorBagCurrentValues = true
    VM_SetVar("Root/Variables/CrossChapter/DoctorBag/iDoctorBagChargesMax", "N", 100)
    VM_SetVar("Root/Variables/CrossChapter/DoctorBag/iDoctorBagChargesCur", "N", 100)
    AdInv_SetProperty("Doctor Bag Charges", 100)
    AdInv_SetProperty("Doctor Bag Charges Max", 100)
    LM_ExecuteScript(gsComputeDoctorBagTotalPath)
    
    --Give Mei the Rusty Katana. Does not happen in NC+.
    if(gbIsNewChapterPlus == false) then
        LM_ExecuteScript(gsItemListing, "Rusty Katana")
        AdvCombat_SetProperty("Push Party Member", "Mei")
            AdvCombatEntity_SetProperty("Equip Item To Slot", "Weapon", "Rusty Katana")
        DL_PopActiveObject()
    end
    
end
Debug_PopPrint("Finished chapter 1 initialization.\n")
