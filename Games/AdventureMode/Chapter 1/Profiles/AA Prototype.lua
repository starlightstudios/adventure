-- |[ ======================================= CharacterName ======================================= ]|
--Description.

-- |[ ===== Unknown Character ====== ]|
--Check if the player has met this character. If not, place a blank entry.
local bShowAllEntries                  = AM_GetPropertyJournal("Show All Entries")
local iHasSeenTrannadarFlorentinaScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
if(iHasSeenTrannadarFlorentinaScene == 0.0 and bShowAllEntries == false) then
    fnCreateProfileStorage(zPE, "CharacterName", "-----", "Null", 0, 0)
    fnSetProfileStorageDescription(zPE, "CharacterName", "")
    return
end

-- |[ ====== Known Character ======= ]|
-- |[Variables]|
local iBlytheTopicLevel = fnGetTopicState("Blythe", "Nadia")

-- |[Profile]|
local sProfile = 
"One of the more eccentric guards at the\\nTrannadar Trading Post, Nadia is a\\nyoung alraune with a fondness for puns.\\n\\n" ..
"She is unfamiliar with the intricacies of\\nhuman interaction, and frequently\\nmakes mistakes in etiquette. But you\\ncan't hold that against her, she's so darn\\nearnest!\\n\\n" .. 
"Despite her charming demeanor, she is\\na capable warrior and exemplary scout,\\nas the plants tell her everything going\\non in the forest. She is also often a\\n"..
"mediator between the trading post and\\nthe local alraunes.\\n\\n"

--Update profile.
if(iBlytheTopicLevel > 0) then
    sProfile = sProfile .. 
    "She was originally joined near Soswitch,\\nbut driven from her covenant and\\nforced to flee across the continent.\\nWhen she met Captain Blythe, she\\nrealized not all humans are the same.\\n\\n"..
    "Now she hopes to resolve the differences between humans and\\nmonstergirls peacefully."
end

-- |[Finish Up]|
fnCreateProfileStorage(zPE, "CharacterName", "CharacterName", "Root/Images/Portraits/Nadia/Neutral", 0, 0)
fnSetProfileStorageDescription(zPE, "CharacterName", sProfile)
