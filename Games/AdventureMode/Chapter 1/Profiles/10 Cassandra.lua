-- |[ ======================================= Cassandra ======================================== ]|
--Sucker for twintails.
local sInternalName    = "Cassandra"
local sDisplayName     = "Cassandra"
local sDisplayPortrait = "Root/Images/Portraits/CassandraDialogue/Human"
local sProfile         = ""

-- |[ ===== Unknown Character ====== ]|
--Check if the player has met this character. If not, place a blank entry.
local bShowAllEntries  = AM_GetPropertyJournal("Show All Entries")
local iSavedCassandra  = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedCassandra", "N")
local iTurnedCassandra = VM_GetVar("Root/Variables/Chapter1/Scenes/iTurnedCassandra", "N")
if(iSavedCassandra == 0.0 and iTurnedCassandra == 0.0 and bShowAllEntries == false) then
    local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
    zProfileEntry:fnBlankProfile()
    return
end

-- |[ ====== Known Character ======= ]|
-- |[Variables]|
-- |[Profile]|
if(iSavedCassandra == 1.0) then
    sProfile = 
    "A human who inexplicably found herself[BR]in the midst of Evermoon Forest and[BR]captured by the werecats. She fought[BR]back, but they outnumbered her.[BR][BR]"..
    "Luckily, Mei saved her. She is currently[BR]recuperating at the Trading Post."

--Werecat
else
    sDisplayPortrait = "Root/Images/Portraits/CassandraDialogue/Werecat"
    sProfile = 
    "A powerful fang, transformed into a[BR]werecat after a fight in northern[BR]Evermoon Forest. She ran off shortly[BR]afterwards, whereabouts unknown."
end

-- |[Finish Up]|
local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
zProfileEntry:fnSetToProfile(sDisplayPortrait, 0, 0)
zProfileEntry:fnSetDescription(sProfile)
