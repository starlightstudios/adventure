-- |[ ======================================= Share;lock ======================================= ]|
--Robot detective! Robot detective!
local sInternalName    = "Sharelock"
local sDisplayName     = "Share;lock"
local sDisplayPortrait = "Root/Images/Portraits/Sharelock/Thinking"
local sProfile         = ""

-- |[ ===== Unknown Character ====== ]|
--Check if the player has met this character. If not, place a blank entry.
local bShowAllEntries = AM_GetPropertyJournal("Show All Entries")
local iMetSharelock   = VM_GetVar("Root/Variables/Chapter1/Sharelock/iMetSharelock", "N")
if(iMetSharelock == 0.0 and bShowAllEntries == false) then
    local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
    zProfileEntry:fnBlankProfile()
    return
end

-- |[ ====== Known Character ======= ]|
-- |[Variables]|
-- |[Profile]|
sProfile = 
"A robot girl of unknown provenance.[BR]She is in poor condition, but her mind is[BR]sharp as a sword.[BR][BR]"..
"She solves puzzles for fun. Can she[BR]solve the puzzle of who she is and[BR]where some comes from?[BR][BR]"..
"Her 'journal' indicates she's a detective[BR]from London. Unfortunately, she doesn't[BR]know where London is, or what it is.[BR][BR]"..
"Stu-pendous!"

-- |[Finish Up]|
local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
zProfileEntry:fnSetToProfile(sDisplayPortrait, 0, 0)
zProfileEntry:fnSetDescription(sProfile)