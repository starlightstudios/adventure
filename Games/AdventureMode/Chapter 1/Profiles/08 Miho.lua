-- |[ ========================================== Miho ========================================== ]|
--Cool fox bounty hunter! Hell yes!
local sInternalName    = "Miho"
local sDisplayName     = "Miho"
local sDisplayPortrait = "Root/Images/Portraits/Miho/Neutral"
local sProfile         = ""

-- |[ ===== Unknown Character ====== ]|
--Check if the player has met this character. If not, place a blank entry.
local bShowAllEntries = AM_GetPropertyJournal("Show All Entries")
local iMetMiho        = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iMetMiho", "N")
local iHasThralls     = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iHasThralls", "N")
if(iMetMiho == 0.0 and iHasThralls == 0.0 and bShowAllEntries == false) then
    local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
    zProfileEntry:fnBlankProfile()
    return
end

-- |[ ====== Known Character ======= ]|
-- |[Variables]|
local iIsRubberMode    = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
local iStartedDrinking = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iStartedDrinking", "N")
local iMihoLostContest = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iMihoLostContest", "N")
local iSpokeToMiho     = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iSpokeToMiho", "N")

-- |[Profile]|
if(iHasThralls == 1.0 and iIsRubberMode == 1.0) then
    sDisplayName = "Thrall"
    sDisplayPortrait = "Root/Images/Portraits/Miho/Rubber"
    sProfile = 
    "A thrall, formerly a bounty hunter.[BR]Covered in rubber, her will is[BR]commanded by it. She serves the rubber[BR]with her body."

--Drinking contest is ongoing:
elseif((iStartedDrinking == 1.0 and iMihoLostContest == 0.0) or (iStartedDrinking == 0.0)) then
    sProfile = 
    "A kitsune bounty hunter from Trafal.[BR]Currently hunting Florentina's bounty.[BR]Let's hope she loses![BR][BR]"

--Miho lost:
elseif(iMihoLostContest == 1.0 and iSpokeToMiho == 0.0) then
    sProfile = 
    "A kitsune bounty hunter from Trafal.[BR]Her attempt to capture Florentina[BR]failed, and she passed out.[BR][BR]"

--Spoke to Miho:
elseif(iSpokeToMiho == 1.0) then
    sProfile = 
    "A kitsune bounty hunter from Trafal.[BR]Humbled by her defeat at the hands of[BR]Florentina, she intends to introspect for[BR]a while before taking another bounty.[BR][BR]"..
    "A fierce warrior, she hews to a code of[BR]honor taught by the Fox Goddess."
end

-- |[Finish Up]|
local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
zProfileEntry:fnSetToProfile(sDisplayPortrait, 0, 0)
zProfileEntry:fnSetDescription(sProfile)
