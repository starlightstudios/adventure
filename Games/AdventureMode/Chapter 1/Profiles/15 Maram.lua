-- |[ ========================================= Maram ========================================== ]|
--The coolest rilmani.
local sInternalName    = "Maram"
local sDisplayName     = "Maram"
local sDisplayPortrait = "Root/Images/Portraits/Maram/Neutral"
local sProfile         = ""

-- |[ ===== Unknown Character ====== ]|
--Check if the player has met this character. If not, place a blank entry.
local bShowAllEntries = AM_GetPropertyJournal("Show All Entries")
local iMetMaram       = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetMaram", "N")
if(iMetMaram == 0.0 and bShowAllEntries == false) then
    local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
    zProfileEntry:fnBlankProfile()
    return
end

-- |[ ====== Known Character ======= ]|
-- |[Variables]|
-- |[Profile]|
sProfile = 
"One of the many Rilmani in Nix Nedar,[BR]she was tasked with watching over the[BR]known bearer, Mei.[BR][BR]"..
"As part of her duty, she has grown[BR]attached to Mei, and used what little[BR]influence she had to comfort her in[BR]times of need.[BR][BR]"..
"Her fashion sense is unimpeachable."

-- |[Finish Up]|
local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
zProfileEntry:fnSetToProfile(sDisplayPortrait, 0, 0)
zProfileEntry:fnSetDescription(sProfile)