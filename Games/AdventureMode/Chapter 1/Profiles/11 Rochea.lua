-- |[ ========================================= Rochea ========================================= ]|
--Local alraune coven leader, tough.
local sInternalName    = "Rochea"
local sDisplayName     = "Rochea"
local sDisplayPortrait = "Root/Images/Portraits/Rochea/Neutral"
local sProfile         = ""

-- |[ ===== Unknown Character ====== ]|
--Check if the player has met this character. If not, place a blank entry.
local bShowAllEntries = AM_GetPropertyJournal("Show All Entries")
local iMetRochea      = VM_GetVar("Root/Variables/Chapter1/Alraunes/iMetRochea", "N")
if(iMetRochea == 0.0 and bShowAllEntries == false) then
    local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
    zProfileEntry:fnBlankProfile()
    return
end

-- |[ ====== Known Character ======= ]|
-- |[Variables]|
local iIsRubberMode   = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
local iRubberedRochea = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iRubberedRochea", "N")

-- |[Profile]|
if(iRubberedRochea == 1.0 and iIsRubberMode == 1.0) then
    sDisplayName = "Thrall"
    sDisplayPortrait = "Root/Images/Portraits/Rochea/Rubber"
    sProfile = 
    "A thrall, former leader of the Evermoon[BR]alraune covenant. Her leadership[BR]knowledge is useless to the command of[BR]the rubber, so she is a mere thrall."..
    ""

--Normal:
else
    sProfile = 
    "Leader of the Evermoon Forest alraune[BR]covenant, she is wise and a capable[BR]leader.[BR][BR]"..
    "She despises humans and sees them as[BR]brutal, violent, and untrustworthy. She[BR]opposes everything they do and[BR]transforms any caught unprepared[BR]in the forest."
end

-- |[Finish Up]|
local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
zProfileEntry:fnSetToProfile(sDisplayPortrait, 0, 0)
zProfileEntry:fnSetDescription(sProfile)
