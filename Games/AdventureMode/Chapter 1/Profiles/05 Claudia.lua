-- |[ ======================================== Claudia ========================================= ]|
--Leader of the Heavenly Doves, went missing in Quantir. 
local zProfileEntry = JournalEntry:new("Claudia", "Claudia")
zProfileEntry:fnSetToProfile("Root/Images/Portraits/Claudia/Neutral", 0, 0)

-- |[ ===== Unknown Character ====== ]|
--Check if the player has met this character. If not, place a blank entry.
local bShowAllEntries = AM_GetPropertyJournal("Show All Entries")
local iMetClaudia     = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetClaudia", "N")
if(iMetClaudia == 0.0 and bShowAllEntries == false) then
    zProfileEntry:fnBlankProfile()
    return
end

-- |[ ====== Known Character ======= ]|
-- |[Variables]|
local iSavedClaudia = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedClaudia", "N")

-- |[Profile]|
local sProfile = 
"Leader of a sect of the Heavenly Doves.[BR]She's gone missing after leading most of[BR]her convent to the Quantir Estate."

--Has met Claudia, but not saved her:
if(iMetClaudia == 1.0 and iSavedClaudia == 0.0) then
    sProfile = 
    "Leader of a sect of the Heavenly Doves.[BR]She's gone missing after leading most of[BR]her convent to the Quantir Estate.[BR][BR]"..
    "She fell into a trap and is currently[BR]imprisoned in the basement near the[BR]underground lake."
elseif(iMetClaudia == 1.0 and iSavedClaudia == 1.0) then
    sProfile = 
    "Leader of a sect of the Heavenly Doves.[BR]She is currently leading what is left of[BR]her convent at the Outland Farm.[BR][BR]"..
    "Mei rescued her from the Quantir[BR]Estate's basement."
end

-- |[Finish Up]|
zProfileEntry:fnSetDescription(sProfile)
