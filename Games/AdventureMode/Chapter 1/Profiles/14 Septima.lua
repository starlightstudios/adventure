-- |[ ======================================== Septima ========================================= ]|
--Damn that umbrella is looking good.
local sInternalName    = "Septima"
local sDisplayName     = "Septima"
local sDisplayPortrait = "Root/Images/Portraits/Septima/NeutralUp"
local sProfile         = ""

-- |[ ===== Unknown Character ====== ]|
--Check if the player has met this character. If not, place a blank entry.
local bShowAllEntries = AM_GetPropertyJournal("Show All Entries")
local iMetSeptima     = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetSeptima", "N")
if(iMetSeptima == 0.0 and bShowAllEntries == false) then
    local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
    zProfileEntry:fnBlankProfile()
    return
end

-- |[ ====== Known Character ======= ]|
-- |[Variables]|
-- |[Profile]|
sProfile = 
"The seventh of the unborn Rilmani,[BR]Septima is the current leader of their[BR]home in Nix Nedar.[BR][BR]"..
"She is a master of dimensional twisting[BR]and wishes to instruct Mei in her latent[BR]powers.[BR][BR]"..
"Her fashion sense is unimpeachable."

-- |[Finish Up]|
local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
zProfileEntry:fnSetToProfile(sDisplayPortrait, 0, 0)
zProfileEntry:fnSetDescription(sProfile)
