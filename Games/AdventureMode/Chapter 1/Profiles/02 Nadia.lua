-- |[ ========================================= Nadia ========================================== ]|
--Keeping the roads safe.
local zProfileEntry = JournalEntry:new("Nadia", "Nadia")
zProfileEntry:fnSetToProfile("Root/Images/Portraits/Nadia/Neutral", 0, 0)

-- |[ ===== Unknown Character ====== ]|
--Check if the player has met this character. If not, place a blank entry.
local bShowAllEntries             = AM_GetPropertyJournal("Show All Entries")
local iHasSeenTrannadarFirstScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N")
local iMetNadiaInWerecatScene     = VM_GetVar("Root/Variables/Chapter1/ScenesWerecat/iMetNadiaInWerecatScene", "N")
if(iHasSeenTrannadarFirstScene == 0.0 and iMetNadiaInWerecatScene == 0.0 and bShowAllEntries == false) then
    zProfileEntry:fnBlankProfile()
    return
end

-- |[ ====== Known Character ======= ]|
-- |[Variables]|
local iBlytheTopicLevel = fnGetTopicState("Blythe", "Nadia")

-- |[Profile]|
local sProfile = 
"One of the more eccentric guards at the[BR]Trannadar Trading Post, Nadia is a[BR]young alraune with a fondness for puns.[BR][BR]" ..
"She is unfamiliar with the intricacies of[BR]human interaction, and frequently[BR]makes mistakes in etiquette. But you[BR]can't hold that against her, she's so darn[BR]earnest![BR][BR]" .. 
"Despite her charming demeanor, she is[BR]a capable warrior and exemplary scout,[BR]as the plants tell her everything going[BR]on in the forest. She is also often a[BR]"..
"mediator between the trading post and[BR]the local alraunes.[BR][BR]"

--Update profile.
if(iBlytheTopicLevel > 0) then
    sProfile = sProfile .. 
    "She was originally joined near Soswitch,[BR]but driven from her covenant and[BR]forced to flee across the continent.[BR]When she met Captain Blythe, she[BR]realized not all humans are the same.[BR][BR]"..
    "Now she hopes to resolve the differences between humans and[BR]monstergirls peacefully."
end

-- |[Finish Up]|
zProfileEntry:fnSetDescription(sProfile)
