-- |[ ====================================== Crowbar-Chan ====================================== ]|
--A living crowbar and a stupid in-joke.
local zProfileEntry = JournalEntry:new("Crowbar-Chan", "Crowbar-Chan")
zProfileEntry:fnSetToProfile("Root/Images/Portraits/CrowbarChan/Neutral", 0, 0)

-- |[ ===== Unknown Character ====== ]|
--Check if the player has met this character. If not, place a blank entry.
local bShowAllEntries = AM_GetPropertyJournal("Show All Entries")
local iMetCrowbarChan = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetCrowbarChan", "N")
if(iMetCrowbarChan == 0.0 and bShowAllEntries == false) then
    zProfileEntry:fnBlankProfile()
    return
end

-- |[ ====== Known Character ======= ]|
-- |[Variables]|
-- |[Profile]|
local sProfile = 
"A crowbar that is inexplicably alive.[BR]That's all you need to know.[BR][BR]Nyyuuuu!"

-- |[Finish Up]|
zProfileEntry:fnSetDescription(sProfile)
