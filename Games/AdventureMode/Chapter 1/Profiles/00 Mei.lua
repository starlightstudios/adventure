-- |[ ========================================== Mei =========================================== ]|
--Mei! The most complex profile, changes dialogue based on her job. Always appears.
local sMeiForm           = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local iIsMeiControlled   = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iIsMeiControlled", "N")
local iMeiRecontrolCount = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iMeiRecontrolCount", "N")
local iBeganSequence     = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iBeganSequence", "N")
local iPolarisRunIn      = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iPolarisRunIn", "N")

-- |[Local Entry Variables]|
--Create and populate default values.
local zProfileEntry = JournalEntry:new("Mei", "Mei")
zProfileEntry:fnSetToProfile("Root/Images/Portraits/MeiDialogue/HumanSmirk", 0, 0)

-- |[Basic Profile]|
--Basic profile. A lot of forms and cases have these in common.
local sProfile = "A 22 year old waitress from Hong Kong.[BR]She was walking home when a bright[BR]flash of light came from nowhere and[BR]transported her to Pandemonium.[BR][BR]" .. 
                 "Now, she's trying to find a way back[BR]home, compelled to do so no matter[BR]how nice this new world is.[BR][BR]" .. 
                 "She carries a small silver runestone that[BR]can heal her in combat. She doesn't[BR]know where she got it, but feels she[BR]should hold on to it.[BR][BR]" .. 
                 "She also has discovered an exemplary[BR]natural agility and talent with weapons.[BR]She can sometimes predict enemy[BR]actions and counter them.[BR][BR]" .. 
                 "She is kind, polite, and likes to make[BR]new friends and meet new people. She[BR]tries to see the best in others and bring it out of them.[BR][BR]"

-- |[ ================== Form Specific Profiles ================== ]|
--Mei's portraits and description changes with each form.

-- |[Human]|
if(sMeiForm == "Human") then
    
    --Rubber queen costume.
    local sHumanCostume = VM_GetVar("Root/Variables/Costumes/Mei/sCostumeHuman", "S")
    if(sHumanCostume == "RubberQueen") then
        zProfileEntry:fnSetPortrait("Root/Images/Portraits/MeiDialogue/RubberQueenNeutral", 0, 0)
    end

    --Adina's Thrall:
    if(iIsMeiControlled == 1.0) then
        
        --Common. Name and portrait are overridden.
        zProfileEntry:fnSetDisplayName("Thrall")
        zProfileEntry:fnSetPortrait("Root/Images/Portraits/MeiDialogue/HumanMC", 0, 0)
        
        --Recontrol over zero, means this is a repeat. Changes description.
        if(iMeiRecontrolCount > 0.0) then
            zProfileEntry:fnSetDescription("A humble thrall of the wise mistress[BR]Adina. She thoughtlessly tends to the[BR]salt flats in southern Evermoon and, at[BR]night, to her mistress.[BR][BR]"..
                                           "She has voluntarily returned to her[BR]mistress temporarily, but will eventually[BR]leave, compelled to return home.")
                                    
        --First time:
        else
            zProfileEntry:fnSetDescription("A humble thrall of the wise mistress[BR]Adina. She thoughtlessly tends to the[BR]salt flats in southern Evermoon and, at[BR]night, to her mistress.")
        end
    
    --Otherwise, use the basic profile.
    else
        zProfileEntry:fnSetDescription(sProfile)
    end

-- |[Alraune]|
elseif(sMeiForm == "Alraune") then
    
    --Adina's Thrall:
    if(iIsMeiControlled == 1.0) then
        zProfileEntry:fnSetDisplayName("Thrall")
        zProfileEntry:fnSetPortrait("Root/Images/Portraits/MeiDialogue/AlrauneMC", 0, 0)
        zProfileEntry:fnSetDescription(""..
        "A humble thrall of the wise mistress[BR]Adina. She thoughlessly tends to the[BR]salt flats in southern Evermoon and, at[BR]night, to her mistress.[BR][BR]"..
        "She has voluntarily returned to her[BR]mistress temporarily, but will eventually[BR]leave, compelled to return home.")
        
    --Normal:
    else
        zProfileEntry:fnSetPortrait("Root/Images/Portraits/MeiDialogue/AlrauneSmirk", 0, 0)
        zProfileEntry:fnSetDescription(sProfile .. 
        "Transformed into an alraune, she has developed a bond with the[BR]natural world and her leaf-sisters. Still, she feels she must return[BR]home, possibly " ..
        "to spread her newfound love of nature to Earth.")
    end
    
-- |[Bee]|
elseif(sMeiForm == "Bee") then

    --Get her dialogue portrait.
    DialogueActor_Push("Mei")
        local sSmirkPath = DialogueActor_GetProperty("Portrait Path", "Smirk")
    DL_PopActiveObject()

    zProfileEntry:fnSetPortrait(sSmirkPath, 0, 0)
    zProfileEntry:fnSetDescription(sProfile .. 
    "Transformed into a bee, she loves her bee sisters more deeply than[BR]any human family could. She converses with the hive constantly, but[BR]knows she must " .. 
    "return home to Earth, possibly to start her own hive...")

-- |[Ghost]|
elseif(sMeiForm == "Ghost") then

    --Brief foray as Natalie:
    local iIsGhostTF = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsGhostTF", "N")
    if(iIsGhostTF == 1.0) then
        zProfileEntry:fnSetDisplayName("Natalie")
        zProfileEntry:fnSetPortrait("Root/Images/Portraits/MeiDialogue/GhostSmirk", 0, 0)
        zProfileEntry:fnSetDescription(""..
        "A young maid from a poor background[BR]working in Lady Quantir's estate. She[BR]doesn't read very well and likes to slack[BR]off in the library where nobody looks[BR]for her.")

    else
        zProfileEntry:fnSetPortrait("Root/Images/Portraits/MeiDialogue/GhostSmirk", 0, 0)
        zProfileEntry:fnSetDescription(sProfile .. 
        "At the Quantir estate, she was transformed into a ghost patterned[BR]after a long-dead girl named Natalie. Their souls have merged[BR]together, and now " ..
        "Natalie lives through her, never to be forgotten.")
    end

-- |[Slime]|
elseif(sMeiForm == "Slime") then
    zProfileEntry:fnSetPortrait("Root/Images/Portraits/MeiDialogue/SlimeSmirk", 0, 0)
    zProfileEntry:fnSetDescription(sProfile .. 
    "She has been transformed into a slime girl, and much as she'd like to[BR]slink off into the forest and live out her life, the desire to return[BR]home " ..
    "is still stronger. At least she can be as busty as she wants now!")

-- |[Werecat]|
elseif(sMeiForm == "Werecat") then
    zProfileEntry:fnSetPortrait("Root/Images/Portraits/MeiDialogue/WerecatSmirk", 0, 0)
    zProfileEntry:fnSetDescription(sProfile .. 
    "She has gained the incredible agility and strength of the werecat[BR]hunters, and is fully prepared to use her heightened strength and[BR]senses to " ..
    "find her way back to Earth.")
    
-- |[Wisphag]|
elseif(sMeiForm == "Wisphag") then
    zProfileEntry:fnSetPortrait("Root/Images/Portraits/MeiDialogue/WisphagSmirk", 0, 0)
    zProfileEntry:fnSetDescription(sProfile .. 
    "Now a Wisphag, her desire to return to Earth remains undiminished.[BR]She just needs to correct a few misconceptions along the way.")
    
-- |[Mannequin]|
elseif(sMeiForm == "Mannequin") then
    
    --Special: During the mannequin quest:
    if(iBeganSequence == 1.0 and iPolarisRunIn == 0.0) then
        zProfileEntry:fnSetDisplayName("Mannequin")
        zProfileEntry:fnSetPortrait("Root/Images/Portraits/MeiDialogue/Mannequin", 0, 0)
        zProfileEntry:fnSetDescription("An unremarkable mannequin.")
    
    --Normal case:
    else
        zProfileEntry:fnSetPortrait("Root/Images/Portraits/MeiDialogue/Mannequin", 0, 0)
        zProfileEntry:fnSetDescription(sProfile .. 
        "Transformed into a mannequin, her emotions are hidden but her desire[BR]to return home is as strong as ever. After all, that's where the[BR]department stores are!")
    end

-- |[Gravemarker]|
elseif(sMeiForm == "Gravemarker") then
    zProfileEntry:fnSetPortrait("Root/Images/Portraits/MeiDialogue/GravemarkerSmirk", 0, 0)
    zProfileEntry:fnSetDescription(sProfile .. 
    "In St. Fora's convent, she was transformed into an angel and petrified.[BR]She escaped with the help of her runestone before the angelic control[BR]forced " ..
    "her to cut her own head off and become mindless. The[BR]experience has shaken her, but she remains resolute in her search for[BR]a way home.")

-- |[Rubber]|
elseif(sMeiForm == "Rubber") then

    --Setup.
    local sImage = "Root/Images/Portraits/MeiDialogue/Rubber"
    
    --Rubber Queen costume
    local sRubberCostume = VM_GetVar("Root/Variables/Costumes/Mei/sCostumeRubber", "S")
    if(sRubberCostume == "RubberQueen") then
        sImage = "Root/Images/Portraits/MeiDialogue/RubberQueenNeutral"
    end
    
    --Set values.
    zProfileEntry:fnSetDisplayName("Thrall")
    zProfileEntry:fnSetPortrait(sImage, 0, 0)
    zProfileEntry:fnSetDescription(""..
    "A thrall coated entirely in rubber, once[BR]known as 'Mei'. All traces of her old[BR]personality are gone, replaced by a[BR]slick sheen.[BR][BR]" ..
    "The rubber coating her is commanding[BR]her thoughts and guiding her on her[BR]quest to do one thing: Spread.")

-- |[Zombee]|
elseif(sMeiForm == "Zombee") then
    zProfileEntry:fnSetDisplayName("Drone")
    zProfileEntry:fnSetPortrait("Root/Images/Portraits/MeiDialogue/BeeMC", 0, 0)
    zProfileEntry:fnSetDescription(""..
    "A mindless, worthless drone. In her old[BR]life, she was called 'Mei', but she no[BR]longer deserves a name.[BR][BR]Corrupted honey flows through her[BR]veins, " ..
    "vastly increasing her strength[BR]and pain tolerance and dominating her[BR]thoughts utterly. The corrupted hive[BR]mind constantly reinforces her[BR]commands.[BR][BR]" .. 
    "Her only purpose is to spread the honey[BR]to all the other bees and, eventually,[BR]everyone on Pandemonium.")
end