-- |[ ======================================== Breanne ========================================= ]|
--Runs her own little inn, very friendly.
local zProfileEntry = JournalEntry:new("Breanne", "Breanne")
zProfileEntry:fnSetToProfile("Root/Images/Portraits/Breanne/Neutral", 0, 0)

-- |[ ===== Unknown Character ====== ]|
--Check if the player has met this character. If not, place a blank entry.
local bShowAllEntries   = AM_GetPropertyJournal("Show All Entries")
local iHasBreanneMetMei = VM_GetVar("Root/Variables/Chapter1/Breanne/iHasMetMei", "N")
if(iHasBreanneMetMei == 0.0 and bShowAllEntries == false) then
    zProfileEntry:fnBlankProfile()
    return
end

-- |[ ====== Known Character ======= ]|
-- |[Variables]|
local iParents = fnGetTopicState("Parents", "Breanne")

-- |[Profile]|
local sProfile = 
"The owner and operator of Breanne's Pit[BR]Stop, a small lodge in Evermoon Forest.[BR]She likes to work with her hands and is[BR]relentlessly polite to strangers.[BR][BR]" ..
"She is adept at cooking, construction,[BR]business, and is deadly with a hammer.[BR][BR]"

--Update profile.
if(iParents > 0) then
    sProfile = sProfile .. 
    "Her parents frequently send suitors to[BR]her in a futile attempt to get her to[BR]marry. She's not interested."
end

-- |[Finish Up]|
zProfileEntry:fnSetDescription(sProfile)
