-- |[ ======================================= Florentina ======================================= ]|
--Very popular merchant.
local zProfileEntry = JournalEntry:new("Florentina", "Florentina")
zProfileEntry:fnSetToProfile("Root/Images/Portraits/FlorentinaDialogue/MerchantHappy", 0, 0)

-- |[ ===== Unknown Character ====== ]|
--Check if the player has met this character. If not, place a blank entry.
local bShowAllEntries                  = AM_GetPropertyJournal("Show All Entries")
local iHasSeenTrannadarFlorentinaScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
if(iHasSeenTrannadarFlorentinaScene == 0.0 and bShowAllEntries == false) then
    zProfileEntry:fnBlankProfile()
    return
end

-- |[ ====== Known Character ======= ]|
-- |[Variables]|
local sProfile = ""
local sDisplayName   = "Florentina"
local sEmotion       = "Happy"
local sJobPortrait   = "Root/Images/Portraits/FlorentinaMerchantDialogue/Happy"
local iIsGhostTF     = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsGhostTF", "N")
local sFlorentinaJob = VM_GetVar("Root/Variables/Global/Florentina/sCurrentJob", "S")
local iBeganSequence = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iBeganSequence", "N")
local iPolarisRunIn  = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iPolarisRunIn", "N")

-- |[Mannequin Sequence]|
if(iBeganSequence == 1.0 and iPolarisRunIn == 0.0) then
    sDisplayName = "Mannequin"
    sProfile = 
    "An unremarkable mannequin."

-- |[Normal Case]|
elseif(iIsGhostTF == 0.0) then
    sProfile = 
    "A trader at the Trannadar Trading Post.[BR]She agreed to tag along with Mei in[BR]order to keep away from the attention[BR]of certain 'elements'.[BR][BR]" .. 
    "Despite her gruff demeanor and prickly[BR]sense of humour, she does care about[BR]Mei and wants to help her find a way[BR]home.[BR][BR]" .. 
    "She is famous for her wit and keen eye[BR]for value. She also has a few uncouth[BR]skills, such as knife tricks and[BR]lockpicking.[BR][BR]" ..
    "Being an alraune, she is also[BR]poison-resistant and capable of[BR]regenerating health in battle. She is just[BR]as capable of speaking to plants as any[BR]" ..
    "other alraune, she just doesn't place as[BR]much value in what they have to say.[BR][BR]" .. 
    "She does not get along with the other local alraunes, except for Nadia,[BR]who has a special place in her heart. She greatly dislikes Rochea and[BR]her covenant, taking issue with their ideology and methods."

    --Special:
    if(sFlorentinaJob == "Lurker") then
        sProfile = sProfile .. 
        "[BR][BR]She is presently using a cursed amulet to become a mannequin, likely[BR]for her own nefarious reasons."
    elseif(sFlorentinaJob == "Agarist") then
        sProfile = sProfile .. 
        "[BR][BR]She symiotically bonded with a fungus and is enjoying the enhanced[BR]power and ability to change her identity at will."
    end

-- |[Ghost TF Case]|
else
    sDisplayName = "Houseplant"
    sEmotion = "Offended"
    sProfile = 
    "A wholly unremarkable houseplant.[BR]Needs periodic watering and likes being[BR]talked to."
end

-- |[Portrait]|
--Portrait changes with job.
if(sFlorentinaJob == "Merchant") then
    sJobPortrait = "Root/Images/Portraits/FlorentinaDialogue/Merchant" .. sEmotion
elseif(sFlorentinaJob == "Mediator") then
    sJobPortrait = "Root/Images/Portraits/FlorentinaDialogue/Mediator" .. sEmotion
elseif(sFlorentinaJob == "TreasureHunter") then
    sJobPortrait = "Root/Images/Portraits/FlorentinaDialogue/Treasure Hunter" .. sEmotion
elseif(sFlorentinaJob == "Agarist") then
    sJobPortrait = "Root/Images/Portraits/FlorentinaDialogue/Agarist" .. sEmotion
elseif(sFlorentinaJob == "Lurker") then
    sJobPortrait = "Root/Images/Portraits/FlorentinaDialogue/Lurker"
end

-- |[Finish Up]|
zProfileEntry:fnSetDisplayName(sDisplayName)
zProfileEntry:fnSetToProfile(sJobPortrait, 0, 0)
zProfileEntry:fnSetDescription(sProfile)