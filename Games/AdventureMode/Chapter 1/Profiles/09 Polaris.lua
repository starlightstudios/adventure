-- |[ ======================================== Polaris ========================================= ]|
--The Sealing Witch of the Stars, she has killed gods. Has a cute dog.
local sInternalName    = "Polaris"
local sDisplayName     = "Polaris"
local sDisplayPortrait = "Root/Images/Portraits/Polaris/Neutral"
local sProfile         = ""

-- |[ ===== Unknown Character ====== ]|
--Check if the player has met this character. If not, place a blank entry.
local bShowAllEntries = AM_GetPropertyJournal("Show All Entries")
local iMetPolaris     = VM_GetVar("Root/Variables/Chapter1/Polaris/iMetPolaris", "N")
if(iMetPolaris == 0.0 and bShowAllEntries == false) then
    local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
    zProfileEntry:fnBlankProfile()
    return
end

-- |[ ====== Known Character ======= ]|
-- |[Variables]|
local iIsRubberMode    = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
local iRubberedPolaris = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iRubberedPolaris", "N")

-- |[Profile]|
if(iRubberedPolaris == 1.0 and iIsRubberMode == 1.0) then
    sDisplayName     = "Thrall"
    sDisplayPortrait = "Root/Images/Portraits/Polaris/Rubber"
    sProfile = 
    "A thrall, formerly a wizard. Her[BR]impressive intellect and powerful[BR]magical skills make her an excellent[BR]rubber thrall."

--Normal:
else
    sProfile = 
    "The famed Sealing Witch of the Stars,[BR]Polaris lives in Starfield Swamp with[BR]her corgi, Dot.[BR][BR]"..
    "She is an extremely capable magician,[BR]master of many different fields. She is[BR]currently catching up on her academic[BR]readings.[BR][BR]"..
    "In order to have spending money, and to[BR]afford her academic fees, she sells[BR]adorable corgi statues made from Dot's[BR]cute template."
end

-- |[Finish Up]|
local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
zProfileEntry:fnSetToProfile(sDisplayPortrait, 0, 0)
zProfileEntry:fnSetDescription(sProfile)
