-- |[ ======================================== Aquillia ======================================== ]|
--Tough punk who ran into some cultists. Mei saves her.
local sInternalName    = "Aquillia"
local sDisplayName     = "Aquillia"
local sDisplayPortrait = "Root/Images/Portraits/AquilliaDialogue/HumanNeutral"
local sProfile         = ""

-- |[ ===== Unknown Character ====== ]|
--Check if the player has met this character. If not, place a blank entry.
local bShowAllEntries = AM_GetPropertyJournal("Show All Entries")
local iSeePrisonerEscape = VM_GetVar("Root/Variables/Chapter1/Scenes/Intro|iSeePrisonerEscape", "N")
if(iSeePrisonerEscape == 1.0 and bShowAllEntries == false) then
    local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
    zProfileEntry:fnBlankProfile()
    return
end

-- |[ ====== Known Character ======= ]|
-- |[Variables]|
local iHasMetAquillia = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasMetAquillia", "N")

-- |[Profile]|
--Has met Aquillia at the lakeside.
if(iHasMetAquillia == 1.0) then
    sProfile = 
    "Aquillia Whitebride, former prisoner of[BR]the cultists in the Dimensional Trap.[BR]With Mei's help, she escaped, but[BR]suffered a broken arm.[BR][BR]"..
    "She is currently recovering at a cabin[BR]near the Trannadar Trading Post.[BR][BR]"..
    "She harbours a grudge against the[BR]cultists, but didn't mention exactly why[BR]she was snooping around the building to[BR]begin with."
else
    sDisplayName = "Prisoner"
    sProfile = 
    "A prisoner Mei met in the basement of[BR]the Dimensional Trap. Her arm was[BR]broken, but she stole the clothes off a[BR]cultist and escaped."
end

-- |[Finish Up]|
local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
zProfileEntry:fnSetToProfile(sDisplayPortrait, 0, 0)
zProfileEntry:fnSetDescription(sProfile)
