-- |[ ========================================= Adina ========================================== ]|
--A very kind soul.
local sInternalName    = "Adina"
local sDisplayName     = "Adina"
local sDisplayPortrait = "Root/Images/Portraits/Adina/Neutral"
local sProfile         = ""

-- |[ ===== Unknown Character ====== ]|
--Check if the player has met this character. If not, place a blank entry.
local bShowAllEntries = AM_GetPropertyJournal("Show All Entries")
local iHasMetAdina    = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iHasMetAdina", "N")
if(iHasMetAdina == 0.0 and bShowAllEntries == false) then
    local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
    zProfileEntry:fnBlankProfile()
    return
end

-- |[ ====== Known Character ======= ]|
-- |[Variables]|
local iIsRubberMode    = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
local iRubberedAdina   = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iRubberedAdina", "N")
local iMeiLovesAdina   = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiLovesAdina", "N")
local iIsMeiControlled = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iIsMeiControlled", "N")

-- |[Profile]|
--Rubber mode.
if(iRubberedAdina == 1.0 and iIsRubberMode == 1.0) then
    sDisplayName     = "Thrall"
    sDisplayPortrait = "Root/Images/Portraits/Adina/Rubber"
    sProfile = 
    "A thrall, former alraune of Evermoon[BR]Forest. Had an emotional connection to[BR]another thrall. Can no longer think for[BR]herself, and serves the rubber with her[BR]body."

--Mei is controlled:
elseif(iIsMeiControlled == 1.0) then
    sDisplayName = "Mistress"
    sProfile = 
    "Thrall's perfect mistress, she is wise and[BR]caring. Her word is law, her will is[BR]thrall's will."
    
--Normal, has met, not enslaved:
else
    sProfile = 
    "An alraune from Evermoon Forest, she[BR]lives in the Salt Flats near Trafal. She is[BR]cultivating the land with special plants[BR]to cleanse the salt which pollutes it.[BR][BR]"
    
    --Mei is in love:
    if(iMeiLovesAdina == 1.0) then
        sProfile = sProfile ..
        "Mei fell in love with her caring nature[BR]when she was enslaved, and considers[BR]her to be her mistress.[BR][BR]"..
        "Though compelled to find a way home,[BR]Mei fully intends to return to the salt[BR]flats to be with her loving mistress for[BR]the rest of time."
    end
end

-- |[Finish Up]|
local zProfileEntry = JournalEntry:new(sInternalName, sDisplayName)
zProfileEntry:fnSetToProfile(sDisplayPortrait, 0, 0)
zProfileEntry:fnSetDescription(sProfile)
