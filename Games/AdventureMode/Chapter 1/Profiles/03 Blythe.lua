-- |[ ========================================= Blythe ========================================= ]|
--Captain of the guard.
local zProfileEntry = JournalEntry:new("Blythe", "Blythe")
zProfileEntry:fnSetToProfile("Root/Images/Portraits/Blythe/Neutral", 0, 0)

-- |[ ===== Unknown Character ====== ]|
--Check if the player has met this character. If not, place a blank entry.
local bShowAllEntries             = AM_GetPropertyJournal("Show All Entries")
local iHasSeenTrannadarFirstScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N")
if(iHasSeenTrannadarFirstScene == 0.0 and bShowAllEntries == false) then
    zProfileEntry:fnBlankProfile()
    return
end

-- |[ ====== Known Character ======= ]|
-- |[Variables]|
local iBlytheTopicLevel = fnGetTopicState("Blythe", "Nadia")

-- |[Profile]|
local sProfile = 
"Captain Darius Blythe, head of the[BR]Trannadar Trading Post guard. A[BR]military man from Rondheim.[BR][BR]" ..
"He is gruff and serious, and takes his[BR]job very seriously. He hires and trains[BR]the guards in the Trading Post.[BR][BR]" .. 
"Technically, his employers are the[BR]trading companies who transport[BR]goods through the post.[BR][BR]"

--Update profile.
if(iBlytheTopicLevel > 0) then
    sProfile = sProfile .. 
    "He took Nadia in to act as a guard and[BR]get her on a productive path in life. He[BR]evidently cares for her and the people[BR]of the Trading Post."
end

-- |[Finish Up]|
zProfileEntry:fnSetDescription(sProfile)
