-- |[ ==================================== Location Handler ==================================== ]|
--Whenever the journal is opened to locations mode, generates location entries.

-- |[ ======================================== Loading ========================================= ]|
--If this flag is nil, then we need to attempt to load the location images.
DL_AddPath("Root/Images/RegionNotifiers/Permanent/")
local sBaseDLPath = "Root/Images/RegionNotifiers/Permanent/"

--Lookups of information needed to load the image.
local zaImageList = {}
table.insert(zaImageList, {"Dimensional Trap",       gsDatafilesPath .. "UIRegionDimensionalTrap.slf",      "DimensionalTrap|21"})
table.insert(zaImageList, {"Evermoon Forest",        gsDatafilesPath .. "UIRegionEvermoonForest.slf",       "EvermoonForest|21"})
table.insert(zaImageList, {"Arbonne Plains",         gsDatafilesPath .. "UIRegionArbonnePlains.slf",        "ArbonnePlains|21"})
table.insert(zaImageList, {"Starfield Swamp",        gsDatafilesPath .. "UIRegionStarfieldSwamp.slf",       "StarfieldSwamp|21"})
table.insert(zaImageList, {"Trannadar Trading Post", gsDatafilesPath .. "UIRegionTrannadarTradingPost.slf", "TrannadarTradingPost|21"})
table.insert(zaImageList, {"Quantir High Wastes",    gsDatafilesPath .. "UIRegionQuantirHighWastes.slf",    "QuantirHighWastes|21"})
table.insert(zaImageList, {"Quantir Estate",         gsDatafilesPath .. "UIRegionQuantirEstate.slf",        "QuantirEstate|21"})
table.insert(zaImageList, {"Trafal Glacier",         gsDatafilesPath .. "UIRegionTrafalGlacier.slf",        "TrafalGlacier|21"})
table.insert(zaImageList, {"Nix Nedar",              gsDatafilesPath .. "UIRegionNixNedar.slf",             "NixNedar|21"})

--Scan the image list.
for i = 1, #zaImageList, 1 do
    
    --Fast-access.
    local sImageName  = zaImageList[i][1]
    local sSLFPath    = zaImageList[i][2]
    local sInfileName = zaImageList[i][3]
    local sImagePath = sBaseDLPath .. sImageName
    
    --If the given image does not exist, order it to load.
    if(DL_Exists(sImagePath) == false) then
        
        --Open the requisite SLF file.
        SLF_Open(sSLFPath)
        
        --Create a bitmap entry.
        DL_ExtractDelayedBitmap(sInfileName, sImagePath)
        
        --Order that entry to load at the next opportunity.
        DL_LoadDelayedBitmap(sImagePath, gciDelayedLoadLoadAtEndOfTick)
    end
end

--Close the SLF file in case it was opened.
SLF_Close()

-- |[ ===================================== Entry Listing ====================================== ]|
--Entries must be re-created each time the file is opened in case a mod needs to add new ones.
local zJournalEntry = nil
gzaCh1LocationEntries = {}

--Set globals.
JournalEntry.zaAppendList = gzaCh1LocationEntries

-- |[ =============== Listing ================ ]|
zJournalEntry = JournalEntry:new("Dimensional Trap")
zJournalEntry:fnSetVariable("Root/Variables/Regions/Dimensional Trap/iHasSeenThisRegion")
zJournalEntry:fnSetToLocation("Root/Images/RegionNotifiers/Permanent/Dimensional Trap", 0, 0)
zJournalEntry:fnSetDescription(""..
    "An ancient rotting manor located in the middle of nowhere. Legend[BR]"     ..
    "has it that the building is older than the world itself, and is a [BR]"    ..
    "low point between dimensions where objects land when they fall[BR]"        ..
    "through the cracks in space.[BR][BR]"                                      .. 
    "It consists of a twisting dungeon basement, a main floor, and a roof[BR]"  .. 
    "with a garden. There is a side door to a second basement that goes[BR]"    ..
    "deeper than the main basement.[BR][BR]"                                    ..
    "The southern exit leads to Evermoon Forest. The north exit leads to a[BR]" ..
    "lake front where a canoe can lead to the Arbonne Plains, if you have[BR]"  ..
    "an oar.")

--Arbonne Plains
zJournalEntry = JournalEntry:new("Arbonne Plains")
zJournalEntry:fnSetVariable("Root/Variables/Regions/Arbonne Plains/iHasSeenThisRegion")
zJournalEntry:fnSetToLocation("Root/Images/RegionNotifiers/Permanent/Arbonne Plains", 0, 0)
zJournalEntry:fnSetDescription(""..
    "West of Evermoon lies an expanse of semi-arid plains called Arbonne.[BR]"..
    "The plains stretch north until Dvinir forest where Soswitch is located.[BR]"..
    "The plains are home to farmers growing grains and orchards. It is[BR]"..
    "otherwise inhabited by prowling werecats and the occasional bee girl.[BR]")

--Starfield Swamp
zJournalEntry = JournalEntry:new("Starfield Swamp")
zJournalEntry:fnSetVariable("Root/Variables/Regions/Starfield Swamp/iHasSeenThisRegion")
zJournalEntry:fnSetToLocation("Root/Images/RegionNotifiers/Permanent/Starfield Swamp", 0, 0)
zJournalEntry:fnSetDescription(""..
    "Splitting the west and east halves of the Evermoon Forest is a[BR]"..
    "sunken region known as Starfield Swamp. It is fed by several rivers[BR]"..
    "from the east and filters the water before it reaches Dormine Lake[BR]"..
    "to the west.[BR][BR]"..
    "A witch lives in the middle of the swamp, while its southern reaches[BR]"..
    "are home to St. Fora's convent, a long abandoned religious fort.[BR]"..
    "Vendrilstadt, a fishing village, is just past the western edge.")

--Trading Post
zJournalEntry = JournalEntry:new("Trannadar Trading Post")
zJournalEntry:fnSetVariable("Root/Variables/Regions/Trannadar Trading Post/iHasSeenThisRegion")
zJournalEntry:fnSetToLocation("Root/Images/RegionNotifiers/Permanent/Trannadar Trading Post", 0, 0)
zJournalEntry:fnSetDescription(""..
    "Sitting at the crossroad between the trade routes between Trafal, the[BR]"..
    "eastern city states, and Jeffespeir, the trading post is one of the few[BR]"..
    "human settlements in the Trannadar region.[BR][BR]"..
    "The guards can be seen in their blue and yellow uniforms escorting[BR]"..
    "traders and their pack animals along the routes. Equipment and[BR]"..
    "general goods are available for purchase within.[BR][BR]"..
    "Please do not bother the goat.")

--Quantir High Wastes
zJournalEntry = JournalEntry:new("Quantir High Wastes")
zJournalEntry:fnSetVariable("Root/Variables/Regions/Quantir High Wastes/iHasSeenThisRegion")
zJournalEntry:fnSetToLocation("Root/Images/RegionNotifiers/Permanent/Quantir High Wastes", 0, 0)
zJournalEntry:fnSetDescription(""..
    "A plateau that occupies much of the northeast reaches of Arulenta,[BR]"..
    "reaching to the ocean before a sharp cliff plunges into the surf.[BR][BR]"..
    "The land ranges from pasture land and scrub to straight desert.[BR]"..
    "Survival in the dry areas is difficult, and there are few settlements.[BR]"..
    "The land was more fertile in the distant past, but what little law[BR]"..
    "the region saw collapsed long ago.")

--Quantir Estate
zJournalEntry = JournalEntry:new("Quantir Estate")
zJournalEntry:fnSetVariable("Root/Variables/Regions/Quantir Estate/iHasSeenThisRegion")
zJournalEntry:fnSetToLocation("Root/Images/RegionNotifiers/Permanent/Quantir Estate", 0, 0)
zJournalEntry:fnSetDescription(""..
    "A manor and fort estate nestled in the forested area of Quantir, near[BR]"..
    "the border with Trannadar. It has been abandoned for centuries, and[BR]"..
    "travellers seeking shelter dare not enter, claiming the fort is haunted.[BR][BR]"..
    "The manor has two floors, several courtyards, and an extensive[BR]"..
    "basement sewer meant to store water in case of siege.")

--Trafal Glacier
zJournalEntry = JournalEntry:new("Trafal Glacier")
zJournalEntry:fnSetVariable("Root/Variables/Regions/Trafal Glacier/iHasSeenThisRegion")
zJournalEntry:fnSetToLocation("Root/Images/RegionNotifiers/Permanent/Trafal Glacier", 0, 0)
zJournalEntry:fnSetDescription(""..
    "The mighty mountains of Trafal crown the center of Arulenta, and[BR]"..
    "were the seat of its mightiest empire 700 years ago. While foreboding[BR]"..
    "and difficult to traverse, trade is impossible between the most habited[BR]"..
    "areas of Arulenta without crossing Trafal.[BR][BR]"..
    "The lower reaches thaw out during the hot months, while the upper [BR]"..
    "reaches are frozen permanently. Underground magma gives rise to hot[BR]"..
    "springs used for agriculture, a practice mastered long ago by Trafal's[BR]"..
    "most famous residents, the Kitsunes.[BR][BR]"..
    "The headwaters for almost all of the rivers in Trannadar originate[BR]"..
    "from Trafal, and the mountain range runs all the way across the[BR]"..
    "continent. Trade, it is said, flows from the melting glaciers.")

--Nix Nedar
zJournalEntry = JournalEntry:new("Nix Nedar")
zJournalEntry:fnSetVariable("Root/Variables/Regions/Nix Nedar/iHasSeenThisRegion")
zJournalEntry:fnSetToLocation("Root/Images/RegionNotifiers/Permanent/Nix Nedar", 0, 0)
zJournalEntry:fnSetDescription(""..
    "Between the folds of the dimensions lies the Rilmani home, Nix Nedar.[BR]"..
    "The city's layout is impossible to map into a simple three-dimensional[BR]"..
    "plane, folding into itself and having many buildings that are larger[BR]"..
    "on the inside than the outside.[BR][BR]"..
    "It is from here that the Rilmani maintain the ever-degrading fabric of[BR]"..
    "the dimensions. Technically, it is above Pandemonium in the fourth[BR]"..
    "dimension, but is still beneath the Still Plane.")
                
-- |[ ================ Upload ================ ]|
for i = 1, #gzaCh1LocationEntries, 1 do
    gzaCh1LocationEntries[i]:fnUploadData()
end

--Clean.
JournalEntry:fnCleanGlobals()

--Clear the list to save memory.
gzaCh1LocationEntries = nil
