-- |[ ===================================== Paragon Handler ==================================== ]|
--Whenever the journal is opened to paragons mode, generates entries and toggles.

-- |[Listing]|
--Setup.
zaParagonDatabase = {}

--Set globals.
JournalEntry.zaAppendList = zaParagonDatabase

-- |[ ======= List Creation ======== ]|
--                      |[Internal Name]|   |[Display Name]|               |[SLF Paragon Grouping Name]|
JournalEntry:newParagon("EvermoonSouth",  "Evermoon Forest South",     "EvermoonSouth")  --Evermoon SEB
JournalEntry:newParagon("EvermoonNorth",  "Evermoon Forest North",     "EvermoonNorth")  --Spooky Exterior
JournalEntry:newParagon("QuantirManse",   "Quantir Manor",             "QuantirManse")   --Quantir Basement E
JournalEntry:newParagon("RiverWilds",     "Trannadar River Wilds",     "RiverWilds")     --River Wilds A
JournalEntry:newParagon("RubberWilds",    "Infested River Wilds",      "RubberWilds")    --Wilds Tower D
JournalEntry:newParagon("StarfieldSwamp", "Starfield Swamp",           "StarfieldSwamp") --Starfield Swamp B
JournalEntry:newParagon("StForasUpper",   "St. Foras Upper Basement",  "StForasUpper")   --St Foras E
JournalEntry:newParagon("StForasLower",   "St. Foras Lower Basement",  "StForasLower")   --St Foras F
JournalEntry:newParagon("TrapBasement",   "Dimensional Trap Basement", "TrapBasement")   --Trap Basement C
JournalEntry:newParagon("TrapDungeon",    "Dimensional Trap Dungeon",  "TrapDungeon")    --Trap Dungeon D
JournalEntry:newParagon("ZombeeHive",     "Infected Beehive",          "ZombeeHive")     --Beehive Basement C

-- |[Assemble List]|
--Upload the information from the list.
for i = 1, #zaParagonDatabase, 1 do
    zaParagonDatabase[i]:fnUploadData()
end

-- |[Finish Up]|
--Reset globals.
JournalEntry:fnCleanGlobals()

--Clear database to save memory.
zaParagonDatabase = nil
