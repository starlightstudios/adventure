-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName  - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
-- 1: sIsFireside - Optional. If it exists, this was activated from the AdventureMenu at a save point. In those cases, the 
--                  Active Object is undefined.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName  = LM_GetScriptArgument(0)
local sIsFireside = LM_GetScriptArgument(1)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
    fnStandardMajorDialogue()
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] Zzzzzz...[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Blush] (She's sleeping.[P] I never realized how beautiful Florentina is...)[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Blush] (The moonlight dances over her...[P] it's -[P] so exciting![P] Maybe I should...)[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Surprise] (...![P] Florentina is sleeping with her knife in her hand!)[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] (She must be really paranoid...)[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Blush] (Better not try anything.[P] Wouldn't want her to stab me by mistake!)") ]])
end
