-- |[ ===================================== Profile Handler ==================================== ]|
--Whenever the journal is opened to profiles mode, generates profiles.

-- |[Listing]|
--Setup.
gzaCh1ProfileEntries = {}

--Set globals.
JournalEntry.zaAppendList = gzaCh1ProfileEntries

-- |[Call Subroutines]|
local sProfilePath = fnResolvePath() .. "Profiles/"
LM_ExecuteScript(sProfilePath .. "00 Mei.lua")
LM_ExecuteScript(sProfilePath .. "01 Florentina.lua")
LM_ExecuteScript(sProfilePath .. "02 Nadia.lua")
LM_ExecuteScript(sProfilePath .. "03 Blythe.lua")
LM_ExecuteScript(sProfilePath .. "04 Breanne.lua")
LM_ExecuteScript(sProfilePath .. "05 Claudia.lua")
LM_ExecuteScript(sProfilePath .. "06 CrowbarChan.lua")
LM_ExecuteScript(sProfilePath .. "07 Aquillia.lua")
LM_ExecuteScript(sProfilePath .. "08 Miho.lua")
LM_ExecuteScript(sProfilePath .. "09 Polaris.lua")
LM_ExecuteScript(sProfilePath .. "10 Cassandra.lua")
LM_ExecuteScript(sProfilePath .. "11 Rochea.lua")
LM_ExecuteScript(sProfilePath .. "12 Adina.lua")
LM_ExecuteScript(sProfilePath .. "13 Sharelock.lua")
LM_ExecuteScript(sProfilePath .. "14 Septima.lua")
LM_ExecuteScript(sProfilePath .. "15 Maram.lua")

-- |[Assemble List]|
--Upload the information from the list.
for i = 1, #gzaCh1ProfileEntries, 1 do
    gzaCh1ProfileEntries[i]:fnUploadData()
end

-- |[Finish Up]|
--Reset globals.
JournalEntry:fnCleanGlobals()

--Clear the list to save memory.
gzaCh1ProfileEntries = nil
