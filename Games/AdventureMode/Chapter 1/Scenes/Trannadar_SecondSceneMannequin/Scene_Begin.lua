-- |[ =========================== Trannadar Second Scene: Mannequin ============================ ]|
--If Nadia first met Mei as a human, and then Mei entered as a mannequin...
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarSecondScene", "N", 1.0)

-- |[Setup]|
--Variables.
local iFlorentinaPresent = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")

--Determine which side the player entered from.
local bIsPlayerAtBottom = false
local bIsPlayerAtTop = false
EM_PushEntity("Adventure Party", 0)
	local iPlayerX, iPlayerY = TA_GetProperty("Position")
DL_PopActiveObject()
if(iPlayerY >= 37 * 16) then bIsPlayerAtBottom = true end
if(iPlayerY <   7 * 16) then bIsPlayerAtTop = true end

--Wait a bit for the fade.
fnCutsceneWait(15)
fnCutsceneBlocker()

-- |[Movement]|
--Mei walks towards Nadia. This is based on where she spawned.
if(bIsPlayerAtBottom) then
    fnCutsceneMove("Mei", 28.25, 38.50)
    if(iFlorentinaPresent == 1) then
        fnCutsceneMove("Florentina", 28.25, 39.50)
    end
elseif(bIsPlayerAtTop) then
    fnCutsceneMove("Mei", 23.25, 6.50)
    if(iFlorentinaPresent == 1) then
        fnCutsceneMove("Florentina", 24.25, 6.50)
    end
else
    fnCutsceneMove("Mei", 43.25, 28.50)
    if(iFlorentinaPresent == 1) then
        fnCutsceneMove("Florentina", 44.25, 28.50)
    end
end
fnCutsceneBlocker()

--Change the music.
fnCutscene([[ AudioManager_PlayMusic("NadiasTheme") ]])

--Wait a bit.
fnCutsceneWait(5)
fnCutsceneBlocker()

-- |[Dialogue]|
fnStandardMajorDialogue()
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Nadia", "Neutral") ]])
fnCutscene([[ Append("Nadia: You can't fool me, Mei.[P] You got turned into a mannequin due to some sort of cursed jewelry, didn't you?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] How could you have possibly known that?[B][C]") ]])
fnCutscene([[ Append("Nadia: Lucky guess?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] The plants told you.[B][C]") ]])
fnCutscene([[ Append("Nadia: Yeah, the plants told me.[P] They saw the whole thing, but I was on the other side of the forest.[P] Couldn't get there in time.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] I'm more interested in what sort of awful pun you're going to come up with.[B][C]") ]])
fnCutscene([[ Append("Nadia: Me?[P] Make light of this situation?[P] Surely you jest.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] I don't.[P] And don't call me Shirley.[B][C]") ]])
fnCutscene([[ Append("Nadia: I di-[P] Hey![P] You're the one pulling jokes on me now![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] It's a classic I borrowed from Earth.[P] You're good, Nadia, but Earth has better.[B][C]") ]])
fnCutscene([[ Append("Nadia: I better up my game before I get shown up by every fashion display from another planet!") ]])
fnCutsceneBlocker()

-- |[Finish Up]|
fnCutscene([[ AudioManager_PlayMusic("TownTheme") ]])
fnCutsceneBlocker()

fnAutoFoldParty()
fnCutsceneBlocker()

