-- |[ ================================ Nadia: Inform of Mycela ================================= ]|
--If the player warps 3 times without visiting Mycela at Breanne's place, then Nadia appears on
-- certain maps and informs the player where she is. It is assumed that the trigger that calls this
-- has already handled spawning NPCs and whatnot, as all the positions are per-map. This is just
-- the dialogue section and variables.

-- |[Flags]|
--Flag this scene not to fire twice.
VM_SetVar("Root/Variables/Chapter1/Mycela/iNadiaToldOfMycela", "N", 1.0)

-- |[Variables]|
--Get values.
local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S") 

-- |[Talking]|
--Dialogue.
fnStandardMajorDialogue()
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Nadia", "Neutral") ]])
fnCutscene([[ Append("Nadia:[E|Neutral] Mei![P] Mei![P] Mei![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Happy] Nadia![P] What's up?[B][C]") ]])
fnCutscene([[ Append("Nadia:[E|Neutral] Stop me if anything I am about to say sounds strange, okay?[B][C]") ]])
fnCutscene([[ Append("Nadia:[E|Neutral] So there's this really grim looking alraune...[B][C]") ]])
fnCutscene([[ Append("Nadia:[E|Neutral] Who has the cutest dark aesthetic, which is weird because we don't usually do that...[B][C]") ]])
fnCutscene([[ Append("Nadia:[E|Neutral] Who says a mushroom took her over...[B][C]") ]])
fnCutscene([[ Append("Nadia:[E|Neutral] And who says she wants to talk to you about said mushroom...[B][C]") ]])
fnCutscene([[ Append("Nadia:[E|Neutral] ...[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Smirk] Yeah that all tracks.[P] We rescued her from a pit under St. Fora's.[B][C]") ]])
fnCutscene([[ Append("Nadia:[E|Neutral] Wow![P] So it was true![P] You're a real hero![B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] A goody-goody.[P] We handed that alraune off to Rochea.[P] Is she already sick of her?[B][C]") ]])
fnCutscene([[ Append("Nadia:[E|Neutral] Don't know, she just asked me to let you know she's at Breanne's Pit Stop and wanted to talk to you.[B][C]") ]])
fnCutscene([[ Append("Nadia:[E|Neutral] Didn't say what about, and she only talks to the little ones haltingly.[P] Okay, I delivered the message![P] Bye!") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()
