-- |[Cultist Meeting]|
--Cutscene where the cultists give you a map and instructions. How nice of those dumb, dumb people.
--This should be called by the door script when it gets opened.
local bSkipMostOfScene = false

--Variables.
local bIsFlorentinaInParty = AL_GetProperty("Is Character Following", "Florentina")

--Flags.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenCultistMeetingScene", "N", 1.0)

--Mei's form
local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")

--Wait a bit for dramatic effect.
fnCutsceneWait(145)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ Append("Mei:[E|Offended] (Uh oh...)") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--If Florentina is present:
if(bIsFlorentinaInParty == true) then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Uh, Mei?[P] I'm not supposed to be in this cutscene.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] You go ahead and I'll meet you in the next room.[P] And then we'll act like nothing happened.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneMove("Florentina", 11.25, 11.50)
	fnCutsceneMove("Florentina", 11.25, 4.50)
	fnCutsceneBlocker()
	fnCutsceneTeleport("Florentina", -100.25, -100.50)
	fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
	fnCutsceneBlocker()
end

--The cultists turn to look at her.
fnCutsceneFace("Cultist A", 0, 1)
fnCutsceneFace("Cultist B", 0, 1)
fnCutsceneFace("Cultist C", 0, 1)
fnCutsceneFace("Cultist D", 0, 1)
fnCutsceneFace("Cultist E", 0, 1)
fnCutsceneFace("Cultist F", 0, 1)
DL_PopActiveObject()
fnCutsceneWait(120)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ Append("Mei:[E|Offended] (Better leg it!)") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneFace("Mei", 0, 1)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("Cultist:[VOICE|CultistF] Wait initiate, don't go!") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneMove("Mei", 4.25, 10.50)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("Mei:[VOICE|Mei] (Initiate..?)") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneMove("Cultist A", 5.25, 10.50)
fnCutsceneFace("Cultist A", -1, 0)
fnCutsceneBlocker()
fnCutsceneFace("Mei", 1, 0)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Sad") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "CultistF", "Neutral") ]])
fnCutscene([[ Append("Cultist: Don't look so nervous, initiate.[P] We don't bite![B][C]") ]])
fnCutscene([[ Append("Cultist: But,[P] you should know better than to enter the meeting room while we're using it.[P] Didn't you see the schedule outside?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Surprise] S-[P]seriously?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Laugh] I mean, uh, sorry![P] It's...[P] my...[P] first...[P] day?[B][C]") ]])
fnCutscene([[ Append("Cultist: Well obviously![P] You haven't even been issued robes yet![B][C]") ]])
fnCutscene([[ Append("*The cultists chuckle*[B][C]") ]])
fnCutscene([[ Append("Cultist: *Psst![P] I'm kinda new here myself, but I just got a great idea![P] Play along!*") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneMove("Cultist A", 5.25, 7.50)
fnCutsceneFace("Cultist A", 1, 0)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Cultists face the room center again.
fnCutsceneFace("Cultist A", 1, 0)
fnCutsceneFace("Cultist B", 0, -1)
fnCutsceneFace("Cultist C", -1, 0)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneFace("Cultist A", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "CultistF", "Neutral") ]])
fnCutscene([[ Append("Cultist: There's a spot right there.[P] Come on in, we're all friends here![B][C]") ]])
fnCutscene([[ Append("Mei: (Eep!)") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneFace("Cultist A", 1, 0)
fnCutsceneMove("Mei", 4.25, 6.50, 0.70)
fnCutsceneBlocker()
fnCutsceneFace("Mei", 1, 0)
fnCutsceneWait(15)
fnCutsceneBlocker()
fnCutsceneMove("Mei", 5.25, 6.50, 0.70)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "CultistF", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "CultistM", "Neutral") ]])
fnCutscene([[ Append("CultistF: Right.[P] Where were we?[B][C]") ]])
fnCutscene([[ Append("CultistM: The patrols from that trading post have been getting dangerously close.[B][C]") ]])
fnCutscene([[ Append("CultistF: Oh, yeah![P] But, I believe I can solve that little problem for us.[B][C]") ]])
fnCutscene([[ Append("CultistM: Is that so?[B][C]") ]])
fnCutscene([[ Append("CultistF: Naturally, we should send someone undercover to infiltrate the trading post.[P] Send us intelligence reports,[P] spread misinformation,[P] that sort of thing.[B][C]") ]])
fnCutscene([[ Append("CultistM: And just who will do that?[P] Our regalia is not to be removed, so says the Sister Superior.[B][C]") ]])
fnCutscene([[ Append("CultistF: And if we were to send an initiate, who has not even earned her chains?[B][C]") ]])
fnCutscene([[ Append("CultistM: And where would you propose we find such a person?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] ... [B][C]") ]])
fnCutscene([[ Append("CultistF: ...[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] ... [B][C]") ]])
fnCutscene([[ Append("CultistF: *Psst![P] Volunteer, you dolt!*[B][C]") ]])
if(sMeiForm == "Ghost") then
    fnCutscene([[ Append("Mei:[E|Neutral] Uh, I can do it![P] I haven't earned my...[P] shackles?[B][C]") ]])
    fnCutscene([[ Append("CultistF: What about those shackles, that you are wearing, right now?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] Oh so you notice those but not the maid outfit.[B][C]") ]])
    fnCutscene([[ Append("CultistF: I assumed that was a stylistic choice.[B][C]") ]])
else
    fnCutscene([[ Append("Mei:[E|Neutral] Uh, I can do it![P] I haven't earned my...[P] shackles?[B][C]") ]])
    fnCutscene([[ Append("CultistF: *Try not to be so nervous, rookie.*[B][C]") ]])
    fnCutscene([[ Append("Mei: Eep![B][C]") ]])
end
fnCutscene([[ Append("CultistM: A brilliant plan![P] I will see to it you are rewarded for this.[P] Initiate, this will be a great way to earn favour for you![P] You should be honored.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Smirk] I'm double-dip honoured right now.[B][C]") ]])
fnCutscene([[ Append("CultistF: Indeed![P] Here, take this map.[P] The trading post is just west of here.[B][C]") ]])
fnCutscene([[ Append("[SOUND|Menu|SpecialItem]*Received Parchment Map!*[P]\\nYou can check the map from the pause menu.[B][C]") ]])
fnCutscene([[ Append("CultistF: We will dispatch another Initiate to receive your reports and relay instructions.[P] The code word will be 'Toothbrush'.[B][C]") ]])
fnCutscene([[ Append("CultistF: Oh, I almost didn't catch your name, Initiate.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Uhh...[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] ...[P] Tess.[B][C]") ]])
fnCutscene([[ Append("CultistF: Good luck on your mission, Initiate Tess![B][C]") ]])
fnCutscene([[ Append("CultistF: *Great![P] We're gonna get promoted for sure![P] Please, don't deprive me of this!*[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Happy] *Oh, sure, happy to help![P] Gotta go!*") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneFace("Mei", 1, 1)
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneFace("Mei", 0, 1)
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneFace("Mei", 1, 1)
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneFace("Mei", 0, 1)
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneMoveFace("Mei", 4.25, 6.50, 1, 0, 0.70)
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneFace("Mei", 1, 1)
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneFace("Cultist A", 0, -1)
fnCutsceneWait(45)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("Cultist:[VOICE|CultistF] Good luck out there![P] We're all counting on you!") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneMoveFace("Mei", 4.25, 4.50, 1, 1, 0.70)
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneMoveFace("Mei", 11.25, 4.50, 1, 0, 1.0)
fnCutsceneFace("Mei", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Transition to the next room. There is a short scene, but another cutscene handles it.
fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
fnCutscene([[ AL_BeginTransitionTo("TrapBasementB", gsRoot .. "Chapter 1/Scenes/Basement_CultistMeeting/Scene_PostTransition.lua") ]])
fnCutsceneBlocker()

--Provide the player with a map.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasNoMap", "N", 0.0)
fnResolveMapLocation("TrapBasementD")
