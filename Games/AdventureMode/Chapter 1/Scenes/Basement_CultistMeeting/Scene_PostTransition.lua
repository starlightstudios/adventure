-- |[Scene Post-Transition]|
--After the cutscene goes to fullbright, it switches maps. This plays afterwards.

--Reposition Mei instantly.
EM_PushEntity("Mei")
	TA_SetProperty("Position", 10, 27)
DL_PopActiveObject()
local bIsFlorentinaInParty = AL_GetProperty("Is Character Following", "Florentina")
if(bIsFlorentinaInParty == true) then
	fnCutsceneTeleport("Florentina", 10.25, 27.50)
end

--Fade.
AL_SetProperty("Activate Fade", 60, gci_Fade_Over_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0)

--Wait a bit.
fnCutsceneWait(120)
fnCutsceneBlocker()

--Mei talks to herself.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Offended") ]])
fnCutscene([[ Append("Mei: Man![P] Those guys are stupid![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Happy] But, I can smell fresh air.[P] Time to get out of here!") ]])
fnCutsceneBlocker()
fnCutscene([[ AL_SetProperty("Fold Party") ]])
fnCutsceneBlocker()

--Achievement.
AM_SetPropertyJournal("Unlock Achievement", "EscapeBasement")
