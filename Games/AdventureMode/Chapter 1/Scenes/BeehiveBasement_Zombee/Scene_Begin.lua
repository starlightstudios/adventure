-- |[Zombee Assault!]|
--Mei must be a Zombee and enter BeehiveBasementA from the north edge. This scene cannot repeat since she can't become
-- a Zombee again after seeing it.

-- |[Movement]|
--Mei moves to this position, then faces south.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (14.25 * gciSizePerTile), (10.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 0.0, 1.0)
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Camera]|
--Focus on Florentina.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 2.0)
	CameraEvent_SetProperty("Focus Position", 14.25 * gciSizePerTile, 21.50 * gciSizePerTile)
DL_PopActiveObject()
fnCutsceneWait(15)
fnCutsceneBlocker()

-- |[Dialogue Sequence]|
--Dialogue setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Florentina", "Neutral") ]])

--Talking.
fnCutscene([[ Append("Florentina:[E|Confused] Left, right, left, right![B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] Come on, you call yourselves bees?[P] You want to save your hive?[P] Then show some urgency![B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Offended] You![P] Straighten that back up.[P] You![P] Sloppy wingwork![P] A good bee is a disciplined bee![B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Facepalm] (They're never going to be ready in time...)") ]])
fnCutsceneBlocker()

-- |[Movement]|
--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--Scout Bee tries to get Florentina's attention.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlScout")
	ActorEvent_SetProperty("Face", 0.0, 1.0)
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneWait(15)
fnCutsceneBlocker()

--Move arounda  bit.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlScout")
	ActorEvent_SetProperty("Face", -1.0, 0.0)
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlScout")
	ActorEvent_SetProperty("Face", 1.0, 0.0)
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlScout")
	ActorEvent_SetProperty("Face", -1.0, 0.0)
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlScout")
	ActorEvent_SetProperty("Face", 1.0, 0.0)
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlScout")
	ActorEvent_SetProperty("Face", 0.0, 1.0)
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneWait(60)
fnCutsceneBlocker()

--Florentina turns to face her.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", 0.0, -1.0)
DL_PopActiveObject()
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Dialogue Sequence]|
--Dialogue setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Florentina", "Confused") ]])

--Talking.
fnCutscene([[ Append("Florentina:[E|Confused] What is it, scout?") ]])
fnCutsceneBlocker()

-- |[Camera]|
--Mei joins the Zombees.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (14.25 * gciSizePerTile), (14.50 * gciSizePerTile))
DL_PopActiveObject()

--Focus on Mei and the Zombees.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 2.0)
	CameraEvent_SetProperty("Focus Position", 14.25 * gciSizePerTile, 14.50 * gciSizePerTile)
DL_PopActiveObject()
fnCutsceneWait(15)
fnCutsceneBlocker()

--Hold a bit.
fnCutsceneWait(90)
fnCutsceneBlocker()

--Focus back on Florentina.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 2.0)
	CameraEvent_SetProperty("Focus Position", 14.25 * gciSizePerTile, 19.50 * gciSizePerTile)
DL_PopActiveObject()
fnCutsceneWait(15)
fnCutsceneBlocker()

-- |[Dialogue Sequence]|
--Dialogue setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Florentina", "Neutral") ]])

--Talking.
fnCutscene([[ Append("Florentina:[E|Confused] Damn it![P] We're out of time...[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] Troops![P] Fall -[P] In!") ]])
fnCutsceneBlocker()

-- |[Movement]|
--All the bees get to position.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlTroopA")
	ActorEvent_SetProperty("Move To", (12.25 * gciSizePerTile), (21.50 * gciSizePerTile), 2.50)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlTroopB")
	ActorEvent_SetProperty("Move To", (13.25 * gciSizePerTile), (21.50 * gciSizePerTile), 2.50)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlTroopC")
	ActorEvent_SetProperty("Move To", (14.25 * gciSizePerTile), (21.50 * gciSizePerTile), 2.50)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlTroopD")
	ActorEvent_SetProperty("Move To", (15.25 * gciSizePerTile), (21.50 * gciSizePerTile), 2.50)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlTroopE")
	ActorEvent_SetProperty("Move To", (16.25 * gciSizePerTile), (21.50 * gciSizePerTile), 2.50)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlScout")
	ActorEvent_SetProperty("Move To", (11.25 * gciSizePerTile), (20.50 * gciSizePerTile), 2.50)
DL_PopActiveObject()

--Bees also change facing.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlTroopA")
	ActorEvent_SetProperty("Face", 0.0, -1.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlTroopB")
	ActorEvent_SetProperty("Face", 0.0, -1.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlTroopC")
	ActorEvent_SetProperty("Face", 0.0, -1.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlTroopD")
	ActorEvent_SetProperty("Face", 0.0, -1.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlTroopE")
	ActorEvent_SetProperty("Face", 0.0, -1.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlScout")
	ActorEvent_SetProperty("Face", 1.0, 0.0)
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(15)
fnCutsceneBlocker()

--Florentina faces south to address her troops.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", 0.0, 1.0)
DL_PopActiveObject()
fnCutsceneBlocker()

-- |[Dialogue Sequence]|
--Dialogue setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Florentina", "Neutral") ]])

--Talking.
fnCutscene([[ Append("Florentina:[E|Confused] Bees![P] Listen to me![B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] This is your chance to save your hive and -[P] to save my friend...[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] Some of you may not make it, but you'll have died for what you believed in most![B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] If this is our end, then we will make our end so bright,[P] so glorious,[P] that it will sear a memory into the universe itself!![B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] Let us die as heroes![B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] Positions![P] Prepare for the assault![B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] Mei...[P] Sorry I couldn't be there...") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Movement]|
--Florentina faces North.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", 0.0, -1.0)
DL_PopActiveObject()

--The Zombees (including Mei) begin marching in step.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (14.25 * gciSizePerTile), (17.50 * gciSizePerTile), 0.70)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "ZombeeNPCA")
	ActorEvent_SetProperty("Move To", (13.25 * gciSizePerTile), (17.50 * gciSizePerTile), 0.70)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "ZombeeNPCB")
	ActorEvent_SetProperty("Move To", (15.25 * gciSizePerTile), (17.50 * gciSizePerTile), 0.70)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "ZombeeNPCC")
	ActorEvent_SetProperty("Move To", (13.25 * gciSizePerTile), (16.50 * gciSizePerTile), 0.70)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "ZombeeNPCD")
	ActorEvent_SetProperty("Move To", (15.25 * gciSizePerTile), (16.50 * gciSizePerTile), 0.70)
DL_PopActiveObject()
fnCutsceneBlocker()

-- |[Dialogue Sequence]|
--Dialogue setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])

--Talking.
fnCutscene([[ Append("Florentina:[E|Surprise] Mei![B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Surprise] Whatever has happened to you -[P] please listen to me![P] You've got to fight it![B][C]") ]])
fnCutscene([[ Append("Drone:[E|MC] Drone obeys.[P] Drone obeys.[P] Capture bees.[P] Convert bees.[P] Drone obeys.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] Come on, Mei...[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] Come on...[B][C]") ]])
fnCutscene([[ Append("Drone:[E|MC] Drone obeys.[P] Drone obeys.[P] Capture bees.[P] Convert bees.[P] Drone obeys.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] I can't believe you're so pathetic.[P] You've become what you hate.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] You're just going to let those stupid cultists beat you like that?[B][C]") ]])
fnCutscene([[ Append("Drone:[E|MC] Drone obeys.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Offended] Fight it![B][C]") ]])
fnCutscene([[ Append("Drone:[E|MC] Drone...") ]])
fnCutsceneBlocker()

--Cut the music.
fnCutscene([[ AL_SetProperty("Music", "Null") ]])
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
fnCutscene([[ Append("Florentina:[E|Offended] YOU ARE NOT SOME MINDLESS TOOL![B][C]") ]])
fnCutscene([[ Append("Drone:[E|MC] Drone...[P] is not...[P] a mindless...[P] tool...[B][C]") ]])
fnCutscene([[ Append("Drone:[E|MC] Drone...[P] Mei...[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Surprise] It's working![B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Surprise] Troops![P] I'll distract them![P] Grab the one in the middle![B][C]") ]])
fnCutscene([[ Append("Drone:[E|MC] Droooooone....") ]])
fnCutsceneBlocker()

-- |[Movement]|
--Wait a bit.
fnCutsceneWait(120)
fnCutsceneBlocker()

--Florentina does a little spin...
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", -1.0, 1.0)
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", 0.0, 1.0)
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", 1.0, 1.0)
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", 1.0, 0.0)
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", 1.0, -1.0)
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", 0.0, -1.0)
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()

--Florentina surges forward!
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (14.25 * gciSizePerTile), (18.50 * gciSizePerTile), 2.50)
DL_PopActiveObject()
fnCutsceneBlocker()

--Sound effect, world goes dark.
fnCutscene([[ AudioManager_PlaySound("World|HardHit") ]])
fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(120)
fnCutsceneBlocker()

-- |[Dialogue Sequence]|
--Dialogue setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])

--Talking.
fnCutscene([[ Append("Florentina:[E|Happy] (Dropped her like a hot rock!)[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] They're disorganized now![P] Capture that one![B][C]") ]])
fnCutscene([[ Append("...[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] Watch your left![P] No, your [P]*other*[P] left![B][C]") ]])
fnCutscene([[ Append("...[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] Their line is breaking![P] Press the attack![B][C]") ]])
fnCutscene([[ Append("...[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] Yeah![P] That's it![P] They're on the run![B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] We've got what we came for![P] Fall back![B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Facepalm] ...[P] In good order, you taffing idiots...") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(120)
fnCutsceneBlocker()

-- |[Transition]|
--Scene transit. Go to the upper beehive.
fnCutscene([[ AL_BeginTransitionTo("BeehiveInner", gsRoot .. "Chapter 1/Scenes/BeehiveBasement_Zombee/Scene_PostTransition.lua") ]])
fnCutsceneBlocker()
