-- |[Scene Post-Transition]|
--After getting punched out, Mei has a brief scene.
AL_SetProperty("Music", "Null")
AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1)

--Florentina doesn't count as being in the party, so spawn her here.
fnSpecialCharacter("Florentina", 13, 14, gci_Face_East, false, nil)

--Move Mei.
EM_PushEntity("Mei")
	TA_SetProperty("Position", 14, 14)
DL_PopActiveObject()

--Switch Mei to Bee mode. She can no longer become a zombee.
LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Bee.lua")

-- |[Dialogue]|
--Dialogue setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])

--Talking.
fnCutscene([[ Append("Drone:[E|MC] (Drone is...[P] Drone is...)[B][C]") ]])
fnCutscene([[ Append("Drone:[E|MC] (Am I a mindless tool?[P] Is drone a mindless tool?)[B][C]") ]])
fnCutscene([[ Append("Drone:[E|MC] (Is drone...[P] is Mei?)[B][C]") ]])
fnCutscene([[ Append("Drone:[E|MC] (Mei...)[B][C]") ]])
fnCutscene([[ Append("Drone:[E|Sad] (Mei...)[B][C]") ]])
fnCutscene([[ Append("Drone:[E|Neutral] (My -[P] name is Mei!)[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] (I'm not a mindless drone!)[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] (I love my bee sisters![P] I'm [P]*so*[P] sorry![P] I couldn't control myself!)[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Laugh] (Yes, I'm back![P] Yes![P] I love you too!)[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Laugh] (And -[P] we can save the other drones![P] If I can do it, so can they!)") ]])
fnCutsceneBlocker()

-- |[Wake Up]|
--Unfade.
fnCutscene([[ AL_SetProperty("Activate Fade", 60, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()

--Wait a few ticks.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Normal music.
fnCutscene([[ AL_SetProperty("Music", "ForestTheme") ]])

-- |[Dialogue]|
--Dialogue setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])

--Talking.
fnCutscene([[ Append("Mei:[E|Neutral] F-[P]Florentina?[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] Easy, easy.[P] Take it slow.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Am I...[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] You're fine.[P] Your friends are holding the line below, like before.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] If anything they're doing it better, now.[P] Their organization was sloppy.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] They struggle when they can't talk to the whole hive at once.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] Well you seem to be a lot less mindless.[P] Are all your bits still there?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] I think so.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Cry] It was...[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Surprise] There there.[P] Let it out, kid.[P] Don't bottle it up.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Cry] They took me and...[P] it was so big...[P] I couldn't even scream...[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Surprise] What was?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] We don't have a chance...[P] It was bigger than the sky and it was just staring right at me.[P] I was so small...[P] I had no choice but to obey...[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] There's always a choice.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] But...[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] Hey.[P] Look at me.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] Taff.[P] That.[P] Thing.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] I don't even know what you're babbling about, and I don't care.[P] Taff it.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] You don't listen to sorholes like that![P] You [P]*kill*[P] them.[P] And if you have to die first, so be it.[P] But you don't give in.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] I know it was hard, but the Mei I know and fight alongside is strong.[P] And you were strong enough to come to your senses.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] ...[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] If it had been me instead of you, I don't know if I'd have come back.[P] Maybe I'd have given in, too.[P] But you're here, now, strong and independent.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] That means they can lose.[P] Whatever those cultists worship [P]*can*[P] be hurt,\n[P]*can*[P] be beaten.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] ...[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Yeah.[P] Yeah![P] You're right![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] The bigger they are, the harder they fall![P] That's a saying we have on Earth![B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] Now that's what I like to hear![B][C]") ]])

local iSavedBeehive = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedBeehive", "N")
if(iSavedBeehive == 0.0) then
    fnCutscene([[ Append("Florentina:[E|Neutral] But the job's not done yet.[P] You ready for round two?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Happy] Let me at them![P] We can do it![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] You're damn skippy we can![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] We should regroup.[P] Check our equipment.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Don't make the same mistake as last time.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Yeah, of course.[P] We know what's ahead.[P] This one's in the bag.[P] C'mon, Mei![B][C]") ]])
    fnCutscene([[ Append("Narrator: (Mei has gained the 'Zombee' combat form.[P] She can transform into a Zombee temporarily during combat.)[B][C]") ]])
    fnCutscene([[ Append("Narrator: (This new form is extra powerful, but only lasts until the battle ends.)") ]])
else
    fnCutscene([[ Append("Florentina:[E|Neutral] Now, if you want to keep helping out the bees, that's up to you.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] I think they can handle the rest, though.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Yeah, but every one we take down is one they don't have to risk fighting.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] It's your call.[P] Let's get going.[B][C]") ]])
    fnCutscene([[ Append("Narrator: (Mei has gained the 'Zombee' combat form.[P] She can transform into a Zombee temporarily during combat.)[B][C]") ]])
    fnCutscene([[ Append("Narrator: (This new form is extra powerful, but only lasts until the battle ends.)") ]])
end
fnCutsceneBlocker()

--Florentina rejoins the party.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (14.25 * gciSizePerTile), (14.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutscene([[ AL_SetProperty("Fold Party") ]])
fnCutsceneBlocker()

--Mei uses her normal name.
AdvCombat_SetProperty("Push Party Member", "Mei")
    AdvCombatEntity_SetProperty("Display Name", "DEFAULT")
DL_PopActiveObject()
        
-- |[System]|
--These lines add Florentina to the party. They execute as soon as the scene starts, but take effect afterwards.
fnAddPartyMember("Florentina")
