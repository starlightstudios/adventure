-- |[ ============================ Trannadar Intro Scene: Mannequin ============================ ]|
--If Mei is a mannequin the first time she enters Trannadar Trading Post, this scene plays.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N", 1.0)

-- |[Move In From Edge]|
--Determine which side the player entered from.
local bIsPlayerAtBottom = false
local bIsPlayerAtTop = false
EM_PushEntity("Adventure Party", 0)
	local iPlayerX, iPlayerY = TA_GetProperty("Position")
DL_PopActiveObject()
if(iPlayerY >= 37 * 16) then bIsPlayerAtBottom = true end
if(iPlayerY <   7 * 16) then bIsPlayerAtTop = true end

--Wait a bit for the fade.
fnCutsceneWait(15)
fnCutsceneBlocker()

-- |[Movement]|
--Mei walks towards Nadia and Blythe. This is based on where she spawned.
if(bIsPlayerAtBottom) then
    fnCutsceneMove("Mei", 28.25, 38.50)
elseif(bIsPlayerAtTop) then
    fnCutsceneMove("Mei", 23.25, 6.50)
else
    fnCutsceneMove("Mei", 43.25, 28.50)
end
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(5)
fnCutsceneBlocker()

--Change the music.
fnCutscene([[ AudioManager_PlayMusic("NadiasTheme") ]])

--Face Nadia and Blythe south.
if(bIsPlayerAtBottom) then
    fnCutsceneFace("Nadia", 0, 1)
    fnCutsceneFace("Blythe", 0, 1)

--Face north.
elseif(bIsPlayerAtTop) then
    fnCutsceneFace("Nadia", 0, -1)
    fnCutsceneFace("Blythe", 0, -1)
    
--Face them east.
else
    fnCutsceneFace("Nadia", 1, 0)
    fnCutsceneFace("Blythe", 1, 0)
end
fnCutsceneWait(5)
fnCutsceneBlocker()

-- |[Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Blythe", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Nadia", "Neutral") ]])
fnCutscene([[ Append("Alraune:[E|Neutral] And then, boom![P] Bites the board in half![P] I was stunned![B][C]") ]])
fnCutscene([[ Append("Man:[E|Neutral] Nadia - [P][C]") ]])
fnCutscene([[ Append("Nadia:[E|Neutral] And then -[P] whapow -[P] spin kick, and he drops like a brick.[P] So that's how I beat them all up.[B][C]") ]])
fnCutscene([[ Append("Man:[E|Neutral] Nadia - [P][C]") ]])
fnCutscene([[ Append("Nadia:[E|Neutral] Of course I had to lash them to a cart to bring them all in.[P] They're out back.[B][C]") ]])
fnCutscene([[ Append("Man:[E|Neutral] Nadia![B][C]") ]])
fnCutscene([[ Append("Nadia:[E|Neutral] Yeah boss?[B][C]") ]])
fnCutscene([[ Append("Man:[E|Neutral] Visitor.[P] Do it properly this time.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Hello?[B][C]") ]])
fnCutscene([[ Append("Nadia:[E|Neutral] Hi![P] Name's Nadia.[P] Hey are you a talking puppet or something?[B][C]") ]])
fnCutscene([[ Append("Man:[E|Neutral] Good heavens, Nadia.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] I'm a mannequin.[P] So sort of, yes?[B][C]") ]])
fnCutscene([[ Append("Nadia:[E|Neutral] That's pretty cool.[P] You selling those clothes?[P] They look cute on you![B][C]") ]])
fnCutscene([[ Append("Man:[E|Neutral] Nadia, give her the official greeting![B][C]") ]])
fnCutscene([[ Append("Nadia:[E|Neutral] Ohhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh.[B][C]") ]])
fnCutscene([[ Append("Nadia:[E|Neutral] *Ahem*[P] Hello, visitor, and welcome to Trannadar Trading Post.[P] I'm Nadia, and this is Captain Blythe.[B][C]") ]])
fnCutscene([[ Append("Nadia:[E|Neutral] We allow all visitors, whether you're here for shopping, employment, or just to stay the night![B][C]") ]])
fnCutscene([[ Append("Nadia:[E|Neutral] Please keep in mind that we don't allow any fighting within the walls.[P] If you have a dispute, please bring it to myself, a guard, or the cap'n here.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] That's very nice of you.[B][C]") ]])
fnCutscene([[ Append("Nadia:[E|Neutral] How'd I do, boss?[B][C]") ]])
fnCutscene([[ Append("Blythe:[E|Neutral] Apart from requiring multiple prompts to do it in the first place?[P] You got the greeting correct.[P] Good job.[B][C]") ]])
fnCutscene([[ Append("Nadia:[E|Neutral] Hoyeah![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] I'm here looking for a way back to Earth.[P] Do you have any advice there?[B][C]") ]])
fnCutscene([[ Append("Nadia:[E|Neutral] Earth?[P] What makes you think you're not on Earth?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] On Earth, you can't get turned into a mannequin.[B][C]") ]])
fnCutscene([[ Append("Nadia:[E|Neutral] So is it like a country?[P] Earth is the strangest name for a country I've heard.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] It's definitely a different planet.[B][C]") ]])
fnCutscene([[ Append("Blythe:[E|Neutral] Madam, it is unlikely Nadia or I can help you there.[P] You may want to speak with the owner of the general store, Florentina.[B][C]") ]])
fnCutscene([[ Append("Blythe:[E|Neutral] She has been abroad and studied law and science.[P] If anyone can point you in the right direction, it's her.[B][C]") ]])
fnCutscene([[ Append("Nadia:[E|Neutral] And you can model for her in the meantime.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Thanks for the advice, I'll check it out.") ]])
fnCutsceneBlocker()

-- |[Finish Up]|
--Change the music.
fnCutscene([[ AudioManager_PlayMusic("TownTheme") ]])
fnCutsceneBlocker()

--Unlock topics.
WD_SetProperty("Unlock Topic", "Alraunes", 1)
WD_SetProperty("Unlock Topic", "Blythe", 1)
WD_SetProperty("Unlock Topic", "Cultists", 1)
WD_SetProperty("Unlock Topic", "Florentina", 1)
WD_SetProperty("Unlock Topic", "Job", 1)
WD_SetProperty("Unlock Topic", "Nadia", 1)
WD_SetProperty("Unlock Topic", "Name", 1)
WD_SetProperty("Unlock Topic", "TradingPost", 1)
WD_SetProperty("Unlock Topic", "Challenge", 1)
