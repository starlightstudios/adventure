-- |[Switch Scene]|
--Cutscene that plays when flipping the switch after talking to the cultist. Also destroys the switch mechanism.

-- |[Flag]|
--Prevents scene from running again.
VM_SetVar("Root/Variables/Chapter1/Scenes/iMetSpecialAcolyte", "N", 2.0)

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])

--Talking.
fnCutscene([[ Append("Florentina:[E|Neutral] So I think this is the one she was talking about.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] When I'm through, hit the switch.[P] Okay?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] All right!") ]])
fnCutsceneBlocker()

-- |[Movement]|
--Florentina moves to a standardized position.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (27.25 * gciSizePerTile), (15.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 0, -1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (15.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Move into the lock.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (8.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Brief dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("[VOICE|Florentina]Florentina:[VOICE|Florentina][E|Neutral] Hit it!") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()
	
--Change switch state.
fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
fnCutscene("AL_SetProperty(\"Switch State\", \"FloodSwitch\", false) ")
fnCutsceneBlocker()
fnCutsceneWait(30)
fnCutsceneBlocker()

--Open the door.
fnCutscene("AL_SetProperty(\"Open Door\", \"Door04\")")
fnCutscene("AL_SetProperty(\"Close Door\", \"Door03\")")
fnCutscene([[AudioManager_PlaySound("World|RemoteDoor")]])
fnCutsceneBlocker()
fnCutsceneWait(30)
fnCutsceneBlocker()

--Florentina moves past it.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (4.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (27.25 * gciSizePerTile), (4.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", 0, -1)
DL_PopActiveObject()
fnCutsceneBlocker()

--Florentina hits the other switch.
fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
fnCutscene("AL_SetProperty(\"Switch State\", \"FloodSwitchN\", true) ")
fnCutsceneBlocker()
fnCutsceneWait(30)
fnCutsceneBlocker()

--Open the door.
fnCutscene("AL_SetProperty(\"Open Door\", \"Door03\")")
fnCutscene("AL_SetProperty(\"Close Door\", \"Door04\")")
fnCutscene([[AudioManager_PlaySound("World|RemoteDoor")]])
fnCutsceneBlocker()
fnCutsceneWait(30)
fnCutsceneBlocker()

--Brief dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("[VOICE|Florentina]Florentina:[VOICE|Florentina][E|Neutral] It worked![P] Come on!") ]])
fnCutsceneBlocker()

--Mei moves over.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (15.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (8.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Florentina hits the switch again.
fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
fnCutscene("AL_SetProperty(\"Switch State\", \"FloodSwitchN\", false) ")
fnCutsceneBlocker()
fnCutsceneWait(30)
fnCutsceneBlocker()

--Open the door.
fnCutscene("AL_SetProperty(\"Open Door\", \"Door04\")")
fnCutscene("AL_SetProperty(\"Close Door\", \"Door03\")")
fnCutscene([[AudioManager_PlaySound("World|RemoteDoor")]])
fnCutsceneBlocker()
fnCutsceneWait(30)
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", 1, 1)
DL_PopActiveObject()

--Mei moves over.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (5.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", -1, -1)
DL_PopActiveObject()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])

--Talking.
fnCutscene([[ Append("Mei:[E|Laugh] Great work![P] I was getting so sick of all these switches![B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] Couldn't have done it without you.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] But, what's this switch over here?") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Movement]|
--Both move over.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (31.25 * gciSizePerTile), (4.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (30.25 * gciSizePerTile), (4.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 0, -1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", 1, -1)
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])

--Talking.
fnCutscene([[ Append("Mei:[E|Neutral] It says 'Initiates, [P]*absolutely*[P] do not pull this lever.[P] It will break all the other levers and force all the gates open, permanently.[P] We will be defenseless!'[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Surprise] [P].[P].[P].[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Surprise] MINE![B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Surprise] NO![P] MINE!") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Random roll. Who will win?
local iRoll = LM_GetRandomNumber(0, 100)

--Low roll, Mei wins!
if(iRoll <= 50) then
	
	--Florentina moves onto Mei. Mei moves slightly ahead.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (31.25 * gciSizePerTile), (4.50 * gciSizePerTile), 2.5)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (31.75 * gciSizePerTile), (4.50 * gciSizePerTile), 1.5)
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--Mei turns and smacks Florentina!
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Face", -1, 0)
	DL_PopActiveObject()
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Special Frame", "Wounded")
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--Florentina moves south a bit. Mei moves to the switch.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (31.75 * gciSizePerTile), (5.00 * gciSizePerTile), 0.25)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (32.25 * gciSizePerTile), (4.50 * gciSizePerTile), 2.5)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Face", 0, -1)
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	
	--Mei hits the switch and breaks everything.
	fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
	fnCutscene("AL_SetProperty(\"Switch State\", \"BreakSwitch\", true) ")
	fnCutsceneBlocker()
	fnCutsceneWait(30)
	fnCutsceneBlocker()
	
	--All the doors open.
	fnCutscene([[ AL_SetProperty("Open Door", "Door00") ]])
	fnCutscene([[ AL_SetProperty("Open Door", "Door01") ]])
	fnCutscene([[ AL_SetProperty("Open Door", "Door02") ]])
	fnCutscene([[ AL_SetProperty("Open Door", "Door03") ]])
	fnCutscene([[ AL_SetProperty("Open Door", "Door04") ]])
	fnCutscene([[ AL_SetProperty("Open Door", "Door05") ]])
	fnCutscene([[ AL_SetProperty("Open Door", "Door06") ]])
	fnCutscene([[ AL_SetProperty("Open Door", "Door07") ]])
	fnCutscene([[ AL_SetProperty("Open Door", "Door08") ]])
	fnCutscene([[ AL_SetProperty("Open Door", "Door09") ]])
	fnCutscene([[ AL_SetProperty("Open Door", "Door10") ]])
	
	--Sounds.
	fnCutscene([[ AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	fnCutscene([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	fnCutscene([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	fnCutscene([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(3)
	fnCutsceneBlocker()
	fnCutscene([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(3)
	fnCutsceneBlocker()
	fnCutscene([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(3)
	fnCutsceneBlocker()
	fnCutscene([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(3)
	fnCutsceneBlocker()
	fnCutscene([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(3)
	fnCutsceneBlocker()
	fnCutscene([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(3)
	fnCutsceneBlocker()
	fnCutscene([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(3)
	fnCutsceneBlocker()
	fnCutscene([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(3)
	fnCutsceneBlocker()

	fnCutsceneWait(120)
	fnCutsceneBlocker()
	
	--Florentina gets back up.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Special Frame", "Crouch")
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", 0, 1)
	DL_PopActiveObject()
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Special Frame", "Null")
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Face", -1, 1)
	DL_PopActiveObject()
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Florentina faces Mei.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", 1, -1)
	DL_PopActiveObject()
	fnCutsceneBlocker()

	-- |[Dialogue]|
	--Setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
	
	--Talking.
	fnCutscene([[ Append("Mei:[E|Surprise] I'm sorry![P] Are you hurt?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Blush] Just my ego.[P] But, I get the next one![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] Okay, okay.[P] Let's get going!") ]])
	fnCutsceneBlocker()
	
	-- |[System]|
	--Move Florentina onto Mei.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (32.25 * gciSizePerTile), (4.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()

	--Fold the party.
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()

--Florentina wins.
else
	--Florentina runs over Mei. Mei is knocked southwards.
	fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (32.25 * gciSizePerTile), (4.50 * gciSizePerTile), 2.5)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Special Frame", "Wounded")
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (31.75 * gciSizePerTile), (5.50 * gciSizePerTile), 0.50)
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--Florentina turns north and flips the switch.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", 0, -1)
	DL_PopActiveObject()
	
	--Wait a bit.
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	
	--Florentina hits the switch and breaks everything.
	fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
	fnCutscene("AL_SetProperty(\"Switch State\", \"BreakSwitch\", true) ")
	fnCutsceneBlocker()
	fnCutsceneWait(30)
	fnCutsceneBlocker()
	
	--All the doors open.
	fnCutscene([[ AL_SetProperty("Open Door", "Door00") ]])
	fnCutscene([[ AL_SetProperty("Open Door", "Door01") ]])
	fnCutscene([[ AL_SetProperty("Open Door", "Door02") ]])
	fnCutscene([[ AL_SetProperty("Open Door", "Door03") ]])
	fnCutscene([[ AL_SetProperty("Open Door", "Door04") ]])
	fnCutscene([[ AL_SetProperty("Open Door", "Door05") ]])
	fnCutscene([[ AL_SetProperty("Open Door", "Door06") ]])
	fnCutscene([[ AL_SetProperty("Open Door", "Door07") ]])
	fnCutscene([[ AL_SetProperty("Open Door", "Door08") ]])
	fnCutscene([[ AL_SetProperty("Open Door", "Door09") ]])
	fnCutscene([[ AL_SetProperty("Open Door", "Door10") ]])
	
	--Sounds.
	fnCutscene([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	fnCutscene([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	fnCutscene([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	fnCutscene([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(3)
	fnCutsceneBlocker()
	fnCutscene([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(3)
	fnCutsceneBlocker()
	fnCutscene([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(3)
	fnCutsceneBlocker()
	fnCutscene([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(3)
	fnCutsceneBlocker()
	fnCutscene([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(3)
	fnCutsceneBlocker()
	fnCutscene([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(3)
	fnCutsceneBlocker()
	fnCutscene([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(3)
	fnCutsceneBlocker()
	fnCutscene([[AudioManager_PlaySound("World|RemoteDoor")]])
	fnCutsceneWait(3)
	fnCutsceneBlocker()

	fnCutsceneWait(120)
	fnCutsceneBlocker()
	
	--Mei gets back up.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Special Frame", "Crouch")
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Face", 0, 1)
	DL_PopActiveObject()
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Special Frame", "Null")
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Face", 1, -1)
	DL_PopActiveObject()
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Florentina faces Mei.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", -1, 1)
	DL_PopActiveObject()
	fnCutsceneBlocker()

	-- |[Dialogue]|
	--Setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
	
	--Talking.
	fnCutscene([[ Append("Florentina:[E|Happy] Yeah,[P] suck it,[P] cultists![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Hey![P] I wanted to do it![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] Destiny waits not for the slow.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Aww.[P] Can I get the next one then?[P] I want to ruin someone's day![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Well, since you asked so nicely,[P] okay.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] All right.[P] Back to work!") ]])
	fnCutsceneBlocker()
	
	-- |[System]|
	--Move Florentina onto Mei.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (32.25 * gciSizePerTile), (4.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()

	--Fold the party.
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
end
