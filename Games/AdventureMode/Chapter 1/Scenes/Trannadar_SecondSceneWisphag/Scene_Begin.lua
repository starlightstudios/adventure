-- |[ ============================ Trannadar Second Scene: Wisphag ============================= ]|
--If Nadia first met Mei as a human, and then Mei entered as a wisphag...
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarSecondScene", "N", 1.0)

-- |[Setup]|
local iFlorentinaPresent = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")

--Determine which side the player entered from.
local bIsPlayerAtBottom = false
local bIsPlayerAtTop = false
EM_PushEntity("Adventure Party", 0)
	local iPlayerX, iPlayerY = TA_GetProperty("Position")
DL_PopActiveObject()
if(iPlayerY >= 37 * 16) then bIsPlayerAtBottom = true end
if(iPlayerY <   7 * 16) then bIsPlayerAtTop = true end

--Wait a bit for the fade.
fnCutsceneWait(15)
fnCutsceneBlocker()

-- |[Movement]|
--Mei walks towards Nadia. This is based on where she spawned.
if(bIsPlayerAtBottom) then
    fnCutsceneMove("Mei", 28.25, 38.50)
    if(iFlorentinaPresent == 1) then
        fnCutsceneMove("Florentina", 28.25, 39.50)
    end
elseif(bIsPlayerAtTop) then
    fnCutsceneMove("Mei", 23.25, 6.50)
    if(iFlorentinaPresent == 1) then
        fnCutsceneMove("Florentina", 24.25, 6.50)
    end
else
    fnCutsceneMove("Mei", 43.25, 28.50)
    if(iFlorentinaPresent == 1) then
        fnCutsceneMove("Florentina", 44.25, 28.50)
    end
end
fnCutsceneBlocker()

--Change the music.
fnCutscene([[ AudioManager_PlayMusic("NadiasTheme") ]])

--Wait a bit.
fnCutsceneWait(5)
fnCutsceneBlocker()

-- |[Dialogue]|
fnStandardMajorDialogue()
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Nadia", "Neutral") ]])
fnCutscene([[ Append("Nadia: Mei?[P] Oh dear, what happened?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] What do you mean?[B][C]") ]])
fnCutscene([[ Append("Nadia: You have a chunk of flame following your algae-like hair around.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Angry] I don't need your approval to style my hair.[B][C]") ]])
fnCutscene([[ Append("Nadia: That isn't wh [P][C]") ]])
fnCutscene([[ Append("Mei:[E|Laugh] Ha ha ha![P] No I got turned into a wisphag.[P] That flame is a part of my soul.[B][C]") ]])
fnCutscene([[ Append("Nadia: That isn't answering my question at all![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Nadia I directly and specifically answered your question.[P] What happened to me is I got turned into a wisphag.[P] It's pretty neat.[B][C]") ]])
fnCutscene([[ Append("Nadia: And you're okay with that?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] People keep asking me that.[P] Yes.[B][C]") ]])
fnCutscene([[ Append("Nadia: If you're happy, I'm happy, I suppose.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] (Here it comes...)[B][C]") ]])
fnCutscene([[ Append("Nadia: So if that flame is part of your soul...[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] (Brace for impact...)[B][C]") ]])
fnCutscene([[ Append("Nadia: Does that mean it's the light of your life?[P] Ha ha ha![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Angry] I swear I'm going to go feral one of these days...[B][C]") ]])
fnCutscene([[ Append("Nadia: Oh don't be a spoilsport.[P] You're the best looking and friendliest wisphag I've ever met, so clearly you're doing something right.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Smirk] That doesn't make up for the pun but thank you, Nadia.") ]])
fnCutsceneBlocker()

-- |[Finish Up]|
fnCutscene([[ AudioManager_PlayMusic("TownTheme") ]])
fnCutsceneBlocker()

fnAutoFoldParty()
fnCutsceneBlocker()
