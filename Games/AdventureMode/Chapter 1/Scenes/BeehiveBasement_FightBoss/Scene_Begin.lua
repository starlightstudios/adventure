-- |[BeehiveBasement Boss]|
--Mei and Florentina have battled their way into the boss chamber. Time to fight the boss!
-- This cutscene is triggered by a floor trigger, not the debug menu.

-- |[Variables]|
--Modifies some of the script dialogue.
local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")

local iZombeeQuestState = VM_GetVar("Root/Variables/Chapter1/Scenes/iZombeeQuestState", "N")
if(iZombeeQuestState < 3.0) then
    VM_SetVar("Root/Variables/Chapter1/Scenes/iZombeeQuestState", "N", 3.0)
end

-- |[Movement]|
--Move the party up to the cultist.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (12.75 * gciSizePerTile), (12.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (13.75 * gciSizePerTile), (12.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Dialogue]|
--Dialogue setup.
fnStandardMajorDialogue()
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Xanna", "Neutral") ]])

--Talking.
fnCutscene([[ Append("Florentina:[E|Happy] Hey look, Mei![P] A dead cultist![B][C]") ]])
fnCutscene([[ Append("Cultist:[E|Upset] ![B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] Has nobody informed you?[P] Allow me to be the first.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] You're dead.[P] C'mere.[B][C]") ]])

--If Mei is a bee:
if(sMeiForm == "Bee") then
	fnCutscene([[ Append("Cultist:[E|Upset] Drone, silence this Alraune interloper![P] How did you allow her into the sanctum?[B][C]") ]])
	fnCutscene([[ Append("Cultist:[E|Upset] ...[B][C]") ]])
	fnCutscene([[ Append("Cultist:[E|Neutral] Drone?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] I don't take orders from dead people.[B][C]") ]])
	fnCutscene([[ Append("Cultist:[E|Upset] Y-[P]you...[B][C]") ]])
	fnCutscene([[ Append("Cultist:[E|Upset] You bear the rune...[P] Just like the sister superior said![B][C]") ]])
	fnCutscene([[ Append("Cultist:[E|Upset] Impossible![P] The rune bearers - [B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Really chatty,[P] for a corpse.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] Let's shut her up.[B][C]") ]])
	fnCutscene([[ Append("Cultist:[E|Upset] Drones, assist me![P] When I bring the runebearer's head...[P] I will be rewarded!") ]])
	fnCutsceneBlocker()

--All other forms:
else
	fnCutscene([[ Append("Cultist:[E|Upset] Interlopers![P] How did you get in here?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Through the...[P] door?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] Your security sucks.[B][C]") ]])
	fnCutscene([[ Append("Cultist:[E|Upset] Y-[P]you...[B][C]") ]])
	fnCutscene([[ Append("Cultist:[E|Upset] You bear the rune...[P] Just like the sister superior said![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] This old thing?[B][C]") ]])
	fnCutscene([[ Append("Cultist:[E|Upset] Impossible![P] The rune bearers - [B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] Really chatty,[P] for a corpse.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] Let's shut her up.[B][C]") ]])
	fnCutscene([[ Append("Cultist:[E|Upset] Drones, assist me![P] When I bring the runebearer's head...[P] I will be rewarded!") ]])
	fnCutsceneBlocker()

end

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Boss battle!
fnCutscene([[
	AdvCombat_SetProperty("World Pulse", true)
    AdvCombat_SetProperty("Next Combat Music", "RottenTheme", 0.0000)
	AdvCombat_SetProperty("Reinitialize")
	AdvCombat_SetProperty("Activate")
	AdvCombat_SetProperty("Unretreatable", true)
	LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/000 Scripted Enemies/Zombee Special.lua", 0)
	LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/000 Scripted Enemies/Boss Cultist Corrupter.lua", 0)
	LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/000 Scripted Enemies/Zombee Special.lua", 0)
	AdvCombat_SetProperty("Victory Script", gsRoot .. "Chapter 1/Scenes/BeehiveBasement_FightBoss/Combat_Victory.lua")
	AdvCombat_SetProperty("Defeat Script", gsRoot .. "Chapter 1/Scenes/200 Defeat/Defeat_Zombee/Scene_Begin.lua")
]])
fnCutsceneBlocker()
