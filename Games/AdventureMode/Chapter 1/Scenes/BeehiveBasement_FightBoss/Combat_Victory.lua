-- |[Combat Victory]|
--The party won!

-- |[Setup]|
--Flag.
VM_SetVar("Root/Variables/Chapter1/Scenes/iSavedBeehive", "N", 1.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iZombeeQuestState", "N", 4.0)

--Darken the screen.
AL_SetProperty("Activate Fade", 30, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1)
AudioManager_PlayMusic("Null")

-- |[Achievement]|
AM_SetPropertyJournal("Unlock Achievement", "FinishZombee")

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])

--Talking.
fnCutscene([[ Append("Florentina:[E|Happy] We -[P] we won![P] Yeah![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Laugh] The outcome was never really in doubt.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] But, what about the honey? [P]The bees?[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] With the source of corruption gone, I'm sure the bees can take care of the rest.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Hm, what if they don't know how?[P] What if they don't know how to fix this?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] ...[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Happy] I just got a good idea.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] I like that smile on your face.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] She's still breathing.[P] Grab her arms.[P] Try not to hurt her.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] No promises.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] Quick, before the rest of the bees recover![P] C'mon!") ]])
fnCutsceneBlocker()

--Wait a while.
fnCutsceneWait(120)
fnCutsceneBlocker()

--Dialogue resumes.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Happy") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Happy") ]])

--Talking.
fnCutscene([[ Append("Mei:[SOUND|World|HardHit][E|Offended][P] Hey![P] Don't bang her head against the wall![B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] Oh, so...[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy][SOUND|World|HardHit][P]*Don't*[P] do that?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] We need her alive!") ]])
fnCutsceneBlocker()

--Wait a while.
fnCutsceneWait(120)
fnCutsceneBlocker()

-- |[Transition]|
--Scene resumes on the top floor of the beehive.
fnCutscene([[ AL_BeginTransitionTo("BeehiveInner", gsRoot .. "Chapter 1/Scenes/BeehiveBasement_FightBoss/Victory_PostTransition.lua") ]])
fnCutsceneBlocker()
