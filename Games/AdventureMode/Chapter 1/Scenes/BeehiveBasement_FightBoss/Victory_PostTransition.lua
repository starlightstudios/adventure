-- |[Scene Post-Transition]|
--Mei and Florentina have dragged the cultist to the upper part of the beehive.

-- |[Variables]|
local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local iHasBeeForm = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")

-- |[Unlock Costume]|
VM_SetVar("Root/Variables/Costumes/Mei/iQueenBee", "N", 1.0)

-- |[Setup]|
--Fade back in.
AL_SetProperty("Music", "ForestTheme")
AL_SetProperty("Activate Fade", 120, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 0)

--Reposition Florentina.
EM_PushEntity("Florentina")
	TA_SetProperty("Position", 14, 17)
	TA_SetProperty("Facing", gci_Face_East)
DL_PopActiveObject()

--Reposition Mei.
EM_PushEntity("Mei")
	TA_SetProperty("Position", 16, 17)
	TA_SetProperty("Facing", gci_Face_West)
DL_PopActiveObject()

--Spawn the downed cultist here.
TA_Create("Cultist")
	TA_SetProperty("Position", 15, 17)
	TA_SetProperty("Facing", gci_Face_South)
	TA_SetProperty("Clipping Flag", false)
	fnSetCharacterGraphics("Root/Images/Sprites/CultistF/", false)
		
	--Special frames.
	TA_SetProperty("Wipe Special Frames")
	TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/CultistF|Wounded")
DL_PopActiveObject()

--Set the downed cultist to the wounded frame.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Cultist")
	ActorEvent_SetProperty("Special Frame", "Wounded")
DL_PopActiveObject()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Happy") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Happy") ]])

--Talking.
fnCutscene([[ Append("Mei:[E|Laugh] There.[P] Have at her, bees![B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] Heh.[P] They'll turn her into a bee, and then use her knowledge to purify the hive.[P] Good thinking.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] It's only fair.[B][C]") ]])

--If Mei is a bee or had bee form, her dialogue changes:
if(sMeiForm == "Bee" or iHasBeeForm == 1.0) then
	fnCutscene([[ Append("Mei:[E|Happy] Besides, now she'll have a family to love her and care for her.[P] All will be forgiven.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Whatever.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I hope the other bees can cleanse the hive.[P] It'll be hard work...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] The immediate danger is over.[P] They can handle it.[P] Might be a while before they take down all the corrupted bees, though.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] In the meantime...[B][C]") ]])

--If Mei does not have bee form:
else
	fnCutscene([[ Append("Mei:[E|Happy] I hope -[P] I hope the bees are nice to her.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Probably.[P] If not, just desserts.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Yeah.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Better not go in the hive basement for a while.[P] Taking down the corrupted bees might be a while in coming.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] In the meantime...[B][C]") ]])
	 
end

--Mei asks Florentina an important question.
fnCutscene([[ Append("Mei:[E|Neutral] Hold up a second.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Surprise] ?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] You didn't have to help.[P] I know you made up some excuse, but you didn't have to help.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] And you did.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] Oh jeez.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] Well let's get this in the open, then.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] I'm starting to like you, kid.[P] When I saw you get all worked up about those cultists, I couldn't help myself.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] You liked me getting all shouty?[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Blush] You believed in something.[P] You put passion into it.[P] You let that passion guide you.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Blush] That's what I -[P] I look for in a f-[P]friend.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Laugh] Friend![B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Surprise] B-[P]business associate![P] You're a business associate.[P] Nothing more.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Happy] Oh, of course![P] You're a cherished business associate, too![B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] I'm never going to live this down, am I?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Depends on how long we live.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Now, business associate, shall we get back to it?[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] Yeah.[P] Back to the mission.[B][C]") ]])
fnCutscene([[ Append("You have unlocked Mei's Queen Bee Costume![P] You may access it from the costumes menu at a campfire, in bee form.[B][C]") ]])
fnCutscene([[ Append("You can also unlock it on subsequent playthroughs by entering the password 'Carry Fire' on the password menu at a campfire.") ]])
fnCutsceneBlocker()

-- |[Party Fold]|
--Florentina walks onto Mei.
Cutscene_CreateEvent("Move Florentina to Mei", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (16.25 * gciSizePerTile), (17.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Fold the party positions up.
fnCutscene([[ AL_SetProperty("Fold Party") ]])
fnCutsceneBlocker()

--Trip this flag. Don't show this cutscene on the way out.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenBeesIgnoreScene", "N", 1.0)
