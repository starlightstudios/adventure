-- |[Alraune Battle Scene]|
--The Alraunes have decided that nature needs some ass kicked on its behalf! Urra!

-- |[Flags]|
--Set this flag so the scene doesn't play twice.
VM_SetVar("Root/Variables/Chapter1/Scenes/iSeenAlrauneBattleIntroScene", "N", 1.0)

--Other variables
local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local iInformedOfDungeon = VM_GetVar("Root/Variables/Chapter1/Scenes/iInformedOfDungeon", "N")

--Flag this so Nadia doesn't inform the party any more.
VM_SetVar("Root/Variables/Chapter1/Scenes/iInformedOfDungeon", "N", 1.0)

-- |[Movement]|
--Move Mei and Florentina down south a bit.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (8.75 * gciSizePerTile), (16.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (9.75 * gciSizePerTile), (16.50 * gciSizePerTile), 1.30)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "AlrauneE")
	ActorEvent_SetProperty("Move To", (9.25 * gciSizePerTile), (18.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnStandardMajorDialogue()
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])

--Talking.
fnCutscene([[ Append("Alraune: Hold, outsiders![B][C]") ]])
fnCutscene([[ Append("Alraune: ...[P] Leaf-sister Florentina?[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Offended] Don't call me leaf-sister![B][C]") ]])
fnCutscene([[ Append("Alraune: Do you answer the call?[P] Have you come to offer aid?[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Offended] I'm here of my own volition.[P] Get out of my way![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Surprise] Relax, you two!") ]])
fnCutsceneBlocker()

-- |[Movement]|
--Rochea looks over.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Rochea")
	ActorEvent_SetProperty("Face", -1, 0)
DL_PopActiveObject()
fnCutsceneWait(45)
fnCutsceneBlocker()

--Rochea comes over.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Rochea")
	ActorEvent_SetProperty("Move To", (11.25 * gciSizePerTile), (18.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Rochea faces north, Mei and Florentina face her.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Rochea")
	ActorEvent_SetProperty("Face", 0, -1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 1, 1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", 1, 1)
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnStandardMajorDialogue()
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Rochea", "Neutral") ]])

--Talking.
fnCutscene([[ Append("Rochea: Do I mistake my ears, or has leaf-sister Mei come to assist us?[B][C]") ]])

--If Mei is an Alraune:
if(sMeiForm == "Alraune") then
	fnCutscene([[ Append("Mei:[E|Happy] Leaf-sister Rochea![B][C]") ]])
	fnCutscene([[ Append("Rochea: At our time of need, you return to us.[P] Perhaps this is the destiny marked upon you?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] Hello...[P] Rochea...[B][C]") ]])
	fnCutscene([[ Append("Rochea: And so comes wanderer Florentina.[P] In dark times, we draw together.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] Spare me.[P] I'm not here to help you.[P] I'm here to help Mei.[B][C]") ]])
	if(iInformedOfDungeon == 0.0) then
		fnCutscene([[ Append("Mei:[E|Neutral] I thought maybe looking into the cultists who kidnapped me might shed some light on a way back to Earth.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] But, what is going on here?[P] Why are so many leaf-sisters here?[B][C]") ]])
		fnCutscene([[ Append("Rochea: You were not aware?[P] Hm.[B][C]") ]])
	else
		fnCutscene([[ Append("Mei:[E|Neutral] Nadia said the cultists were up to something, and that you were taking a stand against it.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] If there's any way we can help, we will.[P] Just say the word.[B][C]") ]])
		fnCutscene([[ Append("Rochea: The situation is grim indeed.[B][C]") ]])
	end
	fnCutscene([[ Append("Rochea: The little ones sent word that humans in strange robes had spread through the forest, seeking ingredients for some ritual most foul.[B][C]") ]])
	fnCutscene([[ Append("Rochea: Soon we began to feel reverberations in the earth.[P] The humans are doing something, but we do not know what.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] It can't be good.[B][C]") ]])
	fnCutscene([[ Append("Rochea: Indeed.[B][C]") ]])
	fnCutscene([[ Append("Rochea: I feared we lacked the numbers to defeat them, but with your help we may yet have a chance.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] I don't care what they're doing, we're going to stop it.[P] Nobody should have to go through what I went through.[B][C]") ]])
	fnCutscene([[ Append("Rochea: We have breached the wall here with the aid of the little ones, but they can go no further.[P] The cultists have fortified the area beyond this door.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Angry] So?[P] Let's go get them![B][C]") ]])
	fnCutscene([[ Append("Rochea: Please, calm yourself, young one.[P][EMOTION|Mei|Offended] You have not been cleansed, nor has Florentina.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] And?[B][C]") ]])
	fnCutscene([[ Append("Rochea: The steps as we dance in battle will be foreign to you.[P] You will obstruct us.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] I'll not sit on the sidelines as my leaf-sisters fight on my behalf![B][C]") ]])
	fnCutscene([[ Append("Rochea: Once we have defeated the immediate garrison, we will slow the tide of reinforcements while you and Florentina press further inside.[B][C]") ]])
	fnCutscene([[ Append("Rochea: They will be off balance and not have time to react.[P] You will strike clean and true, and stop whatever foul ritual they are perpetrating.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] Must we stand by?[B][C]") ]])
	fnCutscene([[ Append("Rochea: It is for the greater good.[P] Please understand.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended]...[P][EMOTION|Mei|Neutral] I trust your judgement, leaf-sister.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Don't matter to me none.[P] If you want to get slapped around so our job is easier, I'm not going to stop you.[B][C]") ]])
	fnCutscene([[ Append("Rochea: Very well.[P] Time is short.[P] Leaf-sisters![P] To me!") ]])
	fnCutsceneBlocker()

--If Mei is not an Alraune:
else
	fnCutscene([[ Append("Mei:[E|Happy] Leaf-sister Rochea![B][C]") ]])
	fnCutscene([[ Append("Rochea: So the little ones tell no fibs today.[P] You walk between forms...[P] Truly, a great destiny weighs upon you.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] I'm green at heart.[B][C]") ]])
	fnCutscene([[ Append("Rochea: No doubt your body still serves nature.[P] We are glad to see you well.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] Hello...[P] Rochea...[B][C]") ]])
	fnCutscene([[ Append("Rochea: And so comes wanderer Florentina.[P] In dark times, we draw together.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] Spare me.[P] I'm not here to help you.[P] I'm here to help Mei.[B][C]") ]])
	if(iInformedOfDungeon == 0.0) then
		fnCutscene([[ Append("Mei:[E|Neutral] I thought maybe looking into the cultists who kidnapped me might shed some light on a way back to Earth.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] But, what is going on here?[P] Why are so many leaf-sisters here?[B][C]") ]])
		fnCutscene([[ Append("Rochea: You were not aware?[P] Hm.[B][C]") ]])
	else
		fnCutscene([[ Append("Mei:[E|Neutral] Nadia said the cultists were up to something, and that you were taking a stand against it.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] If there's any way we can help, we will.[P] Just say the word.[B][C]") ]])
		fnCutscene([[ Append("Rochea: The situation is grim indeed.[B][C]") ]])
	end
	fnCutscene([[ Append("Rochea: The little ones sent word that humans in strange robes had spread through the forest, seeking ingredients for some ritual most foul.[B][C]") ]])
	fnCutscene([[ Append("Rochea: Soon we began to feel reverberations in the earth.[P] The humans are doing something, but we do not know what.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] It can't be good.[B][C]") ]])
	fnCutscene([[ Append("Rochea: Indeed.[B][C]") ]])
	fnCutscene([[ Append("Rochea: I feared we lacked the numbers to defeat them, but with your help we may yet have a chance.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] I don't care what they're doing, we're going to stop it.[P] Nobody should have to go through what I went through.[B][C]") ]])
	fnCutscene([[ Append("Rochea: We have breached the wall here with the aid of the little ones, but they can go no further.[P] The cultists have fortified the area beyond this door.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Angry] So?[P] Let's go get them![B][C]") ]])
	fnCutscene([[ Append("Rochea: Please, calm yourself, young one.[P][EMOTION|Mei|Offended] You have not been cleansed, nor has Florentina.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] And?[B][C]") ]])
	fnCutscene([[ Append("Rochea: The steps as we dance in battle will be foreign to you.[P] You will obstruct us.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] I'll not sit on the sidelines as my leaf-sisters fight on my behalf![B][C]") ]])
	fnCutscene([[ Append("Rochea: Once we have defeated the immediate garrison, we will slow the tide of reinforcements while you and Florentina press further inside.[B][C]") ]])
	fnCutscene([[ Append("Rochea: They will be off balance and not have time to react.[P] You will strike clean and true, and stop whatever foul ritual they are perpetrating.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] Must we stand by?[B][C]") ]])
	fnCutscene([[ Append("Rochea: It is for the greater good.[P] Please understand.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended]...[P][EMOTION|Mei|Neutral] I trust your judgement, leaf-sister.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Don't matter to me none.[P] If you want to get slapped around so our job is easier, I'm not going to stop you.[B][C]") ]])
	fnCutscene([[ Append("Rochea: Very well.[P] Time is short.[P] Leaf-sisters![P] To me!") ]])
	fnCutsceneBlocker()

end

--Fade to black. Cut the music.
fnCutscene([[ AL_SetProperty("Music", "Null") ]])
fnCutscene([[ AL_SetProperty("Activate Fade", 30, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneWait(120)
fnCutsceneBlocker()

-- |[Reposition]|
--Move all the Alraunes off the field. This includes Rochea.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "AlrauneA")
	ActorEvent_SetProperty("Teleport To", -100, -100)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "AlrauneB")
	ActorEvent_SetProperty("Teleport To", -100, -100)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "AlrauneC")
	ActorEvent_SetProperty("Teleport To", -100, -100)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "AlrauneD")
	ActorEvent_SetProperty("Teleport To", -100, -100)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "AlrauneE")
	ActorEvent_SetProperty("Teleport To", -100, -100)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Rochea")
	ActorEvent_SetProperty("Teleport To", -100, -100)
DL_PopActiveObject()

--Move Mei and Florentina to this position.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Teleport To", (15.25 * gciSizePerTile), (17.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Teleport To", (14.25 * gciSizePerTile), (17.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Face the two south.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()
fnCutsceneBlocker()

--Fade back in. Start the din of battle.
fnCutscene([[ AL_SetProperty("Music", "BattleAmbience") ]])
fnCutscene([[ AL_SetProperty("Activate Fade", 150, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneWait(120)
fnCutsceneBlocker()

--At this point, start the sound effect. It has a specific play time. The cumulative total needs to be (about) 14.5 seconds.
fnCutscene([[ AudioManager_PlaySound("Special|War") ]])
fnCutsceneWait(1.5 * 60)
fnCutsceneBlocker()

--Mei looks around.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 1, -1)
DL_PopActiveObject()
fnCutsceneWait(0.9 * 60)
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 1, 1)
DL_PopActiveObject()
fnCutsceneWait(0.1 * 60)
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()

--Wait 5 seconds.
fnCutsceneWait(2.0 * 60)
fnCutsceneBlocker()

--Mei and Florentina look at each other.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", -1, 0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", 1, 0)
DL_PopActiveObject()
fnCutsceneWait(5.0 * 60)
fnCutsceneBlocker()

--Look towards the battle.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 1, -1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", 1, -1)
DL_PopActiveObject()
fnCutsceneWait(2.0 * 60)
fnCutsceneBlocker()

--Look south for now.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()
fnCutsceneWait(3.0 * 60)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])

--Talking.
fnCutscene([[ Append("Mei:[E|Neutral] Sounds like they're having fun in there...") ]])
fnCutsceneBlocker()
	
-- |[Movement]|
--Remove this door, spawn a cultist NPC.
fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
fnCutscene([[ AL_SetProperty("Open Door", "ToDungeonB") ]])
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Cultist")
	ActorEvent_SetProperty("Teleport To", (16.25 * gciSizePerTile), (16.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()

--Mei and Florentina look on with disinterest.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 1, 1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", 1, 1)
DL_PopActiveObject()

--Cultist runs south.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Cultist")
	ActorEvent_SetProperty("Move To", (16.25 * gciSizePerTile), (19.50 * gciSizePerTile), 2.5)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Cultist")
	ActorEvent_SetProperty("Move To", (16.25 * gciSizePerTile), (20.00 * gciSizePerTile), 0.75)
DL_PopActiveObject()
fnCutsceneBlocker()

--Cultist falls over.
fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Cultist")
	ActorEvent_SetProperty("Special Frame", "Wounded")
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Cultist")
	ActorEvent_SetProperty("Move To", (16.25 * gciSizePerTile), (20.10 * gciSizePerTile), 0.25)
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneWait(180)
fnCutsceneBlocker()

--Alraune appears and picks her up.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "AlrauneE")
	ActorEvent_SetProperty("Teleport To", (16.25 * gciSizePerTile), (16.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()

--Alraune walks south.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "AlrauneE")
	ActorEvent_SetProperty("Move To", (16.25 * gciSizePerTile), (19.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--Alraune walks back north. Cultist follows still in wounded state, so it looks like a drag.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "AlrauneE")
	ActorEvent_SetProperty("Move To", (16.25 * gciSizePerTile), (17.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Cultist")
	ActorEvent_SetProperty("Move To", (16.25 * gciSizePerTile), (18.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Mei and Florentina face the Alraune.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 1, 0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", 1, 0)
DL_PopActiveObject()

--Alraune pauses a moment.
fnCutsceneWait(120)
fnCutsceneBlocker()

--Alraune faces Mei and Florentina.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "AlrauneE")
	ActorEvent_SetProperty("Face", -1, 0)
DL_PopActiveObject()
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])

--Talking.
fnCutscene([[ Append("Alraune: Apologies.") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(60)
fnCutsceneBlocker()

-- |[Movement]|
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "AlrauneE")
	ActorEvent_SetProperty("Move To", (16.25 * gciSizePerTile), (16.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Cultist")
	ActorEvent_SetProperty("Move To", (16.25 * gciSizePerTile), (17.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "AlrauneE")
	ActorEvent_SetProperty("Teleport To", (-100 * gciSizePerTile), (-100 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Cultist")
	ActorEvent_SetProperty("Move To", (16.25 * gciSizePerTile), (16.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Cultist")
	ActorEvent_SetProperty("Teleport To", (-100 * gciSizePerTile), (-100 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--Mei and Florentina face away.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()

--Fade to black.
fnCutscene([[ AL_SetProperty("Activate Fade", 300, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneWait(300)
fnCutsceneBlocker()

--Hold on black.
fnCutsceneWait(180)
fnCutsceneBlocker()

--Fade to normal.
fnCutscene([[ AL_SetProperty("Close Door", "ToDungeonB") ]])
fnCutscene([[ AL_SetProperty("Activate Fade", 120, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()

--Back to normal music.
fnCutscene([[ AL_SetProperty("Music", "TheyKnowWeAreHere") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])

--Talking.
fnCutscene([[ Append("Voice: Please, come.") ]])
fnCutsceneBlocker()

-- |[System]|
--Move Florentina onto Mei and fold the party.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (15.25 * gciSizePerTile), (17.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Fold the party positions up.
fnCutscene([[ AL_SetProperty("Fold Party") ]])
fnCutsceneBlocker()
