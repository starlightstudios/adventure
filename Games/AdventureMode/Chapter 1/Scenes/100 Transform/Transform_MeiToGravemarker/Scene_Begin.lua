-- |[ ============================= Transform: Mei to Gravemarker ============================== ]|
--Used at save points to transform, handles blocking cases.

-- |[ ======= Blocking Cases ======= ]|
--Block transformations on the way back from the beehive.
local bBlockTransformation = false
local iHasSeenOutlandBeeScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenOutlandBeeScene", "N")
local iHasBeeForm = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")
if(iHasSeenOutlandBeeScene == 0.0 and iHasBeeForm == 1.0) then
	fnStandardMajorDialogue()
	fnCutscene([[ Append("Mei:[E|Neutral] (Strange, something seems to be stopping me from transforming...)") ]])
	fnCutsceneBlocker()
	return
end

--During the mannequin sequence, transformations are blocked.
local iBeganSequence  = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iBeganSequence", "N")
local iDefeatedLeader = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iDefeatedLeader", "N")
if(iBeganSequence == 1.0 and iDefeatedLeader == 0.0) then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (No runestone.[P] Not allowed.)") ]])
    fnCutsceneBlocker()
    return
end

-- |[ ========= Execution ========== ]|
-- |[Variables]|
--Store which form Mei started the scene in, among other things.
local sStartingForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")

-- |[Achievement]|
AM_SetPropertyJournal("Unlock Achievement", "BecomeGravemarker")

-- |[Execute Transformation]|
--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Flash the active character to white. Immediately after, execute the transformation.
Cutscene_CreateEvent("Flash Mei White", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Flashwhite Quickly")
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Gravemarker.lua") ]])
fnCutsceneWait(gci_Flashwhite_Ticks_Total)
fnCutsceneBlocker()

--Now wait a little bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Cutscene Execution]|
--If Florentina is present, and has not seen Mei transform before, this scene plays. It's mostly the same for all forms.
local bIsFlorentinaPresent = AL_GetProperty("Is Character Following", "Florentina")
local iFlorentinaKnowsAboutRune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")
if(bIsFlorentinaPresent == true and iFlorentinaKnowsAboutRune == 0.0) then

	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N", 1.0)
	
	--Setup. Florentina is opposite to Mei.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
	fnCutscene([[ Append("Florentina:[E|Surprise] What the -[P] is this a sting!?[P] Mei, are you some kinda stone angel hitman?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Angry] Prepare to die in the name of the holy chorus, filthy degenerate![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Cry] Ngggghhh no![P] No![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] Damn it![P] I'm sorry, Florentina.[P] I failed myself there.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] Calm down and tell me what's going on.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] My runestone lets me transform myself into any form I've been in before.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] So you were one of these stone angels before?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Sorry, I didn't mean to hide it from you.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] When they turned me, they filled me with holy light.[P] But I escaped before they could make some...[P] permanent changes.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] But the light is still within me, and I lost control for a second there.[P] I'm sorry.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] I can't believe I let it get the best of me...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] It's okay, kid.[P] You've got it under control now.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] So...[P] you think we can maybe use this as a disguise to con a few angels?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Laugh] Ha ha![P] I wish![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] I'd never betray the holy chorus.[P] I - [B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] Nggghh...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] You okay?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] The light doesn't command me, but it does give me...[P] advice.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] I like to think it makes me a better person.[P] So I'm going to say we shouldn't con anyone, let alone an angel.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] After what they did to you?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] The gravemarkers have no intelligence of their own, they were merely protecting the place they were told to protect.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I crossed them, and they did what they were made to.[P] You may as well turn your ire to a storm on the horizon.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] Have you ever met another angel?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] No, but I'd like to...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] Then reserve your judgement for when you actually meet one.[P] I guarantee they aren't who you think.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] How could such pious people be bad?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] Oh kiddo, you're in for such a rude awakening...") ]])

	--Common.
	fnCutsceneBlocker()
	return
end

--Roll a random number. There is a 25% chance that a scene will play, otherwise the transformation is instant and no scenes play.
local iRoll = LM_GetRandomNumber(1, 100)
if(iRoll <= 75) then
	return
end

--Scenes that are independent of form.
if(true) then
	
end
