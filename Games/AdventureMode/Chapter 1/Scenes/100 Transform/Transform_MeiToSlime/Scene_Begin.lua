-- |[ ================================ Transform: Mei to Slime ================================= ]|
--Used at save points to transform, handles blocking cases.

-- |[ ======= Blocking Cases ======= ]|
--Block transformations on the way back from the beehive.
local bBlockTransformation = false
local iHasSeenOutlandBeeScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenOutlandBeeScene", "N")
local iHasBeeForm = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")
if(iHasSeenOutlandBeeScene == 0.0 and iHasBeeForm == 1.0) then
	fnStandardMajorDialogue()
	fnCutscene([[ Append("Mei:[E|Neutral] (Strange, something seems to be stopping me from transforming...)") ]])
	fnCutsceneBlocker()
	return
end

--During the mannequin sequence, transformations are blocked.
local iBeganSequence  = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iBeganSequence", "N")
local iDefeatedLeader = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iDefeatedLeader", "N")
if(iBeganSequence == 1.0 and iDefeatedLeader == 0.0) then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (No runestone.[P] Not allowed.)") ]])
    fnCutsceneBlocker()
    return
end

-- |[ ========= Execution ========== ]|
-- |[Variables]|
--Store which form Mei started the scene in.
local sStartingForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")

-- |[Achievement]|
AM_SetPropertyJournal("Unlock Achievement", "BecomeSlime")

-- |[Execute Transformation]|
--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Flash the active character to white. Immediately after, execute the transformation.
Cutscene_CreateEvent("Flash Mei White", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Flashwhite Quickly")
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Slime.lua") ]])
fnCutsceneWait(gci_Flashwhite_Ticks_Total)
fnCutsceneBlocker()

--Now wait a little bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Cutscene Execution]|
--If Florentina is present, and has not seen Mei transform before, this scene plays. It's mostly the same for all forms.
local bIsFlorentinaPresent = AL_GetProperty("Is Character Following", "Florentina")
local iFlorentinaKnowsAboutRune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")
if(bIsFlorentinaPresent == true and iFlorentinaKnowsAboutRune == 0.0) then

	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N", 1.0)
	
	--Setup. Florentina is opposite to Mei.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
	
	--Dialogue.
	fnCutscene([[ Append("Florentina:[E|Surprise] Well, isn't that a neat trick.[P] How'd you do that?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] It's the power of my runestone.[P] I just focus on being squishy...[P] and...[B][C]") ]])
	fnCutscene([[ Append("*squish*[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] Oooooohhhh.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Surprise] I get the feeling I shouldn't have seen that.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] You're...[P] welcome to join me...[P] *squish*[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Offended] Get hold of yourself, kid![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] S-[P]sorry.[P] It's a very powerful urge...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] Is this some sort of magic?[P] How does it work?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I don't know, not consciously.[P] I can feel it, and I just need to concentrate.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] If you're going to use it for something other than getting off, it could be handy.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Blush] But, if you need some time alone, I can go get lost for a bit.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] No.[P] We can keep going...") ]])
	fnCutsceneBlocker()
	return
end

--Roll a random number. There is a 25% chance that a scene will play, otherwise the transformation is instant and no scenes play.
local iRoll = LM_GetRandomNumber(1, 100)
if(iRoll <= 75 and false) then
	return
end


--Scenes that are independent of form.
if(true) then
	
	--Scene Variables.
	local iSquish = VM_GetVar("Root/Variables/Global/Mei/Cutscene|Slime|iSquish", "N")
	
	--Mei, uh, well, you know. Squish.
	if(iSquish == 0.0) then
		
		--Flip the variable.
		VM_SetVar("Root/Variables/Global/Mei/Cutscene|Slime|iSquish", "N", 1.0)
		
		--Setup.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		if(bIsFlorentinaPresent == true) then
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		end
		
		--Dialogue.
		fnCutscene([[ Append("Mei: Ahhhhhhhh...[B][C]") ]])
		if(bIsFlorentinaPresent == true) then
			fnCutscene([[ Append("Florentina:[E|Surprise] Are you...?[B][C]") ]])
		end
		fnCutscene([[ Append("*squish*[B][C]") ]])
		fnCutscene([[ Append("Mei: This is the best![B][C]") ]])
		fnCutscene([[ Append("*squish*[B][C]") ]])
		
		--Florentina is not present.
		if(bIsFlorentinaPresent == false) then
			fnCutscene([[ Append("Mei: F-[P]focus...") ]])
		
		--If Florentina is present, she comments.
		else
			fnCutscene([[ Append("Florentina:[E|Offended] Gross and weird![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Happy] Heh heh...[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Offended] Control yourself, damn you!") ]])
		end
		fnCutsceneBlocker()
		fnCutsceneBlocker()
		
	end
	
end
