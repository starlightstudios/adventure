-- |[ =========================== Transform: Florentina to Mushraune =========================== ]|
--Used at save points to transform, handles blocking cases.

-- |[ ======= Blocking Cases ======= ]|
--During the mannequin sequence, transformations are blocked.
local iBeganSequence  = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iBeganSequence", "N")
local iDefeatedLeader = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iDefeatedLeader", "N")
if(iBeganSequence == 1.0 and iDefeatedLeader == 0.0) then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Florentina] (Not allowed.)") ]])
    fnCutsceneBlocker()
    return
end

-- |[ ========= Execution ========== ]|
-- |[Variables]|
--Store which form Mei started the scene in, among other things.
local sStartingForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")

-- |[Achievement]|
-- |[Execute Transformation]|
--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()
fnCutscenePlaySound("World|TakeItem")
fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Florentina/Job_Agarist.lua") ]])
fnCutsceneWait(25)
fnCutsceneBlocker()

-- |[Cutscene Execution]|
--If Mei hasn't seen Florentina TF before (NC+ only) then she comments on that.
local iMeiKnowsFlorentinaTFs = VM_GetVar("Root/Variables/Global/Mei/iMeiKnowsFlorentinaTFs", "N")
local iFlorentinaKnowsAboutRune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")
if(iMeiKnowsFlorentinaTFs == 0.0) then

	--Flags.
    VM_SetVar("Root/Variables/Global/Mei/iMeiKnowsFlorentinaTFs", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N", 1.0)
	
	--Setup. Florentina is opposite to Mei.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
	
	fnCutscene([[ Append("Mei:[E|Happy] Woah![P] Florentina, you didn't tell me you could transform too![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] Oh, this? Yeah, a partner of mine asked to hitch a ride.[P] Wants to see the world.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] It's a helpful business transaction.[P] I get a disguise I can use at any time, plus some handy fungal powers, and he gets to move around and learn how to deal with people in a fair and equitable manner.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] That's great to hear![P] Hello, buddy![P] Nice to meet you![B][C]") ]])
    if(iFlorentinaKnowsAboutRune == 0.0) then
        fnCutscene([[ Append("Florentina:[E|Neutral] You got a fungal buddy in there, too?[P] I think I can get you a cream for that.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] No, I can transform with my runestone, too.[P] I don't know how I picked that up, but I can.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Transform, like, not just unfurl a fungus hiding on you?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] I literally change forms.[P] So...[P] not like what you just did.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] My mind is abuzz with all the things we can get away with...[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Offended] Keep it in your panties, Florentina.[P] Burglaries come second to finding me a way home.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Just means I need to find a way to make getting you home depend on a burglary.") ]])
    else
        fnCutscene([[ Append("Florentina:[E|Neutral] Not quite, this fellow was always here, just hidden.[P] Still, he's handy to have around.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Happy] See, you have a lot of friends you can rely on![B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Associates, Mei.[P] I have associates.") ]])
    end

	--Common.
	fnCutsceneBlocker()
	return
end
