-- |[ ====================================== Transform: Mei to Alraune ====================================== ]|
--Used at save points to transform, handles blocking cases.

-- |[ ======= Blocking Cases ======= ]|
--Block transformations on the way back from the beehive.
local bBlockTransformation = false
local iHasSeenOutlandBeeScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenOutlandBeeScene", "N")
local iHasBeeForm = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")
if(iHasSeenOutlandBeeScene == 0.0 and iHasBeeForm == 1.0) then
	fnStandardMajorDialogue()
	fnCutscene([[ Append("Mei:[E|Neutral] (Strange, something seems to be stopping me from transforming...)") ]])
	fnCutsceneBlocker()
	return
end

--During the mannequin sequence, transformations are blocked.
local iBeganSequence  = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iBeganSequence", "N")
local iDefeatedLeader = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iDefeatedLeader", "N")
if(iBeganSequence == 1.0 and iDefeatedLeader == 0.0) then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (No runestone.[P] Not allowed.)") ]])
    fnCutsceneBlocker()
    return
end

-- |[ ========= Execution ========== ]|
-- |[Variables]|
--Store which form Mei started the scene in, among other things.
local sStartingForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local iFlorentinaSpecialAlraune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaSpecialAlraune", "N")

-- |[Achievement]|
AM_SetPropertyJournal("Unlock Achievement", "BecomeAlraune")
    
-- |[Execute Transformation]|
--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Flash the active character to white. Immediately after, execute the transformation.
Cutscene_CreateEvent("Flash Mei White", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Flashwhite Quickly")
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Alraune.lua") ]])
fnCutsceneWait(gci_Flashwhite_Ticks_Total)
fnCutsceneBlocker()

--Now wait a little bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Cutscene Execution]|
--If Florentina is present, and has not seen Mei transform before, this scene plays. It's mostly the same for all forms.
local bIsFlorentinaPresent = AL_GetProperty("Is Character Following", "Florentina")
local iFlorentinaKnowsAboutRune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")
if(bIsFlorentinaPresent == true and iFlorentinaKnowsAboutRune == 0.0) then

	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N", 1.0)
	
	--Setup. Florentina is opposite to Mei.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
	
	--Special: If this is coming from a chat with Florentina:
	if(iFlorentinaSpecialAlraune == 1.0) then
		
		--Activate topics with Florentina when this ends.
		fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Florentina") ]])
		
		--Dialogue.
		fnCutscene([[ Append("Florentina:[E|Surprise] Well I'll be stuffed.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] My runestone lets me do this.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Surprise] So that's how you know Rochea...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Surprise] She joined you...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] Yes, she did.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] And the runestone lets you turn back into a human?[P] How?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I'm sorry, but I don't know.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Surprise] Incredible.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Yes, I know Rochea.[P] We don't get along, but I do know her.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] I...[P] said some things to her that I probably shouldn't have.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] It's never too late to apologize.[P] I know she'd forgive you.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] Too late, no, but it can be too early.[P] I'll...[P] think about it.[B][C]") ]])
	
	--Normal:
	else
		fnCutscene([[ Append("Florentina:[E|Surprise] Well, isn't that a neat trick.[P] How'd you do that?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] It's the power of my runestone, leaf-sister.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Offended] Don't go calling me leaf-sister.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] Sorry, habit.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] That runestone is something special, isn't it?[P] Can you do that whenever you want?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Sort of.[P] I don't know how it works, it's instinct.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Very useful.[P] I bet we could get away with all sorts of things with that.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] If you've got anything criminal in mind, forget it.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] You're a buzzkill, you know that?") ]])
	end

	--Common.
	fnCutsceneBlocker()
	return
end

--Roll a random number. There is a 25% chance that a scene will play, otherwise the transformation is instant and no scenes play.
local iRoll = LM_GetRandomNumber(1, 100)
if(iRoll <= 75) then
	return
end

--Scenes that are independent of form.
if(true) then
	
	--Scene Variables.
	local iBackLittleOnes = VM_GetVar("Root/Variables/Global/Mei/Cutscene|Alraune|iBackLittleOnes", "N")
	
	--Mei talks to the little plants, telling them she's "back" as it were.
	if(iBackLittleOnes == 0.0) then
		
		--Flip the variable.
		VM_SetVar("Root/Variables/Global/Mei/Cutscene|Alraune|iBackLittleOnes", "N", 1.0)
		
		--Setup.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		
		--Dialogue.
		fnCutscene([[ Append("Mei:[E|Happy] I'm back, little ones![P] I can't wait to hear your stories![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Hehe, I never really left.[P] I just can't hear you sometimes...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] No matter, just whisper to me whenever you feel like it.") ]])
		fnCutsceneBlocker()
		
	end
	
end
