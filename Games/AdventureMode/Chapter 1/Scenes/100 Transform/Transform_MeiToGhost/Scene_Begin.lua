-- |[ ================================ Transform: Mei to Ghost ================================= ]|
--Used at save points to transform, handles blocking cases.

-- |[ ======= Blocking Cases ======= ]|
--Block transformations on the way back from the beehive.
local bBlockTransformation = false
local iHasSeenOutlandBeeScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenOutlandBeeScene", "N")
local iHasBeeForm = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")
if(iHasSeenOutlandBeeScene == 0.0 and iHasBeeForm == 1.0) then
	fnStandardMajorDialogue()
	fnCutscene([[ Append("Mei:[E|Neutral] (Strange, something seems to be stopping me from transforming...)") ]])
	fnCutsceneBlocker()
	return
end

--During the mannequin sequence, transformations are blocked.
local iBeganSequence  = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iBeganSequence", "N")
local iDefeatedLeader = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iDefeatedLeader", "N")
if(iBeganSequence == 1.0 and iDefeatedLeader == 0.0) then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (No runestone.[P] Not allowed.)") ]])
    fnCutsceneBlocker()
    return
end

-- |[ ========= Execution ========== ]|
-- |[Variables]|
--Store which form Mei started the scene in, among other things.
local sStartingForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")

-- |[Achievement]|
AM_SetPropertyJournal("Unlock Achievement", "BecomeGhost")

-- |[Execute Transformation]|
--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Flash the active character to white. Immediately after, execute the transformation.
Cutscene_CreateEvent("Flash Mei White", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Flashwhite Quickly")
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Ghost.lua") ]])
fnCutsceneWait(gci_Flashwhite_Ticks_Total)
fnCutsceneBlocker()

--Now wait a little bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Cutscene Execution]|
--If Florentina is present, and has not seen Mei transform before, this scene plays. It's mostly the same for all forms.
local bIsFlorentinaPresent = AL_GetProperty("Is Character Following", "Florentina")
local iFlorentinaKnowsAboutRune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")
if(bIsFlorentinaPresent == true and iFlorentinaKnowsAboutRune == 0.0) then

	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N", 1.0)
	
	--Setup. Florentina is opposite to Mei.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
	
	fnCutscene([[ Append("Florentina:[E|Surprise] Woah![P] Mei?[P] Are you okay?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] My runestone lets me do this.[P] I can resume any shape I've been in before.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] So -[P] you're a ghost?[P] You [P]*were*[P] a ghost?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Well, yes, but that's not how I started.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] I'd imagine it's not how any ghosts get started.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] Pfft, you know what I meant.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Well, I'm impressed even if you're a total dope about it.[P] That runestone is really something.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] So are you looking to go back to Earth so you can scare people?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I just think this body -[P] or lack of one -[P] will be useful for the moment, is all.[P] I can become human again if I want.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] Becoming human again...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Shall we be off?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Oh.[P] Yeah.[P] Sure.[P] Let's go.") ]])

	--Common.
	fnCutsceneBlocker()
	return
end

--Roll a random number. There is a 25% chance that a scene will play, otherwise the transformation is instant and no scenes play.
local iRoll = LM_GetRandomNumber(1, 100)
if(iRoll <= 75) then
	return
end

--Scenes that are independent of form.
if(true) then
	
end
