-- |[ =============================== Transform: Mei to Wisphag ================================ ]|
--Used at save points to transform, handles blocking cases.

-- |[ ======= Blocking Cases ======= ]|
--Block transformations on the way back from the beehive.
local bBlockTransformation = false
local iHasSeenOutlandBeeScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenOutlandBeeScene", "N")
local iHasBeeForm = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")
if(iHasSeenOutlandBeeScene == 0.0 and iHasBeeForm == 1.0) then
	fnStandardMajorDialogue()
	fnCutscene([[ Append("Mei:[E|Neutral] (Strange, something seems to be stopping me from transforming...)") ]])
	fnCutsceneBlocker()
	return
end

--During the mannequin sequence, transformations are blocked.
local iBeganSequence  = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iBeganSequence", "N")
local iDefeatedLeader = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iDefeatedLeader", "N")
if(iBeganSequence == 1.0 and iDefeatedLeader == 0.0) then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (No runestone.[P] Not allowed.)") ]])
    fnCutsceneBlocker()
    return
end

-- |[ ========= Execution ========== ]|
-- |[Variables]|
--Store which form Mei started the scene in, among other things.
local sStartingForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")

-- |[Achievement]|
--AM_SetPropertyJournal("Unlock Achievement", "BecomeWerecat")

-- |[Execute Transformation]|
--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Flash the active character to white. Immediately after, execute the transformation.
Cutscene_CreateEvent("Flash Mei White", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Flashwhite Quickly")
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Wisphag.lua") ]])
fnCutsceneWait(gci_Flashwhite_Ticks_Total)
fnCutsceneBlocker()

--Now wait a little bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Cutscene Execution]|
--If Florentina is present, and has not seen Mei transform before, this scene plays. It's mostly the same for all forms.
local bIsFlorentinaPresent = AL_GetProperty("Is Character Following", "Florentina")
local iFlorentinaKnowsAboutRune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")
if(bIsFlorentinaPresent == true and iFlorentinaKnowsAboutRune == 0.0) then

	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N", 1.0)
	
	--Setup. Florentina is opposite to Mei.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
	
	fnCutscene([[ Append("Florentina:[E|Surprise] Ewww![P] What happened!?[P] Mei?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] Hi![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Care to explain yourself?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] .......[P] No.[P] Treasure the mystery.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] Funny.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Smirk] You know, people at work never liked my jokes.[P] I'm glad you do.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] Stop stalling and tell me what happened.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Sad] I don't know how it works but I can tell you that the runestone I found right before I came here?[P] That does it.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I can just hold it close, think about this, and poof.[P] Here I am.[P] I can change back, too.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] Interesting, that's a useful thing to have.[P] Plus, nobody is going to see it coming.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Every now and then, a mage shows up who is really good at illusion magic.[P] This...[P] isn't that.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] Thanks![P] I didn't practice and jumped right to the top![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] But runestone or not, we still need to find a way home.[P] All right?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Smirk] Yeah, yeah.[P] Let's get going.") ]])

	--Common.
	fnCutsceneBlocker()
	return
end

--Roll a random number. There is a 25% chance that a scene will play, otherwise the transformation is instant and no scenes play.
local iRoll = LM_GetRandomNumber(1, 100)
if(iRoll <= 75) then
	return
end

--Scenes that are independent of form.
if(true) then
	
end
