-- |[ =============================== Transform: Mei to Werecat ================================ ]|
--Used at save points to transform, handles blocking cases.

-- |[ ======= Blocking Cases ======= ]|
--Block transformations on the way back from the beehive.
local bBlockTransformation = false
local iHasSeenOutlandBeeScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenOutlandBeeScene", "N")
local iHasBeeForm = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")
if(iHasSeenOutlandBeeScene == 0.0 and iHasBeeForm == 1.0) then
	fnStandardMajorDialogue()
	fnCutscene([[ Append("Mei:[E|Neutral] (Strange, something seems to be stopping me from transforming...)") ]])
	fnCutsceneBlocker()
	return
end

--During the mannequin sequence, transformations are blocked.
local iBeganSequence  = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iBeganSequence", "N")
local iDefeatedLeader = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iDefeatedLeader", "N")
if(iBeganSequence == 1.0 and iDefeatedLeader == 0.0) then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (No runestone.[P] Not allowed.)") ]])
    fnCutsceneBlocker()
    return
end

-- |[ ========= Execution ========== ]|
-- |[Variables]|
--Store which form Mei started the scene in, among other things.
local sStartingForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")

-- |[Achievement]|
AM_SetPropertyJournal("Unlock Achievement", "BecomeWerecat")

-- |[Execute Transformation]|
--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Flash the active character to white. Immediately after, execute the transformation.
Cutscene_CreateEvent("Flash Mei White", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Flashwhite Quickly")
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Werecat.lua") ]])
fnCutsceneWait(gci_Flashwhite_Ticks_Total)
fnCutsceneBlocker()

--Now wait a little bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Cutscene Execution]|
--If Florentina is present, and has not seen Mei transform before, this scene plays. It's mostly the same for all forms.
local bIsFlorentinaPresent = AL_GetProperty("Is Character Following", "Florentina")
local iFlorentinaKnowsAboutRune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")
if(bIsFlorentinaPresent == true and iFlorentinaKnowsAboutRune == 0.0) then

	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N", 1.0)
	
	--Setup. Florentina is opposite to Mei.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
	
	fnCutscene([[ Append("Florentina:[E|Surprise] Well, well, well.[P] I saw your rune glowing.[P] I didn't know it could do this![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Purr...[P] it's quite a rush.[P] I'm sorry, but I don't know how it works.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] How did you know what I was going to ask?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] It's written all over your face.[P] You want to find some way to turn this process into profit.[P] Purrr...[P] I like the look...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Blush] Heh.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Smirk] Like me, you are a hunter.[P] A predator.[P] Except, you hunt for advantage.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] I happen to think I'm good at the other kind of hunting, too.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] You suit my pride just fine.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] So about the runestone...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Smirk] Mmm...[P] I'm sorry, but I don't think it will work on anyone but me.[P] I know this to be a fact, but I cannot say how I know it.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Just as I cannot say how I came by the runestone, come to think of it.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] We can experiment later, assuming there's no way back to Earth.[P] Maybe we can tease some secrets out of that thing.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Laugh] The hunt for the truth...[P] I suppose that, too, is a hunt.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Then let us resume our hunt for a way back to Earth.") ]])

	--Common.
	fnCutsceneBlocker()
	return
end

--Roll a random number. There is a 25% chance that a scene will play, otherwise the transformation is instant and no scenes play.
local iRoll = LM_GetRandomNumber(1, 100)
if(iRoll <= 75) then
	return
end

--Scenes that are independent of form.
if(true) then
	
end
