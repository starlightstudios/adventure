-- |[ ================================= Transform: Mei to Bee ================================== ]|
--Used at save points to transform, handles blocking cases.

-- |[ ======= Blocking Cases ======= ]|
--Block transformations on the way back from the beehive.
local bBlockTransformation = false
local iHasSeenOutlandBeeScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenOutlandBeeScene", "N")
local iHasBeeForm = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")
if(iHasSeenOutlandBeeScene == 0.0 and iHasBeeForm == 1.0) then
	fnStandardMajorDialogue()
	fnCutscene([[ Append("Mei:[E|Neutral] (Strange, something seems to be stopping me from transforming...)") ]])
	fnCutsceneBlocker()
	return
end

--During the mannequin sequence, transformations are blocked.
local iBeganSequence  = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iBeganSequence", "N")
local iDefeatedLeader = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iDefeatedLeader", "N")
if(iBeganSequence == 1.0 and iDefeatedLeader == 0.0) then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (No runestone.[P] Not allowed.)") ]])
    fnCutsceneBlocker()
    return
end

-- |[ ========= Execution ========== ]|
-- |[Variables]|
--Store which form Mei started the scene in.
local sStartingForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")

-- |[Achievement]|
AM_SetPropertyJournal("Unlock Achievement", "BecomeBee")

-- |[Execute Transformation]|
--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Flash the active character to white. Immediately after, execute the transformation.
Cutscene_CreateEvent("Flash Mei White", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Flashwhite Quickly")
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Bee.lua") ]])
fnCutsceneWait(gci_Flashwhite_Ticks_Total)
fnCutsceneBlocker()

--Now wait a little bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Cutscene Execution]|
--If Florentina is present, and has not seen Mei transform before, this scene plays. It's mostly the same for all forms.
local bIsFlorentinaPresent = AL_GetProperty("Is Character Following", "Florentina")
local iFlorentinaKnowsAboutRune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")
if(bIsFlorentinaPresent == true and iFlorentinaKnowsAboutRune == 0.0) then

	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N", 1.0)
	
	--Setup. Florentina is opposite to Mei.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
	
	--Dialogue.
	fnCutscene([[ Append("Florentina:[E|Surprise] Well, isn't that a neat trick.[P] How'd you do that?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] It's the power of my runestone.[P] Yes I know.[P] Yes.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Surprise] Are you -[P] talking to the hive?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] They're very interested in where I went.[P] They couldn't hear me suddenly, they thought I was dead.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] Concerned they lost their investment?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] Concerned they lost a dear friend.[P] You wouldn't understand.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Maybe I wouldn't, maybe I would.[P] Things aren't as clear-cut as you like to think.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] Now, that runestone.[P] Can you do that whenever you want?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Sort of.[P] I don't know exactly how it works, but I can feel something and then, poof.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] It's like an instinct.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] Simply fascinating...") ]])
	fnCutsceneBlocker()
	return
end

--Roll a random number. There is a 25% chance that a scene will play, otherwise the transformation is instant and no scenes play.
local iRoll = LM_GetRandomNumber(1, 100)
if(iRoll <= 75) then
	return
end

--Scenes that are independent of form.
if(true) then
	
	--Scene Variables.
	local iStillLoyal = VM_GetVar("Root/Variables/Global/Mei/Cutscene|Bee|iStillLoyal", "N")
	
	--Mei talks to the little plants, telling them she's "back" as it were. 25% chance.
	if(iStillLoyal == 0.0) then
		
		--Flip the variable.
		VM_SetVar("Root/Variables/Global/Mei/Cutscene|Bee|iStillLoyal", "N", 1.0)
		
		--Setup.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		
		--Dialogue.
		fnCutscene([[ Append("Mei: ...[P] Yes.[P] I told you I was loyal.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Not yet, sisters.[P] I'm still looking.") ]])
		fnCutsceneBlocker()
		
	end
	
end
