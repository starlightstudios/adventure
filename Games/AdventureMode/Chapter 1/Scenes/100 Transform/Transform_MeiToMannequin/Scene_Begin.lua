-- |[Transform Mei to Mannequin]|
--Used at save points when Mei transforms from something else to a Mannequin.

--Special: Block transformations.
local iHasSeenOutlandBeeScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenOutlandBeeScene", "N")
local iHasBeeForm = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")
if(iHasSeenOutlandBeeScene == 0.0 and iHasBeeForm == 1.0) then
	fnStandardMajorDialogue()
	fnCutscene([[ Append("Mei:[E|Neutral] (Strange, something seems to be stopping me from transforming...)") ]])
	fnCutsceneBlocker()
	return
end

-- |[Variables]|
--Store which form Mei started the scene in, among other things.
local sStartingForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")

-- |[Achievement]|
--AM_SetPropertyJournal("Unlock Achievement", "BecomeWerecat")

-- |[Execute Transformation]|
--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Flash the active character to white. Immediately after, execute the transformation.
Cutscene_CreateEvent("Flash Mei White", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Flashwhite Quickly")
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Mannequin.lua") ]])
fnCutsceneWait(gci_Flashwhite_Ticks_Total)
fnCutsceneBlocker()

--Now wait a little bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Cutscene Execution]|
--If Florentina is present, and has not seen Mei transform before, this scene plays. It's mostly the same for all forms.
local bIsFlorentinaPresent = AL_GetProperty("Is Character Following", "Florentina")
local iFlorentinaKnowsAboutRune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")
if(bIsFlorentinaPresent == true and iFlorentinaKnowsAboutRune == 0.0) then

	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N", 1.0)
	
	--Setup. Florentina is opposite to Mei.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
	
	fnCutscene([[ Append("Florentina:[E|Surprise] Hey![P] Mei![P] Speak to me![P] Great, did she put on some cursed jewelry or something!?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Yes?[P] Hi, Florentina.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Is this some kind of spell?[P] I didn't have you for a mage.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I'm not a mage, I don't know anything about magic.[P] I didn't think it was real until recently, you know.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] I am looking right at you and you clearly just used some spell.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] This?[P] This is due to my runestone.[P] Don't ask me how it works, but it lets me turn into any form I have been in before.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I just focus on it and poof.[P] There I am.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] That could come in handy in a variety of places.[P] Do you think I could borrow it?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Not only does it only work for me, but why on earth would you want to be a mannequin?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] Vacating someone's bowels with a slight turn of my head in a dark and spooky place?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] At least you didn't want to rob someone with it.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Blush] I didn't want to lead off with that but...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Runestone or not, can we keep going?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Sure, sure.") ]])

	--Common.
	fnCutsceneBlocker()
	return
end

--Roll a random number. There is a 25% chance that a scene will play, otherwise the transformation is instant and no scenes play.
local iRoll = LM_GetRandomNumber(1, 100)
if(iRoll <= 75) then
	return
end

--Scenes that are independent of form.
if(true) then
	
end
