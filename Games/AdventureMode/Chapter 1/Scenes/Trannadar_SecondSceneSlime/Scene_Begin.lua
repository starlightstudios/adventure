-- |[Trannadar Second Scene: Slime]|
--If Nadia first met Mei as a human, and then Mei entered as a slime...
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarSecondScene", "N", 1.0)

--Determine which side the player entered from.
local bIsPlayerAtBottom = false
local bIsPlayerAtTop = false
EM_PushEntity("Adventure Party", 0)
	local iPlayerX, iPlayerY = TA_GetProperty("Position")
DL_PopActiveObject()
if(iPlayerY >= 37 * 16) then bIsPlayerAtBottom = true end
if(iPlayerY <   7 * 16) then bIsPlayerAtTop = true end

--Wait a bit for the fade.
fnCutsceneWait(15)
fnCutsceneBlocker()

--Mei walks towards Nadia. This is based on where she spawned.
local iHasSeenTrannadarFlorentinaScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
if(bIsPlayerAtBottom) then
	
	--Move Mei.
	Cutscene_CreateEvent("Move Mei North", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (38.50 * gciSizePerTile))
	DL_PopActiveObject()
	
	--If Florentina is present, move her as well.
	if(iHasSeenTrannadarFlorentinaScene == 1.0) then
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (39.50 * gciSizePerTile))
		DL_PopActiveObject()
	end

	--Common.
	fnCutsceneBlocker()

elseif(bIsPlayerAtTop) then
	Cutscene_CreateEvent("Move Mei", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (23.25 * gciSizePerTile), (6.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--If Florentina is present, move her as well.
	if(iHasSeenTrannadarFlorentinaScene == 1.0) then
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (24.25 * gciSizePerTile), (6.50 * gciSizePerTile))
		DL_PopActiveObject()
	end
	fnCutsceneBlocker()
else
	--Move Mei.
	Cutscene_CreateEvent("Move Mei West", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (43.25 * gciSizePerTile), (28.50 * gciSizePerTile))
	DL_PopActiveObject()
	
	--If Florentina is present, move her as well.
	if(iHasSeenTrannadarFlorentinaScene == 1.0) then
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (44.25 * gciSizePerTile), (28.50 * gciSizePerTile))
		DL_PopActiveObject()
	end
	
	--Common.
	fnCutsceneBlocker()
end

--Change the music.
fnCutscene([[ AudioManager_PlayMusic("NadiasTheme") ]])

--Wait a bit.
fnCutsceneWait(5)
fnCutsceneBlocker()

--Dialogue setup.
fnStandardMajorDialogue()
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Nadia", "Neutral") ]])

--Talking.
fnCutscene([[ Append("Nadia: Mei?[P] Mei![P] Oh no, no![P] I -[P] I warned you![P] It's dangerous out there, and now you've gone and let this happen.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Huh?[P] What's wrong?[B][C]") ]])
fnCutscene([[ Append("Nadia: Y-[P]you can talk?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Blush] It's weird to be talking without vocal chords, but I figured out how to.[P] Still feels kind of awkward.[B][C]") ]])
fnCutscene([[ Append("Nadia: This is great![P] Okay, maybe less than great, but for a second there I thought we'd lost you forever![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Surprise] What?[P] Why?[B][C]") ]])
fnCutscene([[ Append("Nadia: Uh, slimes normally aren't very intelligent.[P] I suppose it's not like dying, but I don't think you'd remember who you were.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] No, I am very much still me.[P] Just, squishier.[B][C]") ]])
fnCutscene([[ Append("Nadia: Cool![P] You must be really special or really lucky![P] I'll let everyone know not to attack you on sight.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Gee, thanks, Nadia.[B][C]") ]])
fnCutscene([[ Append("Nadia: Don't mention it.[P] Hey, you should be more careful next slime you go into the forest, eh?[P] Ha ha![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] Ugh.") ]])
fnCutsceneBlocker()

--Change the music.
fnCutscene([[ AudioManager_PlayMusic("TownTheme") ]])
fnCutsceneBlocker()

--Move Florentina onto Mei, fold the party.
if(iHasSeenTrannadarFlorentinaScene == 1.0) then
	
	--Bottom.
	if(bIsPlayerAtBottom) then
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (38.50 * gciSizePerTile))
		DL_PopActiveObject()

    elseif(bIsPlayerAtTop) then
        Cutscene_CreateEvent("Move Mei", "Actor")
            ActorEvent_SetProperty("Subject Name", "Florentina")
            ActorEvent_SetProperty("Move To", (23.25 * gciSizePerTile), (6.50 * gciSizePerTile))
        DL_PopActiveObject()
        fnCutsceneBlocker()
	
	--Right.
	else
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (43.25 * gciSizePerTile), (28.50 * gciSizePerTile))
		DL_PopActiveObject()
	end
	fnCutsceneBlocker()

	--Fold the party positions up.
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
end
