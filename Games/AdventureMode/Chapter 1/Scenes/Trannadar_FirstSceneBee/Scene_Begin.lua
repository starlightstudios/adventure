-- |[ =============================== Trannadar Intro Scene: Bee =============================== ]|
--If Mei is a Bee the first time she enters Trannadar Trading Post, this scene plays.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N", 1.0)

-- |[Move In From Edge]|
--Determine which side the player entered from.
local bIsPlayerAtBottom = false
local bIsPlayerAtTop = false
EM_PushEntity("Adventure Party", 0)
	local iPlayerX, iPlayerY = TA_GetProperty("Position")
DL_PopActiveObject()
if(iPlayerY >= 37 * 16) then bIsPlayerAtBottom = true end
if(iPlayerY <   7 * 16) then bIsPlayerAtTop = true end

--Wait a bit for the fade.
fnCutsceneWait(15)
fnCutsceneBlocker()

-- |[Movement]|
--Mei walks towards Nadia and Blythe. This is based on where she spawned.
if(bIsPlayerAtBottom) then
    fnCutsceneMove("Mei", 28.25, 38.50)
elseif(bIsPlayerAtTop) then
    fnCutsceneMove("Mei", 23.25, 6.50)
else
    fnCutsceneMove("Mei", 43.25, 28.50)
end
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(5)
fnCutsceneBlocker()

--Change the music.
fnCutscene([[ AudioManager_PlayMusic("NadiasTheme") ]])

--Face Nadia and Blythe south.
if(bIsPlayerAtBottom) then
    fnCutsceneFace("Nadia", 0, 1)
    fnCutsceneFace("Blythe", 0, 1)

--Face north.
elseif(bIsPlayerAtTop) then
    fnCutsceneFace("Nadia", 0, -1)
    fnCutsceneFace("Blythe", 0, -1)
    
--Face them east.
else
    fnCutsceneFace("Nadia", 1, 0)
    fnCutsceneFace("Blythe", 1, 0)
end
fnCutsceneWait(5)
fnCutsceneBlocker()

-- |[Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Nadia", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Blythe", "Neutral") ]])
fnCutscene([[ Append("Man: ... the point being that you need to keep the uniform on when you're on duty.[B][C]") ]])
fnCutscene([[ Append("Alraune: But it was hot out![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Uh, hello?[B][C]") ]])
fnCutscene([[ Append("Alraune: Ack![P] A bee![P] Don't sting me![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] I wasn't going to sting you, relax.[P][E|Surprise] Can I do that?[P] Can I sting people?[B][C]") ]])
fnCutscene([[ Append("Alraune: A talking bee? [P]Maybe you were right, bossman.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Right about what?[B][C]") ]])
fnCutscene([[ Append("Man: There have been a lot of strange happenings as of late.[P] Now we have a talking bee.[B][C]") ]])
fnCutscene([[ Append("Alraune: Can all the bees talk, or just you?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] We can all talk, we just don't need to.[P] We can talk to each other using these antennae, you see.[B][C]") ]])
fnCutscene([[ Append("Alraune: Oh, cool![P] So are you, like a representative?[P] Are you wanting to buy something?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Er, no.[P] I'm looking for someone who can help me figure out what this runestone is.[B][C]") ]])
fnCutscene([[ Append("Alraune: Oh, that's way out my league.[P] You'll want to talk to Florentina, if anyone is going to know, it's her.[B][C]") ]])
fnCutscene([[ Append("Man: I don't believe I've introduced myself.[P] I am Darius Blythe, captain of the guard here.[P] This is Nadia, our newest recruit.[B][C]") ]])
fnCutscene([[ Append("Nadia: Howdy![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Call me Mei.[P] Pleased to meet you.[B][C]") ]])
fnCutscene([[ Append("Nadia: Ooh, do all bees have names?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] ...[P] no, just me.[B][C]") ]])
fnCutscene([[ Append("Blythe: Don't pester the poor thing, Nadia.[B][C]") ]])
fnCutscene([[ Append("Nadia: Sorry, boss.[B][C]") ]])
fnCutscene([[ Append("Blythe: I must warn you that we will not tolerate any sort of violence during your stay.[P] If you have a dispute, bring it to me.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Got it.") ]])
fnCutsceneBlocker()

-- |[Finish Up]|
--Change the music.
fnCutscene([[ AudioManager_PlayMusic("TownTheme") ]])
fnCutsceneBlocker()

--Unlock topics.
WD_SetProperty("Unlock Topic", "Alraunes", 1)
WD_SetProperty("Unlock Topic", "Blythe", 1)
WD_SetProperty("Unlock Topic", "Cultists", 1)
WD_SetProperty("Unlock Topic", "Florentina", 1)
WD_SetProperty("Unlock Topic", "Job", 1)
WD_SetProperty("Unlock Topic", "Nadia", 1)
WD_SetProperty("Unlock Topic", "Name", 1)
WD_SetProperty("Unlock Topic", "TradingPost", 1)
WD_SetProperty("Unlock Topic", "Challenge", 1)
