-- |[Combat Victory]|
--The party won! Good job!

-- |[Setup]|
--Variables.
local sMeiForm        = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local iHasWisphagForm = VM_GetVar("Root/Variables/Global/Mei/iHasWisphagForm", "N")

--Flag.
VM_SetVar("Root/Variables/Chapter1/Scenes/iCompletedQuantirMansion", "N", 1.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iWardenQuestState", "N", 3.0)
AudioManager_PlayMusic("Null")

--Reset the topic case. Florentina has more to say.
WD_SetProperty("Clear Topic Read", "Warden")

--Wait a bit.
fnCutsceneWait(60)
fnCutsceneBlocker()

--Un-redden the screen.
fnCutscene([[ AL_SetProperty("Activate Fade", 240, gci_Fade_Under_GUI, false, 0.5, 0, 0, 1, 0, 0, 0, 0) ]])

-- |[Achievement]|
AM_SetPropertyJournal("Unlock Achievement", "FinishManor")

-- |[Dialogue]|
--Setup.
fnStandardMajorDialogue()

--Talking.
fnCutscene([[ Append("Mei:[E|Neutral] It's...[P] it's dead.[P] It's done...[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Countess?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] ...[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] ...[B][C]") ]])
if(iHasWisphagForm == 0.0) then
    fnCutscene([[ Append("Mei:[E|Neutral] I guess she's moved on, now.[P] I hope they both found peace...") ]])
elseif(sMeiForm ~= "Wisphag" and iHasWisphagForm == 1.0) then
    fnCutscene([[ Append("Mei:[E|Neutral] She's moved on, I can sense her spirit.[P] Or rather, it's not there anymore.[P] Must be my wisphag...[P] senses.[P] Goodbye, countess.") ]])
else
    fnCutscene([[ Append("Mei:[E|Neutral] The countess is gone, her spirit isn't there anymore.[P] I'm -[P] glad she found peace.[P] Goodbye, countess, may your next life be happier.") ]])
end
fnCutsceneBlocker()
