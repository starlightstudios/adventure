-- |[Combat Defeat]|
--After the battle is over, and the party was defeated, play this scene. There are several scenes here based on the defeat variable.
local iCassandraDefeatHandler = VM_GetVar("Root/Variables/Chapter1/Scenes/iCassandraDefeatHandler", "N")

--Clear censor bars.
WD_SetProperty("Clear Censor Bars")

-- |[Zero Case]|
--This is the case when fighting the werecats at the campsite.
if(iCassandraDefeatHandler == 0.0) then

	--Variables.
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")

	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Party takes the crouch pose.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Special Frame", "Crouch")
	DL_PopActiveObject()
	if(bIsFlorentinaPresent) then
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Special Frame", "Crouch")
		DL_PopActiveObject()
	end
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Party takes the wounded pose.
	fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Special Frame", "Wounded")
	DL_PopActiveObject()
	if(bIsFlorentinaPresent) then
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Special Frame", "Wounded")
		DL_PopActiveObject()
	end
	fnCutsceneWait(120)
	fnCutsceneBlocker()

	--Fade to black.
	fnCutscene([[ AL_SetProperty("Activate Fade", 60, gci_Fade_Over_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])

	--Wait a bit.
	fnCutsceneWait(120)
	fnCutsceneBlocker()

	--Transition to a new map:
	fnCutscene([[ AL_BeginTransitionTo("EvermoonNE", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraDefeat/Scene_PostTransition.lua") ]])
	fnCutsceneBlocker()

-- |[One Case]|
--If Mei is solo, and tried to save Cassandra, and was a werecat, this scene plays:
elseif(iCassandraDefeatHandler == 1.0) then
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iTurnedCassandra", "N", 1.0)
    
    --Order Cassandra's scene images to load.
    fnLoadDelayedBitmapsFromList("Chapter 1 Cassandra", gciDelayedLoadLoadAtEndOfTick)
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Werecat", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
	
	--Talking.
	fnCutscene([[ Append("Werecat: Your strength does not fail you, but you are outnumbered, kinfang.[P] Submit![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] Perhaps you are not so weak...[B][C]") ]])
	fnCutscene([[ Append("Werecat: Nor are you![P] The time draws near.[P] Join us in the ceremony![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Smirk] Ahhh...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Smirk] I will make her feel the moon's grace...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()

	--Activate scenes mode.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Werecat") ]])
	fnCutscene([[ Append("Mei strode forth into the glade.[P] The mewling, helpless girl looked up at her.[P] She had been bound and gagged by the cats.[P] Her struggle ignited a fire in Mei's heart.[P] She was strong.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Neutral") ]])
	fnCutscene([[ Append("Mei knelt in front of the girl.[P] She looked like an adventurer of some sort.[P] Her clothes had scratches and claw marks on them.[P] The nearby cats likewise had signs of battle on them.[P] The sight of it made her wet.[B][C]") ]])
	fnCutscene([[ Append("The time approached.[P] The cats continued to pace and touch themselves as night had fallen and the moon had come out.[P] With each moment, the girl struggled less and less.[P] She, too, had become wet.[B][C]") ]])
	fnCutscene([[ Append("Mei took position behind her, smelling at her fluids.[P] The pheromones of the other cats hit a fevered pitch.[P] Soon they were clawing and licking at one another.[P] Mei focused her attention on the bound girl.") ]])
	fnCutsceneBlocker()

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF0") ]])
	fnCutscene([[ Append("The curse took hold over her, sprouting forth fur and a tail from her behind.[P] Mei pulled down her pants and exposed her glistening sex to the moonlight.[P] Mei began licking.") ]])
	fnCutsceneBlocker()

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF1") ]])
	fnCutscene([[ Append("Other cats took position around them, licking and clawing all over them.[P] Mei continued to tongue the girl's juices as she became bestial like Mei.[P] Her claws sprang from her hands, allowing her to rip open her bindings.") ]])
	fnCutsceneBlocker()

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
	fnCutscene([[ Append("The cats welcomed her by tongueing every inch of her body.[P] She surprised Mei, suddenly, by pushing her over.[P] Mei tumbled over, surprised by the girl's strength.[B][C]") ]])
	fnCutscene([[ Append("She now descended upon Mei's sex as the orgy continued.[P] Mei lost herself in the pile of fur, tongue, and claw.") ]])
	fnCutsceneBlocker()

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ Append("...[B][C]") ]])
	fnCutscene([[ Append("Hours passed as the orgy raged.[P] The moon continued its arc across the sky.[P] Soon, it would set, and pass behind the trees.[B][C]") ]])
	fnCutscene([[ Append("With the loss of their patron moon, the cats felt themselves tire.[P] Orgasm after orgasm had taken its toll.[P] The cats soon fell asleep in a great pile.[P] Mei cuddled against the new fang and slipped into oblivion.[B][C]") ]])
	fnCutscene([[ Append("...[B][C]") ]])
	fnCutscene([[ Append("When she woke, most of the cats had gone.[P] The new fang was nowhere in sight.[P] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[P] She could taste the new fang there, and she tasted strong.[B][C]") ]])
	fnCutscene([[ Append("Mei left the clearing and made her way back to the camp she had visited earlier.[P] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[P] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Change maps. Go back to the campsite.
	fnCutscene([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
	fnCutsceneBlocker()

-- |[Two Case]|
--If Mei is solo, and tried to save Cassandra, and was a human, this scene plays:
elseif(iCassandraDefeatHandler == 2.0) then
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iTurnedCassandra", "N", 1.0)
    
    --Order Cassandra's scene images to load.
    fnLoadDelayedBitmapsFromList("Chapter 1 Cassandra", gciDelayedLoadLoadAtEndOfTick)
    fnLoadDelayedBitmapsFromList("Chapter 1 Mei Werecat TF", gciDelayedLoadLoadAtEndOfTick)
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Werecat", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
	
	--Talking.
	fnCutscene([[ Append("Werecat: Valiant, but futile.[P] Submit to us, human.[P] You shall join our pride.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] I...[P] I...[B][C]") ]])
	fnCutscene([[ Append("Werecat: Come...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] I...[P] submit...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Switch Mei's form to Werecat.
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Werecat.lua") ]])
	fnCutsceneBlocker()

	--Activate scenes mode.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
	fnCutscene([[ Append("Mei stepped gingerly into the glade.[P] The girl, bound, gagged, and helpless, had watched the battle.[P] She continued to struggle against her bonds as Mei knelt beside her.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF0") ]])
	fnCutscene([[ Append("Mei's head was swimming as she began feeling at the girl's body.[P] The cats had been here, awaiting the moon's grace.[P] They were touching themselves and one another in anticipation.[P] Mei followed suit.[B][C]") ]])
	fnCutscene([[ Append("The other werecats scratched at her, causing her skin to tingle wherever their cursed claws went.[P] There was no pain, only excitement.[P] Mei soon began scratching at the bound girl, fresh curse running through her own fingernails.") ]])
	fnCutsceneBlocker()

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF1") ]])
	fnCutscene([[ Append("As the moon drew nearer, the girl stopped struggling.[P] At the sight of the girl, Mei had become wet, and could sense that the girl had as well.[P] Lost in a bestial surge of pheromones, Mei pulled at the girl's pants and began fingering her sex.[B][C]") ]])
	fnCutscene([[ Append("At last the moonlight became strong enough.[P] Mei marvelled at her hands as the curse took over her, turning them to claws and growing black fur all across her body.[P] The scent of the night struck her new nose as the same changes followed on the girl.") ]])
	fnCutsceneBlocker()

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF0") ]])
	fnCutscene([[ Append("Now kinfang, she used her claws to tear off the bindings.[P] The blonde fang had not changed as much as she had. She seemed horrified as her claws formed and her feet remolded to suit her new purpose.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF1") ]])
	fnCutscene([[ Append("Fur grew out from her skin and her teeth sharpened into fangs. While the new instincts of her body began to change her mind, her horror turned to confusion. Uncertainty. Mei dealt with her by planting a kiss on her lips.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
	fnCutscene([[ Append("The thrill of the moonlight overcame the last of the blonde fang's resistance, and she revelled in the changes. She returned Mei's kiss and began groping at the cats who stood near her.[B][C]") ]])
	fnCutscene([[ Append("More cats had entered the clearing, smelling the pheromones.[P] The cats swarmed over the lip-locked lovers, kissing and licking.[P] A werecat began fingering Mei's sex while others licked her fur and skin with their rough tongues.[B][C]") ]])
	fnCutscene([[ Append("Mei and the blonde fang lost themselves in the orgy of cats as the moon gave them its perfect grace.[P] Minutes dragged into hours as cats continued to suck and lick one another.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ Append("...[B][C]") ]])
	fnCutscene([[ Append("Eventually, the moon's arc bent past the line of trees.[P] With the loss of their patron, the endless orgasms exhausted the night hunters.[P] Soon, Mei drifted to sleep with the blonde fang still in her arms.[B][C]") ]])
	fnCutscene([[ Append("...[B][C]") ]])
	fnCutscene([[ Append("When she woke, most of the cats had gone.[P] The new fang was nowhere in sight.[P] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[P] She could taste the new fang there, and she tasted strong.[B][C]") ]])
	fnCutscene([[ Append("Mei left the clearing and made her way back to the camp she had visited earlier.[P] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[P] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Change maps. Go back to the campsite.
	fnCutscene([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
	fnCutsceneBlocker()
	
-- |[Three Case]|
--If Mei is not a human or werecat, is solo, and tried to rescue Cassandra but failed:
elseif(iCassandraDefeatHandler == 3.0) then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iTurnedCassandra", "N", 1.0)
    
    --Order Cassandra's scene images to load.
    fnLoadDelayedBitmapsFromList("Chapter 1 Cassandra", gciDelayedLoadLoadAtEndOfTick)
    fnLoadDelayedBitmapsFromList("Chapter 1 Mei Werecat TF", gciDelayedLoadLoadAtEndOfTick)
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Werecat", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
	
	--Talking.
	fnCutscene([[ Append("Werecat: You smell like a human -[P] your disguise does not fool us![P] Remove it![P] Submit![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] I...[P] I...[B][C]") ]])
	fnCutscene([[ Append("Werecat: Come...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] I...[P] submit...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Switch Mei's form to Werecat.
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Werecat.lua") ]])
	fnCutsceneBlocker()

	--Activate scenes mode.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
	fnCutscene([[ Append("Mei stepped gingerly into the glade.[P] The girl, bound, gagged, and helpless, had watched the battle.[P] She continued to struggle against her bonds as Mei knelt beside her.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF0") ]])
	fnCutscene([[ Append("Mei's head was swimming as she began feeling at the girl's body.[P] The cats had been here, awaiting the moon's grace.[P] They were touching themselves and one another in anticipation.[P] Mei followed suit.[B][C]") ]])
	fnCutscene([[ Append("The other werecats scratched at her, causing her skin to tingle wherever their cursed claws went.[P] There was no pain, only excitement.[P] Mei soon began scratching at the bound girl, fresh curse running through her own fingernails.") ]])
	fnCutsceneBlocker()

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF1") ]])
	fnCutscene([[ Append("As the moon drew nearer, the girl stopped struggling.[P] At the sight of the girl, Mei had become wet, and could sense that the girl had as well.[P] Lost in a bestial surge of pheromones, Mei pulled at the girl's pants and began fingering her sex.[B][C]") ]])
	fnCutscene([[ Append("At last the moonlight became strong enough.[P] Mei marvelled at her hands as the curse took over her, turning them to claws and growing black fur all across her body.[P] The scent of the night struck her new nose as the same changes followed on the girl.") ]])
	fnCutsceneBlocker()

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF0") ]])
	fnCutscene([[ Append("Now kinfang, she used her claws to tear off the bindings.[P] The blonde fang had not changed as much as she had. She seemed horrified as her claws formed and her feet remolded to suit her new purpose.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF1") ]])
	fnCutscene([[ Append("Fur grew out from her skin and her teeth sharpened into fangs. While the new instincts of her body began to change her mind, her horror turned to confusion. Uncertainty. Mei dealt with her by planting a kiss on her lips.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
	fnCutscene([[ Append("The thrill of the moonlight overcame the last of the blonde fang's resistance, and she revelled in the changes. She returned Mei's kiss and began groping at the cats who stood near her.[B][C]") ]])
	fnCutscene([[ Append("More cats had entered the clearing, smelling the pheromones.[P] The cats swarmed over the lip-locked lovers, kissing and licking.[P] A werecat began fingering Mei's sex while others licked her fur and skin with their rough tongues.[B][C]") ]])
	fnCutscene([[ Append("Mei and the blonde fang lost themselves in the orgy of cats as the moon gave them its perfect grace.[P] Minutes dragged into hours as cats continued to suck and lick one another.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ Append("...[B][C]") ]])
	fnCutscene([[ Append("Eventually, the moon's arc bent past the line of trees.[P] With the loss of their patron, the endless orgasms exhausted the night hunters.[P] Soon, Mei drifted to sleep with the blonde fang still in her arms.[B][C]") ]])
	fnCutscene([[ Append("...[B][C]") ]])
	fnCutscene([[ Append("When she woke, most of the cats had gone.[P] The new fang was nowhere in sight.[P] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[P] She could taste the new fang there, and she tasted strong.[B][C]") ]])
	fnCutscene([[ Append("Mei left the clearing and made her way back to the camp she had visited earlier.[P] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[P] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Change maps. Go back to the campsite.
	fnCutscene([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
	fnCutsceneBlocker()

-- |[Four Case]|
--Mei tried to rescue Cassandra as a human with Florentina's help:
elseif(iCassandraDefeatHandler == 4.0) then
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iTurnedCassandra", "N", 1.0)
    
    --Order Cassandra's scene images to load.
    fnLoadDelayedBitmapsFromList("Chapter 1 Cassandra", gciDelayedLoadLoadAtEndOfTick)
    fnLoadDelayedBitmapsFromList("Chapter 1 Mei Werecat TF", gciDelayedLoadLoadAtEndOfTick)
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Werecat", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
	
	--Talking.
	fnCutscene([[ Append("Werecat: Valiant, but futile.[P] Submit to us, human.[P] You shall join our pride.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] Come on, Mei.[P] We can take them.[P] Just, stand up, grip your sword...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] I...[P] I...[B][C]") ]])
	fnCutscene([[ Append("Werecat: Come...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] I...[P] submit...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Surprise] What the hay?[B][C]") ]])
	fnCutscene([[ Append("Werecat: She heeds the call of the night.[P] She will join us, plant.[P] We bear you no ill will.[P] You will be allowed to leave as testament to your strength.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] (Great.[P] I can't take them all on alone.)[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Blush] Can I at least watch?[B][C]") ]])
	fnCutscene([[ Append("Werecat: Purrrrrr...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Switch Mei's form to Werecat.
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Werecat.lua") ]])
	fnCutsceneBlocker()

	--Activate scenes mode.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
	fnCutscene([[ Append("Mei stepped gingerly into the glade.[P] The girl, bound, gagged, and helpless, had watched the battle.[P] She continued to struggle against her bonds as Mei knelt beside her.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF0") ]])
	fnCutscene([[ Append("Mei's head was swimming as she began feeling at the girl's body.[P] The cats had been here, awaiting the moon's grace.[P] They were touching themselves and one another in anticipation.[P] Mei followed suit.[B][C]") ]])
	fnCutscene([[ Append("The other werecats scratched at her, causing her skin to tingle wherever their cursed claws went.[P] There was no pain, only excitement.[P] Mei soon began scratching at the bound girl, fresh curse running through her own fingernails.") ]])
	fnCutsceneBlocker()

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF1") ]])
	fnCutscene([[ Append("As the moon drew nearer, the girl stopped struggling.[P] At the sight of the girl, Mei had become wet, and could sense that the girl had as well.[P] Lost in a bestial surge of pheromones, Mei pulled at the girl's pants and began fingering her sex.[B][C]") ]])
	fnCutscene([[ Append("At last the moonlight became strong enough.[P] Mei marvelled at her hands as the curse took over her, turning them to claws and growing black fur all across her body.[P] The scent of the night struck her new nose as the same changes followed on the girl.") ]])
	fnCutsceneBlocker()

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF0") ]])
	fnCutscene([[ Append("Now kinfang, she used her claws to tear off the bindings.[P] The blonde fang had not changed as much as she had. She seemed horrified as her claws formed and her feet remolded to suit her new purpose.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF1") ]])
	fnCutscene([[ Append("Fur grew out from her skin and her teeth sharpened into fangs. While the new instincts of her body began to change her mind, her horror turned to confusion. Uncertainty. Mei dealt with her by planting a kiss on her lips.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
	fnCutscene([[ Append("The thrill of the moonlight overcame the last of the blonde fang's resistance, and she revelled in the changes. She returned Mei's kiss and began groping at the cats who stood near her.[B][C]") ]])
	fnCutscene([[ Append("More cats had entered the clearing, smelling the pheromones.[P] The cats swarmed over the lip-locked lovers, kissing and licking.[P] A werecat began fingering Mei's sex while others licked her fur and skin with their rough tongues.[B][C]") ]])
	fnCutscene([[ Append("Mei and the blonde fang lost themselves in the orgy of cats as the moon gave them its perfect grace.[P] Minutes dragged into hours as cats continued to suck and lick one another.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ Append("...[B][C]") ]])
	fnCutscene([[ Append("Eventually, the moon's arc bent past the line of trees.[P] With the loss of their patron, the endless orgasms exhausted the night hunters.[P] Soon, Mei drifted to sleep with the blonde fang still in her arms.[B][C]") ]])
	fnCutscene([[ Append("...[B][C]") ]])
	fnCutscene([[ Append("When she woke, most of the cats had gone.[P] The new fang was nowhere in sight.[P] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[P] She could taste the new fang there, and she tasted strong.[B][C]") ]])
	fnCutscene([[ Append("Mei left the clearing and made her way back to the camp she had visited earlier.[P] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[P] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Change maps. Go back to the campsite.
	fnCutscene([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
	fnCutsceneBlocker()

--Mei tried to rescue Cassandra as a werecat with Florentina's help:
elseif(iCassandraDefeatHandler == 5.0) then
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iTurnedCassandra", "N", 1.0)
    
    --Order Cassandra's scene images to load.
    fnLoadDelayedBitmapsFromList("Chapter 1 Cassandra", gciDelayedLoadLoadAtEndOfTick)
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Werecat", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
	
	--Talking.
	fnCutscene([[ Append("Werecat: Your strength does not fail you, but you are outnumbered, kinfang.[P] Submit![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] Perhaps you are not so weak...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] Come on Mei, we can take them.[P] Just, catch your breath...[B][C]") ]])
	fnCutscene([[ Append("Werecat: Ha ha, plant![P] Your resolve is admirable![P] Join us in the ceremony![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] Unnhhh...[P] yes....[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] I think the pheromones are getting to you.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Blush] I can't take them alone.[P] I guess I'll just watch.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Smirk] Unnhh...[P] Ahhh...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Smirk] I will make her feel the moon's grace...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()

	--Activate scenes mode.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Werecat") ]])
	fnCutscene([[ Append("Mei strode forth into the glade.[P] The mewling, helpless girl looked up at her.[P] She had been bound and gagged by the cats.[P] Her struggle ignited a fire in Mei's heart.[P] She was strong.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Neutral") ]])
	fnCutscene([[ Append("Mei knelt in front of the girl.[P] She looked like an adventurer of some sort.[P] Her clothes had scratches and claw marks on them.[P] The nearby cats likewise had signs of battle on them.[P] The sight of it made her wet.[B][C]") ]])
	fnCutscene([[ Append("The time approached.[P] The cats continued to pace and touch themselves as night had fallen and the moon had come out.[P] With each moment, the girl struggled less and less.[P] She, too, had become wet.[B][C]") ]])
	fnCutscene([[ Append("Mei took position behind her, smelling at her fluids.[P] The pheromones of the other cats hit a fevered pitch.[P] Soon they were clawing and licking at one another.[P] Mei focused her attention on the bound girl.") ]])
	fnCutsceneBlocker()

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF0") ]])
	fnCutscene([[ Append("The curse took hold over her, sprouting forth fur and a tail from her behind.[P] Mei pulled down her pants and exposed her glistening sex to the moonlight.[P] Mei began licking.") ]])
	fnCutsceneBlocker()

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF1") ]])
	fnCutscene([[ Append("Other cats took position around them, licking and clawing all over them.[P] Mei continued to tongue the girl's juices as she became bestial like Mei.[P] Her claws sprang from her hands, allowing her to rip open her bindings.") ]])
	fnCutsceneBlocker()

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
	fnCutscene([[ Append("The cats welcomed her by tongueing every inch of her body.[P] She surprised Mei, suddenly, by pushing her over.[P] Mei tumbled over, surprised by the girl's strength.[B][C]") ]])
	fnCutscene([[ Append("She now descended upon Mei's sex as the orgy continued.[P] Mei lost herself in the pile of fur, tongue, and claw.") ]])
	fnCutsceneBlocker()

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ Append("...[B][C]") ]])
	fnCutscene([[ Append("Hours passed as the orgy raged.[P] The moon continued its arc across the sky.[P] Soon, it would set, and pass behind the trees.[B][C]") ]])
	fnCutscene([[ Append("With the loss of their patron moon, the cats felt themselves tire.[P] Orgasm after orgasm had taken its toll.[P] The cats soon fell asleep in a great pile.[P] Mei cuddled against the new fang and slipped into oblivion.[B][C]") ]])
	fnCutscene([[ Append("...[B][C]") ]])
	fnCutscene([[ Append("When she woke, most of the cats had gone.[P] The new fang was nowhere in sight.[P] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[P] She could taste the new fang there, and she tasted strong.[B][C]") ]])
	fnCutscene([[ Append("Mei left the clearing and made her way back to the camp she had visited earlier.[P] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[P] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Change maps. Go back to the campsite.
	fnCutscene([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
	fnCutsceneBlocker()

--Mei tried to rescue Cassandra as a non-human non-werecat with Florentina's help:
elseif(iCassandraDefeatHandler == 6.0) then
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iTurnedCassandra", "N", 1.0)
    
    --Order Cassandra's scene images to load.
    fnLoadDelayedBitmapsFromList("Chapter 1 Cassandra", gciDelayedLoadLoadAtEndOfTick)
    fnLoadDelayedBitmapsFromList("Chapter 1 Mei Werecat TF", gciDelayedLoadLoadAtEndOfTick)
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Werecat", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
	
	--Talking.
	fnCutscene([[ Append("Werecat: Valiant, but futile.[P] Your disguise cannot hide that you are a strong human.[P] Submit to us.[P] You shall join our pride.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] Come on, Mei.[P] We can take them.[P] Just, stand up, grip your sword...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] I...[P] I...[B][C]") ]])
	fnCutscene([[ Append("Werecat: Come...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] I...[P] submit...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Surprise] What the hay?[B][C]") ]])
	fnCutscene([[ Append("Werecat: She heeds the call of the night.[P] She will join us, plant.[P] We bear you no ill will.[P] You will be allowed to leave as testament to your strength.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] (Great.[P] I can't take them all on alone.)[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Blush] Can I at least watch?[B][C]") ]])
	fnCutscene([[ Append("Werecat: Purrrrrr...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Switch Mei's form to Werecat.
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Werecat.lua") ]])
	fnCutsceneBlocker()

	--Activate scenes mode.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
	fnCutscene([[ Append("Mei stepped gingerly into the glade.[P] The girl, bound, gagged, and helpless, had watched the battle.[P] She continued to struggle against her bonds as Mei knelt beside her.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF0") ]])
	fnCutscene([[ Append("Mei's head was swimming as she began feeling at the girl's body.[P] The cats had been here, awaiting the moon's grace.[P] They were touching themselves and one another in anticipation.[P] Mei followed suit.[B][C]") ]])
	fnCutscene([[ Append("The other werecats scratched at her, causing her skin to tingle wherever their cursed claws went.[P] There was no pain, only excitement.[P] Mei soon began scratching at the bound girl, fresh curse running through her own fingernails.") ]])
	fnCutsceneBlocker()

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF1") ]])
	fnCutscene([[ Append("As the moon drew nearer, the girl stopped struggling.[P] At the sight of the girl, Mei had become wet, and could sense that the girl had as well.[P] Lost in a bestial surge of pheromones, Mei pulled at the girl's pants and began fingering her sex.[B][C]") ]])
	fnCutscene([[ Append("At last the moonlight became strong enough.[P] Mei marvelled at her hands as the curse took over her, turning them to claws and growing black fur all across her body.[P] The scent of the night struck her new nose as the same changes followed on the girl.") ]])
	fnCutsceneBlocker()

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF0") ]])
	fnCutscene([[ Append("Now kinfang, she used her claws to tear off the bindings.[P] The blonde fang had not changed as much as she had. She seemed horrified as her claws formed and her feet remolded to suit her new purpose.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF1") ]])
	fnCutscene([[ Append("Fur grew out from her skin and her teeth sharpened into fangs. While the new instincts of her body began to change her mind, her horror turned to confusion. Uncertainty. Mei dealt with her by planting a kiss on her lips.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
	fnCutscene([[ Append("The thrill of the moonlight overcame the last of the blonde fang's resistance, and she revelled in the changes. She returned Mei's kiss and began groping at the cats who stood near her.[B][C]") ]])
	fnCutscene([[ Append("More cats had entered the clearing, smelling the pheromones.[P] The cats swarmed over the lip-locked lovers, kissing and licking.[P] A werecat began fingering Mei's sex while others licked her fur and skin with their rough tongues.[B][C]") ]])
	fnCutscene([[ Append("Mei and the blonde fang lost themselves in the orgy of cats as the moon gave them its perfect grace.[P] Minutes dragged into hours as cats continued to suck and lick one another.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ Append("...[B][C]") ]])
	fnCutscene([[ Append("Eventually, the moon's arc bent past the line of trees.[P] With the loss of their patron, the endless orgasms exhausted the night hunters.[P] Soon, Mei drifted to sleep with the blonde fang still in her arms.[B][C]") ]])
	fnCutscene([[ Append("...[B][C]") ]])
	fnCutscene([[ Append("When she woke, most of the cats had gone.[P] The new fang was nowhere in sight.[P] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[P] She could taste the new fang there, and she tasted strong.[B][C]") ]])
	fnCutscene([[ Append("Mei left the clearing and made her way back to the camp she had visited earlier.[P] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[P] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Change maps. Go back to the campsite.
	fnCutscene([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
	fnCutsceneBlocker()

-- |[Too Late: Failed]|
--Mei is a human, solo, and failed to save Cassandra. She lost the ensuing battle and will now become a werecat.
elseif(iCassandraDefeatHandler == 7.0) then
	
    --Order Cassandra's scene images to load.
    fnLoadDelayedBitmapsFromList("Chapter 1 Cassandra", gciDelayedLoadLoadAtEndOfTick)
    fnLoadDelayedBitmapsFromList("Chapter 1 Mei Werecat TF", gciDelayedLoadLoadAtEndOfTick)
    
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Werecat") ]])
	
	--Talking.
	fnCutscene([[ Append("Fang: Oh, look at you.[P] Trying to save someone who doesn't want to be saved?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] ...[B][C]") ]])
	fnCutscene([[ Append("Fang: Come to me.[P] Join me...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] (Nnnghh...[P] so horny...[P] the moonlight is doing this to me...)[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] Yes...[B][C]") ]])
	fnCutscene([[ Append("Fang: Purrrrr...[B][C]") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Switch Mei's form to Werecat.
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Werecat.lua") ]])
	fnCutsceneBlocker()

	--Activate scenes mode.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
	fnCutscene([[ Append("Wounded in the battle, Mei could hardly summon the strength to resist as the werecats swarmed around her.[P] They seemed to enjoy taunting her with their claws and tails, swiping gently against her skin.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
	fnCutscene([[ Append("The blonde werecat, the girl she had been trying to save, stood at the center of the swarm.[P] Directly in front of Mei, the girl held her attention as she pawed at Mei's face.[B][C]") ]])
	fnCutscene([[ Append("Mei realized that the blonde cat was very gently scratching her, drawing the tiniest rivulet of blood from her cheek.[P] She felt no pain, but arousal, as the moonlight danced over the cat's features.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF0") ]])
	fnCutscene([[ Append("Mei nearly gasped as she realized her vision had been enhanced.[P] She could now see perfectly in the dark.[P] Every inch of the blonde cat's body was visible to her.[P] She could see each individual hair stand on end.[P] She put her hands on the cat's hips...") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF1") ]])
	fnCutscene([[ Append("She was unsurprised to see that her own hands had changed, transforming into clawed paws as she wrapped around the girl's midsection.[P] Her body shuddered with pleasure as the changes rippled forth.[P] Fur sprung from her skin and her ears shifted to the top of her head.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Werecat") ]])
	fnCutscene([[ Append("The girl kissed her as the changes finished.[P] The rest of her pride now descended upon them, licking and sucking every inch of her body as the pheromones and moonlight hit a fevered pitch.[B][C]") ]])
	fnCutscene([[ Append("The kinfangs continued their orgy as the moon looked on, bathing them in its stimulating glow.[P] They worshipped it with their bodies, cleaning their new fangs with their tongues.[P] Inside and out.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ Append("...[B][C]") ]])
	fnCutscene([[ Append("Hours passed as the orgy raged.[P] The moon continued its arc across the sky.[P] Soon, it would set, and pass behind the trees.[B][C]") ]])
	fnCutscene([[ Append("With the loss of their patron moon, the cats felt themselves tire.[P] Orgasm after orgasm had taken its toll.[P] The cats soon fell asleep in a great pile.[P] Mei cuddled against the new fang and slipped into oblivion.[B][C]") ]])
	fnCutscene([[ Append("...[B][C]") ]])
	fnCutscene([[ Append("When she woke, most of the cats had gone.[P] The new fang was nowhere in sight.[P] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[P] She could taste the new fang there, and she tasted strong.[B][C]") ]])
	fnCutscene([[ Append("Mei left the clearing and made her way back to the camp she had visited earlier.[P] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[P] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Change maps. Go back to the campsite.
	fnCutscene([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
	fnCutsceneBlocker()

-- |[Too Late Non-Human]|
--Mei is a non-human, solo, and failed to save Cassandra. She lost the ensuing battle and will now become a werecat.
elseif(iCassandraDefeatHandler == 8.0) then
    
    --Order Cassandra's scene images to load.
    fnLoadDelayedBitmapsFromList("Chapter 1 Cassandra", gciDelayedLoadLoadAtEndOfTick)
    fnLoadDelayedBitmapsFromList("Chapter 1 Mei Werecat TF", gciDelayedLoadLoadAtEndOfTick)

	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Werecat") ]])
	
	--Talking.
	fnCutscene([[ Append("Fang: Oh, look at you.[P] Trying to save someone who doesn't want to be saved?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] ...[B][C]") ]])
	fnCutscene([[ Append("Fang: Your disguise is clever, but we can smell the human on you.[B][C]") ]])
	fnCutscene([[ Append("Fang: Come to me.[P] Join me...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] (Nnnghh...[P] so horny...[P] the moonlight is doing this to me...)[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] Yes...[B][C]") ]])
	fnCutscene([[ Append("Fang: Purrrrr...[B][C]") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Switch Mei's form to Werecat.
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Werecat.lua") ]])
	fnCutsceneBlocker()

	--Activate scenes mode.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
	fnCutscene([[ Append("Wounded in the battle, Mei could hardly summon the strength to resist as the werecats swarmed around her.[P] They seemed to enjoy taunting her with their claws and tails, swiping gently against her skin.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
	fnCutscene([[ Append("The blonde werecat, the girl she had been trying to save, stood at the center of the swarm.[P] Directly in front of Mei, the girl held her attention as she pawed at Mei's face.[B][C]") ]])
	fnCutscene([[ Append("Mei realized that the blonde cat was very gently scratching her, drawing the tiniest rivulet of blood from her cheek.[P] She felt no pain, but arousal, as the moonlight danced over the cat's features.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF0") ]])
	fnCutscene([[ Append("Mei nearly gasped as she realized her vision had been enhanced.[P] She could now see perfectly in the dark.[P] Every inch of the blonde cat's body was visible to her.[P] She could see each individual hair stand on end.[P] She put her hands on the cat's hips...") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF1") ]])
	fnCutscene([[ Append("She was unsurprised to see that her own hands had changed, transforming into clawed paws as she wrapped around the girl's midsection.[P] Her body shuddered with pleasure as the changes rippled forth.[P] Fur sprung from her skin and her ears shifted to the top of her head.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Werecat") ]])
	fnCutscene([[ Append("The girl kissed her as the changes finished.[P] The rest of her pride now descended upon them, licking and sucking every inch of her body as the pheromones and moonlight hit a fevered pitch.[B][C]") ]])
	fnCutscene([[ Append("The kinfangs continued their orgy as the moon looked on, bathing them in its stimulating glow.[P] They worshipped it with their bodies, cleaning their new fangs with their tongues.[P] Inside and out.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ Append("...[B][C]") ]])
	fnCutscene([[ Append("Hours passed as the orgy raged.[P] The moon continued its arc across the sky.[P] Soon, it would set, and pass behind the trees.[B][C]") ]])
	fnCutscene([[ Append("With the loss of their patron moon, the cats felt themselves tire.[P] Orgasm after orgasm had taken its toll.[P] The cats soon fell asleep in a great pile.[P] Mei cuddled against the new fang and slipped into oblivion.[B][C]") ]])
	fnCutscene([[ Append("...[B][C]") ]])
	fnCutscene([[ Append("When she woke, most of the cats had gone.[P] The new fang was nowhere in sight.[P] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[P] She could taste the new fang there, and she tasted strong.[B][C]") ]])
	fnCutscene([[ Append("Mei left the clearing and made her way back to the camp she had visited earlier.[P] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[P] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Change maps. Go back to the campsite.
	fnCutscene([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
	fnCutsceneBlocker()

-- |[Too Late Human + Florentina]|
--Mei is a human, with Florentina, and failed to save Cassandra. She lost the ensuing battle and will now become a werecat.
elseif(iCassandraDefeatHandler == 9.0) then
    
    --Order Cassandra's scene images to load.
    fnLoadDelayedBitmapsFromList("Chapter 1 Cassandra", gciDelayedLoadLoadAtEndOfTick)
    fnLoadDelayedBitmapsFromList("Chapter 1 Mei Werecat TF", gciDelayedLoadLoadAtEndOfTick)

	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Werecat") ]])
	
	--Talking.
	fnCutscene([[ Append("Fang: Oh, look at you.[P] Trying to save someone who doesn't want to be saved?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] ...[B][C]") ]])
	fnCutscene([[ Append("Fang: Come to me.[P] Join me...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] Come -[P] come on, Mei.[P] We can take them, just -[P] stand up.[P] Come on...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] (Nnnghh...[P] so horny...[P] the moonlight is doing this to me...)[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] Yes...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Surprise] Mei? Hey![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] I want this...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] Oh, lovely.[P] You've got the curse, too.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Nothing to do but watch, I guess.[B][C]") ]])
	fnCutscene([[ Append("Fang: Purrrrr...[B][C]") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Switch Mei's form to Werecat.
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Werecat.lua") ]])
	fnCutsceneBlocker()

	--Activate scenes mode.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
	fnCutscene([[ Append("Wounded in the battle, Mei could hardly summon the strength to resist as the werecats swarmed around her.[P] They seemed to enjoy taunting her with their claws and tails, swiping gently against her skin.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
	fnCutscene([[ Append("The blonde werecat, the girl she had been trying to save, stood at the center of the swarm.[P] Directly in front of Mei, the girl held her attention as she pawed at Mei's face.[B][C]") ]])
	fnCutscene([[ Append("Mei realized that the blonde cat was very gently scratching her, drawing the tiniest rivulet of blood from her cheek.[P] She felt no pain, but arousal, as the moonlight danced over the cat's features.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF0") ]])
	fnCutscene([[ Append("Mei nearly gasped as she realized her vision had been enhanced.[P] She could now see perfectly in the dark.[P] Every inch of the blonde cat's body was visible to her.[P] She could see each individual hair stand on end.[P] She put her hands on the cat's hips...") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF1") ]])
	fnCutscene([[ Append("She was unsurprised to see that her own hands had changed, transforming into clawed paws as she wrapped around the girl's midsection.[P] Her body shuddered with pleasure as the changes rippled forth.[P] Fur sprung from her skin and her ears shifted to the top of her head.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Werecat") ]])
	fnCutscene([[ Append("The girl kissed her as the changes finished.[P] The rest of her pride now descended upon them, licking and sucking every inch of her body as the pheromones and moonlight hit a fevered pitch.[B][C]") ]])
	fnCutscene([[ Append("The kinfangs continued their orgy as the moon looked on, bathing them in its stimulating glow.[P] They worshipped it with their bodies, cleaning their new fangs with their tongues.[P] Inside and out.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ Append("...[B][C]") ]])
	fnCutscene([[ Append("Hours passed as the orgy raged.[P] The moon continued its arc across the sky.[P] Soon, it would set, and pass behind the trees.[B][C]") ]])
	fnCutscene([[ Append("With the loss of their patron moon, the cats felt themselves tire.[P] Orgasm after orgasm had taken its toll.[P] The cats soon fell asleep in a great pile.[P] Mei cuddled against the new fang and slipped into oblivion.[B][C]") ]])
	fnCutscene([[ Append("...[B][C]") ]])
	fnCutscene([[ Append("When she woke, most of the cats had gone.[P] The new fang was nowhere in sight.[P] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[P] She could taste the new fang there, and she tasted strong.[B][C]") ]])
	fnCutscene([[ Append("Mei left the clearing and made her way back to the camp she had visited earlier.[P] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[P] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Change maps. Go back to the campsite.
	fnCutscene([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
	fnCutsceneBlocker()

-- |[Too Late Non-Human + Florentina]|
--Mei is a non-human, with Florentina, and failed to save Cassandra. She lost the ensuing battle and will now become a werecat.
elseif(iCassandraDefeatHandler == 10.0) then
    
    --Order Cassandra's scene images to load.
    fnLoadDelayedBitmapsFromList("Chapter 1 Cassandra", gciDelayedLoadLoadAtEndOfTick)
    fnLoadDelayedBitmapsFromList("Chapter 1 Mei Werecat TF", gciDelayedLoadLoadAtEndOfTick)

	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Werecat") ]])
	
	--Talking.
	fnCutscene([[ Append("Fang: Oh, look at you.[P] Trying to save someone who doesn't want to be saved?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] ...[B][C]") ]])
	fnCutscene([[ Append("Fang: Your disguise is clever, but we can smell the human on you.[B][C]") ]])
	fnCutscene([[ Append("Fang: Come to me.[P] Join me...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] Come -[P] come on, Mei.[P] We can take them, just -[P] stand up.[P] Come on...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] (Nnnghh...[P] so horny...[P] the moonlight is doing this to me...)[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] Yes...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Surprise] Mei? Hey![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] I want this...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] Oh, lovely.[P] You've got the curse, too.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Nothing to do but watch, I guess.[B][C]") ]])
	fnCutscene([[ Append("Fang: Purrrrr...[B][C]") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Switch Mei's form to Werecat.
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Werecat.lua") ]])
	fnCutsceneBlocker()

	--Activate scenes mode.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
	fnCutscene([[ Append("Wounded in the battle, Mei could hardly summon the strength to resist as the werecats swarmed around her.[P] They seemed to enjoy taunting her with their claws and tails, swiping gently against her skin.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
	fnCutscene([[ Append("The blonde werecat, the girl she had been trying to save, stood at the center of the swarm.[P] Directly in front of Mei, the girl held her attention as she pawed at Mei's face.[B][C]") ]])
	fnCutscene([[ Append("Mei realized that the blonde cat was very gently scratching her, drawing the tiniest rivulet of blood from her cheek.[P] She felt no pain, but arousal, as the moonlight danced over the cat's features.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF0") ]])
	fnCutscene([[ Append("Mei nearly gasped as she realized her vision had been enhanced.[P] She could now see perfectly in the dark.[P] Every inch of the blonde cat's body was visible to her.[P] She could see each individual hair stand on end.[P] She put her hands on the cat's hips...") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF1") ]])
	fnCutscene([[ Append("She was unsurprised to see that her own hands had changed, transforming into clawed paws as she wrapped around the girl's midsection.[P] Her body shuddered with pleasure as the changes rippled forth.[P] Fur sprung from her skin and her ears shifted to the top of her head.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Werecat") ]])
	fnCutscene([[ Append("The girl kissed her as the changes finished.[P] The rest of her pride now descended upon them, licking and sucking every inch of her body as the pheromones and moonlight hit a fevered pitch.[B][C]") ]])
	fnCutscene([[ Append("The kinfangs continued their orgy as the moon looked on, bathing them in its stimulating glow.[P] They worshipped it with their bodies, cleaning their new fangs with their tongues.[P] Inside and out.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ Append("...[B][C]") ]])
	fnCutscene([[ Append("Hours passed as the orgy raged.[P] The moon continued its arc across the sky.[P] Soon, it would set, and pass behind the trees.[B][C]") ]])
	fnCutscene([[ Append("With the loss of their patron moon, the cats felt themselves tire.[P] Orgasm after orgasm had taken its toll.[P] The cats soon fell asleep in a great pile.[P] Mei cuddled against the new fang and slipped into oblivion.[B][C]") ]])
	fnCutscene([[ Append("...[B][C]") ]])
	fnCutscene([[ Append("When she woke, most of the cats had gone.[P] The new fang was nowhere in sight.[P] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[P] She could taste the new fang there, and she tasted strong.[B][C]") ]])
	fnCutscene([[ Append("Mei left the clearing and made her way back to the camp she had visited earlier.[P] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[P] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Change maps. Go back to the campsite.
	fnCutscene([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
	fnCutsceneBlocker()
end
