-- |[ ============================= Trannadar Intro Scene: Werecat ============================= ]|
--If Mei is a werecat the first time she enters Trannadar Trading Post, this scene plays.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N", 1.0)

-- |[Move In From Edge]|
--Determine which side the player entered from.
local bIsPlayerAtBottom = false
local bIsPlayerAtTop = false
EM_PushEntity("Adventure Party", 0)
	local iPlayerX, iPlayerY = TA_GetProperty("Position")
DL_PopActiveObject()
if(iPlayerY >= 37 * 16) then bIsPlayerAtBottom = true end
if(iPlayerY <   7 * 16) then bIsPlayerAtTop = true end

--Wait a bit for the fade.
fnCutsceneWait(15)
fnCutsceneBlocker()

-- |[Movement]|
--Mei walks towards Nadia and Blythe. This is based on where she spawned.
if(bIsPlayerAtBottom) then
    fnCutsceneMove("Mei", 28.25, 38.50)
elseif(bIsPlayerAtTop) then
    fnCutsceneMove("Mei", 23.25, 6.50)
else
    fnCutsceneMove("Mei", 43.25, 28.50)
end
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(5)
fnCutsceneBlocker()

--Change the music.
fnCutscene([[ AudioManager_PlayMusic("NadiasTheme") ]])

--Face Nadia and Blythe south.
if(bIsPlayerAtBottom) then
    fnCutsceneFace("Nadia", 0, 1)
    fnCutsceneFace("Blythe", 0, 1)

--Face north.
elseif(bIsPlayerAtTop) then
    fnCutsceneFace("Nadia", 0, -1)
    fnCutsceneFace("Blythe", 0, -1)
    
--Face them east.
else
    fnCutsceneFace("Nadia", 1, 0)
    fnCutsceneFace("Blythe", 1, 0)
end
fnCutsceneWait(5)
fnCutsceneBlocker()

-- |[Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Nadia", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Blythe", "Neutral") ]])
fnCutscene([[ Append("Man: ...[P] because the traders have been complaining, is why![B][C]") ]])
fnCutscene([[ Append("Alraune: Look, all I did was rearrange some of the flowers into a naughty word when I was off-duty.[P] Am I not allowed to do that?[B][C]") ]])
fnCutscene([[ Append("Man: It's more about the commitment to professionalism.[P] We are professionals.[B][C]") ]])
fnCutscene([[ Append("Alraune: What does that mean?[B][C]") ]])
fnCutscene([[ Append("Man: It means you're going to make sure this werecat knows who's in charge.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] ...[P] Hello?[B][C]") ]])
fnCutscene([[ Append("Alraune: Hello, my werecat friend![P] How are you?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Am I interrupting something?[B][C]") ]])
fnCutscene([[ Append("Alraune: Oh, not really.[P] My boss here is just helping me get used to my job.[B][C]") ]])
fnCutscene([[ Append("Nadia: I'm Nadia, and this is Cap'n Blythe![P] We're best friends![B][C]") ]])
fnCutscene([[ Append("Blythe: Nadia, be professional.[B][C]") ]])
fnCutscene([[ Append("Nadia: I still don't know what that means![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] I think he wants you to address me formally, and follow the rules of polite discourse.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Smirk] So, Miss Nadia, do you know anything about Earth?[B][C]") ]])
fnCutscene([[ Append("Nadia: Ooh![P] I get it![B][C]") ]])
fnCutscene([[ Append("Nadia: Well, Miss Werecat, I don't know anything about Earth.[P] However, I can inform you that one of our merchants here, Miss Florentina, can likely assist you.[B][C]") ]])
fnCutscene([[ Append("Nadia: I don't believe I got your name.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Smirk] I go by Mei.[P] Thank you.[B][C]") ]])
fnCutscene([[ Append("Nadia: Very good, Miss Mei.[P] Our policy is that violence is strictly prohibited.[P] If you require assistance with anything, any of the guards would be glad to provide it.[B][C]") ]])
fnCutscene([[ Append("Nadia: Major disputes should be brought directly to Captain Blythe.[P] Thank you for your business![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Smirk] Thank you for your hospitality.[B][C]") ]])
fnCutscene([[ Append("Man: .[P].[P].[P] Stunning.[B][C]") ]])
fnCutscene([[ Append("Nadia: I did good![P] I did good didn't I?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] I suppose I'll go talk to this Florentina person, then...") ]])
fnCutsceneBlocker()

-- |[Finish Up]|
--Change the music.
fnCutscene([[ AudioManager_PlayMusic("TownTheme") ]])
fnCutsceneBlocker()

--Unlock topics.
WD_SetProperty("Unlock Topic", "Alraunes", 1)
WD_SetProperty("Unlock Topic", "Blythe", 1)
WD_SetProperty("Unlock Topic", "Cultists", 1)
WD_SetProperty("Unlock Topic", "Florentina", 1)
WD_SetProperty("Unlock Topic", "Job", 1)
WD_SetProperty("Unlock Topic", "Nadia", 1)
WD_SetProperty("Unlock Topic", "Name", 1)
WD_SetProperty("Unlock Topic", "TradingPost", 1)
WD_SetProperty("Unlock Topic", "Challenge", 1)
