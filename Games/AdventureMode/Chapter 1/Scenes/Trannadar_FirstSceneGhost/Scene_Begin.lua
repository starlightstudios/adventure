-- |[ ============================== Trannadar Intro Scene: Ghost ============================== ]|
--If Mei is a Wisphag the first time she enters Trannadar Trading Post, this scene plays.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N", 1.0)

-- |[Move In From Edge]|
--Determine which side the player entered from.
local bIsPlayerAtBottom = false
local bIsPlayerAtTop = false
EM_PushEntity("Adventure Party", 0)
	local iPlayerX, iPlayerY = TA_GetProperty("Position")
DL_PopActiveObject()
if(iPlayerY >= 37 * 16) then bIsPlayerAtBottom = true end
if(iPlayerY <   7 * 16) then bIsPlayerAtTop = true end

--Wait a bit for the fade.
fnCutsceneWait(15)
fnCutsceneBlocker()

-- |[Movement]|
--Mei walks towards Nadia and Blythe. This is based on where she spawned.
if(bIsPlayerAtBottom) then
    fnCutsceneMove("Mei", 28.25, 38.50)
elseif(bIsPlayerAtTop) then
    fnCutsceneMove("Mei", 23.25, 6.50)
else
    fnCutsceneMove("Mei", 43.25, 28.50)
end
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(5)
fnCutsceneBlocker()

--Change the music.
fnCutscene([[ AudioManager_PlayMusic("NadiasTheme") ]])

--Face Nadia and Blythe south.
if(bIsPlayerAtBottom) then
    fnCutsceneFace("Nadia", 0, 1)
    fnCutsceneFace("Blythe", 0, 1)

--Face north.
elseif(bIsPlayerAtTop) then
    fnCutsceneFace("Nadia", 0, -1)
    fnCutsceneFace("Blythe", 0, -1)
    
--Face them east.
else
    fnCutsceneFace("Nadia", 1, 0)
    fnCutsceneFace("Blythe", 1, 0)
end
fnCutsceneWait(5)
fnCutsceneBlocker()

-- |[Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Nadia", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Blythe", "Neutral") ]])
fnCutscene([[ Append("Man: ...[P] look, that's not what matters.[P] What I really want to know is how you got the cow on the roof.[B][C]") ]])
fnCutscene([[ Append("Alraune: I think it'd be more important to know how I got it [P]*off*[P] the roof![B][C]") ]])
fnCutscene([[ Append("Alraune: Hey...[P] B-[P]b-[P]b-[P]boss?[P] Are you seeing what I'm seeing?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Oh, hello.[B][C]") ]])
fnCutscene([[ Append("Alraune: It's a g-[P]g-[P]g-[P]g-[P]ghooooost![P] EEEEEEEE!!!![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Surprise] P-[P]please don't be afraid![P] I'm not trying to scare you![B][C]") ]])
fnCutscene([[ Append("Man: Nadia, get a hold of yourself.[P] She seems lucid enough.[B][C]") ]])
fnCutscene([[ Append("Alraune: Huh?[P] I'm not scared![B][C]") ]])
fnCutscene([[ Append("Alraune: Miss Ghost Lady, you have the CUTEST outfit I've ever seen![P] Oh my gosh it looks so good on you![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Thank.[P] You?[B][C]") ]])
fnCutscene([[ Append("Man: Ugh, Nadia...[B][C]") ]])
fnCutscene([[ Append("Man: My apologies, ma'am.[P] She's new at this.[B][C]") ]])
fnCutscene([[ Append("Blythe: I am Captain Darius Blythe, head of security here.[P] This is Nadia, our newest recruit.[B][C]") ]])
fnCutscene([[ Append("Nadia: Hi![P] Who does your hair?[B][C]") ]])
fnCutscene([[ Append("Blythe: I don't believe we've ever had a guest of your particular species here, and as you seem to be in control of your faculties, I will allow you access to the post.[B][C]") ]])
fnCutscene([[ Append("Nadia: Ooh![P] But -[P] no violence, or we'll kick you out.[P] Right, boss?[B][C]") ]])
fnCutscene([[ Append("Blythe: That is correct.[P] No matter your urges, you must be civil here.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] I wasn't looking for a fight.[P] Actually, I need to find a way back to Earth.[B][C]") ]])
fnCutscene([[ Append("Nadia: Earth?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] You've heard of it?[B][C]") ]])
fnCutscene([[ Append("Nadia: Nope![P] But if anyone would know, it'd be Florentina, at the general goods shop![P] You should go talk to her![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Oh, thank you, Nadia.[B][C]") ]])
fnCutscene([[ Append("Nadia: No really, who does your hair?[P] How do you get it transparent like that?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] *sigh*") ]])
fnCutsceneBlocker()

-- |[Finish Up]|
--Change the music.
fnCutscene([[ AudioManager_PlayMusic("TownTheme") ]])
fnCutsceneBlocker()

--Unlock topics.
WD_SetProperty("Unlock Topic", "Alraunes", 1)
WD_SetProperty("Unlock Topic", "Blythe", 1)
WD_SetProperty("Unlock Topic", "Cultists", 1)
WD_SetProperty("Unlock Topic", "Florentina", 1)
WD_SetProperty("Unlock Topic", "Job", 1)
WD_SetProperty("Unlock Topic", "Nadia", 1)
WD_SetProperty("Unlock Topic", "Name", 1)
WD_SetProperty("Unlock Topic", "TradingPost", 1)
WD_SetProperty("Unlock Topic", "Challenge", 1)
