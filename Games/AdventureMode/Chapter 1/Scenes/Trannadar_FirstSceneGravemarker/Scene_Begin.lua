-- |[ =========================== Trannadar Intro Scene: Gravemarker =========================== ]|
--If Mei is a gravemarker the first time she enters Trannadar Trading Post, this scene plays.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N", 1.0)

-- |[Move In From Edge]|
--Determine which side the player entered from.
local bIsPlayerAtBottom = false
local bIsPlayerAtTop = false
EM_PushEntity("Adventure Party", 0)
	local iPlayerX, iPlayerY = TA_GetProperty("Position")
DL_PopActiveObject()
if(iPlayerY >= 37 * 16) then bIsPlayerAtBottom = true end
if(iPlayerY <   7 * 16) then bIsPlayerAtTop = true end

--Wait a bit for the fade.
fnCutsceneWait(15)
fnCutsceneBlocker()

-- |[Movement]|
--Mei walks towards Nadia and Blythe. This is based on where she spawned.
if(bIsPlayerAtBottom) then
    fnCutsceneMove("Mei", 28.25, 38.50)
elseif(bIsPlayerAtTop) then
    fnCutsceneMove("Mei", 23.25, 6.50)
else
    fnCutsceneMove("Mei", 43.25, 28.50)
end
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(5)
fnCutsceneBlocker()

--Change the music.
fnCutscene([[ AudioManager_PlayMusic("NadiasTheme") ]])

--Face Nadia and Blythe south.
if(bIsPlayerAtBottom) then
    fnCutsceneFace("Nadia", 0, 1)
    fnCutsceneFace("Blythe", 0, 1)

--Face north.
elseif(bIsPlayerAtTop) then
    fnCutsceneFace("Nadia", 0, -1)
    fnCutsceneFace("Blythe", 0, -1)
    
--Face them east.
else
    fnCutsceneFace("Nadia", 1, 0)
    fnCutsceneFace("Blythe", 1, 0)
end
fnCutsceneWait(5)
fnCutsceneBlocker()

-- |[Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Nadia", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Blythe", "Neutral") ]])
fnCutscene([[ Append("Man: ...[P] Because it's okay if you can't count past four.[P] The problem is that you're lying about it.[B][C]") ]])
fnCutscene([[ Append("Alraune: Hey, I can count past four![B][C]") ]])
fnCutscene([[ Append("Man: Okay, then do it.[B][C]") ]])
fnCutscene([[ Append("Alraune: One, two, three, four, six, eight.[B][C]") ]])
fnCutscene([[ Append("Man: That's not right![B][C]") ]])
fnCutscene([[ Append("Alraune: I didn't say it was right, but I definitely counted past four![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Excuse me, am I interrupting?[B][C]") ]])
fnCutscene([[ Append("Man: Not at all.[P] Nadia, standard procedure.[B][C]") ]])
fnCutscene([[ Append("Nadia: Oh, right![P] Uh, okay, think think...[B][C]") ]])
fnCutscene([[ Append("Nadia: ...[B][C]") ]])
fnCutscene([[ Append("Nadia: ...[B][C]") ]])
fnCutscene([[ Append("Nadia: *Hey, buddy, help me out here!*[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Surprise] *With what?*[B][C]") ]])
fnCutscene([[ Append("Nadia: *I forgot what I'm supposed to say first!*[B][C]") ]])
fnCutscene([[ Append("Man: Nadia, you can't ask visitors for help with your guard work![B][C]") ]])
fnCutscene([[ Append("Nadia: I'm sorry Cap'n![P] I'm really trying![B][C]") ]])
fnCutscene([[ Append("Nadia: Okay, the second thing I'm supposed to say is that, if you have a problem, please take it to the guards.[P] Violence is prohibited.[B][C]") ]])
fnCutscene([[ Append("Nadia: Third thing was...[P] uh...[P] right![P] Major disputes should be taken to Cap'n Blythe here![B][C]") ]])
fnCutscene([[ Append("Blythe: Two out of three isn't bad, I suppose.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Smirk] Er, could you maybe help me?[P] I'm looking for a way back to Earth.[B][C]") ]])
fnCutscene([[ Append("Nadia: You're standing on it.[P] Hey, do those wings work?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] I think I'm a bit too heavy to fly with these.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] And by Earth I meant the planet.[B][C]") ]])
fnCutscene([[ Append("Nadia: Nuts, is this because I messed up the first part?[B][C]") ]])
fnCutscene([[ Append("Blythe: No, Nadia.[P] She is clearly from a far away place.[B][C]") ]])
fnCutscene([[ Append("Blythe: Madam, my advice would be to speak to Florentina.[P] She runs the general store.[B][C]") ]])
fnCutscene([[ Append("Blythe: She is knowledgeable of many places, and could at least point you in the right direction.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Smirk] Oh, thank you.[P] And good work, Nadia![P] Keep trying hard![B][C]") ]])
fnCutscene([[ Append("Nadia: Boss![P] Boss![P] I got a compliment![B][C]") ]])
fnCutscene([[ Append("Blythe: Oh dear, now she'll be going on about it for days...") ]])
fnCutsceneBlocker()

-- |[Finish Up]|
--Change the music.
fnCutscene([[ AudioManager_PlayMusic("TownTheme") ]])
fnCutsceneBlocker()

--Unlock topics.
WD_SetProperty("Unlock Topic", "Alraunes", 1)
WD_SetProperty("Unlock Topic", "Blythe", 1)
WD_SetProperty("Unlock Topic", "Cultists", 1)
WD_SetProperty("Unlock Topic", "Florentina", 1)
WD_SetProperty("Unlock Topic", "Job", 1)
WD_SetProperty("Unlock Topic", "Nadia", 1)
WD_SetProperty("Unlock Topic", "Name", 1)
WD_SetProperty("Unlock Topic", "TradingPost", 1)
WD_SetProperty("Unlock Topic", "Challenge", 1)
