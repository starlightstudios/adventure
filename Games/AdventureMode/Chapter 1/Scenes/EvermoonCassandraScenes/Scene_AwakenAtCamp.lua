-- |[ ===================================== Awaken At Camp ===================================== ]|
--Scene plays after Mei and/or Florentina wake up the next day at the campsite. What is said depends on what exactly happened.

-- |[Setup]|
--Variables.
local sMeiForm                    = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local bIsFlorentinaPresent        = fnIsCharacterPresent("Florentina")
local iSavedCassandra             = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedCassandra", "N")
local iTurnedCassandra            = VM_GetVar("Root/Variables/Chapter1/Scenes/iTurnedCassandra", "N")
local iWonFightWithCassandra      = VM_GetVar("Root/Variables/Chapter1/Scenes/iWonFightWithCassandra", "N")
local iHasSeenTrannadarFirstScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N")
local iMetNadiaInWerecatScene     = VM_GetVar("Root/Variables/Chapter1/ScenesWerecat/iMetNadiaInWerecatScene", "N")

--Whether Mei mentions Nadia depends on this:
local bHasMetNadia = false
if(iHasSeenTrannadarFirstScene == 1.0 or iMetNadiaInWerecatScene == 1.0) then bHasMetNadia = true end

-- |[Achievement]|
AM_SetPropertyJournal("Unlock Achievement", "WereCatsandra")
        
--Activate Rest.
fnCutscene([[ AM_SetProperty("Execute Rest") ]])

--Unload any pending Cassandra/Mei TF scenes.
fnUnloadBitmapsFromList("Chapter 1 Cassandra")
fnUnloadBitmapsFromList("Chapter 1 Mei Werecat TF")

-- |[ ==================================== Scene Execution ===================================== ]|
--Blackout.
AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1)

--If Cassandra was saved, spawn her.
if(iSavedCassandra == 1.0) then
    fnStandardNPCByPosition("Cassandra")
end

--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()

--Reposition party.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Teleport To", (22.25 * gciSizePerTile), (18.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face",  0, 1)
DL_PopActiveObject()
if(bIsFlorentinaPresent == true) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Teleport To", (23.25 * gciSizePerTile), (19.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", -1, 0)
	DL_PopActiveObject()
end

--Fade in.
fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneWait(45)
fnCutsceneBlocker()

-- |[Dialogue]|
--In the case that Mei won the fight with a turned-Cassandra:
if(iWonFightWithCassandra == 1.0) then

	--Mei is alone:
	if(bIsFlorentinaPresent == false) then
		fnStandardMajorDialogue()
		fnCutscene([[ Append("Mei:[E|Neutral] (All right, rest's over.[P] Back to the adventure.)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] (If only I had been faster...)") ]])
		fnCutsceneBlocker()
	
	--Florentina is present:
	else
	
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] All right, rest's over.[P] Back to the adventure.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] If only we had been faster...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Don't beat yourself up, kid.[P] You tried.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] That's really all that can be asked of you.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] But...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] You didn't have to get involved.[P] Trying and failing is better than not trying at all.[P] You did good on that count.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] Thanks for trying to cheer me up, but I still don't feel good about it...") ]])
		fnCutsceneBlocker()
		
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (22.25 * gciSizePerTile), (18.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	end
	
--Setup:
elseif(bIsFlorentinaPresent == false) then
	
	--Base:
	fnStandardMajorDialogue()
	
	--If Mei turned Cassandra:
	if(iTurnedCassandra == 1.0) then
		fnCutscene([[ Append("Mei:[E|Neutral] Ahhhhhh...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Smirk] Better get back to my quest, then.[P] That new fang...[P] there was something truly odd about her.[P] I didn't even get her name...") ]])
	
	--If Mei let Cassandra turn her:
	elseif(iTurnedCassandra == 2.0) then
		
		--Setup.
		fnCutscene([[ Append("Mei:[E|Neutral] Ahhhh...[P] Incredible...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] This is fantastic![P] I can see, hear and smell better than ever...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Smirk] I suppose that girl did me a favour, drawing me into the forest so the cats could take me.[P] I'll have to find her and return the favour someday.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Smirk] But for now, I must hunt for a way home.[P] The most challenging hunt of all!") ]])
		fnCutsceneBlocker()
		
	--If Mei saved Cassandra:
	elseif(iSavedCassandra == 1.0) then
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Are you all right, miss?[B][C]") ]])
		fnCutscene([[ Append("Lady: Yes, thank you.[B][C]") ]])
		
        --Variables.
        local iHasSeenTrannadarFirstScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N")
        
		--If Mei is a werecat:
		if(sMeiForm == "Werecat") then
			fnCutscene([[ Append("Lady: But, why did you help me?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Smirk] You're strong.[P] It took many cats to take you down.[P] They don't deserve you.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Smirk] One of our rules is that we do not hunt with more fangs than prey.[P] It seems that they are lax in following it.[B][C]") ]])
			fnCutscene([[ Append("Lady: Oh, I see.[P] I have a re- [P][CLEAR]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] No reward is necessary, miss..?[B][C]") ]])
			fnCutscene([[ Append("Cassandra: Cassandra of Jeffespeir.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Mei...[P] of Hong Kong, I guess.[B][C]") ]])
			fnCutscene([[ Append("Cassandra: Hong Kong?[P] Never heard of it.[P] Is it far away?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Sad] Further than even I know...[P] I'm looking for a way home, actually.[B][C]") ]])
			fnCutscene([[ Append("Cassandra: Oh, I see.[P] I'm afraid I can't help you there.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] What were you doing alone in the forest?[B][C]") ]])
			fnCutscene([[ Append("Cassandra: I -[P] I'm not sure, actually.[P] I don't really remember how I got here.[P] I must have hurt my head.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Let me see...[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] ...[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] No, I don't see any sign of injury.[P] You can't remember?[B][C]") ]])
			fnCutscene([[ Append("Cassandra: No, not at all.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Offended] You should definitely get someplace safe until you recover your memory.[B][C]") ]])
            
            if(iHasSeenTrannadarFirstScene == 1.0) then
                fnCutscene([[ Append("Mei:[E|Neutral] The trading post southwest of here is safe.[P] Would you like me to take you there?[B][C]") ]])
            else
                fnCutscene([[ Append("Mei:[E|Neutral] My map says there's a trading post southwest of here.[P] It's probably safer there.[P] Would you like me to take you there?[B][C]") ]])
            end
            
			fnCutscene([[ Append("Cassandra: Oh, I'll be fine, now.[P] The cats got the jump on me last time, but I can handle myself.[P] Really.[B][C]") ]])
            if(bHasMetNadia == true) then
                fnCutscene([[ Append("Mei:[E|Neutral] All right.[P] Tell Nadia I sent you.[P] She'll take care of you.[B][C]") ]])
            else
                fnCutscene([[ Append("Mei:[E|Neutral] All right.[P] Hopefully someone there can help you.[B][C]") ]])
            end
			fnCutscene([[ Append("Cassandra: I need to get my things before I head out.[P] Thank you, Mei.[P] Good luck finding your way home.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Happy] Same to you, Cassandra.[P] May our paths cross again!") ]])
			fnCutsceneBlocker()
		
		--If Mei is anything else:
		else
			fnCutscene([[ Append("Lady: But, why did you help me?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Smirk] I always help someone in need.[B][C]") ]])
			fnCutscene([[ Append("Lady: Oh, I see.[P] I have a re- [P][CLEAR]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] No reward is necessary, miss..?[B][C]") ]])
			fnCutscene([[ Append("Cassandra: Cassandra of Jeffespeir.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Mei...[P] of Hong Kong, I guess.[B][C]") ]])
			fnCutscene([[ Append("Cassandra: Hong Kong?[P] Never heard of it.[P] Is it far away?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Sad] Further than even I know...[P] I'm looking for a way home, actually.[B][C]") ]])
			fnCutscene([[ Append("Cassandra: Oh, I see.[P] I'm afraid I can't help you there.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] What were you doing alone in the forest?[B][C]") ]])
			fnCutscene([[ Append("Cassandra: I -[P] I'm not sure, actually.[P] I don't really remember how I got here.[P] I must have hurt my head.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Let me see...[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] ...[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] No, I don't see any sign of injury.[P] You can't remember?[B][C]") ]])
			fnCutscene([[ Append("Cassandra: No, not at all.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Offended] You should definitely get someplace safe until you recover your memory.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] The trading post southwest of here is safe.[P] Would you like me to take you there?[B][C]") ]])
			fnCutscene([[ Append("Cassandra: Oh, I'll be fine, now.[P] The cats got the jump on me last time, but I can handle myself.[P] Really.[B][C]") ]])
            if(bHasMetNadia == true) then
                fnCutscene([[ Append("Mei:[E|Neutral] All right.[P] Tell Nadia I sent you.[P] She'll take care of you.[B][C]") ]])
            else
                fnCutscene([[ Append("Mei:[E|Neutral] All right.[P] Hopefully someone there can help you.[B][C]") ]])
            end
			fnCutscene([[ Append("Cassandra: I need to get my things before I head out.[P] Thank you, Mei.[P] Good luck finding your way home.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Happy] Same to you, Cassandra.[P] May our paths cross again!") ]])
			fnCutsceneBlocker()
		end
	end

--If Florentina is present:
else
	
	--If Mei turned Cassandra:
	if(iTurnedCassandra == 1.0) then
		
		--Setup.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Ahhhh...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Surprise] And here I was thinking you were going to try to save that girl.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Oh, I hadn't intended to, really.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] I recall you saying 'If someone calls for help, you help', right?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I did help.[P] I improved her.[P] She's now a hundred times the hunter she was.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Okay, you win that one on a technicality.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I was honestly expecting you to lecture me on this.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Pah.[P] Let what comes, come, and what goes, go.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Hmmm...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I didn't get her name, though.[P] There was definitely something odd about her.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Blush] Whatever.[P] It was fun to watch -[P] I'm not into cats, but...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] So back to business, right?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Smirk] Naturally![P] Let's go!") ]])
		fnCutsceneBlocker()
	
	--If Mei let Cassandra turn her:
	elseif(iTurnedCassandra == 2.0) then
		
		--Setup.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Ahhhh...[P] Incredible...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Well it seems you got what you wanted out of this.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] This is fantastic![P] I can see, hear and smell better than ever...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Uh huh.[P] What about that girl that we were trying to save?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Smirk] As far as I'm concerned, we did save her.[P] She'll be so much better this way.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Hey, we've still got a job to do.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Smirk] Of course.[P] The hunt for a way home will be the most challenging hunt of all.[P] Will you join my pride?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Yeah, whatever.[P] Let's get going.") ]])
		fnCutsceneBlocker()
	
	--If Mei saved Cassandra:
	elseif(iSavedCassandra == 1.0) then
		fnStandardMajorDialogue()
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Are you all right, miss?[B][C]") ]])
		fnCutscene([[ Append("Lady: Yes, thank you.[B][C]") ]])
		
		--If Mei is a werecat:
		if(sMeiForm == "Werecat") then
			fnCutscene([[ Append("Lady: But, why did you help me?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Smirk] You're strong.[P] It took many cats to take you down.[P] They don't deserve you.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Smirk] One of our rules is that we do not hunt with more fangs than prey.[P] It seems that they are lax in following it.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] This is a cat compliment.[P] I suggest you take it.[B][C]") ]])
			fnCutscene([[ Append("Lady: Oh, I see.[P] I have a re- [P][CLEAR]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] No reward is necessary.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] Belay that.[P] I'll take -[P][CLEAR]") ]])
			fnCutscene([[ Append("Mei:[E|Offended] No reward is necessary![B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Surprise] Jeez![P] Okay, okay![B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] *You can pass me a reward later when she's not looking.*[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Smirk] Doing the right thing is its own reward, Florentina.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Happy] And money is like icing on a cake of good intentions.[P] What's your point?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] *sigh*[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Just ignore her, miss..?[B][C]") ]])
			fnCutscene([[ Append("Cassandra: Cassandra of Jeffespeir.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Mei...[P] of Hong Kong, I guess.[B][C]") ]])
			fnCutscene([[ Append("Cassandra: Hong Kong?[P] Never heard of it.[P] Is it far away?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Sad] Further than even I know...[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] We're currently trying to get her back home.[P] And apparently righting wrongs while we're at it.[B][C]") ]])
			fnCutscene([[ Append("Cassandra: Oh, superb![P] The world needs more people like you.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] What were you doing alone in the forest?[B][C]") ]])
			fnCutscene([[ Append("Cassandra: I -[P] I'm not sure, actually.[P] I don't really remember how I got here.[P] I must have hurt my head.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Let me see...[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] ...[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] No, I don't see any sign of injury.[P] You can't remember?[B][C]") ]])
			fnCutscene([[ Append("Cassandra: No, not at all.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Offended] You should definitely get someplace safe until you recover your memory.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] The trading post southwest of here is safe.[P] Would you like me to take you there?[B][C]") ]])
			fnCutscene([[ Append("Cassandra: Oh, I'll be fine, now.[P] The cats got the jump on me last time, but I can handle myself.[P] Really.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] All right.[P] Tell Nadia I sent you.[P] She'll take care of you.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] As much as this goes against my better judgement...[P] If you need something, tell Hypatia I said you could have a discount.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Offended] She gets a discount but I don't?[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Blush] I'm a sucker for pigtails, what can I say?[B][C]") ]])
			fnCutscene([[ Append("Cassandra: I need to get my things before I head out.[P] Thank you, Mei and Florentina.[P] Good luck finding your way home.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Happy] Same to you, Cassandra.[P] May our paths cross again!") ]])
			fnCutsceneBlocker()
		
		--If Mei is anything else:
		else
			fnCutscene([[ Append("Lady: But, why did you help me?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Smirk] I always help someone in need.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] She's what is known in the psychiatric community as an 'idiot'.[B][C]") ]])
			fnCutscene([[ Append("Lady: Oh, I see.[P] I have a re- [P][CLEAR]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] No reward is necessary.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] Belay that.[P] I'll take -[P][CLEAR]") ]])
			fnCutscene([[ Append("Mei:[E|Offended] No reward is necessary![B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Surprise] Jeez![P] Okay, okay![B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] *You can pass me a reward later when she's not looking.*[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Smirk] Doing the right thing is its own reward, Florentina.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Happy] And money is like icing on a cake of good intentions.[P] What's your point?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] *sigh*[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Just ignore her, miss..?[B][C]") ]])
			fnCutscene([[ Append("Cassandra: Cassandra of Jeffespeir.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Mei...[P] of Hong Kong, I guess.[B][C]") ]])
			fnCutscene([[ Append("Cassandra: Hong Kong?[P] Never heard of it.[P] Is it far away?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Sad] Further than even I know...[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] We're currently trying to get her back home.[P] And apparently righting wrongs while we're at it.[B][C]") ]])
			fnCutscene([[ Append("Cassandra: Oh, superb![P] The world needs more people like you.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] What were you doing alone in the forest?[B][C]") ]])
			fnCutscene([[ Append("Cassandra: I -[P] I'm not sure, actually.[P] I don't really remember how I got here.[P] I must have hurt my head.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Let me see...[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] ...[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] No, I don't see any sign of injury.[P] You can't remember?[B][C]") ]])
			fnCutscene([[ Append("Cassandra: No, not at all.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Offended] You should definitely get someplace safe until you recover your memory.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] The trading post southwest of here is safe.[P] Would you like me to take you there?[B][C]") ]])
			fnCutscene([[ Append("Cassandra: Oh, I'll be fine, now.[P] The cats got the jump on me last time, but I can handle myself.[P] Really.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] All right.[P] Tell Nadia I sent you.[P] She'll take care of you.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] As much as this goes against my better judgement...[P] If you need something, tell Hypatia I said you could have a discount.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Offended] She gets a discount but I don't?[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Blush] I'm a sucker for pigtails, what can I say?[B][C]") ]])
			fnCutscene([[ Append("Cassandra: I need to get my things before I head out.[P] Thank you, Mei and Florentina.[P] Good luck finding your way home.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Happy] Same to you, Cassandra.[P] May our paths cross again!") ]])
			fnCutsceneBlocker()
		end
	end
	
	--Florentina walks onto Mei and folds the party.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (22.25 * gciSizePerTile), (18.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
end
