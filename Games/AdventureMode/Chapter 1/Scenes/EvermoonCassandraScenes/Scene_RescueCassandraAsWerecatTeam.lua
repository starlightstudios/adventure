-- |[Combat Victory]|
--Scene plays if Mei defeats the werecats, in a team, as a werecat.
VM_SetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N", 2.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iSavedCassandra", "N", 1.0)

--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()

--Switch back to the forest music.
fnCutscene([[ AL_SetProperty("Music", "Null") ]])
fnCutsceneWait(1)
	
-- |[Dialogue]|
--Set variable:
VM_SetVar("Root/Variables/Chapter1/Scenes/iEncounteredCatCampsite", "N", 2.0)

--Setup.
fnStandardMajorDialogue()
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Werecat", "Neutral") ]])
fnCutscene([[ Append("Florentina:[E|Happy] Oh come on, get up![P] It was just getting good![B][C]") ]])
fnCutscene([[ Append("Werecat: Y-[P]you...[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] Do you want to be next?[P] Hm?[P] Come, bring your fury![B][C]") ]])
fnCutscene([[ Append("Werecat: ...[P] Take her, strong one...[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Laugh] Ha ha![P] Come along, human!") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(60)
fnCutsceneBlocker()

--Fade to black.
fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneWait(120)
fnCutsceneBlocker()

--Setup.
fnStandardMajorDialogue()
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
fnCutscene([[ Append("Mei:[E|Offended] Hey, are you still awake?[B][C]") ]])
fnCutscene([[ Append("Lady: Cure -[P] the cure is in my pack...[P] Hurry...[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] Yeah, she's got some Pickled Tetterleaf in here.[P] It'll work.[P] Here.[B][C]") ]])
fnCutscene([[ Append("Lady: *gulp*[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] ...[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Looks like she passed out.[P] We better get her someplace safe...") ]])
fnCutsceneBlocker()

fnCutsceneWait(120)
fnCutsceneBlocker()
	
--Change maps. Go back to the campsite.
fnCutscene([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
fnCutsceneBlocker()
