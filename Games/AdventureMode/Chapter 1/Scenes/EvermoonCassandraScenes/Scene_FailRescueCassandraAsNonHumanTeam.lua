-- |[Fail to Rescue Cassandra: With Florentina, Mei is Non-Human]|
--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()

--Variables.
VM_SetVar("Root/Variables/Chapter1/Scenes/iWonFightWithCassandra", "N", 1.0)

--Switch back to the forest music.
fnCutscene([[ AL_SetProperty("Music", "Null") ]])
fnCutsceneWait(1)

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Werecat") ]])
	
--Talking.
fnCutscene([[ Append("Mei:[E|Offended] You are defeated![P] Yield![B][C]") ]])
fnCutscene([[ Append("Fang: Gaahhh...[P] so strong...[B][C]") ]])
fnCutscene([[ Append("Fang: We apologize...[P] spare us...[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] They're not so tough, are they?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] We're not going to hurt you any more unless you make us.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] *sigh*[P] But I guess it's too late for you.[B][C]") ]])
fnCutscene([[ Append("Fang: ...[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] We tried.[P] She's one of them now.[B][C]") ]])
fnCutscene([[ Append("Fang: You are strong.[P] We will respect you.[P] Leave now.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] C'mon Florentina, there's nothing left for us here.") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()

--Fade to black.
fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneWait(60)
fnCutsceneBlocker()
	
--Change maps. Go back to the campsite.
fnCutscene([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
fnCutsceneBlocker()
