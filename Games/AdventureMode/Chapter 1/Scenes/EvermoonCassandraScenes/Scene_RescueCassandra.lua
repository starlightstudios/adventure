-- |[Rescue Cassandra]|
--Scene that plays when Cassandra can be rescued. Can occur in one of four locations.
local sMeiForm             = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local sCassandraLocation   = VM_GetVar("Root/Variables/Chapter1/Scenes/sCassandraLocation", "S")
local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
local iCassandraEncounters = VM_GetVar("Root/Variables/Chapter1/Scenes/iCassandraEncounters", "N")

--Once this scene starts, the event is considered over.
VM_SetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N", 2.0)

--Clear censor bars.
WD_SetProperty("Clear Censor Bars")

-- |[Camera Refocus]|
--Focus the camera on Cassandra. Coordinates depend on the map, but it's always on the same actor.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 2.0)
	CameraEvent_SetProperty("Focus Actor Name", "Cassandra")
DL_PopActiveObject()

--The party moves into position. This is map-specific.
local bFlorentinaUseY = false
local fMeiCoordX = 0.0
local fMeiCoordY = 0.0
if(sCassandraLocation == "EvermoonCassandraCC") then
	fMeiCoordX = 14.25
	fMeiCoordY =  7.50
elseif(sCassandraLocation == "EvermoonCassandraCNW") then
	fMeiCoordX = 10.25
	fMeiCoordY = 10.50
elseif(sCassandraLocation == "EvermoonCassandraCNE") then
	fMeiCoordX = 13.25
	fMeiCoordY = 12.50
	bFlorentinaUseY = true
elseif(sCassandraLocation == "EvermoonCassandraCE") then
	fMeiCoordX = 34.25
	fMeiCoordY = 24.50
end

--Reposition if Florentina is present:
if(bIsFlorentinaPresent == true) then
	fMeiCoordX = fMeiCoordX - 0.50
end

--Send the coordinates.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (fMeiCoordX * gciSizePerTile), (fMeiCoordY * gciSizePerTile))
DL_PopActiveObject()
if(bIsFlorentinaPresent == true) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		if(bFlorentinaUseY == false) then
			ActorEvent_SetProperty("Move To", ((fMeiCoordX+1.0) * gciSizePerTile), (fMeiCoordY * gciSizePerTile))
		else
			ActorEvent_SetProperty("Move To", ((fMeiCoordX+1.0) * gciSizePerTile), ((fMeiCoordY+1.0) * gciSizePerTile))
		end
	DL_PopActiveObject()
end
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(60)
fnCutsceneBlocker()

-- |[Dialogue]|
--Yeah, stop them!
fnStandardMajorDialogue()
fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Werecat", "Neutral") ]])

--If Mei is alone:
if(bIsFlorentinaPresent == false) then
	
	--You're in time to save her!
	if(iCassandraEncounters < 6) then
		
		--Cassandra as a human:
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
	
		--If Mei is a human:
		if(sMeiForm == "Human") then
			
			--Flag:
			VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraDefeatHandler", "N", 2.0)
			
			--Dialogue:
			fnCutscene([[ Append("Mei:[E|Offended] Hey, cats![P] Leave her alone![B][C]") ]])
			fnCutscene([[ Append("Werecat: Purrr...[P] You look strong, human.[P] You would make a good kinfang.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Offended] This isn't a negotiation.[P] Give me the girl![B][C]") ]])
			fnCutscene([[ Append("Werecat: Ha ha ha![P] You are outnumbered, human![B][C]") ]])
			fnCutscene([[ Append("Werecat: Fangs![P] Add her to our pride!") ]])
			fnCutsceneBlocker()
			
			--Battle!
            fnCutscene([[ AdvCombat_SetProperty("World Pulse", true) ]])
			fnCutscene([[ AdvCombat_SetProperty("Reinitialize") ]])
			fnCutscene([[ AdvCombat_SetProperty("Activate") ]])
			fnCutscene([[ AdvCombat_SetProperty("Victory Script", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_RescueCassandraAsHumanSolo.lua") ]])
			fnCutscene([[ AdvCombat_SetProperty("Defeat Script", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraDefeat/Scene_Begin.lua") ]])
			fnCutscene([[ AdvCombat_SetProperty("Unretreatable", true) ]])
            fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/003 Evermoon North/Werecat Hunter.lua", 0) ]])
            fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/003 Evermoon North/Werecat Hunter.lua", 0) ]])
            fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/003 Evermoon North/Werecat Hunter.lua", 0) ]])
			fnCutsceneBlocker()
		
		--If Mei is a werecat:
		elseif(sMeiForm == "Werecat") then
			fnCutscene([[ Append("Mei:[E|Neutral] Kinfangs![P] What goes on here?[B][C]") ]])
			fnCutscene([[ Append("Werecat: Purrr...[P] This one will be turned when the moon glides over this glade.[B][C]") ]])
			fnCutscene([[ Append("Werecat: The time will come soon.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Smirk] Oooh, I'm not too late.[B][C]") ]])
			fnCutscene([[ Append("Werecat: Do you seek to join the festivities?[BLOCK]") ]])

			--Decision script is a different one.
			local sDecisionScript = "\"" .. gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Decision_AnswerWerecats.lua" .. "\""
			fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
			fnCutscene(" WD_SetProperty(\"Add Decision\", \"Let her go!\", " .. sDecisionScript .. ", \"LetHerGoSoloCat\") ")
			fnCutscene(" WD_SetProperty(\"Add Decision\", \"I will join you.\",  " .. sDecisionScript .. ", \"JoinThemSoloCat\") ")
			fnCutsceneBlocker()

		--If Mei is anything else:
		else
			
			--Flag:
			VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraDefeatHandler", "N", 3.0)
			
			--Dialogue:
			fnCutscene([[ Append("Mei:[E|Offended] Hey, cats![P] Leave her alone![B][C]") ]])
			fnCutscene([[ Append("Werecat: Purrr...[P] You look strong.[P] You would be a good challenge.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Offended] This isn't a negotiation.[P] Give me the girl![B][C]") ]])
			fnCutscene([[ Append("Werecat: Ha ha ha![P] You are outnumbered![P] You have great courage![B][C]") ]])
			fnCutscene([[ Append("Werecat: Fangs![P] Attack!") ]])
			fnCutsceneBlocker()
			
			--Battle!
            fnCutscene([[ AdvCombat_SetProperty("World Pulse", true) ]])
			fnCutscene([[ AdvCombat_SetProperty("Reinitialize") ]])
			fnCutscene([[ AdvCombat_SetProperty("Activate") ]])
			fnCutscene([[ AdvCombat_SetProperty("Victory Script", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_RescueCassandraAsWerecatSolo.lua") ]])
			fnCutscene([[ AdvCombat_SetProperty("Defeat Script", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraDefeat/Scene_Begin.lua") ]])
			fnCutscene([[ AdvCombat_SetProperty("Unretreatable", true) ]])
            fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/003 Evermoon North/Werecat Hunter.lua", 0) ]])
            fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/003 Evermoon North/Werecat Hunter.lua", 0) ]])
            fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/003 Evermoon North/Werecat Hunter.lua", 0) ]])
			fnCutsceneBlocker()
		end

	--Oh no, you're too late!
	else
		
		--Cassandra as a human:
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Werecat") ]])
	
		--Mei has werecat form:
		local iHasWerecatForm = VM_GetVar("Root/Variables/Global/Mei/iHasWerecatForm", "N")
	
		--If Mei is a human:
		if(sMeiForm == "Human" and iHasWerecatForm == 0.0) then
			
			--Flag:
			VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraDefeatHandler", "N", 7.0)
			
			--Dialogue:
			fnCutscene([[ Append("Mei:[E|Surprise] ..![P] No![P] I'm too late![B][C]") ]])
			fnCutscene([[ Append("Werecat: Purrr...[P] You look strong, human.[P] You would make a good kinfang.[P] Come to us...[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Blush] (I look strong?[P] Maybe I should go to them...)[BLOCK]") ]])
			
			--Give the player a choice:
			local sDecisionScript = "\"" .. gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Decision_AnswerWerecatsFail.lua" .. "\""
			fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
			fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Way!\", " .. sDecisionScript .. ", \"Fight\") ")
			fnCutscene(" WD_SetProperty(\"Add Decision\", \"Join Them\",  " .. sDecisionScript .. ", \"Join\") ")
			fnCutsceneBlocker()
		
		--Mei is a human and has werecat form:
		elseif(sMeiForm == "Human" and iHasWerecatForm == 1.0) then
			fnCutscene([[ Append("Mei:[E|Neutral] ..![P] I'm too late![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Blush] (The pheromones...[P] the moon...[P] so....[P] ungggghhh...[P] horrrrnnnyyy...)[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Blush] (Why even try to stop my kinfangs?[P] I'm -[P] I'm a fang too...[P] ungghhhh)[B][C]") ]])
			fnCutscene([[ Append("Werecat: Purrr...[P] You have come to join us?[P] Purrr....[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Blush] Let us...[P] celebrate this joyous occasion...") ]])

			--Wait a bit.
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			
			--Black the screen out.
			fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
			fnCutsceneWait(60)
			fnCutsceneBlocker()

			--Activate scenes mode.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Activate Scene") ]])
			fnCutscene([[ Append("Mei strode confidently into the glade, her fellow cats already well underway in the same orgy she had enjoyed when she first joined the night hunters.") ]])
			fnCutsceneBlocker()
			
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
			fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Werecat") ]])
			fnCutscene([[ Append("As she approached, she allowed the latent curse that her runestone suppressed to take over again.[P] Fur, claws, and fangs replaced her human features.[P] The werecats were too busy to notice or care.") ]])
			fnCutsceneBlocker()
			
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
			fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Werecat") ]])
			fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
			fnCutscene([[ Append("The new fang was blonde and had her eyes locked on Mei's hips as she approached.[P] Unused to her new body, the fang could hardly control the lust building inside her.[P] She practically dripped in anticipation.[B][C]") ]])
			fnCutscene([[ Append("Mei knelt in front of her and began to suck at her exposed sex.[P] The fang would have fallen in a sudden surge of pleasure, but another cat appeared behind her and groped at her chest.[P] A third soon kissed her and rubbed her gorgeous blonde fur.[B][C]") ]])
			fnCutscene([[ Append("The fang shuddered in orgasm, but was permitted not a second to rest.[P] The cats began to congregate around her, licking and sucking one another and her.[P] Mei continued to suck at her as another cat began working on Mei.[B][C]") ]])
			fnCutscene([[ Append("Her head swimming in sexual overload, Mei was barely aware of her own tongue as it slid in and out of the new fang.[P] Her delirium built with her own orgasm.[P] She continued to suck and lick.") ]])
			fnCutsceneBlocker()
			
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
			fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
			fnCutscene([[ WD_SetProperty("Set Scene Image", "Null") ]])
			fnCutscene([[ Append("...[B][C]") ]])
			fnCutscene([[ Append("Hours passed as the orgy raged.[P] The moon continued its arc across the sky.[P] Soon, it would set, and pass behind the trees.[B][C]") ]])
			fnCutscene([[ Append("With the loss of their patron moon, the cats felt themselves tire.[P] Orgasm after orgasm had taken its toll.[P] The cats soon fell asleep in a great pile.[P] Mei cuddled against the new fang and slipped into oblivion.[B][C]") ]])
			fnCutscene([[ Append("...[B][C]") ]])
			fnCutscene([[ Append("When she woke, most of the cats had gone.[P] The new fang was nowhere in sight.[P] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[P] She could taste the new fang there, and she tasted strong.[B][C]") ]])
			fnCutscene([[ Append("Mei left the clearing and made her way back to the camp she had visited earlier.[P] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[P] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
			fnCutsceneBlocker()
	
			--Wait a bit.
			fnCutsceneWait(60)
			fnCutsceneBlocker()
			
			--Change maps. Go back to the campsite.
			fnCutscene([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
			fnCutsceneBlocker()
		
		--If Mei is a werecat:
		elseif(sMeiForm == "Werecat") then
			fnCutscene([[ Append("Mei:[E|Neutral] ..![P] I'm too late![B][C]") ]])
			fnCutscene([[ Append("Werecat: Purrr...[P] You have come to join us?[P] Purrr....[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] (The pheromones...[P] the moon...[P] so....[P] ungggghhh...[P] horrrrnnnyyy...)[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Smirk] Let us...[P] celebrate this joyous occasion...") ]])

			--Wait a bit.
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			
			--Black the screen out.
			fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
			fnCutsceneWait(60)
			fnCutsceneBlocker()

			--Activate scenes mode.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Activate Scene") ]])
			fnCutscene([[ Append("Mei strode confidently into the glade, her fellow cats already well underway in the same orgy she had enjoyed when she first joined the night hunters.") ]])
			fnCutsceneBlocker()
			
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
			fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
			fnCutscene([[ Append("The new fang was blonde and had her eyes locked on Mei's hips as she approached.[P] Unused to her new body, the fang could hardly control the lust building inside her.[P] She practically dripped in anticipation.[B][C]") ]])
			fnCutscene([[ Append("Mei knelt in front of her and began to suck at her exposed sex.[P] The fang would have fallen in a sudden surge of pleasure, but another cat appeared behind her and groped at her chest.[P] A third soon kissed her and rubbed her gorgeous blonde fur.[B][C]") ]])
			fnCutscene([[ Append("The fang shuddered in orgasm, but was permitted not a second to rest.[P] The cats began to congregate around her, licking and sucking one another and her.[P] Mei continued to suck at her as another cat began working on Mei.[B][C]") ]])
			fnCutscene([[ Append("Her head swimming in sexual overload, Mei was barely aware of her own tongue as it slid in and out of the new fang.[P] Her delirium built with her own orgasm.[P] She continued to suck and lick.") ]])
			fnCutsceneBlocker()
			
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
			fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
			fnCutscene([[ WD_SetProperty("Set Scene Image", "Null") ]])
			fnCutscene([[ Append("...[B][C]") ]])
			fnCutscene([[ Append("Hours passed as the orgy raged.[P] The moon continued its arc across the sky.[P] Soon, it would set, and pass behind the trees.[B][C]") ]])
			fnCutscene([[ Append("With the loss of their patron moon, the cats felt themselves tire.[P] Orgasm after orgasm had taken its toll.[P] The cats soon fell asleep in a great pile.[P] Mei cuddled against the new fang and slipped into oblivion.[B][C]") ]])
			fnCutscene([[ Append("...[B][C]") ]])
			fnCutscene([[ Append("When she woke, most of the cats had gone.[P] The new fang was nowhere in sight.[P] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[P] She could taste the new fang there, and she tasted strong.[B][C]") ]])
			fnCutscene([[ Append("Mei left the clearing and made her way back to the camp she had visited earlier.[P] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[P] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
			fnCutsceneBlocker()
	
			--Wait a bit.
			fnCutsceneWait(60)
			fnCutsceneBlocker()
			
			--Change maps. Go back to the campsite.
			fnCutscene([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
			fnCutsceneBlocker()

		--If Mei is anything else:
		else
			
			--Flag:
			VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraDefeatHandler", "N", 8.0)
			
			--Dialogue:
			fnCutscene([[ Append("Mei:[E|Surprise] ..![P] No![P] I'm too late![B][C]") ]])
			fnCutscene([[ Append("Werecat: Purrr...[P] You smell of a human, even through your disguise.[P] You look strong.[P] You would make a good kinfang.[P] Come to us...[BLOCK]") ]])
			
			--Give the player a choice:
			local sDecisionScript = "\"" .. gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Decision_AnswerWerecatsFail.lua" .. "\""
			fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
			fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Way!\", " .. sDecisionScript .. ", \"Fight\") ")
			fnCutscene(" WD_SetProperty(\"Add Decision\", \"Join Them\",  " .. sDecisionScript .. ", \"Join\") ")
			fnCutsceneBlocker()
		end

	end

--If Florentina is here to help:
else
	
	--You're in time to save her!
	if(iCassandraEncounters < 6) then
		
		--Cassandra as a human:
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
	
		--If Mei is a human:
		if(sMeiForm == "Human") then
			
			--Flag:
			VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraDefeatHandler", "N", 4.0)
			
			--Dialogue:
			fnCutscene([[ Append("Mei:[E|Offended] Hey, cats![P] Leave her alone![B][C]") ]])
			fnCutscene([[ Append("Werecat: Purrr...[P] You look strong, human.[P] You would make a good kinfang.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Offended] This isn't a negotiation.[P] Give me the girl![B][C]") ]])
			fnCutscene([[ Append("Werecat: Ha ha ha![P] You are outnumbered, human![B][C]") ]])
			fnCutscene([[ Append("Werecat: Fangs![P] Add her to our pride!BLOCK][CLEAR]") ]])
			fnCutscene([[ Append("Florentina:[E|Happy] Good![P] I was hoping you wouldn't manage to talk them out of it!") ]])
			fnCutsceneBlocker()
			
			--Battle!
            fnCutscene([[ AdvCombat_SetProperty("World Pulse", true) ]])
			fnCutscene([[ AdvCombat_SetProperty("Reinitialize") ]])
			fnCutscene([[ AdvCombat_SetProperty("Activate") ]])
			fnCutscene([[ AdvCombat_SetProperty("Victory Script", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_RescueCassandraAsHumanTeam.lua") ]])
			fnCutscene([[ AdvCombat_SetProperty("Defeat Script", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraDefeat/Scene_Begin.lua") ]])
			fnCutscene([[ AdvCombat_SetProperty("Unretreatable", true) ]])
            fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/003 Evermoon North/Werecat Hunter.lua", 0) ]])
            fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/003 Evermoon North/Werecat Hunter.lua", 0) ]])
            fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/003 Evermoon North/Werecat Hunter.lua", 0) ]])
			fnCutsceneBlocker()
		
		--If Mei is a werecat:
		elseif(sMeiForm == "Werecat") then
			
			--Flag:
			VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraDefeatHandler", "N", 5.0)
			
			--Dialogue.
			fnCutscene([[ Append("Mei:[E|Neutral] Kinfangs![P] What goes on here?[B][C]") ]])
			fnCutscene([[ Append("Werecat: Purrr...[P] This one will be turned when the moon glides over this glade.[B][C]") ]])
			fnCutscene([[ Append("Werecat: The time will come soon.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Smirk] Oooh, I'm not too late.[B][C]") ]])
			fnCutscene([[ Append("Werecat: Do you seek to join the festivities?[BLOCK]") ]])

			--Decision script is a different one.
			local sDecisionScript = "\"" .. gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Decision_AnswerWerecats.lua" .. "\""
			fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
			fnCutscene(" WD_SetProperty(\"Add Decision\", \"Let her go!\", " .. sDecisionScript .. ", \"LetHerGoTeamCat\") ")
			fnCutscene(" WD_SetProperty(\"Add Decision\", \"I will join you.\",  " .. sDecisionScript .. ", \"JoinThemTeamCat\") ")
			fnCutsceneBlocker()

		--If Mei is anything else:
		else
			
			--Flag:
			VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraDefeatHandler", "N", 6.0)
			
			--Dialogue:
			fnCutscene([[ Append("Mei:[E|Offended] Hey, cats![P] Leave her alone![B][C]") ]])
			fnCutscene([[ Append("Werecat: Purrr...[P] You look strong.[P] You would be a good challenge.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Offended] This isn't a negotiation.[P] Give me the girl![B][C]") ]])
			fnCutscene([[ Append("Werecat: Ha ha ha![P] You are outnumbered![P] You have great courage![B][C]") ]])
			fnCutscene([[ Append("Werecat: Fangs![P] Attack![B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Happy] Good![P] I was worried there'd be a peaceful resolution!") ]])
			fnCutsceneBlocker()
			
			--Battle!
            fnCutscene([[ AdvCombat_SetProperty("World Pulse", true) ]])
			fnCutscene([[ AdvCombat_SetProperty("Reinitialize") ]])
			fnCutscene([[ AdvCombat_SetProperty("Activate") ]])
			fnCutscene([[ AdvCombat_SetProperty("Victory Script", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_RescueCassandraAsWerecatSolo.lua") ]])
			fnCutscene([[ AdvCombat_SetProperty("Defeat Script", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraDefeat/Scene_Begin.lua") ]])
			fnCutscene([[ AdvCombat_SetProperty("Unretreatable", true) ]])
            fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/003 Evermoon North/Werecat Hunter.lua", 0) ]])
            fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/003 Evermoon North/Werecat Hunter.lua", 0) ]])
            fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/003 Evermoon North/Werecat Hunter.lua", 0) ]])
			fnCutsceneBlocker()
		end
	
	--You're too late!
	else
		
		--Cassandra as a human:
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Werecat") ]])
	
		--Mei has werecat form:
		local iHasWerecatForm = VM_GetVar("Root/Variables/Global/Mei/iHasWerecatForm", "N")
	
		--If Mei is a human:
		if(sMeiForm == "Human" and iHasWerecatForm == 0.0) then
			
			--Flag:
			VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraDefeatHandler", "N", 9.0)
			
			--Dialogue:
			fnCutscene([[ Append("Mei:[E|Surprise] ..![P] No![P] We're too late![B][C]") ]])
			fnCutscene([[ Append("Werecat: Purrr...[P] You look strong, human.[P] You would make a good kinfang.[P] Come to us...[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Confused] No chance, cats.[P] Mei, weapons up![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Blush] (I look strong?[P] Maybe I should go to them...)[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Confused] Mei?[P] Don't tell me you're actually considering it![BLOCK]") ]])
			
			--Give the player a choice:
			local sDecisionScript = "\"" .. gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Decision_AnswerWerecatsFail.lua" .. "\""
			fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
			fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Way!\", " .. sDecisionScript .. ", \"Fight\") ")
			fnCutscene(" WD_SetProperty(\"Add Decision\", \"Join Them\",  " .. sDecisionScript .. ", \"Join\") ")
			fnCutsceneBlocker()
		
		--Mei is a human and has werecat form:
		elseif(sMeiForm == "Human" and iHasWerecatForm == 1.0) then
			fnCutscene([[ Append("Mei:[E|Neutral] ..![P] We're too late![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Blush] (The pheromones...[P] the moon...[P] so....[P] ungggghhh...[P] horrrrnnnyyy...)[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Blush] (Why even try to stop my kinfangs?[P] I'm -[P] I'm a fang too...[P] ungghhhh)[B][C]") ]])
			fnCutscene([[ Append("Werecat: Purrr...[P] You have come to join us?[P] Purrr....[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Blush] Let us...[P] celebrate this joyous occasion...[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Surprise] Mei?[P] Mei![P] What are you doing?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Blush] Joining my fangs...[P] you should come too...[P] I'll lick you...[P] unngghhhh...[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Blush] Oh -[P] uh, you have fun.[P] Cats aren't my thing.[P] I'll just -[P] watch.[P] Yeah.") ]])
			fnCutsceneBlocker()

			--Wait a bit.
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			
			--Black the screen out.
			fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
			fnCutsceneWait(60)
			fnCutsceneBlocker()

			--Activate scenes mode.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Activate Scene") ]])
			fnCutscene([[ Append("Mei strode confidently into the glade, her fellow cats already well underway in the same orgy she had enjoyed when she first joined the night hunters.") ]])
			fnCutsceneBlocker()
			
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
			fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Werecat") ]])
			fnCutscene([[ Append("As she approached, she allowed the latent curse that her runestone suppressed to take over again.[P] Fur, claws, and fangs replaced her human features.[P] The werecats were too busy to notice or care.") ]])
			fnCutsceneBlocker()
			
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
			fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Werecat") ]])
			fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
			fnCutscene([[ Append("The new fang was blonde and had her eyes locked on Mei's hips as she approached.[P] Unused to her new body, the fang could hardly control the lust building inside her.[P] She practically dripped in anticipation.[B][C]") ]])
			fnCutscene([[ Append("Mei knelt in front of her and began to suck at her exposed sex.[P] The fang would have fallen in a sudden surge of pleasure, but another cat appeared behind her and groped at her chest.[P] A third soon kissed her and rubbed her gorgeous blonde fur.[B][C]") ]])
			fnCutscene([[ Append("The fang shuddered in orgasm, but was permitted not a second to rest.[P] The cats began to congregate around her, licking and sucking one another and her.[P] Mei continued to suck at her as another cat began working on Mei.[B][C]") ]])
			fnCutscene([[ Append("Her head swimming in sexual overload, Mei was barely aware of her own tongue as it slid in and out of the new fang.[P] Her delirium built with her own orgasm.[P] She continued to suck and lick.") ]])
			fnCutsceneBlocker()
			
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
			fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
			fnCutscene([[ WD_SetProperty("Set Scene Image", "Null") ]])
			fnCutscene([[ Append("...[B][C]") ]])
			fnCutscene([[ Append("Hours passed as the orgy raged.[P] The moon continued its arc across the sky.[P] Soon, it would set, and pass behind the trees.[B][C]") ]])
			fnCutscene([[ Append("With the loss of their patron moon, the cats felt themselves tire.[P] Orgasm after orgasm had taken its toll.[P] The cats soon fell asleep in a great pile.[P] Mei cuddled against the new fang and slipped into oblivion.[B][C]") ]])
			fnCutscene([[ Append("...[B][C]") ]])
			fnCutscene([[ Append("When she woke, most of the cats had gone.[P] The new fang was nowhere in sight.[P] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[P] She could taste the new fang there, and she tasted strong.[B][C]") ]])
			fnCutscene([[ Append("Mei left the clearing and made her way back to the camp she had visited earlier.[P] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[P] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
			fnCutsceneBlocker()
	
			--Wait a bit.
			fnCutsceneWait(60)
			fnCutsceneBlocker()
			
			--Change maps. Go back to the campsite.
			fnCutscene([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
			fnCutsceneBlocker()
		
		--If Mei is a werecat:
		elseif(sMeiForm == "Werecat") then
		
			--Dialogue
			fnCutscene([[ Append("Mei:[E|Neutral] ..![P] We're too late![B][C]") ]])
			fnCutscene([[ Append("Werecat: Purrr...[P] You have come to join us?[P] Purrr....[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] (The pheromones...[P] the moon...[P] so....[P] ungggghhh...[P] horrrrnnnyyy...)[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Smirk] Let us...[P] celebrate this joyous occasion...[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Surprise] Mei?[P] Mei![P] What are you doing?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Blush] Joining my fangs...[P] you should come too...[P] I'll lick you...[P] unngghhhh...[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Blush] Oh -[P] uh, you have fun.[P] Cats aren't my thing.[P] I'll just -[P] watch.[P] Yeah.") ]])
			fnCutsceneBlocker()

			--Wait a bit.
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			
			--Black the screen out.
			fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
			fnCutsceneWait(60)
			fnCutsceneBlocker()

			--Activate scenes mode.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Activate Scene") ]])
			fnCutscene([[ Append("Mei strode confidently into the glade, her fellow cats already well underway in the same orgy she had enjoyed when she first joined the night hunters.") ]])
			fnCutsceneBlocker()
			
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
			fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
			fnCutscene([[ Append("The new fang was blonde and had her eyes locked on Mei's hips as she approached.[P] Unused to her new body, the fang could hardly control the lust building inside her.[P] She practically dripped in anticipation.[B][C]") ]])
			fnCutscene([[ Append("Mei knelt in front of her and began to suck at her exposed sex.[P] The fang would have fallen in a sudden surge of pleasure, but another cat appeared behind her and groped at her chest.[P] A third soon kissed her and rubbed her gorgeous blonde fur.[B][C]") ]])
			fnCutscene([[ Append("The fang shuddered in orgasm, but was permitted not a second to rest.[P] The cats began to congregate around her, licking and sucking one another and her.[P] Mei continued to suck at her as another cat began working on Mei.[B][C]") ]])
			fnCutscene([[ Append("Her head swimming in sexual overload, Mei was barely aware of her own tongue as it slid in and out of the new fang.[P] Her delirium built with her own orgasm.[P] She continued to suck and lick.") ]])
			fnCutsceneBlocker()
			
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
			fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
			fnCutscene([[ WD_SetProperty("Set Scene Image", "Null") ]])
			fnCutscene([[ Append("...[B][C]") ]])
			fnCutscene([[ Append("Hours passed as the orgy raged.[P] The moon continued its arc across the sky.[P] Soon, it would set, and pass behind the trees.[B][C]") ]])
			fnCutscene([[ Append("With the loss of their patron moon, the cats felt themselves tire.[P] Orgasm after orgasm had taken its toll.[P] The cats soon fell asleep in a great pile.[P] Mei cuddled against the new fang and slipped into oblivion.[B][C]") ]])
			fnCutscene([[ Append("...[B][C]") ]])
			fnCutscene([[ Append("When she woke, most of the cats had gone.[P] The new fang was nowhere in sight.[P] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[P] She could taste the new fang there, and she tasted strong.[B][C]") ]])
			fnCutscene([[ Append("Mei left the clearing and made her way back to the camp she had visited earlier.[P] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[P] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
			fnCutsceneBlocker()
	
			--Wait a bit.
			fnCutsceneWait(60)
			fnCutsceneBlocker()
			
			--Change maps. Go back to the campsite.
			fnCutscene([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
			fnCutsceneBlocker()

		--If Mei is anything else:
		else
			
			--Flag:
			VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraDefeatHandler", "N", 10.0)
			
			--Dialogue:
			fnCutscene([[ Append("Mei:[E|Surprise] ..![P] No![P] We're too late![B][C]") ]])
			fnCutscene([[ Append("Werecat: Purrr...[P] You smell of a human, even through your disguise.[P] You look strong.[P] You would make a good kinfang.[P] Come to us...[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Confused] No chance, cats.[P] Mei, weapons up![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Blush] (I look strong?[P] Maybe I should go to them...)[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Confused] Mei, don't tell me you're actually considering it...[BLOCK]") ]])
			
			--Give the player a choice:
			local sDecisionScript = "\"" .. gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Decision_AnswerWerecatsFail.lua" .. "\""
			fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
			fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Way!\", " .. sDecisionScript .. ", \"Fight\") ")
			fnCutscene(" WD_SetProperty(\"Add Decision\", \"Join Them\",  " .. sDecisionScript .. ", \"Join\") ")
			fnCutsceneBlocker()
		end
	
	end
end
