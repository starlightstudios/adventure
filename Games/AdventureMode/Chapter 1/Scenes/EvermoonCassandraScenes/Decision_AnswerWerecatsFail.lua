-- |[Decision Response]|
--In this case, Mei was human and failed to rescue Cassandra. The cats will give you the chance to join them without a fight.
local sMeiForm             = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local sCassandraLocation   = VM_GetVar("Root/Variables/Chapter1/Scenes/sCassandraLocation", "S")
local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")

--Get the decision choice.
local sDecision = LM_GetScriptArgument(0)

--Clear censor bars.
WD_SetProperty("Clear Censor Bars")

-- |[Post Decision]|
--Fight it out!
if(sDecision == "Fight") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Dialogue.
	fnStandardMajorDialogue(true)
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Werecat") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Werecat", "Neutral") ]])
	fnCutscene([[ Append("Mei:[E|Offended] Back off, cat![P] You won't have me![B][C]") ]])
	fnCutscene([[ Append("Werecat: Ha ha ha![P] You are outnumbered, human![B][C]") ]])
	fnCutscene([[ Append("Werecat: Fangs![P] Add her to our pride!") ]])
	fnCutsceneBlocker()
	
	--Battle!
	fnCutscene([[ AdvCombat_SetProperty("Reinitialize") ]])
	fnCutscene([[ AdvCombat_SetProperty("Activate") ]])
	fnCutscene([[ AdvCombat_SetProperty("World Pulse", true) ]])
	
	--Florentina is not here:
	if(bIsFlorentinaPresent == false) then
		fnCutscene([[ AdvCombat_SetProperty("Victory Script", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_FailRescueCassandraAsHumanSolo.lua") ]])
	
	--Florentina is present:
	else
		fnCutscene([[ AdvCombat_SetProperty("Victory Script", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_FailRescueCassandraAsHumanTeam.lua") ]])
	end
	
	fnCutscene([[ AdvCombat_SetProperty("Defeat Script", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraDefeat/Scene_Begin.lua") ]])
	fnCutscene([[ AdvCombat_SetProperty("Unretreatable", true) ]])
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/003 Evermoon North/Werecat Hunter.lua", 0) ]])
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/003 Evermoon North/Werecat Hunter.lua", 0) ]])
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/003 Evermoon North/Werecat Hunter.lua", 0) ]])
	fnCutsceneBlocker()

--Join their pride.
elseif(sDecision == "Join") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
    
    --Load Instruction
    fnLoadDelayedBitmapsFromList("Chapter 1 Mei Werecat TF", gciDelayedLoadLoadAtEndOfTick)
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iTurnedCassandra", "N", 2.0) --Indicates Cassandra turned Mei.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N", 2.0)
	
	--Clear music.
	AL_SetProperty("Music", "Null")
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Werecat") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Werecat", "Neutral") ]])
	
	--Mei is alone:
	if(bIsFlorentinaPresent == false) then
		fnCutscene([[ Append("Mei:[E|Blush] Make me...[P] one of you...[P] please...") ]])
	
	--Florentina is present:
	else
		fnCutscene([[ Append("Mei:[E|Blush] Make me...[P] one of you...[P] please...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Surprise] Mei?[P] Have you lost your mind?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] Why fight them?[P] They're so beautiful and graceful...[P] I want to be like them...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Uh, okay.[P] Well, I guess you've made up your mind.[P] I suppose I'll meet you back at the campsite.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] You could join in...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Cats aren't my thing, kid.[P] But, maybe I'll watch.") ]])
	end
	
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Switch Mei's form to Werecat.
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Werecat.lua") ]])
	fnCutsceneBlocker()

	--Activate scenes mode.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
	fnCutscene([[ Append("Mei strode forward, parting the pack of cats.[P] They allowed her to reach the center of their pride before they swarmed over her.[P] They seemed to enjoy taunting her with their claws and tails, swiping gently against her skin.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
	fnCutscene([[ Append("The blonde werecat, the girl she had been trying to save, stood at the center of the swarm.[P] Directly in front of Mei, the girl held her attention as she pawed at Mei's face.[B][C]") ]])
	fnCutscene([[ Append("Mei realized that the blonde cat was very gently scratching her, drawing the tiniest rivulet of blood from her cheek.[P] She felt no pain, but arousal, as the moonlight danced over the cat's features.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF0") ]])
	fnCutscene([[ Append("Mei nearly gasped as she realized her vision had been enhanced.[P] She could now see perfectly in the dark.[P] Every inch of the blonde cat's body was visible to her.[P] She could see each individual hair stand on end.[P] She put her hands on the cat's hips...") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF0") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF1") ]])
	fnCutscene([[ Append("She was unsurprised to see that her own hands had changed, transforming into clawed paws as she wrapped around the girl's midsection.[P] Her body shuddered with pleasure as the changes rippled forth.[P] Fur sprung from her skin and her ears shifted to the top of her head.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF1") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Werecat") ]])
	fnCutscene([[ Append("The girl kissed her as the changes finished.[P] The rest of her pride now descended upon them, licking and sucking every inch of her body as the pheromones and moonlight hit a fevered pitch.[B][C]") ]])
	fnCutscene([[ Append("The kinfangs continued their orgy as the moon looked on, bathing them in its stimulating glow.[P] They worshipped it with their bodies, cleaning their new fangs with their tongues.[P] Inside and out.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Werecat") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Null") ]])
	fnCutscene([[ Append("...[B][C]") ]])
	fnCutscene([[ Append("Hours passed as the orgy raged.[P] The moon continued its arc across the sky.[P] Soon, it would set, and pass behind the trees.[B][C]") ]])
	fnCutscene([[ Append("With the loss of their patron moon, the cats felt themselves tire.[P] Orgasm after orgasm had taken its toll.[P] The cats soon fell asleep in a great pile.[P] Mei cuddled against the new fang and slipped into oblivion.[B][C]") ]])
	fnCutscene([[ Append("...[B][C]") ]])
	fnCutscene([[ Append("When she woke, most of the cats had gone.[P] The new fang was nowhere in sight.[P] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[P] She could taste the new fang there, and she tasted strong.[B][C]") ]])
	fnCutscene([[ Append("Mei left the clearing and made her way back to the camp she had visited earlier.[P] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[P] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Change maps. Go back to the campsite.
	fnCutscene([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
	fnCutsceneBlocker()
end
