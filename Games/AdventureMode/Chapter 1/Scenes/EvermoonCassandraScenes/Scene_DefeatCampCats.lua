-- |[Combat Victory]|
--Scene plays if the party defeats the werecat trio at the campsite.

--Variables.
local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")

--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()

--Switch back to the forest music.
fnCutscene([[ AL_SetProperty("Music", "Null") ]])
fnCutsceneWait(1)
fnCutsceneBlocker()
fnCutscene([[ AL_SetProperty("Music", "ForestTheme") ]])
fnCutsceneBlocker()
	
-- |[Dialogue]|
--Set variable:
VM_SetVar("Root/Variables/Chapter1/Scenes/iEncounteredCatCampsite", "N", 2.0)

--Unlock this warp scene.
VM_SetVar("Root/Variables/Chapter1/Campfires/iEvermoonCassandraA", "N", 1.0)

--Setup.
fnStandardMajorDialogue()
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Werecat", "Neutral") ]])
fnCutscene([[ Append("Mei: Was that it?[B][C]") ]])
fnCutscene([[ Append("Werecat: S-[P]strong...[B][C]") ]])
fnCutscene([[ Append("Werecat: No need to fight more![P] You can have this spot![P] We didn't want it!") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()

-- |[Movement]|
--Werecats run away.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatA")
	ActorEvent_SetProperty("Move To", (23.25 * gciSizePerTile), (18.50 * gciSizePerTile), 2.50)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatB")
	ActorEvent_SetProperty("Move To", (23.25 * gciSizePerTile), (20.50 * gciSizePerTile), 2.50)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatC")
	ActorEvent_SetProperty("Move To", (25.25 * gciSizePerTile), (20.50 * gciSizePerTile), 2.50)
DL_PopActiveObject()
fnCutsceneBlocker()

--WerecatA trips on the log.
fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatA")
	ActorEvent_SetProperty("Special Frame", "Wounded")
DL_PopActiveObject()
for i = 1, 60, 1 do
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "WerecatA")
		ActorEvent_SetProperty("Teleport To", (23.25 * gciSizePerTile) + (gciSizePerTile * (i / 30.0)), (18.50 * gciSizePerTile))
	DL_PopActiveObject()
end

--Werecat B continues running.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatB")
	ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (20.50 * gciSizePerTile), 2.50)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatB")
	ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (18.50 * gciSizePerTile), 2.50)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatB")
	ActorEvent_SetProperty("Move To", (34.25 * gciSizePerTile), (18.50 * gciSizePerTile), 2.50)
DL_PopActiveObject()

--Werecat C continues running.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatC")
	ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (20.50 * gciSizePerTile), 2.50)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatC")
	ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (18.50 * gciSizePerTile), 2.50)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatC")
	ActorEvent_SetProperty("Move To", (34.25 * gciSizePerTile), (18.50 * gciSizePerTile), 2.50)
DL_PopActiveObject()
fnCutsceneBlocker()

--Teleport Werecats B and C offscreen.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatB")
	ActorEvent_SetProperty("Teleport To", (-100.25 * gciSizePerTile), (-100.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatC")
	ActorEvent_SetProperty("Teleport To", (-100.25 * gciSizePerTile), (-100.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(60)
fnCutsceneBlocker()

--Face Werecat A south, and stand up.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatA")
	ActorEvent_SetProperty("Face",  0, 1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatA")
	ActorEvent_SetProperty("Special Frame", "Null")
DL_PopActiveObject()

--Wait a bit.
fnCutsceneWait(60)
fnCutsceneBlocker()

--Look west.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatA")
	ActorEvent_SetProperty("Face", -1, 0)
DL_PopActiveObject()

--Wait a bit.
fnCutsceneWait(60)
fnCutsceneBlocker()

--Werecat A runs away.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatA")
	ActorEvent_SetProperty("Move To", (34.25 * gciSizePerTile), (18.50 * gciSizePerTile), 2.50)
DL_PopActiveObject()
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatA")
	ActorEvent_SetProperty("Teleport To", (-100.25 * gciSizePerTile), (-100.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

-- |[Dialogue]|
--Mei is alone:
if(bIsFlorentinaPresent == false) then

	--Setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	
	--Dialogue.
	fnCutscene([[ Append("Mei:[E|Laugh] Hah![P] Not so tough now, are you?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Hmmm, but this campsite must not have belonged to those cats.[P] Not enough tents, for one thing.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I definitely get the feeling I should rest up before I keep going...") ]])
	fnCutsceneBlocker()
	
--Florentina is here:
else

	--Setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
	
	--Dialogue.
	fnCutscene([[ Append("Mei:[E|Laugh] Hah![P] Not so tough now, are you?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] Nice work![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] But don't you think it's odd that the werecats would set up a campsite?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] This must not have been their campsite...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I definitely get the feeling we should rest up before we keep going...") ]])
	fnCutsceneBlocker()
	
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (19.25 * gciSizePerTile), (19.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
	fnCutsceneWait(5)
	fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()

end
