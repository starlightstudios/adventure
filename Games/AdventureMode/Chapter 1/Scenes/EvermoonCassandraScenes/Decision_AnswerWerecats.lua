-- |[Decision Response]|
--Script that handles the dialogue case where the werecats offer Mei the chance to join them, or fight to save Cassandra.
local sMeiForm             = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local sCassandraLocation   = VM_GetVar("Root/Variables/Chapter1/Scenes/sCassandraLocation", "S")
local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")

--Get the decision choice.
local sDecision = LM_GetScriptArgument(0)

--Clear censor bars.
WD_SetProperty("Clear Censor Bars")

-- |[Post Decision]|
--Mei tells the cats to let the girl go. Mei is alone and a werecat.
if(sDecision == "LetHerGoSoloCat") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraDefeatHandler", "N", 1.0)
	VM_SetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N", 2.0)
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Werecat", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
	
	--Talking.
	fnCutscene([[ Append("Mei:[E|Neutral] If you let the girl go, I'll let you walk away from here in one piece.[B][C]") ]])
	fnCutscene([[ Append("Werecat: You would threaten our pride?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] Hssss![P] You used so many fangs to hunt one human![P] Is she strong, or are you so weak?[B][C]") ]])
	fnCutscene([[ Append("Werecat: Prove your strength, then, fang![P] Prove it and you may have her![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] Gladly!") ]])
	fnCutsceneBlocker()
	
	--Battle!
	fnCutscene([[ AdvCombat_SetProperty("World Pulse", true) ]])
	fnCutscene([[ AdvCombat_SetProperty("Reinitialize") ]])
	fnCutscene([[ AdvCombat_SetProperty("Activate") ]])
	fnCutscene([[ AdvCombat_SetProperty("Victory Script", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_RescueCassandraAsWerecatSolo.lua") ]])
	fnCutscene([[ AdvCombat_SetProperty("Defeat Script", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraDefeat/Scene_Begin.lua") ]])
	fnCutscene([[ AdvCombat_SetProperty("Unretreatable", true) ]])
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/003 Evermoon North/Werecat Hunter.lua", 0) ]])
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/003 Evermoon North/Werecat Hunter.lua", 0) ]])
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/003 Evermoon North/Werecat Hunter.lua", 0) ]])
	fnCutsceneBlocker()

--Mei tells the cats to let the girl go. Mei has Florentina and is a werecat.
elseif(sDecision == "LetHerGoTeamCat") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraDefeatHandler", "N", 4.0)
	VM_SetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N", 2.0)
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Werecat", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
	
	--Talking.
	fnCutscene([[ Append("Mei:[E|Neutral] If you let the girl go, I'll let you walk away from here in one piece.[B][C]") ]])
	fnCutscene([[ Append("Werecat: You would threaten our pride?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] Hssss![P] You used so many fangs to hunt one human![P] Is she strong, or are you so weak?[B][C]") ]])
	fnCutscene([[ Append("Werecat: Prove your strength, then, fang![P] Prove it and you may have her![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] Gladly!") ]])
	fnCutsceneBlocker()
	
	--Battle!
	fnCutscene([[ AdvCombat_SetProperty("World Pulse", true) ]])
	fnCutscene([[ AdvCombat_SetProperty("Reinitialize") ]])
	fnCutscene([[ AdvCombat_SetProperty("Activate") ]])
	fnCutscene([[ AdvCombat_SetProperty("Victory Script", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_RescueCassandraAsWerecatTeam.lua") ]])
	fnCutscene([[ AdvCombat_SetProperty("Defeat Script", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraDefeat/Scene_Begin.lua") ]])
	fnCutscene([[ AdvCombat_SetProperty("Unretreatable", true) ]])
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/003 Evermoon North/Werecat Hunter.lua", 0) ]])
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/003 Evermoon North/Werecat Hunter.lua", 0) ]])
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/003 Evermoon North/Werecat Hunter.lua", 0) ]])
	fnCutsceneBlocker()

--Mei decides to join in. Mei is alone and a werecat.
elseif(sDecision == "JoinThemSoloCat") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iTurnedCassandra", "N", 1.0)
	VM_SetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N", 2.0)
    
    --Order Cassandra's scene images to load.
    fnLoadDelayedBitmapsFromList("Chapter 1 Cassandra", gciDelayedLoadLoadAtEndOfTick)
	
	--Clear music.
	AL_SetProperty("Music", "Null")
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Werecat", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
	
	--Talking.
	fnCutscene([[ Append("Mei:[E|Happy] She will make an excellent addition.[P] Let us prepare ourselves for the ceremony.") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()

	--Activate scenes mode.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Werecat") ]])
	fnCutscene([[ Append("Mei strode forth into the glade.[P] The mewling, helpless girl looked up at her.[P] She had been bound and gagged by the cats.[P] Her struggle ignited a fire in Mei's heart.[P] She was strong.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Neutral") ]])
	fnCutscene([[ Append("Mei knelt in front of the girl.[P] She looked like an adventurer of some sort.[P] Her clothes had scratches and claw marks on them.[P] The nearby cats likewise had signs of battle on them.[P] The sight of it made her wet.[B][C]") ]])
	fnCutscene([[ Append("The time approached.[P] The cats continued to pace and touch themselves as night had fallen and the moon had come out.[P] With each moment, the girl struggled less and less.[P] She, too, had become wet.[B][C]") ]])
	fnCutscene([[ Append("Mei took position behind her, smelling at her fluids.[P] The pheromones of the other cats hit a fevered pitch.[P] Soon they were clawing and licking at one another.[P] Mei focused her attention on the bound girl.") ]])
	fnCutsceneBlocker()

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF0") ]])
	fnCutscene([[ Append("The curse took hold over her, sprouting forth fur and a tail from her behind.[P] Mei pulled down her pants and exposed her glistening sex to the moonlight.[P] Mei began licking.") ]])
	fnCutsceneBlocker()

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF1") ]])
	fnCutscene([[ Append("Other cats took position around them, licking and clawing all over them.[P] Mei continued to tongue the girl's juices as she became bestial like Mei.[P] Her claws sprang from her hands, allowing her to rip open her bindings.") ]])
	fnCutsceneBlocker()

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
	fnCutscene([[ Append("The cats welcomed her by tongueing every inch of her body.[P] She surprised Mei, suddenly, by pushing her over.[P] Mei tumbled over, surprised by the girl's strength.[B][C]") ]])
	fnCutscene([[ Append("She now descended upon Mei's sex as the orgy continued.[P] Mei lost herself in the pile of fur, tongue, and claw.") ]])
	fnCutsceneBlocker()

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ Append("...[B][C]") ]])
	fnCutscene([[ Append("Hours passed as the orgy raged.[P] The moon continued its arc across the sky.[P] Soon, it would set, and pass behind the trees.[B][C]") ]])
	fnCutscene([[ Append("With the loss of their patron moon, the cats felt themselves tire.[P] Orgasm after orgasm had taken its toll.[P] The cats soon fell asleep in a great pile.[P] Mei cuddled against the new fang and slipped into oblivion.[B][C]") ]])
	fnCutscene([[ Append("...[B][C]") ]])
	fnCutscene([[ Append("When she woke, most of the cats had gone.[P] The new fang was nowhere in sight.[P] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[P] She could taste the new fang there, and she tasted strong.[B][C]") ]])
	fnCutscene([[ Append("Mei left the clearing and made her way back to the camp she had visited earlier.[P] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[P] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Change maps. Go back to the campsite.
	fnCutscene([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
	fnCutsceneBlocker()

--Mei decides to join in. Florentina is here.
elseif(sDecision == "JoinThemTeamCat") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iTurnedCassandra", "N", 1.0)
	VM_SetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N", 2.0)
    
    --Order Cassandra's scene images to load.
    fnLoadDelayedBitmapsFromList("Chapter 1 Cassandra", gciDelayedLoadLoadAtEndOfTick)
	
	--Clear music.
	AL_SetProperty("Music", "Null")
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Werecat", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
	
	--Talking.
	fnCutscene([[ Append("Mei:[E|Happy] She will make an excellent addition.[P] Let us prepare ourselves for the ceremony.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Surprise] Excuse me?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Was I not clear?[P] The girl will become fang when the moon graces us.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Surprise] I seem to recall we were supposed to save her, Mei.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] She is strong.[P] She will be a strong fang.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Smirk] I'll let you take part in the ceremony...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Blush] Eheheh -[P] eeehhh.[P] No thanks.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Blush] I'll -[P] I'll watch...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Smirk] Your loss.") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()

	--Activate scenes mode.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Werecat") ]])
	fnCutscene([[ Append("Mei strode forth into the glade.[P] The mewling, helpless girl looked up at her.[P] She had been bound and gagged by the cats.[P] Her struggle ignited a fire in Mei's heart.[P] She was strong.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Werecat") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Neutral") ]])
	fnCutscene([[ Append("Mei knelt in front of the girl.[P] She looked like an adventurer of some sort.[P] Her clothes had scratches and claw marks on them.[P] The nearby cats likewise had signs of battle on them.[P] The sight of it made her wet.[B][C]") ]])
	fnCutscene([[ Append("The time approached.[P] The cats continued to pace and touch themselves as night had fallen and the moon had come out.[P] With each moment, the girl struggled less and less.[P] She, too, had become wet.[B][C]") ]])
	fnCutscene([[ Append("Mei took position behind her, smelling at her fluids.[P] The pheromones of the other cats hit a fevered pitch.[P] Soon they were clawing and licking at one another.[P] Mei focused her attention on the bound girl.") ]])
	fnCutsceneBlocker()

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Neutral") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF0") ]])
	fnCutscene([[ Append("The curse took hold over her, sprouting forth fur and a tail from her behind.[P] Mei pulled down her pants and exposed her glistening sex to the moonlight.[P] Mei began licking.") ]])
	fnCutsceneBlocker()

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF0") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF1") ]])
	fnCutscene([[ Append("Other cats took position around them, licking and clawing all over them.[P] Mei continued to tongue the girl's juices as she became bestial like Mei.[P] Her claws sprang from her hands, allowing her to rip open her bindings.") ]])
	fnCutsceneBlocker()

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF1") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
	fnCutscene([[ Append("The cats welcomed her by tongueing every inch of her body.[P] She surprised Mei, suddenly, by pushing her over.[P] Mei tumbled over, surprised by the girl's strength.[B][C]") ]])
	fnCutscene([[ Append("She now descended upon Mei's sex as the orgy continued.[P] Mei lost herself in the pile of fur, tongue, and claw.") ]])
	fnCutsceneBlocker()

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Null") ]])
	fnCutscene([[ Append("...[B][C]") ]])
	fnCutscene([[ Append("Hours passed as the orgy raged.[P] The moon continued its arc across the sky.[P] Soon, it would set, and pass behind the trees.[B][C]") ]])
	fnCutscene([[ Append("With the loss of their patron moon, the cats felt themselves tire.[P] Orgasm after orgasm had taken its toll.[P] The cats soon fell asleep in a great pile.[P] Mei cuddled against the new fang and slipped into oblivion.[B][C]") ]])
	fnCutscene([[ Append("...[B][C]") ]])
	fnCutscene([[ Append("When she woke, most of the cats had gone.[P] The new fang was nowhere in sight.[P] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[P] She could taste the new fang there, and she tasted strong.[B][C]") ]])
	fnCutscene([[ Append("Mei left the clearing and made her way back to the camp she had visited earlier.[P] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[P] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Change maps. Go back to the campsite.
	fnCutscene([[ AL_BeginTransitionTo("EvermoonCassandraA", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_AwakenAtCamp.lua") ]])
	fnCutsceneBlocker()
end
