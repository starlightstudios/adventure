-- |[ ==================================== Scene Alias List ==================================== ]|
--Cutscenes can be stored under an alias. This is used for enemies on the overworld. When the player
-- is defeated or retreats or otherwise calls a script, the alias is checked instead of the literal
-- script path. Those aliases are built in this script.
local sDebugPath     = gsRoot .. "Chapter 1/Scenes/000 Debug/"
local sTransformPath = gsRoot .. "Chapter 1/Scenes/100 Transform/"
local sDefeatPath    = gsRoot .. "Chapter 1/Scenes/200 Defeat/"
local sStandardPath  = gsRoot .. "Chapter 1/Scenes/300 Standards/"

--Wipe previous aliases.
PathAlias_Clear()

-- |[ ===================================== Combat Scenes ====================================== ]|
--Whenever a scene occurs as a result of combat, be it due to retreat, surrender, defeat, victory,
-- or ending for cutscene reasons, an alias must be used.
PathAlias_CreatePath("Standard Defeat",  sStandardPath .. "Defeat/Scene_Begin.lua")
PathAlias_CreatePath("Standard Retreat", sStandardPath .. "Retreat/Scene_Begin.lua")
PathAlias_CreatePath("Standard Revert",  sStandardPath .. "Revert/Scene_Begin.lua")

--Defeat Cutscenes
PathAlias_CreatePath("Defeat Alraune",          sDefeatPath .. "Defeat_Alraune/Scene_Begin.lua")
PathAlias_CreatePath("Defeat Bee",              sDefeatPath .. "Defeat_Bee/Scene_Begin.lua")
PathAlias_CreatePath("Defeat Cultists",         sDefeatPath .. "Defeat_Cultists/Scene_Begin.lua")
PathAlias_CreatePath("Defeat Ghost",            sDefeatPath .. "Defeat_Ghost/Scene_Begin.lua")
PathAlias_CreatePath("Defeat Gravemarker",      sDefeatPath .. "Defeat_Gravemarker/Scene_Begin.lua")
PathAlias_CreatePath("Defeat Mushraune",        sDefeatPath .. "Defeat_Mushraune/Scene_Begin.lua")
PathAlias_CreatePath("Defeat Slime",            sDefeatPath .. "Defeat_Slime/Scene_Begin.lua")
PathAlias_CreatePath("Defeat Werecat",          sDefeatPath .. "Defeat_Werecat/Scene_Begin.lua")
PathAlias_CreatePath("Defeat WerecatCassandra", sDefeatPath .. "Defeat_WerecatCassandra/Scene_Begin.lua")
PathAlias_CreatePath("Defeat Wisphag",          sDefeatPath .. "Defeat_Wisphag/Scene_Begin.lua")
PathAlias_CreatePath("Defeat Zombee",           sDefeatPath .. "Defeat_Zombee/Scene_Begin.lua")
PathAlias_CreatePath("Defeat Rubber",           sDefeatPath .. "Defeat_Rubber/Scene_Begin.lua")
PathAlias_CreatePath("Defeat Miho",             sDefeatPath .. "Defeat_Miho/Scene_Begin.lua")

-- |[ ======================================== Aliases ========================================= ]|
--These are the aliases that refer to the paths. These appear in the .slf data saved from Tiled.
PathAlias_CreateAliasToPath("Defeat_Alraune",          "Defeat Alraune")
PathAlias_CreateAliasToPath("Defeat_Bee",              "Defeat Bee")
PathAlias_CreateAliasToPath("Defeat Cultists",         "Defeat Cultists")
PathAlias_CreateAliasToPath("Defeat_Ghost",            "Defeat Ghost")
PathAlias_CreateAliasToPath("Defeat_Gravemarker",      "Defeat Gravemarker")
PathAlias_CreateAliasToPath("Defeat_Mushraune",        "Defeat Mushraune")
PathAlias_CreateAliasToPath("Defeat_Slime",            "Defeat Slime")
PathAlias_CreateAliasToPath("Defeat_Werecat",          "Defeat Werecat")
PathAlias_CreateAliasToPath("Defeat_Wisphag",          "Defeat Wisphag")
PathAlias_CreateAliasToPath("Defeat_WerecatCassandra", "Defeat WerecatCassandra")
PathAlias_CreateAliasToPath("Defeat_Zombee",           "Defeat Zombee")
PathAlias_CreateAliasToPath("Defeat_Rubber",           "Defeat Rubber")
PathAlias_CreateAliasToPath("Defeat_Miho",             "Defeat Miho")
