-- |[ ================================== Scene Post-Transition ================================= ]|
--After transition, play a random dialogue line.

-- |[ ================ Setup ================= ]|
-- |[Variables]|
local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
local iBeganSequence       = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iBeganSequence", "N")
local iCompletedSequence   = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iCompletedSequence", "N")

-- |[Overlay]|
--Set the overlay to fullblack. Fade in slowly.
fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Over_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
fnCutsceneWait(120)
fnCutsceneBlocker()

-- |[Position]|
--Call function to automatically position characters around the save point. This also sets everyone to "Wounded" frames.
SceneReversion:fnSavePointRearrange()

-- |[ ============== Execution =============== ]|
-- |[Fade In]|
--Mannequin sequence, losing a battle resets to night appearances.
if(iBeganSequence == 1.0 and iCompletedSequence == 0.0) then
    fnCutscene([[ AL_SetProperty("Activate Fade", 180, gci_Fade_Under_GUI, true, 0, 0, 0, 1, gcf_Night_R, gcf_Night_G, gcf_Night_B, gcf_Night_A, true) ]])

--Normal daytime.
else
    fnCutscene([[ AL_SetProperty("Activate Fade", 180, gci_Fade_Over_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
end

--Wait a bit.
fnCutsceneWait(240)
fnCutsceneBlocker()

-- |[Stand Up, Face Leader]|
--Handled by a subroutine, party stands up and rotates to face the leader.
SceneReversion:fnPartyStandUp()

--Re-transform. Subroutine makes party members return to their starting forms if they reverted.
SceneReversion:fnRetransform()

-- |[ =============== Dialogue =============== ]|
--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Mei talks to herself.
if(bIsFlorentinaPresent == false) then
	
	--Setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Sad") ]])

	--Roll.
	local iDialogueRoll = LM_GetRandomNumber(0, 8)

	--Dialogue handlers.
	if(iDialogueRoll == 0) then
		fnCutscene([[ Append("Mei:[E|Sad] ...[P] That could have gone better...") ]])

	elseif(iDialogueRoll == 1) then
		fnCutscene([[ Append("Mei:[E|Sad] ...[P] Second verse, same as the first...") ]])

	elseif(iDialogueRoll == 2) then
		fnCutscene([[ Append("Mei:[E|Sad] ...[P] I learned something useful::[P] Getting hit, hurts.") ]])

	elseif(iDialogueRoll == 3) then
		fnCutscene([[ Append("Mei:[E|Sad] ...[P] Let's try not to repeat that particular mistake...") ]])

	elseif(iDialogueRoll == 4) then
		fnCutscene([[ Append("Mei:[E|Sad] ...[P] Now I know how video game characters feel...") ]])

	elseif(iDialogueRoll == 5) then
		fnCutscene([[ Append("Mei:[E|Sad] ...[P] *cough* [P] Still a better love story than Twilight...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Heh.") ]])

	elseif(iDialogueRoll == 6) then
		fnCutscene([[ Append("Mei:[E|Sad] ...[P] Good thing I'm still in one piece...") ]])

	elseif(iDialogueRoll == 7) then
		fnCutscene([[ Append("Mei:[E|Sad] I think the key is to duck right before they hit me.[P] Yeah, that's it.") ]])

	elseif(iDialogueRoll == 8) then
		fnCutscene([[ Append("Mei:[E|Sad] Ugh.[P] Better not make a habit of this.") ]])
	end

--Florentina and Mei.
else
	
	--Setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Sad") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])

	--Roll.
	local iDialogueRoll = LM_GetRandomNumber(0, 12)

	--Dialogue handlers.
	if(iDialogueRoll == 0) then
		fnCutscene([[ Append("Mei:[E|Sad] ...[P] That could have gone better...") ]])

	elseif(iDialogueRoll == 1) then
		fnCutscene([[ Append("Mei:[E|Sad] ...[P] Second verse, same as the first...") ]])

	elseif(iDialogueRoll == 2) then
		fnCutscene([[ Append("Mei:[E|Sad] ...[P] I learned something useful::[P] Getting hit, hurts.") ]])

	elseif(iDialogueRoll == 3) then
		fnCutscene([[ Append("Mei:[E|Sad] ...[P] Let's try not to repeat that particular mistake...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Let's.[B][C]") ]])

	elseif(iDialogueRoll == 4) then
		fnCutscene([[ Append("Mei:[E|Sad] ...[P] Now I know how video game characters feel...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] What's a video game?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Oh, nevermind.") ]])

	elseif(iDialogueRoll == 5) then
		fnCutscene([[ Append("Mei:[E|Sad] ...[P] *cough* [P] Still a better love story than Twilight...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Heh.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] I don't follow.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Laugh] You don't want to know.") ]])

	elseif(iDialogueRoll == 6) then
		fnCutscene([[ Append("Mei:[E|Sad] ...[P] Good thing I'm still in one piece...") ]])

	elseif(iDialogueRoll == 7) then
		fnCutscene([[ Append("Mei:[E|Sad] I think the key is to duck right before they hit me.[P] Yeah, that's it.") ]])

	elseif(iDialogueRoll == 8) then
		fnCutscene([[ Append("Mei:[E|Sad] Ugh.[P] Better not make a habit of this.") ]])
	
	elseif(iDialogueRoll == 9) then
		fnCutscene([[ Append("Florentina:[E|Confused] I'll be generous and assume that wasn't part of the plan.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] Gotta be flexible, right?") ]])
	
	elseif(iDialogueRoll == 10) then
		fnCutscene([[ Append("Florentina:[E|Confused] If I didn't know better, I'd think you [P]*liked*[P] getting beaten up.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] Hey, don't judge me.") ]])
	
	elseif(iDialogueRoll == 11) then
		fnCutscene([[ Append("Mei:[E|Sad] ...[P] Still in one piece, Florentina?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Not for long at this pace!") ]])
	
	elseif(iDialogueRoll == 12) then
		fnCutscene([[ Append("Mei:[E|Sad] ...[P] Why must we fight?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Because violence is usually the answer.") ]])
	end
end
fnCutsceneBlocker()

-- |[ ============== Finish Up =============== ]|
--Characters move onto the party leader. This is handled by the subroutine.
SceneReversion:fnSavePointFold()
