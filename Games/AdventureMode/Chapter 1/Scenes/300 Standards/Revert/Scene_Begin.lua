-- |[ ==================================== Emergency Revert ==================================== ]|
--Common, used when the party is defeated. This causes characters who revert to human to do so
-- on the overworld, followed by the launching of whatever scene follows.
--Note: It is possible for a BackToSave to be called after this, special scenes are not required.
SceneReversion:fnExecute()