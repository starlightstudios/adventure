-- |[Scene Post-Transition]|
--After the cutscene goes to fullbright, it switches maps. This plays afterwards.
AL_SetProperty("Activate Fade", 60, gci_Fade_Over_GUI, false, 1, 1, 1, 1, 1, 1, 1, 0)

--Reposition Mei.
EM_PushEntity("Mei")
	TA_SetProperty("Position", 17, 24)
	TA_SetProperty("Facing", gci_Face_West)
DL_PopActiveObject()

--Reposition Adina.
EM_PushEntity("Adina")
	TA_SetProperty("Position", 16, 24)
	TA_SetProperty("Facing", gci_Face_East)
DL_PopActiveObject()

--Wait a bit.
fnCutscene([[ AL_SetProperty("Foreground Alpha", 0, 0.00, 0) ]])
fnCutsceneWait(120)
fnCutsceneBlocker()

--Mei talks to herself.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
fnCutscene([[ Append("Adina: Rested, my loyal thrall?[B][C]") ]])
fnCutscene([[ Append("Thrall: This slave's body is prepared, mistress.[P] Command me.[B][C]") ]])
fnCutscene([[ Append("Adina: Splendid.[P] You must tend to the little ones.[P] Go.[B][C]") ]])
fnCutscene([[ Append("Thrall: Yes mistress.[P] I obey without question.") ]])
fnCutsceneBlocker()

--Reset it so Mei does not have the special grass or pollen.
VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasGrass", "N", 0)
VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasPollen", "N", 0)

--Generate some problems for the farm.
VM_SetVar("Root/Variables/Global/Time/iTimeOfDay", "N", 9.0)
VM_SetVar("Root/Variables/Chapter1/SaltFlats/iTasksDone", "N", 0)
fnModifyTimeOfDay(9, 0)
fnCutscene([[ fnGenerateFarmProblems() ]])
fnCutsceneBlocker()
