-- |[ ==================================== Adina Sex Scene ===================================== ]|
--Cutscene proper. This scene is triggered from a floor trigger. It will end up moving Mei to the SaltFlats map!

-- |[ ====================== Setup ===================== ]|
--Clear censor bars.
WD_SetProperty("Clear Censor Bars")

-- |[Loading]|
fnLoadDelayedBitmapsFromList("Chapter 1 Adina Sex Scene", gciDelayedLoadLoadAtEndOfTick)

-- |[ ================== Construction ================== ]|
-- |[Spawning]|
TA_Create("Adina")
	TA_SetProperty("Position", -100, 100)
	TA_SetProperty("Clipping Flag", false)
	fnSetCharacterGraphics("Root/Images/Sprites/Adina/", false)
	TA_SetProperty("Facing", gci_Face_South)
DL_PopActiveObject() 

-- |[ =============== Cutscene Execution =============== ]|
-- |[Movement]|
--Move Mei.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (12.25 * gciSizePerTile), (10.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Spawn Adina.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Adina")
	ActorEvent_SetProperty("Teleport To", (7.25 * gciSizePerTile), (4.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a few ticks.
fnCutsceneWait(15)
fnCutsceneBlocker()

--Adina moves south.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Adina")
	ActorEvent_SetProperty("Move To", (7.25 * gciSizePerTile), (10.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Face Adina east, and Mei west.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Adina")
	ActorEvent_SetProperty("Face", 1, 0)
DL_PopActiveObject()
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", -1, 0)
DL_PopActiveObject()
fnCutsceneWait(30)
fnCutsceneBlocker()

--Fade out.
fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])

--Talking.
fnCutscene([[ Append("Adina: Now, my thrall, we sleep.[B][C][CLEAR]") ]])
fnCutscene([[ Append("Adina: There is only one bed.[P] Will you lay with me?[B][C]") ]])
fnCutscene([[ Append("Thrall: Yes mistress.[P] Your will is my will.[B][C]") ]])
fnCutscene([[ Append("Adina: Superb.") ]])
fnCutsceneWait(60)
fnCutsceneBlocker()

-- |[Wait a Bit]|
fnCutsceneWait(60)
fnCutsceneBlocker()

-- |[First Day]|
--On the first day, this scene plays.
local iDaysPassed = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iDaysPassed", "N")
if(iDaysPassed < 2) then

	-- |[Dialogue]|
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	fnCutscene([[ Append("Adina: Thrall.[B][C]") ]])
	fnCutscene([[ Append("Thrall: Yes, my mistress?[B][C]") ]])
	fnCutscene([[ Append("Adina: Kiss me.[B][C]") ]])
	fnCutscene([[ Append("Thrall: Yes miss-") ]])
	fnCutsceneWait(30)
	fnCutsceneBlocker()

	-- |[Scene]|
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
    fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/AdinaSexScene/AdinaSexScene") ]])
	fnCutscene([[ Append("Completely under the spell of the Alraune, Mei embraced her mistress and kissed her deeply.[B][C]") ]])
	fnCutscene([[ Append("The two pressed their bodies against one another.[P] Mei, covered in the sweat of her day's toil, caressed the blue flesh of her leafy mistress.[P] Their fluids merged "..
                         "and mixed as their bodies embraced.[B][C]") ]])
	fnCutscene([[ Append("Unable to resist any command, Mei still felt the pleasure of touching and being touched.[P] Adina proved an able lover.[P] The two continued to kiss as their "..
                         "hands explored one another's features.[B][C]") ]])
	fnCutscene([[ Append("Adina soon found Mei was sensitive along the small of her back.[P] With each touch, Mei would push herself towards Adina.[P] Now, Adina pulled.[P] Mei made no effort to resist her mistress.[B][C]") ]])
	fnCutscene([[ Append("One of the vines that wrapped around Adina's shoulder unwrapped itself and slid down as Adina continued to caress Mei's midriff.[P] Mei mindlessly awaited it.[B][C]") ]])
	fnCutscene([[ Append("The vine entered her dress and found its way to her underwear.[P] It slid in, as did Adina's hand.[P] Mei's followed, and the three appendages began to massage her sex in tandem.[B][C]") ]])
	fnCutscene([[ Append("Mistress and slave writhed together as their hands and vines worked in silent tandem.[P] Adina withdrew her hand as another vine took its place, instead sending her hand to her own sex.[B][C]") ]])
	fnCutscene([[ Append("Adina controlled Mei's masturbation to keep pace with her own.[P] When she slowed, her thrall did.[P] When she hastened, her thrall did.[P] Mei submitted in her entirety "..
                         "to the motions of Adina's vines.[B][C]") ]])
	fnCutscene([[ Append("Mei's crescendo came in perfect step with Adina's.[P] As Adina was about to climax, Mei held her own back.[P] Her mistress had not ordered her to cum, so she did not.[P] "..
                         "The strain began to build.[P] Mei brought all her efforts to stopping her own orgasm.[B][C]") ]])
	fnCutscene([[ Append("The sensation continued to build, yet Mei remained resolute.[P] She would not orgasm, she would not fail her mistress.[P] Adina smiled at the sight of her loyal thrall "..
                         "shaking with intense pleasure as she built higher and higher.[B][C]") ]])
	fnCutscene([[ Append("Now, with a kiss, Adina allowed her thrall to finish...[B][C]") ]])
	fnCutscene([[ Append("Racked by the powerful orgasm, Mei's body accepted her total subservience as her mind accepted its blankness.[P] She fell unconscious as Adina basked in the afterglow...") ]])
	fnCutsceneBlocker()

-- |[Subsequent Days]|
--Every day after the first, this scene plays.
else

	-- |[Dialogue]|
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	fnCutscene([[ Append("Adina: Thrall.[P] Undress.[P] Lie down.[B][C]") ]])
	fnCutscene([[ Append("Thrall: Yes mistress.[B][C]") ]])
	fnCutscene([[ Append("Adina: Present yourself.[B][C]") ]])
	fnCutscene([[ Append("Thrall: Yes mistress.[P] I obey.") ]])
	fnCutsceneWait(30)
	fnCutsceneBlocker()

	-- |[Scene]|
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
    fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/AdinaSexScene/AdinaSexScene") ]])
	fnCutscene([[ Append("Mei waited patiently as she lay on the bed, presenting her rear to her mistress.[P] No thoughts passed her head except that she needed to maintain her pose until told otherwise.[B][C]") ]])
	fnCutscene([[ Append("She could hear her mistress pacing on the salted floor behind her, but she did not know what she was doing.[P] She did not care.[P] She had not been told to care.[B][C]") ]])
	fnCutscene([[ Append("A few moments passed as Mei anticipated her next instruction.[P] She felt something cool and slimy touch her thigh and begin to trace along her leg, moving towards her exposed sex.[P] "..
                         "She did not move or react.[B][C]") ]])
	fnCutscene([[ Append("The cool feeling drew up her leg, wrapping around it as it travelled upwards.[P] Her mistress had moved closer, and now sat next to her on the bed.[P] "..
                        "She placed her soft hand on her thrall's back.[B][C]") ]])
	fnCutscene([[ Append("The feeling of her mistress touching her sent a wave of warmth through her.[P] The hand reassured her empty mind, brought it peace and security.[P] Whatever traces of "..
                         "independence she had felt melted from the thrall's mind.[B][C]") ]])
	fnCutscene([[ Append("The oozing vine of her mistress reached into her sex and began to prod.[P] Instinctively, Mei moved her hips to accommodate it, helping it as it did its duty.[P] "..
                         "The hand of her mistress remained firm on her back.[B][C]") ]])
	fnCutscene([[ Append("Her mistress had begun to touch herself, starting slowly as the vine prodded inside Mei in the same rhythm.[P] Each push inside her was matched by her mistress stroking "..
                         "herself.[P] Mei kept her position and waited for further instructions.[B][C]") ]])
	fnCutscene([[ Append("The hand of her mistress withdrew, replaced by her other.[P] It was cooler, wetter, covered in the nectar of her mistress.[P] The vine pressed deeper as Mei began to build.[P] "..
                         "Anticipation filled her body, but not her empty mind.[B][C]") ]])
	fnCutscene([[ Append("Her mistress stood and repositioned herself behind Mei.[P] She could feel her mistress leaning near her rear.[P] Her mistress pressed her tongue against Mei's lips, working "..
                         "with the slimy vine.[P] Mei built higher, and higher, but still she held her pose.[P] Her mistress had not told her otherwise.[B][C]") ]])
	fnCutscene([[ Append("The pleasure filled her body.[P] Every inch of her trembled with anticipation as she felt wave after wave surge and break inside her.[P] Her mistress was perfect with "..
                         "her tongue.[P] Mei, without being told, now had a thought of her own.[P] She wanted to be perfect for her mistress.[B][C]") ]])
	fnCutscene([[ Append("Mei was now shuddering with sheer delight.[P] Obedience combined with physical stimulation to produce a euphoric wave that threatened to overwhelm her pitiful mind.[P] "..
                         "One thing stood in the way of her orgasm.[P] She had not been ordered to.[P] Not yet.[B][C]") ]])
	fnCutscene([[ Append("The tongue of her mistress withdrew from her, as did the vine.[P] Mei did not care, and could not care.[P] She had held her position and not moved without being told.[P] "..
                         "She had fulfilled her purpose as a thrall.[P] That would have been enough, if her mistress willed it.[B][C]") ]])
	fnCutscene([[ Append("Her mistress, seeing her thrall prostrated before her, clearly shuddering from intense pleasure, said one word.[P][P] 'Cum'.[B][C]") ]])
	fnCutscene([[ Append("The wave overtook Mei, overwhelming her blank, obedient mind.[P] She blacked out in sheer pleasure as every nerve in her body lit in unison.[P] Her sleep was dreamless, thoughtless, and obedient.[P] She was her mistress' thrall, now, and forever.") ]])
	fnCutsceneBlocker()


end

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[ ==================== Finish Up =================== ]|
--Level transition.
fnCutscene([[ AL_BeginTransitionTo("SaltFlats", gsRoot .. "Chapter 1/Scenes/Adina_Sex_Scene/Scene_PostTransition.lua") ]])
fnCutsceneBlocker()
