-- |[ ================================ Zombee Subversion Scene ================================= ]|
--This is a special scene that only plays if the player tries to switch Mei to Zombee form during
-- the battle with the Cultist Corrupter. It doesn't go well for the party.
--This kicks the party back to the last save point. This is meant to be explicitly called in combat
-- and will end the combat as though a retreat action occurred.

-- |[Flag]|
VM_SetVar("Root/Variables/Chapter1/Scenes/iSpecialZombeeFlag", "N", 1.0)

-- |[Dialogue]|
fnStandardMajorDialogue()
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "CultistF", "Neutral") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Ungh...[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Surprise] Mei?[P] What are you doing?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] I can control...[P] it...[P] I can...[B][C]") ]])
fnCutscene([[ Append("Cultist:[E|Neutral] Hah![P] Nobody can resist the call of this intoxicating power.[P] Drone, come to me![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] N-[P]no![P] I will...[B][C]") ]])
fnCutscene([[ Append("Cultist:[E|Neutral] Your will is nothing!") ]])
fnCutsceneBlocker()

-- |[Beat Scene]|
fnCutscene([[ AudioManager_PlaySound("World|Heartbeat") ]])
fnCutscene([[ AL_SetProperty("Activate Fade", 120, gci_Fade_Over_GUI, false, 0.8, 0.2, 1.0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()

-- |[Dialogue]|
fnStandardMajorDialogue()
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "CultistF", "Neutral") ]])
fnCutscene([[ Append("Florentina:[E|Surprise] Mei?[P] Speak to me![B][C]") ]])
fnCutscene([[ Append("Drone:[VOICE|Bee][E|Neutral] Drone...[P] Droooone...[B][C]") ]])
fnCutscene([[ Append("Cultist:[E|Neutral] She's mine now, and always was.[P] What can you possibly do to resist this overwhelming power?[B][C]") ]])
fnCutscene([[ Append("Cultist:[E|Neutral] She came back here and willingly gave herself up.[P] She longs for it![B][C]") ]])
fnCutscene([[ Append("Drone:[VOICE|Bee][E|Neutral] Drone longs to be controlled.[P] Drone will obey.[B][C]") ]])
fnCutscene([[ Append("Cultist:[E|Neutral] Aid me against this unbeliever![B][C]") ]])
fnCutscene([[ Append("Drone:[VOICE|Bee][E|Neutral] Drone obeys.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] Oh yeah?[P] Two can play at that game, punk![P] Hey drone, eat runestone![B][C]") ]])
fnCutscene([[ Append("Drone:[VOICE|Bee][E|Neutral] Ngh!") ]])
fnCutsceneBlocker()

-- |[Flashwhite]|
fnCutscene([[ AL_SetProperty("Activate Fade", 60, gci_Fade_Over_GUI, true, 0.0, 0.0, 0.0, 0, 1, 1, 1, 1) ]])
fnCutsceneWait(80)
fnCutsceneBlocker()

-- |[End Combat]|
fnCutscene([[ AdvCombat_SetProperty("Instant End") ]])

-- |[Transition to Last Save]|
local sPostScenePath = fnResolvePath() .. "Post Scene.lua"
local sCallString = "AL_BeginTransitionTo(\"LASTSAVE\", \"" .. sPostScenePath .. "\")"
fnCutscene(sCallString)
fnCutscene([[ AdvCombat_SetProperty("Restore Party") ]])
