-- |[ ============================== Zombee Subversion Post-Scene ============================== ]|
--Post execution scene played around a campfire.

-- |[Flashwhite]|
fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Over_GUI, true, 1, 1, 1, 1, 1, 1, 1, 1) ]])
fnCutsceneWait(40)
fnCutsceneBlocker()

--Flash down.
fnCutscene([[ AL_SetProperty("Activate Fade", 60, gci_Fade_Over_GUI, true, 1, 1, 1, 1, 1, 1, 1, 0) ]])
fnCutsceneWait(60)
fnCutsceneBlocker()

-- |[Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
fnCutscene([[ Append("Florentina:[E|Neutral] It worked![P] Well that's neat.[P] Maybe I'll start smacking that runestone more often.[B][C]") ]])
fnCutscene([[ Append("Drone:[VOICE|Bee][E|Neutral] Drone...[P] cannot hear hive.[P] Confused.[P] Alone.[P] Alone...[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] Hey, it's okay.[P] Florentina is here.[P] C'mon.[P] Give me a hug.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] And breathe in my opiates...[B][C]") ]])
fnCutscene([[ Append("Drone:[VOICE|Bee][E|Neutral] ...[P] Relaxed...[P] Drone...[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] I'm not...[P] Florentina?[P] What happened?[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] Oh gee, I'm not the one who decided to try transforming into something that a freak cultist could easily manipulate?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] I did that?[P] I did this to myself?") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

-- |[Transform]|
Cutscene_CreateEvent("Flash Mei White", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Flashwhite Quickly")
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Bee.lua") ]])
fnCutsceneWait(gci_Flashwhite_Ticks_Total)
fnCutsceneBlocker()
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
fnCutscene([[ Append("Mei:[E|Happy] Much better![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] Oh, but...[P] I'm so sorry, Florentina.[P] I don't know what came over me...[P] I...[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Cry] What if you hadn't been there and...[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Blush] Geez kid, don't cry on me.[P] There, there.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Blush] Can you tell me what happened?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] I just got so angry, and I wanted to pound that smelly jerk into the floor.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] And I felt my runestone throb and the changes just overwhelmed me.[P] I didn't even think of it, it just happened.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] I'm not the kind of person who can pretend to be an expert on...[P] whatever it is that freak cultist is doing.[P] But what I can say is, extreme emotions are my expertise.[P] That's how you get people.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] Whatever it is those zombees had done to them preys on the flaws in your psyche.[P] Extreme anger, that lets them in.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] Or extreme fear.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] Is that what you felt the first time?[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] The bees must be terrified to be isolated, away from all the thoughts of their friends.[P] So they become open to being used.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] We have to go rescue them, but...[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Surprise] You want to take another swing at that hooded goober?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Angry] I'll pull her arms off and beat her with them for what - [B][C]") ]])
fnCutscene([[ Append("Mei:[E|Surprise] I did it again![B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] Just keep a lid on it and we'll be fine.[P] Especially keep your hands off that runestone.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] Right, right.[P] We can do this.[P] I just need to keep a level head.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

-- |[Finish]|
fnAutoFoldParty()
fnCutsceneBlocker()

-- |[Flags]|
fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Scenes/iSpecialZombeeFlag", "N", 0.0) ]])
