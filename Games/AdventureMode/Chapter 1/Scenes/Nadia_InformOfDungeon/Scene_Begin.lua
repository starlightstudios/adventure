-- |[Nadia Informs of the Dungeon]|
--Nadia will inform the party about the Trap Dungeon.

-- |[Variables]|
--Flag this scene not to fire twice.
VM_SetVar("Root/Variables/Chapter1/Scenes/iInformedOfDungeon", "N", 1.0)

--Get values.
local iHasAlrauneForm = VM_GetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N")
local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S") 

-- |[Talking]|
--Dialogue.
fnStandardMajorDialogue()
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Nadia", "Neutral") ]])
fnCutscene([[ Append("Nadia: Oh![P] There you are![P] I've been looking everywhere for you![B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] But we haven't even done anything awful yet![B][C]") ]])
fnCutscene([[ Append("Nadia: Don't be a big silly, this is serious![B][C]") ]])
fnCutscene([[ Append("Nadia: Mei, the Alraunes told me to give you a special message.[B][C]") ]])

--Mei does not have Alraune form:
if(iHasAlrauneForm == 0.0) then
    
    --Mei is in human form:
    if(sMeiForm == "Human") then
        fnCutscene([[ Append("Mei:[E|Surprise] What would they want with me?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Confused] Probably going to beg you to join them.[P] Typical.[B][C]") ]])
    
    --Bee:
    elseif(sMeiForm == "Bee") then
        fnCutscene([[ Append("Mei:[E|Surprise] What would they want with me?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] Maybe she wants some 'special attention', heh.[B][C]") ]])
    
    --All other cases:
    else
        fnCutscene([[ Append("Mei:[E|Surprise] What would they want with me?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] I bet somebody stepped on a sunflower.[P] You have to go rough them up.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] Actually, I think that'd be a good job.[P] I'm making her sound better.[B][C]") ]])
    end
	fnCutscene([[ Append("Nadia: Oh no, it's not like that![B][C]") ]])
	fnCutscene([[ Append("Nadia: Rochea said that you really ticked off a bunch of nasty cultists who had set up in the Dimensional Trap ruins.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] Yeah?[P] Well that's what they get for kidnapping me![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] ...[P] Who's Rochea, again?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] She's the elder of the local Alraune covenant.[B][C]") ]])
	fnCutscene([[ Append("Nadia: She says the cultists are up to something big and she wants your help.[P] She said you'd know them best.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] Huh, so she's been spying on us, has she?[P] We're not interested.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] Oh, we're interested.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Mei?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] If the Alraunes are going to take the fight to them, I can put aside our differences.[P] Where do we meet them?[B][C]") ]])
	fnCutscene([[ Append("Nadia: She wouldn't say.[P] But it'll be someplace near the ruins...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] If I recall correctly, there's a basement access room on the southwest edge of the building.[P] It's been a while, we might have to search.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] Though I would like to point out that this is a bad idea.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Smirk] You don't want to rough up the cultists?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] Oh, very much so.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] It's Rochea and her ilk that I don't trust...") ]])

--Mei has Alraune form:
else
	fnCutscene([[ Append("Mei:[E|Surprise] Is something wrong?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] I bet somebody stepped on a sunflower.[P] Oohhhh noooo.[B][C]") ]])
	fnCutscene([[ Append("Nadia: Oh come on, Florry![B][C]") ]])
	fnCutscene([[ Append("Nadia: Rochea said that you really ticked off a bunch of nasty cultists who had set up in the Dimensional Trap ruins.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] Yeah?[P] Well that's what they get for kidnapping me![B][C]") ]])
	fnCutscene([[ Append("Nadia: She says they're up to something big and she wants your help.[P] She said you'd know them best.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] Not interested.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] Oh, we're interested.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Mei?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] Those rotten cultists kidnapped me.[P] If my leaf-sisters want to take the fight to them, I'll be right there with them.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] Where does Rochea need us?[B][C]") ]])
	fnCutscene([[ Append("Nadia: She wasn't specific.[P] She said they had broken through into their basement, but I don't know how you'd get there from the surface.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] I think there's a secondary access in the southwest corner of the building.[P] We should look there.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] Though I would like to point out that this is a bad idea.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Smirk] You don't want to rough up the cultists?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] Oh, very much so.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] It's Rochea and her ilk that I don't trust...") ]])

end

--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()
