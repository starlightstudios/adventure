-- |[Beehive Basement - No Florentina]|
--Mei tries to enter the beehive basement mini-dungeon without the help of Florentina. The bees won't let her in.
--The scene should be triggered by a floor trigger. It can't be activated from the debug menu.

-- |[Variables]|
local sMeiForm    = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local iHasBeeForm = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")

-- |[Movement]|
--Move Mei to the blockade.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", 18.25 * gciSizePerTile, 23.50 * gciSizePerTile)
DL_PopActiveObject()

--Bees move to intercept!
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepA")
	ActorEvent_SetProperty("Move To", 17.25 * gciSizePerTile, 24.50 * gciSizePerTile)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepB")
	ActorEvent_SetProperty("Move To", 17.25 * gciSizePerTile, 23.50 * gciSizePerTile)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepC")
	ActorEvent_SetProperty("Move To", 17.25 * gciSizePerTile, 22.50 * gciSizePerTile)
DL_PopActiveObject()

--Turn to face Mei.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepA")
	ActorEvent_SetProperty("Face", 1.0, 0.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepB")
	ActorEvent_SetProperty("Face", 1.0, 0.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepC")
	ActorEvent_SetProperty("Face", 0.0, 1.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", -1.0, 0.0)
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(35)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnStandardMajorDialogue()
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Bee", "Neutral") ]])

--Mei is not a bee, does not have bee form:
if(sMeiForm ~= "Bee" and iHasBeeForm == 0.0) then
	fnCutscene([[ Append("Mei:[E|Offended] Yeah, you want some?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] ...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Hey, bees?[P] This is the part where you attack me...[B][C]") ]])
	fnCutscene([[ Append("*The bees seem to be nervously trying to keep you away...*[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Just what's going on down here, anyway?[B][C]") ]])
	fnCutscene([[ Append("*The bees keep fidgeting and looking over their shoulders...*[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Uh, could I help?[P] Are you in trouble?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Well, maybe I'll come back later.[P] When you're a bit more talkative.") ]])
	fnCutsceneBlocker()

--Mei is not a bee, and has bee form:
elseif(sMeiForm ~= "Bee" and iHasBeeForm == 1.0) then
	fnCutscene([[ Append("Mei:[E|Neutral] Zzzz! Zz! Bzz? (Drone sisters![P] It's me![P] Don't you recognize me?)[B][C]") ]])
	fnCutscene([[ Append("Bee: Zz. Bzz. ZzzZzz. (Drone unidentified. Language credentials accepted. Go back.)[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Zz? (Why?)[B][C]") ]])
	fnCutscene([[ Append("Bee: Zzzz. BzzZz. ZzzBzz. (Information dangerous.[P] Hive under threat.[P] Forget this encounter.)[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Bzz! ZzzZz! (Let me help![P] I am autonomous!)[B][C]") ]])
	fnCutscene([[ Append("Bee: Zzzz. BzzZz. ZzzBzz. (Information dangerous.[P] Hive under threat.[P] Forget this encounter.)[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] ZzzBzzz. (...[P] I will forget this encounter.)") ]])
	fnCutsceneBlocker()
	
	--In this case, Mei walks a bit to the right.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", 22.25 * gciSizePerTile, 23.50 * gciSizePerTile)
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(35)
	fnCutsceneBlocker()
	
	--Brief dialogue.
	fnStandardMajorDialogue()
	fnCutscene([[ Append("Mei:[E|Offended] (Sorry sisters, but I can't forget this.[P] I'll have to get help!)") ]])
	fnCutsceneBlocker()

--Mei is a bee:
elseif(sMeiForm == "Bee") then
	fnCutscene([[ Append("Mei:[E|Neutral] (Sister drone, you are not speaking with the hive.[P] Why?[P] What has happened?)[B][C]") ]])
	fnCutscene([[ Append("Bee: (Isolation required.[P] Unknown threat.[P] Threat intercepts our messages.)[B][C]") ]])
	fnCutscene([[ Append("Bee: (Drones maintain quarantine.[P] You will leave.)[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] (I can help!)[B][C]") ]])
	fnCutscene([[ Append("Bee: (Drones maintain quarantine.[P] You will leave.)[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] (I will...[P] maintain quarantine.[P] I will leave.)") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(35)
	fnCutsceneBlocker()
	
	--In this case, Mei walks a bit to the right.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", 22.25 * gciSizePerTile, 23.50 * gciSizePerTile)
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(35)
	fnCutsceneBlocker()
	
	--Brief dialogue.
	fnStandardMajorDialogue()
	fnCutscene([[ Append("Mei:[E|Sad] (Those drones are going to need help, but I can't tell the other drones.[P] What am I going to do?)") ]])
	fnCutsceneBlocker()

end
