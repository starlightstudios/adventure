-- |[ =============================== Trannadar Third Scene: Any =============================== ]|
--If Nadia first met Mei in a form, and then sees her in a different form, then Nadia deduces that Mei can shapeshift.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarThirdScene", "N", 1.0)

-- |[Setup]|
--Variables.
local sTrannadarSecondSceneForm = VM_GetVar("Root/Variables/Chapter1/Scenes/sTrannadarSecondSceneForm", "S")
local iFlorentinaPresent = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")

--Determine which side the player entered from.
local bIsPlayerAtBottom = false
local bIsPlayerAtTop = false
EM_PushEntity("Adventure Party", 0)
	local iPlayerX, iPlayerY = TA_GetProperty("Position")
DL_PopActiveObject()
if(iPlayerY >= 37 * 16) then bIsPlayerAtBottom = true end
if(iPlayerY <   7 * 16) then bIsPlayerAtTop = true end

--Wait a bit for the fade.
fnCutsceneWait(15)
fnCutsceneBlocker()

-- |[Movement]|
--Mei walks towards Nadia. This is based on where she spawned.
if(bIsPlayerAtBottom) then
    fnCutsceneMove("Mei", 28.25, 38.50)
    if(iFlorentinaPresent == 1) then
        fnCutsceneMove("Florentina", 28.25, 39.50)
    end
elseif(bIsPlayerAtTop) then
    fnCutsceneMove("Mei", 23.25, 6.50)
    if(iFlorentinaPresent == 1) then
        fnCutsceneMove("Florentina", 24.25, 6.50)
    end
else
    fnCutsceneMove("Mei", 43.25, 28.50)
    if(iFlorentinaPresent == 1) then
        fnCutsceneMove("Florentina", 44.25, 28.50)
    end
end
fnCutsceneBlocker()

--Change the music.
fnCutscene([[ AudioManager_PlayMusic("NadiasTheme") ]])

--Wait a bit.
fnCutsceneWait(5)
fnCutsceneBlocker()

-- |[Dialogue]|
fnStandardMajorDialogue()
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Nadia", "Neutral") ]])
fnCutscene([[ Append("Nadia: Mei?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Nadia![P] Good to see you.[B][C]") ]])
fnCutscene([[ Append("Nadia: Do my eyes deceive me?[P] I don't know what to think anymore.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] I don't follow.[B][C]") ]])

if(sTrannadarSecondSceneForm == "Alraune") then
	fnCutscene([[ Append("Nadia: We were leaf-sisters![P] What happened?[P] How?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] W-we still are![P] I just, uh...[B][C]") ]])
elseif(sTrannadarSecondSceneForm == "Bee") then
	fnCutscene([[ Append("Nadia: You're not a bee anymore![P] There's gotta be a story behind that, I won't accept it if there isn't![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Sad] Well, y'see...[B][C]") ]])
elseif(sTrannadarSecondSceneForm == "Ghost") then
	fnCutscene([[ Append("Nadia: You're alive![P] Alive![P] This is a major change from the status quo.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Sad] Well, um...[B][C]") ]])
elseif(sTrannadarSecondSceneForm == "Gravemarker") then
	fnCutscene([[ Append("Nadia: You had me thinking you were a marble angel![P] Or granite![P] Basalt![P] I don't know rocks very well but you weren't *this*![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Sad] Well, y'see...[B][C]") ]])
elseif(sTrannadarSecondSceneForm == "Slime") then
	fnCutscene([[ Append("Nadia: Well you were a slime![P] Are you a slime?[P] Is this a disguise?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] Uhhhh....[B][C]") ]])
elseif(sTrannadarSecondSceneForm == "Werecat") then
	fnCutscene([[ Append("Nadia: What happened to those claws, that fur?[P] Was it a cheap costume and I fell for it?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Sad] Well, um...[B][C]") ]])
elseif(sTrannadarSecondSceneForm == "Wisphag") then
	fnCutscene([[ Append("Nadia: Mei![P] Mei![P] The little wisp, where did it go!?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Sad] Well, um...[B][C]") ]])
elseif(sTrannadarSecondSceneForm == "Mannequin") then
	fnCutscene([[ Append("Nadia: I am now very confused as to how you got un-sapped.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Sad] One, it was *resin*, not sap.[P] And two, uhhh.....[B][C]") ]])
else
	fnCutscene([[ Append("Nadia: You were a human the whole time![P] B-[P]but how?[P] How is this possible?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Sad] Errr...[B][C]") ]])
end

fnCutscene([[ Append("Nadia: Are you some kinda magic shapeshifter?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] That's...[P] one way of putting it.[B][C]") ]])
fnCutscene([[ Append("Nadia: That.[P] Is.[P] So.[P] COOL!!![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Heh.[B][C]") ]])
fnCutscene([[ Append("Nadia: You should have told me that from the beginning, I would have understood![P] And even if I didn't, I'd pretend like I did.[P] I'm really good at that.[B][C]") ]])
fnCutscene([[ Append("Nadia: Don't worry, ol' Nadia's got you covered.[P] I'll keep your secret.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] I don't think this constitutes a secret.[P] I'm not exactly trying to hide it.[B][C]") ]])
fnCutscene([[ Append("Nadia: I'll only tell my closest of close confidants.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Happy] You mean everyone.[B][C]") ]])
fnCutscene([[ Append("Nadia: Right, I'll tell everyone!") ]])
fnCutsceneBlocker()

-- |[Finish Up]|
fnCutscene([[ AudioManager_PlayMusic("TownTheme") ]])
fnCutsceneBlocker()

fnAutoFoldParty()
fnCutsceneBlocker()