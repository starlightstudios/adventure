-- |[ ============================== Trannadar Intro Scene: Slime ============================== ]|
--If Mei is a slime the first time she enters Trannadar Trading Post, this scene plays.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N", 1.0)

-- |[Move In From Edge]|
--Determine which side the player entered from.
local bIsPlayerAtBottom = false
local bIsPlayerAtTop = false
EM_PushEntity("Adventure Party", 0)
	local iPlayerX, iPlayerY = TA_GetProperty("Position")
DL_PopActiveObject()
if(iPlayerY >= 37 * 16) then bIsPlayerAtBottom = true end
if(iPlayerY <   7 * 16) then bIsPlayerAtTop = true end

--Wait a bit for the fade.
fnCutsceneWait(15)
fnCutsceneBlocker()

-- |[Movement]|
--Mei walks towards Nadia and Blythe. This is based on where she spawned.
if(bIsPlayerAtBottom) then
    fnCutsceneMove("Mei", 28.25, 38.50)
elseif(bIsPlayerAtTop) then
    fnCutsceneMove("Mei", 23.25, 6.50)
else
    fnCutsceneMove("Mei", 43.25, 28.50)
end
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(5)
fnCutsceneBlocker()

--Change the music.
fnCutscene([[ AudioManager_PlayMusic("NadiasTheme") ]])

--Face Nadia and Blythe south.
if(bIsPlayerAtBottom) then
    fnCutsceneFace("Nadia", 0, 1)
    fnCutsceneFace("Blythe", 0, 1)

--Face north.
elseif(bIsPlayerAtTop) then
    fnCutsceneFace("Nadia", 0, -1)
    fnCutsceneFace("Blythe", 0, -1)
    
--Face them east.
else
    fnCutsceneFace("Nadia", 1, 0)
    fnCutsceneFace("Blythe", 1, 0)
end
fnCutsceneWait(5)
fnCutsceneBlocker()

-- |[Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Nadia", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Blythe", "Neutral") ]])
fnCutscene([[ Append("Man: ...[P] between all the complaints and the strange goings-on lately![B][C]") ]])
fnCutscene([[ Append("Alraune: Complaints?[P] It was just a turnip![P] Am I never going to live this down?[B][C]") ]])
fnCutscene([[ Append("Man: Heads up, we have company.[B][C]") ]])
fnCutscene([[ Append("Alraune: What?[P] Oh, it's just a slime.[P] Shoo, shoo![P] Get out of here![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Angry] What?[P] I'm not a slime![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] Oh wait, uh, oops.[P] I guess I look like a slime don't I?[B][C]") ]])
fnCutscene([[ Append("Man: Did that slime just talk?[B][C]") ]])
fnCutscene([[ Append("Alraune: See, boss?[P] I told you I'm not making stuff up, they can talk![B][C]") ]])
fnCutscene([[ Append("Man: One talking slime does not a conspiracy make, Nadia.[P] You still have to replace the weather vane.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Am I interrupting something here?[B][C]") ]])
fnCutscene([[ Append("Nadia: No way![P] This is so cool![P] Are you a friendly slime?[P] Ohmygosh, do you want to be my friend?[B][C]") ]])

--Variables.
local iHasReadNadiaSign = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasReadNadiaSign", "N")

--If you've read one of Nadia's signs...
if(iHasReadNadiaSign == 1.0) then
	fnCutscene([[ Append("Mei:[E|Happy] Your name is Nadia?[P] Are you the one who made all those signs?[B][C]") ]])
	fnCutscene([[ Append("Nadia: Eeeeee![P] The slime likes my signs![P] Am I really popular in the slime community?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Uh, I don't know.[P] I haven't been a slime very long.[B][C]") ]])
	fnCutscene([[ Append("Man: Hrmph, this may be more sorcery than miracle.[B][C]") ]])

--If you somehow missed Nadia's signs...
else
	fnCutscene([[ Append("Mei:[E|Offended] I suppose so, what with the chilly reception I've gotten since winding up here.[P] I mean, just look at me![B][C]") ]])
	fnCutscene([[ Append("Nadia: That's what happens when you wander around all by yourself.[P] Always buddy up if you're going exploring.[B][C]") ]])
	fnCutscene([[ Append("Man: I think it's a little late for that advice, Nadia.[B][C]") ]])
	fnCutscene([[ Append("Man: Still, this is not the most unusual thing we've seen recently.[B][C]") ]])
end

--Resume.
fnCutscene([[ Append("Man: As you seem in control of your faculties, I am not going to restrict access to the trading post to you.[B][C]") ]])
fnCutscene([[ Append("Man: Be advised, though, that we will be watching you with great earnest.[P] No sudden moves.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] I didn't intend to pick a fight.[B][C]") ]])
fnCutscene([[ Append("Man: That's what I like to hear.[B][C]") ]])
fnCutscene([[ Append("Blythe: I am captain Darius Blythe, by the way.[P] Be on your best behavior and we can have a fulfilling relationship.[B][C]") ]])
fnCutscene([[ Append("Nadia: Yeah, we're gonna be best friends![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] (This place is getting crazier by the minute.)[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Could either of you perhaps tell me what the symbol on this stone means?[B][C]") ]])
fnCutscene([[ Append("Nadia: Huh.[P] That looks familiar, but I can't place it.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Crud.[B][C]") ]])
fnCutscene([[ Append("Nadia: Must be a foreign language.[P] You should go ask Florentina![P] She knows everything because she knows everyone.[P] I bet she can find out what it means.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Worth a shot.[P] Thanks.") ]])
fnCutsceneBlocker()

-- |[Finish Up]|
--Change the music.
fnCutscene([[ AudioManager_PlayMusic("TownTheme") ]])
fnCutsceneBlocker()

--Unlock topics.
WD_SetProperty("Unlock Topic", "Alraunes", 1)
WD_SetProperty("Unlock Topic", "Blythe", 1)
WD_SetProperty("Unlock Topic", "Cultists", 1)
WD_SetProperty("Unlock Topic", "Florentina", 1)
WD_SetProperty("Unlock Topic", "Job", 1)
WD_SetProperty("Unlock Topic", "Nadia", 1)
WD_SetProperty("Unlock Topic", "Name", 1)
WD_SetProperty("Unlock Topic", "TradingPost", 1)
WD_SetProperty("Unlock Topic", "Challenge", 1)
