-- |[ ============================== Trannadar Intro Scene: Human ============================== ]|
--If Mei is a human the first time she enters Trannadar Trading Post, this scene plays.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N", 1.0)

-- |[Move In From Edge]|
--Determine which side the player entered from.
local bIsPlayerAtBottom = false
local bIsPlayerAtTop = false
EM_PushEntity("Adventure Party", 0)
	local iPlayerX, iPlayerY = TA_GetProperty("Position")
DL_PopActiveObject()
if(iPlayerY >= 37 * 16) then bIsPlayerAtBottom = true end
if(iPlayerY <   7 * 16) then bIsPlayerAtTop = true end

--Wait a bit for the fade.
fnCutsceneWait(15)
fnCutsceneBlocker()

-- |[Movement]|
--Mei walks towards Nadia and Blythe. This is based on where she spawned.
if(bIsPlayerAtBottom) then
    fnCutsceneMove("Mei", 28.25, 38.50)
elseif(bIsPlayerAtTop) then
    fnCutsceneMove("Mei", 23.25, 6.50)
else
    fnCutsceneMove("Mei", 43.25, 28.50)
end
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(5)
fnCutsceneBlocker()

--Change the music.
fnCutscene([[ AudioManager_PlayMusic("NadiasTheme") ]])

--Face Nadia and Blythe south.
if(bIsPlayerAtBottom) then
    fnCutsceneFace("Nadia", 0, 1)
    fnCutsceneFace("Blythe", 0, 1)

--Face north.
elseif(bIsPlayerAtTop) then
    fnCutsceneFace("Nadia", 0, -1)
    fnCutsceneFace("Blythe", 0, -1)
    
--Face them east.
else
    fnCutsceneFace("Nadia", 1, 0)
    fnCutsceneFace("Blythe", 1, 0)
end
fnCutsceneWait(5)
fnCutsceneBlocker()

-- |[Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Nadia", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Blythe", "Neutral") ]])
fnCutscene([[ Append("Man: ...[P] and the plural of anecdote is not evidence![B][C]") ]])
fnCutscene([[ Append("Alraune: What's a plural?[P] And what's an anecdote?[B][C]") ]])
fnCutscene([[ Append("Alraune: Oh, wait, nevermind.[P] I think I see an anecdote right now.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Er, hello?[B][C]") ]])
fnCutscene([[ Append("Alraune: Greetings, anecdote![P] Welcome to Trannadar Trading Post![P] I'm Nadia, and I forgot what I was supposed to say after that![B][C]") ]])
fnCutscene([[ Append("Nadia: How'd I do, Cap'n?[B][C]") ]])
fnCutscene([[ Append("Man: Well, she didn't burst into tears, so you're improving.[B][C]") ]])

--Variables.
local iHasMetFriendly = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasMetFriendly", "N")
local iHasReadNadiaSign = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasReadNadiaSign", "N")

--If Mei hasn't met anyone friendly yet, this happens:
if(iHasMetFriendly == 0.0) then
	VM_SetVar("Root/Variables/Chapter1/Scenes/iHasMetFriendly", "N", 1.0)
	fnCutscene([[ Append("Mei:[E|Laugh] What a relief![P] You're the first thing I've seen today that hasn't attacked me![B][C]") ]])
	fnCutscene([[ Append("Nadia: You're all by yourself?[P] Yeah, the forest is crawling with monsters this time of year.[P] You should always buddy up if you go exploring.[B][C]") ]])
	fnCutscene([[ Append("Nadia: An anecdote like you, in an outfit like that, all alone?[P] That's just asking for trouble.[B][C]") ]])
	fnCutscene([[ Append("Man: Oh for crying out loud, Nadia![P] That's not what that word means![B][C]") ]])

--Mei has met at least one other friendly and read at least one of Nadia's signs.
elseif(iHasReadNadiaSign == 1.0) then
	fnCutscene([[ Append("Mei:[E|Happy] You're Nadia?[P] You're the one who made those signs?[B][C]") ]])
	fnCutscene([[ Append("Nadia: Gee, I've never had a fan before![P] Do you want an autograph?[P] Get it?[B][C]") ]])
	fnCutscene([[ Append("Man: Oh no...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Get what?[B][C]") ]])
	fnCutscene([[ Append("Nadia: Do you want me to *sign* something?[P] Ha ha ha ha![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Sad] That is in keeping with what I've seen so far.[P] Is she always like this?[B][C]") ]])
	fnCutscene([[ Append("Man: Yes, all the time.[B][C]") ]])

--Mei has met at least one other friendly but somehow missed Nadia's signs.
else
	fnCutscene([[ Append("Mei:[E|Happy] Well it's very nice to meet you.[P] I've been having a pretty rough day.[B][C]") ]])
	fnCutscene([[ Append("Nadia: Of course it'd be coarse out there.[P] These woods are very dangerous, especially when you're alone.[B][C]") ]])
	fnCutscene([[ Append("Nadia: If you're going out exploring, you should always bring a friend.[P] Anecdotes need to stick together, you know.[B][C]") ]])
	fnCutscene([[ Append("Man: That word.[P] You keep using it...[B][C]") ]])
end

--Common.
fnCutscene([[ Append("Man: *ahem*[P] I am captain Darius Blythe, head of the trading post's guard.[P] If you need anything, please come see me.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] What I really need is for someone to help me find a way home.[B][C]") ]])
fnCutscene([[ Append("Nadia: Are you lost?[P] Where are you from?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] If I said 'Hong Kong', would that ring a bell?[B][C]") ]])
fnCutscene([[ Append("Blythe: ...[P] Never heard of it.[B][C]") ]])
fnCutscene([[ Append("Mei: China?[P] Asia?[B][C]") ]])
fnCutscene([[ Append("Nadia: Sorry.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] I was afraid you'd say that.[P] I get the sinking feeling I'm not on Earth anymore...[B][C]") ]])
fnCutscene([[ Append("Nadia: Sinking?[P] Earth?[P] Like, quicksand?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Cry] Oh no...[B][C]") ]])
fnCutscene([[ Append("Nadia: H-hey! Don't cry![P] Was it something I said?[P] My jokes aren't that bad, are they?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Cry] Eh, no, no, it wasn't the joke.[P][E|Offended] Okay, it wasn't just the joke.[P] I'll be fine, really.[P] There's gotta be a reasonable explanation for how I got here.[B][C]") ]])
fnCutscene([[ Append("Nadia: That's the spirit![B][C]") ]])
fnCutscene([[ Append("Blythe: Hm.[P] I don't know where Earth might be, but if anyone does, it'd be Florentina at the provisions shop.[P] She'd be happy to help you.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Thanks.[P] I feel better already.") ]])
fnCutsceneBlocker()

-- |[Finish Up]|
--Unlock topics.
WD_SetProperty("Unlock Topic", "Alraunes", 1)
WD_SetProperty("Unlock Topic", "Blythe", 1)
WD_SetProperty("Unlock Topic", "Cultists", 1)
WD_SetProperty("Unlock Topic", "Florentina", 1)
WD_SetProperty("Unlock Topic", "Job", 1)
WD_SetProperty("Unlock Topic", "Nadia", 1)
WD_SetProperty("Unlock Topic", "Name", 1)
WD_SetProperty("Unlock Topic", "TradingPost", 1)
WD_SetProperty("Unlock Topic", "Challenge", 1)

--Change the music.
fnCutscene([[ AudioManager_PlayMusic("TownTheme") ]])
fnCutsceneBlocker()
WD_SetProperty("Unlock Topic", "TradingPost", 1)
