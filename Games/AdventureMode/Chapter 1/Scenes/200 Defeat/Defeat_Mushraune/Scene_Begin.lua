-- |[ ==================================== Defeat Mushraune ==================================== ]|
--Most of the cutscene is handled by a specific map trigger, so this script just kicks the player
-- to that map if nothing is preventing them from replaying the scene.

-- |[Repeat Check]|
--If this form has already been accounted for, run the standard game over.
local iHasJob_Agarist = VM_GetVar("Root/Variables/Global/Florentina/iHasJob_Agarist", "N")
if(iHasJob_Agarist == 1.0) then
	LM_ExecuteScript(gsStandardGameOver)
	return
end

-- |[Map Transition]|
--Transit to the map in a spot that fires the needed trigger.

-- |[Restore Party]|
--Restore party to full HP in case the player doesn't want to revisit a save point.
AdvCombat_SetProperty("Restore Party")

-- |[ ===================================== Map Transition ===================================== ]|
--If we're not on the correct map, switch maps and re-run this script.

-- |[Map Check]|
--Make sure we're in the cutscene map.
local sCurrentLevelName = AL_GetProperty("Name")
if(sCurrentLevelName ~= "StForasF") then

	--If not doing a relive, run the "revert" cutscene. This will show the player party hitting
    -- the ground and switching back to human form, if applicable.
	if(iIsRelivingScene == 0.0) then
        LM_ExecuteScript(gsStandardRevert)
	end

	--Transfer to correct map.
	local sString = "AL_BeginTransitionTo(\"StForasF\", \"" .. LM_GetCallStack(0) .. "\")"
	fnCutscene(sString)
	return
end

-- |[ ======================================= Scene Setup ====================================== ]|
-- |[Variables]|
VM_SetVar("Root/Variables/Global/Mei/iMeiKnowsFlorentinaTFs", "N", 1.0)

-- |[Image Streaming]|
-- |[Form]|
--Change the party lead back to human. Well, "Human".
LM_ExecuteScript(gsRoot .. "FormHandlers/Florentina/Job_Merchant.lua")

-- |[Combat]|
--Restore party to full HP in case the player doesn't want to revisit a save point.
AdvCombat_SetProperty("Restore Party")

-- |[Topics]|
--Unlock these topics if they weren't already.
--WD_SetProperty("Unlock Topic", "Alraunes", 1)

-- |[Music]|
AL_SetProperty("Music", "Null")

-- |[Flags]|
--Mark this form as being unlocked.
VM_SetVar("Root/Variables/Global/Florentina/iHasJob_Agarist", "N", 1.0)

-- |[Spawn NPCs]|
fnStandardNPCByPosition("MushrauneGuard")
fnStandardNPCByPosition("MushrauneGoon")

-- |[Remove NPCs]|
EM_PushEntity("EnemyAA")
    RE_SetDestruct(true)
DL_PopActiveObject()
EM_PushEntity("EnemyAB")
    RE_SetDestruct(true)
DL_PopActiveObject()
EM_PushEntity("EnemyHA")
    RE_SetDestruct(true)
DL_PopActiveObject()
EM_PushEntity("EnemyHB")
    RE_SetDestruct(true)
DL_PopActiveObject()
EM_PushEntity("EnemyHC")
    RE_SetDestruct(true)
DL_PopActiveObject()

--Mushbud!
AL_SetProperty("Set Layer Disabled", "Mushbud", false)

-- |[ ==================================== Scene Execution ===================================== ]|
-- |[Fading]|
--Snap to blackness.
fnCutsceneFadeOut()
fnCutsceneBlocker()

-- |[Position]|
fnCutsceneCamera("Position", gcfCamera_Cutscene_Speed, 32.25, 25.50)
fnCutsceneTeleport("Mei", 48.25, 10.50)
fnCutsceneTeleport("Florentina", 32.25, 24.50)
fnCutsceneFace("Florentina", 0, 1)
fnCutsceneBlocker()
fnCutsceneSetFrame("Mei", "Wounded")

-- |[Fading]|
fnCutsceneFadeIn(165)
fnCutsceneBlocker()
fnCutsceneWait(165)
fnCutsceneBlocker()

-- |[Movement]|
fnCutsceneMove("Florentina", 33.25, 25.50)
fnCutsceneFace("Florentina", 0, 1)
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneMove("Florentina", 31.25, 25.50)
fnCutsceneFace("Florentina", 0, 1)
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneMove("Florentina", 32.25, 25.50)
fnCutsceneFace("Florentina", 0, 1)
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneMove("Florentina", 32.25, 24.50)
fnCutsceneFace("Florentina", 0, -1)
fnCutsceneWait(45)
fnCutsceneBlocker()

-- |[Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Florentina", "Surprise") ]])
fnCutscene([[ Append("Florentina:[E|Surprise] Completely solid.[P] Rusty, but without any tools I'm not breaking out.[P] Figures that a religious convent would have a dungeon.[P] Psychos.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] Mei?[P] Hello?[P] Mei?[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] ...[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] Didn't see what happened to her, but I bet she's working hard on getting me out right now.[P] Naturally.[P] So I gotta one-up her by breaking out before she shows up.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

-- |[Movement]|
fnCutsceneMove("Florentina", 31.25, 25.50)
fnCutsceneFace("Florentina", -1, 1)

-- |[Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("Florentina:[VOICE|Florentina] Hey, you![P] Guard![P] You think you could let me out of here?[P] I'm ever so sorry and won't do it again!") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("MushrauneGuard", 1, 0)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("Mushraune:[VOICE|Alraune] Dissonant voices will be calmed.[P] Your time will come soon, and when it does, you will be eager to serve.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("MushrauneGuard", 0, 1)

-- |[Movement]|
fnCutsceneMove("Florentina", 32.25, 24.50)
fnCutsceneFace("Florentina", 1, 0)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

-- |[Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Florentina", "Neutral") ]])
fnCutscene([[ Append("Florentina:[E|Confused] Horny religious freaks, the worst kind.[P] That moves my timetable up.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] ...[P] Huh?") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("Florentina", -1, 0)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("Florentina", 0, -1)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("Florentina", 1, 1)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("Florentina", -1, 1)
fnCutsceneWait(25)
fnCutsceneBlocker()

-- |[Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Florentina", "Neutral") ]])
fnCutscene([[ Append("Florentina:[E|Confused] Who's there?[P] I heard you.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] (You did?[P] It's working![P] Ha ha!)[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] What are you, a voice inside my head?[P] Oh no, am I hearing the fungus like those freaks?[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] (No, well, yes, but, no.[P] Not quite the same.[P] That's me, on the ground next to you.)") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("Florentina", -1, 0)
fnCutsceneWait(45)
fnCutsceneBlocker()

-- |[Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Florentina", "Neutral") ]])
fnCutscene([[ Append("Florentina:[E|Surprise] So I [P]*am*[P] hearing the fungus now.[P] Am I infected, too?[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Surprise] (No, I'm merely using the same technique I've observed the plants use.[P] I have spent much time learning your language.[P] I can't believe it's working![P] At last!)[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] Yeah, glorious day.[P] You're stuck in a damp jail cell underground with me.[P] Things are going swimmingly.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] (I am a largely immobile mass of fungus, this is actually the ideal place for me to be stuck.[P] Apart from the jail cell part.)[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] I guess so.[P] So what are you in for?[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] (Same as you.[P] I'm a 'dissonant voice' so they threw me in here until I start playing ball.[P] So to speak.)[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] There are divisions even within the fungus community?[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] (I'm not interested in this unity crap.[P] I joined up with these guys in order to get some damn motility but it turns out they want total assimilation.)[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] (Serves me right for trusting the new guys.[P] Everything was great in this cave until 'The Great Leader' showed up and then once he's done he taffs off.)[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Surprise] Woah, slow down there, skippy.[P] Who's this Great Leader?[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Surprise] (He was a fungus, like me, and he spored up the first alraune in the coven, Mycela.[P] Fed her with lies, and then told us to start abducting more.)[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Surprise] (He went from a guy we could hang out with to cult leader real quick, and now he's gone.[P] Left us all here to stew in the darkness.)[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] Let me guess.[P] He said he'd return one day and everyone is to send money to a PO box somewhere in Sturnheim?[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] (He did say he'd come back but what's a PO box?)[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] Heh, classic con-man stuff.[P] Con-fungus stuff.[P] Congus.[P] He played you like a fiddle.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] (Unless you get out of here pronto, you're going to be the latest bag-holder.[P] What was your name again?)[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] Florentina.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] (Okay Florentina, I don't want to do this but...[P] I overheard you saying you couldn't see a way out.[P] Who's Mei?)[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] A real goody-two-shoes business associate of mine.[P] She's not going to leave me down here, but I also don't know just where she wound up.[P] Might be in jail too.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] (I think I have a way for us to get out of here.)[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Surprise] Oh?[P] Going to involve me doing most of the work, right?[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Surprise] (We mushrooms are more industrious than you think.)[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] So what's your plan?[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] (Let me parasitize you, and we walk out of here.)[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Offended] Your plan sucks.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Offended] (I know it does, but you're getting shroomed either way, if your friend doesn't get here in time.)[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] Can I get un-shroomed later?[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] (I want to see the world.[P] I joined this stupid cult to get a host so I could get around.[P] Can you at least find it within your heart to take me with you?)[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Offended] That's not a yes.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Offended] (A fungus can be extracted from its host with vinegar and baking soda.[P] Our little roots come right out and you can put me in a cave someplace.)[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] (I don't want you to do that, but I'm desperate here.)[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] Heh, first rule of business?[P] Don't let them know if you're desperate.[P] Gives you a weak hand in the negotiations.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] Then again, what do you know of business?[P] Two things, jack and squat.[P] And Jack left town.[P] Stole that joke from a business partner of mine, by the way.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] (So do we have a deal?)[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Blush] I am going to regret this so much but, yeah.[P] What do we do?") ]])
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()

-- |[Black Out]|
fnCutsceneFadeOut(45)
fnCutsceneWait(125)
fnCutsceneBlocker()

-- |[Costume Change]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("Florentina:[VOICE|Florentina] I just put this here?[B][C]") ]])
fnCutscene([[ Append("Florentina:[VOICE|Florentina] (Take your shirt off.)[B][C]") ]])
fnCutscene([[ Append("Florentina:[VOICE|Florentina] Are you serious, dude?[P] Are you a dude?[B][C]") ]])
fnCutscene([[ Append("Florentina:[VOICE|Florentina] (A fungus contains the male and female characteristics necessary for reproduction within itself, I am both and neither male and female.)[B][C]") ]])
fnCutscene([[ Append("Florentina:[VOICE|Florentina] Your pickup lines suck, too.[B][C]") ]])
fnCutscene([[ Append("Florentina:[VOICE|Florentina] (Now take the cap and put it -[P] don't rip it!)[B][C]") ]])
fnCutscene([[ Append("Florentina:[VOICE|Florentina] I ripped it.[B][C]") ]])
fnCutscene([[ Append("Florentina:[VOICE|Florentina] (That'll take weeks to regrow.[P] I'll be pulling the nutrition out of your body!)[B][C]") ]])
fnCutscene([[ Append("Florentina:[VOICE|Florentina] Don't be a wuss, I get into a lot of fights and you're going to be scuffed.[P] Besides, I eat sunlight.[P] Lots to go around.[B][C]") ]])
fnCutscene([[ Append("Florentina:[VOICE|Florentina] (Lastly, let me grow around your legs.[P] This will take a second.)[B][C]") ]])
fnCutscene([[ Append("Florentina:[VOICE|Florentina] Ow![P] Ow![P] That pinches![B][C]") ]])
fnCutscene([[ Append("Florentina:[VOICE|Florentina] (All done.[P] Now we're host and symbiotic fungus.[P] How do you feel?)[B][C]") ]])
fnCutscene([[ Append("Florentina:[VOICE|Florentina] ...[P] Miserable.[B][C]") ]])
fnCutscene([[ Append("Florentina:[VOICE|Florentina] (Is that normal?)[B][C]") ]])
fnCutscene([[ Append("Florentina:[VOICE|Florentina] I haven't been this kind of intimate in a long time.[P] Really bringing back memories.[P] Okay, let's get out of here.[P] Hey![P] Guard!") ]])
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()

--Change costume.
fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Florentina/Job_Agarist.lua") ]])
fnCutsceneBlocker()

-- |[Movement]|
fnCutsceneCamera("Position", 1000, 47.25, 11.50)
fnCutsceneBlocker()
fnCutsceneTeleport("Florentina", 52.25, 24.50)
fnCutsceneFadeIn(45)
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneMove("Florentina", 52.25, 14.50)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("Florentina", 1, 0)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("Florentina:[VOICE|Florentina] (Other way.[P] That room over there.)") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneMove("Florentina", 48.25, 14.50)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("MushrauneGoon", 1, 0)
fnCutsceneWait(25)
fnCutsceneBlocker()

-- |[Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("Florentina:[VOICE|Florentina] Hey, you.[P] Fungus-friend.[B][C]") ]])
fnCutscene([[ Append("Mushraune:[VOICE|Alraune] Ah, you have joined us.[P] Good.[B][C]") ]])
fnCutscene([[ Append("Florentina:[VOICE|Florentina] Yep.[B][C]") ]])
fnCutscene([[ Append("Mushraune:[VOICE|Alraune] How is the misery?[P] The despair?[B][C]") ]])
fnCutscene([[ Append("Florentina:[VOICE|Florentina] Deliciously depressing.[P] I'm starting to enjoy it.[P] Anyway, I'm here to help this one see the light.[P] Or the darkness.[B][C]") ]])
fnCutscene([[ Append("Mushraune:[VOICE|Alraune] Good.[P] Keep your wits about you, she is strong.[P] She required six to subdue earlier.[B][C]") ]])
fnCutscene([[ Append("Florentina:[VOICE|Florentina] I know her weaknesses, I can handle her.[P] My master makes me strong.[B][C]") ]])
fnCutscene([[ Append("Mushraune:[VOICE|Alraune] Do not fail us.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

-- |[Movement]|
fnCutsceneMove("MushrauneGoon", 47.25, 15.50)
fnCutsceneBlocker()
fnCutsceneFace("Florentina", 0, 1)
fnCutsceneMove("MushrauneGoon", 52.25, 15.50)
fnCutsceneBlocker()
fnCutsceneFace("Florentina", 1, 0)
fnCutsceneMove("MushrauneGoon", 52.25, 24.50)
fnCutsceneBlocker()

-- |[Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Florentina", "Neutral") ]])
fnCutscene([[ Append("Florentina:[E|Surprise] Hey, it worked.[P] This disguise is pretty good.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Surprise] ('My master makes me strong?')[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] Shut up, it worked, didn't it?[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] (Because it's not a disguise![P] And slow down, you're making me sick.[P] I'm not used to such constant swaying and jostling.)[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] No time for you to get your seaweed-legs, buddy.[P] Mei's right over there.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

-- |[Movement]|
fnCutsceneMove("Florentina", 47.25, 14.50)
fnCutsceneMove("Florentina", 47.25, 10.50)
fnCutsceneFace("Florentina", 1, 0)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

-- |[Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("Florentina:[VOICE|Florentina] Mei![P] Hey, Mei![P] Wake up![B][C]") ]])
fnCutscene([[ Append("Mei:[VOICE|Mei] Mggh...[B][C]") ]])
fnCutscene([[ Append("Florentina:[VOICE|Florentina] She's pretty beat up.[P] Guess we're carrying her out of here.[B][C]") ]])
fnCutscene([[ Append("Florentina:[VOICE|Florentina] (Time for me to do my thing.[P] Get ready, I'll really give you my strength!)[B][C]") ]])
fnCutscene([[ Append("Florentina:[VOICE|Florentina] Woah![P] My plant muscles are bulging.[P] I got you, Mei!") ]])
fnCutsceneBlocker()
fnCutsceneWait(155)
fnCutsceneFadeOut(125)
fnCutsceneBlocker()

-- |[Outside]|
fnCutscene([[ AL_BeginTransitionTo("StForasA", "FORCEPOS:1.0x1.0x0") ]])

-- |[ ======================================== Clean Up ======================================== ]|
-- |[Unload Images]|
--fnCutscene([[ fnUnloadBitmapsFromList("Chapter 1 Mei Wisphag TF") ]])

-- |[Transition]|
--Last save point.
--fnCutscene([[ AL_BeginTransitionTo("LASTSAVE", "Null") ]])
--fnCutsceneBlocker()