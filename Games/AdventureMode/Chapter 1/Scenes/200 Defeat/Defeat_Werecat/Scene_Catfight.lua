-- |[Catfight Scene]|
--You are not fit to join my pride!

-- |[Flag]|
--Set this.
VM_SetVar("Root/Variables/Chapter1/ScenesWerecat/iSpawnNadiaEvermoonW", "N", 1.0)

--Variables.
local iHasFlorentina = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")

-- |[Movement]|
--Move Mei and the werecats up.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (51.25 * gciSizePerTile), (39.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatA")
	ActorEvent_SetProperty("Move To", (50.25 * gciSizePerTile), (38.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatB")
	ActorEvent_SetProperty("Move To", (52.25 * gciSizePerTile), (38.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face",  0, -1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatA")
	ActorEvent_SetProperty("Face",  0, -1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatB")
	ActorEvent_SetProperty("Face",  0, -1)
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--Werecats look back.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatA")
	ActorEvent_SetProperty("Face",  0, 1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatB")
	ActorEvent_SetProperty("Face",  0, 1)
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Werecat", "Neutral") ]])

--Dialogue.
fnCutscene([[ Append("Mei:[E|Neutral] (The hunt...[P] Nadia...)[B][C]") ]])
fnCutscene([[ Append("Werecat: The kinfang hesitates.[P] It will be an easy kill.[P] Perhaps we should let the plant girl run first?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] No.[B][C]") ]])
fnCutscene([[ Append("Werecat: Does the new kinfang not want the kill?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] There will be no hunt here tonight![B][C]") ]])
fnCutscene([[ Append("Werecat: Kinfang must hunt, must kill, or kinfang is weak![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] Not weak, strong![B][C]") ]])
fnCutscene([[ Append("Werecat: Hss![P] Weak![P] The new one is weak![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Angry] Wrong again!") ]])
fnCutsceneBlocker()

fnCutsceneWait(15)
fnCutsceneBlocker()

-- |[Movement]|
--Left Werecat moves onto Mei. Get thrown into the wall.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatA")
	ActorEvent_SetProperty("Move To", (51.00 * gciSizePerTile), (39.25 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Mei rotates in place to emulate a throw.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face",  1, 1)
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face",  1, 0)
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face",  1, -1)
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face",  0, -1)
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()

--Sound effect plays on this frame.
fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face",  -1, -1)
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()

--Werecat switches to wounded frame and flies into the wall.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatA")
	ActorEvent_SetProperty("Special Frame", "Wounded")
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatA")
	ActorEvent_SetProperty("Move To", (50.25 * gciSizePerTile), (36.00 * gciSizePerTile), 2.5)
DL_PopActiveObject()
fnCutsceneBlocker()

--Ricochet.
fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatA")
	ActorEvent_SetProperty("Move To", (49.25 * gciSizePerTile), (37.50 * gciSizePerTile), 0.25)
DL_PopActiveObject()
fnCutsceneBlocker()

--Nadia gets up and walks outside.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nadia")
	ActorEvent_SetProperty("Special Frame", "Null")
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nadia")
	ActorEvent_SetProperty("Teleport To", (48.25 * gciSizePerTile), (30.00 * gciSizePerTile))
DL_PopActiveObject()

--If Florentina is present, she wakes up too.
if(iHasFlorentina == 1.0) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Special Frame", "Null")
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Teleport To", (53.25 * gciSizePerTile), (26.00 * gciSizePerTile))
	DL_PopActiveObject()
end
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(35)
fnCutsceneBlocker()

--Nadia starts moving outside.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nadia")
	ActorEvent_SetProperty("Move To", (48.25 * gciSizePerTile), (32.50 * gciSizePerTile), 2.0)
DL_PopActiveObject()
if(iHasFlorentina == 1.0) then
	fnCutscene([[ AL_SetProperty("Open Door", "DoorN") ]])
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (53.25 * gciSizePerTile), (31.50 * gciSizePerTile), 2.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (51.25 * gciSizePerTile), (31.50 * gciSizePerTile), 2.0)
	DL_PopActiveObject()
end
fnCutsceneBlocker()

--Facing. The werecats look north as the door opens.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face",  0, -1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatB")
	ActorEvent_SetProperty("Face",  0, -1)
DL_PopActiveObject()
fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
fnCutscene([[ AL_SetProperty("Open Door", "DoorW") ]])
fnCutsceneWait(5)
fnCutsceneBlocker()

--Keep moving Nadia outside.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nadia")
	ActorEvent_SetProperty("Move To", (48.25 * gciSizePerTile), (34.50 * gciSizePerTile), 2.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nadia")
	ActorEvent_SetProperty("Move To", (51.25 * gciSizePerTile), (34.50 * gciSizePerTile), 2.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nadia")
	ActorEvent_SetProperty("Move To", (51.25 * gciSizePerTile), (35.50 * gciSizePerTile), 2.0)
DL_PopActiveObject()
if(iHasFlorentina == 1.0) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (51.25 * gciSizePerTile), (34.50 * gciSizePerTile), 2.0)
	DL_PopActiveObject()
end

fnCutsceneBlocker()
fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
fnCutscene([[ AL_SetProperty("Open Door", "DoorS") ]])
fnCutsceneWait(5)
fnCutsceneBlocker()

--Nadia walks outside.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nadia")
	ActorEvent_SetProperty("Move To", (51.25 * gciSizePerTile), (37.50 * gciSizePerTile), 2.0)
DL_PopActiveObject()
if(iHasFlorentina == 1.0) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Move To", (50.75 * gciSizePerTile), (37.50 * gciSizePerTile), 2.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (51.25 * gciSizePerTile), (37.50 * gciSizePerTile), 2.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (51.75 * gciSizePerTile), (37.50 * gciSizePerTile), 2.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Face",  0, 1)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face",  0, 1)
	DL_PopActiveObject()
end
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(35)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Nadia", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Offended") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Werecat", "Neutral") ]])
if(iHasFlorentina == 1.0) then
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Florentina", "Surprise") ]])
end

--Dialogue.
fnCutscene([[ Append("Werecat: Hss![P] The prey awakens![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] Fool kinfang![P] She is not prey![B][C]") ]])
fnCutscene([[ Append("Werecat: You would defy the moonlight!?") ]])
fnCutsceneBlocker()

fnCutsceneWait(15)
fnCutsceneBlocker()

-- |[Movement]|
--Mei attacks the other werecat.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatB")
	ActorEvent_SetProperty("Face",  0, 1)
DL_PopActiveObject()
fnCutsceneWait(15)
fnCutsceneBlocker()

--Mei moves onto the werecat.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (52.25 * gciSizePerTile), (38.50 * gciSizePerTile), 2.0)
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneWait(10)
fnCutsceneBlocker()

--Sound effect, Werecat goes down.
fnCutsceneWait(15)
fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatB")
	ActorEvent_SetProperty("Special Frame", "Wounded")
DL_PopActiveObject()
fnCutsceneWait(20)
fnCutsceneBlocker()

--Mei throws the werecat.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face",  1, 0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatB")
	ActorEvent_SetProperty("Move To", (56.25 * gciSizePerTile), (38.50 * gciSizePerTile), 2.5)
DL_PopActiveObject()
fnCutsceneBlocker()

--Ricochet.
fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatB")
	ActorEvent_SetProperty("Move To", (55.25 * gciSizePerTile), (39.00 * gciSizePerTile), 0.25)
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Nadia", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Offended") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Werecat", "Neutral") ]])
if(iHasFlorentina == 1.0) then
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Florentina", "Happy") ]])
end

--Dialogue.
fnCutscene([[ Append("Mei:[E|Angry] You grow fat from hunting weak prey![P] There are more deserving hunts to be had![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Angry] The idle rich, the wicked, the cruel![P] We do not attack a sleeping one, and we do not hunt with more fangs than preys![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Angry] You are not fit to join my pride![B][C]") ]])
fnCutscene([[ Append("Werecat: Hssss!") ]])
fnCutsceneBlocker()

fnCutsceneWait(35)
fnCutsceneBlocker()

-- |[Movement]|
--WerecatA stands up.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatA")
	ActorEvent_SetProperty("Special Frame", "Null")
DL_PopActiveObject()
fnCutsceneWait(35)
fnCutsceneBlocker()

--WerecatB stands up.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatB")
	ActorEvent_SetProperty("Special Frame", "Null")
DL_PopActiveObject()
fnCutsceneWait(35)
fnCutsceneBlocker()

--Werecats run off.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face",  0, 1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatA")
	ActorEvent_SetProperty("Move To", (49.25 * gciSizePerTile), (52.50 * gciSizePerTile), 2.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatB")
	ActorEvent_SetProperty("Move To", (53.25 * gciSizePerTile), (42.50 * gciSizePerTile), 2.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "WerecatB")
	ActorEvent_SetProperty("Move To", (53.25 * gciSizePerTile), (52.50 * gciSizePerTile), 2.0)
DL_PopActiveObject()
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face",  -1, -1)
DL_PopActiveObject()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Nadia", "Neutral") ]])
if(iHasFlorentina == 0.0) then
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
else
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Happy") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
end

--Dialogue if Florentina is not present.
if(iHasFlorentina == 0.0) then
	fnCutscene([[ Append("Nadia: Mei![P] What the hay is going on here?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Apologies, Nadia.[P] My kinfangs thought you were prey.[B][C]") ]])
	fnCutscene([[ Append("Nadia: You know, if you had told me you were cursed I could have tried to cure you.[P] Now it's too late.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] And leave the moonlight's grace?[P] No thank you.[P] I prefer this.[B][C]") ]])

	--Variables. Determine if Nadia knows Mei is a shapeshifter.
	local iHasSeenTrannadarThirdScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarThirdScene", "N")

	--If Nadia has seen the third scene, she knows Mei is a shapeshifter.
	if(iHasSeenTrannadarThirdScene == 1.0) then
		fnCutscene([[ Append("Mei:[E|Happy] Besides, I can transform if I so choose.[B][C]") ]])
		fnCutscene([[ Append("Nadia: Oh yeah![P] Cool![B][C]") ]])
		fnCutscene([[ Append("Nadia: I almost forgot about that.[P] How does that work?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] My runestone, I think.[P] All I need is to concentrate...[B][C]") ]])
		fnCutscene([[ Append("Nadia: I'm not much of a magic expert unless you need help with your garden.[B][C]") ]])
		fnCutscene([[ Append("Nadia: Oh, I know![P] I know where a grove of catnip is![P] Want some?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] Don't patronize me![B][C]") ]])
		fnCutscene([[ Append("Nadia: It's a joke![P] A joke![P] Ack![B][C]") ]])

	--Nadia does not know Mei is a shapeshifter.
	else
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarThirdScene", "N", 1.0)
		fnCutscene([[ Append("Mei:[E|Happy] I would not have wanted to stop you by force.[B][C]") ]])
		fnCutscene([[ Append("Nadia: Well if you're happy, that's cool.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] ...[P] and...[P] my instinct is that I may return to human form at any time.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] This runestone...[P] yes...[B][C]") ]])
		fnCutscene([[ Append("Nadia: Huh?[P] Let me see that![B][C]") ]])
		fnCutscene([[ Append("Nadia: ...[B][C]") ]])
		fnCutscene([[ Append("Nadia: Well, I can feel the magic but it's not Alraune magic, so I don't know how it works.[P] You say it lets you transform?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] Purr...[P] a demonstration is in order?[B][C]") ]])
		fnCutscene([[ Append("Nadia: Purr-haps![P] Ha ha![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] ...[P] Just for that I'm not showing you.[B][C]") ]])
		fnCutscene([[ Append("Nadia: Aww, c'mon![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Nope.[P] Later.[P] Heh.[B][C]") ]])
		fnCutscene([[ Append("Nadia: Aw, jeez![B][C]") ]])
	end

	--Resumption.
	fnCutscene([[ Append("Mei:[E|Neutral] Anyway, I'm not tired anymore.[P] The sun should be up soon.[B][C]") ]])
	fnCutscene([[ Append("Nadia: Want to move out?[P] I just need to grab my bag.[B][C]") ]])

	--If Mei had sex with Nadia:
	local iMeiHasDoneNadia = VM_GetVar("Root/Variables/Chapter1/ScenesWerecat/iMeiHasDoneNadia", "N")
	if(iMeiHasDoneNadia == 1.0) then
		fnCutscene([[ Append("Mei:[E|Neutral] Perhaps we ought to linger -[P] in your bedroom.[B][C]") ]])
		fnCutscene([[ Append("Nadia: ..![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Ooh, the look on your face...[P] you want - [P][CLEAR]") ]])
		fnCutscene([[ Append("Nadia: Sh-[P]shush![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I see how it is.[P] Later, then.[P][CLEAR]") ]])
		fnCutscene([[ Append("Nadia: ...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Then get your things and let's go...[P][P] lover...[B][C]") ]])
		fnCutscene([[ Append("Nadia: ...[P][P] lover...") ]])

	--Normal case:
	else
		fnCutscene([[ Append("Mei:[E|Neutral] Let's get going then.[P] Probably best to scram before my kinfangs come back.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I hope they'll learn a lesson.[P] If not, I can teach them one again![B][C]") ]])
		fnCutscene([[ Append("Nadia: Okay![P] Let's roll!") ]])

	end
	fnCutsceneBlocker()

--Dialogue is Florentina is present:
else
	fnCutscene([[ Append("Nadia: Mei![P] What the hay is going on here?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] The best beatdown I've seen in a while![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] Why didn't you do that when they attacked us yesterday?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Maybe I wasn't motivated enough.[P][EMOTION|Mei|Smirk] But now?[P] I love this![B][C]") ]])
	fnCutscene([[ Append("Nadia: Well, if you had told me you were cursed, I could have tried to cure you.[P] It's too late now![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Smirk] Too late?[P] Purr...[P] You'd have kept me from the moon's grace?[P] No thank you, keep your cures.[B][C]") ]])

	--Variables. Determine if Nadia knows Mei is a shapeshifter.
	local iHasSeenTrannadarThirdScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarThirdScene", "N")
	local iFlorentinaKnowsAboutRune   = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")

	--If Nadia has seen the third scene, she knows Mei is a shapeshifter.
	if(iHasSeenTrannadarThirdScene == 1.0) then
		fnCutscene([[ Append("Mei:[E|Happy] Besides, I can transform if I so choose.[P] Remember?[B][C]") ]])
		fnCutscene([[ Append("Nadia: Oh yeah![P] Cool![B][C]") ]])
		
		--Florentina doesn't know about Mei's runestone:
		if(iFlorentinaKnowsAboutRune == 0.0) then
			fnCutscene([[ Append("Florentina:[E|Surprise] Excuse me?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Surprise] Purr?[P] Oh, yes, I've not informed you, have I?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] I don't understand it either, but I could show you.[P] Later, at a rest stop.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] It might be dangerous here, if there are any kinfangs nearby.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] You don't have the face of a liar.[P] Then again...[P] cats...[B][C]") ]])
			fnCutscene([[ Append("Nadia: I know you'll find a way to purr-[P]suede her to show you![B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Offended] I WILL HIT YOU.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Smirk] All right, all right![P] Knock it off, you two![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Smirk] We should probably get going.[P] The sun will be up soon, and I'm not tired anymore.[B][C]") ]])
			fnCutscene([[ Append("Nadia: Okay![P] Follow me!") ]])
		
		--Florentina knows about the runestone:
		else
			fnCutscene([[ Append("Florentina:[E|Blush] I hope you elect to stay as a werecat for a while.[P] Maybe we can throw some more cats into trees?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Sad] I wish I didn't have to...[EMOTION|Mei|Neutral] but better to hurt them than suffer weak pridemates.[B][C]") ]])
			fnCutscene([[ Append("Nadia: You're really getting into it, aren't you?[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Happy] This is a big improvement![P] More, please![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Laugh] You have a one-track mind![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Smirk] We may as well get going.[P] The sun should be up soon, and I'm not tired anymore.[B][C]") ]])
			fnCutscene([[ Append("Nadia: Okay![P] Follow me!") ]])
		end

	--Nadia does not know Mei is a shapeshifter.
	else
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarThirdScene", "N", 1.0)
		
		--Dialogue.
		fnCutscene([[ Append("Mei:[E|Happy] Besides, my runestone lets me transform when I so choose.[B][C]") ]])
		
		--Florentina doesn't know about Mei's runestone:
		if(iFlorentinaKnowsAboutRune == 0.0) then
			fnCutscene([[ Append("Florentina:[E|Surprise] It does?[B][C]") ]])
			fnCutscene([[ Append("Nadia: It does?[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Offended] Hey, that's my line![B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] But, you don't look like you're lying.[P] What gives?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] I'll show you when we're in a safe spot.[P] Not here, though, there may be kinfangs nearby.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Nadia, please lead the way.[B][C]") ]])
			fnCutscene([[ Append("Nadia: Okay![P] Follow me!") ]])
		
		--Florentina knows about the runestone:
		else
			fnCutscene([[ Append("Florentina:[E|Blush] I hope you elect to stay as a werecat for a while.[P] Maybe we can throw some more cats into trees?[B][C]") ]])
			fnCutscene([[ Append("Nadia: Wait wait, what?[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] Yeah, I didn't believe it either.[P] But it's true.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] I'd show you, but it leaves me vulnerable and there may be more kinfangs nearby.[P] Perhaps later.[B][C]") ]])
			fnCutscene([[ Append("Nadia: Wow, that's so cool, Mei![P] It's meow-nificent![B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Confused] ...[P] Do you want to claw her eyes out or should I do it?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Offended] Ugh, the cat puns are going to kill me.[P][EMOTION|Mei|Neutral] Could you just take us back to the trading post now?[B][C]") ]])
			fnCutscene([[ Append("Nadia: Okay![P] Follow me!") ]])
		end
	end

end

--Remove the werecats as followers.
fnCutscene([[ AL_SetProperty("Unfollow Actor Name", "WerecatA") ]])
fnCutscene([[ AL_SetProperty("Unfollow Actor Name", "WerecatB") ]])

--Scene relive: Stop here.
local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsRelivingScene", "N")
if(iIsRelivingScene == 1.0) then
	
	--Wait a bit.
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--Return to the last save point and execute the post-script..
	fnCutscene([[ AL_BeginTransitionTo("LASTSAVE", gsStandardReliveEnd) ]])
	fnCutsceneBlocker()

--Normal case:
else

	--Wait a bit.
	fnCutsceneWait(35)
	fnCutsceneBlocker()

	--Warp to Evermoon West.
    fnCutscene([[ AL_BeginTransitionTo("EvermoonW", gsRoot .. "Chapter 1/Scenes/200 Defeat/Defeat_Werecat/Scene_PostTransitionEvermoon.lua") ]])
	fnCutsceneBlocker()
end
