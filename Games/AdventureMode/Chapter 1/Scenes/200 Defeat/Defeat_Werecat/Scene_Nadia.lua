-- |[Nadia Scene]|
--Nadia shows up and offers to help Mei out!

-- |[Variables]|
local iHasFlorentina              = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
local iHasAlrauneForm             = VM_GetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N")
local iHasSeenTrannadarFirstScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N")

-- |[Dialogue]|
--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("Voice:[VOICE|Nadia] Hey![P] Hold up a second!") ]])
fnCutsceneBlocker()

-- |[Movement]|
--Move Nadia to position.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nadia")
	ActorEvent_SetProperty("Teleport To", (51.25 * gciSizePerTile), (55.50 * gciSizePerTile))
DL_PopActiveObject()

--If Florentina exists, reposition her.
if(iHasFlorentina == 1.0) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Teleport To", (50.25 * gciSizePerTile), (55.50 * gciSizePerTile))
	DL_PopActiveObject()
end
fnCutsceneBlocker()

--Mei moves out a bit and Nadia moves north.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (51.25 * gciSizePerTile), (37.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nadia")
	ActorEvent_SetProperty("Move To", (51.25 * gciSizePerTile), (38.50 * gciSizePerTile))
DL_PopActiveObject()
if(iHasFlorentina == 1.0) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (50.25 * gciSizePerTile), (38.50 * gciSizePerTile))
	DL_PopActiveObject()
end
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])

--No Florentina:
if(iHasFlorentina == 0.0) then
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Nadia", "Neutral") ]])

--Yes Florentina:
else
	fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Nadia", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Florentina", "Neutral") ]])

end

--If Florentina is present:
if(iHasFlorentina == 1.0) then
	
	fnCutscene([[ Append("Nadia:[E|Neutral] Mei![P] The little ones told me you got in trouble![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] Nadia![P] Florentina![P] Thank goodness![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] You're quick, aren't you? [P]The cats lost interest in me a few minutes after we got separated.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I've been running for a while...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] I ran into Nadia over here.[P] She volunteered to help look for you.[B][C]") ]])
	fnCutscene([[ Append("Nadia:[E|Neutral] Good thing I was out here on patrol.[P] You all right?[B][C]") ]])
	
	--Variables.
	local bNadiaKnowsShapeshifter = false
	local iHasSeenTrannadarSecondScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarSecondScene", "N")
	local iHasSeenTrannadarThirdScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarThirdScene", "N")
	local sTrannadarFirstSceneForm = VM_GetVar("Root/Variables/Chapter1/Scenes/sTrannadarFirstSceneForm", "S")
	local sTrannadarSecondSceneForm = VM_GetVar("Root/Variables/Chapter1/Scenes/sTrannadarSecondSceneForm", "S")
	
	--Third scene: Flip this flag, no special dialogue.
	if(iHasSeenTrannadarThirdScene == 1.0) then
        bNadiaKnowsShapeshifter = true
	
	--Has seen the second scene, which can only trigger if Scene 1 was "Human" and Scene 2 was "Non-Human".
	elseif(iHasSeenTrannadarSecondScene == 1.0) then
		
		--Set this flag. Nadia knows Mei is a shapeshifter.
		bNadiaKnowsShapeshifter = true
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarThirdScene", "N", 1.0)
		
		if(sTrannadarSecondSceneForm == "Alraune") then
			fnCutscene([[ Append("Nadia:[E|Neutral] You're looking a lot less leafy than last time...[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Huh?[P] Oh, well - [P][CLEAR]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Hah![P] The little ones have already told me.[P] I thought they were joking, but here you are![B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] You've got some magic going on?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Yeah, that's it.[P] I think.[P] Not sure myself.[B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Well as long as you're okay on that front![P] What about the rest of you?[B][C]") ]])
		elseif(sTrannadarSecondSceneForm == "Bee") then
			fnCutscene([[ Append("Nadia:[E|Neutral] You lost your feelers huh?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Huh?[P] Oh, well - [P][CLEAR]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Hah![P] The little ones have already told me.[P] You can shapeshift, huh? Cool![B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] You've got some magic going on?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Yeah, that's it.[P] I think.[P] Not sure myself.[B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Well as long as you're okay on that front![P] What about the rest of you?[B][C]") ]])
		elseif(sTrannadarSecondSceneForm == "Slime") then
			fnCutscene([[ Append("Nadia:[E|Neutral] I see the whole slime thing was temporary for you![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Huh?[P] Oh, well - [P][CLEAR]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Hah![P] The little ones have already told me. You can shapeshift, huh? Cool![B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] You've got some magic going on?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Yeah, that's it.[P] I think.[P] Not sure myself.[B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Well as long as you're okay on that front![P] What about the rest of you?[B][C]") ]])
		elseif(sTrannadarSecondSceneForm == "Ghost") then
			fnCutscene([[ Append("Nadia:[E|Neutral] Did you get tired of being dead?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Huh?[P] Oh, well, you see, I can - [P][CLEAR]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Hah![P] The little ones have already told me.[P] You can shapeshift, huh? Cool![B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] You've got some magic going on?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Yeah, that's it.[P] I think.[P] Not sure myself.[B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Well as long as you're okay on that front![P] What about the rest of you?[B][C]") ]])
		end
	
	--If the first scene was a non-human form:
	elseif(iHasSeenTrannadarSecondScene == 0.0 and sTrannadarFirstSceneForm ~= "Human") then
		
		--Set this flag. Nadia knows Mei is a shapeshifter.
		bNadiaKnowsShapeshifter = true
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarThirdScene", "N", 1.0)
		
		if(sTrannadarFirstSceneForm == "Alraune") then
			fnCutscene([[ Append("Nadia:[E|Neutral] You're looking a lot less leafy than last time...[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Huh?[P] Oh, well - [P][CLEAR]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Hah! The little ones have already told me.[P] I thought they were joking, but here you are![B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] You've got some magic going on?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Yeah, that's it.[P] I think.[P] Not sure myself.[B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Well as long as you're okay on that front![P] What about the rest of you?[B][C]") ]])
		elseif(sTrannadarFirstSceneForm == "Bee") then
			fnCutscene([[ Append("Nadia:[E|Neutral] You lost your feelers huh?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Huh?[P] Oh, well - [P][CLEAR]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Hah! The little ones have already told me.[P] You can shapeshift, huh?[P] Cool![B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] You've got some magic going on?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Yeah, that's it.[P] I think.[P] Not sure myself.[B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Well as long as you're okay on that front![P] What about the rest of you?[B][C]") ]])
		elseif(sTrannadarFirstSceneForm == "Slime") then
			fnCutscene([[ Append("Nadia:[E|Neutral] I see the whole slime thing was temporary for you![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Huh?[P] Oh, well - [P][CLEAR]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Hah! The little ones have already told me.[P] You can shapeshift, huh? Cool![B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] You've got some magic going on?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Yeah, that's it.[P] I think.[P] Not sure myself.[B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Well as long as you're okay on that front![P] What about the rest of you?[B][C]") ]])
		elseif(sTrannadarFirstSceneForm == "Ghost") then
			fnCutscene([[ Append("Nadia:[E|Neutral] Did you get tired of being dead?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Huh?[P] Oh, well, you see, I can - [P][CLEAR]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Hah![P] The little ones have already told me.[P] You can shapeshift, huh? Cool![B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] You've got some magic going on?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Yeah, that's it.[P] I think.[P] Not sure myself.[B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Well as long as you're okay on that front![P] What about the rest of you?[B][C]") ]])
		end
	end
	
	--Resume. This is also what happens if Nadia has only seen Mei as a human.
	fnCutscene([[ Append("Mei:[E|Neutral] I'm a little bruised, but in one piece.[P] I was just thinking I could patch myself up in this cabin.[B][C]") ]])
	fnCutscene([[ Append("Nadia:[E|Neutral] Huh, yeah.[P] This one's abandoned, I think, but it's a pretty defensible position.[P] Or at least that's what the Cap'n would say![B][C]") ]])
	fnCutscene([[ Append("Nadia:[E|Neutral] Ohmygosh, Mei![P] Are you thinking what I'm thinking?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] Oh, please no...[B][C]") ]])
	fnCutscene([[ Append("Nadia:[E|Neutral] SLUMBER PARTY!!![B][C]") ]])
	fnCutscene([[ Append("Nadia:[E|Neutral] That's a thing that us girls do, right?[P] Humans love that![P] I even remember seeing a book on slumber parties once![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] I kinda wish the werecats would come back...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] Oh come on, Florentina.[P] It might be fun![B][C]") ]])
	fnCutscene([[ Append("Nadia:[E|Neutral] SQUEEEEEEEEEEE![B][C]") ]])
	fnCutscene([[ Append("Nadia:[E|Neutral] I promise I'll take you back to the Trading Post first thing in the morning![B][C]") ]])
	fnCutscene([[ Append("Nadia:[E|Neutral] We're gonna have so much fun![P] Pillow fights![P] Scary stories![P] I can't wait![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Actually, uh, I could really just go to bed.[P] Sprinting away from vicious cat creatures really took it out of me.[B][C]") ]])
	fnCutscene([[ Append("Nadia:[E|Neutral] That's okay, too.[P] Here, you can have one of my trail rations for dinner![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] She's not going to want to eat Alraune food, you dummy.[B][C]") ]])
	
	--Determine if Nadia knows Mei can turn into an Alraune.
	local iHasSeenTrannadarFirstScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N")
	
	--Nadia knows Mei is a shapeshifter:
	if(bNadiaKnowsShapeshifter) then
		fnCutscene([[ Append("Nadia:[E|Neutral] You're a shapeshifter, right Mei?[P] Can you eat Alraune food?[B][C]") ]])
		
		--Mei has Alraune form:
		if(iHasAlrauneForm == 1.0) then
			fnCutscene([[ Append("Mei:[E|Neutral] Oh, is that rotted fruit with cinnamon?[P] Delicious![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] *nom*[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Happy] Okay, that was a joy to watch.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Mmmm![P] I -[P] I apparently love the taste of mulch now.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Well, thanks Nadia[P]. I'm bushed.[P] Can we go to bed?[B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Y-[P]you made a pun![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] N-[P]no![P] Not on purpose![B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] It's okay, I'll borrow that one for later![P] Goodnight![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] All right, see you in the morning.[P] *yawn*[B][C]") ]])
		
		--Mei does not have Alraune form:
		else
			fnCutscene([[ Append("Mei:[E|Neutral] Rotting mulch...?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] That's...[P] the exact opposite of appetizing.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Happy] I'll take it if you don't want it.[B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] So that's a no?[P] Okay, here ya go, Florry![B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Offended] Call me that again and I'll cut you.[B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Okay, Florry![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] *yawn*[P] Can you two fight it out tomorrow?[B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Okay![P] See you in the morning![B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Offended] This isn't over...[B][C]") ]])
		end
	
	--Nadia does not know Mei is a shapeshifter:
	else
	
		--Human case, no Alraune TF:
		if(iHasAlrauneForm == 0.0) then
			fnCutscene([[ Append("Mei:[E|Neutral] It smells...[P] kinda bad...[B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] It does?[P] Oh, oops.[P] We Alraunes eat compost.[P] I forgot you humans don't like that.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] You know, I'm absolutely not hungry anymore.[P] I think I'll just go to bed...[B][C]") ]])
	
		--Alraune case:
		else
			fnCutscene([[ Append("Mei:[E|Neutral] [P]*sniff*[P] Rotting fruit, mulch, and...[P] a little cinnamon?[P] Smells delicious...[B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Oh, crud.[P] This is Alraune food, not human food.[P] Sorry![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] I wasn't kidding.[P] Here, let me have some.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] *nom*...[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Mmmm.[P] Squishy...[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Happy] Well that was a treat to watch.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] I'm -[P] hmm.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] (I guess the little ones haven't told Nadia yet.[P] Maybe I'll tell her later.)[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] All right, bed time.[P] See you in the morning...[B][C]") ]])
		end
	end
	
	--Common.
	fnCutscene([[ Append("Florentina:[E|Neutral] There's only two beds.[P] Do you two want to share a room?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Well I'd be okay with it if Nadia is...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Oh, you seem to think that was a choice.[P][EMOTION|Florentina|Happy] You two are sharing a room, I was just wondering if you [P]*wanted* [P] to.[B][C]") ]])
	fnCutscene([[ Append("Nadia: Ooh![P] Fun![P] It won't be a problem, right?[P] We're all girls here![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I'm too tired to argue...") ]])

	--Common.
	fnCutsceneBlocker()
	
--If Mei has not met Nadia before:
elseif(iHasSeenTrannadarFirstScene == 0.0) then
	VM_SetVar("Root/Variables/Chapter1/ScenesWerecat/iMetNadiaInWerecatScene", "N", 1.0)
	fnCutscene([[ Append("Alraune:[E|Neutral] Hey there, stranger![P] I heard a commotion, is everything all right?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Oh dear...[B][C]") ]])
	fnCutscene([[ Append("Alraune:[E|Neutral] Huh?[P] Oh, you can relax![P] I'm one of the good guys![B][C]") ]])
	fnCutscene([[ Append("Nadia:[E|Neutral] Name's Nadia.[P] I work for the Trading Post.[P] Are you in trouble?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I got into a scrap with some cat creatures, but I got away.[P] I'm fine, just a little tired.[B][C]") ]])
	fnCutscene([[ Append("Nadia:[E|Neutral] You ought to be careful if you're going to be out here alone.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I'm not alone by choice...[B][C]") ]])
	fnCutscene([[ Append("Nadia:[E|Neutral] You're also not alone![P] Come on, I'll lead you to the Trading Post.[P] You'll be safe there.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I'm...[P] really tired...[B][C]") ]])
	fnCutscene([[ Append("Nadia:[E|Neutral] Oh crud, am I screwing this up?[P] I'm trying to be nice![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] No, you're doing great.[P] I just really am tired.[P] I was going to check out this cabin, see if it's a safe spot to sleep.[B][C]") ]])
	fnCutscene([[ Append("Nadia:[E|Neutral] This one?[P] It's abandoned, but it should be pretty secure.[P] I think the last owner headed off to Sturnheim.[B][C]") ]])
	fnCutscene([[ Append("Nadia:[E|Neutral] Tell you what, I'll stay here with you and I can lead you to the Trading Post in the morning.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] (Well, she seems nice enough.[P] I don't think I have the strength to argue anyway...)[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Sure, okay.[P] I'm really bushed, and it's late.[P] I've had quite a day.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] [P]*yawn*[P] Hey, you wouldn't know what this rune means, would you?[B][C]") ]])
	fnCutscene([[ Append("Nadia:[E|Neutral] Mm, no.[P] Never seen that before.[P] But, you know who would?[P] My old pal, Florentina.[P] She runs the general goods store at the Trading Post.[B][C]") ]])
	fnCutscene([[ Append("Nadia:[E|Neutral] She or Cap'n Blythe can probably help you.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I guess I'll ask them, then.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] My name's Mei, by the way.[P] Pleased to meet you.[P] Thanks for your help.[B][C]") ]])
	fnCutscene([[ Append("Nadia:[E|Neutral] No problem![P] Here, you can have one of my trail rations for dinner![B][C]") ]])
	
	--Human case, no Alraune TF:
	if(iHasAlrauneForm == 0.0) then
		fnCutscene([[ Append("Mei:[E|Neutral] It smells...[P] kinda bad...[B][C]") ]])
		fnCutscene([[ Append("Nadia:[E|Neutral] It does?[P] Oh, oops.[P] We Alraunes eat compost.[P] I forgot you humans don't like that.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] You know, I'm absolutely not hungry anymore.[P] I think I'll just go to bed...") ]])

	--Alraune case:
	else
		fnCutscene([[ Append("Mei:[E|Neutral] [P]*sniff*[P] rotting fruit, mulch, and...[P] a little cinnamon?[P] Smells delicious...[B][C]") ]])
		fnCutscene([[ Append("Nadia:[E|Neutral] Oh, crud.[P] This is Alraune food, not human food.[P] Sorry![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I wasn't kidding.[P] Here, let me have some.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] *nom*...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Mmmm.[P] Squishy...[B][C]") ]])
		fnCutscene([[ Append("Nadia:[E|Neutral] I've never met a human who liked Alraune food![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I'm -[P] hmm.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] (Might be best not to reveal certain facts to random strangers, even if she is a leaf-sister...)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] All right, bed time.[P] See you in the morning...") ]])
	end
	fnCutsceneBlocker()

--If Mei has met Nadia before:
elseif(iHasSeenTrannadarFirstScene == 1.0) then
	fnCutscene([[ Append("Nadia:[E|Neutral] Hey there, Mei![P] I heard a commotion, is everything all right?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Nadia![P] Oh, thank goodness![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I got into a fight with some cat creatures and barely escaped.[B][C]") ]])
	fnCutscene([[ Append("Nadia:[E|Neutral] Really?[P] Wow, you sure are a magnet for trouble.[B][C]") ]])
	fnCutscene([[ Append("Nadia:[E|Neutral] Good thing I was out here on patrol.[P] You all right?[B][C]") ]])
	
	--Variables.
	local bNadiaKnowsShapeshifter = false
	local iHasSeenTrannadarSecondScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarSecondScene", "N")
	local iHasSeenTrannadarThirdScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarThirdScene", "N")
	local sTrannadarFirstSceneForm = VM_GetVar("Root/Variables/Chapter1/Scenes/sTrannadarFirstSceneForm", "S")
	local sTrannadarSecondSceneForm = VM_GetVar("Root/Variables/Chapter1/Scenes/sTrannadarSecondSceneForm", "S")
	
	--Third scene: Flip this flag.
	if(iHasSeenTrannadarThirdScene == 1.0) then
        bNadiaKnowsShapeshifter = true
	
	--Has seen the second scene, which can only trigger if Scene 1 was "Human" and Scene 2 was "Non-Human".
	elseif(iHasSeenTrannadarSecondScene == 1.0) then
		
		--Set this flag. Nadia knows Mei is a shapeshifter.
		bNadiaKnowsShapeshifter = true
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarThirdScene", "N", 1.0)
		
		if(sTrannadarSecondSceneForm == "Alraune") then
			fnCutscene([[ Append("Nadia:[E|Neutral] You're looking a lot less leafy than last time...[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Huh?[P] Oh, well - [P][CLEAR]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Hah![P] The little ones have already told me.[P] I thought they were joking, but here you are![B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] You've got some magic going on?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Yeah, that's it.[P] I think.[P] Not sure myself.[B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Well as long as you're okay on that front![P] What about the rest of you?[B][C]") ]])
		elseif(sTrannadarSecondSceneForm == "Bee") then
			fnCutscene([[ Append("Nadia:[E|Neutral] You lost your feelers huh?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Huh? Oh, well - [P][CLEAR]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Hah! The little ones have already told me. You can shapeshift, huh? Cool![B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] You've got some magic going on?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Yeah, that's it.[P] I think.[P] Not sure myself.[B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Well as long as you're okay on that front![P] What about the rest of you?[B][C]") ]])
		elseif(sTrannadarSecondSceneForm == "Slime") then
			fnCutscene([[ Append("Nadia:[E|Neutral] I see the whole slime thing was temporary for you![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Huh? Oh, well - [P][CLEAR]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Hah! The little ones have already told me. You can shapeshift, huh? Cool![B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] You've got some magic going on?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Yeah, that's it.[P] I think.[P] Not sure myself.[B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Well as long as you're okay on that front![P] What about the rest of you?[B][C]") ]])
		elseif(sTrannadarSecondSceneForm == "Ghost") then
			fnCutscene([[ Append("Nadia:[E|Neutral] Did you get tired of being dead?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Huh?[P] Oh, well, you see, I can - [P][CLEAR]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Hah![P] The little ones have already told me.[P] You can shapeshift, huh? Cool![B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] You've got some magic going on?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Yeah, that's it.[P] I think.[P] Not sure myself.[B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Well as long as you're okay on that front![P] What about the rest of you?[B][C]") ]])
		end
	
	--If the first scene was a non-human form:
	elseif(iHasSeenTrannadarSecondScene == 0.0 and sTrannadarFirstSceneForm ~= "Human") then
		
		--Set this flag. Nadia knows Mei is a shapeshifter.
		bNadiaKnowsShapeshifter = true
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarThirdScene", "N", 1.0)
		
		if(sTrannadarFirstSceneForm == "Alraune") then
			fnCutscene([[ Append("Nadia:[E|Neutral] You're looking a lot less leafy than last time...[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Huh?[P] Oh, well - [P][CLEAR]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Hah! The little ones have already told me.[P] I thought they were joking, but here you are![B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] You've got some magic going on?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Yeah, that's it.[P] I think.[P] Not sure myself.[B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Well as long as you're okay on that front![P] What about the rest of you?[B][C]") ]])
		elseif(sTrannadarFirstSceneForm == "Bee") then
			fnCutscene([[ Append("Nadia:[E|Neutral] You lost your feelers huh?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Huh?[P] Oh, well - [P][CLEAR]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Hah! The little ones have already told me.[P] You can shapeshift, huh?[P] Cool![B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] You've got some magic going on?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Yeah, that's it.[P] I think.[P] Not sure myself.[B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Well as long as you're okay on that front![P] What about the rest of you?[B][C]") ]])
		elseif(sTrannadarFirstSceneForm == "Slime") then
			fnCutscene([[ Append("Nadia:[E|Neutral] I see the whole slime thing was temporary for you![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Huh?[P] Oh, well - [P][CLEAR]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Hah! The little ones have already told me.[P] You can shapeshift, huh? Cool![B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] You've got some magic going on?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Yeah, that's it.[P] I think.[P] Not sure myself.[B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Well as long as you're okay on that front![P] What about the rest of you?[B][C]") ]])
		elseif(sTrannadarFirstSceneForm == "Ghost") then
			fnCutscene([[ Append("Nadia:[E|Neutral] Did you get tired of being dead?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Huh?[P] Oh, well, you see, I can - [P][CLEAR]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Hah![P] The little ones have already told me.[P] You can shapeshift, huh? Cool![B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] You've got some magic going on?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Yeah, that's it.[P] I think.[P] Not sure myself.[B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Well as long as you're okay on that front![P] What about the rest of you?[B][C]") ]])
		end
	end
	
	--Resume. This is also what happens if Nadia has only seen Mei as a human.
	fnCutscene([[ Append("Mei:[E|Neutral] I'm a little bruised, but in one piece.[P] I was just thinking I could patch myself up in this cabin.[B][C]") ]])
	fnCutscene([[ Append("Nadia:[E|Neutral] Huh, yeah.[P] This one's abandoned, I think, but it's a pretty defensible position.[P] Or at least that's what the Cap'n would say![B][C]") ]])
	fnCutscene([[ Append("Nadia:[E|Neutral] Ohmygosh, Mei![P] Are you thinking what I'm thinking?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] That it's late and I want to go to bed?[B][C]") ]])
	fnCutscene([[ Append("Nadia:[E|Neutral] SLUMBER PARTY!!![B][C]") ]])
	fnCutscene([[ Append("Nadia:[E|Neutral] That's a thing that us girls do, right?[P] Humans love that![P] I even remember seeing a book on slumber parties once![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I guess I wouldn't mind the company.[P] If something happens, it'd be nice to have some backup.[B][C]") ]])
	fnCutscene([[ Append("Nadia:[E|Neutral] SQUEEEEEEEEEEE![B][C]") ]])
	fnCutscene([[ Append("Nadia:[E|Neutral] I promise I'll take you back to the Trading Post first thing in the morning![B][C]") ]])
	fnCutscene([[ Append("Nadia:[E|Neutral] We're gonna have so much fun![P] Pillow fights![P] Scary stories![P] I can't wait![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] No really, Nadia.[P] I just want to eat something and go to bed.[P] I'm...[P] really tired.[P] I guess sprinting away from cat creatures takes it out of you.[B][C]") ]])
	fnCutscene([[ Append("Nadia:[E|Neutral] That's okay, too.[P] Here, you can have one of my trail rations for dinner![B][C]") ]])
	
	--Determine if Nadia knows Mei can turn into an Alraune.
	local iHasSeenTrannadarFirstScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N")
	
	--Nadia knows Mei is a shapeshifter:
	if(bNadiaKnowsShapeshifter) then
		fnCutscene([[ Append("Nadia:[E|Neutral] You're a shapeshifter, right Mei?[P] Can you eat Alraune food?[B][C]") ]])
		
		--Mei has Alraune form:
		if(iHasAlrauneForm == 1.0) then
			fnCutscene([[ Append("Mei:[E|Neutral] Oh, is that rotted fruit with cinnamon?[P] Delicious![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] *nom*[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Mmmm![P] I -[P] I apparently love the taste of mulch now.[B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] It's also really nutritious! [B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Well, thanks Nadia[P]. I'm bushed.[P] Can we go to bed?[B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Y-[P]you made a pun![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] N-[P]no![P] Not on purpose![B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] It's okay, I'll borrow that one for later! Goodnight![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] All right, see you in the morning. *yawn*") ]])
		
		--Mei does not have Alraune form:
		else
			fnCutscene([[ Append("Mei:[E|Neutral] Rotting mulch...?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] That's...[P] the exact opposite of appetizing.[B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] So that's a no?[P] Okay, more for me![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] I'm not hungry anymore.[P] I think I'll just go to bed.[P] Goodnight...[P] *yawn*[B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] See you in the morning!") ]])
		end
	
	--Nadia does not know Mei is a shapeshifter:
	else
	
		--Human case, no Alraune TF:
		if(iHasAlrauneForm == 0.0) then
			fnCutscene([[ Append("Mei:[E|Neutral] It smells...[P] kinda bad...[B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] It does?[P] Oh, oops.[P] We Alraunes eat compost.[P] I forgot you humans don't like that.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] You know, I'm absolutely not hungry anymore.[P] I think I'll just go to bed...") ]])
	
		--Alraune case:
		else
			fnCutscene([[ Append("Mei:[E|Neutral] [P]*sniff*[P] Rotting fruit, mulch, and...[P] a little cinnamon?[P] Smells delicious...[B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] Oh, crud.[P] This is Alraune food, not human food.[P] Sorry![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] I wasn't kidding.[P] Here, let me have some.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] *nom*...[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Mmmm.[P] Squishy...[B][C]") ]])
			fnCutscene([[ Append("Nadia:[E|Neutral] I've never met a human who liked Alraune food![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] I'm -[P] hmm.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] (I guess the little ones haven't told Nadia yet.[P] Maybe I'll tell her later.)[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] All right, bed time.[P] See you in the morning...") ]])
		end
	end

	--Common.
	fnCutsceneBlocker()
end

--Fade to black.
fnCutscene([[ AL_SetProperty("Activate Fade", 90, gci_Fade_Under_GUI, true, gcf_Evening_R, gcf_Evening_G, gcf_Evening_B, gcf_Evening_A, 0, 0, 0, 1, true) ]])
fnCutsceneWait(120)
fnCutsceneBlocker()

-- |[Movement]|
--Reposition the actors. Switch Nadia to sleeping. Where Mei and Florentina go depends on whether or not Florentina was present.
if(iHasFlorentina == 0.0) then

	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Teleport To", (53.25 * gciSizePerTile), (26.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Teleport To", (49.25 * gciSizePerTile), (30.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Special Frame", "Sleep")
	DL_PopActiveObject()
	fnCutsceneBlocker()

--Florentina is present:
else

	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Teleport To", (48.25 * gciSizePerTile), (31.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Teleport To", (49.25 * gciSizePerTile), (30.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Special Frame", "Sleep")
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Teleport To", (54.25 * gciSizePerTile), (26.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Special Frame", "Sleep")
	DL_PopActiveObject()
	fnCutsceneBlocker()


end

--Close the doors.
fnCutscene([[ AL_SetProperty("Close Door", "DoorS") ]])
fnCutscene([[ AL_SetProperty("Close Door", "DoorW") ]])
fnCutscene([[ AL_SetProperty("Close Door", "DoorN") ]])

--Advance to night time.
fnCutscene([[ AL_SetProperty("Activate Fade", 135, gci_Fade_Under_GUI, true, 0, 0, 0, 1, gcf_Night_R, gcf_Night_G, gcf_Night_B, gcf_Night_A, true) ]])
fnCutsceneWait(180)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ Append("Mei:[E|Neutral] (Grr, I'm restless and tired at the same time.)[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] (I can't sleep when it's so bright out![P] Must be a full moon.)") ]])
fnCutsceneBlocker()

-- |[Movement]|
--Walk to the window. Depends on where Mei is.
if(iHasFlorentina == 0.0) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (51.25 * gciSizePerTile), (26.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Face",  0, -1)
	DL_PopActiveObject()
	fnCutsceneBlocker()

--If Mei is in Nadia's room.
else
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (48.25 * gciSizePerTile), (30.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Face",  0, -1)
	DL_PopActiveObject()
	fnCutsceneBlocker()

end

--Wait a bit.
fnCutsceneWait(60)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ Append("Mei:[E|Neutral] (Huh, the moon is behind the clouds...)[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] (...[P] I need to...[P] go outside...[P] see the moon...)") ]])
fnCutsceneBlocker()

--If Florentina is present, set her activation script here. This is in case the player tries to talk to her.
if(iHasFlorentina == 1.0) then
	EM_PushEntity("Florentina")
		TA_SetProperty("Activation Script", gsRoot .. "Chapter 1/Dialogue/Florentina/Werecat_Scene.lua")
	DL_PopActiveObject()
end
