-- |[Post Transition]|
--Scene that plays when the party warps back to Evermoon West.

--Variables.
local iHasFlorentina = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")

--Check.
local iHasSeenTrannadarFirstScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N")
if(iHasSeenTrannadarFirstScene == 0.0) then
	VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N", 1.0)
	VM_SetVar("Root/Variables/Chapter1/Scenes/sTrannadarFirstSceneForm", "S", "Werecat")
end
    
--Execute a rest action.
fnCutscene([[ AM_SetProperty("Execute Rest") ]])
    
-- |[System]|
--Black the screen out.
fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

--Reposition Mei and Nadia.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Teleport To", (15.25 * gciSizePerTile), (46.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nadia")
	ActorEvent_SetProperty("Teleport To", (15.25 * gciSizePerTile), (45.50 * gciSizePerTile))
DL_PopActiveObject()

--If Florentina is present:
if(iHasFlorentina == 1.0) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Teleport To", (15.25 * gciSizePerTile), (47.50 * gciSizePerTile))
	DL_PopActiveObject()
end

--Reposition the camera.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 10000.0)
	CameraEvent_SetProperty("Focus Position", (14.25 * gciSizePerTile), (33.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Fade in.
fnCutsceneWait(45)
fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()

-- |[Movement]|
--Move the two up north.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nadia")
	ActorEvent_SetProperty("Move To", (15.25 * gciSizePerTile), (39.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (15.25 * gciSizePerTile), (39.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nadia")
	ActorEvent_SetProperty("Move To", (16.25 * gciSizePerTile), (38.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (16.25 * gciSizePerTile), (38.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nadia")
	ActorEvent_SetProperty("Move To", (16.25 * gciSizePerTile), (33.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (16.25 * gciSizePerTile), (33.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nadia")
	ActorEvent_SetProperty("Move To", (13.25 * gciSizePerTile), (33.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (14.25 * gciSizePerTile), (33.50 * gciSizePerTile))
DL_PopActiveObject()

--If Florentina is present:
if(iHasFlorentina == 1.0) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (15.25 * gciSizePerTile), (39.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (16.25 * gciSizePerTile), (38.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (16.25 * gciSizePerTile), (33.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (15.25 * gciSizePerTile), (33.50 * gciSizePerTile))
	DL_PopActiveObject()
end
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Reposition the camera.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Focus Actor Name", "Mei")
DL_PopActiveObject()
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Nadia", "Neutral") ]])
if(iHasFlorentina == 0.0) then
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
else
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
end

--Dialogue.
fnCutscene([[ Append("Nadia: So then I said to the bee::[P] Nectar?[P] I hardly even knew her![P] Ha ha ha![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] (Kill me now...)[B][C]") ]])
if(iHasFlorentina == 0.0) then
	fnCutscene([[ Append("Mei:[E|Neutral] I think I can make it from here, Nadia.[P] Thanks a bunch.[B][C]") ]])
else
	fnCutscene([[ Append("Mei:[E|Neutral] I think we can make it from here, Nadia.[P] Thanks a bunch.[B][C]") ]])
end

--If Mei met Nadia in the Werecat scene:
local iMetNadiaInWerecatScene = VM_GetVar("Root/Variables/Chapter1/ScenesWerecat/iMetNadiaInWerecatScene", "N")
if(iMetNadiaInWerecatScene == 1.0) then
	fnCutscene([[ Append("Nadia: You sure?[P] It's just over here.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Positive.[B][C]") ]])
	fnCutscene([[ Append("Nadia: Paws-itive?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Cry] (Argh!)[B][C]") ]])
	fnCutscene([[ Append("Nadia:[EMOTION|Mei|Neutral] Okay then![P] It was great to meet you, Mei![P] Stop by any time![P] I bet the Cap'n and Florentina and everyone will be so stoked to meet you![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Yeah, great to meet you too, Nadia!") ]])

--Mei had met Nadia before.
else
	fnCutscene([[ Append("Nadia: Okay![P] Great seeing you again, Mei![P] Hope everything goes well on your quest![B][C]") ]])
	if(iHasFlorentina == 0.0) then
		fnCutscene([[ Append("Nadia: And -[P] try not to get into any more fights with the forest natives.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] No promises.") ]])
	else
		fnCutscene([[ Append("Nadia: And Florry?[P] Please take care of her, okay?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] You think I wasn't?[P] I'd say she turned out rather well, actually.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] Thanks![P] I think!") ]])
	end
end
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Movement]|
--If Mei had sex with Nadia:
local iMeiHasDoneNadia = VM_GetVar("Root/Variables/Chapter1/ScenesWerecat/iMeiHasDoneNadia", "N")
if(iMeiHasDoneNadia == 1.0) then
	
	-- |[Movement]|
	--Nadia walks away a bit. Slowly.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Move To", (12.75 * gciSizePerTile), (33.50 * gciSizePerTile), 0.10)
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--Mei walks up behind her.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Move To", (12.25 * gciSizePerTile), (33.50 * gciSizePerTile), 0.10)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (12.75 * gciSizePerTile), (33.50 * gciSizePerTile), 0.70)
	DL_PopActiveObject()
	fnCutsceneBlocker()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Face",  1, 0)
	DL_PopActiveObject()
	fnCutsceneBlocker()

	--Wait a bit.
	fnCutsceneWait(30)
	fnCutsceneBlocker()

	-- |[Dialogue]|
	--Setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Nadia", "Neutral") ]])

	--Dialogue.
	fnCutscene([[ Append("Nadia: ..![P] Mei, take your hand - [P][CLEAR]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] About last night...[B][C]") ]])
	fnCutscene([[ Append("Nadia: ...[P][P] talk to me when I'm off my shift...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] Mmm...") ]])
	fnCutsceneBlocker()

	--Wait a bit.
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Nadia walks off.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Move To", (0.25 * gciSizePerTile), (34.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Teleport To", (-100.25 * gciSizePerTile), (-100.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--If Florentina is present, fold the party. Also have a bit of a dialogue.
	if(iHasFlorentina == 1.0) then
		
		--Setup.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])

		--Dialogue.
		fnCutscene([[ Append("Florentina:[E|Surprise] What was that all about?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Surprise] ..![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] I hope you didn't think I wouldn't notice.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Nadia and I...[P][CLEAR]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Oh, I know exactly what happened.[P] You put your hand on her butt, after all.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] You know what?[P] Good for you, kid.[P] The world's a dangerous place.[P] Needs more love in it.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] But if you break her sweet, innocent little heart...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] I'll break your legs...[P] Get it?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Surprise] Jeez, Florentina.[P] Where'd that come from?[P][CLEAR]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] I asked if you got it.[P] Did you?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Absolutely.[P] One-hundred percent.[P][CLEAR]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Good.[P] Back to the mission, then.") ]])
		fnCutsceneBlocker()
		
		--Move Florentina onto Mei.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (12.75 * gciSizePerTile), (33.50 * gciSizePerTile))
		DL_PopActiveObject()
		
		--Instruction to fold the party.
		fnCutscene([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	end
	
--If not, Nadia just walks offscreen and ends the scene.
else
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Move To", (0.25 * gciSizePerTile), (34.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Nadia")
		ActorEvent_SetProperty("Teleport To", (-100.25 * gciSizePerTile), (-100.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--If Florentina is present, fold the party.
	if(iHasFlorentina == 1.0) then
		
		--Move Florentina onto Mei.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (14.25 * gciSizePerTile), (33.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
		--Instruction to fold the party.
		fnCutscene([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	end

end
fnCutsceneBlocker()

--System. Put Florentina back on the follower listing. Remember that this executes immediately and the instructions catch up to it.
if(iHasFlorentina == 1.0) then
    fnAddPartyMember("Florentina")
end
