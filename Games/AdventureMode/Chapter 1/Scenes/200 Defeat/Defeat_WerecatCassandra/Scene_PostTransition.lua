-- |[Scene Post-Transition]|
--After transition, play a random dialogue line.

-- |[Variables]|
local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")

-- |[Wounded]|
--Move Florentina one pixel up so she's behind Mei.
if(bIsFlorentinaPresent and false) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move Amount", 16.0, 0.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move Amount", 0.0, 16.0)
	DL_PopActiveObject()
	fnCutsceneBlocker()
end

-- |[Fade In]|
fnCutscene([[ AL_SetProperty("Activate Fade", 180, gci_Fade_Over_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])

--Change Mei to the "Downed" frames.
Cutscene_CreateEvent("Change Mei Special Frame", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
    ActorEvent_SetProperty("Special Frame", "Wounded")
DL_PopActiveObject()

--If Florentina is present, set her to downed as well.
if(bIsFlorentinaPresent) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Special Frame", "Wounded")
	DL_PopActiveObject()
end

--Wait a bit.
fnCutsceneWait(240)
fnCutsceneBlocker()

-- |[Crouch]|
--Mei gets up.
Cutscene_CreateEvent("Change Mei Special Frame", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
    ActorEvent_SetProperty("Special Frame", "Crouch")
DL_PopActiveObject()

--Florentina, if present, gets up.
if(bIsFlorentinaPresent) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Special Frame", "Crouch")
	DL_PopActiveObject()
end

--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

-- |[Normal]|
--Mei stands up.
Cutscene_CreateEvent("Change Mei Special Frame", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
    ActorEvent_SetProperty("Special Frame", "Null")
DL_PopActiveObject()

--Florentina, if present, gets up.
if(bIsFlorentinaPresent) then
	
	--Facing down.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", 0.0, 1.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Special Frame", "Null")
	DL_PopActiveObject()
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Turn.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", -1.0, 1.0)
	DL_PopActiveObject()
	fnCutsceneWait(5)
	fnCutsceneBlocker()
	
	--Turn.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", -1.0, 0.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Face", 1.0, 1.0)
	DL_PopActiveObject()
	fnCutsceneWait(5)
	fnCutsceneBlocker()
	
	--Turn.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", -1.0, -1.0)
	DL_PopActiveObject()
	fnCutsceneWait(5)
	fnCutsceneBlocker()

end

--Wait a bit.
fnCutsceneWait(15)
fnCutsceneBlocker()

-- |[Talking]|
--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Mei talks to herself.
if(bIsFlorentinaPresent == false) then
	
	--Setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Sad") ]])

	--Roll.
	local iDialogueRoll = LM_GetRandomNumber(0, 8)

	--Dialogue handlers.
	if(iDialogueRoll == 0) then
		fnCutscene([[ Append("Mei:[E|Sad] ...[P] That could have gone better...") ]])

	elseif(iDialogueRoll == 1) then
		fnCutscene([[ Append("Mei:[E|Sad] ...[P] Second verse, same as the first...") ]])

	elseif(iDialogueRoll == 2) then
		fnCutscene([[ Append("Mei:[E|Sad] ...[P] I learned something useful::[P] Getting hit, hurts.") ]])

	elseif(iDialogueRoll == 3) then
		fnCutscene([[ Append("Mei:[E|Sad] ...[P] Let's try not to repeat that particular mistake...") ]])

	elseif(iDialogueRoll == 4) then
		fnCutscene([[ Append("Mei:[E|Sad] ...[P] Now I know how video game characters feel...") ]])

	elseif(iDialogueRoll == 5) then
		fnCutscene([[ Append("Mei:[E|Sad] ...[P] *cough* [P] Still a better love story than Twilight...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Heh.") ]])

	elseif(iDialogueRoll == 6) then
		fnCutscene([[ Append("Mei:[E|Sad] ...[P] Good thing I'm still in one piece...") ]])

	elseif(iDialogueRoll == 7) then
		fnCutscene([[ Append("Mei:[E|Sad] I think the key is to duck right before they hit me.[P] Yeah, that's it.") ]])

	elseif(iDialogueRoll == 8) then
		fnCutscene([[ Append("Mei:[E|Sad] Ugh.[P] Better not make a habit of this.") ]])
	end

--Florentina and Mei.
else
	
	--Setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Sad") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])

	--Roll.
	local iDialogueRoll = LM_GetRandomNumber(0, 12)

	--Dialogue handlers.
	if(iDialogueRoll == 0) then
		fnCutscene([[ Append("Mei:[E|Sad] ...[P] That could have gone better...") ]])

	elseif(iDialogueRoll == 1) then
		fnCutscene([[ Append("Mei:[E|Sad] ...[P] Second verse, same as the first...") ]])

	elseif(iDialogueRoll == 2) then
		fnCutscene([[ Append("Mei:[E|Sad] ...[P] I learned something useful::[P] Getting hit, hurts.") ]])

	elseif(iDialogueRoll == 3) then
		fnCutscene([[ Append("Mei:[E|Sad] ...[P] Let's try not to repeat that particular mistake...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Let's.[B][C]") ]])

	elseif(iDialogueRoll == 4) then
		fnCutscene([[ Append("Mei:[E|Sad] ...[P] Now I know how video game characters feel...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] What's a video game?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Oh, nevermind.") ]])

	elseif(iDialogueRoll == 5) then
		fnCutscene([[ Append("Mei:[E|Sad] ...[P] *cough* [P] Still a better love story than Twilight...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Heh.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] I don't follow.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Laugh] You don't want to know.") ]])

	elseif(iDialogueRoll == 6) then
		fnCutscene([[ Append("Mei:[E|Sad] ...[P] Good thing I'm still in one piece...") ]])

	elseif(iDialogueRoll == 7) then
		fnCutscene([[ Append("Mei:[E|Sad] I think the key is to duck right before they hit me.[P] Yeah, that's it.") ]])

	elseif(iDialogueRoll == 8) then
		fnCutscene([[ Append("Mei:[E|Sad] Ugh.[P] Better not make a habit of this.") ]])
	
	elseif(iDialogueRoll == 9) then
		fnCutscene([[ Append("Florentina:[E|Confused] I'll be generous and assume that wasn't part of the plan.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] Gotta be flexible, right?") ]])
	
	elseif(iDialogueRoll == 10) then
		fnCutscene([[ Append("Florentina:[E|Confused] If I didn't know better, I'd think you [P]*liked*[P] getting beaten up.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] Hey, don't judge me.") ]])
	
	elseif(iDialogueRoll == 11) then
		fnCutscene([[ Append("Mei:[E|Sad] ...[P] Still in one piece, Florentina?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Not for long at this pace!") ]])
	
	elseif(iDialogueRoll == 12) then
		fnCutscene([[ Append("Mei:[E|Sad] ...[P] Why must we fight?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Because violence is usually the answer.") ]])
	end

end

--Common.
if(bIsFlorentinaPresent == true) then
	--[[ 
	fnCutsceneBlocker()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move Amount", 0.0, -16.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move Amount", -16.0, 0.0)
	DL_PopActiveObject()
	fnCutsceneBlocker()]]

	--If Florentina is present, walk her to Mei and fold the party.
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
end

AL_SetProperty("Music", "Null")
local iStartedCassandraEvent = VM_GetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N")
if(iStartedCassandraEvent == 1.0) then
	
	--Music setup.
	AL_SetProperty("Music", "TimeSensitive")
	AdvCombat_SetProperty("Next Combat Music", "TimeSensitive", -1.0)
	
--All other cases:
else
	AL_SetProperty("Music", "ForestTheme")
end
