-- |[Scene Post-Transition]|
--Causes a trigger in the tower area.

--Restore the party.
AdvCombat_SetProperty("Restore Party")

--Move maps.
fnCutscene([[ AL_BeginTransitionTo("WildsTowerA", "FORCEPOS:52.0x37.0x0") ]])
fnCutsceneBlocker()
