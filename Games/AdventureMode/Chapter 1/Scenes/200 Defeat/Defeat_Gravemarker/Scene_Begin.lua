-- |[ ================================= Defeat By Gravemarker ================================== ]|
--Cutscene proper.
local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsRelivingScene", "N")

--Clear censor bars.
WD_SetProperty("Clear Censor Bars")

-- |[Repeat Check]|
--If Mei has already seen this scene, it's a normal KO.
local iHasGravemarkerForm = VM_GetVar("Root/Variables/Global/Mei/iHasGravemarkerForm", "N")
if(iHasGravemarkerForm == 1.0 and iIsRelivingScene == 0.0) then
	
	LM_ExecuteScript(gsStandardGameOver)
	return
end

if(iIsRelivingScene == 0.0) then
    LM_ExecuteScript(gsStandardRevert)
end

-- |[Combat]|
--Restore party to full HP in case the player doesn't want to revisit a save point.
AdvCombat_SetProperty("Restore Party")

-- |[Flags]|
--In case of relive, mark Mei as having Gravemarker form.
VM_SetVar("Root/Variables/Global/Mei/iHasGravemarkerForm", "N", 1.0)

-- |[Topics]|
--Unlock these topics if they weren't already.
--None yet!

-- |[Music]|
AL_SetProperty("Music", "Null")
fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

-- |[Asset Streaming]|
fnLoadDelayedBitmapsFromList("Chapter 1 Mei Gravemarker TF", gciDelayedLoadLoadAtEndOfTick)

-- |[ ===================== Block 1: No Image ==================== ]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ Append("Mei awoke in a darkened chamber deep within the decrepit ruin.[P] She opened her eyes and was greeted with an deep veil of blackness.[P] She could see nothing save a faint, distant grey light that seemed to glow about her.[B][C]") ]])
fnCutscene([[ Append("Thick fog billowed through the room, glowing in the cold grey light.[P] She lay for a moment in the darkness, her hands gliding over her soft skin to explore the contours of her muscles.[P] She was uninjured.[P] No cuts, no scratches.[P] Not even a bruise to mar her skin.[P] She stood.[B][C]") ]])
fnCutscene([[ Append("She tried to clear her head, but the unnatural light played with her perception.[P] She moved quietly through the room, using the cold glow of the fog to sense the walls lest the errant scrape of her hand reveal her location to the statue women.[B][C]") ]])
fnCutscene([[ Append("The fog soon began to smell sweet, and as she fumbled in the dark, she realized it was incense.[P] The smell grew stronger as she approached the light, and she could almost see its source.[B][C]") ]])
fnCutscene([[ Append("Just beyond a pillar she could see a small altar with a dozen candles, all burning the sweet-smelling fog.[P] She remained in the dark, crouched behind the pillar in case the statue women were nearby, but heard nothing.[B][C]") ]])
fnCutscene([[ Append("She held her breath, waiting.[P] Who had lit the candles?[P] Would they come back?[P] And where was her sword?[B][C]") ]])
fnCutscene([[ Append("A soft grinding of stone upon stone answered her, and one of the gravemarkers appeared from the mists and placed another candle on the altar.[P] It held its hands in prayer and knelt.[B][C]") ]])
fnCutscene([[ Append("Mei knelt closer to the pillar, thinking to glean any useful information from this display.[P] She leaned into its shadow, trying to stay out of sight, and placed a hand upon it to steady herself.[B][C]") ]])
fnCutscene([[ Append("The pillar moved.") ]])
fnCutsceneBlocker()


-- |[ =================== Block 2: TF Image 0 ==================== ]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/GravemarkerTF0") ]])
fnCutscene([[ Append("Firm hands gripped her, and the sound of stone grinding upon stone rose up around her.[P] These were not pillars.[P] The room was filled with the statue women![B][C]") ]])
fnCutscene([[ Append("Two pairs of hands hefted her from her hiding spot and shoved her towards the altar.[P] More surrounded her from the shadows as she stood in the candlelight.[B][C]") ]])
fnCutscene([[ Append("Her runestone and sword were behind the altar, glinting in the dancing light.[P] A gravemarker stood atop them, behind the altar, her hands in the air.[P] Surrounded by dozens of the statue women, Mei dared not make a move for her gear.[P] She watched them, wary of any movement that might reveal their plan.[P] Did they intend to sacrifice her?[P] Simply kill her?[B][C]") ]])
fnCutscene([[ Append("The women stood shoulder to shoulder, their wings overlapping to form an impenetrable wall, and began to sway to some unheard rhythm.[P] Mei thought for a moment she could hear a distant chorus, but it was merely the scraping of stone on stone they made when they shifted.[B][C]") ]])
fnCutscene([[ Append("The gravemarker who stood atop her equipment suddenly flared her wings, and the statues stopped swaying.[P] Two of the statues stepped forward, each grabbing one of her arms.[P] Mei realized at that moment that she could not move, even if the statue women were not holding her.[P] Though she did not feel any different, she had become paralyzed.[B][C]") ]])
fnCutscene([[ Append("Stone hands pulled at her dress, pulling the fabric up and exposing her sex to the cold fog of the incense.[P] They raised her arms, her hands turned up in silent requiem, allowing the cold hands to slip the fabric over her head.[P] Her long black hair tumbled over her shoulders and framed her bare breasts.[B][C]") ]])
fnCutscene([[ Append("They lowered her arms and released her.[P] She stood naked before them, willing her unmoving hands to cover herself.[P] They would not respond.[P] She stood in rigid attention before the 'preacher' statue, her body as open to her as her soul might have been to a preacher from her home.[B][C]") ]])
fnCutscene([[ Append("Naked,[P] humiliated,[P] cold...[P] She could only wait as the statue continued to make silent gestures to the others.[B][C]") ]])
fnCutscene([[ Append("Another statue appeared, bearing a robe.[P] The same robe they all wore, but made of fabric instead of stone.[P] Mei felt her arms raise against her protests, but her body would not respond.") ]])
fnCutsceneBlocker()

-- |[ =================== Block 3: TF Image 1 ==================== ]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/GravemarkerTF0") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/GravemarkerTF1") ]])
fnCutscene([[ Append("The preacher raised her hands, and all the statues lifted their gaze.[P] Mei followed suit, and finally, she could hear it.[P] The chorus.[P] The statues began to dress her as she felt their song flood into her body.[B][C]") ]])
fnCutscene([[ Append("This was the heavenly light.[P] The pure, perfect voice of the angels.[P] Now dressed in their robes, the preacher produced a flask of golden liquid.[P] Mei did not want to drink it.[P] Her body did not heed her silent protest.[B][C]") ]])
fnCutscene([[ Append("Struggling against herself, her every fiber screaming at her hands to cast aside the flask, she raised it to her lips and drank deeply.[P] The chorus became louder and clearer, and she felt her back muscles contract as feathered wings began to push forth from her skin.[P] The statues pulled her robes away to give her wings space to grow.") ]])
fnCutsceneBlocker()

-- |[ =================== Block 4: TF Image 2 ==================== ]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/GravemarkerTF1") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/GravemarkerTF2") ]])
fnCutscene([[ Append("Now the preacher picked up one of the incense candles and held it out to her.[P] Mei's hands received the candle and held it before her head.[P] She wanted to throw the thing away,[P] to grab her runestone and flee,[P] but her body refused to listen.[P] The incense was strong, and she took in every bit of it.[P] As the candle dwindled, she felt her skin begin to harden.[B][C]") ]])
fnCutscene([[ Append("The incense invaded her lungs and they grew as rigid as her muscles.[P] Her breath caught in her mouth, and her body began to starve for oxygen.[P] She could feel it spread to her heart, creeping along the delicate tissues that held her body together and turning them into stone fibers.[P] The stone gripped her heart, a claw formed from the very bones of the mountains themselves, and stopped her heart.[B][C]") ]])
fnCutscene([[ Append("The mountainous grasp clutched at her veins and dragged itself throughout her body.[P] It spread to her abdomen and limbs, prickling at her fingers and toes with its grasp.[P] Her body tightened as her mind begged for a single breath with which to scream, but the weighty anger of the stone would not release her.[B][C]") ]])
fnCutscene([[ Append("The stone spread throughout her body, dragging each cell from each organ and bit of tissue within her into its cold embrace.[P] She could not move or whisper even a single breath.[P] She could only scream inside her head as the changes took hold.") ]])
fnCutsceneBlocker()
fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Gravemarker.lua") ]])

-- |[ =================== Block 5: TF Image 3 ==================== ]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/GravemarkerTF2") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/GravemarkerTF3") ]])
fnCutscene([[ Append("Now her muscles listened to her, but the sound of the chorus overwhelmed her thoughts.[P] The stone of her body reverberated with the heavenly choir, allowing it to to fill every piece of her being.[P] Every part of her was angelic,[P] receptive.[P] She looked down at herself.[B][C]") ]])
fnCutscene([[ Append("Where once there was flesh was now nothing more than stone.[P] Even her clothes had followed suit.[P] The other gravemarkers released her.[P] But she still had her head.[P] She was not like them.[P] Not yet.[B][C]") ]])
fnCutscene([[ Append("Every moment of their existence, they were filled by the light in pure bliss,[P] and pure agony.[P] Mei understood now.[P] The angels had left these guardians here for reasons long forgotten, and they stood silent vigil.[P] The chorus bathed them, took away their pain, and yet she still wanted to scream.") ]])
fnCutsceneBlocker()

-- |[ =================== Block 6: TF Image 4 ==================== ]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/GravemarkerTF3") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/GravemarkerTF4") ]])
fnCutscene([[ Append("As the chorus grew to a crescendo, the holiest piece of the ritual was put before her.[P] A diamond-tipped saw.[P] With it she would cut off her own head and become a gravemarker.[P] She held it to her neck and drew it back and forth.[P] Her body told her to do this.[P] The light told her to do this.[P] She wanted to do this,[P] no matter how much it hurt.[B][C]") ]])
fnCutscene([[ Append("But deep within her, in the last part of her that was not smothered by the holy light, she screamed.[P] And the scream shook within her.[B][C]") ]])
fnCutscene([[ Append("She threw down the saw.[P] A statue tried to grab her.[P] She pushed away its hands and threw it to the ground.[P] She was as heavy and as powerful as they were now.") ]])
fnCutsceneBlocker()

-- |[ ===================== Block 7: No Image ==================== ]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/GravemarkerTF4") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Null") ]])
fnCutscene([[ Append("The preacher led the chorus again, and the singing became louder, but Mei was beyond its power.[P] She leapt forward, stepping upon the sacred alter, and knocked away the candles.[B][C]") ]])
fnCutscene([[ Append("She picked up her sword and runestone, and held the rune to her head.[P] The choir faded.[P] Their control disappeared.[P] She was stone.[P] She was angel.[P] But first,[P] she was Mei.[B][C]") ]])
fnCutscene([[ Append("The assembled gravemarkers bore down on her in a circle.[P] There was nowhere to run.[P] She fell to her knees and clutched the runestone to her stone breast.[P] A glow began to emanate from it, seeping from between her fingers until it filled the room with a brilliant light.[P] The glow faded, and the gravemarkers stood in a silent circle.[P] In the middle of them, illuminated by the dying flicker of the overturned candles, was empty space.[P] Mei was not there.") ]])
fnCutsceneBlocker()

-- |[ ========================= Clean Up ========================= ]|
--Clean up.
fnCutsceneWait(gciDialogue_Fadeout_Then_Unload_Ticks)
fnCutsceneBlocker()
fnCutscene([[ fnUnloadBitmapsFromList("Chapter 1 Mei Gravemarker TF") ]])

--Stop here during relives.
if(iIsRelivingScene == 1.0) then
	fnCutscene([[ AL_BeginTransitionTo("LASTSAVE", gsStandardReliveEnd) ]])
	fnCutsceneBlocker()
    return
end

--Jump to StarfieldSwampG for the finale.
fnCutscene([[ AL_BeginTransitionTo("StarfieldSwampG", "FORCEPOS:61.0x33.0x0") ]])
fnCutsceneBlocker()
