-- |[Scene Post-Transition]|
--After the cutscene goes to fullbright, it switches maps. This plays afterwards.
AL_SetProperty("Activate Fade", 60, gci_Fade_Over_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0)

--Wait a bit.
fnCutsceneWait(75)
fnCutsceneBlocker()

-- |[Re-add Florentina]|
local iHasSeenTrannadarFlorentinaScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
if(iHasSeenTrannadarFlorentinaScene == 1.0) then

	--Get Mei's position.
	EM_PushEntity("Mei")
		local iMeiX, iMeiY = TA_GetProperty("Position")
	DL_PopActiveObject()

	--Create Florentina's entity. Place her at Mei.
	local iFlorentinaID = fnSpecialCharacter("Florentina", iMeiX, iMeiY, gci_Face_South, false, nil)
    fnAddPartyMember("Florentina")

end

-- |[Dialogue]|
--Mei talks to herself.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
if(iHasSeenTrannadarFlorentinaScene == 1.0) then
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
end

--Actual talking.
fnCutscene([[ Append("Mei:[E|Blush] Back to work, I suppose...[B][C]") ]])

--If Florentina is present, this dialogue triggers.
if(iHasSeenTrannadarFlorentinaScene == 1.0) then
	fnCutscene([[ Append("Florentina:[E|Neutral] Done having fun with the bees, are you?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] It was...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] Sheesh.[P] Were you a virgin or something?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] ...[P] I've never felt something so intense before...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Surprise] That's lovely.[P] If you're done mucking about, shall we get going?") ]])
end

--Common code.
fnCutsceneBlocker()
