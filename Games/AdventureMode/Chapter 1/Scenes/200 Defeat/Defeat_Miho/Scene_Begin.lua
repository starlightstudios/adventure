-- |[Scene Post-Transition]|
--Miho beats the crap out of you.

-- |[No Arguments: Initial Cutscene]|
local iArgs = LM_GetNumOfArgs()
if(iArgs == 0) then

    --Variables.
    local iIsRubberMode = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
    if(iIsRubberMode == 0.0) then return end

    --Restore the party.
    AdvCombat_SetProperty("Restore Party")

    --Fade to black.
    fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Villager:[VOICE|HumanF0] Eeeek![P] What's wrong with her skin!?[B][C]") ]])
    fnCutscene([[ Append("Guard:[VOICE|MercF] Stay back, fiend![B][C]") ]])
    fnCutscene([[ Append("Guard:[VOICE|MercM] Help!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Miho", "Angry") ]])
    fnCutscene([[ Append("Miho:[E|Angry] [SOUND|Combat|Impact_Slash]Take that![B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Angry] [SOUND|Combat|Impact_Slash_Crit]And that![B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Angry] [SOUND|Combat|Impact_Strike_Crit]Hiyyaa![P] The fiend falls![P] Another victory for the forces of the righteous!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()

    -- |[Dialogue Decisions]|
    --Setup.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])

    --Dialogue.
    fnCutscene([[ Append("Narrator: (Retry, or give up?)[BLOCK]") ]])

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Retry\", "    .. sDecisionScript .. ", \"Retry\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Give Up\",  " .. sDecisionScript .. ", \"Give Up\") ")
    fnCutsceneBlocker()

else
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
    
    --Case:
    local sArgument = LM_GetScriptArgument(0)
	
    --Retry:
    if(sArgument == "Retry") then
        VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iInitializedMihoEvent", "N", 0.0)
        fnCutsceneTeleport("Mei", 46.25, 2.50)
    
    --Give up:
    elseif(sArgument == "Give Up") then
        VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N", 0.0)
        AL_BeginTransitionTo("WildsTowerA", "FORCEPOS:52.0x37.0x0")
    
    end
end
