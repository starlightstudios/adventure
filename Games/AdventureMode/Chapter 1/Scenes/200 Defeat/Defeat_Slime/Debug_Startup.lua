-- |[Debug_Startup]|
--This lua file is only ever called if the cutscene is triggered via the debug menu. It will make sure
-- the map is correct and that the characters are present and in their default positions. It will also,
-- if necessary, set scripting flags so the cutscene makes sense.
Debug_PushPrint(false, "Debug Firing Cutscene: Defeat_Slime\n")

--In debug mode, the scene plays even if we've already seen it. This is determined by whether or not
-- Mei has access to slime form.
VM_SetVar("Root/Variables/Global/Mei/iHasSlimeForm", "N", 0.0)

--Other variables.
VM_SetVar("Root/Variables/Chapter1/Scenes/iSlimeHasSeenTrip", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iSlimeHasSeenFeelFunny", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iSlimeHasFinishedTF", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iSlimeMeetFlorentina", "N", 0.0)

--Optional.
--VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N", 1.0)

--Otherwise, run the file normally. It may or may not change the map itself.
LM_ExecuteScript(fnResolvePath() .. "Scene_Begin.lua")

--Debug.
Debug_PopPrint("Completed debug cutscene firing.\n")
