-- |[Defeat By Slime]|
--Cutscene proper. Uses the dialogue's SceneHandler to get the TF sequence.
local bSkipMostOfScene = false
local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsRelivingScene", "N")

-- |[Repeat Check]|
--If Mei can already turn into a slime, this scene does not play.
local iMeiHasSlimeForm = VM_GetVar("Root/Variables/Global/Mei/iHasSlimeForm", "N")
if(iMeiHasSlimeForm == 1 and iIsRelivingScene == 0.0) then
	LM_ExecuteScript(gsStandardGameOver)
	return
end

-- |[Map Check]|
--Make sure we're in the cutscene map.
local sCurrentLevelName = AL_GetProperty("Name")
if(sCurrentLevelName ~= "EvermoonSlimeScene") then

	--Run the sub-cutscene. It adds a lot of events, and will fadeout the camera automatically.
	if(iIsRelivingScene == 0.0) then
		LM_ExecuteScript(gsStandardRevert)
	end

	--Transfer to correct map.
	local sString = "AL_BeginTransitionTo(\"EvermoonSlimeScene\", \"" .. LM_GetCallStack(0) .. "\")"
	fnCutscene(sString)
	return
end

-- |[Remove Florentina]|
--If Florentina is in the party, remove her. She rejoins shortly.
fnRemovePartyMember("Florentina", true)

-- |[Form]|
--Reshift Mei back into a Human. This keeps the scenes looking sane.
LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Human.lua")

-- |[Combat]|
--Restore party to full HP in case the player doesn't want to revisit a save point.
AdvCombat_SetProperty("Restore Party")

-- |[Music]|
AL_SetProperty("Music", "Null")

-- |[Cutscene Execution]|
--Mei can now turn into a slime whenever she wants. Good job, Mei!
VM_SetVar("Root/Variables/Global/Mei/iHasSlimeForm", "N", 1.0)

--Fade from black to nothing over 60 ticks. This fade is under the UI.
AL_SetProperty("Activate Fade", 60, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0)

-- |[Movement]|
--Reposition Mei, make her use the crouch image.
EM_PushEntity("Mei")
	TA_SetProperty("Position", 13, 6)
	TA_SetProperty("Set Special Frame", "Wounded")
DL_PopActiveObject()
Cutscene_CreateEvent("Face Mei South", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move Amount", 0, 1.0)
DL_PopActiveObject()

--Wait a bit for the fade.
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Sad") ]])

--Actual talking.
fnCutscene([[ Append("Mei: ...[P] It seems I'm still kicking...") ]])

-- |[Movement]|
--Crouch.
Cutscene_CreateEvent("Event", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Special Frame", "Crouch")
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()

--Stand.
Cutscene_CreateEvent("Event", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Special Frame", "Null")
DL_PopActiveObject()
fnCutsceneWait(35)
fnCutsceneBlocker()

--Look around.
Cutscene_CreateEvent("Event", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 1.0, -1.0)
DL_PopActiveObject()
fnCutsceneWait(30)
fnCutsceneBlocker()
Cutscene_CreateEvent("Event", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", -1.0, 1.0)
DL_PopActiveObject()
fnCutsceneWait(30)
fnCutsceneBlocker()
Cutscene_CreateEvent("Event", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 1.0, 1.0)
DL_PopActiveObject()
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei") ]])

--Florentina is not in the party.
local iHasSeenTrannadarFlorentinaScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
if(iHasSeenTrannadarFlorentinaScene == 0.0) then
	fnCutscene([[ Append("Mei:[E|Offended] Seems the slime-thing decided to leave me alone.[P] I guess I don't taste good.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Right then.[P] Back to work finding a way home.") ]])

--Florentina is in the party.
else
	fnCutscene([[ Append("Mei:[E|Surprise] F-[P]Florentina?[P] Where are you?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Sad] I hope she's okay.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] No sense moping.[P] She ain't gonna find herself!") ]])
end
