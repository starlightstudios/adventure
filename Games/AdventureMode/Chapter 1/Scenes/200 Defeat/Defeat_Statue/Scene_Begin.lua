-- |[Scene Post-Transition]|
--After transition, the party gets up and comments on the statue situation.

--Restore the party.
AdvCombat_SetProperty("Restore Party")

-- |[Variables]|
local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")

-- |[Overlay]|
--Set the overlay to fullblack. Fade in slowly.
AL_SetProperty("Activate Fade", 0, gci_Fade_Over_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1)
fnCutsceneWait(120)
fnCutsceneBlocker()

-- |[Wounded]|
--Move Florentina one pixel up so she's behind Mei.
if(bIsFlorentinaPresent) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move Amount", 16.0, 0.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move Amount", 0.0, 16.0)
	DL_PopActiveObject()
	fnCutsceneBlocker()
end

-- |[Fade In]|
fnCutscene([[ AL_SetProperty("Activate Fade", 180, gci_Fade_Over_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])

--Change Mei to the "Downed" frames.
Cutscene_CreateEvent("Change Mei Special Frame", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
    ActorEvent_SetProperty("Special Frame", "Wounded")
DL_PopActiveObject()

--If Florentina is present, set her to downed as well.
if(bIsFlorentinaPresent) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Special Frame", "Wounded")
	DL_PopActiveObject()
end

--Wait a bit.
fnCutsceneWait(240)
fnCutsceneBlocker()

-- |[Crouch]|
--Mei gets up.
Cutscene_CreateEvent("Change Mei Special Frame", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
    ActorEvent_SetProperty("Special Frame", "Crouch")
DL_PopActiveObject()

--Florentina, if present, gets up.
if(bIsFlorentinaPresent) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Special Frame", "Crouch")
	DL_PopActiveObject()
end

--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

-- |[Normal]|
--Mei stands up.
Cutscene_CreateEvent("Change Mei Special Frame", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
    ActorEvent_SetProperty("Special Frame", "Null")
DL_PopActiveObject()

--Florentina, if present, gets up.
if(bIsFlorentinaPresent) then
	
	--Facing down.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", 0.0, 1.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Special Frame", "Null")
	DL_PopActiveObject()
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Turn.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", -1.0, 1.0)
	DL_PopActiveObject()
	fnCutsceneWait(5)
	fnCutsceneBlocker()
	
	--Turn.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", -1.0, 0.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Face", 1.0, 1.0)
	DL_PopActiveObject()
	fnCutsceneWait(5)
	fnCutsceneBlocker()
	
	--Turn.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", -1.0, -1.0)
	DL_PopActiveObject()
	fnCutsceneWait(5)
	fnCutsceneBlocker()

end

--Wait a bit.
fnCutsceneWait(15)
fnCutsceneBlocker()

-- |[Talking]|
--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Mei talks to herself.
if(bIsFlorentinaPresent == false) then
	
	--Setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Sad") ]])
	fnCutscene([[ Append("Mei:[E|Sad] ...[P] Wha..?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Sad] I guess there was some magic on that statue.[P] Better not randomly guess next time.") ]])

--Florentina and Mei.
else
	
	--Setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Sad") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
	
	--Dialogue.
	fnCutscene([[ Append("Mei:[E|Sad] ...[P] Florentina?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] I'm here, don't worry.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Sad] What just happened?[P] I feel sick.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] I think you got the question wrong, and then it all went black.[P] Must be some sort of trap.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] We should look around and see if we can find an answer to the riddle, then.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Exactly what I was thinking.") ]])
end

--Common.
if(bIsFlorentinaPresent == true) then
	fnCutsceneBlocker()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move Amount", 0.0, -16.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move Amount", -16.0, 0.0)
	DL_PopActiveObject()
	fnCutsceneBlocker()

	--If Florentina is present, walk her to Mei and fold the party.
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
end
