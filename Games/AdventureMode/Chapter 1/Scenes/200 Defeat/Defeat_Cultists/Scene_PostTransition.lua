-- |[Scene Post-Transition]|
--After the cutscene goes to fullbright, it switches maps. This plays afterwards.
AL_SetProperty("Activate Fade", 60, gci_Fade_Over_GUI, false, 1, 1, 1, 1, 1, 1, 1, 0)

--Wait a bit.
fnCutsceneWait(120)
fnCutsceneBlocker()

--Mei stands up.
fnCutsceneWait(15)
Cutscene_CreateEvent("Change Mei Special Frame", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
    ActorEvent_SetProperty("Special Frame", "Null")
DL_PopActiveObject()
fnCutsceneBlocker()

--Have Mei look around.
Cutscene_CreateEvent("Face Mei SouthWest", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", -1.0, 1.0)
DL_PopActiveObject()
fnCutsceneWait(30)
fnCutsceneBlocker()
Cutscene_CreateEvent("Face Mei NorthWest", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", -1.0, -1.0)
DL_PopActiveObject()
fnCutsceneWait(30)
fnCutsceneBlocker()
Cutscene_CreateEvent("Face Mei East", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 1.0, 0.0)
DL_PopActiveObject()
fnCutsceneWait(30)
fnCutsceneBlocker()
Cutscene_CreateEvent("Face Mei South", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 0.0, 1.0)
DL_PopActiveObject()
fnCutsceneWait(30)
fnCutsceneBlocker()

--Wait a bit longer on the south.
fnCutsceneWait(60)
fnCutsceneBlocker()

--Mei talks to herself.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Sad") ]])

--Actual talking.
fnCutscene([[ Append("Mei:[E|Sad] What on earth?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] H-[P]hey.[BLOCK] That psycho is gone![BLOCK] And I'm not in a jail cell anymore![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Laugh] And I'm talking to myself![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Well, little rock, I guess I owe you one.[BLOCK] I'll be hanging on to you for a little while longer.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] Right then,[P] back to work finding a way back home!") ]])
fnCutsceneBlocker()
