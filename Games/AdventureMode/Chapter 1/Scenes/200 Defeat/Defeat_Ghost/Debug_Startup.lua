-- |[Debug_Startup]|
--This lua file is only ever called if the cutscene is triggered via the debug menu. It will make sure
-- the map is correct and that the characters are present and in their default positions. It will also,
-- if necessary, set scripting flags so the cutscene makes sense.
Debug_PushPrint(false, "Debug Firing Cutscene: Defeat_Ghost\n")

--Flip the flag so this scene plays even if the player has already seen it.
VM_SetVar("Root/Variables/Global/Mei/iHasGhostForm", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iDisturbedCoffin", "N", 1.0)

--Mei needs to have the key if she doesn't already:
local iKeyCount = AdInv_GetProperty("Item Count", "Quantir Mansion Key")
if(iKeyCount < 1) then
	LM_ExecuteScript(gsItemListing, "Quantir Mansion Key")
end

--Other variables:
VM_SetVar("Root/Variables/Chapter1/Scenes/iIsGhostTF", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iPutMaidOutfitOn", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iToldNatalieTwice", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCanLeaveRoomW", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iLydieLeftParty", "N", 0.0)

--Cleaning Progress
VM_SetVar("Root/Variables/Chapter1/Scenes/iMadeBed", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedBookshelvesW", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedBookshelvesE", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedChairW", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedChairC", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedChairE", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedTableW", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedTableC", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedTableE", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N", 0.0)

--Otherwise, run the file normally. It may or may not change the map itself.
LM_ExecuteScript(fnResolvePath() .. "Scene_Begin.lua")

--Debug.
Debug_PopPrint("Completed debug cutscene firing.\n")
