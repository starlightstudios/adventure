-- |[Defeat By Ghost]|
--Cutscene proper.
local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsRelivingScene", "N")

--Dialogue path.
local sDialoguePath = gsRoot .. "Maps/QuantirManse/QuantirManseCentralW/Dialogue.lua"

-- |[Repeat Check]|
--If Mei has already seen this scene, it's a normal KO.
local iHasGhostForm = VM_GetVar("Root/Variables/Global/Mei/iHasGhostForm", "N")
if(iHasGhostForm == 1.0 and iIsRelivingScene == 0.0) then
	
	LM_ExecuteScript(gsStandardGameOver)
	return
end

--Set.
VM_SetVar("Root/Variables/Chapter1/Scenes/iIsGhostTF", "N", 1.0)

-- |[Knockout Scene]|
--If we're not on the cutscene map, knock down both Mei and Florentina.
local sCurrentLevelName = AL_GetProperty("Name")
if(sCurrentLevelName ~= "QuantirManseCentralW") then
	
	--Transfer to correct map.
	local sString = "AL_BeginTransitionTo(\"QuantirManseCentralW\", \"" .. LM_GetCallStack(0) .. "\")"
	fnCutscene(sString)
	return
end

-- |[Remove Florentina]|
--Take her out of the party, both on the status screen and overworld.
fnRemovePartyMember("Florentina", false)

--If the Florentina entity happens to be on the field, move her here. Also change her dialogue.
if(EM_Exists("Florentina") == true) then
	EM_PushEntity("Florentina")
		TA_SetProperty("Position", 6, 5)
        TA_SetProperty("Activation Script", sDialoguePath)
		TA_SetProperty("Set Special Frame", "Crouch")
	DL_PopActiveObject()
end

-- |[Combat]|
--Restore party to full HP in case the player doesn't want to revisit a save point.
AdvCombat_SetProperty("Restore Party")

-- |[Topics]|
--Unlock these topics if they weren't already.
--None yet!

-- |[Flags]|
--Variables.
local iHasFlorentina = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")

-- |[Music]|
AL_SetProperty("Music", "Null")
fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

-- |[Camera]|
--Move Mei to this position.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Teleport To", (13.25 * gciSizePerTile), (8.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneWait(15)
fnCutsceneBlocker()

--Switch Mei to Human form.
fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Human.lua") ]])
	
--Mei temporarily changes name to Natalie.
fnCutscene([[
AdvCombat_SetProperty("Push Party Member", "Mei")
	AdvCombatEntity_SetProperty("Display Name", "Natalie")
DL_PopActiveObject() ]])

--Unfade.
fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneWait(75)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])

--Talking.
fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Surprise] (.....![P] Oh, no, I was sleeping on the job!)[B][C]") ]])
fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Neutral] (That was a very strange dream, though.[P] I bet Lydie will want to hear all about it!)[B][C]") ]])
fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Sad] (No no, focus![P] The countess will be so angry![P] Better work double-time before she comes for inspection!)") ]])
fnCutsceneBlocker()

-- |[Movement]|
--A ghost enters the room.
fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Lydie")
	ActorEvent_SetProperty("Teleport To", (19.25 * gciSizePerTile), (8.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Ghost moves up to Natalie.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Lydie")
	ActorEvent_SetProperty("Move To", (18.25 * gciSizePerTile), (8.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 1, 0)
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Lydie", "Neutral") ]])

--Talking.
fnCutscene([[ Append("Lydie: Natalie![P] What have you been doing!?[P] This room is a disaster![B][C]") ]])
fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Sad] I know![P] I'm sorry![P] I just -[P] I fell asleep and had the weirdest dream...[B][C]") ]])
fnCutscene([[ Append("Lydie: You can tell me all about it later.[P] You're not even dressed![P] You've got to get this room cleaned up, pronto![B][C]") ]])
fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Surprise] Ack, I left my uniform under my bed![B][C]") ]])
fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Cry] If the countess sees me out of dress she'll really let me have it![B][C]") ]])
fnCutscene([[ Append("Lydie: You're such a scatterbrain![P] Did you forget that there's a spare outfit in the guest room?[B][C]") ]])
fnCutscene([[ Append("Lydie: It's in the crate over there, hurry up![B][C]") ]])
fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Happy] Thanks, Lydie![P] I don't know what I'd do without you![B][C]") ]])
fnCutscene([[ Append("Lydie: Just hurry up!") ]])
fnCutsceneBlocker()
