-- |[Volunteer To Ghost]|
--Cutscene proper.

--Dialogue path.
local sDialoguePath = gsRoot .. "Maps/QuantirManse/QuantirManseCentralW/Dialogue.lua"

--Set.
VM_SetVar("Root/Variables/Chapter1/Scenes/iIsGhostTF", "N", 1.0)
	
--In all cases, mark the combat intro dialogues as complete.
VM_SetVar("Root/Variables/Chapter1/Counts/sMeiSeenPartirhuman", "S", "Complete")

--Set these variables differently. Natalie is already wearing the uniform.
VM_SetVar("Root/Variables/Chapter1/Scenes/iPutMaidOutfitOn", "N", 1.0)

--Spawn Lydie.
TA_Create("Lydie")
	TA_SetProperty("Position", -100, -100)
	TA_SetProperty("Facing", gci_Face_South)
	TA_SetProperty("Clipping Flag", true)
    TA_SetProperty("Activation Script", sDialoguePath)
	fnSetCharacterGraphics("Root/Images/Sprites/Lydie/", false)
DL_PopActiveObject()

-- |[Remove Florentina]|
--Take her out of the party, both on the status screen and overworld.
fnRemovePartyMember("Florentina", false)

--If the Florentina entity happens to be on the field, move her here. Also change her dialogue.
if(EM_Exists("Florentina") == true) then
	EM_PushEntity("Florentina")
		TA_SetProperty("Position", 6, 5)
        TA_SetProperty("Activation Script", sDialoguePath)
		TA_SetProperty("Set Special Frame", "Crouch")
	DL_PopActiveObject()
end

-- |[Combat]|
--Restore party to full HP in case the player doesn't want to revisit a save point.
AdvCombat_SetProperty("Restore Party")

-- |[Topics]|
--Unlock these topics if they weren't already.
--None yet!

-- |[Flags]|
--Variables.
local iHasFlorentina = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")

-- |[Music]|
AL_SetProperty("Music", "Null")
fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

-- |[Camera]|
--Move Mei to this position.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Teleport To", (13.25 * gciSizePerTile), (8.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneWait(15)
fnCutsceneBlocker()

--Switch Mei to Ghost form.
fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Ghost.lua") ]])
	
--Mei temporarily changes name to Natalie.
fnCutscene([[
AdvCombat_SetProperty("Push Party Member", "Mei")
	AdvCombatEntity_SetProperty("Display Name", "Natalie")
DL_PopActiveObject() ]])

--Unfade.
fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneWait(75)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Smirk] (Hmmm hmmm hmmm...[P] dust this, dust that...)[B][C]") ]])
fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Neutral] (Oh, what time is it?[P] I was daydreaming about that Mei girl...[P] I've barely even cleaned this room!)[B][C]") ]])
fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Sad] (I hope the countess doesn't catch me, or she'll really let me have it!)") ]])
fnCutsceneBlocker()

-- |[Movement]|
--A ghost enters the room.
fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Lydie")
	ActorEvent_SetProperty("Teleport To", (19.25 * gciSizePerTile), (8.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Ghost moves up to Natalie.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Lydie")
	ActorEvent_SetProperty("Move To", (18.25 * gciSizePerTile), (8.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 1, 0)
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Lydie", "Neutral") ]])
fnCutscene([[ Append("Lydie: Natalie![P] What have you been doing!?[P] I thought you were supposed to clean this room![B][C]") ]])
fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Sad] I know![P] I'm sorry![P] I was kinda daydreaming...[P] and...[B][C]") ]])
fnCutscene([[ Append("Lydie: You can tell me all about it later.[P] You've got to get this room cleaned up, pronto![B][C]") ]])
fnCutscene([[ Append("Lydie: I'm almost done my chores, so if you're really quick I promise we'll hang out when you're done.[B][C]") ]])
fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Happy] Oh! I'll be sure to hurry![P] Thanks, Lydie![B][C]") ]])
fnCutscene([[ Append("Lydie: Just hurry up!") ]])
fnCutsceneBlocker()
