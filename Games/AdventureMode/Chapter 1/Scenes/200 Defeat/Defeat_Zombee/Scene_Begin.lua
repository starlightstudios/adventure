-- |[ ==================================== Defeat By Zombee ==================================== ]|
--Cutscene proper. Only does anything if Mei is a bee, otherwise it takes you to the last rest point.
local bSkipMostOfScene = false

--Clear censor bars.
WD_SetProperty("Clear Censor Bars")

-- |[ ======== Repeat Check ======== ]|
--If Mei has already seen this scene, it's a normal KO.
local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local iHasSeenZombeeScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenZombeeScene", "N")
local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsRelivingScene", "N")
if(iIsRelivingScene == 1.0) then
    --Proceed as normal.
    
elseif(iHasSeenZombeeScene == 1.0 or sMeiForm ~= "Bee") then
	
	LM_ExecuteScript(gsStandardGameOver)
	return
end

-- |[ ======= Knockout Scene ======= ]|
--If we're not on the cutscene map, knock down both Mei and Florentina.
local sCurrentLevelName = AL_GetProperty("Name")
if(sCurrentLevelName ~= "BeehiveBasementScene") then
	
	--Transfer to correct map.
	local sString = "AL_BeginTransitionTo(\"BeehiveBasementScene\", \"" .. LM_GetCallStack(0) .. "\")"
	fnCutscene(sString)
	return
end

-- |[ ====================================== Scene Setup ======================================= ]|
-- |[Image Streaming]|
fnLoadDelayedBitmapsFromList("Chapter 1 Mei Zombee TF", gciDelayedLoadLoadAtEndOfTick)

-- |[Remove Florentina]|
--Take her out of the party, both on the status screen and overworld.
fnRemovePartyMember("Florentina", true)

-- |[Form]|
--Reshift Mei to Bee.
LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Bee.lua")

-- |[Combat]|
--Restore party to full HP in case the player doesn't want to revisit a save point.
AdvCombat_SetProperty("Restore Party")

-- |[Topics]|
--Unlock these topics if they weren't already.
--None yet!

-- |[Music]|
AL_SetProperty("Music", "Apprehension")

-- |[Actors]|
--Spawn some additional Zombees. These are NPCs.
TA_Create("ZombeeL")
	TA_SetProperty("Position", 5, 5)
	TA_SetProperty("Clipping Flag", false)
	fnSetCharacterGraphics("Root/Images/Sprites/BeeGirlZ/", false)
	TA_SetProperty("Facing", gci_Face_South)
DL_PopActiveObject()
TA_Create("ZombeeR")
	TA_SetProperty("Position", 7, 5)
	TA_SetProperty("Clipping Flag", false)
	fnSetCharacterGraphics("Root/Images/Sprites/BeeGirlZ/", false)
	TA_SetProperty("Facing", gci_Face_South)
DL_PopActiveObject()

--Move Mei to this position.
EM_PushEntity("Mei")
	TA_SetProperty("Position", 6, 5)
	TA_SetProperty("Set Special Frame", "Crouch")
DL_PopActiveObject()

-- |[ =================================== Cutscene Execution =================================== ]|
--Set this variable so the scene doesn't play twice.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenZombeeScene", "N", 1.0)

--Fading.
AL_SetProperty("Activate Fade", 120, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0)

--Reposition Mei, make her use the wounded image.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 0, 1.0)
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit for the fade.
fnCutsceneWait(180)
fnCutsceneBlocker()

-- |[Dialogue Sequence]|
--Dialogue setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Sad") ]])

--Talking.
fnCutscene([[ Append("Mei:[E|Sad] (My head...)[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] (S-[P]sister drones?[P] Oh no, they're-[P] I have to get out of here!)") ]])
fnCutsceneBlocker()

-- |[Movement]|
--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--The bees drag Mei.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "ZombeeL")
	ActorEvent_SetProperty("Move To", (5.25 * gciSizePerTile), (6.50 * gciSizePerTile), 0.50)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (6.25 * gciSizePerTile), (6.50 * gciSizePerTile), 0.50)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "ZombeeR")
	ActorEvent_SetProperty("Move To", (7.25 * gciSizePerTile), (6.50 * gciSizePerTile), 0.50)
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--The bees drag Mei again.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "ZombeeL")
	ActorEvent_SetProperty("Move To", (5.25 * gciSizePerTile), (7.50 * gciSizePerTile), 0.50)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (6.25 * gciSizePerTile), (7.50 * gciSizePerTile), 0.50)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "ZombeeR")
	ActorEvent_SetProperty("Move To", (7.25 * gciSizePerTile), (7.50 * gciSizePerTile), 0.50)
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

-- |[Dialogue Sequence]|
--Dialogue setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Sad") ]])

--Talking.
fnCutscene([[ Append("Mei:[E|Sad] (The honey here...[P] corrupted...)[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Cry] (Pl-[P]please.[P] I don't want this...[P] Snap out of it, sisters!)") ]])
fnCutsceneBlocker()

-- |[Scene]|
--Fade out.
fnCutscene([[ AL_SetProperty("Activate Fade", 120, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])

--Scene plays under darkness. It goes badly for Mei.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ WD_SetProperty("Transition Scene", "Null", "Root/Images/Scenes/Mei/ZombeeTF0") ]])
fnCutscene([[ Append("Heedless to her psychic pleas, the corrupted drones dragged Mei towards the violet honey.[P] Muscle, mind, and spirit all recoiled at the thought of what was to come, but their grip was strong.[P] There would be no escape.[B][C]") ]])
fnCutscene([[ Append("Mei struggled despite her captors' grasp, struggling to buy herself another second,[P] another moment,[P] another hope that would bring rescue closer.") ]])
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/Mei/ZombeeTF0", "Root/Images/Scenes/Mei/ZombeeTF1") ]])
fnCutscene([[ Append("The frothing honey seemed to simmer in anticipation as the drones dragged her to the precipice, and...[P][P] let her go.") ]])
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/Mei/ZombeeTF1", "Root/Images/Scenes/Mei/ZombeeTF2") ]])
fnCutscene([[ Append("Surely this was not to be her fate.[P] Surely someone would come and save her.[P] Surely...") ]])
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/Mei/ZombeeTF2", "Root/Images/Scenes/Mei/ZombeeTF3") ]])
fnCutscene([[ Append("Mei raised her head.[P] Unsure if this was part of their plan or something unexpected, she gingerly tried to stand.[P] As she rose to her feet, the drones shoved her forward.") ]])
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/Mei/ZombeeTF3", "Root/Images/Scenes/Mei/ZombeeTF4") ]])
fnCutscene([[ Append("Now, she could see why she had been brought here.[P] The corrupted honey was not some casualty of this place.[P] It was the corruption.") ]])
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/Mei/ZombeeTF4", "Root/Images/Scenes/Mei/ZombeeTF5") ]])
fnCutscene([[ Append("From within the corrupted honey, something stirred.[P] It seemed as if a hand pressed upwards against the viscous surface, but the honey held naught but a few trapped bubbles.[P] The ghostly hand pressed, lingered, and then subsided.[B][C]") ]])
fnCutscene([[ Append("As if sensing her presence, the hands stirred again, reforming. Becoming larger. Grasping for something. Her.[B][C]") ]])
fnCutscene([[ Append("A feeling of dread clutched at her mind as her feelers began to hear...[P] something.[P] Ideas that were neither hers nor those of her hive began to trickle in.[P] She could not comprehend them, they were beyond her, but she could feel them taking root in her mind...") ]])
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/Mei/ZombeeTF5", "Root/Images/Scenes/Mei/ZombeeTF6") ]])
fnCutscene([[ Append("...") ]])
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/Mei/ZombeeTF6", "Root/Images/Scenes/Mei/ZombeeTF7") ]])
fnCutscene([[ Append("...") ]])
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/Mei/ZombeeTF7", "Root/Images/Scenes/Mei/ZombeeTF8") ]])
fnCutscene([[ Append("...") ]])
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/Mei/ZombeeTF8", "Null") ]])
fnCutscene([[ Append("The world went dark.[P] Panic rose within her as Mei looked around frantically, but saw nothing.[P] An empty void, stretching into the infinite, surrounded her.[B][C]") ]])
fnCutscene([[ Append("She tried to run, but her legs would not carry her.[P] Fear gripped her mind, enthralled her body, and held her fast.[B][C]") ]])
fnCutscene([[ Append("Only when she looked at herself did she realize she was wet with sweat.[P] She wiped it from her brow, and looked at her hand.[P] Though under her full control, it seemed to belong to someone else.[P] It seemed alien.[B][C]") ]])
fnCutscene([[ Append("Transfixed on her hand, she found herself wishing to be back at her home, or the outpost, or with her drone sisters.[P] Anywhere but this cursed place.[B][C]") ]])
fnCutscene([[ Append("When at last she looked up again, the empty void had changed.[P] Stars.[P] The stars of the sky looked back at her.[P] Had she been taken outside?[P] She could not feel the wind on her wings.[B][C]") ]])
fnCutscene([[ Append("Was she underground?[P] No, she was not.[P] She could see the stars.[P] She could see them.[B][C]") ]])
fnCutscene([[ Append("All around her, filling the void.[P] The lights that filled her vision...[P] The lights...[P] The stars...[P] were not stars...[B][C]") ]])
fnCutscene([[ Append("One by one, the stars began to rotate around her.[P] Then, one of them blinked.[P] Soon, more blinked.[P] Now, many stars blinked in unison.[B][C]") ]])
fnCutscene([[ Append("Not stars, but eyes...[P] the eyes of some enormous creature that stretched across the sky and filled the void around her.[B][C]") ]])
fnCutscene([[ Append("Mei's mouth fell open.[P] She tried to call out.[P] She tried to scream.[B][C]") ]])
fnCutscene([[ Append(".[P].[P].[P]s[P]c[P]r[P]e[P]a[P]m[P].[P].[P].[B][C]") ]])
fnCutscene([[ Append("But the void[P] remained silent.[B][C]") ]])
fnCutscene([[ Append("She felt at her mouth, but her hands would no longer follow her commands.[B][C]") ]])
fnCutscene([[ Append("Helpless, terrified, wanting to scream.[P] Silent.[P] Mei could do nothing as the multitude of eyes continued to stare at her.[B][C]") ]])
fnCutscene([[ Append("Then, she felt something prickling at the tender flesh between her legs...[B][C]") ]])
fnCutscene([[ Append("A hand had grabbed her sex, squeezing it.[P] Where the fingers went, a feeling of pure hatred emanated through her flesh.[P] Mei tried to stand,[P] to run ...[P] fight ...[P] but she could not.[P] She was helpless as the hand penetrated her.") ]])
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Transition Scene", "Null", "Root/Images/Scenes/Mei/ZombeeTF9") ]])
fnCutscene([[ Append("It thrust into her, and more thoughts flooded into her mind.[P] She could hear them.[P] She could not understand them.[P] She knew now that she was small, worthless, and...[P] a part of something bigger and grander.") ]])
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/Mei/ZombeeTF9", "Root/Images/Scenes/Mei/ZombeeTFA") ]])
fnCutscene([[ Append("As the hand-[P] as the fingers-[P] As the hatred continued to massage her exposed clitoris, her enfeebled mind was overrun by these new feelings pouring into her.[P] She accepted them.[P] She must accept them.") ]])
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/Mei/ZombeeTFA", "Null") ]])
fnCutscene([[ Append("She was small, worthless, and deserved to be controlled.[B][C]") ]])
fnCutscene([[ Append("The malice that groped her began to change,[P] to feel[P] pleasurable.[P] It was still the hand of hatred, but that was what Mei wanted.[B][C]") ]])
fnCutscene([[ Append("Needed.[B][C]") ]])
fnCutscene([[ Append("She needed it to delve into the deepest reaches of her womanhood.[P] To fill her.[P] And,[P] it did.") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(120)
fnCutsceneBlocker()

--Scene again. Stay strong, Mei!
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ Append("The very essence of time had slipped from her mind.[P] Had she been here for hours?[P] Days?[P] She could not guess.[P] She could not even summon the will to try.[P] The hand had taken her, completely and fully.[P] It left its handprint on her vagina as it receded from her.[B][C]") ]])
fnCutscene([[ Append("Panting, sweating, her thoughts lay barren.[P] The hand had raped her mind as well as her body.[P] There was no Mei.[P] There was only the hand.[P] The Hand.[P] She smiled.[P] This was what she wanted.[P] This was what she needed.[B][C]") ]])
fnCutscene([[ WD_SetProperty("Transition Scene", "Null", "Root/Images/Scenes/Mei/ZombeeTFB") ]])
fnCutscene([[ Append("The world returned to her as the ephemeral Hand slid silently back into the corrupted honey.[P] Now, the drones near her would not stop her.[P] She rose to her feet and gingerly touched her sex, glistening with fresh honey.[P] The Handprint was there, and the Handprint was her whole being.[B][C]") ]])
fnCutscene([[ Append("She turned to leave the room and bring all other creatures of Pandemonium to heel before her unknown, unknowable master...") ]])
fnCutsceneBlocker()

--Activate a fade in.
fnCutscene([[ AL_SetProperty("Activate Fade", 120, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])

--Switch Mei to the MC frames.
fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Zombee.lua") ]])
fnCutsceneBlocker()

--Remove Mei from the crouch pose.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Special Frame", "Null")
DL_PopActiveObject()

-- |[Dialogue Sequence]|
--Dialogue setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])

--Talking.
fnCutscene([[ Append("Drone:[VOICE|Bee][E|MC] (Drone obeys.[P] Drone obeys.[P] Capture bees.[P] Convert bees.[P] Drone obeys.[P] Drone obeys.)") ]])
fnCutsceneBlocker()

-- |[ ======================================== Clean Up ======================================== ]|
-- |[Unload Images]|
fnCutscene([[ fnUnloadBitmapsFromList("Chapter 1 Mei Zombee TF") ]])

-- |[Special]|
--If this is a reliving sequence, end it here.
if(iIsRelivingScene == 1.0) then
	
	--Wait a bit.
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--Return to the last save point and execute the post-script..
	fnCutscene([[ AL_BeginTransitionTo("LASTSAVE", gsStandardReliveEnd) ]])
	fnCutsceneBlocker()

-- |[Finish Up]|
--Mei is not reliving this, it's real!
else

    AdvCombat_SetProperty("Push Party Member", "Mei")
        AdvCombatEntity_SetProperty("Display Name", "Drone")
    DL_PopActiveObject()
end
