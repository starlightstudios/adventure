-- |[Special]|
--Opens a debug vendor that will sell you all the gems in the game for free.

--For now, just give one of each gem.
LM_ExecuteScript(gsItemListing, "Glintsteel Gem")
LM_ExecuteScript(gsItemListing, "Yemite Gem")
LM_ExecuteScript(gsItemListing, "Ardrion Gem")
LM_ExecuteScript(gsItemListing, "Rubose Gem")
LM_ExecuteScript(gsItemListing, "Blurleen Gem")
LM_ExecuteScript(gsItemListing, "Qederphage Gem")
LM_ExecuteScript(gsItemListing, "Pirofine Gem")
LM_ExecuteScript(gsItemListing, "Arctofine Gem")
