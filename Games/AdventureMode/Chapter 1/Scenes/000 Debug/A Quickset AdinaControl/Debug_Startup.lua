-- |[Special]|
--Quickly sets variables such that Mei has met Adina, was enslaved by her, and worked for at least 2 days there. This changes some of their dialogue.
VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasMetAdina", "N", 10.0)
VM_SetVar("Root/Variables/Chapter1/SaltFlats/iTasksDoneTotal", "N", 30)
VM_SetVar("Root/Variables/Chapter1/SaltFlats/iDaysPassed", "N", 3)
VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiLovesAdina", "N", 1.0)
