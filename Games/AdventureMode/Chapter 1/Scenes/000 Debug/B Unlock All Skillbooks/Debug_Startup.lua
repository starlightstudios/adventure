-- |[ ================================= Unlock All Skillbooks ================================== ]|
--Specifies that all skillbooks are unlocked for this chapter.
local iMeiSkillbook = VM_GetVar("Root/Variables/Global/Mei/iSkillbookTotal", "N") 
if(iMeiSkillbook < 4) then
    VM_SetVar("Root/Variables/Global/Mei/iSkillbookTotal", "N", 4.0)
end

--Florentina.
local iFlorentinaSkillbook = VM_GetVar("Root/Variables/Global/Florentina/iSkillbookTotal", "N") 
if(iFlorentinaSkillbook < 4) then
    VM_SetVar("Root/Variables/Global/Florentina/iSkillbookTotal", "N", 4.0)
end

--Debug:
io.write("Skillbooks unlocked. This will not appear until the next time you change jobs, or save and load the game.\n")