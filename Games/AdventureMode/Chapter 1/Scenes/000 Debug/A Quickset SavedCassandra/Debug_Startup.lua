-- |[Special]|
--Quickly sets it such that Mei has rescued Cassandra.
VM_SetVar("Root/Variables/Chapter1/Scenes/iEncounteredCatCampsite", "N", 1.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N", 2.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/sCassandraLocation", "S", "None")
VM_SetVar("Root/Variables/Chapter1/Scenes/iSavedCassandra", "N", 1.0)
