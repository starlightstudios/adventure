-- |[ =================================== Quickadd Florentina ================================== ]|
--This instantly adds Florentina to the party, along with attendant variables. Does nothing if she's already in the party.
for i = 1, giFollowersTotal, 1 do
	if(gsaFollowerNames[i] == "Florentina") then
		return
	end
end

-- |[Field Entity]|
--Create if she doesn't exist.
if(EM_Exists("Florentina") == false) then
	fnSpecialCharacter("Florentina", -100, -100, gci_Face_South, false, nil)
end

--Lua globals.
giFollowersTotal = 1
gsaFollowerNames = {"Florentina"}
giaFollowerIDs = {0}

--Get Florentina's uniqueID. 
EM_PushEntity("Florentina")
	local iFlorentinaID = RE_GetID()
DL_PopActiveObject()

--Store it and tell her to follow.
giaFollowerIDs = {iFlorentinaID}
AL_SetProperty("Follow Actor ID", iFlorentinaID)

-- |[Combat Lineup]|
--Place Florentina in the combat lineup.
AdvCombat_SetProperty("Party Slot", 1, "Florentina")

--Florentina's equipment
if(gbHasFlorentinasEquipment == false) then
	gbHasFlorentinasEquipment = true
	LM_ExecuteScript(gsItemListing, "Hunting Knife")
	LM_ExecuteScript(gsItemListing, "Flowery Tunic")
	LM_ExecuteScript(gsItemListing, "Florentina's Pipe")
	AdvCombat_SetProperty("Push Party Member", "Florentina")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Weapon", "Hunting Knife")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Body", "Flowery Tunic")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Accessory A", "Florentina's Pipe")
	DL_PopActiveObject()
end

--Party Folding
AL_SetProperty("Fold Party")

--Normalize her EXP with Mei's. She will have 80% to 120% of her EXP and JP.
AdvCombat_SetProperty("Push Party Member", "Mei")
    local iMeiExp = AdvCombatEntity_GetProperty("Exp")
    local iMeiJP  = AdvCombatEntity_GetProperty("Total JP")
DL_PopActiveObject()

--Set.
AdvCombat_SetProperty("Push Party Member", "Florentina")

    --Florentina's EXP scatter.
    local fEXPRoll = LM_GetRandomNumber(80, 120) / 100.0
    AdvCombatEntity_SetProperty("Current Exp", math.floor(iMeiExp * fEXPRoll))
    
    --JP. No scatter.
    AdvCombatEntity_SetProperty("Current JP", math.floor(iMeiJP))

DL_PopActiveObject()

-- |[Dialogue and Script]|
--Unlock dialogue topics.
WD_SetProperty("Unlock Topic", "Florentina", 1)
WD_SetProperty("Unlock Topic", "Claudia", 1)
WD_SetProperty("Unlock Topic", "OutlandFarm", 1)
WD_SetProperty("Unlock Topic", "Pandemonium", 1)

--Script variables that normally must be set to meet Florentina. It is assumed Florentina met Mei as a human.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N", 1.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N", 1.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/sTrannadarFirstSceneForm", "S", "Human")
