-- |[Special]|
--Start rubber mode.
VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N", 1.0)

--Switch Mei's form to rubber.
LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Rubber.lua")

--Remove Florentina.
if(EM_Exists("Florentina") == true) then
    fnRemovePartyMember("Florentina", true)
end

--Skip ahead.
fnCutscene([[ AL_BeginTransitionTo("StarfieldSwampF", "FORCEPOS:2.0x38.0x0") ]])
fnCutsceneBlocker()
