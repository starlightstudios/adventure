-- |[ ========================= Debug: Quickset Completed Mycela Quest ========================= ]|
--Immediately sets all variables related to the mushraune quest as completed.
VM_SetVar("Root/Variables/Chapter1/Sharelock/iSawMushraunes", "N", 1.0)
VM_SetVar("Root/Variables/Chapter1/Sharelock/iMetMycela", "N", 1.0)
VM_SetVar("Root/Variables/Chapter1/Sharelock/iDefeatedMycela", "N", 1.0)
VM_SetVar("Root/Variables/Chapter1/Sharelock/iUnlockedStForasBack", "N", 1.0)
