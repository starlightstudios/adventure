-- |[Trannadar Second Scene: Alraune]|
--If Nadia first met Mei as a human, and then Mei entered as an Alraune...
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarSecondScene", "N", 1.0)

--Determine which side the player entered from.
local bIsPlayerAtBottom = false
local bIsPlayerAtTop = false
EM_PushEntity("Adventure Party", 0)
	local iPlayerX, iPlayerY = TA_GetProperty("Position")
DL_PopActiveObject()
if(iPlayerY >= 37 * 16) then bIsPlayerAtBottom = true end
if(iPlayerY <   7 * 16) then bIsPlayerAtTop = true end

--Wait a bit for the fade.
fnCutsceneWait(15)
fnCutsceneBlocker()

--Mei walks towards Nadia. This is based on where she spawned.
local iHasSeenTrannadarFlorentinaScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
if(bIsPlayerAtBottom) then
	
	--Move Mei.
	Cutscene_CreateEvent("Move Mei North", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (38.50 * gciSizePerTile))
	DL_PopActiveObject()
	
	--If Florentina is present, move her as well.
	if(iHasSeenTrannadarFlorentinaScene == 1.0) then
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (39.50 * gciSizePerTile))
		DL_PopActiveObject()
	end

	--Common.
	fnCutsceneBlocker()

elseif(bIsPlayerAtTop) then
	Cutscene_CreateEvent("Move Mei", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (23.25 * gciSizePerTile), (6.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--If Florentina is present, move her as well.
	if(iHasSeenTrannadarFlorentinaScene == 1.0) then
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (24.25 * gciSizePerTile), (6.50 * gciSizePerTile))
		DL_PopActiveObject()
	end
	fnCutsceneBlocker()
else
	--Move Mei.
	Cutscene_CreateEvent("Move Mei West", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (43.25 * gciSizePerTile), (28.50 * gciSizePerTile))
	DL_PopActiveObject()
	
	--If Florentina is present, move her as well.
	if(iHasSeenTrannadarFlorentinaScene == 1.0) then
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (44.25 * gciSizePerTile), (28.50 * gciSizePerTile))
		DL_PopActiveObject()
	end
	
	--Common.
	fnCutsceneBlocker()
end

--Change the music.
fnCutscene([[ AudioManager_PlayMusic("NadiasTheme") ]])

--Wait a bit.
fnCutsceneWait(5)
fnCutsceneBlocker()

--Dialogue setup.
fnStandardMajorDialogue()
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Nadia", "Neutral") ]])

--Talking.
fnCutscene([[ Append("Nadia: Mei?[P] Mei![P] Is that you?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] Nadia....[P] you...[P] killed...[P] meee.....[B][C]") ]])
fnCutscene([[ Append("Nadia: !!![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Angry] The dead demand vengeance![P] You will join me in eternal servitude![B][C]") ]])
fnCutscene([[ Append("Nadia: EEEEEK![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Laugh] Ha ha![P] I'm just goofing, Nadia.[P] Everything is fine.[B][C]") ]])
fnCutscene([[ Append("Nadia: EEEEEEEEEEEEEEEEEEE!!![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Uhhh...[B][C]") ]])
fnCutscene([[ Append("Nadia: Mei![P] I love your outfit so much![P] It's the cutest thing ever![P] Where did you get it?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] I'm a ghost, Nadia.[P] I'm dead...[B][C]") ]])
fnCutscene([[ Append("Nadia: So did you get it at a ghost shop or something?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] Yeah, that's it.[P] The ghost shop.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] This is obviously not getting through to you.[B][C]") ]])
fnCutscene([[ Append("Nadia: Well you seem happy enough, so being dead can't be all bad.[P] Hey, how did you get your hair like that?[P] I love it![P] Is it a special shampoo?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] *sigh*") ]])
fnCutsceneBlocker()

--Change the music.
fnCutscene([[ AudioManager_PlayMusic("TownTheme") ]])
fnCutsceneBlocker()

--Move Florentina onto Mei, fold the party.
if(iHasSeenTrannadarFlorentinaScene == 1.0) then
	
	--Bottom.
	if(bIsPlayerAtBottom) then
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (38.50 * gciSizePerTile))
		DL_PopActiveObject()

    elseif(bIsPlayerAtTop) then
        Cutscene_CreateEvent("Move Mei", "Actor")
            ActorEvent_SetProperty("Subject Name", "Florentina")
            ActorEvent_SetProperty("Move To", (23.25 * gciSizePerTile), (6.50 * gciSizePerTile))
        DL_PopActiveObject()
        fnCutsceneBlocker()
	
	--Right.
	else
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (43.25 * gciSizePerTile), (28.50 * gciSizePerTile))
		DL_PopActiveObject()
	end
	fnCutsceneBlocker()

	--Fold the party positions up.
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
end
