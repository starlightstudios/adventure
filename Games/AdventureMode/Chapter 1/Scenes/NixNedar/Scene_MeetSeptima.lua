-- |[Meeting Septima]|
--Cutscene where Mei meets Septima and the Rilmani.

-- |[Topics]|
--Unlock these topics if they weren't already.
--None yet!

-- |[Music]|
AL_SetProperty("Music", "Null")

-- |[Movement]|
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (20.25 * gciSizePerTile), (20.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 0, -1)
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait.
fnCutsceneWait(30)
fnCutsceneBlocker()

--RilmaniG turns around.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniG")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])

--Talking.
fnCutscene([[ Append("Mei:[E|Neutral] (...)[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] (Uh oh, I don't actually know how to [P]*speak*[P] Rilmani![P] This just shows what the words are, not how to say them!)") ]])
fnCutsceneBlocker()

-- |[Movement]|
--Rilmani move towards Mei.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniB")
	ActorEvent_SetProperty("Move To", (11.25 * gciSizePerTile), (18.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniB")
	ActorEvent_SetProperty("Move To", (18.25 * gciSizePerTile), (18.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Mei looks left.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", -1, -1)
DL_PopActiveObject()

--Rilmani on the right move towards Mei.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniE")
	ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (17.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniE")
	ActorEvent_SetProperty("Move To", (22.25 * gciSizePerTile), (17.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniF")
	ActorEvent_SetProperty("Move To", (22.25 * gciSizePerTile), (18.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Mei looks right.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 1, -1)
DL_PopActiveObject()

--Wait a bit.
fnCutsceneWait(120)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Offended") ]])

--Talking.
fnCutscene([[ Append("Mei:[E|Offended] (Oh jeez, I hope I didn't tick them off...)") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(120)
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 0, -1)
DL_PopActiveObject()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])

--Talking.
fnCutscene([[ Append("Voice:[VOICE|Septima][P] And the cloud billowed forth, swallowing them in time.[B][C]") ]])
fnCutscene([[ Append("Voice:[VOICE|Septima][P] But the darkness waned, for as the cloud grew, so too did the star.[P] Six-pointed, it shone brighter than all the others.[B][C]") ]])
fnCutscene([[ Append("Voice:[VOICE|Septima][P] With its majesty, the third age died.[P] Nothing would be written on its grave, save the inscription::[P] 'Voidwalker'.") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Movement]|
--The Rilmani look north.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniA")
	ActorEvent_SetProperty("Face", 0, -1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniB")
	ActorEvent_SetProperty("Face", 0, -1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniE")
	ActorEvent_SetProperty("Face", 0, -1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniF")
	ActorEvent_SetProperty("Face", 0, -1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniG")
	ActorEvent_SetProperty("Face", 0, -1)
DL_PopActiveObject()
fnCutsceneWait(30)
fnCutsceneBlocker()

--Septima moves south.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Septima")
	ActorEvent_SetProperty("Move To", (20.25 * gciSizePerTile), (15.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(70)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Septima", "Neutral") ]])

--Talking.
fnCutscene([[ Append("Rilmani: The bindings came loose, and certainty died.[P] What was written was erased, and what was erased was forgotten.[P] So cometh the ender, and the forerunner.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] ...[P] Excuse me?[B][C]") ]])
fnCutscene([[ Append("Rilmani: Valor.[P] Courage.[P] Bravery.[B][C]") ]])
fnCutscene([[ Append("Rilmani: ...[P] Mei.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Surprise] ..![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Surprise] You know my name?[B][C]") ]])
fnCutscene([[ Append("Rilmani: We have known of you before your one-thousandth ancestor was born.[P] Your coming is the sole strand of unbreakable destiny.[P] We, the Rilmani, are at your service.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Well at least it's a warm welcome...[P] of sorts.[P] You guys don't smile much, do you?[B][C]") ]])
fnCutscene([[ Append("Rilmani: ...[B][C]") ]])
fnCutscene([[ Append("Rilmani: The voidwalker tells a joke![P] Confidence surges![B][C]") ]])
fnCutscene([[ Append("[P]*The assembled Rilmani murmur assent, but aren't smiling or laughing...*[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Happy] (Well they [P]*sound*[P] happy, so that's good...)[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] So, uh,[P] where am I?[P] Who are you?[B][C]") ]])
fnCutscene([[ Append("Septima: You are in Nix Nedar, the space in no space.[P] This is the home of the Rilmani.[P] I am Septima, oldest among the unborn.[B][C]") ]])
fnCutscene([[ Append("Septima: Doubtless you will have many questions.[P] Please, come to my home when you are ready.[P] We have much to discuss.[B][C]") ]])
fnCutscene([[ Append("Septima: And, please introduce yourself to the unborn here.[P] It will do much to lift their spirits!") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Septima moves north, and teleports off the map.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Septima")
	ActorEvent_SetProperty("Move To", (20.25 * gciSizePerTile), (7.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Septima")
	ActorEvent_SetProperty("Teleport To", (-100.25 * gciSizePerTile), (-100.50 * gciSizePerTile))
DL_PopActiveObject()

fnCutsceneWait(30)
fnCutsceneBlocker()

Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniA")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniB")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniE")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniF")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniG")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()

fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])

--Talking.
fnCutscene([[ Append("Mei:[E|Neutral] ...[P] This just keeps getting weirder and weirder...") ]])
fnCutsceneBlocker()

-- |[Movement]|
--Rilmani B:
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniB")
	ActorEvent_SetProperty("Move To", (11.25 * gciSizePerTile), (18.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniB")
	ActorEvent_SetProperty("Move To", (11.25 * gciSizePerTile), (16.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniB")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()

--Rilmani E:
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniE")
	ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (17.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniE")
	ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (16.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniE")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()

--Rilmani F:
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniF")
	ActorEvent_SetProperty("Move To", (30.25 * gciSizePerTile), (18.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniF")
	ActorEvent_SetProperty("Face", 1, 0)
DL_PopActiveObject()

--Rilmani G:
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "RilmaniG")
	ActorEvent_SetProperty("Face", -1, 0)
DL_PopActiveObject()
fnCutsceneBlocker()

-- |[Music]|
fnCutscene([[ AL_SetProperty("Music", "NixNedar") ]])
fnCutsceneBlocker()
