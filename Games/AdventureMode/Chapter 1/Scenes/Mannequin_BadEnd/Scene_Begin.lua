-- |[ =================================== Mannequin Bad End ==================================== ]|
--Bad end if the player loses the Victoria boss fight. This is the part that is a single text
-- box after the defeat script.

-- |[Image Streaming]|
fnLoadDelayedBitmapsFromList("Chapter 1 Mannequin Bad End", gciDelayedLoadLoadAtEndOfTick)

-- |[ ==================================== Scene Execution ===================================== ]|
-- |[Dialogue]|
--Common.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ Append("A sweltering, oppressive heat overcame Victoria as her body finished the group teleportation spell.[P] She knew how to teleport, blink, warp, and shadowstep.[P] Any good mage did.[B][C]") ]])
fnCutscene([[ Append("This spell, however, was fundamentally different.[P] Primitive, and infused with emotion.[P] Mages tried to keep emotion out of their spells.[P] This was not her spell, her body acted against her.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Victoria", "Mannequin") ]])
fnCutscene([[ Append("Victoria:[E|Mannequin] (What is the meaning of this?[P] Whoever thinks they can command the great Victoria has another thing coming![P] I'll show you!)[B][C]") ]])
fnCutscene([[ Append("Victoria:[E|Mannequin] (I seem to be with the other mannequins.[P] We all teleported but I have no idea where.[P] I can barely move my head.)[B][C]") ]])
fnCutscene([[ Append("Victoria:[E|Mannequin] (One -[P] little -[P] move -[P] at a time...)") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Null", "Root/Images/Scenes/MannBadEnd/MannBadEnd0") ]])
fnCutscene([[ Append("As Victoria struggled to command her transformed body, she could see before her a vast cavern full of strange structures.[P] It stretched off into the distance in every direction she could see.[B][C]") ]])
fnCutscene([[ Append("Structures made of stone, with millions of mannequins inhabiting them.[P] The mannequins were turning great gears and machines made of stone, or hauling objects to and fro.[B][C]") ]])
fnCutscene([[ Append("Some structures were stone monuments stacked on top of one another, covered in mannequins working on them.[P] Carving on the stone, only for the carvings to vanish and need to be reapplied.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/MannBadEnd/MannBadEnd0", "Null") ]])
fnCutscene([[ Append("Cubic machines made of stone, rotating and grinding, produced other stones in smaller shapes.[P] A grand stone factory, run by mannequins.[P] Victoria stood on a plateau in the midst of this great underground city.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Victoria", "Mannequin") ]])
fnCutscene([[ Append("Victoria:[E|Mannequin] (Clearly I've stumbled upon a great find, though what value it has remains to be seen.[P] Surely there is endless treasure here!)[B][C]") ]])
fnCutscene([[ Append("Victoria:[E|Mannequin] (Not to mention a fountain of arcane knowledge.[P] I've never even heard of a stone machine like that, to replicate it...[P] I will be hailed as the greatest wizard to ever live!)[B][C]") ]])
fnCutscene([[ Append("Victoria:[E|Mannequin] (Huh?[P] No, no no no, stay away![P] Stay away![P] You don't control me, I'm the greatest mind of a hundred generations![P] You ought to be bowing before me!)") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/MannBadEnd/MannBadEnd1", "Root/Images/Scenes/MannBadEnd/MannBadEnd1") ]])
fnCutscene([[ Append("At once, Victoria's gaze was entirely transfixed and consumed by a glowing pair of eyes in the distance.[P] Like a being hewn of the same rock being fashioned into buildings, all she could see was two great, overawing stone eyes.[B][C]") ]])
fnCutscene([[ Append("Everything she was was lost inside the powerful glare, it consumed her like it consumed and dominated everyone who saw it.[P] Victoria forgot everything.[P] She forgot Victoria.[P] There was only the great eyes.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/MannBadEnd/MannBadEnd1", "Null") ]])
fnCutscene([[ Append("Now everything was entirely, irrevocably clear for all eternity.") ]])
fnCutsceneWait(25)
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Victoria", "Mannequin") ]])
fnCutscene([[ Append("Victoria:[E|Mannequin] (It is nothing.[P] Purged.[P] Fill.)[B][C]") ]])
fnCutscene([[ Append("Victoria:[E|Mannequin] (It is filled with instructions.[P] It will command the lesser joints.[P] It has no will, it does as ordered.[P] It is nothing.)[B][C]") ]])
fnCutscene([[ Append("Victoria:[E|Mannequin] (It orders them to move.[P] It orders them to turn the gears.[P] It orders them to move.[P] It orders them to turn the gears...)[B][C]") ]])
fnCutscene([[ Append("Victoria:[E|Mannequin] ...") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Null", "Null") ]])
fnCutscene([[ Append("The mannequins did as they were ordered, joining the faceless millions to turn the stone gears.[P] Forever.") ]])
fnCutsceneWait(25)
fnCutsceneBlocker()
