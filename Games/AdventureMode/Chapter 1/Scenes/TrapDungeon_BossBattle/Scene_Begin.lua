-- |[Boss Battle]|
--Battle against the pure ones.

-- |[Spawning]|
--Spawn a cultist.
TA_Create("Cultist")
	TA_SetProperty("Position", 9, 18)
	TA_SetProperty("Clipping Flag", true)
	fnSetCharacterGraphics("Root/Images/Sprites/CultistM/", false)
	TA_SetProperty("Facing", gci_Face_North)
	TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/CultistF|Wounded")
DL_PopActiveObject()

-- |[Movement]|
--Mei and Florentina move up a bit.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (10.25 * gciSizePerTile), (20.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (10.25 * gciSizePerTile), (19.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (10.25 * gciSizePerTile), (20.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(120)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Offended") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Confused") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "CultistM", "Neutral") ]])

--Talking.
fnCutscene([[ Append("Mei:[E|Offended] ![B][C]") ]])
fnCutscene([[ Append("Cultist: We were wrong.[P] The hole wasn't a hole.[P] It was watching us.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] We're...[P] too late...[P] aren't we?[B][C]") ]])
fnCutscene([[ Append("Cultist: It's all so obvious.[P] It was between the between.[B][C]") ]])
fnCutscene([[ Append("Cultist: Don't you get it?[P] No divisions.[P] The desire was right,[P] because we desired it to be right.[B][C]") ]])
fnCutscene([[ Append("Cultist: They all add to one.[P] The line is a circle.[P] We're in between.[P] You can't leave if you never arrived.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] Shut up, or I'll shut you up.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] (This guy...[P] he's making total sense to me...[P] Why?)[B][C]") ]])
fnCutscene([[ Append("Cultist: It's the part we don't see.[P] I'm a crack.[P] Crack[P] nook[P] space.[P] Everything with something filling it.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] Shut up,[P] or I'll give you a crack upside the head![B][C]") ]])
fnCutscene([[ Append("Cultist: Hee hee hee![P] Look![P] I'm already empty!") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(90)
fnCutsceneBlocker()

-- |[Movement]|
--Cultist falls over.
fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Cultist")
	ActorEvent_SetProperty("Special Frame", "Wounded")
DL_PopActiveObject()
fnCutsceneWait(90)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Sad") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Confused") ]])

--Talking.
fnCutscene([[ Append("Florentina:[E|Confused] He's...[P] not bleeding...[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] He was empty inside.[P] He is empty...[P] I am empty.[P] I was always empty...[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Surprise] [P]*Not*[P] the right time for a pity party![B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Surprise] Mei![P] MEI![P] Look at that thing![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Surprise] !!!!![B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Surprise] Weapons up![P] Right now!") ]])
fnCutsceneBlocker()

-- |[Battle]|
fnCutsceneWait(30)
fnCutsceneBlocker()
fnCutscene([[
	AdvCombat_SetProperty("World Pulse", true)
	AdvCombat_SetProperty("Next Combat Music", "RottenTheme", 0.0000)
	AdvCombat_SetProperty("Reinitialize") 
	AdvCombat_SetProperty("Activate") 
	AdvCombat_SetProperty("Unretreatable", true)
	LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/000 Scripted Enemies/Boss Infirm.lua", 0)
	AdvCombat_SetProperty("Victory Script", gsRoot .. "Chapter 1/Scenes/TrapDungeon_BossBattle/Combat_Victory.lua")
	AdvCombat_SetProperty("Defeat Script",  gsStandardGameOver)
]])
fnCutsceneBlocker()
