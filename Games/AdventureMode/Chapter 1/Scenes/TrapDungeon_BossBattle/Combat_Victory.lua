-- |[Combat Victory]|
--The party won! Or, did they?

-- |[Setup]|
--Flag.
VM_SetVar("Root/Variables/Chapter1/Scenes/iCompletedTrapDungeon", "N", 2.0)
		
--Reset flag.
WD_SetProperty("Clear Topic Read", "Cultists")

--Darken the screen.
AL_SetProperty("Activate Fade", 30, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1)
AudioManager_PlayMusic("Null")

-- |[Achievement]|
AM_SetPropertyJournal("Unlock Achievement", "FinishDungeon")

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])

--Talking.
fnCutscene([[ Append("Mei:[E|Offended] *pant*[P] *pant*[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] That...[P] thing...[P] *pant*[P] wasn't even at full strength...[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] What the hell was that!?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] Whatever it was, it's dead.[P] Burn the body.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] *sniff*[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Surprise] No![P] These barrels are full of gunpowder, genius![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] Why?[P] What would they need with so much gunpowder?[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] ...[P] I can think of one reason.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] Insurance.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] They knew this would happen...[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] H-[P]hey.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] Did the body just move?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] B-b[P]-[P]but I cut it in two![B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] Well then cut it into four next time![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] F-[P]Florentina![P] Grab that barrel![P] The one that's leaking powder![B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] And then what?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Angry] RUN!") ]])
fnCutsceneBlocker()

--Wait a while.
fnCutsceneWait(120)
fnCutsceneBlocker()

--Dialogue resumes.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Happy") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Happy") ]])

--Talking.
fnCutscene([[ Append("Mei:[E|Offended][P] I've got the door![B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] Come on, come on, light![P] Light damn it![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] Stop mucking about, and light the damn powder![B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] Got it![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Angry] NOW RUN AND DON'T LOOK BACK!") ]])
fnCutsceneBlocker()

--Wait a while.
fnCutsceneWait(120)
fnCutsceneBlocker()

--Explosion sound effect.
fnCutscene([[ AudioManager_PlaySound("World|Explosion") ]])
fnCutsceneWait(15)
fnCutsceneBlocker()
fnCutscene([[ AudioManager_PlaySound("World|Explosion") ]])
fnCutsceneWait(5)
fnCutsceneBlocker()
fnCutscene([[ AudioManager_PlaySound("World|Explosion") ]])
fnCutsceneWait(15)
fnCutsceneBlocker()
fnCutscene([[ AudioManager_PlaySound("World|Explosion") ]])
fnCutsceneWait(10)
fnCutsceneBlocker()
fnCutscene([[ AudioManager_PlaySound("World|Explosion") ]])
fnCutsceneWait(5)
fnCutscene([[ AudioManager_PlaySound("World|Explosion") ]])
fnCutsceneWait(5)
fnCutscene([[ AudioManager_PlaySound("World|Explosion") ]])
fnCutsceneWait(5)
fnCutsceneBlocker()
fnCutscene([[ AudioManager_PlaySound("World|Explosion") ]])
fnCutsceneWait(120)
fnCutsceneBlocker()

-- |[Transition]|
--Scene resumes on the top floor of the beehive.
fnCutscene([[ AL_BeginTransitionTo("TrapDungeonA", gsRoot .. "Chapter 1/Scenes/TrapDungeon_BossBattle/Victory_PostTransition.lua") ]])
fnCutsceneBlocker()
