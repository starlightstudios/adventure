-- |[Scene Post-Transition]|
--Was this really a victory?
local sDialoguePath = gsRoot .. "Maps/TrapDungeon/TrapDungeonA/Dialogue.lua"

-- |[Spawning]|
TA_Create("AlrauneA")
	TA_SetProperty("Position", 14, 23)
	TA_SetProperty("Clipping Flag", true)
	fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
	TA_SetProperty("Facing", gci_Face_East)
    TA_SetProperty("Activation Script", sDialoguePath)
DL_PopActiveObject()
TA_Create("AlrauneB")
	TA_SetProperty("Position", 17, 23)
	TA_SetProperty("Clipping Flag", true)
	fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
	TA_SetProperty("Facing", gci_Face_West)
    TA_SetProperty("Activation Script", sDialoguePath)
DL_PopActiveObject()
TA_Create("Rochea")
	TA_SetProperty("Position", 16, 24)
	TA_SetProperty("Clipping Flag", true)
	fnSetCharacterGraphics("Root/Images/Sprites/Rochea/", false)
	TA_SetProperty("Facing", gci_Face_North)
    TA_SetProperty("Activation Script", sDialoguePath)
DL_PopActiveObject()

-- |[Setup]|
--Reposition the party way offscreen.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Teleport To", (-100.25 * gciSizePerTile), (-100.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Teleport To", (-100.25 * gciSizePerTile), (-100.50 * gciSizePerTile))
DL_PopActiveObject()

--Camera focuses on Rochea, unfade.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 1000)
	CameraEvent_SetProperty("Focus Actor Name", "Rochea")
DL_PopActiveObject()
fnCutsceneWait(120)
AL_SetProperty("Activate Fade", 120, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 0)
fnCutsceneBlocker()

-- |[Movement]|
--Wait a bit.
fnCutsceneWait(60)
fnCutsceneBlocker()

--Rochea moves north.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Rochea")
	ActorEvent_SetProperty("Move To", (16.25 * gciSizePerTile), (23.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneWait(60)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Rochea", "Neutral") ]])

--Talking.
fnCutscene([[ Append("Rochea: Mei, Florentina...[B][C]") ]])
fnCutscene([[ Append("Rochea: Please do not have sacrificed yourselves...[B][C]") ]])
fnCutscene([[ Append("Rochea: There was so much life left in you...") ]])
fnCutsceneBlocker()
fnCutsceneWait(120)
fnCutsceneBlocker()

-- |[Dialogue Again]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Florentina", "Happy") ]])

--Talking.
fnCutscene([[ Append("Florentina:[E|Happy] Getting all melodramatic?[P] Why am I not surprised!?") ]])
fnCutsceneBlocker()
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Movement]|
--Open the door.
fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
fnCutscene([[ AL_SetProperty("Open Door", "ToDungeonB") ]])
fnCutsceneWait(30)
fnCutsceneBlocker()

--Reposition Mei and Florentina to the door.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Teleport To", (16.25 * gciSizePerTile), (16.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Teleport To", (16.25 * gciSizePerTile), (16.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneWait(30)
fnCutsceneBlocker()

--Move our heroes out.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (15.75 * gciSizePerTile), (22.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (16.75 * gciSizePerTile), (22.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutscene([[ AL_SetProperty("Close Door", "ToDungeonB") ]])
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Rochea", "Neutral") ]])

--Talking.
fnCutscene([[ Append("Rochea: You live![P] We had feared - [P][CLEAR]") ]])
fnCutscene([[ Append("Mei:[E|Sad] Sorry, leaf-sister, but I don't much feel like celebrating.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] It got pretty rough, down there.[B][C]") ]])
fnCutscene([[ Append("Rochea: We heard an explosion.[P] Did the cultists succeed?[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] They did, but we killed...[P] it...[P] and sealed the place off.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] Nothing could have survived that blast.[P] We barely did.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] Yeah...[B][C]") ]])
fnCutscene([[ Append("Rochea: I see.[P] The looks on your faces suggest this was not the victory we had hoped for.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] This wasn't even the start.[P] This was...[P] a taunt.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] You were acting kinda weird down there, kid.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] I think I understand something without knowing it.[P] I feel the answers to questions I didn't ask.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] But, I can count on my friends to help.[P] I -[P] as long as we're there for each other, I know we can do it.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] Nature can take care of herself.[B][C]") ]])
fnCutscene([[ Append("Rochea: Indeed.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] Don't go thinking we're friends, Rochea.[P] This is it.[P] I'm washing my hands of this.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Can't you two make nice?[P] For my sake?[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] Well, you and the girls are good in a scrap.[P] Maybe you're worth a damn after all.[B][C]") ]])
fnCutscene([[ Append("Rochea: You are uncleansed, yet placed yourself at great risk for the sake of all.[P] Perhaps you, too, are worth a 'damn'.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] Yeah?[P] Great.[P] Mei, let's get out of here before Rochea starts thinking I don't despise her.[B][C]") ]])
fnCutscene([[ Append("Rochea: We shall tarry here a little longer to delay the cultists.[P] After that, we have...[P] many new sisters to join.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] You would join them even after what they've done?[B][C]") ]])
fnCutscene([[ Append("Rochea: They will find forgiveness among the daughters of the wild.[P] Further, this is likely not the last conflict.[B][C]") ]])
fnCutscene([[ Append("Rochea: Even with their help, we are outnumbered twenty to one.[P] We cannot afford to be choosy in a time of war.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] Then you need to get the humans on your side, too![P] Talk to Blythe![P] He will help![B][C]") ]])
fnCutscene([[ Append("Rochea: We -[P] we cannot trust them.[P] You are exceptions, clearly, but - [P][CLEAR]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] Some things just don't change.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] C'mon Mei.[P] Let's get out of here.") ]])
fnCutsceneBlocker()
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Party Fold]|
--Florentina walks onto Mei.
Cutscene_CreateEvent("Move Florentina to Mei", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (15.75 * gciSizePerTile), (22.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Fold the party positions up.
fnCutscene([[ AL_SetProperty("Fold Party") ]])
fnCutsceneBlocker()
