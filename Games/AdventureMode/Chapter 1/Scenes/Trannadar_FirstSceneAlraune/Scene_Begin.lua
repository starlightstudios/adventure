-- |[ ============================= Trannadar Intro Scene: Alraune ============================= ]|
--If Mei is a Wisphag the first time she enters Trannadar Trading Post, this scene plays.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N", 1.0)

-- |[Move In From Edge]|
--Determine which side the player entered from.
local bIsPlayerAtBottom = false
local bIsPlayerAtTop = false
EM_PushEntity("Adventure Party", 0)
	local iPlayerX, iPlayerY = TA_GetProperty("Position")
DL_PopActiveObject()
if(iPlayerY >= 37 * 16) then bIsPlayerAtBottom = true end
if(iPlayerY <   7 * 16) then bIsPlayerAtTop = true end

--Wait a bit for the fade.
fnCutsceneWait(15)
fnCutsceneBlocker()

-- |[Movement]|
--Mei walks towards Nadia and Blythe. This is based on where she spawned.
if(bIsPlayerAtBottom) then
    fnCutsceneMove("Mei", 28.25, 38.50)
elseif(bIsPlayerAtTop) then
    fnCutsceneMove("Mei", 23.25, 6.50)
else
    fnCutsceneMove("Mei", 43.25, 28.50)
end
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(5)
fnCutsceneBlocker()

--Change the music.
fnCutscene([[ AudioManager_PlayMusic("NadiasTheme") ]])

--Face Nadia and Blythe south.
if(bIsPlayerAtBottom) then
    fnCutsceneFace("Nadia", 0, 1)
    fnCutsceneFace("Blythe", 0, 1)

--Face north.
elseif(bIsPlayerAtTop) then
    fnCutsceneFace("Nadia", 0, -1)
    fnCutsceneFace("Blythe", 0, -1)
    
--Face them east.
else
    fnCutsceneFace("Nadia", 1, 0)
    fnCutsceneFace("Blythe", 1, 0)
end
fnCutsceneWait(5)
fnCutsceneBlocker()

-- |[Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Blythe", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Nadia", "Neutral") ]])
fnCutscene([[ Append("Man: ...[P] but that's before we get to all the backbiting.[B][C]") ]])
fnCutscene([[ Append("Alraune: Not again.[P] I said I was sorry![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Is something going on?[B][C]") ]])
fnCutscene([[ Append("Man: We have a visitor.[P] Do it properly this time.[B][C]") ]])
fnCutscene([[ Append("Alraune: All right, all right.[P] Hello, and welcome to Trannadar Trading Post.[P] I'm Nadia.[B][C]") ]])
fnCutscene([[ Append("Nadia: ... [B][C]") ]])
fnCutscene([[ Append("Nadia: ... [B][C]") ]])
fnCutscene([[ Append("Nadia: Line?[B][C]") ]])
fnCutscene([[ Append("Man: You need to tell her our security policy.[B][C]") ]])
fnCutscene([[ Append("Nadia: Oh yeah![P] No violence, that's our job.[P] We're allowed to beat people up but you aren't.[B][C]") ]])
fnCutscene([[ Append("Nadia: Was that right?[B][C]") ]])
fnCutscene([[ Append("Man: Would it kill you to be subtle about it?[B][C]") ]])
fnCutscene([[ Append("Nadia: Thanks, I think![B][C]") ]])
fnCutscene([[ Append("Man: I am Darius Blythe, captain of the guard here.[B][C]") ]])
fnCutscene([[ Append("Blythe: Our policy on Alraunes is rather lenient, but we won't tolerate violence.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Mei.[P] And,[P] I wouldn't dream of it.[B][C]") ]])
fnCutscene([[ Append("Nadia: Don't hesitate to ask if you need something, leaf-sister![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Actually, perhaps you could help me.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Have you ever heard of Earth?[B][C]") ]])
fnCutscene([[ Append("Nadia: Like, soil?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] Not a good sign.[B][C]") ]])
fnCutscene([[ Append("Blythe: Why are you looking for an Earth?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] It's where I'm from, and I'd like to be able to get back there.[B][C]") ]])
fnCutscene([[ Append("Nadia: You're taking this whole 'Plant Girl' thing even more literally than I do![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] Har-dee-har.[B][C]") ]])
fnCutscene([[ Append("Blythe: It's not something I've seen on any map.[P] Perhaps you should ask Florentina.[P] She runs the supply shop.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] That I will.[P] Thank you.") ]])
fnCutsceneBlocker()

-- |[Finish Up]|
--Change the music.
fnCutscene([[ AudioManager_PlayMusic("TownTheme") ]])
fnCutsceneBlocker()

--Unlock topics.
WD_SetProperty("Unlock Topic", "Alraunes", 1)
WD_SetProperty("Unlock Topic", "Blythe", 1)
WD_SetProperty("Unlock Topic", "Cultists", 1)
WD_SetProperty("Unlock Topic", "Florentina", 1)
WD_SetProperty("Unlock Topic", "Job", 1)
WD_SetProperty("Unlock Topic", "Nadia", 1)
WD_SetProperty("Unlock Topic", "Name", 1)
WD_SetProperty("Unlock Topic", "TradingPost", 1)
WD_SetProperty("Unlock Topic", "Challenge", 1)
