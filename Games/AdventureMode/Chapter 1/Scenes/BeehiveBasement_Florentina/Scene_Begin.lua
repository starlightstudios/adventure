-- |[Beehive Basement - Florentina]|
--Mei tries to enter the beehive basement mini-dungeon with Florentina in tow. The bees don't want to let her in,
-- but Florentina can be *very* convincing.

-- |[Variables]|
local sMeiForm                  = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local iHasBeeForm               = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")
local iFlorentinaKnowsAboutRune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")

-- |[Special Flags]|
--These Zombees will keep patrolling during the cutscene for dramatic effect.
EM_PushEntity("ZombeeA")
	TA_SetProperty("Set Ignore World Stop", true)
DL_PopActiveObject()
EM_PushEntity("ZombeeB")
	TA_SetProperty("Set Ignore World Stop", true)
DL_PopActiveObject()
EM_PushEntity("ZombeeC")
	TA_SetProperty("Set Ignore World Stop", true)
DL_PopActiveObject()
EM_PushEntity("ZombeeD")
	TA_SetProperty("Set Ignore World Stop", true)
DL_PopActiveObject()

-- |[Movement]|
--Move Mei to the blockade.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", 18.25 * gciSizePerTile, 23.50 * gciSizePerTile)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", 18.25 * gciSizePerTile, 24.50 * gciSizePerTile)
DL_PopActiveObject()

--Bees move to intercept!
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepA")
	ActorEvent_SetProperty("Move To", 17.25 * gciSizePerTile, 24.50 * gciSizePerTile)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepB")
	ActorEvent_SetProperty("Move To", 17.25 * gciSizePerTile, 23.50 * gciSizePerTile)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepC")
	ActorEvent_SetProperty("Move To", 17.25 * gciSizePerTile, 22.50 * gciSizePerTile)
DL_PopActiveObject()

--Turn to face Mei/Florentina.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepA")
	ActorEvent_SetProperty("Face", 1.0, 0.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepB")
	ActorEvent_SetProperty("Face", 1.0, 0.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepC")
	ActorEvent_SetProperty("Face", 0.0, 1.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", -1.0, 0.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", -1.0, 0.0)
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(35)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnStandardMajorDialogue()
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Bee", "Neutral") ]])

--Mei is not a bee, does not have bee form:
if(sMeiForm ~= "Bee" and iHasBeeForm == 0.0) then
	fnCutscene([[ Append("Mei:[E|Offended] All right, let's rumble![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] ...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] What happened?[P] You bees forget how to fight?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] Let's give them a refresher course.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] No, wait.[P] I think something is going on here.[B][C]") ]])
	fnCutscene([[ Append("*The bee seems to be preoccupied.[P] It keeps looking over its shoulder as it tries to block you*[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Surprise] This -[P] this might be bad, Mei.[P] I've never seen bees acting like this.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] What's wrong, bee?") ]])
	fnCutsceneBlocker()

--Mei is not a bee, and has bee form:
elseif(sMeiForm ~= "Bee" and iHasBeeForm == 1.0) then
	fnCutscene([[ Append("Mei:[E|Neutral] Zzzz! Zz! Bzz? (Drone sisters![P] It's me![P] Don't you recognize me?)[B][C]") ]])
	fnCutscene([[ Append("Bee: Zz. Bzz. ZzzZzz. (Drone unidentified.[P] Language credentials accepted.[P] Go back.)[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Zz? (Why?)[B][C]") ]])
	fnCutscene([[ Append("Bee: Zzzz. BzzZz. ZzzBzz. (Information dangerous.[P] Hive under threat.[P] Forget this encounter.)[B][C]") ]])
	
	--If Florentina knows about Mei's runestone:
	if(iFlorentinaKnowsAboutRune == 1.0) then
		fnCutscene([[ Append("Florentina:[E|Confused] Excuse me, are you going to speak in a manner I can understand?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Surprise] Huh?[P] Oh, sorry.[B][C]") ]])
	
	--Otherwise:
	else
		fnCutscene([[ Append("Florentina:[E|Surprise] You speak bee?[P] Is bee even a language?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Surprise] Huh?[P] Oh, sorry.[P] Yeah, sort of.[P] It's complicated.[B][C]") ]])
	end
	
	--Resume.
	fnCutscene([[ Append("Mei:[E|Neutral] These bees are saying there's some sort of threat, but they won't tell me what.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Surprise] Threat?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Well they can handle it.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] No, something is different.[P] Drone, show me.[P] Zzzbzzz.") ]])
	fnCutsceneBlocker()

--Mei is a bee:
elseif(sMeiForm == "Bee") then
	fnCutscene([[ Append("Mei:[E|Neutral] Sister drone, you are not speaking with the hive.[P] Why?[P] What has happened?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Surprise] ...[P] Yes?[P] But -[P] oh my.[P] *Oh my!*[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] I can see your feelers going crazy over there.[P] What's up?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] I don't know![P] They won't tell me.[P] They've put a quarantine in place.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Surprise] Well I'm sure they've got things under control, then.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] That's what they're trying to say, but I can see all over them that they don't.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] Drone, show me.[P] Show me the threat.") ]])
	fnCutsceneBlocker()

end

-- |[Movement]|
--Wait a bit.
fnCutsceneWait(35)
fnCutsceneBlocker()

--Bees look to the north.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepA")
	ActorEvent_SetProperty("Face", 0.0, -1.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepB")
	ActorEvent_SetProperty("Face", 0.0, -1.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepC")
	ActorEvent_SetProperty("Face", 0.0, -1.0)
DL_PopActiveObject()
fnCutsceneWait(35)
fnCutsceneBlocker()

--Mei and Florentina look north.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 0.0, -1.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", 0.0, -1.0)
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneWait(35)
fnCutsceneBlocker()
	
-- |[Camera]|
--Camera moves up north.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 2.0)
	CameraEvent_SetProperty("Focus Position", 14.25 * gciSizePerTile, 11.50 * gciSizePerTile)
DL_PopActiveObject()
fnCutsceneWait(15)
fnCutsceneBlocker()

--Hold position for a while.
fnCutsceneWait(180)
fnCutsceneBlocker()

--Camera reverts to following Mei.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 2.0)
	CameraEvent_SetProperty("Focus Actor Name", "Mei")
DL_PopActiveObject()
fnCutsceneWait(15)
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Movement]|
--Everyone looks back at each other.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepA")
	ActorEvent_SetProperty("Face", 1.0, 0.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepB")
	ActorEvent_SetProperty("Face", 1.0, 0.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepC")
	ActorEvent_SetProperty("Face", 1.0, 0.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", -1.0, 0.0)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", -1.0, 0.0)
DL_PopActiveObject()
fnCutsceneWait(35)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnStandardMajorDialogue()
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Bee", "Neutral") ]])

--Mei is not a bee, does not have bee form:
if(sMeiForm ~= "Bee" and iHasBeeForm == 0.0) then
	fnCutscene([[ Append("Mei:[E|Surprise] What happened to that bee?[B][C]") ]])
	fnCutscene([[ Append("*The bee tries to push you away*[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] N-[P]no![P] That bee -[P] it had a handprint on it![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] So?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] The cultists who kidnapped me...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Angry] Those rat bastards![P] They're preying on these bees, too![P] I won't stand for this![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Angry] Let us through, bee![P] We'll sort them out![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Blush] I am so turned on right now.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Angry] We -[P][EMOTION|Mei|Neutral] Huh?[B][C]") ]])
	fnCutscene([[ Append("*The bees stand firm, and do not move*[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] I guess they're trying to protect us by keeping those things in here.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] Bee, you have to let us help![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] They're way too wired.[P] Luckily, I think I can solve this problem.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] How about a bit of -[P] this?[B][C]") ]])
	fnCutscene([[ Append("*A sweet smell begins to emanate from the flowers on Florentina's body...*[B][C]") ]])
	fnCutscene([[ Append("*...*[B][C]") ]])
	fnCutscene([[ Append("*The smell seems to be relaxing the bees...*[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Mmmm, that's nice.[P] What are you doing?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] You may not have noticed, but I am about two-thirds poppy.[P] My natural perfume is a mild sedative.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] That's right, bees.[P] Breathe it nice and deep.[B][C]") ]])
	
	--Alraune Mei has something to say.
	if(sMeiForm == "Alraune") then
		fnCutscene([[ Append("Mei:[E|Blush] I didn't know we could do that...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] You probably can't.[P] You look more like some sort of nightshade to me.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] What else can this body do...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Heh.[P] Focus on the task at hand, for now.[P] Maybe I can show you some tricks later.[B][C]") ]])
	
	--All other forms.
	else
		fnCutscene([[ Append("Mei:[E|Blush] I didn't know you could do that...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] It does very little to humans, but bees are extra sensitive to plant odors.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Surprise] ...[P] Hey, don't go overboard there, Mei.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] Perish the thought![P] It just smells nice.[B][C]") ]])
	end
	
	--Resume.
	fnCutscene([[ Append("Mei:[E|Neutral] Okay, let's go kick some cultist -[P] hey![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] What?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Why are you helping me?[P] You've got nothing against the cult.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] This is a beehive.[P] The bees don't wear clothes or valuables.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] And so you think they put them somewhere - [B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] When they make a new bee.[P] Precisely.[P] If we find anything nice, the bees won't mind us borrowing it.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] Phew![P] For a second there I thought you were a good person.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] All right, onward!") ]])
	fnCutsceneBlocker()

--Mei is not a bee, and has bee form:
elseif(sMeiForm ~= "Bee" and iHasBeeForm == 1.0) then
	fnCutscene([[ Append("Mei:[E|Sad] That poor drone...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Cry] We've got to do something to help her...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] It's just a bee, Mei.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Sad] Can you not see the pain on her face?[P] I can almost hear her crying out...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Surprise] Calm down, Mei.[P] I didn't mean it like that.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Sad] That handprint on her face...[P] I've seen that before...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] H-[P]hey![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Angry] Those rat-fink BASTARDS![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Surprise] Woah![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Angry] The cultists who kidnapped me![P] They had handprints on their robes![P] They're behind this![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Angry] Let me at them![P] I'll teach them to mess with the bees![B][C]") ]])
	fnCutscene([[ Append("*The bees stand firm, and do not move*[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Blush] Heh, I'm getting a little turned on.[P] Mei, tell you what.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Blush] How about I get these bees to let us through...[B][C]") ]])
	fnCutscene([[ Append("*A sweet smell begins to emanate from the flowers on Florentina's body...*[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] What is that sweet, sweet smell?[B][C]") ]])
	fnCutscene([[ Append("*The smell seems to be relaxing the bees...*[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] We Alraunes naturally secrete perfumes.[P] This is mine.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] I happen to be two-thirds poppy.[P] Do you know what opiates are?[B][C]") ]])
	
	--Alraune Mei has something to say.
	if(sMeiForm == "Alraune") then
		fnCutscene([[ Append("Mei:[E|Neutral] Can I do that too?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Not by the look of you.[P] You look to be more of a nightshade.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] I'm sure I can tease some tricks out of your body, though.[P] But, later.[P] We've got a job to do.[B][C]") ]])
	
	--Otherwise:
	else
		fnCutscene([[ Append("Mei:[E|Neutral] You're drugging them?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] It will calm them down a bit.[P] They should let us through now.[B][C]") ]])
	end
	
	--Resume.
	fnCutscene([[ Append("Mei:[E|Neutral] Thanks, Florentina.[P] I can't just let those cultists walk all over these poor, innocent bees.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Hey, wait a minute![P] Why are you suddenly defending the innocent?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Blush] Seeing you get all worked up really got my juices running...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Uhhhh...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Blush] I mean...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] The bees dump the valuables of their converts somewhere.[P] There's got to be a practical treasure hoard down here somewhere.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] Oh, sure.[P] That's it.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Doesn't matter.[P] Let's go save the bees!") ]])
	fnCutsceneBlocker()

--Mei is a bee:
elseif(sMeiForm == "Bee") then
	fnCutscene([[ Append("Mei:[E|Sad] That poor drone...[P] So this is what happened...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Cry] She cries out to us, but we dare not answer.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Surprise] Get it together, Mei![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Cry] Such a terrible fate...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] I said...[B][C]") ]])
	fnCutscene([[ Append("[SOUND|World|Slap]*slap*[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] Keep.[P] It.[P] Together.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Sad] ...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Sad] You're right.[P] I've got to be strong.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Sad] I feel their despair as my own thoughts, but...[P] I won't be able to save them if I give in.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] You give your own pep-talks?[P] Great, because I happen to be really bad at that.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I saw -[P] I saw - [B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Angry] CULTISTS![P] The handprints, those robes![P] Those cultist bastards are behind this![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Surprise] Woah![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Angry] They're the ones who kidnapped me, and now they're preying on my hive![P] I will not allow it![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Blush] Hot![P] Keep going![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Wait, what?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Blush] Nothing...[P] let's go ruin some cultist's day![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Well, my sister drones won't let us through.[P] They have to maintain the quarantine.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] Oh, is that all?[P] I think I can change their minds...[B][C]") ]])
	fnCutscene([[ Append("*A sweet smell begins to emanate from the flowers on Florentina's body...*[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] That smell...[B][C]") ]])
	fnCutscene([[ Append("*The smell seems to be relaxing the bees...*[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] F-[P]Florentina...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] Worked like a charm.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] What are you doing?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] Natural Alraune perfume.[P] I happen to be two-thirds poppy, you know.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] Let me...[P] taste your nectar...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] Oh, you'd like that wouldn't you?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] But we've got a job to do here.[P] Clear your mind.[P] Focus on the task at hand.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Y-yeah.[P] Gotta...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] Gotta save the hive!") ]])
	fnCutsceneBlocker()
end

-- |[Movement]|
--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Move the bees back to their start positions, but a bit slower.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepA")
	ActorEvent_SetProperty("Move To", 15.25 * gciSizePerTile, 24.50 * gciSizePerTile, 0.45)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepB")
	ActorEvent_SetProperty("Move To", 17.25 * gciSizePerTile, 22.50 * gciSizePerTile, 0.45)
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "BeeGirlRepC")
	ActorEvent_SetProperty("Move To", 16.25 * gciSizePerTile, 22.50 * gciSizePerTile, 0.45)
DL_PopActiveObject()

--Florentina moves onto Mei, fold the party.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", 18.25 * gciSizePerTile, 23.50 * gciSizePerTile)
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutscene([[ AL_SetProperty("Fold Party") ]])
fnCutsceneBlocker()
