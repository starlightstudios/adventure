-- |[Breanne's First Scene]|
--This plays regardless of Mei's form the first time she enters the Pit Stop. Some of the dialogue changes though.
-- The scene is activated by entering the trigger inside the main room of the pit stop.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasMetMei", "N", 1.0)

-- |[Variables]|
--Get values.
local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local iHasFoundOutlandAcolyte = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasFoundOutlandAcolyte", "N")
local bIsFlorentinaHere = AL_GetProperty("Is Character Following", "Florentina")

--Breanne records the first form she saw Mei in.
VM_SetVar("Root/Variables/Chapter1/Breanne/sMeiFirstForm", "S", sMeiForm)

-- |[Walking]|
--Mei walks in a bit.
Cutscene_CreateEvent("Move Mei Inside A", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (18.25 * gciSizePerTile), (13.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("Move Mei Inside B", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (20.25 * gciSizePerTile), (14.50 * gciSizePerTile))
DL_PopActiveObject()

--If Florentina is present, she also walks in.
if(bIsFlorentinaHere) then
	Cutscene_CreateEvent("Move Florentina Inside A", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (18.25 * gciSizePerTile), (13.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("Move Florentina Inside B", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (19.25 * gciSizePerTile), (14.50 * gciSizePerTile))
	DL_PopActiveObject()
end
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(5)
fnCutsceneBlocker()

-- |[Talking]|
--Breanne says she'll be right there.
fnStandardMajorDialogue()
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Breanne", "Neutral") ]])
fnCutscene([[ Append("Breanne: Coming![P] Just a moment!") ]])
fnCutsceneBlocker()

-- |[Walking]|
--Walk Breanne to the door.
Cutscene_CreateEvent("Move Breanne to Door", "Actor")
	ActorEvent_SetProperty("Subject Name", "Breanne")
	ActorEvent_SetProperty("Move To", (24.25 * gciSizePerTile), (16.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
Cutscene_CreateEvent("Face Breanne West", "Actor")
	ActorEvent_SetProperty("Subject Name", "Breanne")
	ActorEvent_SetProperty("Face", -1, 0)
DL_PopActiveObject()
fnCutsceneWait(15)
fnCutsceneBlocker()

--Open the door, play a sound.
fnCutscene([[AL_SetProperty("Open Door", "Kitchen Door")]])
fnCutscene([[AudioManager_PlaySound("World|OpenDoor")]])
fnCutsceneBlocker()
fnCutsceneWait(15)
fnCutsceneBlocker()

--Walk Breanne out.
Cutscene_CreateEvent("Move Breanne to Lobby A", "Actor")
	ActorEvent_SetProperty("Subject Name", "Breanne")
	ActorEvent_SetProperty("Move To", (22.25 * gciSizePerTile), (16.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("Move Breanne to Lobby B", "Actor")
	ActorEvent_SetProperty("Subject Name", "Breanne")
	ActorEvent_SetProperty("Move To", (22.25 * gciSizePerTile), (15.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("Move Breanne to Lobby B", "Actor")
	ActorEvent_SetProperty("Subject Name", "Breanne")
	ActorEvent_SetProperty("Move To", (21.25 * gciSizePerTile), (14.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Timing.
Cutscene_CreateEvent("Face Breanne West", "Actor")
	ActorEvent_SetProperty("Subject Name", "Breanne")
	ActorEvent_SetProperty("Face", -1, 0)
DL_PopActiveObject()
fnCutsceneWait(15)
fnCutsceneBlocker()

-- |[Talking]|
--Standard parts of the dialogue.
fnStandardMajorDialogue()
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Breanne", "Neutral") ]])

--Mei is alone...
if(bIsFlorentinaHere == false) then
	
	--Flag: Breanne met Mei when Florentina was present.
	VM_SetVar("Root/Variables/Chapter1/Breanne/iMetMeiWithFlorentina", "N", 0.0)
	
	--Topic unlocks. Breanne mentioned Florentina, the trading post, the magic field, the pit stop, the season, and her own name.
	WD_SetProperty("Unlock Topic", "Florentina", 1)
	WD_SetProperty("Unlock Topic", "TradingPost", 1)
	WD_SetProperty("Unlock Topic", "PitStopMagicField", 1)
	
	--Mei is an Alraune.
	if(sMeiForm == "Alraune") then
		fnCutscene([[ Append("Breanne: Well howdy, stranger.[P] What can I do for you?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Surprise] My appearance doesn't upset you, human?[B][C]") ]])
		fnCutscene([[ Append("Breanne: Name's Breanne, in case the signs didn't give it away.[B][C]") ]])
		fnCutscene([[ Append("Breanne: We're open to all sorts here.[P] 'Sides, nobody can set foot on the property if they've got violent intentions.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] What?[P] How?[B][C]") ]])
		fnCutscene([[ Append("Breanne: Search me.[P] Some kinda magic.[P] I found out about it a while back, and figured this was a good spot to set up.[B][C]") ]])
		fnCutscene([[ Append("Breanne: If you're here, it means you're a sweet sort.[B][C]") ]])
		fnCutscene([[ Append("Breanne: So, what's your name?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Please call me Mei.[B][C]") ]])
		fnCutscene([[ Append("Breanne: That's a nice name, but I'm guessing it's not your first one.[P] I know how Alraunes work.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] Actually, it is.[P] I...[P] decided not to clear my mind.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Well, what brings you here, then?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I'm not quite sure, myself.[P] I feel the need to find out the purpose behind this runestone, and how I got here from Earth.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Something pushing you to explore, eh?[P] I know the feeling.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Have you ever seen this symbol before?[B][C]") ]])
		fnCutscene([[ Append("Breanne: ...[B][C]") ]])
		fnCutscene([[ Append("Breanne: No, sorry.[P] I'm not much of a scholar.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Magic is a little out of my league.[P] I like to work with my hands.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I see.[P] Thank you for your time.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Now hold on there a moment.[P] Just because I don't know what it is doesn't mean I don't know who might.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Just southwest of here is a trading post.[P] You look up Florentina when you're there, and I bet she'll be able to tell you what this rune is about.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Florentina?[P] Yes, hm.[P] The little ones have spoken of her.[B][C]") ]])
		fnCutscene([[ Append("Breanne: What's a little one?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Laugh] Oh, I'm sorry.[P] You would call them grass, flowers, that sort of thing.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Nifty![P] What do you call trees?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] Grandfather or grandmother, if they're fully grown.[P] Otherwise, they'd be little ones.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Hey, I know I told you to talk to Florentina, but if you want to stick around and chat for a bit...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] You're not busy?[B][C]") ]])
		fnCutscene([[ Append("Breanne: There's been a lull in visitors lately.[P] My schedule is wide open!") ]])
	
	--Mei is a Slime.
	elseif(sMeiForm == "Slime") then
		fnCutscene([[ Append("Breanne: Oh shoot, you managed to get the door open did you?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Surprise] W-wait![P] This isn't what it looks like![B][C]") ]])
		fnCutscene([[ Append("Breanne: Really?[P] Because to me, it looks like a talking slime just got my door open.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] ...[P] Actually, that's pretty much right.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] But I'm not like the other slimes, I promise![B][C]") ]])
		fnCutscene([[ Append("Breanne: Once again, you go stating the obvious.[P] Slimes normally can't talk, silly.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Laugh] Well I'm not going to attack you, is what I mean.[B][C]") ]])
		fnCutscene([[ Append("Breanne: You can't.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I - [P]can't?[B][C]") ]])
		fnCutscene([[ Append("Breanne: Nope.[P] This place is built on some kinda magic field.[P] Repulses anyone who intends violence.[P] I don't know how it works, mind, but it works.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Do you have a name, little slime?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Mei.[P] Call me Mei.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Hm, not a common name around these parts.[P] What brings you to the pit stop?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I was looking for someone who could help me find a way back to Earth.[P] But then I got turned into a slime.[B][C]") ]])
		fnCutscene([[ Append("Breanne: This is recent, is it?[B][C]") ]])
        
        local iGotSlimedFromDrink = VM_GetVar("Root/Variables/Chapter1/Scenes/iGotSlimedFromDrink", "N")
        if(iGotSlimedFromDrink == 1.0) then
            fnCutscene([[ Append("Mei:[E|Sad] Yes.[P] I woke up in a dingy dungeon with this runestone, broke out, found a slime who was really nice, and then kinda drank some slime.[B][C]") ]])
            fnCutscene([[ Append("Breanne: From the same slime?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] No, different slime.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] Look, I was really thirsty and she was offering.[P] How was I supposed to know what was going to happen?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] The worst thing that happens on Earth when you drink something bad is you get sick![B][C]") ]])
            fnCutscene([[ Append("Breanne: Earth?[P] Runestone?[P] I can see there's a lot going on here.[B][C]") ]])
        
        else
        
            fnCutscene([[ Append("Mei:[E|Sad] Yes.[P] I think this runestone is somehow related, I found it when I woke up here and it did something when I got...[P] viscous...[B][C]") ]])
            fnCutscene([[ Append("Breanne: Woah, slow down there.[P] You just said a bunch of stuff.[P] What's this runestone?[P] What's Earth?[B][C]") ]])
        end
            
		fnCutscene([[ Append("Mei:[E|Neutral] Earth is where I'm from.[P] There was a flash of white when I was walking to the train station, and I wound up here, with this runestone.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Let me have a look...[B][C]") ]])
		fnCutscene([[ Append("Breanne: ...[B][C]") ]])
		fnCutscene([[ Append("Breanne: Well I don't know much about magic, but this sure looks important.[B][C]") ]])
		fnCutscene([[ Append("Breanne: You know who'd be able to help you out?[P] Florentina.[P] She runs the provisions shop at the trading post across the river.[B][C]") ]])
		fnCutscene([[ Append("Breanne: At the very least she'd be able to tell you what the symbol means, or tell you how to find out.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] Thanks so much...[B][C]") ]])
		fnCutscene([[ Append("Breanne: Don't mention it![P] And, if you want to stick around and chat, I'm all ears.[P] Not a lot of customers around this time.[P] I'd love to hear what it's like being a slime.") ]])
	
	--Mei is a Bee.
	elseif(sMeiForm == "Bee") then
		fnCutscene([[ Append("Breanne: Oh my![B][C]") ]])
		fnCutscene([[ Append("Breanne: Ain't no nectar in here, honeybun.[P] Hehe.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I'm not looking for nectar, actually.[B][C]") ]])
		fnCutscene([[ Append("Breanne: You can speak?[P] That's a first.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Surprise] You don't mind?[P] My form doesn't upset you?[B][C]") ]])
		fnCutscene([[ Append("Breanne: You kidding?[P] I'd love to have you as a guest.[P] Stay as long as you like![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Phew.[P] I was worried you'd think I was going to attack you.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Hm.[P] Oh![P] Very interesting, thank you.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Uhh...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] Sorry, I was talking to the other bees.[P] I sometimes forget you humans can't hear them.[B][C]") ]])
		fnCutscene([[ Append("Breanne: What did they say?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] They said this place was built on a magic field of some sort.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Yep, it repulses anyone or anything with violent intentions.[P] Very powerful, very old.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Do you have a name?[P] Should I call you Drone C-137 or something?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Mei will do just fine, but thank you for asking.[B][C]") ]])
		fnCutscene([[ Append("Breanne: So what can I do for you, if you're not looking for nectar?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I'm looking for someone who can tell me more about this runestone.[P] It seems to have magical powers, and the hive wants to know how it works.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Runestone, eh?[P] Let me take a look at it.[B][C]") ]])
		fnCutscene([[ Append("Breanne: ...[B][C]") ]])
		fnCutscene([[ Append("Breanne: ...[B][C]") ]])
		fnCutscene([[ Append("Breanne: Well I don't recognize that symbol at all.[P] Magic powers you said?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Quite.[P] It seems to have a mind of its own.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Huh.[P] I can't help you, but I bet I know who can.[P] Florentina.[B][C]") ]])
		fnCutscene([[ Append("Breanne: There's a trading post southwest of here, across the river.[P] She runs the provisions shop there.[P] She tends to get up to all sorts of shady stuff.[B][C]") ]])
		fnCutscene([[ Append("Breanne: I reckon she knows a mage or two who could help you out.[B][C]") ]])
		fnCutscene([[ Append("Breanne: But if you want to stick around and chat, I'd love to hear what you have to say.[P] Don't get a lot of bees in here...") ]])
		
	--Mei is a Ghost.
	elseif(sMeiForm == "Ghost") then
		fnCutscene([[ Append("Breanne: Oh dearest me![B][C]") ]])
		fnCutscene([[ Append("Breanne: Are you -[P] a ghost?[P] Like an actual ghost?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Sort of.[P] It's quite complicated.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Do you -[P] are you here for unfinished business, or something like that?[P] I love to help![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Surprise] This is not the reaction I was expecting.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Smirk] I was assuming you'd be fleeing in terror right now.[B][C]") ]])
		fnCutscene([[ Append("Breanne: I'd never run from someone who needs my help![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Smirk] Well, if you could help me get back to Earth, that'd be lovely.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Oh, well, hmm.[P] Sorry, ghosty...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] My name is Nata -[P] Mei.[P] My name is Mei.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Sorry Ms. Mei, but I don't know where Earth is.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Oh.[P] Unfortunate.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Buck up, deadite![P] I'm sure my friend Florentina can help you![B][C]") ]])
		fnCutscene([[ Append("Breanne: She knows everyone from wizards to cartographers![P] If anyone knows how to get to Earth, it's her![B][C]") ]])
		fnCutscene([[ Append("Breanne: You just head southeast of here, across the river, and she'll be at the trading post.[P] I bet she'd love to meet you![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] That's actually some very good news.[P] Thank you very much, Ms. Breanne.[B][C]") ]])
		fnCutscene([[ Append("Breanne: How'd you know my name?[P] Is it ghost powers?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Smirk] It's...[P] written on the sign out front...[B][C]") ]])
		fnCutscene([[ Append("Breanne: Oh, oops.[P] I forgot about that.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Well, Mei.[P] This is my Pit Stop.[P] If you're tired of your ghostly journey, I'd love to have you for company.[P] I can sell you supplies, too, if you need them.[B][C]") ]])
		fnCutscene([[ Append("Breanne: ...[P] Do ghosts need supplies?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] Not really, but I appreciate the offer.") ]])
	
	--Mei is a Werecat.
	elseif(sMeiForm == "Werecat") then
		fnCutscene([[ Append("Breanne: Well aren't you just the cutest thing in the world![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Purr...[P] Purr...[P][EMOTION|Mei|Surprise] Hey![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] Don't scratch me like -[P] ooooohhhpurrrr....[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] You're a devil with those fingernails...[B][C]") ]])
		fnCutscene([[ Append("Breanne: You want some chicken there little lady?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Laugh] I'd love -[P][EMOTION|Mei|Offended] stop that![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] I am not a housecat![B][C]") ]])
		fnCutscene([[ Append("Breanne: Works every time![B][C]") ]])
		fnCutscene([[ Append("Breanne: So what can I do for you, miss..?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Mei.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I'm looking for someone who can tell me about this runestone, or maybe tell me how to get home.[B][C]") ]])
		fnCutscene([[ Append("Breanne: On the runestone?[P] Mm, nope, I've got nothing.[P] Sorry.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Where's home?[P] I thought you cats lived out in the forest.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Smirk] I've not been a fang for very long, actually, and I'd quite like to return to Hong Kong.[P] Let my parents know I'm all right.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Smirk] Judging from what I've seen, I'm not on Earth anymore, am I?[B][C]") ]])
		fnCutscene([[ Append("Breanne: Earth?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] As I expected.[P] The moon here is different...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] No matter, the hunt for a way home will yield all the more glorious a catch![B][C]") ]])
		fnCutscene([[ Append("Breanne: I'm not much for magic or the like.[P] Can't really help you there.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] Unfortunate.[B][C]") ]])
        
        local iMetNadiaInWerecatScene = VM_GetVar("Root/Variables/Chapter1/ScenesWerecat/iMetNadiaInWerecatScene", "N")
        if(iMetNadiaInWerecatScene == 1.0) then
            fnCutscene([[ Append("Breanne: But I think I know who can - [P][CLEAR]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Florentina?[B][C]") ]])
            fnCutscene([[ Append("Breanne: That's who I was about to suggest![P] How'd you know?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Nadia mentioned her.[P] We're acquainted.[B][C]") ]])
            fnCutscene([[ Append("Breanne: I can't direct you to a more knowledgeable sort than her![P] Just head over to the trading post and tell her I said hi![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Purr...[P] thank you.[B][C]") ]])
            fnCutscene([[ Append("Breanne: Now if you want to stick around, I wasn't kidding about the chicken...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Laugh] Mmm, you know exactly what to say!") ]])
        else
            fnCutscene([[ Append("Breanne: But I think I know who can -[P] Florentina.[B][C]") ]])
            fnCutscene([[ Append("Breanne: She runs the general store at the trading post southwest of here.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Thank you for the directions.[B][C]") ]])
            fnCutscene([[ Append("Breanne: I can't direct you to a more knowledgeable sort than her![P] Just head over to the trading post and tell her I said hi![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] I will.[B][C]") ]])
            fnCutscene([[ Append("Breanne: Now if you want to stick around, I wasn't kidding about the chicken...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Laugh] Mmm, you know exactly what to say!") ]])
	
        end
    
    --Gravemarker.
    elseif(sMeiForm == "Gravemarker") then
		fnCutscene([[ Append("Breanne: Well golly, stranger![P] Never seen your particular kind around here![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] Oh, I enjoy being unique![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I was worried I might scare you with how I look, but it seems you're unafraid.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Takes a bit more to rile me than it does most.[P] 'Sides, you can't do any violence here at the Pit Stop.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Really?[P] That must be that magic field I felt.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Magic field![P] Yeah, that's the stuff.[P] Are you a mage?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] Me?[P] No, no.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I am attuned to the holy light of the angels, and it chafes against this field.[P] I don't think it's demonic magic, though.[B][C]") ]])
		fnCutscene([[ Append("Breanne: I really don't know, but it's been here since forever, so I built my place atop it.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Keeps me safe, and makes sure there's no drunken fights.[B][C]") ]])
		fnCutscene([[ Append("Breanne: So what brings you here?[P] Are you a preacher?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] Despite my appearances, I'm not affiliated with the angels.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I'm actually looking for a way home.[P] I got kidnapped by some people in the big building south of here.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Say, have you ever heard of Hong Kong?[B][C]") ]])
		fnCutscene([[ Append("Breanne: Can't say I have.[P] Wish I could help...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Laugh] Don't worry![P] I don't want to concern you![B][C]") ]])
		fnCutscene([[ Append("Breanne: Tell you what.[P] Go visit Florentina at the Trading Post southwest of here.[B][C]") ]])
		fnCutscene([[ Append("Breanne: She's a merchant, and she's been all over.[P] If anyone's heard of Hong Kong, it's her.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] Thank you very much![P] You're being very nice to me...[B][C]") ]])
		fnCutscene([[ Append("Breanne: I'm nice to everyone, sweetie![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] Heh, I just meant it's a contrast given all the people who've tried to kill me thus far.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Oh yeah, bandits and monstergirls are all over the place.[B][C]") ]])
		fnCutscene([[ Append("Breanne: You can stay as long as you like, though.[P] I love having visitors!") ]])

    --Wisphag:
    elseif(sMeiForm == "Wisphag") then
		fnCutscene([[ Append("Breanne: What in the hay?[P] Hello?[P] Are you friendly?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] To people who are nice to me![B][C]") ]])
		fnCutscene([[ Append("Breanne: And you can speak?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Yep, since I was two.[B][C]") ]])
		fnCutscene([[ Append("Breanne: You are the first wisphag I've seen who can do that.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Smirk] I pride myself on being unique.[P] It takes a lot of work but it's worth it![B][C]") ]])
		fnCutscene([[ Append("Breanne: I 'spose customers come in all forms.[P] Are you looking to do any sort of business?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Smirk] Sure![P] Can I buy some information?[B][C]") ]])
		fnCutscene([[ Append("Breanne: Information's free.[P] What do you need to know?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I'm trying to find my way back home.[P] Hong Kong, to be precise.[P] Given what I've seen...[B][C]") ]])
		fnCutscene([[ Append("Breanne: Never heard of it.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] Nuts.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Aw gee, it tears at my heart to see you so dejected and all.[B][C]") ]])
		fnCutscene([[ Append("Breanne: If anyone knows where Hong Kong is, I bet it's Florentina at the trading post.[P] Just southwest, across the river.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] Thanks![P] That's where I'll check next, I suppose.[P] Thanks for being so nice.[B][C]") ]])
		fnCutscene([[ Append("Breanne: A smile and a kind word goes a long way out here.[P] Feel free to take a load off while you're here.") ]])
    
    --Mannequin:
    elseif(sMeiForm == "Mannequin") then
		fnCutscene([[ Append("Breanne: Oh, I do not like this, not at all.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Hold on, I don't mean to scare you.[B][C]") ]])
		fnCutscene([[ Append("Breanne: You can talk?[P] What are you?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I got turned into a mannequin but I'm still a normal person.[P] It's just a little hard to show my emotions.[P] Without most of my face.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Turned into a mannequin?[P] Like the things they use to model clothes at shops?[B][C]") ]])
		fnCutscene([[ Append("Breanne: What sort of shop would do that?[P] And in Trannadar of all places?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] It wasn't a shop, it's some sort of curse.[P] Look, I'm lost and trying to find my way home, and things just sort of keep piling up.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Maybe I can help you find your way home?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Do you know where Hong Kong is?[B][C]") ]])
		fnCutscene([[ Append("Breanne: Errrrrrrrr...[P] no.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Scratch that, then.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Darn, at least I tried.[P] To cheer you up, that is.[P] Did it work?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I'm smiling on the inside because I met someone who is polite and trying to be helpful.[B][C]") ]])
		fnCutscene([[ Append("Breanne: How about, you talk to Florentina over at the trading post southwest of here?[P] She knows all sorts of things.[B][C]") ]])
		fnCutscene([[ Append("Breanne: If anyone knows who knows where Hong Kong is, Florentina knows them and has beat them at cards.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] All right, I suppose I'll check out the trading post.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Shame I can't help further.[P] Unless you're looking for work.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Do you need some clothes modelled?[P] Do you sell them here?[B][C]") ]])
		fnCutscene([[ Append("Breanne: I meant helping me with my garden and the kitchen, sweetie![P] Do you want to model clothes?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] It's like an instinct.[P] I'll let you know how it goes at the trading post.") ]])

	--Mei is a Human, also the unhandled case.
	else
		fnCutscene([[ Append("Breanne: Howdy, stranger![P] Welcome to my little corner of the world.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] Thank goodness, a friendly face.[B][C]") ]])
		fnCutscene([[ Append("Breanne: You won't find a friendlier one, that's for sure.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Considering how many roving monsters and thugs there are, you're awfully laid back.[P] Are you all alone?[P] Aren't you afraid?[B][C]") ]])
		fnCutscene([[ Append("Breanne: Me?[P] Pah, I ain't scared of a few slimes.[P] Besides, I sited this here building right on top of a magic field.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Found out about it from some survey archives.[P] Apparently, it sends packing anyone who has violent intentions.[P] Very potent![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] So you don't mind if I rest here for a while?[B][C]") ]])
		fnCutscene([[ Append("Breanne: I called this place the Pit Stop for a reason.[P] You traders need to slow down and take it easy every now and then.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Oh, I'm not a trader.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Then what brings you all the way out to Evermoon forest?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] Well...[P] You see, I think some goons kidnapped me.[P] Do you know about the weirdos in the building across the lake?[B][C]") ]])
		fnCutscene([[ Append("Breanne: Sorry, but I don't go out that way very much.[P] How did you escape?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] I ran and fought my way out.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] Since then, I've been trying to find out where I am.[B][C]") ]])
		fnCutscene([[ Append("Breanne: I'd say you're in my Pit Stop right now.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Heh, I meant how to get back home.[P] I'm a real long way, since we sure didn't have monster girls on Earth.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Earth?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Surprise] Do you recognize the name?[B][C]") ]])
		fnCutscene([[ Append("Breanne: Nope, sorry.[P] It must be real far.[P] Pandemonium is a big place.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] *sigh*[P] I was afraid you were going to say that, but I guess I'm not surprised.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Do you have any idea who might know how to get to Earth?[B][C]") ]])
		fnCutscene([[ Append("Breanne: We get an awful lot of travellers through here, but I don't think I've ever took to discussing geography with them.[B][C]") ]])
		fnCutscene([[ Append("Breanne: If anyone knows, it'd be that Florentina over at the trading post.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Head southwest through the forest, across the river.[P] She runs the provisions shop.[P] Tell her I said hi![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I might just stick around here for a little bit.[P] I need to catch my breath.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Stay as long as you like![P] I love having company.[P] Plus you probably know all kinds of stuff about Earth.[P] You can tell me all about it!") ]])
	
		--Topics
		WD_SetProperty("Unlock Topic", "Pandemonium", 1)
	end

-- |[Florentina is Present]|
else
	
	--Flag: Breanne met Mei when Florentina was present.
	VM_SetVar("Root/Variables/Chapter1/Breanne/iMetMeiWithFlorentina", "N", 1.0)
	
	--Variables.
	local iMetClaudia = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetClaudia", "N")
	local iMeiKnowsRilmani = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiKnowsRilmani", "N")
    local iHasFoundOutlandAcolyte = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasFoundOutlandAcolyte", "N")
	
	--Topic unlocks. Breanne mentioned Outland Farm, Quantir, and Claudia's Convent.
	WD_SetProperty("Unlock Topic", "TradingPost", 1)
	WD_SetProperty("Unlock Topic", "OutlandFarm", 1)
	WD_SetProperty("Unlock Topic", "Claudia", 1)

	--Mei is an Alraune.
	if(sMeiForm == "Alraune") then
		fnCutscene([[ Append("Breanne: Well well, if it isn't Florentina![P] And you brought a leaf-sister![B][C]") ]])
		fnCutscene([[ Append("Breanne: That's what you call each other, right?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Offended] Hrmpf.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Did I say something wrong?[P] Sorry...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] No, it's fine.[P] Florentina isn't like that, but normally we call each other leaf-sister.[B][C]") ]])
		fnCutscene([[ Append("Breanne: And you are?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Call me Mei.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Well hello, Mei.[P] And long time, no see, Florentina.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Offended] Yeah, sure.[P] We're not here for pleasantries.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Are you ever?[P] I got some of that Runsdet fungus if you're looking to buy.[B][C]") ]])
		
		--Mei has not met Claudia and does not know Rilmani:
		if(iMetClaudia == 0.0 and iMeiKnowsRilmani == 0.0) then
			fnCutscene([[ Append("Florentina:[E|Happy] Not right now...[P][E|Neutral] Actually, we're looking for Sister Claudia.[P] Is she staying here?[B][C]") ]])
			fnCutscene([[ Append("Breanne: Claudia?[P] I remember she and her convent came through here a few weeks ago.[P] Very tidy, not a hair out of place on them.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Where did they go?[B][C]") ]])
			fnCutscene([[ Append("Breanne: They bought a bunch of supplies and headed out northeast, but I don't know where they were going.[P] They could be anywhere by now.[B][C]") ]])
			fnCutscene([[ Append("Breanne: I was over at the Outland Farm the other day, and one of her followers was there.[P] Maybe you should ask there?[B][C]") ]])
            
            if(iHasFoundOutlandAcolyte == 0.0) then
                fnCutscene([[ Append("Mei:[E|Neutral] Thank you, you've been a great help.[B][C]") ]])
                fnCutscene([[ Append("Breanne: Hey, if you're not too busy, you can stick around and chat for a bit.[P] I love meeting new people![P] Or plants, in your case.") ]])
            else
                fnCutscene([[ Append("Mei:[E|Smirk] Do you mean miss Karina?[B][C]") ]])
                fnCutscene([[ Append("Breanne: Yeah, that was her![P] I take it you already bumped into her?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] She said the rest of her group went to Quantir.[P] Where is that?[B][C]") ]])
                fnCutscene([[ Append("Breanne: Northeast of here.[P] You go out of the pit stop, north, and then east.[P] Watch out for the werecats, though.[P] They make hay out of any intrusion on their hunting grounds.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Happy] Thanks, Breanne!") ]])
            end
            WD_SetProperty("Unlock Topic", "OutlandFarm", 1)
		
		--Mei is looking for Rilmani information.
		else
			fnCutscene([[ Append("Florentina:[E|Happy] Not right now...[P][E|Neutral] Actually, we're looking for anything we can find on the Rilmani.[B][C]") ]])
			fnCutscene([[ Append("Breanne: The whatsit now?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] This runestone has one of their symbols on it.[P] Have you ever seen anything like it?[B][C]") ]])
			fnCutscene([[ Append("Breanne: Mmm, no, sorry.[P] I don't know much about them.[B][C]") ]])
			fnCutscene([[ Append("Breanne: They're supposed to have claws like sawblades and teeth to gobble up children.[P] You be careful if you're going to find one.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Happy] Pff.[P] I bet they're not very tough.[B][C]") ]])
			fnCutscene([[ Append("Breanne: Don't you go picking fights with anything ten feet tall, Florentina!") ]])
		
		end
	
	--Mei is a Slime.
	elseif(sMeiForm == "Slime") then
		fnCutscene([[ Append("Breanne: Oh my![B][C]") ]])
		fnCutscene([[ Append("Breanne: Florentina, there's a slime![P] Right there![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Yes, I see it.[P] Pretty nifty, isn't it?[B][C]") ]])
		fnCutscene([[ Append("Breanne: Did you manage to tame one?[P] Is that your pet?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Yes.[P] Absolutely.[P] Mei, fetch![B][C]") ]])
		fnCutscene([[ Append("Breanne: It's name is Mei?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Angry] Don't treat me like a dog![B][C]") ]])
		fnCutscene([[ Append("Breanne: You taught it to talk, too?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] This has been so worth it already.[B][C]") ]])
		fnCutscene([[ Append("Breanne: I don't - [B][C]") ]])
		fnCutscene([[ Append("Breanne: What is going on here?[B][C]") ]])
		
		--Mei has not met Claudia and does not know Rilmani:
		if(iMetClaudia == 0.0 and iMeiKnowsRilmani == 0.0) then
			fnCutscene([[ Append("Mei:[E|Offended] I'm not her pet![P] We're trying to find Sister Claudia![P] She's just pranking you.[B][C]") ]])
			fnCutscene([[ Append("Breanne: But you're a slime, right?[P] Is that a disguise?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] It's complicated.[P] Sorry to be rude, but have you seen Claudia?[B][C]") ]])
			fnCutscene([[ Append("Breanne: Sheesh.[B][C]") ]])
			fnCutscene([[ Append("Breanne: She came through here a few weeks ago with her convent, and then left after getting some supplies.[B][C]") ]])
			fnCutscene([[ Append("Breanne: They didn't say where they were going, but they did go northeast through the woods.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Happy] Thank you![P] You're very kind.[P] You could learn a thing or two, Florentina.[B][C]") ]])
			fnCutscene([[ Append("Breanne: She's been like this for years.[P] If she were going to learn manners, she'd have done it by now.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Offended] Nice to see you, too, Breanne.[P] We'll be off, then.[B][C]") ]])
			fnCutscene([[ Append("Breanne: Why the hurry?[P] Don't you want to have a chat for a bit?[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Confused] If you're bored, you're doing a bad job of hiding it.[B][C]") ]])
			fnCutscene([[ Append("Breanne: It's the off season.[P] You're not one to talk, you're not busy either.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] I am, in fact, very busy.[B][C]") ]])
			fnCutscene([[ Append("Breanne: So busy that you can't go traipsing through the forest with this very polite slime?[P] Yes, you're sooo busy.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] Hypatia has the shop covered, and I'm insured against Hypatia-related incidents.[B][C]") ]])
			fnCutscene([[ Append("Breanne: All right, but the offer still stands.") ]])
		
		else
			fnCutscene([[ Append("Mei:[E|Offended] I'm not her pet![P] We're looking for information![P] She's just pranking you.[B][C]") ]])
			fnCutscene([[ Append("Breanne: But you're a slime, right?[P] Is that a disguise?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] It's complicated.[P] I don't mean to be rude.[B][C]") ]])
			fnCutscene([[ Append("Breanne: Oh, don't worry about that, hun.[P] Relative to Florentina, you're a saint![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] I'll not argue that point.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Happy] Nor will I.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] So, spill it.[P] What do you know about Rilmani?[B][C]") ]])
			fnCutscene([[ Append("Breanne: The whatsit now?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] This runestone has one of their symbols on it.[P] Have you ever seen anything like it?[B][C]") ]])
			fnCutscene([[ Append("Breanne: Mmm, no, sorry.[P] I don't know much about them.[B][C]") ]])
			fnCutscene([[ Append("Breanne: They're supposed to have claws like sawblades and teeth to gobble up children.[P] You be careful if you're going to find one.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Happy] Pff.[P] I bet they're not very tough.[B][C]") ]])
			fnCutscene([[ Append("Breanne: Don't you go picking fights with anything ten feet tall, Florentina!") ]])
		end
		
	--Mei is a Bee.
	elseif(sMeiForm == "Bee") then
		fnCutscene([[ Append("Breanne: My goodness![B][C]") ]])
		fnCutscene([[ Append("Breanne: Florentina, if you're looking for someplace to be alone with this bee...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Surprise] What is she talking about?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Blush] Uhhhhh...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] ...[P] Yes?[P] Oh.[P][E|Surprise] Oh![P] ...[P][E|Blush] Maybe later...[B][C]") ]])
		fnCutscene([[ Append("Breanne: You can talk?[P] That's a first, I've never met a talking bee![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] Oh, I'm sorry.[P] Yes, I can speak with humans.[P] I'm a bit different than the other bees.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] *Florentina, my sisters just said...*[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] *Shhh![P] Not in front of Breanne!*[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Right, sorry about that.[P] My name is Mei, and we're here on a mission for the hive.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Wow![P] You bees are a lot more complex than I thought![P] What can I help you with?[B][C]") ]])
		
		--Mei has not met Claudia and does not know Rilmani:
		if(iMetClaudia == 0.0 and iMeiKnowsRilmani == 0.0) then
			fnCutscene([[ Append("Mei:[E|Neutral] We're looking for Sister Claudia.[P] Florentina said she was going this way.[B][C]") ]])
			fnCutscene([[ Append("Breanne: Yep, but she went through a few weeks ago.[P] You'll have to hustle to catch up with her.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Did she say where she was going?[B][C]") ]])
			fnCutscene([[ Append("Breanne: I didn't think to ask, but she went northeast through the woods.[P] That'd be the way to go.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Happy] Thank you very much.[P] I'll let my sisters know how helpful you were.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Happy] I'm sure they'll return the favour by jumping you next time you go out.[P] Be on your guard.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Offended] She'd make an excellent drone![B][C]") ]])
			fnCutscene([[ Append("Breanne: That's a compliment, right?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Of course.[B][C]") ]])
			fnCutscene([[ Append("Breanne: I'll be careful, then.[P] Oh, speaking of hives, I think one of Claudia's followers was observing bees at the farm across the way.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] We are aware of her.[B][C]") ]])
			fnCutscene([[ Append("Breanne: We?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] The hive.[P] Sometimes I forget myself.[B][C]") ]])
			fnCutscene([[ Append("Breanne: You know, if you're not in a hurry, I'd love to hear all about it.[P] My door is always open if you want to chat.") ]])
			WD_SetProperty("Unlock Topic", "OutlandFarm", 1)
		else
			fnCutscene([[ Append("Mei:[E|Neutral] We're looking for any information on the Rilmani we can find.[B][C]") ]])
			fnCutscene([[ Append("Breanne: The whatsit now?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] This runestone has one of their symbols on it.[P] Have you ever seen anything like it?[B][C]") ]])
			fnCutscene([[ Append("Breanne: Mmm, no, sorry.[P] I don't know much about them.[B][C]") ]])
			fnCutscene([[ Append("Breanne: They're supposed to have claws like sawblades and teeth to gobble up children.[P] You be careful if you're going to find one.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Happy] Pff.[P] I bet they're not very tough.[B][C]") ]])
			fnCutscene([[ Append("Breanne: Don't you go picking fights with anything ten feet tall, Florentina!") ]])
		end
	
	--Mei is a Ghost.
	elseif(sMeiForm == "Ghost") then
		fnCutscene([[ Append("Breanne: My goodness![P] Florentina, it seems one of your victims has come back to haunt you![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Oh, very funny.[P] Hilarious.[P] It's so funny I forgot to laugh.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] You're Miss Breanne?[P] Hello, it's a pleasure to meet you.[P] My name is Mei.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I'm not normally a ghost.[P] It's quite complicated.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Anyone who says please and thank you is welcome here.[P] Florentina is, too![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] You are just on a roll here, aren't you?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Right?[B][C]") ]])
		fnCutscene([[ Append("Breanne: So what can I do for you two?[B][C]") ]])
		
		--Mei has not met Claudia and does not know Rilmani:
		if(iMetClaudia == 0.0 and iMeiKnowsRilmani == 0.0) then
			fnCutscene([[ Append("Mei:[E|Neutral] We're looking for Sister Claudia.[P] Florentina thinks she might know how to get back to Earth.[B][C]") ]])
			fnCutscene([[ Append("Breanne: Hmmm, Claudia...[P] Oh![B][C]") ]])
			fnCutscene([[ Append("Breanne: She took her convent northeast of here a few weeks back.[P] I think they were going to go look into that mansion up north.[P] You know, the one that's supposed to be...[B][C]") ]])
			fnCutscene([[ Append("Breanne: Haunted...[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] We've been up that way, yes.[P] I suppose we'll have to keep searching.[B][C]") ]])
			fnCutscene([[ Append("Breanne: Sorry I couldn't help.[P] Would you like me to fix you something to eat?[P] Oh, oops.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Thank you for the offer, but I'll never be hungry again.[B][C]") ]])
			fnCutscene([[ Append("Breanne: I really don't know how to offer hospitality to a ghost![P] I'm new at this![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] It was very nice talking with you.[P] Perhaps that's enough?[B][C]") ]])
			fnCutscene([[ Append("Breanne: It'll have to be.[P] Stay as long as you like!") ]])
		else
			fnCutscene([[ Append("Mei:[E|Neutral] We're looking for any information on the Rilmani we can find.[B][C]") ]])
			fnCutscene([[ Append("Breanne: The whatsit now?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] This runestone has one of their symbols on it.[P] Have you ever seen anything like it?[B][C]") ]])
			fnCutscene([[ Append("Breanne: Mmm, no, sorry.[P] I don't know much about them.[B][C]") ]])
			fnCutscene([[ Append("Breanne: They're supposed to have claws like sawblades and teeth to gobble up children.[P] You be careful if you're going to find one.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Happy] Pff.[P] I bet they're not very tough.[B][C]") ]])
			fnCutscene([[ Append("Breanne: Don't you go picking fights with anything ten feet tall, Florentina!") ]])
		end
	
	--Mei is a Werecat.
	elseif(sMeiForm == "Werecat") then
		fnCutscene([[ Append("Breanne: Well hello, Florentina![P] And -[P] hmmm...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Surprise] Why are you looking at me like that?[B][C]") ]])
		fnCutscene([[ Append("Breanne: Some of my fish have gone missing recently.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] I've had nothing to do with it![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] This has been so worth it already.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Of course you'd be behind it.[P] Did you hire a werecat to annoy me?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] Hire?[P] Perish the thought, she'd never part with a penny![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] If I got something out of it, I would.[P] It's called an investment.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Speaking of investments, Mei, why don't you tell Breanne why we're here?[B][C]") ]])
		fnCutscene([[ Append("Breanne: Always business, never pleasure.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Business [P]*is*[P] pleasure, Breanne.[B][C]") ]])
		
		--Mei has not met Claudia and does not know Rilmani:
		if(iMetClaudia == 0.0 and iMeiKnowsRilmani == 0.0) then
			fnCutscene([[ Append("Mei:[E|Neutral] We're looking for Sister Claudia.[P] Have you seen her recently?[B][C]") ]])
			fnCutscene([[ Append("Breanne: Claudia?[P] Oh, Claudia, right...[P] the monk?[B][C]") ]])
			fnCutscene([[ Append("Breanne: Her convent came through here a few weeks ago.[P] I think they went northeast after that.[P] I didn't think to ask them why.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Hm, the kinfang pride is to the northeast.[P] I wonder if any of them hunted Claudia?[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] Guess we're going that way, then.[P] Let's go.[B][C]") ]])
			fnCutscene([[ Append("Breanne: You know, if you weren't so curt I might tell you another useful fact.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] Sheesh, what is it?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Please don't let Florentina's brash attitude get to you.[P] She's helping me, isn't she?[B][C]") ]])
			fnCutscene([[ Append("Breanne: I suppose so, but there's probably something in it for her.[P] I don't believe I got your name, miss..?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Smirk] Mei.[P] Pleased to meet you.[B][C]") ]])
			fnCutscene([[ Append("Breanne: Well, one of Claudia's followers was over at Outland Farm across the river, last I saw.[P] If they were going someplace, she'd know.[B][C]") ]])
            if(iHasFoundOutlandAcolyte == 0.0) then
                fnCutscene([[ Append("Mei:[E|Smirk] Thank you kindly, miss Breanne.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Neutral] Yeah, some tip.[P] We were going that way anyway.[B][C]") ]])
                fnCutscene([[ Append("Breanne: You're welcome, Florentina.[B][C]") ]])
                fnCutscene([[ Append("Breanne: My door is always open if you want to stay a bit.[P] That goes for you too, Florentina.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Neutral] Sure it does.[P] Mei, shall we?") ]])
            else
                fnCutscene([[ Append("Mei:[E|Smirk] Do you mean miss Karina?[B][C]") ]])
                fnCutscene([[ Append("Breanne: Yeah, that was her![P] I take it you already bumped into her?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] She said the rest of her group went to Quantir.[P] Where is that?[B][C]") ]])
                fnCutscene([[ Append("Breanne: Northeast of here.[P] You go out of the pit stop, north, and then east.[P] And try not to upset the werecats there, they don't like competition in their hunting grounds.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Laugh] I think they'll leave me alone.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] Thanks, Breanne!") ]])
            end
			WD_SetProperty("Unlock Topic", "OutlandFarm", 1)
		else
			fnCutscene([[ Append("Mei:[E|Neutral] We're looking for any information on the Rilmani we can find.[B][C]") ]])
			fnCutscene([[ Append("Breanne: The whatsit now?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] This runestone has one of their symbols on it.[P] Have you ever seen anything like it?[B][C]") ]])
			fnCutscene([[ Append("Breanne: Mmm, no, sorry.[P] I don't know much about them.[B][C]") ]])
			fnCutscene([[ Append("Breanne: They're supposed to have claws like sawblades and teeth to gobble up children.[P] You be careful if you're going to find one.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Happy] Pff.[P] I bet they're not very tough.[B][C]") ]])
			fnCutscene([[ Append("Breanne: Don't you go picking fights with anything ten feet tall, Florentina!") ]])
		end
		
    --Gravemarker case.
    elseif(sMeiForm == "Gravemarker") then
		fnCutscene([[ Append("Breanne: Goodness me![P] Hello?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Hello.[P] Are you Breanne?[B][C]") ]])
		fnCutscene([[ Append("Breanne: That's me![P] Are you a -[P] talking...[P] statue...[P] angel?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] Just call her a moron.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Florentina![P] Are you responsible for this?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] No, the gravemarkers in St. Fora's convent are.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] We were on a bit of a treasure hunt.[P] The locals took exception.[B][C]") ]])
		fnCutscene([[ Append("Breanne: I can see that.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] It's not a problem for me, though.[P] Miss Polaris thinks there might be a clue to finding me a way home there.[B][C]") ]])
		fnCutscene([[ Append("Breanne: I s'pose as long as you're okay with the way you are, then it's fine with me.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Just don't let Florentina here goad you into anything crazy.") ]])
        
    --Wisphag:
    elseif(sMeiForm == "Wisphag") then
		fnCutscene([[ Append("Breanne: Hey Florentina![P] Is this your girlfriend?[P] Har har![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Hey, I'm way out of Florentina's league![B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] You know I'm not looking for a relationship, Breanne.[P] Mei here came up from a big estate in Sturnheim.[P] She's looking to wed.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Now that's just a low blow.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] Did I miss something?[B][C]") ]])
		fnCutscene([[ Append("Breanne: Never you mind about that, miss...[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Happy] Mei.[P] I'm a wisphag, if you didn't know.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Is that what they're called?[P] I thought they were more of a beldam.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] The name is never going to be flattering.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Mei's from real far off, so I'm trying to help her find a way home.[P] We're chasing a few leads.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Unless you need something to eat or a soft bed, I'm not much help there.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] That's okay![P] It's really nice meeting you.[B][C]") ]])
		fnCutscene([[ Append("Breanne: But keep your eye on Florentina.[P] She'll take anything that looks shiny to keep for herself.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Really?[P] That thought never occurred to me.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] Girl's gotta charge for her services you know.") ]])

    --Mannequin:
    elseif(sMeiForm == "Mannequin") then
		fnCutscene([[ Append("Breanne: Florentina, are you trying to sell me a...[P] mannequin?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] I'm not for sale.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Yikes![P] It can talk?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] Heh heh heh...[B][C]") ]])
		fnCutscene([[ Append("Breanne: You knew that'd give me a fright and you did it anyway![B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Relax.[P] It's a curse from jewelry.[P] Mei can turn back to normal by taking it off.[P] I can do the same thing.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Being a mannequin has distinct advantages in combat.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Cursed jewelry?[P] Yeesh, is that going around?[P] Do I need to check my inventory?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] I am now attuned to the curse and I don't sense it emanating from here.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Good to know.[B][C]") ]])
		fnCutscene([[ Append("Breanne: If that's the case, why'd you come here, Florentina?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] She's helping me find a way home.[P] I'm from Hong Kong, and I got kidnapped and taken here.[B][C]") ]])
		fnCutscene([[ Append("Breanne: And made to model clothing?[P] Kidnapping people to make mannequins out of them...[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] No, that happened after I got here.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] We're following some leads on getting her back home.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Shame I can't help, I've never even heard of Hong Kong.[P] But, if you need something to eat, or a place to sleep...[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Neither.[B][C]") ]])
		fnCutscene([[ Append("Breanne: My door is still open!") ]])
    
	--Mei is a Human, also the unhandled case.
	else
		fnCutscene([[ Append("Breanne: Oh, Florentina![P]  And you've brought a friend![B][C]") ]])
		fnCutscene([[ Append("Florentina: [E|Neutral]Yes, pleasantries to you, too.[B][C]") ]])
		fnCutscene([[ Append("Breanne: It wouldn't kill you to say hello every now and then.[P] When they say \"Don't be a stranger\", they're talking about you.[B][C]") ]])
		fnCutscene([[ Append("Mei: [E|Happy]Hello, miss Breanne.[P] My name is Mei, and we're looking for someone.[B][C]") ]])
		fnCutscene([[ Append("Breanne: You see?[P] She's not retching on the floor.[B][C]") ]])
		fnCutscene([[ Append("Florentina: [E|Offended]But she is wasting time.[P] We could have already been on our way, which would have suited you just fine.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Don't be terse.[P] So, Mei was it?[P] What can I do for you?[B][C]") ]])
		
		--Mei has not met Claudia and does not know Rilmani:
		if(iMetClaudia == 0.0 and iMeiKnowsRilmani == 0.0) then
			fnCutscene([[ Append("Mei: [E|Neutral]We're trying to find Sister Claudia.[P] Florentina said she may have come out this way.[B][C]") ]])
			fnCutscene([[ Append("Breanne: Claudia...[P] Claudia...[P] oh yes, the researcher.[B][C]") ]])
			fnCutscene([[ Append("Breanne: She came through here a few weeks back.[P] Not here now, unfortunately.[B][C]") ]])
			fnCutscene([[ Append("Mei: [E|Neutral]Did she say where she was going?[B][C]") ]])
			fnCutscene([[ Append("Breanne: I didn't think to ask.[P] Her convent weren't the most talkative types.[P] Very interested in that mansion across the lake, but said someone drove them off.[B][C]") ]])
			fnCutscene([[ Append("Mei: [E|Sad]Crud.[P] That's where I came from, and she wasn't kidding.[B][C]") ]])
			fnCutscene([[ Append("Breanne: When they left, they were going northeast through the woods.[P] That might be a place to start.[B][C]") ]])
			fnCutscene([[ Append("Mei: [E|Neutral]What's northeast of here?[B][C]") ]])
			fnCutscene([[ Append("Breanne: The path goes by the old Quantir estate.[P] It's abandoned, but you might be able to find a campsite.[B][C]") ]])
			fnCutscene([[ Append("Breanne: Oh![P] I think there was someone from her convent researching bees over at Outland Farm west of here.[P]  You might want to check with them.[B][C]") ]])
			fnCutscene([[ Append("Mei: [E|Happy]Thank you very much Breanne![P]  You've been a huge help.[B][C]") ]])
			fnCutscene([[ Append("Breanne: I love helping out strangers.[P] If you're not in a hurry, I could make some tea.[B][C]") ]])
			fnCutscene([[ Append("Florentina: [E|Offended]That's all right.[P] No need for that.[B][C]") ]])
			fnCutscene([[ Append("Breanne: Nice to see you, too, Florentina.[B][C]") ]])
			fnCutscene([[ Append("Florentina: [E|Happy]Of course.[P] Mei, shall we be off?") ]])
			WD_SetProperty("Unlock Topic", "OutlandFarm", 1)
		else
			fnCutscene([[ Append("Mei:[E|Neutral] We're looking for any information on the Rilmani we can find.[B][C]") ]])
			fnCutscene([[ Append("Breanne: The whatsit now?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] This runestone has one of their symbols on it.[P] Have you ever seen anything like it?[B][C]") ]])
			fnCutscene([[ Append("Breanne: Mmm, no, sorry.[P] I don't know much about them.[B][C]") ]])
			fnCutscene([[ Append("Breanne: They're supposed to have claws like sawblades and teeth to gobble up children.[P] You be careful if you're going to find one.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Happy] Pff.[P] I bet they're not very tough.[B][C]") ]])
			fnCutscene([[ Append("Breanne: Don't you go picking fights with anything ten feet tall, Florentina!") ]])
		end
	end
end

--Common code.
fnCutsceneBlocker()

-- |[Finish Up]|
--If Florentina is present, path onto Mei and fold the party up.
if(bIsFlorentinaHere) then
	Cutscene_CreateEvent("Move Florentina To Mei", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (20.25 * gciSizePerTile), (14.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()

	--Fold the party positions up.
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
end

--Flags.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasMetFriendly", "N", 1.0)

--Unlock topics.
WD_SetProperty("Unlock Topic", "Job", 1)
WD_SetProperty("Unlock Topic", "Name", 1)

--Clear the flag on Next Move.
WD_SetProperty("Clear Topic Read", "NextMove")
