-- |[ =================================== Meeting Florentina =================================== ]|
--Meeting our favourite roguish merchant.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N", 1.0)

-- |[Variables]|
local sMeiForm         = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
local iMetClaudia      = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetClaudia", "N")
local iExaminedMirror  = VM_GetVar("Root/Variables/Chapter1/Scenes/iExaminedMirror", "N")
local iMeiKnowsRilmani = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiKnowsRilmani", "N")

--Clear censor bars.
WD_SetProperty("Clear Censor Bars")
        
-- |[Achievement]|
AM_SetPropertyJournal("Unlock Achievement", "MeetFlorentina")

-- |[Initial Movements]|
--Mei walks towards the counter.
Cutscene_CreateEvent("Move Mei To Counter", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (33.25 * gciSizePerTile), (20.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Face Mei to the west.
Cutscene_CreateEvent("Face Mei West", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", -1, 0)
DL_PopActiveObject()
fnCutsceneWait(2)
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(5)
fnCutsceneBlocker()

-- |[Dialogue Sequence]|
--Dialogue setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Hypatia", "Neutral") ]])

--Talking.
fnCutscene([[ Append("Hypatia: Hi there![P] Welcome to Trannadar General Goods.[P] My name is Hypatia, may I take your order?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Oh, I'm sorry.[P] I thought this was Florentina's shop.[P] I was told to speak to her.[B][C]") ]])
fnCutscene([[ Append("Hypatia: I'm her assistant.[P] She's out at the moment, but I can help you.[P] What do you need?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Have you ever heard of Earth?[B][C]") ]])
fnCutscene([[ Append("Hypatia: Uh, we don't really stock fertilizer...[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Happy] No, the planet.[P] Earth.[P] Cradle of humanity.[B][C]") ]])
fnCutscene([[ Append("Hypatia: Err...[B][C]") ]])
fnCutscene([[ Append("Hypatia: Sorry, but I have no idea what that is.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Do you think Florentina might?[B][C]") ]])
fnCutscene([[ Append("Hypatia: I'd ask her, but she's, uh, out.[P] Yep.[P] Not here.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] Really?[P] Are you sure she's not in the back, staring at the leftmost shelf?") ]])
fnCutsceneBlocker()

--Hypatia looks around with confusion.
fnCutsceneWait(30)
fnCutsceneBlocker()
fnCutsceneFace("Hypatia", 0, -1)
fnCutsceneWait(30)
fnCutsceneBlocker()
fnCutsceneFace("Hypatia", 1, 0)
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneFace("Hypatia", 0, -1)
fnCutsceneWait(60)
fnCutsceneBlocker()
fnCutsceneFace("Hypatia", 1, 0)
fnCutsceneWait(45)
fnCutsceneBlocker()

-- |[Hypatia goes north to talk to Florentina]|
--Move to the door.
fnCutsceneMove("Hypatia", 31.25, 19.50)
fnCutsceneBlocker()

--Timing.
fnCutsceneWait(5)
fnCutsceneBlocker()

--Open the door, play a sound.
fnCutscene([[AL_SetProperty("Open Door", "Florentina's Rear Door")]])
fnCutscene([[AudioManager_PlaySound("World|OpenDoor")]])
fnCutsceneBlocker()

--Mei looks at Hypatia as she moves.
fnCutsceneFace("Mei", -1, -1)
fnCutsceneWait(5)
fnCutsceneBlocker()

--Hypatia moves into the back room to look at Florentina.
fnCutsceneMove("Hypatia", 31.25, 16.50)
fnCutsceneFace("Hypatia", 0, -1)
fnCutsceneBlocker()

--Brief pause.
fnCutsceneWait(45)
fnCutsceneBlocker()

--Florentina turns around, and presumably says something to Hypatia. No dialogue.
fnCutsceneFace("Florentina", -1, 1)
fnCutsceneWait(120)
fnCutsceneBlocker()

--Change the music.
fnCutscene([[ AudioManager_PlayMusic("FlorentinasTheme") ]])

--Brief pause, Florentina looks in Mei's direction, as does Hypatia.
fnCutsceneFace("Florentina", 0, 1)
fnCutsceneFace("Hypatia", 0, -1)
fnCutsceneWait(30)
fnCutsceneBlocker()

--They resume speaking to one another. Florentina changes facing.
fnCutsceneFace("Florentina", -1, 1)
fnCutsceneFace("Hypatia", -1, 1)
fnCutsceneWait(120)
fnCutsceneBlocker()

--Florentina comes out to the shop floor. Takes a few moves.
fnCutsceneMove("Florentina", 32.25, 17.50)
fnCutsceneMove("Florentina", 31.25, 17.50)
fnCutsceneBlocker()
fnCutsceneFace("Mei", -1, 0)
fnCutsceneMove("Hypatia", 31.25, 19.50)
fnCutsceneMove("Florentina", 31.25, 20.50)
fnCutsceneBlocker()

--Florentina and Hypatia face Mei.
fnCutsceneFace("Florentina", 1, 0)
fnCutsceneFace("Hypatia", 1, 0)
fnCutsceneWait(5)
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(5)
fnCutsceneBlocker()

-- |[Dialogue Sequence]|
--Dialogue setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Hypatia", "Neutral") ]])

--Slime form.
if(sMeiForm == "Slime") then
	fnCutscene([[ Append("Florentina:[E|Neutral] Well you don't look like a debt collector.[P] You're not slimy enough![P][EMOTION|Florentina|Happy] Hah![B][C]") ]])

--Alraune.
elseif(sMeiForm == "Alraune") then
	fnCutscene([[ Append("Florentina:[E|Neutral] You sure don't *look* like a debt collector.[B][C]") ]])
	
--Bee.
elseif(sMeiForm == "Bee") then
	fnCutscene([[ Append("Florentina:[E|Neutral] You sure don't *look* like a debt collector.[B][C]") ]])
	
--Ghost.
elseif(sMeiForm == "Ghost") then
	fnCutscene([[ Append("Florentina:[E|Neutral] You sure don't *look* like a debt collector.[P] You look like a maid.[P] An undead maid.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] It is not a permanent arrangement.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Maybe a bounty hunter, then?[B][C]") ]])

--Gravemarker (NC+)
elseif(sMeiForm == "Gravemarker") then
	fnCutscene([[ Append("Florentina:[E|Neutral] Oh joy, one of the angels.[P] Hit the road.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Surprise] N-[P]no![P] I'm sorry![P] I'm not with them![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Surprise] I was transformed against my will, but I escaped at the last moment![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] So you're not in one of their hit-squads?[B][C]") ]])

--Wisphag (NC+)
elseif(sMeiForm == "Wisphag") then
	fnCutscene([[ Append("Florentina:[E|Surprise] Holy smokes![P] Hypatia, you didn't mention she was a wisphag![B][C]") ]])
	fnCutscene([[ Append("Hypatia:[E|Neutral] That's relevant?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] Hello![P] It's nice to meet you![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Surprise] And a talking one, no less![B][C]") ]])
	fnCutscene([[ Append("Hypatia:[E|Neutral] That's not my fault, as her being able to talk was implied by me saying someone was asking for you.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Surprise] So what are you, wisphag?[P] A debt collector?[P] A bounty hunter?[P] An avenger of fallen souls?[B][C]") ]])

--Mannequin (NC+)
elseif(sMeiForm == "Mannequin") then
	fnCutscene([[ Append("Florentina:[E|Surprise] Oh it's a crappy prank.[P] Let me guess, she's going to jump to life.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Hello.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] Yeah, I'm really scared.[P] Get back to work, Hypatia.[B][C]") ]])
	fnCutscene([[ Append("Hypatia:[E|Neutral] Boss, she was asking for you![P] Really![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I have been told you can help me.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Wow, a talking mannequin.[P] Now I've seen everything except - [B][C]") ]])
	fnCutscene([[ Append("Hypatia:[E|Neutral] You haven't seen everything![P] You've never seen - [B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] A man eat his own head, yeah, good joke Hypatia.[P] Let me handle this.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] So what are you actually?[P] A bounty hunter?[P] A debt collector?[B][C]") ]])

--Default: Human.
else
	fnCutscene([[ Append("Florentina:[E|Neutral] You don't look like a debt collector, not with that outfit.[B][C]") ]])
end
fnCutscene([[ Append("Mei:[E|Neutral] I'm no such thing.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] Yet you know who I am and are looking for Earth, are you?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Happy] You've heard of it?[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] No.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] Crud.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] But.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] But?[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] That stone.[P] Where did you get it?[B][C]") ]])
if(sMeiForm == "Gravemarker") then
    fnCutscene([[ Append("Mei:[E|Laugh] You might need to be a bit more specific![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] A sense of humour is a rare thing to see in an angel.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] The little rock with a glyph you've got on it.[B][C]") ]])
end
fnCutscene([[ Append("Mei:[E|Neutral] You mean this thing?[P] Well, I...[P] I woke up in a dingy basement not far from here, and I was holding it.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] Interesting.[P] Very interesting.[P] Hypatia?[B][C]") ]])
fnCutscene([[ Append("Hypatia: Yes?[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] Do you recall that monk and her little fan club that came through here a few weeks ago?[B][C]") ]])
fnCutscene([[ Append("Hypatia: Um, I think so.[P] They bought a lot of medical cream.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] What was the name of their leader?[B][C]") ]])
fnCutscene([[ Append("Hypatia: ...[P]...[P] I want to say Claudia, but I don't want to sound dumb if I'm way off.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] And wasn't she snooping around for artifacts at the old mansion before she got sent packing by thugs?[B][C]") ]])
fnCutscene([[ Append("Hypatia: I'm going to say maybe.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] Hypatia, are you thinking what I'm thinking?[B][C]") ]])
fnCutscene([[ Append("Hypatia: I think so, boss.[P] But if the electric charge of a particle is smaller when measured at a distance, doesn't that imply that zero-point energy isn't non-zero, and that the fine-structure constant isn't a constant?[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Surprise] What?[B][C]") ]])
fnCutscene([[ Append("Hypatia: What?[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Facepalm] Hypatia...[P] You never cease to amaze...[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] Ahem.[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] I might have to step out for a bit.[P] For real, this time.[P] You're going to be in charge for a while.[B][C]") ]])
fnCutscene([[ Append("Hypatia: All right![P] I won't let you down![B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] Just don't forget to lock up after closing.[B][C]") ]])

--Hasn't met Claudia, hasn't examined the mirror, doesn't know Rilmani:
if(iMetClaudia == 0.0 and iExaminedMirror == 0.0 and iMeiKnowsRilmani == 0.0) then
	fnCutscene([[ Append("Mei:[E|Blush] What's so interesting about this stone?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] Did it occur to you that the stone might be valuable when you appeared so far away that nobody even knows where you're from, clutching it?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] Well, no. I figured the creepy cult abducted me.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] I've been doing a lot of running and fighting since then, so I haven't really had time to think on it.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] The stone has a rune on it.[P] I don't know what it is, but I've definitely seen it somewhere before.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] And I know someone who will pay top platina for a chance to see it.[P] So, I'm going to help you find that someone.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] How altruistic of you.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] ...[P] I'll be enjoying a nice finder's fee, of course.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] So glad I could help your bottom line.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Confused] You're getting what you want, I suspect.[P] That's a very old symbol, probably something from the second age.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] Wherever Earth is, Claudia will know.[P] I'd even bet it's connected to that rune, too.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Besides, I need to find someplace to be where certain elements cannot locate me.[P] Hanging out with a drifter is as good a place as any.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] I'm not a drifter![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] Maybe not where you're from.[P] Welcome to Pandemonium, by the way.") ]])

--Mei has examined the exit mirror:
elseif(iExaminedMirror == 1.0) then
	fnCutscene([[ Append("Mei:[E|Happy] Well, I actually kind of found a mirror that acts as a portal...[P] and there's Rilmani writing on it...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Surprise] A Rilmani artifact?[P] Are you serious?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] Take me to it![P] I can probably sell that for -[P][CLEAR]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] Okay, okay![P] Thanks for you help!") ]])

--Mei knows Rilmani:
elseif(iMeiKnowsRilmani == 1.0) then
	fnCutscene([[ Append("Mei:[E|Happy] I found a journal Claudia left, it seems to indicate that this runestone is a Rilmani artifact.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Surprise] A Rilmani artifact?[P] Are you serious?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Do you know how I can use it to get back to Earth?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] No, but I bet if we can find another artifact it will help.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] More importantly, then I can sell it and make -[P] if I give you ten percent, you'll never have to work again![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Oh, how...[P] generous?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] The Rilmani are supposed to be a legend, but if you say that your runestone is one of theirs...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] We should go check out the big mansion near the lake.[P] It's called the Dimensional Trap.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Some old stories say it's a Rilmani building or something.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] But that's where I came from...[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] That's just adding more evidence.[P] Let's go!") ]])

--Has met Claudia:
else
	fnCutscene([[ Append("Mei:[E|Happy] Claudia, the monk?[P] I ran into her in the dungeon![P] Sort of![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] Well, that's good.[P] So you'll know where she is, then.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Actually, she said this stone had something to do with the Rilmani.[P] Do you know how to translate it?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Claudia would know more about that than I would.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] She said she had a translation guide but lost it in the mansion way north of here.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Seems you really get around.[P] Have you looked for it?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I couldn't find it.[P] Will you help me?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] Sure -[P] on the condition that I get to keep a share of the loot.[P] That mansion has been unassailable by treasure hunters for quite a while...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I just want to find a way home.[P] You can keep whatever we find.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] It also helps that I be out of the shop.[P] There are some people I'd rather not deal with who would like a word with me.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Wandering in the forest with a drifter is far preferable.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I'm not a drifter![P] I'm lost![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] Welcome to Pandemonium, where getting lost is half the fun.") ]])

end

fnCutsceneBlocker()

-- |[Movement Sequence]|
--Wait a bit.
fnCutsceneWait(5)
fnCutsceneBlocker()

--Florentina walks around the counter.
Cutscene_CreateEvent("Face Hypatia South", "Actor")
	ActorEvent_SetProperty("Subject Name", "Hypatia")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()
Cutscene_CreateEvent("Move Florentina South", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (31.25 * gciSizePerTile), (21.50 * gciSizePerTile))
DL_PopActiveObject()
Cutscene_CreateEvent("Move Florentina East", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (33.25 * gciSizePerTile), (21.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--They look at each other.
Cutscene_CreateEvent("Face Florentina North", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Face", 0, -1)
DL_PopActiveObject()
Cutscene_CreateEvent("Face Mei South", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Dialogue Sequence]|
--Dialogue setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])

--Hasn't met Claudia:
if(iMetClaudia == 0.0 and iMeiKnowsRilmani == 0.0 and iExaminedMirror == 0.0) then
	fnCutscene([[ Append("Florentina:[E|Neutral] Okay, listen up.[P] We need to head to Outland Farm, which is a ways up north.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] We're looking for Sister Claudia.[P] I don't know if she's still there, but that's where she said she was headed.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Will she know how to get back to Earth?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Maybe.[P] She was doing some kind of study of monsters in the region.[P] She's been all over the world, so we could do a lot worse than talking to her.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] I also heard Nadia going on about a witch who moved into the swamps west of here.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] I don't know anything about her, and the swamps are a pretty rough place, but I'll leave the call to you.[P] We could ask her about it if you want.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] All right![P] Finally, some good news![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Surprise] Don't get too excited.[P] I'm not making any guarantees here.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] You're on point.[P] Lead the way.") ]])

--Has met Claudia:
else
	fnCutscene([[ Append("Florentina:[E|Happy] Since you seem to know your way around, I trust you'll be fine leading the way.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] You're on point.[P] Let's go.") ]])
end

fnCutsceneBlocker()

-- |[Florentina Joins the Party!]|
--Wait a bit.
fnCutsceneWait(5)
fnCutsceneBlocker()

--Florentina walks onto Mei.
Cutscene_CreateEvent("Move Florentina to Mei", "Actor")
	ActorEvent_SetProperty("Subject Name", "Florentina")
	ActorEvent_SetProperty("Move To", (33.25 * gciSizePerTile), (20.49 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(15)
fnCutsceneBlocker()

-- |[Indicator]|
--Inform the player that Florentina has joined the party.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ Append("Florentina has joined the party!") ]])
fnCutsceneBlocker()

--Music changes back.
fnCutscene([[ AudioManager_PlayMusic("TownTheme") ]])
fnCutsceneWait(30)
fnCutsceneBlocker()

--Fold the party positions up.
fnCutscene([[ AL_SetProperty("Fold Party") ]])
fnCutsceneBlocker()

-- |[System]|
--These lines add Florentina to the party. They execute as soon as the scene starts, but take effect afterwards.
fnAddPartyMember("Florentina")

--Florentina's equipment
if(gbHasFlorentinasEquipment == false) then
	gbHasFlorentinasEquipment = true
	LM_ExecuteScript(gsItemListing, "Hunting Knife")
	LM_ExecuteScript(gsItemListing, "Flowery Tunic")
	LM_ExecuteScript(gsItemListing, "Florentina's Pipe")
	AdvCombat_SetProperty("Push Party Member", "Florentina")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Weapon",      "Hunting Knife")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Body",        "Flowery Tunic")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Accessory A", "Florentina's Pipe")
	DL_PopActiveObject()
end

--Unlock dialogue topics.
WD_SetProperty("Unlock Topic", "Florentina", 1)
WD_SetProperty("Unlock Topic", "Claudia", 1)
WD_SetProperty("Unlock Topic", "OutlandFarm", 1)
WD_SetProperty("Unlock Topic", "Pandemonium", 1)
WD_SetProperty("Unlock Topic", "Eyepatch", 1)
WD_SetProperty("Unlock Topic", "Quantir", 1)
WD_SetProperty("Unlock Topic", "Swamp Witch", 1)

--Normalize her EXP with Mei's. She will have 80% to 120% of her EXP and JP.
AdvCombat_SetProperty("Push Party Member", "Mei")
    local iMeiExp = AdvCombatEntity_GetProperty("Exp")
    local iMeiJP  = AdvCombatEntity_GetProperty("Total JP")
DL_PopActiveObject()

--Set.
AdvCombat_SetProperty("Push Party Member", "Florentina")

    --Florentina's EXP scatter.
    local fEXPRoll = LM_GetRandomNumber(80, 120) / 100.0
    AdvCombatEntity_SetProperty("Current Exp", math.floor(iMeiExp * fEXPRoll))
    
    --JP. Florentina gets global JP for every point Mei has total, scattered.
    local fJPRoll = LM_GetRandomNumber(80, 120) / 100.0
    AdvCombatEntity_SetProperty("Current JP", math.floor(iMeiJP * fJPRoll))

DL_PopActiveObject()
