-- |[ ================================== Florentina's Goodbye ================================== ]|
--This is an interstitial script, not a cutscene script. It executes in the middle of an existing dialogue.

-- |[Variables]|
--Quests completed.
local iTakenPieJob                = VM_GetVar ("Root/Variables/Chapter1/Breanne/iTakenPieJob", "N")
local iSavedClaudia               = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedClaudia", "N")
local iSavedBeehive               = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedBeehive", "N")
local iMeiTriedFruit              = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiTriedFruit", "N")
local iCompletedTrapDungeon       = VM_GetVar("Root/Variables/Chapter1/Scenes/iCompletedTrapDungeon", "N")
local iCompletedQuantirMansion    = VM_GetVar("Root/Variables/Chapter1/Scenes/iCompletedQuantirMansion", "N")
local iDefeatedMycela             = VM_GetVar("Root/Variables/Chapter1/Sharelock/iDefeatedMycela", "N")
local iHasSeenFlorentinaTwentyWin = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenFlorentinaTwentyWin", "N")

--Forms Mei has:
local iHasAlrauneForm     = VM_GetVar("Root/Variables/Global/Mei/iHasAlrauneForm",     "N")
local iHasBeeForm         = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm",         "N")
local iHasGhostForm       = VM_GetVar("Root/Variables/Global/Mei/iHasGhostForm",       "N")
local iHasGravemarkerForm = VM_GetVar("Root/Variables/Global/Mei/iHasGravemarkerForm", "N")
local iHasSlimeForm       = VM_GetVar("Root/Variables/Global/Mei/iHasSlimeForm",       "N")
local iHasWerecatForm     = VM_GetVar("Root/Variables/Global/Mei/iHasWerecatForm",     "N")
local iHasMannequinForm   = VM_GetVar("Root/Variables/Global/Mei/iHasMannequinForm",   "N")
local iHasWisphagForm     = VM_GetVar("Root/Variables/Global/Mei/iHasWisphagForm",     "N")

--Special:
local iMeiLovesAdina = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiLovesAdina", "N")

--Other.
local iMetSharelock       = VM_GetVar("Root/Variables/Chapter1/Sharelock/iMetSharelock", "N")
local iMihoLostContest    = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iMihoLostContest", "N")
local iMetPolaris         = VM_GetVar("Root/Variables/Chapter1/Polaris/iMetPolaris", "N")
local iSawSlimevilleIntro = VM_GetVar("Root/Variables/Chapter1/Scenes/iSawSlimevilleIntro", "N")

-- |[Opinion Computations]|
--Compute Florentina's general opinion of Mei:
local iFlorentinaOpinion = 0.0
if(iTakenPieJob                == 2.0) then iFlorentinaOpinion = iFlorentinaOpinion + 1 end
if(iSavedClaudia               == 1.0) then iFlorentinaOpinion = iFlorentinaOpinion + 1 end
if(iSavedBeehive               == 1.0) then iFlorentinaOpinion = iFlorentinaOpinion + 1 end
if(iMeiTriedFruit              == 1.0) then iFlorentinaOpinion = iFlorentinaOpinion + 1 end
if(iCompletedTrapDungeon       == 1.0) then iFlorentinaOpinion = iFlorentinaOpinion + 1 end
if(iCompletedQuantirMansion    == 1.0) then iFlorentinaOpinion = iFlorentinaOpinion + 1 end
if(iDefeatedMycela             == 1.0) then iFlorentinaOpinion = iFlorentinaOpinion + 1 end
if(iMihoLostContest            == 1.0) then iFlorentinaOpinion = iFlorentinaOpinion + 1 end
if(iHasSeenFlorentinaTwentyWin == 1.0) then iFlorentinaOpinion = iFlorentinaOpinion + 1 end

--Compute Florentina's general adventure state:
local iFlorentinaAdventure = 0.0
if(iSavedBeehive               == 1.0) then iFlorentinaAdventure = iFlorentinaAdventure + 1 end
if(iCompletedTrapDungeon       == 1.0) then iFlorentinaAdventure = iFlorentinaAdventure + 1 end
if(iCompletedQuantirMansion    == 1.0) then iFlorentinaAdventure = iFlorentinaAdventure + 1 end
if(iHasSeenFlorentinaTwentyWin == 1.0) then iFlorentinaAdventure = iFlorentinaAdventure + 1 end
if(iDefeatedMycela             == 1.0) then iFlorentinaAdventure = iFlorentinaAdventure + 1 end
if(iMihoLostContest            == 1.0) then iFlorentinaAdventure = iFlorentinaAdventure + 1 end

-- |[Scenes]|
--Florentina's general opinion of Mei is low, so they haven't known each other long:
if(iFlorentinaOpinion < 2) then
	fnCutscene([[ Append("Mei:[E|Sad] Hey, Florentina?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] This is the part where you say goodbye, right?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Sad] I know we didn't spend that much time together, but...[P] I'll miss you.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Heh.[P] Yeah, it was a bit of fun.[P] Now it's back to the grind of running a store.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] If you're ever in the dimensional neighbourhood and you want to go on an adventure, look me up, yeah?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] You mean that?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] It's nice to stretch my legs.[P] Plus, it keeps me away from the usual crop of bounty hunters and general sorholes who infest the Trading Post.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Well.[P] Okay![P] I'll think of you when I'm home![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Likewise.[P] Go get 'em, kid.") ]])

--Florentina's opinion of Mei is middling:
elseif(iFlorentinaOpinion < 4) then
	fnCutscene([[ Append("Mei:[E|Sad] Florentina...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Cry] I'm gonna miss you![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Ha ha![P] Crying![P] I love it![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] I'll miss you too, kid.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Sad] If there's a way to visit I'll try to![P] Thanks so much for all your help![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Hey, uh, you're probably not going to need the platina when you're on Earth...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Sad] ...[P][EMOTION|Mei|Laugh] Ha ha ha ha![P] Sure, take it![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Laugh] But you owe me![P] I'll come back so you can work off your debt![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Of course.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Go get em, kid.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] All right![P] Here I go!") ]])

-- |[High Opinion]|
--Advanced, has a bunch of extra cases for doing various quests.
else

    --Common.
	fnCutscene([[ Append("Mei:[E|Sad] Florentina...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Cry] I'm gonna miss you![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] Okay, Mei?[P] I know you're going to be jumping dimensions or whatever but...[B][C]") ]])
	fnCutscene([[ Append("[P]*Hug*[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] If you tell anyone I did that I will hunt you down.[P] Even if you tell people on Earth, I [P]*will*[P] find you.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] F-[P]Florentina![P] You're the best![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] You've got a home to go to, right?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] I just have to let everyone know I'm all right.[P] If I can come back, I will![B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] Whatever for?[B][C]") ]])
	
    --Quest and/or Character Cases
	if(iTakenPieJob == 2.0) then
		fnCutscene([[ Append("Mei:[E|Happy] I didn't have a lot of friends back home...[P] But I do here![P] With you![P] And Breanne![P] And Nadia, and everyone else![B][C]") ]])
	else
		fnCutscene([[ Append("Mei:[E|Happy] I didn't have a lot of friends back home...[P] But I do here![P] With you![B][C]") ]])
	end
    if(iMetSharelock == 1.0) then
		fnCutscene([[ Append("Mei:[E|Happy] And I'd love to catch up with Share;Lock.[P] Maybe help her find out where she's really from.[B][C]") ]])
    end
	if(iMeiLovesAdina == 1.0) then
		fnCutscene([[ Append("Mei:[E|Happy] And, I want to be with my Mistress...[P] and I want to help with the salt flats...[B][C]") ]])
	end
	if(iHasAlrauneForm == 1.0) then
		fnCutscene([[ Append("Mei:[E|Happy] Oh, and I want to be with my leaf-sisters, too...[B][C]") ]])
	end
	if(iHasBeeForm == 1.0) then
		fnCutscene([[ Append("Mei:[E|Happy] And the drones will miss me, too.[P] I can't forget them...[B][C]") ]])
	end
	if(iHasWerecatForm == 1.0) then
		fnCutscene([[ Append("Mei:[E|Happy] And I want to stalk the night with my pride...[B][C]") ]])
	end
    if(iHasWisphagForm == 1.0) then
		fnCutscene([[ Append("Mei:[E|Happy] And help all the wisphags with their sacred duty...[B][C]") ]])
    end
    if(iSawSlimevilleIntro == 1.0 and iHasSlimeForm == 1.0) then
		fnCutscene([[ Append("Mei:[E|Happy] And all the silly slimes at Slimeville![P] I could spend a year there just hanging out![B][C]") ]])
    end
    if(iMihoLostContest == 1.0) then
		fnCutscene([[ Append("Mei:[E|Happy] And there's the kitsune Miho![P] I'm going to miss her![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] You know she did intend to haul me off in chains, right?[B][C]") ]])
    end
    if(iHasMannequinForm == 1.0) then
		fnCutscene([[ Append("Mei:[E|Happy] And we need to spook more people by hanging out in the forest at night as mannequins.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] I think I may have rubbed off on you a bit too much.[B][C]") ]])
    end
	if(iHasGhostForm == 1.0) then
		fnCutscene([[ Append("Mei:[E|Happy] And...[P] we must tell the story of Natalie and all those who died...[B][C]") ]])
	end
    if(iMetPolaris == 1.0) then
		fnCutscene([[ Append("Mei:[E|Happy] And Polaris, and Dot![P] Oh I want to thank them for their help.[P] There's a lot I could learn just by talking to her.[B][C]") ]])
    end
	
    --If enough quests were done...
	if(iFlorentinaAdventure >= 2) then
		fnCutscene([[ Append("Florentina:[E|Neutral] Don't forget there's still a cult that we can visit violence upon.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] Oh yeah.[P] Them.[P] Will you keep an eye on them while I'm away?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Just one, naturally.[P] It's in good hands.[B][C]") ]])
	end
	
    --Common ending.
	fnCutscene([[ Append("Florentina:[E|Happy] I guess it hasn't been so bad, has it.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] I -[P] I get the feeling I'll be back.[P] It's the strange feeling I get, like I always knew.[P] I've been getting it a lot since I've been here.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Happy] I'm not really good at goodbyes, so get going before I throw you through this mirror.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] All right![P] Here I go![P] Goodbye, Florentina! [P]Bye for now!") ]])
end
