-- |[ ===================================== Relive Handler ===================================== ]|
--Special script, used for (most) of the instances of the player choosing to relive a transformation scene.
-- The scene requires the name of the transformation to be passed to it. The C++ code will pass the DISPLAY NAME
-- of the scene in. We would therefore expect "Transform: Alraune", for example.

-- |[Argument Check]|
--Verify.
if(fnArgCheck(1) == false) then return end

--Set.
local sSceneName = LM_GetScriptArgument(0)

-- |[Storage]|
--Store Mei's current form.
local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
VM_SetVar("Root/Variables/Chapter1/Scenes/sOriginalForm", "S", sMeiForm)

--Indicate we are reliving a scene. This causes them to end earlier or at different times.
VM_SetVar("Root/Variables/Chapter1/Scenes/iIsRelivingScene", "N", 1.0)

-- |[Common]|
--Wait a bit.
fnCutsceneWait(10)
fnCutsceneBlocker()

--Common.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])

--Handle Florentina case:
local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
if(bIsFlorentinaPresent == true) then
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
end

-- |[ ======================================== Execution ======================================= ]|
-- |[Alraune]|
if(sSceneName == "Alraune" or sSceneName == "Alraune (V)") then
	
	--Flag this relive sequence.
	VM_SetVar("Root/Variables/Chapter1/Scenes/sLastRelivedScene", "S", "Alraune")
    
    --Block autosaves.
    VM_SetVar("Root/Variables/Global/Autosave/iBlockAutosave", "N", 1.0)
	
	--Dialogue.
	fnCutscene([[ Append("Mei:[E|Blush] (Ah, becoming a leaf-sister...)[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] (Such a wonderful memory...)") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Execute the scene.
    if(sSceneName == "Alraune") then
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/200 Defeat/Defeat_Alraune/Debug_Startup.lua") ]])
    else
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/400 Volunteer/Alraune Pitstop/Debug_Startup.lua") ]])
    end
	
-- |[Bee]|
elseif(sSceneName == "Bee" or sSceneName == "Bee (V)") then
	
	--Flag this relive sequence.
	VM_SetVar("Root/Variables/Chapter1/Scenes/sLastRelivedScene", "S", "Bee")
    
    --Block autosaves.
    VM_SetVar("Root/Variables/Global/Autosave/iBlockAutosave", "N", 1.0)
	
	--Dialogue.
	fnCutscene([[ Append("Mei:[E|Blush] (Hmmm, when I first became a drone of the hive...)[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] (The taste of the honey, the buzzing blotting out my senses...[P] the first time I got my feelers...)") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Execute the scene.
    if(sSceneName == "Bee") then
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/200 Defeat/Defeat_Bee/Debug_Startup.lua") ]])
    else
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/400 Volunteer/Beehive/Scene_Begin.lua") ]])
    end
    
-- |[Ghost]|
elseif(sSceneName == "Ghost" or sSceneName == "Ghost (V)") then
	
	--Flag this relive sequence.
	VM_SetVar("Root/Variables/Chapter1/Scenes/sLastRelivedScene", "S", "Ghost")
    
    --Block autosaves.
    VM_SetVar("Root/Variables/Global/Autosave/iBlockAutosave", "N", 1.0)
	
	--Dialogue.
	fnCutscene([[ Append("Mei:[E|Sad] (Natalie...)[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Sad] (I feel like I've inherited more than just her memories.[P] Is she really a part of me?)[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] (It's okay Natalie, your memory will be honored...)") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Execute the scene.
    if(sSceneName == "Ghost") then
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/200 Defeat/Defeat_Ghost/Debug_Startup.lua") ]])
    else
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/400 Volunteer/Ghost/Scene_Begin.lua") ]])
    end

-- |[Gravemarker]|
elseif(sSceneName == "Gravemarker") then
	
	--Flag this relive sequence.
	VM_SetVar("Root/Variables/Chapter1/Scenes/sLastRelivedScene", "S", "Gravemarker")
    
    --Block autosaves.
    VM_SetVar("Root/Variables/Global/Autosave/iBlockAutosave", "N", 1.0)
	
	--Dialogue.
	fnCutscene([[ Append("Mei:[E|Sad] (Becoming a stone angel...[P] the happiest moment of my life, but a lie...)[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Sad] (But was it a lie?[P] Or was the light trying to tell me something else entirely?)") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Execute the scene.
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/200 Defeat/Defeat_Gravemarker/Debug_Startup.lua") ]])

-- |[Slime]|
elseif(sSceneName == "Slime" or sSceneName == "Slime (V)" or sSceneName == "Slime (M)") then
	
	--Flag this relive sequence.
	VM_SetVar("Root/Variables/Chapter1/Scenes/sLastRelivedScene", "S", "Slime")
    
    --Block autosaves.
    VM_SetVar("Root/Variables/Global/Autosave/iBlockAutosave", "N", 1.0)
	
	--Dialogue.
	fnCutscene([[ Append("Mei:[E|Happy] (There really is nothing more cathartic than just letting go of everything and squishing together in a big heap...)[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] (Wait that sounded really stupid...[P][EMOTION|Mei|Smirk] which is okay.[P] Slimes like me don't really have brains!)") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Execute the scene.
    if(sSceneName == "Slime") then
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/200 Defeat/Defeat_Slime/Debug_Startup.lua") ]])
    elseif(sSceneName == "Slime (V)") then
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/400 Volunteer/Slimeville/Scene_Begin.lua") ]])
    else
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/400 Volunteer/Slimecave/Scene_Begin.lua") ]])
    end

-- |[Werecat]|
elseif(sSceneName == "Werecat" or sSceneName == "Werecat (V)") then
	
	--We need to store this value. It can't be overwritten in our heads!
	local iMeiHasDoneNadia = VM_GetVar("Root/Variables/Chapter1/ScenesWerecat/iMeiHasDoneNadia", "N")
	VM_SetVar("Root/Variables/Chapter1/ScenesWerecat/iMeiHasDoneNadiaStore", "N", iMeiHasDoneNadia)
	
	--Flag this relive sequence.
	VM_SetVar("Root/Variables/Chapter1/Scenes/sLastRelivedScene", "S", "Werecat")
    
    --Block autosaves.
    VM_SetVar("Root/Variables/Global/Autosave/iBlockAutosave", "N", 1.0)
	
	--Dialogue.
	fnCutscene([[ Append("Mei:[E|Blush] (The night...[P] my kinfangs...[P] the smell of their fur...[P] the thrill of the hunt...)[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] (I'm better, stronger, faster, than I've ever been -[P] and I love it!)") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Execute the scene.
    if(sSceneName == "Werecat") then
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/200 Defeat/Defeat_Werecat/Debug_Startup.lua") ]])
    else
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/400 Volunteer/Werecassandra/Scene_Begin.lua") ]])
    end

-- |[Zombee]|
elseif(sSceneName == "Zombee") then
	
	--Flag this relive sequence.
	VM_SetVar("Root/Variables/Chapter1/Scenes/sLastRelivedScene", "S", "Zombee")
    
    --Block autosaves.
    VM_SetVar("Root/Variables/Global/Autosave/iBlockAutosave", "N", 1.0)
	
	--Dialogue.
	fnCutscene([[ Append("Mei:[E|Sad] (What a terrible thing, to reduce someone to a mere speck and subsume them in their moment of weakness...)[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] (The villains behind this will face justice in the end, I swear it.)") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Execute the scene.
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/200 Defeat/Defeat_Zombee/Debug_Startup.lua") ]])

-- |[Wisphag]|
elseif(sSceneName == "Wisphag" or sSceneName == "Wisphag (V)") then
	
	--Flag this relive sequence.
	VM_SetVar("Root/Variables/Chapter1/Scenes/sLastRelivedScene", "S", "Wisphag")
    
    --Block autosaves.
    VM_SetVar("Root/Variables/Global/Autosave/iBlockAutosave", "N", 1.0)
	
	--Dialogue.
	fnCutscene([[ Append("Mei:[E|Blush] (Ah, the embrace of the earth, the gentle burning of the soul...)[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] (The nearly drowning part, okay, that sucked, but...)[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] (Oh to be transformed into a wisphag again...)") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Execute the scene.
    if(sSceneName == "Wisphag") then
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/200 Defeat/Defeat_Wisphag/Debug_Startup.lua") ]])
    else
        fnCutscene([[ AL_BeginTransitionTo("WisphagSwampA", "FORCEPOS:4.0x1.0x0") ]])
    end

-- |[Mannequin]|
elseif(sSceneName == "Mannequin") then
	
    --If Florentina isn't here, you can't run this scene :3
    if(EM_Exists("Florentina") == false) then
        fnCutscene([[ Append("Mei:[E|Neutral] (Turning into a mannequin...[P] Wait, my memory is fuzzy.[P] Too fuzzy to remember what happened.)[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] (I would need to meet a blue-skinned plant lady first, but who?[P] It is a mystery.)") ]])
        fnCutsceneBlocker()
    
        return
    end
    
	--Flag this relive sequence.
	VM_SetVar("Root/Variables/Chapter1/Scenes/sLastRelivedScene", "S", "Mannequin")
    
    --Block autosaves.
    VM_SetVar("Root/Variables/Global/Autosave/iBlockAutosave", "N", 1.0)
	
	--Dialogue.
	fnCutscene([[ Append("Mei:[E|Sad] (To lose myself, my body, my very identity.[P] Have it stripped away and become just another thing, to be used...)[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] (It's oddly exciting...)") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Execute the scene.
    fnCutscene([[ AL_BeginTransitionTo("BanditForestScene", "FORCEPOS:1.0x1.0x0") ]])
    
end
