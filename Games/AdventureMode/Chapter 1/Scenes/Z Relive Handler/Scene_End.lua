-- |[ ===================================== Reliving Ends ====================================== ]|
--After reliving a scene, return to the save point and re-add Florentina to the party if she got removed and play any dialogue that occurs.
VM_SetVar("Root/Variables/Chapter1/Scenes/iIsRelivingScene", "N", 0.0)
    
--Unblock autosaves.
VM_SetVar("Root/Variables/Global/Autosave/iBlockAutosave", "N", 0.0)

-- |[ ========== Handle Florentina =========== ]|
--First, check if Florentina should be in the party but was removed. If so, re-add her.
local bIsFlorentinaHere = false
local iHasSeenTrannadarFlorentinaScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
if(iHasSeenTrannadarFlorentinaScene == 1.0) then

	--Flag.
	bIsFlorentinaHere = true

	--If Florentina is currently in the party, we don't need to do anything.
	local bIsFlorentinaPresent = false
	for i = 1, giFollowersTotal, 1 do
		if(gsaFollowerNames[i] == "Florentina") then
			bIsFlorentinaPresent = true
		end
	end
	
	--Re-add code:
	if(bIsFlorentinaPresent == false) then
        fnAddPartyMember("Florentina")
		AL_SetProperty("Fold Party")
	end
end

-- |[ ========= Handle Original Form ========= ]|
--Return Mei to her original form:
local sOriginalForm = VM_GetVar("Root/Variables/Chapter1/Scenes/sOriginalForm", "S")
if(sOriginalForm == "Human") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Human.lua")
elseif(sOriginalForm == "Alraune") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Alraune.lua")
elseif(sOriginalForm == "Bee") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Bee.lua")
elseif(sOriginalForm == "Ghost") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Ghost.lua")
elseif(sOriginalForm == "Gravemarker") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Gravemarker.lua")
elseif(sOriginalForm == "Slime") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Slime.lua")
elseif(sOriginalForm == "Werecat") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Werecat.lua")
elseif(sOriginalForm == "Mannequin") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Mannequin.lua")
elseif(sOriginalForm == "Wisphag") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Wisphag.lua")
end

-- |[ =========== Scene Execution ============ ]|
-- |[Common]|
--Dialogue setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
if(bIsFlorentinaHere == true) then
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
end

-- |[Alraune]|
local sLastRelivedScene = VM_GetVar("Root/Variables/Chapter1/Scenes/sLastRelivedScene", "S")
if(sLastRelivedScene == "Alraune" or sLastRelivedScene == "AlrauneVolunteer") then
	if(bIsFlorentinaHere == false) then
		fnCutscene([[ Append("Mei:[E|Neutral] (Such a wonderful feeling, being joined...)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] (But, I have to keep going.[P] Got to find a way home!)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] (Then I can join my very own leaf-sisters on Earth![P] Lord knows the environment could use some tending...)") ]])
	else
		fnCutscene([[ Append("Mei:[E|Neutral] Mmmmm...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Were you spacing out or something?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I was just remembering what it was like to be joined.[P] Was it pleasant for you?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Blush] It did feel pretty great...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] It was a very long time ago, Mei.[P] In fact, I really only clearly remember the feeling as the waters soaked into me.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] As far as these things go, I guess Alraune is one of the better ones.[P] Life could certainly be a lot worse.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Yeah...") ]])
	end
	
-- |[Bee]|
elseif(sLastRelivedScene == "Bee") then
	if(bIsFlorentinaHere == false) then
		fnCutscene([[ Append("Mei:[E|Neutral] (It's such a tragedy that I might have to leave my drone sisters behind and go back to Earth.)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] (But if there's a way I can return, I know I will -[P][EMOTION|Mei|Happy] and I'll have all sorts of rare nectars for them!)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] (Not to mention I could turn the humans there into new drones...[P] but I'm getting ahead of myself!)") ]])
	else
		fnCutscene([[ Append("Mei:[E|Neutral] Mmmmm...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] You've got a distant look about you.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Smirk] I was just thinking about how I joined the hive.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Smirk] It's really a shame you can't.[P] You don't know what you're missing![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Yeah, I'd pass.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] You wouldn't be such a grumpus with all the other drones encouraging you and helping you and thinking for you...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] You'd just slip away and mindlessly follow orders...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] *snap*[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Wake up, kid.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] Don't snap your fingers at me![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Why do you want to mindlessly follow orders all of the sudden?[P] Don't you like being,[P] well,[P] *you*?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] \"She who makes a beast of herself, relieves the pain of being a woman.\"[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] ...[P] Earth quote?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] It's...[P] apt.[P] There must have been a lot of wise Earthers.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] \"It is the province of knowledge to speak, and it is the privilege of wisdom to listen.\"[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] I spent a lot of my spare time reading things -[P] and of course, watching cat videos.[P] If you hear me say something smart, I probably copied it from someone wiser than I.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] That's okay.[P] There's nothing new under the sun.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Smirk] Hmm...[P] I think I've heard that on Earth, too...") ]])
	end
	
-- |[Ghost]|
elseif(sLastRelivedScene == "Ghost") then
	if(bIsFlorentinaHere == false) then
		fnCutscene([[ Append("Mei:[E|Sad] (Lydie, Natalie, Laura, Joel...)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Cry] (They died so young...)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] (But I'll be live enough for all of us -[P] I mean, them!)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Smirk] (Now, to find a way home!)") ]])
	else
		fnCutscene([[ Append("Mei:[E|Sad] *sigh*[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] ...[P] Do you want to talk about it?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] The spirit of Natalie...[P] she lives on through me.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I'm not sure if I've merged with her or maybe just got her memories...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] I'm not an expert on possession.[P] Sorry.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Really, I'm not that sad about it.[P] I think she is, and that affects me.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Smirk] But if we live our lives to the fullest, she won't have to be sad anymore.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Well that's - [P][CLEAR]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] She also died a virgin.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] O -[P] kay?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Smirk] I don't know why I felt the need to point that out, but I did.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Blush] Maybe we should go get Natalie laid?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] Oh I am [P]*way*[P] ahead of you on that one...") ]])
	end
    
-- |[Gravemarker]|
elseif(sLastRelivedScene == "Gravemarker") then
    fnCutscene([[ Append("Mei:[E|Neutral] I guess it's not so bad.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] What's not so bad?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] The angels turning people to stone guardians.[P] They take all the pain away and fill you with happiness.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] And how is that no so bad?[P] I don't want to be someone else's mindless pawn.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] And if someone took away that feeling, you wouldn't mind.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] No, Mei.[P] I wouldn't mind, because I wouldn't be me.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Do you want to be you?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Blush] ...[P] Sometimes I'm okay with not being me.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Huh.[P] Your call, I suppose, but it's not my sort of thing.") ]])
	
-- |[Slime]|
elseif(sLastRelivedScene == "Slime") then
	if(bIsFlorentinaHere == false) then
		fnCutscene([[ Append("Mei:[E|Blush] (Just thinking about being so squishy...)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] (Ack![P] Stay focused and find a way home![P] I'll be shlicking all day if I don't focus!)") ]])
	else
		fnCutscene([[ Append("Mei:[E|Blush] Oooohh...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] Mei, I swear that if you start touching yourself right in front of me...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] Ungghh...[P] Sorry...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Smirk] I was just remembering when I first became a slime.[P] It was -[P] quite an experience.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Did it make your hormones go out of control?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] They are exactly where I want them to be![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Smirk] I guess I've just been so horny since I ended up on Pandemonium...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] It's totally natural.[P] Just, please try to control yourself when we're out here.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] I don't need you as an uncoordinated heap when we could get jumped at any minute.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] (I'll just wait until later, and then...[P] I'll squish...)") ]])
	end
	
-- |[Werecat]|
elseif(sLastRelivedScene == "Werecat") then
	
	--Restore this variable.
	local iMeiHasDoneNadiaStore = VM_GetVar("Root/Variables/Chapter1/ScenesWerecat/iMeiHasDoneNadiaStore", "N")
	VM_SetVar("Root/Variables/Chapter1/ScenesWerecat/iMeiHasDoneNadia", "N", iMeiHasDoneNadiaStore)
	
	--If Mei is alone:
	if(bIsFlorentinaHere == false) then
		fnCutscene([[ Append("Mei:[E|Happy] (Okay world, better hang on to your hat![P] You're about to meet the original furry night hunter!)") ]])
	
	--If Florentina is here:
	else
		fnCutscene([[ Append("Mei:[E|Blush] Mmmm...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Thinking of something nice?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] Yeah...[P] when I first felt the moon's grace...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] You're kind of an odd werecat.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Smirk] I truly stand for the principles we werecats are supposed to follow.[P] Honorable hunts, peerless agility, flawless poise...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] You just know those instinctively?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] It's like sort of a contract with the moon.[P] It makes us its perfect night hunters, in exchange we uphold its values.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] And what about balls of string?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Smirk] ...[P] Don't tempt me...") ]])
	end
	
-- |[Zombee]|
elseif(sLastRelivedScene == "Zombee") then
    if(fnIsFlorentinaPresent() == true) then
        fnCutscene([[ Append("Mei:[E|Sad] The horrors I saw in that bee hive...[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] But you're here.[P] Standing in front of me.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] So long as we're here, we're not out of it.[P] Okay?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] It's the only thing I've ever been truly afraid of...[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Dwelling on it isn't going to help.[P] Come on.") ]])
    else
        fnCutscene([[ Append("Mei:[E|Sad] The horrors I saw in that bee hive...[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] I won't dwell on it, but...[P] No.[P] Need to find a way home.") ]])
    end
    
-- |[Wipshag]|
elseif(sLastRelivedScene == "Wisphag") then
    if(fnIsFlorentinaPresent() == true) then
        fnCutscene([[ Append("Florentina:[E|Neutral] What's up, Mei. You look nostalgic.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Blush] Just thinking about transforming into a wisphag.[P] It was, on the whole, an oddly pleasant thing.[P] Frightening, but soothing.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] You are such a sap.") ]])
    else
        fnCutscene([[ Append("Mei:[E|Neutral] I'm glad I am privy to the secret knowledge of the earth now.[P] I won't let you down, fellow wisphags![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] Just gotta find a way home, first.[P] I hope there aren't millions of drifting souls on Earth.[P] Gonna have my work cut out for me...") ]])
    end
    
-- |[Mannequin]|
elseif(sLastRelivedScene == "Mannequin") then
    fnCutscene([[ Append("Mei:[E|Blush] (Losing myself...[P] it felt a little...[P] exciting.[P] If Victoria wasn't such a jerk...)[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] (Nope, can't think like that.[P] Gotta stay focused!)") ]])
end
fnCutsceneBlocker()
