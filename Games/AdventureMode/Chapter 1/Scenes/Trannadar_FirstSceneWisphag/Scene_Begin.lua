-- |[ ============================= Trannadar Intro Scene: Wisphag ============================= ]|
--If Mei is a Wisphag the first time she enters Trannadar Trading Post, this scene plays.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N", 1.0)

-- |[Move In From Edge]|
--Determine which side the player entered from.
local bIsPlayerAtBottom = false
local bIsPlayerAtTop = false
EM_PushEntity("Adventure Party", 0)
	local iPlayerX, iPlayerY = TA_GetProperty("Position")
DL_PopActiveObject()
if(iPlayerY >= 37 * 16) then bIsPlayerAtBottom = true end
if(iPlayerY <   7 * 16) then bIsPlayerAtTop = true end

--Wait a bit for the fade.
fnCutsceneWait(15)
fnCutsceneBlocker()

-- |[Movement]|
--Mei walks towards Nadia and Blythe. This is based on where she spawned.
if(bIsPlayerAtBottom) then
    fnCutsceneMove("Mei", 28.25, 38.50)
elseif(bIsPlayerAtTop) then
    fnCutsceneMove("Mei", 23.25, 6.50)
else
    fnCutsceneMove("Mei", 43.25, 28.50)
end
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(5)
fnCutsceneBlocker()

--Change the music.
fnCutscene([[ AudioManager_PlayMusic("NadiasTheme") ]])

--Face Nadia and Blythe south.
if(bIsPlayerAtBottom) then
    fnCutsceneFace("Nadia", 0, 1)
    fnCutsceneFace("Blythe", 0, 1)

--Face north.
elseif(bIsPlayerAtTop) then
    fnCutsceneFace("Nadia", 0, -1)
    fnCutsceneFace("Blythe", 0, -1)
    
--Face them east.
else
    fnCutsceneFace("Nadia", 1, 0)
    fnCutsceneFace("Blythe", 1, 0)
end
fnCutsceneWait(5)
fnCutsceneBlocker()

-- |[Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Blythe", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Nadia", "Neutral") ]])
fnCutscene([[ Append("Man: No no, you [P]*have*[P] to say it.[B][C]") ]])
fnCutscene([[ Append("Plant: Say what?[P] Please?[B][C]") ]])
fnCutscene([[ Append("Man: Yes.[P] If you don't say please, then it's rude.[P] Oh, a visitor.[P] Here's your chance.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Am I interrupting something?[B][C]") ]])
fnCutscene([[ Append("Plant: No you aren't, please.[B][C]") ]])
fnCutscene([[ Append("Man: Just immediately falling on your face, didn't even reach the first hurdle.[B][C]") ]])
fnCutscene([[ Append("Plant: Boss, please![P] I'm trying, please![B][C]") ]])
fnCutscene([[ Append("Man: Just...[P] introduce us, all right?[B][C]") ]])
fnCutscene([[ Append("Nadia: Please![P] I'm Nadia, and this is Captain Blythe![P] Hi, please![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] Not to be a backseat coach here but I think you should only say please when you're politely asking for something.[B][C]") ]])
fnCutscene([[ Append("Nadia: That makes a ton of sense![P] Thanks![B][C]") ]])
fnCutscene([[ Append("Nadia: Oh my gosh I just said thanks, without being told to![P] This is awesome, Cap'n![P] Cap'n![P] I'm polite now![B][C]") ]])
fnCutscene([[ Append("Blythe: I am enthused.[P] Now, please, finish the rest of the introduction.[B][C]") ]])
fnCutscene([[ Append("Nadia: (Wow, he just did it, so effortlessly...)[B][C]") ]])
fnCutscene([[ Append("Nadia: So...[P] this is Trannadar Trading Post![P] Please [P](yeah!)[P] enjoy your stay, but we do not allow violence within these walls.[P] If you have a problem, bring it to a guard.[B][C]") ]])
fnCutscene([[ Append("Nadia: If there's something we can help you with, let us know right away![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Smirk] Hi![P] My name's Mei, and uh, maybe you can help me.[P] I'm looking for a way back to Earth![B][C]") ]])
fnCutscene([[ Append("Nadia: Oh, cool![P] Is that where the swamp hag creatures come from?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] Search me.[P] Haven't been one for long.[P] But I take it that means you don't know where Earth is.[B][C]") ]])
fnCutscene([[ Append("Nadia: .[P].[P].[P] Nope![B][C]") ]])
fnCutscene([[ Append("Blythe: If I may.[P] You should ask Florentina, she runs the general store.[P] She knows all sorts of people, so she could at least point you in the right direction.[B][C]") ]])
fnCutscene([[ Append("Nadia: Yeah![P] She knows all kinds of stuff![P] But she's not very polite.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Laugh] Don't let her set a bad example.[P] I'll go ask her.[P] Thank you![B][C]") ]])
fnCutscene([[ Append("Nadia: (OH CRAP OH CRAP OH CRAP)[B][C]") ]])
fnCutscene([[ Append("Blythe: Nadia, you're forgetting.[P] It's 'You're welcome'.[B][C]") ]])
fnCutscene([[ Append("Nadia: You're welcome, Mei!") ]])
fnCutsceneBlocker()

-- |[Finish Up]|
--Change the music.
fnCutscene([[ AudioManager_PlayMusic("TownTheme") ]])
fnCutsceneBlocker()

--Unlock topics.
WD_SetProperty("Unlock Topic", "Alraunes", 1)
WD_SetProperty("Unlock Topic", "Blythe", 1)
WD_SetProperty("Unlock Topic", "Cultists", 1)
WD_SetProperty("Unlock Topic", "Florentina", 1)
WD_SetProperty("Unlock Topic", "Job", 1)
WD_SetProperty("Unlock Topic", "Nadia", 1)
WD_SetProperty("Unlock Topic", "Name", 1)
WD_SetProperty("Unlock Topic", "TradingPost", 1)
WD_SetProperty("Unlock Topic", "Challenge", 1)
