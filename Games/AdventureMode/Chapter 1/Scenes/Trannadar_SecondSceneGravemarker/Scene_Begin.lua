-- |[ ========================== Trannadar Second Scene: Gravemarker =========================== ]|
--If Nadia first met Mei as a human, and then Mei entered as a gravemarker...
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarSecondScene", "N", 1.0)

--Determine which side the player entered from.
local bIsPlayerAtBottom = false
local bIsPlayerAtTop = false
EM_PushEntity("Adventure Party", 0)
	local iPlayerX, iPlayerY = TA_GetProperty("Position")
DL_PopActiveObject()
if(iPlayerY >= 37 * 16) then bIsPlayerAtBottom = true end
if(iPlayerY <   7 * 16) then bIsPlayerAtTop = true end

--Wait a bit for the fade.
fnCutsceneWait(15)
fnCutsceneBlocker()

--Mei walks towards Nadia. This is based on where she spawned.
local iHasSeenTrannadarFlorentinaScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
if(bIsPlayerAtBottom) then
	
	--Move Mei.
	Cutscene_CreateEvent("Move Mei North", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (38.50 * gciSizePerTile))
	DL_PopActiveObject()
	
	--If Florentina is present, move her as well.
	if(iHasSeenTrannadarFlorentinaScene == 1.0) then
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (39.50 * gciSizePerTile))
		DL_PopActiveObject()
	end

	--Common.
	fnCutsceneBlocker()

elseif(bIsPlayerAtTop) then
	Cutscene_CreateEvent("Move Mei", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (23.25 * gciSizePerTile), (6.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--If Florentina is present, move her as well.
	if(iHasSeenTrannadarFlorentinaScene == 1.0) then
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (24.25 * gciSizePerTile), (6.50 * gciSizePerTile))
		DL_PopActiveObject()
	end
	fnCutsceneBlocker()
else
	--Move Mei.
	Cutscene_CreateEvent("Move Mei West", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (43.25 * gciSizePerTile), (28.50 * gciSizePerTile))
	DL_PopActiveObject()
	
	--If Florentina is present, move her as well.
	if(iHasSeenTrannadarFlorentinaScene == 1.0) then
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (44.25 * gciSizePerTile), (28.50 * gciSizePerTile))
		DL_PopActiveObject()
	end
	
	--Common.
	fnCutsceneBlocker()
end

--Change the music.
fnCutscene([[ AudioManager_PlayMusic("NadiasTheme") ]])

--Wait a bit.
fnCutsceneWait(5)
fnCutsceneBlocker()

--Dialogue setup.
fnStandardMajorDialogue()
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Nadia", "Neutral") ]])

--Talking.
fnCutscene([[ Append("Nadia: Yikes![P] Stay back, whatever you are![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Nadia, it's me![P] Don't you recognize me?[B][C]") ]])
fnCutscene([[ Append("Nadia: Mei?[P] What happened?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] St. Fora's convent has creatures called Gravemarkers in it.[B][C]") ]])
fnCutscene([[ Append("Nadia: Gravemarkers are statue angels?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] Yes, but I decided to not saw my own head off.[B][C]") ]])
fnCutscene([[ Append("Nadia: Wow, I've walked by there a hundred times and had no idea.[B][C]") ]])
fnCutscene([[ Append("Nadia: ...[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] What?[P] You look distant.[B][C]") ]])
fnCutscene([[ Append("Nadia: ...[B][C]") ]])
fnCutscene([[ Append("Nadia: You're going to hate this one.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] Oh no...[B][C]") ]])
fnCutscene([[ Append("Nadia: Statue?[P] I just met you![P] Hahahaha![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] *sigh*[B][C]") ]])
fnCutscene([[ Append("Nadia: This is pretty nifty, though, because now I can say I've met an angel, and a living statue girl![B][C]") ]])
fnCutscene([[ Append("Nadia: Cross those off the ol' monstergirl bingo!") ]])
fnCutsceneBlocker()

--Change the music.
fnCutscene([[ AudioManager_PlayMusic("TownTheme") ]])
fnCutsceneBlocker()

--Move Florentina onto Mei, fold the party.
if(iHasSeenTrannadarFlorentinaScene == 1.0) then
	
	--Bottom.
	if(bIsPlayerAtBottom) then
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (28.25 * gciSizePerTile), (38.50 * gciSizePerTile))
		DL_PopActiveObject()

    elseif(bIsPlayerAtTop) then
        Cutscene_CreateEvent("Move Mei", "Actor")
            ActorEvent_SetProperty("Subject Name", "Florentina")
            ActorEvent_SetProperty("Move To", (23.25 * gciSizePerTile), (6.50 * gciSizePerTile))
        DL_PopActiveObject()
        fnCutsceneBlocker()
	
	--Right.
	else
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (43.25 * gciSizePerTile), (28.50 * gciSizePerTile))
		DL_PopActiveObject()
	end
	fnCutsceneBlocker()

	--Fold the party positions up.
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
end
