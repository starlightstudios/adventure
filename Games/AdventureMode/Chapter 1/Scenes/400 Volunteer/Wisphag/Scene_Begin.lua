-- |[ =================================== Volunteer Wisphag ==================================== ]|
--Cutscene proper. Uses the dialogue's SceneHandler to get the TF sequence.

-- |[ ======================================= Scene Setup ====================================== ]|
-- |[Variables]|
-- |[Image Streaming]|
fnLoadDelayedBitmapsFromList("Chapter 1 Mei Wisphag TF", gciDelayedLoadLoadAtEndOfTick)

-- |[Form]|
--Change the party lead back to human.
LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Wisphag.lua")

-- |[Combat]|
--Restore party to full HP in case the player doesn't want to revisit a save point.
AdvCombat_SetProperty("Restore Party")

-- |[Music]|
AL_SetProperty("Music", "Null")

-- |[Flags]|
--Mark this form as being unlocked.
VM_SetVar("Root/Variables/Global/Mei/iHasWisphagForm", "N", 1.0)
VM_SetVar("Root/Variables/Chapter1/ScenesWisphag/iWasVoluntary", "N", 1.0)

-- |[Remove Florentina]|
--If Florentina is around, she will rejoin after the segment is over.
fnRemovePartyMember("Florentina", true)

-- |[ ==================================== Scene Execution ===================================== ]|
-- |[Fading]|
--Snap to blackness.
fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

-- |[Position]|
fnCutsceneTeleport("Mei", 13.25, 20.50)
fnCutsceneFace("Mei", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(185)
fnCutsceneBlocker()

-- |[Scene]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ Append("The crash of the water was quieter than she might have anticipated.[P] Colder.[P] Thicker.[P] Mei was drowning.[P] She could hear faint garbled shouting and other various noises.[P] Likely Florentina.[P] The whirlpool cared not and pulled her down suddenly, brutally.[P] Strands of algae, vines, and duckweed all seemed to coalesce around her.[P] She felt the humid water enter her lungs as she gasped and screamed in silence.[B][C]") ]])
fnCutscene([[ Append("Her body lurched as the plant life seemed to restrain her.[P] Tighter and tighter it grew as her vision darkened.[P] The green-tinted water faded to black as her struggling body hit the bottom.[P] The roots of the lilies wrapped around her, fingerlike and firm, and tugged her away.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Null", "Root/Images/Scenes/Mei/WisphagTF0") ]])
fnCutscene([[ Append(".[P].[P].[P].[P].[P].") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/Mei/WisphagTF0", "Root/Images/Scenes/Mei/WisphagTF1") ]])
fnCutscene([[ Append("She regained all her senses just as she was deposited in what appeared to be a secluded grove.[P] She fell onto the water- reaching only her knees and hands as she coughed up the remaining vegetation and excess water.[P] Her black hair pooled around her and floated on the top of the liquid.[P] She finally managed to open her eyes after blinking some water away.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/Mei/WisphagTF1", "Root/Images/Scenes/Mei/WisphagTF2") ]])
fnCutscene([[ Append("Mei looked around, trying to spot her interlocuters.[P] She was met only with dancing, floating balls of teal flame.[P] Whether burning eyes or flames in the darkness, she could not be sure.[P] Her attention was soon set on something else.[P] She ignored the figures that seemed to grow closer in favor of the shrine that glowed and gleamed with a gentle, soothing light.[P] Mist and fog emanated from it.[P] As if beckoning her closer. ") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/Mei/WisphagTF2", "Root/Images/Scenes/Mei/WisphagTF3") ]])
fnCutscene([[ Append("Mei stared in wide-eyed awe.[P] She saw a small little flame dancing around the stone structure, far more coherent and present than the ones beyond the wall of fog.[B][C]") ]])
fnCutscene([[ Append("There were noises all around her, none rising above the others to become a true sound.[P] As though the silence itself spoke in a new language.[P] Calling her forth.[P] The ball floated closer towards Mei.[P] Instinctively, she raised her hands to accept the creature.[P] It felt cold and warm at the same time.[P] It smelled like peppermint, clearing her lungs from the foul brackish liquid she had inhaled earlier.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/Mei/WisphagTF3", "Root/Images/Scenes/Mei/WisphagTF4") ]])
fnCutscene([[ Append("The smoke grew thicker, the ball burned brighter.[P] Her eyes glowed...[P] In an instant, she felt it.[B][C]") ]])
fnCutscene([[ Append("Her iris and pupil reflected the same teal that the ball had.[P] Light seemed to emanate from them in small crackles of new energy.[P] The muddy terrain underneath the clear water began to rise upwards and coat her black boots.[P] She felt the material start to melt into mush and fade away as the mud seemingly disintegrated her footwear.[P] Her body changed and yet still her attention was consumed by the flame.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/Mei/WisphagTF4", "Root/Images/Scenes/Mei/WisphagTF5") ]])
fnCutscene([[ Append("Her legs seemed to grow weaker.[P] There was no uncertainty, no fear, no doubt.[P] She watched with wide eyes, almost entranced by the flame, and her cares were so distant.[P] Instead she felt the collective mass of the terrain coating her lower half.[P] Cold, yet comfortable.[P] Her dress was spared the assimilation, however, and now rose upwards to float against the fluid-like earth.[P] It, too, was changing.[B][C]") ]])
fnCutscene([[ Append("She hardly noticed as her hair began to meld together.[P] It formed thick strands of vine-like algae.[P] Deep blue-green with lighter tones at the top.[P] She felt each strand as though it were a living part of her body.[P] She heard the water break as it reached all the way down to the mud she used to call her legs.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/Mei/WisphagTF5", "Root/Images/Scenes/Mei/WisphagTF6") ]])
fnCutscene([[ Append("A potent realization arrived.[P] She could now see much more in the flame, hear the sizzle of its quiet voice.[P] The glow revealed her hands had changed.[P] Her skin tone began to drain.[P] She felt her heart slow as her skin turned dark grey all over, hardening and thickening as a form of natural armor.[P] Her hands had become like firm loam, thick and strong, clutching at the flame.[B][C]") ]])
fnCutscene([[ Append("Her eyes widened and the flame within her emerged.[P] The licks of the tips of flame revealed so much about the floating wisp in her hands.[P] It said one word to her, now clearly in a voice only she could hear.[B][C]") ]])
fnCutscene([[ Append("Lost.[B][C]") ]])
fnCutscene([[ Append("Lost?[B][C]") ]])
fnCutscene([[ Append("Mei tilted her head.[P] Her voice had become raspy, different, but still hers.[P] Her thickened hair slapped against her body, jolting her back to her senses.[B][C]") ]])
fnCutscene([[ Append("If it was lost, there was only one place to take it.[P] She clutched it within her hands and made towards the glowing shrine.[P] She nearly fell over when she remembered her legs were gone.[B][C]") ]])
fnCutscene([[ Append("Instead, she formed her lower body together, like a lilypad, and shifted her weight.[P] She slid across the pond and brought the little wisp to its glow.[B][C]") ]])
fnCutscene([[ Append("She wanted to speak again, but her mouth was so unfamiliar.[P] Her skin, tongue, lips, and teeth were now jagged and earthen.[P] She did not need to speak, as the wisp responded to her anyway.[B][C]") ]])
fnCutscene([[ Append("Found.[B][C]") ]])
fnCutscene([[ Append("All at once, the breath from her lungs was stolen from her.[P] A flame erupted from her body as the shrine glowed with unearthly fire.[P] The wisp leapt from her hands into it, and was consumed.[P] It hadn't been asking her for help, it had been offering it.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/Mei/WisphagTF6", "Root/Images/Scenes/Mei/WisphagTF7") ]])
fnCutscene([[ Append("Found.[B][C]") ]])
fnCutscene([[ Append("She shuddered-[P] feeling cold wind on a part of her body she didn't even know she had.[P] Instinctively, she knew it was part of a soul.[P] Her soul.[P] One hand shakily raised to touch it as it floated idly around her head.[P] Her finger touched it, and it shot electricity rippling through her hand and down her arm.[B][C]") ]])
fnCutscene([[ Append("Her hands had become withered, old, worn, and worthless.[P] But soon the earth itself rushed around them, reforming them, and they were reborn.[P] Nature provided.[P] Whenever her body would be hurt, it would reform.[P] When she became tired, she would renew.[P] The earth and water were one with her.[B][C]") ]])
fnCutscene([[ Append("There were many spirits in the clearing now, hovering around the shrine.[P] She understood them, now.[P] The people here did not understand the wisphags, they saw them as frightening, evil.[P] The wisphags lingered where many had died.[P] Their work was difficult and unappreciated.[B][C]") ]])
fnCutscene([[ Append("They looked human enough but could not speak, their intellect limited, their visages terrible.[P] People hated them and did not understand them, and it brought melancholy to Mei.[P] Their task was so noble, to find little lost spirits and guide them to the next world.[P] There were many shrines like this throughout the land, and wisphags shepherding souls to them to move on.[B][C]") ]])
fnCutscene([[ Append("She patted her own soul, giggling as she did.[P] It felt nice.[P] She knelt down and freed a part of her dress that had been lodged in her 'legs', and tied it around her arm.[P] She, too, was lost, but now, was found.[P] Her connection to the wisps was something beyond her runestone.[P] She felt at peace, but knew there was still much left for her to do.[P] Yet she did not worry.[B][C]") ]])
fnCutscene([[ Append("For she knew nature would provide.") ]])
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()

-- |[Fade In]|
fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()

-- |[Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ Append("Mei:[E|Happy] I see![P] I see![P] I understand now![P] It's all so clear and - [B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] Huh?[P] I seem to have changed.[P] When did that happen?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] I didn't even notice.[P] Should I be alarmed that part of my soul is outside my body?[P] Guess not, the other wisphags aren't.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Angry] Wisphag.[P] We're not hags![P] We're more of a crone sort of deal.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Smirk] It's so strange...[P] but I like the feeling.[P] And...[P] I can use my runestone if I really have to.[P] Might be a good idea to avoid scaring people.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Happy] Don't worry my friends![P] I will do my best to tell everyone the truth![P] If they'll believe me![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Blush] Heh, I guess now even more people are counting on me.[P] Wisphag Mei is up to the task![P] Now...[P] how do I get out of here?") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

-- |[ ======================================== Clean Up ======================================== ]|
-- |[Unload Images]|
fnCutscene([[ fnUnloadBitmapsFromList("Chapter 1 Mei Wisphag TF") ]])

-- |[Transition]|
--Last save point.
--fnCutscene([[ AL_BeginTransitionTo("LASTSAVE", "Null") ]])
--fnCutsceneBlocker()