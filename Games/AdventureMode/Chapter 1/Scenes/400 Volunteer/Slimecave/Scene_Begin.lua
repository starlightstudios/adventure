-- |[ ===================================== Slime Volunteer ==================================== ]|
--This is used exclusively by the relive handler to simulate the volunteer bee transformation sequence.
-- It is always expected to be fired during a relive.

-- |[ ======================================= Scene Setup ====================================== ]|
--Reshift Mei back into a Human.
LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Human.lua")

--Clear censor bars.
WD_SetProperty("Clear Censor Bars")

-- |[ ======================================== Execution ======================================= ]|
--Variables.
local iKnowsSlimesTalk = 0.0

--Load images.
fnLoadDelayedBitmapsFromList("Chapter 1 Mei Slime TF", gciDelayedLoadLoadAtEndOfTick)

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei") ]])
fnCutscene([[ Append("Mei:[E|Neutral] (Hm, seems this door was welded shut by something.[P] It won't open.)[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] (There doesn't seem to be any other way through.[P] Guess I'll come back once I figure something out...)") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
        
--Movement.
fnCutscene([[ AudioManager_PlaySound("World|LittleSplashA") ]])
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("Slime:[VOICE|Slime] Jeesum crow, it's cold in here!") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutscene([[ AudioManager_PlaySound("World|LittleSplashB") ]])
fnCutsceneWait(15)
fnCutsceneBlocker()
fnCutsceneWait(15)
fnCutsceneBlocker()
fnCutscene([[ AudioManager_PlaySound("World|LittleSplashA") ]])
fnCutsceneWait(15)
fnCutsceneBlocker()
for i = 62.35, 63.25, 0.05 do
    fnCutsceneWait(3)
    fnCutsceneBlocker()
end

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Green") ]])
fnCutscene([[ Append("Slime:[E|Green] Woah hey, there's a human down in this dank pit![B][C]") ]])
fnCutscene([[ Append("Slime:[E|Green] The heck are you doing down here, buddy?[P] Did you not see all the creepy monsters and frigid water?[B][C]") ]])
if(iKnowsSlimesTalk == 0.0) then
    VM_SetVar("Root/Variables/Chapter1/Scenes/iKnowsSlimesTalk", "N", 1.0)
    fnCutscene([[ Append("Mei:[E|Surprise] You can talk![P] And you're a slime![B][C]") ]])
    fnCutscene([[ Append("Slime:[E|Green] Ch'yeah![P] Very sharp eyes on you![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Surprise] How are you talking?[B][C]") ]])
    fnCutscene([[ Append("Slime:[E|Green] With my mouth![P] Hehehehe![B][C]") ]])
    fnCutscene([[ Append("Slime:[E|Green] Some slimes can talk, most can't, because some of us are just smart![P] And we eat smarty fruits![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Well, okay.[P] So long as you're a friendly slime.[B][C]") ]])
else
    fnCutscene([[ Append("Mei:[E|Neutral] Woah, where did you come from?[B][C]") ]])
    fnCutscene([[ Append("Slime:[E|Green] The water, silly![P] Isn't it obvious?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Sorry, but a lot of things have been trying to kill me today.[P] Are you a friendly slime?[B][C]") ]])
end
fnCutscene([[ Append("Slime:[E|Green] Buddy, I am the friendliest slime you'll ever meet![B][C]") ]])
fnCutscene([[ Append("Meryl:[E|Green] Name's Meryl, pleased to meet you![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Mei.[B][C]") ]])
fnCutscene([[ Append("Meryl:[E|Green] So, Mei, I can't help but notice you're staring at that fused door there with some sort of longing.[B][C]") ]])
fnCutscene([[ Append("Meryl:[E|Green] You trying to bone that door?[P] Can I help?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Blush] I'm not trying to bone it, I'm trying to get through it.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] I think there's something over there.[B][C]") ]])
fnCutscene([[ Append("Meryl:[E|Green] Okay![P] I can help![B][C]") ]])
fnCutscene([[ Append("Meryl:[E|Green] I'm a slime, and us slimes can slip through doors.[P] Want some help?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Happy] Sure!") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Green") ]])
fnCutscene([[ Append("Meryl:[E|Green] So you need to get through here?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Happy] Yeah, if you could - [P][CLEAR]") ]])
fnCutscene([[ Append("Mei:[E|Surprise] Woah!") ]])
fnCutsceneBlocker()

--Scene.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Null") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/SlimeTF0") ]])
fnCutscene([[ Append("Mei:[Voice|Mei] What are you doing? Hey, stop that![B][C]") ]])
fnCutscene([[ Append("Meryl:[Voice|Slime] But you said - [P][CLEAR]") ]])
fnCutscene([[ Append("Mei:[Voice|Mei] I wanted [P]*you*[P] to go through and find a way to open it![B][C]") ]])
fnCutscene([[ Append("Mei tried to shake the slime off, but it stuck to her and began to spread.[B][C]") ]])
fnCutscene([[ Append("In mere seconds, she was covered.[P] Meryl the slime tried to stop it, but was powerless.") ]])
fnCutsceneBlocker()

--Next scene.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/SlimeTF0") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/SlimeTF1") ]])
fnCutscene([[ Append("Mei:[Voice|Mei] Wh-[P]what?[P] I'm becoming...[B][C]") ]])
fnCutscene([[ Append("The infection reached up to Mei's head.[P] Suddenly, the spread of the slime wasn't a problem.[P] In fact, it began to feel pleasurable...[B][C]") ]])
fnCutscene([[ Append("Mei:[Voice|Mei] More slime...[P] rub me...[B][C]") ]])
fnCutscene([[ Append("Meryl:[Voice|Slime] Hey, you sure about that?[P] You'll turn into a slime![B][C]") ]])
fnCutscene([[ Append("Mei:[Voice|Mei] Yeah, I want to.[P] Turn me.[P] Turn me![B][C]") ]])
fnCutscene([[ Append("Unsure, Meryl the slime rubbed Mei's body, spreading more slime.[P] It swirled over Mei's body and turned her, inside and out.") ]])
fnCutsceneBlocker()

--Finished.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Slime.lua") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/SlimeTF1") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/SlimeTF2") ]])
fnCutscene([[ WD_SetProperty("Register Censor Bar", 605, 179, 112, 45) ]])
fnCutscene([[ Append("Mei:[Voice|Mei] Slime...[P] I'm a slime...[P] I'm a slime![B][C]") ]])
fnCutscene([[ Append("Meryl:[Voice|Slime] As long as you're happy, buddy.[P] And hey, now you can squeeze through![B][C]") ]])
fnCutscene([[ Append("Mei had become a slimegirl, top to bottom.[P] She smiled to herself.[P] Now she could get through the bars!") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Green") ]])
fnCutscene([[ Append("Meryl:[E|Green] So um, you're gonna go through the bars now?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Smirk] I mean yeah, may as well now that we went through all this trouble.[P] Thanks, Meryl![B][C]") ]])
fnCutscene([[ Append("Meryl:[E|Green] No problem![P] Good luck, buddy!") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Clean up images.
fnCutscene([[ fnUnloadBitmapsFromList("Chapter 1 Mei Slime TF") ]])
	
--Wait a bit.
fnCutsceneWait(10)
fnCutsceneBlocker()

-- |[Exit]|
--Return to the last save point and execute the post-script..
fnCutscene([[ AL_BeginTransitionTo("LASTSAVE", gsStandardReliveEnd) ]])
fnCutsceneBlocker()