-- |[ ========================== Volunteer to Alraune at the Pit Stop ========================== ]|
--This lua file is called when this scene is fired from the debug menu, or the relive menu.
Debug_PushPrint(false, "Debug Firing Cutscene: Volunteer Alraune Pitstop\n")

--Otherwise, run the file normally. It may or may not change the map itself.
LM_ExecuteScript(fnResolvePath() .. "Scene_Begin.lua")

--Debug.
Debug_PopPrint("Completed debug cutscene firing.\n")
