-- |[ ========================== Volunteer to Alraune at the Pit Stop ========================== ]|
--Turns Mei into an Alraune at the pit stop via volunteering.

-- |[ ====================== Setup ===================== ]|
-- |[Variables]|
local bIsFlorentinaPresent      = fnIsCharacterPresent("Florentina")
local iFlorentinaKnowsAboutRune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")

-- |[Flags]|
-- |[Transition]|
--Switches to the Pit Stop.
local sCurrentLevelName = AL_GetProperty("Name")
if(sCurrentLevelName ~= "BreannesPitStop") then
	local sString = "AL_BeginTransitionTo(\"BreannesPitStop\", \"" .. LM_GetCallStack(0) .. "\")"
	fnCutscene(sString)
	return
end

-- |[ =============== Cutscene Execution =============== ]|
-- |[Dialogue]|
--Snap to black.
fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

--Position.
fnCutsceneTeleport("Mei", 22.25, 10.50)
fnCutsceneFace("Mei", 0, -1)
if(bIsFlorentinaPresent) then
    fnCutsceneTeleport("Florentina", 23.25, 10.50)
    fnCutsceneFace("Florentina", -1, -1)
end
fnCutsceneWait(25)
fnCutsceneBlocker()

--Fade in.
fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()
fnCutsceneWait(65)
fnCutsceneBlocker()

--Clean.
WD_SetProperty("Hide")
fnCutscene([[ WD_SetProperty("FastShow") ]])
fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])

--Florentina is not present:
if(bIsFlorentinaPresent == false) then
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] I...[P] want it...[P] I want to be happy...[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Blush] I didn't have a lot of friends back home.[P] Not real friends.[B][C]") ]])
    fnCutscene([[ Append("Alraune: Every blade of grass will be your friend when you are joined.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Blush] I'm just so nervous...[B][C]") ]])
    fnCutscene([[ Append("Alraune: Whatever for?[P] You will be happy and secure.[P] You will not age or falter from disease.[P] You will be more than human.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] ...[P] Let's go.[P] Let's not waste any more time.[B][C]") ]])
    fnCutscene([[ Append("Alraune: Stupendous![P] I will send word ahead.[P] Please, follow me.") ]])
    fnCutsceneBlocker()

--Florentina is present, and knows about the runestone.
elseif(bIsFlorentinaPresent == true and iFlorentinaKnowsAboutRune == 1.0) then
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] I...[P] want it...[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Surprise] Mei, have you lost your mind?[P] Think of what you're giving up![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] It'll be all right.[P] Runestone, remember?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] ...[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] Yeah, I can't argue with that.[P] It's almost like you're running a con.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] If I don't like it...[B][C]") ]])
    fnCutscene([[ Append("Alraune: You will love every moment of your life when you are joined.[P] I speak from experience.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] And you speak for yourself exclusively.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] All right, let's go.[B][C]") ]])
    fnCutscene([[ Append("Alraune: Wanderer Florentina...[P] you will not be present.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Surprise] ![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Surprise] Why not?[B][C]") ]])
    fnCutscene([[ Append("Alraune: She is not like us.[P] She is uncleansed.[P] She may attempt to sabotage the process.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Offended] You haughty scum![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] But...[P][CLEAR]") ]])
    fnCutscene([[ Append("Alraune: It is the only way forward.[P] I am sorry.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] Florentina, would you...[P][CLEAR]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Yeah.[P] I'll wait outside when we get there.[P] As if I wanted to associate with your jerks anyway.") ]])
    fnCutsceneBlocker()

--Florentina is present, and does not know about the runestone.
elseif(bIsFlorentinaPresent == true and iFlorentinaKnowsAboutRune == 0.0) then
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] I...[P] want it...[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Surprise] Mei, have you lost your mind?[P] Think of what you're giving up![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] ..?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] I'm not giving up anything.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Surprise] Your humanity is at stake![P] You won't be you anymore![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] No, I will.[P] I can feel it.[P] I -[P] my runestone...[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Blush] I can't explain it.[P] I'll show you.[P] Afterwards.[B][C]") ]])
    fnCutscene([[ Append("Alraune: Please, come.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Lead the way.[B][C]") ]])
    fnCutscene([[ Append("Alraune: Wanderer Florentina...[P] you will not be present.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Surprise] ![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Surprise] Why not?[B][C]") ]])
    fnCutscene([[ Append("Alraune: She is not like us.[P] She is uncleansed.[P] She may attempt to sabotage the process.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Offended] You haughty scum![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] But...[P][CLEAR]") ]])
    fnCutscene([[ Append("Alraune: It is the only way forward.[P] I am sorry.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] Florentina, would you...[P][CLEAR]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Yeah.[P] I'll wait outside when we get there.[P] As if I wanted to associate with your jerks anyway.") ]])
    fnCutsceneBlocker()
end

--Fade to black.
fnCutscene([[ AL_SetProperty("Activate Fade", 60, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()
fnCutsceneWait(90)
fnCutsceneBlocker()

--Mini dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
fnCutscene([[ Append("Alraune: New sister.[P] Please, breathe my pollen.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Hmm?[B][C]") ]])
fnCutscene([[ Append("Alraune: Your body must be pliable.[P] This pollen will help.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Blush] Ooooh... I feel so...[P] sleepy...") ]])
fnCutsceneBlocker()
fnCutsceneWait(90)
fnCutsceneBlocker()

-- |[ ==================== Finish Up =================== ]|
--Execute the Alraune cutscene.
fnCutscene([[ LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/400 Volunteer/Alraune Pitstop/Scene_Chamber.lua") ]])