-- |[ ================================= Alraune Chamber Section ================================ ]|
--This part takes place in the Alraune Chamber.

-- |[ ====================== Setup ===================== ]|
--Clear censor bars.
WD_SetProperty("Clear Censor Bars")

-- |[Map Check]|
--Make sure we're in the cutscene map.
local sCurrentLevelName = AL_GetProperty("Name")
if(sCurrentLevelName ~= "AlrauneChamber") then
	local sString = "AL_BeginTransitionTo(\"AlrauneChamber\", \"" .. LM_GetCallStack(0) .. "\")"
	fnCutscene(sString)
	return
end

-- |[Variables]|
local iMetRochea            = VM_GetVar("Root/Variables/Chapter1/Alraunes/iMetRochea", "N")
local iFlorentinaToldFungus = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaToldFungus", "N")

-- |[Flags]|
VM_SetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N", 1.0)
VM_SetVar("Root/Variables/Chapter1/Alraunes/iMetRochea", "N", 1.0)

-- |[Remove Florentina]|
--If Florentina is in the party, remove her. She rejoins shortly.
fnRemovePartyMember("Florentina", true)

-- |[Form]|
--Reshift Mei back into a Human. This keeps the scenes looking sane.
LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Human.lua")

-- |[Image Streaming]|
fnLoadDelayedBitmapsFromList("Chapter 1 Mei Alraune TF", gciDelayedLoadLoadAtEndOfTick)

-- |[Topics]|
--Unlock these topics if they weren't already.
WD_SetProperty("Unlock Topic", "Alraunes", 1)
WD_SetProperty("Unlock Topic", "CleansingFungus", 1)

-- |[Music]|
AL_SetProperty("Music", "Null")

-- |[ ================== Construction ================== ]|
--Reposition Mei, make her use the wounded image.
EM_PushEntity("Mei")
	TA_SetProperty("Position", 17, 8)
	TA_SetProperty("Set Special Frame", "Wounded")
DL_PopActiveObject()
Cutscene_CreateEvent("Face Mei South", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 0, 1.0)
DL_PopActiveObject()

--Face Rochea east.
local sDialoguePath = gsRoot .. "Maps/Evermoon/AlrauneChamber/Dialogue.lua"
if(EM_Exists("Rochea") == true) then
	EM_PushEntity("Rochea")
		TA_SetProperty("Facing", gci_Face_East)
	DL_PopActiveObject()
else
    TA_Create("Rochea")
        TA_SetProperty("Position", 16, 8)
        TA_SetProperty("Clipping Flag", true)
        fnSetCharacterGraphics("Root/Images/Sprites/Rochea/", false)
        TA_SetProperty("Facing", gci_Face_East)
        TA_SetProperty("Activation Script", sDialoguePath)
        TA_SetProperty("Activate Wander Mode")
    DL_PopActiveObject()
end

--Extra Alraunes
TA_Create("AlrauneA")
	TA_SetProperty("Position", 17, 6)
	TA_SetProperty("Clipping Flag", true)
	fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
	TA_SetProperty("Facing", gci_Face_South)
	TA_SetProperty("Activation Script", sDialoguePath)
	TA_SetProperty("Activate Wander Mode")
DL_PopActiveObject()
TA_Create("AlrauneB")
	TA_SetProperty("Position", 11, 10)
	TA_SetProperty("Clipping Flag", true)
	fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
	TA_SetProperty("Facing", gci_Face_East)
	TA_SetProperty("Activation Script", sDialoguePath)
	TA_SetProperty("Activate Wander Mode")
DL_PopActiveObject()
TA_Create("AlrauneC")
	TA_SetProperty("Position", 12, 6)
	TA_SetProperty("Clipping Flag", true)
	fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
	TA_SetProperty("Facing", gci_Face_East)
	TA_SetProperty("Activation Script", sDialoguePath)
	TA_SetProperty("Activate Wander Mode")
DL_PopActiveObject()

-- |[ =============== Cutscene Execution =============== ]|
--Fade from black to nothing over 120 ticks. This fade is under the UI.
AL_SetProperty("Activate Fade", 120, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0)

--Wait a bit for the fade.
fnCutsceneWait(90)
fnCutsceneBlocker()

-- |[Dialogue Sequence]|
--Dialogue setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Sad") ]])

--Actual talking.
fnCutscene([[ Append("Mei: H-[P]huh?[P] Where...") ]])
fnCutsceneBlocker()

-- |[Dialogue Sequence]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Sad") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Rochea", "Neutral") ]])

--Actual talking.
fnCutscene([[ Append("Alraune: Shhh...[P] All is well...") ]])
fnCutsceneBlocker()

-- |[Fade]|
--Set a black overlay.
fnCutscene([[ AL_SetProperty("Activate Fade", 35, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])

-- |[TF Sequence]|
--Scene part 0.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
fnCutscene([[ Append("Slowly, Mei came to her senses.[P] The pollen of the Alraune still lingered within her.[P] She felt relaxed and at peace.[P] A plant girl, different from the one who had brought her here, was leading her towards a yellow, glistening pool...[B][C]") ]])
fnCutscene([[ Append("As her mind began to clear, she realized there were more of them, watching her from the edges of the room.[P] She was now underground, somewhere where creeping vines had grown all over the walls.[P] The air was thick with the smell of plant matter, and the pool seemed to draw fluid from the leafy matter surrounding it.") ]])
fnCutsceneBlocker()

--Scene part 1.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/AlrauneTF0") ]])
fnCutscene([[ Append("The plant girl lowered Mei into the pool, carefully and gently.[P] Mei felt the warm and comforting embrace of the pool flow over her, reaching up to her neck, relaxing her further.[P] The Alraune joined her in the pool, supporting her and keeping her head above the surface.[B][C]") ]])
fnCutscene([[ Append("Her skin changed first, becoming blue like the girl who had brought her here.[P] Despite the warmth of the water, Mei still shivered as her body changed.") ]])
fnCutsceneBlocker()

--Scene part 2.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/AlrauneTF0") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/AlrauneTF1") ]])
fnCutscene([[ WD_SetProperty("Register Censor Bar", 611, 214, 105, 38) ]])
fnCutscene([[ Append("As the fluid soaked into her now porous skin, the changes flowed into her head and along her hair, turning it green.[P] Her eyes lost focus, and she blinked several times as if to clear them.[P] Soon, her eyes had reddened, allowing her to see colors that no human could, the edges of the room scintillating as she drew her eyes across them.[B][C]") ]])
fnCutscene([[ Append("Still supported by the Alraune who had brought her here, Mei now felt enough strength to free herself.[P] She tugged, and the plant girl allowed her to float freely.[P] Relaxed and calm, Mei realized her head was still partly human.[P] This thought irked her.[P] She slid below the surface to allow the fluid to soak into her head and complete her transformation.") ]])
fnCutsceneBlocker()

--Disable the special frame.
Cutscene_CreateEvent("Stop Crouch Event", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Special Frame", "Null")
DL_PopActiveObject()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", -1.0, 0)
DL_PopActiveObject()
fnCutscene([[ WD_SetProperty("Show") ]])

--Scene part 3.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Alraune.lua") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/AlrauneTF1") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Alraune") ]])
fnCutscene([[ WD_SetProperty("Send Censor Bars To Crossfade") ]])
fnCutscene([[ Append("Rising from the pool, Mei's transformation had completed.[P] Her humanity gone, her body blue and green like the women who had brought her here.[P] She heard murmurs from the other Alraunes watching her, welcoming her to their family.[P] She smiled.") ]])
fnCutsceneBlocker()

-- |[Fade]|
--Clear the overlay.
fnCutscene([[ AL_SetProperty("Activate Fade", 35, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 0) ]])

-- |[Dialogue Sequence]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Rochea", "Neutral") ]])

--Actual talking.
fnCutscene([[ Append("Mei:[E|Neutral] That whispering...[P] I can hear the voices of the forest![B][C]") ]])
fnCutscene([[ Append("Alraune: Welcome, leaf-sister.[P] What is your name?[B][C]") ]])
if(iMetRochea == 0.0) then
    fnCutscene([[ Append("Mei:[E|Happy] Mei.[P] My name is Mei, leaf-sister.[P] Thank you, thank you![B][C]") ]])
    fnCutscene([[ Append("Alraune: I am Rochea.[P] I am honored to be allowed to join you.[B][C]") ]])
else
    fnCutscene([[ Append("Mei:[E|Happy] Mei.[P] My name is Mei, leaf-sister.[P] Haven't we met?[B][C]") ]])
    fnCutscene([[ Append("Alraune: Indeed.[P] I am Rochea.[P] I am honored to have joined you, and that you remembered me.[B][C]") ]])
end
fnCutscene([[ Append("Mei:[E|Happy] As I am honored to be joined![B][C]") ]])
fnCutscene([[ Append("Rochea: Now that your body has become pure, your mind must follow suit.[P] Will you partake of the cleansing fungus?[B][C]") ]])
if(iFlorentinaToldFungus == 0.0) then
    fnCutscene([[ Append("Mei:[E|Neutral] Cleansing fungus?[P] What is that?[B][C]") ]])
    fnCutscene([[ Append("Rochea: Many of those we bring here wish to forget their human past, to join the forest as we have.[P] They must unlearn their hateful behaviors to live in harmony.[P] For that, we have the cleansing fungus.[P] It will purge the memories of your old life and you will be reborn as one of us.[B][C]") ]])
end
fnCutscene([[ Append("Mei:[E|Sad] I want to, I want to very badly.[P] Yet, I cannot.[B][C]") ]])
fnCutscene([[ Append("Rochea: Are you certain?[P] A life of love and sorority is within your grasp;[P] do not allow your human past to own your future.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] It is not my wish, it is this runestone I found.[P] I think it brought me here, to this world, and to you.[P] For me, it has a greater design, and I must follow it.[P] I am sorry...[B][C]") ]])
fnCutscene([[ Append("Rochea: ...[P] I see that you are determined.[P] It is not our way to force you to stay with us.[P] Go, leaf-sister.[P] Nature will guide you.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Blush] Thank you for understanding.[P] I hope I can return when I have the answers I need.[B][C]") ]])
fnCutscene([[ Append("Rochea: But before you go...[P] please take this.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] [SOUND|World|TakeItem](Received Wildflower's Katana)[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Surprise] A sword?[B][C]") ]])
fnCutscene([[ Append("Rochea: Left here by a leaf-sister who chose a life of peace.[P] We have enchanted it, and while we hope you never need to use it...[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Never in anger, leaf-sister.[B][C]") ]])
fnCutscene([[ Append("Rochea: Go, Mei, find your path.[P] May we meet again when the time is right.") ]])
fnCutsceneBlocker()

--Clean images.
fnCutscene([[ fnUnloadBitmapsFromList("Chapter 1 Mei Alraune TF") ]])

-- |[ ==================== Finish Up =================== ]|
--Wait a bit.
fnCutsceneWait(10)
fnCutsceneBlocker()

--Black the screen out.
fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneWait(45)
fnCutsceneBlocker()

--Return to the last save point and execute the post-script..
fnCutscene([[ AL_BeginTransitionTo("LASTSAVE", gsStandardReliveEnd) ]])
fnCutsceneBlocker()
