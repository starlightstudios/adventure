-- |[ =================================== Beehive Volunteer ==================================== ]|
--This is used exclusively by the relive handler to simulate the volunteer bee transformation sequence.
-- It is always expected to be fired during a relive.

-- |[ ======================================= Scene Setup ====================================== ]|
-- |[Variables]|
--Variables.
local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")

-- |[Remove Florentina]|
--Do it immediately in this case.
fnRemovePartyMember("Florentina", true)

-- |[Form]|
--Reshift Mei back into a Human.
LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Human.lua")

-- |[Music]|
AL_SetProperty("Music", "Null")

-- |[Flags]|
VM_SetVar("Root/Variables/Chapter1/Scenes/iUseSpecialBeeOpening", "N", 1.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasConsumedHoney", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasWorkedNectar", "N", 0.0)

-- |[ ======================================== Execution ======================================= ]|
--Mei is alone.
if(bIsFlorentinaPresent == false) then
    
    --Dialogue.
    fnStandardMajorDialogue()
    fnCutscene([[ Append("Mei:[E|Neutral] (I *am* kind of hungry...[P] Just a little bit...)[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] *slurp*[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Blush] (Oohhhh, so sweet.[P]*slurp*[P] So gooooood...)[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Blush] (It's so thick...[P] OH![P] I got some on my dress...)[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Blush] *giggles*[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Blush] *slurp*[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Blush] (Mmmmmm.....)[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Blush] *slurp*[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Blush] It feels so good on my hands...[P] On my lips...[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Blush] (Maybe I...[P] should rub it on my skin...)[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Blush] (...)[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Blush] (...)[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Blush] (It's hardening?[P] Oh?[P] It's supposed to?[P] But I wanted to eat more...)[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Blush] (I'm kind of tired, though...[P] I'll sleep now, instead...)") ]])
    fnCutsceneBlocker()
    
    --Wait a bit.
    fnCutsceneWait(30)
    fnCutsceneBlocker()
    
    --Execute the bee cutscene. It should do the rest, with the flag set so Mei reacts correctly.
    LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/200 Defeat/Defeat_Bee/Scene_Begin.lua")

--Florentina is present.
else

    --Cutscene setup. Florentina is on the opposite side.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
    
    --Dialogue.
    fnCutscene([[ Append("Mei:[E|Neutral] (Just a little bit...)[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] *slurp*[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Surprise] M-[P]Mei!?[P] What in the wastes are you doing?![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Blush] It's so good...[P] You should try some...[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Surprise] Mei![P] Mei![P] Look at me, focus on me![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Blush] Ooh...[P] I got some on my dress...[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Blush] *giggles*[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] This honey...[P] it's gotta be laced with a psychoactive![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Blush] Mmmmmmm...[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Stop eating it, you dolt![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Blush] I want it...[P] so badly...[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] Don't you dare stop me![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Surprise] Cripes![P] Mei, you're not yourself![P] Try to focus![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Blush] ...[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Blush] ...[P] They call...") ]])
    fnCutsceneBlocker()
    
    --Wait a bit.
    fnCutsceneWait(30)
    fnCutsceneBlocker()

    --Cutscene setup. Florentina is on the opposite side.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Florentina", "Neutral") ]])
    fnCutscene([[ Append("Florentina:[E|Offended] G-[P]get away![P] Stupid bees![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Offended] Mei![P] Mei![P] I'll come back with help![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] (Idiot![P] What did she think was going to happen?)") ]])
    fnCutsceneBlocker()
    
    --Wait a bit.
    fnCutsceneWait(240)
    fnCutsceneBlocker()
    
    --Execute the bee cutscene. It should do the rest, with the flag set so Mei reacts correctly.
    LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/200 Defeat/Defeat_Bee/Scene_Begin.lua")
end