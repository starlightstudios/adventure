-- |[ =================================== Werecat Volunteer ==================================== ]|
--This is used exclusively by the relive handler to simulate the volunteer bee transformation sequence.
-- It is always expected to be fired during a relive.

-- |[ ======================================= Scene Setup ====================================== ]|
--Switch Mei's form to human.
LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Human.lua")

--Clear censor bars.
WD_SetProperty("Clear Censor Bars")

-- |[ ======================================== Execution ======================================= ]|
--Order Cassandra's scene images to load.
fnLoadDelayedBitmapsFromList("Chapter 1 Cassandra", gciDelayedLoadLoadAtEndOfTick)
fnLoadDelayedBitmapsFromList("Chapter 1 Mei Werecat TF", gciDelayedLoadLoadAtEndOfTick)

--Wait a bit.
fnCutsceneWait(85)
fnCutsceneBlocker()

--Reboot the dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Werecat", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])

--Talking.
fnCutscene([[ Append("Werecat: Valiant, but futile.[P] Submit to us, human.[P] You shall join our pride.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] I...[P] I...[B][C]") ]])
fnCutscene([[ Append("Werecat: Come...[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Blush] I...[P] submit...") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(85)
fnCutsceneBlocker()

--Activate scenes mode.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
fnCutscene([[ Append("Mei stepped gingerly into the glade.[P] The girl, bound, gagged, and helpless, had watched the battle.[P] She continued to struggle against her bonds as Mei knelt beside her.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF0") ]])
fnCutscene([[ Append("Mei's head was swimming as she began feeling at the girl's body.[P] The cats had been here, awaiting the moon's grace.[P] They were touching themselves and one another in anticipation.[P] Mei followed suit.[B][C]") ]])
fnCutscene([[ Append("The other werecats scratched at her, causing her skin to tingle wherever their cursed claws went.[P] There was no pain, only excitement.[P] Mei soon began scratching at the bound girl, fresh curse running through her own fingernails.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/WerecatTF1") ]])
fnCutscene([[ Append("As the moon drew nearer, the girl stopped struggling.[P] At the sight of the girl, Mei had become wet, and could sense that the girl had as well.[P] Lost in a bestial surge of pheromones, Mei pulled at the girl's pants and began fingering her sex.[B][C]") ]])
fnCutscene([[ Append("At last the moonlight became strong enough.[P] Mei marvelled at her hands as the curse took over her, turning them to claws and growing black fur all across her body.[P] The scent of the night struck her new nose as the same changes followed on the girl.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF0") ]])
fnCutscene([[ Append("Now kinfang, she used her claws to tear off the bindings.[P] The blonde fang had not changed as much as she had. She seemed horrified as her claws formed and her feet remolded to suit her new purpose.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/WerecatTF1") ]])
fnCutscene([[ Append("Fur grew out from her skin and her teeth sharpened into fangs. While the new instincts of her body began to change her mind, her horror turned to confusion. Uncertainty. Mei dealt with her by planting a kiss on her lips.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Cassandra/Werecat") ]])
fnCutscene([[ Append("The thrill of the moonlight overcame the last of the blonde fang's resistance, and she revelled in the changes. She returned Mei's kiss and began groping at the cats who stood near her.[B][C]") ]])
fnCutscene([[ Append("More cats had entered the clearing, smelling the pheromones.[P] The cats swarmed over the lip-locked lovers, kissing and licking.[P] A werecat began fingering Mei's sex while others licked her fur and skin with their rough tongues.[B][C]") ]])
fnCutscene([[ Append("Mei and the blonde fang lost themselves in the orgy of cats as the moon gave them its perfect grace.[P] Minutes dragged into hours as cats continued to suck and lick one another.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ Append("...[B][C]") ]])
fnCutscene([[ Append("Eventually, the moon's arc bent past the line of trees.[P] With the loss of their patron, the endless orgasms exhausted the night hunters.[P] Soon, Mei drifted to sleep with the blonde fang still in her arms.[B][C]") ]])
fnCutscene([[ Append("...[B][C]") ]])
fnCutscene([[ Append("When she woke, most of the cats had gone.[P] The new fang was nowhere in sight.[P] Mei stood and stretched, her tongue cleaning the juices that lingered in her fur.[P] She could taste the new fang there, and she tasted strong.[B][C]") ]])
fnCutscene([[ Append("Mei left the clearing and made her way back to the camp she had visited earlier.[P] Her quest called to her again, but the lure of the pride was nearly enough to make her forget it.[P] She longed to hunt in the night, but her quest loomed greater than the moon...") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(60)
fnCutsceneBlocker()
    
--Unload.
fnCutscene([[ fnUnloadBitmapsFromList("Chapter 1 Cassandra") ]])
fnCutscene([[ fnUnloadBitmapsFromList("Chapter 1 Mei Werecat TF") ]])

-- |[Exit]|
--Return to the last save point and execute the post-script..
fnCutscene([[ AL_BeginTransitionTo("LASTSAVE", gsStandardReliveEnd) ]])
fnCutsceneBlocker()