-- |[ ===================================== Slime Volunteer ==================================== ]|
--This is used exclusively by the relive handler to simulate the volunteer bee transformation sequence.
-- It is always expected to be fired during a relive.

-- |[ ======================================= Scene Setup ====================================== ]|
--Variables.
local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")

--Reshift Mei back into a Human.
LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Human.lua")

--Load images.
fnLoadDelayedBitmapsFromList("Chapter 1 Mei Slime TF", gciDelayedLoadLoadAtEndOfTick)

--Clear censor bars.
WD_SetProperty("Clear Censor Bars")

-- |[ ======================================== Execution ======================================= ]|
-- |[Dialogue]|
fnStandardMajorDialogue()
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Green") ]])
fnCutscene([[ Append("Slime: Hey, are you a human?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] ...[P] Yes?[B][C]") ]])
fnCutscene([[ Append("Slime: Wow![P] A human visitor![P] We don't get those very often![P] Or, like, ever![P] I don't know the etiquette, TBH.[B][C]") ]])
fnCutscene([[ Append("Slime: Would you like something to drink?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Sure, why not?[B][C]") ]])
	
--Florentina has a bit of a problem with this:
if(bIsFlorentinaPresent == true) then
    fnCutscene([[ Append("Florentina:[E|Confused] Seriously, Mei?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] Oh, come off it.[P] She seems really nice.[B][C]") ]])
    fnCutscene([[ Append("Slime: I'm totally the nicest slime and junk![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] See?[P] Look at those puppy-dog eyes![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] If you get sick, it's not my problem.[B][C]") ]])
end

--Resume.
fnCutscene([[ Append("Mei:[E|Smirk] *Glug*[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] ...[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] ...[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Happy] Delicious![P] What was it?[B][C]") ]])
fnCutscene([[ Append("Slime: Good old plain water![P] It's all we slimes drink![B][C]") ]])

--Florentina isn't going to eat any crow.
if(bIsFlorentinaPresent == true) then
    fnCutscene([[ Append("Mei:[E|Offended] See, Florentina?[P] No need to be suspicious![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Oh yeah, of course it was water.[P] Thick, viscous, green water.[B][C]") ]])
end
fnCutscene([[ Append("Slime: Oh, whoops.[P] I think I got some of my slime in there...[P] whoopsie doodle![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Smirk] ...[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Sad] I feel...[P] kinda light headed...") ]])
fnCutsceneBlocker()

--Switch to scene mode.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Null") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/SlimeTF0") ]])
fnCutscene([[ Append("Mei:[Voice|Mei] Oh no![B][C]") ]])
fnCutscene([[ Append("Slime:[Voice|Slime] I'm sorry![P] Here, let me help you![B][C]") ]])
fnCutscene([[ Append("Mei:[Voice|Mei] You're just getting more slime on me![B][C]") ]])
fnCutscene([[ Append("Mei began to panic, thrashing about as she tried to shake the slime off.[P] Her efforts were in vain, as the slime infection was coming from deep within her.[B][C]") ]])
fnCutscene([[ Append("The slime girl desperately padded at Mei, trying to wipe away what she could.[P] Everywhere she touched, more slime went.") ]])
fnCutsceneBlocker()

--Next scene.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/SlimeTF0") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/SlimeTF1") ]])
fnCutscene([[ Append("Mei:[Voice|Mei] K-keep going...[B][C]") ]])
fnCutscene([[ Append("The infection reached up to Mei's head.[P] Suddenly, the spread of the slime wasn't a problem.[P] In fact, it began to feel pleasurable...[B][C]") ]])
fnCutscene([[ Append("Slime:[Voice|Slime] Oops![P] Oh that -[P] oh my![B][C]") ]])
fnCutscene([[ Append("Mei:[Voice|Mei] More slime...[P] rub me...[B][C]") ]])
fnCutscene([[ Append("Slime:[Voice|Slime] Oh geez, I'm messing this up![B][C]") ]])
fnCutscene([[ Append("Mei:[Voice|Mei] More...[B][C]") ]])
fnCutscene([[ Append("The slime took over Mei's body.[P] Her slime sister ran her goopy hands all over Mei's body, arousing her more and more with each passing moment.[B][C]") ]])
fnCutscene([[ Append("She began to rub herself in tandem, swirling her own slime around.[P] The sensation was incredible, an orgasm more intense than any she had experienced as a human...") ]])
fnCutsceneBlocker()

--Finished.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Slime.lua") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/SlimeTF1") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/SlimeTF2") ]])
fnCutscene([[ WD_SetProperty("Register Censor Bar", 605, 179, 112, 45) ]])
fnCutscene([[ Append("Mei:[Voice|Mei] Yeah...[P] yeah...[P] I'm feeling so much better...[B][C]") ]])
fnCutscene([[ Append("She had become slime like the girl next to her.[P] Every inch of her body was now an erogenous zone, and she slid her slimy hands up and down her slimy membrane.[P] The squishing, slopping feeling brought her to climax as her slime sister withdrew.[B][C]") ]])
fnCutscene([[ Append("The slime stepped back, head hung in shame.[P] Mei placed her slimy hand under the girl's chin and lifted her head up.[P] Mei smiled as broadly as she could, and brought solace to the poor slime girl.") ]])
fnCutsceneBlocker()

--Reboot the dialogue.
fnStandardMajorDialogue(true)
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Green") ]])
fnCutscene([[ Append("Slime: Ah geez I am sooooooo super sorry![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Happy] Whatever for?[P] I'm all slimy![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Happy] And I got your smarty fruit in me![P] I'm a smarty slime![B][C]") ]])
if(bIsFlorentinaPresent == true) then
    fnCutscene([[ Append("Florentina:[E|Happy] ...[P] Dumbass.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Laugh] You're just jealous![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] Well, you can't be any dumber now that you're a slime.[P] So, no great loss there.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Thanks, Florentina![B][C]") ]])
end
fnCutscene([[ Append("Mei:[E|Neutral] Well, thank you for this eye-opening experience.[B][C]") ]])
fnCutscene([[ Append("Slime: You're okay with this?[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Happy] Sure![P] This is awesome![B][C]") ]])
fnCutscene([[ Append("Mei:[E|Happy] But I've got an adventure to get back to.[P] See you later!") ]])
fnCutsceneBlocker()

-- |[Clean]|
--Clean up images.
fnCutscene([[ fnUnloadBitmapsFromList("Chapter 1 Mei Slime TF") ]])
	
--Wait a bit.
fnCutsceneWait(10)
fnCutsceneBlocker()

-- |[Exit]|
--Return to the last save point and execute the post-script..
fnCutscene([[ AL_BeginTransitionTo("LASTSAVE", gsStandardReliveEnd) ]])
fnCutsceneBlocker()