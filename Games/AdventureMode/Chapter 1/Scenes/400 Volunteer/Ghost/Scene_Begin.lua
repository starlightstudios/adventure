-- |[ ===================================== Ghost Volunteer ==================================== ]|
--This is used exclusively by the relive handler to simulate the volunteer bee transformation sequence.
-- It is always expected to be fired during a relive.

--Clear censor bars.
WD_SetProperty("Clear Censor Bars")

-- |[ ======================================= Scene Setup ====================================== ]|
-- |[Variables]|
--Variables.
local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")

-- |[Remove Florentina]|
--Do it immediately in this case.
fnRemovePartyMember("Florentina", true)

-- |[Form]|
--Reshift Mei back into a Human.
LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Human.lua")

-- |[Music]|
AL_SetProperty("Music", "Null")

-- |[Flags]|
--Base.
VM_SetVar("Root/Variables/Chapter1/Scenes/iIsGhostTF", "N", 1.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iPutMaidOutfitOn", "N", 1.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iToldNatalieTwice", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCanLeaveRoomW", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iLydieLeftParty", "N", 0.0)

--Cleaning Progress
VM_SetVar("Root/Variables/Chapter1/Scenes/iMadeBed", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedBookshelvesW", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedBookshelvesE", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedChairW", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedChairC", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedChairE", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedTableW", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedTableC", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedTableE", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N", 0.0)

-- |[Images]|
fnLoadDelayedBitmapsFromList("Chapter 1 Mei Ghost TF", gciDelayedLoadLoadAtEndOfTick)

-- |[ ======================================== Execution ======================================= ]|
-- |[Dialogue]|
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
if(bIsFlorentinaPresent == false) then
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Surprise") ]])
else
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Surprise") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Surprise") ]])
end
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Ghost", "Neutral") ]])
fnCutscene([[ Append("Ghost:[VOICE|Ghost] You are sick.[P] We can help you.[P] Please, come with me.[B][C]") ]])

--Mei is alone:
if(bIsFlorentinaPresent == false) then
    fnCutscene([[ Append("Mei:[E|Sad] Sick?[P] I'm not sick...[B][C]") ]])
    fnCutscene([[ Append("Ghost:[VOICE|Ghost] Look at me.[P] Breathe deeply.[P] You are sick.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] ...[BLOCK]") ]])
    fnCutsceneBlocker()

--Florentina is here:
else
    fnCutscene([[ Append("Mei:[E|Sad] Sick?[P] I'm not sick...[B][C]") ]])
    fnCutscene([[ Append("Ghost:[VOICE|Ghost] Look at me.[P] Breathe deeply.[P] You are sick.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] ...[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Mei?[P] Mei![P] Get a hold of yourself![B][C]") ]])
    fnCutscene([[ Append("Ghost:[VOICE|Ghost] You're ill.[P] We can help.[P] Please, listen to me...[BLOCK]") ]])
    fnCutsceneBlocker()
end
        
--Wait a bit.
fnCutsceneWait(60)
fnCutsceneBlocker()
    
-- |[Dialogue]|
--Reboot the dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
if(bIsFlorentinaPresent == false) then
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Offended") ]])
else
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Offended") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Confused") ]])
end
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Ghost", "Neutral") ]])

--Text.
fnCutscene([[ Append("Mei:[E|Offended] [P]Can...[P] you...[P] help me?[B][C]") ]])
fnCutscene([[ Append("Ghost:[VOICE|Ghost] Yes.[P] Follow me.[B][C]") ]])

--Mei is alone:
if(bIsFlorentinaPresent == false) then
    fnCutscene([[ Append("Mei:[E|Sad] My skin...[P] It looks wrong...[B][C]") ]])
    fnCutscene([[ Append("Ghost:[VOICE|Ghost] It is an effect of the plague.[P] We must wash you immediately.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] Please do...[P] I feel...[P] faint...[B][C]") ]])
    fnCutsceneBlocker()
    
--Florentina is present:
else
    fnCutscene([[ Append("Florentina:[E|Confused] What are you doing, Mei?[P] Hey![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Can you hear me?[P] Mei![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] Did you hear something...?[B][C]") ]])
    fnCutscene([[ Append("Ghost:[VOICE|Ghost] No.[P] We are alone.[B][C]") ]])
    fnCutscene([[ Append("Ghost:[VOICE|Ghost] You're running a fever.[P] You must be hearing things.[P] Come quickly.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] My skin...[P] It looks wrong...[B][C]") ]])
    fnCutscene([[ Append("Ghost:[VOICE|Ghost] It is an effect of the plague.[P] We must wash you immediately.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Mei![P] Don't -[P][CLEAR]") ]])
    fnCutscene([[ Append("[P][SOUND|World|Thump][P][CLEAR]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] ...") ]])
    fnCutsceneBlocker()
end
    
--Wait a bit.
fnCutsceneWait(120)
fnCutsceneBlocker()

--Scene.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
fnCutscene([[ Append("The maid led Mei through the courtyard and back into the main building.[P] A chill ran up and down her body, and her teeth chattered as they walked.[P] Her head spun and the world became a blur.[B][C]") ]])
fnCutscene([[ Append("She saw other maids as they went.[P] They gave her a passing glance or a curt nod, scarcely covering their disinterest in her.[P] Doubtless they had seen many more sick like her.[B][C]") ]])
fnCutscene([[ Append("The maid led her to the baths.[P] Without needing to be told, she disrobed and stepped into the waters.[B][C]") ]])
fnCutscene([[ Append("Her chill seemed to abate.[P] The water was warm and had a flowery aroma.[P] The maid poured some salts into the bath as Mei began to clean herself.[B][C]") ]])
fnCutscene([[ Append("[P]She floated for a time.[P] Her hostess said nary a word.[P] She closed her eyes and enjoyed the bath, and the feeling of cleanliness.[P] She hated being sick.[P] Why had fate done this to her?[B][C]") ]])
fnCutscene([[ Append("After a few moments, the maid gently prodded her.[P] She knew she needed to leave the bath, but her clothes had gone.[P] She dried herself with a nearby towel and wondered what had become of them.[B][C]") ]])
fnCutscene([[ Append("She gave an inquiring look to the maid.[P] [VOICE|Ghost]'Your clothes will need to be cleaned, I sent them to your room.[P] Please, wear these instead.'[B][C]") ]])
fnCutscene([[ Append("The maid produced a uniform much like her own.[P] [VOICE|Ghost]'I'm sorry, but we have no clothes in your size.[P] This is all that is available.'") ]])
fnCutsceneBlocker()

--Restart.
fnCutscene([[ WD_SetProperty("FastShow") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/GhostTF0") ]])
fnCutscene([[ Append("The maid uniform was the most beautiful dress she had ever seen.[P] She loved the frilly lace and ribbons that adorned it, and the fabric was finely made and soft on her skin.[P] She pulled it over her and felt a weight lift from her heart.[B][C]") ]])
fnCutscene([[ Append("She felt a draft in the room.[P] Something seemed to be blowing over and around her, but the lovely dress absorbed her full attention.[P] She scarcely noted something hard circling around her feet, clanking as it went...") ]])
fnCutsceneBlocker()

--Restart.
fnCutscene([[ WD_SetProperty("FastShow") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/GhostTF0") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/GhostTF1") ]])
fnCutscene([[ Append("Something slammed shut around her foot, as another cold, hard feeling began snaking up her leg.[P] She looked down in horror to see a chain climbing her leg -[P] no, her leg -[P] her leg had become a vapour![B][C]") ]])
fnCutscene([[ Append("A horrible realization gripped Mei -[P] none of this was real![P] The bath, the maid -[P] the maid was a ghost![P] She had tricked her and now -[P][CLEAR]") ]])
fnCutscene([[ Append("[P]Calm.[P] No need to panic.[P] Everything was fine.") ]])
fnCutsceneBlocker()

--Restart.
fnCutscene([[ WD_SetProperty("FastShow") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/GhostTF1") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Ghost") ]])
fnCutscene([[ Append("The strange feeling, of something cold blowing, had come again.[P] Natalie was unperturbed.[P] Bath time was over, and she had to resume her duties.[B][C]") ]])
fnCutscene([[ Append("She thought, perhaps, she had fallen asleep in the bath.[P] It happened fairly often.[P] The water was warm and comfortable, she certainly wouldn't have been the first to do so.[P] But her dream had been so vivid.[B][C]") ]])
fnCutscene([[ Append("A charming, pretty girl, from Hong Kong, fighting to find her way home.[P] It seemed so unreal, but then, dreams always did.[B][C]") ]])
fnCutscene([[ Append("She had been assigned to clean the guest room.[P] She made her way there, thinking about Mei and her fantastical journey...") ]])
fnCutsceneBlocker()
    
--Unload.
fnCutsceneWait(gciDialogue_Fadeout_Then_Unload_Ticks)
fnCutsceneBlocker()
fnCutscene([[ fnUnloadBitmapsFromList("Chapter 1 Mei Ghost TF") ]])

--Transfer:
fnCutsceneWait(60)
fnCutsceneBlocker()
fnCutscene([[ AL_BeginTransitionTo("QuantirManseCentralW", gsRoot .. "Chapter 1/Scenes/200 Defeat/Defeat_Ghost/Scene_Alternate.lua") ]])
fnCutsceneBlocker()