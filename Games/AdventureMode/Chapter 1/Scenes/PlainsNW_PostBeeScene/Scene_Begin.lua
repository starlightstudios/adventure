-- |[Outland Farm Post-Bee Scene]|
--After Mei gets turned into a bee, this scene plays on her way back.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenOutlandBeeScene", "N", 1.0)

-- |[Variables]|
local iIsFlorentinaPresent = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
local iFlorentinaKnowsAboutRune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")
local iHasFoundOutlandAcolyte = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasFoundOutlandAcolyte", "N")
local iSavedClaudia = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedClaudia", "N")

-- |[Initial Movements]|
--Mei walks towards the NPCs assembled outside the farm.
Cutscene_CreateEvent("Move Mei To Actors", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (15.25 * gciSizePerTile), (17.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Face Mei to the south.
Cutscene_CreateEvent("Face Mei South", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face", 0, 1)
DL_PopActiveObject()
fnCutsceneWait(2)
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(5)
fnCutsceneBlocker()

-- |[Florentina Not Present]|
--If Florentina is not present, the cutscene is slightly different.
if(iIsFlorentinaPresent == 0.0) then

	--Set this variable to 2.0 so Florentina doesn't ask about bee stuff at the next campfire.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iUseSpecialBeeOpening", "N", 2.0)

	--Dialogue setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	
	--Claudia is not present:
	if(iSavedClaudia == 0.0) then
	
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "MercF", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Karina", "Neutral") ]])

		--If Mei has not previously met Karina:
		if(iHasFoundOutlandAcolyte == 0.0) then

			--Flag.
			VM_SetVar("Root/Variables/Chapter1/Scenes/iHasFoundOutlandAcolyte", "N", 2.0)

			--Talking.
			fnCutscene([[ Append("Guard: Hey, back up, bee.[P] Go on, scram.[P] Stay on your side of the river.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Wait![P] I mean you no harm, I just need to pass through here.[B][C]") ]])
			fnCutscene([[ Append("Lady: ...[B][C]") ]])
			fnCutscene([[ Append("Lady: ...[B][C]") ]])
			fnCutscene([[ Append("Lady: It talked! I was right![P] I was so right![P] Eeeeee![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Why of course I can talk.[P] Please just let me through, I don't want a fight.[B][C]") ]])
			fnCutscene([[ Append("Lady: L-[P]let her through![P] Do as she says![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Happy] Thank you very much.[B][C]") ]])
			fnCutscene([[ Append("Guard: Hrmpf.[P] I'll let her through, but you don't call the shots around here.[P] We've got our eyes on you, bee.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Sad] I don't want to cause any trouble.[P] I...[P] I'm sorry...[B][C]") ]])
			fnCutscene([[ Append("Lady: No trouble at all![P] Oh this is so exciting![B][C]") ]])
			fnCutscene([[ Append("Mei: You...[P][E|Neutral] the hive speaks extensively of you.[P] You are interested in us?[B][C]") ]])
			fnCutscene([[ Append("Lady: Very much so![P] My name is Karina, and I'm researching your hive right now.[B][C]") ]])
			fnCutscene([[ Append("Karina: I'd love to give you an interview, if you're willing.[P] I honestly didn't know bees could talk.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Offended] We haven't much to say to humans.[B][C]") ]])
			fnCutscene([[ Append("Karina: But you do?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Happy] I'm a non-conformist.[B][C]") ]])
			fnCutscene([[ Append("Karina: Splendid![P] About that interview...[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] It'll have to wait.[P] I'm on a very important mission for the hive.[B][C]") ]])
			fnCutscene([[ Append("Karina: Oh, oh![P] You've already given me so much to write about.[B][C]") ]])
			fnCutscene([[ Append("Karina: If you change your mind, please, I'm sure I can find something to reward your hive with.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] The hive will consider your offer.[B][C]") ]])
			fnCutscene([[ Append("Guard: Just don't get up to anything suspicious.") ]])
			fnCutsceneBlocker()
		
		--If Mei has previously met Karina:
		else

			--Talking.
			fnCutscene([[ Append("Guard: Hey, back up, bee.[P] Go on, scram.[P] Stay on your side of the river.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Wait![P] I mean you no harm, I just need to pass through here.[B][C]") ]])
			fnCutscene([[ Append("Lady: H-[P]hey![P] I recognize you![B][C]") ]])
			fnCutscene([[ Append("Lady: Oh, I hope you didn't get turned into a bee on my account.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Blush] You were the one researching us, right?[P] I'm afraid it wasn't on your account.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] I don't want a fight.[P] Please let me pass, I'll be good.[P] We promise.[B][C]") ]])
			fnCutscene([[ Append("Lady: Could you do that for me, Lieutenant?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Please?[B][C]") ]])
			fnCutscene([[ Append("Guard: Fine.[P] Don't try anything suspicious, talking bee, or we'll be all over you like a smoke cloud.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Thank you very much, miss...[B][C]") ]])
			fnCutscene([[ Append("Lady: Sister Karina, at your service.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Laugh] Mei, the talking bee, evidently.[B][C]") ]])
			fnCutscene([[ Append("Karina: Do you think you could spare me some time for an interview?[P] You're literally the only talking bee I've ever encountered.[B][C]") ]])
			fnCutscene([[ Append("Karina: Is it a residual after-effect of having met me earlier?[P] Was there an emotional bond that drew you from the hive mind?[P] I have so many questions![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] I'd love to, but the hive has charged me with a very important mission.[P] Your questions will have to wait.[B][C]") ]])
			fnCutscene([[ Append("Karina: Pleasepleaseplease?[P] I'm sure I could find something to reward your hive with![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] The hive will consider your offer.[B][C]") ]])
			fnCutscene([[ Append("Guard: Just don't get up to anything suspicious.") ]])
			fnCutsceneBlocker()

		end
	
	--Claudia is present:
	else
	
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "MercF", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Claudia", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Karina", "Neutral") ]])

		--If Mei has not previously met Karina:
		if(iHasFoundOutlandAcolyte == 0.0) then

			--Flag.
			VM_SetVar("Root/Variables/Chapter1/Scenes/iHasFoundOutlandAcolyte", "N", 2.0)

			--Talking.
			fnCutscene([[ Append("Guard: Hey, back up, bee.[P] Go on, scram.[P] Stay on your side of the river.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Wait![P] I mean you no harm, I just need to pass through here.[B][C]") ]])
			fnCutscene([[ Append("Claudia: Do my eyes betray me?[P] Is that you, my savior, Mei?[B][C]") ]])
			fnCutscene([[ Append("Lady: Mei?[P] The one who rescued you?[B][C]") ]])
			fnCutscene([[ Append("Claudia: She has changed form again.[P] She walks between them as water flows in the river.[B][C]") ]])
			fnCutscene([[ Append("Claudia: Lieutenant, this bee poses no threat to you.[P] Please, allow her to pass.[P] She is the one who rescued me.[B][C]") ]])
			fnCutscene([[ Append("Guard: Oh, so this bee is responsible for inflicting you on the mercs here?[B][C]") ]])
			fnCutscene([[ Append("Guard: Ugh.[P] Fine.[P] Don't try anything, bee.[P] Talking or not, I don't trust you.[B][C]") ]])
			fnCutscene([[ Append("Claudia: I apologize, Mei.[P] I did not recognize you at first.[B][C]") ]])
			fnCutscene([[ Append("Claudia: This is Sister Karina.[P] She is the Dove responsible for studying your hive.[B][C]") ]])
			fnCutscene([[ Append("Karina: Thank you so much for rescuing Sister Claudia![P] I don't know what I'd do if she didn't come back...[B][C]") ]])
			fnCutscene([[ Append("Claudia: You would pray for guidance and prostrate yourself before the divine, child.[B][C]") ]])
			fnCutscene([[ Append("Claudia: If no guidance came, you were to return to the town of Dry Well.[P] Do you remember?[B][C]") ]])
			fnCutscene([[ Append("Karina: Oh, right.[P] Sorry, Sister.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Good to see that you're okay after being cooped up in that prison.[B][C]") ]])
			fnCutscene([[ Append("Claudia: A test of faith, and no more.[B][C]") ]])
			fnCutscene([[ Append("Claudia: Clearly your runestone draws you to a larger destiny.[P] The bees cannot swerve the path of the divine.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Uh, sure.[P] The hive tasked me with finding out more about my runestone, so I better get to it.[P] Don't want to leave the drones hanging...[B][C]") ]])
			fnCutscene([[ Append("Karina: Wait![P] Can I interview you?[P] It'd help the cause of science![B][C]") ]])
			fnCutscene([[ Append("Claudia: Karina, she has aided us enough.[P] Press no more on her.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] I'll let the hive know you asked.[P] I'm sure I'll be back once I've figured this thing out.[P] Until then!") ]])
			fnCutsceneBlocker()

		end
	end

-- |[Florentina Is Present, Has Not Seen TF]|
--If Florentina was present before the transformation, the cutscene focuses slightly more on her. In this case she doesn't know about Mei's transformation ability.
elseif(iIsFlorentinaPresent == 1.0 and (iFlorentinaKnowsAboutRune == 0.0 or iSavedClaudia == 1.0)) then

	--This cutscene counts as meeting Karina.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iHasFoundOutlandAcolyte", "N", 1.0)
	
	--This cutscene also reveals the power of Mei's runestone to Florentina.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N", 1.0)

	--Dialogue setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Florentina", "Neutral") ]])
	
	--Claudia is not present:
	if(iSavedClaudia == 0.0) then
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Karina", "Neutral") ]])
	
	--Claudia is present:
	else
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Claudia", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Karina", "Neutral") ]])
	end

	--Talking.
	fnCutscene([[ Append("Florentina:[E|Neutral] Look, I'm just asking nicely.[P] She'll have black hair and a slim build.[B][C]") ]])
	fnCutscene([[ Append("Lady: You mean like that one?") ]])
	fnCutsceneBlocker()
	
	--Florentina and Karina look at Mei.
	fnCutsceneWait(30)
	fnCutsceneBlocker()
	Cutscene_CreateEvent("Face Florentina North", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", 0, -1)
	DL_PopActiveObject()
	Cutscene_CreateEvent("Face Karina North", "Actor")
		ActorEvent_SetProperty("Subject Name", "Karina")
		ActorEvent_SetProperty("Face", 0, -1)
	DL_PopActiveObject()
	fnCutsceneWait(120)
	fnCutsceneBlocker()
	
	--They face each other again.
	Cutscene_CreateEvent("Face Florentina North", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", -1, 0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("Face Karina North", "Actor")
		ActorEvent_SetProperty("Subject Name", "Karina")
		ActorEvent_SetProperty("Face", 1, 0)
	DL_PopActiveObject()
	fnCutsceneWait(15)
	fnCutsceneBlocker()

	--Dialogue setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Florentina", "Neutral") ]])
	
	--Claudia is not present:
	if(iSavedClaudia == 0.0) then
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Karina", "Neutral") ]])
	
	--Claudia is present:
	else
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Claudia", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Karina", "Neutral") ]])
	end

	--Talking.
	fnCutscene([[ Append("Florentina:[E|Happy] Nope, that one's way too ugly.[P] Couldn't be her.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Florentina?[P] You're okay?") ]])
	fnCutsceneBlocker()
	
	--Florentina and Karina look at Mei again.
	fnCutsceneBlocker()
	Cutscene_CreateEvent("Face Florentina North", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", 0, -1)
	DL_PopActiveObject()
	Cutscene_CreateEvent("Face Karina North", "Actor")
		ActorEvent_SetProperty("Subject Name", "Karina")
		ActorEvent_SetProperty("Face", 0, -1)
	DL_PopActiveObject()
	fnCutsceneWait(45)
	fnCutsceneBlocker()

	--Dialogue setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Florentina", "Neutral") ]])
	
	--Claudia is not present:
	if(iSavedClaudia == 0.0) then
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Karina", "Neutral") ]])
	
	--Claudia is present:
	else
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Claudia", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Karina", "Neutral") ]])
	end

	--If Mei has not previously met Karina:
	if(iHasFoundOutlandAcolyte == 0.0) then

		--Not saved Claudia:
		if(iSavedClaudia == 0.0) then
			fnCutscene([[ Append("Florentina:[E|Surprise] Well the attitude is hard to mistake.[P] Mei, are you still in there?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Of course![P] I -[P] well I'm mostly all here.[P] There's lots of other drones too, but I'm here.[B][C]") ]])
			fnCutscene([[ Append("Lady: Incredible![P] She retained her identity![P] You said her name was Mei?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Yes, that's my name.[B][C]") ]])
			fnCutscene([[ Append("Lady: Stupendous![P] I don't believe it![P] Mei, Mei, could I get an interview from you?[P] Please?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] An interview?[P] Why?[B][C]") ]])
			fnCutscene([[ Append("Karina: My name is Karina, and I've been researching your hive for some time now.[B][C]") ]])
			fnCutscene([[ Append("Karina: I wasn't aware that bees could speak.[P] I could -[P] I could get all the facts from an insider's perspective![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] We were aware of your attentions, but not their nature.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Happy] Better yet, Karina here is one of Claudia's followers.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Happy] Is that so?[B][C]") ]])
			fnCutscene([[ Append("Karina: That's correct.[P] It was she who charged me with studying you.[P] I haven't heard from the convent in a few weeks, though.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Can you tell us where she went?[P] Perhaps we can find her.[B][C]") ]])
			fnCutscene([[ Append("Karina: We heard some rumours about a very rare partirhuman species in Quantir, east of here.[P] Claudia took most of the convent that way.[B][C]") ]])
			fnCutscene([[ Append("Karina: They were supposed to have come back by now, but maybe they found something incredible.[P] I'm not sure they can top this, though![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Laugh] Quantir is where we head next, then.[B][C]") ]])
			fnCutscene([[ Append("Karina: But my interview...[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] I'm sorry, but the hive has given me a very important mission.[P] I must fulfill it.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Surprise] Hey, don't you get any ideas.[P] I still want to show Claudia that runestone.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] That is the mission I have been charged with.[B][C]") ]])
			fnCutscene([[ Append("Karina: If you change your mind, I'm sure I can find something to reward your hive with.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] The hive will consider your offer.[P] Florentina, shall we be off?") ]])

			--Clear the flag on Next Move.
			WD_SetProperty("Clear Topic Read", "NextMove")
		
		--Saved Claudia:
		else
			fnCutscene([[ Append("Florentina:[E|Surprise] Well the attitude is hard to mistake.[P] Mei, are you still in there?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Of course![P] I -[P] well I'm mostly all here.[P] There's lots of other drones too, but I'm here.[B][C]") ]])
			fnCutscene([[ Append("Claudia: It is as I predicted.[P] Her path is fixed by the divine.[P] The bees cannot alter it.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] Yeah, these two won't shut up about you, Mei.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Happy] Nice to see you again, Claudia![B][C]") ]])
			fnCutscene([[ Append("Claudia: As it is to see you, Mei.[P] This is Sister Karina.[P] She had been aiding my studies until that unfortunate business in the manor.[B][C]") ]])
			fnCutscene([[ Append("Karina: Mei![P] I've been studying your hive![P] Could you maybe give me an interview?[P] I've never met a talking bee before![B][C]") ]])
			fnCutscene([[ Append("Karina: You could tell me all about what it's like being a bee![P] This will help the cause of science so much![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] I'm sorry, but the hive has given me a crucial mission.[P] I must understand my runestone and why it allows me to remember myself.[B][C]") ]])
			fnCutscene([[ Append("Claudia: Not by coincidence that the bees charge you with the task the divine has.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Confused] They've been talking like this since I got here.[P] Cripes.[B][C]") ]])
			fnCutscene([[ Append("Claudia: All is known by the divine.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Confused] Shut up?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] If I have some spare time, I'll put it on my priorities list.[P] Florentina, shall we be off?[B][C]") ]])
			fnCutscene([[ Append("Karina: But the interview...[B][C]") ]])
			fnCutscene([[ Append("Claudia: She has done much for the Doves already, Karina.[P] Trust her judgement.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Confused] *I am so glad to get out of here...*[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Offended] *She was like this the whole time when I rescued her, too!*") ]])
		end
	
	--If Mei has previously met Karina:
	else

		--Not saved Claudia:
		if(iSavedClaudia == 0.0) then
			fnCutscene([[ Append("Florentina:[E|Surprise] Well the attitude is hard to mistake.[P] Mei, are you still in there?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Of course -[P][EMOTION|Mei|Offended] hey![P] Who are you calling ugly?[B][C]") ]])
			fnCutscene([[ Append("Karina: Mei, was it?[P] Do you remember me?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Happy] Of course I do.[B][C]") ]])
			fnCutscene([[ Append("Karina: This is astounding![P] I could barely believe it when you spoke.[P] Can all bees speak?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] We still have the vocal chords for it, but mentally, it's -[P] not easy.[B][C]") ]])
			fnCutscene([[ Append("Karina: Oh, oh, I must get my notepad![B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] Forget the notepad.[P] Maybe you could tell me why you can talk, huh?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Sad] I wish I knew.[P] In fact, that's why I'm here.[P] The hive sent me to find out.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] I can say it has something to do with my runestone, but that's all.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] Saves me the trouble of busting in there and, er, \"recovering\" it.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] You were going to steal my runestone!?[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] Well the bees sure as sh*rts weren't going to need it.[B][C]") ]])
			fnCutscene([[ Append("Karina: That runestone...[P] yes that symbol does seem familiar.[P] I have no doubt Claudia could assist you.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] Good, so the goal hasn't changed.[P] Let's get going.[B][C]") ]])
			fnCutscene([[ Append("Karina: W-[P]wait![P] Mei![P] Please, I have so many questions![B][C]") ]])
			fnCutscene([[ Append("Karina: Would you be willing to give me an interview?[P] You could advance the cause of science so much![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] We'll consider it, but this mission is of crucial importance.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Offended] The entire hive is counting on me.[P] I can't let them down.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Confused] ... you are such a goodie-goodie...") ]])
		
		--Saved Claudia:
		else
			fnCutscene([[ Append("Florentina:[E|Surprise] Well the attitude is hard to mistake.[P] Mei, are you still in there?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Of course![P] I -[P] hey![P] Who are you calling ugly?[B][C]") ]])
			fnCutscene([[ Append("Claudia: It is as I predicted.[P] Her path is fixed by the divine.[P] The bees cannot alter it.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] Yeah, these two won't shut up about you, Mei.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Happy] Nice to see you again, Claudia![B][C]") ]])
			fnCutscene([[ Append("Claudia: As it is to see you, Mei.[P] This is Sister Karina.[P] She had been aiding my studies until that unfortunate business in the manor.[B][C]") ]])
			fnCutscene([[ Append("Karina: Mei![P] I've been studying your hive![P] Could you maybe give me an interview?[P] I've never met a talking bee before![B][C]") ]])
			fnCutscene([[ Append("Karina: You could tell me all about what it's like being a bee![P] This will help the cause of science so much![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] I'm sorry, but the hive has given me a crucial mission.[P] I must understand my runestone and why it allows me to remember myself.[B][C]") ]])
			fnCutscene([[ Append("Claudia: Not by coincidence that the bees charge you with the task the divine has.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Confused] They've been talking like this since I got here.[P] Cripes.[B][C]") ]])
			fnCutscene([[ Append("Claudia: All is known by the divine.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Confused] Shut up?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] If I have some spare time, sure.[P] Florentina, shall we be off?[B][C]") ]])
			fnCutscene([[ Append("Karina: But the interview...[B][C]") ]])
			fnCutscene([[ Append("Claudia: She has done much for the Doves already, Karina.[P] Trust her judgement.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Confused] *I am so glad to get out of here...*[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Offended] *She was like this the whole time when I rescued her, too!*") ]])
		end
	
	end
	fnCutsceneBlocker()

	--Florentina walks onto Mei.
	Cutscene_CreateEvent("Move Florentina To Mei", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (15.25 * gciSizePerTile), (17.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
	fnCutsceneWait(5)
	fnCutsceneBlocker()

	--Fold the party positions up.
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
	
	-- |[System]|
	--These lines add Florentina to the party. They execute as soon as the scene starts, but take effect afterwards.
    fnAddPartyMember("Florentina")

-- |[Florentina Is Present, Has Seen TF]|
--If Florentina was present before the transformation, the cutscene focuses slightly more on her. In this case she knows Mei can transform at will.
elseif(iIsFlorentinaPresent == 1.0 and iFlorentinaKnowsAboutRune == 1.0) then

	--This cutscene counts as meeting Karina.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iHasFoundOutlandAcolyte", "N", 1.0)

	--Dialogue setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Florentina", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Karina", "Neutral") ]])

	--Talking.
	fnCutscene([[ Append("Florentina:[E|Neutral] She'll be coming this way.[P] Look for one with black hair and a little runestone.[B][C]") ]])
	fnCutscene([[ Append("Lady: You mean like that one?") ]])
	fnCutsceneBlocker()
	
	--Florentina and Karina look at Mei.
	fnCutsceneWait(30)
	fnCutsceneBlocker()
	Cutscene_CreateEvent("Face Florentina North", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", 0, -1)
	DL_PopActiveObject()
	Cutscene_CreateEvent("Face Karina North", "Actor")
		ActorEvent_SetProperty("Subject Name", "Karina")
		ActorEvent_SetProperty("Face", 0, -1)
	DL_PopActiveObject()
	fnCutsceneWait(120)
	fnCutsceneBlocker()
	
	--They face each other again.
	Cutscene_CreateEvent("Face Florentina North", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", -1, 0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("Face Karina North", "Actor")
		ActorEvent_SetProperty("Subject Name", "Karina")
		ActorEvent_SetProperty("Face", 1, 0)
	DL_PopActiveObject()
	fnCutsceneWait(15)
	fnCutsceneBlocker()

	--Dialogue setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Florentina", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Karina", "Neutral") ]])

	--Talking.
	fnCutscene([[ Append("Florentina:[E|Happy] Nope, that one's way too ugly.[P] Couldn't be her.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] Glad to see you're all right, Florentina.") ]])
	fnCutsceneBlocker()
	
	--Florentina and Karina look at Mei again.
	fnCutsceneBlocker()
	Cutscene_CreateEvent("Face Florentina North", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", 0, -1)
	DL_PopActiveObject()
	Cutscene_CreateEvent("Face Karina North", "Actor")
		ActorEvent_SetProperty("Subject Name", "Karina")
		ActorEvent_SetProperty("Face", 0, -1)
	DL_PopActiveObject()
	fnCutsceneWait(45)
	fnCutsceneBlocker()

	--Dialogue setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Florentina", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Karina", "Neutral") ]])

	--If Mei has not previously met Karina:
	if(iHasFoundOutlandAcolyte == 0.0) then
		fnCutscene([[ Append("Florentina:[E|Neutral] I take it your runestone had something to do with this?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Yes.[P] It flashed white, and I could remember myself.[B][C]") ]])
		fnCutscene([[ Append("Lady: Incredible![P] Florentina was telling me you'd retain your identity, but I didn't believe it.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] You...[P] you've been watching us.[P] I mean -[P] the hive.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Mei, meet Karina.[P] She's been assigned to research the bees by Sister Claudia.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Claudia?[P] You're one of her followers?[P] Is she here?[B][C]") ]])
		fnCutscene([[ Append("Karina: I'm afraid not, she left with most of the convent to research the partirhumans in Quantir, over the hills.[B][C]") ]])
		fnCutscene([[ Append("Karina: She was supposed to have returned by now to collect my notes, but something must have delayed them.[B][C]") ]])
		fnCutscene([[ Append("Karina: Say, would you be opposed to giving me an interview?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I'm on a very important mission...[B][C]") ]])
		fnCutscene([[ Append("Karina: Please?[P] You must know that you're the only bee that can speak with humans.[P] Your insights would be unprecedented...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] But I'm not a very special bee.[P] Really, I'm just like all the others.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] I want to be like all the others...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] Snap out of it, kid.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I must complete my mission.[B][C]") ]])
		fnCutscene([[ Append("Karina: What mission?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Claudia will know why my runestone does this.[P] We must find her.[P] That is my mission.[B][C]") ]])
		fnCutscene([[ Append("Karina: If you change your mind...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] I'm sorry, but you humans can offer the hive nothing.[B][C]") ]])
		fnCutscene([[ Append("Karina: There's got to be something![P] We can work something out![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Hm, the hive will consider it.[P] Florentina, shall we?") ]])

		--Clear the flag on Next Move.
		WD_SetProperty("Clear Topic Read", "NextMove")
	
	--If Mei has previously met Karina:
	else
		fnCutscene([[ Append("Florentina:[E|Happy] I take it your runestone had something to do with this?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Yes.[P] It flashed white, and I could remember myself.[B][C]") ]])
		fnCutscene([[ Append("Karina: Incredible![P] Florentina was telling me you'd retain your identity, but I didn't believe it.[B][C]") ]])
		fnCutscene([[ Append("Karina: Mei, Mei![P] Do you remember me?[P] Have you lost any other memories?[B][C]") ]])
		fnCutscene([[ Append("Karina: How do you bees communicate?[P] Can the other bees speak?[P] Oh -[P] I have so many questions![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Er, in order?[P] Yes, no, with these antennae, and...[P] sort of, but it's not easy.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] ...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Laugh] Hee hee![P] The other drones said you ask them the same questions![B][C]") ]])
		fnCutscene([[ Append("Karina: From a safe distance, of course.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] The runestone has something to do with it.[P] I don't know what, but it does.[B][C]") ]])
		fnCutscene([[ Append("Karina: So if you got rid of it, would you become like any other bee?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] I -[P] I don't know how I know, but no.[P] I'm sure it wouldn't.[P] I can feel a connection to it...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] So as I had been saying before, if Claudia - [B][C]") ]])
		fnCutscene([[ Append("Karina: We'd love to pay your fee for a chance to study the rune![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] Fee!?[P] Florentina, we're trying to find me a way home![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] The way I see it, once we send you on your way I get to keep the rune.[P] Fair trade, right?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] You won't be needing it back on Earth, will you?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] Who's to say I want to go back to Earth?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] ...[P] you...[P] said it...[P] right?[P] You don't want to go home?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] That was before I became a bee![P] The hive is my home now.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] Fine, whatever.[P] The job is still to find Claudia, so let's do that okay?[B][C]") ]])
		fnCutscene([[ Append("Karina: Would you be willing to give me an interview?[P] You could advance the cause of science![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] My hive will consider it...") ]])
		
	end
	fnCutsceneBlocker()

	--Florentina walks onto Mei.
	Cutscene_CreateEvent("Move Florentina To Mei", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", (15.25 * gciSizePerTile), (17.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
	fnCutsceneWait(5)
	fnCutsceneBlocker()

	--Fold the party positions up.
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
	
	-- |[System]|
	--These lines add Florentina to the party. They execute as soon as the scene starts, but take effect afterwards.
	fnAddPartyMember("Florentina")
end
