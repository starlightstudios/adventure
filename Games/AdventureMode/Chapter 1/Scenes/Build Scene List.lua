-- |[ ================================= Chapter 1 Scene Listing ================================ ]|
--Builds a list of cutscenes the player can make use of through the debug menu. Cutscenes in the debug menu are
-- used at the player's own risk, they may produce unusable situations that can only be remedied with the debug menu.
-- It's an addiction, I tell you.
--Note that not all cutscenes are necessarily on the debug list. There may also be cutscenes stored
-- in map folders that are not intended to be executed via the debug menu.

-- |[Setup]|
--Scene Listing variable
local sPrefix = ""
local saSceneList = {}

-- |[Adder Function]|
local fnAddScene = function(sPath)
    local i = #saSceneList + 1
    saSceneList[i] = sPath
end

-- |[ ====================================== Debug Scenes ====================================== ]|
sPrefix = "Chapter 1/Scenes/000 Debug/"
fnAddScene(sPrefix .. "A Quickadd Florentina")
fnAddScene(sPrefix .. "A Quickremove Florentina")
fnAddScene(sPrefix .. "A Quickset Ch1Items")
fnAddScene(sPrefix .. "A Quickset Gems")
fnAddScene(sPrefix .. "A Quickset AdinaControl")
fnAddScene(sPrefix .. "A Quickset SavedClaudia")
fnAddScene(sPrefix .. "A Quickset SavedCassandra")
fnAddScene(sPrefix .. "A Quickset Rubber")
fnAddScene(sPrefix .. "A Quickset Rubber Swamp")
fnAddScene(sPrefix .. "A Quickset Done Mycela")
fnAddScene(sPrefix .. "B Unlock All Skillbooks")

-- |[ ================================== Save Point TF Scenes ================================== ]|
sPrefix = "Chapter 1/Scenes/100 Transform/"
fnAddScene(sPrefix .. "Transform_MeiToAlraune")
fnAddScene(sPrefix .. "Transform_MeiToBee")
fnAddScene(sPrefix .. "Transform_MeiToGhost")
fnAddScene(sPrefix .. "Transform_MeiToGravemarker")
fnAddScene(sPrefix .. "Transform_MeiToHuman")
fnAddScene(sPrefix .. "Transform_MeiToSlime")
fnAddScene(sPrefix .. "Transform_MeiToWerecat")
fnAddScene(sPrefix .. "Transform_MeiToWisphag")

-- |[ ====================================== Defeat Scenes ===================================== ]|
sPrefix = "Chapter 1/Scenes/200 Defeat/"
fnAddScene(sPrefix .. "Defeat_BackToSave")
fnAddScene(sPrefix .. "Defeat_Alraune")
fnAddScene(sPrefix .. "Defeat_Bee")
fnAddScene(sPrefix .. "Defeat_BeeAsAlraune")
fnAddScene(sPrefix .. "Defeat_Cultists")
fnAddScene(sPrefix .. "Defeat_Gravemarker")
fnAddScene(sPrefix .. "Defeat_Ghost")
fnAddScene(sPrefix .. "Defeat_Slime")
fnAddScene(sPrefix .. "Defeat_Mushraune")
fnAddScene(sPrefix .. "Defeat_Werecat")
fnAddScene(sPrefix .. "Defeat_Wisphag")
fnAddScene(sPrefix .. "Defeat_Zombee")

-- |[ ========================================= Upload ========================================= ]|
--Upload these to the debug menu.
local i = 1
ADebug_SetProperty("Cutscenes Total", #saSceneList)
while(saSceneList[i] ~= nil) do
	ADebug_SetProperty("Cutscene Path", i-1, saSceneList[i])
	i = i + 1
end
