-- |[ ================================= Boot Script Variables ================================== ]|
--Listing of variables used by scripts in Chapter 1. These variables are retained for later chapters.
-- Note: When loading the game, these will get overwritten if newer copies are found.
DL_AddPath("Root/Variables/Chapter1/")

--Zeroth save point is the one right across from the start.
AL_SetProperty("Last Save Point", "TrapBasementB")

-- |[System]|
--The party leader's voice is Mei's at all times.
WD_SetProperty("Set Leader Voice", "Mei")

--Special scene variables.
DL_AddPath("Root/Variables/Chapter1/Scenes/")
VM_SetVar("Root/Variables/Chapter1/Scenes/iShowControls", "N", 1.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasNoMap", "N", 1.0)

-- |[ ==================================== Combat Variables ==================================== ]|
--Redirect the KO counter.
DL_AddPath("Root/Variables/Chapter1/KOTracker/")
VM_SetVar("Root/Variables/Global/Combat/sKOTrackerPath", "S", "Root/Variables/Chapter1/KOTracker/")

-- |[ =================================== Costume Variables ==================================== ]|
--Mei, Unlocked
DL_AddPath("Root/Variables/Costumes/Mei/")
VM_SetVar("Root/Variables/Costumes/Mei/iQueenBee", "N", 0.0)
VM_SetVar("Root/Variables/Costumes/Mei/iRubberQueen", "N", 0.0)

--Mei, Wearing
VM_SetVar("Root/Variables/Costumes/Mei/sCostumeHuman",       "S", "Normal")
VM_SetVar("Root/Variables/Costumes/Mei/sCostumeAlraune",     "S", "Normal")
VM_SetVar("Root/Variables/Costumes/Mei/sCostumeBee",         "S", "Normal")
VM_SetVar("Root/Variables/Costumes/Mei/sCostumeGhost",       "S", "Normal")
VM_SetVar("Root/Variables/Costumes/Mei/sCostumeGravemarker", "S", "Normal")
VM_SetVar("Root/Variables/Costumes/Mei/sCostumeSlime",       "S", "Normal")
VM_SetVar("Root/Variables/Costumes/Mei/sCostumeRubber",      "S", "Normal")
VM_SetVar("Root/Variables/Costumes/Mei/sCostumeMannequin",   "S", "Normal")
VM_SetVar("Root/Variables/Costumes/Mei/sCostumeWerecat",     "S", "Normal")
VM_SetVar("Root/Variables/Costumes/Mei/sCostumeWisphag",     "S", "Normal")
VM_SetVar("Root/Variables/Costumes/Mei/sCostumeZombee",      "S", "Normal")

--Florentina, Wearing
DL_AddPath("Root/Variables/Costumes/Florentina/")
VM_SetVar("Root/Variables/Costumes/Florentina/sCostumeAgarist",        "S", "Normal")
VM_SetVar("Root/Variables/Costumes/Florentina/sCostumeLurker",         "S", "Normal")
VM_SetVar("Root/Variables/Costumes/Florentina/sCostumeMerchant",       "S", "Normal")
VM_SetVar("Root/Variables/Costumes/Florentina/sCostumeMediator",       "S", "Normal")
VM_SetVar("Root/Variables/Costumes/Florentina/sCostumeTreasureHunter", "S", "Normal")

-- |[ ================================== Main Quest Variables ================================== ]|
--Misc Variables
VM_SetVar("Root/Variables/Chapter1/Counts/sMeiSeenPartirhuman", "S", "Nothing")
VM_SetVar("Root/Variables/Chapter1/Counts/iCombatVictories", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Counts/iVictoriesWhenFlorentinaJoined", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Counts/iFlorentinaSaidUpgrade", "N", 0.0)
if(gbBypassIntro) then
	VM_SetVar("Root/Variables/Chapter1/Counts/iCombatVictories", "N", 1)
end

-- |[Rest Reset]|
--These variables reset back to 0 whenever the player rests.
DL_AddPath("Root/Variables/Chapter1/RestReset/")
VM_SetVar("Root/Variables/Chapter1/RestReset/iTestVariable", "N", 0.0)

-- |[Campfire Listing]|
--These store which campfires the player has accessed. The player can warp between them at their discretion.
DL_AddPath("Root/Variables/Chapter1/Campfires/")
VM_SetVar("Root/Variables/Chapter1/Campfires/iTrapBasementB", "N", 1.0) --Always available.
VM_SetVar("Root/Variables/Chapter1/Campfires/iTrapBasementG", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Campfires/iEvermoonW", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Campfires/iEvermoonS", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Campfires/iEvermoonE", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Campfires/iEvermoonCassandraA", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Campfires/iPlainsC", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Campfires/iBeehiveBasementA", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Campfires/iSpookyExterior", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Campfires/iTrapDungeonA", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Campfires/iEvermoonSEA", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Campfires/iStarfieldSwampF", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Campfires/iStarfieldSwampG", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Campfires/iStForasA", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Campfires/iWildsTowerA", "N", 0.0)

-- |[Special Cutscene Variables]|
--This indicates that a cutscene is being "relived". The player may relive cutscenes from the save menu.
VM_SetVar("Root/Variables/Chapter1/Scenes/sOriginalForm", "S", "Human")
VM_SetVar("Root/Variables/Chapter1/Scenes/iIsRelivingScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/sLastRelivedScene", "S", "None")

-- |[General Cutscene Variables]|
DL_AddPath("Root/Variables/Chapter1/Scenes/")
VM_SetVar("Root/Variables/Chapter1/Scenes/iPartyKOCount", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenAlrauneDefeatByBeeScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iTalkedWerecatVendor", "N", 0.0)

--Dimensional Trap Main Floor/Basement
VM_SetVar("Root/Variables/Chapter1/Scenes/iDoorOpenedA", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iDoorOpenedB", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iDoorOpenedC", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iReplacedLadder", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenCultistScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenCultistMeetingScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iEnteredSecretPassage", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iPlatinaBook", "N", 0.0)

--Underground Lake
VM_SetVar("Root/Variables/Chapter1/Scenes/iGotHacksaw", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iMetClaudia", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iMetClaudiaWithoutHacksaw", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iSavedClaudia", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsMeiSavedClaudia", "N", 0.0)

--Upper Dimensional Trap
VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiKnowsRilmani", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iExaminedMirror", "N", 0.0)

--Trannadar Trading Post Scenes
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasMetFriendly", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasReadNadiaSign", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasAnnoyedVendor", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarSecondScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarThirdScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/sTrannadarFirstSceneForm", "S", "Human")
VM_SetVar("Root/Variables/Chapter1/Scenes/sTrannadarSecondSceneForm", "S", "Human")

--Evermoon
VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiTriedFruit", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iMetSlimeGuide", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iKnowsSlimesTalk", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iGotSlimedFromDrink", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iShowedDoorPry", "N", 0.0)

--Cassandra Sequence
VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraDefeatHandler", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iEncounteredCatCampsite", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraTooLate", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraWayTooLate", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraEncounters", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/sCassandraLocation", "S", "None")
VM_SetVar("Root/Variables/Chapter1/Scenes/iSavedCassandra", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iTurnedCassandra", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iWonFightWithCassandra", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraNotHereCC", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraNotHereCE", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraNotHereCNW", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraNotHereCNE", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraSpokenTo", "N", 0.0)

--Salt Flats
VM_SetVar("Root/Variables/Chapter1/Scenes/iAdinaQuestVar", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/sAdinaFirstSceneForm", "S", "Human")
VM_SetVar("Root/Variables/Chapter1/Scenes/iAdinaExtendedMistress", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiLovesAdina", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsMistress", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iReAddFlorentinaAfterLeaving", "N", 0.0)

--Outland Farm and the Bee Hive
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasFoundOutlandAcolyte", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iKarinaSawGravemarker", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iClaudiaSawGravemarker", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenOutlandBeeScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiHasSubmittedGravemarker", "N", 0.0)

--Scenes relating to Aquillia
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasMetAquillia", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasFlorentinaMetAquillia", "N", 0.0)

--Scenes related to the Beehive Basement Dungeon
VM_SetVar("Root/Variables/Chapter1/Scenes/iZombeeQuestState", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenZombeeScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenBeesIgnoreScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenBeePanicScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iConvertedVictimBee", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iDoorFromNorth", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iOpenedOddDoor", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iSeenFlorentinaWarning", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iSavedBeehive", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iSpecialZombeeFlag", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iMetXanna", "N", 0.0)

--Scenes related to the Trap Dungeon
VM_SetVar("Root/Variables/Chapter1/Scenes/iInformedOfDungeon", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iSawTrapWest", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaUnlockedTrapWest", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiUnlockedVineDoor", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iSeenAlrauneBattleIntroScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iMetSpecialAcolyte", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiHatesPuzzles", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iOverlayState", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iOverlayPulseState", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCompletedTrapDungeon", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iPostDungeonScene", "N", 0.0)

--Quantir Mansion Variables
VM_SetVar("Root/Variables/Chapter1/Scenes/iWardenQuestState", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiReadClaudiasJournal", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iDisturbedCoffin", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iSawDutiesScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iOpenedSewerDoor", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiReadLastJournal", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iEastStatueCorrect", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCentralStatueCorrect", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iWesternStatueCorrect", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iIncorrectGuesses", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iSpokeToCountess", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCompletedQuantirMansion", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iNEHallStuckDoorN", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iNEHallStuckDoorS", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iReadAlraunes", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iOpenedStatueA", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iOpenedStatueB", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iOpenedStatueC", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iOpenedStatueD", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iDeployedRope", "N", 0.0)

--Nix Nedar Variables
VM_SetVar("Root/Variables/Chapter1/Scenes/iEnteredNixNedar", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iMetSeptima", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iMetMaram", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iTalkedSeptima", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iSawCorgis", "N", 0.0)

--Slimeville
VM_SetVar("Root/Variables/Chapter1/Scenes/iSawSlimevilleIntro", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaSawSmartSlimes", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iMetCrowbarChan", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iGotSlimeDancersDress", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iMetMeryl", "N", 0.0)

--Alicia and Ginny
VM_SetVar("Root/Variables/Chapter1/Scenes/iMetAlicia", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iMetGinny", "N", 0.0)

--Nervous River Party / Arbonne / Rubber Wilds
VM_SetVar("Root/Variables/Chapter1/Scenes/iRubberQuestState", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iSawRiverParty", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iSawMediatorFight", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iSawYouFirst", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iOpenedTowerLatch", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iPolarisGavePhial", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iFilledPhial", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iReturnedPhial", "N", 0.0)

--Colors related to the Trap Dungeon
gfaTrapDungeonMixers = {}
gfaTrapDungeonMixers[1] = {0.8, 0.6, 1.0}
gfaTrapDungeonMixers[2] = {0.6, 0.4, 0.7}
gfaTrapDungeonMixers[3] = {0.5, 0.3, 0.6}
gfaTrapDungeonMixers[4] = {0.5, 0.2, 0.5}
gfaTrapDungeonMixers[5] = {0.4, 0.2, 0.4}
gfaTrapDungeonMixers[6] = {0.3, 0.1, 0.4}
gfaTrapDungeonMixers[7] = {0.3, 0.1, 0.4}

--Misc
VM_SetVar("Root/Variables/Chapter1/Scenes/iSplashedPlants", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iTriedToPickSisterSuperiorDoor", "N", 0.0)

--Starfield Swamp
VM_SetVar("Root/Variables/Chapter1/Scenes/iStarfieldCaveScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iStarfieldCoffinScene", "N", 0.0)

--Vicky and the Mannequins
VM_SetVar("Root/Variables/Chapter1/Scenes/Mann|iMetVictoriaInVillage", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/Mann|iMetAquilliaInVillage", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/Mann|iFlorentinaReportedGoods", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/Mann|iOverheardBandits", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/Mann|iSawFort", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/Mann|iOpenedFortGate", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/Mann|iBeganSequence", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/Mann|iSawNightFort", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/Mann|iDefeatedLeader", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/Mann|iHeardDog", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/Mann|iPolarisRunIn", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/Mann|iCompletedSequence", "N", 0.0)

-- |[Starfield Mausoleum]|
VM_SetVar("Root/Variables/Chapter1/Scenes/StarMaus|iSawTomb", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/StarMaus|iActivatedBridge", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/StarMaus|iSawGravemarker", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/StarMaus|iSawTestamentA", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/StarMaus|iCoffinState", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/StarMaus|iHitShortcutSwitch", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/StarMaus|iOpenedMajorDoor", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/StarMaus|iDescentShortcut", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/StarMaus|iSawNostalgicRealm", "N", 0.0)

-- |[Opening Cutscene]|
VM_SetVar("Root/Variables/Chapter1/Scenes/Intro|iShowIntroScene", "N", 1.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/Intro|iHearPlea", "N", 1.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/Intro|iFirstTalkToPrisoner", "N", 1.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/Intro|iFoughtCultist", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/Intro|iSeePrisonerEscape", "N", 1.0)
if(gbBypassIntro == true) then
    
    --Variables.
	VM_SetVar("Root/Variables/Chapter1/Scenes/Intro|iShowIntroScene", "N", 0.0)
	VM_SetVar("Root/Variables/Chapter1/Scenes/Intro|iHearPlea", "N", 0.0)
	VM_SetVar("Root/Variables/Chapter1/Scenes/Intro|iFirstTalkToPrisoner", "N", 0.0)
	VM_SetVar("Root/Variables/Chapter1/Scenes/Intro|iFoughtCultist", "N", 1.0)
	VM_SetVar("Root/Variables/Chapter1/Scenes/Intro|iSeePrisonerEscape", "N", 0.0)
	
	--Set the Doctor Bag charges to their default values.
    gbAutoSetDoctorBagCurrentValues = true
    LM_ExecuteScript(gsComputeDoctorBagTotalPath)

end

-- |[Tutorial]|
VM_SetVar("Root/Variables/Chapter1/Scenes/Intro|iTutorialChestA", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/Intro|iTutorialChestB", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/Intro|iTutorialBag", "N", 0.0)

-- |[Rubber Minigame]|
VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iSawTowerIntro", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iSawRubberTaunt", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iInitializedMihoEvent", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iSawMihoTrigger", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iHasThralls", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iRubberedRochea", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iRubberedAdina", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iRubberedPolaris", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iRubberVictory", "N", 0.0)

-- |[ ==================================== Paragon Tracking ==================================== ]|
--Tracks kill counters and paragon spawn locations.
local saGroupings = {}
local function fnAddGrouping(psName)
    local i = #saGroupings + 1
    saGroupings[i] = psName
end

--Grouping list.
fnAddGrouping("EvermoonSouth")
fnAddGrouping("EvermoonNorth")
fnAddGrouping("QuantirManse")
fnAddGrouping("RiverWilds")
fnAddGrouping("RubberWilds")
fnAddGrouping("StarfieldSwamp")
fnAddGrouping("StForasLower")
fnAddGrouping("StForasUpper")
fnAddGrouping("TrapBasement")
fnAddGrouping("TrapDungeon")
fnAddGrouping("ZombeeHive")

--Create variables for each grouping.
for i = 1, #saGroupings, 1 do
    DL_AddPath("Root/Variables/Paragons/" .. saGroupings[i] .. "/")
    VM_SetVar("Root/Variables/Paragons/" .. saGroupings[i] .. "/iIsEnabled", "N", 1.0)
    VM_SetVar("Root/Variables/Paragons/" .. saGroupings[i] .. "/iKillCount", "N", 0.0)
    VM_SetVar("Root/Variables/Paragons/" .. saGroupings[i] .. "/iKillsNeeded", "N", gciParagon_KO_Needed_To_Spawn)
    VM_SetVar("Root/Variables/Paragons/" .. saGroupings[i] .. "/iHasDefeatedParagon", "N", 0.0)
end

-- |[ ==================================== Region Variables ==================================== ]|
--Only the region setup is done here. The gsRegionMarkerPath file does the rest.
DL_AddPath("Root/Variables/Regions/Temporary/")
DL_AddPath("Root/Variables/Regions/Permanent/")

-- |[ ====================================== Party Setup ======================================= ]|
-- |[Mei's Stats]|
--System.
DL_AddPath("Root/Variables/Global/Mei/")

--Mei's form.
VM_SetVar("Root/Variables/Global/Mei/sForm", "S", "Human")
VM_SetVar("Root/Variables/Global/Mei/iHasAlrauneForm",     "N", 0.0)
VM_SetVar("Root/Variables/Global/Mei/iHasBeeForm",         "N", 0.0)
VM_SetVar("Root/Variables/Global/Mei/iHasGhostForm",       "N", 0.0)
VM_SetVar("Root/Variables/Global/Mei/iHasGravemarkerForm", "N", 0.0)
VM_SetVar("Root/Variables/Global/Mei/iHasRubberForm",      "N", 0.0)
VM_SetVar("Root/Variables/Global/Mei/iHasMannequinForm",   "N", 0.0)
VM_SetVar("Root/Variables/Global/Mei/iHasSlimeForm",       "N", 0.0)
VM_SetVar("Root/Variables/Global/Mei/iHasWerecatForm",     "N", 0.0)
VM_SetVar("Root/Variables/Global/Mei/iHasWisphagForm",     "N", 0.0)
VM_SetVar("Root/Variables/Global/Mei/iHasZombeeForm",      "N", 0.0)

--Mei's Skillbook Counter
VM_SetVar("Root/Variables/Global/Mei/iSkillbookTotal", "N", 0.0)

-- |[Florentina's Stats]|
--System
DL_AddPath("Root/Variables/Global/Florentina/")

--Form.
VM_SetVar("Root/Variables/Global/Florentina/sForm", "S", "Alraune")

--Florentina's Skillbook Counter
VM_SetVar("Root/Variables/Global/Florentina/iSkillbookTotal", "N", 0.0)

--Florentina's Jobs
VM_SetVar("Root/Variables/Global/Florentina/sCurrentJob", "S", "Merchant")
VM_SetVar("Root/Variables/Global/Florentina/iHasJob_Merchant", "N", 1.0)
VM_SetVar("Root/Variables/Global/Florentina/iHasJob_Mediator", "N", 0.0)
VM_SetVar("Root/Variables/Global/Florentina/iHasJob_TreasureHunter", "N", 0.0)
VM_SetVar("Root/Variables/Global/Florentina/iHasJob_Agarist", "N", 0.0)
VM_SetVar("Root/Variables/Global/Florentina/iHasJob_Lurker", "N", 0.0)

--Other
VM_SetVar("Root/Variables/Global/Florentina/iMeiKnowsPast", "N", 0.0)

--Controls Florentina's vocal callouts. Most forms use the Merchant set.
VM_SetVar("Root/Variables/Global/Florentina/sVoiceJob", "S", "Merchant")

-- |[ ================================= TF Sequence Variables ================================== ]|
-- |[Alraune Transformation Sequence]|
--Flag set if Mei became an Alraune voluntarily.
VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiVolunteeredToAlraune", "N", 0.0)

--If this flag is set, Florentina will spawn as Mei leaves the Alraune Chamber and rejoin the party.
VM_SetVar("Root/Variables/Chapter1/Scenes/iAlrauneMeetFlorentina", "N", 0.0)

-- |[Bee Transformation Sequence]|
VM_SetVar("Root/Variables/Chapter1/Scenes/iUseSpecialBeeOpening", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iIsDuringBeeTransform", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasConsumedHoney", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasWorkedNectar", "N", 0.0)

-- |[Ghost Transformation Sequence]|
--State
VM_SetVar("Root/Variables/Chapter1/Scenes/iIsGhostTF", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iPutMaidOutfitOn", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iToldNatalieTwice", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCanLeaveRoomW", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iLydieLeftParty", "N", 0.0)

--Cleaning Progress
VM_SetVar("Root/Variables/Chapter1/Scenes/iMadeBed", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedBookshelvesW", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedBookshelvesE", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedChairW", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedChairC", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedChairE", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedTableW", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedTableC", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedTableE", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N", 0.0)

--Misc
VM_SetVar("Root/Variables/Chapter1/Scenes/iGhostFromNCPlus", "N", 0.0)

-- |[Slime Transformation Sequence]|
VM_SetVar("Root/Variables/Chapter1/Scenes/iSlimeHasSeenTrip", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iSlimeHasSeenFeelFunny", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iSlimeHasFinishedTF", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iSlimeMeetFlorentina", "N", 0.0)

-- |[Werecat Transformation Sequence]|
DL_AddPath("Root/Variables/Chapter1/ScenesWerecat/")
VM_SetVar("Root/Variables/Chapter1/ScenesWerecat/iTimeOfDay", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/ScenesWerecat/iIsNight", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/ScenesWerecat/iIsMeiWerecat", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/ScenesWerecat/iMeiHasDoneNadia", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/ScenesWerecat/iSpawnNadiaEvermoonW", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/ScenesWerecat/iMetNadiaInWerecatScene", "N", 0.0)

--Replay storage.
VM_SetVar("Root/Variables/Chapter1/ScenesWerecat/iMeiHasDoneNadiaStore", "N", 0.0)

-- |[Wisphag Transformation Sequence]|
VM_SetVar("Root/Variables/Chapter1/ScenesWisphag/iWasVoluntary", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/ScenesWisphag/iRecoveredFlorentina", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/ScenesWisphag/iToldOfWhirlpools", "N", 0.0)

-- |[Mei's Cutscene Variables]|
--Transform to a Human
VM_SetVar("Root/Variables/Global/Mei/Cutscene|Human|iLittleOnes", "N", 0.0) --From Alraune
VM_SetVar("Root/Variables/Global/Mei/Cutscene|Human|iSoLonely", "N", 0.0) --From Alraune
VM_SetVar("Root/Variables/Global/Mei/Cutscene|Human|iQuietTime", "N", 0.0) --From Bee
VM_SetVar("Root/Variables/Global/Mei/Cutscene|Human|iNotSoSquish", "N", 0.0) --From Slime

--Transform to an Alraune
VM_SetVar("Root/Variables/Global/Mei/Cutscene|Alraune|iBackLittleOnes", "N", 0.0) --From Anything

--Transform to a Bee
VM_SetVar("Root/Variables/Global/Mei/Cutscene|Bee|iStillLoyal", "N", 0.0) --From Anything

--Transform to a Slime
VM_SetVar("Root/Variables/Global/Mei/Cutscene|Slime|iSquish", "N", 0.0) --From Anything

--Mei knows about Mush/Mann Florentina
VM_SetVar("Root/Variables/Global/Mei/iMeiKnowsFlorentinaTFs", "N", 0.0)

-- |[ =================================== Dialogue Variables =================================== ]|
-- |[Dialogue: Florentina]|
--Dictates what Florentina does and doesn't know about Mei. Some of these are in the Scenes/ heading.
DL_AddPath("Root/Variables/Chapter1/Florentina/")
VM_SetVar("Root/Variables/Chapter1/Florentina/iKnowsMeiHasAlraune", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaSpecialAlraune", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaLeaveRochea", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaMetAdina", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaToldFungus", "N", 0.0)

--Variables for Florentina's combat-related dialogues.
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenFlorentinaOneWin", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenFlorentinaFiveWin", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenFlorentinaTwentyWin", "N", 0.0)

--This is a scene that only triggers with Florentina present after visiting the salt flats.
VM_SetVar("Root/Variables/Chapter1/Scenes/iNeedsToSeeSaltFlatsScene", "N", 0.0)

-- |[Dialogue: Rochea/Alraunes]|
--Dialogue variables used by Alraunes, *excluding* Nadia and Florentina.
DL_AddPath("Root/Variables/Chapter1/Alraunes/")
VM_SetVar ("Root/Variables/Chapter1/Alraunes/iIsMeiForeigner", "N", 0.0)
VM_SetVar ("Root/Variables/Chapter1/Alraunes/iMetRochea", "N", 0.0)

-- |[Dialogue: Breanne]|
--Variables used by Breanne. Includes her shop stuff.
DL_AddPath("Root/Variables/Chapter1/Breanne")
VM_SetVar ("Root/Variables/Chapter1/Breanne/iTalkedJob", "N", 0.0)
VM_SetVar ("Root/Variables/Chapter1/Breanne/iTalkedParents", "N", 0.0)
VM_SetVar ("Root/Variables/Chapter1/Breanne/iTakenPieJob", "N", 0.0)
VM_SetVar ("Root/Variables/Chapter1/Breanne/iGotWerecatCollar", "N", 0.0)

--Cutscenes
VM_SetVar("Root/Variables/Chapter1/Breanne/iHasMetMei", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Breanne/iMetMeiWithFlorentina", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Breanne/sMeiFirstForm", "S", "Human")
VM_SetVar("Root/Variables/Chapter1/Breanne/iHasSeenMeiNewForm", "N", 0.0)

--Joanie
VM_SetVar("Root/Variables/Chapter1/Breanne/iCanSpawnJoanie", "N", 0.0)

--Stuff involving people around the Pit Stop. These tie into Breanne's Flower quest.
VM_SetVar("Root/Variables/Chapter1/Breanne/iMeiWillJoinSuitors", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Breanne/iMeiKnowsAboutSuitors", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Breanne/iMeiJoinedSuitor", "N", 0.0)

-- |[Dialogue: Hypatia]|
--Hypatia, who runs the shop in Florentina's absence.
DL_AddPath("Root/Variables/Chapter1/Hypatia")
VM_SetVar("Root/Variables/Chapter1/Hypatia/iHasSeenHypatiaDiscountDialogue", "N", 0.0)

-- |[Polaris]|
DL_AddPath("Root/Variables/Chapter1/Polaris")
VM_SetVar("Root/Variables/Chapter1/Polaris/iLockpickQuestState", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Polaris/iMetPolaris", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Polaris/iFlorentinaMetPolaris", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Polaris/sPolarisFirstForm", "S", "None")
VM_SetVar("Root/Variables/Chapter1/Polaris/iNoticedFormChange", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Polaris/iOpenedDoorInSwampC", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Polaris/iRunestoneQuest", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Polaris/iPolarisSuggestStForas", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Polaris/iPolarisRuneQuestState", "N", 0.0)

-- |[Share;Lock and St. Fora's]|
DL_AddPath("Root/Variables/Chapter1/Sharelock")
VM_SetVar("Root/Variables/Chapter1/Sharelock/iSharelockQuestState", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Sharelock/sSharelockFirstForm", "S", "Null")
VM_SetVar("Root/Variables/Chapter1/Sharelock/iSharelockKnowsTransform", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Sharelock/iSawStForas", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Sharelock/iFoundSecretFloor", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Sharelock/iLookingForSecretFloor", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Sharelock/iMetSharelock", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Sharelock/iSawKitchen", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Sharelock/iSawCampsite", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Sharelock/iSawAltar", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Sharelock/iSawGarden", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Sharelock/iSawGraves", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Sharelock/iTalkedToCrops", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Sharelock/iSawSharelockBasement", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Sharelock/iSawMushraunes", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Sharelock/iMetMycela", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Sharelock/iDefeatedMycela", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Sharelock/iUnlockedStForasBack", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Sharelock/iStForaSwitchA", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Sharelock/iStForaSwitchB", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Sharelock/iStForaSwitchC", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Sharelock/iStForaSwitchD", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Sharelock/iStForaLanguageGuide", "N", 0.0)

-- |[Mycela and Mushraune Stuff]|
DL_AddPath("Root/Variables/Chapter1/Mycela")
VM_SetVar("Root/Variables/Chapter1/Mycela/iWarpsSinceMycela", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Mycela/iNadiaToldOfMycela", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Mycela/iTalkedToMycela", "N", 0.0)

-- |[Oak Farm Characters]|
DL_AddPath("Root/Variables/Chapter1/OakFarm")
VM_SetVar("Root/Variables/Chapter1/OakFarm/iMetJean", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/OakFarm/iExtendedBridge", "N", 0.0)

-- |[Vendrilstadt and Drinking Contest]|
DL_AddPath("Root/Variables/Chapter1/Vendrilstadt")
VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iMetMiho", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/sFirstMihoForm", "S", "Human")
VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iMihoKnowsRunestone", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iMetSammie", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iMetNorah", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iStartedDrinking", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iDrinkingPhase", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iSpokeToNadia", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iSpokeToPolaris", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iSpokeToLydie", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iSpokeToCassandra", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iSpokeToSharelock", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iSpokeToAdina", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iSpokeToRochea", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iTakenBooze", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableA", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableB", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableC", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableD", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iJumpMode", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iDontEditPlayerStates", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iPlayerADown", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iPlayerBDown", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iPlayerCDown", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iPlayerDDown", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iPlayerEDown", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iPlayerFDown", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iMihoLostContest", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iMihoUnconscious", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iSpokeToMiho", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iMihoKnowsCellPhone", "N", 0.0)

-- |[Denise]|
DL_AddPath("Root/Variables/Chapter1/Denise")
VM_SetVar("Root/Variables/Chapter1/Denise/iMetDenise", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Denise/iDeniseShow_Health",     "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Denise/iDeniseShow_Power",      "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Denise/iDeniseShow_Initiative", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Denise/iDeniseShow_Accuracy",   "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Denise/iDeniseShow_Evasion",    "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/Denise/iDeniseShow_Skills",     "N", 0.0)

-- |[Kona]|
DL_AddPath("Root/Variables/Chapter1/Kona")
VM_SetVar("Root/Variables/Chapter1/Kona/iMetKona", "N", 0.0)

-- |[World: State Variables]|
DL_AddPath("Root/Variables/Chapter1/WorldState")

--Beehive Inner
VM_SetVar("Root/Variables/Chapter1/WorldState/iBeehiveJunkSearched", "N", 0.0)

--Dimensional Trap Basement
VM_SetVar("Root/Variables/Chapter1/WorldState/iFoundTinctureA", "N", 0.0)

--Evermoon NE. Door is jammed, can be opened from the south.
VM_SetVar("Root/Variables/Chapter1/WorldState/iEvermoonNEDoorOpen", "N", 0.0)
VM_SetVar("Root/Variables/Chapter1/WorldState/iEvermoonNEDoorFromSouth", "N", 0.0)

-- |[Variables For Which There Is No Excuse]|
--I am so sorry.
DL_AddPath("Root/Variables/Global/Goat")
VM_SetVar("Root/Variables/Global/Goat/iGoatBotherState", "N", 0.0)
VM_SetVar("Root/Variables/Global/Goat/iGoatBotherCount", "N", 0.0)
VM_SetVar("Root/Variables/Global/Goat/iGoatBotherCountTotal", "N", 0.0)

-- |[ ===== RedRobe Minibosses ===== ]|
--These track which redrobe minibosses have been defeated, by room.
DL_AddPath("Root/Variables/Minibosses/Chapter1/")

-- |[ ===================================== Map Variables ====================================== ]|
-- |[Trannadar Central]|
--Constants.
gciTrannadarMapsTotal = 37

--Create library variables for each layer on the map.
DL_AddPath("Root/Variables/Chapter1/TrannadarMap/")
for i = 0, gciTrannadarMapsTotal, 1 do
    local sNumber = string.format("%02i", i)
    VM_SetVar("Root/Variables/Chapter1/TrannadarMap/iRoom" .. sNumber, "N", 0.0)
end

-- |[Trannadar West]|
--Constants.
gciTrannadarWestMapsTotal = 12

--Create library variables for each layer on the map.
DL_AddPath("Root/Variables/Chapter1/TrannadarMapWest/")
for i = 0, gciTrannadarWestMapsTotal, 1 do
    local sNumber = string.format("%02i", i)
    VM_SetVar("Root/Variables/Chapter1/TrannadarMapWest/iRoom" .. sNumber, "N", 0.0)
end

-- |[Mausoleum]|
--Constants
gciMausoleumMapsTotal = 11

--Create library variables for each layer on the map.
DL_AddPath("Root/Variables/Chapter1/Mausoleum/")
for i = 0, gciMausoleumMapsTotal, 1 do
    local sNumber = string.format("%02i", i)
    VM_SetVar("Root/Variables/Chapter1/Mausoleum/iRoom" .. sNumber, "N", 0.0)
end

-- |[ ==================================== Global Variables ==================================== ]|
-- |[Global Variables]|
--Variables.
gsPartyLeaderName = "Mei"
giPartyLeaderID = 0

--Following Characters.
giFollowersTotal = 0
gsaFollowerNames = {}
giaFollowerIDs = {0}

--Florentina's equipment variable.
gbHasFlorentinasEquipment = false

-- |[Map Setup]|
LM_ExecuteScript(gsRoot .. "Maps/Z Map Lookups/Chapter 1 Lookups.lua")
