-- |[ ================================= Chapter Initialization ================================= ]|
--Called when the chapter is initialized. This can be either because it was loaded in the chapter select
-- or because the game was loaded with the chapter already active.
--If loading the game, the initial values created her are overwritten afterwards. These thus serve
-- as initial values. If a variable exists here but does not exist in the save file, the default
-- value is used.
local sBasePath = fnResolvePath()
gbInitializeDebug = false
Debug_PushPrint(gbInitializeDebug, "Beginning Chapter 5 Initialization.\n")

-- |[ ========================================= Loading ======================================== ]|
if(gbLoadedChapter5Assets ~= true and gbShouldLoadChapter5Assets == true) then

    --Set load handlers to chapter 5.
	if(gsMandateTitle ~= "Witch Hunter Izana") then
		LI_BootChapter5()
	end
    
    --Flag.
    Debug_Print(" Loading assets for chapter 5.\n")
    gbLoadedChapter5Assets = true
    
    --Load Sequence
    fnIssueLoadReset("AdventureModeCH5")
    LM_ExecuteScript(gsRoot .. "Chapter 5/Loading/100 Sprites.lua")
    LM_ExecuteScript(gsRoot .. "Chapter 5/Loading/101 Portraits.lua")
    LM_ExecuteScript(gsRoot .. "Chapter 5/Loading/102 Actors.lua")
    LM_ExecuteScript(gsRoot .. "Chapter 5/Loading/103 Scenes.lua")
    LM_ExecuteScript(gsRoot .. "Chapter 5/Loading/104 Overlays.lua")
    LM_ExecuteScript(gsRoot .. "Chapter 5/Loading/105 Audio.lua")
    SLF_Close()
    fnCompleteLoadSequence()  
    Debug_Print(" Loading assets done.\n")  

end

-- |[ ======================================= Path Setup ======================================= ]|
--Set standard paths for the rest of the chapter.
gsStandardGameOver     = gsRoot .. "Chapter 5/Scenes/300 Standards/Defeat/Scene_Begin.lua"
gsStandardRetreat      = gsRoot .. "Chapter 5/Scenes/300 Standards/Retreat/Scene_Begin.lua"
gsStandardRevert       = gsRoot .. "Chapter 5/Scenes/300 Standards/Revert/Scene_Begin.lua"
gsStandardReliveBegin  = gsRoot .. "Chapter 5/Scenes/999 Relive Handler/Scene_Begin.lua"
gsStandardReliveEnd    = gsRoot .. "Chapter 5/Scenes/999 Relive Handler/Scene_End.lua"

--Menu Paths
AM_SetPropertyJournal("Bestiary Resolve Script", gsRoot .. "Chapter 5/200 Bestiary Handler.lua")
AM_SetPropertyJournal("Profile Resolve Script",  gsRoot .. "Chapter 5/210 Profile Handler.lua")
AM_SetPropertyJournal("Quest Resolve Script",    gsRoot .. "Chapter 5/220 Quest Handler.lua")
AM_SetPropertyJournal("Location Resolve Script", gsRoot .. "Chapter 5/230 Location Handler.lua")
AM_SetPropertyJournal("Paragon Resolve Script",  gsRoot .. "Chapter 5/240 Paragon Handler.lua")
AM_SetPropertyJournal("Combat Resolve Script",   gsRoot .. "Chapter 0/240 Combat Handler.lua")

--Autosave Icon
AL_SetProperty("Autosave Icon Path", "Root/Images/AdventureUI/Symbols22/RuneChristine")

--Journal Icons
if(AM_GetPropertyJournal("Can Set Images") == true) then
    AM_SetPropertyJournal("Set Achievement Title",       "Metals of Honor")
    AM_SetPropertyJournal("Set Achievement Show String", "[IMG0] Show Metals")
    AM_SetPropertyJournal("Set Achievement Hide String", "[IMG0] Hide Metals")
    AM_SetPropertyJournal("Set Achievement Over Image",  "Root/Images/AdventureUI/Journal/Static Parts Achievements Over Ch5")
end

--UI Icons
AM_SetProperty("Set Form Icon",    "Root/Images/AdventureUI/CampfireMenuIcon/Unavailable")
AM_SetProperty("Set Relive Icon",  "Root/Images/AdventureUI/CampfireMenuIcon/Unavailable")
AM_SetProperty("Set Costume Icon", "Root/Images/AdventureUI/CampfireMenuIcon/Costume")

-- |[ ======================================== Clearing ======================================== ]|
--Because of how the load handler works, there may be lingering equipment in the inventory if the
-- player saved and loaded in Nowhere, as characters are given their default gear at game load.
--Therefore, clear the inventory again.

-- |[Catalyst Handling]|
if(gbDontRecountCatalysts ~= true) then
    
    --Clear.
    AdInv_SetProperty("Clear")

    --Order the inventory to recompute how many catalysts we've picked up. Chapters can be done out
    -- of order or with NC+ so the current counts aren't necessarily zeroes.
    AdInv_SetProperty("Resolve Catalysts By String", "Chapter 5-1")
    
    --Get the counts from the inventory.
    local iHealthCatalyst     = AdInv_GetProperty("Catalyst Count", gciCatalyst_Health)
    local iAttackCatalyst     = AdInv_GetProperty("Catalyst Count", gciCatalyst_Attack)
    local iInitiativeCatalyst = AdInv_GetProperty("Catalyst Count", gciCatalyst_Initiative)
    local iEvadeCatalyst      = AdInv_GetProperty("Catalyst Count", gciCatalyst_Evade)
    local iAccuracyCatalyst   = AdInv_GetProperty("Catalyst Count", gciCatalyst_Accuracy)
    local iSkillCatalyst      = AdInv_GetProperty("Catalyst Count", gciCatalyst_Skill)

    --Set extra slots from skill catalysts.
    local iSlotCount = math.floor(iSkillCatalyst / gciCatalyst_Skill_Needed)
    AdvCombat_SetProperty("Set Skill Catalyst Slots", iSlotCount)

    --Mirror the catalyst counts into the DataLibrary.
    VM_SetVar("Root/Variables/Global/Catalysts/iHealth",     "N", iHealthCatalyst)
    VM_SetVar("Root/Variables/Global/Catalysts/iAttack",     "N", iAttackCatalyst)
    VM_SetVar("Root/Variables/Global/Catalysts/iInitiative", "N", iInitiativeCatalyst)
    VM_SetVar("Root/Variables/Global/Catalysts/iEvade",      "N", iEvadeCatalyst)
    VM_SetVar("Root/Variables/Global/Catalysts/iAccuracy",   "N", iAccuracyCatalyst)
    VM_SetVar("Root/Variables/Global/Catalysts/iSkill",      "N", iSkillCatalyst)
end

-- |[ ================================== Party Initialization ================================== ]|
-- |[ ========== Christine ========== ]|
--Create Christine. Her default job is "Teacher".
if(AdvCombat_GetProperty("Does Party Member Exist", "Christine") == false) then
    Debug_Print(" Creating Christine.\n")
    LM_ExecuteScript(gsRoot .. "Combat/Party/Christine/000 Initialize.lua")

    --Create a Violet Runestone and equip it to Christine. Also give her the default gear.
    Debug_Print(" Creating Christine's equipment.\n")
    LM_ExecuteScript(gsItemListing, "Violet Runestone")
    LM_ExecuteScript(gsItemListing, "Schoolmaster's Suit")

    --Equip these items.
    AdvCombat_SetProperty("Push Party Member", "Christine")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Item A", "Violet Runestone")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Body", "Schoolmaster's Suit")
    DL_PopActiveObject()
end

-- |[ ========== Tiffany ========== ]|
--Also known as 55.
if(AdvCombat_GetProperty("Does Party Member Exist", "Tiffany") == false) then
    Debug_Print(" Creating Tiffany.\n")
    LM_ExecuteScript(gsRoot .. "Combat/Party/Tiffany/000 Initialize.lua")

    --Default gear, what she first joins the party with.
    Debug_Print(" Creating Tiffany's equipment.\n")
    LM_ExecuteScript(gsItemListing, "Mk IV Pulse Diffractor")
    LM_ExecuteScript(gsItemListing, "Command Unit Garb")

    --Equip these items.
    AdvCombat_SetProperty("Push Party Member", "Tiffany")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Weapon", "Mk IV Pulse Diffractor")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Body", "Command Unit Garb")
    DL_PopActiveObject()
end

--Run Tiffany's costume routines.
LM_ExecuteScript(gsRoot .. "CostumeHandlers/Tiffany/ZZ Common.lua")

-- |[ ========== JX-101 ========== ]|
--Leader of the steam droids in Sprocket City. Temporarily joins the party.
if(AdvCombat_GetProperty("Does Party Member Exist", "JX-101") == false) then
    Debug_Print(" Creating JX-101.\n")
    LM_ExecuteScript(gsRoot .. "Combat/Party/JX-101/000 Initialize.lua")

    --Default gear, what she first joins the party with. Cannot be changed out.
    Debug_Print(" Creating JX-101's equipment.\n")
    LM_ExecuteScript(gsItemListing, "MkV Pulse Pistol")
    LM_ExecuteScript(gsItemListing, "JX-101's Cuirass")

    --Equip these items.
    AdvCombat_SetProperty("Push Party Member", "JX-101")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Weapon", "MkV Pulse Pistol")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Body", "JX-101's Cuirass")
    DL_PopActiveObject()
end

-- |[ ========== SX-399 ========== ]|
--Steam Droid, becomes a Steam Lord and joins the party in the biolabs.
if(AdvCombat_GetProperty("Does Party Member Exist", "SX-399") == false) then
    Debug_Print(" Creating SX-399.\n")
    LM_ExecuteScript(gsRoot .. "Combat/Party/SX-399/000 Initialize.lua")

    --Default gear, what she first joins the party with.
    Debug_Print(" Creating SX-399's equipment.\n")
    LM_ExecuteScript(gsItemListing, "Fission Carbine")
    LM_ExecuteScript(gsItemListing, "Bronze Chestguard")

    --Equip these items.
    AdvCombat_SetProperty("Push Party Member", "SX-399")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Weapon", "Fission Carbine")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Body", "Bronze Chestguard")
    DL_PopActiveObject()
end

--Run SX-399's costume routines.
LM_ExecuteScript(gsRoot .. "CostumeHandlers/SX-399/ZZ Common.lua")

-- |[ ========================================== Flags ========================================= ]|
--Mark Christine as the party leader.
gsPartyLeaderName = "Christine"

--Set the party positions. Christine is the leader, all others are empty.
Debug_Print(" Positioning party slots.\n")
AdvCombat_SetProperty("Clear Party")
AdvCombat_SetProperty("Party Slot", 0, "Christine")

--Setup the XP tables, if they weren't booted already.
Debug_Print(" Booting XP table.\n")
LM_ExecuteScript(gsRoot .. "Combat/Party/XP Table Chapter 5.lua")

-- |[ ================================= Variable Initialization ================================ ]|
--Mark this as the current chapter.
Debug_Print(" Initializing chapter 5 variables.\n")
VM_SetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N", 5.0)

--Create script variables. This is done in its own file.
LM_ExecuteScript(sBasePath .. "001 Variables.lua")

--Create paths referring to enemies.
LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 5/Alias List.lua")

--Combat Paragon script
AdvCombat_SetProperty("Set Paragon Script",      gsRoot .. "Combat/Enemies/Chapter 5/Paragon Handler.lua")
AdvCombat_SetProperty("Set Enemy Auto Script",   gsRoot .. "Combat/Enemies/Chapter 5/Enemy Auto Handler.lua")
AdvCombat_SetProperty("Set Post Combat Handler", gsRoot .. "Combat/Enemies/Chapter 5/Post Combat Handler.lua")

--Combat Volunteer Script
AdvCombat_SetProperty("Set Volunteer Script", gsRoot .. "Chapter 5/100 Volunteer Handler.lua")

-- |[Mines]|
--Call this to initialize the random generator for the Tellurium Mines.
LM_ExecuteScript(gsRoot .. "Chapter 5/Mines/000 Initialize.lua")

-- |[ ===================================== Doctor Bag Zero ==================================== ]|
--Christine gets the doctor bag pretty early, but does not start with it.
Debug_Print(" Setting doctor bag.\n")
VM_SetVar("Root/Variables/CrossChapter/DoctorBag/iDoctorBagChargesMax", "N", 0)
VM_SetVar("Root/Variables/CrossChapter/DoctorBag/iDoctorBagChargesCur", "N", 0)
AdInv_SetProperty("Doctor Bag Charges", 0)
AdInv_SetProperty("Doctor Bag Charges Max", 0)

-- |[ ====================================== Party Restore ===================================== ]|
--Make sure everyone is at full HP. This affects the whole party roster, not just the active party.
Debug_Print(" Restoring party.\n")
AdvCombat_SetProperty("Restore Roster")

-- |[ ====================================== Path Building ===================================== ]|
--Run this script to build cutscenes that can be accessed from the debug menu.
Debug_Print(" Building paths.\n")
LM_ExecuteScript(sBasePath .. "Scenes/Build Scene List.lua")

--Build a set of aliases. These must be used in place of hard paths for in-engine script calls.
LM_ExecuteScript(sBasePath .. "Scenes/Build Alias List.lua")

--Build a list of locations the player can warp to from the debug menu.
LM_ExecuteScript(gsRoot .. "Maps/Build Debug Warp List.lua", "Chapter 5")

--Standard combat paths.
AdvCombat_SetProperty("Standard Retreat Script", gsRoot .. "Chapter 5/Scenes/300 Standards/Retreat/Scene_Begin.lua")
AdvCombat_SetProperty("Standard Defeat Script",  gsRoot .. "Chapter 5/Scenes/300 Standards/Defeat/Scene_Begin.lua")
AdvCombat_SetProperty("Standard Victory Script", gsRoot .. "Chapter 5/Scenes/300 Standards/Victory/Scene_Begin.lua")

-- |[ ===================================== Map Functions ====================================== ]|
--Storage variables. Used by the Load Handler to correctly set the map when loading the game.
gfnLastMapFunction = nil
giLastPlayerX = nil
giLastPlayerY = nil
giLastRemapX = nil
giLastRemapY = nil

--Call creation functions.
LM_ExecuteScript(gsRoot .. "Subroutines/Chapter 5/fnCryogenicsMap.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/Chapter 5/fnCryoLowerMap.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/Chapter 5/fnEquinoxMap.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/Chapter 5/fnLRTEastMap.lua")
LM_ExecuteScript(gsRoot .. "Subroutines/Chapter 5/fnLRTWestMap.lua")

-- |[ ===================================== Music and Sound ==================================== ]|
--Default combat music.
Debug_Print(" Setting combat music.\n")
AdvCombat_SetProperty("Default Combat Music", "BattleThemeChristine", 0.000)

-- |[ ===================================== Field Abilities ==================================== ]|
--Purge field abilities if any exist:
local iSizeOfPath = DL_SizeOfPath("Root/Special/Combat/FieldAbilities/")
if(iSizeOfPath > 0) then
    DL_Purge("Root/Special/Combat/FieldAbilities/", false)
end

--Create field abilities.
Debug_Print(" Setting field abilities.\n")
DL_AddPath("Root/Special/Combat/FieldAbilities/")
LM_ExecuteScript(gsFieldAbilityListing .. "Bend Space.lua",     gciFieldAbility_Create)
LM_ExecuteScript(gsFieldAbilityListing .. "Spot Weld.lua",      gciFieldAbility_Create)
LM_ExecuteScript(gsFieldAbilityListing .. "Ball Lightning.lua", gciFieldAbility_Create)

--Clear off the slots.
AdvCombat_SetProperty("Set Field Ability", 0, "NULL")
AdvCombat_SetProperty("Set Field Ability", 1, "NULL")
AdvCombat_SetProperty("Set Field Ability", 2, "NULL")
AdvCombat_SetProperty("Set Field Ability", 3, "NULL")
AdvCombat_SetProperty("Set Field Ability", 4, "NULL")

-- |[ ======================================== Catalysts ======================================= ]|
--Note: Chapter 5 is split into two halves. The first half and second half use different catalyst counts.
AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Health, 0)
AdInv_SetProperty("Catalyst Local Max", gciCatalyst_Health, 5)

AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Attack, 0)
AdInv_SetProperty("Catalyst Local Max", gciCatalyst_Attack, 0)

AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Initiative, 0)
AdInv_SetProperty("Catalyst Local Max", gciCatalyst_Initiative, 0)

AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Evade, 0)
AdInv_SetProperty("Catalyst Local Max", gciCatalyst_Evade, 0)

AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Accuracy, 0)
AdInv_SetProperty("Catalyst Local Max", gciCatalyst_Accuracy, 0)

AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Skill, 0)
AdInv_SetProperty("Catalyst Local Max", gciCatalyst_Skill, 6)

-- |[ ====================================== Intro Bypass ====================================== ]|
--The player can optionally bypass the intro. If that happens, this code executes.
if(gbBypassIntro == true) then
    
    --Debug.
    Debug_Print(" Setting intro bypass values.\n")

    --Doctor Bag starts at its normal charges. Set flags to upload these to the inventory.
    if(false) then
        gbAutoSetDoctorBagProperties = true
        gbAutoSetDoctorBagCurrentValues = true
        VM_SetVar("Root/Variables/CrossChapter/DoctorBag/iDoctorBagChargesMax", "N", 100)
        VM_SetVar("Root/Variables/CrossChapter/DoctorBag/iDoctorBagChargesCur", "N", 100)
        AdInv_SetProperty("Doctor Bag Charges", 100)
        AdInv_SetProperty("Doctor Bag Charges Max", 100)
        LM_ExecuteScript(gsComputeDoctorBagTotalPath)
    end
    
    --Give Christine her tazer.
    if(false) then
        LM_ExecuteScript(gsItemListing, "Tazer")
        AdvCombat_SetProperty("Push Party Member", "Christine")
            AdvCombatEntity_SetProperty("Equip Item To Slot", "Weapon", "Tazer")
        DL_PopActiveObject()
    end
    
    --Debug: Put other characters in the party for testing.
    --AdvCombat_SetProperty("Party Slot", 1, "Florentina")
    
    --Debug: Give other characters their weaponry.
    --[=[LM_ExecuteScript(gsItemListing, "Hunting Knife")
    LM_ExecuteScript(gsItemListing, "Flowery Tunic")
    LM_ExecuteScript(gsItemListing, "Florentina's Pipe")
    AdvCombat_SetProperty("Push Party Member", "Florentina")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Weapon", "Hunting Knife")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Body", "Flowery Tunic")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Accessory A", "Florentina's Pipe")
    DL_PopActiveObject()]=]
    
    --Change character starting form.
    --LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua")
    
end
Debug_PopPrint("Finished chapter 5 initialization.\n")
