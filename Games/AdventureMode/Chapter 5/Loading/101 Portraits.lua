-- |[ =================================== Chapter 5 Portraits ================================== ]|
--Loads all portraits for chapter 5.
Debug_PushPrint(gbLoadDebug, "Beginning Portrait loader.\n")

-- |[All Portraits List]|
--Create an "Everything" list. All delayed images not in another list get put in this list and loaded
-- as soon as possible, but not during the load sequence.
local sEverythingList = "Chapter 5 Portraits"
fnCreateDelayedLoadList(sEverythingList)

--List used for portraits that load if gbUnloadDialoguePortraitsWhenDialogueCloses is true, and don't when it's false.
local sCh5DialogueList = "Chapter 5 Dialogue"
fnCreateDelayedLoadList(sCh5DialogueList)

-- |[Dialogue Portraits Flag]|
--Setup. Allows suppression of streaming warnings if desired for some reason.
local bShowWarningsForDialoguePortraits = true
local bShowWarningsForCombatPortraits = true

-- |[ ======================================== Christine ======================================= ]|
-- |[Setup]|
--Note: Christine's Human Neutral is already loaded. It still is here to get added to the costume groupings.
Debug_Print("Loading Christine's portraits.\n")
SLF_Open(gsaDatafilePaths.sChristinePath)

--Create a list of all of Christine's portrait sets. When a given form is loaded, all other forms unload.
gsaChristineDialogueList = {}
gsaChristineCombatList   = {}

--Set this flag to suppress warnings if required.
Bitmap_SetNewBitmapNoHandleWarnings(bShowWarningsForDialoguePortraits)

-- |[Dialogue Portraits]|
--Datalibrary Headers
DL_AddPath("Root/Images/Portraits/ChristineDialogue/")
DL_AddPath("Root/Images/Portraits/Combat/")
DL_AddPath("Root/Images/Portraits/Paragon/")

--Lists
local saPattern = {"Neutral", "Happy", "Blush", "Smirk", "Sad", "Scared", "Offended", "Cry", "Laugh", "Angry", "PDU"}
local saForms = {"", "HumanNude", "Darkmatter", "Golem", "Latex", "SteamDroid", "GolemGala", "Raiju", "RaijuClothes", "Eldritch", "Electrosprite", "Doll", "DollSweater", "DollNude", "Secrebot"}

--For each form, place the portraits in the delayed list.
for i = 1, #saForms, 1 do
    
    --Setup.
    local sDelayedLoadList = ""
    
    --If this is the "" list, it's name is 'Human".
    if(saForms[i] == "") then
        sDelayedLoadList = "Christine Costume Human"
    
    --Otherwise, use the form name.
    else
        sDelayedLoadList = "Christine Costume " .. saForms[i]
    end
    
    --Create the load list, and a combat variant.
    fnCreateDelayedLoadList(sDelayedLoadList)
    fnCreateDelayedLoadList(sDelayedLoadList .. " Combat")
    
    --Append it to the list of dialogue loading lists.
    local p = #gsaChristineDialogueList + 1
    gsaChristineDialogueList[p] = sDelayedLoadList
    
    --Append the combat variant to the combat lists.
    p = #gsaChristineCombatList + 1
    gsaChristineCombatList[p] = sDelayedLoadList .. " Combat"
        
    --For each entry in the pattern, perform a delayed-load.
    for p = 1, #saPattern, 1 do
        if(saForms[i] ~= "") then
            fnExtractDelayedBitmapToList(sDelayedLoadList, "Por|Christine" .. saForms[i].. "|" .. saPattern[p], "Root/Images/Portraits/ChristineDialogue/" .. saForms[i] .. "|" .. saPattern[p])
        else
            fnExtractDelayedBitmapToList(sDelayedLoadList, "Por|Christine|" .. saPattern[p], "Root/Images/Portraits/ChristineDialogue/" .. saPattern[p])
        end
    end
end

-- |[Non-Form Lists]|
--Other dialogue costumes:
fnCreateDelayedLoadList("Christine Costume Male")
fnCreateDelayedLoadList("Christine Costume Raibie")
fnCreateDelayedLoadList("Christine Costume SteamDroidSX-399")
fnCreateDelayedLoadList("Christine Costume Doll Sweater")
fnCreateDelayedLoadList("Christine Costume Doll Nude")

--Combat:
fnCreateDelayedLoadList("Christine Costume Male Combat")
fnCreateDelayedLoadList("Christine Costume Raibie Combat")
fnCreateDelayedLoadList("Christine Costume SteamDroidSX-399 Combat")
fnCreateDelayedLoadList("Christine Costume Doll Sweater Combat")
fnCreateDelayedLoadList("Christine Costume Doll Nude Combat")
    
--Append it to the list of dialogue loading lists.
local p = #gsaChristineDialogueList + 1
gsaChristineDialogueList[p+0] = "Christine Costume Male"
gsaChristineDialogueList[p+1] = "Christine Costume Raibie"
gsaChristineDialogueList[p+2] = "Christine Costume SteamDroidSX-399"

--Append the combat variant to the combat lists.
p = #gsaChristineCombatList + 1
gsaChristineCombatList[p+0] = "Christine Costume Male Combat"
gsaChristineCombatList[p+1] = "Christine Costume Raibie Combat"
gsaChristineCombatList[p+2] = "Christine Costume SteamDroidSX-399"

-- |[Miscellaneous Portraits]|
--'Foward' or 'Serious' poses, used when Christine is in a programmed state.
fnExtractDelayedBitmapToList("Christine Costume Golem",    "Por|ChristineGolem|Serious",    "Root/Images/Portraits/ChristineDialogue/Golem|Serious")
fnExtractDelayedBitmapToList("Christine Costume Doll",     "Por|ChristineDoll|Serious",     "Root/Images/Portraits/ChristineDialogue/Doll|Serious")
fnExtractDelayedBitmapToList("Christine Costume Secrebot", "Por|ChristineSecrebot|Serious", "Root/Images/Portraits/ChristineDialogue/Secrebot|Serious")

--Male form, only has one dialogue portrait.
fnExtractDelayedBitmapToList("Christine Costume Male", "Por|Chris|Neutral",  "Root/Images/Portraits/ChristineDialogue/Male|Neutral")

-- |[Combat Portraits]|
--Combat Portraits. Each one is part of a delayed load list associated with a form.
fnExtractDelayedBitmapToList("Christine Costume Darkmatter Combat",    "Party|Christine_Darkmatter",    "Root/Images/Portraits/Combat/Christine_Darkmatter")
fnExtractDelayedBitmapToList("Christine Costume Doll Combat",          "Party|Christine_Doll",          "Root/Images/Portraits/Combat/Christine_Doll")
fnExtractDelayedBitmapToList("Christine Costume Eldritch Combat",      "Party|Christine_Eldritch",      "Root/Images/Portraits/Combat/Christine_Eldritch")
fnExtractDelayedBitmapToList("Christine Costume Electrosprite Combat", "Party|Christine_Electrosprite", "Root/Images/Portraits/Combat/Christine_Electrosprite")
fnExtractDelayedBitmapToList("Christine Costume Golem Combat",         "Party|Christine_Golem",         "Root/Images/Portraits/Combat/Christine_Golem")
fnExtractDelayedBitmapToList("Christine Costume GolemGala Combat",     "Party|Christine_GolemDress",    "Root/Images/Portraits/Combat/Christine_Golem_Dress")
fnExtractDelayedBitmapToList("Christine Costume Golem Combat",         "Party|Christine_Golem_Forward", "Root/Images/Portraits/Combat/Christine_Golem_Forward")
fnExtractDelayedBitmapToList("Christine Costume Human Combat",         "Party|Christine_Human",         "Root/Images/Portraits/Combat/Christine_Human")
fnExtractDelayedBitmapToList("Christine Costume Latex Combat",         "Party|Christine_Latex",         "Root/Images/Portraits/Combat/Christine_Latex")
fnExtractDelayedBitmapToList("Christine Costume Male Combat",          "Party|Christine_Male",          "Root/Images/Portraits/Combat/Christine_Male")
fnExtractDelayedBitmapToList("Christine Costume Raibie Combat",        "Party|Christine_Raibie",        "Root/Images/Portraits/Combat/Christine_Raibie")
fnExtractDelayedBitmapToList("Christine Costume Raiju Combat",         "Party|Christine_Raiju",         "Root/Images/Portraits/Combat/Christine_Raiju")
fnExtractDelayedBitmapToList("Christine Costume RaijuClothes Combat",  "Party|Christine_RaijuClothed",  "Root/Images/Portraits/Combat/Christine_RaijuClothed")
fnExtractDelayedBitmapToList("Christine Costume SteamDroid Combat",    "Party|Christine_SteamDroid",    "Root/Images/Portraits/Combat/Christine_SteamDroid")
fnExtractDelayedBitmapToList("Christine Costume Secrebot Combat",      "Party|Christine_Secrebot",      "Root/Images/Portraits/Combat/Christine_Secrebot")

-- |[Special: SX-399 Portraits]|
--During a period in Sprocket City, Christine disguises herself as SX-399. To prevent conflicts, she has her own set of images.
-- Combat uses her normal steam droid appearance.
fnAppendDelayedBitmapToList("Christine Costume SteamDroidSX-399 Combat", "Party|Christine_SteamDroid", "Root/Images/Portraits/Combat/Christine_SteamDroid")

--The dialogue portraits borrow SX-399's.
SLF_Open(gsaDatafilePaths.sSX399Path)
fnExtractDelayedBitmapToList("Christine Costume SteamDroidSX-399", "Por|SX-399|Steam", "Root/Images/Portraits/ChristineDialogue/SteamDroidSX-399|Neutral")

-- |[Combat Portrait Re-use]|
fnAppendDelayedBitmapToList("Christine Costume Doll Sweater Combat", "Party|Christine_Doll", "Root/Images/Portraits/Combat/Christine_Doll")
fnAppendDelayedBitmapToList("Christine Costume Doll Nude Combat",    "Party|Christine_Doll", "Root/Images/Portraits/Combat/Christine_Doll")

-- |[ =========================================== PDU ========================================== ]|
--PDU. Counts as a character.
Debug_Print("Loading PDU's portraits.\n")
SLF_Open(gsaDatafilePaths.sPDUPath)

--Normal Clothes
DL_AddPath("Root/Images/Portraits/PDUDialogue/")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|PDURobot|Neutral",  "Root/Images/Portraits/PDUDialogue/Neutral")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|PDURobot|Happy",    "Root/Images/Portraits/PDUDialogue/Happy")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|PDURobot|Alarmed",  "Root/Images/Portraits/PDUDialogue/Alarmed")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|PDURobot|Question", "Root/Images/Portraits/PDUDialogue/Question")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|PDURobot|Quiet",    "Root/Images/Portraits/PDUDialogue/Quiet")

-- |[ ======================================== Tiffany ========================================= ]|
-- |[Setup]|
--Tiffany. A doll! She goes by 55 in chapter 5.
Debug_Print("Loading Tiffany's portraits.\n")
SLF_Open(gsaDatafilePaths.sTiffanyPath)

-- |[Costume List Creation]|
--Costumes.
fnCreateDelayedLoadList("Tiffany Costume Normal")
fnCreateDelayedLoadList("Tiffany Costume Sundress")

--Combat version.
fnCreateDelayedLoadList("Tiffany Costume Assault Combat")
fnCreateDelayedLoadList("Tiffany Costume Spearhead Combat")
fnCreateDelayedLoadList("Tiffany Costume Subvert Combat")
fnCreateDelayedLoadList("Tiffany Costume Support Combat")
fnCreateDelayedLoadList("Tiffany Costume Sundress Assault Combat")
fnCreateDelayedLoadList("Tiffany Costume Sundress Spearhead Combat")
fnCreateDelayedLoadList("Tiffany Costume Sundress Subvert Combat")
fnCreateDelayedLoadList("Tiffany Costume Sundress Support Combat")

-- |[Store Lists]|
--Create a list of all of Florentina's costume sets.
gsaTiffanyDialogueList = {}
gsaTiffanyDialogueList[1] = "Tiffany Costume Normal"
gsaTiffanyDialogueList[2] = "Tiffany Costume Sundress"

--Combat version.
gsaTiffanyCombatList = {}
gsaTiffanyCombatList[1] = "Tiffany Costume Assault Combat"
gsaTiffanyCombatList[2] = "Tiffany Costume Spearhead Combat"
gsaTiffanyCombatList[3] = "Tiffany Costume Subvert Combat"
gsaTiffanyCombatList[4] = "Tiffany Costume Support Combat"
gsaTiffanyCombatList[5] = "Tiffany Costume Sundress Assault Combat"
--gsaTiffanyCombatList[6] = "Tiffany Costume Sundress Spearhead Combat"
--gsaTiffanyCombatList[7] = "Tiffany Costume Sundress Subvert Combat"
--gsaTiffanyCombatList[8] = "Tiffany Costume Sundress Support Combat"

-- |[Normal]|
--Normal Clothes
DL_AddPath("Root/Images/Portraits/TiffanyDialogue/")
fnExtractDelayedBitmapToList("Tiffany Costume Normal", "Por|Tiffany|Neutral",  "Root/Images/Portraits/TiffanyDialogue/Neutral")
fnExtractDelayedBitmapToList("Tiffany Costume Normal", "Por|Tiffany|Down",     "Root/Images/Portraits/TiffanyDialogue/Down")
fnExtractDelayedBitmapToList("Tiffany Costume Normal", "Por|Tiffany|Blush",    "Root/Images/Portraits/TiffanyDialogue/Blush")
fnExtractDelayedBitmapToList("Tiffany Costume Normal", "Por|Tiffany|Smirk",    "Root/Images/Portraits/TiffanyDialogue/Smirk")
fnExtractDelayedBitmapToList("Tiffany Costume Normal", "Por|Tiffany|Upset",    "Root/Images/Portraits/TiffanyDialogue/Upset")
fnExtractDelayedBitmapToList("Tiffany Costume Normal", "Por|Tiffany|Smug",     "Root/Images/Portraits/TiffanyDialogue/Smug")
fnExtractDelayedBitmapToList("Tiffany Costume Normal", "Por|Tiffany|Offended", "Root/Images/Portraits/TiffanyDialogue/Offended")
fnExtractDelayedBitmapToList("Tiffany Costume Normal", "Por|Tiffany|Angry",    "Root/Images/Portraits/TiffanyDialogue/Angry")
fnExtractDelayedBitmapToList("Tiffany Costume Normal", "Por|Tiffany|Broken",   "Root/Images/Portraits/TiffanyDialogue/Broken")
fnExtractDelayedBitmapToList("Tiffany Costume Normal", "Por|Tiffany|Cry",      "Root/Images/Portraits/TiffanyDialogue/Cry")

--Normal Clothes, Reversed Eyes
DL_AddPath("Root/Images/Portraits/TiffanyDialogueRev/")
fnExtractDelayedBitmapToList("Tiffany Costume Normal", "Por|TiffanyRev|Neutral",  "Root/Images/Portraits/TiffanyDialogueRev/Neutral")
fnExtractDelayedBitmapToList("Tiffany Costume Normal", "Por|TiffanyRev|Down",     "Root/Images/Portraits/TiffanyDialogueRev/Down")
fnExtractDelayedBitmapToList("Tiffany Costume Normal", "Por|TiffanyRev|Blush",    "Root/Images/Portraits/TiffanyDialogueRev/Blush")
fnExtractDelayedBitmapToList("Tiffany Costume Normal", "Por|TiffanyRev|Smirk",    "Root/Images/Portraits/TiffanyDialogueRev/Smirk")
fnExtractDelayedBitmapToList("Tiffany Costume Normal", "Por|TiffanyRev|Upset",    "Root/Images/Portraits/TiffanyDialogueRev/Upset")
fnExtractDelayedBitmapToList("Tiffany Costume Normal", "Por|TiffanyRev|Smug",     "Root/Images/Portraits/TiffanyDialogueRev/Smug")
fnExtractDelayedBitmapToList("Tiffany Costume Normal", "Por|TiffanyRev|Offended", "Root/Images/Portraits/TiffanyDialogueRev/Offended")
fnExtractDelayedBitmapToList("Tiffany Costume Normal", "Por|TiffanyRev|Angry",    "Root/Images/Portraits/TiffanyDialogueRev/Angry")
fnExtractDelayedBitmapToList("Tiffany Costume Normal", "Por|TiffanyRev|Broken",   "Root/Images/Portraits/TiffanyDialogueRev/Broken")
fnExtractDelayedBitmapToList("Tiffany Costume Normal", "Por|TiffanyRev|Cry",      "Root/Images/Portraits/TiffanyDialogueRev/Cry")

--Combat
fnExtractDelayedBitmapToList("Tiffany Costume Assault Combat",   "Party|Tiffany_Assault",   "Root/Images/Portraits/Combat/Tiffany_Assault")
fnExtractDelayedBitmapToList("Tiffany Costume Spearhead Combat", "Party|Tiffany_Spearhead", "Root/Images/Portraits/Combat/Tiffany_Spearhead")
fnExtractDelayedBitmapToList("Tiffany Costume Subvert Combat",   "Party|Tiffany_Subvert",   "Root/Images/Portraits/Combat/Tiffany_Subvert")
fnExtractDelayedBitmapToList("Tiffany Costume Support Combat",   "Party|Tiffany_Support",   "Root/Images/Portraits/Combat/Tiffany_Support")

-- |[Sundress]|
--Tiffany's Sundress Variant
DL_AddPath("Root/Images/Portraits/TiffanySundressDialogue/")
fnExtractDelayedBitmapToList("Tiffany Costume Sundress", "Por|TiffanySundress|Neutral",  "Root/Images/Portraits/TiffanySundressDialogue/Neutral")
fnExtractDelayedBitmapToList("Tiffany Costume Sundress", "Por|TiffanySundress|Down",     "Root/Images/Portraits/TiffanySundressDialogue/Down")
fnExtractDelayedBitmapToList("Tiffany Costume Sundress", "Por|TiffanySundress|Blush",    "Root/Images/Portraits/TiffanySundressDialogue/Blush")
fnExtractDelayedBitmapToList("Tiffany Costume Sundress", "Por|TiffanySundress|Smirk",    "Root/Images/Portraits/TiffanySundressDialogue/Smirk")
fnExtractDelayedBitmapToList("Tiffany Costume Sundress", "Por|TiffanySundress|Upset",    "Root/Images/Portraits/TiffanySundressDialogue/Upset")
fnExtractDelayedBitmapToList("Tiffany Costume Sundress", "Por|TiffanySundress|Smug",     "Root/Images/Portraits/TiffanySundressDialogue/Smug")
fnExtractDelayedBitmapToList("Tiffany Costume Sundress", "Por|TiffanySundress|Offended", "Root/Images/Portraits/TiffanySundressDialogue/Offended")
fnExtractDelayedBitmapToList("Tiffany Costume Sundress", "Por|TiffanySundress|Angry",    "Root/Images/Portraits/TiffanySundressDialogue/Angry")
fnExtractDelayedBitmapToList("Tiffany Costume Sundress", "Por|TiffanySundress|Broken",   "Root/Images/Portraits/TiffanySundressDialogue/Broken")
fnExtractDelayedBitmapToList("Tiffany Costume Sundress", "Por|TiffanySundress|Cry",      "Root/Images/Portraits/TiffanySundressDialogue/Cry")

--Tiffany's Sundress Variant, Reversed
DL_AddPath("Root/Images/Portraits/TiffanySundressDialogueRev/")
fnExtractDelayedBitmapToList("Tiffany Costume Sundress", "Por|TiffanySundressRev|Neutral",  "Root/Images/Portraits/TiffanySundressDialogueRev/Neutral")
fnExtractDelayedBitmapToList("Tiffany Costume Sundress", "Por|TiffanySundressRev|Down",     "Root/Images/Portraits/TiffanySundressDialogueRev/Down")
fnExtractDelayedBitmapToList("Tiffany Costume Sundress", "Por|TiffanySundressRev|Blush",    "Root/Images/Portraits/TiffanySundressDialogueRev/Blush")
fnExtractDelayedBitmapToList("Tiffany Costume Sundress", "Por|TiffanySundressRev|Smirk",    "Root/Images/Portraits/TiffanySundressDialogueRev/Smirk")
fnExtractDelayedBitmapToList("Tiffany Costume Sundress", "Por|TiffanySundressRev|Upset",    "Root/Images/Portraits/TiffanySundressDialogueRev/Upset")
fnExtractDelayedBitmapToList("Tiffany Costume Sundress", "Por|TiffanySundressRev|Smug",     "Root/Images/Portraits/TiffanySundressDialogueRev/Smug")
fnExtractDelayedBitmapToList("Tiffany Costume Sundress", "Por|TiffanySundressRev|Offended", "Root/Images/Portraits/TiffanySundressDialogueRev/Offended")
fnExtractDelayedBitmapToList("Tiffany Costume Sundress", "Por|TiffanySundressRev|Angry",    "Root/Images/Portraits/TiffanySundressDialogueRev/Angry")
fnExtractDelayedBitmapToList("Tiffany Costume Sundress", "Por|TiffanySundressRev|Broken",   "Root/Images/Portraits/TiffanySundressDialogueRev/Broken")
fnExtractDelayedBitmapToList("Tiffany Costume Sundress", "Por|TiffanySundressRev|Cry",      "Root/Images/Portraits/TiffanySundressDialogueRev/Cry")

--Only Assault gets one, the others use the same image.
fnExtractDelayedBitmapToList("Tiffany Costume Sundress Assault Combat",   "Party|Tiffany_Sundress",   "Root/Images/Portraits/Combat/Tiffany_Sundress")
--fnAppendDelayedBitmapToList("Tiffany Costume Sundress Spearhead Combat", "Party|Tiffany_Spearhead", "Root/Images/Portraits/Combat/Tiffany_Spearhead")
--fnAppendDelayedBitmapToList("Tiffany Costume Sundress Support Combat",   "Party|Tiffany_Subvert",   "Root/Images/Portraits/Combat/Tiffany_Subvert")
--fnAppendDelayedBitmapToList("Tiffany Costume Sundress Support Combat",   "Party|Tiffany_Support",   "Root/Images/Portraits/Combat/Tiffany_Support")

-- |[ =========================================== 56 =========================================== ]|
--Tiffany's sister. Yells a lot. Has a small emote set.
Debug_Print("Loading 56's portraits.\n")
SLF_Open(gsaDatafilePaths.s56Path)

--Normal Clothes
DL_AddPath("Root/Images/Portraits/2856Dialogue/")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|56|Neutral",   "Root/Images/Portraits/2856Dialogue/Neutral")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|56|Punchable", "Root/Images/Portraits/2856Dialogue/Punchable")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|56|Angry",     "Root/Images/Portraits/2856Dialogue/Angry")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|56|Yelling",   "Root/Images/Portraits/2856Dialogue/Yelling")

--Nude Variant
DL_AddPath("Root/Images/Portraits/2856NudeDialogue/")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|56Nude|Neutral",   "Root/Images/Portraits/2856NudeDialogue/Neutral")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|56Nude|Punchable", "Root/Images/Portraits/2856NudeDialogue/Punchable")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|56Nude|Angry",     "Root/Images/Portraits/2856NudeDialogue/Angry")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|56Nude|Yelling",   "Root/Images/Portraits/2856NudeDialogue/Yelling")

-- |[ ========================================= SX-399 ========================================= ]|
-- |[Setup]|
--The one and only Steam Lord.
Debug_Print("Loading SX-399's portraits.\n")
SLF_Open(gsaDatafilePaths.sSX399Path)

-- |[Costume List Creation]|
--Costumes.
fnCreateDelayedLoadList("SX-399 Costume Normal")
fnCreateDelayedLoadList("SX-399 Costume Young")

--Combat version.
fnCreateDelayedLoadList("SX-399 Costume Normal Combat")
fnCreateDelayedLoadList("SX-399 Costume Young Combat")

-- |[Store Lists]|
--Create a list of all of Florentina's costume sets.
gsaSX399DialogueList = {}
gsaSX399DialogueList[1] = "SX-399 Costume Normal"
gsaSX399DialogueList[2] = "SX-399 Costume Young"

--Combat version.
gsaSX399CombatList = {}
gsaSX399CombatList[1] = "SX-399 Costume Normal Combat"
gsaSX399CombatList[2] = "SX-399 Costume Young Combat"

-- |[Normal]|
--This is her "in the party" basic costume.
DL_AddPath("Root/Images/Portraits/SX-399Shocktrooper/")
fnExtractDelayedBitmapToList("SX-399 Costume Normal", "Por|SX-399|Neutral",  "Root/Images/Portraits/SX-399Shocktrooper/Neutral")
fnExtractDelayedBitmapToList("SX-399 Costume Normal", "Por|SX-399|Happy",    "Root/Images/Portraits/SX-399Shocktrooper/Happy")
fnExtractDelayedBitmapToList("SX-399 Costume Normal", "Por|SX-399|Blush",    "Root/Images/Portraits/SX-399Shocktrooper/Blush")
fnExtractDelayedBitmapToList("SX-399 Costume Normal", "Por|SX-399|Smirk",    "Root/Images/Portraits/SX-399Shocktrooper/Smirk")
fnExtractDelayedBitmapToList("SX-399 Costume Normal", "Por|SX-399|Sad",      "Root/Images/Portraits/SX-399Shocktrooper/Sad")
fnExtractDelayedBitmapToList("SX-399 Costume Normal", "Por|SX-399|Flirt",    "Root/Images/Portraits/SX-399Shocktrooper/Flirt")
fnExtractDelayedBitmapToList("SX-399 Costume Normal", "Por|SX-399|Angry",    "Root/Images/Portraits/SX-399Shocktrooper/Angry")
fnExtractDelayedBitmapToList("SX-399 Costume Normal", "Por|SX-399|Flirt2",   "Root/Images/Portraits/SX-399Shocktrooper/Flirt2")

--Combat.
fnExtractDelayedBitmapToList("SX-399 Costume Normal Combat", "Party|SX-399", "Root/Images/Portraits/Combat/SX-399")

-- |[Young Droid]|
--Used before she gets repaired.
DL_AddPath("Root/Images/Portraits/SX-399YoungDroid/")
fnExtractDelayedBitmapToList("SX-399 Costume Young", "Por|SX-399|Steam", "Root/Images/Portraits/SX-399YoungDroid/Neutral")

--No combat pose.
fnAppendDelayedBitmapToList("SX-399 Costume Young Combat", "Party|SX-399", "Root/Images/Portraits/Combat/SX-399")


-- |[ ========================================= Sophie ========================================= ]|
-- |[Sophie's Portraits]|
Debug_Print("Loading Sophie's portraits.\n")
SLF_Open(gsaDatafilePaths.sSophiePath)

--Normal
DL_AddPath("Root/Images/Portraits/SophieDialogue/")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|Sophie|Neutral",   "Root/Images/Portraits/SophieDialogue/Neutral")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|Sophie|Happy",     "Root/Images/Portraits/SophieDialogue/Happy")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|Sophie|Blush",     "Root/Images/Portraits/SophieDialogue/Blush")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|Sophie|Smirk",     "Root/Images/Portraits/SophieDialogue/Smirk")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|Sophie|Sad",       "Root/Images/Portraits/SophieDialogue/Sad")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|Sophie|Surprise",  "Root/Images/Portraits/SophieDialogue/Surprised")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|Sophie|Offended",  "Root/Images/Portraits/SophieDialogue/Offended")

--Gala variant.
DL_AddPath("Root/Images/Portraits/SophieDialogueGala/")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|SophieGala|Neutral",   "Root/Images/Portraits/SophieDialogueGala/Neutral")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|SophieGala|Happy",     "Root/Images/Portraits/SophieDialogueGala/Happy")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|SophieGala|Blush",     "Root/Images/Portraits/SophieDialogueGala/Blush")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|SophieGala|Smirk",     "Root/Images/Portraits/SophieDialogueGala/Smirk")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|SophieGala|Sad",       "Root/Images/Portraits/SophieDialogueGala/Sad")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|SophieGala|Surprise",  "Root/Images/Portraits/SophieDialogueGala/Surprised")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|SophieGala|Offended",  "Root/Images/Portraits/SophieDialogueGala/Offended")

--Domina variant.
DL_AddPath("Root/Images/Portraits/SophieDialogueDomina/")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|SophieWhip|Neutral",   "Root/Images/Portraits/SophieDialogueDomina/Neutral")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|SophieWhip|Happy",     "Root/Images/Portraits/SophieDialogueDomina/Happy")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|SophieWhip|Blush",     "Root/Images/Portraits/SophieDialogueDomina/Blush")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|SophieWhip|Smirk",     "Root/Images/Portraits/SophieDialogueDomina/Smirk")

-- |[ ========================================== JX101 ========================================= ]|
--While not a full party member, JX-101 is a guest party member and has a few emotes.
Debug_Print("Loading JX-101's portraits.\n")
SLF_Open(gsaDatafilePaths.sJX101Path)

--Normal.
DL_AddPath("Root/Images/Portraits/JX-101/")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|JX101|Neutral", "Root/Images/Portraits/JX-101/Neutral")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|JX101|HatA", "Root/Images/Portraits/JX-101/HatA")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|JX101|HatB", "Root/Images/Portraits/JX-101/HatB")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|JX101|HatC", "Root/Images/Portraits/JX-101/HatC")

--Combat.
fnExtractDelayedBitmapToList(sEverythingList, "Party|JX101", "Root/Images/Portraits/Combat/JX-101")

-- |[ ========================================== Sammy ========================================= ]|
--Sammy Davis IS Agent Almond IN Some Moths Do! She also appears in other chapters.
Debug_Print("Loading Sammy's portraits.\n")
SLF_Open(gsaDatafilePaths.sSammyPath)

--Normal/Almond
DL_AddPath("Root/Images/Portraits/Sammy/")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|Sammy|Neutral", "Root/Images/Portraits/Sammy/Neutral")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|Sammy|Sexy",    "Root/Images/Portraits/Sammy/Sexy")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|Sammy|Gun",     "Root/Images/Portraits/Sammy/Gun")

-- |[ ==================================== Minor Characters ==================================== ]|
-- |[Generic Alraune]|
--Borrowed from chapter 1.
local saChapter1DiaEnemyList = {"Alraune"}
DL_AddPath("Root/Images/Portraits/Enemies/")
SLF_Open(gsaDatafilePaths.sChapter1EmotePath)
fnExtractList("Por|", "|Neutral", "Root/Images/Portraits/Enemies/", saChapter1DiaEnemyList)

-- |[Chapter 5 Loading]|
Debug_Print("Loading minor character portraits.\n")
SLF_Open(gsaDatafilePaths.sChapter5EmotePath)

-- |[Generic Golem]|
DL_AddPath("Root/Images/Portraits/Golem/")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|Golem|Neutral", "Root/Images/Portraits/Golem/Neutral")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|GolemPack|Neutral", "Root/Images/Portraits/Golem/Pack")

--Rebels
DL_AddPath("Root/Images/Portraits/NPCs/")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|GolemRebelHead|Neutral",  "Root/Images/Portraits/NPCs/GolemRebelHead")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|GolemRebelScarf|Neutral", "Root/Images/Portraits/NPCs/GolemRebelScarf")

-- |[Secrebots]|
DL_AddPath("Root/Images/Portraits/Secrebots/")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|SecrebotP|Neutral", "Root/Images/Portraits/Secrebots/SecrebotP")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|SecrebotY|Neutral", "Root/Images/Portraits/Secrebots/SecrebotY")

-- |[Night]|
DL_AddPath("Root/Images/Portraits/Night/")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|Night|Human",         "Root/Images/Portraits/Night/Human")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|Night|Panic",         "Root/Images/Portraits/Night/Panic")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|Night|Secrebot",      "Root/Images/Portraits/Night/Secrebot")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|Night|SecrebotSad",   "Root/Images/Portraits/Night/SecrebotSad")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|Night|SecrebotHappy", "Root/Images/Portraits/Night/SecrebotHappy")

-- |[Talos]|
DL_AddPath("Root/Images/Portraits/Talos/")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|Talos|Neutral", "Root/Images/Portraits/Talos/Neutral")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|Talos|Unsure",  "Root/Images/Portraits/Talos/Unsure")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|Talos|Wise",    "Root/Images/Portraits/Talos/Wise")

-- |[Unit 1969]|
DL_AddPath("Root/Images/Portraits/1969/")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|1969|Neutral", "Root/Images/Portraits/1969/Neutral")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|1969|Smarmy",  "Root/Images/Portraits/1969/Smarmy")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|1969|Booyeah", "Root/Images/Portraits/1969/Booyeah")

-- |[Generic Golem Lord]|
DL_AddPath("Root/Images/Portraits/GolemLord/")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|GolemLord|Neutral", "Root/Images/Portraits/GolemLord/Neutral")
DL_AddPath("Root/Images/Portraits/GolemLordB/")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|GolemLordB|Neutral", "Root/Images/Portraits/GolemLordB/Neutral")
DL_AddPath("Root/Images/Portraits/GolemLordC/")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|GolemLordC|Neutral", "Root/Images/Portraits/GolemLordC/Neutral")

-- |[Narissa]|
DL_AddPath("Root/Images/Portraits/Narissa/")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|Raiju|Neutral", "Root/Images/Portraits/Narissa/Neutral")

-- |[Dr. Maisie]|
DL_AddPath("Root/Images/Portraits/Maisie/")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|Maisie|Neutral", "Root/Images/Portraits/Maisie/Neutral")

-- |[Ellie]|
DL_AddPath("Root/Images/Portraits/Ellie/")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|Ellie|Neutral", "Root/Images/Portraits/Ellie/Neutral")

-- |[Katarina]|
DL_AddPath("Root/Images/Portraits/Katarina/")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|Katarina|Neutral", "Root/Images/Portraits/Katarina/Neutral")

-- |[The Administrator]|
--Special: Shadowed Doll as Administrator
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|DollAsAdmin|Neutral",  "Root/Images/Portraits/NPCs/DollAsAdmin")

--Special: Layered Portraits. These always stay loaded.
DL_AddPath("Root/Images/Portraits/Layered/")
fnExtractDelayedBitmapToList(sEverythingList, "Por|Admin|Base",   "Root/Images/Portraits/Layered/Admin|Base")
fnExtractDelayedBitmapToList(sEverythingList, "Por|Admin|Layer1", "Root/Images/Portraits/Layered/Admin|Layer1")
fnExtractDelayedBitmapToList(sEverythingList, "Por|Admin|Layer2", "Root/Images/Portraits/Layered/Admin|Layer2")

-- |[Bosses]|
DL_AddPath("Root/Images/Portraits/609144/")
DL_AddPath("Root/Images/Portraits/Vivify/")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|609144|Neutral", "Root/Images/Portraits/609144/Neutral")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|Vivify|Neutral", "Root/Images/Portraits/Vivify/Neutral")

-- |[Crowbar Chan]|
SLF_Open(gsaDatafilePaths.sChapter0EmotePath)
DL_AddPath("Root/Images/Portraits/CrowbarChan/")
fnExtractDelayedBitmapToList(sCh5DialogueList, "Por|CrowbarChan|Neutral",  "Root/Images/Portraits/CrowbarChan/Neutral")

-- |[Cassandra's Portraits]|
SLF_Open(gsaDatafilePaths.sCassandraPath)
DL_AddPath("Root/Images/Portraits/Cassandra/")
fnExtractDelayedBitmapToList(sEverythingList, "Por|Cassandra|Neutral", "Root/Images/Portraits/Cassandra/Human")
fnExtractDelayedBitmapToList(sEverythingList, "Por|Cassandra|Werecat", "Root/Images/Portraits/Cassandra/Werecat")
fnExtractDelayedBitmapToList(sEverythingList, "Por|Cassandra|Golem",   "Root/Images/Portraits/Cassandra/Golem")

-- |[ ================================ Enemy Dialogue Portraits ================================ ]|
--Portraits used when an enemy type speaks to the party.
local sSystemPortraits = "System Portraits"
sEverythingList = sSystemPortraits
fnCreateDelayedLoadList(sSystemPortraits)

-- |[Loading List]|
local saChapter5DiaEnemyList   = {"Doll", "Electrosprite", "LatexDrone", "Raiju", "DarkmatterGirl", "SteamDroid", "Dreamer", "Vivify", "DollInfluenced", "201890"}
local saChapter5FancyGolemList = {"GolemFancyA", "GolemFancyB", "GolemFancyC", "GolemFancyD", "GolemFancyE", "GolemFancyF"}

-- |[List Extraction]|
--Uses the auto-loader.
DL_AddPath("Root/Images/Portraits/Enemies/")
SLF_Open(gsaDatafilePaths.sChapter5EmotePath)
fnExtractList("Por|", "|Neutral", "Root/Images/Portraits/Enemies/", saChapter5DiaEnemyList)
fnExtractList("Por|", "|Neutral", "Root/Images/Portraits/Enemies/", saChapter5FancyGolemList)

-- |[Misc NPCs]|
--NPCs that are in this chapter even if not specific to it.
SLF_Open(gsaDatafilePaths.sChapter0EmotePath)
DL_AddPath("Root/Images/Portraits/NPCs/")

--Extract.
fnExtractDelayedBitmapToList(sSystemPortraits, "Por|HumanNPCF0|Neutral",  "Root/Images/Portraits/NPCs/HumanNPCF0")
fnExtractDelayedBitmapToList(sSystemPortraits, "Por|HumanNPCF0B|Neutral", "Root/Images/Portraits/NPCs/HumanNPCF0B") --Boobalicious version
fnExtractDelayedBitmapToList(sSystemPortraits, "Por|HumanNPCF1|Neutral",  "Root/Images/Portraits/NPCs/HumanNPCF1")
fnExtractDelayedBitmapToList(sSystemPortraits, "Por|HumanNPCF1B|Neutral", "Root/Images/Portraits/NPCs/HumanNPCF1B") --Boobalicious version
fnExtractDelayedBitmapToList(sSystemPortraits, "Por|MercF|Neutral",       "Root/Images/Portraits/NPCs/MercF")
fnExtractDelayedBitmapToList(sSystemPortraits, "Por|MercM|Neutral",       "Root/Images/Portraits/NPCs/MercM")

--Issue load order.
fnLoadDelayedBitmapsFromList(sSystemPortraits, gciDelayedLoadLoadImmediately)

--Reset the everything list.
sEverythingList = "Chapter 5 Portraits"

-- |[ ==================================== Combat Portraits ==================================== ]|
-- |[Chapter 0 Enemies]|
--File
SLF_Open(gsaDatafilePaths.sChapter0CombatPath)

--Normal
fnExtractDelayedBitmapToList(sEverythingList, "Enemy|BandageGoblin",   "Root/Images/Portraits/Combat/BandageGoblin")
fnExtractDelayedBitmapToList(sEverythingList, "Enemy|BestFriend",      "Root/Images/Portraits/Combat/BestFriend")
fnExtractDelayedBitmapToList(sEverythingList, "Enemy|Hoodie",          "Root/Images/Portraits/Combat/Hoodie")
fnExtractDelayedBitmapToList(sEverythingList, "Enemy|Horrible",        "Root/Images/Portraits/Combat/Horrible")
fnExtractDelayedBitmapToList(sEverythingList, "Enemy|InnGeisha",       "Root/Images/Portraits/Combat/InnGeisha")
fnExtractDelayedBitmapToList(sEverythingList, "Enemy|MirrorImage",     "Root/Images/Portraits/Combat/MirrorImage")
fnExtractDelayedBitmapToList(sEverythingList, "Enemy|SkullCrawler",    "Root/Images/Portraits/Combat/SkullCrawler")
fnExtractDelayedBitmapToList(sEverythingList, "Enemy|VoidRift",        "Root/Images/Portraits/Combat/VoidRift")

--Paragon
fnExtractDelayedBitmapToList(sEverythingList, "Paragon|BandageGoblin",   "Root/Images/Portraits/Paragon/BandageGoblin")
fnExtractDelayedBitmapToList(sEverythingList, "Paragon|BestFriend",      "Root/Images/Portraits/Paragon/BestFriend")
fnExtractDelayedBitmapToList(sEverythingList, "Paragon|Hoodie",          "Root/Images/Portraits/Paragon/Hoodie")
fnExtractDelayedBitmapToList(sEverythingList, "Paragon|Horrible",        "Root/Images/Portraits/Paragon/Horrible")
fnExtractDelayedBitmapToList(sEverythingList, "Paragon|InnGeisha",       "Root/Images/Portraits/Paragon/InnGeisha")
fnExtractDelayedBitmapToList(sEverythingList, "Paragon|MirrorImage",     "Root/Images/Portraits/Paragon/MirrorImage")
fnExtractDelayedBitmapToList(sEverythingList, "Paragon|SkullCrawler",    "Root/Images/Portraits/Paragon/SkullCrawler")
fnExtractDelayedBitmapToList(sEverythingList, "Paragon|VoidRift",        "Root/Images/Portraits/Paragon/VoidRift")

-- |[Chapter 5 Enemies]|
--File
SLF_Open(gsaDatafilePaths.sChapter5CombatPath)

--Normal
fnExtractDelayedBitmapToList(sEverythingList, "Enemy|DarkmatterGirl",  "Root/Images/Portraits/Combat/DarkmatterGirl")
fnExtractDelayedBitmapToList(sEverythingList, "Enemy|Doll",            "Root/Images/Portraits/Combat/Doll")
fnExtractDelayedBitmapToList(sEverythingList, "Enemy|Dreamer",         "Root/Images/Portraits/Combat/Dreamer")
fnExtractDelayedBitmapToList(sEverythingList, "Enemy|GolemLord",       "Root/Images/Portraits/Combat/GolemLord")
fnExtractDelayedBitmapToList(sEverythingList, "Enemy|GolemSlave",      "Root/Images/Portraits/Combat/GolemSlave")
fnExtractDelayedBitmapToList(sEverythingList, "Enemy|LatexDrone",      "Root/Images/Portraits/Combat/LatexDrone")
fnExtractDelayedBitmapToList(sEverythingList, "Enemy|Motilvac",        "Root/Images/Portraits/Combat/Motilvac")
fnExtractDelayedBitmapToList(sEverythingList, "Enemy|MrWipey",         "Root/Images/Portraits/Combat/MrWipey")
fnExtractDelayedBitmapToList(sEverythingList, "Enemy|Plannerbot",      "Root/Images/Portraits/Combat/Plannerbot")
fnExtractDelayedBitmapToList(sEverythingList, "Enemy|Raibie",          "Root/Images/Portraits/Combat/Raibie")
fnExtractDelayedBitmapToList(sEverythingList, "Enemy|Scraprat",        "Root/Images/Portraits/Combat/Scraprat")
fnExtractDelayedBitmapToList(sEverythingList, "Enemy|Secrebot",        "Root/Images/Portraits/Combat/Secrebot")
fnExtractDelayedBitmapToList(sEverythingList, "Enemy|WreckedBot",      "Root/Images/Portraits/Combat/WreckedBot")

--Paragon
fnExtractDelayedBitmapToList(sEverythingList, "Paragon|DarkmatterGirl",  "Root/Images/Portraits/Paragon/DarkmatterGirl")
fnExtractDelayedBitmapToList(sEverythingList, "Paragon|Doll",            "Root/Images/Portraits/Paragon/Doll")
fnExtractDelayedBitmapToList(sEverythingList, "Paragon|Dreamer",         "Root/Images/Portraits/Paragon/Dreamer")
fnExtractDelayedBitmapToList(sEverythingList, "Paragon|GolemLord",       "Root/Images/Portraits/Paragon/GolemLord")
fnExtractDelayedBitmapToList(sEverythingList, "Paragon|GolemSlave",      "Root/Images/Portraits/Paragon/GolemSlave")
fnExtractDelayedBitmapToList(sEverythingList, "Paragon|LatexDrone",      "Root/Images/Portraits/Paragon/LatexDrone")
fnExtractDelayedBitmapToList(sEverythingList, "Paragon|Motilvac",        "Root/Images/Portraits/Paragon/Motilvac")
fnExtractDelayedBitmapToList(sEverythingList, "Paragon|MrWipey",         "Root/Images/Portraits/Paragon/MrWipey")
fnExtractDelayedBitmapToList(sEverythingList, "Paragon|Plannerbot",      "Root/Images/Portraits/Paragon/Plannerbot")
fnExtractDelayedBitmapToList(sEverythingList, "Paragon|Raibie",          "Root/Images/Portraits/Paragon/Raibie")
fnExtractDelayedBitmapToList(sEverythingList, "Paragon|Scraprat",        "Root/Images/Portraits/Paragon/Scraprat")
fnExtractDelayedBitmapToList(sEverythingList, "Paragon|Secrebot",        "Root/Images/Portraits/Paragon/Secrebot")
fnExtractDelayedBitmapToList(sEverythingList, "Paragon|WreckedBot",      "Root/Images/Portraits/Paragon/WreckedBot")

--Bosses
fnExtractDelayedBitmapToList(sEverythingList, "Enemy|609144",          "Root/Images/Portraits/Combat/609144")
fnExtractDelayedBitmapToList(sEverythingList, "Enemy|Vivify",          "Root/Images/Portraits/Combat/Vivify")
fnExtractDelayedBitmapToList(sEverythingList, "Enemy|Serenity",        "Root/Images/Portraits/Combat/Serenity")
fnExtractDelayedBitmapToList(sEverythingList, "Enemy|TechMonstrosity", "Root/Images/Portraits/Combat/TechMonstrosity")

-- |[ ======================================== Clean Up ======================================== ]|
-- |[Reset Flag]|
--Default flag state is true.
Bitmap_SetNewBitmapNoHandleWarnings(true)

-- |[Issue Asset Stream]|
--If this flag is set, order all dialogue images that aren't linked to a costume to load.
if(gbUnloadDialoguePortraitsWhenDialogueCloses == false) then
    fnLoadDelayedBitmapsFromList(sCh5DialogueList, gciDelayedLoadLoadAtEndOfTick)
end

--Order all combat and unconditional images to load.
fnLoadDelayedBitmapsFromList(sEverythingList, gciDelayedLoadLoadAtEndOfTick)

-- |[Clean]|
SLF_Close()
Debug_PopPrint("Completed Portrait Loader.\n")
