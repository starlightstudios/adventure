-- |[ =================================== Chapter 5 Overlays =================================== ]|
--All of this is done via a meta-autoloader.
local sEverythingList = "Chapter 5 Overlays"
fnCreateDelayedLoadList(sEverythingList)

--Execute autoload.
SLF_Open(gsaDatafilePaths.sChapter5MapAutoload)
fnExecAutoloadDelayed("AutoLoad|Adventure|Maps_Ch5_Immediate", sEverythingList)

--Order everything to enqueue loading.
fnLoadDelayedBitmapsFromList(sEverythingList, gciDelayedLoadLoadAtEndOfTick)
