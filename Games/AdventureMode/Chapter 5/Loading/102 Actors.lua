-- |[ =============================== Dialogue Actors: Chapter 5 =============================== ]|
--All dialogue actors from chapter 5.

-- |[ ================================= Party/Important Actors ================================= ]|
-- |[Setup]|
--Alias listings.
local saNoAliases        = {"NOALIAS"}
local saChristineAliases = {"Chris", "771852", "772890", "CR-1-16"}
local saPDUAliases       = {"NOALIAS"}
local saTiffanyAliases   = {"Girl", "55", "2855"}
local saSX399Aliases     = {"Young Droid"}
local saSophieAliases    = {"499323"}
local saJX101Aliases     = {"Battle Droid"}
local saMaramAliases     = {"Rilmani"}

--Emotion listings.
local saNoEmotions         = {"Neutral"}
local saChristineEmotions  = {"Neutral", "Smirk", "Happy", "Blush", "Sad", "Scared", "Offended", "Cry", "Laugh", "Angry", "PDU"}
local saPDUEmotions        = {"Neutral", "Happy", "Alarmed", "Question", "Quiet"}
local saTiffanyEmotions    = {"Neutral", "Down",  "Blush", "Smirk", "Upset", "Smug", "Offended", "Angry", "Broken", "Cry"}
local saSX399Emotions      = {"Neutral", "Happy", "Blush", "Smirk", "Sad", "Flirt", "Angry", "Flirt2", "Steam"}
local saSophieEmotions     = {"Neutral", "Happy", "Blush", "Smirk", "Sad", "Surprised", "Offended"}
local saJX101Emotions      = {"Neutral", "HatA", "HatB", "HatC"}

-- |[Chapter 5]|
--Mainline
fnCreateDialogueActor("Christine",   "Voice|Christine",   "Root/Images/Portraits/ChristineDialogue/", true, saChristineAliases, saChristineEmotions)
fnCreateDialogueActor("PDU",         "Voice|Narrator",    "Root/Images/Portraits/PDUDialogue/",       true, saPDUAliases,       saPDUEmotions)
fnCreateDialogueActor("Tiffany",     "Voice|Tiffany",     "Root/Images/Portraits/TiffanyDialogue/",   true, saTiffanyAliases,   saTiffanyEmotions)
fnCreateDialogueActor("SX-399",      "Voice|SX399",       "Root/Images/Portraits/SX399Dialogue/",     true, saSX399Aliases,     saSX399Emotions)
fnCreateDialogueActor("Sophie",      "Voice|Sophie",      "Root/Images/Portraits/SophieDialogue/",    true, saSophieAliases,    saSophieEmotions)
fnCreateDialogueActor("JX-101",      "Voice|JX101",       "Root/Images/Portraits/JX-101/",            true, saJX101Aliases,     saJX101Emotions)
fnCreateDialogueActor("609144",      "Voice|GenericN02",  "Root/Images/Portraits/609144/",            true, saNoAliases,        saNoEmotions)
fnCreateDialogueActor("CrowbarChan", "Voice|CrowbarChan", "Root/Images/Portraits/CrowbarChan/",       true, saNoAliases,        saNoEmotions)

--Right-Facing Case for Tiffany
DialogueActor_Push("Tiffany")
    DialogueActor_SetProperty("Add Emotion Rev", "Neutral",  "Root/Images/Portraits/TiffanyDialogueRev/Neutral",  true)
    DialogueActor_SetProperty("Add Emotion Rev", "Down",     "Root/Images/Portraits/TiffanyDialogueRev/Down",     true)
    DialogueActor_SetProperty("Add Emotion Rev", "Blush",    "Root/Images/Portraits/TiffanyDialogueRev/Blush",    true)
    DialogueActor_SetProperty("Add Emotion Rev", "Smirk",    "Root/Images/Portraits/TiffanyDialogueRev/Smirk",    true)
    DialogueActor_SetProperty("Add Emotion Rev", "Upset",    "Root/Images/Portraits/TiffanyDialogueRev/Upset",    true)
    DialogueActor_SetProperty("Add Emotion Rev", "Smug",     "Root/Images/Portraits/TiffanyDialogueRev/Smug",     true)
    DialogueActor_SetProperty("Add Emotion Rev", "Offended", "Root/Images/Portraits/TiffanyDialogueRev/Offended", true)
    DialogueActor_SetProperty("Add Emotion Rev", "Angry",    "Root/Images/Portraits/TiffanyDialogueRev/Angry",    true)
    DialogueActor_SetProperty("Add Emotion Rev", "Broken",   "Root/Images/Portraits/TiffanyDialogueRev/Broken",   true)
    DialogueActor_SetProperty("Add Emotion Rev", "Cry",      "Root/Images/Portraits/TiffanyDialogueRev/Cry",      true)
DL_PopActiveObject()

--Alternate costumes
fnCreateDialogueActor("TiffanySundress", "Voice|Tiffany", "Root/Images/Portraits/TiffanySundressDialogue/",  true, saTiffanyAliases,      saTiffanyEmotions)

--Right-Facing Case for 55's Sundress variant
DialogueActor_Push("TiffanySundress")
    DialogueActor_SetProperty("Add Emotion Rev", "Neutral",  "Root/Images/Portraits/TiffanySundressDialogueRev/Neutral",  true)
    DialogueActor_SetProperty("Add Emotion Rev", "Down",     "Root/Images/Portraits/TiffanySundressDialogueRev/Down",     true)
    DialogueActor_SetProperty("Add Emotion Rev", "Blush",    "Root/Images/Portraits/TiffanySundressDialogueRev/Blush",    true)
    DialogueActor_SetProperty("Add Emotion Rev", "Smirk",    "Root/Images/Portraits/TiffanySundressDialogueRev/Smirk",    true)
    DialogueActor_SetProperty("Add Emotion Rev", "Upset",    "Root/Images/Portraits/TiffanySundressDialogueRev/Upset",    true)
    DialogueActor_SetProperty("Add Emotion Rev", "Smug",     "Root/Images/Portraits/TiffanySundressDialogueRev/Smug",     true)
    DialogueActor_SetProperty("Add Emotion Rev", "Offended", "Root/Images/Portraits/TiffanySundressDialogueRev/Offended", true)
    DialogueActor_SetProperty("Add Emotion Rev", "Angry",    "Root/Images/Portraits/TiffanySundressDialogueRev/Angry",    true)
    DialogueActor_SetProperty("Add Emotion Rev", "Broken",   "Root/Images/Portraits/TiffanySundressDialogueRev/Broken",   true)
    DialogueActor_SetProperty("Add Emotion Rev", "Cry",      "Root/Images/Portraits/TiffanySundressDialogueRev/Cry",      true)
DL_PopActiveObject()

--ChrisMaleVoice. Used to manually override the voice of Christine when in male form.
-- This never actually appears, it's just used for the voice.
DialogueActor_Create("ChrisMaleVoice")
	DialogueActor_SetProperty("Add Alias", "None")
DL_PopActiveObject()
WD_SetProperty("Register Voice", "ChrisMaleVoice", "Voice|Chris")

--2856. 2855's identical twin sister. Specially created (for now) because she uses non-standard sprites. Will get emotion sets later.
DialogueActor_Create("2856")
	DialogueActor_SetProperty("Add Alias", "2856")
	DialogueActor_SetProperty("Add Alias", "56")
	DialogueActor_SetProperty("Add Alias", "2855")
	DialogueActor_SetProperty("Add Alias", "55")
	DialogueActor_SetProperty("Add Emotion", "Neutral",   "Root/Images/Portraits/2856Dialogue/Neutral",   true)
	DialogueActor_SetProperty("Add Emotion", "Punchable", "Root/Images/Portraits/2856Dialogue/Punchable", true)
	DialogueActor_SetProperty("Add Emotion", "Angry",     "Root/Images/Portraits/2856Dialogue/Angry",     true)
	DialogueActor_SetProperty("Add Emotion", "Yelling",   "Root/Images/Portraits/2856Dialogue/Yelling",   true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "2856", "Voice|Tiffany")

-- |[ ====================================== Major Actors ====================================== ]|
--Refers to actors who are important enough to usually warrant their own sprite, but not a full emotion set.
-- May also refer to actors who play a big role but only in one section of the game.

--Cassandra is created specially, since her Neutral emotion is her human form. She gets transformed a lot.
DialogueActor_Create("Cassandra")
	DialogueActor_SetProperty("Add Alias", "Lady")
	DialogueActor_SetProperty("Add Alias", "Cassandra")
	DialogueActor_SetProperty("Add Alias", "Fang")
	DialogueActor_SetProperty("Add Emotion", "Neutral", "Root/Images/Portraits/Cassandra/Human",   true)
	DialogueActor_SetProperty("Add Emotion", "Werecat", "Root/Images/Portraits/Cassandra/Werecat", true)
	DialogueActor_SetProperty("Add Emotion", "Golem",   "Root/Images/Portraits/Cassandra/Golem",   true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "Cassandra", "Voice|GenericF13")

--Sammy Davis: Action Moth!
DialogueActor_Create("Sammy")
	DialogueActor_SetProperty("Add Alias", "Almond")
	DialogueActor_SetProperty("Add Emotion", "Neutral", "Root/Images/Portraits/Sammy/Neutral", true)
	DialogueActor_SetProperty("Add Emotion", "Sexy",    "Root/Images/Portraits/Sammy/Sexy",    true)
	DialogueActor_SetProperty("Add Emotion", "Gun",     "Root/Images/Portraits/Sammy/Gun",     true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "Sammy", "Voice|GenericF15")

-- |[Chapter 5]|
fnCreateDialogueActor("TT-233",  "Voice|GenericF09", "Root/Images/Portraits/Enemies/SteamDroid",  true, {"Doctor Droid"}, {"NEUTRALISPATH"})
--fnCreateDialogueActor("Sammy",   "Voice|GenericF15", "Root/Images/Portraits/Sammy/",              true, saSammyAliases,     saSammyEmotions)

-- |[ ====================================== Minor Actors ====================================== ]|
--NPCs or Enemy actors, usually play small roles many times. Some have unique dialogue sprites because their enemy field
-- variants are too big to fit on the dialogue screen.

--"Named" NPCs.
fnCreateDialogueActor("Coconut",  "Voice|GenericF03", "Root/Images/Portraits/NPCs/HumanNPCF0", true,  {"Coconut"}, {"NEUTRALISPATH"})

--"Unnamed" versions. Not characters, just stand-ins for a monster/enemy type.
fnCreateDialogueActor("Alraune",  "Voice|Alraune",    "Root/Images/Portraits/Enemies/Alraune", true, {"Alraune", "Dafoda", "Reika"},            {"NEUTRALISPATH"})

-- |[Chapter 5]|
fnCreateDialogueActor("GolemLord",      "Voice|GenericF09", "Root/Images/Portraits/GolemLord/Neutral",      true,  {"Golem", "Golem Lord", "Lord", "692339", "649728", "Jackie", "Trucy", "487201", "Odess"}, {"NEUTRALISPATH"})
fnCreateDialogueActor("GolemLordB",     "Voice|GenericF10", "Root/Images/Portraits/GolemLordB/Neutral",     true,  {"Golem", "Golem Lord", "Lord", "Primrose", "Priscilla"},         {"NEUTRALISPATH"})
fnCreateDialogueActor("GolemLordC",     "Voice|GenericF09", "Root/Images/Portraits/GolemLordC/Neutral",     true,  {"Golem", "Golem Lord", "Lord"},                     {"NEUTRALISPATH"})
fnCreateDialogueActor("GolemFancyA",    "Voice|GenericF10", "Root/Images/Portraits/Enemies/GolemFancyA",    true,  {"Golem", "Golem Lord", "Lord", "Yellow"},           {"NEUTRALISPATH"})
fnCreateDialogueActor("GolemFancyB",    "Voice|GenericF11", "Root/Images/Portraits/Enemies/GolemFancyB",    true,  {"Golem", "Golem Lord", "Lord", "Black"},            {"NEUTRALISPATH"})
fnCreateDialogueActor("GolemFancyC",    "Voice|GenericF12", "Root/Images/Portraits/Enemies/GolemFancyC",    true,  {"Golem", "Golem Lord", "Lord", "Blue"},             {"NEUTRALISPATH"})
fnCreateDialogueActor("GolemFancyD",    "Voice|GenericF13", "Root/Images/Portraits/Enemies/GolemFancyD",    true,  {"Golem", "Golem Lord", "Lord", "Teal"},             {"NEUTRALISPATH"})
fnCreateDialogueActor("GolemFancyE",    "Voice|GenericF14", "Root/Images/Portraits/Enemies/GolemFancyE",    true,  {"Golem", "Golem Lord", "Lord", "White"},            {"NEUTRALISPATH"})
fnCreateDialogueActor("GolemFancyF",    "Voice|GenericF15", "Root/Images/Portraits/Enemies/GolemFancyF",    true,  {"Golem", "Golem Lord", "Lord", "Pink"},             {"NEUTRALISPATH"})
fnCreateDialogueActor("Ellie",          "Voice|Ellie",      "Root/Images/Portraits/Ellie/Neutral",          true,  {"Ellie"},                                           {"NEUTRALISPATH"})
fnCreateDialogueActor("Katarina",       "Voice|GenericF09", "Root/Images/Portraits/Katarina/Neutral",       true,  {"Lord", "Katarina"},                                {"NEUTRALISPATH"})
fnCreateDialogueActor("Amanda",         "Voice|GenericF16", "Root/Images/Portraits/NPCs/HumanNPCF1",        true,  {"Human", "745110"},                                 {"NEUTRALISPATH"})
fnCreateDialogueActor("Doll",           "Voice|GenericF17", "Root/Images/Portraits/Enemies/Doll",           true,  {"300910", "Command Unit", "1007", "Briget", "Doll", "11042", "8714"},{"NEUTRALISPATH"})
fnCreateDialogueActor("Kernel",         "Voice|GenericF08", "Root/Images/Portraits/Enemies/Doll",           true,  {"Doll", "Kernel"},                                  {"NEUTRALISPATH"})
fnCreateDialogueActor("DollInfluenced", "Voice|GenericF17", "Root/Images/Portraits/Enemies/DollInfluenced", true,  {"300910", "Command Unit"},                          {"NEUTRALISPATH"})
fnCreateDialogueActor("LatexDrone",     "Voice|GenericF08", "Root/Images/Portraits/Enemies/LatexDrone",     false, {"Drone", "Warden", "Mirabelle"},                    {"NEUTRALISPATH"})
fnCreateDialogueActor("LatexDroneB",    "Voice|GenericF08", "Root/Images/Portraits/Enemies/LatexDrone",     false, {"DroneB"},                                          {"NEUTRALISPATH"})
fnCreateDialogueActor("DarkmatterGirl", "Voice|Darkmatter", "Root/Images/Portraits/Enemies/DarkmatterGirl", false, {"Darkmatter", "Darkmatter Girl", "Zohwellb"},       {"NEUTRALISPATH"})
fnCreateDialogueActor("Steam Droid",    "Voice|GenericF14", "Root/Images/Portraits/Enemies/SteamDroid",     true,  {"AJ-99", "RI-15", "NC-88", "VN-19", "IN-12", "PP-81", "Gunner"}, {"NEUTRALISPATH"})
fnCreateDialogueActor("Electrosprite",  "Voice|GenericF16", "Root/Images/Portraits/Enemies/Electrosprite",  false, {"Sprite", "Psue"},                                  {"NEUTRALISPATH"})
fnCreateDialogueActor("201890",         "Voice|Ghost",      "Root/Images/Portraits/Enemies/201890",         true,  {"201890", "Golem", "20"},                           {"NEUTRALISPATH"})
fnCreateDialogueActor("Maisie",         "Voice|Maisie",     "Root/Images/Portraits/Maisie/Neutral",         true,  {"Maisie", "Doctor"},                                {"NEUTRALISPATH"})
fnCreateDialogueActor("Narissa",        "Voice|GenericF14", "Root/Images/Portraits/Narissa/Neutral",        true,  {"Raiju", "Narissa"},                                {"NEUTRALISPATH"})
fnCreateDialogueActor("Vivify",         "Voice|Darkmatter", "Root/Images/Portraits/Vivify/Neutral",         false, {"Vivify"},                                          {"NEUTRALISPATH"})

--Golems. All of these have Pack/No Pack varieties.
DialogueActor_Create("Golem")
	DialogueActor_SetProperty("Add Alias", "Golem")
	DialogueActor_SetProperty("Add Alias", "Golem A")
	DialogueActor_SetProperty("Add Alias", "Chip")
	DialogueActor_SetProperty("Add Alias", "Slave")
	DialogueActor_SetProperty("Add Alias", "278101")
	DialogueActor_SetProperty("Add Alias", "565102")
	DialogueActor_SetProperty("Add Alias", "Mina")
	DialogueActor_SetProperty("Add Alias", "Linda")
	DialogueActor_SetProperty("Add Alias", "Theresa")
	DialogueActor_SetProperty("Add Alias", "Teal")
	DialogueActor_SetProperty("Add Alias", "Vicks")
	DialogueActor_SetProperty("Add Alias", "Lefty")
	DialogueActor_SetProperty("Add Alias", "Ravital")
	DialogueActor_SetProperty("Add Alias", "Bess")
	DialogueActor_SetProperty("Add Emotion", "Neutral", "Root/Images/Portraits/Golem/Neutral", true)
	DialogueActor_SetProperty("Add Emotion", "Pack",    "Root/Images/Portraits/Golem/Pack",    true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "Golem", "Voice|GenericF06")
DialogueActor_Create("GolemB")
	DialogueActor_SetProperty("Add Alias", "Golem B")
	DialogueActor_SetProperty("Add Alias", "Gold")
	DialogueActor_SetProperty("Add Alias", "Wedge")
	DialogueActor_SetProperty("Add Alias", "Righty")
	DialogueActor_SetProperty("Add Emotion", "Neutral", "Root/Images/Portraits/Golem/Neutral", true)
	DialogueActor_SetProperty("Add Emotion", "Pack",    "Root/Images/Portraits/Golem/Pack",    true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "GolemB", "Voice|GenericF07")
DialogueActor_Create("GolemC")
	DialogueActor_SetProperty("Add Alias", "Golem C")
	DialogueActor_SetProperty("Add Alias", "Red")
	DialogueActor_SetProperty("Add Emotion", "Neutral", "Root/Images/Portraits/Golem/Neutral", true)
	DialogueActor_SetProperty("Add Emotion", "Pack",    "Root/Images/Portraits/Golem/Pack",    true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "GolemC", "Voice|GenericF08")

--Talos
DialogueActor_Create("Talos")
	DialogueActor_SetProperty("Add Alias", "Golem")
	DialogueActor_SetProperty("Add Alias", "Lord")
	DialogueActor_SetProperty("Add Alias", "Talos")
	DialogueActor_SetProperty("Add Emotion", "Neutral", "Root/Images/Portraits/Talos/Neutral", true)
	DialogueActor_SetProperty("Add Emotion", "Unsure",  "Root/Images/Portraits/Talos/Unsure",  true)
	DialogueActor_SetProperty("Add Emotion", "Wise",    "Root/Images/Portraits/Talos/Wise",    true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "Talos", "Voice|Talos")

--Unit 5400
DialogueActor_Create("1969")
	DialogueActor_SetProperty("Add Alias", "Command Unit")
	DialogueActor_SetProperty("Add Alias", "1969")
	DialogueActor_SetProperty("Add Emotion", "Neutral", "Root/Images/Portraits/1969/Neutral", true)
	DialogueActor_SetProperty("Add Emotion", "Smarmy",  "Root/Images/Portraits/1969/Smarmy",  true)
	DialogueActor_SetProperty("Add Emotion", "Booyeah", "Root/Images/Portraits/1969/Booyeah", true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "1969", "Voice|GenericF01")

--Secrebots
DialogueActor_Create("SecrebotY")
	DialogueActor_SetProperty("Add Alias", "Secrebot")
	DialogueActor_SetProperty("Add Emotion", "Neutral", "Root/Images/Portraits/Secrebots/SecrebotY", true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "SecrebotY", "Voice|GenericF11")
DialogueActor_Create("SecrebotP")
	DialogueActor_SetProperty("Add Alias", "Secrebot")
	DialogueActor_SetProperty("Add Emotion", "Neutral", "Root/Images/Portraits/Secrebots/SecrebotP", true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "SecrebotP", "Voice|GenericF12")

--Night
DialogueActor_Create("Night")
	DialogueActor_SetProperty("Add Alias", "Human")
	DialogueActor_SetProperty("Add Alias", "Night")
	DialogueActor_SetProperty("Add Alias", "CR-1-15")
	DialogueActor_SetProperty("Add Emotion", "Human",         "Root/Images/Portraits/Night/Human",         true)
	DialogueActor_SetProperty("Add Emotion", "Panic",         "Root/Images/Portraits/Night/Panic",         true)
	DialogueActor_SetProperty("Add Emotion", "Secrebot",      "Root/Images/Portraits/Night/Secrebot",      true)
	DialogueActor_SetProperty("Add Emotion", "SecrebotSad",   "Root/Images/Portraits/Night/SecrebotSad",   true)
	DialogueActor_SetProperty("Add Emotion", "SecrebotHappy", "Root/Images/Portraits/Night/SecrebotHappy", true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "Night", "Voice|Night")

--Rebels!
DialogueActor_Create("RebelGolem")
	DialogueActor_SetProperty("Add Alias", "Golem")
	DialogueActor_SetProperty("Add Alias", "Rebel")
	DialogueActor_SetProperty("Add Alias", "Jo")
	DialogueActor_SetProperty("Add Emotion", "Neutral", "Root/Images/Portraits/NPCs/GolemRebelScarf", true)
	DialogueActor_SetProperty("Add Emotion", "Head",    "Root/Images/Portraits/NPCs/GolemRebelHead",  true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "RebelGolem", "Voice|GenericF06")

--Administrator.
DialogueActor_Create("Administrator")
	DialogueActor_SetProperty("Add Alias", "Administrator")
	DialogueActor_SetProperty("Add Emotion", "Base",   "Root/Images/Portraits/Layered/Admin|Base",   true)
	DialogueActor_SetProperty("Add Emotion", "Layer1", "Root/Images/Portraits/Layered/Admin|Layer1", true)
	DialogueActor_SetProperty("Add Emotion", "Layer2", "Root/Images/Portraits/Layered/Admin|Layer2", true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "Administrator", "Voice|Administrator")

--Administrator as a doll in shadow. Used once.
DialogueActor_Create("AdministratorAsDoll")
	DialogueActor_SetProperty("Add Alias", "Kernel")
	DialogueActor_SetProperty("Add Emotion", "Neutral", "Root/Images/Portraits/NPCs/DollAsAdmin", true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "AdministratorAsDoll", "Voice|Administrator")

--Fake Chris
DialogueActor_Create("FakeChris")
	DialogueActor_SetProperty("Add Alias", "Guy")
	DialogueActor_SetProperty("Add Emotion", "Neutral", "Root/Images/Portraits/ChristineDialogue/Male|Neutral", true)
DL_PopActiveObject()
WD_SetProperty("Register Voice", "FakeChris", "Voice|Chris")


-- |[ ===================================== Generic Actors ===================================== ]|
--Actors from the common images not specific to any chapter. If they already exist, the call will do nothing.
fnCreateDialogueActor("HumanF0",  "Voice|GenericF03", "Root/Images/Portraits/NPCs/HumanNPCF0",  true,  {"Human"}, {"NEUTRALISPATH"})
fnCreateDialogueActor("HumanF0B", "Voice|GenericF03", "Root/Images/Portraits/NPCs/HumanNPCF0B", true,  {"Human"}, {"NEUTRALISPATH"}) --Boobalicious version
fnCreateDialogueActor("HumanF1",  "Voice|GenericF02", "Root/Images/Portraits/NPCs/HumanNPCF1",  true,  {"Human", "Norah"}, {"NEUTRALISPATH"})
fnCreateDialogueActor("HumanF1B", "Voice|GenericF02", "Root/Images/Portraits/NPCs/HumanNPCF1B", true,  {"Human"}, {"NEUTRALISPATH"}) --Boobalicious version
fnCreateDialogueActor("HumanM0",  "Voice|GenericM01", "Root/Images/Portraits/NPCs/HumanNPCM0",  true,  {"Human"}, {"NEUTRALISPATH"})
fnCreateDialogueActor("HumanM1",  "Voice|GenericM00", "Root/Images/Portraits/NPCs/HumanNPCM1",  true,  {"Human"}, {"NEUTRALISPATH"})
