-- |[ ===================================== Audio Loading ====================================== ]|
--Loads audio for this chapter.
local sBasePath = gsRoot .. "Audio/Music/"

-- |[Ambient Tracks]|
local sAmbientPath = sBasePath .. "Ambient Tracks/"
AudioManager_RegisterAdv("DatacoreAmbient", sAmbientPath .. "DatacoreAmbient.ogg", "Stream|Music|Loop:" ..  "1.000:" ..  "56.185")
AudioManager_RegisterAdv("RegulusCave",     sAmbientPath .. "RegulusCave.ogg",     "Stream|Music|Loop:" ..  "0.000:" ..   "8.000")
AudioManager_RegisterAdv("NigissuSegQuppu", sAmbientPath .. "NigissuSegQuppu.ogg", "Stream|Music|Loop:" ..  "0.000:" .. "345.080")
AudioManager_RegisterAdv("PowerCore",       sAmbientPath .. "PowerCore.ogg",       "Stream|Music|Loop:" ..  "2.000:" ..  "14.146")
AudioManager_RegisterAdv("FactoryFloor",    sAmbientPath .. "FactoryFloor.ogg",    "Stream|Music|Loop:" ..  "3.000:" ..  "18.000")

-- |[Battle Themes]|
local sBattlePath = sBasePath .. "Battle Themes/"
AudioManager_RegisterAdv("BattleThemeChristine",      sBattlePath .. "Battle Theme Christine.ogg",                          "Stream|Music|Loop:" ..  "6.371:" ..  "96.367")
AudioManager_RegisterAdv("MotherTheme",               sBattlePath .. "ThemeOfTheMother.ogg",                                "Stream|Music|Loop:" ..  "0.000:" ..  "63.985")
AudioManager_RegisterAdv("RottenTheme",               sBattlePath .. "ThemeOfTheRotten.ogg",                                "Stream|Music|Loop:" ..  "2.000:" ..  "93.195")
AudioManager_RegisterAdv("BattleThemeChristineKazoo", sBattlePath .. "Battle Theme Christine Hope Youre Happy Kumquat.ogg", "Stream|Music|Loop:" ..  "6.371:" ..  "96.367")

-- |[Character Themes]|
local sCharThemePath = sBasePath .. "Character Themes/"
AudioManager_RegisterAdv("TiffanysTheme",     sCharThemePath .. "Tiffanys Theme.ogg",          "Stream|Music|Loop:" .. "36.514:" .. "131.656")
AudioManager_RegisterAdv("BreannesTheme",     sCharThemePath .. "Breannes Theme.ogg",          "Stream|Music|Loop:" .. "13.735:" ..  "96.459")
AudioManager_RegisterAdv("SophiesTheme",      sCharThemePath .. "Sophies Theme.ogg",           "Stream|Music|Loop:" ..  "8.990:" ..  "91.416")
AudioManager_RegisterAdv("SophiesThemeSlow",  sCharThemePath .. "Sophies Theme Slow.ogg",      "Stream|Music|Loop:" ..  "0.000:" ..  "45.806")
AudioManager_RegisterAdv("Vivify",            sCharThemePath .. "Vivify.ogg",                  "Stream|Music|Loop:" .. "11.529:" ..  "39.620")
AudioManager_RegisterAdv("SophieToTheRescue", sCharThemePath .. "Sophie to the Rescue.ogg",    "Stream|Music|Loop:" .. "16.191:" ..  "53.015")
AudioManager_RegisterAdv("SophiesIntro",      sCharThemePath .. "Sophies Theme Slow Open.ogg", "Stream|Music")

-- |[Layered Tracks]|
local sLayeredPath = sBasePath .. "Layered Tracks/" 
AudioManager_RegisterAdv("GalaTenseLayer",        sLayeredPath .. "GalaTension.ogg",      "Stream|Music|Loop:" ..  "0.000:" .. "67.450")
AudioManager_RegisterAdv("GalaPianoDistantLayer", sLayeredPath .. "GalaPianoDistant.ogg", "Stream|Music|Loop:" ..  "0.000:" .. "67.450")
AudioManager_RegisterAdv("GalaPianoLayer",        sLayeredPath .. "GalaPiano.ogg",        "Stream|Music|Loop:" ..  "0.000:" .. "67.450")
AudioManager_RegisterAdv("TelecommLayer0",        sLayeredPath .. "Telecomm0.ogg",        "Stream|Music|Loop:" .. "20.000:" .. "41.328")
AudioManager_RegisterAdv("TelecommLayer1",        sLayeredPath .. "Telecomm1.ogg",        "Stream|Music|Loop:" .. "20.000:" .. "41.328")
AudioManager_RegisterAdv("TelecommLayer2",        sLayeredPath .. "Telecomm2.ogg",        "Stream|Music|Loop:" .. "20.000:" .. "41.328")
AudioManager_RegisterAdv("Admin0",                sLayeredPath .. "Admin0.ogg",           "Stream|Music|Loop:" ..  "0.000:" .. "19.197")
AudioManager_RegisterAdv("Admin1",                sLayeredPath .. "Admin1.ogg",           "Stream|Music|Loop:" ..  "0.000:" .. "19.197")
AudioManager_RegisterAdv("Admin2",                sLayeredPath .. "Admin2.ogg",           "Stream|Music|Loop:" ..  "0.000:" .. "19.197")
AudioManager_RegisterAdv("Admin3",                sLayeredPath .. "Admin3.ogg",           "Stream|Music|Loop:" ..  "0.000:" .. "19.197")

-- |[Misc Tracks]|
local sMiscPath = sBasePath .. "Misc Tracks/"
AudioManager_RegisterAdv("Briefing",       sMiscPath .. "Briefing.ogg",         "Stream|Music|Loop:" ..   "2.432:" ..  "33.057")
AudioManager_RegisterAdv("Waltz",          sMiscPath .. "WaltzNo2.ogg",         "Stream|Music")
AudioManager_RegisterAdv("TimeSensitive",  sMiscPath .. "TimeSensitive.ogg",    "Stream|Music|Loop:" .. "127.561:" .. "154.408")

-- |[World Themes]|
local sWorldPath = sBasePath .. "World Themes/"
AudioManager_RegisterAdv("Biolabs",              sWorldPath .. "Biolabs.ogg",                "Stream|Music|Loop:" .. "46.553:" .. "184.799")
AudioManager_RegisterAdv("ChristineDream",       sWorldPath .. "ChristineDream.ogg",         "Stream|Music|Loop:" ..  "1.961:" ..  "96.000")
AudioManager_RegisterAdv("EquinoxTheme",         sWorldPath .. "EquinoxTheme.ogg",           "Stream|Music|Loop:" ..  "9.101:" .. "232.556")
AudioManager_RegisterAdv("RegulusCity",          sWorldPath .. "Regulus City.ogg",           "Stream|Music|Loop:" ..  "4.673:" ..  "67.384")
AudioManager_RegisterAdv("RegulusContainment",   sWorldPath .. "RegulusContainment.ogg",     "Stream|Music|Loop:" .. "10.408:" .. "127.790")
AudioManager_RegisterAdv("RegulusTense",         sWorldPath .. "RegulusTense.ogg",           "Stream|Music|Loop:" .. "31.503:" .. "143.504")
AudioManager_RegisterAdv("SerenityShort",        sWorldPath .. "SerenityShort.ogg",          "Stream|Music|Loop:" ..  "1.500:" ..   "6.497")
AudioManager_RegisterAdv("SerenityObservatory",  sWorldPath .. "SerenityObservatory.ogg",    "Stream|Music|Loop:" .. "14.983:" ..  "60.001")
AudioManager_RegisterAdv("SerenityCrater",       sWorldPath .. "SerenityCrater.ogg",         "Stream|Music|Loop:" ..  "0.000:" .. "137.485")
AudioManager_RegisterAdv("SerenityTense",        sWorldPath .. "SerenityTense.ogg",          "Stream|Music|Loop:" .. "14.534:" ..  "49.836")
AudioManager_RegisterAdv("SprocketCity",         sWorldPath .. "Sprocket City.ogg",          "Stream|Music|Loop:" ..  "2.423:" .. "143.021")
AudioManager_RegisterAdv("Workout",              sWorldPath .. "Workout.ogg",                "Stream|Music|Loop:" ..  "0.000:" .. " 31.983")

-- |[Some Moths Do]|
--The exciting ACTION MOVIE from Chapter 5.
local sMothPath = sBasePath .. "Some Moths Do/"
AudioManager_RegisterAdv("Moth Catchup",   sMothPath .. "MothCatchUp.ogg",   "Stream|Music")
AudioManager_RegisterAdv("Moth Passive",   sMothPath .. "MothPassive.ogg",   "Stream|Music|Loop:" .. "3.005:" .. "36.501")
AudioManager_RegisterAdv("Moth Intense",   sMothPath .. "MothIntense.ogg",   "Stream|Music|Loop:" .. "4.000:" .. "83.009")
AudioManager_RegisterAdv("Moth Speedboat", sMothPath .. "MothSpeedboat.ogg", "Stream|Music|Loop:" .. "2.000:" ..  "8.009")

-- |[Character Callouts]|
local sVoicePath = gsRoot .. "Audio/VoiceCallouts/"
LM_ExecuteScript(sVoicePath .. "Chris/Z Build Class.lua")
LM_ExecuteScript(sVoicePath .. "Christine/Z Build Class.lua")
LM_ExecuteScript(sVoicePath .. "Tiffany/Z Build Class.lua")
LM_ExecuteScript(sVoicePath .. "SX399/Z Build Class.lua")
LM_ExecuteScript(sVoicePath .. "JX101/Z Build Class.lua")