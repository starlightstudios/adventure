-- |[ =================================== Chapter 5 Loading ==================================== ]|
--This file loads assets unique to chapter 5. Subfiles are used where necessary.

-- |[Common]|
--Open the datafile.
SLF_Open(gsDatafilesPath .. "Sprites.slf")

--Modify the distance filters to keep everything pixellated. This is only for sprites.
local iNearest = DM_GetEnumeration("GL_NEAREST")
ALB_SetTextureProperty("MagFilter", iNearest)
ALB_SetTextureProperty("MinFilter", iNearest)
ALB_SetTextureProperty("Special Sprite Padding", true)
--Bitmap_ActivateAtlasing()

-- |[ ======================================= Autoloader ======================================= ]|
--Execute chapter 5's autoloading list.
fnExecAutoload("AutoLoad|Adventure|Sprites_Ch5")

-- |[ ===================================== Loading Lists ====================================== ]|
--Lists of standardized NPC load patterns.
DL_AddPath("Root/Images/Sprites/Special/")

-- |[ =========== Party Characters =========== ]|
-- |[Christine]|
--Autoloads.
fnExecAutoload("AutoLoad|Adventure|SpritesChristine|Human")
fnExecAutoload("AutoLoad|Adventure|SpritesChristine|Darkmatter")
fnExecAutoload("AutoLoad|Adventure|SpritesChristine|Doll")
fnExecAutoload("AutoLoad|Adventure|SpritesChristine|Raiju")
fnExecAutoload("AutoLoad|Adventure|SpritesChristine|RaijuNude")
fnExecAutoload("AutoLoad|Adventure|SpritesChristine|Dreamer")
fnExecAutoload("AutoLoad|Adventure|SpritesChristine|Latex")
fnExecAutoload("AutoLoad|Adventure|SpritesChristine|Electrosprite")
fnExecAutoload("AutoLoad|Adventure|SpritesChristine|Golem")
fnExecAutoload("AutoLoad|Adventure|SpritesChristine|Secrebot")
fnExecAutoload("AutoLoad|Adventure|SpritesChristine|SteamDroid")

--Manual.
local sChristineList   = {"Human", "Darkmatter", "DreamGirl", "Electrosprite", "Golem", "GolemDress", "Latex", "Male", "SteamDroid", "Raibie", "Raiju", "RaijuClothes", "Doll", "Secrebot"}
local sChristineCWList = {"Christine_Darkmatter", "Christine_DreamGirl", "Christine_Electrosprite", "Christine_Golem", "Christine_Human", "Christine_Latex", "Christine_Male", "Christine_Raibie", "Christine_Raiju", "Christine_RaijuClothes", "Christine_GolemDress", "Christine_Secrebot"}
fnExtractSpriteList   (sChristineList, true, "Christine_")
fnExtractSpecialFrames(sChristineCWList, "Crouch", "Wounded")

--Non-standard special frames.
DL_ExtractBitmap("Spcl|Christine_Golem|Laugh0",   "Root/Images/Sprites/Special/Christine|Laugh0")
DL_ExtractBitmap("Spcl|Christine_Golem|Laugh1",   "Root/Images/Sprites/Special/Christine|Laugh1")
DL_ExtractBitmap("Spcl|Christine_Golem|Sad",      "Root/Images/Sprites/Special/Christine|Sad")
DL_ExtractBitmap("Spcl|Christine_Human|BedSleep", "Root/Images/Sprites/Special/Christine_Human|BedSleep")
DL_ExtractBitmap("Spcl|Christine_Human|BedWake",  "Root/Images/Sprites/Special/Christine_Human|BedWake")
DL_ExtractBitmap("Spcl|Christine_Human|BedFull",  "Root/Images/Sprites/Special/Christine_Human|BedFull")
DL_ExtractBitmap("Spcl|Christine_Human|BedWakeR", "Root/Images/Sprites/Special/Christine_Human|BedWakeR")
DL_ExtractBitmap("Spcl|Christine_Human|BedFullR", "Root/Images/Sprites/Special/Christine_Human|BedFullR")
DL_ExtractBitmap("Spcl|Christine_Doll|FlatbackC", "Root/Images/Sprites/Special/Christine_Doll|FlatbackC")
DL_ExtractBitmap("Spcl|Christine_Doll|FlatbackO", "Root/Images/Sprites/Special/Christine_Doll|FlatbackO")

--Christine and Sophie. Placed under Christine's set.
for i = 0, 9, 1 do
	DL_ExtractBitmap("Spcl|ChristineSophie|HoldHands" .. i, "Root/Images/Sprites/Special/ChristineSophie|HoldHands" .. i)
end
for i = 0, 7, 1 do
	DL_ExtractBitmap("Spcl|ChristineSophie|Kiss" .. i, "Root/Images/Sprites/Special/ChristineSophie|Kiss" .. i)
end

-- |[Tiffany]|
local sTiffanyList = {"Tiffany", "TiffanySundress"}
fnExtractSpriteList   (sTiffanyList, true)
fnExtractSpecialFrames(sTiffanyList, "Crouch", "Wounded")

--Non-standard special frames.
DL_ExtractBitmap("Spcl|Tiffany|Chair0", "Root/Images/Sprites/Special/Tiffany|Chair0")
DL_ExtractBitmap("Spcl|Tiffany|Chair1", "Root/Images/Sprites/Special/Tiffany|Chair1")
DL_ExtractBitmap("Spcl|Tiffany|Chair2", "Root/Images/Sprites/Special/Tiffany|Chair2")
DL_ExtractBitmap("Spcl|Tiffany|Chair3", "Root/Images/Sprites/Special/Tiffany|Chair3")
DL_ExtractBitmap("Spcl|Tiffany|Chair4", "Root/Images/Sprites/Special/Tiffany|Chair4")
DL_ExtractBitmap("Spcl|Tiffany|Downed", "Root/Images/Sprites/Special/Tiffany|Downed")
DL_ExtractBitmap("Spcl|Tiffany|Stealth","Root/Images/Sprites/Special/Tiffany|Stealth")
DL_ExtractBitmap("Spcl|Tiffany|Draw0",  "Root/Images/Sprites/Special/Tiffany|Draw0")
DL_ExtractBitmap("Spcl|Tiffany|Draw1",  "Root/Images/Sprites/Special/Tiffany|Draw1")
DL_ExtractBitmap("Spcl|Tiffany|Draw2",  "Root/Images/Sprites/Special/Tiffany|Draw2")
DL_ExtractBitmap("Spcl|Tiffany|Exec0",  "Root/Images/Sprites/Special/Tiffany|Exec0")
DL_ExtractBitmap("Spcl|Tiffany|Exec1",  "Root/Images/Sprites/Special/Tiffany|Exec1")
DL_ExtractBitmap("Spcl|Tiffany|Exec2",  "Root/Images/Sprites/Special/Tiffany|Exec2")
DL_ExtractBitmap("Spcl|Tiffany|Exec3",  "Root/Images/Sprites/Special/Tiffany|Exec3")
DL_ExtractBitmap("Spcl|Tiffany|Exec4",  "Root/Images/Sprites/Special/Tiffany|Exec4")
DL_ExtractBitmap("Spcl|Tiffany|Exec5",  "Root/Images/Sprites/Special/Tiffany|Exec5")
DL_ExtractBitmap("Spcl|Tiffany|Heavy0", "Root/Images/Sprites/Special/Tiffany|Heavy0")
DL_ExtractBitmap("Spcl|Tiffany|Heavy1", "Root/Images/Sprites/Special/Tiffany|Heavy1")
DL_ExtractBitmap("Spcl|Tiffany|Heavy2", "Root/Images/Sprites/Special/Tiffany|Heavy2")
DL_ExtractBitmap("Spcl|Tiffany|Heavy3", "Root/Images/Sprites/Special/Tiffany|Heavy3")

--Autoloading.
fnExecAutoload("AutoLoad|Adventure|SpritesTiffany|Normal")
fnExecAutoload("AutoLoad|Adventure|SpritesTiffany|Sundress")

-- |[SX-399]|
--Autoloads.
fnExecAutoload("AutoLoad|Adventure|SpritesSX399|Lord")

--Manual.
local saSX399List = {"SX-399Lord"}
fnExtractSpriteList   (saSX399List, true)
fnExtractSpecialFrames(saSX399List, "Crouch", "Wounded")

--Non-standard special frames.
DL_ExtractBitmap("Spcl|SX-399Lord|Laugh0", "Root/Images/Sprites/Special/SX-399Lord|Laugh0")
DL_ExtractBitmap("Spcl|SX-399Lord|Laugh1", "Root/Images/Sprites/Special/SX-399Lord|Laugh1")

-- |[Sophie]|
local saSophieList = {"Sophie", "SophieDress", "SophieLeather"}
fnExtractSpriteList(saSophieList, true)
--No crouch/wounded

--Non-standard special frames.
DL_ExtractBitmap("Spcl|Sophie|Laugh0", "Root/Images/Sprites/Special/Sophie|Laugh0")
DL_ExtractBitmap("Spcl|Sophie|Laugh1", "Root/Images/Sprites/Special/Sophie|Laugh1")
DL_ExtractBitmap("Spcl|Sophie|Cry0",   "Root/Images/Sprites/Special/Sophie|Cry0")
DL_ExtractBitmap("Spcl|Sophie|Cry1",   "Root/Images/Sprites/Special/Sophie|Cry1")

-- |[Sammy]|
local saSammyList = {"Sammy"}
fnExtractSpriteList(saSammyList, true)
--No crouch/wounded

--Sammy's dancing frames.
for i = 0, 7, 1 do
    DL_ExtractBitmap("SammyDanceA|" .. i, "Root/Images/Sprites/Special/SammyDanceA|" .. i)
    DL_ExtractBitmap("SammyDanceB|" .. i, "Root/Images/Sprites/Special/SammyDanceB|" .. i)
    DL_ExtractBitmap("SammyDanceC|" .. i, "Root/Images/Sprites/Special/SammyDanceC|" .. i)
    DL_ExtractBitmap("SammyDanceD|" .. i, "Root/Images/Sprites/Special/SammyDanceD|" .. i)
    DL_ExtractBitmap("SammyDanceE|" .. i, "Root/Images/Sprites/Special/SammyDanceE|" .. i)
    DL_ExtractBitmap("SammyDanceF|" .. i, "Root/Images/Sprites/Special/SammyDanceF|" .. i)
    DL_ExtractBitmap("SammyDanceG|" .. i, "Root/Images/Sprites/Special/SammyDanceG|" .. i)
    DL_ExtractBitmap("SammyDanceH|" .. i, "Root/Images/Sprites/Special/SammyDanceH|" .. i)
end

--Sammy's swimming frames.
DL_ExtractBitmap("SammySwimS", "Root/Images/Sprites/Special/SammySwimS")
DL_ExtractBitmap("SammySwimW", "Root/Images/Sprites/Special/SammySwimW")


-- |[ =========== Named Characters =========== ]|
-- |[Four Direction Characters]|
--Four-direction major characters.
local saMajorNPCList = {"20", "609144", "CassandraH", "CassandraG", "Maisie", "Ellie", "Katarina", "Vivify", "VivifyBlack", "NightHuman", "NightSecrebot", "Talos", "Boombox", "1969"}
fnExtractSpriteList(saMajorNPCList, false)

--Wounded frames for four-direction characters.
fnExtractSpecialFrames({"CassandraH"}, "Wounded")

--Vivify special frames.
DL_ExtractBitmap("VivifyFreeze0",  "Root/Images/Sprites/Special/Vivify|Freeze0")
DL_ExtractBitmap("VivifyFreeze1",  "Root/Images/Sprites/Special/Vivify|Freeze1")
DL_ExtractBitmap("VivifyFreeze2",  "Root/Images/Sprites/Special/Vivify|Freeze2")
DL_ExtractBitmap("VivifyFreeze3",  "Root/Images/Sprites/Special/Vivify|Freeze3")
DL_ExtractBitmap("VivifyFreeze4",  "Root/Images/Sprites/Special/Vivify|Freeze4")

--Workout frames.
DL_ExtractBitmap("Spcl|1969|Workout0", "Root/Images/Sprites/Special/1969|Workout0")
DL_ExtractBitmap("Spcl|1969|Workout1", "Root/Images/Sprites/Special/1969|Workout1")
DL_ExtractBitmap("Spcl|1969|Workout2", "Root/Images/Sprites/Special/1969|Workout2")
DL_ExtractBitmap("Spcl|1969|Workout3", "Root/Images/Sprites/Special/1969|Workout3")
DL_ExtractBitmap("Spcl|Boombox|Song0", "Root/Images/Sprites/Special/Boombox|Song0")
DL_ExtractBitmap("Spcl|Boombox|Song1", "Root/Images/Sprites/Special/Boombox|Song1")
DL_ExtractBitmap("Spcl|Boombox|Song2", "Root/Images/Sprites/Special/Boombox|Song2")
DL_ExtractBitmap("Spcl|Boombox|Song3", "Root/Images/Sprites/Special/Boombox|Song3")

--Night working frames.
DL_ExtractBitmap("Spcl|Night|Dust0", "Root/Images/Sprites/Special/NightSecrebot|Dust0")
DL_ExtractBitmap("Spcl|Night|Dust1", "Root/Images/Sprites/Special/NightSecrebot|Dust1")
DL_ExtractBitmap("Spcl|Night|Dust2", "Root/Images/Sprites/Special/NightSecrebot|Dust2")
DL_ExtractBitmap("Spcl|Night|Dust3", "Root/Images/Sprites/Special/NightSecrebot|Dust3")
DL_ExtractBitmap("Spcl|Night|Mop0",  "Root/Images/Sprites/Special/NightSecrebot|Mop0")
DL_ExtractBitmap("Spcl|Night|Mop1",  "Root/Images/Sprites/Special/NightSecrebot|Mop1")
DL_ExtractBitmap("Spcl|Night|Mop2",  "Root/Images/Sprites/Special/NightSecrebot|Mop2")
DL_ExtractBitmap("Spcl|Night|Mop3",  "Root/Images/Sprites/Special/NightSecrebot|Mop3")

-- |[Eight Direction Characters]|
--Eight-direction major characters.
local saOtherEightList = {"DarkmatterGirl", "DarkmatterGirlParagon", "56", "SX-399", "JX-101"}
fnExtractSpriteList(saOtherEightList, true)

--Wounded frames for eight-direction characters.
fnExtractSpecialFrames({"DarkmatterGirl", "DarkmatterGirlParagon"}, "Wounded")

-- |[ ============= Generic NPCs ============= ]|
--"NPC" lists. Entities that have 4-directional movement and a "Wounded" frame, but are meant to be used for town characters.
-- Notably, these do NOT have paragon equivalents.
local saGenericNPCList = {"Alraune", "DollGrn", "DollPrp", "DollInfluenced", "Electrosprite", "GenericF0", "GenericF1", "GenericM0", "GenericM1", "GenericBiolabsFBlue", "GenericBiolabsFRed", "GenericBiolabsFGreen", "GenericBiolabsFYellow", "GenericBiolabsMBlue", "GenericBiolabsMRed", "GenericBiolabsMGreen", "GenericBiolabsMYellow", "GolemLordB", "GolemLordC", "GolemLordD", "GolemSlave", "GolemSlaveR", "LatexBlonde", "LatexBrown", "LatexRed", "Raiju", "SteamDroid", "Tram"}
fnExtractSpriteList   (saGenericNPCList, false)
--fnExtractSpecialFrames(gcsaGenericNPCList, "Wounded")

--Alraune wounded frame.
DL_ExtractBitmap("Spcl|Alraune|Wounded", "Root/Images/Sprites/Special/Alraune|Wounded")

--Civilian Golem special frames.
DL_ExtractBitmap("Spcl|GolemSlave|Wounded", "Root/Images/Sprites/Special/GolemSlave|Wounded")

--Rebel Golem special frames.
DL_ExtractBitmap("Spcl|GolemSlaveR|Wounded", "Root/Images/Sprites/Special/GolemSlaveR|Wounded")
DL_ExtractBitmap("Spcl|GolemSlaveR|Sitting", "Root/Images/Sprites/Special/GolemSlaveR|Sitting")

--Raiju Workout
DL_ExtractBitmap("Spcl|RaijuWorkout|Kick0", "Root/Images/Sprites/Special/Raiju|Kick0")
DL_ExtractBitmap("Spcl|RaijuWorkout|Kick1", "Root/Images/Sprites/Special/Raiju|Kick1")
DL_ExtractBitmap("Spcl|RaijuWorkout|Kick2", "Root/Images/Sprites/Special/Raiju|Kick2")
DL_ExtractBitmap("Spcl|RaijuWorkout|Kick3", "Root/Images/Sprites/Special/Raiju|Kick3")

--Raiju Dance
DL_ExtractBitmap("Spcl|RaijuWorkout|Dance0", "Root/Images/Sprites/Special/Raiju|Dance0")
DL_ExtractBitmap("Spcl|RaijuWorkout|Dance1", "Root/Images/Sprites/Special/Raiju|Dance1")
DL_ExtractBitmap("Spcl|RaijuWorkout|Dance2", "Root/Images/Sprites/Special/Raiju|Dance2")
DL_ExtractBitmap("Spcl|RaijuWorkout|Dance3", "Root/Images/Sprites/Special/Raiju|Dance3")

-- |[ ================ Enemies =============== ]|
--"Enemy" Lists. This is a list of entities that have 4-directional movement and a "Wounded" frame. Any sprite following this
-- format can be used for an enemy. These must have paragon alternates.
local saChapter0EnemyList = {}
local saChapter5EnemyList = {"Doll", "GolemLordA", "GolemSlaveP", "EldritchDream", "Horrible", "Hoodie", "InnGeisha", "BandageImp", "LatexDrone", "Plannerbot", "Scraprat", "Secrebot", "SecurityBot", "SecurityBotBroke", "Raibie", "VoidRift", "Motilvac", "MrWipey", "SecrebotPink", "SecrebotYellow"}
fnExtractSpriteList   (saChapter0EnemyList, false)
fnExtractSpecialFrames(saChapter0EnemyList, "Wounded")
fnExtractSpriteList   (saChapter5EnemyList, false)
fnExtractSpecialFrames(saChapter5EnemyList, "Wounded")

--Paragons. These are alt-color versions of an enemy that are tougher. These just have "Paragon" tacked on the end.
local saParagon0List = {}
local saParagon5List = {}
for i = 1, #saChapter0EnemyList, 1 do
    saParagon0List[i] = saChapter0EnemyList[i] .. "Paragon"
end
for i = 1, #saChapter5EnemyList, 1 do
    saParagon5List[i] = saChapter5EnemyList[i] .. "Paragon"
end
fnExtractSpriteList   (saParagon0List, false)
fnExtractSpecialFrames(saParagon0List, "Wounded")
fnExtractSpriteList   (saParagon5List, false)
fnExtractSpecialFrames(saParagon5List, "Wounded")

--Scraprat explosion special frames.
for i = 0, 11, 1 do
	local sName = "Explode"
	if(i < 10) then sName = sName .. "0" end
	sName = sName .. i
	DL_ExtractBitmap("Spcl|Scraprat|" .. sName, "Root/Images/Sprites/Special/Scraprat|" .. sName)
end

--Mutant Golem. Boss monster in the black site.
DL_ExtractBitmap("Spcl|GolemSlave|Mutant", "Root/Images/Sprites/Special/GolemSlave|Mutant")

--Latex Drone's Arm Falls Off. Used in the biolabs movie event.
DL_ExtractBitmap("Spcl|LatexDrone|Arm0", "Root/Images/Sprites/Special/LatexDrone|Arm0")
DL_ExtractBitmap("Spcl|LatexDrone|Arm1", "Root/Images/Sprites/Special/LatexDrone|Arm1")
DL_ExtractBitmap("Spcl|LatexDrone|Arm2", "Root/Images/Sprites/Special/LatexDrone|Arm2")
DL_ExtractBitmap("Spcl|LatexDrone|Arm3", "Root/Images/Sprites/Special/LatexDrone|Arm3")
DL_ExtractBitmap("Spcl|LatexDrone|Arm4", "Root/Images/Sprites/Special/LatexDrone|Arm4")

-- |[ ============ Half-Frame NPCs =========== ]|
--Half-Frame Lists. Used for some NPCs with two-directional movement, like sheep and chickens.
local saHalfFrameList = {"Sheep", "Chicken"}
fnExtractSpriteListEW(saHalfFrameList)

-- |[ ================================= Misc Character Sprites ================================= ]|
-- |[Crowbar Chan]|
--*Nyu intensifies*
DL_AddPath("Root/Images/Sprites/CrowbarChan/")
DL_ExtractBitmap("CrowbarChan|0", "Root/Images/Sprites/CrowbarChan/0")
DL_ExtractBitmap("CrowbarChan|1", "Root/Images/Sprites/CrowbarChan/1")
DL_ExtractBitmap("CrowbarChan|2", "Root/Images/Sprites/CrowbarChan/2")

-- |[Gala Lord Golems]|
--These NPCs only have facing frames, no walking frames.
local sCurrentLetter = "A"
while(true) do
    
    --Add the remap.
    local sNPCName = "GolemFancy" .. sCurrentLetter
    fnLoadCharacterGraphicsFacing(sNPCName, "Root/Images/Sprites/" .. sNPCName .. "/")
    
    --If this was the ending letter, or the letter 'Z', stop.
    if(sCurrentLetter == "X" or sCurrentLetter == "Z") then break end
    
    --Move to the next letter.
    local iByteValue = string.byte(sCurrentLetter)
    sCurrentLetter = string.char(iByteValue + 1)
end

-- |[ ================================= Misc Character Sprites ================================= ]|
-- |[Bullet Impacts]|
DL_AddPath("Root/Images/Sprites/Impacts/")
DL_ExtractBitmap("Impact|Bullet0", "Root/Images/Sprites/Impacts/Bullet0")
DL_ExtractBitmap("Impact|Bullet1", "Root/Images/Sprites/Impacts/Bullet1")
DL_ExtractBitmap("Impact|Bullet2", "Root/Images/Sprites/Impacts/Bullet2")
DL_ExtractBitmap("Impact|Bullet3", "Root/Images/Sprites/Impacts/Bullet3")

-- |[Static Burst]|
DL_AddPath("Root/Images/Sprites/StaticBurst/")
for i = 0, 12, 1 do
    DL_ExtractBitmap("Spcl|StaticAnim" .. i, "Root/Images/Sprites/StaticBurst/" .. i)
end

--Clear this flag.
--Bitmap_DeactivateAtlasing()
ALB_SetTextureProperty("Special Sprite Padding", false)

-- |[ ================================= Running Away Minigame ================================== ]|
DL_AddPath("Root/Images/Sprites/RunningMinigame/")
DL_ExtractBitmap("Run|All",       "Root/Images/Sprites/RunningMinigame/All")
DL_ExtractBitmap("Run|Tutorial0", "Root/Images/Sprites/RunningMinigame/Tutorial0")
DL_ExtractBitmap("Run|Tutorial1", "Root/Images/Sprites/RunningMinigame/Tutorial1")
DL_ExtractBitmap("Run|Tutorial2", "Root/Images/Sprites/RunningMinigame/Tutorial2")
DL_ExtractBitmap("Run|Tutorial3", "Root/Images/Sprites/RunningMinigame/Tutorial3")
DL_ExtractBitmap("Run|Tutorial4", "Root/Images/Sprites/RunningMinigame/Tutorial4")
DL_ExtractBitmap("Run|Tutorial5", "Root/Images/Sprites/RunningMinigame/Tutorial5")

ALB_SetTextureProperty("Restore Defaults")

SLF_Close()
