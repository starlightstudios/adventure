-- |[ ==================================== Chapter 5 Scenes ==================================== ]|
--Scenes for chapter 5. This includes TF scenes and runestone flashes.

-- |[Streaming List]|
--Create an "Everything" list. All delayed images not in another list get put in this list and loaded
-- as soon as possible, but not during the load sequence.
--Because scene images tend to be really big, this list *should* be empty in an optimized program.
local sEverythingList = "Chapter 5 Scenes"
fnCreateDelayedLoadList(sEverythingList)

-- |[ =============================== Cassandra's Transformations ============================== ]|
--She shows up a lot. Suspicious, isn't it?
local sCassandraImages = "Chapter 5 Cassandra"
fnCreateDelayedLoadList(sCassandraImages)

--Open, create.
SLF_Open(gsaDatafilePaths.sScnCassandraTF)
DL_AddPath("Root/Images/Scenes/Cassandra/")

--Neutral
fnExtractDelayedBitmapToList(sCassandraImages, "Cassandra|Neutral",    "Root/Images/Scenes/Cassandra/Neutral")
fnExtractDelayedBitmapToList(sCassandraImages, "Cassandra|GolemTF0",   "Root/Images/Scenes/Cassandra/GolemTF0")
fnExtractDelayedBitmapToList(sCassandraImages, "Cassandra|GolemTF1",   "Root/Images/Scenes/Cassandra/GolemTF1")
fnExtractDelayedBitmapToList(sCassandraImages, "Cassandra|GolemTF2",   "Root/Images/Scenes/Cassandra/GolemTF2")
fnExtractDelayedBitmapToList(sCassandraImages, "Cassandra|GolemTF3",   "Root/Images/Scenes/Cassandra/GolemTF3")

-- |[ =============================== Christine's Transformations ============================== ]|
--Each scene gets its own TF grouping.
SLF_Open(gsaDatafilePaths.sScnChristineTF)
DL_AddPath("Root/Images/Scenes/Christine/")

--Chris to Christine to Golem. Six parts.
local sTFCluster = "Chapter 5 Christine Golem TF"
fnCreateDelayedLoadList(sTFCluster)
fnExtractDelayedBitmapToList(sTFCluster, "Christine|GolemTF0", "Root/Images/Scenes/Christine/GolemTF0")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|GolemTF1", "Root/Images/Scenes/Christine/GolemTF1")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|GolemTF2", "Root/Images/Scenes/Christine/GolemTF2")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|GolemTF3", "Root/Images/Scenes/Christine/GolemTF3")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|GolemTF4", "Root/Images/Scenes/Christine/GolemTF4")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|GolemTF5", "Root/Images/Scenes/Christine/GolemTF5")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|Male",     "Root/Images/Scenes/Christine/MaleNeutral")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|Female",   "Root/Images/Scenes/Christine/FemaleNeutral")

--Christine to Doll TF.
sTFCluster = "Chapter 5 Christine Doll TF"
fnCreateDelayedLoadList(sTFCluster)
fnExtractDelayedBitmapToList(sTFCluster, "Christine|DollTF0", "Root/Images/Scenes/Christine/DollTF0")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|DollTF1", "Root/Images/Scenes/Christine/DollTF1")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|DollTF2", "Root/Images/Scenes/Christine/DollTF2")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|DollTF3", "Root/Images/Scenes/Christine/DollTF3")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|DollTF4", "Root/Images/Scenes/Christine/DollTF4")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|DollTF5", "Root/Images/Scenes/Christine/DollTF5")

--Latex Drone TF.
sTFCluster = "Chapter 5 Christine Drone TF"
fnCreateDelayedLoadList(sTFCluster)
fnExtractDelayedBitmapToList(sTFCluster, "Christine|LatexTF0", "Root/Images/Scenes/Christine/LatexTF0")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|LatexTF1", "Root/Images/Scenes/Christine/LatexTF1")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|LatexTF2", "Root/Images/Scenes/Christine/LatexTF2")
fnAppendDelayedBitmapToList (sTFCluster, "Party|Christine_Human", "Root/Images/Portraits/Combat/Christine_Human")

--Raiju TF.
sTFCluster = "Chapter 5 Christine Raiju TF"
fnCreateDelayedLoadList(sTFCluster)
fnExtractDelayedBitmapToList(sTFCluster, "Christine|RaijuTF0", "Root/Images/Scenes/Christine/RaijuTF0")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|RaijuTF1", "Root/Images/Scenes/Christine/RaijuTF1")

--Steam Droid TF.
sTFCluster = "Chapter 5 Christine SteamDroid TF"
fnCreateDelayedLoadList(sTFCluster)
fnExtractDelayedBitmapToList(sTFCluster, "Christine|SteamTF0", "Root/Images/Scenes/Christine/SteamTF0")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|SteamTF1", "Root/Images/Scenes/Christine/SteamTF1")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|SteamTF2", "Root/Images/Scenes/Christine/SteamTF2")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|SteamTF3", "Root/Images/Scenes/Christine/SteamTF3")

--Christine to Eldritch Dreamer
sTFCluster = "Chapter 5 Christine Dreamer TF"
fnCreateDelayedLoadList(sTFCluster)
fnExtractDelayedBitmapToList(sTFCluster, "Christine|DreamerTF0", "Root/Images/Scenes/Christine/DreamerTF0")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|DreamerTF1", "Root/Images/Scenes/Christine/DreamerTF1")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|DreamerTF2", "Root/Images/Scenes/Christine/DreamerTF2")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|DreamerTF3", "Root/Images/Scenes/Christine/DreamerTF3")

--Electrosprite
sTFCluster = "Chapter 5 Christine Electrosprite TF"
fnCreateDelayedLoadList(sTFCluster)
fnExtractDelayedBitmapToList(sTFCluster, "Christine|ElectrospriteTF0", "Root/Images/Scenes/Christine/ElectrospriteTF0")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|ElectrospriteTF1", "Root/Images/Scenes/Christine/ElectrospriteTF1")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|ElectrospriteTF2", "Root/Images/Scenes/Christine/ElectrospriteTF2")

--Darkmatter
sTFCluster = "Chapter 5 Christine Darkmatter TF"
fnCreateDelayedLoadList(sTFCluster)
fnExtractDelayedBitmapToList(sTFCluster, "Christine|DarkmatterTF0", "Root/Images/Scenes/Christine/DarkmatterTF0")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|DarkmatterTF1", "Root/Images/Scenes/Christine/DarkmatterTF1")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|DarkmatterTF2", "Root/Images/Scenes/Christine/DarkmatterTF2")

--Secrebot TF
sTFCluster = "Chapter 5 Christine Secrebot TF"
fnCreateDelayedLoadList(sTFCluster)
fnExtractDelayedBitmapToList(sTFCluster, "Christine|SecrebotTF00", "Root/Images/Scenes/Christine/SecrebotTF00")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|SecrebotTF01", "Root/Images/Scenes/Christine/SecrebotTF01")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|SecrebotTF02", "Root/Images/Scenes/Christine/SecrebotTF02")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|SecrebotTF03", "Root/Images/Scenes/Christine/SecrebotTF03")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|SecrebotTF04", "Root/Images/Scenes/Christine/SecrebotTF04")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|SecrebotTF05", "Root/Images/Scenes/Christine/SecrebotTF05")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|SecrebotTF06", "Root/Images/Scenes/Christine/SecrebotTF06")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|SecrebotTF07", "Root/Images/Scenes/Christine/SecrebotTF07")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|SecrebotTF08", "Root/Images/Scenes/Christine/SecrebotTF08")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|SecrebotTF09", "Root/Images/Scenes/Christine/SecrebotTF09")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|SecrebotTF10", "Root/Images/Scenes/Christine/SecrebotTF10")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|SecrebotTF11", "Root/Images/Scenes/Christine/SecrebotTF11")
fnExtractDelayedBitmapToList(sTFCluster, "Christine|SecrebotTF12", "Root/Images/Scenes/Christine/SecrebotTF12")

-- |[ =================================== Gala Dress Sequence ================================== ]|
--Cluster.
local sDressSequence = "Chapter 5 Dresses"
fnCreateDelayedLoadList(sDressSequence)

--Open, create.
SLF_Open(gsaDatafilePaths.sScnGalaDress)
DL_AddPath("Root/Images/Scenes/GalaDress/")

--Images.
fnExtractDelayedBitmapToList(sDressSequence, "SophieDress|Seq0", "Root/Images/Scenes/GalaDress/Seq0")
fnExtractDelayedBitmapToList(sDressSequence, "SophieDress|Seq1", "Root/Images/Scenes/GalaDress/Seq1")
fnExtractDelayedBitmapToList(sDressSequence, "SophieDress|Seq2", "Root/Images/Scenes/GalaDress/Seq2")

-- |[ ================================= Chapter 5 Major Scenes ================================= ]|
--Other major scenes that don't fit into another category.
SLF_Open(gsaDatafilePaths.sScnCh5Major)

--Window reflection scene.
DL_AddPath("Root/Images/Scenes/WindowReflection/")
fnCreateDelayedLoadList("Chapter 5 Window")
fnExtractDelayedBitmapToList("Chapter 5 Window", "Window|Alone",    "Root/Images/Scenes/WindowReflection/Alone")
fnExtractDelayedBitmapToList("Chapter 5 Window", "Window|Together", "Root/Images/Scenes/WindowReflection/Together")

--Sophie in a leather outfit.
DL_AddPath("Root/Images/Scenes/SophieLeather/")
fnCreateDelayedLoadList("Chapter 5 Leather Sophie")
fnExtractDelayedBitmapToList("Chapter 5 Leather Sophie", "Major|SophieLeather", "Root/Images/Scenes/SophieLeather/SophieLeather")

--Secrebot Bad End
DL_AddPath("Root/Images/Scenes/SecrebotBadEnd/")
fnCreateDelayedLoadList("Chapter 5 Secrebot Bad End")
fnExtractDelayedBitmapToList("Chapter 5 Secrebot Bad End", "Major|SecrebotBadEnd", "Root/Images/Scenes/SecrebotBadEnd/SecrebotBadEnd")

--Finale.
DL_AddPath("Root/Images/Scenes/Ch5Finale/")
fnCreateDelayedLoadList("Chapter 5 Finale")
fnExtractDelayedBitmapToList("Chapter 5 Finale", "Major|Chapter5Finale", "Root/Images/Scenes/Ch5Finale/Explosion")

-- |[ ======================================== Clean Up ======================================== ]|
-- |[Load Issue]|
--Order queued loading.
fnLoadDelayedBitmapsFromList(sEverythingList, gciDelayedLoadLoadAtEndOfTick)

-- |[Debug]|
if(false) then
    fnPrintDelayedBitmapList(sEverythingList)
end

-- |[Close]|
SLF_Close()
