-- |[ ==================================== Scene Alias List ==================================== ]|
--Cutscenes can be stored under an alias. This is used for enemies on the overworld. When the player
-- is defeated or retreats or otherwise calls a script, the alias is checked instead of the literal
-- script path. Those aliases are built in this script.
local sDebugPath     = gsRoot .. "Chapter 5/Scenes/000 Debug/"
local sTransformPath = gsRoot .. "Chapter 5/Scenes/100 Transform/"
local sDefeatPath    = gsRoot .. "Chapter 5/Scenes/200 Defeat/"
local sStandardPath  = gsRoot .. "Chapter 5/Scenes/300 Standards/"
local sNormalPath    = gsRoot .. "Chapter 5/Scenes/500 Normal/"

--Wipe previous aliases.
PathAlias_Clear()

-- |[ ===================================== Combat Scenes ====================================== ]|
--Whenever a scene occurs as a result of combat, be it due to retreat, surrender, defeat, victory,
-- or ending for cutscene reasons, an alias must be used.
PathAlias_CreatePath("Standard Defeat",  sStandardPath .. "Defeat/Scene_Begin.lua")
PathAlias_CreatePath("Standard Retreat", sStandardPath .. "Retreat/Scene_Begin.lua")
PathAlias_CreatePath("Standard Revert",  sStandardPath .. "Revert/Scene_Begin.lua")

--Defeat Cutscenes
PathAlias_CreatePath("Defeat LRT West",         sDefeatPath .. "Defeat LatexDrone/Scene_Begin.lua")
PathAlias_CreatePath("Defeat Raibie Christine", sDefeatPath .. "Defeat Raibie Christine/Scene_Begin.lua")
PathAlias_CreatePath("Defeat Silly Secrebots",  sDefeatPath .. "Defeat Silly Secrebots/Scene_Begin.lua")
PathAlias_CreatePath("Corruption",              sDefeatPath .. "Corruption/Scene_Begin.lua")

--Victory Cutscenes
PathAlias_CreatePath("Victory Raibie",  sNormalPath .. "Victory Raibie/Scene_Begin.lua")

-- |[ ======================================== Aliases ========================================= ]|
--These are the aliases that refer to the paths. These appear in the .slf data saved from Tiled.
PathAlias_CreateAliasToPath("Defeat_LRT_West",        "Defeat LRT West")
PathAlias_CreateAliasToPath("Corruption",             "Corruption")
PathAlias_CreateAliasToPath("Victory_Raibie",         "Victory Raibie")
PathAlias_CreateAliasToPath("Defeat_RaibieChristine", "Defeat Raibie Christine")
PathAlias_CreateAliasToPath("Defeat_Silly",           "Defeat Silly Secrebots")
