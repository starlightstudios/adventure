-- |[ ================================= Chapter 5 Scene Listing ================================ ]|
--Builds a list of cutscenes the player can make use of through the debug menu. Cutscenes in the debug menu are
-- used at the player's own risk, they may produce unusable situations that can only be remedied with the debug menu.
-- It's an addiction, I tell you.
--Note that not all cutscenes are necessarily on the debug list. There may also be cutscenes stored
-- in map folders that are not intended to be executed via the debug menu.

-- |[Setup]|
--Scene Listing variable
local sPrefix = ""
local saSceneList = {}

-- |[Adder Function]|
local fnAddScene = function(sPath)
    local i = #saSceneList + 1
    saSceneList[i] = sPath
end

-- |[ ====================================== Debug Scenes ====================================== ]|
fnAddScene("Chapter 5/Scenes/400 Volunteer/Latex Drone Volunteer")
sPrefix = "Chapter 5/Scenes/000 Debug/"
fnAddScene(sPrefix .. "A Quickadd 55")
fnAddScene(sPrefix .. "A Quickadd SX-399")
fnAddScene(sPrefix .. "A Quickadd JX-101")
fnAddScene(sPrefix .. "A Quickadd Sophie")
fnAddScene(sPrefix .. "A Quickremove 55")
fnAddScene(sPrefix .. "A Quickremove JX-101")
fnAddScene(sPrefix .. "A Quickremove SX-399")
fnAddScene(sPrefix .. "A Quickremove Sophie")
fnAddScene(sPrefix .. "B 0 Jump End of Cryogenics")
fnAddScene(sPrefix .. "B 1 Jump End of Regulus City")
fnAddScene(sPrefix .. "B 2 Jump End of LRT")
fnAddScene(sPrefix .. "B 3 Jump Biolabs")
fnAddScene(sPrefix .. "B 4 Jump Secrebot")
fnAddScene(sPrefix .. "B 5 Jump Secrebot End")
fnAddScene(sPrefix .. "C Quickcomplete Electrosprites")
fnAddScene(sPrefix .. "C Quickcomplete Equinox")
fnAddScene(sPrefix .. "C Quickcomplete Mines")
fnAddScene(sPrefix .. "C Quickcomplete Serenity")
fnAddScene(sPrefix .. "D Add Rep Items")
fnAddScene(sPrefix .. "E Unlock All Skillbooks")

-- |[ ================================== Save Point TF Scenes ================================== ]|
sPrefix = "Chapter 5/Scenes/100 Transform/"
--fnAddScene(sPrefix .. "Transform_MeiToAlraune")

-- |[ ====================================== Defeat Scenes ===================================== ]|
sPrefix = "Chapter 5/Scenes/200 Defeat/"
fnAddScene(sPrefix .. "Defeat_BackToSave")
--fnAddScene(sPrefix .. "Defeat_Alraune")

-- |[ ========================================= Upload ========================================= ]|
--Upload these to the debug menu.
local i = 1
ADebug_SetProperty("Cutscenes Total", #saSceneList)
while(saSceneList[i] ~= nil) do
	ADebug_SetProperty("Cutscene Path", i-1, saSceneList[i])
	i = i + 1
end
