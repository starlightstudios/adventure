-- |[Steam Droid Transformation]|
--Setup.
local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsRelivingScene", "N")
fnLoadDelayedBitmapsFromList("Chapter 5 Christine SteamDroid TF", gciDelayedLoadLoadAtEndOfTick)
    
--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--Clear censor bars.
WD_SetProperty("Clear Censor Bars")

--Black the screen out.
if(gbDontGoToNextPart ~= nil and iIsRelivingScene == 0.0) then
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
end

--Switch to scene mode.
fnCutscene([[ AL_SetProperty("Music", "Null") ]])
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Christine_Human") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] A power core...[P] what do you even make a power core out of?[B][C]") ]])
fnCutscene([[ Append("SX-399 has led her to a long-disused workshop on a higher floor of the mines.[P] She had asked her friends to wait outside, and SX-399 has smiled and told her she would be ready to answer any questions that came up.[B][C]") ]])
fnCutscene([[ Append("She wandered around the workshop to take stock of all the materials available there.[P] Scrap was in abundance.[P] Gauges, connectors, pipes...[P] as she collected the pieces she would need, she came upon one particular item that held rapt her attention.[B][C]") ]])
fnCutscene([[ Append("A rusty old rifle, of Pandemonium make, and an affixed bayonet.[P] They were not unlike the type her forefathers had once used.[P] Rusted from years of neglect, the wood began to crumble beneath her fingers as she picked it up.[B][C]") ]])
fnCutscene([[ Append("She stared at it for a moment, frowned, and threw it back onto the pile.[P] The power core was for her, not for her ancestors.[B][C]") ]])
fnCutscene([[ Append("...[B][C]") ]])
fnCutscene([[ Append("Each step was more complicated than the last, though the provided instructions were sufficient to avoid major complications.[P] But with each step, her thoughts would wander back to the rifle.[P] How might it fill the needs of -[P] no, it would not do.[B][C]") ]])
fnCutscene([[ Append("...[B][C]") ]])
fnCutscene([[ Append("Christine stood before the nearly-finished power core.[P] The messy lab had revealed an organized chaos, formed over the years as the same process had repeated itself a hundred times before.[P] Despite this, she found herself again before the rusted rifle.[B][C]") ]])
fnCutscene([[ Append("The bayonet and the barrel were both made of iron.[P] Not a choice material when dealing with steam, but there were ways to use them that would prevent a meeting between the two.[P] Christine frowned.[P] This *was* the final piece.[B][C]") ]])
fnCutscene([[ Append("Taking the rifle into her hands, she broke away the rotted wood and discarded the trigger, bolt casing, and metal stock frame.[P] She only wanted the bayonet and the barrel.[B][C]") ]])
fnCutscene([[ Append("She began to work again on her core, hammering on the iron from the rifle until it was a flattened mass that she wrapped around the seam in the two halves.[P] Welding it to the two sections would seal them together with a stronger bond than brass and bronze, and should never encounter the steam that the core would pump through to her future systems.[B][C]") ]])
fnCutscene([[ Append("Finished, she held the core aloft, inspecting and admiring it in the same moment.[P] It was crude-looking, but she had no doubts that all Steam Droid cores would be similarly crude.[P] They were never meant to be perfect, they were meant to be an extension of the Droid who made them.[B][C]") ]])
fnCutscene([[ Append("She tweaked the tuning dial and activated the core.[P] It began to hum with a throbbing pulse as it churned to life.[P] Its first task was to scan itself, to ensure the materials it was made with would be capable of withstanding regular wear and the steam it would pump throughout its host body.[B][C]") ]])
fnCutscene([[ Append("It paused at the iron it encountered.[P] An unusual choice, and one that it could not find similarities for in its database, but not one that would be detrimental to its overall design, if maintained correctly.[B][C]") ]])
fnCutscene([[ Append("Finding no concerns nor any further anomalies, it beeped out a confirmation of satisfaction.[P] It was pleased with the caliber of its construction.[B][C]") ]])
fnCutscene([[ Append("Christine smirked with her own satisfaction.[P] She closed her eyes and took a deep breath.[P] When she had been transformed into a golem, she had not been given a choice.[P] Suddenly being presented with one gave her pause.[B][C]") ]])
fnCutscene([[ Append("Was this the only way?[P] Was this what she wanted?[P] Anxiety gave way to certainty.[P] More lives than her own were depending on her.[P] She had to do this.[B][C]") ]])
fnCutscene([[ Append("Exhaling slowly, pushing the air from her chest and feeling her breasts and shoulders gently fall, she raised the core to her chest.[P] She opened her eyes and looked down at it.[P] She was ready.[B][C]") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Christine_Human") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/SteamTF0") ]])
fnCutscene([[ WD_SetProperty("Register Censor Bar", 576, 234, 43, 40) ]])
fnCutscene([[ Append("A long, low tone began to emanate from the core.[P] Perhaps it was meant to give the volunteer one final chance to withdraw from the process, or notify the supervising engineer.[P] Whatever its purpose, the tone continued for several long seconds before at last growing quiet.[B][C]") ]])
fnCutscene([[ Append("Small claw-like tentacles emerged from the core, altered by the internal systems she had been provided with to ensure a safe transformation, and sealed the core to the center of her bare chest.[P] A thin hose emerged from beneath it and bore painlessly into her chest, and a viscous fluid, as dark as the richest inks she had ever seen, began to fill her.[B][C]") ]])
fnCutscene([[ Append("She continued to clutch the core, not of its own design but of her own volition.[P] She blinked as she felt the fluid filling her, and released the core.[P] She was still in control, unlike what the golem core had done to her.[B][C]") ]])
fnCutscene([[ Append("Her skin began to darken until it was indistinguishable from the inky fluid, and inside of her body, she could feel her organs beginning to shift.[P] While it felt unusual, she was more appreciative of the improvements the golem core had sported.[P] She had felt nothing internal during that change.[B][C]") ]])
fnCutscene([[ Append("The black fluid was composed of primitive single-use nanomachines, invented by alchemical processes well in advance of their time.[P] The design was as primitive to the citizens of Regulus as it was advanced to her, a happy marriage of accident and fortune.[B][C]") ]])
fnCutscene([[ Append("Lungs became compressed tanks, her diaphragm a pump, arteries and veins became tubes to carry steam while muscle and bone became specialized actuators.[P] Ultimately would come her heart, which was already being replaced as the core burrowed itself into her chest.[P] It was a feeling even stranger than any she had yet felt, but one not altogether unpleasant.[B][C]") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/SteamTF0") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/SteamTF1") ]])
fnCutscene([[ WD_SetProperty("Send Censor Bars To Crossfade") ]])
fnCutscene([[ WD_SetProperty("Register Censor Bar", 708, 204, 39, 36) ]])
fnCutscene([[ WD_SetProperty("Register Censor Bar", 653, 358, 57, 40) ]])
fnCutscene([[ Append("Now the fluid reached to her brain.[P] At once she was subjected to vivid imagery.[P] She saw all of Regulus as though she were standing on Pandemonium, and she felt a twinge of sadness pass over her.[P] To be so far from the home she had grown to love, with all of its joys and faults, weighed upon her.[B][C]") ]])
fnCutscene([[ Append("As soon as has the vision faded, she saw herself standing in the maintenance bay in Regulus City, watching like a ghost as she worked alongside Sophie.[P] Both were smiling, chatting with each other, though she could not hear the words.[B][C]") ]])
fnCutscene([[ Append("A moment later, a coy smile spread across Sophie's face, and she bumped her shoulder into the Christine beside her.[P] The two looked at each other and giggled for a brief moment together before returning to work.[B][C]") ]])
fnCutscene([[ Append("The scene shifted, and she was now walking alongside 55 in a dark hallway.[P] Where she was she could not say, but she could see a frustrated look on 55's face as the Christine she watched seemed to be telling a long joke.[B][C]") ]])
fnCutscene([[ Append("She finished and, for the briefest of moments, unseen to the Christine that walked alongside her, the barest hint of a smile spread across 55's lips before vanishing almost immediately.[B][C]") ]])
fnCutscene([[ Append("No sooner had the scene ended than another began, and another after that, and another after still, and she soon lost count of all that she had seen.[P] Sophie, 55, SX-399, any many more besides.[P] All were units she cared for, to varying degrees, and all were units she wanted to help.[B][C]") ]])
fnCutscene([[ Append("As the last vision faded, she found herself still standing in the workshop.[P] She glanced down to find that the core had fully merged with her, and that the transformation of her body was complete.[B][C]") ]])
fnCutscene([[ Append("There was only one final task necessary to emerge as a Steam Droid, and a whirring of machinery above her indicated the final step was ready to begin.[P] A hose descended from the ceiling of the chamber.[P] It was thick, but remarkably flexible, and would stay so until it began to fill her with the life-giving steam.[B][C]") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/SteamTF1") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/SteamTF2") ]])
fnCutscene([[ WD_SetProperty("Send Censor Bars To Crossfade") ]])
fnCutscene([[ Append("She took the hose in her hands and guided the nozzle to her mouth.[P] The crude Steam Droid process required many manual interactions, unlike the heavily automated Golem tube.[P] To become a Steam Droid was a choice, at every step.[B][C]") ]])
fnCutscene([[ Append("The cool brass tip of the nozzle slid between her lips, and she gently pulled the sheath back before sliding the nozzle deeper into her mouth.[P] Her tongue flicked about it involuntarily, allowing her saliva, or the oils that now functioned as her saliva, to lubricate its passage.[B][C]") ]])
fnCutscene([[ Append("She stopped before reaching her throat, and after a moment, a quiet tone could be heard from somewhere within the lab, and the hose stiffened and throbbed between her hands as the first hiss of life-giving steam began to flow through it, erupting from the nozzle and flowing into her.[B][C]") ]])
fnCutscene([[ Append("The pumps and compressors that had once been her lungs and diaphragm began to function, pulling the warm, wet steam down her throat and causing her newly-formed brass and alloy clothing to swell at the joints as it filled the steam bladders in her chest.[B][C]") ]])
fnCutscene([[ Append("She looked at her reflection in the glass and patted her lightly-swollen breasts with a smile.[P] Finally they were more than just decoration.[P] Her power core let out its own hum of satisfaction in turn.[B][C]") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/SteamTF2") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/SteamTF3") ]])
fnCutscene([[ WD_SetProperty("Send Censor Bars To Crossfade") ]])
fnCutscene([[ Append("The steam bladders quickly reached their capacity, and the hose began to retract into the ceiling.[P] Her hand guided the once-quivering hose along as she withdrew it from her mouth, and as the nozzle slipped from her lips, a small puff of steam condensed on its tip and dribbled onto her lower lip.[B][C]") ]])
fnCutscene([[ Append("She smiled and flicked her tongue along her lip, catching the errant drop and whisking it away into her mouth and down her throat.[B][C]") ]])
fnCutscene([[ Append("The sheath on the nozzle slipped back into place quietly as the hose disappeared into the recesses of the chamber, and Christine looked about for the first time as a Steam Droid.[P] Her eyes fell once more onto the rotting wood of the old rifle, and her smile faded.[P] It was a part of her, but it had no bearing on her.[P] It held no sway.[P] She kicked it aside and left.[B][C]") ]])
fnCutscene([[ Append("Outside, a giddy SX-399 and a flustered-looking 2855 awaited her.") ]])
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()

--If this is a relive case, end the handler here.
if(iIsRelivingScene == 1.0) then
    
	--Wait a bit.
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Return to the last save point and execute the post-script..
	fnCutscene([[ AL_BeginTransitionTo("LASTSAVE", gsStandardReliveEnd) ]])
	fnCutsceneBlocker()
    return
end

--Switch Christine to Steam Droid.
fnCutscene([[ LM_ExecuteScript(gsRoot .. "/FormHandlers/Christine/Form_SteamDroid.lua") ]])
fnCutsceneBlocker()

--[Dialogue]
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Tiffany", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "SX-399", "Neutral") ]])
fnCutscene([[ Append("SX-399:[E|Neutral] You look great![P] How was it?[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] ...[P] Steamy.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Our path is planned and everything is in place.[P] I must apply the disguise, and we will move out.") ]])
fnCutsceneBlocker()
fnCutsceneWait(325)
fnCutsceneBlocker()

--[Dialogue]
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
fnCutscene([[ Append("Christine:[E|Neutral] You know, 55, since I recovered my memory...[P] I was hoping this would happen.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Stop squirming.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] You're...[P] you're my best friend, 55.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] ...?[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] Well you're doing my make-up.[P] Isn't that what friends do?[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] I...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] Sorry, I'm distracting you.[P] Carry on.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] You'll be done in a few minutes...") ]])
fnCutsceneBlocker()
fnCutsceneWait(325)
fnCutsceneBlocker()
fnCutscene([[ fnUnloadBitmapsFromList("Chapter 5 Christine SteamDroid TF") ]])

--[Post-Scene]
--Switch Christine to Steam Droid.
if(gbDontGoToNextPart == nil) then
    VM_SetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N", 1.5)
    fnCutscene([[ LM_ExecuteScript(gsRoot .. "/FormHandlers/Christine/Form_SX399.lua") ]])
    fnCutsceneBlocker()
end

--[Dialogue]
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Tiffany", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "SX-399", "Neutral") ]])
fnCutscene([[ Append("Christine:[E|Neutral] How do I look?[B][C]") ]])
fnCutscene([[ Append("SX-399:[E|Neutral] It's like looking into a mirror...[P] ungh...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] Are you all right?[B][C]") ]])
fnCutscene([[ Append("SX-399:[E|Neutral] My core...[P] I should already have started my recharge cycle...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] Right, no time to waste.[P] 55, get going.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Come, we will move faster if I carry you...") ]])
fnCutsceneBlocker()

--Next segment.
if(gbDontGoToNextPart == nil) then
    fnCutscene([[ AL_BeginTransitionTo("SprocketCityA", "FORCEPOS:14.0x37.0x0") ]])
else
    gbDontGoToNextPart = nil
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
end