-- |[First H-Scene]|
--Sophie is nervous and doesn't know what she's doing. She also has no idea she's going to be the penetrator.

--Clear censor bars.
WD_SetProperty("Clear Censor Bars")
	
--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
fnCutscene([[ Append("Christine:[E|Neutral] What does synchronizing mean...?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] There is an attachment that goes in your...[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] ...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] Sophie?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Groin.[P] Between your legs.[P] It should be on the side of the tube.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Attach that to -[P] What![B][C]") ]])
fnCutscene([[ Append("*clank*[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] It looks better on you than on me.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] But you're the Lord Unit...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] I want you inside me, Sophie.[P] Synchronize with me.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] I don't...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] I need this.[P] I want my tandem unit to pulse through me.[P] Fill me up.[P] Thrust into me.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Oh -[P] okay...") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()

--Scenes.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
--fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
fnCutscene([[ Append("Gingerly, Sophie felt at the attachment Christine had bolted to her.[P] It felt right, somehow.[P] It was soft in her hands, but as she gazed into Christine's eyes, the attachment began to harden.[B][C]") ]])
fnCutscene([[ Append("Christine leaned forward and pressed her metal lips against Sophie's.[P] Her machine lover was warm inside.[P] Christine's CPU began to race ahead, imagining all the things they would do.[B][C]") ]])
fnCutscene([[ Append("Sophie felt around at Christine's sex, trying to find the release switch.[P] Christine aided her, exposing her receiver port.[P] Sophie began to shake with nervousness and delight.[B][C]") ]])
fnCutscene([[ Append("Christine gripped Sophie's shaft, softly at first.[P] She tugged it towards her sex.[P] Sophie wanted to stop, to run away and hide, until she saw Christine's longing.[B][C]") ]])
fnCutscene([[ Append("She smiled broader than ever, and thrust herself towards Christine.[P] Her penis thrust into Christine's receiver port, and Christine's sensory units lit up like never before.[B][C]") ]])
fnCutscene([[ Append("Instantly, both units began to overclock their processors.[P] Christine worked her hips in unison with Sophie's.[P] She needed it inside her, and it wasn't enough.[P] She hungered for it.[B][C]") ]])
fnCutscene([[ Append("Sophie began to warm to the task, thrusting in and out of Christine's receiver port.[P] They were still standing, balanced against one another as they joined their processors together.[B][C]") ]])
fnCutscene([[ Append("Christine felt something unfamiliar in her memory core.[P] She began to hear instructions and whispers that were not her own.[P] She strained to listen to them, eager to hear them.[B][C]") ]])
fnCutscene([[ Append("As Sophie drilled into her receiver port with mechanical steadiness, Christine soon realized what the instruction were.[P] They were Sophie's thoughts.[P] They were synchronizing, now.[P] She could feel Sophie's mind in her own.[B][C]") ]])
fnCutscene([[ Append("She soon realized that Sophie was experiencing the same phenomenon.[P] She thought to herself that she needed Sophie to thrust faster, and Sophie obliged.[P] They no longer needed words.[P] The two units joined in perfect tandem.[B][C]") ]])
fnCutscene([[ Append("Sophie continued to thrust into Christine as the synchronization reached its peak.[P] She felt a building of her own.[P] It was coming from her power core.[P] She kept calm and continued to penetrate her lover, eager to feel the coming release.[B][C]") ]])
fnCutscene([[ Append("Christine narrowed her receiver port to tighten around Sophie's penis.[P] The sensation redoubled Sophie's stimulation, and before she knew it, her power core began to surpass its cooling unit.[B][C]") ]])
fnCutscene([[ Append("A bolt of electricity shot through every system in Sophie's body.[P] It jumped through her into Christine's lighting them both in unison.[P] Sophie withdrew from Christine, and the two stood next to one another.[B][C]") ]])
fnCutscene([[ Append("Their eyes met one another as they struggled to understand what had just happened.[P] 'Synchronization Completed'[P] they said, in perfect unison.[P] Sophie moved to cover her mouth with her hand, but Christine covered it with her own.[B][C]") ]])
fnCutscene([[ Append("Despite no longer being attached, they could still hear one another in their heads.[P] They began to exchange low-level instructions, and Christine's mouth and Sophie's joined in a beautiful kiss.[B][C]") ]])
fnCutscene([[ Append("As the two kissed one another in deepest unison, their link began to fade.[P] Sophie's voice in Christine's head lessened in volume, until it was eventually gone.[P] She knew Sophie was experiencing the same thing.[B][C]") ]])
fnCutscene([[ Append("While no longer in euphoria due to the loss of her tandem unit's thoughts, Christine relaxed and let the afterglow take her.[P] She and Sophie leaned on one another, and began to defragment their drives.") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()

--Scenes.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
--fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
fnCutscene([[ Append("Their drives finished defragmentation at the same time.[P] Christine had dreamt of Sophie using her spare cycles, and knew Sophie had dreamt of her.[P] They needed not speak, for they were still synchronized from the night before.[B][C]") ]])
fnCutscene([[ Append("Sophie could intuit Christine's desires, but the call of her tasks now took priority.[P] Christine knew the same applied to her.[P] She wanted to synchronize with Sophie again, but her programming mandated a cool-down time.[B][C]") ]])
fnCutscene([[ Append("She delicately removed the attachment from Sophie and replaced it on her pod.[P] Sophie pressed her overlarge hand against Christine's rear, firmly gripping it.[P] Christine smiled.[P] They kissed again.[B][C]") ]])
fnCutscene([[ Append("Then, Sophie took her leave, and returned to the maintenance bay as her programming dictated.[P] Christine was sad to see her go, and ran over the encounter in her memory banks over and over.") ]])
fnCutsceneBlocker()