-- |[LRT Eldritch Dream Girl Part 2]|
--Hallways of LRTH.

--Christine's form.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Black the screen out.
fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneWait(85)
fnCutsceneBlocker()

--Spawn 56.
TA_Create("56")
	TA_SetProperty("Position", 0, 0)
	TA_SetProperty("Facing", gci_Face_East)
	TA_SetProperty("Clipping Flag", false)
	fnSetCharacterGraphics("Root/Images/Sprites/56/", true)
DL_PopActiveObject()

--Move 55 offscreen.
fnCutsceneTeleport("Tiffany", -100.25, -100.50)
fnCutsceneBlocker()

--Fade in.
fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()

--Christine moves north.
fnCutsceneMove("Christine", 26.75, 20.50)
fnCutsceneBlocker()
fnCutsceneFace("Christine", -1, -1)
fnCutsceneWait(15)
fnCutsceneBlocker()
fnCutsceneFace("Christine", -1, 0)
fnCutsceneWait(125)
fnCutsceneBlocker()
fnCutsceneFace("Christine", -1, -1)
fnCutsceneWait(15)
fnCutsceneBlocker()
fnCutsceneMove("Christine", 26.75, 9.50)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 1, -1)
fnCutsceneWait(15)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 1, 0)
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 1, -1)
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 0, -1)
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneFace("Christine", -1, -1)
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneFace("Christine", -1, 0)
fnCutsceneWait(125)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutscene([[ Append("Christine:[E|Neutral] (Hmm, it was getting closer, and now I can't hear it at all.[P] Maybe they stopped?[P] Perhaps I should call out to them...)[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] Hello?[P] Is anyone there?") ]])
fnCutsceneBlocker()

--Teleport 56 in.
fnCutsceneTeleport("56", 26.75, 23.50)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneMove("56", 26.75, 10.50, 1.50)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
fnCutscene([[ Append("Christine:[E|Neutral] Oh, there you are.[P] Did you hear the singing, too?[B][C]") ]])

--Dialogue branches based on Christine's form.
if(sChristineForm == "Human") then
    fnCutscene([[ Append("55:[E|Neutral] There are no humans assigned to this facility, and you lack a security pass.[P] Are you, perhaps, some kind of idiot?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Do you have any idea the extreme danger you have put a superior unit in by virtue of being here?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] Nice to see you, too, 55.[P] Sheesh.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Sorry for wandering off like that, but you don't have to be so harsh all the time.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] 55..?[P][P] Then, are you Christine?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Of course I am.[P] Is your memory on the fritz again?[P] Do you need me to take a look at it?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Then you're the one who found -[P] !!![B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Follow me![P] Quickly!") ]])
    
elseif(sChristineForm == "Golem") then
    fnCutscene([[ Append("55:[E|Neutral] You![P] Who are you, and how did you get in here?[P] There's only supposed to be drone units assigned to this area![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] You're not the sort to try jokes, 55...[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] It's me![P] Christine![P] Sheesh, is your memory on the fritz again?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] ...[P] Christine...[P] But that's the same name as -[P] !![B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Follow me![P] Quickly!") ]])

elseif(sChristineForm == "LatexDrone") then
    fnCutscene([[ Append("55:[E|Neutral] Drone Unit![P] I ordered this area cleared![P] Where is your squad leader?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] What, you don't recognize me?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] *Ahem*[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] GREETINGS, COMMAND UNIT 2855.[P] THIS UNIT APOLOGIZES FOR WANDERING OFF BUT IS WORRIED YOUR MEMORY IS FRIED.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] THIS UNIT RECOMMENDS HAVING HER EXAMINE YOUR HEAD FOR DEFECTIVE SECTORS.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] ...[P] I am not amused, Drone Unit.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Wait, did you say Command Unit 2855?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] AFFIRMATIVE.[P] YOU ARE COMMAND UNIT 2855.[P] UH...[P] aren't you?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Yes, of course I am.[P] We have places to be.[P] Follow me, right now!") ]])
    
elseif(sChristineForm == "Darkmatter") then
    fnCutscene([[ Append("55:[E|Neutral] You there, Darkmatter![P] No -[P] no you're not.[P] You're different.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Uh, yeah, I am.[P] I think that's pretty obvious.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] I mean, the others are far less talkative, right?[P] Ha ha ha![B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] And once again the joke sails over 55's head.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] ...[P] 55?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Are you talking to yourself or something?[P] Are your memory sectors shot?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] If you think I am -[P] then that means -[P] oh no.[P] Damn it![P] DAMN IT![B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Christine, on me![P] Quickly![P] We haven't much time!") ]])
    
elseif(sChristineForm == "Electrosprite") then
    fnCutscene([[ Append("55:[E|Neutral] It can talk, and appears to be holding humanoid form.[P] But you are not the vector.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] The vector?[P] What?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Be silent.[P] I am thinking.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] No need to be so harsh, 55.[P] What's this about a vector?[P] Did you find something in the core already?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Sorry about wandering off, but I thought I heard something...[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Wait, did you say 55?[P] As in, Unit 2855?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Yes?[P] What?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Then you're an electrosprite, according to the vernacular -[P] and you're...[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Damn it, no![P] No![P] Not right here, not right now![P] Christine, follow me![P] Quickly!") ]])
    
elseif(sChristineForm == "SteamDroid") then
    fnCutscene([[ Append("55:[E|Neutral] A looter.[P] Perfect.[P] Exactly what I need at this juncture.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Well I think the term 'scavenging' is a little more classy, but sure, looter it is.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] And I wasn't 'looting', 55.[P] I just thought I heard something.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Did you say, 55?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I did.[P] Are you all right in the processor?[P] You're acting funny.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Not an ordinary Steam Droid...[P] You'd be...[P] Christine...[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Which ties in perfectly with the run of events.[P] And here I've stumbled into it.[P] Christine, follow me.[P] Now![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Ooookayyyyyy...") ]])
    
elseif(sChristineForm == "Raiju") then
    fnCutscene([[ Append("55:[E|Neutral] A stray raiju.[P] None of you are assigned to this facility, state your designation, right now![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Did you forget our mission, 55?[P] Wow, maybe organic brains really are better sometimes.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Absolutely nothing about organics is superior in any way to my mechanical perf -[P] 55?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Unit 2855?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] Now that's worrying.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] I don't have my tools but I could open you up and take a look if you need.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Not necessary.[P] Follow me.[P] Immediately!") ]])
    
elseif(sChristineForm == "Eldritch") then
    fnCutscene([[ Append("55:[E|Neutral] You're...[P] not attacking?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] We did kind of break in.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] And you speak to me.[P] Coherently.[P] Why?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] I'm an English major.[P] If I should be able to do one thing, it's communicate clearly.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] English...[P] Christine?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Yyyeeessss?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] No, no no, if you're here then -[P] we need to leave![P] Follow me, right now!") ]])
    
elseif(sChristineForm == "Doll") then
    fnCutscene([[ Append("55:[E|Neutral] This is -[P] not possible.[P] Not possible at all.[P] Who are you, and what are you doing here?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Oh boy, are we doing a bit?[P] I think it's an odd place to try out a comedy routine, but I'm just tickled that you're coming out of your shell.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Not an imposter, all of your scans are coming back green.[P] Are you a -[P] rogue command unit?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] Takes one to know one, 55![B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] ...[P] This would explain the sudden radio silence...[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Please follow me, Christine.[P] Immediately.[P] We do not have time to waste.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Sudden serious turn, okay then.") ]])
    
elseif(sChristineForm == "Secrebot") then
    fnCutscene([[ Append("55:[E|Neutral] You -[P] no, this isn't possible.[P] How did you get in here?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] I rolled in here.[P] The door was open.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] What is your designation?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] Christine![P] 55, are you being funny on purpose?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] ...[P] This would explain the sudden radio silence...[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Please follow me, Christine.[P] Immediately.[P] We do not have time to waste.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] That's expected, I guess.[P] At least you were funny for two sentences, we can work on that.") ]])
    
end

fnCutsceneBlocker()

--Running.
fnCutsceneMove("56", 26.25, 8.50, 1.50)
fnCutsceneBlocker()
fnCutsceneMove("56", 26.25, 8.50, 1.50)
fnCutsceneMove("Christine", 26.25, 9.50, 1.50)
fnCutsceneBlocker()
fnCutsceneMove("56", 25.25, 8.50, 2.00)
fnCutsceneMove("Christine", 26.25, 8.50, 2.00)
fnCutsceneBlocker()
fnCutsceneMove("56", 14.25, 8.50, 2.00)
fnCutsceneFace("56", 0, -1)
fnCutsceneMove("Christine", 15.25, 8.50, 2.00)
fnCutsceneFace("Christine", -1, -1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneWait(45)
fnCutsceneBlocker()

--Scene transition.
fnCutscene([[ AL_BeginTransitionTo("RegulusLRTGZ", "FORCEPOS:5.0x6.0x0") ]])
fnCutsceneBlocker()