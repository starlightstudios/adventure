--[LRT Eldritch Dream Girl Part 1]
--This takes place in the cafeteria of LRTG.
		
--Music stops.
AL_SetProperty("Music", "Null")

--Party moves forward.
fnCutsceneMove("Christine", 16.25, 10.50)
fnCutsceneMove("Tiffany",        17.25, 10.50)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
fnCutscene([[ Append("55:[E|Smirk] We're here.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] We -[P] are?[B][C]") ]])
fnCutscene([[ Append("55:[E|Smirk] The schematics indicate that terminals in this area should have direct access to the data core.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] ...[P] It's quiet in here, isn't it?[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] Where are all the drones?[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Hopefully, someplace far away.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--55 finds the terminal.
fnCutsceneMove("Tiffany", 17.25, 8.50)
fnCutsceneMove("Tiffany", 24.25, 8.50)
fnCutsceneFace("Tiffany", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(15)
fnCutsceneBlocker()
fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
fnCutscene([[ AL_SetProperty("Open Door", "CanteenDoor") ]])
fnCutsceneWait(15)
fnCutsceneBlocker()
fnCutsceneMove("Tiffany", 24.25, 6.50)
fnCutsceneMove("Tiffany", 20.25, 4.50)
fnCutsceneFace("Tiffany", 0, -1)
fnCutsceneMove("Christine", 16.25, 8.50)
fnCutsceneMove("Christine", 21.25, 8.50)
fnCutsceneFace("Christine", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
fnCutscene([[ Append("55:[E|Smirk] ...[P] Yes, this terminal will do.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] Can you access the core?[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Yes.[P] I have a lot of queries I need to run.[P] Keep watch.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] So...[P] I just stand here and look pretty?[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Yes.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Laugh] I guess I'm pretty good at both of those jobs![B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Be quiet.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Christine hears something.
fnCutsceneFace("Christine", -1, -1)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("Christine", -1, 0)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("Christine", -1, 1)
fnCutsceneWait(125)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
fnCutscene([[ Append("Christine:[E|Neutral] 55...[P] do you hear that?[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] No.[P] I am busy.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Blush] It sounds like someone singing...[P] what a beautiful voice...[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] I've never heard someone sing like that...[B][C]") ]])
fnCutscene([[ Append("55:[E|Offended] ...[P] I do not have cycles to waste, and a unit may hear you.[P] Be quiet.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Christine walks away.
fnCutsceneMove("Christine", 18.25, 8.50, 0.50)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneMove("Christine", 17.25, 9.50, 0.50)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneWait(65)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
fnCutscene([[ Append("Christine:[E|Neutral] (It's coming from over here...)[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] 55?[P] Do you mind if I...?[B][C]") ]])
fnCutscene([[ Append("55:[E|Offended] Do not bother me.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] (I will take that as a yes...)") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Christine moves over and sees the empty security checkpoint.
fnCutsceneMove("Christine", 9.75, 11.50)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 0, -1)
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] (The security checkpoint is totally abandoned.[P] Where are all the drones?)") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Christine looks left and has a dialogue.
fnCutsceneFace("Christine", -1, 0)
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] (Maybe they went to that beautiful voice, too?)") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneMove("Christine", 2.75, 11.50)
fnCutsceneBlocker()

--Darken the screen while Christine is moving.
fnCutsceneMove("Christine", 2.75, 5.50)
fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneWait(85)
fnCutsceneBlocker()

--Transition maps.
fnCutscene([[ AL_BeginTransitionTo("RegulusLRTH", "FORCEPOS:26.5x28.0x0") ]])
fnCutsceneBlocker()