-- |[Meeting Sophie]|
--Plays when Christine first enters the maintenance bay and meets Sophie.
VM_SetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N", 11.0)
		
--Move Christine to the cental position.
fnCutsceneMove("Christine", 26.25, 13.50)
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Serious") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
fnCutscene([[ Append("499323:[E|Neutral] One moment, I'll be right there...[P] Just need to...[B][C]") ]])
fnCutscene([[ Append("*Crack*[B][C]") ]])
fnCutscene([[ Append("499323:[E|Offended] Junk![P] Worthless junk![B][C]") ]])
fnCutscene([[ Append("499323:[E|Offended] Am I ever going to get proper tools around here?") ]])
fnCutsceneBlocker()

--Wait.
fnCutsceneWait(25)
fnCutsceneBlocker()

--Turns to see the Christine.
fnCutsceneFace("Sophie", 1, 0)
fnCutsceneWait(45)
fnCutsceneBlocker()

--Look away again...
fnCutsceneFace("Sophie", 0, -1)
fnCutsceneWait(45)
fnCutsceneBlocker()

--Oh crud!
fnCutsceneFace("Sophie", 1, 0)
fnCutsceneWait(45)
fnCutsceneBlocker()

--Rushes over to Christine.
fnCutsceneMove("Sophie", 22.25, 13.50, 3.00)
fnCutsceneMove("Sophie", 25.25, 13.50, 3.00)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Serious") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
fnCutscene([[ Append("771852:[E|Serious] Is there a problem, Slave Unit 499323?[B][C]") ]])
fnCutscene([[ Append("499323:[E|Surprised] I-[P]I wasn't complaining, Lord Golem![P] Please don't report me -[P] oh oh dear...[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] Unit 771852 reporting for function assignment and initial physical system scan.[B][C]") ]])
fnCutscene([[ Append("499323:[E|Blush] 771852?[P] R-[P]really?[B][C]") ]])
fnCutscene([[ Append("499323:[E|Blush] I mean - [P]function assignment.[P] Okay, I can do that.[P] This is your first time here?[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] Affirmative.[B][C]") ]])
fnCutscene([[ Append("499323:[E|Sad] Please don't report me, Lord Golem![P] I'm begging you![B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] Unit is reporting for function assignment.[P] Priority Zero.[B][C]") ]])
fnCutscene([[ Append("499323:[E|Neutral] Really?[P] Oh.[P] All right then.[B][C]") ]])
fnCutscene([[ Append("499323:[E|Smirk] Um, do you have a secondary designation, or should I get you one?[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] This unit's secondary designation is 'Christine'.[B][C]") ]])
fnCutscene([[ Append("499323:[E|Smirk] Oh that's a cute name.[P] Mine's Sophie if you don't want to call me Unit 499323.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Blush] Hey, um, did you notice that our designations sum to the same number?[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] Unit is reporting for function assignment.[P] Priority Zero.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Oh, of course.[P] Right this way, Unit.[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] Affirmative.") ]])
fnCutsceneBlocker()

--Moves to the console.
fnCutsceneMove("Sophie", 25.25, 17.00)
fnCutsceneMove("Sophie", 24.25, 17.00)
fnCutsceneFace("Sophie", 0, -1)
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Serious") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Just step onto the scanner platform there.[P] I'll do the rest.") ]])
fnCutsceneBlocker()

--Moves to the platform.
fnCutsceneMove("Christine", 25.25, 13.50)
fnCutsceneMove("Christine", 25.25, 15.50)
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Serious") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Running the physical scans now.[P] Run your system diagnostics while I'm at it.[P] The checksums are fine.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Serious] Diagnostics in progress...[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] So, you're new?[P] You said you were a new unit?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Well I guess that's obvious because your designation is really high and you're not on the system logs.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] I'll just make you a log entry, and give you roaming permissions until the work terminal gets you sorted out.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] Mmmm mmmm...[P] All done.[P] It was Christine, right?[P] Oh geez,[P] my circuits must be getting fried.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Blush] Do you um,[P] um,[P] think you could maybe put in for some better equipment for me, with the other Lord Golems?[P] I've put in a lot of requests but they've been ignored.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Blush] ...[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Blush] Oh geez I'm talking to myself again.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Serious] Diagnostics complete.[P] Data uploaded.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Serious] Disengaging core overrides.[P] Diagnostics indicate personality matrices are aligned.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] The answers to your questions are::[P] Yes.[P] Yes.[P] Yes.[P] I will see to it.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Happy] ...[P] Really?[P] You'll put in for better equipment for me?[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] Affirmative.[P] You maintain our infrastructure.[P] Poor maintenance means poor performance.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] That's what I've been saying![B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] When I have my permissions updated, I'll see to it your requests are given priority.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Wow![P] Thanks so much![P] You -[P] you're - [B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Sad] Not good at all...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] Is something the matter?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Physical scans are fine.[P] In fact, you're in prime condition.[P] But, your mental systems are showing some oddities here...[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] In fact I've never seen this before...[P] Hold on...[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] No that didn't do it...[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Sad] ...[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Well, it doesn't seem to be causing any problems, so I can discharge you.[P] You're ready to carry out your function.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] What's wrong with my mental systems?[P] My diagnostics didn't report an error.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] That's because the memory is inert.[P] See, you've got a big section of your memory drives that's encrypted.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] And it's [P]*big*[P] too.[P] It doesn't seem to be doing anything but it's not supposed to be there on a new unit -[P] or any unit for that matter.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Do you have any idea what that is or what the passphrase might be?[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] Not at all.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Strange.[P] Well, I can spend some time figuring it out if you want.[P][E|Blush] But, well, I'd need to do it...[P] in person...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] Noted.[P] I will come and see you later...[B][C]") ]])
fnCutscene([[ Append("PDU:[E|Neutral][EMOTION|Christine|PDU] (Matchmaker routines activated.[P] Hey, 771852, 499323 is clearly ogling you.)[B][C]") ]])
fnCutscene([[ Append("Christine:[E|PDU] (I can see that, PDU.)[B][C]") ]])
fnCutscene([[ Append("PDU:[E|Happy] (You should ask her out.[P] Ask her to be your tandem unit.)[B][C]") ]])
fnCutscene([[ Append("Christine:[E|PDU] (What?[P] No way she'd be into me.[P] She -[P] she...)[B][C]") ]])
fnCutscene([[ Append("Christine:[E|PDU] (...)[B][C]") ]])
fnCutscene([[ Append("PDU:[E|Happy] (What have you got to lose?)[B][C]") ]])
fnCutscene([[ Append("Christine:[E|PDU] (...[P] A lot, actually.)[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Lord Unit?[B][C]") ]])
fnCutscene([[ Append("Christine:[E|PDU] Huh?[P] Oh, call me Christine.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] What do I need to do now?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] S-[P]so, Lor -[P] Christine.[P] You're officially discharged.[P] Please check the work terminals for available functions to carry out.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Blush] If you'd -[P] uh, I -[P] you know -[P] you want me to look over your memory centers I -[P] uh - [B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] Thank you for the offer, but I must be going.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Blush] Are you, sure?[P] Like, sure, sure?[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] (Jeez, what is wrong with her?[P] Maybe she's the one who needs repairs.)[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] I have to go get a function assignment, of course.[P] I'm sorry to be curt.[P] If I get reassigned to another sector, I may never see you again, so, thank you very much for your hard work.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] I'll lodge a request for an equipment budget increase.[P] Now, goodbye, Sophie.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Sad] Goodbye... Lord Unit...") ]])

--[=[
fnCutscene([[ Append("Christine:[E|Neutral] Unit 499323...[P] Sophie...[P] Do you want to go out with me?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Surprised] Wha - [P][CLEAR]") ]])
fnCutscene([[ Append("Christine:[E|Blush] I think you're attractive and your ocular units keep sneaking over to me from the terminal screen.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Surprised] You noticed that...?[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Blush] I return your affection.[P] Let's become tandem units.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Sad] Oh geez, I am so sorry Lord Golem.[P] Please don't do this to me.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] Do you already have a tandem unit?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Sad] No, but - you are a Lord Golem.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] And?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Sad] I am a Slave Golem.[P] Tandem units are not to cross their model variants.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] I suppose you're a new unit, so you didn't know that.[P] It's best if we both just delete this encounter from our memory banks.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] But you do return my affection?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Sad] Please, I don't want to risk getting in trouble.[P] For either of us.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] Then we will become tandem units, and if any unit has a problem with it then they will take it up with me.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Happy] And call me Christine.[P] Or Unit 771852, if that doesn't take too long.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Sad] I don't know...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] I will tell them it is a direct order I made.[P] You were performing your function by being my tandem unit.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Sad] ...[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] ...[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] ...[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] I guess that will work.[P] Obedience is the zero-order function of all Slave Golems.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] But if any unit asks, I'm doing maintenance work on you.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] Affirmative.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] S-[P]so, Lor -[P] Christine.[P] You're officially discharged.[P] Please check the work terminals for available functions to carry out.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] And if you want to do some...[P] maintenance work...[P] I'll be here...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] (Wow![P] That was easy![P] She said yes!)[B][C]") ]])
fnCutscene([[ Append("PDU:[E|Happy][EMOTION|Christine|PDU] (My matchmaker routines showed an 80 percent success rate.)[B][C]") ]])
fnCutscene([[ Append("Christine:[E|PDU] (PDU, you're the best.)[B][C]") ]])
fnCutscene([[ Append("PDU:[E|Quiet] (You're damn right I am![P] ERROR::[P] Humility routine encountered an unhandled exception and needs to close.)") ]])
]=]
fnCutsceneBlocker()

--Change Christine's costume.
VM_SetVar("Root/Variables/Costumes/Christine/sCostumeGolem", "S", "Normal")
LM_ExecuteScript(gsCostumeAutoresolve, "Christine_Golem")

--Change Christine to use her normal name instead of Unit 771852.
AdvCombat_SetProperty("Push Party Member", "Christine")
    AdvCombatEntity_SetProperty("Display Name", "DEFAULT")
DL_PopActiveObject()
