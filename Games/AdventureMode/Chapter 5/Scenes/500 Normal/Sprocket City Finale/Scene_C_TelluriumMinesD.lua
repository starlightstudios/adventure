-- |[Scene C: Mines]|
--Brief finale, sets variables and reconstitutes the party.

-- |[Flags]|
--Mark the quest as completed.
VM_SetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N", 4.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iCompletedSprocketCity", "N", 1.0)

--Return Christine to normal Steam Droid.
VM_SetVar("Root/Variables/Costumes/Christine/sCostumeSteamDroid", "S", "Normal")

--Remove JX-101 from the party.
giFollowersTotal = 0
gsaFollowerNames = {}
giaFollowerIDs = {0}
AL_SetProperty("Unfollow Actor Name", "JX-101")
VM_SetVar("Root/Variables/Chapter5/Scenes/iIsJX101Following", "N", 0.0)

--Clear JX-101 from the combat lineup.
for i = 0, 3, 1 do
    AdvCombat_SetProperty("Party Slot", 0, "Null")
end

--Re-add Christine.
AdvCombat_SetProperty("Party Slot", 1, "Christine")

--Add 55 to the party.
fnSpecialCharacter("Tiffany", 35, 5, gci_Face_South, false, nil)
fnAddPartyMember("Tiffany", "Tiffany", "Root/Variables/Chapter5/Scenes/iIs55Following")

-- |[Actor Spawning]|
--Character teleportation.
fnCutsceneTeleport("JX-101", -100, -100)
fnCutsceneTeleport("Christine", 35.25, 6.50)
fnCutsceneFace("Christine", 0, 1)
fnCutsceneSetFrame("Christine", "Null")

-- |[Scene]|
--Black out the screen.
fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()

--Fade in.
fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()

--Scene.
fnCutsceneMove("Christine", 35.25, 11.50)
fnCutsceneMove("Tiffany", 35.25, 10.50)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 0, -1)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
fnCutscene([[ Append("Christine:[E|Smirk] That went well, though you really were cutting it close there.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] I admit I was wrong.[B][C]") ]])
fnCutscene([[ Append("55:[E|Smirk] The Steam Droids, if they are upgraded to the Steam Lord variant, will make excellent soldiers.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] Uh huh.[P] Yeah, that's it.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] You're not going to mention SX-399?[P] Not going to mention that little kiss?[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] [P][P]We will be comrades in arms soon enough.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] Riiiight.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Remove that smile from your oral intake.[B][C]") ]])

--Optional bonus scene:
if(AdInv_GetProperty("Item Count", "JX-101's Cuirass") > 0) then
    
    --Equip it back to JX-101.
    AdvCombat_SetProperty("Push Party Member", "JX-101")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Body", "JX-101's Cuirass")
    DL_PopActiveObject()
    fnCutscene([[ Append("Christine:[E|Smirk] And focus on the mission, right?[P] Sure, 55, sure.[B][C]") ]])
    fnCutscene([[ Append("Tiffany:[E|Neutral] One last thing.[P] Why were you in possession of JX-101's armor?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Oh that?[P] She gave it to me, said she was 'hot', to hold on to when I was SX-399.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Don't worry, I gave it back.[B][C]") ]])
    fnCutscene([[ Append("Tiffany:[E|Neutral] Bizarre.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] I just hope I look as good as she does when I get to her age.[B][C]") ]])
    fnCutscene([[ Append("Tiffany:[E|Neutral] If you replace your parts as needed, you will look exactly as you do now.[P] Machines do not age.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] Oh 55, sometimes I think you're hopeless...") ]])

else
    fnCutscene([[ Append("Christine:[E|Smirk] And focus on the mission, right?[P] Sure, 55, sure.") ]])
end
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()


--Fold the party.
fnCutsceneMove("Tiffany", 35.25, 11.50)
fnCutsceneBlocker()
fnCutscene([[ AL_SetProperty("Fold Party") ]])
fnCutsceneBlocker()
