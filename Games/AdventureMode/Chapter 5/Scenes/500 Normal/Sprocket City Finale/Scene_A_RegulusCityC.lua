--[Scene A: Regulus City]
--In this Scene, Sophie finally meets 55 and starts to upgrade SX-399. This is called by a trigger
-- script in RegulusCityC.

--[Actor Spawning]
--Special characters.
if(EM_Exists("Sophie") == false) then
	fnSpecialCharacter("Sophie", 18, 16, gci_Face_North, false, nil)
else
	EM_PushEntity("Sophie")
		TA_SetProperty("Clipping Flag", false)
		TA_SetProperty("Activation Script", "Null")
	DL_PopActiveObject()
	fnCutsceneTeleport("Sophie", 18.25, 16.50)
end
fnSpecialCharacter("Tiffany", -100, -100, gci_Face_North, false, nil)
fnSpecialCharacter("SX-399",   -100, -100, gci_Face_North, false, nil)

--Generics.
TA_Create("ScanGolem")
	TA_SetProperty("Position", 19, 15)
	TA_SetProperty("Facing", gci_Face_South)
	TA_SetProperty("Clipping Flag", false)
	fnSetCharacterGraphics("Root/Images/Sprites/GolemSlave/", false)
DL_PopActiveObject()

--[Scene]
--Black out the screen.
fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

--Focus the camera on Sophie.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 5.0)
	CameraEvent_SetProperty("Focus Actor Name", "Sophie")
DL_PopActiveObject()

--Move Christine and JX-101 off the stage.
fnCutsceneTeleport("Christine", -100.0, -100.0)
fnCutsceneTeleport("JX-101", -100.0, -100.0)
fnCutsceneBlocker()

--Fade in.
fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] All right, your scan checks out.[P] You're good to go.[P] See you in a month![B][C]") ]])
fnCutscene([[ Append("Golem:[VOICE|Golem] By the way, is 771852 here?[P] I haven't seen her in a while.[B][C]") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] She said she was doing important administrative work.[P] Probably trying to get me a new Crogsvield Welder.[B][C]") ]])
fnCutscene([[ Append("Golem:[VOICE|Golem] A what?[P] Nevermind, see you later, 499323!") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneMove("Sophie",    19.25, 16.50)
fnCutsceneMove("Sophie",    19.25, 13.50)
fnCutsceneFace("Sophie",     1, 0)
fnCutsceneMove("ScanGolem", 19.25, 13.50)
fnCutsceneMove("ScanGolem", 29.25, 13.50)
fnCutsceneBlocker()
fnCutsceneTeleport("ScanGolem", -100.25, -100.50)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("55:[VOICE|Tiffany] It's just up this ladder...") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("Sophie", -1, 0)
fnCutsceneBlocker()
fnCutsceneTeleport("Tiffany", 11.25, 14.50)
fnCutsceneTeleport("SX-399", 11.25, 14.50)
fnCutsceneBlocker()
fnCutsceneMove("Sophie", 13.25, 13.50)
fnCutsceneMoveFace("Tiffany", 12.25, 14.50, -1, 0)
fnCutsceneBlocker()
fnCutsceneFace("Sophie", -1, 1)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Tiffany", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] So this is the SX-399 I've been hearing about.[P] And you must be 55![B][C]") ]])
fnCutscene([[ Append("55:[E|Upset] We don't have time for pleasantries.[P] Her core has been losing power at an alarming rate.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Surprised] Oh my![P] Quick, get her to my scanning platform and hook her to the secondary system!") ]])
fnCutsceneBlocker()
fnCutsceneWait(15)
fnCutsceneBlocker()

--Movement.
fnCutsceneMove("Sophie", 19.25, 13.50, 1.70) 
fnCutsceneMove("Sophie", 19.25, 16.50, 1.70) 
fnCutsceneMove("Sophie", 18.25, 16.50, 1.70) 
fnCutsceneFace("Sophie", 0, -1)
fnCutsceneMoveFace("Tiffany",    20.25, 14.50, -1, 0, 1.50)
fnCutsceneMoveFace("SX-399", 19.25, 14.50,  0, 1, 1.50)
fnCutsceneBlocker()
fnCutsceneFace("Tiffany", -1, 1)
fnCutsceneMoveFace("SX-399", 19.25, 15.50,  0, 1, 0.20)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Tiffany", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Okay, she's hooked into my systems...[P] but the scanning software isn't much help.[P] Guess I have to do it the old fashioned way.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Are you all right, SX-399?[B][C]") ]])
fnCutscene([[ Append("SX-399:[E|Neutral] ....... Ha haaaa, oh I'm charging up![P] Wow![P] Woooow![B][C]") ]])
fnCutscene([[ Append("SX-399:[E|Neutral] This is great![P] I didn't think we were going to make it but wo[P]o[P]o[P]oahhh![B][C]") ]])
fnCutscene([[ Append("55:[E|Smirk] I'm so relieved![P] We made it![B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] We're not done yet, 55.[P] This...[P] is going to take some real work.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] What can I do to help?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Cover the door.[P] Close it, make sure nobody barges in.[P] I really don't want to have to explain this to anyone.") ]])
fnCutsceneBlocker()

--Resume.
fnCutsceneMove("Tiffany", 27.25, 13.50)
fnCutsceneFace("Tiffany", 1, 0)
fnCutsceneWait(25)
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "SX-399", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
fnCutscene([[ Append("SX-399:[E|Neutral] So you're Sophie, are you?[P] Christine said you were pretty, but I had no idea![B][C]") ]])
fnCutscene([[ Append("SX-399:[E|Neutral] Christine's really nice, doing all this for me.[P] I heard you guys were rebels and -[P] oops, am I saying stuff I shouldn't?[B][C]") ]])
fnCutscene([[ Append("SX-399:[E|Neutral] Because I support you all the way.[P] All we have to do is convince mother and I think we'll have a real chance![B][C]") ]])
fnCutscene([[ Append("SX-399:[E|Neutral] ...[P] How come you're not saying stuff?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Because this is...[P] bad...[P] very bad...[B][C]") ]])
fnCutscene([[ Append("SX-399:[E|Neutral] You know what's wrong?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Everything...[B][C]") ]])
fnCutscene([[ Append("SX-399:[E|Neutral] Can you fix it?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Fix it?[P] Fix it!?[P] Hun, there's nothing to fix![P] You're a pile of rust and scrap![B][C]") ]])
fnCutscene([[ Append("SX-399:[E|Neutral] *sigh*[P] I expected that.[P] So this was nice, but futile - [P][CLEAR]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] There's only one choice...[B][C]") ]])
fnCutscene([[ Append("SX-399:[E|Neutral] One choice?[P] What's the choice?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] I'll need you to suspend cognitive operations for a minute.[P] Begin standby procedure.[B][C]") ]])
fnCutscene([[ Append("SX-399:[E|Neutral] If you say so...") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Black out the screen.
fnCutsceneWait(125)
fnCutscene([[ AL_SetProperty("Activate Fade", 125, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Level transition.
fnCutscene([[ AL_BeginTransitionTo("SprocketCityA", "FORCEPOS:56.0x23.0x0") ]])
fnCutsceneBlocker()

