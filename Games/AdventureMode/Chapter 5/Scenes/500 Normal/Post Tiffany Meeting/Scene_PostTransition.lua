--[After 55 Encounter]
--After meeting 55 under the maintenance bay, this scene plays in RegulusCityB outside the elevator.

--Spawn Sophie.
TA_Create("Sophie")
	TA_SetProperty("Position", -100, -100)
	TA_SetProperty("Clipping Flag", false)
	fnSetCharacterGraphics("Root/Images/Sprites/Sophie/", true)
	TA_SetProperty("Facing", gci_Face_North)
DL_PopActiveObject()
		
--Music fades out.
AL_SetProperty("Music", "Null")
		
--Positioning.
fnCutsceneTeleport("Christine", 6.25, 14.50)
fnCutsceneTeleport("Sophie", 6.25, 15.50)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 0, 1)
fnCutsceneFace("Sophie", 0, -1)

--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
fnCutscene([[ Append("Christine:[E|Smirk] Sophie?[P] Were you waiting for me?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Sad] Oh, I was just about to come up and check on you.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] Whatever for?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Sad] You've been defragmenting for sixteen hours.[P] I got a little worried.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] Well...[P] I had a lot of things I needed to work through...[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] I was thinking about yesterday...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Offended] *Let's go someplace we won't be overheard.*[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] *Uhh...*[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] *The repair bay is safe.*[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Offended] *Really?*[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] *Hee hee![P] The audio pickups in the bay have been on the fritz forever![P] If only they'd assign them higher priority...*[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] *Oh you devilish unit you...*[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] Come on, let's go to 'work'!") ]])
fnCutsceneBlocker()
	
--Set Lua globals.
giFollowersTotal = 1
gsaFollowerNames = {"Sophie"}
giaFollowerIDs = {0}

--Get Sophie's uniqueID. We also need to remove Sophie's dialogue flag and collisions.
EM_PushEntity("Sophie")
	local iSophieID = RE_GetID()
	TA_SetProperty("Clipping Flag", false)
	TA_SetProperty("Activation Script", "Null")
DL_PopActiveObject()

--Store it and tell her to follow.
giaFollowerIDs = {iSophieID}
AL_SetProperty("Follow Actor ID", iSophieID)

--Move Sophie onto Christine's position.
fnCutsceneMove("Sophie", 6.25, 15.50)
fnCutsceneBlocker()

--Fold the party.
fnCutscene([[ AL_SetProperty("Fold Party") ]])
fnCutsceneBlocker()
		
--Music fades out.
fnCutscene([[ AL_SetProperty("Music", "RegulusCity") ]])

--Variables.
VM_SetVar("Root/Variables/Chapter5/Scenes/iSophieImportantMeeting", "N", 1.0)