-- |[After 55 Encounter]|
--After meeting 55 under the maintenance bay, this scene plays in Christine's quarters.

--Clear censor bars.
WD_SetProperty("Clear Censor Bars")

--Spawn Sophie.
TA_Create("Sophie")
	TA_SetProperty("Position", -100, -100)
	TA_SetProperty("Clipping Flag", true)
	fnSetCharacterGraphics("Root/Images/Sprites/Sophie/", true)
	TA_SetProperty("Facing", gci_Face_South)
DL_PopActiveObject()

--Spawn the defragmentation pod in the correct position. Constructor won't put it int he right spot.
TA_Create("DefragPod")
	TA_SetProperty("Clipping Flag", true)
	TA_SetProperty("Facing", gci_Face_South)
	TA_SetProperty("Activation Script", "Null")
	fnSetApartmentObjectGraphics(gci_Quarters_DefragPod)
	
	--Special activation script and directions.
	TA_SetProperty("Activation Script", gsRoot .. "Maps/RegulusCity/RegulusCityZ/Activation Defrag.lua")
	for q = 1, 4, 1 do
		TA_SetProperty("Move Frame", 0, q-1, "Root/Images/Sprites/Quarters/Tube1")
		TA_SetProperty("Move Frame", 2, q-1, "Root/Images/Sprites/Quarters/Tube2")
		TA_SetProperty("Move Frame", 4, q-1, "Root/Images/Sprites/Quarters/Tube0")
		TA_SetProperty("Move Frame", 6, q-1, "Root/Images/Sprites/Quarters/Tube3")
	end
DL_PopActiveObject()

--Move the defragmentation pod. It needs to be offset with decimals.
fnCutsceneTeleport("DefragPod", 14.30, 3.00 + 0.50)
		
--Set extra options. Rendering depth must be set AFTER the object is moved.
EM_PushEntity("DefragPod")
	TA_SetProperty("Rendering Depth", -0.500000 + (4.00 * 0.000100))
DL_PopActiveObject()
		
--Music fades out.
AL_SetProperty("Music", "Null")
		
--Positioning.
fnCutsceneTeleport("Christine", 9.25, 10.50)
fnCutsceneTeleport("Sophie", 8.25, 10.50)
fnCutsceneBlocker()
fnCutsceneFace("Christine", -1, 0)
fnCutsceneFace("Sophie", 1, 0)

--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()

--Fade.
fnCutsceneWait(45)
fnCutsceneBlocker()

--Move them in a bit. Sophie leads Christine to her defrag pod.
fnCutsceneFace("Christine", 0, -1)
fnCutsceneMove("Sophie", 8.25, 9.50)
fnCutsceneBlocker()
fnCutsceneMove("Sophie", 10.25, 9.50)
fnCutsceneFace("Christine", 1, -1)
fnCutsceneBlocker()
fnCutsceneMove("Sophie", 14.25, 9.50)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneFace("Sophie", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
fnCutscene([[ AL_SetProperty("Open Door", "DoorA") ]])
fnCutsceneBlocker()
fnCutsceneFace("Sophie", -1, 1)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Come along, it's all right.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] I feel awful for worrying you like this.[P] I'm fine, really.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneMove("Sophie", 11.25, 9.50)
fnCutsceneMove("Christine", 10.25, 9.50, 0.75)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 1, 0)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Sad") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Sad") ]])
fnCutscene([[ Append("Sophie:[E|Sad] You've got something on your mind, don't you?[P] It's pretty obvious.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] ...[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] I'm sorry.[P] It's none of my business.[P] Good night.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] ...[P] Wait.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] Sophie...[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] Yes?[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] That wasn't just any rogue unit in there.[P] She's the one who turned me into a golem.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] We met at the cryogenics facility south of here.[P] I was human then.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] The cryo -[P][EMOTION|Sophie|Surprised] oh my![B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Surprised] So there was a survivor of the attack.[P] I didn't know that.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] Please keep it a secret.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] ...[P] You're very lucky, you know that?[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] Come again?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] There are microphones everywhere in the city picking up the audio and tracking it.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] It's to allow doors and objects to respond to voice commands, but there's a direct line that the administrators use to record everything.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] They know exactly what we're saying, all the time.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Offended] So, we can't talk here?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] Hee hee![P] I happen to have a work order on the very bottom of priority list to install the microphones in your quarters, actually.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] It's my job to install and maintain them.[P] So, I know where the ones that work and the ones that don't happen to be.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Laugh] Sophie, you're a lifesaver![B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] We're free to chat here.[P] Just don't say anything in the elevator or the hallways, or they'll hear it.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Sad] ...[P] I don't want them to take you away for questioning...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] [EMOTION|Sophie|Neutral]That rogue unit...[P] the one in the basement...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] She told me that she had her memory wiped.[P] Neither of us knew what happened at the Cryogenics Facility.[P] I still don't.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Offended] I don't want to be a part of that.[P] I want to put it behind me.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] I just want to be here.[P] With you.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] ...[P] But?[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Offended] There's no but.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Yes, there is.[P] If there wasn't, you wouldn't have brought it up.[P] I would have let it slide.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] Weren't you the one who said::[P] Who you were doesn't matter, what matters is now?[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Offended] ...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Offended] ...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Offended] I just can't forget this...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Offended] What exactly happened at the cryogenics facility?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Nobody knows.[P] The records were all redacted.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Unofficially...[P][EMOTION|Sophie|Sad] I saw a lot of units in combat gear head out from the southern airlock.[P] They didn't come back.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Since then, our headphones have been on lockdown and the fabricator units have been working non-stop on weapons and combat equipment.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] .........[P][EMOTION|Sophie|Sad] but I'm sure it will be all right.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] I want to believe it will, but what if 2855 is right?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Unit 2855?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Surprised] Former Head of Security, Command Unit 2855?[P] THAT 2855?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Surprised] She's the rogue unit?[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] Is this a big deal?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Surprised] She used to be the head of security for all of Regulus City![P] She's one of the prime command units![B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Sad] If she's gone maverick...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] But she said her memory got wiped.[P] Do you have any idea why?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Sad] I'm not a subversive unit...[P] I'm a good unit...[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Sad] Don't let anyone hear that you know anything about 2855.[P] They'll come and they'll take you away.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Sad] Do you hear me?[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] I do...[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Sad] Okay...[P] okay...[P] it'll be okay...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] I shouldn't have worried you.[P] We're both productive, law-abiding units.[P] Nobody has any reason to suspect us of anything.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Yes, of course.[P] We haven't done anything wrong.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Now go defrag your drive.[P] You've had a long day.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] Yeah.[P] Yeah.[P] Thanks.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
		
--Fade to black.
fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

--Dialogue.
fnCutsceneWait(85)
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "PDU") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "PDU", "PDU") ]])
fnCutscene([[ Append("Christine:[E|PDU] PDU?[B][C]") ]])
fnCutscene([[ Append("PDU:[E|Alarmed] Ready for action, Lord Unit![B][C]") ]])
fnCutscene([[ Append("Christine:[E|PDU] You can access the network discreetly, right?[B][C]") ]])
fnCutscene([[ Append("PDU:[E|Neutral] Indeed.[P] I have high quality encryption routines designed for untraceable access.[B][C]") ]])
fnCutscene([[ Append("PDU:[E|Neutral] These routines were installed to bypass security in the cryogenics facility before you picked me up.[P] They will be adequate for this network.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|PDU] See if you can find out what happened at the cryogenics facility.[P] And see what you can find out about Unit 2855.[B][C]") ]])
fnCutscene([[ Append("PDU:[E|Alarmed] Right away, Chris.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|PDU] PDU, change login name string to 'Christine'.[P] Thank you.[B][C]") ]])
fnCutscene([[ Append("PDU:[E|Happy] Right away, Christine.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ Append("[VOICE|Christine]Unit 771852 logging off for the night...'[B][C]") ]])
fnCutscene([[ Append("...[B][C]") ]])
fnCutscene([[ Append("Christine awoke suddenly to a chill air.[P] She looked about in a daze as she tried to sit up, but found herself held tightly to her bed.[P] Clawing at what lay atop her, she grasped a heavy, overstuffed blanket and flung it to the floor.[B][C]") ]])
fnCutscene([[ Append("Sitting up, she glanced around.[P] She was in her room, in her ancestral home.[P] It was a house that had been in the Dormer family for centuries.[P] Each generation had walked the very same halls that she did.[B][C]") ]])
fnCutscene([[ Append("It was a home she knew, and yet...[B][C]") ]])
fnCutscene([[ Append("A strangeness lingered in the cool air that clouded her mind.[P] She sat there, undressed, covered only by the heavy blanket.[P] How had she gotten to her bed?[P] How long had she been there?[P] She could not remember going to sleep the night before.[B][C]") ]])
fnCutscene([[ Append("A shiver crept along her spine.[P] Her window had been left open, and the cold night air seeped in.[P] She lifted herself from her bed and stared with empty thoughts that mimicked the empty, starless darkness beyond before closing it.[B][C]") ]])
fnCutscene([[ Append("The chill lingered, and she pulled the discarded blanket around herself.[P] Even as she warmed herself at the edge of her bed, a prickling shiver lingered across her skin.[B][C]") ]])
fnCutscene([[ Append("Her room was bare save for her bed,[P] and the walls bore no photos or posters.[P] Even the antique desk at which she had spent so many years of her life studying was absent.[P] Something was amiss.[B][C]") ]])
fnCutscene([[ Append("A dim light flickered to life beside her as she sat, betraying the presence of a phone.[P] Her phone.[P] Had she fallen asleep while playing on it?[B][C]") ]])
fnCutscene([[ Append("She stared down into the dim light.[P] An unfamiliar name adorned the locked screen of the phone.[P] 'Chris Dormer'.[B][C]") ]])
fnCutscene([[ Append("She had never had a nickname, in part to her parents thinking such a thing to be far too crude.[P] Not even her friends, children of similar social status, would ever call her by such a name.[B][C]") ]])
fnCutscene([[ Append("She hesitated before picking it up, and entered her key with a practiced hand.[P] The phone flickered to life.[P] It could belong to no other person.[B][C]") ]])
fnCutscene([[ Append("She stared into the screen as the phone displayed the new message.[B][C]") ]])
fnCutscene([[ Append("There was no name attached to the message.[P] Not even a number was displayed with it.[P] Christine could not think of any friend who would play such a prank,[P] or even know how.[B][C]") ]])
fnCutscene([[ Append("'I will return[P][P][P] soon.'[B][C]") ]])
fnCutscene([[ Append("A sudden knock at her door roused her from her stupor, and she spun to face it with a start.[P] Her pulse quickened and she clutched the blanket about her.[P] Who would come at this hour?[P] Had she awoken her parents?[P] Had one of the servants heard her moving about?[B][C]") ]])
fnCutscene([[ Append("She looked about and found herself standing before the door.[P] She did not remember walking towards it, but could not think of any other way she could have reached it.[P] She stared into the dark outline of the knob, and her heart filled with dread.[P] Clutching the blanket tighter, she opened the door.[B][C]") ]])
fnCutscene([[ Append("A single, solitary, unblinking eye filled the doorway and greeted her gaze.[P] She staggered backwards in shock and stumbled over the heel of the blanket, pulling it from her grasp.[P] The eye narrowed and focused on her.[P] Her body froze and she stared back, unable to cover her nakedness.[B][C]") ]])
fnCutscene([[ Append("The eye belonged to some great, grand creature.[P] It pulled back from the doorway to reveal the empty nothingness behind it.[P] At once Christine felt as if she were falling into it, beyond the hallway and into the nothingness that followed in the wake of the eye.[B][C]") ]])
fnCutscene([[ Append("She shook her head, begging her mind for clarity as she struggled to overcome the rapidly-encroaching nothingness.[P] The creature that the eye belonged to roared as she fought with herself, sending a rumbling tremor through the room.[P] She shook herself alongside the quaking room and lurched towards her bed, desperate to keep herself from falling.[B][C]") ]])
fnCutscene([[ Append("The creature roared louder as she grasped her bed.[P] A terrible curse seemed to echo from the noise, at once both utterly foreign and terrifyingly familiar to Christine.[P] A pure hatred emanated from it and seemed to coalesce into puddles about her feet.[P] Christine held on to her bedpost for dear life.[B][C]") ]])
fnCutscene([[ Append("Christine shut her eyes against the sound as she held on to her bedpost, pleading to whatever power might listen for salvation from the pooling hate.[B][C]") ]])
fnCutscene([[ Append("Slowly the shaking stopped, but Christine dared not release her grip.[P] Her bed had held steady.[P] She realized there was now someone else in the room with her.[P] Shaking, she opened her eyes and turned to see him.[B][C]") ]])
fnCutscene([[ Append("He loomed above her, a man adorned in a faded grey schoolmaster's suit.[P] His head reached the ceiling that seemed to be falling into the sky, and he grew along with it.[P] He looked down upon her and glared at her with a fiendish grin.[B][C]") ]])
fnCutscene([[ Append("He pointed at her.[B][C]") ]])
fnCutscene([[ Append("'Not[P] real',[P] he said,[P] 'Not[P] true.'[B][C]") ]])
fnCutscene([[ Append("She sniffled as tears began to well up in her eyes.[P] He was right.[P] She was not real.[P] None of this was real.[P] She fought back the tears but they poured forth despite her efforts.[P] The man began to laugh as he vanished into the darkness that swallowed the room.[B][C]") ]])
fnCutscene([[ Append("...[B][C]") ]])
fnCutscene([[ Append("[VOICE|Christine]Christine:[VOICE|Christine] Unit 771852 resuming cognitive functions.") ]])
fnCutsceneBlocker()

fnCutsceneWait(105)
fnCutsceneBlocker()
		
--Level transition.
local sPath = fnResolvePath() .. "Scene_PostTransition.lua"
fnCutscene([[ AL_BeginTransitionTo("RegulusCityB", "]] .. sPath .. [[") ]])
fnCutsceneBlocker()
