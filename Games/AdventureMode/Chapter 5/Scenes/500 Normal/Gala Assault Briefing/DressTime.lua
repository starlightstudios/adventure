-- |[Dress Time]|
--Takes place in Christine's quarters. Sophie and Christine get ready for the gala.
fnLoadDelayedBitmapsFromList("Chapter 5 Dresses", gciDelayedLoadLoadAtEndOfTick)

--Clear censor bars.
WD_SetProperty("Clear Censor Bars")

--Spawn characters, position existing ones.
fnCutsceneTeleport("Christine", 8.25, 10.50)
fnSpecialCharacter("Sophie", 9, 10, gci_Face_North, false)

--Black the screen out.
fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(125)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] [MUSIC|Null]Tonight's the big night...[P] Sophie...[P] are you excited?") ]])
fnCutsceneBlocker()

--Fade in.
fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneWait(65)
fnCutsceneBlocker()

--Movement.
fnCutsceneMove("Christine", 8.25, 7.50)
fnCutsceneMove("Sophie", 9.25, 7.50)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 1, 0)
fnCutsceneFace("Sophie", -1, 0)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
fnCutscene([[ Append("Sophie:[E|Blush] Excited.[P] Nervous![P] More nervous than excited![P] Hee![P] Hee![B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Blush] B-b-b-[P]but I'm most nervous about the dresses I made...[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Blush] I s-s-[P]so hope you'll l-l-[P]like them![P] Hee hee![B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] (Okay brain, listen up.)[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] (No matter whether you like or hate the dress, what's most important is responding positively,[P] immediately.)[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] (If you wait for more than 0.002 seconds she will assume you're coming up with a lie.[P] Respond positively, immediately.)[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Offended] (You've let me down a lot in the past, brain, but this is when it counts most.[P] Don't screw this up for me.)[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] I can't wait.[P] Are they already in here?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Blush] Yep![P] I stashed them in here earlier![P] J-j-[P]just -[P] a second...[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Blush] Um...[P] here.[P] Wear this first.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] A blindfold?[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] ...[P] I will also disable the sonar function on my auditory receptors.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Lord Golems have those?[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] It comes with the extended package of sensors.[P] It's only reliable at short range.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Oh, I almost forgot.[P] You need to get my killphrase plate off, and unscrew my headphones.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] Way ahead of you.[P] I also brought some paints.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Paints?[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] We need to deepen your eyes and paint over your cognitive indicator light.[P] Mine is yellow, see?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Oh oh oh, I totally forgot...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] Eeee, I get to do your makeup![P] C'mere!") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Fade out.
fnCutsceneWait(45)
fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()
fnCutsceneWait(65)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] Unit 499323, disable motor functionality.[B][C]") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] Affirmative.[P] Motor functions suspended.[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] PDU, please bridge these wires here...[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] I'll just unscrew this...[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] Okay now the tricky part...[B][C]") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] C-[P]Christine?[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] Yes?[B][C]") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] You can't let me see or hear my killphrase...[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] Yes I can.[P] See?[B][C]") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] !!![B][C]") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] It -[P] didn't do anything?[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] The killphrase on your plate goes to a lookup table in a Lord Unit's database.[P] This isn't your actual killphrase, it's the table key.[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] Your real killphrase is 'Flatlander Machine'.[B][C]") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] C-C-[P]Christine![P] How could you![B][C]") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] ...[P] Nothing happened...[P] again?[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] Of course not.[P] I've removed the receptor spike.[P] Your killphrase is now permanently disabled.[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] ...[P] Sophie, I don't know what I'll be called upon to do once the shooting starts.[P] But this is what I need you to do.[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] When you remove a unit's killphrase, they are well and truly freed.[P] It's difficult for an untrained unit, but I've been practicing.[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] Our operatives have had theirs removed.[P] Now, so do you.[B][C]") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] Am I...[P] free?[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] Unit 499323, resume motor functions.[B][C]") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] Affirmative.[P] Standing up.") ]])
fnCutsceneBlocker()
fnCutsceneWait(65)
fnCutsceneBlocker()

--Scene.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/GalaDress/Seq0") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie][MUSIC|SophiesTheme] How do I look?[P] Hee hee...[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] ...[P] Liberated.[B][C]") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] My headdress should cover up my ears and phraseplate, so...[B][C]") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] Get that blindfold on, dearest.[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] Whatever you say.") ]])
fnCutsceneBlocker()
fnCutsceneWait(65)
fnCutsceneBlocker()

--Scene.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] Will you stop squirming?[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] I'm not -[P] I'm not squirming![B][C]") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] Unit 778152, disable motor functionality.[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] Oh for crying out loud![P] Motor functions suspended![B][C]") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] Hee hee![P] You're looking...[P] Mmmmm...[B][C]") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] And this clips behind your ear...") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Scene.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/GalaDress/Seq1") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] So?[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] ...![P] Sophie![P] Sophie![B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] It's me![P] It's perfect![P] This dress is perfect![B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] Sophie![P] I love you![P] I love you so much![P] *smooch smooch*[B][C]") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] I knew you would, but the blind fold goes back on.[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] Whatever for?[B][C]") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] Because I have to get mine on, silly![P] Just a moment, and no peeking!") ]])
fnCutsceneBlocker()
fnCutsceneWait(65)
fnCutsceneBlocker()

--Scene.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] [MUSIC|Null]Keep that blindfold on![P] Just -[P] hold out your hand and hold this for me...[B][C]") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] Snap that in there...") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] [MUSIC|SophiesThemeSlow]...[P] Don't...[P] don't take the blindfold off.[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] Why not?[B][C]") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] Because...[P] because...[B][C]") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] Just...[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] I'm taking it off.[P] I hope you're ready.[B][C]") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] I'm -[P] I'm not![P] I'm not![B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] You've got five seconds.[P] Better hurry![B][C]") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] Eep!") ]])
fnCutsceneBlocker()
fnCutsceneWait(300)
fnCutsceneBlocker()


fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/GalaDress/Seq2") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] Tell me dearest.[P] What do you think?[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] Sophie...[P] it's...[B][C]") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] ...[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] I don't even know...[B][C]") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] ...[P] You don't...[P] like it...[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] I'm furious![B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] It's even prettier than mine![P] I'm going to get upstaged![P] Ha ha ha![B][C]") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] Ahhhh, I'm sorry.[P] This is just the design I've been thinking of ever since I was first converted...[B][C]") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] I love to stitch and sew, but the designing is the most fun part.[B][C]") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] For 0.002 seconds there...[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] Not the first time my brain let me down, don't worry.[B][C]") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] I knew you'd love it.[P] I shouldn't have doubted you.[P] Hee hee![B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] Sophie...[B][C]") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] Shall we be off, tandem unit?[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] I...[P] yes.[P] We cannot be late, can we?[B][C]") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] Lead the way, dearest.") ]])
fnCutsceneBlocker()

--Make Sophie a party member.
fnAddPartyMember("Sophie", "Null", "Null")

--Inform the game that Christine is wearing her gala dress.
VM_SetVar("Root/Variables/Costumes/Christine/sCostumeGolem", "S", "Gala")
VM_SetVar("Root/Variables/Costumes/Sophie/sCostumeGolem", "S", "Gala")
fnCutscene([[ LM_ExecuteScript(gsCostumeAutoresolve, "Christine_Golem") ]])
fnCutscene([[ LM_ExecuteScript(gsCostumeAutoresolve, "Sophie_Golem") ]])

--Clear images.
fnCutsceneWait(gciDialogue_Fadeout_Then_Unload_Ticks)
fnCutsceneBlocker()
fnCutscene([[ fnUnloadBitmapsFromList("Chapter 5 Dresses") ]])

--Next scene.
fnCutscene([[ AL_BeginTransitionTo("RegulusCityB", "FORCEPOS:6.5x14.0x0") ]])
fnCutsceneBlocker()
