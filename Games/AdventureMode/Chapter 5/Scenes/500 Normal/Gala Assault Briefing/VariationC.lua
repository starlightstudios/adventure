--[Gala Assault Variation C]
--Hardass JX-101 is here to set things straight!
TA_Create("TealLeader")
    TA_SetProperty("Position", 23, 7)
    TA_SetProperty("Facing", gci_Face_West)
    TA_SetProperty("Clipping Flag", false)
    fnSetCharacterGraphics("Root/Images/Sprites/GolemSlave/", false)
DL_PopActiveObject()
TA_Create("GoldLeader")
    TA_SetProperty("Position", 23, 8)
    TA_SetProperty("Facing", gci_Face_West)
    TA_SetProperty("Clipping Flag", false)
    fnSetCharacterGraphics("Root/Images/Sprites/GolemSlaveP/", false)
DL_PopActiveObject()
TA_Create("RedLeader")
    TA_SetProperty("Position", -100, -100)
    TA_SetProperty("Facing", gci_Face_South)
    TA_SetProperty("Clipping Flag", false)
    fnSetCharacterGraphics("Root/Images/Sprites/JX-101/", true)
DL_PopActiveObject()

--Move Christine and 55.
fnCutsceneMove("Christine", 6.25, 7.50)
fnCutsceneFace("Christine", 1, 0)
fnCutsceneFace("Tiffany", 1, 0)

--Teal and Gold enter.
fnCutsceneMove("TealLeader", 11.25, 7.50)
fnCutsceneMove("GoldLeader", 11.25, 8.50)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Christine", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Tiffany", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Golem", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemB", "Neutral") ]])
fnCutscene([[ Append("Teal:[E|Neutral] And so we find out that our mysterious benefactor is, in fact, a Command Unit.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Welcome, Teal and Gold leaders.[P] We're just waiting for Red Leader, and we can begin.[B][C]") ]])
fnCutscene([[ Append("Gold:[E|Neutral] I presume you're Blue leader, but who's the Lord Unit?[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Pink Leader.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Offended] Pink?[P] Not purple?[P] Not violet?[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Codenames are to be single-syllable for simplicity.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] Fine.[B][C]") ]])
fnCutscene([[ Append("Teal:[E|Neutral] How are we supposed to know you're on the level if we don't even know your designations?[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] Is this the first time you've actually met these units...[P] Blue Leader?[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Yes.[P] Meeting in person is unnecessary barring utmost secrecy.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] We are meeting here because the plans we are discussing are not to be on the network, not even as encrypted data.[P] There is to be no trace left.[B][C]") ]])
fnCutscene([[ Append("55:[E|Smirk] To answer the question, you don't have any way to trust me, and cannot.[P] Not logically.[P] But you must take a chance.[B][C]") ]])
fnCutscene([[ Append("55:[E|Smug] After all, the only way to know if I am trustworthy is to trust me, is it not?[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] (Maybe she actually was listening to me...)[B][C]") ]])
fnCutscene([[ Append("Teal:[E|Neutral] That, or you're trying to get us to reveal important information before arresting us.[B][C]") ]])
fnCutscene([[ Append("Voice:[VOICE|JX-101] I'd consider that to be rather unlikely.") ]])
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()

--Movement.
fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
fnCutsceneTeleport("RedLeader", 6.25, 6.50)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Christine", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Tiffany", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "JX-101", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Golem", "Neutral") ]])
fnCutscene([[ Append("JX-101:[E|Neutral] I hope you didn't get started without me.[P] Bickering not withstanding.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Happy] JX-101![B][C]") ]])
fnCutscene([[ Append("JX-101:[E|Neutral] Or Red Leader, for our purposes.[B][C]") ]])
fnCutscene([[ Append("Teal:[E|Neutral] Woah, the famous leader of Fist of the Future?[B][C]") ]])
fnCutscene([[ Append("JX-101:[E|Neutral] We go by Fist of Tomorrow now, actually.[B][C]") ]])
fnCutscene([[ Append("Teal:[E|Neutral] Is it true you've been fighting since the first days of Regulus City?[B][C]") ]])
fnCutscene([[ Append("JX-101:[E|Neutral] Surviving, really.[P] It's been a challenge.[P] But all that ends soon, doesn't it?[B][C]") ]])
fnCutscene([[ Append("JX-101:[E|Neutral] By the way, Blue Leader, you might want to look into replacing those proximity sensors of yours.[B][C]") ]])
fnCutscene([[ Append("JX-101:[E|Neutral] The T-99's made a clicking sound when they register movement because the circuit shorts when the light gets interrupted.[P] So your target will know they've been detected.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Noted.[B][C]") ]])
fnCutscene([[ Append("JX-101:[E|Neutral] But enough chit-chat, let's get down to business.[P] I'm eager to here this plan Blue Leader has been hinting at.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] Great to see you again, JX![B][C]") ]])
fnCutscene([[ Append("JX-101:[E|Neutral] The pleasure is all mine, Pink Leader.[P] SX-399 sends her regards.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneMove("RedLeader", 6.25, 9.50)
fnCutsceneMove("RedLeader", 7.25, 9.50)
fnCutsceneFace("RedLeader", 0, -1)
fnCutsceneMove("GoldLeader", 11.25, 9.50)
fnCutsceneMove("GoldLeader", 8.25, 9.50)
fnCutsceneFace("GoldLeader", 0, -1)
fnCutsceneMove("TealLeader", 11.25, 9.50)
fnCutsceneMove("TealLeader", 9.25, 9.50)
fnCutsceneFace("TealLeader", 0, -1)
fnCutsceneMove("Christine", 7.25, 7.50)
fnCutsceneFace("Christine", 0, 1)
fnCutsceneMove("Tiffany", 9.25, 7.50)
fnCutsceneFace("Tiffany", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Music starts.
fnCutscene([[ AudioManager_PlayMusic("Briefing") ]])

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Christine", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Tiffany", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Golem", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemB", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "JX-101", "Neutral") ]])
fnCutscene([[ Append("55:[E|Neutral] Now, I assume you all read the briefing package I sent you, but I need to make sure we all know our objectives.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] I also need to stress that there is no reason to assume our operation will be a success.[P] If we fail, do not mobilize.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] There will be other opportunities later.[P] Attacking prematurely will likely result in a total rout.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] ...[P] Since we all appear to be on the same page, Pink Leader will give you the outline.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] In two days, the annual Sunrise Gala will commence.[P] Blue Leader has determined that three of the Prime Command Units will be in attendance, along with dozens of other Command Units and hundreds of Lord Units.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] The gala has already been confirmed to have excessively high security, to be expected considering the guest list.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] Fortunately, we have a way in.[P] At least for me.[P] I have an invitation.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] Unfortunately, smuggling any sort of weapon in is too risky, as well as too obvious.[P] That's where Blue Leader comes in.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] The event is taking place at the Arcane University in Sector 0.[P] Approaches to the campus over the surface of Regulus are heavily covered by snipers on the building rooftops.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] In addition, surface access tubes have been locked down for the event.[P] Guests will be arriving via the transit system, which is likewise heavily patrolled.[B][C]") ]])
fnCutscene([[ Append("JX-101:[E|Neutral] I hope you're not suggesting a front assault.[P] We'd be decimated.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] No.[P] The security units will not hesitate to fire on any civilians approaching, so even disguises would be ineffective.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] A direct firefight will be likewise defeated due to the presence of barricades and lockdowns on the approach.[P] An assault is likely to fail.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] But there is a way in that the security forces have considered but passed over.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] There are cargo delivery trams that ship equipment to the Physics Research Block.[P] They are located sixteen floors underground.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] A single freight elevator provides access to the campus proper.[P] Further, because it's so far underground, radio waves cannot penetrate the rock.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] Blue Leader here will enter via these transit tunnels and, if necessary, fight her way to the freight elevator.[B][C]") ]])
fnCutscene([[ Append("Gold:[E|Neutral] And I assume you realize that the elevator will be alarmed.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Yes.[P] Any unauthorized access will lock the elevator down as well as summon a security detachment.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Pink Leader will leave the party two hours before sunrise and make her way to the physics building's basement.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Because the security forces cannot use radio-relays underground, there is a single cable that runs parallel to the elevator.[P] She will install a relay on that cable to suppress the alarms when I use it.[P] The relay is hidden in the battery case of her PDU.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] From there, we will move to the basement under the ballroom and plant shaped charges on the support pillars.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Lastly, a null-point charge will be placed directly beneath the ballroom.[P] When the shaped charges go off, the building will collapse inwards.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] I will then set off the null-point charge and destabilize every atom within two hundred meters.[P] Any survivors of the building collapse will be terminated by the resulting radiation storm.[B][C]") ]])
fnCutscene([[ Append("JX-101:[E|Neutral] And for any who survive the radiation storm?[P] Command Units particularly are well shielded against radiation.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Even if they do survive, it will be days before they can be dug out of the rubble.[P] They will be effectively neutralized.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Now, at this point, Teal Leader's team will form up in Sector 7 and wait until the security forces there move to the University.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] The security detachment there will likely panic and, lacking direct orders because their commander is dead, move to the University to assist with the crisis.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Teal Leader's team will deal with any resistance and steal the vehicles in the motor pool there.[P] Destroy any you cannot steal.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Red Leader and Gold Leader will then mobilize the riots in their respective sectors.[P] Distribute firearms to the civilians at your discretion.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Red Leader, I've assigned you sector 244 because the western wall can be breached to access the armory of sector 170.[B][C]") ]])
fnCutscene([[ Append("JX-101:[E|Neutral] Right.[P] I'll be sure to bring an extra set of fusion cutters in case the breaching charges don't get it done.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Good.[P] But be sure that you do not lose control of the civilian populace.[P] The rioters may damage infrastructure we need.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] Give the Lord Units a chance to surrender.[P] Give them a chance to join us.[P] But -[P] not a gun.[P] Not until they prove themselves.[B][C]") ]])
fnCutscene([[ Append("JX-101:[E|Neutral] Perhaps I ought to give them a gun with exactly one round...[B][C]") ]])
fnCutscene([[ Append("JX-101:[E|Neutral] Hrmpf.[P] Taking prisoners will be hard enough with a riot going on, but we'll try to direct them.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Just don't lose control of them.[P] Remember this is a battlefield, and we are fighting a war.[P] Do not tolerate looting.[P] Maintain discipline.[B][C]") ]])
fnCutscene([[ Append("Gold:[E|Neutral] Our teams will do what they can, but you're basically starting a wildfire here.[P] The riots will spread once the security forces fail to respond.[B][C]") ]])
fnCutscene([[ Append("Gold:[E|Neutral] And just what are Blue and Pink Leaders going to do once the charges are planted?[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] The Arcane University is attached to the biolabs.[P] We'll be exfiltrating through the habitation domes there, since the camera net is fairly spotty in the biological habitats.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] We'll have to fight our way through the lockdowns in the biolabs, but we've been in tighter spots before.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] After that, we'll rendezvous at the command post in Sector 96.[P] Once your objectives are completed, report there.[P] Send any volunteers that way as well.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] There are a number of craters south of Sector 96 where vehicles stolen by Gold Leader's team can be parked.[P] We will improvise a repair garage when we can.[B][C]") ]])
fnCutscene([[ Append("JX-101:[E|Neutral] So that's the plan, then.[P] Gutsy.[P] I like it.[B][C]") ]])
fnCutscene([[ Append("JX-101:[E|Neutral] You can count on the support of the Steam Droids.[P] Our reserves are in position for the second wave of attacks.[B][C]") ]])
fnCutscene([[ Append("Teal:[E|Neutral] What's the plan after we rendezvous in Sector 96?[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] The city will be in chaos for several days until the battle lines stabilize.[P] We will direct refugees to sectors we control while performing attacks of opportunity against vulnerable targets.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Remember that civilians are potential recruits.[P] Provide them recharge pods and repair services.[B][C]") ]])
fnCutscene([[ Append("JX-101:[E|Neutral] Civilians will also provide vital intelligence on the sectors they're fleeing.[P] We can question them to gain scouting data.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] But there is no way we've stockpiled enough repair supplies...[B][C]") ]])
fnCutscene([[ Append("Teal:[E|Neutral] We could prioritize capturing supply depots next.[P] Power plants would also be good targets.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] As a further reminder, if for any reason the operation fails, do not mobilize.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Without a critical blow to the leadership of Regulus City, their security forces will be well organized and far better equipped than ours.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] It will be a massacre to proceed otherwise.[P] Make sure none of your units move until the time is right.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Blush] Remember, my friends.[P] A new day dawns on Regulus.[P] Let's greet the sun as free units.[B][C]") ]])
fnCutscene([[ Append("Teal:[E|Neutral] To freedom, machine sisters.[B][C]") ]])
fnCutscene([[ Append("Gold:[E|Neutral] To freedom.[B][C]") ]])
fnCutscene([[ Append("JX-101:[E|Neutral] To freedom.[P] To the future![B][C]") ]])
fnCutscene([[ Append("Christine:[E|Laugh] To freedom![B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
fnCutscene([[ Append("55:[E|Smirk] To freedom.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

