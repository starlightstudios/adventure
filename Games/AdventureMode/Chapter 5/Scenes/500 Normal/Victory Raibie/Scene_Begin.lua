-- |[ =================================== Victory Over Raibie ================================== ]|
--Scene handler for winning a fight with the rabies in the biolabs. Advances story if applicable.
local iRaibieQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N")
if(iRaibieQuest ~= 1.0) then return end

--Set flag.
VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N", 2.0)

--Scene.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("Christine:[VOICE|Leader] (There, I got the fur samples Dr. Maisie needed.[P] Better go see her.)") ]])
fnCutsceneBlocker()