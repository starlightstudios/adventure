-- |[Videograph: A Talking Dark Matter Girl!?!]|
--Clear censor bars.
WD_SetProperty("Clear Censor Bars")

--Such a horrible tale.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ Append("As the lights in the room dim, the videograph begins playing.[P] A long series of shots pan over the moonscape as an apparently-drunk narrator describe the scenes as they appear.[B][C]") ]])
fnCutscene([[ Append("The voice ends with an audible hiccup as a group of survey units stumble upon a Dark Matter Girl.[P] The surveyors decide to adopt her and bring her along on their mission, where she begins talking to them.[P] She's a talking Dark Matter Girl?!?[B][C]") ]])
fnCutscene([[ Append("The videograph cuts away to a completely unrelated pair of units as they argue over mundane familial issues for no apparent reason.[P] One of the units wants to be a tandem unit with the other, who remains undecided.[B][C]") ]])
fnCutscene([[ Append("The Dark Matter Girl suddenly enters the scene, and after a monotonous monologue over what is apparently supposed to be a montage, she helps the two units become tandem units.[B][C]") ]])
fnCutscene([[ Append("As she's leaving the domestic block, the Dark Matter Girl is struck by an out-of-control tram.[P] She lives with no apparent injuries, but rushes off after being startled by an elongated, unmoving green vegetable.[B][C]") ]])
fnCutscene([[ Append("The overall acting is terrible, and the videograph ends without ever returning to the original group of surveyors or attempting to explain how the Dark Matter Girl could speak.") ]])

fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(65)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
fnCutscene([[ Append("Christine:[E|Neutral] I think my CPU is running slower now that we watched that.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] The Dark Matter Girl was cute![B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] ...[P] Are you one of those units who looks up pictures of Dark Matter Girls on the station network?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] ...[P] Maybe.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Happy] Do you watch videographs of them doing cute things like knocking things over and chasing lights?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] ...[P]...[P]...[P][E|Happy] maybe.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] No problem with that, but that videograph was still bad.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] Hee hee hee![P] We should rewatch it just to see the cute girls again!") ]])
fnCutsceneBlocker()