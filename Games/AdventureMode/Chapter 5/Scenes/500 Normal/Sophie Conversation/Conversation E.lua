-- |[Sophie Conversation E]|
--This sequence plays during one of the dates with Sophie.
WD_SetProperty("Clear Censor Bars")
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ Append("The tandem units cheerfully chatted with one another as their lower-level routines did their work.[B][C]") ]])
fnCutscene([[ Append("Sophie kept her smile, even when the conversation took turns to dour territory.[P] Nothing made her happier than being with Christine.[B][C]") ]])
fnCutscene([[ Append("They would take lulls in their work schedule to hold hands and nuzzle one another's olfactory sensors.[P] No display of affection was too minor.[B][C]") ]])
fnCutscene([[ Append("They worked together in perfect harmony...") ]])