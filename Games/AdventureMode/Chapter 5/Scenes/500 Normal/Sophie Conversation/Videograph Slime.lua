-- |[Videograph: Slime Girls Are Easy]|
--Clear censor bars.
WD_SetProperty("Clear Censor Bars")

--Such a tragic tale. Yep.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ Append("As the lights in the room dim, the videograph begins playing.[P] Out-of-tune and off-beat saxophone music begins playing as a poorly-made title card with terrible kerning comes up,[P] 'Rime Gihs Are Easy'[B][C]") ]])
fnCutscene([[ Append("The scene opens up in an unnamed forest on Pandemonium.[P] A pair of hikers are bragging to each other about their latest 'conquests' when they encounter a slime girl.[B][C]") ]])
fnCutscene([[ Append("One of the pair dares the other to 'conquer' the slime girl, and is himself dared the same by his friend.[P] The pair quickly begin violating the porous body of the slime, only to find themselves being turned into slime girls, as well.[B][C]") ]])
fnCutscene([[ Append("The two newly-made slime girls are soon having sex with each other as well as the original slime girl, and seem to immediately switch partners with each climax.[B][C]") ]])
fnCutscene([[ Append("All three slimes are the same color, making it difficult to see who is penetrating what and where,[P] or if any penetration is happening at all as opposed to simply merging and separating.[B][C]") ]])
fnCutscene([[ Append("Overall, it's difficult to tell what's going on, and the videograph mercifully fades out after 30 minutes as the slime girls continue the rather-unerotic orgy.") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(65)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] Ohhhh, I'd love to couple with a slime girl...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] You would?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] Heh.[P] Are you not into that?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] Slimes don't have to worry about things like work and fitting in.[P] They just ooze over things and have sex whenever they want.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] That's a porno, not real life.[P] They probably have to worry about getting eaten.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] Maybe.[P] I still wish I could couple with one.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Just once.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] I could put in a request with the abductions units...[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Blush] Eek![P] Don't![B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] Mwahaha!") ]])