-- |[Document: The Doll and the Bolts]|
--Such a tragic tale.

--Clear censor bars.
WD_SetProperty("Clear Censor Bars")

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ Append("Christine and Sophie linked their cables together and plugged into the computer.[P] The document loaded and began transmitting into their architectures.[B][C]") ]])
fnCutscene([[ Append("They began to read a story about a Command Unit who,[P] after a particularly productive day that she had overseen,[P] saw a pair of high-quality tungsten bolts sitting atop a high shelf.[B][C]") ]])
fnCutscene([[ Append("She did not have any particular need for the bolts, but they were of excellent quality and precision, and they shone with a shimmering luster.[P] Thoughts of what she could use such perfect bolts for filled her mind.[B][C]") ]])
fnCutscene([[ Append("The shelf, however, was high above the ground, and not only was there no ladder around,[P] but she could find no other units to use as a stool.[B][C]") ]])
fnCutscene([[ Append("She reached for them anyway, stretching as high as her arms and legs would hold her,[P] but she could not reach them.[P] No amount of stretching and straining would allow her to reach the bolts.[B][C]") ]])
fnCutscene([[ Append("She began to pout and grow angry as the bolts taunted her from beyond her reach.[B][C]") ]])
fnCutscene([[ Append("'I don't need those stupid bolts!' she shouted. 'I'll bet they're rusted, anyway!'[B][C]") ]])
fnCutscene([[ Append("The Command Unit stormed off with a huff, abandoning the bolts to the shelf.[B][C]") ]])
fnCutscene([[ Append("Her deductions were correct, of course.[P] The surface of the bolts only appeared to shine due to many years of metal shaving dust.[P] Beneath the dust, the bolts were rusted throughout, and would have broken if used for even the least of her projects.[B][C]") ]])
fnCutscene([[ Append("The moral of the story is that Command Units are wise beyond our understanding,[P] and perfect in every way without need for improvement.") ]])

fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(65)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
fnCutscene([[ Append("Christine:[E|Neutral] Huh.[P] What an odd story.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] What's odd about it?[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Offended] Doesn't the moral of the story seem a little out of place?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] But it's true.[P] That's the moral for almost all the stories involving Command Units.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] Oh, I know it's true and that Command Units are perfect, but I thought the moral would be something like, 'You can't always get what you want.'[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] That's pretty clever![P] But I think it would be more appropriate for a story about a golem.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] I suppose you're right.") ]])
fnCutsceneBlocker()