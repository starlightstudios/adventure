-- |[Videograph: Roberta and Jules]|
--Clear censor bars.
WD_SetProperty("Clear Censor Bars")

--Such a tragic tale.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ Append("As the lights in the room dim, the videograph begins playing.[P] A small structure can be seen in the distance, the only unnatural blemish marring the cratered moonscape.[B][C]") ]])
fnCutscene([[ Append("The camera zooms through the airlock and focuses on Unit PT-116, designated 'Roberta,' who is attending a gathering with numerous other steam droids.[P] She's confiding her romantic failures to a friend, who listens to the gossip with rapt attention.[B][C]") ]])
fnCutscene([[ Append("Just as the friend begins to try and coax more details from her, the gathering is interrupted by a group of Golems.[P] The Golems begin to disperse the crowd, and as Roberta is leaving, she sees Unit 51592, the Lord Golem leader 'Joliet.' [P]She becomes immediately smitten with her.[B][C]") ]])
fnCutscene([[ Append("As the Golems leave the remote facility, Roberta follows at a distance.[P] She's nearly seen on several occasions by Joliet, but each time she's just barely able to duck behind cover.[B][C]") ]])
fnCutscene([[ Append("She follows them back to Regulus City, and being unable to enter the city proper, Roberta sneaks into the waste ducts.[P] After several days of searching and near disasters with Scraprats, she locates the duct that leads to Joliet's room.[B][C]") ]])
fnCutscene([[ Append("For days Roberta pines for Joliet while weathering the savages of the ducts until, at last, Joliet takes notice of her.[P] She admits to having seen Roberta follow them, but chose not to alert the other Golems due to her own attraction to Roberta.[B][C]") ]])
fnCutscene([[ Append("Suddenly, security units pound on the door and demand entry.[P] Joliet shoves Roberta into the sanitation bay to hide her as the security units enter.[P] An infiltrator had been spotted by the scanners![B][C]") ]])
fnCutscene([[ Append("Joliet succeeds in bluffing to the security units, and retrieves a shivering, sterilized Roberta after they leave.[P] They spend the evening together and, in the morning, resolve to escape the city together.[B][C]") ]])
fnCutscene([[ Append("Knowing that they'll never be accepted by either the Golems or steam droids, they resolve to escape to an isolated location where they can be together.[B][C]") ]])
fnCutscene([[ Append("Joliet secures a rover using her access as a Lord Golem, and the pair set off into the sunset.[B][C]") ]])
fnCutscene([[ Append("The last shot is of the two leaning in to kiss each other, at which point the car abruptly explodes for no discernable reason.[B][C]") ]])
fnCutscene([[ Append("The credits roll.") ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(65)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
fnCutscene([[ Append("Christine:[E|Neutral] That videograph ended rather abruptly, didn't it?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] No, I think it ended when it was supposed to.[P] It followed the three-act structure perfectly.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] But why did the car blow up?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Isn't it obvious?[P] It detonated because the two units were incompatible.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Two units of different model variants can never be together or -[P] or...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Offended] That videograph was stupid.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] .[P].[P].[P][E|Offended] Terrible![P] Awful![B][C]") ]])
fnCutscene([[ Append("Christine:[E|Offended] I refuse to learn a single lesson from that story.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Offended] Agreed![B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] ...[P] But I do want to watch it again.[P] Joliet is so cute, and the acting was superb.") ]])