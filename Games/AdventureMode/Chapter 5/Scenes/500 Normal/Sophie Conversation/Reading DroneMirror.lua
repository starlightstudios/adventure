-- |[Document: The Drone and the Mirror]|
--Such a tragic tale.

--Clear censor bars.
WD_SetProperty("Clear Censor Bars")

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ Append("Christine and Sophie linked their cables together and plugged into the computer.[P] The document loaded and began transmitting into their architectures.[B][C]") ]])
fnCutscene([[ Append("They began to read a story about a latex drone who followed orders without hesitation.[P] Her hard work made her very productive, and she was known to be the most diligent latex drone in the city.[B][C]") ]])
fnCutscene([[ Append("To reward the hard-working drone for her work, her Lord Golem gifted her a shiny new spanner.[B][C]") ]])
fnCutscene([[ Append("She was so proud of her new spanner that the latex drone would dance and prance about, showing it off to all the other drones at every chance she had.[B][C]") ]])
fnCutscene([[ Append("She could tell that the other drones were not happy for her.[P] She watched as each one gazed upon her new spanner covetously when they thought she wasn't looking.[B][C]") ]])
fnCutscene([[ Append("She continued to dance about the halls, teasing the other drones as she did, until she happened upon a hanging sheet of tungsten that awaited machining.[B][C]") ]])
fnCutscene([[ Append("The sheet of tungsten was polished like a mirror, and when she looked at the sheet, she became very angry.[P] There in the sheet of metal stood another latex drone with a shiny spanner, just like hers![B][C]") ]])
fnCutscene([[ Append("She could not bear to think that another drone worked as hard as her, and so she swung the spanner at the drone before her just as she attempted to do the same, and struck at it with all the strength she could muster.[B][C]") ]])
fnCutscene([[ Append("She struck the drone before it could strike her, and in doing so, she bent the new spanner she had been so proud of.[P] She dropped the spanner in a fit of sorrow, and as she looked upon it, her power core nearly rent in two.[B][C]") ]])
fnCutscene([[ Append("She looked up at the drone before her, thinking she might steal her spanner instead, but became delighted when she saw the other drone's spanner also lay bent upon the ground.[B][C]") ]])
fnCutscene([[ Append("After all, she was the hardest-working drone in the city, and if she couldn't have a new spanner, none of the other drones should.[B][C]") ]])
fnCutscene([[ Append("The moral of the story is that latex drones are very stupid.") ]])

fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(65)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
fnCutscene([[ Append("Christine:[E|Neutral] Ummm...[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] The story really captured the latex drones well.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Offended] I swear they can barely even follow simple instructions when they come in for servicing.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] Isn't it sad, though?[P] The poor drone lost her new spanner.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] She'd probably forget all about it when she defragmented, anyway.[P] It was no big loss.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] I suppose...") ]])
fnCutsceneBlocker()