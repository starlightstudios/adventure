--[Sophie Conversation D]
--This sequence plays during one of the dates with Sophie.
local iMet55InLowerRegulus = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N")


fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
fnCutscene([[ Append("Christine:[E|Neutral] Sophie...[P] I've been thinking...[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] About RMFB Surface-Cleaner and how we should get it for the repair bay so everything smells like sprinkles?[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Happy] Yes![P] How did you know?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] I -[P] I - [P][CLEAR]") ]])
fnCutscene([[ Append("Sophie:[E|Blush] Uhhhhhh...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|PDU] My PDU is beeping...[B][C]") ]])
fnCutscene([[ Append("PDU: Unit 771852, I was accessed by Unit 499323 earlier.[P] She downloaded your network access history.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|PDU] Hmm, yep, yep, when I left it to go yell at Unit 89901 over sending us a cracked fuel canister...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Offended] And neglecting to drain all the fuel.[P] Good think my hair didn't get singed.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] But back to the task at hand.[P] Why did you access my PDU?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Blush] Well...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] I'm not angry.[P] I set you as a secondary user, you have full permissions.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Fiddlesparks, I thought I was being clever by waiting until you were logged in but left it behind...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] Didn't I tell you that?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Sad] But aren't you mad that I looked up your search history without asking?[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Offended] Oh, I get you.[P] Yes, sort of.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] But not in the way you're thinking.[P] If you want to know anything about me, you can just ask.[P] You don't need to be sneaky.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] There are absolutely no secrets between us.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Sad] But what if I find out about...[P] you know...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Laugh] You think I'd ever be ashamed of something I did on the network?[P] Ha ha![B][C]") ]])
fnCutscene([[ Append("Christine:[E|Blush] Do you want to go over my search history?[P] Because if you're up for it...[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Happy] I would absolutely love to dress up like a cat for you![P] With big fuzzy ears![B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] Some people value their privacy, myself included.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] But you [P]*are*[P] part of my privacy.[P] Anything, absolutely anything is yours.[B][C]") ]])
if(iMet55InLowerRegulus == 0.0) then
    fnCutscene([[ Append("Sophie:[E|Neutral] Okay, okay, but I'm still going to ask from now on.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] I just wanted to know more about you and I let my impulses get the better of me.[P] I'm sorry![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] It's fine.[P] In fact, I'm glad you confessed and we worked it out.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] We have nothing to hide from each other, right?[P] Fully open?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Fully open![P] Oh, I love you so much![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] I love you too, Sophie.") ]])
else
    fnCutscene([[ Append("Christine:[E|Neutral] With...[P] exceptions...[P] But those are carefully encrypted.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Huh?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] My friend sends communications to my PDU.[P] I don't want you to see those...[P] for your own safety.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Surprised] R-[P]right![P] I forgot![P] I'll add a note in my optimized-access lists![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] But they're encrypted so I wouldn't be able to access them.[P] So it's okay to use your PDU and go through your history?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] I insist you snoop.[P] Find every secret out.[P] Know me better than I know myself.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Okay, okay, but I'm still going to ask from now on.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] I just wanted to know more about you and I let my impulses get the better of me.[P] I'm sorry![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] It's fine.[P] In fact, I'm glad you confessed and we worked it out.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] We have nothing to hide from each other, right?[P] Fully open?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Fully open![P] Oh, I love you so much![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] I love you too, Sophie.") ]])
end