-- |[Document: That]|
--Such a tragic tale.

--Clear censor bars.
WD_SetProperty("Clear Censor Bars")

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ Append("Christine and Sophie linked their cables together and plugged into the computer.[P] The document loaded and began transmitting into their architectures.[B][C]") ]])
fnCutscene([[ Append("They began to read a story about a group of bee girls who were working to establish a new hive.[P] They were diligent and hard-working, and always put the needs of the hive first, which allowed them to become very successful.[B][C]") ]])
fnCutscene([[ Append("As the young hive grew stronger, the happy workers began to notice something following them.[P] Sometimes the creature appeared to be a slime,[P] other times it looked like a cat,[P] and occasionally,[P] it even looked like a ghost.[B][C]") ]])
fnCutscene([[ Append("No matter how it looked, it would attack them whenever it saw them, leaving them too afraid to do their jobs.[P] Over time, this lack of productivity caused the thriving colony to slip into decline.[B][C]") ]])
fnCutscene([[ Append("As the colony grew more fearful, the creature grew stronger, and soon the bee girls realized that it fed on the colony's fear.[B][C]") ]])
fnCutscene([[ Append("Determined to grow their colony and help each other achieve maximum productivity, the hive came up with a plan that would destroy the creature's hold over their colony.[B][C]") ]])
fnCutscene([[ Append("The hive decided to organize an orgy, to which the creature would be invited.[B][C]") ]])
fnCutscene([[ Append("The workers began the preparations at once, and the creature came, expecting to feast upon the bee girls and their fear.[P] But even as the creature entered the colony for its victorious feast, it found its powers had vanished![B][C]") ]])
fnCutscene([[ Append("It was suddenly very fearful, but as it sought to escape, the bees ushered it into the hive where they ravished it with their bodies for the glory of their hive.[B][C]") ]])
fnCutscene([[ Append("The creature found itself happy and fulfilled in a way it had not known before, and as the passions of the evening slowed, it graciously accepted a gift of honey from the bee girls.[B][C]") ]])
fnCutscene([[ Append("Little did the creature know that the honey was, in fact, the necessary catalyst to defeat the creature,[P] and in consuming it, the creature's body shifted to a new shape and became like the bees' own bodies.[B][C]") ]])
fnCutscene([[ Append("Now changed, the new bee girl accepted the wisdom of the bee girls, and lived its life as a loyal and productive member of the hive, and helped the hive grow to become the greatest hive around.[B][C]") ]])
fnCutscene([[ Append("The moral of the story is that assimilation is better than elimination.") ]])

fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(65)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
fnCutscene([[ Append("Christine:[E|Blush] That took one hell of a turn, didn't it?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Happy] Love conquers all.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] Or in this case,[P] sex and[P][P] honey.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] I destroy my enemy when I make her like me.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] Sheesh.") ]])
fnCutsceneBlocker()