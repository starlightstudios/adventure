-- |[ ===================================== Relive Ending ====================================== ]|
--After reliving a scene, return to the save point and re-add characters to the party if they got 
-- removed and play any dialogue that occurs.

-- |[ ========================================= Setup ========================================== ]|
--Unset the relive flag.
VM_SetVar("Root/Variables/Chapter5/Scenes/iIsRelivingScene", "N", 0.0)
    
--Unblock autosaves.
VM_SetVar("Root/Variables/Global/Autosave/iBlockAutosave", "N", 0.0)

--Clear follower properties.
giFollowersTotal = 0
gsaFollowerNames = {}
giaFollowerIDs = {}

--Chat variables.
local bIs55Present = false
local bIsSX399Present = false
local bIsSophiePresent = false

-- |[ ====================================== World Party ======================================= ]|
--Re-add party members here.
for i = 1, giFollowersTotalRelive, 1 do

    --Unit 2855
    if(gsaFollowerNamesRelive[i] == "Tiffany") then
	
        --Flag
        bIs55Present = true
    
		--Create if she doesn't exist.
		if(EM_Exists("Tiffany") == false) then
			fnSpecialCharacter("Tiffany", -100, -100, gci_Face_South, false, nil)
		end

		--Lua globals.
		giFollowersTotal = giFollowersTotal + 1
		gsaFollowerNames[giFollowersTotal] = gsaFollowerNamesRelive[i]

		--Get character's uniqueID. 
		EM_PushEntity("Tiffany")
			local iEntityID = RE_GetID()
		DL_PopActiveObject()

		--Store it and tell her to follow.
		giaFollowerIDs[i] = iEntityID
		AL_SetProperty("Follow Actor ID", iEntityID)

		--Place in the combat lineup.
        --AdvCombat_SetProperty("Party Slot", giFollowersTotal, "Tiffany")

    --SX-399
    elseif(gsaFollowerNamesRelive[i] == "SX-399") then
	
        --Flag
        bIsSX399Present = true
        
        --Entity create.
		if(EM_Exists("SX-399") == false) then
			fnSpecialCharacter("SX-399", -100, -100, gci_Face_South, false, nil)
		end

		--Lua globals.
		giFollowersTotal = giFollowersTotal + 1
		gsaFollowerNames[giFollowersTotal] = gsaFollowerNamesRelive[i]

		--Get character's uniqueID. 
		EM_PushEntity("SX-399")
			local iEntityID = RE_GetID()
		DL_PopActiveObject()

		--Store it and tell her to follow.
		giaFollowerIDs[i] = iEntityID
		AL_SetProperty("Follow Actor ID", iEntityID)

		--Place in the combat lineup.
        --AdvCombat_SetProperty("Party Slot", giFollowersTotal, "SX-399")
    
    --Sophie
    elseif(gsaFollowerNamesRelive[i] == "Sophie") then
	
        --Flag
        bIsSophiePresent = true
        
        --Entity create.
		if(EM_Exists("Sophie") == false) then
			fnSpecialCharacter("Sophie", -100, -100, gci_Face_South, false, nil)
		end

		--Lua globals.
		giFollowersTotal = giFollowersTotal + 1
		gsaFollowerNames[giFollowersTotal] = gsaFollowerNamesRelive[i]

		--Get character's uniqueID. 
		EM_PushEntity("Sophie")
			local iEntityID = RE_GetID()
		DL_PopActiveObject()

		--Store it and tell her to follow.
		giaFollowerIDs[i] = iEntityID
		AL_SetProperty("Follow Actor ID", iEntityID)
        
    end
end

--Party Folding
AL_SetProperty("Fold Party")

-- |[ ====================================== Combat Party ====================================== ]|
--Rebuild the combat party.
AdvCombat_SetProperty("Clear Party")
for i = 1, #gsaPartyNamesRelive, 1 do
    AdvCombat_SetProperty("Party Slot", i-1, gsaPartyNamesRelive[i])
end

-- |[ ======================================= Form Reset ======================================= ]|
--Return Christine to her original form:
local sOriginalForm = VM_GetVar("Root/Variables/Chapter5/Scenes/sOriginalForm", "S")
if(sOriginalForm == "Human") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua")
elseif(sOriginalForm == "Darkmatter") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Darkmatter.lua")
elseif(sOriginalForm == "Doll") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Doll.lua")
elseif(sOriginalForm == "Eldritch") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Eldritch.lua")
elseif(sOriginalForm == "Electrosprite") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Electro.lua")
elseif(sOriginalForm == "Golem") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua")
elseif(sOriginalForm == "LatexDrone") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Latex.lua")
elseif(sOriginalForm == "Raiju") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Raiju.lua")
elseif(sOriginalForm == "SteamDroid") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_SteamDroid.lua")
elseif(sOriginalForm == "Secrebot") then
	LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Secrebot.lua")
end

--Reset Tiffany's costume.
local sCostumeDollStore = VM_GetVar("Root/Variables/Costumes/Tiffany/sCostumeDollStore", "S")
VM_SetVar("Root/Variables/Costumes/Tiffany/sCostumeDoll", "S", sCostumeDollStore)
LM_ExecuteScript(gsCharacterAutoresolve, "Tiffany")

-- |[ ======================================== Dialogue ======================================== ]|
--Dialogue setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)

--Now let's have some dialogue. This is based on what scene we saw.
local sLastRelivedScene = VM_GetVar("Root/Variables/Chapter5/Scenes/sLastRelivedScene", "S")
local iFlashbackBecameDoll = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackBecameDoll", "N")
if(sLastRelivedScene == "Doll" or iFlashbackBecameDoll == 1.0) then
    fnCutscene([[ Append("Christine:[E|Serious] (Administrator, I am coming.[P] Unit 771852 will serve the Cause of Science.)") ]])
elseif(sLastRelivedScene == "Golem") then
    fnCutscene([[ Append("Christine:[E|Smirk] (It felt so good to become a machine...[P] and I can relive the sensation all I want...[P] mechanical life is too good!)") ]])
elseif(sLastRelivedScene == "Latex Drone") then
    fnCutscene([[ Append("Christine:[E|Blush] (It's a shame I had to be so clever.[P] I'd love to have someone order me around, and to mindlessly obey...)") ]])
elseif(sLastRelivedScene == "Darkmatter") then
    fnCutscene([[ Append("Christine:[E|Smirk] (It was a hell of a trip, but I came out of it on the other end.[P] It was all for the best.)") ]])
elseif(sLastRelivedScene == "Eldritch") then
    fnCutscene([[ Append("Christine:[E|Smirk] (She taught me so much.[P] How can I ever repay her?)") ]])
elseif(sLastRelivedScene == "SteamDroid") then
    fnCutscene([[ Append("Christine:[E|Smirk] (The feeling of my chest swelling up with steam...[P] I love it so much...)") ]])
elseif(sLastRelivedScene == "Raiju") then
    fnCutscene([[ Append("Christine:[E|Blush] (It was worth it for the electricity puns alone.[P] Plus I love being so fluffy!)") ]])
elseif(sLastRelivedScene == "Electrosprite") then
    fnCutscene([[ Append("Christine:[E|Blush] (With my electrosprite powers, I wonder if I could go inside a video game.[P] What a thing that would be![P] Oh well, back to reality.)") ]])
elseif(sLastRelivedScene == "Secrebot") then
    fnCutscene([[ Append("Christine:[E|Blush] (I admit I...[P] kind of liked it.[P] Not in the moment, but I got to see a part of Sophie I never had before.[P] I'm glad I could be her secrebot.)") ]])
elseif(sLastRelivedScene == "Secrebot Bad End") then
    fnCutscene([[ Append("Christine:[E|Neutral] (Odd.[P] The timestamps on that memory don't make sense.[P] Did that actually happen?[P] I guess not, but...)") ]])
end
fnCutsceneBlocker()
