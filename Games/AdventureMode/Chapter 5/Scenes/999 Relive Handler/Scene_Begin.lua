-- |[ ===================================== Relive Handler ===================================== ]|
--Special script, used for (most) of the instances of the player choosing to relive a transformation scene.
-- The scene requires the name of the transformation to be passed to it. The C++ code will pass the DISPLAY NAME
-- of the scene in. We would therefore expect "Transform: Alraune", for example.

-- |[Argument Check]|
--Verify.
if(fnArgCheck(1) == false) then return end

--Set.
local sSceneName = LM_GetScriptArgument(0)

-- |[Storage]|
--Store Christine's current form.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
VM_SetVar("Root/Variables/Chapter5/Scenes/sOriginalForm", "S", sChristineForm)

--Indicate we are reliving a scene. This causes them to end earlier or at different times.
VM_SetVar("Root/Variables/Chapter5/Scenes/iIsRelivingScene", "N", 1.0)

--Save party properties.
giFollowersTotalRelive = giFollowersTotal
gsaFollowerNamesRelive = gsaFollowerNames
giaFollowerIDsRelive = giaFollowerIDs

--Store all combat party members.
gsaPartyNamesRelive = {}
for i = 0, 5, 1 do
    gsaPartyNamesRelive[i+1] = AdvCombat_GetProperty("Name of Active Member", i)
end

-- |[ ==================================== Common Base Code ==================================== ]|
--Variables.
local iFlashbackBecameDoll = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackBecameDoll", "N")
local iTalkedToSophie      = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")

--Wait a bit.
fnCutsceneWait(10)
fnCutsceneBlocker()

--Common.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)

-- |[Execution]|
--Setup.
local sInstruction = "Null"
    
--Store Tiffany's costume before this starts.
local sCostumeDollCur = VM_GetVar("Root/Variables/Costumes/Tiffany/sCostumeDoll", "S")
VM_SetVar("Root/Variables/Costumes/Tiffany/sCostumeDollStore", "S", sCostumeDollCur)

-- |[Doll]|
if(sSceneName == "Doll") then
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Scenes/sLastRelivedScene", "S", "Doll")
    sInstruction = "LM_ExecuteScript(gsRoot .. \"Maps/RegulusFlashback/RegulusFlashbackC/BecomeDollCutscene.lua\")"
	
	--Dialogue.
    if(iTalkedToSophie == 0.0) then
        fnCutscene([[ Append("Christine:[E|Serious] (Unit 771852 reviewing memory of command unit conversion process.[P] Diagnostic code t-17-p.[P] Moving execution pointer...)") ]])
    else
        fnCutscene([[ Append("Christine:[E|Blush] (My doll form...[P] My true form...[P] The form I was created in by the Administrator.)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Serious] (Yes, the Administrator.[P] I will not fail you.[P] I will serve...)") ]])
        fnCutsceneBlocker()
    end

-- |[Golem]|
elseif(sSceneName == "Golem") then
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Scenes/sLastRelivedScene", "S", "Golem")
    sInstruction = "LM_ExecuteScript(gsRoot .. \"Chapter 5/Scenes/500 Normal/Chris To Golem Cryogenics/Scene_Begin.lua\")"
	
	--Dialogue.
    if(iFlashbackBecameDoll == 0.0) then
        
        if(iTalkedToSophie == 1.0) then
            fnCutscene([[ Append("Christine:[E|Blush] (When I was first brought to mechanical perfection...[P] What a lovely memory...)[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Blush] (And my memory banks recorded it perfectly, it's like I'm really there...)") ]])
        else
            fnCutscene([[ Append("Christine:[E|Serious] (Unit 771852 reviewing memory of conversion process.[P] Diagnostic code t-17-p.[P] Moving execution pointer...)") ]])
        end
    else
        fnCutscene([[ Append("Christine:[E|Serious] (Unit 771852 reviewing memory of golem conversion.[P] The experiences will be useful to the Cause of Science.[P] Begin memory descramble...)") ]])
        
    end
	fnCutsceneBlocker()

-- |[Latex Drone]|
elseif(sSceneName == "Latex Drone" or sSceneName == "Latex Drone (V)") then
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Scenes/sLastRelivedScene", "S", "Latex Drone")
    if(sSceneName == "Latex Drone") then
        sInstruction = "LM_ExecuteScript(gsRoot .. \"Chapter 5/Scenes/200 Defeat/Defeat LatexDrone/Scene_Begin.lua\")"
    else
        sInstruction = "LM_ExecuteScript(gsRoot .. \"Chapter 5/Scenes/400 Volunteer/Latex Drone Volunteer/Scene_Begin.lua\")"
    end
    
    --Change Tiffany's costume.
    VM_SetVar("Root/Variables/Costumes/Tiffany/sCostumeDoll", "S", "Normal")
    LM_ExecuteScript(gsCharacterAutoresolve, "Tiffany")
    
	--Dialogue.
    if(iFlashbackBecameDoll == 0.0) then
        if(iTalkedToSophie == 0.0) then
            fnCutscene([[ Append("Christine:[E|Serious] (Unit 771852 reviewing memory of drone unit conversion process.[P] Diagnostic code t-17-p.[P] Moving execution pointer...)") ]])
        else
            fnCutscene([[ Append("Christine:[E|Blush] (To be wrapped up firm in squishy, tight latex and simplified into someone's tool...)") ]])
        end
    else
        fnCutscene([[ Append("Christine:[E|Serious] (Unit 771852 reviewing memory of drone unit conversion.[P] The experiences will be useful to the Cause of Science.[P] Begin memory descramble...)") ]])
        
    end
	fnCutsceneBlocker()

-- |[Darkmatter]|
elseif(sSceneName == "Darkmatter") then
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Scenes/sLastRelivedScene", "S", "Darkmatter")
    sInstruction = "LM_ExecuteScript(gsRoot .. \"Maps/RegulusSerenity/SerenityCraterH/Examination.lua\", \"Hole\")"
	
	--Dialogue.
    if(iFlashbackBecameDoll == 0.0) then
        if(iTalkedToSophie == 0.0) then
            fnCutscene([[ Append("Christine:[E|Serious] (Unit 771852 reviewing memory of darkmatter transformation.[P] Diagnostic code t-17-p.[P] Moving execution pointer...)") ]])
        else
            fnCutscene([[ Append("Christine:[E|Blush] (I became a part of the stars and witnessed the birth of the universe...)") ]])
        end
    else
        fnCutscene([[ Append("Christine:[E|Serious] (Unit 771852 reviewing memory of darkmatter conversion.[P] The experiences will be useful to the Cause of Science.[P] Begin memory descramble...)") ]])
        
    end
	fnCutsceneBlocker()

-- |[Eldritch Dreamer]|
elseif(sSceneName == "Dreamer") then
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Scenes/sLastRelivedScene", "S", "Eldritch")
    sInstruction = "LM_ExecuteScript(gsRoot .. \"Maps/RegulusLRT/RegulusLRTIG/Combat_DefeatA.lua\")"
	
	--Dialogue.
    if(iFlashbackBecameDoll == 0.0) then
        if(iTalkedToSophie == 0.0) then
            fnCutscene([[ Append("Christine:[E|Serious] (Unit 771852 reviewing memory of ERROR DISPLAY NAME.[P] Diagnostic code t-17-p.[P] Moving execution pointer...)") ]])
        else
            fnCutscene([[ Append("Christine:[E|Blush] (Yes, my master.[P] Show me your truth...)") ]])
        end
    else
        fnCutscene([[ Append("Christine:[E|Serious] (Unit 771852 reviewing memory of dreamer conversion.[P] The experiences will be useful to the Cause of Science.[P] Begin memory descramble...)") ]])
        
    end
	fnCutsceneBlocker()

-- |[Electrosprite]|
elseif(sSceneName == "Electrosprite") then
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Scenes/sLastRelivedScene", "S", "Electrosprite")
    sInstruction = "LM_ExecuteScript(gsRoot .. \"Chapter 5/Scenes/400 Volunteer/Electrosprite Volunteer/Scene_Begin.lua\")"
	
	--Dialogue.
    if(iFlashbackBecameDoll == 0.0) then
        if(iTalkedToSophie == 0.0) then
            fnCutscene([[ Append("Christine:[E|Serious] (Unit 771852 reviewing memory of electrosprite transformation.[P] Diagnostic code t-17-p.[P] Moving execution pointer...)") ]])
        else
            fnCutscene([[ Append("Christine:[E|Blush] (Ah, Psue.[P] We shared minds and cloned bodies.[P] What a time!)") ]])
        end
    else
        fnCutscene([[ Append("Christine:[E|Serious] (Unit 771852 reviewing memory of electrosprite conversion.[P] The experiences will be useful to the Cause of Science.[P] Begin memory descramble...)") ]])
        
    end
	fnCutsceneBlocker()

-- |[Steam Droid]|
elseif(sSceneName == "SteamDroid") then
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Scenes/sLastRelivedScene", "S", "SteamDroid")
    sInstruction = "LM_ExecuteScript(gsRoot .. \"Chapter 5/Scenes/400 Volunteer/Steam Droid Volunteer/Scene_Begin.lua\")"
	
	--Dialogue.
    if(iFlashbackBecameDoll == 0.0) then
        if(iTalkedToSophie == 0.0) then
            fnCutscene([[ Append("Christine:[E|Serious] (Unit 771852 reviewing memory of steam droid transformation.[P] Diagnostic code t-17-p.[P] Moving execution pointer...)") ]])
        else
            fnCutscene([[ Append("Christine:[E|Blush] (Ancient-era mechanical gears and steam power.[P] Somehow, it turns me on...)") ]])
        end
    else
        fnCutscene([[ Append("Christine:[E|Serious] (Unit 771852 reviewing memory of steam droid conversion.[P] The experiences will be useful to the Cause of Science.[P] Begin memory descramble...)") ]])
        
    end
	fnCutsceneBlocker()

-- |[Raiju]|
elseif(sSceneName == "Raiju") then
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Scenes/sLastRelivedScene", "S", "Raiju")
    sInstruction = "LM_ExecuteScript(gsRoot .. \"Chapter 5/Scenes/400 Volunteer/Raiju Volunteer/Scene_Begin.lua\")"
	
	--Dialogue.
    if(iFlashbackBecameDoll == 0.0) then
        if(iTalkedToSophie == 0.0) then
            fnCutscene([[ Append("Christine:[E|Serious] (Unit 771852 reviewing memory of raiju transformation.[P] Diagnostic code t-17-p.[P] Moving execution pointer...)") ]])
        else
            fnCutscene([[ Append("Christine:[E|Blush] (Well when I thought I'd be saving the Raijus, I didn't think it'd involve getting eaten out by one...)") ]])
        end
    else
        fnCutscene([[ Append("Christine:[E|Serious] (Unit 771852 reviewing memory of raiju conversion.[P] The experiences will be useful to the Cause of Science.[P] Begin memory descramble...)") ]])
        
    end
	fnCutsceneBlocker()
    
-- |[Secrebot]|
elseif(sSceneName == "Secrebot") then
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Scenes/sLastRelivedScene", "S", "Secrebot")
    sInstruction = "LM_ExecuteScript(gsRoot .. \"Chapter 5/Scenes/400 Volunteer/Secrebot Volunteer/Scene_Begin.lua\")"
    
	--Dialogue.
    if(iFlashbackBecameDoll == 0.0) then
        if(iTalkedToSophie == 0.0) then
            fnCutscene([[ Append("Christine:[E|Serious] (Unit 771852 reviewing memory of secrebot conversion process.[P] Diagnostic code cr-1-15-p.[P] Moving execution pointer...)") ]])
        else
            fnCutscene([[ Append("Christine:[E|Blush] (An existence of happy servitude and productivity...[P] what else could a robot ask for?)") ]])
        end
    else
        fnCutscene([[ Append("Christine:[E|Serious] (Unit 771852 reviewing memory of secrebot conversion.[P] The experiences will be useful to the Cause of Science.[P] Begin memory descramble...)") ]])
        
    end
	fnCutsceneBlocker()

-- |[Secrebot Bad End]|
elseif(sSceneName == "Secrebot Bad End") then
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Scenes/sLastRelivedScene", "S", "Secrebot Bad End")
    sInstruction = "LM_ExecuteScript(gsRoot .. \"Chapter 5/Scenes/200 Defeat/Defeat Silly Secrebots/Scene_Begin.lua\")"
    
	--Dialogue.
    if(iFlashbackBecameDoll == 0.0) then
        if(iTalkedToSophie == 0.0) then
            fnCutscene([[ Append("Christine:[E|Serious] (Unit 771852 reviewing memory of secrebot sale and subversion.[P] Diagnostic code cr-2-1-p.[P] Moving execution pointer...)") ]])
        else
            fnCutscene([[ Append("Christine:[E|Neutral] (Now just where did this memory file come from?[P] Let's turn it on and see...)") ]])
        end
    else
        fnCutscene([[ Append("Christine:[E|Serious] (Unit 771852 reviewing memory of secrebot sale and subversion scenario.[P] The experiences will be useful to the Cause of Science.[P] Begin memory descramble...)") ]])
        
    end
	fnCutsceneBlocker()
end

--Error Check:
if(sInstruction == "Null") then return end

-- |[ =================================== Common Scene Code ==================================== ]|
-- |[Common Execution]|
--Clear combat party.
AdvCombat_SetProperty("Clear Party")
AdvCombat_SetProperty("Party Slot", 0, "Christine")

--Clear followers.
AL_SetProperty("Unfollow Actor Name", "Tiffany")
AL_SetProperty("Unfollow Actor Name", "SX-399")
AL_SetProperty("Unfollow Actor Name", "JX-101")
AL_SetProperty("Unfollow Actor Name", "Sophie")
    
--Block autosaves.
VM_SetVar("Root/Variables/Global/Autosave/iBlockAutosave", "N", 1.0)

--Wait a bit.
fnCutsceneWait(15)
fnCutsceneBlocker()

--Black the screen out.
fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneWait(60)
fnCutsceneBlocker()

--Execute the scene.
fnCutscene(sInstruction)
