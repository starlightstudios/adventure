-- |[Setup]|
--Variables.
local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsRelivingScene", "N")

--Issue load instruction.
fnLoadDelayedBitmapsFromList("Chapter 5 Christine Electrosprite TF", gciDelayedLoadLoadAtEndOfTick)
    
--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--Clear censor bars.
WD_SetProperty("Clear Censor Bars")

-- |[Scene]|
--Switch to scene mode.
fnCutscene([[ AL_SetProperty("Music", "Null") ]])
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ Append("Christine frowned.[P] She wondered what she was going to do with the electrical disturbance now that she had trapped it in the broken console.[P] What was it?[P] Was it a voidborne partirhuman, perhaps?[P] Or maybe an AI?[P] She wasn't sure, but it wouldn't be causing any more trouble.[B][C]") ]])
fnCutscene([[ Append("She flipped the breaker and cut the access point.[P] The disturbance began racing through the terminal's circuitry, but she had been thorough.[P] There was no escape.[P] She almost felt sorry for it, like a mouse in a trap, but there was no other way.[B][C]") ]])
fnCutscene([[ Append("Absentmindedly, she brushed her arm against one of the exposed wires.[P] Her chassis was insulated enough to resist the current but the disturbance raced towards her. She yanked her arm back but the disturbance, somehow sensing her presence, coalesced where she had touched the wire.[B][C]") ]])
fnCutscene([[ Append("It began to change itself, transforming from electrical current to photonic energy.[P] A bright light appeared and Christine tried to back away, but it was too late.[P] The light lanced towards her and touched her.[P] It seemed to be jumping from the wire to her body, and it flowed into her.[B][C]") ]])
fnCutscene([[ Append("She felt her mind begin to race.[P] Her body was beginning to convulse as electric charge damaged her internal systems.[P] Thoughts were coming and going as if she were having a fevered dream.[P] Her whole life was flashing before her as her body collapsed and went limp.[P] She felt the runestone's power wash over her, and she reverted to human form as she blacked out...[B][C]") ]])
fnCutscene([[ Append("From within her, the light again took form.[P] It appeared on her skin, hardened, and stepped out of her.[P] It had become a woman, made of electricity and metal insulators, made in the same form as Christine.[P] This new creature looked itself over and took stock of the room it stood in.[P] It was pleased.[P] But, her gracious host lay unconscious beneath her.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/ElectrospriteTF0") ]])
fnCutscene([[ WD_SetProperty("Register Censor Bar", 607, 389, 42, 95) ]])
fnCutscene([[ Append("Christine awoke with a start as a bolt of electricity hit her brain.[P] She was human again, and in a vacuum...[P] not again.[P] Not again![P] She began to panic as her lungs breathed nothing.[P] She needed to transform, but she was too weakened and her body would not let her.[B][C]") ]])
fnCutscene([[ Append("There was someone standing over her, reaching down to her, but she began to feel her body go numb and cold.[P] It would be over soon...[B][C]") ]])
fnCutscene([[ Append("She lost feeling in her legs and arms.[P] In a few moments she would fall unconscious again as the oxygen in her blood ran out.[P] This time, she would not reawaken.[B][C]") ]])
fnCutscene([[ Append("The girl made of electricity was staring at her, unsure.[P] Why was she not moving?[P] It reached down and touched her exposed breasts, and placed a blue and red symbol on each nipple.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/ElectrospriteTF0") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/ElectrospriteTF1") ]])
fnCutscene([[ WD_SetProperty("Send Censor Bars To Crossfade") ]])
fnCutscene([[ Append("Then, in her extremities, she began to feel a tingling.[P] Pins and needles, like she slept on them and numbed them.[P] While she had been preparing herself for her end, she realized she was no longer breathing.[P] She no longer needed to.[P] Something about her had changed, but she could still not feel her body save the tingling in her hands and feet.[B][C]") ]])
fnCutscene([[ Append("The tingling moved up her arms and legs, and she lifted her hand.[P] She could see it move but barely feel it, and realized her skin was glowing.[P] Gently, she touched her calf only to see a bolt of electricity arc out of her finger into her leg.[P] Normally, such a thing was impossible in a vacuum, but once the electricity arced out of her finger, her hand dissociated into pure energy.[B][C]") ]])
fnCutscene([[ Append("Her veins no longer carried blood but pure electrical energy.[P] She saw her skin begin to change to become like the electricity girl who still stood over her.[B][C]") ]])
fnCutscene([[ Append("She looked up, and the girl looked down and smiled at her.[P] It placed its body close to hers, holding her arms and legs down.[P] It rubbed its chest against hers until the positive and negative nipple pasties touched...[B][C]") ]])
fnCutscene([[ Append("An incredible burst of power shot through Christine as her mind roared back to life.[P] She had been dimly aware of what was happening to her before, but no longer.[P] She had been changed, transformed into a being of pure energy, and by joining their breasts together, she had temporarily joined minds with the girl.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/ElectrospriteTF1") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/ElectrospriteTF2") ]])
fnCutscene([[ Append("She realized that, when this girl had jumped into her body, it had read her mind and created a physical copy based on her own self image.[P] Information flowed through her head, but she could scarcely comprehend any of it.[P] Programs, algorithms, places, people, text...[P] It was too much.[P] The girl withdrew and broke the circuit connecting their bodies.[B][C]") ]])
fnCutscene([[ Append("Christine quickly stood and began to touch herself all over.[P] Where her energetic hands went, electricity flowed with it.[P] She was a new breed.[P] This girl was the first electrosprite, and she the second.[P] Yet, she could stay this way, in this new joy, forever.[B][C]") ]])
fnCutscene([[ Append("She still had a job to do.[P] Units were counting on her.[P] She turned to face the still-smiling electrical girl and explain the situation.") ]])
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()

-- |[Clean]|
fnCutsceneWait(gciDialogue_Fadeout_Then_Unload_Ticks)
fnCutsceneBlocker()
fnCutscene([[ fnUnloadBitmapsFromList("Chapter 5 Christine Electrosprite TF") ]])

--If this is a relive case, end the handler here.
if(iIsRelivingScene == 1.0) then
    
	--Wait a bit.
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Return to the last save point and execute the post-script..
	fnCutscene([[ AL_BeginTransitionTo("LASTSAVE", gsStandardReliveEnd) ]])
	fnCutsceneBlocker()
    return
end

