-- |[ ================================ Secrebot Transformation ================================= ]|
--Cutscene showing Christine turning into a secrebot. Notably this isn't a volunteer scene but
-- it's also not a defeat scene, so whatever.

-- |[ ======================================= Scene Setup ====================================== ]|
-- |[Variables]|
local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsRelivingScene", "N")

-- |[Image Streaming]|
fnLoadDelayedBitmapsFromList("Chapter 5 Christine Secrebot TF", gciDelayedLoadLoadAtEndOfTick)

-- |[Form]|
--Change Christine into a Secrebot.
fnCutsceneCall(gsRoot .. "FormHandlers/Christine/Form_Secrebot.lua")

-- |[Music]|
AL_SetProperty("Music", "Null")

-- |[Flags]|
--Mark this form as being unlocked.
VM_SetVar("Root/Variables/Global/Christine/iHasSecrebotForm", "N", 1.0)

-- |[Relive Handler]|
--Wait a bit if reliving the scene, it needs to fade.
if(iIsRelivingScene == 0.0) then
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
end
    
-- |[ ==================================== Scene Execution ===================================== ]|
--Section A.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ WD_SetProperty("Transition Scene", "Null", "Root/Images/Scenes/Christine/SecrebotTF00") ]])
fnCutscene([[ Append("Christine clocked down her systems and rerouted power to her auditory pickups.[P] She needed to be as silent as possible.[P] The girl was struggling but the drone was unrelenting.[P] Odess spoke to her softly, making a cooing sound, as if speaking to a baby.[P] She produced a pressurized can and sprayed it in the human's face, and the girl went limp.[B][C]") ]])
fnCutscene([[ Append("Odess ordered the drone to stand nearby and dragged the human towards the waiting capsule.[P] Converting a human was nothing special, but Christine's core froze over.[P] Something terribly wrong was happening here, more than the usual violations of Regulus City's ethics.") ]])
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/Christine/SecrebotTF00", "Root/Images/Scenes/Christine/SecrebotTF01") ]])
fnCutscene([[ Append("Odess seemed more interested in this than a usual lord golem.[P] She was performing the steps personally, relishing in each little moment.[P] The drones stood idle as Odess threw a switch, flooding the chamber with nanite-laden conversion fluid.") ]])
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/Christine/SecrebotTF01", "Root/Images/Scenes/Christine/SecrebotTF02") ]])
fnCutscene([[ Append("The human floated, helpless in the chamber.[P] Yet, Christine realized Odess had not fitted her with a golem core.[P] Even drone units needed golem cores.[P] The upcoming changes were obvious, Christine had expected it, yet it was still unnerving.[P] The girl's skin began to harden, her body being flooded by nanites.[B][C]") ]])
fnCutscene([[ Append("The liquid soon was absorbed into her skin, changing her inside and out.[P] Organs were changed to machines to regulate her power production, skin changed to metal, plugs and hookups created in her back.") ]])
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/Christine/SecrebotTF02", "Root/Images/Scenes/Christine/SecrebotTF03") ]])
fnCutscene([[ Append("Her brain began to mechanize, circuits flooded through her body and reformed her from the ground up.[P] Her eyes became ocular receptors, her brain a CPU.[P] Within minutes, the process was done.") ]])
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/Christine/SecrebotTF03", "Root/Images/Scenes/Christine/SecrebotTF04") ]])
fnCutscene([[ Append("A secrebot floated in the tank, the now spent fluid haphazardly draining onto the floor.[P] A drone stood nearby with a mop.[P] Christine was stunned.[P] The secrebots possessed human-grade intelligence because they were humans!") ]])
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/Christine/SecrebotTF04", "Root/Images/Scenes/Christine/SecrebotTF05") ]])
fnCutscene([[ Append("But why?[P] Regulus City never had a problem converting humans into robots, there was an entire breeding program and abductions department dedicated to this.[P] Why sneak this into the back of a remote warehousing sector?[P] Something still wasn't adding up.[P] Christine needed to get a better look at the hacked-together conversion chamber to be sure, and prepared to depart before Odess and her brand new maid secrebot spotted her.") ]])
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/Christine/SecrebotTF05", "Root/Images/Scenes/Christine/SecrebotTF06") ]])
fnCutscene([[ Append("[VOICE|LatexDrone]'UNAUTHORIZED UNIT DETECTED.[P] ARRESTING.'[B][C]") ]])
fnCutscene([[ Append("Christine had been too focused, one of the wandering security drones had spotted her.[P] She took a sudden, hard hit to the head and collapsed, sent into system standby.") ]])
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/Christine/SecrebotTF06", "Root/Images/Scenes/Christine/SecrebotTF07") ]])
fnCutscene([[ Append("[VOICE|GolemLord]'What have we here?[P] Unit 771852?[P] Snooping around in my sector?[P] Oh, what a problem you've become for me.'[B][C]") ]])
fnCutscene([[ Append("[VOICE|GolemLord]'You know, I actually did owe you a favour.[P] Shame you know far, far too much now.[P] Drone, take her to the loading dock and shoot her, in the head.[P] We'll dispose of the body later.'[B][C]") ]])
fnCutscene([[ Append("[VOICE|GolemLord]'They won't find out what happened to her until it is far, far too late...'") ]])
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/Christine/SecrebotTF07", "NULL") ]])
fnCutscene([[ Append("Hauled away by the drone unit, Christine could do nothing as her systems struggled to reboot themselves.[P] She was only able to partially reactivate her optical sensors as the drone slumped her against a crate.[B][C]") ]])
fnCutscene([[ Append("Mindlessly, wordlessly, the drone raised a pulse pistol, pressed it against Christine's head, and pulled the trigger.[B][C]") ]])
fnCutscene([[ Append("...[B][C]") ]])
fnCutscene([[ Append("...[B][C]") ]])
fnCutscene([[ Append("...[B][C]") ]])
fnCutscene([[ Append("[VOICE|GolemLord]'What do you mean, you LOST Unit 771852?[P] I told you, very explicitly to take her to the loading dock and retire her!'[B][C]") ]])
fnCutscene([[ Append("[VOICE|LatexDrone]'UNIT 771852 WAS NO LONGER PRESENT.[P] CURRENT STATUS, UNKNOWN.'[B][C]") ]])
fnCutscene([[ Append("[VOICE|GolemLord]'This human, where did you find her?'[B][C]") ]])
fnCutscene([[ Append("[VOICE|LatexDrone]'IN THE LOADING DOCK.[P] SHE HAS BEEN APPREHENDED AS A ROGUE HUMAN.'[B][C]") ]])
fnCutscene([[ Append("[VOICE|GolemLord]'You -[P] you whoever you are![P] Wake up![P] Stupid human![P] Wake up!'[B][C]") ]])
fnCutscene([[ Append("[VOICE|GolemLord]'Christine must have had an accomplice, or...[P] did another human get delivered?[P] She must have spirited away Christine's body.[P] Damn it, think, think...'[B][C]") ]])
fnCutscene([[ Append("[VOICE|GolemLord]'Oh.[P] Ohhh, I have the most wicked idea.[P] CR-1-15, help me secure the human in the tube.'[B][C]") ]])
fnCutscene([[ Append("[VOICE|Night]'Yes, lord unit.'") ]])
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Transition Scene", "NULL", "Root/Images/Scenes/Christine/SecrebotTF08") ]])
fnCutscene([[ Append("Christine woke slowly to the all-too-familiar feeling of nanitic fluid surrounding her.[P] She was in the capsule now, and she quickly felt at her chest.[P] No golem core.[B][C]") ]])
fnCutscene([[ Append("She had been hit, everything was fuzzy.[P] She could breathe the fluid but there was someone outside.[P] Odess?[P] No...") ]])
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/Christine/SecrebotTF08", "Root/Images/Scenes/Christine/SecrebotTF09") ]])
fnCutscene([[ Append("Captured![P] She had been captured![P] She pressed against the tube, feeling for a weakness.[P] There was no exposed machinery she could break, her runestone -[P] gone![B][C]") ]])
fnCutscene([[ Append("Her head throbbed as she tried to think of a way out...") ]])
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/Christine/SecrebotTF09", "Root/Images/Scenes/Christine/SecrebotTF10") ]])
fnCutscene([[ Append("The human, the very human she had been trying to save, was standing idle nearby.[P] A secrebot now, she glared coldly at Christine.[P] Odess was not here.[P] Christine wanted to plead, but the liquid coated her.[P] She could not speak.[B][C]") ]])
fnCutscene([[ Append("[VOICE|Night]'Beginning conversion'") ]])
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/Christine/SecrebotTF10", "Root/Images/Scenes/Christine/SecrebotTF11") ]])
fnCutscene([[ Append("This was nothing like the golem transformation.[P] It hurt, it hurt because there was no golem core to disable her nerve endings.[P] The nanites did their grim work, absorbing into her skin.[P] Mechanizing her, inside and out.[P] She tried to resist but there was no hope.[B][C]") ]])
fnCutscene([[ Append("The machines reached inside her, forming a CPU and associated power architecture.[P] Yet there was something else deep within, commanding the CPU.[P] Even as her awareness increased, it collapsed.[P] She could barely think anymore, layers of programming smothering her.") ]])
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/Christine/SecrebotTF11", "Root/Images/Scenes/Christine/SecrebotTF12") ]])
fnCutscene([[ Append("The tube opened, the fluid spilling on the floor, drained of nanites.[P] A drone unit stood nearby with a mop, waiting.[P] CR-1-16 stood by.[B][C]") ]])
fnCutscene([[ Append("[VOICE|Night] 'Conversion completed.[P] CR-1-16, report status.'[B][C]") ]])
fnCutscene([[ Append("[VOICE|Christine] 'CR-1-16 reporting, system check green.[P] What are my orders?'[B][C]") ]])
fnCutscene([[ Append("[VOICE|Night] 'Our lord unit requires information.[P] Were you working with Unit 771852?'[B][C]") ]])
fnCutscene([[ Append("[VOICE|Christine] 'No.'[B][C]") ]])
fnCutscene([[ Append("[VOICE|Night] 'Did you rescue her before conversion?'[B][C]") ]])
fnCutscene([[ Append("[VOICE|Christine] 'No.'[B][C]") ]])
fnCutscene([[ Append("[VOICE|Night] 'Affirmative.[P] Redact knowledge of events in the loading dock under lord unit's authorization.'[B][C]") ]])
fnCutscene([[ Append("[VOICE|Christine] 'Information redacted.'[B][C]") ]])
fnCutscene([[ Append("[VOICE|Night] 'Follow me to the lord unit's office.'[B][C]") ]])
fnCutscene([[ Append("[VOICE|Christine] 'Affirmative.[P] Following.'") ]])
fnCutsceneBlocker()

-- |[Setup for Next Scene]|
fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/Christine/SecrebotTF12", "Null") ]])
fnCutscene([[ Append("...") ]])
fnCutsceneBlocker()

-- |[ ======================================== Clean Up ======================================== ]|
-- |[Unload Images]|
fnCutscene([[ fnUnloadBitmapsFromList("Chapter 5 Christine Secrebot TF") ]])

-- |[Relive Handler]|
--If this is a relive case, end the handler here.
if(iIsRelivingScene == 1.0) then
    
	--Wait a bit.
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Return to the last save point and execute the post-script..
	fnCutscene([[ AL_BeginTransitionTo("LASTSAVE", gsStandardReliveEnd) ]])
	fnCutsceneBlocker()
    return
end

-- |[Transition]|
--To the next map.
fnCutscene([[ AL_BeginTransitionTo("Sector254B", "FORCEPOS:1.0x1.0x0") ]])