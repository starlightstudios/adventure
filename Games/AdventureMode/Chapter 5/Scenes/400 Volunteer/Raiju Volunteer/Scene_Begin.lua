-- |[ ================================== Raiju Transformation ================================== ]|
--Cutscene of Christine's voluntary transformation into a Raiju.

-- |[ ======================================= Scene Setup ====================================== ]|
-- |[Variables]|
local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsRelivingScene", "N")

-- |[Image Streaming]|
fnLoadDelayedBitmapsFromList("Chapter 5 Christine Raiju TF", gciDelayedLoadLoadAtEndOfTick)

-- |[Flags]|
--Clear censor bars.
WD_SetProperty("Clear Censor Bars")

-- |[Relive Handler]|
--Wait a bit if reliving the scene, it needs to fade.
if(iIsRelivingScene == 0.0) then
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
end

-- |[ ==================================== Scene Execution ===================================== ]|
--Scene part 1.
fnCutscene([[ AL_SetProperty("Music", "Null") ]])
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ Append("Christine frowned as her organic stomach fluttered at the waiting.[P] Her eyes tracked Sophie as her girlfriend busied herself with the PDU's she had found scattered around the ranch.[B][C]") ]])
fnCutscene([[ Append("She pulled her eyes from Sophie's beaming face as she looked around at the PDUs, six in total, that were all pointed at the bed.[P] A bed in a small Raiju house.[P] A house for the Raijus' particular brand of...[P] Power generation.[B][C]") ]])
fnCutscene([[ Append("Each PDU had been placed to record the bed from a different angle, and Christine wondered if Sophie could have found more angles if she had found more PDUs.[B][C]") ]])
fnCutscene([[ Append("She crossed and uncrossed her legs as her body fidgeted in nervousness, then noticed she sat directly facing one of the PDUs.[P] A wry smile crossed her face, and she glanced at her distracted lover.[P] Keeping her eyes focused on Sophie, she spread her legs and pulled the hem of her skirt up, then pressed a finger deep against her panties.[B][C]") ]])
fnCutscene([[ Append("A moment passed, perhaps a minute as she rubbed her panties while watching Sophie work, and she closed her legs.[P] Her face heated with a deep flush of embarrassment.[P] She didn't know if the devices were recording yet, but if they were, she hoped Sophie would enjoy the brief pre-show.[B][C]") ]])
fnCutscene([[ Append("The flush had barely begun to fade before her nervousness returned and she began to question herself.[P] She was doing this to help people.[P] To help the ones who could not help themselves.[P] So why was she nervous?[B][C]") ]])
fnCutscene([[ Append("Christine began to fidget again as Sophie finished the last PDU, this one hung from an angle that promised a clear view from the foot of the bed.[P] Sophie spun toward the nearby console, her smile undiminished, and let out a small gasp.[P] A brief smile was all Christine needed to know the devices were indeed already recording.[B][C]") ]])
fnCutscene([[ Append("After a moment of tweaking the devices at the console, Sophie bolted to the door with an excited shout that Christine thought sounded more like a giggle.[B][C]") ]])
fnCutscene([[ Append("Silence descended on the room as the door fell shut.[B][C]") ]])
fnCutscene([[ Append("...[P] The door burst open, and Christine barely had a chance to turn before a Raiju pounced on her, pushing her to the bed and pinning her.[P] A cold metal tag dragged across her chest, teasing her breasts, and Christine glanced down to see the name 'Milky' written on it.[B][C]") ]])
fnCutscene([[ Append("At least, Christine thought it was her name.[P] Judging by the size of the breasts that hung in her view, it may well have been a description.[P] Christine shook her head and looked back up into the Raiju's grinning face.") ]])
fnCutsceneBlocker()

--Scene part 2.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/RaijuTF0") ]])
fnCutscene([[ WD_SetProperty("Register Censor Bar", 566, 337, 78, 116) ]])
fnCutscene([[ Append("'Milky' wasted no time before she placed her first kiss on Christine's lips.[P] A light tingle coursed through them, and Christine wondered if this was how all Raijus said 'hello' to each other.[P] A second tingling kiss on her lips and she began to appreciate the apparent disinterest in speech.[B][C]") ]])
fnCutscene([[ Append("Christine wondered if Milky knew her job was to transform the human beneath her, or if Milky even cared.[P] As the electric kisses moved toward her neck, Christine began to wonder if she cared herself.[P] It soon became clear her Raiju friend had other goals for her.[B][C]") ]])
fnCutscene([[ Append("Christine's hands moved towards Milky's body, hesitant, but drawn by a carnal force as old as history itself.[P] Milky didn't seem to mind the hesitation, nor the light touch that followed as it slid from her bared breasts down to the curve of her hips. [B][C]") ]])
fnCutscene([[ Append("Christine tried to force herself to relax as Milky's kisses moved down her neck.[P] She knew she had no reason to feel bad.[P] Sophie had encouraged this, and was likely already enjoying the show, albeit from a safe distance.[P] She needed to enjoy this, as well, if only so Sophie could.[B][C]") ]])
fnCutscene([[ Append("Gently, Christine put a hand on Milky's head and pushed her down.[P] Milky's lips lead a trail of tingling kisses as she let herself be pushed from Christine's neck to her breasts, nipping lightly as they passed a nipple, before sliding down and sucking at the skin just below her belly button.[B][C]") ]])
fnCutscene([[ Append("Milky glanced up at her and met Christine's eyes with a wink before she moved further down, her tongue delving into Christine's soft folds.[P] The nervousness and fear drained from her as the electric current slid from Milky's tongue and into the depths of Christine's body, teasing her nerves in ways she had never felt before.[B][C]") ]])
fnCutscene([[ Append("She knew she could no longer stop, even if she had wanted to.[P] Her fingers tensed and gripped Milky's hair as the Raiju's tongue poked, prodded, and massaged.[P] It was all Christine could do to hold on.[P] Her eyes slid closed as her body surged with the blissful current.") ]])
fnCutsceneBlocker()

--Scene part 3.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/RaijuTF0") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/RaijuTF1") ]])
fnCutscene([[ WD_SetProperty("Send Censor Bars To Crossfade") ]])
fnCutscene([[ WD_SetProperty("Register Censor Bar", 448, 242, 141, 65) ]])
fnCutscene([[ WD_SetProperty("Register Censor Bar", 784, 443, 114, 61) ]])
fnCutscene([[ Append("Her body had already began to change when she opened her eyes again.[P] Electricity sparked across her body, but the sparks did not hurt her.[P] Electrical arcs jumped between nearby limbs, but she felt little more than a whispered tickle.[B][C]") ]])
fnCutscene([[ Append("Tufts of fur began to sprout.[P] Her nails lengthened and hardened.[P] Her hair...[P] She could feel her hair thickening and standing in a permanent state of static arousal.[P] She could not stop her mind from wondering just how she would keep it managed.[B][C]") ]])
fnCutscene([[ Append("Milky's tongue grew still, then slid from the warm confines of Christine's body, and she withdrew as if she sensed Christine's biggest change was coming.[P] Christine groped for her head, desperate to push her face and tongue back into her body.[P] But when she glanced up at her partner, she realized why she had stopped.[B][C]") ]])
fnCutscene([[ Append("Milky gave her another wink, then opened her mouth.[P] Thick bolts of electricity, of lightning, danced between her fangs.[P] Christine's eyes grew wide, and Milky pressed her face and tongue between Christine's thighs once more.[B][C]") ]])
fnCutscene([[ Append("Christine's body seized and her back arched as the current surged through her.[P] Her fingers gripped Milky's hair with abandon, and the Raiju seemed to purr in response.[P] Her muscles shuddered as the orgasm swept through, chasing after the powerful current as her muscles began to be remade.[B][C]") ]])
fnCutscene([[ Append("All around her, the world surged to a blinding white.[P] A brief fear gripped Christine for a moment as she wondered if she had been blinded, but the fear passed as the light faded in an afterglow of sparks that matched the afterglow of her orgasm.[P] The entire room had filled with sparks.[B][C]") ]])
fnCutscene([[ Append("The shudder of her muscles slowed, and Christine pushed herself up.[P] Milky already stood by the door, a carnal grin stretched drunkenly across her face.[P] Christine stood and looked at her hands.[B][C]") ]])
fnCutscene([[ Append("Her muscles now had electrical glands in them, and every movement generated a current.[P] With time and practice, she knew, she would be able to control the discharge.[B][C]") ]])
fnCutscene([[ Append("A weapon of war.[B][C]") ]])
fnCutscene([[ Append("A tool of civilization.[B][C]") ]])
fnCutscene([[ Append("...[P] An instrument of pleasure.[B][C]") ]])
fnCutscene([[ Append("The door burst open and Milky stepped aside, allowing Sophie to run in.[P] Before Christine could even reach for her, Sophie had thrown her arms around her and drawn a few sparks from her lips with a crushing kiss.") ]])
fnCutsceneBlocker()

-- |[Relive Handler]|
--If this is a relive case, end the handler here.
if(iIsRelivingScene == 1.0) then
    
	--Wait a bit.
	fnCutsceneWait(15)
	fnCutsceneBlocker()
    
    --Unload images once finished booting to next area.
    fnCutscene([[ fnUnloadBitmapsFromList("Chapter 5 Christine Raiju TF") ]])
	
	--Return to the last save point and execute the post-script..
	fnCutscene([[ AL_BeginTransitionTo("LASTSAVE", gsStandardReliveEnd) ]])
	fnCutsceneBlocker()
    return
end

--Execute form changer.
VM_SetVar("Root/Variables/Costumes/Christine/sCostumeRaiju", "S", "Normal")
fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Raiju.lua") ]])

--Scene positioning.
fnCutsceneTeleport("RaijuA",    42.25, 34.50)
fnCutsceneFace(    "RaijuA",     0,     1)
fnCutsceneTeleport("Christine", 42.25, 35.50)
fnCutsceneFace(    "Christine",  1,     0)
fnCutsceneTeleport("Sophie",    43.25, 35.50)
fnCutsceneFace(    "Sophie",    -1,     0)
fnCutsceneBlocker()

--Return to the ranch.
fnCutsceneWait(25)
fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("Raiju:[VOICE|HumanF0] Phew![P] You're pretty good for a first-timer![P] We should go again once you've charged up!") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

fnCutscene([[ AL_SetProperty("Music", "BreannesTheme") ]])

--Raiju walks off.
fnCutsceneMove("RaijuA", 45.25, 34.50)
fnCutsceneBlocker()
fnCutsceneFace("Christine", 1, 0)
fnCutsceneFace("Sophie", 1, 0)
fnCutsceneMove("RaijuA", 51.25, 34.50)
fnCutsceneMove("RaijuA", 51.25, 28.50)
fnCutsceneBlocker()
fnCutsceneTeleport("RaijuA", 36.25, 19.50)
fnCutsceneFace("Sophie", -1, 0)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
fnCutscene([[ Append("Christine:[E|Happy] Eeeeee![P] Look at me Sophie![P] I'm - [P][CLEAR]") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] Just get them all out of your system.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] Huh?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] The electricity puns.[P] Just get them alllllll out.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Happy] Do you find my appearance shocking?[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Laugh] [EMOTION|Sophie|Neutral]I'm excited to see you![B][C]") ]])
fnCutscene([[ Append("Christine:[E|Laugh] [EMOTION|Sophie|Offended]My services are for sale -[P] ask me what I charge![B][C]") ]])
fnCutscene([[ Append("Christine:[E|Laugh] Got to go, better bolt![B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Offended] ...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] I'm hideous, Sophie.[P] But luckily -[P] opposites attract![B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Happy] Okay, you are redeemed.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Happy] And I got some great recordings until you blew out the white balancers![P] Hee hee![B][C]") ]])
fnCutscene([[ Append("Christine:[E|Offended] Yeah, sorry about that.[P] Luckily you can just recalibrate the receivers.[P] No permanent damage.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] I've already set them up to do just that.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] So what precisely are you going to do with the videograph?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Blush] Delete it and never look at it ever again![B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] ...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Cry] Yes, Sophie, I absolutely believe you.[P] That is something you would do.[P] Not an obvious lie, nope.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Blush] Hee hee![B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] Well, back to work, then.[P] I hope 55 doesn't find my appearance too sh - [B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Offended] Don't say it![B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] Surprising.[P] Obviously.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Happy] Hee hee![P] Let's go!") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

-- |[ ======================================== Clean Up ======================================== ]|
-- |[Unload Images]|
--Clean.
fnCutscene([[ fnUnloadBitmapsFromList("Chapter 5 Christine Raiju TF") ]])

--Autofold.
fnAutoFoldParty()
fnCutsceneBlocker()
