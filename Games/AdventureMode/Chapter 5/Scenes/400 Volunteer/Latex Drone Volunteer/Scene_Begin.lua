-- |[Volunteering to become a Latex Drone]|
--Optional part of the manufactory subquest.

-- |[Variables]|
local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsRelivingScene", "N")

-- |[Repeat Check]|
--If Christine can already turn into a Latex Drone, this scene does not play.
local iHasLatexForm = VM_GetVar("Root/Variables/Global/Christine/iHasLatexForm", "N")
if(iHasLatexForm == 1.0 and iIsRelivingScene == 0.0) then
	LM_ExecuteScript(gsStandardGameOver)
	return
end

-- |[Form]|
--Switch Christine to Latex Drone. She's not actually visible at all until the transformation is completed.
--LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Latex.lua")

--Unlock Latex Drone form.
VM_SetVar("Root/Variables/Global/Christine/iHasLatexForm", "N", 1.0)
fnLoadDelayedBitmapsFromList("Chapter 5 Christine Drone TF", gciDelayedLoadLoadAtEndOfTick)

-- |[Topics]|
--Unlock these topics if they weren't already.
if(iIsRelivingScene == 0.0) then
    WD_SetProperty("Unlock Topic", "LatexDrones", 1)
    WD_SetProperty("Unlock Topic", "CPU Inhibitors", 1)
end

-- |[Music]|
fnCutscene([[ AL_SetProperty("Music", "Null") ]])

-- |[Cutscene Execution]|
--Wait a bit.
fnCutsceneWait(65)
fnCutsceneBlocker()

--Clear censor bars.
WD_SetProperty("Clear Censor Bars")

--Scene.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ Append("As Christine walked up to the tube, she shivered.[P] The still basement air was unusually cold against her nude human skin.[P] With the click of a button, Sophie opened the door to the tube, allowing Christine to walk in.[B][C]") ]])
fnCutscene([[ Append("As Christine stepped in, her mind started to race.[P] What if something goes wrong?[P] The tube was sent in to be repaired, and Sophie took care of it almost immediately.[P] But...[P] what if...?[B][C]") ]])
fnCutscene([[ Append("No.[P] Sophie was an amazingly talented repair bot.[P] There's no way she could have messed up something as routine as this.[B][C]") ]])
fnCutscene([[ Append("Pulling herself out of her internal debate, Christine focused her attention back on Sophie.[P] Her fears were assuaged when she saw a beaming, silvery smile plastered all over Sophie's face.[P] Christine gave her a thumbs up, and Sophie responded by closing the tube's door.") ]])
fnCutsceneBlocker()

--Scene.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Null") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/LatexTF0") ]])
fnCutscene([[ WD_SetProperty("Register Censor Bar", 604, 222, 100, 41) ]])
fnCutscene([[ WD_SetProperty("Register Censor Bar", 612, 320, 130, 37) ]])
fnCutscene([[ Append("Almost immediately, the temperature seemed to drop.[P] There was a faint whirr as spindly arms began to descend from their resting spot, each one tipped with a small nozzle.[P] They quickly reached her feet and began to spray a thick, inky substance onto her smooth skin.[P] The mist swirled in the breeze, chilling her feet even more.[B][C]") ]])
fnCutscene([[ Append("It was cool, like water, but it was also firm and comforting, like a warm sock.[P] Christine had never felt anything like it before, and as it continued making its way up her bare legs, she realized...[P] she wanted more.[B][C]") ]])
fnCutscene([[ Append("The seemingly chilly temperature was soon forgotten as thoughts of the substance covering her grew more and more appealing. As she reached down to touch it, she glimpsed her one-machine audience.[P] Sophie was almost as entranced as Christine was.[B][C]") ]])
fnCutscene([[ Append("The substance was silky smooth, and squeaked when she ran her fingers across it.[P] Just as it moved across her legs and now her thighs, the latex-like material started making its way up her fingers and forearms.[P] As it covered more and more of her skin, she found it harder and harder to concentrate.[P] Anywhere the latex touched began to radiate a faint heat, one that grew warmer the more the substance grew.[B][C]") ]])
fnCutscene([[ Append("She could feel her nether lips start to drip with arousal, the latex growing closer and closer until -[P] in a swift, cathartic moment -[P] it dove into her waiting sex.[P] The sudden movement sent a bloom of heat radiating through her body, the sensation causing a nigh instant orgasm -[P] one that seemed to last an eon.[B][C]") ]])
fnCutscene([[ Append("As if spurred on by this, the latex grew faster and faster -[P] not just over her chest and torso, but also deep inside of her, looping back into that wondrous orgasm.[P] She could feel every crevice of her vagina being covered by that wonderful, warm latex material.[P] Christine could barely stand the intense onslaught of her body's rapid encasement.[B][C]") ]])
fnCutscene([[ Append("Just as it seemed to start to fade away, the latex reached her now erect nipples, flowing over and teasing them like a skilled lover.[P] This was enough to bring her to another mind-blanking orgasm, one that felt even more intense than the first.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/LatexTF0") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/LatexTF1") ]])
fnCutscene([[ WD_SetProperty("Send Censor Bars To Crossfade") ]])
fnCutscene([[ WD_SetProperty("Register Censor Bar", 590, 180, 100, 41) ]])
fnCutscene([[ Append("As the latex reached her neck, Christine vaguely remembered something important about how drones are made.[P] Something about...[P] a chip...[P] that did...[P] something?[P] It was too hard now to try to remember.[P] The material was now rushing over the last uncovered bits of her head, her entire body was on fire from the material now covering her completely.[B][C]") ]])
fnCutscene([[ Append("Everything she knew and loved about Regulus society began to be more and more appealing, as well as the desire to...[P] to...[P] serve.[B][C]") ]])
fnCutscene([[ Append("Unbeknownst to Christine, the conversion tube had entered its second phase of the transformation.[P] Her organs were being replaced with infallible machine parts -[P] some universal to all the golem series, some completely new and unique to the drones.[P] Her brain was transforming back into the computer she knew and loved it as.[P] But most importantly, her mind was being scanned for its activity.[B][C]") ]])
fnCutscene([[ Append("As it grew harder and harder for Christine to have an organic thought, her newly formed thought inhibitor chip grew more and more powerful;[P] each thought she had was either being suppressed or converted into a new way to serve.[P] With her thoughts growing dim, her other senses were heightened.[P] She could feel her feet pulling upwards into a hoof-heeled position, customary for all drones.[B][C]") ]])
fnCutscene([[ Append("Her skin became tightly bound by latex ropes, accentuating her every curve;[P] her face growing away from her metal skeleton, becoming an expressive mask;[P] and her now auburn hair spilling down her back, growing all the way down to her ass.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/LatexTF1") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/LatexTF2") ]])
fnCutscene([[ WD_SetProperty("Send Censor Bars To Crossfade") ]])
fnCutscene([[ Append("A blissful eternity after the tube began its work, it retracted its arms back to the metal ceiling and the door clicked open.[B][C]") ]])
fnCutscene([[ Append("Christine was now a fully-fledged latex drone, her mind dominated by the need to serve.[P] As she stepped out of the pod, she noticed on some deep, muted level how much more acute everything felt, the cool basement air now tangible on every inch of her onyx surface.[B][C]") ]])
fnCutscene([[ Append("Now, she could even feel the faint motion of the air as it blew slowly out of a nearby ventilation grate.[P] Her simplified mind soon lost interest in this, and she reverted back to her main assignment.[P] LOCATE SUPERIOR UNIT, SERVE SUPERIOR UNIT.[P] She looked around and noticed Unit 499323 standing in front of her, looking winded.[P] Happy to have found someone to serve, Christine stepped forward.") ]])
fnCutsceneBlocker()

--Clean.
fnCutsceneWait(gciDialogue_Fadeout_Then_Unload_Ticks)
fnCutsceneBlocker()
fnCutscene([[ fnUnloadBitmapsFromList("Chapter 5 Christine Drone TF") ]])

--If this is a relive case, end the handler here.
if(iIsRelivingScene == 1.0) then
    
	--Wait a bit.
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Return to the last save point and execute the post-script..
	fnCutscene([[ AL_BeginTransitionTo("LASTSAVE", gsStandardReliveEnd) ]])
	fnCutsceneBlocker()
    return
end

--Otherwise, the cutscene resumes from Sophie's dialogue handler.