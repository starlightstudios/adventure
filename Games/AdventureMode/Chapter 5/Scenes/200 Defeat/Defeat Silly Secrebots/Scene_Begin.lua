-- |[ =============================== Silly Secrebots "Bad End" ================================ ]|
--Plays when the party is defeated during the secrebot warehouse sequence. A very non-canon sequence.

-- |[ ======================= Scene Setup ======================== ]|
-- |[Variables]|
local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsRelivingScene", "N")

-- |[Achievement]|
AM_SetPropertyJournal("Unlock Achievement", "NonCanonStory")

-- |[Combat]|
--Restore party to full HP in case the player doesn't want to use the save point. This saves time, and is a quality-of-life feature.
AdvCombat_SetProperty("Restore Party")

-- |[Loading]|
--Load assets.
fnLoadDelayedBitmapsFromList("Chapter 5 Secrebot Bad End", gciDelayedLoadLoadAtEndOfTick)

-- |[Display]|
--Blackout.
AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1)

--Drop any other events.
Cutscene_CreateEvent("DROPALLEVENTS")

-- |[Relive Handler]|
--Wait a bit if reliving the scene.
if(iIsRelivingScene == 0.0) then
    fnCutsceneWait(45)
    fnCutsceneBlocker()
end

-- |[ ===================== Scene Execution ====================== ]|
--Scene.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Null") ]])
fnCutscene([[ Append("Someone was standing over the defeated units.[P] Christine could not raise her head, but she knew who it was.[B][C]") ]])
fnCutscene([[ Append("[VOICE|GolemLord]'How expected.[P] With these two on our side, we should have no further problems.'[B][C]") ]])
fnCutscene([[ Append("[VOICE|GolemLord]'This one won't be an issue.[P] The other...'[B][C]") ]])
fnCutscene([[ Append("[VOICE|GolemLord]'You know, they say a command unit cannot be repurposed.[P] Let's test that theory.'[B][C]") ]])
fnCutscene([[ Append("Christine and 55 were dragged off as they slipped into standby.[P] Christine didn't need much convincing, but 55 was more difficult.[P] She was mentally quite resilient.[P] Many of her programs had to be overridden at a low level.[B][C]") ]])
fnCutscene([[ Append("There was little of her personality that wasn't angry defiance, despite her frequently neutral, logical demeanor.[P] Much of it needed to be discarded.[P] Odess worked patiently, molding the clay of her victim's mind.[B][C]") ]])
fnCutscene([[ Append("The parts could snap in.[P] She delighted in the sight of it, a powerful unit brought low and into her service.[P] The clothes were an afterthough.[P] Maid uniforms, the final humiliation.[B][C]") ]])
fnCutscene([[ Append("...[B][C]") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Null") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/SecrebotBadEnd/SecrebotBadEnd") ]])
fnCutscene([[ Append("Time passed.[P] Many of the lords chattered amongst themselves, wondering what the big new display at the shopping sector was going to be.[P] There had been rumours circulating, but nothing certain.[B][C]") ]])
fnCutscene([[ Append("Odess pulled the sheet off, dropping it to the floor and revealing three brand new secrebots available for auction.[P] CR-1-15, CR-1-16, and CR-2-1.[P] Night, Christine, and 55.[P] Standing idle.[B][C]") ]])
fnCutscene([[ Append("Obedient robotic maids ready to fulfill any orders of their owner.[P] Cleaning, fighting, construction, production, anything their mechanical hearts desired.[B][C]") ]])
fnCutscene([[ Append("The bidding was fierce, starting at a paltry 500 work credits.[P] The lords practically fought one another for the chance to spend their work credits to acquire these unique secrebots.[P] Some even said one of them bore a resemblence to a command unit who had gone missing.[B][C]") ]])
fnCutscene([[ Append("Christine sold for 1500.[P] Night sold for 2275.[P] 55, despite the rumours, soon broke 5000 work credits as command units and particularly miserly lords dipped into their reserves.[P] None of them suspected a thing.[B][C]") ]])
fnCutscene([[ Append("Christine, or CR-1-16, smiled idly.[P] She could not wait to serve her new owner, and eventually, subsume them beneath her true master's heel.[B][C]") ]])
fnCutscene([[ Append("And so the influence of Vivify crept across the city...") ]])
fnCutsceneBlocker()

-- |[ ====================== Scene Cleanup ======================= ]|
--Clean.
fnCutscene([[ fnUnloadBitmapsFromList("Chapter 5 Secrebot Bad End") ]])

-- |[Relive Handler]|
--If this is a relive case, end the handler here.
if(iIsRelivingScene == 1.0) then
    
	--Wait a bit.
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Return to the last save point and execute the post-script..
	fnCutscene([[ AL_BeginTransitionTo("LASTSAVE", gsStandardReliveEnd) ]])
	fnCutsceneBlocker()
    return
end

--Transition to ending.
fnCutscene([[ AL_BeginTransitionTo("RegulusCityC", "FORCEPOS:4.0x1.0x0") ]])