-- |[Debug_Startup]|
--This lua file is only ever called if the cutscene is triggered via the debug menu. It will make sure
-- the map is correct and that the characters are present and in their default positions. It will also,
-- if necessary, set scripting flags so the cutscene makes sense.
--Note: This is only and explicitly used for debug menu work, this is NOT the script that gets called
-- during a normal back-to-save case, that's in the 300 Standards folder.
Debug_PushPrint(true, "Debug Firing Cutscene: Defeat_BackToSave\n")

--The debug version does nothing that the regular scene does not.
LM_ExecuteScript(fnResolvePath() .. "Scene_Begin.lua")

--Debug.
Debug_PopPrint("Completed debug cutscene firing.\n")
