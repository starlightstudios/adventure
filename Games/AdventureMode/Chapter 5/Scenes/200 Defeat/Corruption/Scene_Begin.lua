-- |[Corruption]|
--This triggers when an enemy walks into Christine during the dream sequence. Initiative doesn't help.
--The TilemapActor that triggered the hit is on the activity stack during this cutscene.

--Variables.
local iChristineBumpCount = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineBumpCount", "N")

--Less than three bumps: Enemy deallocates, play the effect.
if(iChristineBumpCount < 2.0) then
    AudioManager_PlaySound("World|SparksB")
    VM_SetVar("Root/Variables/Chapter5/Scenes/iStaticTimer", "N", 0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iChristineBumpCount", "N", iChristineBumpCount + 1)
    RE_SetDestruct(true)
    return
end

--Three bumps: Add one to the corruption state, play a cutscene.
AudioManager_PlaySound("World|SparksC")
VM_SetVar("Root/Variables/Chapter5/Scenes/iChristineBumpCount", "N", 0)

--Disable viewcones.
AL_SetProperty("Hide All Viewcones", true)

--Disable music.
AL_SetProperty("Music", "Null")

--Black the screen out immediately.
fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_Characters, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])

--Teleport Christine to the corner so another battle doesn't trigger.
fnCutsceneTeleport("Christine", 72.25, -10.50)
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(65)
fnCutsceneBlocker()
fnCutscene([[ AL_SetProperty("Foreground Alpha", 0, 1.00, 0) ]])
fnCutscene([[ AudioManager_PlaySound("World|Chaos") ]])

--Play an overlay. Use a TPF for this, it works as an animation.
local iTPF = 3

--Run loops at full alpha in time with the sound effect. We need 6 seconds of loops.
for i = 1, 12, 1 do
    for p = 0, 9, 1 do
        local sString = "AL_SetProperty(\"Foreground Image\", 0, \"Root/Images/Overlays/CRTNoise/CRT" .. p .. "\")"
        fnCutscene(sString)
        fnCutsceneWait(iTPF)
        fnCutsceneBlocker()
    end
end

--Run more as alpha decays. Sound effect fades out. 4 seconds of loops.
local iCurrentFrame = 0.0
local iFramesTotal = 8.0 * 10.0
for i = 1, 8, 1 do
    for p = 0, 9, 1 do
        
        --Compute which image to display.
        local sString = "AL_SetProperty(\"Foreground Image\", 0, \"Root/Images/Overlays/CRTNoise/CRT" .. p .. "\")"
        fnCutscene(sString)
        
        --Compute the alpha.
        local iAlpha = 1.0 - (iCurrentFrame / iFramesTotal)
        sString = "AL_SetProperty(\"Foreground Alpha\", 0, " .. iAlpha .. ", 0)"
        fnCutscene(sString)
        
        fnCutsceneWait(iTPF)
        fnCutsceneBlocker()
        iCurrentFrame = iCurrentFrame + 1
    end
end

--Wait a bit.
fnCutscene([[ AL_SetProperty("Foreground Alpha", 0, 0.00, 0) ]])
fnCutsceneWait(185)
fnCutsceneBlocker()

--Corruption case. Increment corruption counter, play a scene based on that. This teleports the player back to Flashback B.
local iCorruptionCount = VM_GetVar("Root/Variables/Chapter5/Scenes/iCorruptionCount", "N")
VM_SetVar("Root/Variables/Chapter5/Scenes/iCorruptionCount", "N", iCorruptionCount + 1)

--Zero corruption:
if(iCorruptionCount == 0.0) then
    fnCutscene([[ AL_BeginTransitionTo("RegulusFlashbackB", "FORCEPOS:1.0x1.0x0") ]])
    fnCutsceneBlocker()

--One corruption, new scene:
elseif(iCorruptionCount == 1.0) then
    fnCutscene([[ AL_BeginTransitionTo("RegulusFlashbackB", "FORCEPOS:5.0x1.0x0") ]])
    fnCutsceneBlocker()

--Two corruption, new scene:
elseif(iCorruptionCount == 2.0) then
    fnCutscene([[ AL_BeginTransitionTo("RegulusFlashbackB", "FORCEPOS:9.0x1.0x0") ]])
    fnCutsceneBlocker()
    
--Three corruption: BDSM Sophie scene.
else
    fnCutscene([[ AL_BeginTransitionTo("RegulusFlashbackL", "FORCEPOS:14.0x5.0x0") ]])
    fnCutsceneBlocker()
    
end

    