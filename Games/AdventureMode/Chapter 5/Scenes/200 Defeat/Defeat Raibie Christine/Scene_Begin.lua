-- |[Defeat By Christine Raibie, Plays a Special Scene]|
--Plays when 55 is defeated by Raibie Christine.
AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1)

--Drop any other events.
Cutscene_CreateEvent("DROPALLEVENTS")

--If we are currently in the last-saved room, reposition the party.
AL_BeginTransitionTo("RegulusBiolabsGammaWestA", "FORCEPOS:48.0x25.0x0")

-- |[Combat]|
--Restore party to full HP in case the player doesn't want to use the save point. This saves time, and is a quality-of-life feature.
AdvCombat_SetProperty("Restore Party")