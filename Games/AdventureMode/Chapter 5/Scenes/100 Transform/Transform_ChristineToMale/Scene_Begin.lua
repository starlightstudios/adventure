-- |[Transform Christine to Male]|
--Used at a save point or sometimes cutscenes to transform the named character to the named form.
local sActorName = "Christine"
local sVarName = "Christine"
local sFormHandler = "LM_ExecuteScript(gsRoot .. \"FormHandlers/Christine/Form_Male.lua\")"

-- |[Blockers]|
--If something is preventing a listed TF from taking place, handle it here.

-- |[Variables]|
--Store the starting form and other variables.
local sStartingForm = VM_GetVar("Root/Variables/Global/" .. sVarName .. "/sForm", "S")

-- |[Execute Transformation]|
--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Flash the active character to white. Immediately after, execute the transformation.
Cutscene_CreateEvent("Flash Actor White", "Actor")
	ActorEvent_SetProperty("Subject Name", sActorName)
	ActorEvent_SetProperty("Flashwhite Quickly")
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
fnCutscene(sFormHandler)
fnCutsceneWait(gci_Flashwhite_Ticks_Total)
fnCutsceneBlocker()

--Now wait a little bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Guaranteed Cutscene Execution]|
--If a cutscene needs to fire after the transformation, handle it here.
if(false) then
    return
end

-- |[Random Cutscene Execution]|
--Roll a random number. There is a 25% chance that a scene will play, otherwise the transformation is instant and no scenes play.
local iRoll = LM_GetRandomNumber(1, 100)
if(iRoll <= 75) then
	return
end

--Fire the scene.
if(true) then
	
end
