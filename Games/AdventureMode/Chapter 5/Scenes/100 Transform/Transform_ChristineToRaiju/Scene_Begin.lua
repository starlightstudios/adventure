-- |[Transform Christine to Raiju]|
--Used at a save point or sometimes cutscenes to transform the named character to the named form.
local sActorName = "Christine"
local sVarName = "Christine"
local sFormHandler = "LM_ExecuteScript(gsRoot .. \"FormHandlers/Christine/Form_Raiju.lua\")"

-- |[Blockers]|
--If something is preventing a listed TF from taking place, handle it here.
local iCompletedBlackSite = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedBlackSite", "N")
if(iCompletedBlackSite >= 1.0 and iCompletedBlackSite < 10.0) then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Christine:[VOICE|Christine] (I have to stay human, or JX-101 will never trust me!)") ]])
    return
end

--Sophie on a date, but haven't shown her your forms.
local iIsOnDate = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
local iSophieKnowsAboutRunestone = VM_GetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N")
if(iIsOnDate == 1.0 or iIsOnDate == 2.0) then
    
    --She doesn't know about the runestone:
    if(iSophieKnowsAboutRunestone == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] (Let's maybe not transform in front of Sophie, it'll scare her.[P] Best break it to her slowly.)") ]])
    
    --She does:
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] (If I transform, I'll just have to transform when I leave the repair bay...)") ]])
    end
    return
end

--Vacuum check.
local sLevelName = AL_GetProperty("Name")
if(sLevelName == "RegulusExteriorWA") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Leader](It might be a bad idea to transform myself into an organic when I am currently exposed to a vacuum.[P] Didn't go so well last time...)") ]])
	fnCutsceneBlocker()
	
	return
end

-- |[Variables]|
--Store the starting form and other variables.
local sStartingForm = VM_GetVar("Root/Variables/Global/" .. sVarName .. "/sForm", "S")

-- |[Execute Transformation]|
--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Flash the active character to white. Immediately after, execute the transformation.
Cutscene_CreateEvent("Flash Actor White", "Actor")
	ActorEvent_SetProperty("Subject Name", sActorName)
	ActorEvent_SetProperty("Flashwhite Quickly")
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
fnCutscene(sFormHandler)
fnCutsceneWait(gci_Flashwhite_Ticks_Total)
fnCutsceneBlocker()

--Now wait a little bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Guaranteed Cutscene Execution]|
--If a cutscene needs to fire after the transformation, handle it here.
if(false) then
    return
end

-- |[Random Cutscene Execution]|
--Roll a random number. There is a 25% chance that a scene will play, otherwise the transformation is instant and no scenes play.
local iRoll = LM_GetRandomNumber(1, 100)
if(iRoll <= 75) then
	return
end

--Fire the scene.
if(true) then
	
end
