-- |[Work Terminal]|
--Work terminals can be accessed from several places on the station, so their scripts are rerouted here.

--Variables.
local iTalkedToSophie         = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
local iCheckedTerminal        = VM_GetVar("Root/Variables/Chapter5/Scenes/iCheckedTerminal", "N")
local iReceivedFunction       = VM_GetVar("Root/Variables/Chapter5/Scenes/iReceivedFunction", "N")
local iMet55InLowerRegulus    = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N")
local iSawSpecialAnnouncement = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawSpecialAnnouncement", "N")
local iMet55InBasement        = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InBasement", "N")

-- |[ =============================== Inactive for Story Reasons =============================== ]|
--Hasn't talked to Sophie yet:
if(iTalkedToSophie == 0.0) then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Console: Login credentials unknown.[P] Please report to maintenance for access permissions.") ]])
	fnCutsceneBlocker()

--Has talked to Sophie, hasn't received a function yet.
elseif(iTalkedToSophie == 1.0 and iCheckedTerminal == 0.0) then
	VM_SetVar("Root/Variables/Chapter5/Scenes/iCheckedTerminal", "N", 1.0)
    --VM_SetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N", 12.0)
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Console: Login credentials accepted.[P] Unit 771852 has no function currently assigned.[B][C]") ]])
	fnCutscene([[ Append("Console: Your request is currently being processed.[P] Please wait in the break room of sector 96.[P] A message will be sent to your PDU when ready.[B][C]") ]])
	fnCutscene([[ Append("Christine:[VOICE|Christine] (Break room, eh.[P] The schematics say that's in the northeast part of this area.)[B][C]") ]])
	fnCutscene([[ Append("Christine:[VOICE|Christine] Lord Unit 771852 will await her function assignment.[B][C]") ]])
	fnCutscene([[ Append("Console: The Cause of Science will be served.[P] Logging you off.") ]])
	fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Set Layer Disabled", "WorkTerminalToggleA", false) ]])
	fnCutscene([[ AL_SetProperty("Set Layer Disabled", "WorkTerminalToggleB", false) ]])
    
    --[=[
	fnCutscene([[ Append("Console: A special note has been placed on your designation.[P] Your function is to assume command over Maintenance and Repair facilities in this sector.[B][C]") ]])
	fnCutscene([[ Append("Christine:[VOICE|Christine] (Maintenance and Repair?[P] I'll be in charge of Sophie's work![P] That's wonderful!)[B][C]") ]])
	fnCutscene([[ Append("Console: Roaming permissions established.[P] Please consult the security units if you require access to a high-security area for your duties.[B][C]") ]])
	fnCutscene([[ Append("Christine:[VOICE|Christine] (I now have access to most of the facility.[P] Sophie will be so happy to hear that we'll be working together![P] I should tell her right away!)[B][C]") ]])
	fnCutscene([[ Append("Console: Assignments appear on work terminals such as this one from time to time.[P] Work terminals will have green screens when an assignment is available, and red when none is available.[P] If you would like to complete assignments for additional work credits, log on and perform the assignment.[P] You will be credited for you work when it is done.[B][C]") ]])
	fnCutscene([[ Append("Console: Work credits may be spent at the fabricators for access to special equipment, or used to procure items for civilian consumption.[B][C]") ]])
	fnCutscene([[ Append("Console: Carry out your function with dignity and poise, Lord Unit.[B][C]") ]])
	fnCutscene([[ Append("Christine:[VOICE|Christine] Lord Unit 771852 will carry out her function.[B][C]") ]])
	fnCutscene([[ Append("Console: The Cause of Science will be served.[P] Logging you off.") ]])
	fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Set Layer Disabled", "WorkTerminalToggleA", false) ]])
	fnCutscene([[ AL_SetProperty("Set Layer Disabled", "WorkTerminalToggleB", false) ]])
    
    --Change the job name of Christine's Repair Unit class.
    AdvCombat_SetProperty("Push Party Member", "Christine")
        AdvCombatEntity_SetProperty("Push Job S", "Repair Unit")
            AdvCombatJob_SetProperty("Display Name", "Repair Unit")
        DL_PopActiveObject()
    DL_PopActiveObject()
    ]=]

-- |[ ================================= No Special Assignments ================================= ]|
--Hasn't met up with 55 yet, no special assignments ever appear.
elseif(iMet55InLowerRegulus == 0.0) then

    --If currently on a date with Sophie, do nothing.
    local iIsOnDate = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
    if(iIsOnDate == 1.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Console: Login accepted for Unit 771852.[P] You are currently assigned - [P][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Oh bother.[P] This can wait until later.[P] I didn't see a single unit come in or out of the repair bay today except Sophie and myself.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] I must remember to check the work terminal after I'm done having fun with Sophie, of course!") ]])

	--If the special assignment to meet 55 in the basement of maintenance is on:
	elseif(iSawSpecialAnnouncement == 1.0 and iMet55InBasement == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawSpecialAnnouncement", "N", 2.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Console: Login accepted for Unit 771852.[P] You are currently assigned to meet a unit in the basement of Maintenance and Repair.[P] No further details are available.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] No further details?[P] Who put the assignment in?[B][C]") ]])
		fnCutscene([[ Append("Console: No metadata present.[P] Confirm function assignment.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Lord Unit 771852 will carry out her function.[B][C]") ]])
		fnCutscene([[ Append("Console: The Cause of Science will be served.[P] Logging you off.") ]])
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Set Layer Disabled", "WorkTerminalToggleA", false) ]])
		fnCutscene([[ AL_SetProperty("Set Layer Disabled", "WorkTerminalToggleB", false) ]])

	--Repeat text.
	elseif(iSawSpecialAnnouncement == 2.0 and iMet55InBasement == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Console: Login credentials accepted.[P] Unit 771852 is assigned to report to the maintenance room basement for further instruction.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Lord Unit 771852 will carry out her function.[B][C]") ]])
		fnCutscene([[ Append("Console: The Cause of Science will be served.[P] Logging you off.") ]])
		fnCutsceneBlocker()

	--Normal.
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Console: Login accepted for Unit 771852.[P] No tasks exist at this time to be completed.") ]])
		fnCutsceneBlocker()
	end

-- |[ ================================= Build Assignment List ================================== ]|
elseif(iTalkedToSophie == 1.0 and iReceivedFunction == 1.0 and iMet55InLowerRegulus == 1.0) then

    -- |[Variables]|
	--General:
	local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "I")

	--Serenity Crater:
	local iSerenityWorkOrder = VM_GetVar("Root/Variables/Chapter5/Scenes/iSerenityWorkOrder", "N")
	local iCompletedSerenity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
	
	--Cassandra Quest:
	local iTookCassandraQuest     = VM_GetVar("Root/Variables/Chapter5/Scenes/iTookCassandraQuest", "N")
	local iResolvedCassandraQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iResolvedCassandraQuest", "N")
	
	--Tellurium Mines
	local iTelluriumMinesQuest     = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasMinesWorkOrder", "N")
	local iCompletedMinesWorkOrder = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedMinesWorkOrder", "N")
    
    --Electrosprites.
    local i198WorkOrder = VM_GetVar("Root/Variables/Chapter5/Scenes/i198WorkOrder", "N")
    local iSaw198Intro  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSaw198Intro", "N")
    
    --Manufactory.
    local iManuTookJob     = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuTookJob", "N")
    local iManuFinishedJob = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuFinishedJob", "N")
    
    --Biolabs flag.
    local iReachedBiolabs = VM_GetVar("Root/Variables/Chapter5/Scenes/iReachedBiolabs", "N")
    
    --Sector 254 (Secrebots)
    local i254WorkOrder      = VM_GetVar("Root/Variables/Chapter5/Scenes/i254WorkOrder", "N")
    local i254TalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/i254TalkedToSophie", "N")
    local i254Transformed    = VM_GetVar("Root/Variables/Chapter5/Scenes/i254Transformed", "N")
    local iSaw55sMemories    = VM_GetVar("Root/Variables/Chapter5/Scenes/iSaw55sMemories", "N")
    
    --Runestone Upgrade Quest
    local iRuneUpgradeTaken     = VM_GetVar("Root/Variables/Chapter5/Scenes/iRuneUpgradeTaken", "N")
    local iRuneUpgradeCompleted = VM_GetVar("Root/Variables/Chapter5/Scenes/iRuneUpgradeCompleted", "N")
	
    -- |[Text]|
	--Basic text.
	local sString = "WD_SetProperty(\"Append\", \"Console: Login accepted for Unit 771852.[P] Current work credits: " .. iWorkCreditsTotal .. ".[P] Please select an option.[BLOCK]\")"
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene(sString)
	local sDecisionScript = "\"" .. fnResolvePath() .. "Response.lua" .. "\""
    local sPurchaseScript = "\"" .. fnResolvePath() .. "Purchase.lua" .. "\""
	fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
	
    -- |[Options Builder]|
	--Basic options that are always present:
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Turn in Credits Chips\", " .. sPurchaseScript .. ", \"CreditsChips\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Purchase Items\", " .. sPurchaseScript .. ", \"Purchase Items\") ")
    
    --Serenity Crater: Does not need to be turned in, disabled when the biolabs are reached.
	if(iCompletedSerenity == 0.0 and iReachedBiolabs == 0.0) then
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Serenity Crater\", " .. sDecisionScript .. ", \"Serenity Crater\") ")
	end
    
    --Cassandra quest. Can be turned in at the biolabs, but not taken.
	if(iResolvedCassandraQuest < 3.0) then
        if(iReachedBiolabs == 0.0 or (iReachedBiolabs == 1.0 and (iResolvedCassandraQuest == 1.0 or iResolvedCassandraQuest == 2.0))) then
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sector 15\", " .. sDecisionScript .. ", \"Sector 15\") ")
        end
	end
    
    --Tellurium Mines. Cannot be taken in the biolabs.
	if(iCompletedMinesWorkOrder == 0.0 and iReachedBiolabs == 0.0) then
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Tellurium Mines\", " .. sDecisionScript .. ", \"Tellurium Mines\") ")
	end
    
    --Electrosprites. Cannot be taken in the biolabs.
    if(i198WorkOrder == 0.0 and iSaw198Intro == 0.0 and iReachedBiolabs == 0.0) then
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sector 198\", " .. sDecisionScript .. ", \"Sector 198\") ")
    end
    
    --Manufactory. Cannot be taken in the biolabs.
    if(iManuFinishedJob == 0.0 and iReachedBiolabs == 0.0) then
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Efficiency Request\", " .. sDecisionScript .. ", \"Efficiency\") ")
    end
    
    --Secrebot Quest. Must complete the LRT facility first.
    if(i254TalkedToSophie == 0.0 and iSaw55sMemories == 1.0 and iReachedBiolabs == 0.0) then
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sector 96 Repair Bay\", " .. sDecisionScript .. ", \"RepairBay\") ")
    end
    
    --Runestone Upgrade Quest. Must complete the LRT facility first.
    if(iRuneUpgradeCompleted == 0.0 and iSaw55sMemories == 1.0 and iReachedBiolabs == 0.0) then
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Undercity First Floor\", " .. sDecisionScript .. ", \"Undercity\") ")
    end
    
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Cancel\", " .. sDecisionScript .. ", \"Cancel\") ")
	fnCutsceneBlocker()
end