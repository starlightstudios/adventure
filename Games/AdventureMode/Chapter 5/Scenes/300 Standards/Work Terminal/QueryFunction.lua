-- |[Work Terminal Query]|
--Sets the return value to true if there's a work assignment available, false if not. It's got the same logic
-- sequence as the script execution file, but doesn't spit any dialogue out.

--Variable.
gbHasWorkAssignment = false

--Variables.
local iTalkedToSophie         = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
local iReceivedFunction       = VM_GetVar("Root/Variables/Chapter5/Scenes/iReceivedFunction", "N")
local iMet55InLowerRegulus    = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N")
local iSawSpecialAnnouncement = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawSpecialAnnouncement", "N")
local iMet55InBasement        = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InBasement", "N")

-- |[Scenario Stuff]|
--Hasn't talked to Sophie yet:
if(iTalkedToSophie == 0.0) then
	gbHasWorkAssignment = false

--Has talked to Sophie, hasn't received a function yet.
elseif(iTalkedToSophie == 1.0 and iReceivedFunction == 0.0) then
	gbHasWorkAssignment = true

--Hasn't met up with 55 yet, no special assignments ever appear.
elseif(iMet55InLowerRegulus == 0.0) then

	--If the special assignment to meet 55 in the basement of maintenance is on:
	if(iSawSpecialAnnouncement == 1.0 and iMet55InBasement == 0.0) then
		gbHasWorkAssignment = true

	--Repeat text.
	elseif(iSawSpecialAnnouncement == 2.0 and iMet55InBasement == 0.0) then
		gbHasWorkAssignment = false

	--Normal.
	else
		gbHasWorkAssignment = false
	end

-- |[Special Assignments]|
--If any one special assignment hasn't been taken but is qualified for, return true.
elseif(iTalkedToSophie == 1.0 and iReceivedFunction == 1.0 and iMet55InLowerRegulus == 1.0) then

	--Variables.
    local i198WorkOrder           = VM_GetVar("Root/Variables/Chapter5/Scenes/i198WorkOrder", "N")
    local iSaw198Intro            = VM_GetVar("Root/Variables/Chapter5/Scenes/iSaw198Intro", "N")
	local iSerenityWorkOrder      = VM_GetVar("Root/Variables/Chapter5/Scenes/iSerenityWorkOrder", "N")
	local iTookCassandraQuest     = VM_GetVar("Root/Variables/Chapter5/Scenes/iTookCassandraQuest", "N")
	local iResolvedCassandraQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iResolvedCassandraQuest", "N")
    local iManuTookJob            = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuTookJob", "N")
    local iManuFinishedJob        = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuFinishedJob", "N")
    local i254WorkOrder           = VM_GetVar("Root/Variables/Chapter5/Scenes/i254WorkOrder", "N")
    local iSaw55sMemories         = VM_GetVar("Root/Variables/Chapter5/Scenes/iSaw55sMemories", "N")
    local iRuneUpgradeTaken       = VM_GetVar("Root/Variables/Chapter5/Scenes/iRuneUpgradeTaken", "N")
	
	--Serenity quest is available:
	if(iSerenityWorkOrder == 0.0) then
		gbHasWorkAssignment = true
		return
	end
	
	--Electrosprite quest is available:
	if(i198WorkOrder == 0.0 and iSaw198Intro == 0.0) then
		gbHasWorkAssignment = true
		return
	end
	
	--Cassandra quest is available.
	if(iTookCassandraQuest == 0.0) then
		gbHasWorkAssignment = true
		return
	end
	
	--Cassandra quest can be turned in.
	if(iTookCassandraQuest == 1.0 and (iResolvedCassandraQuest == 1.0 or iResolvedCassandraQuest == 2.0)) then
		gbHasWorkAssignment = true
		return
	end
    
    --Manufactory job can be taken.
    if(iManuTookJob == 0.0) then
        gbHasWorkAssignment = true
        return
    end
    
    --Secrebot quest.
    if(i254WorkOrder == 0.0 and iSaw55sMemories == 1.0) then
        gbHasWorkAssignment = true
        return
    end

    --Runestone upgrade quest.
    if(iRuneUpgradeTaken == 0.0 and iSaw55sMemories == 1.0) then
        gbHasWorkAssignment = true
        return
    end
end