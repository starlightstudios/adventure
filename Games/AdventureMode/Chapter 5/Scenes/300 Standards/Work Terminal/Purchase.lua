-- |[Purchase]|
--Buying stuff at a work terminal.
local sTopic = LM_GetScriptArgument(0)

-- |[Common]|
WD_SetProperty("Hide")

-- |[Credits Chips]|
if(sTopic == "CreditsChips") then

	--Get how many items we have.
	local iChipsTotal = AdInv_GetProperty("Item Count", "Credits Chip")
	local iCreditsGained = (iChipsTotal * 30)
	for i = 1, iChipsTotal, 1 do
		AdInv_SetProperty("Remove Item", "Credits Chip")
	end
    
    --Add work credits.
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
    VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + iCreditsGained)

    --None turned in:
    if(iChipsTotal == 0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Console: You currently have no Credits Chips on your person.[P] Any chips you find will provide 30 Work Credits.[P] Thank you for your cooperation.") ]])
        fnCutsceneBlocker()
    else
        local sString = "WD_SetProperty(\"Append\", \"Console: Hello, Unit 771852![P] You have provided " .. iChipsTotal .. " for a total of " .. iCreditsGained .. " Work Credits.[P] Thank you for your cooperation.\")"
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene(sString)
        fnCutsceneBlocker()
    end

-- |[Buying Stuff]|
elseif(sTopic == "Purchase Items") then
    
    --Setup.
    local sShopString = "WD_SetProperty(\"Append\", \"Console:[VOICE|Alraune] Hello, Unit 771852![P] What would you like to purchase?[B][C]\")"
    local iShoppingRoll = LM_GetRandomNumber(0, 100)
    if(iShoppingRoll < 90) then
    elseif(iShoppingRoll < 95) then
        sShopString = "WD_SetProperty(\"Append\", \"Console:[VOICE|Alraune] Hello, Unit 771852![P] Today's sale is ERROR IN SALE DATABASE![P] Don't miss it![B][C]\")"
    else
        sShopString = "WD_SetProperty(\"Append\", \"Console:[VOICE|Alraune] Hello, Unit 771852![P] You're looking spiffy today![P] [INSERT COMPLIMENT].[P] WARNING::[P] SHOPPING AI FRIENDLINESS FACTOR EXCEEDING SAFE LEVELS.[B][C]\")"
    end
    
    --Credits.
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "I")
    local sCreditsString = "WD_SetProperty(\"Append\", \"Console:[VOICE|Alraune] (Current work credits: " .. iWorkCreditsTotal .. ")[BLOCK]\")"
    
    --Common.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Console: Activating shopping AI...[B][C]") ]])
    fnCutscene(sShopString)
    fnCutscene(sCreditsString)
    
    --Variables.
    local iHasCatalystTone             = VM_GetVar("Root/Variables/Global/CatalystTone/iHasCatalystTone", "N")
    local iLightBoostA                 = VM_GetVar("Root/Variables/Global/Christine/iLightBoostA", "N")
    local iLightBoostB                 = VM_GetVar("Root/Variables/Global/Christine/iLightBoostB", "N")
    local iWorkCreditsDoctorBagCharges = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsDoctorBagCharges", "N")
    local iWorkCreditsDoctorBagPotency = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsDoctorBagPotency", "N")
    
    --Disable light boost if the player somehow skipped the lantern like a maniac.
	local iHasLantern = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasLantern", "N")
    if(iHasLantern == 0.0) then
        iLightBoostA = 1.0
        iLightBoostB = 1.0
    end
    
    --Decision.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    if(iHasCatalystTone == 0.0) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Platinum Compass.exe (100 Credits)\", " .. sDecisionScript .. ", \"Compass\") ")
    end
    if(iLightBoostA == 0.0) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Pu238 Light Boost (100 Credits)\", " .. sDecisionScript .. ", \"LightBoostA\") ")
    elseif(iLightBoostA == 1.0 and iLightBoostB == 0.0) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Og299 Light Boost (200 Credits)\", " .. sDecisionScript .. ", \"LightBoostB\") ")
    end
    if(iWorkCreditsDoctorBagCharges == 0.0) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"+75 Doctor Bag Charges (300 Credits)\", " .. sDecisionScript .. ", \"DoctorBoostA\") ")
    end
    if(iWorkCreditsDoctorBagPotency == 0.0) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"+10 Doctor Bag Potency (300 Credits)\", " .. sDecisionScript .. ", \"DoctorBoostB\") ")
    end
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Silksteel Electrospear (150 Credits)\", " .. sDecisionScript .. ", \"Electrospear\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Mk VI Pulse Diffractor (150 Credits)\", " .. sDecisionScript .. ", \"PulseDiffractor\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Nanite Injection (100 Credits)\", " .. sDecisionScript .. ", \"NaniteInjection\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Regeneration Mist (250 Credits)\", " .. sDecisionScript .. ", \"RegenerationMist\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Cancel\",  " .. sDecisionScript .. ", \"Cancel\") ")
    fnCutsceneBlocker()
    
    
-- |[Platinum Compass]|
elseif(sTopic == "Compass") then

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] PLATCOMP.EXE is an upgrade for your PDU's local area scanners.[P] When a Catalyst is nearby and you enter a new area, the PDU will play a sound.[B][C]") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] Would you like to purchase PLATCOMP.EXE?[BLOCK]") ]])
    
    --Decision.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "I")
    if(iWorkCreditsTotal >= 100) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"CompassBuy\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"Purchase Items\") ")
    else
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"I can't afford it.\",  " .. sDecisionScript .. ", \"Purchase Items\") ")
    end
    fnCutsceneBlocker()

--Buy it.
elseif(sTopic == "CompassBuy") then

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] [SOUND|Menu|BuyOrSell]Thank you for your purchase, Lord Unit![P] Would you like to continue shopping?[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Purchase Items\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"All Done\",  " .. sDecisionScript .. ", \"Cancel\") ")
    fnCutsceneBlocker()
    
    --Flag.
    VM_SetVar("Root/Variables/Global/CatalystTone/iHasCatalystTone", "N", 1.0)

    --Remove credits.
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
    VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal - 100)

-- |[Light Radius Booster]|
elseif(sTopic == "LightBoostA") then

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] With our special injection of Pu238 into your portable light's power core, we can improve your visible light radius by 50 percent![B][C]") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] Would you like to purchase Pu238 Light Boost?[BLOCK]") ]])
    
    --Decision.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
    if(iWorkCreditsTotal >= 100) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"LightBoostABuy\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"Purchase Items\") ")
    else
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"I can't afford it.\",  " .. sDecisionScript .. ", \"Purchase Items\") ")
    end
    fnCutsceneBlocker()

--Buy it.
elseif(sTopic == "LightBoostABuy") then

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] [SOUND|Menu|BuyOrSell]Thank you for your purchase, Lord Unit![P] Would you like to continue shopping?[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Purchase Items\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"All Done\",  " .. sDecisionScript .. ", \"Cancel\") ")
    fnCutsceneBlocker()

    --Flag.
    VM_SetVar("Root/Variables/Global/Christine/iLightBoostA", "N", 1.0)

    --Remove credits.
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
    VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal - 100)

-- |[Light Radius Booster]|
elseif(sTopic == "LightBoostB") then

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] One of the heaviest elements before non-baryonic properties begin to occur, Og299 will increase your light's power output by a further 50 percent![B][C]") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] (Please note::[P] In the event of a meltdown, contact a cleanup crew AFTER moving two kilometers away from the exploding power core.[P] Thank you!)[B][C]") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] Would you like to purchase Og299 Light Boost?[BLOCK]") ]])
    
    --Decision.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
    if(iWorkCreditsTotal >= 200) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"LightBoostBBuy\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"Purchase Items\") ")
    else
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"I can't afford it.\",  " .. sDecisionScript .. ", \"Purchase Items\") ")
    end
    fnCutsceneBlocker()

--Buy it.
elseif(sTopic == "LightBoostBBuy") then

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] [SOUND|Menu|BuyOrSell]Thank you for your purchase, Lord Unit![P] Would you like to continue shopping?[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Purchase Items\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"All Done\",  " .. sDecisionScript .. ", \"Cancel\") ")
    fnCutsceneBlocker()

    --Flag.
    VM_SetVar("Root/Variables/Global/Christine/iLightBoostB", "N", 1.0)

    --Remove credits.
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
    VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal - 200)

-- |[Doctor Bag Charges]|
elseif(sTopic == "DoctorBoostA") then

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] Produced by a special contract with Hirudinean Pharmaceuticals, the conventional 'Doctor Bag' is effective on both flesh and machine alike.[B][C]") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] While the limited medical research of Regulus City has not improved on their methods, it has improved on their capacity.[P] This upgrade can increase the maximum charges of your Doctor Bag by 75.[B][C]") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] What's more, the capacity boost will persist between chapters![P] Whatever that means, this AI is not sure what the technical document was referring to.[B][C]") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] Would you like to purchase Doctor Bag Capacity Boost?[BLOCK]") ]])
    
    --Decision.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
    if(iWorkCreditsTotal >= 300) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"DoctorBoostABuy\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"Purchase Items\") ")
    else
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"I can't afford it.\",  " .. sDecisionScript .. ", \"Purchase Items\") ")
    end
    fnCutsceneBlocker()

--Buy it.
elseif(sTopic == "DoctorBoostABuy") then

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] [SOUND|Menu|BuyOrSell]Thank you for your purchase, Lord Unit![P] Would you like to continue shopping?[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Purchase Items\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"All Done\",  " .. sDecisionScript .. ", \"Cancel\") ")
    fnCutsceneBlocker()

    --Flag.
    VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsDoctorBagCharges", "N", 1.0)

    --Remove credits.
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
    VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal - 300)
    
    --Modify. The algorithm will handle the upgrade. It will also refill the current charges.
    gbAutoSetDoctorBagCurrentValues = true
    LM_ExecuteScript(gsComputeDoctorBagTotalPath)
    
-- |[Doctor Bag Potency]|
elseif(sTopic == "DoctorBoostB") then

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] Premium quality medical supplies, provided by Hirudinean Pharmaceuticals, can improve the potency of your 'Doctor Bag' product line items.[B][C]") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] These will increase the amount of HP restored for every point of power the Doctor Bag has.[P] Do more, with the same amount![B][C]") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] What's more, the capacity boost will persist between chapters![P] Whatever that means, this AI is not sure what the technical document was referring to.[B][C]") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] Would you like to purchase Doctor Bag Potency Boost?[BLOCK]") ]])
    
    --Decision.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
    if(iWorkCreditsTotal >= 300) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"DoctorBoostBBuy\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"Purchase Items\") ")
    else
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"I can't afford it.\",  " .. sDecisionScript .. ", \"Purchase Items\") ")
    end
    fnCutsceneBlocker()

--Buy it.
elseif(sTopic == "DoctorBoostBBuy") then

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] [SOUND|Menu|BuyOrSell]Thank you for your purchase, Lord Unit![P] Would you like to continue shopping?[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Purchase Items\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"All Done\",  " .. sDecisionScript .. ", \"Cancel\") ")
    fnCutsceneBlocker()

    --Flag.
    VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsDoctorBagPotency", "N", 1.0)

    --Remove credits.
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
    VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal - 300)
    
    --Modify. The algorithm will handle the upgrade. It will also refill the current charges.
    gbAutoSetDoctorBagCurrentValues = true
    LM_ExecuteScript(gsComputeDoctorBagTotalPath)

-- |[Silksteel Electrospear]|
elseif(sTopic == "Electrospear") then

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] As your profile indicates a preference for the Electrospear line of weaponry, the Silksteel Electrospear is available for purchase.[B][C]") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] This Electrospear's base attack power is 50 and comes with 2 gem upgrade slots.[B][C]") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] Would you like to purchase the Silksteel Electrospear?[BLOCK]") ]])
    
    --Decision.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
    if(iWorkCreditsTotal >= 150) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"ElectrospearBuy\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"Purchase Items\") ")
    else
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"I can't afford it.\",  " .. sDecisionScript .. ", \"Purchase Items\") ")
    end
    fnCutsceneBlocker()

--Buy it.
elseif(sTopic == "ElectrospearBuy") then

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] [SOUND|Menu|BuyOrSell]Thank you for your purchase, Lord Unit![P] Would you like to continue shopping?[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Purchase Items\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"All Done\",  " .. sDecisionScript .. ", \"Cancel\") ")
    fnCutsceneBlocker()
    
    --Flag.
    LM_ExecuteScript(gsItemListing, "Silksteel Electrospear")

    --Remove credits.
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
    VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal - 150)
    
-- |[Mk VI Pulse Diffractor]|
elseif(sTopic == "PulseDiffractor") then

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] As a registered combat unit, you have been given the opportunity to provide testing feedback on the prototype MK VI Pulse Diffractor.[B][C]") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] ATP +70, comes with two customizable gem slots. Be wary as the radiation shielding arrays are not currently optimized.[B][C]") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] Would you like to purchase the Mk VI Pulse Diffractor?[BLOCK]") ]])
    
    --Decision.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
    if(iWorkCreditsTotal >= 150) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"PulseDiffractorBuy\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"Purchase Items\") ")
    else
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"I can't afford it.\",  " .. sDecisionScript .. ", \"Purchase Items\") ")
    end
    fnCutsceneBlocker()

--Buy it.
elseif(sTopic == "PulseDiffractorBuy") then

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] [SOUND|Menu|BuyOrSell]Thank you for your purchase, Lord Unit![P] Would you like to continue shopping?[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Purchase Items\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"All Done\",  " .. sDecisionScript .. ", \"Cancel\") ")
    fnCutsceneBlocker()
    
    --Flag.
    LM_ExecuteScript(gsItemListing, "Mk VI Pulse Diffractor")

    --Remove credits.
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
    VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal - 150)

-- |[Nanite Injection]|
elseif(sTopic == "NaniteInjection") then

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] Field repairs for combat units are authorized for your designation.[P] Nanite Injectors are available for requisition.[B][C]") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] The single-use injector restores 35HP and increases your attack power by 20 percent for one round.[B][C]") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] Would you like to purchase a Nanite Injection for 100 credits?[BLOCK]") ]])
    
    --Decision.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
    if(iWorkCreditsTotal >= 100) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"NaniteInjectionBuy\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"Purchase Items\") ")
    else
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"I can't afford it.\",  " .. sDecisionScript .. ", \"Purchase Items\") ")
    end
    fnCutsceneBlocker()

--Buy it.
elseif(sTopic == "NaniteInjectionBuy") then

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] [SOUND|Menu|BuyOrSell]Thank you for your purchase, Lord Unit![P] Would you like to continue shopping?[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Purchase Items\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"All Done\",  " .. sDecisionScript .. ", \"Cancel\") ")
    fnCutsceneBlocker()
    
    --Flag.
    LM_ExecuteScript(gsItemListing, "Nanite Injection")

    --Remove credits.
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
    VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal - 100)

-- |[Regeneration Mist]|
elseif(sTopic == "RegenerationMist") then

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] Field repairs for combat units are authorized for your designation.[P] Nanite Aerosol Dispensers may be requisitioned.[B][C]") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] This single-use aerosol dispenser launches a 'mist' of repair nanites that will restore 80HP to you and your allies.[P] It even works in a vacuum![B][C]") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] Would you like to purchase a Regeneration Mist for 250 work credits?[BLOCK]") ]])
    
    --Decision.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
    if(iWorkCreditsTotal >= 250) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"RegenerationMistBuy\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"Purchase Items\") ")
    else
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"I can't afford it.\",  " .. sDecisionScript .. ", \"Purchase Items\") ")
    end
    fnCutsceneBlocker()

--Buy it.
elseif(sTopic == "RegenerationMistBuy") then

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Console:[VOICE|Alraune] [SOUND|Menu|BuyOrSell]Thank you for your purchase, Lord Unit![P] Would you like to continue shopping?[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Purchase Items\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"All Done\",  " .. sDecisionScript .. ", \"Cancel\") ")
    fnCutsceneBlocker()
    
    --Flag.
    LM_ExecuteScript(gsItemListing, "Regeneration Mist")

    --Remove credits.
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
    VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal - 250)


-- |[Cancel]|
elseif(sTopic == "Cancel") then
end
