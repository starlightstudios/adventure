-- |[ ================================= Work Terminal Response ================================= ]|
--Fired after the user selects an option on the work terminal.
local sTopic = LM_GetScriptArgument(0)

-- |[Common]|
WD_SetProperty("Hide")

-- |[ ==================================== Serenity Crater ===================================== ]|
if(sTopic == "Serenity Crater") then
	
	--Variables.
	local iSerenityWorkOrder = VM_GetVar("Root/Variables/Chapter5/Scenes/iSerenityWorkOrder", "N")
	local iCompletedSerenity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
	
	--Dialogue.
	if(iSerenityWorkOrder == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Console: Your special skillset has been requested at the Serenity Crater Observatory.[B][C]") ]])
		fnCutscene([[ Append("Console: Command Unit 300910 has personally requested your presence at the observatory due to repair requirements in a hostile environment.[P] Please check your weaponry for defects before departing.[B][C]") ]])
		fnCutscene([[ Append("Console: Additional details are to be provided upon arrival at the observatory.[P] It may be reached by taking the tram to the eastern junction, then travelling south to the crater.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] (Unit 300910?[P] I'm not familiar with the designation, but I guess she saw the skills 55 said I have and figured I'd be a good pick.)[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Lord Unit 771852 will carry out her function.[B][C]") ]])
		fnCutscene([[ Append("Console: The Cause of Science will be served.[P] Logging you off.") ]])
		fnCutsceneBlocker()

		--Flags.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSerenityWorkOrder", "N", 1.0)
	
	--Reminder.
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Console: Work Order:: Serenity Crater Observatory Assignment.[P] Your skills are required at Serenity Crater Observatory by special request of Command Unit 300910.[B][C]") ]])
		fnCutscene([[ Append("Console: Take the tram to the eastern junction, then travel east and south to Serenity Crater Observatory.[P] Unit 300910 will brief you upon arrival.[B][C]") ]])
		fnCutscene([[ Append("Console: Confirm function assignment.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Lord Unit 771852 will carry out her function.[B][C]") ]])
		fnCutscene([[ Append("Console: The Cause of Science will be served.[P] Logging you off.") ]])
		fnCutsceneBlocker()
	
	end

-- |[ ================================== Sector 15: Cassandra ================================== ]|
elseif(sTopic == "Sector 15") then

	--Variables
	local iTookCassandraQuest     = VM_GetVar("Root/Variables/Chapter5/Scenes/iTookCassandraQuest", "N")
	local iResolvedCassandraQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iResolvedCassandraQuest", "N")
	
	--Briefing:
	if(iTookCassandraQuest == 0.0 and iResolvedCassandraQuest == 0.0) then
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Console: Your special skillset has been requested in Sector 15.[B][C]") ]])
		fnCutscene([[ Append("Console: An unidentified individual has been consuming organic rations without clearance, and accessing the network illicitly.[B][C]") ]])
		fnCutscene([[ Append("Console: The individual may or may not be a unit assigned to Sector 15.[P] Caution is recommended.[B][C]") ]])
		fnCutscene([[ Append("Console: Your function is to identify and, if possible, apprehend the individual responsible.[P] Report to a work terminal when the function is completed.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] (Hmmm...[P] I wonder if a human got out and is on the loose?)[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Lord Unit 771852 will carry out her function.[B][C]") ]])
		fnCutscene([[ Append("Console: The Cause of Science will be served.[P] Logging you off.") ]])
		fnCutsceneBlocker()

		--Flags.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iTookCassandraQuest", "N", 1.0)
	
	--Reminder:
	elseif(iTookCassandraQuest == 1.0 and iResolvedCassandraQuest == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Console: Work Order:: Sector 15 Assignment.[P] You are currently assigned to locate and apprehend an individual causing trouble in the lower parts of Sector 15.[B][C]") ]])
		fnCutscene([[ Append("Console: Take the tram to Sector 15 and use the elevators there to access the lower floors.[B][C]") ]])
		fnCutscene([[ Append("Console: Confirm function assignment.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Lord Unit 771852 will carry out her function.[B][C]") ]])
		fnCutscene([[ Append("Console: The Cause of Science will be served.[P] Logging you off.") ]])
		fnCutsceneBlocker()
	
	--Cassandra quest ended with Cassandra being converted.
	elseif(iTookCassandraQuest == 1.0 and iResolvedCassandraQuest == 1.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iResolvedCassandraQuest", "N", 3.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Console: You are currently assigned to locate and apprehend an individual causing trouble in the lower parts of Sector 15.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Lord Unit 771852 reports function assignment is completed.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] A rogue human was located in the lower floors of Sector 15.[P] The rogue human was captured and implanted with a golem core.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Unit 771853 will serve the Cause of Science.[B][C]") ]])
		fnCutscene([[ Append("Console: Affirmative Unit 771852.[P] 100 Work credits have been added to your account.[P] Logging you off.") ]])
		fnCutsceneBlocker()
        
        --Provide Work Credits
        local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
        VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + 100)

	--Cassandra quest resolved with Cassandra escaping.
	elseif(iTookCassandraQuest == 1.0 and iResolvedCassandraQuest == 2.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iResolvedCassandraQuest", "N", 4.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Console: You are currently assigned to locate and apprehend an individual causing trouble in the lower parts of Sector 15.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Lord Unit 771852 reports function assignment is completed.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] A rogue security robot was stealing items at random and accessing network terminals without authorization.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] The security unit was retired and sent for scrap.[P] It will not be causing any more trouble.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] (Yeah, they'll believe that...)[B][C]") ]])
		fnCutscene([[ Append("Console: Affirmative Unit 771852.[P] 100 Work credits have been added to your account.[P] Logging you off.") ]])
		fnCutsceneBlocker()
        
        --Provide Work Credits
        local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
        VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + 100)
	end

-- |[ ==================================== Tellurium Mines ===================================== ]|
elseif(sTopic == "Tellurium Mines") then
	
	--Variables.
	local iTelluriumMinesQuest     = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasMinesWorkOrder", "N")
	local iCompletedMinesWorkOrder = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedMinesWorkOrder", "N")
	
	--Briefing:
	if(iTelluriumMinesQuest == 0.0) then
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Console: A Lord Unit has placed a special request for your skillset in Mining Sector 73.[B][C]") ]])
		fnCutscene([[ Append("Console: Recent security reports indicate numerous incursions in the mineshafts and unfillable repair orders.[P] Your task is to repair the crucial access infrastructure and deal with any security issues that arise.[B][C]") ]])
		fnCutscene([[ Append("Console: Further briefing will be provided upon arrival at the Tellurium Mines.[B][C]") ]])
		fnCutscene([[ Append("Console: Confirm function assignment.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Lord Unit 771852 will carry out her function.[B][C]") ]])
		fnCutscene([[ Append("Console: The Cause of Science will be served.[P] Logging you off.") ]])
		fnCutsceneBlocker()

		--Flags.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iHasMinesWorkOrder", "N", 1.0)
		
	--Reminder:
	else
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Console: Work Order:: Sector 73 Assignment.[P] The Tellurium Mines in Sector 73 has requested your assistance.[B][C]") ]])
		fnCutscene([[ Append("Console: Recent security reports indicate numerous incursions in the mineshafts and unfillable repair orders.[P] Your task is to repair the crucial access infrastructure and deal with any security issues that arise.[B][C]") ]])
		fnCutscene([[ Append("Console: Take the tram to Sector 73 and report to the Lord Unit there for further instructions.[B][C]") ]])
		fnCutscene([[ Append("Console: Confirm function assignment.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Lord Unit 771852 will carry out her function.[B][C]") ]])
		fnCutscene([[ Append("Console: The Cause of Science will be served.[P] Logging you off.") ]])
		fnCutsceneBlocker()
	
	end

-- |[ ============================== Electrosprite in Sector 198 =============================== ]|
elseif(sTopic == "Sector 198") then

    --Variables.
    local i198WorkOrder = VM_GetVar("Root/Variables/Chapter5/Scenes/i198WorkOrder", "N")
    local iFinished198  = VM_GetVar("Root/Variables/Chapter5/Scenes/iFinished198", "N")
	
	--Briefing:
	if(i198WorkOrder == 0.0) then
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Console: Your repair skills have been requested in Sector 198.[B][C]") ]])
		fnCutscene([[ Append("Console: A note has been placed on the work order 'Repair oxygen leak.[P] Please see Unit 745110 for special instructions.'[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Uh, why can't Sector 198's local repair units handle it?[P] Fixing an oxygen leak shouldn't be that hard.[B][C]") ]])
		fnCutscene([[ Append("Console: Your unit designation was requested specifically.[P] No reason was provided other than the special instruction request.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Hmm...[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Oh, isn't Sector 198 where the Abductions department trains its teams?[B][C]") ]])
		fnCutscene([[ Append("Console: Correct.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] I'm on the way![B][C]") ]])
		fnCutscene([[ Append("Console: Confirm function assignment.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Lord Unit 771852 will carry out her function.[B][C]") ]])
		fnCutscene([[ Append("Console: The Cause of Science will be served.[P] Logging you off.") ]])
		fnCutsceneBlocker()

		--Flags.
        VM_SetVar("Root/Variables/Chapter5/Scenes/i198WorkOrder", "N", 1.0)
		
	--Reminder:
	else
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Console: Work Order:: Sector 198 Assignment.[P] The Abductions training department has requested your special skills.[B][C]") ]])
		fnCutscene([[ Append("Console: Please contact Unit 745110 for special details.[P] She is current assigned to Sector 198.[B][C]") ]])
		fnCutscene([[ Append("Console: Take the tram to Sector 198 and report to Unit 745110.[P] Confirm function assignment.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Lord Unit 771852 will carry out her function.[B][C]") ]])
		fnCutscene([[ Append("Console: The Cause of Science will be served.[P] Logging you off.") ]])
		fnCutsceneBlocker()
	
    end

-- |[ ================================== Secrebot Quest Start ================================== ]|
elseif(sTopic == "RepairBay") then

    --Variables.
    local i254WorkOrder = VM_GetVar("Root/Variables/Chapter5/Scenes/i254WorkOrder", "N")
    
    --Briefing:
    if(i254WorkOrder == 0.0) then
        
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Console: A request from Unit 499323 has been placed on your account.[P] The request originates from Maintenance and Repair, Sector 96.[B][C]") ]])
		fnCutscene([[ Append("Console: Confirm function assignment.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Lord Unit 771852 will carry out her function.[B][C]") ]])
		fnCutscene([[ Append("Console: The Cause of Science will be served.[P] Logging you off.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] (Sophie is asking for me via the work terminal?[P] I wonder what's going on.[P] Better go see her.)") ]])
		fnCutsceneBlocker()
        
        --Flags.
        VM_SetVar("Root/Variables/Chapter5/Scenes/i254WorkOrder", "N", 1.0)
        
    --Reminder:
    else
        
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Console: A request from Unit 499323 has been placed on your account.[P] The request originates from Maintenance and Repair, Sector 96.[B][C]") ]])
		fnCutscene([[ Append("Console: Confirm function assignment.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Lord Unit 771852 will carry out her function.[P] Is there any additional data on the request?[B][C]") ]])
		fnCutscene([[ Append("Console: Additional instruction.[P] 'It's been a long day and I'm at my wit's end. I might go insane, please help.'[P] The Cause of Science will be served.[P] Logging you off.") ]])
		fnCutsceneBlocker()
    
    end

-- |[ ================================ Runestone Upgrade Quest ================================= ]|
elseif(sTopic == "Undercity") then

    --Variables.
    local iRuneUpgradeTaken = VM_GetVar("Root/Variables/Chapter5/Scenes/iRuneUpgradeTaken", "N")
    
    --Briefing:
    if(iRuneUpgradeTaken == 0.0) then
        
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Append", "Console: A repair order for a network terminal on the first basement floor of Sector 96 is faulty.[P] Investigate.[B][C]") ]])
		fnCutscene([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Lord Unit 771852 will carry out her function.[B][C]") ]])
		fnCutscene([[ WD_SetProperty("Append", "Console: The Cause of Science will be served.[P] Logging you off.[B][C]") ]])
		fnCutscene([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] (That's the console 55 and I met at...[P] Yeah, this order was placed by her.[P] She must want me to see something.)") ]])
		fnCutsceneBlocker()
        
        --Flags.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iRuneUpgradeTaken", "N", 1.0)
        
    --Reminder:
    else
        
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Append", "Console: A repair order for a network terminal on the first basement floor of Sector 96 is faulty.[P] Investigate.[B][C]") ]])
		fnCutscene([[ WD_SetProperty("Append", "Christine:[VOICE|Christine] Lord Unit 771852 will carry out her function.[P] Any further information I should be aware of?[B][C]") ]])
		fnCutscene([[ WD_SetProperty("Append", "Console: The order was placed by -[P] Unit 499323, Maintenance and Repair, Sector 96.[B][C]") ]])
		fnCutscene([[ WD_SetProperty("Append", "Console: No additional data available.[P] Please proceed to the first basement floor of Sector 96.[P] The Cause of Science will be served.[P] Logging you off.") ]])
		fnCutsceneBlocker()
    
    end

-- |[ =================================== Efficiency Expert ==================================== ]|
elseif(sTopic == "Efficiency") then

    --Variables.
    local iManuTookJob = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuTookJob", "N")
    
    --Briefing:
    if(iManuTookJob == 0.0) then
        
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Console: Your social skills have been requested by Efficiency Expert Unit 600484 in Manufacturing Sector 99.[B][C]") ]])
		fnCutscene([[ Append("Console: Please contact Unit 600484 for further details.[P] The tram to Sector 99 can be taken at the tram station as usual.[P] Confirm function assignment.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Lord Unit 771852 will carry out her function.[B][C]") ]])
		fnCutscene([[ Append("Console: The Cause of Science will be served.[P] Logging you off.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] (...[P] Social skills?[P] That's a new one.)") ]])
		fnCutsceneBlocker()
        
        --Flags.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iManuTookJob", "N", 1.0)
        
    --Reminder:
    else
        
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Console: Your social skills have been requested by Efficiency Expert Unit 600484 in Manufacturing Sector 99.[B][C]") ]])
		fnCutscene([[ Append("Console: Please contact Unit 600484 for further details.[P] The tram to Sector 99 can be taken at the tram station as usual.[P] Confirm function assignment.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Lord Unit 771852 will carry out her function.[P] Is there any reason why my social skills were required?[B][C]") ]])
		fnCutscene([[ Append("Console: No additional data available.[P] Please contact Unit 600484.[P] The Cause of Science will be served.[P] Logging you off.") ]])
		fnCutsceneBlocker()
    
    end

-- |[ ========================================= Cancel ========================================= ]|
elseif(sTopic == "Cancel") then
end

--If we're at the Raiju Ranch, never execute past this point.
local iSawRaijuIntro = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawRaijuIntro", "N")
if(iSawRaijuIntro == 1.0) then return end

--Common code, always executes.
LM_ExecuteScript(fnResolvePath() .. "QueryFunction.lua")

--A work assignment is available, so show green work terminals.
if(gbHasWorkAssignment == true) then
	AL_SetProperty("Set Layer Disabled", "WorkTerminalToggleA", true)
	AL_SetProperty("Set Layer Disabled", "WorkTerminalToggleB", true)
else
	AL_SetProperty("Set Layer Disabled", "WorkTerminalToggleA", false)
	AL_SetProperty("Set Layer Disabled", "WorkTerminalToggleB", false)
end