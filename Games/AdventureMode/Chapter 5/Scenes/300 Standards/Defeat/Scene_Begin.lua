-- |[ ============================ Defeat By Anything, Back to Save ============================ ]|
--Cutscene that executes when the player is beaten by something that has no special cutscene attached. 
-- Alternately, it will also fire when another defeat cutscene has already been seen, and won't play again.
--This part of the script just sends the party back to the last save point.

-- |[Clear Music]|
--Music is nulled twice to prevent fadeout issues.
AL_SetProperty("Music", "Null")
AudioManager_PlayMusic("Null")
AudioManager_PlayMusic("Null")

-- |[Reversion]|
--Execute reversion handlers.
LM_ExecuteScript(gsStandardRevert)

--Lock to blackout.
fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])

--Drop any other events.
--Cutscene_CreateEvent("DROPALLEVENTS")

-- |[Post-Execution]|
--If we are currently in the last-saved room, reposition the party.
local sString = ""
local sExecPath = fnResolvePath() .. "Scene_PostTransition.lua"
if(AL_GetProperty("Last Save") == AL_GetProperty("Name") and false) then
    sString = [[ AL_BeginTransitionTo("LASTSAVEINSTANT", "]] .. sExecPath .. [[")]]
	
--Otherwise, transition to that room.
else
    sString = [[ AL_BeginTransitionTo("LASTSAVE", "]] .. sExecPath .. [[")]]
end

--Execute.
fnCutscene(sString)

-- |[Combat]|
--Restore party to full HP in case the player doesn't want to use the save point. This saves time, and is a quality-of-life feature.
AdvCombat_SetProperty("Restore Party")