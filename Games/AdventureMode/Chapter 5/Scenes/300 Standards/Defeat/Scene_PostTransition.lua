-- |[ ================================== Scene Post-Transition ================================= ]|
--After transition, play a random dialogue line.

-- |[ =========================== Setup ========================== ]|
-- |[Variables]|
local iIsPostGolemScene  = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsPostGolemScene", "N")
local iChristineHasGolem = VM_GetVar("Root/Variables/Global/Christine/iHasGolemForm", "N")
local iReceivedFunction  = VM_GetVar("Root/Variables/Chapter5/Scenes/iReceivedFunction", "N")

-- |[Overlay]|
--Set the overlay to fullblack. Fade in slowly.
fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Over_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
fnCutsceneWait(120)
fnCutsceneBlocker()

-- |[Position]|
--Call function to automatically position characters around the save point. This also sets everyone to "Wounded" frames.
SceneReversion:fnSavePointRearrange()

-- |[ ========================= Fade In ========================== ]|
-- |[Fade In]|
fnCutscene([[ AL_SetProperty("Activate Fade", 180, gci_Fade_Over_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])

--Wait a bit.
fnCutsceneWait(240)
fnCutsceneBlocker()

-- |[Stand Up, Face Leader]|
--Handled by a subroutine, party stands up and rotates to face the leader.
SceneReversion:fnPartyStandUp()

--Re-transform. Subroutine makes party members return to their starting forms if they reverted.
SceneReversion:fnRetransform()

-- |[ ========================= Dialogue ========================= ]|
-- |[Chris]|
--If we're still in the "Chris" phase, use this:
if(iIsPostGolemScene == 0.0) then
    
	--Setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])

	--Roll.
	local iDialogueRoll = LM_GetRandomNumber(0, 8)

	--Dialogue handlers.
	if(iDialogueRoll == 0) then
		fnCutscene([[ Append("Chris:[E|Neutral] ...[P] I hate this place.") ]])
    else
		fnCutscene([[ Append("Chris:[E|Neutral] ...[P] Why does it have to be this way?") ]])
    end

-- |[Christine, Reprogrammed]|
elseif(iReceivedFunction == 0.0) then
    
	--Setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Serious") ]])
    fnCutscene([[ Append("Christine:[E|Serious] Internal evaluation indicates core systems unharmed.[P] Unidentified organic disturbance detected, scans indicate no further organic material in chassis.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Serious] Likely cause is improper instrument calibration.[P] Report to Regulus City for function assignment and inspection, priority zero.") ]])

-- |[Christine, Alone]|
--Christine talks to herself.
elseif(giFollowersTotal == 0) then
	
	--Setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Sad") ]])

	--Roll.
	local iDialogueRoll = LM_GetRandomNumber(0, 2)

	--Dialogue handlers.
	if(iDialogueRoll == 0) then
		fnCutscene([[ Append("Christine:[E|Sad] ...[P] If I were a doctor, my medical advice would be to not do that.") ]])
	elseif(iDialogueRoll == 1) then
		fnCutscene([[ Append("Christine:[E|Sad] ...[P] Bunch of bullies...") ]])
	elseif(iDialogueRoll == 2) then
		fnCutscene([[ Append("Christine:[E|Sad] ...[P] This is what I get for walking alone at night...") ]])
	end

-- |[Christine, Tiffany]|
elseif(giFollowersTotal == 1 and gsaFollowerNames[1] == "Tiffany") then
	
	--Setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Sad") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])

	--Roll.
	local iDialogueRoll = LM_GetRandomNumber(0, 2)

	--Dialogue handlers.
	if(iDialogueRoll == 0) then
		fnCutscene([[ Append("Christine:[E|Sad] You all right?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] All systems report green, somehow.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Okay, let's go.") ]])
        
	elseif(iDialogueRoll == 1) then
		fnCutscene([[ Append("Christine:[E|Sad] ...[P] Ugh...[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Our tactics have failed us.[P] We must improve.") ]])
        
	elseif(iDialogueRoll == 2) then
		fnCutscene([[ Append("55:[E|Offended] 771852, are you operational?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] Yeah, I'll be fine.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Ah, good.[P] Let us continue.") ]])
	end

-- |[Christine, JX-101]|
elseif(giFollowersTotal == 1 and gsaFollowerNames[1] == "JX-101") then
	
	--Setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Sad") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])

	--Dialogue handlers.
	if(iDialogueRoll == 0) then
		fnCutscene([[ Append("Christine:[E|Sad] Oww...[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] We're not dead?[P] I thought that was it.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] We have a saying where I'm from.[P] 'Don't look a gift horse in the mouth'.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Okay maybe I borrowed that from the yanks, but the point still stands.[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] And what point is that, exactly?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] When something good happens, don't question it.[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] I see.[P] A useful phrase, then.") ]])
	end

-- |[Christine, Tiffany, SX-399 / Sophie]|
--This is the only case where there are 2 followers.
elseif(giFollowersTotal == 2) then
	
    --If the second follower is SX-399:
    if(gsaFollowerNames[2] == "SX-399") then
    
        --Setup.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Sad") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Tiffany", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "SX-399", "Neutral") ]])

        --Roll.
        local iDialogueRoll = LM_GetRandomNumber(0, 2)

        --Dialogue handlers.
        if(iDialogueRoll == 0) then
            fnCutscene([[ Append("Christine:[E|Sad] Everyone in one piece?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Yipes.[P] Is getting your ears boxed part of the job?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Yes.[P] Yes it is.") ]])
            
        elseif(iDialogueRoll == 1) then
            fnCutscene([[ Append("Christine:[E|Sad] Ow.[P] Ow ow ow...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] SX-399, I have some critiques of your tactics.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Hey, we all got our stuffing beaten out, cutie.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I know.[P] I am just informing you that we will discuss out synergies as a group later.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] She didn't mean it in a bad way.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] I gotcha.[P] It's a date.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] I -[P] did not mean it that way!") ]])
            
        elseif(iDialogueRoll == 2) then
            fnCutscene([[ Append("55:[E|Upset] Get up, 771852.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Don't get short with me![B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Sad] Hey, calm down![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] Oh, I'm sorry![P] This is kind of our dynamic.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] She's just trying to keep me motivated.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] I am not.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] Oh, the old 'harsh taskmaster' routine.[P] Yeah, I got that in my training, too.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] This is not a routine.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] It's important to never break character, 55![P] Shall we continue?") ]])
        end

    --If it's Sophie:
    else
    
        --Setup.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Sad") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Tiffany", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Sophie", "Neutral") ]])

        --Roll.
        local iDialogueRoll = LM_GetRandomNumber(0, 2)

        --Dialogue handlers.
        if(iDialogueRoll == 0) then
            fnCutscene([[ Append("Sophie:[E|Sad] I dislike violence...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] You're not hurt, are you?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Blush] Yo-[P]you're thinking of me?[P] You're the one who took a heavy hit to the cranium![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Blush] Oh, it'll take a little more than a CPU failure to make me stop thinking of you...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] ...") ]])
            
        elseif(iDialogueRoll == 1) then
            fnCutscene([[ Append("Christine:[E|Sad] Sophie, are you okay?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Blush] Hee hee![P] I'm fine![P] We warped away as soon as you went down![B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] Could you maybe...[P] not do whatever you just did?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Listen to her tactical critique, 771852.[P] It is surprisingly prescient.") ]])
            
        elseif(iDialogueRoll == 2) then
            fnCutscene([[ Append("Christine:[E|Neutral] Sorry our date tonight is so lacking, Sophie.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Offended] I wish I could help...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] It is best if you don't.[P] You would not...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Offended] Want to...[P] stain...[P] your clothing?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Very smooth.") ]])
        end
    end

-- |[Christine, Tiffany, SX-399, Sophie]|
--This is the only case where there are 3 followers.
elseif(giFollowersTotal == 3) then
	
	--Setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Sad") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Sad") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Tiffany", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "SX-399", "Neutral") ]])

	--Roll.
	local iDialogueRoll = LM_GetRandomNumber(0, 2)

	--Dialogue handlers.
	if(iDialogueRoll == 0) then
		fnCutscene([[ Append("Sophie:[E|Sad] I dislike violence...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] You're not hurt, are you?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Blush] Yo-[P]you're thinking of me?[P] You're the one who took a heavy hit to the cranium![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Blush] Oh, it'll take a little more than a CPU failure to make me stop thinking of you...[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Are they always like that?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] .[P].[P].[P].[P] Yes.") ]])
        
	elseif(iDialogueRoll == 1) then
		fnCutscene([[ Append("Christine:[E|Sad] Sophie, are you okay?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Blush] Hee hee![P] I'm fine![P] We warped away as soon as you went down![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Could you maybe...[P] not do whatever you just did?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Listen to her tactical critique, 771852.[P] It is surprisingly prescient.") ]])
        
	elseif(iDialogueRoll == 2) then
		fnCutscene([[ Append("SX-399:[E|Sad] I gotta say, Christine.[P] Your choice of double-date is a tad lacking.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Only a tad?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Offended] I wish I could help...[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] It is best if you don't.[P] You would not...[B][C]") ]])
		fnCutscene([[ Append("55:[E|Offended] Want to...[P] stain...[P] your clothing?[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Flirt] A, for effort, 55.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] 'Effort' starts with an 'E'.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] *Sigh*...") ]])
	end
end
fnCutsceneBlocker()

-- |[ ============== Finish Up =============== ]|
--Characters move onto the party leader. This is handled by the subroutine.
SceneReversion:fnSavePointFold()
