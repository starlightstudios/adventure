-- |[ ===================================== Quickadd SX-399 ==================================== ]|
--Instantly adds SX-399 to the party.

-- |[Setup]|
local sFieldName = "SX-399"
local sPartyName = "SX-399"

-- |[Costume]|
VM_SetVar("Root/Variables/Costumes/SX-399/sCostumeShocktrooper", "S", "Normal")

-- |[Field Entity]|
--Create if she doesn't exist.
if(EM_Exists(sFieldName) == false) then
	fnSpecialCharacter(sFieldName, -100, -100, gci_Face_South, false, nil)
end

--Lua globals. Add to the party if she's not already in it.
local bWasInParty = false
for i = 1, giFollowersTotal, 1 do
	if(gsaFollowerNames[i] == sFieldName) then bWasInParty = true end
end

--Not in party, add.
if(bWasInParty == false) then
    local iSlot = giFollowersTotal
    giFollowersTotal = giFollowersTotal + 1
    gsaFollowerNames[iSlot+1] = sFieldName
    giaFollowerIDs = {0}

    --Get character's uniqueID. 
    EM_PushEntity(sFieldName)
        local iCharacterID = RE_GetID()
    DL_PopActiveObject()

    --Store it and tell her to follow.
    giaFollowerIDs[iSlot+1] = iCharacterID
    AL_SetProperty("Follow Actor ID", iCharacterID)
    
    --Party Folding
    AL_SetProperty("Fold Party")
end

-- |[Combat Lineup]|
--Place character in combat lineup.
local iPlacedInSlot = 0
if(AdvCombat_GetProperty("Is Member In Active Party", sPartyName) == false) then
    for i = 0, 3, 1 do
        if(AdvCombat_GetProperty("Name of Active Member", i) == "Null") then
            AdvCombat_SetProperty("Party Slot", i, sPartyName)
            iPlacedInSlot = i
            break
        end
    end
end

--Normalize her EXP with the party leader's. She will have 80% to 120% of her EXP and JP.
local iLeaderXP = 0
local iLeaderJP = 0
if(iPlacedInSlot ~= 0) then
    local sLeaderName = AdvCombat_GetProperty("Name of Active Member", 0)
    AdvCombat_SetProperty("Push Party Member", sLeaderName)
        iLeaderXP = AdvCombatEntity_GetProperty("Exp")
        iLeaderJP = AdvCombatEntity_GetProperty("Total JP")
    DL_PopActiveObject()
end

--Set.
AdvCombat_SetProperty("Push Party Member", sPartyName)

    --EXP scatter.
    local fEXPRoll = LM_GetRandomNumber(80, 120) / 100.0
    AdvCombatEntity_SetProperty("Current Exp", math.floor(iLeaderXP * fEXPRoll))
    
    --JP. No scatter.
    AdvCombatEntity_SetProperty("Current JP", math.floor(iLeaderJP))

DL_PopActiveObject()

-- |[Dialogue and Script]|
--Topic unlocks.

-- |[Costume]|
