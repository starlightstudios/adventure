-- |[ ================================= Unlock All Skillbooks ================================== ]|
--Specifies that all skillbooks are unlocked for this chapter.
local iChristineSkillbook = VM_GetVar("Root/Variables/Global/Christine/iSkillbookTotal", "N") 
if(iChristineSkillbook < 4) then
    VM_SetVar("Root/Variables/Global/Christine/iSkillbookTotal", "N", 4.0)
end

--Tiffany.
local iTiffanySkillbook = VM_GetVar("Root/Variables/Global/Tiffany/iSkillbookTotal", "N") 
if(iTiffanySkillbook < 4) then
    VM_SetVar("Root/Variables/Global/Tiffany/iSkillbookTotal", "N", 4.0)
end

--Florentina.
local iSX399Skillbook = VM_GetVar("Root/Variables/Global/SX-399/iSkillbookTotal", "N") 
if(iSX399Skillbook < 4) then
    VM_SetVar("Root/Variables/Global/SX-399/iSkillbookTotal", "N", 4.0)
end

--Debug:
io.write("Skillbooks unlocked. This will not appear until the next time you change jobs, or save and load the game.\n")