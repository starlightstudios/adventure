-- |[Plot Completion]|
--Sets the scripts such that the player is at the start of the Biolabs.

--Cryogenics variables.
VM_SetVar("Root/Variables/Chapter1/Scenes/Intro|iShowIntroScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/Intro|iUnlockAirlockDoor", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iIntercomState", "N", 8.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPDU", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPDUCryoRedCard", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPDUCryoBlueCard", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasDollMotivatorA", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasDollMotivatorB", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasDollOcular", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasLantern", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasFabricationAAuth", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawRuinedFabricator", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMet55", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSeenRedDoorWarning", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFixedCryoDoor", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iIsPostGolemScene", "N", 1.0)

--Other.
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawWelcomeToRegulus", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iReceivedFunction", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iToldSophieAboutFunction", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N", 3.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawPersonalQuarters", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawSpecialAnnouncement", "N", 3.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMet55InBasement", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSophieImportantMeeting", "N", 2.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iLeftRepairBay", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N", 1.0)

--LRT.
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawLRTOpening", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawOfficeConversation", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawSouthernCoreCutscene", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawBattleCutscene", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iCheckedSecurityConsole", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawPrisonEntryScene", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawDiaryScene", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedToWarden", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawPrisonDiscussion", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedWithRehabGolem", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawFirstEDGScene", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iLRTBossResult", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSaw55sMemories", "N", 1.0)

--Biolabs.
VM_SetVar("Root/Variables/Chapter5/Scenes/iReachedBiolabs", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeTo2856Biolabs", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSophieLeftInBiolabs", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iStarted56Sequence", "N", 1.0)

--UI.
AM_SetProperty("Set Form Icon",   "Root/Images/AdventureUI/CampfireMenuIcon/TransformChristine")
AM_SetProperty("Set Relive Icon", "Root/Images/AdventureUI/CampfireMenuIcon/ReliveChristine")

-- |[Item Name Change]|
--Call this to reset all instances of item types and descriptions from Chris to Christine.
fnSwitchItemTypesForChristine()
