-- |[Plot Completion]|
--This script immediately sets the conditions allowing Christine to exit the Cryogenics facility.
-- This will also shapeshift her to a Golem if she's not already.
VM_SetVar("Root/Variables/Chapter1/Scenes/Intro|iShowIntroScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/Intro|iUnlockAirlockDoor", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iIntercomState", "N", 8.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPDU", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPDUCryoRedCard", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPDUCryoBlueCard", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasDollMotivatorA", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasDollMotivatorB", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasDollOcular", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasLantern", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasFabricationAAuth", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawRuinedFabricator", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMet55", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSeenRedDoorWarning", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFixedCryoDoor", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iIsPostGolemScene", "N", 1.0)

--Quest block.
local iMainQuestBlock = VM_GetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N")
if(iMainQuestBlock < 10) then
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N", 10.0)
end

--Shapeshift.
LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua")
WD_SetProperty("Set Leader Voice", "Christine")

--Light.
AL_SetProperty("Activate Player Light", 3600, 3600)

--UI.
AM_SetProperty("Set Form Icon",   "Root/Images/AdventureUI/CampfireMenuIcon/TransformChristine")
AM_SetProperty("Set Relive Icon", "Root/Images/AdventureUI/CampfireMenuIcon/ReliveChristine")

-- |[Equip Change]|
--Give Christine her Lord Unit equipment.
local iGotGolemEquipment = VM_GetVar("Root/Variables/Global/Christine/iGotGolemEquipment", "N")
if(iGotGolemEquipment == 0.0) then
    
    --Repeat flag.
	VM_SetVar("Root/Variables/Global/Christine/iGotGolemEquipment", "N", 1.0)
        
    --Create new items.
    LM_ExecuteScript(gsItemListing, "Carbonweave Electrospear")
    
    local iLordGolemDressCount = AdInv_GetProperty("Item Count", "Lord Golem Dress")
    if(iLordGolemDressCount < 1) then
        LM_ExecuteScript(gsItemListing, "Lord Golem Dress")
    end
    
    --Push.
    AdvCombat_SetProperty("Push Party Member", "Christine")
    
        --Unequip weapon/body slots.
        AdvCombatEntity_SetProperty("Unequip Slot", "Weapon")
        AdvCombatEntity_SetProperty("Unequip Slot", "Weapon Backup A")
        AdvCombatEntity_SetProperty("Unequip Slot", "Weapon Backup B")
        AdvCombatEntity_SetProperty("Unequip Slot", "Body")
        
        --Remove from inventory.
        AdInv_SetProperty("Remove Item", "Tazer")
        AdInv_SetProperty("Remove Item", "Schoolmaster's Suit")
        
        --Equip new items.
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Weapon", "Carbonweave Electrospear")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Body",   "Lord Golem Dress")
        
        --Hide the "Teacher" job.
        AdvCombatEntity_SetProperty("Push Job S", "Teacher")
            AdvCombatJob_SetProperty("Appears on Skills UI", false)
        DL_PopActiveObject()
        
        --Hide the "Lancer" job.
        AdvCombatEntity_SetProperty("Push Job S", "Lancer")
            AdvCombatJob_SetProperty("Appears on Skills UI", false)
        DL_PopActiveObject()
        AdvCombatEntity_SetProperty("Rebuild Job Skills UI")
	DL_PopActiveObject()
    
    --Set Christine to the "Serious" costume. This changes her combat portrait.
    VM_SetVar("Root/Variables/Costumes/Christine/sCostumeGolem", "S", "Serious")
    LM_ExecuteScript(gsCostumeAutoresolve, "Christine_Golem")
end

-- |[Item Name Change]|
--Call this to reset all instances of item types and descriptions from Chris to Christine.
fnSwitchItemTypesForChristine()