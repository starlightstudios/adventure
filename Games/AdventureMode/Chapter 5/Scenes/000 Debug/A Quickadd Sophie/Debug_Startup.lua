-- |[ ===================================== Quickadd Sophie ==================================== ]|
--Instantly adds Sophie to the party.

-- |[Setup]|
local sFieldName = "Sophie"
local sPartyName = "Null"

-- |[Field Entity]|
--Create if she doesn't exist.
if(EM_Exists(sFieldName) == false) then
	fnSpecialCharacter(sFieldName, -100, -100, gci_Face_South, false, nil)
end

--Lua globals. Add to the party if she's not already in it.
local bWasInParty = false
for i = 1, giFollowersTotal, 1 do
	if(gsaFollowerNames[i] == sFieldName) then bWasInParty = true end
end

--Not in party, add.
if(bWasInParty == false) then
    local iSlot = giFollowersTotal
    giFollowersTotal = giFollowersTotal + 1
    gsaFollowerNames[iSlot+1] = sFieldName
    giaFollowerIDs = {0}

    --Get Sophie's uniqueID. 
    EM_PushEntity(sFieldName)
        local iCharacterID = RE_GetID()
    DL_PopActiveObject()

    --Store it and tell her to follow.
    giaFollowerIDs[iSlot+1] = iCharacterID
    AL_SetProperty("Follow Actor ID", iCharacterID)
    
    --Party Folding
    AL_SetProperty("Fold Party")
end

-- |[Dialogue and Script]|
--Unlock dialogue topics.
--WD_SetProperty("Unlock Topic", "Florentina", 1)

--Script variables that normally must be set to meet this character.
--VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N", 1.0)
