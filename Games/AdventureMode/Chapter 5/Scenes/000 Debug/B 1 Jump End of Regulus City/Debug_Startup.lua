-- |[Plot Completion]|
--This script immediately sets the conditions allowing Christine to exit the Cryogenics facility.
-- This will also shapeshift her to a Golem if she's not already.
local iMet55InLowerRegulus = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N")

--Cryogenics variables.
VM_SetVar("Root/Variables/Chapter1/Scenes/Intro|iShowIntroScene", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/Intro|iUnlockAirlockDoor", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iIntercomState", "N", 8.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPDU", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPDUCryoRedCard", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPDUCryoBlueCard", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasDollMotivatorA", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasDollMotivatorB", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasDollOcular", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasLantern", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasFabricationAAuth", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawRuinedFabricator", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMet55", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSeenRedDoorWarning", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iFixedCryoDoor", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iIsPostGolemScene", "N", 1.0)

--Other.
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawWelcomeToRegulus", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iReceivedFunction", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iToldSophieAboutFunction", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N", 3.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawPersonalQuarters", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSawSpecialAnnouncement", "N", 3.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMet55InBasement", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iSophieImportantMeeting", "N", 2.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iLeftRepairBay", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N", 1.0)

--UI.
AM_SetProperty("Set Form Icon",   "Root/Images/AdventureUI/CampfireMenuIcon/TransformChristine")
AM_SetProperty("Set Relive Icon", "Root/Images/AdventureUI/CampfireMenuIcon/ReliveChristine")

-- |[Add Tiffany]|
--Add Tiffany to the party lineup. She doesn't leave even when she hides in Regulus City.
AdvCombat_SetProperty("Party Slot", 1, "Tiffany")

--If Christine is less than level 2, Tiffany starts at level 2. This is level 3 on the UI.
AdvCombat_SetProperty("Push Party Member", "Christine")
    local iChristineLevel = AdvCombatEntity_GetProperty("Level")
    local iChristineExp = AdvCombatEntity_GetProperty("Exp")
DL_PopActiveObject()
if(iChristineLevel < 2) then
    local iNeededXP = AdvCombatEntity_GetProperty("Exp For Level", 2)
    AdvCombat_SetProperty("Push Party Member", "Tiffany")
        AdvCombatEntity_SetProperty("Current Exp", iNeededXP)
    DL_PopActiveObject()
    
--Otherwise, start at +/- 15% of Christine's XP.
else
    local fScatter = LM_GetRandomNumber(85, 115) * 0.01
    AdvCombat_SetProperty("Push Party Member", "Tiffany")
        AdvCombatEntity_SetProperty("Current Exp", math.floor(iChristineExp * fScatter))
    DL_PopActiveObject()

end

-- |[Equip Change]|
--Give Christine her Lord Unit equipment.
local iGotGolemEquipment = VM_GetVar("Root/Variables/Global/Christine/iGotGolemEquipment", "N")
if(iGotGolemEquipment == 0.0) then
    
    --Repeat flag.
	VM_SetVar("Root/Variables/Global/Christine/iGotGolemEquipment", "N", 1.0)
        
    --Create new items.
    LM_ExecuteScript(gsItemListing, "Carbonweave Electrospear")
    
    local iLordGolemDressCount = AdInv_GetProperty("Item Count", "Lord Golem Dress")
    if(iLordGolemDressCount < 1) then
        LM_ExecuteScript(gsItemListing, "Lord Golem Dress")
    end
    
    --Push.
    AdvCombat_SetProperty("Push Party Member", "Christine")
    
        --Unequip weapon/body slots.
        AdvCombatEntity_SetProperty("Unequip Slot", "Weapon")
        AdvCombatEntity_SetProperty("Unequip Slot", "Weapon Backup A")
        AdvCombatEntity_SetProperty("Unequip Slot", "Weapon Backup B")
        AdvCombatEntity_SetProperty("Unequip Slot", "Body")
        
        --Remove from inventory.
        AdInv_SetProperty("Remove Item", "Tazer")
        AdInv_SetProperty("Remove Item", "Schoolmaster's Suit")
        
        --Equip new items.
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Weapon", "Carbonweave Electrospear")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Body",   "Lord Golem Dress")
        
        --Hide the "Teacher" job.
        AdvCombatEntity_SetProperty("Push Job S", "Teacher")
            AdvCombatJob_SetProperty("Appears on Skills UI", false)
        DL_PopActiveObject()
        
        --Hide the "Lancer" job.
        AdvCombatEntity_SetProperty("Push Job S", "Lancer")
            AdvCombatJob_SetProperty("Appears on Skills UI", false)
        DL_PopActiveObject()
        AdvCombatEntity_SetProperty("Rebuild Job Skills UI")
	DL_PopActiveObject()
    
    --Set Christine to the "Serious" costume. This changes her combat portrait.
    VM_SetVar("Root/Variables/Costumes/Christine/sCostumeGolem", "S", "Serious")
    LM_ExecuteScript(gsCostumeAutoresolve, "Christine_Golem")
end

-- |[Item Name Change]|
--Call this to reset all instances of item types and descriptions from Chris to Christine.
fnSwitchItemTypesForChristine()