-- |[Plot Completion]|
--Jumps to the secrebot quest part where Christine is turned and follows Sophie.
VM_SetVar("Root/Variables/Chapter5/Scenes/i254WorkOrder", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/i254TalkedToSophie", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/i254GotAlert", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/i254SawCommandUnit", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/i254SawDrones", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/i254FreeStuff", "N", 0.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/i254NightEscapeA", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/i254NightEscapeB", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/i254NightCaptured", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/i254Transformed", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/i254SophieLeading", "N", 1.0)
AL_BeginTransitionTo("Sector254B", "FORCEPOS:1.0x1.0x0")
