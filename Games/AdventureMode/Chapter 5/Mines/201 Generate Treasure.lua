-- |[ ============================= MapHelper:fnGenerateTreasure() ============================= ]|
--Given a floor number and the name of the treasure chest in question, either populates the chest
-- with loot, or despawns it if it exceeds the treasure count for this floor.
function MapHelper:fnGenerateTreasure(piFloorNumber, psChestName)
	
	-- |[ =================== Setup ==================== ]|
	-- |[Argument Check]|
	if(piFloorNumber == nil) then return end
	if(psChestName  == nil) then return end
	
	-- |[Validity Check]|
	--Check if there's a chest with this name. If not, do nothing.
	if(AL_GetProperty("Does Chest Exist", psChestName) == false) then
		return
	end
	
	-- |[Treasure Count Check]|
	--Chests are expected to have the names "ChestA", "ChestB", etc. The letter represents which
	-- chest this is, with A being 1, B is 2, etc. Each floor has a maximum number of allowed
	-- treasure chests, and any excess chests are deleted.
	--Check the numerical value of the given chest. If it exceeds the allowed count, remove it.
	local iValueOfA     = string.byte("A")
	local sEntryLetter  = string.sub(psChestName, -1, -1)
	local iValueOfEntry = string.byte(sEntryLetter) - iValueOfA
	
	--Get the maximum number of entries allowed on this floor. If this group exceeds that count, despawn.
	local iMaxTreasures = self:fnGetMaxTreasuresForFloor(piFloorNumber)
	if(iValueOfEntry >= iMaxTreasures) then
		AL_RemoveObject("Chest", psChestName)
		return
	end
	
	-- |[ ============== Treasure Select =============== ]|
	--Get the treasure set for this floor.
	local zaTreasureData = self:fnGetTreasureDataForFloor(piFloorNumber)
	
	--Select a treasure for this floor. If it comes back nil, despawn the chest.
	local sTreasureName = self:fnSelectTreasure(zaTreasureData)
	if(sTreasureName == nil) then
		AL_RemoveObject("Chest", psChestName)
		return
	end
	
	--Set.
	AL_SetProperty("Set Chest Contents", psChestName, sTreasureName)
end
