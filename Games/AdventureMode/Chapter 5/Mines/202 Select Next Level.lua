-- |[ ============================= MapHelper:fnSelectNextLevel() ============================== ]|
--Given a floor number, executes transition code to go to the room specified. If the room is not
-- a special room, goes to "RANDOMLEVEL".
--This function is local to a particular pregenrandom implementation and is nil by default in
-- the base class.
function MapHelper:fnSelectNextLevel(piFloorNumber)
	
	-- |[ ============== Setup =============== ]|
	-- |[Argument Check]|
	--If a floor number is not provided, goes to "RANDOMLEVEL".
	if(piFloorNumber == nil) then
		AL_BeginTransitionTo("RANDOMLEVEL", "Null")
		return
	end

	-- |[Run Random Floor Roller]|
	--If this is the first time that random floors have been generated, we need to build a list and
	-- store it to specify what floors go where.
	local iHasRolledFloors = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasRolledFloors", "N")
	if(iHasRolledFloors < 2) then
		LM_ExecuteScript(MapHelper.sFloorRoller)
	end

	-- |[ =========== Diagnostics ============ ]|
	--If this diagnostic flag is on, all random levels are skipped. Only the story-important floors
	-- will be moved to. This also modifies the current floor variable.
	local iDebugBypassRandomLevels = VM_GetVar("Root/Variables/Chapter5/Scenes/iDebugBypassRandomLevels", "N")
	if(iDebugBypassRandomLevels == 1.0) then
		
		if(piFloorNumber < 10) then
			VM_SetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "N", 10)
			AL_BeginTransitionTo("TelluriumMinesC", "FORCEPOS:23.0x23.0x0")
		elseif(piFloorNumber < 20) then
			VM_SetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "N", 20)
			AL_BeginTransitionTo("TelluriumMinesD", "FORCEPOS:15.0x9.0x0")
		elseif(piFloorNumber < 30) then
			VM_SetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "N", 30)
			AL_BeginTransitionTo("TelluriumMinesF", "FORCEPOS:16.0x4.0x0")
		elseif(piFloorNumber < 40) then
			VM_SetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "N", 40)
			AL_BeginTransitionTo("TelluriumMinesG", "FORCEPOS:15.0x4.0x0")
		else
			VM_SetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "N", 50)
			AL_BeginTransitionTo("TelluriumMinesH", "FORCEPOS:5.0x25.0x0")
		end
		return
	end

	-- |[ ========= Floor Selection ========== ]|
	-- |[Interqual Floors]|
	--These floors are always in the same spots. They have elevators and other quest properties.
	if(piFloorNumber == 10) then
		AL_BeginTransitionTo("TelluriumMinesC", "FORCEPOS:23.0x23.0x0")
		return

	--Special case: Floor 20 is TelluriumMinesD:
	elseif(piFloorNumber == 20) then
		AL_BeginTransitionTo("TelluriumMinesD", "FORCEPOS:15.0x9.0x0")
		return

	--Special case: Floor 30 is TelluriumMinesF:
	elseif(piFloorNumber == 30) then
		AL_BeginTransitionTo("TelluriumMinesF", "FORCEPOS:16.0x4.0x0")
		return

	--Special case: Floor 40 is TelluriumMinesG:
	elseif(piFloorNumber == 40) then
		AL_BeginTransitionTo("TelluriumMinesG", "FORCEPOS:15.0x4.0x0")
		return

	--Special case: Floor 50 is TelluriumMinesH:
	elseif(piFloorNumber == 50) then
		AL_BeginTransitionTo("TelluriumMinesH", "FORCEPOS:5.0x25.0x0")
		return
	end

	-- |[Special Floors]|
	--Setup.
	local iMineAFloor = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineAFloor", "N")
	local iMineBFloor = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineBFloor", "N")
	local iMineCFloor = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineCFloor", "N")
	local iMineDFloor = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineDFloor", "N")
		
	--Special cases: Check for the randomly placed floors.
	if(piFloorNumber == iMineAFloor) then
		AL_BeginTransitionTo("MinesRandomA", "FORCEPOS:9.0x20.0x0")
		return
		
	elseif(piFloorNumber == iMineBFloor) then
		AL_BeginTransitionTo("MinesRandomB", "FORCEPOS:5.0x15.0x0")
		return
		
	elseif(piFloorNumber == iMineCFloor) then
		AL_BeginTransitionTo("MinesRandomC", "FORCEPOS:9.0x16.0x0")
		return
		
	elseif(piFloorNumber == iMineDFloor) then
		AL_BeginTransitionTo("MinesRandomD", "FORCEPOS:37.0x13.0x0")
		return
	end

	-- |[By-Slot Floors]|
	--Check if there is a string for this floor. If it doesn't exist, go to a random level.
	-- It is also possible the variable name comes back "RANDOMLEVEL".
	local sVarPath = string.format("Root/Variables/Chapter5/Random/sMineFloor%02i", piFloorNumber)
	if(VM_Exists(sVarPath) == true) then
		local sMinesToRoom = VM_GetVar(sVarPath, "S")
		AL_BeginTransitionTo(sMinesToRoom, "Null")
		return
	end

	-- |[No Special Floors]|
	--Generate a random level.
	AL_BeginTransitionTo("RANDOMLEVEL", "Null")

end