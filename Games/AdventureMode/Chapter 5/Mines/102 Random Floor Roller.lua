-- |[ ================================== Random Floor Roller =================================== ]|
--This file determines which floors the Randomly-Placed Pregenerated levels are on. They are the same
-- once selected in a playthrough. All floors always appear, but might appear out of order or on different
-- floors in a given playthrough.

-- |[ ================== System Setup ================== ]|
--Diagnostics.
local bDiagnostics = fnAutoCheckDebugFlag("MapHelper: PregenRandom")
Debug_PushPrint(bDiagnostics, "Beginning random floor roller.\n")

--Constants.
local ciMaxFloors = 50

--Flag so this doesn't happen again.
VM_SetVar("Root/Variables/Chapter5/Scenes/iHasRolledFloors", "N", 2.0)

--Make a list of floors from 1 to 50. By default, each is "RANDOMLEVEL".
local saFloorNames = {}
for i = 1, ciMaxFloors, 1 do
    saFloorNames[i] = "RANDOMLEVEL"
end

-- |[ ================== Quest Levels ================== ]|
--First, place the four high-priority levels. These take precedence over all other levels, and
-- the saFloorNames list will be ignored on these floors. This is because these contain some
-- quest things and must always appear in a playthrough.

--Variable storage.
local iFloorsToGenerate = 4
local iaFloorList = {}
local iaFloorRanges = {{4, 19}, {3, 30}, {6, 35}, {14, 38}}
local iaFixedFloors = {10, 20, 30, 40, 50}
local iFixedFloorsTotal = #iaFixedFloors

-- |[Overall Generation Loop]|
for i = 1, iFloorsToGenerate, 1 do
    
    -- |[Internal Generation Loop]|
    local bLegalRoll = false
    while(bLegalRoll == false) do
        
        --Generate and set.
        bLegalRoll = true
        iaFloorList[i] = LM_GetRandomNumber(iaFloorRanges[i][1], iaFloorRanges[i][2])
        
        --Special, fixed floors.
        for p = 1, iFixedFloorsTotal, 1 do
            if(iaFloorList[i] == iaFixedFloors[p]) then
                bLegalRoll = false 
                p = iFixedFloorsTotal
            end
        end
        
        --Previous floors. Two floors cannot land on the same value.
        for p = 1, i-1, 1 do
            if(iaFloorList[i] == iaFloorList[p]) then
                bLegalRoll = false 
                p = iFixedFloorsTotal
            end
        end
    end
end

--Store everything in variables so the engine knows where to find them.
VM_SetVar("Root/Variables/Chapter5/Scenes/iMineAFloor", "N", iaFloorList[1])
VM_SetVar("Root/Variables/Chapter5/Scenes/iMineBFloor", "N", iaFloorList[2])
VM_SetVar("Root/Variables/Chapter5/Scenes/iMineCFloor", "N", iaFloorList[3])
VM_SetVar("Root/Variables/Chapter5/Scenes/iMineDFloor", "N", iaFloorList[4])

--Create a diagnostics copy.
local iaStoredFloors = {iaFloorList[1], iaFloorList[2], iaFloorList[3], iaFloorList[4]}

--Diagnostics.
Debug_Print("Set quest floors.\n")

-- |[ ============== Pregenerated Levels =============== ]|
-- |[Placement Function]|
--Takes the floor/level list and places them into the destination list. The destination list is the final
-- list of pregenerated levels.
local function fnPlaceListsInto(psaDestination, piaFloors, psaLevels)
    while(#piaFloors > 0 and #psaLevels > 0) do
        
        --Select a floor, select a level.
        local iFloorRoll = LM_GetRandomNumber(1, #piaFloors)
        local iLevelRoll = LM_GetRandomNumber(1, #psaLevels)
        
        --Fast-access variables.
        local iSelectFloor = piaFloors[iFloorRoll]
        local sSelectLevel = psaLevels[iLevelRoll]
        
        --Set floor name.
        psaDestination[iSelectFloor] = sSelectLevel
        
        --Remove the selected elements from their lists.
        table.remove(piaFloors, iFloorRoll)
        table.remove(psaLevels, iLevelRoll)
    end
end

-- |[Setup]|
--Pregenerated floors that get populated with random enemies and treasures. Each block of floors
-- gets populated with a set of pregenerated floors, and any unused slots are left as "RANDOMLEVEL".
DL_AddPath("Root/Variables/Chapter5/Random/")

--Note: Format is as follows. Level 0 is not used, floors start at 1.
--VM_SetVar("Root/Variables/Chapter5/Random/sMineFloor01", "S", "RegulusMinesCavesA")

-- |[Caves Block, 1-9]|
--Create a list of all available floors these can spawn on.
iaFloorList = {1, 2, 3, 4, 5, 6, 7, 8, 9}

--Create a list of all the pregenerated levels that can appear in this block.
local saLevelList = {"RegulusMinesCavesA", "RegulusMinesCavesB", "RegulusMinesCavesC", "RegulusMinesCavesD", "RegulusMinesCavesE", "RegulusMinesCavesF", "RegulusMinesCavesG", "RegulusMinesCavesH", "RegulusMinesCavesI"}

--Call routine to populate.
fnPlaceListsInto(saFloorNames, iaFloorList, saLevelList)
Debug_Print("Set cave block floors.\n")

-- |[Runoff Block, 11-19]|
--Create list of floors.
iaFloorList = {11, 12, 13, 14, 15, 16, 17, 18, 19}

--Create a list of all the pregenerated levels that can appear in this block.
saLevelList = {"RegulusMinesRunoffA", "RegulusMinesRunoffB", "RegulusMinesRunoffC", "RegulusMinesRunoffD", "RegulusMinesRunoffE", "RegulusMinesRunoffF", "RegulusMinesRunoffG", "RegulusMinesRunoffH", "RegulusMinesRunoffI"}

--Call routine to populate.
fnPlaceListsInto(saFloorNames, iaFloorList, saLevelList)
Debug_Print("Set runoff block floors.\n")

-- |[Fixed Floors and Overrides]|
--Purely for diagnostics, these floors are marked with "Fixed".
saFloorNames[10] = "Fixed"
saFloorNames[20] = "Fixed"
saFloorNames[30] = "Fixed"
saFloorNames[40] = "Fixed"
saFloorNames[50] = "Fixed"

--Mark the quest floors as well, overwriting the pregenerated levels.
saFloorNames[iaStoredFloors[1]] = "Mines Random A"
saFloorNames[iaStoredFloors[2]] = "Mines Random B"
saFloorNames[iaStoredFloors[3]] = "Mines Random C"
saFloorNames[iaStoredFloors[4]] = "Mines Random D"

--Diagnostics.
Debug_Print("Set override floors.\n")

-- |[ ================= Final Assembly ================= ]|
-- |[Store Rooms]|
--Store the room affiliations in the DataLibrary so they are saved between playthroughs.
Debug_Print("Printing random floor list.\n")
for i = 1, ciMaxFloors, 1 do
    
    --Generate variable.
    local sVariableName = string.format("Root/Variables/Chapter5/Random/sMineFloor%02i", i)
    
    --Set.
    VM_SetVar(sVariableName, "S", saFloorNames[i])
    
    --Diagnostics.
    Debug_Print(" Floor " .. i .. " is " .. saFloorNames[i] .. "\n")
end

--Diagnostics.
Debug_PopPrint("Completed random floor roller.\n")
