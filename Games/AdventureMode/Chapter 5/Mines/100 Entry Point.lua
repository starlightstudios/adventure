-- |[ ============================= Mines PregenRandom Entry Point ============================= ]|
--Entry point, called from map constructor. Handles calling scripts to populate enemy data, set
-- chests, set appearances, and so on.
local iFloorNumber = VM_GetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "N")

-- |[Enemy Scanning]|
--Orders enemies to generate patrols, set appearance and combat values, or to despawn if there
-- are too many enemies in the level.
MapHelper:fnPreranSetPatrols(iFloorNumber)
MapHelper:fnPreranSetTreasure(iFloorNumber)