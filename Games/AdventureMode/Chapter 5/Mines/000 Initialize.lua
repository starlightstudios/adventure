-- |[ ============================= Mines PregenRandom Initialize ============================== ]|
--Called at chapter setup, initializes the mines pregen-random paths to scripts in this folder.
-- Also verifies the scripts, so if anything is missing this will spit a lot of errors.
local sBasePath = fnResolvePath()

-- |[Clear]|
--Clear everything back to starting values to make sure nothing is lingering in memory.
MapHelper:fnClearPreranVariables()

-- |[Set Vars and Paths]|
--This path is called to generate an enemy patrol if one is needed.
MapHelper.sEntryPointPath       = sBasePath .. "100 Entry Point.lua"
MapHelper.sExamineIntercept     = sBasePath .. "100 Examination Intercept.lua"
MapHelper.sFloorRoller          = sBasePath .. "102 Random Floor Roller.lua"

-- |[Set Functions]|
--Call scripts to set functions.
LM_ExecuteScript(sBasePath .. "200 Generate Enemy Group.lua")
LM_ExecuteScript(sBasePath .. "201 Generate Treasure.lua")
LM_ExecuteScript(sBasePath .. "202 Select Next Level.lua")

-- |[Verify]|
--If any variables aren't set, this will bark warnings.
MapHelper.bIsRandomGenerationReady = MapHelper:fnVerifyPreranVariables()

-- |[ ==================================== Enemy Data Setup ==================================== ]|
--Each of these tables contains a set of enemies. Floors can then reference these tables to pick
-- which enemy groups are able to spawn on that floor.
local zaCryogenicsCluster = {}
table.insert(zaCryogenicsCluster, MapHelper:fnCreateEnemyRollData(100, {"Scraprat Scrounger"}))
table.insert(zaCryogenicsCluster, MapHelper:fnCreateEnemyRollData(100, {"Scraprat Scrounger", "Scraprat Scrounger"}))
table.insert(zaCryogenicsCluster, MapHelper:fnCreateEnemyRollData(100, {"Wrecked Bot"}))
table.insert(zaCryogenicsCluster, MapHelper:fnCreateEnemyRollData(100, {"Wrecked Bot", "Wrecked Bot"}))
table.insert(zaCryogenicsCluster, MapHelper:fnCreateEnemyRollData(100, {"Wrecked Bot", "Scraprat Scrounger"}))

-- |[ ================================== Treasure Data Setup =================================== ]|
-- |[ ========== List Construction =========== ]|
--Treasure is handled differently from enemy clusters. Some treasure items are common to all floors,
-- while others are unique to certain floors. Therefore, a set of lists is made and then treasure
-- clusters made by assembling those lists.
--MapHelper:fnCreateTreasureRollData(piRollChance, psaTreasureNames)

-- |[Gem Clusters]|
--"Common" gems. Each color gets a cluster.
local zCommonGemsA = MapHelper:fnCreateTreasureRollData( 10, {"Glintsteel Gem", "Phassicsteel Gem", "Arensteel Gem"})
local zCommonGemsB = MapHelper:fnCreateTreasureRollData( 10, {"Yemite Gem",     "Romite Gem",       "Morite Gem"})
local zCommonGemsC = MapHelper:fnCreateTreasureRollData( 10, {"Ardrion Gem",    "Nockrion Gem",     "Donion Gem"})
local zCommonGemsD = MapHelper:fnCreateTreasureRollData( 10, {"Rubose Gem",     "Piorose Gem",      "Iniorose Gem"})
local zCommonGemsE = MapHelper:fnCreateTreasureRollData( 10, {"Blurleen Gem",   "Mordreen Gem",     "Quorine Gem"})
local zCommonGemsF = MapHelper:fnCreateTreasureRollData( 10, {"Qederphage Gem", "Phonophage Gem",   "Thatophage Gem"})

--Damage-type gems are less likely to spawn than stat gems.
local zRareGemsA = MapHelper:fnCreateTreasureRollData(  1, {"Surfofine Gem", "Blunofine Gem", "Pokemfine Gem"})
local zRareGemsB = MapHelper:fnCreateTreasureRollData(  1, {"Pirofine Gem",  "Arctofine Gem", "Execrine Gem"})
local zRareGemsC = MapHelper:fnCreateTreasureRollData(  1, {"Photofine Gem", "Corrofine Gem", "Psychofine Gem"})
local zRareGemsD = MapHelper:fnCreateTreasureRollData(  1, {"Hemofine Gem",  "Toxofine Gem",  "Entrofine Gem"})

-- |[Adamantite]|
--Adamantite powder and flakes can always spawn. Above floor 20, shards. Above 40, pieces.
local zAdamPowder = MapHelper:fnCreateTreasureRollData(180, {"Adamantite Powder x1"})
local zAdamFlakes = MapHelper:fnCreateTreasureRollData( 90, {"Adamantite Flakes x1"})
local zAdamShards = MapHelper:fnCreateTreasureRollData( 45, {"Adamantite Shard x1"}) --Floor 20 or higher
local zAdamPieces = MapHelper:fnCreateTreasureRollData( 10, {"Adamantite Piece x1"}) --Floor 40 or higher

-- |[Rep Items]|
--High-ish chances to spawn.
local zRepItems = MapHelper:fnCreateTreasureRollData(300, {"Recycleable Junk", "Assorted Parts", "Bent Tools"})
local zCredits  = MapHelper:fnCreateTreasureRollData(100, {"Credits Chip"})

-- |[Cash, JP]|
--Platina, scales with floor, every 5 floors.
local zPlatinaA = MapHelper:fnCreateTreasureRollData(150, "Platina x45")
local zPlatinaB = MapHelper:fnCreateTreasureRollData(150, "Platina x90")
local zPlatinaC = MapHelper:fnCreateTreasureRollData(150, "Platina x135")
local zPlatinaD = MapHelper:fnCreateTreasureRollData(150, "Platina x180")
local zPlatinaE = MapHelper:fnCreateTreasureRollData(150, "Platina x225")
local zPlatinaF = MapHelper:fnCreateTreasureRollData(150, "Platina x270")
local zPlatinaG = MapHelper:fnCreateTreasureRollData(150, "Platina x315")
local zPlatinaH = MapHelper:fnCreateTreasureRollData(150, "Platina x360")
local zPlatinaI = MapHelper:fnCreateTreasureRollData(150, "Platina x405")
local zPlatinaJ = MapHelper:fnCreateTreasureRollData(150, "Platina x450")

--JP. Always 25, doesn't scale.
local zJPGain = MapHelper:fnCreateTreasureRollData(50, "JP x25")

-- |[Combat Items]|
--These items can spawn on any floor. Magnesium Grenades later on.
local zCombatItems = MapHelper:fnCreateTreasureRollData( 20, {"Nanite Injection", "Explication Spike", "Regeneration Mist", "Emergency Medkit"})
local zMagGrenade  = MapHelper:fnCreateTreasureRollData( 10, {"Magnesium Grenade"}) --Floor 10 or higher

-- |[Equipment]|
--These items can spawn at any depth. The last set at floor 40 or higher.
local zEquipA = MapHelper:fnCreateTreasureRollData( 20, {"Dispersion Cloak", "Adaptive Cloth Vest", "Battle Skirt", "Hyperweave Chemise", "Ceramic Weave Vest", "Neutronium Jacket", "Recoil Dampener"})
local zEquipB = MapHelper:fnCreateTreasureRollData( 20, {"Sphalite Ring", "Decorative Bracer", "Jade Eye Ring", "Enchanted Ring", "Jade Necklace", "Arm Brace", "Alacrity Bracer", "Distribution Frame", "Insulated Boots"})
local zEquipC = MapHelper:fnCreateTreasureRollData( 20, {"Kinetic Capacitor", "Viewfinder Module", "Sure-Grip Gloves", "Spear Foregrip"})
local zEquipD = MapHelper:fnCreateTreasureRollData( 20, {"Wide-Spectrum Scanner", "Pulse Radiation Dampener", "Magrail Harmonization Module", "Smoke Bomb"})
local zEquipE = MapHelper:fnCreateTreasureRollData( 10, {"Neon Tanktop", "Brass Polymer Chestguard", "Titanweave Vest"}) --Floor 40 or higher

--Weapons are sorted by floor cluster.
local zWeaponA = MapHelper:fnCreateTreasureRollData( 20, {"Carbonweave Electrospear", "Cillium Cryospear", "Mk IV Pulse Diffractor"}) --Floor 17 or lower.
local zWeaponB = MapHelper:fnCreateTreasureRollData( 20, {"Niobium Thermospear", "Silksteel Electrospear", "Silksteel Electrohalberd", "Mk IV Pulse Rifle", "Mk V Pulse Diffractor"}) --Floors 18 - 32.
local zWeaponC = MapHelper:fnCreateTreasureRollData( 20, {"Yttrium Electrospear", "K-47 Assault Rifle", "Stalker Bullpup Rifle"}) --Floors 33 - 40.
local zWeaponD = MapHelper:fnCreateTreasureRollData( 20, {"Lithium Thermospear", "Magnetide Cryospear", "Supersteel Electrohalberd", "Mk VI Pulse Diffractor", "R-77 Pulse Diffractor"}) --Floors 41 and up.

-- |[ ========= Cluster Construction ========= ]|
--First, create a common cluster. Further clusters just clone this one.
local zaCommonCluster = {}
table.insert(zaCommonCluster, zCommonGemsA)
table.insert(zaCommonCluster, zCommonGemsB)
table.insert(zaCommonCluster, zCommonGemsC)
table.insert(zaCommonCluster, zCommonGemsD)
table.insert(zaCommonCluster, zCommonGemsE)
table.insert(zaCommonCluster, zCommonGemsF)
table.insert(zaCommonCluster, zRareGemsA)
table.insert(zaCommonCluster, zRareGemsB)
table.insert(zaCommonCluster, zRareGemsC)
table.insert(zaCommonCluster, zRareGemsD)
table.insert(zaCommonCluster, zAdamPowder)
table.insert(zaCommonCluster, zAdamFlakes)
table.insert(zaCommonCluster, zAdamShards)
table.insert(zaCommonCluster, zAdamPieces)
table.insert(zaCommonCluster, zRepItems)
table.insert(zaCommonCluster, zCredits)
table.insert(zaCommonCluster, zJPGain)
table.insert(zaCommonCluster, zCombatItems)
table.insert(zaCommonCluster, zEquipA)
table.insert(zaCommonCluster, zEquipB)
table.insert(zaCommonCluster, zEquipC)
table.insert(zaCommonCluster, zEquipD)

-- |[Assemble Major Clusters]|
local zaCluster1To5   = MapHelper:fnCloneTreasureCluster(zaCommonCluster, {zPlatinaA, zWeaponA})
local zaCluster6To10  = MapHelper:fnCloneTreasureCluster(zaCommonCluster, {zPlatinaB, zWeaponA})
local zaCluster11To15 = MapHelper:fnCloneTreasureCluster(zaCommonCluster, {zPlatinaC, zWeaponA})
local zaCluster16To17 = MapHelper:fnCloneTreasureCluster(zaCommonCluster, {zPlatinaD, zWeaponA})
local zaCluster18To20 = MapHelper:fnCloneTreasureCluster(zaCommonCluster, {zPlatinaD, zWeaponB})
local zaCluster21To25 = MapHelper:fnCloneTreasureCluster(zaCommonCluster, {zPlatinaE, zWeaponB})
local zaCluster26To30 = MapHelper:fnCloneTreasureCluster(zaCommonCluster, {zPlatinaF, zWeaponB})
local zaCluster31To32 = MapHelper:fnCloneTreasureCluster(zaCommonCluster, {zPlatinaG, zWeaponB})
local zaCluster33To35 = MapHelper:fnCloneTreasureCluster(zaCommonCluster, {zPlatinaG, zWeaponC})
local zaCluster36To40 = MapHelper:fnCloneTreasureCluster(zaCommonCluster, {zPlatinaH, zWeaponC})
local zaCluster41To45 = MapHelper:fnCloneTreasureCluster(zaCommonCluster, {zPlatinaI, zWeaponD, zEquipE})
local zaCluster46To50 = MapHelper:fnCloneTreasureCluster(zaCommonCluster, {zPlatinaJ, zWeaponD, zEquipE})

-- |[ ==================================== Floor Data Setup ==================================== ]|
--For each floor, specifies properties like enemy counts and treasure counts. Note that 100 is effectively
-- having no cap as rooms are never that large.

--MapHelper:fnSetFloorData(piFloor, piMaxEnemies, piMaxTreasures,     pzaEnemyLookups, pzaTreasureLookups)
MapHelper:fnSetFloorData(        1,            3,              2, zaCryogenicsCluster,      zaCluster1To5)
MapHelper:fnSetFloorData(        2,            3,              2, zaCryogenicsCluster,      zaCluster1To5)
MapHelper:fnSetFloorData(        3,            3,              2, zaCryogenicsCluster,      zaCluster1To5)
MapHelper:fnSetFloorData(        4,            3,              2, zaCryogenicsCluster,      zaCluster1To5)
MapHelper:fnSetFloorData(        5,            3,              2, zaCryogenicsCluster,      zaCluster1To5)
MapHelper:fnSetFloorData(        6,            3,              2, zaCryogenicsCluster,     zaCluster6To10)
MapHelper:fnSetFloorData(        7,            3,              2, zaCryogenicsCluster,     zaCluster6To10)
MapHelper:fnSetFloorData(        8,            3,              2, zaCryogenicsCluster,     zaCluster6To10)
MapHelper:fnSetFloorData(        9,            3,              2, zaCryogenicsCluster,     zaCluster6To10)
MapHelper:fnSetFloorData(       10,            4,              3, zaCryogenicsCluster,     zaCluster6To10)
MapHelper:fnSetFloorData(       11,            4,              3, zaCryogenicsCluster,    zaCluster11To15)
MapHelper:fnSetFloorData(       12,            4,              3, zaCryogenicsCluster,    zaCluster11To15)
MapHelper:fnSetFloorData(       13,            4,              3, zaCryogenicsCluster,    zaCluster11To15)
MapHelper:fnSetFloorData(       14,            4,              3, zaCryogenicsCluster,    zaCluster11To15)
MapHelper:fnSetFloorData(       15,            5,              3, zaCryogenicsCluster,    zaCluster11To15)
MapHelper:fnSetFloorData(       16,            5,              3, zaCryogenicsCluster,    zaCluster16To17)
MapHelper:fnSetFloorData(       17,            5,              3, zaCryogenicsCluster,    zaCluster16To17)
MapHelper:fnSetFloorData(       18,            5,              3, zaCryogenicsCluster,    zaCluster18To20)
MapHelper:fnSetFloorData(       19,            5,              3, zaCryogenicsCluster,    zaCluster18To20)
MapHelper:fnSetFloorData(       20,            6,              4, zaCryogenicsCluster,    zaCluster18To20)
MapHelper:fnSetFloorData(       21,            6,              4, zaCryogenicsCluster,    zaCluster21To25)
MapHelper:fnSetFloorData(       22,            6,              4, zaCryogenicsCluster,    zaCluster21To25)
MapHelper:fnSetFloorData(       23,            6,              4, zaCryogenicsCluster,    zaCluster21To25)
MapHelper:fnSetFloorData(       24,            6,              4, zaCryogenicsCluster,    zaCluster21To25)
MapHelper:fnSetFloorData(       25,            6,              4, zaCryogenicsCluster,    zaCluster21To25)
MapHelper:fnSetFloorData(       26,            6,              4, zaCryogenicsCluster,    zaCluster26To30)
MapHelper:fnSetFloorData(       27,            6,              4, zaCryogenicsCluster,    zaCluster26To30)
MapHelper:fnSetFloorData(       28,            6,              4, zaCryogenicsCluster,    zaCluster26To30)
MapHelper:fnSetFloorData(       29,            6,              4, zaCryogenicsCluster,    zaCluster26To30)
MapHelper:fnSetFloorData(       30,            6,              4, zaCryogenicsCluster,    zaCluster26To30)
MapHelper:fnSetFloorData(       31,            6,              5, zaCryogenicsCluster,    zaCluster31To32)
MapHelper:fnSetFloorData(       32,            6,              5, zaCryogenicsCluster,    zaCluster31To32)
MapHelper:fnSetFloorData(       33,            6,              5, zaCryogenicsCluster,    zaCluster33To35)
MapHelper:fnSetFloorData(       34,            6,              5, zaCryogenicsCluster,    zaCluster33To35)
MapHelper:fnSetFloorData(       35,            6,              5, zaCryogenicsCluster,    zaCluster33To35)
MapHelper:fnSetFloorData(       36,          100,              5, zaCryogenicsCluster,    zaCluster36To40)
MapHelper:fnSetFloorData(       37,          100,              5, zaCryogenicsCluster,    zaCluster36To40)
MapHelper:fnSetFloorData(       38,          100,              5, zaCryogenicsCluster,    zaCluster36To40)
MapHelper:fnSetFloorData(       39,          100,              5, zaCryogenicsCluster,    zaCluster36To40)
MapHelper:fnSetFloorData(       40,          100,              5, zaCryogenicsCluster,    zaCluster36To40)
MapHelper:fnSetFloorData(       41,          100,              5, zaCryogenicsCluster,    zaCluster41To45)
MapHelper:fnSetFloorData(       42,          100,              5, zaCryogenicsCluster,    zaCluster41To45)
MapHelper:fnSetFloorData(       43,          100,              5, zaCryogenicsCluster,    zaCluster41To45)
MapHelper:fnSetFloorData(       44,          100,              5, zaCryogenicsCluster,    zaCluster41To45)
MapHelper:fnSetFloorData(       45,          100,              5, zaCryogenicsCluster,    zaCluster41To45)
MapHelper:fnSetFloorData(       46,          100,            100, zaCryogenicsCluster,    zaCluster46To50)
MapHelper:fnSetFloorData(       47,          100,            100, zaCryogenicsCluster,    zaCluster46To50)
MapHelper:fnSetFloorData(       48,          100,            100, zaCryogenicsCluster,    zaCluster46To50)
MapHelper:fnSetFloorData(       49,          100,            100, zaCryogenicsCluster,    zaCluster46To50)
MapHelper:fnSetFloorData(       50,          100,            100, zaCryogenicsCluster,    zaCluster46To50)

-- |[ ================================= Appearance Data Setup ================================== ]|
--Add appearance lookups for every enemy that can appear in the generator.

--MapHelper:fnAddAppearanceData(psEnemyName, psSpriteName, piToughness)
MapHelper:fnAddAppearanceData("Scraprat Scrounger", "Scraprat",         0)
MapHelper:fnAddAppearanceData("Scraprat Forager",   "Scraprat",         1)
MapHelper:fnAddAppearanceData("Scraprat Bomber",    "Scraprat",         2)
MapHelper:fnAddAppearanceData("Wrecked Bot",        "SecurityBotBroke", 0)
MapHelper:fnAddAppearanceData("Subverted Bot",      "SecurityBotBroke", 1)
MapHelper:fnAddAppearanceData("Compromised Bot",    "SecurityBotBroke", 2)
MapHelper:fnAddAppearanceData("Mr. Wipey",          "MrWipey",          0)
MapHelper:fnAddAppearanceData("Mr. Wipesalot",      "MrWipey",          1)
MapHelper:fnAddAppearanceData("Der Viper",          "MrWipey",          2)
MapHelper:fnAddAppearanceData("Secrebot",           "Secrebot",         0)
MapHelper:fnAddAppearanceData("Attendebot",         "Secrebot",         1)
MapHelper:fnAddAppearanceData("Plannerbot",         "Plannerbot",       2)
MapHelper:fnAddAppearanceData("Motilvac",           "Motilvac",         0)
MapHelper:fnAddAppearanceData("Autovac",            "Motilvac",         1)
MapHelper:fnAddAppearanceData("Intellivac",         "Motilvac",         2)
MapHelper:fnAddAppearanceData("Darkmatter",         "DarkmatterGirl",   0)
MapHelper:fnAddAppearanceData("Starseer",           "DarkmatterGirl",   1)
MapHelper:fnAddAppearanceData("Nebula Sight",       "DarkmatterGirl",   2)
MapHelper:fnAddAppearanceData("Waylighter",         "Horrible",         0)
MapHelper:fnAddAppearanceData("Lightsman",          "Horrible",         1)
MapHelper:fnAddAppearanceData("Blind Seer",         "Horrible",         2)
MapHelper:fnAddAppearanceData("Void Rift",          "VoidRift",         0)
MapHelper:fnAddAppearanceData("Spatial Tear",       "VoidRift",         1)
MapHelper:fnAddAppearanceData("Billowing Maw",      "VoidRift",         2)
MapHelper:fnAddAppearanceData("Latex Drone",        "LatexDrone",       0)
MapHelper:fnAddAppearanceData("Latex Packmaster",   "LatexDrone",       1)
MapHelper:fnAddAppearanceData("Latex Security",     "LatexDrone",       2)
MapHelper:fnAddAppearanceData("Mad Worker",         "GolemSlaveP",      0)
MapHelper:fnAddAppearanceData("Insane Golem",       "GolemSlaveP",      1)
MapHelper:fnAddAppearanceData("Elite Security",     "GolemSlaveP",      2)
MapHelper:fnAddAppearanceData("Mad Lord",           "GolemLordA",       0)
MapHelper:fnAddAppearanceData("Insane Lord",        "GolemLordA",       1)
MapHelper:fnAddAppearanceData("Elite Lord",         "GolemLordA",       2)
MapHelper:fnAddAppearanceData("Mad Doll",           "Doll",             0)
MapHelper:fnAddAppearanceData("Insane Doll",        "Doll",             1)
MapHelper:fnAddAppearanceData("Security Chief",     "Doll",             2)
MapHelper:fnAddAppearanceData("Geisha",             "InnGeisha",        0)
MapHelper:fnAddAppearanceData("Mummy Imp",          "BandageImp",       0)
MapHelper:fnAddAppearanceData("Hoodie",             "Hoodie",           0)
MapHelper:fnAddAppearanceData("Raibie",             "Raibie",           0)
MapHelper:fnAddAppearanceData("Dreamer",            "EldritchDream",    0)
