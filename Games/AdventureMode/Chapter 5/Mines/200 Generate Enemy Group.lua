-- |[ ============================ MapHelper:fnGenerateEnemyGroup() ============================ ]|
--Given a floor number and the name of the leader, who should already have spawned, generates the
-- rest of the data for an enemy group. This includes spawning followers, setting which enemies
-- appear in battle, and modifying the appearances of the entities.
--This function is local to a particular pregenrandom implementation and is nil by default in
-- the base class.
function MapHelper:fnGenerateEnemyGroup(piFloorNumber, psLeaderName)
	
	-- |[ =================== Setup ==================== ]|
	-- |[Argument Check]|
	if(piFloorNumber == nil) then return end
	if(psLeaderName  == nil) then return end
	
	-- |[Validity Check]|
	--Make sure this is an enemy that is subject to being randomly modified. To do this, the enemy
	-- name needs to be "AUTO". If it isn't, stop.
	EM_PushEntity(psLeaderName)
		local sInternalEnemyName = TA_GetProperty("Zeroth Enemy Name")
	DL_PopActiveObject()
	if(sInternalEnemyName ~= "AUTO") then return end
	
	-- |[Enemy Count Check]|
	--Enemies are expected to have name formats where the last two letters are capitals that specify
	-- the number of the enemy. Ex: "EnemyAA", "EnemyBA"
	--The first of these two letters is the one that determines which numerical value this patrol has.
	-- A is 0, B is 1, etc. If the patrol has a value above the max number of enemies allowed on this
	-- floor, it despawns and no further processing is needed.
	local iValueOfA     = string.byte("A")
	local sEntryLetter  = string.sub(psLeaderName, -2, -2)
	local iValueOfEntry = string.byte(sEntryLetter) - iValueOfA
	
	--Get the maximum number of entries allowed on this floor. If this group exceeds that count, despawn.
	local iMaxEnemies = self:fnGetMaxEnemiesForFloor(piFloorNumber)
	if(iValueOfEntry >= iMaxEnemies) then
		
		--Order the enemy to despawn by self-destructing.
		EM_PushEntity(psLeaderName)
			RE_SetDestruct(true)
		DL_PopActiveObject()
		
		--Stop processing.
		return
	end

	-- |[ ============ Resolve Composition ============= ]|
	--Enemy type and loot are determined by which floor we're on. The difficulty starts at Cryogenics
	-- and proceeds all the way to early Biolabs.
	--The floor number is scattered by 3 in either direction.
	local iUseFloorDifficulty = piFloorNumber + LM_GetRandomNumber(-3, 3)

	--Clamp.
	if(iUseFloorDifficulty <  0) then iUseFloorDifficulty =  1 end
	if(iUseFloorDifficulty > 50) then iUseFloorDifficulty = 50 end
	
	-- |[Enemy Roll Data]|
	--Get the enemy roll data for the floor. If the list comes back empty, despawn the leader and stop.
	-- This means there are no enemy packages for the floor.
	local zaEnemyRollData = self:fnGetEnemyDataForFloor(iUseFloorDifficulty)
	if(#zaEnemyRollData < 1) then
		EM_PushEntity(psLeaderName)
			RE_SetDestruct(true)
		DL_PopActiveObject()
		return
	end
	
	--Select which enemy package will be used. If this list comes back empty, despawn the leader and stop.
	-- It is possible for there to be multiple enemy packages on a floor, and if one is empty, then no
	-- enemy spawns in that clump.
	local saEnemyList = self:fnSelectEnemyGroup(zaEnemyRollData)
	if(#saEnemyList < 1) then
		EM_PushEntity(psLeaderName)
			RE_SetDestruct(true)
		DL_PopActiveObject()
		return
	end
	
	-- |[ =============== Apply Package ================ ]|
	-- |[Leader]|
	--In we got this far, there is at least one enemy in the selected pack. The first one is the leader,
	-- and that entity already exists. We just need to adjust the appearance and combat properties.
	local sCombatName = saEnemyList[1]
	local sSpriteName, iToughness = self:fnGetAppearanceData(sCombatName)
	
	--Apply.
	EM_PushEntity(psLeaderName)
	
		--Set properties.
		TA_SetProperty("Set Combat Enemy", sCombatName)
		TA_SetProperty("Toughness", iToughness)
		LM_ExecuteScript(gsRoot .. "Subroutines/Field Entities/Build Graphics From Name.lua", sSpriteName, 0, 4)
		
		--Retrieve the leader's location. This is needed for spawning additional enemies.
		local fLeaderPosX, fLeaderPosY = TA_GetProperty("Position")
	DL_PopActiveObject()

	-- |[Additional Enemies]|
	--If the list contains more than one entry, then those additional enemies need to be spawned and ordered
	-- to follow behind the leader.
	local sShortName = string.sub(psLeaderName, 1, -2) --All but last letter of the leader's name. Last letter is replaced.

	--Iterate across the names table and spawn enemies.
	for i = 2, #saEnemyList, 1 do
		
		--Fast-access variables.
		local sCombatName = saEnemyList[i]
		
		--Generate a unique name. Additional enemies start at B.
		local iLastLetterByte = string.byte("A") + (i - 1)
		local sEnemyName = sShortName .. string.char(iLastLetterByte)
		
		--Resolve appearance data.
		local sSpriteName, iToughness = self:fnGetAppearanceData(sCombatName)
		
		--Create enemy.
		TA_Create(sEnemyName)
			TA_SetProperty("Position", fLeaderPosX / gciSizePerTile, fLeaderPosY / gciSizePerTile)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Clipping Flag", false)
			TA_SetProperty("Activate Enemy Mode", sCombatName)
			TA_SetProperty("Toughness", iToughness)
			LM_ExecuteScript(gsRoot .. "Subroutines/Field Entities/Build Graphics From Name.lua", sSpriteName, 0, 4)
			TA_SetProperty("Set Follow Target", psLeaderName)
		DL_PopActiveObject()
	end
end