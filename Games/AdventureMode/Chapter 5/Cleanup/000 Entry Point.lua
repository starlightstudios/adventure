-- |[ ==================================== Chapter 5 Cleanup =================================== ]|
--This script is fired once Chapter 5 is completed. General states of Chapter 5 are stored in a special
-- DataLibrary section to be queried later.

-- |[Flag Completion]|
VM_SetVar("Root/Variables/Global/ChapterComplete/iChapter5", "N", 1.0)

-- |[ ================================ New Chapter Plus Backup ================================= ]|
--Store variables that will be needed if the player decides to do a New Chapter Plus. This includes
-- inventory, forms, money, and more!
DL_AddPath("Root/Variables/NewChapterPlus/Chapter5/")

-- |[Inventory]|
--Basic Platina count.
VM_SetVar("Root/Variables/NewChapterPlus/Chapter5/iPlatina", "N", AdInv_GetProperty("Platina"))

--Adamantite counts.
for i = gciCraft_Adamantite_Powder, gciCraft_Adamantite_Total-1, 1 do
    local iCount = AdInv_GetProperty("Crafting Count", i)
    VM_SetVar("Root/Variables/NewChapterPlus/Chapter5/iAdamantite"..i, "N", iCount)
end

--Items in the inventory, not equipped:
local iFreeItems = AdInv_GetProperty("Total Items Unequipped")
VM_SetVar("Root/Variables/NewChapterPlus/Chapter5/iItemsUnequipped", "N", iFreeItems)

--Store the items and their counts:
for i = 1, iFreeItems, 1 do
    AdInv_PushItemI(i-1)
        local sName = AdItem_GetProperty("Name")
        local iQuantity = AdItem_GetProperty("Quantity")
        VM_SetVar("Root/Variables/NewChapterPlus/Chapter5/sItem"..i.."Name", "S", sName)
        VM_SetVar("Root/Variables/NewChapterPlus/Chapter5/iItem"..i.."Quantity", "N", iQuantity)
    DL_PopActiveObject()
end

-- |[Doctor Bag Boosts]|
local iWorkCreditsDoctorBagCharges = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsDoctorBagCharges", "N")
local iWorkCreditsDoctorBagPotency = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsDoctorBagPotency", "N")
VM_SetVar("Root/Variables/NewChapterPlus/Chapter5/iWorkCreditsDoctorBagCharges", "N", iWorkCreditsDoctorBagCharges)
VM_SetVar("Root/Variables/NewChapterPlus/Chapter5/iWorkCreditsDoctorBagPotency", "N", iWorkCreditsDoctorBagPotency)

-- |[Costumes]|
--Store costumes unlocked by sidequest. Many costumes are unlocked implicitly as the events of chapter 5
-- always unlock them.

-- |[Forms]|
--Store the forms unlocked for Christine. Only mark the optional ones.
VM_SetVar("Root/Variables/NewChapterPlus/Chapter5/iFormUnlocked_Latex",         "N", VM_GetVar("Root/Variables/Global/Christine/iHasLatexForm", "N"))
VM_SetVar("Root/Variables/NewChapterPlus/Chapter5/iFormUnlocked_Darkmatter",    "N", VM_GetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N"))
VM_SetVar("Root/Variables/NewChapterPlus/Chapter5/iFormUnlocked_Electrosprite", "N", VM_GetVar("Root/Variables/Global/Christine/iHasElectrospriteForm", "N"))
VM_SetVar("Root/Variables/NewChapterPlus/Chapter5/iFormUnlocked_Secrebot",      "N", VM_GetVar("Root/Variables/Global/Christine/iHasSecrebotForm", "N"))
VM_SetVar("Root/Variables/NewChapterPlus/Chapter5/iFormUnlocked_SteamDroid",    "N", VM_GetVar("Root/Variables/Global/Christine/iHasSteamDroidForm", "N"))
VM_SetVar("Root/Variables/NewChapterPlus/Chapter5/iFormUnlocked_Raiju",         "N", VM_GetVar("Root/Variables/Global/Christine/iHasRaijuForm", "N"))
VM_SetVar("Root/Variables/NewChapterPlus/Chapter5/iFormUnlocked_Raibie",        "N", VM_GetVar("Root/Variables/Global/Christine/iHasRaibieForm", "N"))

-- |[Skillbooks]|
--Store the total number of skillbooks unlocked. Individual skillbooks are handled later.
VM_SetVar("Root/Variables/NewChapterPlus/Chapter5/iSkillbooksChristine", "N", VM_GetVar("Root/Variables/Global/Christine/iSkillbookTotal", "N"))
VM_SetVar("Root/Variables/NewChapterPlus/Chapter5/iSkillbooksTiffany",   "N", VM_GetVar("Root/Variables/Global/Tiffany/iSkillbookTotal",   "N"))
VM_SetVar("Root/Variables/NewChapterPlus/Chapter5/iSkillbooksSX-399",    "N", VM_GetVar("Root/Variables/Global/SX-399/iSkillbookTotal",    "N"))

-- |[ ======================================== Clearing ======================================== ]|
-- |[Catalyst Counts]|
local iCatalystH  = VM_GetVar("Root/Variables/Global/Catalysts/iHealth", "N")
local iCatalystAt = VM_GetVar("Root/Variables/Global/Catalysts/iAttack", "N")
local iCatalystI  = VM_GetVar("Root/Variables/Global/Catalysts/iInitiative", "N")
local iCatalystE  = VM_GetVar("Root/Variables/Global/Catalysts/iEvade", "N") + VM_GetVar("Root/Variables/Global/Catalysts/iDodge", "N")
local iCatalystAc = VM_GetVar("Root/Variables/Global/Catalysts/iAccuracy", "N")
local iCatalystSk = VM_GetVar("Root/Variables/Global/Catalysts/iMovement", "N") + VM_GetVar("Root/Variables/Global/Catalysts/iSkill", "N")

-- |[Inventory Handling]|
--Remove the Platinum Compass
VM_SetVar("Root/Variables/Global/CatalystTone/iHasCatalystTone", "N", 0.0)

--Change Christine to no longer be the party leader.
AL_SetProperty("Player Actor ID", 0)

--Remove everyone from the party.
AdvCombat_SetProperty("Clear Party")

--Set Cash to 0, remove crafting ingredients, drop the inventory entirely.
AdInv_SetProperty("Clear")

-- |[Reinstate Catalysts]|
--Clearing the inventory zeroes off the catalyst count, but it persists between chapters. Reset them here.
AdInv_SetProperty("Catalyst Count", gciCatalyst_Health,     iCatalystH)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Attack,     iCatalystAt)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Initiative, iCatalystI)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Evade,      iCatalystE)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Accuracy,   iCatalystAc)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Skill,      iCatalystSk)

--Call the auto-set function with "Null". Zeroes the cur/max counts for the catalysts.
AdInv_SetProperty("Resolve Catalysts By String", "Null")
