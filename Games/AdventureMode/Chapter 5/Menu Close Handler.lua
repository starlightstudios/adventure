-- |[ =================================== Menu Close Handler =================================== ]|
--Whenever the player closes the pause menu, this script gets executed. This is used to allow the
-- player to equip the harpy badge and make harpies no longer target the party.
if(fnArgCheck(1) == false) then return end
local sSwitchType = LM_GetScriptArgument(0)

-- |[ ==================================== Change Equipment ==================================== ]|
--Only handle this when changing equipment.
if(sSwitchType ~= "Change Equipment") then return end

--Special exception: In RegulusBiolabsDatacoreD, darkmatters don't spawn until the player has the red keycard.
local sLevelName = AL_GetProperty("Name")
local iBiolabsFoundRedKeycard = VM_GetVar("Root/Variables/Chapter5/Scenes/iBiolabsFoundRedKeycard", "N")
if(sLevelName == "RegulusBiolabsDatacoreD" and iBiolabsFoundRedKeycard == 0.0) then return end

--Pulse.
fnStandardEnemyPulse()
