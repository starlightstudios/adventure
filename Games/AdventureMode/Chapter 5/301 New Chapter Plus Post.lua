-- |[ ================================ New Chapter Plus - Post ================================= ]|
--After the initializer file is called, call this file to overwrite relevant variables.

-- |[Inventory]|
--Platina.
AdInv_SetProperty("Add Platina", VM_GetVar("Root/Variables/NewChapterPlus/Chapter5/iPlatina", "N"))

--Adamantite.
for i = gciCraft_Adamantite_Powder, gciCraft_Adamantite_Total-1, 1 do
    AdInv_SetProperty("Crafting Material", i, VM_GetVar("Root/Variables/NewChapterPlus/Chapter5/iAdamantite"..i, "N"))
end

--Inventory items.
local iItemsTotal = VM_GetVar("Root/Variables/NewChapterPlus/Chapter5/iItemsUnequipped", "N")
for i = 1, iItemsTotal, 1 do
    local sItemName     = VM_GetVar("Root/Variables/NewChapterPlus/Chapter5/sItem"..i.."Name",     "S")
    local iItemQuantity = VM_GetVar("Root/Variables/NewChapterPlus/Chapter5/iItem"..i.."Quantity", "N")
    for p = 1, iItemQuantity, 1 do
        LM_ExecuteScript(gsItemListing, sItemName)
    end
end

-- |[Doctor Bag Boosts]|
local iWorkCreditsDoctorBagCharges = VM_GetVar("Root/Variables/NewChapterPlus/Chapter5/iWorkCreditsDoctorBagCharges", "N")
local iWorkCreditsDoctorBagPotency = VM_GetVar("Root/Variables/NewChapterPlus/Chapter5/iWorkCreditsDoctorBagPotency", "N")
VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsDoctorBagCharges", "N", iWorkCreditsDoctorBagCharges)
VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsDoctorBagPotency", "N", iWorkCreditsDoctorBagPotency)

-- |[Costumes]|
--Christine's costumes. These start unlocked.
VM_SetVar("Root/Variables/Costumes/Christine/iGolemGala",   "N", 1.0)
VM_SetVar("Root/Variables/Costumes/Christine/iDollSweater", "N", 1.0)
VM_SetVar("Root/Variables/Costumes/Christine/iDollNude",    "N", 1.0)
VM_SetVar("Root/Variables/Costumes/Christine/iHumanNude",   "N", 1.0)
VM_SetVar("Root/Variables/Costumes/Christine/iRaijuNude",   "N", 1.0)

--Sophie's costumes.
VM_SetVar("Root/Variables/Costumes/Sophie/iGolemGala", "N", 1.0)

-- |[Forms]|
--Forms unlocked for Christine:
VM_SetVar("Root/Variables/Global/Christine/iHasDollForm",          "N", 1.0)
VM_SetVar("Root/Variables/Global/Christine/iHasGolemForm",         "N", 1.0)
VM_SetVar("Root/Variables/Global/Christine/iHasLatexForm",         "N", VM_GetVar("Root/Variables/NewChapterPlus/Chapter5/iFormUnlocked_Latex", "N"))
VM_SetVar("Root/Variables/Global/Christine/iHasDarkmatterForm",    "N", VM_GetVar("Root/Variables/NewChapterPlus/Chapter5/iFormUnlocked_Darkmatter", "N"))
VM_SetVar("Root/Variables/Global/Christine/iHasEldritchForm",      "N", 1.0)
VM_SetVar("Root/Variables/Global/Christine/iHasElectrospriteForm", "N", VM_GetVar("Root/Variables/NewChapterPlus/Chapter5/iFormUnlocked_Electrosprite", "N"))
VM_SetVar("Root/Variables/Global/Christine/iHasSecrebotForm",      "N", VM_GetVar("Root/Variables/NewChapterPlus/Chapter5/iFormUnlocked_Secrebot", "N"))
VM_SetVar("Root/Variables/Global/Christine/iHasSteamDroidForm",    "N", VM_GetVar("Root/Variables/NewChapterPlus/Chapter5/iFormUnlocked_SteamDroid", "N"))
VM_SetVar("Root/Variables/Global/Christine/iHasRaijuForm",         "N", VM_GetVar("Root/Variables/NewChapterPlus/Chapter5/iFormUnlocked_Raiju", "N"))
VM_SetVar("Root/Variables/Global/Christine/iHasRaibieForm",        "N", VM_GetVar("Root/Variables/NewChapterPlus/Chapter5/iFormUnlocked_Raibie", "N"))

--Unlock Tiffany's Spearhead job.
VM_SetVar("Root/Variables/Global/Tiffany/iHasJob_Spearhead", "N", 1.0)

-- |[Skillbooks]|
VM_SetVar("Root/Variables/Global/Christine/iSkillbookTotal", "N", VM_GetVar("Root/Variables/NewChapterPlus/Chapter5/iSkillbooksChristine", "N"))
VM_SetVar("Root/Variables/Global/Tiffany/iSkillbookTotal",   "N", VM_GetVar("Root/Variables/NewChapterPlus/Chapter5/iSkillbooksTiffany",   "N"))
VM_SetVar("Root/Variables/Global/SX-399/iSkillbookTotal",    "N", VM_GetVar("Root/Variables/NewChapterPlus/Chapter5/iSkillbooksSX-399",    "N"))

-- |[Characters]|
--Reset characters to their defaults.
LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Male.lua")
LM_ExecuteScript(gsRoot .. "FormHandlers/Tiffany/Job_Assault.lua")
LM_ExecuteScript(gsRoot .. "FormHandlers/SX399/Job_Shocktrooper.lua")

-- |[ ===================================== Chest Handling ===================================== ]|
--Store the catalyst chests.
local saList = {}
local function fnAddToList(psLevelName, psChestName, psChapter, psCatalystType)
    local i = #saList + 1
    saList[i] = {}
    saList[i].sLevelName    = psLevelName
    saList[i].sChestName    = psChestName
    saList[i].sChapter      = psChapter
    saList[i].sCatalystType = psCatalystType
end

--Chapter 1 Catalysts.
fnAddToList("ArbonnePlainsD",         "ChestB", "Chapter 1", "Accuracy")
fnAddToList("BeehiveBasementB",       "ChestA", "Chapter 1", "Evade")
fnAddToList("BeehiveBasementE",       "ChestA", "Chapter 1", "Skill")
fnAddToList("TrapMainFloorCentral",   "ChestA", "Chapter 1", "Attack")
fnAddToList("TrapMainFloorEast",      "ChestA", "Chapter 1", "Attack")
fnAddToList("TrapUpperFloorMain",     "ChestA", "Chapter 1", "Skill")
fnAddToList("EvermoonCassandraA",     "ChestA", "Chapter 1", "Skill")
fnAddToList("EvermoonE",              "ChestB", "Chapter 1", "Accuracy")
fnAddToList("EvermoonNE",             "ChestB", "Chapter 1", "Health")
fnAddToList("EvermoonS",              "ChestC", "Chapter 1", "Evade")
fnAddToList("EvermoonSEC",            "ChestA", "Chapter 1", "Initiative")
fnAddToList("EvermoonSlimeVillage",   "ChestA", "Chapter 1", "Health")
fnAddToList("PlainsC",                "ChestD", "Chapter 1", "Skill")
fnAddToList("PlainsNW",               "ChestA", "Chapter 1", "Health")
fnAddToList("SaltFlats",              "ChestA", "Chapter 1", "Initiative")
fnAddToList("TrannadarTradingPost",   "ChestB", "Chapter 1", "Accuracy")
fnAddToList("QuantirManseBasementE",  "ChestF", "Chapter 1", "Initiative")
fnAddToList("QuantirManseBasementW",  "ChestA", "Chapter 1", "Evade")
fnAddToList("QuantirManseSecretExit", "ChestA", "Chapter 1", "Health")
fnAddToList("StarfieldSwampB",        "ChestE", "Chapter 1", "Skill")
fnAddToList("TrafalNW",               "ChestB", "Chapter 1", "Skill")
fnAddToList("TrapBasementC",          "ChestA", "Chapter 1", "Health")
fnAddToList("TrapBasementE",          "ChestB", "Chapter 1", "Attack")
fnAddToList("TrapBasementH",          "ChestA", "Chapter 1", "Health")

--Chapter 5 Catalysts.
fnAddToList("RegulusBiolabsAlphaC",     "Chest D", "Chapter 5 Bio", "Evade")
fnAddToList("RegulusBiolabsAmphibianE", "ChestA",  "Chapter 5 Bio", "Accuracy")
fnAddToList("RegulusBiolabsAmphibianE", "ChestB",  "Chapter 5 Bio", "Accuracy")
fnAddToList("RegulusBiolabsAmphibianE", "ChestC",  "Chapter 5 Bio", "Accuracy")
fnAddToList("RegulusBiolabsDatacoreD",  "Chest C", "Chapter 5 Bio", "Evade")
fnAddToList("RegulusBiolabsGeneticsC",  "Chest B", "Chapter 5 Bio", "Evade")
fnAddToList("RegulusCity198C",          "Chest A", "Chapter 5", "Health")
fnAddToList("RegulusCryoC",             "Chest A", "Chapter 5", "Health")
fnAddToList("RegulusCryoLowerC",        "ChestB",  "Chapter 5", "Skill")
fnAddToList("RegulusCryoPowerCoreD",    "Chest A", "Chapter 5", "Skill")
fnAddToList("RegulusExteriorTRNB",      "ChestA",  "Chapter 5", "Skill")
fnAddToList("RegulusExteriorWB",        "Chest H", "Chapter 5", "Health")
fnAddToList("RegulusLRTEA",             "Chest A", "Chapter 5", "Health")
fnAddToList("RegulusLRTHB",             "ChestA",  "Chapter 5", "Skill")
fnAddToList("RegulusManufactoryG",      "ChestC",  "Chapter 5", "Skill")
fnAddToList("SprocketCityA",            "Chest A", "Chapter 5", "Health")
fnAddToList("SerenityObservatoryE",     "Chest A", "Chapter 5", "Skill")

-- |[Store]|
--Setup.
DL_AddPath("Root/Variables/NewChapterPlus/Chests/")
local iTotalStoredOpen = 0

--Catalyst counts.
local iCatalystCounts = {}
iCatalystCounts[gciCatalyst_Health] = 0
iCatalystCounts[gciCatalyst_Attack] = 0
iCatalystCounts[gciCatalyst_Initiative] = 0
iCatalystCounts[gciCatalyst_Evade] = 0
iCatalystCounts[gciCatalyst_Accuracy] = 0
iCatalystCounts[gciCatalyst_Skill] = 0

--Iterate across the list and store the open state of the chests.
for i = 1, #saList, 1 do
    
    --Check if the chest variable exists.
    local sChestPath = "Root/Variables/Chests/" .. saList[i].sLevelName .. "/" .. saList[i].sChestName
    local bExists = VM_Exists(sChestPath)
    if(bExists) then
        
        --Debug.
        --io.write("Chest " .. sChestPath .. " was opened.\n")
        
        --Create a variable. This will flag the chest as already opened.
        local sVariable = string.format("sChestUnlock%03i", iTotalStoredOpen)
        VM_SetVar("Root/Variables/NewChapterPlus/Chests/" .. sVariable, "S", sChestPath)
        
        --Increment the "current" count of the associated catalyst, if and only if it's the current chapter in play.
        --Chapter 5 is split into two halves, which have different catalyst counts. We are concerned with the first half.
        if(saList[i].sChapter == "Chapter 5") then
            if(saList[i].sCatalystType == "Health") then
                iCatalystCounts[gciCatalyst_Health] = iCatalystCounts[gciCatalyst_Health] + 1
            elseif(saList[i].sCatalystType == "Attack") then
                iCatalystCounts[gciCatalyst_Attack] = iCatalystCounts[gciCatalyst_Attack] + 1
            elseif(saList[i].sCatalystType == "Initiative") then
                iCatalystCounts[gciCatalyst_Initiative] = iCatalystCounts[gciCatalyst_Initiative] + 1
            elseif(saList[i].sCatalystType == "Dodge" or saList[i].sCatalystType == "Evade") then
                iCatalystCounts[gciCatalyst_Evade] = iCatalystCounts[gciCatalyst_Evade] + 1
            elseif(saList[i].sCatalystType == "Accuracy") then
                iCatalystCounts[gciCatalyst_Accuracy] = iCatalystCounts[gciCatalyst_Accuracy] + 1
            elseif(saList[i].sCatalystType == "Skill") then
                iCatalystCounts[gciCatalyst_Skill] = iCatalystCounts[gciCatalyst_Skill] + 1
            end
        end
        
        --Increment.
        iTotalStoredOpen = iTotalStoredOpen + 1
    else
        --io.write("Chest " .. sChestPath .. " was not opened.\n")
    end
end
--io.write("Total number of opened chests: " .. iTotalStoredOpen .. "\n")

-- |[Clear]|
--Purge the chests part of the datalibrary.
DL_Purge("Root/Variables/Chests/", false)

-- |[Restore]|
--Now iterate back across the stored chests and write their variables.
for i = 0, iTotalStoredOpen - 1, 1 do
    
    --Get variables.
    local sVariable = string.format("sChestUnlock%03i", i)
    local sChestPath = VM_GetVar("Root/Variables/NewChapterPlus/Chests/" .. sVariable, "S")
    
    --Add the path to make sure.
    DL_AddPath(sChestPath)
    VM_SetVar(sChestPath, "N", 0.0)
    --io.write("Marked chest " .. sChestPath .. " as open.\n")
end

-- |[ ==================================== Catalyst Handling =================================== ]|
--The player retains catalysts for this chapter and from other chapters. The number of "current"
-- catalysts needs to be based on which chests were open.
AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Health,     iCatalystCounts[gciCatalyst_Health])
AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Attack,     iCatalystCounts[gciCatalyst_Attack])
AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Initiative, iCatalystCounts[gciCatalyst_Initiative])
AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Evade,      iCatalystCounts[gciCatalyst_Evade])
AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Accuracy,   iCatalystCounts[gciCatalyst_Accuracy])
AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Skill,      iCatalystCounts[gciCatalyst_Skill])

-- |[ ===================================== Topic Handling ===================================== ]|
WD_SetProperty("Set All Topics To Default")

-- |[ ==================================== Special Instances =================================== ]|
--Reset monsters. Execute a rest action.
AM_SetProperty("Execute Rest")

--Set special flags here:

--Remove all special items:
