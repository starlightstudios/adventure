-- |[ ==================================== Bestiary Handler ==================================== ]|
--Whenever the journal is opened to bestiary mode, run this script to populate bestiary entries.

-- |[Stat Table Check]|
if(gczaChapter5Stats == nil) then return end

-- |[Setup]|
--Access variable.
local zEntry = nil 

--Create chart for storage.
gzaCh5BestiaryEntries = {}

--Set active globals. This reduces the number of arguments that need to be passed.
JournalEntry.zaEnemyStats = gczaChapter5Stats
JournalEntry.zaAppendList = gzaCh5BestiaryEntries

-- |[Prototype]|
--zEntry = JournalEntry:new(sUniqueName, sDisplayName)
--zEntry:fnSetToChartEntry (sNameOnStatChart, bAlwaysShow, fPortraitOffsetX, fPortraitOffsetY, bCanTFPlayer)
--zEntry:fnSetDescription  (sDescription)

-- |[ =============== Listing ================ ]|
-- |[Cryogenics]|
zEntry = JournalEntry:new("Scraprat Scrounger", "Scraprat Scrounger")
zEntry:fnSetToChartEntry ("Scraprat Scrounger", gbDoNotAlwaysShow, 752, 160, gbCannotTFPlayer)
zEntry:fnSetDescription  ("The lowest-ranking member of the Scraprat line, the Scrounger is[BR]mass-produced for scavenging in locations with significant threat. " ..
                          "[BR]Their limited programming makes it difficult for them to distinguish[BR]a functional unit from scrap.[BR][BR][BR]Commonly Found: Cryogenics")

zEntry = JournalEntry:new("Wrecked Bot", "Wrecked Bot")
zEntry:fnSetToChartEntry ("Wrecked Bot", gbDoNotAlwaysShow,  712, 215, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A badly-damaged security bot that is in dire need of repairs. With[BR]its connection to the central datacenter cut off, this bot continues[BR]to " ..
                          "execute its last instructions: to eliminate any unauthorized[BR]entities within the vicinity.[BR][BR][BR]Commonly Found: Cryogenics")

zEntry = JournalEntry:new("Motilvac", "Motilvac")
zEntry:fnSetToChartEntry ("Motilvac", gbDoNotAlwaysShow, 760, 151, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A damaged pet autovacuum that has been released into the tunnels[BR]after a heartless Unit decided they would no longer wanted to care[BR]for it. It has " ..
                          "accumulated significant damage since its release,[BR]destabilizing its cleaning algorithms and making it highly susceptible[BR]to shocks and corrosion.[BR][BR]Commonly Found: Tellurium Mines")

zEntry = JournalEntry:new("Mr. Wipey", "Mr. Wipey")
zEntry:fnSetToChartEntry ("Mr. Wipey", gbDoNotAlwaysShow, 744, 269, gbCannotTFPlayer)
zEntry:fnSetDescription  ("Tired of Slave Units tracking dirt into your department? Want to[BR]keep your office clean without resorting to unreliable " ..
                          "drones?[BR]Get a Mr. Wipey Floor Wash-and-Wax unit today for the cleanest[BR]floors with the brightest shine you could ever see![BR]WARNING: " ..
                          "Do not damage your Mr. Wipey Floor Wash-and-Wax unit![BR]Damage voids your warranty and may cause your Mr. Wipey to rebel.[BR]Commonly Found: Tellurium Mines")

zEntry = JournalEntry:new("Secrebot", "Secrebot")
zEntry:fnSetToChartEntry ("Secrebot", gbDoNotAlwaysShow, 783, 159, gbCannotTFPlayer)
zEntry:fnSetDescription  ("An older-model administrative unit who once handled the day-to-day[BR]planning for a Lord Unit, the Secrebot line were deemed too " ..
                          "costly to[BR]upgrade and were slated for retirement. Some managed to escape[BR]into the underbelly of the city where they attack anyone they " ..
                          "believe [BR]is trying to fulfill those old orders.[BR][BR]Commonly Found: Tellurium Mines")

zEntry = JournalEntry:new("Scraprat Forager", "Scraprat Forager")
zEntry:fnSetToChartEntry ("Scraprat Forager", gbDoNotAlwaysShow, 752, 160, gbCannotTFPlayer)
zEntry:fnSetDescription  ("The forager is a mass-produced reclamations robot designed to locate[BR]and retrieve valuable salvage material in dangerous areas. " ..
                          "They are[BR]frequently adopted as pets, though this requires significant[BR]reprogramming to prevent them from trying to salvage their owners."..
                          "[BR][BR][BR]Commonly Found: Lower Regulus City")

zEntry = JournalEntry:new("Subverted Bot", "Subverted Bot")
zEntry:fnSetToChartEntry ("Subverted Bot", gbDoNotAlwaysShow, 712, 215, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A damaged security bot that has been reprogrammed by an unknown [BR]Unit to defend the tunnels against the very Units it once protected. [BR]Its alarm " ..
                          "functions appear to be destroyed, preventing it from [BR]alerting the city to threats.[BR][BR][BR]Commonly Found: Lower Regulus City")

zEntry = JournalEntry:new("Void Rift", "Void Rift")
zEntry:fnSetToChartEntry ("Void Rift", gbDoNotAlwaysShow, 658, 123, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A spacial anomaly through which a the maw of a bodiless horror is[BR]attempting to enter Pandemonium and Regulus.[BR]The beast " ..
                          "is more tooth than face and its tough hide gives it a[BR]measure of resistance against most forms of physical damage.[BR][BR][BR]Commonly Found: Serenity Crater")

zEntry = JournalEntry:new("Waylighter", "Waylighter")
zEntry:fnSetToChartEntry ("Waylighter", gbDoNotAlwaysShow, 659, 134, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A winged horror whose lantern glows with an empty light. They are[BR]heralds for the forces of the otherworld and guide them along the [BR]path into the " ..
                          "realm that is Pandemonium and Regulus.[BR][BR][BR][BR]Commonly Found: Serenity Crater")

zEntry = JournalEntry:new("Latex Drone", "Latex Drone")
zEntry:fnSetToChartEntry ("Latex Drone", gbDoNotAlwaysShow, 779, 165, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A standard latex drone capable of performing simple tasks while[BR]maintaining a remarkably spiffy mindset. The rubber-like coating on[BR]the chassis " ..
                          "of a latex drone is easy to repair and provides it with[BR]a measure of shock-resistance, making them useful in light combat or[BR]as security guards.[BR][BR]Commonly Found: LRT Facility")

zEntry = JournalEntry:new("Darkmatter", "Darkmatter")
zEntry:fnSetToChartEntry ("Darkmatter", gbDoNotAlwaysShow, 649, 157, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A strange partirhuman who seems to be made of the stuff of stars, in[BR]a more literal way than your average physical construct. Their " ..
                          "bodies[BR]are as dense or as ephemeral as they need to be at that point in time,[BR]and they are resistant to most forms of damage. Despite this, they " ..
                          "are[BR]remarkably easy to frighten, not unlike the kittens their antics are[BR]so often compared to.[BR]Commonly Found: Regulus Surface")

zEntry = JournalEntry:new("Mad Worker", "Mad Worker")
zEntry:fnSetToChartEntry ("Mad Worker", gbDoNotAlwaysShow, 759, 153, gbCannotTFPlayer)
zEntry:fnSetDescription  ("This Slave Golem seems to mutter to itself, repeating the same words[BR]until they lose meaning. It wanders about, its path seemingly " ..
                          "random[BR]though a hint of a pattern may emerge if watched long enough. It is[BR]unfortunate that it does not seem willing to wait that long before[BR]it " ..
                          "attacks the first Unit it sees.[BR][BR]Commonly Found: Tellurium Mines")

zEntry = JournalEntry:new("Mad Lord", "Mad Lord")
zEntry:fnSetToChartEntry ("Mad Lord", gbDoNotAlwaysShow, 795, 171, gbCannotTFPlayer)
zEntry:fnSetDescription  ("This once-dignified Lord Unit shuffles about, muttering to herself[BR]in whispers that sound almost like she were trying to mimic a song.[BR]Her optics " ..
                          "are unfocused and dart about at the slightest motion,[BR]casting an empty gaze at whatever caught her attention.[BR][BR][BR]Commonly Found: Tellurium Mines")

zEntry = JournalEntry:new("Mad Doll", "Mad Doll")
zEntry:fnSetToChartEntry ("Mad Doll", gbDoNotAlwaysShow, 795, 169, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A Command Unit, the pinnacle of production and performance, laid [BR]low by the mad whisperings that echo through the corridors. Though " ..
                          "[BR]her gaze appears focused at first glance, there is an emptiness hiding[BR]behind her ocular sensors that belies the source of her madness.[BR][BR][BR]Commonly Found: Tellurium Mines")

zEntry = JournalEntry:new("Autovac", "Autovac")
zEntry:fnSetToChartEntry ("Autovac", gbDoNotAlwaysShow, 760, 151, gbCannotTFPlayer)
zEntry:fnSetDescription  ("Popular with Lord Golems who have a more... eccentric... taste, the[BR]Autovac features a more powerful motor, providing a stronger[BR]suction for " ..
                          "its vacuum, as well as a more robust framework to allow it [BR]to reach even the hardest-to-reach areas that its base-line Motilvac[BR]counterpart will miss. " ..
                          "Some appear to have escaped to the tunnels as[BR]their Lord Units replaced them with Slave Golems.[BR]Commonly Found: LRT Facility")

zEntry = JournalEntry:new("Mr. Wipesalot", "Mr. Wipesalot")
zEntry:fnSetToChartEntry ("Mr. Wipesalot", gbDoNotAlwaysShow, 744, 269, gbCannotTFPlayer)
zEntry:fnSetDescription  ("After the public and embarrassing events involving four Lord Golems,[BR]a ream of synthetic paper, two paperclips, a dirty scrubbrush, and " ..
                          "one[BR]canister of quick-dry wax, the Mr. Wipey line of cleaning bots was[BR]discontinued and replaced with the Mr. Wipesalot. Unlike the original[BR]version, " ..
                          "the Wipesalot line is only capable of washing floors, not[BR]waxing them. They still tend to go rogue when damaged, however.[BR]Commonly Found: LRT Facility")

zEntry = JournalEntry:new("Attendebot", "Attendebot")
zEntry:fnSetToChartEntry ("Attendebot", gbDoNotAlwaysShow, 783, 159, gbCannotTFPlayer)
zEntry:fnSetDescription  ("An older-model high-end administrative unit who once handled the[BR]day-to-day needs of a Lord Unit, the Attendebot were scheduled for[BR]retirement after " ..
                          "their duties were moved to PDUs and [BR]general-purpose units such as Slaves and Drones. A small number[BR]escaped retirement only to be driven mad as their circuits " ..
                          "degraded.[BR][BR]Commonly Found: LRT Facility")

zEntry = JournalEntry:new("Scraprat Bomber", "Scraprat Bomber")
zEntry:fnSetToChartEntry ("Scraprat Bomber", gbDoNotAlwaysShow, 752, 160, gbCannotTFPlayer)
zEntry:fnSetDescription  ("An unstable scraprat... more than usual. Damage acquired during their[BR]salvaging activities has left these scraprats with a core that " ..
                          "is liable[BR]to rupture at any time. They have been abandoned and cut off from[BR]the primary network, making it even more difficult for them to[BR]distinguish a functional " ..
                          "unit from a pile of scrap worth salvaging.[BR][BR]Commonly Found: Tellurium Mines.")

zEntry = JournalEntry:new("Compromised Bot", "Compromised Bot")
zEntry:fnSetToChartEntry ("Compromised Bot", gbDoNotAlwaysShow, 712, 215, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A damaged security bot whose circuitry has been dominated by[BR]Vivify, causing it to turn on its creators and defend its new master. " ..
                          "[BR]Though its alarm systems appear to be destroyed, it can likely[BR]communicate with its master using... unconventional means.[BR][BR][BR]Commonly Found: Tellurium Mines")

zEntry = JournalEntry:new("Spatial Tear", "Spatial Tear")
zEntry:fnSetToChartEntry ("Spatial Tear", gbDoNotAlwaysShow, 658, 123, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A terrifyingly-large spatial anomaly through which[BR]a monstrous maw is attempting to enter Pandemonium and Regulus.[BR]The " ..
                          "beast is more tooth than face and its tough hide gives it a[BR]measure of resistance against most forms of physical damage.[BR][BR][BR]Commonly Found: Arcane University.")

zEntry = JournalEntry:new("Lightsman", "Lightsman")
zEntry:fnSetToChartEntry ("Lightsman", gbDoNotAlwaysShow, 659, 134, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A massive, winged beast whose lantern shines with an empty light. It[BR]trudges through a reality that only it and those " ..
                          "like it may see,[BR]lighting the path for those who will come after it. It is a beacon[BR]of madness that shines in the turbulence of sanity.[BR][BR][BR]Commonly Found: Arcane University.")

zEntry = JournalEntry:new("Latex Packmaster", "Latex Packmaster")
zEntry:fnSetToChartEntry ("Latex Packmaster", gbDoNotAlwaysShow, 779, 165, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A Latex Drone whose work has been deemed Spiffy and has thus[BR]earned the position of leading a pack of volatile scraprats. The " ..
                          "[BR]hardest part of their job is focusing long enough to provide adequate[BR]security.[BR][BR][BR]Commonly Found: LRT Facility")

zEntry = JournalEntry:new("Starseer", "Starseer")
zEntry:fnSetToChartEntry ("Starseer", gbDoNotAlwaysShow, 649, 157, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A strange partirhuman who seems to be made of the stuff of stars, in[BR]a more literal way than your average physical construct. They " ..
                          "appear[BR]to be older than their Darkmatter counterparts, if the expanse of the[BR]universe visible within them could be considered \'older\'. They " ..
                          "are[BR]surprisingly easy to frighten despite their resilience to injury.[BR][BR]Commonly Found: Tellurium Mines")

zEntry = JournalEntry:new("Golem Security", "Golem Security")
zEntry:fnSetToChartEntry ("Golem Security", gbDoNotAlwaysShow, 759, 153, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A Slave Golem commanded to assist in the maintenance of a blacksite.[BR]Provided with advanced combat algorithms and intense training, " ..
                          "they[BR]will defend the site and the prisoners it holds to the last of their[BR]abilities, lest they themselves end up an occupant of the site.[BR][BR][BR]Commonly Found: Black Site.")

zEntry = JournalEntry:new("Security Lord", "Security Lord")
zEntry:fnSetToChartEntry ("Security Lord", gbDoNotAlwaysShow, 795, 171, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A Lord Golem charged with overseeing the Slave Golems in a [BR]blacksite. They've been provided with advanced combat and tactical " ..
                          "[BR]algorithms. They take pride in their work, and their only regret is that[BR]they are not permitted to brag about their position to other[BR]Lord " ..
                          "Golems.[BR][BR]Commonly Found: Black Site.")

zEntry = JournalEntry:new("Security Doll", "Security Doll")
zEntry:fnSetToChartEntry ("Security Doll", gbDoNotAlwaysShow, 795, 169, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A Command Unit in charge of a blacksite. Their primary function is[BR]to ensure the enemies of the Cause of Science are bent to the " ..
                          "will[BR]of science. If they must break the will of the prisoner as they do[BR]so, then all the better.[BR][BR][BR]Commonly Found: Black Site.")

zEntry = JournalEntry:new("Insane Golem", "Insane Golem")
zEntry:fnSetToChartEntry ("Insane Golem", gbDoNotAlwaysShow, 759, 153, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A Slave Golem working for a security team deployed to the mines.[BR]Negatively affected by the creatures there, they have since 'joined'[BR]the beasts they were" ..
                          "ordered to destroy. There is little logic in their[BR]processors. Try to ignore what they say.[BR][BR][BR]Commonly Found: Tellurium Mines.")

zEntry = JournalEntry:new("Insane Lord", "Insane Lord")
zEntry:fnSetToChartEntry ("Insane Lord", gbDoNotAlwaysShow, 795, 171, gbCannotTFPlayer)
zEntry:fnSetDescription  ("One of many security Lord golems in the city, this one was deployed[BR]to the Tellurium Mines and soon stopped reporting in. Her circuits " ..
                          "[BR]are most likely completed subverted. Shoot on sight." ..
                          "[BR][BR][BR][BR]Commonly Found: Tellurium Mines.")

zEntry = JournalEntry:new("Insane Doll", "Insane Doll")
zEntry:fnSetToChartEntry ("Insane Doll", gbDoNotAlwaysShow, 795, 169, gbCannotTFPlayer)
zEntry:fnSetDescription  ("One of the Command Units lost to the mines in an attempt to lead in[BR]battle. They seem quite intelligent but all they speak is gibberish, and[BR]they attack " ..
                          "anyone or anything they encounter that is not likewise[BR]'aligned' with them.[BR][BR][BR]Commonly Found: Tellurium Mines.")

zEntry = JournalEntry:new("Intellivac", "Intellivac")
zEntry:fnSetToChartEntry ("Intellivac", gbDoNotAlwaysShow, 760, 151, gbCannotTFPlayer)
zEntry:fnSetDescription  ("The highest-end of the Motilvac auto-vacuum line, the Intellivac was[BR]popular as they could assist with day-to-day mundane work " ..
                          "such as[BR]scheduling and taking messages. This brought them into conflict with[BR]the Secrebot line of personal assistants, and the two machines " ..
                          "would[BR]frequently cause significant damage as they competed to complete[BR]the same tasks. Both were obsoleted as Slave Golem usage increased.[BR]Commonly Found: Arcane University.")

zEntry = JournalEntry:new("Der Viper", "Der Viper")
zEntry:fnSetToChartEntry ("Der Viper", gbDoNotAlwaysShow, 744, 269, gbCannotTFPlayer)
zEntry:fnSetDescription  ("The Der Viper replaced the Wipesalot model in an attempt to correct[BR]the frequency with which cleaners assigned to Lord Golems went " ..
                          "[BR]rogue. Unfortunately, the flaws resulting in aggressive behavior could[BR]not be fully removed. The final version was discontinued, replaced[BR]with the " ..
                          "more aesthetic Motilvac. All developers assigned to the Der[BR]Viper product line were converted to Latex Drones.[BR]Commonly Found: Arcane University.")

zEntry = JournalEntry:new("Plannerbot", "Plannerbot")
zEntry:fnSetToChartEntry ("Plannerbot", gbDoNotAlwaysShow, 778, 160, gbCannotTFPlayer)
zEntry:fnSetDescription  ("An older-model mid-end administrative unit who once handled the[BR]day-to-day planning for a Lord Unit, the Plannerbot unit were slated[BR]for " ..
                          "retirement after advancements obsoleted the need of a dedicated[BR]secretarial unit. A small number escaped into the tunnels where the[BR]lack of order and " ..
                          "planning drove them mad.[BR][BR]Commonly Found: Arcane University, Biolabs.")

zEntry = JournalEntry:new("Billowing Maw", "Billowing Maw")
zEntry:fnSetToChartEntry ("Billowing Maw", gbDoNotAlwaysShow, 658, 123, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A manifestation of madness that followed in the wake of Vivify. This[BR]horror seeks to devour all that is yet untouched by madness. Its[BR]thick hide grants " ..
                          "it a measure of resistance against sharp and jagged[BR]objects, perhaps a result of its chomping bite that leaves little[BR]more than a broken reality in its wake.[BR][BR]Commonly " ..
                          "Found: Biolabs.")

zEntry = JournalEntry:new("Blind Seer", "Blind Seer")
zEntry:fnSetToChartEntry ("Blind Seer", gbDoNotAlwaysShow, 659, 134, gbCannotTFPlayer)
zEntry:fnSetDescription  ("Loyal servants of Vivify, the predictions of Blind Seers guide their[BR]kin as they carry out the unfathomable will of their master. Their[BR]very presence " ..
                          "is a portent of the madness that follows in their wake.[BR][BR][BR][BR]Commonly Found: Biolabs.")

zEntry = JournalEntry:new("Latex Security", "Latex Security")
zEntry:fnSetToChartEntry ("Latex Security", gbDoNotAlwaysShow, 779, 165, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A latex drone that has been assigned a security-related function. The[BR]rubber-like coating on the chassis of a latex drone is easy to " ..
                          "repair[BR]and provides it with a measure of shock-resistance, making them[BR]useful in light combat or as security guards.[BR][BR][BR]Commonly Found: Tellurium Mines")

zEntry = JournalEntry:new("Nebula Sight", "Nebula Sight")
zEntry:fnSetToChartEntry ("Nebula Sight", gbDoNotAlwaysShow, 649, 157, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A strange partirhuman who seems to be made of the stuff of stars, in[BR]a more literal way than your average physical construct. The " ..
                          "galaxies[BR]within them appear to be ancient, among the oldest in the universe.[BR]They maintain their playful demeanor despite their age, and debates[BR] " ..
                          "rage over which variety of Darkmatter is cuter. Despite their age[BR]and resilience, they remain remarkably easy to frighten.[BR]Commonly Found: Datacore.")

zEntry = JournalEntry:new("Elite Security", "Elite Security")
zEntry:fnSetToChartEntry ("Elite Security", gbDoNotAlwaysShow, 759, 153, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A Slave Golem in a blacksite charged with guarding its high-value[BR]prisoners. Only the most loyal of Golems achieve this rank, and " ..
                          "they[BR]are provided with the latest combat algorithms that have been honed[BR]against the rebels and enemies of the administration.[BR][BR][BR]Commonly Found: Tellurium Mines.")

zEntry = JournalEntry:new("Elite Lord", "Elite Lord")
zEntry:fnSetToChartEntry ("Elite Lord", gbDoNotAlwaysShow,  795, 171, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A Lord Golem whose loyalty to the administration is so encompassing[BR]that the very thought of bragging about their position would " ..
                          "cause[BR]them to retire themselves on the spot. They oversee the elite Slave[BR]Golems and handle the most high-value of prisoners[BR]Their " ..
                          "combat and tactical algorithms have been tested against those[BR]who would rebel against the administration.[BR]Commonly Found: Tellurium Mines.")

zEntry = JournalEntry:new("Security Chief", "Security Chief")
zEntry:fnSetToChartEntry ("Security Chief", gbDoNotAlwaysShow, 795, 169, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A Command Unit of the highest authority, the Security Chief is often[BR]in direct communication with Central Administration as they " ..
                          "oversee[BR]the operation of high-value blacksites. Their combat algorithms are[BR]the results of many of the largest battles in the history of Regulus[BR]and " ..
                          "Pandemonium. They have not been found wanting.[BR][BR]Commonly Found: Tellurium Mines.")

zEntry = JournalEntry:new("Geisha", "Geisha")
zEntry:fnSetToChartEntry ("Geisha", gbDoNotAlwaysShow, 744, 168, gbCannotTFPlayer)
zEntry:fnSetDescription  ("The swaying dance and moaning voice of the Geisha conjure visions of[BR]a turbulent darkness. It is liquid, it is empty, a frothing foam that[BR]drifts on " ..
                          "the breeze until if lands on an untainted surface and...[BR][BR]pops.[BR][BR]Commonly Found: Biolabs.")

zEntry = JournalEntry:new("Mummy Imp", "Mummy Imp")
zEntry:fnSetToChartEntry ("Mummy Imp", gbDoNotAlwaysShow, 750, 150, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A low-ranking horror loyal to Vivify. The primary strength of these[BR]small creatures is their pack-like mentality and their ability to mob[BR]their " ..
                          "would-be victims. The decayed bandages that wrap around their[BR]bodies make them very susceptible to fire.[BR][BR][BR]Commonly Found: Biolabs.")

zEntry = JournalEntry:new("Hoodie", "Hoodie")
zEntry:fnSetToChartEntry ("Hoodie", gbDoNotAlwaysShow, 808, 165, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A strange humanoid figure that followed in the wake of madness that[BR]is Vivify. A hazy static hum surrounds it, not unlike an untuned[BR]radio. " ..
                          "Those who listen too closely may hear the sound of a voice[BR]within that static. Those who understand the voice are already lost.[BR][BR][BR]Commonly Found: Biolabs.")

zEntry = JournalEntry:new("Raibie", "Raibie")
zEntry:fnSetToChartEntry ("Raibie", gbDoNotAlwaysShow, 741, 144, gbCannotTFPlayer)
zEntry:fnSetDescription  ("A raiju that has been driven mad. These feral ladies are as vicious[BR]as their gentle-minded counterparts are affectionate. They only want[BR]to hug " ..
                          "you. And claw you. And bite you. And make you one of them.[BR]Unsurprisingly, they can shrug off electrical attacks with ease.[BR][BR][BR]Commonly Found: Biolabs Gamma.")

zEntry = JournalEntry:new("Dreamer", "Dreamer")
zEntry:fnSetToChartEntry ("Dreamer", gbDoNotAlwaysShow, 769, 159, gbCannotTFPlayer)
zEntry:fnSetDescription  ("Former humans, living or dead, who have joined Vivify's movement.[BR]They listen to her word and study the future. They are extremely[BR]dangerous, feel no pain, and are relentless." .. 
                          "[BR][BR][BR][BR]Commonly Found: Biolabs Epsilon.")

-- |[ ============== Finish Up =============== ]|
-- |[Assemble List]|
for i = 1, #gzaCh5BestiaryEntries, 1 do
    gzaCh5BestiaryEntries[i]:fnUploadData()
end

-- |[Clean]|
--Unsets globals.
JournalEntry:fnCleanGlobals()

--Clear the list to save memory.
gzaCh5BestiaryEntries = nil
