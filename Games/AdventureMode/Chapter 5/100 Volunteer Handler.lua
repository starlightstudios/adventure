-- |[ ==================================== Volunteer Handler =================================== ]|
--When combat begins, this script is called to determine if the surrender option should say
-- "Volunteer" instead. This is to help players find out if they get a special scene when they
-- lose the battle in question.
gbIsVolunteer = false

--The script expects to have the defeat script passed in.
if(fnArgCheck(1) == false) then return end
local sDefeatScript = LM_GetScriptArgument(0)

-- |[Subdivision]|
local bIsOnDirectory = false
local sLastPart = ""
local sDirectory = ""
for i = string.len(sDefeatScript), 1, -1 do
    
    --Current letter.
    local c = string.sub(sDefeatScript, i, i)
    
    --Letter is a slash:
    if(c == "/" or c == "\\") then
        if(bIsOnDirectory == false) then
            bIsOnDirectory = true
        else
            break
        end
    
    --Otherwise, copy.
    else
        if(bIsOnDirectory == false) then
            sLastPart = c .. sLastPart
        else
            sDirectory =  c .. sDirectory
        end
    end
end

-- |[ ====================================== Case Listing ====================================== ]|
--Now that we know which directory this scene is in, we can figure out if a special scene will play
-- as a result.
if(sDirectory == "Defeat LatexDrone") then
    local iHasLatexForm = VM_GetVar("Root/Variables/Global/Christine/iHasLatexForm", "N")
    if(iHasLatexForm == 0.0) then gbIsVolunteer = true end
    
--Boss Battle against Vivify. Has the volunteer option if player doesn't have EDG form.
elseif(sDirectory == "RegulusLRTIG") then
    local iHasEldritchForm = VM_GetVar("Root/Variables/Global/Christine/iHasEldritchForm", "N")
    if(iHasEldritchForm == 0.0) then gbIsVolunteer = true end

end
