-- |[ ======================================= Dr. Maisie ======================================= ]|
--Mosquito doctor!

--Basic profile.
local zProfileEntry = JournalEntry:new("Maisie", "Dr. Maisie")
zProfileEntry:fnSetToProfile("Root/Images/Portraits/Maisie/Neutral", 0, 0)

--Variables.
local iRaibieQuest              = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N")
local iMainQuestBlock           = VM_GetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N")
local iFlashbackWakeup          = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackWakeup", "N")
local iChristineCorruptedEnding = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineCorruptedEnding", "N")

-- |[ =================== Variations =================== ]|
--Empty profile.
if(iRaibieQuest == 0.0) then
    zProfileEntry:fnBlankProfile()
    
--Special: No profile.
elseif(iMainQuestBlock == 60.0) then
    zProfileEntry:fnSetDisplayName("Dr??Ma??ie")
    zProfileEntry:fnSetDescription(""..
        "Profile unavailable. Please descramble[BR]errant memory files. Code 7715-e.")

--Met Maisie.
elseif(iFlashbackWakeup == 0.0) then
    zProfileEntry:fnSetDescription(""..
        "Doctor Maisie de Havilland, MD, PhD,[BR]University of An Nador.[BR]Professor at Arcane University, Sector[BR]Zero.[BR][BR]"..
        "A leading medical researcher in internal[BR]medicine, practicing surgeon, and[BR]pharmaceutical chemist, there are few[BR]fields that Dr. de Havilland does not[BR]excel at. From a young age, "..
        "her work[BR]ethic pushed her to attend school in[BR]An Nador, funded by her farmer[BR]parents. She was top of the class every[BR]year and earned a scholarship to[BR]An Nador's prestigious "..
        "university, where[BR]she founded an entirely new field of[BR]pharmaceutical research.[BR][BR]"..
        "Her exemplary record as a surgeon got[BR]her noticed by the field observation[BR]teams in An Nador, and she was[BR]approached about working in the biolabs. While she officially[BR]answers "..
        "to the Head of Research, Unit 2856, she largely sets her own[BR]agenda. The two do not get along.[BR][BR]"..
        "Her primary concern is making sure the administration of Regulus[BR]does not keep her revolutionary medical advances a secret.")

--Final profile, does not change with alignment.
else
    zProfileEntry:fnSetDescription(""..
        "Doctor Maisie de Havilland, MD, PhD,[BR]University of An Nador.[BR]Professor at Arcane University, Sector[BR]Zero.[BR][BR]"..
        "A leading medical researcher in internal[BR]medicine, practicing surgeon, and[BR]pharmaceutical chemist, there are few[BR]fields that Dr. de Havilland does not[BR]excel at. From a young age, "..
        "her work[BR]ethic pushed her to attend school in[BR]An Nador, funded by her farmer[BR]parents. She was top of the class every[BR]year and earned a scholarship to[BR]An Nador's prestigious "..
        "university, where[BR]she founded an entirely new field of[BR]pharmaceutical research.[BR][BR]"..
        "Her exemplary record as a surgeon got[BR]her noticed by the field observation[BR]teams in An Nador, and she was[BR]approached about working in the biolabs. While she officially[BR]answers "..
        "to the Head of Research, Unit 2856, she largely sets her own[BR]agenda. The two do not get along.[BR][BR]"..
        "Her primary concern is making sure the administration of Regulus[BR]does not keep her revolutionary medical advances a secret.")
end