-- |[ ====================================== Crowbar-Chan ====================================== ]|
--Nyyuu!
local iSCryoRepoweredDoor       = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoRepoweredDoor", "N")
local iMainQuestBlock           = VM_GetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N")
local iFlashbackWakeup          = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackWakeup", "N")

--Basic profile.
local zProfileEntry = JournalEntry:new("Crowbar-Chan", "Crowbar-Chan")
zProfileEntry:fnSetToProfile("Root/Images/Portraits/CrowbarChan/Neutral", 0, 0)

-- |[ =================== Variations =================== ]|
--Set.
if(iSCryoRepoweredDoor == 0.0) then
    zProfileEntry:fnBlankProfile()
    
--Special: No profile.
elseif(iMainQuestBlock == 60.0) then
    zProfileEntry:fnSetDisplayName("C?owb?r-??an")
    zProfileEntry:fnSetDescription(""..
        "Profile unavailable. Please descramble[BR]errant memory files. Code 7715-e.")

elseif(iFlashbackWakeup == 0.0) then
    zProfileEntry:fnSetDescription(""..
        "She's a robot of some kind. Probably.[BR][BR]Nyyuuuu.")

else
    zProfileEntry:fnSetDescription(""..
        "She's a robot of some kind. Probably.[BR][BR]Nyyuuuu.")
end