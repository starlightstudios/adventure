-- |[ =========================================== Jo =========================================== ]|
--The rebel golem.

--Basic profile.
local zProfileEntry = JournalEntry:new("Jo", "Jo")
zProfileEntry:fnSetToProfile("Root/Images/Portraits/NPCs/GolemRebelHead", 0, 0)

--Variables.
local iAquaticMeeting           = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticMeeting", "N")
local iMainQuestBlock           = VM_GetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N")
local iFlashbackWakeup          = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackWakeup", "N")
local iChristineCorruptedEnding = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineCorruptedEnding", "N")

-- |[ =================== Variations =================== ]|
--Empty profile.
if(iAquaticMeeting == 0.0) then
    zProfileEntry:fnBlankProfile()
    
--Special: No profile.
elseif(iMainQuestBlock == 60.0) then
    zProfileEntry:fnSetDisplayName("??")
    zProfileEntry:fnSetDescription(""..
        "Profile unavailable. Please descramble[BR]errant memory files. Code 7715-e.")

--Met Jo.
elseif(iFlashbackWakeup == 0.0) then
    zProfileEntry:fnSetDescription(""..
        "Unit 209913, Manufacturing,[BR]Sector 34. Her friends call her Jo.[BR][BR]"..
        "A former welding unit, Jo was involved[BR]in a small three-unit group that would[BR]covertly assassinate lord units despised[BR]by the workers. Unit 2855 intervened[BR]when their group was at "..
        "risk of being[BR]discovered, and improved their security[BR]routines. She also gave them some[BR]bombs, which won their trust.[BR][BR]"..
        "Jo is regarded as a capable leader and[BR]tactician. Her squad trusts her with[BR]their lives, and she in turn trusts 55 and[BR]Christine to lead them.")

--Final profile, changes with alignment.
else
    --Normal:
    if(iChristineCorruptedEnding == 0.0) then
        zProfileEntry:fnSetDescription(""..
            "Unit 209913, Manufacturing,[BR]Sector 34. Her friends call her Jo.[BR][BR]"..
            "A former welding unit, Jo was involved[BR]in a small three-unit group that would[BR]covertly assassinate lord units despised[BR]by the workers. Unit 2855 intervened[BR]when their group was at "..
            "risk of being[BR]discovered, and improved their security[BR]routines. She also gave them some[BR]bombs, which won their trust.[BR][BR]"..
            "Jo is regarded as a capable leader and[BR]tactician. Her squad trusts her with[BR]their lives, and she in turn trusts 55 and[BR]Christine to lead them.")

    --Corrupted Christine:
    else
        zProfileEntry:fnSetDisplayName("Unit 209913")
        zProfileEntry:fnSetDescription(""..
            "Unit 209913, Manufacturing,[BR]Sector 34.[BR][BR]"..
            "Maverick unit. Retire on sight.[BR]Priority one.")
    end
end