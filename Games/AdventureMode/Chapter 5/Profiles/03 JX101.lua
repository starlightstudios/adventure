-- |[ ========================================= JX-101 ========================================= ]|
--Appears once 55 mentions her in the mines.

--Basic profile.
local zProfileEntry = JournalEntry:new("JX-101", "JX-101")
zProfileEntry:fnSetToProfile("Root/Images/Portraits/JX-101/Neutral", 0, 0)

--Variables.
local i55ExplainedHerself       = VM_GetVar("Root/Variables/Chapter5/Scenes/i55ExplainedHerself", "N")
local iSawWakeUpIntro           = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawWakeUpIntro", "N")
local iCompletedSprocketCity    = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSprocketCity", "N")
local iSXUpgradeQuest           = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
local iMainQuestBlock           = VM_GetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N")
local iFlashbackWakeup          = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackWakeup", "N")
local iChristineCorruptedEnding = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineCorruptedEnding", "N")

-- |[ =================== Variations =================== ]|
--Retrofit: If the upgrade quest is 4.0 or higher, flag completed sprocket city. [REMOVECHAPTER2]
if(iSXUpgradeQuest >= 4.0) then
    iCompletedSprocketCity = 1.0
end

--Empty profile.
if(i55ExplainedHerself == 0.0) then
    zProfileEntry:fnBlankProfile()

--Special: No profile.
elseif(iMainQuestBlock == 60.0) then
    zProfileEntry:fnSetDisplayName("JX???1")
    zProfileEntry:fnSetDescription(""..
        "Profile unavailable. Please descramble[BR]errant memory files. Code 7715-e.")

--55 explained the goal.
elseif(iSawWakeUpIntro == 0.0 and iFlashbackWakeup == 0.0) then
    zProfileEntry:fnSetDescription(""..
        "The enigmatic JX-101. Leader of one of[BR]the larger steam droid settlements on[BR]Regulus. She has arrest-on-sight[BR]notices for her on the network.[BR][BR]"..
        "55 believes she can be recruited to help[BR]overthrow the administration of[BR]Regulus City. She is located somewhere[BR]in the Tellurium Mines.")

--Met her, woke up in Sprocket City.
elseif(iCompletedSprocketCity == 0.0 and iFlashbackWakeup == 0.0) then
    zProfileEntry:fnSetDescription(""..
        "The enigmatic JX-101. Leader of one of[BR]the larger steam droid settlements on[BR]Regulus. She has arrest-on-sight[BR]notices for her on the network.[BR][BR]"..
        "Mother to SX-399 and leader of Fist of[BR]Tomorrow, she is stern but caring. Many[BR]steam droids respect her intellect and[BR]tactical knowledge.[BR][BR]"..
        "She hates golems and command units,[BR]and will likely not ally with Christine[BR]and 55 directly. There may be a way to[BR]her heart elsewhere.")

--Completed Sprocket City and she joined the rebellion.
elseif(iFlashbackWakeup == 0.0 and iFlashbackWakeup == 0.0) then
    zProfileEntry:fnSetDescription(""..
        "The enigmatic JX-101. Leader of one of[BR]the larger steam droid settlements on[BR]Regulus. She has arrest-on-sight[BR]notices for her on the network.[BR][BR]"..
        "Mother to SX-399 and leader of Fist of[BR]Tomorrow, she is stern but caring. Many[BR]steam droids respect her intellect and[BR]tactical knowledge.[BR][BR]"..
        "After Christine and 55 aided in the[BR]repair of her daughter, she has seen[BR]that some golems are worthy of respect.[BR]She is actively aiding the rebellion.")

--Final profile, changes based on Christine's alignment.
else
    --Normal:
    if(iChristineCorruptedEnding == 0.0) then
        zProfileEntry:fnSetDescription(""..
            "The enigmatic JX-101. Leader of one of[BR]the larger steam droid settlements on[BR]Regulus. She has arrest-on-sight[BR]notices for her on the network.[BR][BR]"..
            "Mother to SX-399 and leader of Fist of[BR]Tomorrow, she is stern but caring. Many[BR]steam droids respect her intellect and[BR]tactical knowledge.[BR][BR]"..
            "After Christine and 55 aided in the[BR]repair of her daughter, she has seen[BR]that some golems are worthy of respect.[BR]She is actively aiding the rebellion.")

    --Corrupted Christine:
    else
        zProfileEntry:fnSetDescription(""..
            "JX-101, leader of the steam droid[BR]faction Fist of Tomorrow.[BR][BR]"..
            "Retire on sight, priority zero.")
    end
end