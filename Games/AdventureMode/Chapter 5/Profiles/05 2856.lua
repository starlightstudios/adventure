-- |[ ======================================= Unit 2856 ======================================== ]|
--Appears after the player meets Vivify.

--Basic profile.
local zProfileEntry = JournalEntry:new("2856", "Unit 2856")
zProfileEntry:fnSetToProfile("Root/Images/Portraits/2856Dialogue/Neutral", 0, 0)

--Variables.
local iSawFirstEDGScene         = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawFirstEDGScene", "N")
local iStarted56Sequence        = VM_GetVar("Root/Variables/Chapter5/Scenes/iStarted56Sequence", "N")
local iMainQuestBlock           = VM_GetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N")
local iFlashbackWakeup          = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackWakeup", "N")
local iChristineCorruptedEnding = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineCorruptedEnding", "N")

-- |[ =================== Variations =================== ]|
--Empty profile.
if(iSawFirstEDGScene == 0.0) then
    zProfileEntry:fnBlankProfile()
    
--Special: No profile.
elseif(iMainQuestBlock == 60.0) then
    zProfileEntry:fnSetDisplayName("Unit ???6")
    zProfileEntry:fnSetDescription(""..
        "Profile unavailable. Please descramble[BR]errant memory files. Code 7715-e.")

--Encountered Vivify at least once.
elseif(iStarted56Sequence == 0.0) then
    zProfileEntry:fnSetDescription(""..
        "Command Unit 2856, Head of Research[BR]for Regulus City.[BR][BR]"..
        "Identical twin of 55, sharing her[BR]intelligence and ruthlessness, she is well[BR]regarded among the aristocracy of[BR]Regulus City. She gets results, and gets[BR]rid "..
        "of anyone in her way.[BR][BR]"..
        "She knows about Project Vivify, and[BR]was in the LRT facility when Christine[BR]and 55 were. What else she knows is[BR]uncertain.")

--After meeting 56 at the gala:
elseif(iFlashbackWakeup == 0.0) then
    zProfileEntry:fnSetDescription(""..
        "Command Unit 2856, Head of Research[BR]for Regulus City.[BR][BR]"..
        "Identical twin of 55, sharing her[BR]intelligence and ruthlessness, she is well[BR]regarded among the aristocracy of[BR]Regulus City. She gets results, and gets[BR]rid "..
        "of anyone in her way.[BR][BR]"..
        "She knows about Project Vivify, and[BR]was in the LRT facility when Christine[BR]and 55 were. Realizing that Project[BR]Vivify is a serious existential threat to[BR]the "..
        "city, she has temporarily allied with[BR]Christine and 55, as they are among the[BR]few units to survive an encounter with[BR]it.[BR][BR]"..
        "She is ill-tempered, aggressive, and[BR]endlessly cruel. She shouts and bullies[BR]others when she is inconvenienced.")

--Final profile, changes based on Christine's alignment.
else
    --Normal:
    if(iChristineCorruptedEnding == 0.0) then
        zProfileEntry:fnSetDescription(""..
            "Command Unit 2856, Head of Research[BR]for Regulus City.[BR][BR]"..
            "Identical twin of 55, sharing her[BR]intelligence and ruthlessness, she is well[BR]regarded among the aristocracy of[BR]Regulus City. She gets results, and gets[BR]rid "..
            "of anyone in her way.[BR][BR]"..
            "She knows about Project Vivify, and[BR]was in the LRT facility when Christine[BR]and 55 were. Realizing that Project[BR]Vivify is a serious existential threat to[BR]the "..
            "city, she has temporarily allied with[BR]Christine and 55, as they are among the[BR]few units to survive an encounter with[BR]it.[BR][BR]"..
            "She is ill-tempered, aggressive, and[BR]endlessly cruel. She shouts and bullies[BR]others when she is inconvenienced.[BR][BR]"..
            "She was acting against Vivify without the permission of the[BR]Administrator, and as such most of her activities are unknown to the[BR]rest of the administration. She "..
            "attempted to subvert Christine's will,[BR]but failed, and has been retired by her sister.")

    --Corrupted Christine:
    else
        zProfileEntry:fnSetDescription(""..
            "Command Unit 2856, Head of Research[BR]for Regulus City.[BR][BR]"..
            "Betrayed the Administrator by[BR]attempting to destroy Project Vivify[BR]without instruction. While normally the[BR]Administrator would pass final[BR]judgement, maintaining her cover[BR]"..
            "forced Unit 771852 to allow Unit 2855[BR]to retire the maverick.")
    end
end
