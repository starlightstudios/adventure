-- |[ ========================================== Psue ========================================== ]|
--VA VA VOOM!

--Basic profile.
local zProfileEntry = JournalEntry:new("Psue", "Psue")
zProfileEntry:fnSetToProfile("Root/Images/Portraits/Enemies/Electrosprite", -150, 0)

--Variables.
local iBecameElectrosprite      = VM_GetVar("Root/Variables/Chapter5/Scenes/iBecameElectrosprite", "N")
local iFinished198              = VM_GetVar("Root/Variables/Chapter5/Scenes/iFinished198", "N")
local iMainQuestBlock           = VM_GetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N")
local iFlashbackWakeup          = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackWakeup", "N")
local iChristineCorruptedEnding = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineCorruptedEnding", "N")

-- |[ =================== Variations =================== ]|
--Empty profile.
if(iFinished198 == 0.0) then
    zProfileEntry:fnBlankProfile()
    
--Special: No profile.
elseif(iMainQuestBlock == 60.0) then
    zProfileEntry:fnSetDisplayName("Ps??")
    zProfileEntry:fnSetDescription(""..
        "Profile unavailable. Please descramble[BR]errant memory files. Code 7715-e.")

--Quest completed.
elseif(iFinished198 == 1.0 and iFlashbackWakeup == 0.0) then
    zProfileEntry:fnSetDescription(""..
        "An electrosprite, her name is short for[BR]Pseudorandom.[BR][BR]"..
        "She was likely created in some old[BR]experiment in AI, and achieved[BR]sentience as a program inside the[BR]Regulus City Abductions Training[BR]Program. She gained self awareness and[BR]escaped "..
        "the program with Christine's[BR]help.[BR][BR]Since then, she has pledged to help[BR]Christine in her revolution, along with[BR]all the other electrosprites who were[BR]liberated alongside her.[BR][BR]"..
        "Due to the extraction process,[BR]electrosprites briefly share minds with[BR]the golem liberating them. Psue knows[BR]many things Christine does, though the[BR]process is not perfect. The two "..
        "are[BR]extremely compatible.")

--Final profile, changes based on Christine's alignment.
else
    --Normal:
    if(iChristineCorruptedEnding == 0.0) then
        zProfileEntry:fnSetDescription(""..
        "An electrosprite, her name is short for[BR]Pseudorandom.[BR][BR]"..
        "She was likely created in some old[BR]experiment in AI, and achieved[BR]sentience as a program inside the[BR]Regulus City Abductions Training[BR]Program. She gained self awareness and[BR]escaped "..
        "the program with Christine's[BR]help.[BR][BR]Since then, she has pledged to help[BR]Christine in her revolution, along with[BR]all the other electrosprites who were[BR]liberated alongside her.[BR][BR]"..
        "Due to the extraction process,[BR]electrosprites briefly share minds with[BR]the golem liberating them. Psue knows[BR]many things Christine does, though the[BR]process is not perfect. The two "..
        "are[BR]extremely compatible.")

    --Corrupted Christine:
    else
        zProfileEntry:fnSetDescription(""..
        "An electrosprite, her name is short for[BR]Pseudorandom.[BR][BR]"..
        "A rogue monstergirl of interest,[BR]electrosprites are capable hackers. If[BR]she cannot be reined in for study, she[BR]is to be terminated.")
    end
end