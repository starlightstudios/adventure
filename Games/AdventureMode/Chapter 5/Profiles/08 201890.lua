-- |[ ====================================== Unit 201890 ======================================= ]|
--Follower of Vivify.

--Basic profile.
local zProfileEntry = JournalEntry:new("201890", "201890")
zProfileEntry:fnSetToProfile("Root/Images/Portraits/Enemies/201890", 0, 0)

--Variables.
local iReachedBiolabs       = VM_GetVar("Root/Variables/Chapter5/Scenes/iReachedBiolabs", "N")
local iMetVivify            = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetVivify", "N")
local iMainQuestBlock       = VM_GetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N")
local iFlashbackWakeup      = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackWakeup", "N")
local iFlashbackStartFinale = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackStartFinale", "N")

-- |[ =================== Variations =================== ]|
--Empty profile.
if(iReachedBiolabs == 0.0) then
    zProfileEntry:fnBlankProfile()
    
--Special: No profile.
elseif(iMainQuestBlock == 60.0) then
    zProfileEntry:fnSetDisplayName("???8??")
    zProfileEntry:fnSetDescription(""..
        "Profile unavailable. Please descramble[BR]errant memory files. Code 7715-e.")

--Met her at the gala.
elseif(iMetVivify == 0.0 and iFlashbackStartFinale == 0.0 and iFlashbackWakeup == 0.0) then
    zProfileEntry:fnSetDisplayName("201890 (?)")
    zProfileEntry:fnSetDescription(""..
        "A unit who claims the name 201890,[BR]though that is not her designation in the[BR]database, if there ever was one.[BR][BR]"..
        "The most promising adherent of Project[BR]Vivify, and potentially her most[BR]dangerous follower. Claims to have[BR]secretly organized the Sunrise Gala to[BR]suit her purposes. Demonstrates a[BR]knowledge "..
        "of Regulan society better[BR]than most of its citizens, and an[BR]uncanny foresight.[BR][BR]"..
        "Physically, resembles a slave golem, but[BR]has grafted parts from other units onto[BR]herself, including organic parts that[BR]integrate with her systems.")

--Vivify freezes herself.
elseif(iFlashbackWakeup == 0.0) then
    zProfileEntry:fnSetDisplayName("201890 (?)")
    zProfileEntry:fnSetDescription(""..
        "A unit who claims the name 201890,[BR]though that is not her designation in the[BR]database, if there ever was one.[BR][BR]"..
        "The most promising adherent of Project[BR]Vivify, and potentially her most[BR]dangerous follower. Claims to have[BR]secretly organized the Sunrise Gala to[BR]suit her purposes. Demonstrates a[BR]knowledge "..
        "of Regulan society better[BR]than most of its citizens, and an[BR]uncanny foresight.[BR][BR]"..
        "Physically, resembles a slave golem, but[BR]has grafted parts from other units onto[BR]herself, including organic parts that[BR]integrate with her systems.[BR][BR]"..
        "Does not fully understand the teaching[BR]of Project Vivify, and harbours intense[BR]jealousy towards Christine for her[BR]effortless futuresight. She stands guard over Vivify's frozen form.")

--Final profile. Same as freezes.
else
    zProfileEntry:fnSetDisplayName("201890 (?)")
    zProfileEntry:fnSetDescription(""..
        "A unit who claims the name 201890,[BR]though that is not her designation in the[BR]database, if there ever was one.[BR][BR]"..
        "The most promising adherent of Project[BR]Vivify, and potentially her most[BR]dangerous follower. Claims to have[BR]secretly organized the Sunrise Gala to[BR]suit her purposes. Demonstrates a[BR]knowledge "..
        "of Regulan society better[BR]than most of its citizens, and an[BR]uncanny foresight.[BR][BR]"..
        "Physically, resembles a slave golem, but[BR]has grafted parts from other units onto[BR]herself, including organic parts that[BR]integrate with her systems.[BR][BR]"..
        "Does not fully understand the teaching[BR]of Project Vivify, and harbours intense[BR]jealousy towards Christine for her[BR]effortless futuresight. She stands guard over Vivify's frozen form.")
end