-- |[ ========================================= Night ========================================== ]|
--Night, a secrebot.

--Basic profile.
local zProfileEntry = JournalEntry:new("Night", "Night")
zProfileEntry:fnSetToProfile("Root/Images/Portraits/Night/Secrebot", 0, 0)

--Variables.
local i254Transformed           = VM_GetVar("Root/Variables/Chapter5/Scenes/i254Transformed", "N")
local i254FixedChristine        = VM_GetVar("Root/Variables/Chapter5/Scenes/i254FixedChristine", "N")
local i254Completed             = VM_GetVar("Root/Variables/Chapter5/Scenes/i254Completed", "N")
local iChristineCorruptedEnding = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineCorruptedEnding", "N")
local iMainQuestBlock           = VM_GetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N")

-- |[ =================== Variations =================== ]|
--Blank profile.
if(i254Transformed == 0.0) then
    zProfileEntry:fnBlankProfile()

--Special: No profile.
elseif(iMainQuestBlock == 60.0) then
    zProfileEntry:fnSetDisplayName("?R-?-?5")
    zProfileEntry:fnSetDescription(""..
        "Profile unavailable. Please descramble[BR]errant memory files. Code 7715-e.")

--Has met Night, Christine is a secrebot.
elseif(i254Transformed == 1.0 and i254FixedChristine == 0.0) then
    zProfileEntry:fnSetDisplayName("CR-1-15")
    zProfileEntry:fnSetDescription(""..
        "Secrebot CR-1-15. A secrebot serving in[BR]sector 254, presumably until a lord unit[BR]purchases her.[BR][BR]Manufactured recently, has no[BR]particular history.")

--Fixed Christine.
elseif(i254Transformed == 1.0 and i254FixedChristine == 1.0 and i254Completed == 0.0) then
    zProfileEntry:fnSetDisplayName("CR-1-15")
    zProfileEntry:fnSetDescription(""..
        "Secrebot CR-1-15. The 'rogue human'[BR]who Christine was captured trying to[BR]rescue, she has been transformed into a[BR]secrebot and is presumably working for[BR]Vifify and Odess against her will.")

--Finished sector 254.
else

    --Normal case:
    if(iChristineCorruptedEnding == 0.0) then
        zProfileEntry:fnSetToProfile("Root/Images/Portraits/Night/SecrebotHappy", 0, 0)
        zProfileEntry:fnSetDescription(""..
            "Night, also known as Secrebot CR-1-15.[BR]A rogue human released deliberately to[BR]be captured and transformed into a[BR]secrebot in order to help the security[BR]"..
            "forces figure out what Odess was up to.[BR][BR]"..
            "Like Christine, she had a dead organic[BR]mass in her cranial chassis that was[BR]controlling her actions. Now that she[BR]has been liberated, she has joined the[BR]"..
            "revolution and intends to help the other[BR]secrebots.[BR][BR]"..
            "She is a diligent worker and likely was[BR]before conversion. She seems to have[BR]taken well to life as a robot, and does[BR]not mind having to pretend to be a[BR]"..
            "dumb refurbished secrebot at times.[BR]She is even-tempered and friendly[BR]towards robots of all stations.[BR][BR]"..
            "If you see her around Sector 96, be sure to say hello!")

    --Corrupted case:
    else
        zProfileEntry:fnSetDisplayName("CR-1-15")
        zProfileEntry:fnSetDescription(""..
            "Secrebot CR-1-15, a maverick[BR]revolutionary. Elimination is likely not[BR]required.[BR][BR]Capture and reprogram.")
    end
end