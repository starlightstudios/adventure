-- |[ ========================================= Sophie ========================================= ]|
--Appears once Christine meets her in the repair bay.

--Basic profile.
local zProfileEntry = JournalEntry:new("Sophie", "Sophie")
zProfileEntry:fnSetToProfile("Root/Images/Portraits/SophieDialogue/Smirk", 0, 0)

--Variables.
local iFlashbackWakeup          = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackWakeup", "N")
local iMet55InBasement          = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InBasement", "N")
local iTalkedToSophie           = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
local iIsGalaTime               = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
local iFlashbackStartFinale     = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackStartFinale", "N")
local iChristineCorruptedEnding = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineCorruptedEnding", "N")
local iMainQuestBlock           = VM_GetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N")

-- |[ =================== Variations =================== ]|
--Empty profile.
if(iTalkedToSophie == 0.0) then
    zProfileEntry:fnBlankProfile()

--Special: No profile.
elseif(iMainQuestBlock == 60.0) then
    zProfileEntry:fnSetDisplayName("U?it 4??323")
    zProfileEntry:fnSetDescription(""..
        "Profile unavailable. Please descramble[BR]errant memory files. Code 7715-e.")

--Met her. Christine asks her out.
elseif(iMet55InBasement == 0.0) then
    zProfileEntry:fnSetDescription(""..
        "Slave Unit 499323, Maintenance and[BR]Repair, Sector 96. Her friends call her[BR]Sophie.[BR][BR]"..
        "Beautiful, kind, intelligent, and[BR]resourceful, Sophie connected very[BR]quickly with Christine and the two[BR]formed a relationship despite the[BR]cultural taboo of mixing model variants.[BR][BR]"..
        "She has been working in the repair bay[BR]for some time and is quite skilled at her[BR]job. Not having a lord golem above her[BR]meant she could dress as she liked and[BR]grow out her hair.")

--Pre-gala.
elseif(iIsGalaTime == 0.0) then
    zProfileEntry:fnSetDescription(""..
        "Slave Unit 499323, Maintenance and[BR]Repair, Sector 96. Her friends call her[BR]Sophie.[BR][BR]"..
        "Beautiful, kind, intelligent, and[BR]resourceful, Sophie connected very[BR]quickly with Christine and the two[BR]formed a relationship despite the[BR]cultural taboo of mixing model variants.[BR][BR]"..
        "She has been working in the repair bay[BR]for some time and is quite skilled at her[BR]job. Not having a lord golem above her[BR]meant she could dress as she liked and[BR]grow out her hair.[BR][BR]"..
        "She has agreed to help Christine and 55[BR]with their goal of a free, equal Regulus[BR]City. She fudges records and tells slave[BR]golems she trusts to spread the word.")

--Gala, pre-memory program.
elseif(iFlashbackWakeup == 0.0 and iMainQuestBlock < 60.0) then
    zProfileEntry:fnSetDescription(""..
        "Slave Unit 499323, Maintenance and[BR]Repair, Sector 96. Her friends call her[BR]Sophie.[BR][BR]"..
        "Beautiful, kind, intelligent, and[BR]resourceful, Sophie connected very[BR]quickly with Christine and the two[BR]formed a relationship despite the[BR]cultural taboo of mixing model variants.[BR][BR]"..
        "She has been working in the repair bay[BR]for some time and is quite skilled at her[BR]job. Not having a lord golem above her[BR]meant she could dress as she liked and[BR]grow out her hair.[BR][BR]"..
        "She has agreed to help Christine and 55[BR]with their goal of a free, equal Regulus[BR]City. She fudges records and tells slave[BR]golems she trusts to spread the word.[BR][BR]"..
        "She is deeply in love with Christine and means the world to her.")

--Final profile, changes based on Christine's alignment.
else
    --Normal:
    if(iChristineCorruptedEnding == 0.0) then
        zProfileEntry:fnSetDescription(""..
            "Slave Unit 499323, Maintenance and[BR]Repair, Sector 96. Her friends call her[BR]Sophie.[BR][BR]"..
            "Beautiful, kind, intelligent, and[BR]resourceful, Sophie connected very[BR]quickly with Christine and the two[BR]formed a relationship despite the[BR]cultural taboo of mixing model variants.[BR][BR]"..
            "She has been working in the repair bay[BR]for some time and is quite skilled at her[BR]job. Not having a lord golem above her[BR]meant she could dress as she liked and[BR]grow out her hair.[BR][BR]"..
            "She has agreed to help Christine and 55[BR]with their goal of a free, equal Regulus[BR]City. She fudges records and tells slave[BR]golems she trusts to spread the word.[BR][BR]"..
            "She is deeply in love with Christine and means the world to her.")

    --Corrupted Christine:
    else
        zProfileEntry:fnSetDisplayName("Unit 499323")
        zProfileEntry:fnSetPortrait("Root/Images/Portraits/SophieDialogue/Neutral", 0, 0)
        zProfileEntry:fnSetDescription(""..
            "Slave Unit 499323, Maintenance and[BR]Repair, Sector 96. Her friends call her[BR]Sophie.[BR][BR]"..
            "A maverick who turned against the[BR]Administrator at the behest of Unit[BR]2855 and Unit 771852. Unless the[BR]Administrator orders it, she is to be[BR]captured and interrogated.[BR][BR]"..
            "She is a non-combatant and opposed to[BR]violence. She could likely be convinced[BR]to surrender by Unit 771852.[BR][BR]"..
            "If possible, she is to be reprogrammed[BR]so as to allow Unit 771852 to continue[BR]her sexual relationship with 499323.")
    end
end