-- |[ ====================================== Sammy Davis ======================================= ]|
--ACTION MOTH!

--Basic profile.
local zProfileEntry = JournalEntry:new("Sammy Davis", "Sammy Davis")
zProfileEntry:fnSetToProfile("Root/Images/Portraits/Sammy/Neutral", 0, 0)

--Variables.
local iBiolabsFirstWatchMovie = VM_GetVar("Root/Variables/Chapter5/Scenes/iBiolabsFirstWatchMovie", "N")
local iMainQuestBlock         = VM_GetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N")
local iFlashbackWakeup        = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackWakeup", "N")

-- |[ =================== Variations =================== ]|
--Empty profile.
if(iBiolabsFirstWatchMovie == 0.0) then
    zProfileEntry:fnBlankProfile()
    
--Special: No profile.
elseif(iMainQuestBlock == 60.0) then
    zProfileEntry:fnSetDisplayName("S??my D??is")
    zProfileEntry:fnSetDescription(""..
        "Profile unavailable. Please descramble[BR]errant memory files. Code 7715-e.")

--Watched the movie.
elseif(iFlashbackWakeup == 0.0) then
    zProfileEntry:fnSetDescription(""..
        "Sammy Davis, star of videographs like[BR]'Some Moths Do', 'Some Moths Two',[BR]'Seven Kings of Yeto', and the[BR]edutainment hit 'Radiation and How to[BR]Not Eat It'.[BR][BR]"..
        "A moth girl from Pandemonium,[BR]originally a dancer and bouncer at a[BR]club in Jeffespeir. She was noticed for[BR]her unique combination of dancing and[BR]wrestling and cast "..
        "in cheesy action[BR]videographs. She is beloved by lord and[BR]slave alike for her sharp wit both on[BR]and off screen.[BR][BR]"..
        "It was not her idea to wear a bikini in[BR]almost all of her roles. That was the[BR]director. She can't help that she makes[BR]it work.[BR][BR]"..
        "She is currently planetside, wrapping[BR]production and editing of 'Some Moths Two', which should be available[BR]in a few months.")

--Final profile, does not change for alignment.
else
    zProfileEntry:fnSetDescription(""..
        "Sammy Davis, star of videographs like[BR]'Some Moths Do', 'Some Moths Two',[BR]'Seven Kings of Yeto', and the[BR]edutainment hit 'Radiation and How to[BR]Not Eat It'.[BR][BR]"..
        "A moth girl from Pandemonium,[BR]originally a dancer and bouncer at a[BR]club in Jeffespeir. She was noticed for[BR]her unique combination of dancing and[BR]wrestling and cast "..
        "in cheesy action[BR]videographs. She is beloved by lord and[BR]slave alike for her sharp wit both on[BR]and off screen.[BR][BR]"..
        "It was not her idea to wear a bikini in[BR]almost all of her roles. That was the[BR]director. She can't help that she makes[BR]it work.[BR][BR]"..
        "She is currently planetside, wrapping[BR]production and editing of 'Some Moths Two', which should be available[BR]in a few months.")
end