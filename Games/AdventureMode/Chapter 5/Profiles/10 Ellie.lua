-- |[ ========================================= Ellie ========================================== ]|
--The rebel golem.

--Basic profile.
local zProfileEntry = JournalEntry:new("Ellie", "Ellie")
zProfileEntry:fnSetToProfile("Root/Images/Portraits/Ellie/Neutral", 0, 0)

--Variables.
local iAquaticMeeting           = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticMeeting", "N")
local iMainQuestBlock           = VM_GetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N")
local iFlashbackWakeup          = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackWakeup", "N")
local iChristineCorruptedEnding = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineCorruptedEnding", "N")

-- |[ =================== Variations =================== ]|
--Empty profile.
if(iAquaticMeeting == 0.0) then
    zProfileEntry:fnBlankProfile()
    
--Special: No profile.
elseif(iMainQuestBlock == 60.0) then
    zProfileEntry:fnSetDisplayName("??np??")
    zProfileEntry:fnSetDescription(""..
        "Profile unavailable. Please descramble[BR]errant memory files. Code 7715-e.")

--Met Ellie.
elseif(iFlashbackWakeup == 0.0) then
    zProfileEntry:fnSetDescription(""..
        "Unit 405626, Janitorial Services,[BR]Sector Zero. Her friends call her Ellie.[BR][BR]"..
        "After seeing the videographs recorded[BR]by Christine to recruit for the rebel[BR]cause, she joined up and provided the[BR]rebels some access codes. She is one of[BR]the few lord "..
        "golems to volunteer.[BR][BR]"..
        "She is nervous, but kind hearted and[BR]hopes to be a great leader someday.")

--Final profile, changes with alignment.
else
    --Normal:
    if(iChristineCorruptedEnding == 0.0) then
        zProfileEntry:fnSetDescription(""..
            "Unit 405626, Janitorial Services,[BR]Sector Zero.[BR][BR]"..
            "After seeing the videographs recorded[BR]by Christine to recruit for the rebel[BR]cause, she joined up and provided the[BR]rebels some access codes. She is one of[BR]the few lord "..
            "golems to volunteer.[BR][BR]"..
            "She is nervous, but kind hearted and[BR]hopes to be a great leader someday.")

    --Corrupted Christine:
    else
        zProfileEntry:fnSetDisplayName("Unit 405626")
        zProfileEntry:fnSetDescription(""..
            "Unit 405626, Janitorial Services,[BR]Sector Zero.[BR][BR]"..
            "Maverick unit, betrayed the[BR]Administrator.[BR][BR]"..
            "Capture on sight. Likely punishment:[BR]Repurposement to drone unit.")
    end
end