-- |[ ========================================= SX-399 ========================================= ]|
--Appears once seen in the mines arguing with her mom.

--Basic profile.
local zProfileEntry = JournalEntry:new("SX-399", "SX-399")
zProfileEntry:fnSetToProfile("Root/Images/Portraits/SX-399YoungDroid/Neutral", 0, 0)

--Variables.
local iSawMinesECutsceneA       = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMinesECutsceneA", "N")
local iSXMet55                  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXMet55", "N")
local iSteamDroid250RepState    = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid250RepState", "N")
local iSpokeWithTT233           = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeWithTT233", "N")
local iSX399JoinsParty          = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
local iSXUpgradeQuest           = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
local iMainQuestBlock           = VM_GetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N")
local iFlashbackWakeup          = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackWakeup", "N")
local iChristineCorruptedEnding = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineCorruptedEnding", "N")

-- |[ =================== Variations =================== ]|
--Empty profile.
if(iSawMinesECutsceneA == 0.0) then
    zProfileEntry:fnBlankProfile()

--Special: No profile.
elseif(iMainQuestBlock == 60.0) then
    if(iSX399JoinsParty == 0.0) then
        --Uses defaults.
    else
        zProfileEntry:fnSetDisplayName("SX-???")
        zProfileEntry:fnSetPortrait("Root/Images/Portraits/SX-399Shocktrooper/Neutral", 0, 0)
    end
    zProfileEntry:fnSetDescription(""..
        "Profile unavailable. Please descramble[BR]errant memory files. Code 7715-e.")

--Saw her in the mines.
elseif(iSteamDroid250RepState < 3.0 and iFlashbackWakeup == 0.0) then
    zProfileEntry:fnSetDescription(""..
        "SX-399 is the daughter of JX-101 and[BR]the saboteur of equipment in the[BR]Tellurium Mines. She's been stealing[BR]parts to help repair her home in[BR]Sprocket City.[BR][BR]"..
        "Her relationship with JX-101 may be a[BR]route to recruiting the steam droids.")

--Talked JX-101 into letting her have a job, hasn't met 55 yet.
elseif(iSXMet55 == 0.0 and iFlashbackWakeup == 0.0) then
    zProfileEntry:fnSetDescription(""..
        "SX-399 is the daughter of JX-101 and[BR]the saboteur of equipment in the[BR]Tellurium Mines. She's been stealing[BR]parts to help repair her home in[BR]Sprocket City.[BR][BR]"..
        "Her mother normally keeps her in the[BR]city and under her watchful eye, so she[BR]has to sneak out in order to have any[BR]fun. Christine helped convince JX-101 to[BR]let her daughter "..
        "help the city, and she[BR]is now on guard just outside.")

--Met 55. She likes her!
elseif(iSpokeWithTT233 == 0.0 and iFlashbackWakeup == 0.0) then
    zProfileEntry:fnSetDescription(""..
        "SX-399 is the daughter of JX-101 and[BR]the saboteur of equipment in the[BR]Tellurium Mines. She's been stealing[BR]parts to help repair her home in[BR]Sprocket City.[BR][BR]"..
        "Her mother normally keeps her in the[BR]city and under her watchful eye, so she[BR]has to sneak out in order to have any[BR]fun. Christine helped convince JX-101 to[BR]let her daughter "..
        "help the city, and she[BR]is now on guard just outside.[BR][BR]"..
        "She has some sort of issue with her[BR]power core, and needs to spend[BR]enormous amounts of time in low-power[BR]mode. You'll need to speak to TT-233 to[BR]learn more.[BR][BR]"..
        "She *greatly* enjoys 55's company.")

--Spoke to TT-233.
elseif(iSXUpgradeQuest < 3.0 and iFlashbackWakeup == 0.0) then
    zProfileEntry:fnSetDescription(""..
        "SX-399 is the daughter of JX-101 and[BR]the saboteur of equipment in the[BR]Tellurium Mines. She's been stealing[BR]parts to help repair her home in[BR]Sprocket City.[BR][BR]"..
        "Her mother normally keeps her in the[BR]city and under her watchful eye, so she[BR]has to sneak out in order to have any[BR]fun. Christine helped convince JX-101 to[BR]let her daughter "..
        "help the city, and she[BR]is now on guard just outside.[BR][BR]"..
        "Her power core leaks horribly and is[BR]beyond the ability of any steam droid[BR]to repair.[BR][BR]"..
        "She *greatly* enjoys 55's company.")

--Upgraded, but not at the gala yet.
elseif(iSX399JoinsParty == 0.0 and iFlashbackWakeup == 0.0) then
    zProfileEntry:fnSetPortrait("Root/Images/Portraits/SX-399Shocktrooper/Flirt", 0, 0)
    zProfileEntry:fnSetDescription(""..
        "SX-399 is the daughter of JX-101 and[BR]the saboteur of equipment in the[BR]Tellurium Mines. She's been stealing[BR]parts to help repair her home in[BR]Sprocket City.[BR][BR]"..
        "Thanks to Christine, Sophie, and 55, she[BR]was successfully upgraded to a[BR]brand-new model she calls the[BR]'Steam Lord'. Requiring parts from[BR]golems and command units, she "..
        "is the[BR]envy of every steam droid. Hopefully,[BR]she is also a model to upgrade others.")

--Upgraded, joined party at the gala.
elseif(iFlashbackWakeup == 0.0 and iFlashbackWakeup == 0.0) then
    zProfileEntry:fnSetPortrait("Root/Images/Portraits/SX-399Shocktrooper/Flirt", 0, 0)
    zProfileEntry:fnSetDescription(""..
        "SX-399 is the daughter of JX-101 and[BR]the saboteur of equipment in the[BR]Tellurium Mines. She's been stealing[BR]parts to help repair her home in[BR]Sprocket City.[BR][BR]"..
        "Thanks to Christine, Sophie, and 55, she[BR]was successfully upgraded to a[BR]brand-new model she calls the[BR]'Steam Lord'. Requiring parts from[BR]golems and command units, she "..
        "is the[BR]envy of every steam droid. Hopefully,[BR]she is also a model to upgrade others.[BR][BR]"..
        "After receiving training and spending[BR]time fighting in the mines, she[BR]volunteered to aid Christine and 55's[BR]mission at the gala. Her extreme[BR]durability and love of close-quarters[BR]"..
        "combat makes her a powerful ally. She[BR]is, however, inexperienced.[BR][BR]"..
        "She really likes 55. Like, really. Calls her 'Cutie-5' and everything.")

--Final profile, changes based on Christine's alignment.
else
    --Normal:
    if(iChristineCorruptedEnding == 0.0) then
        zProfileEntry:fnSetPortrait("Root/Images/Portraits/SX-399Shocktrooper/Flirt", 0, 0)
        zProfileEntry:fnSetDescription(""..
            "SX-399 is the daughter of JX-101 and[BR]the saboteur of equipment in the[BR]Tellurium Mines. She's been stealing[BR]parts to help repair her home in[BR]Sprocket City.[BR][BR]"..
            "Thanks to Christine, Sophie, and 55, she[BR]was successfully upgraded to a[BR]brand-new model she calls the[BR]'Steam Lord'. Requiring parts from[BR]golems and command units, she "..
            "is the[BR]envy of every steam droid. Hopefully,[BR]she is also a model to upgrade others.[BR][BR]"..
            "After receiving training and spending[BR]time fighting in the mines, she[BR]volunteered to aid Christine and 55's[BR]mission at the gala. Her extreme[BR]durability and love of close-quarters[BR]"..
            "combat makes her a powerful ally. She[BR]is, however, inexperienced.[BR][BR]"..
            "She really likes 55. Like, really. Calls her 'Cutie-5' and everything.")

    --Corrupted Christine:
    else
        zProfileEntry:fnSetPortrait("Root/Images/Portraits/SX-399Shocktrooper/Neutral", 0, 0)
        zProfileEntry:fnSetDescription(""..
            "SX-399, daughter of JX-101. Her body[BR]has been upgraded using stolen parts[BR]and novel engineering upgrades.[BR][BR]"..
            "Retire on sight, priority zero. Body is to[BR]be studied for engineering novelties,[BR]then scrapped.")
    end
end