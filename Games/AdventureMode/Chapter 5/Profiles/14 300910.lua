-- |[ ====================================== Unit 300910 ======================================= ]|
--Head of Serenity Crater Observatory.

--Basic profile.
local zProfileEntry = JournalEntry:new("Unit 300910", "Unit 300910")
zProfileEntry:fnSetToProfile("Root/Images/Portraits/Enemies/Doll", 0, 0)

--Variables.
local iSawSerenityIntro         = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawSerenityIntro", "N")
local iCompletedSerenity        = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
local iMainQuestBlock           = VM_GetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N")
local iFlashbackWakeup          = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackWakeup", "N")
local iChristineCorruptedEnding = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineCorruptedEnding", "N")
local iFlashbackStartFinale     = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackStartFinale", "N")

-- |[ =================== Variations =================== ]|
--Empty profile.
if(iSawSerenityIntro == 0.0) then
    zProfileEntry:fnBlankProfile()
    
--Special: No profile.
elseif(iMainQuestBlock == 60.0) then
    zProfileEntry:fnSetDisplayName("Uni? 3??91?")
    zProfileEntry:fnSetDescription(""..
        "Profile unavailable. Please descramble[BR]errant memory files. Code 7715-e.")

--Met 300910.
elseif(iCompletedSerenity == 0.0 and iFlashbackStartFinale == 0.0 and iFlashbackWakeup == 0.0) then
    zProfileEntry:fnSetDescription(""..
        "Unit 300910, Head of Serenity Crater[BR]Observatory. Her friends call her Eileen.[BR][BR]"..
        "One of the command units converted to[BR]oversee the city's expansion, she was[BR]born in the breeding program and[BR]selected due to exemplary[BR]academic scores. Her interests include[BR]astronomy, physics, "..
        "calculus, and[BR]logistics.[BR][BR]"..
        "She is much more approachable than[BR]most other command units, possibly[BR]owing to being less reprogrammed than[BR]most. Breeding program candidates[BR]already have most of the training[BR]necessary "..
        "for their roles. Even still, she[BR]is viewed with suspicion by other[BR]command units, and dislikes their[BR]politicking.[BR][BR]"..
        "At some point in the past, Unit 2855 assisted the observatory greatly.[BR]She and the workers there are very friendly towards 55, even if 55[BR]doesn't remember them.")

--Finished the crater.
elseif(iFlashbackWakeup == 0.0 and iFlashbackStartFinale == 0.0 and iFlashbackWakeup == 0.0) then
    zProfileEntry:fnSetDescription(""..
        "Unit 300910, Head of Serenity Crater[BR]Observatory. Her friends call her Eileen.[BR][BR]"..
        "One of the command units converted to[BR]oversee the city's expansion, she was[BR]born in the breeding program and[BR]selected due to exemplary[BR]academic scores. Her interests include[BR]astronomy, physics, "..
        "calculus, and[BR]logistics.[BR][BR]"..
        "She is much more approachable than[BR]most other command units, possibly[BR]owing to being less reprogrammed than[BR]most. Breeding program candidates[BR]"..
        "already have most of the training[BR]necessary for their roles. Even still, she[BR]is viewed with suspicion by other[BR]command units, and dislikes their[BR]politicking.[BR][BR]"..
        "At some point in the past, Unit 2855 assisted the observatory greatly.[BR]She and the workers there are very friendly towards 55, even if 55[BR]doesn't remember them.[BR][BR]"..
        "She has vowed to aid Christine and 55 in their revolution, thanks to[BR]Christine's help at the observatory.")

--Final profile, does not change with alignment.
else
    --Normal:
    if(iChristineCorruptedEnding == 0.0) then
        if(iCompletedSerenity == 0.0) then
            zProfileEntry:fnSetDescription(""..
                "Unit 300910, Head of Serenity Crater[BR]Observatory. Her friends call her Eileen.[BR][BR]"..
                "One of the command units converted to[BR]oversee the city's expansion, she was[BR]born in the breeding program and[BR]selected due to exemplary[BR]academic scores. Her interests include[BR]astronomy, "..
                "physics, calculus, and[BR]logistics.[BR][BR]"..
                "She is much more approachable than[BR]most other command units, possibly[BR]owing to being less reprogrammed than[BR]most. Breeding program candidates[BR]"..
                "already have most of the training[BR]necessary for their roles. Even still, she[BR]is viewed with suspicion by other[BR]command units, and dislikes their[BR]politicking.[BR][BR]"..
                "At some point in the past, Unit 2855 assisted the observatory greatly.[BR]She and the workers there are very friendly towards 55, even if 55[BR]doesn't remember them.")
        else
            zProfileEntry:fnSetDescription(""..
                "Unit 300910, Head of Serenity Crater[BR]Observatory. Her friends call her Eileen.[BR][BR]"..
                "One of the command units converted to[BR]oversee the city's expansion, she was[BR]born in the breeding program and[BR]selected due to exemplary[BR]academic scores. Her interests include[BR]astronomy, "..
                "physics, calculus, and[BR]logistics.[BR][BR]"..
                "She is much more approachable than[BR]most other command units, possibly[BR]owing to being less reprogrammed than[BR]most. Breeding program candidates[BR]"..
                "already have most of the training[BR]necessary for their roles. Even still, she[BR]is viewed with suspicion by other[BR]command units, and dislikes their[BR]politicking.[BR][BR]"..
                "At some point in the past, Unit 2855 assisted the observatory greatly.[BR]She and the workers there are very friendly towards 55, even if 55[BR]doesn't remember them.[BR][BR]"..
                "She has vowed to aid Christine and 55 in their revolution, thanks to[BR]Christine's help at the observatory.")
        end

    --Corrupted Christine:
    else
        if(iCompletedSerenity == 0.0) then
            zProfileEntry:fnSetDescription(""..
                "Unit 300910, Head of Serenity Crater[BR]Observatory.[BR][BR]"..
                "Errant unit in need of re-education.[BR]Capture and force to submit to loyalty[BR]program. Priority one.")
        else
            zProfileEntry:fnSetDescription(""..
                "Unit 300910, Head of Serenity Crater[BR]Observatory.[BR][BR]"..
                "Maverick who has betrayed the[BR]Administrator. Retire on sight.[BR]Priority zero.")
        end
    end
end