-- |[ ======================================== Christine ======================================== ]|
--Christine! The most complex profile, changes dialogue based on her job. Always appears.
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

--Basic profile.
local zProfileEntry = JournalEntry:new("Christine", "Christine")
zProfileEntry:fnSetToProfile("Root/Images/Portraits/ChristineDialogue/Golem|Smirk", 0, 0)

--Other variables.
local iFlashbackStartFinale     = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackStartFinale", "N")
local iChristineCorruptedEnding = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineCorruptedEnding", "N")
local iMainQuestBlock           = VM_GetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N")
local i254Transformed           = VM_GetVar("Root/Variables/Chapter5/Scenes/i254Transformed", "N")
local i254FixedChristine        = VM_GetVar("Root/Variables/Chapter5/Scenes/i254FixedChristine", "N")
local iBiolabsFinale            = VM_GetVar("Root/Variables/Chapter5/Scenes/iBiolabsFinale", "N")

-- |[ ============= Form Specific Profiles ============= ]|
--Christine as a male is only at the start of the chapter.
if(sChristineForm == "Male") then
    zProfileEntry:fnSetDisplayName("Chris")
    zProfileEntry:fnSetPortrait("Root/Images/Portraits/ChristineDialogue/Male|Neutral", 0, 0)
    zProfileEntry:fnSetDescription("Chris Dormer, professor of English at an[BR]all-girl's boarding school in the United[BR]Kingdom. Born into a wealthy family[BR]with a history of military service to the[BR]crown "..
                                   "stretching back centuries, Chris[BR]is one of the few men in the family to[BR]not pursue a career in the army or navy.[BR][BR]"..
                                   "Inexplicably, he woke up in an airlock in[BR]some sort of moon base on 'Regulus'.[BR]There are broken robots all over the[BR]place and a very stern woman on the[BR]intercom yelling at him.")

--Golem. Has a few subcases based on the plot flags.
elseif(sChristineForm == "Golem") then

    --Variables.
    local iTalkedToSophie      = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
    local iMet55InLowerRegulus = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N")

    --Function-assignment pending:
    if(iTalkedToSophie == 0.0) then
        zProfileEntry:fnSetDisplayName("Unit 771852")
        zProfileEntry:fnSetPortrait("Root/Images/Portraits/ChristineDialogue/Golem|Serious", 0, 0)
        zProfileEntry:fnSetDescription(""..
            "Unit 771852, function assignment[BR]pending. Transformed in the Cryogenics[BR]facility from a human named Chris[BR]Dormer of Earth.[BR][BR]"..
            "Her power core is presently in override[BR]mode, compelling her to locate a[BR]superior unit or assignment terminal[BR]and receive a function assignment.[BR][BR]"..
            "She is a lord golem, containing most of[BR]the same parts and programs as the[BR]slave golems, but less overall shielding[BR]from hazardous environments. Like all[BR]golems, "..
            "she is extremely durable, strong,[BR]and intelligent relative to the human[BR]base stock.[BR][BR]"..
            "Despite her lord pedigree, she is a[BR]capable warrior, wielding an[BR]electrospear. This form of spear has a[BR]battery attached to its point to allow discharge. It can be swapped "..
            "out[BR]with other types of spears to inflict other kinds of damage.[BR][BR]"..
            "Whenever any spear attack is used, 'Spearguard' is triggered, [BR]increasing her defense. She can protect allies and repair the injured.")
    
    --Function assignment get, but not a rebel yet.
    elseif(iTalkedToSophie == 1.0 and iMet55InLowerRegulus == 0.0) then
        zProfileEntry:fnSetPortrait("Root/Images/Portraits/ChristineDialogue/Golem|Smirk", 0, 0)
        zProfileEntry:fnSetDescription(""..
            "Unit 771852, repair unit, sector 96.[BR]Formerly Chris Dormer of Earth, she[BR]was transformed in the Cryogenics[BR]facility on Regulus.[BR][BR]"..
            "She has been assigned as Lord Golem of[BR]Maintenance and Repair, Sector 96.[BR][BR]Shortly after assignment, she began[BR]pursuing a relationship with Sophie,[BR]Unit 499323.[BR][BR]"..
            "She is a lord golem, containing most of[BR]the same parts and programs as the[BR]slave golems, but less overall shielding[BR]from hazardous environments. Like all[BR]golems, "..
            "she is extremely durable, strong,[BR]and intelligent relative to the human[BR]base stock.[BR][BR]"..
            "Despite her lord pedigree, she is a[BR]capable warrior, wielding an electrospear. This form of spear has a[BR]battery attached to its point to allow discharge. It can be swapped "..
            "out[BR]with other types of spears to inflict other kinds of damage.[BR][BR]"..
            "Whenever any spear attack is used, 'Spearguard' is triggered, [BR]increasing her defense. She can protect allies and repair the injured.")
    
    --Met 55, became a maverick. This is the profile used for most of the chapter.
    else
        zProfileEntry:fnSetPortrait("Root/Images/Portraits/ChristineDialogue/Golem|Smirk", 0, 0)
        zProfileEntry:fnSetDescription(""..
            "Unit 771852, repair unit, sector 96.[BR]Formerly Chris Dormer of Earth, she[BR]was transformed in the Cryogenics[BR]facility on Regulus.[BR][BR]"..
            "She has been assigned as Lord Golem of[BR]Maintenance and Repair, Sector 96.[BR][BR]Shortly after assignment, she began[BR]pursuing a relationship with Sophie,[BR]Unit 499323.[BR][BR]"..
            "She is a lord golem, containing most of[BR]the same parts and programs as the[BR]slave golems, but less overall shielding[BR]from hazardous environments. Like all[BR]golems, "..
            "she is extremely durable, strong,[BR]and intelligent relative to the human[BR]base stock.[BR][BR]"..
            "Despite her lord pedigree, she is a[BR]capable warrior, wielding an electrospear. This form of spear has a[BR]battery attached to its point to allow discharge. It can be swapped "..
            "out[BR]with other types of spears to inflict other kinds of damage.[BR][BR]"..
            "Because of irregularities in the Cryogenics facility concerning Unit[BR]2855, she is a maverick working to undermine the administration of[BR]Regulus City.")
    
    end

--Human.
elseif(sChristineForm == "Human") then
    
    --Normal case:
    if(iMainQuestBlock ~= 60) then
        zProfileEntry:fnSetPortrait("Root/Images/Portraits/ChristineDialogue/Smirk", 0, 0)
        zProfileEntry:fnSetDescription(""..
        "Unit 771852, repair unit, sector 96.[BR]Formerly Chris Dormer of Earth, she[BR]was transformed in the Cryogenics[BR]facility on Regulus.[BR][BR]"..
        "She has been assigned as Lord Golem of[BR]Maintenance and Repair, Sector 96.[BR][BR]Shortly after assignment, she began[BR]pursuing a relationship with Sophie,[BR]Unit 499323.[BR][BR]"..
        
        "She can use her runestone, passed down[BR]in her family since time immemorial, to[BR]transform. As a human, she is not as[BR]durable as her golem form. She is,[BR]however, notably "..
        "lighter and more agile.[BR][BR]"..
        
        "Despite her lord pedigree, she is a fine[BR] warrior, wielding an electrospear.[BR]This form of spear has a battery[BR]attached to its point to allow discharge. It can be swapped "..
        "out with[BR]other types of spears to inflict other kinds of damage.[BR][BR]"..
        "Because of irregularities in the Cryogenics facility concerning Unit[BR]2855, she is a maverick working to undermine the administration of[BR]Regulus City.")

    --Scrambled!
    else
        zProfileEntry:fnSetDisplayName("???")
        zProfileEntry:fnSetPortrait("Root/Images/Portraits/ChristineDialogue/Smirk", 0, 0)
        zProfileEntry:fnSetDescription("This is you? Maybe.[BR]Data unavailable.[BR]Error code K-L-C-C110.")

    end

--Darkmatter.
elseif(sChristineForm == "Darkmatter") then
    zProfileEntry:fnSetPortrait("Root/Images/Portraits/ChristineDialogue/Darkmatter|Smirk", 0, 0)
    zProfileEntry:fnSetDescription(""..
        "Unit 771852, repair unit, sector 96.[BR]Formerly Chris Dormer of Earth, she[BR]was transformed in the Cryogenics[BR]facility on Regulus.[BR][BR]"..
        "She has been assigned as Lord Golem of[BR]Maintenance and Repair, Sector 96.[BR][BR]Shortly after assignment, she began[BR]pursuing a relationship with Sophie,[BR]Unit 499323.[BR][BR]"..
        
        "She can use her runestone, passed down[BR]in her family since time immemorial, to[BR]transform. Due to an encounter at[BR]Serenity Crater, she can transform into[BR]a darkmatter girl. "..
        "She is not as[BR]experienced with that body as her[BR]human or golem bodies, but is durable[BR]and agile in it.[BR][BR]"..
        
        "Despite her lord pedigree, she is a fine warrior, wielding an[BR]electrospear. This form of spear has a battery attached to its point to[BR]allow discharge. It can be swapped "..
        "out with other types of spears to[BR]inflict other kinds of damage.[BR][BR]"..
        "Because of the crimes of its rulers, she is a maverick working to[BR]undermine the administration of Regulus City.")

--Latex Drone.
elseif(sChristineForm == "LatexDrone") then

    --Variables.
    local iManufactoryDepopulated = VM_GetVar("Root/Variables/Chapter5/Scenes/iManufactoryDepopulated", "N")
    local iManuReassigned         = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuReassigned", "N")

    --Manufactory Drone Case
    if(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0) then
        zProfileEntry:fnSetDisplayName("Unit 772890")
        zProfileEntry:fnSetPortrait("Root/Images/Portraits/ChristineDialogue/Latex|Neutral", 0, 0)
        zProfileEntry:fnSetDescription(""..
            "Unit 772890, drone unit, sector 99.[BR]Wholly unremarkable drone. Unit was [BR]reassigned to the sector unexpectedly[BR]and is tasked with simple support[BR]assignments until she can be brought[BR]up to code.")
    
    --Normal Case
    else
        zProfileEntry:fnSetPortrait("Root/Images/Portraits/ChristineDialogue/Latex|Offended", 0, 0)
        zProfileEntry:fnSetDescription(""..
            "Unit 771852, repair unit, sector 96.[BR]Formerly Chris Dormer of Earth, she[BR]was transformed in the Cryogenics[BR]facility on Regulus.[BR][BR]"..
            "She has been assigned as Lord Golem of[BR]Maintenance and Repair, Sector 96.[BR][BR]Shortly after assignment, she began[BR]pursuing a relationship with Sophie,[BR]Unit 499323.[BR][BR]"..
            
            "She can use her runestone, passed down[BR]in her family since time immemorial, to[BR]transform. As a drone unit, she is[BR]considerably less vulnerable to shocks[BR]than her normal golem form, "..
            "but more[BR]vulnerable to piercing damage. Her[BR]mental inhibitor chip is deactivated, so[BR]she retains normal intellect.[BR][BR]"..
            
            "Despite her lord pedigree, she is a fine warrior, wielding an[BR]electrospear. This form of spear has a battery attached to its point to[BR]allow discharge. It can be swapped "..
            "out with other types of spears to[BR]inflict other kinds of damage.[BR][BR]"..
            "Because of the crimes of its rulers, she is a maverick working to[BR]undermine the administration of Regulus City.")
    end

--Steam Droid.
elseif(sChristineForm == "SteamDroid") then
    zProfileEntry:fnSetPortrait("Root/Images/Portraits/ChristineDialogue/SteamDroid|Smirk", 0, 0)
    zProfileEntry:fnSetDescription(""..
        "Unit 771852, repair unit, sector 96.[BR]Formerly Chris Dormer of Earth, she[BR]was transformed in the Cryogenics[BR]facility on Regulus.[BR][BR]"..
        "She has been assigned as Lord Golem of[BR]Maintenance and Repair, Sector 96.[BR][BR]Shortly after assignment, she began[BR]pursuing a relationship with Sophie,[BR]Unit 499323.[BR][BR]"..
        
        "She can use her runestone, passed down[BR]in her family since time immemorial, to[BR]transform. With SX-399's help, she can[BR]become a steam droid. While the form[BR]is not as advanced as"..
        "her golem body,[BR]the heat regulators make her[BR]very resistant to flames and her natural[BR]strength does the rest.[BR][BR]"..
        
        "Despite her lord pedigree, she is a fine warrior, wielding an[BR]electrospear. This form of spear has a battery attached to its point to[BR]allow discharge. It can be swapped "..
        "out with other types of spears to[BR]inflict other kinds of damage.[BR][BR]"..
        "Because of the crimes of its rulers, she is a maverick working to[BR]undermine the administration of Regulus City.")

--Raiju.
elseif(sChristineForm == "Raiju") then
    zProfileEntry:fnSetPortrait("Root/Images/Portraits/ChristineDialogue/RaijuClothes|Laugh", 0, 0)
    zProfileEntry:fnSetDescription(""..
        "Unit 771852, repair unit, sector 96.[BR]Formerly Chris Dormer of Earth, she[BR]was transformed in the Cryogenics[BR]facility on Regulus.[BR][BR]"..
        "She has been assigned as Lord Golem of[BR]Maintenance and Repair, Sector 96.[BR][BR]Shortly after assignment, she began[BR]pursuing a relationship with Sophie,[BR]Unit 499323.[BR][BR]"..
        
        "She can use her runestone, passed down[BR]in her family since time immemorial, to[BR]transform. At the Raiju Ranch, she[BR]became a Raiju with the enthusiastic[BR]help of the locals. "..
        "Her furry self can[BR]shock enemies and is resistant to[BR]shocks in turn.[BR][BR]"..
        
        "Despite her lord pedigree, she is a[BR]fine warrior, wielding an electrospear. This form of spear has a[BR]battery attached to its point to allow discharge. It can be swapped[BR]"..
        "out with other types of spears to inflict other kinds of damage.[BR][BR]"..
        "Because of the crimes of its rulers, she is a maverick working to[BR]undermine the administration of Regulus City.")

--Electrosprite.
elseif(sChristineForm == "Electrosprite") then
    zProfileEntry:fnSetPortrait("Root/Images/Portraits/ChristineDialogue/Electrosprite|Smirk", 0, 0)
    zProfileEntry:fnSetDescription(""..
        "Unit 771852, repair unit, sector 96.[BR]Formerly Chris Dormer of Earth, she[BR]was transformed in the Cryogenics[BR]facility on Regulus.[BR][BR]"..
        "She has been assigned as Lord Golem of[BR]Maintenance and Repair, Sector 96.[BR][BR]Shortly after assignment, she began[BR]pursuing a relationship with Sophie,[BR]Unit 499323.[BR][BR]"..
        
        "She can use her runestone, passed down[BR]in her family since time immemorial, to[BR]transform. Due to a mishap involving[BR]living electricity AIs in a training[BR]program, she can "..
        "become an[BR]electrosprite. This form can shock[BR]enemies easily and is very resistant to[BR]electricity.[BR][BR]"..
        
        "Despite her lord pedigree, she is a fine warrior, wielding an[BR]electrospear. This form of spear has a battery attached to its point to[BR]allow discharge. It can be swapped "..
        "out with other types of spears to[BR]inflict other kinds of damage.[BR][BR]"..
        "Because of the crimes of its rulers, she is a maverick working to[BR]undermine the administration of Regulus City.")

--Eldritch.
elseif(sChristineForm == "Eldritch") then
    zProfileEntry:fnSetPortrait("Root/Images/Portraits/ChristineDialogue/Eldritch|Neutral", 0, 0)
    zProfileEntry:fnSetDescription(""..
        "Unit 771852, repair unit, sector 96.[BR]Formerly Chris Dormer of Earth, she[BR]was transformed in the Cryogenics[BR]facility on Regulus.[BR][BR]"..
        "She has been assigned as Lord Golem of[BR]Maintenance and Repair, Sector 96.[BR][BR]Shortly after assignment, she began[BR]pursuing a relationship with Sophie,[BR]Unit 499323.[BR][BR]"..
        
        "Encounters with Project Vivify and her[BR]adherents have transformed Christine[BR]into one of their followers, the so-called[BR]'dreamers'. Technically dead, "..
        "her brain[BR]is always in a state of sleep. She is[BR]resistant to most damage and capable of[BR]some disturbing attacks.[BR][BR]"..
        
        "Despite her lord pedigree, she is a[BR]fine warrior, wielding an electrospear. This form of spear has a[BR]battery attached to its point to allow discharge. It can be swapped[BR]"..
        "out with other types of spears to inflict other kinds of damage.[BR][BR]"..
        "Because of the crimes of its rulers, she is a maverick working to[BR]undermine the administration of Regulus City.")

--Doll.
elseif(sChristineForm == "Doll") then

    --NC+ only: Hasn't started the memory sequence so just have a normal set.
    if(iBiolabsFinale == 0.0) then
        zProfileEntry:fnSetPortrait("Root/Images/Portraits/ChristineDialogue/Doll|Smirk", 0, 0)
        zProfileEntry:fnSetDescription(""..
            "Unit 771852, repair unit, sector 96.[BR]Formerly Chris Dormer of Earth, she[BR]was transformed in the Cryogenics[BR]facility on Regulus.[BR][BR]"..
            "She has been assigned as Lord Golem of[BR]Maintenance and Repair, Sector 96.[BR][BR]Shortly after assignment, she began[BR]pursuing a relationship with Sophie,[BR]Unit 499323.[BR][BR]"..
            
            "Through time shenanigans or, I dunno,[BR]magic? She's a command unit. Try not[BR]to talk about it too much, you might[BR]embarass her.[BR][BR]"..
        
            "Despite her lord pedigree, she is a fine[BR]warrior, wielding an electrospear. This[BR]form of spear has a battery attached to[BR]its point to allow discharge. It can be[BR]swapped "..
            "out with other types of spears to inflict other kinds of[BR]damage.[BR][BR]"..
            "Because of the crimes of its rulers, she is a maverick working to[BR]undermine the administration of Regulus City.")

    --Still hasn't woken up:
    elseif(iFlashbackStartFinale == 0.0) then
        zProfileEntry:fnSetDisplayName("Unit Zero")
        zProfileEntry:fnSetPortrait("Root/Images/Portraits/ChristineDialogue/Doll|Neutral", 0, 0)
        zProfileEntry:fnSetDescription(""..
            "Command Unit Zero, the first artificially[BR]created human-grade intelligence[BR]placed in a command unit body.[BR]Specially created by the administrator[BR]of Regulus City "..
            "and given a runestone[BR]to allow observation of the people of[BR]'Earth'. Her mission was to observe the[BR]people there and report back all[BR]available data for the "..
            "Cause of Science.[BR][BR]"..
            "Something, however, went wrong, and[BR]Unit Zero lost her memories of her true[BR]self. After much struggle, she has[BR]returned to the administrator and her[BR]true form "..
            "to fulfill her true purpose. All[BR]she needs to do is unscramble her[BR]memory files and exit the diagnostics[BR]program.")

    --Normal:
    elseif(iChristineCorruptedEnding == 0.0) then
        zProfileEntry:fnSetPortrait("Root/Images/Portraits/ChristineDialogue/Doll|Smirk", 0, 0)
        zProfileEntry:fnSetDescription(""..
            "Unit 771852, repair unit, sector 96.[BR]Formerly Chris Dormer of Earth, she[BR]was transformed in the Cryogenics[BR]facility on Regulus.[BR][BR]"..
            "She has been assigned as Lord Golem of[BR]Maintenance and Repair, Sector 96.[BR][BR]Shortly after assignment, she began[BR]pursuing a relationship with Sophie,[BR]Unit 499323.[BR][BR]"..
            
            "After a false betrayal by her best[BR]friend, Unit 2855, she was transformed[BR]into a command unit in order to gain the[BR]information necessary to track down[BR]and destroy the administrator "..
            "of[BR]Regulus City. This form gives her[BR]superior intellect, reflexes, strength,[BR]and durability.[BR][BR]"..
        
            "Despite her lord pedigree, she is a fine warrior, wielding an[BR]electrospear. This form of spear has a battery attached to its point to[BR]allow discharge. It can be swapped "..
            "out with other types of spears to[BR]inflict other kinds of damage.[BR][BR]")

    --Corrupted:
    else
        zProfileEntry:fnSetDisplayName("Unit 771852")
        zProfileEntry:fnSetPortrait("Root/Images/Portraits/ChristineDialogue/Doll|Serious", 0, 0)
        zProfileEntry:fnSetDescription(""..
            "Command Unit 771852, formerly Chris[BR]Dormer of Earth. After a false betrayal[BR]by her comrade, Unit 2855, she was[BR]transformed into a command unit.[BR][BR]"..
            "During reprogramming, she was[BR]overcome by the loyalty programs[BR]created by the Administrator, and has[BR]voluntarily subsumed all personal[BR]willpower to the Administrator.[BR][BR]"..
            "She is now a mere puppet, doing as she[BR]is told.[BR][BR]"..
            "To protect the administrator, she is[BR]pretending to be her old self, intent on[BR]betraying the mavericks.")
    end

--Secrebot.
elseif(sChristineForm == "Secrebot") then

    --During the quest, Christine is reprogrammed.
    if(i254Transformed == 1.0 and i254FixedChristine == 0.0) then
        zProfileEntry:fnSetDisplayName("CR-1-16")
        zProfileEntry:fnSetPortrait("Root/Images/Portraits/ChristineDialogue/Secrebot|Serious", 0, 0)
        zProfileEntry:fnSetDescription(""..
            "Secrebot CR-1-16, produced in Sector[BR]254. A mostly mindless secrebot in the[BR]second-wave development with[BR]advanced AI able to perform a variety[BR]of complicated tasks, from repair to[BR]security "..
            "to creative work, and more![BR][BR]At least that's what she thinks. In[BR]reality, Christine got turned into a[BR]secrebot and reprogrammed, and Sophie[BR]is trying desperately to fix her.")

    --Normal case.
    else
        zProfileEntry:fnSetPortrait("Root/Images/Portraits/ChristineDialogue/Secrebot|Neutral", 0, 0)
        zProfileEntry:fnSetDescription(""..
            "Unit 771852, repair unit, sector 96.[BR]Formerly Chris Dormer of Earth, she[BR]was transformed in the Cryogenics[BR]facility on Regulus.[BR][BR]"..
            "She has been assigned as Lord Golem of[BR]Maintenance and Repair, Sector 96.[BR][BR]Shortly after assignment, she began[BR]pursuing a relationship with Sophie,[BR]Unit 499323.[BR][BR]"..
            "She is a lord golem, containing most of[BR]the same parts and programs as the[BR]slave golems, but less overall shielding[BR]from hazardous environments. Like all[BR]golems, "..
            "she is extremely durable, strong,[BR]and intelligent relative to the human[BR]base stock.[BR][BR]"..
            "Despite her lord pedigree, she is a[BR]capable warrior, wielding an electrospear. This form of spear has a[BR]battery attached to its point to allow discharge. It can be swapped "..
            "out[BR]with other types of spears to inflict other kinds of damage.[BR][BR]"..
            "She is presently using the low-profile form of a secrebot to go about[BR]her activities, which keeps the attention off her as every other[BR]robot assumes she's a mostly mindless menial task unit.")
    end
end