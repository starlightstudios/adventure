-- |[ ======================================== Tiffany ========================================= ]|
--Appears since she talks on the intercom at the start of the chapter. Name and appearance change though.

--Basic profile.
local zProfileEntry = JournalEntry:new("Tiffany", "Unit 2855")
zProfileEntry:fnSetToProfile("Root/Images/Portraits/TiffanyDialogue/Neutral", 0, 0)

--Variables.
local iMet55                    = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55", "N")
local iMet55InBasement          = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InBasement", "N")
local iSaw55sMemories           = VM_GetVar("Root/Variables/Chapter5/Scenes/iSaw55sMemories", "N")
local iFlashbackWakeup          = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackWakeup", "N")
local iFlashbackStartFinale     = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackStartFinale", "N")
local iChristineCorruptedEnding = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineCorruptedEnding", "N")
local iMainQuestBlock           = VM_GetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N")

-- |[ =================== Variations =================== ]|
--Until you actually meet her, 55 remains anonymous.
if(iMet55 == 0.0) then
    zProfileEntry:fnSetDisplayName("Intercom Lady")
    zProfileEntry:fnSetPortrait("Null", 0, 0)
    zProfileEntry:fnSetDescription(""..
        "The voice of a woman shouting at you[BR]over the intercom. She's harsh and[BR]critical, but seems to be able to access[BR]information you can't.[BR][BR]"..
        "What little information you have[BR]suggests she doesn't know what's going[BR]on any better than you do.")

--Having met 55, but not re-met her in Regulus City:
elseif(iMet55InBasement == 0.0) then
    zProfileEntry:fnSetDescription(""..
        "Command Unit 2855, Head of Security[BR]for Regulus City.[BR][BR]"..
        "Christine's memory banks indicate her[BR]rank, and as a command unit, she[BR]requires deference. All other records[BR]have been expunged.[BR][BR]"..
        "She seems vaguely familiar, though as a[BR]command unit, that could simply be[BR]because she shares 95%%%% of her parts[BR]with other command units.")

--Re-met 55, but haven't finished the LRT facility:
elseif(iSaw55sMemories == 0.0) then
    zProfileEntry:fnSetDescription(""..
        "Command Unit 2855, Former Head of[BR]Security for Regulus City.[BR][BR]"..
        "Christine met her in the Cryogenics[BR]facility, but the conversion process[BR]scrambled the memory files. 55 hid[BR]copies on your memory drive and[BR]re-enabled them afterwards.[BR][BR]"..
        "Being a former security unit, her[BR]hacking skills are exemplary and her[BR]combat prowiss unmatched. Her light[BR]but sturdy frame is designed to excel[BR]and provides impressive "..
        "agility.[BR]However, she is not as massive or as[BR]durable as Christine.[BR][BR]"..
        "Her demeanor is direct and unflattering.[BR]Her social skills are generally poor, and[BR]she has difficulty empathizing with[BR]others.[BR][BR]"..
        "Due to the circumstances at Cryogenics, she believes she[BR]participated in or even led the revolt there. Her memories were,[BR]however, wiped and she was badly damaged. "..
        "She believes[BR]investigating the LRT facility east of the city may produce backup[BR]of the events and many of the missing events of her life.")

--Saw the memories, this remains until Christine goes into the doll memory program.
elseif(iFlashbackWakeup == 0.0 and iMainQuestBlock < 60.0) then
    zProfileEntry:fnSetPortrait("Root/Images/Portraits/TiffanyDialogue/Smirk", 0, 0)
    zProfileEntry:fnSetDescription(""..
        "Command Unit 2855, Former Head of[BR]Security for Regulus City.[BR][BR]"..
        "Christine met her in the Cryogenics[BR]facility, but the conversion process[BR]scrambled the memory files. 55 hid[BR]copies on your memory drive and[BR]re-enabled them afterwards.[BR][BR]"..
        "Being a former security unit, her[BR]hacking skills are exemplary and her[BR]combat prowiss unmatched. Her light[BR]but sturdy frame is designed to excel[BR]and provides impressive "..
        "agility.[BR]However, she is not as massive or as[BR]durable as Christine.[BR][BR]"..
        "Her demeanor is direct and unflattering.[BR]Her social skills are generally poor, and[BR]she has difficulty empathizing with[BR]others.[BR][BR]"..
        "She led the attack against the rebels in the Cryogenics facility, but[BR]was defeated and wiped her memories in an effort to prevent a virus,[BR]possibly caused by Project Vivify, "..
        "from escaping. Since then, she has[BR]pledged to aid Christine's revolt against Regulus City's administration.")

--Profile during the Christine doll memory program. Always 'unavailable'.
elseif(iFlashbackStartFinale == 0.0) then
    zProfileEntry:fnSetDisplayName("?nit 28?5")
    zProfileEntry:fnSetDescription(""..
        "Profile unavailable. Please descramble[BR]errant memory files. Code 7715-e.")

--Final profile, changes based on Christine's alignment.
else
    --Normal:
    if(iChristineCorruptedEnding == 0.0) then
        zProfileEntry:fnSetPortrait("Root/Images/Portraits/TiffanyDialogue/Smirk", 0, 0)
        zProfileEntry:fnSetDescription(""..
            "Command Unit 2855, Former Head of[BR]Security for Regulus City.[BR][BR]"..
            "Christine met her in the Cryogenics[BR]facility, but the conversion process[BR]scrambled the memory files. 55 hid[BR]copies on your memory drive and[BR]re-enabled them afterwards.[BR][BR]"..
            "Being a former security unit, her[BR]hacking skills are exemplary and her[BR]combat prowiss unmatched. Her light[BR]but sturdy frame is designed to excel[BR]and provides impressive "..
            "agility.[BR]However, she is not as massive or as[BR]durable as Christine.[BR][BR]"..
            "Her demeanor is direct and unflattering.[BR]Her social skills are generally poor, and[BR]she has difficulty empathizing with[BR]others.[BR][BR]"..
            "She led the attack against the rebels in the Cryogenics facility, but[BR]was defeated and wiped her memories in an effort to prevent a virus,[BR]possibly caused by Project Vivify, "..
            "from escaping. Since then, she has[BR]pledged to aid Christine's revolt against Regulus City's administration.[BR][BR]"..
            "She is Christine's best friend.")

    --Corrupted Christine:
    else
        zProfileEntry:fnSetDescription(""..
            "Command Unit 2855, Former Head of[BR]Security for Regulus City.[BR][BR]"..
            "A maverick unit to be eliminated for[BR]the administrator. She is extremely[BR]intelligent, dangerous, and ruthless.[BR][BR]Elimination priority zero.")
    end
end