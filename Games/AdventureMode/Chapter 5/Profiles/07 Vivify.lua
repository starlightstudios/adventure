-- |[ ===================================== Project Vivify ===================================== ]|
--Oh Dear.

--Basic profile.
local zProfileEntry = JournalEntry:new("Vivify", "Project Vivify")
zProfileEntry:fnSetToProfile("Root/Images/Portraits/Vivify/Neutral", -150, 0)

--Variables.
local iLRTBossResult            = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTBossResult", "N")
local iReachedBiolabs           = VM_GetVar("Root/Variables/Chapter5/Scenes/iReachedBiolabs", "N")
local iMetVivify                = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetVivify", "N")
local iMainQuestBlock           = VM_GetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N")
local iFlashbackWakeup          = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackWakeup", "N")
local iChristineCorruptedEnding = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineCorruptedEnding", "N")
local iFlashbackStartFinale     = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackStartFinale", "N")

-- |[ =================== Variations =================== ]|
--Empty profile.
if(iLRTBossResult == 0.0) then
    zProfileEntry:fnBlankProfile()
    
--Special: No profile.
elseif(iMainQuestBlock == 60.0) then
    zProfileEntry:fnSetDisplayName("Proj??t ?iv??y")
    zProfileEntry:fnSetDescription(""..
        "This memory file screams at you from[BR]beyond time. You cannot understand it,[BR]but you know who Vivify is.")

--Met her.
elseif(iReachedBiolabs == 0.0 and iFlashbackStartFinale == 0.0 and iFlashbackWakeup == 0.0) then
    zProfileEntry:fnSetDescription(""..
        "Project Vivify, the prime research[BR]candidate of the Cryogenics facility and[BR]the reason for the revolt there.[BR]Constantly in a state of REM sleep and[BR]bodily dead, "..
        "she moves anyway. Created[BR]as an attempt to revive dead matter.[BR][BR]"..
        "The sample used to create Vivify is of[BR]unknown provenance even to the[BR]researchers working at the Cryogenics[BR]facility.[BR][BR]"..
        "Extremely dangerous in combat. She is[BR]physically durable, feels no pain, and[BR]can attack the minds of those around[BR]her. Few units have survived a direct[BR]encounter with her.[BR][BR]"..
        "Her motives are unknown, but her[BR]'singing' can be heard by those sensitive[BR]to it. This includes Christine.")

--Saw her at the gala.
elseif(iMetVivify == 0.0 and iFlashbackWakeup == 0.0) then
    zProfileEntry:fnSetDescription(""..
        "Project Vivify, the prime research[BR]candidate of the Cryogenics facility and[BR]the reason for the revolt there.[BR]Constantly in a state of REM sleep and[BR]bodily dead, "..
        "she moves anyway. Created[BR]as an attempt to revive dead matter.[BR][BR]"..
        "The sample used to create Vivify is of[BR]unknown provenance even to the[BR]researchers working at the Cryogenics[BR]facility.[BR][BR]"..
        "Extremely dangerous in combat. She is[BR]physically durable, feels no pain, and[BR]can attack the minds of those around[BR]her. Few units have survived a direct[BR]encounter with her.[BR][BR]"..
        "Her motives are unknown, but her[BR]'singing' can be heard by those sensitive[BR]to it. This includes Christine.[BR][BR]"..
        "She has exerted direct influence over Christine through her proxy of[BR]201890. However, Christine can retain her own will, for the moment,[BR]with the help of her friends.")

--Vivify freezes herself.
elseif(iFlashbackWakeup == 0.0) then
    zProfileEntry:fnSetDescription(""..
        "Project Vivify, the prime research[BR]candidate of the Cryogenics facility and[BR]the reason for the revolt there.[BR]Constantly in a state of REM sleep and[BR]bodily dead, "..
        "she moves anyway. Created[BR]as an attempt to revive dead matter.[BR][BR]"..
        "The sample used to create Vivify is of[BR]unknown provenance even to the[BR]researchers working at the Cryogenics[BR]facility.[BR][BR]"..
        "Extremely dangerous in combat. She is[BR]physically durable, feels no pain, and[BR]can attack the minds of those around[BR]her. Few units have survived a direct[BR]encounter with her.[BR][BR]"..
        "Her motives are unknown, but her[BR]'singing' can be heard by those sensitive[BR]to it. This includes Christine.[BR][BR]"..
        "She was apparently unaware of the nature of this material reality and[BR]what 'death' truly means. She has sealed herself in ice to contemplate[BR]her actions and what to do in this world.")

--Final profile. Same as freezes.
else
    zProfileEntry:fnSetDescription(""..
        "Project Vivify, the prime research[BR]candidate of the Cryogenics facility and[BR]the reason for the revolt there.[BR]Constantly in a state of REM sleep and[BR]bodily dead, "..
        "she moves anyway. Created[BR]as an attempt to revive dead matter.[BR][BR]"..
        "The sample used to create Vivify is of[BR]unknown provenance even to the[BR]researchers working at the Cryogenics[BR]facility.[BR][BR]"..
        "Extremely dangerous in combat. She is[BR]physically durable, feels no pain, and[BR]can attack the minds of those around[BR]her. Few units have survived a direct[BR]encounter with her.[BR][BR]"..
        "Her motives are unknown, but her[BR]'singing' can be heard by those sensitive[BR]to it. This includes Christine.[BR][BR]"..
        "She was apparently unaware of the nature of this material reality and[BR]what 'death' truly means. She has sealed herself in ice to contemplate[BR]her actions and what to do in this world.")
end