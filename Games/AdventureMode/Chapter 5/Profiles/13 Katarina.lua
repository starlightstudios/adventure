-- |[ ======================================== Katarina ======================================== ]|
--Efficiency expert lord golem.

--Basic profile.
local zProfileEntry = JournalEntry:new("Katarina", "Katarina")
zProfileEntry:fnSetToProfile("Root/Images/Portraits/Katarina/Neutral", 0, 0)

--Variables.
local iManuMetKatarina          = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuMetKatarina", "N")
local iMainQuestBlock           = VM_GetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N")
local iFlashbackWakeup          = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackWakeup", "N")
local iChristineCorruptedEnding = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineCorruptedEnding", "N")

-- |[ =================== Variations =================== ]|
--Empty profile.
if(iManuMetKatarina == 0.0) then
    zProfileEntry:fnBlankProfile()
    
--Special: No profile.
elseif(iMainQuestBlock == 60.0) then
    zProfileEntry:fnSetDisplayName("K??a?i?a")
    zProfileEntry:fnSetDescription(""..
        "Profile unavailable. Please descramble[BR]errant memory files. Code 7715-e.")

--Met Katarina.
elseif(iFlashbackWakeup == 0.0) then
    zProfileEntry:fnSetDescription(""..
        "Unit 600484, Efficiency Management[BR]Advisor, Sector 22. Often on special[BR]assignment in other sectors. Her friends[BR]call her Katarina.[BR][BR]"..
        "A well-respected efficiency expert, her[BR]job is to make sure Regulus City runs as[BR]smoothly as possible. She frequently[BR]visits other sectors to improve output[BR]without increasing input, "..
        "a crucial skill.[BR][BR]"..
        "She is usually curt and focused on her[BR]work, though she recognizes the[BR]importance of morale.")

--Final profile, does not change with alignment.
else
    --Normal:
    if(iChristineCorruptedEnding == 0.0) then
        zProfileEntry:fnSetDescription(""..
            "Unit 600484, Efficiency Management[BR]Advisor, Sector 22. Often on special[BR]assignment in other sectors. Her friends[BR]call her Katarina.[BR][BR]"..
            "A well-respected efficiency expert, her[BR]job is to make sure Regulus City runs as[BR]smoothly as possible. She frequently[BR]visits other sectors to improve output[BR]without increasing input, "..
            "a crucial skill.[BR][BR]"..
            "She is usually curt and focused on her[BR]work, though she recognizes the[BR]importance of morale.")

    --Corrupted Christine:
    else
        zProfileEntry:fnSetDisplayName("Unit 600484")
        zProfileEntry:fnSetDescription(""..
            "Unit 600484, Efficiency Management[BR]Advisor, Sector 22.[BR][BR]"..
            "In need of discipline for interacting with[BR]mavericks. Recommended reprimand.")
    end
end