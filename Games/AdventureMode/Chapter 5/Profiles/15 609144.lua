-- |[ ====================================== Unit 609144 ======================================= ]|
--Lunatic at the bottom of the mines.

--Basic profile.
local zProfileEntry = JournalEntry:new("609144", "609144")
zProfileEntry:fnSetToProfile("Root/Images/Portraits/609144/Neutral", 0, 0)

--Variables.
local iMetBoss50                = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetBoss50", "N")
local iDefeatedBoss50           = VM_GetVar("Root/Variables/Chapter5/Scenes/iDefeatedBoss50", "N")
local iMainQuestBlock           = VM_GetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N")
local iFlashbackWakeup          = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackWakeup", "N")
local iChristineCorruptedEnding = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineCorruptedEnding", "N")

-- |[ =================== Variations =================== ]|
--Empty profile.
if(iMetBoss50 == 0.0) then
    zProfileEntry:fnBlankProfile()
    
--Special: No profile.
elseif(iMainQuestBlock == 60.0) then
    zProfileEntry:fnSetDisplayName("Unit 6??1?4")
    zProfileEntry:fnSetDescription(""..
        "Profile unavailable. Please descramble[BR]errant memory files. Code 7715-e.")

--Met her.
elseif(iDefeatedBoss50 == 0.0 and iFlashbackWakeup == 0.0) then
    zProfileEntry:fnSetDescription(""..
        "Unit 609144, Research Director,[BR]Cryogenics Facility.[BR][BR]"..
        "Driven mad by her exposure to Project[BR]Vivify, she has begun grafting parts to[BR]herself and gibbering constantly. She is[BR]clearly unstable.")

--Defeated her.
elseif(iDefeatedBoss50 == 1.0 and iFlashbackWakeup == 0.0) then
    zProfileEntry:fnSetDescription(""..
        "Unit 609144, Research Director,[BR]Cryogenics Facility.[BR][BR]"..
        "Driven mad by her exposure to Project[BR]Vivify, she has begun grafting parts to[BR]herself and gibbering constantly. She is[BR]clearly unstable.[BR][BR]"..
        "After being defeated, she threw herself[BR]into a hole so deep, there may well be[BR]no bottom.")

--Final profile, does not change with alignment.
else
    --Met her.
    if(iDefeatedBoss50 == 0.0) then
        zProfileEntry:fnSetDescription(""..
            "Unit 609144, Research Director,[BR]Cryogenics Facility.[BR][BR]"..
            "Driven mad by her exposure to Project[BR]Vivify, she has begun grafting parts to[BR]herself and gibbering constantly. She is[BR]clearly unstable.")

    --Defeated her.
    else
        zProfileEntry:fnSetDescription(""..
            "Unit 609144, Research Director,[BR]Cryogenics Facility.[BR][BR]"..
            "Driven mad by her exposure to Project[BR]Vivify, she has begun grafting parts to[BR]herself and gibbering constantly. She is[BR]clearly unstable.[BR][BR]"..
            "After being defeated, she threw herself[BR]into a hole so deep, there may well be[BR]no bottom.")
    end
end