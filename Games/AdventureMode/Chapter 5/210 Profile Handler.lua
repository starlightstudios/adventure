-- |[ ===================================== Profile Handler ==================================== ]|
--Whenever the journal is opened to profiles mode, generates profiles.

-- |[Listing]|
--Setup.
gzaCh5ProfileEntries = {}

--Set globals.
JournalEntry.zaAppendList = gzaCh5ProfileEntries

-- |[Call Subroutines]|
local sProfilePath = fnResolvePath() .. "Profiles/"
LM_ExecuteScript(sProfilePath .. "00 Christine.lua")
LM_ExecuteScript(sProfilePath .. "01 Tiffany.lua")
LM_ExecuteScript(sProfilePath .. "02 Sophie.lua")
LM_ExecuteScript(sProfilePath .. "03 JX101.lua")
LM_ExecuteScript(sProfilePath .. "04 SX399.lua")
LM_ExecuteScript(sProfilePath .. "05 2856.lua")
LM_ExecuteScript(sProfilePath .. "06 Psue.lua")
LM_ExecuteScript(sProfilePath .. "06a Night.lua")
LM_ExecuteScript(sProfilePath .. "07 Vivify.lua")
LM_ExecuteScript(sProfilePath .. "08 201890.lua")
LM_ExecuteScript(sProfilePath .. "09 Sammy.lua")
LM_ExecuteScript(sProfilePath .. "10 Ellie.lua")
LM_ExecuteScript(sProfilePath .. "11 Jo.lua")
LM_ExecuteScript(sProfilePath .. "12 Maisie.lua")
LM_ExecuteScript(sProfilePath .. "13 Katarina.lua")
LM_ExecuteScript(sProfilePath .. "14 300910.lua")
LM_ExecuteScript(sProfilePath .. "15 609144.lua")
LM_ExecuteScript(sProfilePath .. "16 CrowbarChan.lua")

-- |[Assemble List]|
--Upload the information from the list.
for i = 1, #gzaCh5ProfileEntries, 1 do
    gzaCh5ProfileEntries[i]:fnUploadData()
end

-- |[Finish Up]|
--Reset globals.
JournalEntry:fnCleanGlobals()

--Clear the list to save memory.
gzaCh5ProfileEntries = nil
