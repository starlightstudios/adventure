-- |[ ====================================== Quest Handler ===================================== ]|
--Whenever the journal is opened to quest mode, generates quest entries.

-- |[Setup]|
--Create chart for storage.
gzaCh5QuestEntries = {}

--Set active globals. This reduces the number of arguments that need to be passed.
JournalEntry.zaAppendList = gzaCh5QuestEntries

-- |[Call Subscripts]|
local sBasePath = fnResolvePath() .. "Quests/"
LM_ExecuteScript(sBasePath .. "00 Main Quest.lua")

--Special: During the reprogramming sequence, only this quest is called. All others are bypassed.
local iMainQuestBlock = VM_GetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N")
if(iMainQuestBlock == 60.0) then
    LM_ExecuteScript(sBasePath .. "01 Special Reprogram.lua")
    
--Normal case:
else
    LM_ExecuteScript(sBasePath .. "02 Gather Allies.lua")
    LM_ExecuteScript(sBasePath .. "03 Equinox.lua")
    LM_ExecuteScript(sBasePath .. "04 Cassandra.lua")
    LM_ExecuteScript(sBasePath .. "05 South Cryogenics.lua")
    LM_ExecuteScript(sBasePath .. "06 Serenity Crater.lua")
    LM_ExecuteScript(sBasePath .. "07 Tellurium Mines.lua")
    LM_ExecuteScript(sBasePath .. "08 Manufactory.lua")
    LM_ExecuteScript(sBasePath .. "09 Electrosprites.lua")
    LM_ExecuteScript(sBasePath .. "10 Secrebots.lua")
    LM_ExecuteScript(sBasePath .. "11 Rune Upgrade.lua")
    LM_ExecuteScript(sBasePath .. "20 Reika.lua")
    LM_ExecuteScript(sBasePath .. "21 Fireteam.lua")
    LM_ExecuteScript(sBasePath .. "22 Raibies.lua")
end

-- |[ ============== Finish Up =============== ]|
-- |[Assemble List]|
for i = 1, #gzaCh5QuestEntries, 1 do
    gzaCh5QuestEntries[i]:fnUploadData()
end

-- |[Clean]|
--Unsets globals.
JournalEntry:fnCleanGlobals()

--Clear the list to save memory.
gzaCh5QuestEntries = nil
