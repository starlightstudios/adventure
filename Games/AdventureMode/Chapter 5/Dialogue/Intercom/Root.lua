-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Intercom state.
	local iIntercomState = VM_GetVar("Root/Variables/Chapter5/Scenes/iIntercomState", "N")
	
	--Basic setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])

	--If this is the first time talking to the intercom, unlock the airlock. Chris should be male in this.
	if(iIntercomState == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iIntercomState", "N", 1.0)
		VM_SetVar("Root/Variables/Chapter5/Scenes/Intro|iUnlockAirlockDoor", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N", 0.5)
		fnCutscene([[ Append("Voice:[VOICE|Tiffany] Ah, you are operational.[P] Are you able to speak?[B][C]") ]])
		fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] *cou[P]gh*[P][P] who are you?[B][C]") ]])
		fnCutscene([[ Append("Voice:[VOICE|Tiffany] That's not relevant, but I am within normal cognitive parameters.[P] Your concern is noted.[B][C]") ]])
		fnCutscene([[ Append("Voice:[VOICE|Tiffany] I observed you in the airlock on the thermal sensors.[P] The optical camera feed has been cut for that area.[P] Are you able to proceed?[B][C]") ]])
		fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] Could you please just give me a moment...[P][CLEAR]") ]])
		fnCutscene([[ Append("Voice:[VOICE|Tiffany] I will need your assistance if we are to exit this facility in a timely manner.[P] I've sustained a great deal of material damage, but you seem to be in functional order.[B][C]") ]])
		fnCutscene([[ Append("Voice:[VOICE|Tiffany] I am in the control center at the moment.[P] I will unlock the airlock door from here, but I won't be able to get you much further unless you have a PDU.[P] One moment...[B][C]") ]])
		fnCutscene([[ Append("Voice:[VOICE|Tiffany] Ah, good.[P] It seems there is one in the EVA room.[P] Please obtain it and report to the nearest intercom for instructions.[B][C]") ]])
		fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] Hey![P] What's going on, where am I, and who are you?[P][CLEAR]") ]])
		fnCutscene([[ Append("Voice:[VOICE|Tiffany] ...[P] I am unable to answer your questions at the moment, as I have the same ones.[P] I am unsure of how I got here.[B][C]") ]])
		fnCutscene([[ Append("Voice:[VOICE|Tiffany] Perhaps if we are to meet up, we can pool our knowledge.[P] Please obtain the PDU.[B][C]") ]])
		fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] But [P][CLEAR]") ]])
		fnCutscene([[ Append("Voice:[VOICE|Tiffany] Were the instructions not clear?[P] Please obtain the PDU in the next room, then report to an intercom.[B][C]") ]])
		fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] (This lady's not letting up, but I can't get out of this room on my own.[P] Perhaps she was brought here like me?[P] Guess I better get that PDU, either way...)") ]])
		fnCutsceneBlocker()
	
	--Tiffany chides Chris and tells him to get to work.
	elseif(iIntercomState == 1.0) then
		fnCutscene([[ Append("Voice:[VOICE|Tiffany] Please obtain the PDU in the next room, then report to an intercom.[P] I will have further instructions then.") ]])
		fnCutsceneBlocker()
	
	--Chris has the PDU.
	elseif(iIntercomState == 2.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iIntercomState", "N", 3.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N", 1.0)
		fnCutscene([[ Append("Voice:[VOICE|Tiffany] You have arrived.[P] The network is down and I am unable to reboot it, but the PDU should be able to provide access to the rest of the facility.[B][C]") ]])
		fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] This thing said I was on Regulus.[P] Where is that?[P] Where is Earth?[P] And who -[P] retired -[P] all those golems?[B][C]") ]])
		fnCutscene([[ Append("Voice:[VOICE|Tiffany] Please do not ask nonsense questions.[P] We will need to work together if we are to make it to Regulus City.[B][C]") ]])
		fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] The PDU said I should go there...[B][C]") ]])
		fnCutscene([[ Append("Voice:[VOICE|Tiffany] That is where I intend to go.[P] I have numerous queries for the database.[P] We can therefore assist one another, but you need to stop asking questions and do as you are told.[B][C]") ]])
		fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] Okay...[B][C]") ]])
		fnCutscene([[ Append("Voice:[VOICE|Tiffany] Good.[B][C]") ]])
		fnCutscene([[ Append("Voice:[VOICE|Tiffany] I can provide you access to the facility via your PDU now.[P] However, the doors are on lockdown.[P] You'll need special security cards to override them.[B][C]") ]])
		fnCutscene([[ Append("Voice:[VOICE|Tiffany] You will also need to locate several parts if we are to access Regulus City.[P] One moment.[B][C]") ]])
		fnCutscene([[ Append("Voice:[VOICE|Tiffany] Diagnostics suggest we will need two motivators from a Doll unit, an ocular unit, and an authenticator chip.[B][C]") ]])
		fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] How do I - [P][CLEAR]") ]])
		fnCutscene([[ Append("Voice:[VOICE|Tiffany] Do not ask questions until directed.[B][C]") ]])
		fnCutscene([[ Append("Voice:[VOICE|Tiffany] Your PDU should be able to extract the parts.[P] Make sure it scans the part before extracting it.[P] Damaged components will not work.[B][C]") ]])
		fnCutscene([[ Append("Voice:[VOICE|Tiffany] You will need a security-red keycard to access the command deck...[B][C]") ]])
		fnCutscene([[ Append("Voice:[VOICE|Tiffany] I can't locate one.[P] Protocols suggest one should be on a command unit in that area.[P] Get searching.[B][C]") ]])
		fnCutscene([[ Append("Voice:[VOICE|Tiffany] Any questions?[B][C]") ]])
		fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] ...[P] No.[B][C]") ]])
		fnCutscene([[ Append("Voice:[VOICE|Tiffany] Good.[P] I will do what I can from here to assist you.[P] Get moving.") ]])
		fnCutsceneBlocker()
	
	--Chris has the PDU, repeat.
	elseif(iIntercomState == 3.0) then
		fnCutscene([[ Append("Voice:[VOICE|Tiffany] Do you need me to repeat any instructions?[B][C]") ]])
		fnCutscene([[ Append("Voice:[VOICE|Tiffany] Your current objective is to locate a security-red keycard.[P] One should be on a command unit in that area.[B][C]") ]])
		fnCutscene([[ Append("Voice:[VOICE|Tiffany] You're also looking for two motivators, an ocular unit, and an authenticator chip.[P] Damaged parts will not do.[B][C]") ]])
		fnCutscene([[ Append("Voice:[VOICE|Tiffany] Now get moving.") ]])
	
	--Chris has the red keycard.
	elseif(iIntercomState == 4.0 or iIntercomState == 40.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iIntercomState", "N", 5.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N", 2.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iUnlockContainment", "N", 1.0)
        
        --Normal event sequence:
        if(iIntercomState == 4.0) then
            fnCutscene([[ Append("Voice:[VOICE|Tiffany] Do you have the red security card?[B][C]") ]])
            fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] Yeah.[P] I found it on a dead...[P] doll...[B][C]") ]])
            fnCutscene([[ Append("Voice:[VOICE|Tiffany] Oh, then you should have scanned its parts.[B][C]") ]])
            fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] No good.[P] The PDU said they were useless.[P] That chip you wanted had been removed.[B][C]") ]])
        
        --If the player skipped talking to the intercom and got the red card:
        elseif(iIntercomState == 40.0) then
            fnCutscene([[ Append("Voice:[VOICE|Tiffany] I noticed you did not report to me upon acquiring the PDU, disappeared off my receivers, and returned with a red security card.[B][C]") ]])
            fnCutscene([[ Append("Voice:[VOICE|Tiffany] It is good to have acquired the card, but do not ignore my orders again.[B][C]") ]])
            fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] Sorry.[P] I found the card on a dead...[P] doll...[B][C]") ]])
            fnCutscene([[ Append("Voice:[VOICE|Tiffany] I had already signalled your PDU to scan for the parts I need.[P] In the future, I will order it to admonish you for disobedience.[B][C]") ]])
            fnCutscene([[ Append("Voice:[VOICE|Tiffany] More to the point, did you acquire an authenticator chip?[B][C]") ]])
            fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] No good.[P] That chip you wanted had been removed, and the other parts were useless.[B][C]") ]])
        end
        
        --Common.
        fnCutscene([[ Append("Voice:[VOICE|Tiffany] ...[P][P] Unfortunate.[P] The authenticator chip is the most important piece we need.[P] I am assuming neither you nor your PDU have one.[B][C]") ]])
        fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] ...[P] No?[B][C]") ]])
        fnCutscene([[ Append("Voice:[VOICE|Tiffany] There should be more command units in the facility.[P] Plus, there is a blue security restriction on the command deck.[B][C]") ]])
        fnCutscene([[ Append("Voice:[VOICE|Tiffany] The logs are incomplete, but they indicate that at least one blue-authorized unit was assigned to the containment area.[P] You'll need that security card.[B][C]") ]])
        fnCutscene([[ Append("Voice:[VOICE|Tiffany] More importantly you need to find those parts.[P] Search thoroughly for them.[B][C]") ]])
        fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] Can't I just rest for a moment?[P] This place is a nightmare.[B][C]") ]])
        fnCutscene([[ Append("Voice:[VOICE|Tiffany] You have higher concerns than that.[P] The longer you delay, the longer you're in this 'nightmare'.[B][C]") ]])
        fnCutscene([[ Append("Voice:[VOICE|Tiffany] Now, the containment area is on the west side past the red security door.[P] I'll unlock it for you.[B][C]") ]])
        fnCutscene([[ Append("Voice:[VOICE|Tiffany] When you're in there...[P] be quiet and don't waste time.[P] The power is out in that whole sector.[P] I can't guarantee the containment seals are still in place.[B][C]") ]])
        fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] Just what does that mean?[B][C]") ]])
        fnCutscene([[ Append("Voice:[VOICE|Tiffany] Don't ask nonsense questions.[P] Just find that blue card and the parts we need.[P] Is your task clear?[B][C]") ]])
        fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] ...[P] Yes.[B][C]") ]])
        fnCutscene([[ Append("Voice:[VOICE|Tiffany] I obviously can't check the visible scanners, but I am assuming that area is dark due to the power outage.[P] There are a number of portable lights in that area.[B][C]") ]])
        fnCutscene([[ Append("Voice:[VOICE|Tiffany] See if you can locate one, otherwise it will be difficult to navigate the containment area.[B][C]") ]])
        fnCutscene([[ Append("Voice:[VOICE|Tiffany] Now.[P] Get going.") ]])
    
	--Chris has the PDU, repeat.
	elseif(iIntercomState == 5.0) then
	
		--Variables.
		local iHasDollMotivatorA = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasDollMotivatorA", "N")
		local iHasDollMotivatorB = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasDollMotivatorB", "N")
		local iHasDollOcular     = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasDollOcular",     "N")
	
		--Chris does not have the parts.
		if(iHasDollMotivatorA == 0 or iHasDollMotivatorB == 0 or iHasDollOcular == 0) then
			fnCutscene([[ Append("Voice:[VOICE|Tiffany] I can repeat your instructions, if necessary.[B][C]") ]])
			fnCutscene([[ Append("Voice:[VOICE|Tiffany] You need to locate a security-blue keycard.[P] One should be in the containment area in the northwest of the facility.[B][C]") ]])
			fnCutscene([[ Append("Voice:[VOICE|Tiffany] You're also looking for two motivators, an ocular unit, and an authenticator chip.[P] Damaged parts will not do.[P] Scan any command units you find.[B][C]") ]])
			fnCutscene([[ Append("Voice:[VOICE|Tiffany] Now get moving.") ]])
		
		--Chris has the parts.
		else
			VM_SetVar("Root/Variables/Chapter5/Scenes/iIntercomState", "N", 6.0)
			VM_SetVar("Root/Variables/Chapter5/Scenes/iHasFabricationAAuth", "N", 2.0)
            VM_SetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N", 3.0)
			fnCutscene([[ Append("Voice:[VOICE|Tiffany] The thermal scope indicates an unusually high heat output.[P] Is something wrong?[B][C]") ]])
			fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] Listen, some of these doll things got killed by something big.[P] Really big.[B][C]") ]])
			fnCutscene([[ Append("Voice:[VOICE|Tiffany] So the thermal output is related to a fear response.[P] Suppress it.[P] Fear will make you make irrational choices and lead to failure.[B][C]") ]])
			fnCutscene([[ Append("Voice:[VOICE|Tiffany] You have the keycard and the parts, correct?[B][C]") ]])
			fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] Well, I got some of them.[P] But I couldn't find the chip you wanted.[P] All the dolls had theirs removed.[B][C]") ]])
			fnCutscene([[ Append("Voice:[VOICE|Tiffany] ...[P] Unfortunate.[P] There are tactical advantages to be had removing the chips.[P] An intelligence is at work.[B][C]") ]])
			fnCutscene([[ Append("Voice:[VOICE|Tiffany] We may yet be able to fabricate one.[P] The fabricators are past the green doors in the east end of the facility.[P] A blanked chip would do just as well as a recovered one.[B][C]") ]])
			fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] So I'll - [P][CLEAR]") ]])
			fnCutscene([[ Append("Voice:[VOICE|Tiffany] Not yet.[P] It seems that area doesn't have enough power to open the doors.[P] I'll need to reroute power to the fabricators, so you'll have to find a way around.[B][C]") ]])
			fnCutscene([[ Append("Voice:[VOICE|Tiffany] I've given you access to the transit tunnels.[P] You should be able to find a way to the fabrication bays.[P] Go east from your position.[B][C]") ]])
			fnCutscene([[ Append("Voice:[VOICE|Tiffany] If there are no further reasons to delay, get going.[B][C]") ]])
			fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] (It's not worth the effort to argue with her...)") ]])
		end
		
	--Chris needs to go to the fabrication bay.
	elseif(iIntercomState == 6.0) then
	
		--Variables.
		local iSawRuinedFabricator = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawRuinedFabricator", "N")
	
		--Hasn't seen the fabricator yet.
		if(iSawRuinedFabricator == 0.0) then
			fnCutscene([[ Append("Voice:[VOICE|Tiffany] Your current assignment is to enter the fabrication bay.[P] You'll need to find a way in other than the main doorway.[P] Check the transit station on the east side of the facility.[B][C]") ]])
			fnCutscene([[ Append("Voice:[VOICE|Tiffany] Once there, fabricate an authenticator chip.[P] Then, report to the command center on the upper floor of the facility.[B][C]") ]])
			fnCutscene([[ Append("Voice:[VOICE|Tiffany] Get going.") ]])
		
		--Has seen it.
		else
			VM_SetVar("Root/Variables/Chapter5/Scenes/iIntercomState", "N", 7.0)
            VM_SetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N", 4.0)
			fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] Umm...[B][C]") ]])
			fnCutscene([[ Append("Voice:[VOICE|Tiffany] Motion trackers in the fabrication bay indicate someone was in them.[P] Was it you?[B][C]") ]])
			fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] Yes...[B][C]") ]])
			fnCutscene([[ Append("Voice:[VOICE|Tiffany] And do you have an authenticator chip?[B][C]") ]])
			fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] No.[B][C]") ]])
			fnCutscene([[ Append("Voice:[VOICE|Tiffany] There appears to be a problem with the power supply to that area.[P] I will - [P][CLEAR]") ]])
			fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] The whole place was wrecked.[P] It looked like a bomb went off.[B][C]") ]])
			fnCutscene([[ Append("Voice:[VOICE|Tiffany] ...[B][C]") ]])
			fnCutscene([[ Append("Voice:[VOICE|Tiffany] You are certain?[B][C]") ]])
			fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] Nothing was usable in there.[B][C]") ]])
			fnCutscene([[ Append("Voice:[VOICE|Tiffany] *tap tap tap*[B][C]") ]])
			fnCutscene([[ Append("Voice:[VOICE|Tiffany] It seems any record of the damage was removed from the central system.[B][C]") ]])
			fnCutscene([[ Append("Voice:[VOICE|Tiffany] ...[B][C]") ]])
			fnCutscene([[ Append("Voice:[VOICE|Tiffany] Well, without a chip, we're stranded here.[B][C]") ]])
			fnCutscene([[ Append("Voice:[VOICE|Tiffany] We should meet.[P] Strategize.[P] We may yet be able to help one another.[B][C]") ]])
			fnCutscene([[ Append("Voice:[VOICE|Tiffany] I'll remove the lock on the command station's door.[P] It's next to the airlock on the command deck.[B][C]") ]])
			fnCutscene([[ Append("Voice:[VOICE|Tiffany] I...[P] I will wait here for you.[P] Do not delay.") ]])
	
		end
	
	--Repeat.
	elseif(iIntercomState == 7.0) then
		fnCutscene([[ Append("Voice:[VOICE|Tiffany] Please proceed to the command station.[P] I removed the lock on the door.[P] It's next to the airlock on the command deck.") ]])
	
	--No answer.
    elseif(iIntercomState == 8.0) then
        local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
        if(iTalkedToSophie == 0.0) then
            fnCutscene([[ Append("771852:[E|Serious] (No unit answering intercom.[P] Report error to Regulus City Administration, priority::[P] 8820355U-B2.[P] Resume current assignment.)") ]])
    
        else
            fnCutscene([[ Append("Christine:[E|Neutral] (There is no answer on the intercom...)") ]])
        end
	end
end
