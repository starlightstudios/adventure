-- |[Coming up with a Plan]|
--Subscript, called when coming up with the plan to upgrade SX-399. There's a lot of possible variation
-- so it gets its own script.

--Variables.
local sChristineForm              = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
local iSXKnowsChristineTransforms = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXKnowsChristineTransforms", "N")
	
--If SX-399 knows that Christine can transform, her form doesn't matter.
if(iSXKnowsChristineTransforms == 1.0) then
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] Hey 55![P] Do you like my hair?[P] I was just...[P] fiddling with it, you know?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] It looks...[P] red.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Smooth, 55.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Listen, SX-399...[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] I don't like this at all.[P] You're doing that thing where you have bad news.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Your condition...[P] It's not good.[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] I know.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] No, I mean, I spoke to TT-233...[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] She estimated one, maybe two, more cycles.[P] Then... done...[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] ...[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] D-d-d-d-d-done?[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] W-w-what - [B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] *sniff*[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Oh, she's going to cry...[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] But don't worry, we can fix you![B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] *sniff*[P] You're not just saying that?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] I showed you I can transform myself at will, if you'll recall.[P] Well, I have connections at Regulus City.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] My tandem unit, Sophie, the best repair unit there is.[P] We can hook you up to the backup distributor in Sector 96 and finally get to the bottom of this.[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] R-r-r-really?[P] R-r-really really really?[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] But your tandem unit - [P]she'd be a golem, right?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Yes, she is.[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] Mother has always said that the golems are cruel and heartless...[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] [P]Not all of them.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Some of them will risk their lives for others at no personal gain.[P] Some of them care about strangers because we are all sisters.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Christine is one such golem.[P] If she says Sophie is, then she is.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Wow, 55...[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Their altruism is aggravating, I assure you.[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] You're right, aren't you.[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] But mother won't just let me go.[P] I've snuck out before, but never so far as Regulus City...[P] And if she knew it was because a golem told me so...[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Don't worry, we've got a plan for that, too.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Simply put, you will transform me into a Steam Droid, and 55 will disguise me to look like you.[P] JX-101 won't know the difference.[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] And then -[P] you'll transform back once I'm repaired![P] Keen![B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] But there's just one problem.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] And that is?[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] I can't transform you.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Oh, drat.[P] Well - [P][CLEAR]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] Nobody can.[P] The schematics are long gone.[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] You see, the transformation involves creating your power core and fusing it with your heart.[P] You create it from flesh and your own magic.[P] It's a difficult and complex process.[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] I did it, and every other Steam Droid did it, but we used the blueprints we were given by an expert designer.[P] We're all custom-made.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Hmmm...[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] If we could find the schematics, would that be enough?[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] You'd have to adapt them to your own physical properties.[P] They spent a lot of time measuring me and asking me to take weird poses and hold my breath a lot.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] I'm sure I can figure it out.[P] But finding the blueprints, if they even exist, won't be easy.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Why do you assume they don't exist?[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] Because if they did, we Steam Droids could truly repair ourselves.[P] If we found my old schematics, I'm sure we could have repaired me long ago.[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] But, they were lost.[P] Your best shot is to find an old workshop, and Regulus City repurposed or destroyed them all long ago.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] ... Incorrect.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Really?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] There are many locations that the administrators have declared destroyed that were not.[P] They are used as detention sites.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] They cannot be found on the Regulus City network without special access.[P] I can guess the location of several based on my earlier research.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Give me some time.[P] If an old Steam Droid workshop exists, I will find it.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Raiding a black site might be a bad idea, 55...[P] Regulus City security would not take kindly to an incursion.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Correct.[P] We will need force.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] The Fist of the Future can provide that force.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] But JX-101's not going to help us if she knows it's to kidnap her daughter![B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] No, but she will help us rescue prisoners.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Detention centers usually contain political prisoners, and doubtless weaponry and supplies.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] I will arrange the information to fall into her lap, and she will ask you, Christine, to help her raid the site.[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] And if you find the blueprints, then I can help you adapt them and make your own power core![B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] If the blueprints exist, that is.[P] And, we are assuming Christine is not liquidated during the operation.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Gee, thanks for your concern.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] All right, 55.[P] We'll have to make sure JX-101 trusts me absolutely to take me along on this job.[P] After that, find me a black site and get it to her scouts.[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] Be careful...[P] I don't want you getting retired for my sake...[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] I'll be fine.[P] I've been in tricky spots before.[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] Oh, certainly.[P] But, I was talking about 55.[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] Don't get that pretty face shot off, hun.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] [P][P]Ihaveworktodogoodbye!") ]])

--If SX-399 doesn't know that Christine can transform, the conversation goes differently:
else

	--Christine is still a human, so SX-399 doesn't know she's a golem:
	if(sChristineForm == "Human") then
	
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Hey 55![P] Do you like my hair?[P] I was just...[P] fiddling with it, you know?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] It looks...[P] red.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Smooth, 55.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Listen, SX-399...[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] I don't like this at all.[P] You're doing that thing where you have bad news.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Your condition...[P] It's not good.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] I know.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] No, I mean, I spoke to TT-233...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] She estimated one, maybe two, more cycles.[P] Then... done...[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] ...[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] D-d-d-d-d-done?[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] W-w-what - [B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] *sniff*[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Oh, she's going to cry...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] But don't worry, we can fix you![B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] *sniff*[P] You're not just saying that?") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()

		--Flashwhite.
		Cutscene_CreateEvent("Flash Christine White", "Actor")
			ActorEvent_SetProperty("Subject Name", "Christine")
			ActorEvent_SetProperty("Flashwhite Quickly", "Null")
		DL_PopActiveObject()
		fnCutsceneBlocker()

		fnCutsceneWait(75)
		fnCutsceneBlocker()
		fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
		fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] *sniff*[P] What happened...[P] to you?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] We may not have time to be subtle.[P] So, I'll be blunt.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] I am Unit 771852, Lord Golem of Maintenance and Repair, Sector 96.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] This runestone allows me to transform myself.[P] I used it to appear as a human so Sprocket City would not gun me down.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] *sniff*[P] Keep going...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Oh good, I was worried you might be upset by this.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] I don't think you could upset me more if you tried...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Now, 55 and I believe that Golem technology may be able to help you.[P] At the very least, we can hook you to our backup distributor.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] That would mean you wouldn't need to recharge for most of the year, as well.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] S-[P]sign me up![P] Let's go![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] There are risks...[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] I don't care![P] When do we leave?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] If we leave, your mother will mobilize every resource to stop us.[P] We can't just go.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] If we tell her that I'm going to die...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] She knew.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] ...[P] Mother...[P] how could you...[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] [P]Waaaahhhhh![B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] M-m-m-m-my[P] own[P] mother[P] *sniff*...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] She was trying to protect you.[P] She didn't want you to become depressed...[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] But how could she keep this from me?[P] Does my life not matter to her?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] I don't think she was right to do it, but I understand why she did it.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Now, there's something I need from you before we go.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] I need you to transform me.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] ..?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] I'll shapeshift to a human, and then you must transform me into a Steam Droid.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Then, 55 will disguise me to look like you.[P] She will take you to Regulus City for repairs.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] If your mother doesn't think you're gone, she won't send out a search party.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] And you think that will work?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] I can apply camouflage paint to accuracy within one micrometer.[P] My optical receptors will be able to photograph your facial structure and constitute a three-dimensional replica.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] I estimate I can replicate your appearance to within one part in one-thousand.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] And then you'll take me to Regulus City...[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] I will make sure the creatures in the mines do not harm you.[P] You will be safe with me.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] ...[P] but there's just one problem.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] And that is?[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] I can't transform you.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Oh, drat. Well - [P][CLEAR]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Nobody can.[P] The schematics are long gone.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] You see, the transformation involves creating your power core and fusing it with your heart.[P] You create it from flesh and your own magic.[P] It's a difficult and complex process.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] I did it, and every other Steam Droid did it, but we used the blueprints we were given by an expert designer.[P] We're all custom-made.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Hmmm...[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] If we could find the schematics, would that be enough?[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] You'd have to adapt them to your own physical properties.[P] They spent a lot of time measuring me and asking me to take weird poses and hold my breath a lot.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] I'm sure I can figure it out.[P] But finding the blueprints, if they even exist, won't be easy.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Why do you assume they don't exist?[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Because if they did, we Steam Droids could truly repair ourselves.[P] If we found my old schematics, I'm sure we could have repaired me long ago.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] But, they were lost.[P] Your best shot is to find an old workshop, and Regulus City repurposed or destroyed them all long ago.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] ...[P] Incorrect.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Really?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] There are many locations that the administrators have declared destroyed that were not.[P] They are used as detention sites.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] They cannot be found on the Regulus City network without special access.[P] I can guess the location of several based on my earlier research.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Give me some time.[P] If an old Steam Droid workshop exists, I will find it.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Raiding a black site might be a bad idea, 55...[P] Regulus City security would not take kindly to an incursion.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Correct.[P] We will need force.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] The Fist of the Future can provide that force.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] But JX-101's not going to help us if she knows it's to kidnap her daughter![B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] No, but she will help us rescue prisoners.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Detention centers usually contain political prisoners, and doubtless weaponry and supplies.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] I will arrange the information to fall into her lap, and she will ask you, Christine, to help her raid the site.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] And if you find the blueprints, then I can help you adapt them and make your own power core![B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] If the blueprints exist, that is.[P] And, we are assuming Christine is not liquidated during the operation.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Gee, thanks for your concern.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] All right, 55.[P] We'll have to make sure JX-101 trusts me absolutely to take me along on this job.[P] After that, find me a black site and get it to her scouts.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Be careful...[P] I don't want you getting retired for my sake...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] I'll be fine.[P] I've been in tricky spots before.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Oh, certainly.[P] But, I was talking about 55.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Don't get that pretty face shot off, hun.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] [P][P]Ihaveworktodogoodbye!") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
	
	--Non-human form. Christine must demonstrate it's still her.
	else
	
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Hey 55![P] Do you like my hair?[P] I was just...[P] fiddling with it, you know?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] It looks...[P] red.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] And, er, Christine?[P] What happened to you?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] SX-399, I know this is going to seem crazy, but I can transform.[P] I can change my shape at will.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Oh, really?[P] Because I legitimately thought my optical receptors were fried.[P] Again.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()

		--Flashwhite.
		Cutscene_CreateEvent("Flash Christine White", "Actor")
			ActorEvent_SetProperty("Subject Name", "Christine")
			ActorEvent_SetProperty("Flashwhite Quickly", "Null")
		DL_PopActiveObject()
		fnCutsceneBlocker()

		fnCutsceneWait(75)
		fnCutsceneBlocker()
		fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
		fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Abracapresto![B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Keen![P] You must be some kinda super-mage![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] That, I would not go so far as to say.[P] But, this is a secret.[P] Please don't tell anyone.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] It provides us a useful tactical advantage.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Oh, sure, whatever you say.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] So, does that mean you can change yourself to look like 55?[P] Can you impersonate people?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Well, no.[P] I still look like me.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] But, I showed you that for a reason.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Yeah?[P] What's going on?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] (She seems really chipper...[P] I hate to ruin it for her, but...)[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Your condition...[P] It's not good.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Tell me something I don't know, ha ha![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] No, I mean, I spoke to TT-233...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] She estimated one, maybe two, more cycles.[P] Then...[P] done...[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] ...[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] D-d-d-d-d-done?[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] W-w-what - [B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] *sniff*[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Oh, she's going to cry...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] But don't worry, we can fix you![B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] *sniff*[P] You're not just saying that?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] You see, I'm a Lord Golem.[P] I run the repair facility at Sector 96.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] *sniff*[P] *sniff*[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] My tandem unit, Sophie, is the best repair unit there is.[P] We can hook you up to the backup distributor in Sector 96 and finally get to the bottom of this.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] R-r-r-really?[P] R-r-really really really?[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] But your tandem unit - [P]she'd be a golem, right?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Yes, she is.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Mother has always said that the golems are cruel and heartless...[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] [P]Not all of them.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Some of them will risk their lives for others at no personal gain.[P] Some of them care about strangers because we are all sisters.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Christine is one such golem.[P] If she says Sophie is, then she is.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Wow, 55...[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Their altruism is aggravating, I assure you.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] You're right, aren't you.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] But mother won't just let me go.[P] I've snuck out before, but never so far as Regulus City...[P] And if she knew it was because a golem told me so...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Don't worry, we've got a plan for that, too.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Simply put, you will transform me into a Steam Droid, and 55 will disguise me to look like you.[P] JX-101 won't know the difference.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] And then -[P] you'll transform back once I'm repaired![P] Keen![B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] But there's just one problem.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] And that is?[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] I can't transform you.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Oh, drat.[P] Well - [P][CLEAR]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Nobody can.[P] The schematics are long gone.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] You see, the transformation involves creating your power core and fusing it with your heart.[P] You create it from flesh and your own magic.[P] It's a difficult and complex process.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] I did it, and every other Steam Droid did it, but we used the blueprints we were given by an expert designer.[P] We're all custom-made.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Hmmm...[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] If we could find the schematics, would that be enough?[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] You'd have to adapt them to your own physical properties.[P] They spent a lot of time measuring me and asking me to take weird poses and hold my breath a lot.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] I'm sure I can figure it out.[P] But finding the blueprints, if they even exist, won't be easy.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Why do you assume they don't exist?[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Because if they did, we Steam Droids could truly repair ourselves.[P] If we found my old schematics, I'm sure we could have repaired me long ago.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] But, they were lost.[P] Your best shot is to find an old workshop, and Regulus City repurposed or destroyed them all long ago.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] ... Incorrect.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Really?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] There are many locations that the administrators have declared destroyed that were not.[P] They are used as detention sites.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] They cannot be found on the Regulus City network without special access.[P] I can guess the location of several based on my earlier research.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Give me some time.[P] If an old Steam Droid workshop exists, I will find it.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Raiding a black site might be a bad idea, 55...[P] Regulus City security would not take kindly to an incursion.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Correct.[P] We will need force.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] The Fist of the Future can provide that force.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] But JX-101's not going to help us if she knows it's to kidnap her daughter![B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] No, but she will help us rescue prisoners.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Detention centers usually contain political prisoners, and doubtless weaponry and supplies.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] I will arrange the information to fall into her lap, and she will ask you, Christine, to help her raid the site.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] And if you find the blueprints, then I can help you adapt them and make your own power core![B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] If the blueprints exist, that is.[P] And, we are assuming Christine is not liquidated during the operation.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Gee, thanks for your concern.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] All right, 55.[P] We'll have to make sure JX-101 trusts me absolutely to take me along on this job.[P] After that, find me a black site and get it to her scouts.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Be careful...[P] I don't want you getting retired for my sake...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] I'll be fine.[P] I've been in tricky spots before.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Oh, certainly.[P] But, I was talking about 55.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Don't get that pretty face shot off, hun.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] [P][P]Ihaveworktodogoodbye!") ]])
	end

end
