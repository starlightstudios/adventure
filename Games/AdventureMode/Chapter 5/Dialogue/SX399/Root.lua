-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables.
	local iSpokeWith55AboutSX         = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeWith55AboutSX", "N")
	local iPlanHatched                = VM_GetVar("Root/Variables/Chapter5/Scenes/iPlanHatched", "N")
	local iSXMet55                    = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXMet55", "N")
	local iIs55Following              = VM_GetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N")
	local sChristineForm              = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	local iSteamDroid250RepState      = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid250RepState", "N")
	local iSXKnowsChristineTransforms = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXKnowsChristineTransforms", "N")
	local iSteamDroid500RepState      = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid500RepState", "N")
	
	--Talking to SX-399 after the first meeting.
	if(iSteamDroid250RepState < 3.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] *Are you sure this will work?*[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] *Be polite but assertive.[P] She needs to respect your independence.*") ]])
	
	--Has not met 55 yet:
	elseif(iSXMet55 == 0.0 and iIs55Following == 1.0) then
	
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSXMet55", "N", 1.0)
	
		--If Christine is a human during this:
		if(sChristineForm == "Human" or iSXKnowsChristineTransforms == 1.0) then
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Hey Christine -[P] and hello to you, ma'am.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] *Now would be the ideal time to kidnap her.*[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Offended] 55, for the last time we are not kidnapping her![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Oh, you can kidnap me if you want...[P] Just have me back in time for my recharge, and I'll go anywhere with you...[B][C]") ]])
			fnCutscene([[ Append("55:[E|Smirk] She seems to want us to take her.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Just you, babe...[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Oh, I'm sorry, Christine.[P] Are you together?[P] I don't want to get in the way...[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] 55 is my good friend, but my heart lies elsewhere.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] So you're saying you don't mind?[P] Radical![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] 55, was it?[P] Don't be a stranger, honey.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] !!![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Ha ha![P] I'm just being friendly.[P] Any friend of Christine's is a friend of mine.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] So, Christine, what *is* 55?[P] Some kind of doll girl?[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] I am Command Unit 2855.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Keen![P] So that's what a Command Unit looks like![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Mother tells me they're the worst of the worst, but I guess you're not all bad.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] She's a bit of a special case.[P] The other ones aren't like her, at all.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Lucky, then, that I don't give a fig about them.[P] So, what brings you here?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Well, I just thought I might introduce you two.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Sad] And...[P] please don't tell anyone else.[P] I don't want to cause any trouble.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Sprocket City welcomes all comers.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Upset] No, it does not.[P] I am not interested in residence.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] The other Steam Droids will doubtless not react well to me.[P] Command Units are their most bitter of enemies.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Okay then, I won't tell.[P] Hey, more for me...[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] Christine, why does she keep pressing herself up against me like that?[B][C]") ]])
			fnCutscene([[ Append("55:[E|Angry] ...[P] Stop![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Blush] *SX, you better stop.[P] You're being too forward.*[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] *I'm soooo thirsty, and she's the hottest robot I've ever seen.*[B][C]") ]])
			fnCutscene([[ Append("55:[E|Upset] My auditory receivers are perfectly capable of hearing you regardless of volume, you realize.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Okay, SX-399, I'll be honest here.[P] I think we can trust you.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] 55 and I are looking for allies.[P] We're planning a revolt against the administration of Regulus City.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] We came here in order to meet your mother and try to get her on our side.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Hm, so you only came to my room for that?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Offended] No![P] We help anyone in need![P] Right, 55?[B][C]") ]])
			fnCutscene([[ Append("55:[E|Upset] Ngh -[P] She is squeezing my arm.[P] Stop that![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Unfortunately we fell into that trap, and now JX-101 thinks 55 is both villainous, and dead.[P] I'm not really sure how to proceed.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Quite a bind you're in.[P] I'd love to help, but I need to go in for recharging soon.[P] I -[P] I hope your rebellion works out.[P] Come visit me when I wake up again.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Wait...[P] You mentioned your condition.[P] Do you know why you have to spend so much recharging?[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Something wrong with my power core.[P] I think it's a leak, but I have to be asleep to open me up.[P] You should ask TT-233.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Ha, wait a minute now.[P] Are you thinking of fixing me?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] I am pretty good with machines.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Good luck with that.[P] Mother has gotten every mechanic on Regulus to look at me, and none of them had a fix.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Well I'm not just any old mechanic.[P] I'm going to go talk to TT-233.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Yeah, 55, you stay out here with me.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Upset] !!!!!!!![P] Release me![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] (I better hurry up, or 55 might have a system overload!)") ]])
	
		--Golem:
		elseif(sChristineForm == "Golem") then
		
			--Flags.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iSXKnowsChristineTransforms", "N", 1.0)
		
			--Dialogue.
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Hey Christine.[P] Anything interesting out in the mines?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Did I -[P] oops, I should have transformed before speaking to you.[P] Don't be afraid![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] I'm not afraid of anything.[P] By the way, what happened?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] I am a Golem, SX-399.[P] I have been since before we met.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Keen![P] So this is what a Golem looks like![P] Nice dress, by the way.[P] Do you have more like it?[P] I think it'd look pretty good on me![B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] Unit 771852, we should acquire her while she is away from the other Steam Droids.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Hot stuff, you can acquire me all you want.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Wait, that sounded stupid.[P] What does acquire mean?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Blush] *Sigh*[P] 55 wants to kidnap you.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] 55, hm?[P] Well I wouldn't mind going someplace private with you, if you know what I mean.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Upset] ..![P] Unhand me![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Laugh] No, 55, no![P] She's being friendly![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] So if you're a Golem, does that mean 55 is a Command Unit?[P] Radical![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Christine, you are just the coolest![P] Mother was totally wrong about the Golems![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Well, not quite.[P] You see, 55 and I are mavericks.[P] We're planning a revolt against Regulus City's administration.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] That's the whole reason we came into the mines, you see.[P] We were looking for JX-101.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] We believe the Steam Droids will assist us.[P] We share the same objectives.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Yeah, no.[P] Pretty sure mother would have you shot on sight.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Well, unless you looked like a human.[P] Was that a disguise?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Ah -[P] no.[P] I can change between forms.[P] It is because of this runestone here, see?[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Very cool![P] Can I see?") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
	
			--Flashwhite.
			Cutscene_CreateEvent("Flash Christine White", "Actor")
				ActorEvent_SetProperty("Subject Name", "Christine")
				ActorEvent_SetProperty("Flashwhite Quickly", "Null")
			DL_PopActiveObject()
			fnCutsceneBlocker()

			fnCutsceneWait(75)
			fnCutsceneBlocker()
			fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
			fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Tadaa![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Awwwwesome![P] Can 55 do that too?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] No, just me.[P] It's magic, but I don't know exactly how it works.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] So, do you want me to speak to mother for you?[P] I'm not sure she'd believe me.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] That, unfortunately, is the problem.[P] I highly doubt she will appreciate all the duplicity.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] From what I've seen of her, she hates Golems with a passion.[P] She'd likely assume I was converted and have me retired.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] I was about to say that, actually.[P] Mother is extremely stubborn.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Hmm...[P] That condition of yours...[P] Do you know why you have to spend so much recharging?[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Something wrong with my power core.[P] I think it's a leak, but I have to be asleep to open me up.[P] You should ask TT-233.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Ha, wait a minute now.[P] Are you thinking of fixing me?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] I *am* the Lord Golem of Maintenance and Repair, Sector 96.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Well that explains why the other Steam Droids say you're good with repairs.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] But...[P] Mother has gotten every mechanic on Regulus to look at me, and none of them had a fix.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Well I'm not just any old mechanic.[P] I'm going to go talk to TT-233.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] All right.[P] You go do that, and 55 and I will stay out here...[P] and...[P] bond...[B][C]") ]])
			fnCutscene([[ Append("55:[E|Upset] !!!!!!!![P] Release me![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] (I better hurry up, or 55 might have a system overload!)") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
	
		--Latex Drone:
		elseif(sChristineForm == "LatexDrone") then
		
			--Flags.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iSXKnowsChristineTransforms", "N", 1.0)
		
			--Dialogue.
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Wow, Christine.[P] Didn't know you were into that, but more power to you.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Blush] You -[P] you recognize me?[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] It's the hair, I'd recognize it anywhere.[P] By the way, what happened?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] I am a Drone Unit, SX-399.[P] It's one of the model variants of the Golems.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Tubular![P] So, who's your friend there?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] This is 55.[P] Say hello, 55.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] I do not appreciate how this Steam Droid is looking at me.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] I don't mean to be off-putting.[P] I just -[P] like what I see.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] What are the connotations behind that[P][CLEAR]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] So![P] SX-399![P] I figure we can trust you, right?[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Of course![P] It's because of you that I'm not being watched every minute of every day.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Great![P] So, I'll admit this, then.[P] The reason we came to Sprocket City was that 55 and I are planning a rebellion against Regulus City.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Oh my gosh, seriously?[P] That is so stellar![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] And I'm guessing you needed the help of the Steam Droids?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] You don't like the Administration, and neither do we.[P] We should work together.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Unfortunately, your mother is very hostile towards Golems...[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Obviously.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Which is why I had to appear as a human, you see.[P] But that only made things worse.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Because we fell into the trap you placed, 55 had to pretend like she was holding me hostage to escape.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] So now JX-101 would probably have her shot on sight, and I don't know how to approach JX-101 without her assuming I'm a spy...[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Well, why don't you just get 55 to put on a disguise like you did?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Oh -[P] no.[P] It was not a disguise...") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
	
			--Flashwhite.
			Cutscene_CreateEvent("Flash Christine White", "Actor")
				ActorEvent_SetProperty("Subject Name", "Christine")
				ActorEvent_SetProperty("Flashwhite Quickly", "Null")
			DL_PopActiveObject()
			fnCutsceneBlocker()

			fnCutsceneWait(75)
			fnCutsceneBlocker()
			fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
			fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Tadaa![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] I can transform myself.[P] It's magic, you see.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Awwwwesome![P] You're rad to the max, Christine![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] So, do you want me to speak to mother for you?[P] I'm not sure she'd believe me.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] That, unfortunately, is the problem.[P] I highly doubt she will appreciate all the duplicity.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Your mother will not believe anything I or 55 say in our current state.[P] She'd likely assume I was converted and have me retired.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] I was about to say that, actually.[P] Mother is extremely stubborn.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Hmm...[P] That condition of yours...[P] Do you know why you have to spend so much recharging?[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Something wrong with my power core.[P] I think it's a leak, but I have to be asleep to open me up.[P] You should ask TT-233.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Ha, wait a minute now.[P] Are you thinking of fixing me?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Well, you see, I'm a repair Unit. I run a whole repair shop, actually.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Well that explains why the other Steam Droids say you're good with repairs.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] But...[P] Mother has gotten every mechanic on Regulus to look at me, and none of them had a fix.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Well I'm not just any old mechanic.[P] I'm going to go talk to TT-233.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] All right.[P] You go do that, and 55 and I will stay out here...[P] and...[P] bond...[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] !!!!!!!![P] Release me![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] (I better hurry up, or 55 might have a system overload!)") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
        
        --Darkmatter!
        elseif(sChristineForm == "Darkmatter") then
		
            --Flags.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSXKnowsChristineTransforms", "N", 1.0)
        
            --Dialogue.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Christine![P] And -[P] you're one of those space girls I've seen on the computers?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Blush] Yes I am![P] We're called Darkmatters, by the way.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Sweet![P] Do you like to knock things off of tables, too?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Say, I haven't been to the surface in...[P] decades.[P] Are the stars I see in your hair a reflection of the night sky?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Oooh ooh![P] Can you phase through stuff, too?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Sheesh, SX-399.[P] Calm down.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Aw, but don't you miss being a human?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Blush] Well...[P] you see...") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
	
			--Flashwhite.
			Cutscene_CreateEvent("Flash Christine White", "Actor")
				ActorEvent_SetProperty("Subject Name", "Christine")
				ActorEvent_SetProperty("Flashwhite Quickly", "Null")
			DL_PopActiveObject()
			fnCutsceneBlocker()

			fnCutsceneWait(75)
			fnCutsceneBlocker()
			fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
			fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Tadaa![P] I can transform![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Oh cool cool![P] That's so radical![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] So does that mean you're both a human and a darkmatter?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Blush] Actually, I'm a golem.[P] I just transform into a human when it's helpful.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Huh.[P] So if you're a golem, what does that make her?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Blush] Oh, I forgot![P] How rude of me![P] This is Unit 2855.[P] She's a Command Unit.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] Greetings.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Keen![P] Wow, I've never seen one of those before![P] Are they all this cute?[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] Excuse me?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Offended] Well, no, because most of them want you dead.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Oh, I know.[P] Mother tells me all sorts of stories.[P] I've just never been fortunate enough to see one.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] Fortunate?[P] The stories your mother tells are likely negative.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Correct.[P] But so far, one-hundred percent of the Command Units I have seen are really cute.[P] I'm going to chalk that up as a positive.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] You see, the stories are the problem.[P] We're mavericks. [P]We want to overthrow Regulus City's administration.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Sad] But then we walked into that trap and I accidentally transformed into a human and things have gotten out of control.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Oh, I get it![P] If mother knew you were a golem, she'd probably think you were a spy.[P] Makes sense.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] So, do you want me to speak to mother for you?[P] I'm not sure she'd believe me.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] That, unfortunately, is the problem.[P] I highly doubt she will appreciate all the duplicity.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Your mother will not believe anything I or 55 say in our current state.[P] She'd likely assume I was converted and have me retired.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] I was about to say that, actually.[P] Mother is extremely stubborn.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Hmm...[P] That condition of yours...[P] Do you know why you have to spend so much recharging?[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Something wrong with my power core.[P] I think it's a leak, but I have to be asleep to open me up.[P] You should ask TT-233.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Ha, wait a minute now.[P] Are you thinking of fixing me?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Well, you see, I'm a repair Unit. I run a whole repair shop, actually.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Well that explains why the other Steam Droids say you're good with repairs.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] But...[P] Mother has gotten every mechanic on Regulus to look at me, and none of them had a fix.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Well I'm not just any old mechanic.[P] I'm going to go talk to TT-233.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] All right.[P] You go do that, and 55 and I will stay out here...[P] and...[P] bond...[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] !!!!!!!![P] Release me![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] (I better hurry up, or 55 might have a system overload!)") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
        
        --Raiju!
        elseif(sChristineForm == "Raiju") then
		
            --Flags.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSXKnowsChristineTransforms", "N", 1.0)
        
            --Dialogue.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Christine![P] Nice...[P] electric...[P] rat...[P] what happened?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] I'm a raiju![B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] I know what a raiju is, I'm wondering how that happened.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] I've heard from the other steam droids that there's a way into the biolabs, but I didn't think a human could do it.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] However you did it, good for you![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] It gets even better![B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] You've got the look on your face like you're about to impress me.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] So I'm going to do my best to be totally unimpressed.[P] Go on, try.") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
	
			--Flashwhite.
			Cutscene_CreateEvent("Flash Christine White", "Actor")
				ActorEvent_SetProperty("Subject Name", "Christine")
				ActorEvent_SetProperty("Flashwhite Quickly", "Null")
			DL_PopActiveObject()
			fnCutsceneBlocker()

			fnCutsceneWait(75)
			fnCutsceneBlocker()
			fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
			fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Tadaa![P] I can transform![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Okay, I'm impressed.[P] You did it, good job.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Yep![P] I can turn into any form I've been in before.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Does being human have some sort of advantage over being a raiju?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Blush] I can also transform into a golem, and a command unit![P] And that's not even the full list![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Speaking of command units, is that what your friend there is?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Blush] Oh, I forgot![P] How rude of me![P] This is Unit 2855.[P] She's a command unit, but full time.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] Hello.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Keen![P] Wow, I've never seen one of those before![P] Are they all this cute?[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] Excuse me?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Offended] Well, no, because most of them want you dead.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Oh, I know.[P] Mother tells me all sorts of stories.[P] I've just never been fortunate enough to see one.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] Fortunate?[P] The stories your mother tells are likely negative.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Correct.[P] But so far, one-hundred percent of the Command Units I have seen are really cute.[P] I'm going to chalk that up as a positive.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] You see, the stories are the problem.[P] We're mavericks. [P]We want to overthrow Regulus City's administration.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Sad] But then we walked into that trap and I accidentally transformed into a human and things have gotten out of control.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Oh, I get it![P] If mother knew you were a golem, she'd probably think you were a spy.[P] Makes sense.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] So, do you want me to speak to mother for you?[P] I'm not sure she'd believe me.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] That, unfortunately, is the problem.[P] I highly doubt she will appreciate all the duplicity.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Your mother will not believe anything I or 55 say in our current state.[P] She'd likely assume I was converted and have me retired.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] I was about to say that, actually.[P] Mother is extremely stubborn.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Hmm...[P] That condition of yours...[P] Do you know why you have to spend so much recharging?[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Something wrong with my power core.[P] I think it's a leak, but I have to be asleep to open me up.[P] You should ask TT-233.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Ha, wait a minute now.[P] Are you thinking of fixing me?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Well, you see, I'm a repair Unit. I run a whole repair shop, actually.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Well that explains why the other Steam Droids say you're good with repairs.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] But...[P] Mother has gotten every mechanic on Regulus to look at me, and none of them had a fix.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Well I'm not just any old mechanic.[P] I'm going to go talk to TT-233.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] All right.[P] You go do that, and 55 and I will stay out here...[P] and...[P] bond...[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] !!!!!!!![P] Release me![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] (I better hurry up, or 55 might have a system overload!)") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
            
        --Dreamer!
        elseif(sChristineForm == "Eldritch") then
		
            --Flags.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSXKnowsChristineTransforms", "N", 1.0)
        
            --Dialogue.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Christine![P] Have a run in with the creepy crawlies?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Blush] You can tell it's me?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] You don't see a full head of hair like that every day.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Funny, none of the other...[P] things...[P] ever talk.[P] I hear stories about them but they don't talk.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Can all of you talk?[P] Are you still you?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Yes, but I'm rather unique...") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
	
			--Flashwhite.
			Cutscene_CreateEvent("Flash Christine White", "Actor")
				ActorEvent_SetProperty("Subject Name", "Christine")
				ActorEvent_SetProperty("Flashwhite Quickly", "Null")
			DL_PopActiveObject()
			fnCutsceneBlocker()

			fnCutsceneWait(75)
			fnCutsceneBlocker()
			fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
			fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Tadaa![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Woooooaaaahhhhh!!![P] Radical, Christine![P] Just radical![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] So, uh, were you a golem before?[P] I'm a little confused.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Blush] I just transformed into a human, actually.[P] I was always a golem.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] So then what's your friend there?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Blush] That's 55.[P] She's a Command Unit.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] Hello.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Keen![P] Wow, I've never seen one of those before![P] Are they all this cute?[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] Excuse me?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Offended] Well, no, because most of them want you dead.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Oh, I know.[P] Mother tells me all sorts of stories.[P] I've just never been fortunate enough to see one.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] Fortunate?[P] The stories your mother tells are likely negative.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Correct.[P] But so far, one-hundred percent of the Command Units I have seen are really cute.[P] I'm going to chalk that up as a positive.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] You see, the stories are the problem.[P] We're mavericks. [P]We want to overthrow Regulus City's administration.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Sad] But then we walked into that trap and I accidentally transformed into a human and things have gotten out of control.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Oh, I get it![P] If mother knew you were a golem, she'd probably think you were a spy.[P] Makes sense.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] So, do you want me to speak to mother for you?[P] I'm not sure she'd believe me.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] That, unfortunately, is the problem.[P] I highly doubt she will appreciate all the duplicity.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Your mother will not believe anything I or 55 say in our current state.[P] She'd likely assume I was converted and have me retired.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] I was about to say that, actually.[P] Mother is extremely stubborn.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Hmm...[P] That condition of yours...[P] Do you know why you have to spend so much recharging?[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Something wrong with my power core.[P] I think it's a leak, but I have to be asleep to open me up.[P] You should ask TT-233.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Ha, wait a minute now.[P] Are you thinking of fixing me?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Well, you see, I'm a repair Unit. I run a whole repair shop, actually.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Well that explains why the other Steam Droids say you're good with repairs.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] But...[P] Mother has gotten every mechanic on Regulus to look at me, and none of them had a fix.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Well I'm not just any old mechanic.[P] I'm going to go talk to TT-233.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] All right.[P] You go do that, and 55 and I will stay out here...[P] and...[P] bond...[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] !!!!!!!![P] Release me![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] (I better hurry up, or 55 might have a system overload!)") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
        
        --Doll!
        elseif(sChristineForm == "Doll") then
		
			--Flags.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iSXKnowsChristineTransforms", "N", 1.0)
		
			--Dialogue.
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Christine, that's quite an outfit you've got there.[P] Really shows off your actuators.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Blush] SX![P] Please![P] You're making me blush![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] You can blush?[P] Keen![P] How's that work?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] The resin layers can shift slightly to change how light is absorbed.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] So what'd you do to get turned into resin and actuators and junk?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Really, I'm just glad you're not freaking out.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Should I be?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Yes, because I'm what is called a 'command unit'.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Oh really, so that's what they look like?[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Mother has nothing but bad things to say, but I don't see any flaws in you.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Other than your friend there possibly not being single.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Upset] Christine, what does she mean?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Sad] Uh, well, she's...[P] hitting on you...[B][C]") ]])
			fnCutscene([[ Append("55:[E|Upset] How does she know I find her attractive!?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] No, 55, when SHE finds YOU attractive, she hits on you.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Oh ho ho![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] But I need to circle back around.[P] Christine, how did you get turned into one of these?[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] If half of what mother says is true, you should be trying to kill me right now.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] I'm sure she didn't mention...[P] this!") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
	
			--Flashwhite.
			Cutscene_CreateEvent("Flash Christine White", "Actor")
				ActorEvent_SetProperty("Subject Name", "Christine")
				ActorEvent_SetProperty("Flashwhite Quickly", "Null")
			DL_PopActiveObject()
			fnCutsceneBlocker()

			fnCutsceneWait(75)
			fnCutsceneBlocker()
			fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
			fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Gee whiz, seems she left that bit out![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Heh, not all of us can do that.[P] Just me, in fact.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] It's magic, but I'm not sure exactly how it works.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] What can you do, 55?[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] My joints are quantum-locked superfluids and can rotate at any angle.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] I can use my interface cables to hack nearly any electronic device, or discharge my power core into things to shock them.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] My ocular receptors can see beyond the visible spectrum, as low as 200nm.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Wow![P] You're really impressive, 55![B][C]") ]])
			fnCutscene([[ Append("55:[E|Down] ...[P] I...[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] That's a good thing, 55.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] Do you find me impressive?[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] You're really neat![B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] Thank you.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] She's really shy.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] That just makes her even cooler![P] I wish I could see UV light and junk![P] And she's so humble![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] But why is this a big deal?[P] Why were you hiding this?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Because unfortunately, your mother probably doesn't agree about how neat 55 is.[B][C]") ]])
            
			fnCutscene([[ Append("SX-399:[E|Neutral] Yeah, probably.[P] She really hates command units and golems.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] You and 55 are, like, renegades or something and you think mother will try to capture her.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] She already did, once, when we tried to initiate negotiations.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Do you want me to speak to mother for you?[P] I'm not sure she'd believe me.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] That, unfortunately, is the problem.[P] I highly doubt she will appreciate all the duplicity.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Your mother will not believe anything I or 55 say in our current state.[P] She'd assume I disguised myself as a human to infiltrate the city.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Yeah you'd be right about that.[P] She's paranoid.[P] She's right to be paranoid, but she's still paranoid.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Hmm...[P] That condition of yours...[P] Do you know why you have to spend so much recharging?[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Something wrong with my power core.[P] I think it's a leak, but I have to be asleep to open me up.[P] You should ask TT-233.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Ha, wait a minute now.[P] Are you thinking of fixing me?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Well, you see, I'm a repair unit in Regulus City.[P] I run a whole repair shop, actually.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Well that explains why the other Steam Droids say you're good with repairs.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] But...[P] Mother has gotten every mechanic on Regulus to look at me, and none of them had a fix.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Well I'm not just any old mechanic.[P] I'm going to go talk to TT-233.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] All right.[P] You go do that, and 55 and I will stay out here...[P] and...[P] bond...[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] !!!!!!!![P] Release me![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] (I better hurry up, or 55 might have a system overload!)") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
        
        --Secrebot!
        elseif(sChristineForm == "Secrebot") then
		
			--Flags.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iSXKnowsChristineTransforms", "N", 1.0)
		
			--Dialogue.
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Christine![P] Wow, with all the ladders in the mines you choose to go around wearing those legs or wheels or whatever?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Blush] My cover lasted all of two seconds.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Not even.[P] So what happened, by the way?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] It's a bit of a long story, but suffice is to say, I'm a secrebot.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Never actually seen a working one, but sometimes you see parts from a scrapped one.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Yeah, and I'm glad you're not freaking out.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] I'm glad you're okay with being a secrebot.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] It's not permanent.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] I have the vague feeling you're about to really impress me.[B][C]") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
	
			--Flashwhite.
			Cutscene_CreateEvent("Flash Christine White", "Actor")
				ActorEvent_SetProperty("Subject Name", "Christine")
				ActorEvent_SetProperty("Flashwhite Quickly", "Null")
			DL_PopActiveObject()
			fnCutsceneBlocker()

			fnCutsceneWait(75)
			fnCutsceneBlocker()
			fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
			fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Keen![P] Tubular![P] How'd you do that?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] I have a magic runestone.[P] I don't know how it works, unfortunately.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Can you turn into other stuff?[P] Like, rocks and tubes and guns?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] No, only forms I've been before.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Can we transform you into a big gun?[P] That'd be really cool![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] I think you're missing the point here.[P] I'm a golem.[P] Always have been.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] ...[P] And?[P] You can transform so you're not really?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Yeah, but the steam droids won't trust me if I'm a golem.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Ohhhh, I get you.[P] Yeah, your secret is safe with me, secrebot Christine.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] We want their help, we're planning a rebellion.[P] Do you know of any way to get them to trust a golem like me?[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] I'd like to tell you yes, but mother is incredibly stubborn.[P] I really don't know.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] I'll keep working on that.[P] Even if your mother isn't going to trust me, least I can do is help out Sprocket City, and you.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Me?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] I'm a repair unit, and I have access to the city's repair facilities.[P] Maybe I can help fix you?[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Good luck![P] I heard it's something wrong with my power core.[P] We've tried everything to fix it.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] If you want details, talk to TT-233.[P] Just -[P] don't get your hopes up, okay.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] All right, and thanks for keeping my secret!") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
        
        --Shocking!
        elseif(sChristineForm == "Electrosprite") then
		
			--Flags.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iSXKnowsChristineTransforms", "N", 1.0)
		
			--Dialogue.
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Christine![P] I love your hair![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Blush] You can tell it's me?[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] You stuck your hand in a power converter didn't you?[P] But it didn't change your jawline.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] It's a very strong jawline.[P] Inspires real confidence.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Blush] I was expecting you to freak out a bit more...[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] It takes a lot to rattle me -[P] such as your friend there not being single.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] (Oh?)[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] 55, meet SX-399.[P] SX-399, meet 55.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Upset] Why is she looking at me this way?[P] Christine?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Laugh] Noreasonatall - [P][CLEAR]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] So, SX-399, we figured you're a trustworthy sort![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Ohhhh, you're about to ask me to keep a secret.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Nobody says [P]'Hey you're trustworthy'[P] if they're not about to ask you to keep a secret.[P] Nobody.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Well we are, so good guess.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Laugh] Brace yourself!") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
	
			--Flashwhite.
			Cutscene_CreateEvent("Flash Christine White", "Actor")
				ActorEvent_SetProperty("Subject Name", "Christine")
				ActorEvent_SetProperty("Flashwhite Quickly", "Null")
			DL_PopActiveObject()
			fnCutsceneBlocker()

			fnCutsceneWait(75)
			fnCutsceneBlocker()
			fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
			fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Tadaa![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] I can transform myself.[P] It's magic, you see.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Best.[P] Secret.[P] EVER![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] This is so cool, Christine![P] And 55?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Sorry, only I can do that.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Bummer.[P] Oh well.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] So what is 55, then?[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Sad] Uh, she's a doll, SX-399.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Upset] Command Unit...[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Oh, I get it now![P] That explains all the commotion earlier![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] You and 55 are, like, renegades or something and you don't think mother will trust her.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] How far off am I?[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] Quite close, actually.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Do you want me to speak to mother for you?[P] I'm not sure she'd believe me.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] That, unfortunately, is the problem.[P] I highly doubt she will appreciate all the duplicity.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Your mother will not believe anything I or 55 say in our current state.[P] She really doesn't like golems or dolls.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Upset] Command Unit![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] [EMOTION|Tiffany|Neutral]I was about to say that, actually.[P] Mother is extremely stubborn.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Hmm...[P] That condition of yours...[P] Do you know why you have to spend so much recharging?[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Something wrong with my power core.[P] I think it's a leak, but I have to be asleep to open me up.[P] You should ask TT-233.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Ha, wait a minute now.[P] Are you thinking of fixing me?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Well, you see, I'm a repair unit in Regulus City.[P] I run a whole repair shop, actually.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Well that explains why the other Steam Droids say you're good with repairs.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] But...[P] Mother has gotten every mechanic on Regulus to look at me, and none of them had a fix.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Well I'm not just any old mechanic.[P] I'm going to go talk to TT-233.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] All right.[P] You go do that, and 55 and I will stay out here...[P] and...[P] bond...[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] !!!!!!!![P] Release me![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] (I better hurry up, or 55 might have a system overload!)") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
        
        --Oh come on.
        elseif(sChristineForm == "SteamDroid") then
		
			--Flags.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iSXKnowsChristineTransforms", "N", 1.0)
		
			--Dialogue.
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Oh, very clever.[P] You saw that this was NC+ and decided to show up in a form that obviates the quest I'm a part of.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] You didn't consider my feelings in the matter, of course.[P] How do you think I feel, being just cast aside as part of a questline?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] But I'm doing the quest to get you in the party later.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] I would have insisted, had I any influence over proceedings at this stage.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Okay fine. But you have to transform in this scene, so get to it.") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
	
			--Flashwhite.
			Cutscene_CreateEvent("Flash Christine White", "Actor")
				ActorEvent_SetProperty("Subject Name", "Christine")
				ActorEvent_SetProperty("Flashwhite Quickly", "Null")
			DL_PopActiveObject()
			fnCutsceneBlocker()

			fnCutsceneWait(75)
			fnCutsceneBlocker()
			fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
			fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Okay so that's done, you two need to get horny for each other.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] Way ahead of you on that one.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] The next part of the quest is to speak to TT-233.[P] So go do that.") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
            
        --Unhandled form.
        else
		
			--Flags.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iSXKnowsChristineTransforms", "N", 1.0)
		
			--Dialogue.
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Wow, Christine.[P] You're in a form Salty hasn't written a handler for yet![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Blush] That lazybones had to release a prototype but didn't have time to finish and test all the forms. Sorry.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Offended] She already knows so you don't need to file a bug report. This will get updated soon.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Okay. We'll just pretend like the cutscene ran anyway.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Your next task is to speak to TT-233, the Steam Droid doctor in the Fist of Tomorrow headquarters.") ]])
        
		end
	
	--Time to come up with a plan.
	elseif(iSpokeWith55AboutSX == 1.0 and iPlanHatched == 0.0) then
		
		--If 55 isn't following:
		if(iIs55Following == 0.0) then
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Something on your mind, Christine?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Yeah, but I want 55 here before I tell you.[P] It's important.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] (I'd best go get 55, she wouldn't forgive me if I left her out of this...)") ]])
			
		--55 is following.
		else
		
			--Execute.
			TA_SetProperty("Face Character", "PlayerEntity")
			LM_ExecuteScript(fnResolvePath() .. "Plan.lua")
			
			--Flags.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iSXKnowsChristineTransforms", "N", 1.0)
			VM_SetVar("Root/Variables/Chapter5/Scenes/iPlanHatched", "N", 1.0)
		end
	
	--55 isn't following but SX-399 doesn't know Christine can transform:
	elseif(iSXKnowsChristineTransforms == 0.0 and sChristineForm ~= "Human") then
	
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSXKnowsChristineTransforms", "N", 1.0)
		
		--Golem:
		if(sChristineForm == "Golem") then
			
			--Dialogue.
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Hey Christine, nice dress.[P] Have you got any in my size?[P] I think it might look good on me![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Now, don't be alarmed, SX-399...[P] I'm not like the other Golems.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Ohhhh, I was wondering about that.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Excuse me?[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] I didn't actually know what a Golem looks like.[P] But, I must say, you look lovely.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Why thank you![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Did you get converted or something?[P] Mother said that's what they do to humans they capture.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] If you're one of their slaves...[P] No, I must be missing something here.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] This, actually.") ]])
			fnCutsceneBlocker()
	
			--Flashwhite.
			Cutscene_CreateEvent("Flash Christine White", "Actor")
				ActorEvent_SetProperty("Subject Name", "Christine")
				ActorEvent_SetProperty("Flashwhite Quickly", "Null")
			DL_PopActiveObject()
			fnCutsceneBlocker()

			fnCutsceneWait(75)
			fnCutsceneBlocker()
			fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
			fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] I can transform myself, you see.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Ohmygoshthisissokeen![P] How?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] This magical runestone here.[P] I don't know how it works, but it does.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Wow![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] But you can't tell anyone.[P] Your mother despises Golems and I don't want to upset her.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] I don't think anyone would believe me, but don't worry.[P] Your secret is safe with me.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Very good.[P] Thank you.") ]])
			fnCutsceneBlocker()
			
		--LatexDrone:
		elseif(sChristineForm == "LatexDrone") then
			
			--Dialogue.
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Wow, nice bondage outfit, Christine.[P] Not that there's anything wrong with that.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Offended] Now, don't be alarmed, SX-399...[P] I'm not like the other Golems.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Ohhhh, I was wondering about that.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Excuse me?[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] I didn't actually know what a Golem looks like.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Oh, well I'm what's called a Drone Unit.[P] It's one of our model variants.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Did you get converted or something?[P] Mother said that's what they do to humans they capture.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] If you're one of their slaves...[P] No, I must be missing something here.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] This, actually.") ]])
			fnCutsceneBlocker()
	
			--Flashwhite.
			Cutscene_CreateEvent("Flash Christine White", "Actor")
				ActorEvent_SetProperty("Subject Name", "Christine")
				ActorEvent_SetProperty("Flashwhite Quickly", "Null")
			DL_PopActiveObject()
			fnCutsceneBlocker()

			fnCutsceneWait(75)
			fnCutsceneBlocker()
			fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
			fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] I can transform myself, you see.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Ohmygoshthisissokeen![P] How?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] This magical runestone here.[P] I don't know how it works, but it does.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Wow![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] But you can't tell anyone.[P] Your mother despises Golems and I don't want to upset her.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] I don't think anyone would believe me, but don't worry.[P] Your secret is safe with me.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Very good.[P] Thank you.") ]])
			fnCutsceneBlocker()
			
		--Raiju:
		elseif(sChristineForm == "Raiju") then
			
			--Dialogue.
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Hey Christine![P] You get lucky with a raiju or something?[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Color me green with envy.[P] And copper oxide, but mostly envy.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Blush] Yeah, I did, actually.[P] But it's even cooler than that.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Apparently, some of the other steam droids have found a way to sneak into the biolabs and make out with the raijus.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] So tell me all about your cool spy adventures.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Prepare to be amazed!") ]])
			fnCutsceneBlocker()
	
			--Flashwhite.
			Cutscene_CreateEvent("Flash Christine White", "Actor")
				ActorEvent_SetProperty("Subject Name", "Christine")
				ActorEvent_SetProperty("Flashwhite Quickly", "Null")
			DL_PopActiveObject()
			fnCutsceneBlocker()

			fnCutsceneWait(75)
			fnCutsceneBlocker()
			fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
			fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Tadaa![P] I can transform myself![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Radical![P] How'd you do that?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] This magical runestone here.[P] I don't know how it works, but it does.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] So you can use that as a disguise, and also a secret way to shock people unexpectedly?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Absolutely![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] But you can't tell anyone.[P] Only a couple of people know.[P] After all, a good disguise has to be secret.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] I don't think anyone would believe me, but don't worry.[P] Your secret is safe with me.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Very good.[P] Thank you.") ]])
			fnCutsceneBlocker()
			
		--Doll:
		elseif(sChristineForm == "Doll") then
			
			--Dialogue.
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Woah, Christine![P] I can see it all![P] Leave something to the imagination![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Offended] Now, don't be alarmed, SX-399...[P] I'm not like the other command units.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] C-[P]command -[P] oh yeah, that's what mom was talking about![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] You've got those superfluid joints and everything![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Excuse me?[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] I've never actually seen a command unit.[P] You look kind of like a toy doll, don't you.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] The resemblance is not accidental, I assure you.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] So did you get captured and converted?[P] Or did you, like, break into a place and get converted to steal access codes or something?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] No, it's even cooler.[P] Check this out.") ]])
			fnCutsceneBlocker()
	
			--Flashwhite.
			Cutscene_CreateEvent("Flash Christine White", "Actor")
				ActorEvent_SetProperty("Subject Name", "Christine")
				ActorEvent_SetProperty("Flashwhite Quickly", "Null")
			DL_PopActiveObject()
			fnCutsceneBlocker()

			fnCutsceneWait(75)
			fnCutsceneBlocker()
			fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
			fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] I can transform myself, you see.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Ohmygoshthisissokeen![P] How?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] This magical runestone here.[P] I don't know how it works, but it does.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Wow![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] But you can't tell anyone.[P] Your mother despises dolls like me, and I don't want to upset her.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] I don't think anyone would believe me, but don't worry.[P] Your secret is safe with me.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Very good.[P] Thank you.") ]])
			fnCutsceneBlocker()
	
        --Darkmatter!
        elseif(sChristineForm == "Darkmatter") then
			
			--Dialogue.
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Well there's something you don't see every day.[P] Is that a black hole on your chest?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Sad] Now, don't be alarmed, SX-399...[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Oh, it is![P] It's a real black hole![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] No, I mean, I'm a friend.[P] I'm Christine![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Yes, and?[P] It's pretty obvious.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Oh, good![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] I didn't even know they made more Darkmatters![P] I'm impressed![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Get ready to be more impressed...") ]])
			fnCutsceneBlocker()
	
			--Flashwhite.
			Cutscene_CreateEvent("Flash Christine White", "Actor")
				ActorEvent_SetProperty("Subject Name", "Christine")
				ActorEvent_SetProperty("Flashwhite Quickly", "Null")
			DL_PopActiveObject()
			fnCutsceneBlocker()

			fnCutsceneWait(75)
			fnCutsceneBlocker()
			fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
			fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] I can transform myself, you see.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Ohmygoshthisissokeen![P] How?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] This magical runestone here.[P] I don't know how it works, but it does.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Wow![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Though I'd appreciate it if you kept it a secret.[P] I'm actually a golem, I just turned into a human for...[P] reasons.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] I don't think anyone would believe me, but don't worry.[P] Your secret is safe with me.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Very good.[P] Thank you.") ]])
			fnCutsceneBlocker()
            
        --Dreamer!
        elseif(sChristineForm == "Eldritch") then
			
			--Dialogue.
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Hey, Christine![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Sad] Oh come on![P] I picked my scariest and weirdest form![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Scariest?[P] Oh that's a stretch.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Mother when she mentions her war stories?[P] Now that's a scary form.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] How so?[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Not scary in the angry way, but in the sad kind of way.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] She gets quiet.[P] She starts to mumble.[P] She's always so strong and then that comes up.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Yes, father was like that sometimes, too.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Was he in war?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Yes, he was.[P] But he would have been on the other side.[P] He would have been with the oppressors.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Hey![P] Forget about that, watch this!") ]])
			fnCutsceneBlocker()
	
			--Flashwhite.
			Cutscene_CreateEvent("Flash Christine White", "Actor")
				ActorEvent_SetProperty("Subject Name", "Christine")
				ActorEvent_SetProperty("Flashwhite Quickly", "Null")
			DL_PopActiveObject()
			fnCutsceneBlocker()

			fnCutsceneWait(75)
			fnCutsceneBlocker()
			fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
			fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] I can transform myself, you see.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Wow, keen![P] That's amazing![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] This magical runestone here.[P] I don't know how it works, but it does.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Wow![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] By the way, what was that thing you were before?[P] I've heard a lot of stories from the patrols about weird creepies in the mines.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] I think the best term is 'dreamers', but they don't really have a name.[P] They're beyond names.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] That's kind of cool in a way.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Though I'd appreciate it if you kept it a secret.[P] I'm actually a golem, I just turned into a human for...[P] reasons.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] I don't think anyone would believe me, but don't worry.[P] Your secret is safe with me.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Very good.[P] Thank you.") ]])
			fnCutsceneBlocker()
        
        --Secrebot!
        elseif(sChristineForm == "Secrebot") then
		
			--Flags.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iSXKnowsChristineTransforms", "N", 1.0)
		
			--Dialogue.
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Christine![P] Wow, with all the ladders in the mines you choose to go around wearing those legs or wheels or whatever?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Blush] My cover lasted all of two seconds.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Not even.[P] So what happened, by the way?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] It's a bit of a long story, but suffice is to say, I'm a secrebot.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Never actually seen a working one, but sometimes you see parts from a scrapped one.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Yeah, and I'm glad you're not freaking out.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] I'm glad you're okay with being a secrebot.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] It's not permanent.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] I have the vague feeling you're about to really impress me.[B][C]") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
	
			--Flashwhite.
			Cutscene_CreateEvent("Flash Christine White", "Actor")
				ActorEvent_SetProperty("Subject Name", "Christine")
				ActorEvent_SetProperty("Flashwhite Quickly", "Null")
			DL_PopActiveObject()
			fnCutsceneBlocker()

			fnCutsceneWait(75)
			fnCutsceneBlocker()
			fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
			fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Keen![P] Tubular![P] How'd you do that?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] I have a magic runestone.[P] I don't know how it works, unfortunately.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Can you turn into other stuff?[P] Like, rocks and tubes and guns?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] No, only forms I've been before.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Can we transform you into a big gun?[P] That'd be really cool![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] I think you're missing the point here.[P] I'm a golem.[P] Always have been.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] ...[P] And?[P] You can transform so you're not really?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Yeah, but the steam droids won't trust me if I'm a golem.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Ohhhh, I get you.[P] Yeah, your secret is safe with me, secrebot Christine.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Awesome![P] I'm really putting a lot of trust in you, here.") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
            
        --Shocking!
        elseif(sChristineForm == "Electrosprite") then
			
			--Dialogue.
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Of all the things I expected to happen today, seeing you waltz up to me as a lightbulb was not one of them.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Oh, good.[P] You can tell it's me?[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Well yeah.[P] You've got an air to you that's hard to mistake.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Such a relief![P] I was worried you'd not believe me and we'd spend a hour doing ridiculous quizzes.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] So uh, what are you?[P] Did you run into a rogue lightbulb girl and she transformed you?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] I'm an electrosprite.[P] They're a species of intelligent electricity from a long-running computer experiment.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Really?[P] So the characters in video games are real people?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Not yet, unfortunately...[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Brief change of topic.[P] Watch this!") ]])
			fnCutsceneBlocker()
	
			--Flashwhite.
			Cutscene_CreateEvent("Flash Christine White", "Actor")
				ActorEvent_SetProperty("Subject Name", "Christine")
				ActorEvent_SetProperty("Flashwhite Quickly", "Null")
			DL_PopActiveObject()
			fnCutsceneBlocker()

			fnCutsceneWait(75)
			fnCutsceneBlocker()
			fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
			fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Tadaa![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] What in the world did I just watch happen?[P] How are you human again?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] This magical runestone here.[P] I don't know how it works, but it does.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Wow![P] That's pretty neat![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] So are you like, a human underneath the lightbulb?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Golem, actually![P] I just change forms when it's useful in a fight or otherwise.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Though I'd appreciate it if you kept it a secret.[P] Your steam droid compatriots are not fans of golems.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] I don't think anyone would believe me, but don't worry.[P] Your secret is safe with me.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Very good.[P] Thank you.") ]])
			fnCutsceneBlocker()
            
        --Yeah!
        elseif(sChristineForm == "SteamDroid") then
			
			--Dialogue.
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Nice NC+ trick, wow, good job.[P] The dialogue for the other un-gettable forms is a lot better, but you went for this one.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Offended] No need to be snippy.[P] We still have to do the mission in order, so play along.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Oh gee oh wow, you can transform.[P] Okay great.[P] Next scene, please.") ]])
			fnCutsceneBlocker()
	
			--Flashwhite.
			Cutscene_CreateEvent("Flash Christine White", "Actor")
				ActorEvent_SetProperty("Subject Name", "Christine")
				ActorEvent_SetProperty("Flashwhite Quickly", "Null")
			DL_PopActiveObject()
			fnCutsceneBlocker()

			fnCutsceneWait(75)
			fnCutsceneBlocker()
			fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
			fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Okay good, the flag is ticked. Moving on.") ]])
			fnCutsceneBlocker()
        
        --Unhandled case.
        else
			
			--Dialogue.
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Hey Christine![P] Looks like you're in a form that isn't handled yet.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Blush] Yeah.[P] Salty knows already and will be updating this later.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Typical.[P] Hey![P] Transform for me![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Okay.") ]])
			fnCutsceneBlocker()
	
			--Flashwhite.
			Cutscene_CreateEvent("Flash Christine White", "Actor")
				ActorEvent_SetProperty("Subject Name", "Christine")
				ActorEvent_SetProperty("Flashwhite Quickly", "Null")
			DL_PopActiveObject()
			fnCutsceneBlocker()

			fnCutsceneWait(75)
			fnCutsceneBlocker()
			fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
			fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Bim bop pow![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Tubular![P] Now the flag is set.[P] Resume playing the game!") ]])
			fnCutsceneBlocker()
    
		end
	
	--Black site is completed:
	elseif(iSteamDroid500RepState == 2.0) then
	
		--55 is here:
		if(iIs55Following == 1.0) then
			
			--Variables.
			local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
			if(iSXUpgradeQuest == 1.0) then
				
				--Dialogue.
				TA_SetProperty("Face Character", "PlayerEntity")
				fnCutscene([[ WD_SetProperty("Show") ]])
				fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
				fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
				fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
				fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
				fnCutscene([[ Append("SX-399:[E|Neutral] Ready to get this thing going?[BLOCK]") ]])

				--Decision script is this script. It must be surrounded by quotes.
				local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
				fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
				fnCutscene(" WD_SetProperty(\"Add Decision\", \"Let's Go\", " .. sDecisionScript .. ", \"Yes\") ")
				fnCutscene(" WD_SetProperty(\"Add Decision\", \"Not Yet\",  " .. sDecisionScript .. ", \"No\") ")
				fnCutsceneBlocker()
			
			--First time.
            elseif(iSXUpgradeQuest < 3.0) then
            
				--Flag.
				VM_SetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N", 1.0)
				
				--Dialogue.
				TA_SetProperty("Face Character", "PlayerEntity")
				fnCutscene([[ WD_SetProperty("Show") ]])
				fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
				fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
				fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
				fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
				fnCutscene([[ Append("SX-399:[E|Neutral] Everyone else has already come back from the job.[P] They said you were the rear guard.[B][C]") ]])
				fnCutscene([[ Append("SX-399:[E|Neutral] So...[P] did you...[B][C]") ]])
				fnCutscene([[ Append("55:[E|Neutral] Mission successful.[P] We located the blueprints we require.[B][C]") ]])
				fnCutscene([[ Append("Christine:[E|Neutral] 55, about that.[P] Did you know that they'd be there?[B][C]") ]])
				fnCutscene([[ Append("55:[E|Neutral] A guess.[P] I managed to access some transit manifests and noticed that a disproportionately high number of Steam Droid prisoners were diverted to that site.[B][C]") ]])
				fnCutscene([[ Append("55:[E|Neutral] There must have been a reason.[P] I did not think they would set up the site in a disused workshop.[B][C]") ]])
				fnCutscene([[ Append("Christine:[E|Neutral] We saw some things in there...[B][C]") ]])
				fnCutscene([[ Append("SX-399:[E|Neutral] Mother said you rescued four prisoners while you were in there.[B][C]") ]])
				fnCutscene([[ Append("SX-399:[E|Neutral] If this doesn't work out, or something goes wrong, then I just want you to know that you're real heroes.[B][C]") ]])
				fnCutscene([[ Append("55:[E|Neutral] We are not heroes.[P] We are performing the tasks laid out for us in the most efficient manner possible.[B][C]") ]])
				fnCutscene([[ Append("SX-399:[E|Neutral] Humble, tough, and cute.[P] Shame you're not much of a sweet-talker.[B][C]") ]])
				fnCutscene([[ Append("55:[E|Upset] !!![B][C]") ]])
				fnCutscene([[ Append("Christine:[E|Blush] (Uh oh, better change the subject!)[B][C]") ]])
				fnCutscene([[ Append("Christine:[E|Smirk] So, I took a photo of the blueprints we found.[P] Sophie thinks she can scavenge up some parts, but I can't make the power core until I figure out what these symbols mean.[B][C]") ]])
				fnCutscene([[ Append("SX-399:[E|Neutral] Hmmm, oh![P] Ha ha, these are how we used to denote volume, pressure, and temperature way back when.[B][C]") ]])
				fnCutscene([[ Append("SX-399:[E|Neutral] Just do this, do that, carry the y...[P] There.[B][C]") ]])
				fnCutscene([[ Append("Christine:[E|Neutral] ...[P] I feel like such a dope.[B][C]") ]])
				fnCutscene([[ Append("55:[E|Smirk] ...[P] Therefore, we can begin the transformation.[B][C]") ]])
				fnCutscene([[ Append("SX-399:[E|Neutral] Yes, but I'll need to supervise.[P] I'll know how to revert you if you make a mistake.[B][C]") ]])
				fnCutscene([[ Append("SX-399:[E|Neutral] So, are you ready?[BLOCK]") ]])

				--Decision script is this script. It must be surrounded by quotes.
				local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
				fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
				fnCutscene(" WD_SetProperty(\"Add Decision\", \"Let's Go\", " .. sDecisionScript .. ", \"Yes\") ")
				fnCutscene(" WD_SetProperty(\"Add Decision\", \"Not Yet\",  " .. sDecisionScript .. ", \"No\") ")
				fnCutsceneBlocker()
                
            --Finale has completed, SX-399 is a Steam Lord.
            else
				
                --Flags.
                local iTalkedAfterFinale = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedAfterFinale", "N")
                
				--Dialogue.
                if(iTalkedAfterFinale == 0.0) then
                    VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedAfterFinale", "N", 1.0)
                    TA_SetProperty("Face Character", "PlayerEntity")
                    fnCutscene([[ WD_SetProperty("Show") ]])
                    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
                    fnCutscene([[ Append("SX-399:[E|Happy] Hey guys![P] How are you?[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Neutral] I was meaning to ask you the same.[P] The new body is holding up well?[B][C]") ]])
                    fnCutscene([[ Append("SX-399:[E|Happy] I've never felt better, and I'm the envy of the town![B][C]") ]])
                    fnCutscene([[ Append("SX-399:[E|Smirk] ...[P] You know, I don't think I've ever had the chance to brag about something.[P] Do you think the other droids resent me?[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Neutral] Doubtless some of them do, but you can't let that hold you back.[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Smirk] If you're kind and compassionate, that resentment will fade.[P] If you are cruel and vain, it will fester.[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Smirk] How they see you will, eventually, reflect how you are.[P] Be the person they love.[B][C]") ]])
                    fnCutscene([[ Append("SX-399:[E|Neutral] Are you speaking from experience here?[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Sad] ...[P] I wish I was.[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Sad] In truth, there will be those who resent you forever.[P] You can do nothing about that.[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Sad] It's not that they hate you, it's that they hate what they think you are, and they hate it so much that they don't care if it's real or not.[B][C]") ]])
                    fnCutscene([[ Append("55:[E|Neutral] You could have them imprisoned.[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Offended] 55, no![P] Sheesh![B][C]") ]])
                    fnCutscene([[ Append("55:[E|Smirk] Those who speak ill of SX-399 are a problem to be eliminated.[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Offended] Really?[P] Because this sounds like an overprotective girlfriend talking.[B][C]") ]])
                    fnCutscene([[ Append("SX-399:[E|Smirk] Speaking of...[P] *rub*[B][C]") ]])
                    fnCutscene([[ Append("55:[E|Offended] Ngh![P] No![P] I retract my statement![B][C]") ]])
                    fnCutscene([[ Append("55:[E|Neutral] I will not act on this without explicit orders.[P] The Steam Droids may think what they like.[B][C]") ]])
                    fnCutscene([[ Append("SX-399:[E|Flirt] Your reinforced chassis is putty in my hands...[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Neutral] Come on, 55.[P] We better get back to it before you melt.[B][C]") ]])
                    fnCutscene([[ Append("SX-399:[E|Smirk] Good luck out there.[P] I'll keep to my training, and some day, I'll be the one who saves you two!") ]])
                else
                    TA_SetProperty("Face Character", "PlayerEntity")
                    fnCutscene([[ WD_SetProperty("Show") ]])
                    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
                    fnCutscene([[ Append("SX-399:[E|Happy] I'll hold down the fort, just leave Sprocket City's defense to me!") ]])
                end
			end
	
		--55 is not here, go get her!
		else
			
			--Variables.
			local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
			if(iSXUpgradeQuest == 1.0) then
				TA_SetProperty("Face Character", "PlayerEntity")
				fnCutscene([[ WD_SetProperty("Show") ]])
				fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
				fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
				fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
				fnCutscene([[ Append("SX-399:[E|Neutral] You had better go get 55, she can protect us while you're making your power core.") ]])
				fnCutsceneBlocker()
				
            elseif(iSXUpgradeQuest < 3.0) then
				TA_SetProperty("Face Character", "PlayerEntity")
				fnCutscene([[ WD_SetProperty("Show") ]])
				fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
				fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
				fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
				fnCutscene([[ Append("SX-399:[E|Neutral] Everyone else has already come back from the job.[P] They said you were the rear guard.[B][C]") ]])
				fnCutscene([[ Append("SX-399:[E|Neutral] So...[P] did you...[B][C]") ]])
				fnCutscene([[ Append("Christine:[E|Neutral] Yep, I'll go get 55 and we can begin.[B][C]") ]])
				fnCutscene([[ Append("SX-399:[E|Neutral] Just, keen![P] Eee![P] I feel lighter than air!") ]])
				fnCutsceneBlocker()
                
            --Finale has completed, SX-399 is a Steam Lord.
            else
				
				--Dialogue.
				TA_SetProperty("Face Character", "PlayerEntity")
				fnCutscene([[ WD_SetProperty("Show") ]])
				fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
				fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
				fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
				fnCutscene([[ Append("SX-399:[E|Neutral] Uh, it really shouldn't be possible to see this dialogue.[B][C]") ]])
				fnCutscene([[ Append("SX-399:[E|Neutral] Better report it as a bug.") ]])
				fnCutsceneBlocker()
			end
		end
	
	--No special scenes:
	else
	
		--The plan has been hatched:
		if(iPlanHatched == 1.0) then
			
			--55 is here:
			if(iIs55Following == 1.0) then
				TA_SetProperty("Face Character", "PlayerEntity")
				fnCutscene([[ WD_SetProperty("Show") ]])
				fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
				fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
				fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
				fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
				fnCutscene([[ Append("SX-399:[E|Neutral] This plan...[P] It'll work, right?[P] I won't have to...[P] to...[B][C]") ]])
				fnCutscene([[ Append("55:[E|Neutral] It will work.[P] We will save you.[B][C]") ]])
				fnCutscene([[ Append("SX-399:[E|Neutral] Thank you so much...") ]])
				fnCutsceneBlocker()
			
			--55 is not here:
			else
				TA_SetProperty("Face Character", "PlayerEntity")
				fnCutscene([[ WD_SetProperty("Show") ]])
				fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
				fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
				fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
				fnCutscene([[ Append("SX-399:[E|Neutral] Christine, everything changed the moment I met you and 55.[P] If this plan doesn't work, or something happens...[B][C]") ]])
				fnCutscene([[ Append("Christine:[E|Neutral] It will work.[B][C]") ]])
				fnCutscene([[ Append("SX-399:[E|Neutral] Yes, but -[P] just, thank you.[P] Thank you for everything.") ]])
				fnCutsceneBlocker()
	
			end
	
		--If 55 is following:
		elseif(iIs55Following == 1.0) then
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Stick around for a while, 55.[P] Have you got any stories about your adventures?[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] ...") ]])
			fnCutsceneBlocker()
			
		--55 isn't following.
		elseif(iSXMet55 == 1.0 and iIs55Following == 0.0) then
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Christine...[P] Do you think 55 likes me?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] It's difficult to get a read on her, honestly.[P] You need to be patient with her.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] When I touch her, she goes stiff and acts all weird.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] She's not used to affection, believe me...") ]])
			fnCutsceneBlocker()
		
		elseif(iSteamDroid250RepState == 3.0) then
			VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroid250RepState", "N", 4.0)
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] Christine! Hello![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] How are things going with the traps?[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] I'll be honest.[P] This isn't the most entertaining work there is.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] But?[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] But I saw BI-102 earlier, and she nodded at me![P] Like I was doing a good job![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Great![B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] I'm still getting used to this.[P] I have to run along the trap lines and make sure they're all lubricated and that the springs aren't oversensitive.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] But I get to set my own hours and I don't have to sneak out![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] You've got responsibility now, so no slacking off.[P] Sprocket City is counting on you.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] I'll keep the city safe, don't worry.[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] And thank you for talking to mother for me.[P] She can be very obstinate.") ]])
			fnCutsceneBlocker()
				
		elseif(iSteamDroid250RepState == 4.0) then
			TA_SetProperty("Face Character", "PlayerEntity")
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] BI-102 mentioned you fell into this trap earlier.[P] Is that true?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Errrr....[B][C]") ]])
			fnCutscene([[ Append("SX-399:[E|Neutral] I'm amazed you survived![P] You must be as sturdy as a golem![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] You don't know the half of it...") ]])
			fnCutsceneBlocker()
		end
	end

--Begin the Steam Droid transformation.
elseif(sTopicName == "Yes") then
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Let's get to work.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] I will retrieve the parts from Unit 499323.[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] There's a spot in the mines we can use, all we need to do is tap into Sprocket City's steam pipes.[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] 55, meet us there when you've got the parts.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N", 1.5)
	
	--Fade out.
	fnCutsceneWait(45)
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	
	--Remove 55 from the party.
	giFollowersTotal = 0
	gsaFollowerNames = {}
	giaFollowerIDs = {0}
	AL_SetProperty("Unfollow Actor Name",  "Tiffany")
	VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
	
	--Execute Steam Droid transformation.
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/500 Normal/Christine To Steam Droid/Scene_Begin.lua") ]])
	fnCutsceneBlocker()

--Cancel.
elseif(sTopicName == "No") then
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] I have a few loose-ends to tie up.[P] I'll be back.[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] Don't leave me waiting too long, hun.") ]])
end
