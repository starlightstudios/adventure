-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.
-- Note that this is used for chat dialogues, not the part in Sector 198.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName  - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
-- 1: sIsFireside - Optional. If it exists, this was activated from the AdventureMenu at a save point. In those cases, the 
--                  Active Object is undefined.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName  = LM_GetScriptArgument(0)
local sIsFireside = LM_GetScriptArgument(1)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Change music to Florentina's theme.
	glLevelMusic = AL_GetProperty("Music")
	AL_SetProperty("Music", "SophiesTheme")
	
	--Position party opposite one another.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
	
	--Florentina's introduction.
	fnCutscene([[ Append("Christine:[E|Smirk] Dearest, you look like you have something to say.[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Neutral] Me?[P] No, I couldn't.[P] I don't want to interrupt.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] Balderdash![P] This is a rest stop, let's pass the time.[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Smirk] All right![P] What do you want to talk about?[B][C]") ]])

	-- |[Topics]|
	--Activate topics mode once the dialogue is complete.
    fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Sophie") ]])
end
