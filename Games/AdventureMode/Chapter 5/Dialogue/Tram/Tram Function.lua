-- |[ ====================================== fnRunTram() ======================================= ]|
--Function used to make the player's party enter the tram and order the tram to move. Handles various
-- cases like the party needing to enter or exit.
function fnRunTram(pbIsSector254)

    -- |[ ======================== Setup ========================= ]|
    -- |[Arguments]|
    if(pbIsSector254 == nil) then pbIsSector254 = false end

    -- |[Variables]|
    local b55Joined = true
    local iIs55Following           = VM_GetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N")
    local iStartedShoppingSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N")
    local iIsOnDate                = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
    local iIsGalaTime              = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    local sCurrentTramArea         = VM_GetVar("Root/Variables/Chapter5/Scenes/sCurrentTramArea", "S")
    local bIsSophiePresent         = EM_Exists("Sophie")
    
    -- |[Shopping Sequence]|
    --Shopping sequence. Precludes everything else, 55 does not join up.
    if(iStartedShoppingSequence == 1.0 and iIsOnDate == 1.0 or iIsGalaTime > 0.0) then
        b55Joined = false
        
    --Shopping end. 55 does not join up.
    elseif(iStartedShoppingSequence == 2.0) then
        b55Joined = false
    
    -- |[55 Should Leave]|
    --Sector 254 diallows 55 from being on the tram.
    elseif(pbIsSector254) then
    
        --Flag.
        b55Joined = false
        VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
    
        --If 55 is not in the party we don't need to do anything. Otherwise, she needs to leave.
        if(iIs55Following == 1.0) then
        
            --Sector 198: 55 tells Christine to go ahead.
            if(sCurrentTramArea == "RegulusCity198A") then
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ Append("55:[VOICE|Tiffany] Please proceed to Sector 254 without me.[P] I will be in touch.") ]])
                fnCutsceneBlocker()
            
            --All other cases:
            else
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ Append("55:[VOICE|Tiffany] If you are going to Sector 254, I will take another tram.[P] Be alert.") ]])
                fnCutsceneBlocker()
            end
        end
        
        --Remove from party lineup.
        giFollowersTotal = 0
        gsaFollowerNames = {}
        giaFollowerIDs = {0}
        AL_SetProperty("Unfollow Actor Name", "Tiffany")
    
    -- |[55 Should Join]|
    --55 joins the party.
    elseif(iIs55Following == 0.0) then
    
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 1.0)
        
        --In sector 198, 55 just walks up to the tram.
        if(sCurrentTramArea == "RegulusCity198A") then
            
            --Remove 55's collision and activation script.
            EM_PushEntity("Tiffany")
                TA_SetProperty("Clipping Flag", false)
                TA_SetProperty("Activation Script", "Null")
            DL_PopActiveObject()
            
            --55 walks up to Christine.
            fnCutsceneFace("Christine", 1, 1)
            fnCutsceneMove("Tiffany", 25.25, 10.50)
            fnCutsceneFace("Tiffany", 0, -1)
            fnCutsceneBlocker()
            
            --Open the door.
            if(AL_GetProperty("Is Door Open", "Door") == false) then
                fnCutsceneWait(5)
                fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
                fnCutscene([[ AL_SetProperty("Open Door", "Door") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(5)
            end
        
            --Move 55.
            fnCutsceneMove("Tiffany", 25.25, 8.50)
            fnCutsceneMove("Tiffany", 19.25, 8.50)
            fnCutsceneBlocker()
            fnCutsceneFace("Christine", 0, -1)
            fnCutsceneFace("Tiffany", 0, -1)
            fnCutsceneWait(25)
    
            --Set Lua globals.
            giFollowersTotal = 1
            gsaFollowerNames = {"Tiffany"}
            giaFollowerIDs = {0}

            --Get 55's uniqueID.
            EM_PushEntity("Tiffany")
                local i55ID = RE_GetID()
            DL_PopActiveObject()

            --Store it and tell her to follow.
            giaFollowerIDs = {i55ID}
            AL_SetProperty("Follow Actor ID", i55ID)
        
        --Sector 254, 55 sends a message.
        elseif(sCurrentTramArea == "Sector254A") then
        
            --Normal case:
            local i254FoundPDU = VM_GetVar("Root/Variables/Chapter5/Scenes/i254FoundPDU", "N")
            if(i254FoundPDU == 0.0) then
            
                --Flag.
                b55Joined = false
            
                --Dialogue.
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ Append("Christine:[VOICE|Christine] PDU, please contact 55 and tell her to meet me at switching station R-21.[B][C]") ]])
                fnCutscene([[ Append("PDU:[VOICE|Narrator] Affirmative.[P] 55 has replied she is on her way.") ]])
                fnCutsceneBlocker()
                
                --Spawn her offscreen so her ID can be added.
                fnSpecialCharacter("Tiffany", -100, -100, gci_Face_North, false, nil)
        
                --Set Lua globals.
                giFollowersTotal = 1
                gsaFollowerNames = {"Tiffany"}
                giaFollowerIDs = {0}

                --Get 55's uniqueID.
                EM_PushEntity("Tiffany")
                    local i55ID = RE_GetID()
                DL_PopActiveObject()

                --Store it and tell her to follow.
                giaFollowerIDs = {i55ID}
                AL_SetProperty("Follow Actor ID", i55ID)
            
            --Sophie is leaving the sector.
            else
                b55Joined = false
                io.write("Leaving sector.\n")
            end
        
        --Otherwise, she spawns and approached Christine.
        else
        
            --Spawn her.
            fnSpecialCharacter("Tiffany", -100, -100, gci_Face_North, false, nil)
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("55:[VOICE|Tiffany] I will be right there.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(45)
            fnCutsceneBlocker()
            
            --Christine looks over.
            fnCutsceneFace("Christine", 1, 1)
            
            --55 looks around.
            fnCutsceneTeleport("Tiffany", 25.25, 8.50)
            fnCutsceneWait(45)
            fnCutsceneBlocker()
            fnCutsceneFace("Tiffany", 1, 0)
            fnCutsceneWait(45)
            fnCutsceneBlocker()
            fnCutsceneFace("Tiffany", -1, 0)
            fnCutsceneWait(45)
            fnCutsceneBlocker()
            
            --55 joins Christine on the tram.
            fnCutsceneMove("Tiffany", 19.25, 8.50)
            fnCutsceneBlocker()
            fnCutsceneFace("Christine", 0, 1)
            fnCutsceneFace("Tiffany", 0, -1)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("55:[VOICE|Tiffany] Let's get going.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
    
            --Set Lua globals.
            giFollowersTotal = 1
            gsaFollowerNames = {"Tiffany"}
            giaFollowerIDs = {0}

            --Get 55's uniqueID.
            EM_PushEntity("Tiffany")
                local i55ID = RE_GetID()
            DL_PopActiveObject()

            --Store it and tell her to follow.
            giaFollowerIDs = {i55ID}
            AL_SetProperty("Follow Actor ID", i55ID)
        end
    end

    -- |[ =================== Scene Execution ==================== ]|
    -- |[Position Setup]|
    --Constants.
    local fEnterTramX     = 19.25
    local fEnterTramY     =  7.00
    local fEnterTramFaceX =     0
    local fEnterTramFaceY =    -1
    local fCameraFocusX   = 19.25
    local fCameraFocusY   =  7.50
    local fTramDepartX    = 17.25
    local fTramDepartY    =  6.50
    
    --Change constants for certain areas.
    if(sCurrentTramArea == "Sector254A") then
        fEnterTramX     = 15.75
        fEnterTramY     = 11.50
        fEnterTramFaceX =     0
        fEnterTramFaceY =     1
        fCameraFocusX   = 15.75
        fCameraFocusY   = 11.50
        fTramDepartX    = 13.75
        fTramDepartY    = 13.00
    end
    
    --List of characters.
    local saCharList = {"Christine"}
    if(b55Joined == false and bIsSophiePresent == true) then table.insert(saCharList, "Sophie")  end
    if(b55Joined == true)                               then table.insert(saCharList, "Tiffany") end
    
    -- |[Characters Enter Tram]|
    --Group gets on the tram.
    for i = 1, #saCharList, 1 do
        fnCutsceneMove(saCharList[i], fEnterTramX, fEnterTramY)
    end
    fnCutsceneBlocker()
    for i = 1, #saCharList, 1 do
        fnCutsceneFace(saCharList[i], fEnterTramFaceX, fEnterTramFaceY)
    end
    fnCutsceneBlocker()
    fnCutsceneWait(15)
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Focus Position", (fCameraFocusX * gciSizePerTile), (fCameraFocusY * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    
    --Teleport group offscreen.
    for i = 1, #saCharList, 1 do
        fnCutsceneTeleport(saCharList[i], -100.25, -100.50)
    end
    fnCutsceneBlocker()
    
    --Wait a bit.
    fnCutsceneWait(55)
    fnCutsceneBlocker()
    
    -- |[Tram Leaves]|
    --Tram moves offscreen.
    fnCutscene([[ AudioManager_PlaySound("World|TramStart") ]])
    fnCutsceneMoveFace("TramA", fTramDepartX + 0.0 - 3.0, fTramDepartY, -1, 0, 1.10)
    fnCutsceneMoveFace("TramB", fTramDepartX + 1.0 - 3.0, fTramDepartY, -1, 0, 1.10)
    fnCutsceneMoveFace("TramC", fTramDepartX + 2.0 - 3.0, fTramDepartY, -1, 0, 1.10)
    fnCutsceneMoveFace("TramD", fTramDepartX + 3.0 - 3.0, fTramDepartY, -1, 0, 1.10)
    fnCutsceneMoveFace("TramE", fTramDepartX + 4.0 - 3.0, fTramDepartY, -1, 0, 1.10)
    fnCutsceneMoveFace("TramF", fTramDepartX + 5.0 - 3.0, fTramDepartY, -1, 0, 1.10)
    fnCutsceneBlocker()
    
    --Trams speeds up.
    fnCutsceneMoveFace("TramA", fTramDepartX + 0.0 - 30.0, fTramDepartY, -1, 0, 2.50)
    fnCutsceneMoveFace("TramB", fTramDepartX + 1.0 - 30.0, fTramDepartY, -1, 0, 2.50)
    fnCutsceneMoveFace("TramC", fTramDepartX + 2.0 - 30.0, fTramDepartY, -1, 0, 2.50)
    fnCutsceneMoveFace("TramD", fTramDepartX + 3.0 - 30.0, fTramDepartY, -1, 0, 2.50)
    fnCutsceneMoveFace("TramE", fTramDepartX + 4.0 - 30.0, fTramDepartY, -1, 0, 2.50)
    fnCutsceneMoveFace("TramF", fTramDepartX + 5.0 - 30.0, fTramDepartY, -1, 0, 2.50)
    fnCutsceneBlocker()
    
    --Darken the screen.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
end