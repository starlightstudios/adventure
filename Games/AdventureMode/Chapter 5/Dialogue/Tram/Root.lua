-- |[ ===================================== Tram Dialogue ====================================== ]|
--When examining the tram in Regulus City, this dialogue script is called regardless of which tram
-- the player is actually at. This is a dialogue script as the tram is an NPC.
--The normal functionality is to list off the destinations and then fire a cutscene to take the player
-- to the given location.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[ =================================== Function Handling ==================================== ]|
--Create a blank function. Also overrides the function if it was not cleared correctly.
function fnRunTram(pbIsSector254)
end

--If the player chooses to go to a destination, update the function to handle the possible cases.
if(sTopicName ~= "Hello" and sTopicName ~= "Nevermind") then
	LM_ExecuteScript(fnResolvePath() .. "Tram Function.lua")
end

-- |[ =================================== List Destinations ==================================== ]|
--"Hello", standard entry topic. The player is usually given a list of locations to go to, or a
-- reason why they can't use the tram.
if(sTopicName == "Hello") then
	
	-- |[Variables]|
	local sCurrentTramArea         = VM_GetVar("Root/Variables/Chapter5/Scenes/sCurrentTramArea", "S")
    local iStartedShoppingSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N")
    local iIsOnDate                = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
    local iIsGalaTime              = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    local iManuTookJob             = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuTookJob", "N")
    local iSawSector99Scene        = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawSector99Scene", "N")
    local i254TalkedToSophie       = VM_GetVar("Root/Variables/Chapter5/Scenes/i254TalkedToSophie", "N")
    local i254FoundPDU             = VM_GetVar("Root/Variables/Chapter5/Scenes/i254FoundPDU", "N")
    local i254FixedChristine       = VM_GetVar("Root/Variables/Chapter5/Scenes/i254FixedChristine", "N")
    
    -- |[Cannot Use Tram]|
    --If the player disembarked at Sector 48 during the Manufactory subquest:
    if(sCurrentTramArea == "RegulusManufactoryA") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](The tram is out of service and will be redirected soon.[P] I should see if there's another way to Sector 99.)") ]])
        fnCutsceneBlocker()
        fnRunTram = nil
        return
    
    --If the player is in Sector 119 but hasn't completed the subquest yet:
    elseif(iStartedShoppingSequence < 1.0 and sCurrentTramArea == "RegulusCity119A") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](We should go ask around the shops and try to find some dressmaking materials before we leave.)[BLOCK]") ]])
        fnCutsceneBlocker()
        fnRunTram = nil
        return
    
    --Can't leave the gala.
    elseif(sCurrentTramArea == "RegulusArcaneA") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](No going back.[P] We have a job to do.)") ]])
        fnCutsceneBlocker()
        fnRunTram = nil
        return
    
    --Can't use the tram when in sector 96 as Sophie.
    elseif(sCurrentTramArea == "RegulusCityF" and i254FoundPDU == 1.0 and i254FixedChristine == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Sophie](I had better get Christine to the repair bay!)") ]])
        fnCutsceneBlocker()
        fnRunTram = nil
        return
    
    --If used when Sophie is leaving Sector 254, no option is presented.
    elseif(sCurrentTramArea == "Sector254A" and i254FoundPDU == 1.0) then
        fnRunTram(false)
        fnCutscene([[ AL_BeginTransitionTo("RegulusCityF", gsRoot .. "Maps/RegulusCity/RegulusCityF/Tram_Arrive.lua") ]])
        fnCutsceneBlocker()
        return
    end
	
    -- |[Common]|
	--In all cases.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Christine](Where shall I take the tram to?)[BLOCK]") ]])
    
    --Gala. Only that is available.
    if(iIsGalaTime > 0.0) then
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Arcane University\", " .. sDecisionScript .. ", \"Arcane University\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Nevermind\",  " .. sDecisionScript .. ", \"Nevermind\") ")
        fnCutsceneBlocker()
        fnRunTram = nil
        return
    end
    
    --Shopping sequence! Restricts options.
    if((iStartedShoppingSequence == 1.0 and iIsOnDate == 1.0) or (iStartedShoppingSequence == 2.0)) then
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        if(sCurrentTramArea ~= "RegulusCityF") then
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Regulus City Sector 96\", " .. sDecisionScript .. ", \"Regulus City Sector 96\") ")
        end
        if(sCurrentTramArea ~= "RegulusCity119A") then
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Regulus City Sector 119\", " .. sDecisionScript .. ", \"Regulus City Sector 119\") ")
        end
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Nevermind\",  " .. sDecisionScript .. ", \"Nevermind\") ")
        fnCutsceneBlocker()
        fnRunTram = nil
        return
    
    --Shopping sequence is 3.0.
    elseif(iStartedShoppingSequence == 3.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](I should take Sophie back to the repair bay before I go anywhere else.)") ]])
        fnCutsceneBlocker()
        fnRunTram = nil
        return
    end

    -- |[Normal Destination List]|
	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
	if(sCurrentTramArea ~= "RegulusCityF") then
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Regulus City Sector 96\", " .. sDecisionScript .. ", \"Regulus City Sector 96\") ")
	end
	if(sCurrentTramArea ~= "RegulusCity15A") then
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Regulus City Sector 15\", " .. sDecisionScript .. ", \"Regulus City Sector 15\") ")
	end
	if(sCurrentTramArea ~= "RegulusCity198A") then
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Regulus City Sector 198\", " .. sDecisionScript .. ", \"Regulus City Sector 198\") ")
	end
	if(sCurrentTramArea ~= "RegulusExteriorStationE") then
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Telemetry Facility\", " .. sDecisionScript .. ", \"Telemetry Facility\") ")
	end
	if(sCurrentTramArea ~= "TelluriumMinesA") then
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Tellurium Mines\", " .. sDecisionScript .. ", \"Tellurium Mines\") ")
	end
    if(iManuTookJob == 1.0 and iSawSector99Scene == 0.0) then
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Regulus City Sector 99\", " .. sDecisionScript .. ", \"Regulus City Sector 99\") ")
    end
    if(i254TalkedToSophie == 1.0 and sCurrentTramArea ~= "Sector254A") then
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Regulus City Sector 254\", " .. sDecisionScript .. ", \"Regulus City Sector 254\") ")
    end
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Nevermind\",  " .. sDecisionScript .. ", \"Nevermind\") ")
	fnCutsceneBlocker()

-- |[ ================================= Destination Activation ================================= ]|
--Regulus City. Leads to Regulus City F.
elseif(sTopicName == "Regulus City Sector 96") then
	
	--Subroutine.
	WD_SetProperty("Hide")
	fnRunTram()
	
    --Scene that plays after the shopping sequence is over.
    local iStartedShoppingSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N")
    if(iStartedShoppingSequence == 2.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N", 3.0)
        LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/500 Normal/Sophie Tram Ride/Scene_Begin.lua")
    end
    
	--Transit to the next area.
	fnCutscene([[ AL_BeginTransitionTo("RegulusCityF", gsRoot .. "Maps/RegulusCity/RegulusCityF/Tram_Arrive.lua") ]])
	fnCutsceneBlocker()

--Sector 15.
elseif(sTopicName == "Regulus City Sector 15") then
	
	--Subroutine.
	WD_SetProperty("Hide")
	fnRunTram()
	
	--Transit to the next area.
	fnCutscene([[ AL_BeginTransitionTo("RegulusCity15A", gsRoot .. "Maps/RegulusCity/RegulusCity15A/Tram_Arrive.lua") ]])
	fnCutsceneBlocker()

--Sector 198.
elseif(sTopicName == "Regulus City Sector 198") then
	
	--Subroutine.
	WD_SetProperty("Hide")
	fnRunTram()
	
	--Transit to the next area.
	fnCutscene([[ AL_BeginTransitionTo("RegulusCity198A", gsRoot .. "Maps/RegulusCity/RegulusCity198A/Tram_Arrive.lua") ]])
	fnCutsceneBlocker()

--Sector 254.
elseif(sTopicName == "Regulus City Sector 254") then
	
	--Subroutine.
	WD_SetProperty("Hide")
	fnRunTram(true)
	
	--Transit to the next area.
	fnCutscene([[ AL_BeginTransitionTo("Sector254A", gsRoot .. "Maps/Chapter 5/Sector254/Sector254A/Tram_Arrive.lua") ]])
	fnCutsceneBlocker()

--Sector 73.
elseif(sTopicName == "Tellurium Mines") then
	
	--Subroutine.
	WD_SetProperty("Hide")
	fnRunTram()
	
	--Transit to the next area.
	fnCutscene([[ AL_BeginTransitionTo("TelluriumMinesA", gsRoot .. "Maps/RegulusMines/TelluriumMinesA/Tram_Arrive.lua") ]])
	fnCutsceneBlocker()

--Tellurium Mines, below Regulus City.
elseif(sTopicName == "Telemetry Facility") then
	
	--Subroutine.
	WD_SetProperty("Hide")
	fnRunTram()
	
	--Transit to the next area.
	fnCutscene([[ AL_BeginTransitionTo("RegulusExteriorStationE", gsRoot .. "Maps/RegulusExterior/RegulusExteriorStationE/Tram_Arrive.lua") ]])
	fnCutsceneBlocker()

--Telemetry Facility, eastern part of Regulus.
elseif(sTopicName == "Telemetry Facility") then
	
	--Subroutine.
	WD_SetProperty("Hide")
	fnRunTram()
	
	--Transit to the next area.
	fnCutscene([[ AL_BeginTransitionTo("RegulusExteriorStationE", gsRoot .. "Maps/RegulusExterior/RegulusExteriorStationE/Tram_Arrive.lua") ]])
	fnCutsceneBlocker()

--Sector 99, Manufactory sidequest.
elseif(sTopicName == "Regulus City Sector 99") then
	
	--Subroutine.
	WD_SetProperty("Hide")
	fnRunTram()
	
	--Transit to the next area.
	fnCutscene([[ AL_BeginTransitionTo("RegulusManufactoryA", gsRoot .. "Maps/RegulusManufactory/RegulusManufactoryA/Tram_Arrive.lua") ]])
	fnCutsceneBlocker()

--Regulus City. Leads to the shopping sector.
elseif(sTopicName == "Regulus City Sector 119") then
	
	--Subroutine.
	WD_SetProperty("Hide")
	fnRunTram()
	
	--Transit to the next area.
	fnCutscene([[ AL_BeginTransitionTo("RegulusCity119A", gsRoot .. "Maps/RegulusCity/RegulusCity119A/Tram_Arrive.lua") ]])
	fnCutsceneBlocker()
    
--To the gala!
elseif(sTopicName == "Arcane University") then
	
	--Subroutine.
	WD_SetProperty("Hide")
	fnRunTram()
	
	--Transit to the next area.
	fnCutscene([[ AL_BeginTransitionTo("RegulusArcaneA", gsRoot .. "Maps/RegulusArcane/RegulusArcaneA/Tram_Arrive.lua") ]])
	fnCutsceneBlocker()

--Nevermind. Cancels the action.
elseif(sTopicName == "Nevermind") then
	WD_SetProperty("Hide")

end

--Clean.
fnRunTram = nil