-- |[ ==================================== Location Handler ==================================== ]|
--Whenever the journal is opened to locations mode, generates location entries.

-- |[ ======================================== Loading ========================================= ]|
--If this flag is nil, then we need to attempt to load the location images.
DL_AddPath("Root/Images/RegionNotifiers/Permanent/")
local sBaseDLPath = "Root/Images/RegionNotifiers/Permanent/"

--Lookups of information needed to load the image.
local zaImageList = {}
table.insert(zaImageList, {"Cryogenics",          gsDatafilesPath .. "UIRegionCryogenics.slf",          "Cryogenics|21"})
table.insert(zaImageList, {"Arcane University",   gsDatafilesPath .. "UIRegionArcaneUniversity.slf",    "ArcaneUniversity|21"})
table.insert(zaImageList, {"Equinox",             gsDatafilesPath .. "UIRegionEquinox.slf",             "EquinoxLabs|21"})
table.insert(zaImageList, {"LRTFacility",         gsDatafilesPath .. "UIRegionLRTFacility.slf",         "LRTFacility|21"})
table.insert(zaImageList, {"RaijuRanch",          gsDatafilesPath .. "UIRegionBiolabsRaijuRanch.slf",   "RaijuRanch|21"})
table.insert(zaImageList, {"RegulusCity15",       gsDatafilesPath .. "UIRegionRegulusCitySector15.slf", "RegulusCitySector15|21"})
table.insert(zaImageList, {"RegulusCity96",       gsDatafilesPath .. "UIRegionRegulusCitySector96.slf", "RegulusCitySector96|21"})
table.insert(zaImageList, {"SerenityObservatory", gsDatafilesPath .. "UIRegionSerenityObservatory.slf", "SerenityObservatory|21"})
table.insert(zaImageList, {"BiolabsAlpha",        gsDatafilesPath .. "UIRegionBiolabsAlpha.slf",        "BiolabsAlpha|21"})
table.insert(zaImageList, {"BiolabsBeta",         gsDatafilesPath .. "UIRegionBiolabsBeta.slf",         "BiolabsBeta|21"})
table.insert(zaImageList, {"BiolabsGamma",        gsDatafilesPath .. "UIRegionBiolabsGamma.slf",        "BiolabsGamma|21"})
table.insert(zaImageList, {"BiolabsDelta",        gsDatafilesPath .. "UIRegionBiolabsDelta.slf",        "BiolabsDelta|21"})
table.insert(zaImageList, {"BiolabsEpsilon",      gsDatafilesPath .. "UIRegionBiolabsEpsilon.slf",      "BiolabsEpsilon|21"})
table.insert(zaImageList, {"BiolabsDatacore",     gsDatafilesPath .. "UIRegionBiolabsDatacore.slf",     "BiolabsDatacore|21"})
table.insert(zaImageList, {"BiolabsHydroponics",  gsDatafilesPath .. "UIRegionBiolabsHydroponics.slf",  "BiolabsHydroponics|21"})
table.insert(zaImageList, {"BiolabsGenetics",     gsDatafilesPath .. "UIRegionAquaticGenetics.slf",     "AquaticGenetics|21"})
    
--Scan the image list.
for i = 1, #zaImageList, 1 do
    
    --Fast-access.
    local sImageName  = zaImageList[i][1]
    local sSLFPath    = zaImageList[i][2]
    local sInfileName = zaImageList[i][3]
    local sImagePath = sBaseDLPath .. sImageName
    
    --If the given image does not exist, order it to load.
    if(DL_Exists(sImagePath) == false) then
        
        --Open the requisite SLF file.
        SLF_Open(sSLFPath)
        
        --Create a bitmap entry.
        DL_ExtractDelayedBitmap(sInfileName, sImagePath)
        
        --Order that entry to load at the next opportunity.
        DL_LoadDelayedBitmap(sImagePath, gciDelayedLoadLoadAtEndOfTick)
    end
end

--Close the SLF file in case it was opened.
SLF_Close()

-- |[ ===================================== Entry Listing ====================================== ]|
--Entries must be re-created each time the file is opened in case a mod needs to add new ones.
local zJournalEntry = nil
gzaCh5LocationEntries = {}

--Set globals.
JournalEntry.zaAppendList = gzaCh5LocationEntries

-- |[ ============= Fake Listing ============== ]|
--During the reprogramming sequence, a series of fake entries are placed instead of real locations.
local iMainQuestBlock = VM_GetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N")
if(iMainQuestBlock == 60) then
    
    --Set fae entries.
    zJournalEntry = JournalEntry:new("Fake0", "Uoos-")
    zJournalEntry:fnSetVariable("TRUE")
    zJournalEntry:fnSetToLocation("Root/Images/RegionNotifiers/Permanent/Cryogenics", 0, 0)
    zJournalEntry:fnSetDescription("Data unavailable.")
    
    zJournalEntry = JournalEntry:new("Fake1", "England")
    zJournalEntry:fnSetVariable("TRUE")
    zJournalEntry:fnSetToLocation("Root/Images/RegionNotifiers/Permanent/BiolabsGenetics", 0, 0)
    zJournalEntry:fnSetDescription("This is almost certainly not England.")
    
    zJournalEntry = JournalEntry:new("Fake2", "Above? Above? Above?")
    zJournalEntry:fnSetVariable("TRUE")
    zJournalEntry:fnSetToLocation("Root/Images/RegionNotifiers/Permanent/Equinox", 0, 0)
    zJournalEntry:fnSetDescription("Data retrieval error. What could the text say?")
    
    --Upload.
    for i = 1, #gzaCh5LocationEntries, 1 do
        gzaCh5LocationEntries[i]:fnUploadData()
    end

    --Clean.
    JournalEntry:fnCleanGlobals()

    --Clear the list to save memory.
    gzaCh5LocationEntries = nil
    return
end

-- |[ =============== Listing ================ ]|
--Cryogenics.
zJournalEntry = JournalEntry:new("Cryogenics", "Cryogenics Facility")
zJournalEntry:fnSetVariable("Root/Variables/Regions/Cryogenics/iHasSeenThisRegion")
zJournalEntry:fnSetToLocation("Root/Images/RegionNotifiers/Permanent/Cryogenics", 0, 0)
zJournalEntry:fnSetDescription(""..
    "A research facility concerned with freezing and repairing of organic[BR]tissue. Construction was not finished before calamity struck.[BR][BR]" ..
    "The facility consists of three floors. The uppermost is the 'command'[BR]deck, with the computer servers and surveillance equipment. The main[BR]" ..
    "floor contains the research labs and crew quarters. Below that is the[BR]power generation and fluid transfer floor, carved into natural caves[BR]" ..
    "beneath the surface.[BR][BR]" ..
    "Power is intermittent within the building due to damage to the[BR]generators. Malfunctioning security robots patrol the corridors,[BR]attacking anything " ..
    "they see. Retired golems litter the hallways.")

--Sector 96.
zJournalEntry = JournalEntry:new("RegulusCity96", "Regulus City Sector 96")
zJournalEntry:fnSetVariable("Root/Variables/Regions/RegulusCity96/iHasSeenThisRegion")
zJournalEntry:fnSetToLocation("Root/Images/RegionNotifiers/Permanent/RegulusCity96", 0, 0)
zJournalEntry:fnSetDescription(""..
    "Sector 96 is along the southernmost edges of Regulus City. It is a[BR]mixed sector responsible for light industry, research, power, and[BR]repairs.[BR]\n"..
    "It is the sector closest to the Cryogenics facility. It is also[BR]Christine's home sector.")

--Sector 15.
zJournalEntry = JournalEntry:new("RegulusCity15", "Regulus City Sector 15")
zJournalEntry:fnSetVariable("Root/Variables/Regions/RegulusCity15/iHasSeenThisRegion")
zJournalEntry:fnSetToLocation("Root/Images/RegionNotifiers/Permanent/RegulusCity15", 0, 0)
zJournalEntry:fnSetDescription(""..
    "Sector 15 sits in the southwestern corner of Regulus City, just north of[BR]Nodoru Overlook, where the unstable terrain make large-scale[BR]construction difficult.[BR][BR]"..
    "A manufacturing and storage sector, it has fallen into disrepair as of[BR]late. Poor management has left scrap and garbage throughout the[BR]corridors.")

--Equinox.
zJournalEntry = JournalEntry:new("Equinox", "Equinox Labs")
zJournalEntry:fnSetVariable("Root/Variables/Regions/Equinox/iHasSeenThisRegion")
zJournalEntry:fnSetToLocation("Root/Images/RegionNotifiers/Permanent/Equinox", 0, 0)
zJournalEntry:fnSetDescription(""..
    "Constructed as a tectonic research station, Equinox has been upgraded[BR]and retrofitted for over a century of continuous service. The labs have[BR]been used for all kinds "..
    "of physical sciences, including chemistry,[BR]physics, materials science, and geology.[BR][BR]"..
    "The current usage for Equinox is 'whatever projects we can't find[BR]space for in the university'. The remote location outside the city[BR]makes the labs perfect for more "..
    "dangerous research as collateral[BR]damage is reduced.[BR][BR]"..
    "The labs follow the same layout as most sectors in Regulus City, with a[BR]main production floor, basement power controllers, and a habitation[BR]block above.")

--LRT Facility.
zJournalEntry = JournalEntry:new("LRTFacility", "LRT Facility")
zJournalEntry:fnSetVariable("Root/Variables/Regions/LRTFacility/iHasSeenThisRegion")
zJournalEntry:fnSetToLocation("Root/Images/RegionNotifiers/Permanent/LRTFacility", 0, 0)
zJournalEntry:fnSetDescription(""..
    "The Long-Range Telemetry (LRT) Facility collects signals from all over[BR]Regulus. Hundreds of small research beacons send data to satellites[BR]that route to the LRT Facility, "..
    "which logs them and forwards them to[BR]the Regulus City network. In turn, the network sends backups of its[BR]traffic to the LRT Facility for archival in the enormous[BR]"..
    "server banks there.[BR][BR]"..
    "The facility is off-limits to normal foot traffic. Only units with special[BR]clearance and permissions may visit the facility. Most security work is[BR]handled by drone "..
    "units and autonomous security robots.[BR][BR]"..
    "Material deliveries are conducted by uncrewed or drone-crewed[BR]shuttles on a different track than the normal Regulus City transit[BR]network. The facility also shares power "..
    "generation and logistics with[BR]Serenity Crater Observatory.")

--Serenity Crater Observatory.
zJournalEntry = JournalEntry:new("SerenityObservatory", "Serenity Crater Observatory")
zJournalEntry:fnSetVariable("Root/Variables/Regions/SerenityObservatory/iHasSeenThisRegion")
zJournalEntry:fnSetToLocation("Root/Images/RegionNotifiers/Permanent/SerenityObservatory", 0, 0)
zJournalEntry:fnSetDescription(""..
    "Named for the crater shelf it sits on, Serenity Crater Observatory is[BR]shielded from the lights of Regulus City and thus an ideal position for[BR]astronomical observation.[BR][BR]"..
    "It has been in continuous service since the times of the Arcane[BR]Academy when Regulus City was founded. It even contains some[BR]leftover artifacts from the time, including "..
    "an impressive brass[BR]telescope.[BR][BR]"..
    "The Observatory is run by Unit 300910, a command unit assigned[BR]there after graduating from the breeding program. There has been[BR]trouble on the shelf with darkmatters and "..
    "other unidentified... things.")

--Arcane University.
zJournalEntry = JournalEntry:new("ArcaneAcademy", "Arcane University")
zJournalEntry:fnSetVariable("Root/Variables/Regions/ArcaneAcademy/iHasSeenThisRegion")
zJournalEntry:fnSetToLocation("Root/Images/RegionNotifiers/Permanent/ArcaneAcademy", 0, 0)
zJournalEntry:fnSetDescription(""..
    "Founded centuries ago by magisters fleeing the political whims of[BR]feudal lords, it originally housed a few elite magicians and their[BR]families. Over time, " ..
    "the university expanded and changed until it[BR]became what it is today: The center of Regulus City and a seat of[BR]learning of all stripes.[BR][BR]" .. 
    "Everything, including the foundations, has been redone over the years[BR]to fit modern architecture and materials science. It still contains a few[BR]old passages "..
    "beneath the surface that are too expensive to retrofit.[BR][BR]"..
    "The university contains approximately three hundred classrooms and[BR]teaching laboratories, a laser research lab, medical research wing,[BR]geological cores archive, "..
    "particle accelerator, three different[BR]supercomputers, superconducting research labs, and even a[BR]hardcopy archive of books that are undergoing digital archival.")

--Raiju Ranch.
zJournalEntry = JournalEntry:new("RaijuRanch", "Raiju Ranch")
zJournalEntry:fnSetVariable("Root/Variables/Regions/RaijuRanch/iHasSeenThisRegion")
zJournalEntry:fnSetToLocation("Root/Images/RegionNotifiers/Permanent/RaijuRanch", 0, 0)
zJournalEntry:fnSetDescription(""..
    "The center of biological research in the biolabs, the Raiju Ranch is a[BR]collection of simple dormitory facilities for the raijus meant to assist[BR]with feeding, care, and "..
    "research into power generation. Several[BR]decades ago, it was expanded with a full school and habitation system[BR]for the human breeding program, to better assist "..
    "with producing[BR]high-calibre lord units for research and management.[BR][BR]"..
    "Like most of the biolabs facilities, the ranch is open to visitors in[BR]exchange for work credits. Many of the recreational facilities are[BR]shared with the humans "..
    "in the breeding program. This includes[BR]swimming, sports, and an 'outdoor' videograph system.[BR][BR]"..
    "Please exercise caution when interacting with organics.")

--Biolabs - Alpha
zJournalEntry = JournalEntry:new("BiolabsAlpha", "Biolabs Alpha")
zJournalEntry:fnSetVariable("Root/Variables/Regions/BiolabsAlpha/iHasSeenThisRegion")
zJournalEntry:fnSetToLocation("Root/Images/RegionNotifiers/Permanent/BiolabsAlpha", 0, 0)
zJournalEntry:fnSetDescription(""..
    "Adjacent to the Arcane University along the southern edge of the[BR]biolabs concourse, the alpha labs are a forest preserve used to study[BR]wild plant growth and insect "..
    "interactions. There are only a few[BR]dedicated crop areas, as the alpha labs are meant to study a 'wild'[BR]forest ecosystem.[BR][BR]"..
    "The alpha labs are connected to the hydroponics labs on the western[BR]edge and the delta labs through the sluice gates on the east.")

--Biolabs - Beta
zJournalEntry = JournalEntry:new("BiolabsBeta", "Biolabs Beta")
zJournalEntry:fnSetVariable("Root/Variables/Regions/BiolabsBeta/iHasSeenThisRegion")
zJournalEntry:fnSetToLocation("Root/Images/RegionNotifiers/Permanent/BiolabsBeta", 0, 0)
zJournalEntry:fnSetDescription(""..
    "This semi-tropical lab has year-round deciduous growth. It is the[BR]warmest of the labs, meant to study semi-arid natural growths in[BR]varying degrees of artificial "..
    "hydration, which coincides with its[BR]position near the fluid-transfer complex to the north.[BR][BR]"..
    "It is also the site of the biolabs winery, which is very popular with the[BR]raijus. The labs are currently researching large-scale irrigation in[BR]conjunction with the "..
    "hydroponics labs.")

--Biolabs - Gamma
zJournalEntry = JournalEntry:new("BiolabsGamma", "Biolabs Gamma")
zJournalEntry:fnSetVariable("Root/Variables/Regions/BiolabsGamma/iHasSeenThisRegion")
zJournalEntry:fnSetToLocation("Root/Images/RegionNotifiers/Permanent/BiolabsGamma", 0, 0)
zJournalEntry:fnSetDescription(""..
    "More of a resort destination than a research lab, the gamma labs are[BR]directly connected to Omicron station where most visitors arrive. This[BR]is the most heavily "..
    "cultivated lab and the source of much of the[BR]organic food enjoyed by the organic citizens of Regulus City.[BR][BR]"..
    "Being the centralized lab, it also contains the power switching stations[BR]that regulate power to the other labs. Near the tram station are[BR]recreational facilities, "..
    "with lockers and shops in the tram station itself.")

--Biolabs - Delta
zJournalEntry = JournalEntry:new("BiolabsDelta", "Biolabs Delta")
zJournalEntry:fnSetVariable("Root/Variables/Regions/BiolabsDelta/iHasSeenThisRegion")
zJournalEntry:fnSetToLocation("Root/Images/RegionNotifiers/Permanent/BiolabsDelta", 0, 0)
zJournalEntry:fnSetDescription(""..
    "Probably the most famous labs on Regulus, the delta labs are home to[BR]the Raiju Ranch as well as numerous parks and Biological Services to[BR]maintain them. It contains the "..
    "largest number of live animal pens, used[BR]for protein production for the raijus.[BR][BR]"..
    "After the Raiju Ranch, the delta labs contain the second most popular[BR]tourist attraction. The duck ponds are adjacent to Omicron Station[BR]and frequented by hundreds "..
    "of golems every year.")

--Biolabs - Epsilon
zJournalEntry = JournalEntry:new("BiolabsEpsilon", "Biolabs Epsilon")
zJournalEntry:fnSetVariable("Root/Variables/Regions/BiolabsEpsilon/iHasSeenThisRegion")
zJournalEntry:fnSetToLocation("Root/Images/RegionNotifiers/Permanent/BiolabsEpsilon", 0, 0)
zJournalEntry:fnSetDescription(""..
    "Currently under 'renovation' and closed to the public, the epsilon labs[BR]are where experiments with an unknown substance connected to[BR]Project Vivify took place.[BR][BR]"..
    "It is considerably larger on the inside than the outside.[BR][BR]"..
    "The original cryogenics research facility was once attached, but[BR]cannot be found reliably anymore from within. Only select researchers[BR]are permitted entry.")

--Biolabs - Datacore
zJournalEntry = JournalEntry:new("BiolabsDatacore", "Biolabs Datacore")
zJournalEntry:fnSetVariable("Root/Variables/Regions/BiolabsDatacore/iHasSeenThisRegion")
zJournalEntry:fnSetToLocation("Root/Images/RegionNotifiers/Permanent/BiolabsDatacore", 0, 0)
zJournalEntry:fnSetDescription(""..
    "Originally meant to act as an additional archive for data in the Arcane[BR]University, the biolabs datacore has grown to become one of the[BR]largest storage and processing "..
    "facilities in Regulus City. It it primarily[BR]concerned with scientific data, but also contains some centralized[BR]email and routing servers for the city.")
            
--Biolabs - Hydroponics
zJournalEntry = JournalEntry:new("BiolabsHydroponics", "Biolabs Hydroponics")
zJournalEntry:fnSetVariable("Root/Variables/Regions/BiolabsHydroponics/iHasSeenThisRegion")
zJournalEntry:fnSetToLocation("Root/Images/RegionNotifiers/Permanent/BiolabsHydroponics", 0, 0)
zJournalEntry:fnSetDescription(""..
    "The hydroponics research center studies plant interactions in artificial[BR]environments, hoping to allow more efficient mass production of[BR]food and biological "..
    "materials in controlled, industrial spaces. As a[BR]result of its work, it has become a hub of plant genetics research.[BR][BR]It shares a connection tunnel "..
    "with the biolabs datacore.")
            
--Biolabs - Aquatic Genetics
zJournalEntry = JournalEntry:new("BiolabsGenetics", "Aquatic Genetics Labs")
zJournalEntry:fnSetVariable("Root/Variables/Regions/BiolabsGenetics/iHasSeenThisRegion")
zJournalEntry:fnSetToLocation("Root/Images/RegionNotifiers/Permanent/BiolabsGenetics", 0, 0)
zJournalEntry:fnSetDescription(""..
    "The aquatic genetics labs sit in the southeastern corner of the biolabs[BR]near the large lake dug for the delta labs. Like many of the dedicated[BR]research labs, "..
    "it is currently interested in sequencing genetic[BR]codes of animal species and related research.[BR][BR]"..
    "It also contains an aquatics show that is performed twice daily with[BR]some of the more amusing aquatic mammal species, an outdoor diner,[BR]and public aquariums.[BR][BR]"..
    "It is connected to the delta labs and sector 54 across the[BR]southeastern edge.")

-- |[ ================ Upload ================ ]|
for i = 1, #gzaCh5LocationEntries, 1 do
    gzaCh5LocationEntries[i]:fnUploadData()
end

--Clean.
JournalEntry:fnCleanGlobals()

--Clear the list to save memory.
gzaCh5LocationEntries = nil
