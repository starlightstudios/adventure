-- |[ ========================== Special Case: Reprogramming Sequence ========================== ]|
--Create a bunch of fake quests here with scrambled data. If this is used, none of the other quests
-- should appear in the quest log.

-- |[Fake Quest 0]|
local zQuestEntry = JournalEntry:new("FakeQuest0", "???")
zQuestEntry:fnSetToQuest("HHonSpd11. ?. End pp.")
zQuestEntry:fnSetDescription("Data retrieval error.")

-- |[Fake Quest 1]|
zQuestEntry = JournalEntry:new("FakeQuest1", "???")
zQuestEntry:fnSetToQuest("Cos8849 sey ..x0x")
zQuestEntry:fnSetDescription("Data retrieval error.")

-- |[Fake Quest 2]|
zQuestEntry = JournalEntry:new("FakeQuest2", "???")
zQuestEntry:fnSetToQuest("soreDANDY--023ieo")
zQuestEntry:fnSetDescription("Data retrieval error.")

-- |[Fake Quest 3]|
zQuestEntry = JournalEntry:new("FakeQuest3", "???")
zQuestEntry:fnSetToQuest("!00oepSS~12/TE")
zQuestEntry:fnSetDescription("Data retrieval error.")
