-- |[ ========================================= Equinox ======================================== ]|
--Equinox, appears when the player enters sector 15.

-- |[Setup]|
local sInternalName = "Equinox Labs"
local sDisplayName = "Investigate the Equinox Labs"
local sObjective = ""
local sDescription = ""
local zQuestEntry = JournalEntry:new(sInternalName, sDisplayName)

--Variables.
local iSawEquinoxOpening = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawEquinoxOpening", "N")
local iCompletedEquinox  = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedEquinox", "N")
local iSawEquinoxMessage = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawEquinoxMessage", "N")

-- |[Blank Quest]|
if(iSawEquinoxMessage == 0.0) then
    zQuestEntry:fnBlankQuest()
    return
end
    
-- |[ ============================== Variants ============================== ]|
--Investigate.
if(iSawEquinoxOpening == 0.0) then
    sObjective = "Investigate the Equinox Labs"
    sDescription = "You received a rogue signal from the Equinox labs, but immediately[BR]"..
                   "afterwards, the signal was jammed and wiped from the network.[BR][BR]"..
                   "Regulus City's administration is keeping it quiet. You need to look into[BR]"..
                   "this. It could be another Cryogenics."

--Quest started.
elseif(iCompletedEquinox == 0.0) then
    sObjective = "Find out what First Drummer is"
    sDescription = "You received a rogue signal from the Equinox labs, but immediately[BR]"..
                   "afterwards, the signal was jammed and wiped from the network.[BR][BR]"..
                   "Equinox is a mess. The golems within have gone mad and attacked each[BR]"..
                   "other, and most of the residents are dead. What little you got from a[BR]"..
                   "survivor is that something called First Drummer was activated.[BR][BR]"..
                   "Find out who is responsible and what First Drummer is."
                
--Quest completed.
else
    sObjective = "Complete"
    sDescription = "You received a rogue signal from the Equinox labs, but immediately[BR]"..
                   "afterwards, the signal was jammed and wiped from the network.[BR][BR]"..
                   "Equinox is a mess. The golems within have gone mad and attacked each[BR]"..
                   "other, and most of the residents are dead. What little you got from a[BR]"..
                   "survivor is that something called First Drummer was activated.[BR][BR]"..
                   "First Drummer was some sort of override program that forced all the[BR]"..
                   "golems to fight each other for dominance. It likely had some physical[BR]"..
                   "component installed on the golems without them knowing, as it did not[BR]"..
                   "affect a golem who was there making a delivery.[BR][BR]"..
                   "A command unit was watching the monitors the whole time and taking[BR]"..
                   "notes. Regulus City already has the data. There's nothing more you can[BR]"..
                   "do here."
end

-- |[ =============================== Upload =============================== ]|
--Upload information.
zQuestEntry:fnSetToQuest(sObjective)
zQuestEntry:fnSetDescription(sDescription)