-- |[ ===================================== Serenity Crater ==================================== ]|
--Appears when found on the work terminal.
    
-- |[Setup]|
local sInternalName = "Serenity Crater"
local sDisplayName = "Serenity Crater Observatory"
local sObjective = ""
local sDescription = ""
local zQuestEntry = JournalEntry:new(sInternalName, sDisplayName)
    
--Variables.
local iSerenityWorkOrder    = VM_GetVar("Root/Variables/Chapter5/Scenes/iSerenityWorkOrder", "N")
local iSawSerenityIntro     = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawSerenityIntro", "N")
local iSawUndergroundSceneA = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawUndergroundSceneA", "N")
local iCompletedSerenity    = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")

-- |[Blank Quest]|
if(iSerenityWorkOrder < 1.0) then
    zQuestEntry:fnBlankQuest()
    return
end

-- |[ ============================== Variants ============================== ]|
--Has not seen area intro.
if(iSawSerenityIntro == 0.0) then
    sObjective = "Find out what Serenity Crater Observatory needs"
    sDescription = "Command Unit 300910, Head of Serenity Crater Observatory, has[BR]"..
                   "specifically requested your skillset for assistance. It's a combat[BR]"..
                   "assignment, so be prepared.[BR][BR]"..
                   "The observatory is far from the city and well situated to resist direct[BR]"..
                   "attack. They would be a useful ally to have. Getting them on your side[BR]"..
                   "is a good idea.[BR][BR]"..
                   "You can reach the observatory by taking the tram to the eastern[BR]"..
                   "junction 'LRT Facility', then going east and south."

--Has seen intro, hasn't found the terminal.
elseif(iSawUndergroundSceneA == 0.0) then
    sObjective = "Search the shelf for the secret facility"
    sDescription = "Command Unit 300910, Head of Serenity Crater Observatory, has[BR]"..
                   "specifically requested your skillset for assistance. It's a combat[BR]"..
                   "assignment, so be prepared.[BR][BR]"..
                   "The observatory is far from the city and well situated to resist direct[BR]"..
                   "attack. They would be a useful ally to have. Getting them on your side[BR]"..
                   "is a good idea.[BR][BR]"..
                   "Command Unit 300910 seems to know 55 from some time in the past,[BR]"..
                   "and she believes something the administration did has angered the[BR]"..
                   "darkmatters. Find out what they were doing in the secret facility down[BR]"..
                   "the crater shelf."

--Saw the terminal.
elseif(iCompletedSerenity == 0.0) then
    sObjective = "Close the rift caused by the artifact"
    sDescription = "Command Unit 300910, Head of Serenity Crater Observatory, has[BR]"..
                   "specifically requested your skillset for assistance. It's a combat[BR]"..
                   "assignment, so be prepared.[BR][BR]"..
                   "The observatory is far from the city and well situated to resist direct[BR]"..
                   "attack. They would be a useful ally to have. Getting them on your side[BR]"..
                   "is a good idea.[BR][BR]"..
                   "The secret facility located an artifact in the crust of Regulus. It is[BR]"..
                   "some sort of otherworldly statue. Bombarding it with specific energy[BR]"..
                   "fields opened a rift that let in the creatures aggravating the[BR]"..
                   "darkmatters, and only Christine can close it."

--Quest complete.
else
    sObjective = "Complete"
    sDescription = "Christine and 55 defeated the artifact and closed the rift. Hopefully,[BR]"..
                   "the darkmatters can deal with the creatures on the shelf.[BR][BR]"..
                   "The people of Serenity Crater Observatory are in your debt, and have[BR]"..
                   "vowed to support your revolt."
end

-- |[ =============================== Upload =============================== ]|
--Upload information.
zQuestEntry:fnSetToQuest(sObjective)
zQuestEntry:fnSetDescription(sDescription)