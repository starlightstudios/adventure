-- |[ ===================================== Electrosprites ===================================== ]|
--Appears when found on the work terminal.
    
-- |[Setup]|
local sInternalName = "Electrosprite"
local sDisplayName = "Sector 198, Abductions Training"
local sObjective = ""
local sDescription = ""
local zQuestEntry = JournalEntry:new(sInternalName, sDisplayName)

--Variables.
local i198WorkOrder          = VM_GetVar("Root/Variables/Chapter5/Scenes/i198WorkOrder", "N")
local iSaw198Intro           = VM_GetVar("Root/Variables/Chapter5/Scenes/iSaw198Intro",  "N")
local iMetAmanda             = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetAmanda", "N")
local iRanTrainingProgram    = VM_GetVar("Root/Variables/Chapter5/Scenes/iRanTrainingProgram", "N")
local iSawProblemWithProgram = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawProblemWithProgram", "N")
local iTalkedToElectrosprite = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToElectrosprite", "N")
local iBecameElectrosprite   = VM_GetVar("Root/Variables/Chapter5/Scenes/iBecameElectrosprite", "N")
local iFinished198           = VM_GetVar("Root/Variables/Chapter5/Scenes/iFinished198", "N")

-- |[Blank Quest]|
if(i198WorkOrder == 0.0 and iSaw198Intro == 0.0) then
    zQuestEntry:fnBlankQuest()
    return
end

-- |[Special]|
--Modify these local variables to advance the state if necessary.
if(iTalkedToElectrosprite == 1.0) then
    iSawProblemWithProgram = 1.0
end

-- |[ ============================== Variants ============================== ]|
--Initial
if(iMetAmanda == 0.0) then
    sObjective = "Find out what's going on in Sector 198"
    sDescription = "Ostensibly, there is an oxygen leak in sector 198, and your special[BR]"..
                   "skills have been requested. There's probably more to it than that.[BR][BR]"..
                   "Speak to Unit 745110 for more details."

--Spoke to Amanda.
elseif(iRanTrainingProgram == 0.0) then
    sObjective = "Diagnose the training program"
    sDescription = "Unit 745110, 'Amanda', has a problem. The training program is[BR]"..
                   "malfunctioning, and she's been assigned to fix it, but she's an organic,[BR]"..
                   "and can't get into the datacore. Her lord golem doesn't seem to care.[BR][BR]"..
                   "You'll have to fix it yourself. Run the training program and try to find[BR]"..
                   "the bug. You can find an open console in the datacore south of[BR]"..
                   "sector 198."

--Ran the program.
elseif(iSawProblemWithProgram == 0.0) then
    sObjective = "Diagnose the training program"
    sDescription = "Unit 745110, 'Amanda', has a problem. The training program is[BR]"..
                   "malfunctioning, and she's been assigned to fix it, but she's an organic,[BR]"..
                   "and can't get into the datacore. Her lord golem doesn't seem to care.[BR][BR]"..
                   "You'll have to fix it yourself.[BR][BR]"..
                   "You didn't see anything immediately wrong. Maybe if you played the[BR]"..
                   "training program to completion?"

--Saw Psue run off.
elseif(iTalkedToElectrosprite == 0.0) then
    sObjective = "Find a way to isolate the bug"
    sDescription = "Unit 745110, 'Amanda', has a problem. The training program is[BR]"..
                   "malfunctioning, and she's been assigned to fix it, but she's an organic,[BR]"..
                   "and can't get into the datacore. Her lord golem doesn't seem to care.[BR][BR]"..
                   "You'll have to fix it yourself.[BR][BR]"..
                   "You saw an NPC in the program behave oddly before teleporting away.[BR]"..
                   "Amanda may be able to offer a cheat code to help catch it."

--Time to catch that electrosprite!
elseif(iBecameElectrosprite == 0.0) then
    sObjective = "Find a way to isolate the bug"
    sDescription = "Unit 745110, 'Amanda', has a problem. The training program is[BR]"..
                   "malfunctioning, and she's been assigned to fix it, but she's an organic,[BR]"..
                   "and can't get into the datacore. Her lord golem doesn't seem to care.[BR][BR]"..
                   "You'll have to fix it yourself.[BR][BR]"..
                   "You saw an NPC in the program behave oddly before teleporting away.[BR]"..
                   "Amanda may be able to offer a cheat code to help catch it.[BR][BR]"..
                   "Your PDU can probably set a trap. Look around the datacore."

--They're alive!
elseif(iFinished198 == 0.0) then
    sObjective = "Liberate the electrosprites"
    sDescription = "Unit 745110, 'Amanda', has a problem. The training program is[BR]"..
                   "malfunctioning, and she's been assigned to fix it, but she's an organic,[BR]"..
                   "and can't get into the datacore. Her lord golem doesn't seem to care.[BR][BR]"..
                   "You'll have to fix it yourself.[BR][BR]"..
                   "Turns out the AIs in the game are living, thinking creatures called[BR]"..
                   "electrosprites. They deserve freedom, too. Talk to 55 about this!"

--Finale.
else
    bIsElectrospritesComplete = true
    sObjective = "Complete"
    sDescription = "With the help of a bit of a conspiracy, the electrosprites have[BR]"..
                   "manifested themselves within the training program students.[BR][BR]"..
                   "Owing you everything, the electrosprites have pledged to help your[BR]"..
                   "revolt, as have the people of sector 198."
end

-- |[ =============================== Upload =============================== ]|
--Upload information.
zQuestEntry:fnSetToQuest(sObjective)
zQuestEntry:fnSetDescription(sDescription)