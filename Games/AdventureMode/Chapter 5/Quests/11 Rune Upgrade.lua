-- |[ =================================== Runestone Upgrade ==================================== ]|
--Appears when found on the work terminal, after completing the LRT facility.
    
-- |[Setup]|
local sInternalName = "RuneUpgrade"
local sDisplayName = "Decrypt the Runestone Data"
local sObjective = ""
local sDescription = ""
local zQuestEntry = JournalEntry:new(sInternalName, sDisplayName)

--Variables.
local iRuneUpgradeTaken     = VM_GetVar("Root/Variables/Chapter5/Scenes/iRuneUpgradeTaken", "N")
local iRuneUpgradeBriefing  = VM_GetVar("Root/Variables/Chapter5/Scenes/iRuneUpgradeBriefing", "N")
local iRuneUpgradeEquinox   = VM_GetVar("Root/Variables/Chapter5/Scenes/iRuneUpgradeEquinox", "N")
local iRuneUpgradeSerenity  = VM_GetVar("Root/Variables/Chapter5/Scenes/iRuneUpgradeSerenity", "N")
local iRuneUpgradeTransit   = VM_GetVar("Root/Variables/Chapter5/Scenes/iRuneUpgradeTransit", "N")
local iRuneUpgradeCompleted = VM_GetVar("Root/Variables/Chapter5/Scenes/iRuneUpgradeCompleted", "N")

-- |[Blank Quest]|
if(iRuneUpgradeTaken == 0.0) then
    zQuestEntry:fnBlankQuest()
    return
end

-- |[ ============================== Variants ============================== ]|
--Initial
if(iRuneUpgradeBriefing == 0.0) then
    sObjective = "Meet with 55 in Lower Regulus City, B1"
    sDescription = "55 has clearly placed a bogus work order for you to 'repair' the[BR]"..
                   "terminal you met her at much earlier. She has something to show you,[BR]"..
                   "so go find out what it is."

--Spoke to 55
elseif(iRuneUpgradeBriefing == 1.0 and iRuneUpgradeCompleted == 0.0) then

    --Three strings to mark how the player has proceeded.
    local sEquinox     = "Equinox Labs"
    local sSerenity    = "Serenity Observatory"
    local sCryoTransit = "Transit Station North of Cryogenics"
    if(iRuneUpgradeEquinox  == 1.0) then sEquinox     = sEquinox     .. " (Obtained)" end
    if(iRuneUpgradeSerenity == 1.0) then sSerenity    = sSerenity    .. " (Obtained)" end
    if(iRuneUpgradeTransit  == 1.0) then sCryoTransit = sCryoTransit .. " (Obtained)" end


    sObjective = "Locate the three academic decryption engine parts"
    sDescription = "55 suspects the administration knows about your runestone, because[BR]"..
                   "there is academic discussion of others seen planetside. The[BR]"..
                   "information is encrypted even on the LRT servers.[BR][BR]" ..
                   "She's fairly certain she can decode it if she can get leftover parts of[BR]"..
                   "an academic decryption engine at the following locations:[BR][BR]" .. 
                   sEquinox .. "[BR]"..
                   sSerenity .. "[BR]"..
                   sCryoTransit .. "[BR][BR]"..
                   "Once all three are found, return to the terminal in Regulus City B1."

--Finale.
else
    sObjective = "Complete"
    sDescription = "With 55's help, you have 'cleaned' the runestone, already resulting in a[BR]"..
                   "major improvement in its power. Unfortunately that's as far as the[BR]"..
                   "administration got with its research.[BR][BR]"..
                   "It will take dedicated study to improve it further, and you have a city[BR]"..
                   "to liberate."
end

-- |[ =============================== Upload =============================== ]|
--Upload information.
zQuestEntry:fnSetToQuest(sObjective)
zQuestEntry:fnSetDescription(sDescription)