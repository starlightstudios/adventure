-- |[ ===================================== Fireteam Down ====================================== ]|
--Appears when the player either talks to Dafoda or sees Reika.

-- |[Setup]|
local sInternalName = "Company Nine"
local sDisplayName = "Company Nine Down"
local sObjective = ""
local sDescription = ""
local zQuestEntry = JournalEntry:new(sInternalName, sDisplayName)

--Variables.
local iAquaticMeeting        = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticMeeting", "N")
local iAquaticsSawHydrophobe = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticsSawHydrophobe", "N")
local iAquaticsActionScene   = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticsActionScene", "N")

-- |[Blank Quest]|
if(iAquaticMeeting == 0.0) then
    zQuestEntry:fnBlankQuest()
    return
end

-- |[ ============================== Variants ============================== ]|
--Got the mission, hasn't seen the elevator yet.
if(iAquaticsSawHydrophobe == 0.0) then
    sObjective = "Check eastern Aquatic Genetics for signs of Company Nine"
    sDescription = "In the chaos of the revolution, Company Nine got ambushed in the[BR]"..
                   "biolabs and is way out of position, suffering severe casualties.[BR][BR]"..
                   "A machine gun nest firing lead slugs hit them while crossing a bridge.[BR]"..
                   "Some of the units are still there, and the gun nest is likely on auto, still[BR]"..
                   "active.[BR][BR]"..
                   "Captain Jo doesn't want you risking your cranial chassis for the fallen[BR]"..
                   "units. Rescue them anyway."

--Saw the elevator.
elseif(iAquaticsActionScene == 0.0) then
    sObjective = "Check eastern Aquatic Genetics for signs of Company Nine"
    sDescription = "In the chaos of the revolution, Company Nine got ambushed in the[BR]"..
                   "biolabs and is way out of position, suffering severe casualties.[BR][BR]"..
                   "A machine gun nest firing lead slugs hit them while crossing a bridge.[BR]"..
                   "Some of the units are still there, and the gun nest is likely on auto, still[BR]"..
                   "active.[BR][BR]"..
                   "Captain Jo doesn't want you risking your cranial chassis for the fallen[BR]"..
                   "units. Rescue them anyway.[BR][BR]"..
                   "55 has concerns about deep water, and cannot follow. Christine will[BR]"..
                   "have to proceed alone."

--Complete.
else
    sObjective = "Complete"
    sDescription = "The survivors of Company Nine have been rescued and will be[BR]"..
                   "repaired as soon as possible.[BR][BR]"..
                   "The Dormers don't leave anyone behind."

end

-- |[ =============================== Upload =============================== ]|
--Upload information.
zQuestEntry:fnSetToQuest(sObjective)
zQuestEntry:fnSetDescription(sDescription)