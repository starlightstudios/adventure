-- |[ ====================================== Rescue Reika ====================================== ]|
--Appears when the player either talks to Dafoda or sees Reika.

-- |[Setup]|
local sInternalName = "Rescue Reika"
local sDisplayName = "Rescue Reika"
local sObjective = ""
local sDescription = ""
local zQuestEntry = JournalEntry:new(sInternalName, sDisplayName)

--Variables.
local iAmphibianRescuedReika = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianRescuedReika", "N")
local iAmphibianMetDafoda = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianMetDafoda", "N")
local iAmphibianSawReika  = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianSawReika",  "N")

-- |[Blank Quest]|
if(iAmphibianMetDafoda == 0.0 and iAmphibianSawReika == 0.0) then
    zQuestEntry:fnBlankQuest()
    return
end

-- |[ ============================== Variants ============================== ]|
--Talked to Dafoda, but didn't see Reika yet.
if(iAmphibianMetDafoda == 1.0 and iAmphibianSawReika == 0.0 and iAmphibianRescuedReika == 0.0) then
    sObjective = "Search the Amphibian Research facility for the missing alraune"
    sDescription = "One of Biological Services' alraunes went off on her own to try to fix[BR]"..
                   "the pipe problems in the biolabs. Dafoda said she went to the beta labs,[BR]"..
                   "and is probably in the amphibian research area.[BR][BR]"..
                   "Find her."

--Saw Reika without talking to Dafoda.
elseif(iAmphibianMetDafoda == 0.0 and iAmphibianSawReika == 1.0 and iAmphibianRescuedReika == 0.0) then
    sObjective = "Rescue the alraune in the Amphibian Research building"
    sDescription = "You saw an alraune through the frosted glass in the back of Amphibian[BR]"..
                   "Research in the beta labs. She's alive, but freezing.[BR][BR]"..
                   "The pipes will burst and possibly kill her if opened forcibly. You'll need[BR]"..
                   "to find and open the reserve valves in the area to safely break the ice."


--Saw Reika, talked to Dafoda.
elseif(iAmphibianMetDafoda == 1.0 and iAmphibianSawReika == 1.0 and iAmphibianRescuedReika == 0.0) then
    sObjective = "Rescue Reika in the Amphibian Research building"
    sDescription = "You saw Reika through the frosted glass in the back of Amphibian[BR]"..
                   "Research in the beta labs. She's alive, but freezing.[BR][BR]"..
                   "The pipes will burst and possibly kill her if opened forcibly. You'll need[BR]"..
                   "to find and open the reserve valves in the area to safely break the ice."
                
--Quest complete, hasn't gone back to Dafoda.
elseif(iAmphibianRescuedReika == 1.0) then
    sObjective = "Complete"
    sDescription = "You found Reika in the Amphibian Research building and rescued her.[BR]"..
                   "She'll make her way back to Biological Services on her own, so she says.[BR][BR]"..
                   "You might want to go make sure Dafoda isn't kicking her ass too hard."

--Quest complete, talked to Dafoda.
else
    sObjective = "Complete"
    sDescription = "You found Reika in the Amphibian Research building and rescued her.[BR]"..
                   "Dafoda is happy at the safe return of her friend, but hasn't pledged[BR]"..
                   "anything for the revolt. She says she'll consider your actions.[BR][BR]"..
                   "At least Reika is okay."

end

-- |[ =============================== Upload =============================== ]|
--Upload information.
zQuestEntry:fnSetToQuest(sObjective)
zQuestEntry:fnSetDescription(sDescription)