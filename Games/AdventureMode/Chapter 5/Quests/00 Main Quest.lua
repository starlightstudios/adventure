-- |[ ======================================= Main Quest ======================================= ]|
--Whatever the main quest currently is. The internal name is the same, but the quest display name
-- changes as the plot goes on.
local iMainQuestBlock = VM_GetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N")
    
-- |[ ============================== Setup ============================= ]|
--Setup.
local sInternalName = "Chapter 5 Main Quest"
local sDisplayName = ""
local sObjective = ""
local sDescription = ""
local zQuestEntry = JournalEntry:new(sInternalName, sInternalName)

-- |[ ========================== Primary Track ========================= ]|
-- |[Case: Start of Chapter]|
--Check the intercom.
if(iMainQuestBlock == 0.0) then

    --Display Name / Objective
    sDisplayName = "The Cryogenics Facility"
    sObjective = "Speak to the lady on the intercom"
    
    --Description.
    sDescription = "You have awoken in a strange place, like an advanced space facility.[BR]" ..
                   "Someone is yelling at you on the intercom. Better answer her."
                   
--Retrieve the PDU.
elseif(iMainQuestBlock == 0.5) then

    --Display Name / Objective
    sDisplayName = "The Cryogenics Facility"
    sObjective = "Find the PDU in the next room"
    
    --Description.
    sDescription = "You have awoken in a strange place, like an advanced space facility.[BR]" ..
                   "The lady on the intercom told you to find a PDU in the next room.[BR]"..
                   "Afterwards, contact her on the intercom."
                
--Get the red keycard.
elseif(iMainQuestBlock == 1.0) then

    --Display Name / Objective
    sDisplayName = "The Cryogenics Facility"
    sObjective = "Find a security-red keycard"
    
    --Description.
    sDescription = "You are on Regulus, moon of Pandemonium. The Cryogenics facility is[BR]" ..
                   "filled with bodies, and the only other person still alive is a voice[BR]" ..
                   "on the intercom.[BR][BR]"..
                   "She said you'll need to find a red keycard to have a chance of escaping,[BR]"..
                   "but doesn't know where one is. It should be on a 'command unit'.[BR]"..
                   "Report back to an intercom when you have found it.[BR][BR]"..
                   "You are also looking for spare parts from a command unit. Your PDU[BR]"..
                   "will scan them for you when you examine them."
                
--Get the blue keycard.
elseif(iMainQuestBlock == 2.0) then

    --Display Name / Objective
    sDisplayName = "The Cryogenics Facility"
    sObjective = "Find a security-blue keycard"
    
    --Description.
    sDescription = "You are on Regulus, moon of Pandemonium. The Cryogenics facility is[BR]" ..
                   "filled with bodies, and the only other person still alive is a voice[BR]" ..
                   "on the intercom.[BR][BR]"..
                   "She said you'll need to find a blue keycard to have a chance of[BR]"..
                   "escaping, which should be in the containment area to the northeast.[BR][BR]"..
                   "You are also looking for spare parts from a command unit. Your PDU[BR]"..
                   "will scan them for you when you examine them."
                
--Go to the fabrication bay.
elseif(iMainQuestBlock == 3.0) then

    --Display Name / Objective
    sDisplayName = "The Cryogenics Facility"
    sObjective = "Enter the fabrication bay, fabricate an authenticator chip"
    
    --Description.
    sDescription = "You are on Regulus, moon of Pandemonium. The Cryogenics facility is[BR]" ..
                   "filled with bodies, and the only other person still alive is a voice[BR]" ..
                   "on the intercom.[BR][BR]"..
                   "You're still missing an important part to escape the facility, an[BR]"..
                   "authenticator chip. The lady on the intercom says you may be able to[BR]"..
                   "make one.[BR][BR]"..
                   "Go to the fabrication bay. You'll have to go under the tram station on[BR]"..
                   "the east side of the facility and find a way around."
                
--Meet 55 in the command room.
elseif(iMainQuestBlock == 4.0) then

    --Display Name / Objective
    sDisplayName = "The Cryogenics Facility"
    sObjective = "Meet your collaborator in the command center"
    
    --Description.
    sDescription = "You are on Regulus, moon of Pandemonium. The Cryogenics facility is[BR]" ..
                   "filled with bodies, and the only other person still alive is a voice[BR]" ..
                   "on the intercom.[BR][BR]"..
                   "Since the fabrication bays were destroyed, you'll have to meet with[BR]"..
                   "the lady on the intercom and figure out your next move in person.[BR]"..
                   "She is presently on the upper floor of the facility in the command[BR]"..
                   "center, next to the airlock."

-- |[Golem, Regulus City]|
--Programming at Regulus City.
elseif(iMainQuestBlock == 10.0) then

    --Display Name / Objective
    sDisplayName = "Regulus City"
    sObjective = "Report to Regulus City and receive a function assignment."
    
    --Description.
    sDescription = "You have been transformed into a lord golem, designation Unit 771852.[BR]"..
                   "Your programming requires you to travel to Regulus City and receive[BR]"..
                   "a function assignment there.[BR][BR]"..
                   "The city is north of the Cryogenics Facility.[BR][BR]"..
                   "First, report for diagnostics to the repair bay. Priority zero."
                
--Get a function assignment.
elseif(iMainQuestBlock == 11.0) then

    --Display Name / Objective
    sDisplayName = "Regulus City"
    sObjective = "Report to Regulus City and receive a function assignment."
    
    --Description.
    sDescription = "You have been transformed into a lord golem, designation Unit 771852.[BR]"..
                   "Your programming requires you to travel to Regulus City and receive[BR]"..
                   "a function assignment there.[BR][BR]"..
                   "The city is north of the Cryogenics Facility.[BR][BR]"..
                   "Obtain a function assignment there and carry it out."
                
--Get a function assignment.
elseif(iMainQuestBlock == 12.0) then

    --Display Name / Objective
    sDisplayName = "Regulus City"
    sObjective = "Tell Sophie the good news."
    
    --Description.
    sDescription = "You have been transformed into a lord golem, designation Unit 771852.[BR]"..
                   "You have been assigned to the repair bay in Sector 96.[BR][BR]"..
                   "Go tell Sophie the good news."
                
--Date!
elseif(iMainQuestBlock == 13.0) then

    --Display Name / Objective
    sDisplayName = "Regulus City"
    sObjective = "Enjoy your date with Sophie."
    
    --Description.
    sDescription = "You have been transformed into a lord golem, designation Unit 771852.[BR]"..
                   "You have been assigned to the repair bay in Sector 96.[BR][BR]"..
                   "Sophie agreed to go on a date, so take her someplace in the sector and[BR]"..
                   "spend some time with her."
                
--55 meeting.
elseif(iMainQuestBlock == 14.0) then

    --Display Name / Objective
    sDisplayName = "Regulus City"
    sObjective = "Fulfill the special work order."
    
    --Description.
    sDescription = "You are Unit 771852, 'Christine', Lord Golem of Maintenance and Repair,[BR]"..
                   "Sector 96.[BR][BR]"..
                   "Your primary duty is to oversee the speedy and efficient repair of the [BR]"..
                   "sector's damaged equipment.[BR][BR]"..
                   "You currently have a special assignment on the work terminal. See[BR]"..
                   "that it is completed as soon as possible."
                
--Go talk to Sophie about 55.
elseif(iMainQuestBlock == 14.5) then

    --Display Name / Objective
    sDisplayName = "Regulus City"
    sObjective = "Speak with Sophie."
    
    --Description.
    sDescription = "You are Unit 771852, 'Christine', Lord Golem of Maintenance and Repair,[BR]"..
                   "Sector 96.[BR][BR]"..
                   "Your primary duty is to oversee the speedy and efficient repair of the [BR]"..
                   "sector's damaged equipment.[BR][BR]"..
                   "During the conversion process, your memories were largely scrambled.[BR]"..
                   "Unit 2855 had preserved them for you in secret, and unlocked them[BR]"..
                   "with a shock. She clearly needs your help, and you likely need hers.[BR][BR]"..
                   "Speak to Sophie about finding her."
                
--Find 55.
elseif(iMainQuestBlock == 15.0) then

    --Display Name / Objective
    sDisplayName = "Regulus City"
    sObjective = "Locate and speak to Unit 2855"
    
    --Description.
    sDescription = "You are Unit 771852, 'Christine', Lord Golem of Maintenance and Repair,[BR]"..
                   "Sector 96.[BR][BR]"..
                   "Your primary duty is to oversee the speedy and efficient repair of the [BR]"..
                   "sector's damaged equipment.[BR][BR]"..
                   "During the conversion process, your memories were largely scrambled.[BR]"..
                   "Unit 2855 had preserved them for you in secret, and unlocked them[BR]"..
                   "with a shock. She clearly needs your help, and you likely need hers.[BR][BR]"..
                   "Find her and talk to her. The PDU has tracked her network activity to[BR]"..
                   "basement 1F beneath sector 96."
                
--Fight the power.
elseif(iMainQuestBlock == 16.0) then

    --Display Name / Objective
    sDisplayName = "The LRT Facility"
    sObjective = "Investigate the LRT Facility Datacore"
    
    --Description.
    sDescription = "You are Unit 771852, 'Christine', Lord Golem of Maintenance and Repair,[BR]"..
                   "Sector 96.[BR][BR]"..
                   "Your primary duty is to oversee the speedy and efficient repair of the [BR]"..
                   "sector's damaged equipment.[BR][BR]"..
                   "The administration of Regulus City is an oppressive authoritarian[BR]"..
                   "regime that treats its citizens as disposable pawns. The atrocities you[BR]"..
                   "saw at Cryogenics are testament enough that this system must be[BR]"..
                   "overthrown.[BR][BR]"..
                   "55 lost her memories but believes she was part of the rebellion that[BR]"..
                   "took place there. She thinks a backup of the videograph data that was[BR]"..
                   "there will be on the servers at the LRT facility east of Regulus City.[BR]"..
                   "You can take the tram most of the way there, but will need to go over[BR]"..
                   "the surface to access it.[BR][BR]"..
                   "The information you need is in the main datacore in the facility."

--Optional: If transformed by Vivify, remind the player to check the datacore.
elseif(iMainQuestBlock == 17.0) then

    --Display Name / Objective
    sDisplayName = "The LRT Facility"
    sObjective = "Investigate the LRT Facility Datacore"
    
    --Description.
    sDescription = "You are Unit 771852, 'Christine', Lord Golem of Maintenance and Repair,[BR]"..
                   "Sector 96.[BR][BR]"..
                   "Your primary duty is to oversee the speedy and efficient repair of the [BR]"..
                   "sector's damaged equipment.[BR][BR]"..
                   "The administration of Regulus City is an oppressive authoritarian[BR]"..
                   "regime that treats its citizens as disposable pawns. The atrocities you[BR]"..
                   "saw at Cryogenics are testament enough that this system must be[BR]"..
                   "overthrown.[BR][BR]"..
                   "55 lost her memories but believes she was part of the rebellion that[BR]"..
                   "took place there. She thinks a backup of the videograph data that was[BR]"..
                   "there will be on the servers at the LRT facility east of Regulus City.[BR][BR]"..
                   "Project Vivify confronted you in the datacore, and transformed you[BR]"..
                   "into a dreamer. You know she won't inhibit you any more, so the [BR]"..
                   "datacore should be safe to investigate now."

-- |[The Gala]|
elseif(iMainQuestBlock == 20.0) then

    --Display Name / Objective
    sDisplayName = "Return to Regulus City"
    sObjective = "Return to Regulus City"
    
    --Description.
    sDescription = "After reviewing the records at the LRT Facility, 55 learned who she[BR]"..
                   "was. It wasn't who she thought.[BR][BR]"..
                   "She needs time to think. Go back to Regulus City."
                   
elseif(iMainQuestBlock == 21.0) then

    --Display Name / Objective
    sDisplayName = "The Sunrise Gala"
    sObjective = "Speak to 55"
    
    --Description.
    sDescription = "You've just received an invitation to the Sunrise Gala, a yearly event[BR]"..
                   "where the aristocracy of Regulus City gather to watch the sun rise.[BR][BR]"..
                   "You haven't spoken to 55 since the LRT facility. She'll probably want to[BR]"..
                   "know about the gala, and you should check in to make sure she's doing[BR]"..
                   "okay. She will be in the repair bay basement."
                   
elseif(iMainQuestBlock == 22.0) then

    --Display Name / Objective
    sDisplayName = "The Sunrise Gala"
    sObjective = "Take Sophie to Sector 115 and get materials"
    
    --Description.
    sDescription = "You've just received an invitation to the Sunrise Gala, a yearly event[BR]"..
                   "where the aristocracy of Regulus City gather to watch the sun rise.[BR][BR]"..
                   "55 wants to use this opportunity to blow up a lot of high-ranking units.[BR]"..
                   "But more importantly, you need something to wear![BR][BR]"..
                   "Take Sophie to Sector 115 and get her some dressmaking materials."
                   
elseif(iMainQuestBlock == 23.0) then

    --Display Name / Objective
    sDisplayName = "The Sunrise Gala"
    sObjective = "Talk to 55 in the repair bay basement"
    
    --Description.
    sDescription = "You've just received an invitation to the Sunrise Gala, a yearly event[BR]"..
                   "where the aristocracy of Regulus City gather to watch the sun rise.[BR][BR]"..
                   "55 wants to use this opportunity to blow up a lot of high-ranking units.[BR]"..
                   "But more importantly, you need something to wear![BR][BR]"..
                   "Sophie is working on making some dresses for you. Meanwhile, 55[BR]"..
                   "wants to meet with you. She's in the repair bay basement."
                   
elseif(iMainQuestBlock == 24.0) then

    --Display Name / Objective
    sDisplayName = "The Sunrise Gala"
    sObjective = "Talk to 55 in the repair bay basement"
    
    --Description.
    sDescription = "You've just received an invitation to the Sunrise Gala, a yearly event[BR]"..
                   "where the aristocracy of Regulus City gather to watch the sun rise.[BR][BR]"..
                   "55 wants to use this opportunity to blow up a lot of high-ranking units.[BR]"..
                   "But more importantly, you need something to wear![BR][BR]"..
                   "Sophie is working on making some dresses for you. 55 wants you to[BR]"..
                   "complete any other tasks you may have, and then begin preparations[BR]"..
                   "full time for the gala.[BR][BR]"..
                   "Find allies, equipment, items, and skillbooks. Once you are ready, speak[BR]"..
                   "to 55 in the repair bay basement."
                   
elseif(iMainQuestBlock == 25.0) then

    --Display Name / Objective
    sDisplayName = "The Sunrise Gala"
    sObjective = "Attend the gala"
    
    --Description.
    sDescription = "You're dressed and ready to go. Take the tram to the Arcane[BR]University."
                   
elseif(iMainQuestBlock == 26.0) then

    --Display Name / Objective
    sDisplayName = "The Sunrise Gala"
    sObjective = "Complete your preassigned objectives"
    
    --Description.
    sDescription = "You've got a list of objectives to complete before letting 55 in to set[BR]"..
                   "the charges.[BR][BR]"..
                   "Don't forget to dance with Sophie![BR][BR]"..
                   "Once the objectives are complete, exit the party and make your way[BR]"..
                   "to the basement."
                   
elseif(iMainQuestBlock == 27.0) then

    --Display Name / Objective
    sDisplayName = "The Sunrise Gala"
    sObjective = "Find the physics access lift"
    
    --Description.
    sDescription = "Christine should disable the alarms on the access lift nearby. Find it."
                   
elseif(iMainQuestBlock == 28.0) then

    --Display Name / Objective
    sDisplayName = "The Sunrise Gala"
    sObjective = "Find your interlocutor"
    
    --Description.
    sDescription = "2856 seems to know what you're up to, and sent a drone to tell you to[BR]"..
                   "meet her. Since she didn't call a thousand security units, maybe you[BR]"..
                   "should hear her out."
                   
elseif(iMainQuestBlock == 29.0) then

    --Display Name / Objective
    sDisplayName = "The Sunrise Gala"
    sObjective = "Seal the underground accessways"
    
    --Description.
    sDescription = "2856 knows about your plans, but you have a much bigger problem.[BR]"..
                   "Project Vivify's minions are beneath the gala and intend to assimilate[BR]"..
                   "or destroy everyone within.[BR][BR]"..
                   "You'll need to break past them and plant explosive charges on the[BR]"..
                   "access points beneath the university, then return to 2856."

--Optional: Caused by losing the battle to Dreamer Christine.
elseif(iMainQuestBlock == 30.0) then

    --Display Name / Objective
    sDisplayName = "The Sunrise Gala"
    sObjective = "Enlighten the universe"
    
    --Description.
    sDescription = "Your eyes are open for the first time. Bring the grand truth to the[BR]"..
                   "people of Regulus."

-- |[Biolabs]|
elseif(iMainQuestBlock == 50.0) then

    --Display Name / Objective
    sDisplayName = "Stop Project Vivify"
    sObjective = "Reach the Raiju Ranch"
    
    --Description.
    sDescription = "201890 crashed the gala, but this greatly angered Vivify, who has[BR]"..
                   "called her followers back to her in the Epsilon labs. You've formed a[BR]"..
                   "temporary truce with Unit 2856 to stop her.[BR][BR]"..
                   "Your first goal is to regroup with the available security units at the[BR]"..
                   "Raiju Ranch. You'll need the supplies in storage there to enter Epsilon."
                
elseif(iMainQuestBlock == 51.0) then

    --Display Name / Objective
    sDisplayName = "Stop Project Vivify"
    sObjective = "Enter the Epsilon labs"
    
    --Description.
    sDescription = "201890 crashed the gala, but this greatly angered Vivify, who has[BR]"..
                   "called her followers back to her in the Epsilon labs. You've formed a[BR]"..
                   "temporary truce with Unit 2856 to stop her.[BR][BR]"..
                   "Now that you have the tools you'll need, go to the entrance of the[BR]"..
                   "Epsilon labs. The security checkpoint is north of the Gamma labs[BR]"..
                   "entrance."
                
elseif(iMainQuestBlock == 52.0) then

    --Display Name / Objective
    sDisplayName = "Stop Project Vivify"
    sObjective = "Find Vivify"
    
    --Description.
    sDescription = "201890 crashed the gala, but this greatly angered Vivify, who has[BR]"..
                   "called her followers back to her in the Epsilon labs. You've formed a[BR]"..
                   "temporary truce with Unit 2856 to stop her.[BR][BR]"..
                   "Vivify is somewhere in the labs. Find her."
                
elseif(iMainQuestBlock == 53.0) then

    --Display Name / Objective
    sDisplayName = "Eliminate Unit 2856"
    sObjective = "Exit the Epsilon labs, retire 2856"
    
    --Description.
    sDescription = "Vivify froze herself and is, for now, no longer a threat to the city.[BR][BR]"..
                   "Find and destroy Unit 2856 before she can rally the administration."

-- |[Doll Sequence]|
elseif(iMainQuestBlock == 60.0) then

    --Display Name / Objective
    sDisplayName = "???"
    sObjective = "1-C ?? 7 W"
    
    --Description.
    sDescription = "200 Quest Handler.lua - Error, line 444. Invalid character."
    
elseif(iMainQuestBlock == 61.0) then

    --Display Name / Objective
    sDisplayName = "Destroy the administrator"
    sObjective = "Destroy the administrator"
    
    --Description.
    sDescription = "You've managed to avoid being reprogrammed, and now have[BR]"..
                   "knowledge of the Administrator who pulls the strings on the[BR]"..
                   "command units.[BR][BR]"..
                   "Find and destroy it, so the golems will have a chance in their[BR]"..
                   "revolution. To freedom!"
    
elseif(iMainQuestBlock == 62.0) then

    --Display Name / Objective
    sDisplayName = "Betray the mavericks"
    sObjective = "Lure Unit 2855 into a trap"
    
    --Description.
    sDescription = "You have been reprogrammed successfully and are loyal to the[BR]"..
                   "Administrator. Unit 2855 suspects nothing.[BR][BR]"..
                   "Lead her into a trap and allow the Administrator to decide her fate."
end

-- |[ =============================== Upload =============================== ]|
--Upload information.
zQuestEntry:fnSetDisplayName(sDisplayName)
zQuestEntry:fnSetToQuest(sObjective)
zQuestEntry:fnSetDescription(sDescription)