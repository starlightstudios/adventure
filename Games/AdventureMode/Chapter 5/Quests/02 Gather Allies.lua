-- |[ ===================================== Gather Allies ====================================== ]|
--Appears once 55 and Christine are working together. Gets checked off when all other subquests in
-- the first half of chapter 5 are done.
--This gets set further down when all of the quests are done updating.

-- |[Setup]|
local sInternalName = "Gather Allies"
local sDisplayName = "Gather Allies and Equipment"
local sObjective = ""
local sDescription = ""
local zQuestEntry = JournalEntry:new(sInternalName, sDisplayName)
    
--Local Variables.
local iMainQuestBlock = VM_GetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N")

-- |[Blank Quest]|
if(iMainQuestBlock < 16) then
    zQuestEntry:fnBlankQuest()
    return
end

    
-- |[Base Description]|
--Set the value for the incomplete version. Complete version is later in this file.
sObjective = "Search for allies and equipment"
sDescription = "There are still things you can do to gather more allies and equipment[BR]"..
               "for your eventual revolt against the revolution. See the other quest log[BR]"..
               "entries for details, or check a work terminal for assignments."

-- |[ ================================== Update Allies Quest =================================== ]|
--If the player has completed all of the listed subquests, then this changes the quest. This requires checking
-- a lot of variables.

--Variable set:
local bIsEquinoxComplete = false
local bIsCassandraComplete = false
local bIsSouthernCryogenicsComplete = false
local bIsSerenityComplete = false
local bIsMinesComplete = false
local bIsManufactoryComplete = false
local bIsElectrospritesComplete = false

-- |[Equinox]|
local iCompletedEquinox = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedEquinox", "N")
if(iCompletedEquinox >= 1.0) then bIsEquinoxComplete = true end

-- |[Cassandra]|
local iResolvedCassandraQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iResolvedCassandraQuest", "N")
if(iResolvedCassandraQuest >= 1.0) then bIsCassandraComplete = true end

-- |[Southern Cryogenics]|
local iSCryoRepoweredDoor = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoRepoweredDoor", "N")
if(iSCryoRepoweredDoor == 1.0) then bIsSouthernCryogenicsComplete = true end

-- |[Serenity Crater]|
local iCompletedSerenity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
if(iCompletedSerenity == 1.0) then bIsSerenityComplete = true end

-- |[Tellurium Mines]|
local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
if(iSXUpgradeQuest > 2.0) then bIsMinesComplete = true end

-- |[Manufactory]|
local iManuFinale = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuFinale", "N")
if(iManuFinale == 1.0) then bIsManufactoryComplete = true end

-- |[Electrosprites]|
local iFinished198 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFinished198", "N")
if(iFinished198 == 1.0) then bIsElectrospritesComplete = true end

-- |[All Sidequests Complete]|
--If the player has completed all of the side quests related to the revolution, then update the
-- log entry.
if(bIsEquinoxComplete and bIsCassandraComplete and bIsSouthernCryogenicsComplete and bIsSerenityComplete and
   bIsMinesComplete and bIsManufactoryComplete and bIsElectrospritesComplete) then
    
    -- |[Setup]|
    sObjective    = "Complete"
    sDescription  = "You've done all you can for the moment to prepare for your eventual[BR]"..
                    "revolt against the administration."
end

-- |[ =============================== Upload =============================== ]|
--Upload information.
zQuestEntry:fnSetToQuest(sObjective)
zQuestEntry:fnSetDescription(sDescription)