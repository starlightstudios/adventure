-- |[ ======================================= Manufactory ====================================== ]|
--Appears when found on the work terminal.
    
-- |[Setup]|
local sInternalName = "Manufactory"
local sDisplayName = "Sector 99, the Manufactory"
local sObjective = ""
local sDescription = ""
local zQuestEntry = JournalEntry:new(sInternalName, sDisplayName)

--Variables.
local iManuTookJob            = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuTookJob", "N")
local iManuMetKatarina        = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuMetKatarina", "N")
local iManuTold55Plan         = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuTold55Plan", "N")
local iManuReassigned         = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuReassigned", "N")
local iManufactoryDepopulated = VM_GetVar("Root/Variables/Chapter5/Scenes/iManufactoryDepopulated", "N")
local iManuFinale             = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuFinale", "N")

-- |[Blank Quest]|
if(iManuTookJob == 0.0) then
    zQuestEntry:fnBlankQuest()
    return
end

-- |[ ============================== Variants ============================== ]|
--Not met Katarina.
if(iManuMetKatarina == 0.0) then
    sObjective = "Find out what's going on in Sector 99"
    sDescription = "Lord Unit 600484 has requested your skills in Sector 99, a[BR]"..
                   "manufacturing sector located under a ridge northeast of the city[BR]center.[BR][BR]"..
                   "Go find out what she wants."

--Met Katarina, got assignment.
elseif(iManuTold55Plan == 0.0) then
    sObjective = "Find out how Sector 99 became so productive"
    sDescription = "Lord Unit 600484 has requested your skills in Sector 99, a[BR]"..
                   "manufacturing sector located under a ridge northeast of the city[BR]center.[BR][BR]"..
                   "Katarina is an efficient expert who cannot explain the sector's sudden[BR]"..
                   "surge in productivity. Finding out what they're doing could be useful[BR]"..
                   "for your revolt. Question the units there and try to find out what's[BR]"..
                   "going on."

--Nothing is wrong - OR IS IT?
elseif(iManuReassigned == 0.0) then
    sObjective = "Disguise yourself as a latex drone"
    sDescription = "Lord Unit 600484 has requested your skills in Sector 99, a[BR]"..
                   "manufacturing sector located under a ridge northeast of the city[BR]center.[BR][BR]"..
                   "Katarina is an efficient expert who cannot explain the sector's sudden[BR]"..
                   "surge in productivity. Finding out what they're doing could be useful[BR]"..
                   "for your revolt.[BR][BR]"..
                   "If you disguise yourself as a drone unit and pretend to have been[BR]"..
                   "assigned to work in sector 99, you might be able to search the area[BR]"..
                   "more thoroughly or overhear the units talking.[BR][BR]"..
                   "Sophie might want to transform you even if you found another way to[BR]"..
                   "become a drone unit..."

--Oh no!
elseif(iManufactoryDepopulated == 0.0) then
    sObjective = "Perform your assigned tasks"
    sDescription = "NETWORK OVERRIDE. DRONE WILL PERFORM ASSIGNED TASKS."

--Get to the storage area!
elseif(iManuFinale == 0.0) then
    sObjective = "Get into the secure storage rooms"
    sDescription = "The units in Sector 99 tried to install some sort of hardware in your[BR]"..
                   "head! They've clearly gone maverick, but something is not right. The[BR]"..
                   "storage area they've been guarding is a prime suspect.[BR][BR]"..
                   "Go into the storage area in Sector 99 and get to the bottom of this."

--Finale.
else
    bIsManufactoryComplete = true
    sObjective = "Complete"
    sDescription = "Some enormous machine hivemind called Project Mirabelle was[BR]"..
                   "controlling the units in Sector 99. Luckily, 55 got a mail from a[BR]"..
                   "benefactor of some sort that contained its killphrase.[BR][BR]"..
                   "The units of Sector 99 are all mavericks by default, as the[BR]"..
                   "administration is likely to cover the whole thing up. They will back[BR]"..
                   "your revolt.[BR][BR]"..
                   "Any other questions about Mirabelle will have to be put aside for now.[BR]"..
                   "All that information was on the servers that blew up."
end

-- |[ =============================== Upload =============================== ]|
--Upload information.
zQuestEntry:fnSetToQuest(sObjective)
zQuestEntry:fnSetDescription(sDescription)