-- |[ ================================ Secrebots in Sector 254 ================================= ]|
--Appears when found on the work terminal, after LRT facility.
    
-- |[Setup]|
local sInternalName = "Secrebots"
local sDisplayName = "Sector 254, Secrebots"
local sObjective = ""
local sDescription = ""
local zQuestEntry = JournalEntry:new(sInternalName, sDisplayName)

--Variables.
local i254WorkOrder      = VM_GetVar("Root/Variables/Chapter5/Scenes/i254WorkOrder", "N")
local i254TalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/i254TalkedToSophie", "N")
local i254GotAlert       = VM_GetVar("Root/Variables/Chapter5/Scenes/i254GotAlert", "N")
local i254SawCommandUnit = VM_GetVar("Root/Variables/Chapter5/Scenes/i254SawCommandUnit", "N")
local i254SawDrones      = VM_GetVar("Root/Variables/Chapter5/Scenes/i254SawDrones", "N")
local i254FreeStuff      = VM_GetVar("Root/Variables/Chapter5/Scenes/i254FreeStuff", "N")
local i254NightEscapeA   = VM_GetVar("Root/Variables/Chapter5/Scenes/i254NightEscapeA", "N")
local i254NightEscapeB   = VM_GetVar("Root/Variables/Chapter5/Scenes/i254NightEscapeB", "N")
local i254NightCaptured  = VM_GetVar("Root/Variables/Chapter5/Scenes/i254NightCaptured", "N")
local i254Transformed    = VM_GetVar("Root/Variables/Chapter5/Scenes/i254Transformed", "N")
local i254SophieLeading  = VM_GetVar("Root/Variables/Chapter5/Scenes/i254SophieLeading", "N")
local i254TalkedTo55     = VM_GetVar("Root/Variables/Chapter5/Scenes/i254TalkedTo55", "N")
local i254FoundPDU       = VM_GetVar("Root/Variables/Chapter5/Scenes/i254FoundPDU", "N")
local i254FixedChristine = VM_GetVar("Root/Variables/Chapter5/Scenes/i254FixedChristine", "N")
local i254Completed     = VM_GetVar("Root/Variables/Chapter5/Scenes/i254Completed", "N")

-- |[Blank Quest]|
if(i254WorkOrder == 0.0) then
    zQuestEntry:fnBlankQuest()
    return
end

-- |[ ============================== Variants ============================== ]|
--Initial
if(i254TalkedToSophie == 0.0) then
    sObjective = "Ask Sophie about the work order."
    sDescription = "Sophie put a work order on the terminal asking for some help. Seems[BR]"..
                   "to have been a rough day for her, go ask her what the problem is."

--Spoke to Sophie.
elseif(i254SawCommandUnit == 0.0) then
    sObjective = "Go to Sector 254 and ask about secrebots"
    sDescription = "There are apparently new second-wave secrebots with advanced[BR]"..
                   "functionality being produced. Sophie wants one for the repair bay,[BR]"..
                   "but the waiting list is long. Still, it can't hurt to ask..."

--Met with Odess.
elseif(i254SawDrones == 0.0) then
    sObjective = "Oh well"
    sDescription = "It seems the waiting list isn't getting any shorter. You tried, but it just[BR]"..
                   "wasn't in the cards. However, there was a command unit visiting, and[BR]"..
                   "there is something fishy going on in Sector 254."

--Saw the drones, there's a rogue human!
elseif(i254NightCaptured == 0.0) then
    sObjective = "Get to the rogue human before the drones do!"
    sDescription = "There is somehow a rogue human loose in the warehouse! There is no[BR]"..
                   "way this is a coincidence, but regardless you need to get to them[BR]"..
                   "before the drones do."

--Night is captured, but Christine isn't.
elseif(i254Transformed == 0.0) then
    sObjective = "Try to save the human"
    sDescription = "Seems the drones caught up to the human before you did. Investigate,[BR]"..
                   "it may not be too late to stop them!"

--Christine is a secrebot following Sophie.
elseif(i254TalkedTo55 == 0.0) then
    sObjective = "Get Christine to the tram station"
    sDescription = "Christine is clearly doing some spy stuff, which is super hot but she's[BR]"..
                   "really into it, and it probably isn't safe to tell you that yet.[BR]"..
                   "Get her someplace isolated so she can let you in on her plans, hee hee!"

--Found out about the problems, go find the PDU.
elseif(i254FoundPDU == 0.0) then
    sObjective = "Find Christine's belongings"
    sDescription = "Christine got reprogrammed, and she can't turn back without her[BR]"..
                   "runestone! You had better find it, it's probably still in the sector.[BR]"..
                   "Search!"

--Got her stuff, get her fixed!
elseif(i254FixedChristine == 0.0) then
    sObjective = "Get Christine to the repair bay"
    sDescription = "Christine can't transform even when ordered, something is really[BR]"..
                   "wrong. Take her to the repair bay, 2855 will know what to do!"
    
--Get revenge!
elseif(i254Completed == 0.0) then
    sObjective = "Destroy the secrebot machine!"
    sDescription = "There is a strike team mobilizing to hit Sector 254 and arrest everyone[BR]"..
                   "involved, and probably innocents, too. You can't let them capture the[BR]"..
                   "secrebot machine, who knows what the administration could do with it.[BR]"..
                   "Get to it first and blow it up."

--Finale.
else
    sObjective = "Complete"
    sDescription = "The second-wave secrebots were converted humans, explaining their[BR]"..
                   "advanced 'AI' capabilities, that were infested with flesh that made[BR]"..
                   "them loyal to Vivify. The network of spies in the city can be[BR]"..
                   "unravelled by resistance cells or the security forces, whoever gets to[BR]"..
                   "them first, but the machine responsible has been destroyed."
end

-- |[ =============================== Upload =============================== ]|
--Upload information.
zQuestEntry:fnSetToQuest(sObjective)
zQuestEntry:fnSetDescription(sDescription)