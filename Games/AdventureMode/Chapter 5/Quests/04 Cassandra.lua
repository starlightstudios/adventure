-- |[ ======================================= Cassandra ======================================== ]|
--Appears when the job is taken from the work terminal.

-- |[Setup]|
local sInternalName = "Sector 15"
local sDisplayName = "Organic Disturbance in Sector 15"
local sObjective = ""
local sDescription = ""
local zQuestEntry = JournalEntry:new(sInternalName, sDisplayName)

--Variables.
local iTookCassandraQuest     = VM_GetVar("Root/Variables/Chapter5/Scenes/iTookCassandraQuest", "N")
local iCassandraRanOff        = VM_GetVar("Root/Variables/Chapter5/Scenes/iCassandraRanOff", "N")
local iResolvedCassandraQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iResolvedCassandraQuest", "N")
    
-- |[Blank Quest]|
if(iTookCassandraQuest == 0.0) then
    zQuestEntry:fnBlankQuest()
    return
end

-- |[ ============================== Variants ============================== ]|
--Investigate.
if(iCassandraRanOff == 0.0 and iResolvedCassandraQuest == 0.0) then
    sObjective = "Investigate the disturbance in Sector 15"
    sDescription = "A special assignment on the work terminal requested you investigate[BR]"..
                   "a disturbance in Sector 15.[BR][BR]"..
                   "An unidentified organic has been stealing rations. They may be hostile,[BR]"..
                   "exercise caution."

--Saw Cassandra.
elseif(iResolvedCassandraQuest == 0.0) then
    sObjective = "Capture or negotiate with the human"
    sDescription = "A special assignment on the work terminal requested you investigate[BR]"..
                   "a disturbance in Sector 15.[BR][BR]"..
                   "The rations have been stolen by a human. Before you could get[BR]"..
                   "anything out of her, she ran away.[BR][BR]"..
                   "It seems she's terrified of robots. You'll need to find a way to get close[BR]"..
                   "to her without alarming her."
                
--Quest completed.
elseif(iResolvedCassandraQuest == 1.0 or iResolvedCassandraQuest == 3.0) then
    sObjective = "Complete"
    sDescription = "A special assignment on the work terminal requested you investigate[BR]"..
                   "a disturbance in Sector 15.[BR][BR]"..
                   "The disturbance was a human. You successfully converted her to[BR]"..
                   "Unit 771853, 'Cassandra'. Mission complete."

--Quest complete.
else
    sObjective = "Complete"
    sDescription = "A special assignment on the work terminal requested you investigate[BR]"..
                   "a disturbance in Sector 15.[BR][BR]"..
                   "The disturbance was a human named Cassandra. Knowing what the[BR]"..
                   "administration does to humans, you gave her a map to a steam droid[BR]"..
                   "settlement. She said she'd find her way there on her own."
end
    
-- |[ =============================== Upload =============================== ]|
--Upload information.
zQuestEntry:fnSetToQuest(sObjective)
zQuestEntry:fnSetDescription(sDescription)