-- |[ ==================================== Tellurium Mines ===================================== ]|
--Appears when found on the work terminal.

-- |[Setup]|
local sInternalName = "Tellurium Mines"
local sDisplayName = "Tellurium Mines"
local sObjective = ""
local sDescription = ""
local zQuestEntry = JournalEntry:new(sInternalName, sDisplayName)

--Variables.
local iHasMinesWorkOrder     = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasMinesWorkOrder", "N")
local iTalkedToMinesLord     = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToMinesLord", "N")
local i55ExplainedHerself    = VM_GetVar("Root/Variables/Chapter5/Scenes/i55ExplainedHerself", "N")
local iSawMinesECutsceneB    = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMinesECutsceneB", "N")
local iSteamDroid250RepState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid250RepState", "N")
local iSXMet55               = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXMet55", "N")
local iSpokeWithTT233        = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeWithTT233", "N")
local iSteamDroid500RepState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid500RepState", "N")
local iFoundSchematics       = VM_GetVar("Root/Variables/Chapter5/Scenes/iFoundSchematics", "N")
local iSXUpgradeQuest        = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")

-- |[Blank Quest]|
if(iHasMinesWorkOrder == 0.0 and iTalkedToMinesLord == 0.0) then
    zQuestEntry:fnBlankQuest()
    return
end

-- |[ ============================== Variants ============================== ]|
--Has not seen area intro.
if(iTalkedToMinesLord == 0.0) then
    sObjective = "Find out what Sector 73 needs"
    sDescription = "Mining Sector 73 has a problem with their equipment and needs you[BR]"..
                   "to fix it. Go to Sector 73, the 'Tellurium Mines', and find out what they[BR]"..
                   "need."

--Talked to the lord but not 55.
elseif(i55ExplainedHerself == 0.0) then
    sObjective = "Go into the mines"
    sDescription = "Mining Sector 73 has a problem with their equipment and needs you[BR]"..
                   "to fix it. Apparently the elevators are malfunctioning, and your job is[BR]"..
                   "to go into the mines and fix them."

--55 explained what's really going on.
elseif(iSawMinesECutsceneB == 0.0) then
    sObjective = "Locate the steam droid settlement"
    sDescription = "Your actual purpose for being in the mines is to locate the steam droid[BR]"..
                   "settlement that 55 suspects is in the area. They would be very useful[BR]"..
                   "allies to have for a revolution. Find them."

--Got to Sprocket City.
elseif(iSteamDroid250RepState < 3.0) then
    sObjective = "Earn JX-101's trust"
    sDescription = "Your actual purpose for being in the mines is to locate the steam droid[BR]"..
                   "settlement that 55 suspects is in the area. They would be very useful[BR]"..
                   "allies to have for a revolution.[BR][BR]"..
                   "Unfortunately, you fell into a trap and reverted to human form, and[BR]"..
                   "were unable to actually conduct negotiations. You'll need to earn the[BR]"..
                   "trust of the steam droids somehow, and they won't like it if you lied to[BR]"..
                   "them about being a lord golem.[BR][BR]"..
                   "Earn trust with JX-101 and hope an opportunity opens up."

--SX-399.
elseif(iSXMet55 == 0.0) then
    sObjective = "Help SX-399"
    sDescription = "Your actual purpose for being in the mines is to locate the steam droid[BR]"..
                   "settlement that 55 suspects is in the area. They would be very useful[BR]"..
                   "allies to have for a revolution.[BR][BR]"..
                   "Unfortunately, you fell into a trap and reverted to human form, and[BR]"..
                   "were unable to actually conduct negotiations. You'll need to earn the[BR]"..
                   "trust of the steam droids somehow, and they won't like it if you lied to[BR]"..
                   "them about being a lord golem.[BR][BR]"..
                   "SX-399, JX-101's daughter, has a problem with her systems. Fixing her[BR]"..
                   "might be a good way to earn the aid of the steam droids.[BR][BR]"..
                   "Introduce her to 55."

--Met 55.
elseif(iSpokeWithTT233 == 0.0) then
    sObjective = "Speak to TT-233"
    sDescription = "Your actual purpose for being in the mines is to locate the steam droid[BR]"..
                   "settlement that 55 suspects is in the area. They would be very useful[BR]"..
                   "allies to have for a revolution.[BR][BR]"..
                   "Unfortunately, you fell into a trap and reverted to human form, and[BR]"..
                   "were unable to actually conduct negotiations. You'll need to earn the[BR]"..
                   "trust of the steam droids somehow, and they won't like it if you lied to[BR]"..
                   "them about being a lord golem.[BR][BR]"..
                   "SX-399, JX-101's daughter, has a problem with her systems. Fixing her[BR]"..
                   "might be a good way to earn the aid of the steam droids.[BR][BR]"..
                   "She seems to get along quite well with 55. Her problem is related to[BR]"..
                   "her power core. TT-233, the steam droid doctor, knows more."

--Black site.
elseif(iSteamDroid500RepState == 0.0) then
    sObjective = "Earn JX-101's trust"
    sDescription = "Your actual purpose for being in the mines is to locate the steam droid[BR]"..
                   "settlement that 55 suspects is in the area. They would be very useful[BR]"..
                   "allies to have for a revolution.[BR][BR]"..
                   "Unfortunately, you fell into a trap and reverted to human form, and[BR]"..
                   "were unable to actually conduct negotiations. You'll need to earn the[BR]"..
                   "trust of the steam droids somehow, and they won't like it if you lied to[BR]"..
                   "them about being a lord golem.[BR][BR]"..
                   "SX-399, JX-101's daughter, has a problem with her systems. Fixing her[BR]"..
                   "might be a good way to earn the aid of the steam droids.[BR][BR]"..
                   "She seems to get along quite well with 55. Her problem is related to[BR]"..
                   "her power core. You'll need to get schematics of old steam droids, and[BR]"..
                   "55 will 'arrange' for JX-101 to learn where some might be. Keep[BR]"..
                   "earning JX-101's trust and hopefully she'll let you tag along."

--Raid the black site.
elseif(iSteamDroid500RepState == 1.0 and iFoundSchematics == 0.0) then
    sObjective = "Find the schematics at the black site"
    sDescription = "Your actual purpose for being in the mines is to locate the steam droid[BR]"..
                   "settlement that 55 suspects is in the area. They would be very useful[BR]"..
                   "allies to have for a revolution.[BR][BR]"..
                   "Unfortunately, you fell into a trap and reverted to human form, and[BR]"..
                   "were unable to actually conduct negotiations. You'll need to earn the[BR]"..
                   "trust of the steam droids somehow, and they won't like it if you lied to[BR]"..
                   "them about being a lord golem.[BR][BR]"..
                   "SX-399, JX-101's daughter, has a problem with her systems. Fixing her[BR]"..
                   "might be a good way to earn the aid of the steam droids.[BR][BR]"..
                   "She seems to get along quite well with 55. Her problem is related to[BR]"..
                   "her power core. You may be able to fix her with steam droid[BR]"..
                   "schematics, which might be in a disused workshop in the black site.[BR][BR]"..
                   "Search the workshop at the back of the site for them.[BR]Go east from Sprocket City."

--Got the schematics.
elseif(iFoundSchematics == 1.0 and iSXUpgradeQuest < 2.0) then
    sObjective = "Get the schematics back to SX-399"
    sDescription = "Your actual purpose for being in the mines is to locate the steam droid[BR]"..
                   "settlement that 55 suspects is in the area. They would be very useful[BR]"..
                   "allies to have for a revolution.[BR][BR]"..
                   "Unfortunately, you fell into a trap and reverted to human form, and[BR]"..
                   "were unable to actually conduct negotiations. You'll need to earn the[BR]"..
                   "trust of the steam droids somehow, and they won't like it if you lied to[BR]"..
                   "them about being a lord golem.[BR][BR]"..
                   "SX-399, JX-101's daughter, has a problem with her systems. Fixing her[BR]"..
                   "might be a good way to earn the aid of the steam droids.[BR][BR]"..
                   "She seems to get along quite well with 55. Her problem is related to[BR]"..
                   "her power core, and you might be able to fix her with the schematics[BR]"..
                   "you found.[BR][BR]"..
                   "Get the schematics back to SX-399!"

--Disguised as SX-399.
elseif(iSXUpgradeQuest == 2.0) then
    sObjective = "Distract JX-101"
    sDescription = "SX-399 is on her way to Regulus City to get fixed, and you're[BR]"..
                   "disguised as SX-399 in her place. Distract JX-101 and make sure she[BR]"..
                   "doesn't suspect her daughter is missing.[BR][BR]"..
                   "Examine things around town and keep her occupied."

--Finished.
else
    bIsMinesComplete = true
    sObjective = "Complete"
    sDescription = "SX-399 has been repaired and is better than ever, thanks to 55 and[BR]"..
                   "Sophie. While initially angry, JX-101 relented and gave you time to[BR]"..
                   "explain things.[BR][BR]"..
                   "Thanks to all the great things you did for the city, not least of which[BR]"..
                   "was helping SX-399, JX-101 has agreed to coordinate with you to[BR]"..
                   "overthrow the administration."

end

-- |[ =============================== Upload =============================== ]|
--Upload information.
zQuestEntry:fnSetToQuest(sObjective)
zQuestEntry:fnSetDescription(sDescription)