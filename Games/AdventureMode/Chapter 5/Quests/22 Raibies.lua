-- |[ ======================================= The Raibies ====================================== ]|
--Appears when the player talks to Dr. Maisie.
    
-- |[Setup]|
local sInternalName = "Raibies"
local sDisplayName = "The Raibie Infection"
local sObjective = ""
local sDescription = ""
local zQuestEntry = JournalEntry:new(sInternalName, sDisplayName)

--Variables.
local iRaibieQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N")
local iRaibieDrank = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieDrank", "N")

-- |[Blank Quest]|
if(iRaibieQuest == 0.0) then
    zQuestEntry:fnBlankQuest()
    return
end

-- |[ ============================== Variants ============================== ]|
--Got the mission, time to find some samples.
if(iRaibieQuest == 1.0) then
    sObjective = "Get some raibie fur samples for Dr. Maisie"
    sDescription = "The raijus in the biolabs are suffering from some sort of pathogen that[BR]"..
                   "turns them green and makes them blindly violent towards golems and[BR]"..
                   "humans alike.[BR][BR]"..
                   "The physician at the Raiju Ranch, Dr. Maisie, is working on a cure, but[BR]"..
                   "the outbreak of the revolution isn't helping. The security forces are all[BR]"..
                   "busy, and you're all she has.[BR][BR]"..
                   "Bring Dr. Maisie some samples from a raibie by defeating some."

--Got the samples.
elseif(iRaibieQuest == 2.0) then
    sObjective = "Deliver the samples to Dr. Maisie"
    sDescription = "The raijus in the biolabs are suffering from some sort of pathogen that[BR]"..
                   "turns them green and makes them blindly violent towards golems and[BR]"..
                   "humans alike.[BR][BR]"..
                   "The physician at the Raiju Ranch, Dr. Maisie, is working on a cure, but[BR]"..
                   "the outbreak of the revolution isn't helping. The security forces are all[BR]"..
                   "busy, and you're all she has.[BR][BR]"..
                   "Return the samples you found to Dr. Maisie."

--Find the prime pathogen.
elseif(iRaibieQuest == 3.0) then
    sObjective = "Find the prime pathogen"
    sDescription = "The raijus in the biolabs are suffering from some sort of pathogen that[BR]"..
                   "turns them green and makes them blindly violent towards golems and[BR]"..
                   "humans alike.[BR][BR]"..
                   "The physician at the Raiju Ranch, Dr. Maisie, is working on a cure, but[BR]"..
                   "the outbreak of the revolution isn't helping. The security forces are all[BR]"..
                   "busy, and you're all she has.[BR][BR]"..
                   "The pathogen is self-reactive, but finding a 'prime' version of the[BR]"..
                   "original pathogen would go a long way towards finding a cure. Search[BR]"..
                   "the gamma labs for a possible vector."

--It's E-v89-r!
elseif(iRaibieQuest == 4.0) then
    sObjective = "Become a raiju"
    sDescription = "The raijus in the biolabs are suffering from some sort of pathogen that[BR]"..
                   "turns them green and makes them blindly violent towards golems and[BR]"..
                   "humans alike.[BR][BR]"..
                   "The physician at the Raiju Ranch, Dr. Maisie, is working on a cure, but[BR]"..
                   "the outbreak of the revolution isn't helping. The security forces are all[BR]"..
                   "busy, and you're all she has.[BR][BR]"..
                   "The pathogen is related to substance E-v89-4, which is one of the[BR]"..
                   "many illicit pieces of secret research conducted in the labs.[BR][BR]"..
                   "To synthesize the antibodies she need, Maisie will need to watch an[BR]"..
                   "infection occur in action. This means Christine will need to infect[BR]"..
                   "herself. To do that, she'll need to become a raiju.[BR][BR]"..
                   "Talk to Dr. Maisie when you're ready."

--It's E-v89-r!
elseif(iRaibieQuest == 5.0) then
    sObjective = "Get the nanites from Dr. Maisie"
    sDescription = "The raijus in the biolabs are suffering from some sort of pathogen that[BR]"..
                   "turns them green and makes them blindly violent towards golems and[BR]"..
                   "humans alike.[BR][BR]"..
                   "The physician at the Raiju Ranch, Dr. Maisie, is working on a cure, but[BR]"..
                   "the outbreak of the revolution isn't helping. The security forces are all[BR]"..
                   "busy, and you're all she has.[BR][BR]"..
                   "The pathogen is related to substance E-v89-4, which is one of the[BR]"..
                   "many illicit pieces of secret research conducted in the labs.[BR][BR]"..
                   "To synthesize the antibodies she need, Maisie will need to watch an[BR]"..
                   "infection occur in action. This means Christine will need to infect[BR]"..
                   "herself. Talk to Dr. Maisie to get the monitoring nanites."

--Drink stuff you find in a barrel.
elseif(iRaibieQuest == 6.0) then
    sObjective = "Dose Christine with E-v89-r"
    sDescription = "The raijus in the biolabs are suffering from some sort of pathogen that[BR]"..
                   "turns them green and makes them blindly violent towards golems and[BR]"..
                   "humans alike.[BR][BR]"..
                   "The physician at the Raiju Ranch, Dr. Maisie, is working on a cure, but[BR]"..
                   "the outbreak of the revolution isn't helping. The security forces are all[BR]"..
                   "busy, and you're all she has.[BR][BR]"..
                   "The pathogen is related to substance E-v89-4, which is one of the[BR]"..
                   "many illicit pieces of secret research conducted in the labs.[BR][BR]"..
                   "To synthesize the antibodies she need, Maisie will need to watch an[BR]"..
                   "infection occur in action. This means Christine will need to infect[BR]"..
                   "herself. Go to the gamma labs and take a drink."

--What a bust!
elseif(iRaibieQuest == 7.0 and iRaibieDrank == 1.0) then
    sObjective = "Go back to Dr. Maisie"
    sDescription = "The raijus in the biolabs are suffering from some sort of pathogen that[BR]"..
                   "turns them green and makes them blindly violent towards golems and[BR]"..
                   "humans alike.[BR][BR]"..
                   "The physician at the Raiju Ranch, Dr. Maisie, is working on a cure, but[BR]"..
                   "the outbreak of the revolution isn't helping. The security forces are all[BR]"..
                   "busy, and you're all she has.[BR][BR]"..
                   "The pathogen is related to substance E-v89-4, which is one of the[BR]"..
                   "many illicit pieces of secret research conducted in the labs.[BR][BR]"..
                   "To synthesize the antibodies she need, Maisie will need to watch an[BR]"..
                   "infection occur in action. This means Christine will need to infect[BR]"..
                   "herself. But the barrels in the gamma labs did nothing.[BR][BR]"..
                   "Talk to Dr. Maisie and figure out your next move."

--What a bust!
elseif(iRaibieQuest == 7.0 and iRaibieDrank == 2.0) then
    sObjective = "Neutralize Christine"
    sDescription = "The raijus in the biolabs are suffering from some sort of pathogen that[BR]"..
                   "turns them green and makes them blindly violent towards golems and[BR]"..
                   "humans alike.[BR][BR]"..
                   "The physician at the Raiju Ranch, Dr. Maisie, is working on a cure, but[BR]"..
                   "the outbreak of the revolution isn't helping. The security forces are all[BR]"..
                   "busy, and you're all she has.[BR][BR]"..
                   "The pathogen is related to substance E-v89-4, which is one of the[BR]"..
                   "many illicit pieces of secret research conducted in the labs.[BR][BR]"..
                   "To synthesize the antibodies she need, Maisie will need to watch an[BR]"..
                   "infection occur in action. This means Christine will need to infect[BR]"..
                   "herself. But the barrels in the gamma labs did nothing.[BR][BR]"..
                   "The reaction was delayed, and Christine is out of control. She's in the[BR]"..
                   "Tissue Testing Labs, and will need to be stopped. Use the local[BR]"..
                   "resources to disable her."

--Complete.
else
    sObjective = "Complete"
    sDescription = "With a little help from 55, Christine has managed to transform back to[BR]"..
                   "normal, and her robotic form killed the remaining pathogens.[BR][BR]"..
                   "Dr. Maisie was able to observe the reaction and determine how it[BR]"..
                   "overwhelms the host. She can now create and administer a cure to the[BR]"..
                   "raibies. Your work, for the moment, is done."

end
        
-- |[ =============================== Upload =============================== ]|
--Upload information.
zQuestEntry:fnSetToQuest(sObjective)
zQuestEntry:fnSetDescription(sDescription)
