-- |[ ================================== Southern Cryogenics =================================== ]|
--Appears when 55 joins the party.

-- |[Setup]|
local sInternalName = "Southern Cryogenics"
local sDisplayName = "Search Cryogenics"
local sObjective = ""
local sDescription = ""
local zQuestEntry = JournalEntry:new(sInternalName, sDisplayName)

--Variables.
local iMainQuestBlock        = VM_GetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N")
local iSCryoSawUnpoweredDoor = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoSawUnpoweredDoor", "N")
local iSCryoRepoweredDoor    = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoRepoweredDoor", "N")
    
-- |[Blank Quest]|
if(iMainQuestBlock < 16.0) then
    zQuestEntry:fnBlankQuest()
    return
end

-- |[ ============================== Variants ============================== ]|
--Door not repowered, hasn't seen it yet.
if(iSCryoSawUnpoweredDoor == 0.0) then
    sObjective = "Search Cryogenics for survivors and equipment"
    sDescription = "The Cryogenics Facility south of Regulus City has been abandoned and[BR]"..
                   "quarantined. Nobody in the city will notice if you were to take[BR]"..
                   "equipment there for your purposes.[BR][BR]"..
                   "There may be survivors still within the facility. There is an airlock on[BR]"..
                   "the southeast side of the facility that leads to a construction site.[BR]"..
                   "That's the most likely place to find anyone still functioning."

--Saw door.
elseif(iSCryoRepoweredDoor == 0.0) then
    sObjective = "Rescue the survivor in Cryogenics"
    sDescription = "The Cryogenics Facility south of Regulus City has been abandoned and[BR]"..
                   "quarantined. Nobody in the city will notice if you were to take[BR]"..
                   "equipment there for your purposes.[BR][BR]"..
                   "There may be a survivor in the construction site south of the main[BR]"..
                   "facility. The building she is in is sealed, and will require a power jump[BR]"..
                   "to open.[BR][BR]"..
                   "Search the construction site for batteries and get that door open."

--Quest complete.
else
    bIsSouthernCryogenicsComplete = true
    sObjective = "Complete"
    sDescription = "The Cryogenics Facility south of Regulus City has been abandoned and[BR]"..
                   "quarantined. Nobody in the city will notice if you were to take[BR]"..
                   "equipment there for your purposes.[BR][BR]"..
                   "You found a survivor in an incomplete building south of the main[BR]"..
                   "facility, among other things. You've searched the area carefully, and[BR]"..
                   "can now say there are no other survivors."
end

-- |[ =============================== Upload =============================== ]|
--Upload information.
zQuestEntry:fnSetToQuest(sObjective)
zQuestEntry:fnSetDescription(sDescription)