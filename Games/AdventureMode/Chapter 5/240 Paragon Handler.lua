-- |[ ===================================== Paragon Handler ==================================== ]|
--Whenever the journal is opened to paragons mode, generates entries and toggles.

-- |[Listing]|
--Setup.
zaParagonDatabase = {}

--Set globals.
JournalEntry.zaAppendList = zaParagonDatabase

-- |[ ======= List Creation ======== ]|
--                      |[Internal Name]|     |[Display Name]|               |[SLF Paragon Grouping Name]|
JournalEntry:newParagon("RegulusCryo",      "Cryogenics Facility Main",  "RegulusCryo")
JournalEntry:newParagon("RegulusCryoSouth", "Cryogenics Facility South", "RegulusCryoSouth")
JournalEntry:newParagon("LowerCity",        "Lower Regulus City",        "LowerCity")
JournalEntry:newParagon("RegulusEast",      "Regulus Surface: East",     "RegulusEast")
JournalEntry:newParagon("RegulusSouth",     "Regulus Surface: South",    "RegulusSouth")
JournalEntry:newParagon("Equinox",          "Regulus Surface: West",     "Equinox")
JournalEntry:newParagon("SerenityCrater",   "Serenity Crater",           "SerenityCrater")
JournalEntry:newParagon("BlackSite",        "Black Site",                "BlackSite")
JournalEntry:newParagon("LRTFacility",      "LRT Facility",              "LRTFacility")
JournalEntry:newParagon("BiolabsAlpha",     "Biolabs: Alpha",            "BiolabsAlpha")
JournalEntry:newParagon("BiolabsBeta",      "Biolabs: Beta",             "BiolabsBeta")
JournalEntry:newParagon("BiolabsDatacore",  "Biolabs: Datacore",         "BiolabsDatacore")
JournalEntry:newParagon("BiolabsDelta",     "Biolabs: Delta",            "BiolabsDelta")
JournalEntry:newParagon("BiolabsEpsilon",   "Biolabs: Epsilon",          "BiolabsEpsilon")
JournalEntry:newParagon("BiolabsGamma",     "Biolabs: Gamma",            "BiolabsGamma")
JournalEntry:newParagon("BiolabsGenetics",  "Biolabs: Genetics",         "BiolabsGenetics")

-- |[Assemble List]|
--Upload the information from the list.
for i = 1, #zaParagonDatabase, 1 do
    zaParagonDatabase[i]:fnUploadData()
end

-- |[Finish Up]|
--Reset globals.
JournalEntry:fnCleanGlobals()

--Clear database to save memory.
zaParagonDatabase = nil
