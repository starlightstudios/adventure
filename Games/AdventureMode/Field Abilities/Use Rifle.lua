-- |[ ======================================= Use Rifle ======================================== ]|
--Sanya aims her rifle. Activates special code in the AdventureLevel to handle inputs.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================== Create Field Ability ================================== ]|
--Creates and stores the field ability. This is the prototype ability, Run/Cancel are called with
-- packages that point at the prototype ability.
if(iSwitchType == gciFieldAbility_Create) then
    FieldAbility_Create("Use Rifle", "Root/Special/Combat/FieldAbilities/Use Rifle")
        FieldAbility_SetProperty("Display Name", "Use Rifle")
        FieldAbility_SetProperty("Script Path", LM_GetCallStack(0))
        FieldAbility_SetProperty("Cooldown Max", 1)
        FieldAbility_SetProperty("Backing Image", gsAbility_Backing_Direct)
        FieldAbility_SetProperty("Frame Image", gsAbility_Frame_Special)
        FieldAbility_SetProperty("Main Image", "Root/Images/AdventureUI/Abilities/Sanya|Hunter|AimedShot")
        FieldAbility_SetProperty("Allocate Display Strings", 3)
        FieldAbility_SetProperty("Set Display Strings", 0, "Draw and aim your rifle. Can be used to stun enemies or shoot switches. Hitting certain enemies will cause them to")
        FieldAbility_SetProperty("Set Display Strings", 1, "drop special unique items. All enemies become 'alerted' when you shoot.")
        FieldAbility_SetProperty("Set Display Strings", 2, "You cannot retreat from battle for 20 seconds, and enemy viewcones become larger.")
    DL_PopActiveObject()
    
-- |[ ======================================== Execute ========================================= ]|
--Runs the ability. Typically called when the player pushes the matching button. Additional variables
-- local to each execution are provided, such as a timer and a control lock state.
elseif(iSwitchType == gciFieldAbility_Run) then
    
    --This variable commands which string displays if Sanya can't fire the rifle.
    local iRifleCode = VM_GetVar("Root/Variables/Global/Sanya/iRifleCode", "N")
    if(iRifleCode == gciSanyaRifle_CanFire) then
        AL_SetProperty("Activate Shooting")
    
    --Doesn't have her rifle.
    elseif(iRifleCode == gciSanyaRifle_DontHave) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I don't have my rifle!)") ]])
        fnCutsceneBlocker()
    
    --Really shouldn't be pointing guns here.
    elseif(iRifleCode == gciSanyaRifle_NotHere) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I really shouldn't be shooting anything here!)") ]])
        fnCutsceneBlocker()
    end
    
    --Expire the ability.
    AL_SetProperty("Set Field Ability Expired")
    
-- |[ =================================== Run While Cooling ==================================== ]|
--Runs the ability while it is cooling down. Cancels the ability.
elseif(iSwitchType == gciFieldAbility_RunWhileCooling) then
    --Never activates.
    
-- |[ ==================================== Cancel Execution ==================================== ]|
--Called when the ability cancels out for activating a cutscene spot.
elseif(iSwitchType == gciFieldAbility_Cancel) then
    AL_SetProperty("Set Field Ability Expired")
    
-- |[ ===================================== Touch an Enemy ===================================== ]|
--Called if the player touches an enemy to begin a battle while this field ability is active.
elseif(iSwitchType == gciFieldAbility_TouchEnemy) then
    --Never activates.
    
-- |[ ====================================== Modify Actor ====================================== ]|
--Called when an actor is touched, field abilities can then modify the TilemapActor on the top of
-- the activity stack.
elseif(iSwitchType == gciFieldAbility_ModifyActor) then
    --Never activates.

end
