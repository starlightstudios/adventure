-- |[ ================================= Ball Lightning Finish ================================== ]|
--Called when the Ball Lightning field ability is finished animating.

-- |[Delete Object]|
EM_PushEntity("Ball Lightning")
    RE_SetDestruct(true)
DL_PopActiveObject()

-- |[Mug]|
--Constants.
local fBallReach = gciSizePerTile * 4.0
    
--Get impact position.
local fBallLightningHitX = VM_GetVar("Root/Variables/FieldAbilities/All/fBallLightningHitX", "N")
local fBallLightningHitY = VM_GetVar("Root/Variables/FieldAbilities/All/fBallLightningHitY", "N")

--Mug every enemy in the impact zone.
AL_GetProperty("Build Objects In Range Of Position", fBallLightningHitX, fBallLightningHitY, fBallReach)
local iTotalHits = AL_GetProperty("Total Objects At Position")

--Zero hits, stop.
if(iTotalHits < 1) then return end

--Disallow auto-win.
local iPreviousAutoWin = VM_GetVar("Root/Variables/System/Special/iAllowAutoWinOnMug", "N")
VM_SetVar("Root/Variables/System/Special/iAllowAutoWinOnMug", "N", 0)

--Check all hits. Must be a TilemapActor to do anything.
for p = 0, iTotalHits-1, 1 do
    
    --Get variables.
    local iType = AL_GetProperty("Type Of Object At Position", p)
    local sName = AL_GetProperty("Name Of Object At Position", p)
    
    --Is type Actor:
    if(iType == gciLevel_SpecialType_Actor) then
        
        --The entity in question needs to be an enemy to get mugged. Anything else will ignore it.
        EM_PushEntity(sName)
            TA_SetProperty("Mug Now")
        DL_PopActiveObject()
    end
end

--Restore auto-win handler.
VM_SetVar("Root/Variables/System/Special/iAllowAutoWinOnMug", "N", iPreviousAutoWin)