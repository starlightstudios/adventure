-- |[ ==================================== Change Pamphlet ===================================== ]|
--Switches which pamphlet Izuna is showing people.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================== Create Field Ability ================================== ]|
--Creates and stores the field ability. This is the prototype ability, Run/Cancel are called with
-- packages that point at the prototype ability.
if(iSwitchType == gciFieldAbility_Create) then
    FieldAbility_Create("Change Pamphlet", "Root/Special/Combat/FieldAbilities/Change Pamphlet")
        FieldAbility_SetProperty("Display Name", "Change Pamphlet")
        FieldAbility_SetProperty("Script Path", LM_GetCallStack(0))
        FieldAbility_SetProperty("Cooldown Max", 1)
        FieldAbility_SetProperty("Backing Image", gsAbility_Backing_Direct)
        FieldAbility_SetProperty("Frame Image", gsAbility_Frame_Special)
        FieldAbility_SetProperty("Main Image", "Root/Images/AdventureUI/Abilities/Izuna|Common|ChangePamphlet")
        FieldAbility_SetProperty("Allocate Display Strings", 1)
        FieldAbility_SetProperty("Set Display Strings", 0, "Changes which pamphlet you're showing to people with 'Extra, Extra!'. Can be used at any time.")
    DL_PopActiveObject()
    
-- |[ ======================================== Execute ========================================= ]|
--Runs the ability. Typically called when the player pushes the matching button. Additional variables
-- local to each execution are provided, such as a timer and a control lock state.
elseif(iSwitchType == gciFieldAbility_Run) then
    
    --Decision.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Izuna] (Which pamphlet should I show off?)[B]") ]])
    
    --Decision script is an adjacent dialogue script.
    local sDecisionScript = "\"" .. fnResolvePath() .. "Change Pamphlet Dialogue.lua" .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    
    --Add pamhplets.
    if(AdInv_GetProperty("Item Count", "Pamphlet: Dragon") > 0) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Dragon\", " .. sDecisionScript .. ", \"Dragon\") ")
    end
    if(AdInv_GetProperty("Item Count", "Pamphlet: Green Westwoods") > 0) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Green Westwoods\", " .. sDecisionScript .. ", \"Green Westwoods\") ")
    end
    if(AdInv_GetProperty("Item Count", "Pamphlet: Bun Runners") > 0) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Bun Runners\", " .. sDecisionScript .. ", \"Bun Runners\") ")
    end
    if(AdInv_GetProperty("Item Count", "Pamphlet: Bandits in Westwoods") > 0) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Bandits in Westwoods\", " .. sDecisionScript .. ", \"Bandits in Westwoods\") ")
    end
    
    --Expire the ability.
    AL_SetProperty("Set Field Ability Expired")
    
-- |[ =================================== Run While Cooling ==================================== ]|
--Runs the ability while it is cooling down. Cancels the ability.
elseif(iSwitchType == gciFieldAbility_RunWhileCooling) then
    --Never activates.
    
-- |[ ==================================== Cancel Execution ==================================== ]|
--Called when the ability cancels out for activating a cutscene spot.
elseif(iSwitchType == gciFieldAbility_Cancel) then
    --Never activates.
    
-- |[ ===================================== Touch an Enemy ===================================== ]|
--Called if the player touches an enemy to begin a battle while this field ability is active.
elseif(iSwitchType == gciFieldAbility_TouchEnemy) then
    --Never activates.
    
-- |[ ====================================== Modify Actor ====================================== ]|
--Called when an actor is touched, field abilities can then modify the TilemapActor on the top of
-- the activity stack.
elseif(iSwitchType == gciFieldAbility_ModifyActor) then
    --Never activates.

end
