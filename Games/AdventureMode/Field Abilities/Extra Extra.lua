-- |[ ====================================== Extra, Extra! ===================================== ]|
--Shows people pamphlets!

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================== Create Field Ability ================================== ]|
--Creates and stores the field ability. This is the prototype ability, Run/Cancel are called with
-- packages that point at the prototype ability.
if(iSwitchType == gciFieldAbility_Create) then
    FieldAbility_Create("Extra Extra", "Root/Special/Combat/FieldAbilities/Extra Extra")
        FieldAbility_SetProperty("Display Name", "Extra, Extra!")
        FieldAbility_SetProperty("Script Path", LM_GetCallStack(0))
        FieldAbility_SetProperty("Cooldown Max", 1)
        FieldAbility_SetProperty("Backing Image", gsAbility_Backing_Direct)
        FieldAbility_SetProperty("Frame Image", gsAbility_Frame_Special)
        FieldAbility_SetProperty("Main Image", "Root/Images/AdventureUI/Abilities/Izuna|Common|ExtraExtra")
        FieldAbility_SetProperty("Allocate Display Strings", 2)
        FieldAbility_SetProperty("Set Display Strings", 0, "Shows whoever is unfortunate enough to be in front of you a pamphlet. If you have more than one, use")
        FieldAbility_SetProperty("Set Display Strings", 1, "'Change Pamphlet' to switch it.")
    DL_PopActiveObject()
    
-- |[ ======================================== Execute ========================================= ]|
--Runs the ability. Typically called when the player pushes the matching button. Additional variables
-- local to each execution are provided, such as a timer and a control lock state.
elseif(iSwitchType == gciFieldAbility_Run) then

    --Call the relevant field ability script.
    if(FS_Exists(gsFieldAbilityCheckPath) == false) then
        AL_SetProperty("Set Field Ability Expired")
        return
    end
    
    LM_ExecuteScript(gsFieldAbilityCheckPath, gciFieldAbility_Activate_ExtraExtra)
    
    --Expire the ability.
    AL_SetProperty("Set Field Ability Expired")
    
-- |[ =================================== Run While Cooling ==================================== ]|
--Runs the ability while it is cooling down. Cancels the ability.
elseif(iSwitchType == gciFieldAbility_RunWhileCooling) then
    --Never activates.
    
-- |[ ==================================== Cancel Execution ==================================== ]|
--Called when the ability cancels out for activating a cutscene spot.
elseif(iSwitchType == gciFieldAbility_Cancel) then
    --Never activates.
    
-- |[ ===================================== Touch an Enemy ===================================== ]|
--Called if the player touches an enemy to begin a battle while this field ability is active.
elseif(iSwitchType == gciFieldAbility_TouchEnemy) then
    --Never activates.
    
-- |[ ====================================== Modify Actor ====================================== ]|
--Called when an actor is touched, field abilities can then modify the TilemapActor on the top of
-- the activity stack.
elseif(iSwitchType == gciFieldAbility_ModifyActor) then
    --Never activates.

end
