-- |[ ======================================= Spot Weld ======================================== ]|
--Restores HP.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================== Create Field Ability ================================== ]|
--Creates and stores the field ability. This is the prototype ability, Run/Cancel are called with
-- packages that point at the prototype ability.
if(iSwitchType == gciFieldAbility_Create) then
    FieldAbility_Create("Spot Weld", "Root/Special/Combat/FieldAbilities/Spot Weld")
        FieldAbility_SetProperty("Display Name", "Spot Weld")
        FieldAbility_SetProperty("Script Path", LM_GetCallStack(0))
        FieldAbility_SetProperty("Cooldown Max", 1)
        FieldAbility_SetProperty("Backing Image", gsAbility_Backing_Direct)
        FieldAbility_SetProperty("Frame Image", gsAbility_Frame_Special)
        FieldAbility_SetProperty("Main Image", "Root/Images/AdventureUI/Abilities/Christine|PercussiveMaintenance")
        FieldAbility_SetProperty("Allocate Display Strings", 2)
        FieldAbility_SetProperty("Set Display Strings", 0, "Fix up the party by any means necessary. Restores 50% of everyone's HP.")
        FieldAbility_SetProperty("Set Display Strings", 1, "One charge. Rest at a campfire to restore charges.")
    DL_PopActiveObject()
    
-- |[ ======================================== Execute ========================================= ]|
--Runs the ability. Typically called when the player pushes the matching button. Additional variables
-- local to each execution are provided, such as a timer and a control lock state.
elseif(iSwitchType == gciFieldAbility_Run) then

    -- |[Christine Isn't Leading]|
    local iRaibieDrank = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieDrank", "N")
    local iRaibieQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N")
    if(iRaibieDrank == 2 and iRaibieQuest < 8) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (We can't use that field ability without Christine.)") ]])
        fnCutsceneBlocker()
        AL_SetProperty("Set Field Ability Expired")
        FieldAbility_SetProperty("Cooldown Max", 0)
        return
    end

    -- |[Normal Execution]|
    --Always expire the ability.
    AL_SetProperty("Set Field Ability Expired")
    
    --Variables.
    local iHasSpentSpotWeldCharge = VM_GetVar("Root/Variables/Chapter5/RestReset/iHasSpentSpotWeldCharge", "N")
    
    --If the value is zero, restore!
    if(iHasSpentSpotWeldCharge == 0.0) then
    
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/RestReset/iHasSpentSpotWeldCharge", "N", 1.0)
        
        --Sound effect.
        AudioManager_PlaySound("Combat|DoctorBag")
        
        --Get variables.
        local iActivePartySize = AdvCombat_GetProperty("Active Party Size")
        for i = 0, iActivePartySize-1, 1 do
            AdvCombat_SetProperty("Push Active Party Member By Slot", i)
            
                --Get HP values.
                local iHPCur = AdvCombatEntity_GetProperty("Health")
                local iHPMax = AdvCombatEntity_GetProperty("Health Max")
                local fHPPct = iHPCur / iHPMax
        
                --Add 50% to the HP and clamp at 100%.
                fHPPct = fHPPct + 0.50
                if(fHPPct >= 1.0) then fHPPct = 1.0 end
        
                --Set it.
                AdvCombatEntity_SetProperty("Health Percent", fHPPct)
    
            DL_PopActiveObject()
        end
    
    --Nonzero, can't use.
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (We'll need to rest before we can use Spot Weld again.)") ]])
        fnCutsceneBlocker()
    
    end
    
-- |[ =================================== Run While Cooling ==================================== ]|
--Runs the ability while it is cooling down. Cancels the ability.
elseif(iSwitchType == gciFieldAbility_RunWhileCooling) then
    
-- |[ ==================================== Cancel Execution ==================================== ]|
--Called when the ability cancels out for activating a cutscene spot.
elseif(iSwitchType == gciFieldAbility_Cancel) then
    
-- |[ ===================================== Touch an Enemy ===================================== ]|
--Called if the player touches an enemy to begin a battle while this field ability is active.
elseif(iSwitchType == gciFieldAbility_TouchEnemy) then
    --Never activates.
    
-- |[ ====================================== Modify Actor ====================================== ]|
--Called when an actor is touched, field abilities can then modify the TilemapActor on the top of
-- the activity stack.
elseif(iSwitchType == gciFieldAbility_ModifyActor) then
    --Never activates.

end
