-- |[ ===================================== Ball Lightning ===================================== ]|
--Electrosprite skill. Blasts an area in front of the player with a ball of lightning, mugging any
-- enemies in the impact zone.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Constants]|
local iCooldownTicks = 1800

-- |[ ================================== Create Field Ability ================================== ]|
--Creates and stores the field ability. This is the prototype ability, Run/Cancel are called with
-- packages that point at the prototype ability.
if(iSwitchType == gciFieldAbility_Create) then
    FieldAbility_Create("Ball Lightning", "Root/Special/Combat/FieldAbilities/Ball Lightning")
        FieldAbility_SetProperty("Display Name", "Ball Lightning")
        FieldAbility_SetProperty("Script Path", LM_GetCallStack(0))
        FieldAbility_SetProperty("Cooldown Max", iCooldownTicks)
        FieldAbility_SetProperty("Backing Image", gsAbility_Backing_Direct)
        FieldAbility_SetProperty("Frame Image", gsAbility_Frame_Special)
        FieldAbility_SetProperty("Main Image", "Root/Images/AdventureUI/Abilities/Christine|Storm")
        FieldAbility_SetProperty("Allocate Display Strings", 2)
        FieldAbility_SetProperty("Set Display Strings", 0, "Blasts an area in front of you, instantly mugging any enemies in it.")
        FieldAbility_SetProperty("Set Display Strings", 1, "30 second cooldown.")
    DL_PopActiveObject()
    
-- |[ ======================================== Execute ========================================= ]|
--Runs the ability. Typically called when the player pushes the matching button. Additional variables
-- local to each execution are provided, such as a timer and a control lock state.
elseif(iSwitchType == gciFieldAbility_Run or iSwitchType == gciFieldAbility_Cancel) then

    -- |[Christine Isn't Leading]|
    local iRaibieDrank = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieDrank", "N")
    local iRaibieQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N")
    if(iRaibieDrank == 2 and iRaibieQuest < 8) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (We can't use that field ability without Christine.)") ]])
        fnCutsceneBlocker()
        AL_SetProperty("Set Field Ability Expired")
        FieldAbility_SetProperty("Cooldown Max", 0)
        return
    end

    -- |[No Enemies]|
    if(AL_GetProperty("Any Active Enemies") == false) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I don't need to use that here.)") ]])
        fnCutsceneBlocker()
        AL_SetProperty("Set Field Ability Expired")
        FieldAbility_SetProperty("Cooldown Max", 0)
        return
    end

    -- |[Expire]|
    AL_SetProperty("Set Field Ability Expired")
    FieldAbility_SetProperty("Cooldown Max", iCooldownTicks)
    
    -- |[SFX]|
    AudioManager_PlaySound("FieldAbi|BallLightningLaunch")

    -- |[Spawn NPC]|
    TA_Create("Ball Lightning")
    
        --Basic.
        TA_SetProperty("Position", -1, -1)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        TA_SetProperty("Rendering Offsets", -30, -30)
        TA_SetProperty("Ignores Gravity", false)
        TA_SetProperty("Z Speed", -1)
        TA_SetProperty("Z Gravity", 0.0625)
        
        --Set graphics to a goat so something renders in case of an error.
		for i = 1, 8, 1 do
			for p = 1, 4, 2 do
				TA_SetProperty("Move Frame", i-1, p-1, "Root/Images/Sprites/BallLightning/00")
				TA_SetProperty("Run Frame",  i-1, p-1, "Root/Images/Sprites/BallLightning/00")
			end
			for p = 2, 4, 2 do
				TA_SetProperty("Move Frame", i-1, p-1, "Root/Images/Sprites/BallLightning/00")
				TA_SetProperty("Run Frame",  i-1, p-1, "Root/Images/Sprites/BallLightning/00")
			end
		end
        
        --Special frames:
        TA_SetProperty("Wipe Special Frames")
        TA_SetProperty("Add Special Frame", "Lightning00", "Root/Images/Sprites/BallLightning/00")
        TA_SetProperty("Add Special Frame", "Lightning01", "Root/Images/Sprites/BallLightning/01")
        TA_SetProperty("Add Special Frame", "Lightning02", "Root/Images/Sprites/BallLightning/02")
        TA_SetProperty("Add Special Frame", "Lightning03", "Root/Images/Sprites/BallLightning/03")
        TA_SetProperty("Add Special Frame", "Lightning04", "Root/Images/Sprites/BallLightning/04")
        TA_SetProperty("Add Special Frame", "Lightning05", "Root/Images/Sprites/BallLightning/05")
        TA_SetProperty("Add Special Frame", "Lightning06", "Root/Images/Sprites/BallLightning/06")
        TA_SetProperty("Add Special Frame", "Lightning07", "Root/Images/Sprites/BallLightning/07")
        TA_SetProperty("Add Special Frame", "Lightning08", "Root/Images/Sprites/BallLightning/08")
        TA_SetProperty("Add Special Frame", "Lightning09", "Root/Images/Sprites/BallLightning/09")
        TA_SetProperty("Add Special Frame", "Lightning10", "Root/Images/Sprites/BallLightning/10")
    DL_PopActiveObject()

    -- |[Positioning]|
    --Constants.
    local fBallReach = gciSizePerTile * 4.0

    --Get the party leader's position.
    EM_PushEntity(gsPartyLeaderName)
        local iPartyX, iPartyY = TA_GetProperty("Position")
        local iFacing = TA_GetProperty("Facing")
    DL_PopActiveObject()
    
    --Offset.
    iPartyX = iPartyX + 8
    iPartyY = iPartyY + 8
    
    --Turn to radians.
    local fRadians = (iFacing-2) * 45 * 3.1415926 / 180.0
    
    --Modify by rotation.
    local iTargetX = iPartyX + (math.cos(fRadians) * fBallReach) + 0
    local iTargetY = iPartyY + (math.sin(fRadians) * fBallReach) + 0
    
    --Save the target positions for a later script.
    VM_SetVar("Root/Variables/FieldAbilities/All/fBallLightningHitX", "N", iTargetX)
    VM_SetVar("Root/Variables/FieldAbilities/All/fBallLightningHitY", "N", iTargetY)
    
    -- |[Move Lightning]|
    --Constants.
    local iTravelTicks = 30
    local iExplodeStartFrame = 4
    local iExplodeTPF = 4
    local iExplodeFrames = 7
    local iExplodeTicks = iExplodeTPF * iExplodeFrames
    
    fnCutsceneSetFrame("Ball Lightning", "Lightning0")
    fnCutsceneTeleport("Ball Lightning", iPartyX / gciSizePerTile, iPartyY / gciSizePerTile)
    for i = 0, iTravelTicks-1, 1 do
        
        --Animation.
        if(i % 12 < 3) then
            fnCutsceneSetFrame("Ball Lightning", "Lightning00")
        elseif(i % 12 < 6) then
            fnCutsceneSetFrame("Ball Lightning", "Lightning01")
        elseif(i % 12 < 9) then
            fnCutsceneSetFrame("Ball Lightning", "Lightning02")
        else
            fnCutsceneSetFrame("Ball Lightning", "Lightning03")
        end
        
        --Movement.
        local fPct = i / iTravelTicks
        local fXPos = iPartyX + ((iTargetX - iPartyX) * fPct)
        local fYPos = iPartyY + ((iTargetY - iPartyY) * fPct)
        fnCutsceneTeleport("Ball Lightning", fXPos / gciSizePerTile, fYPos / gciSizePerTile)
        fnCutsceneBlocker()
    end

    -- |[Explode]|
    fnCutscenePlaySound("FieldAbi|BallLightningImpact")
    for i = 0, iExplodeTicks-1, 1 do
        local iFrame = math.floor(i / iExplodeTPF) + iExplodeStartFrame
        local sFrame = string.format("%02i", iFrame)
        fnCutsceneSetFrame("Ball Lightning", "Lightning" .. sFrame)
        fnCutsceneWait(2)
        fnCutsceneBlocker()
    end
    
    -- |[Fire Finishing Script]|
    fnCutsceneCall(gsRoot .. "Field Abilities/Ball Lightning Finish.lua")
    
-- |[ ===================================== Touch an Enemy ===================================== ]|
--Called if the player touches an enemy to begin a battle while this field ability is active.
elseif(iSwitchType == gciFieldAbility_TouchEnemy) then
    
-- |[ ====================================== Modify Actor ====================================== ]|
--Called when an actor is touched, field abilities can then modify the TilemapActor on the top of
-- the activity stack.
elseif(iSwitchType == gciFieldAbility_ModifyActor) then

end
