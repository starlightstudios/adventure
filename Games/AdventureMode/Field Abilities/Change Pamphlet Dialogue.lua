-- |[ ================================ Change Pamphlet Dialogue ================================ ]|
--Same basic format as a dialogue script, handles changing which pamphlet the player is using.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[UI]|
WD_SetProperty("Hide")

-- |[SFX]|
--Always plays a sound effect.
fnCutscenePlaySound("World|TakeItem")

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Dragon") then
    VM_SetVar("Root/Variables/Global/Izuna/sPamphlet", "S", "Dragon")
    
elseif(sTopicName == "Green Westwoods") then
    VM_SetVar("Root/Variables/Global/Izuna/sPamphlet", "S", "Green Westwoods")
    
elseif(sTopicName == "Bun Runners") then
    VM_SetVar("Root/Variables/Global/Izuna/sPamphlet", "S", "Bun Runners")
    
elseif(sTopicName == "Bandits in Westwoods") then
    VM_SetVar("Root/Variables/Global/Izuna/sPamphlet", "S", "Bandits in Westwoods")
end
