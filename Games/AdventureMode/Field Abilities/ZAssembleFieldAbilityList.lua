-- |[ =============================== Assemble Field Ability List ============================== ]|
--Assemble a list of all unlocked field abilities. Called whenever the field ability menu is opened.

-- |[Mei's Field Abilities]|
if(gsPartyLeaderName == "Mei" or fnIsCharacterPresent("Mei") == true) then
    if(AdvCombat_GetProperty("Is Ability Unlocked", "Mei", "Prowler", "Prowler|Deadly Jump") == true) then
        AM_SetProperty("Register Field Ability", "Root/Special/Combat/FieldAbilities/DeadlyJump")
    end
    if(AdvCombat_GetProperty("Is Ability Unlocked", "Mei", "Maid", "Maid|Wraithform") == true) then
        AM_SetProperty("Register Field Ability", "Root/Special/Combat/FieldAbilities/WraithForm")
    end
    if(AdvCombat_GetProperty("Is Ability Unlocked", "Mei", "Hive Scout", "Hive Scout|Scout Sight") == true) then
        AM_SetProperty("Register Field Ability", "Root/Special/Combat/FieldAbilities/ScoutSight")
    end
end

-- |[Florentina's Field Abilities]|
if(gsPartyLeaderName == "Florentina" or fnIsCharacterPresent("Florentina") == true) then
    if(AdvCombat_GetProperty("Is Ability Unlocked", "Florentina", "TreasureHunter", "Treasure Hunter|Pick Lock") == true) then
        AM_SetProperty("Register Field Ability", "Root/Special/Combat/FieldAbilities/PickLock")
    end
end

-- |[Sanya's Field Abilites]|
--These are contingent on variables, mostly.
if(gsPartyLeaderName == "Sanya" or fnIsCharacterPresent("Sanya") == true) then
    
    --Reset.
    local iGotResetAbility = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iGotResetAbility", "N")
    if(iGotResetAbility == 1.0) then
        AM_SetProperty("Register Field Ability", "Root/Special/Combat/FieldAbilities/Reset")
    end
    
    --Use Rifle.
    local iRecoveredRifle = VM_GetVar("Root/Variables/Global/Sanya/iRecoveredRifle", "N")
    if(iRecoveredRifle == 1.0) then
        AM_SetProperty("Register Field Ability", "Root/Special/Combat/FieldAbilities/Use Rifle")
    end
    
    --Pamphlets. These appear if the player has pamphlets in their inventory.
    local iTotal = 0
    iTotal = iTotal + AdInv_GetProperty("Item Count", "Pamphlet: Dragon")
    iTotal = iTotal + AdInv_GetProperty("Item Count", "Pamphlet: Green Westwoods")
    iTotal = iTotal + AdInv_GetProperty("Item Count", "Pamphlet: Bun Runners")
    iTotal = iTotal + AdInv_GetProperty("Item Count", "Pamphlet: Bandits in Westwoods")
    
    --Exactly one: Just 'Extra, Extra!'
    if(iTotal == 1) then
        AM_SetProperty("Register Field Ability", "Root/Special/Combat/FieldAbilities/Extra Extra")
        
    --More than one, both pamphlet abilities.
    elseif(iTotal > 1) then
        AM_SetProperty("Register Field Ability", "Root/Special/Combat/FieldAbilities/Extra Extra")
        AM_SetProperty("Register Field Ability", "Root/Special/Combat/FieldAbilities/Change Pamphlet")
    end
end

-- |[Christine's Field Abilities]|
if(gsPartyLeaderName == "Christine" or fnIsCharacterPresent("Christine") == true) then
    if(AdvCombat_GetProperty("Is Ability Unlocked", "Christine", "Starseer", "Starseer|Bend Space") == true) then
        AM_SetProperty("Register Field Ability", "Root/Special/Combat/FieldAbilities/Bend Space")
    end
    if(AdvCombat_GetProperty("Is Ability Unlocked", "Christine", "Brass Brawler", "Brass Brawler|Spot Weld") == true) then
        AM_SetProperty("Register Field Ability", "Root/Special/Combat/FieldAbilities/Spot Weld")
    end
    if(AdvCombat_GetProperty("Is Ability Unlocked", "Christine", "Live Wire", "Live Wire|Ball Lightning") == true) then
        AM_SetProperty("Register Field Ability", "Root/Special/Combat/FieldAbilities/Ball Lightning")
    end
end

-- |[Mod Field Abilities]|
fnExecModScript("Field Abilities/ZAssembleFieldAbilityList.lua")
