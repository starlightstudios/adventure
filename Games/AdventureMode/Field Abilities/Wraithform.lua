-- |[ ======================================= Wraithform ======================================= ]|
--Turns invisible, causing enemies to ignore the party for 10 seconds.
--During cancel mode, instantly stops.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================== Create Field Ability ================================== ]|
--Creates and stores the field ability. This is the prototype ability, Run/Cancel are called with
-- packages that point at the prototype ability.
if(iSwitchType == gciFieldAbility_Create) then
    FieldAbility_Create("Wraith Form", "Root/Special/Combat/FieldAbilities/WraithForm")
        FieldAbility_SetProperty("Display Name", "Wraith Form")
        FieldAbility_SetProperty("Script Path", LM_GetCallStack(0))
        FieldAbility_SetProperty("Cooldown Max", 600)
        FieldAbility_SetProperty("Backing Image", gsAbility_Backing_Direct)
        FieldAbility_SetProperty("Frame Image", gsAbility_Frame_Special)
        FieldAbility_SetProperty("Main Image", "Root/Images/AdventureUI/Abilities/Mei|Undying")
        FieldAbility_SetProperty("Allocate Display Strings", 2)
        FieldAbility_SetProperty("Set Display Strings", 0, "Use your ghostly powers to turn invisible. Enemies will ignore you entirely")
        FieldAbility_SetProperty("Set Display Strings", 1, "for 5 seconds. 10 second cooldown.")
    DL_PopActiveObject()
    
-- |[ ======================================== Execute ========================================= ]|
--Runs the ability. Typically called when the player pushes the matching button. Additional variables
-- local to each execution are provided, such as a timer and a control lock state.
elseif(iSwitchType == gciFieldAbility_Run) then

    -- |[Rubber Mode]|
    local iIsRubberMode = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
    if(iIsRubberMode == 1.0) then 
        FieldAbility_SetProperty("Cooldown Max", 0)
        return 
    end
    
    --Reset cooldown.
    FieldAbility_SetProperty("Cooldown Max", 600)
    
    -- |[Setup]|
    --Variables.
    local iInvisTime = 300
    local iTimer = AL_GetProperty("Field Ability Timer")
    
    -- |[Zeroth Tick]|
    --Do additional setup on the first tick.
    if(iTimer == 0) then
        giWraithformTimerOverride = nil
        TA_SetProperty("Enemies Always Ignore Player", true)
        EM_PushEntity(gsPartyLeaderName)
            TA_SetProperty("Disable Main Render", true)
        DL_PopActiveObject()
        
        --Disable render for all followers.
        for i = 1, #gsaFollowerNames, 1 do
            EM_PushEntity(gsaFollowerNames[i])
                TA_SetProperty("Disable Main Render", true)
            DL_PopActiveObject()
        end
    end
    
    --Special: Overrides the timer to cancel it immediately.
    if(giWraithformTimerOverride ~= nil) then
        iTimer = giWraithformTimerOverride
        giWraithformTimerOverride = nil
    end
    
    --Animate a poof!
    local iTicksPerFrame = 3
    if(iTimer < iTicksPerFrame*1) then
        EM_PushEntity(gsPartyLeaderName)
            TA_SetProperty("Set Manual Overlay", "Root/Images/Sprites/EnemyPoof/0")
        DL_PopActiveObject()
    elseif(iTimer < iTicksPerFrame*2) then
        EM_PushEntity(gsPartyLeaderName)
            TA_SetProperty("Set Manual Overlay", "Root/Images/Sprites/EnemyPoof/1")
        DL_PopActiveObject()
    elseif(iTimer < iTicksPerFrame*3) then
        EM_PushEntity(gsPartyLeaderName)
            TA_SetProperty("Set Manual Overlay", "Root/Images/Sprites/EnemyPoof/2")
        DL_PopActiveObject()
    elseif(iTimer < iTicksPerFrame*4) then
        EM_PushEntity(gsPartyLeaderName)
            TA_SetProperty("Set Manual Overlay", "Root/Images/Sprites/EnemyPoof/3")
        DL_PopActiveObject()
    else
        EM_PushEntity(gsPartyLeaderName)
            TA_SetProperty("Set Manual Overlay", "Null")
        DL_PopActiveObject()
    end
    
    -- |[Finale]|
    --On the final tick, order this object to self-destruct.
    if(iTimer >= iInvisTime) then
        AL_SetProperty("Set Field Ability Expired")
        TA_SetProperty("Enemies Always Ignore Player", false)
        EM_PushEntity(gsPartyLeaderName)
            TA_SetProperty("Disable Main Render", false)
            TA_SetProperty("Set Manual Overlay", "Null")
        DL_PopActiveObject()
        
        --Enable render for all followers.
        for i = 1, #gsaFollowerNames, 1 do
            EM_PushEntity(gsaFollowerNames[i])
                TA_SetProperty("Disable Main Render", false)
            DL_PopActiveObject()
        end
    end
    
-- |[ =================================== Run While Cooling ==================================== ]|
--Runs the ability while it is cooling down. Cancels the ability.
elseif(iSwitchType == gciFieldAbility_RunWhileCooling) then
    giWraithformTimerOverride = 300
    
-- |[ ==================================== Cancel Execution ==================================== ]|
--Called when the ability cancels out for activating a cutscene spot.
elseif(iSwitchType == gciFieldAbility_Cancel) then
    AL_SetProperty("Set Field Ability Expired")
    TA_SetProperty("Enemies Always Ignore Player", false)
    EM_PushEntity(gsPartyLeaderName)
        TA_SetProperty("Disable Main Render", false)
        TA_SetProperty("Set Manual Overlay", "Null")
    DL_PopActiveObject()
        
    --Enable render for all followers.
    for i = 1, #gsaFollowerNames, 1 do
        EM_PushEntity(gsaFollowerNames[i])
            TA_SetProperty("Disable Main Render", false)
        DL_PopActiveObject()
    end
    
    --Universal.
    giWraithformTimerOverride = nil
    
-- |[ ===================================== Touch an Enemy ===================================== ]|
--Called if the player touches an enemy to begin a battle while this field ability is active.
elseif(iSwitchType == gciFieldAbility_TouchEnemy) then
    --Never activates.
    
-- |[ ====================================== Modify Actor ====================================== ]|
--Called when an actor is touched, field abilities can then modify the TilemapActor on the top of
-- the activity stack.
elseif(iSwitchType == gciFieldAbility_ModifyActor) then
    --Never activates.

end
