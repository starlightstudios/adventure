-- |[ ======================================= Deadly Jump ====================================== ]|
--Combat script for Deadly Jump. Causes all enemies to have 100 stun at combat start.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ====================================== Begin Combat ====================================== ]|
if(iSwitchType == gciFieldAbility_Response_BeginCombat) then
    
    --For each enemy party member:
    local iEnemyPartySize = AdvCombat_GetProperty("Enemy Party Size")
    for i = 1, iEnemyPartySize, 1 do
        
        --Get ID and push.
        local iID = AdvCombat_GetProperty("Enemy ID", i-1)
        AdvCombat_SetProperty("Push Entity By ID", iID)
        
            --Add 100 to the stun value.
            local iCurrentStun = AdvCombatEntity_GetProperty("Stun")
            AdvCombatEntity_SetProperty("Stun", iCurrentStun + 100)
            
        --Clean up.
        DL_PopActiveObject()
    end
end
