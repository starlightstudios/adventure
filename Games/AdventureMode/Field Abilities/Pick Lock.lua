-- |[ ======================================== Pick Lock ======================================= ]|
--Picks a lock. Can open some doors in the world.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================== Create Field Ability ================================== ]|
--Creates and stores the field ability. This is the prototype ability, Run/Cancel are called with
-- packages that point at the prototype ability.
if(iSwitchType == gciFieldAbility_Create) then
    FieldAbility_Create("Pick Lock", "Root/Special/Combat/FieldAbilities/PickLock")
        FieldAbility_SetProperty("Display Name", "Pick Lock")
        FieldAbility_SetProperty("Script Path", LM_GetCallStack(0))
        FieldAbility_SetProperty("Cooldown Max", 1)
        FieldAbility_SetProperty("Backing Image", gsAbility_Backing_Direct)
        FieldAbility_SetProperty("Frame Image", gsAbility_Frame_Special)
        FieldAbility_SetProperty("Main Image", "Root/Images/AdventureUI/Abilities/Florentina|Botany")
        FieldAbility_SetProperty("Allocate Display Strings", 1)
        FieldAbility_SetProperty("Set Display Strings", 0, "Picks a lock. Only certain special locks can be picked.")
    DL_PopActiveObject()
    
-- |[ ========================================= Execute ======================================== ]|
--Runs the ability. Typically called when the player pushes the matching button. Additional variables
-- local to each execution are provided, such as a timer and a control lock state.
elseif(iSwitchType == gciFieldAbility_Run) then

    -- |[Rubber Mode]|
    local iIsRubberMode = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
    if(iIsRubberMode == 1.0) then 
        AL_SetProperty("Set Field Ability Expired")
        return
    end
    
    --If the file exists...
    gbFieldAbilityHandledInput = false
    if(FS_Exists(gsFieldAbilityCheckPath) == true) then
        LM_ExecuteScript(gsFieldAbilityCheckPath, gciFieldAbility_Activate_Florentina_PickLock)
    end
    
    --If the room didn't handle it, print some text.
    if(gbFieldAbilityHandledInput == false) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (There's no lock to pick here.)") ]])
        fnCutsceneBlocker()
    end
    
    --Expire the ability.
    AL_SetProperty("Set Field Ability Expired")
    
-- |[ ==================================== Cancel Execution ==================================== ]|
--Called when the ability cancels out for activating a cutscene spot.
elseif(iSwitchType == gciFieldAbility_Cancel) then
    AL_SetProperty("Set Field Ability Expired")
    
-- |[ ===================================== Touch an Enemy ===================================== ]|
--Called if the player touches an enemy to begin a battle while this field ability is active.
elseif(iSwitchType == gciFieldAbility_TouchEnemy) then
    --Never activates.
    
-- |[ ====================================== Modify Actor ====================================== ]|
--Called when an actor is touched, field abilities can then modify the TilemapActor on the top of
-- the activity stack.
elseif(iSwitchType == gciFieldAbility_ModifyActor) then
    --Never activates.

end
