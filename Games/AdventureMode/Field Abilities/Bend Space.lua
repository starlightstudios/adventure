-- |[ ======================================= Bend Space ======================================= ]|
--Starseer skill. Disables enemy viewcones for 10 seconds, 30 second cooldown.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ================================== Create Field Ability ================================== ]|
--Creates and stores the field ability. This is the prototype ability, Run/Cancel are called with
-- packages that point at the prototype ability.
if(iSwitchType == gciFieldAbility_Create) then
    FieldAbility_Create("Bend Space", "Root/Special/Combat/FieldAbilities/Bend Space")
        FieldAbility_SetProperty("Display Name", "Bend Space")
        FieldAbility_SetProperty("Script Path", LM_GetCallStack(0))
        FieldAbility_SetProperty("Cooldown Max", 1800)
        FieldAbility_SetProperty("Backing Image", gsAbility_Backing_Direct)
        FieldAbility_SetProperty("Frame Image", gsAbility_Frame_Special)
        FieldAbility_SetProperty("Main Image", "Root/Images/AdventureUI/Abilities/Christine|Eclipse")
        FieldAbility_SetProperty("Allocate Display Strings", 2)
        FieldAbility_SetProperty("Set Display Strings", 0, "Bends spacetime, rendering your party totally invisible to all enemies for 5 seconds.")
        FieldAbility_SetProperty("Set Display Strings", 1, "30 second cooldown.")
    DL_PopActiveObject()
    
-- |[ ======================================== Execute ========================================= ]|
--Runs the ability. Typically called when the player pushes the matching button. Additional variables
-- local to each execution are provided, such as a timer and a control lock state.
elseif(iSwitchType == gciFieldAbility_Run or iSwitchType == gciFieldAbility_Cancel) then

    -- |[Christine Isn't Leading]|
    local iRaibieDrank = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieDrank", "N")
    local iRaibieQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N")
    if(iRaibieDrank == 2 and iRaibieQuest < 8) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (We can't use that field ability without Christine.)") ]])
        fnCutsceneBlocker()
        AL_SetProperty("Set Field Ability Expired")
        FieldAbility_SetProperty("Cooldown Max", 0)
        return
    end
    
    -- |[Normal]|
    AL_SetProperty("Player Camo Timer", 600)
    AL_SetProperty("Set Field Ability Expired")
    FieldAbility_SetProperty("Cooldown Max", 1800)

-- |[ ===================================== Touch an Enemy ===================================== ]|
--Called if the player touches an enemy to begin a battle while this field ability is active.
elseif(iSwitchType == gciFieldAbility_TouchEnemy) then
    
-- |[ ====================================== Modify Actor ====================================== ]|
--Called when an actor is touched, field abilities can then modify the TilemapActor on the top of
-- the activity stack.
elseif(iSwitchType == gciFieldAbility_ModifyActor) then

end
