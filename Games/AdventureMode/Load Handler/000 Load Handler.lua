-- |[ ====================================== Load Handler ====================================== ]|
--Called when a game loads, handles routing variables where they need to go and creating C++ objects.
gbLoadDebug = false
local bShowDetailedPartyMemberInfo = false
Debug_PushPrint(gbLoadDebug, "Beginning Load Handler.\n")

--Achievement debug is disabled when loading.
local bStoredAchievementDebug = gbAchievementDebug
gbAchievementDebug = false

-- |[Chapter Variable]|
--This variable specifies which chapter the save was. It can be 0 or 1 for Chapter 1, otherwise
-- it's exactly equal to the chapter.
local iCurrentChapter = VM_GetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N")
Debug_Print("Chapter-specific loaders: " .. iCurrentChapter .. ".\n")

-- |[ ===================================== Structure Setup ==================================== ]|
--Common structure that changes properties based on which chapter is being loaded.
zLoad = {}

-- |[Chapter Select]|
local sCurrentLevel = AL_GetProperty("Name")
if(sCurrentLevel == "Nowhere") then
    
    --Common.
    zLoad.sDebugString      = "Running chapter 0 load.\n"
    zLoad.sMapLookupPath    = "Null"
    zLoad.bCheckNoMapVar    = false
    zLoad.sMapCheckVar      = "Null"
    zLoad.iMapCheckCase     = 0.0
    zLoad.sXPTablePath      = "Null"
    zLoad.sEnemyAliasPath   = "Null"
    zLoad.sParagonPath      = "Null"
    zLoad.sEnemyAutoPath    = "Null"
    zLoad.sPostCombatPath   = "Null"
    zLoad.sVolunteerPath    = "Null"
    zLoad.sSceneListPath    = "Null"
    zLoad.sSceneAliasPath   = "Null"
    zLoad.sWarpListPath     = "Null"
    zLoad.sWarpListName     = "Null"
    zLoad.sRetreatPath      = "Null"
    zLoad.sDefeatPath       = "Null"
    zLoad.sVictoryPath      = "Null"
    zLoad.sCombatMusic      = "Null"
    zLoad.fCombatMusicStart = 0.000
    zLoad.sLightVarPath     = "Null"
    zLoad.sCatalystName     = "Null"
    
    --System Paths
    zLoad.sStandardGameOver    = gsRoot .. "Chapter 1/Scenes/300 Standards/Defeat/Scene_Begin.lua"
    zLoad.sStandardRetreat     = gsRoot .. "Chapter 1/Scenes/300 Standards/Retreat/Scene_Begin.lua"
    zLoad.sStandardRevert      = gsRoot .. "Chapter 1/Scenes/300 Standards/Revert/Scene_Begin.lua"
    zLoad.sStandardReliveBegin = gsRoot .. "Chapter 1/Scenes/Z Relive Handler/Scene_Begin.lua"
    zLoad.sStandardReliveEnd   = gsRoot .. "Chapter 1/Scenes/Z Relive Handler/Scene_End.lua"
    zLoad.sBestiaryHandler     = gsRoot .. "Chapter 1/200 Bestiary Handler.lua"
    zLoad.sProfileHandler      = gsRoot .. "Chapter 1/210 Profile Handler.lua"
    zLoad.sQuestHandler        = gsRoot .. "Chapter 1/220 Quest Handler.lua"
    zLoad.sLocationHandler     = gsRoot .. "Chapter 1/230 Location Handler.lua"
    zLoad.sParagonHandler      = gsRoot .. "Chapter 1/240 Paragon Handler.lua"
    zLoad.sCombatHandler       = gsRoot .. "Chapter 0/240 Combat Handler.lua"
    zLoad.sKOTrackerPath       = "Root/Variables/Chapter1/KOTracker/"
    zLoad.sAutosaveIconPath    = "Null"
    
    --Images.
    zLoad.sFormIcon         = "Root/Images/AdventureUI/CampfireMenuIcon/TransformMei"
    zLoad.sReliveIcon       = "Root/Images/AdventureUI/CampfireMenuIcon/ReliveMei"
    zLoad.sCostumeIcon      = "Root/Images/AdventureUI/CampfireMenuIcon/Costume"
    zLoad.sAchievementTitle = "Plantchievements"
    zLoad.sAchievementShow  = "[IMG0] Show Plantchievements"
    zLoad.sAchievementHide  = "[IMG0] Hide Plantchievements"
    zLoad.sAchievementPath  = "Root/Images/AdventureUI/Journal/Static Parts Achievements Over Ch1"
    
    --Field Abilities
    zLoad.saFieldAbilityPaths = {}

-- |[Chapter 1]|
elseif(iCurrentChapter == 0.0 or iCurrentChapter == 1.0) then
    
    --Common.
    zLoad.sDebugString      = "Running chapter 1 load.\n"
    zLoad.sMapLookupPath    = gsRoot .. "Maps/Z Map Lookups/Chapter 1 Lookups.lua"
    zLoad.bCheckNoMapVar    = true
    zLoad.sMapCheckVar      = "Root/Variables/Chapter1/Scenes/iHasNoMap"
    zLoad.iMapCheckCase     = 0.0
    zLoad.sXPTablePath      = gsRoot .. "Combat/Party/XP Table Chapter 1.lua"
    zLoad.sEnemyAliasPath   = gsRoot .. "Combat/Enemies/Chapter 1/Alias List.lua"
    zLoad.sParagonPath      = gsRoot .. "Combat/Enemies/Chapter 1/Paragon Handler.lua"
    zLoad.sEnemyAutoPath    = gsRoot .. "Combat/Enemies/Chapter 1/Enemy Auto Handler.lua"
    zLoad.sPostCombatPath   = gsRoot .. "Combat/Enemies/Chapter 1/Post Combat Handler.lua"
    zLoad.sVolunteerPath    = gsRoot .. "Chapter 1/100 Volunteer Handler.lua"
    zLoad.sSceneListPath    = gsRoot .. "Chapter 1/Scenes/Build Scene List.lua"
    zLoad.sSceneAliasPath   = gsRoot .. "Chapter 1/Scenes/Build Alias List.lua"
    zLoad.sWarpListPath     = gsRoot .. "Maps/Build Debug Warp List.lua"
    zLoad.sWarpListName     = "Chapter 1"
    zLoad.sRetreatPath      = gsRoot .. "Chapter 1/Scenes/300 Standards/Retreat/Scene_Begin.lua"
    zLoad.sDefeatPath       = gsRoot .. "Chapter 1/Scenes/300 Standards/Defeat/Scene_Begin.lua"
    zLoad.sVictoryPath      = gsRoot .. "Chapter 1/Scenes/300 Standards/Victory/Scene_Begin.lua"
    zLoad.sCombatMusic      = "BattleThemeMei"
    zLoad.fCombatMusicStart = 93.816
    zLoad.sLightVarPath     = "Root/Variables/Chapter5/Scenes/iHasLantern"
    zLoad.sCatalystName     = "Chapter 1"
    
    --System Paths
    zLoad.sStandardGameOver    = gsRoot .. "Chapter 1/Scenes/300 Standards/Defeat/Scene_Begin.lua"
    zLoad.sStandardRetreat     = gsRoot .. "Chapter 1/Scenes/300 Standards/Retreat/Scene_Begin.lua"
    zLoad.sStandardRevert      = gsRoot .. "Chapter 1/Scenes/300 Standards/Revert/Scene_Begin.lua"
    zLoad.sStandardReliveBegin = gsRoot .. "Chapter 1/Scenes/Z Relive Handler/Scene_Begin.lua"
    zLoad.sStandardReliveEnd   = gsRoot .. "Chapter 1/Scenes/Z Relive Handler/Scene_End.lua"
    zLoad.sBestiaryHandler     = gsRoot .. "Chapter 1/200 Bestiary Handler.lua"
    zLoad.sProfileHandler      = gsRoot .. "Chapter 1/210 Profile Handler.lua"
    zLoad.sQuestHandler        = gsRoot .. "Chapter 1/220 Quest Handler.lua"
    zLoad.sLocationHandler     = gsRoot .. "Chapter 1/230 Location Handler.lua"
    zLoad.sParagonHandler      = gsRoot .. "Chapter 1/240 Paragon Handler.lua"
    zLoad.sCombatHandler       = gsRoot .. "Chapter 0/240 Combat Handler.lua"
    zLoad.sKOTrackerPath       = "Root/Variables/Chapter1/KOTracker/"
    zLoad.sAutosaveIconPath    = "Root/Images/AdventureUI/Symbols22/RuneMei"
    
    --Images.
    zLoad.sFormIcon         = "Root/Images/AdventureUI/CampfireMenuIcon/TransformMei"
    zLoad.sReliveIcon       = "Root/Images/AdventureUI/CampfireMenuIcon/ReliveMei"
    zLoad.sCostumeIcon      = "Root/Images/AdventureUI/CampfireMenuIcon/Costume"
    zLoad.sAchievementTitle = "Plantchievements"
    zLoad.sAchievementShow  = "[IMG0] Show Plantchievements"
    zLoad.sAchievementHide  = "[IMG0] Hide Plantchievements"
    zLoad.sAchievementPath  = "Root/Images/AdventureUI/Journal/Static Parts Achievements Over Ch1"
    
    --Field Abilities
    zLoad.saFieldAbilityPaths = {}
    zLoad.saFieldAbilityPaths[1] = gsFieldAbilityListing .. "Deadly Jump.lua"
    zLoad.saFieldAbilityPaths[2] = gsFieldAbilityListing .. "Wraithform.lua"
    zLoad.saFieldAbilityPaths[3] = gsFieldAbilityListing .. "Scout Sight.lua"
    zLoad.saFieldAbilityPaths[4] = gsFieldAbilityListing .. "Pick Lock.lua"
    
    --Load interrupt.
	if(gsMandateTitle ~= "Witch Hunter Izana") then
		LI_BootChapter1()
	end

    --When not loading assets immediately, reset this flag.
    if(gbAlwaysLoadImmediately == false) then
        gbLoadedChapter1Assets = false
    end

    --Order assets to load. Normally this happens in the chapter 000 Initialize.lua file, but that
    -- file is run before we know what chapter it is.
    --If they already loaded, do nothing.
    if(gbLoadedChapter1Assets ~= true) then
        
        --Flag.
        Debug_Print(" Loading assets for chapter 1.\n")
        gbLoadedChapter1Assets = true
        
        --Load Sequence
        fnIssueLoadReset("AdventureModeCH1")
        LM_ExecuteScript(gsRoot .. "Chapter 1/Loading/100 Sprites.lua")
        LM_ExecuteScript(gsRoot .. "Chapter 1/Loading/101 Portraits.lua")
        LM_ExecuteScript(gsRoot .. "Chapter 1/Loading/102 Actors.lua")
        LM_ExecuteScript(gsRoot .. "Chapter 1/Loading/103 Scenes.lua")
        LM_ExecuteScript(gsRoot .. "Chapter 1/Loading/104 Overlays.lua")
        LM_ExecuteScript(gsRoot .. "Chapter 1/Loading/105 Audio.lua")
        SLF_Close()
        fnCompleteLoadSequence()  
        Debug_Print(" Loading assets done.\n") 
        
    end

-- |[Chapter 2]|
elseif(iCurrentChapter == 2.0) then
    
    --Common.
    zLoad.sDebugString      = "Running chapter 2 load.\n"
    zLoad.sMapLookupPath    = gsRoot .. "Maps/Z Map Lookups/Chapter 2 Lookups.lua"
    zLoad.bCheckNoMapVar    = true
    zLoad.sMapCheckVar      = "Root/Variables/Chapter2/Scenes/iHasNoMap"
    zLoad.iMapCheckCase     = 0.0
    zLoad.sXPTablePath      = gsRoot .. "Combat/Party/XP Table Chapter 2.lua"
    zLoad.sEnemyAliasPath   = gsRoot .. "Combat/Enemies/Chapter 2/Alias List.lua"
    zLoad.sParagonPath      = gsRoot .. "Combat/Enemies/Chapter 2/Paragon Handler.lua"
    zLoad.sEnemyAutoPath    = gsRoot .. "Combat/Enemies/Chapter 2/Enemy Auto Handler.lua"
    zLoad.sPostCombatPath   = gsRoot .. "Combat/Enemies/Chapter 2/Post Combat Handler.lua"
    zLoad.sVolunteerPath    = gsRoot .. "Chapter 2/100 Volunteer Handler.lua"
    zLoad.sSceneListPath    = gsRoot .. "Chapter 2/Scenes/Build Scene List.lua"
    zLoad.sSceneAliasPath   = gsRoot .. "Chapter 2/Scenes/Build Alias List.lua"
    zLoad.sWarpListPath     = gsRoot .. "Maps/Build Debug Warp List.lua"
    zLoad.sWarpListName     = "Chapter 2"
    zLoad.sRetreatPath      = gsRoot .. "Chapter 2/Scenes/300 Standards/Retreat/Scene_Begin.lua"
    zLoad.sDefeatPath       = gsRoot .. "Chapter 2/Scenes/300 Standards/Defeat/Scene_Begin.lua"
    zLoad.sVictoryPath      = gsRoot .. "Chapter 2/Scenes/300 Standards/Victory/Scene_Begin.lua"
    zLoad.sCombatMusic      = "BattleThemeSanya"
    zLoad.fCombatMusicStart = 0.000
    zLoad.sLightVarPath     = "Null"
    zLoad.sCatalystName     = "Chapter 2"
    
    --System Paths
    zLoad.sStandardGameOver    = gsRoot .. "Chapter 2/Scenes/300 Standards/Defeat/Scene_Begin.lua"
    zLoad.sStandardRetreat     = gsRoot .. "Chapter 2/Scenes/300 Standards/Retreat/Scene_Begin.lua"
    zLoad.sStandardRevert      = gsRoot .. "Chapter 2/Scenes/300 Standards/Revert/Scene_Begin.lua"
    zLoad.sStandardReliveBegin = gsRoot .. "Chapter 2/Scenes/Z Relive Handler/Scene_Begin.lua"
    zLoad.sStandardReliveEnd   = gsRoot .. "Chapter 2/Scenes/Z Relive Handler/Scene_End.lua"
    zLoad.sBestiaryHandler     = gsRoot .. "Chapter 2/200 Bestiary Handler.lua"
    zLoad.sProfileHandler      = gsRoot .. "Chapter 2/210 Profile Handler.lua"
    zLoad.sQuestHandler        = gsRoot .. "Chapter 2/220 Quest Handler.lua"
    zLoad.sLocationHandler     = gsRoot .. "Chapter 2/230 Location Handler.lua"
    zLoad.sParagonHandler      = gsRoot .. "Chapter 2/240 Paragon Handler.lua"
    zLoad.sCombatHandler       = gsRoot .. "Chapter 0/240 Combat Handler.lua"
    zLoad.sKOTrackerPath       = "Root/Variables/Chapter2/KOTracker/"
    zLoad.sAutosaveIconPath    = "Root/Images/AdventureUI/Symbols22/RuneSanya"
    
    --Images.
    zLoad.sFormIcon         = "Root/Images/AdventureUI/CampfireMenuIcon/TransformSanya"
    zLoad.sReliveIcon       = "Root/Images/AdventureUI/CampfireMenuIcon/ReliveSanya"
    zLoad.sCostumeIcon      = "Root/Images/AdventureUI/CampfireMenuIcon/Costume"
    zLoad.sAchievementTitle = "Plantchievements"
    zLoad.sAchievementShow  = "[IMG0] Show Plantchievements"
    zLoad.sAchievementHide  = "[IMG0] Hide Plantchievements"
    zLoad.sAchievementPath  = "Root/Images/AdventureUI/Journal/Static Parts Achievements Over Ch1"
    
    --Field Abilities
    zLoad.saFieldAbilityPaths = {}
    zLoad.saFieldAbilityPaths[1] = gsFieldAbilityListing .. "Reset.lua"
    zLoad.saFieldAbilityPaths[2] = gsFieldAbilityListing .. "Use Rifle.lua"
    zLoad.saFieldAbilityPaths[3] = gsFieldAbilityListing .. "Extra Extra.lua"
    zLoad.saFieldAbilityPaths[4] = gsFieldAbilityListing .. "Change Pamphlet.lua"
    
    --Load interrupt.
	if(gsMandateTitle ~= "Witch Hunter Izana") then
		LI_BootChapter2()
	end

    --When not loading assets immediately, reset this flag.
    if(gbAlwaysLoadImmediately == false) then
        gbLoadedChapter2Assets = false
    end

    --Order assets to load. Normally this happens in the chapter 000 Initialize.lua file, but that
    -- file is run before we know what chapter it is.
    --If they already loaded, do nothing.
    if(gbLoadedChapter2Assets ~= true) then
        
        --Flag.
        Debug_Print(" Loading assets for chapter 2.\n")
        gbLoadedChapter2Assets = true
        
        --Load Sequence
        fnIssueLoadReset("AdventureModeCH2")
        local sDirectory = gsRoot .. "Chapter 2/Loading/"
        LM_ExecuteScript(sDirectory .. "100 Sprites.lua")
        LM_ExecuteScript(sDirectory .. "101 Portraits.lua")
        LM_ExecuteScript(sDirectory .. "102 Actors.lua")
        LM_ExecuteScript(sDirectory .. "103 Scenes.lua")
        LM_ExecuteScript(sDirectory .. "104 Overlays.lua")
        LM_ExecuteScript(sDirectory .. "105 Audio.lua")
        SLF_Close()
        fnCompleteLoadSequence()  
        Debug_Print(" Loading assets done.\n") 
    
    end

    -- |[Special Functions]|
    LM_ExecuteScript(gsRoot .. "Subroutines/Chapter 2/fnResolveKitsuneState.lua")
    
-- |[Chapter 5]|
elseif(iCurrentChapter == 5.0) then

    --Common.
    zLoad.sDebugString      = "Running chapter 5 load.\n"
    zLoad.sMapLookupPath    = gsRoot .. "Maps/Z Map Lookups/Chapter 5 Lookups.lua"
    zLoad.bCheckNoMapVar    = true
    zLoad.sMapCheckVar      = "Root/Variables/Chapter5/Scenes/iReceivedMap"
    zLoad.iMapCheckCase     = 1.0
    zLoad.sXPTablePath      = gsRoot .. "Combat/Party/XP Table Chapter 5.lua"
    zLoad.sEnemyAliasPath   = gsRoot .. "Combat/Enemies/Chapter 5/Alias List.lua"
    zLoad.sParagonPath      = gsRoot .. "Combat/Enemies/Chapter 5/Paragon Handler.lua"
    zLoad.sEnemyAutoPath    = gsRoot .. "Combat/Enemies/Chapter 5/Enemy Auto Handler.lua"
    zLoad.sPostCombatPath   = gsRoot .. "Combat/Enemies/Chapter 5/Post Combat Handler.lua"
    zLoad.sVolunteerPath    = gsRoot .. "Chapter 5/100 Volunteer Handler.lua"
    zLoad.sSceneListPath    = gsRoot .. "Chapter 5/Scenes/Build Scene List.lua"
    zLoad.sSceneAliasPath   = gsRoot .. "Chapter 5/Scenes/Build Alias List.lua"
    zLoad.sWarpListPath     = gsRoot .. "Maps/Build Debug Warp List.lua"
    zLoad.sWarpListName     = "Chapter 5"
    zLoad.sRetreatPath      = gsRoot .. "Chapter 5/Scenes/300 Standards/Retreat/Scene_Begin.lua"
    zLoad.sDefeatPath       = gsRoot .. "Chapter 5/Scenes/300 Standards/Defeat/Scene_Begin.lua"
    zLoad.sVictoryPath      = gsRoot .. "Chapter 5/Scenes/300 Standards/Victory/Scene_Begin.lua"
    zLoad.sCombatMusic      = "BattleThemeChristine"
    zLoad.fCombatMusicStart = 0.000
    zLoad.sLightVarPath     = "Root/Variables/Chapter5/Scenes/iHasLantern"
    zLoad.sCatalystName     = "Chapter 5-1"
    
    --System Paths
    zLoad.sStandardGameOver    = gsRoot .. "Chapter 5/Scenes/300 Standards/Defeat/Scene_Begin.lua"
    zLoad.sStandardRetreat     = gsRoot .. "Chapter 5/Scenes/300 Standards/Retreat/Scene_Begin.lua"
    zLoad.sStandardRevert      = gsRoot .. "Chapter 5/Scenes/300 Standards/Revert/Scene_Begin.lua"
    zLoad.sStandardReliveBegin = gsRoot .. "Chapter 5/Scenes/999 Relive Handler/Scene_Begin.lua"
    zLoad.sStandardReliveEnd   = gsRoot .. "Chapter 5/Scenes/999 Relive Handler/Scene_End.lua"
    zLoad.sBestiaryHandler     = gsRoot .. "Chapter 5/200 Bestiary Handler.lua"
    zLoad.sProfileHandler      = gsRoot .. "Chapter 5/210 Profile Handler.lua"
    zLoad.sQuestHandler        = gsRoot .. "Chapter 5/220 Quest Handler.lua"
    zLoad.sLocationHandler     = gsRoot .. "Chapter 5/230 Location Handler.lua"
    zLoad.sParagonHandler      = gsRoot .. "Chapter 5/240 Paragon Handler.lua"
    zLoad.sCombatHandler       = gsRoot .. "Chapter 0/240 Combat Handler.lua"
    zLoad.sKOTrackerPath       = "Root/Variables/Chapter5/KOTracker/"
    zLoad.sAutosaveIconPath    = "Root/Images/AdventureUI/Symbols22/RuneChristine"
    
    --Images.
    zLoad.sFormIcon    = "Root/Images/AdventureUI/CampfireMenuIcon/TransformChristine"
    zLoad.sReliveIcon  = "Root/Images/AdventureUI/CampfireMenuIcon/ReliveChristine"
    zLoad.sCostumeIcon = "Root/Images/AdventureUI/CampfireMenuIcon/Costume"
    zLoad.sAchievementTitle = "Metals of Honor"
    zLoad.sAchievementShow  = "[IMG0] Show Metals"
    zLoad.sAchievementHide  = "[IMG0] Hide Metals"
    zLoad.sAchievementPath  = "Root/Images/AdventureUI/Journal/Static Parts Achievements Over Ch5"
    
    --Field Abilities
    zLoad.saFieldAbilityPaths = {}
    zLoad.saFieldAbilityPaths[1] = gsFieldAbilityListing .. "Bend Space.lua"
    zLoad.saFieldAbilityPaths[2] = gsFieldAbilityListing .. "Spot Weld.lua"
    zLoad.saFieldAbilityPaths[3] = gsFieldAbilityListing .. "Ball Lightning.lua"
    
    --Load interrupt.
	if(gsMandateTitle ~= "Witch Hunter Izana") then
		LI_BootChapter5()
	end

    --When not loading assets immediately, reset this flag.
    if(gbAlwaysLoadImmediately == false) then
        gbLoadedChapter5Assets = false
    end
    
    --Order assets to load. Do nothing if already loaded.
    if(gbLoadedChapter5Assets ~= true) then
        
        --Flag.
        Debug_Print(" Loading assets for chapter 5.\n")
        gbLoadedChapter5Assets = true
        
        --Load Sequence
        fnIssueLoadReset("AdventureModeCH5")
        LM_ExecuteScript(gsRoot .. "Chapter 5/Loading/100 Sprites.lua")
        LM_ExecuteScript(gsRoot .. "Chapter 5/Loading/101 Portraits.lua")
        LM_ExecuteScript(gsRoot .. "Chapter 5/Loading/102 Actors.lua")
        LM_ExecuteScript(gsRoot .. "Chapter 5/Loading/103 Scenes.lua")
        LM_ExecuteScript(gsRoot .. "Chapter 5/Loading/104 Overlays.lua")
        LM_ExecuteScript(gsRoot .. "Chapter 5/Loading/105 Audio.lua")
        SLF_Close()
        fnCompleteLoadSequence()  
        Debug_Print(" Loading assets done.\n")  
        
    end

	--If Christine has transformed, modify her properties.
	local iIsPostGolemScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsPostGolemScene", "N")
	if(iIsPostGolemScene == 1.0) then
        
        --Use Christine's voice.
		WD_SetProperty("Set Leader Voice", "Christine")
        
        --Set Christine to no longer have the "Teacher" job in her UI.
        AdvCombat_SetProperty("Push Party Member", "Christine")
            AdvCombatEntity_SetProperty("Push Job S", "Teacher")
                AdvCombatJob_SetProperty("Appears on Skills UI", false)
            DL_PopActiveObject()
        DL_PopActiveObject()
    
    --Christine has never transformed. Set these to "Unavailable".
    else
        zLoad.sFormIcon   = "Root/Images/AdventureUI/CampfireMenuIcon/Unavailable"
        zLoad.sReliveIcon = "Root/Images/AdventureUI/CampfireMenuIcon/Unavailable"
	end
        
    --Modify 56's properties so she doesn't share dialogue with 55. This occurs after a certain scene.
    local iStarted56Sequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStarted56Sequence", "N")
    if(iStarted56Sequence >= 1.0) then
        DialogueActor_Push("2856")
            DialogueActor_SetProperty("Remove Alias", "55")
            DialogueActor_SetProperty("Remove Alias", "2855")
        DL_PopActiveObject()
    end
    
    --Reached biolabs, modify which catalyst set we're using.
    local iReachedBiolabs = VM_GetVar("Root/Variables/Chapter5/Scenes/iReachedBiolabs", "N")
    if(iReachedBiolabs == 1.0) then
        zLoad.sCatalystName = "Chapter 5-2"
    end

-- |[Unhandled / Mod]|
--Unhandled chapter. Ask the mods if they can handle it.
else

    --Scan mods.
    local bModHandledLoad = fnExecModScript("Load Handler/Load Handler.lua")
    
    --No mod handled load, print an error.
    if(bModHandledLoad == false) then
        Debug_ForcePrint("Error: Chapter " ..  iCurrentChapter .. " unresolved.\n")
    end
end

-- |[ ======================================== Map Setup ======================================= ]|
-- |[Debug]|
Debug_Print(zLoad.sDebugString)
Debug_Print("Running map setup.\n")

-- |[UI Images]|
AM_SetProperty("Set Form Icon",                      zLoad.sFormIcon)
AM_SetProperty("Set Relive Icon",                    zLoad.sReliveIcon)
AM_SetProperty("Set Costume Icon",                   zLoad.sCostumeIcon)
AM_SetPropertyJournal("Set Achievement Title",       zLoad.sAchievementTitle)
AM_SetPropertyJournal("Set Achievement Show String", zLoad.sAchievementShow)
AM_SetPropertyJournal("Set Achievement Hide String", zLoad.sAchievementHide)
AM_SetPropertyJournal("Set Achievement Over Image",  zLoad.sAchievementPath)
    
-- |[Run Lookup Builder]|
--Re-run the map lookup builder.
if(zLoad.sMapLookupPath ~= "Null") then LM_ExecuteScript(zLoad.sMapLookupPath) end

--If there is a re-resolve function, call it.
if(gfnLastMapFunction ~= nil) then
    
    --Chapter 2:
    if(iCurrentChapter == 1.0 or iCurrentChapter == 2.0) then
        gfnLastMapFunction(sCurrentLevel)
    else
        gfnLastMapFunction(giLastPlayerX, giLastPlayerY, giLastRemapX, giLastRemapY)
    end

--Otherwise, resolve maps as normal.
else
    fnResolveMapLocation(sCurrentLevel)
end

--Remove the map if the party has not found it yet. The variable is a number, if it's 1
-- remove the map, otherwise keep it.
if(zLoad.bCheckNoMapVar == true) then
    local iHasNoMap = VM_GetVar(zLoad.sMapCheckVar, "N")
    if(iHasNoMap ~= zLoad.iMapCheckCase) then
        AM_SetMapInfo("Null", "Null", 0, 0)
    end
end

-- |[Chapter 2]|
--Has unique handling.

-- |[ ======================================= Path Setup ======================================= ]|
--To load correctly, all chapter initializers run once. This code sets the paths to whichever chapter
-- is currently active.
Debug_Print("Running path setup.\n")
if(zLoad.sEnemyAliasPath ~= "Null") then LM_ExecuteScript(zLoad.sEnemyAliasPath) end
if(zLoad.sParagonPath ~= "Null")    then AdvCombat_SetProperty("Set Paragon Script",      zLoad.sParagonPath) end
if(zLoad.sEnemyAutoPath ~= "Null")  then AdvCombat_SetProperty("Set Enemy Auto Script",   zLoad.sEnemyAutoPath) end
if(zLoad.sPostCombatPath ~= "Null") then AdvCombat_SetProperty("Set Post Combat Handler", zLoad.sPostCombatPath) end
if(zLoad.sVolunteerPath ~= "Null")  then AdvCombat_SetProperty("Set Volunteer Script",    zLoad.sVolunteerPath) end
if(zLoad.sSceneListPath ~= "Null")  then LM_ExecuteScript(zLoad.sSceneListPath) end
if(zLoad.sSceneAliasPath ~= "Null") then LM_ExecuteScript(zLoad.sSceneAliasPath) end
if(zLoad.sWarpListPath ~= "Null")   then LM_ExecuteScript(zLoad.sWarpListPath, zLoad.sWarpListName) end
AdvCombat_SetProperty("Standard Retreat Script", zLoad.sRetreatPath)
AdvCombat_SetProperty("Standard Defeat Script",  zLoad.sDefeatPath)
AdvCombat_SetProperty("Standard Victory Script", zLoad.sVictoryPath)

--Combat variables.
DL_AddPath(zLoad.sKOTrackerPath)
AdvCombat_SetProperty("Default Combat Music", zLoad.sCombatMusic, zLoad.fCombatMusicStart)
VM_SetVar("Root/Variables/Global/Combat/sKOTrackerPath", "S", zLoad.sKOTrackerPath)

--System Standard Paths
gsStandardGameOver     = zLoad.sStandardGameOver
gsStandardRetreat      = zLoad.sStandardRetreat
gsStandardRevert       = zLoad.sStandardRevert
gsStandardReliveBegin  = zLoad.sStandardReliveBegin
gsStandardReliveEnd    = zLoad.sStandardReliveEnd

--Menu Paths
AM_SetPropertyJournal("Bestiary Resolve Script", zLoad.sBestiaryHandler)
AM_SetPropertyJournal("Profile Resolve Script",  zLoad.sProfileHandler)
AM_SetPropertyJournal("Quest Resolve Script",    zLoad.sQuestHandler)
AM_SetPropertyJournal("Location Resolve Script", zLoad.sLocationHandler)
AM_SetPropertyJournal("Paragon Resolve Script",  zLoad.sParagonHandler)
AM_SetPropertyJournal("Combat Resolve Script",   zLoad.sCombatHandler)

--Autosave Icon
AL_SetProperty("Autosave Icon Path", zLoad.sAutosaveIconPath)

-- |[ ===================================== Catalyst Loader ==================================== ]|
-- |[Catalysts]|
--Call an internal function that will track catalysts, both in the local area and global catalysts found.
AdInv_SetProperty("Resolve Catalysts By String", zLoad.sCatalystName)

--Must be tracked before we call the form setter, since that's where stats are computed.
Debug_Print("Setting catalysts.\n")
local iHealthCatalyst     = AdInv_GetProperty("Catalyst Count", gciCatalyst_Health)
local iAttackCatalyst     = AdInv_GetProperty("Catalyst Count", gciCatalyst_Attack)
local iInitiativeCatalyst = AdInv_GetProperty("Catalyst Count", gciCatalyst_Initiative)
local iEvadeCatalyst      = AdInv_GetProperty("Catalyst Count", gciCatalyst_Evade)
local iAccuracyCatalyst   = AdInv_GetProperty("Catalyst Count", gciCatalyst_Accuracy)
local iSkillCatalyst      = AdInv_GetProperty("Catalyst Count", gciCatalyst_Skill)

--Set extra slots from skill catalysts.
local iSlotCount = math.floor(iSkillCatalyst / gciCatalyst_Skill_Needed)
AdvCombat_SetProperty("Set Skill Catalyst Slots", iSlotCount)

--Mirror the catalyst counts into the DataLibrary.
VM_SetVar("Root/Variables/Global/Catalysts/iHealth",     "N", iHealthCatalyst)
VM_SetVar("Root/Variables/Global/Catalysts/iAttack",     "N", iAttackCatalyst)
VM_SetVar("Root/Variables/Global/Catalysts/iInitiative", "N", iInitiativeCatalyst)
VM_SetVar("Root/Variables/Global/Catalysts/iEvade",      "N", iEvadeCatalyst)
VM_SetVar("Root/Variables/Global/Catalysts/iAccuracy",   "N", iAccuracyCatalyst)
VM_SetVar("Root/Variables/Global/Catalysts/iSkill",      "N", iSkillCatalyst)

-- |[ ====================================== Party Setup ======================================= ]|
-- |[Boot]|
--Clear the active party.
Debug_Print("Booting party members.\n")
AdvCombat_SetProperty("Clear Party")

-- |[XP Table]|
--Build the XP table. It may change between chapters.
if(zLoad.sXPTablePath ~= "Null") then LM_ExecuteScript(zLoad.sXPTablePath) end

-- |[Load Party]|
--Debug
Debug_PushPrint(bShowDetailedPartyMemberInfo, "Running party construction.\n")
local bSuppressErrorsOld = gbSuppressNoListErrors
gbSuppressNoListErrors = true

-- |[Kiss Izuna]|
--At a certain point in the story, Sanya's Agontea ability "Kiss Izuna" is unlocked. This has to be called before
-- the program parses the abilities.
local iCompletedHunt = VM_GetVar("Root/Variables/Chapter2/Northwoods/iCompletedHunt", "N")
if(iCompletedHunt == 1.0) then
    AdvCombat_SetProperty("Push Party Member", "Sanya")
    
        --Register.
        AdvCombatEntity_SetProperty("Register Ability To Job", "Agontea|Kiss Izuna", "Agontea")
        
        --Move it up 3 slots.
        AdvCombatEntity_SetProperty("Push Job S", "Agontea")
            AdvCombatJob_SetProperty("Move Ability Up One Slot", "Agontea|Kiss Izuna")
            AdvCombatJob_SetProperty("Move Ability Up One Slot", "Agontea|Kiss Izuna")
            AdvCombatJob_SetProperty("Move Ability Up One Slot", "Agontea|Kiss Izuna")
        DL_PopActiveObject()
    DL_PopActiveObject()
    
end

--Load all party members who have a roster entry, including those not in the party.
local iRosterSize = VM_GetVar("Root/Saving/Combat/Base/iRosterSize", "N")
for i = 0, iRosterSize-1, 1 do
    
    --Get the character.
    Debug_Print(" Character: " .. i .. "\n")
    local sCharName = VM_GetVar("Root/Saving/Combat/Party/sCharacter"..i.."Name",  "S")
    Debug_Print("  Name: " .. sCharName .. "\n")

    --If the character exists...
    if(AdvCombat_GetProperty("Does Party Member Exist", sCharName) == true) then

        --Get variables.
        Debug_Print("  Exists.\n")
        local fHPPercent  = VM_GetVar("Root/Saving/Combat/Party/fCharacter"..i.."HpPct", "N")
        local iExperience = VM_GetVar("Root/Saving/Combat/Party/iCharacter"..i.."Exp",   "N")
        local iGlobalJP   = VM_GetVar("Root/Saving/Combat/Party/iCharacter"..i.."JP",    "N")
        local sCurrentJob = VM_GetVar("Root/Saving/Combat/Party/sCharacter"..i.."JobCur","S")
        local iTotalJobs  = VM_GetVar("Root/Saving/Combat/Party/iCharacter"..i.."JobsTotal", "N")

        --Push the entity.
        Debug_Print("  Pushing.\n")
        AdvCombat_SetProperty("Push Party Member", sCharName)
        
            --Base.
            Debug_Print("  Setting base statistics.\n")
            AdvCombatEntity_SetProperty("Current Exp", iExperience)
            AdvCombatEntity_SetProperty("Current JP", iGlobalJP)
            AdvCombatEntity_SetProperty("Active Job", sCurrentJob)
            AdvCombatEntity_SetProperty("Health Percent", fHPPercent)
            
            --Drop all equipment.
            Debug_Print("  Dropping previous equipment.\n")
            AdvCombatEntity_SetProperty("Unequip Slot", "All")
            
            -- |[Jobs]|
            --Jobs.
            Debug_Print("  Beginning job iteration.\n")
            for p = 0, iTotalJobs-1, 1 do
                
                --Name of the job.
                local sJobName = VM_GetVar("Root/Saving/Combat/Party/sCharacter"..i.."Job"..p.."Name", "S")
                Debug_Print("  Job " .. p .. " is named " .. sJobName .. ".\n")
                AdvCombatEntity_SetProperty("Push Job S", sJobName)
                
                    --Base.
                    Debug_Print("   Setting job baseline.\n")
                    local iAvailableJP = VM_GetVar("Root/Saving/Combat/Party/iCharacter"..i.."Job"..p.."JP",        "N")
                    local iAbilities   = VM_GetVar("Root/Saving/Combat/Party/iCharacter"..i.."Job"..p.."Abilities", "N")
                    local sProfileName = VM_GetVar("Root/Saving/Combat/Party/iCharacter"..i.."Job"..p.."Profile",   "S")
        
                    --Set.
                    AdvCombatJob_SetProperty("JP Available", iAvailableJP)
                    AdvCombatJob_SetProperty("Associated Profile Name", sProfileName)
                    
                    --For each ability...
                    Debug_Print("   Iterating " .. iAbilities .. " abilities.\n")
                    for o = 0, iAbilities, 1 do
                        
                        --Get variables.
                        local sAbilityName = VM_GetVar("Root/Saving/Combat/Party/iCharacter"..i.."Job"..p.."Ability"..o.."Name", "S")
                        local iIsUnlocked  = VM_GetVar("Root/Saving/Combat/Party/iCharacter"..i.."Job"..p.."Ability"..o.."Unlocked", "N")
                        Debug_Print("    Ability " .. o .. " is " .. sAbilityName .. ".\n")
                        
                        --Switch to boolean.
                        local bIsUnlocked = false
                        if(iIsUnlocked == 1.0) then bIsUnlocked = true end
                        
                        --Set.
                        AdvCombatJob_SetProperty("Ability Unlocked", sAbilityName, bIsUnlocked)
                        Debug_Print("    Done.\n")
                        
                    end
                DL_PopActiveObject()
                Debug_Print("  Done.\n")
            end
            Debug_Print("  Done job iteration.\n")
    
            -- |[Abilities]|
            --Abilities. Store abilities in the optional slots. First, block A:
            Debug_Print("  Setting abilities in blocks.\n")
            for x = gciAbility_Saveblock_X1, gciAbility_Saveblock_X2, 1 do
                for y = gciAbility_Saveblock_Y1, gciAbility_Saveblock_Y2, 1 do
                    local sAbilityName = VM_GetVar("Root/Saving/Combat/Party/iCharacter"..i.."Ability"..x..y.."Name", "S")
                    AdvCombatEntity_SetProperty("Set Ability Slot", x, y, sAbilityName)
                end
            end
            Debug_Print("  Done Setting abilities in blocks.\n")
            
            -- |[Profiles]|
            --Clear all profiles.
            AdvCombatEntity_SetProperty("Wipe Profiles")
            
            --Iterate across the stored profiles.
            local iTotalProfiles = VM_GetVar("Root/Saving/Combat/Party/iCharacter"..i.."ProfilesTotal", "N")
            for p = 0, iTotalProfiles-1, 1 do
                
                --Name of the profile. If the profile is "Default" the function call won't do anything.
                local sNameOfProfile = VM_GetVar("Root/Saving/Combat/Party/iCharacter"..i.."Profile"..p.."Name", "S")
                AdvCombatEntity_SetProperty("Create Profile", sNameOfProfile)
                
                --Iterate across the slots, store all the names.
                for x = 0, gciAbility_Memorized_Block_Wid-1, 1 do
                    for y = 0, gciAbility_Memorized_Block_Hei-1, 1 do
                        local sNameOfAbility = VM_GetVar("Root/Saving/Combat/Party/iCharacter"..i.."Profile"..p.."Ability"..x.."by"..y, "S")
                        AdvCombatEntity_SetProperty("Set Profile Ability", sNameOfProfile, x, y, sNameOfAbility)
                    end
                end
            end
            
            -- |[Rebuild]|
            --Recheck all jobs against their profiles. Sets any missing to "Default".
            AdvCombatEntity_SetProperty("Recheck Jobs Against Profiles")
    
        DL_PopActiveObject()
        Debug_Print("  Done with party member.\n")

    --Error.
    else
        io.write("Error, party member " .. sCharName .. " does not exist.\n")
    end
    Debug_Print(" Completed.\n")
end

--Debug.
Debug_PopPrint("Done running party members.\n")
gbSuppressNoListErrors = bSuppressErrorsOld

-- |[Reconstruction]|
--Place the characters who are in the active party in the same slots.
Debug_Print("Beginning party reconstruction.\n")
for i = 0, gciCombat_MaxActivePartySize-1, 1 do
    
    --Get the named entity.
    local sCharacterName = VM_GetVar("Root/Saving/Combat/Base/sActiveChar"..i, "S")
    Debug_Print(" Slot " .. i .. " is " .. sCharacterName .. ".\n")
    
    --Name is "Null", empty slot.
    if(sCharacterName == "Null") then
        AdvCombat_SetProperty("Party Slot", i, "Null")
    
    --Not "Null", place the character.
    else
        AdvCombat_SetProperty("Party Slot", i, sCharacterName)
    end
end
Debug_Print("Party reconstructed.\n")
    
-- |[Followers]|
--Dynamically tracks who is following the main character. This will add any characters who were not
-- in the combat lineup. Characters who already were will get their field sprites created.
Debug_Print("Placing followers.\n")
gsPartyLeaderName = VM_GetVar("Root/Saving/Followers/Base/sLeader", "S")

--Set the leader voice to match.
WD_SetProperty("Set Leader Voice", gsPartyLeaderName)

--If this is chapter 0, wipe the party leader. This can be caused by an autosave not having reset
-- the party lead before ending the chapter.
if(sCurrentLevel == "Nowhere") then
    gsPartyLeaderName = "Null"
end

--Flag. Don't run the costume handler yet.
gbDontRunCostumeHandler = true

--Debug.
Debug_Print(" Party leader: " .. gsPartyLeaderName .. "\n")

--Create the party leader and mark them.
fnAddPartyMember(gsPartyLeaderName, true)
EM_PushEntity(gsPartyLeaderName)
    giPartyLeaderID = RO_GetID()
DL_PopActiveObject()

--For each follower, get their name.
local iFollowersToRead = VM_GetVar("Root/Saving/Followers/Base/iFollowers", "N")
for i = 0, iFollowersToRead-1, 1 do
    
    --Get variables.
    local sFollowerName   = VM_GetVar("Root/Saving/Followers/Base/sFollower"..i.."Name", "S")
    local sFollowVariable = VM_GetVar("Root/Saving/Followers/Base/sFollower"..i.."Variable", "S")
    
    --Run a function to create them. If this is "Nowhere", don't do this.
    if(sCurrentLevel ~= "Nowhere") then
        Debug_Print(" Follower " .. i .. ": " .. sFollowerName .. "\n")
        fnAddPartyMember(sFollowerName)
    end
end

--Unset this flag.
gbDontRunCostumeHandler = false

-- |[ =================================== Jobs on Skills UI ==================================== ]|
--Give the characters all the jobs they have unlocked. This is chapter-independent, as it affects the roster.
Debug_Print("Placing jobs on skills UI.\n")

-- |[Function]|
--Adder function. Places the job in an array for auto-processing.
local zaArray = {}
local fnPlaceInArray = function(sCharName, sJobName)
    local iSlot = #zaArray + 1
    zaArray[iSlot] = {sCharName, sJobName}
end

-- |[Christine]|
--Christine's Job Variables
local iMet55InLowerRegulus           = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N")
local iChristine_HasJob_Commissar    = VM_GetVar("Root/Variables/Global/Christine/iHasDollForm", "N")
local iChristine_HasJob_RepairUnit   = VM_GetVar("Root/Variables/Global/Christine/iHasGolemForm", "N")
local iChristine_HasJob_Drone        = VM_GetVar("Root/Variables/Global/Christine/iHasLatexForm", "N")
local iChristine_HasJob_Starseer     = VM_GetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N")
local iChristine_HasJob_Handmaiden   = VM_GetVar("Root/Variables/Global/Christine/iHasEldritchForm", "N")
local iChristine_HasJob_LiveWire     = VM_GetVar("Root/Variables/Global/Christine/iHasElectrospriteForm", "N")
local iChristine_HasJob_BrassBrawler = VM_GetVar("Root/Variables/Global/Christine/iHasSteamDroidForm", "N")
local iChristine_HasJob_ThunderRat   = VM_GetVar("Root/Variables/Global/Christine/iHasRaijuForm", "N")
local iChristine_HasJob_Menialist    = VM_GetVar("Root/Variables/Global/Christine/iHasSecrebotForm", "N")

--Position in story dictates what Christine has on her skill list.
if(iChristine_HasJob_RepairUnit == 0.0) then
    fnPlaceInArray("Christine", "Teacher")

--Normal forms:
else

    --Lancer. Appears after meeting 55 in the basement of Regulus City.
    if(iMet55InLowerRegulus == 1.0) then fnPlaceInArray("Christine", "Lancer") end
    
    --Normal:
    if(iChristine_HasJob_Commissar    == 1.0) then fnPlaceInArray("Christine", "Commissar")     end
    if(iChristine_HasJob_RepairUnit   == 1.0) then fnPlaceInArray("Christine", "Repair Unit")   end
    if(iChristine_HasJob_Drone        == 1.0) then fnPlaceInArray("Christine", "Drone")         end
    if(iChristine_HasJob_Starseer     == 1.0) then fnPlaceInArray("Christine", "Starseer")      end
    if(iChristine_HasJob_Handmaiden   == 1.0) then fnPlaceInArray("Christine", "Handmaiden")    end
    if(iChristine_HasJob_LiveWire     == 1.0) then fnPlaceInArray("Christine", "Live Wire")     end
    if(iChristine_HasJob_BrassBrawler == 1.0) then fnPlaceInArray("Christine", "Brass Brawler") end
    if(iChristine_HasJob_ThunderRat   == 1.0) then fnPlaceInArray("Christine", "Thunder Rat")   end
    if(iChristine_HasJob_Menialist    == 1.0) then fnPlaceInArray("Christine", "Menialist")     end
end

-- |[Florentina]|
--Florentina's Job Variables
local iHasJob_Merchant       = VM_GetVar("Root/Variables/Global/Florentina/iHasJob_Merchant", "N")
local iHasJob_Mediator       = VM_GetVar("Root/Variables/Global/Florentina/iHasJob_Mediator", "N")
local iHasJob_TreasureHunter = VM_GetVar("Root/Variables/Global/Florentina/iHasJob_TreasureHunter", "N")
local iHasJob_Agarist        = VM_GetVar("Root/Variables/Global/Florentina/iHasJob_Agarist", "N")
local iHasJob_Lurker         = VM_GetVar("Root/Variables/Global/Florentina/iHasJob_Lurker", "N")

--Place.
fnPlaceInArray("Florentina", "Merchant")
if(iHasJob_Mediator       == 1.0) then fnPlaceInArray("Florentina", "Mediator")       end
if(iHasJob_TreasureHunter == 1.0) then fnPlaceInArray("Florentina", "TreasureHunter") end
if(iHasJob_Agarist        == 1.0) then fnPlaceInArray("Florentina", "Agarist")        end
if(iHasJob_Lurker         == 1.0) then fnPlaceInArray("Florentina", "Lurker")         end

-- |[Izuna]|
fnPlaceInArray("Izuna", "Miko")

-- |[JX-101]|
--Does not get additional jobs.
fnPlaceInArray("JX-101", "Tactician")

-- |[Mei]|
--Mei's Job Variables
local iHasAlrauneForm     = VM_GetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N")
local iHasBeeForm         = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")
local iHasGhostForm       = VM_GetVar("Root/Variables/Global/Mei/iHasGhostForm", "N")
local iHasGravemarkerForm = VM_GetVar("Root/Variables/Global/Mei/iHasGravemarkerForm", "N")
local iHasMannequinForm   = VM_GetVar("Root/Variables/Global/Mei/iHasMannequinForm", "N")
local iHasSlimeForm       = VM_GetVar("Root/Variables/Global/Mei/iHasSlimeForm", "N")
local iHasWisphagForm     = VM_GetVar("Root/Variables/Global/Mei/iHasWisphagForm", "N")
local iHasWerecatForm     = VM_GetVar("Root/Variables/Global/Mei/iHasWerecatForm", "N")

--Place.
fnPlaceInArray("Mei", "Fencer")
if(iHasAlrauneForm     == 1.0) then fnPlaceInArray("Mei", "Nightshade")  end
if(iHasBeeForm         == 1.0) then fnPlaceInArray("Mei", "Hive Scout")  end
if(iHasGhostForm       == 1.0) then fnPlaceInArray("Mei", "Maid")        end
if(iHasGravemarkerForm == 1.0) then fnPlaceInArray("Mei", "Petraian")    end
if(iHasMannequinForm   == 1.0) then fnPlaceInArray("Mei", "Stalker")     end
if(iHasSlimeForm       == 1.0) then fnPlaceInArray("Mei", "Smarty Sage") end
if(iHasWisphagForm     == 1.0) then fnPlaceInArray("Mei", "Soulherd")    end
if(iHasWerecatForm     == 1.0) then fnPlaceInArray("Mei", "Prowler")     end

-- |[Sanya]|
--Sanya's job variables.
local iHasSevaviForm  = VM_GetVar("Root/Variables/Global/Sanya/iHasSevaviForm", "N")
local iHasWerebatForm = VM_GetVar("Root/Variables/Global/Sanya/iHasWerebatForm", "N")
local iHasHarpyForm   = VM_GetVar("Root/Variables/Global/Sanya/iHasHarpyForm", "N")
local iHasBunnyForm   = VM_GetVar("Root/Variables/Global/Sanya/iHasBunnyForm", "N")

--Place.
fnPlaceInArray("Sanya", "Hunter")
if(iHasSevaviForm  == 1.0) then fnPlaceInArray("Sanya", "Agontea")    end
if(iHasWerebatForm == 1.0) then fnPlaceInArray("Sanya", "Skystriker") end
if(iHasBunnyForm   == 1.0) then fnPlaceInArray("Sanya", "Ranger") end
if(iHasHarpyForm   == 1.0) then fnPlaceInArray("Sanya", "Scofflaw") end

-- |[SX-399]|
--Does not get additional jobs (yet).
fnPlaceInArray("SX-399", "Shocktrooper")

-- |[Tiffany]|
--Variables.
local iTiffany_HasJob_Spearhead = VM_GetVar("Root/Variables/Global/Tiffany/iHasJob_Spearhead", "N")

--Place.
fnPlaceInArray("Tiffany", "Assault")
fnPlaceInArray("Tiffany", "Support")
fnPlaceInArray("Tiffany", "Subvert")
if(iTiffany_HasJob_Spearhead == 1.0) then fnPlaceInArray("Tiffany", "Spearhead") end

-- |[Zeke]|
fnPlaceInArray("Zeke", "Goat")
    
-- |[Unlock Jobs]|
--For prestige unlocking, create a list of unique character names.
local saUniqueCharacterNames = {}

--Run through the array and unlock them. Note that the characters are not necessarily in order,
-- so the push/pop happens every entry.
for i = 1, #zaArray, 1 do
    
    --Check if the character exists in the roster. If not created yet, do nothing.
    if(AdvCombat_GetProperty("Does Party Member Exist", zaArray[i][1]) == true) then
    
        --Check if the character is already on the unique name list.
        local bOnUniqueList = false
        for p = 1, #saUniqueCharacterNames, 1 do
            if(saUniqueCharacterNames[p] == zaArray[i][1]) then
                bOnUniqueList = true
                break
            end
        end
        
        --Add to the unique character name list. Not fast, not optimized, but automated!
        if(bOnUniqueList == false) then
            table.insert(saUniqueCharacterNames, zaArray[i][1])
        end
    
        --Push and add the job to the UI.
        AdvCombat_SetProperty("Push Party Member", zaArray[i][1])
            AdvCombatEntity_SetProperty("Push Job S", zaArray[i][2])
                AdvCombatJob_SetProperty("Appears on Skills UI", true)
            DL_PopActiveObject()
        DL_PopActiveObject()
    end
end

--For each character who has a prestige handler, run that.
for i = 1, #saUniqueCharacterNames, 1 do
    
    --Assemble path.
    local sPrestigePath = gsRoot .. "Combat/Party/" .. saUniqueCharacterNames[i] .. "/401 Mastery Bonus Handler.lua"
    
    --Make sure the file actually exists.
    if(FS_Exists(sPrestigePath) == true) then
        LM_ExecuteScript(sPrestigePath, "Prestige Unlock")
        LM_ExecuteScript(sPrestigePath, "Resolve Skill Visibility")
    end
end

-- |[Rebuild]|
--Suppress warnings when a costume list does not exist. Characters not in memory cannot unload their costumes
-- so it doesn't matter.
Debug_Print("Rebuilding character costumes.\n")
bSuppressErrorsOld = gbSuppressNoListErrors
gbSuppressNoListErrors = true

--Order all characters to rebuild their job visibility lists and costumes.
local saCharList = {"Christine", "Florentina", "Izuna", "JX-101", "Mei", "Sanya", "SX-399", "Tiffany", "Zeke", "Empress"}
for i = 1, #saCharList, 1 do
    
    --Check to make sure the character exists in this game version.
    if(AdvCombat_GetProperty("Does Party Member Exist", saCharList[i]) == true) then
        
        --Rebuild the jobs UI to hide all unused jobs.
        AdvCombat_SetProperty("Push Party Member", saCharList[i])
            AdvCombatEntity_SetProperty("Rebuild Job Skills UI")
        DL_PopActiveObject()
        
        --Run the costume handler for this character.
        LM_ExecuteScript(gsCharacterAutoresolve, saCharList[i])
    end
end

-- |[Other Costumes]|
--Run costume handlers for other characters.
saCharList = {"Sophie", "Rubberbee", "Rubberraune"}
for i = 1, #saCharList, 1 do
    LM_ExecuteScript(gsCharacterAutoresolve, saCharList[i])
end

-- |[Modded Characters]|
--Ask the mods to run their costume resolvers.
fnExecModScript("Load Handler/Character Costumes.lua")

--Reset flag.
gbSuppressNoListErrors = bSuppressErrorsOld
    
-- |[ =================================== Inventory Handling =================================== ]|
-- |[Dump All Items]|
--Characters may have dumped their starting equipment into the inventory. Clear those off here.
Debug_Print("Clearing inventory.\n")
AdInv_SetProperty("Clear")

-- |[Catalysts]|
--The clear action dumps the inventory, which includes the catalysts. We already have these as local
-- variables so just reset them here.
AdInv_SetProperty("Catalyst Count", gciCatalyst_Health,     iHealthCatalyst)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Attack,     iAttackCatalyst)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Initiative, iInitiativeCatalyst)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Evade,      iEvadeCatalyst)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Accuracy,   iAccuracyCatalyst)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Skill,      iSkillCatalyst)

-- |[Money]|
Debug_Print("Setting money.\n")
local iPlatina = VM_GetVar("Root/Saving/Inventory/Base/iPlatina", "N")
AdInv_SetProperty("Add Platina", iPlatina)

-- |[Adamantite]|
Debug_Print("Setting adamantite.\n")
for i = 0, 5, 1 do
    local iAdamantite = VM_GetVar("Root/Saving/Inventory/Base/iAdamantite"..i, "N")
    AdInv_SetProperty("Crafting Material", i, iAdamantite)
end

-- |[Inventory Items]|
--Items that are not equipped. This includes unused equipment, key items, or consumables.
Debug_Print("Creating inventory items.\n")
local iSocketCount = 0
local iTotalItems = VM_GetVar("Root/Saving/Inventory/Items/iItemsTotal", "N")
for i = 0, iTotalItems-1, 1 do
    
    --Variables.
    local sName         = VM_GetVar("Root/Saving/Inventory/Items/sItem" .. i .. "Name",         "S")
    local iQuantity     = VM_GetVar("Root/Saving/Inventory/Items/iItem" .. i .. "Quantity",     "N")
    local sInstructions = VM_GetVar("Root/Saving/Inventory/Items/sItem" .. i .. "Instructions", "S")
    local iIsGem        = VM_GetVar("Root/Saving/Inventory/Items/iItem" .. i .. "IsGem",        "N")
    Debug_Print(" Creating item " .. sName .. "\n")
    
    --If the item is a gem, we may need to merge it with subgems. It may also have a different
    -- name than the stored one. Gems never have quantities over 1.
    if(iIsGem == 1.0) then
        Debug_Print("  Is Gem.\n")
        local sOrigGemName = VM_GetVar("Root/Saving/Inventory/Items/sItem" .. i .. "GemName", "S")
        LM_ExecuteScript(gsItemListing, sOrigGemName)
        
        --Mark as the master gem. Future merge actions will merge with the master gem.
        AdInv_SetProperty("Mark Last Item As Master Gem")
    
        --Check the subgems, created by merging gems together.
        for p = 0, gciSubgemMax-1, 1 do
            local sSubgemName = VM_GetVar("Root/Saving/Inventory/Items/sItem" .. i .. "Subgem"..p.."Name", "S")
            if(sSubgemName ~= "Null" and sSubgemName ~= "NULL") then
                LM_ExecuteScript(gsItemListing, sSubgemName)
                AdInv_SetProperty("Merge Last Gem With Master")
            end
        end
    
        --If ordered to socket, do so here.
        if(iSocketCount > 0) then
            AdInv_SetProperty("Socket Last Item In Socket Item")
            iSocketCount = iSocketCount - 1
            if(iSocketCount == 0) then
                AdInv_SetProperty("Clear Socket Item")
            end
        end
    
        --Clean up the master gem pointer.
        AdInv_SetProperty("Clear Master Gem")
    
    --Item is not a gem, so construct it normally. As of Starlight v3.1 unequipped inventory items can store gems. 
    -- This does not occur in RoP but does in Carnation.
    else
        Debug_Print("  Normal item. Quantity: " .. iQuantity .. "\n")
        
        --Create the item.
        LM_ExecuteScript(gsItemListing, sName)
        
        --If the quantity value is above one, manually specify the quantity into the inventory.
        if(iQuantity > 1) then
            AdInv_SetProperty("Mark Last Item Quantity", iQuantity)
        end
    
        --Always mark the item as the last equipped item. This is in case it actually is equipment.
        Debug_Print("  Marked item as equipment.\n")
        AdInv_SetProperty("Mark Last Item As Equipment")
        
        --Mark as the master gem. Future merge actions will merge with the master gem.
        AdInv_SetProperty("Mark Last Item As Master Gem")
    
        --Check the subgems, created by merging gems together.
        for p = 0, gciSubgemMax-1, 1 do
            local sSubgemName = VM_GetVar("Root/Saving/Inventory/Items/sItem" .. i .. "Subgem"..p.."Name", "S")
            if(sSubgemName ~= "Null" and sSubgemName ~= "NULL" and sSubgemName ~= 0.0) then
                LM_ExecuteScript(gsItemListing, sSubgemName)
                AdInv_SetProperty("Merge Last Gem With Master")
            end
        end
    
        --If ordered to socket, do so here.
        if(iSocketCount > 0) then
            AdInv_SetProperty("Socket Last Item In Socket Item")
            iSocketCount = iSocketCount - 1
            if(iSocketCount == 0) then
                AdInv_SetProperty("Clear Socket Item")
            end
        end
    
        --Clean up the master gem pointer.
        AdInv_SetProperty("Clear Master Gem")
    end
    
    --Debug.
    Debug_Print("  Item created.\n")
    
    --If there is an instruction, handle that here.
    local saStrings = fnSubdivide(sInstructions, "|")
    for p = 1, #saStrings, 1 do
        
        --Further subdivide.
        local saSubstrings = fnSubdivide(saStrings[p], ":")
        local iSubstringsTotal = #saSubstrings
        
        --If the 1st substring is "Equip", we're equipping this item.
        if(saSubstrings[1] == "Equip" and iSubstringsTotal >= 3) then
            AdvCombat_SetProperty("Push Party Member", saSubstrings[2]) 
                AdvCombatEntity_SetProperty("Equip Marked Item To Slot", saSubstrings[3])
            DL_PopActiveObject()
            AdInv_SetProperty("Clear Equipment Marker")
        
        --Socket instruction. Increment the socketing stack by 1.
        elseif(saSubstrings[1] == "SocketNext") then
            AdInv_SetProperty("Mark Last Item As Socket Item")
            iSocketCount = iSocketCount + 1
        end
    end
    AdInv_SetProperty("Clear Equipment Marker")
    Debug_Print("  Finished instructions.\n")
end

--Clean.
AdInv_SetProperty("Clear Equipment Marker")

-- |[Doctor Bag]|
--Load the charges and update the inventory.
Debug_Print("Setting Doctor Bag.\n")
LM_ExecuteScript(gsComputeDoctorBagTotalPath)
local iDoctorBagCharges    = VM_GetVar("Root/Variables/System/Special/iDoctorBagCharges", "N")
local iDoctorBagChargesMax = VM_GetVar("Root/Variables/System/Special/iDoctorBagChargesMax", "N")
AdInv_SetProperty("Doctor Bag Charges Max", iDoctorBagChargesMax)
AdInv_SetProperty("Doctor Bag Charges", iDoctorBagCharges)

--Charge Rate/Potency Upgrades
local fDoctorBagChargeRateUpgrade = VM_GetVar("Root/Variables/CrossChapter/DoctorBag/fDoctorBagChargeRateUpgrade", "N")
local fDoctorBagPotencyUpgrade    = VM_GetVar("Root/Variables/CrossChapter/DoctorBag/fDoctorBagPotencyUpgrade", "N")
AdInv_SetProperty("Doctor Bag Charge Factor", 1.0 + fDoctorBagChargeRateUpgrade)
AdInv_SetProperty("Doctor Bag Potency",       1.0 + fDoctorBagPotencyUpgrade)
    
-- |[ ===================================== Topic Variables ==================================== ]|
--Refresh topic information for each listed character.
Debug_Print("Setting topics.\n")
local iTotalTopics = VM_GetVar("Root/Saving/Topics/Base/iTotalTopics", "N")

--For each topic:
for i = 0, iTotalTopics - 1, 1 do
    
    --Variables.
    local sTopicName  = VM_GetVar("Root/Saving/Topics/Base/sTopic"..i.."Name",  "S")
    local iTopicLevel = VM_GetVar("Root/Saving/Topics/Base/sTopic"..i.."Level", "N")
    WD_SetProperty("Unlock Topic", sTopicName, iTopicLevel)

    --For each NPC...
    local iTopicNPCs  = VM_GetVar("Root/Saving/Topics/Base/sTopic"..i.."NPCs", "N")
    for p = 0, iTopicNPCs-1, 1 do
    
        --Variables.
        local sSubNPCName  = VM_GetVar("Root/Saving/Topics/Base/sTopic"..i.."Sub"..p.."Name",  "S")
        local iSubNPCLevel = VM_GetVar("Root/Saving/Topics/Base/iTopic"..i.."Sub"..p.."Level", "N")
    
        --Set.
        WD_SetProperty("Topic NPC Level", sTopicName, sSubNPCName, iSubNPCLevel)
    
    end
end
    
-- |[Inventory Sort]|
--Order the inventory to sort using whatever flag was saved.
local iInventorySort = VM_GetVar("Root/Saving/Inventory/Base/iSortFlag", "N")
AdInv_SetProperty("Set Internal Sort", iInventorySort)
    
-- |[ ==================================== Field Abilities ===================================== ]|
-- |[Field Abilities]|
Debug_Print("Creating field abilities.\n")

--Adder function, used to create a list of abilities.
local saAbilityList = {}
local fnAddAbility = function(sName, sPath)
    local i = #saAbilityList + 1
    saAbilityList[i] = {}
    saAbilityList[i].sName = sName
    saAbilityList[i].sPath = sPath
end

--Re-run field ability scripts. For safety, we need to purge them to make sure no field abilities
-- from other chapters sneak in.
DL_Purge("Root/Special/Combat/FieldAbilities/", false)
DL_AddPath("Root/Special/Combat/FieldAbilities/")
for i = 1, #zLoad.saFieldAbilityPaths, 1 do
    LM_ExecuteScript(zLoad.saFieldAbilityPaths[i], gciFieldAbility_Create)
end

--Create a list of all possible field abilities. If an ability is not created due to chapter-load,
-- it will not be valid for placement on the field ability bar.
fnAddAbility("Deadly Jump",     "Root/Special/Combat/FieldAbilities/DeadlyJump")
fnAddAbility("Wraith Form",     "Root/Special/Combat/FieldAbilities/WraithForm")
fnAddAbility("Scout Sight",     "Root/Special/Combat/FieldAbilities/ScoutSight")
fnAddAbility("Pick Lock",       "Root/Special/Combat/FieldAbilities/PickLock")
fnAddAbility("Reset",           "Root/Special/Combat/FieldAbilities/Reset")
fnAddAbility("Use Rifle",       "Root/Special/Combat/FieldAbilities/Use Rifle")
fnAddAbility("Extra Extra",     "Root/Special/Combat/FieldAbilities/Extra Extra")
fnAddAbility("Change Pamphlet", "Root/Special/Combat/FieldAbilities/Change Pamphlet")
fnAddAbility("Bend Space",      "Root/Special/Combat/FieldAbilities/Bend Space")
fnAddAbility("Spot Weld",       "Root/Special/Combat/FieldAbilities/Spot Weld")
fnAddAbility("Ball Lightning",  "Root/Special/Combat/FieldAbilities/Ball Lightning")

--Store the names of the field abilities in these slots.
for i = 0, gciFieldAbility_Slots-1, 1 do
    AdvCombat_SetProperty("Set Field Ability", i, "NULL")
    local sFieldAbilityName = VM_GetVar("Root/Saving/FieldAbilities/All/" .. i, "S")
    
    --Iterate across the ability list.
    for p = 1, #saAbilityList, 1 do
        if(sFieldAbilityName == saAbilityList[p].sName) then
            AdvCombat_SetProperty("Set Field Ability", i, saAbilityList[p].sPath)
            break
        end
    end
end
    
-- |[ =================================== Finalize All Stats =================================== ]|
--For each party member, once all equipment, gems, and abilities are set, refresh their UI and set their HP.
Debug_Print("Finalizing stats.\n")
for i = 0, iRosterSize-1, 1 do
    
    --Get the character.
    local sCharName = VM_GetVar("Root/Saving/Combat/Party/sCharacter"..i.."Name",  "S")

    --Double check the character exists:
    if(AdvCombat_GetProperty("Does Party Member Exist", sCharName) == true) then

        --Get the saved HP percent.
        local fHPPercent  = VM_GetVar("Root/Saving/Combat/Party/fCharacter"..i.."HpPct", "N")
        
        --Order the character to run all their UI applications for abilities.
        AdvCombat_SetProperty("Push Party Member", sCharName)
            AdvCombatEntity_SetProperty("Refresh Stats for UI")
            AdvCombatEntity_SetProperty("Health Percent", fHPPercent)
        DL_PopActiveObject()
    end
end

-- |[ ====================================== Map Handling ====================================== ]|
--Place the assembled party in the map they saved at. Also issue a load instruction to give them
-- their sprites as those have now loaded.
Debug_Print("Map post-process.\n")
AL_SetProperty("Reset Actor Graphics")

--Set this as the last save point, in case the player doesn't touch the save point. The variable
-- should be in memory. On autosaves, it may not be the current level.
local sLastSavePoint = VM_GetVar("Root/Variables/Global/Autosave/sLastSavePoint", "S")
AL_SetProperty("Last Save Point", sLastSavePoint)

-- |[ ======================================== Options ========================================= ]|
--Debug
Debug_Print("Setting system options.\n")

--Sound Settings.
local iHasSoundSettings = VM_GetVar("Root/Variables/System/Special/iHasSoundSettings", "N")
local bUseSaveAudio     = OM_GetOption("Save Audio Levels In Savefile")
if(iHasSoundSettings ~= 0.0 and bUseSaveAudio == true) then
    
    --Basic volumes.
    local fMusicVolume = VM_GetVar("Root/Variables/System/Special/fMusicVolume", "N")
    local fSoundVolume = VM_GetVar("Root/Variables/System/Special/fSoundVolume", "N")
	AudioManager_SetProperty("Music Volume", fMusicVolume)
    AudioManager_SetProperty("Sound Volume", fSoundVolume)

    --Abstract Volumes. These can be used for specific volumes, often by mods.
    for i = 0, 9, 1 do
        local fAbsVol = VM_GetVar("Root/Variables/System/Special/fAbsVol"..i, "N")
        AudioManager_SetProperty("Abstract Volume", i, fAbsVol)
    end
else
	--AudioManager_SetProperty("Music Volume", .50)
    --AudioManager_SetProperty("Sound Volume", .75)
end

--Tourist Mode.
local iIsTouristMode = VM_GetVar("Root/Variables/System/Special/iIsTouristMode", "N")
if(iIsTouristMode == 1.0) then AdvCombat_SetProperty("Tourist Mode", true) end

--Ambient Light Boost
local fAmbientLightBoost = VM_GetVar("Root/Variables/System/Special/fAmbientLightBoost", "N")
AL_SetProperty("Ambient Light Boost", fAmbientLightBoost)

--Combat Options.
local iAutoUseDoctorBag = VM_GetVar("Root/Variables/System/Special/iAutoUseDoctorBag", "N")
local iMemoryCursor     = VM_GetVar("Root/Variables/System/Special/iMemoryCursor", "N")
if(iAutoUseDoctorBag == 0.0) then 
    AdvCombat_SetProperty("Auto Doctor Bag", false) 
else
    AdvCombat_SetProperty("Auto Doctor Bag", true) 
end
if(iMemoryCursor == 0.0) then
    AdvCombat_SetProperty("Memory Cursor", false)
else
    AdvCombat_SetProperty("Memory Cursor", true)
end
    
--Dialogue options.
local iAutoHastenDialogue = VM_GetVar("Root/Variables/System/Special/iAutoHastenDialogue", "N")
if(iAutoHastenDialogue == 1.0) then WD_SetProperty("Set AutoHasten", true) end

--Electrosprite quest is unlocked on the main menu.
local iFinished198 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFinished198", "N")
if(iFinished198 == 1.0) then
    OM_SetOption("Unlock Electrosprites", "1.000000")
    OM_WriteConfigFiles()
end


-- |[ ================================ Chapter 1 Special Events ================================ ]|
if(iCurrentChapter == 1.0) then
    LM_ExecuteScript(fnResolvePath() .. "100 Chapter 1 Handler.lua")
end

-- |[ ================================ Chapter 2 Special Events ================================ ]|
--If and only if we are loading a chapter 2 save, we need to run all the hunting creation handlers.
-- These are not part of the chapter initializers because we don't need all these variables when
-- in other chapters wasting RAM.
if(iCurrentChapter == 2.0) then
    LM_ExecuteScript(gsHuntingPath .. "000 Initialize.lua")
end

-- |[ ===================================== Map Reposition ===================================== ]|
--Debug
Debug_Print("Repositioning on map.\n")

--Check if this is an autosave.
local iIsAutosave = VM_GetVar("Root/Variables/Global/Autosave/iIsAutosave", "N")
local iPlayerX    = VM_GetVar("Root/Variables/Global/Autosave/iPlayerX", "N")
local iPlayerY    = VM_GetVar("Root/Variables/Global/Autosave/iPlayerY", "N")
local iPlayerZ    = VM_GetVar("Root/Variables/Global/Autosave/iPlayerZ", "N")

--Reposition to the last save point if this is not an autosave.
if(iIsAutosave == 0.0) then
    AL_BeginTransitionTo("LASTSAVEINSTANT", "Null")

--If this is an autosave, run the transition, and order the position based on the autosave coordinates.
else

    --Get the position in terms of tiles.
    local iXPos = iPlayerX/16
    local iYPos = iPlayerY/16
    
    --Check if the position is clipped. If so, put the player at the player start.
    local iClipAt = AL_GetProperty("Collision", math.floor(iXPos), math.floor(iYPos), 0)
    if(iClipAt > 0) then
        iXPos, iYPos = AL_GetProperty("Player Start")
    end
    
    --Position leader.
    if(EM_Exists(gsPartyLeaderName)) then
        EM_PushEntity(gsPartyLeaderName)
            TA_SetProperty("Position", iXPos, iYPos)
            TA_SetProperty("Depth", iPlayerZ)
        DL_PopActiveObject()
    end
    
    --Unset the autosave flag.
    VM_SetVar("Root/Variables/Global/Autosave/iIsAutosave", "N", 0)

end

--Order the party to fold.
AL_SetProperty("Fold Party")
AL_SetProperty("Run Enemy Spawner")

--Run enemy movement emulation. This normally runs on map transition, but does not run the first time
-- a level is loaded.
AL_SetProperty("Run Enemy Move Emulation")

-- |[Actor Images Reresolve]|
--As above, any images that the TilemapActors were missing will get resolved here. This is set up in ZLaunch.lua.
TA_SetProperty("Reresolve All Images")
TA_SetProperty("Store Image Paths", false)

-- |[Special Flag]|
--This flag is 1.0 when loading the game. Used for certain trigger handlers.
VM_SetVar("Root/Variables/Global/LoadSetting/iIsLoaded", "N", 1.0)

-- |[Nowhere Code]|
--If the starting map is "Nowhere" then this is chapter-select. In that case, we take control
-- of Septima and zero out the combat party.
if(sCurrentLevel == "Nowhere") then
	EM_PushEntity("Septima")
		giPartyLeaderID = RO_GetID()
	DL_PopActiveObject()
	
	--Order the player to take control of the party leader.
	AL_SetProperty("Player Actor ID", giPartyLeaderID)
    
    --Clear combat party.
    AdvCombat_SetProperty("Clear Party")
    
    --Set everyone to their default forms.
    gbSuppressNoListErrors = true
    VM_SetVar("Root/Variables/Global/Mei/sForm", "S", "Human")
    VM_SetVar("Root/Variables/Global/Sanya/sForm", "S", "Human")
    VM_SetVar("Root/Variables/Global/Christine/sForm", "S", "Human")
    LM_ExecuteScript(gsRoot .. "CostumeHandlers/Mei/Human_Normal.lua")
    LM_ExecuteScript(gsRoot .. "CostumeHandlers/Sanya/Human_Normal.lua")
    LM_ExecuteScript(gsRoot .. "CostumeHandlers/Christine/Human_Normal.lua")
    gbSuppressNoListErrors = true

-- |[All Other Maps]|
else
end
    
-- |[Player Light]|
--Activate the lantern if the player has it. If the variable is "Null", don't activate.
if(zLoad.sLightVarPath ~= nil and zLoad.sLightVarPath ~= "Null") then
    
    --This value must be nonzero.
    local iHasLight = VM_GetVar(zLoad.sLightVarPath, "N")
    
    --If the variable name is "True" then always have the light.
    if(zLoad.sLightVarPath == "True") then iHasLight = 1 end
    
    --Light does not activate in the chapter select.
    if(iHasLight == 1.0 and sCurrentLevel ~= "Nowhere") then
        AL_SetProperty("Activate Player Light", 3600, 3600)
    end
end

-- |[Debug]|
Debug_PopPrint("Completed Load Handler.\n")

-- |[ ========================= Menu Close Handler ========================= ]|
--When loading, reset the menu close handler since the standard level caller wasn't reset yet. This is only
-- done for chapter 2.
if(iCurrentChapter == 1.0) then
    AL_SetProperty("Menu Close Script", gsRoot .. "Chapter 1/Menu Close Handler.lua")
elseif(iCurrentChapter == 2.0) then
    AL_SetProperty("Menu Close Script", gsRoot .. "Chapter 2/Menu Close Handler.lua")
end

--If a map object handled the last call:
local zHandlingMap = GUIMap:fnLocateMapContainingLookup(sCurrentLevel)
if(zHandlingMap ~= nil) then
    
    --Run the subroutines.
    zHandlingMap:fnHandleUnlock(sCurrentLevel)
    zHandlingMap:fnHandleMap(sCurrentLevel)
    
    --Clean other globals.
    gfnLastMapFunction = nil
    
    --Mark this object as the last one to handle the map call.
    gsLastGUIMap = zHandlingMap.sLocalName

--If there is a re-resolve function, call it.
elseif(gfnLastMapFunction ~= nil) then
    
    --Chapter 2:
    if(iCurrentChapter == 1.0 or iCurrentChapter == 2.0) then
        gfnLastMapFunction(sCurrentLevel)
    else
        gfnLastMapFunction(giLastPlayerX, giLastPlayerY, giLastRemapX, giLastRemapY)
    end

--Otherwise, resolve maps as normal.
else
    fnResolveMapLocation(sCurrentLevel)
end

-- |[ ============================ Hunting Data ============================ ]|
--If the hunting minigame exists in the savefile, reinit it here.
local iHasHuntData = VM_GetVar("Root/Saving/Hunting/System/iHasHuntData", "N")
if(iHasHuntData == 1.0) then
	Hunting:fnLoadHuntData()
end

-- |[ ========================== Zeke Skills Code ========================== ]|
--Boost Zeke's statistics based on how many times he has purchased these special skills, then recompute
-- their JP costs.

-- |[Variables]|
local iRepurchaseHealth       = VM_GetVar("Root/Variables/Global/Zeke/iRepurchaseHealth",       "N")
local iRepurchasePower        = VM_GetVar("Root/Variables/Global/Zeke/iRepurchasePower",        "N")
local iRepurchaseAccuracy     = VM_GetVar("Root/Variables/Global/Zeke/iRepurchaseAccuracy",     "N")
local iRepurchaseEvade        = VM_GetVar("Root/Variables/Global/Zeke/iRepurchaseEvade",        "N")
local iRepurchaseInitiative   = VM_GetVar("Root/Variables/Global/Zeke/iRepurchaseInitiative",   "N")
local iRepurchaseItemPower    = VM_GetVar("Root/Variables/Global/Zeke/iRepurchaseItemPower",    "N")
local iRepurchaseBleedDamage  = VM_GetVar("Root/Variables/Global/Zeke/iRepurchaseBleedDamage",  "N")
local iRepurchaseStrikeDamage = VM_GetVar("Root/Variables/Global/Zeke/iRepurchaseStrikeDamage", "N")

--Reset the total to match the number of sub-purchases.
local iRepurchaseTotal = iRepurchaseHealth + iRepurchasePower + iRepurchaseAccuracy + iRepurchaseEvade + iRepurchaseInitiative + iRepurchaseItemPower + iRepurchaseBleedDamage + iRepurchaseStrikeDamage
VM_SetVar("Root/Variables/Global/Zeke/iRepurchaseTotal", "N", iRepurchaseTotal)

--Debug.
if(false) then
    io.write("Zeke skillboosts.\n")
    io.write(" Tot: " .. iRepurchaseTotal .. "\n")
    io.write(" HP : " .. iRepurchaseHealth .. "\n")
    io.write(" Atk: " .. iRepurchasePower .. "\n")
    io.write(" Acc: " .. iRepurchaseAccuracy .. "\n")
    io.write(" Evd: " .. iRepurchaseEvade .. "\n")
    io.write(" Ini: " .. iRepurchaseInitiative .. "\n")
    io.write(" IP : " .. iRepurchaseItemPower .. "\n")
    io.write(" Bld: " .. iRepurchaseBleedDamage .. "\n")
    io.write(" Stk: " .. iRepurchaseStrikeDamage .. "\n")
end

-- |[Set Zeke's Stats]|
if(AdvCombat_GetProperty("Does Party Member Exist", "Zeke") == true) then
    AdvCombat_SetProperty("Push Party Member", "Zeke")

        -- |[Health]|
        --Track Zeke's current health percent and keep it updated when his HP goes up.
        local iCurHp = AdvCombatEntity_GetProperty("Health")
        local iMaxHp = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
        local fHpPct = iCurHp / iMaxHp
        
        -- |[Call Subscripts]|
        AdvCombatEntity_SetProperty("Push Ability S", "Goat Internal|Accuracy Boost")
            LM_ExecuteScript(gsRoot .. "Combat/Party/Zeke/Jobs/Goat/Abilities/Repur_Accuracy.lua", gciAbility_UIRecompute)
        DL_PopActiveObject()
        AdvCombatEntity_SetProperty("Push Ability S", "Goat Internal|Bleed Boost")
            LM_ExecuteScript(gsRoot .. "Combat/Party/Zeke/Jobs/Goat/Abilities/Repur_Bleed.lua",      gciAbility_UIRecompute)
        DL_PopActiveObject()
        AdvCombatEntity_SetProperty("Push Ability S", "Goat Internal|Evade Boost")
            LM_ExecuteScript(gsRoot .. "Combat/Party/Zeke/Jobs/Goat/Abilities/Repur_Evade.lua",      gciAbility_UIRecompute)
        DL_PopActiveObject()
        AdvCombatEntity_SetProperty("Push Ability S", "Goat Internal|Health Boost")
            LM_ExecuteScript(gsRoot .. "Combat/Party/Zeke/Jobs/Goat/Abilities/Repur_Health.lua", gciAbility_UIRecompute)
        DL_PopActiveObject()
        AdvCombatEntity_SetProperty("Push Ability S", "Goat Internal|Initiative Boost")
            LM_ExecuteScript(gsRoot .. "Combat/Party/Zeke/Jobs/Goat/Abilities/Repur_Initiative.lua", gciAbility_UIRecompute)
        DL_PopActiveObject()
        AdvCombatEntity_SetProperty("Push Ability S", "Goat Internal|Item Boost")
            LM_ExecuteScript(gsRoot .. "Combat/Party/Zeke/Jobs/Goat/Abilities/Repur_Item.lua",       gciAbility_UIRecompute)
        DL_PopActiveObject()
        AdvCombatEntity_SetProperty("Push Ability S", "Goat Internal|Power Boost")
            LM_ExecuteScript(gsRoot .. "Combat/Party/Zeke/Jobs/Goat/Abilities/Repur_Power.lua",      gciAbility_UIRecompute)
        DL_PopActiveObject()
        AdvCombatEntity_SetProperty("Push Ability S", "Goat Internal|Strike Boost")
            LM_ExecuteScript(gsRoot .. "Combat/Party/Zeke/Jobs/Goat/Abilities/Repur_Strike.lua",     gciAbility_UIRecompute)
        DL_PopActiveObject()
        
        --Set the percentage.
        AdvCombatEntity_SetProperty("Health Percent", fHpPct)
    DL_PopActiveObject()
    
    -- |[Recompute Costs]|
    --Subroutine does most of the work.
    fnRecomputeZekeCosts()
end

-- |[ ==================================== Music Post-Exec ===================================== ]|
--Because the music can resolve after the level's constructor has fired, we check if the music is
-- playing and, if not, re-play the music.
local sMusicName = AL_GetProperty("Music")
if(AudioManager_GetProperty("Is Music Playing") == false) then
    
    --List of layered tracks. They need to add "LAYER|" to the front of the name to run correctly.
    local bIsLayeredTrack = false
    local saLayeredTrackNames = {"CultistProfile", "EmbassyFunction", "Telecomm", "Manufactory", "Sleep"}
    
    --Check if it's layered:
    for i = 1, #saLayeredTrackNames, 1 do
        if(sMusicName == saLayeredTrackNames[i]) then
            bIsLayeredTrack = true
            AL_SetProperty("Music", "Null")
            AL_SetProperty("Music", "LAYER|" .. sMusicName)
            break
        end
    end

    --Clear the level music back to NULL, as it may still have the name of the previously playing
    -- music on it even though that pack failed to resolve.
    if(bIsLayeredTrack == false) then
        AL_SetProperty("Music", "Null")
        AL_SetProperty("Music", sMusicName)
    end
    
end

-- |[ ======================================= Debug Code ======================================= ]|
--Reset achievement debug flag.
gbAchievementDebug = bStoredAchievementDebug

-- |[Clean]|
zLoad = nil
