-- |[ =================================== Chapter 1 Handler ==================================== ]|
--If any non-standard code needs to be called for chapter 1 after loading a game, it is done here.
-- Note that this only calls if chapter 1 is the *active* chapter.

-- |[ ===== Mannequin Sequence ===== ]|
--Display name changes are not stored in savefiles, so if the player saves during the mannequin
-- quest sequence, this is called to make sure the character have the correct display name.
local iMannBeganSequence = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iBeganSequence", "N")
local iMannPolarisRunIn  = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iPolarisRunIn", "N")
if(iMannBeganSequence == 1.0 and iMannPolarisRunIn == 0.0) then
    AdvCombat_SetProperty("Push Party Member", "Mei")
        AdvCombatEntity_SetProperty("Display Name", "Mannequin")
    DL_PopActiveObject()
    AdvCombat_SetProperty("Push Party Member", "Florentina")
        AdvCombatEntity_SetProperty("Display Name", "Mannequin")
    DL_PopActiveObject()
end