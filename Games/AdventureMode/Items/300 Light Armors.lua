-- |[ =========================== Light Armor, Character Independent =========================== ]|
--Light armors can be used by pretty much everyone. They provide minimal protection, but +20 INI.
local bDebug = false

-- |[ ========================================= Charts ========================================= ]|
--For equipment which can be made as part of a chart, construct it here.
if(gzLightArmorChart == nil) then
    
    --Setup. All functions operate on this chart.
    gzActiveChart = {}
    
    --Make aliases for the functions. This shortens the chart for readability.
    local fnAdd            = fnAddItemToChartMarkdown
    local fnSetDescription = fnSetDescriptionOnChart

    -- |[Armors Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22    |  Stats Array
    fnAdd("Troubadour's Robe",                  45,          0,     false,     gciArmorCode, "ArmGenLight", "Prt|Flat|2 Ini|Flat|20")
    fnAdd("Traveller's Vest",                   67,          0,     false,     gciArmorCode, "ArmGenLight", "Prt|Flat|1 Ini|Flat|20 Atk|Flat|5")
    fnAdd("Leather Greatcoat",                  95,          2,     false,     gciArmorCode, "ArmGenLight", "Prt|Flat|3 Ini|Flat|20")
    fnAdd("Lined Miko Robes",                   50,          2,     false,     gciArmorCode, "ArmGenLight", "Prt|Flat|2 Ini|Flat|20 ResFrz|Flat|3 ResShk|Flat|3")

    fnAdd("Dispersion Cloak",                   75,          0,     false,     gciArmorCode, "ArmGenLight", "Prt|Flat|2 Ini|Flat|20")
    fnAdd("Adaptive Cloth Vest",               115,          1,     false,     gciArmorCode, "ArmGenLight", "Prt|Flat|3 Ini|Flat|20")
    fnAdd("Battle Skirt",                      115,          1,     false,     gciArmorCode, "ArmGenLight", "Prt|Flat|1 Ini|Flat|35")
    fnAdd("Hyperweave Chemise",                165,          1,     false,     gciArmorCode, "ArmGenLight", "Prt|Flat|2 Ini|Flat|20 Atk|Flat|5")
    fnAdd("Neon Tanktop",                      125,          1,     false,     gciArmorCode, "ArmGenLight", "Prt|Flat|1 Ini|Flat|20 Atk|Flat|10")

    -- |[Descriptions]|
    --Weapons
    fnSetDescription("Troubadour's Robe",   "Light, simple clothing worn by composers and artists the world over.")
    fnSetDescription("Traveller's Vest",    "A simple cloth vest used by many ordinary citizens.")
    fnSetDescription("Leather Greatcoat",   "A coat that stretches to the ground, but is made of light cloth and leather. More protective than most light\narmors.")
    fnSetDescription("Lined Miko Robes",    "Miko robes lined with leather. Surprisingly durable and comfortable, and insulates from freeze and shock\ndamage.")
    fnSetDescription("Dispersion Cloak",    "A light, partially transparent cloak that produces a weak deflection field from latent static charge. It provide\nlittle protection but is very natural to move in.")
    fnSetDescription("Adaptive Cloth Vest", "A steelweave vest made from quantum-destabilized fibers. The fibers go rigid when struck, increasing defense.")
    fnSetDescription("Battle Skirt",        "Simple skirt woven from synthweave. Extremely easy to move in, but most importantly, lets you look cute.")
    fnSetDescription("Hyperweave Chemise",  "An ordinary shirt made of hyperweave. The extreme durability of hyperweave makes this a combat-ready piece\nof fashion.")
    fnSetDescription("Neon Tanktop",        "Cloth fiber tanktop that fluoresces under most lighting conditions. Provides almost no protection, but is very\nintimidating.")

    -- |[Store]|
    --Store the chart under a unique name.
    gzLightArmorChart = gzActiveChart
    gzActiveChart = nil

end

-- |[ ==================================== Item Is On Chart ==================================== ]|
--If the item is on the prefabricated chart, we can create it algorithmically.
Debug_PushPrint(bDebug, "Scanning light armor chart.\n")
for i = 1, #gzLightArmorChart, 1 do
    if(gzLightArmorChart[i].sName == gsItemName) then
        
        --Debug.
        Debug_Print("Matched item " .. gsItemName .. " in light armor slot " .. i .. "\n")

        --Quick-access copy.
        local zItem = gzLightArmorChart[i]

        --Create item.
        Debug_Print("Creating item.\n")
        AdInv_CreateItem(gsItemName)
        
            -- |[System]|
            Debug_Print("Basic Properties.\n")
            AdItem_SetProperty("Value", zItem.iValue)
            AdItem_SetProperty("Gem Slots", zItem.iGemSlots)
            AdItem_SetProperty("Is Key Item", zItem.bIsKeyItem)
            
            -- |[Display]|
            Debug_Print("Display Properties.\n")
            AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/" .. zItem.sSymbol22)
            AdItem_SetProperty("Description", zItem.sDescription)
            AdItem_SetProperty("Type", "Light Armor")
            
            -- |[Usability]|
            --Light armors can be used by nearly every character in the game.
            Debug_Print("Usability Properties.\n")
            AdItem_SetProperty("Is Equipment", true)
            AdItem_SetProperty("Add Equippable Slot", "Body")
            AdItem_SetProperty("Add Equippable Character", "Mei")
            AdItem_SetProperty("Add Equippable Character", "Florentina")
            AdItem_SetProperty("Add Equippable Character", "Christine")
            AdItem_SetProperty("Add Equippable Character", "Tiffany")
            AdItem_SetProperty("Add Equippable Character", "SX-399")
            AdItem_SetProperty("Add Equippable Character", "Sanya")
            AdItem_SetProperty("Add Equippable Character", "Izuna")
            AdItem_SetProperty("Add Equippable Character", "Empress")
            
            -- |[Modded Usability]|
            --Execute this script with the name of the item in question. Modded characters may be able to use base-game items.
            fnExecModScript("Items/Usability Exec.lua")
            
            -- |[Stat Array]|
            Debug_Print("Statistic Properties.\n")
            for p = 1, #zItem.iaStatArray, 2 do
                AdItem_SetProperty("Statistic", zItem.iaStatArray[p+0], zItem.iaStatArray[p+1])
            end
            
            -- |[Advanced Description]|
            Debug_Print("Running standard description.\n")
            fnStandardizeDescription(gcs_UI_DescriptionStd_Armor, gci_UI_AdItem_Max_Description_Lines)
        DL_PopActiveObject()

        --Finish up.
        gbFoundItem = true
        Debug_PopPrint("Done.\n")
        return
    end
end
Debug_PopPrint("Not on chart.\n")

-- |[ ====================================== Unique Items ====================================== ]|
--Items that do not fit on the chart are created here. These are items that deviate from the normal
-- creation pattern for some reason.

--No items yet.
