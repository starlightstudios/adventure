-- |[ ===================================== Items for Mei ====================================== ]|
--Items that only Mei can equip for various reasons. Mei can use her work clothes, light armor, and katanas.
-- This listing also includes her runestone, like all chapter protagonists.
--Like all item subscripts, uses globals gsItemName and gfBypassAdamantite for efficiency reasons.

-- |[ ================================= Weapon Standardization ================================= ]|
--Sets standard equippable case, slots, and weapon animations.
local fnStandardWeapon = function()
    
    --Description type.
    AdItem_SetProperty("Type", "Curved Sword")
    
    --Can be used as a weapon.
    AdItem_SetProperty("Is Equipment", true)
    AdItem_SetProperty("Add Equippable Character", "Mei")
    AdItem_SetProperty("Add Equippable Slot", "Weapon")
    AdItem_SetProperty("Add Equippable Slot", "Weapon Backup A")
    AdItem_SetProperty("Add Equippable Slot", "Weapon Backup B")
    
    --Animations and Sound
    AdItem_SetProperty("Equipment Attack Animation", "Sword Slash")
    AdItem_SetProperty("Equipment Attack Sound", "Combat\\|Impact_CrossSlash")
    AdItem_SetProperty("Equipment Critical Sound", "Combat\\|Impact_CrossSlash_Crit")
    
    --Description handling.
    fnStandardizeDescription(gcs_UI_DescriptionStd_Weapon, gci_UI_AdItem_Max_Description_Lines)
end

-- |[ ====================================== Unique Items ====================================== ]|
--Mei's runestone. Restores HP in combat, recharges.
if(gsItemName == "Silver Runestone") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A runestone with a silver spiral marking on it.\nRestores 20%% of max HP when used in combat.\nKey Item.")
        AdItem_SetProperty("Type", "Combat Item (Mei)")
		AdItem_SetProperty("Value", -1)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Mei")
		AdItem_SetProperty("Add Equippable Slot", "Item A")
		AdItem_SetProperty("Add Equippable Slot", "Item B")
        AdItem_SetProperty("Ability Path", gsRoot .. "Combat/Party/Items/Abilities/Silver Runestone.lua")
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/RuneMei")
        fnStandardizeDescription(gcs_UI_DescriptionStd_CombatItem, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true

--Upgraded runestone.
elseif(gsItemName == "Silver Runestone Mk II") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A runestone with a silver spiral marking on it.\nRestores 25%% of max HP and increases accuracy when used in combat.\nKey Item.")
        AdItem_SetProperty("Type", "Combat Item (Mei)")
		AdItem_SetProperty("Value", -1)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Mei")
		AdItem_SetProperty("Add Equippable Slot", "Item A")
		AdItem_SetProperty("Add Equippable Slot", "Item B")
        AdItem_SetProperty("Ability Path", gsRoot .. "Combat/Party/Items/Abilities/Silver Runestone Mk II.lua")
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/RuneMeiMkII")
        fnStandardizeDescription(gcs_UI_DescriptionStd_CombatItem, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true
	
--Special Bee Honey. Restores HP in combat!
elseif(gsItemName == "Everlasting Honey") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Delicious honey for long-range scouting drones. Restores HP in combat and never runs out!")
        AdItem_SetProperty("Type", "Combat Item (Mei)")
		AdItem_SetProperty("Value", -1)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Mei")
		AdItem_SetProperty("Add Equippable Slot", "Item A")
		AdItem_SetProperty("Add Equippable Slot", "Item B")
        AdItem_SetProperty("Ability Path", gsRoot .. "Combat/Party/Items/Abilities/Everlasting Honey.lua")
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/EverlastingHoney")
        fnStandardizeDescription(gcs_UI_DescriptionStd_CombatItem, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true

-- |[ ==================================== Weapons (Tier 0) ==================================== ]|
-- |[Rusty Katana]|
--Rusty Katana, Mei finds this at the start of the game.
elseif(gsItemName == "Rusty Katana") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A beaten up katana. It still has an edge, but it won't cut easily. Still better than nothing.\nDeals slashing damage.")
		AdItem_SetProperty("Value", 25)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepMeiKatana")
		AdItem_SetProperty("Gem Slots", 1)
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack, 12)
        AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, gciDamageType_Slashing + gciDamageOffsetToResistances, 1.0)
        
        --Function setup.
        fnStandardWeapon()
        
	DL_PopActiveObject()
	gbFoundItem = true

-- |[Rusty Jian]|
--Rusty Jian. Can be upgraded. Provides a speed boost.
elseif(gsItemName == "Rusty Jian") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A popular straight sword often used by officers. Its blade is rusted, but it will still cut.\nIncreases combat dexterity. Deals slashing damage.")
		AdItem_SetProperty("Value", 25)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepMeiJian")
		AdItem_SetProperty("Gem Slots", 1)
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack, 10)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 3)
        AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, gciDamageType_Slashing + gciDamageOffsetToResistances, 1.0)
        
        --Function setup.
        fnStandardWeapon()
        
	DL_PopActiveObject()
	gbFoundItem = true

-- |[ ==================================== Weapons (Tier 1) ==================================== ]|
-- |[Bronze Katana]|
elseif(gsItemName == "Bronze Katana") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A katana sword, made from bronze. Sharp, but fairly brittle.\nDeals slashing damage.")
		AdItem_SetProperty("Value", 100)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepMeiKatana")
		AdItem_SetProperty("Gem Slots", 1)
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack, 22)
        AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, gciDamageType_Slashing + gciDamageOffsetToResistances, 1.0)
        
        --Function setup.
        fnStandardWeapon()
	DL_PopActiveObject()
	gbFoundItem = true
    
-- |[Bronze Jian]|
elseif(gsItemName == "Bronze Jian") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "An officer's straight sword, forged from bronze. Sharp, but fairly brittle.\nDeals slashing damage.")
		AdItem_SetProperty("Value", 100)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepMeiJian")
		AdItem_SetProperty("Gem Slots", 1)
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack, 19)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 10)
        AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, gciDamageType_Slashing + gciDamageOffsetToResistances, 1.0)
        
        --Function setup.
        fnStandardWeapon()
	DL_PopActiveObject()
	gbFoundItem = true
    
-- |[ ==================================== Weapons (Tier 2) ==================================== ]|
-- |[Serrated Katana]|
--Deals bleed damage.
elseif(gsItemName == "Serrated Katana") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A katana sword with an edge that is filed irregularly to give it an uneven, tearing cut.\nDeals bleeding damage.")
		AdItem_SetProperty("Value", 250)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepMeiBleed")
		AdItem_SetProperty("Gem Slots", 1)
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack, 26)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 10)
        AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, gciDamageType_Bleeding + gciDamageOffsetToResistances, 1.0)
        
        --Function setup.
        fnStandardWeapon()
    
        --Override to use bleeding attack.
        AdItem_SetProperty("Equipment Attack Animation", "Bleed")
        AdItem_SetProperty("Equipment Attack Sound", "Combat\\|Impact_CrossSlash")
        AdItem_SetProperty("Equipment Critical Sound", "Combat\\|Impact_CrossSlash_Crit")
	DL_PopActiveObject()
	gbFoundItem = true
    
-- |[Wildflower's Katana]|
--Granted to friends of the forest.
elseif(gsItemName == "Wildflower's Katana") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A pretty blue gem adorns the hilt, and vines seem to grow from it.\nThe blade is alive, and in tune with the forest.\nDeals slashing damage.")
		AdItem_SetProperty("Value", 250)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepMeiKatana")
		AdItem_SetProperty("Gem Slots", 2)
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack,     29)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 10)
        AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, gciDamageType_Slashing + gciDamageOffsetToResistances, 1.0)
        
        --Function setup.
        fnStandardWeapon()
	DL_PopActiveObject()
	gbFoundItem = true
	
-- |[Steel Jian]|
--Steel Jian, straight upgrade over the Rusty Jian.
elseif(gsItemName == "Steel Jian") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A popular straight sword often used by officers. Its blade is light and easy to handle.\nIncreases combat dexterity. Deals slashing damage.")
		AdItem_SetProperty("Value", 200)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepMeiJian")
		AdItem_SetProperty("Gem Slots", 1)
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack, 23)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 10)
        AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, gciDamageType_Slashing + gciDamageOffsetToResistances, 1.0)
        
        --Function setup.
        fnStandardWeapon()
	DL_PopActiveObject()
	gbFoundItem = true

-- |[ ==================================== Weapons (Tier 3) ==================================== ]|
-- |[Steel Katana]|
elseif(gsItemName == "Steel Katana") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A katana sword, with a sharp edge and a relatively short reach. Fairly light for its size.\nDeals slashing damage.")
		AdItem_SetProperty("Value", 350)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepMeiKatana")
		AdItem_SetProperty("Gem Slots", 1)
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack, 42)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 10)
        AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, gciDamageType_Slashing + gciDamageOffsetToResistances, 1.0)
        
        --Function setup.
        fnStandardWeapon()
	DL_PopActiveObject()
	gbFoundItem = true
    
-- |[Razor Katana]|
elseif(gsItemName == "Razor Katana") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A katana sword with a thin, jagged edge.\nDeals bleeding damage.")
		AdItem_SetProperty("Value", 350)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepMeiBleed")
		AdItem_SetProperty("Gem Slots", 1)
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack, 42)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 10)
        AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, gciDamageType_Bleeding + gciDamageOffsetToResistances, 1.0)
        
        --Function setup.
        fnStandardWeapon()
	DL_PopActiveObject()
	gbFoundItem = true
    
-- |[ ==================================== Weapons (Tier 4) ==================================== ]|
-- |[Foran Ritual Blade]|
elseif(gsItemName == "Foran Ritual Blade") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Made from marble, yet somehow is light and holds an edge. Used for unknown rituals.\nDeals slashing damage.")
		AdItem_SetProperty("Value", 520)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepMeiKatana")
		AdItem_SetProperty("Gem Slots", 2)
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack, 65)
        AdItem_SetProperty("Statistic", gciStatIndex_Accuracy, 5)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 10)
        AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, gciDamageType_Slashing + gciDamageOffsetToResistances, 1.0)
        
        --Function setup.
        fnStandardWeapon()
	DL_PopActiveObject()
	gbFoundItem = true
    
-- |[Rubber Cavalry Sword]|
elseif(gsItemName == "Rubber Cavalry Sword") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A straight sword used by cavalry, that is inexplicably made of rubber.\nDeals striking damage.")
		AdItem_SetProperty("Value", 470)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepMeiJian")
		AdItem_SetProperty("Gem Slots", 1)
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack, 59)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 15)
        AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, gciDamageType_Striking + gciDamageOffsetToResistances, 1.0)
        
        --Function setup.
        fnStandardWeapon()
	DL_PopActiveObject()
	gbFoundItem = true
    
-- |[Soulfire Blade]|
elseif(gsItemName == "Soulfire Blade") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "The edge cuts not just through flesh, but through the soul.\nDeals flaming damage.")
		AdItem_SetProperty("Value", 710)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepMeiKatana")
		AdItem_SetProperty("Gem Slots", 2)
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack, 85)
        AdItem_SetProperty("Statistic", gciStatIndex_Accuracy, 15)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 15)
        AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, gciDamageType_Flaming + gciDamageOffsetToResistances, 1.0)
        
        --Function setup.
        fnStandardWeapon()
	DL_PopActiveObject()
	gbFoundItem = true

-- |[ ==================================== Armors (Tier 0) ===================================== ]|
-- |[Mei's Work Uniform]|
--Not really armor. Provides minimal protection.
elseif(gsItemName == "Mei's Work Uniform") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Clothes from the themed restaurant Mei works for. Aimed at tourists, it's not normal street apparel.")
        AdItem_SetProperty("Type", "Armor (Mei)")
		AdItem_SetProperty("Value", 25)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Mei")
		AdItem_SetProperty("Add Equippable Slot", "Body")
        AdItem_SetProperty("Statistic", gciStatIndex_Protection, 2)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 20)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmorMei")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Armor, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true

-- |[ ==================================== Armors (Tier 1) ===================================== ]|
-- |[Fencer's Raiment]|
--Clothing that provides less protection than most, but increases damage output.
elseif(gsItemName == "Fencer's Raiment") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A light cloth robe infused with simple magic. Easy to move in, it increases damage dealt by Mei's weapons.")
        AdItem_SetProperty("Type", "Armor (Mei)")
		AdItem_SetProperty("Value", 75)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Mei")
		AdItem_SetProperty("Add Equippable Slot", "Body")
        AdItem_SetProperty("Statistic", gciStatIndex_Attack,    10)
        AdItem_SetProperty("Statistic", gciStatIndex_Accuracy,   3)
        AdItem_SetProperty("Statistic", gciStatIndex_Evade,      3)
        AdItem_SetProperty("Statistic", gciStatIndex_Protection, 2)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 20)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmorMei")
		AdItem_SetProperty("Gem Slots", 0)
        fnStandardizeDescription(gcs_UI_DescriptionStd_Armor, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true

-- |[Striker's Tunic]|
--Clothing that provides less protection than most, but increases damage output.
elseif(gsItemName == "Striker's Tunic") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A magic robe that greatly increases combat accuracy, but doesn't provide much protection.")
        AdItem_SetProperty("Type", "Armor (Mei)")
		AdItem_SetProperty("Value", 200)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Mei")
		AdItem_SetProperty("Add Equippable Slot", "Body")
        AdItem_SetProperty("Statistic", gciStatIndex_Attack,      5)
        AdItem_SetProperty("Statistic", gciStatIndex_Accuracy,   10)
        AdItem_SetProperty("Statistic", gciStatIndex_Protection,  2)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 20)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmorMei")
		AdItem_SetProperty("Gem Slots", 0)
        fnStandardizeDescription(gcs_UI_DescriptionStd_Armor, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true

-- |[ ==================================== Armors (Tier 2) ===================================== ]|
-- |[Tiger Dancer's Dress]|
--A dress worn by Tiger Dancers, duh.
elseif(gsItemName == "Tiger Dancer's Dress") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A dress supposedly worn by the Tiger Dancers of Quantir. Fierce warrior women who danced through the\nbattlefield, carving through the enemy lines, they are said to be long extinct.")
        AdItem_SetProperty("Type", "Armor (Mei)")
		AdItem_SetProperty("Value", 300)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Mei")
		AdItem_SetProperty("Add Equippable Slot", "Body")
        AdItem_SetProperty("Statistic", gciStatIndex_Attack,     15)
        AdItem_SetProperty("Statistic", gciStatIndex_Accuracy,    5)
        AdItem_SetProperty("Statistic", gciStatIndex_Protection,  2)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 20)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmorMei")
		AdItem_SetProperty("Gem Slots", 1)
        fnStandardizeDescription(gcs_UI_DescriptionStd_Armor, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true
    
-- |[Dress of the Slime Dancer]|
--Bigger increase in damage.
elseif(gsItemName == "Dress of the Slime Dancer") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "While wearing clothes is considered odd for most slimes, their dancers find it helps keep from slinging goo\neverywhere.")
        AdItem_SetProperty("Type", "Armor (Mei)")
		AdItem_SetProperty("Value", 400)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Mei")
		AdItem_SetProperty("Add Equippable Slot", "Body")
        AdItem_SetProperty("Statistic", gciStatIndex_Attack,    10)
        AdItem_SetProperty("Statistic", gciStatIndex_Evade,      3)
        AdItem_SetProperty("Statistic", gciStatIndex_Accuracy,   3)
        AdItem_SetProperty("Statistic", gciStatIndex_Protection, 5)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmorMei")
		AdItem_SetProperty("Gem Slots", 2)
        fnStandardizeDescription(gcs_UI_DescriptionStd_Armor, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true
    
-- |[ ==================================== Armors (Tier 3) ===================================== ]|
-- |[Rubbery Plate Armor]|
elseif(gsItemName == "Rubbery Plate Armor") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Plate armor somehow infused with rubber. Offers less protection, but is light, and protects from striking attacks.")
        AdItem_SetProperty("Type", "Armor (Mei)")
		AdItem_SetProperty("Value", 650)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Mei")
		AdItem_SetProperty("Add Equippable Slot", "Body")
        AdItem_SetProperty("Statistic", gciStatIndex_Attack,     7)
        AdItem_SetProperty("Statistic", gciStatIndex_Evade,      7)
        AdItem_SetProperty("Statistic", gciStatIndex_Accuracy,   7)
        AdItem_SetProperty("Statistic", gciStatIndex_Protection, 5)
        AdItem_SetProperty("Statistic", gciStatIndex_Resist_Strike, 3)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmorMei")
		AdItem_SetProperty("Gem Slots", 2)
        fnStandardizeDescription(gcs_UI_DescriptionStd_Armor, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true
    
-- |[Armored Cultist Robe]|
elseif(gsItemName == "Armored Cultist Robe") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A cultist robe with thin ceramic plates sewn in. Light, firm, and fairly protective.")
        AdItem_SetProperty("Type", "Armor (Mei)")
		AdItem_SetProperty("Value", 650)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Mei")
		AdItem_SetProperty("Add Equippable Slot", "Body")
        AdItem_SetProperty("Statistic", gciStatIndex_Attack,     12)
        AdItem_SetProperty("Statistic", gciStatIndex_Evade,      18)
        AdItem_SetProperty("Statistic", gciStatIndex_Accuracy,    5)
        AdItem_SetProperty("Statistic", gciStatIndex_Protection,  2)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 20)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmorMei")
		AdItem_SetProperty("Gem Slots", 2)
        fnStandardizeDescription(gcs_UI_DescriptionStd_Armor, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true
    
-- |[Swamp Dreg's Robe]|
elseif(gsItemName == "Swamp Dreg's Robe") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "The ruined swampy clothes of a dreg. Provides defense based on the user's soul.")
        AdItem_SetProperty("Type", "Armor (Mei)")
		AdItem_SetProperty("Value", 650)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Mei")
		AdItem_SetProperty("Add Equippable Slot", "Body")
        AdItem_SetProperty("Statistic", gciStatIndex_Attack,     20)
        AdItem_SetProperty("Statistic", gciStatIndex_Evade,      15)
        AdItem_SetProperty("Statistic", gciStatIndex_Accuracy,    1)
        AdItem_SetProperty("Statistic", gciStatIndex_Protection,  5)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative,  5)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmorMei")
		AdItem_SetProperty("Gem Slots", 2)
        fnStandardizeDescription(gcs_UI_DescriptionStd_Armor, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true

-- |[ ================================== Accessories (Tier 1) ================================== ]|
-- |[Haemophiliac's Pendant]|
elseif(gsItemName == "Haemophiliac's Pendant") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "For those who just love bleeding. Specifically, the bleeding of others. +25 attack power for bleed attacks.")
        AdItem_SetProperty("Type", "Accessory (Mei)")
		AdItem_SetProperty("Value", 180)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Mei")
		AdItem_SetProperty("Add Equippable Slot", "Accessory A")
		AdItem_SetProperty("Add Equippable Slot", "Accessory B")
		AdItem_SetProperty("Add Tag", "Bleed Damage Dealt +", 25)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/PendantR")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Accessory, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true
    
-- |[ ================================== Accessories (Tier 2) ================================== ]|
-- |[Moonglow Collar]|
--Awww, kitty!
elseif(gsItemName == "Moonglow Collar") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A collar that seems to shine with the light of the moon.")
        AdItem_SetProperty("Type", "Accessory (Mei)")
		AdItem_SetProperty("Value", 150)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Mei")
		AdItem_SetProperty("Add Equippable Slot", "Accessory A")
		AdItem_SetProperty("Add Equippable Slot", "Accessory B")
        AdItem_SetProperty("Statistic", gciStatIndex_Attack,     15)
        AdItem_SetProperty("Statistic", gciStatIndex_Evade,      12)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 25)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AccRingGldD")
		AdItem_SetProperty("Gem Slots", 1)
        fnStandardizeDescription(gcs_UI_DescriptionStd_Accessory, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true
    
-- |[Translucent Feather Duster]|
--It never needs to be cleaned! Thanks, ghost magic!
elseif(gsItemName == "Translucent Feather Duster") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Due to magic, it never needs to be cleaned! It can dust shelves for eternity!")
        AdItem_SetProperty("Type", "Accessory (Mei)")
		AdItem_SetProperty("Value", 150)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Mei")
		AdItem_SetProperty("Add Equippable Slot", "Accessory A")
		AdItem_SetProperty("Add Equippable Slot", "Accessory B")
        AdItem_SetProperty("Statistic", gciStatIndex_Evade, 8)
        AdItem_SetProperty("Statistic", gciStatIndex_Accuracy, 8)
        AdItem_SetProperty("Statistic", gciStatIndex_Protection, 2)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AccRing")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Accessory, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true
	
-- |[ ================================== Accessories (Tier 3) ================================== ]|
-- |[Diamond Bracelet]|
elseif(gsItemName == "Diamond Bracelet") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Authentic diamond bracelet with an enchantment that increases attack power. Fashionable and furious!")
        AdItem_SetProperty("Type", "Accessory (Mei)")
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Mei")
		AdItem_SetProperty("Add Equippable Slot", "Accessory A")
		AdItem_SetProperty("Add Equippable Slot", "Accessory B")
        AdItem_SetProperty("Statistic", gciStatIndex_Attack, 30)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AccRing")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Accessory, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true
end

-- |[ ========================================= Badges ========================================= ]|
--Badges never have stats, and are only used to make enemy types not attack the player.
if(gzMeiBadgeList == nil) then
    
    --List.
    gzMeiBadgeList = {}
    
    --Adder Function.
    local function fnAddBadge(psName, psSymbol, psDescription)
        local zObject = {}
        zObject.sName = psName
        zObject.sDescription = psDescription
        zObject.sSymbol = "Root/Images/AdventureUI/Symbols22/" .. psSymbol
        table.insert(gzMeiBadgeList, zObject)
    end
    
    --Suffix.
    local sSuffix = "[BR]Badges are obtained automatically upon entering a form for the first time."
    
    --Listing.
    fnAddBadge("Nature Warden Badge", "Badge|Alraune", "A badge with a flower painted on it. Alraunes will know you're a friend if you wear this." .. sSuffix)
    fnAddBadge("Bee Vanguard Badge",  "Badge|Bee",     "A badge with a saluting bee on it. Bees will know you're a hive-sister if you wear this." .. sSuffix)
    fnAddBadge("Werecat Claw Badge",  "Badge|Werecat", "A badge with a werecat claw on it. Crudely made, lets werecats know you're one of them - and plenty tough!" .. sSuffix)
    fnAddBadge("Slime Pal Badge",     "Badge|Slime",   "A badge with some slime on it. Slimes will know you're a friend if you wear this. Is it - dripping?" .. sSuffix)
    fnAddBadge("Burning Soul Badge",  "Badge|Wisphag", "A badge with a wisphag on it. Wisphags will know you are a fellow warden if you wear this. Don't ask how." .. sSuffix)
end

--Scan the badges for a match.
for i = 1, #gzMeiBadgeList, 1 do
    if(gzMeiBadgeList[i].sName == gsItemName) then
    
        --Create.
        AdInv_CreateItem(gsItemName)
            AdItem_SetProperty("Description", gzMeiBadgeList[i].sDescription)
            AdItem_SetProperty("Type", "Badge")
            AdItem_SetProperty("Value", 0)
            AdItem_SetProperty("Is Key Item", true)
            AdItem_SetProperty("Is Equipment", true)
            AdItem_SetProperty("Add Equippable Character", "Mei")
            AdItem_SetProperty("Add Equippable Slot", "Badge")
            AdItem_SetProperty("Rendering Image", gzMeiBadgeList[i].sSymbol)
            fnStandardizeDescription(gcs_UI_DescriptionStd_Misc, gci_UI_AdItem_Max_Description_Lines)
        DL_PopActiveObject()
    
        --Finish.
        gbFoundItem = true
        return
    end
end
