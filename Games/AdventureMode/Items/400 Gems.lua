-- |[ ====================================== Gem Listing ======================================= ]|
--Gems. Can be upgraded with Adamantite, allowing them to be merged together. Two gems cannot be
-- merged together if they share colors.

-- |[ ======================================= Gem Colors ======================================= ]|
-- |[Grey]|
--Glintsteel   == +Accuracy, +Evade
--Phassicsteel == +Attack, -SlsRes

-- |[Red]|
--Yemite     == +Attack
--Romite     == +SlsRes

-- |[Blue]|
--Ardrion    == +BldDam, +PsnDam
--Nockrion   == +BldRes

-- |[Orange]|
--Rubose     == +PrcRes
--Piorose    == +Acc, +Ini

-- |[Violet]|
--Blurleen   == +StkRes
--Mordreen   == +Evd

-- |[Yellow]|
--Qederphage == +Ini, -Atk
--Phonophage == +Atk, -SlsRes, -PrcRes

-- |[Other Colors]|
--Surfofine  == Changes attack type to slash
--Blunofine  == Changes attack type to strike
--Pokemfine  == Changes attack type to peirce
--Pirofine   == Changes attack type to fire
--Arctofine  == Changes attack type to ice
--Execrine   == Changes attack type to shock
--Photofine  == Changes attack type to crusade
--Corrofine  == Changes attack type to obscure
--Hemofine   == Changes attack type to bleed
--Toxofine   == Changes attack type to poison
--Entrofine  == Changes attack type to corrode
--Psychofine == Changes attack type to terrify
    
-- |[ ======================================== Gem Chart ======================================= ]|
if(gzGemItemChart == nil) then
    
    --Setup.
    gzGemItemChart = {}
    
    --Function for stat/tag gems.
    local fnAddGem = function(psName, piColor, piValue, psSymbol, piaStatTable, pzaTagTable, psDescription)
        
        --New slot.
        local i = #gzGemItemChart + 1
        gzGemItemChart[i] = {}
        
        --Set.
        gzGemItemChart[i].sName = psName
        gzGemItemChart[i].iColor = piColor
        gzGemItemChart[i].iValue = piValue
        gzGemItemChart[i].sSymbol = psSymbol
        gzGemItemChart[i].sDescription = psDescription
        gzGemItemChart[i].iaStatArray = piaStatTable
        gzGemItemChart[i].zaTagArray = pzaTagTable
        gzGemItemChart[i].iDamageOverride = -1
    end
    
    --Function for adding damage-override gems.
    local fnAddOverrideGem = function(psName, piColor, piValue, psSymbol, piaStatTable, pzaTagTable, piDamageType, psDescription)
        
        --New slot.
        local i = #gzGemItemChart + 1
        gzGemItemChart[i] = {}
        
        --Set.
        gzGemItemChart[i].sName = psName
        gzGemItemChart[i].iColor = piColor
        gzGemItemChart[i].iValue = piValue
        gzGemItemChart[i].sSymbol = psSymbol
        gzGemItemChart[i].sDescription = psDescription
        gzGemItemChart[i].iaStatArray = piaStatTable
        gzGemItemChart[i].zaTagArray = pzaTagTable
        gzGemItemChart[i].iDamageOverride = piDamageType
    end
    
    --Function that adds a gem via markdown.
    local function fnMkDnGem(psName, piColor, piValue, psSymbol, psMarkdown, psDescription)

        --Structure
        local zGemEntry = {}
        zGemEntry.sName = psName
        zGemEntry.iColor = piColor
        zGemEntry.iValue = piValue
        zGemEntry.sSymbol = psSymbol
        zGemEntry.sDescription = psDescription
        zGemEntry.iDamageOverride = -1
        
        --Get the tag/stat arrays using the markdown.
        local iaStatArray, zaTagArray = fnMarkdownToArrays(psMarkdown)
        zGemEntry.iaStatArray = iaStatArray
        zGemEntry.zaTagArray  = zaTagArray

        --New slot.
        table.insert(gzGemItemChart, zGemEntry)
    end
    
    -- |[Standard Suffix]|
    --This is appended onto all normal gem descriptions to indicate they are gems.
    local sSuffix = "[BR]Can be socketed into equipment or merged with other gems."

    -- |[Gem Chart]|
    fnMkDnGem("Glintsteel Gem",   gciGemColor_Grey,   200, "Gem1|G", "Acc|Flat|2 Evd|Flat|1",                     "A small grey gem that sparks with static electricity." .. sSuffix)
    fnMkDnGem("Phassicsteel Gem", gciGemColor_Grey,   200, "Gem1|G", "Atk|Flat|6 ResSls|Flat|-1",                 "A small grey gem that feels very rough to the touch." .. sSuffix)
    fnMkDnGem("Arensteel Gem",    gciGemColor_Grey,   200, "Gem1|G", "Acc|Flat|6 Evd|Flat|-3",                    "A small grey gem that smells like eggplants." .. sSuffix)
    fnMkDnGem("Yetesteel Gem",    gciGemColor_Grey,   200, "Gem1|G", "ResSls|Flat|-4 SlshDam+|Flat|10",           "A small grey gem that shines even in darkness. +10[PCT] Slash Damage dealt." .. sSuffix)
    fnMkDnGem("Yemite Gem",       gciGemColor_Red,    200, "Gem1|R", "Atk|Flat|3",                                "A small red gem that glows faintly in the dark." .. sSuffix)
    fnMkDnGem("Romite Gem",       gciGemColor_Red,    200, "Gem1|R", "ResSls|Flat|1",                             "A small red gem that feels wet even when dry." .. sSuffix)
    fnMkDnGem("Morite Gem",       gciGemColor_Red,    200, "Gem1|R", "Ini|Flat|5",                                "A small red gem that has hundreds of tiny cracks in it." .. sSuffix)
    fnMkDnGem("Kyite Gem",        gciGemColor_Red,    200, "Gem1|R", "ResFlm|Flat|1 ResFrz|Flat|1",               "A small red gem that glows when you speak near it." .. sSuffix)
    fnMkDnGem("Ardrion Gem",      gciGemColor_Blue,   200, "Gem1|B", "BldDam+|Flat|3 PsnDam+|Flat|3",             "A small blue gem that seems to squish when squeezed. +3[PCT] Bleed/Poison Damage dealt." .. sSuffix)
    fnMkDnGem("Nockrion Gem",     gciGemColor_Blue,   200, "Gem1|B", "ResBld|Flat|1",                             "A small blue gem that is extremely smooth to the touch." .. sSuffix)
    fnMkDnGem("Donion Gem",       gciGemColor_Blue,   200, "Gem1|B", "Atk|Flat|3",                                "A small blue gem that is like sandpaper to the touch." .. sSuffix)
    fnMkDnGem("Samlion Gem",      gciGemColor_Blue,   200, "Gem1|B", "ResPsn|Flat|2",                             "A small blue gem that looks a little like an olive if you squint." .. sSuffix)
    fnMkDnGem("Rubose Gem",       gciGemColor_Orange, 200, "Gem1|O", "ResPrc|Flat|1",                             "A small orange gem that is warm to the touch." .. sSuffix)
    fnMkDnGem("Piorose Gem",      gciGemColor_Orange, 200, "Gem1|O", "Acc|Flat|2 Ini|Flat|3",                     "A small orange gem that is sharp, but doesn't cut your fingers." .. sSuffix)
    fnMkDnGem("Iniorose Gem",     gciGemColor_Orange, 200, "Gem1|O", "Prt|Flat|1",                                "A small orange gem that becomes softer when pressed." .. sSuffix)
    fnMkDnGem("Whodorose Gem",    gciGemColor_Orange, 200, "Gem1|O", "Atk|Flat|2",                                "A small orange gem that absorbs water and never seems to let it out." .. sSuffix)
    fnMkDnGem("Blurleen Gem",     gciGemColor_Violet, 200, "Gem1|V", "ResStk|Flat|1",                             "A small violet gem which has fluid trapped inside it." .. sSuffix)
    fnMkDnGem("Mordreen Gem",     gciGemColor_Violet, 200, "Gem1|V", "Evd|Flat|4",                                "A small violet gem that is almost transparent." .. sSuffix)
    fnMkDnGem("Quorine Gem",      gciGemColor_Violet, 200, "Gem1|V", "ShkDam+|Flat|5",                            "A small violet gem that tingles. +5[PCT] Shock Damage dealt." .. sSuffix)
    fnMkDnGem("Phirine Gem",      gciGemColor_Violet, 200, "Gem1|V", "Acc|Flat|4 PsnDam+|Flat|3",                 "A small violet gem that is sharp but doesn't cut your skin. +3[PCT] Poison Damage dealt." .. sSuffix)
    fnMkDnGem("Qederphage Gem",   gciGemColor_Yellow, 200, "Gem1|Y", "Ini|Flat|10 Atk|Flat|-3",                   "A small yellow gem that becomes significantly harder when pressed." .. sSuffix)
    fnMkDnGem("Phonophage Gem",   gciGemColor_Yellow, 200, "Gem1|Y", "Atk|Flat|7 ResSls|Flat|-1 ResPrc|Flat|-1",  "A small yellow gem that is almost rubbery." .. sSuffix)
    fnMkDnGem("Thatophage Gem",   gciGemColor_Yellow, 200, "Gem1|Y", "PrcDam+|Flat|5",                            "A small yellow gem with intricate designs on it. +5[PCT] Pierce Damage dealt." .. sSuffix)
    fnMkDnGem("Cirrophage Gem",   gciGemColor_Yellow, 200, "Gem1|Y", "ResSls|Flat|2 ResStk|Flat|2 ResPrc|Flat|2", "A small yellow gem that makes a whistling sound in the wind." .. sSuffix)

    -- |[Damage Override Gems]|
    --These gems override damage values and need different code.
    sSuffix = " when socketed in a\nweapon. Does nothing otherwise.\nIf multiple gems are socketed in a weapon, the first one sets the type."
    local sAltSuffix = " when\nsocketed in a weapon. Does nothing otherwise.\nIf multiple gems are socketed in a weapon, the first one sets the type."
    fnAddOverrideGem("Surfofine Gem", gciGemColor_All, 500, "GemGreen", {}, {}, gciDamageType_Slashing,  "A small green gem that is razor thin at the edges. Changes weapon damage to [GemDamage]" .. sSuffix)
    fnAddOverrideGem("Blunofine Gem", gciGemColor_All, 500, "GemGreen", {}, {}, gciDamageType_Striking,  "A small green gem that is flat and featureless. Changes weapon damage to [GemDamage]" .. sSuffix)
    fnAddOverrideGem("Pokemfine Gem", gciGemColor_All, 500, "GemGreen", {}, {}, gciDamageType_Piercing,  "A small green gem that is shaped like an arrow. Changes weapon damage to [GemDamage]" .. sSuffix)
    fnAddOverrideGem("Pirofine Gem",  gciGemColor_All, 500, "GemGreen", {}, {}, gciDamageType_Flaming,   "A small green gem that gives off constant warmth. Changes weapon damage to [GemDamage]" .. sSuffix)
    fnAddOverrideGem("Arctofine Gem", gciGemColor_All, 500, "GemGreen", {}, {}, gciDamageType_Freezing,  "A small green gem that feels cool no matter what. Changes weapon damage to [GemDamage]" .. sSuffix)
    fnAddOverrideGem("Execrine Gem",  gciGemColor_All, 500, "GemGreen", {}, {}, gciDamageType_Shocking,  "A small green gem that always has a static shock waiting. Changes weapon damage to [GemDamage]" .. sSuffix)
    fnAddOverrideGem("Photofine Gem", gciGemColor_All, 500, "GemGreen", {}, {}, gciDamageType_Crusading, "A small green gem that glows brightly. Changes weapon damage to [GemDamage]" .. sSuffix)
    fnAddOverrideGem("Corrofine Gem", gciGemColor_All, 500, "GemGreen", {}, {}, gciDamageType_Obscuring, "A small green gem that has a dark blotch in the center. Changes weapon damage to [GemDamage]" .. sSuffix)
    fnAddOverrideGem("Hemofine Gem",  gciGemColor_All, 500, "GemGreen", {}, {}, gciDamageType_Bleeding,  "A small green gem that feels greasy. Changes weapon damage to [GemDamage]" .. sSuffix)
    fnAddOverrideGem("Toxofine Gem",  gciGemColor_All, 500, "GemGreen", {}, {}, gciDamageType_Poisoning, "A small green gem that feels dirty. Changes weapon damage to [GemDamage]" .. sSuffix)
    fnAddOverrideGem("Entrofine Gem", gciGemColor_All, 500, "GemGreen", {}, {}, gciDamageType_Corroding, "A small green gem that feels lumpy, but looks smooth. Changes weapon damage to [GemDamage]" .. sAltSuffix)
    fnAddOverrideGem("Psychofine Gem",gciGemColor_All, 500, "GemGreen", {}, {}, gciDamageType_Terrifying,"A small green gem that changes shape when you're not looking. Changes weapon damage to [GemDamage]" .. sAltSuffix)
    
end

-- |[ ===================================== List Scanning ====================================== ]|
for i = 1, #gzGemItemChart, 1 do
    if(gzGemItemChart[i].sName == gsItemName) then

        --Quick-access copy.
        local zItem = gzGemItemChart[i]

        --Create item.
        AdInv_CreateItem(gsItemName)
        
            --Basics.
            AdItem_SetProperty("Description", zItem.sDescription)
            AdItem_SetProperty("Value", zItem.iValue)
            AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/" .. zItem.sSymbol)
            AdItem_SetProperty("Is Gem", zItem.iColor)
            AdItem_SetProperty("Type", "Gem")
            
            --If this gem changes weapon damage type:
            if(zItem.iDamageOverride ~= -1) then
                AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Override, zItem.iDamageOverride + gciDamageOffsetToResistances, 1.0)
            end
            
            --For each stat pairing, apply:
            for p = 1, #zItem.iaStatArray, 2 do
                AdItem_SetProperty("Statistic", zItem.iaStatArray[p+0], zItem.iaStatArray[p+1])
            end
            
            --For each tag pairing, apply:
            for p = 1, #zItem.zaTagArray, 1 do
                AdItem_SetProperty("Add Tag", zItem.zaTagArray[p][1], zItem.zaTagArray[p][2])
            end
            
            -- |[Advanced Description]|
            fnStandardizeDescription(gcs_UI_DescriptionStd_Gem, gci_UI_AdItem_Max_Description_Lines)
        DL_PopActiveObject()
        
        --Finish up.
        gbFoundItem = true
        break
    end
end
