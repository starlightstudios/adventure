-- |[ ==================================== Items for JX-101 ==================================== ]|
--Items for JX-101. She comes with these and cannot equip anything else.

-- |[ ================================= Weapon Standardization ================================= ]|
--Sets standard equippable case, slots, and weapon animations.
local fnStandardWeapon = function()
    
    --Can be used as a weapon.
    AdItem_SetProperty("Is Equipment", true)
    AdItem_SetProperty("Add Equippable Character", "JX-101")
    AdItem_SetProperty("Add Equippable Slot", "Weapon")
    AdItem_SetProperty("Add Equippable Slot", "Weapon Backup A")
    AdItem_SetProperty("Add Equippable Slot", "Weapon Backup B")
    
    --Animations and Sound
    AdItem_SetProperty("Equipment Attack Animation", "Laser Shot")
    AdItem_SetProperty("Equipment Attack Sound",     "Combat\\|Impact_Laser")
    AdItem_SetProperty("Equipment Critical Sound",   "Combat\\|Impact_Laser_Crit")
    
    --Description standardization.
    fnStandardizeDescription(gcs_UI_DescriptionStd_Weapon, gci_UI_AdItem_Max_Description_Lines)
end

-- |[ ====================================== Unique Items ====================================== ]|
-- |[MkV Pulse Pistol]|
--She took what 55 said to heart.
if(gsItemName == "MkV Pulse Pistol") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A one-handed EM-Rail pistol, a clear improvement over the flawed Mk. IV variety.[BR]JX-101 has customized it to her needs.")
		AdItem_SetProperty("Value", 25)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepJX101")
		AdItem_SetProperty("Gem Slots", 0)
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack, 40)
        AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, gciDamageType_Piercing + gciDamageOffsetToResistances, 1.0)
        
        --Function setup.
        fnStandardWeapon()
        
	DL_PopActiveObject()
	gbFoundItem = true

-- |[JX-101's Clothes]|
--Yes, she does wear clothes.
elseif(gsItemName == "JX-101's Cuirass") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A heavy steel cuirass, forged to fit JX-101's body.")
        AdItem_SetProperty("Type", "Armor (JX-101)")
		AdItem_SetProperty("Value", 25)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "JX-101")
		AdItem_SetProperty("Add Equippable Slot", "Body")
        AdItem_SetProperty("Statistic", gciStatIndex_Protection, 1)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 20)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmorSX399")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Armor, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true

end
