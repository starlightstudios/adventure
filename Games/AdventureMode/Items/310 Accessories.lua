-- |[ =========================== Accessories, Character Independent =========================== ]|
--Accessories that can be used by anyone.

-- |[ ========================================= Charts ========================================= ]|
--For equipment which can be made as part of a chart, construct it here.
if(gzAccessoryChart == nil or gzGunUserChart == nil) then
    
    -- |[Setup]|
    --Setup. All functions operate on this chart.
    gzActiveChart = {}
    
    --Make aliases for the functions. This shortens the chart for readability.
    local fnAdd            = fnAddItemToChartMarkdown
    local fnSetDescription = fnSetDescriptionOnChart

    -- |[Accessories Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22      | Stats Array
    fnAdd("Sphalite Ring",                     100,          0,     false, gciAccessoryCode, "AccRingSlvA",   "Ini|5")
    fnAdd("Decorative Bracer",                 125,          0,     false, gciAccessoryCode, "AccBracer",     "Atk|4")
    fnAdd("Jade Eye Ring",                     150,          1,     false, gciAccessoryCode, "AccRingGldB",   "Ini|5 Atk|3")
    fnAdd("Enchanted Ring",                    150,          1,     false, gciAccessoryCode, "AccRingSlvC",   "Atk|5 HPMax|15")
    fnAdd("Jade Necklace",                     250,          1,     false, gciAccessoryCode, "PendantG",      "Acc|5 Evd|5")
    fnAdd("Arm Brace",                          75,          0,     false, gciAccessoryCode, "AccBracer",     "Prt|1")
    fnAdd("Alacrity Bracer",                    75,          1,     false, gciAccessoryCode, "AccBracer",     "Ini|10 Prt|1")
    fnAdd("Swamp Boots",                        75,          1,     false, gciAccessoryCode, "AccBoots",      "Ini|5 Evd|5 ResPsn|3")
    fnAdd("Comfy Socks",                       200,          0,     false, gciAccessoryCode, "AccBoots",      "Ini|5 Acc|5 ResFrz|5")
    fnAdd("Surgical Mask",                     250,          0,     false, gciAccessoryCode, "AccBracer",     "ResPsn|5")
    fnAdd("Fencer's Gauntlet",                 350,          2,     false, gciAccessoryCode, "AccBracer",     "Atk|12")
    fnAdd("Distribution Frame",                350,          0,     false, gciAccessoryCode, "ItemTech",      "Prt|2 Atk|5")
    fnAdd("Insulated Boots",                   350,          1,     false, gciAccessoryCode, "AccBoots",      "Prt|2 ResShk|5")
    fnAdd("Glove Liners",                       50,          0,     false, gciAccessoryCode, "AccGlovesBlk",  "Acc|7")
    fnAdd("Scale Gloves",                       50,          1,     false, gciAccessoryCode, "AccGloves",     "Prt|1 Acc|5")
    fnAdd("Butterfly Cape",                     50,          1,     false, gciAccessoryCode, "AccGloves",     "Evd|10 Ini|10")
    fnAdd("Wooden Greaves",                     50,          1,     false, gciAccessoryCode, "AccBoots",      "Prt|3 Ini|-10 Evd|-10 ResFlm|-4")
    fnAdd("Slicked Wooden Greaves",             50,          1,     false, gciAccessoryCode, "AccBoots",      "Prt|3 ResFlm|-4")
    fnAdd("Treated Wooden Greaves",             50,          1,     false, gciAccessoryCode, "AccBoots",      "Prt|3 Ini|-7 Evd|-7")
    fnAdd("Wing Badge",                          0,          1,      true, gciAccessoryCode, "HarpyBadge",    "Prt|2 Atk|5 Ini|20 Evd|20")
    fnAdd("Rush Bracers",                      150,          0,     false, gciAccessoryCode, "AccBracer",     "Prt|1 Atk|20 Ini|10")
    fnAdd("Warding Necklace",                  250,          0,     false, gciAccessoryCode, "PendantG",      "ResCrd|5")
    fnAdd("Everchill Thighpads",               250,          1,     false, gciAccessoryCode, "AccBoots",      "Prt|1 ResFlm|5")
    fnAdd("Sparkling Gloves",                  250,          1,     false, gciAccessoryCode, "AccGloves",     "Prt|1 Atk|10 Evd|10")
    fnAdd("Maniac Cape",                       250,          0,     false, gciAccessoryCode, "AccGloves",     "Prt|1 Atk|30 Evd|-10 Acc|-10")
    fnAdd("Manaflow Ring",                     350,          0,     false, gciAccessoryCode, "AccGloves",     "MPReg|5")

    -- |[Descriptions]|
    fnSetDescription("Sphalite Ring",          "A ring forged from Sphalite. It seems to hum when touched.")
    fnSetDescription("Decorative Bracer",      "A pretty cloth bracer that fits snugly on the wrist.")
    fnSetDescription("Jade Eye Ring",          "Golden ring with a cut jewel socketed in it. The jewel resembles an eye.")
    fnSetDescription("Enchanted Ring",         "A simple copper band, enchanted with a minor power spell. Increases health and damage.")
    fnSetDescription("Jade Necklace",          "Polished jade on a silver band. Has a minor agility enchantment.")
    fnSetDescription("Arm Brace",              "This light leather brace fits around the forearm to provide protection from attacks.")
    fnSetDescription("Alacrity Bracer",        "A light leather armbrace that seems to be enchanted. Increases initiative.")
    fnSetDescription("Swamp Boots",            "Tough boots made of leather, insulated to keep water from getting in.")
    fnSetDescription("Comfy Socks",            "Soft sheep's wool woven into sock form.")
    fnSetDescription("Surgical Mask",          "Thin, layered cloth that keeps you from breathing bad things. Effective against pandemics.")
    fnSetDescription("Fencer's Gauntlet",      "A wristguard for a fencer. The extra protection means you can be a little more fancy with your swings and not\nhurt yourself.")
    fnSetDescription("Distribution Frame",     "A thin exoskeleton designed to fit under clothing. Redistributes kinetic energy, reducing damage and increasing\nattack power.")
    fnSetDescription("Insulated Boots",        "Thermally insulated rubber boots. Look great, protect yourself - these have everything!")
    fnSetDescription("Glove Liners",           "Simple, thin glove liners that improve weapon handling.")
    fnSetDescription("Scale Gloves",           "Gloves made from crawler carapace. Durable, and improve weapon handling.")
    fnSetDescription("Butterfly Cape",         "A simple cape with magiical butterfly gossamer. Makes you light on your feet.")
    fnSetDescription("Wooden Greaves",         "Plain greaves made of wood. Protects your legs, but are heavy and prone to starting on fire.")
    fnSetDescription("Slicked Wooden Greaves", "Plain greaves made of wood. Lubricant makes them less cumbersome in combat, but they are still flammable.")
    fnSetDescription("Treated Wooden Greaves", "Plain greaves made of wood, treated with lacquer. They won't start on fire, but they are still somewhat\ncumbersome.")
    fnSetDescription("Wing Badge",             "A metal badge with two adorable little wings on it. Harpies will know not to attack you if you wear it.")
    fnSetDescription("Rush Bracers",           "These bracers provide a sudden rush of power when attacking.")
    fnSetDescription("Warding Necklace",       "An ordinary necklace that de-natures corrosive chemicals in the air around it.")
    fnSetDescription("Everchill Thighpads",    "Cloth pads woven with a special thread that remains cold even in direct sunlight.")
    fnSetDescription("Sparkling Gloves",       "Light leather gloves with an insert that amplifies magic, improving combat performance.")
    fnSetDescription("Maniac Cape",            "A great cape for people who don't mind getting hit, and want to hit back harder.")
    fnSetDescription("Manaflow Ring",          "This simple brass ring increases the natural MP regeneration of its wearer.")
    
    -- |[Usability]|
    fnSetUsabilityOnChart("Sphalite Ring",          gciUsableNotZeke)
    fnSetUsabilityOnChart("Decorative Bracer",      gciUsableEveryone)
    fnSetUsabilityOnChart("Jade Eye Ring",          gciUsableNotZeke)
    fnSetUsabilityOnChart("Enchanted Ring",         gciUsableNotZeke)
    fnSetUsabilityOnChart("Jade Necklace",          gciUsableEveryone)
    fnSetUsabilityOnChart("Arm Brace",              gciUsableEveryone)
    fnSetUsabilityOnChart("Alacrity Bracer",        gciUsableEveryone)
    fnSetUsabilityOnChart("Swamp Boots",            gciUsableNotZeke)
    fnSetUsabilityOnChart("Comfy Socks",            gciUsableNotZeke)
    fnSetUsabilityOnChart("Surgical Mask",          gciUsableNotZeke)
    fnSetUsabilityOnChart("Fencer's Gauntlet",      gciUsableNotZeke)
    fnSetUsabilityOnChart("Distribution Frame",     gciUsableNotZeke)
    fnSetUsabilityOnChart("Insulated Boots",        gciUsableNotZeke)
    fnSetUsabilityOnChart("Glove Liners",           gciUsableNotZeke)
    fnSetUsabilityOnChart("Scale Gloves",           gciUsableNotZeke)
    fnSetUsabilityOnChart("Butterfly Cape",         gciUsableEveryone)
    fnSetUsabilityOnChart("Wooden Greaves",         gciUsableEveryone)
    fnSetUsabilityOnChart("Slicked Wooden Greaves", gciUsableEveryone)
    fnSetUsabilityOnChart("Treated Wooden Greaves", gciUsableEveryone)
    fnSetUsabilityOnChart("Wing Badge",             gciUsableEveryone)
    fnSetUsabilityOnChart("Rush Bracers",           gciUsableEveryone)
    fnSetUsabilityOnChart("Warding Necklace",       gciUsableEveryone)
    fnSetUsabilityOnChart("Everchill Thighpads",    gciUsableEveryone)
    fnSetUsabilityOnChart("Sparkling Gloves",       gciUsableNotZeke)
    fnSetUsabilityOnChart("Maniac Cape",            gciUsableEveryone)
    fnSetUsabilityOnChart("Manaflow Ring",          gciUsableNotZeke)

    -- |[Store]|
    --Store the chart under a unique name.
    gzAccessoryChart = gzActiveChart
    gzActiveChart = nil

    -- |[Gun Accessories Chart]|
    --These items can be used by gun-using characters.
    gzActiveChart = {}

    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22      | Stats Array
    fnAdd("Integrated Gunsight",               250,          0,     false, gciAccessoryCode, "ItemScope",     "Atk|10 Acc|10")
    fnAdd("Tungsten Suppressor",               250,          0,     false, gciAccessoryCode, "ItemSuppressor","Acc|10 Evd|10")

    -- |[Descriptions]|
    fnSetDescription("Integrated Gunsight", "A sight that uses laser pulses to transmit telemetry from the weapon to its user's processor. Increases accuracy\nand damage.")
    fnSetDescription("Tungsten Suppressor", "A noise-diffusion attachment that enhances conductivity in pulse and fission weapons, or reduces vibration in\nballistic weapons. Requires manual operation but, for a machine-girl, that is hardly an issue.")
    
    -- |[Store]|
    --Store the chart under a unique name.
    gzGunUserChart = gzActiveChart
    gzActiveChart = nil

end

-- |[ ==================================== Item Is On Chart ==================================== ]|
--If the item is on the prefabricated chart, we can create it algorithmically.
for i = 1, #gzAccessoryChart, 1 do
    if(gzAccessoryChart[i].sName == gsItemName) then

        --Quick-access copy.
        local zItem = gzAccessoryChart[i]

        --Create item.
        AdInv_CreateItem(gsItemName)
        
            -- |[System]|
            AdItem_SetProperty("Value", zItem.iValue)
            AdItem_SetProperty("Gem Slots", zItem.iGemSlots)
            AdItem_SetProperty("Is Key Item", zItem.bIsKeyItem)
            
            -- |[Display]|
            AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/" .. zItem.sSymbol22)
            AdItem_SetProperty("Description", zItem.sDescription)
            AdItem_SetProperty("Type", "Accessory")
            
            -- |[Usability]|
            --Accessories make use of the usability array.
            AdItem_SetProperty("Is Equipment", true)
            AdItem_SetProperty("Add Equippable Slot", "Accessory A")
            AdItem_SetProperty("Add Equippable Slot", "Accessory B")
            
            for i = 1, #zItem.saUsabilityArray, 1 do
                AdItem_SetProperty("Add Equippable Character", zItem.saUsabilityArray[i])
            end
            
            -- |[Modded Usability]|
            --Execute this script with the name of the item in question. Modded characters may be able to use base-game items.
            fnExecModScript("Items/Usability Exec.lua")
            
            -- |[Stat Array]|
            for p = 1, #zItem.iaStatArray, 2 do
                AdItem_SetProperty("Statistic", zItem.iaStatArray[p+0], zItem.iaStatArray[p+1])
            end
            
            -- |[Advanced Description]|
            fnStandardizeDescription(gcs_UI_DescriptionStd_Accessory, gci_UI_AdItem_Max_Description_Lines)
        DL_PopActiveObject()

        --Finish up.
        gbFoundItem = true
        return
    end
end

--Gun-user chart.
for i = 1, #gzGunUserChart, 1 do
    if(gzGunUserChart[i].sName == gsItemName) then

        --Quick-access copy.
        local zItem = gzGunUserChart[i]

        --Create item.
        AdInv_CreateItem(gsItemName)
        
            -- |[System]|
            AdItem_SetProperty("Value", zItem.iValue)
            AdItem_SetProperty("Gem Slots", zItem.iGemSlots)
            AdItem_SetProperty("Is Key Item", zItem.bIsKeyItem)
            
            -- |[Display]|
            AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/" .. zItem.sSymbol22)
            AdItem_SetProperty("Description", zItem.sDescription)
            AdItem_SetProperty("Type", "Accessory")
            
            -- |[Usability]|
            --Light armors can be used by nearly every character in the game.
            AdItem_SetProperty("Is Equipment", true)
            AdItem_SetProperty("Add Equippable Slot", "Accessory A")
            AdItem_SetProperty("Add Equippable Slot", "Accessory B")
            AdItem_SetProperty("Add Equippable Character", "Tiffany")
            AdItem_SetProperty("Add Equippable Character", "SX-399")
            AdItem_SetProperty("Add Equippable Character", "Sanya")
            
            -- |[Stat Array]|
            for p = 1, #zItem.iaStatArray, 2 do
                AdItem_SetProperty("Statistic", zItem.iaStatArray[p+0], zItem.iaStatArray[p+1])
            end
            
            -- |[Advanced Description]|
            fnStandardizeDescription(gcs_UI_DescriptionStd_Accessory, gci_UI_AdItem_Max_Description_Lines)
        DL_PopActiveObject()

        --Finish up.
        gbFoundItem = true
        return
    end
end


-- |[ ====================================== Unique Items ====================================== ]|
--Items that do not fit on the chart are created here. These are items that deviate from the normal
-- creation pattern for some reason.

--None yet.