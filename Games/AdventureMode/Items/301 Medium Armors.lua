-- |[ ========================== Medium Armor, Character Independent =========================== ]|
--Medium armors, providing at least 5 protection but no INI bonus. Restricted to mesoweights.

-- |[ ========================================= Charts ========================================= ]|
--For equipment which can be made as part of a chart, construct it here.
if(gzMediumArmorChart == nil) then
    
    --Setup. All functions operate on this chart.
    gzActiveChart = {}
    
    --Make aliases for the functions. This shortens the chart for readability.
    local fnAdd            = fnAddItemToChartMarkdown
    local fnSetDescription = fnSetDescriptionOnChart

    -- |[Armors Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22      | Stats Array
    fnAdd("Light Leather Vest",                 75,          1,     false,     gciArmorCode, "ArmGenMedium",  "Prt|Flat|5")
    fnAdd("Plated Leather Vest",               150,          1,     false,     gciArmorCode, "ArmGenMedium",  "Prt|Flat|5 HPMax|Flat|15")

    fnAdd("Ceramic Weave Vest",                 75,          1,     false,     gciArmorCode, "ArmGenMedium",  "Prt|Flat|5")
    fnAdd("Brass Polymer Chestguard",          230,          2,     false,     gciArmorCode, "ArmGenMedium",  "Prt|Flat|7")

    -- |[Descriptions]|
    --Weapons
    fnSetDescription("Light Leather Vest",       "A hand-crafted leather vest that fits comfortably and will stop a hit or two.")
    fnSetDescription("Plated Leather Vest",      "A leather vest, reinforced with metal plates in key areas. Light enough to move in but strong enough to absorb\na blow.")
    fnSetDescription("Ceramic Weave Vest",       "A vest of synthweave infused with ceramics. Increases the weight, but absorbs more impact than a normal vest.")
    fnSetDescription("Brass Polymer Chestguard", "Using technology similar to silksteel but at considerably lower temperatures, the steam droids have produced\na brass combat armor.")

    -- |[Store]|
    --Store the chart under a unique name.
    gzMediumArmorChart = gzActiveChart
    gzActiveChart = nil

end

-- |[ ==================================== Item Is On Chart ==================================== ]|
--If the item is on the prefabricated chart, we can create it algorithmically.
for i = 1, #gzMediumArmorChart, 1 do
    if(gzMediumArmorChart[i].sName == gsItemName) then

        --Quick-access copy.
        local zItem = gzMediumArmorChart[i]

        --Create item.
        AdInv_CreateItem(gsItemName)
        
            -- |[System]|
            AdItem_SetProperty("Value", zItem.iValue)
            AdItem_SetProperty("Gem Slots", zItem.iGemSlots)
            AdItem_SetProperty("Is Key Item", zItem.bIsKeyItem)
            
            -- |[Display]|
            AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/" .. zItem.sSymbol22)
            AdItem_SetProperty("Description", zItem.sDescription)
            AdItem_SetProperty("Type", "Medium Armor")
            
            -- |[Usability]|
            --Medium armors cannot be used by fragile characters.
            AdItem_SetProperty("Is Equipment", true)
            AdItem_SetProperty("Add Equippable Slot", "Body")
            AdItem_SetProperty("Add Equippable Character", "Mei")
            AdItem_SetProperty("Add Equippable Character", "Christine")
            AdItem_SetProperty("Add Equippable Character", "SX-399")
            AdItem_SetProperty("Add Equippable Character", "Sanya")
            AdItem_SetProperty("Add Equippable Character", "Empress")
            
            -- |[Modded Usability]|
            --Execute this script with the name of the item in question. Modded characters may be able to use base-game items.
            fnExecModScript("Items/Usability Exec.lua")
            
            -- |[Stat Array]|
            for p = 1, #zItem.iaStatArray, 2 do
                AdItem_SetProperty("Statistic", zItem.iaStatArray[p+0], zItem.iaStatArray[p+1])
            end
            
            -- |[Advanced Description]|
            fnStandardizeDescription(gcs_UI_DescriptionStd_Armor, gci_UI_AdItem_Max_Description_Lines)
        DL_PopActiveObject()

        --Finish up.
        gbFoundItem = true
        return
    end
end

-- |[ ====================================== Unique Items ====================================== ]|
--Items that do not fit on the chart are created here. These are items that deviate from the normal
-- creation pattern for some reason.

--No items yet.
