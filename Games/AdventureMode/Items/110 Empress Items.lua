-- |[ =================================== Items for Empress ==================================== ]|
--Empress' weapons and armor.

-- |[ ========================================= Charts ========================================= ]|
--For equipment which can be made as part of a chart, construct it here.
if(gzEmpressItemsChart == nil) then
    
    --Setup. All functions operate on this chart.
    gzActiveChart = {}
    
    --Make aliases for the functions. This shortens the chart for readability.
    local fnAdd            = fnAddItemToChartMarkdown
    local fnAddWpn         = fnAddWeaponToChartMarkdown
    local fnAddAbility     = fnAddAbilityToChart
    local fnSetDescription = fnSetDescriptionOnChart

    -- |[Weapons Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22             | Damage Type           | Stats Array
    fnAddWpn("Ancient Gauntlets",              100,          1,     false,    gciWeaponCode,           "WepEmpress", gciDamageType_Slashing, "Atk|25") --Tier 0
    fnAddWpn("Bruiser Gauntlets",              300,          1,     false,    gciWeaponCode,           "WepEmpress", gciDamageType_Slashing, "Atk|45") --Tier 1
    fnAddWpn("Bruiser Gauntlets +",            500,          1,     false,    gciWeaponCode,           "WepEmpress", gciDamageType_Slashing, "Atk|62")
    fnAddWpn("Striker Gauntlets",              600,          2,     false,    gciWeaponCode,           "WepEmpress", gciDamageType_Slashing, "Atk|68 Acc|25 Evd|10") --Tier 2
    fnAddWpn("Striker Gauntlets +",           1200,          2,     false,    gciWeaponCode,           "WepEmpress", gciDamageType_Slashing, "Atk|90 Acc|25 Evd|10")
    fnAddWpn("Grappler Gauntlets",            1300,          2,     false,    gciWeaponCode,           "WepEmpress", gciDamageType_Slashing, "Atk|112 Acc|25 Evd|10") --Tier 3
    fnAddWpn("Grappler Gauntlets +",          7000,          2,     false,    gciWeaponCode,           "WepEmpress", gciDamageType_Slashing, "Atk|135 Acc|25 Evd|10")
    fnAddWpn("Conquerer's Gauntlets",        10000,          3,     false,    gciWeaponCode,           "WepEmpress", gciDamageType_Slashing, "Atk|150 Acc|35 Evd|10") --Tier 4
    fnAddWpn("Conquerer's Gauntlets +",      30000,          3,     false,    gciWeaponCode,           "WepEmpress", gciDamageType_Slashing, "Atk|250 Acc|35 Evd|15")

    -- |[Armors Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22      | Stats Array
    fnAdd("Ancient Cuirass",                   200,          1,     false,     gciArmorCode,  "ArmorEmpress", "Prt|Flat|9 Ini|Flat|-20") --Tier 1
    fnAdd("Ancient Cuirass +",                 400,          1,     false,     gciArmorCode,  "ArmorEmpress", "Prt|Flat|10 Ini|Flat|-20 Atk|5")
    
    fnAdd("Bronze Armor",                      500,          1,     false,     gciArmorCode,  "ArmorEmpress", "Prt|Flat|11 Ini|Flat|-20 Atk|10")
    fnAdd("Bronze Armor +",                    900,          1,     false,     gciArmorCode,  "ArmorEmpress", "Prt|Flat|12 Ini|Flat|-20 Atk|17")
    
    fnAdd("Steel Armor",                      1000,          2,     false,     gciArmorCode,  "ArmorEmpress", "Prt|Flat|13 Ini|Flat|-20 Atk|20")
    fnAdd("Steel Armor +",                    3000,          2,     false,     gciArmorCode,  "ArmorEmpress", "Prt|Flat|14 Ini|Flat|-20 Atk|30")
    
    fnAdd("Imperial Plate",                   4000,          3,     false,     gciArmorCode,  "ArmorEmpress", "Prt|Flat|16 Ini|Flat|-20 Atk|45")
    fnAdd("Imperial Plate +",                 8000,          3,     false,     gciArmorCode,  "ArmorEmpress", "Prt|Flat|18 Ini|Flat|-20 Atk|60")

    -- |[Accessories Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22      | Stats Array
    --fnAdd("Silk Pouch",                         50,          2,     false, gciAccessoryCode,    "JarGreen", {gciStatIndex_Initiative, 20})

    -- |[Combat Items Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22      | Stats Array

    -- |[Descriptions]|
    --Weapons
    fnSetDescription("Ancient Gauntlets",       "Gauntlets forged in the long-lost dragon empire of Arulente. Empress prefers to beat her enemies with her[BR]clawed hands.[BR]Deals slashing damage.")
    fnSetDescription("Bruiser Gauntlets",       "Forged warrior gauntlets designed for large hands.[BR]Deals slashing damage.")
    fnSetDescription("Bruiser Gauntlets +",     "Forged warrior gauntlets designed for large hands.[BR]Enhanced by Miso's alchemical wizardry.[BR]Deals slashing damage.")
    fnSetDescription("Striker Gauntlets",       "Shaped specifically by ancient engineers to help your hands deliver maximum force to an idiot's face.[BR]Deals slashing damage.")
    fnSetDescription("Striker Gauntlets +",     "Shaped specifically by ancient engineers to help your hands deliver maximum force to an idiot's face.[BR]Enhanced by Miso's alchemical wizardry.[BR]Deals slashing damage.")
    fnSetDescription("Grappler Gauntlets",      "Gauntlets with extra grips. You can throw people without them, but do you want to?[BR]Deals slashing damage.")
    fnSetDescription("Grappler Gauntlets +",    "Gauntlets with extra grips. You can throw people without them, but do you want to?[BR]Enhanced by Miso's alchemical wizardry.[BR]Deals slashing damage.")
    fnSetDescription("Conquerer's Gauntlets",   "Reforged bronze gauntlets once worn by the Dragon Empress herself. She's impressed they still fit![BR]Deals slashing damage.")
    fnSetDescription("Conquerer's Gauntlets +", "Reforged bronze gauntlets once worn by the Dragon Empress herself. She's impressed they still fit![BR]Enhanced by Miso's alchemical wizardry.[BR]Deals slashing damage.")
    
    --Armors
    fnSetDescription("Ancient Cuirass",   "A cuirass of ancient dragon bronze, lacquered to prevent rust. Emphasizes Empress' Excellent physique.")
    fnSetDescription("Ancient Cuirass +", "A cuirass of ancient dragon bronze, lacquered to prevent rust. Emphasizes Empress' Excellent physique.[BR]Enhanced by Miso's alchemical wizardry.")
    fnSetDescription("Bronze Armor",      "Reforged bronze armor, it brings back a nostalgic feeling for the time before steel.")
    fnSetDescription("Bronze Armor +",    "Reforged bronze armor, it brings back a nostalgic feeling for the time before steel.[BR]Enhanced by Miso's alchemical wizardry.")
    fnSetDescription("Steel Armor",       "Heavy plate armor meant to be used by the biggest, toughest, and strongest.")
    fnSetDescription("Steel Armor +",     "Heavy plate armor meant to be used by the biggest, toughest, and strongest.[BR]Enhanced by Miso's alchemical wizardry.")
    fnSetDescription("Imperial Plate",    "Platemail in the classic blue imperial style. Similar to the old style Empress once wore, but does not show off her abs as much.")
    fnSetDescription("Imperial Plate +",  "Platemail in the classic blue imperial style. Similar to the old style Empress once wore, but does not show off her abs as much.[BR]Enhanced by Miso's alchemical wizardry.")
    
    --Accessories
    --fnSetDescription("Silk Pouch", "A pouch made of fine silk. Contains magical ingredients for Izuna's spells. Increases initiative.")

    --Combat Items

    -- |[Store]|
    --Store the chart under a unique name.
    gzEmpressItemsChart = gzActiveChart
    gzActiveChart = nil

end

-- |[ ==================================== Item Is On Chart ==================================== ]|
--If the item is on the prefabricated chart, we can create it algorithmically.
for i = 1, #gzEmpressItemsChart, 1 do
    if(gzEmpressItemsChart[i].sName == gsItemName) then

        --Quick-access copy.
        local zItem = gzEmpressItemsChart[i]

        --Create item.
        AdInv_CreateItem(gsItemName)
        
            -- |[System]|
            AdItem_SetProperty("Value", zItem.iValue)
            AdItem_SetProperty("Gem Slots", zItem.iGemSlots)
            AdItem_SetProperty("Is Key Item", zItem.bIsKeyItem)
            
            -- |[Display]|
            AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/" .. zItem.sSymbol22)
            AdItem_SetProperty("Description", zItem.sDescription)
            -- |[Usability]|
            AdItem_SetProperty("Is Equipment", true)
            AdItem_SetProperty("Add Equippable Character", "Empress")
            
            -- |[Stat Array]|
            for p = 1, #zItem.iaStatArray, 2 do
                AdItem_SetProperty("Statistic", zItem.iaStatArray[p+0], zItem.iaStatArray[p+1])
            end
            
            -- |[Usability / Description]|
            --Weapons.
            if(zItem.iUsability == gciWeaponCode) then
                
                --Type.
                AdItem_SetProperty("Type", "Gauntlets")
            
                --Equipment slots. 
                AdItem_SetProperty("Add Equippable Slot", "Weapon")
                AdItem_SetProperty("Add Equippable Slot", "Weapon Backup A")
                AdItem_SetProperty("Add Equippable Slot", "Weapon Backup B")
                
                --Animations and Sound
                AdItem_SetProperty("Equipment Attack Animation", "Claw Slash")
                AdItem_SetProperty("Equipment Attack Sound",     "Combat\\|Impact_Slash")
                AdItem_SetProperty("Equipment Critical Sound",   "Combat\\|Impact_Slash_Crit")
                
                --Damage type.
                AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, zItem.iDamageType + gciDamageOffsetToResistances, 1.0)
                AdItem_SetProperty("Add Tag", zItem.sSecondaryType, 1)
                fnStandardizeDescription(gcs_UI_DescriptionStd_Weapon, gci_UI_AdItem_Max_Description_Lines)
            
            --Armors:
            elseif(zItem.iUsability == gciArmorCode) then
                AdItem_SetProperty("Type", "Armor (Empress)")
                AdItem_SetProperty("Add Equippable Slot", "Body")
                
                --Description.
                fnStandardizeDescription(gcs_UI_DescriptionStd_Armor, gci_UI_AdItem_Max_Description_Lines)
            
            --Accessories:
            elseif(zItem.iUsability == gciAccessoryCode) then
                AdItem_SetProperty("Type", "Accessory (Empress)")
                AdItem_SetProperty("Add Equippable Slot", "Accessory A")
                AdItem_SetProperty("Add Equippable Slot", "Accessory B")
                fnStandardizeDescription(gcs_UI_DescriptionStd_Accessory, gci_UI_AdItem_Max_Description_Lines)
            
            --Combat Items:
            elseif(zItem.iUsability == gciItemCode) then
                AdItem_SetProperty("Type", "Combat Item (Empress)")
                AdItem_SetProperty("Add Equippable Slot", "Item A")
                AdItem_SetProperty("Add Equippable Slot", "Item B")
                AdItem_SetProperty("Ability Path", zItem.sAbilityPath)
                fnStandardizeDescription(gcs_UI_DescriptionStd_CombatItem, gci_UI_AdItem_Max_Description_Lines)
            
            end
        DL_PopActiveObject()

        --Finish up.
        gbFoundItem = true
        return
    end
end

-- |[ ====================================== Unique Items ====================================== ]|
--Items that do not fit on the chart are created here. These are items that deviate from the normal
-- creation pattern for some reason.

--No items yet.
