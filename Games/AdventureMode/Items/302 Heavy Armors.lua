-- |[ ========================== Heavy Armor, Character Independent =========================== ]|
--Heavy armors, which can only be used by a few characters and provide an INI penalty.

-- |[ ========================================= Charts ========================================= ]|
--For equipment which can be made as part of a chart, construct it here.
if(gzHeavyArmorChart == nil) then
    
    --Setup. All functions operate on this chart.
    gzActiveChart = {}
    
    --Make aliases for the functions. This shortens the chart for readability.
    local fnAdd            = fnAddItemToChartMarkdown
    local fnSetDescription = fnSetDescriptionOnChart

    -- |[Armors Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22      | Stats Array
    fnAdd("Steel Half-Plate",                  300,          2,     false,     gciArmorCode, "ArmGenHeavy",  "Prt|Flat|10 Ini|Flat|-20")
    fnAdd("Jawell Platemail",                  300,          2,     false,     gciArmorCode, "ArmGenHeavy",  "Prt|Flat|15 Ini|Flat|-20")

    fnAdd("Neutronium Jacket",                 300,          2,     false,     gciArmorCode, "ArmGenHeavy",  "Prt|Flat|13 Ini|Flat|-20")
    fnAdd("Titanweave Vest",                   700,          2,     false,     gciArmorCode, "ArmGenHeavy",  "Prt|Flat|13 Ini|Flat|-25 HPMax|Flat|50")

    -- |[Descriptions]|
    --Weapons
    fnSetDescription("Steel Half-Plate", "Conventional steel platemail, covering the necessities. Big, slow, heavy, but very protective.")
    fnSetDescription("Jawell Platemail", "Woven steel platemail, made using a technique from the Jawell region of the old Arulente empire. Still in use to\nthis day.")
    fnSetDescription("Neutronium Jacket", "An armored jacket of sorts, composed of quantum-stasis neutronium within carbon fiber. While less than one\nnanolitre of neutronium is actually present, the armor is extremely dense and provides excellent protection.")
    fnSetDescription("Titanweave Vest", "A crystalline vest woven with silksteel embedded with quartz and titanium. Heavy but absurdly strong.")

    -- |[Store]|
    --Store the chart under a unique name.
    gzHeavyArmorChart = gzActiveChart
    gzActiveChart = nil

end

-- |[ ==================================== Item Is On Chart ==================================== ]|
--If the item is on the prefabricated chart, we can create it algorithmically.
for i = 1, #gzHeavyArmorChart, 1 do
    if(gzHeavyArmorChart[i].sName == gsItemName) then

        --Quick-access copy.
        local zItem = gzHeavyArmorChart[i]

        --Create item.
        AdInv_CreateItem(gsItemName)
        
            -- |[System]|
            AdItem_SetProperty("Value", zItem.iValue)
            AdItem_SetProperty("Gem Slots", zItem.iGemSlots)
            AdItem_SetProperty("Is Key Item", zItem.bIsKeyItem)
            
            -- |[Display]|
            AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/" .. zItem.sSymbol22)
            AdItem_SetProperty("Description", zItem.sDescription)
            AdItem_SetProperty("Type", "Heavy Armor")
            
            -- |[Usability]|
            --Heavy armor has a highly restricted equipment requirement.
            AdItem_SetProperty("Is Equipment", true)
            AdItem_SetProperty("Add Equippable Slot", "Body")
            AdItem_SetProperty("Add Equippable Character", "Christine")
            AdItem_SetProperty("Add Equippable Character", "Empress")
            
            -- |[Modded Usability]|
            --Execute this script with the name of the item in question. Modded characters may be able to use base-game items.
            fnExecModScript("Items/Usability Exec.lua")
            
            -- |[Stat Array]|
            for p = 1, #zItem.iaStatArray, 2 do
                AdItem_SetProperty("Statistic", zItem.iaStatArray[p+0], zItem.iaStatArray[p+1])
            end
            
            -- |[Advanced Description]|
            fnStandardizeDescription(gcs_UI_DescriptionStd_Armor, gci_UI_AdItem_Max_Description_Lines)
        DL_PopActiveObject()

        --Finish up.
        gbFoundItem = true
        return
    end
end

-- |[ ====================================== Unique Items ====================================== ]|
--Items that do not fit on the chart are created here. These are items that deviate from the normal
-- creation pattern for some reason.

--No items yet.
