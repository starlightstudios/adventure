-- |[ ===================================== Items for Izuna ==================================== ]|
--Izuna's weapons and armor.

-- |[ ========================================= Charts ========================================= ]|
--For equipment which can be made as part of a chart, construct it here.
if(gzIzunaItemsChart == nil) then
    
    --Setup. All functions operate on this chart.
    gzActiveChart = {}
    
    --Make aliases for the functions. This shortens the chart for readability.
    local fnAdd            = fnAddItemToChartMarkdown
    local fnAddWpn         = fnAddWeaponToChartMarkdown
    local fnAddAbility     = fnAddAbilityToChart
    local fnSetDescription = fnSetDescriptionOnChart

    -- |[Weapons Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22             | Damage Type           | Stats Array
    fnAddWpn("Silver Straightsword",           100,          2,     false,    gciWeaponCode,       "WepIzunaSilver", gciDamageType_Slashing, "Atk|10") --Tier 0
    
    fnAddWpn("Argentum Straightsword",         300,          2,     false,    gciWeaponCode,       "WepIzunaSilver", gciDamageType_Slashing, "Atk|25") --Tier 1
    fnAddWpn("Argentum Straightsword +",       500,          2,     false,    gciWeaponCode,       "WepIzunaSilver", gciDamageType_Slashing, "Atk|30 Evd|5 Ini|5")
    
    fnAddWpn("Electrum Straightsword",         600,          2,     false,    gciWeaponCode,       "WepIzunaSilver", gciDamageType_Slashing, "Atk|35 Evd|15 Ini|15") --Tier 2
    fnAddWpn("Electrum Straightsword +",      1200,          2,     false,    gciWeaponCode,       "WepIzunaSilver", gciDamageType_Slashing, "Atk|60 Evd|10 Ini|10")
    
    fnAddWpn("Goddess' Whisker",              1300,          1,     false,    gciWeaponCode,       "WepIzunaSilver", gciDamageType_Slashing, "Atk|65 Evd|10 Ini|10") --Tier 3
    fnAddWpn("Goddess' Whisker +",            7000,          1,     false,    gciWeaponCode,       "WepIzunaSilver", gciDamageType_Slashing, "Atk|70 Evd|15 Ini|15")
    
    fnAddWpn("Terasu's Quill",               10000,          3,     false,    gciWeaponCode,       "WepIzunaSilver", gciDamageType_Slashing, "Atk|70 Evd|25 Ini|20") --Tier 4
    fnAddWpn("Terasu's Quill +",             30000,          3,     false,    gciWeaponCode,       "WepIzunaSilver", gciDamageType_Slashing, "Atk|100 Evd|35 Ini|30")

    -- |[Armors Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22      | Stats Array
    fnAdd("Miko Robes",                        100,          1,     false,     gciArmorCode,    "ArmorIzuna", "Prt|1 Ini|20") --Tier 0
    
    fnAdd("Infused Miko Robes",                300,          2,     false,     gciArmorCode,    "ArmorIzuna", "Prt|3 Ini|20 Atk|10") --Tier 1
    fnAdd("Infused Miko Robes +",              500,          2,     false,     gciArmorCode,    "ArmorIzuna", "Prt|4 Ini|20 Atk|10")
    
    fnAdd("Warrior's Robes",                   600,          2,     false,     gciArmorCode,    "ArmorIzuna", "Prt|4 Ini|20 Atk|15 Evd|10") --Tier 2
    fnAdd("Warrior's Robes +",                 900,          2,     false,     gciArmorCode,    "ArmorIzuna", "Prt|4 Ini|20 Atk|20 Evd|10")
    
    fnAdd("Saint's Robes",                    1000,          2,     false,     gciArmorCode,    "ArmorIzuna", "Prt|4 Ini|20 Atk|20 Evd|14") --Tier 3
    fnAdd("Saint's Robes +",                  2000,          2,     false,     gciArmorCode,    "ArmorIzuna", "Prt|5 Ini|20 Atk|30 Evd|14")
    
    fnAdd("Great Sage Robes",                 3000,          3,     false,     gciArmorCode,    "ArmorIzuna", "Prt|6 Ini|20 Atk|30 Evd|20") --Tier 4
    fnAdd("Great Sage Robes +",               6000,          3,     false,     gciArmorCode,    "ArmorIzuna", "Prt|6 Ini|20 Atk|60 Evd|30")

    -- |[Accessories Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22      | Stats Array
    fnAdd("Silk Pouch",                         50,          2,     false, gciAccessoryCode,      "JarGreen", "Ini|Flat|20")

    -- |[Combat Items Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22      | Stats Array

    -- |[Descriptions]|
    --Weapons
    fnSetDescription("Silver Straightsword",     "Not a fighting sword, the blade has a core of silver surrounded by steel. It works to focus the user's mana.[BR]Deals slashing damage.")
    fnSetDescription("Argentum Straightsword",   "A focusing sword made of argentum, a silver/steel alloy. It helps focus the user's mana.[BR]Deals slashing damage.")
    fnSetDescription("Argentum Straightsword +", "A focusing sword made of argentum, a silver/steel alloy. It helps focus the user's mana.[BR]Enhanced by Miso's alchemical wizardry.[BR]Deals slashing damage.")
    fnSetDescription("Electrum Straightsword",   "A focusing sword made of electrum, a silver/gold alloy. Focuses mana and improves agility.[BR]Deals slashing damage.")
    fnSetDescription("Electrum Straightsword +", "A focusing sword made of electrum, a silver/gold alloy. Focuses mana and improves agility.[BR]Enhanced by Miso's alchemical wizardry.[BR]Deals slashing damage.")
    fnSetDescription("Goddess' Whisker",         "One of the silver focusing swords made in the old empire for magisters.[BR]A standardized blade, its power has not waned.[BR]Deals slashing damage.")
    fnSetDescription("Goddess' Whisker +",       "One of the silver focusing swords made in the old empire for magisters.[BR]A standardized blade, its power has not waned.[BR]Enhanced by Miso's alchemical wizardry."..
                                                 "[BR]Deals slashing damage.")
    fnSetDescription("Terasu's Quill",           "The name given to a sword said to be made in the image of the Fox Goddess' pet hedgehog. It glows with power.[BR]Deals slashing damage.")
    fnSetDescription("Terasu's Quill +",         "The name given to a sword said to be made in the image of the Fox Goddess' pet hedgehog. It glows with power.[BR]Enhanced by Miso's alchemical wizardry."..
                                                 "[BR]Deals slashing damage.")
    
    --Armors
    fnSetDescription("Miko Robes",           "Everyday travel and work clothes for the kitsune mikos. The robes are warm, loose fitting, and very comfortable.[BR]They are not combat attire.")
    fnSetDescription("Infused Miko Robes",   "Miko robes lined with gossamer-infused leather. Provides protection and helps the user focus their mana.")
    fnSetDescription("Infused Miko Robes +", "Miko robes lined with gossamer-infused leather. Provides protection and helps the user focus their mana.[BR]Enhanced by Miso's alchemical wizardry.")
    fnSetDescription("Warrior's Robes",      "Robes often used by wandering kitsune warriors.")
    fnSetDescription("Warrior's Robes",      "Robes often used by wandering kitsune warriors.[BR]Enhanced by Miso's alchemical wizardry.")
    fnSetDescription("Saint's Robes",        "Meant to be used when a miko is tending the sick, the miraculous healing they do often has the public refer to the mikos as 'Saints'.")
    fnSetDescription("Saint's Robes +",      "Meant to be used when a miko is tending the sick, the miraculous healing they do often has the public refer to the mikos as 'Saints'.[BR]Enhanced by Miso's alchemical wizardry.")
    fnSetDescription("Great Sage Robes",     "Simple robes worn by a humble sage, yet still overflowing with magical power.")
    fnSetDescription("Great Sage Robes +",   "Simple robes worn by a humble sage, yet still overflowing with magical power.[BR]Enhanced by Miso's alchemical wizardry.")
    
    --Accessories
    fnSetDescription("Silk Pouch", "A pouch made of fine silk. Contains magical ingredients for Izuna's spells. Increases initiative.")

    --Combat Items

    -- |[Store]|
    --Store the chart under a unique name.
    gzIzunaItemsChart = gzActiveChart
    gzActiveChart = nil

end

-- |[ ==================================== Item Is On Chart ==================================== ]|
--If the item is on the prefabricated chart, we can create it algorithmically.
for i = 1, #gzIzunaItemsChart, 1 do
    if(gzIzunaItemsChart[i].sName == gsItemName) then

        --Quick-access copy.
        local zItem = gzIzunaItemsChart[i]

        --Create item.
        AdInv_CreateItem(gsItemName)
        
            -- |[System]|
            AdItem_SetProperty("Value", zItem.iValue)
            AdItem_SetProperty("Gem Slots", zItem.iGemSlots)
            AdItem_SetProperty("Is Key Item", zItem.bIsKeyItem)
            
            -- |[Display]|
            AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/" .. zItem.sSymbol22)
            AdItem_SetProperty("Description", zItem.sDescription)
            -- |[Usability]|
            AdItem_SetProperty("Is Equipment", true)
            AdItem_SetProperty("Add Equippable Character", "Izuna")
            
            -- |[Stat Array]|
            for p = 1, #zItem.iaStatArray, 2 do
                AdItem_SetProperty("Statistic", zItem.iaStatArray[p+0], zItem.iaStatArray[p+1])
            end
            
            -- |[Usability / Description]|
            --Weapons.
            if(zItem.iUsability == gciWeaponCode) then
                
                --Type.
                AdItem_SetProperty("Type", "Focusing Sword")
            
                --Equipment slots. 
                AdItem_SetProperty("Add Equippable Slot", "Weapon")
                AdItem_SetProperty("Add Equippable Slot", "Weapon Backup A")
                AdItem_SetProperty("Add Equippable Slot", "Weapon Backup B")
                
                --Animations and Sound
                AdItem_SetProperty("Equipment Attack Animation", "Sword Slash")
                AdItem_SetProperty("Equipment Attack Sound",     "Combat\\|Impact_CrossSlash")
                AdItem_SetProperty("Equipment Critical Sound",   "Combat\\|Impact_CrossSlash_Crit")
                
                --Damage type.
                AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, zItem.iDamageType + gciDamageOffsetToResistances, 1.0)
                AdItem_SetProperty("Add Tag", zItem.sSecondaryType, 1)
                fnStandardizeDescription(gcs_UI_DescriptionStd_Weapon, gci_UI_AdItem_Max_Description_Lines)
            
            --Armors:
            elseif(zItem.iUsability == gciArmorCode) then
                AdItem_SetProperty("Type", "Armor (Izuna)")
                AdItem_SetProperty("Add Equippable Slot", "Body")
                
                --Description.
                fnStandardizeDescription(gcs_UI_DescriptionStd_Armor, gci_UI_AdItem_Max_Description_Lines)
            
            --Accessories:
            elseif(zItem.iUsability == gciAccessoryCode) then
                AdItem_SetProperty("Type", "Accessory (Izuna)")
                AdItem_SetProperty("Add Equippable Slot", "Accessory A")
                AdItem_SetProperty("Add Equippable Slot", "Accessory B")
                fnStandardizeDescription(gcs_UI_DescriptionStd_Accessory, gci_UI_AdItem_Max_Description_Lines)
            
            --Combat Items:
            elseif(zItem.iUsability == gciItemCode) then
                AdItem_SetProperty("Type", "Combat Item (Izuna)")
                AdItem_SetProperty("Add Equippable Slot", "Item A")
                AdItem_SetProperty("Add Equippable Slot", "Item B")
                AdItem_SetProperty("Ability Path", zItem.sAbilityPath)
                fnStandardizeDescription(gcs_UI_DescriptionStd_CombatItem, gci_UI_AdItem_Max_Description_Lines)
            
            end
        DL_PopActiveObject()

        --Finish up.
        gbFoundItem = true
        return
    end
end

-- |[ ====================================== Unique Items ====================================== ]|
--Items that do not fit on the chart are created here. These are items that deviate from the normal
-- creation pattern for some reason.

--No items yet.
