-- |[ ========================== Character Independent Offense Items =========================== ]|
--Items that can be used by anyone and that damage or debuff enemies.

-- |[ ========================================= Charts ========================================= ]|
--For equipment which can be made as part of a chart, construct it here.
if(gzOffenseItemChart == nil or gzOffenseGunItemChart == nil) then
    
    --Setup. All functions operate on this chart.
    gzActiveChart = {}
    
    --Make aliases for the functions. This shortens the chart for readability.
    local fnAdd            = fnAddItemToChart
    local fnAddAbility     = fnAddAbilityToChart
    local fnSetDescription = fnSetDescriptionOnChart

    -- |[ ===== Combat Items Chart ===== ]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22      | Stats Array
    fnAdd("Poison Spore Cap",                  100,          0,     false,      gciItemCode, "PsnShroom",     {})
    fnAdd("Firebomb",                          100,          0,     false,      gciItemCode, "Molotov",       {})
    fnAdd("Lamp Oil",                           50,          0,     false,      gciItemCode, "Molotov",       {})
    fnAdd("Pitch Firebomb",                     50,          0,     false,      gciItemCode, "Molotov",       {})
    fnAdd("Clothing Patch",                     50,          0,     false,      gciItemCode, "Paint",         {})
    fnAdd("Magnesium Grenade",                 180,          0,     false,      gciItemCode, "ItemFlashbang", {})

    -- |[Abilities]|
    local sItemAbilityPath = gsRoot .. "Combat/Party/Items/Abilities/"
    fnAddAbility("Poison Spore Cap",  sItemAbilityPath .. "Poison Spore Cap.lua")
    fnAddAbility("Firebomb",          sItemAbilityPath .. "Firebomb.lua")
    fnAddAbility("Lamp Oil",          sItemAbilityPath .. "Lamp Oil.lua")
    fnAddAbility("Pitch Firebomb",    sItemAbilityPath .. "Pitch Firebomb.lua")
    fnAddAbility("Clothing Patch",    sItemAbilityPath .. "Clothing Patch.lua")
    fnAddAbility("Magnesium Grenade", sItemAbilityPath .. "Magnesium Grenade.lua")

    -- |[Descriptions]|
    fnSetDescription("Poison Spore Cap",  "Some sort of mushroom that is very poisonous.[BR]Waving it at people will poison them for 0.51x[ICOATK] over 3 turns.")
    fnSetDescription("Firebomb",          "A small on-contact accelerant package using alcohol to ignite the victim on fire.[BR]Deals 1.00x[ICOATK] as fire damage.")
    fnSetDescription("Lamp Oil",          "A phial of lamp oil, thrown at the enemy to cover them. Makes them vulnerable to fire damage.")
    fnSetDescription("Pitch Firebomb",    "A small on-contact accelerant package using resin to ignite the victim on fire.[BR]Deals 1.20x[ICOATK] as fire damage.")
    fnSetDescription("Clothing Patch",    "Enchanted metal patch that clones itself and reinforces armor in the field, and then vanishes.[BR]Increases protection by 10 and decreases Ini and Evd by 10 for 3 turns.")
    fnSetDescription("Magnesium Grenade", "A blasting charge attached to a core of magnesium, which burns quickly and at extreme heat. Blinds all enemies.[BR]"..
                                          "[IcoEffectHostile][IcoStr7][IcoCrusading]. -30[IcoAccuracy]. 3[IcoClock]. All Enemies.")

    -- |[Store]|
    --Store the chart under a unique name.
    gzOffenseItemChart = gzActiveChart
    gzActiveChart = nil

    -- |[ ===== Gun-User Items Chart ===== ]|
    --These items can only be used by gun-using characters.
    gzActiveChart = {}
    
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22      | Stats Array
    fnAdd("Recoil Dampener",                   100,          0,     false,      gciItemCode, "Paint",         {})

    -- |[Abilities]|
    fnAddAbility("Recoil Dampener", sItemAbilityPath .. "Recoil Dampener.lua")

    -- |[Descriptions]|
    fnSetDescription("Recoil Dampener", "A memory-gel that can be attached to a long-barrel weapon. When activated, absorbs all recoil and increases[BR]damage for the next attack.")

    -- |[Store]|
    --Store the chart under a unique name.
    gzOffenseGunItemChart = gzActiveChart
    gzActiveChart = nil

end

-- |[ ==================================== Item Is On Chart ==================================== ]|
--If the item is on the prefabricated chart, we can create it algorithmically.
for i = 1, #gzOffenseItemChart, 1 do
    if(gzOffenseItemChart[i].sName == gsItemName) then

        --Quick-access copy.
        local zItem = gzOffenseItemChart[i]

        --Create item.
        AdInv_CreateItem(gsItemName)
        
            -- |[System]|
            AdItem_SetProperty("Value", zItem.iValue)
            AdItem_SetProperty("Gem Slots", zItem.iGemSlots)
            AdItem_SetProperty("Is Key Item", zItem.bIsKeyItem)
            
            -- |[Display]|
            AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/" .. zItem.sSymbol22)
            AdItem_SetProperty("Description", zItem.sDescription)
            AdItem_SetProperty("Type", "Combat Item")
            
            -- |[Usability]|
            AdItem_SetProperty("Is Equipment", true)
            AdItem_SetProperty("Add Equippable Character", "Mei")
            AdItem_SetProperty("Add Equippable Character", "Florentina")
            AdItem_SetProperty("Add Equippable Character", "Christine")
            AdItem_SetProperty("Add Equippable Character", "Tiffany")
            AdItem_SetProperty("Add Equippable Character", "SX-399")
            AdItem_SetProperty("Add Equippable Character", "Sanya")
            AdItem_SetProperty("Add Equippable Character", "Izuna")
            AdItem_SetProperty("Add Equippable Character", "Zeke")
            AdItem_SetProperty("Add Equippable Character", "Empress")
            AdItem_SetProperty("Add Equippable Slot", "Item A")
            AdItem_SetProperty("Add Equippable Slot", "Item B")
            AdItem_SetProperty("Add Equippable Slot", "Item C")
            AdItem_SetProperty("Add Equippable Slot", "Item D")
            AdItem_SetProperty("Ability Path", zItem.sAbilityPath)
            
            -- |[Modded Usability]|
            --Execute this script with the name of the item in question. Modded characters may be able to use base-game items.
            fnExecModScript("Items/Usability Exec.lua")
            
            -- |[Stat Array]|
            for p = 1, #zItem.iaStatArray, 2 do
                AdItem_SetProperty("Statistic", zItem.iaStatArray[p+0], zItem.iaStatArray[p+1])
            end
            
            -- |[Advanced Description]|
            fnStandardizeDescription(gcs_UI_DescriptionStd_Accessory, gci_UI_AdItem_Max_Description_Lines)
        DL_PopActiveObject()

        --Finish up.
        gbFoundItem = true
        return
    end
end

--As above, but for gun-users.
for i = 1, #gzOffenseGunItemChart, 1 do
    if(gzOffenseGunItemChart[i].sName == gsItemName) then

        --Quick-access copy.
        local zItem = gzOffenseGunItemChart[i]

        --Create item.
        AdInv_CreateItem(gsItemName)
        
            -- |[System]|
            AdItem_SetProperty("Value", zItem.iValue)
            AdItem_SetProperty("Gem Slots", zItem.iGemSlots)
            AdItem_SetProperty("Is Key Item", zItem.bIsKeyItem)
            
            -- |[Display]|
            AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/" .. zItem.sSymbol22)
            AdItem_SetProperty("Description", zItem.sDescription)
            AdItem_SetProperty("Type", "Combat Item")
            
            -- |[Usability]|
            AdItem_SetProperty("Is Equipment", true)
            AdItem_SetProperty("Add Equippable Character", "Tiffany")
            AdItem_SetProperty("Add Equippable Character", "SX-399")
            AdItem_SetProperty("Add Equippable Slot", "Item A")
            AdItem_SetProperty("Add Equippable Slot", "Item B")
            AdItem_SetProperty("Ability Path", zItem.sAbilityPath)
            
            -- |[Stat Array]|
            for p = 1, #zItem.iaStatArray, 2 do
                AdItem_SetProperty("Statistic", zItem.iaStatArray[p+0], zItem.iaStatArray[p+1])
            end
            
            -- |[Advanced Description]|
            fnStandardizeDescription(gcs_UI_DescriptionStd_Accessory, gci_UI_AdItem_Max_Description_Lines)
        DL_PopActiveObject()

        --Finish up.
        gbFoundItem = true
        return
    end
end

-- |[ ====================================== Unique Items ====================================== ]|
--Items that do not fit on the chart are created here. These are items that deviate from the normal
-- creation pattern for some reason.

--No items yet.
