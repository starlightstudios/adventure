-- |[ ======================================= Key Items ======================================== ]|
--Items that have no purpose in combat or whatnot, but are often checked by scripts for accessibility.
--Like all item subscripts, uses globals gsItemName and gfBypassAdamantite for efficiency reasons.

-- |[ ======================================= Chapter 1 ======================================== ]|
--Prying bar, used to open doors.
if(gsItemName == "Pry Bar") then
    AdInv_CreateItem(gsItemName)
        AdItem_SetProperty("Description", "A curved metal bar used for prying open crates or stuck doors.")
        AdItem_SetProperty("Value", 0)
        AdItem_SetProperty("Is Key Item", true)
        AdItem_SetProperty("Is Unique", true)
        AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/Crowbar")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Misc, gci_UI_AdItem_Max_Description_Lines)
    DL_PopActiveObject()
    gbFoundItem = true
	
--Cultist Key. Escape the starting dungeon.
elseif(gsItemName == "Cultist Key") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "It's decorated with yellow and purple hands, etched and painted with extreme care.")
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/KeyBronze")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Misc, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true

--Hacksaw. Give this to Claudia.
elseif(gsItemName == "Hacksaw") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A hacksaw, probably with an enchanted blade. It'd make short work of metal bars.")
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/Hacksaw")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Misc, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true

--Oars. Allows usage of the canoes that can be found in some areas.
elseif(gsItemName == "Boat Oars") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Boat oars. Could be used in conjunction with a boat... obviously.")
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/BoatOars")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Misc, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true

--Quantir Mansion Key. Unlocks a bunch of the doors in the Quantir Mansion. Duh.
elseif(gsItemName == "Quantir Mansion Key") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A key that fits the locks in the Quantir Mansion. Seems to have belonged to the serving staff.")
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/KeySilver")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Misc, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true

--Quantir Sewer Key. Unlocks the sewer door.
elseif(gsItemName == "Quantir Sewer Key") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A spare key that opens the sewers in the Quantir Mansion. Seems to have belonged to the serving staff.")
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/KeySilver")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Misc, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true

--Plot-crucial item. Found in the Quantir Mansion.
elseif(gsItemName == "Rilmani Language Guide") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Created by the Heavenly Doves researchers, this guide shows some Rilmani symbols and can be used to translate their language.")
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/BookOrange")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Misc, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true

--Item for Breanne's Flower quest.
elseif(gsItemName == "Decayed Bee Nectar") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A jar of nectar collected by the bees that decayed because it went too long without processing.\nThe bees don't seem to want it.")
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/JarYellow")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Misc, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true

--Item for Breanne's Flower quest.
elseif(gsItemName == "Hypnotic Flower Petals") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Petals from a flower. When smelled, they put a person to sleep fairly quickly.")
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/FlowerWhite")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Misc, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true

--Item for Breanne's Flower quest.
elseif(gsItemName == "Gaardian Cave Moss") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "It'd take a leap of faith to believe this moss could make anything tasty...")
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/JarGreen")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Misc, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true

--Item for Breanne's Flower quest.
elseif(gsItemName == "Booped Paper") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Paper that has been booped against a dog's nose. No, it doesn't make any sense to me either. Roll with it.")
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/BookBlue")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Misc, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true

--Item for Breanne's Flower quest.
elseif(gsItemName == "Translucent Quantirian Salami") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Despite it's name, not made out of Quantir residents. It's actually pork. You can see through it partially...")
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/JarPurple")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Misc, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true

--Item for Breanne's Flower quest.
elseif(gsItemName == "Kokayanee") then
	AdInv_CreateItem(gsItemName)
	
		local iRoll = LM_GetRandomNumber(0, 99)
		if(iRoll < 50) then
			AdItem_SetProperty("Description", "When you want to get down, get right down on the ground, Kokayanee.")
		else
			AdItem_SetProperty("Description", "Kokayanee is a hell of a drug.")
		end
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AdmPowder")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Misc, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true

--Item for the runestone upgrade quest.
elseif(gsItemName == "Greaseflower Petals") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Petals from a particularly greasy flower. Polaris needs these.")
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/FlowerWhite")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Misc, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true
    
--Item for the Polaris rubber quest.
elseif(gsItemName == "Empty Vacuum Phial") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A glass phial, magically enchanted to form a vacuum seal around whatever is put inside. Polaris gave it to you.")
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/JarEmpty")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Misc, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true
    
--Item for the Polaris rubber quest.
elseif(gsItemName == "Vacuum Phial With Rubber") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A glass phial, magically enchanted to form a vacuum seal around whatever is put inside. Polaris gave it to you.\n" .. 
                                          "You filled it with some of the primal rubber you've been seeing. Return it to Polaris.")
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/JarPurple")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Misc, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true
    
--Needed in the mausoleum.
elseif(gsItemName == "Rusted Key") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A key made entirely out of rust, held together by presumably magic. Found in the mausoleum under\nStarfield Swamp.")
		AdItem_SetProperty("Value", 0)
        AdItem_SetProperty("Is Stackable")
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/KeyBronze")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Misc, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true

-- |[ ======================================= Chapter 2 ======================================== ]|
elseif(gsItemName == "Jug of Springwater") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A jug of fresh springwater from Northwoods. The alraunes in Westwoods want this.")
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/KeyBronze")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Misc, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true

elseif(gsItemName == "Pamphlet: Dragon") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A copy of the Nine Tails Post's pamphlet, written by Izuna, detailing Empress and Mount Sarulente. The article\nexplains she's alive and is going to help save the glacier.\n"..
                                          "Mia added a paragraph detailing exactly how tall Empress is.")
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ItemPamphlet")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Misc, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true

elseif(gsItemName == "Pamphlet: Green Westwoods") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A copy of the Nine Tails Post's pamphlet, written by Izuna, detailing Marigold and the alraunes in Westwoods.\nShe talks about the ancient tree and the covenant there helping it "..
                                          "purify the water.")
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ItemPamphlet")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Misc, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true

elseif(gsItemName == "Pamphlet: Bun Runners") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A copy of the Nine Tails Post's pamphlet, written by Izuna, going over Angelface and her unique proclivities.\nWhere to find the bunnies is tastefully left out. That is an "..
                                          "exercise for the reader.")
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ItemPamphlet")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Misc, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true

elseif(gsItemName == "Pamphlet: Bandits in Westwoods") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A copy of the Nine Tails Post's pamphlet, written by Izuna, about the bandit attack in Westwoods.\nShe mentions the incredibly beautiful Sanya saving the day by helping to "..
                                          "fix a wheel.\nIt becomes more of an opinion piece towards the end.")
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Unique", true)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ItemPamphlet")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Misc, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true

-- |[ ======================================= Chapter 5 ======================================== ]|
--Credits Chip. Provides 30 Work Credits when cashed in.
elseif(gsItemName == "Credits Chip") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "30 Work Credits, good only in Regulus City. Insert this into a Work Terminal and the credits will be added to\nyour account.")
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
        AdItem_SetProperty("Is Stackable")
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ComputerChip")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Misc, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
    AdInv_SetProperty("Stack Items")
	gbFoundItem = true

end
