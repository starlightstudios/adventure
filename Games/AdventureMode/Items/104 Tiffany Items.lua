-- |[ ==================================== Items for Tiffany =================================== ]|
--Items that only Tiffany can use. Mostly weapons, some items.

-- |[ ========================================= Charts ========================================= ]|
--For equipment which can be made as part of a chart, construct it here.
if(gzTiffanyItemChart == nil) then
    
    --Setup. All functions operate on this chart.
    gzActiveChart = {}
    
    --Make aliases for the functions. This shortens the chart for readability.
    local fnAdd            = fnAddItemToChart
    local fnAddWpn         = fnAddWeaponToChart
    local fnAddAbility     = fnAddAbilityToChart
    local fnSetDescription = fnSetDescriptionOnChart

    -- |[Weapons Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22             | Damage Type            | Stats Array
    fnAddWpn("Mk IV Pulse Diffractor",         200,          1,     false,    gciWeaponCode, "WepTiffanyDiffractor", gciDamageType_Slashing,  {gciStatIndex_Attack,  10})
    fnAddWpn("Mk IV Pulse Rifle",              300,          1,     false,    gciWeaponCode, "WepTiffanyRifle",      gciDamageType_Piercing,  {gciStatIndex_Attack,  32})
    fnAddWpn("Mk V Pulse Diffractor",          300,          3,     false,    gciWeaponCode, "WepTiffanyDiffractor", gciDamageType_Slashing,  {gciStatIndex_Attack,  37})
    fnAddWpn("K-47 Assault Rifle",             500,          0,     false,    gciWeaponCode, "WepTiffanyBullpup",    gciDamageType_Striking,  {gciStatIndex_Attack,  52})
    fnAddWpn("Stalker Bullpup Rifle",          700,          2,     false,    gciWeaponCode, "WepTiffanySniper",     gciDamageType_Obscuring, {gciStatIndex_Attack,  80, gciStatIndex_Initiative, -20})
    fnAddWpn("Mk VI Pulse Diffractor",         500,          2,     false,    gciWeaponCode, "WepTiffanyDiffractor", gciDamageType_Slashing,  {gciStatIndex_Attack,  70})
    fnAddWpn("R-77 Pulse Diffractor",          600,          2,     false,    gciWeaponCode, "WepTiffanyDiffractor", gciDamageType_Slashing,  {gciStatIndex_Attack,  85})
    fnAddWpn("R-10 Ballistic Cannon",          600,          0,     false,    gciWeaponCode, "WepTiffanyBullpup",    gciDamageType_Striking,  {gciStatIndex_Attack, 100, gciStatIndex_Initiative, -10})
    fnAddWpn("Hotshot Pulse Diffractor",      1000,          3,     false,    gciWeaponCode, "WepTiffanyDiffractor", gciDamageType_Slashing,  {gciStatIndex_Attack,  90})
    fnAddWpn("Overcharge Pulse Rifle",        1200,          2,     false,    gciWeaponCode, "WepTiffanyRifle",      gciDamageType_Piercing,  {gciStatIndex_Attack,  95})

    -- |[Armors Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22     | Stats Array
    fnAdd("Command Unit Garb",                 100,          0,     false,     gciArmorCode, "ArmorTiffany", {gciStatIndex_Protection, 1, gciStatIndex_Initiative, 20})

    -- |[Accessories Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22  | Stats Array
    fnAdd("Wide-Spectrum Scanner",             300,          0,     false, gciAccessoryCode, "ItemScope", {gciStatIndex_Attack,    5, gciStatIndex_Accuracy,  5})
    fnAdd("Pulse Radiation Dampener",          300,          0,     false, gciAccessoryCode, "ItemTech",  {gciStatIndex_Attack,   10})
    fnAdd("Magrail Harmonization Module",      300,          0,     false, gciAccessoryCode, "ItemTech",  {gciStatIndex_Attack,   15, gciStatIndex_Accuracy,  10})

    -- |[Combat Items Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22      | Stats Array
    fnAdd("Smoke Bomb",                        150,          0,     false,      gciItemCode, "Bomb",      {})
    fnAddAbility("Smoke Bomb", gsRoot .. "Combat/Party/Items/Abilities/Smoke Bomb.lua")

    -- |[Descriptions]|
    --Weapons
    fnSetDescription("Mk IV Pulse Diffractor",   "A short-ranged weapon that builds electric charge on tiny metal shards and launches them at the target.[BR]Deals slashing damage.")
    fnSetDescription("Mk IV Pulse Rifle",        "The standard pulse rifle, builds an electric charge on a single metal slug and fires it magnetically. Very accurate[BR]and powerful.[BR]Deals piercing damage.")
    fnSetDescription("Mk V Pulse Diffractor",    "The next generation in pulse weaponry, only a few have been issued to elite units. All parts are lighter and[BR]better made than the Mk IV variant.[BR]Deals slashing damage.")
    fnSetDescription("K-47 Assault Rifle",       "An old-world rifle that uses expanding gas to launch projectiles. Simple, but very easy to manufacture and[BR]maintain.[BR]Deals striking damage.")
    fnSetDescription("Stalker Bullpup Rifle",    "A rifle that phases its projectile between zero and normal mass, allowing it to travel instantaneously. Used by[BR]elite snipers.[BR]Deals obscuring damage.")
    fnSetDescription("Mk VI Pulse Diffractor",   "An experimental Mk VI diffractor currently being rushed into service. Clanks when firing, and exposes the user[BR]to thermal radiation, but you can't argue with power.[BR]Deals slashing damage.")
    fnSetDescription("R-77 Pulse Diffractor",    "A pulse diffractor using the R-77 series of magnetic launch coils. The extra radiation won't harm the user.[BR]Probably.[BR]Deals slashing damage.")
    fnSetDescription("R-10 Ballistic Cannon",    "A cannon that fires 15mm slugs using expanding gasses. Normally meant to breach walls, it deals horrendous[BR]damage to soft targets.[BR]Deals striking damage.")
    fnSetDescription("Hotshot Pulse Diffractor", "A pulse diffractor with its firing capacitor doubled. Requires a crack operator to keep it from overheating.[BR]Deals slashing damage.")
    fnSetDescription("Overcharge Pulse Rifle",   "By removing the volt clamps from a pulse rifle, its charge rate increases but risks detonation. A skilled operator[BR]can discharge it in a narrow window for extreme power.[BR]Deals piercing damage.")

    --Armors
    fnSetDescription("Command Unit Garb",        "Steelweave clothes that fit on a command unit's standardized chassis. Normal command unit wear.")

    --Accessories
    fnSetDescription("Wide-Spectrum Scanner",        "A scope attachment for a pulse diffractor or similar long-barrel weapon. Scans a wide range of photon bands[BR]for weak spots and reports them to the user, increasing damage.")
    fnSetDescription("Pulse Radiation Dampener",     "A barrel attachment for a long-barrel weapon that greatly reduces outgoing radiation. Increases damage to the[BR]target, reduces damage to the environment.")
    fnSetDescription("Magrail Harmonization Module", "Anti-vibration module attached to a long-barrel weapon, keeps the magrails (or the rifling, if applicable) from[BR]shaking and reduces need to adjust the sights. Increases accuracy.")

    --Combat Items
    fnSetDescription("Smoke Bomb",  "A gas grenade that unleashes a billowing cloud of soot. Stops infrared, visible, and ultraviolet photons, cloaking[BR]the party temporarily.[BR]"..
                                    "[IcoEffectFriendly]. +40[IcoEvade]. 3[IcoClock]. All Enemies. 1 use per battle.")

    -- |[Store]|
    --Store the chart under a unique name.
    gzTiffanyItemChart = gzActiveChart
    gzActiveChart = nil

end

-- |[ ==================================== Item Is On Chart ==================================== ]|
--If the item is on the prefabricated chart, we can create it algorithmically.
for i = 1, #gzTiffanyItemChart, 1 do
    if(gzTiffanyItemChart[i].sName == gsItemName) then

        --Quick-access copy.
        local zItem = gzTiffanyItemChart[i]

        --Create item.
        AdInv_CreateItem(gsItemName)
        
            -- |[System]|
            AdItem_SetProperty("Value", zItem.iValue)
            AdItem_SetProperty("Gem Slots", zItem.iGemSlots)
            AdItem_SetProperty("Is Key Item", zItem.bIsKeyItem)
            
            -- |[Display]|
            AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/" .. zItem.sSymbol22)
            AdItem_SetProperty("Description", zItem.sDescription)
            
            -- |[Usability]|
            AdItem_SetProperty("Is Equipment", true)
            AdItem_SetProperty("Add Equippable Character", "Tiffany")
            
            -- |[Stat Array]|
            for p = 1, #zItem.iaStatArray, 2 do
                AdItem_SetProperty("Statistic", zItem.iaStatArray[p+0], zItem.iaStatArray[p+1])
            end
            
            -- |[Usability / Description]|
            --Weapons:
            if(zItem.iUsability == gciWeaponCode) then
                
                --Type.
                AdItem_SetProperty("Type", "Regulan Specialist Arms")
                
                --Equipment slots.
                AdItem_SetProperty("Add Equippable Slot", "Weapon")
                AdItem_SetProperty("Add Equippable Slot", "Weapon Backup A")
                AdItem_SetProperty("Add Equippable Slot", "Weapon Backup B")
                
                --Animations and Sound
                AdItem_SetProperty("Equipment Attack Animation", "Laser Shot")
                AdItem_SetProperty("Equipment Attack Sound",     "Combat\\|Impact_Laser")
                AdItem_SetProperty("Equipment Critical Sound",   "Combat\\|Impact_Laser_Crit")
                
                --Damage type.
                AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, zItem.iDamageType + gciDamageOffsetToResistances, 1.0)
                AdItem_SetProperty("Add Tag", zItem.sSecondaryType, 1)
                fnStandardizeDescription(gcs_UI_DescriptionStd_Weapon, gci_UI_AdItem_Max_Description_Lines)
            
            --Armors:
            elseif(zItem.iUsability == gciArmorCode) then
                AdItem_SetProperty("Type", "Armor (55)")
                AdItem_SetProperty("Add Equippable Slot", "Body")
                fnStandardizeDescription(gcs_UI_DescriptionStd_Armor, gci_UI_AdItem_Max_Description_Lines)
            
            --Accessories:
            elseif(zItem.iUsability == gciAccessoryCode) then
                AdItem_SetProperty("Type", "Accessory (55)")
                AdItem_SetProperty("Add Equippable Slot", "Accessory A")
                AdItem_SetProperty("Add Equippable Slot", "Accessory B")
                fnStandardizeDescription(gcs_UI_DescriptionStd_Accessory, gci_UI_AdItem_Max_Description_Lines)
            
            --Combat Items:
            elseif(zItem.iUsability == gciItemCode) then
                AdItem_SetProperty("Type", "Combat Item (55)")
                AdItem_SetProperty("Add Equippable Slot", "Item A")
                AdItem_SetProperty("Add Equippable Slot", "Item B")
                AdItem_SetProperty("Ability Path", zItem.sAbilityPath)
                fnStandardizeDescription(gcs_UI_DescriptionStd_CombatItem, gci_UI_AdItem_Max_Description_Lines)
            
            end
        DL_PopActiveObject()

        --Finish up.
        gbFoundItem = true
        return
    end
end

-- |[ ====================================== Unique Items ====================================== ]|
--Items that do not fit on the chart are created here. These are items that deviate from the normal
-- creation pattern for some reason.

--No items yet.
