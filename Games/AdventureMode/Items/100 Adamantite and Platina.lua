-- |[ ================================= Adamantite and Platina ================================= ]|
--Used by the main item list, this handles the miscellaneous Adamantite and Platina items.
--Like all item subscripts, uses globals gsItemName and gfBypassAdamantite for efficiency reasons.

-- |[ ==================================== Adamantite Stacks ==================================== ]|
--These items stack. This can be bypassed if you need an item to be created anyway (usually for shop inventories).
-- Sample: "Adamantite Powder x1"
-- The x is important. It denotes the quantity start. In the case of 1, use x1!
if(string.sub(gsItemName, 1, 11) == "Adamantite " and gfBypassAdamantite == 0.0) then

	--Setup.
	local iSection = -1
	local iQuantity = 0
    local sImgPath = ""

	--Find out what the subtype is.
	if(string.sub(gsItemName, 12, 17) == "Powder") then
		iSection = gciCraft_Adamantite_Powder
		iQuantity = tonumber(string.sub(gsItemName, 20))
        sImgPath = "Root/Images/AdventureUI/Symbols22/AdmPowder"
        
	elseif(string.sub(gsItemName, 12, 17) == "Flakes") then
		iSection = gciCraft_Adamantite_Flakes
		iQuantity = tonumber(string.sub(gsItemName, 20))
        sImgPath = "Root/Images/AdventureUI/Symbols22/AdmFlakes"
        
	elseif(string.sub(gsItemName, 12, 16) == "Shard") then
		iSection = gciCraft_Adamantite_Shard
		iQuantity = tonumber(string.sub(gsItemName, 19))
        sImgPath = "Root/Images/AdventureUI/Symbols22/AdmShards"
        
	elseif(string.sub(gsItemName, 12, 16) == "Piece") then
		iSection = gciCraft_Adamantite_Piece
		iQuantity = tonumber(string.sub(gsItemName, 19))
        sImgPath = "Root/Images/AdventureUI/Symbols22/AdmPieces"
        
	elseif(string.sub(gsItemName, 12, 16) == "Chunk") then
		iSection = gciCraft_Adamantite_Chunk
		iQuantity = tonumber(string.sub(gsItemName, 19))
        sImgPath = "Root/Images/AdventureUI/Symbols22/AdmChunks"
        
	elseif(string.sub(gsItemName, 12, 14) == "Ore") then
		iSection = gciCraft_Adamantite_Ore
		iQuantity = tonumber(string.sub(gsItemName, 17))
        sImgPath = "Root/Images/AdventureUI/Symbols22/AdmOre"
	end

    --Special: Override the "Last Received Item" to use the matching item's image. This is used for
    -- notification handlers.
    AdInv_SetProperty("Override Last Item Image", sImgPath)

	--Send this information to the inventory.
	AdInv_SetProperty("Add Crafting Material", iSection, iQuantity)
	gbFoundItem = true
	return
end

-- |[ ========================================= Platina ======================================== ]|
--The currency of Pandemonium is Platina, at least for the game's purposes.
-- Will be of the format "Platina x[NUM]".
if(string.sub(gsItemName, 1, 9) == "Platina x") then
	local iPlatinaAmount = string.sub(gsItemName, 10)
	AdInv_SetProperty("Add Platina", iPlatinaAmount)
    AdInv_SetProperty("Override Last Item Image", "Root/Images/AdventureUI/Symbols22/Platina")
	gbFoundItem = true
	return
end

--Wallets. These give platina from enemy drops.
if(string.sub(gsItemName, 1, 8) == "Wallet x") then
	local iPlatinaAmount = string.sub(gsItemName, 9)
	AdInv_SetProperty("Add Platina", iPlatinaAmount)
    AdInv_SetProperty("Override Last Item Image", "Root/Images/AdventureUI/Symbols22/Platina")
	gbFoundItem = true
	return
end

-- |[ ======================================= Job Points ======================================= ]|
--If the chest contains JP, immediately adds some JP to all party members.
-- Format is "JP x[NUM]".
if(string.sub(gsItemName, 1, 4) == "JP x") then

    --Get amount.
	local iJPAmount = tonumber(string.sub(gsItemName, 5))
    if(iJPAmount < 1) then return end
    
    --Iterate across party.
    local iActiveMembers = AdvCombat_GetProperty("Active Party Size")
    for i = 0, iActiveMembers-1, 1 do
        AdvCombat_SetProperty("Push Active Party Member By Slot", i)
            local iCurrentJP = AdvCombatEntity_GetProperty("Global JP")
            AdvCombatEntity_SetProperty("Current JP", iCurrentJP + iJPAmount)
        DL_PopActiveObject()
    end
    
    --Clear image flag.
    AdInv_SetProperty("Override Last Item Image", "Null")
    
    --Flag.
	gbFoundItem = true
	return
end

--Tip book, provides JP.
if(string.sub(gsItemName, 1, 10) == "Tip Book x") then

    --Get amount.
	local iJPAmount = tonumber(string.sub(gsItemName, 11))
    if(iJPAmount < 1) then return end
    
    --Iterate across party.
    local iActiveMembers = AdvCombat_GetProperty("Active Party Size")
    for i = 0, iActiveMembers-1, 1 do
        AdvCombat_SetProperty("Push Active Party Member By Slot", i)
            local iCurrentJP = AdvCombatEntity_GetProperty("Global JP")
            AdvCombatEntity_SetProperty("Current JP", iCurrentJP + iJPAmount)
        DL_PopActiveObject()
    end
    
    --Clear image flag.
    AdInv_SetProperty("Override Last Item Image", "Null")
    
    --Flag.
	gbFoundItem = true
	return
end

-- |[ =================================== Experience Points ==================================== ]|
--If the chest contains EXP, immediately adds some EXP to all party members.
-- Format is "EXP x[NUM]".
if(string.sub(gsItemName, 1, 5) == "EXP x") then

    --Get amount.
	local iEXPAmount = tonumber(string.sub(gsItemName, 6))
    if(iEXPAmount < 1) then return end
    
    --Iterate across party.
    local iActiveMembers = AdvCombat_GetProperty("Active Party Size")
    for i = 0, iActiveMembers-1, 1 do
        AdvCombat_SetProperty("Push Active Party Member By Slot", i)
            local iCurrentExp = AdvCombatEntity_GetProperty("Exp")
            AdvCombatEntity_SetProperty("Current Exp", iCurrentExp + iEXPAmount)
        DL_PopActiveObject()
    end
    
    --Clear image flag.
    AdInv_SetProperty("Override Last Item Image", "Null")
    
    --Flag.
	gbFoundItem = true
	return
end

-- |[ ================================= Adamantite Shop Items ================================== ]|
--These can only be created for the purposes of shops. If the player buys one, it gets added to their
-- inventory and cannot be resold.
if(gsItemName == "Adamantite Powder") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Is Adamantite")
		AdItem_SetProperty("Description", Translate(gsTranslationItems, "Oxide powder of the rare adamantite metal, which is unique to this world.[BR]Can be used to upgrade gems, which can be socketed in equipment."))
		AdItem_SetProperty("Value", 50)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AdmPowder")
        AdItem_SetProperty("Process Description")
	DL_PopActiveObject()
	gbFoundItem = true
	
elseif(gsItemName == "Adamantite Flakes") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Is Adamantite")
		AdItem_SetProperty("Description", Translate(gsTranslationItems, "Assorted metallic flakes of the rare adamantite metal.[BR]Can be used to upgrade gems, which can be socketed in equipment."))
		AdItem_SetProperty("Value", 250)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AdmFlakes")
        AdItem_SetProperty("Process Description")
	DL_PopActiveObject()
	gbFoundItem = true
	
elseif(gsItemName == "Adamantite Shard") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Is Adamantite")
		AdItem_SetProperty("Description", Translate(gsTranslationItems, "A broken shard of the rare adamantite metal.[BR]Can be used to upgrade gems, which can be socketed in equipment."))
		AdItem_SetProperty("Value", 450)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AdmShards")
        AdItem_SetProperty("Process Description")
	DL_PopActiveObject()
	gbFoundItem = true
	
elseif(gsItemName == "Adamantite Piece") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Is Adamantite")
		AdItem_SetProperty("Description", Translate(gsTranslationItems, "A fist-sized piece of the rare adamantite metal.[BR]Can be used to upgrade gems, which can be socketed in equipment."))
		AdItem_SetProperty("Value", 700)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AdmPieces")
        AdItem_SetProperty("Process Description")
	DL_PopActiveObject()
	gbFoundItem = true
	
elseif(gsItemName == "Adamantite Chunk") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Is Adamantite")
		AdItem_SetProperty("Description", Translate(gsTranslationItems, "A solid chunk of the rare adamantite metal.[BR]Can be used to upgrade gems, which can be socketed in equipment."))
		AdItem_SetProperty("Value", 1200)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AdmChunks")
        AdItem_SetProperty("Process Description")
	DL_PopActiveObject()
	gbFoundItem = true
	
elseif(gsItemName == "Adamantite Ore") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Is Adamantite")
		AdItem_SetProperty("Description", Translate(gsTranslationItems, "Base ore of adamantite. Extremely rare, as adamantite veins cannot be located through traditional means.[BR]Can be used to upgrade gems, which can be socketed in equipment."))
		AdItem_SetProperty("Value", 2000)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AdmOre")
        AdItem_SetProperty("Process Description")
	DL_PopActiveObject()
	gbFoundItem = true
end
