-- |[ ================================= Deconstruction Handler ================================= ]|
--This script gets called whenever the player selects an item for deconstruction in the inventory.

--Resolve the item in question.
local sItemName = LM_GetScriptArgument(0)

-- |[ =================================== List Construction ==================================== ]|
--The first time this script runs, create a list of all items in the game that can be deconstructed.
if(gzaDeconstructList == nil) then
    
    -- |[ ======= Create list, Function ====== ]|
    --List.
    gzaDeconstructList = {}
    
    --Adds a deconstruction entry. Item list format: {"Item x1", "Item x1", etc}
    local function fnAddDeconstruct(psItemName, psaItemList)
        
        --Create structure.
        local zEntry = {}
        zEntry.sName = psItemName
        zEntry.saItems = psaItemList
        
        --Add it.
        table.insert(gzaDeconstructList, zEntry)
        
    end
    
    -- |[ ============= Materials ============ ]|
    --All materials get broken into 8 of the next tier down. Tier I materials don't do this.
    fnAddDeconstruct("Base Metal II",  {"Base Metal I x8"})
    fnAddDeconstruct("Base Metal III", {"Base Metal II x8"})
    fnAddDeconstruct("Base Metal IV",  {"Base Metal III x8"})
    fnAddDeconstruct("Base Metal V",   {"Base Metal IV x8"})
    
    fnAddDeconstruct("Fiber II",  {"Fiber I x8"})
    fnAddDeconstruct("Fiber III", {"Fiber II x8"})
    fnAddDeconstruct("Fiber IV",  {"Fiber III x8"})
    fnAddDeconstruct("Fiber V",   {"Fiber IV x8"})
    
    fnAddDeconstruct("Hide II",  {"Hide I x8"})
    fnAddDeconstruct("Hide III", {"Hide II x8"})
    fnAddDeconstruct("Hide IV",  {"Hide III x8"})
    fnAddDeconstruct("Hide V",   {"Hide IV x8"})
    
    fnAddDeconstruct("Casing II",  {"Casing I x8"})
    fnAddDeconstruct("Casing III", {"Casing II x8"})
    fnAddDeconstruct("Casing IV",  {"Casing III x8"})
    fnAddDeconstruct("Casing V",   {"Casing IV x8"})
    
    fnAddDeconstruct("Tin",      {"Mudstone x8"})
    fnAddDeconstruct("Silver",   {"Tin x8"})
    fnAddDeconstruct("Gold",     {"Silver x8"})
    fnAddDeconstruct("Platinum", {"Gold x8"})
    fnAddDeconstruct("Wooden Greaves", {"Bruised Meat", "Bruised Meat"})

    -- |[ =========== Tier 1 Items =========== ]|
    fnAddDeconstruct("Yew Crossbow",       { {"BMet I",  1}, {"Fbr I",  1}, {"Hide I",  0}, {"Csng I",  1}, {"XMet I",  0} })
    fnAddDeconstruct("Infused Crossbow",   { {"BMet I",  5}, {"Fbr I",  5}, {"Hide I",  0}, {"Csng I",  1}, {"XMet I",  1} })
    fnAddDeconstruct("Steel Broadsword",   { {"BMet I",  1}, {"Fbr I",  0}, {"Hide I",  0}, {"Csng I",  1}, {"XMet I",  0} })
    fnAddDeconstruct("Steel Shortsword",   { {"BMet I",  1}, {"Fbr I",  0}, {"Hide I",  0}, {"Csng I",  1}, {"XMet I",  0} })
    fnAddDeconstruct("Yew Staff",          { {"BMet I",  0}, {"Fbr I",  2}, {"Hide I",  2}, {"Csng I",  1}, {"XMet I",  0} })
    fnAddDeconstruct("Oaken Staff",        { {"BMet I",  0}, {"Fbr I",  6}, {"Hide I",  6}, {"Csng I",  1}, {"XMet I",  0} })
    fnAddDeconstruct("Cotton Robes",       { {"BMet I",  0}, {"Fbr I",  2}, {"Hide I",  0}, {"Csng I",  0}, {"XMet I",  0} })
    fnAddDeconstruct("Leather Armor",      { {"BMet I",  0}, {"Fbr I",  0}, {"Hide I",  2}, {"Csng I",  0}, {"XMet I",  0} })
    fnAddDeconstruct("Hard Leather Armor", { {"BMet I",  1}, {"Fbr I",  0}, {"Hide I",  2}, {"Csng I",  0}, {"XMet I",  0} })
    fnAddDeconstruct("Leather Sandals",    { {"BMet I",  0}, {"Fbr I",  1}, {"Hide I",  2}, {"Csng I",  0}, {"XMet I",  0} })
    fnAddDeconstruct("Leather Boots",      { {"BMet I",  0}, {"Fbr I",  2}, {"Hide I",  0}, {"Csng I",  1}, {"XMet I",  0} })
    fnAddDeconstruct("Plated Boots",       { {"BMet I",  2}, {"Fbr I",  0}, {"Hide I",  1}, {"Csng I",  1}, {"XMet I",  0} })
    fnAddDeconstruct("Steel Greaves",      { {"BMet I",  3}, {"Fbr I",  0}, {"Hide I",  0}, {"Csng I",  2}, {"XMet I",  0} })
    fnAddDeconstruct("Silk Gloves",        { {"BMet I",  0}, {"Fbr I",  2}, {"Hide I",  0}, {"Csng I",  0}, {"XMet I",  0} })
    fnAddDeconstruct("Cloth Armband",      { {"BMet I",  0}, {"Fbr I",  2}, {"Hide I",  0}, {"Csng I",  0}, {"XMet I",  0} })
    fnAddDeconstruct("Cotton Gloves",      { {"BMet I",  0}, {"Fbr I",  2}, {"Hide I",  1}, {"Csng I",  0}, {"XMet I",  0} })
    fnAddDeconstruct("Leather Gloves",     { {"BMet I",  1}, {"Fbr I",  0}, {"Hide I",  1}, {"Csng I",  0}, {"XMet I",  0} })
    fnAddDeconstruct("Plated Gloves",      { {"BMet I",  1}, {"Fbr I",  0}, {"Hide I",  0}, {"Csng I",  1}, {"XMet I",  0} })
    fnAddDeconstruct("Steel Gauntlets",    { {"BMet I",  2}, {"Fbr I",  0}, {"Hide I",  0}, {"Csng I",  1}, {"XMet I",  0} })
    fnAddDeconstruct("Mana Tide Ring",     { {"BMet I",  1}, {"Fbr I",  0}, {"Hide I",  0}, {"Csng I",  1}, {"XMet I",  3} })
    fnAddDeconstruct("Mana Reservoir",     { {"BMet I",  0}, {"Fbr I",  1}, {"Hide I",  1}, {"Csng I",  0}, {"XMet I",  3} })
    fnAddDeconstruct("Blackbelt",          { {"BMet I",  0}, {"Fbr I",  3}, {"Hide I",  0}, {"Csng I",  1}, {"XMet I",  3} })
    fnAddDeconstruct("Gunpowder Bag",      { {"BMet I",  0}, {"Fbr I",  3}, {"Hide I ", 1}, {"Csng I ", 0}, {"XMet I",  2} })
    fnAddDeconstruct("Fencing Mask",       { {"BMet I",  1}, {"Fbr I",  0}, {"Hide I",  0}, {"Csng I",  1}, {"XMet I",  2} })
    
    -- |[ =========== Tier 2 Items =========== ]|
    fnAddDeconstruct("Steel Bolt Crossbow",    { {"BMet II", 1}, {"Fbr II", 1}, {"Hide II", 0}, {"Csng II", 1}, {"XMet II", 0} })
    fnAddDeconstruct("Heartseeker Crossbow",   { {"BMet II", 5}, {"Fbr II", 5}, {"Hide II", 0}, {"Csng II", 1}, {"XMet II", 1} })
    fnAddDeconstruct("Tenfold Broadsword",     { {"BMet II", 1}, {"Fbr II", 0}, {"Hide II", 0}, {"Csng II", 1}, {"XMet II", 0} })
    fnAddDeconstruct("Tenfold Shortsword",     { {"BMet II", 1}, {"Fbr II", 0}, {"Hide II", 0}, {"Csng II", 1}, {"XMet II", 0} })
    fnAddDeconstruct("Hundredfold Broadsword", { {"BMet II", 4}, {"Fbr II", 0}, {"Hide II", 0}, {"Csng II", 5}, {"XMet II", 1} })
    fnAddDeconstruct("Hundredfold Shortsword", { {"BMet II", 4}, {"Fbr II", 0}, {"Hide II", 0}, {"Csng II", 5}, {"XMet II", 1} })
    fnAddDeconstruct("Yew Quarterstaff",       { {"BMet II", 0}, {"Fbr II", 2}, {"Hide II", 2}, {"Csng II", 1}, {"XMet II", 0} })
    fnAddDeconstruct("Oaken Quarterstaff",     { {"BMet II", 0}, {"Fbr II", 6}, {"Hide II", 6}, {"Csng II", 1}, {"XMet II", 1} })
    fnAddDeconstruct("Lined Robe",             { {"BMet II", 0}, {"Fbr II", 2}, {"Hide II", 2}, {"Csng II", 0}, {"XMet II", 1} })
    fnAddDeconstruct("Studded Cuirass",        { {"BMet II", 1}, {"Fbr II", 0}, {"Hide II", 1}, {"Csng II", 1}, {"XMet II", 0} })
    fnAddDeconstruct("Steel Plate",            { {"BMet II", 3}, {"Fbr II", 0}, {"Hide II", 1}, {"Csng II", 1}, {"XMet II", 1} })
    fnAddDeconstruct("Thick Footwraps",        { {"BMet II", 0}, {"Fbr II", 2}, {"Hide II", 1}, {"Csng II", 1}, {"XMet II", 0} })
    fnAddDeconstruct("Hiking Boots",           { {"BMet II", 2}, {"Fbr II", 1}, {"Hide II", 1}, {"Csng II", 0}, {"XMet II", 0} })
    fnAddDeconstruct("War Greaves",            { {"BMet II", 2}, {"Fbr II", 0}, {"Hide II", 0}, {"Csng II", 1}, {"XMet II", 0} })
    fnAddDeconstruct("Magicians Gloves",       { {"BMet II", 1}, {"Fbr II", 1}, {"Hide II", 1}, {"Csng II", 0}, {"XMet II", 2} })
    fnAddDeconstruct("Hunters Gloves",         { {"BMet II", 1}, {"Fbr II", 3}, {"Hide II", 1}, {"Csng II", 0}, {"XMet II", 0} })
    fnAddDeconstruct("War Gauntlets",          { {"BMet II", 2}, {"Fbr II", 0}, {"Hide II", 0}, {"Csng II", 1}, {"XMet II", 0} })
    fnAddDeconstruct("Sprint Shoes",           { {"BMet II", 0}, {"Fbr II", 1}, {"Hide II", 0}, {"Csng II", 0}, {"XMet II", 4} })
    fnAddDeconstruct("Ruby Ring",              { {"BMet II", 0}, {"Fbr II", 1}, {"Hide II", 0}, {"Csng II", 0}, {"XMet II", 4} })
    fnAddDeconstruct("Sapphire Ring",          { {"BMet II", 0}, {"Fbr II", 1}, {"Hide II", 0}, {"Csng II", 0}, {"XMet II", 4} })
    fnAddDeconstruct("Holy Focus",             { {"BMet II", 2}, {"Fbr II", 0}, {"Hide II", 0}, {"Csng II", 0}, {"XMet II", 3} })
end

-- |[ ===================================== List Scanning ====================================== ]|
--Scan across the list. If there's a match, add all the listed components and stop.
for i = 1, #gzaDeconstructList, 1 do
    io.write("Compare: " .. gzaDeconstructList[i].sName .. " to " .. sItemName .. "\n")
    if(gzaDeconstructList[i].sName == sItemName) then
        io.write("Match!\n")
        for p = 1, #gzaDeconstructList[i].saItems, 1 do
            io.write(" Append " .. gzaDeconstructList[i].saItems[p] .. "\n")
            AM_SetProperty("Append Deconstruct Item", gzaDeconstructList[i].saItems[p])
        end
        break
    end
end
