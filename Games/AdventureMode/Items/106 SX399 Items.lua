-- |[ ==================================== Items for SX-399 ==================================== ]|
--Items that only SX-399 can use.

-- |[ ========================================= Charts ========================================= ]|
--For equipment which can be made as part of a chart, construct it here.
if(gzSX399ItemChart == nil) then
    
    --Setup. All functions operate on this chart.
    gzActiveChart = {}
    
    --Make aliases for the functions. This shortens the chart for readability.
    local fnAdd            = fnAddItemToChart
    local fnAddWpn         = fnAddWeaponToChart
    local fnAddAbility     = fnAddAbilityToChart
    local fnSetDescription = fnSetDescriptionOnChart

    -- |[Weapons Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22      | Damage Type             | Stats Array
    fnAddWpn("Fission Carbine",                300,          1,     false,    gciWeaponCode, "WepSX399Rifle", gciDamageType_Piercing,  {gciStatIndex_Attack,  65})
    fnAddWpn("Cryogenic Carbine",              300,          1,     false,    gciWeaponCode, "WepSX399Ice",   gciDamageType_Freezing,  {gciStatIndex_Attack,  68})
    fnAddWpn("Fission Rifle",                  500,          2,     false,    gciWeaponCode, "WepSX399Rifle", gciDamageType_Piercing,  {gciStatIndex_Attack,  90})
    fnAddWpn("Plutonite Fission Carbine",      500,          2,     false,    gciWeaponCode, "WepSX399Fire",  gciDamageType_Flaming,   {gciStatIndex_Attack,  95})
    fnAddWpn("J-12 Freeze Carbine",            500,          2,     false,    gciWeaponCode, "WepSX399Ice",   gciDamageType_Freezing,  {gciStatIndex_Attack,  93})
    
    -- |[Armors Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22      | Stats Array
    fnAdd("Bronze Chestguard",                 100,          0,     false,     gciArmorCode, "ArmorSX399",   {gciStatIndex_Protection, 1, gciStatIndex_Initiative, 20})
    fnAdd("Armored Gothic Corset",             300,          1,     false,     gciArmorCode, "ArmorSX399",   {gciStatIndex_Protection, 5})

    -- |[Accessories Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22      | Stats Array
    fnAdd("Sweet Flame Decals",                300,          0,     false, gciAccessoryCode, "Paint",      {gciStatIndex_Attack, 10, gciStatIndex_Accuracy, 5})

    -- |[Combat Items Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22      | Stats Array
    fnAdd("Underslung Flamethrower",             -1,          0,    false,      gciItemCode, "ItemTech",      {})
    fnAddAbility("Underslung Flamethrower", gsRoot .. "Combat/Party/Items/Abilities/Underslung Flamethrower.lua")

    -- |[Descriptions]|
    --Weapons
    fnSetDescription("Fission Carbine",           "Fires heated slugs with miniature nuclear explosions. Achieves speeds that make pulse weapons blush, but is an\nunstable weapon no matter what.\nDeals piercing damage.")
    fnSetDescription("Cryogenic Carbine",         "Fires a single charge of ultra-compressed hydrogen. When the container ruptures, the gas expands and absorbs\nall ambient heat, freezing the target solid.\nDeals freezing damage.")
    fnSetDescription("Fission Rifle",             "A rifle that ostensibly fires heated slugs. In practice, the fission reactions that launch the rounds tend to simply\nmelt the slug - and the target.\nDeals piercing damage.")
    fnSetDescription("Plutonite Fission Carbine", "By encasing the barrel in plutonite, the melting point increases, meaning a higher reaction speed and a higher\nlaunch temperature.\nDeals flaming damage.")
    fnSetDescription("J-12 Freeze Carbine",       "A mysterious substance found in nodules on the lunar surface only known as 'Substance J-12' allows for even\nmore extreme hydrogen compression.\nFreezes enemies on impact.")

    --Armors
    fnSetDescription("Bronze Chestguard",     "A simple chestguard made of ancient materials. Luckily, SX-399's chassis is tough enough as it is.")
    fnSetDescription("Armored Gothic Corset", "Steelweave corset used as ladies' fashion in the biolabs. Might stop a bullet or two, who knows?")

    --Accessories
    fnSetDescription("Sweet Flame Decals", "Ceramite decals that can be attached to the side of your weapon to make it look sweet while you melt people\nwith it. Can withstand temperatures over 6000 degrees!")

    --Combat Items
    fnSetDescription("Underslung Flamethrower", "A miniature flamethrower that tucks under a long-barrel weapon. Only holds a single charge, but nobody says\nno to more fire.\n1.00x[IcoAttack] as [IcoFlaming].")

    -- |[Store]|
    --Store the chart under a unique name.
    gzSX399ItemChart = gzActiveChart
    gzActiveChart = nil

end

-- |[ ==================================== Item Is On Chart ==================================== ]|
--If the item is on the prefabricated chart, we can create it algorithmically.
for i = 1, #gzSX399ItemChart, 1 do
    if(gzSX399ItemChart[i].sName == gsItemName) then

        --Quick-access copy.
        local zItem = gzSX399ItemChart[i]

        --Create item.
        AdInv_CreateItem(gsItemName)
        
            -- |[System]|
            AdItem_SetProperty("Value", zItem.iValue)
            AdItem_SetProperty("Gem Slots", zItem.iGemSlots)
            AdItem_SetProperty("Is Key Item", zItem.bIsKeyItem)
            
            -- |[Display]|
            AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/" .. zItem.sSymbol22)
            AdItem_SetProperty("Description", zItem.sDescription)
            
            -- |[Usability]|
            AdItem_SetProperty("Is Equipment", true)
            AdItem_SetProperty("Add Equippable Character", "SX-399")
            
            -- |[Stat Array]|
            for p = 1, #zItem.iaStatArray, 2 do
                AdItem_SetProperty("Statistic", zItem.iaStatArray[p+0], zItem.iaStatArray[p+1])
            end
            
            -- |[Usability / Description]|
            --Weapons:
            if(zItem.iUsability == gciWeaponCode) then
                
                --Description type.
                AdItem_SetProperty("Type", "Regular Heavy Arms")
                
                --Equipment slots.
                AdItem_SetProperty("Add Equippable Slot", "Weapon")
                AdItem_SetProperty("Add Equippable Slot", "Weapon Backup A")
                AdItem_SetProperty("Add Equippable Slot", "Weapon Backup B")
                
                --Animations and Sound
                AdItem_SetProperty("Equipment Attack Animation", "Laser Shot")
                AdItem_SetProperty("Equipment Attack Sound",     "Combat\\|Impact_Laser")
                AdItem_SetProperty("Equipment Critical Sound",   "Combat\\|Impact_Laser_Crit")
                
                --Damage type.
                AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, zItem.iDamageType + gciDamageOffsetToResistances, 1.0)
                AdItem_SetProperty("Add Tag", zItem.sSecondaryType, 1)
                fnStandardizeDescription(gcs_UI_DescriptionStd_Weapon, gci_UI_AdItem_Max_Description_Lines)
            
            --Armors:
            elseif(zItem.iUsability == gciArmorCode) then
                AdItem_SetProperty("Type", "Armor (SX-399)")
                AdItem_SetProperty("Add Equippable Slot", "Body")
                fnStandardizeDescription(gcs_UI_DescriptionStd_Armor, gci_UI_AdItem_Max_Description_Lines)
            
            --Accessories:
            elseif(zItem.iUsability == gciAccessoryCode) then
                AdItem_SetProperty("Type", "Accessory (SX-399)")
                AdItem_SetProperty("Add Equippable Slot", "Accessory A")
                AdItem_SetProperty("Add Equippable Slot", "Accessory B")
                fnStandardizeDescription(gcs_UI_DescriptionStd_Accessory, gci_UI_AdItem_Max_Description_Lines)
            
            --Combat Items:
            elseif(zItem.iUsability == gciItemCode) then
                AdItem_SetProperty("Type", "Combat Item (SX-399)")
                AdItem_SetProperty("Add Equippable Slot", "Item A")
                AdItem_SetProperty("Add Equippable Slot", "Item B")
                AdItem_SetProperty("Ability Path", zItem.sAbilityPath)
                fnStandardizeDescription(gcs_UI_DescriptionStd_CombatItem, gci_UI_AdItem_Max_Description_Lines)
            
            end
        DL_PopActiveObject()

        --Finish up.
        gbFoundItem = true
        return
    end
end

-- |[ ====================================== Unique Items ====================================== ]|
--Items that do not fit on the chart are created here. These are items that deviate from the normal
-- creation pattern for some reason.

--No items yet.
