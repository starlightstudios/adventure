-- |[ ==================================== Florentina Items ==================================== ]|
--Items that only Florentina can equip for various reasons. Florentina uses hunting knives and light armor, and has a unique Smoking Pipe.
--Like all item subscripts, uses globals gsItemName and gfBypassAdamantite for efficiency reasons.

-- |[ ================================= Weapon Standardization ================================= ]|
--Sets standard equippable case, slots, and weapon animations.
local fnStandardWeapon = function()
    
    --Description type.
    AdItem_SetProperty("Type", "Knife")
    
    --Can be used as a weapon.
    AdItem_SetProperty("Is Equipment", true)
    AdItem_SetProperty("Add Equippable Character", "Florentina")
    AdItem_SetProperty("Add Equippable Slot", "Weapon")
    AdItem_SetProperty("Add Equippable Slot", "Weapon Backup A")
    AdItem_SetProperty("Add Equippable Slot", "Weapon Backup B")
    
    --Animations and Sound
    AdItem_SetProperty("Equipment Attack Animation", "Sword Slash")
    AdItem_SetProperty("Equipment Attack Sound", "Combat\\|Impact_CrossSlash")
    AdItem_SetProperty("Equipment Critical Sound", "Combat\\|Impact_CrossSlash_Crit")
    
    --Description handling.
    fnStandardizeDescription(gcs_UI_DescriptionStd_Weapon, gci_UI_AdItem_Max_Description_Lines)
end

-- |[ ====================================== Unique Items ====================================== ]|
--Florentina's Smoking Pipe. It probably has Alraune drugs in it.
if(gsItemName == "Florentina's Pipe") then
    
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Florentina's pipe. It's anyone's guess what's in it...\nKey Item.")
        AdItem_SetProperty("Type", "Accessory (Florentina)")
		AdItem_SetProperty("Value", -1)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AccRing")
		AdItem_SetProperty("Gem Slots", 0)
        
        --Accessory.
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Florentina")
		AdItem_SetProperty("Add Equippable Slot", "Accessory A")
		AdItem_SetProperty("Add Equippable Slot", "Accessory B")
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack, 15)
        AdItem_SetProperty("Statistic", gciStatIndex_Protection, 2)
        AdItem_SetProperty("Statistic", gciStatIndex_Resist_Freeze, 2)
        AdItem_SetProperty("Statistic", gciStatIndex_Resist_Terrify, 2)
    
        --Description handling.
        fnStandardizeDescription(gcs_UI_DescriptionStd_Accessory, gci_UI_AdItem_Max_Description_Lines)
        
	DL_PopActiveObject()
	gbFoundItem = true

-- |[ ==================================== Weapons (Tier 0) ==================================== ]|
-- |[Hunting Knife]|
--Florentina starts with a good-quality hunting knife.
elseif(gsItemName == "Hunting Knife") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A large, flat blade, with a serrated edge on one side. Light, easy to use, and dangerous in the right hands.\nDeals slashing damage.")
		AdItem_SetProperty("Value", 50)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepFlorentinaKnife")
		AdItem_SetProperty("Gem Slots", 0)
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack, 12)
        AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, gciDamageType_Slashing + gciDamageOffsetToResistances, 1.0)
        
        --Standardized function.
        fnStandardWeapon()
        
	DL_PopActiveObject()
	gbFoundItem = true

-- |[ ==================================== Weapons (Tier 1) ==================================== ]|
-- |[ ==================================== Weapons (Tier 2) ==================================== ]|
-- |[Wildflower's Knife]|
--A knife explicitly made for Alraunes.
elseif(gsItemName == "Wildflower's Knife") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Odd blue gems adorn the hilt, and a vine seems to be growing over it.\nIt's also really sharp, so Florentina is happy with it.\nDeals slashing damage.")
		AdItem_SetProperty("Value", 250)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepFlorentinaKnife")
		AdItem_SetProperty("Gem Slots", 1)
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack,  16)
        AdItem_SetProperty("Statistic", gciStatIndex_Evade,    3)
        AdItem_SetProperty("Statistic", gciStatIndex_Accuracy, 3)
        AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, gciDamageType_Slashing + gciDamageOffsetToResistances, 1.0)
        
        --Standardized function.
        fnStandardWeapon()
    
	DL_PopActiveObject()
	gbFoundItem = true

-- |[Butterfly Knife]|
--Good for stabbin'.
elseif(gsItemName == "Butterfly Knife") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A knife that can be concealed in its handle and revealed with an elegant flipping motion. Good for murder.\nBoosts combat dexterity. Deals slashing damage.")
		AdItem_SetProperty("Value", 280)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepFlorentinaKnife")
		AdItem_SetProperty("Gem Slots", 1)
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack,     12)
        AdItem_SetProperty("Statistic", gciStatIndex_Evade,       7)
        AdItem_SetProperty("Statistic", gciStatIndex_Accuracy,    7)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 15)
        AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, gciDamageType_Slashing + gciDamageOffsetToResistances, 1.0)
        
        --Standardized function.
        fnStandardWeapon()
        
	DL_PopActiveObject()
	gbFoundItem = true

-- |[Giant Stinger]|
elseif(gsItemName == "Giant Stinger") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A really big stinger from a bee. Some sort of bee heirloom, probably, but good for stabbing.\nDeals poison damage.")
		AdItem_SetProperty("Value", 280)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepFlorentinaPoisonA")
		AdItem_SetProperty("Gem Slots", 1)
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack,     18)
        AdItem_SetProperty("Statistic", gciStatIndex_Evade,       3)
        AdItem_SetProperty("Statistic", gciStatIndex_Accuracy,    3)
        AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, gciDamageType_Poisoning + gciDamageOffsetToResistances, 1.0)
        
        --Standardized function.
        fnStandardWeapon()
        
	DL_PopActiveObject()
	gbFoundItem = true

-- |[ ==================================== Weapons (Tier 3) ==================================== ]|
-- |[Steel Dagger]|
elseif(gsItemName == "Steel Dagger") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Made of steel, nothing special except an edge and no conscience.\nDeals slashing damage.")
		AdItem_SetProperty("Value", 430)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepFlorentinaKnife")
		AdItem_SetProperty("Gem Slots", 1)
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack,     42)
        AdItem_SetProperty("Statistic", gciStatIndex_Evade,       3)
        AdItem_SetProperty("Statistic", gciStatIndex_Accuracy,    3)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 10)
        AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, gciDamageType_Slashing + gciDamageOffsetToResistances, 1.0)
        
        --Standardized function.
        fnStandardWeapon()
        
	DL_PopActiveObject()
	gbFoundItem = true

-- |[Haggler's Backup Plan]|
--Oh, I get it.
elseif(gsItemName == "Haggler's Backup Plan") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Negotiations sometimes break down. It's best to have a 'backup plan'.\nDeals slashing damage.")
		AdItem_SetProperty("Value", 440)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepFlorentinaKnife")
		AdItem_SetProperty("Gem Slots", 2)
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack,     40)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 10)
        AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, gciDamageType_Slashing + gciDamageOffsetToResistances, 1.0)
        
        --Standardized function.
        fnStandardWeapon()
	DL_PopActiveObject()
	gbFoundItem = true

-- |[Blackjack]|
--Send away.
elseif(gsItemName == "Blackjack") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "For when you want to strike a bargain.\nDeals striking damage.")
		AdItem_SetProperty("Value", 520)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepFlorentinaStrike")
		AdItem_SetProperty("Gem Slots", 1)
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack,     45)
        AdItem_SetProperty("Statistic", gciStatIndex_Accuracy,    5)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative,  5)
        AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, gciDamageType_Striking + gciDamageOffsetToResistances, 1.0)
        
        --Standardized function.
        fnStandardWeapon()
	DL_PopActiveObject()
	gbFoundItem = true

-- |[ ==================================== Weapons (Tier 4) ==================================== ]|
-- |[Honey-Soaked Dagger]|
elseif(gsItemName == "Honey-Soaked Dagger") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "The blade seems to have been long abandoned. It's sharp and very dangerous, and smells horrible.\nDeals Terrify damage.")
		AdItem_SetProperty("Value", 715)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/WepFlorentinaPoisonB")
		AdItem_SetProperty("Gem Slots", 1)
        
        --Set statistics and damage type.
        AdItem_SetProperty("Statistic", gciStatIndex_Attack,     63)
        AdItem_SetProperty("Statistic", gciStatIndex_Evade,       5)
        AdItem_SetProperty("Statistic", gciStatIndex_Accuracy,    5)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 15)
        AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, gciDamageType_Terrifying + gciDamageOffsetToResistances, 1.0)
        
        --Standardized function.
        fnStandardWeapon()
        
	DL_PopActiveObject()
	gbFoundItem = true
    
-- |[ ==================================== Armors (Tier 0) ===================================== ]|
-- |[Flowery Tunic]|
--Starting armor for Florentina in Chapter 1.
elseif(gsItemName == "Flowery Tunic") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Florentina's clothing. Stylish, light, comfortable, but not particularly protective.")
        AdItem_SetProperty("Type", "Armor (Florentina)")
		AdItem_SetProperty("Value", 75)
		AdItem_SetProperty("Gem Slots", 0)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmorFlorentina")
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Florentina")
		AdItem_SetProperty("Add Equippable Slot", "Body")
        AdItem_SetProperty("Statistic", gciStatIndex_Protection, 2)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 20)
        fnStandardizeDescription(gcs_UI_DescriptionStd_Armor, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true
	
-- |[ ==================================== Armors (Tier 1) ===================================== ]|
-- |[Hemp Skirt]|
elseif(gsItemName == "Hemp Skirt") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A skirt made of hemp, which smells a lot like whatever's in Florentina's pipe, actually.")
        AdItem_SetProperty("Type", "Armor (Florentina)")
		AdItem_SetProperty("Value", 140)
		AdItem_SetProperty("Gem Slots", 0)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Florentina")
		AdItem_SetProperty("Add Equippable Slot", "Body")
        AdItem_SetProperty("Statistic", gciStatIndex_Attack,     2)
        AdItem_SetProperty("Statistic", gciStatIndex_Accuracy,   2)
        AdItem_SetProperty("Statistic", gciStatIndex_Protection, 2)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 20)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmorFlorentina")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Armor, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true
	
-- |[ ==================================== Armors (Tier 2) ===================================== ]|
-- |[Merchant's Tunic]|
elseif(gsItemName == "Merchant's Tunic") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "It fits Florentina perfectly, therefore, it's a merchant's tunic. Figure out the logic on your own time.")
        AdItem_SetProperty("Type", "Armor (Florentina)")
		AdItem_SetProperty("Value", 200)
		AdItem_SetProperty("Gem Slots", 1)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Florentina")
		AdItem_SetProperty("Add Equippable Slot", "Body")
        AdItem_SetProperty("Statistic", gciStatIndex_Attack,     10)
        AdItem_SetProperty("Statistic", gciStatIndex_Protection,  2)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 20)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmorFlorentina")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Armor, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true
	
-- |[Regalia of a Smoothtalker]|
elseif(gsItemName == "Regalia of a Smoothtalker") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "She'll convince you up is down. That way the money will fall out of your pockets.")
        AdItem_SetProperty("Type", "Armor (Florentina)")
		AdItem_SetProperty("Value", 210)
		AdItem_SetProperty("Gem Slots", 1)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Florentina")
		AdItem_SetProperty("Add Equippable Slot", "Body")
        AdItem_SetProperty("Statistic", gciStatIndex_Attack,      5)
        AdItem_SetProperty("Statistic", gciStatIndex_Evade,       3)
        AdItem_SetProperty("Statistic", gciStatIndex_Accuracy,    3)
        AdItem_SetProperty("Statistic", gciStatIndex_Protection,  2)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 20)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmorFlorentina")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Armor, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true
	
-- |[Rubber Corset]|
elseif(gsItemName == "Rubber Corset") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Was it always rubber? Either way, it's protective and form-fitting.")
        AdItem_SetProperty("Type", "Armor (Florentina)")
		AdItem_SetProperty("Value", 210)
		AdItem_SetProperty("Gem Slots", 2)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Florentina")
		AdItem_SetProperty("Add Equippable Slot", "Body")
        AdItem_SetProperty("Statistic", gciStatIndex_Attack,      8)
        AdItem_SetProperty("Statistic", gciStatIndex_Protection,  2)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 20)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmorFlorentina")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Armor, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true
    
-- |[ ==================================== Armors (Tier 3) ===================================== ]|
-- |[Choirmaid's Robe]|
elseif(gsItemName == "Choirmaid's Robe") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Used by the religious choirs once in St. Fora's. They don't seem to need it now.")
        AdItem_SetProperty("Type", "Armor (Florentina)")
		AdItem_SetProperty("Value", 390)
		AdItem_SetProperty("Gem Slots", 1)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Florentina")
		AdItem_SetProperty("Add Equippable Slot", "Body")
        AdItem_SetProperty("Statistic", gciStatIndex_Attack,     15)
        AdItem_SetProperty("Statistic", gciStatIndex_Evade,       3)
        AdItem_SetProperty("Statistic", gciStatIndex_Accuracy,    3)
        AdItem_SetProperty("Statistic", gciStatIndex_Protection,  2)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 20)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmorFlorentina")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Armor, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true
    
-- |[Nightwatch Vest]|
elseif(gsItemName == "Nightwatch Vest") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A vest used by a mercenary in the night's watch. They're probably a bee now. Probably.")
        AdItem_SetProperty("Type", "Armor (Florentina)")
		AdItem_SetProperty("Value", 410)
		AdItem_SetProperty("Gem Slots", 1)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Florentina")
		AdItem_SetProperty("Add Equippable Slot", "Body")
        AdItem_SetProperty("Statistic", gciStatIndex_Attack,     20)
        AdItem_SetProperty("Statistic", gciStatIndex_Protection,  2)
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 20)
        AdItem_SetProperty("Statistic", gciStatIndex_Resist_Pierce, 2)
        AdItem_SetProperty("Statistic", gciStatIndex_Resist_Flame, 2)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/ArmorFlorentina")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Armor, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true

-- |[ ================================== Accessories (Tier 1) ================================== ]|
-- |[Methlace]|
--Accessory for Florentina. Increases initiative.
elseif(gsItemName == "Methlace") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "Turns out most drugs are fairly mild when taken by Alraunes. A necklace that increases combat initiative.")
        AdItem_SetProperty("Type", "Accessory (Florentina)")
		AdItem_SetProperty("Value", 150)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Florentina")
		AdItem_SetProperty("Add Equippable Slot", "Accessory A")
		AdItem_SetProperty("Add Equippable Slot", "Accessory B")
        AdItem_SetProperty("Statistic", gciStatIndex_Initiative, 20)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/PendantB")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Accessory, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true
    
-- |[Flax Cloak]|
elseif(gsItemName == "Flax Cloak") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A simple cloak made of flax. Increases protection and cold resist.")
        AdItem_SetProperty("Type", "Accessory (Florentina)")
		AdItem_SetProperty("Value", 150)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Florentina")
		AdItem_SetProperty("Add Equippable Slot", "Accessory A")
		AdItem_SetProperty("Add Equippable Slot", "Accessory B")
        AdItem_SetProperty("Statistic", gciStatIndex_Protection, 1)
        AdItem_SetProperty("Statistic", gciStatIndex_Resist_Freeze, 3)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AccCapeBrown")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Accessory, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true

-- |[ ================================== Accessories (Tier 2) ================================== ]|
-- |[Ruby Necklace]|
elseif(gsItemName == "Ruby Necklace") then
	AdInv_CreateItem(gsItemName)
		AdItem_SetProperty("Description", "A big, bright red ruby catches your reflection in this silver necklace. Not just fashionable, it also increases\ndefense and accuracy.")
        AdItem_SetProperty("Type", "Accessory (Florentina)")
		AdItem_SetProperty("Value", 0)
		AdItem_SetProperty("Is Key Item", true)
		AdItem_SetProperty("Is Equipment", true)
		AdItem_SetProperty("Add Equippable Character", "Florentina")
		AdItem_SetProperty("Add Equippable Slot", "Accessory A")
		AdItem_SetProperty("Add Equippable Slot", "Accessory B")
        AdItem_SetProperty("Statistic", gciStatIndex_Protection, 5)
        AdItem_SetProperty("Statistic", gciStatIndex_Accuracy,  12)
		AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/AccRing")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Accessory, gci_UI_AdItem_Max_Description_Lines)
	DL_PopActiveObject()
	gbFoundItem = true
end
