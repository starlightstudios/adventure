-- |[ ====================================== Item Listing ====================================== ]|
--Contains a list of all the items in the game. Call this script with the name of the item you want and the
-- item will be added to the player's inventory.
--Subscripts use Lua globals for efficiency (there is no need to re-declare script arguments since they are shared).

--Argument Listing:
-- 0: sItemName - What item to add.
-- 1: bBypassAdamantite - Optional. If this argument is true, the Adamantite entry will be created. Used for shops.
--io.write("Item listing begins.\n")

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
gsItemName = LM_GetScriptArgument(0)
gfBypassAdamantite = 0.0
if(iArgs >= 2) then gfBypassAdamantite = tonumber(LM_GetScriptArgument(1)) end

-- |[ =================================== Subscript Iteration ================================== ]|
--Global flag, set to false. Stop execution as soon as one of the subscripts sets it to true.
local sBasePath = fnResolvePath()
gbFoundItem = false

--Call this function if the item is being placed in the inventory.
if(AdInv_GetProperty("Is Registering To Shop") == false) then
    fnCheckItemAchievements(gsItemName)
end
        
-- |[Call Subscripts]|
--Variables.
local iTotal = 1
local saFileList = {}

--Adder function.
local fnAddFile = function(sName)
	saFileList[iTotal] = sName
	iTotal = iTotal + 1
end

--List of subscripts.
fnAddFile("100 Adamantite and Platina")
fnAddFile("101 Mei Items")
fnAddFile("102 Florentina Items")
fnAddFile("103 Christine Items")
fnAddFile("104 Tiffany Items")
fnAddFile("105 JX101 Items")
fnAddFile("106 SX399 Items")
fnAddFile("107 Sanya Items")
fnAddFile("108 Izuna Items")
fnAddFile("109 Zeke Items")
fnAddFile("110 Empress Items")
fnAddFile("200 Healing Items")
fnAddFile("300 Light Armors")
fnAddFile("301 Medium Armors")
fnAddFile("302 Heavy Armors")
fnAddFile("310 Accessories")
fnAddFile("320 Offense Items")
fnAddFile("900 Misc Items")
fnAddFile("901 Key Items")
fnAddFile("902 Repeatable Quest Items")
fnAddFile("903 Hunting Rewards")

--Iterate across the file list.
for i = 1, iTotal-1, 1 do
    --io.write(" Checking " .. i .. "\n")
	LM_ExecuteScript(sBasePath .. saFileList[i] .. ".lua")
	
	--End execution. Print debug if needed.
	if(gbFoundItem == true) then
        --io.write(" Found.\n")
		return
	end
end

-- |[Gem Case]|
--Gems are the only items which use the (+1) upgrade cases. Therefore, we use the unstripped item name here.
LM_ExecuteScript(sBasePath .. "400 Gems" .. ".lua")
if(gbFoundItem == true) then
	return
end

-- |[Mods]|
--Scan the mods to see if any of them have the items in question.
gbFoundItem = fnExecModScript("Items/Item List.lua")

-- |[Error Case]|
if(gbFoundItem == false) then
	Debug_ForcePrint("Item List: Error, no item " .. gsItemName .. "\n")
end
