-- |[ ========================== Character Independent Healing Items =========================== ]|
--Items that can be used by anyone and that heals or buffs the party.

-- |[ ========================================= Charts ========================================= ]|
--For equipment which can be made as part of a chart, construct it here.
if(gzHealingItemChart == nil) then
    
    --Setup. All functions operate on this chart.
    gzActiveChart = {}
    
    --Make aliases for the functions. This shortens the chart for readability.
    local fnAdd            = fnAddItemToChart
    local fnAddAbility     = fnAddAbilityToChart
    local fnSetDescription = fnSetDescriptionOnChart

    -- |[ Healing Items Chart ]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22      | Stats Array
    fnAdd("Healing Tincture",                   75,          0,     false,      gciItemCode, "ItemPotionRed",     {})
    fnAdd("Bubbling Healing Tincture",         200,          0,     false,      gciItemCode, "ItemPotionRed",     {})
    fnAdd("Palliative",                        150,          0,     false,      gciItemCode, "ItemPotionGrn",     {})
    fnAdd("Pepper Pie",                        500,          0,      true,      gciItemCode, "PepperPie",         {})
    fnAdd("Smelling Salts",                    100,          0,     false,      gciItemCode, "SmellingSalts",     {})
    fnAdd("Controversial Sugar Cookies",       200,          0,     false,      gciItemCode, "Cookies",           {})
    fnAdd("Infused Rye Bread",                 200,          0,     false,      gciItemCode, "BreadB",            {})
    fnAdd("Sparkling Sourdough",               200,          0,     false,      gciItemCode, "BreadA",            {})
    fnAdd("Hearty Soup",                        50,          0,     false,      gciItemCode, "ItemPotionRed",     {})
    fnAdd("Brutal Sandwich",                    50,          0,     false,      gciItemCode, "ItemPotionRed",     {})
    fnAdd("Gourmet Lunch",                      50,          0,     false,      gciItemCode, "BreadB",            {})
    fnAdd("Nanite Injection",                  125,          0,     false,      gciItemCode, "ItemInjectorA",     {})
    fnAdd("Explication Spike",                 125,          0,     false,      gciItemCode, "ItemInjectorB",     {})
    fnAdd("Regeneration Mist",                 125,          0,     false,      gciItemCode, "ItemInjectorA",     {})
    fnAdd("Emergency Medkit",                  175,          0,     false,      gciItemCode, "ItemMedkit",        {})

    -- |[Abilities]|
    local sItemAbilityPath = gsRoot .. "Combat/Party/Items/Abilities/"
    fnAddAbility("Healing Tincture",            sItemAbilityPath .. "Healing Tincture.lua")
    fnAddAbility("Bubbling Healing Tincture",   sItemAbilityPath .. "Bubbling Healing Tincture.lua")
    fnAddAbility("Palliative",                  sItemAbilityPath .. "Palliative.lua")
    fnAddAbility("Pepper Pie",                  sItemAbilityPath .. "Pepper Pie.lua")
    fnAddAbility("Smelling Salts",              sItemAbilityPath .. "Smelling Salts.lua")
    fnAddAbility("Controversial Sugar Cookies", sItemAbilityPath .. "Controversial Sugar Cookies.lua")
    fnAddAbility("Infused Rye Bread",           sItemAbilityPath .. "Infused Rye Bread.lua")
    fnAddAbility("Sparkling Sourdough",         sItemAbilityPath .. "Sparkling Sourdough.lua")
    fnAddAbility("Hearty Soup",                 sItemAbilityPath .. "Hearty Soup.lua")
    fnAddAbility("Brutal Sandwich",             sItemAbilityPath .. "Brutal Sandwich.lua")
    fnAddAbility("Gourmet Lunch",               sItemAbilityPath .. "Gourmet Lunch.lua")
    fnAddAbility("Nanite Injection",            sItemAbilityPath .. "Nanite Injection.lua")
    fnAddAbility("Explication Spike",           sItemAbilityPath .. "Explication Spike.lua")
    fnAddAbility("Regeneration Mist",           sItemAbilityPath .. "Regeneration Mist.lua")
    fnAddAbility("Emergency Medkit",            sItemAbilityPath .. "Emergency Medkit.lua")

    -- |[Descriptions]|
    fnSetDescription("Healing Tincture",            "A magic tincture that restores health of the user in combat. Restores 20HP, 1 charge per battle.")
    fnSetDescription("Bubbling Healing Tincture",   "A magic tincture that restores health of the user in combat. Restores 100HP, 1 charge per battle.")
    fnSetDescription("Palliative",                  "A tonic that may or may not actually work, but it seems to relieve and prevent the symptoms of poison.\nAlso cures and prevents bleeding... somehow. 1 charge per battle.")
    fnSetDescription("Pepper Pie",                  "A powerful potion baked into a pie. Increases [ICOATK] by 50%% for 1 turn, and tastes better than sex with\na supermodel. 3 charges per battle.")
    fnSetDescription("Smelling Salts",              "Revives a downed party member with 30%% health. 1 charge per battle.")
    fnSetDescription("Controversial Sugar Cookies", "Yum! Heals you for 35HP and cures bleeds and poisons! Wait, hold on, what? Usable once per battle.")
    fnSetDescription("Infused Rye Bread",           "Yummy rye bread with a healing potion baked in. Heals 50HP, once per battle.")
    fnSetDescription("Sparkling Sourdough",         "Sourdough with a fortification potion in it. Turns your skin harder.\nRestores 25HP and Protection +2 for 3 turns, once per battle.")
    fnSetDescription("Hearty Soup",                 "Miso's trademark hearty soup! Restores 20MP once per battle.")
    fnSetDescription("Brutal Sandwich",             "A sandwich that pulses with pure anger. Doesn't even have any vinegar, it's just that mad.\nRestores 70HP, 1 charge per battle.")
    fnSetDescription("Gourmet Lunch",               "Miso outdid herself on this one. It's got everything, soup, sandwich, roast, and a bit of dessert.\nRestores 100HP and 10MP, increases Evade by 50 for 1 turn, 1 charge per battle.")
    fnSetDescription("Nanite Injection",            "An opaque, white fluid containing billions of nanites. These will rapidly repair flesh and alloy alike when\ninjected, as well as briefly increasing attack power by 20%%. Restores 35 HP. 1 use per battle.")
    fnSetDescription("Explication Spike",           "An electronic 'spike', or single-use hacking tool. When applied to one's CPU/brain, greatly improves accuracy for\none turn. Organics even report only mild migraines as a side-effect! 1 use per battle.")
    fnSetDescription("Regeneration Mist",           "Repair nanites loaded into an aerosol container. Not actually a fluid, so it still works in a vacuum. Heals the\nentire party for 80 HP. 1 charge per battle.")
    fnSetDescription("Emergency Medkit",            "A package of self-assembling repair nanites. Heals 150HP and can revive KO'd teammates. 3 charges per battle.")

    -- |[Store]|
    --Store the chart under a unique name.
    gzHealingItemChart = gzActiveChart
    gzActiveChart = nil

end

-- |[ ==================================== Item Is On Chart ==================================== ]|
--If the item is on the prefabricated chart, we can create it algorithmically.
for i = 1, #gzHealingItemChart, 1 do
    if(gzHealingItemChart[i].sName == gsItemName) then

        --Quick-access copy.
        local zItem = gzHealingItemChart[i]

        --Create item.
        AdInv_CreateItem(gsItemName)
        
            -- |[System]|
            AdItem_SetProperty("Value", zItem.iValue)
            AdItem_SetProperty("Gem Slots", zItem.iGemSlots)
            AdItem_SetProperty("Is Key Item", zItem.bIsKeyItem)
            
            -- |[Display]|
            AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/" .. zItem.sSymbol22)
            AdItem_SetProperty("Description", zItem.sDescription)
            AdItem_SetProperty("Type", "Combat Item")
            
            -- |[Usability]|
            AdItem_SetProperty("Is Equipment", true)
            AdItem_SetProperty("Add Equippable Character", "Mei")
            AdItem_SetProperty("Add Equippable Character", "Florentina")
            AdItem_SetProperty("Add Equippable Character", "Christine")
            AdItem_SetProperty("Add Equippable Character", "Tiffany")
            AdItem_SetProperty("Add Equippable Character", "SX-399")
            AdItem_SetProperty("Add Equippable Character", "Sanya")
            AdItem_SetProperty("Add Equippable Character", "Izuna")
            AdItem_SetProperty("Add Equippable Character", "Zeke")
            AdItem_SetProperty("Add Equippable Character", "Empress")
            AdItem_SetProperty("Add Equippable Slot", "Item A")
            AdItem_SetProperty("Add Equippable Slot", "Item B")
            AdItem_SetProperty("Add Equippable Slot", "Item C")
            AdItem_SetProperty("Add Equippable Slot", "Item D")
            AdItem_SetProperty("Ability Path", zItem.sAbilityPath)
            
            -- |[Modded Usability]|
            --Execute this script with the name of the item in question. Modded characters may be able to use base-game items.
            fnExecModScript("Items/Usability Exec.lua")
            
            -- |[Stat Array]|
            for p = 1, #zItem.iaStatArray, 2 do
                AdItem_SetProperty("Statistic", zItem.iaStatArray[p+0], zItem.iaStatArray[p+1])
            end
            
            -- |[Advanced Description]|
            fnStandardizeDescription(gcs_UI_DescriptionStd_CombatItem, gci_UI_AdItem_Max_Description_Lines)
        DL_PopActiveObject()

        --Finish up.
        gbFoundItem = true
        return
    end
end

-- |[ ====================================== Unique Items ====================================== ]|
--Items that do not fit on the chart are created here. These are items that deviate from the normal
-- creation pattern for some reason.

--No items yet.
