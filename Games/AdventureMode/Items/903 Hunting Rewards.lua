-- |[ ===================================== Hunting Rewards ==================================== ]|
--Items that can be used for crafting and are generally acquired from special prey animals located in chapter 2.

-- |[ =========== List Construction ========== ]|
if(gzaHuntingRewardsList == nil) then
    
    -- |[List Creation]|
    --Storage list.
    local zaList = {}

    --Creates a new entry.
    local function fnAdd(psName, piValue, psImg, psDescription)
        local zEntry = {}
        zEntry.sName        = psName
        zEntry.sDescription = psDescription
        zEntry.iValue       = piValue
        zEntry.sImg         = "Root/Images/AdventureUI/Symbols22/" .. psImg
        table.insert(zaList, zEntry)
    end

    -- |[Common]|
    fnAdd("Bruised Meat",       10, "JunkB", "Damaged meat. Not great for eating. You can take out animals with Sanya's rifle to get better rewards.")
    
    -- |[Northwoods]|
    fnAdd("Bird Meat",          400, "JunkB", "Meat from a bird. Anger seems to pulse from it. Find a specialist to make unique items from it.[BR]Found in Northwoods.")
    fnAdd("Slippery Boar Meat", 400, "JunkB", "Meat from a most unusual boar. Luckily, not venomous. Find a specialist to make unique items from it.[BR]Found in Northwoods.")
    fnAdd("Mighty Venison",     400, "JunkB", "Meat from an enormous single-horned creature. Find a specialist to make unique items from it.[BR]Found in Northwoods.")
    fnAdd("Gossamer",           100, "JunkB", "Delicate organic fiber that forms a butterbomber's wings. Find a specialist to make unique items from it.[BR]Found in Northwoods.")
    fnAdd("Hardened Bark",      100, "JunkB", "Bark with unusual magical properties. Find a specialist to make unique items from it.[BR]Found in Northwoods.")
    fnAdd("Light Leather",       50, "JunkB", "Lightweight leather. Find a specialist to make unique items from it.[BR]Found in Northwoods.")
    fnAdd("Light Fiber",         50, "JunkB", "Strong fiber used by mobile plants. Find a specialist to make unique items from it.[BR]Found in Northwoods.")
    fnAdd("Light Carapace",      50, "JunkB", "Carapace from a giant insect, very tough. Find a specialist to make unique items from it.[BR]Found in Northwoods.")
    
    -- |[Westwoods]|
    fnAdd("Unmelting Snow",     300, "JunkB", "Snow that doesn't melt no matter how warm it is. It's unnerving! I hate it! Find a specialist to make unique items[BR]from it.[BR]Found in Westwoods.")
    fnAdd("Crystallized Acid",  300, "JunkB", "Caustic goo that has turned solid and doesn't burn you. Don't let it touch water. Find a specialist to make unique[BR]items from it.[BR]Found in Westwoods.")
    fnAdd("Buneye Fluff",       600, "JunkB", "A tuft of furr from the elusive Buneye. Find a specialist to make unique items from it.[BR]Found in Westwoods.")
    fnAdd("Griffon Egg",        600, "JunkB", "It's unfertilized, don't get any ideas you lunatic. Find a specialist to make unique items from it.[BR]Found in Westwoods.")
    fnAdd("Bonesaw Hog Meat",   600, "JunkB", "Hoo yeah, this meat takes on all challengers! Find a specialist to make unique items from it.[BR]Found in Westwoods.")
    fnAdd("Caustic Vension",    600, "JunkB", "Venison from unielks that graze on the toxic fungus in Westwoods. Find a specialist to make unique items from it.[BR]Found in Westwoods.")
    fnAdd("Tough Leather",      200, "JunkB", "Toughened leather made from the hardened skin of Westwoods fauna. Find a specialist to make unique items[BR]from it.[BR]Found in Westwoods.")
    fnAdd("Tough Fiber",        200, "JunkB", "Amazingly durable fibers from the 'flora' of Westwoods. Find a specialist to make unique items from it.[BR]Found in Westwoods.")
    fnAdd("Tough Carapace",     200, "JunkB", "Acid-resistant carapace from the creatures in Westwoods. Find a specialist to make unique items from it.[BR]Found in Westwoods.")

    -- |[Finish Up]|
    gzaHuntingRewardsList = zaList
end

-- |[ =============== Scanning =============== ]|
--Scan for the entry.
local zUseEntry = nil
for i = 1, #gzaHuntingRewardsList, 1 do
    if(gsItemName == gzaHuntingRewardsList[i].sName) then
        zUseEntry = gzaHuntingRewardsList[i]
        break
    end
end

--No match, stop.
if(zUseEntry == nil) then return end

-- |[ =============== Creation =============== ]|
AdInv_CreateItem(gsItemName)
    AdItem_SetProperty("Description", zUseEntry.sDescription)
    AdItem_SetProperty("Value", zUseEntry.iValue)
    AdItem_SetProperty("Is Stackable")
    AdItem_SetProperty("Rendering Image", zUseEntry.sImg)
    fnStandardizeDescription(gcs_UI_DescriptionStd_Misc, gci_UI_AdItem_Max_Description_Lines)
DL_PopActiveObject()
AdInv_SetProperty("Stack Items")
gbFoundItem = true
