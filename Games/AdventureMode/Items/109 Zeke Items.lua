-- |[ ===================================== Items for Zeke ===================================== ]|
--Items for Zeke. These include his collar and the combat items he can use.

-- |[ ========================================= Charts ========================================= ]|
--For equipment which can be made as part of a chart, construct it here.
if(gzZekeItemChart == nil) then
    
    --Setup. All functions operate on this chart.
    gzActiveChart = {}
    
    --Make aliases for the functions. This shortens the chart for readability.
    local fnAdd            = fnAddItemToChartMarkdown
    local fnAddWpn         = fnAddWeaponToChartMarkdown
    local fnAddAbility     = fnAddAbilityToChart
    local fnSetDescription = fnSetDescriptionOnChart

    -- |[Weapons Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22             | Damage Type            | Stats Array
    --Zeke doesn't use weapons.

    -- |[Armors Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22     | Stats Array
    fnAdd("Zeke's Bell Collar",                100,          1,     false,     gciArmorCode,   "CollarZeke", "Atk|20 Prt|1 Ini|20") --Tier 0
    
    fnAdd("Thick Collar",                      300,          2,     false,     gciArmorCode,   "CollarZeke", "Atk|30 Prt|4 Ini|20") --Tier 1
    fnAdd("Thick Collar +",                    500,          2,     false,     gciArmorCode,   "CollarZeke", "Atk|37 Prt|5 Ini|20")
    
    fnAdd("Shimmering Collar",                 600,          2,     false,     gciArmorCode,   "CollarZeke", "Atk|40 Prt|7 Ini|30") --Tier 2
    fnAdd("Shimmering Collar +",              1200,          2,     false,     gciArmorCode,   "CollarZeke", "Atk|53 Prt|7 Ini|30")

    fnAdd("Catalytic Collar",                 1300,          2,     false,     gciArmorCode,   "CollarZeke", "Atk|60 Prt|7 Ini|30") --Tier 3
    fnAdd("Catalytic Collar +",               8000,          2,     false,     gciArmorCode,   "CollarZeke", "Atk|80 Prt|7 Ini|30")
    
    fnAdd("Galactic Collar",                 10000,          3,     false,     gciArmorCode,   "CollarZeke", "Atk|90 Prt|10 Ini|50 ResCrd|5 ResPsn|5") --Tier 4
    fnAdd("Galactic Collar +",               30000,          3,     false,     gciArmorCode,   "CollarZeke", "Atk|120 Prt|10 Ini|50 ResCrd|5 ResPsn|5")

    -- |[Accessories Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22  | Stats Array

    -- |[Combat Items Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22      | Stats Array

    -- |[Descriptions]|
    --Weapons
    --Armors
    fnSetDescription("Zeke's Bell Collar",  "A simple leather collar with a bell on it, preventing the notoriously stealthy goat from sneaking up on people.")
    fnSetDescription("Thick Collar",        "Made of Trafalian leather, the extra thickness helps Zeke hit people with his head. Don't ask how.")
    fnSetDescription("Thick Collar +",      "Made of Trafalian leather, the extra thickness helps Zeke hit people with his head. Don't ask how.[BR]Enhanced by Miso's alchemical wizardry.")
    fnSetDescription("Shimmering Collar",   "A powerful magical collar. It makes Zeke irresistably cute, and also he hits people harder? Goats are already pretty mysterious, don't ask.")
    fnSetDescription("Shimmering Collar +", "A powerful magical collar. It makes Zeke irresistably cute, and also he hits people harder? Goats are already pretty mysterious, don't ask."..
                                            "[BR]Enhanced by Miso's alchemical wizardry.")
    fnSetDescription("Catalytic Collar",    "A leather collar that makes goat metabolism run faster and allows them to perform superhuman actions. This might also be a total lie and it's a normal collar.")
    fnSetDescription("Catalytic Collar +",  "A leather collar that makes goat metabolism run faster and allows them to perform superhuman actions. This might also be a total lie and it's a normal collar."..
                                            "[BR]Enhanced by Miso's alchemical wizardry.")
    fnSetDescription("Galactic Collar",     "A collar blessed by the great Galactic Goat, who nobody even thinks exists. Might just be a normal collar.")
    fnSetDescription("Galactic Collar +",   "A collar blessed by the great Galactic Goat, who nobody even thinks exists. Might just be a normal collar.[BR]Enhanced by Miso's alchemical wizardry.")

    --Accessories
    --Combat Items

    -- |[Store]|
    --Store the chart under a unique name.
    gzZekeItemChart = gzActiveChart
    gzActiveChart = nil

end

-- |[ ==================================== Item Is On Chart ==================================== ]|
--If the item is on the prefabricated chart, we can create it algorithmically.
for i = 1, #gzZekeItemChart, 1 do
    if(gzZekeItemChart[i].sName == gsItemName) then

        --Quick-access copy.
        local zItem = gzZekeItemChart[i]

        --Create item.
        AdInv_CreateItem(gsItemName)
        
            -- |[System]|
            AdItem_SetProperty("Value", zItem.iValue)
            AdItem_SetProperty("Gem Slots", zItem.iGemSlots)
            AdItem_SetProperty("Is Key Item", zItem.bIsKeyItem)
            
            -- |[Display]|
            AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/" .. zItem.sSymbol22)
            AdItem_SetProperty("Description", zItem.sDescription)
            
            -- |[Usability]|
            AdItem_SetProperty("Is Equipment", true)
            AdItem_SetProperty("Add Equippable Character", "Zeke")
            
            -- |[Stat Array]|
            for p = 1, #zItem.iaStatArray, 2 do
                AdItem_SetProperty("Statistic", zItem.iaStatArray[p+0], zItem.iaStatArray[p+1])
            end
            
            -- |[Usability / Description]|
            --Weapons:
            if(zItem.iUsability == gciWeaponCode) then
                --Zeke doesn't have a weapon.
            
            --Armors:
            elseif(zItem.iUsability == gciArmorCode) then
                AdItem_SetProperty("Type", "Armor (Zeke)")
                AdItem_SetProperty("Add Equippable Slot", "Collar")
                fnStandardizeDescription(gcs_UI_DescriptionStd_Armor, gci_UI_AdItem_Max_Description_Lines)
            
            --Accessories:
            elseif(zItem.iUsability == gciAccessoryCode) then
                AdItem_SetProperty("Type", "Accessory (Zeke)")
                AdItem_SetProperty("Add Equippable Slot", "Accessory A")
                AdItem_SetProperty("Add Equippable Slot", "Accessory B")
                fnStandardizeDescription(gcs_UI_DescriptionStd_Accessory, gci_UI_AdItem_Max_Description_Lines)
            
            --Combat Items:
            elseif(zItem.iUsability == gciItemCode) then
                AdItem_SetProperty("Type", "Combat Item (Zeke)")
                AdItem_SetProperty("Add Equippable Slot", "Item A")
                AdItem_SetProperty("Add Equippable Slot", "Item B")
                AdItem_SetProperty("Ability Path", zItem.sAbilityPath)
                fnStandardizeDescription(gcs_UI_DescriptionStd_CombatItem, gci_UI_AdItem_Max_Description_Lines)
            
            end
        DL_PopActiveObject()

        --Finish up.
        gbFoundItem = true
        return
    end
end

-- |[ ====================================== Unique Items ====================================== ]|
--Items that do not fit on the chart are created here. These are items that deviate from the normal
-- creation pattern for some reason.

--No items yet.
