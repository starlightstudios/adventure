-- |[ ===================================== Items for Sanya ==================================== ]|
--Items that only Sanya can use, such as her weapons and runestone.

-- |[ ========================================= Charts ========================================= ]|
--For equipment which can be made as part of a chart, construct it here.
if(gzSanyaItemsChart == nil) then
    
    --Setup. All functions operate on this chart.
    gzActiveChart = {}
    
    --Make aliases for the functions. This shortens the chart for readability.
    local fnAdd            = fnAddItemToChartMarkdown
    local fnAddWpn         = fnAddWeaponToChartMarkdown
    local fnAddAbility     = fnAddAbilityToChart
    local fnSetDescription = fnSetDescriptionOnChart

    -- |[Weapons Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22             | Damage Type           | Stats Array
    fnAddWpn("Bare Hands",                       0,          0,      true,    gciWeaponCode,         "WepSanyaFist", gciDamageType_Striking, "Atk|1")
    fnAddWpn("Gloved Hands",                     0,          0,      true,    gciWeaponCode,         "WepSanyaFist", gciDamageType_Striking, "Atk|1")
    fnAddWpn("Enfield Rifle",                 1000,          0,      true,    gciWeaponCode,        "WepSanyaRifle", gciDamageType_Piercing, "Atk|30")
    fnAddWpn("Slotted Enfield Rifle",         3000,          2,      true,    gciWeaponCode,        "WepSanyaRifle", gciDamageType_Piercing, "Atk|30")
    
    -- |[Ammo Chart]|
    fnAddWpn("Anger",                            0,          0,     false, gciSanyaAmmoCode,        "AmmoSanyaRage", gciDamageType_Striking,  "Atk|1")
    fnAddWpn("7.62x56mmR Rounds",              100,          0,     false, gciSanyaAmmoCode,         "AmmoSanyaPrc", gciDamageType_Piercing,  "Atk|10") --Tier 0
    fnAddWpn("Musket Rounds",                  100,          0,     false, gciSanyaAmmoCode,         "AmmoSanyaPrc", gciDamageType_Piercing,  "Atk|40 Acc|-20") --Tier 1
    fnAddWpn("Enchanted Cartridges",           100,          0,     false, gciSanyaAmmoCode,         "AmmoSanyaPrc", gciDamageType_Piercing,  "Atk|20")
    fnAddWpn("Flame Rounds",                   200,          0,     false, gciSanyaAmmoCode,         "AmmoSanyaPrc", gciDamageType_Flaming,   "Atk|15")
    fnAddWpn("Enchanted Cartridges +",         500,          0,     false, gciSanyaAmmoCode,         "AmmoSanyaPrc", gciDamageType_Piercing,  "Atk|50")
    fnAddWpn("AP Rounds",                      600,          0,     false, gciSanyaAmmoCode,         "AmmoSanyaPrc", gciDamageType_Piercing,  "Atk|55") --Tier 2
    fnAddWpn("Starfire Rounds",                600,          0,     false, gciSanyaAmmoCode,         "AmmoSanyaPrc", gciDamageType_Crusading, "Atk|55")
    fnAddWpn("AP Rounds +",                   1200,          0,     false, gciSanyaAmmoCode,         "AmmoSanyaPrc", gciDamageType_Piercing,  "Atk|85 Acc|20 Evd|10")
    fnAddWpn("Manhunter Rounds",              1320,          0,     false, gciSanyaAmmoCode,         "AmmoSanyaPrc", gciDamageType_Piercing,  "Atk|90 Acc|40 Evd|10") --Tier 3
    fnAddWpn("Manhunter Rounds +",            2800,          0,     false, gciSanyaAmmoCode,         "AmmoSanyaPrc", gciDamageType_Piercing,  "Atk|130 Acc|40 Evd|10")
    fnAddWpn("Diamant Rounds",                3000,          0,     false, gciSanyaAmmoCode,         "AmmoSanyaPrc", gciDamageType_Piercing,  "Atk|140 Acc|40 Evd|15 Ini|10") --Tier 4
    fnAddWpn("Diamant Rounds +",             10000,          0,     false, gciSanyaAmmoCode,         "AmmoSanyaPrc", gciDamageType_Piercing,  "Atk|200 Acc|50 Evd|25 Ini|15")

    -- |[Armors Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22      | Stats Array
    fnAdd("Sanya's Jacket",                    100,          0,     false,     gciArmorCode,    "ArmorSanya", "Prt|Flat|1 Ini|Flat|20")
    fnAdd("Hunter's Jacket",                   200,          1,     false,     gciArmorCode,    "ArmorSanya", "Prt|Flat|3 Ini|Flat|20 ResFrz|Flat|3") --Tier 0
    
    fnAdd("Warrior's Pelt",                    300,          1,     false,     gciArmorCode,    "ArmorSanya", "Prt|Flat|3 Ini|Flat|20 Atk|Flat|15 ResFrz|Flat|3") --Tier 1
    fnAdd("Warrior's Pelt +",                  500,          1,     false,     gciArmorCode,    "ArmorSanya", "Prt|Flat|3 Ini|Flat|20 Atk|Flat|23 ResFrz|Flat|3")
    
    fnAdd("Hunter's Armor",                    600,          1,     false,     gciArmorCode,    "ArmorSanya", "Prt|Flat|3 Ini|Flat|20 Atk|Flat|25 ResFrz|Flat|3") --Tier 2
    fnAdd("Hunter's Armor +",                  900,          1,     false,     gciArmorCode,    "ArmorSanya", "Prt|Flat|4 Ini|Flat|20 Atk|Flat|35 ResFrz|Flat|3")
    
    fnAdd("Marksman's Armor",                 1000,          2,     false,     gciArmorCode,    "ArmorSanya", "Prt|Flat|5 Ini|Flat|20 Atk|Flat|40 Acc|5 Evd|5 ResFrz|Flat|3") --Tier 3
    fnAdd("Marksman's Armor +",               1700,          2,     false,     gciArmorCode,    "ArmorSanya", "Prt|Flat|6 Ini|Flat|20 Atk|Flat|55 Acc|5 Evd|5 ResFrz|Flat|3")
    
    fnAdd("Crackshot Cuirass",                2000,          3,     false,     gciArmorCode,    "ArmorSanya", "Prt|Flat|6 Ini|Flat|20 Atk|Flat|60 Acc|5 Evd|5 ResFrz|Flat|3") --Tier 4
    fnAdd("Crackshot Cuirass +",              5000,          3,     false,     gciArmorCode,    "ArmorSanya", "Prt|Flat|7 Ini|Flat|20 Atk|Flat|75 Acc|5 Evd|5 ResFrz|Flat|3")

    -- |[Accessories Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22      | Stats Array
    fnAdd("Ammo Bag",                          200,          2,     false, gciAccessoryCode,    "JarGreen", "Ini|Flat|30 Evd|Flat|-15")

    -- |[Combat Items Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22      | Stats Array
    fnAdd("Crimson Runestone",                  -1,          0,      true,      gciItemCode,     "RuneSanya", "")
    fnAddAbility("Crimson Runestone", gsRoot .. "Combat/Party/Items/Abilities/Crimson Runestone.lua")

    -- |[Descriptions]|
    --Weapons
    fnSetDescription("Bare Hands",            "Sanya doesn't have a weapon right now. In case you missed it, she's also carrying a cute fox girl. She can't fight!")
    fnSetDescription("Gloved Hands",          "Sanya doesn't have a weapon right now. Good luck with any fights because they aren't gonna go well.")
    fnSetDescription("Enfield Rifle",         "A WW2-era Enfield rifle, manufactured by the British and paradropped to partisans in modern-day Croatia. It[BR]"..
                                              "has been in Sanya's family since then.[BR]Damage type is based on the ammo used.")
    fnSetDescription("Slotted Enfield Rifle", "A WW2-era Enfield rifle, manufactured by the British and paradropped to partisans in modern-day Croatia. It[BR]"..
                                              "has been in Sanya's family since then.[BR]It has been improved with two sockets to allow gems to enhance it. Note the damage-type gems will not work here.[BR]"..
                                              "Damage type is based on the ammo used.")
    
    --Ammo
    fnSetDescription("Anger",                  "The only ammo a Croat needs is their unbridled rage.[BR]Unfortunately it's considerably less effective than bullets.[BR]Deals striking damage.")
    fnSetDescription("7.62x56mmR Rounds",      "Also known a 303 rounds in the imperial measurement system. High-power cartridges meant for battle rifles.[BR]Has excellent power and accuracy.[BR]Deals piercing damage.")
    fnSetDescription("Musket Rounds",          "Musket balls in flammable cloth bags with the powder charge beneath. More powerful than the 7.62 round[BR]because magic gunpowder is cheating![BR]Also a lot less accurate." .. 
                                               "[BR]Deals piercing damage.")
    fnSetDescription("Enchanted Cartridges",   "Cartridges that miraculously reload themselves with ambient magic! The future is here![BR]Deals piercing damage.")
    fnSetDescription("Enchanted Cartridges +", "Cartridges that miraculously reload themselves with ambient magic! The future is here![BR]Enhanced by Miso's alchemical wizardry.[BR]Deals piercing damage.")
    fnSetDescription("Flame Rounds",           "Rounds that ignite into flames when fired, which is metal as hell and also very useful against some targets.[BR]Deals flaming damage.")
    fnSetDescription("AP Rounds",              "Dense rounds for Sanya's rifle, designed to punch through armor.[BR]Deals piercing damage.")
    fnSetDescription("AP Rounds +",            "Dense rounds for Sanya's rifle, designed to punch through armor.[BR]Enhanced by Miso's alchemical wizardry.[BR]Deals piercing damage.")
    fnSetDescription("Starfire Rounds",        "Magical rounds that explode in blinding light on contact. It's like an angel's blessing of lead![BR]Deals crusading damage.")
    fnSetDescription("Manhunter Rounds",       "Rounds that actually curve slightly in the air to seek out targets.[BR]Deals piercing damage.")
    fnSetDescription("Manhunter Rounds +",     "Rounds that actually curve slightly in the air to seek out targets.[BR]Enhanced by Miso's alchemical wizardry.[BR]Deals piercing damage.")
    fnSetDescription("Diamant Rounds",         "The head of the bullet is made of a diamond bit that is far denser than lead. Who knows how it works, but boy does it.[BR]Deals piercing damage.")
    fnSetDescription("Diamant Rounds +",       "The head of the bullet is made of a diamond bit that is far denser than lead. Who knows how it works, but boy does it.[BR]Enhanced by Miso's alchemical wizardry."..
                                               "[BR]Deals piercing damage.")
    
    --Armors
    fnSetDescription("Sanya's Jacket",      "Sanya's red jacket. Light and meant for quick movements, does not restrict the arms.[BR]Obviously not designed for melee combat.")
    fnSetDescription("Hunter's Jacket",     "A thicker jacket made of leather and fiber from Trafal. Provides excellent protection while being light, and[BR]resists freezing damage.")
    fnSetDescription("Warrior's Pelt",      "The stitched pelts of Trafal's animals. Really gives off that barbarian vibe, and increases attack power to boot!")
    fnSetDescription("Warrior's Pelt +",    "The stitched pelts of Trafal's animals. Really gives off that barbarian vibe, and increases attack power to boot![BR]Enhanced by Miso's alchemical wizardry.")
    fnSetDescription("Hunter's Armor",      "Cloth armor with chitin plates from fallen monsters gives protection, mobility, and that sweet, sweet attack power.")
    fnSetDescription("Hunter's Armor +",    "Cloth armor with chitin plates from fallen monsters gives protection, mobility, and that sweet, sweet attack power.[BR]Enhanced by Miso's alchemical wizardry.")
    fnSetDescription("Marksman's Armor",    "A thick jacket issued to professional fusiliers, designed to take a hit or two without restricting the arms.")
    fnSetDescription("Marksman's Armor +",  "A thick jacket issued to professional fusiliers, designed to take a hit or two without restricting the arms.[BR]Enhanced by Miso's alchemical wizardry.")
    fnSetDescription("Crackshot Cuirass",   "A metal chestpiece meant to be used by cavalry. It's thick and leaves your arms free to reload or strangle people.")
    fnSetDescription("Crackshot Cuirass +", "A metal chestpiece meant to be used by cavalry. It's thick and leaves your arms free to reload or strangle people.[BR]Enhanced by Miso's alchemical wizardry.")
    
    --Accessories
    fnSetDescription("Ammo Bag", "A bag to store all your ammo at the hip. Slightly reduces your evade, but makes initiative higher because loading[BR]is faster.")
    
    --Combat Items
    fnSetDescription("Crimson Runestone", "A crimson runestone with a black marking on it. Next attack will always critically strike when used in combat.[BR]Key Item.")

    -- |[Store]|
    --Store the chart under a unique name.
    gzSanyaItemsChart = gzActiveChart
    gzActiveChart = nil

end

-- |[ ==================================== Item Is On Chart ==================================== ]|
--If the item is on the prefabricated chart, we can create it algorithmically.
for i = 1, #gzSanyaItemsChart, 1 do
    if(gzSanyaItemsChart[i].sName == gsItemName) then

        --Quick-access copy.
        local zItem = gzSanyaItemsChart[i]

        --Create item.
        AdInv_CreateItem(gsItemName)
        
            -- |[System]|
            AdItem_SetProperty("Value", zItem.iValue)
            AdItem_SetProperty("Gem Slots", zItem.iGemSlots)
            AdItem_SetProperty("Is Key Item", zItem.bIsKeyItem)
            
            -- |[Display]|
            AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/" .. zItem.sSymbol22)
            AdItem_SetProperty("Description", zItem.sDescription)

            -- |[Usability]|
            AdItem_SetProperty("Is Equipment", true)
            AdItem_SetProperty("Add Equippable Character", "Sanya")
            
            -- |[Stat Array]|
            for p = 1, #zItem.iaStatArray, 2 do
                AdItem_SetProperty("Statistic", zItem.iaStatArray[p+0], zItem.iaStatArray[p+1])
            end
            
            -- |[Usability / Description]|
            --Weapons. Note that Sanya does not use her weapon for animation properties.
            if(zItem.iUsability == gciWeaponCode) then
                
                --Type.
                AdItem_SetProperty("Type", "Firearm")
            
                --Equipment slots. 
                AdItem_SetProperty("Add Equippable Slot", "Weapon")
                
                --Description.
                fnStandardizeDescription(gcs_UI_DescriptionStd_Weapon, gci_UI_AdItem_Max_Description_Lines)
            
            --Ammunition. This is what Sanya uses for animations.
            elseif(zItem.iUsability == gciSanyaAmmoCode) then
                
                --Type.
                AdItem_SetProperty("Type", "Ammunition")
            
                --Equipment slots. 
                AdItem_SetProperty("Add Equippable Slot", "Ammo")
                AdItem_SetProperty("Add Equippable Slot", "Ammo Backup")
                
                --Animations and Sound
                if(gsItemName ~= "Anger") then
                    AdItem_SetProperty("Equipment Attack Animation", "Gun Shot")
                    AdItem_SetProperty("Equipment Attack Sound",     "Combat\\|Impact_Rifle")
                    AdItem_SetProperty("Equipment Critical Sound",   "Combat\\|Impact_Rifle_Crit")
                else
                    AdItem_SetProperty("Equipment Attack Animation", "Strike")
                    AdItem_SetProperty("Equipment Attack Sound",     "Combat\\|Impact_Strike")
                    AdItem_SetProperty("Equipment Critical Sound",   "Combat\\|Impact_Strike_Crit")
                end
                
                --Damage type.
                AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, zItem.iDamageType + gciDamageOffsetToResistances, 1.0)
                AdItem_SetProperty("Add Tag", zItem.sSecondaryType, 1)
                fnStandardizeDescription(gcs_UI_DescriptionStd_Weapon, gci_UI_AdItem_Max_Description_Lines)
            
            --Armors:
            elseif(zItem.iUsability == gciArmorCode) then
                AdItem_SetProperty("Type", "Armor (Sanya)")
                AdItem_SetProperty("Add Equippable Slot", "Body")
                
                --Description.
                fnStandardizeDescription(gcs_UI_DescriptionStd_Armor, gci_UI_AdItem_Max_Description_Lines)
            
            --Accessories:
            elseif(zItem.iUsability == gciAccessoryCode) then
                AdItem_SetProperty("Type", "Accessory (Sanya)")
                AdItem_SetProperty("Add Equippable Slot", "Accessory A")
                AdItem_SetProperty("Add Equippable Slot", "Accessory B")
                fnStandardizeDescription(gcs_UI_DescriptionStd_Accessory, gci_UI_AdItem_Max_Description_Lines)
            
            --Combat Items:
            elseif(zItem.iUsability == gciItemCode) then
                AdItem_SetProperty("Type", "Combat Item (Sanya)")
                AdItem_SetProperty("Add Equippable Slot", "Item A")
                AdItem_SetProperty("Add Equippable Slot", "Item B")
                AdItem_SetProperty("Ability Path", zItem.sAbilityPath)
                fnStandardizeDescription(gcs_UI_DescriptionStd_CombatItem, gci_UI_AdItem_Max_Description_Lines)
            
            end
        DL_PopActiveObject()

        --Finish up.
        gbFoundItem = true
        return
    end
end

-- |[ ====================================== Unique Items ====================================== ]|
--Items that do not fit on the chart are created here. These are items that deviate from the normal
-- creation pattern for some reason.

--No items yet.
