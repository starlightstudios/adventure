-- |[ ================================== Items for Christine =================================== ]|
--Items that only Christine can use, such as her weapons and runestone.

-- |[ ========================================= Charts ========================================= ]|
--For equipment which can be made as part of a chart, construct it here.
if(gzChristineItemChart == nil) then
    
    --Setup. All functions operate on this chart.
    gzActiveChart = {}
    
    --Make aliases for the functions. This shortens the chart for readability.
    local fnAdd            = fnAddItemToChart
    local fnAddWpn         = fnAddWeaponToChartChristine
    local fnAddAbility     = fnAddAbilityToChart
    local fnSetDescription = fnSetDescriptionOnChart

    -- |[Weapons Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22          | Damage Type           | Secondary Type              | Stats Array
    fnAddWpn("Tazer",                           -1,          0,      true,    gciWeaponCode, "WepChrisTazer",     gciDamageType_Striking, "Christine Secondary Shock",  {gciStatIndex_Attack,  10})
    fnAddWpn("Carbonweave Electrospear",       200,          2,     false,    gciWeaponCode, "WepChristineShock", gciDamageType_Piercing, "Christine Secondary Shock",  {gciStatIndex_Attack,  30})
    fnAddWpn("Cillium Cryospear",              200,          2,     false,    gciWeaponCode, "WepChristineIce",   gciDamageType_Piercing, "Christine Secondary Freeze", {gciStatIndex_Attack,  33})
    fnAddWpn("Niobium Thermospear",            200,          2,     false,    gciWeaponCode, "WepChristineFire",  gciDamageType_Piercing, "Christine Secondary Flame",  {gciStatIndex_Attack,  55})
    fnAddWpn("Silksteel Electrospear",         350,          2,     false,    gciWeaponCode, "WepChristineShock", gciDamageType_Piercing, "Christine Secondary Shock",  {gciStatIndex_Attack,  50})
    fnAddWpn("Silksteel Electrohalberd",       420,          1,     false,    gciWeaponCode, "WepChristineShock", gciDamageType_Slashing, "Christine Secondary Shock",  {gciStatIndex_Attack,  73, gciStatIndex_Initiative, -5})
    fnAddWpn("Yttrium Electrospear",           570,          2,     false,    gciWeaponCode, "WepChristineShock", gciDamageType_Piercing, "Christine Secondary Shock",  {gciStatIndex_Attack,  65})
    fnAddWpn("Lithium Thermospear",            730,          2,     false,    gciWeaponCode, "WepChristineFire",  gciDamageType_Piercing, "Christine Secondary Flame",  {gciStatIndex_Attack,  83})
    fnAddWpn("Magnetide Cryospear",            710,          2,     false,    gciWeaponCode, "WepChristineIce",   gciDamageType_Piercing, "Christine Secondary Freeze", {gciStatIndex_Attack,  81})
    fnAddWpn("Supersteel Electrohalberd",      900,          1,     false,    gciWeaponCode, "WepChristineShock", gciDamageType_Slashing, "Christine Secondary Shock",  {gciStatIndex_Attack, 110, gciStatIndex_Initiative, -5})
    fnAddWpn("Tellurine Electrospear",        1500,          3,     false,    gciWeaponCode, "WepChristineShock", gciDamageType_Piercing, "Christine Secondary Shock",  {gciStatIndex_Attack, 125})

    -- |[Armors Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22      | Stats Array
    fnAdd("Schoolmaster's Suit",                -1,          0,      true,     gciArmorCode, "ArmorChris",     {gciStatIndex_Protection, 1, gciStatIndex_Initiative, 20})
    fnAdd("Lord Golem Dress",                   -1,          1,      true,     gciArmorCode, "ArmorChristine", {gciStatIndex_Protection, 3})

    -- |[Accessories Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22      | Stats Array
    fnAdd("Kinetic Capacitor",                 300,          0,     false, gciAccessoryCode, "ItemTech",      {gciStatIndex_Attack,   10})
    fnAdd("Viewfinder Module",                 300,          0,     false, gciAccessoryCode, "ItemScope",     {gciStatIndex_Accuracy, 15})
    fnAdd("Sure-Grip Gloves",                  300,          0,     false, gciAccessoryCode, "AccGlovesBlue", {gciStatIndex_Attack,    8, gciStatIndex_Accuracy, 5})
    fnAdd("Spear Foregrip",                    300,          0,     false, gciAccessoryCode, "ItemTech",      {gciStatIndex_Accuracy, 15})

    -- |[Combat Items Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22      | Stats Array
    fnAdd("Violet Runestone",                   -1,          0,      true,      gciItemCode, "RuneChristine", {})
    fnAdd("Violet Runestone Mk II",             -1,          0,      true,      gciItemCode, "RuneChristine", {})
    fnAddAbility("Violet Runestone",       gsRoot .. "Combat/Party/Items/Abilities/Violet Runestone.lua")
    fnAddAbility("Violet Runestone Mk II", gsRoot .. "Combat/Party/Items/Abilities/Violet Runestone Mk II.lua")

    -- |[Descriptions]|
    --Weapons
    fnSetDescription("Tazer",                    "It looks vaguely like a cattle prod with a handle. Better than nothing.[BR]Deals strike/shock damage.")
    fnSetDescription("Carbonweave Electrospear", "A carbon-fibre spear with built-in fission battery.[BR]Light and strong, it deals pierce/shock damage.")
    fnSetDescription("Cillium Cryospear",        "A spear designed to siphon heat when discharged through a cillium-gas lockpump.[BR]Deals pierce/freeze damage.")
    fnSetDescription("Niobium Thermospear",      "A spear infused with heat-trapping niobium that can discharge passive heat at the tip in violation of[BR]thermodynamics.[BR]Deals pierce/flame damage.")
    fnSetDescription("Silksteel Electrospear",   "A model of electrospear made out of woven threads of steel. It holds together like a spider-web and is[BR]extraordinarily strong.[BR]Deals pierce/shock damage.")
    fnSetDescription("Silksteel Electrohalberd", "A large axe/spear hybrid made of woven threads of steel. Heavy, and slows the user, but is more powerful for[BR]its material class.[BR]Deals slash/shock damage.")
    fnSetDescription("Yttrium Electrospear",     "An electrospear whose tip is coated with a superconductive yttrium alloy.[BR]Deals pierce/shock damage.")
    fnSetDescription("Lithium Thermospear",      "An improvement on the thermospear line using lithium crystals to store heat buildup.[BR]Deals pierce/flame damage.")
    fnSetDescription("Magnetide Cryospear",      "By creating a pulse of negative pressure between arranged crystals, the spear can siphon heat out of a target.[BR]Deals pierce/freeze damage.")
    fnSetDescription("Supersteel Electrohalberd","A halberd made of so-called supersteel that alloys a variety of metals into steel, doubling its weight and[BR]quintupling its strength.[BR]Deals slash/shock damage.")
    fnSetDescription("Tellurine Electrospear",   "An electrospear made of an alloy that does not exist yet.[BR]Deals pierce/shock damage.")

    --Armors
    fnSetDescription("Schoolmaster's Suit",      "Clothes that Chris wore at his job as an English Teacher. Not suited for combat.")
    fnSetDescription("Lord Golem Dress",         "A beautiful dress given to 771852 upon conversion. Not combat attire, but still extremely durable.")

    --Accessories
    fnSetDescription("Kinetic Capacitor",        "A special module added to the battery of an electric weapon. Stores up charge from the user's movement[BR]through passive electrical fields. Increases damage.")
    fnSetDescription("Viewfinder Module",        "Removable module for 771852's PDU that highlights enemy weak points and predicts their movements.[BR]Increases accuracy.")
    fnSetDescription("Sure-Grip Gloves",         "Gloves with high-friction padding in the palms. Vastly increases weapon grip, boosting damage and accuracy.")
    fnSetDescription("Spear Foregrip",           "A high-friction grip near the head of the spear. Simple, but very effective for precision strikes.")

    --Combat Items
    fnSetDescription("Violet Runestone",         "A violet runestone with a black marking on it. Restores 20%% of max HP when used in combat.[BR]Key Item.")
    fnSetDescription("Violet Runestone Mk II",   "A violet runestone with a black marking on it. Restores 30%% of max HP when used in combat and increases[BR]attack power.[BR]Key Item.")

    -- |[Store]|
    --Store the chart under a unique name.
    gzChristineItemChart  = gzActiveChart
    gzActiveChart = nil

end

-- |[ ==================================== Item Is On Chart ==================================== ]|
--If the item is on the prefabricated chart, we can create it algorithmically.
for i = 1, #gzChristineItemChart, 1 do
    if(gzChristineItemChart[i].sName == gsItemName) then

        --Check if Christine has finished Cryogenics. If not, switch the name to "Chris". This only
        -- refers to typing on some items, not the usability flags.
        local sUseCharacterName = "Christine"
        local iIsPostGolemScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsPostGolemScene", "N")
        if(iIsPostGolemScene == 0) then sUseCharacterName = "Chris" end

        --Quick-access copy.
        local zItem = gzChristineItemChart[i]

        --Create item.
        AdInv_CreateItem(gsItemName)
        
            -- |[System]|
            AdItem_SetProperty("Value", zItem.iValue)
            AdItem_SetProperty("Gem Slots", zItem.iGemSlots)
            AdItem_SetProperty("Is Key Item", zItem.bIsKeyItem)
            
            -- |[Display]|
            AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/" .. zItem.sSymbol22)
            AdItem_SetProperty("Description", zItem.sDescription)
            -- |[Usability]|
            AdItem_SetProperty("Is Equipment", true)
            AdItem_SetProperty("Add Equippable Character", "Christine")
            
            -- |[Stat Array]|
            for p = 1, #zItem.iaStatArray, 2 do
                AdItem_SetProperty("Statistic", zItem.iaStatArray[p+0], zItem.iaStatArray[p+1])
            end
            
            -- |[Usability / Description]|
            --Weapons:
            if(zItem.iUsability == gciWeaponCode) then
                
                --Type.
                AdItem_SetProperty("Type", "Spear")
                
                --Exception: Tazer is not a spear.
                if(gsItemName == "Tazer") then
                    AdItem_SetProperty("Type", "Stun Gun")
                end
            
                --Equipment slots.
                AdItem_SetProperty("Add Equippable Slot", "Weapon")
                AdItem_SetProperty("Add Equippable Slot", "Weapon Backup A")
                AdItem_SetProperty("Add Equippable Slot", "Weapon Backup B")
                
                --Animations and Sound
                AdItem_SetProperty("Equipment Attack Animation", "Sword Slash")
                AdItem_SetProperty("Equipment Attack Sound",     "Combat\\|Impact_CrossSlash")
                AdItem_SetProperty("Equipment Critical Sound",   "Combat\\|Impact_CrossSlash_Crit")
                
                --Damage type.
                AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, zItem.iDamageType + gciDamageOffsetToResistances, 1.0)
                AdItem_SetProperty("Add Tag", zItem.sSecondaryType, 1)
                fnStandardizeDescription(gcs_UI_DescriptionStd_Weapon, gci_UI_AdItem_Max_Description_Lines)
            
            --Armors:
            elseif(zItem.iUsability == gciArmorCode) then
                AdItem_SetProperty("Type", "Armor (" .. sUseCharacterName .. ")")
                AdItem_SetProperty("Add Equippable Slot", "Body")
                fnStandardizeDescription(gcs_UI_DescriptionStd_Armor, gci_UI_AdItem_Max_Description_Lines)
            
            --Accessories:
            elseif(zItem.iUsability == gciAccessoryCode) then
                AdItem_SetProperty("Type", "Accessory (" .. sUseCharacterName .. ")")
                AdItem_SetProperty("Add Equippable Slot", "Accessory A")
                AdItem_SetProperty("Add Equippable Slot", "Accessory B")
                fnStandardizeDescription(gcs_UI_DescriptionStd_Accessory, gci_UI_AdItem_Max_Description_Lines)
            
            --Combat Items:
            elseif(zItem.iUsability == gciItemCode) then
                AdItem_SetProperty("Type", "Combat Item (" .. sUseCharacterName .. ")")
                AdItem_SetProperty("Add Equippable Slot", "Item A")
                AdItem_SetProperty("Add Equippable Slot", "Item B")
                AdItem_SetProperty("Ability Path", zItem.sAbilityPath)
                fnStandardizeDescription(gcs_UI_DescriptionStd_CombatItem, gci_UI_AdItem_Max_Description_Lines)
            
            end
        DL_PopActiveObject()

        --Finish up.
        gbFoundItem = true
        return
    end
end

-- |[ ========================================= Badges ========================================= ]|
--Badges never have stats, and are only used to make enemy types not attack the player.
if(gzChristineBadgeList == nil) then
    
    --List.
    gzChristineBadgeList = {}
    
    --Adder Function.
    local function fnAddBadge(psName, psSymbol, psDescription)
        local zObject = {}
        zObject.sName = psName
        zObject.sDescription = psDescription
        zObject.sSymbol = "Root/Images/AdventureUI/Symbols22/" .. psSymbol
        table.insert(gzChristineBadgeList, zObject)
    end

    --Suffix.
    local sSuffix = "[BR]Badges are obtained automatically upon entering a form for the first time."
    
    --Listing.
    fnAddBadge("Voidguard Badge", "Badge|Darkmatter", "A badge with a black hole on it. Darkmatters will know you're a friend if you wear this." .. sSuffix)
end

--Scan the badges for a match.
for i = 1, #gzChristineBadgeList, 1 do
    if(gzChristineBadgeList[i].sName == gsItemName) then
    
        --Create.
        AdInv_CreateItem(gsItemName)
            AdItem_SetProperty("Description", gzChristineBadgeList[i].sDescription)
            AdItem_SetProperty("Type", "Badge")
            AdItem_SetProperty("Value", 0)
            AdItem_SetProperty("Is Key Item", true)
            AdItem_SetProperty("Is Equipment", true)
            AdItem_SetProperty("Add Equippable Character", "Christine")
            AdItem_SetProperty("Add Equippable Slot", "Badge")
            AdItem_SetProperty("Rendering Image", gzChristineBadgeList[i].sSymbol)
            fnStandardizeDescription(gcs_UI_DescriptionStd_Misc, gci_UI_AdItem_Max_Description_Lines)
        DL_PopActiveObject()
    
        --Finish.
        gbFoundItem = true
        return
    end
end

-- |[ ====================================== Unique Items ====================================== ]|
--Items that do not fit on the chart are created here. These are items that deviate from the normal
-- creation pattern for some reason.

--No items yet.
