-- |[ ================================== Sanchez 30pt Kerning ================================== ]|
--Sets the kerning values for the listed font.

-- |[Main Scaler]|
--This sets the standard distance between letters.
StarFont_SetKerning(1.0)

-- |[Letter Groupings]|
--The value -1 indicates "All Letters". Use this for thin/thick letters.
--StarFont_SetKerning(string.byte("i"), -1, 3.0)
--StarFont_SetKerning(-1, string.byte("i"), 2.0)

--Punctuation groupings.
StarFont_SetKerning(-1, string.byte(":"), 2.0)
StarFont_SetKerning(-1, string.byte("!"), 3.0)
StarFont_SetKerning(-1, string.byte("."), 2.0)
StarFont_SetKerning(-1, string.byte(","), 2.0)

--Space Sizings.
--StarFont_SetKerning(string.byte(" "), -1, 10.0)
--StarFont_SetKerning(-1, string.byte(" "), 0.0)

-- |[Punctuation-to-Punctuation]|
--StarFont_SetKerning(string.byte("."), string.byte("."), 0.0)

-- |[Numbers-To-Anything]|
--StarFont_SetKerning(string.byte("1"), string.byte("."), 3.0)

-- |[Letters-to-Letter]|
--StarFont_SetKerning(string.byte("t"), string.byte("k"), 3.0)

-- |[Letter-to-Punctuation]|
--StarFont_SetKerning(string.byte("r"), string.byte(","), 0.0)
