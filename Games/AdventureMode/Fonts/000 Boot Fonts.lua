-- |[ ======================================= Boot Fonts ======================================= ]|
--Boots fonts used by Adventure Mode
if(gbBootedAdventureFonts == true) then return end
gbBootedAdventureFonts = true

-- |[Replacement]|
--If this is variable is not nil, call this script instead of the current one. This replacement is
-- typically used by translations that need alternate fonts for different character sets.
if(gsAdventureFontPathReplace ~= nil) then
    LM_ExecuteScript(gsAdventureFontPathReplace, fnResolvePath())
    return
end

-- |[Setup]|
--Debug timer.
local fStartTime = 0.0
LM_StartTimer("Fonts")

--Paths.
local sEngineFontPath   = "Data/Scripts/Fonts/"
local sLocalFontPath    = fnResolvePath()
local sHasNoKerningFile = "Null"

--Special Flags
local ciFontNoFlags = 0
local ciFontNearest    = Font_GetProperty("Constant Precache With Nearest")
local ciFontEdge       = Font_GetProperty("Constant Precache With Edge")
local ciFontDownfade   = Font_GetProperty("Constant Precache With Downfade")
local ciFontSpecialS   = Font_GetProperty("Constant Precache With Special S")
local ciFontSpecialExc = Font_GetProperty("Constant Precache With Special !")
local ciFontUnicode    = Font_GetProperty("Constant Precache Full Unicode")
local ciFontDummy      = Font_GetProperty("Constant Precache Dummy")
local ciFontSpecialDbg = Font_GetProperty("Constant Precache Debug")

-- |[Precompilation Handling]|
local iCompileFlag = 2
if(iCompileFlag == 1) then
    StarFont_BeginCompilation("Data/FontPrebuild/AdventureFontPrecompileEN.slf")
elseif(iCompileFlag == 2) then
    StarFont_OpenCompiledFile("Data/FontPrebuild/AdventureFontPrecompileEN.slf")
end

-- |[ =============================== Title Screen Redirections ================================ ]|
-- |[Witch Hunter Izana]|
--If the game is mandating WHI's title screen, then several fonts get switched to dummy mode. This
-- reduces overhead as the fonts will not precache or upload data. If Adventure is booted after
-- this, it will cause a lot of errors. So don't do that.
if(gsMandateTitle == "Witch Hunter Izana") then
    Font_RegisterAsDummy("Oxygen 27 DF")
    Font_RegisterAsDummy("Sanchez 70 DFO")
    Font_RegisterAsDummy("Sanchez 60 DFO")
    Font_RegisterAsDummy("Sanchez 50 DFO")
    Font_RegisterAsDummy("Sanchez 30 DFO")
    Font_RegisterAsDummy("Trigger 28 DFO")
end

-- |[ ==================================== Font Registration =================================== ]|
-- |[Mister Pixel]|
Font_SetProperty("Downfade", 0.90, 0.60)
Font_SetProperty("Outline Width", 3)
Font_Register("Mister Pixel 120 DFO", sLocalFontPath .. "MisterPixel.ttf", sHasNoKerningFile, 120, ciFontEdge + ciFontDownfade)

Font_SetProperty("Downfade", 0.90, 0.60)
Font_SetProperty("Outline Width", 3)
Font_Register("Mister Pixel 90 DFO", sLocalFontPath .. "MisterPixel.ttf", sHasNoKerningFile, 90, ciFontNearest + ciFontEdge + ciFontDownfade)

Font_SetProperty("Downfade", 0.90, 0.60)
Font_SetProperty("Outline Width", 1)
Font_Register("Mister Pixel 30 DFO", sLocalFontPath .. "MisterPixel.ttf", sHasNoKerningFile, 25, ciFontEdge + ciFontDownfade + ciFontSpecialExc)

Font_SetProperty("Downfade", 0.90, 0.60)
Font_SetProperty("Outline Width", 1)
Font_Register("Mister Pixel 20 DFO", sLocalFontPath .. "MisterPixel.ttf", sHasNoKerningFile, 20, ciFontNearest + ciFontEdge + ciFontDownfade + ciFontSpecialExc)

Font_SetProperty("Downfade", 0.90, 0.60)
Font_SetProperty("Outline Width", 1)
Font_Register("Mister Pixel 16 DFO", sLocalFontPath .. "MisterPixel.ttf", sHasNoKerningFile, 16, ciFontNearest + ciFontEdge + ciFontDownfade)

Font_SetProperty("Outline Width", 1)
Font_Register("Mister Pixel 8 O", sLocalFontPath .. "MisterPixel.ttf", sHasNoKerningFile, 8, ciFontNearest + ciFontEdge)

-- |[Oxygen]|
Font_SetProperty("Downfade", 0.95, 0.75)
Font_Register("Oxygen 27 DF", sLocalFontPath .. "Oxygen-Regular.ttf", sHasNoKerningFile, 27, ciFontDownfade + ciFontSpecialS)

-- |[Sanchez]|
Font_SetProperty("Downfade", 0.90, 0.60)
Font_SetProperty("Outline Width", 3)
Font_Register("Sanchez 70 DFO", sEngineFontPath .. "SanchezRegular.otf", sHasNoKerningFile, 70, ciFontEdge + ciFontDownfade)

Font_SetProperty("Downfade", 0.90, 0.60)
Font_SetProperty("Outline Width", 3)
Font_Register("Sanchez 60 DFO", sEngineFontPath .. "SanchezRegular.otf", sHasNoKerningFile, 60, ciFontNearest + ciFontEdge + ciFontDownfade)

Font_SetProperty("Downfade", 0.90, 0.60)
Font_SetProperty("Outline Width", 3)
Font_Register("Sanchez 50 DFO", sEngineFontPath .. "SanchezRegular.otf", sHasNoKerningFile, 40, ciFontNearest + ciFontEdge + ciFontDownfade)

Font_SetProperty("Downfade", 0.90, 0.60)
Font_SetProperty("Outline Width", 1)
Font_Register("Sanchez 30 DFO", sEngineFontPath .. "SanchezRegular.otf", sHasNoKerningFile, 30, ciFontEdge + ciFontDownfade)

--Sanchez 35 and 22 are created by the EngineFonts.lua booter.

-- |[Trigger]|
Font_SetProperty("Downfade", 0.99, 0.60)
Font_SetProperty("Outline Width", 1)
Font_Register("Trigger 28 DFO", sLocalFontPath .. "TriggerModified.ttf", sHasNoKerningFile, 28, ciFontNearest + ciFontEdge + ciFontDownfade)

-- |[ ========================================= Aliases ======================================== ]|
-- |[Mister Pixel]|
--Mister Pixel 120 DFO
Font_SetProperty("Add Alias", "Mister Pixel 120 DFO", "Adventure Combat Damage Effect")
Font_SetProperty("Add Alias", "Mister Pixel 120 DFO", "Puzzle Battle Center Title")

--Mister Pixel 90 DFO
Font_SetProperty("Add Alias", "Mister Pixel 90 DFO", "Adventure Combat Header")

--Mister Pixel 20 DFO
--Font_SetProperty("Add Alias", "Mister Pixel 20 DFO", "Adventure Combat Description")
Font_SetProperty("Add Alias", "Mister Pixel 20 DFO", "Adventure Menu Small")
Font_SetProperty("Add Alias", "Mister Pixel 20 DFO", "Adventure Menu Costume Small")
Font_SetProperty("Add Alias", "Mister Pixel 20 DFO", "Adventure Menu Doctor Bag Small")
Font_SetProperty("Add Alias", "Mister Pixel 20 DFO", "Adventure Combat Exp")
Font_SetProperty("Add Alias", "Mister Pixel 20 DFO", "Adventure Combat VicItem")
Font_SetProperty("Add Alias", "Mister Pixel 20 DFO", "Carnation Combat Player UI")

--Mister Pixel 16 DFO
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Adventure Combat Ally Bar")
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Adventure Combat Enemy Bar")
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Adventure Combat Small")
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Adventure Menu Status Small")
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Adventure Menu File Select Small")
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Adlev Generator Small")
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Adventure Level UI")
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Context Menu Main")
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Classic Level Small")
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Dance Score")
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Adventure Menu Status Equipment")
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Adventure Level Reinforcement")
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Adventure Menu Journal Achievement Description")

--Mister Pixel 8 O
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Adventure Mug")

-- |[Oxygen]|
--Oxygen 27 DF
Font_SetProperty("Add Alias", "Oxygen 27 DF", "World Dialogue Main")
Font_SetProperty("Add Alias", "Oxygen 27 DF", "World Dialogue Decision")
Font_SetProperty("Add Alias", "Oxygen 27 DF", "World Dialogue Credits Oxy")

-- |[Sanchez]|
--Sanchez 70. Used for double-sized headers.
Font_SetProperty("Add Alias", "Sanchez 70 DFO", "Adventure Menu Base Double Header")
Font_SetProperty("Add Alias", "Sanchez 70 DFO", "Adventure Menu Campfire Double Header")
Font_SetProperty("Add Alias", "Sanchez 70 DFO", "Adventure Menu Equipment Double Header")
Font_SetProperty("Add Alias", "Sanchez 70 DFO", "Adventure Menu Field Abilities Double Header")
Font_SetProperty("Add Alias", "Sanchez 70 DFO", "Adventure Menu File Select Double Header")
Font_SetProperty("Add Alias", "Sanchez 70 DFO", "Adventure Menu Inventory Double Header")
Font_SetProperty("Add Alias", "Sanchez 70 DFO", "Adventure Menu Journal Big Header")
Font_SetProperty("Add Alias", "Sanchez 70 DFO", "Adventure Menu Skills Double Header")
Font_SetProperty("Add Alias", "Sanchez 70 DFO", "Adventure Menu Status Double Header")
Font_SetProperty("Add Alias", "Sanchez 70 DFO", "Adventure Menu Options Double Header")
Font_SetProperty("Add Alias", "Sanchez 70 DFO", "Adventure Menu Trainer Double Header")
Font_SetProperty("Add Alias", "Sanchez 70 DFO", "Adventure Menu Trainer Cash Value")
Font_SetProperty("Add Alias", "Sanchez 70 DFO", "Adventure Menu Vendor Double Header")
Font_SetProperty("Add Alias", "Sanchez 70 DFO", "Adventure Menu Standard Double Header")

--Sanchez 60
Font_SetProperty("Add Alias", "Sanchez 60 DFO", "Adventure Combat New Turn Font")
Font_SetProperty("Add Alias", "Sanchez 60 DFO", "Adventure Combat Confirmation Big")
Font_SetProperty("Add Alias", "Sanchez 60 DFO", "Adventure Menu Status Level")
Font_SetProperty("Add Alias", "Sanchez 60 DFO", "Adventure Menu Base Level")
Font_SetProperty("Add Alias", "Sanchez 60 DFO", "Puzzle Battle Confirm")

--Sanchez 50 DFO.
Font_SetProperty("Add Alias", "Sanchez 50 DFO", "Adventure Combat Description Header")
Font_SetProperty("Add Alias", "Sanchez 50 DFO", "Adventure Combat Player Bar Name")

--Sanchez 35
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "World Dialogue Heading")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Combat Ability Title")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Combat Confirmation Small")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Combat Doctor")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Combat Inspector Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Combat Platina")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Combat Target Cluster Selection")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Base Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Campfire Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Costume Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Doctor Bag Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Equipment Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Field Abilities Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu File Select Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Form Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Gemcutter Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Inventory Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Journal Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Options Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Quit Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Skills Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Status Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Trainer Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Trainer Name")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Vendor Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Standard Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Help Heading")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "String Entry Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Classic Level Main")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "World Dialogue Credits Sanchez")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Puzzle Battle Time")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Puzzle Battle Moves")

--Sanchez 30
Font_SetProperty("Add Alias", "Sanchez 30 DFO", "Adventure Combat Card Title")
Font_SetProperty("Add Alias", "Sanchez 30 DFO", "Adventure Combat Description Simple")
Font_SetProperty("Add Alias", "Sanchez 30 DFO", "Adventure Combat Player Bar")
Font_SetProperty("Add Alias", "Sanchez 30 DFO", "Adventure Menu Skills Simple Description")
Font_SetProperty("Add Alias", "Sanchez 30 DFO", "Adventure Menu Standard Help")
Font_SetProperty("Add Alias", "Sanchez 30 DFO", "Carnation Combat Textbox UI")

--Sanchez 22
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Help Mainline")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Combat Description")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Combat Inspector Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Combat Help")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Base Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Campfire Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Doctor Bag Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Equipment Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Field Abilities Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu File Select Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Form Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Gemcutter Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Inventory Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Journal Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Journal Achievement Title")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Options Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Skills Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Skills Description")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Status Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Trainer Help")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Trainer Statistics")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Trainer Payment Help")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Vendor Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Standard Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "String Entry Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adlev Generator Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Puzzle Battle Confirm Small")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Level Notifications")

-- |[Trigger]|
--Trigger 28 DFO
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Adventure Combat Main")
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Adventure Combat Prediction")
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Adventure Combat Inspector Statistic")
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Adventure Menu Base Statistic")
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Adventure Menu Equip Statistic")
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Adventure Menu Inventory Statistic")
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Adventure Menu Journal Stats")
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Adventure Menu Skills Statistic")
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Adventure Menu Standard Statistic")
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Adventure Menu Status Statistic")
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Adventure Menu Vendor Statistic")
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Adventure Menu Effect")

-- |[ ==================================== End Compilation ===================================== ]|
if(iCompileFlag == 1) then
    StarFont_FinishCompilation()
elseif(iCompileFlag == 2) then
    StarFont_OpenCompiledFile("Null")
end

local fEndTime = LM_CheckTimer("Fonts")
io.write(string.format("%6.4f: Adventure Font Loading\n", fEndTime - fStartTime))
LM_FinishTimer("Fonts")
    