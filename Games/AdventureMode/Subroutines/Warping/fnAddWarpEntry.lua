-- |[ ==================================== fnAddWarpEntry() ==================================== ]|
--Adds an entry to the global warp list. This list gets cleared each time the player reaches a
-- warp point, as the list may dynamically changed based on local variables.
--The list is gzaWarpList, declared below.
gzaWarpList = {}

--Function.
function fnAddWarpEntry(psRegionName, psDisplayName, psCurrentRoomName, psRoomName, psVariablePath, pfMapX, pfMapY)
    -- |[Argument Check]|
    if(psRegionName      == nil) then return end
    if(psDisplayName     == nil) then return end
    if(psCurrentRoomName == nil) then return end
    if(psRoomName        == nil) then return end
    if(psVariablePath    == nil) then return end
    --pfMapX is optional
    --pfMapY is optional

    -- |[Create]|
	--Add entry.
    local zNewEntry = {}
	zNewEntry.sRegionName = psRegionName
	zNewEntry.sDisplayName = psDisplayName
	zNewEntry.sRoomName = psRoomName
	zNewEntry.sVariablePath = psVariablePath
	zNewEntry.sMapName = "No Map"
	zNewEntry.fMapX = 0.0
	zNewEntry.fMapY = 0.0

    -- |[Unlock]|
	--If this variable has not been unlocked before, unlock it now. This is nominally done by a trigger.
	if(psRoomName == psCurrentRoomName) then
		VM_SetVar(psVariablePath, "N", 1.0)
	end
    
    -- |[Map Positioning]|
    --Find the entry on the map lookups list.
    local iCanUseRoom = VM_GetVar(psVariablePath, "N")
    if(iCanUseRoom == 1.0) then
        for i = 1, #gsaMapLookups, 1 do
            if(gsaMapLookups[i][1] == psRoomName) then
                zNewEntry.sMapName = gsaMapLookups[i][2]
                zNewEntry.fMapX = gsaMapLookups[i][3]
                zNewEntry.fMapY = gsaMapLookups[i][4]
            end
        end
    end
    
    --If these values are not nil, set them. Override the existing values.
    if(pfMapX ~= nil) then zNewEntry.fMapX = pfMapX end
    if(pfMapY ~= nil) then zNewEntry.fMapY = pfMapY end

    -- |[Finish Up]|
    --Append.
    table.insert(gzaWarpList, zNewEntry)
end