-- |[ =================================== Assemble Warp List =================================== ]|
--This script is fired when the player selects "Warp" from the save menu. It goes through the game's conditions and assembles
-- a list of locations the player may warp to.
--The list is always clear before this executes and this script must re-build it each time.
--Generally speaking, these are set by activating the campfire in each area. It is possible to have no entries on the list.
-- This script is always fired with the name of the current room as the 0th argument. It should not be possible to warp
-- to the current location!

-- |[Argument Check]|
--Verify.
if(fnArgCheck(1) == false) then return end

--Set.
local sCurrentRoomName = LM_GetScriptArgument(0)

--If the current room's name is "Nowhere", we can't warp.
gsLastAssembledWarpRoom = sCurrentRoomName
if(sCurrentRoomName == "Nowhere") then return end

-- |[Variable Registry]|
--Get which chapter we're currently playing.
local iCurrentChapter = VM_GetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N")

-- |[ ==================================== Chapter Listings ==================================== ]|
-- |[Clear]|
--Reset the warp list each time this script executes.
gzaWarpList = {}

-- |[Chapter 1]|
--Associate the room names with variable names. This is done in a parallel-list format to make iteration easier.
if(iCurrentChapter == 1) then

-- |[Chapter 2]|
--Trafal is a lovely place.
elseif(iCurrentChapter == 2) then

    --Can't warp at all if Izuna hasn't been dropped off. After that, special warnings will appear until the player
    -- completes the hunting tutorial.
    local iDroppedOffIzuna = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iDroppedOffIzuna", "N")
    if(iDroppedOffIzuna == 0.0) then return end

    fnAddWarpEntry("Northwoods", "Below Shrine of the Hero", sCurrentRoomName,  "LowerShrineC",    "Root/Variables/Chapter2/Campfires/iLowerShrineC") 
    fnAddWarpEntry("Northwoods", "Sanya's Cabin",            sCurrentRoomName,  "SanyaCabinA",     "Root/Variables/Chapter2/Campfires/iSanyaCabinA") 
    fnAddWarpEntry("Northwoods", "Northwoods NE",            sCurrentRoomName,  "NorthwoodsNEA",   "Root/Variables/Chapter2/Campfires/iNorthwoodsNEA") 
    fnAddWarpEntry("Northwoods", "Northwoods NW",            sCurrentRoomName,  "NorthwoodsNWE",   "Root/Variables/Chapter2/Campfires/iNorthwoodsNWE") 
    fnAddWarpEntry("Northwoods", "Mt. Sarulente Approach",   sCurrentRoomName,  "NorthwoodsSWC",   "Root/Variables/Chapter2/Campfires/iNorthwoodsSWC") 
    
    fnAddWarpEntry("MtSarulente",  "Fort Sarulente", sCurrentRoomName,  "SarulenteF", "Root/Variables/Chapter2/Campfires/iMtSarulenteF") 
    
    fnAddWarpEntry("Westwoods",  "Kitsune Village",  sCurrentRoomName,  "KitsuneVillageB", "Root/Variables/Chapter2/Campfires/iKitsuneVillageB") 
    fnAddWarpEntry("Westwoods",  "Granvire Pass",    sCurrentRoomName,  "GranvirePassD",   "Root/Variables/Chapter2/Campfires/iGranvireD") 
    fnAddWarpEntry("Westwoods",  "Vuca Pass",        sCurrentRoomName,  "VucaPassD",       "Root/Variables/Chapter2/Campfires/iVucaPassD") 
    fnAddWarpEntry("Westwoods",  "Harpy Fort",       sCurrentRoomName,  "HarpyBaseA",      "Root/Variables/Chapter2/Campfires/iHarpyBaseA") 
    fnAddWarpEntry("Westwoods",  "Caverns Entrance", sCurrentRoomName,  "WestwoodsSEB",    "Root/Variables/Chapter2/Campfires/iWestwoodsSEB") 


-- |[Chapter 5]|
--Same deal but on Regulus.
elseif(iCurrentChapter == 5) then

    --If this flag is set, warping is disabled.
    local iNoWarpsAllowed = VM_GetVar("Root/Variables/Chapter5/WorldState/iNoWarpsAllowed", "N")
    if(iNoWarpsAllowed == 1.0) then return end
    
    --Biolabs.
    local iReachedBiolabs = VM_GetVar("Root/Variables/Chapter5/Scenes/iReachedBiolabs", "N")

    --If currently in Sprocket City and we have not completed the Sprocket City quest chain, then the
    -- only place we can warp is the outskirts.
    local iCompletedSprocketCity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSprocketCity", "N")
    if(sCurrentRoomName == "SprocketCityA" and iCompletedSprocketCity == 0.0) then
        fnAddWarpEntry("Regulus", "Sprocket City Outskirts",  sCurrentRoomName, "TelluriumMinesE",   "Root/Variables/Chapter5/Campfires/iTelluriumMinesE") 
    
    --Not-Biolabs. First part of the chapter.
    elseif(iReachedBiolabs == 0.0) then
        fnAddWarpEntry("Regulus",    "Regulus City: Sector 96",  sCurrentRoomName, "RegulusCityC",         "Root/Variables/Chapter5/Campfires/iRegulusCityC") 
        fnAddWarpEntry("Regulus",    "Sector 96 Basement",       sCurrentRoomName, "LowerRegulusCityB",    "Root/Variables/Chapter5/Campfires/iLowerRegulusCityB") 
        fnAddWarpEntry("Regulus",    "Sector 99 Lower",          sCurrentRoomName, "RegulusManufactoryC",  "Root/Variables/Chapter5/Campfires/iRegulusManufactoryC") 
        fnAddWarpEntry("Cryogenics", "Cryogenics Facility",      sCurrentRoomName, "RegulusCryoC",         "Root/Variables/Chapter5/Campfires/iRegulusCryoC",        1334, 1148) 
        fnAddWarpEntry("Regulus",    "Regulus City: Sector 15",  sCurrentRoomName, "RegulusCity15C",       "Root/Variables/Chapter5/Campfires/iRegulusCity15C") 
        fnAddWarpEntry("Regulus",    "Eastern Regulus Surface",  sCurrentRoomName, "RegulusExteriorEA",    "Root/Variables/Chapter5/Campfires/iRegulusExteriorEA") 
        fnAddWarpEntry("Regulus",    "Southern Regulus Surface", sCurrentRoomName, "RegulusExteriorSB",    "Root/Variables/Chapter5/Campfires/iRegulusExteriorSB") 
        fnAddWarpEntry("Regulus",    "Western Regulus Surface",  sCurrentRoomName, "RegulusExteriorWA",    "Root/Variables/Chapter5/Campfires/iRegulusExteriorWA") 
        fnAddWarpEntry("LRT East",   "Entrance",                 sCurrentRoomName, "RegulusLRTA",          "Root/Variables/Chapter5/Campfires/iRegulusLRTA",         1329,  515)
        fnAddWarpEntry("LRT West",   "West Overlook",            sCurrentRoomName, "RegulusLRTF",          "Root/Variables/Chapter5/Campfires/iRegulusLRTF",         1250, 1221)
        fnAddWarpEntry("LRT West",   "Datacore",                 sCurrentRoomName, "RegulusLRTID",         "Root/Variables/Chapter5/Campfires/iRegulusLRTID",        1171,  406) 
        fnAddWarpEntry("Equinox",    "Equinox Labs",             sCurrentRoomName, "RegulusEquinoxA",      "Root/Variables/Chapter5/Campfires/iRegulusEquinoxA",     1708,  608)
        fnAddWarpEntry("Regulus",    "Serenity Observatory",     sCurrentRoomName, "SerenityObservatoryE", "Root/Variables/Chapter5/Campfires/iSerenityObservatoryE")
        fnAddWarpEntry("Regulus",    "Serenity Crater Shelf",    sCurrentRoomName, "SerenityCraterD",      "Root/Variables/Chapter5/Campfires/iSerenityCraterD")
        fnAddWarpEntry("Regulus",    "Serenity Research Lab",    sCurrentRoomName, "SerenityCraterF",      "Root/Variables/Chapter5/Campfires/iSerenityCraterF")
        
        --Sprocket City. Can only be accessed if the quest chain is completed.
        if(iCompletedSprocketCity == 1.0) then
            fnAddWarpEntry("Regulus", "Fist of the Future HQ", sCurrentRoomName, "SprocketCityA", "Root/Variables/Chapter5/Campfires/iSprocketA") 
        end
        fnAddWarpEntry("Regulus", "Tellurium Mines Entrance", sCurrentRoomName, "TelluriumMinesB",   "Root/Variables/Chapter5/Campfires/iTelluriumMinesB") 
        fnAddWarpEntry("Regulus", "Sprocket City Outskirts",  sCurrentRoomName, "TelluriumMinesE",   "Root/Variables/Chapter5/Campfires/iTelluriumMinesE") 
        fnAddWarpEntry("Regulus", "Mines Black Site",         sCurrentRoomName, "BlackSiteA",        "Root/Variables/Chapter5/Campfires/iBlackSiteA") 
    
    --Biolabs.
    else
        fnAddWarpEntry("Biolabs", "Alpha Laboratories",        sCurrentRoomName, "RegulusBiolabsAlphaA",    "Root/Variables/Chapter5/Campfires/iRegulusBiolabsAlphaA") 
        fnAddWarpEntry("Biolabs", "Gamma Checkpoint",          sCurrentRoomName, "RegulusBiolabsGammaA",    "Root/Variables/Chapter5/Campfires/iRegulusBiolabsGammaA") 
        fnAddWarpEntry("Biolabs", "Raiju Ranch Entrance",      sCurrentRoomName, "RegulusBiolabsDeltaB",    "Root/Variables/Chapter5/Campfires/iRegulusBiolabsDeltaB") 
        fnAddWarpEntry("Biolabs", "Aquatic Genetics Building", sCurrentRoomName, "RegulusBiolabsGeneticsA", "Root/Variables/Chapter5/Campfires/iRegulusBiolabsGeneticsA") 
    end

end

-- |[ ====================================== Region Images ===================================== ]|
-- |[Constants]|
local cfSizeFactor = 1.0 / 1.5
local cfScreenWid  = 1366.0
local cfScreenHei  =  768.0
local cfMapWid     = 2732.0
local cfMapHei     = 1536.0

-- |[List of Map Objects]|
--All map objects that currently exist will attempt to handle the warp call. These are per-chapter.
GUIMap:fnRunWarpHandlerOnAllMaps(sCurrentRoomName)

-- |[Chapter 1]|
-- |[Chapter 2]|
fnHandleNorthwoodsMapWarp()  --Subroutine for Northwoods
fnHandleMtSarulenteMapWarp() --Subroutine for Westwoods
fnHandleWestwoodsMapWarp()   --Subroutine for Westwoods

-- |[Chapter 5 Basic]|
--Non-advanced maps.
AM_SetProperty("Register Warp Region",       "Regulus",    "Root/Images/AdventureUI/AdvCampfireWarpIcon/Regulus",    "Root/Images/AdvMaps/General/RegulusMap",      "Root/Images/AdvMaps/General/PDUFrame")
AM_SetProperty("Set Warp Region Alignments", "Regulus",     0.0, 0.0, 0.0, 0.0, 90.0, 90.0)
AM_SetProperty("Register Warp Region",       "Biolabs",    "Root/Images/AdventureUI/AdvCampfireWarpIcon/Biolabs",    "Root/Images/AdvMaps/General/BiolabsMap",      "Root/Images/AdvMaps/General/PDUFrame")
AM_SetProperty("Set Warp Region Alignments", "Biolabs",     0.0, 0.0, 0.0, 0.0, 90.0, 90.0)

-- |[Cryogenics]|
AM_SetProperty("Register Warp Region",            "Cryogenics", "Root/Images/AdventureUI/AdvCampfireWarpIcon/Cryogenics", "Advanced", "Root/Images/AdvMaps/General/PDUFrame")
AM_SetProperty("Set Warp Region Alignments",      "Cryogenics",  0.0, 0.0, 0.0, 0.0, 90.0, 90.0)
AM_SetProperty("Set Warp Region Advanced Clamps", "Cryogenics", 0, 0, -(cfMapWid * cfSizeFactor) + cfScreenWid, -(cfMapHei * cfSizeFactor) + cfScreenHei)
AM_SetProperty("Register Warp Region Advanced Pack", "Cryogenics", "Layer0", "Root/Images/AdvMaps/Cryogenics/CryolabMain0")
AM_SetProperty("Warp Region Advanced Render Chance", "Cryogenics", "Layer0", 0, 10)
AM_SetProperty("Register Warp Region Advanced Pack", "Cryogenics", "Layer1", "Root/Images/AdvMaps/Cryogenics/CryolabMain1")
AM_SetProperty("Warp Region Advanced Render Chance", "Cryogenics", "Layer1", 11, 20)
AM_SetProperty("Register Warp Region Advanced Pack", "Cryogenics", "Layer2", "Root/Images/AdvMaps/Cryogenics/CryolabMain2")
AM_SetProperty("Warp Region Advanced Render Chance", "Cryogenics", "Layer2", 21, 30)
AM_SetProperty("Register Warp Region Advanced Pack", "Cryogenics", "Layer3", "Root/Images/AdvMaps/Cryogenics/CryolabMain3")
AM_SetProperty("Warp Region Advanced Render Chance", "Cryogenics", "Layer3", 31, 40)
AM_SetProperty("Register Warp Region Advanced Pack", "Cryogenics", "Layer4", "Root/Images/AdvMaps/Cryogenics/CryolabMain5")
AM_SetProperty("Warp Region Advanced Render Chance", "Cryogenics", "Layer4", 41, 50)
AM_SetProperty("Register Warp Region Advanced Pack", "Cryogenics", "Layer5", "Root/Images/AdvMaps/Cryogenics/CryolabMain4")
AM_SetProperty("Warp Region Advanced Render Chance", "Cryogenics", "Layer5", 51, 1000)

--Equinox.
AM_SetProperty("Register Warp Region",            "Equinox", "Root/Images/AdventureUI/AdvCampfireWarpIcon/Equinox", "Advanced", "Root/Images/AdvMaps/General/PDUFrame")
AM_SetProperty("Set Warp Region Alignments",      "Equinox", 0.0, 0.0, 0.0, 0.0, 90.0, 90.0)
AM_SetProperty("Set Warp Region Advanced Clamps", "Equinox", 0, 0, -(cfMapWid * cfSizeFactor) + cfScreenWid, -(cfMapHei * cfSizeFactor) + cfScreenHei)
AM_SetProperty("Register Warp Region Advanced Pack", "Equinox", "Layer0", "Root/Images/AdvMaps/Equinox/Equinox0")
AM_SetProperty("Warp Region Advanced Render Chance", "Equinox", "Layer0", 0, 10)
AM_SetProperty("Register Warp Region Advanced Pack", "Equinox", "Layer1", "Root/Images/AdvMaps/Equinox/Equinox1")
AM_SetProperty("Warp Region Advanced Render Chance", "Equinox", "Layer1", 11, 20)
AM_SetProperty("Register Warp Region Advanced Pack", "Equinox", "Layer2", "Root/Images/AdvMaps/Equinox/Equinox2")
AM_SetProperty("Warp Region Advanced Render Chance", "Equinox", "Layer2", 21, 30)
AM_SetProperty("Register Warp Region Advanced Pack", "Equinox", "Layer3", "Root/Images/AdvMaps/Equinox/Equinox3")
AM_SetProperty("Warp Region Advanced Render Chance", "Equinox", "Layer3", 31, 40)
AM_SetProperty("Register Warp Region Advanced Pack", "Equinox", "Layer4", "Root/Images/AdvMaps/Equinox/Equinox5")
AM_SetProperty("Warp Region Advanced Render Chance", "Equinox", "Layer4", 41, 50)
AM_SetProperty("Register Warp Region Advanced Pack", "Equinox", "Layer5", "Root/Images/AdvMaps/Equinox/Equinox4")
AM_SetProperty("Warp Region Advanced Render Chance", "Equinox", "Layer5", 51, 1000)

--LRT East.
AM_SetProperty("Register Warp Region",            "LRT East", "Root/Images/AdventureUI/AdvCampfireWarpIcon/LRTE", "Advanced", "Root/Images/AdvMaps/General/PDUFrame")
AM_SetProperty("Set Warp Region Alignments",      "LRT East", 0.0, 0.0, 0.0, 0.0, 90.0, 90.0)
AM_SetProperty("Set Warp Region Advanced Clamps", "LRT East", 0, 0, -(cfMapWid * cfSizeFactor) + cfScreenWid, -(cfMapHei * cfSizeFactor) + cfScreenHei)
AM_SetProperty("Register Warp Region Advanced Pack", "LRT East", "Layer0", "Root/Images/AdvMaps/LRT/LRTEast0")
AM_SetProperty("Warp Region Advanced Render Chance", "LRT East", "Layer0", 0, 10)
AM_SetProperty("Register Warp Region Advanced Pack", "LRT East", "Layer1", "Root/Images/AdvMaps/LRT/LRTEast1")
AM_SetProperty("Warp Region Advanced Render Chance", "LRT East", "Layer1", 11, 20)
AM_SetProperty("Register Warp Region Advanced Pack", "LRT East", "Layer2", "Root/Images/AdvMaps/LRT/LRTEast2")
AM_SetProperty("Warp Region Advanced Render Chance", "LRT East", "Layer2", 21, 30)
AM_SetProperty("Register Warp Region Advanced Pack", "LRT East", "Layer3", "Root/Images/AdvMaps/LRT/LRTEast3")
AM_SetProperty("Warp Region Advanced Render Chance", "LRT East", "Layer3", 31, 40)
AM_SetProperty("Register Warp Region Advanced Pack", "LRT East", "Layer4", "Root/Images/AdvMaps/LRT/LRTEast5")
AM_SetProperty("Warp Region Advanced Render Chance", "LRT East", "Layer4", 41, 50)
AM_SetProperty("Register Warp Region Advanced Pack", "LRT East", "Layer5", "Root/Images/AdvMaps/LRT/LRTEast4")
AM_SetProperty("Warp Region Advanced Render Chance", "LRT East", "Layer5", 51, 1000)

--LRT West.
AM_SetProperty("Register Warp Region",            "LRT West", "Root/Images/AdventureUI/AdvCampfireWarpIcon/LRTW", "Advanced", "Root/Images/AdvMaps/General/PDUFrame")
AM_SetProperty("Set Warp Region Alignments",      "LRT West", 0.0, 0.0, 0.0, 0.0, 90.0, 90.0)
AM_SetProperty("Set Warp Region Advanced Clamps", "LRT West", 0, 0, -(cfMapWid * cfSizeFactor) + cfScreenWid, -(cfMapHei * cfSizeFactor) + cfScreenHei)
AM_SetProperty("Register Warp Region Advanced Pack", "LRT West", "Layer0", "Root/Images/AdvMaps/LRT/LRTWest0")
AM_SetProperty("Warp Region Advanced Render Chance", "LRT West", "Layer0", 0, 10)
AM_SetProperty("Register Warp Region Advanced Pack", "LRT West", "Layer1", "Root/Images/AdvMaps/LRT/LRTWest1")
AM_SetProperty("Warp Region Advanced Render Chance", "LRT West", "Layer1", 11, 20)
AM_SetProperty("Register Warp Region Advanced Pack", "LRT West", "Layer2", "Root/Images/AdvMaps/LRT/LRTWest2")
AM_SetProperty("Warp Region Advanced Render Chance", "LRT West", "Layer2", 21, 30)
AM_SetProperty("Register Warp Region Advanced Pack", "LRT West", "Layer3", "Root/Images/AdvMaps/LRT/LRTWest3")
AM_SetProperty("Warp Region Advanced Render Chance", "LRT West", "Layer3", 31, 40)
AM_SetProperty("Register Warp Region Advanced Pack", "LRT West", "Layer4", "Root/Images/AdvMaps/LRT/LRTWest5")
AM_SetProperty("Warp Region Advanced Render Chance", "LRT West", "Layer4", 41, 50)
AM_SetProperty("Register Warp Region Advanced Pack", "LRT West", "Layer5", "Root/Images/AdvMaps/LRT/LRTWest4")
AM_SetProperty("Warp Region Advanced Render Chance", "LRT West", "Layer5", 51, 1000)

-- |[ ======================================== Execution ======================================= ]|
--Go through the variables and add entries for all of them. If less than one entry is in place, the menu cannot be opened.
-- We also check to make sure the current room is not added, since we can't warp to where we are.
for i = 1, #gzaWarpList, 1 do
	
	--If this is the current room, add it with the display name "You are here".
	if(gzaWarpList[i].sRoomName == sCurrentRoomName) then
        AM_SetProperty("Register Warp Location", gzaWarpList[i].sRegionName, gzaWarpList[i].sDisplayName .. "(you are here)", gzaWarpList[i].sRoomName, gzaWarpList[i].fMapX, gzaWarpList[i].fMapY)
		
	--Otherwise, check the variable and add it.
	else
	
		--Get the variable. It has to be 1 to be usable.
		local iIsUnlocked = VM_GetVar(gzaWarpList[i].sVariablePath, "N")
		if(iIsUnlocked == 1.0) then
			AM_SetProperty("Register Warp Location", gzaWarpList[i].sRegionName, gzaWarpList[i].sDisplayName, gzaWarpList[i].sRoomName, gzaWarpList[i].fMapX, gzaWarpList[i].fMapY)
		end
	end
	
end