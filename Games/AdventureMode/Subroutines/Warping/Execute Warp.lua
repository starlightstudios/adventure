-- |[ ====================================== Execute Warp ====================================== ]|
--When the player warps to a location, this script is called. It receives the name of the map to warp to as its argument.
--The script played each time is the same, and the receiving room has the chance to respond with a standard handler.

-- |[ ===================================== Warp Blockers ====================================== ]|
--Setup.
local bBlockWarp = false

--Get which chapter we're currently playing.
local iCurrentChapter = VM_GetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N")

-- |[Chapter 1]|
if(iCurrentChapter == 1.0) then
    
    --Block warping when exiting the bee hive after transforming. We need to watch a cutscene.
    local iHasSeenOutlandBeeScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenOutlandBeeScene", "N")
    local iHasBeeForm = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")
    if(iHasSeenOutlandBeeScene == 0.0 and iHasBeeForm == 1.0) then
        bBlockWarp = true
    end

    --If the player is currently in the Cassandra event sequence, block out warping.
    local iStartedCassandraEvent = VM_GetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N")
    if(iStartedCassandraEvent == 1.0) then
        bBlockWarp = true
    end

    --Block warping if this cutscene needs to play when exiting the trap dungeon.
    local iPostDungeonScene     = VM_GetVar("Root/Variables/Chapter1/Scenes/iPostDungeonScene", "N")
    local iCompletedTrapDungeon = VM_GetVar("Root/Variables/Chapter1/Scenes/iCompletedTrapDungeon", "N")
    if(iPostDungeonScene == 0.0 and iCompletedTrapDungeon == 1.0) then
        bBlockWarp = true
    end
    
    --Block warping during the rubber sequence in its entirety.
    local iIsRubberMode = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
    if(iIsRubberMode == 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Narrator: (The runestone is smothered in rubber.[P] Nothing happens.)") ]])
        fnCutsceneBlocker()
        return
    end

-- |[Chapter 2]|
elseif(iCurrentChapter == 2.0) then

    --Block warping if at the cabin.
    local iCompletedHunt = VM_GetVar("Root/Variables/Chapter2/Northwoods/iCompletedHunt", "N")
    if(iCompletedHunt == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Sanya](While the prospect of going back to that stupid mole-statue-people cave filled with puzzles is alluring, I should probably wait until that weeb wakes up.)") ]])
        fnCutsceneBlocker()
        return
    end

    --Block warping if Izuna needs to be rescued!
    local iSawKidnapIntro = VM_GetVar("Root/Variables/Chapter2/KitsuneVillage/iSawKidnapIntro", "N")
    local iRescuedIzuna   = VM_GetVar("Root/Variables/Chapter2/KitsuneVillage/iRescuedIzuna", "N")
    if(iSawKidnapIntro == 1.0 and iRescuedIzuna == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Sanya](I can't leave without rescuing Izuna!)") ]])
        fnCutsceneBlocker()
        return
    end
    
-- |[Chapter 5]|
elseif(iCurrentChapter == 5.0) then

	--Christine cannot leave Sector 96 until 55 joins the party.
	local iReceivedFunction = VM_GetVar("Root/Variables/Chapter5/Scenes/iReceivedFunction", "N")
	local iMet55InLowerRegulus = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N")
	local iIsOnDate = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
	if(iReceivedFunction == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](My programming prohibits exiting the sector until I have received a function assignment.)") ]])
		fnCutsceneBlocker()
        return
	
	--Has a function assignment but hasn't met up with 55.
	elseif(iReceivedFunction == 1.0 and iMet55InLowerRegulus == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](I don't have a reason to leave Regulus City.)") ]])
		fnCutsceneBlocker()
        return
	
	--If Sophie is currently following.
	elseif(iIsOnDate == 1.0 or iIsOnDate == 2.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] 771852?[P] Are you feeling all tingly?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] A little.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] It feels odd.[P] I don't like it...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] (Better not warp anywhere with Sophie, especially if it would put her at risk.)") ]])
		fnCutsceneBlocker()
        return
	end
    
    --Sprocket City restrictions.
    local iSawWakeUpIntro = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawWakeUpIntro", "N")
    local iSprung55 = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprung55", "N")
    if(iSawWakeUpIntro > 0.0 and iSprung55 < 1.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](I should go get 55 out of the lockup before I leave.)") ]])
		fnCutsceneBlocker()
        return
    end
	local iTold55Go        = VM_GetVar("Root/Variables/Chapter5/Scenes/iTold55Go", "N")
	local iSawGoEastPrompt = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawGoEastPrompt", "N")
	if(iTold55Go == 1.0 and iSawGoEastPrompt == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](I should probably go out and check the black site path before warping anywhere.)") ]])
		fnCutsceneBlocker()
        return
    end
    
    --Black Site quest.
	local iCompletedBlackSite = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedBlackSite", "N")
	if(iCompletedBlackSite > 0.0 and iCompletedBlackSite < 10.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](Warping out of here with JX-101 watching would be a be idea on many levels...)") ]])
		fnCutsceneBlocker()
        return
    end
end

-- |[Warp Block Execution]|
if(bBlockWarp == true) then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Strange, something seems to be stopping me from warping...)") ]])
    fnCutsceneBlocker()
	return
end


-- |[ ===================================== Warp Execution ===================================== ]|
-- |[Argument Check]|
--Verify.
if(fnArgCheck(1) == false) then return end

--Set.
local sTargetRoomName = LM_GetScriptArgument(0)

-- |[Same Room Check]|
--If in the same room as the target, no need to warp. The variable gsLastAssembledWarpRoom is populated
-- in Assemble Warp List.lua
if(sTargetRoomName == gsLastAssembledWarpRoom) then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (I'm already at that warp point.)") ]])
    fnCutsceneBlocker()
    return
end

-- |[Sequence]|
--Wait a bit.
fnCutsceneWait(5)
fnCutsceneBlocker()

--Fullwhite for party leader.
if(gsPartyLeaderName ~= nil) then
    Cutscene_CreateEvent("Flash Leader White", "Actor")
        ActorEvent_SetProperty("Subject Name", gsPartyLeaderName)
        ActorEvent_SetProperty("Flashwhite Quickly")
    DL_PopActiveObject()
end

--Iterate across the party members.
for i = 1, giFollowersTotal, 1 do
	Cutscene_CreateEvent("Flash Follower White", "Actor")
		ActorEvent_SetProperty("Subject Name", gsaFollowerNames[i])
		ActorEvent_SetProperty("Flashwhite Quickly")
	DL_PopActiveObject()
end
fnCutsceneBlocker()

fnCutsceneWait(gci_Flashwhite_Ticks_Total / 2.0)
fnCutsceneBlocker()

--Fade the screen out.
fnCutscene([[ AudioManager_PlaySound("World|Teleport") ]])
fnCutscene([[ AL_SetProperty("Activate Fade", 15, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()
fnCutsceneWait(15)
fnCutsceneBlocker()

--Search for a remap.
local sRemappedString = sTargetRoomName
for i = 1, #gsaMasterList, 1 do
	if(gsaMasterList[i].sStartName == sTargetRoomName) then
		sRemappedString = gsaMasterList[i].sRemapName
		break
	end
end

--Warp to the destination room.
local sString = "AL_BeginTransitionTo(\"" .. sTargetRoomName .. "\", gsRoot .. \"Maps/" .. sRemappedString .. "/WarpHandler.lua\")"
fnCutscene(sString)
