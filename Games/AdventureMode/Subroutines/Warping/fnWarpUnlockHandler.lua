-- |[ =================================== Warp Unlock Handler ================================== ]|
--When the player approaches a new warp point for the first time, this script is called to unlock it
-- and play a little animation. The arguments passed in specify where the center point is and what
-- path to unlock. This will do nothing if already unlocked.
function fnWarpUnlockHandler(psVarPath, pfCampfireX, pfCampfireY)

    -- |[ ====================== Arguments ======================= ]|
    --Verify arguments.
    if(psVarPath   == nil) then return end
    if(pfCampfireX == nil) then return end
    if(pfCampfireY == nil) then return end
    
    --If the warp point is already unlocked, do nothing.
    local iUnlocked = VM_GetVar(psVarPath, "N")
    if(iUnlocked == 1.0) then return end
    
    -- |[ ======================== Setup ========================= ]|
    --Firework image paths. These rotate as the fireworks spawn.
    local saImagePaths = {}
    saImagePaths[1] = "Root/Images/Sprites/Catalyst/HeartFirework"
    saImagePaths[2] = "Root/Images/Sprites/Catalyst/SwordFirework"
    saImagePaths[3] = "Root/Images/Sprites/Catalyst/BootFirework"
    saImagePaths[4] = "Root/Images/Sprites/Catalyst/DodgeFirework"
    saImagePaths[5] = "Root/Images/Sprites/Catalyst/TargetFirework"
    saImagePaths[6] = "Root/Images/Sprites/Catalyst/SkillFirework"
    local iImagePathsTotal = #saImagePaths

    --Mark the warp point as being unlocked.
    VM_SetVar(psVarPath, "N", 1.0)

    -- |[ ================== Warp Unlock Object ================== ]|
    --This object says "Warp Unlocked" and is used as a billboard.
    TA_Create("WarpUnlock")
        TA_SetProperty("Position", -100, -100)
        TA_SetProperty("Rendering Depth", 0.000000)
        TA_SetProperty("Renders After Tiles", true)
        TA_SetProperty("Walk Ticks Per Frame", 1)
        TA_SetProperty("Wipe Special Frames")
        for i = 0, 20, 1 do
            TA_SetProperty("Add Special Frame", "Frame"..i, "Root/Images/Sprites/WarpUnlock/"..i)
        end
    DL_PopActiveObject()

    -- |[ ======================= Animation ======================= ]|
    --Setup.
    local iFireworkCount = 0
    local iImagePathCursor = 1

    -- |[ ============ First Cycle =========== ]|
    --Sound effect.
    fnCutscenePlaySound("World|WarpAppear")
    
    --A set of fireworks moves in a circle around the campfire and reaches it.
    for fDegrees = 0, 360, 36 do
        
        -- |[Common]|
        --Compute percentage completion. The radius will decrease over time.
        local fPercent = fDegrees / 360.0
        
        --Compute radius.
        local fRadiusBgn = 32.0
        local fRadiusEnd =  1.0
        local fRadius = fRadiusBgn + ((fRadiusEnd - fRadiusBgn) * fPercent)
        
        -- |[First Arm]|
        --Compute X/Y in world coordinates.
        local fRadians = ((fDegrees + 0.0) / 360.0) * (math.pi * 2.0)
        local fXPos = (pfCampfireX * gciSizePerTile) + (math.cos(fRadians) * fRadius)
        local fYPos = (pfCampfireY * gciSizePerTile) + (math.sin(fRadians) * fRadius)
        
        --Set to tile coordinates.
        fXPos = fXPos / gciSizePerTile
        fYPos = fYPos / gciSizePerTile
        
        --Spawn a firework.
        fnWarpSpawnFirework(iFireworkCount, saImagePaths[iImagePathCursor])
        fnWarpActivateFirework(iFireworkCount, fXPos, fYPos)
        iFireworkCount = iFireworkCount + 1
        
        -- |[Second Arm]|
        --Same as above, but is 180 degrees from the first firework.
        fRadians = ((fDegrees + 180.0) / 360.0) * (math.pi * 2.0)
        fXPos = (pfCampfireX * gciSizePerTile) + (math.cos(fRadians) * fRadius)
        fYPos = (pfCampfireY * gciSizePerTile) + (math.sin(fRadians) * fRadius)
        
        --Set to tile coordinates.
        fXPos = fXPos / gciSizePerTile
        fYPos = fYPos / gciSizePerTile
        
        --Spawn a firework.
        fnWarpSpawnFirework(iFireworkCount, saImagePaths[iImagePathCursor])
        fnWarpActivateFirework(iFireworkCount, fXPos, fYPos)
        iFireworkCount = iFireworkCount + 1
        
        -- |[Next Set]|
        --Play a sound effect.
        --fnCutscenePlaySound("World|Firework" .. LM_GetRandomNumber(0, 2))
        
        --Increment the image path cursor.
        iImagePathCursor = iImagePathCursor + 1
        if(iImagePathCursor > iImagePathsTotal) then iImagePathCursor = 1 end
        
        --Wait a bit.
        fnCutsceneWait(4)
        fnCutsceneBlocker()
        
    end

    --Move the Warp Unlock object to the campfire. Play a sequence.
    fnCutscenePlaySound("World|WarpMove")
    fnCutsceneTeleport("WarpUnlock", pfCampfireX - 1.0, pfCampfireY + 0.0)
    for i = 0, 11, 1 do
        fnCutsceneSetFrame("WarpUnlock", "Frame"..i)
        fnCutsceneWait(6)
        fnCutsceneBlocker()
    end

    --Wait a bit before the next cycle.
    fnCutsceneWait(24)
    fnCutsceneBlocker()

    --Sound effect.
    fnCutscenePlaySound("World|WarpDecrypt")

    -- |[ =========== Second Cycle =========== ]|
    --A set of fireworks spreads from the campfire outwards.
    for iDistance = 0, 7, 1 do
        
        -- |[Common]|
        --Compute percentage completion. The radius will increase over time.
        local fPercent = iDistance / 7.0
        
        --Compute radius.
        local fRadiusBgn =  1.0
        local fRadiusEnd = 48.0
        local fRadius = fRadiusBgn + ((fRadiusEnd - fRadiusBgn) * fPercent)
        
        --At these instances, change the warp unlock frame.
        if(iDistance == 1) then
            fnCutsceneSetFrame("WarpUnlock", "Frame12")
        elseif(iDistance == 3) then
            fnCutsceneSetFrame("WarpUnlock", "Frame13")
        elseif(iDistance == 5) then
            fnCutsceneSetFrame("WarpUnlock", "Frame14")
        elseif(iDistance == 7) then
            fnCutsceneSetFrame("WarpUnlock", "Frame15")
        end
        
        -- |[Spawn Loop]|
        --We spawn 8 fireworks at fixed intervals around the campfire.
        for fDegrees = 0, 360, 45 do
            
            --Compute X/Y in world coordinates.
            local fRadians = ((fDegrees + 0.0) / 360.0) * (math.pi * 2.0)
            local fXPos = (pfCampfireX * gciSizePerTile) + (math.cos(fRadians) * fRadius)
            local fYPos = (pfCampfireY * gciSizePerTile) + (math.sin(fRadians) * fRadius)
            
            --Set to tile coordinates.
            fXPos = fXPos / gciSizePerTile
            fYPos = fYPos / gciSizePerTile
        
            --Spawn a firework.
            fnWarpSpawnFirework(iFireworkCount, saImagePaths[iImagePathCursor])
            fnWarpActivateFirework(iFireworkCount, fXPos, fYPos)
            iFireworkCount = iFireworkCount + 1
        end
        
        -- |[Next Set]|
        --Play a sound effect.
        fnCutscenePlaySound("World|Firework" .. LM_GetRandomNumber(0, 2))
        
        --Increment the image path cursor.
        iImagePathCursor = iImagePathCursor + 1
        if(iImagePathCursor > iImagePathsTotal) then iImagePathCursor = 1 end
        
        --Wait a bit.
        fnCutsceneWait(4)
        fnCutsceneBlocker()
        
    end

    -- |[ ============ Final Cycle =========== ]|
    --Warp Unlock fades out.
    fnCutsceneWait(8)
    fnCutsceneBlocker()

    --Holds a bit longer on this frame.
    fnCutsceneSetFrame("WarpUnlock", "Frame16")
    fnCutsceneWait(40)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|WarpMove")

    for i = 17, 20, 1 do
        fnCutsceneSetFrame("WarpUnlock", "Frame"..i)
        fnCutsceneWait(8)
        fnCutsceneBlocker()
    end

    --Warp offscreen.
    fnCutsceneTeleport("WarpUnlock", -100, -100)
end

-- |[ ==================================== Worker Functions ==================================== ]|
-- |[fnWarpSpawnFirework]|
--Create a new firework and positions it offscreen.
function fnWarpSpawnFirework(piCount, psImagePath)
    
    -- |[Arg Check]|
    if(piCount == nil) then return end
    
    -- |[Constants]|
    local ciFireworkTPF = 6
    
    -- |[Create]|
    --Firework spawns offscreen.
    TA_Create("Firework" .. piCount)
        TA_SetProperty("Position", -100, -100)
        TA_SetProperty("Rendering Depth", -0.000001)
        TA_SetProperty("Renders After Tiles", true)
        TA_SetProperty("Walk Ticks Per Frame", ciFireworkTPF)
        TA_SetProperty("Auto Animates Fast", true)
        for i = 1, 8, 1 do
            for p = 1, 4, 1 do
                TA_SetProperty("Move Frame", i-1, p-1, psImagePath .. (p-1))
            end
        end
    DL_PopActiveObject()
end

-- |[fnWarpActivateFirework]|
--Tells the firework in question to move onscreen, activate, and then delete itself when it is done animating.
function fnWarpActivateFirework(piCount, pfXPos, fpYPos)
    
    -- |[Arg Check]|
    if(piCount == nil) then return end
    if(pfXPos  == nil) then return end
    if(fpYPos  == nil) then return end
    
    -- |[Position Onscreen]|
    fnCutsceneTeleport("Firework" .. piCount, pfXPos, fpYPos)
    
    -- |[Set Properties]|
    --Order the actor to reset its move timer and then despawn when the move timer is complete.
    Cutscene_CreateEvent("ActorEvent", "Actor")
        ActorEvent_SetProperty("Subject Name", "Firework" .. piCount)
        ActorEvent_SetProperty("Reset Move Timer")
    DL_PopActiveObject()
    Cutscene_CreateEvent("ActorEvent", "Actor")
        ActorEvent_SetProperty("Subject Name", "Firework" .. piCount)
        ActorEvent_SetProperty("Auto Despawn")
    DL_PopActiveObject()
    
end