-- |[ ================================== Handle Region Marker ================================== ]|
--When the player enters a region, a notification appears in the top right to indicate where they
-- are. Each level script calls this script to decide which image to show.
--The first time the player enters a region, a larger overlay appears. This requires extra loading.
-- This script also handles that.

--First, clear this temporary section of the DataLibrary. This will contain the previous region
-- images which are no longer needed.
DL_Purge("Root/Images/RegionNotifiers/Temporary/", false)

--We need to be passed in a pathname to the map's SLF file. We resolve the map name and
-- figure out which region it belongs to.
if(fnArgCheck(1) == false) then return end
local sMapPath = LM_GetScriptArgument(0)

-- |[ ===================================== Modded Regions ===================================== ]|
--Query all mods. If any of them handle things, stop execution.
gbHandledRegion = false
local bHandledRegion = fnExecModScript("Maps/Region Handler.lua", sMapPath)
if(bHandledRegion == true) then return end

-- |[ ===================================== Lookup Builder ===================================== ]|
--If this is the first time running this script, set up the ref table here.
gsaRegionRefTable = nil
if(gsaRegionRefTable == nil) then
    
    -- |[ ================================== Constants ========================================= ]|
    local cfStandardFinalScale = 0.30
    
    -- |[ ================================== Functions ========================================= ]|
    -- |[Region Lookup Data]|
    --Creation function, adds entries to the table.
    gsaRegionRefTable = {}
    local sRefArea = "null"
    local function fnAddToRefTable(psRegionName, psMapName, psRangeS, psRangeE)
        local i = #gsaRegionRefTable + 1
        
        if(psRangeS ~= nil and psRangeE ~= nil) then
            local iByteS = string.byte(psRangeS, 1)
            local iByteE = string.byte(psRangeE, 1)
            for p = iByteS, iByteE, 1 do
                i = #gsaRegionRefTable + 1
                gsaRegionRefTable[i] = {psRegionName, psMapName .. string.char(p)}
            end
        else
            gsaRegionRefTable[i] = {psRegionName, psMapName}
        end
    end
    
    -- |[ ===================================== By Area ======================================== ]|
    -- |[Other]|
    --Nix Nedar
    sRegionName = "Nix Nedar"
    fnAddToRefTable(sRegionName, "NixNedarHouseEast")
    fnAddToRefTable(sRegionName, "NixNedarHousePath")
    fnAddToRefTable(sRegionName, "NixNedarHouseSouthcenter")
    fnAddToRefTable(sRegionName, "NixNedarHouseSouthwest")
    fnAddToRefTable(sRegionName, "NixNedarMain")
    fnAddToRefTable(sRegionName, "NixNedarMaramHouse")
    fnAddToRefTable(sRegionName, "NixNedarSeptimaHouse")
    fnAddToRefTable(sRegionName, "NixNedarSouthPath")
    fnAddToRefTable(sRegionName, "NixNedarSouthPathWander")
    
    --Trading Post
    sRegionName = "Trannadar Trading Post"
    fnAddToRefTable(sRegionName, "TrannadarTradingPost")
    
    -- |[Trannadar]|
    --Arbonne Plains
    sRegionName = "Arbonne Plains"
    fnAddToRefTable(sRegionName, "PlainsC")
    fnAddToRefTable(sRegionName, "PlainsNW")
    fnAddToRefTable(sRegionName, "ArbonnePlains", "A", "D")
    
    --Evermoon Forest
    sRegionName = "Evermoon Forest"
    fnAddToRefTable(sRegionName, "BeehiveBasement", "A", "F")
    fnAddToRefTable(sRegionName, "BeehiveInner")
    fnAddToRefTable(sRegionName, "BeehiveOuter")
    fnAddToRefTable(sRegionName, "AlrauneChamber")
    fnAddToRefTable(sRegionName, "BreannesPitStop")
    fnAddToRefTable(sRegionName, "EvermoonCassandraA")
    fnAddToRefTable(sRegionName, "EvermoonCassandraAMid")
    fnAddToRefTable(sRegionName, "EvermoonCassandraB")
    fnAddToRefTable(sRegionName, "EvermoonCassandraCC")
    fnAddToRefTable(sRegionName, "EvermoonCassandraCE")
    fnAddToRefTable(sRegionName, "EvermoonCassandraCNE")
    fnAddToRefTable(sRegionName, "EvermoonCassandraCNW")
    fnAddToRefTable(sRegionName, "EvermoonE")
    fnAddToRefTable(sRegionName, "EvermoonNE")
    fnAddToRefTable(sRegionName, "EvermoonNW")
    fnAddToRefTable(sRegionName, "EvermoonS")
    fnAddToRefTable(sRegionName, "EvermoonSEA")
    fnAddToRefTable(sRegionName, "EvermoonSEB")
    fnAddToRefTable(sRegionName, "EvermoonSEC")
    fnAddToRefTable(sRegionName, "EvermoonSlimeVillage")
    fnAddToRefTable(sRegionName, "EvermoonSW")
    fnAddToRefTable(sRegionName, "EvermoonW")
    fnAddToRefTable(sRegionName, "SaltFlats")
    fnAddToRefTable(sRegionName, "SaltFlatsCave")
    
    --Dimensional Trap. Note that the "A" room is not selected, as Mei starts here.
    sRegionName = "Dimensional Trap"
    fnAddToRefTable(sRegionName, "TrapBasement", "B", "H")
    fnAddToRefTable(sRegionName, "TrapDungeon", "A", "F")
    fnAddToRefTable(sRegionName, "TrapBasementSecret")
    fnAddToRefTable(sRegionName, "TrapMainFloorCentral")
    fnAddToRefTable(sRegionName, "TrapMainFloorExterior")
    fnAddToRefTable(sRegionName, "TrapMainFloorExteriorN")
    fnAddToRefTable(sRegionName, "TrapMainFloorSouthHall")
    fnAddToRefTable(sRegionName, "TrapUpperFloorE")
    fnAddToRefTable(sRegionName, "TrapUpperFloorMain")
    fnAddToRefTable(sRegionName, "TrapUpperFloorW")
    fnAddToRefTable(sRegionName, "TrapDungeonEntry")
    
    --Starfield Swamp
    sRegionName = "Starfield Swamp"
    fnAddToRefTable(sRegionName, "StarfieldSwamp", "A", "I")
    
    -- |[Quantir]|
    --Quantir High Wastes
    sRegionName = "Quantir High Wastes"
    fnAddToRefTable(sRegionName, "QuantirNW")
    fnAddToRefTable(sRegionName, "QuantirNWCave")
    
    --Quantir Estate
    sRegionName = "Quantir Estate"
    fnAddToRefTable(sRegionName, "QuantirManseBasementE")
    fnAddToRefTable(sRegionName, "QuantirManseBasementW")
    fnAddToRefTable(sRegionName, "QuantirManseCentralE")
    fnAddToRefTable(sRegionName, "QuantirManseCentralW")
    fnAddToRefTable(sRegionName, "QuantirManseEntrance")
    fnAddToRefTable(sRegionName, "QuantirManseNEHall")
    fnAddToRefTable(sRegionName, "QuantirManseNEHallFloor2")
    fnAddToRefTable(sRegionName, "QuantirManseNEHallFloor2")
    fnAddToRefTable(sRegionName, "QuantirManseNWHall")
    fnAddToRefTable(sRegionName, "QuantirManseSecretExit")
    fnAddToRefTable(sRegionName, "QuantirManseSEHall")
    fnAddToRefTable(sRegionName, "QuantirManseSWYard")
    fnAddToRefTable(sRegionName, "QuantirManseSWYardInterior")
    fnAddToRefTable(sRegionName, "QuantirManseTruth")
    fnAddToRefTable(sRegionName, "SpookyExterior")
    
    -- |[Regulus]|
    --Cryogenics. Skips A and B until the player gets the PDU.
    sRegionName = "Cryogenics"
    fnAddToRefTable(sRegionName, "RegulusCryo", "C", "G")
    fnAddToRefTable(sRegionName, "RegulusCryoLower", "A", "D")
    fnAddToRefTable(sRegionName, "RegulusCryoContainment", "A", "B")
    fnAddToRefTable(sRegionName, "RegulusCryoToFabrication", "A", "B")
    fnAddToRefTable(sRegionName, "RegulusCryoCommand", "A", "C")
    fnAddToRefTable(sRegionName, "RegulusCryoSouth", "A", "G")
    fnAddToRefTable(sRegionName, "RegulusCryoPowerCore", "A", "E")
    
    --Equinox.
    sRegionName = "Equinox"
    fnAddToRefTable(sRegionName, "RegulusEquinox", "A", "H")
    
    --LRT Facility.
    sRegionName = "LRTFacility"
    fnAddToRefTable(sRegionName, "RegulusLRT", "A", "H")
    fnAddToRefTable(sRegionName, "RegulusLRTEA")
    fnAddToRefTable(sRegionName, "RegulusLRTGX")
    fnAddToRefTable(sRegionName, "RegulusLRTGZ")
    fnAddToRefTable(sRegionName, "RegulusLRTH", "A", "F")
    fnAddToRefTable(sRegionName, "RegulusLRTI", "A", "G")
    fnAddToRefTable(sRegionName, "RegulusLRTZA")
    fnAddToRefTable(sRegionName, "RegulusLRTZB")
    
    --Serenity Observatory
    sRegionName = "SerenityObservatory"
    fnAddToRefTable(sRegionName, "SerenityObservatory", "A", "H")
    
    -- |[Regulus Biolabs]|
    --Biolabs Areas
    fnAddToRefTable("BiolabsGenetics", "RegulusBiolabsGenetics", "A", "H")
    fnAddToRefTable("BiolabsAlpha", "RegulusBiolabsAlpha", "B", "E")
    fnAddToRefTable("BiolabsBeta", "RegulusBiolabsBeta", "A", "E")
    fnAddToRefTable("BiolabsBeta", "RegulusBiolabsBetaMelted", "A", "E")
    fnAddToRefTable("BiolabsBeta", "RegulusBiolabsAmphibian", "A", "E")
    fnAddToRefTable("BiolabsGamma", "RegulusBiolabsGamma", "A", "D")
    fnAddToRefTable("BiolabsGamma", "RegulusBiolabsGammaWest", "A", "E")
    fnAddToRefTable("BiolabsDelta", "RegulusBiolabsDelta", "A", "I")
    fnAddToRefTable("BiolabsEpsilon", "RegulusBiolabsEpsilon", "A", "E")
    fnAddToRefTable("BiolabsDatacore", "RegulusBiolabsDatacore", "A", "H")
    fnAddToRefTable("BiolabsHydroponics", "RegulusBiolabsHydroponics", "A", "D")
    fnAddToRefTable("RaijuRanch", "RegulusBiolabsRaijuRanch", "A", "C")
    
    -- |[Regulus City]|
    fnAddToRefTable("ArcaneAcademy", "RegulusArcane", "B", "H")
    fnAddToRefTable("RegulusCity15", "RegulusCity15", "B", "C")
    fnAddToRefTable("RegulusCity96", "RegulusCity", "A", "G")
    fnAddToRefTable("RegulusCity96", "LowerRegulusCity", "A", "D")
    fnAddToRefTable("RegulusCity96", "RegulusCityX")
    fnAddToRefTable("RegulusCity96", "RegulusCityZ")
    fnAddToRefTable("RegulusCity96", "RegulusCityBAlt")
    
    -- |[Trafal]|
    --Trafal Glacier
    sRegionName = "Trafal Glacier"
    fnAddToRefTable(sRegionName, "TrafalNW")
    
    --Northwoods
    sRegionName = "Northwoods"
    fnAddToRefTable(sRegionName, "NorthwoodsNC", "A", "D")
    fnAddToRefTable(sRegionName, "NorthwoodsNE", "A", "E")
    fnAddToRefTable(sRegionName, "NorthwoodsNW", "A", "F")
    fnAddToRefTable(sRegionName, "NorthwoodsSC", "A", "E")
    fnAddToRefTable(sRegionName, "NorthwoodsSE", "A", "D")
    fnAddToRefTable(sRegionName, "NorthwoodsSW", "A", "D")
    
    --Westwoods
    sRegionName = "Westwoods"
    fnAddToRefTable(sRegionName, "WestwoodsC",  "A", "G")
    fnAddToRefTable(sRegionName, "WestwoodsE",  "A", "G")
    fnAddToRefTable(sRegionName, "WestwoodsN",  "A", "F")
    fnAddToRefTable(sRegionName, "WestwoodsSE", "A", "C")
    fnAddToRefTable(sRegionName, "WestwoodsW",  "A", "D")
    fnAddToRefTable(sRegionName, "Warrens",  "A", "D")
    fnAddToRefTable(sRegionName, "WarrensCaveA")
    fnAddToRefTable(sRegionName, "WarrensCaveAAlt")
    --WarrensCaveB is only used for a cutscene
    
    --Mt. Sarulente
    sRegionName = "MtSarulente"
    fnAddToRefTable(sRegionName, "EmpressCave",   "A", "D")
    fnAddToRefTable(sRegionName, "ForeSarulente", "A", "C")
    fnAddToRefTable(sRegionName, "Sarulente",     "A", "G")

    -- |[ ================================= Properties ========================================= ]|
    -- |[Region Property Data]|
    --This stores the data used by each region.
    gsaRegionData = {}
    local function fnAddToDataTable(psRegionName, psFileName, psImagePrefix, piImagesTotal, pfScale)
        local i = #gsaRegionData + 1
        gsaRegionData[i] = {psRegionName, psFileName, psImagePrefix, piImagesTotal, pfScale}
    end
    
    --Create.
    fnAddToDataTable("Arbonne Plains",         "UIRegionArbonnePlains",        "ArbonnePlains|",        22, cfStandardFinalScale)
    fnAddToDataTable("ArcaneAcademy",          "UIRegionArcaneUniversity",     "ArcaneUniversity|",     22, cfStandardFinalScale)
    fnAddToDataTable("BiolabsGenetics",        "UIRegionAquaticGenetics",      "AquaticGenetics|",      22, cfStandardFinalScale)
    fnAddToDataTable("BiolabsAlpha",           "UIRegionBiolabsAlpha",         "BiolabsAlpha|",         22, cfStandardFinalScale)
    fnAddToDataTable("BiolabsBeta",            "UIRegionBiolabsBeta",          "BiolabsBeta|",          22, cfStandardFinalScale)
    fnAddToDataTable("BiolabsDelta",           "UIRegionBiolabsDelta",         "BiolabsDelta|",         22, cfStandardFinalScale)
    fnAddToDataTable("BiolabsEpsilon",         "UIRegionBiolabsEpsilon",       "BiolabsEpsilon|",       22, cfStandardFinalScale)
    fnAddToDataTable("BiolabsGamma",           "UIRegionBiolabsGamma",         "BiolabsGamma|",         22, cfStandardFinalScale)
    fnAddToDataTable("BiolabsDatacore",        "UIRegionBiolabsDatacore",      "BiolabsDatacore|",      22, cfStandardFinalScale)
    fnAddToDataTable("BiolabsHydroponics",     "UIRegionBiolabsHydroponics",   "BiolabsHydroponics|",   22, cfStandardFinalScale)
    fnAddToDataTable("Cryogenics",             "UIRegionCryogenics",           "Cryogenics|",           22, cfStandardFinalScale)
    fnAddToDataTable("Equinox",                "UIRegionEquinox",              "EquinoxLabs|",          22, cfStandardFinalScale)
    fnAddToDataTable("Evermoon Forest",        "UIRegionEvermoonForest",       "EvermoonForest|",       22, cfStandardFinalScale)
    fnAddToDataTable("Dimensional Trap",       "UIRegionDimensionalTrap",      "DimensionalTrap|",      22, cfStandardFinalScale)
    fnAddToDataTable("LRTFacility",            "UIRegionLRTFacility",          "LRTFacility|",          22, cfStandardFinalScale)
    fnAddToDataTable("Quantir Estate",         "UIRegionQuantirEstate",        "QuantirEstate|",        22, cfStandardFinalScale)
    fnAddToDataTable("Quantir High Wastes",    "UIRegionQuantirHighWastes",    "QuantirHighWastes|",    22, cfStandardFinalScale)
    fnAddToDataTable("RaijuRanch",             "UIRegionBiolabsRaijuRanch",    "RaijuRanch|",           22, cfStandardFinalScale)
    fnAddToDataTable("RegulusCity15",          "UIRegionRegulusCitySector15",  "RegulusCitySector15|",  22, cfStandardFinalScale)
    fnAddToDataTable("RegulusCity96",          "UIRegionRegulusCitySector96",  "RegulusCitySector96|",  22, cfStandardFinalScale)
    fnAddToDataTable("SerenityObservatory",    "UIRegionSerenityObservatory",  "SerenityObservatory|",  22, cfStandardFinalScale)
    fnAddToDataTable("Starfield Swamp",        "UIRegionStarfieldSwamp",       "StarfieldSwamp|",       22, cfStandardFinalScale)
    fnAddToDataTable("Trafal Glacier",         "UIRegionTrafalGlacier",        "TrafalGlacier|",        22, cfStandardFinalScale)
    fnAddToDataTable("Trannadar Trading Post", "UIRegionTrannadarTradingPost", "TrannadarTradingPost|", 22, cfStandardFinalScale)
    fnAddToDataTable("Nix Nedar",              "UIRegionNixNedar",             "NixNedar|",             22, cfStandardFinalScale)
    
    --Trafal uses 27 per set, and a smaller scale.
    local cfTrafalScale = 0.20
    fnAddToDataTable("Northwoods",  "UIRegionNorthwoods",  "Northwoods|",  27, cfTrafalScale)
    fnAddToDataTable("Westwoods",   "UIRegionWestwoods",   "Westwoods|",   27, cfTrafalScale)
    fnAddToDataTable("MtSarulente", "UIRegionMtSarulente", "MtSarulente|", 27, cfTrafalScale)
end

-- |[ ===================================== Name Resolver ====================================== ]|
--Run backwards through the name and isolate the last slash to figure out the actual map name.
--io.write(" Resolving map name.\n")
local iLen = string.len(sMapPath)
local bNonSlashes = false
local iNameStart = 1
local iNameEnd = iLen
for i = iLen - 1, 1, -1 do
    
    --Get this letter.
    local sLetter = string.sub(sMapPath, i, i)

    --If it's a slash:
    if(sLetter == "/" or sLetter == "\\") then
        
        --Hasn't seen a non-slash yet.
        if(bNonSlashes == false) then
        
        --Has seen a non-slash, end here.
        else
            iNameStart = i+1
            break
        end
    
    --Normal letter:
    else

        --Hasn't seen a non-slash letter, so this is the last letter.
        if(bNonSlashes == false) then
            bNonSlashes = true
            iNameEnd = i
        end
    end
end

--Result
local sMapTrueName = string.sub(sMapPath, iNameStart, iNameEnd)
--io.write(" Final name is: " .. sMapTrueName .. "\n")

-- |[ ========================================= Lookup ========================================= ]|
--Scan across the ref table and find a map name match. That's the region in question.
local sUseRegion = "Null"
for i = 1, #gsaRegionRefTable, 1 do
    if(gsaRegionRefTable[i][2] == sMapTrueName) then
        sUseRegion = gsaRegionRefTable[i][1]
        break
    end
end

--Debug.
--io.write(" Resolved region: " .. sUseRegion .. "\n")

-- |[Special Cases]|
--Nix Nedar does not show until the player has met Septima.
if(sUseRegion == "Nix Nedar") then
	local iMetSeptima = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetSeptima", "N")
    if(iMetSeptima == 0.0) then return end
end

-- |[ ======================================= Execution ======================================== ]|
--First, if the region came back as "Null", do nothing. It may be an unlisted region.
if(sUseRegion == "Null") then return end

--Locate the relevant variable. This determines if the player has seen this region before.
local iHasSeenThisRegion = VM_GetVar("Root/Variables/Regions/"..sUseRegion.."/iHasSeenThisRegion", "N")
--io.write("Has seen region: " .. iHasSeenThisRegion .. "\n")

-- |[Has Visited Region Before]|
--Has seen the region before.
if(iHasSeenThisRegion == 1.0) then
    
    -- |[Property Grouping]|
    --Resolve which property set to use.
    local saPropertySet = {"None", "None", 0}
    for i = 1, #gsaRegionData, 1 do
        if(gsaRegionData[i][1] == sUseRegion) then
            saPropertySet = gsaRegionData[i]
            --io.write(" Got property set in slot " .. i .. ".\n")
            break
        end
    end
    if(saPropertySet[1] == "None") then
        --io.write(" Error: No property set matching " .. sUseRegion .. "\n")
        return
    end
    
    --On a saved game, the region may have been seen but the asset not loaded. Check if the header exists.
    if(DL_Exists("Root/Images/RegionNotifiers/Permanent/" .. sUseRegion) == false) then
        
        --Open.
        SLF_Open(gsDatafilesPath .. saPropertySet[2] .. ".slf")
        
        --Extract.
        local sLastImageBuf = string.format("%s%02i", saPropertySet[3], saPropertySet[4]-1)
        DL_AddPath("Root/Images/RegionNotifiers/Permanent/")
        DL_ExtractBitmap(sLastImageBuf, "Root/Images/RegionNotifiers/Permanent/" .. sUseRegion)
        
        --Clean up.
        SLF_Close()
    end
    
    --Set it as the region notifier.
    AL_SetProperty("Region Notifier Image", "Root/Images/RegionNotifiers/Permanent/" .. sUseRegion)
    
    --Sizing.
    AL_SetProperty("Region Notifier Final Scale", saPropertySet[5])

-- |[Has Not Visited Region Before]|
--Has not seen the region, show the large overlay.
else

    -- |[Property Grouping]|
    --Resolve which property set to use.
    local saPropertySet = {"None", "None", "None", 0}
    for i = 1, #gsaRegionData, 1 do
        if(gsaRegionData[i][1] == sUseRegion) then
            saPropertySet = gsaRegionData[i]
            --io.write(" Got property set in slot " .. i .. ".\n")
            break
        end
    end
    if(saPropertySet[1] == "None") then
        --io.write(" Error: No property set matching " .. sUseRegion .. "\n")
        return
    end

    -- |[Flags]|
    --Flag the region as seen.
    DL_AddPath("Root/Variables/Regions/"..sUseRegion.."/")
    VM_SetVar("Root/Variables/Regions/"..sUseRegion.."/iHasSeenThisRegion", "N", 1.0)

    --Setup.
    SLF_Open(gsDatafilesPath .. saPropertySet[2] .. ".slf")
    
    --Sizing.
    AL_SetProperty("Region Notifier Final Scale", saPropertySet[5])

    -- |[Permanent Notifier]|
    --Permanent notifier is always the last image.
    local sLastImageBuf = string.format("%s%02i", saPropertySet[3], saPropertySet[4]-1)
    DL_AddPath("Root/Images/RegionNotifiers/Permanent/")
    DL_ExtractBitmap(sLastImageBuf, "Root/Images/RegionNotifiers/Permanent/" .. sUseRegion)
    AL_SetProperty("Region Notifier Image", "Root/Images/RegionNotifiers/Permanent/" .. sUseRegion)

    -- |[Temporary Notifiers]|
    --Allocate space.
    AL_SetProperty("Allocate Region Notifier Images", saPropertySet[4], 2)

    --Load the temporary images.
    DL_AddPath("Root/Images/RegionNotifiers/Temporary/")
    for p = 0, saPropertySet[4]-1, 1 do
        local sImageBuf = string.format("%s%02i", saPropertySet[3], p)
        DL_ExtractBitmap(sImageBuf, "Root/Images/RegionNotifiers/Temporary/" .. p)
        AL_SetProperty("Set Region Notifier Frame", p, "Root/Images/RegionNotifiers/Temporary/" .. p)
    end

    -- |[Clean Up]|
    --io.write(" Done.\n")
    SLF_Close()
end
