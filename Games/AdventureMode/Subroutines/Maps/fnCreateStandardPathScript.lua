-- |[ ============================== fnCreateStandardPathScript() ============================== ]|
--Creates a parallel execution pack for a given NPC, giving the pack the same name as the NPC.
-- Also handles extra flags common to this sort of package.
--pbTimingDebug can use the enumerations: gcbHideParallelTiming, gcbShowParallelTiming
function fnCreateStandardPathScript(psNPCName, psScriptPath, pbTimingDebug, piScatterRangeLo, piScatterRangeHi)
    
    -- |[Argument Check]|
    if(psNPCName     == nil) then return end
    if(psScriptPath  == nil) then return end
    if(pbTimingDebug == nil) then pbTimingDebug = gcbHideParallelTiming end
    
    -- |[Execute]|
    --If the script is named after the entity in question, disable their collision flag. Scripts
    -- that refer to multiple NPCs need to either do this manually, or not refer to moving NPCs.
    if(EM_Exists(psNPCName) == true) then
        TA_ChangeCollisionFlag(psNPCName, false)
    end
    Cutscene_HandleParallel("Create", psNPCName, psScriptPath, pbTimingDebug)
    
    -- |[Scattering]|
    --If both the lo and hi range are not nil, attempt to scatter the script. This slightly randomizes
    -- the starting positions so that NPCs don't always follow the exact same initial path when the map loads.
    if(piScatterRangeLo == nil or piScatterRangeHi == nil) then return end
    Cutscene_HandleParallel("Scatter", psNPCName, piScatterRangeLo, piScatterRangeHi)
end