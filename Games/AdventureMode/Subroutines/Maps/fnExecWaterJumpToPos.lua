-- |[ ================================= fnExecWaterJumpToPos() ================================= ]|
--Used in Starfield Swamp, causes the party to jump into a whirlpool, then transition to another
-- map. This fires a set of sprite creations, plays sounds, it's a whole event.
function fnExecWaterJumpToPos(pfWhirlAX, pfWhirlAY, pfWhirlBX, pfWhirlBY, pfLandX, pfLandY)
    
    -- |[Argument Check]|
    if(pfWhirlAX == nil) then return end
    if(pfWhirlAY == nil) then return end
    if(pfWhirlBX == nil) then return end
    if(pfWhirlBY == nil) then return end
    if(pfLandX   == nil) then return end
    if(pfLandY   == nil) then return end
    
    -- |[Focus Camera]|
    fnCutsceneCamera("Position", gcfCamera_Cutscene_Speed, pfWhirlAX, pfWhirlAY)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Jump Party In]|
    --Create lits of members.
    local saList = {gsPartyLeaderName}
    for i = 1, #gsaFollowerNames, 1 do
        table.insert(saList, gsaFollowerNames[i])
    end
    
    --Order the party to jump in.
    for i = 1, #saList, 1 do
        
        --Timing between members.
        if(i > 1) then
            fnCutsceneWait(10)
            fnCutsceneBlocker()
        end
        
        --Spawn the splash.
        fnSpawnSplash(saList[i] .. "SplashA")
        
        --Jump into the water.
        fnCutsceneFacePos(saList[i], pfWhirlAX, pfWhirlAY)
        fnCutsceneJumpTo(saList[i], pfWhirlAX, pfWhirlAY, 25)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneTeleport(saList[i], -1.25, -1.50)
        fnCutscenePlaySound("World|MedSplash")
    
        --Reposition the splash to this position, order it to reset animations, and to expire when done one animation.
        fnCutsceneMoveExpirableSprite(saList[i] .. "SplashA", pfWhirlAX, pfWhirlAY)
    end
    
    -- |[Reposition to Destination]|
    --Compute time.
    local fDistance = fnGetPlanarDistance(pfWhirlAX, pfWhirlAY, pfLandX, pfLandY) * 16.0
    local iTicks = (math.floor(fDistance / gcfCamera_Cutscene_Speed)) + 5
    
    --Execute.
    fnCutsceneCamera("Position", gcfCamera_Cutscene_Speed, pfLandX, pfLandY)
    fnCutsceneWait(iTicks)
    fnCutsceneBlocker()
    
    -- |[Party Appears]|
    --Order the party to jump out.
    for i = 1, #saList, 1 do
        
        --Timing between members.
        if(i > 1) then
            fnCutsceneWait(10)
            fnCutsceneBlocker()
        end
        
        --Spawn the splash.
        fnSpawnSplash(saList[i] .. "SplashB")
        
        --Emerge from the water.
        fnCutsceneTeleport(saList[i], pfWhirlBX, pfWhirlBY)
        fnCutsceneBlocker()
        fnCutsceneFacePos(saList[i], pfLandX, pfLandY)
        
        --Jump onto land.
        fnCutsceneJumpTo(saList[i], pfLandX, pfLandY, 15)
        fnCutsceneWait(15)
        fnCutscenePlaySound("World|MedSplash")
    
        --Reposition the splash to this position, order it to reset animations, and to expire when done one animation.
        fnCutsceneMoveExpirableSprite(saList[i] .. "SplashB", pfWhirlBX, pfWhirlBY)
        fnCutsceneBlocker()
    end
    
    -- |[Fold]|
    fnAutoFoldParty()
    
end