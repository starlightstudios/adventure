-- |[ ===================================== Chest Response ===================================== ]|
--The random chest handler, normally called with a keyword which then resolves the chest's contents.
-- If called with the special argument "OPENED CHEST", re-resolves the map pins.
if(fnArgCheck(1) == false) then return end
local sSwitchType = LM_GetScriptArgument(0)

-- |[Special Case]|
if(sSwitchType == "OPENED CHEST") then
    local sLevelName = AL_GetProperty("Name")
    GUIMap:fnRunPinHandlerOnAllMaps(sLevelName)
    return
end
