-- |[ ================================= fnConstructorFacing() ================================== ]|
--Causes the player party to change facing if a global variable is tripped. Used in room constructors.
function fnConstructorFacing()

    -- |[Error Check]|
    --If giForceFacing is -1, ignore it.
	if(giForceFacing == -1) then return end
    
    -- |[Set Facings]|
    --Variables.
    local iFaceX = 0
    local iFaceY = 0
    
    --Resolve facing directions.
    if(giForceFacing == gci_Face_North) then
        iFaceY = -1
    elseif(giForceFacing == gci_Face_NorthEast) then
        iFaceX = 1
        iFaceY = -1
    elseif(giForceFacing == gci_Face_East) then
        iFaceX = 1
    elseif(giForceFacing == gci_Face_SouthEast) then
        iFaceX = 1
        iFaceY = 1
    elseif(giForceFacing == gci_Face_South) then
        iFaceY = 1
    elseif(giForceFacing == gci_Face_SouthWest) then
        iFaceX = -1
        iFaceY = 1
    elseif(giForceFacing == gci_Face_West) then
        iFaceX = -1
    else
        iFaceX = -1
        iFaceY = -1
    end
    
    --Set facings.
    fnCutsceneFace(gsPartyLeaderName, iFaceX, iFaceY)
    for i = 1, giFollowersTotal, 1 do
        fnCutsceneFace(gsaFollowerNames[i], iFaceX, iFaceY)
    end
    
    -- |[Flag Reset]|
    --Always reset the flag back to -1.
    giForceFacing = -1
    
end
