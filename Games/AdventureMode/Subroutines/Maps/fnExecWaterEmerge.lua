-- |[ =================================== fnExecWaterEmerge() ================================== ]|
--Used in Starfield Swamp. Emerges from a whirlpool and lands on dry land.
function fnExecWaterEmerge(pfWhirlX, pfWhirlY, pfLandingX, pfLandingY)
    
    -- |[Argument Check]|
    if(pfWhirlX   == nil) then return end
    if(pfWhirlY   == nil) then return end
    if(pfLandingX == nil) then return end
    if(pfLandingY == nil) then return end
    
    -- |[Listing]|
    --Make a list of all party members, including the leader.
    local saList = {gsPartyLeaderName}
    for i = 1, #gsaFollowerNames, 1 do
        table.insert(saList, gsaFollowerNames[i])
    end
    
    -- |[Map Setup]|
    --Blackout.
    fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Camera.
    fnCutsceneCamera("Position", 1000, pfLandingX, pfLandingY)
    
    -- |[Move Party Offscreen]|
    --Reposition everyone offscreen. They teleport in when they jump out of the water.
    for i = 1, #saList, 1 do
        fnCutsceneTeleport(saList[i], -1.25, -1.50)
    end
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()

    -- |[Entry]|
    --For each party member.
    for i = 1, #saList, 1 do
        
        --Wait a few ticks for each party member after the first.
        if(i > 1) then
            fnCutsceneWait(15)
            fnCutsceneBlocker()
        end
        
        --Create a splash sprite.
        fnSpawnSplash(saList[i] .. "Splash")
        
        --Emerge from the water.
        fnCutscenePlaySound("World|MedSplash")
        fnCutsceneTeleport(saList[i], pfWhirlX, pfWhirlY)
        fnCutsceneFacePos(saList[i], pfLandingX, pfLandingY)
        fnCutsceneJumpTo(saList[i], pfLandingX, pfLandingY, 15)
        fnCutsceneMoveExpirableSprite(saList[i] .. "Splash", pfWhirlX, pfWhirlY)
        fnCutsceneBlocker()
    end

    -- |[Finish]|
    fnAutoFoldParty()
    fnCutsceneBlocker()
    
end