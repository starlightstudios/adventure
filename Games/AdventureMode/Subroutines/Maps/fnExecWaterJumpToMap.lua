-- |[ ================================= fnExecWaterJumpToMap() ================================= ]|
--Used in Starfield Swamp, causes the party to jump into a whirlpool, then transition to another
-- map. This fires a set of sprite creations, plays sounds, it's a whole event.
function fnExecWaterJumpToMap(pfWhirlX, pfWhirlY, psNewRoomName, piNewRoomX, piNewRoomY)
    
    -- |[Argument Check]|
    if(pfWhirlX == nil) then return end
    if(pfWhirlY == nil) then return end
    
    -- |[Focus Camera]|
    fnCutsceneCamera("Position", gcfCamera_Cutscene_Speed, pfWhirlX, pfWhirlY)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Jump Party In]|
    --Create lits of members.
    local saList = {gsPartyLeaderName}
    for i = 1, #gsaFollowerNames, 1 do
        table.insert(saList, gsaFollowerNames[i])
    end
    
    -- |[Jump Followers In]|
    --List is of variable size, may be zero.
    for i = 1, #saList, 1 do
        
        --Spawn the splash.
        fnSpawnSplash(saList[i] .. "Splash")
        
        --Jump into the water.
        fnCutsceneFacePos(saList[i], pfWhirlX, pfWhirlY)
        fnCutsceneJumpTo(saList[i], pfWhirlX, pfWhirlY, 25)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneTeleport(saList[i], -1.25, -1.50)
        fnCutscenePlaySound("World|MedSplash")
    
        --Reposition the splash to this position, order it to reset animations, and to expire when done one animation.
        fnCutsceneMoveExpirableSprite(saList[i] .. "Splash", pfWhirlX, pfWhirlY)
    end
    
    -- |[Transition to Next Room]|
    --Wait a bit.
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Transition.
    local sString = "AL_BeginTransitionTo(\"" .. psNewRoomName .. "\", \"FORCEPOS:"..piNewRoomX..".0x"..piNewRoomY..".0x0\")"
	fnCutscene(sString)
    
end