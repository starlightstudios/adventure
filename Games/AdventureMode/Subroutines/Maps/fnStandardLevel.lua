-- |[ ==================================== fnStandardLevel() =================================== ]|
--Standard level boot sequence. Creates a new AdventureLevel, parses the map data, and sets the examinable script to the
-- default location. Almost every level does this, and in this order, so it got its own function. It perfectly legal
-- to bypass this function call.
function fnStandardLevel(psCallingPath, piTileSize)

    -- |[Argument Check]|
    if(psCallingPath == nil) then return end
    
    -- |[Optional Argument]|
    --If this argument is not provided, use the default of 16.
    if(piTileSize == nil) then piTileSize = 16 end

    -- |[Execution]|
    --Clear all parallel scripts.
    Cutscene_HandleParallel("ClearAll")

	--Create the map and pass it to the MapManager.
	AL_Create()
	
	--Calling path must exist.
	if(psCallingPath == nil) then return end

	--Basic terrain data, automatically handles layers and depth-sorting.
	AL_ParseSLF(psCallingPath .. "MapData.slf", piTileSize)
    
    --Call the region marker.
    LM_ExecuteScript(gsRegionMarkerPath, psCallingPath)

	--Set the examination script to the standard path. Most examinable points are set via the .slf file object parsing.
    AL_SetProperty("Base Path", psCallingPath)
	AL_SetProperty("Trigger Script", psCallingPath .. "Triggers.lua")
	AL_SetProperty("Examine Script", psCallingPath .. "Examination.lua")
    gsFieldAbilityCheckPath = psCallingPath .. "FieldAbilities.lua"

    --Set the menu close script. Only certain chapters use this.
    local iCurrentChapter = VM_GetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N")
    if(iCurrentChapter == 1.0) then
        AL_SetProperty("Menu Close Script", gsRoot .. "Chapter 1/Menu Close Handler.lua")
    elseif(iCurrentChapter == 2.0) then
        AL_SetProperty("Menu Close Script", gsRoot .. "Chapter 2/Menu Close Handler.lua")
    elseif(iCurrentChapter == 5.0) then
        AL_SetProperty("Menu Close Script", gsRoot .. "Chapter 5/Menu Close Handler.lua")
    end

	--Drop all events. This prevents the game from running while the loading screen is firing.
	Debug_DropEvents()

end
