-- |[ ============================== fnAddWeaponToChartChristine =============================== ]|
--Adds a weapon to the standard item chart. Uses the standard function, but adds damage type handlers.
-- Christine uses an extra secondary damage type on her weapons as well.
function fnAddWeaponToChartChristine(psName, piValue, piGemSlots, pbIsKey, piUseCode, psSymbol22, piDamageType, psSecondaryType, piaStats)
    
    --Run the adder.
    local i = fnAddItemToChart(psName, piValue, piGemSlots, pbIsKey, piUseCode, psSymbol22, piaStats)
    
    --Modify.
    gzActiveChart[i].iDamageType = piDamageType
    gzActiveChart[i].sSecondaryType = psSecondaryType
    
    --Finish.
    return i
end