-- |[ ===================================== fnAddTagToChart ==================================== ]|
--For an item on the item chart, adds a tag the given number of times.
function fnAddTagToChart(psName, psTag, piCount)
    
    -- |[Arg Check]|
    if(psName  == nil) then return end
    if(psTag   == nil) then return end
    if(piCount == nil) then return end
    
    -- |[Execution]|
    --Scan. On match, set.
    for i = 1, #gzActiveChart, 1 do
        if(gzActiveChart[i].sName == psName) then
            table.insert(gzActiveChart[i].zaTagArray, {psTag, piCount})
        end
    end
end