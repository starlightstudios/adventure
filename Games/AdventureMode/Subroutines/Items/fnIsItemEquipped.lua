-- |[ =================================== fnIsItemEquipped() =================================== ]|
--Returns true if the named item is equipped on any party member in the active party.
function fnIsItemEquipped(psItemName)
    
    -- |[Error Check]|
    --Verify arguments.
    if(psItemName == nil) then return false end

    -- |[Iterate]|
    --Scan across the active party members.
    local iActivePartySize = AdvCombat_GetProperty("Active Party Size")
    for i = 0, iActivePartySize - 1, 1 do
        
        --Push the member.
        AdvCombat_SetProperty("Push Active Party Member By Slot", i)
        
            --Get their equipment slots and scan across them.
            local iTotalEquipSlots = AdvCombatEntity_GetProperty("Total Equipment Slots")
            for p = 0, iTotalEquipSlots-1, 1 do
                
                --Get the item in the slot. If the name matches, then succeed.
                local sItemName = AdvCombatEntity_GetProperty("Equipment In Slot I", p)
                if(sItemName == psItemName) then
                    DL_PopActiveObject()
                    return true
                end
            end
        DL_PopActiveObject()
    end
    
    -- |[Not Found]|
    --Didn't find any items. Fail.
    return false
end
