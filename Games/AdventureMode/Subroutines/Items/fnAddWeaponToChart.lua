-- |[ ================================== fnAddWeaponToChart() ================================== ]|
--Adds a weapon to the standard item chart. Uses the standard function, but adds damage type handlers.
function fnAddWeaponToChart(psName, piValue, piGemSlots, pbIsKey, piUseCode, psSymbol22, piDamageType, piaStats)
    
    --Run the adder.
    local i = fnAddItemToChart(psName, piValue, piGemSlots, pbIsKey, piUseCode, psSymbol22, piaStats)
    
    --Modify.
    gzActiveChart[i].iDamageType = piDamageType
    gzActiveChart[i].sSecondaryType = "No Secondary Type"
    
    --Finish.
    return i
end

-- |[ ============================== fnAddWeaponToChartMarkdown() ============================== ]|
--Adds a weapon but uses a stat string instead of arrays.
function fnAddWeaponToChartMarkdown(psName, piValue, piGemSlots, pbIsKey, piUseCode, psSymbol22, piDamageType, psStatString)
    
    --Get the tag/stat arrays using the markdown.
    local iaStatArray, zaTagArray = fnMarkdownToArrays(psStatString)
        
    --Run the adder.
    local i = fnAddItemToChart(psName, piValue, piGemSlots, pbIsKey, piUseCode, psSymbol22, iaStatArray)
    
    --Modify.
    gzActiveChart[i].zaTagArray = zaTagArray
    gzActiveChart[i].iDamageType = piDamageType
    gzActiveChart[i].sSecondaryType = "No Secondary Type"
    
    --Finish.
    return i
end