-- |[ ==================================== fnStandardizeDescription() =================================== ]|
--Item descriptions typically have the same text on the last 3-4 lines, such as showing properties, gem slots,
-- value, etc. This function appends the given standard onto the last lines of the description automatically.
-- This requires padding the description with newlines.
--The AdventureItem in question should be atop the activity stack.
function fnStandardizeDescription(psStandardEnding, piMaxLines)
    
    -- |[Error Check]|
    --Verify arguments.
    if(psStandardEnding == nil) then return end
    if(piMaxLines       == nil) then return end

    --Max lines can't be less than one.
    if(piMaxLines < 1) then return end

    -- |[Setup]|
    
    -- |[Standard Lines]|
    --Determine how many lines the standard occupies. It always requires 1, plus 1 for each newline.
    local iLinesForStandard = fnCountNewlines(psStandardEnding) + 1
    
    -- |[Description Lines]|
    --Retrieve the description.
    local sDescription = AdItem_GetProperty("Description")
    
    --Run any active translations.
    sDescription = Translate(gsTranslationItems, sDescription)
    
    --Replace [BR] tags with '\n' cases. This must be done *after* translation.
    sDescription = string.gsub(sDescription, "%[BR%]", "\n")
    
    --Replace [PCT] tags with '%' cases. Done after translation.
    sDescription = string.gsub(sDescription, "%[PCT%]", "%%%%")
    
    --Determine how many lines the description occupies. Starts at 1, plus 1 for each newline.
    local iLineCount = fnCountNewlines(sDescription) + 1
    
    --Add padding newlines so the standard winds up on the final lines.
    for i = iLineCount, piMaxLines-iLinesForStandard, 1 do
        sDescription = sDescription .. "\n"
    end
    
    -- |[Append]|
    --Now append the standard ending to place stats.
    sDescription = sDescription .. psStandardEnding
    
    -- |[Upload]|
    --Re-upload the description and process it.
    AdItem_SetProperty("Description", sDescription)
    AdItem_SetProperty("Process Description")
end
