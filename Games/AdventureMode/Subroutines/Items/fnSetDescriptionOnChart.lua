-- |[ ================================ fnSetDescriptionOnChart ================================= ]|
--For an item on the item chart, sets its description pre-markup. This is used to prevent clogging the 
-- scripts with extremely long lines since descriptions tend to be long. Go figure.
function fnSetDescriptionOnChart(psName, psDescription)
    
    -- |[Arg Check]|
    if(psName        == nil) then return end
    if(psDescription == nil) then return end
    
    -- |[Execution]|
    --Scan. On match, set description.
    for i = 1, #gzActiveChart, 1 do
        if(gzActiveChart[i].sName == psName) then
            gzActiveChart[i].sDescription = psDescription
        end
    end
end
