-- |[ =================================== fnAddAbilityToChart ================================== ]|
--For an item on the item chart, adds an ability to it. Used for combat-execution items.
function fnAddAbilityToChart(psName, psAbilityPath)
    
    -- |[Arg Check]|
    if(psName        == nil) then return end
    if(psAbilityPath == nil) then return end
    
    -- |[Execution]|
    --Scan. On match, set.
    for i = 1, #gzActiveChart, 1 do
        if(gzActiveChart[i].sName == psName) then
            gzActiveChart[i].sAbilityPath = psAbilityPath
        end
    end
end