-- |[ =================================== fnAddItemToChart() =================================== ]|
--Adds an item to the standard item chart, with basic properties.
function fnAddItemToChart(psName, piValue, piGemSlots, pbIsKey, piUseCode, psSymbol22, piaStats)
    
    --Setup.
    zItem = {}
    
    --System.
    zItem.sName = psName
    zItem.iValue = piValue
    zItem.iGemSlots = piGemSlots
    zItem.bIsKeyItem = pbIsKey
    zItem.iUsability = piUseCode
    
    --Display.
    zItem.sSymbol22 = psSymbol22
    zItem.sDescription = "No Description"
    
    --Weapons-only.
    zItem.iDamageType = gciDamageType_Slashing
    zItem.sSecondaryType = "No Secondary Type"
    
    --Abilities
    zItem.sAbilityPath = "Null"
    
    --Equipment stats array.
    zItem.iaStatArray = piaStats
    
    --Tags.
    zItem.zaTagArray = {}
    
    --Usability. Not always used.
    zItem.saUsabilityArray = {}
    
    --Store.
    local i = #gzActiveChart + 1
    gzActiveChart[i] = zItem
    return i
end

-- |[ =============================== fnAddItemToChartMarkdown() =============================== ]|
--Adds an item to the standard item chart, with basic properties. pzaTagTable is optional.
function fnAddItemToChartMarkdown(psName, piValue, piGemSlots, pbIsKey, piUseCode, psSymbol22, psStatString, pzaTagTable)

    -- |[Setup]|
    zItem = {}
    
    -- |[Variables]|
    --System.
    zItem.sName = psName
    zItem.iValue = piValue
    zItem.iGemSlots = piGemSlots
    zItem.bIsKeyItem = pbIsKey
    zItem.iUsability = piUseCode
    
    --Display.
    zItem.sSymbol22 = psSymbol22
    zItem.sDescription = "No Description"
    
    --Weapons-only.
    zItem.iDamageType = gciDamageType_Slashing
    zItem.sSecondaryType = "No Secondary Type"
    
    --Abilities
    zItem.sAbilityPath = "Null"
    
    --Usability. Not always used.
    zItem.saUsabilityArray = {}
    
    -- |[Markdown Handling]|
    --Get the tag/stat arrays using the markdown.
    local iaStatArray, zaTagArray = fnMarkdownToArrays(psStatString)
    zItem.iaStatArray = iaStatArray
    zItem.zaTagArray = zaTagArray
    
    -- |[Additional Tags]|
    --If pzaTagTable is not nil, adds them to the tag table.
    if(pzaTagTable ~= nil) then
        for i = 1, #pzaTagTable, 1 do
        
            --Name of tag.
            local sTagName  = pzaTagTable[i][1]
            local iQuantity = pzaTagTable[i][2]
        
            --Scan the existing tag array.
            local bFoundOnExistingTable = false
            for p = 1, #zItem.zaTagArray, 1 do
        
                --Match. Add.
                if(zItem.zaTagArray[p][1] == sTagName) then
                    bFoundOnExistingTable = true
                    zItem.zaTagArray[p][2] = pzaTagTable[i][2] + iQuantity
                    break
                end
            end
            
            --If the tag was not on the existing table, add a new entry.
            if(bFoundOnExistingTable == false) then
                table.insert(zaTagArray, {sTagName, iQuantity})
            end
        end
    end
    
    -- |[Register, Finish Up]|
    local iChartSlot = #gzActiveChart + 1
    table.insert(gzActiveChart, zItem)
    return iChartSlot
end