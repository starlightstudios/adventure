-- |[ ================================== fnSetUsabilityOnChart ================================= ]|
--For an item on the item chart, sets its usability flags. The flags are defined in the startup
-- variables.
function fnSetUsabilityOnChart(psName, piUsability)
    
    -- |[Arg Check]|
    if(psName      == nil) then return end
    if(piUsability == nil) then return end
    
    -- |[Execution]|
    --Scan. On match, set description.
    for i = 1, #gzActiveChart, 1 do
        
        --The item has the name in question:
        if(gzActiveChart[i].sName == psName) then
            
            --Clear usability.
            gzActiveChart[i].saUsabilityArray = {}
            
            --Iterate across the usability chart, checking if the flags provided match usabilities. If they
            -- do, add the character's name.
            --Note that it is possible for the same flag to match multiple characters.
            for p = 1, #gzaUsabilityTable, 1 do
            
                --Fast-access variables.
                local iFlag = gzaUsabilityTable[p][1]
                local sName = gzaUsabilityTable[p][2]
            
                --Match:
                if(piUsability & iFlag > 0) then
                    table.insert(gzActiveChart[i].saUsabilityArray, sName)
                end
            end
            
        end
    end
end
