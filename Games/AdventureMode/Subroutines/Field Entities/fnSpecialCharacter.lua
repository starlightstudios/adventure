-- |[ ================================== fnSpecialCharacter() ================================== ]|
--Creates a character with a special name, such as a player character or party member, or major NPC.
-- This is the same as creating the character normally, but will also automatically call the requisite
-- costume handler for that character.
--It is legal to pass nil for the dialogue path. With no dialogue path, the character cannot be interacted with. This
-- is for party members.
--Returns the ID of the created character, or 0 on error.
--Built from: ./ZLaunch.lua
--Example: fnSpecialCharacter("Florentina", -100, -100, gci_Face_South, false, nil)
function fnSpecialCharacter(psCharacterName, piXPos, piYPos, piFacing, pbIsClipped, psDialoguePath)
	
	-- |[Arg check]|
	if(psCharacterName == nil) then return 0 end
	if(piXPos          == nil) then return 0 end
	if(piYPos          == nil) then return 0 end
	if(piFacing        == nil) then return 0 end
	if(pbIsClipped     == nil) then return 0 end
	
	-- |[Common Code]|
	TA_Create(psCharacterName)
	
		--Get the ID of the created character.
		local iID = RE_GetID()
	
        --Basic Properties
		TA_SetProperty("Position", piXPos, piYPos)
		TA_SetProperty("Facing", piFacing)
		TA_SetProperty("Clipping Flag", pbIsClipped)
		if(psDialoguePath ~= nil) then 
			TA_SetProperty("Activation Script", psDialoguePath)
		end
	DL_PopActiveObject()
    
    -- |[Costume Handler]|
    --Run the resolver.
    if(gbDontRunCostumeHandler == false) then
        LM_ExecuteScript(gsCharacterAutoresolve, psCharacterName)
    end
    
    -- |[Finish]|
	return iID
end
