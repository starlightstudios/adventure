-- |[ ===================================== fnSpawnSplash() ==================================== ]|
--Spawns a splashing animation offscreen, for use with cutscenes.
function fnSpawnSplash(psName)
    
    -- |[Argument Check]|
    if(psName == nil) then return end
    
    -- |[Spawn]|
	TA_Create(psName)
		TA_SetProperty("Position", -100, -100)
		TA_SetProperty("Rendering Depth", 0.000000)
		TA_SetProperty("Walk Ticks Per Frame", 5)
		TA_SetProperty("Auto Animates Fast", true)
		for i = 1, 8, 1 do
			for p = 1, 4, 1 do
				TA_SetProperty("Move Frame", i-1, p-1, "Root/Images/Sprites/Splash/" .. (p-1))
			end
		end
	DL_PopActiveObject()
    
end
