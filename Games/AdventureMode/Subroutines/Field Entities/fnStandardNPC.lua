-- |[Function: fnStandardNPC]|
--Standard NPC which the player can talk to. All parameters are mandator. The dialogue extension always checks 
-- the Root/CharacterDialogue/ folder, and adds the .lua for you.
--Built from: ./ZLaunch.lua
--Example: fnStandardNPC("AmuletVendor", 34, 20, gci_Face_North, "Imp", "AmuletVendor/Root")
function fnStandardNPC(sName, fXPosition, fYPosition, iFacing, sSpritePath, sDialogueExtension)

	--Arg check.
	if(sName              == nil) then return end
	if(fXPosition         == nil) then return end
	if(fYPosition         == nil) then return end
	if(iFacing            == nil) then return end
	if(sSpritePath        == nil) then return end
	if(sDialogueExtension == nil) then return end

	--Create.
	TA_Create(sName)
		TA_SetProperty("Position", fXPosition, fYPosition)
		TA_SetProperty("Facing", iFacing)
		TA_SetProperty("Clipping Flag", true)
		fnSetCharacterGraphics("Root/Images/Sprites/" .. sSpritePath .. "/", false)
		TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/" .. sDialogueExtension .. ".lua")
	DL_PopActiveObject()

end

--Alternate, uses Location Name instead of fixed coordinates.
function fnStandardNPCLocation(sName, sLocationName, iFacing, sSpritePath, sDialogueExtension)

	--Arg check.
	if(sName              == nil) then return end
	if(sLocationName      == nil) then return end
	if(iFacing            == nil) then return end
	if(sSpritePath        == nil) then return end
	if(sDialogueExtension == nil) then return end

	--Create.
	TA_Create(sName)
		TA_SetProperty("Position By Entity", sLocationName)
		TA_SetProperty("Facing", iFacing)
		TA_SetProperty("Clipping Flag", true)
		fnSetCharacterGraphics("Root/Images/Sprites/" .. sSpritePath .. "/", false)
		TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/" .. sDialogueExtension .. ".lua")
	DL_PopActiveObject()

end
