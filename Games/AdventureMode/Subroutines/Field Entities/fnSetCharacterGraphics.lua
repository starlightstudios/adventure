-- |[ ================================ fnSetCharacterGraphics() ================================ ]|
--Assuming a TiledActor is on the Activity Stack, sets their rendering frames according to the standard pattern.
-- The DLPath is generally one used in fnLoadCharacterGraphics().
--Examples:
--fnSetCharacterGraphics("Root/Images/Sprites/Mei_Human/", true)
function fnSetCharacterGraphics(sDLPath, bUsesEightDirections)

	-- |[ ==================== Argument Check ==================== ]|
	if(sDLPath              == nil) then return end
	if(bUsesEightDirections == nil) then return end
	
    -- |[ ==================== Special Cases ===================== ]|
	-- |[Goat]|
    --Semi-animated goat oscillated between two frames.
	if(sDLPath == "Goat") then
		for i = 1, 8, 1 do
			for p = 1, 4, 2 do
				TA_SetProperty("Move Frame", i-1, p-1, "Root/Images/Sprites/Goat/1")
				TA_SetProperty("Run Frame",  i-1, p-1, "Root/Images/Sprites/Goat/1")
			end
			for p = 2, 4, 2 do
				TA_SetProperty("Move Frame", i-1, p-1, "Root/Images/Sprites/Goat/2")
				TA_SetProperty("Run Frame",  i-1, p-1, "Root/Images/Sprites/Goat/2")
			end
		end
        return
	end

    -- |[Gala Golems]|
    --Fancy Golems. All movement sprites are their idle sprites.
    if(string.sub(sDLPath, 1, 30) == "Root/Images/Sprites/GolemFancy") then
		--Specify shadows.
		TA_SetProperty("Shadow", gsStandardShadow)
	
		--Standardized loading set.
		local saSets =   {"North", "NE", "East", "SE", "South", "SW", "West", "NW"}
		local iaFrames = {      4,    0,      4,    0,       4,    0,      4,    0}
		
		--Setting loop.
		local i = 1
		while(saSets[i] ~= nil) do
			for p = 1, iaFrames[i], 1 do
				TA_SetProperty("Move Frame", i-1, p-1, sDLPath .. saSets[i] .. "|0")
				TA_SetProperty("Run Frame",  i-1, p-1, sDLPath .. saSets[i] .. "|0")
			end
			i = i + 1
		end
        return
    end
    
	-- |[ ===================== Normal Case ====================== ]|
    --Specify shadows.
    TA_SetProperty("Shadow", gsStandardShadow)
    
    --Frame resolve. This defaults to TA_MOVE_IMG_STANDARD, but can be between 1 and TA_MOVE_IMG_STANDARD.
    local iMovImg = TA_GetProperty("Move Images Total")

    -- |[Standard Set]|
    --Standardized loading set.
    local saSets =   {"North", "NE",  "East", "SE", "South", "SW",  "West", "NW"}
    local iaFrames = {iMovImg,    0, iMovImg,    0, iMovImg,    0, iMovImg,    0}
    if(bUsesEightDirections) then
        iaFrames = {iMovImg, iMovImg, iMovImg, iMovImg, iMovImg, iMovImg, iMovImg, iMovImg}
    end
    
    --Setting loop.
    for i = 1, #saSets, 1 do
        for p = 1, iaFrames[i], 1 do
            TA_SetProperty("Move Frame", i-1, p-1, sDLPath .. saSets[i] .. "|" .. (p-1))
            TA_SetProperty("Run Frame",  i-1, p-1, sDLPath .. saSets[i] .. "|" .. (p-1))
        end
    end
    
    -- |[4-Direction Entities]|
    --Additional frames. 4-directional entities use this.
    if(bUsesEightDirections == false) then
        for p = 1, iaFrames[7], 1 do
            TA_SetProperty("Move Frame", 5, p-1, sDLPath .. saSets[7] .. "|" .. (p-1))
            TA_SetProperty("Run Frame",  5, p-1, sDLPath .. saSets[7] .. "|" .. (p-1))
            TA_SetProperty("Move Frame", 7, p-1, sDLPath .. saSets[7] .. "|" .. (p-1))
            TA_SetProperty("Run Frame",  7, p-1, sDLPath .. saSets[7] .. "|" .. (p-1))
        end
        for p = 1, iaFrames[3], 1 do
            TA_SetProperty("Move Frame", 1, p-1, sDLPath .. saSets[3] .. "|" .. (p-1))
            TA_SetProperty("Run Frame",  1, p-1, sDLPath .. saSets[3] .. "|" .. (p-1))
            TA_SetProperty("Move Frame", 3, p-1, sDLPath .. saSets[3] .. "|" .. (p-1))
            TA_SetProperty("Run Frame",  3, p-1, sDLPath .. saSets[3] .. "|" .. (p-1))
        end
    end
    
    -- |[8-Direction entities]|
    --If this is an 8-directional character, they generally have run frames associated. Only characters
    -- who are in the player's party, or who are in cutscenes, ever actually run.
    if(bUsesEightDirections) then
    
        --Special: Darkmatters have 8 idle/walk directions, but don't have 8 run directions. Most other
        -- sprite sheets do, but Darkmatters don't, so special code prevents console errors.
        if(string.sub(sDLPath, 1, 34) == "Root/Images/Sprites/DarkmatterGirl") then
            for i = 1, #saSets, 1 do
                for p = 1, iaFrames[i], 1 do
                    TA_SetProperty("Run Frame",  i-1, p-1, sDLPath .. saSets[i] .. "|" .. (p-1))
                end
            end
    
        --All other cases:
        else
            for i = 1, #saSets, 1 do
                for p = 1, iaFrames[i], 1 do
                    TA_SetProperty("Run Frame",  i-1, p-1, sDLPath .. saSets[i] .. "|Run" .. (p-1))
                end
            end
        end
    end
end
