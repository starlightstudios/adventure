-- |[ ================================== fnHasSightBetween() =================================== ]|
--Iterates from X1 to Y1, checking every pixel in a rough line between them for collisions. Returns
-- true if the line has no collisions, false otherwise.
function fnHasSightBetween(pfX1, pfY1, pfX2, pfY2, piCollisionLayer)
    
    -- |[Argument Check]|
    if(pfX1 == nil) then return false end
    if(pfY1 == nil) then return false end
    if(pfX2 == nil) then return false end
    if(pfY2 == nil) then return false end
    
    --Optional, if no layer is specified, use 0.
    if(piCollisionLayer == nil) then piCollisionLayer = 0 end
    
    -- |[Angle]|
    --Get the angle between the two.
    local fXPos = pfX1
    local fYPos = pfY1
    local fAngleTo = math.atan2(pfY2 - pfY1, pfX2 - pfX1)
    local iDistance = math.floor(fnGetPlanarDistance(pfX1, pfY1, pfX2, pfY2))
    
    -- |[Iterate]|
    --io.write("Pulsing:\n")
    --io.write(" " .. pfX1 .. "x" .. pfY1 .. " to " .. pfX2 .. "x" .. pfY2 .. "\n")
    --io.write(" Angle: " .. fAngleTo .. "\n")
   -- io.write(" Distance: " .. iDistance .. "\n")
    for i = 1, iDistance, 1 do
    
        --Increment position.
        fXPos = fXPos + math.cos(fAngleTo)
        fYPos = fYPos + math.sin(fAngleTo)
    
        --Check for a hit.
        --io.write(" Checking " .. math.floor(fXPos) .. " x " .. math.floor(fYPos) .. "\n")
        if(AL_GetProperty("Collision Pixel", math.floor(fXPos), math.floor(fYPos), piCollisionLayer)) then
            return false
        end

    end

    -- |[All Checks Passed]|
    --Line of sight is clear.
    return true
end