-- |[ ================================ fnNPCPathToRandomNode() ================================= ]|
--Causes an NPC to path to one of the nodes in the provided list. This uses the pathing algorithm
-- used by enemies to get around. It should be inside a parallel script.
function fnNPCPathToRandomNode(psEntityName, pfWalkSpeed, psaNodeList)
    
    -- |[Argument Check]|
    if(psEntityName == nil) then return end
    if(pfWalkSpeed  == nil) then return end
    if(psaNodeList  == nil) then return end

    --Debug:
    --io.write("Running fnNPCPathToRandomNode.\n")

    -- |[Variables]|
    local iTargetX = 0
    local iTargetY = 0
    local iTargetFace = 0
    local iTargetLinger = 0

    -- |[Entity Data]|
    --Make sure the entity exists.
    if(EM_Exists(psEntityName) == false) then
        io.write("fnNPCPathToRandomNode() - Error, no entity: " .. psEntityName .. "\n")
        return
    end
    
    --Get position of this entity.
    EM_PushEntity(psEntityName)
        local fEntityX, fEntityY = TA_GetProperty("Position")
    DL_PopActiveObject()

    --Convert to tile coordinates.
    local iTileX = math.floor(fEntityX / gciSizePerTile)
    local iTileY = math.floor(fEntityY / gciSizePerTile)
    
    --Debug.
    --io.write(" NPC: " .. psEntityName .. "\n")
    --io.write(" Pos: " .. iTileX .. "x" .. iTileY .. "\n")

    -- |[Node Selection]|
    --Select a node. If it's too close to the current position, remove it and try again.
    while(true) do
        
        --List is empty. Fail.
        if(#psaNodeList < 1) then
            --io.write("  Failed. All nodes rejected.\n")
            return
        end
        
        --Select a node.
        local iRoll = LM_GetRandomNumber(1, #psaNodeList)
        local sNodeName = psaNodeList[iRoll]
        
        --Get properties.
        iTargetX, iTargetY = AL_GetProperty("Patrol Node Location", sNodeName)
        iTargetFace        = AL_GetProperty("Patrol Node Facing",   sNodeName)
        iTargetLinger      = AL_GetProperty("Patrol Node Linger",   sNodeName)
        
        --Convert node position to tile coordinates.
        iTargetX = math.floor(iTargetX / gciSizePerTile)
        iTargetY = math.floor(iTargetY / gciSizePerTile)

        --Debug.
        --io.write(" Rolled node " .. iRoll .. " named " .. sNodeName .. "\n")
        --io.write("  Pos: " .. iTargetX .. "x" .. iTargetY .. "\n")

        --Check distance. If it's less than one tile, this node is illegal (or we're already there). Pick another node.
        local fXDistSqr = (iTargetX - iTileX) * (iTargetX - iTileX)
        local fYDistSqr = (iTargetY - iTileY) * (iTargetY - iTileY)
        local fDistance = math.sqrt(fXDistSqr + fYDistSqr)
        if(fDistance <= 1.0) then
            table.remove(psaNodeList, iRoll)
            --io.write("  Rejected.\n")
            
        --Otherwise, select this node.
        else
            break
        end
    end

    -- |[Path Resolve]|
    --Select a position to move to.
    --io.write(" Running path algorithm to selected node.\n")
    PathTile_RunPathing(iTileX, iTileY, iTargetX, iTargetY)

    --Get the length of the path.
    local iPathLen = Path_GetPathLength()
    --io.write(" Path len: " .. iPathLen .. "\n")
    for i = 0, iPathLen-1, 1 do
        
        --Get the instruction.
        local iDirection = Path_GetPathDirection(i)
        
        --North:
        if(iDirection == gci_Face_North) then
            iTileY = iTileY - 1
        
        --East
        elseif(iDirection == gci_Face_East) then
            iTileX = iTileX + 1

        --South
        elseif(iDirection == gci_Face_South) then
            iTileY = iTileY + 1

        --West
        elseif(iDirection == gci_Face_West) then
            iTileX = iTileX - 1
        end

        --Path to this position
        fnCutsceneMove(psEntityName, iTileX + 0.25, iTileY + 0.50, pfWalkSpeed)
        fnCutsceneBlocker()
    end

    --Resolve direction to face.
    local iCutsceneFaceX = 0
    local iCutsceneFaceY = 0
    if(iTargetFace == gci_Face_North) then
        iCutsceneFaceY = -1
    elseif(iTargetFace == gci_Face_East) then
        iCutsceneFaceX = 1
    elseif(iTargetFace == gci_Face_South) then
        iCutsceneFaceY = 1
    elseif(iTargetFace == gci_Face_West) then
        iCutsceneFaceX = -1
    end

    --Wait at the position in question.
    fnCutsceneBlocker()
    fnCutsceneFace(psEntityName, iCutsceneFaceX, iCutsceneFaceY)
    fnCutsceneWait(iTargetLinger)
    fnCutsceneBlocker()

end