-- |[ ====================================== fnDespawnNPC ====================================== ]|
--Removes an NPC from existence.
function fnDespawnNPC(psName)
    
    -- |[Argument Check]|
    if(psName == nil) then return end
    
    -- |[Remove]|
    EM_PushEntity(psName)
        RE_SetDestruct(true)
    DL_PopActiveObject()
end
