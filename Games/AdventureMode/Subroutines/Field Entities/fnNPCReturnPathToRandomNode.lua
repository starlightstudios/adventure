-- |[ ============================= fnNPCReturnPathToRandomNode() ============================== ]|
--Creates a path between the entity position and a randomly selected node. Returns this path as
-- a table containing the movement instructions.
--The table format is {{X1, Y1}, {X2, Y2}, etc}
--If the table comes back empty, there was an error or no path was found.
function fnNPCReturnPathToRandomNode(psEntityName, psaNodeList)
    
    -- |[Argument Check]|
    local iaMoveInstructions = {}
    if(psEntityName == nil) then return iaMoveInstructions end
    if(psaNodeList  == nil) then return iaMoveInstructions end

    --Debug:
    --io.write("Running fnNPCReturnPathToRandomNode().\n")

    -- |[Variables]|
    local iTargetX = 0
    local iTargetY = 0
    local iTargetFace = 0
    local iTargetLinger = 0

    -- |[Entity Data]|
    --Get position of this entity.
    EM_PushEntity(psEntityName)
        local fEntityX, fEntityY = TA_GetProperty("Position")
    DL_PopActiveObject()

    --Convert to tile coordinates.
    local iTileX = math.floor(fEntityX / gciSizePerTile)
    local iTileY = math.floor(fEntityY / gciSizePerTile)
    
    --Debug.
    --io.write(" NPC: " .. psEntityName .. "\n")
    --io.write(" Pos: " .. iTileX .. "x" .. iTileY .. "\n")
    --io.write(" Size: " .. gciSizePerTile .. "\n")

    -- |[Node Selection]|
    --Select a node. If it's too close to the current position, remove it and try again.
    while(true) do
        
        --List is empty. Fail.
        if(#psaNodeList < 1) then
            io.write("  Failed. All nodes rejected.\n")
            return iaMoveInstructions
        end
        
        --Select a node.
        local iRoll = LM_GetRandomNumber(1, #psaNodeList)
        local sNodeName = psaNodeList[iRoll]
        
        --Get properties.
        iTargetX, iTargetY = AL_GetProperty("Patrol Node Location", sNodeName)
        iTargetFace        = AL_GetProperty("Patrol Node Facing",   sNodeName)
        iTargetLinger      = AL_GetProperty("Patrol Node Linger",   sNodeName)
        
        --Convert node position to tile coordinates.
        iTargetX = math.floor(iTargetX / gciSizePerTile)
        iTargetY = math.floor(iTargetY / gciSizePerTile)

        --Debug.
        --io.write(" Rolled node " .. iRoll .. " named " .. sNodeName .. "\n")
        --io.write("  Pos: " .. iTargetX .. "x" .. iTargetY .. "\n")

        --Check distance. If it's less than one tile, this node is illegal (or we're already there). Pick another node.
        local fXDistSqr = (iTargetX - iTileX) * (iTargetX - iTileX)
        local fYDistSqr = (iTargetY - iTileY) * (iTargetY - iTileY)
        local fDistance = math.sqrt(fXDistSqr + fYDistSqr)
        if(fDistance <= 1.0) then
            table.remove(psaNodeList, iRoll)
            --io.write("  Rejected.\n")
            
        --Otherwise, select this node.
        else
            break
        end
    end

    -- |[Path Resolve]|
    --Select a position to move to.
    --io.write(" Running path algorithm to selected node.\n")
    PathTile_RunPathing(iTileX, iTileY, iTargetX, iTargetY)

    --Get the length of the path.
    local iPathLen = Path_GetPathLength()
    --io.write(" Path len: " .. iPathLen .. "\n")
    for i = 0, iPathLen-1, 1 do
        
        --Get the instruction.
        local iDirection = Path_GetPathDirection(i)
        
        --North:
        if(iDirection == gci_Face_North) then
            iTileY = iTileY - 1
        
        --East
        elseif(iDirection == gci_Face_East) then
            iTileX = iTileX + 1

        --South
        elseif(iDirection == gci_Face_South) then
            iTileY = iTileY + 1

        --West
        elseif(iDirection == gci_Face_West) then
            iTileX = iTileX - 1
        end

        --Path to this position
        table.insert(iaMoveInstructions, {iTileX, iTileY})
    end

    -- |[Finish]|
    return iaMoveInstructions
end