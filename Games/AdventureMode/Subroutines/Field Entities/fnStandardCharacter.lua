-- |[Function: fnStandardCharacter]|
--Creates a debug character at the provided debug location, which is usually set as a PlayerStart object in the map's objects layers.
-- An AdventureLevel should be atop the activity stack.
--Entities are registered in the opposite order of their appearance! This is to make sure the party leader renders in front in
-- case of ties for depth.
--Built from: ./ZLaunch.lua
--Example: fnStandardCharacter()
function fnStandardCharacter()

	--Get the location to spawn the player.
	local iSpawnX, iSpawnY = AL_GetProperty("Player Start")
	
	--Create party members.
	if(giFollowersTotal > 0) then
		for i = giFollowersTotal, 1, -1 do
			
			--Create this entity.
			TA_Create(gsaFollowerNames[i])
				giaFollowerIDs[i] = RO_GetID()
				TA_SetProperty("Position", iSpawnX, iSpawnY)
				fnSetCharacterGraphics("Root/Images/Sprites/" .. gsaFollowerNames[i] .. "/", true)
				
				--Special frames.
				TA_SetProperty("Wipe Special Frames")
				
				--Mei's frames.
				if(gsaFollowerNames[i] == "Mei") then
					TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/Mei|Wounded")
					TA_SetProperty("Add Special Frame", "Crouch",  "Root/Images/Sprites/Special/Mei|Crouch")
				
				--Christine's frames.
				elseif(gsaFollowerNames[i] == "Christine") then
					TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/Christine|Wounded")
					TA_SetProperty("Add Special Frame", "Crouch",  "Root/Images/Sprites/Special/Christine|Crouch")
					TA_SetProperty("Add Special Frame", "Laugh0",  "Root/Images/Sprites/Special/Christine|Laugh0") --Golem
					TA_SetProperty("Add Special Frame", "Laugh1",  "Root/Images/Sprites/Special/Christine|Laugh1") --Golem
					TA_SetProperty("Add Special Frame", "Sad",     "Root/Images/Sprites/Special/Christine|Sad")    --Golem
				
				--Florentina's frames.
				elseif(gsaFollowerNames[i] == "Florentina") then
					TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/Florentina|Wounded")
					TA_SetProperty("Add Special Frame", "Crouch",  "Root/Images/Sprites/Special/Florentina|Crouch")
					TA_SetProperty("Add Special Frame", "Sleep",   "Root/Images/Sprites/Special/Florentina|Sleep")
                
                --55's frames.
				elseif(gsaFollowerNames[i] == "Tiffany") then
					TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/55|Wounded")
					TA_SetProperty("Add Special Frame", "Crouch",  "Root/Images/Sprites/Special/55|Crouch")
                
                --SX-399's frames.
				elseif(gsaFollowerNames[i] == "SX-399") then
                    TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/SX399/Wounded")
                    TA_SetProperty("Add Special Frame", "Crouch",  "Root/Images/Sprites/SX399/Crouch")
                    TA_SetProperty("Add Special Frame", "Laugh0",  "Root/Images/Sprites/SX399/Laugh0")
                    TA_SetProperty("Add Special Frame", "Laugh1",  "Root/Images/Sprites/SX399/Laugh1")
				end
			DL_PopActiveObject()
			
			--Add to the follower IDs.
			AL_SetProperty("Follow Actor ID", giaFollowerIDs[i])
		end
	end

	--Resolve the name of the 0th character in the combat party. This is the party leader. It can (theoretically) vary
	-- in Chapter 6, but 1-5 always have the same leader.
    if(gbSuppressLeaderCreation == true) then return end
    if(gsPartyLeaderName == nil) then gsPartyLeaderName = "Mei" end

	--Create the default character. This is Mei in Chapter 1. The sprites selected are based on the party leader's name.
	TA_Create(gsPartyLeaderName)
		giPartyLeaderID = RO_GetID()
		TA_SetProperty("Position", iSpawnX, iSpawnY)
		fnSetCharacterGraphics("Root/Images/Sprites/" .. gsPartyLeaderName .. "_Human/", true)
		
		--Special frames.
        TA_SetProperty("Wipe Special Frames")
        TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/" .. gsPartyLeaderName .. "|Wounded")
        TA_SetProperty("Add Special Frame", "Crouch",  "Root/Images/Sprites/Special/" .. gsPartyLeaderName .. "|Crouch")
	DL_PopActiveObject()
	
	--Order the player to take control of the party leader.
	AL_SetProperty("Player Actor ID", giPartyLeaderID)

end
