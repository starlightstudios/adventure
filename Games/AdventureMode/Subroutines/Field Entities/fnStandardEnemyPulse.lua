-- |[ ================================= fnStandardEnemyPulse() ================================= ]|
--Once a level has booted, it usually spawns the enemies in it and tells them to ignore the player if they
-- are of the same form as that enemy. This function does that.
function fnStandardEnemyPulse()

    -- |[ =================== Setup ==================== ]|
	--Spawn the enemies.
	AL_SetProperty("Run Enemy Spawner")

    --Which chapter?
    local sPlayerForm = "Null"
    local iCurrentChapter = VM_GetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N")

    --String assembly.
    local sUseString = ""

    -- |[ ================= Chapter 1 ================== ]|
    if(iCurrentChapter == 0.0 or iCurrentChapter == 1.0) then

        --Mei's form.
        sPlayerForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
        sUseString = sUseString .. sPlayerForm .. "|"

        --Badge listing.
        local saBadgeListing = {}
        table.insert(saBadgeListing, {"Nature Warden Badge", "Alraune|"})
        table.insert(saBadgeListing, {"Bee Vanguard Badge",  "Bee|"})
        table.insert(saBadgeListing, {"Werecat Claw Badge",  "Werecat|"})
        table.insert(saBadgeListing, {"Slime Pal Badge",     "Slime|"})
        table.insert(saBadgeListing, {"Burning Soul Badge",  "Wisphag|"})

        --Iterate.
        for i = 1, #saBadgeListing, 1 do
            if(fnIsItemEquipped(saBadgeListing[i][1])) then
                sUseString = sUseString .. saBadgeListing[i][2]
            end
        end

    -- |[ ================= Chapter 2 ================== ]|
    elseif(iCurrentChapter == 2.0) then

        --Form.
        sPlayerForm = VM_GetVar("Root/Variables/Global/Sanya/sForm", "S")
        sUseString = sUseString .. sPlayerForm .. "|"

        --Wing Badge. Makes harpies ignore the party.
        if(fnIsItemEquipped("Wing Badge")) then
            sUseString = sUseString .. "Harpy|"
        end

    --Chapter 5. Player form.
    elseif(iCurrentChapter == 5.0) then
        sPlayerForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
        sUseString = sUseString .. sPlayerForm .. "|"

        --Badge listing.
        local saBadgeListing = {}
        table.insert(saBadgeListing, {"Voidguard Badge", "Darkmatter|"})

        --Iterate.
        for i = 1, #saBadgeListing, 1 do
            if(fnIsItemEquipped(saBadgeListing[i][1])) then
                sUseString = sUseString .. saBadgeListing[i][2]
            end
        end
    
    -- |[ ============== Mods / Unhandled ============== ]|
    else
    
        --Set flag.
        gsModPlayerForm = "Null"
        
        --Iterate across the mods to see if any caught the pulse.
        local bModHandledPulse = fnExecModScript("Combat/Enemy Pulse Response.lua")
        if(bModHandledPulse == true) then
            sPlayerForm = gsModPlayerForm
        else
            io.write("Chapter not found in fnStandardEnemyPulse.lua\n")
        end
    end
    
    -- |[ ================= Run Pulse ================== ]|
    --Pulse. The program will handle subdivision.
    AL_PulseIgnore(sUseString)
end
