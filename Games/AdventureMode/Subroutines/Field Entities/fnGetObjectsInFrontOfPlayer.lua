-- |[ ============================= fnGetObjectsInFrontOfPlayer() ============================== ]|
--Returns a list of the names and types of objects in front of the player, as if they were using the
-- Activate key to examine something. The list is formatted:
--{{sNameA, iTypeA}, {sNameB, iTypeB}, ...}
--Note: The list can legally be empty if nothing was hit.
function fnGetObjectsInFrontOfPlayer(pbAllowExtendedActivate)
    
    -- |[Argument Check]|
    if(pbAllowExtendedActivate == nil) then return {} end
    
    -- |[Setup]|
    local zaReturnList = {}
    
    -- |[Examination Position]|
    --Get the party leader's position.
    EM_PushEntity(gsPartyLeaderName)
        local iPartyX, iPartyY = TA_GetProperty("Position")
        local iFacing = TA_GetProperty("Facing")
    DL_PopActiveObject()
    
    --Turn to radians.
    local fRadians = (iFacing-2) * 45 * 3.1415926 / 180.0
    
    --Modify by rotation.
    iPartyX = iPartyX + (math.cos(fRadians) * 8.0) - 4
    iPartyY = iPartyY + (math.sin(fRadians) * 8.0) - 8
    
    --List of locations to check. These are in an X arrangement around the center
    -- point, to allow some overlap in case of narrow miss.
    local iaOffX = {0, -4, 4, -4, 4, 12, 12, -12, -12, -4,  4,  -4,   4}
    local iaOffY = {0, -4, -4, 4, 4, -4,  4,  -4,   4, 12, 12, -12, -12}
    
    -- |[Iterate Across Objects]|
    --Iterate across the offsets.
    for i = 1, #iaOffX, 1 do
        
        --Build, get total.
        AL_GetProperty("Build Objects At Position", iPartyX + iaOffX[i], iPartyY + iaOffY[i], true)
        local iTotalHits = AL_GetProperty("Total Objects At Position")
        
        --Check all hits.
        for p = 0, iTotalHits-1, 1 do
            
            --Get variables.
            local sName = AL_GetProperty("Name Of Object At Position", p)
            local iType = AL_GetProperty("Type Of Object At Position", p)
            
            --Store in the list. Remove duplicates by not adding if the entry is already present.
            local bAlreadyFound = false
            for z = 1, #zaReturnList, 1 do
                if(zaReturnList[z][1] == sName) then
                    bAlreadyFound = true
                end
            end
            
            --If the entry has not been recorded yet:
            if(bAlreadyFound == false) then
                table.insert(zaReturnList, {sName, iType})
            end
        end
    end
    
    -- |[Finish Up]|
    return zaReturnList
end
