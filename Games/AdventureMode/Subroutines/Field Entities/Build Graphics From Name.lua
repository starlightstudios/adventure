-- |[ ================================ Build Graphics From Name ================================ ]|
--This script is called from the C++ code for entities constructed inside the C++ and not Lua functions,
-- which is usually entities defined on map with "Position" or "Enemy" types.
--The entity will be the active object.

-- |[Arguments]|
local sGraphicalName = LM_GetScriptArgument(0) --String in the "Sprite" field in Tiled.
local iHasRunFrames  = LM_GetScriptArgument(1, "N")
local iDirections    = LM_GetScriptArgument(2, "N") --Will be 2, 4, or 8

-- |[Constants]|
local ciDirectionsTotal = 8
local ciFramesTotal = 4

-- |[ ====================================== Special Cases ===================================== ]|
--Some sprite names have different layouts than others.

-- |[Null]|
--Don't bother setting any images.
if(sGraphicalName == "Null") then return end

--AUTO means the frames are going to be repopulated later.
if(sGraphicalName == "AUTO") then return end

-- |[BagRefill]|
--Recovers the player's doctor bag when touched.
if(sGraphicalName == "BagRefill") then
    
    --Flags.
    TA_SetProperty("Auto Animates", true)
    
    --Images.
    for i = 0, ciDirectionsTotal-1, 1 do
        
        --First three frames cycle normally.
        for p = 0, ciFramesTotal-2, 1 do
            TA_SetProperty("Move Frame", i, p, "Root/Images/Sprites/BagRefill/"..p)
            TA_SetProperty("Run Frame",  i, p, "Root/Images/Sprites/BagRefill/"..p)
        end
        
        --Last frame is the middle frame.
        TA_SetProperty("Move Frame", i, 3, "Root/Images/Sprites/BagRefill/"..1)
        TA_SetProperty("Run Frame",  i, 3, "Root/Images/Sprites/BagRefill/"..1)
    end
    return
end

-- |[Item Nodes]|
--Provides items when activated.
if(sGraphicalName == "AdamantiteNode") then
    
    --Six frames, auto-animates.
    TA_SetProperty("Auto Animates Fast", true)
    TA_SetProperty("Move Frames Total", 6)
    
    --Set frames.
    for i = 0, ciDirectionsTotal-1, 1 do
        for p = 0, 6-1, 1 do
            TA_SetProperty("Move Frame", i, p, "Root/Images/Sprites/AdamantiteNode/" .. p)
            TA_SetProperty("Run Frame",  i, p, "Root/Images/Sprites/AdamantiteNode/" .. p)
        end
    end
    return
end

-- |[Block]|
--Puzzle blocks. Activates some special flags.
if(sGraphicalName == "Block") then
    
    --Flags.
    TA_SetProperty("Render As Single Block")
    TA_SetProperty("Whole Collision")
    TA_SetProperty("No Automatic Shadow", true)
    TA_SetProperty("Activate By Rub")

    --All images are the block.
    for i = 0, ciDirectionsTotal-1, 1 do
        for p = 0, ciFramesTotal-1, 1 do
            TA_SetProperty("Move Frame", i, p, "Root/Images/Sprites/Objects/Block")
            TA_SetProperty("Run Frame",  i, p, "Root/Images/Sprites/Objects/Block")
        end
    end
    return
end

-- |[FloorSwitch]|
--Used for puzzles, again.
if(sGraphicalName == "FloorSwitch") then

    --Flags.
    TA_SetProperty("Clipping Flag", false)
    TA_SetProperty("Render As Single Block")
    TA_SetProperty("No Automatic Shadow", true)

    --All all directions to the down image.
    for i = 0, ciDirectionsTotal-1, 1 do
        for p = 0, ciFramesTotal-1, 1 do
            TA_SetProperty("Move Frame", i, p, "Root/Images/Sprites/Objects/FloorSwitchDn")
            TA_SetProperty("Run Frame",  i, p, "Root/Images/Sprites/Objects/FloorSwitchDn")
        end
    end
    
    --Override south to be up.
    for p = 0, ciFramesTotal-1, 1 do
        TA_SetProperty("Move Frame", gci_Face_South, p, "Root/Images/Sprites/Objects/FloorSwitchUp")
        TA_SetProperty("Run Frame",  gci_Face_South, p, "Root/Images/Sprites/Objects/FloorSwitchUp")
    end
    return
end

-- |[Goat]|
if(sGraphicalName == "Goat") then
    for i = 1, 8, 1 do
        for p = 1, 4, 2 do
            TA_SetProperty("Move Frame", i-1, p-1, "Root/Images/Sprites/Goat/1")
            TA_SetProperty("Run Frame",  i-1, p-1, "Root/Images/Sprites/Goat/1")
        end
        for p = 2, 4, 2 do
            TA_SetProperty("Move Frame", i-1, p-1, "Root/Images/Sprites/Goat/2")
            TA_SetProperty("Run Frame",  i-1, p-1, "Root/Images/Sprites/Goat/2")
        end
    end
    return
end

-- |[GolemFancy]|
--The GolemFancy set don't have movement sprites, they are always stationary.
if(string.sub(sGraphicalName, 1, 10) == "GolemFancy") then
    
    --Set directions.
    local saDirections = {"North", "East", "East", "East", "South", "West", "West", "West"}
    
    --Apply the idle frame to all.
    for i = 0, ciDirectionsTotal-1, 1 do
        for p = 0, ciFramesTotal-1, 1 do
            TA_SetProperty("Move Frame", i, p, "Root/Images/Sprites/" .. sGraphicalName .. "/" .. saDirections[i+1] .. "|0")
            TA_SetProperty("Run Frame",  i, p, "Root/Images/Sprites/" .. sGraphicalName .. "/" .. saDirections[i+1] .. "|0")
        end
    end
    return
end

-- |[ =================================== Direction Handler ==================================== ]|
-- |[Direction Array]|
--This array is used to figure out which image is associated with a direction.
local saDirections = {"North", "NE", "East", "SE", "South", "SW", "West", "NW"}

--Eight-direction entities use the full sheet. Don't edit the saDirections array.
if(iDirections == 8) then
    
--Four-direction, all diagonals are replaced with the West or East of that direction.
elseif(iDirections == 4) then
    saDirections = {"North", "East", "East", "East", "South", "West", "West", "West"}
    
--Two-direction entities merge all East and West frames together. North goes to East, south goes to West.
else
    saDirections = {"East", "East", "East", "East", "West", "West", "West", "West"}
end

-- |[Apply]|
--Set the directions by their names.
for i = 0, ciDirectionsTotal-1, 1 do
    for p = 0, ciFramesTotal-1, 1 do

        --Move frames.
        TA_SetProperty("Move Frame", i, p, "Root/Images/Sprites/" .. sGraphicalName .. "/" .. saDirections[i+1] .. "|" .. p)
        
        --If this entity does not have unique run frames, then the run is the same as the move.
        if(iHasRunFrames == 0.0) then
            TA_SetProperty("Run Frame",  i, p, "Root/Images/Sprites/" .. sGraphicalName .. "/" .. saDirections[i+1] .. "|" .. p)
        
        --Otherwise, use the run frames.
        else
            TA_SetProperty("Run Frame",  i, p, "Root/Images/Sprites/" .. sGraphicalName .. "/" .. saDirections[i+1] .. "|Run" .. p)
        end
    end
end

-- |[ ================================== Special Flag Setting ================================== ]|
--Certain sprites are meant to have special flags set, while using the normal sheet layout.

--Scraprats are at double resolution and have a unique idle frame.
if(sGraphicalName == "Scraprat" or sGraphicalName == "ScrapratParagon") then
    TA_SetProperty("Special Idle")
    TA_SetProperty("Tiny", true)
end

--Void Rifts use a unique animation pattern.
if(sGraphicalName == "VoidRift" or sGraphicalName == "VoidRiftParagon") then
    TA_SetProperty("Void Rift")
end

--Beegirls fly and auto-animate.
if(sGraphicalName == "BeeGirl" or sGraphicalName == "BeeGirlParagon" or sGraphicalName == "XannaBee") then
    TA_SetProperty("Flying", true)
    TA_SetProperty("Auto Animates", true)
    TA_SetProperty("Y Oscillates", true)
end

--Bee Buddies also fly and auto-animate.
if(sGraphicalName == "BeeBuddy" or sGraphicalName == "BeeBuddyParagon") then
    TA_SetProperty("Flying", true)
    TA_SetProperty("Auto Animates", true)
    TA_SetProperty("Y Oscillates", true)
end

--BATS! Fly and auto-animate.
if(sGraphicalName == "Bat" or sGraphicalName == "BatParagon") then
    TA_SetProperty("Flying", true)
    TA_SetProperty("Auto Animates", true)
    TA_SetProperty("Y Oscillates", true)
end

--Butterbombers fly and auto-animate.
if(sGraphicalName == "Butterbomber" or sGraphicalName == "ButterbomberParagon") then
    TA_SetProperty("Flying", true)
    TA_SetProperty("Auto Animates", true)
    TA_SetProperty("Y Oscillates", true)
end

-- |[ ===================================== Special Frames ===================================== ]|
--By default, a "Wounded" is needed for most entities, particularly enemies. The game will not print
-- a warning if the wounded frame is not found.
TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/" .. sGraphicalName .. "|Wounded")

--Water-over is the overlay used when this entity is partially submerged in water.
TA_SetProperty("Add Special Frame", "WaterOver", "Root/Images/Sprites/Shadows/Depth")
