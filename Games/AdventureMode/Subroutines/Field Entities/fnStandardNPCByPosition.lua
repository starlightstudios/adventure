-- |[Function: fnStandardNPCByPosition]|
--Standard NPC which the player can talk to. The only needed parameter is the name. The NPC must be stored as 
-- a Position object in the Tiled Map in question. The NPC will be created with the same name as the position pack.
--Built from: ./ZLaunch.lua
--Example: fnStandardNPCByPosition(sName)
function fnStandardNPCByPosition(sName)

	-- |[Arg check]|
	if(sName == nil) then return end
    
    -- |[Execution]|
	TA_CreateUsingPosition(sName, sName)
		TA_SetProperty("Shadow", gsStandardShadow)
	DL_PopActiveObject()

end
