-- |[ =================================== fnAddPartyMember() =================================== ]|
--Adds a party member. This adds the field entity and the combat entity, if both exist. If one does
-- not exist (such as Sophie, who is not a combat character) then it is not added.
--Some characters also have a following variable to indicate they are following. That will also
-- be updated if present.
function fnAddPartyMember(psPartyMemberName, pbMakeLeader)
	
	-- |[Arg check]|
	if(psPartyMemberName == nil) then return end
    if(pbMakeLeader      == nil) then pbMakeLeader = false end
    
    -- |[Null Check]|
    --If the name is "Null", do nothing.
    if(psPartyMemberName == "Null") then return end
    
    -- |[Lookup Table]|
    --Some characters use slightly different names for field vs combat.
    local sCombatName = psPartyMemberName
    local sFieldName = psPartyMemberName
    local sVariableName = "Null"
    
    --55, has a variable.
    if(psPartyMemberName == "Tiffany") then
        sVariableName = "Root/Variables/Chapter5/Scenes/iIs55Following"
    
    --Sophie, is not a combat character.
    elseif(psPartyMemberName == "Sophie") then
        sCombatName = "Null"
    
    --JX-101. Has a variable.
    elseif(psPartyMemberName == "JX-101") then
        sVariableName = "Root/Variables/Chapter5/Scenes/iIsJX101Following"
        
    --One-off characters, no combat appearance.
    elseif(psPartyMemberName == "Rubberraune" or psPartyMemberName == "Rubberbee") then
        sCombatName = "Null"
    end
    
	-- |[Field Presence]|
	--If the field name is not null, the character is added as a follower.
	if(sFieldName ~= "Null") then
		
        --Character is meant to assume leadership:
        if(pbMakeLeader == true) then
                
            --If the character does not exist, create them.
            if(EM_Exists(sFieldName) == false) then
                fnSpecialCharacter(sFieldName, -100, -100, gci_Face_South, false, nil)
            end

            --Get the characters's uniqueID. Also turn off collision and activation. 
            EM_PushEntity(sFieldName)
                local iCharacterID = RE_GetID()
                TA_SetProperty("Clipping Flag", false)
                TA_SetProperty("Activation Script", "Null")
            DL_PopActiveObject()
            
            --Become leader.
            AL_SetProperty("Player Actor ID", iCharacterID)
        
        --Character is a follower:
        else
        
            --Scan to see if the character is already following.
            local bAlreadyFollowing = false
            for i = 1, giFollowersTotal, 1 do
                if(gsaFollowerNames[i] == sFieldName) then
                    bAlreadyFollowing = true
                    break
                end
            end
		
            --If not following, add them.
            if(bAlreadyFollowing == false) then
                
                --If the character does not exist, create them.
                if(EM_Exists(sFieldName) == false) then
                    fnSpecialCharacter(sFieldName, -100, -100, gci_Face_South, false, nil)
                end

                --Get the characters's uniqueID. Also turn off collision and activation. 
                EM_PushEntity(sFieldName)
                    local iCharacterID = RE_GetID()
                    TA_SetProperty("Clipping Flag", false)
                    TA_SetProperty("Activation Script", "Null")
                DL_PopActiveObject()

                -- |[Lua Globals]|
                --Create a new table with this character in it.
                local tFollowerNames = {sFieldName}
                local tFollowerIDs   = {iCharacterID}

                --Append the tables together.
                giFollowersTotal = giFollowersTotal + 1
                fnAppendTables(gsaFollowerNames, tFollowerNames)
                fnAppendTables(giaFollowerIDs,   tFollowerIDs)
                AL_SetProperty("Follow Actor ID", iCharacterID)
            end
		end
	end
	
    -- |[Combat Character]|
	--Combat name is not "Null", add this character to the combat lineup.
	if(sCombatName ~= "Null") then
        
        --Check if the character is already in the active party.
        if(AdvCombat_GetProperty("Is Member In Active Party", sCombatName) == true) then
            --Already in party.
            
        --Not in the active party.
        else
            
            --Find the next empty slot for this character.
            local bFoundEmptySlot = false
            for i = 0, gciCombat_MaxActivePartySize-1, 1 do 
                local sNameInSlot = AdvCombat_GetProperty("Name of Active Member", i)
                if(sNameInSlot == "Null") then
                    bFoundEmptySlot = true
                    AdvCombat_SetProperty("Party Slot", i, sCombatName)
                    break
                end
            end
            
            --No space!
            if(bFoundEmptySlot == false) then
                io.write("fnAddPartyMember() Error: No space for member " .. sCombatName .. "\n")
            end
        end
		
	end
	
    -- |[Variable]|
	--Join/Leave variable. Some characters manage joining the party with script variables.
	if(sVariableName ~= "Null") then
		VM_SetVar(sVariableName, "N", 1.0)
	end
end

--No-Combat version, only modifies field sprites and variables. This is used exclusively by the load handler
-- as sometimes a character may be in the party but not marked as a follower.
