-- |[ ================================= fnIsCharacterPresent() ================================= ]|
--Returns true if the named character is in the party, false if not. Note that this uses the world-name, not the party name,
-- and is checking for the world sprite.
--Example: fnIsCharacterPresent("Florentina")
function fnIsCharacterPresent(sName)
	
	-- |[Arg check]|
	if(sName == nil) then return false end
	
    -- |[Follower Matches]|
	--Check for matches among the party follower listing.
	for i = 1, giFollowersTotal, 1 do
		if(gsaFollowerNames[i] == sName) then
			return true
		end
	end
	
	--All checks failed, character is not present.
	return false
end

--Returns if the character exists in the world at all.
function fnCharacterExists(sName)
    return EM_Exists(sName)
end
