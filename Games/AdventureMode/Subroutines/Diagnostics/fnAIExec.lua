-- |[ ================================ gzDiagnostics.fnAIExec() ================================ ]|
--Prints a given string if the AI diagnostic flag requires it.
function gzDiagnostics.fnAIExec(piCheckFlag, pString)
    
    -- |[Argument Check]|
    if(piCheckFlag == nil) then return end
    if(pString     == nil) then return end
    
    -- |[Flag Not Enabled]|
    local iUseFlag = giAI_Diagnostic_Level
    if((iUseFlag & piCheckFlag) == 0) then return end
    
    -- |[Print]|
    io.write(pString)
    
end
