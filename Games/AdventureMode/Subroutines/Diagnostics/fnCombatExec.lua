-- |[ ============================== gzDiagnostics.fnCombatExec() ============================== ]|
--Prints a given string if the combat diagnostic flag requires it.
function gzDiagnostics.fnCombatExec(piCheckFlag, pString)
    
    -- |[Argument Check]|
    if(piCheckFlag == nil) then return end
    if(pString     == nil) then return end
    
    -- |[Flag Not Enabled]|
    local iUseFlag = giCombatExec_Diagnostic_Level
    if((iUseFlag & piCheckFlag) == 0) then return end
    
    -- |[Print]|
    io.write(pString)
    
end
