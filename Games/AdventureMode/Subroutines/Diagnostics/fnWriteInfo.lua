-- |[ ====================================== fnWriteInfo() ===================================== ]|
--Function used when writing information for translations to an outfile which can then be put into
-- a spreadsheet for translation.
--The function is predicated on the existence of gfOutfile, the receiving file, and if that doesn't
-- exist it does nothing. Therefore the caller can toggle it on and off by just not creating the file.
function fnWriteInfo(psString)
    
    -- |[Flag Check]|
    if(gfOutfile == nil) then return end
    
    -- |[Argument Check]|
    if(psString == nil) then return end
    
    -- |[Write]|
    gfOutfile:write("\"".. psString .. "\",,,\n")

end
