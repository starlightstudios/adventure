-- |[ =================================== fnDiagnosticFlag() =================================== ]|
--Makes diagnostic flags into human-readable strings. Returns true if the given psCompare flag is
-- active in the given psFlag, false if not.
function fnDiagnosticFlag(psFlag, psCompare)
    
    -- |[Argument Check]|
    if(psFlag    == nil) then return end
    if(psCompare == nil) then return end
    
    -- |[Setup]|
    local iDiagnosticFlag = 0
    local iCompareValue = 0
    
    -- |[Autoloader Diagnostics]|
    if(psFlag == "Autoloader") then
    
        --Set flag.
        iDiagnosticFlag = gciAutoloader_Diagnostic_Level
    
        --Names.
        if(psCompare == "Report Names") then
            iCompareValue = gciAutoloader_Diagnostic_ReportNames
        elseif(psCompare == "Report Invalid Groups") then
            iCompareValue = gciAutoloader_Diagnostic_ReportInvalidGroup
        elseif(psCompare == "Report Entries") then
            iCompareValue = gciAutoloader_Diagnostic_ReportEntries
            
        --Unhandled.
        else
            io.write("Warning: No diagnostic flag for " .. psFlag .. " named " .. psCompare .. "\n")
            return false
        end
        
    -- |[Unhandled]|
    else
        io.write("Warning: No diagnostic flag " .. psFlag .. "\n")
        return false
    end
    
    -- |[Finish Up]|
    --Normal case.
    return fnIsFlag(iDiagnosticFlag, iCompareValue)
end
