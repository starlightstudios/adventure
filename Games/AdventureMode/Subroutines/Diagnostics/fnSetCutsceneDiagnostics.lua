-- |[ =============================== fnSetCutsceneDiagnostics() =============================== ]|
--Same as the base version, but changes the function calls for AdventureMode to use more explicit
-- logging. This is called by ZLaunch.lua
function fnSetCutsceneDiagnostics(piLevel)
    
    -- |[Argument Check]|
    if(piLevel == nil) then return end
    
    -- |[Set]|
    Cutscene_SetProperty("Diagnostic Level", piLevel)
    gi_Diagnostic_Cutscene_Flag = piLevel
    
    -- |[Call Functions]|
    --No diagnostics.
    if(gi_Diagnostic_Cutscene_Flag == gci_Diagnostic_Cutscene_None) then
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/fnAutoFoldParty.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/fnCrawlThroughVent.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/fnCutsceneBlocker.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/fnCutsceneCall.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/fnCutsceneCamera.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/fnCutsceneDoNothing.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/fnCutsceneFace.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/fnCutsceneFaceTarget.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/fnCutscene.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/fnCutsceneJumpNPC.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/fnCutsceneJumpTo.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/fnCutsceneLayerDisabled.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/fnCutsceneMergeParty.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/fnCutsceneMove.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/fnCutsceneMoveAmount.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/fnCutsceneMoveFace.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/fnCutsceneSetFrame.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/fnCutsceneSetHeight.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/fnCutsceneTeleport.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/fnCutsceneWait.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/fnCutscenePlaySound.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/fnGetEntityPosition.lua")
    
    --With diagnostics:
    else
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/Diagnostic/fnAutoFoldParty.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/Diagnostic/fnCrawlThroughVent.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/Diagnostic/fnCutsceneBlocker.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/Diagnostic/fnCutsceneCall.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/Diagnostic/fnCutsceneCamera.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/Diagnostic/fnCutsceneDoNothing.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/Diagnostic/fnCutsceneFace.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/Diagnostic/fnCutsceneFaceTarget.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/Diagnostic/fnCutscene.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/Diagnostic/fnCutsceneJumpNPC.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/Diagnostic/fnCutsceneJumpTo.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/Diagnostic/fnCutsceneLayerDisabled.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/Diagnostic/fnCutsceneMergeParty.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/Diagnostic/fnCutsceneMove.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/Diagnostic/fnCutsceneMoveAmount.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/Diagnostic/fnCutsceneMoveFace.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/Diagnostic/fnCutsceneSetFrame.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/Diagnostic/fnCutsceneSetHeight.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/Diagnostic/fnCutsceneTeleport.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/Diagnostic/fnCutsceneWait.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/Diagnostic/fnCutscenePlaySound.lua")
        LM_ExecuteScript(gsRoot .. "Subroutines/Cutscenes/Diagnostic/fnGetEntityPosition.lua")
    end
end