-- |[ ============================= fnGetFirstObjectFromPosition() ============================= ]|
--Given a position and facing (typically taken from the party leader), and a list of valid objects,
-- checks to see if any of the valid objects are right in front of the position. This is used like
-- an activation check for field abilities, such as picking whatever lock is in front of the party.
function fnGetFirstObjectFromPosition(piPositionX, piPositionY, piFacing, psaValidHitNames)
    
    -- |[Argument Check]|
    if(piPositionX      == nil) then return end
    if(piPositionY      == nil) then return end
    if(piFacing         == nil) then return end
    if(psaValidHitNames == nil) then return end
    
    -- |[Constants]|
    --List of locations to check. These are in an X arrangement around the center
    -- point, to allow some overlap in case of narrow miss.
    local iaOffX = {0, -4, 4, -4, 4, 12, 12, -12, -12, -4,  4,  -4,   4}
    local iaOffY = {0, -4, -4, 4, 4, -4,  4,  -4,   4, 12, 12, -12, -12}
    
    -- |[Modify Position]|
    --Turn the facing value into radians and then advance the position ahead of the character.
    -- This causes the character to examine a spot 8 pixels in front of it.
    local fRadians = (piFacing-2) * 45 * 3.1415926 / 180.0
    
    --Modify by rotation.
    piPositionX = piPositionX + (math.cos(fRadians) * 8.0) - 4
    piPositionY = piPositionY + (math.sin(fRadians) * 8.0) - 8
    
    -- |[Look For Hits]|
    --Iterate across the offsets, but stop when a hit is registered.
    local bGotHit = false
    for i = 1, #iaOffX, 1 do
        
        --Build, get total.
        AL_GetProperty("Build Objects At Position", piPositionX + iaOffX[i], piPositionY + iaOffY[i])
        local iTotalHits = AL_GetProperty("Total Objects At Position")
        
        --Check all hits.
        for p = 0, iTotalHits-1, 1 do
            
            --Get variables.
            local iType = AL_GetProperty("Type Of Object At Position", p)
            local sName = AL_GetProperty("Name Of Object At Position", p)
            
            --Check if hit any of the valid names. Any hit is good.
            for q = 1, #psaValidHitNames, 1 do
                if(sName == psaValidHitNames[q]) then
                    return psaValidHitNames[q]
                end
            end
        end
    end
    
    -- |[No Results]|
    --Return "Null" to indicate nothing was found.
    return "Null"
    
end