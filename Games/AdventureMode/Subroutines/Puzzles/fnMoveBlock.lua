-- |[ ======================================= fnMoveBlock ====================================== ]|
--During puzzle segments, moves a block when the player pushes it. The push direction is based on
-- the player's position relative to the block.
--Returns the final coordinates of the block after it has moved. Returns -1,-1 if the block did
-- not move.
--Built from: ./ZLaunch.lua
--Example: local iFinalX, iFinalY = fnMoveBlock("BlockA", {"BlockA", "BlockB"}, 1)
function fnMoveBlock(psBlockName, psaBlockList, piCollisionDepth)
    
    -- |[Arg Check]|
    if(psBlockName      == nil) then return -1, -1 end
    if(psaBlockList     == nil) then return -1, -1 end
    if(piCollisionDepth == nil) then return -1, -1 end
        
    -- |[Get Positions]|
    --Get the block's position.
    EM_PushEntity(psBlockName)
        local fBlockOrigX, fBlockOrigY = TA_GetProperty("Position")
    DL_PopActiveObject()
    
    --Get the player's position.
    EM_PushEntity("By ID", giPartyLeaderID)
        local fPlayerOrigX, fPlayerOrigY = TA_GetProperty("Position")
        local iPlayerFacing = TA_GetProperty("Facing")
    DL_PopActiveObject()
    
    --Turn them into tile coordinates.
    local fBlockX = math.floor(fBlockOrigX / 16.0)
    local fBlockY = math.floor(fBlockOrigY / 16.0)
    local fPlayerX = math.floor(fPlayerOrigX / 16.0)
    local fPlayerY = math.floor(fPlayerOrigY / 16.0)
        
    -- |[Resolve Push Direction]|
    --Determine the X/Y change based on where the player is.
    local iDirectionX = 0
    local iDirectionY = 0
    if(iPlayerFacing == gci_Face_North) then
        iDirectionY = -1
    elseif(iPlayerFacing == gci_Face_East) then
        iDirectionX = 1
    elseif(iPlayerFacing == gci_Face_South) then
        iDirectionY = 1
    elseif(iPlayerFacing == gci_Face_West) then
        iDirectionX = -1
    else
        return -1, -1
    end
    
    --Now check how the player and tile are positioned by angle.
    local fAngleTo = math.atan2(fBlockY - fPlayerY, fBlockX - fPlayerX) * 360 / 3.1415926 / 2
    
    --Due to floating point errors, we put some ranges here.
    local iExpectedX = 0
    local iExpectedY = 0
    if(fAngleTo >= -91.0 and fAngleTo <= -89.0) then
        iExpectedY = -1
    elseif(fAngleTo >= -1.0 and fAngleTo <= 1.0) then
        iExpectedX = 1
    elseif(fAngleTo >= 89.0 and fAngleTo <= 91.0) then
        iExpectedY = 1
    elseif(fAngleTo >= 179.0 and fAngleTo <= 181.0) then
        iExpectedX = -1
    end
    
    --Debug.
    --io.write("Angle is: " .. fAngleTo .. "\n")
    --io.write("Expected move: " .. iExpectedX .. "x" .. iExpectedY .. "\n")
    
    --If the two directions disagree, disallow the move.
    if(iDirectionX ~= iExpectedX and iDirectionY ~= iExpectedY) then
        return -1, -1
    end
    
    --If the expected is zero because this is a diagonal move, fail.
    if(iExpectedX == 0 and iExpectedY == 0) then
        return -1, -1
    end
    
    --Debug.
    --io.write("Leader ID: " .. giPartyLeaderID .. "\n")
    --io.write("Block position: " .. fBlockX .. "x" .. fBlockY .. "\n")
    --io.write("Player position: " .. fPlayerX .. "x" .. fPlayerY .. "\n")
    --io.write("Direction: " .. iDirectionX .. "x" .. iDirectionY .. "\n")
    
    -- |[Collision Check]|
    --Check if the block can move to that location. First, check if it hits a collision:
    local iDestX = fBlockX + iDirectionX
    local iDestY = fBlockY + iDirectionY
    local iPositionResult = AL_GetProperty("Collision", iDestX, iDestY, piCollisionDepth)
    if(iPositionResult > 0) then
        --io.write("Block cannot move, hits collision.\n")
        return -1, -1
    end
    
    -- |[Block On Block Check]|
    --Check if the block hits another block.
    for i = 1, #psaBlockList, 1 do
    
        --Skip if it's the current block:
        if(psaBlockList[i] == psBlockName) then
    
        --Different block:
        else
        
            --Get the position.
            EM_PushEntity(psaBlockList[i])
                local fOtherBlockX, fOtherBlockY = TA_GetProperty("Position")
            DL_PopActiveObject()
            
            --Turn it into a tile coordinate.
            fOtherBlockX = math.floor(fOtherBlockX / 16.0)
            fOtherBlockY = math.floor(fOtherBlockY / 16.0)
            
            --Hit!
            if(iDestX == fOtherBlockX and iDestY == fOtherBlockY) then
                --io.write("Block cannot move, hits other block.\n")
                return -1, -1
            end
        end
    end
    
    -- |[All Checks Passed, Move]|
    --In order to move at less than full speed, we need to compute the final position ahead of time.
    fBlockOrigX  = fBlockOrigX  / gciSizePerTile
    fBlockOrigY  = fBlockOrigY  / gciSizePerTile
    fPlayerOrigX = fPlayerOrigX / gciSizePerTile
    fPlayerOrigY = fPlayerOrigY / gciSizePerTile
    
    --Resolve which kick frame to use.
    local sKickFrame = "KickN"
    if(iPlayerFacing == gci_Face_North) then
        sKickFrame = "KickN"
    elseif(iPlayerFacing == gci_Face_East) then
        sKickFrame = "KickE"
    elseif(iPlayerFacing == gci_Face_South) then
        sKickFrame = "KickS"
    elseif(iPlayerFacing == gci_Face_West) then
        sKickFrame = "KickW"
    end
    
    --Wind up. Character switches to kick frame.
    fnCutsceneSetFrame(gsPartyLeaderName, sKickFrame .. "0")
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    
    --Kick the block, starting its movement.
    fnCutsceneSetFrame(gsPartyLeaderName, sKickFrame .. "1")
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutscene([[ AudioManager_PlaySound("World|BlockSlide") ]])
    
    --Create a cutscene ordering the block to move.
    local iMax = math.floor(gciSizePerTile * 0.45)
    for i = 1, iMax, 1 do
        local fPercent = i / iMax
        fnCutsceneTeleport(psBlockName, fBlockOrigX  + (iDirectionX * fPercent), fBlockOrigY  + (iDirectionY * fPercent))
        fnCutsceneBlocker()
        if(fPercent >= 0.90) then
            fnCutsceneSetFrame(gsPartyLeaderName, "Null")
        end
    end
    AL_SetProperty("Disable Borders For This Cutscene")
    fnCutsceneBlocker()
    
    --Pass back the final position.
    return iDestX, iDestY
end