-- |[ =================================== fnExecModScript() ==================================== ]|
--Calls all mod scripts given a file pattern. A global variable, gbStopModExec, can be flipped
-- by the calling script to make the loop stop iterating.
--The flag gbModExecutedAnything is set to true if the function executed any file.
--Returns the global variable gbModHandledCall, a boolean variable that scripts can optionally
-- trip to indicate they handled a call (such as when creating items). Note that stopping execution
-- and handling execution are distinct.
--Can optionally handle up to three arguments, all strings. This is handled manually.
function fnExecModScript(psPathAppend, psArg0, psArg1, psArg2)

    -- |[Argument Check]|
    if(psPathAppend == nil) then return false end
    
    -- |[Reset Flags]|
    gbModExecutedAnything = false
    gbStopModExec = false
    gbModHandledCall = false
    
    -- |[Execution]|
    --Scan the mods and see if any of them reply.
    for i = 1, #gsModDirectories, 1 do

        --Path.
        local sCheckPath = gsModDirectories[i] .. psPathAppend
        
        --If it exists, run it.
        if(FS_Exists(sCheckPath) == true) then
            
            --Varies based on provided arguments.
            if(psArg0 == nil) then
                LM_ExecuteScript(sCheckPath)
            elseif(psArg1 == nil) then
                LM_ExecuteScript(sCheckPath, psArg0)
            elseif(psArg2 == nil) then
                LM_ExecuteScript(sCheckPath, psArg0, psArg1)
            elseif(psArg2 ~= nil) then
                LM_ExecuteScript(sCheckPath, psArg0, psArg1, psArg2)
            end
            
            --Set this flag.
            gbModExecutedAnything = true
        end
        
        --If this flag got tripped, stop execution.
        if(gbStopModExec) then
            return gbModHandledCall
        end
    end

    -- |[Finish Up]|
    --Pass back if we handled the call.
    return gbModHandledCall
end
