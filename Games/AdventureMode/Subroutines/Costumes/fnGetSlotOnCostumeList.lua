-- |[ ================================ fnGetSlotOnCostumeList() ================================ ]|
--Given a character name, such as "Mei" or "Florentina", returns which slot they are in on the
-- global list gzCostumeResolveList. Returns -1 if the entry is not found.
function fnGetSlotOnCostumeList(psName)
    
    -- |[Argument Check]|
    if(psName == nil) then return -1 end
    
    -- |[List Check]|
    --If the list was not initialized yet, fail.
    if(gzCostumeResolveList == nil) then return -1 end
    
    -- |[Scan]|
    for i = 1, #gzCostumeResolveList, 1 do
        if(gzCostumeResolveList[i].sCharacterName == psName) then
            return i
        end
    end
    
    -- |[Not Found]|
    return -1
end
