-- |[ ================================= fnGetFormFromCostume() ================================= ]|
--Given a slot in the global list gzCostumeResolveList, resolve the current job of the character,
-- then return the form that matches the job. Returns "Fail" on error or if the job doesn't have
-- a matching form for any reason.
function fnGetFormFromCostume(piSlot)
    
    -- |[Argument Check]|
    if(piSlot == nil) then return "Fail" end
    
    -- |[List Check]|
    --If the list was not initialized yet, fail.
    if(gzCostumeResolveList == nil) then return "Fail" end
    
    -- |[Setup]|
    --The character's name is in the list.
    local sCharacterName = gzCostumeResolveList[piSlot].sCharacterName
    
    --Get the character's job name.
    AdvCombat_SetProperty("Push Party Member", sCharacterName)
        local sCurrentJob = AdvCombatEntity_GetProperty("Current Job")
    DL_PopActiveObject()
    
    -- |[Scan]|
    --Scan the job listing.
    for i = 1, #gzCostumeResolveList[piSlot].saJobListing, 1 do
        if(gzCostumeResolveList[piSlot].saJobListing[i] == sCurrentJob) then
            
            --If the matching form listing doesn't exist, the list was malformed.
            if(gzCostumeResolveList[piSlot].saFormListing[i] == nil) then
                io.write("fnGetFormFromCostume(): Warning, malformed listing for character " .. sCharacterName .. ". Got a nil form list entry.\n")
                return "Fail"
            end
            
            --Otherwise, return the listing.
            return gzCostumeResolveList[piSlot].saFormListing[i]
        end
    end
    
    -- |[Fail]|
    --Entry not found, the job was not on the list.
    return "Fail"
end