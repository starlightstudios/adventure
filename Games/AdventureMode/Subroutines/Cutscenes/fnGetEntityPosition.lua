-- |[ ================================= fnGetEntityPosition() ================================== ]|
--Returns the X and Y coordinates of the named entity. Returns -100, -100 if the entity was not found.
-- The entity will have its position returned in terms of tiles, so 10.25 instead of 164.0.
function fnGetEntityPosition(psEntityName)

	-- |[Argument Check]|
	if(psEntityName == nil) then
        return -100, -100
    end
	
    -- |[Existence Check]|
    --Check if the entity exists. If not, return defaults.
    if(EM_Exists(psEntityName) == false) then
        return -100, -100
    end

    -- |[Execution]|
    --Get entity's position.
    EM_PushEntity(psEntityName)
        local fEntityX, fEntityY = TA_GetProperty("Position")
    DL_PopActiveObject()
    
    --Convert to tile.
    fEntityX = fEntityX / gciSizePerTile
    fEntityY = fEntityY / gciSizePerTile
		
	return fEntityX, fEntityY
end
