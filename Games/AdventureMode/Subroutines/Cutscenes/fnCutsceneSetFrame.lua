-- |[ ================================== fnCutsceneSetFrame() ================================== ]|
--Creates an event to set a special frame for the given character.
function fnCutsceneSetFrame(psActorName, psSpecialFrameName)

	-- |[Argument Check]|
	if(psActorName        == nil) then return end
	if(psSpecialFrameName == nil) then return end

    -- |[Execution]|
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", psActorName)
		ActorEvent_SetProperty("Special Frame", psSpecialFrameName)
	DL_PopActiveObject()

end
