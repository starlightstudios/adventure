-- |[ =============================== fnCutsceneLayerDisabled() ================================ ]|
--Creates and registers a cutscene event that changes visibility on a given tile layer. True means
-- the layer does not appear, false means it does.
function fnCutsceneLayerDisabled(psLayerName, pbIsDisabled)

	-- |[Argument Check]|
	if(psLayerName  == nil) then return end
	if(pbIsDisabled == nil) then return end
	
    -- |[Setup]|
    --Construct the string.
    local sInstruction = "AL_SetProperty(\"Set Layer Disabled\", \"" .. psLayerName .. "\", "
    if(pbIsDisabled == true) then
        sInstruction = sInstruction .. "true)"
    else
        sInstruction = sInstruction .. "false)"
    end
    
    -- |[Execution]|
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("AutoInstruction")
		REvent_SetProperty("InstructionExec", sInstruction)
	DL_PopActiveObject()

end
