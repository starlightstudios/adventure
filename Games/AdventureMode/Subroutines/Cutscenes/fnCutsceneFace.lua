-- |[ ==================================== fnCutsceneFace() ==================================== ]|
--Creates and registers a cutscene change-facing event. This is an ActorEvent. Facing is done by "Speeds".
-- The facing is resolved by acting as though the entity is moving at the provided speeds, and getting
-- that facing. To face North, pass 0, -1.
function fnCutsceneFace(sActorName, fX, fY)

	-- |[Arg Check]|
	if(sActorName == nil) then return end
	if(fX == nil) then return end
	if(fY == nil) then return end

    -- |[Execution]|
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", sActorName)
		ActorEvent_SetProperty("Face", fX, fY)
	DL_PopActiveObject()

end
