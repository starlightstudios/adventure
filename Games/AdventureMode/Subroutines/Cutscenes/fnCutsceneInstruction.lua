-- |[ ================================ fnCutsceneInstruction() ================================= ]|
--Creates and registers a cutscene event using the provided instruction string.
function fnCutsceneInstruction(psInstruction)

	-- |[Argument Check]|
	if(psInstruction == nil) then return end
	
    -- |[Execution]|
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("AutoInstruction")
		REvent_SetProperty("InstructionExec", psInstruction)
	DL_PopActiveObject()

end

--Alternate spelling:
function fnCutscene(psInstruction)

	-- |[Argument Check]|
	if(psInstruction == nil) then return end
	
    -- |[Execution]|
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("AutoInstruction")
		REvent_SetProperty("InstructionExec", psInstruction)
	DL_PopActiveObject()
end