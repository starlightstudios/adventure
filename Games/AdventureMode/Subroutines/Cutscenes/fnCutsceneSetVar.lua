-- |[ =================================== fnCutsceneSetVar() =================================== ]|
--Calls a VM_SetVar() in cutscene stride.
function fnCutsceneSetVar(psVarPath, psVarType, psValue)

	-- |[Argument Check]|
	if(psVarPath == nil) then return end
	if(psVarType == nil) then return end
	if(psValue   == nil) then return end
    
    -- |[Setup]|
    --Common.
    local sInstruction = "VM_SetVar(\""  .. psVarPath .. "\", \"" .. psVarType .. "\", "
    
    --If this is a numerical type:
    if(psVarPath == "N") then
        sInstruction = sInstruction .. psValue .. ")"
    
    --String type.
    else
        sInstruction = sInstruction .. "\"" .. psValue .. "\")"
    end
    
    -- |[Execution]|
	--Autogenerates the instruction name.
    --io.write("Set variable " .. sInstruction .. "\n")
	Cutscene_CreateEvent("AutoInstruction")
		REvent_SetProperty("InstructionExec", sInstruction)
	DL_PopActiveObject()

end
