-- |[ =================================== fnCutsceneSetAnim() ================================== ]|
--Creates an event to set a special frame animation for the character.
function fnCutsceneSetAnim(psActorName, psSpecialAnimName)

	-- |[Argument Check]|
	if(psActorName       == nil) then return end
	if(psSpecialAnimName == nil) then return end

    -- |[Execution]|
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", psActorName)
		ActorEvent_SetProperty("Special Anim", psSpecialAnimName)
	DL_PopActiveObject()

end
