-- |[ ================================== fnCutsceneMoveList() ================================== ]|
--Same as fnCutsceneMove() but allows many instances to be populated at once for the same actor. This
-- is often used when an actor moves between multiple points in a single action.
--The format is specific, but has many optional entries. A single large table is expected after the name.
--If a single actor is moving multiple times and stopping, this function can also be used, but if multiple actors
-- are doing this the stops would interfere so a conventional Move/Break/Wait/Break will need to be used.

-- |[Examples]|
--fnCutsceneMoveList("Florentina", { {fX, fY, fSpeed, iStopFacingX, iStopFacingY, iWaitTicks}, {EntryB}, {EntryC}, ..., {EntryX} })

-- |[Entry Format]|
--fX: The X position to move to.
--fY: The Y position to move to.
--fSpeed: How fast to move. Default is 1.0. Optional.
--iStopFacingX: When the entity stops an fnCutsceneFace() is used, this is the X coordinate. Optional. Pass -2 to bypass.
--iStopFacingY: When the entity stops an fnCutsceneFace() is used, this is the Y coordinate. Optional. Pass -2 to bypass.
--iWaitTicks: When the entity stops, if this value is nonzero then a wait action is queued. Optional.

-- |[Function]|
function fnCutsceneMoveList(psActorName, pzaTable)

	-- |[Argument Check]|
	if(psActorName == nil) then return end
	if(pzaTable    == nil) then return end

    -- |[Execution]|
    --Iterate across the entries.
    local i = 1
    while(pzaTable[i] ~= nil) do
        
        --Fast-access pointers.
        local zEntry = pzaTable[i]
        
        --Error check the entry. The X/Y are mandatory, everything else is optional.
        local fX = zEntry[1]
        local fY = zEntry[2]
        if(fX == nil or fY == nil) then
            return
        end
        
        --Resolve all the optional entries.
        local fSpeed = zEntry[3]
        local iStopFacingX = zEntry[4]
        local iStopFacingY = zEntry[5]
        local iWaitTicks = zEntry[6]
        
        --Only movement function:
        if(fSpeed == nil) then
            fnCutsceneMove(psActorName, fX, fY)

        --Movement with speed:
        elseif(iStopFacingX == nil) then
            fnCutsceneMove(psActorName, fX, fY, fSpeed)
        
        --Movement with a stop facing:
        elseif(iWaitTicks == nil) then
            fnCutsceneMove(psActorName, fX, fY, fSpeed)
            fnCutsceneFace(psActorName, iStopFacingX, iStopFacingY)
        
        --Movement with a wait sequence. Facing change is optional.
        else
            fnCutsceneMove(psActorName, fX, fY, fSpeed)
            if(iStopFacingX ~= -2 and iStopFacingY ~= 2) then
                fnCutsceneFace(psActorName, iStopFacingX, iStopFacingY)
            end
            fnCutsceneBlocker()
            fnCutsceneWait(iWaitTicks)
            fnCutsceneBlocker()
        
        end
    
        --Next.
        i = i + 1
    end

end
