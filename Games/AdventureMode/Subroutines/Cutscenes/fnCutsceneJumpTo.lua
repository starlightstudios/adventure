-- |[ =================================== fnCutsceneJumpTo() =================================== ]|
--Orders the named NPC to "jump" to a given position. This is actually a series of teleports and ignores collisions.
-- The position is in tile coordinates.
function fnCutsceneJumpTo(psActorName, pfX, pfY, piTicks)

	-- |[Argument Check]|
	if(psActorName == nil) then return end
	if(pfX         == nil) then return end
	if(pfY         == nil) then return end
	if(piTicks     == nil) then return end

    -- |[Execution]|
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", psActorName)
			ActorEvent_SetProperty("Jump To", pfX * gciSizePerTile, pfY * gciSizePerTile, piTicks)
	DL_PopActiveObject()

end
