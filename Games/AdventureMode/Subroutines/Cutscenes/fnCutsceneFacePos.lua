-- |[ =================================== fnCutsceneFacePos() ================================== ]|
--Creates and registers a cutscene change-facing event. This is an ActorEvent. The NPC will attempt
-- to face the provided position.
--Position is in Tile coordinates. It needs to be translated to world pixel coordinates.
function fnCutsceneFacePos(psActorName, pfXPos, pfYPos)

	-- |[Arg Check]|
	if(psActorName == nil) then return end
	if(pfXPos == nil) then return end
	if(pfYPos == nil) then return end

    -- |[Execution]|
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", psActorName)
		ActorEvent_SetProperty("Face Pos", pfXPos * gciSizePerTile, pfYPos * gciSizePerTile)
	DL_PopActiveObject()

end
