-- |[ ==================================== fnCutsceneWait() ==================================== ]|
--Creates and registers a cutscene event which will wait the provided number of ticks. 60 ticks is 1 second.
function fnCutsceneWait(piTicks)

	-- |[Argument Check]|
	if(piTicks == nil) then return end
    
    -- |[Diagnostics]|
    Debug_ForcePrint("fnCutsceneWait() Called.\n")
    if(gi_Diagnostic_Cutscene_Flag & gci_Diagnostic_Cutscene_Arguments > 0) then
        Debug_ForcePrint(" piTicks: " .. piTicks .. "\n")
    end
    if(gi_Diagnostic_Cutscene_Flag & gci_Diagnostic_Cutscene_Paths > 0) then
        Debug_ForcePrint(" Caller: " .. LM_GetCallStack(0) .. "\n")
    end
	
    -- |[Execution]|
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("WaitTicks", "Timer")
		TimeEvent_SetProperty("Timer", piTicks)
	DL_PopActiveObject()

end
