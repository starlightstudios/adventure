-- |[ ==================================== fnCutsceneFace() ==================================== ]|
--Creates and registers a cutscene change-facing event. This is an ActorEvent. Facing is done by "Speeds".
-- The facing is resolved by acting as though the entity is moving at the provided speeds, and getting
-- that facing. To face North, pass 0, -1.
function fnCutsceneFace(sActorName, fX, fY)

	-- |[Arg Check]|
	if(sActorName == nil) then return end
	if(fX         == nil) then return end
	if(fY         == nil) then return end
    
    -- |[Diagnostics]|
    Debug_ForcePrint("fnCutsceneFace() Called.\n")
    if(gi_Diagnostic_Cutscene_Flag & gci_Diagnostic_Cutscene_Arguments > 0) then
        Debug_ForcePrint(" sActorName: " .. sActorName .. "\n")
        Debug_ForcePrint(" fX: " .. fX .. "\n")
        Debug_ForcePrint(" fY: " .. fY .. "\n")
    end
    if(gi_Diagnostic_Cutscene_Flag & gci_Diagnostic_Cutscene_Paths > 0) then
        Debug_ForcePrint(" Caller: " .. LM_GetCallStack(0) .. "\n")
    end

    -- |[Execution]|
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", sActorName)
		ActorEvent_SetProperty("Face", fX, fY)
	DL_PopActiveObject()

end
