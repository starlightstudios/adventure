-- |[ ================================= fnCutsceneFaceTarget() ================================= ]|
--Creates and registers a cutscene change-facing event. This is an ActorEvent. Facing is done with the name
-- of the entity to face.
function fnCutsceneFaceTarget(psActorName, psActorTarget)

	-- |[Arg Check]|
	if(psActorName   == nil) then return end
	if(psActorTarget == nil) then return end
    
    -- |[Diagnostics]|
    Debug_ForcePrint("fnCutsceneFaceTarget() Called.\n")
    if(gi_Diagnostic_Cutscene_Flag & gci_Diagnostic_Cutscene_Arguments > 0) then
        Debug_ForcePrint(" psActorName: " .. psActorName .. "\n")
        Debug_ForcePrint(" psActorTarget: " .. psActorTarget .. "\n")
    end
    if(gi_Diagnostic_Cutscene_Flag & gci_Diagnostic_Cutscene_Paths > 0) then
        Debug_ForcePrint(" Caller: " .. LM_GetCallStack(0) .. "\n")
    end

    -- |[Execution]|
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", psActorName)
		ActorEvent_SetProperty("Face", psActorTarget)
	DL_PopActiveObject()

end
