-- |[ ================================== fnCutsceneJumpNPC() =================================== ]|
--Creates and registers a cutscene event that causes the named NPC to jump.
function fnCutsceneJumpNPC(psNPCName, pfSpeed)

	-- |[Arg Check]|
	if(psNPCName == nil) then return end
	if(pfSpeed   == nil) then return end
    
    -- |[Diagnostics]|
    Debug_ForcePrint("fnCutsceneJumpNPC() Called.\n")
    if(gi_Diagnostic_Cutscene_Flag & gci_Diagnostic_Cutscene_Arguments > 0) then
        Debug_ForcePrint(" psNPCName: " .. psNPCName .. "\n")
        Debug_ForcePrint(" pfSpeed: " .. pfSpeed .. "\n")
    end
    if(gi_Diagnostic_Cutscene_Flag & gci_Diagnostic_Cutscene_Paths > 0) then
        Debug_ForcePrint(" Caller: " .. LM_GetCallStack(0) .. "\n")
    end
	
    -- |[Setup]|
    --Construct the string.
    local sInstruction = "EM_PushEntity(\""  .. psNPCName .. "\")\n"
    sInstruction = sInstruction .. "TA_SetProperty(\"Z Speed\", " .. pfSpeed .. ")\n"
    sInstruction = sInstruction .. "DL_PopActiveObject()"
    
    -- |[Create]|
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("AutoInstruction")
		REvent_SetProperty("InstructionExec", sInstruction)
	DL_PopActiveObject()

end
