-- |[ ================================= fnCutsceneSetHeight() ================================== ]|
--Creates and registers a cutscene event that causes the named to set their height.
function fnCutsceneSetHeight(psNPCName, pfHeight)

	-- |[Argument Check]|
	if(psNPCName == nil) then return end
	if(pfHeight  == nil) then return end
    
    -- |[Diagnostics]|
    Debug_ForcePrint("fnCutsceneSetHeight() Called.\n")
    if(gi_Diagnostic_Cutscene_Flag & gci_Diagnostic_Cutscene_Arguments > 0) then
        Debug_ForcePrint(" psNPCName: " .. psNPCName .. "\n")
        Debug_ForcePrint(" pfHeight: " .. pfHeight .. "\n")
    end
    if(gi_Diagnostic_Cutscene_Flag & gci_Diagnostic_Cutscene_Paths > 0) then
        Debug_ForcePrint(" Caller: " .. LM_GetCallStack(0) .. "\n")
    end
	
    -- |[Setup]|
    --Construct the string.
    local sInstruction = "EM_PushEntity(\""  .. psNPCName .. "\")\n"
    sInstruction = sInstruction .. "TA_SetProperty(\"Y Vertical Offset\", " .. pfHeight .. ")\n"
    sInstruction = sInstruction .. "DL_PopActiveObject()"
    
    -- |[Execution]|
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("AutoInstruction")
		REvent_SetProperty("InstructionExec", sInstruction)
	DL_PopActiveObject()

end
