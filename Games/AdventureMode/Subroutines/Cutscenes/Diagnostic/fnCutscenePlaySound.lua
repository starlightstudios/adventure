-- |[ ================================= fnCutscenePlaySound() ================================== ]|
--Creates and registers a sound event in cutscene string.
function fnCutscenePlaySound(psSoundName)

	-- |[Argument Check]|
	if(psSoundName == nil) then return end
    
    -- |[Diagnostics]|
    Debug_ForcePrint("fnCutscenePlaySound() Called.\n")
    if(gi_Diagnostic_Cutscene_Flag & gci_Diagnostic_Cutscene_Arguments > 0) then
        Debug_ForcePrint(" psSoundName: " .. psSoundName .. "\n")
    end
    if(gi_Diagnostic_Cutscene_Flag & gci_Diagnostic_Cutscene_Paths > 0) then
        Debug_ForcePrint(" Caller: " .. LM_GetCallStack(0) .. "\n")
    end
	
    -- |[Setup]|
    local sInstruction = "AudioManager_PlaySound(\"" .. psSoundName .. "\")"
    
    -- |[Execution]|
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("AutoInstruction")
		REvent_SetProperty("InstructionExec", sInstruction)
	DL_PopActiveObject()

end
