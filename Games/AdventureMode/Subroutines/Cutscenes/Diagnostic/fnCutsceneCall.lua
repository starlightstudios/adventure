-- |[ ==================================== fnCutsceneCall() ==================================== ]|
--Calls a script in cutscene stride.
function fnCutsceneCall(psPath)

	-- |[Argument Check]|
	if(psPath == nil) then return end
    
    -- |[Diagnostics]|
    Debug_ForcePrint("fnCutsceneCall() Called.\n")
    if(gi_Diagnostic_Cutscene_Flag & gci_Diagnostic_Cutscene_Arguments > 0) then
        Debug_ForcePrint(" psPath: " .. psPath .. "\n")
    end
    if(gi_Diagnostic_Cutscene_Flag & gci_Diagnostic_Cutscene_Paths > 0) then
        Debug_ForcePrint(" Caller: " .. LM_GetCallStack(0) .. "\n")
    end
	
    -- |[Assemble String]|
    local sString = "LM_ExecuteScript(\"" .. psPath .. "\")"
    
    -- |[Instruction]|
	Cutscene_CreateEvent("AutoInstruction")
		REvent_SetProperty("InstructionExec", sString)
	DL_PopActiveObject()

end
