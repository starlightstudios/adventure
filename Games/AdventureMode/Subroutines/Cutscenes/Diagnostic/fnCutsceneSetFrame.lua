-- |[ ================================== fnCutsceneSetFrame() ================================== ]|
--Creates an event to set a special frame for the given character.
function fnCutsceneSetFrame(psActorName, psSpecialFrameName)

	-- |[Argument Check]|
	if(psActorName        == nil) then return end
	if(psSpecialFrameName == nil) then return end
    
    -- |[Diagnostics]|
    Debug_ForcePrint("fnCutsceneSetFrame() Called.\n")
    if(gi_Diagnostic_Cutscene_Flag & gci_Diagnostic_Cutscene_Arguments > 0) then
        Debug_ForcePrint(" psActorName: " .. psActorName .. "\n")
        Debug_ForcePrint(" psSpecialFrameName: " .. psSpecialFrameName .. "\n")
    end
    if(gi_Diagnostic_Cutscene_Flag & gci_Diagnostic_Cutscene_Paths > 0) then
        Debug_ForcePrint(" Caller: " .. LM_GetCallStack(0) .. "\n")
    end

    -- |[Execution]|
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", psActorName)
		ActorEvent_SetProperty("Special Frame", psSpecialFrameName)
	DL_PopActiveObject()

end
