-- |[ ================================= fnCutsceneDoNothing() ================================== ]|
--Creates and registers a do-nothing event for an NPC.
function fnCutsceneDoNothing(psActorName, piTicks)

	-- |[Arg Check]|
	if(psActorName == nil) then return end
	if(piTicks     == nil) then return end
    
    -- |[Diagnostics]|
    Debug_ForcePrint("fnCutsceneDoNothing() Called.\n")
    if(gi_Diagnostic_Cutscene_Flag & gci_Diagnostic_Cutscene_Paths > 0) then
        Debug_ForcePrint(" Caller: " .. LM_GetCallStack(0) .. "\n")
    end

    -- |[Execution]|
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", psActorName)
        ActorEvent_SetProperty("Do Nothing", piTicks)
	DL_PopActiveObject()

end
