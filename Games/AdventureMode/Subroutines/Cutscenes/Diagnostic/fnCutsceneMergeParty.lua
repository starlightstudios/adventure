-- |[ ================================= fnCutsceneMergeParty() ================================= ]|
--Merges the party onto the party leader. Uses the party leader's entity by default.
--This is used usually at the start of a cutscene to make sure all the party positions are already known.
-- If it is possible to "snake" party members and get them stuck on terrain, then this function uses the
-- already existing party movement logic to move them to the party leader's position.
function fnCutsceneMergeParty()
    
    -- |[Diagnostics]|
    Debug_ForcePrint("fnCutsceneMergeParty() Called.\n")
    if(gi_Diagnostic_Cutscene_Flag & gci_Diagnostic_Cutscene_Paths > 0) then
        Debug_ForcePrint(" Caller: " .. LM_GetCallStack(0) .. "\n")
    end

    -- |[Execution]|
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", gsPartyLeaderName)
		ActorEvent_SetProperty("Merge Party")
	DL_PopActiveObject()

end
