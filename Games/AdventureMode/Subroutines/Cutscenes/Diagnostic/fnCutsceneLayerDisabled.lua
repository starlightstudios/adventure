-- |[ =============================== fnCutsceneLayerDisabled() ================================ ]|
--Creates and registers a cutscene event that changes visibility on a given tile layer. True means
-- the layer does not appear, false means it does.
function fnCutsceneLayerDisabled(psLayerName, pbIsDisabled)

	-- |[Argument Check]|
	if(psLayerName  == nil) then return end
	if(pbIsDisabled == nil) then return end
    
    -- |[Diagnostics]|
    Debug_ForcePrint("fnCutsceneLayerDisabled() Called.\n")
    if(gi_Diagnostic_Cutscene_Flag & gci_Diagnostic_Cutscene_Arguments > 0) then
        Debug_ForcePrint(" psLayerName: " .. psLayerName .. "\n")
        Debug_ForcePrint(" pbIsDisabled: " .. pbIsDisabled .. "\n")
    end
    if(gi_Diagnostic_Cutscene_Flag & gci_Diagnostic_Cutscene_Paths > 0) then
        Debug_ForcePrint(" Caller: " .. LM_GetCallStack(0) .. "\n")
    end
	
    -- |[Setup]|
    --Construct the string.
    local sInstruction = "AL_SetProperty(\"Set Layer Disabled\", \"" .. psLayerName .. "\", "
    if(pbIsDisabled == true) then
        sInstruction = sInstruction .. "true)"
    else
        sInstruction = sInstruction .. "false)"
    end
    
    -- |[Execution]|
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("AutoInstruction")
		REvent_SetProperty("InstructionExec", sInstruction)
	DL_PopActiveObject()

end
