-- |[ ================================== fnCutsceneBlocker() =================================== ]|
--Creates and registers a cutscene blocking event.
function fnCutsceneBlocker()
    
    -- |[Diagnostics]|
    Debug_ForcePrint("fnAutoFoldParty() Called.\n")
    if(gi_Diagnostic_Cutscene_Flag & gci_Diagnostic_Cutscene_Paths > 0) then
        Debug_ForcePrint(" Caller: " .. LM_GetCallStack(0) .. "\n")
    end

    -- |[Execution]|
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("Blocker")
	DL_PopActiveObject()

end
