-- |[ ================================ fnCutsceneInstruction() ================================= ]|
--Creates and registers a cutscene event using the provided instruction string.
function fnCutsceneInstruction(psInstruction)

	-- |[Argument Check]|
	if(psInstruction == nil) then return end
    
    -- |[Diagnostics]|
    Debug_ForcePrint("fnCutscene() Called.\n")
    if(gi_Diagnostic_Cutscene_Flag & gci_Diagnostic_Cutscene_Arguments > 0) then
        Debug_ForcePrint(" psInstruction: " .. psInstruction .. "\n")
    end
    if(gi_Diagnostic_Cutscene_Flag & gci_Diagnostic_Cutscene_Paths > 0) then
        Debug_ForcePrint(" Caller: " .. LM_GetCallStack(0) .. "\n")
    end
	
    -- |[Execution]|
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("AutoInstruction")
		REvent_SetProperty("InstructionExec", psInstruction)
	DL_PopActiveObject()

end

--Alternate spelling:
function fnCutscene(psInstruction)

	-- |[Argument Check]|
	if(psInstruction == nil) then return end
    
    -- |[Diagnostics]|
    Debug_ForcePrint("fnCutscene() Called.\n")
    if(gi_Diagnostic_Cutscene_Flag & gci_Diagnostic_Cutscene_Arguments > 0) then
        Debug_ForcePrint(" psInstruction: " .. psInstruction .. "\n")
    end
    if(gi_Diagnostic_Cutscene_Flag & gci_Diagnostic_Cutscene_Paths > 0) then
        Debug_ForcePrint(" Caller: " .. LM_GetCallStack(0) .. "\n")
    end
	
    -- |[Execution]|
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("AutoInstruction")
		REvent_SetProperty("InstructionExec", psInstruction)
	DL_PopActiveObject()
end