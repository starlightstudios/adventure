-- |[ ==================================== fnCutsceneMove() ==================================== ]|
--Creates and registers a cutscene movement event. This is an ActorEvent. Position is in tile coordinates and allows decimals.

-- |[Examples]|
--fnCutsceneMove("Florentina", 1.25, 1.50)
--fnCutsceneMove("Florentina", 1.25, 1.50, 0.50) --Has movement speed specified.

-- |[Function]|
function fnCutsceneMove(psActorName, pfX, pfY, pfMoveSpeed)

	-- |[Argument Check]|
	if(psActorName == nil) then return end
	if(pfX         == nil) then return end
	if(pfY         == nil) then return end
    
    -- |[Diagnostics]|
    Debug_ForcePrint("fnCutsceneCamera() Called.\n")
    if(gi_Diagnostic_Cutscene_Flag & gci_Diagnostic_Cutscene_Arguments > 0) then
        Debug_ForcePrint(" psActorName: " .. psActorName .. "\n")
        Debug_ForcePrint(" pfX: " .. pfX .. "\n")
        Debug_ForcePrint(" pfY: " .. pfY .. "\n")
        if(pfMoveSpeed ~= nil) then Debug_ForcePrint(" pfMoveSpeed: " .. pfMoveSpeed .. "\n") end
    end
    if(gi_Diagnostic_Cutscene_Flag & gci_Diagnostic_Cutscene_Paths > 0) then
        Debug_ForcePrint(" Caller: " .. LM_GetCallStack(0) .. "\n")
    end

    -- |[Execution]|
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", psActorName)
		
		--If the movement speed is not specified:
		if(pfMoveSpeed == nil) then
			ActorEvent_SetProperty("Move To", pfX * gciSizePerTile, pfY * gciSizePerTile)
			
		--Move speed is specified:
		else
			ActorEvent_SetProperty("Move To", pfX * gciSizePerTile, pfY * gciSizePerTile, pfMoveSpeed)
		end
	DL_PopActiveObject()

end
