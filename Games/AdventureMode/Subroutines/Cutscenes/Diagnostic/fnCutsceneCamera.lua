-- |[ =================================== fnCutsceneCamera() =================================== ]|
--Creates and registers a camera modifier package in cutscene stride. Has several "modes" based on what the
-- camera is meant to focus on.
--The default camera movement speed is gcfCamera_Cutscene_Speed.

-- |[Examples]|
--fnCutsceneCamera("Actor ID",   gcfCamera_Cutscene_Speed, 1)
--fnCutsceneCamera("Actor Name", gcfCamera_Cutscene_Speed, "Mei")
--fnCutsceneCamera("Position",   gcfCamera_Cutscene_Speed, 1.25, 1.50)

-- |[Function]|
function fnCutsceneCamera(psType, pfMoveSpeed, pzArg1, pzArg2)

	-- |[Argument Check]|
	if(psType      == nil) then return end
	if(pfMoveSpeed == nil) then return end
    
    -- |[Diagnostics]|
    Debug_ForcePrint("fnCutsceneCamera() Called.\n")
    if(gi_Diagnostic_Cutscene_Flag & gci_Diagnostic_Cutscene_Arguments > 0) then
        Debug_ForcePrint(" psType: " .. psType .. "\n")
        Debug_ForcePrint(" pfMoveSpeed: " .. pfMoveSpeed .. "\n")
        if(pzArg1 ~= nil) then Debug_ForcePrint(" pzArg1: " .. pzArg1 .. "\n") end
        if(pzArg2 ~= nil) then Debug_ForcePrint(" pzArg2: " .. pzArg2 .. "\n") end
    end
    if(gi_Diagnostic_Cutscene_Flag & gci_Diagnostic_Cutscene_Paths > 0) then
        Debug_ForcePrint(" Caller: " .. LM_GetCallStack(0) .. "\n")
    end
    
    -- |[Handling]|
    --Type is "Actor ID". Arg1 is the ID of the actor, Arg2 is unused.
    if(psType == "Actor ID") then
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", pfMoveSpeed)
            CameraEvent_SetProperty("Focus Actor ID", pzArg1)
        DL_PopActiveObject()
    
    --Type is "Actor Name". Arg1 is the name of the actor, Arg2 is unused.
    elseif(psType == "Actor Name") then
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", pfMoveSpeed)
            CameraEvent_SetProperty("Focus Actor Name", pzArg1)
        DL_PopActiveObject()
    
    --Type is "Position". Arg1 is the X position in tile coordinates, Arg2 is the Y position.
    elseif(psType == "Position") then
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", pfMoveSpeed)
            CameraEvent_SetProperty("Focus Position", (pzArg1 * gciSizePerTile), (pzArg2 * gciSizePerTile))
        DL_PopActiveObject()

    --Unhandled type.
    else
        io.write("fnCutsceneCamera: Warning, unhandled type \"" .. psType .. "\"\n")
    end
end
