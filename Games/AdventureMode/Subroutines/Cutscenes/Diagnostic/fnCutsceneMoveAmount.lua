-- |[ ================================= fnCutsceneMoveAmount() ================================= ]|
--Creates and registers a cutscene movement event. This is an ActorEvent. Position is in tile coordinates and allows decimals.
function fnCutsceneMoveAmount(psActorName, pfX, pfY)

	-- |[Argument Check]|
	if(psActorName == nil) then return end
	if(pfX         == nil) then return end
	if(pfY         == nil) then return end
    
    -- |[Diagnostics]|
    Debug_ForcePrint("fnCutsceneMoveAmount() Called.\n")
    if(gi_Diagnostic_Cutscene_Flag & gci_Diagnostic_Cutscene_Arguments > 0) then
        Debug_ForcePrint(" psActorName: " .. psActorName .. "\n")
        Debug_ForcePrint(" pfX: " .. pfX .. "\n")
        Debug_ForcePrint(" pfY: " .. pfY .. "\n")
    end
    if(gi_Diagnostic_Cutscene_Flag & gci_Diagnostic_Cutscene_Paths > 0) then
        Debug_ForcePrint(" Caller: " .. LM_GetCallStack(0) .. "\n")
    end

    -- |[Execution]|
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", psActorName)
        ActorEvent_SetProperty("Move Amount", pfX * gciSizePerTile, pfY * gciSizePerTile)
	DL_PopActiveObject()
    
end