-- |[ =================================== fnCutsceneFadeOut() ================================== ]|
--Fades to black over the given number of ticks.
function fnCutsceneFadeOut(piTicks)
    
    -- |[Argument Check]|
    --If the ticks is nil, it's assumed to be 0.
    if(piTicks == nil) then piTicks = 0 end
    
    -- |[Execute]|
    local sString = "AL_SetProperty(\"Activate Fade\", " .. piTicks .. ", gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1)"
    fnCutscene(sString)
    
end