-- |[ ==================================== fnCutsceneMove() ==================================== ]|
--Creates and registers a cutscene movement event. This is an ActorEvent. Position is in tile coordinates and allows decimals.

-- |[Examples]|
--fnCutsceneMove("Florentina", 1.25, 1.50)
--fnCutsceneMove("Florentina", 1.25, 1.50, 0.50) --Has movement speed specified.

-- |[Function]|
function fnCutsceneMove(psActorName, pfX, pfY, pfMoveSpeed)

	-- |[Argument Check]|
	if(psActorName == nil) then return end
	if(pfX         == nil) then return end
	if(pfY         == nil) then return end

    -- |[Execution]|
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", psActorName)
		
		--If the movement speed is not specified:
		if(pfMoveSpeed == nil) then
			ActorEvent_SetProperty("Move To", pfX * gciSizePerTile, pfY * gciSizePerTile)
			
		--Move speed is specified:
		else
			ActorEvent_SetProperty("Move To", pfX * gciSizePerTile, pfY * gciSizePerTile, pfMoveSpeed)
		end
	DL_PopActiveObject()

end
