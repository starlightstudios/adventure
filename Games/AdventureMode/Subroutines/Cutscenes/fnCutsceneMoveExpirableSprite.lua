-- |[ ============================ fnCutsceneMoveExpirableSprite() ============================= ]|
--An expirable sprite is an NPC that runs its animation once and then vanishes, such as the ones
-- used by catalyst chests. This function moves the sprite to the given position, resets its 
-- animation timer, and marks it to despawn when it's done.
function fnCutsceneMoveExpirableSprite(psName, pfX, pfY)
    
    -- |[Argument Check]|
    if(psName == nil) then return end
    if(pfX == nil) then return end
    if(pfY == nil) then return end
    
    -- |[Orders]|
    fnCutsceneTeleport(psName, pfX, pfY)
    Cutscene_CreateEvent("ActorEvent", "Actor")
        ActorEvent_SetProperty("Subject Name", psName)
        ActorEvent_SetProperty("Reset Move Timer")
    DL_PopActiveObject()
    Cutscene_CreateEvent("ActorEvent", "Actor")
        ActorEvent_SetProperty("Subject Name", psName)
        ActorEvent_SetProperty("Auto Despawn")
    DL_PopActiveObject()
end
