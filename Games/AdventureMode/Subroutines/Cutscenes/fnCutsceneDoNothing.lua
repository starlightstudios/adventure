-- |[ ================================= fnCutsceneDoNothing() ================================== ]|
--Creates and registers a do-nothing event for an NPC.
function fnCutsceneDoNothing(psActorName, piTicks)

	-- |[Arg Check]|
	if(psActorName == nil) then return end
	if(piTicks     == nil) then return end

    -- |[Execution]|
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", psActorName)
        ActorEvent_SetProperty("Do Nothing", piTicks)
	DL_PopActiveObject()

end
