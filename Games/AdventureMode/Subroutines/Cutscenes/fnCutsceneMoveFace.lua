-- |[ ================================== fnCutsceneMoveFace() ================================== ]|
--Creates and registers a cutscene movement event. This is an ActorEvent. Position is in tile coordinates and allows decimals.
-- The entity in question will face the given direction while moving.

-- |[Examples]|
--fnCutsceneMove("Florentina", 1.25, 1.50, 0, 1)       --Face south
--fnCutsceneMove("Florentina", 1.25, 1.50, 0, 1, 0.50) --Has movement speed specified.

-- |[Function]|
function fnCutsceneMoveFace(psActorName, pfX, pfY, piFaceX, piFaceY, pfMoveSpeed)

	-- |[Argument Check]|
	if(psActorName == nil) then return end
	if(pfX         == nil) then return end
	if(pfY         == nil) then return end
	if(piFaceX     == nil) then return end
	if(piFaceY     == nil) then return end

    -- |[Execution]|
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", psActorName)
		
		--If the movement speed is not specified:
		if(pfMoveSpeed == nil) then
			ActorEvent_SetProperty("Move To While Facing", pfX * gciSizePerTile, pfY * gciSizePerTile, piFaceX, piFaceY)
			
		--Move speed is specified:
		else
			ActorEvent_SetProperty("Move To While Facing", pfX * gciSizePerTile, pfY * gciSizePerTile, piFaceX, piFaceY, pfMoveSpeed)
		end
	DL_PopActiveObject()

end
