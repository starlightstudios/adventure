-- |[ ================================== fnCutsceneJumpNPC() =================================== ]|
--Creates and registers a cutscene event that causes the named NPC to jump.
function fnCutsceneJumpNPC(psNPCName, pfSpeed)

	-- |[Arg Check]|
	if(psNPCName == nil) then return end
	if(pfSpeed   == nil) then return end
	
    -- |[Setup]|
    --Construct the string.
    local sInstruction = "EM_PushEntity(\""  .. psNPCName .. "\")\n"
    sInstruction = sInstruction .. "TA_SetProperty(\"Z Speed\", " .. pfSpeed .. ")\n"
    sInstruction = sInstruction .. "DL_PopActiveObject()"
    
    -- |[Create]|
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("AutoInstruction")
		REvent_SetProperty("InstructionExec", sInstruction)
	DL_PopActiveObject()

end
