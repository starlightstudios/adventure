-- |[ ==================================== fnCutsceneWait() ==================================== ]|
--Creates and registers a cutscene event which will wait the provided number of ticks. 60 ticks is 1 second.
function fnCutsceneWait(piTicks)

	-- |[Argument Check]|
	if(piTicks == nil) then return end
	
    -- |[Execution]|
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("WaitTicks", "Timer")
		TimeEvent_SetProperty("Timer", piTicks)
	DL_PopActiveObject()

end
