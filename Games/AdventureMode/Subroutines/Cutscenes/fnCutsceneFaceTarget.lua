-- |[ ================================= fnCutsceneFaceTarget() ================================= ]|
--Creates and registers a cutscene change-facing event. This is an ActorEvent. Facing is done with the name
-- of the entity to face.
function fnCutsceneFaceTarget(psActorName, psActorTarget)

	-- |[Arg Check]|
	if(psActorName == nil) then return end
	if(psActorTarget == nil) then return end

    -- |[Execution]|
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", psActorName)
		ActorEvent_SetProperty("Face", psActorTarget)
	DL_PopActiveObject()

end
