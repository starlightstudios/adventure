-- |[ ================================== fnCutsceneTeleport() ================================== ]|
--Creates and registers a cutscene teleport event. This is an ActorEvent. Position is in tile coordinates and allows decimals.
function fnCutsceneTeleport(psActorName, pfX, pfY)

	-- |[Argument Check]|
	if(psActorName == nil) then return end
	if(pfX         == nil) then return end
	if(pfY         == nil) then return end

    -- |[Execution]|
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", psActorName)
		ActorEvent_SetProperty("Teleport To", pfX * gciSizePerTile, pfY * gciSizePerTile)
	DL_PopActiveObject()

end
