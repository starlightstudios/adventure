-- |[ ================================= fnCutsceneMergeParty() ================================= ]|
--Merges the party onto the party leader. Uses the party leader's entity by default.
--This is used usually at the start of a cutscene to make sure all the party positions are already known.
-- If it is possible to "snake" party members and get them stuck on terrain, then this function uses the
-- already existing party movement logic to move them to the party leader's position.
function fnCutsceneMergeParty()

	--Autogenerates the instruction name.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", gsPartyLeaderName)
		ActorEvent_SetProperty("Merge Party")
	DL_PopActiveObject()

end
