-- |[ ==================================== fnCutsceneCall() ==================================== ]|
--Calls a script in cutscene stride. Can handle arguments, but they are all assumed to be strings.
function fnCutsceneCall(psPath, ...)

	-- |[Argument Check]|
	if(psPath == nil) then return end
	
    -- |[One Argument]|
    --If only the path is presented, we can skip a lot of work.
    local iArgs = select("#", ...)
    if(iArgs < 1) then
        local sString = "LM_ExecuteScript(\"" .. psPath .. "\")"
        Cutscene_CreateEvent("AutoInstruction")
            REvent_SetProperty("InstructionExec", sString)
        DL_PopActiveObject()
        return
    end

    -- |[Variable Arguments]|
    --Additional arguments have been passed so we need to send them along to the script.
    local sString = "LM_ExecuteScript(\"" .. psPath .. "\", "
    for i = 1, iArgs, 1 do
    
        --Resolve the string.
        local sArgString = select(i, ...)
    
        --Append it, leaving space for the next string.
        if(i < iArgs) then
            sString = sString .. "\"" .. sArgString .. "\", "
    
        --Append it, and finish the string.
        else
            sString = sString .. "\"" .. sArgString .. "\")"
        end
    end
    
    --Create event.
    --io.write("Advanced call: " .. sString .. "\n")
    Cutscene_CreateEvent("AutoInstruction")
        REvent_SetProperty("InstructionExec", sString)
    DL_PopActiveObject()
end
