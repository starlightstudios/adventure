-- |[ ================================= fnCutscenePlaySound() ================================== ]|
--Creates and registers a sound event in cutscene string.
function fnCutscenePlaySound(psSoundName)

	-- |[Argument Check]|
	if(psSoundName == nil) then return end
	
    -- |[Setup]|
    local sInstruction = "AudioManager_PlaySound(\"" .. psSoundName .. "\")"
    
    -- |[Execution]|
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("AutoInstruction")
		REvent_SetProperty("InstructionExec", sInstruction)
	DL_PopActiveObject()

end
