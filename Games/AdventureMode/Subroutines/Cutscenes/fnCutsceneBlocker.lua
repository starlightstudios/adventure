-- |[ ================================== fnCutsceneBlocker() =================================== ]|
--Creates and registers a cutscene blocking event.
function fnCutsceneBlocker()

	--Autogenerates the instruction name.
	Cutscene_CreateEvent("Blocker")
	DL_PopActiveObject()

end
