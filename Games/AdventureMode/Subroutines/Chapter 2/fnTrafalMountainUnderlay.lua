-- |[ =============================== fnTrafalMountainUnderlay() =============================== ]|
--Function that loads and places underlays for when the player is on a map at the peak of Trafal.
-- The function needs to be called twice, once to specify it should load the images, and in the
-- post-constructor to position them.
function fnTrafalMountainUnderlay(pbIsLoading, piOffsetX, piOffsetY)
    
    -- |[Argument Check]|
    if(pbIsLoading == nil) then return end
    
    -- |[Loading Call]|
    if(pbIsLoading == true) then
    
        --If the images are already loaded, stop.
        if(DL_Exists("Root/Images/AdventureUI/MapUnderlays/PlateauSunset0") == true) then return end
            
        --Underlays use linear filtering.
        local iLinear = DM_GetEnumeration("GL_LINEAR")
        ALB_SetTextureProperty("MagFilter", iLinear)
        ALB_SetTextureProperty("MinFilter", iLinear)

        --Underlays need to wrap on the X axis, but clamp on the Y axis.
        local iClampEdgeEnum = DM_GetEnumeration("GL_CLAMP_TO_EDGE")
        local iRepeatEnum    = DM_GetEnumeration("GL_REPEAT")
        ALB_SetTextureProperty("Wrap S", iRepeatEnum)
        ALB_SetTextureProperty("Wrap T", iClampEdgeEnum)
        
        --Load.
        SLF_Open(gsDatafilesPath .. "UIAdventure.slf")
        DL_AddPath("Root/Images/AdventureUI/MapUnderlays/")
        DL_ExtractBitmap("Underlay|PlateauSunset0", "Root/Images/AdventureUI/MapUnderlays/PlateauSunset0")
        DL_ExtractBitmap("Underlay|PlateauSunset1", "Root/Images/AdventureUI/MapUnderlays/PlateauSunset1")
        SLF_Close()
        
        --Return to normal.
        ALB_SetTextureProperty("Restore Defaults")
        SLF_Close()
    
    -- |[Positioning Call]|
    else
    
        --Allocate Space.
        AL_SetProperty("Allocate Backgrounds", 2)
    
        --Layer 0. This is the "Sky".
        local fMoveScale = 0.000
        local fMapScale =  1.000
        AL_SetProperty("Background Image",          0, "Root/Images/AdventureUI/MapUnderlays/PlateauSunset0")
        AL_SetProperty("Background Render Offsets", 0, 0.0, 0, fMoveScale, fMoveScale)
        AL_SetProperty("Background Alpha",          0, 1.0, 0)
        AL_SetProperty("Background Autoscroll",     0, 0.0, 0.0)
        AL_SetProperty("Background Scale",          0, -fMapScale)
    
        --Layer 1. This is the mountains. These can have an optional offset to give the impression of height.
        local iOffsetX = 0
        local iOffsetY = 0
        fMoveScale = -0.200
        fMapScale = 1.50
        if(piOffsetX ~= nil) then iOffsetX = piOffsetX end
        if(piOffsetY ~= nil) then iOffsetY = piOffsetY end
        AL_SetProperty("Background Image",          1, "Root/Images/AdventureUI/MapUnderlays/PlateauSunset1")
        AL_SetProperty("Background Render Offsets", 1, iOffsetX, iOffsetY, fMoveScale, fMoveScale)
        AL_SetProperty("Background Alpha",          1, 1.0, 0)
        AL_SetProperty("Background Autoscroll",     1, 0.0, 0.0)
        AL_SetProperty("Background Scale",          1, -fMapScale)
    
    end
end
