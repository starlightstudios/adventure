-- |[ ================================ fnHandleNorthwoodsMap() ================================= ]|
--Given the name of a map in the Northwoods region, runs the setup to create the map with all of the
-- parts that have been revealed thus far. Also places the player marker.
function fnHandleNorthwoodsMap(psMapName)
    
    -- |[Arg Check]|
    if(psMapName == nil) then return end
    
    -- |[Clear]|
    AM_SetProperty("Clear Advanced Map")
    
    -- |[Base]|
    --Always visible.
    AM_SetProperty("Create Advanced Map Layer", "Base", "Root/Images/AdvMaps/Northwoods/Base")
    
    -- |[Pieces]|
    --Overlay all discovered pieces atop the map.
    for i = 0, gciNorthwoodsTotalMaps, 1 do
        
        --Get the variable path and the map piece path.
        local sLayerName    = string.format("Layer%02i", i)
        local sVariablePath = string.format("Root/Variables/Chapter2/NorthwoodsMap/iRoom%02i", i)
        local sPiecePath    = string.format("Root/Images/AdvMaps/Northwoods/Piece%02i",        i)
    
        --If the variable is 1, place the piece.
        local iVariable = VM_GetVar(sVariablePath, "N")
        if(iVariable == 1.0) then
            AM_SetProperty("Create Advanced Map Layer", sLayerName, sPiecePath)
            AM_SetProperty("Layer Writes Stencil", sLayerName, 10)
        end
    end
    
    -- |[Connections Layer]|
    AM_SetProperty("Create Advanced Map Layer", "Connections", "Root/Images/AdvMaps/Northwoods/Rooms")
    AM_SetProperty("Layer Reads Stencil", "Connections", 10)
    AM_SetProperty("Layer Is Toggleable", "Connections", true)
    
    -- |[Player Indicator]|
    --Where the player is. On a per-map basis. The lookup table should have already set coordinates.
    if(gzaChapter2MapLookups == nil) then return end
    for i = 1, #gzaChapter2MapLookups, 1 do
        if(gzaChapter2MapLookups[i].sName == psMapName) then
            AM_SetProperty("Create Advanced Map Layer", "LayerP", "Null")
            AM_SetProperty("Layer Renders Player",      "LayerP", "Root/Images/AdvMaps/MapIndicators/Sanya_Human", true, gzaChapter2MapLookups[i].fXOffset, gzaChapter2MapLookups[i].fYOffset)
        end
    end

end

-- |[ ============================== fnHandleNorthwoodsMapWarp() =============================== ]|
--Warp version. Identical, except uses the warp code. Called when assembling the warp lists.
function fnHandleNorthwoodsMapWarp()
    
    -- |[Error Check]|
    --If the map hasn't been set up yet, don't call this function. Other chapters ignore this call
    -- but it is still part of the warp setup.
    if(gciNorthwoodsTotalMaps == nil) then return end
    
    -- |[Region Setup]|
    --Constants
    local cfSizeFactor = 1.0 / 1.5
    local cfScreenWid  = 1366.0
    local cfScreenHei  =  768.0
    local cfMapWid     = 2732.0
    local cfMapHei     = 1536.0
    
    --Creation
    AM_SetProperty("Register Warp Region",            "Northwoods", "Root/Images/AdventureUI/AdvCampfireWarpIcon/Northwoods", "Advanced", "Null")
    AM_SetProperty("Set Warp Region Alignments",      "Northwoods",  0.0, 0.0, 0.0, 0.0, 90.0, 90.0)
    AM_SetProperty("Set Warp Region Advanced Clamps", "Northwoods", 0, 0, -(cfMapWid * cfSizeFactor) + cfScreenWid, -(cfMapHei * cfSizeFactor) + cfScreenHei)

    -- |[Base Layer]|
    AM_SetProperty("Register Warp Region Advanced Pack", "Northwoods", "Base", "Root/Images/AdvMaps/Northwoods/Base")

    -- |[Pieces]|
    --Overlay all discovered pieces atop the map.
    for i = 0, gciNorthwoodsTotalMaps, 1 do
        
        --Get the variable path and the map piece path.
        local sLayerName    = string.format("Layer%02i", i)
        local sVariablePath = string.format("Root/Variables/Chapter2/NorthwoodsMap/iRoom%02i", i)
        local sPiecePath    = string.format("Root/Images/AdvMaps/Northwoods/Piece%02i",        i)

        --If the variable is 1, place the piece.
        local iVariable = VM_GetVar(sVariablePath, "N")
        if(iVariable == 1.0) then
            AM_SetProperty("Register Warp Region Advanced Pack",         "Northwoods", sLayerName, sPiecePath)
            AM_SetProperty("Warp Region Advanced Render Writes Stencil", "Northwoods", sLayerName, 10)
        end
    end

    -- |[Connections Layer]|
    AM_SetProperty("Register Warp Region Advanced Pack",        "Northwoods", "Connections", "Root/Images/AdvMaps/Northwoods/Rooms")
    AM_SetProperty("Warp Region Advanced Render Reads Stencil", "Northwoods", "Connections", 10)
    AM_SetProperty("Warp Region Advanced Render Can Toggle",    "Northwoods", "Connections", true)
end
