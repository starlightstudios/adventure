-- |[ ============================= fnBuildMisoRequirementsString ============================== ]|
--Given an array of pattern {{"Item Name", iQuantity}, {"Item Name", iQuantity}, etc}, produces the
-- string Miso says during dialogue to indicate its requirements.
function fnBuildMisoRequirementsString(pzaRequirements)
    
    -- |[Argument Check]|
    if(pzaRequirements == nil) then return [[ Append("Miso:[E|Neutral] This is an error![P] Oops![B][C]") ]] end
    
    -- |[Setup]|
    local sRequirementsString = [[ Append("Miso:[E|Neutral] I'll need ]]
    
    -- |[Fixed Size Cases]|
    --Error.
    if(#pzaRequirements < 1) then
        sRequirementsString = sRequirementsString .. [[ nothing![P] This is an error![P] Oops![B][C]") ]]

    --Exactly one requirement.
    elseif(#pzaRequirements == 1) then
        sRequirementsString = sRequirementsString .. pzaRequirements[1][2] .. " [" .. pzaRequirements[1][1] .. "] to make this.[B][C]\")"
        
    -- |[Variable Size Cases]|
    else

        --Middle.
        local iTotal = #pzaRequirements
        for i = 1, #pzaRequirements-1, 1 do
            sRequirementsString = sRequirementsString .. pzaRequirements[i][2] .. " [" .. pzaRequirements[i][1] .. "], "
        end

        --End.
        sRequirementsString = sRequirementsString .. "and " .. pzaRequirements[iTotal][2] .. " [" .. pzaRequirements[iTotal][1] .. "] to make this.[B][C]\")"
    end

    -- |[Finish Up]|
    return sRequirementsString
    
end
