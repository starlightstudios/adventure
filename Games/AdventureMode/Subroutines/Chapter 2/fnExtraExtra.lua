-- |[ ===================================== fnExtraExtra() ===================================== ]|
--Given a path to the folder that is calling it, executes the 'Extra, Extra!' field ability. This
-- ability tries to find an NPC in front of the player and triggers a dialogue to show them a pamphlet.

-- |[Worker Function]|
--Given an entry in the form {sName, iType}, checks if this entry should be removed from the Extra, Extra!
-- call by virtue of being someone who can't be canvassed, such as a party member or a door.
--Returns true if the entry should be removed, false otherwise.
function fnShouldRemoveEntry(pzEntry)
    
    -- |[Argument Check]|
    if(pzEntry == nil) then return false end
    
    -- |[Non-NPC]|
    --The algorithm that assembles this list populates it with non-NPCs. Prune those.
    if(pzEntry[2] ~= gciLevel_SpecialType_Actor) then 
        return true
    end
    
    -- |[Party Leader]|
    --Same name as the party leader. Can be called due to inaccuracies in the hit function.
    if(pzEntry[1] == gsPartyLeaderName) then
        return true
    end
    
    -- |[Party Members]|
    --You can hit party members by folding the party back on itself.
    for p = 1, giFollowersTotal, 1 do
        if(pzEntry[1] == gsaFollowerNames[p]) then
            return true
        end
    end
    
    -- |[All Checks Passed]|
    return false
end

-- |[Main Function]|
function fnExtraExtra(psBasePath)
    
    -- |[Argument Check]|
    if(psBasePath == nil) then return end
    
    -- |[Hit Checking]|
    --Get a list of all entities in front of the player.
    local zaHitList = fnGetObjectsInFrontOfPlayer(true)
    
    --If the list has zero elements, stop.
    if(#zaHitList < 1) then return end
    
    --Diagnostics:
    if(gciExtraExtra_Diagnostic_Level == gciExtraExtra_Diagnostic_On) then
        io.write("Running Extra, Extra!\n")
        io.write(" Got " .. #zaHitList .. " hits.\n")
        for i = 1, #zaHitList, 1 do
            io.write("  " .. zaHitList[i][1] .. ": " .. zaHitList[i][2] .. "\n")
        end
    end
    
    -- |[Trimming]|
    --Debug.
    if(gciExtraExtra_Diagnostic_Level == gciExtraExtra_Diagnostic_On) then
        io.write(" Removing party members and non-NPCs.\n")
    end
    
    --Remove instances of party members. As funny as it is to show Zeke a pamphlet.
    local iListSize = #zaHitList
    local i = 1
    while(i <= iListSize) do
        
        --Run this subroutine:
        if(fnShouldRemoveEntry(zaHitList[i]) == true) then
            table.remove(zaHitList, i)
            i = i - 1
            iListSize = iListSize - 1
        end
        
        --Next.
        i = i + 1
    end
    
    --Debug.
    if(gciExtraExtra_Diagnostic_Level == gciExtraExtra_Diagnostic_On) then
        io.write(" List size after removal: " .. #zaHitList .. "\n")
        for i = 1, #zaHitList, 1 do
            io.write("  " .. zaHitList[i][1] .. ": " .. zaHitList[i][2] .. "\n")
        end
    end
    
    --If the list has zero elements, stop.
    if(#zaHitList < 1) then return end
    
    -- |[Execution]|
    --Get and execute the sub-pamphlet name.
    local sPamphlet = VM_GetVar("Root/Variables/Global/Izuna/sPamphlet", "S")
    if(sPamphlet == "None") then return end

    --Pattern: "Pamphlet [Name].lua"
    local sPath = psBasePath .. "Pamphlet " .. sPamphlet .. ".lua"
    if(gciExtraExtra_Diagnostic_Level == gciExtraExtra_Diagnostic_On) then
        io.write(" Exec path: " .. sPath .. "\n")
    end
    
    --Pick the first entity's name as the target.
    local sTargetName = zaHitList[1][1]
    
    --If the path doesn't exist, stop.
    if(FS_Exists(sPath) == false) then return end
    LM_ExecuteScript(sPath, sTargetName)
    
end
