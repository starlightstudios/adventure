-- |[ ================================== fnRecomputeZekeCosts ================================== ]|
--Whenever Zeke spends JP to upgrade his stats, all of his repurchaseable skills increase in cost.
-- This function handles that cost computation.
function fnRecomputeZekeCosts()

    -- |[Error Check]|
    --Make sure Zeke exists!
    if(AdvCombat_GetProperty("Does Party Member Exist", "Zeke") == false) then return end

    -- |[Variables]|
    local iRepurchaseTotal = VM_GetVar("Root/Variables/Global/Zeke/iRepurchaseTotal",  "N")
    
    -- |[Compute]|
    local iCostPerSkill = gzaZekeStats.iJPCostBase + (gzaZekeStats.iJPCostIncrease * iRepurchaseTotal)

    -- |[Push and Set]|
    --Push Zeke.
    AdvCombat_SetProperty("Push Party Member", "Zeke")
    
        --Push the skills and set their costs.
        AdvCombatEntity_SetProperty("Push Ability S", "Goat Internal|Health Boost")
            AdvCombatAbility_SetProperty("JP Cost", iCostPerSkill)
        DL_PopActiveObject()
        AdvCombatEntity_SetProperty("Push Ability S", "Goat Internal|Power Boost")
            AdvCombatAbility_SetProperty("JP Cost", iCostPerSkill)
        DL_PopActiveObject()
        AdvCombatEntity_SetProperty("Push Ability S", "Goat Internal|Accuracy Boost")
            AdvCombatAbility_SetProperty("JP Cost", iCostPerSkill)
        DL_PopActiveObject()
        AdvCombatEntity_SetProperty("Push Ability S", "Goat Internal|Evade Boost")
            AdvCombatAbility_SetProperty("JP Cost", iCostPerSkill)
        DL_PopActiveObject()
        AdvCombatEntity_SetProperty("Push Ability S", "Goat Internal|Initiative Boost")
            AdvCombatAbility_SetProperty("JP Cost", iCostPerSkill)
        DL_PopActiveObject()
        AdvCombatEntity_SetProperty("Push Ability S", "Goat Internal|Item Boost")
            AdvCombatAbility_SetProperty("JP Cost", iCostPerSkill)
        DL_PopActiveObject()
        AdvCombatEntity_SetProperty("Push Ability S", "Goat Internal|Bleed Boost")
            AdvCombatAbility_SetProperty("JP Cost", iCostPerSkill)
        DL_PopActiveObject()
        AdvCombatEntity_SetProperty("Push Ability S", "Goat Internal|Strike Boost")
            AdvCombatAbility_SetProperty("JP Cost", iCostPerSkill)
        DL_PopActiveObject()
    DL_PopActiveObject()
end
