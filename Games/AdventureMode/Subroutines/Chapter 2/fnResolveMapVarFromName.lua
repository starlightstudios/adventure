-- |[ =============================== fnResolveMapVarFromName() ================================ ]|
--Given the name of a room, resolves which piece it is on the overlay map. The overlay map has no 
-- particular piece sorting so a lookup table is required.
--Returns the DataLibrary path of the matching variable, or "Null" if it was not found.
function fnResolveMapVarFromName(psMapName)

    -- |[Arg Check]|
    if(psMapName == nil) then return end
    
    --Debug.
    gzaChapter1TrannadarLookups = nil
    gzaChapter2MapLookups = nil
    
    -- |[First-Time Table Creation]|
    --The first time this algorithm runs, create a table to scan.
    if(gzaChapter1TrannadarLookups == nil) then
        LM_ExecuteScript(gsRoot .. "Maps/Z Map Lookups/Chapter 1 Lookups.lua")
    end

    if(gzaChapter2MapLookups == nil) then
        
        -- |[Setup]|
        gzaChapter2MapLookups = {}
        
        -- |[Prototype]|
        --Entries are:
        --[1] Name of the room
        --[2] Path to the variable
        --[3] X Offset for the indicator
        --[4] Y Offset for the indicator
        
        -- |[Northwoods Block]|
        table.insert(gzaChapter2MapLookups, {"NorthwoodsNWE", "Root/Variables/Chapter2/NorthwoodsMap/iRoom00",  795,  127})
        table.insert(gzaChapter2MapLookups, {"NorthwoodsNWD", "Root/Variables/Chapter2/NorthwoodsMap/iRoom01",  804,  319})
        table.insert(gzaChapter2MapLookups, {"NorthwoodsNWF", "Root/Variables/Chapter2/NorthwoodsMap/iRoom02",  826,  531})
        table.insert(gzaChapter2MapLookups, {"NorthwoodsNCC", "Root/Variables/Chapter2/NorthwoodsMap/iRoom03",  835,  758})
        table.insert(gzaChapter2MapLookups, {"NorthwoodsSWA", "Root/Variables/Chapter2/NorthwoodsMap/iRoom04",  807,  967})
        table.insert(gzaChapter2MapLookups, {"NorthwoodsSWB", "Root/Variables/Chapter2/NorthwoodsMap/iRoom05",  729, 1260})
        table.insert(gzaChapter2MapLookups, {"NorthwoodsNWB", "Root/Variables/Chapter2/NorthwoodsMap/iRoom06", 1136,  218})
        table.insert(gzaChapter2MapLookups, {"NorthwoodsNWA", "Root/Variables/Chapter2/NorthwoodsMap/iRoom07", 1069,  406})
        table.insert(gzaChapter2MapLookups, {"NorthwoodsNCB", "Root/Variables/Chapter2/NorthwoodsMap/iRoom08", 1106,  601})
        table.insert(gzaChapter2MapLookups, {"NorthwoodsNCD", "Root/Variables/Chapter2/NorthwoodsMap/iRoom09", 1102,  844})
        table.insert(gzaChapter2MapLookups, {"NorthwoodsSCD", "Root/Variables/Chapter2/NorthwoodsMap/iRoom10", 1111, 1023})
        table.insert(gzaChapter2MapLookups, {"NorthwoodsSWC", "Root/Variables/Chapter2/NorthwoodsMap/iRoom11",  984, 1365})
        table.insert(gzaChapter2MapLookups, {"NorthwoodsNWC", "Root/Variables/Chapter2/NorthwoodsMap/iRoom12", 1402,  227})
        table.insert(gzaChapter2MapLookups, {"SanyaCabinA",   "Root/Variables/Chapter2/NorthwoodsMap/iRoom13", 1456,  380})
        table.insert(gzaChapter2MapLookups, {"NorthwoodsNCA", "Root/Variables/Chapter2/NorthwoodsMap/iRoom14", 1447,  575})
        table.insert(gzaChapter2MapLookups, {"NorthwoodsSCA", "Root/Variables/Chapter2/NorthwoodsMap/iRoom15", 1455,  770})
        table.insert(gzaChapter2MapLookups, {"NorthwoodsSCB", "Root/Variables/Chapter2/NorthwoodsMap/iRoom16", 1495, 1038})
        table.insert(gzaChapter2MapLookups, {"NorthwoodsSWD", "Root/Variables/Chapter2/NorthwoodsMap/iRoom17", 1210, 1352})
        table.insert(gzaChapter2MapLookups, {"NorthwoodsSEA", "Root/Variables/Chapter2/NorthwoodsMap/iRoom18", 1499, 1286})
        table.insert(gzaChapter2MapLookups, {"NorthwoodsNEE", "Root/Variables/Chapter2/NorthwoodsMap/iRoom19", 1655,  213})
        table.insert(gzaChapter2MapLookups, {"NorthwoodsNED", "Root/Variables/Chapter2/NorthwoodsMap/iRoom20", 1676,  524})
        table.insert(gzaChapter2MapLookups, {"NorthwoodsSCE", "Root/Variables/Chapter2/NorthwoodsMap/iRoom21", 1735,  743})
        table.insert(gzaChapter2MapLookups, {"NorthwoodsSEC", "Root/Variables/Chapter2/NorthwoodsMap/iRoom22", 1707, 1039})
        table.insert(gzaChapter2MapLookups, {"NorthwoodsSEB", "Root/Variables/Chapter2/NorthwoodsMap/iRoom23", 1727, 1309})
        table.insert(gzaChapter2MapLookups, {"HeroPlateauC",  "Root/Variables/Chapter2/NorthwoodsMap/iRoom24", 2018,  106})
        table.insert(gzaChapter2MapLookups, {"NorthwoodsNEA", "Root/Variables/Chapter2/NorthwoodsMap/iRoom25", 1936,  249})
        table.insert(gzaChapter2MapLookups, {"NorthwoodsNEB", "Root/Variables/Chapter2/NorthwoodsMap/iRoom26", 1897,  459})
        table.insert(gzaChapter2MapLookups, {"NorthwoodsNEC", "Root/Variables/Chapter2/NorthwoodsMap/iRoom27", 1898,  607})
        table.insert(gzaChapter2MapLookups, {"NorthwoodsSED", "Root/Variables/Chapter2/NorthwoodsMap/iRoom28", 1939, 1349})
        
        -- |[Westwoods Block]|
        table.insert(gzaChapter2MapLookups, {"GranvirePassA",   "Root/Variables/Chapter2/WestwoodsMap/iRoom00", 1080,  224})
        table.insert(gzaChapter2MapLookups, {"GranvirePassB",   "Root/Variables/Chapter2/WestwoodsMap/iRoom01", 1306,  240})
        table.insert(gzaChapter2MapLookups, {"GranvirePassC",   "Root/Variables/Chapter2/WestwoodsMap/iRoom02", 1060,  393})
        table.insert(gzaChapter2MapLookups, {"GranvirePassD",   "Root/Variables/Chapter2/WestwoodsMap/iRoom03", 1048,  558})
        table.insert(gzaChapter2MapLookups, {"VucaPassA",       "Root/Variables/Chapter2/WestwoodsMap/iRoom04", 1773,  126})
        table.insert(gzaChapter2MapLookups, {"KitsuneVillageK", "Root/Variables/Chapter2/WestwoodsMap/iRoom05", 1961,   38})
        table.insert(gzaChapter2MapLookups, {"KitsuneVillageB", "Root/Variables/Chapter2/WestwoodsMap/iRoom06", 1964,  146})
        table.insert(gzaChapter2MapLookups, {"VucaPassB",       "Root/Variables/Chapter2/WestwoodsMap/iRoom07", 1797,  273})
        table.insert(gzaChapter2MapLookups, {"KitsuneVillageA", "Root/Variables/Chapter2/WestwoodsMap/iRoom08", 1947,  255})
        table.insert(gzaChapter2MapLookups, {"VucaPassC",       "Root/Variables/Chapter2/WestwoodsMap/iRoom09", 1741,  396})
        table.insert(gzaChapter2MapLookups, {"VucaPassD",       "Root/Variables/Chapter2/WestwoodsMap/iRoom10", 1765,  502})
        table.insert(gzaChapter2MapLookups, {"KitsuneVillageC", "Root/Variables/Chapter2/WestwoodsMap/iRoom08", 1947,  255}) --As C
        table.insert(gzaChapter2MapLookups, {"KitsuneVillageL", "Root/Variables/Chapter2/WestwoodsMap/iRoom05", 1961,   38}) --As K
        table.insert(gzaChapter2MapLookups, {"KitsuneVillageD", "Root/Variables/Chapter2/WestwoodsMap/iRoom06", 1964,  146}) --As B
        table.insert(gzaChapter2MapLookups, {"KitsuneVillageE", "Root/Variables/Chapter2/WestwoodsMap/iRoom06", 1964,  146}) --As B
        table.insert(gzaChapter2MapLookups, {"KitsuneVillageF", "Root/Variables/Chapter2/WestwoodsMap/iRoom06", 1964,  146}) --As B
        table.insert(gzaChapter2MapLookups, {"KitsuneVillageG", "Root/Variables/Chapter2/WestwoodsMap/iRoom06", 1964,  146}) --As B
        table.insert(gzaChapter2MapLookups, {"KitsuneVillageH", "Root/Variables/Chapter2/WestwoodsMap/iRoom06", 1964,  146}) --As B
        table.insert(gzaChapter2MapLookups, {"KitsuneVillageI", "Root/Variables/Chapter2/WestwoodsMap/iRoom06", 1964,  146}) --As B
        table.insert(gzaChapter2MapLookups, {"KitsuneVillageJ", "Root/Variables/Chapter2/WestwoodsMap/iRoom06", 1964,  146}) --As B
        table.insert(gzaChapter2MapLookups, {"KitsuneVillageK", "Root/Variables/Chapter2/WestwoodsMap/iRoom06", 1964,  146}) --As B
        table.insert(gzaChapter2MapLookups, {"KitsuneVillageL", "Root/Variables/Chapter2/WestwoodsMap/iRoom06", 1964,  146}) --As B
        table.insert(gzaChapter2MapLookups, {"KitsuneVillageM", "Root/Variables/Chapter2/WestwoodsMap/iRoom06", 1964,  146}) --As B
        table.insert(gzaChapter2MapLookups, {"WestwoodsNA",     "Root/Variables/Chapter2/WestwoodsMap/iRoom11",  866,  737})
        table.insert(gzaChapter2MapLookups, {"WestwoodsNB",     "Root/Variables/Chapter2/WestwoodsMap/iRoom12", 1057,  731})
        table.insert(gzaChapter2MapLookups, {"WestwoodsNC",     "Root/Variables/Chapter2/WestwoodsMap/iRoom13", 1207,  731})
        table.insert(gzaChapter2MapLookups, {"WestwoodsND",     "Root/Variables/Chapter2/WestwoodsMap/iRoom14", 1392,  731})
        table.insert(gzaChapter2MapLookups, {"WestwoodsNE",     "Root/Variables/Chapter2/WestwoodsMap/iRoom15", 1568,  716})
        table.insert(gzaChapter2MapLookups, {"WestwoodsNF",     "Root/Variables/Chapter2/WestwoodsMap/iRoom16", 1768,  719})
        table.insert(gzaChapter2MapLookups, {"WestwoodsWA",     "Root/Variables/Chapter2/WestwoodsMap/iRoom17",  857,  863})
        table.insert(gzaChapter2MapLookups, {"WestwoodsWB",     "Root/Variables/Chapter2/WestwoodsMap/iRoom18",  854,  992})
        table.insert(gzaChapter2MapLookups, {"WestwoodsCA",     "Root/Variables/Chapter2/WestwoodsMap/iRoom19",  995,  999})
        table.insert(gzaChapter2MapLookups, {"WestwoodsCB",     "Root/Variables/Chapter2/WestwoodsMap/iRoom19", 1000,  999})
        table.insert(gzaChapter2MapLookups, {"WestwoodsCC",     "Root/Variables/Chapter2/WestwoodsMap/iRoom20", 1137,  999})
        table.insert(gzaChapter2MapLookups, {"WestwoodsCD",     "Root/Variables/Chapter2/WestwoodsMap/iRoom21", 1276,  977})
        table.insert(gzaChapter2MapLookups, {"WestwoodsCE",     "Root/Variables/Chapter2/WestwoodsMap/iRoom22", 1453,  977})
        table.insert(gzaChapter2MapLookups, {"WestwoodsCF",     "Root/Variables/Chapter2/WestwoodsMap/iRoom22", 1453,  977})
        table.insert(gzaChapter2MapLookups, {"WestwoodsCG",     "Root/Variables/Chapter2/WestwoodsMap/iRoom23", 1600,  986})
        table.insert(gzaChapter2MapLookups, {"WestwoodsEA",     "Root/Variables/Chapter2/WestwoodsMap/iRoom24", 1768,  945})
        table.insert(gzaChapter2MapLookups, {"WestwoodsEB",     "Root/Variables/Chapter2/WestwoodsMap/iRoom24", 1768,  945})
        table.insert(gzaChapter2MapLookups, {"WestwoodsWC",     "Root/Variables/Chapter2/WestwoodsMap/iRoom25",  842, 1104})
        table.insert(gzaChapter2MapLookups, {"WestwoodsWD",     "Root/Variables/Chapter2/WestwoodsMap/iRoom26",  851, 1230})
        table.insert(gzaChapter2MapLookups, {"WestwoodsEG",     "Root/Variables/Chapter2/WestwoodsMap/iRoom27", 1773, 1110})
        table.insert(gzaChapter2MapLookups, {"WestwoodsEE",     "Root/Variables/Chapter2/WestwoodsMap/iRoom28", 1442, 1298})
        table.insert(gzaChapter2MapLookups, {"WestwoodsED",     "Root/Variables/Chapter2/WestwoodsMap/iRoom29", 1638, 1295})
        table.insert(gzaChapter2MapLookups, {"WestwoodsEC",     "Root/Variables/Chapter2/WestwoodsMap/iRoom30", 1779, 1262})
        table.insert(gzaChapter2MapLookups, {"WestwoodsEF",     "Root/Variables/Chapter2/WestwoodsMap/iRoom31", 1955, 1239})
        table.insert(gzaChapter2MapLookups, {"WestwoodsSEC",    "Root/Variables/Chapter2/WestwoodsMap/iRoom32", 1609, 1427})
        table.insert(gzaChapter2MapLookups, {"WestwoodsSEA",    "Root/Variables/Chapter2/WestwoodsMap/iRoom33", 1770, 1374})
        table.insert(gzaChapter2MapLookups, {"WestwoodsSEB",    "Root/Variables/Chapter2/WestwoodsMap/iRoom34", 1823, 1500})
        table.insert(gzaChapter2MapLookups, {"WarrensCaveA",    "Root/Variables/Chapter2/WestwoodsMap/iRoom35",  827,  433})
        table.insert(gzaChapter2MapLookups, {"WarrensA",        "Root/Variables/Chapter2/WestwoodsMap/iRoom36",  713,  530})
        table.insert(gzaChapter2MapLookups, {"WarrensB",        "Root/Variables/Chapter2/WestwoodsMap/iRoom37",  568,  523})
        table.insert(gzaChapter2MapLookups, {"WarrensC",        "Root/Variables/Chapter2/WestwoodsMap/iRoom38",  565,  407})
    
        table.insert(gzaChapter2MapLookups, {"HarpyBaseA",      "Root/Variables/Chapter2/WestwoodsMap/iRoom32", 2003, 1050})
        table.insert(gzaChapter2MapLookups, {"HarpyBaseB",      "Root/Variables/Chapter2/WestwoodsMap/iRoom32", 2003, 1050})
        table.insert(gzaChapter2MapLookups, {"HarpyBaseC",      "Root/Variables/Chapter2/WestwoodsMap/iRoom32", 2003, 1050})
        
        -- |[Mt. Sarulente]|
        table.insert(gzaChapter2MapLookups, {"SarulenteA",        "Root/Variables/Chapter2/MtSarulenteMap/iRoom14", 1546, 1413})
        table.insert(gzaChapter2MapLookups, {"SarulenteB",        "Root/Variables/Chapter2/MtSarulenteMap/iRoom07", 1557, 1094})
        table.insert(gzaChapter2MapLookups, {"SarulenteC",        "Root/Variables/Chapter2/MtSarulenteMap/iRoom05", 1538,  818})
        table.insert(gzaChapter2MapLookups, {"SarulenteD",        "Root/Variables/Chapter2/MtSarulenteMap/iRoom08", 2055,  898})
        table.insert(gzaChapter2MapLookups, {"SarulenteE",        "Root/Variables/Chapter2/MtSarulenteMap/iRoom09", 2411,  924})
        table.insert(gzaChapter2MapLookups, {"SarulenteF",        "Root/Variables/Chapter2/MtSarulenteMap/iRoom04", 1046,  644})
        table.insert(gzaChapter2MapLookups, {"SarulenteG",        "Root/Variables/Chapter2/MtSarulenteMap/iRoom03",  568,  661})
        table.insert(gzaChapter2MapLookups, {"SarulenteLibraryA", "Root/Variables/Chapter2/MtSarulenteMap/iRoom06", 1322,  952})
        table.insert(gzaChapter2MapLookups, {"EmpressCaveA",      "Root/Variables/Chapter2/MtSarulenteMap/iRoom02",  536,  370})
        table.insert(gzaChapter2MapLookups, {"EmpressCaveB",      "Root/Variables/Chapter2/MtSarulenteMap/iRoom02",  532,  216})
        table.insert(gzaChapter2MapLookups, {"EmpressCaveC",      "Root/Variables/Chapter2/MtSarulenteMap/iRoom01",  532,  114})
        table.insert(gzaChapter2MapLookups, {"EmpressCaveD",      "Root/Variables/Chapter2/MtSarulenteMap/iRoom00",  324,  248})
        table.insert(gzaChapter2MapLookups, {"FortSarulenteA",    "Root/Variables/Chapter2/MtSarulenteMap/iRoom10", 1099,  373})
        table.insert(gzaChapter2MapLookups, {"FortSarulenteB",    "Root/Variables/Chapter2/MtSarulenteMap/iRoom11", 1352,  387})
        table.insert(gzaChapter2MapLookups, {"FortSarulenteC",    "Root/Variables/Chapter2/MtSarulenteMap/iRoom12", 1648,  373})
        
        -- |[Shrine of the Hero]|
        table.insert(gzaChapter2MapLookups, {"HeroPlateauA", "Null", 2406, 168})
        table.insert(gzaChapter2MapLookups, {"HeroPlateauB", "Null", 2406, 168})
        table.insert(gzaChapter2MapLookups, {"HeroPlateauC", "Null", 2406, 168})
        table.insert(gzaChapter2MapLookups, {"HeroShrineA",  "Null", 2406, 168})
        table.insert(gzaChapter2MapLookups, {"HeroShrineB",  "Null", 2406, 168})
        table.insert(gzaChapter2MapLookups, {"HeroShrineC",  "Null", 2406, 168})
        table.insert(gzaChapter2MapLookups, {"HeroShrineD",  "Null", 2406, 168})
        table.insert(gzaChapter2MapLookups, {"HeroShrineE",  "Null", 2406, 168})
        table.insert(gzaChapter2MapLookups, {"HeroShrineF",  "Null", 2406, 168})
        table.insert(gzaChapter2MapLookups, {"LowerShrineA", "Null", 2406, 168})
        table.insert(gzaChapter2MapLookups, {"LowerShrineB", "Null", 2406, 168})
        table.insert(gzaChapter2MapLookups, {"LowerShrineC", "Null", 2406, 168})
        table.insert(gzaChapter2MapLookups, {"LowerShrineD", "Null", 2406, 168})
        table.insert(gzaChapter2MapLookups, {"LowerShrineE", "Null", 2406, 168})
        table.insert(gzaChapter2MapLookups, {"LowerShrineF", "Null", 2406, 168})
        table.insert(gzaChapter2MapLookups, {"LowerShrineG", "Null", 2406, 168})
        table.insert(gzaChapter2MapLookups, {"LowerShrineH", "Null", 2406, 168})
    
        -- |[Apply Names]|
        for i = 1, #gzaChapter2MapLookups, 1 do
        
            --Set names.
            gzaChapter2MapLookups[i].sName    = gzaChapter2MapLookups[i][1]
            gzaChapter2MapLookups[i].sVarPath = gzaChapter2MapLookups[i][2]
            gzaChapter2MapLookups[i].fXOffset = gzaChapter2MapLookups[i][3]
            gzaChapter2MapLookups[i].fYOffset = gzaChapter2MapLookups[i][4]
        
            --Remove duplicate data.
            gzaChapter2MapLookups[i][1] = nil
            gzaChapter2MapLookups[i][2] = nil
            gzaChapter2MapLookups[i][3] = nil
            gzaChapter2MapLookups[i][4] = nil
        
        end
    end

    -- |[Scan]|
    if(gzaChapter1TrannadarLookups ~= nil) then
        for i = 1, #gzaChapter1TrannadarLookups, 1 do
            if(psMapName == gzaChapter1TrannadarLookups[i].sName) then
                return gzaChapter1TrannadarLookups[i].sVarPath
            end
        end
    end
    if(gzaChapter2MapLookups ~= nil) then
        for i = 1, #gzaChapter2MapLookups, 1 do
            if(psMapName == gzaChapter2MapLookups[i].sName) then
                return gzaChapter2MapLookups[i].sVarPath
            end
        end
    end

    -- |[Not Found]|
    --Return "Null" to indicate the map was not found.
    return "Null"
end
