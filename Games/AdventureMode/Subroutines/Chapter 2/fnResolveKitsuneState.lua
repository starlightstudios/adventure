-- |[ ================================= fnResolveKitsuneState ================================== ]|
--In chapter 2, uses assorted variables to resolve a single integer used to represent the "state"
-- of kitsune dialogue.
function fnResolveKitsuneState()
    
    -- |[Intro]|
    --Has not gone to the temple yet.
    local iRescuedIzuna = VM_GetVar("Root/Variables/Chapter2/KitsuneVillage/iRescuedIzuna", "N")
    if(iRescuedIzuna == 0.0) then return gciKitsuneIntro end
    
    --Has rescued Izuna.
    if(true) then return gciKitsuneRescuedIzuna end
    
    -- |[Debug]|
    io.write("Unhandled kitsune dialogue case!\n")
    return 0
    
end
