-- |[Function: fnRubberRemover]|
--Special function, removes any enemies from the map that cannot be rubbered, and enemies that can get a new defeat script.
function fnRubberRemover()

    --Valid List
    local iAsciiA = string.byte("A")
    local saValidList = {"WerecatProwler", "WerecatThief", "BeeSkirmisher", "AlrauneKeeper", "AlrauneStem", "AlraunePetal", "BeeGatherer", "Werecat"}
    local saIgnoreList = {"Rubberraune", "Rubbercat", "Rubberbee", "RubbercatThief"}
    
    --Dynamically scan enemy names.
    for x = 1, 26, 1 do
        for y = 1, 26, 1 do
            
            --Resolve name.
            local sEnemyName = "Enemy" .. string.char(iAsciiA + x -1) .. string.char(iAsciiA + y - 1)
            if(EM_Exists(sEnemyName) == true) then
                EM_PushEntity(sEnemyName)
                
                    --Scan to see if a valid entry is found.
                    local bMatch = false
                    local sCombatName = TA_GetProperty("Zeroth Enemy Name")
                    for i = 1, #saValidList, 1 do
                        if(sCombatName == saValidList[i]) then
                            bMatch = true
                            TA_SetProperty("Defeat Scene", "Defeat_Rubber")
                        end
                    end
                    
                    --See if we need to ignore this enemy.
                    if(bMatch == false) then
                        for i = 1, #saIgnoreList, 1 do
                            if(sCombatName == saIgnoreList[i]) then
                                bMatch = true
                            end
                        end
                    end
                    
                    --No matches: Move entity offscreen.
                    if(bMatch == false) then
                        RE_SetDestruct(true)
                    end
                DL_PopActiveObject()
            end
        end
    end
    
    --Scan wandering enemies.
    for x = 1, 26, 1 do
            
        --Resolve name.
        local sEnemyName = "EnemyWander" .. string.char(iAsciiA + x -1) 
        if(EM_Exists(sEnemyName) == true) then
            EM_PushEntity(sEnemyName)
            
                --Scan to see if a valid entry is found.
                local bMatch = false
                local sCombatName = TA_GetProperty("Zeroth Enemy Name")
                for i = 1, #saValidList, 1 do
                    if(sCombatName == saValidList[i]) then
                        bMatch = true
                        TA_SetProperty("Defeat Scene", "Defeat_Rubber")
                    end
                end
                
                --See if we need to ignore this enemy.
                if(bMatch == false) then
                    for i = 1, #saIgnoreList, 1 do
                        if(sCombatName == saIgnoreList[i]) then
                            bMatch = true
                        end
                    end
                end
                
                --No matches: Move entity offscreen.
                if(bMatch == false) then
                    RE_SetDestruct(true)
                end
            DL_PopActiveObject()
        end
    end
end
