-- |[ ================================= fnIsFlorentinaPresent() ================================ ]|
--Returns true if Florentina is in the active party. Used for things like lockpick checks.
function fnIsFlorentinaPresent()
    
    -- |[Scan Party]|
    for i = 1, #gsaFollowerNames, 1 do
        if(gsaFollowerNames[i] == "Florentina") then
            return true
        end
    end
    
    -- |[Not Found]|
    return false
end