-- |[ =================================== fnStandardCh1Map() =================================== ]|
--Given the name of a level, searches for that level's associated map function and runs it, setting
-- any associated variables if needed.
function fnStandardCh1Map(psLevelName)

    -- |[Argument Check]|
    if(psLevelName == nil) then return end

    -- |[Locate Automatic Map]|
    local zHandlingMap = GUIMap:fnLocateMapContainingLookup(psLevelName)
    if(zHandlingMap ~= nil) then
        
        --Run the subroutines.
        zHandlingMap:fnHandleUnlock(psLevelName)
        zHandlingMap:fnHandleMap(psLevelName)
        
        --Clean other globals.
        gfnLastMapFunction = nil
        
        --Mark this object as the last one to handle the map call.
        gsLastGUIMap = zHandlingMap.sLocalName
        return
    end
end