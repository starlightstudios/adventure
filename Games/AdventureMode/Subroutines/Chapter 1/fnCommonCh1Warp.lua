-- |[ =================================== fnCommonCh1Warp() ==================================== ]|
--Called by all warp ending handlers in chapter 1, which allows for incrementing some quest variables.
function fnCommonCh1Warp(psRoomName)
    
    -- |[Argument Check]|
    if(psRoomName == nil) then return end
    
    -- |[Mycela Warps Count]|
    --If the player warps enough times without talking to Mycela, Nadia shows up at their warp destination to tell them.
    local iDefeatedMycela = VM_GetVar("Root/Variables/Chapter1/Sharelock/iDefeatedMycela", "N")
    if(iDefeatedMycela == 1.0) then
        local iWarpsSinceMycela = VM_GetVar("Root/Variables/Chapter1/Mycela/iWarpsSinceMycela", "N")
        VM_SetVar("Root/Variables/Chapter1/Mycela/iWarpsSinceMycela", "N", iWarpsSinceMycela + 1)
    end
end