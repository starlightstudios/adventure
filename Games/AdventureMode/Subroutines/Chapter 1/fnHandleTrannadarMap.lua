-- |[ ================================= fnHandleTrannadarMap() ================================= ]|
--Given the name of a map in the Trannadar (central) region, runs the setup to create the map with all of the
-- parts that have been revealed thus far. Also places the player marker.
function fnHandleTrannadarMap(psMapName)
    
    -- |[Arg Check]|
    if(psMapName == nil) then return end
    io.write("Handling map with name " .. psMapName .. "\n")
    
    -- |[Clear]|
    AM_SetProperty("Clear Advanced Map")
    
    -- |[Base]|
    --Always visible.
    AM_SetProperty("Create Advanced Map Layer", "Base", "Root/Images/AdvMaps/Trannadar/Base")
    DL_ReportBitmap("Root/Images/AdvMaps/Trannadar/Base")
    
    -- |[Pieces]|
    --Overlay all discovered pieces atop the map.
    for i = 0, gciTrannadarMapsTotal, 1 do
        
        --Get the variable path and the map piece path.
        local sLayerName    = string.format("Layer%02i", i)
        local sVariablePath = string.format("Root/Variables/Chapter1/TrannadarMap/iRoom%02i", i)
        local sPiecePath    = string.format("Root/Images/AdvMaps/Trannadar/Slice_%02i",        i)
    
        --If the variable is 1, place the piece.
        local iVariable = VM_GetVar(sVariablePath, "N")
        if(iVariable == 1.0) then
            AM_SetProperty("Create Advanced Map Layer", sLayerName, sPiecePath)
            AM_SetProperty("Layer Writes Stencil", sLayerName, 10)
        end
    end
    
    -- |[Connections Layer]|
    --AM_SetProperty("Create Advanced Map Layer", "Connections", "Root/Images/AdvMaps/MtSarulente/Rooms")
    --AM_SetProperty("Layer Reads Stencil", "Connections", 10)
    --AM_SetProperty("Layer Is Toggleable", "Connections", true)
    
    -- |[Player Indicator]|
    --Where the player is. On a per-map basis. The lookup table should have already set coordinates.
    if(gzaChapter1TrannadarLookups == nil) then return end
    for i = 1, #gzaChapter1TrannadarLookups, 1 do
        if(gzaChapter1TrannadarLookups[i].sName == psMapName) then
            AM_SetProperty("Create Advanced Map Layer", "LayerP", "Null")
            AM_SetProperty("Layer Renders Player",      "LayerP", "Root/Images/AdvMaps/MapIndicators/Mei_Human", true, gzaChapter1TrannadarLookups[i].fXOffset, gzaChapter1TrannadarLookups[i].fYOffset)
        end
    end
    
    -- |[Overlay]|
    --Looks nice!
    AM_SetProperty("Create Advanced Map Layer", "Overlay", "Root/Images/AdvMaps/Trannadar/Overlay")
end

-- |[ ============================ fnHandleTrannadarMapWarp() ============================ ]|
--Warp version. Identical, except uses the warp code. Called when assembling the warp lists.
function fnHandleTrannadarMapWarp()

    -- |[Error Check]|
    --If the map hasn't been set up yet, don't call this function. Other chapters ignore this call
    -- but it is still part of the warp setup.
    if(gciTrannadarMapsTotal == nil) then return end
    
    -- |[Region Setup]|
    --Constants
    local cfSizeFactor = 1.0 / 1.5
    local cfScreenWid  = 1366.0
    local cfScreenHei  =  768.0
    local cfMapWid     = 2732.0
    local cfMapHei     = 1536.0
    
    --Creation
    AM_SetProperty("Register Warp Region",            "Trannadar Province", "Root/Images/AdventureUI/AdvCampfireWarpIcon/Trannadar", "Advanced", "Null")
    AM_SetProperty("Set Warp Region Alignments",      "Trannadar Province",  0.0, 0.0, 0.0, 0.0, 90.0, 90.0)
    AM_SetProperty("Set Warp Region Advanced Clamps", "Trannadar Province", 0, 0, -(cfMapWid * cfSizeFactor) + cfScreenWid, -(cfMapHei * cfSizeFactor) + cfScreenHei)

    -- |[Base Layer]|
    AM_SetProperty("Register Warp Region Advanced Pack", "Trannadar Province", "Base", "Root/Images/AdvMaps/Trannadar/Base")

    -- |[Pieces]|
    --Overlay all discovered pieces atop the map.
    for i = 0, gciTrannadarMapsTotal, 1 do
        
        --Get the variable path and the map piece path.
        local sLayerName    = string.format("Layer%02i", i)
        local sVariablePath = string.format("Root/Variables/Chapter1/TrannadarMap/iRoom%02i", i)
        local sPiecePath    = string.format("Root/Images/AdvMaps/Trannadar/Slice_%02i",        i)

        --If the variable is 1, place the piece.
        local iVariable = VM_GetVar(sVariablePath, "N")
        if(iVariable == 1.0) then
            AM_SetProperty("Register Warp Region Advanced Pack",         "Trannadar Province", sLayerName, sPiecePath)
            AM_SetProperty("Warp Region Advanced Render Writes Stencil", "Trannadar Province", sLayerName, 10)
        end
    end

    -- |[Connections Layer]|
    AM_SetProperty("Register Warp Region Advanced Pack",        "Trannadar Province", "Connections", "Root/Images/AdvMaps/Trannadar/Rooms")
    AM_SetProperty("Warp Region Advanced Render Reads Stencil", "Trannadar Province", "Connections", 10)
    AM_SetProperty("Warp Region Advanced Render Can Toggle",    "Trannadar Province", "Connections", true)
end
