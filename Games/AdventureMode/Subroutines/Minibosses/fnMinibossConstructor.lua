-- |[ ================================ fnMinibossConstructor() ================================= ]|
--Called in the constructor for a level, spawns a miniboss at either the A or B position, with B
-- being the position the enemy spawns in after it has been defeated at least once. This means
-- that miniboss encounters can be fought repeatedly for loot, but won't block the way the second time.
--Note: In RoP these are named "RedRobeAA" while in WHI they are "MinibossAA". The function also
-- requires a chapter string in RoP.
function fnMinibossConstructor(psLetter, psChapterString, psRoomName, piXStart, piYStart, piXEnd, piYEnd)
    
    -- |[Argument Check]|
    if(psLetter        == nil) then return end
    if(psChapterString == nil) then return end
    if(psRoomName      == nil) then return end
    
    -- |[Resolve Variables]|
    --Resolve entity names.
    local sEntityNameActive   = "RedRobe" .. psLetter .. "A"
    local sEntityNameInactive = "RedRobe" .. psLetter .. "B"
    
    --Resolve variable name.
    local sVarName = "Root/Variables/Minibosses/" .. psChapterString .. "/i" .. psRoomName .. "|" .. psLetter
    
    --Retrieve variable.
    local iVariable = VM_GetVar(sVarName, "N")
    
    -- |[Spawn Entity]|
    --Miniboss spawns in the active position.
    if(iVariable == 0.0) then
        fnStandardNPCByPosition(sEntityNameActive)
        AL_SetProperty("Set Layer Disabled", "BossArea"..psLetter, false)

    --Miniboss spawns in the inactive position. Unset collisions.
    else
    
        --Spawn.
        fnStandardNPCByPosition(sEntityNameInactive)
        
        --Clear collisions.
        if(piXStart ~= nil and piYStart ~= nil and piXEnd ~= nil and piYEnd ~= nil) then
            for x = piXStart, piXEnd, 1 do
                for y = piYStart, piYEnd, 1 do
                    AL_SetProperty("Set Collision", x, y, 0, 0)
                end
            end
        end
    
        --Open doors.
        local iLetter = string.byte("A")
        local sDoorName = "MinibossDoor" .. psLetter .. string.char(iLetter)
        while(AL_GetProperty("Does Door Exist", sDoorName) == true) do
            
            --Open it.
            AL_SetProperty("Open Door", sDoorName)
            
            --Next door name.
            iLetter = iLetter + 1
            sDoorName = "MinibossDoor" .. psLetter .. string.char(iLetter)
        end
        
        --Tile layer.
        AL_SetProperty("Set Layer Disabled", "BossArea"..psLetter, true)
    end
end