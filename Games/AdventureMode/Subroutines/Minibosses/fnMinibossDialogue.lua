-- |[ ================================== fnMinibossDialogue() ================================== ]|
--Called in the dialogue for a level when the player speaks to a given miniboss entity. This triggers
-- a battle with a specific victory case which marks the entity as defeated and changes collisions.
function fnMinibossDialogue(psActorName, psBasePath, psaEnemyList)
    
    -- |[Argument Check]|
    if(psActorName  == nil) then return end
    if(psBasePath   == nil) then return end
    if(psaEnemyList == nil) then return end
    
    -- |[Activation]|
    --Make sure the player can't talk to a downed miniboss.
    local sSpecialFrameName = TA_GetProperty("Current Special Frame")
    if(sSpecialFrameName ~= "Null") then return end
    
    -- |[Resolve Variables]|
    --Setup.
    local sVictoryPath   = psBasePath .. psActorName .. ".lua"
    local sVictoryString = "AdvCombat_SetProperty(\"Victory Script\", \"" .. sVictoryPath .. "\")"
    local sDefeatPath    = gsStandardGameOver
    local sDefeatString  = "AdvCombat_SetProperty(\"Defeat Script\", \"" .. sDefeatPath .. "\")"
    
    -- |[Fire Battle]|
    --Tourist mode check.
    local bIsTouristMode = AdvCombat_GetProperty("Is Tourist Mode")

    --Flags.
    fnCutscene([[ AdvCombat_SetProperty("Reinitialize") ]])
    if(bIsTouristMode == false) then
        fnCutscene([[ AdvCombat_SetProperty("Unretreatable", true) ]])
    end
    if(FS_Exists(sVictoryPath) == true) then
        fnCutscene(sVictoryString)
    end
    if(FS_Exists(sDefeatPath) == true) then
        fnCutscene(sDefeatString)
    end
    
    -- |[Enemy Handling]|
    --For each enemy in the list, run that enemy's script.
    for i = 1, #psaEnemyList, 1 do
        
        --If the enemy's name does not begin with "STD|" then this is a pre-defined enemy, such as a boss.
        local sFourLet = string.sub(psaEnemyList[i], 1, 4)
        if(sFourLet ~= "STD|") then
            io.write("By-script enemy spawning is not supported in RoP, please use a roster enemy. " .. psaEnemyList[i] .. "\n")
            --The entity's script should be in a specific folder in the combat directory.
            --local sEnemyPath = gsMonocerosRoot .. "Combat/Enemies/Enemy List/000 Scripted Enemies/" .. psaEnemyList[i] .. ".lua"
        
            --Build a string around this and fire it.
            --local sEnemyString = "LM_ExecuteScript(\"" .. sEnemyPath .. "\", 0)"
            --fnCutscene(sEnemyString)
        
        --This is a normal enemy from the roster and uses the standard handler.
        else
        
            --Get the entity's name as it appears on the enemy list.
            local sEnemyName = string.sub(psaEnemyList[i], 5)
        
            --Path is the auto handler.
            local sEnemyPath = AdvCombat_GetProperty("Enemy Auto Handler")
            
            --We also need to add the name of the enemy when calling the script.
            local sEnemyString = "LM_ExecuteScript(\"" .. sEnemyPath .. "\", \"" .. sEnemyName .. "\")"
            fnCutscene(sEnemyString)
        
        end
    end
    
    -- |[Finish Up]|
    fnCutscene([[ AdvCombat_SetProperty("Activate") ]])
    fnCutscene([[ AdvCombat_SetProperty("World Pulse", true) ]])
    fnCutsceneBlocker()
    
end