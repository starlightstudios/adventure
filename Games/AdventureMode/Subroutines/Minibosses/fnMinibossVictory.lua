-- |[ =================================== fnMinibossVictory() ================================== ]|
--Called by the victory script after the miniboss has been defeated. Causes that enemy to become
-- downed, removes collisions, and sets the repeat flag.
function fnMinibossVictory(psLetter, psActivity, psChapterString, psRoomName, piXStart, piYStart, piXEnd, piYEnd)
    
    -- |[Argument Check]|
    if(psLetter        == nil) then return end
    if(psActivity      == nil) then return end
    if(psChapterString == nil) then return end
    if(psRoomName      == nil) then return end

    -- |[Derived Variables]|
    --Resolve entity names.
    local sEntityName = "RedRobe" .. psLetter .. psActivity

    --Resolve variable name.
    local sVarName   = "Root/Variables/Minibosses/" .. psChapterString .. "/i" .. psRoomName .. "|" .. psLetter

    -- |[Repeat Flag]|
    --The party won! The entity will spawn in the "off" position and not block the path later.
    VM_SetVar(sVarName, "N", 1.0)

    -- |[Entity Knockout]|
    --Remove collision, add special frame to the entity.
    EM_PushEntity(sEntityName)

        --Collisions.
        TA_SetProperty("Clipping Flag", false)
        
        --Set special frame.
        if(TA_GetProperty("Has Special Frame", "Wounded") == false) then
            TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/RedRobe|Wounded")
        end
        
        --Activate.
        TA_SetProperty("Set Special Frame", "Wounded")
    DL_PopActiveEntity()

    -- |[Clear Collisions]|
    --Zero all collisions in the block defined by the provided coordinates.
    if(piXStart ~= nil and piYStart ~= nil and piXEnd ~= nil and piYEnd ~= nil) then
        for x = piXStart, piXEnd, 1 do
            for y = piYStart, piYEnd, 1 do
                AL_SetProperty("Set Collision", x, y, 0, 0)
            end
        end
    end
    
    -- |[Open Doors]|
    --Iterate across possible doors. This is used for collisions near paths where enemies walk. Doors
    -- don't need to recompile the blockmap to correctly change collisions so they are used instead of tiles.
    --The algorithm iterates across doors named the same thing as the miniboss until one is not found.
    local iLetter = string.byte("A")
    local sDoorName = "MinibossDoor" .. psLetter .. string.char(iLetter)
    while(AL_GetProperty("Does Door Exist", sDoorName) == true) do
        
        --Open it.
        AL_SetProperty("Open Door", sDoorName)
        
        --Next door name.
        iLetter = iLetter + 1
        sDoorName = "MinibossDoor" .. psLetter .. string.char(iLetter)
    end
    
    -- |[Tile Layers]|
    --If there is an associated tile layer, disable it.
    AL_SetProperty("Set Layer Disabled", "BossArea"..psLetter, true)
end
