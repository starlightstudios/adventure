-- |[ ==================================== Catalyst Handler ==================================== ]|
--Called whenever the player opens a Catalyst chest. Set as a global path to be used by the C++ code.
-- This script is responsible for both printing to the dialogue and also recomputing stats.
--The 0th argument is which catalyst type was received.

--Argument check.
if(fnArgCheck(1) == false) then return end
local sCatalystType = LM_GetScriptArgument(0)

-- |[Setup]|
--Variables.
local bRerunStats = false
local ciFireworks = 8
local ciFireworkTPF = 6

-- |[Common]|
--Position of the last opened chest.
local fChestX, fChestY = AL_GetProperty("Last Chest Position")

--Resolve the catalyst image path.
local sCatalystPath = "Root/Images/Sprites/Catalyst/Heart"
if(sCatalystType == "Health") then
	sCatalystPath = "Root/Images/Sprites/Catalyst/Heart"
elseif(sCatalystType == "Attack") then
	sCatalystPath = "Root/Images/Sprites/Catalyst/Sword"
elseif(sCatalystType == "Initiative") then
	sCatalystPath = "Root/Images/Sprites/Catalyst/Boot"
elseif(sCatalystType == "Evade") then
	sCatalystPath = "Root/Images/Sprites/Catalyst/Dodge"
elseif(sCatalystType == "Accuracy") then
	sCatalystPath = "Root/Images/Sprites/Catalyst/Target"
elseif(sCatalystType == "Skill") then
	sCatalystPath = "Root/Images/Sprites/Catalyst/Skill"
end

-- |[ ======================================== Animation ======================================= ]|
--In all cases, spawn the catalyst animation.
TA_Create("CatalystAnim")
	TA_SetProperty("Position", -100, -100)
	TA_SetProperty("Rendering Depth", 0.000000)
	for i = 1, 8, 1 do
		for p = 1, 4, 1 do
			TA_SetProperty("Move Frame", i-1, p-1, sCatalystPath)
		end
	end
DL_PopActiveObject()

--Create some fireworks.
for q = 1, (ciFireworks*2)+1, 1 do
	TA_Create("CatalystFirework" .. q)
		TA_SetProperty("Position", -100, -100)
		TA_SetProperty("Rendering Depth", 0.000000)
		TA_SetProperty("Walk Ticks Per Frame", ciFireworkTPF)
		TA_SetProperty("Auto Animates Fast", true)
		for i = 1, 8, 1 do
			for p = 1, 4, 1 do
				TA_SetProperty("Move Frame", i-1, p-1, sCatalystPath .. "Firework" .. (p-1))
			end
		end
	DL_PopActiveObject()
end

--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()

--Play the special sound.
fnCutscene([[ AudioManager_PlaySound("World|GetCatalyst") ]])

--Reposition the icon.
local iRunningCount = 0
for i = 1, 32, 1 do
	
	--Set the teleportation case.
	local fOffset = math.sin(i / 32.0 * 3.1415926 * 0.5) * 1.0
	fnCutsceneTeleport("CatalystAnim", (fChestX / gciSizePerTile) + 0.25, (fChestY / gciSizePerTile) - fOffset)
	fnCutsceneBlocker()
end
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(20)
fnCutsceneBlocker()

--Spawn fireworks. Despawn them after a few frames.
for i = 1, ciFireworks, 1 do
	
	--Play a sound effect.
	local sString = "AudioManager_PlaySound(\"World|Firework\" .. LM_GetRandomNumber(0, 2))"
	fnCutscene(sString)

	--Generate a position.
	local fRange = math.floor(LM_GetRandomNumber(17, 20)) / 10.0
	local fDegrees = ((900 / ciFireworks) * i)  +  LM_GetRandomNumber(-25, 25)
	local fXPos = (fChestX / gciSizePerTile) + 0.25 + (math.cos(fDegrees * 3.1415926 / 180.0) * fRange)
	local fYPos = (fChestY / gciSizePerTile) - 1.00 + (math.sin(fDegrees * 3.1415926 / 180.0) * fRange)

	--Create an event to spawn the firework.
	fnCutsceneTeleport("CatalystFirework" .. (i*2)+0, fXPos, fYPos)
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "CatalystFirework" .. (i*2)+0)
		ActorEvent_SetProperty("Reset Move Timer")
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "CatalystFirework" .. (i*2)+0)
		ActorEvent_SetProperty("Auto Despawn")
	DL_PopActiveObject()
	
	--Wait for the next firework.
	fnCutsceneWait(LM_GetRandomNumber(3, 4))
	fnCutsceneBlocker()
	
	--Create another firework on a shorter radius.
	fRange = LM_GetRandomNumber(9, 14) / 10.0
	fDegrees = ((900 / ciFireworks) * i)  +  LM_GetRandomNumber(-25, 25)
	fXPos = (fChestX / gciSizePerTile) + 0.25 + (math.cos(fDegrees * 3.1415926 / 180.0) * -fRange)
	fYPos = (fChestY / gciSizePerTile) - 1.00 + (math.sin(fDegrees * 3.1415926 / 180.0) * -fRange)

	--Create an event to spawn the firework.
	fnCutsceneTeleport("CatalystFirework" .. (i*2)+1, fXPos, fYPos)
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "CatalystFirework" .. (i*2)+1)
		ActorEvent_SetProperty("Reset Move Timer")
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "CatalystFirework" .. (i*2)+1)
		ActorEvent_SetProperty("Auto Despawn")
	DL_PopActiveObject()
	
	--Wait for the next firework.
	fnCutsceneWait(LM_GetRandomNumber(3, 4))
	fnCutsceneBlocker()
end

--Wait a bit.
fnCutsceneWait(10)
fnCutsceneBlocker()

-- |[ ==================================== Backend Handling ==================================== ]|
-- |[Health]|
if(sCatalystType == "Health") then
	
	--How many we have.
	local iCatalystCount = AdInv_GetProperty("Catalyst Count", gciCatalyst_Health)
	AdInv_SetProperty("Catalyst Count", gciCatalyst_Health, iCatalystCount + 1)
	
	--Update the DataLibrary version.
	VM_SetVar("Root/Variables/Global/Catalysts/iHealth", "N", iCatalystCount + 1)
    
    --Update local counts.
    local iLocalCurrent = AdInv_GetProperty("Catalyst Local Cur", gciCatalyst_Health)
    AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Health, iLocalCurrent + 1)
	
	--Modulus.
	local iCatalystModulus = (iCatalystCount + 1) % gciCatalyst_Health_Needed
	local iCatalystAgainst = gciCatalyst_Health_Needed - iCatalystModulus
	
	--Full!
	if(iCatalystAgainst == gciCatalyst_Health_Needed) then
		
		--Text.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("You completed a Health Catalyst![P] The entire party's HP increased by 10!") ]])
		fnCutsceneBlocker()
		
		--Refresh stats.
		bRerunStats = true
		
	--Partial.
	else
	
		--Text.
		fnCutscene([[ WD_SetProperty("Show") ]])
		local sString = "WD_SetProperty(\"Append\", \"You got a Health Catalyst![P] Collect " .. iCatalystAgainst .. " more to increase the party's max health!\")"
		fnCutscene(sString)
		fnCutsceneBlocker()
	end
	
-- |[Attack]|
elseif(sCatalystType == "Attack") then
	
	--How many we have.
	local iCatalystCount = AdInv_GetProperty("Catalyst Count", gciCatalyst_Attack)
	AdInv_SetProperty("Catalyst Count", gciCatalyst_Attack, iCatalystCount + 1)
	
	--Update the DataLibrary version.
	VM_SetVar("Root/Variables/Global/Catalysts/iAttack", "N", iCatalystCount + 1)
    
    --Update local counts.
    local iLocalCurrent = AdInv_GetProperty("Catalyst Local Cur", gciCatalyst_Attack)
    AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Attack, iLocalCurrent + 1)
	
	--Modulus.
	local iCatalystModulus = (iCatalystCount + 1) % gciCatalyst_Attack_Needed
	local iCatalystAgainst = gciCatalyst_Attack_Needed - iCatalystModulus
	
	--Full!
	if(iCatalystAgainst == gciCatalyst_Attack_Needed) then
		
		--Text.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("You completed an Attack Catalyst![P] The entire party's attack power increased by 3!") ]])
		fnCutsceneBlocker()
		
		--Refresh stats.
		bRerunStats = true
		
	--Partial.
	else
	
		--Text.
		fnCutscene([[ WD_SetProperty("Show") ]])
		local sString = "WD_SetProperty(\"Append\", \"You got an Attack Catalyst![P] Collect " .. iCatalystAgainst .. " more to increase the party's attack power!\")"
		fnCutscene(sString)
		fnCutsceneBlocker()
	end
	
-- |[Initiative]|
elseif(sCatalystType == "Initiative") then
	
	--How many we have.
	local iCatalystCount = AdInv_GetProperty("Catalyst Count", gciCatalyst_Initiative)
	AdInv_SetProperty("Catalyst Count", gciCatalyst_Initiative, iCatalystCount + 1)
	
	--Update the DataLibrary version.
	VM_SetVar("Root/Variables/Global/Catalysts/iInitiative", "N", iCatalystCount + 1)
    
    --Update local counts.
    local iLocalCurrent = AdInv_GetProperty("Catalyst Local Cur", gciCatalyst_Initiative)
    AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Initiative, iLocalCurrent + 1)
	
	--Modulus.
	local iCatalystModulus = (iCatalystCount + 1) % gciCatalyst_Initiative_Needed
	local iCatalystAgainst = gciCatalyst_Initiative_Needed - iCatalystModulus
	
	--Full!
	if(iCatalystAgainst == gciCatalyst_Initiative_Needed) then
		
		--Text.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("You completed an Initiative Catalyst![P] The entire party's combat initiative increased by 4!") ]])
		fnCutsceneBlocker()
		
		--Refresh stats.
		bRerunStats = true
		
	--Partial.
	else
	
		--Text.
		fnCutscene([[ WD_SetProperty("Show") ]])
		local sString = "WD_SetProperty(\"Append\", \"You got an Initiative Catalyst![P] Collect " .. iCatalystAgainst .. " more to increase the party's combat initiative!\")"
		fnCutscene(sString)
		fnCutsceneBlocker()
	end
	
-- |[Evade]|
elseif(sCatalystType == "Evade") then
	
	--How many we have.
	local iCatalystCount = AdInv_GetProperty("Catalyst Count", gciCatalyst_Evade)
	AdInv_SetProperty("Catalyst Count", gciCatalyst_Evade, iCatalystCount + 1)
	
	--Update the DataLibrary version.
	VM_SetVar("Root/Variables/Global/Catalysts/iEvade", "N", iCatalystCount + 1)
    
    --Update local counts.
    local iLocalCurrent = AdInv_GetProperty("Catalyst Local Cur", gciCatalyst_Evade)
    AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Evade, iLocalCurrent + 1)
	
	--Modulus.
	local iCatalystModulus = (iCatalystCount + 1) % gciCatalyst_Evade_Needed
	local iCatalystAgainst = gciCatalyst_Evade_Needed - iCatalystModulus
	
	--Full!
	if(iCatalystAgainst == gciCatalyst_Evade_Needed) then
		
		--Text.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("You completed an Evasion Catalyst![P] The entire party's evade rate increased by 3!") ]])
		fnCutsceneBlocker()
		
		--Refresh stats.
		bRerunStats = true
		
	--Partial.
	else
	
		--Text.
		fnCutscene([[ WD_SetProperty("Show") ]])
		local sString = "WD_SetProperty(\"Append\", \"You got an Evasion Catalyst![P] Collect " .. iCatalystAgainst .. " more to increase the party's evade rate!\")"
		fnCutscene(sString)
		fnCutsceneBlocker()
	end
	
-- |[Accuracy]|
elseif(sCatalystType == "Accuracy") then
	
	--How many we have.
	local iCatalystCount = AdInv_GetProperty("Catalyst Count", gciCatalyst_Accuracy)
	AdInv_SetProperty("Catalyst Count", gciCatalyst_Accuracy, iCatalystCount + 1)
	
	--Update the DataLibrary version.
	VM_SetVar("Root/Variables/Global/Catalysts/iAccuracy", "N", iCatalystCount + 1)
    
    --Update local counts.
    local iLocalCurrent = AdInv_GetProperty("Catalyst Local Cur", gciCatalyst_Accuracy)
    AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Accuracy, iLocalCurrent + 1)
	
	--Modulus.
	local iCatalystModulus = (iCatalystCount + 1) % gciCatalyst_Accuracy_Needed
	local iCatalystAgainst = gciCatalyst_Accuracy_Needed - iCatalystModulus
	
	--Full!
	if(iCatalystAgainst == gciCatalyst_Accuracy_Needed) then
		
		--Text.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("You completed an Accuracy Catalyst![P] The entire party's accuracy increased by 3!") ]])
		fnCutsceneBlocker()
		
		--Refresh stats.
		bRerunStats = true
		
	--Partial.
	else
	
		--Text.
		fnCutscene([[ WD_SetProperty("Show") ]])
		local sString = "WD_SetProperty(\"Append\", \"You got an Accuracy Catalyst![P] Collect " .. iCatalystAgainst .. " more to increase the party's accuracy!\")"
		fnCutscene(sString)
		fnCutsceneBlocker()
	end
	
--Skill type:
elseif(sCatalystType == "Skill") then
	
	--How many we have.
	local iCatalystCount = AdInv_GetProperty("Catalyst Count", gciCatalyst_Skill)
	AdInv_SetProperty("Catalyst Count", gciCatalyst_Skill, iCatalystCount + 1)
	
	--Update the DataLibrary version.
	VM_SetVar("Root/Variables/Global/Catalysts/iSkill", "N", iCatalystCount + 1)
    
    --Update local counts.
    local iLocalCurrent = AdInv_GetProperty("Catalyst Local Cur", gciCatalyst_Skill)
    AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Skill, iLocalCurrent + 1)
	
	--Modulus.
	local iCatalystModulus = (iCatalystCount + 1) % gciCatalyst_Skill_Needed
	local iCatalystAgainst = gciCatalyst_Skill_Needed - iCatalystModulus
	
	--Full!
	if(iCatalystAgainst == gciCatalyst_Skill_Needed) then
		
		--Text.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("You completed a Skill Catalyst![P] You now have another slot to equip skills in!") ]])
		fnCutsceneBlocker()
		
		--Refresh stats.
		bRerunStats = true
        
        --Set.
        local iCount = math.floor((iCatalystCount+1) / gciCatalyst_Skill_Needed)
        AdvCombat_SetProperty("Set Skill Catalyst Slots", iCount)
		
	--Partial.
	else
	
		--Text.
		fnCutscene([[ WD_SetProperty("Show") ]])
		local sString = "WD_SetProperty(\"Append\", \"You got a Skill Catalyst![P] Collect " .. iCatalystAgainst .. " more to gain a new slot to equip skills in!\")"
		fnCutscene(sString)
		fnCutsceneBlocker()
	end
end

-- |[Animation Cleanup]|
--Disappear the health catalyst sprite.
fnCutsceneTeleport("CatalystAnim", -100, -100)
fnCutsceneBlocker()

--Order the catalyst to self-destruct.
fnCutscene([[ 
	EM_PushEntity("CatalystAnim")
		RE_SetDestruct(true)
	DL_PopActiveObject()
]])

-- |[Stat Computation]|
--If flagged, re-run the form handlers to update stats.
if(bRerunStats == true) then
	
    --Run across all roster members.
    local iRosterSize = AdvCombat_GetProperty("Roster Size")
    for i = 0, iRosterSize-1, 1 do
        AdvCombat_SetProperty("Push Party Member By Slot", i)
            AdvCombatEntity_SetProperty("Compute Level Statistics", -1)
        DL_PopActiveObject()
    end
end

-- |[Indicator Check]|
fnCutscene([[ AL_SetProperty("Rerun Catalyst Check") ]])
