-- |[ ===================================== fnCheckAutoWin ===================================== ]|
--When an enemy is mugged, the combat handler runs this routine. The routine then determines if the
-- player would win an auto-combat against the enemies present in the group, disregarding reinforcements
-- past 2 turns. If so, it calls a function informing the engine of an auto-win, rewarding full XP/cash
-- to the party and defeating all related enemies.
--To compute an auto-win, the game checks that the player's party a) outnumbers or matches the enemy
-- numerically and b) the player's party is 5 or more levels above the advisory level for the 
-- highest enemy in the group. In addition, enemies may have a flag that blocks auto-wins.
--The script itself is called to check the auto-win, but the function is only assembled on the first
-- pass as a global.
if(fnCheckAutoWin ~= nil) then
    fnCheckAutoWin()
    return
end


-- |[ =================================== Function Definition ================================== ]|
function fnCheckAutoWin()
    
    -- |[Variables]|
    --Get variables from the combat handler.
    local iEnemyPartySize = AdvCombat_GetProperty("Enemy Party Size")
    local iPlayerPartySize = AdvCombat_GetProperty("Active Party Size")
    
    -- |[No-Auto-Win Flag]|
    --If any one enemy has this flag, regardless of level, stop here.
    local bBlockAutoWin = AdvCombat_GetProperty("Do Any Enemies Block Auto Win")
    if(bBlockAutoWin) then
        --io.write("Has block auto win.\n")
        return
    end
    
    -- |[Party Numeracy]|
    --If the party is outnumbered, they never auto-win.
    --io.write("Checking party sizes: " .. iPlayerPartySize .. " versus " .. iEnemyPartySize .. "\n")
    if(iPlayerPartySize < iEnemyPartySize) then 
        --io.write("Failed, player party is outnumbered.\n")
        return 
    end
    
    -- |[Level Check]|
    --Find the highest enemy level, and compare it to the lowest player party level.
    local iHighestEnemyLevel = 0
    local iLowestPlayerLevel = 100
    
    --Scan enemy party.
    for i = 1, iEnemyPartySize, 1 do
        local iEnemyID = AdvCombat_GetProperty("Enemy ID", i-1)
        AdvCombat_SetProperty("Push Entity By ID", iEnemyID)
            local iLevel = AdvCombatEntity_GetProperty("Mug Level For Auto Win")
            if(iLevel > iHighestEnemyLevel) then
                iHighestEnemyLevel = iLevel
            end
        DL_PopActiveObject()
    end
    
    --Scan player party.
    for i = 1, iPlayerPartySize, 1 do
        AdvCombat_SetProperty("Push Active Party Member By Slot", i-1)
            local iLevel = AdvCombatEntity_GetProperty("Level")
            if(iLevel < iLowestPlayerLevel) then
                iLowestPlayerLevel = iLevel
            end
        DL_PopActiveObject()
    end
    
    --Check.
    --io.write("Checking party levels.\n")
    --io.write(" Party lowest: " .. iLowestPlayerLevel .. "\n")
    --io.write(" Enemy highest: " .. iHighestEnemyLevel .. "\n")
    if(iLowestPlayerLevel < iHighestEnemyLevel + 5) then
        --io.write("Failed. Player party is not 5 levels above highest enemy level.\n")
        return
    end
    
    -- |[Pass]|
    --The party auto-wins.
    --io.write("Tripping auto win.\n")
    AdvCombat_SetProperty("Trip Auto Win")
    
    -- |[KO Counting]|
    --Get KO tracker path.
    local sKOTrackerPath = VM_GetVar("Root/Variables/Global/Combat/sKOTrackerPath", "S")
    
    --Add one for each entity present in the group.
    for i = 1, iEnemyPartySize, 1 do
        local iEnemyID = AdvCombat_GetProperty("Enemy ID", i-1)
        AdvCombat_SetProperty("Push Entity By ID", iEnemyID)
        
            -- |[Individual KO Tracking]|
            --Add one to individual KO count for this enemy.
            local sClusterName = AdvCombatEntity_GetProperty("Cluster Name")
            local iKOsSoFar = VM_GetVar(sKOTrackerPath .. sClusterName, "N")
            VM_SetVar(sKOTrackerPath .. sClusterName, "N", iKOsSoFar + 1)
            
            --Special: Award achievements for specific enemy names.
            fnCheckDefeatedEnemyAchievements(sClusterName)
        
            -- |[Paragon Group]|
            --Get the associated paragon grouping.
            local sParagonGrouping = AdvCombatEntity_GetProperty("Paragon Grouping")
            
            --If the grouping starts with ALWAYS, this is the unique paragon.
            local bIsUniqueParagon = false
            if(string.sub(sParagonGrouping, 1, 6) == "ALWAYS") then
                bIsUniqueParagon = true
                sParagonGrouping = string.sub(sParagonGrouping, 8)
            end
            
            --Locate the grouping and get its values.
            local iKillCount          = VM_GetVar("Root/Variables/Paragons/"..sParagonGrouping.."/iKillCount", "N")
            local iHasDefeatedParagon = VM_GetVar("Root/Variables/Paragons/"..sParagonGrouping.."/iHasDefeatedParagon", "N")
            
            --Increment kills by 1.
            VM_SetVar("Root/Variables/Paragons/"..sParagonGrouping.."/iKillCount", "N", iKillCount + 1)
        
            --If this was the unique paragon, mark it as killed.
            if(bIsUniqueParagon) then
                VM_SetVar("Root/Variables/Paragons/"..sParagonGrouping.."/iHasDefeatedParagon", "N", 1.0)
            end
        
        DL_PopActiveObject()
    end
    
end

--Call.
fnCheckAutoWin()