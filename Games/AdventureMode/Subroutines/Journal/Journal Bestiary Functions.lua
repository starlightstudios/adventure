-- |[ =================================== Bestiary Functions =================================== ]|
--Functions used by the journal's bestiary subsection. Most automation of lists and descriptions.
    
-- |[ ==================================== fnLocateEntry() ===================================== ]|
--Locates an entry in the given statistics chart.
function fnLocateEntry(psName, pzaChart)
    
    --Arg check.
    if(psName   == nil) then return nil end
    if(pzaChart == nil) then return nil end
    
    --Scan.
    for i = 1, #pzaChart, 1 do
        if(pzaChart[i].sActualName == psName) then
            return pzaChart[i]
        end
    end
    
    --Not found.
    return nil
end

-- |[ ================================ fnCreateBestiaryEntry() ================================= ]|
--Creates a normal bestiary entry given some basic information.
function fnCreateBestiaryEntry(pzaEnemyStatChart, psInternalName, psChartName, pbAlwaysShow, psDisplayName, psPortrait, psParagon, pfPortraitX, pfPortraitY, psTurnIco, pbCanTFPlayer, psaDescriptionLines)

    -- |[Argument Check]|
    if(pzaEnemyStatChart   == nil) then return end
    if(psInternalName      == nil) then return end
    if(psChartName         == nil) then return end
    if(pbAlwaysShow        == nil) then return end
    if(psDisplayName       == nil) then return end
    if(psPortrait          == nil) then return end
    if(psParagon           == nil) then return end
    if(pfPortraitX         == nil) then return end
    if(pfPortraitY         == nil) then return end
    if(pbCanTFPlayer       == nil) then return end
    if(psaDescriptionLines == nil) then return end

    -- |[Setup]|
    --Flags.
    local iAlwaysShowStats = VM_GetVar("Root/Variables/Global/Debug/iAlwaysShowStats", "N")
    
    --Debug flag.
    local bShowAllEntries = AM_GetPropertyJournal("Show All Entries")
    if(bShowAllEntries) then 
        pbAlwaysShow = true
        iAlwaysShowStats = 1
    end
    
    -- |[Visiblity]|
    --Huntable track. Doesn't always exist, but if it does it can reveal an entry even with zero KO's.
    local sHuntTrackerPath = VM_GetVar("Root/Variables/Global/Combat/sHuntTrackerPath", "S")
    local iHuntCount = VM_GetVar(sHuntTrackerPath .. psInternalName, "N")
    
    --Get the KO count. If the count is zero, and not flagged to always show, then don't create an entry (unless a hunt has occurred).
    local sKOTrackerPath = VM_GetVar("Root/Variables/Global/Combat/sKOTrackerPath", "S")
    local iKOCount = VM_GetVar(sKOTrackerPath .. psInternalName, "N")
    
    --No KO's, no hunts.
    if(iKOCount < 1 and iHuntCount < 1 and pbAlwaysShow == false) then
        AM_SetPropertyJournal("Add Bestiary Entry", psInternalName, "-----")
        return
    end
    
    --KO for the paragon variant, if it exists.
    local iParagonKOCount = VM_GetVar(sKOTrackerPath .. psInternalName .. " Paragon", "N")
    
    -- |[Basic Display]|
    --Display.
    AM_SetPropertyJournal("Add Bestiary Entry",             psInternalName, psDisplayName)
    AM_SetPropertyJournal("Set Bestiary Image",             psInternalName, psPortrait, pfPortraitX, pfPortraitY)
    AM_SetPropertyJournal("Set Bestiary Paragon",           psInternalName, psParagon,  pfPortraitX, pfPortraitY)
    AM_SetPropertyJournal("Set Bestiary Turn Ico",          psInternalName, psTurnIco)
    AM_SetPropertyJournal("Set Bestiary Defeat Count",      psInternalName, iKOCount)
    AM_SetPropertyJournal("Set Bestiary Defeat Paragon",    psInternalName, iParagonKOCount)
    AM_SetPropertyJournal("Set Bestiary Defeat Max",        psInternalName, 0)
    AM_SetPropertyJournal("Set Bestiary Can Transform You", psInternalName, pbCanTFPlayer)
    
    --Description.
    AM_SetPropertyJournal("Set Bestiary Description Auto", psInternalName, psaDescriptionLines[1])
    --AM_SetPropertyJournal("Allocate Bestiary Description Lines", psInternalName, #psaDescriptionLines)
    --for i = 1, #psaDescriptionLines, 1 do
    --    AM_SetPropertyJournal("Set Bestiary Description Line", psInternalName, i-1, psaDescriptionLines[i])
    --end

    -- |[Chart Statistics]|
    --Get the chart entry. If not found, stop here.
    local zEnt = fnLocateEntry(psChartName, pzaEnemyStatChart)
    if(zEnt == nil) then return end
    
    --Combat results.
    AM_SetPropertyJournal("Set Bestiary Results", psInternalName, zEnt.iExp, zEnt.iPlatina)
    
    --Determine highest KOs needed to show all stats.
    local iHighestKOs = zEnt.iKOForStats
    if(zEnt.iKOForResists > iHighestKOs) then iHighestKOs = zEnt.iKOForResists end
    AM_SetPropertyJournal("Set Bestiary Defeat Max", psInternalName, iHighestKOs)

    --Determine if we should print statistics/resistances.
    local bPrintStats   = (iAlwaysShowStats == 1) or (zEnt.iKOForStats   ~= -1 and iKOCount >= zEnt.iKOForStats)
    local bPrintResists = (iAlwaysShowStats == 1) or (zEnt.iKOForResists ~= -1 and iKOCount >= zEnt.iKOForResists)
    
    --Level. Always shows.
    AM_SetPropertyJournal("Set Bestiary Level", psInternalName, zEnt.iLevel)
    
    --Statistics. Shows if the player has enough KOs.
    if(bPrintStats) then
        AM_SetPropertyJournal("Set Bestiary Statistics", psInternalName, zEnt.iHealth, zEnt.iAtk, zEnt.iIni, zEnt.iAcc, zEnt.iEvd, zEnt.iPrt)
    end
    
    --Resistances. Shows if the player has enough KOs.
    if(bPrintResists) then
        AM_SetPropertyJournal("Set Bestiary Resistances", psInternalName, zEnt.iSls, zEnt.iStk, zEnt.iPrc, zEnt.iFlm, zEnt.iFrz, zEnt.iShk, zEnt.iCru, zEnt.iObs, zEnt.iBld, zEnt.iPsn, zEnt.iCrd, zEnt.iTrf)
    end
end
    
-- |[ ========================== fnCreateBestiaryEntryFromListEntry() ========================== ]|
--Creates a normal bestiary entry using a prefabricated list entry.
function fnCreateBestiaryEntryList(pzaEnemyStatChart, pzaListEntry)
    local x = pzaListEntry
    fnCreateBestiaryEntry(pzaEnemyStatChart, x.sInternalName, x.sChartName, x.bAlwaysShow, x.sDisplayName, x.sPortrait, x.sParagon, x.fPortraitX, x.fPortraitY, x.sTurnIco, x.bCanTFPlayer, x.saDescription)
end

-- |[ ================================ fnCreateBestiaryStorage() =============================== ]|
--Function for base entry. Used to add a raw element to the storage list.
function fnCreateBestiaryStorage(pzaList, psInternalName, psChartName, pbAlwaysShow, psDisplayName, psPortrait, psParagon, pfPortraitX, pfPortraitY, psTurnIco, pbCanTFPlayer)
    
    --Arg check.
    if(pzaList        == nil) then return end
    if(psInternalName == nil) then return end
    if(psChartName    == nil) then return end
    if(pbAlwaysShow   == nil) then return end
    if(psDisplayName  == nil) then return end
    if(psPortrait     == nil) then return end
    if(psParagon      == nil) then return end
    if(pfPortraitX    == nil) then return end
    if(pfPortraitY    == nil) then return end
    if(psTurnIco      == nil) then return end
    if(pbCanTFPlayer  == nil) then return end
    
    --New entry.
    local i = #pzaList + 1
    pzaList[i] = {}
    
    --Set.
    pzaList[i].sInternalName = psInternalName
    pzaList[i].sChartName    = psChartName
    pzaList[i].bAlwaysShow   = pbAlwaysShow
    pzaList[i].sDisplayName  = psDisplayName
    pzaList[i].sPortrait     = psPortrait
    pzaList[i].sParagon      = psParagon
    pzaList[i].fPortraitX    = pfPortraitX
    pzaList[i].fPortraitY    = pfPortraitY
    pzaList[i].sTurnIco      = psTurnIco
    pzaList[i].bCanTFPlayer  = pbCanTFPlayer
    pzaList[i].saDescription = {""}
    
end

-- |[ ============================= fnCreateBestiaryStorageChart() ============================= ]|
--Function for entry, using the storage chart provided.
function fnCreateBestiaryStorageChart(pzaChart, pzaList, psInternalName, psChartName, pbAlwaysShow, psDisplayName, pfPortraitX, pfPortraitY, pbCanTFPlayer)

    --Arg check.
    if(pzaChart       == nil) then return end
    if(pzaList        == nil) then return end
    if(psInternalName == nil) then return end
    if(psChartName    == nil) then return end
    if(pbAlwaysShow   == nil) then return end
    if(psDisplayName  == nil) then return end
    if(pfPortraitX    == nil) then return end
    if(pfPortraitY    == nil) then return end
    if(pbCanTFPlayer  == nil) then return end
    
    --Locate entry.
    local zEntry = fnLocateEntry(psChartName, pzaChart)
    if(zEntry == nil) then return end

    --Extract information from chart.
    local sPortraitPath     = zEntry.sPortraitPath
    local sTurnPortraitPath = zEntry.sTurnPortraitPath
    
    --Get the paragon path.
    local sToSlash = ""
    local iLen = string.len(sPortraitPath)
    for x = iLen, 1, -1 do
        if(string.sub(sPortraitPath, x, x) == "/") then
            sToSlash = string.sub(sPortraitPath, x+1)
            break
        end
    end
    local sParagonPath = "Root/Images/Portraits/Paragon/" .. sToSlash

    --Call.
    fnCreateBestiaryStorage(pzaList, psInternalName, psChartName, pbAlwaysShow, psDisplayName, sPortraitPath, sParagonPath, pfPortraitX, pfPortraitY, sTurnPortraitPath, pbCanTFPlayer)
    
    --Translation File Writing
    if(gfOutfile ~= nil) then
        gfOutfile:write("\"".. psDisplayName .. "\",,,\n")
    end
end

-- |[ =========================== fnSetBestiaryStorageDescription() ============================ ]|
--Function for descriptions. Has a basic markdown handler. Descriptions tend to be long so this
-- pairs with fnCreateBestiaryStorage to avoid making lines unmanageable.
function fnSetBestiaryStorageDescription(pzaList, psInternalName, psDescriptionLine)

    -- |[Argument Check]|
    if(pzaList           == nil) then return end
    if(psInternalName    == nil) then return end
    if(psDescriptionLine == nil) then return end

    -- |[Locate entry]|
    local p = -1
    for i = 1, #pzaList, 1 do
        if(pzaList[i].sInternalName == psInternalName) then
            p = i
            break
        end
    end
    
    --Error check.
    if(p == -1) then return end
    
    -- |[Translation Writing]|
    if(gfOutfile ~= nil) then
        gfOutfile:write("\"" .. psDescriptionLine .. "\",,,\n")
    end
    
    -- |[Setup]|
    local iSkips = 0
    local iCurrentLine = 1
    local sCurrentLine = ""
    pzaList[p].saDescription = {}
    
    -- |[Translation]|
    --If a translation is available, use that.
    psDescriptionLine = Translate(gsTranslationJournal, psDescriptionLine)
    pzaList[p].saDescription[1] = psDescriptionLine
    
    --[=[
    
    -- |[Description Building]|
    local iLen = string.len(psDescriptionLine)
    for i = 1, iLen, 1 do
        
        --Store.
        local sLetter = string.sub(psDescriptionLine, i, i)
        
        --Skip this letter.
        if(iSkips > 0) then
            iSkips = iSkips - 1
        
        --New line.
        elseif(string.sub(psDescriptionLine, i, i+1)) == "\\n" then
            pzaList[p].saDescription[iCurrentLine] = sCurrentLine
            sCurrentLine = ""
            iCurrentLine = iCurrentLine + 1
            iSkips = 1
        
        --New line, tag.
        elseif(string.sub(psDescriptionLine, i, i+3) == "[BR]") then
            pzaList[p].saDescription[iCurrentLine] = sCurrentLine
            sCurrentLine = ""
            iCurrentLine = iCurrentLine + 1
            iSkips = 3
        
        --Copy the letter.
        else
            sCurrentLine = sCurrentLine .. sLetter
        end
    end
    
    --Final line.
    if(sCurrentLine ~= "") then
        pzaList[p].saDescription[iCurrentLine] = sCurrentLine
    end]=]
end
