-- |[ ==================================== Profile Functions =================================== ]|
--Functions used to build profiles in the journal UI.

-- |[ ================================ fnCreateProfileFromList() =============================== ]|
function fnCreateProfileFromList(pzaList, psInternalName)
    
    --Arg check.
    if(pzaList        == nil) then return end
    if(psInternalName == nil) then return end

    --Locate the entry on the list.
    local p = -1
    for i = 1, #pzaList, 1 do
        if(pzaList[i].sInternalName == psInternalName) then
            p = i
            break
        end
    end
    
    --Error check.
    if(p == -1) then return end
    
    --System.
    AM_SetPropertyJournal("Add Profile Entry", pzaList[p].sInternalName, pzaList[p].sDisplayName)
    AM_SetPropertyJournal("Set Profile Image", pzaList[p].sInternalName, pzaList[p].sImagePath, pzaList[p].fOffX, pzaList[p].fOffY)
    
    --If the provided description is "", this is an empty entry.
    if(pzaList[p].saDescription == " ") then
        AM_SetPropertyJournal("Allocate Profile Description Lines", pzaList[p].sInternalName, 1)
        AM_SetPropertyJournal("Set Profile Description Line", pzaList[p].sInternalName, 0, " ")
    
    --Otherwise, use the auto-setter.
    else
        AM_SetPropertyJournal("Set Profile Description Auto", pzaList[p].sInternalName, pzaList[p].saDescription[1])
    end
    
    --[=[
    --Description.
    AM_SetPropertyJournal("Allocate Profile Description Lines", pzaList[p].sInternalName, #pzaList[p].saDescription)
    for i = 1, #pzaList[p].saDescription, 1 do
        AM_SetPropertyJournal("Set Profile Description Line", pzaList[p].sInternalName, i-1, pzaList[p].saDescription[i])
    end
    ]=]
end

-- |[ ================================ fnCreateProfileStorage() ================================ ]|
--Function for entry, using the storage chart provided.
function fnCreateProfileStorage(pzaList, psInternalName, psDisplayName, psImagePath, pfOffX, pfOffY)

    --Arg check.
    if(pzaList        == nil) then return end
    if(psInternalName == nil) then return end
    if(psDisplayName  == nil) then return end
    if(psImagePath    == nil) then return end
    if(pfOffX         == nil) then return end
    if(pfOffY         == nil) then return end

    --New entry.
    local i = #pzaList + 1
    pzaList[i] = {}
    
    --Set.
    pzaList[i].sInternalName = psInternalName
    pzaList[i].sDisplayName  = psDisplayName
    pzaList[i].sImagePath    = psImagePath
    pzaList[i].fOffX         = pfOffX
    pzaList[i].fOffY         = pfOffY
    pzaList[i].saDescription = {""}
end

-- |[ ============================ fnSetProfileStorageDescription() ============================ ]|
--Function for descriptions. Has a basic markdown handler. Descriptions tend to be long so this
-- pairs with fnCreateBestiaryStorage to avoid making lines unmanageable.
function fnSetProfileStorageDescription(pzaList, psInternalName, psDescriptionLine)

    -- |[Argument Check]|
    if(pzaList           == nil) then return end
    if(psInternalName    == nil) then return end
    if(psDescriptionLine == nil) then return end

    -- |[Locate]|
    local p = -1
    for i = 1, #pzaList, 1 do
        if(pzaList[i].sInternalName == psInternalName) then
            p = i
            break
        end
    end
    
    --Error check.
    if(p == -1) then return end
    
    -- |[Setup]|
    local iSkips = 0
    local iCurrentLine = 1
    local sCurrentLine = ""
    pzaList[p].saDescription = {}
    
    -- |[Translation]|
    --If a translation is available, use that.
    psDescriptionLine = Translate(gsTranslationJournal, psDescriptionLine)
    
    --Empty:
    if(psDescriptionLine == "") then psDescriptionLine = " " end
    
    --Store a single line.
    pzaList[p].saDescription[1] = psDescriptionLine
    
    --[=[
    
    -- |[Construct Description]|
    local iLen = string.len(psDescriptionLine)
    for i = 1, iLen, 1 do
        
        --Store.
        local sLetter = string.sub(psDescriptionLine, i, i)
        
        --Skip this letter.
        if(iSkips > 0) then
            iSkips = iSkips - 1
        
        --New line.
        elseif(string.sub(psDescriptionLine, i, i+1) == "\\n") then
            pzaList[p].saDescription[iCurrentLine] = sCurrentLine
            sCurrentLine = ""
            iCurrentLine = iCurrentLine + 1
            iSkips = 1
        
        --New line, tag edition.
        elseif(string.sub(psDescriptionLine, i, i+3) == "[BR]") then
            pzaList[p].saDescription[iCurrentLine] = sCurrentLine
            sCurrentLine = ""
            iCurrentLine = iCurrentLine + 1
            iSkips = 3
        
        --Copy the letter.
        else
            sCurrentLine = sCurrentLine .. sLetter
        end
    end
    
    --Final line.
    if(sCurrentLine ~= "") then
        pzaList[p].saDescription[iCurrentLine] = sCurrentLine
    end]=]
end
