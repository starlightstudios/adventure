-- |[ =================================== Location Functions =================================== ]|
--Functions used to build location entries in the journal UI.

-- |[ ================================= fnCreateLocationPack() ================================= ]|
function fnCreateLocationPack(pzaList, psInternalName, psDisplayName, psImage, pfXOffset, pfYOffset, psDescription)
    
    -- |[Argument Check]|
    if(pzaList        == nil) then return end
    if(psInternalName == nil) then return end
    if(psDisplayName  == nil) then return end
    if(psImage        == nil) then return end
    if(pfXOffset      == nil) then return end
    if(pfYOffset      == nil) then return end
    if(psDescription  == nil) then return end
    
    -- |[New Entry]|
    local i = #pzaList + 1
    pzaList[i] = {}
    
    -- |[Description Translation]|
    --If a translation exists, use it for the description.
    psDescription = Translate(gsTranslationJournal, psDescription)
    
    -- |[Set Properties]|
    pzaList[i].sInternalName = psInternalName
    pzaList[i].sDisplayName = psDisplayName
    pzaList[i].sImagePath = psImage
    pzaList[i].fXOffset = pfXOffset
    pzaList[i].fYOffset = pfYOffset
    pzaList[i].saDescription = fnParseDescriptionToArray(psDescription)
end

-- |[ ================================= fnUploadLocationPack() ================================= ]|
function fnUploadLocationPack(pzPack)
    
    -- |[Argument Check]|
    if(pzPack == nil) then return end
    
    -- |[Basic Properties]|
    AM_SetPropertyJournal("Add Location Entry", pzPack.sInternalName, pzPack.sDisplayName)
    AM_SetPropertyJournal("Set Location Image", pzPack.sInternalName, pzPack.sImagePath)
    AM_SetPropertyJournal("Set Location Image Offsets", pzPack.sInternalName, pzPack.fXOffset, pzPack.fYOffset)
    
    -- |[Description]|
    AM_SetPropertyJournal("Set Location Description Auto", pzPack.sInternalName, pzPack.saDescription[1])
    
    --[=[AM_SetPropertyJournal("Allocate Location Description Lines", pzPack.sInternalName, #pzPack.saDescription)
    for i = 1, #pzPack.saDescription, 1 do
        AM_SetPropertyJournal("Set Location Description Line", pzPack.sInternalName, i-1, pzPack.saDescription[i])
    end]=]
end


-- |[ ================================ fnUploadLocationDashes() ================================ ]|
--Used to indicate an undiscovered area.
function fnUploadLocationDashes(pzPack)
    
    --Arg check.
    if(pzPack == nil) then return end
    
    --Basics.
    AM_SetPropertyJournal("Add Location Entry", pzPack.sInternalName, "-----")
end

