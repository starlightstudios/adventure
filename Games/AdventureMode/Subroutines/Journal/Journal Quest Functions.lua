-- |[ ===================================== Quest Functions ==================================== ]|
--Functions used to build profiles in the quest UI.

-- |[ =============================== fnUploadDescriptionArray() =============================== ]|
--Given a description array, uploads it to the quest entry provided.
function fnUploadDescriptionArray(psQuestName, psaDescription)
    
    --Arg check.
    if(psQuestName    == nil) then return end
    if(psaDescription == nil) then return end
    
    --Use auto-handler.
    AM_SetPropertyJournal("Set Quest Description Auto", psQuestName, psaDescription[1])
    
    --Upload.
    --[=[AM_SetPropertyJournal("Allocate Quest Description Lines", psQuestName, #psaDescription)
    for i = 1, #psaDescription, 1 do
        AM_SetPropertyJournal("Set Quest Description Line", psQuestName, i-1, psaDescription[i])
    end]=]
end
