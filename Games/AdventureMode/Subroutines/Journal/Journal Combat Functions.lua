-- |[ ==================================== Combat Functions ==================================== ]|
--Functions used to build combat entries in the journal glossary.

-- |[ ============================== fnCreateCombatGlossaryPack() ============================== ]|
function fnCreateCombatGlossaryPack(pzaList, psInternalName, psDisplayName, piPieces)
    
    -- |[Argument Check]|
    if(pzaList        == nil) then return end
    if(psInternalName == nil) then return end
    if(psDisplayName  == nil) then return end
    if(piPieces       == nil) then return end
    
    -- |[Apply Translation]|
    psDisplayName = Translate(gsTranslationJournal, psDisplayName)
    
    -- |[Create and Set]|
    local i = #pzaList + 1
    pzaList[i] = {}
    
    --Setup.
    pzaList[i].sInternalName = psInternalName
    pzaList[i].sDisplayName = psDisplayName
    pzaList[i].iPieces = piPieces
    pzaList[i].zaPieces = {}
    for p = 1, piPieces, 1 do
        pzaList[i].zaPieces[p] = {}
        pzaList[i].zaPieces[p].fXPos = 0.0
        pzaList[i].zaPieces[p].fYPos = 0.0
        pzaList[i].zaPieces[p].sImgPath = "Null"
        pzaList[i].zaPieces[p].iFlags = 0
        pzaList[i].zaPieces[p].sFont = "Main"
        pzaList[i].zaPieces[p].saDescription = {}
    end
end

-- |[ ================================ fnSetGlossaryPackImage() ================================ ]|
function fnSetGlossaryPackImage(pzaList, psInternalName, piPiece, pfXPos, pfYPos, psImagePath)
    
    -- |[Argument Check]|
    if(pzaList        == nil) then return end
    if(psInternalName == nil) then return end
    if(piPiece        == nil) then return end
    if(pfXPos         == nil) then return end
    if(pfYPos         == nil) then return end
    if(psImagePath    == nil) then return end
    
    -- |[Locate Entry]|
    local i = -1
    for p = 1, #pzaList, 1 do
        if(pzaList[p].sInternalName == psInternalName) then
            i = p
            break
        end
    end
    if(i == -1) then return end
    
    --Range check:
    piPiece = piPiece + 1
    if(piPiece < 1 or piPiece > pzaList[i].iPieces) then return end
    
    -- |[Set]|
    pzaList[i].zaPieces[piPiece].fXPos = pfXPos
    pzaList[i].zaPieces[piPiece].fYPos = pfYPos
    pzaList[i].zaPieces[piPiece].sImgPath = psImagePath
end

-- |[ ============================= fnSetGlossaryPackDescription() ============================= ]|
function fnSetGlossaryPackDescription(pzaList, psInternalName, piPiece, pfXPos, pfYPos, piFlags, psDescription, psFont)
    
    -- |[Argument Check]|
    if(pzaList        == nil) then return end
    if(psInternalName == nil) then return end
    if(piPiece        == nil) then return end
    if(pfXPos         == nil) then return end
    if(pfYPos         == nil) then return end
    if(piFlags        == nil) then return end
    if(psDescription  == nil) then return end
    if(psFont         == nil) then return end
    
    -- |[Locate Entry]|
    local i = -1
    for p = 1, #pzaList, 1 do
        if(pzaList[p].sInternalName == psInternalName) then
            i = p
            break
        end
    end
    if(i == -1) then return end
    
    --Range check:
    piPiece = piPiece + 1
    if(piPiece < 1 or piPiece > pzaList[i].iPieces) then return end
    
    -- |[Apply Translation]|
    psDescription = Translate(gsTranslationJournal, psDescription)
    
    --Change [PCT] to %'s
    --psDescription = string.gsub(psDescription, "%[PCT%]", "%%")
    
    -- |[Set]|
    pzaList[i].zaPieces[piPiece].fXPos = pfXPos
    pzaList[i].zaPieces[piPiece].fYPos = pfYPos
    pzaList[i].zaPieces[piPiece].iFlags = piFlags
    pzaList[i].zaPieces[piPiece].saDescription = fnParseDescriptionToArray(psDescription)
    pzaList[i].zaPieces[piPiece].sFont = psFont
end

-- |[ ================================= fnUploadGlossaryEntry() ================================ ]|
function fnUploadGlossaryEntry(pzPack)
    
    --Arg check.
    if(pzPack == nil) then return end
    
    --System.
    AM_SetPropertyJournal("Add Combat Glossary Entry", pzPack.sInternalName, pzPack.sDisplayName)
    AM_SetPropertyJournal("Allocate Combat Glossary Pieces", pzPack.sInternalName, pzPack.iPieces)
    
    --Pieces:
    for i = 1, #pzPack.zaPieces, 1 do
        if(pzPack.zaPieces[i].sImgPath ~= "Null") then
            AM_SetPropertyJournal("Set Combat Glossary Image", pzPack.sInternalName, i-1, pzPack.zaPieces[i].sImgPath, pzPack.zaPieces[i].fXPos, pzPack.zaPieces[i].fYPos)
        end
        
        if(pzPack.zaPieces[i].saDescription[1] ~= nil) then
            --AM_SetPropertyJournal("Allocate Combat Glossary Text Lines", pzPack.sInternalName, i-1, #pzPack.zaPieces[i].saDescription, pzPack.zaPieces[i].fXPos, pzPack.zaPieces[i].fYPos)
            AM_SetPropertyJournal("Set Combat Glossary Text Auto", pzPack.sInternalName, i-1, pzPack.zaPieces[i].fXPos, pzPack.zaPieces[i].fYPos, pzPack.zaPieces[i].saDescription[1], pzPack.zaPieces[i].sFont, pzPack.zaPieces[i].iFlags)
            --for p = 1, #pzPack.zaPieces[i].saDescription, 1 do
            --    AM_SetPropertyJournal("Set Combat Glossary Text", pzPack.sInternalName, i-1, p-1, pzPack.zaPieces[i].saDescription[p], pzPack.zaPieces[i].sFont, pzPack.zaPieces[i].iFlags)
            --end
        end
    end
end
