-- |[ ============================ fnSwitchItemTypesForChristine() ============================= ]|
--Switches all items in the inventory that have a type containing specific strings for Christine.
-- This should be called when finishing Cryogenics, as it basically renames existing types for
-- items in the inventory.
function fnSwitchItemTypesForChristine()

    -- |[Activation Check]|
    --If Christine's item list is somehow not ready, stop.
    if(gzChristineItemChart == nil) then return end

    -- |[Items in Inventory]|
    local iTotalItems = AdInv_GetProperty("Total Items Unequipped")
    for i = 0, iTotalItems-1, 1 do
        AdInv_PushItemI(i)
            fnHandleTypeResetForActiveItem()
        DL_PopActiveObject()
    end
    
    -- |[Equipment]|
    AdvCombat_SetProperty("Push Party Member", "Christine")
        local iTotalEquipSlots = AdvCombatEntity_GetProperty("Total Equipment Slots")
        for i = 0, iTotalEquipSlots-1, 1 do
            
            --Check that the slot has something in it.
            local sEquipmentName = AdvCombatEntity_GetProperty("Equipment In Slot I", i)
            if(sEquipmentName ~= "Null") then
                AdvCombatEntity_SetProperty("Push Item In Slot I", i)
                    fnHandleTypeResetForActiveItem()
                DL_PopActiveObject()
            end
        end
    DL_PopActiveObject()
end

--Worker function that works on the activity stacks's active item. This allows it to operate on
-- inventory items as well as equipment on a character.
function fnHandleTypeResetForActiveItem()
    
    -- |[Setup]|
    --Check if Christine has finished Cryogenics. If not, switch the name to "Chris". This only
    -- refers to typing on some items, not the usability flags.
    local sUseCharacterName = "Christine"
    local iIsPostGolemScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsPostGolemScene", "N")
    local iChristineCorruptedEnding = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineCorruptedEnding", "N")
    if(iIsPostGolemScene == 0) then sUseCharacterName = "Chris" end
    if(iChristineCorruptedEnding == 1) then sUseCharacterName = "Unit 771852" end
    
    -- |[Locate Entry]|
    --Get name.
    local sItemName = AdItem_GetProperty("Name")
    
    --Locate the index on the lookup for the item. If not found, then this item is not one of
    -- Christine's items and doesn't need to be modified.
    local zItemEntry = nil
    for i = 1, #gzChristineItemChart, 1 do
        if(gzChristineItemChart[i].sName == sItemName) then
            zItemEntry = gzChristineItemChart[i]
            break
        end
    end
    
    --Not found, stop here.
    if(zItemEntry == nil) then return end
    
    -- |[Modify Description]|
    --Reset the description back to the base.
    AdItem_SetProperty("Description", zItemEntry.sDescription)
    
    --Modify type by code. Armors:
    if(zItemEntry.iUsability == gciArmorCode) then
        AdItem_SetProperty("Type", "Armor (" .. sUseCharacterName .. ")")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Armor, gci_UI_AdItem_Max_Description_Lines)
    
    --Accessories:
    elseif(zItemEntry.iUsability == gciAccessoryCode) then
        AdItem_SetProperty("Type", "Accessory (" .. sUseCharacterName .. ")")
        fnStandardizeDescription(gcs_UI_DescriptionStd_Accessory, gci_UI_AdItem_Max_Description_Lines)
    
    --Combat Items:
    elseif(zItemEntry.iUsability == gciItemCode) then
        AdItem_SetProperty("Type", "Combat Item (" .. sUseCharacterName .. ")")
        fnStandardizeDescription(gcs_UI_DescriptionStd_CombatItem, gci_UI_AdItem_Max_Description_Lines)
    end
end