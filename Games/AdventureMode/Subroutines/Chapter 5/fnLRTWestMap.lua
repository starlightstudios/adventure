-- |[ ====================================== fnLRTWestMap ====================================== ]|
--The maps in chapter 5 use fixed configurations. This sets that for the western LRT facility.
function fnLRTWestMap(piPlayerX, piPlayerY, piRemapX, piRemapY)
    
    -- |[Argument Check]|
    if(piPlayerX == nil) then return end
    if(piPlayerY == nil) then return end
    if(piRemapX  == nil) then return end
    if(piRemapY  == nil) then return end
    
    -- |[Basic Properties]|
    --Set properties.
    AM_SetMapInfo("No Special Map", "Null", piPlayerX, piPlayerY)
    AM_SetProperty("Clear Advanced Map")
    AM_SetProperty("Map Edge Pad", 150)
    
    -- |[Layers]|
    AM_SetProperty("Create Advanced Map Layer", "Layer0", "Root/Images/AdvMaps/LRT/LRTWest0")
    
    AM_SetProperty("Create Advanced Map Layer", "Layer1", "Root/Images/AdvMaps/LRT/LRTWest1")
    AM_SetProperty("Layer Render Chance",       "Layer1", 0, 10)
    
    AM_SetProperty("Create Advanced Map Layer", "Layer2", "Root/Images/AdvMaps/LRT/LRTWest2")
    AM_SetProperty("Layer Render Chance",       "Layer2", 11, 20)
    
    AM_SetProperty("Create Advanced Map Layer", "Layer3", "Root/Images/AdvMaps/LRT/LRTWest3")
    AM_SetProperty("Layer Render Chance",       "Layer3", 21, 30)

    AM_SetProperty("Create Advanced Map Layer", "Layer4", "Root/Images/AdvMaps/LRT/LRTWest5")
    AM_SetProperty("Layer Render Chance",       "Layer4", 31, 40)
    
    AM_SetProperty("Create Advanced Map Layer", "Layer5", "Root/Images/AdvMaps/LRT/LRTWest4")
    AM_SetProperty("Layer Render Chance",       "Layer5", 51, 1000)
    
    AM_SetProperty("Create Advanced Map Layer", "LayerP", "Null")
    AM_SetProperty("Layer Renders Player",      "LayerP", "Null", false, 0, 0)
    
    AM_SetProperty("Create Advanced Map Layer", "LayerO", "Root/Images/AdvMaps/General/PDUFrame")
    AM_SetProperty("Layer Is Fixed",            "LayerO", true)
    
    -- |[Map Pins]|
    --Variables.
    local iTalosShowHealth     = VM_GetVar("Root/Variables/Chapter5/Talos/iTalosShow_Health", "N")
    local iTalosShowPower      = VM_GetVar("Root/Variables/Chapter5/Talos/iTalosShow_Power", "N")
    local iTalosShowInitiative = VM_GetVar("Root/Variables/Chapter5/Talos/iTalosShow_Initiative", "N")
    local iTalosShowAccuracy   = VM_GetVar("Root/Variables/Chapter5/Talos/iTalosShow_Accuracy", "N")
    local iTalosShowEvade      = VM_GetVar("Root/Variables/Chapter5/Talos/iTalosShow_Evasion", "N")
    local iTalosShowSkills     = VM_GetVar("Root/Variables/Chapter5/Talos/iTalosShow_Skills", "N")
    
    --Catalysts.
    if(iTalosShowHealth == 1.0) then
        if(DL_Exists("Root/Variables/Chests/RegulusLRTEA/Chest A") == false) then
            AM_SetProperty("Register Map Pin", 2105, 1045, gcsMapPinSet_IcoHlt)
        end
    end

    -- |[Remap Scales]|
    AM_SetProperty("Advanced Map Properties", piRemapX, piRemapY, 70.0 / 96.0, 70.0 / 96.0)

    -- |[Storage]|
    gfnLastMapFunction = fnLRTEastMap
    giLastPlayerX = piPlayerX
    giLastPlayerY = piPlayerY
    giLastRemapX = piRemapX
    giLastRemapY = piRemapY
    
end