-- |[ ====================================== fnEquinoxMap ====================================== ]|
--The maps in chapter 5 use fixed configurations. This sets that for the Equinox facility.
function fnEquinoxMap(piPlayerX, piPlayerY, piRemapX, piRemapY)
    
    -- |[Argument Check]|
    if(piPlayerX == nil) then return end
    if(piPlayerY == nil) then return end
    if(piRemapX  == nil) then return end
    if(piRemapY  == nil) then return end
    
    -- |[Basic Properties]|
    --Set properties.
    AM_SetMapInfo("No Special Map", "Null", piPlayerX, piPlayerY)
    AM_SetProperty("Clear Advanced Map")
    AM_SetProperty("Map Edge Pad", 150)
    
    -- |[Layers]|
    AM_SetProperty("Create Advanced Map Layer", "Layer0", "Root/Images/AdvMaps/Equinox/Equinox0")
    
    AM_SetProperty("Create Advanced Map Layer", "Layer1", "Root/Images/AdvMaps/Equinox/Equinox1")
    AM_SetProperty("Layer Render Chance",       "Layer1", 0, 10)
    
    AM_SetProperty("Create Advanced Map Layer", "Layer2", "Root/Images/AdvMaps/Equinox/Equinox2")
    AM_SetProperty("Layer Render Chance",       "Layer2", 11, 20)
    
    AM_SetProperty("Create Advanced Map Layer", "Layer3", "Root/Images/AdvMaps/Equinox/Equinox3")
    AM_SetProperty("Layer Render Chance",       "Layer3", 21, 30)

    AM_SetProperty("Create Advanced Map Layer", "Layer4", "Root/Images/AdvMaps/Equinox/Equinox5")
    AM_SetProperty("Layer Render Chance",       "Layer4", 31, 40)
    
    AM_SetProperty("Create Advanced Map Layer", "Layer5", "Root/Images/AdvMaps/Equinox/Equinox4")
    AM_SetProperty("Layer Render Chance",       "Layer5", 51, 1000)
    
    AM_SetProperty("Create Advanced Map Layer", "LayerP", "Null")
    AM_SetProperty("Layer Renders Player",      "LayerP", "Null", false, 0, 0)
    
    AM_SetProperty("Create Advanced Map Layer", "LayerO", "Root/Images/AdvMaps/General/PDUFrame")
    AM_SetProperty("Layer Is Fixed",            "LayerO", true)

    -- |[Remap Scales]|
    AM_SetProperty("Advanced Map Properties", piRemapX, piRemapY, 70.0 / 96.0, 70.0 / 96.0)
    
    -- |[Storage]|
    gfnLastMapFunction = fnEquinoxMap
    giLastPlayerX = piPlayerX
    giLastPlayerY = piPlayerY
    giLastRemapX = piRemapX
    giLastRemapY = piRemapY
    
end