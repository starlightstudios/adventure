-- |[ =================================== fnIsSophieLeader() =================================== ]|
--Determines if Sophie is the current party leader. This happens during the Secrebot questline.
-- It it used to check examinable objects and whatnot for variations.
--Returns true if she's the leader, false if not.
function fnIsSophieLeader()
    
    -- |[Variables]|
    local i254SophieLeading  = VM_GetVar("Root/Variables/Chapter5/Scenes/i254SophieLeading", "N")
    local i254FixedChristine = VM_GetVar("Root/Variables/Chapter5/Scenes/i254FixedChristine", "N")
    
    -- |[Case: Secrebot Quest]|
    if(i254SophieLeading == 1.0 and i254FixedChristine == 0.0) then
        return true
    end
    
    -- |[All Checks Failed]|
    return false
end
