-- |[ ========================== fnResolveCommandUnitObjectiveName() =========================== ]|
--During the gala, the objective "1: Identify Prime Command Units" has a trailing number indicating
-- how many the player has identified, the max being 3. This resolves the display name for the objective.
--Returns the appropriate display name.
function fnResolveCommandUnitObjectiveName()
    
    --Get how many of the command units have been found.
    local iGalaMetPrimeA = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeA", "N")
    local iGalaMetPrimeB = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeB", "N")
    local iGalaMetPrimeC = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeC", "N")
    local iCount = math.floor(iGalaMetPrimeA + iGalaMetPrimeB + iGalaMetPrimeC)
    
    --If all three are found, the objective is complete.
    if(iCount >= 3) then
        return "1: Identify Prime Command Units"
    end
    
    --Otherwise, resolve the name.
    local sName = "1: Identify Prime Command Units " .. iCount .. "/3"
    return sName
end
