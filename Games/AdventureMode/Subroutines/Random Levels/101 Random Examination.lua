-- |[ ================================ Random Floor Examinables ================================ ]|
--These are the ladders up and down. This script handles their examination. Some other levels share their properties
-- and thus will call this script.
--The script modifies a variable, gbCaughtScript, if it handled the examination case. It expects the same arguments
-- an examination script usually expects.
--This script's path is stored in the variable gsRandomExaminationHandler.
gbCaughtScript = false

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
--Down to a randomly generated level.
if(sObjectName == "LadderD" or sObjectName == "Exit") then
    
    -- |[Baseline]|
    --Flag to indicate we caught an examine case.
    gbCaughtScript = true
    
	--SFX.
	AudioManager_PlaySound("World|ClimbLadder")
    
	-- |[Execute Script Handler]|
	--Increment the floor.
	local iCurrentMinesFloor = VM_GetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "N")
	VM_SetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "N", iCurrentMinesFloor + 1)
	
	--Call script.
	MapHelper:fnSelectNextLevel(iCurrentMinesFloor+1)
	
--Up ladder.
elseif(sObjectName == "LadderU" or sObjectName == "Entrance") then
	
	--Dialogue.
    gbCaughtScript = true
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] (Climb the ladder back to the surface?)[BLOCK]") ]])

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"No\") ")
	fnCutsceneBlocker()

-- |[Decisions]|
--Really exit the mines.
elseif(sObjectName == "Yes") then

	--Go to Mines B.
    gbCaughtScript = true
	WD_SetProperty("Hide")
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("TelluriumMinesB", "FORCEPOS:19.0x24.0x0")
    
    --When the player has left Mines B after getting the ammo, throw this flag.
    local iMineBGotAmmo      = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineBGotAmmo", "N")
    local iMineBFloor        = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineBFloor", "N")
	local iCurrentMinesFloor = VM_GetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "N")
    if(iCurrentMinesFloor == iMineBFloor and iMineBGotAmmo == 2.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMineBLeft", "N", 1.0)
    end
    
--Nope.
elseif(sObjectName == "No") then
	WD_SetProperty("Hide")
    gbCaughtScript = true
end
