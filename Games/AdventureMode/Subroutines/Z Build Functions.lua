-- |[ ===================================== Build Functions ==================================== ]|
--Builds the functions found in this folder.
local sFunctionPath = gsRoot .. "Subroutines/"

-- |[ ================================ Structures and Formulas ================================= ]|
-- |[ =================================== Function Execution =================================== ]|

-- |[ ========= Base Folder ======== ]|
-- |[ ========= Subfolders ========= ]|
-- |[Achievements]|
gbAchievementDebug = false
LM_ExecuteScript(sFunctionPath .. "Achievements/fnCheckDefeatedEnemyAchievements.lua")
LM_ExecuteScript(sFunctionPath .. "Achievements/fnCheckItemAchievements.lua")
LM_ExecuteScript(sFunctionPath .. "Achievements/fnGetAchievementProgress.lua")
LM_ExecuteScript(sFunctionPath .. "Achievements/fnSetAchievementProgress.lua")

-- |[Campfire Menu]|
-- |[Catalysts]|
-- |[Chapter 1]|
LM_ExecuteScript(sFunctionPath .. "Chapter 1/fnCommonCh1Warp.lua")
LM_ExecuteScript(sFunctionPath .. "Chapter 1/fnHandleTrannadarMap.lua")
LM_ExecuteScript(sFunctionPath .. "Chapter 1/fnIsFlorentinaPresent.lua")
LM_ExecuteScript(sFunctionPath .. "Chapter 1/fnRubberRemover.lua")
LM_ExecuteScript(sFunctionPath .. "Chapter 1/fnStandardCh1Map.lua")

-- |[Chapter 2]|
LM_ExecuteScript(sFunctionPath .. "Chapter 2/fnExtraExtra.lua")
LM_ExecuteScript(sFunctionPath .. "Chapter 2/fnHandleNorthwoodsMap.lua")
LM_ExecuteScript(sFunctionPath .. "Chapter 2/fnHandleMtSarulenteMap.lua")
LM_ExecuteScript(sFunctionPath .. "Chapter 2/fnHandleWestwoodsMap.lua")
LM_ExecuteScript(sFunctionPath .. "Chapter 2/fnRecomputeZekeCosts.lua")
LM_ExecuteScript(sFunctionPath .. "Chapter 2/fnResolveMapVarFromName.lua")

-- |[Chapter 3]|
-- |[Chapter 4]|
-- |[Chapter 5]|
LM_ExecuteScript(sFunctionPath .. "Chapter 5/fnIsSophieLeader.lua")
LM_ExecuteScript(sFunctionPath .. "Chapter 5/fnSetApartmentObjectGraphics.lua")
LM_ExecuteScript(sFunctionPath .. "Chapter 5/fnResolveCommandUnitObjectiveName.lua")
LM_ExecuteScript(sFunctionPath .. "Chapter 5/fnSwitchItemTypesForChristine.lua")

-- |[Chapter 6]|
-- |[Combat]|
-- |[Costumes]|
LM_ExecuteScript(sFunctionPath .. "Costumes/fnGetFormFromCostume.lua")
LM_ExecuteScript(sFunctionPath .. "Costumes/fnGetSlotOnCostumeList.lua")

-- |[Cutscenes]|
LM_ExecuteScript(sFunctionPath .. "Cutscenes/fnAutoFoldParty.lua")
LM_ExecuteScript(sFunctionPath .. "Cutscenes/fnCrawlThroughVent.lua")
LM_ExecuteScript(sFunctionPath .. "Cutscenes/fnCutsceneBlocker.lua")
LM_ExecuteScript(sFunctionPath .. "Cutscenes/fnCutsceneCall.lua")
LM_ExecuteScript(sFunctionPath .. "Cutscenes/fnCutsceneCamera.lua")
LM_ExecuteScript(sFunctionPath .. "Cutscenes/fnCutsceneDoNothing.lua")
LM_ExecuteScript(sFunctionPath .. "Cutscenes/fnCutsceneFace.lua")
LM_ExecuteScript(sFunctionPath .. "Cutscenes/fnCutsceneFacePos.lua")
LM_ExecuteScript(sFunctionPath .. "Cutscenes/fnCutsceneFaceTarget.lua")
LM_ExecuteScript(sFunctionPath .. "Cutscenes/fnCutsceneFadeIn.lua")
LM_ExecuteScript(sFunctionPath .. "Cutscenes/fnCutsceneFadeOut.lua")
LM_ExecuteScript(sFunctionPath .. "Cutscenes/fnCutsceneInstruction.lua")
LM_ExecuteScript(sFunctionPath .. "Cutscenes/fnCutsceneJumpNPC.lua")
LM_ExecuteScript(sFunctionPath .. "Cutscenes/fnCutsceneJumpTo.lua")
LM_ExecuteScript(sFunctionPath .. "Cutscenes/fnCutsceneLayerDisabled.lua")
LM_ExecuteScript(sFunctionPath .. "Cutscenes/fnCutsceneMergeParty.lua")
LM_ExecuteScript(sFunctionPath .. "Cutscenes/fnCutsceneMove.lua")
LM_ExecuteScript(sFunctionPath .. "Cutscenes/fnCutsceneMoveList.lua")
LM_ExecuteScript(sFunctionPath .. "Cutscenes/fnCutsceneMoveAmount.lua")
LM_ExecuteScript(sFunctionPath .. "Cutscenes/fnCutsceneMoveExpirableSprite.lua")
LM_ExecuteScript(sFunctionPath .. "Cutscenes/fnCutsceneMoveFace.lua")
LM_ExecuteScript(sFunctionPath .. "Cutscenes/fnCutsceneSetAnim.lua")
LM_ExecuteScript(sFunctionPath .. "Cutscenes/fnCutsceneSetFrame.lua")
LM_ExecuteScript(sFunctionPath .. "Cutscenes/fnCutsceneSetHeight.lua")
LM_ExecuteScript(sFunctionPath .. "Cutscenes/fnCutsceneSetVar.lua")
LM_ExecuteScript(sFunctionPath .. "Cutscenes/fnCutsceneTeleport.lua")
LM_ExecuteScript(sFunctionPath .. "Cutscenes/fnCutsceneWait.lua")
LM_ExecuteScript(sFunctionPath .. "Cutscenes/fnCutscenePlaySound.lua")
LM_ExecuteScript(sFunctionPath .. "Cutscenes/fnGetEntityPosition.lua")

-- |[Diagnostics]|
LM_ExecuteScript(sFunctionPath .. "Diagnostics/fnAIExec.lua")
LM_ExecuteScript(sFunctionPath .. "Diagnostics/fnCombatExec.lua")
LM_ExecuteScript(sFunctionPath .. "Diagnostics/fnDiagnosticFlag.lua")
LM_ExecuteScript(sFunctionPath .. "Diagnostics/fnSetCutsceneDiagnostics.lua")
LM_ExecuteScript(sFunctionPath .. "Diagnostics/fnWriteInfo.lua")

-- |[Dialogue]|
LM_ExecuteScript(sFunctionPath .. "Dialogue/fnConstructTopic.lua")
LM_ExecuteScript(sFunctionPath .. "Dialogue/fnCreateDialogueActor.lua")
LM_ExecuteScript(sFunctionPath .. "Dialogue/fnDecisionSet.lua")
LM_ExecuteScript(sFunctionPath .. "Dialogue/fnGetTopicState.lua")
LM_ExecuteScript(sFunctionPath .. "Dialogue/fnStandardDialogue.lua")
LM_ExecuteScript(sFunctionPath .. "Dialogue/fnStandardMajorDialogue.lua")

-- |[Field Abilities]|
LM_ExecuteScript(sFunctionPath .. "Field Abilities/fnGetFirstObjectFromPosition.lua")

-- |[Field Entities]|
LM_ExecuteScript(sFunctionPath .. "Field Entities/fnDespawnNPC.lua")
LM_ExecuteScript(sFunctionPath .. "Field Entities/fnGetObjectsInFrontOfPlayer.lua")
LM_ExecuteScript(sFunctionPath .. "Field Entities/fnHasSightBetween.lua")
LM_ExecuteScript(sFunctionPath .. "Field Entities/fnNPCPathToRandomNode.lua")
LM_ExecuteScript(sFunctionPath .. "Field Entities/fnNPCReturnPathToRandomNode.lua")
LM_ExecuteScript(sFunctionPath .. "Field Entities/fnSpawnNPCPattern.lua")
LM_ExecuteScript(sFunctionPath .. "Field Entities/fnSpawnSplash.lua")
LM_ExecuteScript(sFunctionPath .. "Field Entities/fnStandardNPC.lua")
LM_ExecuteScript(sFunctionPath .. "Field Entities/fnStandardNPCByPosition.lua")
LM_ExecuteScript(sFunctionPath .. "Field Entities/fnSetCharacterGraphics.lua")
LM_ExecuteScript(sFunctionPath .. "Field Entities/fnStandardCharacter.lua")
LM_ExecuteScript(sFunctionPath .. "Field Entities/fnStandardEnemyPulse.lua")
LM_ExecuteScript(sFunctionPath .. "Field Entities/fnSpecialCharacter.lua")

-- |[Items]|
LM_ExecuteScript(sFunctionPath .. "Items/fnAddAbilityToChart.lua")
LM_ExecuteScript(sFunctionPath .. "Items/fnAddItemToChart.lua")
LM_ExecuteScript(sFunctionPath .. "Items/fnAddTagToChart.lua")
LM_ExecuteScript(sFunctionPath .. "Items/fnAddWeaponToChart.lua")
LM_ExecuteScript(sFunctionPath .. "Items/fnAddWeaponToChartChristine.lua")
LM_ExecuteScript(sFunctionPath .. "Items/fnIsItemEquipped.lua")
LM_ExecuteScript(sFunctionPath .. "Items/fnSetDescriptionOnChart.lua")
LM_ExecuteScript(sFunctionPath .. "Items/fnSetUsabilityOnChart.lua")
LM_ExecuteScript(sFunctionPath .. "Items/fnStandardizeDescription.lua")

-- |[Journal]|
LM_ExecuteScript(sFunctionPath .. "Journal/Journal Bestiary Functions.lua")
LM_ExecuteScript(sFunctionPath .. "Journal/Journal Profile Functions.lua")
LM_ExecuteScript(sFunctionPath .. "Journal/Journal Quest Functions.lua")
LM_ExecuteScript(sFunctionPath .. "Journal/Journal Location Functions.lua")
LM_ExecuteScript(sFunctionPath .. "Journal/Journal Combat Functions.lua")

-- |[Loading Functions]|
LM_ExecuteScript(sFunctionPath .. "Loading Functions/fnAppendDelayedBitmapToList.lua")
LM_ExecuteScript(sFunctionPath .. "Loading Functions/fnCreateDelayedLoadList.lua")
LM_ExecuteScript(sFunctionPath .. "Loading Functions/fnExecAutoloadCostume.lua")
LM_ExecuteScript(sFunctionPath .. "Loading Functions/fnExecAutoloadDelayed.lua")
LM_ExecuteScript(sFunctionPath .. "Loading Functions/fnExtractDelayedBitmapToList.lua")
LM_ExecuteScript(sFunctionPath .. "Loading Functions/fnExtractSetList.lua")
LM_ExecuteScript(sFunctionPath .. "Loading Functions/fnExtractSpecialFrames.lua")
LM_ExecuteScript(sFunctionPath .. "Loading Functions/fnExtractSpriteList.lua")
LM_ExecuteScript(sFunctionPath .. "Loading Functions/fnExtractSpriteListEW.lua")
LM_ExecuteScript(sFunctionPath .. "Loading Functions/fnLoadDelayedBitmapsFromList.lua")
LM_ExecuteScript(sFunctionPath .. "Loading Functions/fnLoadGrouping.lua")
LM_ExecuteScript(sFunctionPath .. "Loading Functions/fnPrintDelayedBitmapList.lua")
LM_ExecuteScript(sFunctionPath .. "Loading Functions/fnUnloadBitmapsFromList.lua")
LM_ExecuteScript(sFunctionPath .. "Loading Functions/fnUnloadGrouping.lua")
LM_ExecuteScript(sFunctionPath .. "Loading Functions/fnLoadCharacterGraphics.lua")

-- |[Maps]|
LM_ExecuteScript(sFunctionPath .. "Maps/fnStandardLevel.lua")
LM_ExecuteScript(sFunctionPath .. "Maps/fnCreateStandardPathScript.lua")
LM_ExecuteScript(sFunctionPath .. "Maps/fnExecWaterEmerge.lua")
LM_ExecuteScript(sFunctionPath .. "Maps/fnExecWaterJumpToMap.lua")
LM_ExecuteScript(sFunctionPath .. "Maps/fnExecWaterJumpToPos.lua")
LM_ExecuteScript(sFunctionPath .. "Maps/fnResolveMapLocation.lua")
LM_ExecuteScript(sFunctionPath .. "Maps/fnConstructorFacing.lua")

-- |[Minibosses]|
LM_ExecuteScript(sFunctionPath .. "Minibosses/fnMinibossConstructor.lua")
LM_ExecuteScript(sFunctionPath .. "Minibosses/fnMinibossDialogue.lua")
LM_ExecuteScript(sFunctionPath .. "Minibosses/fnMinibossVictory.lua")

-- |[Mods]|
LM_ExecuteScript(sFunctionPath .. "Mods/fnExecModScript.lua")

-- |[Overlays]|
LM_ExecuteScript(sFunctionPath .. "Overlays/fnFog.lua")
LM_ExecuteScript(sFunctionPath .. "Overlays/fnHeavyForest.lua")
LM_ExecuteScript(sFunctionPath .. "Overlays/fnLightForest.lua")

-- |[Party]|
LM_ExecuteScript(sFunctionPath .. "Party/fnAddPartyMember.lua")
LM_ExecuteScript(sFunctionPath .. "Party/fnIsCharacterPresent.lua")
LM_ExecuteScript(sFunctionPath .. "Party/fnRemovePartyMember.lua")

-- |[Puzzles]|
LM_ExecuteScript(sFunctionPath .. "Puzzles/fnMoveBlock.lua")

-- |[Random Levels]|
-- |[System]|
LM_ExecuteScript(sFunctionPath .. "System/fnArgCheck.lua")
LM_ExecuteScript(sFunctionPath .. "System/fnBoolToInt.lua")
LM_ExecuteScript(sFunctionPath .. "System/fnCountNewlines.lua")
LM_ExecuteScript(sFunctionPath .. "System/fnIsFlag.lua")
LM_ExecuteScript(sFunctionPath .. "System/fnParseDescriptionToArray.lua")
LM_ExecuteScript(sFunctionPath .. "System/fnGetStringFromDirCode.lua")
LM_ExecuteScript(sFunctionPath .. "System/fnGetLastDirectory.lua")
LM_ExecuteScript(sFunctionPath .. "System/fnGetPlanarDistance.lua")

-- |[Warping]|
LM_ExecuteScript(sFunctionPath .. "Warping/fnAddWarpEntry.lua")
LM_ExecuteScript(sFunctionPath .. "Warping/fnWarpUnlockHandler.lua")