-- |[ ====================================== fnHeavyForest ===================================== ]|
--Creates an overlay for a "heavy" forest, with high cover. Darkens most of the screen.
-- Should be called during level construction.
function fnHeavyForest()
    
    AL_SetProperty("Allocate Foregrounds", 3)
    AL_SetProperty("Foreground Image",          0, "Root/Images/AdventureUI/MapOverlays/ForestA")
    AL_SetProperty("Foreground Render Offsets", 0, 0.0, 0.0, 0.7, 0.7)
    AL_SetProperty("Foreground Alpha",          0, 0.40, 0)
    AL_SetProperty("Foreground Autoscroll",     0, 0.0, 0.0)
    AL_SetProperty("Foreground Scale",          0, 0.5)
	
    AL_SetProperty("Foreground Image",          1, "Root/Images/AdventureUI/MapOverlays/ForestC")
    AL_SetProperty("Foreground Render Offsets", 1, 0.0, 0.0, 0.6, 0.6)
    AL_SetProperty("Foreground Alpha",          1, 0.40, 0)
    AL_SetProperty("Foreground Autoscroll",     1, 0.0, 0.0)
    AL_SetProperty("Foreground Scale",          1, 0.5)
	
    AL_SetProperty("Foreground Image",          2, "Root/Images/AdventureUI/MapOverlays/ForestB")
    AL_SetProperty("Foreground Render Offsets", 2, 0.0, 0.0, 0.5, 0.5)
    AL_SetProperty("Foreground Alpha",          2, 0.40, 0)
    AL_SetProperty("Foreground Autoscroll",     2, 0.0, 0.0)
    AL_SetProperty("Foreground Scale",          2, 0.5)
    
end
