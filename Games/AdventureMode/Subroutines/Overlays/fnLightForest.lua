-- |[ ====================================== fnLightForest ===================================== ]|
--Creates an overlay for a "light" forest, with low cover. Does not obscure the camera much.
-- Should be called during level construction.
function fnLightForest()
    
    AL_SetProperty("Allocate Foregrounds", 1)
    AL_SetProperty("Foreground Image",          0, "Root/Images/AdventureUI/MapOverlays/ForestA")
    AL_SetProperty("Foreground Render Offsets", 0, 0.0, 0.0, 0.6, 0.6)
    AL_SetProperty("Foreground Alpha",          0, 0.40, 0)
    AL_SetProperty("Foreground Autoscroll",     0, 0.0, 0.0)
    AL_SetProperty("Foreground Scale",          0, 0.5)
    
end
