-- |[ ========================================== fnFog ========================================= ]|
--Creates an overlay for fog. Used in the Quantir Mansion sequence.
function fnFog(psPath)
    
    -- |[Variable Check]|
    --If the path is not provided, use the default fog overlay.
    if(psPath == nil) then psPath = "Root/Images/AdventureUI/MapOverlays/Fog" end
    
    -- |[Set]|
    AL_SetProperty("Allocate Foregrounds", 1)
    AL_SetProperty("Foreground Image",          0, psPath)
    AL_SetProperty("Foreground Render Offsets", 0, 0.0, 0.0, 2.5, 2.5)
    AL_SetProperty("Foreground Alpha",          0, 0.50, 0)
    AL_SetProperty("Foreground Autoscroll",     0, 0.30, -0.30)
    AL_SetProperty("Foreground Scale",          0, 2.0)
    
end
