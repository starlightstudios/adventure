-- |[ ================================ fnCheckItemAchievements() =============================== ]|
--Called when an item is added to the inventory. Checks item achievements and unlocks as needed.
function fnCheckItemAchievements(psItemName)
    
    -- |[Argument Check]|
    if(psItemName == nil) then return end
    
    -- |[Debug]|
    --io.write("Item " .. psItemName .. " has been created, checking achievements.\n")
    
    -- |[Listing]|
    --Create a listing of all achievements and related items. It is possible for an item to be on
    -- more than one list. If an entry appears more than once for the same achievement, it will be
    -- added more than once per creation.
    local zaList = {}
    local function fnAddToList(psName, psAchievement, piSlot, pfValueToAdd)
        local zEntry = {}
        zEntry.sName        = psName
        zEntry.sAchievement = psAchievement
        zEntry.iSlot        = piSlot
        zEntry.fValueToAdd  = pfValueToAdd
        table.insert(zaList, zEntry)
    end
    
    -- |[General Achievements]|
    fnAddToList("Glintsteel Gem",   "Needlemouse", 0, 1.0)
    fnAddToList("Phassicsteel Gem", "Needlemouse", 0, 1.0)
    fnAddToList("Arensteel Gem",    "Needlemouse", 0, 1.0)
    fnAddToList("Yemite Gem",       "Needlemouse", 1, 1.0)
    fnAddToList("Romite Gem",       "Needlemouse", 1, 1.0)
    fnAddToList("Morite Gem",       "Needlemouse", 1, 1.0)
    fnAddToList("Ardrion Gem",      "Needlemouse", 2, 1.0)
    fnAddToList("Nockrion Gem",     "Needlemouse", 2, 1.0)
    fnAddToList("Donion Gem",       "Needlemouse", 2, 1.0)
    fnAddToList("Rubose Gem",       "Needlemouse", 3, 1.0)
    fnAddToList("Piorose Gem",      "Needlemouse", 3, 1.0)
    fnAddToList("Iniorose Gem",     "Needlemouse", 3, 1.0)
    fnAddToList("Blurleen Gem",     "Needlemouse", 4, 1.0)
    fnAddToList("Mordreen Gem",     "Needlemouse", 4, 1.0)
    fnAddToList("Quorine Gem",      "Needlemouse", 4, 1.0)
    fnAddToList("Qederphage Gem",   "Needlemouse", 5, 1.0)
    fnAddToList("Phonophage Gem",   "Needlemouse", 5, 1.0)
    fnAddToList("Thatophage Gem",   "Needlemouse", 5, 1.0)

    -- |[Scan]|
    --io.write("Item entries: " .. #zaList .. "\n")
    for i = 1, #zaList, 1 do
        
        --Fast-access pointers.
        local zCurEntry = zaList[i]
        --io.write(" Compare: " .. zCurEntry.sName .. "\n")
        
        --Match.
        if(zCurEntry.sName == psItemName) then
            local fCurVal = fnGetAchievementProgress(zCurEntry.sAchievement, zCurEntry.iSlot)
            fnSetAchievementProgress(zCurEntry.sAchievement, zCurEntry.iSlot, fCurVal + zCurEntry.fValueToAdd)
            --io.write(" Match.\n")
        end

    end
    
    -- |[Group Achievements]|
    --None yet.

end
