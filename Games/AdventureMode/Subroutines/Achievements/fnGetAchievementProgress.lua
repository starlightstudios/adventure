-- |[ ================================ fnGetAchievementProgress ================================ ]|
--Returns the value for the given achievement. Basically does what the lua functions do, but also
-- barks warnings if something is out of range.
function fnGetAchievementProgress(psInternalName, piProgressSlot)

    -- |[Argument Check]|
    if(psInternalName == nil) then return 0 end
    if(piProgressSlot == nil) then return 0 end

    -- |[Warning Check]|
    --Check if the achievement exists.
    if(AM_GetPropertyJournal("Achievement Exists", psInternalName) == false) then
        if(gbAchievementDebug) then
            Debug_ForcePrint("fnGetAchievementProgress: Warning, achievement " .. psInternalName .. " does not exist.\n")
        end
        return 0
    end
    
    --Check if the slot is out of range.
    local iTotalProgressSlots = AM_GetPropertyJournal("Achievement Progress Slots", psInternalName)
    if(piProgressSlot < 0 or piProgressSlot >= iTotalProgressSlots) then
        if(gbAchievementDebug) then
            Debug_ForcePrint("fnGetAchievementProgress: Warning, achievement " .. psInternalName .. " has " .. iTotalProgressSlots .. " slots, but function tried to set slot " .. piProgressSlot .. ".\n")
        end
        return 0
    end

    -- |[Get]|
    local fReturn = AM_GetPropertyJournal("Achievement Progress Current", psInternalName, piProgressSlot)
    return fReturn

end
