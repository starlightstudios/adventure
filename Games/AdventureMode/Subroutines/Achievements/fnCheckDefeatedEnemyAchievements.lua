-- |[ ============================ fnCheckDefeatedEnemyAchievements ============================ ]|
--Called when an enemy is defeated, checks if that enemy qualifies for an achievement. This is mostly
-- used for paragons as there are several achievements related to those.
function fnCheckDefeatedEnemyAchievements(psEnemyName)
    
    -- |[Argument Check]|
    if(psEnemyName == nil) then return end
    
    -- |[Debug]|
    --io.write("Enemy name " .. psEnemyName .. " has been defeated, checking achievements.\n")
    
    -- |[Listing]|
    --Create a listing of all achievements and related enemies. It is possible for an enemy to be on
    -- more than one list. If an entry appears more than once for the same achievement, it will be
    -- added more than once per kill.
    local zaList = {}
    local function fnAddToList(psName, psAchievement, piSlot, pfValueToAdd)
        local zEntry = {}
        zEntry.sName        = psName
        zEntry.sAchievement = psAchievement
        zEntry.iSlot        = piSlot
        zEntry.fValueToAdd  = pfValueToAdd
        table.insert(zaList, zEntry)
    end
    
    -- |[Chapter 1 Listing]|
    --Create a list of chapter 1's achievement enemies.
    fnAddToList("Toxic Slimegirl Paragon", "Ch1ParaNature", 0, 1.0)
    fnAddToList("Alraune Keeper Paragon",  "Ch1ParaNature", 1, 1.0)
    fnAddToList("Sprout Thrasher Paragon", "Ch1ParaNature", 2, 1.0)
    fnAddToList("Bee Skirmisher Paragon",  "Ch1ParaNature", 3, 1.0)
    fnAddToList("Bee Tracker Paragon",     "Ch1ParaNature", 4, 1.0)
    fnAddToList("Werecat Thief Paragon",   "Ch1ParaNature", 5, 1.0)
    fnAddToList("Werecat Prowler Paragon", "Ch1ParaNature", 6, 1.0)
    
    fnAddToList("Ghost Maid Paragon",    "Ch1ParaSpooky", 0, 1.0)
    fnAddToList("Candle Haunt Paragon",  "Ch1ParaSpooky", 1, 1.0)
    fnAddToList("Bloody Mirror Paragon", "Ch1ParaSpooky", 2, 1.0)
    fnAddToList("Eye Drawer Paragon",    "Ch1ParaSpooky", 3, 1.0)
    
    fnAddToList("Gravemarker Paragon", "Ch1ParaPowerful", 0, 1.0)
    fnAddToList("Mushthrall Paragon",  "Ch1ParaPowerful", 1, 1.0)
    fnAddToList("Zombee Paragon",      "Ch1ParaPowerful", 2, 1.0)
    fnAddToList("Finger Paragon",      "Ch1ParaPowerful", 3, 1.0)
    fnAddToList("Palm Paragon",        "Ch1ParaPowerful", 4, 1.0)
    
    -- |[Chapter 5 Listing]|
    fnAddToList("Dreamer Paragon", "Ch5ParaDreamer", 0, 1.0)
    
    -- |[Scan]|
    for i = 1, #zaList, 1 do
        
        --Fast-access pointers.
        local zCurEntry = zaList[i]
        
        --Match.
        if(zCurEntry.sName == psEnemyName) then
            local fCurVal = fnGetAchievementProgress(zCurEntry.sAchievement, zCurEntry.iSlot)
            fnSetAchievementProgress(zCurEntry.sAchievement, zCurEntry.iSlot, fCurVal + zCurEntry.fValueToAdd)
        end

    end
    
    -- |[Group Achievements]|
    --Scan if any of the achievements that require other achievements to be completed changed state.
    if(AM_GetPropertyJournal("Achievement Is Unlocked", "Ch1ParaNature")) then
        fnSetAchievementProgress("Ch1ParaWeDidIt", 0, 1.0)
    end
    if(AM_GetPropertyJournal("Achievement Is Unlocked", "Ch1ParaSpooky")) then
        fnSetAchievementProgress("Ch1ParaWeDidIt", 1, 1.0)
    end
    if(AM_GetPropertyJournal("Achievement Is Unlocked", "Ch1ParaPowerful")) then
        fnSetAchievementProgress("Ch1ParaWeDidIt", 2, 1.0)
    end

end
