-- |[ =============================== fnSetAchievementProgress() =============================== ]|
--Sets a given progress variable for an achievement, and automatically unlocks the achievement if
-- all progress variables are at 100%. If you want an achievement to unlock using different circumstances,
-- do it manually. Almost every achievement unlocks in this way, or a direct call to unlock.
--Barks an error if the achievement does not exist.
function fnSetAchievementProgress(psInternalName, piProgressSlot, pfValue)

    -- |[Argument Check]|
    if(psInternalName == nil) then return end
    if(piProgressSlot == nil) then return end
    if(pfValue        == nil) then return end

    -- |[Warning Check]|
    if(AM_GetPropertyJournal("Achievement Exists", psInternalName) == false) then
        if(gbAchievementDebug) then
            Debug_ForcePrint("fnSetAchievementProgress: Warning, achievement " .. psInternalName .. " does not exist.\n")
        end
        return
    end

    -- |[Set]|
    AM_SetPropertyJournal("Set Achievement Progress", psInternalName, piProgressSlot, pfValue)
    
    -- |[Check Unlock State]|
    --Scan all the progress requirements. If all of them are 100%, unlock the achievement.
    local iTotalProgressSlots = AM_GetPropertyJournal("Achievement Progress Slots", psInternalName)
    for i = 0, iTotalProgressSlots-1, 1 do
        
        --Get values.
        local fCurrent = AM_GetPropertyJournal("Achievement Progress Current", psInternalName, i)
        local fMaximum = AM_GetPropertyJournal("Achievement Progress Maximum", psInternalName, i)
        
        --If the current is lower, fail here.
        if(fCurrent < fMaximum) then return end

    end

    --If we got this far, all the values are high enough. Unlock.
    AM_SetPropertyJournal("Unlock Achievement", psInternalName)

end
