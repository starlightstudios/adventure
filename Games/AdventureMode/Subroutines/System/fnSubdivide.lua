-- |[ ===================================== fnSubdivide() ====================================== ]|
--Given a string formatted "Dogs|Are|Great" returns each of the individual words "Dogs" "Are" "Great"
-- as a list. The delimiter can be configured to use other types if desired.
--To configure delimiters, the string "|" uses | as a delimiter, and "|:" uses both "|" and ":"
-- as delimiters. There is no particular limit to how many can be used.
--The parameter psStack is optional. If provided, it is expected to be a single-character string
-- that allows 'stack' behavior. Delimiters inside a stack are ignored until a matching entry in
-- that stack is found. So for example, if the delimiter | is in use, and the stack ' is in use:
-- The string Dogs|'Are|good|boys'|Is a sentence
-- Will results in 3 strings. "Dogs", "'Are|good|boys'", "Is a sentence". Stack delimiters are NOT trimmed.
function fnSubdivide(psString, psDelimiters, psStack)
    
    -- |[Argument Check]|
    if(psString     == nil) then return {""} end
    if(psDelimiters == nil) then return {""} end
    
    -- |[Setup]|
    --Create a list.
    local i = 0
    local saReturn = {}
    
    --Variables.
    local iLastStringIndex = 1
    local iLength = string.len(psString)
    local iDelimiters = string.len(psDelimiters)
    
    --Stack handling
    local bIsInStack = false
    
    -- |[Iterate Across String]|
    --Begin iteration.
    for p = 1, iLength, 1 do
    
        --Get the letter here.
        local sLetter = string.sub(psString, p, p)
        
        --If this is the stack letter:
        if(psStack ~= nil and sLetter == psStack) then
            if(bIsInStack == true) then
                bIsInStack = false
            else
                bIsInStack = true
            end
        end
        
        --If not in a stack, scan for delimiters:
        if(bIsInStack == false) then
            for x = 1, iDelimiters, 1 do
                local sDelimiter = string.sub(psDelimiters, x, x)
                
                --Match, create a new substring and add it to the list.
                if(sDelimiter == sLetter) then
                    
                    i = i + 1
                    saReturn[i] = string.sub(psString, iLastStringIndex, p-1)
                    iLastStringIndex = p+1
                    break
                end
            end
        
        --When in a stack, ignore delimiters.
        else
    
        end
    end
    
    -- |[Last Segment]|
    --If we iterated across the whole thing and did not find a delimiter, or there was not a delimiter
    -- in the last slot, we need to copy the final stretch.
    if(iLastStringIndex <= iLength) then
        i = i + 1
        saReturn[i] = string.sub(psString, iLastStringIndex, iLength)
    end
    
    --Pass back the array.
    return saReturn
end

-- |[ ================================== fnSubdivideStrict() =================================== ]|
--The same as the above, except the delimiter is a string that does not use OR functionality. Instead,
-- the entire string must be found to act as a delimiter. This allows more exotic delimiters.
function fnSubdivideStrict(psString, psDelimiter)
    
    -- |[Argument Check]|
    if(psString    == nil) then return {""} end
    if(psDelimiter == nil) then return {""} end
    
    --Create a list.
    local i = 0
    local saReturn = {}
    
    --Setup.
    local iLastStringIndex = 1
    local iLength = string.len(psString)
    local iDelimiterLen = string.len(psDelimiter)
    
    --Begin iteration.
    for p = 1, iLength, 1 do
    
        --Get the letter here.
        local sLetter = string.sub(psString, p, p)
        
        --Check the delimiter for a match.
        local sSubstring = string.sub(psString, p, p+iDelimiterLen-1)
        if(sSubstring == psDelimiter) then
            table.insert(saReturn, string.sub(psString, iLastStringIndex, p-1))
            iLastStringIndex = p+iDelimiterLen
            p = iLastStringIndex
        end
    end
    
    --If we iterated across the whole thing and did not find a delimiter, or there was not a delimiter
    -- in the last slot, we need to copy the final stretch.
    if(iLastStringIndex <= iLength) then
        table.insert(saReturn, string.sub(psString, iLastStringIndex, iLength))
    end
    
    --Pass back the array.
    return saReturn
end
