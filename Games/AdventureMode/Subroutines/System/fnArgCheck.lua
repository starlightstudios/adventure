-- |[ ====================================== fnArgCheck() ====================================== ]|
--Standardized argument checker, using arguments held by the LuaManager. Returns true if the required
-- number of arguments was passed by the LuaManager, false if not.
function fnArgCheck(piRequiredArgs)
    
    -- |[Argument Check]|
    if(piRequiredArgs == nil) then return false end
    
    -- |[Query]|
	local iArgs = LM_GetNumOfArgs()
	if(iArgs < piRequiredArgs) then
		Debug_ForcePrint(fnResolvePath() .. fnResolveName() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. piRequiredArgs .. ".\n")
		return false
	end
	
    -- |[All Checks Passed]|
	return true
end
