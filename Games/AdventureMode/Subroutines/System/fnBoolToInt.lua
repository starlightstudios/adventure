-- |[ ======================================= fnBoolToInt ====================================== ]|
--Function that converts a boolean (true/false) to an int (1/0). If the provided value is nil, 
-- returns -1. This is generally used for diagnostics.
function fnBoolToInt(pbValue)
    
    --Value is nil:
    if(pbValue == nil) then return -1 end
    
    --Value is true or false:
    if(pbValue == true) then return 1 end
    if(pbValue == false) then return 0 end
    
    --Otherwise, return 2 to indicate it was a non-boolean type and could not be casted.
    return 2
end
