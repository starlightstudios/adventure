-- |[ =============================== fnParseDescriptionToArray() ============================== ]|
--Function for descriptions. Has a basic markdown handler. Returns an array of description lines
-- split by "\n" or [BR] cases.
function fnParseDescriptionToArray(psDescriptionLine)

    -- |[Argument Check]|
    if(psDescriptionLine == nil) then return {} end
    
    -- |[Setup]|
    local iSkips = 0
    local iCurrentLine = 1
    local sCurrentLine = ""
    local saDescription = {}
    
    -- |[Translation]|
    --If a translation is available, use that.
    psDescriptionLine = Translate(gsTranslationJournal, psDescriptionLine)
    saDescription[1] = psDescriptionLine
    
    --[=[
    -- |[Parse]|
    local iLen = string.len(psDescriptionLine)
    for i = 1, iLen, 1 do
        
        --Store.
        local sLetter = string.sub(psDescriptionLine, i, i)
        
        --Skip this letter.
        if(iSkips > 0) then
            iSkips = iSkips - 1
        
        --New line.
        elseif(string.sub(psDescriptionLine, i, i+1) == "\\n") then
            saDescription[iCurrentLine] = sCurrentLine
            sCurrentLine = ""
            iCurrentLine = iCurrentLine + 1
            iSkips = 1
        
        --New line, tag edition.
        elseif(string.sub(psDescriptionLine, i, i+3) == "[BR]") then
            saDescription[iCurrentLine] = sCurrentLine
            sCurrentLine = ""
            iCurrentLine = iCurrentLine + 1
            iSkips = 3
        
        --Copy the letter.
        else
            sCurrentLine = sCurrentLine .. sLetter
        end
    end
    
    --Final line.
    if(sCurrentLine ~= "") then
        saDescription[iCurrentLine] = sCurrentLine
    end]=]
    
    -- |[Finish Up]|
    return saDescription
end