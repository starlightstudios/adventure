-- |[ ================================= fnGetPlanarDistance() ================================== ]|
--Returns the planar distance between the two provided points.
function fnGetPlanarDistance(pfX1, pfY1, pfX2, pfY2)
	local fXDistSqr = (pfX1 - pfX2) * (pfX1 - pfX2)
	local fYDistSqr = (pfY1 - pfY2) * (pfY1 - pfY2)
	return math.sqrt(fXDistSqr + fYDistSqr)
end
