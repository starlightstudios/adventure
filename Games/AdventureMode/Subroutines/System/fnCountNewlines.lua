-- |[ ==================================== fnCountNewlines() =================================== ]|
--Given a string, returns how many newline indicators are in it.
function fnCountNewlines(psString)
    
    -- |[Error Check]|
    --Verify arguments.
    if(psString == nil) then return 0 end
    
    -- |[Setup]|
    --Variables.
    local iLineCount = 0
    local iStart = 1
    
    -- |[Iterate]|
    --Run across the string, starting at each found newline until none is found.
    while(true) do
        local iNewline = string.find(psString, "\n", iStart)
        local iAltline = string.find(psString, "%[BR%]", iStart)
        if(iNewline == nil and iAltline == nil) then break end
        
        --Use Newline if no Altline was found:
        if(iAltline == nil) then
            iStart = iNewline + 1

        --Use Altline if no Newline was found:
        elseif(iNewline == nil) then
            iStart = iAltline + 4
        
        --Both were found, so use the lowest of the two.
        elseif(iNewline < iAltline) then
            iStart = iNewline + 1
        else
            iStart = iAltline + 4
        end
        
        --Increment line count.
        iLineCount = iLineCount + 1
    end
    
    -- |[Finish Up]|
    --Return the total newlines found.
    return iLineCount
    
end
