-- |[ ======================================= fnIsFlag() ======================================= ]|
--Returns true if a given flag is nonzero, false if it is zero. This is basically a compact way to
-- to bitwise AND operations.
function fnIsFlag(piValue, piFlag)
    if(piValue & piFlag > 0) then return true end
    return false
end
