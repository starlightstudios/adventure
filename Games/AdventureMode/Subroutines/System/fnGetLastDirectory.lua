-- |[ =================================== fnGetLastDirectory =================================== ]|
--Given a file path, removes the last directory and returns the directory.
function fnGetLastDirectory(psFilePath)
    
    -- |[Argument Check]|
    if(psFilePath == nil) then return "Null" end

    -- |[Setup]|
	--Variable setup.
	local sEndingPath = "Null"
	local bHasFoundNonSlash = false
	
	--String properties.
	local iPos = string.len(psFilePath)
	local iEndingLetter = iPos
	
    -- |[Iterate]|
	--Iterate across the string backwards.
	while(iPos > 1) do
		
		--Get the letter.
		local sLetter = string.sub(psFilePath, iPos, iPos)
		
		--If the letter is a slash:
		if(sLetter == "/" or sLetter == "\\") then
			
			--We have found a non-slash before this:
			if(bHasFoundNonSlash == true) then
				return string.sub(psFilePath, iPos+1)
			
			--We have not found a non-slash, so move the ending letter back.
			else
				iEndingLetter = iPos - 1
			end
		
		--If the letter is not a slash:
		else
			bHasFoundNonSlash = true
		end
		iPos = iPos - 1
	end

    -- |[Finish Up]|
	--If we got this far, return the original path. It may not have had a valid slash.
	return psFilePath
end
