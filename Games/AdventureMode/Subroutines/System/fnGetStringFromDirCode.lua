-- |[fnGetStringFromDirCode]|
function fnGetStringFromDirCode(piCode)
    if(piCode == gci_Face_North)     then return "N" end
    if(piCode == gci_Face_NorthEast) then return "NE" end
    if(piCode == gci_Face_East)      then return "E" end
    if(piCode == gci_Face_SouthEast) then return "SE" end
    if(piCode == gci_Face_South)     then return "S" end
    if(piCode == gci_Face_SouthWest) then return "SW" end
    if(piCode == gci_Face_West)      then return "W" end
    if(piCode == gci_Face_NorthWest) then return "NW" end
end

-- |[fnGetDirCodeFromString]|
function fnGetDirCodeFromString(psString)
    if(psString == "N")  then return gci_Face_North     end
    if(psString == "NE") then return gci_Face_NorthEast end
    if(psString == "E")  then return gci_Face_East      end
    if(psString == "SE") then return gci_Face_SouthEast end
    if(psString == "S")  then return gci_Face_South     end
    if(psString == "SW") then return gci_Face_SouthWest end
    if(psString == "W")  then return gci_Face_West      end
    if(psString == "NW") then return gci_Face_NorthWest end
end

-- |[fnGetRevDirCodeFromString]|
function fnGetRevDirCodeFromString(psString)
    if(psString == "S")  then return gci_Face_North     end
    if(psString == "SW") then return gci_Face_NorthEast end
    if(psString == "W")  then return gci_Face_East      end
    if(psString == "NW") then return gci_Face_SouthEast end
    if(psString == "N")  then return gci_Face_South     end
    if(psString == "NE") then return gci_Face_SouthWest end
    if(psString == "E")  then return gci_Face_West      end
    if(psString == "SE") then return gci_Face_NorthWest end
end

-- |[fnGetReverseDirFromCode]|
function fnGetReverseDirFromCode(piCode)
    if(piCode == gci_Face_North)     then return gci_Face_South end
    if(piCode == gci_Face_NorthEast) then return gci_Face_SouthWest end
    if(piCode == gci_Face_East)      then return gci_Face_West end
    if(piCode == gci_Face_SouthEast) then return gci_Face_NorthWest end
    if(piCode == gci_Face_South)     then return gci_Face_North end
    if(piCode == gci_Face_SouthWest) then return gci_Face_NorthEast end
    if(piCode == gci_Face_West)      then return gci_Face_East end
    if(piCode == gci_Face_NorthWest) then return gci_Face_SouthEast end
end