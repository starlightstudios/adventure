-- |[ ================================== Assemble Relive List ================================== ]|
--This script is fired when the save menu opens up. It will resolve the list of scenes the player can "relive",
-- which involves playing through them again until a certain point, at which they warp back to their last save.
--It is possible to have no scenes available, in which case the menu doesn't open. The menu is clear before this fires.
local iCurrentChapter = VM_GetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N")

--If currently in the "Nowhere" map, don't generate anything.
local sName = AL_GetProperty("Name")
if(sName == "Nowhere") then return end

-- |[ ===================================== Chapter 1: Mei ===================================== ]|
if(iCurrentChapter == 1.0) then
    
    -- |[Rubber]|
    --Blocks all relives.
    local iIsRubberMode = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
    if(iIsRubberMode == 1.0) then return end
    
    -- |[Mannequin]|
    --Blocks all relives.
    local iBeganSequence     = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iBeganSequence", "N")
    local iCompletedSequence = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iCompletedSequence", "N")
    if(iBeganSequence == 1.0 and iCompletedSequence == 0.0) then return end
    
    -- |[Variables]|
    --Get variables.
    local iHasAlrauneForm     = VM_GetVar("Root/Variables/Global/Mei/iHasAlrauneForm",     "N")
    local iHasBeeForm         = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm",         "N")
    local iHasSlimeForm       = VM_GetVar("Root/Variables/Global/Mei/iHasSlimeForm",       "N")
    local iHasWerecatForm     = VM_GetVar("Root/Variables/Global/Mei/iHasWerecatForm",     "N")
    local iHasGhostForm       = VM_GetVar("Root/Variables/Global/Mei/iHasGhostForm",       "N")
    local iHasGravemarkerForm = VM_GetVar("Root/Variables/Global/Mei/iHasGravemarkerForm", "N")
    local iHasWisphagForm     = VM_GetVar("Root/Variables/Global/Mei/iHasWisphagForm",     "N")
    local iHasMannequinForm   = VM_GetVar("Root/Variables/Global/Mei/iHasMannequinForm",   "N")
    local iHasZombeeForm      = VM_GetVar("Root/Variables/Global/Mei/iHasZombeeForm",      "N")

    -- |[Forms]|
    --Alraune transformation:
    if(iHasAlrauneForm == 1.0) then
        
        --Combat.
        AM_SetProperty("Register Relive Script", "Alraune", gsStandardReliveBegin, "Root/Images/AdventureUI/AdvCampfireReliveIcon/MeiAlrauneC")
        AM_SetProperty("Set Relive Alignments",  "Alraune", 0.0, 0.0, 0.0, 0.0, 96.0, 96.0)
        
        --Voluntary.
        AM_SetProperty("Register Relive Script", "Alraune (V)", gsStandardReliveBegin, "Root/Images/AdventureUI/AdvCampfireReliveIcon/MeiAlrauneV")
        AM_SetProperty("Set Relive Alignments",  "Alraune (V)", 0.0, 0.0, 0.0, 0.0, 96.0, 96.0)
    end

    --Bee transformation:
    if(iHasBeeForm == 1.0) then
        
        --Combat.
        AM_SetProperty("Register Relive Script", "Bee", gsStandardReliveBegin, "Root/Images/AdventureUI/AdvCampfireReliveIcon/MeiBeeC")
        AM_SetProperty("Set Relive Alignments",  "Bee", 0.0, 0.0, 0.0, 0.0, 96.0, 96.0)
        
        --Voluntary.
        AM_SetProperty("Register Relive Script", "Bee (V)", gsStandardReliveBegin, "Root/Images/AdventureUI/AdvCampfireReliveIcon/MeiBeeV")
        AM_SetProperty("Set Relive Alignments",  "Bee (V)", 0.0, 0.0, 0.0, 0.0, 96.0, 96.0)
    end

    --Ghost transformation:
    if(iHasGhostForm == 1.0) then
        
        --Combat.
        AM_SetProperty("Register Relive Script", "Ghost", gsStandardReliveBegin, "Root/Images/AdventureUI/AdvCampfireReliveIcon/MeiGhostC")
        AM_SetProperty("Set Relive Alignments",  "Ghost", 0.0, 0.0, 0.0, 0.0, 96.0, 96.0)
        
        --Voluntary.
        AM_SetProperty("Register Relive Script", "Ghost (V)", gsStandardReliveBegin, "Root/Images/AdventureUI/AdvCampfireReliveIcon/MeiGhostV")
        AM_SetProperty("Set Relive Alignments",  "Ghost (V)", 0.0, 0.0, 0.0, 0.0, 96.0, 96.0)
    end

    --Slime transformation:
    if(iHasSlimeForm == 1.0) then
        
        --Combat.
        AM_SetProperty("Register Relive Script", "Slime", gsStandardReliveBegin, "Root/Images/AdventureUI/AdvCampfireReliveIcon/MeiSlimeC")
        AM_SetProperty("Set Relive Alignments",  "Slime", 0.0, 0.0, 0.0, 0.0, 96.0, 96.0)
        
        --Volunteer.
        AM_SetProperty("Register Relive Script", "Slime (V)", gsStandardReliveBegin, "Root/Images/AdventureUI/AdvCampfireReliveIcon/MeiSlimeV")
        AM_SetProperty("Set Relive Alignments",  "Slime (V)", 0.0, 0.0, 0.0, 0.0, 96.0, 96.0)
        
        --Meryl uWu
        AM_SetProperty("Register Relive Script", "Slime (M)", gsStandardReliveBegin, "Root/Images/AdventureUI/AdvCampfireReliveIcon/MeiSlimeM")
        AM_SetProperty("Set Relive Alignments",  "Slime (M)", 0.0, 0.0, 0.0, 0.0, 96.0, 96.0)
    end

    --Werecat transformation:
    if(iHasWerecatForm == 1.0) then
        
        --Base.
        AM_SetProperty("Register Relive Script", "Werecat", gsStandardReliveBegin, "Root/Images/AdventureUI/AdvCampfireReliveIcon/MeiWerecatC")
        AM_SetProperty("Set Relive Alignments",  "Werecat", 0.0, 0.0, 0.0, 0.0, 96.0, 96.0)
        
        --Volunteer. Requires Cassandra.
        local iSavedCassandra  = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedCassandra",  "N")
        local iTurnedCassandra = VM_GetVar("Root/Variables/Chapter1/Scenes/iTurnedCassandra", "N")
        if(iSavedCassandra == 1.0 or iTurnedCassandra >= 1.0) then
            AM_SetProperty("Register Relive Script", "Werecat (V)", gsStandardReliveBegin, "Root/Images/AdventureUI/AdvCampfireReliveIcon/MeiWerecatV")
            AM_SetProperty("Set Relive Alignments",  "Werecat (V)", 0.0, 0.0, 0.0, 0.0, 96.0, 96.0)
        end
    end

    --Wisphag transformation:
    if(iHasWisphagForm == 1.0) then
        
        --Base.
        AM_SetProperty("Register Relive Script", "Wisphag", gsStandardReliveBegin, "Root/Images/AdventureUI/AdvCampfireReliveIcon/MeiWisphag")
        AM_SetProperty("Set Relive Alignments",  "Wisphag", 0.0, 0.0, 0.0, 0.0, 96.0, 96.0)
        
        --Volunteer.
        AM_SetProperty("Register Relive Script", "Wisphag (V)", gsStandardReliveBegin, "Root/Images/AdventureUI/AdvCampfireReliveIcon/MeiWisphagV")
        AM_SetProperty("Set Relive Alignments",  "Wisphag (V)", 0.0, 0.0, 0.0, 0.0, 96.0, 96.0)
    end

    --Mannequin transformation:
    if(iHasMannequinForm == 1.0) then
        AM_SetProperty("Register Relive Script", "Mannequin", gsStandardReliveBegin, "Root/Images/AdventureUI/AdvCampfireReliveIcon/MeiMannequin")
        AM_SetProperty("Set Relive Alignments",  "Mannequin", 0.0, 0.0, 0.0, 0.0, 96.0, 96.0)
    end

    --Gravemarker transformation:
    if(iHasGravemarkerForm == 1.0) then
        AM_SetProperty("Register Relive Script", "Gravemarker", gsStandardReliveBegin, "Root/Images/AdventureUI/AdvCampfireReliveIcon/MeiGravemarker")
        AM_SetProperty("Set Relive Alignments",  "Gravemarker", 0.0, 0.0, 0.0, 0.0, 96.0, 96.0)
    end
    
    --Zombee transformation:
    if(iHasZombeeForm == 1.0) then
        AM_SetProperty("Register Relive Script", "Zombee", gsStandardReliveBegin, "Root/Images/AdventureUI/AdvCampfireReliveIcon/MeiZombee")
        AM_SetProperty("Set Relive Alignments",  "Zombee", 0.0, 0.0, 0.0, 0.0, 96.0, 96.0)
    end

-- |[ ================================== Chapter 5: Christine ================================== ]|
elseif(iCurrentChapter == 5.0) then

    -- |[Exclusion Case]|
    --If still in male form at chapter start, never put anything on the relive list:
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    if(sChristineForm == "Male") then return end

    -- |[Variables]|
    --Get variables.
    local iHasDollForm          = VM_GetVar("Root/Variables/Global/Christine/iHasDollForm", "N")
    local iHasGolemForm         = VM_GetVar("Root/Variables/Global/Christine/iHasGolemForm", "N")
    local iHasLatexForm         = VM_GetVar("Root/Variables/Global/Christine/iHasLatexForm", "N")
    local iHasDarkmatterForm    = VM_GetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N")
    local iHasEldritchForm      = VM_GetVar("Root/Variables/Global/Christine/iHasEldritchForm", "N")
    local iHasElectrospriteForm = VM_GetVar("Root/Variables/Global/Christine/iHasElectrospriteForm", "N")
    local iHasSteamDroidForm    = VM_GetVar("Root/Variables/Global/Christine/iHasSteamDroidForm", "N")
    local iHasRaijuForm         = VM_GetVar("Root/Variables/Global/Christine/iHasRaijuForm", "N")
    local iHasSecrebotForm      = VM_GetVar("Root/Variables/Global/Christine/iHasSecrebotForm", "N")
    local i254Completed         = VM_GetVar("Root/Variables/Chapter5/Scenes/i254Completed", "N")
    
    -- |[Forms]|
    --Doll:
    if(iHasDollForm == 1.0) then
        AM_SetProperty("Register Relive Script", "Doll", gsStandardReliveBegin, "Root/Images/AdventureUI/AdvCampfireReliveIcon/ChristineDoll")
        AM_SetProperty("Set Relive Alignments",  "Doll", 0.0, 0.0, 0.0, 0.0, 96.0, 96.0)
    end
    
    --Golem:
    if(iHasGolemForm == 1.0) then
        AM_SetProperty("Register Relive Script", "Golem", gsStandardReliveBegin, "Root/Images/AdventureUI/AdvCampfireReliveIcon/ChristineGolem")
        AM_SetProperty("Set Relive Alignments",  "Golem", 0.0, 0.0, 0.0, 0.0, 96.0, 96.0)
    end
    
    --Latex Drone:
    if(iHasLatexForm == 1.0) then
        
        --This always registers.
        AM_SetProperty("Register Relive Script", "Latex Drone", gsStandardReliveBegin, "Root/Images/AdventureUI/AdvCampfireReliveIcon/ChristineLatexC")
        AM_SetProperty("Set Relive Alignments",  "Latex Drone", 0.0, 0.0, 0.0, 0.0, 96.0, 96.0)
        
        --If the player is on the Manufactory quest, the volunteer sequence appears.
        local iManuSetAuthChip = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuSetAuthChip", "N")
        if(iManuSetAuthChip == 1.0) then
            AM_SetProperty("Register Relive Script", "Latex Drone (V)", gsStandardReliveBegin, "Root/Images/AdventureUI/AdvCampfireReliveIcon/ChristineLatexV")
            AM_SetProperty("Set Relive Alignments",  "Latex Drone (V)", 0.0, 0.0, 0.0, 0.0, 96.0, 96.0)
        end
    end
    
    --Darkmatter
    if(iHasDarkmatterForm == 1.0) then
        AM_SetProperty("Register Relive Script", "Darkmatter", gsStandardReliveBegin, "Root/Images/AdventureUI/AdvCampfireReliveIcon/ChristineDarkmatter")
        AM_SetProperty("Set Relive Alignments",  "Darkmatter", 0.0, 0.0, 0.0, 0.0, 96.0, 96.0)
    end

    --Eldritch
    if(iHasEldritchForm == 1.0) then
        AM_SetProperty("Register Relive Script", "Dreamer", gsStandardReliveBegin, "Root/Images/AdventureUI/AdvCampfireReliveIcon/ChristineDreamer")
        AM_SetProperty("Set Relive Alignments",  "Dreamer", 0.0, 0.0, 0.0, 0.0, 96.0, 96.0)
    end
    
    --Electrosprite
    if(iHasElectrospriteForm == 1.0) then
        AM_SetProperty("Register Relive Script", "Electrosprite", gsStandardReliveBegin, "Root/Images/AdventureUI/AdvCampfireReliveIcon/ChristineElectrosprite")
        AM_SetProperty("Set Relive Alignments",  "Electrosprite", 0.0, 0.0, 0.0, 0.0, 96.0, 96.0)
    end
    
    --Steam Droid
    if(iHasSteamDroidForm == 1.0) then
        AM_SetProperty("Register Relive Script", "SteamDroid", gsStandardReliveBegin,"Root/Images/AdventureUI/AdvCampfireReliveIcon/ChristineSteamDroid")
        AM_SetProperty("Set Relive Alignments",  "SteamDroid", 0.0, 0.0, 0.0, 0.0, 96.0, 96.0)
    end
    
    --Raiju
    if(iHasRaijuForm == 1.0) then
        AM_SetProperty("Register Relive Script", "Raiju", gsStandardReliveBegin, "Root/Images/AdventureUI/AdvCampfireReliveIcon/ChristineRaiju")
        AM_SetProperty("Set Relive Alignments",  "Raiju", 0.0, 0.0, 0.0, 0.0, 96.0, 96.0)
    end
    
    --Secrebot.
    if(iHasSecrebotForm == 1.0) then
        AM_SetProperty("Register Relive Script", "Secrebot", gsStandardReliveBegin, "Root/Images/AdventureUI/AdvCampfireReliveIcon/ChristineSecrebot")
        AM_SetProperty("Set Relive Alignments",  "Secrebot", 0.0, 0.0, 0.0, 0.0, 96.0, 96.0)
    end

    --Secrebot Bad End.
    if(i254Completed == 1.0) then
        AM_SetProperty("Register Relive Script", "Secrebot Bad End", gsStandardReliveBegin, "Root/Images/AdventureUI/AdvCampfireReliveIcon/ChristineSecrebotBadEnd")
        AM_SetProperty("Set Relive Alignments",  "Secrebot Bad End", 0.0, 0.0, 0.0, 0.0, 96.0, 96.0)
    end
end
