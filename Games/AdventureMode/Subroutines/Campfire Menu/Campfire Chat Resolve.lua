-- |[ ================================== System Party Resolve ================================== ]|
--When the player opens up the AdventureMenu at a campfire, this script is fired. It builds a list of
-- characters that the party leader may speak to in a dialogue. In Chapter 1, this is just Florentina.
--If nobody can be spoken to, "Chat" is greyed out on the menu. The menu is implicitly cleared when 
-- this is called. Members need to be re-added each time it is called.
local sCurrentLevel = AL_GetProperty("Name")
if(sCurrentLevel == "Nowhere") then return end

--Get current chapter.
local iCurrentChapter = VM_GetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N")

-- |[ ===================================== Adder Function ===================================== ]|
--Storage list.
zaLookups = {}

--Add an element to the list.
function fnAddCampfireChat(psName, psDisplayName, psDialogue, fOffX, fOffY, fLft, fTop, fRgt, fBot, psIcon)
    
    --Argument check.
    if(psName     == nil) then return end
    if(psDialogue == nil) then return end
    if(fLft       == nil) then return end
    if(fTop       == nil) then return end
    if(fRgt       == nil) then return end
    if(fBot       == nil) then return end
    if(psIcon     == nil) then return end
    
    --Increment.
    local i = #zaLookups + 1
    zaLookups[i] = {}
    
    --Set.
    zaLookups[i].sName        = psName
    zaLookups[i].sDisplayName = psDisplayName
    zaLookups[i].sDialogue    = psDialogue
    zaLookups[i].fOffX        = fOffX
    zaLookups[i].fOffY        = fOffY
    zaLookups[i].fLft         = fLft
    zaLookups[i].fTop         = fTop
    zaLookups[i].fRgt         = fRgt
    zaLookups[i].fBot         = fBot
    zaLookups[i].sIcon        = psIcon
    
end

-- |[ ======================================== Chapter 1 ======================================= ]|
if(iCurrentChapter == 1.0) then
    
    -- |[Exclusion]|
    --Cannot chat during the mannequin quest
    local iBeganSequence = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iBeganSequence", "N")
    local iPolarisRunIn  = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iPolarisRunIn", "N")
    if(iBeganSequence == 1.0 and iPolarisRunIn == 0.0) then return end
    
    -- |[Setup]|
    --Dialogue root.
    local sDialoguePath = gsRoot .. "Chapter 1/Dialogue/"
    
    -- |[Florentina]|
    --Florentina's dimensions do not change.
    local sFlorentinaImg = "Null"
    local zaDim = {0.0, 0.0, 0.0, 0.0, 180.0, 180.0}
    
    --Resolve which portrait to use.
    local sFlorentinaJob = VM_GetVar("Root/Variables/Global/Florentina/sCurrentJob", "S")
    if(sFlorentinaJob == "Mediator") then
        sFlorentinaImg = "Root/Images/AdventureUI/ChatIcons/FlorentinaMediator"
    elseif(sFlorentinaJob == "TreasureHunter") then
        sFlorentinaImg = "Root/Images/AdventureUI/ChatIcons/FlorentinaTreasureHunter"
    elseif(sFlorentinaJob == "Merchant") then
        sFlorentinaImg = "Root/Images/AdventureUI/ChatIcons/FlorentinaMerchant"
    elseif(sFlorentinaJob == "Agarist") then
        sFlorentinaImg = "Root/Images/AdventureUI/ChatIcons/FlorentinaMushraune"
    elseif(sFlorentinaJob == "Lurker") then
        sFlorentinaImg = "Root/Images/AdventureUI/ChatIcons/FlorentinaMannequin"
    end
    
    --Lookup table.
    fnAddCampfireChat("Florentina", "Florentina", sDialoguePath .. "Florentina/Campfire.lua", zaDim[1], zaDim[2], zaDim[3], zaDim[4], zaDim[5] + zaDim[3], zaDim[6] + zaDim[4], sFlorentinaImg)

-- |[ ======================================== Chapter 2 ======================================= ]|
elseif(iCurrentChapter == 2.0) then
    
    -- |[Setup]|
    --Dialogue root.
    local sDialoguePath = gsRoot .. "Chapter 2/Dialogue/"
    
    -- |[Zeke]|
    --What a goat!
    local zaDim = {0.0, 0.0, 0.0, 0.0, 180.0, 180.0}
    
    --Lookup table.
    fnAddCampfireChat("Zeke", "Zeke", sDialoguePath .. "Zeke/Campfire.lua", zaDim[1], zaDim[2], zaDim[3], zaDim[4], zaDim[5] + zaDim[3], zaDim[6] + zaDim[4], "Root/Images/AdventureUI/ChatIcons/ZekeGoat")
    
    -- |[Izuna]|
    --The one and only.
    fnAddCampfireChat("Izuna", "Izuna", sDialoguePath .. "Izuna/Campfire.lua", zaDim[1], zaDim[2], zaDim[3], zaDim[4], zaDim[5] + zaDim[3], zaDim[6] + zaDim[4], "Root/Images/AdventureUI/ChatIcons/IzunaMiko")
    
    -- |[Empress]|
    --Ready to beat ass.
    fnAddCampfireChat("Empress", "Empress", sDialoguePath .. "Empress/Campfire.lua", zaDim[1], zaDim[2], zaDim[3], zaDim[4], zaDim[5] + zaDim[3], zaDim[6] + zaDim[4], "Root/Images/AdventureUI/ChatIcons/EmpressConquerer")
    
-- |[ ======================================== Chapter 3 ======================================= ]|
-- |[ ======================================== Chapter 4 ======================================= ]|
-- |[ ======================================== Chapter 5 ======================================= ]|
elseif(iCurrentChapter == 5.0) then
    
    -- |[Setup]|
    --Dialogue root.
    local sDialoguePath = gsRoot .. "Chapter 5/Dialogue/"
    local zaDim = {0.0, 0.0, 0.0, 0.0, 1.0, 1.0}
    
    -- |[Tiffany]|
    --Only has one costume for now.
    local sTiffanyImg = "Root/Images/AdventureUI/ChatIcons/TiffanyAll"
    zaDim = {0.0, 0.0, 0.0, 0.0, 180.0, 180.0}
    fnAddCampfireChat("Tiffany", "55", sDialoguePath .. "Tiffany/Campfire.lua", zaDim[1], zaDim[2], zaDim[3], zaDim[4], zaDim[5] + zaDim[3], zaDim[6] + zaDim[4], sTiffanyImg)
    
    -- |[SX-399]|
    --Only gets added in the biolabs, even if she's following for another reason.
    local iReachedBiolabs = VM_GetVar("Root/Variables/Chapter5/Scenes/iReachedBiolabs", "N")
    if(iReachedBiolabs == 1.0) then
        
        --Only has one costume.
        local sSX399Img = "Root/Images/AdventureUI/ChatIcons/SX-399Shocktrooper"
        zaDim = {0.0, 0.0, 0.0, 0.0, 180.0, 180.0}
        fnAddCampfireChat("SX-399", "SX-399", sDialoguePath .. "SX399/Campfire.lua", zaDim[1], zaDim[2], zaDim[3], zaDim[4], zaDim[5] + zaDim[3], zaDim[6] + zaDim[4], sSX399Img)
        
    end
    
    -- |[Sophie]|
    --As above, only chattable in the biolabs.
    if(iReachedBiolabs == 1.0) then
        
        --Setup.
        local sSophieImg = "Null"
        local sSophieCostume = VM_GetVar("Root/Variables/Costumes/Sophie/sCostumeGolem", "S")
        
        --Normal work clothes.
        if(sSophieCostume == "Normal") then
            sSophieImg = "Root/Images/AdventureUI/ChatIcons/SophieNormal"
            zaDim = {0.0, 0.0, 0.0, 0.0, 180.0, 180.0}
        
        --Gala dress.
        else
            sSophieImg = "Root/Images/AdventureUI/ChatIcons/SophieGala"
            zaDim = {0.0, 0.0, 0.0, 0.0, 180.0, 180.0}
        end
        
        --Add.
        fnAddCampfireChat("Sophie", "Sophie", sDialoguePath .. "Sophie/Campfire.lua", zaDim[1], zaDim[2], zaDim[3], zaDim[4], zaDim[5] + zaDim[3], zaDim[6] + zaDim[4], sSophieImg)
    end
end

-- |[ ======================================== Chapter 6 ======================================= ]|
-- |[ ========================================== Mods ========================================== ]|
--Mods have the opportunity to add themselves. Each active mod will fire this script, if it exists, and
-- can attempt to add to the campfire options.
fnExecModScript("Campfire Dialogue/Campfire Handler.lua")

-- |[ ========================================= Iterate ======================================== ]|
--Iterate across the party members currently following, checking them against the lookup table.
-- If there's a match, register the chat character.
for i = 1, giFollowersTotal, 1 do

    --Iterate across the lookup table:
    for p = 1, #zaLookups, 1 do
        if(gsaFollowerNames[i] == zaLookups[p].sName) then
            AM_SetProperty("Chat Member", zaLookups[p].sDisplayName, zaLookups[p].sDialogue, zaLookups[p].fOffX, zaLookups[p].fOffY, zaLookups[p].fLft, zaLookups[p].fTop, zaLookups[p].fRgt, zaLookups[p].fBot, zaLookups[p].sIcon)
        end
    end
end

-- |[Clean Up]|
zaLookups = nil
fnAddCampfireChat = nil
