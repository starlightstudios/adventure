-- |[ ==================================== fnDecisionSet() ===================================== ]|
--A set of functions used to create a dialogue decision. It is possible to create a set of decisions via
-- a single call, but because if statements are often used to generate decisions, individual calls
-- are also provided.
--These calls are meant to occur in cutscene stride. The format for psaDecisions is:
-- {{sDecisionDisplayName, sDecisionFireName}, {sDecisionDisplayName, sDecisionFireName}, etc }
function fnDecisionSet(psCallPath, psaDecisions)
    
    -- |[Argument Check]|
    if(psCallPath   == nil) then return end
    if(psaDecisions == nil) then return end
    
    --Decision list has no entries, stop.
    if(#psaDecisions < 1) then return end
    
    -- |[Common]|
    --Activate decision mode.
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    
    -- |[Decisions]|
    --Typically, all decisions call the base script. Use the functions below if different scripts are needed.
    for i = 1, #psaDecisions, 1 do
        
        --Fast-access.
        local sEntryDisplayName = psaDecisions[i][1]
        local sEntryExecuteName = psaDecisions[i][2]
        
        --Apply translation only to the display name.
        sEntryDisplayName = Translate(gsTranslationUI, sEntryDisplayName)
        
        --Assemble a decision string.
        local sString = [[ WD_SetProperty("Add Decision", "]] .. sEntryDisplayName .. [[", "]] .. psCallPath .. [[", "]] .. sEntryExecuteName.. [[") ]]
        
        --Add it in-stride.
        fnCutscene(sString)
        
    end
    
    -- |[Finish]|
    fnCutsceneBlocker()
    
end

-- |[ ================================= Decision Sub-Functions ================================= ]|
--Begins decision mode, using globals to store the call path.
gsActiveCallPath = nil
function fnDecisionBegin(psCallPath)
    
    -- |[Argument Check]|
    if(psCallPath == nil) then return end
    
    -- |[Activity Check]|
    --If the active call path isn't nil, then there may be an open decision sequence. Print a warning.
    if(gsActiveCallPath ~= nil) then
        Debug_ForcePrint("fnDecisionBegin() - Warning, active call path is not nil. There may be an open decision in the code.\n")
    end
    
    -- |[Execution]|
    --Set call path.
    gsActiveCallPath = psCallPath
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
end

--Adds a decision. Can optionally override the execution path, otherwise uses the global call path.
function fnDecisionAdd(psDisplayName, psExecutionName, psCallPath)
    
    -- |[Argument Check]|
    if(psDisplayName   == nil) then return end
    if(psExecutionName == nil) then return end
    
    -- |[Activity Check]|
    --No active path, and no provided call path, fail with a warning.
    if(gsActiveCallPath == nil and psCallPath == nil) then
        Debug_ForcePrint("fnDecisionAdd() - Warning, active call path is nil and no call path was provided. Failing.\n")
        return
    end
    
    -- |[Execution]|
    --If psCallPath is provided, use that. Otherwise use the global.
    local sUsePath = gsActiveCallPath
    if(psCallPath ~= nil) then sUsePath = psCallPath end
    
    --Translate.
    psDisplayName = Translate(gsTranslationUI, psDisplayName)
    
    --Assemble a decision string.
    local sString = [[ WD_SetProperty("Add Decision", "]] .. psDisplayName .. [[", "]] .. sUsePath .. [[", "]] .. psExecutionName.. [[") ]]
    
    --Add it in-stride.
    fnCutscene(sString)
end

--Finishes decisions. Nominally this is just a blocker, but also unsets globals.
function fnDecisionEnd()
    gsActiveCallPath = nil
    fnCutsceneBlocker()
end
