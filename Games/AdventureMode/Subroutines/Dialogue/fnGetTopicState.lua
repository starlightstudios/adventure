-- |[ =================================== fnGetTopicState() ==================================== ]|
--Returns the topic state of the given character with the given name, which is an integer indicating
-- how advanced the topic is. 0 means the topic is unread, or error.
function fnGetTopicState(psTopicName, psCharacterName)
    
    --Arg check.
    if(psTopicName     == nil) then return 0 end
    if(psCharacterName == nil) then return 0 end
    
    --Iterate across all topics.
    local iTopics = WD_GetProperty("Total Topics")
    for i = 0, iTopics-1, 1 do
        
        --Check if the topic matches:
        if(WD_GetProperty("Topic Name", i) == psTopicName) then
            
            --Iterate across all NPCs in this topic:
            local iNPCs = WD_GetProperty("Topic NPCs Total", i)
            for p = 0, iNPCs-1, 1 do
                
                --Check if this NPC name matches:
                if(WD_GetProperty("Topic NPC Name", i, p) == psCharacterName) then
                    
                    --Return the topic level.
                    return WD_GetProperty("Topic NPC Level", i, p)
                end
            end
        end
    end

    --Not found.
    return 0
end