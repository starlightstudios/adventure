-- |[ ================================= fnCreateDialogueActor() ================================ ]|
--Creates an actor for dialogue. This is a pairing of a name, images, and a voice tick. If psaAliasRemapList
-- is provided, remaps for the aliases can be put into the engine, allowing translations to change
-- character names.
--The first entry on psaAliasRemapList is the character's base name, after that it should mirror the
-- alias list. The psaAliasRemapList list should therefore be of size (#psaAliasList+1)
function fnCreateDialogueActor(psActorName, psVoiceSample, psPathPattern, pbIsNotWide, psaAliasList, psaEmotionList, psaAliasRemapList)

    -- |[Arg Check]|
    if(psActorName    == nil) then return end
    if(psVoiceSample  == nil) then return end
    if(psPathPattern  == nil) then return end
    if(pbIsNotWide    == nil) then return end
    if(psaAliasList   == nil) then return end
    if(psaEmotionList == nil) then return end
    
    --psaAliasRemapList is optional.
    
    -- |[Error Check]|
    --If psaAliasRemapList, it MUST have a length equal to #psaAliasList + 1. Failing this will
    -- bark a warning and disable alias remaps.
    if(psaAliasRemapList ~= nil and #psaAliasRemapList ~= #psaAliasList+1) then
        io.write("fnCreateDialogueActor() - Warning. Character: " .. psActorName .. " has Alias Remap list is length " .. #psaAliasRemapList .. " but should be Alias List length +1 (Alias List Length: " .. #psaAliasList .. ")\n")
        psaAliasRemapList = nil
    end
    
    -- |[Create]|
    --Create, or push. If the actor already exists, it is pushed atop the activity stack.
    DialogueActor_Create(psActorName)

        --Alias listing. Always create the same alias as the character's name.
        local sAliasRemap = "DEFAULT"
        if(psaAliasRemapList ~= nil) then sAliasRemap = psaAliasRemapList[1] end
        DialogueActor_SetProperty("Add Alias", psActorName, sAliasRemap)

        --Create additional aliases if the list contains any entries.
        for i = 1, #psaAliasList, 1 do
            if(psaAliasList[i] == "NOALIAS") then break end
            local sAliasRemap = "DEFAULT"
            if(psaAliasRemapList ~= nil) then sAliasRemap = psaAliasRemapList[i+1] end
            DialogueActor_SetProperty("Add Alias", psaAliasList[i], sAliasRemap)
        end

        --Create the neutral emotion. All entities have this. If the saEmotionList contains no entries, then
        -- the neutral emotion is the sPathPattern.
        if(psaEmotionList[1] == nil or psaEmotionList[1] == "NEUTRALISPATH") then
            DialogueActor_SetProperty("Add Emotion", "Neutral", psPathPattern, pbIsNotWide)
        
        --Otherwise, the emotion listing follows the pattern provided.
        else
        
            --The first emotion is "Neutral". This is the case even if that's the only emotion, if you want to use the path pattern.
            DialogueActor_SetProperty("Add Emotion", "Neutral", psPathPattern .. "Neutral", pbIsNotWide)
        
            --Now iterate across the rest of the list.
            for i = 2, #psaEmotionList, 1 do
                DialogueActor_SetProperty("Add Emotion", psaEmotionList[i], psPathPattern .. psaEmotionList[i], pbIsNotWide)
            end
        
        end

    -- |[Clean Up]|
    DL_PopActiveObject()

    -- |[Voices]|
    WD_SetProperty("Register Voice", psActorName, psVoiceSample)
    
end
