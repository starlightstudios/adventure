-- |[Set Party Dialogue]|
--Subscript, sets party positions based on who is in the party. The script uses the gsaFollowerNames table to
-- try to figure out who goes where.

-- |[Differences]|
--Try to figure out the dialogue actor names of following characters, which may be different from their field names.
local saFollowerList = {}
for i = 1, giFollowersTotal, 1 do
    saFollowerList[i] = gsaFollowerNames[i]
    
    if(saFollowerList[i] == "Tiffany") then
        saFollowerList[i] = "Tiffany"
    elseif(saFollowerList[i] == "SX-399") then
        saFollowerList[i] = "SX-399"
    end
end

--Alternately, act on the party leader.
local sUsePartyLeader = gsPartyLeaderName
if(sUsePartyLeader == "Tiffany") then
    sUsePartyLeader = "Tiffany"
elseif(sUsePartyLeader == "SX-399") then
    sUsePartyLeader = "SX-399"
end

-- |[Base]|
--Just the lead character.
if(giFollowersTotal == 0) then
    local sString = "WD_SetProperty(\"Actor In Slot\", 1, \"" .. sUsePartyLeader .."\", \"Neutral\")"
    fnCutscene(sString)

--One follower.
elseif(giFollowersTotal == 1) then
    local sString = "WD_SetProperty(\"Actor In Slot\", 2, \"" .. sUsePartyLeader .. "\", \"Neutral\")"
    fnCutscene(sString)
    sString = "WD_SetProperty(\"Actor In Slot\", 0, \"" .. saFollowerList[1] .. "\", \"Neutral\")"
    fnCutscene(sString)

--Two followers.
elseif(giFollowersTotal == 2) then
    local sString = "WD_SetProperty(\"Actor In Slot\", 2, \"" .. sUsePartyLeader .. "\", \"Neutral\")"
    fnCutscene(sString)
    sString = "WD_SetProperty(\"Actor In Slot\", 1, \"" .. saFollowerList[1] .. "\", \"Neutral\")"
    fnCutscene(sString)
    sString = "WD_SetProperty(\"Actor In Slot\", 0, \"" .. saFollowerList[2] .. "\", \"Neutral\")"
    fnCutscene(sString)

--Three followers.
elseif(giFollowersTotal == 3) then
    local sString = "WD_SetProperty(\"Actor In Slot\", 2, \"" .. sUsePartyLeader .. "\", \"Neutral\")"
    fnCutscene(sString)
    sString = "WD_SetProperty(\"Actor In Slot\", 1, \"" .. saFollowerList[1] .. "\", \"Neutral\")"
    fnCutscene(sString)
    sString = "WD_SetProperty(\"Actor In Slot\", 0, \"" .. saFollowerList[2] .. "\", \"Neutral\")"
    fnCutscene(sString)
    sString = "WD_SetProperty(\"Actor In Slot\", 6, \"" .. saFollowerList[3] .. "\", \"Neutral\")"
    fnCutscene(sString)

end
