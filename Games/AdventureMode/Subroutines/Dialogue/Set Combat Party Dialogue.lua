-- |[ =============================== Set Combat Party Dialogue ================================ ]|
--Subscript, sets party positions based on who is in the party. Uses the combat party, and KO'd
-- characters will not appear.
local saList = {}

-- |[ ======================================== Remapping ======================================= ]|
--Build a remap list. Characters not on this list never appear in a combat party dialogue.
local saRemapping = {}
local function fnAddRemap(psInternalName, psDialogueActor)
    local i = #saRemapping + 1
    saRemapping[i] = {}
    saRemapping[i].sInternalName  = psInternalName
    saRemapping[i].sDialogueActor = psDialogueActor
end

--Build list.
fnAddRemap("Mei", "Mei")
fnAddRemap("Florentina", "Florentina")
fnAddRemap("Christine", "Christine")
fnAddRemap("Tiffany", "Tiffany")
fnAddRemap("SX-399", "SX-399")
fnAddRemap("JX-101", "JX-101")

-- |[ ================================= Get Internal Names ================================ ]|
--Adder function.
local function fnAddToPartyByRemap(psInternalName)
    for i = 1, #saRemapping, 1 do
        if(saRemapping[i].sInternalName == psInternalName) then
            local p = #saList + 1
            saList[p] = saRemapping[i].sDialogueActor
            return
        end
    end
end

--Build a list of actors.
local iPartySize = AdvCombat_GetProperty("Combat Party Size")
for i = 0, iPartySize-1, 1 do

    --Character's ID.
    local iCharacterID = AdvCombat_GetProperty("Combat Party ID", i)
    
    --Push.
    AdvCombat_SetProperty("Push Entity By ID", iCharacterID)

        --Variables
        local sInternalName = AdvCombatEntity_GetProperty("Internal Name")
        local iHealth = AdvCombatEntity_GetProperty("Health")
        
        --Health is over zero, so add them:
        if(iHealth > 0) then
            fnAddToPartyByRemap(sInternalName)
        end

    --Clean.
    DL_PopActiveObject()
end

-- |[ ======================================== Assembly ======================================== ]|
--List size:
local iListSize = #saList

--Debug:
if(false) then
    io.write("Printing party dialogue list.\n")
    for i = 1, iListSize, 1 do
        io.write(" " .. i .. ": " .. saList[i] .. "\n")
    end
end

--Just the lead character.
if(iListSize == 1) then
    local sString = "WD_SetProperty(\"Actor In Slot\", 1, \"" .. saList[1] .."\", \"Neutral\")"
    fnCutscene(sString)

--One follower.
elseif(iListSize == 2) then
    local sString = "WD_SetProperty(\"Actor In Slot\", 2, \"" .. saList[1] .. "\", \"Neutral\")"
    fnCutscene(sString)
    sString = "WD_SetProperty(\"Actor In Slot\", 0, \"" .. saList[2] .. "\", \"Neutral\")"
    fnCutscene(sString)

--Two followers.
elseif(iListSize == 3) then
    local sString = "WD_SetProperty(\"Actor In Slot\", 2, \"" .. saList[1] .. "\", \"Neutral\")"
    fnCutscene(sString)
    sString = "WD_SetProperty(\"Actor In Slot\", 1, \"" .. saList[2] .. "\", \"Neutral\")"
    fnCutscene(sString)
    sString = "WD_SetProperty(\"Actor In Slot\", 0, \"" .. saList[3] .. "\", \"Neutral\")"
    fnCutscene(sString)

--Three followers.
elseif(iListSize == 4) then
    local sString = "WD_SetProperty(\"Actor In Slot\", 2, \"" .. saList[1] .. "\", \"Neutral\")"
    fnCutscene(sString)
    sString = "WD_SetProperty(\"Actor In Slot\", 1, \"" .. saList[2] .. "\", \"Neutral\")"
    fnCutscene(sString)
    sString = "WD_SetProperty(\"Actor In Slot\", 0, \"" .. saList[3] .. "\", \"Neutral\")"
    fnCutscene(sString)
    sString = "WD_SetProperty(\"Actor In Slot\", 6, \"" .. saList[4] .. "\", \"Neutral\")"
    fnCutscene(sString)

end
