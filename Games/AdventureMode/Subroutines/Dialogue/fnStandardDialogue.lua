-- |[Function: fnStandardDialogue]|
--Standard dialogue opener, with optional text argument. Clears, shows, and adds text to the dialogue box. Text is optional.
--Built from: ./ZLaunch.lua
--Example: fnStandardDialogue("Sample Text.")
function fnStandardDialogue(sText)
	
	--Clear and show the text.
	WD_SetProperty("Clear")
	WD_SetProperty("Show")
	
	--If there was text passed in, then append it.
	if(sText ~= nil) then
		Append(sText)
	end
	
end
