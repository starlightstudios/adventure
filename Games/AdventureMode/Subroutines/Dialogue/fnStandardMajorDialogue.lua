-- |[Function: fnStandardMajorDialogue]|
--Once the major dialogue sequence boots up, we need to place the player's party on the left side.
-- Who is in the party is variable for most cutscenes, so this function does the heavy lifting.
--Only the player's party is handled. The right side, which is usually the other party of a dialogue,
-- must still be set manually.
--Built from: ./ZLaunch.lua
--Example: fnStandardMajorDialogue()
function fnStandardMajorDialogue(bIsFast)

	--Optional: Pass this argument (true or false doesn't matter) to make the dialogue show instantly with no fade.
	if(bIsFast ~= nil) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
		
	--Normal:
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	end
	
	--Name of the party leader. Should always exist.
	local sLeaderName = AL_GetProperty("Party Leader Name")
	
	--Count the party members and store their names.
	local iPartyMemberCount = 0
	local saPartyMemberNames = {}
	local sCurrentName =  AL_GetProperty("Follower Name", iPartyMemberCount)
	while(sCurrentName ~= "Null") do
		
		--Store
		iPartyMemberCount = iPartyMemberCount + 1
		saPartyMemberNames[iPartyMemberCount] = sCurrentName
		if(saPartyMemberNames[iPartyMemberCount] == "Tiffany") then
			saPartyMemberNames[iPartyMemberCount] = "Tiffany"
		end
		
		--Next
		sCurrentName =  AL_GetProperty("Follower Name", iPartyMemberCount)
	end
	
	--If there are no party members, put the leader in the 1th slot.
	if(iPartyMemberCount == 0) then
		fnCutscene( "WD_SetProperty(\"Actor In Slot\", 1, \"" .. sLeaderName .. "\") ")
		fnCutscene( "WD_SetProperty(\"Actor Emotion\", \"" .. sLeaderName .. "\", \"Neutral\") ")
	
	--If there is one follower then the leader takes slot 2 and the follower takes slot 0.
	elseif(iPartyMemberCount == 1) then
		fnCutscene( "WD_SetProperty(\"Actor In Slot\", 2, \"" .. sLeaderName .. "\") ")
		fnCutscene( "WD_SetProperty(\"Actor Emotion\", \"" .. sLeaderName .. "\", \"Neutral\") ")
		fnCutscene( "WD_SetProperty(\"Actor In Slot\", 0, \"" .. saPartyMemberNames[1] .. "\") ")
		fnCutscene( "WD_SetProperty(\"Actor Emotion\", \"" .. saPartyMemberNames[1] .. "\", \"Neutral\") ")
	
	--Two followers, fill the slots.
	else
		fnCutscene(" WD_SetProperty(\"Actor In Slot\", 2, \"" .. sLeaderName .. "\") ")
		fnCutscene(" WD_SetProperty(\"Actor Emotion\", " .. sLeaderName .. "\", \"Neutral\") ")
		fnCutscene(" WD_SetProperty(\"Actor In Slot\", 1, \"" .. saPartyMemberNames[1] .. ") ")
		fnCutscene(" WD_SetProperty(\"Actor Emotion\", " .. saPartyMemberNames[1] .. "\", \"Neutral\") ")
		fnCutscene(" WD_SetProperty(\"Actor In Slot\", 0, \"" .. saPartyMemberNames[2] .. ") ")
		fnCutscene(" WD_SetProperty(\"Actor Emotion\", " .. saPartyMemberNames[2] .. "\", \"Neutral\") ")
	end

end
