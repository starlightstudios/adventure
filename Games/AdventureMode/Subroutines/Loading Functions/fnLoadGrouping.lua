-- |[ ===================================== fnLoadGrouping ===================================== ]|
--Given a list, order the delayed-load bitmaps in the group that matches the name to load.
-- This is often used with costumes to group and ungroup image loading by costume.
--Execute this after unloading images, as some costumes share images between them.
function fnLoadGrouping(psaGroupingList, psName)
    
    --Argument check.
    if(psaGroupingList == nil) then return end
    if(psName          == nil) then return end
    
    --Iterate. Load the group that matches the name.
    --io.write("Loading grouping " .. psName .. "\n")
    for i = 1, #psaGroupingList, 1 do
        if(psaGroupingList[i] == psName) then
            fnLoadDelayedBitmapsFromList(psaGroupingList[i], gciDelayedLoadLoadAtEndOfTick)
            return
        end
    end

end
