-- |[ ================================ fnUnloadBitmapsFromList() =============================== ]|
--Orders a list of images to drop their pixel data. Used to save space for images not in use.
-- The images can be ordered to re-load their data with fnLoadDelayedBitmapsFromList().
function fnUnloadBitmapsFromList(psListName)
    
    -- |[Debug]|
    --Debug check. Blocks all unloading actions.
    if(gbAlwaysLoadImmediately == true) then return end
    
    -- |[Setup]|
    --Argument check.
    if(psListName == nil) then return end
    
    -- |[Find List Entry]|
    --Check for a matching list entry. If there is no entry, bark an error.
    local zEntry = nil
    for p = 1, #gzaDelayedLoadList, 1 do
        if(gzaDelayedLoadList[p].sName == psListName) then
            zEntry = gzaDelayedLoadList[p]
            break
        end
    end
    
    --No entry on the list.
    if(zEntry == nil) then
        if(gbSuppressNoListErrors == false) then
            io.write("fnUnloadBitmapsFromList(): Warning, no list " .. psListName .. " was found.\n")
        end
        return
    end
    
    -- |[Execute Load Action]|
    --Order all entries on the list to load.
    for i = 1, #zEntry.saList, 1 do
        DL_UnloadBitmap(zEntry.saList[i])
    end
    
end
