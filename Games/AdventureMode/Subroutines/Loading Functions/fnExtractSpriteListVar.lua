-- |[ ================================ fnExtractSpriteListVar() ================================ ]|
--Same as fnExtractSpriteList(), except allows the user to specify how many frames are in the walk cycle.
-- Clamps between 1 and TA_MOVE_IMG_MAXIMUM.
function fnExtractSpriteList(saNameList, bIsEightDirections, piMoveFramesTotal, sNamePrefix)
	
	-- |[Argument Check]|
	if(saNameList == nil) then return end
	if(bIsEightDirections == nil) then bIsEightDirections = false end
	--sNamePrefix can be nil. It is used for characters with multiple forms.
	
    -- |[Load Execution]|
    for i = 1, #saNameList, 1 do
		
		--The word SKIP indicates we do not rip this. It's a placeholder for characters who will eventually
		-- fill their form list in.
		if(saNameList[i] == "SKIP") then
		
		--No prefix.
		elseif(sNamePrefix == nil) then
			fnLoadCharacterGraphics(saNameList[i], "Root/Images/Sprites/" .. saNameList[i] .. "/", bIsEightDirections)
			
		--Prefix.
		else
			fnLoadCharacterGraphics(sNamePrefix .. saNameList[i], "Root/Images/Sprites/" .. sNamePrefix .. saNameList[i] .. "/", bIsEightDirections)
		end
	end
end