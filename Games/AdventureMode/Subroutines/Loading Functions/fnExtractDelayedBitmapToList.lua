-- |[ ============================= fnExtractDelayedBitmapToList() ============================= ]|
--When loading delayed bitmaps, the function DL_ExtractDelayedBitmap() causes the bitmap to be delayed.
-- This function allows us to cluster similar bitmaps together.
--The function fnLoadDelayedBitmapsFromList() causes them all to load at once.
function fnExtractDelayedBitmapToList(psListName, psSLFName, psDLPath)
    
    -- |[Setup]|
    --Argument check.
    if(psListName == nil) then return end
    if(psSLFName  == nil) then return end
    if(psDLPath   == nil) then return end
    
    -- |[Find List Entry]|
    --Check for a matching list entry. If there is no entry, bark an error.
    local zEntry = nil
    for p = 1, #gzaDelayedLoadList, 1 do
        if(gzaDelayedLoadList[p].sName == psListName) then
            zEntry = gzaDelayedLoadList[p]
            break
        end
    end
    
    --No entry on the list.
    if(zEntry == nil) then
        
        --Debug.
        io.write("fnExtractDelayedBitmapToList(): Warning, no list " .. psListName .. " was found.\n")
        
        --Diagnostics.
        if(false) then
            io.write("List of lists:\n")
            for p = 1, #gzaDelayedLoadList, 1 do
                io.write(" " .. p .. ": " .. gzaDelayedLoadList[p].sName .. "\n")
            end
        end
        
        return
    end
    
    -- |[Execute Load Action]|
    --Calling this function causes the engine to create the matching bitmap in the DataLibrary.
    if(gbAlwaysLoadImmediately ~= true) then
        DL_ExtractDelayedBitmap(psSLFName, psDLPath)
    
    --Debug override, load the image immediately.
    else
        DL_ExtractBitmap(psSLFName, psDLPath)
    end
    
    -- |[Add To Named List]|
    --The DLPath is stored to our internal list. When fnLoadDelayedBitmapsFromList() is called, all
    -- bitmaps on the list will load their data.
    --First, make sure this entry isn't already on the list.
    for i = 1, #zEntry.saList, 1 do
        if(zEntry.saList[i] == psDLPath) then
            return
        end
    end
    
    --If we got this far, no duplicate. Add it.
    local q = #zEntry.saList + 1
    zEntry.saList[q] = psDLPath
    --io.write(" List " .. psListName .. " added " .. psSLFName .. "\n")
    
end
