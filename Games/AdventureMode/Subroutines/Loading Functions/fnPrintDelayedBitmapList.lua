-- |[ ================================ fnPrintDelayedBitmapList ================================ ]|
--Prints the contents of the named delayed bitmap list.
function fnPrintDelayedBitmapList(psListName)
    
    -- |[Setup]|
    --Argument check.
    if(psListName == nil) then return end
    
    -- |[Find List Entry]|
    --Check for a matching list entry. If there is no entry, bark an error.
    local zEntry = nil
    for p = 1, #gzaDelayedLoadList, 1 do
        if(gzaDelayedLoadList[p].sName == psListName) then
            zEntry = gzaDelayedLoadList[p]
            break
        end
    end
    
    --No entry on the list.
    if(zEntry == nil) then
        io.write("fnPrintDelayedBitmapList(): Warning, no list " .. psListName .. " was found.\n")
        return
    end
    
    -- |[Print]|
    --Display list size.
    io.write("List " .. psListName .. " has " .. #zEntry.saList .. " entries.\n")
    for i = 1, #zEntry.saList, 1 do
        io.write(" " .. zEntry.saList[i] .. "\n")
    end
    
end
