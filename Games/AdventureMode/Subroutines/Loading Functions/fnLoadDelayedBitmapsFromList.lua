-- |[ ============================== fnLoadDelayedBitmapsFromList ============================== ]|
--When images are on a delayed-load list, this function causes all of the bitmaps on the matching
-- list to immediately load their resources. This can optionally be done via enqueuing, direct load,
-- or "Don't care" which allows the engine to pick.
--If the override is not specified, "Don't Care" is used.
function fnLoadDelayedBitmapsFromList(psListName, piOverride)
    
    -- |[Setup]|
    --Argument check.
    if(psListName == nil) then return end
    if(piOverride == nil) then piOverride = gciDelayedLoadDontCare end
    
    -- |[Find List Entry]|
    --Check for a matching list entry. If there is no entry, bark an error.
    local zEntry = nil
    for p = 1, #gzaDelayedLoadList, 1 do
        if(gzaDelayedLoadList[p].sName == psListName) then
            zEntry = gzaDelayedLoadList[p]
            break
        end
    end
    
    --No entry on the list.
    if(zEntry == nil) then
        if(gbSuppressNoListErrors == false) then
            io.write("fnLoadDelayedBitmapsFromList(): Warning, no list " .. psListName .. " was found.\n")
        end
        return
    end
    
    -- |[Execute Load Action]|
    --Order all entries on the list to load.
    for i = 1, #zEntry.saList, 1 do
        DL_LoadDelayedBitmap(zEntry.saList[i], piOverride)
    end
    
end
