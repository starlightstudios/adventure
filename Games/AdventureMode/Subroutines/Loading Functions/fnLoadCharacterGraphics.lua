-- |[ =============================== fnLoadCharacterGraphics() ================================ ]|
--Loads character graphics from Sprites.slf given a pattern and uploads them to the datalibrary. Will
-- not double-load any images that is already in the library.
--Example: fnLoadCharacterGraphics("Mei", "Root/Images/Sprites/Mei/", true)
function fnLoadCharacterGraphics(sCharacterName, sDLPath, bUsesEightDirections)
	
	-- |[Argument Check]|
	if(sCharacterName       == nil) then return end
	if(sDLPath              == nil) then return end
	if(bUsesEightDirections == nil) then return end
	
    -- |[Standard Loading Set]|
	--Standardized loading set.
	local saSets =   {     "South",      "North",       "West",       "East"}
	local iaFrames = {giMoveFrames, giMoveFrames, giMoveFrames, giMoveFrames}
	if(bUsesEightDirections == true) then
		saSets =   {     "South",      "North",       "West",       "East",         "NE",         "NW",         "SW",         "SE"}
		iaFrames = {giMoveFrames, giMoveFrames, giMoveFrames, giMoveFrames, giMoveFrames, giMoveFrames, giMoveFrames, giMoveFrames}
	end
	
    -- |[Setup]|
	--Add the DL directory if it doesn't exist yet.
	DL_AddPath(sDLPath)
	
    -- |[Load Execution]|
	--Loading loop.
    for i = 1, #saSets, 1 do
		for p = 1, iaFrames[i], 1 do
			
			--Setup.
			local sLoadName = sCharacterName .. "|" .. saSets[i] .. "|" .. (p-1)
			local sLoadPath = sDLPath .. saSets[i] .. "|" .. (p-1)
			
			--Entry already exists, don't load it. This is actually handled internally now.
			--if(DL_Exists(sLoadPath) == false) then
				DL_ExtractBitmap(sLoadName, sLoadPath)
			--end
		end
	end
	
    -- |[Special Frame Set]|
	--If this belong to one of the special frame sets, we also load the running animations.
	-- Not all entities use this, only ones that join the player party. The two exceptions
	-- are Breanne and Sophie, who have 8-directions but aren't in the party.
	--The one exception is the Darkmatter Girls, who don't run or use their 8-directional cases
	-- except for plot-related scenes.
	if(bUsesEightDirections and (sCharacterName ~= "DarkmatterGirl" and sCharacterName ~= "DarkmatterGirlParagon")) then
        
        --Iterate.
        for i = 1, #saSets, 1 do
			for p = 1, iaFrames[i], 1 do
				
				--Setup.
				local sLoadName = sCharacterName .. "|" .. saSets[i] .. "|Run" .. (p-1)
				local sLoadPath = sDLPath .. saSets[i] .. "|Run" .. (p-1)
				
				--Entry already exists, don't load it. This is actually handled internally now.
				--if(DL_Exists(sLoadPath) == false) then
					DL_ExtractBitmap(sLoadName, sLoadPath)
				--end
			end
		end
		
	end
end

-- |[ ============================== fnLoadCharacterGraphicsEW() =============================== ]|
--Loads only the west and east directions. Some NPCs only show these because they are animals.
function fnLoadCharacterGraphicsEW(sCharacterName, sDLPath)
	
	-- |[Argument Check]|
	if(sCharacterName == nil) then return end
	if(sDLPath        == nil) then return end
	
    -- |[Setup]|
	--Standardized loading set.
	local saSets =   {      "West",       "East"}
	local iaFrames = {giMoveFrames, giMoveFrames}

	--Add the DL directory if it doesn't exist yet.
	DL_AddPath(sDLPath)
	
    -- |[Execution]|
	--Loading loop.
    for i = 1, #saSets, 1 do
		for p = 1, iaFrames[i], 1 do
			
			--Setup.
			local sLoadName = sCharacterName .. "|" .. saSets[i] .. "|" .. (p-1)
			local sLoadPath = sDLPath .. saSets[i] .. "|" .. (p-1)
			
			--Entry already exists, don't load it.
			--if(DL_Exists(sLoadPath) == false) then
				DL_ExtractBitmap(sLoadName, sLoadPath)
			--end
		end
	end
end

-- |[ ============================ fnLoadCharacterGraphicsFacing() ============================= ]|
--Loads the facing directions only. Used for NPCs who never move.
--Example: fnLoadCharacterGraphics("GolemFancyA", "Root/Images/Sprites/GolemFancyA/")
function fnLoadCharacterGraphicsFacing(sCharacterName, sDLPath)
	
	-- |[Argument Check]|
	if(sCharacterName       == nil) then return end
	if(sDLPath              == nil) then return end
	
    -- |[Setup]|
	--Standardized loading set.
	local saSets =   {"South", "North", "West", "East"}
	local iaFrames = {      1,       1,      1,      1}
	
	--Add the DL directory if it doesn't exist yet.
	DL_AddPath(sDLPath)
	
    -- |[Execute Load]|
    for i = 1, #saSets, 1 do
		for p = 1, iaFrames[i], 1 do
			
			--Setup.
			local sLoadName = sCharacterName .. "|" .. saSets[i] .. "|" .. (p-1)
			local sLoadPath = sDLPath .. saSets[i] .. "|" .. (p-1)
			
			--Entry already exists, don't load it.
			--if(DL_Exists(sLoadPath) == false) then
				DL_ExtractBitmap(sLoadName, sLoadPath)
			--end
		end
	end
end
