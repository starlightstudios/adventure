-- |[ =================================== fnExtractSetList() =================================== ]|
--Given a list of names, and how many frames there are, extracts the images to the DataLibrary in
-- the specified location.
--Example:
--local saSets   = {"ThrowSouth", "ThrowNorth", "ThrowWest", "ThrowEast"}
--local iaFrames = {           6,            6,           6,           6}
--local sDLPath = "Root/Images/Sprites/Special/Doll_Pink|"
function fnExtractSetList(psaSets, piaFrames, psPrefix, psDLPath)
    
    -- |[Argument Check]|
    if(psaSets   == nil) then return end
    if(piaFrames == nil) then return end
    if(psPrefix  == nil) then return end
    if(psDLPath  == nil) then return end
    
    -- |[Extraction]|
    for i = 1, #psaSets, 1 do
        
        --Get how many frames are in this set.
        local iFramesToRip = piaFrames[i]
        
        --For each frame, resolve name and extract.
        for p = 1, iFramesToRip, 1 do
            local sUseName = psaSets[i] .. "|" .. p-1
            DL_ExtractBitmap(psPrefix .. sUseName, psDLPath .. sUseName)
            --io.write(psPrefix .. sUseName .. " -> " .. psDLPath .. sUseName .. "\n")
        end
    end

end