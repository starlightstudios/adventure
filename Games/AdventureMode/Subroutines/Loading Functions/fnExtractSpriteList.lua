-- |[ ================================== fnExtractSpriteList() ================================= ]|
--Loads sprites by name. Uses lists built in 000 Variables.lua, to better compartmentalize loading.
-- All characters within the same list are expected to share the same eight-directions case.
-- Thus, NPCs and Enemies need their own lists, separated from the party.
function fnExtractSpriteList(saNameList, bIsEightDirections, sNamePrefix)
	
	-- |[Argument Check]|
	if(saNameList == nil) then return end
	if(bIsEightDirections == nil) then bIsEightDirections = false end
	--sNamePrefix can be nil. It is used for characters with multiple forms.
	
    -- |[Load Execution]|
    for i = 1, #saNameList, 1 do
		
		--The word SKIP indicates we do not rip this. It's a placeholder for characters who will eventually
		-- fill their form list in.
		if(saNameList[i] == "SKIP") then
		
		--No prefix.
		elseif(sNamePrefix == nil) then
			fnLoadCharacterGraphics(saNameList[i], "Root/Images/Sprites/" .. saNameList[i] .. "/", bIsEightDirections)
			
		--Prefix.
		else
			fnLoadCharacterGraphics(sNamePrefix .. saNameList[i], "Root/Images/Sprites/" .. sNamePrefix .. saNameList[i] .. "/", bIsEightDirections)
		end
	end
end