-- |[ ================================ fnExtractSpecialFrames() ================================ ]|
--Loads sprites in the special frame listing. For example, most player characters have  "Name_Human|Crouch" special frames.
-- This function extracts all with the same pattern from a list.
--Presently only two special frames are handled. The function can be modified to have more.
gbExtractDebug = false
function fnExtractSpecialFrames(saNameList, sSpecialFrameA, sSpecialFrameB)
	
	-- |[Argument Check]|
	if(saNameList     == nil) then return end
	if(sSpecialFrameA == nil) then return end
	--sSpecialFrameB can be nil, in which case only one special frame is loaded.
	
	-- |[Execute Load]|
    for i = 1, #saNameList, 1 do
        
        --Ignore SKIP.
		if(saNameList[i] == "SKIP") then
		
        else
        
            --Always load frame A.
            if(gbExtractDebug == true) then
                io.write("Extract Special Frames: " .. "Spcl|" .. saNameList[i] .. "|" .. sSpecialFrameA .. " to " .. "Root/Images/Sprites/Special/" .. saNameList[i] .. "|" .. sSpecialFrameA .. "\n")
            end
            DL_ExtractBitmap("Spcl|" .. saNameList[i] .. "|" .. sSpecialFrameA,  "Root/Images/Sprites/Special/" .. saNameList[i] .. "|" .. sSpecialFrameA)
            if(gbExtractDebug == true) then
                DL_ReportBitmap("Root/Images/Sprites/Special/" .. saNameList[i] .. "|" .. sSpecialFrameA)
            end
            
            --Load frame B if provided.
            if(sSpecialFrameB ~= nil) then
                DL_ExtractBitmap("Spcl|" .. saNameList[i] .. "|" .. sSpecialFrameB,  "Root/Images/Sprites/Special/" .. saNameList[i] .. "|" .. sSpecialFrameB)
            end
		end
	end
end
