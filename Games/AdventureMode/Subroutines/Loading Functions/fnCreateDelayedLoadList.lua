-- |[ =============================== fnCreateDelayedLoadList() ================================ ]|
--Creates a new delayed load listing. When loading bitmaps, you can optionally have the engine
-- store which file they were in and their name, and load the actual data later on-demand.
--Because similar images often load together in groups, we create named sets of these images.
-- With a single function we can order all of these images to load at once.
function fnCreateDelayedLoadList(psListName)
    
    --Argument check.
    if(psListName == nil) then return end
    
    --Check if there is already a matching entry.
    --io.write("Creating delayed load list " .. psListName .. "\n")
    for p = 1, #gzaDelayedLoadList, 1 do
        if(gzaDelayedLoadList[p].sName == psListName) then
            --io.write(" Duplicate!\n")
            return
        end
    end
    
    --Create.
    local i = #gzaDelayedLoadList + 1
    gzaDelayedLoadList[i] = {}
    gzaDelayedLoadList[i].sName = psListName
    gzaDelayedLoadList[i].saList = {}
    
    --Debug.
    --io.write("Created a delayed load list: " .. psListName .. "\n")
    
end
