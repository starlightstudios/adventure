-- |[ ==================================== fnUnloadGrouping ==================================== ]|
--Given a list, order all groupings in the list except the one matching the given name to unload.
-- Often used with costume groupings to batch-load related images.
function fnUnloadGrouping(psaGroupingList, psName)
    
    --Debug check. Blocks all unloading actions.
    if(gbAlwaysLoadImmediately == true) then return end
    
    --Argument check.
    if(psaGroupingList == nil) then return end
    if(psName          == nil) then return end
    
    --Iterate. Unload all non-matching groups.
    for i = 1, #psaGroupingList, 1 do
        if(psaGroupingList[i] ~= psName) then
            fnUnloadBitmapsFromList(psaGroupingList[i])
        end
    end
    
    --Debug
    --io.write("Unloaded all except grouping " .. psName .. "\n")

end
