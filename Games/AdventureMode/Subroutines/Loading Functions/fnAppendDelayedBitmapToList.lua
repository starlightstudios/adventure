-- |[ ============================= fnAppendDelayedBitmapToList() ============================= ]|
--Appends the bitmap path to the given list, but does not attempt to load it. Used if the same
-- image appears in multiple lists.
function fnAppendDelayedBitmapToList(psListName, psSLFName, psDLPath)
    
    -- |[Setup]|
    --Argument check.
    if(psListName == nil) then return end
    if(psSLFName  == nil) then return end
    if(psDLPath   == nil) then return end
    
    -- |[Find List Entry]|
    --Check for a matching list entry. If there is no entry, bark an error.
    local zEntry = nil
    for p = 1, #gzaDelayedLoadList, 1 do
        if(gzaDelayedLoadList[p].sName == psListName) then
            zEntry = gzaDelayedLoadList[p]
            break
        end
    end
    
    --No entry on the list.
    if(zEntry == nil) then
        io.write("fnExtractDelayedBitmapToList(): Warning, no list " .. psListName .. " was found.\n")
        return
    end
    
    -- |[Add To Named List]|
    --The DLPath is stored to our internal list. When fnLoadDelayedBitmapsFromList() is called, all
    -- bitmaps on the list will load their data.
    --First, make sure this entry isn't already on the list.
    for i = 1, #zEntry.saList, 1 do
        if(zEntry.saList[i] == psDLPath) then
            return
        end
    end
    
    --If we got this far, no duplicate. Add it.
    local q = #zEntry.saList + 1
    zEntry.saList[q] = psDLPath
    
end
