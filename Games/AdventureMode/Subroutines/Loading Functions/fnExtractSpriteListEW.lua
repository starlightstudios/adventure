-- |[ ================================ fnExtractSpriteListEW() ================================= ]|
--Variation of fnExtractSpriteList() that uses East/West loader only.
function fnExtractSpriteListEW(saNameList)
	
	-- |[Argument Check]|
	if(saNameList == nil) then return end
	
    -- |[Execute Load]|
    for i = 1, #saNameList, 1 do
		
		--The word SKIP indicates we do not rip this. It's a placeholder for characters who will eventually
		-- fill their form list in.
		if(saNameList[i] == "SKIP") then
		
		--Normal.
		else
			fnLoadCharacterGraphicsEW(saNameList[i], "Root/Images/Sprites/" .. saNameList[i] .. "/")
		end
	end
end