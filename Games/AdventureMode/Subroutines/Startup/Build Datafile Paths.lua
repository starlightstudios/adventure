-- |[ ================================== Build Datafile Paths ================================== ]|
--Called at startup from the launcher, builds a list of datafile paths.
--This should only be called once at startup.
local bUseLowResMode = OM_GetOption("LowResAdventureMode")

-- |[ ==================================== Function Builder ==================================== ]|
-- |[Normal Path Function]|
local function fnCreatePathNormal(psPath)
    return psPath
end
    
-- |[Low-Res Path Function]|
local function fnCreatePathLoDef(psPath)
    
    -- |[Argument Check]|
    if(psPath == nil) then return "NO PATH" end
    
    -- |[Existence Check]|
    --Check if the LoDef version of the datafile exists.
    local sLocalPath = ""
    
    --If the path starts with "Portraits_", try "PortraitsLD_"
    if(string.sub(psPath, 1, 10) == "Portraits_") then
        sLocalPath = "PortraitsLD_" .. string.sub(psPath, 11)
    
    --If the path starts with "AdvScn_", try "AdvScnLD_"
    elseif(string.sub(psPath, 1, 7) == "AdvScn_") then
        sLocalPath = "AdvScnLD_" .. string.sub(psPath, 8)
    
    --If the path starts with "Maps_", try "MapsLD_"
    elseif(string.sub(psPath, 1, 5) == "Maps_") then
        sLocalPath = "MapsLD_" .. string.sub(psPath, 6)
    
    --No special case found, so use the provided path directly.
    else
        sLocalPath = psPath
    end
    
    --Ask the filesystem if this file exists.
    local sCheckPath = gsDatafilesPath .. sLocalPath
    if(FS_Exists(sCheckPath) == true) then
        return sLocalPath
    end
    
    -- |[Not Found]|
    --The low-def file does not exist. Return the provided path without additions.
    return psPath
end

-- |[Resolve Function]|
--Determine which function to use.
local fnCreatePath = fnCreatePathNormal
if(bUseLowResMode == true) then
    fnCreatePath = fnCreatePathLoDef
end

-- |[ ====================================== List Building ===================================== ]|
-- |[Adventure Scenes]|
gsaDatafilePaths.sScnCassandraTF = gsDatafilesPath .. fnCreatePath("AdvScn_CassandraTF.slf")
gsaDatafilePaths.sScnCh0Major    = gsDatafilesPath .. fnCreatePath("AdvScn_CH0Major.slf")
gsaDatafilePaths.sScnCh1Major    = gsDatafilesPath .. fnCreatePath("AdvScn_CH1Major.slf")
gsaDatafilePaths.sScnCh2Major    = gsDatafilesPath .. fnCreatePath("AdvScn_CH2Major.slf")
gsaDatafilePaths.sScnCh5Major    = gsDatafilesPath .. fnCreatePath("AdvScn_CH5Major.slf")
gsaDatafilePaths.sScnChristineTF = gsDatafilesPath .. fnCreatePath("AdvScn_ChristineTF.slf")
gsaDatafilePaths.sScnGalaDress   = gsDatafilesPath .. fnCreatePath("AdvScn_GalaDress.slf")
gsaDatafilePaths.sScnMeiRune     = gsDatafilesPath .. fnCreatePath("AdvScn_MeiRune.slf")
gsaDatafilePaths.sScnMeiTF       = gsDatafilesPath .. fnCreatePath("AdvScn_MeiTF.slf")

-- |[Sprites]|
gsaDatafilePaths.sSprites = gsDatafilesPath .. fnCreatePath("Sprites.slf")

-- |[Portraits]|
gsaDatafilePaths.s56Path             = gsDatafilesPath .. fnCreatePath("Portraits_56.slf")
gsaDatafilePaths.sAquilliaPath       = gsDatafilesPath .. fnCreatePath("Portraits_Aquillia.slf")
gsaDatafilePaths.sCassandraPath      = gsDatafilesPath .. fnCreatePath("Portraits_Cassandra.slf")
gsaDatafilePaths.sChapter0CombatPath = gsDatafilesPath .. fnCreatePath("Portraits_CH0Combat.slf")
gsaDatafilePaths.sChapter0EmotePath  = gsDatafilesPath .. fnCreatePath("Portraits_CH0Emote.slf")
gsaDatafilePaths.sChapter1CombatPath = gsDatafilesPath .. fnCreatePath("Portraits_CH1Combat.slf")
gsaDatafilePaths.sChapter1EmotePath  = gsDatafilesPath .. fnCreatePath("Portraits_CH1Emote.slf")
gsaDatafilePaths.sChapter2CombatPath = gsDatafilesPath .. fnCreatePath("Portraits_CH2Combat.slf")
gsaDatafilePaths.sChapter2EmotePath  = gsDatafilesPath .. fnCreatePath("Portraits_CH2Emote.slf")
gsaDatafilePaths.sChapter5CombatPath = gsDatafilesPath .. fnCreatePath("Portraits_CH5Combat.slf")
gsaDatafilePaths.sChapter5EmotePath  = gsDatafilesPath .. fnCreatePath("Portraits_CH5Emote.slf")
gsaDatafilePaths.sChristinePath      = gsDatafilesPath .. fnCreatePath("Portraits_Christine.slf")
gsaDatafilePaths.sEmpressPath        = gsDatafilesPath .. fnCreatePath("Portraits_Empress.slf")
gsaDatafilePaths.sFlorentinaPath     = gsDatafilesPath .. fnCreatePath("Portraits_Florentina.slf")
gsaDatafilePaths.sIzunaPath          = gsDatafilesPath .. fnCreatePath("Portraits_Izuna.slf")
gsaDatafilePaths.sJX101Path          = gsDatafilesPath .. fnCreatePath("Portraits_JX101.slf")
gsaDatafilePaths.sMarriedraunesPath  = gsDatafilesPath .. fnCreatePath("Portraits_Marriedraunes.slf")
gsaDatafilePaths.sMaramPath          = gsDatafilesPath .. fnCreatePath("Portraits_Maram.slf")
gsaDatafilePaths.sMeiPath            = gsDatafilesPath .. fnCreatePath("Portraits_Mei.slf")
gsaDatafilePaths.sMiaPath            = gsDatafilesPath .. fnCreatePath("Portraits_Mia.slf")
gsaDatafilePaths.sMisoPath           = gsDatafilesPath .. fnCreatePath("Portraits_Miso.slf")
gsaDatafilePaths.sMihoPath           = gsDatafilesPath .. fnCreatePath("Portraits_Miho.slf")
gsaDatafilePaths.sOdarPath           = gsDatafilesPath .. fnCreatePath("Portraits_Odar.slf")
gsaDatafilePaths.sPDUPath            = gsDatafilesPath .. fnCreatePath("Portraits_PDU.slf")
gsaDatafilePaths.sPolarisPath        = gsDatafilesPath .. fnCreatePath("Portraits_Polaris.slf")
gsaDatafilePaths.sSammyPath          = gsDatafilesPath .. fnCreatePath("Portraits_Sammy.slf")
gsaDatafilePaths.sSanyaPath          = gsDatafilesPath .. fnCreatePath("Portraits_Sanya.slf")
gsaDatafilePaths.sSeptimaPath        = gsDatafilesPath .. fnCreatePath("Portraits_Septima.slf")
gsaDatafilePaths.sSharelockPath      = gsDatafilesPath .. fnCreatePath("Portraits_Sharelock.slf")
gsaDatafilePaths.sSophiePath         = gsDatafilesPath .. fnCreatePath("Portraits_Sophie.slf")
gsaDatafilePaths.sSX399Path          = gsDatafilesPath .. fnCreatePath("Portraits_SX399.slf")
gsaDatafilePaths.sTiffanyPath        = gsDatafilesPath .. fnCreatePath("Portraits_Tiffany.slf")
gsaDatafilePaths.sYukinaPath         = gsDatafilesPath .. fnCreatePath("Portraits_Yukina.slf")
gsaDatafilePaths.sZekePath           = gsDatafilesPath .. fnCreatePath("Portraits_Zeke.slf")

-- |[Maps]|
gsaDatafilePaths.sMapCh1Trannadar     = gsDatafilesPath .. fnCreatePath("Maps_Ch1_Trannadar.slf")
gsaDatafilePaths.sMapCh1TrannadarWest = gsDatafilesPath .. fnCreatePath("Maps_Ch1_TrannadarWest.slf")
gsaDatafilePaths.sMapCh1Mausoleum     = gsDatafilesPath .. fnCreatePath("Maps_Ch1_Mausoleum.slf")
gsaDatafilePaths.sMapBiolabs       = gsDatafilesPath .. fnCreatePath("Maps_Biolabs.slf")
gsaDatafilePaths.sMapCRTNoise      = gsDatafilesPath .. fnCreatePath("Maps_CRTNoise.slf")
gsaDatafilePaths.sMapCryoLower     = gsDatafilesPath .. fnCreatePath("Maps_CryoLower.slf")
gsaDatafilePaths.sMapCryoMain      = gsDatafilesPath .. fnCreatePath("Maps_CryoMain.slf")
gsaDatafilePaths.sMapEquinox       = gsDatafilesPath .. fnCreatePath("Maps_Equinox.slf")
gsaDatafilePaths.sMapLRTEast       = gsDatafilesPath .. fnCreatePath("Maps_LRTEast.slf")
gsaDatafilePaths.sMapLRTWest       = gsDatafilesPath .. fnCreatePath("Maps_LRTWest.slf")
gsaDatafilePaths.sMapNixNedar      = gsDatafilesPath .. fnCreatePath("Maps_NixNedar.slf")
gsaDatafilePaths.sMapNorthwoods    = gsDatafilesPath .. fnCreatePath("Maps_Northwoods.slf")
gsaDatafilePaths.sMapRegulus       = gsDatafilesPath .. fnCreatePath("Maps_Regulus.slf")
gsaDatafilePaths.sMapSystem        = gsDatafilesPath .. fnCreatePath("Maps_System.slf")
gsaDatafilePaths.sMapWestwoods     = gsDatafilesPath .. fnCreatePath("Maps_Westwoods.slf")
gsaDatafilePaths.sMapMtSarulente   = gsDatafilesPath .. fnCreatePath("Maps_MtSarulente.slf")

-- |[UI]|
gsaDatafilePaths.sAdvCombat               = gsDatafilesPath .. fnCreatePath("AdvCombat.slf")
gsaDatafilePaths.sUIAdventure             = gsDatafilesPath .. fnCreatePath("UIAdventure.slf")
gsaDatafilePaths.sUIAdvIcons              = gsDatafilesPath .. fnCreatePath("UIAdvIcons.slf")
gsaDatafilePaths.sUIAdvMenuBase           = gsDatafilesPath .. fnCreatePath("UIAdvMenuBase.slf")
gsaDatafilePaths.sUIAdvMenuCampfire       = gsDatafilesPath .. fnCreatePath("UIAdvMenuCampfire.slf")
gsaDatafilePaths.sUIAdvMenuDoctor         = gsDatafilesPath .. fnCreatePath("UIAdvMenuDoctor.slf")
gsaDatafilePaths.sUIAdvMenuEquipment      = gsDatafilesPath .. fnCreatePath("UIAdvMenuEquipment.slf")
gsaDatafilePaths.sUIAdvMenuFieldAbilities = gsDatafilesPath .. fnCreatePath("UIAdvMenuFieldAbilities.slf")
gsaDatafilePaths.sUIAdvMenuFileSelect     = gsDatafilesPath .. fnCreatePath("UIAdvMenuFileSelect.slf")
gsaDatafilePaths.sUIAdvMenuInventory      = gsDatafilesPath .. fnCreatePath("UIAdvMenuInventory.slf")
gsaDatafilePaths.sUIAdvMenuJournal        = gsDatafilesPath .. fnCreatePath("UIAdvMenuJournal.slf")
gsaDatafilePaths.sUIAdvMenuOptions        = gsDatafilesPath .. fnCreatePath("UIAdvMenuOptions.slf")
gsaDatafilePaths.sUIAdvMenuQuit           = gsDatafilesPath .. fnCreatePath("UIAdvMenuQuit.slf")
gsaDatafilePaths.sUIAdvMenuSkills         = gsDatafilesPath .. fnCreatePath("UIAdvMenuSkills.slf")
gsaDatafilePaths.sUIAdvMenuStandard       = gsDatafilesPath .. fnCreatePath("UIAdvMenuStandard.slf")
gsaDatafilePaths.sUIAdvMenuStatus         = gsDatafilesPath .. fnCreatePath("UIAdvMenuStatus.slf")
gsaDatafilePaths.sUIAdvMenuTrainer        = gsDatafilesPath .. fnCreatePath("UIAdvMenuTrainer.slf")
gsaDatafilePaths.sUIAdvMenuVendor         = gsDatafilesPath .. fnCreatePath("UIAdvMenuVendor.slf")
gsaDatafilePaths.sUIControls              = gsDatafilesPath .. fnCreatePath("UIControls.slf")
gsaDatafilePaths.sUITextAdventure         = gsDatafilesPath .. fnCreatePath("UITextAdventure.slf")
gsaDatafilePaths.sElectrospriteAdventure  = gsDatafilesPath .. fnCreatePath("ElectrospriteAdventure.slf")

-- |[Meta-Autoloaders]|
--Datafiles containing only autoload instructions.
gsaDatafilePaths.sChapter1MapAutoload      = gsDatafilesPath .. fnCreatePath("Maps_CH1_Autoloader.slf")
gsaDatafilePaths.sChapter2MapAutoload      = gsDatafilesPath .. fnCreatePath("Maps_CH2_Autoloader.slf")
gsaDatafilePaths.sChapter5MapAutoload      = gsDatafilesPath .. fnCreatePath("Maps_CH5_Autoloader.slf")
gsaDatafilePaths.sChapter1PortraitAutoload = gsDatafilesPath .. fnCreatePath("Portraits_CH1_Autoloader.slf")
gsaDatafilePaths.sChapter2PortraitAutoload = gsDatafilesPath .. fnCreatePath("Portraits_CH2_Autoloader.slf")
gsaDatafilePaths.sChapter1SpriteAutoload   = gsDatafilesPath .. fnCreatePath("Sprites_CH1_Autoloader.slf")
gsaDatafilePaths.sChapter2SpriteAutoload   = gsDatafilesPath .. fnCreatePath("Sprites_CH2_Autoloader.slf")

-- |[ ================================== Datafile Identities =================================== ]|
--These create internal Name/Path pairs in the SugarLumpManager so the C++ code knows where to find
-- the files. This is used by the Autoloader to automate file switching, and allows mods to switch
-- the file paths without breaking the autoloader.

-- |[Sprites]|
SLF_RegisterFileIdentity("Datafile Sprites",  gsaDatafilePaths.sSprites)

-- |[Portraits]|
SLF_RegisterFileIdentity("Datafile Ch0Emote",   gsaDatafilePaths.sChapter0EmotePath)
SLF_RegisterFileIdentity("Datafile Ch0Combat",  gsaDatafilePaths.sChapter0CombatPath)
SLF_RegisterFileIdentity("Datafile Ch1Emote",   gsaDatafilePaths.sChapter1EmotePath)
SLF_RegisterFileIdentity("Datafile Ch1Combat",  gsaDatafilePaths.sChapter1CombatPath)
SLF_RegisterFileIdentity("Datafile Ch2Emote",   gsaDatafilePaths.sChapter2EmotePath)
SLF_RegisterFileIdentity("Datafile Ch2Combat",  gsaDatafilePaths.sChapter2CombatPath)
SLF_RegisterFileIdentity("Datafile Cassandra",  gsaDatafilePaths.sCassandraPath)
SLF_RegisterFileIdentity("Datafile Empress",    gsaDatafilePaths.sEmpressPath)
SLF_RegisterFileIdentity("Datafile Florentina", gsaDatafilePaths.sFlorentinaPath)
SLF_RegisterFileIdentity("Datafile Izuna",      gsaDatafilePaths.sIzunaPath)
SLF_RegisterFileIdentity("Datafile Mei",        gsaDatafilePaths.sMeiPath)
SLF_RegisterFileIdentity("Datafile Mia",        gsaDatafilePaths.sMiaPath)
SLF_RegisterFileIdentity("Datafile Miso",       gsaDatafilePaths.sMisoPath)
SLF_RegisterFileIdentity("Datafile Odar",       gsaDatafilePaths.sOdarPath)
SLF_RegisterFileIdentity("Datafile Sanya",      gsaDatafilePaths.sSanyaPath)
SLF_RegisterFileIdentity("Datafile Yukina",     gsaDatafilePaths.sYukinaPath)
SLF_RegisterFileIdentity("Datafile Zeke",       gsaDatafilePaths.sZekePath)

-- |[Maps]|
SLF_RegisterFileIdentity("Datafile Maps Ch1 Trannadar",      gsaDatafilePaths.sMapCh1Trannadar)
SLF_RegisterFileIdentity("Datafile Maps Ch1 Trannadar West", gsaDatafilePaths.sMapCh1TrannadarWest)
SLF_RegisterFileIdentity("Datafile Maps Ch1 Mausoleum",      gsaDatafilePaths.sMapCh1Mausoleum)

SLF_RegisterFileIdentity("Datafile Maps Biolabs",          gsaDatafilePaths.sMapBiolabs)
SLF_RegisterFileIdentity("Datafile Maps CRT Noise",        gsaDatafilePaths.sMapCRTNoise)
SLF_RegisterFileIdentity("Datafile Maps Cryogenics Lower", gsaDatafilePaths.sMapCryoLower)
SLF_RegisterFileIdentity("Datafile Maps Cryogenics Main",  gsaDatafilePaths.sMapCryoMain)
SLF_RegisterFileIdentity("Datafile Maps Equinox",          gsaDatafilePaths.sMapEquinox)
SLF_RegisterFileIdentity("Datafile Maps LRT East",         gsaDatafilePaths.sMapLRTEast)
SLF_RegisterFileIdentity("Datafile Maps LRT West",         gsaDatafilePaths.sMapLRTWest)
SLF_RegisterFileIdentity("Datafile Maps Nix Nedar",        gsaDatafilePaths.sMapNixNedar)
SLF_RegisterFileIdentity("Datafile Maps Northwoods",       gsaDatafilePaths.sMapNorthwoods)
SLF_RegisterFileIdentity("Datafile Maps Regulus",          gsaDatafilePaths.sMapRegulus)
SLF_RegisterFileIdentity("Datafile Maps System",           gsaDatafilePaths.sMapSystem)
SLF_RegisterFileIdentity("Datafile Maps Westwoods",        gsaDatafilePaths.sMapWestwoods)
SLF_RegisterFileIdentity("Datafile Maps MtSarulente",      gsaDatafilePaths.sMapMtSarulente)

-- |[UI]|
SLF_RegisterFileIdentity("Datafile UI AdvCombat",               gsaDatafilePaths.sAdvCombat)
SLF_RegisterFileIdentity("Datafile UI Adventure",               gsaDatafilePaths.sUIAdventure)
SLF_RegisterFileIdentity("Datafile UI AdvIcons",                gsaDatafilePaths.sUIAdvIcons)
SLF_RegisterFileIdentity("Datafile UI AdvMenu Base",            gsaDatafilePaths.sUIAdvMenuBase)
SLF_RegisterFileIdentity("Datafile UI AdvMenu Campfire",        gsaDatafilePaths.sUIAdvMenuCampfire)
SLF_RegisterFileIdentity("Datafile UI AdvMenu Doctor",          gsaDatafilePaths.sUIAdvMenuDoctor)
SLF_RegisterFileIdentity("Datafile UI AdvMenu Equipment",       gsaDatafilePaths.sUIAdvMenuEquipment)
SLF_RegisterFileIdentity("Datafile UI AdvMenu Field Abilities", gsaDatafilePaths.sUIAdvMenuFieldAbilities)
SLF_RegisterFileIdentity("Datafile UI AdvMenu File Select",     gsaDatafilePaths.sUIAdvMenuFileSelect)
SLF_RegisterFileIdentity("Datafile UI AdvMenu Inventory",       gsaDatafilePaths.sUIAdvMenuInventory)
SLF_RegisterFileIdentity("Datafile UI AdvMenu Journal",         gsaDatafilePaths.sUIAdvMenuJournal)
SLF_RegisterFileIdentity("Datafile UI AdvMenu Options",         gsaDatafilePaths.sUIAdvMenuOptions)
SLF_RegisterFileIdentity("Datafile UI AdvMenu Quit",            gsaDatafilePaths.sUIAdvMenuQuit)
SLF_RegisterFileIdentity("Datafile UI AdvMenu Skills",          gsaDatafilePaths.sUIAdvMenuSkills)
SLF_RegisterFileIdentity("Datafile UI AdvMenu Standard",        gsaDatafilePaths.sUIAdvMenuStandard)
SLF_RegisterFileIdentity("Datafile UI AdvMenu Status",          gsaDatafilePaths.sUIAdvMenuStatus)
SLF_RegisterFileIdentity("Datafile UI AdvMenu Trainer",         gsaDatafilePaths.sUIAdvMenuTrainer)
SLF_RegisterFileIdentity("Datafile UI AdvMenu Vendor",          gsaDatafilePaths.sUIAdvMenuVendor)
SLF_RegisterFileIdentity("Datafile UI Controls",                gsaDatafilePaths.sUIControls)
SLF_RegisterFileIdentity("Datafile UI Text Adventure",          gsaDatafilePaths.sUITextAdventure)
SLF_RegisterFileIdentity("Datafile Electrosprite Adventure",    gsaDatafilePaths.sElectrospriteAdventure)

-- |[ ======================================== Finish Up ======================================= ]|
fnCreatePath = nil
