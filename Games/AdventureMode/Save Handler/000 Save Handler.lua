-- |[ ====================================== Save Handler ====================================== ]|
--Called when saving the game. Organizes variables that will then get written to a file as a savefile.
-- This is called just after the LOADINFO block is assembled and just before the SCRIPTVARS block
-- is assembled.

-- |[Clear]|
--Clear any lingering variables.
DL_Purge("Root/Saving/", false)

--Recreate the path.
DL_AddPath("Root/Saving/")

-- |[ ==================================== Field Abilities ===================================== ]|
--Store the names of the field abilities in these slots.
DL_AddPath("Root/Saving/FieldAbilities/All/")
for i = 0, gciFieldAbility_Slots-1, 1 do
    local sFieldAbilityName = AdvCombat_GetProperty("Field Ability Name", i)
    VM_SetVar("Root/Saving/FieldAbilities/All/" .. i, "S", sFieldAbilityName)
end

-- |[ ======================================== Options ========================================= ]|
--Options values that get written to savefiles.
local fMusicVolume = AudioManager_GetProperty("Music Volume")
local fSoundVolume = AudioManager_GetProperty("Sound Volume")
VM_SetVar("Root/Variables/System/Special/fMusicVolume", "N", fMusicVolume)
VM_SetVar("Root/Variables/System/Special/fSoundVolume", "N", fSoundVolume)

--Abstract Volumes. These can be used for specific volumes, often by mods.
for i = 0, 9, 1 do
    local fAbsVol = AudioManager_GetProperty("Abstract Volume", i)
    VM_SetVar("Root/Variables/System/Special/fAbsVol"..i, "N", fAbsVol)
end

--Memory Cursor
local bIsMemoryCursor = AdvCombat_GetProperty("Is Memory Cursor")
if(bIsMemoryCursor) then
    VM_SetVar("Root/Variables/System/Special/iMemoryCursor", "N", 1)
else
    VM_SetVar("Root/Variables/System/Special/iMemoryCursor", "N", 0)
end

--Tourist Mode
local bIsTouristMode = AdvCombat_GetProperty("Is Tourist Mode")
if(bIsTouristMode) then
    VM_SetVar("Root/Variables/System/Special/iIsTouristMode", "N", 1)
else
    VM_SetVar("Root/Variables/System/Special/iIsTouristMode", "N", 0)
end
    
--Auto-Hasten Dialogue.
local bIsAutoHasten = WD_GetProperty("Is Auto Hastening")
if(bIsAutoHasten) then
    VM_SetVar("Root/Variables/System/Special/iAutoHastenDialogue", "N", 1)
else
    VM_SetVar("Root/Variables/System/Special/iAutoHastenDialogue", "N", 0)
end

-- |[ ========================================= Topics ========================================= ]|
--Topics, indicating who has been spoken to about which topics.
DL_AddPath("Root/Saving/Topics/Base/")

--How many topics, total.
local iTotalTopics = WD_GetProperty("Total Topics")
VM_SetVar("Root/Saving/Topics/Base/iTotalTopics", "N", iTotalTopics)

--For each topic:
for i = 0, iTotalTopics - 1, 1 do
    
    --Variables.
    local sTopicName  = WD_GetProperty("Topic Name", i)
    local iTopicLevel = WD_GetProperty("Topic Level", i)
    local iTopicNPCs  = WD_GetProperty("Topic NPCs Total", i)
    
    --Write.
    VM_SetVar("Root/Saving/Topics/Base/sTopic"..i.."Name",  "S", sTopicName)
    VM_SetVar("Root/Saving/Topics/Base/sTopic"..i.."Level", "N", iTopicLevel)
    VM_SetVar("Root/Saving/Topics/Base/sTopic"..i.."NPCs", "N",  iTopicNPCs)
    
    --For each NPC...
    for p = 0, iTopicNPCs-1, 1 do
    
        --Variables.
        local sSubNPCName  = WD_GetProperty("Topic NPC Name", i, p)
        local iSubNPCLevel = WD_GetProperty("Topic NPC Level", i, p)
    
        --Write.
        VM_SetVar("Root/Saving/Topics/Base/sTopic"..i.."Sub"..p.."Name",  "S", sSubNPCName)
        VM_SetVar("Root/Saving/Topics/Base/iTopic"..i.."Sub"..p.."Level", "N", iSubNPCLevel)
    
    end
end

-- |[ ======================================= Followers ======================================== ]|
--Dynamically tracks who is following the main character.
DL_AddPath("Root/Saving/Followers/Base/")
VM_SetVar("Root/Saving/Followers/Base/sLeader", "S", gsPartyLeaderName)

--For each follower, write their name.
if(giFollowersTotal == nil) then giFollowersTotal = 0 end
VM_SetVar("Root/Saving/Followers/Base/iFollowers", "N", giFollowersTotal)
for i = 0, giFollowersTotal-1, 1 do
    
    --Get the follower name. Save that.
    local sFollowerName = gsaFollowerNames[i+1]
    VM_SetVar("Root/Saving/Followers/Base/sFollower"..i.."Name", "S", gsaFollowerNames[i+1])
    
    --Now look up their combat name and variable.
    for p = 1, #gzaFollowerRemaps, 1 do
        if(gzaFollowerRemaps[p] == sFollowerName) then
        
            VM_SetVar("Root/Saving/Followers/Base/sFollower"..i.."CombatName", "S", gzaFollowerRemaps[p][2])
            VM_SetVar("Root/Saving/Followers/Base/sFollower"..i.."Variable",   "S", gzaFollowerRemaps[p][3])
            break
        end
    end
end

-- |[ =================================== Combat Characters ==================================== ]|
--Assemble the combat variables.
DL_AddPath("Root/Saving/Combat/Base/")

-- |[Party Information]|
--Store variables related to all characters on the roster.
local iRosterSize = AdvCombat_GetProperty("Roster Size")
VM_SetVar("Root/Saving/Combat/Base/iRosterSize", "N", iRosterSize)

--Storage for equipment
local iAdditionalEquippedItems = 0

--For each character...
DL_AddPath("Root/Saving/Combat/Party/")
for i = 0, iRosterSize-1, 1 do
    AdvCombat_SetProperty("Push Party Member By Slot", i)
    
        -- |[Basic Setup]|
        --Variables.
        local sInternalName = AdvCombatEntity_GetProperty("Internal Name")
        local iHPCur        = AdvCombatEntity_GetProperty("Health")
        local iHPMax        = AdvCombatEntity_GetProperty("Statistic", gciStatIndex_HPMax)
        local iExperience   = AdvCombatEntity_GetProperty("Exp")
        local iGlobalJP     = AdvCombatEntity_GetProperty("Global JP")
        local sCurrentJob   = AdvCombatEntity_GetProperty("Current Job")
        local iTotalJobs    = AdvCombatEntity_GetProperty("Total Jobs")
        
        --Resolve HP Percent.
        if(iHPMax < 1.0) then iHPMax = 1.0 end
        local fHPPercent = iHPCur / iHPMax
    
        --Store.
        VM_SetVar("Root/Saving/Combat/Party/sCharacter"..i.."Name",  "S", sInternalName)
        VM_SetVar("Root/Saving/Combat/Party/fCharacter"..i.."HpPct", "N", fHPPercent)
        VM_SetVar("Root/Saving/Combat/Party/iCharacter"..i.."Exp",   "N", iExperience)
        VM_SetVar("Root/Saving/Combat/Party/iCharacter"..i.."JP",    "N", iGlobalJP)
        VM_SetVar("Root/Saving/Combat/Party/sCharacter"..i.."JobCur","S", sCurrentJob)
        
        -- |[Jobs]|
        --Job Handling. For each stored job...
        VM_SetVar("Root/Saving/Combat/Party/iCharacter"..i.."JobsTotal", "N", iTotalJobs)
        for p = 0, iTotalJobs-1, 1 do
            AdvCombatEntity_SetProperty("Push Job I", p)
                local sJobName               = AdvCombatJob_GetProperty("Name")
                local iAvailableJP           = AdvCombatJob_GetProperty("Current JP")
                local iAbilities             = AdvCombatJob_GetProperty("Total Abilities")
                local sAssociatedProfileName = AdvCombatJob_GetProperty("Associated Profile Name")
    
                --Store.
                VM_SetVar("Root/Saving/Combat/Party/sCharacter"..i.."Job"..p.."Name",      "S", sJobName)
                VM_SetVar("Root/Saving/Combat/Party/iCharacter"..i.."Job"..p.."JP",        "N", iAvailableJP)
                VM_SetVar("Root/Saving/Combat/Party/iCharacter"..i.."Job"..p.."Abilities", "N", iAbilities)
                VM_SetVar("Root/Saving/Combat/Party/iCharacter"..i.."Job"..p.."Profile",   "S", sAssociatedProfileName)
                
                --For each ability...
                for o = 0, iAbilities, 1 do
                    local sAbilityName = AdvCombatJob_GetProperty("Ability Name I", o)
                    local bIsUnlocked  = AdvCombatJob_GetProperty("Is Ability Unlocked I", o)
                    
                    --Resolve which value to write.
                    local iWriteValue = 0
                    if(bIsUnlocked) then iWriteValue = 1 end
                    VM_SetVar("Root/Saving/Combat/Party/iCharacter"..i.."Job"..p.."Ability"..o.."Name", "S", sAbilityName)
                    VM_SetVar("Root/Saving/Combat/Party/iCharacter"..i.."Job"..p.."Ability"..o.."Unlocked", "N", iWriteValue)
                end
            DL_PopActiveObject()
        end
        
        -- |[Abilities]|
        --Abilities. Store abilities in the optional slots.
        for x = gciAbility_Saveblock_X1, gciAbility_Saveblock_X2, 1 do
            for y = gciAbility_Saveblock_Y1, gciAbility_Saveblock_Y2, 1 do
                local sAbilityName = AdvCombatEntity_GetProperty("Ability In Slot", x, y)
                VM_SetVar("Root/Saving/Combat/Party/iCharacter"..i.."Ability"..x..y.."Name", "S", sAbilityName)
            end
        end
        
        -- |[Profiles]|
        --Profiles.
        local iTotalProfiles = AdvCombatEntity_GetProperty("Total Profiles")
        VM_SetVar("Root/Saving/Combat/Party/iCharacter"..i.."ProfilesTotal", "N", iTotalProfiles)
        for p = 0, iTotalProfiles-1, 1 do
            
            --Name of the profile.
            local sNameOfProfile = AdvCombatEntity_GetProperty("Name of Profile", p)
            VM_SetVar("Root/Saving/Combat/Party/iCharacter"..i.."Profile"..p.."Name", "S", sNameOfProfile)
            
            --Iterate across the slots, store all the names.
            for x = 0, gciAbility_Memorized_Block_Wid-1, 1 do
                for y = 0, gciAbility_Memorized_Block_Hei-1, 1 do
                    local sNameOfAbility = AdvCombatEntity_GetProperty("Ability In Profile", p, x, y)
                    VM_SetVar("Root/Saving/Combat/Party/iCharacter"..i.."Profile"..p.."Ability"..x.."by"..y, "S", sNameOfAbility)
                end
            end
        end
        
    DL_PopActiveObject()
end

-- |[Active Party]|
--Store who is in the active party.
for i = 0, gciCombat_MaxActivePartySize-1, 1 do
    AdvCombat_SetProperty("Push Active Party Member By Slot", i)
    
        --Filled slot:
        if(DL_ActiveIsValid() == true) then
            local sInternalName = AdvCombatEntity_GetProperty("Internal Name")
            VM_SetVar("Root/Saving/Combat/Base/sActiveChar"..i, "S", sInternalName)
    
        --Empty slot:
        else
            VM_SetVar("Root/Saving/Combat/Base/sActiveChar"..i, "S", "Null")
        end
    DL_PopActiveObject()
end

-- |[ ======================================= Inventory ======================================== ]|
--Assemble the inventory variables.
DL_AddPath("Root/Saving/Inventory/Base")

-- |[Sorting Flag]|
local iFlag = AdInv_GetProperty("Internal Sort Flag")
VM_SetVar("Root/Saving/Inventory/Base/iSortFlag", "N", AdInv_GetProperty("Internal Sort Flag"))

-- |[ID Counter]|
VM_SetVar("Root/Saving/Inventory/Base/iUniqueIDCounter", "N", AdInv_GetProperty("Unique ID Counter"))

-- |[Money]|
VM_SetVar("Root/Saving/Inventory/Base/iPlatina", "N", AdInv_GetProperty("Platina"))

-- |[Adamantite]|
VM_SetVar("Root/Saving/Inventory/Base/iAdamantite0", "N", AdInv_GetProperty("Crafting Count", gciCraft_Adamantite_Powder))
VM_SetVar("Root/Saving/Inventory/Base/iAdamantite1", "N", AdInv_GetProperty("Crafting Count", gciCraft_Adamantite_Flakes))
VM_SetVar("Root/Saving/Inventory/Base/iAdamantite2", "N", AdInv_GetProperty("Crafting Count", gciCraft_Adamantite_Shard))
VM_SetVar("Root/Saving/Inventory/Base/iAdamantite3", "N", AdInv_GetProperty("Crafting Count", gciCraft_Adamantite_Piece))
VM_SetVar("Root/Saving/Inventory/Base/iAdamantite4", "N", AdInv_GetProperty("Crafting Count", gciCraft_Adamantite_Chunk))
VM_SetVar("Root/Saving/Inventory/Base/iAdamantite5", "N", AdInv_GetProperty("Crafting Count", gciCraft_Adamantite_Ore))

-- |[Catalysts]|
--Just the totals, not which ones specifically were found.
VM_SetVar("Root/Saving/Inventory/Base/iCatalystHealth",     "N", AdInv_GetProperty("Catalyst Count", gciCatalyst_Health))
VM_SetVar("Root/Saving/Inventory/Base/iCatalystAttack",     "N", AdInv_GetProperty("Catalyst Count", gciCatalyst_Attack))
VM_SetVar("Root/Saving/Inventory/Base/iCatalystInitiative", "N", AdInv_GetProperty("Catalyst Count", gciCatalyst_Initiative))
VM_SetVar("Root/Saving/Inventory/Base/iCatalystEvade",      "N", AdInv_GetProperty("Catalyst Count", gciCatalyst_Evade))
VM_SetVar("Root/Saving/Inventory/Base/iCatalystAccuracy",   "N", AdInv_GetProperty("Catalyst Count", gciCatalyst_Accuracy))
VM_SetVar("Root/Saving/Inventory/Base/iCatalystSkill",      "N", AdInv_GetProperty("Catalyst Count", gciCatalyst_Skill))

--Also save the chapter progress in catalysts.
VM_SetVar("Root/Saving/Inventory/Base/iCatalystCurHealth", "N", AdInv_GetProperty("Catalyst Local Cur", gciCatalyst_Health))
VM_SetVar("Root/Saving/Inventory/Base/iCatalystMaxHealth", "N", AdInv_GetProperty("Catalyst Local Max", gciCatalyst_Health))

VM_SetVar("Root/Saving/Inventory/Base/iCatalystCurAttack", "N", AdInv_GetProperty("Catalyst Local Cur", gciCatalyst_Attack))
VM_SetVar("Root/Saving/Inventory/Base/iCatalystMaxAttack", "N", AdInv_GetProperty("Catalyst Local Max", gciCatalyst_Attack))

VM_SetVar("Root/Saving/Inventory/Base/iCatalystCurInitiative", "N", AdInv_GetProperty("Catalyst Local Cur", gciCatalyst_Initiative))
VM_SetVar("Root/Saving/Inventory/Base/iCatalystMaxInitiative", "N", AdInv_GetProperty("Catalyst Local Max", gciCatalyst_Initiative))

VM_SetVar("Root/Saving/Inventory/Base/iCatalystCurEvade", "N", AdInv_GetProperty("Catalyst Local Cur", gciCatalyst_Evade))
VM_SetVar("Root/Saving/Inventory/Base/iCatalystMaxEvade", "N", AdInv_GetProperty("Catalyst Local Max", gciCatalyst_Evade))

VM_SetVar("Root/Saving/Inventory/Base/iCatalystCurAccuracy", "N", AdInv_GetProperty("Catalyst Local Cur", gciCatalyst_Accuracy))
VM_SetVar("Root/Saving/Inventory/Base/iCatalystMaxAccuracy", "N", AdInv_GetProperty("Catalyst Local Max", gciCatalyst_Accuracy))

VM_SetVar("Root/Saving/Inventory/Base/iCatalystCurSkill", "N", AdInv_GetProperty("Catalyst Local Cur", gciCatalyst_Skill))
VM_SetVar("Root/Saving/Inventory/Base/iCatalystMaxSkill", "N", AdInv_GetProperty("Catalyst Local Max", gciCatalyst_Skill))

-- |[Doctor Bag]|
--Refresh and save the doctor bag's properties.
local iCurDoctorCharges    = AdInv_GetProperty("Doctor Bag Charges")
local iDoctorBagChargesMax = AdInv_GetProperty("Doctor Bag Charges Max")
VM_SetVar("Root/Variables/System/Special/iDoctorBagCharges", "N", iCurDoctorCharges)
VM_SetVar("Root/Variables/System/Special/iDoctorBagChargesMax", "N", iDoctorBagChargesMax)

-- |[Totals]|
--The total number of items in the inventory section is the number of equipped items, which will count
-- regular items as well.
local iEquippedItems = AdInv_GetProperty("Total Items Equipped")
local iItemsGrandTotal = 0

-- |[Inventory Items]|
--Items that are not equipped. This includes unused equipment, key items, or consumables.
local iItemsTotal = AdInv_GetProperty("Total Items Unequipped")
local iWriteIndex = 0
for i = 0, iItemsTotal-1, 1 do
    AdInv_PushItemI(i)
    
        --Variables.
        local sName = AdItem_GetProperty("Name")
        local iQuantity = AdItem_GetProperty("Quantity")
        local sInstructions = " "
    
        --Store.
        VM_SetVar("Root/Saving/Inventory/Items/sItem" .. iWriteIndex .. "Name",         "S", sName)
        VM_SetVar("Root/Saving/Inventory/Items/iItem" .. iWriteIndex .. "Quantity",     "N", iQuantity)
        VM_SetVar("Root/Saving/Inventory/Items/sItem" .. iWriteIndex .. "Instructions", "S", sInstructions)
        
        --If the item is a gem:
        if(AdItem_GetProperty("Is Gem") == true) then
            
            --Flag.
            VM_SetVar("Root/Saving/Inventory/Items/iItem" .. iWriteIndex .. "IsGem", "N", 1.0)
            
            --Gem's original name.
            local sOrigName = AdItem_GetProperty("Base Gem Name")
            VM_SetVar("Root/Saving/Inventory/Items/sItem" .. iWriteIndex .. "GemName", "S", sOrigName)
            
            --Get the gems stored within this gem. It is possible for all the slots to be "Null".
            for p = 0, gciSubgemMax-1, 1 do
                local sSubgemName = AdItem_GetProperty("Subgem Name", p)
                VM_SetVar("Root/Saving/Inventory/Items/sItem" .. iWriteIndex .. "Subgem"..p.."Name", "S", sSubgemName)
            end
            
        --Not a gem. As of Starlight 3.1 (WoR Pr1.02) it is now possible for unequipped items to have gems in them.
        else
        
            --Not a gem.
            VM_SetVar("Root/Saving/Inventory/Items/iItem" .. iWriteIndex .. "IsGem", "N", 0.0)
            
            --Get the gems stored within this gem. It is possible for all the slots to be "Null".
            for p = 0, gciSubgemMax-1, 1 do
                local sSubgemName = AdItem_GetProperty("Subgem Name", p)
                VM_SetVar("Root/Saving/Inventory/Items/sItem" .. iWriteIndex .. "Subgem"..p.."Name", "S", sSubgemName)
            end
        end
        
        --Next.
        iWriteIndex = iWriteIndex + 1
    
    DL_PopActiveObject()
    
    --Add one written item.
    iItemsGrandTotal = iItemsGrandTotal + 1
end

-- |[ ===================================== Equipped Items ===================================== ]|
--Items that are equipped. Iterate across the characters.
for i = 0, iRosterSize-1, 1 do
    AdvCombat_SetProperty("Push Party Member By Slot", i)
    
        --Name of the character.
        local sCharacterName = AdvCombatEntity_GetProperty("Internal Name")
    
        --Iterate across the equipped items.
        local iCharItems = AdvCombatEntity_GetProperty("Total Equipment Slots")
        for p = 0, iCharItems, 1 do
            
            --Check the name of the item in the slot.
            local sSlotName      = AdvCombatEntity_GetProperty("Name of Equipment Slot", p)
            local sEquipmentName = AdvCombatEntity_GetProperty("Equipment In Slot I", p)
        
            --If not null, we need to push it and get its data.
            if(sEquipmentName ~= "Null") then
                AdvCombatEntity_SetProperty("Push Item In Slot I", p)
                    
                    --Variables.
                    local sEqpName = AdItem_GetProperty("Name")
                    local iQuantity = AdItem_GetProperty("Quantity")
                    local sEqpInstructions = ""
                
                    --Store.
                    local iStoredIndex = iWriteIndex
                    VM_SetVar("Root/Saving/Inventory/Items/sItem" .. iStoredIndex .. "Name",         "S", sEqpName)
                    VM_SetVar("Root/Saving/Inventory/Items/iItem" .. iStoredIndex .. "Quantity",     "N", iQuantity)
                    iItemsGrandTotal = iItemsGrandTotal + 1
                    
                    --Iterate across the item's gems.
                    for c = 0, gciGemSlotsMax-1, 1 do
                        
                        --If there is a gem in the slot:
                        if(AdItem_GetProperty("Has Gem In Slot", c) == true) then
                            AdItem_SetProperty("Push Gem In Slot", c)
        
                                --Next Index.
                                iWriteIndex = iWriteIndex + 1
                                iItemsGrandTotal = iItemsGrandTotal + 1
                                
                                --Mark the instructions to equip this item.
                                sEqpInstructions = sEqpInstructions .. "SocketNext|"
        
                                --Variables.
                                local sName = AdItem_GetProperty("Name")
                                local iQuantity = AdItem_GetProperty("Quantity")
                                local sInstructions = " "
                            
                                --Store.
                                VM_SetVar("Root/Saving/Inventory/Items/sItem" .. iWriteIndex .. "Name",         "S", sName)
                                VM_SetVar("Root/Saving/Inventory/Items/iItem" .. iWriteIndex .. "Quantity",     "N", iQuantity)
                                VM_SetVar("Root/Saving/Inventory/Items/sItem" .. iWriteIndex .. "Instructions", "S", sInstructions)
                                
                                --If the item is a gem:
                                if(AdItem_GetProperty("Is Gem") == true) then
                                    
                                    --Flag.
                                    VM_SetVar("Root/Saving/Inventory/Items/iItem" .. iWriteIndex .. "IsGem", "N", 1.0)
                                    
                                    --Gem's original name.
                                    local sOrigName = AdItem_GetProperty("Base Gem Name")
                                    VM_SetVar("Root/Saving/Inventory/Items/sItem" .. iWriteIndex .. "GemName", "S", sOrigName)
                                    
                                    --Get the gems stored within this gem. It is possible for all the slots to be "Null".
                                    for o = 0, gciSubgemMax-1, 1 do
                                        local sSubgemName = AdItem_GetProperty("Subgem Name", o)
                                        VM_SetVar("Root/Saving/Inventory/Items/sItem" .. iWriteIndex .. "Subgem"..o.."Name", "S", sSubgemName)
                                    end
                                    
                                --Not a gem. Should not be logically reachable.
                                else
                                    VM_SetVar("Root/Saving/Inventory/Items/iItem" .. iWriteIndex .. "IsGem", "N", 0.0)
                                end
                            DL_PopActiveObject()
                        end
                    end
                    
                    --Add the equip instruction. This must be *after* a socket instruction, or the object will be cleared from the inventory.
                    sEqpInstructions = sEqpInstructions .. "Equip:"..sCharacterName..":"..sSlotName.."|"
                    
                    --Write the instructions.
                    VM_SetVar("Root/Saving/Inventory/Items/sItem" .. iStoredIndex .. "Instructions", "S", sEqpInstructions)
                    
                    iWriteIndex = iWriteIndex + 1
                DL_PopActiveObject()
            end
        end
    
    DL_PopActiveObject()
end

--Write the grand-total items. This includes socketed gems but does not include sub-gems within gems.
VM_SetVar("Root/Saving/Inventory/Items/iItemsTotal", "N", iItemsGrandTotal)

-- |[ =================================== Chapter 2 Hunting ==================================== ]|
--Saves whatever the current state of the hunt in chapter 2 is. In other chapters this will save
-- some zeroes.
DL_AddPath("Root/Saving/Hunting/System/")
DL_AddPath("Root/Saving/Hunting/Prey/")

-- |[No Data]|
--If there are no stored prey values, write a single variable to indicate we don't need to bother with loading it.
if(#Hunting.zaPreyList < 1) then
    VM_SetVar("Root/Saving/Hunting/System/iHasHuntData", "N", 0.0)

-- |[Has Hunt Data]|
else
    Hunting:fnSaveHuntData()
end

-- |[ ======================================= Finish Up ======================================== ]|
--Debug here.
