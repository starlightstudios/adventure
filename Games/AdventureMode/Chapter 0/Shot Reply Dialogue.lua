-- |[ ===================================== Dialogue Script ==================================== ]|
--Default dialogue path for enemies. This allows them to handle getting shot by Sanya's rifle
-- in chapter 2 and get stunned appropriately.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
io.write("Call " .. sTopicName .. "\n")
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then

-- |[ ====================================== "Shot Check" ====================================== ]|
--If this entity is targeted by a rifle shot, this section of code returns whether or not a response is merited.
elseif(sTopicName == "Shot Check") then
    
    -- |[Name Resolve]|
    --Resolve the name of the calling entity. No further processing is needed.
    local sActorName = TA_GetProperty("Name")
    
    -- |[Non-Prey]|
    --If this entity got SHOT but was not a prey entity, check if it's an enemy. If it is, the enemy
    -- is stunned (similar to being mugged) on hit. NPCs don't respond to shots.
    EM_PushEntity(sActorName)
    
        --Check if we're an enemy.
        local bIsEnemy = TA_GetProperty("Is Enemy")
        
        --If so, respond to the shot and also activate the stun.
        if(bIsEnemy == true) then
            TA_SetProperty("Get Stunned")
        end
    DL_PopActiveObject()
    
    --Flag reply.
    if(bIsEnemy) then 
        AL_SetProperty("Examinable Reply To Hit")
    end

-- |[ ===================================== "Shot Handle" ====================================== ]|
--If this entity is targeted by a rifle shot, and responded to the shot check, this block of code is used.
elseif(sTopicName == "Shot Handle") then
    
end
