-- |[ ================================ Combat Glossary Handler ================================= ]|
--Whenever the journal is opened to the combat glossary, this builds the entries.

-- |[Standards]|
--Heading position.
local cfHeadingX = 918.0
local cfHeadingY = 117.0

--Left-alignment for text blocks.
local cfTxL = 523.0
local cfTxT = 160.0

-- |[ ========================================= Images ========================================= ]|
if(gbCombatGlossaryLoadedImages == nil) then
    
    -- |[Setup]|
    --Flag.
    gbCombatGlossaryLoadedImages = true
    
    --Delayed Load List.
    local sListName = "Combat Glossary"
    fnCreateDelayedLoadList(sListName)
    
    --Debug: Clear
    DL_Purge("Root/Images/AdventureUI/CombatGlossary/", false)
    
    --Open, load, close.
    SLF_Open(gsDatafilesPath .. "UIAdvMenuJournal.slf")
    fnExecAutoloadDelayed("AutoLoad|Adventure|AdvMenuJournalGlossary", sListName)
    SLF_Close()
    
    -- |[Order Load]|
    fnLoadDelayedBitmapsFromList(sListName)
end

-- |[ ======================================== Listing ========================================= ]|
--Create if not done so already.
gzaCombatList = nil
if(gzaCombatList == nil) then

    -- |[Setup]|
    gzaCombatList = {}

    -- |[Combat Statistics]|
    local sInt = "Combat Stats"
    fnCreateCombatGlossaryPack(  gzaCombatList, sInt, "Combat Stats", 13)
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 0, cfHeadingX, cfHeadingY, gci_Font_AutocenterX, "Combat Statistics", "Header")
    fnSetGlossaryPackImage(      gzaCombatList, sInt, 1, 0.0, 0.0, "Root/Images/AdventureUI/CombatGlossary/CombatStats|0 Health Icon")
    fnSetGlossaryPackImage(      gzaCombatList, sInt, 2, 0.0, 0.0, "Root/Images/AdventureUI/CombatGlossary/CombatStats|1 Power Icon")
    fnSetGlossaryPackImage(      gzaCombatList, sInt, 3, 0.0, 0.0, "Root/Images/AdventureUI/CombatGlossary/CombatStats|2 Initiative Icon")
    fnSetGlossaryPackImage(      gzaCombatList, sInt, 4, 0.0, 0.0, "Root/Images/AdventureUI/CombatGlossary/CombatStats|3 AccEvade Icon")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 5, 574.0, 162.0, 0, "Health", "Header")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 6, 544.0, 203.0, 0, "Health is how much damage your character can take. If it reaches\\n" .. 
                                                                          "zero, the character is KO'd. If the entire party is KO'd, you lose.\\n" ..
                                                                          "Health can be restored with items and abilities.", "Main")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 7, 574.0, 283.0, 0, "Power", "Header")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 8, 544.0, 324.0, 0, "Damage dealt by your character. Most abilities have a scaler that\\n" .. 
                                                                          "multiplies by power to compute damage. Some healing skills also \\n" ..
                                                                          "scale with power.", "Main")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 9, 574.0, 413.0, 0, "Initiative", "Header")
    fnSetGlossaryPackDescription(gzaCombatList, sInt,10, 544.0, 454.0, 0, "Determines turn order. Each turn, a random number between 1 and\\n" .. 
                                                                          "80 is rolled and added to your initiative. Characters act from\\n" ..
                                                                          "highest to lowest initiative.", "Main")
    fnSetGlossaryPackDescription(gzaCombatList, sInt,11, 614.0, 542.0, 0, "Accuracy / Evade", "Header")
    fnSetGlossaryPackDescription(gzaCombatList, sInt,12, 544.0, 583.0, 0, "Determines chance to hit. Your accuracy is compared against your\\n" .. 
                                                                          "target's evade. Some abilities have higher or lower hit rates.\\n" ..
                                                                          "Accuracy over 100%%%% gives a chance for a critical strike.", "Main")

    -- |[Exotic Statistics]|
    sInt = "Exotic Stats"
    fnCreateCombatGlossaryPack(  gzaCombatList, sInt, "Other Stats", 7)
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 0, cfHeadingX, cfHeadingY, gci_Font_AutocenterX, "Other Statistics", "Header")
    fnSetGlossaryPackImage(      gzaCombatList, sInt, 1, 0.0, 0.0, "Root/Images/AdventureUI/CombatGlossary/OtherStats|0 Adrenaline Icon")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 2, 574.0, 162.0, 0, "Adrenaline", "Header")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 3, 544.0, 203.0, 0, "Adrenaline is temporary health that can be gained in combat. It is\\n"..
                                                                          "treated like health, but takes damage before health does.\\n\\n" .. 
                                                                          "If combat ends when you still have adrenaline, half of the adrenaline\\n"..
                                                                          "is added to your health as healing, the other half is lost.\\n" ..
                                                                          "Adrenaline can be used to exceed 100%%%% of your max health. You may\\n"..
                                                                          "have up to 50%%%% of your max health as adrenaline, any more is lost.", "Main")
    fnSetGlossaryPackImage(      gzaCombatList, sInt, 4, 0.0, 0.0, "Root/Images/AdventureUI/CombatGlossary/OtherStats|1 Shield Icon")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 5, 574.0, 362.0, 0, "Shields", "Header")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 6, 544.0, 403.0, 0, "Shields are temporary health gained in combat. Much like\\n"..
                                                                          "Adrenaline, they take damage in place of your health. Unlike\\n" .. 
                                                                          "Adrenaline, Shields are bypassed by Damage-Over-Time attacks.\\n\\n"..
                                                                          "Shields do not stack like Adrenaline does, if you already have 10 \\n" ..
                                                                          "shields and apply 50 shields, you will have 50 shields, keeping the\\n"..
                                                                          "highest. Take care when renewing shield effects.\\n\\n"..
                                                                          "Shields allow you to exceed 100%%%% health, and do not have a cap\\n"..
                                                                          "other than your most powerful shield ability.\\n\\n"..
                                                                          "All Shields are lost when combat ends.", "Main")

    -- |[Protection]|
    sInt = "Protection"
    fnCreateCombatGlossaryPack(  gzaCombatList, sInt, "Protection", 2)
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 0, cfHeadingX, cfHeadingY, gci_Font_AutocenterX, "Protection", "Header")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 1, 544.0, 162.0, 0, "Protection is how much your character can resist direct attacks.\\n"..
                                                                          "Anything that inflicts damage immediately is reduced by Protection.\\n\\n" .. 
                                                                          "Its value is added to your resistance when the attack hits, so it acts\\n"..
                                                                          "like a resistance to all damages, but won't help with status effects.\\n\\n" ..
                                                                          "The damage reduction scales exponentially. Each additional point of\\n"..
                                                                          "Protection provides less damage reduction than the last. You can see\\n"..
                                                                          "the amount on the Status screen.\\n\\n"..
                                                                          "Protection does not help against damage-over-times (DoTs) or \\n"..
                                                                          "effects. Some abilities may also bypass Protection entirely, or \\n"..
                                                                          "reduce it. This is called 'Penetration'.\\n\\n"..
                                                                          "If an enemy has negative Protection, attacks will deal additional\\ndamage!", "Main")

    -- |[Damage Types]|
    sInt = "Damage Types"
    fnCreateCombatGlossaryPack(  gzaCombatList, sInt, "Damage Types", 7)
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 0, cfHeadingX, cfHeadingY, gci_Font_AutocenterX, "Damage Types", "Header")
    fnSetGlossaryPackImage(      gzaCombatList, sInt, 1, 0.0, 0.0, "Root/Images/AdventureUI/CombatGlossary/DamageTypes|0 Damage Icons")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 2, 650.0, 162.0, 0, "Slash / Strike / Pierce", "Header")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 3, 650.0, 200.0, 0, "Flame / Freeze / Shock", "Header")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 4, 650.0, 238.0, 0, "Crusade / Obscure / Terrify", "Header")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 5, 650.0, 276.0, 0, "Bleed / Poison / Corrode", "Header")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 6, cfTxL, 319.0, 0, "There are 12 damage types in the game, represented by the icons\\n" .. 
                                                                          "above. Every offensive ability uses one or more types, and every\\n" ..
                                                                          "enemy in the game has a resistance to each.\\n\\n"..
                                                                          "Using abilities your enemy is less resistant to deals more damage,\\n"..
                                                                          "and effects of the right type are more likely to apply.\\n\\n"..
                                                                          "The resistances of your enemy can be seen on the combat inspector\\n"..
                                                                          "after the enemy has been defeated a few times. You can also try to \\n"..
                                                                          "guess their resistances with simple logic. Statues will resist bleeds,\\n"..
                                                                          "robots are weak to corrosion, plants are weak to flames, and so on.\\n\\n"..
                                                                          "You can see exactly what damage type an ability does in its \\n"..
                                                                          "description panel. For enemies, you can see what types they are using\\n"..
                                                                          "by the animation that appears when they attack.", "Main")

    -- |[Critical Strikes]|
    sInt = "Critical Strikes"
    fnCreateCombatGlossaryPack(  gzaCombatList, sInt, "Critical Strikes", 3)
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 0, cfHeadingX, cfHeadingY, gci_Font_AutocenterX, "Critical Strikes", "Header")
    fnSetGlossaryPackImage(      gzaCombatList, sInt, 1, 0.0, 0.0, "Root/Images/AdventureUI/CombatGlossary/CriticalStrikes|0 Crit Example")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 2, cfTxL, 204.0, 0, "Some abilities are capable of striking critically. The chance\\n" .. 
                                                                          "of a critical strike is based on your accuracy stat.\\n\\n" ..
                                                                          "Every point of accuracy over 100%%%% gives a 1%%%% critical strike\\n"..
                                                                          "chance. Some abilities cannot strike critically.\\n\\n"..
                                                                          "When an attack crits, it deals 125%%%% damage and inflict some stun.\\n\\n"..
                                                                          "Enemy attacks never crit.", "Main")
    
    -- |[Stun]|
    sInt = "Stun"
    fnCreateCombatGlossaryPack(  gzaCombatList, sInt, "Stun", 3)
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 0, cfHeadingX, cfHeadingY, gci_Font_AutocenterX, "Stun", "Header")
    fnSetGlossaryPackImage(      gzaCombatList, sInt, 1, 0.0, 0.0, "Root/Images/AdventureUI/CombatGlossary/Stun|0 Stun Example")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 2, cfTxL, 297.0, 0, "Stun is a secondary damage type inflicted by some abilities.\\n" .. 
                                                                          "Abilities that inflict stun will show a small yellow star icon.\\n\\n" ..
                                                                          "When stunned, enemies will skip their next turn and not act. They\\n"..
                                                                          "also cannot dodge attacks when stunned, but can still resist effects.\\n\\n"..
                                                                          "An enemy's stun threshold can be seen next to their health bar. When\\n"..
                                                                          "the current stun value passes the threshold, the enemy is stunned. \\n"..
                                                                          "Stun value decreases each turn.\\n\\n"..
                                                                          "When stunned, an enemy gains 1 star to indicate stun resistance. This\\n"..
                                                                          "reduces how much stun damage they take. It will reset in 3 turns, \\n"..
                                                                          "when the clock counts to zero. If stunned again, they gain another \\n"..
                                                                          "stun resistance and the timer resets. Repeatedly stunning an enemy \\n"..
                                                                          "becomes harder and harder over time.\\n\\n"..
                                                                          "The default stun cap is 100, but some enemies are harder or easier\\n"..
                                                                          "to stun, and have higher or lower caps. Some enemies, like bosses,\\n"..
                                                                          "may be totally impossible to stun.", "Main")
    
    -- |[Combat Inspector]|
    sInt = "Combat Inspector"
    fnCreateCombatGlossaryPack(  gzaCombatList, sInt, "Combat Inspector", 3)
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 0, cfHeadingX, cfHeadingY, gci_Font_AutocenterX, "Combat Inspector", "Header")
    fnSetGlossaryPackImage(      gzaCombatList, sInt, 1, 0.0, 0.0, "Root/Images/AdventureUI/CombatGlossary/Inspector|0 Inspector Example")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 2, cfTxL, 300.0, 0, "The combat inspector can be opened during your turn by pressing a\\n" .. 
                                                                          "key. Default F1.\\n"..
                                                                          "It shows a list of all characters on both sides and a list of their effects, \\n"..
                                                                          "stats, resistances, and abilities.\\n\\n"..
                                                                          "If you are unsure what an effect does, want to plan your build around \\n"..
                                                                          "enemy resistances, or need to check an ally's status, the combat \\n"..
                                                                          "inspector has all the information you need.\\n\\n"..
                                                                          "When checking an enemy's statistics, you must defeat a certain \\n"..
                                                                          "number of that enemy before their stats are revealed. This is not true \\n"..
                                                                          "for bosses. The number of enemies you must defeat will be listed on\\n"..
                                                                          "their stats pane.", "Main")
                                                                      
    -- |[Effects]|
    sInt = "Effects"
    fnCreateCombatGlossaryPack(  gzaCombatList, sInt, "Effects", 16)
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 0, cfHeadingX, cfHeadingY, gci_Font_AutocenterX, "Effects", "Header")
    fnSetGlossaryPackImage(      gzaCombatList, sInt, 1, 0.0, 0.0, "Root/Images/AdventureUI/CombatGlossary/Effects|0 Effect Example")
    fnSetGlossaryPackImage(      gzaCombatList, sInt, 2, 0.0, 0.0, "Root/Images/AdventureUI/CombatGlossary/Effects|1 Dot Frame")
    fnSetGlossaryPackImage(      gzaCombatList, sInt, 3, 0.0, 0.0, "Root/Images/AdventureUI/CombatGlossary/Effects|2 Buff Frame")
    fnSetGlossaryPackImage(      gzaCombatList, sInt, 4, 0.0, 0.0, "Root/Images/AdventureUI/CombatGlossary/Effects|3 Debuff Frame")
    fnSetGlossaryPackImage(      gzaCombatList, sInt, 5, 0.0, 0.0, "Root/Images/AdventureUI/CombatGlossary/Effects|4 Hot Frame")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 6, cfTxL, 222.0, 0, "Effects can be positive, negative, or neutral changes to stats or\\n" .. 
                                                                          "modifications of the way combat plays out. They can cause damage or\\n"..
                                                                          "healing every turn, make someone vulnerable to a damage type, or\\n"..
                                                                          "even cause them to randomly attack their allies.\\n\\n"..
                                                                          "A list of effects for each character can be seen in the combat\\n"..
                                                                          "inspector.", "Main")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 7, 582.0, 384.0, 0, "Damage Over Time", "Header")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 8, 602.0, 416.0, 0, "Effect deals damage every turn.", "Main")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 9,1035.0, 384.0, 0, "Heal Over Time", "Header")
    fnSetGlossaryPackDescription(gzaCombatList, sInt,10,1055.0, 416.0, 0, "Heals every turn.", "Main")
    fnSetGlossaryPackDescription(gzaCombatList, sInt,11, 582.0, 439.0, 0, "Buff", "Header")
    fnSetGlossaryPackDescription(gzaCombatList, sInt,12, 602.0, 471.0, 0, "Increases stats or resistances.", "Main")
    fnSetGlossaryPackDescription(gzaCombatList, sInt,13,1035.0, 439.0, 0, "Debuff", "Header")
    fnSetGlossaryPackDescription(gzaCombatList, sInt,14,1055.0, 471.0, 0, "Decreases stats/resists.", "Main")
    fnSetGlossaryPackDescription(gzaCombatList, sInt,15, cfTxL, 528.0, 0, "The chance for an effect to apply is based on its type and the\\n"..
                                                                          "resistances the enemy has. Abilities have a listed strength value\\n" .. 
                                                                          "which increase application chance by 10%%%% for each point. Finally,\\n"..
                                                                          "each level your party gains increases application chance by 2%%%%.", "Main")
                                                                      
    -- |[Threat]|
    sInt = "Threat"
    fnCreateCombatGlossaryPack(  gzaCombatList, sInt, "Threat", 4)
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 0, cfHeadingX, cfHeadingY, gci_Font_AutocenterX, "Threat", "Header")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 1, cfTxL, cfTxT, 0, "Most enemies do not attack completely at random, but will instead\\n" .. 
                                                                          "prioritize whichever party member poses the greatest threat. You can\\n"..
                                                                          "see the current threat values in the combat inspector.", "Main")
    fnSetGlossaryPackImage(      gzaCombatList, sInt, 2, 0.0, 0.0, "Root/Images/AdventureUI/CombatGlossary/Threat|0 Threat Example")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 3, cfTxL, 353.0, 0, "Whichever party member has the highest threat has a 70%%%% chance to\\n" .. 
                                                                          "be targeted, and the second-highest has a 20%%%% chance. All other party\\n"..
                                                                          "members share the remaining 10%%%% chance.\\n\\n"..
                                                                          "Once targeted by an enemy, the party member will lose 60%%%% of their\\n"..
                                                                          "threat with that enemy, so party members tend to cycle in priority.\\n\\n"..
                                                                          "Not all enemies act this way, particularly bosses. Splash-damage\\n"..
                                                                          "abilities will obviously also not target by threat.\\n\\n"..
                                                                          "Some abilities will cause extra threat, even if they don't hit the target.\\n"..
                                                                          "Use of these abilities allows you to focus enemy attention on your \\n"..
                                                                          "party members with more health or defense.\\n\\n"..
                                                                          "Likewise, some abilities can reduce or even zero threat, which allows\\n"..
                                                                          "a weak party member to avoid being targeted.", "Main")
    
    -- |[Combat Items]|
    sInt = "Combat Items"
    fnCreateCombatGlossaryPack(  gzaCombatList, sInt, "Combat Items", 3)
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 0, cfHeadingX, cfHeadingY, gci_Font_AutocenterX, "Combat Items", "Header")
    fnSetGlossaryPackImage(      gzaCombatList, sInt, 1, 0.0, 0.0, "Root/Images/AdventureUI/CombatGlossary/Items|0 Item Example")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 2, cfTxL, 225.0, 0, "Each character has at least two slots to equip combat items in. These\\n" .. 
                                                                          "items can be used on that character's turn during battle, and are\\n"..
                                                                          "usually a free action.\\n\\n"..
                                                                          "Some items, particularly the runestones, can be used over and over \\n"..
                                                                          "with a cooldown. Others have a number of charges per battle.\\n\\n"..
                                                                          "At the end of the battle, the items recover all their charges. They \\n"..
                                                                          "never run out, so make use of them.", "Main")

    -- |[Free Actions]|
    sInt = "Free Actions"
    fnCreateCombatGlossaryPack(  gzaCombatList, sInt, "Free Actions", 3)
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 0, cfHeadingX, cfHeadingY, gci_Font_AutocenterX, "Free Actions", "Header")
    fnSetGlossaryPackImage(      gzaCombatList, sInt, 1, 0.0, 0.0, "Root/Images/AdventureUI/CombatGlossary/Free Actions|0 Free Action Example")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 2, cfTxL, 225.0, 0, "Abilities with a square border are 'free actions', actions you can take\\n" .. 
                                                                          "during your turn that do not end your turn. Most combat items are \\n"..
                                                                          "free actions, as well as switching weapons and some skills.\\n\\n"..
                                                                          "Free actions may still cost MP or CP, but allow you to act more than\\n"..
                                                                          "once per turn. By default, each character gets one free action per\\n"..
                                                                          "turn. Some passive abilities can increase this limit.\\n\\n"..
                                                                          "In addition, there are some 'effortless' abilities that do not even\\n"..
                                                                          "consume a free action. These are marked in their descriptions.\\n"..
                                                                          "Free actions are usually buffs, but can sometimes deal damage or\\n"..
                                                                          "heal. You can do a lot of damage with these, but watch your MP\\n"..
                                                                          "consumption as they can still cost MP.", "Main")

    -- |[MP, CP, and Finishers]|
    sInt = "MP, CP, and Finishers"
    fnCreateCombatGlossaryPack(  gzaCombatList, sInt, "MP, CP, and Finishers", 5)
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 0, cfHeadingX, cfHeadingY, gci_Font_AutocenterX, "MP, CP, and Finishers", "Header")
    fnSetGlossaryPackImage(      gzaCombatList, sInt, 1, 0.0, 0.0, "Root/Images/AdventureUI/CombatGlossary/MP CP Finishers|0 MP Display")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 2, cfTxL, 241, 0, "At the start of combat, your characters will have 50 MP each. Some\\n" .. 
                                                                          "passive abilities or equipment can change this value. Each turn, a\\n"..
                                                                          "character will generate 10 more MP. This is stored to a cap of 100.\\n\\n"..
                                                                          "All of these values can change with equipment or passive abilities. It\\n"..
                                                                          "is also possible to have zero MP regeneration, but it not possible to\\n"..
                                                                          "have negative MP generation.", "Main")
    fnSetGlossaryPackImage(      gzaCombatList, sInt, 3, 0.0, 0.0, "Root/Images/AdventureUI/CombatGlossary/MP CP Finishers|1 CP Display")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 4, cfTxL, 462.0, 0, "When using most abilities, points of CP are generated, represented by \\n"..
                                                                          "purple pips on the character pane. CP is needed for special finisher\\n"..
                                                                          "attacks, which have a special pink border.\\n"..
                                                                          "If an ability has a CP cost, it will be shown as a number over its icon.\\n\\n"..
                                                                          "CP attacks are extra-powerful, dealing enormous damage or providing \\n"..
                                                                          "big stat increases.\\n\\n"..
                                                                          "Both MP and CP are lost when the battle ends, they do not carry to the\\n"..
                                                                          "next fight. Use them or lose them!", "Main")

    -- |[Gems]|
    sInt = "Gems"
    fnCreateCombatGlossaryPack(  gzaCombatList, sInt, "Gems", 9)
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 0, cfHeadingX, cfHeadingY, gci_Font_AutocenterX, "Gems", "Header")
    fnSetGlossaryPackImage(      gzaCombatList, sInt, 1, 0.0, 0.0, "Root/Images/AdventureUI/CombatGlossary/Gems|0 Mergeable Gems")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 2, 820.0, 162.0, 0, "Basic Gems", "Header")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 3, cfTxL, 213, 0, "Gems can be placed in the sockets in your equipment. You can see the\\n" .. 
                                                                        "sockets on the Equip screen. Each gem has unique stats it increases\\n"..
                                                                        "or decreases.\\n\\n"..
                                                                        "Gems with the above colors can be merged together by a gemcutter.\\n"..
                                                                        "Merged gems retain the stats of both original gems. However, you\\n"..
                                                                        "cannot merge two gems of the same color, and this includes gems that\\n"..
                                                                        "have been merged.", "Main")
    fnSetGlossaryPackImage(      gzaCombatList, sInt, 4, 0.0, 0.0, "Root/Images/AdventureUI/CombatGlossary/Gems|1 Merged Gems")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 5, cfTxL, 454, 0, "Each new gem merged in requires a bigger, more rare kind of\\n" .. 
                                                                        "Adamantite. You can find Adamantite in the world, through battles,\\n"..
                                                                        "and for sale from some merchants.", "Main")
    fnSetGlossaryPackImage(      gzaCombatList, sInt, 6, 0.0, 0.0, "Root/Images/AdventureUI/CombatGlossary/Gems|2 Green Gems")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 7, 574.0, 530.0, 0, "Weapon-Only Gems", "Header")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 8, cfTxL, 586, 0, "Green gems cannot be merged, and generally change the damage\\n" .. 
                                                                        "type a weapon has. Only the first one socketed in a weapon takes\\n"..
                                                                        "effect. These change the weapon's basic attack and abilities that make\\n"..
                                                                        "use of the weapon's type.", "Main")
    
    -- |[Paragons]|
    sInt = "Paragons"
    fnCreateCombatGlossaryPack(  gzaCombatList, sInt, "Paragons", 3)
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 0, cfHeadingX, cfHeadingY, gci_Font_AutocenterX, "Paragons", "Header")
    fnSetGlossaryPackImage(      gzaCombatList, sInt, 1, 0.0, 0.0, "Root/Images/AdventureUI/CombatGlossary/Paragons|0 Paragon Header")
    fnSetGlossaryPackDescription(gzaCombatList, sInt, 2, cfTxL, 254, 0, "Paragons are rare, extremely tough versions of normal enemies.\\n" .. 
                                                                        "Their stats are considerably higher, and the rewards are increased\\n"..
                                                                        "to match.\\n\\n"..
                                                                        "To encounter paragons, you must first defeat enough enemies in a\\n"..
                                                                        "given region of the world. You can see the regions in the 'Paragons' tab\\n"..
                                                                        "of the journal.\\n\\n"..
                                                                        "Once enough are defeated, an 'Activator' paragon will spawn\\n"..
                                                                        "somewhere in  that region. This paragon is unique. If you can defeat\\n"..
                                                                        "them, then all other enemies in that region have a chance to become\\n"..
                                                                        "paragons when they respawn. Rest at a campfire to reroll paragons\\n"..
                                                                        "for an area.\\n\\n"..
                                                                        "If you no longer want paragons to spawn in a region, you may disable\\n"..
                                                                        "them from the journal. You will need to exit and re-enter an area for\\n"..
                                                                        "the change to take effect.\\n", "Main")
    
end

-- |[ ========================================= Upload ========================================= ]|
for i = 1, #gzaCombatList, 1 do
    fnUploadGlossaryEntry(gzaCombatList[i])
end

