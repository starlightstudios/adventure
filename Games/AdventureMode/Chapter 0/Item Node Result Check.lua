-- |[ ================================= Item Node Result Check ================================= ]|
--When the player activates an item node, this script is called to figure out what they get. This is
-- a flat-out script call which can trigger cutscenes just like examinations.
--The name of the node and its keyword are passed in.
