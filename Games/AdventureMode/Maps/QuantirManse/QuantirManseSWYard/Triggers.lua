-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "Duties") then
	
	--Player has not seen this scene before.
	local iSawDutiesScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iSawDutiesScene", "N")
	local iDisturbedCoffin = VM_GetVar("Root/Variables/Chapter1/Scenes/iDisturbedCoffin", "N")
	if(iSawDutiesScene == 0.0 and iDisturbedCoffin == 1.0) then
		
		--Variables.
		local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iSawDutiesScene", "N", 1.0)
	
		--Dialogue.
		fnStandardMajorDialogue()
		fnCutscene([[ Append("Voice:[VOICE|Ghost][P]*Return to your duties...*[B][C]") ]])
		
		--If Mei is alone:
		if(bIsFlorentinaPresent == false) then
			fnCutscene([[ Append("Mei:[E|Neutral] (..?[P] I thought I heard something...)[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] (.[P].[P].[P] Guess it was nothing.)[P][CLEAR]") ]])
			fnCutscene([[ Append("Voice:[VOICE|Ghost][P]*You will be punished...*[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Surprise] Is someone there?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Sad] It's -[P] really cold all of a sudden.[P] Something is different...") ]])
			fnCutsceneBlocker()

		--Florentina is here:
		else
			fnCutscene([[ Append("Mei:[E|Neutral] Did you say something?[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] I thought you did.[B][C]") ]])
			fnCutscene([[ Append("Voice:[VOICE|Ghost][P]*You will be punished...*[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Surprise] Okay, that wasn't you.[P] Who said that?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Sad] It's -[P] really cold all of a sudden.[P] Something is different...") ]])
			fnCutsceneBlocker()
		end
	
		--Start the music.
		AL_SetProperty("Activate Fade", 30, gci_Fade_Under_GUI, true, 1, 1, 1, 1, 0.50, 0.50, 1.0, 1, true)
		fnCutscene([[ AL_SetProperty("Music", "QuantirManseTheme") ]])
		fnCutsceneBlocker()
		
		--Re-enable the enemies.
		fnCutscene([[ 
		EM_PushEntity("GhostA0")
			TA_SetProperty("Disabled", false)
		DL_PopActiveObject()
		EM_PushEntity("GhostB0")
			TA_SetProperty("Disabled", false)
		DL_PopActiveObject()
		]])
	
	end
end
