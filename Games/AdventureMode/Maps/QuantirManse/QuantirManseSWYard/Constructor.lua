-- |[Constructor]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "QuantirManseSWYard"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	local iDisturbedCoffin = VM_GetVar("Root/Variables/Chapter1/Scenes/iDisturbedCoffin", "N")
	if(iDisturbedCoffin == 0.0) then
		AL_SetProperty("Music", "Null")
	else
		AL_SetProperty("Music", "QuantirManseTheme")
	end
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
    
    -- |[Map Handling]|
    --Resolve the map's variable.
    fnStandardCh1Map(sLevelName)

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--If the player has not disturbed the coffin yet, disable the ghost enemies.
	local iDisturbedCoffin = VM_GetVar("Root/Variables/Chapter1/Scenes/iDisturbedCoffin", "N")
	if(iDisturbedCoffin == 0.0) then
		EM_PushEntity("GhostA0")
			TA_SetProperty("Disabled", true)
		DL_PopActiveObject()
		EM_PushEntity("GhostB0")
			TA_SetProperty("Disabled", true)
		DL_PopActiveObject()
	end
	
	--Overlay.
	if(iDisturbedCoffin == 1.0) then
		AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0.75, 0.75, 1.0, 1, true)
	
		--Overlays.
        fnFog()
	end

end
