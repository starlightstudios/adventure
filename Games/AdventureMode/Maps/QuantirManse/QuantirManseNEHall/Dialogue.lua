-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Laura]|
    if(sActorName == "Laura") then

        --Common.
        fnStandardMajorDialogue()
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Laura", "Neutral") ]])

        --Variables
        local iHasGhostForm = VM_GetVar("Root/Variables/Global/Mei/iHasGhostForm", "N")
        
        --Mei does not have ghost form, so Laura does not even notice her.
        if(iHasGhostForm == 0.0) then
            
            fnCutscene([[ Append("Mei: (Seems this ghost hasn't even noticed me...)[B][C]") ]])
            fnCutscene([[ Append("Ghost: Yes yes, I'm busy.[P] Go pester someone else!") ]])
        
        --Mei has ghost form.
        else
            fnCutscene([[ Append("Mei:[E|Neutral] Hello...[P] Laura.[B][C]") ]])
            fnCutscene([[ Append("Laura: Oh![P] I'm sorry Natalie, I didn't see you there.[B][C]") ]])
            fnCutscene([[ Append("Laura: Are you taking yet another half-hour break?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Uh, yeah.[P] That's it.[B][C]") ]])
            fnCutscene([[ Append("Laura: Don't let anyone catch you.[P] And I'm not giving you another snack![P] The food is already running low![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] (The food looks like it's ghostly, too...)[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] I wasn't really all that hungry.[B][C]") ]])
            fnCutscene([[ Append("Laura: You better get back to work.[P] Nice seeing you.") ]])
        end
    end
end
