-- |[ ==================================== Field Abilities ===================================== ]|
--Called when a field ability is used, at the discretion of the field ability script. The variable
-- gbFieldAbilityHandledInput should be set to true if the script handled the ability.

-- |[Arguments]|
--Argument Listing:
-- 0: iSwitchCode - Switch code used by the ability. One of the gciFieldAbility_Activate_[X] series.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local iSwitchCode = tonumber(LM_GetScriptArgument(0))

-- |[ ======================================== Handling ======================================== ]|
--Picking open the door locks.
if(iSwitchCode == gciFieldAbility_Activate_Florentina_PickLock) then
    
    -- |[Hit Detection]|
    --Storage.
    local sDoorHit = "Null"
    
    --Get the party leader's position.
    EM_PushEntity(gsPartyLeaderName)
        local iPartyX, iPartyY = TA_GetProperty("Position")
        local iFacing = TA_GetProperty("Facing")
    DL_PopActiveObject()
    
    --Turn to radians.
    local fRadians = (iFacing-2) * 45 * 3.1415926 / 180.0
    
    --Modify by rotation.
    iPartyX = iPartyX + (math.cos(fRadians) * 8.0) - 4
    iPartyY = iPartyY + (math.sin(fRadians) * 8.0) - 8
    
    --List of locations to check. These are in an X arrangement around the center
    -- point, to allow some overlap in case of narrow miss.
    local iaOffX = {0, -4, 4, -4, 4, 12, 12, -12, -12, -4,  4,  -4,   4}
    local iaOffY = {0, -4, -4, 4, 4, -4,  4,  -4,   4, 12, 12, -12, -12}
    
    --Iterate across the offsets, but stop when a hit is registered.
    local bGotHit = false
    for i = 1, #iaOffX, 1 do
        
        --Build, get total.
        AL_GetProperty("Build Objects At Position", iPartyX + iaOffX[i], iPartyY + iaOffY[i])
        local iTotalHits = AL_GetProperty("Total Objects At Position")
        
        --Check all hits.
        for p = 0, iTotalHits-1, 1 do
            
            --Get variables.
            local iType = AL_GetProperty("Type Of Object At Position", p)
            local sName = AL_GetProperty("Name Of Object At Position", p)
            
            --The first 8 letters must be "LockDoor" for this to be valid.
            if(string.sub(sName, 1, 8) == "LockDoor") then
                sDoorHit = sName
                bGotHit = true
                break
            end
        end
        
        --Got a hit, break out.
        if(bGotHit) then break end
    end
    
    -- |[Success Handler]|
    --Handle if we got a hit!
    if(bGotHit == false) then return end
    
    --Flag.
    gbFieldAbilityHandledInput = true
    
    --Check if Florentina is around.
    local bHasFlorentina = false
    for i = 1, #gsaFollowerNames, 1 do
        if(gsaFollowerNames[i] == "Florentina") then
            bHasFlorentina = true
            break
        end
    end
    
    --Not around:
    if(bHasFlorentina == false) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Florentina can't pick the lock if she's not here!)") ]])
        fnCutsceneBlocker()
        return
    end
    
    --There are four statues that are responsible for all the locks. Opening a single door opens all other
    -- related doors.
    if(sDoorHit == "LockDoorA" or sDoorHit == "LockDoorB") then
    
        --Door is already open:
        local iOpenedStatueA = VM_GetVar("Root/Variables/Chapter1/Scenes/iOpenedStatueA", "N")
        if(iOpenedStatueA == 1.0) then return end
    
        --Flag.
        VM_SetVar("Root/Variables/Chapter1/Scenes/iOpenedStatueA", "N", 1.0)
    
        --Dialogue.
        fnCutscene([[ AudioManager_PlaySound("World|PickingLock") ]])
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Florentina:[VOICE|Florentina] This'll take no time.[P] Stand back...[B][C]") ]])
        fnCutscene([[ Append("Florentina:[VOICE|Florentina] Huh, there's an internal mechanism. Don't worry, I can spring it.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutscene([[ AudioManager_PlaySound("World|LockOpen") ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
        --Open doors.
        fnCutscene([[ AL_SetProperty("Open Door", "LockDoorA") ]])
        fnCutscene([[ AudioManager_PlaySound("World|OpenDoor") ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutscene([[ AL_SetProperty("Open Door", "LockDoorB") ]])
        fnCutscene([[ AudioManager_PlaySound("World|OpenDoor") ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Florentina:[VOICE|Florentina] Piece of cake![P] Seems I opened two doors at once.[P] Damn, I'm good!") ]])
        fnCutsceneBlocker()
    
    --DoorC is a solo door.
    elseif(sDoorHit == "LockDoorC") then
    
        --Door is already open:
        local iOpenedStatueB = VM_GetVar("Root/Variables/Chapter1/Scenes/iOpenedStatueB", "N")
        if(iOpenedStatueB == 1.0) then return end
    
        --Flag.
        VM_SetVar("Root/Variables/Chapter1/Scenes/iOpenedStatueB", "N", 1.0)
    
        --Dialogue.
        fnCutscene([[ AudioManager_PlaySound("World|PickingLock") ]])
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Florentina:[VOICE|Florentina] I'm on it...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutscene([[ AudioManager_PlaySound("World|LockOpen") ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
        --Open doors.
        fnCutscene([[ AL_SetProperty("Open Door", "LockDoorC") ]])
        fnCutscene([[ AudioManager_PlaySound("World|OpenDoor") ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Florentina:[VOICE|Florentina] All done!") ]])
        fnCutsceneBlocker()
    
    --Doors D though I:
    elseif(sDoorHit == "LockDoorD" or sDoorHit == "LockDoorE" or sDoorHit == "LockDoorF" or sDoorHit == "LockDoorG" or sDoorHit == "LockDoorH" or sDoorHit == "LockDoorI") then
    
        --Door is already open:
        local iOpenedStatueC = VM_GetVar("Root/Variables/Chapter1/Scenes/iOpenedStatueC", "N")
        if(iOpenedStatueC == 1.0) then return end
    
        --Flag.
        VM_SetVar("Root/Variables/Chapter1/Scenes/iOpenedStatueC", "N", 1.0)
    
        --Dialogue.
        fnCutscene([[ AudioManager_PlaySound("World|PickingLock") ]])
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Florentina:[VOICE|Florentina] All right, cover me.[P] Hmm, there's an extension mechanism in here.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[VOICE|Florentina] Nothing I can't handle!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutscene([[ AudioManager_PlaySound("World|LockOpen") ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
        --Open doors.
        fnCutscene([[ AL_SetProperty("Open Door", "LockDoorD") ]])
        fnCutscene([[ AudioManager_PlaySound("World|OpenDoor") ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutscene([[ AL_SetProperty("Open Door", "LockDoorE") ]])
        fnCutscene([[ AudioManager_PlaySound("World|OpenDoor") ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutscene([[ AL_SetProperty("Open Door", "LockDoorF") ]])
        fnCutscene([[ AudioManager_PlaySound("World|OpenDoor") ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutscene([[ AL_SetProperty("Open Door", "LockDoorG") ]])
        fnCutscene([[ AudioManager_PlaySound("World|OpenDoor") ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutscene([[ AL_SetProperty("Open Door", "LockDoorH") ]])
        fnCutscene([[ AudioManager_PlaySound("World|OpenDoor") ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutscene([[ AL_SetProperty("Open Door", "LockDoorI") ]])
        fnCutscene([[ AudioManager_PlaySound("World|OpenDoor") ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Florentina:[VOICE|Florentina] How many lockpickers can say they've picked six doors at once?[P] Just one, and you're looking at her.") ]])
        fnCutsceneBlocker()
    
    --DoorJ is a solo door.
    elseif(sDoorHit == "LockDoorJ") then
    
        --Door is already open:
        local iOpenedStatueD = VM_GetVar("Root/Variables/Chapter1/Scenes/iOpenedStatueD", "N")
        if(iOpenedStatueD == 1.0) then return end
    
        --Flag.
        VM_SetVar("Root/Variables/Chapter1/Scenes/iOpenedStatueD", "N", 1.0)
    
        --Dialogue.
        fnCutscene([[ AudioManager_PlaySound("World|PickingLock") ]])
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Florentina:[VOICE|Florentina] No problem.[P] Watch my back.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutscene([[ AudioManager_PlaySound("World|LockOpen") ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
        --Open doors.
        fnCutscene([[ AL_SetProperty("Open Door", "LockDoorJ") ]])
        fnCutscene([[ AudioManager_PlaySound("World|OpenDoor") ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Florentina:[VOICE|Florentina] Got it.[P] I make this look easy.") ]])
        fnCutsceneBlocker()
    
    end
    
end
