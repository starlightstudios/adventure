-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
--Crate.
if(sObjectName == "Crate") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Cleaning supplies.[P] Scrap rags, dusters, extra aprons, that sort of thing.)") ]])
    fnCutsceneBlocker()
	
--Foodshelves.
elseif(sObjectName == "Foodshelves") then

	--Pie Job:
	local iSalamiCount = AdInv_GetProperty("Item Count", "Translucent Quantirian Salami")
	local iTakenPieJob = VM_GetVar("Root/Variables/Chapter1/Breanne/iTakenPieJob", "N")
	if(iTakenPieJob == 1.0 and iSalamiCount < 1) then
		
		--Short scene.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Hmmmmmmmm...[P] hey look![P] Quantirian Salami![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Surprise] Yes, the rare see-through kind.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Are you sure this is a good idea?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] It's ghost-salami, but it's still salami![P] This counts![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Okay, I'll let you taste-test the final product first...[B][C]") ]])
		fnCutscene([[ Append("[SOUND|World|TakeItem]*Got Translucent Quantirian Salami*") ]])
		
		--Add the item.
		LM_ExecuteScript(gsItemListing, "Translucent Quantirian Salami")
		
		--Reset flag.
		WD_SetProperty("Clear Topic Read", "Pepper Pie")
		
	--Normal case:
	else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The food doesn't seem to be rotted or decaying, but it's still probably ghost food.[P] No thanks.)") ]])
        fnCutsceneBlocker()
	end

--Coffins.
elseif(sObjectName == "Coffins") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (So many coffins...)") ]])
    fnCutsceneBlocker()

--Barrels
elseif(sObjectName == "Barrels") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The barrel is full of a viscous, black tar...)") ]])
    fnCutsceneBlocker()

-- |[Journals]|
elseif(sObjectName == "JournalA") then

	--Setup.
	fnStandardMajorDialogue()
		
	--Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A sketchbook, with a drawing of an Alraune on it.[P] Read it?)[BLOCK]") ]])

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sure\", " .. sDecisionScript .. ", \"JournalReadA\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"Ignore\") ")
	fnCutsceneBlocker()
	
elseif(sObjectName == "JournalB") then

	--Setup.
	fnStandardMajorDialogue()
		
	--Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A sketchbook, with a drawing of a werecat on it.[P] Read it?)[BLOCK]") ]])

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sure\", " .. sDecisionScript .. ", \"JournalReadB\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"Ignore\") ")
	fnCutsceneBlocker()
	
elseif(sObjectName == "JournalC") then

	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiReadClaudiasJournal", "N", 1.0)
    
    --Variables.
    local iFlorentinaKnowsMeiSavedClaudia = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsMeiSavedClaudia", "N")
    local iSavedClaudia = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedClaudia", "N")

	--Setup.
	fnStandardMajorDialogue()
		
	--If Mei has not met Florentina, she doesn't know who Claudia is...
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	if(bIsFlorentinaPresent == false) then
		fnCutscene([[ Append("Mei:[E|Neutral] (This journal belongs to Claudia Romanus.[P] Looks like this journal is the last one she wrote in.)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] (Should I read it?)[B][C]") ]])
	
	--If Florentina is present, Mei knows who Claudia is.
	else
		fnCutscene([[ Append("Mei:[E|Laugh] Florentina![P] This is Claudia's journal![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] ...[P] Yep, this is hers.[P] The last entry is dated a few weeks back.[B][C]") ]])
        if(iFlorentinaKnowsMeiSavedClaudia == 0.0) then
            fnCutscene([[ Append("Florentina:[E|Neutral] But she's not around.[P] Just creepy ghosts.[P] You don't think...[B][C]") ]])
            if(iSavedClaudia == 0.0) then
                fnCutscene([[ Append("Mei:[E|Neutral] Better read the journal to find out, right?[BLOCK]") ]])
            else
                VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsMeiSavedClaudia", "N", 1.0)
                fnCutscene([[ Append("Mei:[E|Neutral] Oh, her?[P] I already ran into her in the basement.[P] She fell into a trap, she's fine.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Neutral] Oh well, can't win 'em all.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Offended] Excuse me?[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Happy] Just hurry up and read the book, Mei.[BLOCK]") ]])
            end
        else
            fnCutscene([[ Append("Florentina:[E|Neutral] I guess this is the last one she wrote before she got trapped.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Let's see what she wrote![BLOCK]") ]])
        end
	end

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sure\", " .. sDecisionScript .. ", \"JournalReadC\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"Ignore\") ")
	fnCutsceneBlocker()
	
elseif(sObjectName == "JournalD") then

	--Setup.
	fnStandardMajorDialogue()
		
	--If Mei has not met Florentina, she doesn't know who Claudia is...
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	if(bIsFlorentinaPresent == false) then
		fnCutscene([[ Append("Mei:[E|Neutral] (Hey![P] There's some images on this book that look like my runestone!)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] (Should I read it?)[B][C]") ]])
	
	--If Florentina is present, Mei knows who Claudia is.
	else
		fnCutscene([[ Append("Mei:[E|Laugh] F-[P]Florentina, look![P] That symbol![P] It's just like my runestone's![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Hey, calm down, kid.[P] You'll wake the dead[P]. Literally.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Laugh] I can't wait to read it![BLOCK]") ]])
	end

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sure\", " .. sDecisionScript .. ", \"JournalReadD\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"Ignore\") ")
	fnCutsceneBlocker()
	
elseif(sObjectName == "JournalE") then

	--Setup.
	fnStandardMajorDialogue()
	fnCutscene([[ Append("[VOICE|Mei] (A sketchbook, with a drawing of a bee girl on it.[P] There's only a small amount on them.)[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei] ('Bee girls are a species of partirhuman common in many parts of the world. They can survive in any non-freezing non-aquatic biome.')[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei] (That seems to be it.[P] Seems they haven't finished their research.") ]])

--Reading journal A.
elseif(sObjectName == "JournalReadA") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutscene([[ Append("Mei:[E|Neutral] 'Heavenly Doves Research Log::[P] Alraune Partirhuman, Trannadar Region'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] In between the text are sketches of Alraunes.[P] Some of the notes are on which species of plant they take after.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'A very peculiar partirhuman, the Alraune is one of several species capable of coexisting with humans, though they often prefer not to.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'Most partirhumans have extremely potent reproductive drives, yet Alraunes seem to be able to control theirs.[P] Indeed, they have been known to even take jobs in human settlements.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'The Alraune reproductive process is fairly straightforward.[P] The Alraune passively collects pollen from plant species and returns to a communal pool to deposit the collected pollen.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'The pollen is then mixed with water and enchanted.[P] When a human is placed in this mixture, they soon become an Alraune.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'Alraunes are intelligent, can communicate with humans, and also seem to be able to speak with plant species using some unknown mechanism.[P] It is unknown if they can communicate with fungi or animals.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'Attempts to interview Alraunes usually end in conflict, as many of them are 'wild'.[P] These ones are uninterested in humans except for reproduction, and often attack or flee.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'Alraunes seem to be able to recover from nearly any injury.[P] They will regrow a lost limb over a long period of time.[P] They obviously cannot regrow a lost head, though.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'However, they are extremely frail.[P] Alraunes do not bleed the way humans do.[P] A cut that might take a human a few hours to heal from may scar an Alraune for weeks or months.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'Because they take after plants, Alraunes appear to eat rotted material, like mulch, for sustenance, and need sunlight to survive.[P] They can go long periods without sunlight, though, before they wither.[P] None have ever been in captivity long enough to observe this.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'Each Alraune is unique in that it takes affinity after a certain plant species.[P] Some have extra thorns on their body like roses, while others appear to be tulips, or even potatoes.[P] It seems the species may be related to the Alraune's personality.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'Lastly, Alraunes often refer to one another as 'leaf-sister' and to plants as 'little ones'.[P] Their society does not seem to have a hierarchy the way human society does.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] I feel smarter already!") ]])
	VM_SetVar("Root/Variables/Chapter1/Scenes/iReadAlraunes", "N", 1.0)

--Reading journal B.
elseif(sObjectName == "JournalReadB") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutscene([[ Append("Mei:[E|Neutral] 'Heavenly Doves Research Log::[P] Werecat Partirhuman, Trannadar Region'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] There are several sketches of werecats in various poses.[P] It seems the artist was a real cat lover, as there are regular cats here, too.[P] So cute![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'The werecat is a partirhuman species known for their superb hunting skills and extreme agility.[P] Werecats have been reported to be able to leap up cliffs in a single bound.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'Their bone structure and muscle density configure like a spring, allowing them to move extremely quickly in short bursts.[P] They are like common housecats in many respects besides these.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'Werecat society is highly fragmented.[P] Werecats usually move in small groups, between one and four cats.[P] The membership of these prides changes periodically, usually after a major hunt.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'The werecats hunt for food, but also sport.[P] They frequently kill animals simply because they can, and may attack travellers but leave them alive.[P] They are merely in it for the challenge.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'There may be some sort of honor code associated with werecat hunts, but it is inconsistently applied.[P] The few werecats that agreed to an interview expressed disdain for attacking children, for example.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'The reproductive process is difficult to discern.[P] It appears to be connected to the phase of the moon, and a large collection of cats will group together when transforming a human.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'Werecats refer to one another as 'kinfang', though they do not appear to have status or hierarchy.[P] They freely break association with one another, depending on whether they prefer to hunt alone or in a group.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'Hopefully, we will be able to observe a werecat transformation ritual in progress, though the odds of doing this undetected are very low.[P] More research is required.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] I'm going to be an expert on monster girls soon enough!") ]])

--Reading journal C.
elseif(sObjectName == "JournalReadC") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Topic.
	WD_SetProperty("Unlock Topic", "Rilmani", 1)
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutscene([[ Append("Mei:[E|Neutral] 'Heavenly Doves Research Log::[P] Quantir Expedition'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Let's see...[P] this part -[P] Rilmani...?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Hmm, a species that -[P] communicates with symbols![P] Some of these look like the one on my runestone![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'We believe that the large manor near the lake in eastern Trannadar may be an artifact known as the 'Dimensional Trap'.[P] Local maps refer to it as such, and its position is consistent with legend.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'It is probably a misnomer to call it a manor, as it supposedly changes shape periodically to match modern styles.[P] In a few centuries it may have completely changed its layout.[P] Some older stories refer to the structure as a tower, for example.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'The Rilmani species is likely involved with the creation of the building.[P] Most scholars do not believe the Rilmani exist, but I, Claudia Romanus, can personally attest to their existence.[P] I have encountered several on my journeys.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'The building appears to be some sort of low point between dimensions.[P] Objects lost between dimensions tend to accrue there, sometimes doing so years or decades after the fact.[P] In a few cases, explorers claim to have found objects from the future, as well.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'Unfortunately, most of this is uncorroborated legend. [P]There is little in the Dimensional Trap at present, save for a religious cult who has taken up residence there.[P] They have halted all research progress.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] ...[P] Crud![P] She doesn't say anything more about the Rilmani in here...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] ...[P] But she does make a few references to another document.[P] If I can find that, it might have a translation in it![B][C]") ]])
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	if(bIsFlorentinaPresent == true) then
		fnCutscene([[ Append("Florentina:[E|Neutral] Well then let's go find it.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Have you ever heard of a Rilmani, Florentina?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] There's local legends around here, yes.[P] I didn't think they exist.[P] Maybe they don't, but your runestone...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] If they have something to do with dimensional travel, maybe they're the reason I'm here?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Maybe.[P] Let's look for that document.") ]])
    else
		fnCutscene([[ Append("Mei:[E|Neutral] It's gotta be nearby...[B][C]") ]])
	end
	
--Reading journal D.
elseif(sObjectName == "JournalReadD") then
	
	--Flags.
	local iMeiKnowsRilmani = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiKnowsRilmani", "N")
	VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiKnowsRilmani", "N", 1.0)
	
	--Topic.
	WD_SetProperty("Unlock Topic", "Rilmani", 1)
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Clear the flag on this topic if this is the first time reading the book.
	if(iMeiKnowsRilmani == 0.0) then
		WD_SetProperty("Clear Topic Read", "NextMove")
	end
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutscene([[ Append("Mei:[E|Neutral] 'Heavenly Doves Research Log::[P] Rilmani Partirhuman, Trannadar Region'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Diagrams, diagrams...[P] Seems the doves have no idea what these things look like.[P] This one...[P] looks kind of like an olive?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'The Rilmani are believed to be one of the oldest partirhuman species in existence.[P] What little we know of them is based on legend and rumour.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'Artifacts of Rilmani origin are rare and usually prized.[P] They often contain potent magics which cannot be replicated by human sorcery, and thus are sought after by the wise and the greedy alike.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'Most curiously, the Rilmani language does not evolve the way human languages do.[P] Instead, the Rilmani are said to already have every word they will ever need, and instead simply discover the new word at the right time.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'What follows is an incomplete study of known Rilmani words...'[B][C]") ]])
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	if(bIsFlorentinaPresent == true) then
		fnCutscene([[ Append("Mei:[E|Laugh] Courage![P] Valor![P] Bravery![P] Look, Florentina, that's what my runestone means![P] It's a Rilmani word![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] It means all three?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] 'These are the six words that are considered first-order in the Rilmani language.[P] They are only used in specific contexts, though the nature of the context is not known.'[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] So my runestone is one of these first-order words![P] I don't recognize the other five...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] 'Proper nouns are third-order, and are allowed to be created.[P] Most other words are second-order and supposedly have existed as long as the Rilmani have.[P] The first-order words are therefore meant to be older than the Rilmani language.'[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] So just what does that mean?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Laugh] I haven't got any idea![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] But, I bet if we poke around the Dimensional Trap for a bit, we can find some hints.[P] With this, we can try to translate anything we find![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] So...[P] no payoff?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] Oh for crying - [P][CLEAR]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Relax, kid.[P] If we can find a real-live Rilmani, I can probably mug her and get something worth a fortune.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Are you joking right now?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] If they look like this sketch of a cloudy pig, yes.[P] If they look like this one that has all the spikes...[P] not so much...") ]])
	else
		fnCutscene([[ Append("Mei:[E|Laugh] Courage![P] Valor![P] Bravery![P] It's a Rilmani word![P] My runestone is definitely a Rilmani artifact![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] All right, with this translation guide, I bet I can find something at that old mansion to help me get home![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I hope the Rilmani can help me, if it comes to that...") ]])
	end

--Ignore the journal.
elseif(sObjectName == "Ignore") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")

--Bookshelf A.
elseif(sObjectName == "BookshelfA") then

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Mei](This handwriting looks to be the same as the person who was put in charge of burying bodies...[P] Seems there's a few torn pages jammed in various books here...)[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei](Seems this is the first in the series...)[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]('Countess Quantir keeps on disappearing for days at a time.[P] The head maid doesn't know any more than anyone else where she is.')[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]('I asked her about it, but she blew me off.[P] She looks so tired all the time, but then again, everyone is.[P] The sick keep coming and all we can do is ease their pain.')[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]('I think she's working harder than anyone else.[P] She was a great mage, once, and I bet she's trying to find a magical cure.[P] The failure must be eating at her.')") ]])

--Bookshelf B.
elseif(sObjectName == "BookshelfB") then

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Mei](This handwriting looks to be the same as the person who was put in charge of burying bodies...[P] Seems there's a few torn pages jammed in various books here...)[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei](Seems this is the second in the series...)[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]('While the Countess was out, I decided to take a look at some of her old magic books.[P] It's all way above my level, but I tried a few of the incantations and - [P]they worked![P] I was so excited!')[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]('As soon as the Countess got back, I showed her what I learned.[P] I wasn't expecting her to yell at me like that.')[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]('I guess I should have known better.[P] Those were *her* magic books, after all, but it's not like she was using them.')[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]('She looked really, really scared when I told her.[P] I guess she's under a lot of stress.[P] Everyone is counting on her to cure the sickness, because nothing else has worked.')") ]])

--Bookshelf C.
elseif(sObjectName == "BookshelfC") then

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Mei](This handwriting looks to be the same as the person who was put in charge of burying bodies...[P] Seems there's a few torn pages jammed in various books here...)[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei](Seems this is the third in the series...)[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]('Ernie came down sick.[P] Doctor said it was the plague.[P] Now the serving staff are getting sick, even though we've been keeping clean and bathing just like the Countess said.')[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]('I've handled a lot of the dead bodies -[P] am I going to get sick too?[P] I feel great, but so did Ernie.[P] Now, he's pretty much done for.[P] There's no cure...')[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]('...[P] Countess came back after one of her absences today.[P] Ernie had gone in the middle of the night, as had a bunch of the sick patients![P] Countess said she managed to cure them!')[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]('She said she had been working on a healing spell, and it worked![P] But, only on some people, and not others.[P] She's not sure why.[P] But, there's hope![P] She can do it!')[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]('I decided to borrow her books again, and try my best to learn magic, too.[P] Of course I won't be telling her I'm practicing, at least not until I'm sure I can help.[P] But maybe we can cure everyone!')") ]])

--Bookshelf D.
elseif(sObjectName == "BookshelfD") then

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Mei](This handwriting looks to be the same as the person who was put in charge of burying bodies...[P] Seems there's a few torn pages jammed in various books here...)[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei](Seems this is the fourth in the series...)[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]('I asked the head maid where Ernie was, and she said the Countess had to send all the cured patients away to Trannadar.[P] She said they needed to be quarantined so they don't get sick again.')[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]('I didn't know Ernie too well, but May has to be really broken up.[P] She and him were all sweet and such, and I don't think she knew.[P] I bet she wants to head out to Trannadar, but she has to keep helping the sick here.[P] Must be tough.')[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]('I decided to talk to the Countess again, but I didn't tell her about my magic practice.[P] She's really nice on the outside, but I can tell she doesn't like me.[P] She probably holds a grudge about the books.[P] It's okay, she'll come around.')") ]])

--Bookshelf E.
elseif(sObjectName == "BookshelfE") then

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Mei](This handwriting looks to be the same as the person who was put in charge of burying bodies...[P] Seems there's a few torn pages jammed in various books here...)[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei](Seems this is the fifth in the series...)[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]('More of the other maids got sick this week.[P] The Countess said she's trying, but she can't cure them.[P] We had to bury Laura last week.[P] I've never cried so hard.')[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]('Every minute I'm not on duty, I'm reading those books.[P] I still don't understand half of it, but I'm getting better.[P] The Countess mentioned I have a lot of talent, last we talked.[P] I think she knows I'm reading them.')[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]('...[P] Nina's dead.[P] So is Emmeline.[P] There's only five maids left.[P] All the butlers are either dead or really sick.[P] I'm fine, but I think everyone else is showing the first signs.')[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]('I don't want to be alone...')") ]])

--Bookshelf F.
elseif(sObjectName == "BookshelfF") then

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Mei](This handwriting looks to be the same as the person who was put in charge of burying bodies...[P] Seems there's a few torn pages jammed in various books here...)[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei](Seems this is the last in the series...)[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]('Well, I found out where the Countess has been disappearing to.[P] She's been using an invisibility spell and going down into the sewers![P] There's a secret room and everything![P] That must be where she's been working on her healing spell.')[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]('I have to bury Penrose, and...[P] then I'm going to show her what I've learned.[P] No more patients are showing up, I think everyone has either died or fled Quantir.[P] I can't blame them.')[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]('I'm still feeling fine, though.[P] Maybe I'm immune, or maybe one of my spells helped.[P] I don't know.[P] I've never told her, but the Countess is my hero.[P] I wanted to tell her because, if she dies, I won't know what to do.')[B][C]") ]])

	--If Mei doesn't have the sewer key:
	local iSewerKeyCount = AdInv_GetProperty("Item Count", "Quantir Sewer Key")
	if(iSewerKeyCount == 0.0) then
		LM_ExecuteScript(gsItemListing, "Quantir Sewer Key")
		fnCutscene([[ Append("[VOICE|Mei]('I hope she's not sick and hiding it, down there in the sewers.[P] She's given so much for us.[P] We failed, but at least we tried.')[B][C]") ]])
		fnCutscene([[ Append("[VOICE|Mei](Hey, looks like the author left their keyring here.[P] There are several spare keys.)[B][C]") ]])
		fnCutscene([[ Append("[VOICE|Mei][SOUND|World|TakeItem]*Got Quantir Sewer Key*") ]])
	else
		fnCutscene([[ Append("[VOICE|Mei]('I hope she's not sick and hiding it, down there in the sewers.[P] She's given so much for us.[P] We failed, but at least we tried.')") ]])
	end

--Bookshelf X.
elseif(sObjectName == "BookshelfX") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Violence is Usually the Answer'.[P] It's a book about why diplomacy is for losers.)") ]])
    fnCutsceneBlocker()

--Bookshelf Z.
elseif(sObjectName == "BookshelfZ") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Let's Talk Softly'.[P] It's a book about why violence is for losers.)") ]])
    fnCutsceneBlocker()

-- |[Statues]|
elseif(sObjectName == "OddStatueA") then

	local iOpenedStatueA = VM_GetVar("Root/Variables/Chapter1/Scenes/iOpenedStatueA", "N")
	
	if(iOpenedStatueA == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iOpenedStatueA", "N", 1.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought:[VOICE|Leader] (This statue looks different from the others...[P] hey!)[B][C]") ]])
		fnCutscene([[ Append("Thought:[VOICE|Leader] (There's a switch here...)[B][C]") ]])
		fnCutscene([[ Append("Thought:[VOICE|Leader] ([SOUND|World|RemoteDoor]Something was opened...)") ]])
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Open Door", "LockDoorA") ]])
		fnCutscene([[ AL_SetProperty("Open Door", "LockDoorB") ]])
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought:[VOICE|Leader] (I've already flipped the switch on this statue.)") ]])
	end
	
elseif(sObjectName == "OddStatueB") then

	local iOpenedStatueB = VM_GetVar("Root/Variables/Chapter1/Scenes/iOpenedStatueB", "N")
	
	if(iOpenedStatueB == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iOpenedStatueB", "N", 1.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought:[VOICE|Leader] (This statue looks different from the others...[P] hey!)[B][C]") ]])
		fnCutscene([[ Append("Thought:[VOICE|Leader] (There's a switch here...)[B][C]") ]])
		fnCutscene([[ Append("Thought:[VOICE|Leader] ([SOUND|World|RemoteDoor]Something was opened...)") ]])
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Open Door", "LockDoorC") ]])
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought:[VOICE|Leader] (I've already flipped the switch on this statue.)") ]])
	end
	
elseif(sObjectName == "OddStatueC") then

	local iOpenedStatueC = VM_GetVar("Root/Variables/Chapter1/Scenes/iOpenedStatueC", "N")
	
	if(iOpenedStatueC == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iOpenedStatueC", "N", 1.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought:[VOICE|Leader] (This statue looks different from the others...[P] hey!)[B][C]") ]])
		fnCutscene([[ Append("Thought:[VOICE|Leader] (There's a switch here...)[B][C]") ]])
		fnCutscene([[ Append("Thought:[VOICE|Leader] ([SOUND|World|RemoteDoor]Something was opened...)") ]])
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Open Door", "LockDoorD") ]])
		fnCutscene([[ AL_SetProperty("Open Door", "LockDoorE") ]])
		fnCutscene([[ AL_SetProperty("Open Door", "LockDoorF") ]])
		fnCutscene([[ AL_SetProperty("Open Door", "LockDoorG") ]])
		fnCutscene([[ AL_SetProperty("Open Door", "LockDoorH") ]])
		fnCutscene([[ AL_SetProperty("Open Door", "LockDoorI") ]])
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought:[VOICE|Leader] (I've already flipped the switch on this statue.)") ]])
	end
	
elseif(sObjectName == "OddStatueD") then

	local iOpenedStatueD = VM_GetVar("Root/Variables/Chapter1/Scenes/iOpenedStatueD", "N")
	
	if(iOpenedStatueD == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iOpenedStatueD", "N", 1.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought:[VOICE|Leader] (This statue looks different from the others...[P] hey!)[B][C]") ]])
		fnCutscene([[ Append("Thought:[VOICE|Leader] (There's a switch here...)[B][C]") ]])
		fnCutscene([[ Append("Thought:[VOICE|Leader] ([SOUND|World|RemoteDoor]Something was opened...)") ]])
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Open Door", "LockDoorJ") ]])
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought:[VOICE|Leader] (I've already flipped the switch on this statue.)") ]])
	end
		
-- |[Doors]|
elseif(sObjectName == "LockedDoor") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] ([SOUND|World|RemoteDoor]Locked.[P] There's a mechanism attached to the door.[P] Maybe something nearby opens it?)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "BrokenDoor") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] ([SOUND|World|RemoteDoor]The lock is broken.[P] No way I can get through this door.)") ]])
	fnCutsceneBlocker()

-- |[Exits]|
elseif(sObjectName == "ToEntrance") then
	AudioManager_PlaySound("World|FlipSwitch")
	AL_BeginTransitionTo("QuantirManseEntrance", "FORCEPOS:14x7")
	
-- |[Skillbook]|
elseif(sObjectName == "Skillbook") then
    AL_SetProperty("Set Layer Disabled", "SkillbookIco", true)
	LM_ExecuteScript(gsFlorentinaSkillbook, 2)

-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
