-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ===================================== Warp Activation ==================================== ]|
if(sObjectName == "WarpActivate") then
    fnWarpUnlockHandler("Root/Variables/Chapter1/Campfires/iSpookyExterior", 24.25, 13.50)

-- |[ ======================================= Rochea NC+ ======================================= ]|
elseif(sObjectName == "Alraunes") then

    -- |[Activation Check]|
    --To activate, the player must have alraune form without having met Rochea. This is only possible
    -- during NC+.
    local iHasAlrauneForm = VM_GetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N")
    local iMetRochea      = VM_GetVar("Root/Variables/Chapter1/Alraunes/iMetRochea", "N")
    if(iHasAlrauneForm ~= 1.0 or iMetRochea == 1.0) then return end
    
    -- |[Variables]|
    local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
    
    -- |[Flags]|
    VM_SetVar("Root/Variables/Chapter1/Alraunes/iMetRochea", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter1/Alraunes/iIsMeiForeigner", "N", 1.0)
    
    --Topics.
	WD_SetProperty("Unlock Topic", "Alraunes", 1)
	WD_SetProperty("Unlock Topic", "CleansingFungus", 1)
	WD_SetProperty("Unlock Topic", "Cultists", 1)
	WD_SetProperty("Unlock Topic", "Name", 1)
    
    -- |[Spawn NPCs]|
    TA_Create("Rochea")
        TA_SetProperty("Position", -1, -1)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/Rochea/", false)
    DL_PopActiveObject()
    TA_Create("Alraune")
        TA_SetProperty("Position", -1, -1)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
    DL_PopActiveObject()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Voice:[VOICE|Rochea] Leaf-sister![P] Do you not know where you are?[B][C]") ]])
    fnCutscene([[ Append("Mei:[VOICE|Mei] Huh?[P] Who said that?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    -- |[Movement]|
    fnCutsceneMove("Mei", 5.25, 14.50)
    fnCutsceneFace("Mei", -1, 0)
    fnCutsceneTeleport("Rochea", 0.25, 14.50)
    fnCutsceneTeleport("Alraune", 0.25, 14.50)
    fnCutsceneBlocker()
    fnCutsceneMove("Rochea", 1.25, 14.50)
    fnCutsceneBlocker()
    fnCutsceneMove("Rochea", 4.25, 14.50)
    fnCutsceneMove("Alraune", 3.25, 14.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Rochea", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Alraune", "Neutral") ]])
    fnCutscene([[ Append("Rochea:[E|Neutral] Greetings, I am Rochea.[P] This is Thistle.[B][C]") ]])
    
    -- |[Not in Alraune Form]|
    if(sMeiForm ~= "Alraune") then
        fnCutscene([[ Append("Rochea:[E|Neutral] You...[P] are not an alraune.[P] I apologize.[P] The little ones must be playing a trick on me.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Blush] Actually...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(15)
        fnCutsceneBlocker()
    
        --Flash the active character to white. Immediately after, execute the transformation.
        Cutscene_CreateEvent("Flash Mei White", "Actor")
            ActorEvent_SetProperty("Subject Name", "Mei")
            ActorEvent_SetProperty("Flashwhite Quickly")
        DL_PopActiveObject()
        fnCutsceneWait(5)
        fnCutsceneBlocker()
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Alraune.lua") ]])
        fnCutsceneWait(gci_Flashwhite_Ticks_Total)
        fnCutsceneBlocker()

        --Now wait a little bit.
        fnCutsceneWait(30)
        fnCutsceneBlocker()
    
        --Resume dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Rochea", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Alraune", "Neutral") ]])
        fnCutscene([[ Append("Mei:[E|Happy] Ta-daa![B][C]") ]])
        fnCutscene([[ Append("Rochea:[E|Neutral] Very impressive.[B][C]") ]])
        fnCutscene([[ Append("Thistle:[E|Neutral] Some sort of disguise magic![P] Incredible![P] But you cannot hide your pure heart from the little ones.[B][C]") ]])
        fnCutscene([[ Append("Rochea:[E|Neutral] They told us a soul attuned with nature was here.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] It's nice to meet you, Rochea.[P] My name is Mei.[B][C]") ]])
        fnCutscene([[ Append("Rochea:[E|Neutral] But if you are an alraune, surely you know you ought to introduce yourself when you enter a new coven's territory?[B][C]") ]])
    
    -- |[In Alraune Form]|
    else
        fnCutscene([[ Append("Rochea:[E|Neutral] Surely you know you ought to introduce yourself when you enter a new coven's territory?[B][C]") ]])
    end
    fnCutscene([[ Append("Thistle:[E|Neutral] Just where are you from that you do not know the customs here?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Hong Kong.[P] It's...[P] really far away.[B][C]") ]])
    fnCutscene([[ Append("Rochea:[E|Neutral] Oh dear.[P] I have never heard of such a place.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] I had a feeling you'd say that...[B][C]") ]])
    fnCutscene([[ Append("Rochea:[E|Neutral] Then I see no problem here.[P] The little ones will inform you of the local customs.[P] If you are just passing through, feel free to visit.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] My apologies for being rude...[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] But it is very nice to see some friendly faces, indeed, some leaf-sisters I can relate to![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] I'm looking for a way back to my home, so I may call upon you if you can help.[B][C]") ]])
    fnCutscene([[ Append("Thistle:[E|Neutral] Though I am loathe to admit it, the trading post not far from here may be the best place to ask.[P] They know more of the wider world there.[B][C]") ]])
    fnCutscene([[ Append("Rochea:[E|Neutral] Indeed.[P] Just don't ask Florentina for anything.[P] She's likely to take advantage of you.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] I was going that direction anyway.[P] Thank you for the advice.[B][C]") ]])
    fnCutscene([[ Append("Rochea:[E|Neutral] We will be in touch.[P] Good luck on your quest, leaf-sister.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(30)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("Rochea", 1.25, 14.50)
    fnCutsceneMove("Alraune", 0.25, 14.50)
    fnCutsceneBlocker()
    fnCutsceneTeleport("Alraune", -1.75, -1.50)
    fnCutsceneMove("Rochea", 0.25, 14.50)
    fnCutsceneBlocker()
    fnCutsceneTeleport("Rochea", -1.75, -1.50)
    fnCutsceneBlocker()
end
