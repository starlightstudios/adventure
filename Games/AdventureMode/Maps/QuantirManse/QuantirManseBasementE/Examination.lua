-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
--Bag with a hacksaw in it.
if(sObjectName == "HacksawBag") then
	
	--If Mei hasn't taken the hacksaw yet:
	local iGotHacksaw = VM_GetVar("Root/Variables/Chapter1/Scenes/iGotHacksaw", "N")
	if(iGotHacksaw == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iGotHacksaw", "N", 1.0)
		LM_ExecuteScript(gsItemListing, "Hacksaw")
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] (...[P] This pile contains a hacksaw, metal files, all the things you'd need to break out of prison...)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] (And it was put here in full view of anyone behind these fused doors...)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] (Guess I'll take the hacksaw...)") ]])
		fnCutsceneBlocker()
		
		--Teleport away the NPC that shows the hacksaw.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "HacksawNPC")
			ActorEvent_SetProperty("Teleport To", (-100.25 * gciSizePerTile), (-100.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ Append("[SOUND|World|TakeItem]*Got a hacksaw*") ]])
		fnCutsceneBlocker()
		fnCutsceneBlocker()
	
	--Mei has taken the hacksaw:
	else
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] (Leaving this stuff where a prisoner could see it...)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] (It must have been intentional, right?[P] But who would do that?)") ]])
		fnCutsceneBlocker()
	
	end
	
--This door is impassable. But a Slime could just ooze through it.
elseif(sObjectName == "FusedDoorS") then
	
	--Variables.
	local sMeiForm                  = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local iMeiKnowsRilmani          = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiKnowsRilmani", "N")
	local iHasSlimeForm             = VM_GetVar("Root/Variables/Global/Mei/iHasSlimeForm", "N")
	local iHasGhostForm             = VM_GetVar("Root/Variables/Global/Mei/iHasGhostForm", "N")
	local bIsFlorentinaPresent      = fnIsCharacterPresent("Florentina")
	local iFlorentinaKnowsAboutRune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")
	
	--Get Mei's position.
	EM_PushEntity("Mei")
		local fMeiX, fMeiY = TA_GetProperty("Position")
	DL_PopActiveObject()
	
	--South side of the cell door:
	if(fMeiY >= 29.0 * gciSizePerTile) then
	
		--If Florentina is not present:
		if(bIsFlorentinaPresent == false) then
			
			--If Mei is a slime:
			if(sMeiForm == "Slime") then
				fnStandardMajorDialogue()
				fnCutscene([[ Append("Mei:[E|Neutral] (Hm, seems this door was welded shut by something.[P] It won't open.)[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Laugh] (... as if that matters![P] I'll just squeeze through it!)") ]])
				fnCutsceneBlocker()
				
				--Position Mei in front of the door.
                fnCutsceneMove("Mei", 69.25, 30.50)
                fnCutsceneFace("Mei", 0, -1)
				fnCutsceneBlocker()
				
				--Scene.
				fnCutsceneWait(30)
				fnCutsceneBlocker()
				
				--Mei squeezes through.
				for i = 1, 20, 1 do
					Cutscene_CreateEvent("ActorEvent", "Actor")
						ActorEvent_SetProperty("Subject Name", "Mei")
						ActorEvent_SetProperty("Teleport To", (69.25 * gciSizePerTile), (30.50 * gciSizePerTile) - (i * 0.10 * gciSizePerTile))
					DL_PopActiveObject()
					fnCutsceneWait(7)
					fnCutsceneBlocker()
				end
				
				--Scene.
				fnCutsceneWait(30)
				fnCutsceneBlocker()
				
				--Dialogue.
				fnStandardMajorDialogue()
				fnCutscene([[ Append("Mei:[E|Neutral] [P]*huff* [P]*huff*[P] Phew![P][EMOTION|Mei|Happy] That actually felt kinda good!") ]])
				fnCutsceneBlocker()
				
			--If Mei is a ghost:
        elseif(sMeiForm == "Ghost") then
				fnStandardMajorDialogue()
				fnCutscene([[ Append("Mei:[E|Neutral] (Hm, seems this door was welded shut by something.[P] It won't open.)[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Laugh] (... as if that matters![P] I'll just walk through it!)") ]])
				fnCutsceneBlocker()
				
				--Position Mei in front of the door.
                fnCutsceneMove("Mei", 69.25, 30.50)
				fnCutsceneBlocker()
                fnCutsceneFace("Mei", 0, -1)
				fnCutsceneBlocker()
				
				--Scene.
				fnCutsceneWait(30)
				fnCutsceneBlocker()
				
				--Mei squeezes through.
				for i = 1, 20, 1 do
					Cutscene_CreateEvent("ActorEvent", "Actor")
						ActorEvent_SetProperty("Subject Name", "Mei")
						ActorEvent_SetProperty("Teleport To", (69.25 * gciSizePerTile), (30.50 * gciSizePerTile) - (i * 0.10 * gciSizePerTile))
					DL_PopActiveObject()
					fnCutsceneWait(7)
					fnCutsceneBlocker()
				end
				
				--Scene.
				fnCutsceneWait(30)
				fnCutsceneBlocker()
				
				--Dialogue.
				fnStandardMajorDialogue()
				fnCutscene([[ Append("Mei:[E|Neutral] (...[P] Phasing through stuff feels really weird.[P] I don't think ghosts are supposed to walk through walls...)[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Smirk] (So I guess all those ghost videos were full of it!)") ]])
				fnCutsceneBlocker()
			
			--If Mei is not a slime, but has access to slime form:
			elseif(sMeiForm ~= "Slime" and iHasSlimeForm == 1.0) then
				fnStandardMajorDialogue()
				fnCutscene([[ Append("Mei:[E|Neutral] (Hm, seems this door was welded shut by something.[P] It won't open.)[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Offended] (This sucks, there's no way through...)[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Laugh] (Which is what I'd be thinking if I couldn't become a slime at will!)[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] (I'll just come back once I've found a safe spot to transform...)") ]])
				fnCutsceneBlocker()
			
			--If Mei is not a slime, but has access to slime form:
			elseif(sMeiForm ~= "Ghost" and iHasGhostForm == 1.0) then
				fnStandardMajorDialogue()
				fnCutscene([[ Append("Mei:[E|Neutral] (Hm, seems this door was welded shut by something.[P] It won't open.)[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Offended] (This sucks, there's no way through...)[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Laugh] (Which is what I'd be thinking if I couldn't become a ghost at will!)[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] (I'll just come back once I've found a safe spot to transform...)") ]])
				fnCutsceneBlocker()
			
            -- |[Special Slime Scene]|
            --Mei is a human and does not have slime form: Special!
            elseif(sMeiForm == "Human" and iHasSlimeForm == 0.0) then
            
                --Variables.
                local iKnowsSlimesTalk = VM_GetVar("Root/Variables/Chapter1/Scenes/iKnowsSlimesTalk", "N")
                local iMetMeryl        = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetMeryl", "N")

                --Has not met Meryl before:
                if(iMetMeryl == 0.0) then

                    --Flag.
                    VM_SetVar("Root/Variables/Chapter1/Scenes/iMetMeryl", "N", 1.0)
            
                    --Dialogue.
                    fnStandardMajorDialogue()
                    fnCutscene([[ Append("Mei:[E|Neutral] (Hm, seems this door was welded shut by something.[P] It won't open.)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Offended] (There doesn't seem to be any other way through.[P] Guess I'll come back once I figure something out...)") ]])
                    fnCutsceneBlocker()
                    fnCutsceneWait(25)
                    fnCutsceneBlocker()
                    
                    --Spawn a slime.
                    TA_Create("Meryl")
                        TA_SetProperty("Position", -1, -1)
                        TA_SetProperty("Facing", gci_Face_East)
                        TA_SetProperty("Clipping Flag", true)
                        TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
                        fnSetCharacterGraphics("Root/Images/Sprites/Meryl/", false)
                    DL_PopActiveObject()
                            
                    --Movement.
                    fnCutscene([[ AudioManager_PlaySound("World|LittleSplashA") ]])
                    fnCutsceneLayerDisabled("SlimeFrame0", false)
                    fnCutsceneWait(25)
                    fnCutsceneBlocker()
                    fnCutscene([[ WD_SetProperty("Show") ]])
                    fnCutscene([[ Append("Slime:[VOICE|Slime] Jeesum crow, it's cold in here!") ]])
                    fnCutsceneBlocker()
                    fnCutsceneWait(25)
                    fnCutsceneBlocker()
                    
                    --Movement.
                    fnCutscene([[ AudioManager_PlaySound("World|LittleSplashB") ]])
                    fnCutsceneLayerDisabled("SlimeFrame0", true)
                    fnCutsceneLayerDisabled("SlimeFrame1", false)
                    fnCutsceneWait(15)
                    fnCutsceneBlocker()
                    fnCutsceneLayerDisabled("SlimeFrame1", true)
                    fnCutsceneLayerDisabled("SlimeFrame2", false)
                    fnCutsceneWait(15)
                    fnCutsceneBlocker()
                    fnCutsceneTeleport("Meryl", 62.25, 30.00)
                    fnCutscene([[ AudioManager_PlaySound("World|LittleSplashA") ]])
                    fnCutsceneLayerDisabled("SlimeFrame2", true)
                    fnCutsceneWait(15)
                    fnCutsceneBlocker()
                    for i = 62.35, 63.25, 0.05 do
                        fnCutsceneTeleport("Meryl", i, 30.00)
                        fnCutsceneWait(3)
                        fnCutsceneBlocker()
                    end
                    
                    fnCutsceneWait(25)
                    fnCutsceneBlocker()
                    fnCutsceneFace("Mei", -1, 0)
                    fnCutsceneFace("Meryl", -1, 0)
                    fnCutsceneWait(65)
                    fnCutsceneBlocker()
                    fnCutsceneFace("Meryl", 1, 0)
                    fnCutsceneWait(25)
                    fnCutsceneBlocker()

                    --Dialogue.
                    fnCutscene([[ WD_SetProperty("Show") ]])
                    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Purple") ]])
                    fnCutscene([[ Append("Slime:[E|Purple] Woah hey, there's a human down in this dank pit![B][C]") ]])
                    fnCutscene([[ Append("Slime:[E|Purple] The heck are you doing down here, buddy?[P] Did you not see all the creepy monsters and frigid water?[B][C]") ]])
                    if(iKnowsSlimesTalk == 0.0) then
                        VM_SetVar("Root/Variables/Chapter1/Scenes/iKnowsSlimesTalk", "N", 1.0)
                        fnCutscene([[ Append("Mei:[E|Surprise] You can talk![P] And you're a slime![B][C]") ]])
                        fnCutscene([[ Append("Slime:[E|Purple] Ch'yeah![P] Very sharp eyes on you![B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Surprise] How are you talking?[B][C]") ]])
                        fnCutscene([[ Append("Slime:[E|Purple] With my mouth![P] Hehehehe![B][C]") ]])
                        fnCutscene([[ Append("Slime:[E|Purple] Some slimes can talk, most can't, because some of us are just smart![P] And we eat smarty fruits![B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] Well, okay.[P] So long as you're a friendly slime.[B][C]") ]])
                    else
                        fnCutscene([[ Append("Mei:[E|Neutral] Woah, where did you come from?[B][C]") ]])
                        fnCutscene([[ Append("Slime:[E|Purple] The water, silly![P] Isn't it obvious?[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] Sorry, but a lot of things have been trying to kill me today.[P] Are you a friendly slime?[B][C]") ]])
                    end
                    fnCutscene([[ Append("Slime:[E|Purple] Buddy, I am the friendliest slime you'll ever meet![B][C]") ]])
                    fnCutscene([[ Append("Meryl:[E|Purple] Name's Meryl, pleased to meet you![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Mei.[B][C]") ]])
                    fnCutscene([[ Append("Meryl:[E|Purple] So, Mei, I can't help but notice you're staring at that fused door there with some sort of longing.[B][C]") ]])
                    fnCutscene([[ Append("Meryl:[E|Purple] You trying to bone that door?[P] Can I help?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Blush] I'm not trying to bone it, I'm trying to get through it.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] I think there's something over there.[B][C]") ]])
                    fnCutscene([[ Append("Meryl:[E|Purple] Okay![P] I can help![B][C]") ]])
                    fnCutscene([[ Append("Meryl:[E|Purple] I'm a slime, and us slimes can slip through doors.[P] Want some help?[BLOCK]") ]])
                    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
                    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
                    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sure\", " .. sDecisionScript .. ", \"SlimeTF\") ")
                    fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"NoMeryl\") ")
                    fnCutsceneBlocker()
                
                --Has met Meryl before:
                else
                
                    --If Meryl is on the map already:
                    if(EM_Exists("Meryl")) then
                        
                    --Otherwise, spawn her:
                    else
                        TA_Create("Meryl")
                            TA_SetProperty("Position", -1, -1)
                            TA_SetProperty("Facing", gci_Face_East)
                            TA_SetProperty("Clipping Flag", true)
                            TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
                            fnSetCharacterGraphics("Root/Images/Sprites/Meryl/", false)
                        DL_PopActiveObject()
                    end
                    fnCutsceneFace("Meryl", 1, 0)
                            
                    --Movement.
                    fnCutscene([[ AudioManager_PlaySound("World|LittleSplashA") ]])
                    fnCutsceneLayerDisabled("SlimeFrame0", false)
                    fnCutsceneWait(25)
                    fnCutsceneBlocker()
                    fnCutscene([[ WD_SetProperty("Show") ]])
                    fnCutscene([[ Append("Meryl:[VOICE|Slime] Gah, it continues to be extremely cold in here![P] I didn't learn my lesson at all!") ]])
                    fnCutsceneBlocker()
                    fnCutsceneWait(25)
                    fnCutsceneBlocker()
                    
                    --Movement.
                    fnCutscene([[ AudioManager_PlaySound("World|LittleSplashB") ]])
                    fnCutsceneLayerDisabled("SlimeFrame0", true)
                    fnCutsceneLayerDisabled("SlimeFrame1", false)
                    fnCutsceneWait(15)
                    fnCutsceneBlocker()
                    fnCutsceneLayerDisabled("SlimeFrame1", true)
                    fnCutsceneLayerDisabled("SlimeFrame2", false)
                    fnCutsceneWait(15)
                    fnCutsceneBlocker()
                    fnCutsceneTeleport("Meryl", 62.25, 30.00)
                    fnCutscene([[ AudioManager_PlaySound("World|LittleSplashA") ]])
                    fnCutsceneLayerDisabled("SlimeFrame2", true)
                    fnCutsceneWait(15)
                    fnCutsceneBlocker()
                    for i = 62.35, 63.25, 0.05 do
                        fnCutsceneTeleport("Meryl", i, 30.00)
                        fnCutsceneWait(3)
                        fnCutsceneBlocker()
                    end
                    
                    fnCutsceneWait(25)
                    fnCutsceneBlocker()
                    fnCutsceneFace("Mei", -1, 0)
                    fnCutsceneFace("Meryl", -1, 0)
                    fnCutsceneWait(65)
                    fnCutsceneBlocker()
                    fnCutsceneFace("Meryl", 1, 0)
                    fnCutsceneWait(25)
                    fnCutsceneBlocker()

                    --Dialogue.
                    fnCutscene([[ WD_SetProperty("Show") ]])
                    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Purple") ]])
                    fnCutscene([[ Append("Meryl:[E|Purple] Jeez, I gotta take a second![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Surprise] You all right, Meryl?[B][C]") ]])
                    fnCutscene([[ Append("Meryl:[E|Purple] Oh yeah, this is nothing.[P] Homeostasis is barely a challenge for me.[B][C]") ]])
                    fnCutscene([[ Append("Meryl:[E|Purple] How about you?[P] Still trying to find a way through?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Yeah.[B][C]") ]])
                    fnCutscene([[ Append("Meryl:[E|Purple] Want some help? I know you're very independent but it's okay to rely on others![BLOCK]") ]])
                    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
                    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
                    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sure\", " .. sDecisionScript .. ", \"SlimeTF\") ")
                    fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"NoMeryl\") ")
                    fnCutsceneBlocker()
                end
            
            
			--Mei is not a slime/ghost and does not have slime/ghost form:
			else
				fnStandardMajorDialogue()
				fnCutscene([[ Append("Mei:[E|Neutral] (Hm, seems this door was welded shut by something.[P] It won't open.)[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Offended] (There doesn't seem to be any other way through.[P] Guess I'll come back once I figure something out...)") ]])
				fnCutsceneBlocker()
			
			end

		--Florentina is present:
		else

			--If Mei is a slime:
			if(sMeiForm == "Slime") then
				fnStandardMajorDialogue()
				fnCutscene([[ Append("Mei:[E|Neutral] Hm, seems this door was welded shut by something.[P] It won't open.[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Laugh] ... as if that matters![P] I'll just squeeze through it![B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Neutral] So I'll just wait here then?[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] I'll try to find a way around for you.[P] I'll come right back, promise.") ]])
				fnCutsceneBlocker()
				
				--Position Mei in front of the door.
				Cutscene_CreateEvent("ActorEvent", "Actor")
					ActorEvent_SetProperty("Subject Name", "Mei")
					ActorEvent_SetProperty("Move To", (69.25 * gciSizePerTile), (30.50 * gciSizePerTile))
				DL_PopActiveObject()
				Cutscene_CreateEvent("ActorEvent", "Actor")
					ActorEvent_SetProperty("Subject Name", "Florentina")
					ActorEvent_SetProperty("Move To", (68.25 * gciSizePerTile), (30.50 * gciSizePerTile))
				DL_PopActiveObject()
				fnCutsceneBlocker()
				Cutscene_CreateEvent("ActorEvent", "Actor")
					ActorEvent_SetProperty("Subject Name", "Florentina")
					ActorEvent_SetProperty("Face",  1, 0)
				DL_PopActiveObject()
				Cutscene_CreateEvent("ActorEvent", "Actor")
					ActorEvent_SetProperty("Subject Name", "Mei")
					ActorEvent_SetProperty("Face",  0, -1)
				DL_PopActiveObject()
				fnCutsceneBlocker()
				
				--Scene.
				fnCutsceneWait(30)
				fnCutsceneBlocker()
				
				--Mei squeezes through.
				for i = 1, 20, 1 do
					Cutscene_CreateEvent("ActorEvent", "Actor")
						ActorEvent_SetProperty("Subject Name", "Mei")
						ActorEvent_SetProperty("Teleport To", (69.25 * gciSizePerTile), (30.50 * gciSizePerTile) - (i * 0.10 * gciSizePerTile))
					DL_PopActiveObject()
					fnCutsceneWait(7)
					fnCutsceneBlocker()
				end
				
				--Scene.
				fnCutsceneWait(30)
				fnCutsceneBlocker()
				
				--Dialogue.
				fnStandardMajorDialogue()
				fnCutscene([[ Append("Mei:[E|Neutral] [P]*huff* [P]*huff*[P] Phew![P][EMOTION|Mei|Happy] That actually felt kinda good![B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Neutral] Take your time, slimey.") ]])
				fnCutsceneBlocker()
				
				--Remove Florentina from the follow list.
				AL_SetProperty("Unfollow Actor Name", "Florentina")

			--If Mei is a ghost:
			elseif(sMeiForm == "Ghost") then
				fnStandardMajorDialogue()
				fnCutscene([[ Append("Mei:[E|Neutral] Hm, seems this door was welded shut by something.[P] It won't open.[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Laugh] ... as if that matters![P] I'll just walk through it![B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Neutral] So I'll just wait here then?[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] I'll try to find a way around for you.[P] I'll come right back, promise.") ]])
				fnCutsceneBlocker()
				
				--Position Mei in front of the door.
				Cutscene_CreateEvent("ActorEvent", "Actor")
					ActorEvent_SetProperty("Subject Name", "Mei")
					ActorEvent_SetProperty("Move To", (69.25 * gciSizePerTile), (30.50 * gciSizePerTile))
				DL_PopActiveObject()
				Cutscene_CreateEvent("ActorEvent", "Actor")
					ActorEvent_SetProperty("Subject Name", "Florentina")
					ActorEvent_SetProperty("Move To", (68.25 * gciSizePerTile), (30.50 * gciSizePerTile))
				DL_PopActiveObject()
				fnCutsceneBlocker()
				Cutscene_CreateEvent("ActorEvent", "Actor")
					ActorEvent_SetProperty("Subject Name", "Florentina")
					ActorEvent_SetProperty("Face",  1, 0)
				DL_PopActiveObject()
				Cutscene_CreateEvent("ActorEvent", "Actor")
					ActorEvent_SetProperty("Subject Name", "Mei")
					ActorEvent_SetProperty("Face",  0, -1)
				DL_PopActiveObject()
				fnCutsceneBlocker()
				
				--Scene.
				fnCutsceneWait(30)
				fnCutsceneBlocker()
				
				--Mei squeezes through.
				for i = 1, 20, 1 do
					Cutscene_CreateEvent("ActorEvent", "Actor")
						ActorEvent_SetProperty("Subject Name", "Mei")
						ActorEvent_SetProperty("Teleport To", (69.25 * gciSizePerTile), (30.50 * gciSizePerTile) - (i * 0.10 * gciSizePerTile))
					DL_PopActiveObject()
					fnCutsceneWait(7)
					fnCutsceneBlocker()
				end
				
				--Scene.
				fnCutsceneWait(30)
				fnCutsceneBlocker()
				
				--Dialogue.
				fnStandardMajorDialogue()
				fnCutscene([[ Append("Mei:[E|Neutral] ...[P] That felt really weird.[P] Youku lied, ghosts aren't supposed to walk through walls![B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Neutral] Take your time, spooks.") ]])
				fnCutsceneBlocker()
				
				--Remove Florentina from the follow list.
				AL_SetProperty("Unfollow Actor Name", "Florentina")
			
			--If Mei is not a slime, but has access to slime form:
			elseif(sMeiForm ~= "Slime" and iHasSlimeForm == 1.0) then
				fnStandardMajorDialogue()
				fnCutscene([[ Append("Mei:[E|Neutral] Hm, seems this door was welded shut by something.[P] It won't open.[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Happy] No biggy, I'll just turn into a slime and squeeze through![B][C]") ]])
				
				if(iFlorentinaKnowsAboutRune == 0.0) then
					fnCutscene([[ Append("Florentina:[E|Surprise] Care to repeat that bit of blithering idiocy?[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Happy] Oh, have I not shown you yet?[P] Okay![P] But, not here.[P] We need to find a safe spot.[B][C]") ]])
					fnCutscene([[ Append("Florentina:[E|Happy] This is gonna be good, isn't it?") ]])
				else
					fnCutscene([[ Append("Florentina:[E|Neutral] So what's stopping you?[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] I can only do it in certain places.[P] I don't know why, I can just feel when it's right.[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] We'll need to go to a campsite.[B][C]") ]])
					fnCutscene([[ Append("Florentina:[E|Neutral] Lead the way, then.") ]])
				end
				fnCutsceneBlocker()
			
			--If Mei is not a ghost, but has access to slime form:
			elseif(sMeiForm ~= "Ghost" and iHasGhostForm == 1.0) then
				fnStandardMajorDialogue()
				fnCutscene([[ Append("Mei:[E|Neutral] Hm, seems this door was welded shut by something.[P] It won't open.[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Happy] No biggy, I'll just turn into a ghost and walk through![B][C]") ]])
				
				if(iFlorentinaKnowsAboutRune == 0.0) then
					fnCutscene([[ Append("Florentina:[E|Surprise] Care to repeat that bit of blithering idiocy?[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Happy] Oh, have I not shown you yet?[P] Okay![P] But, not here.[P] We need to find a safe spot.[B][C]") ]])
					fnCutscene([[ Append("Florentina:[E|Happy] This is gonna be good, isn't it?") ]])
				else
					fnCutscene([[ Append("Florentina:[E|Neutral] So what's stopping you?[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] I can only do it in certain places.[P] I don't know why, I can just feel when it's right.[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] We'll need to go to a campsite.[B][C]") ]])
					fnCutscene([[ Append("Florentina:[E|Neutral] Lead the way, then.") ]])
				end
				fnCutsceneBlocker()
			
			--Mei is not a slime/ghost and does not have slime/ghost form:
			else
				fnStandardMajorDialogue()
				fnCutscene([[ Append("Mei:[E|Neutral] Hm, seems this door was welded shut by something.[P] It won't open.[B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Neutral] Hm, I can't think of a way to get through, and I don't see a way around.[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Offended] Guess we'll come back once we figure something out...") ]])
				fnCutsceneBlocker()
			
			end
		end
	
	--North side. Since it's impossible to get here if Mei is not a slime, we don't do that check. This means debug/glitch cases are handled!
	else
	
		--Dialogue.
		fnStandardMajorDialogue()
		fnCutscene([[ Append("Mei:[E|Neutral] (Here we go...)") ]])
		fnCutsceneBlocker()
		
		--Position Mei in front of the door.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move To", (69.25 * gciSizePerTile), (28.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Face",  0, 1)
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
		--Scene.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--Mei squeezes through.
		for i = 1, 20, 1 do
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Mei")
				ActorEvent_SetProperty("Teleport To", (69.25 * gciSizePerTile), (28.50 * gciSizePerTile) + (i * 0.10 * gciSizePerTile))
			DL_PopActiveObject()
			fnCutsceneWait(7)
			fnCutsceneBlocker()
		end
		
		--Scene.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--Variables.
		local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
		
		--If Florentina is not present:
		if(bIsFlorentinaPresent == false) then
			
			fnStandardMajorDialogue()
			
			--Slime:
			if(sMeiForm == "Slime") then
				fnCutscene([[ Append("Mei:[E|Neutral] [P]*huff*[P] ooh, that feels incredible...[P] I love feeling so full...[P] so squishy...") ]])
			
			--Ghost:
			else
				fnCutscene([[ Append("Mei:[E|Neutral] This feels so weird...") ]])
			
			end
			fnCutsceneBlocker()
		
		--If Florentina is present:
		else
			--Variables.
			local iMetClaudia = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetClaudia", "N")
			local iSavedClaudia = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedClaudia", "N")
			local iFlorentinaKnowsMeiSavedClaudia = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsMeiSavedClaudia", "N")
			
			--If Mei came back after finding Claudia and giving her the hacksaw:
			if(iFlorentinaKnowsMeiSavedClaudia == 0.0 and iMetClaudia == 1.0 and iSavedClaudia == 1.0) then
				VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsMeiSavedClaudia", "N", 1.0)
				fnCutscene([[ WD_SetProperty("Show") ]])
				fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
				fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
				fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
				fnCutscene([[ Append("Florentina:[E|Neutral] I heard something going on in there.[P] What happened?[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] I found Claudia![P] She's stuck in there![B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] I gave her a hacksaw I found.[P] She said she's going to saw the bars off after she performed some rites, or something.[B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Neutral] And yet you come out here without any cash to speak of...[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] Oh...[P] Yeah, I showed her my runestone, but she's not exactly in a position to pay us.[B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Neutral] Yeah, understandable.[P] Did she know what it means?[B][C]") ]])
				
				if(iMeiKnowsRilmani == 0.0) then
					fnCutscene([[ Append("Mei:[E|Neutral] No, but she did say she had written about the symbol.[P] She left her research notes in the northeast wing of the Quantir Estate![B][C]") ]])
					fnCutscene([[ Append("Florentina:[E|Confused] Feh, guess we're going up there.[P] Shame about the payoff...[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Happy] I'm sure we'll find some loot for you to tote around.[B][C]") ]])
					fnCutscene([[ Append("Florentina:[E|Blush] Aww, you know how to lead a girl on don't you?") ]])
				
				else
					fnCutscene([[ Append("Mei:[E|Neutral] Nothing we didn't already find in her research, unfortunately.[B][C]") ]])
					fnCutscene([[ Append("Florentina:[E|Neutral] Guess it's back to the Dimensional Trap, then.[P] Shame about the payoff...[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Happy] I'm sure we'll find some loot for you to tote around.[B][C]") ]])
					fnCutscene([[ Append("Florentina:[E|Blush] Aww, you know how to lead a girl on don't you?") ]])
				end
				fnCutsceneBlocker()
			
			--If Mei didn't save Claudia.
			elseif(iFlorentinaKnowsMeiSavedClaudia == 0.0 and iMetClaudia == 1.0 and iSavedClaudia == 0.0) then
				VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsMeiSavedClaudia", "N", 1.0)
				fnCutscene([[ WD_SetProperty("Show") ]])
				fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
				fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
				fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
				fnCutscene([[ Append("Florentina:[E|Neutral] I heard something going on in there.[P] What happened?[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] I found Claudia![P] She's stuck in there![B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] I -[P] couldn't get her out, unfortunately.[B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Neutral] Guess that means the cash reward is out.[P] Maybe I'll hire some workers to come get her.[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] I showed her my runestone![B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Neutral] Did she know what it means?[B][C]") ]])
				
				if(iMeiKnowsRilmani == 0.0) then
					fnCutscene([[ Append("Mei:[E|Neutral] No, but she did say she had written about the symbol.[P] She left her research notes in the northeast wing of the Quantir Estate![B][C]") ]])
					fnCutscene([[ Append("Florentina:[E|Confused] Feh, guess we're going up there.[P] Shame about the payoff...[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Happy] I'm sure we'll find some loot for you to tote around.[B][C]") ]])
					fnCutscene([[ Append("Florentina:[E|Blush] Aww, you know how to lead a girl on don't you?") ]])
				
				else
					fnCutscene([[ Append("Mei:[E|Neutral] Nothing we didn't already find in her research, unfortunately.[B][C]") ]])
					fnCutscene([[ Append("Florentina:[E|Neutral] Guess it's back to the Dimensional Trap, then.[P] Shame about the payoff...[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Happy] I'm sure we'll find some loot for you to tote around.[B][C]") ]])
					fnCutscene([[ Append("Florentina:[E|Blush] Aww, you know how to lead a girl on don't you?") ]])
				end
				fnCutsceneBlocker()
			
			--Otherwise, Florentina joins up without discussing Claudia.
			else
			
				fnStandardMajorDialogue()
				fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
				
				--Slime version.
				if(sMeiForm == "Slime") then
					fnCutscene([[ Append("Mei:[E|Neutral] [P]*huff*[P] ooh, that feels so incredible...[P] I love feeling so full...[B][C]") ]])
					fnCutscene([[ Append("Florentina:[E|Offended] I am so glad I had to sit here and watch you do that![P] Let's get going before I throw up!") ]])
					fnCutsceneBlocker()
				
				--Ghost version:
				else
					fnCutscene([[ Append("Mei:[E|Neutral] It feels so weird, walking through stuff.[P] I don't think I like it.[B][C]") ]])
					fnCutscene([[ Append("Florentina:[E|Offended] Well then don't do that.[P] Duh.") ]])
					fnCutsceneBlocker()
				end
			end
				
			--Wait a bit.
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			
			--Rejoin Florentina to follow Mei.
			EM_PushEntity("Florentina")
				local iFlorentinaID = RE_GetID()
			DL_PopActiveObject()

			--Store it and tell her to follow.
			giaFollowerIDs = {iFlorentinaID}
			AL_SetProperty("Follow Actor ID", iFlorentinaID)
			
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Florentina")
				ActorEvent_SetProperty("Move To", (69.25 * gciSizePerTile), (30.50 * gciSizePerTile))
			DL_PopActiveObject()
			fnCutsceneBlocker()
			fnCutscene([[ AL_SetProperty("Fold Party") ]])
			fnCutsceneBlocker()
		end
	end
	
--Ooze again!
elseif(sObjectName == "FusedDoorN") then
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	--Get Mei's position.
	EM_PushEntity("Mei")
		local fMeiX, fMeiY = TA_GetProperty("Position")
	DL_PopActiveObject()
	
	--South position, ooze north.
	if(fMeiY >= 23.5 * gciSizePerTile) then
	
		--Dialogue.
		fnStandardMajorDialogue()
		if(sMeiForm == "Slime") then
			fnCutscene([[ Append("Mei:[E|Blush] (Okay.[P] Try not to cum this time...)") ]])
			fnCutsceneBlocker()
		else
			fnCutscene([[ Append("Mei:[E|Blush] (Okay.[P] Here goes nothing...)") ]])
			fnCutsceneBlocker()
		end
		
		--Position Mei in front of the door.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move To", (69.25 * gciSizePerTile), (24.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Face",  0, -1)
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
		--Scene.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--Mei squeezes through.
		for i = 1, 20, 1 do
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Mei")
				ActorEvent_SetProperty("Teleport To", (69.25 * gciSizePerTile), (24.50 * gciSizePerTile) - (i * 0.10 * gciSizePerTile))
			DL_PopActiveObject()
			fnCutsceneWait(7)
			fnCutsceneBlocker()
		end
		
		--Scene.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
	
		--Dialogue.
		fnStandardMajorDialogue()
		if(sMeiForm == "Slime") then
			fnCutscene([[ Append("Mei:[E|Blush] (Ungh... I...)[B][C]") ]])
			fnCutscene([[ Append("[VOICE|Mei] *squish*[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Blush] (Still got a job to do...)") ]])
		else
			fnCutscene([[ Append("Mei:[E|Smirk] (Maybe I'm getting used to being a ghost...)") ]])
		end
		fnCutsceneBlocker()
	
	--North position, ooze north.
	else
	
		--Dialogue.
		fnStandardMajorDialogue()
		if(sMeiForm == "Slime") then
			fnCutscene([[ Append("Mei:[E|Blush] (Unnnghhhh...)") ]])
		else
			fnCutscene([[ Append("Mei:[E|Smirk] (Here we go...)") ]])
		end
		fnCutsceneBlocker()
		
		--Position Mei in front of the door.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move To", (69.25 * gciSizePerTile), (22.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Face",  0, 1)
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
		--Scene.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--Mei squeezes through.
		for i = 1, 20, 1 do
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Mei")
				ActorEvent_SetProperty("Teleport To", (69.25 * gciSizePerTile), (22.50 * gciSizePerTile) + (i * 0.10 * gciSizePerTile))
			DL_PopActiveObject()
			fnCutsceneWait(7)
			fnCutsceneBlocker()
		end
		
		--Scene.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
	
		--Dialogue.
		if(sMeiForm == "Slime") then
			fnStandardMajorDialogue()
			fnCutscene([[ Append("Mei:[E|Blush] (Sooooo sliiiimy....)") ]])
			fnCutsceneBlocker()
		end
	
	end

-- |[Respond to Meryl: Yes]|
elseif(sObjectName == "SlimeTF") then

    --Clear censor bars.
    WD_SetProperty("Clear Censor Bars")
    
    --Clean.
	WD_SetProperty("Hide")
    
    --Load images.
    fnLoadDelayedBitmapsFromList("Chapter 1 Mei Slime TF", gciDelayedLoadLoadAtEndOfTick)

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Purple") ]])
    fnCutscene([[ Append("Mei:[E|Happy] Sure!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Meryl", 63.25, 30.50)
    fnCutsceneMove("Meryl", 69.25, 30.50)
    fnCutsceneMove("Mei", 70.25, 30.50)
    fnCutsceneFace("Mei", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Purple") ]])
    fnCutscene([[ Append("Meryl:[E|Purple] So you need to get through here?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Happy] Yeah, if you could - [P][CLEAR]") ]])
    fnCutscene([[ Append("Mei:[E|Surprise] Woah!") ]])
    fnCutsceneBlocker()
    
    --Scene.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Activate Scene") ]])
    fnCutscene([[ WD_SetProperty("Set Scene Image", "Null") ]])
    fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/SlimeTF0") ]])
    fnCutscene([[ Append("Mei:[Voice|Mei] What are you doing? Hey, stop that![B][C]") ]])
    fnCutscene([[ Append("Meryl:[Voice|Slime] But you said - [P][CLEAR]") ]])
    fnCutscene([[ Append("Mei:[Voice|Mei] I wanted [P]*you*[P] to go through and find a way to open it![B][C]") ]])
    fnCutscene([[ Append("Mei tried to shake the slime off, but it stuck to her and began to spread.[B][C]") ]])
    fnCutscene([[ Append("In mere seconds, she was covered.[P] Meryl the slime tried to stop it, but was powerless.") ]])
    fnCutsceneBlocker()
    
    --Next scene.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
    fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/SlimeTF0") ]])
    fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/SlimeTF1") ]])
    fnCutscene([[ Append("Mei:[Voice|Mei] Wh-[P]what?[P] I'm becoming...[B][C]") ]])
    fnCutscene([[ Append("The infection reached up to Mei's head.[P] Suddenly, the spread of the slime wasn't a problem.[P] In fact, it began to feel pleasurable...[B][C]") ]])
    fnCutscene([[ Append("Mei:[Voice|Mei] More slime...[P] rub me...[B][C]") ]])
    fnCutscene([[ Append("Meryl:[Voice|Slime] Hey, you sure about that?[P] You'll turn into a slime![B][C]") ]])
    fnCutscene([[ Append("Mei:[Voice|Mei] Yeah, I want to.[P] Turn me.[P] Turn me![B][C]") ]])
    fnCutscene([[ Append("Unsure, Meryl the slime rubbed Mei's body, spreading more slime.[P] It swirled over Mei's body and turned her, inside and out.") ]])
    fnCutsceneBlocker()

    --Finished.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
    fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Slime.lua") ]])
    fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/SlimeTF1") ]])
    fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/SlimeTF2") ]])
    fnCutscene([[ WD_SetProperty("Register Censor Bar", 612, 184, 92, 39) ]])
    fnCutscene([[ Append("Mei:[Voice|Mei] Slime...[P] I'm a slime...[P] I'm a slime![B][C]") ]])
    fnCutscene([[ Append("Meryl:[Voice|Slime] As long as you're happy, buddy.[P] And hey, now you can squeeze through![B][C]") ]])
    fnCutscene([[ Append("Mei had become a slimegirl, top to bottom.[P] She smiled to herself.[P] Now she could get through the bars!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Purple") ]])
    fnCutscene([[ Append("Meryl:[E|Purple] So um, you're gonna go through the bars now?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] I mean yeah, may as well now that we went through all this trouble.[P] Thanks, Meryl![B][C]") ]])
    fnCutscene([[ Append("Meryl:[E|Purple] No problem![P] Good luck, buddy!") ]])
    fnCutsceneBlocker()
    fnCutsceneMove("Meryl", 67.25, 30.50)
    fnCutsceneFace("Meryl", 0, 1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Clean up images.
    fnCutscene([[ fnUnloadBitmapsFromList("Chapter 1 Mei Slime TF") ]])
                
-- |[Respond to Meryl: No]|
elseif(sObjectName == "NoMeryl") then
    
    --Clean.
	WD_SetProperty("Hide")
    
    --Load images.
    fnLoadDelayedBitmapsFromList("Chapter 1 Mei Slime TF", gciDelayedLoadLoadAtEndOfTick)

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Purple") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] Thanks for the offer, but I think I'll figure it out on my own.[B][C]") ]])
    fnCutscene([[ Append("Meryl:[E|Purple] Awww, and I wanted to be all helpful and junk![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] You're doing great, I just like to do things on my own.[B][C]") ]])
    fnCutscene([[ Append("Meryl:[E|Purple] Hey, if you need my help, just ask.[P] See ya around!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneFace("Meryl", -1, 0)
    for i = 63.35, 62.25, -0.05 do
        fnCutsceneTeleport("Meryl", i, 30.00)
        fnCutsceneWait(4)
        fnCutsceneBlocker()
    end
    fnCutscene([[ AudioManager_PlaySound("World|LittleSplashB") ]])
    fnCutsceneTeleport("Meryl", -100.25, -100.00)
    fnCutsceneLayerDisabled("SlimeFrame3", false)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneLayerDisabled("SlimeFrame3", true)
    fnCutsceneLayerDisabled("SlimeFrame4", false)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneLayerDisabled("SlimeFrame4", true)
    fnCutsceneLayerDisabled("SlimeFrame5", false)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Meryl:[VOICE|Slime] Coooold!!!!") ]])
    fnCutsceneBlocker()
    fnCutsceneLayerDisabled("SlimeFrame5", true)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|LittleSplashA") ]])
    fnCutsceneLayerDisabled("SlimeFrame2", true)
    fnCutsceneWait(15)
    fnCutsceneBlocker()

-- |[Exits]|
--To the Quantir Mansion Basement.
elseif(sObjectName == "NWExit") then
	AudioManager_PlaySound("World|FlipSwitch")
	AL_BeginTransitionTo("QuantirManseBasementW", "FORCEPOS:58.5x20")

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
