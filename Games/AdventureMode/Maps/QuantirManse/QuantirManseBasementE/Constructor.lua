-- |[Constructor]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "QuantirManseBasementE"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "QuantirManseThemeLow")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
    
    -- |[Map Handling]|
    --Resolve the map's variable.
    fnStandardCh1Map(sLevelName)

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then
	
	--If the player has not taken the Hacksaw yet, spawn the Hacksaw NPC.
	local iGotHacksaw = VM_GetVar("Root/Variables/Chapter1/Scenes/iGotHacksaw", "N")
	if(iGotHacksaw == 0.0) then
		TA_Create("HacksawNPC")
			TA_SetProperty("Position", 69, 26)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Clipping Flag", true)

			for i = 1, 8, 1 do
				for p = 1, 4, 1 do
					TA_SetProperty("Move Frame", i-1, p-1, "Root/Images/Sprites/Hacksaw/Hacksaw")
				end
			end
		DL_PopActiveObject()
	end

	--Spawn Claudia if she hasn't been freed yet.
	local iSavedClaudia = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedClaudia", "N")
	if(iSavedClaudia == 0.0) then
        fnStandardNPCByPosition("Claudia")
	
	--If Claudia was saved, remove these doors. She sawed through them.
	else
		AL_SetProperty("Open Door", "FusedDoorN")
		AL_SetProperty("Open Door", "FusedDoorS")
	end
    
    --Hide these lays.
    AL_SetProperty("Set Layer Disabled", "SlimeFrame0", true)
    AL_SetProperty("Set Layer Disabled", "SlimeFrame1", true)
    AL_SetProperty("Set Layer Disabled", "SlimeFrame2", true)
    AL_SetProperty("Set Layer Disabled", "SlimeFrame3", true)
    AL_SetProperty("Set Layer Disabled", "SlimeFrame4", true)
    AL_SetProperty("Set Layer Disabled", "SlimeFrame5", true)
end
