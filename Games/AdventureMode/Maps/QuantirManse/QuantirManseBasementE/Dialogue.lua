-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Meryl, The Second Best Slime]|
    if(sActorName == "Meryl") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Meryl:[VOICE|Slime] I'm heading back to the surface.[P] I thought there might be some nice fish in here, but it's super freakin' cold!") ]])
    
    -- |[Claudia]|
    elseif(sActorName == "Claudia") then
        
        --Variables.
        local sMeiForm                = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
        local iHasGravemarkerForm     = VM_GetVar("Root/Variables/Global/Mei/iHasGravemarkerForm", "N")
        local iMetClaudia             = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetClaudia", "N")
        local iHasHacksaw             = AdInv_GetProperty("Item Count", "Hacksaw")
        local iGotHacksaw             = VM_GetVar("Root/Variables/Chapter1/Scenes/iGotHacksaw", "N")
        local iHasMetFlorentina       = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
        local iMeiReadClaudiasJournal = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiReadClaudiasJournal", "N")
        local iMeiKnowsRilmani        = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiKnowsRilmani", "N")
        local iSavedClaudia           = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedClaudia", "N")
        local iClaudiaSawGravemarker  = VM_GetVar("Root/Variables/Chapter1/Scenes/iClaudiaSawGravemarker", "N")
        
        -- |[Achievement]|
        AM_SetPropertyJournal("Unlock Achievement", "FindClaudia")
        
        -- |[ ================= Gravemarker Mei ================== ]|
        --Gravemarker Mei changes all the scenes.
        if(iHasGravemarkerForm == 1.0) then
    
            -- |[Flag]|
            VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiHasSubmittedGravemarker", "N", 1.0)
    
            -- |[Setup]|
            --The first time Mei meets Claudia, she has a bit of extra dialogue.
            if(iClaudiaSawGravemarker == 0.0) then
                VM_SetVar("Root/Variables/Chapter1/Scenes/iClaudiaSawGravemarker", "N", 1.0)
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ Append("Mei:[VOICE|Mei] (S-something...[P] something is in my head...)[B][C]") ]])
                fnCutscene([[ Append("Mei:[VOICE|Mei] (Ungghhh...)") ]])
                fnCutsceneBlocker()
            end
    
            --Mei transforms into a Gravemarker.
            fnCutsceneWait(30)
            fnCutsceneBlocker()
            Cutscene_CreateEvent("Flash Mei White", "Actor")
                ActorEvent_SetProperty("Subject Name", "Mei")
                ActorEvent_SetProperty("Flashwhite Quickly")
            DL_PopActiveObject()
            fnCutsceneWait(5)
            fnCutsceneBlocker()
            fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Gravemarker.lua") ]])
            fnCutsceneWait(gci_Flashwhite_Ticks_Total)
            fnCutsceneBlocker()
            fnCutsceneWait(30)
            fnCutsceneBlocker()
    
            -- |[First Time Meeting Claudia]|
            --Hasn't met Claudia yet.
            if(iMetClaudia == 0.0) then
            
                --Flag.
                VM_SetVar("Root/Variables/Chapter1/Scenes/iMetClaudia", "N", 1.0)
    
                --If Mei has the hacksaw:
                if(iHasHacksaw == 1.0) then
                    
                    --Remove the hacksaw.
                    AdInv_SetProperty("Remove Item", "Hacksaw")
                    VM_SetVar("Root/Variables/Chapter1/Scenes/iSavedClaudia", "N", 1.0)
    
                    fnStandardMajorDialogue()
                    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Claudia", "Neutral") ]])
                    fnCutscene([[ Append("Claudia: And the clouds parted at her behest, for she had proven her penitence.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Tool, give me that hacksaw.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Yes, sister.[SOUND|World|TakeItem] [B][C]") ]])
                    fnCutscene([[ Append("Claudia: Curious.[P] You have retained your faculties.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Tool, tell me how this has come to pass?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] A runestone of unknown providence.[P] It allows this one to change its body to any form it has been in before.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] This one escaped the ritual before removing its head.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Interesting.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Tool, why did you resist?[P] Why do you bear this runestone?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] This one has desire to return home.[P] This one do not know why it has the stone.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: A tool has no desires.[P] Return to where you came from and remove your head.[P] Become pure.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Yes, sister.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Sad] Nggghh...[P] No.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Hmm.[P] Tool, I gave you an order.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] This one will not follow it.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Tool, what other orders will you refuse to follow?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] This one does not...[P] know...[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Angry] St-[P]stop commanding me![B][C]") ]])
                    fnCutscene([[ Append("Claudia: Tool, be at peace.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Blush] Nghhh...[P] it has become calm...[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Blush] (It feels so good to obey...)[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Tool, you will resolve this difficulty.[P] Fulfill whatever destiny has been ordained upon you.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: The divine has given you this for a reason.[P] It is not our place to question such providence.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Yes, sister.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Tell me of your home.[P] May I aid your divine purpose?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] A place called Earth.[P] It is on another world.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: ...[P] Hmmm, another world.[P] You are certain?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] This one is certain.[P] It is not from Pandemonium.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: The runestone...[P] yes.[P] A rilmani symbol.[P] The pieces fall into place.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: It is only fitting that one of the promised joins our flock.[P] The others will rejoice when I tell them of this.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Tool, you must seek the rilmani translation in my journal.[P] There are rilmani artifacts in this world that will lead you to them.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: I cannot aid you in the search past that.[P] My journal may give you a suggestion.[P] Find it, find the rilmani.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Yes, sister.[P] Your will be done.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: And, thank you for rescuing me.[P] You are hereby rewarded.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Surprise] (Nnnngggghhhh!!!)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Blush] (This one -[P] came![P] It came![P] It loves to obey![P] It loves to obey the light!)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Thank you, sister.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Tool, you are dismissed.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Yes, sister.") ]])
                    fnCutsceneBlocker()
                
                --Mei does not have the hacksaw.
                else
                    fnStandardMajorDialogue()
                    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Claudia", "Neutral") ]])
                    fnCutscene([[ Append("Claudia: And the clouds parted at her behest, for she had proven her penitence.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Tool, there is a hacksaw in the next room. You did not destroy the gate to get in here.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Return there and come back with the hacksaw. I require it.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Yes, sister.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Now to the more pertinent question.[P] You have retained your faculties, and did not destroy the gates.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Tool, tell me how this has come to pass?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] A runestone of unknown providence.[P] It allows this one to change its body to any form it has been in before.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] This one escaped the ritual before removing its head.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Interesting.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Tool, why did you resist?[P] Why do you bear this runestone?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] This one has desire to return home.[P] This one do not know why it has the stone.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: A tool has no desires.[P] Return to where you came from and remove your head.[P] Become pure.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Yes, sister.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Sad] Nggghh...[P] No.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Hmm.[P] Tool, I gave you an order.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] This one will not follow it.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Tool, what other orders will you refuse to follow?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] This one does not...[P] know...[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Angry] St-[P]stop commanding me![B][C]") ]])
                    fnCutscene([[ Append("Claudia: Tool, be at peace.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Blush] Nghhh...[P] it has become calm...[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Blush] (It feels so good to obey...)[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Tool, you will resolve this difficulty.[P] Fulfill whatever destiny has been ordained upon you.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: The divine has given you this for a reason.[P] It is not our place to question such providence.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Yes, sister.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Tell me of your home.[P] May I aid your divine purpose?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] A place called Earth.[P] It is on another world.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: ...[P] Hmmm, another world.[P] You are certain?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] This one is certain.[P] It is not from Pandemonium.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: The runestone...[P] yes.[P] A rilmani symbol.[P] The pieces fall into place.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: It is only fitting that one of the promised joins our flock.[P] The others will rejoice when I tell them of this.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Tool, you must seek the rilmani translation in my journal.[P] There are rilmani artifacts in this world that will lead you to them.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: I cannot aid you in the search past that.[P] My journal may give you a suggestion.[P] Find it, find the rilmani.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Yes, sister.[P] Your will be done.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Now, retrieve that hacksaw and return it to me.[P] You are dismissed.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Yes, sister.") ]])
                    fnCutsceneBlocker()
                end
            
            -- |[Repeats]|
            --Not the first time meeting Claudia.
            else
            
                --Common:
                fnStandardMajorDialogue()
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Claudia", "Neutral") ]])
            
                -- |[Really Exotic Case]|
                --If Mei met Claudia, left, turned into a Gravemarker, and returned:
                if(iClaudiaSawGravemarker == 0.0) then
                    fnCutscene([[ Append("Claudia: Interesting.[P] Mei, you have become one of our tools.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Offended] Is that what you call them?[P] Are they just tools to you?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Yes.[P] I suppose there is no further need to hide my divine nature.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: I am Celestine Claudia Romanus, second aide to the ninth herald's discoveries and prophecies agency.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: And you are now a tool.[P] Submit to me, tool.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Offended] I'm not some object for you to manipulate![B][C]") ]])
                    fnCutscene([[ Append("Claudia: Your body disagrees.[P] Which is the stronger?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Sad] Huh?[P] What?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Sad] Voices...[P] the light?[P] No, no![P] Get away![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Cry] Nnnnggghhhh...[P] Impure...[P] the runestone...[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Cry] I...[P] This one...[P] This one will...[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Follow orders.[P] This one will follow orders, sister.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Very good.[P] I did not want to pull rank on you like that, but defiance cannot be tolerated.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] There is one exception.[P] This one's quest to return to Earth is not to be compromised.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: And you dare to give me orders?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] This one is not making the demand.[P] This one's runestone is.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: I doubt I would win a battle of wills against a divine artifact.[P] But why would a divine artifact contravene the will of the Chorus?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Something is amiss, or my information is incorrect.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] This one has no more information.[P] It is awaiting orders.[B][C]") ]])
            
                    --Mei didn't save Claudia, doesn't have the hacksaw.
                    if(iSavedClaudia == 0.0 and iHasHacksaw == 0.0) then
                        fnCutscene([[ Append("Claudia: Very well.[P] Tool.[P] Retrieve the hacksaw and return it to me.[P] You are dismissed.[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] Yes, sister.[P] This one will obey.") ]])
                        fnCutsceneBlocker()
                        
                    --Mei didn't save Claudia, but has the hacksaw now:
                elseif(iSavedClaudia == 0.0 and iHasHacksaw == 1.0) then
                
                        --Remove the hacksaw.
                        AdInv_SetProperty("Remove Item", "Hacksaw")
                        VM_SetVar("Root/Variables/Chapter1/Scenes/iSavedClaudia", "N", 1.0)
                        
                        --Dialogue.
                        fnCutscene([[ Append("Claudia: Very well.[P] As you have done what I needed, I see no reason to discipline you.[P] Tool.[P] Give me the hacksaw.[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] Yes, sister.[SOUND|World|TakeItem] [B][C]") ]])
                        fnCutscene([[ Append("Claudia: Good.[P] You will be rewarded.[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Surprise] (Nnnngggghhhh!!!)[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Blush] (This one -[P] came![P] It came![P] It loves to obey![P] It loves to obey the light!)[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] Thank you, sister.[B][C]") ]])
                        fnCutscene([[ Append("Claudia: I must attend to the victims of this prison.[P] I know not their sins, if any, but their last rites shall be seen to.[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] Can this tool help?[B][C]") ]])
                        fnCutscene([[ Append("Claudia: No.[P] Return to your quest.[P] Make no mention of what took place here to any save the other faithful.[P] You are dismissed.[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] Yes, sister.") ]])
                        fnCutsceneBlocker()
                        
                    --Future dialogues, shouldn't be nominally possible to see this.
                    else
                        fnCutscene([[ Append("Claudia: Very well.[P] Tool, there is no further need to converse.[B][C]") ]])
                        fnCutscene([[ Append("Claudia: You will return to your ordained mission.[P] You will render aid as a tool of the Chorus to any faithful you find.[B][C]") ]])
                        fnCutscene([[ Append("Claudia: When your mission is completed, you will return to us and join the Chorus.[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] Yes, sister.[B][C]") ]])
                        fnCutscene([[ Append("Claudia: Tool, you are dismissed.[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] Yes, sister.") ]])
                        fnCutsceneBlocker()
                    end
            
                -- |[Normal Cases]|
                else
                    --Mei didn't save Claudia, doesn't have the hacksaw.
                    if(iSavedClaudia == 0.0 and iHasHacksaw == 0.0) then
                        fnCutscene([[ Append("Claudia: Tool.[P] I gave you an order.[P] Retrieve the hacksaw and return it to me.[P] You are dismissed.[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Offended] (This one -[P] don't have to.[P] This one doesn't have to if it doesn't want to...)[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Blush] (But it feels so good to obey her.[P] Obey the light.[P] Obey the light...)[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] Yes, sister.[P] This one will obey.") ]])
                        fnCutsceneBlocker()
                
                    --Mei didn't save Claudia, but has the hacksaw now:
                    elseif(iSavedClaudia == 0.0 and iHasHacksaw == 1.0) then
                
                        --Remove the hacksaw.
                        AdInv_SetProperty("Remove Item", "Hacksaw")
                        VM_SetVar("Root/Variables/Chapter1/Scenes/iSavedClaudia", "N", 1.0)
                        
                        --Dialogue.
                        fnCutscene([[ Append("Claudia: Tool.[P] Give me the hacksaw.[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] Yes, sister.[SOUND|World|TakeItem] [B][C]") ]])
                        fnCutscene([[ Append("Claudia: Good.[P] You will be rewarded.[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Surprise] (Nnnngggghhhh!!!)[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Blush] (This one -[P] came![P] It came![P] It loves to obey![P] It loves to obey the light!)[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] Thank you, sister.[B][C]") ]])
                        fnCutscene([[ Append("Claudia: I must attend to the victims of this prison.[P] I know not their sins, if any, but their last rites shall be seen to.[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] Can this tool help?[B][C]") ]])
                        fnCutscene([[ Append("Claudia: No.[P] Return to your quest.[P] Make no mention of what took place here to any save the other faithful.[P] You are dismissed.[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] Yes, sister.") ]])
                        fnCutsceneBlocker()
                
                    --Future dialogues:
                    else
                        fnCutscene([[ Append("Claudia: Tool, there is no further need to converse. You are dismissed.[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] Yes, sister.") ]])
                
                    end
                end
            
            end
        
            -- |[Return to Original Form]|
            --Once the scene is over, transform back into whatever form Mei started in.
            fnCutsceneWait(30)
            fnCutsceneBlocker()
            Cutscene_CreateEvent("Flash Mei White", "Actor")
                ActorEvent_SetProperty("Subject Name", "Mei")
                ActorEvent_SetProperty("Flashwhite Quickly")
            DL_PopActiveObject()
            fnCutsceneWait(5)
            fnCutsceneBlocker()
            if(sMeiForm == "Slime") then
                fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Slime.lua") ]])
            else
                fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Ghost.lua") ]])
            end
            fnCutsceneWait(gci_Flashwhite_Ticks_Total)
            fnCutsceneBlocker()
            fnCutsceneWait(30)
            fnCutsceneBlocker()
            
            return
        end
            
        -- |[ ============== Non-Gravemarker Cases =============== ]|
        --This is always a major dialogue.
        fnStandardMajorDialogue()
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Claudia", "Neutral") ]])
        
        --Hasn't met Claudia yet.
        if(iMetClaudia == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter1/Scenes/iMetClaudia", "N", 1.0)
        
            --If Mei has the hacksaw:
            if(iHasHacksaw == 1.0) then
                
                --If Mei hasn't met Florentina, she doesn't know who Claudia is or why she's looking for her:
                if(iHasMetFlorentina == 0.0) then
                    
                    --Standard.
                    fnCutscene([[ Append("Claudia: And the clouds parted at her behest, for she had proven her penitence.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Her deliverer bore a tool.[P] The faithful were rewarded for their piety.[B][C]") ]])
                    
                    --Mei is a slime:
                    if(sMeiForm == "Slime") then
                        fnCutscene([[ Append("Mei:[E|Neutral] Uhhhh....[B][C]") ]])
                        fnCutscene([[ Append("Claudia: ..![B][C]") ]])
                        fnCutscene([[ Append("Claudia: You can speak, slime?[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] You can call me Mei, if you like.[B][C]") ]])
                        fnCutscene([[ Append("Claudia: Apologies![P] I've been alone here for -[P] I'm not sure how long.[P] My only company has been these bones.[B][C]") ]])
                        fnCutscene([[ Append("Claudia: Surely you are no mean slime.[P] I've no doubt the Host has sent you to guide me back to my path.[B][C]") ]])
                    
                    --Mei is a ghost:
                    else
                        fnCutscene([[ Append("Mei:[E|Neutral] Uhhhh....[B][C]") ]])
                        fnCutscene([[ Append("Claudia: Oh, most interesting.[P] Your comrades were not talkative.[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] Comrades?[P] Oh, the other ghosts.[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] I'm not like them.[P] I remember who I am.[B][C]") ]])
                        fnCutscene([[ Append("Claudia: And who is that?[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] Call me Mei.[B][C]") ]])
                        fnCutscene([[ Append("Claudia: Well, Mei, doubtless you have seen what happened to my convent.[P] They, unfortunately, are not so aware of themselves.[B][C]") ]])
                    end
                    
                    fnCutscene([[ Append("Claudia: Are you here to deliver me?[P] Within your form I see you have an instrument I could use.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Offended] Maybe you can answer a few questions first.[P] Namely, what are you doing in this moldy jail cell?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Jail cell?[P] My child, this is no jail cell.[P] It is a cruel stage where the actors suffer and die for the audience.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Do you see the indent on the roof there?[P] There is a mechanism there that opens.[P] People fall in here, but they cannot leave by any means.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Here they starve to death in obscurity.[P] Their means of deliverance cruelly just without their reach...[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] That'd explain the bag of tools.[P] Just who are you?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: I am Claudia Romanus, leader of the Heavenly Doves.[P] Or former leader, I should say...[B][C]") ]])
                    
                    if(iMeiReadClaudiasJournal == 1.0) then
                        fnCutscene([[ Append("Mei:[E|Neutral] I saw your journals upstairs.[P] How'd you wind up here?[B][C]") ]])
                    else
                        fnCutscene([[ Append("Mei:[E|Neutral] Never heard of you, but I guess that's not surprising.[P] I haven't been here very long.[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] How'd you wind up in here?[B][C]") ]])
                    end
                    fnCutscene([[ Append("Claudia: My convent and I were researching the unusual partirhuman specimens in the Quantir Estate, though we could not find a trace of them.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: We were about to give up and move on when they came vengefully from every direction at once.[P] We split up, and I must have wandered into a trap.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: I fell into this room.[P] My convent has not come looking...[P] I fear they are now residents of the Estate.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Offended] Man, everyone keeps getting locked up around here.[P] This place sucks.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Hey, you wouldn't happen to know how I can get back home, would you?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Well I -[P] that runestone...[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] This?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: That symbol is of Rilmani origin![P] You are -[P] you are not of Pandemonium, are you?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: A joyous occasion![P] One of the promised has come and finds me![P] I am blessed![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Offended] Kind of an odd attitude for someone who was going to starve to death.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Starve?[P] Certainly not.[P] My faith -[P] I am unyielding.[P] I could endure a thousand years in this cage if it proved my loyalty.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Yeah, sure you could.[P] So -[P] the Rilmani can help me get home?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: They are arbiters of the dimensional pact.[P] If you are here, they must be involved, or at least aware of it.[P] Find one.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: I regret that I cannot help you further.[P] My translation notes are still in the Quantir Estate.[P] Without them I cannot help you locate a Rilmani.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] But if I can find those notes?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: There are no 'ifs' in the Divine's plans.[P] You will find them and you will be delivered to the Rilmani as surely as water rolls downhill.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Lovely.[P] Guess I'll go find those notes then.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Perhaps you would see fit to loan me your saw?[P] If the Host has found my atonement sufficient, I will return to my mission.[BLOCK]") ]])

                    --Decision script is this script. It must be surrounded by quotes.
                    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
                    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
                    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Here you go\", " .. sDecisionScript .. ", \"Loan\") ")
                    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Nope\",  " .. sDecisionScript .. ", \"Nope\") ")
                    fnCutsceneBlocker()
        
                    --Topic.
                    WD_SetProperty("Unlock Topic", "Rilmani", 1)
                    
                --Otherwise, Mei knows this is her girl:
                else
                    
                    --Standard:
                    fnCutscene([[ Append("Claudia: And the clouds parted at her behest, for she had proven her penitence.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Her deliverer bore a tool.[P] The faithful were rewarded for their piety.[B][C]") ]])
                    
                    --Mei is a slime:
                    if(sMeiForm == "Slime") then
                        fnCutscene([[ Append("Mei:[E|Neutral] Uhhhh....[B][C]") ]])
                        fnCutscene([[ Append("Claudia: ..![B][C]") ]])
                        fnCutscene([[ Append("Claudia: You can speak, slime?[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] You can call me Mei, if you like.[B][C]") ]])
                        fnCutscene([[ Append("Claudia: Apologies![P] I've been alone here for -[P] I'm not sure how long.[P] My only company has been these bones.[B][C]") ]])
                        fnCutscene([[ Append("Claudia: Surely you are no mean slime.[P] I've no doubt the Host has sent you to guide me back to my path.[B][C]") ]])
                    
                    --Mei is a ghost:
                    else
                        fnCutscene([[ Append("Mei:[E|Neutral] Uhhhh....[B][C]") ]])
                        fnCutscene([[ Append("Claudia: Oh, most interesting.[P] Your comrades were not talkative.[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] Comrades?[P] Oh, the other ghosts.[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] I'm not like them.[P] I remember who I am.[B][C]") ]])
                        fnCutscene([[ Append("Claudia: And who is that?[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] Call me Mei.[B][C]") ]])
                        fnCutscene([[ Append("Claudia: Well, Mei, doubtless you have seen what happened to my convent.[P] They, unfortunately, are not so aware of themselves.[B][C]") ]])
                    end
                    
                    fnCutscene([[ Append("Claudia: Are you here to deliver me?[P] Within your form I see you have an instrument I could use.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Offended] Maybe you can answer a few questions first.[P] Namely, what are you doing in this moldy jail cell?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Jail cell?[P] My child, this is no jail cell.[P] It is a cruel stage where the actors suffer and die for the audience.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Do you see the indent on the roof there?[P] There is a mechanism there that opens.[P] People fall in here, but they cannot leave by any means.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Here they starve to death in obscurity.[P] Their means of deliverance cruelly just without their reach...[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] That'd explain the bag of tools.[P] Just who are you?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: I am Claudia Romanus, leader of the Heavenly Doves.[P] Or former leader, I should say...[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Happy] Claudia![P] You're the one I've been looking for![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Happy] Florentina said you'd know how to get back to Earth![B][C]") ]])
                    fnCutscene([[ Append("Claudia: The name Earth is foreign to me, I am afraid.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Sad] ...[P][CLEAR]") ]])
                    fnCutscene([[ Append("Mei:[E|Cry] B-[P]but - [P][CLEAR]") ]])
                    fnCutscene([[ Append("Claudia: Please, child, do not lose hope.[P] As surely as you have found me by the Divine's will, you have the means of your own deliverance.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: The runestone that hovers in your form...[P] It is a Rilmani artifact.[B][C]") ]])
                    if(iMeiKnowsRilmani == 0.0) then
                        fnCutscene([[ Append("Mei:[E|Neutral] Really?[B][C]") ]])

                        --Clear the flag on Next Move.
                        WD_SetProperty("Clear Topic Read", "NextMove")
                    else
                        fnCutscene([[ Append("Mei:[E|Happy] Yeah![P] That's what your journal said![B][C]") ]])
                    end
                    fnCutscene([[ Append("Claudia: Indeed.[P] Please ignore the doubt and rumours.[P] The Rilmani are very real, and theirs is the duty of upholding the Dimensional Pact.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: If you can find one, I have no doubt they will return you to your home.[B][C]") ]])
                    if(iMeiKnowsRilmani == 0.0) then
                        fnCutscene([[ Append("Mei:[E|Happy] Great![P] How do I find one?[B][C]") ]])
                        fnCutscene([[ Append("Claudia: That, I cannot aid you with.[P] My research notes are still somewhere in the Quantir Estate.[P] If you could find them, they might help you.[B][C]") ]])
                    end
                    fnCutscene([[ Append("Mei:[E|Neutral] Okay![P] Now I know what to do![B][C]") ]])
                    fnCutscene([[ Append("Claudia: The Divine will guide you as it guides all things.[P] Perhaps you may see fit to loan me the hacksaw that is within your body?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: If I have atoned for my failures, I may continue my mission of holy purpose.[BLOCK]") ]])
                    
                    --Decision script is this script. It must be surrounded by quotes.
                    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
                    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
                    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Here you go\", " .. sDecisionScript .. ", \"Loan\") ")
                    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Nope\",  " .. sDecisionScript .. ", \"Nope\") ")
                    fnCutsceneBlocker()
        
                    --Topic.
                    WD_SetProperty("Unlock Topic", "Rilmani", 1)
            
                end
            
            --Mei does not have the hacksaw:
            else
            
                --Flag.
                VM_SetVar("Root/Variables/Chapter1/Scenes/iMetClaudiaWithoutHacksaw", "N", 1.0)
                
                --If Mei hasn't met Florentina, she doesn't know who Claudia is or why she's looking for her:
                if(iHasMetFlorentina == 0.0) then
                    
                    --Standard:
                    fnCutscene([[ Append("Claudia: And the clouds did not part, the sun did not shine.[P] Her faith was insufficient, her piety pathetic.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: She would suffer until true purity had been attained, and no sooner.[B][C]") ]])
                    
                    --Mei is a slime:
                    if(sMeiForm == "Slime") then
                        fnCutscene([[ Append("Mei:[E|Neutral] Uhhhh....[B][C]") ]])
                        fnCutscene([[ Append("Claudia: ..![B][C]") ]])
                        fnCutscene([[ Append("Claudia: You can speak, slime?[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] You can call me Mei, if you like.[B][C]") ]])
                        fnCutscene([[ Append("Claudia: Apologies![P] I've been alone here for -[P] I'm not sure how long.[P] My only company has been these bones.[B][C]") ]])
                        fnCutscene([[ Append("Claudia: Surely you are no mean slime.[P] I've no doubt the Host has sent you to guide me back to my path.[B][C]") ]])
                    
                    --Mei is a ghost:
                    else
                        fnCutscene([[ Append("Mei:[E|Neutral] Uhhhh....[B][C]") ]])
                        fnCutscene([[ Append("Claudia: Oh, most interesting.[P] Your comrades were not talkative.[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] Comrades?[P] Oh, the other ghosts.[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] I'm not like them.[P] I remember who I am.[B][C]") ]])
                        fnCutscene([[ Append("Claudia: And who is that?[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] Call me Mei.[B][C]") ]])
                        fnCutscene([[ Append("Claudia: Well, Mei, doubtless you have seen what happened to my convent.[P] They, unfortunately, are not so aware of themselves.[B][C]") ]])
                    end
                    
                    fnCutscene([[ Append("Claudia: Are you here to deliver me, perchance?[P] I apologize that I did not recognize you for what you were sooner![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Offended] Maybe you can answer a few questions first.[P] Namely, what are you doing in this moldy jail cell?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Jail cell?[P] My child, this is no jail cell.[P] It is a cruel stage where the actors suffer and die for the audience.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Do you see the indent on the roof there?[P] There is a mechanism there that opens.[P] People fall in here, but they cannot leave by any means.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Here they starve to death in obscurity.[P] Their means of deliverance cruelly just without their reach...[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] That'd explain the bag of tools.[P] Just who are you?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: I am Claudia Romanus, leader of the Heavenly Doves.[P] Or former leader, I should say...[B][C]") ]])
                    
                    if(iMeiReadClaudiasJournal == 1.0) then
                        fnCutscene([[ Append("Mei:[E|Neutral] I saw your journals upstairs.[P] How'd you wind up here?[B][C]") ]])
                    else
                        fnCutscene([[ Append("Mei:[E|Offended] Never heard of you, but I guess that's not surprising.[P] I haven't been here very long.[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] How'd you wind up in here?[B][C]") ]])
                    end
                    fnCutscene([[ Append("Claudia: My convent and I were researching the unusual partirhuman specimens in the Quantir Estate, though we could not find a trace of them.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: We were about to give up and move on when they came vengefully from every direction at once.[P] We split up, and I must have wandered into a trap.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: I fell into this room.[P] My convent has not come looking...[P] I fear they are now residents of the Estate.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Offended] Man, everyone keeps getting locked up around here.[P] This place sucks.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Hey, you wouldn't happen to know how I can get back home, would you?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Well I -[P] that runestone...[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] This?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: That symbol is of Rilmani origin![P] You are -[P] you are not of Pandemonium, are you?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: A joyous occasion![P] One of the promised has come and finds me![P] I am blessed![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Offended] Kind of an odd attitude for someone who was going to starve to death.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Starve?[P] Certainly not.[P] My faith -[P] I am unyielding.[P] I could endure a thousand years in this cage if it proved my loyalty.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Yeah, sure you could.[P] So -[P] the Rilmani can help me get home?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: They are arbiters of the dimensional pact.[P] If you are here, they must be involved, or at least aware of it.[P] Find one.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: I regret that I cannot help you further.[P] My translation notes are still in the Quantir Estate.[P] Without them I cannot help you locate a Rilmani.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] But if I can find those notes?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: There are no [P]'ifs'[P] in the Divine's plans.[P] You will find them and you will be delivered to the Rilmani as surely as water rolls downhill.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Lovely.[P] Guess I'll go find those notes then.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Unfortunately, it seems that your nature is your advantage.[P] If you could find a way to help me out of this cage, I would be grateful.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] I'll see what I can do...") ]])
                    
                    --Topic.
                    WD_SetProperty("Unlock Topic", "Rilmani", 1)
                
                --Otherwise, Mei knows this is her girl:
                else
        
                    --Topic.
                    WD_SetProperty("Unlock Topic", "Rilmani", 1)
                    
                    --Standard:
                    fnCutscene([[ Append("Claudia: And the clouds did not part, the sun did not shine.[P] Her faith was insufficient, her piety pathetic.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: She would suffer until true purity had been attained, and no sooner.[B][C]") ]])
                    
                    --Mei is a slime:
                    if(sMeiForm == "Slime") then
                        fnCutscene([[ Append("Mei:[E|Neutral] Uhhhh....[B][C]") ]])
                        fnCutscene([[ Append("Claudia: ..![B][C]") ]])
                        fnCutscene([[ Append("Claudia: You can speak, slime?[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] You can call me Mei, if you like.[B][C]") ]])
                        fnCutscene([[ Append("Claudia: Apologies![P] I've been alone here for -[P] I'm not sure how long.[P] My only company has been these bones.[B][C]") ]])
                        fnCutscene([[ Append("Claudia: Surely you are no mean slime.[P] I've no doubt the Host has sent you to guide me back to my path.[B][C]") ]])
                    
                    --Mei is a ghost:
                    else
                        fnCutscene([[ Append("Mei:[E|Neutral] Uhhhh....[B][C]") ]])
                        fnCutscene([[ Append("Claudia: Oh, most interesting.[P] Your comrades were not talkative.[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] Comrades?[P] Oh, the other ghosts.[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] I'm not like them.[P] I remember who I am.[B][C]") ]])
                        fnCutscene([[ Append("Claudia: And who is that?[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] Call me Mei.[B][C]") ]])
                        fnCutscene([[ Append("Claudia: Well, Mei, doubtless you have seen what happened to my convent.[P] They, unfortunately, are not so aware of themselves.[B][C]") ]])
                    end
                    
                    --Continue:
                    fnCutscene([[ Append("Claudia: Are you here to deliver me, perchance?[P] I apologize that I did not recognize you for what you were sooner![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Offended] Maybe you can answer a few questions first.[P] Namely, what are you doing in this moldy jail cell?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Jail cell?[P] My child, this is no jail cell.[P] It is a cruel stage where the actors suffer and die for the audience.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Do you see the indent on the roof there?[P] There is a mechanism there that opens.[P] People fall in here, but they cannot leave by any means.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Here they starve to death in obscurity.[P] Their means of deliverance cruelly just without their reach...[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] That'd explain the bag of tools.[P] Just who are you?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: I am Claudia Romanus, leader of the Heavenly Doves.[P] Or former leader, I should say...[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Happy] Claudia![P] You're the one I've been looking for![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Happy] Florentina said you'd know how to get back to Earth![B][C]") ]])
                    fnCutscene([[ Append("Claudia: The name Earth is foreign to me, I am afraid.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Sad] ...[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Cry] B-[P]but - [P][CLEAR]") ]])
                    fnCutscene([[ Append("Claudia: Please, child, do not lose hope.[P] As surely as you have found me by the Divine's will, you have the means of your own deliverance.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: The runestone that hovers in your form...[P] It is a Rilmani artifact.[B][C]") ]])
                    if(iMeiKnowsRilmani == 0.0) then
                        fnCutscene([[ Append("Mei:[E|Neutral] Really?[B][C]") ]])

                        --Clear the flag on Next Move.
                        WD_SetProperty("Clear Topic Read", "NextMove")
                    else
                        fnCutscene([[ Append("Mei:[E|Neutral] Yeah![P] That's what your journal said![B][C]") ]])
                    end
                    fnCutscene([[ Append("Claudia: Indeed.[P] Please ignore the doubt and rumours.[P] The Rilmani are very real, and theirs is the duty of upholding the Dimensional Pact.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: If you can find one, I have no doubt they will return you to your home.[B][C]") ]])
                    if(iMeiKnowsRilmani == 0.0) then
                        fnCutscene([[ Append("Mei:[E|Happy] Great![P] How do I find one?[B][C]") ]])
                        fnCutscene([[ Append("Claudia: That, I cannot aid you with.[P] My research notes are still somewhere in the Quantir Estate.[P] If you could find them, they might help you.[B][C]") ]])
                    end
                    fnCutscene([[ Append("Mei:[E|Neutral] Okay![P] Now I know what to do![B][C]") ]])
                    fnCutscene([[ Append("Claudia: Unfortunately, it seems that your nature is your advantage.[P] If you could find a way to help me out of this cage, I would be grateful.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] I'll see what I can do...") ]])
                end
            end
            
            --Common.
            fnCutsceneBlocker()
        
        --Mei has met with Claudia:
        else
        
            --If Mei has the hacksaw on her person/slime.
            if(iHasHacksaw == 1.0) then
                fnCutscene([[ Append("Claudia: Blessed child.[P] Has my moment of redemption come?[BLOCK]") ]])
                    
                --Decision script is this script. It must be surrounded by quotes.
                local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
                fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Here you go\", " .. sDecisionScript .. ", \"Loan\") ")
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Nope\",  " .. sDecisionScript .. ", \"Nope\") ")
                fnCutsceneBlocker()
            
            --Mei doesn't have the hacksaw and didn't pick it up to begin with.
            elseif(iGotHacksaw == 0.0) then
                fnCutscene([[ Append("Claudia: Blessed child.[P] Is there anything else I may do to assist you?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] I might have an idea of how to get you out of here, but I need a minute.[B][C]") ]])
                fnCutscene([[ Append("Claudia: A minute or a decade, all are equal in the infinite sight of the Divine.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Offended] (What a nut...)") ]])
            
            --Mei has freed Claudia.
            else
                fnCutscene([[ Append("Claudia: Blessed child.[P] I must tarry and ease the suffering of those who died here.[P] Please, go on and find your way.") ]])
            end
        end
    end
    
--Give Claudia the hacksaw.
elseif(sTopicName == "Loan") then
	
	--Remove the hacksaw.
	AdInv_SetProperty("Remove Item", "Hacksaw")
	VM_SetVar("Root/Variables/Chapter1/Scenes/iSavedClaudia", "N", 1.0)
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Claudia", "Neutral") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Hold on...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] Here you go, sorry it's a little - you know...[B][C]") ]])
	fnCutscene([[ Append("Claudia: Truly you are a saint.[P] I must remain here a few more moments, though.[P] The dead here are restless, and I must perform their rites.[B][C]") ]])
	fnCutscene([[ Append("Claudia: If our paths cross again, I may yet have a reward for one as kind as you.[P] Please, be well.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] (She's so nice it's kind of sickening...)") ]])
	fnCutsceneBlocker()
	
	--Clear Claudia's topic flag.
	WD_SetProperty("Clear Topic Read", "Claudia")
	
--Don't give Claudia the hacksaw.
elseif(sTopicName == "Nope") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Claudia", "Neutral") ]])
	fnCutscene([[ Append("Mei:[E|Sad] ...[B][C]") ]])
	fnCutscene([[ Append("Claudia: The look on your face is one of guilt.[P] Please do not feel that way.[P] Your choice is not yours, but of the Divine's.[B][C]") ]])
	fnCutscene([[ Append("Claudia: I shall prove my worth by fasting here, with the dead.[B][C]") ]])
	fnCutscene([[ Append("Claudia: If I may aid you further, please return, child.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Surprise] You're not mad?[B][C]") ]])
	fnCutscene([[ Append("Claudia: I have much to atone for with my suffering.[P] When the Divine sees fit, I will be released, and no sooner.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] (Religious people sure are wacko...)") ]])
	fnCutsceneBlocker()
end

