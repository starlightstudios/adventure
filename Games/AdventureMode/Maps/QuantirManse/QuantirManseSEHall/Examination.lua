-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
--Filthy Bed.
if(sObjectName == "Bed") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Looks like a bed used by the serving staff.[P] It's filthy, and it looks like there are traces of old vomit on it...)") ]])
    fnCutsceneBlocker()
	
--Crates
elseif(sObjectName == "Crates") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (There was food in these crates once, but it seems to have rotted to nothing.)") ]])
    fnCutsceneBlocker()
	
--Men's Lavatory
elseif(sObjectName == "MensLavatory") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Men's lavatory.[P] No thanks.)") ]])
    fnCutsceneBlocker()
	
--Women's Lavatory
elseif(sObjectName == "WomensLavatory") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Women's lavatory.[P] Not tempted...)") ]])
    fnCutsceneBlocker()
	
--Baths
elseif(sObjectName == "Baths") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Looks like a public bath, maybe for the serving staff.[P] Seems they were okay with the sexes mixing, as this area is pretty open.)") ]])
    fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
