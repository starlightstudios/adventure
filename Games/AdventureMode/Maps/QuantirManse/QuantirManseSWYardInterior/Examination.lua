-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
--Barrels.
if(sObjectName == "Barrels") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A terrible smell is coming from these barrels.[P] No way am I opening them!)") ]])
    fnCutsceneBlocker()

--Crates.
elseif(sObjectName == "Crates") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (There's something oppressive about the crates here.[P] I'm not opening them!)") ]])
    fnCutsceneBlocker()

--Coffin.
elseif(sObjectName == "Coffin") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A coffin.[P] It seems to be occupied.[P] I'm not a graverobber, so it's staying closed.)") ]])
    fnCutsceneBlocker()

--Coffin Special.
elseif(sObjectName == "CoffinSpecial") then

	--First time disturbing the coffin:
	local iDisturbedCoffin = VM_GetVar("Root/Variables/Chapter1/Scenes/iDisturbedCoffin", "N")
	if(iDisturbedCoffin == 0.0) then
		LM_ExecuteScript(gsItemListing, "Quantir Mansion Key")
		VM_SetVar("Root/Variables/Chapter1/Scenes/iDisturbedCoffin", "N", 1.0)
		fnStandardMajorDialogue()
		fnCutscene([[ Append("[VOICE|Mei](This coffin is open a crack...[P] and I think I can see something in there!)[B][C]") ]])
		fnCutscene([[ Append("[VOICE|Mei](...)[B][C]") ]])
		fnCutscene([[ Append("[VOICE|Mei](Looks like it's a journal page left by the person who was doing the burial.[P] Let's see...)[B][C]") ]])
		fnCutscene([[ Append("[VOICE|Mei]('We've been working so hard lately, but the ranks of the sick and dying continue to swell.[P] Countess Quantir says she will find a cure, but I am not so certain.')[B][C]") ]])
		fnCutscene([[ Append("[VOICE|Mei]('Rumour has it that Countess Quantir was a great magician in her younger days, but fat lot of good that seems to be doing against the epidemic!')[B][C]") ]])
		fnCutscene([[ Append("[VOICE|Mei]('I shouldn't be so harsh.[P] She's trying to find a cure.[P] She says that the disease spreads in filth and among rodents, so we've been bathing the sick and keeping the manor spotless.')[B][C]") ]])
		fnCutscene([[ Append("[VOICE|Mei]('I've been charged with the burials, but I'd be lying if I said it wasn't getting to me.[P] And yet, I don't want to let the Countess down after all she's done for me.')[B][C]") ]])
		fnCutscene([[ Append("[VOICE|Mei](..!)[B][C]") ]])
		fnCutscene([[ Append("[VOICE|Mei](There's a key in here too!)[B][C]") ]])
		fnCutscene([[ Append("[SOUND|World|TakeItem]*Got Quantir Mansion Key*") ]])

	--Further cases.
	else
		fnStandardMajorDialogue()
		fnCutscene([[ Append("[VOICE|Mei](The journal page left by the person doing the burial...)[B][C]") ]])
		fnCutscene([[ Append("[VOICE|Mei]('We've been working so hard lately, but the ranks of the sick and dying continue to swell.[P] Countess Quantir says she will find a cure, but I am not so certain.')[B][C]") ]])
		fnCutscene([[ Append("[VOICE|Mei]('Rumour has it that Countess Quantir was a great magician in her younger days, but fat lot of good that seems to be doing against the epidemic!')[B][C]") ]])
		fnCutscene([[ Append("[VOICE|Mei]('I shouldn't be so harsh.[P] She's trying to find a cure.[P] She says that the disease spreads in filth and among rodents, so we've been bathing the sick and keeping the manor spotless.')[B][C]") ]])
		fnCutscene([[ Append("[VOICE|Mei]('I've been charged with the burials, but I'd be lying if I said it wasn't getting to me.[P] And yet, I don't want to let the Countess down after all she's done for me.')") ]])
	end
	
--Gaardian Cave Moss.
elseif(sObjectName == "GaardianMoss") then
	
	--Pie Job:
	local iMossCount = AdInv_GetProperty("Item Count", "Gaardian Cave Moss")
	local iTakenPieJob = VM_GetVar("Root/Variables/Chapter1/Breanne/iTakenPieJob", "N")
	if(iTakenPieJob == 1.0 and iMossCount < 1) then
		
		--Short scene.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Wait up a second, Mei.[P] This guy is the kind of moss we need.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Really?[P] That's going into the pie?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] It is...[P] less than appetizing...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] You're going to eat it and you're going to like it.[P] Now shut up and grab a handful.[B][C]") ]])
		fnCutscene([[ Append("[SOUND|World|TakeItem]*Got Gaardian Cave Moss*") ]])
		
		--Add the item.
		LM_ExecuteScript(gsItemListing, "Gaardian Cave Moss")
		
		--Reset flag.
		WD_SetProperty("Clear Topic Read", "Pepper Pie")
		
	--Normal:
	else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Moss growing on the edge of the room.[P] It is...[P] oddly appropriate, for some reason.)") ]])
        fnCutsceneBlocker()
	end

--Exit door:
elseif(sObjectName == "ExitDoor") then
	AL_BeginTransitionTo("QuantirManseSWYard", "FORCEPOS:5.0x8.0x0")

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
