-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================== Encounter Ghost Scene ======================= ]|
--Maid Ghost cutscene.
if(sObjectName == "Duties") then
	
    -- |[Repeat Check]|
	--Player has not seen this scene before.
	local iSawDutiesScene  = VM_GetVar("Root/Variables/Chapter1/Scenes/iSawDutiesScene", "N")
	local iDisturbedCoffin = VM_GetVar("Root/Variables/Chapter1/Scenes/iDisturbedCoffin", "N")
	if(iSawDutiesScene ~= 0.0 or iDisturbedCoffin ~= 1.0) then return end
		
    -- |[Variables]|
    --Flag.
    VM_SetVar("Root/Variables/Chapter1/Scenes/iSawDutiesScene", "N", 1.0)
    
    --Variables.
    local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
    local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
    
    -- |[Setup]|
    --Sound effect.
    AudioManager_PlaySound("World|FlipSwitch")
    
    --Spawn a ghost.
    TA_Create("GhostMaid")
        TA_SetProperty("Position", 5, 16)
        TA_SetProperty("Facing", gci_Face_North)
        TA_SetProperty("Clipping Flag", true)
        fnSetCharacterGraphics("Root/Images/Sprites/MaidGhost/", false)
    DL_PopActiveObject()
    
    -- |[Movement]|
    --Wait a bit.
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Ghost walks up to the party.
    fnCutsceneMove("GhostMaid", 5.25, 15.50)
    fnCutsceneBlocker()
    
    --Wait a bit.
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Not-Ghost Scene]|
    --If Mei is not a ghost, this plays out. Under normal circumstances this is the only allowable version, but in NC+
    -- it's possible to be a ghost and see this scene.
    if(sMeiForm ~= "Ghost") then
    
        -- |[Dialogue]|
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        if(bIsFlorentinaPresent == false) then
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Surprise") ]])
        else
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Surprise") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Surprise") ]])
        end
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Ghost", "Neutral") ]])
        fnCutscene([[ Append("Ghost:[VOICE|Ghost][P] I smell...[P] sickness...[B][C]") ]])
        fnCutscene([[ Append("Mei: S-[P]stay back!") ]])
        fnCutsceneBlocker()

        -- |[Movement]|
        --Fade to blue.
        fnCutscene([[ AL_SetProperty("Activate Fade", 30, gci_Fade_Under_GUI, true, 1, 1, 1, 1, 0.50, 0.50, 1.0, 1, true) ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Ghost walks up to the party.
        fnCutsceneMove("GhostMaid", 5.25, 12.50)
        fnCutsceneBlocker()
        
        --Wait a bit.
        fnCutsceneWait(25)
        fnCutsceneBlocker()

        -- |[Dialogue]|
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        if(bIsFlorentinaPresent == false) then
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Surprise") ]])
        else
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Surprise") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Surprise") ]])
        end
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Ghost", "Neutral") ]])
        fnCutscene([[ Append("Ghost:[VOICE|Ghost] You are sick.[P] We can help you.[P] Please, come with me.[B][C]") ]])
        
        --Mei is alone:
        if(bIsFlorentinaPresent == false) then
            fnCutscene([[ Append("Mei:[E|Sad] Sick?[P] I'm not sick...[B][C]") ]])
            fnCutscene([[ Append("Ghost:[VOICE|Ghost] Look at me.[P] Breathe deeply.[P] You are sick.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Sad] ...[B]") ]])

            --Decision script is this script. It must be surrounded by quotes.
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Listen to her.\", " .. sDecisionScript .. ", \"Listen\") ")
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Fight her!\",  " .. sDecisionScript .. ", \"Fight\") ")
            fnCutsceneBlocker()
        
        --Florentina is here:
        else
            fnCutscene([[ Append("Mei:[E|Sad] Sick?[P] I'm not sick...[B][C]") ]])
            fnCutscene([[ Append("Ghost:[VOICE|Ghost] Look at me.[P] Breathe deeply.[P] You are sick.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Sad] ...[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Confused] Mei?[P] Mei![P] Get a hold of yourself![B][C]") ]])
            fnCutscene([[ Append("Ghost:[VOICE|Ghost] You're ill.[P] We can help.[P] Please, listen to me...[B]") ]])

            --Decision script is this script. It must be surrounded by quotes.
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Listen to her.\", " .. sDecisionScript .. ", \"Listen\") ")
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Fight her!\",  " .. sDecisionScript .. ", \"Fight\") ")
            fnCutsceneBlocker()
        end

    -- |[Ghost Version]|
    --If, due to NC+ or debug shenanigans, Mei is already a ghost. Do this:
    else
    
        -- |[Flag]|
        VM_SetVar("Root/Variables/Chapter1/Scenes/iGhostFromNCPlus", "N", 1.0)
    
        -- |[Dialogue]|
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        if(bIsFlorentinaPresent == false) then
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Surprise") ]])
        else
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Surprise") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Surprise") ]])
        end
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Ghost", "Neutral") ]])
        fnCutscene([[ Append("Ghost:[VOICE|Ghost][P] Hmmm...[P] Something is...[B][C]") ]])
        fnCutscene([[ Append("Mei: S-[P]stay back![B][C]") ]])
        fnCutscene([[ Append("Ghost:[VOICE|Ghost] I knew it![P] Natalie, are you slacking off in here!?[P] Ooooh, I ought to get the countess right now!") ]])
        fnCutsceneBlocker()

        -- |[Movement]|
        --Fade to blue.
        fnCutscene([[ AL_SetProperty("Activate Fade", 30, gci_Fade_Under_GUI, true, 1, 1, 1, 1, 0.50, 0.50, 1.0, 1, true) ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Ghost walks up to the party.
        fnCutsceneMove("GhostMaid", 5.25, 12.50)
        fnCutsceneBlocker()
        
        --Wait a bit.
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        -- |[Dialogue]|
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        if(bIsFlorentinaPresent == false) then
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Surprise") ]])
        else
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Surprise") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Surprise") ]])
        end
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Ghost", "Neutral") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] (Natalie?[P] Wait...[P] I know that name, it's my name![P] But from before...)[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] (Ugh...[P] suddenly, a rush of memories...[P] a life I lived before...)[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Offended] (Darn it, I can sort through those memories later!)[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Offended] Really, Betty?[P] Going to rat me out to the countess?[P] And after I let you use my uniform when yours was soiled.[B][C]") ]])
        fnCutscene([[ Append("Ghost:[VOICE|Ghost] Ugh![P] Fine![P] I owe you one.[B][C]") ]])
        if(bIsFlorentinaPresent == true) then
            fnCutscene([[ Append("Florentina:[E|Surprise] Uh, yeah![P] So buzz off![B][C]") ]])
            fnCutscene([[ Append("Ghost:[VOICE|Ghost] Did you hear something?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] *Let me handle this, Florentina.*[B][C]") ]])
        end
        fnCutscene([[ Append("Mei:[E|Offended] Look, I'm not slacking, I promise.[P] I was just looking for a key I dropped, and look, I found it![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] See?[P] Now can we at least act like friends?[B][C]") ]])
        fnCutscene([[ Append("Ghost:[VOICE|Ghost] I-[P]I'm sorry.[P] Jeez, I need a break.[P] I haven't slept in... I can't remember the last time I even had a nap...[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] I forgive you.[P] Really![P] It'll be okay.[P] I'm going to go get the laundry done, all right?[B][C]") ]])
        fnCutscene([[ Append("Ghost:[VOICE|Ghost] Okay.[P] But don't let anyone else catch you wandering around because I can't guarantee they'll be as nice as I am![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] See you later, Betty.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
		
        -- |[Movement]|
        --Ghost walks to the door.
        fnCutsceneMove("GhostMaid", 5.25, 16.50)
        fnCutsceneBlocker()
        
        --Play a sound and teleport the ghost away.
        fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
        fnCutsceneTeleport("GhostMaid", -100.25, -100.50)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()

        -- |[Dialogue]|
        if(bIsFlorentinaPresent == true) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Happy") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] That was some solid bluff-work![B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Confused] Now spill the beans, see-through.[P] What do you know about this place?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] I'll try to be quick.[P] It's not exactly a happy story, but...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] I'm dead, but I was possessed by the ghost of a maid who worked here, Natalie.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] We share spirits now, I think.[P] All the other maids here that have a more physical body are other humans who were likewise possessed.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] But, due to a powerful curse, they think they're still alive, and are cleaning the manor and treating sick people from a plague.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] The plague that killed them, I take it?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Sad] Yeah...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] My memories from that other life are still a little fuzzy, but they see me as Natalie.[P] And they see you as a houseplant.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] A houseplant who is going to sucker-punch them?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] If it gives you the advantage of surprise...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] But the countess was a real taskmaster, so they aren't going to just let us wander around.[P] They'll think I'm slacking when there's work to be done.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] Fine with me, I didn't want to be bored.[P] Lead the way, [P]'Natalie'.") ]])
            fnCutsceneBlocker()
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
            fnCutscene([[ Append("Mei:[E|Sad] (That was close. I swear I've been here before, having this same conversation before.)[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] (It must be that Natalie's memories and my memories are getting mixed up.[P] I can sort them out later.[P] Better keep moving.)") ]])
            fnCutsceneBlocker()
        end
        
        --Fold.
        fnAutoFoldParty()
        fnCutsceneBlocker()
    end

-- |[ ====================== Voluntary Ghost TF Case ======================= ]|
--Mei decides to listen to the ghost.
elseif(sObjectName == "Listen") then
	
	--Variables.
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
		
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
        
    --Load images.
    fnLoadDelayedBitmapsFromList("Chapter 1 Mei Ghost TF", gciDelayedLoadLoadAtEndOfTick)
	
	--Darken the screen.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	if(bIsFlorentinaPresent == false) then
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Offended") ]])
	else
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Offended") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Confused") ]])
	end
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Ghost", "Neutral") ]])
	
	--Text.
	fnCutscene([[ Append("Mei:[E|Offended] [P]Can...[P] you...[P] help me?[B][C]") ]])
	fnCutscene([[ Append("Ghost:[VOICE|Ghost] Yes.[P] Follow me.[B][C]") ]])
	
	--Mei is alone:
	if(bIsFlorentinaPresent == false) then
		fnCutscene([[ Append("Mei:[E|Sad] My skin...[P] It looks wrong...[B][C]") ]])
		fnCutscene([[ Append("Ghost:[VOICE|Ghost] It is an effect of the plague.[P] We must wash you immediately.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] Please do...[P] I feel...[P] faint...[B][C]") ]])
		fnCutsceneBlocker()
		
	--Florentina is present:
	else
		fnCutscene([[ Append("Florentina:[E|Confused] What are you doing, Mei?[P] Hey![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] Can you hear me?[P] Mei![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] Did you hear something...?[B][C]") ]])
		fnCutscene([[ Append("Ghost:[VOICE|Ghost] No.[P] We are alone.[B][C]") ]])
		fnCutscene([[ Append("Ghost:[VOICE|Ghost] You're running a fever.[P] You must be hearing things.[P] Come quickly.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] My skin...[P] It looks wrong...[B][C]") ]])
		fnCutscene([[ Append("Ghost:[VOICE|Ghost] It is an effect of the plague.[P] We must wash you immediately.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] Mei![P] Don't -[P][CLEAR]") ]])
		fnCutscene([[ Append("[P][SOUND|World|Thump][P][CLEAR]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] ...") ]])
		fnCutsceneBlocker()
	end
		
	--Wait a bit.
	fnCutsceneWait(120)
	fnCutsceneBlocker()

    --Clear censor bars.
    WD_SetProperty("Clear Censor Bars")
	
	--Scene.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
	fnCutscene([[ Append("The maid led Mei through the courtyard and back into the main building.[P] A chill ran up and down her body, and her teeth chattered as they walked.[P] Her head spun and the world became a blur.[B][C]") ]])
	fnCutscene([[ Append("She saw other maids as they went.[P] They gave her a passing glance or a curt nod, scarcely covering their disinterest in her.[P] Doubtless they had seen many more sick like her.[B][C]") ]])
	fnCutscene([[ Append("The maid led her to the baths.[P] Without needing to be told, she disrobed and stepped into the waters.[B][C]") ]])
	fnCutscene([[ Append("Her chill seemed to abate.[P] The water was warm and had a flowery aroma.[P] The maid poured some salts into the bath as Mei began to clean herself.[B][C]") ]])
	fnCutscene([[ Append("[P]She floated for a time.[P] Her hostess said nary a word.[P] She closed her eyes and enjoyed the bath, and the feeling of cleanliness.[P] She hated being sick.[P] Why had fate done this to her?[B][C]") ]])
	fnCutscene([[ Append("After a few moments, the maid gently prodded her.[P] She knew she needed to leave the bath, but her clothes had gone.[P] She dried herself with a nearby towel and wondered what had become of them.[B][C]") ]])
	fnCutscene([[ Append("She gave an inquiring look to the maid.[P] [VOICE|Ghost]'Your clothes will need to be cleaned, I sent them to your room.[P] Please, wear these instead.'[B][C]") ]])
	fnCutscene([[ Append("The maid produced a uniform much like her own.[P] [VOICE|Ghost]'I'm sorry, but we have no clothes in your size.[P] This is all that is available.'") ]])
	fnCutsceneBlocker()
	
	--Restart.
	fnCutscene([[ WD_SetProperty("FastShow") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/GhostTF0") ]])
	fnCutscene([[ Append("The maid uniform was the most beautiful dress she had ever seen.[P] She loved the frilly lace and ribbons that adorned it, and the fabric was finely made and soft on her skin.[P] She pulled it over her and felt a weight lift from her heart.[B][C]") ]])
	fnCutscene([[ Append("She felt a draft in the room.[P] Something seemed to be blowing over and around her, but the lovely dress absorbed her full attention.[P] She scarcely noted something hard circling around her feet, clanking as it went...") ]])
	fnCutsceneBlocker()
	
	--Restart.
	fnCutscene([[ WD_SetProperty("FastShow") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/GhostTF0") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/GhostTF1") ]])
	fnCutscene([[ Append("Something slammed shut around her foot, as another cold, hard feeling began snaking up her leg.[P] She looked down in horror to see a chain climbing her leg -[P] no, her leg -[P] her leg had become a vapour![B][C]") ]])
	fnCutscene([[ Append("A horrible realization gripped Mei -[P] none of this was real![P] The bath, the maid -[P] the maid was a ghost![P] She had tricked her and now -[P][CLEAR]") ]])
	fnCutscene([[ Append("[P]Calm.[P] No need to panic.[P] Everything was fine.") ]])
	fnCutsceneBlocker()
	
	--Restart.
	fnCutscene([[ WD_SetProperty("FastShow") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/GhostTF1") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Ghost") ]])
	fnCutscene([[ Append("The strange feeling, of something cold blowing, had come again.[P] Natalie was unperturbed.[P] Bath time was over, and she had to resume her duties.[B][C]") ]])
	fnCutscene([[ Append("She thought, perhaps, she had fallen asleep in the bath.[P] It happened fairly often.[P] The water was warm and comfortable, she certainly wouldn't have been the first to do so.[P] But her dream had been so vivid.[B][C]") ]])
	fnCutscene([[ Append("A charming, pretty girl, from Hong Kong, fighting to find her way home.[P] It seemed so unreal, but then, dreams always did.[B][C]") ]])
	fnCutscene([[ Append("She had been assigned to clean the guest room.[P] She made her way there, thinking about Mei and her fantastical journey...") ]])
	fnCutsceneBlocker()
        
    --Unload.
    fnCutsceneWait(gciDialogue_Fadeout_Then_Unload_Ticks)
    fnCutsceneBlocker()
    fnCutscene([[ fnUnloadBitmapsFromList("Chapter 1 Mei Ghost TF") ]])
	
	--Transfer:
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	fnCutscene([[ AL_BeginTransitionTo("QuantirManseCentralW", gsRoot .. "Chapter 1/Scenes/200 Defeat/Defeat_Ghost/Scene_Alternate.lua") ]])
	fnCutsceneBlocker()

-- |[ ======================= No Transformation Case ======================= ]|
--Mei decides to fight the ghost:
elseif(sObjectName == "Fight") then
	
	--Variables.
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
		
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	if(bIsFlorentinaPresent == false) then
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Offended") ]])
	else
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Offended") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Confused") ]])
	end
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Ghost", "Neutral") ]])
	
	--Text.
	fnCutscene([[ Append("Mei:[E|Offended] [P][SOUND|World|Thump]Back off![B][C]") ]])
	fnCutscene([[ Append("Ghost:[VOICE|Ghost] Ngh![P] I'm trying to help you...[B][C]") ]])
	fnCutscene([[ Append("Ghost:[VOICE|Ghost] The sickness must be eradicated...[P] We will help you...[P] You will be cured...") ]])
	fnCutsceneBlocker()
		
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
		
	--Ghost walks to the door.
	fnCutsceneMove("GhostMaid", 5.25, 16.50)
	fnCutsceneBlocker()
	
	--Play a sound and teleport the ghost away.
	fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
	fnCutsceneTeleport("GhostMaid", -100.25, -100.50)
	fnCutsceneBlocker()
		
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Sad") ]])
	if(bIsFlorentinaPresent == true) then
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Confused") ]])
	end
	
	--Talking.
	if(bIsFlorentinaPresent == false) then
		fnCutscene([[ Append("Mei:[E|Sad] What on Earth was she talking about... I'm not sick... Am I?") ]])
		fnCutsceneBlocker()
	
	--With Florentina:
	else
		fnCutscene([[ Append("Mei:[E|Sad] What on Earth was she talking about...[P] I'm not sick...[P] Am I?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] Of course this old place would be haunted.[P] Just my luck.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] Stay strong, Mei.[P] They might not be keen on letting us leave.") ]])
		fnCutsceneBlocker()
	end
end
