-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Cutscene Triggers]|
--Lydie leaves the party.
if(sObjectName == "GhostTrigger") then
	
	--Variables.
	local iIsGhostTF      = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsGhostTF", "N")
	local iLydieLeftParty = VM_GetVar("Root/Variables/Chapter1/Scenes/iLydieLeftParty", "N")
	local iHasFlorentina  = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
    
	--Trigger scene:
	if(iLydieLeftParty == 0.0 and iIsGhostTF == 1.0) then
	
        --Issue a loading order.
        fnLoadDelayedBitmapsFromList("Chapter 1 Mei Runestone", gciDelayedLoadLoadAtEndOfTick)
		
		--Move Natalie:
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move To", (8.25 * gciSizePerTile), (9.50 * gciSizePerTile))
		DL_PopActiveObject()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move To", (8.25 * gciSizePerTile), (7.50 * gciSizePerTile))
		DL_PopActiveObject()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move To", (11.25 * gciSizePerTile), (7.50 * gciSizePerTile))
		DL_PopActiveObject()
		
		--Move Lydie:
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Lydie")
			ActorEvent_SetProperty("Move To", (9.25 * gciSizePerTile), (9.50 * gciSizePerTile))
		DL_PopActiveObject()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Lydie")
			ActorEvent_SetProperty("Move To", (9.25 * gciSizePerTile), (6.50 * gciSizePerTile))
		DL_PopActiveObject()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Lydie")
			ActorEvent_SetProperty("Move To", (10.25 * gciSizePerTile), (6.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Face",  0, -1)
		DL_PopActiveObject()
		
		--Dialogue:
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Lydie", "Neutral") ]])
		fnCutscene([[ Append("Lydie: Ugh, what a day![B][C]") ]])
		fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Sad] I'm sorry...[B][C]") ]])
		fnCutscene([[ Append("Lydie: Oh, it's not you.[P] More sick came in today.[P] This one had a sword and was swinging it all over the place![B][C]") ]])
		fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Surprise] A sword?[P] Yikes![B][C]") ]])
		fnCutscene([[ Append("Lydie: Yeah, the plague is making people desperate.[P] I don't know what we're gonna do...[B][C]") ]])
		fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Smirk] We'll be fine.[P] The Countess will think of something.[B][C]") ]])
		fnCutscene([[ Append("Lydie: I don't want to get sick...[B][C]") ]])
		fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Blush] Keep clean, bathe regularly.[P] We'll make it through this together, Lydie.[B][C]") ]])
		fnCutscene([[ Append("Lydie: Yeah.[P] Thanks for cheering me up.[P] I know we'll be okay.[B][C]") ]])
		fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Offended] Hey, the sword wasn't curved, was it?[B][C]") ]])
		fnCutscene([[ Append("Lydie: Hmm, maybe.[P] The girl was oddly dressed, too.[P] Maybe she wasn't from Quantir?[B][C]") ]])
		fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Offended] It's just like my dream...[B][C]") ]])
		fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Neutral] I'm being silly.[P] Just a dream.[B][C]") ]])
		fnCutscene([[ Append("Lydie: Get washing, you goof![B][C]") ]])
		fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Smirk] Yeah, sure...") ]])
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(30)
		fnCutsceneBlocker()

        --Clear censor bars.
        WD_SetProperty("Clear Censor Bars")
		
		--Scene.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Activate Scene") ]])
		fnCutscene([[ Append("Natalie picked up the odd clothes she had been wearing.[P] She checked over them again.[P] Gold and grey, with a big blue symbol on the front. They were unlike anything she'd ever worn.[P] Yet, she had been wearing them.[P] Why?[B][C]") ]])
		fnCutscene([[ Append("She absentmindedly washed the clothes while pondering her life.[P] She couldn't really recall what she had been doing before she got dressed that day.[P] In fact, she could only vaguely recall a lot of things.[B][C]") ]])
		fnCutscene([[ Append("Lydie didn't seem any different than usual.[P] She was her cheerful, prissy self.[P] Maybe, Natalie worried, she'd gotten sick?[P] Had any of the other patients shown memory loss?[B][C]") ]])
		fnCutscene([[ Append("As she scrubbed, she realized there was something concealed in a hidden pocket inside the dress.[P] Without really thinking about it, her hand touched the object...") ]])
		fnCutsceneBlocker()
		
		--Scene.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])

		--Animation.
		local sAllocationString = "WD_SetProperty(\"Allocate Animation\", gciMeiRuneFramesTotal, 1, -90)"
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
		fnCutscene(sAllocationString)
		for i = 0, gciMeiRuneFramesTotal-1, 1 do
			local iNumber = string.format("%02i", i)
			local sString = "WD_SetProperty(\"Set Animation Frame\", " .. i .. ", \"Root/Images/Scenes/Mei/RuneAnim" .. iNumber .. "\")"
			fnCutscene(sString)
		end
		
		fnCutscene([[ WD_SetProperty("Activate Flash", 0) ]])
		fnCutscene([[ Append("All at once, Mei remembered who she was.[P] The runestone sent a shock through her ghostly form, awakening her true self,[P] formerly lost within Natalie.[B][C]") ]])
		fnCutscene([[ Append("It took all of Mei's concentration not to react in pure horror at what she had become.[P] Lydie, her best friend, hadn't noticed...[P] No...[P] She was Mei...[P] not Natalie...[B][C]") ]])
		fnCutscene([[ Append("...[B][C]") ]])
		fnCutscene([[ Append("Natalie let despair wash over her as the realization became inescapable.[P] She was dead.[P] She had died a long time ago, cold, scared, and alone.[P] This poor girl, Mei, had been forced to share her memories with her, to donate her body involuntarily.[B][C]") ]])
		fnCutscene([[ Append("Lydie remained oblivious to the revelation, still content in the delusion of undeath.[P] In a perverted way, the curse gave a second chance to the dead.[P] But it was wrong.[P] It was a lie, and it needed to end.[B][C]") ]])
		fnCutscene([[ Append("Mei began to sort herself from Natalie.[P] The two had become so inseparable, so interwoven...[P] but, the runestone had split them again.[P] Memories that weren't hers...[P] No.[P] They were hers now.[P] She would cherish them always.[B][C]") ]])
		fnCutscene([[ Append("Natalie deserved to be remembered, to have the second chance that undeath had teased to her.[P] Mei would give her that chance, but not because of a curse.[P] From her own free will.[P] Natalie and Mei's spirits embraced and became one...") ]])
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--Get the feather duster.
		local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsRelivingScene", "N")
		if(iIsRelivingScene == 0.0) then
			LM_ExecuteScript(gsItemListing, "Translucent Feather Duster")
		end
		
		--Dialogue:
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Lydie", "Neutral") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Lydie?[B][C]") ]])
		fnCutscene([[ Append("Lydie: Hmm, Natalie?[P] You sound awful![P] Are you sick?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] No, I'm fine.[P] Really.[P] Lydie...[P] I -[P] I love you...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Cry] You're...[P] like my sister.[P] You're the closest thing I have to family.[B][C]") ]])
		fnCutscene([[ Append("Lydie: Where's this coming from?[P] Are you sure you're okay?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] (I don't think she realizes what's going on...)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] (What terrible curse would do this to someone?)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I need to go.[P] I have some things I need to do.[B][C]") ]])
		fnCutscene([[ Append("Lydie: Oh, okay![P] I have to finish washing up.[B][C]") ]])
		fnCutscene([[ Append("Lydie: Don't let any of the others catch you or they'll think you're slacking.[P] Apparently, the Countess authorized corporal punishment for slackers.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] I'll -[P] I'll set you free if I can...[B][C]") ]])
		fnCutscene([[ Append("Lydie: Huh?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Nothing...[P] see you later...[B][C]") ]])
		fnCutscene([[ Append("Lydie: Hey, wait![P] You forgot your feather duster![B][C]") ]])
		fnCutscene([[ Append("Mei: [SOUND|World|TakeItem](Received Translucent Feather Duster)[B][C]") ]])
		fnCutscene([[ Append("Lydie: Sheesh, you really are a scatterbrain![P] What would you do if someone saw you without this?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Uh,[P] panic?[B][C]") ]])
		fnCutscene([[ Append("Lydie: Yeah, that's your answer for everything.[B][C]") ]])
		fnCutscene([[ Append("Lydie: ...[P] Sorry.[P] I think never having any time off is getting to me.[P] I didn't mean to snap.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] It's okay.[P] See you later.") ]])
		fnCutsceneBlocker()
	
		--Mei remembers her real name.
		AdvCombat_SetProperty("Push Party Member", "Mei")
			AdvCombatEntity_SetProperty("Display Name", "DEFAULT")
		DL_PopActiveObject()
		
		--Flags
		VM_SetVar("Root/Variables/Chapter1/Scenes/iLydieLeftParty", "N", 1.0)
		
		--Remove Lydie as a follower.
		giFollowersTotal = 0
		gsaFollowerNames = {}
		giaFollowerIDs = {0}
		AL_SetProperty("Unfollow Actor Name", "Lydie")
		
		--Re-add Lydie's collision flag and set her dialogue.
		fnCutscene([[ 
		EM_PushEntity("Lydie")
			TA_SetProperty("Clipping Flag", true)
			TA_SetProperty("Activation Script", gsRoot .. "Maps/QuantirManse/QuantirManseCentralE/Dialogue.lua")
		DL_PopActiveObject()
		]])
		
		--If Florentina was not in the party, unset the ghost TF flag:
		if(iHasFlorentina == 0.0) then
			VM_SetVar("Root/Variables/Chapter1/Scenes/iIsGhostTF", "N", 0.0)
		end
		
	end
end
