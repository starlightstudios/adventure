-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Lydie]|
    if(sActorName == "Lydie") then

        --Common.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Lydie", "Neutral") ]])

        --Variables
        local iIsGhostTF        = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsGhostTF", "N")
        local iPutMaidOutfitOn  = VM_GetVar("Root/Variables/Chapter1/Scenes/iPutMaidOutfitOn", "N")
        local iToldNatalieTwice = VM_GetVar("Root/Variables/Chapter1/Scenes/iToldNatalieTwice", "N")
        local iCleaningProgress = VM_GetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N")
        local iLydieLeftParty   = VM_GetVar("Root/Variables/Chapter1/Scenes/iLydieLeftParty", "N")
        local iGhostFromNCPlus  = VM_GetVar("Root/Variables/Chapter1/Scenes/iGhostFromNCPlus", "N")
        
        --After the ghost TF is over:
        if(iIsGhostTF == 0.0 or (iIsGhostTF == 1.0 and iLydieLeftParty == 1.0)) then
        
            --Variables
            local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
            local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
            local iCompletedQuantirMansion = VM_GetVar("Root/Variables/Chapter1/Scenes/iCompletedQuantirMansion", "N")
            
            --If Florentina is present, reorganize the dialogue.
            if(bIsFlorentinaPresent == true) then
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Null") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
            end
            
            --If the mansion has been completed:
            if(iCompletedQuantirMansion == 1.0) then
                fnCutscene([[ Append("Lydie: Natalie.[P] No.[P] Not Natalie.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] ..?[B][C]") ]])
                fnCutscene([[ Append("Lydie: Am I -[P] dead?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Sad] I don't know how to say this...[B][C]") ]])
                fnCutscene([[ Append("Lydie: We're all dead.[P] You're not.[P] Right?[B][C]") ]])
                fnCutscene([[ Append("Lydie: Something is different.[P] You're not Natalie, but you look so much like her.[B][C]") ]])
                fnCutscene([[ Append("Lydie: You freed us, didn't you?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Sad] The Warden...[P] The Countess...[P] They've moved on.[B][C]") ]])
                fnCutscene([[ Append("Lydie: I knew them, I think.[P] I -[P] this isn't my body, is it?[P] I'm sorry, whoever I was.[B][C]") ]])
                fnCutscene([[ Append("Lydie: Natalie -[P] Mei.[P] You were Mei.[P] Please remember us.[P] Tell someone what happened here.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] I will.[P] You can rest easily.[B][C]") ]])
                fnCutscene([[ Append("Lydie: Funny, isn't it?[P] Natalie was my best friend, and now, after all you've done, you are, too.[P] Thank you, Mei.[B][C]") ]])
                fnCutscene([[ Append("Lydie: I don't know how long until I fade away, but I can do it happily...") ]])
            
            --Normal:
            elseif(sMeiForm == "Ghost" and bIsFlorentinaPresent == false) then
                fnCutscene([[ Append("Lydie: Oh hey, Natalie.[P] Everything all right?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Yeah.[P] Just wanted to see how you're doing.[B][C]") ]])
                fnCutscene([[ Append("Lydie: Still some washing to do.[P] I'll catch up with you later!") ]])
            
            --Normal, with Florentina:
            elseif(sMeiForm == "Ghost" and bIsFlorentinaPresent == true) then
                fnCutscene([[ Append("Lydie: Natalie, why are you lugging around a houseplant?[B][C]") ]])
                if(iGhostFromNCPlus == 0.0) then
                    fnCutscene([[ Append("Florentina:[E|Confused] *Why is she calling me a houseplant?!*[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] *That's what you look like to them!*[B][C]") ]])
                else
                    fnCutscene([[ Append("Florentina:[E|Confused] *I refuse to get used to being called that*[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] *Shhh!*[B][C]") ]])
                end
                fnCutscene([[ Append("Mei:[E|Neutral] I was just doing some...[P] redecorating.[P] Don't worry, the Countess said it was okay.[B][C]") ]])
                fnCutscene([[ Append("Lydie: Oh, good.[P] This place could use some livening up.[P] Don't strain yourself, though.[P] That's a huge plant!") ]])
        
            --Non-ghost without Florentina:
            elseif(sMeiForm ~= "Ghost" and bIsFlorentinaPresent == false) then
                fnCutscene([[ Append("Lydie: Jeez, Natalie, you look terrible![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Oh, I'm fine.[P] My makeup is just a little off.[B][C]") ]])
                fnCutscene([[ Append("Lydie: If you say so.[P] Stay healthy, okay?[P] Last thing we need is you getting sick, too!") ]])
        
            --Non-ghost with Florentina:
            else
                fnCutscene([[ Append("Lydie: Jeez, Natalie, you look terrible![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Oh, I'm fine.[P] My makeup is just a little off.[B][C]") ]])
                fnCutscene([[ Append("Lydie: Maybe you're allergic to that houseplant you're lugging around?[B][C]") ]])
                if(iGhostFromNCPlus == 0.0) then
                    fnCutscene([[ Append("Florentina:[E|Confused] *Why is she calling me a houseplant?!*[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] *That's what you look like to them!*[B][C]") ]])
                else
                    fnCutscene([[ Append("Florentina:[E|Confused] *I refuse to get used to being called that*[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] *Shhh!*[B][C]") ]])
                end
                fnCutscene([[ Append("Mei:[E|Neutral] Oh, maybe.[P] I was just doing some redecorating.[P] The Countess said it was okay.[B][C]") ]])
                fnCutscene([[ Append("Lydie: Well, all right then.[P] Just make sure you don't inhale too much pollen or you'll be sneezing all week!") ]])
            end
        end
    end
end
