-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Walk Natalie back during the ghost TF
if(sObjectName == "WalkbackN") then
	
	--Variables.
	local iIsGhostTF = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsGhostTF", "N")
	local iLydieLeftParty = VM_GetVar("Root/Variables/Chapter1/Scenes/iLydieLeftParty", "N")
	if(iIsGhostTF == 1.0) then

		--If Lydie is in the party:
		if(iLydieLeftParty == 0.0) then

			--Common.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Lydie", "Neutral") ]])
			fnCutscene([[ Append("Lydie: Natalie![P] That's not the way to the laundry![B][C]") ]])
			fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Blush] Oops, sorry!") ]])
			fnCutsceneBlocker()
		
		--Lydie is not in the party, so we need to rescue Florentina:
		else

			--Common.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
			fnCutscene([[ Append("Mei:[E|Smirk] (I should probably go rescue Florentina before she finds her own way out and comes after me!)") ]])
			fnCutsceneBlocker()
		
		end

		--Walk them both back.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move To", (9.75 * gciSizePerTile), (15.50 * gciSizePerTile))
		DL_PopActiveObject()
		if(iLydieLeftParty == 0.0) then
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Lydie")
				ActorEvent_SetProperty("Move To", (9.75 * gciSizePerTile), (15.50 * gciSizePerTile))
			DL_PopActiveObject()
			fnCutsceneBlocker()
		end
		
		--Fold the party positions up.
		fnCutscene([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()

	end

--Walk Natalie back during the ghost TF
elseif(sObjectName == "WalkbackS") then
	
	--Variables.
	local iIsGhostTF = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsGhostTF", "N")
	local iLydieLeftParty = VM_GetVar("Root/Variables/Chapter1/Scenes/iLydieLeftParty", "N")
	if(iIsGhostTF == 1.0) then

		--If Lydie is in the party:
		if(iLydieLeftParty == 0.0) then

			--Common.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Lydie", "Neutral") ]])
			fnCutscene([[ Append("Lydie: Natalie![P] That's not the way to the laundry![B][C]") ]])
			fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Blush] Oops, sorry!") ]])
			fnCutsceneBlocker()
		
		--Lydie is not in the party, so we need to rescue Florentina:
		else

			--Common.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
			fnCutscene([[ Append("Mei:[E|Smirk] (I should probably go rescue Florentina before she finds her own way out and comes after me!)") ]])
			fnCutsceneBlocker()
		
		end

		--Walk them both back.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move To", (9.75 * gciSizePerTile), (16.50 * gciSizePerTile))
		DL_PopActiveObject()
		if(iLydieLeftParty == 0.0) then
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Lydie")
				ActorEvent_SetProperty("Move To", (9.75 * gciSizePerTile), (16.50 * gciSizePerTile))
			DL_PopActiveObject()
		end
		fnCutsceneBlocker()
		
		--Fold the party positions up.
		fnCutscene([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()

	end
end
