-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
--Tent.
if(sObjectName == "Tent") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A single-person tent.[P] Looks pretty beaten up...)") ]])
    fnCutsceneBlocker()
	
--Cooking pots.
elseif(sObjectName == "CookingPot") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Cooking utensils.[P] Spotless.[P] Whoever used them last cleaned them really well.)") ]])
    fnCutsceneBlocker()
	
--Sleeping bags.
elseif(sObjectName == "SleepingBag") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Portable cloth sleeping bags.[P] They feel damp.[P] Probably haven't been used for a while.)") ]])
    fnCutsceneBlocker()
	
--Journal A.
elseif(sObjectName == "JournalA") then

	--Setup.
	fnStandardMajorDialogue()
		
	--Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A sketchbook, with a drawing of a slime girl on it.[P] Read it?)[BLOCK]") ]])

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sure\", " .. sDecisionScript .. ", \"JournalRead\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"Ignore\") ")
	fnCutsceneBlocker()

--Reading the southern journal.
elseif(sTopicName == "JournalReadA") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutscene([[ Append("Mei:[E|Neutral] 'Heavenly Doves Research Log:: [P]Slime Partirhuman, Trannadar Region'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] There are several sketches of various slime girls, some as humanoids and some as puddles on the ground.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'The common slime girl, these creatures can be found in many biomes across Pandemonium. They are a carrion feeder which periodically hunts live game.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'Slimes are never found in dry regions, as they appear to require frequent moisture refilling. Their locomotion appears to consume water at a great rate.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'In addition, they are not found in freezing regions for presumably a similar reason. Otherwise, they have a great range. Their properties do not change across their habitat.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'Slimes usually stay in an amorphous form, hiding from sunlight during the day and emerging at night to consume insects, rodents, or unaware birds. Slimes are not choosy in what they eat, but rarely eat much leafy material.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'Their life duration is considered to be indefinite at this point in time. Their body structure is highly resistant to parasites and diseases, and they do not appear to age like humans do.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'Slimes tend to be killed by drying out or being unable to find food. Most predators do not regard them as worth the effort, as their bodies provide very little nutrition.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'To reproduce, slimes will behave erratically for several weeks during the summer. Instead of hiding during the day, they will instead take on human form, always female.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'While in human form, their locomotive rate is increased, as is their water consumption. The slime will then attempt to locate humans to infect.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'An infected human will slowly become covered in slime. The infection progresses over a period of about thirty minutes. Once the entirety of the human has been overcome, they are permanently a slime.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'A number of cures exist for this state, but all are worthless once the human succumbs. Folk remedies and modern medicine can revert an infected human from approximately an eighty percent infection rate.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] After that is a series of diagrams of infected humans, and a discussion of cures.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] I guess I know all about slimes now!") ]])
	
--Journal B.
elseif(sObjectName == "JournalB") then

	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiReadClaudiasJournal", "N", 1.0)

	--Setup.
	fnStandardMajorDialogue()
		
	--If Mei has not met Florentina, she doesn't know who Claudia is...
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	if(bIsFlorentinaPresent == false) then
		fnCutscene([[ Append("Mei:[E|Neutral] (This journal belongs to Claudia Romanus.[P] Seems she wrote a lot, as this book is full and the last entry is over a year ago.)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] (Should I read it?)[B][C]") ]])
	
	--If Florentina is present, Mei knows who Claudia is.
	else
		fnCutscene([[ Append("Mei:[E|Laugh] Look, Florentina![P] The journal says 'Claudia Romanus'![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Don't get too excited.[P] This is one of her older journals.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] ...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Yep, the last entry is over a year ago, before they came to Trannadar.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] We should probably read it anyway, right?[BLOCK]") ]])
	end

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sure\", " .. sDecisionScript .. ", \"JournalReadB\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"Ignore\") ")
	fnCutsceneBlocker()

--Reading the southern journal.
elseif(sObjectName == "JournalRead") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutscene([[ Append("Mei:[E|Neutral] 'Heavenly Doves Research Log:: [P]Slime Partirhuman, Trannadar Region'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] There are several sketches of various slime girls, some as humanoids and some as puddles on the ground.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'The common slime girl, these creatures can be found in many biomes across Pandemonium.[P] They are a carrion feeder which periodically hunts live game.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'Slimes are never found in dry regions, as they appear to require frequent moisture refilling.[P] Their locomotion appears to consume water at a great rate.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'In addition, they are not found in freezing regions for presumably a similar reason.[P] Otherwise, they have a great range.[P] Their properties do not change across their habitat.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'Slimes usually stay in an amorphous form, hiding from sunlight during the day and emerging at night to consume insects, rodents, or unaware birds.[P] Slimes are not choosy in what they eat, but rarely eat much leafy material.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'Their life duration is considered to be indefinite at this point in time.[P] Their body structure is highly resistant to parasites and diseases, and they do not appear to age like humans do.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'Slimes tend to be killed by drying out or being unable to find food.[P] Most predators do not regard them as worth the effort, as their bodies provide very little nutrition.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'To reproduce, slimes will behave erratically for several weeks during the summer.[P] Instead of hiding during the day, they will take on human form, always female.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'While in human form, their locomotive rate is increased, as is their water consumption.[P] The slime will then attempt to locate humans to infect.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'An infected human will slowly become covered in slime.[P] The infection progresses over a period of about thirty minutes.[P] Once the entirety of the human has been overcome, they are permanently a slime.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'A number of cures exist for this state, but all are worthless once the human succumbs.[P] Folk remedies and modern medicine can revert an infected human from approximately an eighty percent infection rate.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] After that is a series of diagrams of infected humans, and a discussion of cures.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] I guess I know all about slimes now!") ]])

--Reading the northern journal.
elseif(sObjectName == "JournalReadB") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutscene([[ Append("Mei:[E|Neutral] 'Heavenly Doves Research Log::[P] Quantir Expedition'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'We met up with the faithful located in Dry Well today.[P] Despite the climate, they were hospitable and cheerful.[P] It's always nice to see fellow adherents on Pandemonium!'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'Sadly, none of the convent elected to stay and study with them.[P] It seems they wanted to follow me and aid in my research all the more.[P] No matter, I will act as their shepherd.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'We must first study the demiwasps in the Quantir High Wastes region west of here, and possibly the harpies if we have time.[P] After that, we must head to the Trannadar region.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] 'I'm looking forward to spreading the word amongst the people of Trannadar, but our research does take priority.[P] It is so exhausting being on the road, but our purpose is clear and our will is strong.'[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] There's a lot of research notes about Quantir, but the book is filled before they cross to Trannadar.[P] There must be another journal around here...") ]])
	
--Ignore the journal.
elseif(sObjectName == "Ignore") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")

-- |[Exits]|
elseif(sObjectName == "ToCenterW") then

    --Key check. During a relive, the key is not checked.
	local iMansionKeyCount = AdInv_GetProperty("Item Count", "Quantir Mansion Key")
	local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsRelivingScene", "N")
    if(iIsRelivingScene == 1.0) then iMansionKeyCount = 1 end
    
    --Handler.
	if(iMansionKeyCount > 0) then
		AudioManager_PlaySound("World|FlipSwitch")
		AL_BeginTransitionTo("QuantirManseCentralW", "Null")
	else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Locked tight.[P] There must be a key around here.)") ]])
        fnCutsceneBlocker()
	end
	
elseif(sObjectName == "ToCenterE") then
    
    --Key check. During a relive, the key is not checked.
	local iMansionKeyCount = AdInv_GetProperty("Item Count", "Quantir Mansion Key")
	local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsRelivingScene", "N")
    if(iIsRelivingScene == 1.0) then iMansionKeyCount = 1 end
    
    --Handler.
	if(iMansionKeyCount > 0) then
		AudioManager_PlaySound("World|FlipSwitch")
		AL_BeginTransitionTo("QuantirManseCentralE", "Null")
	else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Locked tight.[P] There must be a key around here.)") ]])
        fnCutsceneBlocker()
	end
	
elseif(sObjectName == "ToNorthE") then

    --Key check. During a relive, the key is not checked.
	local iMansionKeyCount = AdInv_GetProperty("Item Count", "Quantir Mansion Key")
	local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsRelivingScene", "N")
    if(iIsRelivingScene == 1.0) then iMansionKeyCount = 1 end
    
    --Handler.
	if(iMansionKeyCount > 0) then
		AudioManager_PlaySound("World|FlipSwitch")
		AL_BeginTransitionTo("QuantirManseNEHall", "FORCEPOS:25.0x42.0x0")
	else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Locked tight.[P] There must be a key around here.)") ]])
        fnCutsceneBlocker()
	end
	
elseif(sObjectName == "ToNorthW") then

    --Key check. During a relive, the key is not checked.
	local iMansionKeyCount = AdInv_GetProperty("Item Count", "Quantir Mansion Key")
	local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsRelivingScene", "N")
    if(iIsRelivingScene == 1.0) then iMansionKeyCount = 1 end
    
    --Handler.
	if(iMansionKeyCount > 0) then
		AudioManager_PlaySound("World|FlipSwitch")
		AL_BeginTransitionTo("QuantirManseNWHall", "Null")
	else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Locked tight.[P] There must be a key around here.)") ]])
        fnCutsceneBlocker()
	end
	
elseif(sObjectName == "ToOutside") then
	AudioManager_PlaySound("World|FlipSwitch")
	AL_BeginTransitionTo("SpookyExterior", "FORCEPOS:13.5x6.0x0.0")

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
