-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
--Empty Journal.
if(sObjectName == "EmptyJournal") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A journal.[P] No words appear on its pages.)") ]])
    fnCutsceneBlocker()
	
--Empty Crate.
elseif(sObjectName == "EmptyCrate") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (There is nothing in this crate.[P] It is spotless.[P] It has never been used.)") ]])
    fnCutsceneBlocker()
	
--Empty Books.
elseif(sObjectName == "EmptyBooks") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (All of the books are blank.)") ]])
    fnCutsceneBlocker()

-- |[Big Sequence]|
--Countess Quantir...
elseif(sObjectName == "Countess") then
				
	--Topic.
	WD_SetProperty("Unlock Topic", "Countess Quantir", 1)

	--Variables.
	local iSpokeToCountess = VM_GetVar("Root/Variables/Chapter1/Scenes/iSpokeToCountess", "N")
	local iCompletedQuantirMansion = VM_GetVar("Root/Variables/Chapter1/Scenes/iCompletedQuantirMansion", "N")
	
	--Event is complete:
	if(iCompletedQuantirMansion == 1.0) then
		
		--Base.
		fnStandardMajorDialogue(true)
		fnCutscene([[ Append("Mei:[E|Neutral] A pool of inert blood.[P] There's nothing else here...") ]])
	
	--Mei has not spoken to the Countess before.
	elseif(iSpokeToCountess == 0.0) then
	
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iSpokeToCountess", "N", 1.0)
        
        --If the quest value is less than this, increment.
        local iWardenQuestState = VM_GetVar("Root/Variables/Chapter1/Scenes/iWardenQuestState", "N")
        if(iWardenQuestState < 2.0) then
            VM_SetVar("Root/Variables/Chapter1/Scenes/iWardenQuestState", "N", 2.0)
        end

		--Base.
		fnStandardMajorDialogue(true)
		fnCutscene([[ Append("Mei:[E|Sad] That's a lot of blood...[P] and it's very fresh...[B][C]") ]])
		fnCutscene([[ Append("Voice:[VOICE|Ghost] So you have come.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Who's there?[P] Who is it?[B][C]") ]])
		fnCutscene([[ Append("Voice:[VOICE|Ghost] A naive question.[P] More appropriately, who was it?[B][C]") ]])
		fnCutscene([[ Append("Voice:[VOICE|Ghost] Once, I was Countess Josephina Quantir.[P] This land was mine by birth.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Are you one commanding the ghosts here?[B][C]") ]])
		fnCutscene([[ Append("Quantir:[VOICE|Ghost] No.[P] They are beyond my reach.[P] I am here, serving my sentence.[B][C]") ]])
		fnCutscene([[ Append("Quantir:[VOICE|Ghost] I have not had a visitor before.[P] My jailor will no doubt be here, soon, to send you on your way.[P] This is not the business of the living.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Countess Quantir...[P] You were looking for a cure.[P] Did you find one?[P] Did you cure the sickness?[B][C]") ]])
		fnCutscene([[ Append("Quantir:[VOICE|Ghost] You have read her diaries, I assume, as you certainly were not alive to see the events.[P] Your perception is mangled.[P] At no point was I looking for a cure.[B][C]") ]])
		fnCutscene([[ Append("Quantir:[VOICE|Ghost] I created the disease.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Surprise] ..![B][C]") ]])
		fnCutscene([[ Append("Quantir:[VOICE|Ghost] Such innocence![P] It reminds me of her.[P] Before she found out, she was like you.[P] Did you idolize me?[P] Did you think me a hero?[B][C]") ]])
		fnCutscene([[ Append("Quantir:[VOICE|Ghost][EMOTION|Mei|Neutral] Your answer doesn't matter.[B][C]") ]])
		fnCutscene([[ Append("Quantir:[VOICE|Ghost] The people were unwashed, gullible, easily led.[P] Their deaths meant nothing to me.[P] Ruling them was a burden which kept me from my true passions.[P] They were my property, I ought to be able to kill them.[B][C]") ]])
		fnCutscene([[ Append("Quantir:[VOICE|Ghost] The disease was meant to wipe out the ones who were a burden.[P] But she survived.[P] She had as much talent as I, perhaps more.[P] She found my books and studied without my consent.[B][C]") ]])
		fnCutscene([[ Append("Quantir:[VOICE|Ghost] She would have been one of the two left alive once the disease ran its course,[P] but when she found out,[P] she plunged a butcher's knife into my chest.[P] Then, her own.[B][C]") ]])
		fnCutscene([[ Append("Quantir:[VOICE|Ghost] The fool had no idea the magical power she had been refining.[P] Her anger broke the levies and let it spill forth.[B][C]") ]])
		fnCutscene([[ Append("Quantir:[VOICE|Ghost] Each day, she murders me, and herself, again.[P] I feel the terror, the lament, the release.[P] Doubtless she does.[P] It is only fair.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] Did you really kill so many people?[P] Didn't it matter to you?[B][C]") ]])
		fnCutscene([[ Append("Quantir:[VOICE|Ghost] Yes, they died by my hand.[P] I hid some of the bodies and claimed I had a potential cure, so I could watch the hope glimmer and then fade in their eyes.[B][C]") ]])
		fnCutscene([[ Append("Quantir:[VOICE|Ghost] Even now, I don't regret it.[P] They were my property, it was my right.[B][C]") ]])
		fnCutscene([[ Append("Quantir:[VOICE|Ghost] Do you think me a monster?[BLOCK]") ]])
		
		--Decision.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"You Sicken Me\",  " .. sDecisionScript .. ", \"Yes\") ")
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"I Forgive You\",  " .. sDecisionScript .. ", \"No\") ")
		fnCutsceneBlocker()
	
	--Mei has spoken to the Countess before:
	else
		--Base.
		fnStandardMajorDialogue(true)
		fnCutscene([[ Append("Mei:[E|Neutral] Countess...[B][C]") ]])
		fnCutscene([[ Append("Quantir:[VOICE|Ghost] You return.[P] You linger.[P] Will you confront my jailor?[BLOCK]") ]])
		
		--Decision.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Help Her\",  " .. sDecisionScript .. ", \"Help\") ")
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Leave Her\",  " .. sDecisionScript .. ", \"ForgetIt\") ")
		fnCutsceneBlocker()
	end

--You Sicken Me!
elseif(sObjectName == "Yes") then
	
	--Variables.
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	
	--Clean.
	WD_SetProperty("Hide")
		
	--Base.
	fnStandardMajorDialogue(true)
	fnCutscene([[ Append("Mei:[E|Offended] Lady, that is the sickest thing I've ever heard.[P] You deserve everything you get and more.[B][C]") ]])
	
	--Florentina chimes in.
	if(bIsFlorentinaPresent == true) then
		fnCutscene([[ Append("Florentina:[E|Confused] Gotta agree.[P] I'd join in the daily murder if I could.[B][C]") ]])
	end
	
	--Resume.
	fnCutscene([[ Append("Quantir:[VOICE|Ghost] I'll not argue the point.[P] It doesn't matter.[B][C]") ]])
	fnCutscene([[ Append("Quantir:[VOICE|Ghost] So why have you come, if not just to taunt the dead?[P] Perhaps you will free this tormented soul?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] After all you've done?[B][C]") ]])
	fnCutscene([[ Append("Quantir:[VOICE|Ghost] I have suffered as the wicked ought, but I could suffer more and still not pay my debt.[P] I leave it to you to decide.[B][C]") ]])
	fnCutscene([[ Append("Quantir:[VOICE|Ghost] I warn you.[P] The warden will be most displeased.[BLOCK]") ]])
	
	--Decision.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Help Her\",  " .. sDecisionScript .. ", \"Help\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Leave Her\",  " .. sDecisionScript .. ", \"ForgetIt\") ")
	fnCutsceneBlocker()
	
--You deserve forgiveness.
elseif(sObjectName == "No") then
	
	--Variables.
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	
	--Clean.
	WD_SetProperty("Hide")
		
	--Base.
	fnStandardMajorDialogue(true)
	fnCutscene([[ Append("Mei:[E|Offended] No matter what terrible crimes you have committed, nobody need suffer.[P] Even if you aren't sorry, you still don't deserve this.[B][C]") ]])
	
	--Florentina chimes in.
	if(bIsFlorentinaPresent == true) then
		fnCutscene([[ Append("Florentina:[E|Confused] I'm not so sure.[P] I'd be giving her a daily perforation too, if I had the chance.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] Then again, all things gotta end.[P] Maybe it's time for this to...[B][C]") ]])
	end
	
	--Resume.
	fnCutscene([[ Append("Quantir:[VOICE|Ghost] I'll not argue the point.[P] It doesn't matter.[B][C]") ]])
	fnCutscene([[ Append("Quantir:[VOICE|Ghost] So why have you come, if not just to taunt the dead?[P] Perhaps you will free this tormented soul?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] After all you've done?[B][C]") ]])
	fnCutscene([[ Append("Quantir:[VOICE|Ghost] I have suffered as the wicked ought, but I could suffer more and still not pay my debt.[P] I leave it to you to decide.[B][C]") ]])
	fnCutscene([[ Append("Quantir:[VOICE|Ghost] I warn you.[P] The warden will be most displeased.[BLOCK]") ]])
	
	--Decision.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Help Her\",  " .. sDecisionScript .. ", \"Help\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Leave Her\",  " .. sDecisionScript .. ", \"ForgetIt\") ")
	fnCutsceneBlocker()

--Help her against her jailor:
elseif(sObjectName == "Help") then
	
    --Flag.
    local bIsFlorentinaPresent = AL_GetProperty("Is Character Following", "Florentina")
    
	--Clean.
	WD_SetProperty("Hide")
		
	--Base.
	fnStandardMajorDialogue(true)
    if(bIsFlorentinaPresent == false) then
        fnCutscene([[ Append("Mei:[E|Neutral] I'll help.[P] I'll speak to your jailor.[B][C]") ]])
    else
        fnCutscene([[ Append("Mei:[E|Neutral] We'll help.[P] We'll speak to your jailor.[B][C]") ]])
    end
	fnCutscene([[ Append("Quantir:[VOICE|Ghost] There can be no negotiation.[P] So lost is she in her rage that I doubt she even understands my pleas.[B][C]") ]])
	fnCutscene([[ Append("Quantir:[VOICE|Ghost] You will have to fight her, I am afraid.[P] Even now I hear her draw close.[P] She comes to resume our ritual.[B][C]") ]])
	fnCutscene([[ Append("Quantir:[VOICE|Ghost] Prepare yourselves, mortals.[B][C]") ]])
    if(bIsFlorentinaPresent == false) then
        fnCutscene([[ Append("Mei:[E|Offended] Okay...[P] here I go...") ]])
    else
        fnCutscene([[ Append("Mei:[E|Offended] Okay...[P] here we go...") ]])
    end
	fnCutsceneBlocker()

	--Wait a bit.
	fnCutscene([[ AL_SetProperty("Activate Fade", 30, gci_Fade_Under_GUI, true, 0.5, 0, 0, 0, 0.5, 0, 0, 1) ]])
	fnCutsceneWait(30)
	fnCutsceneBlocker()

	--Boss battle!
	fnCutscene([[
        AdvCombat_SetProperty("World Pulse", true)
		AdvCombat_SetProperty("Next Combat Music", "RottenTheme", 0.0000)
		AdvCombat_SetProperty("Reinitialize")
		AdvCombat_SetProperty("Activate")
		AdvCombat_SetProperty("Unretreatable", true)
		LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/000 Scripted Enemies/Boss Arachnophelia.lua", 0)
		AdvCombat_SetProperty("Victory Script", gsRoot .. "Chapter 1/Scenes/QuantirManse_Ending/Combat_Victory.lua")
		AdvCombat_SetProperty("Defeat Script", gsStandardGameOver)
	]])
	fnCutsceneBlocker()

--Let her rot.
elseif(sObjectName == "ForgetIt") then
	
	--Clean.
	WD_SetProperty("Hide")
		
	--Base.
	fnStandardMajorDialogue(true)
	fnCutscene([[ Append("Mei:[E|Neutral] You'll stay where you are, for now.[B][C]") ]])
	fnCutscene([[ Append("Quantir:[VOICE|Ghost] Understandable.[P] I'll not ask you to risk yourself on my behalf.[P] Nothing would be gained or lost in any case.[B][C]") ]])
	fnCutscene([[ Append("Quantir:[VOICE|Ghost] Please leave.[P] I would not want you to be caught in her mindless wrath.") ]])
	fnCutsceneBlocker()

-- |[Exits]|
elseif(sObjectName == "ToNWHall") then
	AudioManager_PlaySound("World|FlipSwitch")
	AL_BeginTransitionTo("QuantirManseNWHall", "FORCEPOS:5x7")

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
