-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
--Point where the party can drop down.
if(sObjectName == "DropPoint") then
	
	--Variables
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
    local iDeployedRope = VM_GetVar("Root/Variables/Chapter1/Scenes/iDeployedRope", "N")
    if(iDeployedRope == 1.0) then return end
	
    --Set flag.
    VM_SetVar("Root/Variables/Chapter1/Scenes/iDeployedRope", "N", 1.0)
    
	--If Florentina is not present:
	if(bIsFlorentinaPresent == false) then
		
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
		fnCutscene([[ Append("Mei:[E|Neutral] (Hmm, looks like a back exit.[P] Maybe if I...)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("Mei", 43.25, 12.50)
        fnCutsceneFace("Mei", 1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutscene([[ AudioManager_PlaySound("World|TakeItem") ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneMove("Mei", 39.25, 13.50)
        fnCutsceneFace("Mei", 0, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutscene([[ AudioManager_PlaySound("World|TakeItem") ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Rope", false) ]])
        fnCutscene([[ AL_SetProperty("Set Collision", 39, 14, 0, 0) ]])
        fnCutscene([[ AL_SetProperty("Set Collision", 39, 15, 0, 0) ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
	
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
		fnCutscene([[ Append("Mei:[E|Laugh] (Problem solved![P] Now I can slip in here whenever I want![P] Thank goodness for inexplicably convenient ropes!)") ]])
        fnCutsceneBlocker()
        
	--If Florentina is present:
	else
		
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
		fnCutscene([[ Append("Mei:[E|Neutral] Hmm...[P] Hey Florentina, want to see a magical magic trick?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Oh my goodness I have been waiting for this moment.[P] Let me see if I can guess how you did it afterwards.") ]])
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("Florentina", 39.25, 12.50)
        fnCutsceneFace("Florentina", 1, 0)
        fnCutsceneMove("Mei", 43.25, 12.50)
        fnCutsceneFace("Mei", 1, 0)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutscene([[ AudioManager_PlaySound("World|TakeItem") ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneMove("Mei", 39.25, 13.50)
        fnCutsceneFace("Mei", 0, 1)
        fnCutsceneBlocker()
        fnCutsceneFace("Florentina", 0, 1)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutscene([[ AudioManager_PlaySound("World|TakeItem") ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Rope", false) ]])
        fnCutscene([[ AL_SetProperty("Set Collision", 39, 14, 0, 0) ]])
        fnCutscene([[ AL_SetProperty("Set Collision", 39, 15, 0, 0) ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("Mei", 0, -1)
	
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
		fnCutscene([[ Append("Mei:[E|Laugh] Ta-da![P] I made a shortcut appear![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Okay let me see if I can guess how you did it.[P] Hmm...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] You picked up the rope, and put it down so we can climb up.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] Correct![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] So then what's the magic?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Laugh] The magic of friendship![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] This 'friendship' lets us break into places?[P] Maybe I was wrong about friendship.[P] Come on, let's go.") ]])
        fnCutsceneBlocker()
        fnAutoFoldParty()
        fnCutsceneBlocker()
	end

-- |[Exits]|
elseif(sObjectName == "ExitSecret") then
	AudioManager_PlaySound("World|FlipSwitch")
	AL_BeginTransitionTo("QuantirManseBasementW", "FORCEPOS:26x8")
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
