-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
if(sObjectName == "Crates") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] (Cleaning supplies and old clothes.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Firepit") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] (A firepit, long since burned down.[P] I can see book covers in the ash pile.[P] There is no wind here, so the ash hasn't blown away...)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "MagicBookA") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] (The covers promise books on sorcery, but the pages have all been ripped out.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "MagicBookB") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] (Books on the history of Quantir.[P] All the pages are ripped out.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "MagicBookC") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] (Storybooks about knights and dragons.[P] The pages are all ripped out.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Corpse") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] (I suppose this is whoever burned the books.[P] It seems to be the only unburied skeleton in the mansion.[P] Does that mean this was the final survivor?)[B][C]") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] (There was a rusty knife lodged in the skeleton's ribcage.[P] I'll leave it where it is.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "JournalD") then

	--Setup.
	fnStandardMajorDialogue()
		
	--If Mei has not met Florentina, she doesn't know who Claudia is...
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	if(bIsFlorentinaPresent == false) then
		fnCutscene([[ Append("Mei:[E|Neutral] (Hey![P] There's some images on this book that look like my runestone!)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] (Should I read it?)[B][C]") ]])
	
	--If Florentina is present, Mei knows who Claudia is.
	else
		fnCutscene([[ Append("Mei:[E|Laugh] F-[P]Florentina, look![P] That symbol![P] It's just like my runestone's![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Hey, calm down, kid.[P] You'll wake the dead[P]. Literally.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Laugh] I can't wait to read it![BLOCK]") ]])
	end

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sure\", " .. sDecisionScript .. ", \"JournalReadD\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"Ignore\") ")
	fnCutsceneBlocker()
	
--Reading journal D.
elseif(sObjectName == "JournalReadD") then
	
	--Flags.
	local iMeiKnowsRilmani = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiKnowsRilmani", "N")
	VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiKnowsRilmani", "N", 1.0)
	
	--Topic.
	WD_SetProperty("Unlock Topic", "Rilmani", 1)
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Clear the flag on this topic if this is the first time reading the book.
	if(iMeiKnowsRilmani == 0.0) then
		WD_SetProperty("Clear Topic Read", "NextMove")
        
        fnStandardMajorDialogue(true)
        fnCutscene([[ Append("Mei:[E|Neutral] 'Heavenly Doves Research Log::[P] Rilmani Partirhuman, Trannadar Region'[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] 'The Rilmani are believed to be one of the oldest partirhuman species in existence.[P] What little we know of them is based on legend and rumour.'[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] 'Artifacts of Rilmani origin are rare and usually prized.[P] They often contain potent magics which cannot be replicated by human sorcery, and thus are sought after by the wise and the greedy alike.'[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] 'The Rilmani are renowned for their usage of mirrors to act as temporary gateways.[P] This is performed in total defiance of teleportation magic.'[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] 'Curiously, the Rilmani language does not evolve the way human languages do.[P] Instead, the Rilmani are said to already have every word they will ever need, and instead simply discover the new word at the right time.'[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] 'What follows is an incomplete study of known Rilmani words...'[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] The image on my runestone apparently means a variety of words like bravery, courage, and perseverence.[B][C]") ]])
        local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
        if(bIsFlorentinaPresent == true) then
            fnCutscene([[ Append("Florentina:[E|Happy] Rilmani, huh. We should search the old mansion you said you woke up in.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Okay, we'll search that crummy dump and look for Rilmani symbols.[P] Maybe that will do it!") ]])
        else
            fnCutscene([[ Append("Mei:[E|Smirk] All right, think, Mei.[P] I showed up at the creepy weird mansion with cultists in it.[P] Maybe if I look there...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Okay, I'll search that crummy dump and look for Rilmani symbols.[P] Maybe that will do it!") ]])
        end
        
    --First read.
    else
	
        fnStandardMajorDialogue(true)
        fnCutscene([[ Append("Mei:[E|Neutral] 'Heavenly Doves Research Log::[P] Rilmani Partirhuman, Trannadar Region'[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Diagrams, diagrams...[P] Seems the doves have no idea what these things look like.[P] This one...[P] looks kind of like an olive?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] 'The Rilmani are believed to be one of the oldest partirhuman species in existence.[P] What little we know of them is based on legend and rumour.'[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] 'Artifacts of Rilmani origin are rare and usually prized.[P] They often contain potent magics which cannot be replicated by human sorcery, and thus are sought after by the wise and the greedy alike.'[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] 'The Rilmani are renowned for their usage of mirrors to act as temporary gateways.[P] This is performed in total defiance of teleportation magic.'[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] 'Curiously, the Rilmani language does not evolve the way human languages do.[P] Instead, the Rilmani are said to already have every word they will ever need, and instead simply discover the new word at the right time.'[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] 'What follows is an incomplete study of known Rilmani words...'[B][C]") ]])
        local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
        if(bIsFlorentinaPresent == true) then
            fnCutscene([[ Append("Mei:[E|Laugh] Courage![P] Valor![P] Bravery![P] Look, Florentina, that's what my runestone means![P] It's a Rilmani word![B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] It means all three?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] 'These are the six words that are considered first-order in the Rilmani language.[P] They are only used in specific contexts, though the nature of the context is not known.'[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] So my runestone is one of these first-order words![P] I don't recognize the other five...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] 'Proper nouns are third-order, and are allowed to be created.[P] Most other words are second-order and supposedly have existed as long as the Rilmani have.[P] The first-order words are therefore meant to be older than the Rilmani language.'[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] So just what does that mean?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Laugh] I haven't got any idea![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] But, I bet if we poke around the Dimensional Trap for a bit, we can find some hints.[P] With this, we can try to translate anything we find![B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] So...[P] no payoff?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] Oh for crying - [P][CLEAR]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] Relax, kid.[P] If we can find a real-live Rilmani, I can probably mug her and get something worth a fortune.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Are you joking right now?[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] If they look like this sketch of a cloudy pig, yes.[P] If they look like this one that has all the spikes...[P] not so much...") ]])
        else
            fnCutscene([[ Append("Mei:[E|Laugh] Courage![P] Valor![P] Bravery![P] It's a Rilmani word![P] My runestone is definitely a Rilmani artifact![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] All right, with this translation guide, I bet I can find something at that old mansion to help me get home![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] I hope the Rilmani can help me, if it comes to that...") ]])
        end
	end

--Ignore the journal.
elseif(sObjectName == "Ignore") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")

--Bookshelf F.
elseif(sObjectName == "BookshelfF") then

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Mei](This handwriting looks to be the same as the person who was put in charge of burying bodies...[P] Seems there's a few torn pages jammed in various books here...)[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei](Seems this is the last in the series...)[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]('Well, I found out where the Countess has been disappearing to.[P] She's been using an invisibility spell and going down into the sewers![P] There's a secret room and everything![P] That must be where she's been working on her healing spell.')[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]('I have to bury Penrose, and...[P] then I'm going to show her what I've learned.[P] No more patients are showing up, I think everyone has either died or fled Quantir.[P] I can't blame them.')[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]('I'm still feeling fine, though.[P] Maybe I'm immune, or maybe one of my spells helped.[P] I don't know.[P] I've never told her, but the Countess is my hero.[P] I wanted to tell her because, if she dies, I won't know what to do.')[B][C]") ]])

	--If Mei doesn't have the sewer key:
	local iSewerKeyCount = AdInv_GetProperty("Item Count", "Quantir Sewer Key")
	if(iSewerKeyCount == 0.0) then
		LM_ExecuteScript(gsItemListing, "Quantir Sewer Key")
		fnCutscene([[ Append("[VOICE|Mei]('I hope she's not sick and hiding it, down there in the sewers.[P] She's given so much for us.[P] We failed, but at least we tried.')[B][C]") ]])
		fnCutscene([[ Append("[VOICE|Mei](Hey, looks like the author left their keyring here.[P] There are several spare keys.)[B][C]") ]])
		fnCutscene([[ Append("[VOICE|Mei][SOUND|World|TakeItem]*Got Quantir Sewer Key*") ]])
	else
		fnCutscene([[ Append("[VOICE|Mei]('I hope she's not sick and hiding it, down there in the sewers.[P] She's given so much for us.[P] We failed, but at least we tried.')") ]])
	end
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
