-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
--Crates.
if(sObjectName == "Crates") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (These crates are full of bones...)")  ]])
    fnCutsceneBlocker()
	
--Coffins.
elseif(sObjectName == "Coffins") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Row upon row of coffins.[P] Some are partially open, and they're full to bursting with bones...)") ]])
    fnCutsceneBlocker()

--Bed.
elseif(sObjectName == "Bed") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A long-disused bed.[P] The layer of dust is so thick that the sheets have hardened.)") ]])
    fnCutsceneBlocker()
	
--Left bookshelf.
elseif(sObjectName == "BookshelfA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Some of the pages are stained with brown fluid.[P] Otherwise, it's a text about surgery.[P] 21st century Earth has a lot to teach Pandemonium about anesthetics.)") ]])
    fnCutsceneBlocker()
	
--Right bookshelf.
elseif(sObjectName == "BookshelfB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('The Humours'.[P] Discusses the four humours, which are various fluids thought to be responsible for diseases.)") ]])
    fnCutsceneBlocker()

--Secret room locked door.
elseif(sObjectName == "DoorW") then
	local iMansionKeyCount = AdInv_GetProperty("Item Count", "Quantir Mansion Key")
	if(iMansionKeyCount > 0) then
		AudioManager_PlaySound("World|FlipSwitch")
		AL_SetProperty("Open Door", "DoorW")
	else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Locked tight.)") ]])
        fnCutsceneBlocker()
	end

--Secret room locked door.
elseif(sObjectName == "DoorE") then
	local iMansionKeyCount = AdInv_GetProperty("Item Count", "Quantir Mansion Key")
	if(iMansionKeyCount > 0) then
		AudioManager_PlaySound("World|FlipSwitch")
		AL_SetProperty("Open Door", "DoorE")
	else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Locked tight.)") ]])
        fnCutsceneBlocker()
	end

--Locked door, western side.
elseif(sObjectName == "LockedDoor") then

	--Variables.
	local iSewerKeycount = AdInv_GetProperty("Item Count", "Quantir Sewer Key")
	local iOpenedSewerDoor = VM_GetVar("Root/Variables/Chapter1/Scenes/iOpenedSewerDoor", "N")
	
	--Open the door.
	if(iSewerKeycount > 0) then
		AL_SetProperty("Open Door", "LockedDoor")
		AudioManager_PlaySound("World|FlipSwitch")
		
		--Show dialogue on the first pass.
		if(iOpenedSewerDoor == 0.0) then
			VM_SetVar("Root/Variables/Chapter1/Scenes/iOpenedSewerDoor", "N", 1.0)
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (The sewer key fit the lock.)") ]])
            fnCutsceneBlocker()
		end
	
	--Locked!
	else
		local iMansionKeyCount = AdInv_GetProperty("Item Count", "Quantir Mansion Key")
		if(iMansionKeyCount > 0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (Locked.[P] The mansion key doesn't fit.)") ]])
            fnCutsceneBlocker()
		else
			
			--Mei is a slime, but she can't squeeze through here.
			local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
			if(sMeiForm == "Slime") then
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ Append("Thought:[VOICE|Leader] (Locked.[P] The bars are freezing cold![P] I'd get frozen solid if I squeezed through!)") ]])
                fnCutsceneBlocker()
			else
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ Append("Thought:[VOICE|Leader] (Locked.)") ]])
                fnCutsceneBlocker()
			end
		end
	end
	
--Journal with its pages torn out.
elseif(sObjectName == "TornJournal") then
	
    --If the quest value is less than one, increment.
    local iWardenQuestState = VM_GetVar("Root/Variables/Chapter1/Scenes/iWardenQuestState", "N")
    if(iWardenQuestState < 1.5) then
        VM_SetVar("Root/Variables/Chapter1/Scenes/iWardenQuestState", "N", 1.5)
    end
    
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Mei](This handwriting looks to be the same as the person who was put in charge of burying bodies...[P] This must have been the journal that the other pages were from.)[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei](The last entry is still in here...)[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]('There is no hope.[P] All turns to ash.[P] The body is merely a contagion that is to be snuffed out.[P] Impurities are the norm, not the exception, and the degenerates indulge these sins.')[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei](That seems to be it.[P] It's the same handwriting, but not the same style at all.)[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei](Oh, there's something written on the back cover...)[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]('Rot forever, you hateful, wicked monster.[P] I'll dedicate every fibre of my being to guarantee your suffering.')[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei](...[P] There's nothing else here.)") ]])
	VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiReadLastJournal", "N", 1.0)

-- |[Exits]|
--To the Quantir Mansion Basement East.
elseif(sObjectName == "SEExit") then
	AudioManager_PlaySound("World|FlipSwitch")
	AL_BeginTransitionTo("QuantirManseBasementE", "FORCEPOS:10.5x7")

--Secret exit!
elseif(sObjectName == "ExitSecret") then
	AudioManager_PlaySound("World|FlipSwitch")
	AL_BeginTransitionTo("QuantirManseSecretExit", "Null")
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
