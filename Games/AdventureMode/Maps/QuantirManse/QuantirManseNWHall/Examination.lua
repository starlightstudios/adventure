-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
--Eastern Statue.
if(sObjectName == "StatueE") then
	
    --If the quest value is less than one, increment.
    local iWardenQuestState = VM_GetVar("Root/Variables/Chapter1/Scenes/iWardenQuestState", "N")
    if(iWardenQuestState < 1.0) then
        VM_SetVar("Root/Variables/Chapter1/Scenes/iWardenQuestState", "N", 1.0)
    end
    
	--Variable dictating if this statue has been answered.
	local iEastStatueCorrect = VM_GetVar("Root/Variables/Chapter1/Scenes/iEastStatueCorrect", "N")
	if(iEastStatueCorrect == 0.0) then
		
		--Base.
		fnStandardMajorDialogue()
		fnCutscene([[ Append("Mei:[E|Neutral] (There's an inscription on the statue.)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] ('What is the fate of all things?')[BLOCK]") ]])
		
		--Correct answer.
		local sCorrectAnswer = "To Become Ash"
		
		--Generate the answers.
		local saAnswerPool = {"To Die", "To Reproduce", "To Fight", "To Decay", "To Struggle", "To Betray", "To Want", "To Indulge"}
		local saAnswerSet = {}
		saAnswerSet[1] = saAnswerPool[LM_GetRandomNumber(1, 8)]
		saAnswerSet[2] = saAnswerPool[LM_GetRandomNumber(1, 8)]
		while(saAnswerSet[2] == saAnswerSet[1]) do saAnswerSet[2] = saAnswerPool[LM_GetRandomNumber(1, 8)] end
		saAnswerSet[3] = saAnswerPool[LM_GetRandomNumber(1, 8)]
		while(saAnswerSet[3] == saAnswerSet[1] or saAnswerSet[3] == saAnswerSet[2]) do saAnswerSet[3] = saAnswerPool[LM_GetRandomNumber(1, 8)] end
		
		--Roll the slot for the correct answer.
		local iCorrectSlot = LM_GetRandomNumber(1, 4)
		if(iCorrectSlot == 4) then
			saAnswerSet[4] = sCorrectAnswer
		else
			saAnswerSet[4] = saAnswerSet[iCorrectSlot]
			saAnswerSet[iCorrectSlot] = sCorrectAnswer
		end

		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
		for i = 1, 4, 1 do
			
			--If this is the correct answer:
			if(saAnswerSet[i] == sCorrectAnswer) then
				fnCutscene(" WD_SetProperty(\"Add Decision\", \"" .. saAnswerSet[i] .. "\",  " .. sDecisionScript .. ", \"CorrectE\") ")
			
			--Wrong answer:
			else
				fnCutscene(" WD_SetProperty(\"Add Decision\", \"" .. saAnswerSet[i] .. "\",  " .. sDecisionScript .. ", \"Wrong\") ")
			end
		end
		
		--Add on the "Ignore it" option so the player can back out.
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Leave This Be\",  " .. sDecisionScript .. ", \"Ignore\") ")
		fnCutsceneBlocker()
		
	--Statue already answered.
	else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The inscription on the statue has vanished...)") ]])
        fnCutsceneBlocker()
	end

--Central Statue.
elseif(sObjectName == "StatueC") then
	
    --If the quest value is less than one, increment.
    local iWardenQuestState = VM_GetVar("Root/Variables/Chapter1/Scenes/iWardenQuestState", "N")
    if(iWardenQuestState < 1.0) then
        VM_SetVar("Root/Variables/Chapter1/Scenes/iWardenQuestState", "N", 1.0)
    end
	
	--Variable dictating if this statue has been answered.
	local iCentralStatueCorrect = VM_GetVar("Root/Variables/Chapter1/Scenes/iCentralStatueCorrect", "N")
	if(iCentralStatueCorrect == 0.0) then
		
		--Base.
		fnStandardMajorDialogue()
		fnCutscene([[ Append("Mei:[E|Neutral] (There's an inscription on the statue.)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] ('What is the body?')[BLOCK]") ]])
		
		--Correct answer.
		local sCorrectAnswer = "A Contagion"
		
		--Generate the answers.
		local saAnswerPool = {"A Vessel", "A Construction", "An Illusion", "An Object", "A Person", "A Lie", "A Crime", "An Inevitability", "A Corruption", "A Failure"}
		local saAnswerSet = {}
		saAnswerSet[1] = saAnswerPool[LM_GetRandomNumber(1, 10)]
		saAnswerSet[2] = saAnswerPool[LM_GetRandomNumber(1, 10)]
		while(saAnswerSet[2] == saAnswerSet[1]) do saAnswerSet[2] = saAnswerPool[LM_GetRandomNumber(1, 10)] end
		saAnswerSet[3] = saAnswerPool[LM_GetRandomNumber(1, 10)]
		while(saAnswerSet[3] == saAnswerSet[1] or saAnswerSet[3] == saAnswerSet[2]) do saAnswerSet[3] = saAnswerPool[LM_GetRandomNumber(1, 10)] end
		
		--Roll the slot for the correct answer.
		local iCorrectSlot = LM_GetRandomNumber(1, 4)
		if(iCorrectSlot == 4) then
			saAnswerSet[4] = sCorrectAnswer
		else
			saAnswerSet[4] = saAnswerSet[iCorrectSlot]
			saAnswerSet[iCorrectSlot] = sCorrectAnswer
		end

		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
		for i = 1, 4, 1 do
			
			--If this is the correct answer:
			if(saAnswerSet[i] == sCorrectAnswer) then
				fnCutscene(" WD_SetProperty(\"Add Decision\", \"" .. saAnswerSet[i] .. "\",  " .. sDecisionScript .. ", \"CorrectC\") ")
			
			--Wrong answer:
			else
				fnCutscene(" WD_SetProperty(\"Add Decision\", \"" .. saAnswerSet[i] .. "\",  " .. sDecisionScript .. ", \"Wrong\") ")
			end
		end
		
		--Add on the "Ignore it" option so the player can back out.
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Leave This Be\",  " .. sDecisionScript .. ", \"Ignore\") ")
		fnCutsceneBlocker()
		
	--Statue already answered.
	else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The inscription on the statue has vanished...)") ]])
        fnCutsceneBlocker()
	end


--Central Statue.
elseif(sObjectName == "StatueW") then
	
    --If the quest value is less than one, increment.
    local iWardenQuestState = VM_GetVar("Root/Variables/Chapter1/Scenes/iWardenQuestState", "N")
    if(iWardenQuestState < 1.0) then
        VM_SetVar("Root/Variables/Chapter1/Scenes/iWardenQuestState", "N", 1.0)
    end
	
	--Variable dictating if this statue has been answered.
	local iWesternStatueCorrect = VM_GetVar("Root/Variables/Chapter1/Scenes/iWesternStatueCorrect", "N")
	if(iWesternStatueCorrect == 0.0) then
		
		--Base.
		fnStandardMajorDialogue()
		fnCutscene([[ Append("Mei:[E|Neutral] (There's an inscription on the statue.)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] ('What is justice?')[BLOCK]") ]])
		
		--Correct answer.
		local sCorrectAnswer = "That The Wicked Suffer"
		
		--Generate the answers.
		local saAnswerPool = {"That Crime Is Punished", "That The Guilty Are Jailed", "That The Weak Are Purged", "That The Pain Is Shared", "That The Strong Are Feted", "That Revenge Is Meted"}
		local saAnswerSet = {}
		saAnswerSet[1] = saAnswerPool[LM_GetRandomNumber(1, 6)]
		saAnswerSet[2] = saAnswerPool[LM_GetRandomNumber(1, 6)]
		while(saAnswerSet[2] == saAnswerSet[1]) do saAnswerSet[2] = saAnswerPool[LM_GetRandomNumber(1, 6)] end
		saAnswerSet[3] = saAnswerPool[LM_GetRandomNumber(1, 6)]
		while(saAnswerSet[3] == saAnswerSet[1] or saAnswerSet[3] == saAnswerSet[2]) do saAnswerSet[3] = saAnswerPool[LM_GetRandomNumber(1, 6)] end
		
		--Roll the slot for the correct answer.
		local iCorrectSlot = LM_GetRandomNumber(1, 4)
		if(iCorrectSlot == 4) then
			saAnswerSet[4] = sCorrectAnswer
		else
			saAnswerSet[4] = saAnswerSet[iCorrectSlot]
			saAnswerSet[iCorrectSlot] = sCorrectAnswer
		end

		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
		for i = 1, 4, 1 do
			
			--If this is the correct answer:
			if(saAnswerSet[i] == sCorrectAnswer) then
				fnCutscene(" WD_SetProperty(\"Add Decision\", \"" .. saAnswerSet[i] .. "\",  " .. sDecisionScript .. ", \"CorrectW\") ")
			
			--Wrong answer:
			else
				fnCutscene(" WD_SetProperty(\"Add Decision\", \"" .. saAnswerSet[i] .. "\",  " .. sDecisionScript .. ", \"Wrong\") ")
			end
		end
		
		--Add on the "Ignore it" option so the player can back out.
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Leave This Be\",  " .. sDecisionScript .. ", \"Ignore\") ")
		fnCutsceneBlocker()
		
	--Statue already answered.
	else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The inscription on the statue has vanished...)") ]])
        fnCutsceneBlocker()
	end

-- |[Results]|
--Player gave the correct answer for the east statue.
elseif(sObjectName == "CorrectE") then
	
	--Clean.
	WD_SetProperty("Hide")
		
	--Base.
	fnStandardMajorDialogue(true)
	fnCutscene([[ Append("Mei:[E|Neutral] ([SOUND|World|RemoteDoor]I heard something open in the distance...)[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] (The inscription vanished!)") ]])
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iEastStatueCorrect", "N", 1.0)
	
--Player gave the correct answer for the central statue.
elseif(sObjectName == "CorrectC") then
	
	--Clean.
	WD_SetProperty("Hide")
		
	--Base.
	fnStandardMajorDialogue(true)
	fnCutscene([[ Append("Mei:[E|Neutral] ([SOUND|World|RemoteDoor]I heard something open in the distance...)[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] (The inscription vanished!)") ]])
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iCentralStatueCorrect", "N", 1.0)
	
--Player gave the correct answer for the west statue.
elseif(sObjectName == "CorrectW") then
	
	--Clean.
	WD_SetProperty("Hide")
		
	--Base.
	fnStandardMajorDialogue(true)
	fnCutscene([[ Append("Mei:[E|Neutral] ([SOUND|World|RemoteDoor]I heard something open in the distance...)[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] (The inscription vanished!)") ]])
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iWesternStatueCorrect", "N", 1.0)

--Wrong answer!
elseif(sObjectName == "Wrong") then
	
	--Clean.
	WD_SetProperty("Hide")
	
	--Wound the party.
	fnCutscene([[ AudioManager_PlaySound("World|Heartbeat") ]])
	fnCutscene([[ AL_SetProperty("Activate Fade", 15, gci_Fade_Over_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(18)
	fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Activate Fade", 30, gci_Fade_Over_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneWait(120)
	fnCutsceneBlocker()

	--Reduce Mei and Florentina's HP by 100. If either gets KO'd, send the party back to the last save point.
	local bEitherKOd = false
	AdvCombat_SetProperty("Push Party Member", "Mei")
		local iMeiHP = AdvCombatEntity_GetProperty("Health")
		if(iMeiHP <= 100) then 
			bEitherKOd = true 
		else
            AdvCombatEntity_SetProperty("Health", iMeiHP - 100)
		end
	DL_PopActiveObject()
	
	--If Florentina is present:
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	if(bIsFlorentinaPresent == true) then
		AdvCombat_SetProperty("Push Party Member", "Florentina")
			local iFlorentinaHP = AdvCombatEntity_GetProperty("Health")
			if(iFlorentinaHP <= 100) then 
				bEitherKOd = true 
			else
                AdvCombatEntity_SetProperty("Health", iFlorentinaHP - 100)
			end
		DL_PopActiveObject()
	end
	
	--In all cases, increment the failure count.
	local iIncorrectGuesses = VM_GetVar("Root/Variables/Chapter1/Scenes/iIncorrectGuesses", "N")
	VM_SetVar("Root/Variables/Chapter1/Scenes/iIncorrectGuesses", "N", iIncorrectGuesses + 1)
	
	--If either character was KO'd:
	if(bEitherKOd == true) then
		
		--Dialogue.
		fnStandardMajorDialogue()
		fnCutscene([[ AL_SetProperty("Activate Fade", 30, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
		fnCutscene([[ Append("Mei:[E|Neutral] (The statue doesn't seem to be happy with that answer...)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] (A chill blows...[P] I feel weak...)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] (It's...[P] so...[P] dark...)") ]])
		fnCutsceneBlocker()
		
		--Back to last save.
		fnCutsceneWait(120)
		fnCutsceneBlocker()
		fnCutscene([[ AL_BeginTransitionTo("LASTSAVE", gsRoot .. "Chapter 1/Scenes/Defeat_Statue/Scene_Begin.lua") ]])
		fnCutsceneBlocker()
	
	--Wounded but not KO'd...
	else
		
		--Dialogue.
		fnStandardMajorDialogue()
		fnCutscene([[ Append("Mei:[E|Neutral] (The statue doesn't seem to be happy with that answer...)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] (A chill blows...[P] I feel weak...)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] (Ugh...[P] Better be more careful about my answers...[P] There must be a clue to this riddle somewhere...)") ]])
		fnCutsceneBlocker()
	
	end

--Player elected to ignore this object.
elseif(sObjectName == "Ignore") then
	WD_SetProperty("Hide")

-- |[Exits]|
elseif(sObjectName == "ToEntrance") then
	AudioManager_PlaySound("World|FlipSwitch")
	AL_BeginTransitionTo("QuantirManseEntrance", "FORCEPOS:5x7")

--To the ending encounter.
elseif(sObjectName == "ToTruth") then

	--Variables.
	local iEastStatueCorrect    = VM_GetVar("Root/Variables/Chapter1/Scenes/iEastStatueCorrect", "N")
	local iCentralStatueCorrect = VM_GetVar("Root/Variables/Chapter1/Scenes/iCentralStatueCorrect", "N")
	local iWesternStatueCorrect = VM_GetVar("Root/Variables/Chapter1/Scenes/iWesternStatueCorrect", "N")

	--If any one of the variables is not set:
	if(iEastStatueCorrect == 0.0 or iCentralStatueCorrect == 0.0 or iWesternStatueCorrect == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The door is held shut with an unearthly power...) ") ]])
        fnCutsceneBlocker()

	--Open it.
	else
		AudioManager_PlaySound("World|FlipSwitch")
		AL_BeginTransitionTo("QuantirManseTruth", "Null")
	end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
