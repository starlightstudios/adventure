-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Lydie]|
    if(sActorName == "Lydie") then

        DialogueActor_Push("Mei")
            DialogueActor_SetProperty("Add Alias", "Natalie")
        DL_PopActiveObject()


        --Common.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Lydie", "Neutral") ]])

        --Variables
        local iIsGhostTF        = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsGhostTF", "N")
        local iPutMaidOutfitOn  = VM_GetVar("Root/Variables/Chapter1/Scenes/iPutMaidOutfitOn", "N")
        local iToldNatalieTwice = VM_GetVar("Root/Variables/Chapter1/Scenes/iToldNatalieTwice", "N")
        local iCleaningProgress = VM_GetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N")
        local iLydieLeftParty   = VM_GetVar("Root/Variables/Chapter1/Scenes/iLydieLeftParty", "N")
    
        --Natalie has not put the maid outfit on yet:
        if(iPutMaidOutfitOn == 0.0) then
            fnCutscene([[ Append("Lydie: Quick, put the spare uniform on before someone sees![P] It's in the crate over there![B][C]") ]])
            fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Neutral] Right, right, okay!") ]])
        
        --Natalie has put the maid outfit on:
        elseif(iPutMaidOutfitOn == 1.0 and iToldNatalieTwice == 0.0 and iCleaningProgress < 9.0) then
            VM_SetVar("Root/Variables/Chapter1/Scenes/iToldNatalieTwice", "N", 1.0)
            fnCutscene([[ Append("Lydie: What are you waiting for?[P] Get cleaning![B][C]") ]])
            fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Neutral] Don't gotta tell me twice!") ]])
        
        --Natalie has put the maid outfit on, and been told twice:
        elseif(iPutMaidOutfitOn == 1.0 and iToldNatalieTwice == 1.0 and iCleaningProgress < 9.0) then
            VM_SetVar("Root/Variables/Chapter1/Scenes/iToldNatalieTwice", "N", 1.0)
            fnCutscene([[ Append("Lydie: What are you waiting for?[P] Get cleaning![B][C]") ]])
            fnCutscene([[ Append("Natalie:[EMOTION|Mei|Neutral] Don't gotta tell me twice![B][C]") ]])
            fnCutscene([[ Append("Lydie: Apparently I do, because I have told you twice![B][C]") ]])
            fnCutscene([[ Append("Natalie:[EMOTION|Mei|Surprise] Ack![P] Don't scold me, Lydie!") ]])
        
        --Natalie is done cleaning:
        elseif(iCleaningProgress == 9.0) then
            
            --Dialogue.
            fnCutscene([[ Append("Lydie: Wow, you really managed to pull it off![P] If you hadn't been slacking, I'd be tempted to tell the Countess how well you did![B][C]") ]])
            fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Neutral] Pah![P] She's never going to believe you![B][C]") ]])
            fnCutscene([[ Append("Lydie: And why not?[B][C]") ]])
            fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Happy] You're such a suck-up![B][C]") ]])
            fnCutscene([[ Append("Lydie: Am not![B][C]") ]])
            fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Laugh] Are too![P] Ha ha![B][C]") ]])
            fnCutscene([[ Append("Lydie: You're not normally such a slacker.[P] What happened?[P] Were you daydreaming again?[B][C]") ]])
            fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Neutral] I think so.[P] I had such a weird dream.[P] I dreamt I was some girl named Mei, from Hong Kong.[B][C]") ]])
            fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Neutral] I was such a great warrior![P] I had a sword and I escaped from some creepy cultists and - [P][CLEAR]") ]])
            fnCutscene([[ Append("Lydie: Always the hero![P] You couldn't hurt a fly![B][C]") ]])
            fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Blush] That's why we have dreams![B][C]") ]])
            fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Neutral] Have you ever had a dream like that?[B][C]") ]])
            fnCutscene([[ Append("Lydie: Oh, of course.[P] I once dreamt I was a researcher, looking into monster species.[P] I was writing about...[P] slimes, I think?[B][C]") ]])
            fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Neutral] Well that's much more fitting.[P] You're always reading stuff![B][C]") ]])
            fnCutscene([[ Append("Lydie: How come you don't?[P] I could teach you how to read, it's not hard.[B][C]") ]])
            fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Neutral] It's way too late for me.[B][C]") ]])
            fnCutscene([[ Append("Lydie: It's never too late.[B][C]") ]])
            fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Sad] I don't think the Countess would approve.[B][C]") ]])
            fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Surprise] Oh, jeez.[P] I should really clean my clothes![P] They're covered in sweat![B][C]") ]])
            fnCutscene([[ Append("Lydie: Look at this...[P] grey and gold?[P] Where'd you get this outfit?[B][C]") ]])
            fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Neutral] (Hmm, was I wearing this in my dream?[P] And...[P][EMOTION|Mei|Surprise] a sword?[P] Why was I carrying that?)[B][C]") ]])
            fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Smirk] I don't know![P] I must have borrowed it from someone.[B][C]") ]])
            fnCutscene([[ Append("Lydie: Come on, let's go clean it up before someone sees you.[P] The Countess said we've got to keep the manor spotless.") ]])
            fnCutsceneBlocker()
            
            --Flags.
            VM_SetVar("Root/Variables/Chapter1/Scenes/iCanLeaveRoomW", "N", 1.0)

            --Get Lydie's uniqueID. 
            EM_PushEntity("Lydie")
                local iLydieID = RE_GetID()
                TA_SetProperty("Clipping Flag", false)
                TA_SetProperty("Activation Script", "Null")
            DL_PopActiveObject()
            
            --Disable Lydie's collision and add her to Natalie's following list.
            giFollowersTotal = 1
            gsaFollowerNames = {"Lydie"}
            giaFollowerIDs = {iLydieID}
            AL_SetProperty("Follow Actor ID", iLydieID)
            
            --Move Natalie onto Lydie:
            fnAutoFoldParty()
            fnCutsceneBlocker()
        end
        
    -- |[Florentina]|
    elseif(sActorName == "Florentina") then
    
        --Variables:
        local iLydieLeftParty           = VM_GetVar("Root/Variables/Chapter1/Scenes/iLydieLeftParty", "N")
        local iFlorentinaKnowsAboutRune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")
        
        --Setup.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
        
        --Dialogue when Natalie is still a ghost:
        if(iLydieLeftParty == 0.0) then
            fnCutscene([[ Append("Houseplant:[E|Confused] Grrr![P] Mei![P] Unchain me![B][C]") ]])
            fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Neutral] The countess got some sort of wiggling houseplant.[P] How exotic![P] I wonder where she got that?[B][C]") ]])
            fnCutscene([[ Append("Houseplant:[E|Confused] I'm not a -[P] *glurg*[P] *ack*[B][C]") ]])
            fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Laugh] There you go, all watered up.[P] Weren't you a thirsty little one?[B][C]") ]])
            fnCutscene([[ Append("Houseplant:[E|Offended] Stop -[P] *glarg*[P] I AM NO HOUSE PLANT![B][C]") ]])
            fnCutscene([[ Append("Houseplant:[E|Confused] When I manage to get out of here...[B][C]") ]])
            fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Neutral] Countess said that talking to plants makes them grow faster.[P] But, I have work to do.[P] Talk to you later, little lady.") ]])

        --Rescue Florentina:
        else
        
            -- |[Dialogue]|
            fnCutscene([[ Append("Florentina:[E|Confused] Mei![P] Snap out of it and unchain me, damn it![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Relax.[P] I can't get the lock when you're wiggling so much.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Surprise] You're in there again?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Yeah.[P] I touched my runestone.[P] I'm fine now.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] Phew.[P] I hate being chained up![B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] So the runestone made you remember who you are?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Sad] Sort of.[P] I'm Natalie, too, but I think I just absorbed her memories.[P] Maybe her soul is in me now?[P] I don't know...[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] Natalie, huh?[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] Well at least you're not treating me like a houseplant.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Sorry.[P] Whatever the curse is, it -[P] does things to the way you see the world...[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] So you're fine, right?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Sad] ...[P] I'm dead, Florentina...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] But not fully?[P] The runestone...[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] You're only dead if you believe you are.[P] Stay motivated.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] I think the runestone is keeping me focused.[P] I can...[P] change back...[P] if I have to.[B][C]") ]])
            
            if(iFlorentinaKnowsAboutRune == 0.0) then
                fnCutscene([[ Append("Florentina:[E|Neutral] Oh really?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Blush] I can show you when we get to a safe spot.[P] I can feel it...[B][C]") ]])
            end
            fnCutscene([[ Append("Mei:[E|Sad] But I don't want to leave Natalie behind...[P] the poor girl didn't even get to see what life could really be...[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] She's dead.[P] She's at peace.[P] Let it lie.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] But if her soul is within me...[P] We should see if we can help the other maids.[P] They were my friends.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] Ugh, ghostbusting...[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] I don't think I can talk you out of it, eh?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Laugh] Not a chance!") ]])
            fnCutsceneBlocker()

            --Wait a bit.
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            -- |[Movement]|
            --Florentina moves on to Mei.
            Cutscene_CreateEvent("ActorEvent", "Actor")
                ActorEvent_SetProperty("Subject Name", "Mei")
                ActorEvent_SetProperty("Move To", (6.25 * gciSizePerTile), (6.50 * gciSizePerTile))
            DL_PopActiveObject()
            Cutscene_CreateEvent("ActorEvent", "Actor")
                ActorEvent_SetProperty("Subject Name", "Florentina")
                ActorEvent_SetProperty("Move To", (6.25 * gciSizePerTile), (6.50 * gciSizePerTile))
            DL_PopActiveObject()
            fnCutsceneBlocker()
            
            --Fold the party.
            fnCutscene([[ AL_SetProperty("Fold Party") ]])
            fnCutsceneBlocker()
            
            --Un-crouch Florentina.
            EM_PushEntity("Florentina")
                TA_SetProperty("Set Special Frame", "Null")
                TA_SetProperty("Activation Script", "Null")
            DL_PopActiveObject()
            
            -- |[System]|
            --Re-add Florentina to the lineup.
            fnAddPartyMember("Florentina")
            
            --Modify the flags. Ghost TF is off.
            VM_SetVar("Root/Variables/Chapter1/Scenes/iIsGhostTF", "N", 0.0)

        end
    end
end
