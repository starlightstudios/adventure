-- |[Constructor]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "QuantirManseCentralW"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "QuantirManseThemeLow")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
    
    -- |[Map Handling]|
    --Resolve the map's variable.
    fnStandardCh1Map(sLevelName)

	--During the ghost TF, spawn this NPC.
	local iIsGhostTF = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsGhostTF", "N")
	if(iIsGhostTF == 1.0) then
        fnStandardNPCByPosition("Lydie")
        AL_SetProperty("Block Autosave Once")
	end

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then
	
	--During the ghost TF, spawn Florentina here if she's in the party and hasn't spawned yet.
	local iIsGhostTF = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsGhostTF", "N")
	local iHasFlorentina = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
	if(iIsGhostTF == 1.0 and iHasFlorentina == 1.0 and EM_Exists("Florentina") == false) then
		fnSpecialCharacter("Florentina", 6, 5, gci_Face_South, fnResolvePath() .. "Dialogue.lua")
		EM_PushEntity("Florentina")
			TA_SetProperty("Set Special Frame", "Crouch")
            TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
		DL_PopActiveObject()
	end
end
