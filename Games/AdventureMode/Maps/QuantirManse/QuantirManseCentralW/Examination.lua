-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Variables.
local iIsGhostTF        = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsGhostTF", "N")
local iPutMaidOutfitOn  = VM_GetVar("Root/Variables/Chapter1/Scenes/iPutMaidOutfitOn", "N")
local iCleaningProgress = VM_GetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N")

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
--Western Bookshelves.
if(sObjectName == "BookshelvesWA") then
	
	--Variables
	local iCleanedBookshelvesW = VM_GetVar("Root/Variables/Chapter1/Scenes/iCleanedBookshelvesW", "N")
	
	--Normal examination:
	if(iIsGhostTF == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (A book about a fire elemental who finds love with a water elemental.[P] It's called 'Steamy Times'.)") ]])
        fnCutsceneBlocker()
	
	--Maid TF, but the uniform isn't on:
	elseif(iPutMaidOutfitOn == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Better put the uniform on before I start cleaning!)") ]])
        fnCutsceneBlocker()
	
	--Maid TF, uniform is on, and the bookshelves haven't been cleaned yet:
	elseif(iCleanedBookshelvesW == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N", iCleaningProgress + 1)
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedBookshelvesW", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (So much dust on these shelves!) [B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (...) [B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (All right, that's done!) ") ]])
        fnCutsceneBlocker()
	
	--Maid TF, uniform is on, and the bookshelves are clean:
	else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (These shelves are already dusted.)") ]])
        fnCutsceneBlocker()
	end
--Western Bookshelves.
elseif(sObjectName == "BookshelvesWB") then
	
	--Variables
	local iCleanedBookshelvesW = VM_GetVar("Root/Variables/Chapter1/Scenes/iCleanedBookshelvesW", "N")
	
	--Normal examination:
	if(iIsGhostTF == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('Froh Bibble'.[P] The whole book is incomprehensible gibberish.)") ]])
        fnCutsceneBlocker()
	
	--Maid TF, but the uniform isn't on:
	elseif(iPutMaidOutfitOn == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Better put the uniform on before I start cleaning!)") ]])
        fnCutsceneBlocker()
	
	--Maid TF, uniform is on, and the bookshelves haven't been cleaned yet:
	elseif(iCleanedBookshelvesW == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N", iCleaningProgress + 1)
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedBookshelvesW", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (So much dust on these shelves!) [B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (...) [B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (All right, that's done!) ") ]])
        fnCutsceneBlocker()
	
	--Maid TF, uniform is on, and the bookshelves are clean:
	else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (These shelves are already dusted.)") ]])
        fnCutsceneBlocker()
	end

--Eastern Bookshelves.
elseif(sObjectName == "BookshelvesEA") then
	
	--Variables
	local iCleanedBookshelvesE = VM_GetVar("Root/Variables/Chapter1/Scenes/iCleanedBookshelvesE", "N")
	
	--Normal examination:
	if(iIsGhostTF == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Ancient books about romance, legends, and manners.[P] Nothing interesting.)") ]])
        fnCutsceneBlocker()
	
	--Maid TF, but the uniform isn't on:
	elseif(iPutMaidOutfitOn == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Better put the uniform on before I start cleaning!)") ]])
        fnCutsceneBlocker()
	
	--Maid TF, uniform is on, and the bookshelves haven't been cleaned yet:
	elseif(iCleanedBookshelvesE == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N", iCleaningProgress + 1)
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedBookshelvesE", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (So much dust on these shelves and books!) [B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (...) [B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (All right, that's done!) ") ]])
        fnCutsceneBlocker()
	
	--Maid TF, uniform is on, and the bookshelves are clean:
	else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (These shelves are already dusted.)") ]])
        fnCutsceneBlocker()
	end

--Eastern Bookshelves.
elseif(sObjectName == "BookshelvesEB") then
	
	--Variables
	local iCleanedBookshelvesE = VM_GetVar("Root/Variables/Chapter1/Scenes/iCleanedBookshelvesE", "N")
	
	--Normal examination:
	if(iIsGhostTF == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (A book about a dog who gets lost and finds his way home.[P] I love happy endings!)") ]])
        fnCutsceneBlocker()
	
	--Maid TF, but the uniform isn't on:
	elseif(iPutMaidOutfitOn == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Better put the uniform on before I start cleaning!)") ]])
        fnCutsceneBlocker()
	
	--Maid TF, uniform is on, and the bookshelves haven't been cleaned yet:
	elseif(iCleanedBookshelvesE == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N", iCleaningProgress + 1)
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedBookshelvesE", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (So much dust on these shelves and books!) [B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (...) [B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (All right, that's done!) ") ]])
        fnCutsceneBlocker()
	
	--Maid TF, uniform is on, and the bookshelves are clean:
	else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (These shelves are already dusted.)") ]])
        fnCutsceneBlocker()
	end

--Eastern Bookshelves.
elseif(sObjectName == "BookshelvesEC") then
	
	--Variables
	local iCleanedBookshelvesE = VM_GetVar("Root/Variables/Chapter1/Scenes/iCleanedBookshelvesE", "N")
	
	--Normal examination:
	if(iIsGhostTF == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('Modern Diagnosis and Treatment of Humourous Deficiencies'.[P] Not a treatise on mirth-through-suffering, but a discussion of precious bodily fluids.)") ]])
        fnCutsceneBlocker()
	
	--Maid TF, but the uniform isn't on:
	elseif(iPutMaidOutfitOn == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Better put the uniform on before I start cleaning!)") ]])
        fnCutsceneBlocker()
	
	--Maid TF, uniform is on, and the bookshelves haven't been cleaned yet:
	elseif(iCleanedBookshelvesE == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N", iCleaningProgress + 1)
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedBookshelvesE", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (So much dust on these shelves and books!) [B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (...) [B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (All right, that's done!) ") ]])
        fnCutsceneBlocker()
	
	--Maid TF, uniform is on, and the bookshelves are clean:
	else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (These shelves are already dusted.)") ]])
        fnCutsceneBlocker()
	end

--Eastern Bookshelves.
elseif(sObjectName == "BookshelvesED") then
	
	--Variables
	local iCleanedBookshelvesE = VM_GetVar("Root/Variables/Chapter1/Scenes/iCleanedBookshelvesE", "N")
	
	--Normal examination:
	if(iIsGhostTF == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('Drain The Lizard'.[P] This seems to be a guide on using iguana blood to cure sick people...)") ]])
        fnCutsceneBlocker()
	
	--Maid TF, but the uniform isn't on:
	elseif(iPutMaidOutfitOn == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Better put the uniform on before I start cleaning!)") ]])
        fnCutsceneBlocker()
	
	--Maid TF, uniform is on, and the bookshelves haven't been cleaned yet:
	elseif(iCleanedBookshelvesE == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N", iCleaningProgress + 1)
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedBookshelvesE", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (So much dust on these shelves and books!) [B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (...) [B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (All right, that's done!) ") ]])
        fnCutsceneBlocker()
	
	--Maid TF, uniform is on, and the bookshelves are clean:
	else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (These shelves are already dusted.)") ]])
        fnCutsceneBlocker()
	end

--Crates.
elseif(sObjectName == "Crates") then
	
	--In Ghost TF but haven't put on the uniform yet:
	if(iIsGhostTF == 1.0 and iPutMaidOutfitOn == 0.0) then

        --Clear censor bars.
        WD_SetProperty("Clear Censor Bars")

		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iPutMaidOutfitOn", "N", 1.0)
        
        --Load images.
        fnLoadDelayedBitmapsFromList("Chapter 1 Mei Ghost TF", gciDelayedLoadLoadAtEndOfTick)

		--Fade to black quickly.
		fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
		fnCutsceneBlocker()

		--Scene.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Activate Scene") ]])
		fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
		fnCutscene([[ Append("Natalie sorted through the contents of the crate, looking for the spare uniform.[P] In her panic, she didn't notice that it had materialized from nothing underneath an old bedsheet as she lifted it.") ]])
		fnCutsceneBlocker()
		
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
		fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
		fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/GhostTF0") ]])
		fnCutscene([[ Append("She picked it up with haste and checked it over.[P] There were no stains, nor wear.[P] It was immaculate, like it had never been used.[P] She undressed and quickly began putting the uniform on.[B][C]") ]])
		fnCutscene([[ Append("It fit poorly, but Natalie didn't have time to complain.[P] If the Countess caught her neglecting her duty, she'd be disciplined.[P] She needed to think of how to clean the room up, not how she looked.[B][C]") ]])
		fnCutscene([[ Append("Beneath her notice, her body slowly conformed to the size of the uniform.[P] Her hair grew in length as her skin grew translucent...") ]])
		fnCutsceneBlocker()
		
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
		fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/GhostTF0") ]])
		fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/GhostTF1") ]])
		fnCutscene([[ Append("She felt a mild discomfort, but shrugged it off.[P] The uniform was tighter than she would have liked.[P] It felt like it was constricting her neck and legs, but this was her fault -[P] she should have worn her uniform this morning![B][C]") ]])
		fnCutscene([[ Append("She felt a cold wind blow through her.[P] She felt odd, like she had lost something important. But, she didn't have time to ponder it.[P] The countess could be there at any moment!") ]])
		fnCutsceneBlocker()
		
		fnCutscene([[ WD_SetProperty("FastShow") ]])
		fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
		fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/GhostTF1") ]])
		fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Ghost") ]])
		fnCutscene([[ Append("As Natalie did one last check over the uniform, looking for anything out of place, her thoughts wandered yet again.[P] She thought back to what she had been daydreaming about.[B][C]") ]])
		fnCutscene([[ Append("That very pretty girl, Mei, fighting to find her way home.[P] It was so fantastical, but so vivid.[P] She decided to tell Lydie all about her dream later...[B][C]") ]])
		fnCutsceneBlocker()
		
		--Switch Natalie to ghost form.
		fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Ghost.lua") ]])
		fnCutsceneBlocker()
	
		--Mei temporarily changes name to Natalie.
		fnCutscene([[
		AdvCombat_SetProperty("Push Party Member", "Mei")
			AdvCombatEntity_SetProperty("Display Name", "Natalie")
		DL_PopActiveObject() ]])

		--Wait a bit.
		fnCutsceneWait(25)
		fnCutsceneBlocker()

		--Fade to black quickly.
		fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
		fnCutsceneBlocker()

		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ Append("Natalie:[EMOTION|Mei|Neutral][VOICE|Mei] (A little tight, but it'll have to do!)[B][C]") ]])
		fnCutscene([[ Append("Natalie:[EMOTION|Mei|Sad][VOICE|Mei] (Why is it so cold in here all of the sudden?)[B][C]") ]])
		fnCutscene([[ Append("Natalie:[EMOTION|Mei|Neutral][VOICE|Mei] (No time, gotta clean or the Countess will be furious!)") ]])
        
        --Unload.
        fnCutscene([[ fnUnloadBitmapsFromList("Chapter 1 Mei Ghost TF") ]])

	elseif(iIsGhostTF == 1.0 and iPutMaidOutfitOn == 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I'm already wearing my uniform!)") ]])
        fnCutsceneBlocker()
	end

--Bed.
elseif(sObjectName == "Bed") then
	
	--Variables
	local iMadeBed = VM_GetVar("Root/Variables/Chapter1/Scenes/iMadeBed", "N")
	
	--Normal examination:
	if(iIsGhostTF == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (A very nice bed, fit for two.)") ]])
        fnCutsceneBlocker()
	
	--Maid TF, but the uniform isn't on:
	elseif(iPutMaidOutfitOn == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Better put the uniform on before I start cleaning!)") ]])
        fnCutsceneBlocker()
	
	--Maid TF, uniform is on, and the job isn't done:
	elseif(iMadeBed == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N", iCleaningProgress + 1)
		VM_SetVar("Root/Variables/Chapter1/Scenes/iMadeBed", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (This bed could be made better...) [B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (...) [B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Nice and tidy![P] On to the next job!) ") ]])
        fnCutsceneBlocker()
	
	--Maid TF, uniform is on, and the job is done:
	else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The bed has already been made.)") ]])
        fnCutsceneBlocker()
	end

--Table West.
elseif(sObjectName == "TableW") then
	
	--Variables
	local iCleanedTableW = VM_GetVar("Root/Variables/Chapter1/Scenes/iCleanedTableW", "N")
	
	--Normal examination:
	if(iIsGhostTF == 0.0) then
	
	--Maid TF, but the uniform isn't on:
	elseif(iPutMaidOutfitOn == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Better put the uniform on before I start cleaning!)") ]])
        fnCutsceneBlocker()
	
	--Maid TF, uniform is on, and the job isn't done:
	elseif(iCleanedTableW == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N", iCleaningProgress + 1)
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedTableW", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (There's a lot of dust on this table...) [B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (...) [B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (That should do it!) ") ]])
        fnCutsceneBlocker()
	
	--Maid TF, uniform is on, and the job is done:
	else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The table has already been dusted.)") ]])
        fnCutsceneBlocker()
	end

--Table Center.
elseif(sObjectName == "TableC") then
	
	--Variables
	local iCleanedTableC = VM_GetVar("Root/Variables/Chapter1/Scenes/iCleanedTableC", "N")
	
	--Normal examination:
	if(iIsGhostTF == 0.0) then
	
	--Maid TF, but the uniform isn't on:
	elseif(iPutMaidOutfitOn == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Better put the uniform on before I start cleaning!)") ]])
        fnCutsceneBlocker()
	
	--Maid TF, uniform is on, and the job isn't done:
	elseif(iCleanedTableC == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N", iCleaningProgress + 1)
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedTableC", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (There's a lot of dust on this table...) [B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (...) [B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (That should do it!) ") ]])
        fnCutsceneBlocker()
	
	--Maid TF, uniform is on, and the job is done:
	else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The table has already been dusted.)") ]])
        fnCutsceneBlocker()
	end

--Table East.
elseif(sObjectName == "TableE") then
	
	--Variables
	local iCleanedTableE = VM_GetVar("Root/Variables/Chapter1/Scenes/iCleanedTableE", "N")
	
	--Normal examination:
	if(iIsGhostTF == 0.0) then
	
	--Maid TF, but the uniform isn't on:
	elseif(iPutMaidOutfitOn == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Better put the uniform on before I start cleaning!)") ]])
        fnCutsceneBlocker()
	
	--Maid TF, uniform is on, and the job isn't done:
	elseif(iCleanedTableE == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N", iCleaningProgress + 1)
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedTableE", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (There's a lot of dust on this table...) [B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (...) [B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (That should do it!) ") ]])
        fnCutsceneBlocker()
	
	--Maid TF, uniform is on, and the job is done:
	else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The table has already been dusted.)") ]])
        fnCutsceneBlocker()
	end

--Chair West.
elseif(sObjectName == "ChairW") then
	
	--Variables
	local iCleanedChairW = VM_GetVar("Root/Variables/Chapter1/Scenes/iCleanedChairW", "N")
	
	--Normal examination:
	if(iIsGhostTF == 0.0) then
	
	--Maid TF, but the uniform isn't on:
	elseif(iPutMaidOutfitOn == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Better put the uniform on before I start cleaning!)") ]])
        fnCutsceneBlocker()
	
	--Maid TF, uniform is on, and the job isn't done:
	elseif(iCleanedChairW == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N", iCleaningProgress + 1)
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedChairW", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (This chair is so dusty!) [B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (...) [B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (All cleaned!) ") ]])
        fnCutsceneBlocker()
	
	--Maid TF, uniform is on, and the job is done:
	else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The chair has already been cleaned up.)") ]])
        fnCutsceneBlocker()
	end

--Chair Center.
elseif(sObjectName == "ChairC") then
	
	--Variables
	local iCleanedChairC = VM_GetVar("Root/Variables/Chapter1/Scenes/iCleanedChairC", "N")
	
	--Normal examination:
	if(iIsGhostTF == 0.0) then
	
	--Maid TF, but the uniform isn't on:
	elseif(iPutMaidOutfitOn == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Better put the uniform on before I start cleaning!)") ]])
        fnCutsceneBlocker()
	
	--Maid TF, uniform is on, and the job isn't done:
	elseif(iCleanedChairC == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N", iCleaningProgress + 1)
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedChairC", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (This chair is so dusty!) [B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (...) [B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (All cleaned!) ") ]])
        fnCutsceneBlocker()
	
	--Maid TF, uniform is on, and the job is done:
	else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The chair has already been cleaned up.)") ]])
        fnCutsceneBlocker()
	end

--Chair East.
elseif(sObjectName == "ChairE") then
	
	--Variables
	local iCleanedChairE = VM_GetVar("Root/Variables/Chapter1/Scenes/iCleanedChairE", "N")
	
	--Normal examination:
	if(iIsGhostTF == 0.0) then
	
	--Maid TF, but the uniform isn't on:
	elseif(iPutMaidOutfitOn == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Better put the uniform on before I start cleaning!)") ]])
        fnCutsceneBlocker()
	
	--Maid TF, uniform is on, and the job isn't done:
	elseif(iCleanedChairE == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N", iCleaningProgress + 1)
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCleanedChairE", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (This chair is so dusty!)  [B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (...) [B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (All cleaned!) ") ]])
        fnCutsceneBlocker()
	
	--Maid TF, uniform is on, and the job is done:
	else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The chair has already been cleaned up.)") ]])
        fnCutsceneBlocker()
	end

-- |[Exits]|
elseif(sObjectName == "ExitDoor") then

	--Not ghost TF, or the cleaning job is done:
	local iCanLeaveRoomW = VM_GetVar("Root/Variables/Chapter1/Scenes/iCanLeaveRoomW", "N")
	if(iIsGhostTF == 0.0 or iCanLeaveRoomW == 1.0) then 
		AudioManager_PlaySound("World|FlipSwitch")
		AL_BeginTransitionTo("QuantirManseEntrance", "FORCEPOS:5.0x15.0x0")
	
	--Otherwise, stop Natalie from leaving, she needs to do cleaning:
	elseif(iCleaningProgress < 9.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (No slacking![P] I still need to clean the room up!)") ]])
        fnCutsceneBlocker()

	--Stop Natalie from leaving, she needs to talk to Lydie:
	else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I should talk to Lydie before I go wandering off...)") ]])
        fnCutsceneBlocker()
	end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end