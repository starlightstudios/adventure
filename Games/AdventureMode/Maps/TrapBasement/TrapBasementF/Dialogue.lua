-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Cultist A]|
    if(sActorName == "Cultist A") then
        WD_SetProperty("Show")
        Append("Cultist:[VOICE|CultistF] Oh crud, are you an intruder?[P] Whatever, I'm on my break.")
        
    -- |[Cultist B]|
    elseif(sActorName == "Cultist B") then
    
        --Variables.
        local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
    
        --Normal:
        if(sMeiForm ~= "Ghost") then
            WD_SetProperty("Show")
            Append("Cultist:[VOICE|CultistM] You don't have your chains yet, eh?[P] Careful, someone might think you're an outsider![P] Ha ha![P] *Hic*")
        else
            WD_SetProperty("Show")
            Append("Cultist:[VOICE|CultistM] Jeesum crow, if this was the length you went to get your chains, you went too far, buddy.")
        end
    end
end
