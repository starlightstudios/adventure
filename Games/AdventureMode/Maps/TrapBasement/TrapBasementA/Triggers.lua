-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Intro. Shows the introduction and the instruction screen.
if(sObjectName == "IntroLaunch") then

	-- |[Checks]|
	--Don't show this twice.
	local iShowIntroScene = VM_GetVar("Root/Variables/Chapter1/Scenes/Intro|iShowIntroScene", "N")
	if(iShowIntroScene == 0) then return end
	
	-- |[Flags]|
	--Set the flag to prevent playing this twice.
	VM_SetVar("Root/Variables/Chapter1/Scenes/Intro|iShowIntroScene", "N", 0.0)
    
    --Issue load instruction.
    fnLoadDelayedBitmapsFromList("Chapter 1 Introduction", gciDelayedLoadLoadImmediately)
		
	-- |[Setup]|
	--Immediately fade to fullblack. This prevents the player from seeing anything.
	Debug_DropEvents()
	AL_SetProperty("Activate Fade", 0, gci_Fade_Over_GUI_Blackout_World, true, 0, 0, 0, 1, 0, 0, 0, 1)
	
	--Set Mei to a crouch.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Special Frame", "Crouch")
	DL_PopActiveObject()
		
	--Wait a bit.
	fnCutscene([[ AL_SetProperty("Activate Fade", 60, gci_Fade_Over_GUI_Blackout_World, true, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneWait(30)
	fnCutsceneBlocker()
	
	-- |[Dialogue]|
    --Clear censor bars.
    WD_SetProperty("Clear Censor Bars")

	--Now, show the opening scene..
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
    fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Ch1Introduction/Introduction") ]])
	fnCutscene([[ Append("It was a hot, muggy evening in Hong Kong.[P] In a small restaurant, Mei Yonling worked diligently,[P] smiling for the customers,[P] joking with her co-workers,[P] and trying to distract her mind from the heat.[B][C]") ]])
	fnCutscene([[ Append("As the clock reached 11 PM, she bid a hasty goodbye to her co-workers as she fled from the thick air to catch her train.[B][C]") ]])
	fnCutscene([[ Append("The night offered little respite, and she tugged at her uniform as the thin fabric clung to her sweat-dampened skin.[P] The clinging fabric had been more revealing than she had desired, an effect noticed by no small number of her customers.[B][C]") ]])
	fnCutscene([[ Append("Resigning her efforts, she glanced around the dark streets.[P] Though intimidating to some, Mei felt calm and confident.[P] She knew the route well, and trusted in her intuition and skills.[B][C]") ]])
	fnCutscene([[ Append("Her thoughts drifted over the day as she walked, remembering conversations, jokes, and even the complaints of her snobbish manager, until a distant train whistle demanded her attention.[B][C]") ]])
	fnCutscene([[ Append("Looking at a distant street clock, she realized she was running late.[P] The heat and humidity had been draining, and her feet had not been as swift as they usually were.[B][C]") ]])
	fnCutscene([[ Append("'No worries,'[P] she thought to herself.[P] There was an alley she could cut through to save some time.[P] However dirty it usually was, it was a better option than a long walk home. [B][C]") ]])
	fnCutscene([[ Append("Turning down the alley, she began to run as the beckoning whistles of the trains rose above the endless din of Hong Kong's nightlife.[B][C]") ]])
	fnCutscene([[ Append("So focused was she on the cluttered alley that she did not notice a pale grey glow emanating from somewhere near her.[B][C]") ]])
	fnCutscene([[ Append("It was not until the glow had practically overwhelmed her that she took notice of it.[P] It increased in intensity, soon blotting out the streetlights, forcing her to stop as the brightness burned into her night-adjusted vision.[B][C]") ]])
	fnCutscene([[ Append("A strange curiosity began to overcome her, first enveloping,[P] then dispersing a brief thought of panic.[P] Somehow, she knew not to be afraid.[B][C]") ]])
	fnCutscene([[ Append("Then, she lost consciousness...") ]])
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(180)
	fnCutsceneBlocker()
	
	--Activate music.
	fnCutscene([[ AL_SetProperty("Music", "TheyKnowWeAreHere") ]])
	
	--Deactivate the overlay.
	fnCutscene([[ AL_SetProperty("Activate Fade", 60, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	
	--Next part.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
	fnCutscene([[ Append("The glow began to subside, and she stirred to awareness again.[P] No longer in the alley, she lay in a stuffy, mold-covered stone room.[B][C]") ]])
	fnCutscene([[ Append("She did not know how much time had passed, or where she was.[B][C]") ]])
	fnCutscene([[ Append("As she stood, a small object fell from her grasp.[P] Picking it up, she looked at a small disc of smooth stone.[P] On its face was a symbol that felt as familiar as it did foreign...") ]])
	fnCutsceneBlocker()
	
	-- |[Dialogue]|
	fnStandardMajorDialogue()
	fnCutscene([[ Append("Mei:[E|Sad] W-[P]What is this?[P] More importantly, where am I?[B][C]") ]])
	fnCutscene([[ Append("Mei: ...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Guess I'm not catching the train tonight.[P] If it's still tonight.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] Was I kidnapped?[P] I guess that's what I get for taking a shortcut.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Sad] *Sigh*[P] Tomorrow was my day off, too.[P] Now I have to spend it filling out police reports.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] That's once I get out of here, of course.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] Enough mucking about.[P] Kidnappers, you picked the wrong girl to mess with!") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ AL_SetProperty("Activate Fade", 60, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])

    --Clean images.
    fnCutscene([[ fnUnloadBitmapsFromList("Chapter 1 Introduction") ]])
    
	-- |[Finish Up]|
	--Clear flags.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Special Frame", "Null")
	DL_PopActiveObject()

--Trigger that activates when the game starts. Mei sees the cultist and decides not to go that way.
elseif(sObjectName == "SpotCultistZone") then
	
	-- |[Checks]|
	--If the player has already seen this, don't do it again.
	local iHearPlea = VM_GetVar("Root/Variables/Chapter1/Scenes/Intro|iHearPlea", "N")
	if(iHearPlea == 0.0) then return end
	
	-- |[Flags]|
	--Don't play this scene twice.
	VM_SetVar("Root/Variables/Chapter1/Scenes/Intro|iHearPlea", "N", 0.0)
	
	-- |[Movement]|
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	-- |[Camera]|
	--Pan the camera to show the cultist.
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Focus Position", 8.0 * gciSizePerTile, 10.0 * gciSizePerTile)
	DL_PopActiveObject()
	fnCutsceneBlocker()

	--Wait a bit.
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--Pan back.
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Focus Actor Name", "Mei")
	DL_PopActiveObject()
	fnCutsceneBlocker()

	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	-- |[Dialogue]|
	--Setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Offended") ]])

	--Talking.
	fnCutscene([[ Append("Mei: (Oh geez...)[B][C]") ]])
	fnCutscene([[ Append("Mei: (She's got a knife, and sure doesn't look like a friendly face...)") ]])
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	-- |[Dialogue]|
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Non-major sequence dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Voice:[VOICE|Aquillia] H-help...") ]])
	fnCutsceneBlocker()
	
	-- |[Movement]|
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Mei faces north.
	Cutscene_CreateEvent("Event", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Face", -1.0, -1.0)
	DL_PopActiveObject()
	
	-- |[Dialogue]|
	--Setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])

	--Talking.
	fnCutscene([[ Append("Mei: (I heard someone...)") ]])
	fnCutsceneWait(15)
	fnCutsceneBlocker()

--Game start, if Mei doesn't have the Rusty Katana yet, she doesn't go forward.
elseif(sObjectName == "WalkbackZone") then
	
	-- |[Checks]|
	--None of these scenes play if the intro scene is over.
	local iFoughtCultist = VM_GetVar("Root/Variables/Chapter1/Scenes/Intro|iFoughtCultist", "N")
	if(iFoughtCultist == 1.0) then return end
	
	--If Mei has the Rusty Katana and has equipped it, don't play any scenes.
	local iFirstTalkToPrisoner = VM_GetVar("Root/Variables/Chapter1/Scenes/Intro|iFirstTalkToPrisoner", "N")
	local bMeiHasWeaponEquipped = AdInv_GetProperty("Is Any Item Equipped In", "Mei", "Weapon")
    
    --Debug:
	if(iFirstTalkToPrisoner == 1.0) then
	
		-- |[Dialogue]|
		--Setup.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])

		--Talking.
		fnCutscene([[ Append("Mei: (I think that voice came from up there...)") ]])
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		
		-- |[Movement]|
		--Wait a bit.
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Mei walks east.
		Cutscene_CreateEvent("Event", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move Amount", 1.0 * gciSizePerTile, 0.0)
		DL_PopActiveObject()
	
	--Mei has the Rusty Katana but didn't equip it.
	elseif(bMeiHasWeaponEquipped == false) then
	
		-- |[Dialogue]|
		--Setup.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])

		--Talking.
		fnCutscene([[ Append("Mei: (Might be a good idea to equip that sword before I try to rush someone...)[B][C]") ]])
		fnCutscene([[ Append("*Press X to bring up the menu. Select Equip with Z and the Arrow Keys, and put your sword to use!*") ]])
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		
		-- |[Movement]|
		--Wait a bit.
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Mei walks east.
		Cutscene_CreateEvent("Event", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move Amount", 1.0 * gciSizePerTile, 0.0)
		DL_PopActiveObject()
	
	end

--Game start. Engage the cultist!
elseif(sObjectName == "FightCultistZone") then

	-- |[Checks]|
	--Don't play this scene if it's not the intro.
	local iFoughtCultist = VM_GetVar("Root/Variables/Chapter1/Scenes/Intro|iFoughtCultist", "N")
	if(iFoughtCultist == 1.0) then return end
		
	-- |[Flags]|
	VM_SetVar("Root/Variables/Chapter1/Scenes/Intro|iFoughtCultist", "N", 1.0)
	
	-- |[Dialogue]|
	--Setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Angry") ]])

	--Talking.
	fnCutscene([[ Append("Mei: Hey![P] Kidnapper!") ]])
	fnCutsceneWait(15)
	fnCutsceneBlocker()
		
	-- |[Movement]|
	--Wait a bit.
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Cultist faces Mei.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Cultist")
		ActorEvent_SetProperty("Face", 1.0, 0.0)
	DL_PopActiveObject()
	
	--Wait a bit.
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	-- |[Dialogue]|
	--Setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Angry") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "CultistF", "Neutral") ]])
	--Talking.
	fnCutscene([[ Append("Cultist: I-[P]intruder![P] Stop the intruder![B][C]") ]])
	fnCutscene([[ Append("Mei: Intrude this!") ]])
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	-- |[Movement]|
	--Meu runs towards Cultist.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", 10.25 * gciSizePerTile, 10.50 * gciSizePerTile, 2.25)
	DL_PopActiveObject()
	--Cultist runs towards Mei.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Cultist")
		ActorEvent_SetProperty("Move To", 9.25 * gciSizePerTile, 10.50 * gciSizePerTile, 2.25)
	DL_PopActiveObject()
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	-- |[Battle]|
	--Trigger a fight here. The battle does not begin until the "Activate" call executes, which is in a queue. All other commands
    -- are performed at the start of the script.
	AdvCombat_SetProperty("Reinitialize")
	AdvCombat_SetProperty("Unloseable", true)
	AdvCombat_SetProperty("Unsurrenderable", true)
	AdvCombat_SetProperty("Unretreatable", true)
	AdvCombat_SetProperty("Victory Script", fnResolvePath() .. "Combat_Victory.lua")
	LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/000 Scripted Enemies/Cultist Tutorial.lua", 0)
	fnCutscene([[ AdvCombat_SetProperty("Activate") ]])
	fnCutscene([[ AdvCombat_SetProperty("World Pulse", true) ]])
	fnCutsceneBlocker()
	
end
