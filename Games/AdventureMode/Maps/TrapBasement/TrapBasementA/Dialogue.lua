-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Cultist]|
    if(sActorName == "Cultist") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Cultist:[VOICE|CultistF] Oww...") ]])
    
    -- |[Aquillia]|
    elseif(sActorName == "Aquillia") then
	
        -- |[Setup]|
        --This is always a major dialogue.
        fnStandardMajorDialogue()
        
        --Prisoner takes slot 5.
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Aquillia", "Neutral") ]])
        
        -- |[Starting]|
        --The first time Mei speaks to the prisoner, we get a longer scene that clears some flags.
        local iFirstTalkToPrisoner = VM_GetVar("Root/Variables/Chapter1/Scenes/Intro|iFirstTalkToPrisoner", "N")
        local iFoughtCultist       = VM_GetVar("Root/Variables/Chapter1/Scenes/Intro|iFoughtCultist", "N")
        if(iFirstTalkToPrisoner == 1.0) then
            
            -- |[Flags]|
            VM_SetVar("Root/Variables/Chapter1/Scenes/Intro|iFirstTalkToPrisoner", "N", 0.0)
        
            -- |[Dialogue]|
            fnCutscene([[ Append("Prisoner: Help...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Surprise] Hey hey, stay with me here.[P] What happened, where are we?[B][C]") ]])
            fnCutscene([[ Append("Prisoner: They grabbed me, beat me up, tossed me in here...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] Who's they?[B][C]") ]])
            fnCutscene([[ Append("Prisoner: Don't know.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] All right, sit tight.[P] I'll try to find a way out.[B][C]") ]])
            fnCutscene([[ Append("Prisoner: Wait.[P] I found this.[P] Probably left by a previous occupant.[B][C]") ]])
            fnCutscene([[ Append("Prisoner: My arm's broken, but maybe you can use it.[B][C]") ]])
            fnCutscene([[ Append("[SOUND|World|TakeItem]*Received Rusty Katana*[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Surprise] A sword?[P] You want me to...[B][C]") ]])
            fnCutscene([[ Append("Prisoner: They won't just let us go, you know.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] (It always comes back to violence, doesn't it?)[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] (Stay strong, you can do this!)[B][C]") ]])
            fnCutscene([[ Append("Prisoner: Aim for their eyes...[P] they really hate that...") ]])
            fnCutsceneBlocker()
            
            -- |[Item]|
            --Give Mei a sword.
            LM_ExecuteScript(gsItemListing, "Rusty Katana")
        
        -- |[Second Time]|
        --The prisoner changes her dialogue after you talk to her.
        elseif(iFoughtCultist == 0.0) then
        
            -- |[Dialogue]|
            fnCutscene([[ Append("Prisoner: I'll...[P] I'll be fine.[P] Go on without me...") ]])

        -- |[Beaten Cultist]|
        --If you talk to the Prisoner after defeating the cultist...
        else
        
            -- |[Dialogue]|
            fnCutscene([[ Append("Prisoner: Go ahead, I'll catch up in a minute.") ]])

        end
    end
end
