-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Examinations]|
--Book on Philosophy.
if(sObjectName == "Philosophy") then
	fnStandardDialogue("[VOICE|Mei](\"The Writings of Osborne to Theodasus\".[P] Seems to be a book on philosophy, but I've never heard of any of these people.)")
	
--Book on Worship.
elseif(sObjectName == "Worship") then
	fnStandardDialogue("[VOICE|Mei](Hmm, all of these books are rules and rituals.[P] Sacrifice comes up a lot...)")
	
--Assorted boxes.
elseif(sObjectName == "Boxes") then
	fnStandardDialogue("[VOICE|Mei](Boxes containing mostly clothes, some jewelry and the like.[P] I guess the cultists left their old stuff in here?)")

--Locked door, can't enter that room.
elseif(sObjectName == "LockedDoor") then
	fnStandardDialogue("[VOICE|Mei](Locked.)")
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
