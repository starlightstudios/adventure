-- |[ ===================================== Field Abilities ==================================== ]|
--Called when a field ability is used, at the discretion of the field ability script. The variable
-- gbFieldAbilityHandledInput should be set to true if the script handled the ability.

-- |[Arguments]|
--Argument Listing:
-- 0: iSwitchCode - Switch code used by the ability. One of the gciFieldAbility_Activate_[X] series.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local iSwitchCode = tonumber(LM_GetScriptArgument(0))

-- |[ ======================================== Handling ======================================== ]|
--Picking open the sister-superior's door.
if(iSwitchCode == gciFieldAbility_Activate_Florentina_PickLock) then
    
    --Shed door already open.
    if(AL_GetProperty("Is Door Open", "DoorB") == true) then return end 
    
    --Get the party leader's position.
    EM_PushEntity(gsPartyLeaderName)
        local iPartyX, iPartyY = TA_GetProperty("Position")
        local iFacing = TA_GetProperty("Facing")
    DL_PopActiveObject()
    
    --Turn to radians.
    local fRadians = (iFacing-2) * 45 * 3.1415926 / 180.0
    
    --Modify by rotation.
    iPartyX = iPartyX + (math.cos(fRadians) * 8.0) - 4
    iPartyY = iPartyY + (math.sin(fRadians) * 8.0) - 8
    
    --List of locations to check. These are in an X arrangement around the center
    -- point, to allow some overlap in case of narrow miss.
    local iaOffX = {0, -4, 4, -4, 4, 12, 12, -12, -12, -4,  4,  -4,   4}
    local iaOffY = {0, -4, -4, 4, 4, -4,  4,  -4,   4, 12, 12, -12, -12}
    
    --Iterate across the offsets, but stop when a hit is registered.
    local bGotHit = false
    for i = 1, #iaOffX, 1 do
        
        --Build, get total.
        AL_GetProperty("Build Objects At Position", iPartyX + iaOffX[i], iPartyY + iaOffY[i])
        local iTotalHits = AL_GetProperty("Total Objects At Position")
        
        --Check all hits.
        for p = 0, iTotalHits-1, 1 do
            
            --Get variables.
            local iType = AL_GetProperty("Type Of Object At Position", p)
            local sName = AL_GetProperty("Name Of Object At Position", p)
            
            --If we hit the shed door
            if(sName == "DoorB") then
                bGotHit = true
                break
            end
        end
        
        --Got a hit, break out.
        if(bGotHit) then break end
    end
    
    --Handle if we got a hit!
    if(bGotHit == true) then
        
        --Flag.
        gbFieldAbilityHandledInput = true
        
        --Check if Florentina is around.
        local bHasFlorentina = false
        for i = 1, #gsaFollowerNames, 1 do
            if(gsaFollowerNames[i] == "Florentina") then
                bHasFlorentina = true
                break
            end
        end
        
        --Not around:
        if(bHasFlorentina == false) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (Florentina can't pick the lock if she's not here!)") ]])
            fnCutsceneBlocker()
            return
        end
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter1/Scenes/iTriedToPickSisterSuperiorDoor", "N", 1.0)
        
        --Lock time!
        fnCutsceneMove("Mei", 15.25, 15.50)
        fnCutsceneFace("Mei", 1, 0)
        fnCutsceneMove("Florentina", 16.25, 15.50)
        fnCutsceneFace("Florentina", 0, -1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Florentina] This won't be a problem...[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Florentina] Huh...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Florentina] I got the tumblers up, but the door isn't opening.[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Florentina] It's held in place by magic.[P] Not much I can do past this point.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Mei:[E|Neutral] I guess we're not getting in there, then.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] The magic activated while I was picking the lock.[P] Either she's paranoid, or she did it by reflex.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Confused] Either way, better not pick a fight with a mage like that.[P] She could go toe-to-toe with Polaris and come out on top...") ]])
        fnCutsceneBlocker()
        
        --Fold.
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnAutoFoldParty()
        fnCutsceneBlocker()
    end
end
