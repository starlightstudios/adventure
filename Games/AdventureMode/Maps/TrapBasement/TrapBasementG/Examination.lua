-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Examinations]|
--This door is locked tight until Chapter 6.
if(sObjectName == "TightLocked") then
	fnStandardDialogue("[VOICE|Mei](This door doesn't seem to open at all.[P] Seems to be welded shut, and I can feel some sort of force keeping me back...)")

--No entry!
elseif(sObjectName == "NoEntry") then
	fnStandardDialogue("[VOICE|Mei](\"No entry without explicit authorization from the Sister Superior.[P] THIS MEANS YOU.\")")
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
