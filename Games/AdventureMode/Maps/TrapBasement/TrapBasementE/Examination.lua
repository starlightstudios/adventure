-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToBasementC") then
	AudioManager_PlaySound("World|FlipSwitch")
	fnCutscene([[ AL_BeginTransitionTo("TrapBasementC", "FORCEPOS:4.0x52.0x0") ]])
	fnCutsceneBlocker()
	
-- |[Examinations]|
--Shelves with food on them.
elseif(sObjectName == "FoodShelves") then
	fnStandardDialogue("[VOICE|Mei](Breads.[P] Hard and crumbly, the opposite of appetizing.)")

--What?
elseif(sObjectName == "BagOfMarbles") then
	fnStandardDialogue("[VOICE|Mei](Strangely, this bag is full of marbles.)")
	
--Sandbags!
elseif(sObjectName == "Sand") then
	fnStandardDialogue("[VOICE|Mei](This bag is full of sand.[P] Just sand.)")
	
--Empty, nothing useful.
elseif(sObjectName == "Empty") then
	fnStandardDialogue("[VOICE|Mei](Nothing useful here.)")
	
--Contains a tincture!
elseif(sObjectName == "TinctureBarrel") then

	--If the player has not found the tincture yet...
	local iFoundTinctureA = VM_GetVar("Root/Variables/Chapter1/WorldState/iFoundTinctureA", "N")
	if(iFoundTinctureA == 0.0) then
		LM_ExecuteScript(gsItemListing, "Healing Tincture")
		VM_SetVar("Root/Variables/Chapter1/WorldState/iFoundTinctureA", "N", 1.0)
		fnStandardDialogue("[VOICE|Mei]([SOUND|World|TakeItem]Found a Healing Tincture in the barrel!)")
	else
		fnStandardDialogue("[VOICE|Mei](Nothing useful here.)")
	end

--Note for the Sister Superior's room.
elseif(sObjectName == "SisterSuperiorNote") then
	fnStandardDialogue("[VOICE|Mei](\"Initiates, interrupt the Sister Superior at your peril...\")")

--Break time.
elseif(sObjectName == "BreakRoom") then
	fnStandardDialogue("[VOICE|Mei](\"Break room.\")")

--Someone help!
elseif(sObjectName == "DesperateNote") then
	fnStandardDialogue("[VOICE|Mei](\"Help, I lost my glasses![P] If anyone has seen them, help![P]\" The handwriting is understandably pretty bad...)")
	
--Note for the acolyte's barracks.
elseif(sObjectName == "AcolytesNote") then
	fnStandardDialogue("[VOICE|Mei](\"Adepts' barracks.\")")

--Warning!
elseif(sObjectName == "WarningNote") then
	fnStandardDialogue("[VOICE|Mei](\"Initiates caught eating past their allocated rations will be punished with latrine duty![P]\" Who would want to steal this stale crap?)")

--Tells a bit about tougher enemies.
elseif(sObjectName == "ToughEnemiesNote") then
	fnStandardDialogue("Thought: [VOICE|Leader](\"Initiates::[P] Creatures surrounded by a glowing aura are more dangerous than normal creatures.[P] Exercise caution.\")")

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
