-- |[ ==================================== Debug Shop Setup ==================================== ]|
--Debug shop. Sells a bunch of items for testing.

-- |[Gemcutter Flag]|
AM_SetShopProperty("Allow Gemcutting")

-- |[Item Stock Listing]|
--Adamantite
AM_SetShopProperty("Add Item", "Pepper Pie", -1, -1)
AM_SetShopProperty("Add Item", "Poison Spore Cap", -1, -1)
AM_SetShopProperty("Add Item", "Adamantite Powder", -1, -1)
AM_SetShopProperty("Add Item", "Adamantite Flakes", -1, -1)

--Weapons
AM_SetShopProperty("Add Item", "Rusty Katana",          0, -1)
AM_SetShopProperty("Add Item", "Serrated Katana",       0, -1)
AM_SetShopProperty("Add Item", "Wildflower's Katana",   0, -1)
AM_SetShopProperty("Add Item", "Hunting Knife",         0, -1)
AM_SetShopProperty("Add Item", "Butterfly Knife",       0, -1)
AM_SetShopProperty("Add Item", "Haggler's Backup Plan", 0, -1)

--Armors
AM_SetShopProperty("Add Item", "Mei's Work Uniform",        0, -1)
AM_SetShopProperty("Add Item", "Tiger Dancer's Dress",      0, -1)
AM_SetShopProperty("Add Item", "Flowery Tunic",             0, -1)
AM_SetShopProperty("Add Item", "Merchant's Tunic",          0, -1)
AM_SetShopProperty("Add Item", "Regalia of a Smoothtalker", 0, -1)
AM_SetShopProperty("Add Item", "Troubadour's Robe",         0, -1)
AM_SetShopProperty("Add Item", "Traveller's Vest",          0, -1)

--Accessories
AM_SetShopProperty("Add Item", "Sphalite Ring",     0, -1)
AM_SetShopProperty("Add Item", "Decorative Bracer", 0, -1)
AM_SetShopProperty("Add Item", "Jade Eye Ring",     0, -1)

--Gems
AM_SetShopProperty("Add Item", "Glintsteel Gem", 0, -1)
AM_SetShopProperty("Add Item", "Yemite Gem",     0, -1)
AM_SetShopProperty("Add Item", "Ardrion Gem",    0, -1)
AM_SetShopProperty("Add Item", "Rubose Gem",     0, -1)
AM_SetShopProperty("Add Item", "Blurleen Gem",   0, -1)
AM_SetShopProperty("Add Item", "Qederphage Gem", 0, -1)
