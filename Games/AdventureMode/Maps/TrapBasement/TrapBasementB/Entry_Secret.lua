-- |[Entrance Script]|
--Plays a brief cutscene if Mei arrives here from the secret passage for the first time.
local iEnteredSecretPassage = VM_GetVar("Root/Variables/Chapter1/Scenes/iEnteredSecretPassage", "N")

--Play a brief scene.
if(iEnteredSecretPassage == 0.0) then
	
	--Set the flag.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iEnteredSecretPassage", "N", 1.0)

	--Setup.
	fnStandardMajorDialogue()
	fnCutscene([[ Append("Mei:[E|Surprise] ..![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] There's a secret passage that leads back here?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I hope the cult doesn't know about it...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Didn't see any in the underground lake, so I hope not...") ]])
	fnCutsceneBlocker()

end
