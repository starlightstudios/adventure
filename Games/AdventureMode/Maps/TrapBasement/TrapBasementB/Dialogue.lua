-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = "None"
sActorName = TA_GetProperty("Name")

--Dialogue opening.
if(sTopicName == "Hello") then

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
        
    -- |[Cultist Door Keeper]|
    if(sActorName == "DoorCultist") then
        
        --Variables.
        local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
        
        --Dialogue.
        fnStandardMajorDialogue()
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "CultistF", "Neutral") ]])

        --If Mei is nonhuman:
        local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
        if(sMeiForm ~= "Human") then
            fnCutscene([[ Append("Cultist: Hey![P] You're Tess, right?[P] Special mission and all that?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Y-[P]yeah![B][C]") ]])
            fnCutscene([[ Append("Cultist: That's a nice disguise![P] You must be really good at being a spy![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Laugh] Thanks![P] I -[P] made it myself?[P] Sort of?[B][C]") ]])
            
            --Florentina is not present:
            if(bIsFlorentinaPresent == false) then
                fnCutscene([[ Append("Cultist: I hope it's going well![P] Good luck!") ]])
            
            --Florentina is present:
            else
                fnCutscene([[ Append("Cultist: And you even have a prisoner already![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Laugh] T-[P]totally![P] I...[P] tamed...[P] this Alraune![B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Offended] TAMED!?[B][C]") ]])
                fnCutscene([[ Append("Cultist: Great work, Tess![P] The Sister Superior will be so proud!") ]])
            end
        
        --If Mei is a human:
        else

            --Florentina is not present:
            if(bIsFlorentinaPresent == false) then
                fnCutscene([[ Append("Cultist: Hey![P] You're not supposed to take off your robes![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] W-[P]wait, I'm on a special mission![B][C]") ]])
                fnCutscene([[ Append("Cultist: Oh.[P] Okay.[P] Carry on then.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Happy] (I guess these guys will believe anything...)") ]])
            
            --Florentina is present:
            else
                fnCutscene([[ Append("Cultist: Hey![P] You're Tess, right?[P] Special mission and all that?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] T-[P]totally![P] Yep![P] I even captured and tamed an Alraune![B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Offended] TAMED!?[B][C]") ]])
                fnCutscene([[ Append("Cultist: Great work, Tess![P] The Sister Superior will be so proud!") ]])
            end
        end
    
    -- |[Debug]|
    elseif(sActorName == "HumanA") then
    
        --Set facing.
        TA_SetProperty("Face Character", "PlayerEntity")

        --Setup.
        local sBasePath = fnResolvePath()
        local sString = "AM_SetShopProperty(\"Show\", \"Doug Vendor\", \"" .. sBasePath .. "Shop Setup.lua\", \"" .. sBasePath .. "Shop Teardown.lua\")"

        --Run the shop.
        fnCutscene(sString)
        fnCutsceneBlocker()
	end
end
