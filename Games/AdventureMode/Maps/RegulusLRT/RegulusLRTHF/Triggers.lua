-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Discussion cutscene.
if(sObjectName == "DiscussionScene") then

	--Variables.
	local iSawPrisonDiscussion = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawPrisonDiscussion", "N")
	if(iSawPrisonDiscussion == 0.0) then
		
		--Flags.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawPrisonDiscussion", "N", 1.0)
		VM_SetVar("Root/Variables/Chapter5/Scenes/iCheckedSecurityConsole", "N", 0.0)
		
		--Movement.
		fnCutsceneMove("Christine", 17.25, 14.50)
		fnCutsceneMove("Tiffany", 17.25, 17.50)
		fnCutsceneBlocker()
		fnCutsceneWait(45)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Come on 55, we have someone important to talk to.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(45)
		fnCutsceneBlocker()
		
		--Movement.
		fnCutsceneFace("Christine", 0, 1)
		fnCutsceneBlocker()
		fnCutsceneWait(45)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
		fnCutscene([[ Append("55:[E|Neutral] Christine, do you know the levels of offenses here on Regulus?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] No.[P] No I don't.[P] Do you?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] The lowest level of offense results in reprimand.[P] The offending unit receives physical discipline and usually a loss of privileges.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] After that is rehabilitation.[P] A unit who commits a major offense is taken to an isolated facility and made to regret their offense.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Third is reprogramming, where a unit is forced to forget their past and is returned to the state they were in when they were first converted.[P] It is a very extreme punishment.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Fourth is repurposement.[P] You've seen the latex drones here.[P] They are what becomes of units who are dangerous even when reprogrammed...[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] And what comes next?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Retirement.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] 55, you're a different unit than you were back then.[P] We can make it up to 278101.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] No I'm not, and no we can't.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Rehabilitation involves -[P] beatings, electrocution, power starvation...[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] We torment the unit until they realize that obedience is preferable.[P] And then we keep tormenting them...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] You know this even with your memory wipe?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] This goes deeper than my active memories.[P] I did this so often, it would be like forgetting how to speak.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] 55, you're trying to become better.[P] That's important.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Is it?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Units are repurposed when they cannot change their behavior even when reprogrammed.[P] It's all-too-common a fate for disobedient units.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] How am I any different?[P] Why wouldn't I just do these awful things again?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Because you can choose to be better![P] You're not tied to your programming, you can overcome it and be who you want to be![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] The past is over.[P] It doesn't own you.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Now, come.[P] The first step is to make reparations with your past.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] ...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Fold the party.
		fnCutsceneMove("Tiffany", 17.25, 14.50)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
		
	end
	
--Party is forced to back up.
elseif(sObjectName == "BackUp") then

	--Variables.
	local iTalkedToWarden = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToWarden", "N")
	local iTalkedWithRehabGolem = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedWithRehabGolem", "N")
	if(iTalkedToWarden == 1.0 and iTalkedWithRehabGolem == 0.0) then
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine](We can't leave without talking to unit 278101...)") ]])
		fnCutsceneBlocker()
		
		--Move.
		fnCutsceneMove("Christine", 17.25, 27.50)
		fnCutsceneMove("Tiffany", 17.25, 27.50)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	end
end
