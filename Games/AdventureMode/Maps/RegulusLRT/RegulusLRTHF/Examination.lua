-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
--Exit.
if(sObjectName == "ToLRTHE") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusLRTHE", "FORCEPOS:48.0x18.0x0")

-- |[Objects]|
elseif(sObjectName == "PrisonDoor") then
	
	--Variables.
	local iTalkedToWarden = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToWarden", "N")
	
	--Hasn't spoken to the warden unit yet.
	if(iTalkedToWarden == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[SOUND|World|AutoDoorFail]Access denied.[P] Please receive authorization from the Warden Unit first.") ]])
		fnCutsceneBlocker()
	
	--Warden has opened the door.
	else
		AL_SetProperty("Open Door", "DoorB")
		AudioManager_PlaySound("World|AutoDoorOpen")

	end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
