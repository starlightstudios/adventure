-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[RehabGolem]|
    if(sActorName == "RehabGolem") then
	
        --First time.
        local iTalkedWithRehabGolem = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedWithRehabGolem", "N")
        if(iTalkedWithRehabGolem == 0.0) then
            
            --Flags.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedWithRehabGolem", "N", 1.0)
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutscene([[ Append("278101: Unit is a good unit.[P] Unit is an obedient unit...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Hello, Unit 278101...[B][C]") ]])
            fnCutscene([[ Append("278101: Hello, Unit 2855.[P] This unit is ready for further rehabilitation.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] There will be no need for that.[P] We're here to set you free.[B][C]") ]])
            fnCutscene([[ Append("278101: This unit is unsure of your intended meaning.[P] It is obedient and ready for further rehabilitation.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Unit 278101.[P] I am -[P] I am discharging you.[B][C]") ]])
            fnCutscene([[ Append("278101: Is this -[P] no, Unit does not question orders.[P] Unit will be discharged as intended.[P] Unit is obedient.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] Listen, 278101 -[P] do you have a secondary designation I can use?[B][C]") ]])
            fnCutscene([[ Append("278101: Unit 278101 has no secondary designation.[B][C]") ]])
            fnCutscene([[ Append("278101: But if you wanted to, you could call her Mina...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] Mina, I know you've been hurt here.[P] But, we're here to put an end to that.[P][EMOTION|Christine|Neutral] You're free now.[P] You can go back to your old life.[B][C]") ]])
            fnCutscene([[ Append("Mina: ...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] It's true.[P] There's no trick.[P] We mean it.[B][C]") ]])
            fnCutscene([[ Append("Mina: Really?[B][C]") ]])
            fnCutscene([[ Append("Mina: You're not doing Good Unit, Bad Unit?[P] I have nothing I can be interrogated about.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral]  And, Unit 2855 has something she'd like to say, as well.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] I do?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] *Say you're sorry!*[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] You will receive a full pardon, 278101.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] *Say it!*[B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] *But I don't even remember what I did!*[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] *Do you think that matters to her?*[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Mina -[P] I'm sorry.[P] For what I did to you.[B][C]") ]])
            fnCutscene([[ Append("Mina: No you're not.[B][C]") ]])
            fnCutscene([[ Append("Mina: I -[P] I'm sorry![P] This unit is obedient![P] This unit didn't mean it![B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] No, you have every right to disbelieve me.[B][C]") ]])
            fnCutscene([[ Append("Mina: I do?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] Yes.[P] You don't have to think I'm being sincere, but I am.[P] I want to become a better person.[P] Some day I hope you can forgive me.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] She means it, Mina.[B][C]") ]])
            fnCutscene([[ Append("Mina: I don't even know if this is a trick any more...[B][C]") ]])
            fnCutscene([[ Append("Mina: I -[P] I'm an obedient unit.[P] I will go back to my quarters and get a new function assignment.[P] I won't break the law again.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] *Even when we're apologizing, she can't believe us...*[B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] *She's been isolated for months.[P] I wouldn't believe us either.*[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] PDU, send her pardon along to the warden.[P] She is to be let out at her earliest convenience.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] The LRT facility is currently in lockdown, but we've cleared a path for you.[P] If you wish to wait until the lockdown is removed, that's up to you.[B][C]") ]])
            fnCutscene([[ Append("Mina: Thank you for your pardon, command unit.[B][C]") ]])
            fnCutscene([[ Append("Mina: Please excuse me...") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
        
            --Mina leaves the prison.
            fnCutsceneMove("RehabGolem", 29.25, 12.50)
            fnCutsceneBlocker()
            fnCutsceneFace("Christine", 0, 1)
            fnCutsceneFace("Tiffany", 0, 1)
            fnCutsceneWait(185)
            fnCutsceneBlocker()
            fnCutsceneFace("RehabGolem", 0, -1)
            fnCutsceneBlocker()
            fnCutsceneWait(45)
            fnCutsceneBlocker()
            fnCutsceneMove("RehabGolem", 17.25, 12.50)
            fnCutsceneMove("RehabGolem", 17.25, 23.50)
            fnCutsceneBlocker()
            fnCutsceneTeleport("RehabGolem", -100.25, -100.50)
            fnCutsceneBlocker()
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
            fnCutscene([[ Append("55:[E|Down] She didn't believe me...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] She'll come around, eventually.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] You'll have to visit her later.[P] Find a way to make amends on her terms.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] And if she never forgives you, you'll have to be strong enough to live with that.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] ...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I'm a machine.[P] I am unbothered.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Oh, you really believe that?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] If you were just a machine you wouldn't be struggling to find the words, would you?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] ...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] We have the warden's auth codes.[P] Let's get going.") ]])
            fnCutsceneBlocker()
        end
        
    -- |[Warden]|
    elseif(sActorName == "Warden") then
	
        --Variables.
        local iTalkedToWarden = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToWarden", "N")
        
        --First scene.
        if(iTalkedToWarden == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedToWarden", "N", 1.0)
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "PDU", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "LatexDrone", "Neutral") ]])
            fnCutscene([[ Append("Warden: GREETINGS UNIT 2855.[P] WELCOME BACK.[P] ARE YOU HERE TO RESUME REHABILITATION?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Rehabilitation?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] You recognize me, drone?[B][C]") ]])
            fnCutscene([[ Append("Warden: YES, UNIT 2855.[P] YOU WERE LAST HERE 31 DAYS AGO.[P] YOU SPENT MOST OF YOUR VISIT REHABILITATING UNIT 278101.[B][C]") ]])
            fnCutscene([[ Append("Warden: UNIT 278101 IS STILL IN HER CELL.[P] SHE HAS NOT COMMITTED ANY MORE CRIMES SINCE YOU LAST VISITED.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] May we see Unit 278101?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] ...[B][C]") ]])
            fnCutscene([[ Append("Warden: REQUEST ACCEPTED.[P] THE DOOR IS OPEN.[P] HAVE A NICE DAY.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|PDU] *PDU, download the warden's auth codes discreetly, please.*[B][C]") ]])
            fnCutscene([[ Append("PDU:[E|Quiet] *Downloading...[P] done![P] Shall I steal anything else while I am at it?*[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] *No, that will be all.*[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Let's go see Unit 278101...") ]])
            fnCutsceneBlocker()

        --Repeat.
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Warden:[VOICE|LatexDrone] EVERYTHING IN THE REHABILITATION BLOCK IS SPIFFY, COMMAND UNITS.[P] THANK YOU FOR VISITING.") ]])
            fnCutsceneBlocker()
        end
    end
end
