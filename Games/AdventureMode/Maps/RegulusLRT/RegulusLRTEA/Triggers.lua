-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--A silly conversation.
if(sObjectName == "Joints") then

    -- |[Repeat Check]|
    local iSawJointsTalk = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawJointsTalk", "N")
    if(iSawJointsTalk == 1.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter5/Scenes/iSawJointsTalk", "N", 1.0)
    
    -- |[Variables]|
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

	-- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Christine:[E|Offended] I just want to make it clear that I do not enjoy crawling around in vents like this.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Upon what impetus does this statement rest?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] There was probably some way around to get into this room from above but you went right for that vent.[B][C]") ]])
    if(sChristineForm == "Human") then
        fnCutscene([[ Append("55:[E|Neutral] You seemed to have no difficulty.[P] Are you worried about getting your clothes dirty?[B][C]") ]])
    elseif(sChristineForm == "Golem") then
        fnCutscene([[ Append("55:[E|Neutral] If this is about fashion or some nonsense, now is not the time.[B][C]") ]])
    elseif(sChristineForm == "LatexDrone") then
        fnCutscene([[ Append("55:[E|Neutral] Do a drone unit's actuators have difficulty with small spaces?[B][C]") ]])
    elseif(sChristineForm == "Electrosprite") then
        fnCutscene([[ Append("55:[E|Neutral] Does your electrical body interface unfavourably with the metal vent covers?[B][C]") ]])
    elseif(sChristineForm == "Darkmatter") then
        fnCutscene([[ Append("55:[E|Neutral] I do not see the point of your complaints when phasing through matter entirely is an option you refused to take.[B][C]") ]])
    elseif(sChristineForm == "SteamDroid") then
        fnCutscene([[ Append("55:[E|Neutral] If there is an issue with your joints or actuators I should know about it before we strain the systems.[B][C]") ]])
    elseif(sChristineForm == "Eldritch") then
        fnCutscene([[ Append("55:[E|Neutral] Do your orbiting objects require more space than usual when contracting?[P] Perhaps you ought to change forms.[B][C]") ]])
    elseif(sChristineForm == "Raiju") then
        fnCutscene([[ Append("55:[E|Neutral] As long as you do not discharge into the metal surface I do not see a problem.[B][C]") ]])
    elseif(sChristineForm == "Doll") then
        fnCutscene([[ Append("55:[E|Neutral] You have almost the exact same specifications as me.[P] Why are you struggling with this simple task?[B][C]") ]])
    elseif(sChristineForm == "Secrebot") then
        fnCutscene([[ Append("55:[E|Neutral] Your wheeled chassis is capable of crawling, you are performing the action just fine.[B][C]") ]])
    end
    fnCutscene([[ Append("Christine:[E|Neutral] That's not the point.[P] Why are you so keen on using vents even when doors are applicable?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] My superfluid joints experience no difficulty whatsoever using them, and it keeps my out of sight of civilians when moving around the city.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Don't you think your desire not to be seen is a problem?[P] It makes you choose sub-optimal paths.[P] There's no cameras in this vent system![B][C]") ]])
    fnCutscene([[ Append("55:[E|Smug] Nor are there the alternate pathways you promised.[P] Likely a maintenance ladder must be lowered from above.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] But you didn't even look before shooting through there![B][C]") ]])
    fnCutscene([[ Append("55:[E|Upset] Then what, precisely, is your complaint?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Can we at least maintain a pretense of dignity while on assignment?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] No.[P] Our comfort and emotions are secondary to the objective.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] ...[P] Darn it, I completely agree.[P] Okay, I won't complain about crawling around in vents.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] It is indeed rare to see someone change their mind when confronted with logic.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Yeah, because then you gloat about it.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Understood. I will not gloat.[P] Thank you for listening to me, let us proceed.") ]])
    fnCutsceneBlocker()

end
