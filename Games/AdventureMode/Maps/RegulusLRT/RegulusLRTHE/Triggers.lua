-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Entry cutscene. Triggers once.
if(sObjectName == "EntryScene") then

	--Repeat check.
	local iSawPrisonEntryScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawPrisonEntryScene", "N")
	if(iSawPrisonEntryScene == 1.0) then return end
		
    --Variables.
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iSawPrisonEntryScene", "N", 1.0)
    
    --Move up.
    fnCutsceneMove("Christine", 4.75, 31.50)
    fnCutsceneMove("Tiffany",        5.75, 31.50)
    fnCutsceneFace("Christine", 0, -1)
    fnCutsceneFace("Tiffany",        0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "LatexDrone", "Neutral") ]])
    fnCutscene([[ Append("Drone: GREETINGS, COMMAND UNIT.[P] HOW MAY THIS UNIT SERVE YOU?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] *They're not attacking?*[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] *They probably don't know about the base status.[P] Remember, they're isolated from the grid.*[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smirk] Drone, report on the security status of this site.[B][C]") ]])
    fnCutscene([[ Append("Drone: EVERYTHING IS SPIFFY, COMMAND UNIT.[P] NO INTRUDERS HAVE BEEN DETECTED.[B][C]") ]])
    fnCutscene([[ Append("Drone: IF YOU WOULD LIKE TO TRANSFER YOUR CARGO FOR REPROGRAMMING OR REPURPOSEMENT, WE CAN HAVE A COURIER FOR YOU IN A MOMENT.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Cargo?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smirk] That'd be you.[B][C]") ]])
    
    --Human:
    if(sChristineForm == "Human") then
        fnCutscene([[ Append("55:[E|Neutral] Drone, this human is my property.[P] See that the security grid is updated to allow her through.[B][C]") ]])
        fnCutscene([[ Append("Drone: AFFIRMATIVE, COMMAND UNIT.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] EXCUSE ME?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] By default, humans have no rights when not bearing a security badge.[P] The drones in this site will leave you alone, now.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Mgrgmgr...[B][C]") ]])
    
    --Doll. NC+ only.
    elseif(sChristineForm == "Doll") then
        fnCutscene([[ Append("55:[E|Neutral] Drone, this is a command unit under my care, not a prisoner.[B][C]") ]])
        fnCutscene([[ Append("Drone: AFFIRMATIVE, COMMAND UNIT.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Why didn't you recognize me as a command unit, drone?[B][C]") ]])
        fnCutscene([[ Append("Drone: IT IS OUR STANDING ORDER TO TREAT ALL UNREGISTERED UNITS AS PRISONERS UNTIL INFORMED OTHERWISE.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Has a command unit ever been a prisoner?[B][C]") ]])
        fnCutscene([[ Append("Drone: NEGATIVE.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I suppose you're just following your programming...[B][C]") ]])
    
    --Raiju:
    elseif(sChristineForm == "Raiju") then
        fnCutscene([[ Append("55:[E|Neutral] Drone, this Raiju is my personal pet.[P] No harm is to come to her.[B][C]") ]])
        fnCutscene([[ Append("Drone: AFFIRMATIVE, COMMAND UNIT.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] *Pet?[P] Careful what you say, or I just might zap you.*[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] *Maintain your cover.*[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] *Grrr.*[B][C]") ]])
    
    --Golem:
    elseif(sChristineForm == "Golem") then
        fnCutscene([[ Append("55:[E|Neutral] Drone, Unit 771852 is to be afforded full command privileges, effective immediately.[B][C]") ]])
        fnCutscene([[ Append("Drone: AFFIRMATIVE, COMMAND UNIT.[B][C]") ]])
        fnCutscene([[ Append("Drone: GREETINGS, LORD UNIT.[P] HOW MAY THIS UNIT SERVE YOU?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Could you curtsy?[B][C]") ]])
        fnCutscene([[ Append("Drone: AFFIRMATIVE, LORD UNIT.[B][C]") ]])
        fnCutscene([[ Append("*The drone does an awkward curtsy.*[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Happy] Hee hee hee![P] Oh, I'm already letting the power go to my head.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Shall we continue...?[B][C]") ]])
    
    --Latex Drone:
    elseif(sChristineForm == "LatexDrone") then
        fnCutscene([[ Append("55:[E|Neutral] Drone, Unit 771852 is in my retinue.[P] Update her permissions.[B][C]") ]])
        fnCutscene([[ Append("Drone: AFFIRMATIVE, COMMAND UNIT.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] *Play along, Christine*[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] ...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] THANK YOU FOR UPDATING MY PERMISSIONS, COMMAND UNIT.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Let us continue, then.[B][C]") ]])
    
    --Darkmatter:
    elseif(sChristineForm == "Darkmatter") then
        fnCutscene([[ Append("55:[E|Neutral] Drone, this Darkmatter is my personal pet.[P] No harm is to come to her.[B][C]") ]])
        fnCutscene([[ Append("Drone: AFFIRMATIVE, COMMAND UNIT.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] *The things I let you get away with...*[B][C]") ]])
    
    --Eldritch. NC+ only.
    elseif(sChristineForm == "Eldritch") then
        fnCutscene([[ Append("55:[E|Neutral] Drone, this specimen is under my care. She is not a danger.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] If you are asked by a superior unit, she is a biological testing specimen.[P] Provide no other details. Is that clear?[B][C]") ]])
        fnCutscene([[ Append("Drone: AFFIRMATIVE, COMMAND UNIT.[B][C]") ]])
        fnCutscene([[ Append("Drone: BIOLOGICAL TESTING SPECIMEN.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Yes?[B][C]") ]])
        fnCutscene([[ Append("Drone: YOU HAVE VERY PRETTY EYES.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Blush] Oh my goodness, thank you![B][C]") ]])
    
    --Electrosprite.
    elseif(sChristineForm == "Electrosprite") then
        fnCutscene([[ Append("55:[E|Neutral] Drone, this golem is my assistant.[B][C]") ]])
        fnCutscene([[ Append("Drone: COMMAND UNIT.[P] ERROR.[P] UNIT DOES NOT APPEAR TO BE A GOLEM.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Drone, what are you talking about?[P] I am clearly a golem.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Are you not picking up my authenticator chip's signal?[B][C]") ]])
        fnCutscene([[ Append("Drone: NEGATIVE.[P] VISUAL APPEARANCE ALSO SUGGEST A CREATURE MADE OF ELECTRICITY.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Drone, your optical receptors are defective.[P] I am a golem.[B][C]") ]])
        fnCutscene([[ Append("Drone: AFFIRMATIVE.[P] YOU ARE A GOLEM.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Much better.[P] We'll be on our way now.[B][C]") ]])
    
    --Secrebot, NC+.
    elseif(sChristineForm == "Secrebot") then
        fnCutscene([[ Append("55:[E|Neutral] Drone, this secrebot is my assistant.[B][C]") ]])
        fnCutscene([[ Append("Drone: COMMAND UNIT.[P] ERROR.[P] SECREBOTS ARE PHASED OUT.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Excuse me, I am a refurbished unit.[B][C]") ]])
        fnCutscene([[ Append("Drone: AFFIRMATIVE.[P] UPDATING.[P] ARE SECREBOTS SUPERIOR TO DRONE UNITS?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] This one is.[P] Treat her as you do a lord golem.[B][C]") ]])
        fnCutscene([[ Append("Drone: AFFIRMATIVE.[P] PERMISSIONS UPDATED.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Much better.[P] Thank you, drone.[B][C]") ]])
    
    --Raiju. NC+ only.
    elseif(sChristineForm == "Raibie") then
        fnCutscene([[ Append("55:[E|Neutral] Drone, this raiju is my property.[P] See that the security grid is updated to allow her through.[B][C]") ]])
        fnCutscene([[ Append("Drone: AFFIRMATIVE, COMMAND UNIT.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] EXCUSE ME?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] By default, raijus have no rights when not bearing a security badge.[P] The drones in this site will leave you alone, now.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Mgrgmgr...[B][C]") ]])
    
    --Steam Droid:
    elseif(sChristineForm == "SteamDroid") then
        fnCutscene([[ Append("55:[E|Neutral] Drone, this Steam Droid is an important ambassador.[P] Treat her with the respect accorded to Command Units.[B][C]") ]])
        fnCutscene([[ Append("Drone: AFFIRMATIVE, COMMAND UNIT.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Very good.[P] I am glad these drones are hospitable.[B][C]") ]])
        fnCutscene([[ Append("Drone: THANK YOU FOR THE COMPLIMENT, AMBASSADOR.[P] HAVE A PLEASANT STAY.[B][C]") ]])
    end
    
    --Resume.
    fnCutscene([[ Append("55:[E|Neutral] Drone, we need to speak to the warden.[B][C]") ]])
    fnCutscene([[ Append("Drone: THE WARDEN IS AT HER COMMAND TERMINAL IN THE PRISON BLOCK, DOWN THE LADDER.[B][C]") ]])
    fnCutscene([[ Append("Drone: PLEASE HAVE A FRUSTRATINGLY SAFE DAY.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] (They really need to change the adjective generator on that...)[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Let's get going.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Merge party.
    fnCutsceneMove("Tiffany", 4.75, 31.50)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Fold Party") ]])
    fnCutsceneBlocker()

--Diary cutscene. Triggers once.
elseif(sObjectName == "DiaryScene") then

	--Variables.
	local iSawDiaryScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawDiaryScene", "N")
	if(iSawDiaryScene == 0.0) then
		
		--Variables.
		local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawDiaryScene", "N", 1.0)
		
		--Move up.
		fnCutsceneMove("Christine", 26.75, 31.50)
		fnCutsceneMove("Tiffany",        22.75, 31.50)
		fnCutsceneFace("Tiffany",        1, 0)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", -1, 0)
		fnCutsceneBlocker()
		fnCutsceneWait(65)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Are you coming?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] My motivators are indeed familiar with this building.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] We shouldn't dawdle, or the drones might suspect something.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Down] Christine.[P] Tell me about your grandfather.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Which one?[P] My mother's side?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] I never knew him, he stayed in India and never visited us.[P] Mother said her family did not approve of her marriage in the slightest.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] The one who wrote the diary you mentioned.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Ah, my father's father.[P] Conrad Dormer.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] ...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Movement.
		fnCutsceneMove("Christine", 28.75, 31.50, 0.25)
		fnCutsceneBlocker()
		fnCutsceneWait(65)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Offended] The old bastard died well before I was born.[P] Apparently, he was on deployment and was shot by a civilian.[P] I never found out why.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] My ancestors were all officers, and before that, knights and lords.[P] He served in the expeditionary force during World War 2. [B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Which was?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] The largest conflict in the history of Earth, where I am from.[P] 60 million humans dead in six years.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Down] That -[P] completely dwarfs any conflict in Pandemonium's history...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] The world tore itself apart as ideologies of mass production retooled their factories to produce corpses.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] When I was a little boy I had looked up to my grandfather.[P] His portrait hangs in the hall outside the bedrooms.[P] Father said he was a great hero in the war.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] I believed all of it.[P] Father said I had his eyes, and maybe I did.[P] I certainly had his intellect.[P] But I didn't have his force of will.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Father told me stories about what he did.[P] He had led a company of commandos on daring raids.[P] His elite squad was the terror of the Reich, striking at night and vanishing before the guards could rouse.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] He told me how he led his men with courage.[P] How he once had run onto a battlefield in sight of a machine gun to haul a wounded man back to the lines.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] How he had walked calmly as shells burst around him, because an officer could never flinch, lest the men lose their nerve.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] How they had resolved once to use only their knives on one mission, and gutted the Jerries like pigs.[P] The enemy fled rather than face them the next night.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] I wanted to be just like him.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] And then I found his diary.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] You see, I had been going through my father's study looking for something.[P] I don't remember what.[P] And I found grandfather Conrad's diary tucked behind a photo album.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] I eagerly read it.[P] The stories were true, at least according to him.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] But then I read the things he did after the war.[P] He was redeployed to West Germany.[P] He worked in places that didn't officially exist.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] Captured spies were taken there.[P] He would beat them.[P] Torture them.[P] Starve them.[P] To find out what they knew, sometimes.[P] To break their spirits, always.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] He would write how the Soviet spies were barely human.[P] He loved to humiliate them.[P] He said the lower orders needed to know their place, and that he was happy to oblige.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] They were your enemies.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] His enemies.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] Yes, they were his enemies.[P] The enemies he was told to hate.[P] When I was young, that's what I thought, too.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] But then I realized that the men who he was killing, they -[P] well, they were boys.[P] Kids.[P] Most of them teenagers.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] They would never go to school.[P] They would never grow up.[P] Find love.[P] Start families.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] He was a killer, like any other.[P] If killing isn't glorious, then being courageous while doing it is no more glorious.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] Sophie thinks the work we do out here...[P] the sneaking, fighting, searching, hacking.[P] She thinks it's sexy.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] It's not.[P] It's vile and disgusting.[P] I'm doing it because to do nothing would be worse.[P] But don't you think for a second we'll find redemption out here.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Down] ...[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Was your home much like Regulus?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] In some ways, no.[P] In many ways, yes.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] When I was converted, I didn't want to think about it.[P] But, what happened at the cryogenics facility would not be out of place where I come from.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] I read his whole diary in one sitting.[P] Then I read it again the following night.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] My family's history is not one of glory and heroism.[P] That's just the part they like to tell you about.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Cry] The knights of old England did the same thing to peasants that my grandfather did to spies.[P] That's my family's legacy.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] When I came of age, I told my father I would never join the army.[P] I told him his connections were worthless and his friends were thugs.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] What did he do?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] He told me I was young and unschooled in the ways of the world.[P] He acted like he was in the right.[P] It sickened me.[P] I could guess he did the same things when he was stationed in India.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] I asked my mother, but she wouldn't hear anything of it.[P] She wants to believe he's a good man.[P] I know better.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] We've not really spoken since.[P] I took my trust fund and got an apartment.[P] I went to school to learn something useful.[P] I became a teacher.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] Teachers...[P] Teachers don't make corpses for a living...[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Christine, I know this place.[P] I've been here before.[P] Do you think I... [P][CLEAR]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] I would love to believe you're innocent, but I don't think I can fool myself like my mother did.[P] And this coming from someone who knows how to fool herself.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] But it's not who you were that matters, it's who you are.[P] Make your own future, 55.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Merge party.
		fnCutsceneMove("Tiffany", 28.75, 31.50)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	
	end
end
