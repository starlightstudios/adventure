-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToLRTA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] (Looks like a shortcut back to the building's entrance.)[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] (Drop down?)[BLOCK]") ]])

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"No\") ")
    fnCutsceneBlocker()
    
elseif(sObjectName == "ToLRTZA") then
    AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusLRTZA", "FORCEPOS:22.0x16.0x0")

-- |[Decision]|
elseif(sObjectName == "Yes") then
	WD_SetProperty("Hide")
    fnCutsceneWait(15)
    fnCutscene([[ AL_SetProperty("Activate Fade", 15, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Transition to LRTA, with this flag tripped.
    gbFallIntoLRTA = true
    fnCutscene([[ AL_BeginTransitionTo("RegulusLRTA", "FORCEPOS:4.5x10.0x0") ]])
    
elseif(sObjectName == "No") then
	WD_SetProperty("Hide")

-- |[Objects]|
-- |[Unhandled Case]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
