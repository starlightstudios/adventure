-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
--Exit.
if(sObjectName == "ToLRTE") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusLRTE", "FORCEPOS:3.0x14.0x0")

--Exit.
elseif(sObjectName == "ToLRTG") then

	--Haven't seen the cutscene, go to G.
	local iSawSouthernCoreCutscene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawSouthernCoreCutscene", "N")
	if(iSawSouthernCoreCutscene == 0.0) then
		AudioManager_PlaySound("World|AutoDoorOpen")
		AL_BeginTransitionTo("RegulusLRTG", "FORCEPOS:13.5x40.0x0")
	else
		AudioManager_PlaySound("World|AutoDoorOpen")
		AL_BeginTransitionTo("RegulusLRTGX", "FORCEPOS:13.5x40.0x0")
	end

-- |[Elevator E]|
--Allows the player to enter the main floor.
elseif(sObjectName == "ElevatorESouth") then

	--Open the elevator.
	fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutscene([[ AL_SetProperty("Open Door", "ElevatorESouth") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Move Christine and 55 inside.
	fnCutsceneMove("Christine", 61.25, 35.50)
	fnCutsceneMove("Tiffany", 62.25, 35.50)
	fnCutsceneBlocker()
	fnCutsceneFace("Christine", 0, -1)
	fnCutsceneFace("Tiffany", 0, -1)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Close the elevator.
	fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutscene([[ AL_SetProperty("Close Door", "ElevatorESouth") ]])
	
	--Teleport both character offscreen. Move the camera north.
	fnCutsceneTeleport("Christine", -100.25, -100.50)
	fnCutsceneTeleport("Tiffany", -100.25, -100.50)
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 0.5)
		CameraEvent_SetProperty("Focus Position", (61.25 * gciSizePerTile), (26.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--Teleport the characters back and set the camera to follow Christine again.
	fnCutsceneTeleport("Christine", 61.25, 26.50)
	fnCutsceneTeleport("Tiffany", 62.25, 26.50)
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 2.0)
		CameraEvent_SetProperty("Focus Actor Name", "Christine")
	DL_PopActiveObject()
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutscene([[ AL_SetProperty("Open Door", "ElevatorENorth") ]])
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Move the party out.
	fnCutsceneMove("Christine", 61.25, 24.50)
	fnCutsceneMove("Tiffany", 62.25, 24.50)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Close the door.
	fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutscene([[ AL_SetProperty("Close Door", "ElevatorENorth") ]])
	fnCutsceneBlocker()
	
	--Move 55 onto Christine.
	fnCutsceneMove("Tiffany", 61.25, 24.50)
	fnCutsceneBlocker()
	
	--Fold party.
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()

--Northern edge.
elseif(sObjectName == "ElevatorENorth") then

	--Open the elevator.
	fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutscene([[ AL_SetProperty("Open Door", "ElevatorENorth") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Move Christine and 55 inside.
	fnCutsceneMove("Christine", 61.25, 26.50)
	fnCutsceneMove("Tiffany", 62.25, 26.50)
	fnCutsceneBlocker()
	fnCutsceneFace("Christine", 0, 1)
	fnCutsceneFace("Tiffany", 0, 1)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Close the elevator.
	fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutscene([[ AL_SetProperty("Close Door", "ElevatorENorth") ]])
	
	--Teleport both character offscreen. Move the camera south.
	fnCutsceneTeleport("Christine", -100.25, -100.50)
	fnCutsceneTeleport("Tiffany", -100.25, -100.50)
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 0.5)
		CameraEvent_SetProperty("Focus Position", (61.25 * gciSizePerTile), (35.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--Teleport the characters back and set the camera to follow Christine again.
	fnCutsceneTeleport("Christine", 61.25, 35.50)
	fnCutsceneTeleport("Tiffany", 62.25, 35.50)
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 2.0)
		CameraEvent_SetProperty("Focus Actor Name", "Christine")
	DL_PopActiveObject()
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutscene([[ AL_SetProperty("Open Door", "ElevatorESouth") ]])
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Move the party out.
	fnCutsceneMove("Christine", 61.25, 37.50)
	fnCutsceneMove("Tiffany", 62.25, 37.50)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Close the door.
	fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutscene([[ AL_SetProperty("Close Door", "ElevatorESouth") ]])
	fnCutsceneBlocker()
	
	--Move 55 onto Christine.
	fnCutsceneMove("Tiffany", 61.25, 37.50)
	fnCutsceneBlocker()
	
	--Fold party.
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()

-- |[Southern Vent]|
--Eastern edge.
elseif(sObjectName == "VentAEast") then
	
	--Subroutine does the work.
	fnCrawlThroughVent(33.25, 32.50, 23.25, 32.50)
	
	--Optional scene.
	local iSawUp55sSkirt = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawUp55sSkirt", "N")
	if(iSawUp55sSkirt == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawUp55sSkirt", "N", 1.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
		fnCutscene([[ Append("55:[E|Neutral] Unit 771852, were you looking up my skirt when we were crawling through the vent?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Perish the thought![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Laugh] ...[P] You have barely any skirt to look up.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Tch.") ]])
		fnCutsceneBlocker()
	end
	
--Western edge.
elseif(sObjectName == "VentAWest") then
	fnCrawlThroughVent(23.25, 32.50, 33.25, 32.50)
	
-- |[Northern Vent]|
--Western edge.
elseif(sObjectName == "VentBWest") then
	fnCrawlThroughVent(26.25, 5.50, 33.25, 10.50)
	
--Eastern edge.
elseif(sObjectName == "VentBEast") then
	fnCrawlThroughVent(33.25, 10.50, 26.25, 5.50)

-- |[Objects]|
--Locked doors.
elseif(sObjectName == "LockedDoor") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[SOUND|World|AutoDoorFail]Access denied.[P] Security lockdown in effect.") ]])
	fnCutsceneBlocker()

--Temporarily locked door, north.
elseif(sObjectName == "DoorMidN") then

	--Door has been unlocked or not.
	local iLRTFOpenedMid = VM_GetVar("Root/Variables/Chapter5/World/iLRTFOpenedMid", "N")
	if(iLRTFOpenedMid == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[SOUND|World|AutoDoorFail]Access denied.[P] Security lockdown in effect.[B][C]") ]])
		fnCutscene([[ Append("PDU:[VOICE|PDU] One moment, I will bypass the circuit... Complete.") ]])
		fnCutsceneBlocker()
		fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
		fnCutscene([[ AL_SetProperty("Open Door", "DoorMidN") ]])
		fnCutsceneBlocker()
		VM_SetVar("Root/Variables/Chapter5/World/iLRTFOpenedMid", "N", 1.0)

	--Unlock the door.
	else
		AudioManager_PlaySound("World|AutoDoorOpen")
		AL_SetProperty("Open Door", "DoorMidN")
	end

--Locked door, south.
elseif(sObjectName == "DoorMidS") then

	--Door has been unlocked or not.
	local iLRTFOpenedMid = VM_GetVar("Root/Variables/Chapter5/World/iLRTFOpenedMid", "N")
	if(iLRTFOpenedMid == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[SOUND|World|AutoDoorFail]Access denied.[P] Security lockdown in effect.[B][C]") ]])
		fnCutscene([[ Append("PDU:[VOICE|PDU] Christine, it may be possible to force this door open from the other side.[P] Please open the circuit panel on the far side of the door.") ]])
		fnCutsceneBlocker()

	--Unlock the door.
	else
		AudioManager_PlaySound("World|AutoDoorOpen")
		AL_SetProperty("Open Door", "DoorMidS")
	end

--Airlock doors.
elseif(sObjectName == "AirlockSE") then
	AL_SetProperty("Close Door", "AirlockSS")

elseif(sObjectName == "AirlockSS") then

    --Check Christine's forms, TF if necessary. Yes, we check against Raiju even though you can't have it unless you're a cheater.
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    if(sChristineForm == "Human" or sChristineForm == "Raiju") then
        
        --Make sure the door stays closed.
        AL_SetProperty("Close Door", "AirlockSS")
        
        --Variables:
        local iHasLatexForm         = VM_GetVar("Root/Variables/Global/Christine/iHasLatexForm", "N")
        local iHasDarkmatterForm    = VM_GetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N")
        local iHasEldritchForm      = VM_GetVar("Root/Variables/Global/Christine/iHasEldritchForm", "N")
        local iHasElectrospriteForm = VM_GetVar("Root/Variables/Global/Christine/iHasElectrospriteForm", "N")
        local iHasSteamDroidForm    = VM_GetVar("Root/Variables/Global/Christine/iHasSteamDroidForm", "N")
        local iHasDollForm          = VM_GetVar("Root/Variables/Global/Christine/iHasDollForm", "N")
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Door:[VOICE|Narrator] Warning:: Depressurized area ahead.[P] Secure organic subjects for transport before exiting.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] (Better change to an inorganic form before I go outside...)[BLOCK]") ]])

        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        if(iHasDarkmatterForm == 1.0) then
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Darkmatter\",  " .. sDecisionScript .. ", \"TFToDarkmatter\") ")
        end
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Golem\", " .. sDecisionScript .. ", \"TFToGolem\") ")
        if(iHasLatexForm == 1.0) then
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Latex Drone\",  " .. sDecisionScript .. ", \"TFToLatex\") ")
        end
        if(iHasEldritchForm == 1.0) then
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Dreamer\",  " .. sDecisionScript .. ", \"TFToEldritch\") ")
        end
        if(iHasElectrospriteForm == 1.0) then
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Electrosprite\",  " .. sDecisionScript .. ", \"TFToElectrosprite\") ")
        end
        if(iHasSteamDroidForm == 1.0) then
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Steam Droid\",  " .. sDecisionScript .. ", \"TFToSteam\") ")
        end
        if(iHasDollForm == 1.0) then
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Doll\",  " .. sDecisionScript .. ", \"TFToDoll\") ")
        end
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Cancel\", " .. sDecisionScript .. ", \"NoCancel\") ")
        fnCutsceneBlocker()

    --Inorganic cases.
    else
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_SetProperty("Close Door", "AirlockSE")
        AL_SetProperty("Open Door", "AirlockSS")
    end
	
elseif(sObjectName == "AirlockNS") then

    --Check Christine's forms, TF if necessary. Yes, we check against Raiju even though you can't have it unless you're a cheater.
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    if(sChristineForm == "Human" or sChristineForm == "Raiju") then
        
        --Make sure the door stays closed.
        AL_SetProperty("Close Door", "AirlockNS")
        
        --Variables:
        local iHasLatexForm         = VM_GetVar("Root/Variables/Global/Christine/iHasLatexForm", "N")
        local iHasDarkmatterForm    = VM_GetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N")
        local iHasEldritchForm      = VM_GetVar("Root/Variables/Global/Christine/iHasEldritchForm", "N")
        local iHasElectrospriteForm = VM_GetVar("Root/Variables/Global/Christine/iHasElectrospriteForm", "N")
        local iHasSteamDroidForm    = VM_GetVar("Root/Variables/Global/Christine/iHasSteamDroidForm", "N")
        local iHasDollForm          = VM_GetVar("Root/Variables/Global/Christine/iHasDollForm", "N")
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Door:[VOICE|Narrator] Warning:: Depressurized area ahead.[P] Secure organic subjects for transport before exiting.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] (Better change to an inorganic form before I go outside...)[BLOCK]") ]])

        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        if(iHasDarkmatterForm == 1.0) then
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Darkmatter\",  " .. sDecisionScript .. ", \"TFToDarkmatter\") ")
        end
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Golem\", " .. sDecisionScript .. ", \"TFToGolem\") ")
        if(iHasLatexForm == 1.0) then
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Latex Drone\",  " .. sDecisionScript .. ", \"TFToLatex\") ")
        end
        if(iHasEldritchForm == 1.0) then
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Dreamer\",  " .. sDecisionScript .. ", \"TFToEldritch\") ")
        end
        if(iHasElectrospriteForm == 1.0) then
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Electrosprite\",  " .. sDecisionScript .. ", \"TFToElectrosprite\") ")
        end
        if(iHasSteamDroidForm == 1.0) then
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Steam Droid\",  " .. sDecisionScript .. ", \"TFToSteam\") ")
        end
        if(iHasDollForm == 1.0) then
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Doll\",  " .. sDecisionScript .. ", \"TFToDoll\") ")
        end
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Cancel\", " .. sDecisionScript .. ", \"NoCancel\") ")
        fnCutsceneBlocker()

    --Inorganic cases.
    else
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_SetProperty("Close Door", "AirlockNN")
        AL_SetProperty("Open Door", "AirlockNS")
    end
	
elseif(sObjectName == "AirlockNN") then
	AL_SetProperty("Close Door", "AirlockNS")

-- |[Objects]|
elseif(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Searching for logs on Unit 2855...[P] I can only access part of the database...)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This is a very long list of deployments.[P] Seems 55 got a lot of assignments to outposts with barely any staff...)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Why would the head of security be assigned to posts where there are literally no duty logs...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Searching for logs on Unit 2855...[P] I can only access part of the database...)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This is a log file about 55 before she became a Command Unit![P] But...)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The log just indicates the hardcopies of the records were incinerated...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Searching for logs on Unit 2855...[P] I can only access part of the database...)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This is a bunch of cooking recipes, written by Unit 2855!)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Oh, wait, oops.[P] This is recipes written by Unit 22855.[P] My mistake.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Looks like a bunch of bug reports. Hmm... Let's check this one...)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('The mantis species in habitat R-9 is performing well despite the modified diet.[P] We recommend increasing vitamin uptake just to be sure there are no long term health effects.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Oh.[P] Guess that's a different kind of bug report.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Looks like some literature written by a Drone Unit! Let's see...)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (THE VERY CUTE DRONE UNIT SEARCHED THE CORRIDORS AND DIDN'T FIND ANYTHING.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (THE VERY CUTE DRONE UNIT SEARCHED THE CORRIDORS AND DIDN'T FIND ANYTHING.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (THE VERY CUTE DRONE UNIT SEARCHED THE CORRIDORS AND FOUND A PIECE OF PAPER.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (THE VERY CUTE DRONE UNIT SEARCHED THE CORRIDORS AND DIDN'T FIND ANYTHING.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (It...[P] goes on like this...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalF") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The freight lift here is locked down.[P] I can't access it.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalOffline") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This terminal is offline.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RVD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This RVD was playing 'All My Processors' before the security lockdown.[P] I guess the Drone Units like the show.)") ]])
    fnCutsceneBlocker()

-- |[Transformation]|
--Causes Christine to change forms before heading out an airlock.
elseif(sObjectName == "TFToDarkmatter") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/100 Transform/Transform_ChristineToDarkmatter/Scene_Begin.lua")
    
elseif(sObjectName == "TFToGolem") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/100 Transform/Transform_ChristineToGolem/Scene_Begin.lua")
    
elseif(sObjectName == "TFToLatex") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/100 Transform/Transform_ChristineToLatexDrone/Scene_Begin.lua")
    
elseif(sObjectName == "TFToEldritch") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/100 Transform/Transform_ChristineToEldritch/Scene_Begin.lua")
    
elseif(sObjectName == "TFToElectrosprite") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/100 Transform/Transform_ChristineToElectrosprite/Scene_Begin.lua")

elseif(sObjectName == "TFToSteam") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/100 Transform/Transform_ChristineToSteamDroid/Scene_Begin.lua")

elseif(sObjectName == "TFToDoll") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/100 Transform/Transform_ChristineToDoll/Scene_Begin.lua")

elseif(sObjectName == "NoCancel") then
	WD_SetProperty("Hide")

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
