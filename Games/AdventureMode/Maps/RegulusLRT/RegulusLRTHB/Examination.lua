-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
--Exit.
if(sObjectName == "ToLRTHA") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusLRTHA", "FORCEPOS:19.0x21.0x0")
	
--Exit.
elseif(sObjectName == "ToLRTHC") then
	
	--Variables.
	local iLRTHBLightSectionA = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTHBLightSectionA", "N")
	if(iLRTHBLightSectionA == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[SOUND|World|AutoDoorFail]Access denied.[P] Security lockdown in effect.") ]])
		fnCutsceneBlocker()
	
	--Opens if the player has turned the door lights on.
	else
		AudioManager_PlaySound("World|AutoDoorOpen")
		AL_BeginTransitionTo("RegulusLRTHC", "FORCEPOS:4.5x11.0x0")
	end

--Exit.
elseif(sObjectName == "ToLRTHD") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusLRTHD", "FORCEPOS:4.5x21.0x0")

-- |[Vents]|
--Southern Vent.
elseif(sObjectName == "VentWest") then
	fnCrawlThroughVent(17.25, 49.50, 32.25, 49.50)
	
--Southern Vent.
elseif(sObjectName == "VentEast") then
	fnCrawlThroughVent(32.25, 49.50, 17.25, 49.50)

-- |[Objects]|
--Locked doors.
elseif(sObjectName == "LockedDoor") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[SOUND|World|AutoDoorFail]Access denied.[P] Security lockdown in effect.") ]])
	fnCutsceneBlocker()

--Broken glass.
elseif(sObjectName == "BrokenGlass") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Leader](The reinforced glass was shattered from the inside.[P] The force needed would be immense...)[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Leader](The security access wire goes under the concrete and emerges on the northern end of the room.)") ]])
	fnCutsceneBlocker()
	
--Locked doors.
elseif(sObjectName == "BrokenAirlock") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[SOUND|World|AutoDoorFail]Access denied.[P] Airlock exterior door is malfunctioning.[P] Please contact the maintenance crew.") ]])
	fnCutsceneBlocker()

--Security Terminal.
elseif(sObjectName == "SecurityTerminal") then

	--Variables.
	local iCheckedSecurityConsole = VM_GetVar("Root/Variables/Chapter5/Scenes/iCheckedSecurityConsole", "N")
	local iTalkedWithRehabGolem   = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedWithRehabGolem", "N")
    local iCompletedBlackSite     = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedBlackSite", "N")
	
	--Player doesn't have the code:
	if(iTalkedWithRehabGolem == 0.0) then
		
		--First time checking the console:
		if(iCheckedSecurityConsole == 0.0) then
			
			--Flag.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iCheckedSecurityConsole", "N", 1.0)
			
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "PDU", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] ...[P] This console is locked out.[P] 55, do you have the access code?[B][C]") ]])
			fnCutscene([[ Append("55:[E|Smirk] I should...[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] No.[P] No I do not.[P] In fact, this console is set to an access code that is not even used in the Long-Range Telemetry facility.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Hmm, do you think this console was taken from another facility and they didn't reset the code?[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] No.[P] The serial number...[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] PDU, please confirm the schematics of the LRT facility.[P] Check the area just east of us.[P] What's there?[B][C]") ]])
			fnCutscene([[ Append("PDU:[E|Neutral][EMOTION|Christine|PDU] The door on the eastern edge of the building leads to a mineshaft.[P] It has not been used in 16 years, well before the LRT facility was constructed.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] PDU, please check the power levels being sent to the eastern edge of the building.[P] Do they match an unused mineshaft?[B][C]") ]])
			fnCutscene([[ Append("PDU:[E|Alarmed] Power transfer levels are 341,000 percent higher than expected for an unused facility.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Just what are you getting at, 55?[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] It's a black site.[P] But -[P][EMOTION|Tiffany|Down] I think I already knew that.[P] The layouts of these hallways are familiar to me.[B][C]") ]])
            
            --Christine doesn't know about the black sites.
            if(iCompletedBlackSite < 10.0) then
                fnCutscene([[ Append("Christine:[E|Sad] What do you mean by a 'black site'?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Sections of Regulus that do not officially exist.[P] When the administration wishes to do something that the Slave Units are best not knowing about, it is done in black sites.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Considering the staffing here and the high-security nature, the LRT facility is a good fit for one such site.[P] There are more beneath Regulus City, I believe.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Down] ...[P] My memory drives were wiped, and yet I'm almost certain I know where most of them are...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] You were Head of Security.[P] You would probably know...[B][C]") ]])
                fnCutscene([[ Append("55:[E|Down] I visited them.[P] Personally.[P] So many times that my motivators updated their flash chips for their layouts.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ...[P] 55, this may be inappropriate, but...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] My grandfather's journal contained references to something like what you'd call a black site.[P] They're one of the reasons I refused to join the army.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] He -[P] he -[P] he was not a good man.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] And?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Just -[P] don't read into that.[P] We don't know for certain what's down there.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] If we want the access codes, we'll need to find out.[P] I can't hack this terminal due to the blue box encryption.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] ...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] Okay.[P] But please don't - [P][CLEAR]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Machines do not suffer emotional crises, Christine.[P] I am a machine.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Whatever we find out, I will continue to perform my function, as will you.[P] Is that clear?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ... as crystal...") ]])
			
            --She does.
            else
                fnCutscene([[ Append("Christine:[E|Offended] Another black site, just like in the mines.[P] Do you think the same...[P] things...[P] will be in there?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Unlikely.[P] Otherwise, the containment protocols would be more prepared for something like Vivify.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] This does not discount the possibility, just reduces its likelihood.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Down] And, I am -[P] I am certain I have been to this particular site before.[P] My motivators have flash memory of them.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] ...[P] 55, this may be inappropriate, but...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] My grandfather's journal contained references to something like what you'd call a black site.[P] They're one of the reasons I refused to join the army.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] He -[P] he -[P] he was not a good man.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] And?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Just -[P] don't read into that.[P] We don't know for certain what's down there.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] We must enter to acquire the access codes we need.[P] That much is certain.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] If you are worried about my emotional state, you are wasting your cycles.[P] Machines do not suffer emotional crises.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Whatever we find out, I will continue to perform my function, as will you.[P] Is that clear?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ... as crystal...") ]])
            end
            
		--All other attempts:
		else
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ Append("[VOICE|Leader](We need the auth codes from the black site on the eastern edge of the facility...)") ]])
			fnCutsceneBlocker()
		end
	
	--Has the auth codes.
	else
		
		--First time checking the console:
		if(iCheckedSecurityConsole == 0.0) then
	
			--Flag.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iCheckedSecurityConsole", "N", 1.0)
			VM_SetVar("Root/Variables/Chapter5/Scenes/iLRTHBLightSectionA", "N", 1.0)
			
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
			fnCutscene([[ Append("55:[E|Neutral] The warden's authcodes were accepted.") ]])
			fnCutsceneBlocker()

			--Lights turn on.
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			fnCutscene([[ AL_SetProperty("Enable Light", "LgtA00") ]])
			fnCutsceneWait(5)
			fnCutsceneBlocker()
			fnCutscene([[ AL_SetProperty("Enable Light", "LgtA01") ]])
			fnCutsceneWait(5)
			fnCutsceneBlocker()
			fnCutscene([[ AL_SetProperty("Enable Light", "LgtA03") ]])
			fnCutsceneWait(5)
			fnCutsceneBlocker()
			fnCutscene([[ AL_SetProperty("Enable Light", "LgtA02") ]])
			fnCutscene([[ AL_SetProperty("Enable Light", "LgtA11") ]])
			fnCutscene([[ AL_SetProperty("Enable Light", "LgtA09") ]])
			fnCutsceneWait(5)
			fnCutsceneBlocker()
			fnCutscene([[ AL_SetProperty("Enable Light", "LgtA04") ]])
			fnCutscene([[ AL_SetProperty("Enable Light", "LgtA05") ]])
			fnCutscene([[ AL_SetProperty("Enable Light", "LgtA12") ]])
			fnCutscene([[ AL_SetProperty("Enable Light", "LgtA13") ]])
			fnCutscene([[ AL_SetProperty("Enable Light", "LgtA10") ]])
			fnCutsceneWait(5)
			fnCutsceneBlocker()
			fnCutscene([[ AL_SetProperty("Enable Light", "LgtA06") ]])
			fnCutscene([[ AL_SetProperty("Enable Light", "LgtA07") ]])
			fnCutscene([[ AL_SetProperty("Enable Light", "LgtA08") ]])
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
			fnCutscene([[ Append("55:[E|Neutral] The door to the prisoner exchange lifts should be open now.[P] We can access the transit tunnels from there.") ]])
			fnCutsceneBlocker()
			
		--Repeat.
		else	
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ Append("[VOICE|Leader](We should be able to backtrack to the exchange lifts now that this terminal is unlocked...)") ]])
			fnCutsceneBlocker()
		end
	end
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
