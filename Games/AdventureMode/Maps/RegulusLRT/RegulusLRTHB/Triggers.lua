-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Cutscene that takes place the first time the player enters.
if(sObjectName == "EntryScene") then

	--Variables.
	local iSawBattleCutscene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawBattleCutscene", "N")
	
	--Only play the scene once.
	if(iSawBattleCutscene == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawBattleCutscene", "N", 1.0)

		--Move 55 and Christine south.
		fnCutsceneMove("Christine", 11.25, 28.50)
		fnCutsceneMove("Tiffany",        11.25, 28.50)
		fnCutsceneMove("Christine", 11.25, 30.50)
		fnCutsceneMove("Tiffany",        11.25, 29.50)
		fnCutsceneBlocker()
		
		--Christine stops and looks around.
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", 1, 1)
		fnCutsceneWait(35)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", 1, 0)
		fnCutsceneWait(35)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", 1, 1)
		fnCutsceneWait(35)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", 0, 1)
		fnCutsceneWait(35)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", -1, 1)
		fnCutsceneWait(35)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", -1, 0)
		fnCutsceneWait(35)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Sad] 55...[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] You don't have to say anything.[P] My olfactory sensors have picked up leaked coolant and acid residue.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--55 Moves in.
		fnCutsceneMove("Tiffany",        11.25, 30.50)
		fnCutsceneMove("Christine", 10.25, 30.50)
		fnCutsceneMove("Tiffany",        12.25, 30.50)
		fnCutsceneFace("Christine", 1, 0)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
		fnCutscene([[ Append("55:[E|Neutral] Two security teams, judging by the ID tags and weapon loadouts.[P] Odds of survival, near-zero.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] But how?[P] Was this all done by Vivify?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Likely, unless another explanation shows itself.[P] The acid residue and lacerations are all I need for that conclusion.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] ...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--55 checks the window.
		fnCutsceneMove("Tiffany", 16.25, 30.50)
		fnCutsceneFace("Tiffany", 0, -1)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneMove("Christine", 15.25, 30.50)
		fnCutsceneFace("Christine", 1, -1)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
		fnCutscene([[ Append("55:[E|Neutral] Considering the glass was broken outwards, this is the likely point of ingress.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Smirk] Presumably, Vivify entered via the transit tunnels below us, climbed up the shaft here, and wiped out the security detachment.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] The security teams tried to construct makeshift barricades, but to little effect.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] ...[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Are you paying attention?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] Do we have to dwell on this?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Smirk] These units were retired.[P] We cannot change that.[P] But we can learn from their combat data.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Mobility will be crucial in combat.[P] Cover will do little.[P] We'll need to find a way to protect ourselves from the acid, as well.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Pulse weaponry may be ineffective.[P] I can't see any evidence of electrical or explosive weapons, so we may need to try those.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Vivify also has exceptional strength.[P] You may be able to engage it in frontal combat if you focus on defense.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] ...[P] Are you done?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] I will save as much of this as I can to my memory banks for analysis as we move.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] Good.[P] I'd rather not dally in this mortuary...[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Search this area for the security console. Hopefully it was not destroyed during the battle.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Right, let's go.") ]])
		fnCutsceneBlocker()
		
		--Move 55 onto Christine.
		fnCutsceneMove("Tiffany", 15.25, 30.50)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()

	end
end
