-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
--Exit.
if(sObjectName == "ToLRTBNorth") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusLRTB", "FORCEPOS:21.0x10.0x0")
	
--Exit.
elseif(sObjectName == "ToLRTBSouth") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusLRTB", "FORCEPOS:21.0x16.0x0")
	
--Exit.
elseif(sObjectName == "ToLRTHA") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusLRTHA", "FORCEPOS:11.0x21.0x0")

--Exit.
elseif(sObjectName == "ToLRTIA") then
	
	--Variables.
	local iLRTHBLightSectionA = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTHBLightSectionA", "N")
	if(iLRTHBLightSectionA == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[SOUND|World|AutoDoorFail]Access denied.[P] Security lockdown in effect.") ]])
		fnCutsceneBlocker()
	
	else
		AudioManager_PlaySound("World|AutoDoorOpen")
		AL_BeginTransitionTo("RegulusLRTIA", "FORCEPOS:13.5x26.0x0")
	end

-- |[Doors]|
elseif(sObjectName == "DoorA") then
	
	--Variables.
	local iLRTCLightSectionB = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionB", "N")
	if(iLRTCLightSectionB == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[SOUND|World|AutoDoorFail]Access denied.[P] Security lockdown in effect.") ]])
		fnCutsceneBlocker()
	
	--Opens if the player has turned the door lights on.
	else
		AL_SetProperty("Open Door", "DoorA")
		AudioManager_PlaySound("World|AutoDoorOpen")
	end
	
elseif(sObjectName == "DoorB") then
	
	--Variables.
	local iLRTCLightSectionA = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionA", "N")
	if(iLRTCLightSectionA == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[SOUND|World|AutoDoorFail]Access denied.[P] Security lockdown in effect.") ]])
		fnCutsceneBlocker()
	
	--Opens if the player has turned the door lights on.
	else
		AL_SetProperty("Open Door", "DoorB")
		AudioManager_PlaySound("World|AutoDoorOpen")
	end
	
elseif(sObjectName == "DoorC") then
	
	--Variables.
	local iLRTCLightSectionC = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionC", "N")
	if(iLRTCLightSectionC == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[SOUND|World|AutoDoorFail]Access denied.[P] Security lockdown in effect.") ]])
		fnCutsceneBlocker()
	
	--Opens if the player has turned the door lights on.
	else
		AL_SetProperty("Open Door", "DoorC")
		AudioManager_PlaySound("World|AutoDoorOpen")
	end
	
elseif(sObjectName == "DoorD") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[SOUND|World|AutoDoorFail]Access denied.[P] Security lockdown in effect.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "DoorE") then
	
	--Variables.
	local iLRTCLightSectionC = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionC", "N")
	if(iLRTCLightSectionC == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[SOUND|World|AutoDoorFail]Access denied.[P] Security lockdown in effect.") ]])
		fnCutsceneBlocker()
	
	--Opens if the player has turned the door lights on.
	else
		AL_SetProperty("Open Door", "DoorE")
		AudioManager_PlaySound("World|AutoDoorOpen")
	end
	
elseif(sObjectName == "DoorI") then
	
	--Variables.
	local iLRTCLightSectionD = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionD", "N")
	if(iLRTCLightSectionD == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[SOUND|World|AutoDoorFail]Access denied.[P] Security lockdown in effect.") ]])
		fnCutsceneBlocker()
	
	--Opens if the player has turned the door lights on.
	else
		AL_SetProperty("Open Door", "DoorI")
		AudioManager_PlaySound("World|AutoDoorOpen")
	end
	
elseif(sObjectName == "DoorJ") then
	
	--Variables.
	local iLRTCLightSectionD = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionD", "N")
	if(iLRTCLightSectionD == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[SOUND|World|AutoDoorFail]Access denied.[P] Security lockdown in effect.") ]])
		fnCutsceneBlocker()
	
	--Opens if the player has turned the door lights on.
	else
		AL_SetProperty("Open Door", "DoorJ")
		AudioManager_PlaySound("World|AutoDoorOpen")
	end
	
elseif(sObjectName == "DoorK") then
	
	--Variables.
	local iLRTCLightSectionD = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionD", "N")
	if(iLRTCLightSectionD == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[SOUND|World|AutoDoorFail]Access denied.[P] Security lockdown in effect.") ]])
		fnCutsceneBlocker()
	
	--Opens if the player has turned the door lights on.
	else
		AL_SetProperty("Open Door", "DoorK")
		AudioManager_PlaySound("World|AutoDoorOpen")
	end

-- |[Terminals]|
--Flavour text.
elseif(sObjectName == "TerminalA") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Leader](This terminal was last set to a directory of all the drones working here.[P] But, they have no secondary designations, so it just a long list of numbers...)") ]])
	fnCutsceneBlocker()
	
--Terminal that unlocks the doors, IF the player has completed the west side of the facility.
elseif(sObjectName == "TerminalB") then

	--Variables.
	local iSawSouthernCoreCutscene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawSouthernCoreCutscene", "N")
	local iSawLightCutscene        = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawLightCutscene", "N")
	
	--Locked out.
	if(iSawSouthernCoreCutscene == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] ...[P] It's no good, this terminal is locked.[P] Can you hack it?[B][C]") ]])
		fnCutscene([[ Append("55:[VOICE|Tiffany] I could -[P] oh.[P] Great, a blue box.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] ...[P] Help a simple repair robot out?[B][C]") ]])
		fnCutscene([[ Append("55:[VOICE|Tiffany] Quantum blue box encryption.[P] The passphrase changes each time it's accessed, and is quantum-entangled to a box somewhere else.[B][C]") ]])
		fnCutscene([[ Append("55:[VOICE|Tiffany] So I'd have to hack it for every single action on it.[P] Every file access, every menu query.[P] It'd take weeks to do anything.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] So we need to find the blue box?[B][C]") ]])
		fnCutscene([[ Append("55:[VOICE|Tiffany] Or lift the security lockdown with an auth code.[P] In either case, we'd need access to the datacore.[P] Let's find another way.") ]])
		fnCutsceneBlocker()

	--Saw the datacore scene.
	elseif(iSawSouthernCoreCutscene == 1.0 and iSawLightCutscene == 0.0) then
	
		--Flags.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawLightCutscene", "N", 1.0)
		VM_SetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionA", "N", 1.0)
	
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] ...[P] This terminal is locked.[P] 55?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] I downloaded the auth codes for most of these terminals from the datacore.[P] I can lift the lockdown...[B][C]") ]])
		fnCutscene([[ Append("55:[E|Smirk] Got it.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()

		--Lights turn on.
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Enable Light", "LgtA00") ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Enable Light", "LgtA01") ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Enable Light", "LgtA02") ]])
		fnCutscene([[ AL_SetProperty("Enable Light", "LgtA03") ]])
		fnCutscene([[ AL_SetProperty("Enable Light", "LgtA04") ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Enable Light", "LgtA05") ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Enable Light", "LgtA06") ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Enable Light", "LgtA07") ]])
		fnCutsceneWait(25)
		fnCutsceneBlocker()
	
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
		fnCutscene([[ Append("55:[E|Smirk] The floor lights indicate which door each console is responsible for.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Guess we have to go around.[P] Where's our objective?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Northern end of this sector...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Show the target.
		Cutscene_CreateEvent("CameraEvent", "Camera")
			CameraEvent_SetProperty("Max Move Speed", 2.0) 
			CameraEvent_SetProperty("Focus Position", (19.75 * gciSizePerTile), (4.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneWait(125)
		fnCutsceneBlocker()
		
		--Show the target.
		Cutscene_CreateEvent("CameraEvent", "Camera")
			CameraEvent_SetProperty("Max Move Speed", 2.0) 
			CameraEvent_SetProperty("Focus Actor Name", "Christine")
		DL_PopActiveObject()
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
	
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Offended] Wait -[P] does this seem like a puzzle to you?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] It's called a security system, Christine.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Good.[P] [EMOTION|Christine|Angry]Because I HATE PUZZLES.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] ...[P] Sorry, lost my composure.[P][EMOTION|Christine|Neutral] Won't happen again.[P] Let's be off.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] (Yikes.)") ]])
		fnCutsceneBlocker()

	--All other cases.
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] (The door that the lights lead to should be open now...)") ]])
		fnCutsceneBlocker()
	end
	
--Opens doors.
elseif(sObjectName == "TerminalC") then

	--Variables.
	local iLRTCLightSectionB = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionB", "N")
	
	--Activate it.
	if(iLRTCLightSectionB == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionB", "N", 1.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Disabling the security lockout...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()

		--Lights turn on.
		fnCutscene([[ AL_SetProperty("Enable Light", "LgtB00") ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Enable Light", "LgtB01") ]])
		fnCutscene([[ AL_SetProperty("Enable Light", "LgtB02") ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Enable Light", "LgtB03") ]])
		fnCutsceneBlocker()

	--Already activated.
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] (The door that the lights lead to should be open now...)") ]])
		fnCutsceneBlocker()
	end
	
--Opens doors.
elseif(sObjectName == "TerminalD") then

	--Variables.
	local iLRTCLightSectionC = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionC", "N")
	
	--Activate it.
	if(iLRTCLightSectionC == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionC", "N", 1.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Disabling the security lockout...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()

		--Lights turn on.
		fnCutscene([[ AL_SetProperty("Enable Light", "LgtC00") ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Enable Light", "LgtC01") ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Enable Light", "LgtC02") ]])
		fnCutscene([[ AL_SetProperty("Enable Light", "LgtC03") ]])
		fnCutscene([[ AL_SetProperty("Enable Light", "LgtC04") ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()

	--Already activated.
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] (The doors that the lights lead to should be open now...)") ]])
		fnCutsceneBlocker()
	end
	
--Opens doors.
elseif(sObjectName == "TerminalE") then

	--Variables.
	local iLRTCLightSectionD = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionD", "N")
	
	--Activate it.
	if(iLRTCLightSectionD == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iLRTCLightSectionD", "N", 1.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Disabling the security lockout...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()

		--Lights turn on.
		fnCutscene([[ AL_SetProperty("Enable Light", "LgtD00") ]])
		fnCutscene([[ AL_SetProperty("Enable Light", "LgtD01") ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Enable Light", "LgtD02") ]])
		fnCutscene([[ AL_SetProperty("Enable Light", "LgtD03") ]])
		fnCutscene([[ AL_SetProperty("Enable Light", "LgtD04") ]])
		fnCutscene([[ AL_SetProperty("Enable Light", "LgtD07") ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Enable Light", "LgtD06") ]])
		fnCutscene([[ AL_SetProperty("Enable Light", "LgtD05") ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()

	--Already activated.
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] (The doors that the lights lead to should be open now...)") ]])
		fnCutsceneBlocker()
	end

-- |[Objects]|
--Locked doors.
elseif(sObjectName == "LockedDoor") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[SOUND|World|AutoDoorFail]Access denied.[P] Security lockdown in effect.") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "GenTermA") then
	fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Searching for logs on Unit 2855...)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Unit 2855 Reprimand Notice.[P] Apparently, she got into a fight with a bunch of Lord Units...)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (There is an implicit no-cutting-in-line rule at the cafeteria in the Geology Research Block of the Arcane University.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (2855 apparently saw a unit cut in line and proceeded to beat that unit, and other nearby units, into system standby until explicit orders from Central told her to stop.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Though it seems nobody has cut in line a single time since that day...)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "GenTermB") then
	fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Searching for logs on Unit 2855...)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A few fragmented logs on Unit 2855 including service commendations.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This log file indicates that unauthorized appropriations decrease in whatever sector she is stationed in by at least 25 percent.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (No action is required, merely stationing her causes the drop.[P] The log recommends creating Unit 2855 body-doubles to get the most of this.)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "GenTermC") then
	fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Searching for logs on Unit 2855...)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Part of a repair manual written by Unit 2855...)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (It logs proper care and maintenance of hyperweave clothing, as the unique magnetic refractor properties are less efficient when the clothing is dirty.)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "GenTermD") then
	fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A security log from Sector 388.[P] Apparently, that sector had several 'The Administration is Watching' motivational posters.[P] One of them got defaced by a Slave Unit.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The security cameras had been tampered with, but Unit 2855 tracked down the culprit and beat them to system standby in front of half of the sector's workers.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (From then on, 'Unit 2855 is Watching' posters were posted.[P] They were never defaced.)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "Fabricator") then
	fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A fabrication bench.[P] It hasn't been used in a while.)") ]])
	fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
