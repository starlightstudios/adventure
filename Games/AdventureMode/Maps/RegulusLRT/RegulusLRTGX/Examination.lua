-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
--Exit.
if(sObjectName == "ToLRTF") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusLRTF", "FORCEPOS:60.5x12.0x0")
    
elseif(sObjectName == "ToLRTZA") then
	AL_BeginTransitionTo("RegulusLRTZA", "FORCEPOS:4.5x6.0x0")

-- |[Objects]|
elseif(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Sad] Oh, the access terminal here is completely wrecked...[B][C]") ]])
    fnCutscene([[ Append("55:[E|Upset] It is fortunate that I brought a repair unit such as yourself.[P] I would have been unable to reach that conclusion otherwise.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Oilmaker") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Well, at least the oilmaker is untouched.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smirk] What comfort the Drone Units here will derive from that.") ]])
    fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
