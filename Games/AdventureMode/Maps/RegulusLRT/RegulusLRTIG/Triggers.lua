-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Big boss cutscene.
if(sObjectName == "MainScene") then
	
	--Variables.
	local iSawFirstEDGScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawFirstEDGScene", "N")
	
	--First time this scene fires.
	if(iSawFirstEDGScene == 0.0) then
		
		--Flags.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawFirstEDGScene", "N", 1.0)
		
		--Spawn the EDG. She is referred to as "Vivify" later.
		TA_Create("EDG")
			TA_SetProperty("Position", -100, -100)
			TA_SetProperty("Facing", gci_Face_North)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/Vivify/", false)
            TA_SetProperty("Rendering Offsets", gcfTADefaultXOffset - 9.0, gcfTADefaultYOffset - 18.0)
		DL_PopActiveObject()
		
		--Move the party up.
		fnCutsceneMove("Christine", 12.25, 7.50)
		fnCutsceneMove("Tiffany", 13.25, 7.50)
		fnCutsceneBlocker()

		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
		fnCutscene([[ Append("55:[E|Neutral] Access permissions check...[P] interface check...[P] I can plug directly into the core from here...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Superb![P] Do you need my help?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Actually the PDU would be more useful.[P] It can interface directly with the system.[P] Plug it in over there.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Affirmative.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] The security isn't very good.[P] I don't think they expected a hacking attempt from a terminal right in the core.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
        
        --Begin animating the text.
        local iWaitTime = 25
		fnCutscene([[ AudioManager_PlaySound("World|TerminalOn") ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "AnimLayer5", false) ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "AnimLayer5", true) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "AnimLayer6", false) ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "AnimLayer6", true) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "AnimLayer0", false) ]])
		fnCutsceneWait(45)
		fnCutsceneBlocker()
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "AnimLayer0", true) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "AnimLayer1", false) ]])
        fnCutsceneWait(iWaitTime)
        fnCutsceneBlocker()
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "AnimLayer1", true) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "AnimLayer2", false) ]])
        fnCutsceneWait(iWaitTime)
        fnCutsceneBlocker()
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "AnimLayer2", true) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "AnimLayer3", false) ]])
        fnCutsceneWait(iWaitTime)
        fnCutsceneBlocker()
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "AnimLayer3", true) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "AnimLayer4", false) ]])
        fnCutsceneWait(iWaitTime)
        fnCutsceneBlocker()
		fnCutsceneWait(45)
		fnCutsceneBlocker()

		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
		fnCutscene([[ Append("55:[E|Neutral] Interesting...[P] PDU, download that section...[P] Yes, as I suspected...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] (...?[P] I hear something...)") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Kill the music.
        fnCutsceneWait(45)
        fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Music", "Null") ]])
		
		--Christine moves south.
		fnCutsceneMove("Christine", 12.25, 8.50, 0.50)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", -1, 1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", 1, 1)
		fnCutsceneWait(65)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", 0, -1)
		fnCutsceneWait(65)
		fnCutsceneBlocker()

		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Sad] Uhh, 55?[P] We may have a problem.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] What is it?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] I hear -[P] singing...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Face south. Sound effect.
		fnCutscene([[ AudioManager_PlaySound("World|ThingSmash") ]])
		fnCutsceneFace("Christine", 0, 1)
		fnCutsceneWait(105)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Music", "Vivify") ]])
		fnCutsceneWait(95)
		fnCutsceneBlocker()
        
        --Screen animates.
		fnCutscene([[ AudioManager_PlaySound("World|TerminalOff") ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "AnimLayer4", true) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "AnimLayer6", false) ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "AnimLayer6", true) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "AnimLayer5", false) ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "AnimLayer5", true) ]])
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		fnCutscene([[ AudioManager_PlaySound("World|TerminalOn") ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "AnimLayer4", true) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "AnimLayer5", false) ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "AnimLayer5", true) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "AnimLayer6", false) ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "AnimLayer6", true) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "AnimLayer7", false) ]])
		fnCutsceneWait(35)
		fnCutsceneBlocker()
		
		--Screen shows up.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Doll:[VOICE|Tiffany] Unit 2855![P] Get Christine out of there, right now![B][C]") ]])
		fnCutscene([[ Append("Doll:[VOICE|Tiffany] I was such an idiot.[P] I've lost control of the creature and it's coming towards you![P] You must not let the creature come in contact with Christine![B][C]") ]])
		fnCutscene([[ Append("Doll:[VOICE|Tiffany] I repeat::[P] Do not let the creature come in contact with Christine![P] We can't predict what will happen if it does!") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		fnCutscene([[ AudioManager_PlaySound("World|TerminalOff") ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "AnimLayer7", true) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "AnimLayer6", false) ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "AnimLayer6", true) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "AnimLayer5", false) ]])
		fnCutsceneWait(5)
		fnCutsceneBlocker()
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "AnimLayer5", true) ]])
		fnCutsceneWait(65)
		fnCutsceneBlocker()

		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] She's singing...[P] But it's not like last time.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] It's strained.[P] She's angry.[P] We upset her.[P] We did something we should not have.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] What?[P] What did we do?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] We fooled her.[P] Fooled her and now she's coming.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Who the hell sent that message...[P] Christine, we need to leave![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] That would make her mad.[P] We don't want to make her mad.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Damn it, not now![P] PDU, cancel all queries.[P] Download every file related to Project Vivify, right now![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] It's coming...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Teleport the EDG in.
		fnCutsceneTeleport("EDG", 12.75, 18.50)
		fnCutsceneBlocker()
		fnCutsceneMove("Christine", 12.75, 11.50, 0.40)
		fnCutsceneBlocker()
		fnCutsceneWait(125)
		fnCutsceneBlocker()
		fnCutsceneMove("EDG", 12.75, 14.50, 0.50)
		fnCutsceneBlocker()
		fnCutsceneWait(105)
		fnCutsceneBlocker()

		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
		fnCutscene([[ Append("55:[E|Neutral] Still downloading...[P] Christine, try to distract it![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Distract...[P] my master?") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Move Christine south.
		fnCutsceneMove("Christine", 12.75, 12.50, 0.20)
		fnCutsceneBlocker()
		fnCutsceneWait(65)
		fnCutsceneBlocker()

		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] I can hear her...[P] she whispers...[P] garbled...[P] but it all makes sense...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] It was inside all along.[P] Oh.[P] Yes, it was between the between.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] But I'm not empty yet.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Please, teach me.[P] Teach me how to collect the gaps.[P] I'll be good.[P] I won't run away.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(65)
		fnCutsceneBlocker()
		
		--Move 55 south.
		fnCutsceneMove("Tiffany", 12.75, 12.50, 2.50)
		fnCutsceneBlocker()
		fnCutsceneMoveFace("Christine", 12.75, 11.50, 0, 1, 1.50)
		fnCutsceneWait(25)
		fnCutsceneBlocker()

		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
		fnCutscene([[ Append("55:[E|Upset] What are you doing?[P] Get your weapon out![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] We can't fight her, she's already empty...[B][C]") ]])
		fnCutscene([[ Append("55:[E|Upset] Christine, snap out of it![P] Right now![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] ...[B][C]") ]])
		fnCutscene([[ Append("55:[E|Upset] You'll never make it back to Sophie if you listen to this thing![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Blush] S-[P]Sophie, my precious Sophie...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] If -[P] Sophie is waiting for me to come back to her.[P] 55?[P] 55 are you there?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Wake up![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] We -[P] we have to destroy this thing, 55![P] We have to chop it into pieces so it can't be put back together![B][C]") ]])
		fnCutscene([[ Append("55:[E|Angry] Welcome back to the real world.[P] It seems your little vacation has pissed it off.[P] Weapon up, right now!") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
        
        --Battle.
		fnCutscene([[ AdvCombat_SetProperty("Next Combat Music", "RottenTheme", 0.0000) ]])
        fnCutscene([[ AdvCombat_SetProperty("World Pulse", true) ]])
        fnCutscene([[ AdvCombat_SetProperty("Reinitialize") ]])
        fnCutscene([[ AdvCombat_SetProperty("Activate") ]])
        fnCutscene([[ AdvCombat_SetProperty("Unretreatable", true) ]])
        fnCutscene([[ AdvCombat_SetProperty("Victory Script", gsRoot .. "Maps/RegulusLRT/RegulusLRTIG/Combat_VictoryA.lua") ]])
        fnCutscene([[ AdvCombat_SetProperty("Defeat Script",  gsRoot .. "Maps/RegulusLRT/RegulusLRTIG/Combat_DefeatA.lua") ]])
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 5/000 Scripted Enemies/Boss Vivify.lua") ]])
        fnCutsceneBlocker()

	end
end
