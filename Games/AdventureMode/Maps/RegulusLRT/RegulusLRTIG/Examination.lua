-- |[ ====================================== Examination ======================================= ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "ToLRTIF") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusLRTIF", "FORCEPOS:10.0x23.0x0")

-- |[ ======================================== Console ========================================= ]|
elseif(sObjectName == "Console") then
    local iSaw55sMemories = VM_GetVar("Root/Variables/Chapter5/Scenes/iSaw55sMemories", "N")
    if(iSaw55sMemories == 0.0) then
        LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/500 Normal/LRT Datacore/Scene 4 After Boss.lua")
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (A data tap has been installed, we can easily access the database remotely.[P] There's no further need to access the console.)") ]])
        fnCutsceneBlocker()
    end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
