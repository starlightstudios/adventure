-- |[Combat Defeat]|
--The party was defeated.
local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsRelivingScene", "N")

--Music change.
AudioManager_PlayMusic("Null")

-- |[Images]|
fnLoadDelayedBitmapsFromList("Chapter 5 Christine Dreamer TF", gciDelayedLoadLoadAtEndOfTick)

--Clear censor bars.
WD_SetProperty("Clear Censor Bars")

-- |[Movement]|
--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--55 Collapses.
if(iIsRelivingScene == 0.0) then
    VM_SetVar("Root/Variables/Chapter5/Scenes/iLRTBossResult", "N", 3.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N", 17.0)
    Cutscene_CreateEvent("ActorEvent", "Actor")
        ActorEvent_SetProperty("Subject Name", "Tiffany")
        ActorEvent_SetProperty("Special Frame", "Crouch")
    DL_PopActiveObject()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    Cutscene_CreateEvent("ActorEvent", "Actor")
        ActorEvent_SetProperty("Subject Name", "Tiffany")
        ActorEvent_SetProperty("Special Frame", "Wounded")
    DL_PopActiveObject()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Christine moves up.
    fnCutsceneMoveFace("Tiffany", 12.75, 10.50, 0, 1, 0.30)
    fnCutsceneMove("Christine", 12.75, 12.50, 0.50)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I'm sorry.[P] Please don't hurt 55.[P] She doesn't know you like I do.") ]])
    fnCutsceneBlocker()

    --Christine's form. Transform her to human if she isn't already.
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    if(sChristineForm ~= "Human") then
        
        --Wait a bit.
        fnCutsceneWait(30)
        fnCutsceneBlocker()

        --Flash the active character to white. Immediately after, execute the transformation.
        Cutscene_CreateEvent("Flash Christine White", "Actor")
            ActorEvent_SetProperty("Subject Name", "Christine")
            ActorEvent_SetProperty("Flashwhite Quickly")
        DL_PopActiveObject()
        fnCutsceneWait(5)
        fnCutsceneBlocker()
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
        fnCutsceneWait(gci_Flashwhite_Ticks_Total)
        fnCutsceneBlocker()

        --Now wait a little bit.
        fnCutsceneWait(30)
        fnCutsceneBlocker()
        
    end

    --Dialogue.
    fnCutscene([[ AudioManager_PlayMusic("Vivify") ]])
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] ...[P] Yes Master.[P] Thank you...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(30)
    fnCutsceneBlocker()

    --Fade to black.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
end

--Transformation scene.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ Append("Christine lost consciousness...[P] No...[P] She was awake...[P] But the world had gone dark...[P] It smelled...[P] Wrong...[B][C]") ]])
fnCutscene([[ Append("...[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] Urgh...[P] M...[P] my head...[P] Where...[P] Where am I?![P] 55?![B][C]") ]])
fnCutscene([[ Append("She looked about frantically as she called for her friend, but her voice caught in her throat as her eyes began to focus.[P] Her thoughts collided in her mind, fighting against the visions that surrounded her, refusing to believe her eyes were seeing correctly.[B][C]") ]])
fnCutscene([[ Append("The sights before her eyes were unlike anything she had seen on Regulus or Earth.[P] Writhing tendrils grew from walls of flesh as orb-like pustules shivered and pulsed with a familiar hue.[P] Wherever she was, she was no longer in the LRT facility, and she suspected she was no longer even in the same solar system.[B][C]") ]])
fnCutscene([[ Append("Before her, shrouded in darkness, was an enormous figure.[P] It stepped forth with an inaudible chuckle that seemed to resonate within Christine's mind.[B][C]") ]])
fnCutscene([[ Append("[P][P]Vivify.[B][C]") ]])
fnCutscene([[ Append("Christine opened her mouth to speak, to issue what challenge she could muster against Vivify, but found her captor vanished from sight.[P] She closed her mouth, confused, only for the abomination to appear directly in front of her as if she had blinked.[B][C]") ]])
fnCutscene([[ Append("With a drawn smile, the abomination returned its gaze to Christine.[P] It was grinning at her. Christine heard someone whisper, someone from far away in a familiar voice, but one she could not place.[P] There were no gods here.[P] If she had refused to learn before, she would now.[B][C]") ]])
fnCutscene([[ Append("Christine's eyes widened and her mouth fell agape as one of the tendrils attached to Vivify reached out and dangled slowly in front of her face.[P] Her body convulsed again as her head twisted away from the tendril.[B][C]") ]])
fnCutscene([[ Append("A forgotten instinct pushed a silent prayer to her mind that those [P][P]things[P][P] would not touch her.[B][C]") ]])
fnCutscene([[ Append("Vivify frowned, and the distant whisper returned to Christine's mind.[P] She was fighting.[P] She should not be fighting.[P] She could not refuse the lesson that was to come.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Null") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DreamerTF0") ]])
fnCutscene([[ Append("With a lunge, the tentacle wrapped itself around her waist.[P] The grip on her arms and legs slackened as the tendrils from the wall dutifully released their grip, and Christine gasped as she was jerked forward, nearer to the abomination that now tormented her.[B][C]") ]])
fnCutscene([[ Append("A second tentacle reached out to her, snaking around her waist once before sliding up across her stomach and under her shirt.[P] It wrapped itself around a single breast and squeezed at it gently as the tip continued upward before dipping behind her neck, only to reappear on her other shoulder where it brushed lightly against her cheek.[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] V-[P]vivify, please, don't![P] This -[P] what you're doing is wrong, you must be able to see that! [B][C]") ]])
fnCutscene([[ Append("The smile that had been drawn across Vivify's face fell briefly before twisting back into itself at odd angles.[P] The whisper returned, but slurred and with a hint of disinterest towards Christine.[P] She was exaggerating.[P] Over-reacting.[P] She should not do so towards her captor.[B][C]") ]])
fnCutscene([[ Append("With a jerk that threw her head back, Christine's body was pulled next to Vivify.[P] She opened her mouth to scream, but was silenced as the tentacle pushed her head forward, forcing her lips against her captor's all-too-eager lips.[B][C]") ]])
fnCutscene([[ Append("A cloud seemed to pass over her mind even as it clouded her eyes, and her body fell limp.[P] Only the tentacles that held her waist and head kept her from crumpling to the floor.[B][C]") ]])
fnCutscene([[ Append("The kiss drew her from her thoughts and held her captive.[P] Her mind numbed and her thoughts grew dull, leaving her consciousness soft and malleable.[P] She was nothing more than a bit of potter's clay that Vivify's skilled fingers twisted into a crude mockery of all she had once been.[B][C]") ]])
fnCutscene([[ Append("Slowly Vivify began to pull away, her broken mouth twisting into a smirk as one of the tentacles wiped a thin strand of saliva from her lips.[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] (What's...[P] Going on... [P]I can't...[P] Move...[P] Can't...[P] think...[P] Master...) [B][C]") ]])
fnCutscene([[ Append("Christine's eyes drifted upward to the face of her captor, where her empty gaze was met by the unfathomable depths within Vivify's impossible eyes.[P] A third tentacle began to wrap itself about her body as the second continued to massage her breast and caress her face.[B][C]") ]])
fnCutscene([[ Append("Her strength failed her, and within the back of her thoughts, where the last hint of herself lingered, she knew her body would not have complied even if she were not hurt and tired.[P] Instead, her body pressed itself invitingly against the abomination's probing grasp.  [B][C]") ]])
fnCutscene([[ Append("She moaned softly as the slimy tentacles rolled along her body and across her sides in an unending caress.[P] Wherever they touched they left a thin film that grew hot on her skin and seemed to increase her body's sensitivity.[P] Each inch that the tentacles crept along seemed to go painfully slow.[B][C]") ]])
fnCutscene([[ Append("Deep within her mind the last lingering point of sanity screamed at her, telling her to resist, to struggle, to fight.[P] The thought seared her mind as it clashed against the seductive calls that had infected her, and she cried out in mental anguish.[B][C]") ]])
fnCutscene([[ Append("The groping undulations of the tentacles ceased as her silent cry became vocal.[P] They pulled her away from Vivify.[P] Christine stared up into the depths of her face, and Vivify smiled her broken smile.[P] She held up a single finger and placed it against her captive's lips.[B][C]") ]])
fnCutscene([[ Append("A quiet thought pierced Christine's mind, seeking out this lingering ember of herself.[P] It penetrated deep within her thoughts to the very core of her psyche to where her vestigial consciousness yet lingered.[P] It chided her.[P] Scolded her.[P] Fighting would displease her master.[P] Her master required obedience.[P] She would be obedient.[B][C]") ]])
fnCutscene([[ Append("Again the ember flared to life and screamed at her, struggling against the probing thought that was as alien as it was familiar.[P] It burned into her mind as it sought to rekindle what remained of her fighting spirit.[P] It burned until her very essence began to hurt as the two thoughts collided. [B][C]") ]])
fnCutscene([[ Append("Her mind pleaded for mercy, begging to escape the burning thought even as it sought to rekindle itself.[P] An empty cry fell from her lips as her captor pulled her close again.[P] The finger was removed, and once more her lips were pressed against the lips of her captor.[B][C]") ]])
fnCutscene([[ Append("The kiss was cool against the burning ember within her mind.[P] It soothed her, welcomed her, and as a laggard smile crept across Christine's face,[P] the flame that was the last anchor that held her mind was smothered, and she reached eagerly for her master's embracing lips.[B][C]") ]])
fnCutscene([[ Append("Her body convulsed and her eyes grew wide as another of the tendrils had begun to probe her, wrapping gently about the stiffening nipples her clothes had sought to hide away.[P] The slime-enhanced sensitivity sent a surge of pleasure throughout her body with each flick, each followed by a new convulsion.[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] P-[P]please...[P] Don't stop![P] ... [B][C]") ]])
fnCutscene([[ Append("Vivify again chuckled within her mind...") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DreamerTF0") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DreamerTF1") ]])
fnCutscene([[ WD_SetProperty("Register Censor Bar", 565, 176, 188, 114) ]])
fnCutscene([[ Append("Before she could comprehend it, the tentacles and tendrils enveloped her body.[B][C]") ]])
fnCutscene([[ Append("They lashed out, grasping at her while rubbing every inch of her body.[P] They slid beneath her clothes, tearing the constricting shirt away.[P] They brushed over her breasts and flicked at her nipples, twisted around her legs and massaged her thighs as they slid playfully across the fabric that yet covered her dampening pussy.[B][C]") ]])
fnCutscene([[ Append("A pleading moan fell from her lips as the lustful passion began to well up within the deepest folds of her being.[P] Vivify looked on with a smile that was unseen by her captive's empty gaze, and was all too eager to comply.[B][C]") ]])
fnCutscene([[ Append("Her tentacles pulled her captive's legs apart, and in a single swift surge the soft material of her pants was torn from her legs.[P] A tentacle held her captive aloft by her raised arms as another probed the outer lips of her pleading pussy.[B][C]") ]])
fnCutscene([[ Append("Pulling the thin fabric of her underwear aside, a single tendril began pressing inside her.[P] It was thick and slick, and every inch within her wiggled, writhed, and throbbed with a greedy hunger.[P] A tingling sensation spread though the depths of her womanhood from the tentacle, and Christine's body tensed.[P] Her moans caught in her throat and her eyes shot wide.[B][C]") ]])
fnCutscene([[ Append("Christine stared down with equal greed.[P] The tentacle buried itself deeper within her, pulling itself inch by writhing inch into the depths of her welcoming womb.[P] Christine trembled.[P] Vivify moaned.[B][C]") ]])
fnCutscene([[ Append("But even as Christine's head lolled back, the small ember took light in the back of her mind.[B][C]") ]])
fnCutscene([[ Append("Perhaps it was renewed by the heat of the slime, or perhaps her captor's attention had been pulled away with her orgasm.[P] However it had occurred, the burning thought that would not let Christine be free returned, and she gasped as realization began to sweep over her.[P] Her breath hitched and she tried to squeeze her legs shut.[B][C]") ]])
fnCutscene([[ Append("Vivify frowned and the orgasmic pleasure that had covered her face washed away to be replaced by a terrible frustration.[P] She licked her lips and growled quietly.[P] The whispering dug itself into Christine's mind.[P] Christine was strong.[P] A kiss was not enough.[P] It understood this.[P] The abomination was stronger.[P] Other solutions would be had. [B][C]") ]])
fnCutscene([[ Append("The tentacle that had been within her began to writhe again, and with a single thrust, a cold fluid began to surge from it and into her body.[P] Christine cried out.[P] Her mind could scarcely comprehend the pleasure that was flooding her body as the squirming tentacle pressed itself deeper into her body and filled her with the cold fluid.[B][C]") ]])
fnCutscene([[ Append("A shiver of ecstasy danced its way along Christine's spine, and Vivify smiled once more.[P] She reached out to her captive and cupped her cheek in one hand.[P] Immediately the whispering voice in Christine's mind grew soft.[P] Again, she was ready to learn.[B][C]") ]])
fnCutscene([[ Append("The tentacle began pumping harder, and Christine's mind clouded over as more of the fluid filled her.[P] She could do nothing more than moan and squirm in bliss until a fresh orgasm coursed through her limbs and lingered deep within her muscles and bones.[B][C]") ]])
fnCutscene([[ Append("Her breath caught in her chest as the orgasm faded, and she coughed once, then gagged as her tongue seemed to fill her mouth as fully as the tentacle did her vagina, and she wondered if the tentacle might make its way through her enough to greet her tongue.[B][C]") ]])
fnCutscene([[ Append("Even as the warmth of a new orgasm began to build up within her at the thought of such penetration, the tentacle retracted, leaving Christine gaping as the liquid dripped from her now-leaking pussy.[P] She looked around, confused and disappointed as her body took sorrowful notice of how empty she felt without it.[B][C]") ]])
fnCutscene([[ Append("A wistful frown crossed her face as she looked at her master, but it was short-lived as her gaze fell upon why Vivify had pulled out.[P] From between her legs slid a long, hard, chitinous appendage.[P] It was shiny, slick, and tube-like, and immediately Christine knew what it would be used for.[B][C]") ]])
fnCutscene([[ Append("She spread her legs as much as her body would allow, her eyes never leaving her master's newest offering.[P] Vivify's cold eyes narrowed in glee as she watched her captive's awe as the alien protrusion grew longer and closer, until a small trickle of the slick, cold fluid leaked from its tip. [B][C]") ]])
fnCutscene([[ Append("Vivify would not leave Christine waiting.[P] She took hold of Christine roughly with her hands, grabbing at her hips and pulling her onto the alien appendage.[B][C]") ]])
fnCutscene([[ Append("Christine felt like she was being impaled.[P] Her body tensed as every muscle seemed to clench, and her mouth flew wide as if to scream, but before even a single sound could form within her a tentacle buried itself within her throat to fill the void.[B][C]") ]])
fnCutscene([[ Append("Tears stung her eyes and she squirmed in the vice-like grip of the tentacles.[P] She struggled against them, but with each push and pull, the tentacle dove deeper within her mouth and down her throat, all the while the alien[P] thing [P]thrust into her over and over and over again.[B][C]") ]])
fnCutscene([[ Append("The force of the thrusting rocked her entire body, and a searing light invaded her vision with each thrust.[P] She could feel her body filling with the strange fluid the tentacle in her mouth released.[P] Her stomach began to distend and her body shivered with the whimpering moans the tentacle kept buried within her.[B][C]") ]])
fnCutscene([[ Append("Even as her mind began to give way to the pleasures the thrusting provided her, a new tentacle reached up and plunged itself within her anus.[P] Her body convulsed with the whispers of another orgasm as brief hints of panic began to invade her addled mind.[B][C]") ]])
fnCutscene([[ Append("The panic began to grow as her vision took in a large, faintly-glowing sphere making its way through Vivify's appendage, and she began to struggle against the tentacles that held her.[B][C]") ]])
fnCutscene([[ Append("With each push and pull against her bonds, Vivify seemed to thrust harder and faster until the sphere reached the lips of her pussy.[P] It pressed against them, rolling through them and pressing, briefly, against her clitoris as it passed through and into her womb.[P] As it did, the whispers of the orgasm broke forth into a towering crescendo, and her entire body answered its call.[B][C]") ]])
fnCutscene([[ Append("The orgasm surged through her, racing from the tips of her toes to the top of her scalp, stretching into her fingers and emanating from every point of her body with an electric joy.[P] A strangled scream worked its way from behind the tentacle in her mouth, but as the orgasm passed, her mind went quiet.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DreamerTF1") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DreamerTF2") ]])
fnCutscene([[ WD_SetProperty("Send Censor Bars To Crossfade") ]])
fnCutscene([[ WD_SetProperty("Register Censor Bar", 590, 308, 55, 37) ]])
fnCutscene([[ WD_SetProperty("Register Censor Bar", 635, 473, 69, 46) ]])
fnCutscene([[ Append("Her body felt like it was made of air and she was drifting, floating through an endless universe of exploding stars and all-consuming black holes.[P] She watched eons pass, suns burn out and die, civilizations rise, grow fat, then fall under their own weight.[B][C]") ]])
fnCutscene([[ Append("Her stomach was warm and full, and a light glowed within it.[P] The life and power within this light sent shockwaves of knowledge through her and throughout the universe.[P] She watched herself disintegrate, be re-born, then shatter into an uncountable number of atoms, only to put herself back together in a thousand shapes that were as possible as they were impossible.[B][C]") ]])
fnCutscene([[ Append("She saw the illogical geometry of the sacred cities, and the guard dogs walk between the lines to protect their masters.[P] She saw them squirm, grow, reach out and multiply, and she reached out to be with them. [B][C]") ]])
fnCutscene([[ Append("The light in her stomach expanded as it pulsated, growing bigger by the second.[P] By the century.[P] By the millennia.[P] Christine could no longer tell.[P] Time had no meaning.[P] There was no sequence to it.[P] What was.[P] What is.[P] What will be.[P] She saw the universe expand, contract, explode, implode, be born, and die before her very eyes, and she understood. [B][C]") ]])
fnCutscene([[ Append("She looked up into Vivify's impossible eyes, and saw they made sense.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DreamerTF2") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DreamerTF3") ]])
fnCutscene([[ WD_SetProperty("Send Censor Bars To Crossfade") ]])
fnCutscene([[ Append("...[B][C]") ]])
fnCutscene([[ Append("Her master smiled gently at her, tilted her head to the side, and chuckled again within her mind.[P] Christine's mind was so racked by new experience that she had not noticed 55's recovery.[B][C]") ]])
fnCutscene([[ Append("She was ripped from her master and sent tumbling over the guard rail as 55 tackled her and threw her over the edge.[P] 55 cast one hate-filled glance back at Vivify before following.[B][C]") ]])
fnCutscene([[ Append("Why was she doing this?[P] Did she not understand?[P] Of course she did not.[P] 55 was a machine.[P] It would take special work to teach her.[P] Her master would - [P][CLEAR]") ]])
fnCutscene([[ Append("Christine's thoughts were cut short as 55 struck her head with her pulse diffractor's stock.[P] It disoriented her, but she was well beyond such primitive feelings as 'pain'.[B][C]") ]])
fnCutscene([[ Append("55 was dragging her back to the transit tunnels.[P] She did not resist.[P] 55 was unruly.[P] She would need to be tired before she would be pliant.[B][C]") ]])
fnCutscene([[ Append("Now 55 threw her down and busied herself with a nearby supply crate.[P] She tore through its contents before finding two bottles of cleaning fluid.[P] 55 poured them onto the concrete before Christine, and an opaque gas emerged from the mixture.[B][C]") ]])
fnCutscene([[ Append("The vapour smelled materialistic, and Christine would have been amused had 55 then not stuck her power discharge cable onto Christine's head and unleashed the full voltage.[B][C]") ]])
fnCutscene([[ Append("All at once the blur of emptiness cleared from her mind.[P] Christine began to scream and convulse as pain, loss, and lucidity returned to her...[B][C]") ]])
fnCutscene([[ Append("...") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Change Christine's form.
fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Eldritch.lua") ]])
fnCutsceneBlocker()

--Unlock Latex Drone form.
VM_SetVar("Root/Variables/Global/Christine/iHasEldritch", "N", 1.0)

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
fnCutscene([[ Append("Christine:[E|Neutral] ...[P] 55...[P] What...[P] What did you do?[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Confirm unit status.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] ...[P] Unit 771852...[P] Status is operational...[P] Cognitive routines at normal levels...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] 55 -[P] please tell me what you did...[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] I downloaded every file related to Project Vivify I could find.[P] The experiments proved useful.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Vivify is constantly in a state of REM sleep, and I determined that your brain patterns were in a similar phase.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Attempts to wake Vivify failed, but certain stimuli generated stronger responses than others.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] The most likely candidate was exposure to high concentrations of chlorine gas followed by a high-voltage shock administered to the cranium.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] The research was not finished before the Cryogenics incident took place.[P] It seems they were on the right track.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] So everything I saw, everything I was, was a dream?[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Or it was an alternate state of mind.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] All the things I learned are with me still, but I don't know them anymore.[P] When I sleep...[B][C]") ]])
fnCutscene([[ Append("55:[E|Upset] You will not.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Returning to REM sleep will likely return you to Vivify's influence.[P] Remain awake at all times.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] And if I defragment?[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] There is no data available as to the effects of defragmentation on an exposed golem.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] ...[P] I can't stay awake forever, 55...[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] No, but you can until we run our queries on the datacore.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] ...[P] Yes.[P] Yes, we can return.[P] I will do it for you.[P] Vivify...[P] I know she'll let us run our queries.[B][C]") ]])
fnCutscene([[ Append("55:[E|Upset] Any 'knowledge' gained in your altered state is highly suspect.[P] Ignore it.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] You just don't understand...[B][C]") ]])
fnCutscene([[ Append("55:[E|Upset] Nor have I any intention to understand.[P] What you know, or think you know, is responsible for the retirement of hundreds of golems.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Consider that carefully before putting any weight in it.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] ...[B][C]") ]])
fnCutscene([[ Append("55:[E|Upset] Now, follow my instruction without question.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] Yes, okay, 55...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] ...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] (I wonder if someday I can bring 55 around...)") ]])
fnCutsceneBlocker()
fnCutsceneWait(30)
fnCutsceneBlocker()

--Clean.
fnCutscene([[ fnUnloadBitmapsFromList("Chapter 5 Christine Dreamer TF") ]])

--If this is a relive case, end the handler here.
if(iIsRelivingScene == 1.0) then
    
	--Wait a bit.
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Return to the last save point and execute the post-script..
	fnCutscene([[ AL_BeginTransitionTo("LASTSAVE", gsStandardReliveEnd) ]])
	fnCutsceneBlocker()
    return
end

--Move to the save point outside.
fnCutscene([[ AL_BeginTransitionTo("RegulusLRTID", "FORCEPOS:4x4x0") ]])

--Restore the party's HP.
fnCutscene([[ AdvCombat_SetProperty("Restore Party") ]])

--Remove wounded frame.
fnCutsceneSetFrame("Tiffany", "Null")
