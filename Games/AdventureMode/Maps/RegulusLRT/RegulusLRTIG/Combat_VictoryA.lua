-- |[Combat Victory]|
--The party won!
VM_SetVar("Root/Variables/Chapter5/Scenes/iLRTBossResult", "N", 1.0)

--Music change.
AudioManager_PlayMusic("Null")

-- |[Movement]|
--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--Vivify runs off.
fnCutsceneMoveFace("EDG", 12.75, 18.50, 0, -1, 0.30)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
fnCutscene([[ Append("Christine:[E|Neutral] I'm sorry![P] Please don't be angry!") ]])
fnCutsceneBlocker()

--Vivify runs off.
fnCutsceneTeleport("EDG", -100.25, -100.50)
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutsceneFace("Tiffany", 0, -1)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Sad") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
fnCutscene([[ Append("55:[E|Neutral] Just what do you mean that you're sorry?[P] That thing was trying to retire us![B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] No, she isn't.[P] She's asleep, dreaming.[P] She doesn't know what she's doing.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] She -[P] there's so many of her.[P] Some of them are friendly.[P] Some of them want to help us.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] And how do you know that?[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] Because they told me![B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] The notes on Project Vivify suggest it has internal emotional contradictions, assuming it has emotions like a normal organic.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] We cannot, and should not, listen to it.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Do not show mercy if we encounter that creature again.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] She said she'd leave us alone until we're done here.[P] So, you can download whatever you need from the core.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Of course I'm going to trust a gibbering creature that speaks in a manner I can't even hear.[P] Sure.[P] Fine.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] I trust her.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] ...") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Play the common scene.
LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/500 Normal/LRT Datacore/Scene 4 After Boss.lua")
