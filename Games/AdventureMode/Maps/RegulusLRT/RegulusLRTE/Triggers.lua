-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Christine and 55 discuss what the drones do all day.
if(sObjectName == "ConversationTrigger") then

	--Variables.
	local iSawOfficeConversation = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawOfficeConversation", "N")
	
	--If we haven't seen the scene, play it.
	if(iSawOfficeConversation == 0.0) then
		
		--Flags.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawOfficeConversation", "N", 1.0)
		
		--Move forward.
		fnCutsceneMove("Christine", 28.25, 26.50)
		fnCutsceneMove("Tiffany", 28.25, 27.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", 0, 1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Hey, 55?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] What is it?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Is it me, or was there a lot of office furniture and meeting rooms on the way here?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Your assessment is correct.[P] This facility contains a great deal of such objects.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] But why?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] The LRT facility is staffed exclusively by Latex Drone units.[P] They are not particularly smart.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Is it because the security is so high here?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Yes.[P] Latex Drones do not ask difficult questions.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Smirk] I expect the office equipment is so they can be kept busy.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Doing what, meetings?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Smirk] Meetings, creating slide shows, discussing production targets.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Units with low intelligence aren't good for much else...") ]])
		fnCutsceneBlocker()
		
		--Move 55 onto Christine and fold.
		fnCutsceneMove("Tiffany", 28.25, 26.50)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
		
	end
end
