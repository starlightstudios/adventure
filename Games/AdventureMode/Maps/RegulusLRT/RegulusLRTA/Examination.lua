-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
--Exit.
if(sObjectName == "ToExteriorED") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusExteriorED", "FORCEPOS:58.0x7.0x0")

--Exit.
elseif(sObjectName == "ToLRTB") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusLRTB", "FORCEPOS:12.0x15.0x0")
    
-- |[Vents]|
elseif(sObjectName == "VentSW") then
	fnCrawlThroughVent(15.25, 11.50, 18.25, 5.50)
    
elseif(sObjectName == "VentNE") then
	fnCrawlThroughVent(18.25, 5.50, 15.25, 11.50)

-- |[Objects]|
--Locked doors.
elseif(sObjectName == "LockedDoor") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[SOUND|World|AutoDoorFail]Access denied.[P] Security lockdown in effect.") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 55?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] *tap tap tap*[B][C]") ]])
    fnCutscene([[ Append("55:[E|Upset] No good, the entire system is on lockdown.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Upset] I'll need a terminal closer to the core to access it with a dataspike.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] The core is on the northwestern end of the facility.[P] With the doors locked down, we will need to use the ventilation systems to access it.") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "TerminalB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The visitor login terminal.[P] It's currently locked down.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "TerminalC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Please select a videograph to view while your security escort is prepared.'[P] But the terminal is locked down, so no watching naughty slimes...)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "Junk") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A broken sensor node.[P] Not worth stealing.)") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
