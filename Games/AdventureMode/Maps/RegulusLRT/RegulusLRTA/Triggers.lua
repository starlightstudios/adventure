-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "WarpActivate") then
    fnWarpUnlockHandler("Root/Variables/Chapter5/Campfires/iRegulusLRTA", 9.25, 6.50)
    
--Opening cutscene.
elseif(sObjectName == "EntryScene") then

	--Variables.
	local iSawLRTOpening = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawLRTOpening", "N")
	
	--Hasn't seen the scene.
	if(iSawLRTOpening == 0.0) then
		
		--Flags.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawLRTOpening", "N", 1.0)
        
        --Merge.
        fnCutsceneMergeParty()
        fnCutsceneBlocker()
	
		--Move the party.
		fnCutsceneMove("Christine", 12.25, 14.50)
		fnCutsceneMove("Tiffany", 11.25, 14.50)
		fnCutsceneBlocker()
        fnCutsceneFace("Christine", 0, -1)
        fnCutsceneFace("Tiffany", 0, -1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Is there nobody here?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Seems they haven't caught on to us yet...") ]])
		fnCutsceneBlocker()
		
		--Lights go off.
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Activate Lights") ]])
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "PDU", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
		fnCutscene([[ Append("Voice:[VOICE|Golem] Attention all units::[P] unidentified intruder detected in the facility.[P] All units to combat stations.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] You were saying?[B][C]") ]])
		fnCutscene([[ Append("55: ...[P] This was expected.[P] I'm just surprised they didn't sound the alarm when they saw us on the external cameras.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] So what do we do now?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] There is a remote-access terminal we can use in the northwestern corner of the facility.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] The security alert will have locked out the other terminals, but we might be able to find something useful on them anyway.[B][C]") ]])
		fnCutscene([[ Append("PDU:[E|Neutral][EMOTION|Christine|PDU] Correct.[P] I will be able to scan a terminal's cache from close range if required.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Okay, better get looking around then...[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Are you discomforted?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] Well, last time I was poking around a dark facility by myself...[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Don't be ridiculous.[P] This is nothing like that.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] Maybe not to you it isn't.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] You just stay close to me, okay?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Affirmative.[P] It would be dangerous to split up.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] Not what I meant...") ]])
		fnCutsceneBlocker()
		
		--Move 55 onto Christine.
		fnCutsceneMove("Tiffany", 12.25, 14.50)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
        
        fnCutscene([[ AL_SetProperty("Music", "LAYER|Telecomm") ]])
        fnCutscene([[ AL_SetProperty("Mandated Music Intensity", 0.0) ]])
	
    end

--Falling out of the vents.
elseif(sObjectName == "FallScene") then
    if(gbFallIntoLRTA == nil) then return end
    gbFallIntoLRTA = nil

    --Black the screen out.
    fnCutsceneTeleport("Christine", -100.25, -100.50)
    fnCutsceneTeleport("Tiffany", -100.25, -100.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneFace("Tiffany", 0, 1)
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 115.0) --Default is 5.0
        CameraEvent_SetProperty("Focus Position", (4.25 * gciSizePerTile), (11.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Fade back in.
    fnCutsceneWait(75)
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()

    --Christine teleports in and falls.
    fnCutscene([[ AudioManager_PlaySound("World|Fall") ]])
    fnCutsceneBlocker()
    for i = 1, 20, 1 do
        fnCutsceneTeleport("Christine", 4.75, 11.50 + (i * 3.0 / 20.0) - 3.0)
        fnCutsceneTeleport("Tiffany",   4.75, 11.50 + (i * 3.0 / 20.0) - 3.0)
        fnCutsceneWait(1)
        fnCutsceneBlocker()
    end
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Fold Party") ]])

end
