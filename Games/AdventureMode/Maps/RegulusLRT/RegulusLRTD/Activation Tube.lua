-- |[Activation Defrag]|
--Similar to the defragmentation chamber in Christine's apartment, this is an NPC. However, this is basically its
-- examination script.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Leader](A conversion tube.[P] It's receiving power and all systems are nominal.[P] It's ready to turn someone into a drone.)") ]])
	fnCutsceneBlocker()
	

end
