-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToLRTHB") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusLRTHB", "FORCEPOS:27.5x44.0x0")

-- |[Examinables]|
elseif(sObjectName == "Terminal") then

    --Variables.
    local iSawArmoryList = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawArmoryList", "N")
    if(iSawArmoryList == 0.0) then
    
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawArmoryList", "N", 1.0)
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "PDU", "Neutral") ]])
        fnCutscene([[ Append("55:[E|Neutral] Unit 771852, check the armory logs.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] No problem...[P] PDU?[B][C]") ]])
        fnCutscene([[ Append("PDU:[E|Neutral] Encryption bypassed.[P] I do so enjoy burglary.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Are you serious, PDU?[B][C]") ]])
        fnCutscene([[ Append("PDU:[E|Happy] Humour protocols remain active.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] ...[P] Too bad, 55.[P] The security units took everything not nailed down.[P] Most of the armory is what we saw outside, melted.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I was not expecting an equipment upgrade.[P] Stealing these weapons could be useful for a later armed insurrection, but is not vital to our current operation.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] This does mean that the security teams were insufficient.[P] They had time to arm themselves, and were not caught out of position.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] Why is that making you smile?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] It means that they are similarly unprotected elsewhere.[P] The LRT facility has a number of elite units stationed at it for obvious reasons.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] If we were to unleash Project Vivify on a facility of our choosing...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] No.[P] Absolutely not.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Merely a suggestion.[P] Controlling the creature is extremely dangerous, I would not advise it unless we were to learn a direct, safe method of command.[P] But, were such an opportunity to present itself...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Angry] Still no![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Angry] She is not an attack dog, 55![P] What is wrong with you?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I am being practical.[P] If we intended to destroy her, sending her at our enemies before doing so would deal them crippling damage.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] It is very easy to win a three-way battle if you can choose when to engage.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] I absolutely forbid it.[P] Don't mention such a thing again.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] As you like.") ]])
    
    --Repeats.
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Christine:[VOICE|Leader] (The armory has been stripped of firearms, according to the logs.[P] There may be something useful if we search.)") ]])
        
    end
    
elseif(sObjectName == "EMPBox") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
        fnCutscene([[ Append("55:[E|Upset] EMP grenades, but the box is empty.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] EMP grenades wouldn't work against an organic that spits acid, would they?[P] Am I forgetting something?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Your assessment is correct.[P] This indicates that the security units did not know they were dealing with an organic enemy.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Or, they were sufficiently desperate.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Well, there's none left.[P] Moving on.") ]])
        
elseif(sObjectName == "ImplosionBox") then

    --Variables.
    local iGotImplosionGrenades = VM_GetVar("Root/Variables/Chapter5/Scenes/iGotImplosionGrenades", "N")
    if(iGotImplosionGrenades == 0.0) then
    
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iGotImplosionGrenades", "N", 1.0)
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
        fnCutscene([[ Append("55:[E|Neutral] Ah, the so called 'Seris Class Anti-Pressure Grenade'.[P] Not many of these were manufactured.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] So those are the 'Implosion Grenades' that you've been chucking about recklessly.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Is there a reason they weren't mass-manufactured?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] When used carelessly, they are extremely dangerous and tend to destroy targets intended to be incapacitated.[P] They are not useful for riot control.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] When detonating, the grenade temporarily generates a microsingularity for 2-3 zeptoseconds.[P] Targets within range are pulled into the center, usually colliding with one another.[P] The more targets affected, the more catastrophic the impact.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] Yeah, throwing that in a crowded room would get pretty messy...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Failing their original purpose, not many were produced.[P] It seems the security units here decided not to equip them.[P] So much the better for us.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] While I don't have a problem with stealing them for use in a later rebellion...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Did you consider that the reason the security forces don't use them might also apply to us?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I did.[P] Minimizing enemy casualties is not a concern of ours, it is a concern of theirs.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Wild idea here.[P] Why don't we just detonate a nuclear weapon in 'their' territory?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] And all the innocent units will melt, and the territory we intend to capture for the good of everyone will be irradiated?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] After all, it doesn't matter how you win, right?[P] So long as you win?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I see.[P] You think the anti-pressure grenades may have negative long-term side effects.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Angry] No, I think killing people is the long-term side effect![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Angry] What's the point of overthrowing the administration if we're even less responsible, more brutal![P] Even they don't use these grenades![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Angry] And all those innocent units you hurt, or the friends and families of the units you killed instead of capturing, oh, they'll be so pleased![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Within one revolution, we sow the seeds of the next one.[P] And rightly so![B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I see.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I do not agree with your assessment, but I respect your passion for it.[P] Perhaps we can find an industrial or engineering use for the weapons.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] At the very least, we will secure them later to prevent the enemy using them on us.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] That's better, 55.[P] Sorry I flew off the handle.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] You made several points I am unable to answer at this time.[P] I will debate this with you later.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Yeah yeah, let's get going.") ]])
    
    --Repeats.
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Microsingularities sound kind of absurdly dangerous...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] The size of the singularity versus its mass means it persists for an extremely brief time.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Most of the grenade's casing it designed to contain the singularity and provide mass for it.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Unfortunately, the laws of physics do not make it possible to modify the grenade to exhibit more power.[P] Keeping the singularity active for longer requires magnitudinal increases in mass.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] How reassuring...") ]])
        
    end

elseif(sObjectName == "FlashbombBox") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] Flash Grenades, designed to shut down a unit's optical receptors.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Ineffective against properly armored units, designed for suppressing civilians.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Would they be of any use to us?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Not when the security teams are using them.[P] The boxes are empty.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Dash it.[P] Let's keep going.") ]])
    
elseif(sObjectName == "Gloves") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Only the special radiation-resistant gloves are here.[P] They're not suited for combat.[P] All the other armor was stripped.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ArmorRacks") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The flak armor racks are bare.)") ]])
    fnCutsceneBlocker()
        
elseif(sObjectName == "Guns") then

    --Variables.
    local iScavengedArmoryGuns = VM_GetVar("Root/Variables/Chapter5/Scenes/iScavengedArmoryGuns", "N")
    
    --First time, get an item.
    if(iScavengedArmoryGuns == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iScavengedArmoryGuns", "N", 1.0)

        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
        fnCutscene([[ Append("55:[E|Neutral] Stripped.[P] The only remaining firearms are ones noted to be defective and in need of maintenance.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Let me take a look...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Hmm, no,[P] hmm, maybe...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Can you perhaps use this Magrail Harmonizer?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] It seems the quartermaster neglected to remove it.[P] These are carefully controlled, meant only for elite security teams.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] Judging from what happened outside...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] They will have no further use for it.[P] Let us proceed.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] [SOUND|World|TakeItem](Received Magrail Harmonizer Module)") ]])
        LM_ExecuteScript(gsItemListing, "Magrail Harmonization Module")
    
    --Repeats.
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (These guns are defective and I've already removed the useful parts.)") ]])
        fnCutsceneBlocker()

    end

elseif(sObjectName == "LockedDoor") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[SOUND|World|AutoDoorFail]Access denied.[P] Security lockdown in effect.") ]])
	fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
