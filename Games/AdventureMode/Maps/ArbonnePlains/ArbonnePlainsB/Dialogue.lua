-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    if(sActorName == "Beth") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        
        --Runestone quest:
        local iRunestoneQuest = VM_GetVar("Root/Variables/Chapter1/Polaris/iRunestoneQuest", "N")
        if(iRunestoneQuest == 2.0) then
            VM_SetVar("Root/Variables/Chapter1/Polaris/iRunestoneQuest", "N", 3.0)
            VM_SetVar("Root/Variables/Chapter1/Polaris/iPolarisRuneQuestState", "N", 4.0)
            fnCutscene([[ Append("Beth:[VOICE|CultistF] Greaseflower?[P] Oh yes, you can find some of that just northwest of the farm![B][C]") ]])
            fnCutscene([[ Append("Beth:[VOICE|CultistF] Go up to pa's farm, then head west, then north past the old guard post.[P] There's a clearing where the all kinds of flowers grow.[B][C]") ]])
            fnCutscene([[ Append("Beth:[VOICE|CultistF] Greaseflower is a white flower with four points.[P] It grows fairly close to the water.[B][C]") ]])
            fnCutscene([[ Append("Beth:[VOICE|CultistF] Oh, dinner should be ready soon -[P] but I'm not done with these lilypads![P] Choices, choices...") ]])
        
        elseif(iRunestoneQuest == 3.0) then
            fnCutscene([[ Append("Beth:[VOICE|CultistF] Go up to pa's farm, then head west, then north past the old guard post.[P] There's a clearing where the all kinds of flowers grow.[B][C]") ]])
            fnCutscene([[ Append("Beth:[VOICE|CultistF] Greaseflower is a white flower with four points.[P] It grows fairly close to the water.") ]])
        
        --Normal case:
        else
            fnCutscene([[ Append("Beth:[VOICE|CultistF] Flowers are just lovely.[P] I'd like to be an alraune someday, but not until I've had a few kids of my own.[B][C]") ]])
            fnCutscene([[ Append("Beth:[VOICE|CultistF] Wish I didn't have to wait...") ]])
        end
    end
end
