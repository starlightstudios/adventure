-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Jean]|
    if(sActorName == "Jean") then
        
        --Common.
        TA_SetProperty("Face Character", "PlayerEntity")
        
        --Variables.
        local iMetJean = VM_GetVar("Root/Variables/Chapter1/OakFarm/iMetJean", "N")
        local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
        
        --Bee:
        if(sMeiForm == "Bee") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Jean", "Neutral") ]])
            if(iMetJean == 0.0) then
                fnCutscene([[ Append("Werecat:[E|Neutral] Hit the road, bee![P] Lay a finger on my kids and you'll have me to deal with!") ]])
            else
                fnCutscene([[ Append("Jean:[E|Neutral] Hit the road, bee![P] Lay a finger on my kids and you'll have me to deal with!") ]])
            end
            fnCutsceneBlocker()
        
        --Slime:
        elseif(sMeiForm == "Slime") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Jean", "Neutral") ]])
            if(iMetJean == 0.0) then
                fnCutscene([[ Append("Werecat:[E|Neutral] Shoo, shoo![P] Bad slime![P] Go away!") ]])
            else
                fnCutscene([[ Append("Jean:[E|Neutral] Shoo, shoo![P] Bad slime![P] Go away!") ]])
            end
            fnCutsceneBlocker()
        
        --All other cases.
        else
        
            --First time.
            if(iMetJean == 0.0) then
                VM_SetVar("Root/Variables/Chapter1/OakFarm/iMetJean", "N", 1.0)
                
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Jean", "Neutral") ]])
                fnCutscene([[ Append("Werecat:[E|Neutral] Hi there! Welcome to the Oak farm.[P] If you're looking for work, I'm afraid we're not hiring right now.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] You run the farm?[B][C]") ]])
                fnCutscene([[ Append("Jean:[E|Neutral] Sure do![P] Jean Oak, proprietor and co-owner with my lovely wife.[B][C]") ]])
                fnCutscene([[ Append("Jean:[E|Neutral] She's off on business at the moment, if you're looking for her.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Surprise] But you're...[P] a...[B][C]") ]])
                fnCutscene([[ Append("Jean:[E|Neutral] Cat?[B][C]") ]])
                if(sMeiForm ~= "Werecat") then
                    fnCutscene([[ Append("Mei:[E|Surprise] I'm not trying to be rude.[B][C]") ]])
                    fnCutscene([[ Append("Jean:[E|Neutral] Nah, it's nothin'.[B][C]") ]])
                else
                    fnCutscene([[ Append("Mei:[E|Surprise] Surely you would much prefer to catch a bird or a fish?[B][C]") ]])
                end
                fnCutscene([[ Append("Jean:[E|Neutral] I run this here farm for my kids, y'see.[P] Nia and Mark should be kickin' about, and pretty sure Beth is off looking at swamp flowers.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] You have three...[P] kids?[B][C]") ]])
                fnCutscene([[ Append("Jean:[E|Neutral] Nine boys, seven girls![P] Ha ha![P] Most of 'em have gone off to start their own families, 'course.[B][C]") ]])
                fnCutscene([[ Append("Jean:[E|Neutral] But they come visit every year and we have a real feast![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] I'm sorry, I just wasn't under the impression that monstergirls could have children.[B][C]") ]])
                fnCutscene([[ Append("Jean:[E|Neutral] 'Course they can't, silly![P] But I wasn't a cat my whole life.[P] This is a recent development.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] And the name Jean?[B][C]") ]])
                fnCutscene([[ Append("Jean:[E|Neutral] It worked when I was a man, may as well keep it.[P] Real pain to go around telling everyone to change all the paperwork.[B][C]") ]])
                fnCutscene([[ Append("Jean:[E|Neutral] You're not from around here, are you?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Nope.[P] Thanks for educating me on how things work.[B][C]") ]])
                fnCutscene([[ Append("Jean:[E|Neutral] Anytime, stranger.[P] Oh, and be careful if you're heading northwest. Pretty sure I saw smoke plumes that way.[B][C]") ]])
                fnCutscene([[ Append("Jean:[E|Neutral] Might be some kinda battle.[P] Best to steer clear of those.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Any idea who would be fighting?[B][C]") ]])
                fnCutscene([[ Append("Jean:[E|Neutral] Could be anyone.[P] There's always bad blood between city states, and Trannadar has no army to stop you from fighting here.[B][C]") ]])
                fnCutscene([[ Append("Jean:[E|Neutral] I'll be staying out of it, myself.[P] If you're going that way, keep a low profile.") ]])
                fnCutsceneBlocker()
                
            --Repeats.
            else
                
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Jean", "Neutral") ]])
                
                --Runestone quest:
                local iRunestoneQuest = VM_GetVar("Root/Variables/Chapter1/Polaris/iRunestoneQuest", "N")
                if(iRunestoneQuest == 1.0) then
                    VM_SetVar("Root/Variables/Chapter1/Polaris/iRunestoneQuest", "N", 2.0)
                    VM_SetVar("Root/Variables/Chapter1/Polaris/iPolarisRuneQuestState", "N", 3.0)
                    fnCutscene([[ Append("Jean:[E|Neutral] Howdy there![P] What can I do for you?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Miss Polaris told me you might know where some greaseflower is.[P] I'm supposed to get some for her.[B][C]") ]])
                    fnCutscene([[ Append("Jean:[E|Neutral] Oh no, I wouldn't know where that is.[P] Beth probably does![B][C]") ]])
                    fnCutscene([[ Append("Jean:[E|Neutral] She's down at the swamp edge lookin' at flowers.[P] Go ask her, and tell her dinner will be ready soon.") ]])
                    fnCutsceneBlocker()
                
                elseif(iRunestoneQuest == 2.0 or iRunestoneQuest == 3.0) then
                    fnCutscene([[ Append("Jean:[E|Neutral] Beth'll be just south of the chicken coop, lookin' at swamp flowers I reckon.") ]])
                
                --Normal case:
                else
                    fnCutscene([[ Append("Jean:[E|Neutral] Beth has been reading books on all kinds of plants as of late.[P] She's been making medicines out of the ones that grow in the swamp.[B][C]") ]])
                    fnCutscene([[ Append("Jean:[E|Neutral] Crush up a Horsewhallop flower and put it in a bread dough, you got yourself sausage loaf.[P] Mmmm mmm!") ]])
                    fnCutsceneBlocker()
                end
            end
        end
    
    elseif(sActorName == "Mark") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Mark:[VOICE|MercM] Big Brother Tom is off studying magic in Jeffespeir.[P] I think I want to stay here and run the farm, to be honest.") ]])
        fnCutsceneBlocker()
    
    elseif(sActorName == "Nia") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Nia:[VOICE|MercF] Chickens are dumb, but very endearing.[P] Mostly dumb, though.") ]])
        fnCutsceneBlocker()
    
    end
end
