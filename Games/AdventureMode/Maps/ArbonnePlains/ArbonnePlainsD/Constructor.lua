-- |[ ======================================= Constructor ====================================== ]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.
local sLevelMusic = "ForestTheme"
local sMapResolveName = "ArbonnePlainsD"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[ ======================================== Standard ======================================== ]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

    -- |[Construction]|
	--Music.
	AL_SetProperty("Music", sLevelMusic)
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
    
    -- |[Map Handling]|
    --Resolve the map's variable.
    fnStandardCh1Map(sLevelName)
	
	-- |[Spawn NPCS Here]|
	--NPCs set to spawn here will be available for post-exec cutscenes.

-- |[ ======================================== Post-Exec ======================================= ]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[Spawn NPCs Here]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    local iSawMediatorFight = VM_GetVar("Root/Variables/Chapter1/Scenes/iSawMediatorFight", "N")
    local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
    
    --Nobody spawns if Florentina is not present.
    if(bIsFlorentinaPresent == false) then
        
    --Florentina is present, haven't seen the big mediator scene yet:
    elseif(iSawMediatorFight == 0.0) then
        fnSpawnNPCPattern("AlrauneFight", "A", "F")
        fnSpawnNPCPattern("BeeFight", "A", "G")
        fnStandardNPCByPosition("Stana")
        fnStandardNPCByPosition("BeeFight0")
        fnStandardNPCByPosition("BeeFight1")
        fnStandardNPCByPosition("BeeFight2")
        fnStandardNPCByPosition("BeeFight3")
        fnStandardNPCByPosition("BeeFight4")
        fnStandardNPCByPosition("BeeFight5")
        fnStandardNPCByPosition("BeeFight6")
        EM_PushEntity("BeeFightF")
            TA_SetProperty("Set Ignore Clips", true)
        DL_PopActiveObject()
        EM_PushEntity("BeeFightG")
            TA_SetProperty("Set Ignore Clips", true)
        DL_PopActiveObject()
        EM_PushEntity("BeeFight5")
            TA_SetProperty("Set Ignore Clips", true)
        DL_PopActiveObject()
        EM_PushEntity("BeeFight6")
            TA_SetProperty("Set Ignore Clips", true)
        DL_PopActiveObject()
        
        --No autosaves.
        AL_SetProperty("Block Autosave Once")

    --Saw the mediator scene.
    else
        fnSpawnNPCPattern("AlrauneNormal", "A", "D")
        fnSpawnNPCPattern("BeeNormal", "A", "D")
        fnStandardNPCByPosition("Stana")
    end
end
