-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "SampleExit") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("LevelName", "FORCEPOS:28.0x16.0x0")
    
-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "Greaseflower") then
    local iRunestoneQuest = VM_GetVar("Root/Variables/Chapter1/Polaris/iRunestoneQuest", "N")
    if(iRunestoneQuest == 3.0) then
        VM_SetVar("Root/Variables/Chapter1/Polaris/iRunestoneQuest", "N", 4.0)
        VM_SetVar("Root/Variables/Chapter1/Polaris/iPolarisRuneQuestState", "N", 5.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Oh, this flower is slippery - it must be greaseflower!)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Narrator] [SOUND|World|TakeItem]Received greaseflower petals.") ]])
        fnCutsceneBlocker()

        --Item:
        LM_ExecuteScript(gsItemListing, "Greaseflower Petals")
    end

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
