-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Bees.
    if(sActorName == "BeeNormalA" or sActorName == "BeeNormalB" or sActorName == "BeeNormalC" or sActorName == "BeeNormalD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Bee:[VOICE|Bee] Zzzz!") ]])
    
    --Alraunes.
    elseif(sActorName == "AlrauneNormalA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Alraune:[VOICE|Alraune] There are infected only half a day's walk from here.[P] We need to move quickly.") ]])
        
    elseif(sActorName == "AlrauneNormalB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Alraune:[VOICE|Alraune] If you intend to investigate, be careful.[P] Do not add yourself to the ranks of the afflicted.") ]])
        
    elseif(sActorName == "AlrauneNormalC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Alraune:[VOICE|Alraune] Florentina...[P] The name of a hero!") ]])
        
    elseif(sActorName == "AlrauneNormalD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Alraune:[VOICE|Alraune] Yes, little ones, it's all right.[P] We'll never dream of fighting our bee friends ever again!") ]])
        
    elseif(sActorName == "Stana") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
        fnCutscene([[ Append("Stana:[E|Neutral] My friends, do you intend to investigate this plague?[B][C]") ]])
        
        --Variables.
        local iSawTowerIntro = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iSawTowerIntro", "N")
        local iRubberVictory = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iRubberVictory", "N")
        if(iSawTowerIntro == 0.0) then
            fnCutscene([[ Append("Florentina:[E|Neutral] We're trying to find Mei here a way back home, actually.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] We'd love to help![P] What do you need?[B][C]") ]])
            fnCutscene([[ Append("Stana:[E|Neutral] Information.[P] Of any kind.[B][C]") ]])
            fnCutscene([[ Append("Stana:[E|Neutral] The afflicted seem to wander about in a haze, mindlessly spreading the...[P] blight...[B][C]") ]])
            fnCutscene([[ Append("Stana:[E|Neutral] If there is a source of affliction, perhaps a cure could be devised?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Got it![P] We're on the job![B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] Hey, don't go thinking of me as some kind of hero.[P] Mei's the real deathwish lunatic.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] If we can help, we should![B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] I'm not saying no.[P] I don't want this plague getting to Trannadar.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] As long as you're willing to put finding a way home on the back burner.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] I think it can wait.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Sad] (Hmm, the feeling I normally get that pushes me to search -[P] I'm not getting it right now.)[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] (I wonder if I'm getting used to this place.)[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] We'll quiz the locals to the west and see what we can figure out.[P] You do your thing, Stana.[B][C]") ]])
            fnCutscene([[ Append("Stana:[E|Neutral] Indeed.") ]])
            fnCutsceneBlocker()
        
        --Saw tower, no rubber:
        elseif(iSawTowerIntro == 1.0 and iRubberVictory == 0.0) then
            fnCutscene([[ Append("Florentina:[E|Neutral] It seems to be coming from an old draconic guard post, Dormine Tower.[P] The woods around it are just lousy with rubber people.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] As for what it truly is, no ideas yet.[B][C]") ]])
            fnCutscene([[ Append("Stana:[E|Neutral] I see.[P] You have done us an invaluable service with this information.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] The rubbery goop on the ground seems inert, but the rubber people?[P] Very dangerous.[B][C]") ]])
            fnCutscene([[ Append("Stana:[E|Neutral] That is what we had observed. Still, the rubber likely relays information to the infected.[P] They seem to know we are approaching.[B][C]") ]])
            fnCutscene([[ Append("Stana:[E|Neutral] Be careful if you decide to return.[B][C]") ]])
        
        --Rubber victory:
        else
            fnCutscene([[ Append("Mei:[E|Offended] All will be subsumed by it.[P] You will enjoy it.[B][C]") ]])
            fnCutscene([[ Append("Stana:[E|Neutral] Excuse me?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Hm?[P] I didn't say anything.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] You just coughed.[B][C]") ]])
            fnCutscene([[ Append("Stana:[E|Neutral] But...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Please ignore me, I didn't mean to rudely cough like that.[B][C]") ]])
            fnCutscene([[ Append("Stana:[E|Neutral] All right then.") ]])
        
        end
    end
end
