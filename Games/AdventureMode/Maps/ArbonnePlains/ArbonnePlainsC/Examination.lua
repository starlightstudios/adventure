-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "SampleExit") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("LevelName", "FORCEPOS:28.0x16.0x0")
    
-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "SwitchA") then

    --Switch not flipped yet:
    local iExtendedBridge = VM_GetVar("Root/Variables/Chapter1/OakFarm/iExtendedBridge", "N")
    local iIsRubberMode = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
    if(iIsRubberMode == 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The switch is jammed...)") ]])
        fnCutsceneBlocker()
        
    elseif(iExtendedBridge == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter1/OakFarm/iExtendedBridge", "N", 1.0)
        
        --Flip to down, remove collisions.
        AudioManager_PlaySound("World|FlipSwitch")
        AL_SetProperty("Switch State", "SwitchA", false)
        AL_SetProperty("Set Collision", 51, 37, 1, 0)
        AL_SetProperty("Set Collision", 51, 38, 1, 0)
        AL_SetProperty("Set Collision", 45, 37, 1, 0)
        AL_SetProperty("Set Collision", 45, 38, 1, 0)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Cutscene.
        fnCutscene([[ AudioManager_PlaySound("World|RemoteDoor") ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "BridgeA", false) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "BridgeALower", false) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "BridgeNone", true) ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutscene([[ AudioManager_PlaySound("World|RemoteDoor") ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "BridgeA", true) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "BridgeALower", true) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "BridgeB", false) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "BridgeBLower", false) ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutscene([[ AudioManager_PlaySound("World|RemoteDoor") ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "BridgeB", true) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "BridgeBLower", true) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "BridgeC", false) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "BridgeCLower", false) ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    end

    AL_SetProperty("Switch Handled Update")

-- |[ ========================================== Other ========================================= ]|
-- |[Skillbook]|
elseif(sObjectName == "Skillbook") then
    AL_SetProperty("Set Layer Disabled", "SkillbookIco", true)
	LM_ExecuteScript(gsFlorentinaSkillbook, 6)
    
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
