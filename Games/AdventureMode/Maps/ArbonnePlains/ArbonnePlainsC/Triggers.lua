-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "Taunt") then
    
    --Repeat check.
    local iIsRubberMode   = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
    local iSawRubberTaunt = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iSawRubberTaunt", "N")
    if(iIsRubberMode == 0.0 or iSawRubberTaunt == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iSawRubberTaunt", "N", 1.0)
    
    --Scene.
    fnCutsceneMove("Mei", 43.25, 37.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Werecat", "Neutral") ]])
    fnCutscene([[ Append("Werecat:[E|Neutral] Oh no you don't![P] You keep your infected butt on that side of the river![B][C]") ]])
    fnCutscene([[ Append("Werecat:[E|Neutral] You're not getting across this way, so taff off!") ]])
    fnCutsceneBlocker()

end
