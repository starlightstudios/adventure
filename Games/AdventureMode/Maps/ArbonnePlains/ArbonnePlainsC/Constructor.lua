-- |[ ======================================= Constructor ====================================== ]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.
local sLevelMusic = "ForestTheme"
local sMapResolveName = "ArbonnePlainsC"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[ ======================================== Standard ======================================== ]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

    -- |[Construction]|
	--Music.
	AL_SetProperty("Music", sLevelMusic)
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
    
    -- |[Map Handling]|
    --Resolve the map's variable.
    fnStandardCh1Map(sLevelName)
	
	-- |[Spawn NPCS Here]|
	--NPCs set to spawn here will be available for post-exec cutscenes.

-- |[ ======================================== Post-Exec ======================================= ]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[Spawn NPCs Here]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    
    -- |[Layers]|
    local iExtendedBridge = VM_GetVar("Root/Variables/Chapter1/OakFarm/iExtendedBridge", "N")
    local iIsRubberMode = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
    if(iExtendedBridge == 0.0 or iIsRubberMode == 1.0) then
        AL_SetProperty("Set Collision", 51, 37, 1, 1)
        AL_SetProperty("Set Collision", 51, 38, 1, 1)
        AL_SetProperty("Set Collision", 45, 37, 1, 1)
        AL_SetProperty("Set Collision", 45, 38, 1, 1)
        AL_SetProperty("Set Layer Disabled", "BridgeA", true)
        AL_SetProperty("Set Layer Disabled", "BridgeB", true)
        AL_SetProperty("Set Layer Disabled", "BridgeC", true)
        AL_SetProperty("Set Layer Disabled", "BridgeALower", true)
        AL_SetProperty("Set Layer Disabled", "BridgeBLower", true)
        AL_SetProperty("Set Layer Disabled", "BridgeCLower", true)
    else
        AL_SetProperty("Set Layer Disabled", "BridgeA", true)
        AL_SetProperty("Set Layer Disabled", "BridgeB", true)
        AL_SetProperty("Set Layer Disabled", "BridgeALower", true)
        AL_SetProperty("Set Layer Disabled", "BridgeBLower", true)
        AL_SetProperty("Set Layer Disabled", "BridgeNone", true)
        AL_SetProperty("Switch State", "SwitchA", false)
    end
    
    --New NPC spawns in rubber mode.
    if(iIsRubberMode == 1.0) then
        fnStandardNPCByPosition("WerecatNPC")
    end
    
    -- |[Skillbook]|
    local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
    local iSkillbook6 = VM_GetVar("Root/Variables/Global/Florentina/iSkillbook6", "N")
    if(iSkillbook6 == 1.0 or bIsFlorentinaPresent == false) then
		AL_SetProperty("Set Layer Disabled", "SkillbookIco", true)
    end
end
