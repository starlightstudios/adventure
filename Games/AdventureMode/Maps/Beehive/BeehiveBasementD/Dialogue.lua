-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Clear censor bars.
WD_SetProperty("Clear Censor Bars")

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Set facing.
    local sActorName = TA_GetProperty("Name")
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[ZombeeNPCA / B]|
    if(sActorName == "ZombeeNPCA" or sActorName == "ZombeeNPCB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Zombee", "Neutral") ]])
        fnCutscene([[ Append("Drone:[VOICE|Bee][E|MC] (Drone obeys.[P] Drone obeys.[P] Capture bees.[P] Convert bees.[P] Drone obeys.)") ]])
        fnCutsceneBlocker()
    
    -- |[ZombeeNPCD]|
    elseif(sActorName == "ZombeeNPCD") then
    
        --Variables.
        local iConvertedVictimBee = VM_GetVar("Root/Variables/Chapter1/Scenes/iConvertedVictimBee", "N")
        
        --If the victim has not been converted yet:
        if(iConvertedVictimBee == 0.0) then
        
            --Setup.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Bee", "Neutral") ]])

            --Talking.
            fnCutscene([[ Append("Drone:[VOICE|Bee][E|MC] (Drone obeys.[P] Bee captured.[P] Bee will be converted.)[BLOCK]") ]])
        
            --Fade to black.
            fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
        
            --Decision script is this script. It must be surrounded by quotes.
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Drone Obeys\", " .. sDecisionScript .. ", \"DroneObeys\") ")
            fnCutsceneBlocker()
        
        --Already converted.
        else
        
            --Set facing.
            TA_SetProperty("Face Character", "PlayerEntity")
            
            --Setup.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Zombee", "Neutral") ]])

            --Talking.
            fnCutscene([[ Append("Drone:[VOICE|Bee][E|MC] (Drone obeys.[P] Bee already converted.[P] Drone obeys.[P] Capture bees.[P] Convert bees.[P] Drone obeys.)") ]])
            fnCutsceneBlocker()
        end
    end

--Conversion sequence.
elseif(sTopicName == "DroneObeys") then

	--Flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iConvertedVictimBee", "N", 1.0)

	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")
	
	--Scene.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
	fnCutscene([[ Append("The worker bee was wounded and helpless.[P] The drone obeyed.[P] The drone held the worker bee down, pushing her face close to the corrupted honey.[P] The drone obeyed.[B][C]") ]])
	fnCutscene([[ Append("Slowly, surely, a ghostly hand emerged from the honey.[P] The drone held the worker bee down.[P] The drone obeyed.[B][C]") ]])
	fnCutscene([[ Append("The hand pressed against the worker bee's face.[P] The bee tried to resist, but the drone held her down.[P] The drone obeyed.[B][C]") ]])
	fnCutscene([[ Append("The hand violated the worker bee's essence.[P] The drone obeyed.[P] The worker bee was converted.[P] The drone obeyed.") ]])
	fnCutsceneBlocker()
	
	--Switch the bee to use the zombee sprites.
	fnCutscene([[ 
	EM_PushEntity("ZombeeNPCD")
		fnSetCharacterGraphics("Root/Images/Sprites/BeeGirlZ/", false)
		TA_SetProperty("Auto Animates", false)
		TA_SetProperty("Y Oscillates", false)
	DL_PopActiveObject() ]])

	--Fade back in.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
		
	--Setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Zombee", "Neutral") ]])

	--Talking.
	fnCutscene([[ Append("Drone:[VOICE|Bee][E|MC] (Drone obeys.[P] Bee converted.[P] Drone obeys.[P] Capture bees.[P] Convert bees.[P] Drone obeys.)") ]])
	fnCutsceneBlocker()
end
