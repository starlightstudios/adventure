-- |[Constructor]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "Apprehension")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/BeehiveBasementD/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", "BeehiveBasementD")

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end
	
	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
    
    -- |[Map Handling]|
    --Resolve the map's variable.
    fnStandardCh1Map(sLevelName)

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then


	--If Mei is a Zombee, spawn some extra Zombee NPCs she can talk to. Well, "Talk".
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	if(sMeiForm == "Zombee") then
		
		--NPCs.
        fnStandardNPCByPosition("ZombeeNPCA")
        fnStandardNPCByPosition("ZombeeNPCB")
		
		--Victim Bee. If this flag is set, spawn as a zombee.
		local iConvertedVictimBee = VM_GetVar("Root/Variables/Chapter1/Scenes/iConvertedVictimBee", "N")
		if(iConvertedVictimBee == 1.0) then
            fnStandardNPCByPosition("ZombeeNPCD")
		
		--Otherwise, spawn as the victim. For simplicity, the name is the same.
		else
            fnStandardNPCByPosition("ZombeeNPCD")
			EM_PushEntity("ZombeeNPCD")
				fnSetCharacterGraphics("Root/Images/Sprites/BeeGirl/", false)
				TA_SetProperty("Auto Animates", true)
				TA_SetProperty("Y Oscillates", true)
			DL_PopActiveObject()
		end
		
		--Despawn the enemies.
		EM_PushEntity("ZombeeA")
			RE_SetDestruct(true)
		DL_PopActiveObject()
		EM_PushEntity("ZombeeB")
			RE_SetDestruct(true)
		DL_PopActiveObject()
		
	end

end
