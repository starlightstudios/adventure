-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "WarpActivate") then
    fnWarpUnlockHandler("Root/Variables/Chapter1/Campfires/iBeehiveBasementA", 22.25, 28.50)
    
--Brief cutscene if the player has not seen it yet.
elseif(sObjectName == "BeePanicScene" and iIsWholeCollision == 1.0) then

	--Variables.
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	local iHasSeenBeePanicScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenBeePanicScene", "N")
		
	--First time seeing the scene.
	if(iHasSeenBeePanicScene == 0.0) then
		
        --Flags.
        VM_SetVar("Root/Variables/Chapter1/Scenes/iZombeeQuestState", "N", 1.0)
    
		-- |[Florentina is not present]|
		--If Mei is alone, she can't proceed into the bee hive basement. Go get Florentina!
		if(bIsFlorentinaPresent == false) then
			
			--1.0 changes the level spawner and will change the Florentina version.
			VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenBeePanicScene", "N", 1.0)
			
			--Another script handles the events.
			LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/BeehiveBasement_NoFlorentina/Scene_Begin.lua")
		
		-- |[Florentina is present]|
		--This scene plays out a lot differently.
		else
			--2.0 means this scene won't play again.
			VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenBeePanicScene", "N", 2.0)
            VM_SetVar("Root/Variables/Chapter1/Scenes/iZombeeQuestState", "N", 2.0)
			
			--Another script handles the events.
			LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/BeehiveBasement_Florentina/Scene_Begin.lua")
		end
	
	--If this value is 1.0 and Florentina is present, play the third possible scene.
	elseif(iHasSeenBeePanicScene == 1.0 and bIsFlorentinaPresent == true) then
	
		--2.0 means this scene won't play again.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenBeePanicScene", "N", 2.0)
        VM_SetVar("Root/Variables/Chapter1/Scenes/iZombeeQuestState", "N", 2.0)
		
		--Another script handles the events.
		LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/BeehiveBasement_FlorentinaRound2/Scene_Begin.lua")
	end

--Zombee trigger. Causes a cutscene where Mei is relieved of her corruption.
elseif(sObjectName == "ZombeeTrigger" and iIsWholeCollision == 1.0) then

	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")

	--Mei must be a Zombee. Cutscene is in its own script.
	if(sMeiForm == "Zombee") then
		LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/BeehiveBasement_Zombee/Scene_Begin.lua")
	end
end
