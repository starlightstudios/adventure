-- |[Constructor]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "Apprehension")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/BeehiveBasementC/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", "BeehiveBasementC")

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end
	
	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
    
    -- |[Map Handling]|
    --Resolve the map's variable.
    fnStandardCh1Map(sLevelName)

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--If this door has been opened, remove it immediately.
	local iOpenedOddDoor = VM_GetVar("Root/Variables/Chapter1/Scenes/iOpenedOddDoor", "N")
	if(iOpenedOddDoor == 1.0) then
		AL_SetProperty("Open Door", "Door")
	end
	
	--If Mei is a zombee, remove all the chests.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	if(sMeiForm == "Zombee") then
		AL_RemoveObject("To Fake Chest", "ChestA")
		AL_RemoveObject("To Fake Chest", "ChestB")
		AL_RemoveObject("To Fake Chest", "ChestC")
	end
end
