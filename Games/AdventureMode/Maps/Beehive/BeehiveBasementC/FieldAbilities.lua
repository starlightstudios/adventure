-- |[ ===================================== Field Abilities ==================================== ]|
--Called when a field ability is used, at the discretion of the field ability script. The variable
-- gbFieldAbilityHandledInput should be set to true if the script handled the ability.

-- |[Arguments]|
--Argument Listing:
-- 0: iSwitchCode - Switch code used by the ability. One of the gciFieldAbility_Activate_[X] series.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local iSwitchCode = tonumber(LM_GetScriptArgument(0))

-- |[ ======================================== Handling ======================================== ]|
--Picking open the shortcut door.
if(iSwitchCode == gciFieldAbility_Activate_Florentina_PickLock) then
    
    -- |[Repeat Check]|
    --If the door is already open, stop.
    if(AL_GetProperty("Is Door Open", "Door") == true) then return end 
    
    -- |[Variables]|
    --Get the party leader's position.
    EM_PushEntity(gsPartyLeaderName)
        local iPartyX, iPartyY = TA_GetProperty("Position")
        local iFacing = TA_GetProperty("Facing")
    DL_PopActiveObject()
    
    -- |[Run Routine]|
    --This routine returns the name of the object it hit.
    local sHitObject = fnGetFirstObjectFromPosition(iPartyX, iPartyY, iFacing, {"Door"})
    
    -- |[Pickable Door]|
    --Handle if we got a hit!
    if(sHitObject == "Door") then
        
        --Flag.
        gbFieldAbilityHandledInput = true
        
        --Variables.
        local iDoorFromNorth = VM_GetVar("Root/Variables/Chapter1/Scenes/iDoorFromNorth", "N")
        
        -- |[Already Unlocked]|
        --Door can already be opened:
        local iOpenedOddDoor = VM_GetVar("Root/Variables/Chapter1/Scenes/iOpenedOddDoor", "N")
        if(iOpenedOddDoor == 1.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (The door is already unlocked.)") ]])
        
        -- |[Approaching from the North]|
        elseif(iDoorFromNorth == 1.0) then
            AL_SetProperty("Open Door", "Door")
            AudioManager_PlaySound("World|OpenDoor")
            fnStandardDialogue([[ [VOICE|Mei](The door has been unlocked.) ]])
            VM_SetVar("Root/Variables/Chapter1/Scenes/iOpenedOddDoor", "N", 1.0)

        -- |[Approaching from the South]|
        --Pick it.
        else
        
            --Check if Florentina is around. If not, we can't pick a lock.
            if(fnIsFlorentinaPresent() == false) then
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ Append("Thought:[VOICE|Leader] (Florentina can't pick the lock if she's not here!)") ]])
                fnCutsceneBlocker()
                return
            end
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter1/Scenes/iOpenedOddDoor", "N", 1.0)
            
            --Lock time!
            fnCutsceneMove("Mei", 13.25, 32.50)
            fnCutsceneFace("Mei", 0, -1)
            fnCutsceneMove("Florentina", 13.25, 31.50)
            fnCutsceneFace("Florentina", 0, -1)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Florentina] Huh, this is odd.[P] This lock was installed by someone with no talent for construction.[B][C]") ]])
            fnCutscene([[ Append("Thought:[VOICE|Mei] Is that a problem?[P] Can you get it open?[B][C]") ]])
            fnCutscene([[ Append("Thought:[VOICE|Florentina] What?[P] Yeah, it's already unlocked.[P] I didn't even really need the pick.[P] Just sloppy!") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            fnCutscene([[ AL_SetProperty("Open Door", "Door") ]])
            fnCutscene([[ AudioManager_PlaySound("World|OpenDoor") ]])
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Florentina] There you go!") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()

            --Fold.
            fnAutoFoldParty()
            fnCutsceneBlocker()
        end
    end
end
