-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Brief cutscene if the player has not seen it yet.
if(sObjectName == "BossBattleWarning" and iIsWholeCollision == 1.0) then

	--Play this brief scene if we haven't done so yet.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local iSeenFlorentinaWarning = VM_GetVar("Root/Variables/Chapter1/Scenes/iSeenFlorentinaWarning", "N")
	if(iSeenFlorentinaWarning == 0.0 and sMeiForm ~= "Zombee") then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iSeenFlorentinaWarning", "N", 1.0)
		
		--Setup.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		
		--Dialogue.
		fnCutscene([[ Append("Florentina: Mei, get ready.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] ..?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] Wait, you're right.[P] I can...[P] feel...[P] something...[P] up ahead...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] (My runestone is pulsing...[P] maybe that's a warning?)") ]])
	
	end

elseif(sObjectName == "GotBehindDoor") then
    VM_SetVar("Root/Variables/Chapter1/Scenes/iDoorFromNorth", "N", 1.0)

elseif(sObjectName == "AheadOfDoor") then
    VM_SetVar("Root/Variables/Chapter1/Scenes/iDoorFromNorth", "N", 0.0)
end
