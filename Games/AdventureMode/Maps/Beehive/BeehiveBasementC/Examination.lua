-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
	
--Get Mei's form. All the examinations use it.
local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")

-- |[Examinations]|
--Out-of-place Door.
if(sObjectName == "OddDoor") then
	
	--Get Mei's position. The door can be opened only from the north.
	EM_PushEntity("Mei")
		local fPlayerX, fPlayerY = TA_GetProperty("Position")
	DL_PopActiveObject()
	
	--Examining from the north, as anything other than a Zombee.
	if(fPlayerY < 30.0 * gciSizePerTile and sMeiForm ~= "Zombee") then
		AL_SetProperty("Open Door", "Door")
		AudioManager_PlaySound("World|OpenDoor")
		fnStandardDialogue([[ [VOICE|Mei](The door has been unlocked.) ]])
		VM_SetVar("Root/Variables/Chapter1/Scenes/iOpenedOddDoor", "N", 1.0)
	
	--Examining from the south, as anything other than a Zombee.
	elseif(fPlayerY >= 30.0 * gciSizePerTile and sMeiForm ~= "Zombee") then
		AudioManager_PlaySound("World|RemoteDoor")
		fnStandardDialogue([[ [VOICE|Mei](This door was clearly not built by the bees.[P] It's locked...) ]])
	
	--Zombee:
	else
		fnStandardDialogue("Drone:[VOICE|Bee] (Drone obeys.[P] Ignore door.[P] Capture bees.[P] Convert bees.[P] Drone obeys.[P] Drone obeys.)")
	end
	
--Foodshelf.
elseif(sObjectName == "FoodShelf") then
	
	--Normal examination:
	if(sMeiForm ~= "Zombee") then
		fnStandardDialogue([[ [VOICE|Mei](A shelf stocked with food.[P] Human food.) ]])
	
	--Zombee:
	else
		fnStandardDialogue("Drone:[VOICE|Bee] (Drone obeys.[P] Ignore foodshelf.[P] Capture bees.[P] Convert bees.[P] Drone obeys.[P] Drone obeys.)")
	end
	
--Bed.
elseif(sObjectName == "Bed") then
	
	--Normal examination:
	if(sMeiForm ~= "Zombee") then
		fnStandardDialogue([[ [VOICE|Mei](A poorly-kept bed.[P] Not the kind of thing bees use...) ]])
	
	--Zombee:
	else
		fnStandardDialogue("Drone:[VOICE|Bee] (Drone obeys.[P] Ignore bed.[P] Capture bees.[P] Convert bees.[P] Drone obeys.[P] Drone obeys.)")
	end
	
--Barrels.
elseif(sObjectName == "Barrels") then
	
	--Normal examination:
	if(sMeiForm ~= "Zombee") then
		fnStandardDialogue([[ [VOICE|Mei](Barrels with drinkable water in them.) ]])
	
	--Zombee:
	else
		fnStandardDialogue("Drone:[VOICE|Bee] (Drone obeys.[P] Ignore barrels.[P] Capture bees.[P] Convert bees.[P] Drone obeys.[P] Drone obeys.)")
	end
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
