-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    local sActorName = TA_GetProperty("Name")
    
    -- |[CultistCorrupter]|
    if(sActorName == "CultistCorrupter") then
	
        --Setup.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Xanna", "Neutral") ]])

        --Talking.
        fnCutscene([[ Append("Cultist:[E|Neutral] Get back to work, drone![BLOCK]") ]])
        
        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Drone Obeys\", " .. sDecisionScript .. ", \"Obey\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Resist...\",  " .. sDecisionScript .. ", \"ResistFirst\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Resist...\",  " .. sDecisionScript .. ", \"ResistFirst\") ")
        fnCutsceneBlocker()
    
    -- |[Zombees]|
    elseif(sActorName == "ZombeeNPCA" or sActorName == "ZombeeNPCB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Zombee", "Neutral") ]])
        fnCutscene([[ Append("Drone:[VOICE|Bee][E|MC] (Drone obeys.[P] Drone obeys.[P] Capture bees.[P] Convert bees.[P] Drone obeys.)") ]])
        fnCutsceneBlocker()
    end
    
-- |[ ============================= Responses to Cultist Corrupter ============================= ]|
--Obey: Sooner or later, the player picks this:
elseif(sTopicName == "Obey") then

	--Clear any pending instructions.
	WD_SetProperty("Hide")
	
	--Setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Xanna", "Neutral") ]])

	--Talking.
	fnCutscene([[ Append("Drone:[VOICE|Bee] Drone obeys.[P] Drone obeys.[P] Capture bees.[P] Convert bees.[P] Drone obeys.") ]])
	fnCutsceneBlocker()

--ResistFirst, part of the resistance sequence.
elseif(sTopicName == "ResistFirst") then

	--Clear any pending instructions.
	WD_SetProperty("Hide")
	
	--Setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Xanna", "Neutral") ]])

	--Talking.
	fnCutscene([[ Append("Cultist:[E|Neutral] Perhaps you needed some additional motivation?[P][CLEAR]") ]])
	fnCutsceneBlocker()
	
	--Screen overlay. Purple, fade down.
	fnCutscene([[ AudioManager_PlaySound("World|Heartbeat") ]])
	fnCutscene([[ AL_SetProperty("Activate Fade", 120, gci_Fade_Over_GUI, false, 0.8, 0.2, 1.0, 1, 0, 0, 0, 0) ]])
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Xanna", "Neutral") ]])
	fnCutscene([[ Append("[P]*Hatred and obedience surge from the handprint on your vagina...*[BLOCK]") ]])
	
	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Drone Obeys\", " .. sDecisionScript .. ", \"Obey\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Drone Obeys\", " .. sDecisionScript .. ", \"Obey\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Resist...\",   " .. sDecisionScript .. ", \"ResistSecond\") ")
	fnCutsceneBlocker()

--ResistSecond, part of the resistance sequence.
elseif(sTopicName == "ResistSecond") then

	--Clear any pending instructions.
	WD_SetProperty("Hide")
	
	--Setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Xanna", "Neutral") ]])

	--Talking.
	fnCutscene([[ Append("Cultist:[E|Neutral] Heh, again, then.[P][CLEAR]") ]])
	fnCutsceneBlocker()
	
	--Screen overlay. Purple, fade down.
	fnCutscene([[ AudioManager_PlaySound("World|Heartbeat") ]])
	fnCutscene([[ AL_SetProperty("Activate Fade", 120, gci_Fade_Over_GUI, false, 0.8, 0.2, 1.0, 1, 0, 0, 0, 0) ]])
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Xanna", "Neutral") ]])
	fnCutscene([[ Append("[P]*The print on your vagina surges.[P] Obedience rushes through you.[P] It fills you.[P] It is you.*[B][C]") ]])
	fnCutscene([[ Append("[P]*You are a drone.[P] Drone obeys.*[BLOCK]") ]])
	
	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Drone Obeys\", " .. sDecisionScript .. ", \"Obey\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Drone Obeys\", " .. sDecisionScript .. ", \"Obey\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Drone Obeys\", " .. sDecisionScript .. ", \"Obey\") ")
	fnCutsceneBlocker()
end
