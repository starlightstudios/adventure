-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    if(sActorName == "BeeA") then
      TA_SetProperty("Face Character", "PlayerEntity")
      fnCutscene([[ WD_SetProperty("Show") ]])
      fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
      LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
      fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "BlueBee", "Neutral") ]])
       fnCutscene([[ Append("Bee:[E|Neutral] Bzzz?") ]])
      fnCutsceneBlocker()
      
      VM_SetVar("Root/Variables/Volcano/Scenes/iMetBlueBees", "N", 0.0)
      VM_SetVar("Root/Variables/Volcano/Scenes/iIsTaraFollowing", "N", 0.0)
    
    end
end
