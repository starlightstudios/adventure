-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Decision Handler]|
--This is fired if Mei selects the decision to eat the honey when she has entered the hive as a human without the bee TF.
if(sObjectName == "EatHoney") then
		
	--Variables.
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	
	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")
	
	--Mei is alone.
	if(bIsFlorentinaPresent == false) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iUseSpecialBeeOpening", "N", 1.0)
		
		--Dialogue.
		fnStandardMajorDialogue()
		fnCutscene([[ Append("Mei:[E|Neutral] (I *am* kind of hungry...[P] Just a little bit...)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] *slurp*[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] (Oohhhh, so sweet.[P]*slurp*[P] So gooooood...)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] (It's so thick...[P] OH![P] I got some on my dress...)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] *giggles*[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] *slurp*[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] (Mmmmmm.....)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] *slurp*[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] It feels so good on my hands...[P] On my lips...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] (Maybe I...[P] should rub it on my skin...)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] (...)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] (...)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] (It's hardening?[P] Oh?[P] It's supposed to?[P] But I wanted to eat more...)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] (I'm kind of tired, though...[P] I'll sleep now, instead...)") ]])
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--Mei crouches and falls over.
		Cutscene_CreateEvent("Event", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Special Frame", "Crouch")
		DL_PopActiveObject()
		fnCutsceneWait(60)
		fnCutsceneBlocker()
		Cutscene_CreateEvent("Event", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Special Frame", "Wounded")
		DL_PopActiveObject()
		fnCutsceneWait(60)
		fnCutsceneBlocker()
		
		--Fade to black.
		fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
		fnCutsceneWait(240)
		fnCutsceneBlocker()
		
		--Execute the bee cutscene. It should do the rest, with the flag set so Mei reacts correctly.
		LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/200 Defeat/Defeat_Bee/Scene_Begin.lua")
	
	--Florentina is present.
	else
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iUseSpecialBeeOpening", "N", 1.0)
	
		--Cutscene setup. Florentina is on the opposite side.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		
		--Dialogue.
		fnCutscene([[ Append("Mei:[E|Neutral] (Just a little bit...)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] *slurp*[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Surprise] M-[P]Mei!?[P] What in the wastes are you doing?![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] It's so good...[P] You should try some...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Surprise] Mei![P] Mei![P] Look at me, focus on me![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] Ooh...[P] I got some on my dress...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] *giggles*[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] This honey...[P] it's gotta be laced with a psychoactive![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] Mmmmmmm...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] Stop eating it, you dolt![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] I want it...[P] so badly...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] Don't you dare stop me![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Surprise] Cripes![P] Mei, you're not yourself![P] Try to focus![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] ...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] ...[P] They call...") ]])
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--Mei crouches and falls over.
		Cutscene_CreateEvent("Event", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Special Frame", "Crouch")
		DL_PopActiveObject()
		fnCutsceneWait(60)
		fnCutsceneBlocker()
		Cutscene_CreateEvent("Event", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Special Frame", "Wounded")
		DL_PopActiveObject()
		fnCutsceneWait(60)
		fnCutsceneBlocker()
		
		--Florentina's dialogue.
		fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
		fnCutsceneWait(120)
		fnCutsceneBlocker()
	
		--Cutscene setup. Florentina is on the opposite side.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Florentina", "Neutral") ]])
		
		--Dialogue.
		fnCutscene([[ Append("Florentina:[E|Offended] G-[P]get away![P] Stupid bees![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Offended] Mei![P] Mei![P] I'll come back with help![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] (Idiot![P] What did she think was going to happen?)") ]])
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(240)
		fnCutsceneBlocker()
		
		--Execute the bee cutscene. It should do the rest, with the flag set so Mei reacts correctly.
		LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/200 Defeat/Defeat_Bee/Scene_Begin.lua")
	end
	return

--This is fired if Mei decides not to eat the honey. It basically just closes the dialogue.
elseif(sObjectName == "DontEatHoney") then
	WD_SetProperty("Hide")
	return
end

-- |[Execution]|
--Honey Pool. Different during bee examination.
if(sObjectName == "HoneyPool") then

	--Variables.
	local sMeiForm              = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local iHasBeeForm           = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")
	local iHasConsumedHoney     = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasConsumedHoney", "N")
	local iIsDuringBeeTransform = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsDuringBeeTransform", "N")
	
	--During normal examination, but Mei can voluntarily begin the bee transformation.
	if(iHasBeeForm == 0.0 and sMeiForm == "Human" and iIsDuringBeeTransform == 0.0) then
		
		--Bit of dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Mei](The honey looks delicious.[P] Try a little?)[BLOCK]") ]])
		
		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		
		--Decision mode.
		fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"EatHoney\") ")
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"DontEatHoney\") ")
		fnCutsceneBlocker()
	
	--During bee transformation...
	elseif(iIsDuringBeeTransform == 1.0) then
		
		--Common.
		fnCutscene([[ WD_SetProperty("Show") ]])
        
        --Load images.
        fnLoadDelayedBitmapsFromList("Chapter 1 Mei Bee TF", gciDelayedLoadLoadAtEndOfTick)
        fnLoadDelayedBitmapsFromList("Chapter 1 Mei Runestone", gciDelayedLoadLoadAtEndOfTick)
		
		--First time.
		if(iHasConsumedHoney == 0.0) then
			VM_SetVar("Root/Variables/Chapter1/Scenes/iHasConsumedHoney", "N", 1.0)
			fnCutscene([[ Append("Mei dipped a finger into the honey and raised it to her lips. She hesitated for a moment, letting the sweet scent wash over her face before reaching a tentative tongue out to taste it.[B][C]") ]])
			fnCutscene([[ Append("Mei:[VOICE|Mei] (...[P] Delicious![B][C]") ]])
			fnCutscene([[ Append("She stuck her finger in her mouth.[P] Her tongue wrapped around its length, searching to find every last sticky drop of honey as a quiet need began to grow deep in her abdomen. [B][C]") ]])
			fnCutscene([[ Append("Mei:[VOICE|Mei] (I...[P] I think I want a little more...)") ]])
		
		--Second time.
		elseif(iHasConsumedHoney == 1.0) then
			VM_SetVar("Root/Variables/Chapter1/Scenes/iHasConsumedHoney", "N", 2.0)
			fnCutscene([[ Append("Mei knelt before the open pool of honey and dipped her hand in.[P] The viscous fluid quivered and dripped from her cupped hand as she raised it to her lips.[P] It slid down her throat, where it settled heavily in her stomach.[B][C]") ]])
			fnCutscene([[ Append("Mei:[VOICE|Mei] (...[P] Oh it's so good![P] Maybe just a bit more...)") ]])
		
		--Third time.
		elseif(iHasConsumedHoney == 2.0) then

            --Clear censor bars.
            WD_SetProperty("Clear Censor Bars")

			VM_SetVar("Root/Variables/Chapter1/Scenes/iHasConsumedHoney", "N", 3.0)
			fnCutscene([[ WD_SetProperty("Activate Scene") ]])
			fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
			fnCutscene([[ Append("Mei took another handful of honey, her head dipping to meet her hand as it raised to her lips.[P] She sucked the fluid into her mouth and swallowed it down with deep gulps.[P] Her sticky hand dropped to the edge of the pool to support her as she gasped for air after the hasty meal.[B][C]") ]])
			fnCutscene([[ Append("Mei:[VOICE|Mei] (...[P] Soooo[P] *gasp* [P] tasty...)") ]])
			fnCutsceneBlocker()
			
			fnCutscene([[ WD_SetProperty("FastShow") ]])
			fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
			fnCutscene([[ WD_SetProperty("Set Scene Image Fast", "Root/Images/Portraits/Combat/Mei_Human") ]])
			fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/BeeTF0") ]])
            fnCutscene([[ WD_SetProperty("Register Censor Bar", 615, 315, 64, 45) ]])
			fnCutscene([[ Append("[VOICE|Bee](Process...)[B][C]") ]])
			fnCutscene([[ Append("Mei:[VOICE|Mei] (Yeah...[P] Process...[P] Make more honey...)[B][C]") ]])
			fnCutscene([[ Append("Her forehead began to tickle, and she reached her honey-covered hand to scratch at it as she stood.[P] The honey hardened quickly on her head, and cracked as it dried.[P] A pair of insectile feelers began to push up through her skin beneath the honey.[B][C]") ]])
			fnCutscene([[ Append("Mei looked about, her thoughts oblivious to her new appendages.[P] Her attention had turned entirely to the instructions she had been given.") ]])
			fnCutsceneBlocker()
		
		--No more honey.
        elseif(iHasConsumedHoney == 3.0) then
            
            local iHasWorkedNectar      = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasWorkedNectar", "N")
            if(iHasWorkedNectar == 0.0) then
                fnCutscene([[ Append("[VOICE|Bee](Process...)[B][C]") ]])
                fnCutscene([[ Append("Mei:[VOICE|Mei] (I'll go...[P] Process...[P] Make more honey...)") ]])
            else
                fnCutscene([[ Append("[VOICE|Bee](Clean...)[B][C]") ]])
                fnCutscene([[ Append("Mei:[VOICE|Mei] (I need to...[P] clean the things...[P] I left from my old life...)") ]])
                
            end
		end
		
		--Common.
		fnCutsceneBlocker()
	
	--General examination.
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought: (A pool full of golden honey...)[B][C]") ]])
		fnCutscene([[ Append("Thought: (Thick, viscous honey...)") ]])
		fnCutsceneBlocker()
	end

--Nectar Pool. Has a different examination during the bee transformation.
elseif(sObjectName == "NectarPool") then

	--During bee transformation...
	local iIsDuringBeeTransform = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsDuringBeeTransform", "N")
	local iHasWorkedNectar      = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasWorkedNectar", "N")
	if(iIsDuringBeeTransform == 1.0 and iHasWorkedNectar == 0.0) then
		
        --Clear censor bars.
        WD_SetProperty("Clear Censor Bars")
            
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Mei:[VOICE|Mei] (I need to process this...[P] But how?[P] What does that mean?)") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		
		--Fade out.
		fnCutscene([[ AL_SetProperty("Activate Fade", 120, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
		
		--Scene.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Activate Scene") ]])
		fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/BeeTF0") ]])
        fnCutscene([[ WD_SetProperty("Register Censor Bar", 615, 315, 64, 45) ]])
		fnCutscene([[ Append("New ideas began to flow into Mei's mind, as if responding to her thoughts.[P] The ideas brought about a feeling of languid contentment, of fullness, that seemed to press on her abdomen.[B][C]") ]])
		fnCutscene([[ Append("She understood what she needed to do.[P] She understood how she needed to process the nectar.[P] She knelt silently by the pool of nectar,[P] then scooped it into her hands and began spreading it on her sex...") ]])
		fnCutsceneBlocker()
		
		fnCutscene([[ WD_SetProperty("FastShow") ]])
		fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
		fnCutscene([[ WD_SetProperty("Set Scene Image Fast", "Root/Images/Scenes/Mei/BeeTF0") ]])
		fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/BeeTF1") ]])
        fnCutscene([[ WD_SetProperty("Send Censor Bars To Crossfade") ]])
        fnCutscene([[ WD_SetProperty("Register Censor Bar", 684, 176, 95, 54) ]])
		fnCutscene([[ Append("Her body tingled as she slid a nectar-covered finger between the lips of her vagina.[P] She could feel the thick fluid being absorbed into the tender depths of her core, then spreading throughout her.[B][C]") ]])
		fnCutscene([[ Append("She scooped another handful of nectar and poured it on herself, sliding a second finger beside the first.[P] A chill ran up her spine despite the warmth of the hive and the nectar.[P] She shivered at the pleasant sensation as it spread through her limbs and pebbled her nipples beneath her dress.[B][C]") ]])
		fnCutscene([[ Append("Her thrusts deepened as she dribbled another handful of nectar onto herself.[P] She pressed her thumb against her sensitive clit as the fingers drove the honey deeper inside of her, massaging the bundle of nerves in slow circles as she felt her fullness growing.[B][C]") ]])
		fnCutscene([[ Append("The chill began to fade as her fullness grew.[P] It lingered on her back and her legs, and she scratched at it absentmindedly as her fingers pumped in and out of her.[B][C]") ]])
		fnCutscene([[ Append("Her eyes slid closed as translucent wings pushed out from her back, and her feet grew hard and insectile.[P] A burning heat seemed to kindle in her core as the itching faded, and she knew that she, and the nectar within her, were nearly ready. [B][C]") ]])
		fnCutscene([[ Append("Her thumb pressed more firmly against her clit,[P] light shocks of electric ecstasy pulsing through her body with each beat of her heart as her fingers quickened inside her. [B][C]") ]])
		fnCutscene([[ Append("It pushed her body over the edge, her orgasm coursing through her as she shouted her release into the air.[P] Dozens of voices,[P] perhaps echoes,[P] seemed to join her release in her mind as she began to slowly come down. [B][C]") ]])
		fnCutscene([[ Append("She slid her fingers out from within her, her own fluids mixing with the nectar and leaving a long, thin strand as she lifted the fingers to her lips.[P] Her tongue snaked out for a single lick as she stood.[P] It was already sweetening, the flavor distinct.[P] The flavor of herself mixed with the nectar. [B][C]") ]])
		fnCutscene([[ Append("The nectar would process inside her, to be secreted later with as much pleasure as when she had begun to prepare it.[P] This she knew, an understanding based entirely on instinct.[P] Even so, she knew it would take some time,[P] and further preparation,[P] before it would be ready.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		
		--Switch Mei's sprite to the bee.
		fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Bee.lua") ]])
		
		--Fade back in.
		fnCutscene([[ AL_SetProperty("Activate Fade", 120, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 0) ]])
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Mei:[VOICE|Mei] (I...[P] we...[P] clean...[P] clean...)") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		
		
		--Variables.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasWorkedNectar", "N", 1.0)
	
	--General examination.
	else
		--Normal dialogue:
		local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
		if(sMeiForm ~= "Bee") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("[VOICE|Mei](A large pool containing the nectar that bees are so keen on collecting.)") ]])
            fnCutsceneBlocker()
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("[VOICE|Mei](A large pool containing the nectar that my hive collects.)") ]])
            fnCutsceneBlocker()
        end
	end
	
--Stuff left behind by previous victims. 
elseif(sObjectName == "Junk") then
	
	--Bee Transformation: This fixes Mei's brain and makes her conscious again.
	local iIsDuringBeeTransform = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsDuringBeeTransform", "N")
	if(iIsDuringBeeTransform == 1.0) then
	
        --Issue a loading order.
        fnLoadDelayedBitmapsFromList("Chapter 1 Mei Runestone", gciDelayedLoadLoadAtEndOfTick)
	
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Mei:[VOICE|Mei] (Clean...[P] Useless...[P] Clean...)") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		
		--Scene.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Activate Scene") ]])
		fnCutscene([[ WD_SetProperty("Set Scene Image Fast", "Root/Images/Scenes/Mei/BeeTF2") ]])
		fnCutscene([[ Append("Mei began to thoughtlessly sort through the remnants of her old life.[P] Her clothes and weapons meant nothing to her anymore.[P] She was not even fully aware of what she was looking at.[B][C]") ]])
		fnCutscene([[ Append("Images of other parts of the world flowed into her head.[P] Ideas and whispers came and went of their own accord.[P] Mei sent out her own thoughts in return.[B][C]") ]])
		fnCutscene([[ Append("But, as she picked up her old work uniform, a small stone rune fell out and landed on the ground near her.") ]])
		fnCutsceneBlocker()
		
		--Animation.
		local sAllocationString = "WD_SetProperty(\"Allocate Animation\", gciMeiRuneFramesTotal, 1, -90)"
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
		fnCutscene(sAllocationString)
		fnCutscene([[ WD_SetProperty("Set Scene Image Fast", "Root/Images/Scenes/Mei/BeeTF2") ]])
		fnCutscene([[ WD_SetProperty("Activate Flash", 0) ]])
		for i = 0, gciMeiRuneFramesTotal-1, 1 do
			local iNumber = string.format("%02i", i)
			local sString = "WD_SetProperty(\"Set Animation Frame\", " .. i .. ", \"Root/Images/Scenes/Mei/RuneAnim" .. iNumber .. "\")"
			fnCutscene(sString)
		end
		fnCutscene([[ Append("A pale grey light began to surround Mei.[P] Somehow, her own thoughts rushed into her head.[P] Memories of her life, her childhood, her friends...[P] They had never left her, but somehow she had still forgotten them.") ]])
		fnCutsceneBlocker()
	
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] M-[P]m-[P]Mei?[P] My name is Mei?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] But I'm...[P] What happened to me?[P][EMOTION|Mei|Surprise] Is that a stinger?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] No![P] Wait, please, be quiet![P] Be quiet![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Angry] G-[P]get out of my head![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] ...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] ...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] I'm sorry![P] I don't know what's going on, really![P] I was just doing what we said to do...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] Oh, right, the runestone.[P] Yes, I came here with it.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] It's not dangerous, sisters.[P] It will only work on me, I'm sure of it.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Yes.[P] Yes.[P] Yes, of course![P] I didn't mean to...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] H-[P]hey![P] Don't question my loyalty, I'm a drone![P] I obey without question, sisters![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] ...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] ...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] ...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I will honor the consensus.[P] I will...[P][EMOTION|Mei|Sad] find out why I'm different...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] No, it's okay.[P] This is for the best.[P] If I find any exotic nectars, I'll mark them right away.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Thank you for understanding, sisters![P] I'll do my best![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] ...[P] Oh, certainly.[P] Yes, some honey will help greatly.[P] Thank you.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] [SOUND|World|TakeItem](Received Everlasting Bee Honey!)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] (...[P] Okay then, the hive has tasked me with a crucial mission::[P] Solve the mystery of the runestone!)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] (...)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] (Which is...[P] what I was doing before all this happened, I guess...)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] (But now I'm doing it for my drone sisters![P] Onward!)") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		
		--Variables.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iIsDuringBeeTransform", "N", 0.0)
        
        --Clear images.
        fnCutscene([[ fnUnloadBitmapsFromList("Chapter 1 Mei Bee TF") ]])
        fnCutscene([[ fnUnloadBitmapsFromList("Chapter 1 Mei Runestone") ]])
		
		--If this is the relive case, return to the last save point and execute that scene.
		local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsRelivingScene", "N")
		if(iIsRelivingScene == 1.0) then
	
			--Wait a bit.
			fnCutsceneWait(10)
			fnCutsceneBlocker()
			
			--Black the screen out.
			fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
			fnCutsceneWait(45)
			fnCutsceneBlocker()
			
			--Return to the last save point and execute the post-script..
			fnCutscene([[ AL_BeginTransitionTo("LASTSAVE", gsStandardReliveEnd) ]])
			fnCutsceneBlocker()
	
		--Not reliving, gain the bee honey.
		else
			LM_ExecuteScript(gsItemListing, "Everlasting Honey")
	
		end
	
	--All other cases.
	else
	
		--Normal dialogue:
		local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
		if(sMeiForm ~= "Bee") then
			
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ Append("[VOICE|Mei](Assorted junk left over from previous victims.[P] The bees don't seem to care about it.)") ]])
			fnCutsceneBlocker()
		
		--Bee-form dialogue.
		else
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ Append("[VOICE|Mei](The belongings left over from the previous lives of my bee sisters.[P] They'll get rid of it eventually.)") ]])
			fnCutsceneBlocker()
		end
		
		--If the player has not searched this, they find some adamantite powder.
		local iBeehiveJunkSearched = VM_GetVar("Root/Variables/Chapter1/WorldState/iBeehiveJunkSearched", "N")
		if(iBeehiveJunkSearched == 0.0) then
			
			--Flags.
			iBeehiveJunkSearched = VM_SetVar("Root/Variables/Chapter1/WorldState/iBeehiveJunkSearched", "N", 1.0)
			LM_ExecuteScript(gsItemListing, "Adamantite Powder x1")
			
			--Dialogue.
			fnCutscene([[ AudioManager_PlaySound("World|TakeItem") ]])
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ Append("[VOICE|Mei](There was some Adamantite Powder in the pile.[P] Might as well take it!)") ]])
			fnCutsceneBlocker()
		end
	end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
