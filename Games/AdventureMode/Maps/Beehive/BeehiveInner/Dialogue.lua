-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[All Bees]|
    if(sActorName == "BeeGirl") then

        --Variables.
        local sForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
        local iHasBeeForm = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")
        local iIsDuringBeeTransform = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsDuringBeeTransform", "N")

        --If Mei is not in bee mode, the bee is just ignoring her.
        if(sForm ~= "Bee") then
            
            --If Mei has the bee transformation.
            if(iHasBeeForm == 1.0) then
                fnStandardDialogue("Your bee sister is uninterested in you for the moment.")
            
            --If not during the bee transformation:
            elseif(iIsDuringBeeTransform == 0.0) then
                fnStandardDialogue("This bee seems to be ignoring you.[P] It seems to be too busy to view you as a threat.")
            
            --During the bee transformation.
            else
                fnStandardDialogue("This bee seems to be ignoring you.")
            end
        
        --In bee mode, the dialogue changes.
        else
            fnStandardDialogue("There is no need to speak to this bee, you can already hear her thoughts.")
        end
	
    -- |[Xanna]|
	elseif(sActorName == "Xanna") then
	
		--Variables.
        local sForm       = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
        local iHasBeeForm = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")
		local iMetXanna   = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetXanna", "N")
	
		--First meeting:
		if(iMetXanna == 0.0) then
			
			--Mei is in Bee form:
			if(sForm == "Bee") then
				VM_SetVar("Root/Variables/Chapter1/Scenes/iMetXanna", "N", 1.0)
				fnCutscene([[ WD_SetProperty("Show") ]])
				fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
				fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Xanna", "BeeNeutral") ]])
				LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
				fnCutscene([[ Append("Mei:[E|Happy] ...[B][C]") ]])
				fnCutscene([[ Append("Xanna:[E|BeeNeutral] ...[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] ...[B][C]") ]])
				fnCutscene([[ Append("Xanna:[E|BeeNeutral] ...[B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Neutral] Hey, idiots.[P] Having a nice private conversation?[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] Oh![P] Sorry![P] Florentina, this is Xanna.[P] She was with the cult, but now she's a bee.[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] How about, I translate what her thoughts are for you?[B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Happy] Do individual bees still do that?[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Happy] Of course they do, silly![P] It's just our thoughts are also the thoughts of everyone.[P] She still knows when she's cold or tired.[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Happy] Things just get a little tangled up, you forget whether it's you that is the cold one.[P] You just let the hive take over and become a little voice in it.[B][C]") ]])
				fnCutscene([[ Append("Xanna:[E|BeeNeutral] (It's very nice.[P] I like it here, I'm never lonely.)[B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Neutral] You've told the hive how to undo your little corruption?[B][C]") ]])
				fnCutscene([[ Append("Xanna:[E|BeeNeutral] (Of course.[P] I would not keep it from them even if I could.[P] I love my sisters.[P] Once I have recovered from my injuries I will be personally cleaning the hive of that filth.)[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Smirk] (Sorry about those injuries, sister...[P] I tried to stop Florentina![P] Really!)[B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Blush] Couldn't be me, you've got me mixed up for someone else.[B][C]") ]])
				fnCutscene([[ Append("Xanna:[E|BeeNeutral] (Is there anything you'd like to know, sister drone?)[B][C]") ]])
				fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Xanna") ]])
			
			--Mei has bee form:
			elseif(sForm ~= "Bee" and iHasBeeForm == 1.0) then
				fnCutscene([[ WD_SetProperty("Show") ]])
				fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
				fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Xanna", "BeeNeutral") ]])
				LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
				fnCutscene([[ Append("Mei:[E|Neutral] ...[B][C]") ]])
				fnCutscene([[ Append("Xanna:[E|BeeNeutral] Bzz?[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] I think she wants to talk but I obviously can't understand her without being a bee myself.[P] I could guess, but...") ]])
			
			--Mei doesn't have bee form:
			else
				fnCutscene([[ WD_SetProperty("Show") ]])
				fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
				fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Xanna", "BeeNeutral") ]])
				LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
				fnCutscene([[ Append("Xanna:[E|BeeNeutral] Bzz?[P] Bzz![B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] Huh, I guess we can't understand her, obviously.[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] See's giving it a try but bees seem to be unable to speak when attached to a hive mind.[P] Oh well.") ]])
			end
		
		--Repeats.
		else
			--Mei is in Bee form:
			if(sForm == "Bee") then
				fnCutscene([[ WD_SetProperty("Show") ]])
				fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
				LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
				fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Xanna", "BeeNeutral") ]])
				fnCutscene([[ Append("Xanna:[E|BeeNeutral] (I already know what you want to talk about, but what do you want to talk about, sister drone Mei?)[B][C]") ]])
				fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Xanna") ]])
	
			--Nope.
			else
				fnCutscene([[ WD_SetProperty("Show") ]])
				fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
				LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
				fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Xanna", "BeeNeutral") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] (If I want to 'talk' to sister drone Xanna, I'll need to be a bee myself!)") ]])
			
			end
		end
    end
end
