-- |[Constructor]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "ForestTheme")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/BeehiveInner/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", "BeehiveInner")

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end
    
    -- |[Map Handling]|
    --Resolve the map's variable.
    fnStandardCh1Map(sLevelName)

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then
	
	--Bee spawner function. All bees use the same properties.
	local fnSpawnBee = function(iX, iY)
		if(iX == nil or iY == nil) then return end
		
		TA_Create("BeeGirl")
			TA_SetProperty("Position", iX, iY)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/BeeGirl/", false)
			TA_SetProperty("Facing", LM_GetRandomNumber(gci_Face_North, gci_Face_NorthWest))
			TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
			TA_SetProperty("Activate Wander Mode")
			TA_SetProperty("Auto Animates", true)
			TA_SetProperty("Y Oscillates", true)
		DL_PopActiveObject()
	end
	
	--Add some bee NPCs, set to wander.
	fnSpawnBee(13, 15)
	fnSpawnBee( 9, 22)
	fnSpawnBee(19, 17)
	fnSpawnBee(14, 10)
	
	--Xanna. Appears after corrupter is defeated.
	local iSavedBeehive = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedBeehive", "N")
	if(iSavedBeehive == 1.0) then
		fnStandardNPCByPosition("Xanna")
	end
end
