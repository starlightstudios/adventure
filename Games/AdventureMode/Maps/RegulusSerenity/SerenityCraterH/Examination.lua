-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.
--Special: This script is also used during the reliving sequences.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
local bSpecial = false
if(not fnArgCheck(1)) then
    bSpecial = true
end

--Arg resolve.
local sObjectName = "Null"
if(bSpecial == false) then
    sObjectName = LM_GetScriptArgument(0)
else
    sObjectName = "Hole"
end

-- |[Exits]|
-- |[Examinables]|
--Handprint.
if(sObjectName == "Hand") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] (The handprint of some enormous creature.[P] It's oddly human-like, but how could something so big fit in here?)") ]])
	fnCutsceneBlocker()
	
--Green Egg.
elseif(sObjectName == "GreenEgg") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] (A strange green egg.[P] It's cold to the touch...)") ]])
	fnCutsceneBlocker()
	
--Hole.
elseif(sObjectName == "Hole") then

    --Music.
    local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsRelivingScene", "N")
    if(iIsRelivingScene == 1.0) then
        fnCutscene([[ AL_SetProperty("Music", "Null") ]])
    end
    
    --Issue load instruction.
    fnLoadDelayedBitmapsFromList("Chapter 5 Christine Darkmatter TF", gciDelayedLoadLoadAtEndOfTick)

	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] (One of those eggs was here...[P] but someone took it...)[B][C]") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] (The golems...[P] should not have taken it...)[B][C]") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] (I think -[P] I think this is important.[P] I need to close this hole, but with what?)[B][C]") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] (I'll just...[P] stick my hand in there...)") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Screen blacks out.
    if(iIsRelivingScene == 0.0) then
        fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    end
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] Eeeek![P] It's stuck![P] Let go![B][C]") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] Get off me![P] No![P] EEEEEEEEEEEEEEEEEEEKKKKK!!!!!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

    --Clear censor bars.
    WD_SetProperty("Clear Censor Bars")
	
	--Scene.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
	fnCutscene([[ Append("Christine pulled at her arm, her feet digging into the fleshy floor as she pulled, but could not free herself.[P] Something within the hole had taken hold of her and would not yield its grip.[B][C]") ]])
	fnCutscene([[ Append("Its strength was immense, and though it felt like the grip of a human, she knew that was impossible. [P] Something deep within her whispered to her, and she knew she was the only human to ever see this place.[B][C]") ]])
	fnCutscene([[ Append("A second hand latched on to her arm, followed by a third, then a fourth,[P] and soon, she lost count.[P] They felt human.[P] They had five fingers.[P] They gripped as a human would grip, wrapping four fingers around her arm and locking the grip into place with a thumb.[P] But she could think of no human whose hold was so absolute that it yielded no movement at all.[B][C]") ]])
	fnCutscene([[ Append("She screamed again, but it echoed, unheard, off the pulsating walls of skin and muscle.[B][C]") ]])
	fnCutscene([[ Append("A new scream welled up within her, and erupted from the depths of her very being.[P] It flowed from her,[P] piercing into the air,[P] then faded,[P] muted into silence by the pulsating walls of flesh as though she were in the depths of a void.[B][C]") ]])
	fnCutscene([[ Append("She pulled again at her arm, and her mind began to clear.[P] She needed to escape, whatever the cost.[P] She drew her spear as she cast a silent glare at the advancing hands.[P] If she could not free herself, she would need to cut off her own arm.[B][C]") ]])
	fnCutscene([[ Append("She swallowed a growing lump that had risen in her throat and steadied herself.[P] She could only hope she would be able to staunch the bleeding and find a way back to Regulus City.[P] Surely Sophie would be able to replace the lost limb if she could return to golem form.[B][C]") ]])
	fnCutscene([[ Append("The hands continued their advance, now emerging from the black void of the hole.[P] They were the very same hands that appeared on the walls and floor.[P] They were ethereal, with no physical form, but she could still see and feel as the hands continued their advance up her arm.[P] She gritted her teeth and raised the spear.[B][C]") ]])
	fnCutscene([[ Append("She squinted, struggling to keep her eyes open to what she knew was to come.[P] The hands were advancing.[P] She would have only a single chance before they reached her shoulder.[P] She could not risk anything less than a clean and complete cut.[B][C]") ]])
	fnCutscene([[ Append("And then,[P] even as she finalized her resolve,[P] the hands loosened their grip.[P] She blinked, and the hands wavered with an ephemeral trembling,[P] and faded into nothing.[B][C]") ]])
	fnCutscene([[ Append("Her spear fell to the floor, any noise it could make fading into the walls of flesh that surrounded her.[P] She soon joined it, dropping to her knees and clutching at her arm in wonderment.[P] All along it, from the tips of her fingers to the toned muscle of her bicep and tricep, an uncountable number of hand prints had been etched into her skin.[B][C]") ]])
	fnCutscene([[ Append("She looked herself over, cautious of any possible threat that may have affected her as the hands had occupied her,[P] but could see nothing beyond the countless overlapping handprints now etched into her skin.[P] Her thoughts fell to silence as she struggled to comprehend what had just befallen her.[B][C]") ]])
	fnCutscene([[ Append("Satisfied that no further dangers were immediately present, she looked more closely at the handprints on her arm.[P] They were identical to the hands that had gripped her, as though they had been tattooed onto her arm with a shimmering translucent ink.[P] She frowned and drew her arm nearer.[P] The handprints had not been shimmering a moment ago.") ]])
	fnCutsceneBlocker()

    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
    fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DarkmatterTF0") ]])
	fnCutscene([[ Append("She stared at the handprints, her gaze sweeping across the length of her arm, and for a brief moment, she thought she could see something within them.[P] She drew her arm nearer still, nearly touching it to her face, and realized that, behind the handprints, she could see...[P] stars.[B][C]") ]])
	fnCutscene([[ Append("The stars were spinning out of some cataclysm, and as she stared into them, she realized it was the birth of a new universe.[P] The realization took hold of her mind, holding it with a grip no less absolute than the grip of the hands, but where the hands had brought a sense of dread, the grip on her mind brought a different feeling entirely.[P] Comfort.[B][C]") ]])
	fnCutscene([[ Append("Christine did not notice the world around her vanish into the empty nothing of a space outside the space of a new reality that had formed mere seconds ago.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
    
	fnCutscene([[ AL_SetProperty("Music", "Null") ]])
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ Append("Christine stared into the star-filled void that now existed within her arm, heedless to the world around her as the room and all that lay beyond it faded into the nothingness of a new reality that she herself had given birth to mere moments ago.[B][C]") ]])
	fnCutscene([[ Append("The space within her arm expanded to fill the empty void around her, dragging the stars with it until their light whited out all she could see.[B][C]") ]])
	fnCutscene([[ Append("The light of the stars shimmered as they burned through their fuel, expanded, went nova, and collapsed in upon themselves.[P] They spewed forth gasses and heavy elements into the vast spaces that had formed between them where before they had been close enough to touch.[B][C]") ]])
	fnCutscene([[ Append("As she gazed about herself, watching the spectacle, one star caught her attention.[P] It was a massive star, one she knew at once was the size of an entire galaxy.[P] She watched it as it burned through its fuel in mere seconds before bursting into an enormous nebula, then collapsing back in upon itself to form a massive black hole in the center.[B][C]") ]])
	fnCutscene([[ Append("The nebula spun around it, and new stars began to form in the wake of its death.[P] She looked out upon these stars, and saw that they were familiar.[B][C]") ]])
	fnCutscene([[ Append("She recognized the positions of some stars and the shapes they made.[P] These were the stars that would one day fill the skies of Pandemonium.[P] Regulus, her home, would soon join them in a mere few billion years.[P] She let out a sigh of relief as understanding washed over her.[B][C]") ]])
	fnCutscene([[ Append("This was the time before time.[P] There was no sentient life.[P] There could be no sentient life.[P] The creator would first need to make a world for people like her to live on.[P] She whiled away the years waiting for her home to form in the lingering clouds of gas and dust, passing millions of years in a mere blink, watching with an eager anticipation..[B][C]") ]])
	fnCutscene([[ Append("She began to notice the presence of another.[B][C]") ]])
	fnCutscene([[ Append("They would appear at the edge of her vision, giggling and laughing before darting back into the void just as she tried to look at them.[P] As the millions of years drifted by, she grew curious about them, and when she saw them peek out from a near-by star cluster, she decided to follow her.[B][C]") ]])
	fnCutscene([[ Append("She followed the creature across the void, from star to star, ever just a few moments behind.[P] Sometimes she was able to get within a few steps of the creature,[P] other times she lost sight of them entirely.[B][C]") ]])
	fnCutscene([[ Append("When this would happen, she would turn back to watch the birth of her future home, only to find the creature watching her from some nearby proto-planet or young star.[B][C]") ]])
	fnCutscene([[ Append("After many aeons of chase, the creature set down gently on a drifting rock and beckoned to her.[P] [P] She landed beside her, and saw that the girl was made of melted starlight.[P] Quietly the girl motioned for Christine to look behind her,[P] to look at what the rock orbited.[B][C]") ]])
	fnCutscene([[ Append("She turned around to find the event horizon of a singularity.[P] All around it space and time warped into a twisted vortex.[P] Christine reached out a trembling hand to touch it.[P] Even as the tips of her fingers neared the event horizon, the girl took her hand and held her back.") ]])
	fnCutsceneBlocker()
    
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
    fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DarkmatterTF0") ]])
    fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DarkmatterTF1") ]])
	fnCutscene([[ Append("The girl guided her hand downward, and Christine smiled at her.[P] Her hand slipped beneath the waist of her skirt, and she began to massage herself.[P] The girl stood behind her and embraced her, pulling her tight and placing her own hand over Christine's.[B][C]") ]])
	fnCutscene([[ Append("She guided it.[P] She showed Christine's unpracticed hands to how raise and lower herself until a tingling began to build up in her periphery.[P] The girl smiled in recognition as Christine's eyes fluttered, and quickened the pace to kindle the sensation.[B][C]") ]])
	fnCutscene([[ Append("Christine closed her eyes and let the girl teach her virgin hands the rhythm of her own womanhood.[P] She was building, and knew she would soon peak, but she did not want to.[P] She wanted the building sensation to last for all of time, to grow without limit until it would coincide with the birth of the next universe...[B][C]") ]])
	fnCutscene([[ Append("A surge of energy coursed through her body as the first wave of the orgasm struck.[P] Her eyes flew open as her chest grew tight, and she could see that they had drifted closer to the singularity, floating near its event horizon.[P] She looked down to where the girl's hands continued to guide her own, and she saw that they had changed.[B][C]") ]])
	fnCutscene([[ Append("They were metal, they were cold.[P] Metal lips kissed her neck as course hair brushed her cheek, and she turned her gaze just enough to see the smiling face of Sophie.[B][C]") ]])
	fnCutscene([[ Append("The girl of melted starlight smiled at her within her mind, even as the face of Sophie gave her a coy smile.[P] The orgasm began to well up within her at the sight of her tandem unit, and she knew that the girl was pleased that her new form could bring such joy to Christine.[B][C]") ]])
	fnCutscene([[ Append("Even as one hand was guided by the girl, Christine let her other hand drift over her body, but as she reached beneath her shirt, she realized her body was no longer human.[P] Her skin had become ethereal, her body translucent.[P] She was melting into starlight.[P] A curiosity began to come over her as she watched her body change.[B][C]") ]])
	fnCutscene([[ Append("A curiosity that ended as the girl's ministrations drew forth a new surge of growing orgasmic energy.[B][C]") ]])
	fnCutscene([[ Append("Her climax approached, and it loomed over her.[P] She wanted to hold it in, to let it grow, but she knew she could not.[P] In a mere million years, perhaps two, she would orgasm.[P] It would have cosmic ramifications.[B][C]") ]])
	fnCutscene([[ Append("She looked into the singularity and saw the future scrawled across its surface, compressed like all other time in the quantum nothingness.[P] She saw her orgasm would create a star, and around it would form many companion planets and celestial bodies.[P] One of these she recognized to be Pandemonium and its silent companion, Regulus.[B][C]") ]])
	fnCutscene([[ Append("She gazed upon what would be, and reached out to it.[P] Just as the tips of her fingers reached the edge of the nothingness, the girl pushed her over her own event horizon.[B][C]") ]])
	fnCutscene([[ Append("The orgasm tore her body apart, scattering it across the infinite as her toes curled.[P] Her mind remained a single distinct entity, but she allowed her body to be broken into countless smaller ones as the energy that had built up within her coursed through her, filling her as quickly as she spread across the void.[P] Then,[P] she entered the singularity, one and many.") ]])
	fnCutsceneBlocker()
    
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
    fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DarkmatterTF1") ]])
    fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DarkmatterTF2") ]])
	fnCutscene([[ Append("It was quiet inside the expanse of the black hole.[P] Peaceful.[P] She began to reconstitute herself as she waited for the singularity to dissipate, only to realize that her body had reformed as the same melted starlight as the girl who brought her to this place.[P] She turned her gaze to the edge of the void and saw all that lay beyond, and stepped out from it.[B][C]") ]])
	fnCutscene([[ Append("The energy swirled around her as she passed over the event horizon, the last lingering touch of her orgasm.[P] She emerged, having been born, conceived, and given birth all at the same singular moment.[P] As she looked out, a strange, pale women stood looking at her.[B][C]") ]])
	fnCutscene([[ Append("Gone was the girl who she had followed.[P] In her place stood the strange, pale woman.[P] She was was dressed in black and adorned with ribbons and skulls.[P] The woman gasped as if in shock as her own vision took in Christine, but her face remained flat and uninterested.[P] Christine giggled at the thought of it and waved to her, but could not be delayed.[B][C]") ]])
	fnCutscene([[ Append("The time was approaching, and she had someone she needed to be.[P] She reached back into a time to where her old self had first fallen into the space between space, and caught herself.[P] Christine was very glad she remembered to do that, or the loop would have lasted for eternity![B][C]") ]])
	fnCutscene([[ Append("She placed herself back at the hole in Serenity Crater's time, then pinched it shut.[P] The gap closed, and with her task complete, she grinned and merged herself with the physical body she had left behind those billions of years ago...") ]])
	fnCutsceneBlocker()
    fnCutsceneWait(gciDialogue_Fadeout_Then_Unload_Ticks)
    fnCutsceneBlocker()
	
	--Switch Christine to Darkmatter.
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Darkmatter.lua") ]])
    
    -- |[Clean]|
    fnCutscene([[ fnUnloadBitmapsFromList("Chapter 5 Christine Darkmatter TF") ]])
	
	--Transition.
	fnCutscene([[ AL_BeginTransitionTo("SerenityObservatoryA", "FORCEPOS:14.0x10.0x0") ]])
	fnCutsceneBlocker()

-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
