-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
--Door Exit.
if(sObjectName == "ToSerenityE") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	fnCutscene([[ AL_BeginTransitionTo("SerenityCraterE", "FORCEPOS:27.0x48.0x0") ]])
	fnCutsceneBlocker()
	
--Door Exit.
elseif(sObjectName == "ToSerenityG") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	fnCutscene([[ AL_BeginTransitionTo("SerenityCraterG", "FORCEPOS:15.5x4.0x0") ]])
	fnCutsceneBlocker()

-- |[Examinables]|
elseif(sObjectName == "TerminalA") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (Looks like there's some research logs on the local drive...)[B][C]") ]])
	fnCutscene([[ Append("Thought: (\"Molecular composition of the sample provided is still unknown, pending the results of the high-EM tests.[P] The substance is neither acidic nor basic in the conventional sense, but neutral is probably the wrong word to describe it.\")[B][C]") ]])
	fnCutscene([[ Append("Thought: (\"While ostensibly a granitic composition, that's complete nonsense.[P] Most of the crust of Regulus is andesitic to basaltic, with only the very surface area being granitic.[P] Thus we know with a great deal of certainty where this 'statue' was carved.[P] Not on Regulus.\")[B][C]") ]])
	fnCutscene([[ Append("Thought: (\"The uranic dating will provide some clues, no doubt.[P] If this object was created on Pandemonium and transported here recently, possibly as a bizarre ruse, then that's at least possible.[P] If not, then I am at a complete loss.\")") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalB") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (Looks like there's some research logs on the local drive...)[B][C]") ]])
	fnCutscene([[ Append("Thought: (\"The biological samples I was provided are most unusual.[P] They react very negatively to sudden temperature changes, but can survive in a vacuum complete unhindered.[P] This is like nothing organic I've ever seen.\")[B][C]") ]])
	fnCutscene([[ Append("Thought: (\"Unfortunately we won't have time to do more extensive testing at this facility, but I've forwarded everything I have to Central Administration.[P] I sincerely hope they authorize more tests and more sample collection.\")[B][C]") ]])
	fnCutscene([[ Append("Thought: (\"I also discovered something unusual.[P] The samples have a distinct metabolism, and are maintaining homeostasis, yet have no obvious food source.\")[B][C]") ]])
	fnCutscene([[ Append("Thought: (\"The 'blood' within them does not contain any sort of energy transfer compound like glucose.[P] If we can determine how they are storing energy, we could greatly improve our own power core technology.[P] This is very exciting!\")") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalC") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (Looks like there's some research logs on the local drive...)[B][C]") ]])
	fnCutscene([[ Append("Thought: (\"I was taken away from my research team and smuggled into this barren facility in the middle of nowhere to conduct routine zircon dating tests?[P] Is there some particular reason why I wasn't even allowed to tell my Slave Units where I was going?[P] No matter.\")[B][C]") ]])
	fnCutscene([[ Append("Thought: (\"The sample I was provided was a granite.[P] I was not told the origin rock, possibly because the sample is of unknown genesis.[P] The composition is unusual for Regulus and would likely be located in a very highly sodic deposition environment, probably somewhere on Pandemonium under a salt flat.  \")[B][C]") ]])
	fnCutscene([[ Append("Thought: (\"What is most curious is the age of the sample.[P] I was able to locate several zircon grains that I could establish uranium radiometric dates from.[P] The dates agree with one another, but not with our knowledge of physics.\")[B][C]") ]])
	fnCutscene([[ Append("Thought: (\"The current expected age of the universe has an upper bound of about 13.799 billion years.[P] It may be younger or older, based on which method is used, but that's our estimate based on the microwave background and current understanding of physics.\")[B][C]") ]])
	fnCutscene([[ Append("Thought: (\"This sample suggests a date over 20 billion years, which is considerably older than the age of Pandemonium, Regulus, the Bilarus solar system itself.[P] I have repeated the measurements multiple times and recalibrated the equipment.[P] It is not in error.[P] This rock is older than it should be.\")") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalD") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (Looks like there's some research logs on the local drive...)[B][C]") ]])
	fnCutscene([[ Append("Thought: (\"A biological sample burst when it was removed from its container.[P] We quarantined the area immediately and scrubbed down every unit who was in contact with it, but it seemed to be chemically inert.\")[B][C]") ]])
	fnCutscene([[ Append("Thought: (\"After several hours, we sent in a cleaning team.[P] We noticed something very unusual.[P] The biological material left a black, blue, or yellow imprint on areas when it was removed.\")[B][C]") ]])
	fnCutscene([[ Append("Thought: (\"While a biological residue is fairly common amongst a number of Pandemonium-bound species, what was most unusual is that the imprints were always in the shape of hands, despite the splatter pattern of the biological material.\")[B][C]") ]])
	fnCutscene([[ Append("Thought: (\"While more research is certainly required, Central Administration does not find it sufficiently interesting and has ordered us to decommission the facility immediately.[P] A shame.\")") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalE") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (The hard drive from this terminal was removed completely.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalF") then
	
	--Variables.
	local iSawFirstEDGScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawFirstEDGScene", "N")
	local iSawUndergroundSceneA = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawUndergroundSceneA", "N")
	local iSawSouthernCoreCutscene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawSouthernCoreCutscene", "N")
	
	--Has seen the underground scene.
	if(iSawUndergroundSceneA == 1.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought: (This log belongs to the administrator of this facility...)[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"WARNING::[P] Contents are classification code 7-B.[P] Command Units and specifically cleared Lord Units ONLY.[P] NO EXCEPTIONS.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"Administrative Log of Unit 2819.[P] I was assigned to this facility with the cover story of a rich ore vein having been discovered.[P] Command Unit 300910 appeared to believe that story and ceased her inquiries afterwards.[P] Good.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"We've secured the area and set up basic security checkpoints.[P] The Slave Units assigned to security detail were reassigned and underwent forcible reprogramming as per standard containment protocols.[P] We'll be using Drone Units from here on to prevent unnecessary reprogramming.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"I've ordered any units who were in contact with the artifact to be scrubbed multiple times by a variety of different cleaners, and ordered follow-up examinations after their reprogramming.[P] If there are any long-term effects due to exposure, I'd prefer to know about them before exposing myself.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"As such, all samples are to be sealed and provided via Drone Unit delivery, and all units experiencing contact are to wear protective clothing well in excess of the usual requirements.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"...\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"After several days of work, we have our laboratory space set up, and can begin analysis of the spatial disturbances reported.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"While contact with voidborne species such as the Rilmani and Darkmatters has been recorded on several instances, we've never been able to determine the exact method of their dimensional locomotion.[P] That ends today.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"When the artifact is bombarded with positrons from close range, spacetime itself begins to tear open.[P] This has been predicted by the work of Unit 91102 but never observed in practice.[P] The exact mechanism that the artifact uses to generate these rifts is unknown at the current time.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"Unlike the predicted mechanism, the rift does not travel to the other side of a dimensional fold.[P] Instead it appears to be some other dimension entirely.[P] Perhaps the old legends of the Inferno and the Stainless Vista are not mere folklore.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"Our next task will be to send remote-controlled units with videograph cameras attached through the rift to attempt to document whatever is on the other side.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"...\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"Central Administration has seen fit to vastly increase our material budget, but is refusing to add more personnel.[P] Fortunately the Drone Units don't complain about all the extra work.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"Our initial survey of the rift site has produced encouraging results.[P] The artifact only opens rifts to exactly one location, no matter where or how it is bombarded with positrons.[P] We sent a unit in with a radial camera and successfully retrieved her after six hours.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"The unit appeared no worse for wear, but I ordered her isolated pending observation anyway.[P] Central Administration has advised to exceed the normal maximum containment procedures.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"I am not usually prone to speculation, but Central Administration seems unusually keen to receive reports.[P] Further, they seem to know what I am going to report well in advance and frequently send me advice on how to achieve the results they desire.[P] It is...[P] off putting...\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"...\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"Currently, there are no reports of anything motile at the rift destination.[P] No subjects, organic or inorganic, have been located to date.[P] While the material that composes the walls, floors, and ceiling appears organic, it is definitely not alive.[P] It does, however, seem to respond to injury.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"We acquired some samples for study and I've sent them along to the laboratory staff.[P] Their excitement turned to dread after a single day of experimentation, but I've ordered them to press on.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"...\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"This is an official letter of protest addressed to Unit 2856.[P] We're making breakthrough after breakthrough here, and yet we have had our facility decommissioned and are being ordered to abandon everything where it is.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"I've requested a personal meeting with her but it's been denied.[P] As it stands, we'll have to follow orders.[P] It's a shame.[P] Perhaps this has something to do with the recent increase in Darkmatter sightings in the area.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (That was the last log entry.[P] It was dated...[P] one day after the Cryogenics incident...)") ]])
		fnCutsceneBlocker()
		
	--Hasn't seen it, begin the scene.
	else
	
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawUndergroundSceneA", "N", 1.0)
		
		--Spawn entities.
		TA_Create("DarkmatterA")
			TA_SetProperty("Position", -100, -100)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/DarkmatterGirl/", true)
		DL_PopActiveObject()
		TA_Create("DarkmatterB")
			TA_SetProperty("Position", -100, -100)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/DarkmatterGirl/", true)
		DL_PopActiveObject()
		TA_Create("DarkmatterC")
			TA_SetProperty("Position", -100, -100)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/DarkmatterGirl/", true)
		DL_PopActiveObject()
		TA_Create("DarkmatterD")
			TA_SetProperty("Position", -100, -100)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/DarkmatterGirl/", true)
		DL_PopActiveObject()
		TA_Create("DarkmatterE")
			TA_SetProperty("Position", -100, -100)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/DarkmatterGirl/", true)
		DL_PopActiveObject()
		TA_Create("DarkmatterF")
			TA_SetProperty("Position", -100, -100)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/DarkmatterGirl/", true)
		DL_PopActiveObject()
		TA_Create("DarkmatterG")
			TA_SetProperty("Position", -100, -100)
			TA_SetProperty("Facing", gci_Face_North)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/DarkmatterGirl/", true)
		DL_PopActiveObject()
		
		--Movement.
		fnCutsceneMove("Christine", 24.75, 25.50)
		fnCutsceneMove("Tiffany", 25.75, 25.50)
		fnCutsceneFace("Christine", 0,  -1)
		fnCutsceneFace("Tiffany", 0, -1)
		
		--Reading.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought: (This log belongs to the administrator of this facility...)[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"WARNING::[P] Contents are classification code 7-B.[P] Command Units and specifically cleared Lord Units ONLY.[P] NO EXCEPTIONS.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"Administrative Log of Unit 2819.[P] I was assigned to this facility with the cover story of a rich ore vein having been discovered.[P] Command Unit 300910 appeared to believe that story and ceased her inquiries afterwards.[P] Good.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"We've secured the area and set up basic security checkpoints.[P] The Slave Units assigned to security detail were reassigned and underwent forcible reprogramming as per standard containment protocols.[P] We'll be using Drone Units from here on to prevent unnecessary reprogramming.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"I've ordered any units who were in contact with the artifact to be scrubbed multiple times by a variety of different cleaners, and ordered follow-up examinations after their reprogramming.[P] If there are any long-term effects due to exposure, I'd prefer to know about them before exposing myself.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"As such, all samples are to be sealed and provided via Drone Unit delivery, and all units experiencing contact are to wear protective clothing well in excess of the usual requirements.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"...\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"After several days of work, we have our laboratory space set up, and can begin analysis of the spatial disturbances reported.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"While contact with voidborne species such as the Rilmani and Darkmatters has been recorded on several instances, we've never been able to determine the exact method of their dimensional locomotion.[P] That ends today.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"When the artifact is bombarded with positrons from close range, spacetime itself begins to tear open.[P] This has been predicted by the work of Unit 91102 but never observed in practice.[P] The exact mechanism that the artifact uses to generate these rifts is unknown at the current time.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"Unlike the predicted mechanism, the rift does not travel to the other side of a dimensional fold.[P] Instead it appears to be some other dimension entirely.[P] Perhaps the old legends of the Inferno and the Stainless Vista are not mere folklore.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"Our next task will be to send remote-controlled units with videograph cameras attached through the rift to attempt to document whatever is on the other side.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"...\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"Central Administration has seen fit to vastly increase our material budget, but is refusing to add more personnel.[P] Fortunately the Drone Units don't complain about all the extra work.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"Our initial survey of the rift site has produced encouraging results.[P] The artifact only opens rifts to exactly one location, no matter where or how it is bombarded with positrons.[P] We sent a unit in with a radial camera and successfully retrieved her after six hours.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"The unit appeared no worse for wear, but I ordered her isolated pending observation anyway.[P] Central Administration has advised to exceed the normal maximum containment procedures.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"I am not usually prone to speculation, but Central Administration seems unusually keen to receive reports.[P] Further, they seem to know what I am going to report well in advance and frequently send me advice on how to achieve the results they desire.[P] It is...[P] off putting...\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"...\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"Currently, there are no reports of anything motile at the rift destination.[P] No subjects, organic or inorganic, have been located to date.[P] While the material that composes the walls, floors, and ceiling appears organic, it is definitely not alive.[P] It does, however, seem to respond to injury.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"We acquired some samples for study and I've sent them along to the laboratory staff.[P] Their excitement turned to dread after a single day of experimentation, but I've ordered them to press on.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"...\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"This is an official letter of protest addressed to Unit 2856.[P] We're making breakthrough after breakthrough here, and yet we have had our facility decommissioned and are being ordered to abandon everything where it is.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (\"I've requested a personal meeting with her but it's been denied.[P] As it stands, we'll have to follow orders.[P] It's a shame.[P] Perhaps this has something to do with the recent increase in Darkmatter sightings in the area.\")[B][C]") ]])
		fnCutscene([[ Append("Thought: (That was the last log entry.[P] It was dated...[P] one day after the Cryogenics incident...)") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Music.
		fnCutscene([[ AL_SetProperty("Music", "Vivify") ]])
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] 55, did you read that?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Yes.[B][C]") ]])
        
        --Player has not seen the LRT scene.
		if(iSawFirstEDGScene == 0.0 and iSawSouthernCoreCutscene == 0.0) then
			fnCutscene([[ Append("Christine:[E|Neutral] Unit 2856 is adjacent to your designation.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] I realize that, but I do not know her.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Hmmm...[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] The biological samples collected here were transported to other locations.[P] Shortly afterwards, the Cryogenics Incident occurred.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] Further, if you recall the log files from Cryogenics, the biological subject they received had unusual egg-like objects in its body.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] It looks like some other samples were sent to the Biological Research Labs.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] But what does this all mean?[P] And where's this artifact the administrator mentioned?") ]])
        
        --Player has seen the LRT scene.
		elseif(iSawFirstEDGScene > 0.0) then
			fnCutscene([[ Append("Christine:[E|Neutral] Unit 2856....[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] My biological sister, before conversion.[P] She was involved here, somehow...[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] I believe it was she who contacted us in the data core at the LRT facility.[P] Considering her facial structure and vocal synthesizer are a near-perfect match for mine.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] What does this mean, though?[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] That, I do not know.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] I will note that the Cryogenics Incident occurred shortly after receiving a sample from this facility, and that other samples were sent to the Biological Research labs.[P] Look at the manifests.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] The dates coincide...[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] We should have a look at this artifact.[P] Where is it?") ]])
        
        --Player met 2856 in the western core entry.
        else
			fnCutscene([[ Append("Christine:[E|Neutral] Unit 2856....[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] You mentioned my 'strange behavior' in the LRT facility, did you not?[P] Do you recall?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] It was right before we saw what Vivify did to the data core's break room...[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] I believe Unit 2856 may be my biological sister, before our conversion.[P] Unfortunately, her records are highly classified.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] We would need to finish our work at the LRT facility in order to confirm, but I believe she is identical to me in terms of facial structure and vocal tones.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] So one day after the Cryogenics incident occurred, your sister shut down this facility.[P] Hmmm...[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] Correct.[P] She is a Prime Command Unit, as I was, and would have the requisite authority.[P] I can only assume she recognized a serious threat.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] The dates do coincide...[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] We should have a look at this artifact.[P] Where is it?") ]])
        
		end
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
        
		--Movement.
		fnCutsceneTeleport("DarkmatterA", 16.25, 17.50)
		fnCutsceneTeleport("DarkmatterB", 17.25, 17.50)
		fnCutsceneTeleport("DarkmatterC",  6.25, 33.50)
		fnCutsceneTeleport("DarkmatterD",  6.25, 34.50)
		fnCutsceneTeleport("DarkmatterE", 13.25, 33.50)
		fnCutsceneTeleport("DarkmatterF", 13.25, 34.50)
		fnCutsceneBlocker()
		fnCutsceneMove("DarkmatterA", 16.25, 25.50)
		fnCutsceneMove("DarkmatterA", 20.25, 26.50)
		fnCutsceneFace("DarkmatterA", 1, 1)
		fnCutsceneMove("DarkmatterB", 17.25, 25.50)
		fnCutsceneMove("DarkmatterB", 19.15, 27.50)
		fnCutsceneMove("DarkmatterB", 19.15, 28.50)
		fnCutsceneFace("DarkmatterB", 1, 0)
		fnCutsceneMove("DarkmatterC", 18.25, 33.50)
		fnCutsceneFace("DarkmatterC", 1, -1)
		fnCutsceneMove("DarkmatterD", 18.25, 34.50)
		fnCutsceneFace("DarkmatterD", 1, -1)
		fnCutsceneMove("DarkmatterE", 14.25, 33.50)
		fnCutsceneMove("DarkmatterF", 14.25, 34.50)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", -1, 0)
		fnCutsceneFace("Tiffany", -1, 1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] 55?[P] Did you hear that?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] We are not alone...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--G teleports in.
		fnCutsceneTeleport("DarkmatterG", 28.25, 28.50)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", 1, 0)
		fnCutsceneFace("Tiffany", 1, 0)
		fnCutsceneMove("DarkmatterG", 28.25, 27.50)
		fnCutsceneFace("DarkmatterG", -1, 0)
		fnCutsceneBlocker()
		fnCutsceneWait(65)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Yikes![P] Where did you come from?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Back off, Darkmatter![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Will you *please* calm down?[P] She's doesn't look aggressive at all...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Hey, wait a second.[P] You're the Darkmatter who wanted to see my runestone, aren't you?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Come here, Little Lady.[P] We're not going to hurt you.[P] C'mon.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Movement.
		fnCutsceneMove("Christine", 25.25, 26.50)
		fnCutsceneMove("Tiffany", 25.25, 27.50)
		fnCutsceneFace("Christine", 1, 0)
		fnCutsceneFace("Tiffany", 1, 0)
		fnCutsceneMove("DarkmatterG", 27.25, 27.50, 0.20)
		fnCutsceneMove("DarkmatterG", 26.25, 26.50, 0.20)
		fnCutsceneFace("DarkmatterG", -1, 0)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "DarkmatterGirl", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] You want to see my runestone again?[P] Okay, but -[P] Eep![B][C]") ]])
		fnCutscene([[ Append("Darkmatter: Touch.[P] Help.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Oh my goodness![P] You can talk?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] What's happening?[P] What'd she do?[B][C]") ]])
		fnCutscene([[ Append("Darkmatter: No.[P] Hurt.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Uh, did you hear that, 55?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] No![P] Back off, Darkmatter![P] Get back![B][C]") ]])
		fnCutscene([[ Append("Darkmatter: No.[P] Hurt.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Wait![P] I think she said she's not going to hurt us![B][C]") ]])
		fnCutscene([[ Append("Darkmatter: Wrong.[P] Doll.[P] Hurt.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Err, or she -[P] doesn't want you to hurt her?[B][C]") ]])
		fnCutscene([[ Append("Darkmatter: Wrong.[P] Doll.[P] Hurt.[P] Doll.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Okay okay, I think I get it.[P] She doesn't want you to hurt yourself![B][C]") ]])
		fnCutscene([[ Append("Darkmatter: Good.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] ...[P] How do you know that?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] You can't hear that?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Maybe I can hear it through my runestone...[B][C]") ]])
		fnCutscene([[ Append("Darkmatter: Help.[P] Please.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Help you?[P] Do you want my help?[B][C]") ]])
		fnCutscene([[ Append("Darkmatter: Please.[P] Follow.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] And just where are you going?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] She wants me to follow her.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Movement.
		fnCutsceneMove("DarkmatterG", 26.25, 30.50)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", 0, 1)
		fnCutsceneFace("Tiffany", 0, 1)
		fnCutsceneBlocker()
		fnCutsceneWait(45)
		fnCutsceneBlocker()
		fnCutsceneFace("DarkmatterG", 0, -1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneMove("Christine", 26.25, 26.50)
		fnCutsceneMove("Christine", 26.25, 28.50)
		fnCutsceneBlocker()
		fnCutsceneFace("Tiffany", 1, 1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "DarkmatterGirl", "Neutral") ]])
		fnCutscene([[ Append("55:[E|Neutral] This is pure idiocy.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] If you've got a better idea, I'm open to suggestion.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] It seems you are.[P] That thing is using you.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] I'll be the judge of that.[P] She needs our help.[B][C]") ]])
		fnCutscene([[ Append("Darkmatter:[E|Neutral] Wrong.[P] You.[P] Just.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Uh, just me?[P] Okay...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Are you coming, 55?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] I will be keeping my weapon trained on you, creature.[P] Don't do anything you'll regret.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Fall in.
		fnCutsceneMove("Christine", 25.25, 28.50)
		fnCutsceneFace("Christine", 0, 1)
		fnCutsceneMove("DarkmatterG", 24.35, 30.50)
		fnCutsceneFace("DarkmatterG", 1, 0)
		fnCutsceneBlocker()
		fnCutsceneMove("Christine", 25.25, 30.50)
		fnCutsceneMove("Tiffany", 25.25, 29.50)
		fnCutsceneBlocker()
		fnCutsceneMove("Christine", 24.25, 30.50)
		fnCutsceneMove("Tiffany", 25.25, 30.50)
		fnCutsceneMove("DarkmatterG", 23.25, 30.50)
		fnCutsceneBlocker()
		fnCutsceneMove("DarkmatterG", 19.75, 30.50)
		fnCutsceneMove("Christine", 20.75, 30.50)
		fnCutsceneMove("Tiffany", 21.75, 30.50)
		fnCutsceneBlocker()
		fnCutsceneMove("DarkmatterG", 19.75, 31.50)
		fnCutsceneMove("Christine", 19.75, 30.50)
		fnCutsceneMove("Tiffany", 20.75, 30.50)
		fnCutsceneBlocker()
		fnCutsceneMove("DarkmatterG", 19.75, 32.50)
		fnCutsceneMove("Christine", 19.75, 31.50)
		fnCutsceneMove("Tiffany", 19.75, 30.50)
		fnCutsceneBlocker()
		fnCutsceneMove("DarkmatterG", 19.75, 35.50)
		fnCutsceneMove("Christine", 19.75, 34.50)
		fnCutsceneMove("Tiffany", 19.75, 33.50)
		fnCutsceneBlocker()
        
        --Guards move aside.
		fnCutsceneMove("DarkmatterGuardL", 18.25, 37.50)
		fnCutsceneMove("DarkmatterGuardR", 21.25, 37.50)
		fnCutsceneFace("DarkmatterGuardL", 1, 0)
		fnCutsceneFace("DarkmatterGuardR", -1, 0)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
        
        --Open the door.
        fnCutscene([[ AL_SetProperty("Open Door", "WideDoor") ]])
        fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
        
        --Everyone moves through.
		fnCutsceneMove("DarkmatterG", 19.75, 42.50)
		fnCutsceneMove("Christine",   19.75, 41.50)
		fnCutsceneMove("Tiffany",     19.75, 40.50)
		fnCutsceneBlocker()
		fnCutsceneMove("DarkmatterG", 20.75, 42.50)
		fnCutsceneMove("Christine",   19.75, 42.50)
		fnCutsceneMove("Tiffany",     19.75, 41.50)
		fnCutsceneBlocker()
		fnCutsceneMove("DarkmatterG", 21.75, 42.50)
		fnCutsceneMove("Christine",   20.75, 42.50)
		fnCutsceneMove("Tiffany",     19.75, 42.50)
		fnCutsceneBlocker()
		fnCutsceneMove("DarkmatterG", 38.75, 42.50)
		fnCutsceneFace("DarkmatterG", 0, 1)
		fnCutsceneMove("Christine", 37.75, 42.50)
		fnCutsceneMove("Tiffany", 36.75, 42.50)
		fnCutsceneBlocker()
		fnCutsceneWait(35)
		fnCutsceneBlocker()
		fnCutsceneFace("Tiffany", 0, 1)
		fnCutsceneFace("Christine", 0, 1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "DarkmatterGirl", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] I've got a very bad feeling about this...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Movement.
		fnCutsceneMove("Christine", 37.75, 46.50)
		fnCutsceneMove("Tiffany", 36.75, 46.50)
		fnCutsceneMove("DarkmatterG", 38.75, 46.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Transport to the next level.
		fnCutscene([[ AL_BeginTransitionTo("SerenityCraterG", "FORCEPOS:16.0x4.0x0") ]])
		fnCutsceneBlocker()
		
	end
	
elseif(sObjectName == "TerminalG") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (The defragmentation monitor shows a massive increase in processor usage when this unit was defragmenting...[P] Nightmares, perhaps?)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalH") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (The logs indicate this unit defragmented normally until the third day she was stationed here, at which point she stopped defragmenting and injected oil into her power core to maintain functionality.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalI") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (The unit stationed here was reassigned after reporting auditory hallucinations.[P] Her maintenance log came back clean...)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalJ") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (This unit requested transfers after two days in the facility.[P] She was reassigned when the facility was decommissioned...)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TubeFull") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (The storage tube has a number of small organic objects floating in it.[P] They look somewhat like eggs, but are porous.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "Chemicals") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (Chemicals used for testing and identifying compounds.[P] There's a variety of acids and bases here, and various dissolved pure metals.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "EmptyTube") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (A storage tube.[P] This one wasn't cleaned before being put in storage, and has traces of residue around the edges.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "SecurityStationDoor") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (This door leads to the security station.[P] It's locked and probably not worth hacking open.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TableGross") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (The table is covered in some sort of organic material.[P] It appears to be corroding the metal in some places, but not in others.[P] Is it choosing to corrode in some places, perhaps?)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "WideDoor") then
    
    --Variables.
    local iSawUndergroundSceneA = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawUndergroundSceneA", "N")
    if(iSawUndergroundSceneA == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought: (The door isn't locked, but something is holding it shut...)") ]])
        fnCutsceneBlocker()
	else
        AL_SetProperty("Open Door", "WideDoor")
        AudioManager_PlaySound("World|AutoDoorOpen")
    
    end
-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
