-- |[Constructor]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveFolderName(fnResolvePath())

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
    local iCompletedSerenity    = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
    local iSawUndergroundSceneA = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawUndergroundSceneA", "N")
    if(iCompletedSerenity == 1.0 or iSawUndergroundSceneA == 0.0) then
        AL_SetProperty("Music", "Null")
    else
        AL_SetProperty("Music", "Vivify")
    end
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation(sLevelName)
	
	-- |[Spawn NPCS Here]|
	--NPCs set to spawn here will be available for post-exec cutscenes.
	
	-- |[Lights]|
	AL_SetProperty("Activate Lights")
	AL_SetProperty("Set Player Light No Drain", true)

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[Spawn NPCs Here]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    local sBasePath = fnResolvePath()
    local iCompletedSerenity    = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
    local iSawUndergroundSceneA = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawUndergroundSceneA", "N")
    if(iCompletedSerenity == 1.0) then return end
    
    --Darkmatters holding the door shut.
    if(iSawUndergroundSceneA == 0.0) then
        TA_Create("DarkmatterGuardL")
            TA_SetProperty("Position", 19, 37)
            TA_SetProperty("Facing", gci_Face_North)
            TA_SetProperty("Clipping Flag", true)
            fnSetCharacterGraphics("Root/Images/Sprites/DarkmatterGirl/", true)
        DL_PopActiveObject()
        TA_Create("DarkmatterGuardR")
            TA_SetProperty("Position", 20, 37)
            TA_SetProperty("Facing", gci_Face_North)
            TA_SetProperty("Clipping Flag", true)
            fnSetCharacterGraphics("Root/Images/Sprites/DarkmatterGirl/", true)
        DL_PopActiveObject()
        
        
    --Darkmatters who were holding the door shut.
    elseif(iSawUndergroundSceneA == 1.0) then
        
        --Entities.
        TA_Create("DarkmatterGuardL")
            TA_SetProperty("Position", 18, 37)
            TA_SetProperty("Facing", gci_Face_East)
            TA_SetProperty("Clipping Flag", true)
            fnSetCharacterGraphics("Root/Images/Sprites/DarkmatterGirl/", true)
            TA_SetProperty("Activation Script", sBasePath .. "Dialogue.lua")
        DL_PopActiveObject()
        TA_Create("DarkmatterGuardR")
            TA_SetProperty("Position", 21, 37)
            TA_SetProperty("Facing", gci_Face_West)
            TA_SetProperty("Clipping Flag", true)
            fnSetCharacterGraphics("Root/Images/Sprites/DarkmatterGirl/", true)
            TA_SetProperty("Activation Script", sBasePath .. "Dialogue.lua")
        DL_PopActiveObject()
        TA_Create("DarkmatterMiscA")
            TA_SetProperty("Position", 18, 42)
            TA_SetProperty("Facing", gci_Face_East)
            TA_SetProperty("Clipping Flag", true)
            fnSetCharacterGraphics("Root/Images/Sprites/DarkmatterGirl/", true)
            TA_SetProperty("Activation Script", sBasePath .. "Dialogue.lua")
        DL_PopActiveObject()
        TA_Create("DarkmatterMiscB")
            TA_SetProperty("Position", 28, 37)
            TA_SetProperty("Facing", gci_Face_SouthWest)
            TA_SetProperty("Clipping Flag", true)
            fnSetCharacterGraphics("Root/Images/Sprites/DarkmatterGirl/", true)
        DL_PopActiveObject()
    end
end
