-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[GolemBA]|
    if(sActorName == "GolemBA") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] We had a group meeting to determine breaktime policy.[P] We unanimously decided to triple the number of breaks.[P] I was surprised that Unit 300910 agreed, but she did!") ]])
        fnCutsceneBlocker()
        
    -- |[GolemBB]|
    elseif(sActorName == "GolemBB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I haven't been to Regulus City in some time.[P] Is it true that synthleather seating is reserved for Lord Units?[P] It's not like it's hard to fabricate...") ]])
        fnCutsceneBlocker()

    end
end
