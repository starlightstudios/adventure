-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToExteriorEF") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusExteriorEF", "FORCEPOS:36.5x17.0x0")

--Elevator.
elseif(sObjectName == "Elevator") then

	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Christine](Which floor should I go to?)[BLOCK]") ]])
	
	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Main Floor\", " .. sDecisionScript .. ", \"ElevatorToMainFloor\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Observation Deck\", " .. sDecisionScript .. ", \"ElevatorToObservation\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Crater Access Basement\", " .. sDecisionScript .. ", \"ElevatorToBasement\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Cancel\",  " .. sDecisionScript .. ", \"NoClose\") ")
	fnCutsceneBlocker()

--Elevator to Domiciles.
elseif(sObjectName == "ElevatorToMainFloor") then
	WD_SetProperty("Hide")
	fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutscene([[ AL_BeginTransitionTo("SerenityObservatoryA", "FORCEPOS:20.5x4.0x0") ]])
	fnCutsceneBlocker()
    giForceFacing = gci_Face_South

--Elevator to Observation Deck.
elseif(sObjectName == "ElevatorToObservation") then
	WD_SetProperty("Hide")
	fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutscene([[ AL_BeginTransitionTo("SerenityObservatoryC", "FORCEPOS:19.5x4.0x0") ]])
	fnCutsceneBlocker()
    giForceFacing = gci_Face_South
	
--Elevator to the basement.
elseif(sObjectName == "ElevatorToBasement") then
	WD_SetProperty("Hide")
	fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutscene([[ AL_BeginTransitionTo("SerenityObservatoryD", "FORCEPOS:15.5x4.0x0") ]])
	fnCutsceneBlocker()
    giForceFacing = gci_Face_South
	
--Cancel Elevator.
elseif(sObjectName == "NoClose") then
	WD_SetProperty("Hide")

-- |[Examinables]|
elseif(sObjectName == "DrinkMachines") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (Fizzy Pop![P] The best way to recharge your power core after a long day![P] A pink Lord Unit machine is here.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "OilmakerA") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (An oilmaker with a number of custom flavours, such as rubber, screws, plastic, and of course 'Industrial Waste'.[P] The package says it's real waste imported from the factories under Regulus City, but everyone knows it's made in some chemical vat.[P] Totally fake.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "OilmakerB") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (An oilmaker.[P] This one makes two varieties::[P] Black, and Really Black.[P] How much more black could it be?[P] I guess the answer is...[P] none.[P] None more black.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalOilmaker") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (The defragmentation log indicates the unit here doesn't defragment very often...[P] I wonder where she gets the energy?)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalTable") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (This terminal was last used to display a list of commendations this unit has received from Unit 300910.[P] Way to go!)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalLib") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (There's a reprimand notice on here from a Command Unit assigned to an abduction team.[P] Apparently, her staff were asked to acquire human artifacts for a personal collection...)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Books") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (A collection of books, authentic books, from the surface of Pandemonium![P] They're in excellent condition.[P] I guess the unit here likes reading old-style.)[B][C]") ]])
	fnCutscene([[ Append("Thought: (This novella is called 'Magical Settlement', and seems to be about a man who becomes a magical girl through a series of mishaps.)[B][C]") ]])
	fnCutscene([[ Append("Thought: (...[P] I really feel for this guy...)[B][C]") ]])
	fnCutscene([[ Append("Thought: (...[P] Giant bee girls?[P] Really?[P] That's ridiculous!)[B][C]") ]])
	fnCutscene([[ Append("Thought: (...)[B][C]") ]])
	fnCutscene([[ Append("Thought: (... Oh wow, I hope there's a way to make his butt go back to normal.)[B][C]") ]])
	fnCutscene([[ Append("Thought: (...)[B][C]") ]])
	fnCutscene([[ Append("Thought: (... It ends there?[P] Aww, I wanted to see where this was going!)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalRec") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (Notice from Command Unit 300910::[P] Please remove videographs from the display terminal if you're done with them and store them on your own local terminals.[P] The RVD terminal is running low on drive space.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "RVDScreen") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (A Remote Videograph Display.[P] It's currently set to play the episodes of All My Processors in a random order.[P] I guess it really doesn't matter which order you see them in, since *anything can happen!*)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalTableB") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (The terminal has a set of repair manuals queued to be uploaded during defragmentation.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalNothing") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (There's a picture of a Slave Unit blowing a kiss at the camera.[P] At the bottom it says 'I miss you, come see me soon' in curly text, and there are hearts all over it.)[B][C]") ]])
	fnCutscene([[ Append("Thought: (This is wildly inappropriate and I should report this to Unit 300910 immediately!)[B][C]") ]])
	fnCutscene([[ Append("Thought: (...)[B][C]") ]])
	fnCutscene([[ Append("Thought: (These should be power cores, not organic hearts![P] What sort of unit doesn't know that!?)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalNothingB") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (Administrative records, going back until the Observatory was founded.[P] They're set to be synched whenever the defragmentation pod is used.)") ]])
	fnCutsceneBlocker()

-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
