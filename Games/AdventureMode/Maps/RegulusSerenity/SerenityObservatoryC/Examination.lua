-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
--Elevator.
if(sObjectName == "Elevator") then

	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Christine](Which floor should I go to?)[BLOCK]") ]])
	
	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Main Floor\", " .. sDecisionScript .. ", \"ElevatorToMainFloor\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Domicile Block\", " .. sDecisionScript .. ", \"ElevatorToDomiciles\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Crater Access Basement\", " .. sDecisionScript .. ", \"ElevatorToBasement\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Cancel\",  " .. sDecisionScript .. ", \"NoClose\") ")
	fnCutsceneBlocker()

--Elevator to Main Floor.
elseif(sObjectName == "ElevatorToMainFloor") then
	WD_SetProperty("Hide")
	fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutscene([[ AL_BeginTransitionTo("SerenityObservatoryA", "FORCEPOS:20.5x4.0x0") ]])
	fnCutsceneBlocker()
    giForceFacing = gci_Face_South

--Elevator to Domiciles.
elseif(sObjectName == "ElevatorToDomiciles") then
	WD_SetProperty("Hide")
	fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutscene([[ AL_BeginTransitionTo("SerenityObservatoryB", "FORCEPOS:19.5x4.0x0") ]])
	fnCutsceneBlocker()
    giForceFacing = gci_Face_South
	
--Elevator to the basement.
elseif(sObjectName == "ElevatorToBasement") then
	WD_SetProperty("Hide")
	fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutscene([[ AL_BeginTransitionTo("SerenityObservatoryD", "FORCEPOS:15.5x4.0x0") ]])
	fnCutsceneBlocker()
    giForceFacing = gci_Face_South
	
--Cancel Elevator.
elseif(sObjectName == "NoClose") then
	WD_SetProperty("Hide")

-- |[Examinables]|
elseif(sObjectName == "TerminalA") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (Records of astronomical data.[P] Particular attention is paid to black holes and pulsars, by order of Central Administration.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalB") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (A series of postulations on the composition of Darkmatter Girls.[P] A number of possible substances are proposed, such as quantum-synchronized neutrinos or double-membrane leptons in a halogen soup.[P] All of the propositions have serious problems...)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalC") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (Artillery Telemetry.[P] Seems Unit 300910 wants to launch satellites from the observatory, but Central Administration has denied all requests.[P] She has even created schematics for a launcher cannon based on Pulse Rifle technology.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "Telescope") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (A conventional optical telescope, with a chassis made from brass.[P] It's beautifully polished and well maintained, though it's not hooked to any computer systems.)") ]])
	fnCutsceneBlocker()

-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
