-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Standard ======================================== ]|
if(sObjectName == "WarpActivate") then
    fnWarpUnlockHandler("Root/Variables/Chapter5/Campfires/iSerenityObservatoryE", 28.25, 9.50)
    
-- |[ ================================= Meeting the Darkmatter ================================= ]|
elseif(sObjectName == "CutsceneTrigger") then
	
	--Repeat check.
	local iSawDarkmatterIntro = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawDarkmatterIntro", "N")
	if(iSawDarkmatterIntro == 1.0) then return end
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSawDarkmatterIntro", "N", 1.0)
    
    --Variables.
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    
    --Merge.
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneMove("Christine", 22.25, 17.50)
	fnCutsceneFace("Christine", 0, 1)
	fnCutsceneMove("Tiffany", 23.25, 17.50)
	fnCutsceneFace("Tiffany", 0, 1)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Darkmatter turns to face.
	fnCutsceneFace("Darkmatter", -1, -1)
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] ^Oh look, there's a Darkmatter.^[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] ^Weapon up.^[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] ^Come on, 55.[P] She doesn't look dangerous.^") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneMove("Darkmatter", 22.25, 18.50)
	fnCutsceneFace("Darkmatter", 0, -1)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "DarkmatterGirl", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] ^Hello little lady.[P] You're such a cutie wootie![P] My power core's going to miss a cycle!^[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] ^Stay on your guard.^[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] ^She's not attacking, she's just curious.[P] Right, little lady?^[B][C]") ]])
    if(sChristineForm == "Darkmatter") then
        fnCutscene([[ Append("Christine:[E|Neutral] ^Look, I know for a fact that you can understand me, but we have to play along in order to make sure we don't mess up time.^[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] ^More than we already have, evidently.^[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] ^What are you saying?[P] Your radio frequency was covered in static.^[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] ^Oh, nothing.[P] Sorry, user error.^[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] ^Communicating with darkmatters is pointless, they cannot understand us.^[B][C]") ]])
    else
        fnCutscene([[ Append("55:[E|Neutral] ^Stop asking it questions, it can't hear you.^[B][C]") ]])
    end
	fnCutscene([[ Append("Christine:[E|Neutral] ^There is no way that you, or anyone else, actually knows that, 55.[P] Don't be rude to the poor Darkmatter.^") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneMove("Darkmatter", 22.25, 18.10, 0.10)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "DarkmatterGirl", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] ^Woah, she's nuzzling me!^[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] ^...^[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] ^Give it a rest, 55.[P] Sheesh.^[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] ^She is not being affectionate.[P] She is trying to get your runestone.^[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] ^...[P] Oh you want to see that?[P] Okay, take a look.^[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] ^Be careful![P] What if she steals it?^[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] ^You just don't understand how trust works, do you?^[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] ^If I'm going to get her to trust me, I have to show I'm willing to trust her.^[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] ^...^[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] ^See?[P] She's just curious.^") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(185)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "DarkmatterGirl", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] ^Well okay, enough playing with my runestone.^") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Darkmatter runs off.
	fnCutsceneMoveFace("Darkmatter", 22.25, 19.10, 0, -1, 0.20)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneMove("Darkmatter", 22.25, 21.10, 2.00)
	fnCutsceneMove("Darkmatter", 25.25, 24.10, 2.00)
	fnCutsceneMove("Darkmatter", 25.25, 27.10, 2.00)
	fnCutsceneMove("Darkmatter", 15.25, 27.10, 2.00)
	fnCutsceneMove("Darkmatter", 15.25, 26.10, 2.00)
	fnCutsceneMove("Darkmatter",  8.25, 26.10, 2.00)
	fnCutsceneBlocker()
	fnCutsceneTeleport("Darkmatter", -100.25, -100.50)
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] ^Was it something I radioed?^[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] ^Something about that runestone had her attention.^[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] ^Still, we should be extremely cautious.[P] Darkmatters are difficult to influence with physical weaponry.^[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] ^Hmm...^") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Move 55 onto Christine.
	fnCutsceneMove("Tiffany", 22.25, 17.50)
	fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()

end
