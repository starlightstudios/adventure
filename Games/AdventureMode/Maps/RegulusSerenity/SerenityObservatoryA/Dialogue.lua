-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Darkmatter]|
    if(sActorName == "Darkmatter") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Darkmatter:[VOICE|Darkmatter] You have done well.[P] We will try to live up to your expectations here.") ]])
        fnCutsceneBlocker()
        
    -- |[GolemAA]|
    elseif(sActorName == "GolemAA") then
	
        --Variables.
        local iCompletedSerenity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
        
        --Not completed the questline yet.
        if(iCompletedSerenity == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] You're visiting from Regulus City?[P] Do you know any units in the abductions department?[P] I -[P] oh, uh nevermind...") ]])
            fnCutsceneBlocker()
        
        --Completed the questline.
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Unit 2855 told us everything.[P] You're a true hero to us.") ]])
            fnCutsceneBlocker()
        end
        
    -- |[GolemAB]|
    elseif(sActorName == "GolemAB") then
	
        --Variables.
        local iCompletedSerenity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
    
        --Not completed the questline yet.
        if(iCompletedSerenity == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] I don't understand why the darkmatters would start attacking units.[P] We've always gotten along just fine before, why now?") ]])
            fnCutsceneBlocker()
        
        --Completed the questline.
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] So the darkmatters were just trying to protect us?[P] I guess they're a lot smarter than we thought.") ]])
            fnCutsceneBlocker()
        end
    
    -- |[ConcernedGolemA]|
    elseif(sActorName == "ConcernedGolemA") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Do not worry, 771852, your secret is safe with us.") ]])
        fnCutsceneBlocker()
    
    -- |[ConcernedGolemB]|
    elseif(sActorName == "ConcernedGolemB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Perhaps there is far more to the universe than we can hope to understand...") ]])
        fnCutsceneBlocker()
    
    -- |[ConcernedGolemC]|
    elseif(sActorName == "ConcernedGolemC") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Everyone was so worried you were hurt, but I'll be honest, I was thinking you were just the most adorable little thing...") ]])
        fnCutsceneBlocker()
    
    -- |[ConcernedGolemD]|
    elseif(sActorName == "ConcernedGolemD") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Unit 2855 told us everything.[P] We're in your debt.[P] You'll always have friends at Serenity Crater Observatory.") ]])
        fnCutsceneBlocker()
    
    -- |[300910]|
    elseif(sActorName == "300910") then
    
        --Variables.
        local iCompletedSerenity      = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
        local i300910ToldAboutCredits = VM_GetVar("Root/Variables/Chapter5/Scenes/i300910ToldAboutCredits", "N")
        local sChristineForm          = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
        local i300910Raiju            = VM_GetVar("Root/Variables/Chapter5/Scenes/i300910Raiju", "N")
        
        --Raiju!
        if(i300910Raiju == 0.0 and sChristineForm == "Raiju") then
            VM_SetVar("Root/Variables/Chapter5/Scenes/i300910Raiju", "N", 1.0)
    
            --Dialogue.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
            fnCutscene([[ Append("300910: A raiju![P] In here?[P] How!?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I - [P][CLEAR]") ]])
            fnCutscene([[ Append("300910: Fluffy tail, fluffy tail![P] Mmmm![B][C]") ]])
            fnCutscene([[ Append("300910: *She nuzzles into Christine's tail without warning.*[B][C]") ]])
            fnCutscene([[ Append("300910: Oh it's been so long since I cuddled a raiju.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Laugh] Yummy![P] That was fun![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Blush] Oh great, now I know why all the raijus are airheads.[P] That's...[P] so much fun...[P] I love cuddling dolls...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Are you two quite done?[B][C]") ]])
            fnCutscene([[ Append("300910: Yes, just, give me a moment...") ]])
            return
        end
        
        --Special dialogue.
        if(iCompletedSerenity == 1.0 and i300910ToldAboutCredits == 0.0) then
        
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/i300910ToldAboutCredits", "N", 1.0)
            
            --Dialogue.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
            fnCutscene([[ Append("300910: Oh, well, this might seem odd to mention, but I already flagged the work order completed.[B][C]") ]])
            fnCutscene([[ Append("300910: You should have 300 additional credits in your account now.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Oh, thank you very much.[B][C]") ]])
            fnCutscene([[ Append("300910: Considering everything that's happened, it's really the least I can do.[B][C]") ]])
            fnCutscene([[ Append("300910: If there's anything, and I do mean anything, that I or the Observatory can do to help, we'll do it.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] There is.[B][C]") ]])
            fnCutscene([[ Append("300910: Name it.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Assist us in our rebellion.[B][C]") ]])
            fnCutscene([[ Append("300910: !!![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] If not, then don't tell anyone what 55 just said.[B][C]") ]])
            fnCutscene([[ Append("300910: No, no...[P] We'll help. Serenity Crater Observatory...[P] will help you.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] You mean it?[B][C]") ]])
            fnCutscene([[ Append("300910: 2855 gave us a full rundown on what they did in the crater and elsewhere.[P] She also informed us of your plans.[P] We know.[B][C]") ]])
            fnCutscene([[ Append("300910: And...[P] it pains me to say it.[P] We believe in the Cause of Science.[P] Everyone does.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] So do we.[B][C]") ]])
            fnCutscene([[ Append("300910: But it has to have limits.[P] What if this had happened on Pandemonium?[P] What if more of those things ran amok?[P] How many humans would have died?[B][C]") ]])
            fnCutscene([[ Append("300910: We're not soldiers, but we'll help.[P] I know every Golem here agrees with me.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] This observatory is well-situated to resist assault.[P] I would recommend you acquire whatever supplies you can, discreetly.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] You will also need to acquire an independent power supply.[P] The facility in the crater is unused, send a team down when the area is cleared.[B][C]") ]])
            fnCutscene([[ Append("300910: But...[P] when the shooting starts...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] You and your researchers will have to retire enemy security teams.[B][C]") ]])
            fnCutscene([[ Append("300910: ...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I'm sorry...[B][C]") ]])
            fnCutscene([[ Append("300910: So this is what it's come to.[B][C]") ]])
            fnCutscene([[ Append("300910: I guess this is part of being a Command Unit.[P] I -[P] I know some of my friends here will die.[P] But it's the right thing to do, and I can't escape that.[B][C]") ]])
            fnCutscene([[ Append("300910: All I can do is prepare and try to minimize those losses.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] When Christine and I are prepared, I will assist you.[P] I can provide training and updated combat algorithms.[B][C]") ]])
            fnCutscene([[ Append("300910: Thank you...[B][C]") ]])
            fnCutscene([[ Append("300910: Now.[P] Was there anything else?[P] Something less grim, I hope.[B][C]") ]])
        
        
        --Normal dialogue.
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
            fnCutscene([[ Append("300910: If there's anything I can do to help, just let me know.[B][C]") ]])
        end

        --Topic unlocks.
        WD_SetProperty("Unlock Topic", "2855", 1)

        -- |[Topics]|
        --Activate topics mode once the dialogue is complete.
        fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "300910") ]])
    end
end
