-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToExteriorEF") then
    
	--If Christine is an organic:
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	if(sChristineForm == "Human" or sChristineForm == "Raiju") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Probably unwise to head into a vacuum while organic.)") ]])
        fnCutsceneBlocker()
    
    else
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_BeginTransitionTo("RegulusExteriorEF", "FORCEPOS:36.5x17.0x0")
    end

--Elevator.
elseif(sObjectName == "Elevator") then

    --Hasn't seen intro yet.
	local iSawSerenityIntro = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawSerenityIntro", "N")
	if(iSawSerenityIntro == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](I should speak to the unit in charge before we go anywhere else.)") ]])
        return
    end

	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Christine](Which floor should I go to?)[BLOCK]") ]])
	
	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Observation Deck\", " .. sDecisionScript .. ", \"ElevatorToObservation\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Domicile Block\", " .. sDecisionScript .. ", \"ElevatorToDomiciles\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Crater Access Basement\", " .. sDecisionScript .. ", \"ElevatorToBasement\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Cancel\",  " .. sDecisionScript .. ", \"NoClose\") ")
	fnCutsceneBlocker()

--Elevator to Observation Deck.
elseif(sObjectName == "ElevatorToObservation") then
	WD_SetProperty("Hide")
	fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutscene([[ AL_BeginTransitionTo("SerenityObservatoryC", "FORCEPOS:19.5x4.0x0") ]])
	fnCutsceneBlocker()
    giForceFacing = gci_Face_South

--Elevator to Domiciles.
elseif(sObjectName == "ElevatorToDomiciles") then
	WD_SetProperty("Hide")
	fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutscene([[ AL_BeginTransitionTo("SerenityObservatoryB", "FORCEPOS:19.5x4.0x0") ]])
	fnCutsceneBlocker()
    giForceFacing = gci_Face_South
	
--Elevator to the basement.
elseif(sObjectName == "ElevatorToBasement") then
	WD_SetProperty("Hide")
	fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutscene([[ AL_BeginTransitionTo("SerenityObservatoryD", "FORCEPOS:15.5x4.0x0") ]])
	fnCutsceneBlocker()
    giForceFacing = gci_Face_South
	
--Cancel Elevator.
elseif(sObjectName == "NoClose") then
	WD_SetProperty("Hide")

-- |[Examinables]|
elseif(sObjectName == "DrinkMachines") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Leader](Fizzy Pop![P] The best way to recharge your power core after a long day![P] It seems both machines here are the pink Lord Unit variety.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "FabrBench") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Leader](A fabrication machine, probably used to construct spare parts.[P] It seems to be in excellent condition.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Supplies") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Leader](Tools, parts, and extra cable.[P] Lots of extra cable.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalA") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Leader](Notice to all Units from Command Unit 300910.[P] All survey assignments are hereby cancelled.[P] All Units are to return to the Observatory immediately unless explicitly ordered otherwise.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalB") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: [VOICE|Leader](Seems Unit 300910 was in an argument with someone on the network...)[B][C]") ]])
	fnCutscene([[ Append("Thought: [VOICE|Leader](300910:: I understand the danger to the Units, but why must the decommission order be permanent?[P] Surely it would make more sense to issue a temporary evacuation until the security situation is resolved!)[B][C]") ]])
	fnCutscene([[ Append("Thought: [VOICE|Leader](10022:: That would create unnecessary complications in the database software.[P] It will be easier to decommission and recommission the facility.)[B][C]") ]])
	fnCutscene([[ Append("Thought: [VOICE|Leader](300910:: Simpler in the software?[P] How is reassigning a dozen units going to be simpler than issuing an evacuation order?[P] This is nonsense, and I won't be complying until I hear directly from Central Administration.)[B][C]") ]])
	fnCutscene([[ Append("Thought: [VOICE|Leader](10022:: Suit yourself, but Central is not going to be pleased with any delays.[P] I'd recommend decommissioning immediately, but you're not going to listen to me.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Oilmaker") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Leader](An oilmaker.[P] What's this?[P] It's flavoured with a bit of vulcanized rubber and metal shavings?[P] Delicious!)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "RVDScreen") then
    
    --Check 300910's position. If she's in the office, she comments.
    local fDollX, fDollY = fnGetEntityPosition("300910")
    if(fDollX < 9.00) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("300910:[VOICE|Doll] Ah, I see you noticed our widescreen RVD.[P] We fabricated it ourselves and have a videograph night every week.[P] You're welcome to attend![P] This week is 'Electric Horror from the Deep'![B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Uh, an electric monster from underwater?[P] Really?[B][C]") ]])
        fnCutscene([[ Append("300910:[VOICE|Doll] Nobody said we watched *good* videographs.") ]])
        fnCutsceneBlocker()
    
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (A big-screen RVD.[P] The last played video was 'Boiling Creature from the Salty Swamp!!!'.[P] Yes, it has three exclamation points.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "WorkTerminal") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Leader](A work terminal, hooked into the Observatory's local network.[P] It contains a list of work assignments and the designations of those set to complete them.)") ]])
	fnCutsceneBlocker()

--Entry door. Christine transforms if the quest is not completed.
elseif(sObjectName == "TFDoor") then

    --Transform Christine if necessary.
	local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    local iCompletedSerenity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
    local sFirstSerenityForm = VM_GetVar("Root/Variables/Chapter5/Scenes/sFirstSerenityForm", "S")
    local iSawSerenityIntro  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawSerenityIntro", "N")
	if(sChristineForm ~= sFirstSerenityForm and iCompletedSerenity == 0.0 and iSawSerenityIntro == 1.0) then
		
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](Better transform to maintain my cover...)") ]])
        fnCutsceneBlocker()
    
        --Flashwhite.
        Cutscene_CreateEvent("Flash Christine White", "Actor")
            ActorEvent_SetProperty("Subject Name", "Christine")
            ActorEvent_SetProperty("Flashwhite Quickly", "Null")
        DL_PopActiveObject()
        fnCutsceneBlocker()

        fnCutsceneWait(75)
        fnCutsceneBlocker()
        if(sFirstSerenityForm == "Golem") then
            fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
        elseif(sFirstSerenityForm == "LatexDrone") then
            fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Latex.lua") ]])
        elseif(sFirstSerenityForm == "Eldritch") then
            fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Eldritch.lua") ]])
        elseif(sFirstSerenityForm == "Electrosprite") then
            fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Electro.lua") ]])
        elseif(sFirstSerenityForm == "SteamDroid") then
            fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_SteamDroid.lua") ]])
        elseif(sFirstSerenityForm == "Doll") then
            fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Doll.lua") ]])
        elseif(sFirstSerenityForm == "Secrebot") then
            fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Secrebot.lua") ]])
        end
        fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
        fnCutsceneBlocker()
    end


-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
