-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ====================================== Introduction ====================================== ]|
if(sObjectName == "CutsceneTrigger") then
	
	--Don't play this cutscene if we've already seen it.
	local iSawSerenityIntro = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawSerenityIntro", "N")
	if(iSawSerenityIntro == 1.0) then return end
	
	--Variables.
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
        
    --Achievement.
    AM_SetPropertyJournal("Unlock Achievement", "MeetEileen")
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSawSerenityIntro", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/sFirstSerenityForm", "S", sChristineForm)
    
    --Allow 300910's topics.
	WD_SetProperty("Unlock Topic", "Sensors", 1)
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
	fnCutscene([[ Append("Command Unit:[E|Neutral] Oh, visitors?[P] I'll be right there!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneMove("Christine", 19.25, 10.50)
    fnCutsceneFace("Christine", -1, 0)
	fnCutsceneMove("Tiffany", 19.25, 11.50)
    fnCutsceneFace("Tiffany", -1, 0)
	fnCutsceneMove("300910", 12.25, 11.50)
	fnCutsceneMove("300910", 16.25, 11.50)
    fnCutsceneFace("300910", 1, 0)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	-- |[Golem]|
	--If Christine is a golem:
	if(sChristineForm == "Golem") then
	
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] Unit 771852![P] I'm glad you decided to assist![P] Honestly, when I saw your profile, I thought it might be a mistake, but here you are![B][C]") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] And -[P] do my optical receptors deceive me, or is that Unit 2855?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Angry] !!![B][C]") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] Everyone![P] Unit 2855 is here!") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
	
	-- |[Doll]|
	--If Christine is a doll:
	elseif(sChristineForm == "Doll") then
	
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] Unit 771852![P] Your -[P] your profile said you were a repair unit?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] I wear many hats.[P] Command Units need to be capable of repairs, of course.[B][C]") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] Well yes but -[P] nevermind.[B][C]") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] And -[P] do my optical receptors deceive me, or is that Unit 2855?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Angry] !!![B][C]") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] Everyone![P] Unit 2855 is here!") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()

	-- |[Latex Drone]|
	elseif(sChristineForm == "LatexDrone") then
	
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] Are you -[P] Unit 771852?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Hello.[P] How may I help you?[B][C]") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] I -[P] Your profile said you were a Lord Unit.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Perhaps there was a misprint.[P] No matter, my faculties are intact and I can assist you to the best of my ability.[B][C]") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] Well, that's a relief.[P] I thought we were in real trouble for a moment.[B][C]") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] And -[P] do my optical receptors deceive me, or is that Unit 2855?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Angry] !!![B][C]") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] Everyone![P] Unit 2855 is here!") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
	
	-- |[Eldritch Dreamer]|
    elseif(sChristineForm == "Eldritch") then
	
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] Are you -[P] what exactly are you?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] We don't really have a name...[P] Dreamers, perhaps?[B][C]") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] And you're Unit 771852?[P] Because your auth-chip is transmitting that signal.[B][C]") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] (Oh my goodness, where is the auth chip?[P] Maybe it's best if I don't ask...)[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] That's me![P] Pleased to meet you.[B][C]") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] Your profile stated you were a Lord Unit.[P] Is...[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] She was involved in an incident.[P] I assure you, she is perfectly capable of meeting your expectations,[EMOTION|Tiffany|Smug] so long as they are adjusted downwards.[B][C]") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] An incident?[P] Well, if Unit 2855 says so, then I'll believe it![P] Greetings, 2855![B][C]") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] Your auth-chip isn't transmitting for some reason, but I'd recognize that face anywhere![B][C]") ]])
		fnCutscene([[ Append("55:[E|Angry] *I've been identified!*[B][C]") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] Everyone![P] Look![P] Unit 2855 is here!") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
    
    -- |[Electrosprite]|
    elseif(sChristineForm == "Electrosprite") then
	
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] Uhhhh...[P] Hey, has anyone seen a voidborne species like this before?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] We're not voidborne, Command Unit.[P] We're actually a partirhuman species that lives inside electrical neuro-webs.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Or rather, others like me are.[P] I'm a Lord Unit.[B][C]") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] That's what your profile said you were.[P] Unit 771852, right?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] That's me![P] How did you know?[B][C]") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] The door recorded your authenticator chip's signature.[P] Where -[P] where is it stored on your person?[B][C]") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] In fact, forget I asked, I don't want to know.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Despite Unit 771852's physical appearance,[P][EMOTION|Tiffany|Smug] she is perfectly capable of absorbing impacts if that is what you require.[B][C]") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] Hey, if Unit 2855 says you're all right, that's good enough for us.[B][C]") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] Been a while since I saw you last.[P] How have you been?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Angry] *I've been identified!*[B][C]") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] Unit 2855 is here, everyone!") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
    
    -- |[Secrebot]|
    elseif(sChristineForm == "Secrebot") then
	
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] I'm quite impressed a secrebot made it here, considering the tectonic issues.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Hello, command unit.[P] There were plenty of flat surfaces to use, but I appreciate your concern.[B][C]") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] You are...[P] Unit 771852?[P] I thought you were a lord unit![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] It's complicated, but yes, that's me.[P] I am fully programmed for repair, service, analysis, and more![B][C]") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] You must be one of those second-wave secrebots.[P] I'm quite impressed.[P] High-quality vocal systems, variety of skills, and totally synthetic?[B][C]") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] I suppose it's no surprise you have one for testing, Unit 2855.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Angry] *I've been identified!*[B][C]") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] Unit 2855 is here, everyone!") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
    
    -- |[Steam Droid]|
	elseif(sChristineForm == "SteamDroid") then
	
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] Well that wasn't what I was expecting.[P] You're Unit 771852, right?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] That is correct.[B][C]") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] Huh.[P] I didn't think any un-upgraded Steam Droids were still in operation.[P] How have you stayed...[P] working?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] I'm a more recent conversion.[P] I can also return to my Lord Unit state if needed.[B][C]") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] Hmm...[P] Beggars can't be choosers, I suppose.[B][C]") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] Unit 2855 -[P] an unexpected surprise![P] Could you perhaps vouch for her?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Angry] !!![B][C]") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] It's nice to be working with you again![P] Everyone![P] Unit 2855 is here!") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
    
    -- |[Darkmatter]|
	elseif(sChristineForm == "Darkmatter") then
	
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] (Okay, just gotta use my darkmatter powers to pretend to be a lord golem.)[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] (Let's not make mention of the fact that I'm a darkmatter, because I'd rather not cause a time paradox.)[B][C]") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] Unit 771852![P] I'm glad you decided to assist![P] Honestly, when I saw your profile, I thought it might be a mistake, but here you are![B][C]") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] And -[P] do my optical receptors deceive me, or is that Unit 2855?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Angry] !!![B][C]") ]])
		fnCutscene([[ Append("Command Unit:[E|Neutral] Everyone![P] Unit 2855 is here!") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
	
	end

	--Movement.
	fnCutsceneMove("300910", 18.25, 11.50)
	fnCutsceneFace("300910", 1, 0)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--55 knocks 300910 back!
	fnCutsceneFace("GolemAA", 1, 0)
	fnCutsceneFace("GolemAB", 1, 0)
	fnCutsceneMove("Tiffany", 18.75, 11.50)
	fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Music", "Null") ]])
	fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
	fnCutsceneMoveFace("300910", 17.25, 11.50, 1, 0, 2.00)
	fnCutsceneBlocker()
    
    --55 draws her weapon.
	fnCutsceneWait(15)
	fnCutsceneBlocker()
    fnCutsceneSetFrame("Tiffany", "Draw0")
	fnCutsceneWait(10)
	fnCutsceneBlocker()
    fnCutsceneSetFrame("Tiffany", "Draw1")
	fnCutsceneWait(10)
	fnCutsceneBlocker()
    fnCutsceneSetFrame("Tiffany", "Draw2")
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	fnCutsceneWait(35)
	fnCutsceneBlocker()

	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
	fnCutscene([[ Append("55:[E|Angry] That's close enough![B][C]") ]])
	fnCutscene([[ Append("Command Unit:[E|Neutral] Oof![P] What are you doing?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Scared] Oh my goodness![B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Scared] 55![P] We are guests here, and it is impolite to point a weapon at one's host![B][C]") ]])
	fnCutscene([[ Append("55:[E|Angry] It's a trap, 771852.[P] Get your weapon out.[B][C]") ]])
	fnCutscene([[ Append("Command Unit:[E|Neutral] W-[P]wait![P] This isn't a trap![P] Unit 2855, do you not recognize me?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Angry] ...[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] No.[B][C]") ]])
	fnCutscene([[ Append("Command Unit:[E|Neutral] Unit 300910, remember?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Sad] Put the pulse diffractor away, 55.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Upset] How do you know me?[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] We're good friends![P] You've aided us before.[P] Don't you remember the dissociation incident?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Sad] Unit 300910, she -[P] doesn't remember you.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Sad] She wiped her memory drives recently.[P] For security reasons.[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] You did?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Sad] She's very defensive about it.[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] Well, if there's anything I can do to help, you just let me know.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Of course.[P] And 55 will gladly put her weapon away.[P] Right 55?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Upset] [P][P][EMOTION|Tiffany|Neutral]Fine.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(45)
	fnCutsceneBlocker()
    
    --55 holsters hear weapon.
	fnCutsceneWait(15)
	fnCutsceneBlocker()
    fnCutsceneSetFrame("Tiffany", "Draw1")
	fnCutsceneWait(10)
	fnCutsceneBlocker()
    fnCutsceneSetFrame("Tiffany", "Draw0")
	fnCutsceneWait(40)
	fnCutsceneBlocker()
    fnCutsceneSetFrame("Tiffany", "Null")
	fnCutsceneWait(10)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
    
    --Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
	fnCutscene([[ Append("300910:[E|Neutral] Ah, good.[P] Sorry for the misunderstanding.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] And 771852 - [B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] Please call me Christine.[P] It's my secondary designation.") ]])
	fnCutsceneBlocker()
    
    --Resume with new music.
	fnCutscene([[ AL_SetProperty("Music", "SerenityObservatory") ]])
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Smirk") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
	fnCutscene([[ Append("300910:[E|Neutral] A friend of 2855's is a friend of ours![B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Maybe you can answer a few questions, then.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] What was the dissociation incident you mentioned?[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] A few months ago, we received a bad batch of adhesive from Regulus City.[P] A lot of our equipment was damaged and rendered inoperable.[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] When Central Administration found out, they demanded the immediate, permanent closure of the observatory.[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] But you overrode their demands.[P] After all, you're a Prime Command Unit![B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] It's because of you we're still operating and gathering astronomical data.[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] We're in a similar bind again, and again you've arrived to help us out of it.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Why would damaged equipment call for a full closure?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Hmmm...[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] I'm not sure.[P] I'm not privy to the decisions of Central Administration.[P] But they were most adamant about it.[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] And that brings me to the current problem.[P] Please, follow me to my office.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Movement.
	fnCutsceneFace("GolemAA", 0, 1)
	fnCutsceneFace("GolemAB", 0, -1)
	fnCutsceneMove("300910",    18.25, 13.50)
	fnCutsceneBlocker()
	fnCutsceneMove("300910",    18.25, 14.50)
	fnCutsceneMove("300910",     6.25, 14.50)
	fnCutsceneFace("300910", 0, -1)
	fnCutsceneMove("Christine", 18.25, 12.50)
	fnCutsceneMove("Christine", 18.25, 14.50)
	fnCutsceneMove("Christine",  8.25, 14.50)
	fnCutsceneMove("Tiffany",        18.25, 12.50)
	fnCutsceneMove("Tiffany",        18.25, 14.50)
	fnCutsceneMove("Tiffany",         7.25, 14.50)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Open Door", "OfficeDoor") ]])
	fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneMove("300910",    6.25, 11.50)
	fnCutsceneMove("300910",    8.25, 11.50)
	fnCutsceneMove("300910",    8.25,  9.70)
	fnCutsceneMove("300910",    7.25,  9.70)
	fnCutsceneFace("300910", 0, 1)
	fnCutsceneTeleport("300910",7.15,  9.70)
	fnCutsceneTeleport("300910",7.05,  9.70)
	fnCutsceneTeleport("300910",6.95,  9.70)
	fnCutsceneTeleport("300910",6.85,  9.70)
	fnCutsceneTeleport("300910",6.75,  9.70)
	fnCutsceneTeleport("300910",6.65,  9.70)
	fnCutsceneTeleport("300910",6.55,  9.70)
	fnCutsceneTeleport("300910",6.45,  9.70)
	fnCutsceneTeleport("300910",6.35,  9.70)
	fnCutsceneTeleport("300910",6.25,  9.70)
	fnCutsceneMove("Christine", 6.25, 14.50)
	fnCutsceneMove("Christine", 6.25, 11.50)
	fnCutsceneMove("Christine", 5.75, 11.50)
	fnCutsceneFace("Christine", 0, -1)
	fnCutsceneMove("Tiffany",        6.25, 14.50)
	fnCutsceneMove("Tiffany",        6.25, 11.50)
	fnCutsceneMove("Tiffany",        6.75, 11.50)
	fnCutsceneFace("Tiffany", 0, -1)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
	fnCutscene([[ Append("300910:[E|Neutral] May I get you anything?[P] Oil, Fizzy Pop!, perhaps a synaptic spike?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Upset] Just get to the point.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Offended] 55, please don't be rude.[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] She hasn't changed a bit![P] Ha ha![B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] Oh, sorry.[P] If you have any questions about your past, I can try to help.[P] But, I should tell you about our predicament first.[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] I'm sure you encountered the Darkmatters on the way here.[P] They've been swarming over this area.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] We had a few run-ins, yes.[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] Well, my official reason for calling you here is to recalibrate some sensors down in the crater.[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] The Darkmatters have started assaulting my survey teams.[P] I've recalled my crews to the observatory for the time being, and I'm not sending any out.[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] Naturally, the split second the Darkmatters got ornery, Central Administration began using it as an excuse to shut the observatory down.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Offended] Again?[P] Suspicious...[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] Precisely.[P] I imagine if someone dropped a pipe wrench they'd call for us to be shut down.[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] Unfortunately, my staff can't deal with this on their own.[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] We don't have a dedicated security detail, and -[P] well, I honestly don't want to risk any of them.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] What do you mean by that?[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] Look, these Slave Units are my -[P] they're like family to me.[P] I don't want any of them getting damaged.[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] I know it's selfish to ask but -[P] can you help us?[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] The sensors in the crater shelf don't matter.[P] What I really need is someone to find out what has the darkmatters so angry, and deal with that.[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] I know it entails some risk, so I am prepared to provide work credits...[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] We'll help, of course![B][C]") ]])
	fnCutscene([[ Append("55:[E|Upset] Really?[P] Will we?[P] Why should we put ourselves at risk?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Because these units need our help, 55.[P] That's reason enough.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] ...[P] 300910, why did I help you last time?[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] I'm not sure, but I assume you had your reasons.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] Of course, she helped you because she wanted to do the right thing.[P] Right?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Down] ...[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral][EMOTION|Tiffany|Neutral] She's never been particularly talkative, but she's a good friend when you need one.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] So, what do you know about the Darkmatters?[P] When did they start attacking your staff?[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] Let's see...[P] Here's the logs.[P] The first assault report is highlighted there.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] This -[P] Christine.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] *Yes?*[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] *The first assault is a week before the Cryogenics Incident.[P] There may be a connection.*[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] *Hmmm...*[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Have there been any other units going through the area recently?[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] Well, yes.[P] Have you not heard about the discoveries in the crater?[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] About a year ago, a survey team discovered a rare and very valuable ore down in the crater.[P] So Central sent a mining team in.[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] There's an entire facility down there.[P] With their own local power supply and everything.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Offended] Is it still in operation?[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] ...[P] I don't know.[P] They don't send any communications to us, they communicate directly with Central.[P] In fact I'm not supposed to know they exist, but I have Command Unit privileges.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Well then, I know where we'll be going.[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] Now I *do* still need you to recalibrate those sensors.[P] I'll even give you a bonus for it.[P] You'll find them on your way into the crater.[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] So, good luck.[P] And thank you for doing this.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Unit 300910?[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] Yes?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Smirk] It was -[P] nice.[P] Seeing you again.[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] And it's always a pleasure seeing you![B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] The transit elevator in the basement will take you about halfway down the crater rim.[P] You'll have to climb down the cliffs on your own from there.[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] Be careful near the edge, obviously.[P] And good luck, 771852.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] We'll have this sorted out before you know it.[P] Come on, 55!") ]])
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneMove("Christine", 6.25, 11.50)
	fnCutsceneMove("Christine", 6.25, 14.50)
	fnCutsceneFace("Christine", 0, -1)
	fnCutsceneMove("Tiffany", 6.25, 11.50)
	fnCutsceneMove("Tiffany", 6.25, 14.50)
	fnCutsceneMove("Tiffany", 7.25, 14.50)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Close Door", "OfficeDoor") ]])
	fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneFace("Christine", 1, 0)
	fnCutsceneFace("Tiffany", -1, 0)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Happy] Well I am just so proud of you![B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] ..?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Happy] You told 300910 that it was nice to see her again![P] That's a very sweet thing to say![B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] I forced myself to say it.[P] She will make a good ally for our purposes.[P] She dislikes the Administration and her facility is well situated to resist armed assault.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] It's okay to admit you did it to be nice.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] I didn't.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] Then.[P][EMOTION|Christine|Neutral] Lie.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Lie until it becomes true.[P] If you can't change for the better, trick yourself.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Not a traditional moral lesson, and certainly not the one I expected from you.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Sad] I -[P] I know what it's like having multiple lives.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Now, shall we?") ]])
	fnCutsceneBlocker()
	
	--Fold the party.
	fnCutsceneMove("Christine", 7.25, 14.50)
	fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
    
    --Topics
    WD_SetProperty("Unlock Topic", "SerenityCrater", 1)

-- |[ =================================== Darkmatter Post-TF =================================== ]|
--Cutscene that triggers after the Darkmatter transformation.
elseif(sObjectName == "DarkmatterTrigger") then

	--Repeat check.
	local iIsRelivingScene      = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsRelivingScene", "N")
	local iSawUndergroundSceneC = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawUndergroundSceneC", "N")
	local iCompletedSerenity    = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
	if(iSawUndergroundSceneC == 0.0 or iCompletedSerenity == 1.0 and iIsRelivingScene == 0.0) then return end
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N", 1.0)
    
    --Add work credits.
    if(iIsRelivingScene == 0.0) then
        local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
        VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + 300)
    end
	
	--Music.
	AL_SetProperty("Music", "Null")
	
	--Spawn 55.
	fnSpecialCharacter("Tiffany", 15, 10, gci_Face_South, false, nil)
	
	--Spawn some other golems.
    local sDialoguePath = fnResolvePath() .. "Dialogue.lua"
	TA_Create("ConcernedGolemA")
		TA_SetProperty("Position", 15, 7)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Clipping Flag", true)
		TA_SetProperty("Activation Script", sDialoguePath)
		fnSetCharacterGraphics("Root/Images/Sprites/GolemSlave/", false)
	DL_PopActiveObject()
	TA_Create("ConcernedGolemB")
		TA_SetProperty("Position", 19, 12)
		TA_SetProperty("Facing", gci_Face_West)
		TA_SetProperty("Clipping Flag", true)
		TA_SetProperty("Activation Script", sDialoguePath)
		fnSetCharacterGraphics("Root/Images/Sprites/GolemSlave/", false)
	DL_PopActiveObject()
	TA_Create("ConcernedGolemC")
		TA_SetProperty("Position", 15, 14)
		TA_SetProperty("Facing", gci_Face_North)
		TA_SetProperty("Clipping Flag", true)
		TA_SetProperty("Activation Script", sDialoguePath)
		fnSetCharacterGraphics("Root/Images/Sprites/GolemSlave/", false)
	DL_PopActiveObject()
	TA_Create("ConcernedGolemD")
		TA_SetProperty("Position", 16, 14)
		TA_SetProperty("Facing", gci_Face_North)
		TA_SetProperty("Clipping Flag", true)
		TA_SetProperty("Activation Script", sDialoguePath)
		fnSetCharacterGraphics("Root/Images/Sprites/GolemSlave/", false)
	DL_PopActiveObject()
	TA_Create("Darkmatter")
		TA_SetProperty("Position", -100, -100)
		TA_SetProperty("Facing", gci_Face_North)
		TA_SetProperty("Clipping Flag", true)
		TA_SetProperty("Activation Script", sDialoguePath)
		fnSetCharacterGraphics("Root/Images/Sprites/DarkmatterGirl/", true)
	DL_PopActiveObject()

	--Black the screen out, spawn entities as needed.
	fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	
	--Teleport characters to their positions if they didn't spawn there.
	fnCutsceneTeleport("Christine", 14.25, 10.50)
	fnCutsceneTeleport("300910", 13.25, 10.50)
	fnCutsceneFace("Christine", 0, 1)
	fnCutsceneFace("300910", 1, 0)
	fnCutsceneFace("Tiffany", -1, 0)
	fnCutsceneSetFrame("Christine", "Wounded")
	fnCutsceneBlocker()
	
	--Fade us in.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(70)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
	fnCutscene([[ Append("300910:[E|Neutral] Is she...[P] dead?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] ...[P] I don't know.[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] Is she even still 771852?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Down] ...[P] I don't know.[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] What happened?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Angry] I DON'T KNOW![B][C]") ]])
	fnCutscene([[ Append("55:[E|Down] ...[P] I'm sorry.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] I did not see what happened after the green flash of light.[P] But I am certain this is her.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] She was clutching the runestone, and the other Darkmatters seemed most concerned about her.[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] Please wake up, Christine.[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] The observatory's future wasn't worth losing yours...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(70)
	fnCutsceneBlocker()
	
	--Move Christine slightly.
	fnCutsceneMove("Christine", 14.25, 11.00, 0.20)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Christine crouches.
	fnCutsceneSetFrame("Christine", "Crouch")
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ AL_SetProperty("Music", "SerenityObservatory") ]])
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Sad") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
	fnCutscene([[ Append("300910:[E|Neutral] She moved![P] Christine!?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Sad] Unghhh...[P] When am I?[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] She's alive![B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] ...[P] Within parameters, then.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Unit 771852, report mission results.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Sad] Mission successful...[P] Reporting for new function assignment...[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] 55, give her a moment, will you?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Upset] Your current function assignment is to recover.[P] Take your time.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Sad] Affirmative...[P] ungh...[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] 55, is that you?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Yes, it's me.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Where's Sophie?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Unit 499323 will be located in the repair facility of Sector 96.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] So this -[P] everything is as it should be.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(65)
	fnCutsceneBlocker()
	
	--Teleport the Darkmatter in.
	local ciListX = {0, 1, 1,  1,  0, -1, -1, -1}
	local ciListY = {1, 1, 0, -1, -1, -1,  0,  1}
	fnCutsceneTeleport("Darkmatter", 14.25, 9.50)
	fnCutsceneWait(3)
	fnCutsceneFace("300910", 0, -1)
	fnCutsceneFace("Tiffany", 0, -1)
	fnCutsceneBlocker()
	for i = 1, 2, 1 do
		for p = 1, 8, 1 do
			fnCutsceneFace("Darkmatter", ciListX[p], ciListY[p])
			fnCutsceneWait(3)
			fnCutsceneBlocker()
		end
	end
	for i = 1, 2, 1 do
		for p = 1, 8, 1 do
			fnCutsceneFace("Darkmatter", ciListX[p], ciListY[p])
			fnCutsceneWait(5)
			fnCutsceneBlocker()
		end
	end
	for i = 1, 2, 1 do
		for p = 1, 8, 1 do
			fnCutsceneFace("Darkmatter", ciListX[p], ciListY[p])
			fnCutsceneWait(12)
			fnCutsceneBlocker()
		end
	end
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneSetFrame("Christine", "Null")
	fnCutsceneFace("Darkmatter", 0, 1)
	fnCutsceneFace("Christine", 0, -1)
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Doll", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "DarkmatterGirl", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Everyone...[P] this Darkmatter is my friend...[P] Please don't be alarmed...[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] 300910, this is the Darkmatter I told you about.[P] She was the one who led us to the artifact site.[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] How can you tell?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Their stellar fold patterns are distinct.[P] This is the one I saw earlier.[B][C]") ]])
	fnCutscene([[ Append("Darkmatter:[E|Neutral] Are you well, my friend?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Happy] I am -[P] what did I see?[P] What was that?[P] Was that you I chased across the arc of the sky?[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] ...[P] Excuse me?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] She is likely speaking to it.[P] They can converse, but we cannot hear them respond.[B][C]") ]])
	fnCutscene([[ Append("Darkmatter:[E|Neutral] No, young one.[P] What you saw was perhaps a glimpse of the Creator's history.[P] Her will is woven into the fabric of reality, and you rode along with it.[B][C]") ]])
	fnCutscene([[ Append("Darkmatter:[E|Neutral] Of course, there are many histories.[P] The past before sentience is fuzzy, and changes as the future needs.[P] Time is a sphere, after all.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Happy] But I saw...[P] everything...[P] I *was* everything.[B][C]") ]])
	fnCutscene([[ Append("Darkmatter:[E|Neutral] That is true.[P] And I am sorry that you must surrender that to return to those you love.[P] Perhaps someday you will play in the void with us.[B][C]") ]])
	fnCutscene([[ Append("Darkmatter:[E|Neutral] But you have shut the gap.[P] The rift is sealed.[P] Thank you.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] 55, what did you see?[P] Did I close the rift?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] I had the PDU download the recordings from the equipment.[P] The artifact seems to be inert now, but I will not even attempt to guess how or why.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Probably best if you don't.[P] I don't think I could explain it to you.[B][C]") ]])
	fnCutscene([[ Append("Darkmatter:[E|Neutral] We Darkmatters will attend to the rifts and creatures that remain.[P] Please tell your hardmatter friends that the ridge remains unsafe.[B][C]") ]])
	fnCutscene([[ Append("Darkmatter:[E|Neutral] We will let them know when we have cleared it for them.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] My friend said that the ridge isn't safe, and that they're going to work on clearing it of the...[P] things...[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] Can we help?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Offended] No.[P] No.[P] It's best if hardmatter is not exposed to them.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Your materialistic minds are not yet prepared for their true nature.[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] I suppose that will have to do.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Unit 771852...[P] What did you see?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Sad] Walls made of flesh, dead and bleeding.[P] Screams that died on my lips.[P] Hands.[P] There were hands...[P] They...[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Don't strain yourself.[P] We can discuss this later.[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] You mentioned that she can transform herself.[P] Is that still the case?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Happy] Yes, yes.[P] I just need to rest and adjust myself to this new body.[P] I am -[P] melted starlight...[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Are you prepared, Christine?[P] We still have work ahead of us.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] I think so.[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] Would you mind if we...[P] ran some tests?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Laugh] Ha ha ha![B][C]") ]])
	fnCutscene([[ Append("Darkmatter:[E|Neutral] Their understanding of physics is insufficient.[P] Their machinery will not avail them yet.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] I'm sorry, but it will be a waste of time.[P] Regulus Science has a few centuries of physics research to go before I will be of any service.[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] But...[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Oh, but I did see the universe being born.[P] I must remember to upload what I saw.[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] WHAT!?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] You just need to open your mind, hardmatter.[P] Nothing is something.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] That's enough toying with her, Christine.[B][C]") ]])
	fnCutscene([[ Append("Darkmatter:[E|Neutral] We will not forget what you have done to aid us.[P] Wherever you go, the Darkmatters will be your friends.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Thank you.[B][C]") ]])
	fnCutscene([[ Append("300910:[E|Neutral] Unit 771852, we are in your debt.[P] You're always welcome here.[P] And -[P] we'll keep your secret safe.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Laugh] Thanks.[P] I'd rather not have to fill out the administrative reports for how I pulled this mission off.[P] Hee hee![B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Oh...[P] I should go see Sophie...[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Good.[P] Let's get going.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
    
    --Handler for reliving.
    if(iIsRelivingScene == 1.0) then
    
        --Wait a bit.
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        
        --Return to the last save point and execute the post-script..
        fnCutscene([[ AL_BeginTransitionTo("LASTSAVE", gsStandardReliveEnd) ]])
        fnCutsceneBlocker()
        return
    end
	
	--Re-add 55. Fold the party.
	giFollowersTotal = 1
	gsaFollowerNames = {"Tiffany"}
	giaFollowerIDs = {0}
	EM_PushEntity("Tiffany")
		local iCharacterID = RE_GetID()
	DL_PopActiveObject()
	giaFollowerIDs = {iCharacterID}
	AL_SetProperty("Follow Actor ID", iCharacterID)
    AdvCombat_SetProperty("Party Slot", 1, "Tiffany")
	
	--Move 55 and Christine onto the same tile.
	fnCutsceneMove("Christine", 14.25, 10.50)
	fnCutsceneMove("Tiffany", 14.25, 10.50)
	fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()

end
