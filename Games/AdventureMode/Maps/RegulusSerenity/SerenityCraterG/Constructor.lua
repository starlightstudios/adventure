-- |[Constructor]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveFolderName(fnResolvePath())

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "Vivify")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation(sLevelName)
	
	-- |[Spawn NPCS Here]|
	--NPCs set to spawn here will be available for post-exec cutscenes.
	
	-- |[Lights]|
	AL_SetProperty("Activate Lights")
	AL_SetProperty("Set Player Light No Drain", true)

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[Spawn NPCs Here]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    local sBasePath = fnResolvePath()
    local iCompletedSerenity    = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
	local iSawUndergroundSceneB = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawUndergroundSceneB", "N")
	if(iSawUndergroundSceneB == 0.0) then
		TA_Create("Darkmatter")
			TA_SetProperty("Position", 15, 4)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Clipping Flag", true)
			fnSetCharacterGraphics("Root/Images/Sprites/DarkmatterGirl/", true)
            TA_SetProperty("Activation Script", sBasePath .. "Dialogue.lua")
		DL_PopActiveObject()
    elseif(iSawUndergroundSceneB == 1.0 and iCompletedSerenity == 0.0) then
		TA_Create("Darkmatter")
			TA_SetProperty("Position", 12, 10)
			TA_SetProperty("Facing", gci_Face_SouthEast)
			TA_SetProperty("Clipping Flag", true)
			fnSetCharacterGraphics("Root/Images/Sprites/DarkmatterGirl/", true)
            TA_SetProperty("Activation Script", sBasePath .. "Dialogue.lua")
		DL_PopActiveObject()
	end

end
