-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
--Door Exit.
if(sObjectName == "ToSerenityF") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	fnCutscene([[ AL_BeginTransitionTo("SerenityCraterF", "FORCEPOS:37.5x46.0x0") ]])
	fnCutsceneBlocker()

-- |[Examinables]|
elseif(sObjectName == "Terminal") then

    --Variables.
    local iCompletedSerenity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
    
    --Has not completed Serenity Crater. Option for boss battle.
    if(iCompletedSerenity == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("55:[VOICE|Tiffany] Are you prepared, Christine?[P] Shall I activate the positron bombardment?[BLOCK]") ]])
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"No\") ")
        fnCutsceneBlocker()
        
       
    --Has completed Serenity.
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The terminal indicates that whatever the statue was, it's inactive.[P] For now...)") ]])
        fnCutsceneBlocker()
       
    end
    
elseif(sObjectName == "Serenity") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (An object not of this world.[P] It resembles a statue, but is anything but...)") ]])
    fnCutsceneBlocker()

-- |[Decisions]|
elseif(sObjectName == "Yes") then
	WD_SetProperty("Hide")
    
    --Movement.
    fnCutsceneMove("Christine", 15.75, 16.50)
    fnCutsceneFace("Christine", 0, -1)
    fnCutsceneMove("Tiffany", 20.25, 15.50)
    fnCutsceneFace("Tiffany", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] Positron accelerators are powered.[P] Beginning bombardment cycle.[P] Stand by...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I feel...[P] nauseous...[P] ugh...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] It's inside my brain...[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Records mismatch.[P] The reaction is not as expected.[P] No rift is opening.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Because...[P] Something's different.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 55...[P] Modify the arrays.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] To?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 105.66 MeV.[P] Set the array to oscillate slowly between present energy and new setup over 15 seconds on my mark.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Mark.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Set.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] .[P].[P].[P] There is definitely a reaction.[P] How did you know about this?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 55...[P] It's stirring...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Come here![P] Quickly![P] 55, HELP!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Screen goes green.
    fnCutsceneWait(25)
    fnCutscene([[ AL_SetProperty("Activate Fade", 25, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0.5, 0, 1) ]])
    fnCutsceneBlocker()
        
    --Battle.
    fnCutscene([[ AdvCombat_SetProperty("Next Combat Music", "RottenTheme", 0.0000) ]])
    fnCutscene([[ AdvCombat_SetProperty("World Pulse", true) ]])
    fnCutscene([[ AdvCombat_SetProperty("Reinitialize") ]])
    fnCutscene([[ AdvCombat_SetProperty("Activate") ]])
    fnCutscene([[ AdvCombat_SetProperty("Unretreatable", true) ]])
    fnCutscene([[ AdvCombat_SetProperty("Victory Script", gsRoot .. "Maps/RegulusSerenity/SerenityCraterG/Combat_Victory.lua") ]])
    fnCutscene([[ AdvCombat_SetProperty("Defeat Script", gsStandardGameOver) ]])
    fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 5/000 Scripted Enemies/Boss Serenity.lua") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "No") then
	WD_SetProperty("Hide")

-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
