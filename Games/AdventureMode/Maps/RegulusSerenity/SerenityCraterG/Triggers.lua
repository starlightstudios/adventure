-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Sample object.
if(sObjectName == "CutsceneTrigger") then
	
	--Variables. Don't play this scene twice.
	local iSawUndergroundSceneB = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawUndergroundSceneB", "N")
	if(iSawUndergroundSceneB == 1.0) then return end
	
	--Other variables.
	local iLRTBossResult = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTBossResult", "N") 
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSawUndergroundSceneB", "N", 1.0)
	
	--Save Christine's form.
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	VM_SetVar("Root/Variables/Chapter5/Scenes/sStartedUndergroundForm", "S", sChristineForm)
	
	--Movement.
	fnCutsceneMove("Christine", 18.25, 5.50)
	fnCutsceneBlocker()
	fnCutsceneMove("Christine", 18.25, 7.50)
	fnCutsceneMove("Christine", 16.25, 9.50)
	fnCutsceneFace("Christine", 0, 1)
	fnCutsceneMove("Tiffany", 18.25, 5.50)
	fnCutsceneMove("Tiffany", 18.25, 7.50)
	fnCutsceneMove("Tiffany", 17.25, 9.50)
	fnCutsceneFace("Tiffany", 0, 1)
	fnCutsceneMove("Darkmatter", 13.25, 5.50)
	fnCutsceneMove("Darkmatter", 13.25, 7.50)
	fnCutsceneMove("Darkmatter", 15.25, 9.50)
	fnCutsceneFace("Darkmatter", 0, 1)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
		
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "DarkmatterGirl", "Neutral") ]])
	fnCutscene([[ Append("Darkmatter:[E|Neutral] Danger...[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] 55, wait.[P] Don't get any closer.[P] The Darkmatter says it's dangerous.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
	if(iLRTBossResult == 0.0) then
		fnCutscene([[ Append("Christine:[E|Neutral] That thing...[P] what is it?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] The artifact we've been reading about.[P] It seems to be glowing.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] It...[P] it's not of this world...[P] I can feel it...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] It doesn't belong here.[P] We need to get rid of it.[B][C]") ]])
		fnCutscene([[ Append("Darkmatter:[E|Neutral] Right.[P] Good.[B][C]") ]])
	else
		fnCutscene([[ Append("Christine:[E|Neutral] It looks like Vivify...[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] I can see the resemblance, but it is not total.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] It...[P] it's not of this world...[P] I can feel it...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] It doesn't belong here.[P] We need to get rid of it.[B][C]") ]])
		fnCutscene([[ Append("Darkmatter:[E|Neutral] Right.[P] Good.[B][C]") ]])
	end
	fnCutscene([[ Append("Christine:[E|Neutral] The golems should have left this thing buried.[P] Darkmatter, what do you need us to do?[B][C]") ]])
	fnCutscene([[ Append("Darkmatter:[E|Neutral] Start.[P] Activate.[P] Do.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] ...[P] What does that mean?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] What did it say?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Start, activate, do.[P] Do you think it wants us to -[P] activate the positron bombardment cycle?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] If the logs are correct, that will open the portal to the other dimension.[B][C]") ]])
	fnCutscene([[ Append("Darkmatter:[E|Neutral] Scary...[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] But...[P] Isn't that exactly what we don't want to do?[B][C]") ]])
	fnCutscene([[ Append("Darkmatter:[E|Neutral] Please...[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] I don't like this.[P] I don't like this one bit.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Prepare yourself.[P] If the creatures we encountered on the crater shelf originated here, then there may be more.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Sad] Can't you just empathize with me for a second here?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Sad] I -[P] this is like Cryogenics...[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] You are experiencing a fear response.[P] Suppress it.[P] Fear will make you make irrational choices and lead to failure.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] I have -[P] said that before.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] You're right.[P] Let's -[P] let's start the bombardment.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] The terminal over there is hooked to the apparatus.") ]])
    fnCutsceneBlocker()
    
    --Fold the party.
	fnCutsceneMove("Tiffany", 16.25, 9.50)
	fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Fold Party") ]])
    fnCutsceneBlocker()
    
    --[=[
	
	--Movement.
	fnCutsceneMove("Christine", 17.25,  9.50)
	fnCutsceneMove("Christine", 17.25, 16.50)
	fnCutsceneMove("Christine", 15.75, 16.50)
	fnCutsceneFace("Christine", 0, -1)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] (I just know I'm going to regret this...)[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] *Smooch*[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] ...[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] !") ]])
	
	--Screen goes black.
	fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	
	--Transition.
	fnCutscene([[ AL_BeginTransitionTo("SerenityCraterH", "FORCEPOS:20.0x50.0x0") ]])
	fnCutsceneBlocker()]=]

end
