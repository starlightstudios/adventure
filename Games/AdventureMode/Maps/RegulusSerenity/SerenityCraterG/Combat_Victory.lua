-- |[Combat Victory]|
--The party won!

--Achievement.
AM_SetPropertyJournal("Unlock Achievement", "FinishSerenity")

-- |[Movement]|
--Wait a bit.
fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0.5, 0, 1, 0, 0, 0, 1) ]])
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutscene([[ AL_BeginTransitionTo("SerenityCraterH", "FORCEPOS:20.0x50.0x0") ]])
fnCutsceneBlocker()
