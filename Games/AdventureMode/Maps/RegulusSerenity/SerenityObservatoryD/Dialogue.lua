-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
    
    -- |[GolemDA]|
    if(sActorName == "GolemDA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] All systems are nominal Eile-[P] Oh![P] I'm sorry, I thought you were Command Unit 300910.") ]])
        fnCutsceneBlocker()
        
    -- |[GolemDB]|
    elseif(sActorName == "GolemDB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] When you're this far from the logistics network, you tend not to throw anything out.[P] We have keyboard interfaces from -[P] woah, twenty years ago?[P] Okay, maybe we should recycle that one...") ]])
        fnCutsceneBlocker()
    
    end
end
