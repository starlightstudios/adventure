-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
--Elevator.
if(sObjectName == "Exit") then

	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (Should I take the elevator down to the crater rim?)[BLOCK]") ]])
	
	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"ElevatorToCraterRim\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Cancel\",  " .. sDecisionScript .. ", \"NoClose\") ")
	fnCutsceneBlocker()
	
--Elevator.
elseif(sObjectName == "Elevator") then

	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Christine](Which floor should I go to?)[BLOCK]") ]])
	
	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Main Floor\", " .. sDecisionScript .. ", \"ElevatorToMainFloor\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Domicile Block\", " .. sDecisionScript .. ", \"ElevatorToDomiciles\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Observation Deck\", " .. sDecisionScript .. ", \"ElevatorToObservation\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Cancel\",  " .. sDecisionScript .. ", \"NoClose\") ")
	fnCutsceneBlocker()

--Elevator to the rim of the crater.
elseif(sObjectName == "ElevatorToCraterRim") then
	WD_SetProperty("Hide")
	fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutscene([[ AL_BeginTransitionTo("SerenityObservatoryE", "FORCEPOS:21.5x8.0x0") ]])
	fnCutsceneBlocker()
    giForceFacing = gci_Face_South

--Elevator to Main Floor.
elseif(sObjectName == "ElevatorToMainFloor") then
	WD_SetProperty("Hide")
	fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutscene([[ AL_BeginTransitionTo("SerenityObservatoryA", "FORCEPOS:20.5x4.0x0") ]])
	fnCutsceneBlocker()
    giForceFacing = gci_Face_South

--Elevator to Domiciles.
elseif(sObjectName == "ElevatorToDomiciles") then
	WD_SetProperty("Hide")
	fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutscene([[ AL_BeginTransitionTo("SerenityObservatoryB", "FORCEPOS:19.5x4.0x0") ]])
	fnCutsceneBlocker()
    giForceFacing = gci_Face_South

--Elevator to Observation Deck.
elseif(sObjectName == "ElevatorToObservation") then
	WD_SetProperty("Hide")
	fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutscene([[ AL_BeginTransitionTo("SerenityObservatoryC", "FORCEPOS:19.5x4.0x0") ]])
	fnCutsceneBlocker()
    giForceFacing = gci_Face_South
	
--Cancel Elevator.
elseif(sObjectName == "NoClose") then
	WD_SetProperty("Hide")

-- |[Examinables]|
elseif(sObjectName == "Scanner") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (A spare volumetric radar unit.[P] It's not currently hooked up to anything.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "DrinkMachine") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (A Fizzy Pop! machine.[P] A note lists it as broken.[P] Well, I'm a repair unit, let's see what's wrong!)[B][C]") ]])
	fnCutscene([[ Append("Thought: (...)[B][C]") ]])
	fnCutscene([[ Append("Thought: (Nothing is wrong at all...[P] Wonder why it's down here, then?)") ]])
	fnCutsceneBlocker()

-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
