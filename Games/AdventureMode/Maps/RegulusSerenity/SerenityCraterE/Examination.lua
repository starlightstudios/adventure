-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
--Ladder Exit.
if(sObjectName == "LadderUp") then
	AudioManager_PlaySound("World|ClimbLadder")
	fnCutscene([[ AL_BeginTransitionTo("SerenityCraterB", "FORCEPOS:29.0x7.0x0") ]])
	fnCutsceneBlocker()
	
--Door Exit.
elseif(sObjectName == "ToSerenityF") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	fnCutscene([[ AL_BeginTransitionTo("SerenityCraterF", "FORCEPOS:10.0x8.0x0") ]])
	fnCutsceneBlocker()

-- |[Examinables]|
-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
