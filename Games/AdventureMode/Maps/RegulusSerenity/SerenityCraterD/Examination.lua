-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
--Ladder Exit.
if(sObjectName == "LadderUp") then
	AudioManager_PlaySound("World|ClimbLadder")
	fnCutscene([[ AL_BeginTransitionTo("SerenityCraterC", "FORCEPOS:78.0x35.0x0") ]])
	fnCutsceneBlocker()
	
--Ladder Exit.
elseif(sObjectName == "LadderDn") then
	AudioManager_PlaySound("World|ClimbLadder")
	fnCutscene([[ AL_BeginTransitionTo("SerenityCraterB", "FORCEPOS:47.0x23.0x0") ]])
	fnCutsceneBlocker()

	--Variables.
	local iOpenedMidwayDoor = VM_GetVar("Root/Variables/Chapter5/Scenes/iOpenedMidwayDoor", "N")
	if(iOpenedMidwayDoor == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iOpenedMidwayDoor", "N", 1.0)
    elseif(iOpenedMidwayDoor == 3.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iOpenedMidwayDoor", "N", 4.0)
	end

-- |[Airlocks]|
elseif(sObjectName == "DoorN") then

	--If Christine is an organic, don't open the door.
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	if(sChristineForm == "Human") then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Leader](This frail, fleshy body probably wouldn't do well in a vacuum...)") ]])
		fnCutsceneBlocker()
        AL_SetProperty("Close Door", "DoorS")
		AL_SetProperty("Close Door", "DoorN")
		
	--Open the door and close the other one.
	else
		AudioManager_PlaySound("World|AutoDoorOpen")
        AL_SetProperty("Close Door", "DoorS")
		AL_SetProperty("Open Door", "DoorN")
	end
	
elseif(sObjectName == "DoorS") then
    
    --Close the northern door in all cases.
	AL_SetProperty("Close Door", "DoorN")
    
    --Cutscene. Plays if it hasn't yet.
    local iSawRecalibrateCutscene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawRecalibrateCutscene", "N")
    if(iSawRecalibrateCutscene == 0.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawRecalibrateCutscene", "N", 1.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("300910:[VOICE|Doll] Ah, I see you've made it to the waystation.[P] Contact me on the intercom, please.") ]])
		fnCutsceneBlocker()
    end

-- |[Examinables]|
elseif(sObjectName == "RecalibrateA" or sObjectName == "RecalibrateB" or sObjectName == "RecalibrateC" or sObjectName == "RecalibrateD" or sObjectName == "RecalibrateE" or sObjectName == "RecalibrateF") then

	--Variables.
	local iHasRecalibrationCode = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasRecalibrationCode", "N")
	local iRecalibrateVar = VM_GetVar("Root/Variables/Chapter5/Scenes/i" .. sObjectName, "N")
	local iRecalibratesDone = VM_GetVar("Root/Variables/Chapter5/Scenes/iRecalibratesDone", "N")
	
	--Doesn't have the code:
	if(iHasRecalibrationCode == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought: (A survey radar.[P] There's an error code on the display, but I don't know what it means.)") ]])
		fnCutsceneBlocker()
	
	--Has the code, hasn't recalibrated it yet.
	elseif(iHasRecalibrationCode == 1.0 and iRecalibrateVar == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought: (This survey radar needs to be recalibrated... done!)[B][C]") ]])
		
		--Set Variables.
		VM_SetVar("Root/Variables/Chapter5/Scenes/i" .. sObjectName, "N", 1.0)
		VM_SetVar("Root/Variables/Chapter5/Scenes/iRecalibratesDone", "N", iRecalibratesDone + 1.0)
		
		--Ending case.
		if(iRecalibratesDone + 1 == 6) then
			fnCutscene([[ Append("Thought: (That was the last one.[P] I should let Unit 300910 know as soon as I can.)") ]])
		
		--Other cases:
		else
			local sString = "WD_SetProperty(\"Append\", \"Thought: (Okay, " .. math.floor(6 - iRecalibratesDone - 1) .. " left to go!)\")"
			fnCutscene(sString)
		end
	
	--Has the code, already recalibrated.
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought: (This radar's error is cleared and it's sending data to the server.)") ]])
		
	end

elseif(sObjectName == "TerminalA") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (A data server, responsible for survey information.[P] It's hooked to several radar units in the area via the red power cable.)") ]])

elseif(sObjectName == "TerminalB") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (Looks like the unit assigned to this defragmentation pod left for the observatory some time ago.)") ]])

elseif(sObjectName == "RVDScreen") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (There was a videograph playing, but it reached its end and a selection menu has popped up.)") ]])

elseif(sObjectName == "Intercom") then

	--Variables.
	local iHasRecalibrationCode = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasRecalibrationCode", "N")
	local iRecalibratesDone     = VM_GetVar("Root/Variables/Chapter5/Scenes/iRecalibratesDone", "N")
	
	--Doesn't have the code yet, start the quest.
	if(iHasRecalibrationCode == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iHasRecalibrationCode", "N", 1.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("300910:[VOICE|Doll] 771852?[P] I take it you're at the survey checkpoint safe and sound?[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Affirmative.[P] What was it you needed us to do here?[B][C]") ]])
		fnCutscene([[ Append("300910:[VOICE|Doll] I don't know why, but the volumetric scanners in that area are all on the fritz.[P] I'd guess they need to be recalibrated.[B][C]") ]])
		fnCutscene([[ Append("300910:[VOICE|Doll] The recalibration code is written on the intercom there.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Yes I see it.[P] What do I do now?[B][C]") ]])
		fnCutscene([[ Append("300910:[VOICE|Doll] You'll have to punch it into the survey scanners.[P] Just follow the power cables.[B][C]") ]])
		fnCutscene([[ Append("300910:[VOICE|Doll] There's six of them.[P] Recalibrate all of them, please.[B][C]") ]])
		fnCutscene([[ Append("300910:[VOICE|Doll] I have some spare work credits in my budget.[P] I'll transfer them to you.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Oh that's all right, really.[B][C]") ]])
		fnCutscene([[ Append("300910:[VOICE|Doll] I insist.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Well, okay then.[P] We'll get this fixed straight away.") ]])
	
	--Has the code but hasn't finished the quest.
	elseif(iHasRecalibrationCode == 1.0 and iRecalibratesDone < 6.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("300910:[VOICE|Doll] Oh, yes 771852?[P] My display indicates the scanners still aren't working.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] We're still working on it.[P] Just checking in so you know we're all right.[B][C]") ]])
		fnCutscene([[ Append("300910:[VOICE|Doll] Be very careful, especially near the cliff edges there.") ]])
	
	--Has the code and hasn't finished the quest.
	elseif(iHasRecalibrationCode == 1.0 and iRecalibratesDone == 6.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iRecalibratesDone", "N", 1000)
        local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
        VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + 200)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("300910:[VOICE|Doll] Nice work, 771852![P] The scanners are all sending data again![B][C]") ]])
		fnCutscene([[ Append("300910:[VOICE|Doll] And there's 200 more work credits in your account.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] We're just happy to help.[B][C]") ]])
		fnCutscene([[ Append("300910:[VOICE|Doll] Good luck out there.[P] Come back in one piece.") ]])

	--Has the code and has finished the quest.
	elseif(iHasRecalibrationCode == 1.0 and iRecalibratesDone == 1000.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("300910:[VOICE|Doll] Everything all right out there, 771852?[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] So far, so good.[P] Just letting you know we're all right.[B][C]") ]])
		fnCutscene([[ Append("300910:[VOICE|Doll] Glad to hear it, but don't take any risks.") ]])
	
	end
	
elseif(sObjectName == "FabrBench") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (A fabrication bench.[P] Several partially-completed parts are on sitting nearby, disorganized.)") ]])

-- |[Skillbook]|
elseif(sObjectName == "Skillbook") then
    AL_SetProperty("Set Layer Disabled", "SkillbookIco", true)
	LM_ExecuteScript(gsChristineSkillbook, 4)
    
-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
