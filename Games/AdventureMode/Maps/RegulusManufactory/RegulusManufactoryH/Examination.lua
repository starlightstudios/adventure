-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
-- |[Examinables]|
if(sObjectName == "ToManufactoryIDoor") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|PDU] ...[P] No good, this door's been welded shut.[P] We'd need a bomb to open it.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smirk] Given the material storage area, I could - [P][CLEAR]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Spend a great deal of time analyzing the cave layout because it'd likely cause a collapse?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] We don't need to be standing next to the explosive, Christine.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] I'm not convinced we need to bury the units here in a thousand tons of rock, 55.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] ...[P] Yet.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Let's just look for a way around before we start mixing explosives, okay?") ]])

    
-- |[Other]|
elseif(sObjectName == "CrateSwitch") then
    
    --Variables.
    local iManuCrateState = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuCrateState", "N")
    
    --Crate is in the default state as it appears in Tiled.
    if(iManuCrateState == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iManuCrateState", "N", 1.0)
        
        --Camera.
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 3.0)
            CameraEvent_SetProperty("Focus Position", (47.25 * gciSizePerTile), (21.50 * gciSizePerTile))
        DL_PopActiveObject()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Disable the high layer.
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "CrateHi", true) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "CrateLo", true) ]])
        
        --Movement.
        for i = 1, 48, 1 do
            local sString = "AL_SetProperty(\"Modify Dislocation\", \"CrateDislocation\", " .. ((42 * gciSizePerTile) - i) .. ", " .. (19 * gciSizePerTile) .. ")"
            fnCutscene(sString)
            fnCutsceneWait(2)
            fnCutsceneBlocker()
        end
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 3.0)
            CameraEvent_SetProperty("Focus Actor Name", "Christine")
        DL_PopActiveObject()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Change collisions.
        AL_SetProperty("Set Collision", 51, 22, 0, 1)
        AL_SetProperty("Set Collision", 52, 22, 0, 1)
        AL_SetProperty("Set Collision", 43, 22, 0, 0)
        AL_SetProperty("Set Collision", 44, 22, 0, 0)
        
    --Crate is in the shifted state.
    else
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iManuCrateState", "N", 0.0)
        
        --Camera.
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 3.0)
            CameraEvent_SetProperty("Focus Position", (47.25 * gciSizePerTile), (21.50 * gciSizePerTile))
        DL_PopActiveObject()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        for i = 1, 48, 1 do
            local sString = "AL_SetProperty(\"Modify Dislocation\", \"CrateDislocation\", " .. ((42 * gciSizePerTile) - 48 + i) .. ", " .. (19 * gciSizePerTile) .. ")"
            fnCutscene(sString)
            fnCutsceneWait(2)
            fnCutsceneBlocker()
        end
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 3.0)
            CameraEvent_SetProperty("Focus Actor Name", "Christine")
        DL_PopActiveObject()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Enable the high layer.
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "CrateHi", false) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "CrateLo", false) ]])
        
        --Change collisions.
        AL_SetProperty("Set Collision", 51, 22, 0, 0)
        AL_SetProperty("Set Collision", 52, 22, 0, 0)
        AL_SetProperty("Set Collision", 43, 22, 0, 1)
        AL_SetProperty("Set Collision", 44, 22, 0, 1)
    
    
    end

-- |[Debug]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
