-- |[ ====================================== Examination ======================================= ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--Variables.
local iManuReassigned         = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuReassigned", "N")
local iManuDroneTaskA         = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskA", "N")
local iManufactoryDepopulated = VM_GetVar("Root/Variables/Chapter5/Scenes/iManufactoryDepopulated", "N")
local iManuFinale             = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuFinale", "N")

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "ToManufactoryC") then
    
    --Can't leave in latex drone mode.
    local iManuSetAuthChip = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuSetAuthChip", "N")
    local iManuReassigned  = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuReassigned", "N")
    local iManuDroneTaskC  = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskC", "N")
    if(iManuSetAuthChip == 1.0 and iManuReassigned == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I should present myself for assignment as a new drone unit before I report back to 55.)") ]])
        fnCutsceneBlocker()
        return
    elseif(iManuReassigned == 1.0 and iManuDroneTaskC < 2.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (CURRENT FUNCTION ASSIGNMENT DOES NOT REQUIRE ACCESS TO LOWER CITY.)") ]])
        fnCutsceneBlocker()
        return
    end
    
    --Map change.
    fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutscene([[ AL_BeginTransitionTo("RegulusManufactoryC", "ENTPOS:ToManufactoryD:S") ]])

elseif(sObjectName == "ToManufactoryELft") then

    --Variables.
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    local iManuShowElevatorScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuShowElevatorScene", "N")
    
    --Finale is in place: Don't stop any movement.
    if(iManuShowElevatorScene == 1.0) then
        fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutscene([[ AL_BeginTransitionTo("RegulusManufactoryE", "ENTPOS:ToManufactoryDLft:N") ]])
    
    --Can't use the slave entrance as a lord:
    elseif(sChristineForm == "Golem") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Better not use the slave unit entrance, might make the local lords suspicious.)") ]])
        fnCutsceneBlocker()
    
    --All other cases, allow:
    else
        fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutscene([[ AL_BeginTransitionTo("RegulusManufactoryE", "ENTPOS:ToManufactoryDLft:N") ]])
    end
    
elseif(sObjectName == "ToManufactoryERgt") then

    --Variables.
    local sChristineForm  = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    local iManuShowElevatorScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuShowElevatorScene", "N")
    local iManuReassigned = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuReassigned", "N")
    
    --Finale is in place: Don't stop any movement.
    if(iManuShowElevatorScene == 1.0) then
        fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutscene([[ AL_BeginTransitionTo("RegulusManufactoryE", "ENTPOS:ToManufactoryDRgt:N") ]])
    
    --Can't use the lord entrance as a drone:
    elseif(sChristineForm == "LatexDrone" and iManuReassigned == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The lords might get suspicious if a new drone were to enter using the lord entrance.[P] Better use the slave entrance.)") ]])
        fnCutsceneBlocker()
    
    --All other cases, allow:
    else
        fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutscene([[ AL_BeginTransitionTo("RegulusManufactoryE", "ENTPOS:ToManufactoryDRgt:N") ]])
    
    end

-- |[Examinables]|
elseif(sObjectName == "Airlock") then

    if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (This airlock leads to the surface, which is not where I want to go.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (DRONE FUNCTION ASSIGNMENT DOES NOT REQUIRE EXITING SECTOR 99.[P] ACCESS DENIED.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "Intercom") then

    if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Voice:[VOICE|Golem] Intercom service.[P] How may I help you?[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Just checking.[P] Intercom operator response time is excellent.[B][C]") ]])
        fnCutscene([[ Append("Voice:[VOICE|Golem] Thank you, Unit 771852.") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0 and iManuFinale == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Voice:[VOICE|Golem] Intercom service.[P] How may I help you?[B][C]") ]])
        fnCutscene([[ Append("772890:[VOICE|Christine] DRONE AUTOMATED SYSTEMS CHECK.[P] AUTHBACK 449Q.[B][C]") ]])
        fnCutscene([[ Append("Voice:[VOICE|Golem] Registered 449Q.[P] Return to work, drone.[B][C]") ]])
        fnCutscene([[ Append("772890:[VOICE|Christine] RETURNING TO FUNCTION ASSIGNMENT AS ORDERED.") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Voice:[VOICE|Golem] Intercom service.[P] How may I help you?[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Can you tell me how everything is going in the sector?[B][C]") ]])
        fnCutscene([[ Append("Voice:[VOICE|Golem] Oh, Christine![P] We're taking it one step at a time.[P] Thanks for asking![B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Keep up the good work, then.") ]])
    end
    
    
elseif(sObjectName == "Note") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Slave unit entrance and deliveries.[P] Visiting lords, use right door.')") ]])
    fnCutsceneBlocker()

-- |[Other]|
-- |[Debug]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
