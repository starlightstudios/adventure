-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Meeting Katarina.
if(sObjectName == "TalkToKatarina") then

    --Repeat check.
    local iManuMetKatarina = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuMetKatarina", "N")
    if(iManuMetKatarina == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iManuMetKatarina", "N", 1.0)
    
    --Movement.
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
    fnCutsceneMove("Christine", 18.25, 8.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneMove("Tiffany", 19.25, 8.50)
    fnCutsceneFace("Tiffany", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Katarina turns around.
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneFace("Tiffany", -1, 0)
    fnCutsceneFace("Katarina", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Katarina", "Neutral") ]])
    fnCutscene([[ Append("Lord:[E|Neutral] Intriguing.[P] You came from the underground passages?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Yes, we did.[P] The trams were out, we got out at Sector 48.[B][C]") ]])
    fnCutscene([[ Append("Lord:[E|Neutral] I heard, my PDU let me know there was a transit interruption.[P] You are Unit 771852?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Lord Golem of Maintenance and Repair, Sector 96, at your service.[P] How many I serve the Cause of Science?[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] Unit 600484 speaking.[P] Though please, call me Katarina.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Christine.[P] Pleased to meet you.[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] And the command unit who is shadowing you...?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] ...[P] Hello.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] Oh, where [P]*are*[P] my manners?[P] This Unit...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] 1...[P] 1...[P] 1...[P] uuhhhh...[P] 1![B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] Command Unit 1111?[P] Head of Intelligence and AI research?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Correct.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Well, you see, I'm currently being considered for a pay raise in my department.[P] My command unit wanted me to be evaluated during my next special assignment.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Unit 1111 is under orders just to observe, not interact with anyone.[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] Ah, yes.[P] Standard procedure.[P] Still, you should let the staff know that before your inspection, Christine.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] Oh, of course![P] I'll have my PDU let them know right away![B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] So, Katarina.[P] You're an efficiency expert.[P] Why have you called me here today for a special assignment?[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] You have a reputation among the workers in Sector 96 for being kindhearted and open.[P] Good.[P] I've been trying for years to get my superiors to listen to me.[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] An arrogant or dismissive lord does not inspire the slave units to work themselves until their parts are falling off.[P] An open palm is better than a closed fist.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] I couldn't agree more.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] (Also 55 probably fudged the paperwork to give me a better 'reputation', but I guess I am kind of popular...)[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] (Mostly due to Sophie's kind heart...)[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] Well, Sector 99 is a manufacturing sector.[P] Until a few months ago, light industry, mostly.[P] Consumer goods, electronics, that sort of thing.[P] If you've placed your metallic feet on a carpet in Regulus City, there's a 70 percent chance it came from Sector 99.[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] Recently, they were switched to weapons and ammunition manufacturing for the security forces.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Any idea why?[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] Oh, it's rather common for sectors to switch assignments.[P] Central Administration likely weighed the productivity loss of retooling against many other factors.[P] It's nothing unusual.[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] But, something odd happened last month.[P] A sudden spike of productivity, an increase of fifty percent.[P] Fifty![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] Capital![P] May I borrow their secret?[P] My sector could do with a productivity increase![B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] And this is why I've called you here.[P] I don't know the secret.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] May I interrupt?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Of course, Command Unit 1111.[P] Am I doing something wrong?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smug] A detailed list of errors will be in my full report later.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] (Well, she's a good actress, isn't she?)[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] However, I would like to ask Unit 600484 how she does not know the 'Secret'.[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] Productivity is easy to measure, but the exact source is difficult to determine.[P] It's my job to determine what makes units produce more, and make sure other sectors replicate that success.[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] An increase of personnel, a new work technique, better scheduling, incentives programs.[P] Sometimes simply moving the production line around increases output.[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] I've never seen a sector increase productivity by fifty percent and not do [P]*anything*.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Nothing?[P] At all?[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] Nothing.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] You've checked the shipping records?[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] Yes, the shipped parts arrived as declared.[P] There were more of them, it was not a miscount.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Have you tried asking the units what they did?[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] Absolutely nothing, so they say.[P] That's where you come in.[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] Unit 771852, I need you to investigate this sector for me and determine what it is they did to produce such an impressive increase in production.[P] You are doing this so that other sectors might share the benefits.[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] Now I recognize that you are basically doing my job.[P] I apologize for that.[P] However, I believe the issue may be that it is me asking.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] But why would a sector not tell you immediately?[P] How does the Cause benefit from withholding information like that?[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] I am not sure.[P] The lord units here are as tight-lipped as the slave units.[P] I simply cannot proceed as I am.[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] I am hoping your charm will be enough to win them over.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] I'll do what I can.[P] Leave it to me.[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] Very good.[P] Now, I would help, but it's likely best if I'm not seen with you.[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] I was going to go back to Sector 22 and at least get some work done, but I suppose with the trams out, it'd be hours before I got back here if you found something.[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] I hate being idle...[P] Perhaps I can help with Sector 15's problems with my PDU...[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] That is fine.[P] You have done well, Unit 600484.[P] Unit 771852, proceed.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Yes, Command Unit.[P] Moving out.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] (She's better at acting like someone else than acting like herself, isn't she?)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    fnAutoFoldParty()
    fnCutsceneBlocker()

--After disabling the golems.
elseif(sObjectName == "ReadyToGo") then

    --Repeat check.
    local iManuShowElevatorScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuShowElevatorScene", "N")
    if(iManuShowElevatorScene ~= 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iManuShowElevatorScene", "N", 0.0)
    
    --Black out the scene.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Spawn 55.
    fnSpecialCharacter("Tiffany", -100, -100, gci_Face_South, false, nil)
    AL_SetProperty("Open Door", "DoorE")
    
    --Position actors.
    fnCutsceneTeleport("Tiffany", 19.25, 7.50)
    fnCutsceneFace("Tiffany", -1, 1)
    fnCutsceneTeleport("Christine", -1.25, -1.50)
    fnCutsceneFace("Christine", 0, 1)
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0)
        CameraEvent_SetProperty("Focus Position", (19.25 * gciSizePerTile), (7.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutsceneWait(45)
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --Movement.
    fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneTeleport("Christine", 18.25, 4.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Christine", 19.25, 6.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Tiffany", 0, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Any movement up here?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] No activity.[P] Local network traffic is suspended.[P] I cannot hear the hum of the factory machinery.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I don't like where this is going at all.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] The golems?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] System standby.[P] I checked the cranial chassis of the one you bumped on the head to disable.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Were repairs necessary?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] She'll be fine, but I found something unusual under her killphrase synaptic spike.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] It looked like a receiver of some sort, but it's not in my repair database and I don't have any idea what purpose it serves.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] There's an entanglement send and receive unit, pretty crudely made, but it wasn't hooked to anything.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Are you certain?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I found the same one in all three golems, and removed them, just to be sure.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] I...[P] also found another one on the ground.[P] I think they were going to put it in me...[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Proceed with extreme caution, 771852.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] ...[P] Katarina and that other golem don't seem to have noticed.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] She is not a threat to them.[P] We are.[P] She will become a threat if she speaks to us and attempts to leave the sector.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Do not say anything to alarm her.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Sure, okay.[P] That will help.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smirk] Are you intending to stay in drone unit form for the duration of this mission?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] I kinda like being a rubber robot![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] But I suppose it doesn't matter if I'm seen at this point.[P] I can go back downstairs and change forms if needed.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Let us proceed, then.[P] Lead the way.") ]])
    fnCutsceneBlocker()
    
    --Fold the party.
    fnAddPartyMember("Tiffany")
    fnAutoFoldParty()
    fnCutsceneBlocker()

elseif(sObjectName == "VolUp") then
    AL_SetProperty("Mandated Music Intensity Now", 20.0)
elseif(sObjectName == "VolDn") then
    AL_SetProperty("Mandated Music Intensity Now", 5.0)

end
