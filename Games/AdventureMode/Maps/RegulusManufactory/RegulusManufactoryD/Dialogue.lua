-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--Variables.
local sForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
local iManufactoryDepopulated = VM_GetVar("Root/Variables/Chapter5/Scenes/iManufactoryDepopulated", "N")
local iManuFinale = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuFinale", "N")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Katarina, efficiency expert.
    if(sActorName == "Katarina") then
        
        --Variables.
        local iManuSetAuthChip = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuSetAuthChip", "N")
        
        --Initial meeting.
        if(iManuSetAuthChip == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Katarina:[VOICE|Katarina] Let me know if you need anything.[P] I dislike idleness, but I have no choice at the moment.") ]])
        
        --Drone. Beep boop.
        elseif(iManufactoryDepopulated == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Katarina:[VOICE|Katarina] I wouldn't guess you're here to report the track is cleared, or Christine found something?[P] No?[P] Oh well.[P] Back to work, drone.") ]])
        
        --Manufactory depopulated...
        elseif(iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
            
            if(sForm == "Human") then
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Katarina", "Neutral") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] A human?[P] Here?[P] Command unit 1111, what is going on?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] A breeding program unit.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Angry] The clumsy organic lost her day pass badge, so I must make sure she is not converted until she returns to the biolabs.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] (...)[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] I'm -[P] so sorry Command Unit![P] I swear it won't happen again![B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] Please go easy on her, command unit.[P] You know their brains tend to forget things.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] I understand.[P] It is irresponsible to hold them to the same standards.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] I'm doing my best.[P] When I am converted, I will not fail you again![B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] At least she has the right attitude.") ]])
                fnCutsceneBlocker()
                
            elseif(sForm == "Golem") then
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Katarina", "Neutral") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] Oh, Christine.[P] You were gone for a while.[P] Find anything?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ...[P] Nothing at all.[P] Stay here.[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] I wasn't intending to leave.[P] The trams will be out for some time and I don't leave a function assignment incomplete.[P] That's how I got my current job.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] ...[P] Excuse me?[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] I wasn't always a lord unit.[P] I was repurposed when the lord I was working for was demoted.[P] I think she's a drone someplace in Sector 189.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] You're a promoted slave unit?[P] I don't think I've ever heard of that.[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] Former drone unit, actually.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] Oh my...[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] Even with my inhibitor on, I was more capable than she was.[P] Evidently the Administration could see that.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] Did you enjoy being a drone?[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] I don't miss it.[P] I like my hair, and my ability to run advanced scripts.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] If you could keep your hair, and your intellect?[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] What are you implying, Christine?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] N-[P]nothing![B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] I suppose it is an experience not many lords would understand, so I suppose I should share.[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] The chassis is certainly smoother and more environmentally protective.[P] They're designed for dangerous conditions.[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] But the sensation of having someone rub their hand up and down your back will make your leg motivators give out.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] Mmmmmm...[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] There's a certain quality to simply doing as you're told and not thinking.[P] It's quite relaxing to not have to worry about the future.[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] Just be quiet, obedient, and empty.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] Yeah...[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] Well, I've taken enough of your time.[P] Let me know if you find anything during your investigation.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] Sure...[P] of course...") ]])
                fnCutsceneBlocker()
                
            elseif(sForm == "LatexDrone") then
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Katarina", "Neutral") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] You're back, drone.[P] Any news about the transit network?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] REPAIR CREWS ARE WORKING TO CLEAR THE RUBBLE.[P] NO FURTHER NEWS AT THIS TIME.[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] Can't be helped, then.[P] As you were, drone.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] AFFIRMATIVE.[P] RETURNING TO FUNCTION ASSIGNMENT.") ]])
                
            elseif(sForm == "Darkmatter") then
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Katarina", "Neutral") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] Oh my![P] A darkmatter![P] You frightened me![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ...[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] She appears to be allowing me to follow her.[P] I am trying to gather what data I can.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Laugh] ...[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] Oh![P] She looks happy![B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] She's a cute one.[P] If you don't find anything out, you can at least upload photos of her to the network.[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] Nothing helps me relax after a long day quite like looking at pictures of darkmatter girls.") ]])
                
            elseif(sForm == "Eldritch") then
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Katarina", "Neutral") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] !!![P] What are you?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Good question.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] She is a biological research specimen.[P] While docile, I must accompany her at all times.[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] Oh, I see.[P] What have they gotten up to at the university?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Despite being organic, she has an order of mangitude more strength, durability, and agility compared to a human.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] She is impervious to the effects of vacuum, cold, heat, and radiation.[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] Amazing![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Thank you, lord unit.[P] I hope my genetics help the Cause of Science.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Please watch the network as we intend to begin publishing our initial trial results soon.[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] I certainly will![P] Good luck, command unit.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] *She believed us?*[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] *The researchers at the university are in no way constrained by any sort of ethical boundaries.*") ]])
                
            elseif(sForm == "Electrosprite") then
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Katarina", "Neutral") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] !!![P] What are you?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Hi![P] I'm an electrical girl![B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] We have discovered this partirhuman species on Pandemonium.[P] They possess an innate sense of electrical wiring.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] I believe they may be useful in optimizing power grids.[P] I have asked her here to help with the task you charged 771852 with.[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] Bringing in contractors from Pandemonium?[P] That...[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] Well, you're a command unit.[P] You have the authority to do such things.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] And all they need to pay me with is a nice live wire to bite into![B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] When Unit 1111 is done with you, I may wish to acquire your services.[P] Until then, good luck.") ]])
                
            elseif(sForm == "Secrebot") then
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Katarina", "Neutral") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] Ah, one of those secrebots everyone has been talking about.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Hello, lord golem.[P] May I serve you?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] They are adept and loyal assistants.[P] This one is programmed for combat, repair, and data analysis.[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] My my, perhaps they'll make a fine addition to Regulus City's workforce.[P] After the testing phase, that is.[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] I will look forward to your work, secrebot.") ]])
                
            elseif(sForm == "Raiju") then
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Katarina", "Neutral") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] Oh, my![P] You're a cute one![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Hi![P] Wanna snuggle?[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] Oh no, I couldn't.[P] I'm technically at work right now.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] Awwwww...[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] This raiju will be assisting me for a moment.[P] We located an electrical surge.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] They are immune to voltage of nearly any magnitude, so she will be aiding in the repairs.[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] Clever, but most of the raijus I've met lack the intellect for advanced work.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] I'm a class six technician![B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] Very impressive.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] I just love a good snuggle between jobs...[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] Later![P] Oh ho ho, later when I'm not at work!") ]])
                
            elseif(sForm == "Doll") then
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Katarina", "Neutral") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] Oh, another command unit?[P] Where has Unit 771852 gotten to?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] She is occupied, working on the task you requested.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Hello.[P] Is this the efficiency expert you mentioned?[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] I did not think I would be the topic of conversation among the command units.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] I wanted to ask you something.[P] What is your take on morale in the workplace?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] My staff tend to work better when they are motivated, and I have found that positive reinforcement is always better than negative reinforcement.[P] Obviously, I have not conducted long-form trials.[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] Wouldn't such a thing be on the university's servers?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] For political reasons, no.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] We keep certain topics from public knowledge.[P] The Administration believes this is one such topic.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] As such, only a handful of studies on small samples have been conducted.[P] I was wondering if you, personally, knew more.[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] I can tell you right now that productivity increases when a unit is motivated, happy, and rested.[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] However, I understand fully the political motivation.[P] Units that are not overworked also tend to be more...[P] aspirational.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Clarify.[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] They talk of revolt more openly.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] I see.[P] Thank you for your insight.[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] Just doing my part for the Cause.") ]])
                
            elseif(sForm == "SteamDroid") then
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Katarina", "Neutral") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] Unit 1111, I know it is not my place to question a command unit.[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] But why is there a steam droid here?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Hey, I'll take any job that pays.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] The few remaining steam droids have made a number of novel engineering improvements to continue to function.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smirk] It is not common knowledge, but the Administration retains contact with some steam droids.[P] We contract out their services on occasion.[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] And you are not punished for breaking the rules?[P] She could steal something![B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] I am keeping track of all her activities.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] This doll girl just wants me to see if any of the fabricator benches were upgraded illegally.[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] Ah, you're working with 771852.[P] Now I understand.[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] She apparently has friends in high places.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] And low places, too.[B][C]") ]])
                fnCutscene([[ Append("Katarina:[E|Neutral] Unfortunately, we officially have not met, miss steam droid.[P] I wouldn't want the security services to ask a lot of questions.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Me neither.[P] Better not mention me.[P] Unit 1111, shall we?") ]])
            end
        
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Katarina", "Neutral") ]])
            fnCutscene([[ Append("Katarina:[E|Neutral] Just in time for me to not leave the sector, the work crews are getting the transit situation under control.[B][C]") ]])
            fnCutscene([[ Append("Katarina:[E|Neutral] Funny that it was originally going to take a month, and now it will be done in a few hours.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Mirabelle?[B][C]") ]])
            fnCutscene([[ Append("Katarina:[E|Neutral] Likely.[P] Apparently the work crews had been provided 'outdated' schematics.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] It was protecting itself.[P] Sector 99 is on a hill and has no cover on the overland approach.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] The storage facility is built into a ridgeline and would be difficult to bombard.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Being discovered would be the real problem, 55.[P] Disabling the transit line would keep lords from snooping around.[B][C]") ]])
            fnCutscene([[ Append("Katarina:[E|Neutral] I wonder what would have happened to me if you hadn't come, Christine.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Speaking of, how is everyone taking it?[B][C]") ]])
            fnCutscene([[ Append("Katarina:[E|Neutral] They remember vaguely what they have to keep secret.[P] I spoke to the other lords, and they're with you.[B][C]") ]])
            fnCutscene([[ Append("Katarina:[E|Neutral] Maybe it's a residual part of the hive mind, but the lords don't consider themselves too high and mighty to work with slave units as equals anymore.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Laugh] Oh, we should have let it assimilate the city, then![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Seriously, that's a positive development.[P] Guess there's a silver lining here.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Do what you can to keep up appearances.[P] Sector 99's productive capacity will prove extremely useful in the future.[B][C]") ]])
            fnCutscene([[ Append("Katarina:[E|Neutral] For now, we're still shipping off pulse weapons.[P] We have to fulfill our requisition claims.[B][C]") ]])
            fnCutscene([[ Append("Katarina:[E|Neutral] Good luck, Christine.[P] For all our sakes.") ]])
        
        end
    
    --Delivery golem.
    elseif(sActorName == "GolemA") then
        
        --Variables.
        local iManuSetAuthChip = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuSetAuthChip", "N")
        
        --Initial meeting.
        if(iManuSetAuthChip == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] ...") ]])
        
        --Drone. Beep boop.
        elseif(iManufactoryDepopulated == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] I had to deliver some chips but the track was out, so I did it on foot.[P] Now I need to wait for the tram.[P] Hey, just following procedure, right?[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Besides, Lord Golem Katarina is actually nice to talk to.[P] I don't mind.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] ...[P] Wow I'm so desperate, I'm talking to a drone.[P] I must be losing it...") ]])
        
        --Manufactory depopulated...
        elseif(iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (She's playing a game on her PDU.[P] Looks like it has something to do with stacking slime girls.[P] Better not interrupt her.)") ]])
            fnCutsceneBlocker()
        
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Pack") ]])
            fnCutscene([[ Append("Chip:[E|Pack] By the way, Christine.[P] My name's Chip.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Charmed.[B][C]") ]])
            fnCutscene([[ Append("Chip:[E|Pack] Katarina is pretty nice.[P] She got me a transfer order to work in Sector 99 out of her own work credits.[B][C]") ]])
            fnCutscene([[ Append("Chip:[E|Pack] I think she just likes talking to me.[P] We're both possession fetishists.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Scared] Woah![P] A bit too much information, Chip![B][C]") ]])
            fnCutscene([[ Append("Chip:[E|Pack] Oh, as if you don't have any kinks.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Blush] Now that you mention it...[B][C]") ]])
            fnCutscene([[ Append("Chip:[E|Pack] Hey, did you know Katarina used to be a drone unit?[P] Imagine getting that promotion.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] ...[P] Okay?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] (Chip sure does pick weird topics...)[B][C]") ]])
            fnCutscene([[ Append("Chip:[E|Pack] Oh yeah, I'll let you go, Christine.[P] Just give me your PDU address.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|PDU] To keep in touch?[P] Sure![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|PDU] ...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] You sent me a photo of a darkmatter?[B][C]") ]])
            fnCutscene([[ Append("Chip:[E|Pack] I got tons of them.[P] Good luck out there.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] (What an interesting unit...)") ]])
        end
    
    end
end
