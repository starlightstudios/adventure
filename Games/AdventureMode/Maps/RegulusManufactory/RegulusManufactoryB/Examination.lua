-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToManufactoryA") then
    
	--Variables.
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	
	--Christine needs to change form.
	if(sChristineForm ~= "Golem") then
	
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](Better transform to a Lord Unit...)") ]])
		fnCutsceneBlocker()
	
		--Flashwhite.
		Cutscene_CreateEvent("Flash Christine White", "Actor")
			ActorEvent_SetProperty("Subject Name", "Christine")
			ActorEvent_SetProperty("Flashwhite Quickly", "Null")
		DL_PopActiveObject()
		fnCutsceneBlocker()

		fnCutsceneWait(75)
		fnCutsceneBlocker()
		fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
		fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
		fnCutsceneBlocker()
	end
    
    fnCutscene([[ AudioManager_PlaySound("World|ClimbLadder") ]])
    fnCutscene([[ AL_BeginTransitionTo("RegulusManufactoryA", "ENTPOS:ToManufactoryB:N") ]])
    fnCutsceneBlocker()
    
-- |[Examinables]|
elseif(sObjectName == "FizzyPop") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Fizzy Pop! machines![P] These things are everywhere, as that is the natural order of the universe.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Intercom") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This intercom has been disconnected and the microphone stripped out.[P] It's probably been sitting here for a while.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Fabricator") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A fabrication bench in working order.[P] Odd that some unit just left it sitting here.[P] Maybe they were transporting it and got called away?)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Crates") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Aluminum cans.[P] Cleaned out and sorted, probably used to store organic rations and destined for recycling.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Note") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Fluid retention pond for organic shrimp farming.[P] NO TOXIN DUMPING, VIOLATERS WILL BE PUNISHED.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Terminal") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The terminal is monitoring the water content for the storage reservoir.)") ]])
    fnCutsceneBlocker()
    
-- |[Other]|
-- |[Debug]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
