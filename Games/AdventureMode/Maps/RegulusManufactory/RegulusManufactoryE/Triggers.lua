-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Volume changers.
if(sObjectName == "ManuLoud") then
    AL_SetProperty("Mandated Music Intensity", 70.0)
    
elseif(sObjectName == "ManuQuiet") then
    AL_SetProperty("Mandated Music Intensity", 40.0)
    
--Cutscene.
elseif(sObjectName == "FirstVisit") then
    
    --Repeat check.
    local iManuGotBadge = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuGotBadge", "N")
    if(iManuGotBadge == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iManuGotBadge", "N", 1.0)
    
    --Scene.
    fnCutsceneTeleport("Christine", 29.25, 16.50)
    fnCutsceneFace("Christine", 0, -1)
    fnCutsceneTeleport("Tiffany", 30.25, 16.50)
    fnCutsceneFace("Tiffany", 0, -1)
    fnCutsceneBlocker()
    
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Hello, lord unit.[P] Command unit.[P] Is your visit official or unofficial?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Unofficial.[P] Behest of a fellow lord unit.[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Is the lord unit employed in this sector?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] No.[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Very good, here are your visitor security badges.[P] Please keep them on at all times.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] (Better play my role...)[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Slave unit, why do we require security badges?[P] Are you implying your superior units are possible security threats?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] (Oh I hope I don't get her in trouble...)[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] No, lord unit.[P] These security badges are for your protection.[P] They will emit a radio warning if you are in an area you are not cleared for.[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] This sector has many automated moving parts that do not check for clearance.[P] We would not want you to be damaged if you entered an active area unknowingly.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Ah, I see.[P] Excellent work, unit.[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Please do not enter the automated long-term storage area at the rear of the facility, or cross any of the marked yellow lines.[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] If you have any comments, please speak to the supply administrator or the factory floor administrator.[P] Thank you.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] (Hmm, those two sector administrators would be a good place to start our investigation...)") ]])
    fnCutsceneBlocker()
    fnAutoFoldParty()
    fnCutsceneBlocker()
    
elseif(sObjectName == "Reassignment") then

	--Do the thing here.
    local iManuSetAuthChip = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuSetAuthChip", "N")
    local iManuReassigned = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuReassigned", "N")
    if(iManuReassigned == 1.0 or iManuSetAuthChip ~= 1.0) then return end
    
    --Spawn NPC.
    fnStandardNPCByPosition("Inspector")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iManuReassigned", "N", 1.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Golem:[VOICE|Golem] Drone unit identified without appropriate passcodes.[P] Drone, state purpose.[B][C]") ]])
    fnCutscene([[ Append("Christine:[VOICE|Christine] UNIT 772890 REPORTING FOR FUNCTION ASSIGNMENT.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Inspector", 23.25, 14.50)
    fnCutsceneMove("Inspector", 23.25, 15.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Golem:[VOICE|Golem] ...[P] Permission check green.[P] Network handshake successful.[P] Drone, follow me for function assignment.[B][C]") ]])
    fnCutscene([[ Append("Christine:[VOICE|Christine] AFFIRMATIVE.[P] FOLLOWING SUPERIOR UNIT.[B][C]") ]])
    fnCutscene([[ Append("Christine:[VOICE|Christine] (Seems they bought it.[P] Great spoofing work, 55!)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Inspector", 23.25, 14.50)
    fnCutsceneMove("Christine", 23.25, 15.50)
    fnCutsceneBlocker()
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Open Door", "DoorW") ]])
    fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneMove("Inspector", 23.25, 6.50)
    fnCutsceneMove("Inspector", 18.25, 6.50)
    fnCutsceneMove("Inspector", 18.25, 4.50)
    fnCutsceneMove("Christine", 23.25, 7.50)
    fnCutsceneMove("Christine", 23.25, 6.50)
    fnCutsceneMove("Christine", 19.25, 6.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Golem:[VOICE|Golem] Permission setup completed.[P] Begin inspection.[P] Drone, stand at attention.[B][C]") ]])
    fnCutscene([[ Append("Christine:[VOICE|Christine] AFFIRMATIVE.") ]])
    fnCutsceneBlocker()
    fnCutsceneMove("Inspector", 18.25, 6.50)
    fnCutsceneFace("Inspector", 1, 0)
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Golem:[VOICE|Golem] Minor drone anomaly detected.[P] Drone, turn around.[P] Expose CPU casing.[B][C]") ]])
    fnCutscene([[ Append("Christine:[VOICE|Christine] (Uh oh![P] I think they're on to me, but I can't blow my cover!)[B][C]") ]])
    fnCutscene([[ Append("Christine:[VOICE|Christine] ...[P] AFFIRMATIVE.[P] TURNING AROUND.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 1, 0)
    fnCutsceneWait(85)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Fix completed.[P] Drone, report status of inhibitor chip.[P] Run basic diagnostics.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] (W-[P]wait![P] She did so -[P] something?[P] I can't...)[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] (WHAT DID SHE DO? SUPERIOR UNIT DID SOMETHING.[P] DRONE WAS SUPPOSED TO DO SOMETHING.)[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] (I -[P] I...[P] PREVIOUS MISSION PARAMETERS EXCEED ALLOWED COMPLEXITY.[P] DISREGARD.[P] RESPOND TO REQUEST.)[B][C]") ]])
    fnCutscene([[ Append("772890:[E|Neutral] INHIBITOR CHIP REPORTS GREEN.[P] MENTAL ACUITY WITHIN NORMAL PARAMETERS.[B][C]") ]])
    fnCutscene([[ Append("772890:[E|Neutral] IDENTITY CHECK.[P] UNIT 772890.[P] ASSIGNMENT, SECTOR 99, LABOUR AND SERVICE.[P] PLEASE INPUT FUNCTION ASSIGNMENT.[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Response within expected parameters.[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] An order was misfiled.[P] Move material from the brown crates at the loading dock to the storage rooms north of the factory floor.[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Familiarize yourself with the layout of the area.[P] You will not be assigned to the factory floor until your hair has been removed as per standard.[P] It may get caught in the machinery, do not pass the yellow lines unless ordered.[B][C]") ]])
    fnCutscene([[ Append("772890:[E|Neutral] AFFIRMATIVE.[P] FLASH CHIPS IN STANDBY.[P] UNIT WILL NOT CROSS YELLOW LINES UNLESS ORDERED.[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Execute function assignment, drone.[B][C]") ]])
    fnCutscene([[ Append("772890:[E|Neutral] AFFIRMATIVE.[P] UNIT 772890 CARRYING OUT FUNCTION ASSIGNMENT.") ]])
    fnCutsceneBlocker()
    
    --Objectives.
    fnCutscene([[ AL_SetProperty("Register Objective", "DRONE FUNCTION ASSIGNMENT:") ]])
    fnCutscene([[ AL_SetProperty("Register Objective", "1: LOCATE MMF-RC-8 CHIPSET IN BROWN BOXES OF LOADING BAY") ]])
    fnCutscene([[ AL_SetProperty("Register Objective", "2: REPLACE MMF-RC-8 CHIPSET IN STORAGE ROOM ON NORTH END OF FACILITY.") ]])

--Phone call.
elseif(sObjectName == "PDUCall") then

    --Repeat check.
    local iManuDroneTaskC = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskC", "N")
    if(iManuDroneTaskC ~= 1.0) then return end

    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskC", "N", 2.0)

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "PDU") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "PDU", "Neutral") ]])
    fnCutscene([[ Append("PDU:[E|Alarmed] Christine, you are receiving a priority call.[B][C]") ]])
    fnCutscene([[ Append("772890:[E|PDU] UNIDENTIFIED ALIAS, 'Christine'.[P] PRIORITY CHECK -[P] HIGH.[P] CONNECT CALL.[B][C]") ]])
    fnCutscene([[ Append("772890:[E|PDU] UNIT 772890 REPORTING.[P] PLEASE IDENTIFY CALLER.[B][C]") ]])
    fnCutscene([[ Append("PDU:[E|Quiet] [VOICE|Tiffany]Christine, the PDU is hooked into your audio receivers.[P] You don't need to keep the act up, nobody else can hear us.[B][C]") ]])
    fnCutscene([[ Append("772890:[E|PDU] UNIDENTIFIED ALIAS, 'Christine'.[B][C]") ]])
    fnCutscene([[ Append("PDU:[E|Quiet] [VOICE|Tiffany]...[P] I was watching you on the cameras.[P] Did you just give a blue Fizzy Pop! to that lord unit?[B][C]") ]])
    fnCutscene([[ Append("772890:[E|PDU] AFFIRMATIVE.[B][C]") ]])
    fnCutscene([[ Append("PDU:[E|Quiet] [VOICE|Tiffany]And she drank it?[B][C]") ]])
    fnCutscene([[ Append("772890:[E|PDU] AFFIRMATIVE.[B][C]") ]])
    fnCutscene([[ Append("PDU:[E|Quiet] [VOICE|Tiffany]That doesn't seem unusual to you?[B][C]") ]])
    fnCutscene([[ Append("772890:[E|PDU] REPEAT QUERY.[B][C]") ]])
    fnCutscene([[ Append("PDU:[E|Quiet] [VOICE|Tiffany]...[P] Drone 772890, report to the underground of Sector 99.[P] Command Authorization QUIM77T.[B][C]") ]])
    fnCutscene([[ Append("772890:[E|PDU] ASSIGNMENT ACCEPTED.[P] MOVING TO UNDERGROUND.") ]])
    fnCutsceneBlocker()

    --Objective handler.
    fnCutscene([[ AL_SetProperty("Clear Objectives") ]])
    fnCutscene([[ AL_SetProperty("Register Objective", "DRONE FUNCTION ASSIGNMENT:") ]])
    fnCutscene([[ AL_SetProperty("Register Objective", "1: REPORT TO SECTOR 99 UNDERGROUND - OVERRIDE QUIM77T.") ]])
    fnCutscene([[ AL_SetProperty("Register Objective", "2: (DELAYED) MOVE TO LONG-TERM STORAGE FACILITY FOR NETWORK INTEGRATION.") ]])

--Finale.
elseif(sObjectName == "Finale") then

    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iManuFinale", "N", 1.0)
    
    --Achievement.
    AM_SetPropertyJournal("Unlock Achievement", "FinishManufactory")
    
    --Topic.
    WD_SetProperty("Unlock Topic", "Mirabelle", 1)
    
    --Fade to black.
	AL_SetProperty("Music", "Null")
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Flag.
    local iManuFinale = VM_SetVar("Root/Variables/Chapter5/Scenes/iManuFinale", "N", 1.0)
    
    --Spawn entities.
    fnStandardNPCByPosition("Katarina")
    fnStandardNPCByPosition("DeliveryGolem")
    
    --Crowd.
    local iCounter = 0
    local iaPositions = {}
    local fnAddPosition = function(iX, iY)
        iaPositions[iCounter] = {iX, iY}
        iCounter = iCounter + 1
    end
    fnAddPosition( 9,  4)
    fnAddPosition( 9,  5)
    fnAddPosition( 9,  6)
    fnAddPosition( 9,  7)
    fnAddPosition( 9,  8)
    fnAddPosition( 9,  9)
    fnAddPosition( 9, 10)
    fnAddPosition( 9, 11)
    fnAddPosition( 9, 12)
    fnAddPosition(10,  4)
    fnAddPosition(10,  5)
    fnAddPosition(10,  6)
    fnAddPosition(10,  7)
    fnAddPosition(10,  8)
    fnAddPosition(10,  9)
    fnAddPosition(10, 10)
    fnAddPosition(10, 11)
    fnAddPosition(10, 12)
    fnAddPosition(11,  4)
    fnAddPosition(11,  5)
    fnAddPosition(11,  6)
    fnAddPosition(11,  7)
    fnAddPosition(12,  4)
    fnAddPosition(12,  5)
    fnAddPosition(12,  6)
    fnAddPosition(12,  7)
    for i = 0, iCounter-1, 1 do
        TA_Create("Golem"..i)
            TA_SetProperty("Position", iaPositions[i][1], iaPositions[i][2])
            TA_SetProperty("Facing", gci_Face_West)
            TA_SetProperty("Clipping Flag", false)
            local iRoll = LM_GetRandomNumber(1, 100)
            if(iRoll < 40) then
                fnSetCharacterGraphics("Root/Images/Sprites/GolemSlave/", false)
                TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/GolemSlave|Wounded")
            elseif(iRoll < 70) then
                fnSetCharacterGraphics("Root/Images/Sprites/LatexDrone/", false)
                TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/LatexDrone|Wounded")
            else
                fnSetCharacterGraphics("Root/Images/Sprites/GolemLordA/", false)
                TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/GolemLordA|Wounded")
            end
        DL_PopActiveObject()
    end
    
    --Position everyone.
    fnCutsceneTeleport("Christine", 4.25, 6.50)
    fnCutsceneFace("Christine", 1, 0)
    fnCutsceneTeleport("Tiffany", 4.25, 7.50)
    fnCutsceneFace("Tiffany", 1, 0)
    fnCutsceneTeleport("Katarina", 4.25, 8.50)
    fnCutsceneFace("Katarina", 1, 0)
    fnCutsceneTeleport("DeliveryGolem", 3.25, 8.50)
    fnCutsceneFace("DeliveryGolem", 1, 0)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutsceneWait(365)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneWait(365)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Katarina", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Golem", "Pack") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] Christine, why do I have the niggling feeling I'm about to die?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Well, I'll try to be brief.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I'm a transforming maverick with a magical runestone, this is Unit 2855.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Hello.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] We're pretty sure these golems are linked in some sort of hive mind called Project Mirabelle.[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] Transforming bit aside, I'm with you so far.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] And they're not going to let us go because we're a threat to their collective.[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] All of that sounds reasonable.[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Pack] So are they going to add us to the hive mind too, or just retire us?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] They can't add 55 because she's a Command Unit.[P] Might be because she's incompatible.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Command Units don't have killphrases so they don't have synaptic override spikes.[P] I think that's how they control the individual members.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] ...[P] That is correct, I do not have a killphrase.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Tiffany", 5.25, 7.50)
    fnCutsceneBlocker()
    fnCutsceneWait(85)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "LatexDrone", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] Project Mirabelle?[B][C]") ]])
    fnCutscene([[ Append("Mirabelle:[E|Neutral] I will speak for the collective.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] You cannot add me to your collective, because I am incompatible.[B][C]") ]])
    fnCutscene([[ Append("Mirabelle:[E|Neutral] We will develop the technology.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] And if you cannot?[P] There is no guarantee that it is even possible.[P] Command Unit architecture is fundamentally different.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] What if I can never be integrated into your society?[P] Will you retire me?[B][C]") ]])
    fnCutscene([[ Append("Mirabelle:[E|Neutral] No.[P] You will be kept safe.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Down] I will be a burden.[P] I will consume resources but not contribute.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] I do not want that.[B][C]") ]])
    fnCutscene([[ Append("Mirabelle:[E|Neutral] A place for you will be found in our collective.[P] We will find one for you.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] A place for me to belong...[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] 'The Quiet Aristocrat Reads Shivering Ink'[B][C]") ]])
    fnCutscene([[ Append("Mirabelle:[E|Neutral] Sh...[P] Shh...[P] Zzzz...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Sound.
    fnCutscene([[ AudioManager_PlaySound("World|Explosion") ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|Explosion") ]])
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|Explosion") ]])
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneFace("Tiffany", -1, 0)
    
    --Everyone is knocked over.
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneSetFrame("Golem5", "Wounded")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneSetFrame("Golem14", "Wounded")
    fnCutsceneSetFrame("Golem3", "Wounded")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneSetFrame("Golem20", "Wounded")
    fnCutsceneSetFrame("Golem9", "Wounded")
    fnCutsceneSetFrame("Golem3", "Wounded")
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    for i = 0, iCounter-1, 1 do
        fnCutsceneSetFrame("Golem" .. i, "Wounded")
    end
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Sad") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Cry") ]])
    fnCutscene([[ Append("Christine:[E|Sad] 55?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Cry] ...[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] I apologize for the deception, Christine.[P] While we were running, I was scanning everything I could access for information on Project Mirabelle.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] I managed to get a network signal for a few moments and found a mail waiting for me.[P] It contained Mirabelle's killphrase.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] Who sent the mail?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] No sending address.[P] Someone knew I was looking, I did not have time to hide it.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] While that's really bad, at least we have some sort of benefactor.[P] Perhaps a researcher who worked on the project?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] ...[P] If Mirabelle had a killphrase, whoever made it knew it might go rogue...[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] I will see what I can find later.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] So you talking to them like that was you waiting until all the golems were here with us and not in the server room.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Because they're networked together, the server heard the killphrase and blew up, but none of the golems were harmed![P] Clever work, 55![B][C]") ]])
    fnCutscene([[ Append("55:[E|Down] (A place to belong...)[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Thank you, Christine. I was unsure if the killphrase would cause a shutdown or an overheat.[P] It seems we found out which.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Katarina", 5.25, 8.50)
    fnCutsceneFace("Katarina", 0, -1)
    fnCutsceneMove("DeliveryGolem", 4.25, 8.50)
    fnCutsceneFace("DeliveryGolem", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneFace("Tiffany", 0, 1)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Katarina", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Golem", "Pack") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] I'm just going to go ahead and assume I'm now privy to a large amount of things I don't have clearance for.[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Pack] Yeah.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Yep.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Affirmative.[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] Well, I don't particularly want to be reprogrammed, so.[P] Guess I won't be mentioning this on my report.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Oh, Unit 771852 arrived and determined that a compound leaked into the oilmaker and boosted unit productivity.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Unfortunately, it also causes degradation of CPU synaptic integrity, leading to memory ambiguation.[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] Meaning the units become forgetful?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Correct.[P] I suspect Project Mirabelle stored unit memories on its servers.[P] They will not remember anything when they wake up.[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] And if they do?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] They probably don't want to be reprogrammed, either.[P] Help them keep it quiet.[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] Guess we're all mavericks, now.[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] Unit 2855, you were the Head of Security.[P] Can't you do something?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] I am a maverick and have no influence.[P] Your best hope is to cooperate with us, as the Administration is likely to cover this up.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] We're fighting the Administration.[P] They use us as unwilling test subjects for their sick experiments.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] Mirabelle could be used to rehabilitate the hurt, help machines find community.[P] But instead, it was used to make robots work harder.[P] How banal.[P] Such a waste.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Down] ...[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] Are we even sure this was an experiment?[P] What if the project went rogue?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Unfortunately, the answers are on the servers that we just exploded.[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Pack] Then...[P] We'll have to stay here, Katarina.[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] That we will.[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] So, Christine.[P] We don't have a choice, but, count us in.[P] We'll work with you.[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Pack] We'll keep an eye on this sector.[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] I'll get us transferred here officially.[P] I'm sure the lords will agree when they wake up.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] There's just so much wrong in Regulus City.[P] It looks like a paradise from outside...[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] We'll need to return to our other missions, Christine.[P] Katarina, I will contact you later with instructions on how to reach me.[B][C]") ]])
    fnCutscene([[ Append("Katarina:[E|Neutral] All right.[P] Well, first thing to do is get this factory back up and running.[P] A drop in productivity will be even more suspicious than an increase!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Transit to last scene.
    fnCutscene([[ AL_BeginTransitionTo("RegulusManufactoryC", "FORCEPOS:35.0x2.0x0") ]])
    fnCutsceneBlocker()
    
end
