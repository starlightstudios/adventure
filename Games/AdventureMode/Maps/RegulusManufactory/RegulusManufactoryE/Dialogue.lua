-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--Variables.
local sChristineForm          = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
local iManuReassigned         = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuReassigned", "N")
local iManuDroneTaskB         = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskB", "N")
local iManufactoryDepopulated = VM_GetVar("Root/Variables/Chapter5/Scenes/iManufactoryDepopulated", "N")
local iManuFinale             = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuFinale", "N")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
    
    --Mandated for story progression.
    if(sActorName == "Admin") then
        
        --Variables.
        local iManuMetForegolemAsGolem = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuMetForegolemAsGolem", "N")
        local iManuMetAdminAsGolem     = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuMetAdminAsGolem", "N")
            
        --First meeting case:
        if(iManuMetAdminAsGolem == 0.0) then
        
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iManuMetAdminAsGolem", "N", 1.0)
            
            --Dialogue.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Yes, Unit 771852.[P] How may I help you?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Are you the unit in charge of this sector?[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Affirmative.[P] I have been tasked with administration here.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Oh, all right.[P] My sector has a command unit in charge.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Ours was reassigned.[P] I have been tasked with administration here.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Uh, all right.[P] Do you know where she was reassigned to?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|PDU] *PDU, run a query for me.*[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] She was reassigned.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Okay, then.[P] How long have you been in charge here?[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Since our command unit was reassigned.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Interesting.[P] Well, it was nice meeting you.[P] I was just stopping in to say hi.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Hello.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] ...[P] Goodbye?[B][C]") ]])
            
            --"Ending" case.
            if(iManuMetForegolemAsGolem == 1.0) then
                fnCutscene([[ Append("Lord:[E|Neutral] Goodbye.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] (We're not getting anywhere at all with this approach, but I think I have an idea.[P] I should head back to the undercity and speak with 55.)") ]])
                fnCutsceneBlocker()
                fnCutsceneFace("Admin", 0, 1)
            
            --Normal case.
            else
                fnCutscene([[ Append("Lord:[E|Neutral] Goodbye.") ]])
                fnCutsceneBlocker()
                fnCutsceneFace("Admin", 0, 1)
            end
        
        --Repeat.
        elseif(iManuReassigned == 0.0) then
            
            --Dialogue.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Unit 771852.[P] Have you any further questions for me?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Erm, no?[B][C]") ]])
            
            --"Ending" case.
            if(iManuMetForegolemAsGolem == 1.0) then
                fnCutscene([[ Append("Lord:[E|Neutral] Very good. Enjoy your stay.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] (We're not getting anywhere at all with this approach, but I think I have an idea.[P] I should head back to the undercity and speak with 55.)") ]])
                fnCutsceneBlocker()
                fnCutsceneFace("Admin", 0, 1)
            
            --Normal case.
            else
                fnCutscene([[ Append("Lord:[E|Neutral] Very good.[P] Enjoy your stay.") ]])
                fnCutsceneBlocker()
                fnCutsceneFace("Admin", 0, 1)
            end
        
        --Christine is a drone.
        elseif(iManuReassigned == 1.0 and iManuFinale == 0.0) then
            
            --Not on assignment.
            if(iManuDroneTaskB == 0.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] I do not have an assignment for you, Drone 772890.[P] Finish your current assignment.[B][C]") ]])
                fnCutscene([[ Append("772890:[E|Neutral] AFFIRMATIVE.[P] DRONE WILL FINISH CURRENT ASSIGNMENT.") ]])
                fnCutsceneBlocker()
                fnCutsceneFace("Admin", 0, 1)
                
            --Needs to go get the drink.
            elseif(iManuDroneTaskB == 1.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] Drone 772890, acquire a Fizzy Pop! canister and deliver it to me.[B][C]") ]])
                fnCutscene([[ Append("772890:[E|Neutral] AFFIRMATIVE.[P] DRONE WILL ACQUIRE FIZZY POP! CANISTER.[P] MOVING OUT.") ]])
                fnCutsceneBlocker()
                fnCutsceneFace("Admin", 0, 1)
                
            --Deliver the drink.
            elseif(iManuDroneTaskB == 2.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskB", "N", 3.0)
                VM_SetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskC", "N", 1.0)
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutscene([[ Append("772890:[E|Neutral] LORD UNIT, THIS UNIT IS DELIVERING A CANISTER OF FIZZY POP! DRINK.[B][C]") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] Thank you, drone.[P] I notice you are not yet hooked into the network.[B][C]") ]])
                fnCutscene([[ Append("772890:[E|Neutral] DRONE NETWORK CONNECTION GREEN.[P] THIS DRONE IS MAINTAINING A LOCAL NETWORK CONNECTION.[B][C]") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] Incorrect.[P] Drone, report to the long-term storage area immediately.[B][C]") ]])
                fnCutscene([[ Append("772890:[E|Neutral] AFFIRMATIVE.[P] MOVING TO LONG-TERM STORAGE FACILITY.") ]])
                fnCutsceneBlocker()
                fnCutsceneFace("Admin", 0, 1)
                
                --Objective handler.
                fnCutscene([[ AL_SetProperty("Clear Objectives") ]])
                fnCutscene([[ AL_SetProperty("Register Objective", "DRONE FUNCTION ASSIGNMENT:") ]])
                fnCutscene([[ AL_SetProperty("Register Objective", "1: MOVE TO LONG-TERM STORAGE FACILITY FOR NETWORK INTEGRATION.") ]])
                
            --Repeat.
            else
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] Drone, report to the long-term storage area for network upgrade.[B][C]") ]])
                fnCutscene([[ Append("772890:[E|Neutral] AFFIRMATIVE.[P] REPORTING TO LONG-TERM STORAGE FACILITY.") ]])
                fnCutsceneBlocker()
                fnCutsceneFace("Admin", 0, 1)
            end
        
        --Post-sequence.
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Ah, hello Christine.[P] What brings you to this sector?[B][C]") ]])
            if(sChristineForm ~= "Golem" and sChristineForm ~= "LatexDrone") then
                fnCutscene([[ Append("Christine:[E|Neutral] You recognize me?[B][C]") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] The entire sector does.[P] I saw you as a drone unit, an electrical girl, a steam droid, a human, a plant, a slime, a raiju...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] Wait, what?[P] Slime, plant?[B][C]") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] I'm afraid I don't know whose memory files they were, the whole thing is still rather fuzzy.[P] But we all shared them.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] (Were they mine from the future?[P] Did...[P] did I get assimilated?)[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] I don't understand, but...[B][C]") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] Maybe we shouldn't dwell on it, then.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] So, how are things in the sector, then?[B][C]") ]])
            else
                fnCutscene([[ Append("Christine:[E|Neutral] How are things in the sector?[B][C]") ]])
            end
            fnCutscene([[ Append("Lord:[E|Neutral] We're all having group sessions to talk about what we remember when we're not keeping our production numbers up.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] I've gotten several mails asking me about the explosion.[P] I said we're investigating.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] Got any ideas on how to cover it up?[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Something tells me the Administration already knows, so it'll be a formality either way.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] We can play dumb, but Mirabelle didn't get here on its own.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Do you have any memories on the initial setup time for Project Mirabelle?[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] No, in fact, the past few months are a haze.[P] Someone might, and I hope we find out more at our group sessions.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] If we manage to dig up the servers, I'll let you know if we find something.[P] We have to keep our production quota up, but what labour we can spare goes to the unofficial dig teams.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Is there any opposition to the resistance movement in the sector?[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] None.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] I speak for everyone.[P] I know that, somehow.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Sharing minds isn't all bad, right?[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Sector 99 will protect its own.[P] We're with you, Christine, 2855.") ]])
        end
        
    elseif(sActorName == "LordA") then
        if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Lord:[VOICE|GolemLord] I am currently maintaining personnel files.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("LordA", 0, 1)
        elseif(iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Lord:[VOICE|GolemLord] I do not require your assistance, drone.[P] Perform your function assignment.[B][C]") ]])
            fnCutscene([[ Append("772890:[VOICE|Christine] AFFIRMATIVE.[P] MOVING TO PERFORM FUNCTION ASSIGNMENT.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("LordA", 0, 1)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Lord:[VOICE|GolemLord] I've got the work ethic of a slave unit, the fashion sense of a lord, and the blissful happiness of a drone.[P] Maybe we should revive Project Mirabelle?") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("LordA", 0, 1)
        end
        
    elseif(sActorName == "LordB") then
        if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Lord:[VOICE|GolemLordB] I am currently updating function assignments.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("LordB", 0, -1)
        elseif(iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Lord:[VOICE|GolemLordB] I do not require your assistance, drone.[P] Perform your function assignment.[B][C]") ]])
            fnCutscene([[ Append("772890:[VOICE|Christine] AFFIRMATIVE.[P] MOVING TO PERFORM FUNCTION ASSIGNMENT.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("LordB", 0, -1)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Lord:[VOICE|GolemLordB] We have to keep productive, both to keep from generating suspicion, and because our slave unit sisters need us to.[B][C]") ]])
            fnCutscene([[ Append("Lord:[VOICE|GolemLordB] To think of all the times we failed them because of our own vanity...[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] What about switching tasks with them?[P] Can't they do administrative work?[B][C]") ]])
            fnCutscene([[ Append("Lord:[VOICE|GolemLordB] Oh, of course.[P] We just need to get the passwords all straightened out, and make sure we're ready in case of a surprise inspection.[B][C]") ]])
            fnCutscene([[ Append("Lord:[VOICE|GolemLordB] We'll have to maintain the division of labour a little longer.[P] Believe me, I'd love to be on the factory floor with my friends...") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("LordB", 0, -1)
        end
        
        
    elseif(sActorName == "LordC") then
        if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Lord:[VOICE|GolemLordB] I am ordering additional materials and maintaining invoices.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("LordC", 0, -1)
        elseif(iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Lord:[VOICE|GolemLord] I do not require your assistance, drone.[P] Perform your function assignment.[B][C]") ]])
            fnCutscene([[ Append("772890:[VOICE|Christine] AFFIRMATIVE.[P] MOVING TO PERFORM FUNCTION ASSIGNMENT.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("LordC", 0, -1)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Lord:[VOICE|GolemLord] We have unofficially set our break room to be used by the slave units.[P] If an inspection comes by, we just pretend we were getting them to clean it.[B][C]") ]])
            fnCutscene([[ Append("Lord:[VOICE|GolemLord] I feel just awful for all the times I yelled at them.[P] What was wrong with me?[P] They're thinking, feeling machines just like us...[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] Are you saying you didn't know?[B][C]") ]])
            fnCutscene([[ Append("Lord:[VOICE|GolemLord] I did, but...[P] I just... [B][C]") ]])
            fnCutscene([[ Append("Lord:[VOICE|GolemLord] I'm so sorry.[P] I'll just keep working and maybe they'll forgive me.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("LordC", 0, -1)
        end
        
    elseif(sActorName == "GolemA") then
        if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Special instructions are received through work terminals.[P] I am making sure the members of my shift receive their instructions.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemA", 0, -1)
        elseif(iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Function assignment for Drone 772890 -[P] checks are green.[P] Follow instructions on your HUD, drone.[B][C]") ]])
            fnCutscene([[ Append("772890:[VOICE|Christine] AFFIRMATIVE, DRONE WILL COMPLETE ASSIGNMENTS ON HUD.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemA", 0, -1)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] The past few months are kind of a blur, all my memory files are fragmented.[P] But I know you helped us, Christine.[P] Thank you.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemA", 0, -1)
        end
        
        
    elseif(sActorName == "GolemB") then
        if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] The transit system is experiencing an interruption.[P] We must organize these crates for fast deployment when transit resumes.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemB", 1, 0)
        elseif(iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Good units maintain maximum productivity.[P] Resume your assignments, drone.[B][C]") ]])
            fnCutscene([[ Append("772890:[VOICE|Christine] AFFIRMATIVE, DRONE WILL MAXIMIZE PRODUCTIVITY.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemB", 1, 0)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] The more things change, the more they stay the same.[P] I'm still packing shipping crates, but at least we have a sense of community now.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Also, there's no stigma to dating between model variants anymore.[P] Ooooh, I hope she says yes...[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] Who?[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] As if I'd tell you![P] She's all mine!") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemA", 0, -1)
        end
        
    elseif(sActorName == "GolemC") then
        if(iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] These parts must be sent as soon as the transit system is cleared.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemC", -1, 0)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Explosions, hive minds.[P] My job is still to keep everything marked and sorted!") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemC", -1, 0)
        
        end
        
    elseif(sActorName == "GolemD") then
        if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Visitors should not cross the yellow lines unless appropriately hooked to the local network.[P] Please have your badge visible at all times.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemD", 0, 1)
        elseif(iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Do not cross the yellow lines until your hair has been removed, drone.[B][C]") ]])
            fnCutscene([[ Append("772890:[VOICE|Christine] AFFIRMATIVE, DRONE WILL NOT CROSS YELLOW LINES.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemD", 0, 1)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Hey, you were a drone for a while, right?[P] If you want to be a drone again, we'd love to have you![B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] AFFIRMATIVE, DRONE THANKS YOU FOR THE OFFER.[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] But it'll have to wait.[P] To freedom!") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemD", 0, 1)
        end
        
    elseif(sActorName == "Inspector") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Carry out your function assignment, drone.") ]])
        fnCutsceneBlocker()
    end
end
