-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
-- |[Examinables]|
if(sObjectName == "Machines") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Spare processing machines.[P] Unlike the rest of this stuff, these are probably for the sector itself and not for shipping out.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Tubes") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Organic containment tubes, also used to study fluid dynamics.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Equipment") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Fabricator benches and replacement parts boxes.[P] Presumably these will be shipped out at some point.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "FizzyPop") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Fizzy Pop! machines made in this sector, ready to deliver unbridled joy followed by sugar withdrawal to Regulus City!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Terminals") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A wall of unused terminals ready to be shipped out.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Lights") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Surface lights.[P] There's no power supply in them.)") ]])
    fnCutsceneBlocker()
    
-- |[Other]|
-- |[Skillbook]|
elseif(sObjectName == "Skillbook") then
    AL_SetProperty("Set Layer Disabled", "SkillbookIco", true)
	LM_ExecuteScript(gsTiffanySkillbook, 0)

-- |[Debug]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
