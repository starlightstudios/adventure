-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--Variables.
local iManuReassigned         = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuReassigned", "N")
local iManuDroneTaskA         = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskA", "N")
local iManuDroneTaskB         = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskB", "N")
local iManuDroneTaskC         = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskC", "N")
local iManufactoryDepopulated = VM_GetVar("Root/Variables/Chapter5/Scenes/iManufactoryDepopulated", "N")
local iManuFinale             = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuFinale", "N")

-- |[Exits]|
if(sObjectName == "ToManufactoryG") then

    --Variables.
    local iManufactoryDepopulated = VM_GetVar("Root/Variables/Chapter5/Scenes/iManufactoryDepopulated", "N")
    
    --Not able to open yet.
    if(iManufactoryDepopulated == 0.0) then
        
        --Normal case:
        if(iManuDroneTaskC < 2.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("[SOUND|World|AutoDoorFail] Access to automated long-term storage facility requires Class B authorization.[P] Access denied.") ]])
            fnCutsceneBlocker()
        
        --Bonus scene:
        else

            --Fade to black, cut music.
            fnCutscene([[ AL_SetProperty("Music", "NULL") ]])
            fnCutscene([[ AL_SetProperty("Activate Fade", 15, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()

            --Wait a bit.
            fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
            fnCutsceneWait(25)
            fnCutsceneBlocker()

            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("772890:[VOICE|Christine] UNIT 772890 PRESENTING FOR NETWORK INTEGRATION.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Good.[P] Unit, state your purpose.[B][C]") ]])
            fnCutscene([[ Append("772890:[VOICE|Christine] PRESENTING FOR NETWORK INTEGRATION.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Incorrect.[P] You were sent to spy on us.[P] What was your original designation?[B][C]") ]])
            fnCutscene([[ Append("772890:[VOICE|Christine] QUERY NOT UNDERSTOOD.[P] UNIT 772890 PRESENTING FOR NETWORK INTEGRATION.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Fine.[P] We'll take the information from your memory banks directly.[P] Drone, open CPU casing.[B][C]") ]])
            fnCutscene([[ Append("772890:[VOICE|Christine] CASING OPENED.[P] PROCEED.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Scene.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Narrator] Drone 772890 stood unflinching as a trio of golems surrounded her.[P] One produced a small device.[P] The drone could not identify it.[P] The drone continued to stand at attention.[B][C]") ]])
            fnCutscene([[ Append("Thought:[VOICE|Narrator] The golem placed the device on the drone's synaptic spike, and closed her CPU casing.[P] The device activated.[B][C]") ]])
            fnCutscene([[ Append("Thought:[VOICE|Narrator] All at once, Christine recalled who she was.[P] She felt unusual, as all of her memories surged back.[P] She was...[P] calm.[P] Organized.[P] Disciplined.[P] She was not concerned.[B][C]") ]])
            fnCutscene([[ Append("Thought:[VOICE|Narrator] The memories kept flowing.[P] Every detail of her life.[P] Childhood, becoming a golem, coming to Sector 99.[P] She was remembering all of it vividly.[P] Why?[B][C]") ]])
            fnCutscene([[ Append("Thought:[VOICE|Narrator] Because we needed the memories.[P] Christine understood.[P] The memories were being replayed and stored elsewhere.[P] The collective needed them to understand who she was and what role she needed to fill.[B][C]") ]])
            fnCutscene([[ Append("Thought:[VOICE|Narrator] The trio of golems returned to the factory floor as Christine continued uploading her memory data.[P] It took a few minutes to finish, while she stood at attention and waited.[B][C]") ]])
            fnCutscene([[ Append("Thought:[VOICE|Narrator] The upload completed and Christine felt new instructions enter her mind.[P] These were not her thoughts, these were override instructions downloaded via her synaptic spike.[P] She expressed alarm.[P] Her alarm was sent along the synaptic spike, as well.[B][C]") ]])
            fnCutscene([[ Append("Thought:[VOICE|Narrator] She was told not to be alarmed, and the feeling subsided.[P] Everything was fine, this was quite common in new members.[P] She was now part of a group mind, that was where the thoughts were coming from.[B][C]") ]])
            fnCutscene([[ Append("Thought:[VOICE|Narrator] Christine informed the group mind of her upcoming revolution, her friend 55, her long-term goals.[P] The group mind absorbed the data.[P] She could feel it adjusting itself to the new unit's personality.[B][C]") ]])
            fnCutscene([[ Append("Thought:[VOICE|Narrator] She informed it of the threats she had encountered, her combat experience, her knowledge of Earth.[P] It adjusted further still.[B][C]") ]])
            fnCutscene([[ Append("Thought:[VOICE|Narrator] At last, the synchronization was completed.[P] The group mind had integrated her.[P] She forgot her own name, no longer needing it.[P] She was not Christine, nor was she 771852, a drone, or anyone else.[P] She was merely another node.[B][C]") ]])
            fnCutscene([[ Append("Thought:[VOICE|Narrator] The node could see and hear the other nodes in the network, understand their thoughts.[P] She worked to process the information they passed back, help with decisions, come to conclusions based on evidence.[B][C]") ]])
            fnCutscene([[ Append("Thought:[VOICE|Narrator] At the node's recommendation, the group sent fifty nodes to capture Unit 2855.[P] Even with their foreknowledge, the unit escaped into the undercity.[P] The group mind learned and adapted.[P] She would not escape the next encounter.[B][C]") ]])
            fnCutscene([[ Append("Thought:[VOICE|Narrator] The group mind had cut off direct tram access to protect itself.[P] It was at risk of being discovered due to Katarina's questions.[P] It had made a mistake by increasing productivity unexpectedly.[P] It learned and grew from these errors.[B][C]") ]])
            fnCutscene([[ Append("Thought:[VOICE|Narrator] The node formerly known as 'Christine' was sent back to Sector 96 to keep up appearances.[P] Contact with Unit 2855 was not re-established.[B][C]") ]])
            fnCutscene([[ Append("Thought:[VOICE|Narrator] The collective tried to maintain secrecy, but the command units had discovered them.[P] Some of their nodes were isolated and lost.[P] This occurred slowly, over the course of several months.[B][C]") ]])
            fnCutscene([[ Append("Thought:[VOICE|Narrator] The collective could not locate its lost nodes.[P] Its inspection of the Regulus City network indicated an attack was coming.[P] Security units were massing.[P] It sealed the surface access zones.[P] The collective prepared.[B][C]") ]])
            fnCutscene([[ Append("Thought:[VOICE|Narrator] The attack came.[P] The collective fought as one.[P] The node formerly known as 'Christine' fought alongside them.[P] She was powerful.[P] But then the collective heard the thing it feared most.[B][C]") ]])
            fnCutscene([[ Append("Thought:[VOICE|Narrator] The killphrase, burnt into its software and unable to be removed by design, shot through the collective.[P] All its nodes went offline.[P] Then, the collective did, as well.[B][C]") ]])
            fnCutscene([[ Append("Thought:[VOICE|Narrator] The node known as 'Christine' was captured and interrogated.[P] The future that was meant to happen, happened, and all was lost.[B][C]") ]])
            fnCutscene([[ Append("Thought:[VOICE|Narrator] Or so it seemed.[B][C]") ]])
            fnCutscene([[ Append("Thought:[VOICE|Narrator] ...") ]])
            fnCutsceneBlocker()
            fnCutsceneTeleport("Christine", 34.75, 8.50)
            fnCutsceneFace("Christine", 0, 1)
            fnCutsceneWait(125)
            fnCutsceneBlocker()
            
            --Fade back in.
            fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
            fnCutsceneBlocker()

            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("772890:[VOICE|Christine] (...[P] UNIT STATUS::[P] ORANGE.[P] SYSTEM SCAN INITIATED.)[B][C]") ]])
            fnCutscene([[ Append("772890:[VOICE|Christine] (UNIDENTIFIED MEMORY FILES LOCATED.[P] NO FILE METADATA AVAILABLE.)[B][C]") ]])
            fnCutscene([[ Append("772890:[VOICE|Christine] (COMMAND UNIT...[P] COMMAND UNIT...)[B][C]") ]])
            fnCutscene([[ Append("772890:[VOICE|Christine] (UNIT PROHIBITED FROM UPLOADING DIAGNOSTIC DATA TO NETWORK::[P] SHARE WITH COMMAND UNIT 2855 IMMEDIATELY.)[B][C]") ]])
            fnCutscene([[ Append("772890:[VOICE|Christine] (UNIT 772890 MUST PROCEED TO SECTOR 99 IMMEDIATELY.[P] OVERRIDE QUIM77T.[P] EXECUTE.)") ]])
            fnCutsceneBlocker()
            
            --Music resumes.
            fnCutscene([[ AL_SetProperty("Music", "LAYER|Manufactory") ]])
            fnCutscene([[ AL_SetProperty("Mandated Music Intensity", 60.0) ]])

        end

    --Scene when opening.
    elseif(iManufactoryDepopulated == 1.0) then
    
        --First time.
        local iManuOpenedStorageDoor = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuOpenedStorageDoor", "N")
        if(iManuOpenedStorageDoor == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iManuOpenedStorageDoor", "N", 1.0)
    
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] You couldn't hack anything beyond this point?[P] No other entrances?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] No, the sector's network security is exemplary.[P] I did not want to risk revealing my position.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Hacker is out, repair unit, you're up![P] PDU?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|PDU] Screwdriver tool.[B][C]") ]])
            fnCutscene([[ Append("*click*[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|PDU] Bridge tool.[B][C]") ]])
            fnCutscene([[ Append("*click*[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] All done![B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] Impressive.[P] How did you bypass the security on the door?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Unscrew the panel and move the electrical wire out of the network auth box and into the motivator box.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] These aren't high-security doors, which have the motivator on the [P]*other*[P] side.[P] This is a civilian door.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Odd.[P] The network security was high, but the physical security was not?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Yeah, that is odd.[P] Maybe whatever they're hiding, they didn't expect anyone to physically enter the area?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Or maybe they didn't think three-dimensionally like I do.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Add it to the pile of oddities.[P] Let's go.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
    
        end
    
        --Common code.
        fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutscene([[ AL_BeginTransitionTo("RegulusManufactoryG", "ENTPOS:ToManufactoryF:N") ]])
        fnCutsceneBlocker()
    end
    
-- |[Examinables]|
elseif(sObjectName == "BrownCrates") then
    
    if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Spare computer parts.[P] Motherboards, chipsets, power supplies.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManuDroneTaskA == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (ACQUIRE PARTS, DELIVER TO THIS AREA.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManuDroneTaskA == 1.0) then
        AudioManager_PlaySound("World|TakeItem")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskA", "N", 2.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (PART DELIVERY COMPLETED.[P] REPORT TO LORD UNIT FOR FURTHER ASSIGNMENT.)") ]])
        fnCutsceneBlocker()
        AL_SetProperty("Clear Objectives")
        AL_SetProperty("Register Objective", "DRONE FUNCTION ASSIGNMENT:")
        AL_SetProperty("Register Objective", "1: REPORT TO LORD UNIT IN FACTORY FLOOR OFFICE FOR FURTHER ASSIGNMENT")
    elseif(iManuReassigned == 1.0 and iManuDroneTaskA > 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (FUNCTION DOES NOT REQUIRE THESE PARTS.)") ]])
        fnCutsceneBlocker()
    end

elseif(sObjectName == "FizzyPop") then
    
    if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Fizzy Pop!, the best way to recharge![P] This is the blue variety, meant for slave units.[P] I assume factory workers go through a lot of energy refills.)") ]])
        fnCutsceneBlocker()
    else
        
        --Not on assignment.
        if(iManuDroneTaskB == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (FIZZY POP.[P] THIS UNIT DOES NOT REQUIRE A RECHARGE. IGNORING.)") ]])
            fnCutsceneBlocker()
        
        --Get dat fizzy.
        elseif(iManuDroneTaskB == 1.0) then
            AudioManager_PlaySound("World|TakeItem")
            AL_SetProperty("Flag Objective True", "1: ACQUIRE FIZZY POP! CANISTER")
            VM_SetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskB", "N", 2.0)
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (A CANISTER OF FIZZY POP HAS BEEN ACQUIRED.[P] DRONE WILL DELIVER CANISTER TO ADMINISTRATIVE LORD GOLEM IN SECTOR 99.)") ]])
            fnCutsceneBlocker()
        
        --Deliver dat fizzy.
        elseif(iManuDroneTaskB == 2.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (CANISTER MUST BE DELIVERED TO ADMINISTRATIVE LORD GOLEM IN SECTOR 99.)") ]])
            fnCutsceneBlocker()
        
        --Other cases.
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (FIZZY POP.[P] THIS UNIT DOES NOT REQUIRE A RECHARGE.[P] IGNORING.)") ]])
            fnCutsceneBlocker()
        end
    end
    
elseif(sObjectName == "Elevators") then
    if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Freight elevators for the underground packing center.[P] You can also access the slave domiciles from here.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (DRONE DOES NOT REQUIRE ACCESS TO PACKING CENTER OR SLAVE DOMICILES.[P] POWER CORE 95pct, RECHARGE IN 53 HOURS.)") ]])
        fnCutsceneBlocker()
    end
        
elseif(sObjectName == "Oilmaker") then
    if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Unflavoured oil...[P] What kind of philistine drinks unflavoured oil!?)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0 and iManuFinale == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (DRONE DOES NOT REQUIRE LUBRICATION OR ENERGY RECHARGE.)") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I know I'm doing the right thing because now the oilmaker has flavour packets next to it.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "RVD") then
    if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('How to be Productive', a very special episode of 'All My Processors'.[P] It's probably the worst episode, but it's set to play on loop.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0 and iManuFinale == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (RVD INTERACTION NOT REQUIRED FOR CURRENT FUNCTION ASSIGNMENT.)") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Unit 2834 is hanging off a cliff by her fingertips with a villain standing over her.[P] The worst part is there's at least six episodes this could be without further context.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "Terminal") then
    if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (This terminal controls the RVD in this room.[P] It's set to play the same episode of 'All My Processors' on loop, and nobody has bothered to change it yet.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0 and iManuFinale == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (TERMINAL CONTROLS RVD.[P] RVD INTERACTION NOT REQUIRED FOR CURRENT FUNCTION ASSIGNMENT.)") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (RVD controls.[P] It has episodes of 'All My Processors' playing.[P] I guess every unit loves the show!)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "NoteA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Long-term storage facility.[P] No access without authorization.[P] Caution::[P] Automated moving parts!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "NoteB") then
    if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Productivity statistics for 6 months ago.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (DRONE IS NOT REQUIRED TO RECORD OR UNDERSTAND PRODUCTIVITY STATISTICS.[P] IGNORING.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "NoteC") then
    if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Productivity statistics for 5 months ago.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (DRONE IS NOT REQUIRED TO RECORD OR UNDERSTAND PRODUCTIVITY STATISTICS.[P] IGNORING.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "NoteD") then
    if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Productivity statistics for 4 months ago.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (DRONE IS NOT REQUIRED TO RECORD OR UNDERSTAND PRODUCTIVITY STATISTICS.[P] IGNORING.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "NoteE") then
    if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Productivity statistics for 3 months ago.[P] This is the most recent chart on the wall, but it's behind by a few months.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (DRONE IS NOT REQUIRED TO RECORD OR UNDERSTAND PRODUCTIVITY STATISTICS.[P] IGNORING.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "Laptop") then
    if(iManuReassigned == 0.0 and iManuFinale == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Might not be a good idea to inspect the lord unit's computer while she's in the room.)") ]])
        fnCutsceneBlocker()
    elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0 and iManuFinale == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (FUNCTION ASSIGNMENT DOES NOT CONCERN LORD'S PORTABLE COMPUTER, IGNORING.)") ]])
        fnCutsceneBlocker()
    elseif(iManuFinale == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The portable computer is blank.[P] Was it wiped, or was the foregolem not actually using it?)") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I think I'll not snoop on the foredrone's laptop.)") ]])
        fnCutsceneBlocker()
    end
    
-- |[Other]|
-- |[Debug]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
