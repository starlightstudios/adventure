-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--Variables.
local iManuReassigned         = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuReassigned", "N")
local iManufactoryDepopulated = VM_GetVar("Root/Variables/Chapter5/Scenes/iManufactoryDepopulated", "N")
local iManuFinale             = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuFinale", "N")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Mandated for story progression.
    if(sActorName == "Foregolem") then
        
        --Variables.
        local iManuMetForegolemAsGolem = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuMetForegolemAsGolem", "N")
        local iManuMetAdminAsGolem     = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuMetAdminAsGolem", "N")
            
        --First meeting case:
        if(iManuMetForegolemAsGolem == 0.0) then
        
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iManuMetForegolemAsGolem", "N", 1.0)
            
            --Dialogue.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Unit 771852, Unit 1111.[P] How may I help you?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] You're the unit in charge of the factory floor?[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Affirmative.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Well, I just came to commend you for your outstanding performance![P] Well done, the Cause needs more units like you.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Thank you, however, I did not do anything outside normal parameters.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] No need to be humble about it.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Though I suppose you could say it's your talented units who did the impressive work, right?[P] How charitable of you![B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] No.[P] They performed their function assignments within normal parameters.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Oh, well.[P] No commendations to give out, then.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] No.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] (She's like 55, but without the charm or charisma.)[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] (No wait, that's mean, to both of them.)[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Actually, I'm a repair unit.[P] Maybe the new machinery you got has a repair manual that's not in my database?[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] We do not have any machinery or manuals not in the standard database.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] Well.[P] Suppose I ought to be off back to my sector.[P] It was very nice meeting you.[B][C]") ]])
            
            --"Ending" case.
            if(iManuMetAdminAsGolem == 1.0) then
                fnCutscene([[ Append("Lord:[E|Neutral] Thank you.[P] Returning to work.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] (We're not getting anywhere at all with this approach, but I think I have an idea.[P] I should head back to the undercity and speak with 55.)") ]])
                fnCutsceneBlocker()
                fnCutsceneFace("Foregolem", 0, 1)
            
            --Normal case.
            else
                fnCutscene([[ Append("Lord:[E|Neutral] Thank you.[P] Returning to work.") ]])
                fnCutsceneBlocker()
                fnCutsceneFace("Foregolem", 0, 1)
            end
        
        --Repeat.
        elseif(iManuMetForegolemAsGolem == 1.0 and iManuReassigned == 0.0) then
            
            --Dialogue.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Unit 771852, Unit 1111.[P] Have you anything to discuss?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Erm, no?[B][C]") ]])
            
            --"Ending" case.
            if(iManuMetAdminAsGolem == 1.0) then
                fnCutscene([[ Append("Lord:[E|Neutral] Thank you for your consideration.[P] I must return to my assignments.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] (We're not getting anywhere at all with this approach, but I think I have an idea.[P] I should head back to the undercity and speak with 55.)") ]])
                fnCutsceneBlocker()
                fnCutsceneFace("Foregolem", 0, 1)
            
            --Normal case.
            else
                fnCutscene([[ Append("Lord:[E|Neutral] Thank you for your consideration.[P] I must return to my assignments.") ]])
                fnCutsceneBlocker()
                fnCutsceneFace("Foregolem", 0, 1)
            end
        
        --Christine is a latex drone.
        elseif(iManuReassigned == 1.0 and iManuFinale == 0.0) then
        
            --Variables.
            local iManuDroneTaskA = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskA", "N")
            local iManuDroneTaskB = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskB", "N")
        
            --First task to-be-completed.
            if(iManuDroneTaskA < 2.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] Drone 772890, acquire the incorrectly assigned parts from the brown crates in the loading bay.[P] Then, deliver them to the store room and store them in the brown crates.[B][C]") ]])
                fnCutscene([[ Append("772890:[E|Neutral] AFFIRMATIVE.[P] CARRYING OUT ORDERS.") ]])
                fnCutsceneBlocker()
                fnCutsceneFace("Foregolem", 0, 1)
            
            --First task completed, receive second task.
            elseif(iManuDroneTaskA == 2.0 and iManuDroneTaskB == 0.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iManuDroneTaskB", "N", 1.0)
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] Drone 772890, acquire a canister of Fizzy Pop! and deliver it to the administrative lord unit in the office block.[B][C]") ]])
                fnCutscene([[ Append("772890:[E|Neutral] AFFIRMATIVE.[P] CARRYING OUT ORDERS.") ]])
                fnCutsceneBlocker()
                fnCutsceneFace("Foregolem", 0, 1)
                
                AL_SetProperty("Clear Objectives")
                AL_SetProperty("Register Objective", "DRONE FUNCTION ASSIGNMENT:")
                AL_SetProperty("Register Objective", "1: ACQUIRE FIZZY POP! CANISTER")
                AL_SetProperty("Register Objective", "2: DELIVER FIZZY POP! CANISTER TO ADMINISTRATIVE LORD")
            
            --First task completed, receive second task.
            elseif(iManuDroneTaskA == 2.0 and iManuDroneTaskB == 1.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] Drone 772890, your assignment is to acquire a canister of Fizzy Pop! and deliver it to the administrative lord unit in the office block.[B][C]") ]])
                fnCutscene([[ Append("772890:[E|Neutral] AFFIRMATIVE.[P] CARRYING OUT ORDERS.") ]])
                fnCutsceneBlocker()
                fnCutsceneFace("Foregolem", 0, 1)
            else
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] Drone, carry out your current function assignment.[B][C]") ]])
                fnCutscene([[ Append("772890:[E|Neutral] AFFIRMATIVE.[P] DRONE WILL COMPLETE CURRENT FUNCTION ASSIGNMENT.") ]])
                fnCutsceneBlocker()
                fnCutsceneFace("Foregolem", 0, 1)
        
            end
        
        --Post-finale.
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Oh, greetings, Christine, 2855.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Are you cleaning the walls?[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Affirmative.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] ...[P] First time for everything?[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] My hands are just as capable as a drone's or a slave unit's.[P] We rotate tasks.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Don't worry, if there's a surprise inspection, I'll head back into the foregolem's office and watch the production line as I used to.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Laugh] Great![P] This is how it ought to be![B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Maybe it's my memories being fuzzy, but I recall something about you doing that in your sector?[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] I'm not used to manual labour, but I find it oddly refreshing to just do something with your hands for a while.[P] It feels good.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] In short bursts, anyway.[P] A machine's life should be balanced.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] By the way, am I recalling right that you're dating a slave unit?[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Sorry if I'm being too forward, my fuzzy memories seem to indicate 771852 having a reputation for that.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] ...[P] Yes?[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] How do you go about asking one out?[P] The...[P] the unit who works at the loading dock...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Blush] Be honest and upfront.[P] You have nothing to be ashamed of, love is love.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Yeah.[P] It's going to take a while to shake off years of conditioning.[P] I can't believe I put my feelings aside for such a petty reason as model variant.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Thanks, Christine.[P] For everything.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("Foregolem", 0, -1)
            
        end
        
    elseif(sActorName == "GolemA") then
        if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Oil is a useful way to recharge a depleted core, though not as efficient as a defragmentation pod.[P] This unit is consuming oil.[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] (All the oil is unflavoured.[P] Don't the golems here like any of the flavours?)") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemA", 0, -1)
        elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Oil is a useful way to recharge a depleted core, though not as efficient as a defragmentation pod.[P] This unit is consuming oil.[B][C]") ]])
            fnCutscene([[ Append("772890:[VOICE|Christine] AFFIRMATIVE.[P] DRONE 772890 DOES NOT REQUIRE RECHARGING.[P] RETURNING TO ASSIGNMENT.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemA", 0, -1)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Oil flavour packets...[P] mmmm...[P] tungsten![P] Getting assimilated into a hive mind was the best thing to happen to me!") ]])
            fnCutsceneBlocker()
        end
        
    elseif(sActorName == "GolemB") then
        if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] I am watching a popular show. It is called 'All My Processors'.[P] I enjoy it.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemB", 0, -1)
        elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] I am watching a popular show. It is called 'All My Processors'.[P] I enjoy it.[B][C]") ]])
            fnCutscene([[ Append("772890:[VOICE|Christine] AFFIRMATIVE.[P] ENJOYMENT NOT REQUIRED FOR DRONE EFFICIENCY.[P] RETURNING TO ASSIGNMENT.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemB", 0, -1)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] 'All My Processors' is the one constant throughout the years.[P] No matter what happens, I still have no idea what the hell is going on in this show.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Though I had a great idea for an episode.[P] Unit 593490 and Unit 399421 get assimilated into a hive mind...") ]])
            fnCutsceneBlocker()
        end
        
    elseif(sActorName == "GolemC") then
        if(iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Fizzy Pop! is a useful way to recharge a depleted core, though not as efficient as a defragmentation pod.[P] This unit is consuming Fizzy Pop!") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemC", 0, -1)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] If there's one thing everyone::[P] Steam droid, golem, lord, drone, can bond over, it's our shared addiction to Fizzy Pop!") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemC", 0, -1)
        
        end
        
    elseif(sActorName == "GolemD") then
        if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Materials are stored below and transferred for unloading here.[P] I am making sure nothing jams the line.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemD", 0, 1)
        elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Materials are stored below and transferred for unloading here.[P] I am making sure nothing jams the line.[B][C]") ]])
            fnCutscene([[ Append("772890:[VOICE|Christine] DRONE INQUIRES IF ASSISTANCE IS REQUIRED.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Negative.[P] Return to your existing assignment, drone.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemD", 0, 1)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] We rotate tasks now, but I like to get this job when I can.[P] Watching the crates go by is just so relaxing...") ]])
            fnCutsceneBlocker()
        end
        
    elseif(sActorName == "GolemE") then
        if(iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] I am observing production statistics.[P] It is important to notice production trends and improve on them.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemE", 0, -1)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] The production statistics haven't been updated in months.[P] My guess is that's when this whole thing began.[P] Hopefully we'll find out more if there's anything left on the blown-out servers.") ]])
            fnCutsceneBlocker()
        end
        
    elseif(sActorName == "GolemF") then
        if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Visitors should not cross the yellow lines unless they are connected to the network and properly programmed.[P] Please stand back.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemF", 0, 1)
        elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Drone 772890's hair is out of standard.[P] Do not cross the yellow markers unless explicitly ordered.[B][C]") ]])
            fnCutscene([[ Append("772890:[VOICE|Christine] AFFIRMATIVE.[P] DRONE WILL REMAIN BEHIND YELLOW MARKERS.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemF", 0, 1)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Oh, Christine, 2855![P] Sorry, but you'd still best stay behind the lines.[P] Wouldn't want to get in the way of our production line.[P] We're even more efficient now that we work as a team!") ]])
            fnCutsceneBlocker()
        end
        
    elseif(sActorName == "GolemG") then
        if(iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] My task is to report errors or defects in the production line.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemG", 0, -1)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] My task is to report errors or defects in the production line...[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Ha![P] Gotcha![P] I remember saying that really clearly, but not much else.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("GolemG", 0, -1)
        
        end
    elseif(sActorName == "DroneA") then
        if(iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Drone:[VOICE|LatexDrone] THIS UNIT IS BEING INSPECTED FOR FLAWS.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("DroneA", 0, 1)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Drone:[VOICE|LatexDrone] Sometimes I want to just turn my inhibitor chip back on and slip away, but then I'd be letting down all my fellow units.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("DroneA", 0, 1)
        end
        
    elseif(sActorName == "DroneB") then
        if(iManuReassigned == 0.0 or iManufactoryDepopulated == 1.0 and iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Drone:[VOICE|LatexDrone] THIS UNIT IS PERFORMING AN INSPECTION OF A FELLOW DRONE.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("DroneB", 1, 0)
        elseif(iManuReassigned == 1.0 and iManufactoryDepopulated == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Drone:[VOICE|LatexDrone] THIS UNIT IS PERFORMING AN INSPECTION OF A FELLOW DRONE.[B][C]") ]])
            fnCutscene([[ Append("772890:[VOICE|Christine] EVALUATION.[P] DRONE TECHNIQUE RATING::[P] SPIFFY.[P] LOGGING RESULTS WITH CENTRAL SERVER.[B][C]") ]])
            fnCutscene([[ Append("Drone:[VOICE|LatexDrone] AFFIRMATIVE.[P] THIS UNIT APPRECIATES EVALUATION.[P] RESUMING.[B][C]") ]])
            fnCutscene([[ Append("772890:[VOICE|Christine] (DRONE EMOTION::[P] ENJOYMENT.[P] CAUSE::[P] HIGH-QUALITY INSPECTION TECHNIQUE OBSERVED.[P] LOGGED WITH CENTRAL SERVER.[P] RESUMING TASK.)") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("DroneB", 1, 0)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Drone:[VOICE|LatexDrone] Inspecting drones is the best job...[P] I'm so lucky!") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("DroneB", 1, 0)
        end
        
    elseif(sActorName == "DroneC") then
        if(iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Drone:[VOICE|LatexDrone] THIS UNIT IS CLEANING DUST FROM THE WALLS.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("DroneC", 0, -1)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Drone:[VOICE|LatexDrone] THIS UNIT IS UNABLE TO FUNCTION WITHOUT ITS INHIBITOR CHIP ACTIVATED.[P] THIS UNIT WILL MOP THE FLOORS AS ORDERED.[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] (I wonder if she was forced to become a drone after having her CPU badly damaged...)") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("DroneC", 0, -1)
        end
    elseif(sActorName == "DroneD") then
        if(iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Drone:[VOICE|LatexDrone] THIS UNIT IS CLEANING DUST FROM THE WALLS.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("DroneD", 0, -1)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Drone:[VOICE|LatexDrone] I was elected foregolem almost unanimously.[P] With my inhibitor chip turned off, I'm a capable organizer.[B][C]") ]])
            fnCutscene([[ Append("55:[VOICE|Tiffany] That means you were likely a revolutionary who was repurposed as punishment.[B][C]") ]])
            fnCutscene([[ Append("Drone:[VOICE|LatexDrone] That's what I had guessed, but I can't remember anything before becoming a drone.[B][C]") ]])
            fnCutscene([[ Append("Drone:[VOICE|LatexDrone] When we win our freedom, I hope all the others like me can find out who we were.[B][C]") ]])
            fnCutscene([[ Append("55:[VOICE|Tiffany] If the data exists, we will find it for you.[B][C]") ]])
            fnCutscene([[ Append("Drone:[VOICE|LatexDrone] Thank you, 2855, Christine.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("DroneD", 0, 1)
        
        end
        
    elseif(sActorName == "LordA") then
        if(iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Lord:[VOICE|GolemLord] I am relaxing with my fellow units.[P] Breaks improve productivity.") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("LordA", 0, -1)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Lord:[VOICE|GolemLord] I never frequented the slave break room before, but now?[P] We simply *must* get this place redecorated!") ]])
            fnCutsceneBlocker()
        
        end
    end
end
