-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--Variables.
local iManuFinale = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuFinale", "N")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    if(sActorName == "GolemA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] We breed shrimp and transport them to Sector 82, where they are ground up and frozen.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Organic rations?[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Affirmative.[P] When treated, they never rot if stored correctly.[P] They are mostly fed to raijus who produce power.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] I thought that food production mostly took place in the biolabs.[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] As I understand it, they've been retooling their aquatic habitats for research, so our sector was reassigned to food production.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] I see.[P] Good work, unit.[P] The city thanks you.[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Thank you, lord unit.") ]])
        
    elseif(sActorName == "GolemB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Is my tandem unit in her domicile -[P] oh![P] Lord unit![P] I was - [B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Performing a vital function for the city.[P] Carry on, friend.[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Y-[P]yes, lord unit!") ]])
        
    elseif(sActorName == "GolemC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I am taking inventory, lord unit.[P] If you need ground access to Sector 99, I think that ladder will take you there.[P] Eventually.[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] ...[P] At least the pits have guard rails down there...") ]])
        
    elseif(sActorName == "GolemD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Work is currently underway to restore transit access.[P] Please remain behind the yellow line unless you have received the proper programming.") ]])
        
    elseif(sActorName == "LordA") then
        if(iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Lord:[VOICE|GolemLord] It's not very often you witness a transit crash.[P] Do you think it hopped a rail?") ]])
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Lord:[VOICE|GolemLord] It will be some time before the tunnel is clear, so I've been sure to prepare delicacies for the lord units who may be forced to walk to Sector 99.") ]])
            
        end
        
    elseif(sActorName == "LordB") then
        if(iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Lord:[VOICE|GolemLordC] What a day![P] We've got so many diverse units visiting our little sector.") ]])
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Lord:[VOICE|GolemLordC] Care for a shrimp candy?[P] They're delicious![P] Perhaps it was my calling to prepare treats...") ]])
            
        end
        
    elseif(sActorName == "LordC") then
        if(iManuFinale == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Lord:[VOICE|GolemLord] I've been informed relief trams are on the way.[P] Please make yourself comfortable in my sector's break room if you like.") ]])
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Lord:[VOICE|GolemLord] Another day, another six-thousand forms to fill out![P] Golem life is the best life!") ]])
            
        end
    elseif(sActorName == "HumanA") then
        
        --Variables.
        local iKnowsAboutBreedingProgram = VM_GetVar("Root/Variables/Global/Christine/iKnowsAboutBreedingProgram", "N")
        
        --First time.
        if(iKnowsAboutBreedingProgram == 0.0) then
            VM_SetVar("Root/Variables/Global/Christine/iKnowsAboutBreedingProgram", "N", 1.0)
            
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] A human?[P] What are you doing here?[B][C]") ]])
            fnCutscene([[ Append("Human:[VOICE|HumanF0] Breeding program work-experience, lord unit![P] I'm so excited to be taken in by Unit 544932![B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] Oh my...[P] What if you had been on that tram...[B][C]") ]])
            fnCutscene([[ Append("Human:[VOICE|HumanF0] Don't worry, lord unit.[P] I've got my vac survival equipment.[P] Never board a tram without it![B][C]") ]])
            fnCutscene([[ Append("Human:[VOICE|HumanF0] ...[P] The tram's software actually won't begin departure unless I scan it and verify it before boarding.[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] Would you have time to put it on in an emergency?[B][C]") ]])
            fnCutscene([[ Append("Human:[VOICE|HumanF0] You have to wear the facemask in the tram, and we drill on putting them on in an emergency.[P] I can get mine on in under two seconds.[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] And the ambient radiation?[P] Heat from exposure to sunlight during Regulan daytime?[B][C]") ]])
            fnCutscene([[ Append("Human:[VOICE|HumanF0] Portable vacsuit can be assembled and worn in under thirty seconds, lord unit![P] We practice it every three days![B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] That kind of dedication means you'll make a superb unit.[P] Good luck with your graduation assignment![B][C]") ]])
            fnCutscene([[ Append("Human:[VOICE|HumanF0] Thank you, lord unit![P] I'm hoping to make Sector Administrator if I can!") ]])

        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] Human, can you tell me about your procedures for unexpected depressurization?[B][C]") ]])
            fnCutscene([[ Append("Human:[VOICE|HumanF0] I've got my vac survival equipment.[P] Never board a tram without it![B][C]") ]])
            fnCutscene([[ Append("Human:[VOICE|HumanF0] ...[P] The tram's software actually won't begin departure unless I scan it and verify it before boarding.[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] Would you have time to put it on in an emergency?[B][C]") ]])
            fnCutscene([[ Append("Human:[VOICE|HumanF0] You have to wear the facemask in the tram, and we drill on putting them on in an emergency.[P] I can get mine on in under two seconds.[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] And the ambient radiation?[P] Heat from exposure to sunlight during Regulan daytime?[B][C]") ]])
            fnCutscene([[ Append("Human:[VOICE|HumanF0] Portable vacsuit can be assembled and worn in under thirty seconds, lord unit![P] We practice it every three days![B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] That kind of dedication means you'll make a superb unit.[P] Good luck with your graduation assignment![B][C]") ]])
            fnCutscene([[ Append("Human:[VOICE|HumanF0] Thank you, lord unit![P] I'm hoping to make Sector Administrator if I can!") ]])
        end
    elseif(sActorName == "GolemTramA") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Our lord golem is going to kill us for being late...") ]])
    elseif(sActorName == "GolemTramB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] What do you think happened?[P] Sabotage?[P] Accident?") ]])
    elseif(sActorName == "GolemTramC") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lord:[VOICE|GolemLord] I bet it was a glitch and there isn't even a real problem.") ]])
    elseif(sActorName == "GolemTramD") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lord:[VOICE|GolemLordC] Oh dear, these boots are new and now I have to get surface dust on them?") ]])
    elseif(sActorName == "GolemTramE") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Lord golem, shall we get going?") ]])
    end
end
