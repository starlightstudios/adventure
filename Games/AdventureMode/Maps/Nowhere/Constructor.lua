-- |[ ====================================== Constructor ======================================= ]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "Nowhere"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

    -- |[ ======================== System ======================== ]|
	--Music.
	AL_SetProperty("Music", "Null")
	
	--Chest path.
	--DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)
	
	--Create the default character.
	TA_Create("Septima")
		giPartyLeaderID = RO_GetID()
		TA_SetProperty("Position", 13, 4)
		TA_SetProperty("Facing", gci_Face_South)
		fnSetCharacterGraphics("Root/Images/Sprites/Septima/", true)
	DL_PopActiveObject()
	
	--Order the player to take control of the party leader.
	AL_SetProperty("Player Actor ID", giPartyLeaderID)
	
	--Map Setup
	fnResolveMapLocation("Nowhere")
	
    -- |[ ===================== NPC Handling ===================== ]|
    -- |[Load Assets]|
    --Order a load action.
    fnLoadDelayedBitmapsFromList("Nowhere Portraits", gciDelayedLoadLoadAtEndOfTick)
    
	-- |[Chapter 1]|
    --Spawn NPC.
    TA_Create("Mei")
        TA_SetProperty("Position", 13, 6)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", true)
        fnSetCharacterGraphics("Root/Images/Sprites/Mei_Human/", true)
        TA_SetProperty("Activation Script", gsRoot .. "ChapterSelectDialogue/Chapter1.lua")
        
        --Special frames.
        TA_SetProperty("Wipe Special Frames")
        TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/Mei|Wounded")
        TA_SetProperty("Add Special Frame", "Crouch",  "Root/Images/Sprites/Special/Mei|Crouch")
    DL_PopActiveObject()
    
    --Reset Mei's dialogue portraits to human.
    DialogueActor_Push("Mei")
        DialogueActor_SetProperty("Add Emotion", "Neutral",  "Root/Images/Portraits/MeiDialogue/HumanNeutral", true)
        DialogueActor_SetProperty("Add Emotion", "Smirk",    "Root/Images/Portraits/MeiDialogue/HumanSmirk", true)
        DialogueActor_SetProperty("Add Emotion", "Happy",    "Root/Images/Portraits/MeiDialogue/HumanHappy", true)
        DialogueActor_SetProperty("Add Emotion", "Blush",    "Root/Images/Portraits/MeiDialogue/HumanBlush", true)
        DialogueActor_SetProperty("Add Emotion", "Sad",      "Root/Images/Portraits/MeiDialogue/HumanSad", true)
        DialogueActor_SetProperty("Add Emotion", "Surprise", "Root/Images/Portraits/MeiDialogue/HumanSurprise", true)
        DialogueActor_SetProperty("Add Emotion", "Offended", "Root/Images/Portraits/MeiDialogue/HumanOffended", true)
        DialogueActor_SetProperty("Add Emotion", "Cry",      "Root/Images/Portraits/MeiDialogue/HumanCry", true)
        DialogueActor_SetProperty("Add Emotion", "Laugh",    "Root/Images/Portraits/MeiDialogue/HumanLaugh", true)
        DialogueActor_SetProperty("Add Emotion", "Angry",    "Root/Images/Portraits/MeiDialogue/HumanAngry", true)
        DialogueActor_SetProperty("Add Emotion", "MC",       "Root/Images/Portraits/MeiDialogue/HumanMC", true)
    DL_PopActiveObject()
    
    --NC+ Chapter 1:
    local iChapter1 = VM_GetVar("Root/Variables/Global/ChapterComplete/iChapter1", "N")
    if(iChapter1 < 1.0) then
        AL_SetProperty("Set Layer Disabled", "NCPlusChapter1", true)
    else
        AL_SetProperty("Set Layer Disabled", "NCPlusChapter1", false)
    end
	
    -- |[Chapter 2]|
	--Sanya, represents Chapter 2.
	TA_Create("Sanya")
		TA_SetProperty("Position", 7, 8)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Clipping Flag", true)
		fnSetCharacterGraphics("Root/Images/Sprites/Sanya_Human/", true)
		TA_SetProperty("Activation Script", gsRoot .. "ChapterSelectDialogue/Chapter2.lua")
	DL_PopActiveObject()
    DialogueActor_Push("Sanya")
        DialogueActor_SetProperty("Add Emotion", "Neutral",  "Root/Images/Portraits/SanyaDialogue/HumanNoRifleNeutral", true)
    DL_PopActiveObject()
	
    -- |[Chapter 3]|
	--Jeanne, represents Chapter 3.
	TA_Create("Jeanne")
		TA_SetProperty("Position", 9, 10)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Clipping Flag", true)
		fnSetCharacterGraphics("Root/Images/Sprites/Jeanne_Human/", true)
		TA_SetProperty("Activation Script", gsRoot .. "ChapterSelectDialogue/Chapter3.lua")
	DL_PopActiveObject()
	
    -- |[Chapter 4]|
	--Lotta, represents Chapter 4.
	TA_Create("Lotta")
		TA_SetProperty("Position", 17, 10)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Clipping Flag", true)
		fnSetCharacterGraphics("Root/Images/Sprites/Lotta_Human/", true)
		TA_SetProperty("Activation Script", gsRoot .. "ChapterSelectDialogue/Chapter4.lua")
	DL_PopActiveObject()
	
    -- |[Chapter 5]|
    --If the player has never seen Christine before, she is replaced by Chris.
    local iShowChristine = VM_GetVar("Root/Variables/Global/ChapterComplete/iChapter5", "N")
    
    --In all cases the NPC's internal name is Christine.
    TA_Create("Christine")
        TA_SetProperty("Position", 19, 8)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", true)
        
        --Use Christine's sprites:
        if(iShowChristine == 1.0) then
            fnSetCharacterGraphics("Root/Images/Sprites/Christine_Human/", true)
        --Use Chris':
        else
            fnSetCharacterGraphics("Root/Images/Sprites/Christine_Male/", true)
        end
        TA_SetProperty("Activation Script", gsRoot .. "ChapterSelectDialogue/Chapter5.lua")
    DL_PopActiveObject()
    
    --Reset the dialogue portraits based on whether Chris or Christine is being used.
    if(iShowChristine == 1.0) then
        DialogueActor_Push("Christine")
            DialogueActor_SetProperty("Add Emotion", "Neutral",  "Root/Images/Portraits/ChristineDialogue/Neutral", true)
            DialogueActor_SetProperty("Add Emotion", "Happy",    "Root/Images/Portraits/ChristineDialogue/Happy", true)
            DialogueActor_SetProperty("Add Emotion", "Blush",    "Root/Images/Portraits/ChristineDialogue/Blush", true)
            DialogueActor_SetProperty("Add Emotion", "Smirk",    "Root/Images/Portraits/ChristineDialogue/Smirk", true)
            DialogueActor_SetProperty("Add Emotion", "Sad",      "Root/Images/Portraits/ChristineDialogue/Sad", true)
            DialogueActor_SetProperty("Add Emotion", "Scared",   "Root/Images/Portraits/ChristineDialogue/Scared", true)
            DialogueActor_SetProperty("Add Emotion", "Offended", "Root/Images/Portraits/ChristineDialogue/Offended", true)
            DialogueActor_SetProperty("Add Emotion", "Serious",  "Root/Images/Portraits/ChristineDialogue/Serious", true)
            DialogueActor_SetProperty("Add Emotion", "Cry",      "Root/Images/Portraits/ChristineDialogue/Cry", true)
            DialogueActor_SetProperty("Add Emotion", "Laugh",    "Root/Images/Portraits/ChristineDialogue/Laugh", true)
            DialogueActor_SetProperty("Add Emotion", "Angry",    "Root/Images/Portraits/ChristineDialogue/Angry", true)
            DialogueActor_SetProperty("Add Emotion", "PDU",      "Root/Images/Portraits/ChristineDialogue/PDU", true)
        DL_PopActiveObject()
    
    --Chris:
    else
        DialogueActor_Push("Christine")
            DialogueActor_SetProperty("Add Emotion", "Neutral",  "Root/Images/Portraits/ChristineDialogue/Male|Neutral", true)
            DialogueActor_SetProperty("Add Emotion", "Happy",    "Root/Images/Portraits/ChristineDialogue/Male|Neutral", true)
            DialogueActor_SetProperty("Add Emotion", "Blush",    "Root/Images/Portraits/ChristineDialogue/Male|Neutral", true)
            DialogueActor_SetProperty("Add Emotion", "Smirk",    "Root/Images/Portraits/ChristineDialogue/Male|Neutral", true)
            DialogueActor_SetProperty("Add Emotion", "Sad",      "Root/Images/Portraits/ChristineDialogue/Male|Neutral", true)
            DialogueActor_SetProperty("Add Emotion", "Scared",   "Root/Images/Portraits/ChristineDialogue/Male|Neutral", true)
            DialogueActor_SetProperty("Add Emotion", "Offended", "Root/Images/Portraits/ChristineDialogue/Male|Neutral", true)
            DialogueActor_SetProperty("Add Emotion", "Serious",  "Root/Images/Portraits/ChristineDialogue/Male|Neutral", true)
            DialogueActor_SetProperty("Add Emotion", "Cry",      "Root/Images/Portraits/ChristineDialogue/Male|Neutral", true)
            DialogueActor_SetProperty("Add Emotion", "Laugh",    "Root/Images/Portraits/ChristineDialogue/Male|Neutral", true)
            DialogueActor_SetProperty("Add Emotion", "Angry",    "Root/Images/Portraits/ChristineDialogue/Male|Neutral", true)
            DialogueActor_SetProperty("Add Emotion", "PDU",      "Root/Images/Portraits/ChristineDialogue/Male|Neutral", true)
        DL_PopActiveObject()
    end
    
    --NC+ Layer:
    local iChapter5 = VM_GetVar("Root/Variables/Global/ChapterComplete/iChapter5", "N")
    if(iChapter5 < 1.0) then
        AL_SetProperty("Set Layer Disabled", "NCPlusChapter5", true)
    else
        AL_SetProperty("Set Layer Disabled", "NCPlusChapter5", false)
    end
    
    --Chapter 5 Credits.
	TA_Create("Credits5")
		TA_SetProperty("Position", 19, 12)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Clipping Flag", true)
		fnSetCharacterGraphics("Root/Images/Sprites/Rilmani/", false)
		TA_SetProperty("Activation Script", gsRoot .. "ChapterSelectDialogue/Credits5.lua")
	DL_PopActiveObject()
	
    -- |[Change Game Options]|
	--Maram. Helps set some debug stuff.
	TA_Create("Maram")
		TA_SetProperty("Position", 7, 2)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Clipping Flag", true)
		fnSetCharacterGraphics("Root/Images/Sprites/Rilmani/", false)
		TA_SetProperty("Activation Script", gsRoot .. "ChapterSelectDialogue/Debug.lua")
	DL_PopActiveObject()
    
    -- |[Special NPCs]|
    --A special NPC will spawn to activate mods, if one is detected.
    if(#gsModDirectories > 0 and EM_Exists("ModActivate") == false) then
        TA_Create("ModActivate")
            TA_SetProperty("Position", 13, 14)
            TA_SetProperty("Facing", gci_Face_South)
            TA_SetProperty("Clipping Flag", true)
            TA_SetProperty("Activation Script", gsRoot .. "ChapterSelectDialogue/ChapterM.lua")
            fnSetCharacterGraphics("Root/Images/Sprites/Rilmani/", false)
        DL_PopActiveObject()
    end

	-- |[Controls]|
    --Commented out as it was causing a black screen issue. Controls are now written directly onto the background.
    --[=[
	--Variables.
	local iHasSeenControlsDialogue = VM_GetVar("Root/Variables/Nowhere/Scenes/iHasSeenControlsDialogue", "N")
	if(iHasSeenControlsDialogue == 1.0) then return end
	
    --Option: Can bypass the controls dialogue.
    if(OM_GetOption("Skip Controls Dialogue") == true) then return end
    
	--Flag.
	VM_SetVar("Root/Variables/Nowhere/Scenes/iHasSeenControlsDialogue", "N", 1.0)
	
	--Show the dialogue and set it to scenes mode. This scene takes up the whole screen.
    WD_SetProperty("Clear Censor Bars")
	fnCutscene([[ AL_SetProperty("Activate Fade", 60, gci_Fade_Over_GUI_Blackout_World, true, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/System/Controls") ]])
	fnCutscene([[ WD_SetProperty("Set Ignore Scene Offsets", true) ]])
	fnCutscene([[ Append("Press Examine (Z) to continue...") ]])
	fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()
    AL_SetProperty("Can Edit Field Abilities", false)
    ]=]

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[Spawn NPCs Here]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.

    -- |[Field Abilities]|
    --Disable Scout Sight if it's active.
    local iScoutSightActive = VM_GetVar("Root/Variables/FieldAbilities/All/iScoutSightActive", "N")
    if(iScoutSightActive == 1.0) then
        VM_SetVar("Root/Variables/FieldAbilities/All/iScoutSightActive", "N", 0.0)
        AL_SetProperty("Camera Zoom", AL_GetProperty("Default Camera Scale"))
    end

    -- |[Clear Field Abilities]|
    AL_SetProperty("Can Edit Field Abilities", false)
    AdvCombat_SetProperty("Set Field Ability", 0, "NULL")
    AdvCombat_SetProperty("Set Field Ability", 1, "NULL")
    AdvCombat_SetProperty("Set Field Ability", 2, "NULL")
    AdvCombat_SetProperty("Set Field Ability", 3, "NULL")
    AdvCombat_SetProperty("Set Field Ability", 4, "NULL")
    
    -- |[Special NPCs]|
    --A special NPC will spawn to activate mods, if one is detected.
    if(#gsModDirectories > 0 and EM_Exists("ModActivate") == false) then
        TA_Create("ModActivate")
            TA_SetProperty("Position", 10, 14)
            TA_SetProperty("Facing", gci_Face_South)
            TA_SetProperty("Clipping Flag", true)
            TA_SetProperty("Activation Script", gsRoot .. "ChapterSelectDialogue/ChapterM.lua")
            fnSetCharacterGraphics("Root/Images/Sprites/Rilmani/", false)
        DL_PopActiveObject()
    end
end
