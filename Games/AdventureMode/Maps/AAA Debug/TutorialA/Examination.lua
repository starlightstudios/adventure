-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "SampleExit") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("LevelName", "FORCEPOS:28.0x16.0x0")
    
-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "SignA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Welcome to the tutorial room.[P] Nothing in here is canon!)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (And now that you're in the tutorial room, you can officially no longer complain that this game is too hard to understand.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This whole thing is optional.[P] If you want to figure out how the game works on your own -[P] taff off!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (You can walk up behind an enemy and rapidly press the [Activate] key to 'Mug' them.[P] This will stun them and give you some of their loot.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Many enemies follow patrol paths.[P] Watch their paths and then sneak up behind them.[P] Touching an enemy without being seen gives you a free turn in combat.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This is a (fake) firepit.[P] These act as savepoints and rest points.[P] If you rest, all enemies respawn and your health is refilled.[P] You can warp to any save point you've activated before.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This is a (fake) save bench.[P] They are the same as firepits, but you can't warp.[P] You can still rest and save at a bench.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignF") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (You should examine objects whenever you can, as some may contain hints or even bonus items.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignG") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This is a regular chest.[P] It contains goodies like weapons, armor, items, money, or even experience and job points.[P] This is a fake tutorial chest, though.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignH") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Blue chests contain special catalyst items.[P] When you find a certain number of catalysts, your character's stats will increase.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Catalysts persist between chapters, meaning other characters get the same benefits.[P] You can see how many catalysts you have collected on the pause menu.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This chest is a fake, but keep your eyes peeled for blue chests.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "Bag") then
    local iTutorialBag = VM_GetVar("Root/Variables/Chapter1/Scenes/Intro|iTutorialBag", "N")
    if(iTutorialBag == 0.0) then
        VM_SetVar("Root/Variables/Chapter1/Scenes/Intro|iTutorialBag", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ([SOUND|World|TakeItem]Got 50 platina!)") ]])
        fnCutsceneBlocker()
        LM_ExecuteScript(gsItemListing, "Platina x50")
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The bag is empty.)") ]])
        fnCutsceneBlocker()
    end

elseif(sObjectName == "FakeChest") then
    local iTutorialChestA = VM_GetVar("Root/Variables/Chapter1/Scenes/Intro|iTutorialChestA", "N")
    if(iTutorialChestA == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ([SOUND|World|FlipSwitch]This was a fake chest, just like the sign said!)") ]])
        fnCutsceneBlocker()
        AL_SetProperty("Set Layer Disabled", "OpenedChestUR", false)
        AL_SetProperty("Set Layer Disabled", "OpenedChestR",  false)
        AL_SetProperty("Set Layer Disabled", "UnopenedChestR",  true)
    end
    
elseif(sObjectName == "FakeCatalyst") then
    local iTutorialChestB = VM_GetVar("Root/Variables/Chapter1/Scenes/Intro|iTutorialChestB", "N")
    if(iTutorialChestB == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ([SOUND|World|FlipSwitch]Normally you'd see a special animation play, but this was a fake chest.[P] Prank'd!)") ]])
        fnCutsceneBlocker()
        AL_SetProperty("Set Layer Disabled", "OpenedChestUL", false)
        AL_SetProperty("Set Layer Disabled", "OpenedChestL",  false)
        AL_SetProperty("Set Layer Disabled", "UnopenedChestL",  true)
    end

elseif(sObjectName == "BookshelfA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Money in Pandemonium is called 'Platina' and can be spent at shops to buy things.[P] Be on the lookout for Adamantite, though, as it will allow you to upgrade gems at gemcutters.[P] There is at least one gemcutter in each chapter.[P] Seek them out!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (When defeating an enemy, you also get 'Job Points' or JP.[P] On the skills menu, you can spend JP to purchase abilities and then equip them and use them in battle.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (JP found by defeating enemies is specific to the job you were in when you defeated the enemy.[P] JP gained from chests or quests goes to the 'global' pool of JP and can be spent on any job.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (If you learn all the abilities in a job, you master the job, and any unspent JP goes to the global pool.[P] Any further JP gained in that job likewise goes to the global pool, so if you master a job you like, you can stay on it and purchase skills from other jobs.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (When you defeat an enemy, you get 'Experience Points' that allow you to level up.[P] All your stats increase when you gain a level.[P] You can also get experience by finding it in chests, completing certain quests, or mugging enemies.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (When in combat, you can press F1 during your turn to see the Combat Inspector.[P] This will show you your buffs, debuffs, and even how much 'threat' you have against an enemy.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Threat determines which character an enemy will target.[P] They are more likely to target the character with the highest threat.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (You can also quickly check what all the buffs and debuffs do with the Combat Inspector.[P] Don't forget about it!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Every turn in combat, your party generates 10 Magic Points, or MP.[P] Some abilities cost more than this, some less, and some are free.[P] You don't get to retain MP between battles, so spend it!)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Defeating enemies quickly provides extra Doctor Bag charges, so it's okay to take a bit more damage and then heal up afterwards.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (For longer battles, make sure you have enough MP to use your skills.[P] It might be a good idea to defend for a turn or two to build up some MP.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfF") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Most actions in combat generate Combo Points, or CP.[P] Abilities with a number on their icon require CP, and are generally more powerful than normal abilities.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Some abilities may be weaker than normal, but build up CP faster, and some strong abilities might not build up CP.[P] You lose CP after the battle is over, so go ahead and spend it.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfG") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (You can purchase skills between battles for JP, and equip them to the ten slots in the bottom right.[P] There are also six skills that are job-specific in the bottom-left box.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (When changing jobs, the job skills change but the abilities you equipped remain, allowing you to use abilities your job cannot normally use.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Characters with runestones, like Mei, can even change jobs in combat, allowing you to adapt your strategy and skills to certain enemies.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (On the expert UI, press up or down on the edge of the ability selection to change pages.[P] On the simple UI, select the Tactics card.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Some of the skills on your job-specific bar are 'Locked'.[P] Find skillbooks in the world to unlock these.[P] Look for a sword icon above a bookcase or other examinable object to denote a skillbook.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfH") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Some abilities are 'Free Actions', which have a square frame around them.[P] These do not end your turn when used.[P] Most equipped items are free actions.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (You can normally only use one free action per turn, but hey -[P] it's free![P] Free actions can range from buffs to heals to quick attacks.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Most equipped items have 'charges' that refill after combat, limiting their use.[P] Some don't, such as Mei's runestone, so use them early and often.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "BookA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (At the start of a turn, every character uses their 'Initiative' stat to determine who goes first.[P] There is a random scatter of up to 80 for this roll, but having a higher initiative means you will usually go first.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Status effects may inflict 'Slow' or 'Fast' on a character.[P] Slow causes you to go last regardless of initiative, fast does the opposite.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (If two characters are Slow, they compare initiative as usual.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (There is an additional, more severe version of slow and fast.[P] 'Always Strikes First' and 'Always Strikes Last' do the same things but override slow and fast. They are fairly rare.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "BookB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Most abilities deal damage based on your character's attack power.[P] A multiplier is shown in the ability description.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Your character's hit chance is determined by their accuracy versus the enemy's evade rate.[P] Higher accuracy means you're more likely to hit, while higher evade means less likely.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (If an ability has more than 100pct chance to hit, it becomes a critical strike.[P] It will deal extra damage, usually +25pct, and also inflict some stun on the enemy.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Attacks that nearly hit but still miss instead become 'Glancing Blows', which deal reduced damage.[P] However, glancing blows still inflict status effects.[P] If an enemy is very hard to hit, it may be a good idea to use damage-over-time abilities.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "BookC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Stun is visible above an enemy's health bar.[P] When their stun value goes over their stun threshold, they skip their next turn.[P] They then gain stun resistance.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Stun resistance reduces how much stun damage they take, making them harder to stun.[P] It wears off after a few turns.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Stunned enemies cannot dodge attacks, so stunning a fast enemy may allow you to hit them with slower attacks.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Some abilities, like Mei's Pommel Bash, deal extra stun damage.[P] Stunning a dangerous enemy can make it easier to take down.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "BookD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Some abilities, like Mei's Rend, inflict 'Damage Over Time', or DoT.[P] The victim will take damage at the beginning of their turn until the effect is removed or wears off.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Direct attacks have their damage reduced by the enemy's 'Protection' stat, but Damage-Over-Times ignore it and deal damage directly.[P] Very tough enemies may be vulnerable to DoTs!)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Enemies who are highly evasive may take glancing blows instead of direct hits, but DoTs deal full damage when applied.[P] The tradeoff is that you must wait for the full damage to take effect.[P] You can, however, ignore an enemy who will be defeated by DoTs when their turn begins.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The chance for an effect like a DoT to apply is based on the effect type and the enemy's effect resistance.[P] An enemy made of rock won't bleed, but might corrode.[P] A plant enemy like an alraune will be weak to fire and fire-effects.[P] Experiment, and have a wide range of skills available!)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "Note") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Did you know you can hold the Activate and Cancel keys (Z and X by default) down to greatly speed up text?[P] You do now!)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "NoteAgain") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (There is an alternate combat layout.[P] Is the default Expert layout too confusing?[P] Try the simple layout![P] You can enable it from the options menu.)") ]])
    fnCutsceneBlocker()


-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
