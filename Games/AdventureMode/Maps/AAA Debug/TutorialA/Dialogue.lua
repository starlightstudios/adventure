-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    if(sActorName == "HumanA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lady:[VOICE|HumanF0] There are twelve different 'types' of damage.[P] Slashing, Piercing, and Striking are exactly what you expect.[B][C]") ]])
        fnCutscene([[ Append("Lady:[VOICE|HumanF0] Flaming, Freezing, and Shocking are your typical elements.[P] Crusading and Obscuring are light and dark.[B][C]") ]])
        fnCutscene([[ Append("Lady:[VOICE|HumanF0] There's also Bleeding, Poisoning, and Corroding, which are usually damage-over-times but can be direct attacks.[B][C]") ]])
        fnCutscene([[ Append("Lady:[VOICE|HumanF0] Lastly, you have Terrifying, which is psychological damage.[P] It tends to be less effective against dumb or mindless enemies, and effective against total cowards.") ]])
        
    elseif(sActorName == "AlrauneA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Alraune:[VOICE|Alraune] You can retreat from a fight if you're losing, but you won't get experience or items.[B][C]") ]])
        fnCutscene([[ Append("Alraune:[VOICE|Alraune] After retreating, all enemies will become more alert for 20 seconds.[P] If you get into another battle during this time, you won't be able to retreat![B][C]") ]])
        fnCutscene([[ Append("Alraune:[VOICE|Alraune] Also, you can surrender to lose the battle immediately.[P] In some cases, you can 'Volunteer' instead.[B][C]") ]])
        fnCutscene([[ Append("Alraune:[VOICE|Alraune] If you see the chance to volunteer, there is a special cutscene.[P] You can often get new forms by volunteering to a new enemy type.[P] Losing is a strategy!") ]])
        
    elseif(sActorName == "AlrauneB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Alraune:[VOICE|Alraune] If you check the equip screen, the little hole icons are 'sockets'.[P] You can put gems in those to make your equipment better.[B][C]") ]])
        fnCutscene([[ Append("Alraune:[VOICE|Alraune] You can upgrade gems by merging them together at a gemcutter, which costs Adamantite.[P] Gems automatically unequip when you unequip an item.") ]])
        
    elseif(sActorName == "HumanB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lady:[VOICE|HumanF0] Enemies with an outline around them are tougher versions of the same enemy you saw earlier.[P] They may have higher stats and new abilities.[B][C]") ]])
        fnCutscene([[ Append("Lady:[VOICE|HumanF0] If you see a discolored enemy -[P] that's a paragon![P] They have five times the stats and rewards.[P] Each area has a paragon that will spawn after you defeat enough enemies.[B][C]") ]])
        fnCutscene([[ Append("Lady:[VOICE|HumanF0] If you manage to defeat the paragon, ordinary enemies in that area have a chance to spawn as paragons.") ]])
        
    elseif(sActorName == "HumanC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Mercenary:[VOICE|MercF] Hold down the run key, default [Shift] to run.[P] This consumes stamina, which you can see in a ring near your character (or in the top left corner if you change the display option.)[B][C]") ]])
        fnCutscene([[ Append("Mercenary:[VOICE|MercF] If there are no enemies around, it will not consume stamina.[P] Otherwise, be careful not to run out when darting past an enemy.") ]])
    end
end
