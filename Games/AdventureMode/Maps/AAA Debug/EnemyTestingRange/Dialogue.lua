-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = "Null"
if(sTopicName == "Hello") then 
    sActorName = TA_GetProperty("Name")
end

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then

    --Cultist tutorial fight.
    if(sActorName == "CultistTutorial") then
        AdvCombat_SetProperty("Reinitialize")

        --Enemy script:
        --LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/000 Scripted Enemies/Cultist Tutorial.lua", 0)

        --Charted enemy:
        local sAutoHandler = gsRoot .. "Combat/Enemies/Chapter 1/Enemy Auto Handler.lua"
        local sEnemyString = "LM_ExecuteScript(\"" .. sAutoHandler .. "\", \"Mannequin Scrub\")"
        fnCutscene(sEnemyString)

        --Common.
        fnCutscene([[ AdvCombat_SetProperty("Activate") ]])
        fnCutscene([[ AdvCombat_SetProperty("World Pulse", true) ]])
        fnCutsceneBlocker()

    --Cultist tutorial fight.
    elseif(sActorName == "Sound Test") then

        --Setup.
        local iChorusConstant = AudioManager_GetProperty("Bass Enumeration", "BASS_FX_BFX_CHORUS")

        --Clear and re-register.
        AudioManager_SetProperty("Clear Effect Profiles")
        AudioManager_SetProperty("Register Effect Profile", "Mei Sample Chorus", iChorusConstant)

        --Set Properties.
        AudioManager_SetProperty("Set Effect Profile Property", "Mei Sample Chorus", iChorusConstant, "Dry Mix",     0.90)
        AudioManager_SetProperty("Set Effect Profile Property", "Mei Sample Chorus", iChorusConstant, "Wet Mix",     0.40)
        AudioManager_SetProperty("Set Effect Profile Property", "Mei Sample Chorus", iChorusConstant, "Feedback",    0.50)
        AudioManager_SetProperty("Set Effect Profile Property", "Mei Sample Chorus", iChorusConstant, "Min Sweep",  1.00)
        AudioManager_SetProperty("Set Effect Profile Property", "Mei Sample Chorus", iChorusConstant, "Max Sweep",  10.00)
        AudioManager_SetProperty("Set Effect Profile Property", "Mei Sample Chorus", iChorusConstant, "Rate",       5.00)
        AudioManager_SetProperty("Set Effect Profile Property", "Mei Sample Chorus", iChorusConstant, "Channel",      63)
        
        --Play a sound using this chorus.
        AudioManager_SetProperty("Apply Effect To Sound", "Callout|Mei|Ability|JigglyChestB", "Mei Sample Chorus")
        AudioManager_PlaySound("Callout|Mei|Ability|JigglyChestB")

    --Debug Vendor.
    elseif(sActorName == "HumanA") then

        --Set facing.
        TA_SetProperty("Face Character", "PlayerEntity")

        --Setup.
        local sBasePath = fnResolvePath()
        local sString = "AM_SetShopProperty(\"Show\", \"Debug Vendor\", \"" .. sBasePath .. "Shop Setup.lua\", \"" .. sBasePath .. "Shop Teardown.lua\")"

        --Run the shop.
        fnCutscene(sString)
        fnCutsceneBlocker()

    --Cutscene checker.
    elseif(sActorName == "HumanB") then

        --Common.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Ghost", "Neutral") ]])

        --Variables
        local iIsGhostTF        = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsGhostTF", "N")
        local iPutMaidOutfitOn  = VM_GetVar("Root/Variables/Chapter1/Scenes/iPutMaidOutfitOn", "N")
        local iToldNatalieTwice = VM_GetVar("Root/Variables/Chapter1/Scenes/iToldNatalieTwice", "N")
        local iCleaningProgress = VM_GetVar("Root/Variables/Chapter1/Scenes/iCleaningProgress", "N")
        local iLydieLeftParty   = VM_GetVar("Root/Variables/Chapter1/Scenes/iLydieLeftParty", "N")
        
        
            
            --Dialogue.
            fnCutscene([[ Append("Lydie: Wow, you really managed to pull it off![P] If you hadn't been slacking, I'd be tempted to tell the Countess how well you did![B][C]") ]])
            fnCutscene([[ Append("Natalie:[VOICE|Mei][EMOTION|Mei|Neutral] Pah![P] She's never going to believe you!") ]])
            fnCutsceneBlocker()
            
            --Flags.
            VM_SetVar("Root/Variables/Chapter1/Scenes/iCanLeaveRoomW", "N", 1.0)

            --Get Lydie's uniqueID. 
            EM_PushEntity("Lydie")
                local iLydieID = RE_GetID()
                TA_SetProperty("Clipping Flag", false)
                TA_SetProperty("Activation Script", "Null")
            DL_PopActiveObject()
            
            --Disable Lydie's collision and add her to Natalie's following list.
            giFollowersTotal = 1
            gsaFollowerNames = {"Lydie"}
            giaFollowerIDs = {iLydieID}
            AL_SetProperty("Follow Actor ID", iLydieID)
            
            --Move Natalie onto Lydie:
            fnAutoFoldParty()
            fnCutsceneBlocker()
    
    
    
    
    
    --Bosses/Skipper.
    elseif(sActorName == "HumanC") then
    
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Trigger a boss fight?[BLOCK]") ]])

        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Arachnophelia\", "      .. sDecisionScript .. ", \"Arachnophelia\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Infirm\",  "            .. sDecisionScript .. ", \"Infirm\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Cultist Corrupter\",  " .. sDecisionScript .. ", \"Cultist Corrupter\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Triple Werecats\",  "   .. sDecisionScript .. ", \"Triple Werecats\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Mycela\",  "            .. sDecisionScript .. ", \"Mycela\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Victoria\",  "          .. sDecisionScript .. ", \"Victoria\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Complete Chapter\",  "  .. sDecisionScript .. ", \"Complete Chapter\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  "         .. sDecisionScript .. ", \"No Thanks\") ")
        fnCutsceneBlocker()

    end

elseif(sTopicName == "Arachnophelia") then
	WD_SetProperty("Hide")
    AdvCombat_SetProperty("World Pulse", true)
    AdvCombat_SetProperty("Next Combat Music", "RottenTheme", 0.0000)
    AdvCombat_SetProperty("Reinitialize")
    AdvCombat_SetProperty("Activate")
    AdvCombat_SetProperty("Victory Script", gsRoot .. "Chapter 1/Scenes/QuantirManse_Ending/Combat_Victory.lua")
    AdvCombat_SetProperty("Defeat Script", gsStandardGameOver)
    LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/000 Scripted Enemies/Boss Arachnophelia.lua", 0)
    
elseif(sTopicName == "Infirm") then
	WD_SetProperty("Hide")
    AdvCombat_SetProperty("World Pulse", true)
	AdvCombat_SetProperty("Next Combat Music", "RottenTheme", 0.0000)
	AdvCombat_SetProperty("Reinitialize") 
	AdvCombat_SetProperty("Activate") 
	AdvCombat_SetProperty("Victory Script", gsRoot .. "Chapter 1/Scenes/TrapDungeon_BossBattle/Combat_Victory.lua")
	AdvCombat_SetProperty("Defeat Script",  gsStandardGameOver)
	LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/000 Scripted Enemies/Boss Infirm.lua", 0)
    
elseif(sTopicName == "Cultist Corrupter") then
	WD_SetProperty("Hide")
    AdvCombat_SetProperty("World Pulse", true)
	AdvCombat_SetProperty("Next Combat Music", "RottenTheme", 0.0000)
	AdvCombat_SetProperty("Reinitialize")
	AdvCombat_SetProperty("Activate")
	AdvCombat_SetProperty("Victory Script", gsRoot .. "Chapter 1/Scenes/BeehiveBasement_FightBoss/Combat_Victory.lua")
	AdvCombat_SetProperty("Defeat Script", gsRoot .. "Chapter 1/Scenes/200 Defeat/Defeat_Zombee/Scene_Begin.lua")
	LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/000 Scripted Enemies/Zombee Special.lua", 0)
	LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/000 Scripted Enemies/Boss Cultist Corrupter.lua", 0)
	LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/000 Scripted Enemies/Zombee Special.lua", 0)
    
elseif(sTopicName == "Triple Werecats") then
	WD_SetProperty("Hide")
    AdvCombat_SetProperty("World Pulse", true)
	AdvCombat_SetProperty("Reinitialize")
	AdvCombat_SetProperty("Activate")
	AdvCombat_SetProperty("Victory Script", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_DefeatCampCats.lua")
	AdvCombat_SetProperty("Defeat Script", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraDefeat/Scene_Begin.lua")
    LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/003 Evermoon North/Werecat Hunter.lua", 0)
    LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/003 Evermoon North/Werecat Hunter.lua", 0)
    LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/003 Evermoon North/Werecat Hunter.lua", 0)
    
elseif(sTopicName == "Mycela") then
	WD_SetProperty("Hide")
    AdvCombat_SetProperty("World Pulse", true)
	AdvCombat_SetProperty("Reinitialize")
	AdvCombat_SetProperty("Activate")
	LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/000 Scripted Enemies/Boss Mycela.lua", 0)
    
elseif(sTopicName == "Victoria") then
	WD_SetProperty("Hide")
    AdvCombat_SetProperty("World Pulse", true)
    AdvCombat_SetProperty("Next Combat Music", "RottenTheme", 0.0000)
	AdvCombat_SetProperty("Reinitialize")
	AdvCombat_SetProperty("Activate")
	LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/000 Scripted Enemies/Boss Victoria.lua", 0)
    
elseif(sTopicName == "Complete Chapter") then
	WD_SetProperty("Hide")
    
    --Execute the cleaner script.
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "Chapter 1/Cleanup/000 Entry Point.lua") ]])
	fnCutsceneBlocker()
    
    --Return to the "Nowhere" map.
    fnCutscene([[ AL_BeginTransitionTo("Nowhere", "FORCEPOS:10.0x2.0x0") ]])
    fnCutsceneBlocker()
    
elseif(sTopicName == "No Thanks") then
	WD_SetProperty("Hide")
    
end
