-- |[ ==================================== Debug Shop Setup ==================================== ]|
--Debug shop. Sells a bunch of items for testing.

-- |[Gemcutter Flag]|
AM_SetShopProperty("Allow Gemcutting")

-- |[Item Stock Listing]|
--Adamantite
AM_SetShopProperty("Add Item", "Pepper Pie", 0, -1)
AM_SetShopProperty("Add Item", "Poison Spore Cap", 0, -1)
AM_SetShopProperty("Add Item", "Adamantite Powder", 0, -1)
AM_SetShopProperty("Add Item", "Adamantite Flakes", 0, -1)

--Weapons
AM_SetShopProperty("Add Item", "Rusty Katana",          0, -1)
AM_SetShopProperty("Add Item", "Serrated Katana",       0, -1)
AM_SetShopProperty("Add Item", "Wildflower's Katana",   0, -1)
AM_SetShopProperty("Add Item", "Hunting Knife",         0, -1)
AM_SetShopProperty("Add Item", "Butterfly Knife",       0, -1)
AM_SetShopProperty("Add Item", "Haggler's Backup Plan", 0, -1)

--Armors
AM_SetShopProperty("Add Item", "Mei's Work Uniform",        0, -1)
AM_SetShopProperty("Add Item", "Tiger Dancer's Dress",      0, -1)
AM_SetShopProperty("Add Item", "Flowery Tunic",             0, -1)
AM_SetShopProperty("Add Item", "Merchant's Tunic",          0, -1)
AM_SetShopProperty("Add Item", "Regalia of a Smoothtalker", 0, -1)
AM_SetShopProperty("Add Item", "Troubadour's Robe",         0, -1)
AM_SetShopProperty("Add Item", "Traveller's Vest",          0, -1)

--Accessories
AM_SetShopProperty("Add Item", "Sphalite Ring",     0, -1)
AM_SetShopProperty("Add Item", "Decorative Bracer", 0, -1)
AM_SetShopProperty("Add Item", "Jade Eye Ring",     0, -1)

--Gems
AM_SetShopProperty("Add Item", "Glintsteel Gem", 0, -1)
AM_SetShopProperty("Add Item", "Yemite Gem",     0, -1)
AM_SetShopProperty("Add Item", "Ardrion Gem",    0, -1)
AM_SetShopProperty("Add Item", "Rubose Gem",     0, -1)
AM_SetShopProperty("Add Item", "Blurleen Gem",   0, -1)
AM_SetShopProperty("Add Item", "Qederphage Gem", 0, -1)

--Debug.
AM_SetShopProperty("Add Item", "Violet Runestone", 0, -1)
AM_SetShopProperty("Add Item", "Nanite Injection", 0, -1)
AM_SetShopProperty("Add Item", "Palliative", 0, -1)
AM_SetShopProperty("Add Item", "Pepper Pie", 0, -1)
AM_SetShopProperty("Add Item", "Silver Runestone Mk II", 0, -1)
AM_SetShopProperty("Add Item", "Regeneration Mist", 0, -1)
AM_SetShopProperty("Add Item", "Recoil Dampener", 0, -1)
AM_SetShopProperty("Add Item", "Poison Spore Cap", 0, -1)
AM_SetShopProperty("Add Item", "Smoke Bomb", 0, -1)
AM_SetShopProperty("Add Item", "Smelling Salts", 0, -1)
AM_SetShopProperty("Add Item", "Sparkling Sourdough", 0, -1)
AM_SetShopProperty("Add Item", "Underslung Flamethrower", 0, -1)