-- |[ =================================== Weapon Shop Setup ==================================== ]|
--Sells Weapons

-- |[Gemcutter Flag]|
AM_SetShopProperty("Allow Gemcutting")

-- |[Item Stock Listing]|
--All prices overridden to 0.
AM_SetShopProperty("Add Item", "7.62x56mmR Rounds",      0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "7.62x56mmR FL",          0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "7.62x56mmR LT",          0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "7.62x56mmR Expd",        0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "7.62x56mmR AP",          0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Musket Rounds",          0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Silver Straightsword",   0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Argentum Straightsword", 0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Electrum Straightsword", 0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Goddess' Whisker",       0, gciNoQuantityLimit)