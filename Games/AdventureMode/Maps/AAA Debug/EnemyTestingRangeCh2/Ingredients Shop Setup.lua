-- |[ ================================= Ingredients Shop Setup ================================= ]|
--Sells Ingredients, including huntable items.

-- |[Gemcutter Flag]|
AM_SetShopProperty("Allow Gemcutting")

-- |[Item Stock Listing]|
--All prices overridden to 0.
AM_SetShopProperty("Add Item", "Bruised Meat",       0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Bird Meat",          0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Slippery Boar Meat", 0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Mighty Venison",     0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Light Leather",      0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Light Fiber",        0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Light Carapace",     0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Gossamer",           0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Hardened Bark",      0, gciNoQuantityLimit)

AM_SetShopProperty("Add Item", "Unmelting Snow",    0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Crystallized Acid", 0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Buneye Fluff",      0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Griffon Egg",       0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Bonesaw Hog Meat",  0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Caustic Vension",   0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Tough Leather",     0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Tough Fiber",       0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Tough Carapace",    0, gciNoQuantityLimit)
