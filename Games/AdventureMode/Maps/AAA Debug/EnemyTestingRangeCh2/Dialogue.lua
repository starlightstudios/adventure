-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = "Null"
if(sTopicName == "Hello") then 
    sActorName = TA_GetProperty("Name")
end

-- |[Variables]|
--If variables need to be resolved commonly, do so here.
local iAmphibianCorrectSpecies = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianCorrectSpecies", "N")

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
    
    -- |[Shop Handlers]|
    if(sActorName == "Weapons") then
        TA_SetProperty("Face Character", "PlayerEntity")
        local sBasePath = fnResolvePath()
        local sString = "AM_SetShopProperty(\"Show\", \"Weapon Vendor\", \"" .. sBasePath .. "Weapon Shop Setup.lua\", \"Null\")"
        fnCutscene(sString)
        fnCutsceneBlocker()
    
    elseif(sActorName == "Armor") then
        TA_SetProperty("Face Character", "PlayerEntity")
        local sBasePath = fnResolvePath()
        local sString = "AM_SetShopProperty(\"Show\", \"Armor Vendor\", \"" .. sBasePath .. "Armor Shop Setup.lua\", \"Null\")"
        fnCutscene(sString)
        fnCutsceneBlocker()
        
    elseif(sActorName == "Accessories") then
        TA_SetProperty("Face Character", "PlayerEntity")
        local sBasePath = fnResolvePath()
        local sString = "AM_SetShopProperty(\"Show\", \"Accessory Vendor\", \"" .. sBasePath .. "Accessory Shop Setup.lua\", \"Null\")"
        fnCutscene(sString)
        fnCutsceneBlocker()
        
    elseif(sActorName == "Items") then
        TA_SetProperty("Face Character", "PlayerEntity")
        local sBasePath = fnResolvePath()
        local sString = "AM_SetShopProperty(\"Show\", \"Item Vendor\", \"" .. sBasePath .. "Item Shop Setup.lua\", \"Null\")"
        fnCutscene(sString)
        fnCutsceneBlocker()
    elseif(sActorName == "Gems") then
    elseif(sActorName == "Ingredients") then
        TA_SetProperty("Face Character", "PlayerEntity")
        local sBasePath = fnResolvePath()
        local sString = "AM_SetShopProperty(\"Show\", \"Ingredient Vendor\", \"" .. sBasePath .. "Ingredients Shop Setup.lua\", \"Null\")"
        fnCutscene(sString)
        fnCutsceneBlocker()
    
    -- |[Miso]|
    --Used to test Miso's unlock dialogue.
    elseif(sActorName == "Miso") then
        TA_SetProperty("Face Character", "PlayerEntity")
        local sBasePath = fnResolvePath()
        local sString = "AM_SetShopProperty(\"Show\", \"Miso\", \"" .. sBasePath .. "Miso Shop Setup.lua\", \"Null\")"
        fnCutscene(sString)
        fnCutsceneBlocker()
    
    end
end
