-- |[ ================================== Accessory Shop Setup ================================== ]|
--Sells Accessories

-- |[Gemcutter Flag]|
AM_SetShopProperty("Allow Gemcutting")

-- |[Item Stock Listing]|
--All prices overridden to 0.
AM_SetShopProperty("Add Item", "Ammo Bag",               0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Silk Pouch",             0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Sphalite Ring",          0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Decorative Bracer",      0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Jade Eye Ring",          0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Enchanted Ring",         0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Jade Necklace",          0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Arm Brace",              0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Alacrity Bracer",        0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Swamp Boots",            0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Comfy Socks",            0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Surgical Mask",          0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Fencer's Gauntlet",      0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Glove Liners",           0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Scale Gloves",           0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Butterfly Cape",         0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Wooden Greaves",         0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Slicked Wooden Greaves", 0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Treated Wooden Greaves", 0, gciNoQuantityLimit)