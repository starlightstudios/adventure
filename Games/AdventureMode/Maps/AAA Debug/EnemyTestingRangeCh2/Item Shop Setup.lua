-- |[ ===================================== Item Shop Setup ==================================== ]|
--Sells Items

-- |[Gemcutter Flag]|
AM_SetShopProperty("Allow Gemcutting")

-- |[Item Stock Listing]|
--All prices overridden to 0.
AM_SetShopProperty("Add Item", "Healing Tincture",            0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Bubbling Healing Tincture",   0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Palliative",                  0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Smelling Salts",              0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Controversial Sugar Cookies", 0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Infused Rye Bread",           0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Sparkling Sourdough",         0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Hearty Soup",                 0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Brutal Sandwich",             0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Gourmet Lunch",               0, gciNoQuantityLimit)

--Offense Items
AM_SetShopProperty("Add Item", "Lamp Oil",       0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Pitch Firebomb", 0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Clothing Patch", 0, gciNoQuantityLimit)

--Gems
AM_SetShopProperty("Add Item", "Yetesteel Gem", 0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Ardrion Gem", 0, gciNoQuantityLimit)