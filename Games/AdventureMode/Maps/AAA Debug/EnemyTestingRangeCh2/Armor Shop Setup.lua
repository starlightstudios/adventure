-- |[ ==================================== Armor Shop Setup ==================================== ]|
--Sells Armors

-- |[Gemcutter Flag]|
AM_SetShopProperty("Allow Gemcutting")

-- |[Item Stock Listing]|
--All prices overridden to 0.
AM_SetShopProperty("Add Item", "Sanya's Jacket",     0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Hunter's Jacket",    0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Warrior's Pelt",     0, gciNoQuantityLimit)

AM_SetShopProperty("Add Item", "Miko Robes",         0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Infused Miko Robes", 0, gciNoQuantityLimit)

AM_SetShopProperty("Add Item", "Zeke's Bell Collar", 0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Thick Collar",       0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Shimmering Collar",  0, gciNoQuantityLimit)

AM_SetShopProperty("Add Item", "Troubadour's Robe", 0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Traveller's Vest",  0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Leather Greatcoat", 0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Lined Miko Robes",  0, gciNoQuantityLimit)

AM_SetShopProperty("Add Item", "Light Leather Vest",  0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Plated Leather Vest",  0, gciNoQuantityLimit)

AM_SetShopProperty("Add Item", "Yemite Gem",  0, gciNoQuantityLimit)
AM_SetShopProperty("Add Item", "Thatophage Gem",  0, gciNoQuantityLimit)
