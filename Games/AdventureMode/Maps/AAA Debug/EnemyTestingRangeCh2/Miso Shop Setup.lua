-- |[ ===================================== Miso Shop Setup ==================================== ]|
--This is the "Unlock" dialogue to unlock stuff that Miso will later stock.

-- |[Unlock Mode]|
AM_SetShopProperty("Set Unlock Mode")

-- |[ ===================================== Shorthand Names ==================================== ]|
local saShorthandTable = {}
table.insert(saShorthandTable, {"Light Leather",      "LtLth"})
table.insert(saShorthandTable, {"Light Fiber",        "LtFib"})
table.insert(saShorthandTable, {"Light Carapace",     "LtCara"})
table.insert(saShorthandTable, {"Gossamer",           "Goss"})
table.insert(saShorthandTable, {"Hardened Bark",      "HBark"})
table.insert(saShorthandTable, {"Slippery Boar Meat", "BoarMt"})
table.insert(saShorthandTable, {"Mighty Venison",     "Vnsn"})
table.insert(saShorthandTable, {"Bird Meat",          "BirdMt"})

-- |[ ======================================== Functions ======================================= ]|
--List.
local zaUnlockableList = {}

--Given a name, variable, and set of requirements, places them on the table for later addition.
local function fnAddUnlockable(psName, psVariablePath, pzaRequirementsTable)
    
    -- |[Argument Check]|
    if(psName               == nil) then return end
    if(psVariablePath       == nil) then return end
    if(pzaRequirementsTable == nil) then return end
    
    -- |[Creation]|
    local zEntry = {}
    zEntry.sName               = psName
    zEntry.sVariablePath       = psVariablePath
    zEntry.zaRequirementsTable = pzaRequirementsTable
    table.insert(zaUnlockableList, zEntry)
end

-- |[ ================================== List of Unlockables =================================== ]|
--Constants
local sMisoPath = "Root/Variables/Chapter2/Miso/"

--List
fnAddUnlockable("Hunter's Jacket",        sMisoPath .. "iHuntersJacket",        {{"Light Leather",  5}, {"Light Fiber",        3}})
fnAddUnlockable("Infused Miko Robes",     sMisoPath .. "iInfusedMikoRobes",     {{"Light Leather",  5}, {"Gossamer",           5}})
fnAddUnlockable("Lined Miko Robes",       sMisoPath .. "iLinedMikoRobes",       {{"Light Leather",  7}, {"Light Fiber",        3}})
fnAddUnlockable("Warrior's Pelt",         sMisoPath .. "iWarriorsPelt",         {{"Light Leather", 10}, {"Light Fiber",        3}, {"Slippery Boar Meat", 3}})
fnAddUnlockable("Thick Collar",           sMisoPath .. "iThickCollar",          {{"Light Leather",  5}})
fnAddUnlockable("Shimmering Collar",      sMisoPath .. "iShimmeringCollar",     {{"Light Leather",  3}, {"Gossamer",           3}, {"Mighty Venison", 3}})
fnAddUnlockable("Wooden Greaves",         sMisoPath .. "iWoodenGreaves",        {{"Hardened Bark",  5}})
fnAddUnlockable("Slicked Wooden Greaves", sMisoPath .. "iSlickedWoodenGreaves", {{"Hardened Bark",  5}, {"Slippery Boar Meat", 3}})
fnAddUnlockable("Treated Wooden Greaves", sMisoPath .. "iTreatedWoodenGreaves", {{"Hardened Bark",  5}, {"Light Carapace",     3}})
fnAddUnlockable("Scale Gloves",           sMisoPath .. "iScaleGloves",          {{"Light Carapace", 5}})
fnAddUnlockable("Glove Liners",           sMisoPath .. "iGloveLiners",          {{"Gossamer",       3}})
fnAddUnlockable("Butterfly Cape",         sMisoPath .. "iHuntersJacket",        {{"Light Fiber",    5}, {"Gossamer",          10}})
fnAddUnlockable("Ammo Bag",               sMisoPath .. "iAmmoBag",              {{"Light Leather",  3}, {"Light Carapace",     3}})
fnAddUnlockable("Silk Pouch",             sMisoPath .. "iSilkPouch",            {{"Light Carapace", 3}, {"Gossamer",           3}})
fnAddUnlockable("Pitch Firebomb",         sMisoPath .. "iPitchFirebomb",        {{"Hardened Bark",  1}, {"Light Fiber",        3}, {"Gossamer",           3}, {"Slippery Boar Meat", 3}})
fnAddUnlockable("Brutal Sandwich",        sMisoPath .. "iBrutalSandwich",       {{"Bird Meat",      5}, {"Mighty Venison",     3}})
fnAddUnlockable("Gourmet Lunch",          sMisoPath .. "iGourmetLunch",         {{"Bird Meat",      5}, {"Mighty Venison",     5}, {"Slippery Boar Meat", 5}})
fnAddUnlockable("Clothing Patch",         sMisoPath .. "iClothingPatch",        {{"Light Fiber",    3}, {"Gossamer",           5}, {"Light Leather",      1}})
fnAddUnlockable("Hearty Soup",            sMisoPath .. "iHeartySoup",           {{"Bird Meat",      5}, {"Slippery Boar Meat", 3}})
fnAddUnlockable("Lamp Oil",               sMisoPath .. "iLampOil",              {{"Bird Meat",      3}, {"Slippery Boar Meat", 2}, {"Hardened Bark",      3}, {"Gossamer", 5}})

-- |[ =============================== Apply List of Unlockables ================================ ]|
--Run across the unlockables list and unlock everything.
for i = 1, #zaUnlockableList, 1 do
    
    -- |[Variable]|
    --Query the variable. If the value is 1, don't add it to the list.
    local iCheckVar = VM_GetVar(zaUnlockableList[i].sVariablePath, "N")
    if(iCheckVar == 1.0) then
    
    else
        
        -- |[Basics]|
        AM_SetShopProperty("Add Item", zaUnlockableList[i].sName, 0, 0)
        
        -- |[Requirement Setting]|
        --Fast-access pointer.
        local zaReqs = zaUnlockableList[i].zaRequirementsTable
        
        --Run across the table.
        for p = 1, #zaReqs, 1 do
            
            --Fast-access pointers.
            local sReqName     = zaReqs[p][1]
            local iReqQuantity = zaReqs[p][2]
            
            --In case the shorthand is not found, it defaults to the requirement name.
            local sShorthandName = sReqName
            
            --Check the lookup table for the shorthand name.
            for o = 1, #saShorthandTable, 1 do
                if(sReqName == saShorthandTable[o][1]) then
                    sShorthandName = saShorthandTable[o][2]
                    break
                end
            end
            
            --Add it to the list.
            AM_SetShopProperty("Set Unlock Variable", zaUnlockableList[i].sName, zaUnlockableList[i].sVariablePath)
            AM_SetShopProperty("Add Unlock Requirement", zaUnlockableList[i].sName, sReqName, sShorthandName, iReqQuantity)
        
        end
    end
end
