-- |[ ================================== Accessory Shop Setup ================================== ]|
--Sells Accessories

-- |[Gemcutter Flag]|
AM_SetShopProperty("Allow Gemcutting")

-- |[Item Stock Listing]|
--Adamantite
AM_SetShopProperty("Add Item", "Smoke Bomb", 0, -1)
AM_SetShopProperty("Add Item", "Underslung Flamethrower", 0, -1)
AM_SetShopProperty("Add Item", "Magnesium Grenade", 0, -1)
AM_SetShopProperty("Add Item", "Recoil Dampener", 0, -1)
AM_SetShopProperty("Add Item", "Nanite Injection", 0, -1)
AM_SetShopProperty("Add Item", "Explication Spike", 0, -1)
AM_SetShopProperty("Add Item", "Regeneration Mist", 0, -1)
AM_SetShopProperty("Add Item", "Emergency Medkit", 0, -1)
