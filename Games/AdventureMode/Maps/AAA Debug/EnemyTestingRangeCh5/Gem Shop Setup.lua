-- |[ ===================================== Gem Shop Setup ===================================== ]|
--Sells Gems

-- |[Gemcutter Flag]|
AM_SetShopProperty("Allow Gemcutting")

-- |[Item Stock Listing]|
--Adamantite
AM_SetShopProperty("Add Item", "Adamantite Powder", 0, -1)
AM_SetShopProperty("Add Item", "Adamantite Flakes", 0, -1)
AM_SetShopProperty("Add Item", "Adamantite Shard", 0, -1)

--Gems
AM_SetShopProperty("Add Item", "Glintsteel Gem", 0, -1)
AM_SetShopProperty("Add Item", "Phassicsteel Gem", 0, -1)
AM_SetShopProperty("Add Item", "Arensteel Gem", 0, -1)
AM_SetShopProperty("Add Item", "Yemite Gem", 0, -1)
AM_SetShopProperty("Add Item", "Romite Gem", 0, -1)
AM_SetShopProperty("Add Item", "Morite Gem", 0, -1)
AM_SetShopProperty("Add Item", "Ardrion Gem", 0, -1)
AM_SetShopProperty("Add Item", "Nockrion Gem", 0, -1)
AM_SetShopProperty("Add Item", "Donion Gem", 0, -1)
AM_SetShopProperty("Add Item", "Rubose Gem", 0, -1)
AM_SetShopProperty("Add Item", "Piorose Gem", 0, -1)
AM_SetShopProperty("Add Item", "Iniorose Gem", 0, -1)
AM_SetShopProperty("Add Item", "Blurleen Gem", 0, -1)
AM_SetShopProperty("Add Item", "Mordreen Gem", 0, -1)
AM_SetShopProperty("Add Item", "Quorine Gem", 0, -1)
AM_SetShopProperty("Add Item", "Qederphage Gem", 0, -1)
AM_SetShopProperty("Add Item", "Phonophage Gem", 0, -1)
AM_SetShopProperty("Add Item", "Thatophage Gem", 0, -1)
