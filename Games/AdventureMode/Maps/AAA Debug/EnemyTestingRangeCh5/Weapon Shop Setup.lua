-- |[ =================================== Weapon Shop Setup ==================================== ]|
--Sells Weaponry.

-- |[Gemcutter Flag]|
AM_SetShopProperty("Allow Gemcutting")

-- |[Item Stock Listing]|
--Adamantite
AM_SetShopProperty("Add Item", "Carbonweave Electrospear", 0, -1)
AM_SetShopProperty("Add Item", "Cillium Cryospear", 0, -1)
AM_SetShopProperty("Add Item", "Niobium Thermospear", 0, -1)
AM_SetShopProperty("Add Item", "Silksteel Electrospear", 0, -1)
AM_SetShopProperty("Add Item", "Silksteel Electrohalberd", 0, -1)
AM_SetShopProperty("Add Item", "Yttrium Electrospear", 0, -1)
AM_SetShopProperty("Add Item", "Lithium Thermospear", 0, -1)
AM_SetShopProperty("Add Item", "Magnetide Cryospear", 0, -1)
AM_SetShopProperty("Add Item", "Supersteel Electrohalberd", 0, -1)
AM_SetShopProperty("Add Item", "Tellurine Electrospear", 0, -1)

AM_SetShopProperty("Add Item", "Mk IV Pulse Diffractor", 0, -1)
AM_SetShopProperty("Add Item", "Mk IV Pulse Rifle", 0, -1)
AM_SetShopProperty("Add Item", "Mk V Pulse Diffractor", 0, -1)
AM_SetShopProperty("Add Item", "K-47 Assault Rifle", 0, -1)
AM_SetShopProperty("Add Item", "Stalker Bullpup Rifle", 0, -1)
AM_SetShopProperty("Add Item", "Mk VI Pulse Diffractor", 0, -1)
AM_SetShopProperty("Add Item", "R-77 Pulse Diffractor", 0, -1)
AM_SetShopProperty("Add Item", "R-10 Ballistic Cannon", 0, -1)
AM_SetShopProperty("Add Item", "Hotshot Pulse Diffractor", 0, -1)
AM_SetShopProperty("Add Item", "Overcharge Pulse Rifle", 0, -1)

AM_SetShopProperty("Add Item", "Fission Carbine", 0, -1)
AM_SetShopProperty("Add Item", "Cryogenic Carbine", 0, -1)
AM_SetShopProperty("Add Item", "Fission Rifle", 0, -1)
AM_SetShopProperty("Add Item", "Plutonite Fission Carbine", 0, -1)
AM_SetShopProperty("Add Item", "J-12 Freeze Carbine", 0, -1)