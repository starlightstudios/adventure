-- |[ ==================================== Armor Shop Setup ==================================== ]|
--Sells Armors

-- |[Gemcutter Flag]|
AM_SetShopProperty("Allow Gemcutting")

-- |[Item Stock Listing]|
--Adamantite
AM_SetShopProperty("Add Item", "Lord Golem Dress", 0, -1)
AM_SetShopProperty("Add Item", "Command Unit Garb", 0, -1)
AM_SetShopProperty("Add Item", "Bronze Chestguard", 0, -1)
AM_SetShopProperty("Add Item", "Armored Gothic Corset", 0, -1)
AM_SetShopProperty("Add Item", "Dispersion Cloak", 0, -1)
AM_SetShopProperty("Add Item", "Adaptive Cloth Vest", 0, -1)
AM_SetShopProperty("Add Item", "Battle Skirt", 0, -1)
AM_SetShopProperty("Add Item", "Hyperweave Chemise", 0, -1)
AM_SetShopProperty("Add Item", "Neon Tanktop", 0, -1)
AM_SetShopProperty("Add Item", "Ceramic Weave Vest", 0, -1)
AM_SetShopProperty("Add Item", "Brass Polymer Chestguard", 0, -1)
AM_SetShopProperty("Add Item", "Neutronium Jacket", 0, -1)
AM_SetShopProperty("Add Item", "Titanweave Vest", 0, -1)
