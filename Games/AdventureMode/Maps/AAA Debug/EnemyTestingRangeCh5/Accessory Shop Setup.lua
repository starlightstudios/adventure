-- |[ ================================== Accessory Shop Setup ================================== ]|
--Sells Accessories

-- |[Gemcutter Flag]|
AM_SetShopProperty("Allow Gemcutting")

-- |[Item Stock Listing]|
--Adamantite
AM_SetShopProperty("Add Item", "Kinetic Capacitor", 0, -1)
AM_SetShopProperty("Add Item", "Viewfinder Module", 0, -1)
AM_SetShopProperty("Add Item", "Sure-Grip Gloves", 0, -1)
AM_SetShopProperty("Add Item", "Spear Foregrip", 0, -1)
AM_SetShopProperty("Add Item", "Wide-Spectrum Scanner", 0, -1)
AM_SetShopProperty("Add Item", "Pulse Radiation Dampener", 0, -1)
AM_SetShopProperty("Add Item", "Magrail Harmonization Module", 0, -1)
AM_SetShopProperty("Add Item", "Sweet Flame Decals", 0, -1)
AM_SetShopProperty("Add Item", "Integrated Gunsight", 0, -1)
AM_SetShopProperty("Add Item", "Tungsten Suppressor", 0, -1)
AM_SetShopProperty("Add Item", "Sphalite Ring", 0, -1)
AM_SetShopProperty("Add Item", "Decorative Bracer", 0, -1)
AM_SetShopProperty("Add Item", "Jade Eye Ring", 0, -1)
AM_SetShopProperty("Add Item", "Enchanted Ring", 0, -1)
AM_SetShopProperty("Add Item", "Jade Necklace", 0, -1)
AM_SetShopProperty("Add Item", "Arm Brace", 0, -1)
AM_SetShopProperty("Add Item", "Alacrity Bracer", 0, -1)
AM_SetShopProperty("Add Item", "Swamp Boots", 0, -1)
AM_SetShopProperty("Add Item", "Comfy Socks", 0, -1)
AM_SetShopProperty("Add Item", "Surgical Mask", 0, -1)
AM_SetShopProperty("Add Item", "Fencer's Gauntlet", 0, -1)
AM_SetShopProperty("Add Item", "Distribution Frame", 0, -1)
AM_SetShopProperty("Add Item", "Insulated Boots", 0, -1)
