-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = "Null"
if(sTopicName == "Hello") then 
    sActorName = TA_GetProperty("Name")
end

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Functions ======================================== ]|
local function fnRunBoss(psBossPath)
	WD_SetProperty("Hide")
    AdvCombat_SetProperty("World Pulse", true)
    AdvCombat_SetProperty("Next Combat Music", "RottenTheme", 0.0000)
    AdvCombat_SetProperty("Reinitialize")
    AdvCombat_SetProperty("Activate")
    AdvCombat_SetProperty("Unretreatable", false)
    AdvCombat_SetProperty("Defeat Script", gsStandardGameOver)
    LM_ExecuteScript(psBossPath)
end

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then

    -- |[Weapons Vendor]|
    if(sActorName == "Weapon Buddy") then
        TA_SetProperty("Face Character", "PlayerEntity")
        local sBasePath = fnResolvePath()
        local sString = "AM_SetShopProperty(\"Show\", \"Weapons Vendor\", \"" .. sBasePath .. "Weapon Shop Setup.lua\", \"Null\")"
        fnCutscene(sString)
        fnCutsceneBlocker()
    
    -- |[Armors Vendor]|
    elseif(sActorName == "Armor Buddy") then
        TA_SetProperty("Face Character", "PlayerEntity")
        local sBasePath = fnResolvePath()
        local sString = "AM_SetShopProperty(\"Show\", \"Armor Vendor\", \"" .. sBasePath .. "Armor Shop Setup.lua\", \"Null\")"
        fnCutscene(sString)
        fnCutsceneBlocker()
    
    -- |[Accessory Vendor]|
    elseif(sActorName == "Accessory Buddy") then
        TA_SetProperty("Face Character", "PlayerEntity")
        local sBasePath = fnResolvePath()
        local sString = "AM_SetShopProperty(\"Show\", \"Accessory Vendor\", \"" .. sBasePath .. "Accessory Shop Setup.lua\", \"Null\")"
        fnCutscene(sString)
        fnCutsceneBlocker()
    
    -- |[Item Vendor]|
    elseif(sActorName == "Item Buddy") then
        TA_SetProperty("Face Character", "PlayerEntity")
        local sBasePath = fnResolvePath()
        local sString = "AM_SetShopProperty(\"Show\", \"Item Vendor\", \"" .. sBasePath .. "Item Shop Setup.lua\", \"Null\")"
        fnCutscene(sString)
        fnCutsceneBlocker()
    
    -- |[Gem Vendor]|
    elseif(sActorName == "Gem Buddy") then
        TA_SetProperty("Face Character", "PlayerEntity")
        local sBasePath = fnResolvePath()
        local sString = "AM_SetShopProperty(\"Show\", \"Gem Vendor\", \"" .. sBasePath .. "Gem Shop Setup.lua\", \"Null\")"
        fnCutscene(sString)
        fnCutsceneBlocker()
    
    -- |[Boss Runner]|
    elseif(sActorName == "Boss Buddy") then
    
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Trigger a boss fight?[BLOCK]") ]])

        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Equinox Golem Timid\", "    .. sDecisionScript .. ", \"EquinoxTimid\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Equinox Golem Chemical\", " .. sDecisionScript .. ", \"EquinoxChemical\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Equinox Golem Physical\", " .. sDecisionScript .. ", \"EquinoxPhysical\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Equinox Golem Lord\", "     .. sDecisionScript .. ", \"EquinoxLord\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Equinox Doll\", "           .. sDecisionScript .. ", \"EquinoxDoll\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"609144\", "                 .. sDecisionScript .. ", \"609144\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Serenity\", "               .. sDecisionScript .. ", \"Serenity\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Monstrosity\", "            .. sDecisionScript .. ", \"Monstrosity\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Vivify\", "                 .. sDecisionScript .. ", \"Vivify\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Christine\", "              .. sDecisionScript .. ", \"Christine\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  "             .. sDecisionScript .. ", \"No Thanks\") ")
        fnCutsceneBlocker() 
    
    -- |[Ending Handler]|
    elseif(sActorName == "Ending Buddy") then
    
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Complete the chapter?[BLOCK]") ]])

        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Complete Chapter\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"No Thanks\") ")
        fnCutsceneBlocker()
    
    end

elseif(sTopicName == "EquinoxTimid") then
    fnRunBoss(gsRoot .. "Combat/Enemies/Chapter 5/000 Scripted Enemies/Boss Equinox Golem Timid.lua")
    
elseif(sTopicName == "EquinoxChemical") then
    fnRunBoss(gsRoot .. "Combat/Enemies/Chapter 5/000 Scripted Enemies/Boss Equinox Golem Chemical.lua")
    
elseif(sTopicName == "EquinoxLord") then
    fnRunBoss(gsRoot .. "Combat/Enemies/Chapter 5/000 Scripted Enemies/Boss Equinox Golem Lord.lua")
    
elseif(sTopicName == "EquinoxPhysical") then
    fnRunBoss(gsRoot .. "Combat/Enemies/Chapter 5/000 Scripted Enemies/Boss Equinox Golem Physical.lua")
    
elseif(sTopicName == "EquinoxDoll") then
    fnRunBoss(gsRoot .. "Combat/Enemies/Chapter 5/000 Scripted Enemies/Boss Equinox Doll.lua")
    
elseif(sTopicName == "609144") then
    fnRunBoss(gsRoot .. "Combat/Enemies/Chapter 5/000 Scripted Enemies/Boss 609144.lua")
    
elseif(sTopicName == "Serenity") then
    fnRunBoss(gsRoot .. "Combat/Enemies/Chapter 5/000 Scripted Enemies/Boss Serenity.lua")
    
elseif(sTopicName == "Monstrosity") then
    fnRunBoss(gsRoot .. "Combat/Enemies/Chapter 5/000 Scripted Enemies/Boss Monstrosity.lua")
    
elseif(sTopicName == "Vivify") then
    fnRunBoss(gsRoot .. "Combat/Enemies/Chapter 5/000 Scripted Enemies/Boss Vivify.lua")
    
elseif(sTopicName == "Christine") then
    fnRunBoss(gsRoot .. "Combat/Enemies/Chapter 5/000 Scripted Enemies/Boss Christine.lua")
    
elseif(sTopicName == "Complete Chapter") then
	WD_SetProperty("Hide")
    
    --Execute the cleaner script.
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "Chapter 5/Cleanup/000 Entry Point.lua") ]])
	fnCutsceneBlocker()
    
    --Return to the "Nowhere" map.
    fnCutscene([[ AL_BeginTransitionTo("Nowhere", "FORCEPOS:10.0x2.0x0") ]])
    fnCutsceneBlocker()
    
elseif(sTopicName == "No Thanks") then
	WD_SetProperty("Hide")
    
end
