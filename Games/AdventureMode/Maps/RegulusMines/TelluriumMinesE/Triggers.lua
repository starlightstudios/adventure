-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ========================================= Stock ========================================== ]|
if(sObjectName == "WarpActivate") then
    fnWarpUnlockHandler("Root/Variables/Chapter5/Campfires/iTelluriumMinesE", 38.25, 39.50)
    
-- |[ ==================================== SX-399 Overheard ==================================== ]|
elseif(sObjectName == "Overhearing") then

	--Variables.
	local iSawMinesECutsceneA = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMinesECutsceneA", "N")
	if(iSawMinesECutsceneA == 1.0) then return end
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesECutsceneA", "N", 1.0)
	
	--Movement.
	fnCutsceneMove("Christine", 62.25-45, 54.50-24)
	fnCutsceneFace("Christine", 0, -1)
	fnCutsceneMove("Tiffany", 61.25-45, 54.50-24)
	fnCutsceneFace("Tiffany", 0, -1)
	fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|ButtonClick") ]])
    fnCutscene([[ AL_SetProperty("Deactivate Player Light") ]])
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Christine:[VOICE|Christine] I think I see a droid over there.[P] Stay out of sight...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--SX-399 enters the scene.
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 3.0)
		CameraEvent_SetProperty("Focus Actor Name", "SX-399")
	DL_PopActiveObject()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneMove("SX-399", 80.25-45, 43.50-24)
	fnCutsceneMove("SX-399", 80.25-45, 41.50-24)
	fnCutsceneMove("SX-399", 77.25-45, 41.50-24)
	fnCutsceneMove("SX-399", 77.25-45, 37.50-24)
	fnCutsceneMove("SX-399", 73.25-45, 37.50-24)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "JX-101", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] SX-399![P] There you are![P] I've been worried sick![B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] Mother?[P] Is that you?[P] Don't you need me to say the passphrase?") ]])
	fnCutsceneBlocker()
	fnCutsceneFace("SX-399", 0, -1)
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneMove("JX-101", 72.25-45, 37.50-24)
	fnCutsceneFace("JX-101", 1, 0)
	fnCutsceneBlocker()
	fnCutsceneFace("SX-399", -1, 0)
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "JX-101", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Pah, as if you care about security![P] What good is a passphrase when you sneak out without permission?[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Humour me for a moment.[P] Imagine if a golem had seen you?[P] What then?[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] I -[P][CLEAR]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Oh, I know what'd happen.[P] They'd either capture you and have you scrapped, or perhaps they'd follow you here.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] And then they'd know where our settlement was, and send a team down to retire us all.[P] Would you prefer that?[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] Mother, no one saw me![B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] You're certain of this, I take it.[P] After all, you were checking behind you, removing all traces of your passing, and disabling security robots?[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] ...[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Sooner or later, you will be unlucky.[P] It only takes one mistake.[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] But - [P][CLEAR]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] And is this chemical powder on your hands the result of tampering?[P] Yes, you look like you've been in golem electronics again.[P] What did you steal?[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] I pulled out a power conduit, and fixed the network terminal near the entrance.[P] Now we can see all their shipping manifests![B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] Y-you're -[P] you're not proud of me?[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] I'm not proud of your foolishness, that much is certain![P] If the golems do not have a repair unit fix a terminal, and it suddenly starts working, that will draw their suspicion![B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Doubtless they will send a repair unit after the conduit you sabotaged and realize where the parts went.[P] You put us all at risk with your carelessness.[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] Argh![P] Is nothing good enough for you?[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] I always do my best and try to help, but all I get is shouted at![B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Do not raise your voice at me, young lady.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] We will talk about this later.[P] I have a meeting I must attend.[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] Grrrr...[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] I said, we will continue this conversation later.[P] Is that understood?[B][C]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] Yes, mother.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] SX-399...[P] I worried the whole time you were out.[P] Please - [P][CLEAR]") ]])
	fnCutscene([[ Append("SX-399:[E|Neutral] I don't even care![P] I'll be in my room!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Movement.
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 3.0)
		CameraEvent_SetProperty("Focus Actor Name", "JX-101")
	DL_PopActiveObject()
	fnCutsceneMove("SX-399", 73.25-45, 35.50-24)
	fnCutsceneMove("SX-399", 72.25-45, 35.50-24)
	fnCutsceneBlocker()
	fnCutsceneFace("JX-101", 0, -1)
	fnCutsceneMove("SX-399", 72.25-45, 30.50-24)
	fnCutsceneMove("SX-399", 74.25-45, 30.50-24)
	fnCutsceneMove("SX-399", 74.25-45, 29.50-24)
	fnCutsceneMove("SX-399", 76.25-45, 29.50-24)
	fnCutsceneFace("SX-399", 0, 1)
	fnCutsceneTeleport("SX-399", -100.25, -100.50)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "JX-101", "Neutral") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] *Sigh*...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Move the camera to Christine.
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 3.0)
		CameraEvent_SetProperty("Focus Actor Name", "Christine")
	DL_PopActiveObject()
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
	fnCutscene([[ Append("55:[E|Neutral] Target identified::[P] JX-101.[P] Leader of the Fist of the Future faction.[P] One of the more important Steam Droids, she is an ideal candidate for recruitment.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Sad] So we're just going to ignore that little spat they had?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] The psychological profiles I found indicate that JX-101 cares for her daughter, SX-399.[P] Kidnapping her would be a good way to ensure the loyalty of her faction.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Offended] 55![P] What's gotten in to you?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] We should be careful not to aggravate the situation further.[P] A falling out would reduce the effectiveness of blackmail.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Offended] Okay, that's wrong on a lot of levels.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] First, an argument isn't going to make a mother love her daughter any less...[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Offended] And -[P] kidnapping?[P] Honestly, 55, is that what your plan was?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Smirk] Yes.[P] Yes it was.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] The Steam Droids are old technology.[P] Their aim is shaky, their foot-speed is slower, and their chassis is more vulnerable to damage.[P] They are poor soldiers.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] During a direct confrontation with Regulus City security forces, they would be decimated.[P] Cultivating long-term relations is a waste of energy.[P] They are expendable.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Offended] I am not surprised, just disappointed.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] You disagree?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Oh, your assessment is probably spot on.[P] They don't look to be in good condition, I'll give you that.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Offended] But people are not to be used up and thrown away.[P] You treat them like that, and you can expect them to treat you the same way.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Smirk] So long as we are the ones discarding them, I see no problem with it.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Angry] And will you discard me when I am no longer useful?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Down] ...[P] No.[P] Because...[P] because you will not cease to be useful.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Offended] 'And them that take the sword shall perish by the sword.'[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] I do not use such primitive melee weapons.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Offended] It's a metaphor, dumb bolt.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] We do not have time for pointless philosophizing.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Offended] Just -[P] let me do the talking.[P] We'll discuss your intentions later.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Come on, their settlement can't be far away.[P] And keep your eyes peeled for traps.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneTeleport("JX-101", -100.25, -100.50)

	--55 moves onto Christine.
    fnCutscene([[ AudioManager_PlaySound("World|ButtonClick") ]])
    fnCutscene([[ AL_SetProperty("Activate Player Light", 100, 100) ]])
	fnCutsceneWait(5)
	fnCutsceneMove("Tiffany", 62.25-45, 54.50-24)
	fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
	
-- |[ =================================== Falling Into Trap ==================================== ]|
elseif(sObjectName == "DashItAll") then

	--Variables.
	local iSawMinesECutsceneB = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMinesECutsceneB", "N")
	if(iSawMinesECutsceneB == 1.0) then return end
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesECutsceneB", "N", 1.0)
    
    --If Christine is a human, set this flag. It affects a conversation later.
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    if(sChristineForm == "Human") then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iWasHumanForMinesB", "N", 1.0)
    else
        VM_SetVar("Root/Variables/Chapter5/Scenes/iWasHumanForMinesB", "N", 0.0)
    end
	
	--Movement.
	fnCutsceneMove("Christine", 67.25-45, 44.50-24)
	fnCutsceneMove("Tiffany", 66.25-45, 44.50-24)
	fnCutsceneBlocker()
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--SFX.
	fnCutscene([[ AudioManager_PlaySound("Combat|Impact_Strike") ]])
	fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Walls3Bridge", true) ]])
	fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Floors3False", true) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	fnCutsceneFace("Christine", 0, 1)
	fnCutsceneFace("Tiffany", 0, 1)
	fnCutsceneWait(55)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Christine:[VOICE|Christine] Oh, dash it all...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(5)
	fnCutsceneBlocker()
	
	--Move them downwards.
	for y = 20.50, 31.50, 0.50 do
		fnCutsceneTeleport("Christine", 67.25-45, y)
		fnCutsceneTeleport("Tiffany", 66.25-45, y)
		fnCutsceneWait(1)
		fnCutsceneBlocker()
	end
	
	--Black the screen out.
	fnCutscene([[ AudioManager_PlaySound("Combat|Impact_Strike_Crit") ]])
	fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(240)
	fnCutsceneBlocker()
	
	--Transition to the next scene.
	fnCutscene([[ AL_BeginTransitionTo("SprocketCityA", "FORCEPOS:59.0x11.0x0") ]])
	fnCutsceneBlocker()
	
--55 escapes.
elseif(sObjectName == "Spring55") then

	--Variables.
	local iSprung55 = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprung55", "N")
	if(iSprung55 ~= 1.0) then return end

	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSprung55", "N", 2.0)
	VM_SetVar("Root/Variables/Chapter5/Scenes/iKnowsAboutSecretPassage", "N", 1.0)

	--Spawn 55.
	fnSpecialCharacter("Tiffany", 31, 13, gci_Face_South, false, nil)
	fnCutsceneTeleport("Christine", 31.25, 14.50)
	fnCutsceneSetFrame("Christine", "Crouch")

	--Blackout.
	fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	
	--Fade in.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()

	--Movement.
	fnCutsceneMove("Christine", 31.25, 15.50, 0.50)
	fnCutsceneMove("Tiffany", 31.25, 14.50, 0.50)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneMoveFace("Christine", 32.25, 15.50, 0, 1, 0.50)
	fnCutsceneMoveFace("Tiffany", 32.25, 14.50, 0, 1, 0.50)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] *cough*[P] I don't think they're following us...[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] It seems not, but they will likely form a search party soon.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Uncrouch.
	fnCutsceneSetFrame("Christine", "Null")
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Phew![P] That really hurt, 55![B][C]") ]])
	fnCutscene([[ Append("55:[E|Smirk] I needed the performance to be convincing.[P] You should not be permanently damaged.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Ugh...[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] I believe I heard some of the Steam Droids mention a secret passage.[P] Let's find it.[P] I will brief you when we are clear.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneMove("Tiffany", 32.25, 15.50)
	fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 1.0)

	--Lua globals.
	giFollowersTotal = 1
	gsaFollowerNames = {"Tiffany"}
	giaFollowerIDs = {0}

	--Get 55's uniqueID. 
	EM_PushEntity("Tiffany")
		local iCharacterID = RE_GetID()
	DL_PopActiveObject()

	--Store it and tell her to follow.
	giaFollowerIDs = {iCharacterID}
	AL_SetProperty("Follow Actor ID", iCharacterID)

	--Place 55 in the combat lineup.
    AdvCombat_SetProperty("Party Slot", 1, "Tiffany")
	
--Safe spot.
elseif(sObjectName == "GoToSafeSpot" or sObjectName == "GoToSafeSpotAlternate") then

	--Variables.
	local iSprung55 = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprung55", "N")
	if(iSprung55 == 2.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSprung55", "N", 3.0)
		
		--Movement.
		fnCutsceneMove("Christine", 34.25, 37.50)
		fnCutsceneMove("Christine", 37.25, 37.50)
		fnCutsceneFace("Christine", 0, 1)
		fnCutsceneMove("Tiffany", 34.25, 37.50)
		fnCutsceneMove("Tiffany", 37.25, 38.50)
		fnCutsceneFace("Tiffany", 0, -1)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Okay, this should be clear enough.[P] Now, you best have a good reason for putting me in a headlock like that![B][C]") ]])
		fnCutscene([[ Append("55:[E|Smirk] If it was not obvious, I was making sure you did not compromise your cover by being seen with me.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] The trap was unexpected.[P] I had never intended to enter the settlement with you.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] It was my assumption the Steam Droids would think you were an escaped human, and come to accept you.[P] Is that correct?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Yes.[P] I don't think they suspect me.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] They will not trust anyone from Regulus City, and will never believe that I would wipe my own memories.[P] More than likely they would have me scrapped.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] JX-101 mentioned that...[P] but I'm sure if you talk to her - [P][CLEAR]") ]])
		fnCutscene([[ Append("55:[E|Upset] No.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] But - [P][CLEAR]") ]])
		fnCutscene([[ Append("55:[E|Neutral] The Steam Droids do not attack golems strictly for political reasons.[P] If they knew I was a wanted fugitive, they would just as soon turn me in.[P] The administration would likely reward them with parts or technology.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] They have much to gain by betraying me, and much to lose by accepting me.[P] It is best if I am not seen there.[P] You must conduct this diplomacy on your own.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] *sigh*[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Fine.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] So, why did you play dead like that?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Smirk] The Steam Droids lack the technical expertise to evaluate my architecture.[P] I overheard a number of useful facts while they thought I was in standby.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] SX-399 is the daughter of JX-101, and the guards mentioned that she needs to receive special recharging of some sort.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] I did not receive the specifics, but this may be a way to ingratiate her to us.[P] If you can isolate her, we can more easily kidnap her.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Angry] No![P] No kidnapping![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] Look, I'll look into SX-399 and see what I can find.[P] But don't attack anyone![P] Don't make this worse![B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] I will conceal myself here.[P] If you need to venture into the mines, I will accompany you.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Okay, fine, good.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] You know, I'm starting to think you prefer crawling around in the vents and shadows.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Smirk] I am a high-value target to the majority of Regulus' population.[P] Social interactions are dangerous.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Right, so when this is all over, the first thing I'm doing is giving you a crash-course in etiquette.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] I think lesson one should be 'Do not place your friends in headlocks'.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] We should move out, and not dally with formalities.[P] Let's go.") ]])
		fnCutsceneBlocker()
		
		--Fold the party.
		fnCutsceneMove("Tiffany", 37.25, 38.50)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	
	--If 55 is not following, spawn her and add her to the party.
	else
	
		--Variables.
		local iIs55Following         = VM_GetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N")
		local iSpokeWithTT233        = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeWithTT233", "N")
		local iSpokeWith55AboutSX    = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeWith55AboutSX", "N")
		local iTold55Go              = VM_GetVar("Root/Variables/Chapter5/Scenes/iTold55Go", "N")
		local iSteamDroid500RepState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid500RepState", "N")
		if(iIs55Following == 1.0) then return end

		--Spawn 55.
		fnSpecialCharacter("Tiffany", 41, 38, gci_Face_West, false, nil)
		
		--Move 55 out of the wall.
        if(sObjectName == "GoToSafeSpot") then
            fnCutsceneMove("Christine", 34.25, 37.50)
            fnCutsceneFace("Christine", 1, 0)
            for x = 41.25, 40.25, -0.05 do
                fnCutsceneTeleport("Tiffany", x, 38.50)
            end
            fnCutsceneMove("Tiffany", 36.25, 38.50)
            fnCutsceneMove("Tiffany", 35.25, 37.50)
            fnCutsceneFace("Tiffany", -1, 0)
            fnCutsceneBlocker()
        
        --Alternate, using the campfire.
        else
            fnCutsceneFace("Christine", 1, 0)
            for x = 41.25, 40.25, -0.05 do
                fnCutsceneTeleport("Tiffany", x, 38.50)
            end
            fnCutsceneMove("Tiffany", 39.25, 38.50)
            fnCutsceneFace("Tiffany", -1, 0)
            fnCutsceneBlocker()
        end
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue, normal.
		if(iSpokeWithTT233 == 0.0 or iSpokeWith55AboutSX == 1.0) then
			
			--Need to tell 55 that the plan with the black site is a go.
			if(iTold55Go == 0.0 and iSteamDroid500RepState == 1.0) then
				
				--Flag.
				VM_SetVar("Root/Variables/Chapter5/Scenes/iTold55Go", "N", 1.0)
		
				--Dialogue.
				fnCutscene([[ WD_SetProperty("Show") ]])
				fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
				fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
				fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
				fnCutscene([[ Append("55:[E|Smirk] I have seen to it that JX-101 found a suitable location.[B][C]") ]])
				fnCutscene([[ Append("Christine:[E|Neutral] She just got through telling me about it.[P] Everything else is in place?[B][C]") ]])
				fnCutscene([[ Append("55:[E|Neutral] Correct.[P] We will proceed to the site.[B][C]") ]])
				fnCutscene([[ Append("55:[E|Neutral] I will not be able to accompany you, but I will be able to provide network support.[P] I should be able to hack open doors for you.[B][C]") ]])
				fnCutscene([[ Append("55:[E|Smirk] Contact me via your PDU.[P] I have set aside a special frequency for it.[B][C]") ]])
				fnCutscene([[ Append("Christine:[E|Neutral] You know, 55, this is just like old times.[P] Me risking my neck, you opening doors and angrily giving me orders...[B][C]") ]])
				fnCutscene([[ Append("55:[E|Neutral] I am not programmed for nostalgia.[P] Let us proceed.") ]])
				fnCutsceneBlocker()
				
			--Normal.
			else
				fnCutscene([[ WD_SetProperty("Show") ]])
				fnCutscene([[ Append("55:[VOICE|Tiffany] Let us proceed.") ]])
				fnCutsceneBlocker()
			end
			
		--Special:
		else
		
			--Flag.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeWith55AboutSX", "N", 1.0)
		
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Sad") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
			fnCutscene([[ Append("55:[E|Neutral] Your face betrays concern.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Sad] I suppose it's encouraging that you noticed.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] Advertising your emotions is a weakness.[P] Your enemies will use any piece of information they can find to destroy you.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Offended] You love that stonefaced look of yours.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] It is for practical reasons.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] So...[P] I just talked to TT-233.[P] She's one of the mechanics who has tried to work on SX-399.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Sad] She said...[P] She said SX-399 doesn't have long...[B][C]") ]])
			fnCutscene([[ Append("55:[E|Upset] What [P](partial data retrieval error)[P] does that phrase mean?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Sad] She's going to die, 55.[P] Her systems are fried.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Upset] But we.[P][P] Can she.[P][P] We can repair her.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Upset] All units can be repaired.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Sad] TT-233 said they've tried everything.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Upset] We will not allow her to be retired.[P][EMOTION|Tiffany|Angry] You're a repair unit, think of something![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Scared] 55 -[P] 55 let go of me![B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] ...[P][EMOTION|Tiffany|Down] I apologize.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] TT-233 thinks it might be her power distribution system.[P] But she said the only way to find out would likely offline her permanently.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Sad] I mean, if I could get her to Sector 96 we could just hook her to the backup distributor...[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] Then let's take her to Sector 96![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Offended] Woah, calm down there.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Even if we confirm the distribution system is the problem, that means we'd have to replace it.[P] As in, replace the wiring and pumps in her whole body.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] They don't manufacture Steam Droid parts in Regulus City, so I don't know what we'd do even if we got her there...[B][C]") ]])
			fnCutscene([[ Append("55:[E|Smirk] If she were attached to the backup system, she would remain online indefinitely.[P] It is better than letting her expire.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] You're acting a little odd, 55.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] I am concerned about SX-399, as she is our means of acquiring JX-101's aid.[P] If she is offlined, we lose our best chance.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Sad] But taking her to Sector 96 presents problems of its own.[P] You take her, and JX-101 will have every Steam Droid on the moon after her.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Smirk] I am certain I can outmaneuver them.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Sad] It's still too risky...[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Unless...[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] Unless what.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] What if SX-399 never left Sprocket City?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] What if someone took her place while she was away?[B][C]") ]])
			fnCutscene([[ Append("55:[E|Upset] Constructing a crude replacement dummy would not work.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Offended] No, you lug-nut, me![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] I get her to transform me into a Steam Droid, and then we disguise me to look like her.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] I'll take her place, and you can take her to the repair bay.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] But you'll be here, therefore, unable to maintain SX-399.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Laugh] Sophie's twice the repair unit I am![P] I'll send her a message from the PDU![B][C]") ]])
			fnCutscene([[ Append("55:[E|Down] This plan...[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] It'll work.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] I was not doubting it.[P] I am merely unsure about SX-399's fate.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] She'll be in the best of hands.[P] I trust Sophie, I know she'll know what to do.[P] Come on, let's go tell her.") ]])
			fnCutsceneBlocker()
		
		end
	
		--Move 55 and fold the party.
        if(sObjectName == "GoToSafeSpot") then
            fnCutsceneMove("Tiffany", 34.25, 37.50)
        else
            fnCutsceneMove("Tiffany", 38.25, 38.50)
        end
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()

		--Lua globals.
		giFollowersTotal = 1
		gsaFollowerNames = {"Tiffany"}
		giaFollowerIDs = {0}

		--Get 55's uniqueID. 
		EM_PushEntity("Tiffany")
			local iCharacterID = RE_GetID()
		DL_PopActiveObject()

		--Store it and tell her to follow.
		giaFollowerIDs = {iCharacterID}
		AL_SetProperty("Follow Actor ID", iCharacterID)

		--Place 55 in the combat lineup. She will not be added to the party if already in it.
        AdvCombat_SetProperty("Party Slot", 1, "Tiffany")
		VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 1.0)
	
	end
end
