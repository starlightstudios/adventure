-- |[Warp Handler]|
--If the player warps here using the campfire, this script is called to reposition them as necessary.

--Constants. This is the important part that changes between each room.
local fTargetX = (38.25 * gciSizePerTile)
local fTargetY = (38.50 * gciSizePerTile)

--If 55 was not in the party when the warp started, add her.
local bWas55PresentAtStart = fnIsCharacterPresent("Tiffany")
if(bWas55PresentAtStart == false) then
    --fnAddPartyMember("Tiffany")
end

--Execute.
local bAlreadySpawned55 = false
local iSaidWarpDialogue = VM_GetVar("Root/Variables/Global/2855/iSaidWarpDialogue", "N")
if(bWas55PresentAtStart == false) then
    
    --Spawn 55.
    fnSpecialCharacter("Tiffany", 39, 38, gci_Face_South, false, nil)

	--Lua globals.
	giFollowersTotal = 1
	gsaFollowerNames = {"Tiffany"}
	giaFollowerIDs = {0}

	--Get 55's uniqueID. 
	EM_PushEntity("Tiffany")
		local iCharacterID = RE_GetID()
	DL_PopActiveObject()

	--Store it and tell her to follow.
	giaFollowerIDs = {iCharacterID}
	AL_SetProperty("Follow Actor ID", iCharacterID)
    bAlreadySpawned55 = true
end

--Handler.
LM_ExecuteScript(gsStandardWarpHandler, fTargetX, fTargetY)

--If 55 was not present before the warp started, and we have not seen this dialogue yet, show it.
if(iSaidWarpDialogue == 0.0 and bWas55PresentAtStart == false) then
    
    --Flag.
    VM_SetVar("Root/Variables/Global/2855/iSaidWarpDialogue", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 1.0)
    
    --Handler.
    fnCutsceneMoveFace("Tiffany", 39.25, 38.50, 0, -1, 0.20)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Tiffany", "Downed")
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] !!![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] 55?[P] Are you okay?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Interesting.[P] The reports *did* indicate your runestone allowed limited-scope teleportation of an unknown mechanism.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Then why are you surprised?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Because I was attempting to climb a ladder when this happened.[P] I was not, as is usual, standing right next to the teleportation vector.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] In most circumstances, the caster is in physical contact with any passengers.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] Oh...[P] Yes, I suppose you weren't.[P] Sorry.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Maybe my rune's magic pulled you along because you're such a close friend?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smug] An unproveable supposition.[P] You are not the ideal candidate to advance the Cause of Science.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Gee, thanks, 55.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] The magic on the runestone has proved convenient for our purposes.[P] I am undamaged.[P] Let us continue.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fold the party.
    fnCutsceneSetFrame("Tiffany", "Null")
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Tiffany", -1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Tiffany", 38.25, 38.50)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Fold Party") ]])
    fnCutsceneBlocker()

--No need to play this scene, but also don't need to show 55 walking up.
elseif(iSaidWarpDialogue == 1.0 and bWas55PresentAtStart == false) then
    VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 1.0)
    if(bAlreadySpawned55 == false) then
        fnSpecialCharacter("Tiffany", 38, 38, gci_Face_South, false, nil)
    end
    fnCutsceneTeleport("Tiffany", 38.25, 38.50)

end
