-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
--Next randomly generated level. Premade levels cannot generate below level 40 anyway.
if(sObjectName == "LadderD") then
    
    --Can't leave until the elevator has been fixed.
    local iSawMinesGCutscenePost = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMinesGCutscenePost", "N")
    if(iSawMinesGCutscenePost < 2.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I probably shouldn't leave this floor until I've attended to those Steam Droids over there...)") ]])
        fnCutsceneBlocker()
        return
    end

    -- |[Subscript]|
    --Run this script. If the variable gbCaughtScript is thrown to true, stop there.
    gbCaughtScript = false
    LM_ExecuteScript(gsRandomExaminationHandler, sObjectName)
    if(gbCaughtScript == true) then
        gbCaughtScript = nil
        return
    end

    --Clear.
    gbCaughtScript = nil

--Back to the entrance.
elseif(sObjectName == "LadderU") then
    
    --Can't leave until the elevator has been fixed.
    local iSawMinesGCutscenePost = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMinesGCutscenePost", "N")
    if(iSawMinesGCutscenePost < 2.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I probably shouldn't leave this floor until I've attended to those Steam Droids over there...)") ]])
        fnCutsceneBlocker()
        return
    end

    -- |[Subscript]|
    --Run this script. If the variable gbCaughtScript is thrown to true, stop there.
    gbCaughtScript = false
    LM_ExecuteScript(gsRandomExaminationHandler, sObjectName)
    if(gbCaughtScript == true) then
        gbCaughtScript = nil
        return
    end

    --Clear.
    gbCaughtScript = nil

--Elevator, select any of the visited floors in increments of 10.
elseif(sObjectName == "Elevator") then

    --Variables.
    local iFixedElevator40 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFixedElevator40", "N")
    if(iFixedElevator40 == 0.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iFixedElevator40", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Someone jammed a pipe into the hydraulic motivator.[P] That was pretty easy to diagnose...[P] but if I wasn't a repair unit, I'd have no idea how to fix that!)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (All fixed![P] Now I can travel to Floor 40 quickly by accessing the elevator!)") ]])
        fnCutsceneBlocker()
    else
	
        --Can't leave until the elevator has been fixed.
        local iSawMinesGCutscenePost = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMinesGCutscenePost", "N")
        if(iSawMinesGCutscenePost < 2.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (I probably shouldn't leave this floor until I've attended to those Steam Droids over there...)") ]])
            fnCutsceneBlocker()
            return
        end
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Take the elevator back to the surface?)[BLOCK]") ]])

        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"YesE\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"No\") ")
        fnCutsceneBlocker()
        
    end

-- |[Examinables]|
-- |[Decisions]|
--Really exit the mines.
elseif(sObjectName == "Yes") then

	--Go to Mines B.
	WD_SetProperty("Hide")
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("TelluriumMinesB", "FORCEPOS:19.0x24.0x0")

--Really exit the mines.
elseif(sObjectName == "YesE") then

	--Go to Mines B.
	WD_SetProperty("Hide")
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("TelluriumMinesB", "FORCEPOS:15.5x23.0x0")

--Nope.
elseif(sObjectName == "No") then
	WD_SetProperty("Hide")
    
-- |[Skillbook]|
elseif(sObjectName == "Skillbook") then
    AL_SetProperty("Set Layer Disabled", "SkillbookIco", true)
	LM_ExecuteScript(gsTiffanySkillbook, 4)

-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
