-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    -- |[DroidGuardA]|
    if(sActorName == "DroidGuardA") then
        
        --Variables.
        local iMinesGDroidKnowsChristine = VM_GetVar("Root/Variables/Chapter5/Scenes/iMinesGDroidKnowsChristine", "N")
        local iCompletedSprocketCity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSprocketCity", "N")
        if(iMinesGDroidKnowsChristine == 0.0 and iCompletedSprocketCity == 1.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iMinesGDroidKnowsChristine", "N", 1.0)
        
            --Dialogue.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Hey, if I told you I was Christine, and that this was 55, would that mean anything to you?[B][C]") ]])
            fnCutscene([[ Append("Steam Droid:[E|Neutral] ...[P] Seriously?[B][C]") ]])
            fnCutscene([[ Append("Steam Droid:[E|Neutral] Yeah, yeah, that'd make sense.[P] I've heard about you.[P] Transforming hero of Sprocket City.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Sorry about earlier, we had to keep my identity a secret.[P] I wasn't publically with 55 at the time.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] It was unlikely I would be accepted by Steam Droid society, regardless of my sincerity.[B][C]") ]])
            fnCutscene([[ Append("Steam Droid:[E|Neutral] Can't fault you for that, I suppose.[P] I was ready to plug you when I saw you come down the ladder.[B][C]") ]])
            fnCutscene([[ Append("Steam Droid:[E|Neutral] Just stay alive, yeah?[P] It's rare to find new friends in the mines, I'd hate to lose you to a bunch of wandering creeps.") ]])
            fnCutsceneBlocker()
            
            local iFixedElevator40 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFixedElevator40", "N")
            if(iFixedElevator40 == 1.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesGCutscenePost", "N", 2.0)
            end
        
        --Does not know Christine, has not finished Sprocket City.
        elseif(iMinesGDroidKnowsChristine == 0.0 and iCompletedSprocketCity == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] Try not to stay close to the creeps for too long.[P] I don't much care for what happens to those who do.[B][C]") ]])
            fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] Don't give me a reason to shoot you, yeah?") ]])
            fnCutsceneBlocker()
            
            local iFixedElevator40 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFixedElevator40", "N")
            if(iFixedElevator40 == 1.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesGCutscenePost", "N", 2.0)
            end
        
        --Knows Christine.
        elseif(iMinesGDroidKnowsChristine == 1.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] Good hunting out there, kiddo.[P] Make Sprocket City proud!") ]])
            fnCutsceneBlocker()
            
            local iFixedElevator40 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFixedElevator40", "N")
            if(iFixedElevator40 == 1.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesGCutscenePost", "N", 2.0)
            end
        end

    -- |[DroidGuardB]|
    elseif(sActorName == "DroidGuardB") then
        
        --Variables.
        local iMinesGDroidKnowsChristine = VM_GetVar("Root/Variables/Chapter5/Scenes/iMinesGDroidKnowsChristine", "N")
        
        --Does not know Christine
        if(iMinesGDroidKnowsChristine == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] You've probably already faced a lot of weird things on the way here.[P] Stay normal.") ]])
            fnCutsceneBlocker()
        
        --Knows Christine.
        elseif(iMinesGDroidKnowsChristine == 1.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] Jeez, if the famous Christine and 55 are here, that means we're in real trouble, doesn't it?[P] No offense.") ]])
            fnCutsceneBlocker()
        end
    
    -- |[DroidMedic]|
    elseif(sActorName == "DroidMedic") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] This golem is a little beaten up, but her internals are fine.[P] I told her to go into standby to conserve power.") ]])
        fnCutsceneBlocker()
            
    -- |[GolemA]|
    elseif(sActorName == "GolemA") then
        
        --Variables.
        local iTalkedToGGolem = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToGGolem", "N")
        local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
        
        --First time.
        if(iTalkedToGGolem == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedToGGolem", "N", 1.0)
        
            --Dialogue.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Pack") ]])
            fnCutscene([[ Append("Golem:[E|Pack] Hello, Lord Golem. So this is it?[B][C]") ]])
            if(sChristineForm == "Golem") then
                fnCutscene([[ Append("Golem:[E|Pack] But you are not just a Lord Golem, are you?[B][C]") ]])
                fnCutscene([[ Append("Golem:[E|Pack] I can see the real you.[P] I can see all of them, all at once.[P] I'm sorry...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] Yes, I can see that there in no point in lying.[P] You've been touched, haven't you?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Touched?[B][C]") ]])
                fnCutscene([[ Append("Golem:[E|Pack] Just a little.[P] We were running, and one of them grabbed my arm.[P] I pulled and I pulled, and then I saw something.[B][C]") ]])
                fnCutscene([[ Append("Golem:[E|Pack] In the dark, a shadow that hid nothing, I saw three eyes.[P] They were looking me up and down.[B][C]") ]])
                fnCutscene([[ Append("Golem:[E|Pack] I've been seeing things since then, things as they are, or could be.[P] And the eyes -[P] I still see the eyes...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] I'm sorry, I don't think I can help you.[B][C]") ]])
            
            else
                fnCutscene([[ Append("Christine:[E|Neutral] Ah, but I am not a Lord Golem...[B][C]") ]])
                fnCutscene([[ Append("Golem:[E|Pack] You are.[P] I can see the real you.[P] I can see all of them, all at once.[P] I'm sorry...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] Yes, I can see that.[P] You've been touched, haven't you?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Touched?[B][C]") ]])
                fnCutscene([[ Append("Golem:[E|Pack] Just a little.[P] We were running, and one of them grabbed my arm.[P] I pulled and I pulled, and then I saw something.[B][C]") ]])
                fnCutscene([[ Append("Golem:[E|Pack] In the dark, a shadow that hid nothing, I saw three eyes.[P] They were looking me up and down.[B][C]") ]])
                fnCutscene([[ Append("Golem:[E|Pack] I've been seeing things since then, things as they are, or could be.[P] And the eyes -[P] I still see the eyes...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] I'm sorry, I don't think I can help you.[B][C]") ]])
            end
            
            --Rejoin.
            fnCutscene([[ Append("Golem:[E|Pack] Are you here to retire us?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] No...[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Pack] I was asking Unit 2855.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I am not.[P] If you are mavericks, I am no longer with the security services.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Pack] Hmmm, well.[P] If you were to try to retire us, we wouldn't stop you.[P] We're done running and fighting.[P] Done.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] What happened?[P] How did you get here?[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Pack] We used to work in the Cryogenics Facility, south of Regulus City.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Scared] ...![B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Pack] [EMOTION|Christine|Sad]Yes, yes, I understand.[P] You know what happened there.[P] You're the one she was talking about.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Pack] Our Lord Golem, Unit 609144, told us to follow her.[P] She seemed so complacent.[P] The security teams were on their way, but she just said 'Follow me'.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Pack] We left when the shooting started.[P] A few stray pulse shots whizzed by us but the Lord Golem just kept walking.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Pack] We followed her down the tram tracks, through the caves, deeper and deeper and deeper.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Pack] I was scared, but I was too scared to run away.[P] We just kept following.[P] And then the rock became some kind of meat, and Unit 609144 started laughing.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Pack] She laughed and those things just came from the walls.[P] They just melted out and surrounded us.[P] I bolted.[P] My two friends here ran as soon as I did.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Pack] And we ran and hid and climbed...[P] We've been doing it for so long my chronometer lost track of time.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Pack] These Steam Droids found us.[P] I think they want to shoot us, but I don't care any more.[P] All I needed to do was make sure you, Christine, knew.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Pack] She's still down there.[P] On floor 50.[P] Waiting for you.[P] I can still hear her giggling.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] She knows who I am?[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Pack] Yes.[P] She said your name when we first left Cryogenics.[P] Said we were going to wait for you.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] (But that would be before I was even on Regulus...)[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] A cryptic warning and an invitation to a trap?[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Pack] Unit 2855, please listen.[P] You will need to be able to defend yourself.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] You would imply I cannot?[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Pack] You won't be able to avoid her.[P] You'll need to hide.[P] To take cover.[P] Please, please.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Pack] If you don't stop her, I'll never stop hearing her laugh...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Hrmph.[P] I am not predisposed to listen to combat advice from a non-combat unit.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] It's not bad advice, 55.[P] I think this unit knows what she's talking about.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] I just hope you'll be able to find cover when we find this Unit 609144...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] So long as I'm near you, cover will not be in short supply.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Gee, thanks, 55.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Pack] Please stop her, Christine.[P] Please...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I haven't met a robot yet who I couldn't best.[P] You won't suffer much longer.[P] Let's go, 55.") ]])
        
        --Repeats.
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Pack") ]])
            fnCutscene([[ Append("Golem:[E|Pack] Please, Christine.[P] We have nothing to offer you but our gratitude.[P] Please...") ]])
        end
        
    -- |[GolemB]|
    elseif(sActorName == "GolemB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] We're sick of running and hiding...") ]])
        fnCutsceneBlocker()
    end
end
