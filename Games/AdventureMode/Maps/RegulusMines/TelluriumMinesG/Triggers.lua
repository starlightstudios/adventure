-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[ ======================================== Triggers ======================================== ]|
--Start the mines cutscene.
if(sObjectName == "StartCutscene") then

    --Variables.
    local iSawMinesGCutscene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMinesGCutscene", "N")
    if(iSawMinesGCutscene == 1.0) then return end
    
    --More variables.
    local iHasSteamDroidForm = VM_GetVar("Root/Variables/Global/Christine/iHasSteamDroidForm", "N")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesGCutscene", "N", 1.0)
    
    --Movement.
    fnCutsceneMove("Christine", 9.25, 15.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneMove("Tiffany", 8.25, 15.50)
    fnCutsceneFace("Tiffany", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
    fnCutscene([[ Append("Steam Droid:[E|Neutral] Contact![P] Two from the north![P] Get down![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Scared] Wait![P] We come in peace![B][C]") ]])
    fnCutscene([[ Append("Steam Droid:[E|Neutral] Hands up![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Weapons down, 55.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Negotiating is your strong point.[P] However I would like to point out that pneumatic weaponry is not a threat to my chassis.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Yes, pointing out your invulnerability is sure to inspire trust.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] We're friends.[P] May we come closer?[B][C]") ]])
    fnCutscene([[ Append("Steam Droid:[E|Neutral] Make a fast move and I'll gun you down.[P] Nice and easy now.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 10.25, 16.50, 0.75)
    fnCutsceneMove("Christine", 10.25, 18.50, 0.75)
    fnCutsceneMove("Tiffany", 9.25, 16.50, 0.75)
    fnCutsceneMove("Tiffany", 9.25, 18.50, 0.75)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Christine has Steam Droid form. This means Sprocket City knows about her.
    if(iHasSteamDroidForm == 1.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMinesGDroidKnowsChristine", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesGCutscenePost", "N", 2.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Identify yourself.[P] Who are you?[P] What are you doing here?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] If I said I was Christine, and that this is 55, would that mean anything to you?[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] ...[P] Christine?[P] The one from Sprocket City?[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Who busted up the Black Site?[P] Who saved SX-399?[P] Who is practically JX-101's right-hand?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Laugh] I didn't think I was that famous![B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] You do right by us, it gets around.[P] Yes, we've heard of Christine.[P] And yes, we've heard of 55.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] I would prefer if you hadn't.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] She's a little gruff, don't mind her.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Well, we're in a bit of a tough spot.[P] Are you here to help again?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] What's the problem?[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] This is about as far as we can go down.[P] After this, things start to get too hairy.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] We found a couple of golems earlier.[P] They keep saying something bad is ten floors down.[P] Something we need to deal with.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] I really just came down here to fix the elevator.[P] But I guess some cleanup duty could be added.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Fix the elevator?[P] You mean we can get back to Sprocket City without having to fight our way up twenty floors?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Uh, yeah?[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Now I know why Sprocket City's folk mention you every time we swing by there.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] What can I say?[P] I like helping.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Talk to the golems we have over there.[P] If what they say is true, your services might be needed below.[P] Good luck.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
    --All other forms:
    else
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesGCutscenePost", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Identify yourself.[P] Who are you?[P] What are you doing here?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I'm not really sure if I should tell you who I am.[P] To protect myself, and my friend here.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] You're going to need to prove I can trust you before I let you walk away from here.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Oh that'll be easy.[P] I was just going to fix the elevator over there.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Then you'll be able to use it to get around easily.[P] Just don't go to the top floor because the golems will probably shoot you.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] ...[P] You're serious?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Probably not much point in stopping me.[P] It's not like you can use the broken elevator anyway.[P] Let me take a look at it for you.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Don't try anything.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] We won't.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
    end

    --Fold the party up.
    fnCutsceneMove("Tiffany", 10.25, 18.50)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Fold Party") ]])
    fnCutsceneBlocker()

--Second cutscene.
elseif(sObjectName == "StartCutscene2") then

    --Variables.
    local iSawMinesGCutscenePost = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMinesGCutscenePost", "N")
    if(iSawMinesGCutscenePost == 0.0 or iSawMinesGCutscenePost == 2.0) then return end
    
    --Elevator needs to be fixed.
    local iFixedElevator40 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFixedElevator40", "N")
    if(iFixedElevator40 ~= 1.0) then return end
    
    --More variables.
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesGCutscenePost", "N", 2.0)
    
    --Movement.
    fnCutsceneMove("Christine", 13.25, 20.50)
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneMove("Tiffany", 13.25, 21.50)
    fnCutsceneFace("Tiffany", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneFace("DroidGuardA", 1, 0)
    fnCutsceneFace("DroidGuardB", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Elevator's fixed.[B][C]") ]])
    fnCutscene([[ Append("Steam Droid:[E|Neutral] While I will wait until I've actually checked it myself for that judgement, you didn't obviously sabotage it.[B][C]") ]])
    
    --Human Form:
    if(sChristineForm == "Human") then
        fnCutscene([[ Append("Steam Droid:[E|Neutral] I don't know if you're a runaway or something, but your Command Unit friend isn't inspiring confidence.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] She's a maverick.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Yeah, sure.[P] Whatever you say.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] So listen up, human.[P] If you're really committed to helping, these golems we captured have been saying there's something ten floors down we need to deal with.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] We're not leaving this position.[P] You want to prove yourself?[P] Ladder's right over there.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Hmmm...[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Talk to them, see what details you can get out of them.[P] Try not to get yourself killed down there.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] And be careful.[P] You humans are probably fresh recruits for the things crawling in the mines.[P] I don't need more enemies.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Don't worry about us, we can handle ourselves.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
    --Golem Form:
    elseif(sChristineForm == "Golem") then
        fnCutscene([[ Append("Steam Droid:[E|Neutral] So are you two mavericks or something?[P] Foes of Regulus City?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] That's precisely the case![P] Though don't spread it around.[P] I don't want the other golems knowing.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Oh yeah, I totally believe you.[P] Really.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] So listen up, then.[P] If you're really committed to helping, these golems we captured have been saying there's something ten floors down we need to deal with.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] We're not leaving this position.[P] You want to prove yourself?[P] Ladder's right over there.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Hmmm...[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Talk to them, see what details you can get out of them.[P] Try not to get yourself killed down there.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] And be careful.[P] We've seen other golems down here, and they aren't exactly lucid.[P] Whatever happens to them can happen to you.[P] I don't need more enemies.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Don't worry about us, we can handle ourselves.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
    --Latex Drone:
    elseif(sChristineForm == "LatexDrone") then
        fnCutscene([[ Append("Steam Droid:[E|Neutral] So are you two mavericks or something?[P] Foes of Regulus City?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] That's precisely the case![P] Appearances can be deceiving.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Oh yeah, I totally believe you.[P] Really.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] So listen up, then.[P] If you're really committed to helping, these golems we captured have been saying there's something ten floors down we need to deal with.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] We're not leaving this position.[P] You want to prove yourself?[P] Ladder's right over there.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Hmmm...[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Talk to them, see what details you can get out of them.[P] Try not to get yourself killed down there.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] And be careful.[P] We've seen other Regulus City types down here, and they aren't exactly lucid.[P] Whatever happens to them can happen to you.[P] I don't need more enemies.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Don't worry about us, we can handle ourselves.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
    --Other:
    else
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Not to be too judgemental, but I've seen a lot of weird stuff lately.[P] Sorry if I jumped to conclusions about -[P] whatever you are.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Appearances can be deceiving.[P] I don't mean to intimidate you.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Doesn't mean I trust you, yet.[P] But you're on the right track.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] So listen up, then.[P] If you're really committed to helping, these golems we captured have been saying there's something ten floors down we need to deal with.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] We're not leaving this position.[P] You want to prove yourself?[P] Ladder's right over there.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Hmmm...[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Talk to them, see what details you can get out of them.[P] Try not to get yourself killed down there.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] And be careful.[P] Things in the mines can mess with your head.[P] I've had to shoot fellow Steam Droids, I'd rather not have to shoot you if I can avoid it.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Don't worry about us, we can handle ourselves.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
    end

    --Fold the party up.
    fnCutsceneMove("Tiffany", 13.25, 20.50)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Fold Party") ]])
    fnCutsceneBlocker()
end
