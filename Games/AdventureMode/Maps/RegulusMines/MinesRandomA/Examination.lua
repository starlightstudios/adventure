-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Subscript]|
--Run this script. If the variable gbCaughtScript is thrown to true, stop there.
gbCaughtScript = false
LM_ExecuteScript(gsRandomExaminationHandler, sObjectName)
if(gbCaughtScript == true) then
    gbCaughtScript = nil
    return
end

--Clear.
gbCaughtScript = nil

-- |[Examinables]|
if(sObjectName == "Smelters") then

    --Variables.
    local iMineASmelters = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineASmelters", "N")
    if(iMineASmelters == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMineASmelters", "N", 1.0)
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Tiffany", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "PDU", "Neutral") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Hmm, are these electrical smelters?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|PDU] PDU, give me a local area scan.[P] What ore is this?[B][C]") ]])
        fnCutscene([[ Append("PDU:[E|Neutral] Scanning...[P] Done![B][C]") ]])
        fnCutscene([[ Append("PDU:[E|Alarmed] Ore is a rare mineral known as abrissite.[P] It is a particularly good source of Aluminum, Cobalt, Tantalum, and Yttrium.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] But these are primitive smelters, and -[P] shovels?[P] Were they hand-operating these?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Abrissite occurs in small nodules, no more than a few meters across.[P] It is extremely highly concentrated but in small areas.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] This mining facility was decommissioned.[P] What we see around us is probably lower-grade ores or slag.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] So they just left it all down here?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Carrying materials to the surface costs a significant amount of energy and time.[P] Anything not worth the cost was abandoned.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] [SOUND|World|TakeItem]Maybe I'll just take some of this leftover abrissite...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] May as well look around.[P] Maybe they left something else we can use.") ]])
        fnCutsceneBlocker()
    
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Electrical smelters.[P] The heating coils were stripped out, but the heavy casing is still here.)") ]])
        fnCutsceneBlocker()
    
    end
    
elseif(sObjectName == "Pod") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Defragmentation pods used by the miners.[P] The electrical parts were stripped out.)") ]])
    fnCutsceneBlocker()
        
elseif(sObjectName == "Fabricator") then
    
    --Variables.
    local iMineAFabricators = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineAFabricators", "N")
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMineAFabricators", "N", 1.0)
    
    if(iMineAFabricators == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (A fabrication bench.[P] The built-in toolbox was stripped.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] [SOUND|World|TakeItem](Some broken tools were left behind.[P] One robot's trash is another's treasure!)") ]])
        LM_ExecuteScript(gsItemListing, "Bent Tools")
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (A fabrication bench.[P] The built-in toolbox was stripped.)") ]])
    end
    
elseif(sObjectName == "Crates") then
    
    --Variables.
    local iMineACrates = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineACrates", "N")
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMineACrates", "N", 1.0)
    
    if(iMineACrates == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Storage crates.[P] Mostly contains damaged parts not worth carrying.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] [SOUND|World|TakeItem](I'm sure we could find a use for some of this...)") ]])
        LM_ExecuteScript(gsItemListing, "Assorted Parts")
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Storage crates.[P] Mostly contains damaged parts not worth carrying.)") ]])
    end
    
elseif(sObjectName == "Defragment") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Looks like the Lord Golem's defragmentation chamber and attached terminal.[P] All the useful electrical parts were stripped out.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Terminal") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A terminal left behind. The processor, power supply -[P] even the cooling fans were stripped out.[P] All that's left is the casing.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "FakeExit") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This door isn't receiving power, and the deadbolts have been thrown.[P] We're not getting it open without a blasting charge.)") ]])
    fnCutsceneBlocker()

-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
