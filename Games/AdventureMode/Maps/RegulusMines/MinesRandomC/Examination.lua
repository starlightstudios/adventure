-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Subscript]|
--Run this script. If the variable gbCaughtScript is thrown to true, stop there.
gbCaughtScript = false
LM_ExecuteScript(gsRandomExaminationHandler, sObjectName)
if(gbCaughtScript == true) then
    gbCaughtScript = nil
    return
end

--Clear.
gbCaughtScript = nil

-- |[Examinables]|
if(sObjectName == "Penkak") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A chessboard, though it's called Penkak on Pandemonium.[P] The board is here but the pieces are nowhere to be found.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "ShelvesA") then

    --Variables.
    local iMineCShelves = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineCShelves", "N")
    if(iMineCShelves == 0.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMineCShelves", "N", 1.0)
        LM_ExecuteScript(gsItemListing, "Recycleable Junk")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ([SOUND|World|TakeItem]Some junk on this shelf could probably be recycled...)") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Nothing else on the shelf looks salvageable.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "ShelvesB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Blank blueprints and useless junk.[P] Nothing useful.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The computer had its drives bricked in a hurry.[P] There's nothing useful here.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This computer had most of its internal parts ripped out.[P] Looks like it was done as a rush job.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (There's several incendiary bullet trails through the processor and drives.[P] Guess they melted what they couldn't carry.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Books") then

    --Variables.
    local iMineCFoundMemento = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineCFoundMemento", "N")
    if(iMineCFoundMemento == 0.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMineCFoundMemento", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Assorted books in very poor condition...[P] wait what's this?)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] [SOUND|World|TakeItem](There was a small unicorn figurine tucked between some of the books.[P] I'll take it with me.)") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Just a bunch of books that are falling apart.[P] Nothing useful.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "Poster") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A recruitment poster for 'Fist of Tomorrow'.[P] It looks pretty old.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "EscapeRoute") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Unit 2855, strategic assessment?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] This is likely a reserve escape route for the inhabitants of this outpost.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] While it is too dark to see the base, I would expect the tunnel beneath here was set to be collapsed when the evacuation order was completed.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] It would be unwise to simply leap down the shaft, as climbing back out may be difficult.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I wasn't intending to.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Fortunately it doesn't seem any Steam Droids got left behind.[P] Looks like their escape route worked.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] That was my assessment as well.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] They seem to have done a good job of destroying what they could not carry and prioritizing high-value items.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smirk] Whichever unit was in charge of security performed admirably, considering the limited Steam Droid resources.[P] We should see about recruiting her.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Noted.[P] Moving on...") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "Note") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (To RS-56::[P] If you're reading this note, then meet up with us at the spot.[P] You know the one.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (We'll wait there for a 36 hours and then make our way to Pipe Junction Point.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Don't bother searching for anything, we grabbed everything useful and cleared out.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (I couldn't find RT-88's memento -[P] the one you made her last year? -[P] but you can just make her another one.[P] It's not worth the risk.[P] See you soon.)") ]])
    fnCutsceneBlocker()

-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
