-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Start the mines cutscene.
if(sObjectName == "StartCutscene") then

	--Variables.
	local iCutscene50 = VM_GetVar("Root/Variables/Chapter5/Scenes/iCutscene50", "N")
	if(iCutscene50 == 1.0) then return end
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iCutscene50", "N", 1.0)
    
    --More Variables.
    local iHasDarkmatterForm = VM_GetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N")
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    
    --Dialogue.
    if(iHasDarkmatterForm == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] 55...[P] I've been here before.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] When?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] No, not before...[P] after.[P] I've been here after...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] In the future.[P] I spent millions of years here...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] I will wander in a haze.[P] Joining the flesh, unjoining the flesh.[P] Part and not part.[P] Forever.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] The stalks grow and grow to infinity and I look up and see -[P] the great billowing cloud of One.[P] Master.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Unit 771852, cognitive systems check.[B][C]") ]])
        if(sChristineForm == "Human") then
            fnCutscene([[ Append("Christine:[E|Offended] I can't do a cognitive systems check on wetware, 55.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I'm not feeling ill, or seeing things or anything.[P] I'm just remembering something that hasn't happened yet.[B][C]") ]])
            
        elseif(sChristineForm == "Golem" or sChristineForm == "LatexDrone" or sChristineForm == "SteamDroid" or sChristineForm == "Doll") then
            fnCutscene([[ Append("Christine:[E|Offended] All checksums are green, 55.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I'm not feeling ill, or seeing things or anything.[P] I'm just remembering something that hasn't happened yet.[B][C]") ]])
            
        else
            fnCutscene([[ Append("Christine:[E|Offended] Cognitive systems?[P] What a quaint notion.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I'm not feeling ill, or seeing things or anything.[P] I'm just remembering something that hasn't happened yet.[B][C]") ]])
        end
        
        --Resume.
        fnCutscene([[ Append("55:[E|Upset] So you claim.[P] Focus on the present, not the future.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] If you have been here before as you assert, you can tell what is waiting ahead.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] No, it's not like that...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] This place is young.[P] So young.[P] The walls do not bleed, there are no hands.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] 'He' is not here, yet.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] He?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] ...[P] I don't know.[P] I don't know who 'He' is, or even if 'He' is the right term.[P] But 'He' is not here yet.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] But there is someone waiting for us up ahead.[P] We will have to destroy her.[P] It won't matter, but we'll do it.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] We should turn back now.[P] We have nothing to gain from this.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] And yet, somehow, we're going to go ahead.[P] I know it.[P] I will, I have, I always have.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Then lead the way.[P] I will be monitoring you very closely.") ]])
        fnCutsceneBlocker()
    
    --Has Darkmatter form.
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] 55...[P] I've been here before.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I recall your report about walls made of meat from the Serenity Crater incident.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Yes, but -[P] that place was not here.[P] It was not even in this dimension, this universe.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] This is that other universe, making its place here.[P] Taking it.[P] We are not there, but we will be.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] So you have not been here before?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] No, not before...[P] after.[P] I've been here after...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] In the future.[P] I spent millions of years here...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] I will wander in a haze.[P] Joining the flesh, unjoining the flesh.[P] Part and not part.[P] Forever.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] The stalks grow and grow to infinity and I look up and see -[P] the great billowing cloud of One.[P] Master.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Unit 771852, cognitive systems check.[B][C]") ]])
        if(sChristineForm == "Human") then
            fnCutscene([[ Append("Christine:[E|Offended] I can't do a cognitive systems check on wetware, 55.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I'm not feeling ill, or seeing things or anything.[P] I'm just remembering something that hasn't happened yet.[B][C]") ]])
            
        elseif(sChristineForm == "Golem" or sChristineForm == "LatexDrone" or sChristineForm == "SteamDroid") then
            fnCutscene([[ Append("Christine:[E|Offended] All checksums are green, 55.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I'm not feeling ill, or seeing things or anything.[P] I'm just remembering something that hasn't happened yet.[B][C]") ]])
            
        else
            fnCutscene([[ Append("Christine:[E|Offended] Cognitive systems?[P] What a quaint notion.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I'm not feeling ill, or seeing things or anything.[P] I'm just remembering something that hasn't happened yet.[B][C]") ]])
        end
        
        --Resume.
        fnCutscene([[ Append("55:[E|Upset] So you claim.[P] Focus on the present, not the future.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] If you have been here before as you assert, you can tell what is waiting ahead.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] No, it's not like that...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] This place is young.[P] So young.[P] The walls do not bleed, there are no hands.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] 'He' is not here, yet.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] He?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] ...[P] I don't know.[P] I don't know who 'He' is, or even if 'He' is the right term.[P] But 'He' is not here yet.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] But there is someone waiting for us up ahead.[P] We will have to destroy her.[P] It won't matter, but we'll do it.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] We should turn back now.[P] We have nothing to gain from this.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] And yet, somehow, we're going to go ahead.[P] I know it.[P] I will, I have, I always have.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Then lead the way.[P] I will be monitoring you very closely.") ]])
        fnCutsceneBlocker()
    
    end
end
