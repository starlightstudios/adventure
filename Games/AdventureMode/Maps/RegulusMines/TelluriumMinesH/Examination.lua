-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Objects]|
--Ladder. Goes up to the surface.
if(sObjectName == "LadderU") then
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] (Climb the ladder back to the surface?)[BLOCK]") ]])

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"No\") ")
	fnCutsceneBlocker()

    
--Elevator, select any of the visited floors in increments of 10.
elseif(sObjectName == "Elevator") then

    --Variables.
    local iFixedElevator50 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFixedElevator50", "N")
    if(iFixedElevator50 == 0.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iFixedElevator50", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The elevator was already functional...[P] and it also doesn't go down 50 floors.[P] And yet, it's here.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (It will work now. That is all there is to it.)") ]])
        fnCutsceneBlocker()
    else
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Take the elevator back to the surface?)[BLOCK]") ]])

        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"YesE\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"No\") ")
        fnCutsceneBlocker()
        
    end
    
-- |[Decisions]|
--Really exit the mines.
elseif(sObjectName == "Yes") then

	--Go to Mines B.
	WD_SetProperty("Hide")
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("TelluriumMinesB", "FORCEPOS:19.0x24.0x0")

--Really exit the mines.
elseif(sObjectName == "YesE") then

	--Go to Mines B.
	WD_SetProperty("Hide")
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("TelluriumMinesB", "FORCEPOS:15.5x23.0x0")

--Nope.
elseif(sObjectName == "No") then
	WD_SetProperty("Hide")
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
