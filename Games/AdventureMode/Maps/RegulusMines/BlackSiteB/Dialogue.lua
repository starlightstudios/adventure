-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
    
    -- |[DroidA]|
    if(sActorName == "DroidA") then
	
        --Dialogue.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "JX-101", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] W-who's there?[P] Who are you?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Relax, we're friends.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] I -[P] I -[P] I can't see anything.[P] My optical receptors are offline.[B][C]") ]])
        fnCutscene([[ Append("JX-101:[E|Neutral] RI-15, is that you?[P] We thought you were retired![B][C]") ]])
        fnCutscene([[ Append("RI-15:[E|Neutral] Do I hear JX-101?[P] Where am I?[B][C]") ]])
        fnCutscene([[ Append("JX-101:[E|Neutral] It's me, and it's okay now.[P] We're going to get you out of here. Follow us.[B][C]") ]])
        fnCutscene([[ Append("RI-15:[E|Neutral] Thank gears you've come, JX.[P] My receptors...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] It's not safe here.[P] Take my hand and follow me, we'll get you out of here and get you fixed up.") ]])
        fnCutsceneBlocker()
        
        --Get Christine's position.
        EM_PushEntity("Christine")
            local iX, iY = TA_GetProperty("Position")
        DL_PopActiveObject()
        
        --Move the droid to her.
        fnCutsceneMove("DroidA", iX / 16.0, iY / 16.0)
        fnCutsceneBlocker()
        fnCutsceneTeleport("DroidA", -100.0, -100.0)
        fnCutsceneBlocker()
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iRescuedDroidA", "N", 1.0)
            
        --Check ending case.
        local iRescuedDroidA = VM_GetVar("Root/Variables/Chapter5/Scenes/iRescuedDroidA", "N")
        local iRescuedDroidB = VM_GetVar("Root/Variables/Chapter5/Scenes/iRescuedDroidB", "N")
        local iRescuedDroidC = VM_GetVar("Root/Variables/Chapter5/Scenes/iRescuedDroidC", "N")
        local iRescuedGolemD = VM_GetVar("Root/Variables/Chapter5/Scenes/iRescuedGolemD", "N")
        local iFoundSchematics = VM_GetVar("Root/Variables/Chapter5/Scenes/iFoundSchematics", "N")
        if(iRescuedDroidA == 1.0 and iRescuedDroidB == 1.0 and iRescuedDroidC == 1.0 and iRescuedGolemD == 1.0 and iFoundSchematics == 1.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iCompletedBlackSite", "N", 9.0)
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
            fnCutscene([[ Append("JX-101:[E|Neutral] All right Christine, we've done enough.[P] I believe we've swept the site thoroughly.[B][C]") ]])
            fnCutscene([[ Append("JX-101:[E|Neutral] Let's get back to the entrance and get out of here before Regulus City figures out what we've done.") ]])
            fnCutsceneBlocker()
        end
        
    -- |[DroidB]|
    elseif(sActorName == "DroidB") then
	
        --Dialogue.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "JX-101", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Ungh, a human?[P] And -[P] JX-101![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] We're here to rescue you![P] Follow us![B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Oh thank the gears.[P] But first, JX-101, please listen.[B][C]") ]])
        fnCutscene([[ Append("JX-101:[E|Neutral] What is it?[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] There's -[P] other prisoners.[P] In the back.[P] I think they were doing experiments on them.[P] You've got to save them![B][C]") ]])
        fnCutscene([[ Append("JX-101:[E|Neutral] How many?[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] I don't know, but I could hear them screaming...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Leave it to us.[P] Come on.") ]])
        fnCutsceneBlocker()
        
        --Get Christine's position.
        EM_PushEntity("Christine")
            local iX, iY = TA_GetProperty("Position")
        DL_PopActiveObject()
        
        --Move the droid to her.
        fnCutsceneMove("DroidB", iX / 16.0, iY / 16.0)
        fnCutsceneBlocker()
        fnCutsceneTeleport("DroidB", -100.0, -100.0)
        fnCutsceneBlocker()
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iRescuedDroidB", "N", 1.0)
            
        --Check ending case.
        local iRescuedDroidA = VM_GetVar("Root/Variables/Chapter5/Scenes/iRescuedDroidA", "N")
        local iRescuedDroidB = VM_GetVar("Root/Variables/Chapter5/Scenes/iRescuedDroidB", "N")
        local iRescuedDroidC = VM_GetVar("Root/Variables/Chapter5/Scenes/iRescuedDroidC", "N")
        local iRescuedGolemD = VM_GetVar("Root/Variables/Chapter5/Scenes/iRescuedGolemD", "N")
        local iFoundSchematics = VM_GetVar("Root/Variables/Chapter5/Scenes/iFoundSchematics", "N")
        if(iRescuedDroidA == 1.0 and iRescuedDroidB == 1.0 and iRescuedDroidC == 1.0 and iRescuedGolemD == 1.0 and iFoundSchematics == 1.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iCompletedBlackSite", "N", 9.0)
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
            fnCutscene([[ Append("JX-101:[E|Neutral] All right Christine, we've done enough.[P] I believe we've swept the site thoroughly.[B][C]") ]])
            fnCutscene([[ Append("JX-101:[E|Neutral] Let's get back to the entrance and get out of here before Regulus City figures out what we've done.") ]])
            fnCutsceneBlocker()
        end
    end
end
