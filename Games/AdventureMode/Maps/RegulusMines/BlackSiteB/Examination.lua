-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "NorthExit") then
	
	--Variables.
	local iBlackSiteKeycard = VM_GetVar("Root/Variables/Chapter5/Scenes/iBlackSiteKeycard", "N")
	
	--Can't access it yet.
	if(iBlackSiteKeycard == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Narrator: [SOUND|World|AutoDoorFail]Access denied.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] *55, can you hear me?[P] We're at a locked door.*[B][C]") ]])
		fnCutscene([[ Append("PDU:[VOICE|Tiffany] *I can't hack the door open from here, it's not on the network.[P] But, there is a keycode you can transmit to override it.*[B][C]") ]])
		fnCutscene([[ Append("PDU:[VOICE|Tiffany] *Check the terminals on the west side of the site.[P] They may be guarded.*[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] *Affirmative.*") ]])
		fnCutsceneBlocker()
	
	--Level transition.
	else
		AudioManager_PlaySound("World|AutoDoorOpen")
		AL_BeginTransitionTo("BlackSiteC", "FORCEPOS:12.5x24.0x0")
	end
	
elseif(sObjectName == "SouthExit") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("BlackSiteA", "FORCEPOS:20.0x4.0x0")
	
-- |[Examinables]|
elseif(sObjectName == "WreckedTerminal") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (A very old Steam Droid computer, not receiving power.[P] It's doubtful it'd work even if it was powered.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "OneWayMirror") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (A one way mirror.[P] Unlike a camera, such a mirror would not record the goings-on of the room...)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalA") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (A video displaying sheep, dogs, and raijus playing together.[P] Was this being used for recreation?)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalB") then
	
	--Variables.
	local iBlackSiteKeycard = VM_GetVar("Root/Variables/Chapter5/Scenes/iBlackSiteKeycard", "N")
	
	--First time.
	if(iBlackSiteKeycard == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iBlackSiteKeycard", "N", 1.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought:[VOICE|Christine] (Hmmm, yep...[P] There's an override code in the hard drive.[P] [SOUND|World|Keycard]I downloaded it to my PDU.)") ]])
		fnCutsceneBlocker()
	
	--Repeats.
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought:[VOICE|Christine] (There's nothing else on the terminal except shipping manifests...)") ]])
		fnCutsceneBlocker()
	end
	
elseif(sObjectName == "TerminalC") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (The terminal is used to monitor Steam Droid recharging.[P] The pressure rates are far too high...[P] the prisoner must have been in extreme pain...)") ]])
	fnCutsceneBlocker()
		
elseif(sObjectName == "TerminalD") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (The terminal is used to monitor Steam Droid recharging.[P] There's barely any pressure allocated, so the prisoner would constantly feel weak and disoriented...)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalE") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (Interrogation logs.[P] The Steam Droid prisoners gave all kinds of information, some false, some true.[P] The interrogator concluded that pain was producing unreliable testimony, but was ordered to keep inflicting it...)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "PartsShelf") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (Steam Droid replacement parts.[P] These are relatively new, and must have been fabricated recently.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "ElevatorShaft") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (An empty elevator shaft.[P] It goes down, not up.[P] I think I can see garbage at the bottom of it...[P] No...[P] Those are retired robots down there!)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "Vats") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (Storage vats.[P] Long disused, it probably doesn't contain anything.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "VatsLeaking") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (Storage vats.[P] It burst and spilled lubricant everywhere.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Oilmaker") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (An oilmaker.[P] Unflavoured, cold oil is sitting in the container.)") ]])
	fnCutsceneBlocker()

-- |[Other]|
-- |[Debug]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
