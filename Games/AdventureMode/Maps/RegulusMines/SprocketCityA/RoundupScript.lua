-- |[Roundup]|
--Called after each activity is completed, reports player status.

--Variables.
local iSprocketGardening = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketGardening", "N")
local iSprocketComputers = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketComputers", "N")
local iSprocketBoilers   = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketBoilers", "N")
local iSprocketLibrary   = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketLibrary", "N")
local iSprocketChess     = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketChess", "N")
local iSprocketPhotos    = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketPhotos", "N")
local iSprocketHats      = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketHats", "N")
local iSprocketDrawing   = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketDrawing", "N")

--Sum.
local iSum = iSprocketBoilers + iSprocketChess + iSprocketComputers + iSprocketDrawing + iSprocketGardening + iSprocketHats + iSprocketLibrary + iSprocketPhotos

--Total is 1: First activity.
if(iSum == 1.0) then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Christine:[VOICE|Christine] (All right, I think I'll need to distract JX-101 with two more activities...)") ]])
	fnCutsceneBlocker()

--Total is 2: Second activity.
elseif(iSum == 2.0) then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Christine:[VOICE|Christine] (Okay, I think I'll need to distract JX-101 with one more activity...)") ]])
	fnCutsceneBlocker()

--Total is 3: First part is complete.
elseif(iSum == 3.0) then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Christine:[VOICE|Christine] (I think I've bought 55 enough time.[P] I should go check the recharge pod in SX-399's room.)[B][C]") ]])
	fnCutscene([[ Append("Christine:[VOICE|Christine] (Though if I keep distracting JX-101, I'll buy more time for 55 and also gain 100 experience for each activity I do...)") ]])
	fnCutsceneBlocker()

--Bonus:
elseif(iSum < 8.0) then
	
	--Build string.
	local sString = ""
	if(iSum < 7.0) then
		sString = "WD_SetProperty(\"Append\", \"Christine:[VOICE|Christine] (There are " .. string.format("%i", 8.0 - iSum) .. " activities I could still do.)\")"
	else
		sString = "WD_SetProperty(\"Append\", \"Christine:[VOICE|Christine] (There is one activity I could still do.)\")"
	end

	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Christine:[VOICE|Christine] (All right, 100 experience!)[B][C]") ]])
	fnCutscene(sString)
	fnCutsceneBlocker()
    
    --Give Christine and 55 the EXP.
    AdvCombat_SetProperty("Push Party Member", "Christine")
        local iCurrentXP = AdvCombatEntity_GetProperty("Exp")
        AdvCombatEntity_SetProperty("Current Exp", iCurrentXP + 100)
    DL_PopActiveObject()
    AdvCombat_SetProperty("Push Party Member", "Tiffany")
        iCurrentXP = AdvCombatEntity_GetProperty("Exp")
        AdvCombatEntity_SetProperty("Current Exp", iCurrentXP + 100)
    DL_PopActiveObject()

--Last one.
else
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Christine:[VOICE|Christine] (I bought 55 a lot of time, but that's all I can do.)[B][C]") ]])
	fnCutscene([[ Append("Christine:[VOICE|Christine] (I should go check the recharge pod in SX-399's room to call it a night.)") ]])
	fnCutsceneBlocker()
    
    --Give Christine and 55 the EXP.
    AdvCombat_SetProperty("Push Party Member", "Christine")
        local iCurrentXP = AdvCombatEntity_GetProperty("Exp")
        AdvCombatEntity_SetProperty("Current Exp", iCurrentXP + 100)
    DL_PopActiveObject()
    AdvCombat_SetProperty("Push Party Member", "Tiffany")
        iCurrentXP = AdvCombatEntity_GetProperty("Exp")
        AdvCombatEntity_SetProperty("Current Exp", iCurrentXP + 100)
    DL_PopActiveObject()

end
