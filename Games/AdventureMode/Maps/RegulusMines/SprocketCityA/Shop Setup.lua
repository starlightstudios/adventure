-- |[ ================================= Steam Droid Shop Setup ================================= ]|
--Normal shop, no gemcutting.

-- |[Item Stock Listing]|
--Adamantite
AM_SetShopProperty("Add Item", "Adamantite Powder", -1, -1)

--Rep Items
AM_SetShopProperty("Add Item", "Recycleable Junk", -1, -1)

--Gems
AM_SetShopProperty("Add Item", "Blurleen Gem", -1, -1)
AM_SetShopProperty("Add Item", "Yemite Gem", -1, -1)
AM_SetShopProperty("Add Item", "Phassicsteel Gem", -1, -1)

--Equipment
AM_SetShopProperty("Add Item", "Sphalite Ring", -1, -1)
AM_SetShopProperty("Add Item", "Adaptive Cloth Vest", -1, -1)
AM_SetShopProperty("Add Item", "Cillium Cryospear", -1, -1)
AM_SetShopProperty("Add Item", "Mk IV Pulse Rifle", -1, -1)

--Items
AM_SetShopProperty("Add Item", "Nanite Injection", -1, -1)
AM_SetShopProperty("Add Item", "Regeneration Mist", -1, -1)
AM_SetShopProperty("Add Item", "Emergency Medkit", -1, -1)
