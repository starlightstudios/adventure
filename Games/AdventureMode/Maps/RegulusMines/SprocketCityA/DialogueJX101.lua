-- |[ ==================================== Dialogue JX-101 ===================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables.
	local iMetJX101 = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetJX101", "N")
    local iCompletedSprocketCity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSprocketCity", "N")
	
	--First meeting.
	if(iMetJX101 == 0.0) then
		TA_SetProperty("Face Character", "PlayerEntity")
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
		fnCutscene([[ Append("JX-101: Ah, the human![P] I see your legs have been repaired.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Yes, I feel much better now.[P] And you can call me Christine.[P] Certainly more polite than 'the human'![P] Hee hee![B][C]") ]])
		fnCutscene([[ Append("JX-101: Well, as you already seemed to know, my name is JX-101.[P] I'm the leader of the Fist of Tomorrow.[B][C]") ]])
		fnCutscene([[ Append("JX-101: What you see around you is Sprocket City, one of the largest Steam Droid settlements on Regulus.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Fist of Tomorrow?[B][C]") ]])
		fnCutscene([[ Append("JX-101: Do you like the name?[P] We used to be called Fist of the Future, but I think Tomorrow sounds better.[B][C]") ]])
		fnCutscene([[ Append("JX-101: We're pretty much the only group who are focused on the future of the Steam Droids, you see.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] I'm afraid I'm not terribly familiar with your situation.[B][C]") ]])
		fnCutscene([[ Append("JX-101: Hmm, just how did you get down here, anyway?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] As I said, I woke up in an airlock someplace.[P] After a lot of running and fighting with robots, I ended up in the mines.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] And everywhere I went, I was being followed by that doll-like girl.[P] I think her name was 55 or something?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] (Heh, just true enough not to get me in trouble!)[B][C]") ]])
		fnCutscene([[ Append("JX-101: Ah, you were probably a victim of the golem abduction teams.[P] They send squads to Pandemonium to abduct humans and convert them to golems.[B][C]") ]])
		fnCutscene([[ Append("JX-101: If you had not escaped when you did, you would likely be one of their robotic slaves.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Scared] Being a robot?[P] Sounds awful![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] No offense...[B][C]") ]])
		fnCutscene([[ Append("JX-101: I don't mind.[B][C]") ]])
		fnCutscene([[ Append("JX-101: I suppose I can trust you, though not with any sensitive secrets.[P] You could still be converted if you were captured again, and I'd rather you didn't know anything compromising.[B][C]") ]])
		fnCutscene([[ Append("JX-101: I'll be frank, we're in trouble.[P] Big trouble.[B][C]") ]])
		fnCutscene([[ Append("JX-101: Recently, strange creatures have been flooding into the mines.[P] We were already having difficulty locating enough spare parts and supplies to get by, but now it's extremely dangerous to leave the settlement.[B][C]") ]])
		fnCutscene([[ Append("JX-101: Fist of Tomorrow is fighting for the future, but we're struggling to survive the present.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] I see.[P] Is there anything I can do to help?[B][C]") ]])
		fnCutscene([[ Append("JX-101: That depends.[P] What sort of skills do you have?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] I'd say I'm pretty handy in a scrap.[P] I found this spear a while ago and I've gotten pretty good at using it.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] I'm also pretty good with machines.[P] I love to fix things and figure them out.[B][C]") ]])
		fnCutscene([[ Append("JX-101: Very valuable skillsets...[B][C]") ]])
		fnCutscene([[ Append("JX-101: Christine, if you have nowhere else to go, you may stay here.[P] We try to harbor any escaped humans we find, but resources are thin.[P] If you wish to stay, you must make yourself useful.[B][C]") ]])
		fnCutscene([[ Append("JX-101: Since you consider yourself a fighter, I will consider it a personal favour if you would venture into the mines and locate spare parts for us.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] All I have to do is not fall into any of your traps.[P] I was doing fine until then![B][C]") ]])
		fnCutscene([[ Append("JX-101: Yes, I apologize for that.[B][C]") ]])
		fnCutscene([[ Append("JX-101: Speaking of, that Command Unit that was chasing you is in the lockup at the north end of the city.[P] We're not quite sure what to do with her.[B][C]") ]])
		fnCutscene([[ Append("JX-101: Normally I'd have her scrapped, but if the golems find out we've captured her, they will retaliate...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] (Great, a political problem.[P] I better go talk with 55.)[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Thanks for your help, JX-101.[P] I'll see about making myself useful.[B][C]") ]])
		fnCutscene([[ Append("JX-101: Good luck, Christine.[P] And be careful, the mines are very dangerous.") ]])
		fnCutsceneBlocker()
		
		VM_SetVar("Root/Variables/Chapter5/Scenes/iMetJX101", "N", 1.0)
		
	--All subsequent dialogues.
	else
		
		--Get reputation.
		local iSteamDroidReputation = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "I")
		local sString = "WD_SetProperty(\"Append\", \"JX-101: Well met, Christine.[P] What would you like to discuss? (Reputation:: " .. iSteamDroidReputation .. ")[BLOCK]\")"
        if(iCompletedSprocketCity == 1.0) then
            sString = "WD_SetProperty(\"Append\", \"JX-101: Well met, Christine.[P] What would you like to discuss?[BLOCK]\")"
        end
		
		--Dialogue.
		TA_SetProperty("Face Character", "PlayerEntity")
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
		fnCutscene(sString)
		
		--Variables.
		local iAskedAboutHelpingDroids = VM_GetVar("Root/Variables/Chapter5/Scenes/iAskedAboutHelpingDroids", "N")
		
		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
		
		--Asking JX-101 about helping out:
		if(iAskedAboutHelpingDroids == 0.0) then
			fnCutscene(" WD_SetProperty(\"Add Decision\", \"How can I help?\", " .. sDecisionScript .. ", \"Help\") ")
		
		--Already asked, so put up the special options.
		else
			fnCutscene(" WD_SetProperty(\"Add Decision\", \"Turn In Items\", " .. sDecisionScript .. ", \"Items\") ")
			fnCutscene(" WD_SetProperty(\"Add Decision\", \"Special Assignments\", " .. sDecisionScript .. ", \"Special\") ")
			if(false) then fnCutscene(" WD_SetProperty(\"Add Decision\", \"(Debug) +50 Rep\", " .. sDecisionScript .. ", \"Debug\") ") end
            
            --Additional options for mines subquests.
            local iMineAReward = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineAReward", "N")
            local iMineBReward = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineBReward", "N")
            local iMineCReward = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineCReward", "N")
            local iMineDReward = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineDReward", "N")
            
            --Variables.
            local iMineASmelters     = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineASmelters", "N")
            local iMineBGotAmmo      = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineBGotAmmo", "N")
            local iMineCFoundMemento = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineCFoundMemento", "N")
            local iMineDArtSupplies  = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineDArtSupplies", "N")
            local iMineDTerminal     = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineDTerminal", "N")
            local iMineDCrate        = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineDCrate", "N")
            
            --Reward for Mines A:
            if(iMineASmelters == 1.0 and iMineAReward == 0.0) then
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Abrissite\", " .. sDecisionScript .. ", \"Abrissite\") ")
            end
            
            --Reward for Mines B:
            if(iMineBGotAmmo == 2.0 and iMineBReward == 0.0) then
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sleeping Furiously\", " .. sDecisionScript .. ", \"Sleeping\") ")
            end
            
            --Reward for Mines C:
            if(iMineCFoundMemento == 1.0 and iMineCReward == 0.0) then
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Unicorn Memento\", " .. sDecisionScript .. ", \"Memento\") ")
            end
            
            --Reward for Mines D:
            if(iMineDArtSupplies == 2.0 and iMineDTerminal == 2.0 and iMineDCrate == 2.0 and iMineDReward == 0.0) then
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"PP-81\", " .. sDecisionScript .. ", \"ArtAppreciation\") ")
            end
		end
		
		--Last option is always "Goodbye".
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Goodbye\",  " .. sDecisionScript .. ", \"Goodbye\") ")
		fnCutsceneBlocker()
	end

--Ask JX-101 about helping the Steam Droids.
elseif(sTopicName == "Help") then
	VM_SetVar("Root/Variables/Chapter5/Scenes/iAskedAboutHelpingDroids", "N", 1.0)
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] What can I do to help Sprocket City?[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] To be specific, we need materials and tools.[P] If you find any Bent Tools or Recycleable Junk we can make use of.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] I will give you 4 Reputation for each Junk and 6 for each Bent Tools.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] The citizens of Sprocket City could also likely use your assistance, as you claim to be mechanically minded.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] I've also got a number of patrols in the mines that aren't reporting in, or areas worth investigating.[P] Sending out search parties is beyond our current capability.[P] Any assistance rendered is appreciated.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] We also may have special tasks.[P] Ask me about them.[P] Secrecy is important, so do not discuss the matters openly.[P] Even with other members of Fist of Tomorrow.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Why wouldn't you tell them what you're doing?[P] Do you not trust them?[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] With my life.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] But, if captured, the Golems have ways...[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Sad] Say no more.[P] I understand.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] You will need to develop a reputation before I would consider giving a special assignment out, you understand.[P] Support the city and we will do what we can to support you.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Gotcha...") ]])

--Turning in reputation items.
elseif(sTopicName == "Items") then

	--Common.
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])

	--Get how many items we have.
	local iJunkCount  = AdInv_GetProperty("Item Count", "Recycleable Junk")
	local iToolsCount = AdInv_GetProperty("Item Count", "Bent Tools")
	local iReputationGained = (iJunkCount * 4) + (iToolsCount * 6)
			
	--Remove the parts from the inventory.
	for i = 1, iJunkCount, 1 do
		AdInv_SetProperty("Remove Item", "Recycleable Junk")
	end
	for i = 1, iToolsCount, 1 do
		AdInv_SetProperty("Remove Item", "Bent Tools")
	end
	
    --Variables.
    local iCompletedSprocketCity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSprocketCity", "N")
    if(iCompletedSprocketCity == 0.0) then
        
        --Add reputation.
        local iSteamDroidReputation = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N", iSteamDroidReputation + iReputationGained)
        
        --Nothing to turn in.
        if(iJunkCount < 1 and iToolsCount < 1) then
            fnCutscene([[ Append("Christine:[E|Neutral] I hate to be a bother, but what sorts of items were you in need of again?[B][C]") ]])
            fnCutscene([[ Append("JX-101:[E|Neutral] Specifically, Bent Tools (6 Rep) or Recycleable Junk (4 Rep).[P] I know it may not seem useful, but we are very resourceful.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Even *bent* tools?[B][C]") ]])
            fnCutscene([[ Append("JX-101:[E|Neutral] Life as a Steam Droid is three parts ingenuity and one part compromise...") ]])
        
        --Only junk.
        elseif(iJunkCount >= 1 and iToolsCount < 1) then
            fnCutscene([[ Append("Christine:[E|Smirk] I found some junk that looks like it might be useful.[B][C]") ]])
            fnCutscene([[ Append("JX-101:[E|Neutral] [SOUND|World|TakeItem]Superb![P] I'll get this to my quartermaster immediately.[B][C]") ]])
            local sString = "WD_SetProperty(\"Append\", \"Thought: (Gained " .. iReputationGained .. " reputation.)\")"
            fnCutscene(sString)
            
        --Only tools.
        elseif(iJunkCount < 1 and iToolsCount >= 1) then
            fnCutscene([[ Append("Christine:[E|Smirk] You might be able to fix these tools I found.[B][C]") ]])
            fnCutscene([[ Append("JX-101:[E|Neutral] [SOUND|World|TakeItem]We could get a few uses out of these, yes.[P] Thank you.[B][C]") ]])
            local sString = "WD_SetProperty(\"Append\", \"Thought: (Gained " .. iReputationGained .. " reputation.)\")"
            fnCutscene(sString)
        
        --Both.
        else
            fnCutscene([[ Append("Christine:[E|Smirk] Tools and some junk...[P] Is this helpful?[B][C]") ]])
            fnCutscene([[ Append("JX-101:[E|Neutral] [SOUND|World|TakeItem]Excellent, this should alleviate our supply crisis...[P] if only for a few moments...[B][C]") ]])
            local sString = "WD_SetProperty(\"Append\", \"Thought: (Gained " .. iReputationGained .. " reputation.)\")"
            fnCutscene(sString)
            
        end
        
    --After completing the quest chain...
    else
    
        --Add work credits.
        iReputationGained = math.floor(iReputationGained / 2)
        local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
        VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + iReputationGained)
        
        --Nothing to turn in.
        if(iJunkCount < 1 and iToolsCount < 1) then
            fnCutscene([[ Append("Christine:[E|Neutral] I hate to be a bother, but what sorts of items were you in need of again?[B][C]") ]])
            fnCutscene([[ Append("JX-101:[E|Neutral] Specifically, Bent Tools (2 credits) or Recycleable Junk (2 credits).[P] I know it may not seem useful, but we are very resourceful.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Even *bent* tools?[B][C]") ]])
            fnCutscene([[ Append("JX-101:[E|Neutral] Life as a Steam Droid is three parts ingenuity and one part compromise...") ]])
        
        --Only junk.
        elseif(iJunkCount >= 1 and iToolsCount < 1) then
            fnCutscene([[ Append("Christine:[E|Smirk] I found some junk that looks like it might be useful.[B][C]") ]])
            fnCutscene([[ Append("JX-101:[E|Neutral] [SOUND|World|TakeItem]Superb![P] I'll get this to my quartermaster immediately.[B][C]") ]])
            local sString = "WD_SetProperty(\"Append\", \"Thought: (Gained " .. iReputationGained .. " credits.)\")"
            fnCutscene(sString)
            
        --Only tools.
        elseif(iJunkCount < 1 and iToolsCount >= 1) then
            fnCutscene([[ Append("Christine:[E|Smirk] You might be able to fix these tools I found.[B][C]") ]])
            fnCutscene([[ Append("JX-101:[E|Neutral] [SOUND|World|TakeItem]We could get a few uses out of these, yes.[P] Thank you.[B][C]") ]])
            local sString = "WD_SetProperty(\"Append\", \"Thought: (Gained " .. iReputationGained .. " credits.)\")"
            fnCutscene(sString)
        
        --Both.
        else
            fnCutscene([[ Append("Christine:[E|Smirk] Tools and some junk...[P] Is this helpful?[B][C]") ]])
            fnCutscene([[ Append("JX-101:[E|Neutral] [SOUND|World|TakeItem]War funding.[P] We'll make use of these.[B][C]") ]])
            local sString = "WD_SetProperty(\"Append\", \"Thought: (Gained " .. iReputationGained .. " credits.)\")"
            fnCutscene(sString)
            
        end

    end

--Special assignments.
elseif(sTopicName == "Special") then

	--Variables.
	local iSteamDroidReputation  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N")
	local iSteamDroid100RepState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid100RepState", "N")
	local iSteamDroid250RepState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid250RepState", "N")
	local iSteamDroid500RepState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid500RepState", "N")

	--Common.
	if(iSteamDroid250RepState ~= 2.0) then
		WD_SetProperty("Hide")
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	end

	-- |[X20 Transducer Quest]|
	--First quest: Assignment.
	if(iSteamDroid100RepState == 0.0) then

		--Not enough reputation:
		if(iSteamDroidReputation < 100) then
			fnCutscene([[ Append("Christine:[E|Neutral] Is there anything special you needed me to do?[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] At the moment, no.[P] I'll let you know if anything comes up.[B][C]") ]])
			fnCutscene([[ Append("Thought: (Next quest requires 100 Reputation.)") ]])
			
		--Give the quest:
		else
			VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroid100RepState", "N", 1.0)
			fnCutscene([[ Append("Christine:[E|Neutral] You mentioned special assignments.[P] Was there anything you needed?[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] Considering your capabilities, yes.[P] I think there is.[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] Here, take this.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] [SOUND|World|TakeItem]Er, if I don't miss my guess, that is a Series-X20 Transducer, right?[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] Your knowledge of electronics is uncanny.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] It's a talent of mine.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] (Having thousands of blueprints and manuals downloaded into your mechanized brain doesn't hurt either...)[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] We scavenged this device from the mines.[P] We've got only a few, and they're vital for monitoring steam pressure remotely.[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] In locations where we can't look at the pipes directly, we use these to identify potential leaks.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] This one's broken...[P] but I can't say how.[P] I'd need a micro-wave scanner.[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] Do you think you can fix it?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Let me hold on to it, and I'll see what I can do.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] (I could take a look at it using the scanners in the repair bay.[P] Sophie can help with that.)[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] I must admit it is most likely that you are the most capable repair unit here.[P] If you can fix it, Sprocket City will be in your debt.") ]])
		
		end

	--Received the quest, haven't completed it yet.
	elseif(iSteamDroid100RepState == 1.0) then
		fnCutscene([[ Append("JX-101:[E|Neutral] How goes the X20 Transducer, Christine?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Still working on it, I'll get back to you when I've figured something out.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral](I bet Sophie will know what to do.)") ]])

	--Quest completed.
	elseif(iSteamDroid100RepState == 2.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroid100RepState", "N", 3.0)
		fnCutscene([[ Append("JX-101:[E|Neutral] You've got a smile on your face.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Happy] One Series-X20 Transducer, fixed and ready to go![B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] Capital![P] How did you get it fixed?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] (I hope I'm as good at lying as I am at fixing things...)[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] I snuck into the workshop at the top of the mines and used their scanner.[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] You what!?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] It was risky, but it was the only way.[P] I put everything back, the Golems won't suspect a thing.[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] Let us hope not.[P] The Golems will take any excuse to exterminate us, I assure you.[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] Still, this transducer will be of great assistance.[P] Thank you, Christine.[B][C]") ]])
		fnCutscene([[ Append("Thought: (Gained 50 Reputation!)") ]])
		
		--Add reputation.
		local iSteamDroidReputation = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N")
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N", iSteamDroidReputation + 50)

	-- |[Second Quest: Meet SX-399]|
	--Quest assignment.
	elseif(iSteamDroid250RepState == 0.0) then

		--Not enough reputation:
		if(iSteamDroidReputation < 250) then
			fnCutscene([[ Append("Christine:[E|Neutral] Is there anything special you needed me to do?[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] At the moment, no.[P] But, I've got my eye out.[P] Return later.[B][C]") ]])
			fnCutscene([[ Append("Thought: (Next quest requires 250 Reputation.)") ]])
			
		--Give the quest:
		else
			VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroid250RepState", "N", 1.0)
			fnCutscene([[ Append("Christine:[E|Neutral] Have any other special assignments come up?[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] *Sigh* No, but...[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Sad] Nobody starts with a sigh unless it's serious.[P] Do you want to talk about it?[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] I'm sorry, Christine.[P] It's a personal matter.[P] I don't want to involve you.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Let me guess...[P] It's about your daughter?[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] That, my friend, is too accurate to be a guess.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Before all this, I was an English teacher.[P] I've dealt with many a teenager and frustrated parent.[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] Is it so universal?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Everything changes but the sigh.[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] ...[P] We had an argument recently, I'm afraid.[P] She hasn't come out of her room since.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Naturally, you want me to go talk to her.[P] Understood.[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] This isn't an official assignment.[P] Don't be so formal.[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] Honestly, I'd rather be shooting at rogue servitors right about now...[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] I'll take care of it.[P] Don't worry.[P] And if you're like all those other parents, you're going to worry nonetheless.[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] Your predictive abilities are uncanny...") ]])
		
		end

	--Repeats.
	elseif(iSteamDroid250RepState == 1.0) then
		fnCutscene([[ Append("JX-101:[E|Neutral] I love my daughter, Christine.[P] She is my whole world.[P] Is it bad that we don't get along?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] No, it's part of growing up.[P] I'll talk to her and see what I can do.") ]])

	--Repeats.
	elseif(iSteamDroid250RepState == 2.0) then
	
		--Flags.
		WD_SetProperty("Hide")
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroid250RepState", "N", 3.0)
		
		--Movement.
		fnCutsceneMove("Christine", 53.25, 23.50)
		fnCutsceneFace("Christine", -1, 0)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Mother, I did not mean to shout.[P] But - [B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] We're all friends here, dear.[P] Go on.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] I am trying my best, but I do not want Sprocket City to be my cage.[P] I have a right to contribute to society.[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] I suppose you do, don't you?[P] But if anything happened to you...[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] We've had this conversation before.[P] I already know how it ends.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Is there anything SX-399 can do around town to help?[P] Anything at all?[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] ...[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Please?[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] Perhaps you could monitor and reset the traps around the entrances...[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Really?[P] Trap duty?[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] But you have to be back before 19::00 and I'll have the sentries checking up on you and - [P][CLEAR]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] I won't let you down, mother![B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] *kiss*") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--SX-399 Moves away.
		fnCutsceneMove("SX-399", 53.25, 22.50)
		fnCutsceneFace("Christine", 0, -1)
		fnCutsceneFace("JX-101", 0, -1)
		fnCutsceneBlocker()
		fnCutsceneMove("SX-399", 56.25, 22.50)
		fnCutsceneFace("Christine", 1, -1)
		fnCutsceneFace("JX-101", 1, 0)
		fnCutsceneBlocker()
		fnCutsceneMove("SX-399", 56.25, 16.50)
		fnCutsceneMove("SX-399", 53.25, 16.50)
		fnCutsceneFace("SX-399", 0, 1)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Open Door", "FistOfTomorrowHQDoor") ]])
		fnCutsceneBlocker()
		fnCutsceneMove("SX-399", 53.25, 18.50)
		fnCutsceneMove("SX-399", 37.25, 18.50)
		fnCutsceneTeleport("SX-399", -100, -100)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] I admit it has been some time since she kissed me on the cheek like that.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] She's ready to be her own person.[P] She needs the space to make that a reality.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Her life is moving into its next stage.[P] It won't be the same as before, but different isn't the same as bad.[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] ...[P] Has she told you of her special situation?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] In passing, but not detail.[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] I see...[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] Please do not trouble yourself on this matter.[P] I'm happy enough that she's talking to me![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Happy to help![B][C]") ]])
		fnCutscene([[ Append("Thought: (Gained 50 Reputation!)") ]])
		fnCutsceneBlocker()
		
		--Add reputation.
		local iSteamDroidReputation = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N")
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N", iSteamDroidReputation + 50)
	
	-- |[Black Site Quest]|
	--Quest assignment.
	elseif(iSteamDroid500RepState == 0.0) then

        --Variables:
		local iSXMet55 = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXMet55", "N")
		local iSpokeWithTT233 = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeWithTT233", "N")

		--Hasn't done the planning phase yet.
		local iPlanHatched = VM_GetVar("Root/Variables/Chapter5/Scenes/iPlanHatched", "N")
		if(iPlanHatched == 0.0) then
			fnCutscene([[ Append("Christine:[E|Neutral] JX-101, have you any other special tasks for me?[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] Afraid not.[P] You've done well so far.[P] Keep it up.[B][C]") ]])
            if(iSXMet55 == 0.0) then
                fnCutscene([[ Append("Thought: (I need to coordinate with 55.[P] I should probably introduce her to SX-399, and get her help.)") ]])
            elseif(iSXMet55 == 1.0 and iSpokeWithTT233 == 0.0) then
                fnCutscene([[ Append("Thought: (I should speak with TT-233 about SX-399's condition...)") ]])
            else
                fnCutscene([[ Append("Thought: (I should go talk with SX-399 and 55, and let them know what I found out.)") ]])
            end

		--Not enough reputation:
		elseif(iSteamDroidReputation < 500) then
			fnCutscene([[ Append("Christine:[E|Neutral] JX-101, have you any other special tasks for me?[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] Afraid not.[P] You've done well so far.[P] Keep it up.[B][C]") ]])
			fnCutscene([[ Append("Thought: (Next quest requires 500 Reputation.)") ]])
			
		--Give the quest:
		else
			VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroid500RepState", "N", 1.0)
			fnCutscene([[ Append("Christine:[E|Neutral] Have any other special assignments come up?[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] Hmmmm...[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] Yes, yes, I suppose I must call upon you again...[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Excuse me?[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] Now, Christine, I know you're asking to help.[P] I know you've given Sprocket City so much...[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] And I want to emphasize that you can say no.[P] What I am about to ask you is extremely dangerous.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] (I guess 55 got the information to her...)[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Whatever it is, I'm ready.[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] The administration of Regulus City routinely captures and imprisons those who would oppose it.[P] This includes us, escaped humans, or maverick golems.[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] Because they must maintain the facade of an idyllic robotic paradise, such...[P] undesirables...[P] are imprisoned in locations kept secret even from their own population.[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] One such location has recently fallen into our hands.[P] We found a badly beaten Lord Unit who must have gotten lost in the mines.[P] Her PDU pointed us straight to it.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Offended] (Oh, very subtle, 55...)[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] What of the Lord Unit?[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] Why do you concern yourself so with the lives of our enemies?[P] This Lord Unit, had she the chance, would kidnap you and transform you into her personal bootlicker.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Offended] Enemy or not, every death is a tragedy.[P] Please don't retire her.[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] We will not stoop to the level of the golems.[P] We do not retire our prisoners unless they pose an immediate danger, as the Command Unit you encountered evidently did.[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] Now, as to the detention site, we do not believe the golems know we know of it.[P] With the advantage of surprise, I believe we can liberate the prisoners within.[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] The site is located at a disused tram repair station a few kilometers from here.[P] I will update your PDU with a map.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] (...[P] Looks like this is under Sector 12.[P] I hope 55 found an old workshop.)[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] When do we leave?[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] We will not be moving together, we will split into small groups and meet up near the site.[P] This will reduce the likelihood of the golems sighting our raiding party and mobilizing a counterattack.[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] You should be able to make your way along the rail track just outside Sprocket City.[P] Will you be all right getting there under your own power?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Sure thing. I'll meet you there.[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] Christine...[P] Thank you for your sacrifice.[P] Few people, and even fewer humans, would be willing to risk themselves for the sake of another as you do.[P] Good hunting.") ]])
		
		end
	
	--Repeats.
	elseif(iSteamDroid500RepState == 1.0) then
		fnCutscene([[ Append("JX-101:[E|Neutral] I have sent the signal to marshall our forces.[P] I will be heading out momentarily myself.") ]])
	
	-- |[No More Quests]|
	else
		fnCutscene([[ Append("Christine:[E|Neutral] Is there anything special you needed me to do?[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] Christine, I realize that you enjoy helping, but at this point I think Sprocket City can go no further in debt to you.[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] From the bottom of my power core, thank you.[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] Though if you do find any additional equipment in the mines, we will still take it.[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] In fact, I think I can scrounge up work credits for you as a reward.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Work credits?[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] My patrols periodically find credits chips that have been discarded.[P] We can't make much use of them, but perhaps you can.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Great![P] We could use the credits to purchase equipment.[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] Exactly what I was thinking.[P] Use Regulus City's resources against them.[P] Good hunting, Christine.") ]])
	end

--Debug, adds +50 Rep.
elseif(sTopicName == "Debug") then
	WD_SetProperty("Hide")
	local iSteamDroidReputation = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N")
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N", iSteamDroidReputation + 50)

-- |[Mine Rewards Topics]|
elseif(sTopicName == "Abrissite") then
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMineAReward", "N", 1.0)
	WD_SetProperty("Hide")
    fnStandardMajorDialogue(true)
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Have you ever heard of abrissite, JX-101?[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Rare mineral of some sort.[P] The golems used to mine it back when this was an active dig site.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] We don't have a lot of geologists in the Steam Droid population.[P] Most of them were upgraded ages ago.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] I found a sample of the stuff.[P] It's this pink ore here.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] Apparently it can be smelted into a few useful metals.[P] My PDU has more.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] This pink rock is abrissite![P] Curses![B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] I never knew we could make use of it, I figured it was slag.[P] There's a dozen sites we could have been mining ourselves this whole time![B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] But didn't Regulus City mine the good ore veins?[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Yes and no.[P] You sometimes see nodules of it exposed due to cavern collapses.[P] Plus we could use the lower-grade ores that the golems passed over. We're always in need of metals.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] And of course we've been sitting on it the whole time.[P] Knowledge is just as valuable as the ore itself.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Thank you greatly, Christine.[P] This will help immensely.[P] Sprocket City is in your debt.[B][C]") ]])
    
    --Rewards.
    local iCompletedSprocketCity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSprocketCity", "N")
    if(iCompletedSprocketCity == 0.0) then
        local iSteamDroidReputation = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N", iSteamDroidReputation + 75)
        fnCutscene([[ Append("Narrator: (+75 Reputation!)") ]])
    else
        local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
        VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + 25)
        fnCutscene([[ Append("Narrator: (+25 Work Credits!)") ]])
    end
    
elseif(sTopicName == "Sleeping") then
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMineBReward", "N", 2.0)
	WD_SetProperty("Hide")
    fnStandardMajorDialogue(true)
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] What if I said 'Sleeping Furiously' to you, JX-101?[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] I'd say you were either a great linguist, or someone to whom we owe a debt of gratitude.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] It's a phrase we use to indicate someone helped us in the mines, but it also means we're not going to say who or how.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] I'm not going to ask who you helped, how you helped them, or anything like that.[P] But you helped us, so Fist of Tomorrow will help you.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] Great![P] Thanks, JX-101![B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] And since you've heard it, you've earned the right to use the phrase as well.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Oh, and another phrase.[P] 'Colorless Green Dreams'.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] Does it mean the same thing?[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] No.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] If someone has betrayed you and you want us to deal with them, give them that phrase.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Oh...[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] I've never had to deal with someone like that myself, but suffice is to say it won't be a pleasant experience.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Now here's a reward for you.[P] Thanks again.[B][C]") ]])
    
    --Rewards.
    local iCompletedSprocketCity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSprocketCity", "N")
    if(iCompletedSprocketCity == 0.0) then
        local iSteamDroidReputation = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N", iSteamDroidReputation + 125)
        fnCutscene([[ Append("Narrator: (+125 Reputation!)") ]])
    else
        local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
        VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + 60)
        fnCutscene([[ Append("Narrator: (+60 Work Credits!)") ]])
    end
    
elseif(sTopicName == "Memento") then
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMineCReward", "N", 3.0)
	WD_SetProperty("Hide")
    fnStandardMajorDialogue(true)
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] I found this while in the mines.[P] Does it look like anything you recognize?[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Where did you find it?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] A Steam Droid settlement.[P] Pretty small.[P] It had been overrun, but I think everyone got out safely.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Might have been Trilby Junction.[P] In which case, I think this belongs to one of the Steam Droids who passed through.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] [SOUND|World|TakeItem]I would guess it has some sentimental value.[P] In any case, I'll pass it along to the next team heading out past Cider Gorge.[P] That's where they wound up settling.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] Just doing my part.[B][C]") ]])
    
    --Rewards.
    local iCompletedSprocketCity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSprocketCity", "N")
    if(iCompletedSprocketCity == 0.0) then
        local iSteamDroidReputation = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N", iSteamDroidReputation + 75)
        fnCutscene([[ Append("Narrator: (+75 Reputation!)") ]])
    else
        local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
        VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + 25)
        fnCutscene([[ Append("Narrator: (+25 Work Credits!)") ]])
    end
    
elseif(sTopicName == "ArtAppreciation") then
	WD_SetProperty("Hide")
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMineDReward", "N", 4.0)
    fnStandardMajorDialogue(true)
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Do you know an artist named PP-81?[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] I've heard the name.[P] And yes, I heard you helped her out.[P] She was telling anyone who'd listen, apparently.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] As long as she made it back safely.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] I'd heard she was a recluse, which is not a safe thing to be in the mines.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] But you tolerate artists?[P] Especially when supplies are so hard to get?[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] That is, unfortunately, a common attitude.[P] But, we do tolerate and even encourage artists.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Strange as it may seem, we have to set aside something for them even if times are dire.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Art is the only reason we keep going.[P] If it was just endless struggle and violence, I'm not sure any of us would bother.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] Yeah, I guess you're right.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Just try to keep her away from all the creatures that are going to eat her.[P] She seemed a little loopy.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] An artist without a few loose bolts is no artist at all...[B][C]") ]])
    
    --Rewards.
    local iCompletedSprocketCity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSprocketCity", "N")
    if(iCompletedSprocketCity == 0.0) then
        local iSteamDroidReputation = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N", iSteamDroidReputation + 125)
        fnCutscene([[ Append("Narrator: (+125 Reputation!)") ]])
    else
        local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
        VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + 60)
        fnCutscene([[ Append("Narrator: (+60 Work Credits!)") ]])
    end

-- |[Goodbye]|
elseif(sTopicName == "Goodbye") then
	WD_SetProperty("Hide")
    fnStandardMajorDialogue(true)
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutscene([[ Append("JX-101: Good hunting, my friend.") ]])
end
