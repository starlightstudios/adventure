-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToMinesE") then
	
	--Variables.
	local iSprung55       = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprung55", "N")
	local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
	if(iSprung55 == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] (I should probably go get 55 out of jail before I leave...)") ]])
		fnCutsceneBlocker()
		return
	elseif(iSXUpgradeQuest == 2.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] (I can't leave with JX-101 following me like this, I'll need to distract her...)") ]])
		fnCutsceneBlocker()
		return
	end
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("TelluriumMinesE", "FORCEPOS:31.0x5.0x0")

-- |[Examinables]|
elseif(sObjectName == "PaintingA") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (A painting of a city.[P] It's titled 'Swan Song of the People'.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "PaintingB") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (A painting of a landscape made out of brass.[P] It's titled 'Faraway Promise'.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "PaintingC") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (A painting of a shoreline.[P] It's titled 'Unpunished'.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "PaintingD") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (A painting of a shadow looking into the distance.[P] It's titled 'Isolation'.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "PaintingE") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (A painting of a landscape made out of brass.[P] It's titled 'Faraway Promise'.[P] A tag on the name plate indicates this is the original and many copies have been made.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Boxes") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (Boxes full of various parts.[P] Gears, tools, pipes...)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "BookA") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (A book of notes about various locations in the mines.[P] I suppose Steam Droids don't have portable computers, and have to record everything with pen and paper.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "BookB") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (A fiction book about a girl who uses a space ship to fly to various planets and have sex with the locals.[P] It's titled 'Star Sex in the Year 7000'.[P] It's the opposite of subtle.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "PartsShelfA") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (Parts in bad condition.[P] It's hard to believe anyone would be desperate enough to install these...)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "PartsShelfB") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (There's some labels on the shelf.[P] It used to be full of parts, but now there's only a few...)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "PartsShelfC") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (Scanners and hand tools, used for diagnostics on power cores.[P] There's even an old blueprint of a Steam Droid core here, but it's so faded as to be illegible.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "LeakingVat") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (This vat is clearly broken.[P] I hope they didn't spill too much lubricant before they drained it.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Crate") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (A fairly good crate.[P] Not the greatest crate, but one my great uncle Nate would be proud of.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "BarrelA") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (It's full of water.[P] Presumably it will be used for steam at some point.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "ComputerA") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (There's a play-by-mail version of a game that looks like Chess here.[P] Whoever owns this terminal is not winning.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "ComputerB") then

	--Variables.
	local iSXUpgradeQuest    = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
	local iSprocketComputers = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketComputers", "N")
	local iSprocketPhotos    = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketPhotos", "N")
	
	--No upgrade quest, or already done.
	if(iSXUpgradeQuest ~= 2.0) then
		
		--Broken.
		if(iSprocketComputers == 0.0) then
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ Append("Thought:[VOICE|Christine] (This computer is broken.)") ]])
			fnCutsceneBlocker()
		
		--Fixed.
		else
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ Append("Thought:[VOICE|Christine] (This computer is showing cute photos of Darkmatters playing with cats.[P] Too adorable!)") ]])
			fnCutsceneBlocker()
		end
	
	--Let's renormalize some pressures!
	else
	
		if(iSprocketComputers == 0.0) then
			LM_ExecuteScript(fnResolvePath() .. "SpecialScripts.lua", "Computers")
		elseif(iSprocketPhotos == 0.0) then
			LM_ExecuteScript(fnResolvePath() .. "SpecialScripts.lua", "Photos")
		else
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ Append("Thought:[VOICE|Christine] (This computer is showing cute photos of Darkmatters playing with cats.[P] Too adorable!)") ]])
			fnCutsceneBlocker()
		end
	end
		
	
elseif(sObjectName == "ComputerC") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (This computer is hacked into the Regulus City network, and highlights security threats in the mines.[P] There's...[P] a lot of them...)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "ComputerD") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] ('Fist of Tomorrow Newsletter'.[P] Reports on recent skirmishes and encouraging words.[P] There's a section dedicated to signing up new Steam Droids at the top.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "ComputerE") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (There's a play-by-mail game that looks like Chess active on this terminal.[P] This player is winning, big time.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "ComputerF") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (Oh, this terminal has all sorts of photos of Darkmatters playing and doing cute things![P] Adorable!)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "BookshelfA" or sObjectName == "BookshelfB" or sObjectName == "BookshelfC" or sObjectName == "BookshelfD") then

	--Variables.
	local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
	local iSprocketLibrary  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketLibrary", "N")
	
	--No upgrade quest, or already done.
	if(iSXUpgradeQuest ~= 2.0 or iSprocketLibrary == 1.0) then
	
		if(sObjectName == "BookshelfA") then
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ Append("Thought:[VOICE|Christine] ('Adventures of Bidoof the Camel'.[P] In this novel, Bidoof journeys across a desert and spits on people.[P] Riveting!)") ]])
			fnCutsceneBlocker()
			
		elseif(sObjectName == "BookshelfB") then
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ Append("Thought:[VOICE|Christine] ('Making Bombs for Beginners'.[P] Hey, if you're new to blowing things up, you gotta start somewhere.)") ]])
			fnCutsceneBlocker()
			
		elseif(sObjectName == "BookshelfC") then
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ Append("Thought:[VOICE|Christine] ('Structural Geology:: Principles and Practices'.[P] Reminds me of the time I walked in on one of the Geology professors berating at a student, because I didn't understand half of what he said.)") ]])
			fnCutsceneBlocker()
			
		elseif(sObjectName == "BookshelfD") then
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ Append("Thought:[VOICE|Christine] ('Sex as a Tool'...[P] Oh, it's literally about someone transforming into a voltometer and having sex with a Steam Droid.[P] Okay...)") ]])
			fnCutsceneBlocker()
		end
		
	--Let's sort some books!
	else
		LM_ExecuteScript(fnResolvePath() .. "SpecialScripts.lua", "Library")
	end
	
elseif(sObjectName == "BookshelfE") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (Books on strategy, tactics, and organization.[P] Not the sort of thing SX-399 seems she'd read...)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Chair") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (This chair is well worn.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "ArtStudioSign") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] (Public art studio.[P] All are welcome.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "LibrarySign") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] (Public library.[P] Please return books after four days, thank you!)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "FistOfTheFutureSign") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] (Fist of the Future Headquarters.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Schematics") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] (Gear and pipe network blueprints.)") ]])
	fnCutsceneBlocker()

-- |[Special]|
elseif(sObjectName == "Boilers") then

	--Variables.
	local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
	local iSprocketBoilers = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketBoilers", "N")
	
	--No upgrade quest, or already done.
	if(iSXUpgradeQuest ~= 2.0 or iSprocketBoilers == 1.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought:[VOICE|Leader] (Storage vats and boilers.[P] Some contain water for vaporization, some contain lubricant.[P] Better not mix those up!)") ]])
		fnCutsceneBlocker()
	
	--Let's renormalize some pressures!
	else
		LM_ExecuteScript(fnResolvePath() .. "SpecialScripts.lua", "Boilers")
	end

-- |[Special]|
--Chessboard. Can be played during the SX-399 quest.
elseif(sObjectName == "Chessboard") then

	--Variables.
	local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
	local iSprocketChess  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketChess", "N")
	
	--No upgrade quest, or already done.
	if(iSXUpgradeQuest ~= 2.0 or iSprocketChess == 1.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought:[VOICE|Christine] (A chessboard, though on Pandemonium it's called Penkak.[P] It's almost identical to Chess except Rooks and Bishops can only move five spaces maximum.)") ]])
		fnCutsceneBlocker()
	
	--Let's play some Penkak!
	else
		LM_ExecuteScript(fnResolvePath() .. "SpecialScripts.lua", "Penkak")
	end
	
-- |[Special]|
--Easel. Drawing side-activity during the SX-399 quest.
elseif(sObjectName == "Easel") then

	--Variables.
	local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
	local iSprocketDrawing  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketDrawing", "N")
	
	--No upgrade quest, or already done.
	if(iSXUpgradeQuest ~= 2.0 or iSprocketDrawing == 1.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought:[VOICE|Christine] (A painting easel, used by the artists her to draw or paint.)") ]])
		fnCutsceneBlocker()
	
	--Let's play some Penkak!
	else
		LM_ExecuteScript(fnResolvePath() .. "SpecialScripts.lua", "Drawing")
	end

-- |[Special]|
elseif(sObjectName == "DoorSX399") then

	--Variables.
	local iSteamDroid250RepState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid250RepState", "N")
	
	--No quest yet:
	if(iSteamDroid250RepState < 1.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought:[VOICE|Christine] (It's locked.[P] I can hear someone pouting on the other side.)") ]])
		fnCutsceneBlocker()
	
	--Quest taken, execute:
	elseif(iSteamDroid250RepState == 1.0) then
	
		--State.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroid250RepState", "N", 2.0)
        
        --Achievement.
        AM_SetPropertyJournal("Unlock Achievement", "MeetSX")
	
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought:[VOICE|Christine] (This must be SX-399's room...)") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Sound.
		fnCutscene([[ AudioManager_PlaySound("World|Knock") ]])
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Turn and move.
		fnCutsceneFace("SX-399", 0, 1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] I am not speaking to you, mother![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Excuse me?[P] Are you SX-399?[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] You're not -[P] oh my!") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Movement.
		fnCutsceneMove("SX-399", 70.25, 13.50, 2.00)
		fnCutsceneBlocker()
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Open Door", "DoorSX399") ]])
		fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] My goodness, are you a human?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Happy] Why yes I am![B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Keen![P] I can't remember the last time I saw a real human![B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Please, come in!") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Movement.
		fnCutsceneMove("SX-399", 70.25, 12.50)
		fnCutsceneMove("SX-399", 69.25, 12.50)
		fnCutsceneFace("SX-399", 1, 0)
		fnCutsceneMove("Christine", 70.25, 12.50)
		fnCutsceneFace("Christine", -1, 0)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] So, you're SX-399?[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] I am![P] Wow![P] Are you from Regulus City?[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] No, wait.[P] Are you from Pandemonium?[P] Maybe?[P] How did you get here?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] I'm from England, actually.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] I have no idea where that is, but I want to hear all about it![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Oh, it's not that interesting.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Is it more interesting than being cooped up in this room?[P] Because if it is, no detail is unimportant.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Oh, where did you get that skirt?[P] I love the stripe![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] This?[P] Well...[P] I literally manifested it with sheer willpower.[P] I think.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Are you a mage?[P] Tubular![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Listen, SX-399...[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] And what sort of accent is that?[P] Does everyone in Angland talk like that?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] You might be surprised how many accents we have.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] But, SX-399, your mother sent me to speak to you.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Tch.[P] That figures.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] I don't need someone to check up on me.[P] You can go right back to her and tell her - [P][CLEAR]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Tell that stuffy old lady what, exactly?[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] ...?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Pfft.[P] I'm not that old, dear.[P] Your mom?[P] An antique.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Oh, don't let her hear you say that.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Laugh] What's she going to do?[P] Blow steam at me?[P] I'm sooo scared.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Aww, you're just trying to get in my good graces, aren't you?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] So what if I am?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Now I'm no detective, but I look around Sprocket City and I don't see a lot of Steam Droids your age.[P] Do you have any friends?[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] ...[P] You don't know much about Steam Droids, do you?[P] Then again, I suppose you're a human.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] What do you mean?[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] We all became Steam Droids decades ago.[P] In fact, I'm probably older than you are, by a lot.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] You don't look it.[P] You look half my age, dear.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Flattery will get you everywhere![B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Oh, I just realized I didn't get your name.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Christine.[P] Pleased to meet you.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] So, Christine, maybe you didn't know, but -[P] I have a condition.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] I won't bore you with the details, but no, I don't have a lot of friends.[P] I can't.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Why not?[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] My power core doesn't work right, and I have to spend most of my time recharging it.[P] I sleep for all but seven days each year.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] I tried to get to know some Steam Droids, but then I go to sleep again.[P] When I wake up, they're like different people.[P] Or, they've moved to another settlement.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] The only ones who are always here are...[P] are...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Who?[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Just mother...[P] I suppose all the others are gone now.[P] It's been ten years, hasn't it?[P] But it feels like yesterday...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] I'm sorry...[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Because of my condition, mother detests me leaving.[P] She wants to spend all her time with me.[P] I have to sneak out when I want to do something fun.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Well, from her perspective, it's a special occasion.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] True, but I have other things I want to do.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Sprocket City gets more dilapidated every time I wake.[P] Yet, all the other Steam Droids look at me like I'm a burden.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] That's not how they see you.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] For a newcomer, you're frighteningly certain of that.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] Then again, why am I bothering with you?[P] When I go to sleep, you'll be gone when I wake up.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] ...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] So, if you want to help around the city, why don't you just tell your mother that?[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] As if she would listen.[P] She just scolds me.[P] I'm sick of it.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] You need to take a different angle.[P] Show her that you want to be responsible, not selfish.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Ask her to give you a job.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] A job?[P] She would never - [P][CLEAR]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Have you tried?[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] No...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Be polite, but firm.[P] Make it obvious that a compromise will be easier for both of you.[P] She's reasonable, she'll listen.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] ...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] You wouldn't leap over a mountain in one jump.[P] You need to climb it slowly and carefully.[P] Ask for a job where you'll be safe, and when she's okay with that job, work your way up.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] And you think that will work?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] You've nothing to lose and your future to gain![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Plus this is kind of sort of how I managed to get a car when I was seventeen.[P] I promised to use it to run errands for my mother, and before you knew it I was driving to classes and back without the chauffeur.[B][C]") ]])
		fnCutscene([[ Append("SX-399:[E|Neutral] I don't know what half of what you said means, but I may as well try.[P] Now or never, right?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Go ahead, I'll be right behind you.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		fnCutsceneMove("SX-399", 69.25, 13.50)
		fnCutsceneFace("Christine", -1, 1)
		fnCutsceneBlocker()
		fnCutsceneMove("SX-399", 70.25, 13.50)
		fnCutsceneFace("Christine", 0, 1)
		fnCutsceneBlocker()
		fnCutsceneMove("SX-399", 70.25, 15.50)
		fnCutsceneMove("SX-399", 59.25, 15.50)
		fnCutsceneMove("SX-399", 59.25, 16.50)
		fnCutsceneMove("SX-399", 56.25, 16.50)
		fnCutsceneMove("SX-399", 56.25, 22.50)
		fnCutsceneTeleport("JX-101", 52.25, 23.50)
		fnCutsceneTeleport("SX-399", 52.25, 22.50)
		fnCutsceneBlocker()
		fnCutsceneFace("SX-399", 0, 1)
		fnCutsceneBlocker()
	
	--All other cases, open immediately.
	else
		AL_SetProperty("Open Door", "DoorSX399")
		AudioManager_PlaySound("World|FlipSwitch")
	end

-- |[SX-399's Pod]|
elseif(sObjectName == "SXPod") then

	--Variables.
	local iSXUpgradeQuest  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
	local iSprocketGardening = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketGardening", "N")
	local iSprocketComputers = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketComputers", "N")
	local iSprocketBoilers   = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketBoilers", "N")
	local iSprocketLibrary   = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketLibrary", "N")
	local iSprocketChess     = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketChess", "N")
	local iSprocketPhotos    = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketPhotos", "N")
	local iSprocketHats      = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketHats", "N")
	local iSprocketDrawing   = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketDrawing", "N")

	--Sum.
	local iSum = iSprocketBoilers + iSprocketChess + iSprocketComputers + iSprocketDrawing + iSprocketGardening + iSprocketHats + iSprocketLibrary + iSprocketPhotos
	
	--No upgrade quest.
	if(iSXUpgradeQuest < 2.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought:[VOICE|Christine] (SX-399's recharging pod.[P] It has several special parts and extra gauges.)") ]])
		fnCutsceneBlocker()
	
	--Upgrade quest, but less than 3 activities done.
	elseif(iSXUpgradeQuest == 2.0 and iSum < 3) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought:[VOICE|Christine] (I haven't bought enough time yet.[P] I should go find some activities around town...)") ]])
		fnCutsceneBlocker()
		
	--Upgrade quest, ready to finish.
	elseif(iSXUpgradeQuest == 2.0 and iSum >= 3) then
		LM_ExecuteScript(fnResolvePath() .. "SpecialScripts.lua", "Finale")
	
	--Future cases.
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought:[VOICE|Christine] (SX-399's recharging pod.[P] Seems she won't be needing all the extra add-ons now!)") ]])
		fnCutsceneBlocker()
		
	end
	


-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
