-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
    
    -- |[DroidAA]|
    if(sActorName == "DroidAA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] It's quite a logistics mess to get steam piped to every residence without losing pressure.[P] I spend a lot of time fixing leaks.") ]])
        fnCutsceneBlocker()
    
    -- |[DroidAB]|
    elseif(sActorName == "DroidAB") then
	
        --Variables.
        local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
        
        --Common.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        
        if(iSXUpgradeQuest < 2.0) then
            fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] You're the human they found, right?[P] Sorry about that.[P] We've had to double the number of traps due to all the monsters in the mines recently.") ]])
        elseif(iSXUpgradeQuest == 2.0) then
            fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] Out for a night on the town?[P] Have fun!") ]])
        else
            fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] You were a golem all along...[P] Well, I suppose we'll take allies where we can find them.") ]])
        end
        fnCutsceneBlocker()
    
    -- |[DroidAC]|
    elseif(sActorName == "DroidAC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] These computers may not be as advanced as the ones in Regulus City, but nobody's spying on you when you log on.[P] That's worth the slow processing speeds.") ]])
        fnCutsceneBlocker()
    
    -- |[DroidAD]|
    elseif(sActorName == "DroidAD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] This isn't the oldest Steam Droid settlement, but it's one of the biggest.[P] We get migrants in here from time to time, and even the occasional escaped human!") ]])
        fnCutsceneBlocker()
    
    -- |[DroidAE]|
    elseif(sActorName == "DroidAE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] We have to make everything by hand.[P] We've scavenged a few fabricators, but they're always on the verge of breaking down.") ]])
        fnCutsceneBlocker()
    
    -- |[DroidAF]|
    elseif(sActorName == "DroidAF") then
	
        --Variables.
        local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
        
        --Common.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(iSXUpgradeQuest < 2.0) then
            fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] Were you out in the mines?[P] I've heard there's a lot of strange monsters prowling around.[P] Be careful if you're going out.") ]])
        elseif(iSXUpgradeQuest == 2.0) then
            fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] Nice work on the trap lines, SX-399.[P] You're doing the city proud.") ]])
        else
            fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] So now the golems are helping us with the monsters in the mines?[P] Strange times.") ]])
        end
        fnCutsceneBlocker()
        
    -- |[DroidAG]|
    elseif(sActorName == "DroidAG") then
	
        --Variables.
        local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
        
        --Common.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        
        if(iSXUpgradeQuest < 2.0) then
            fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] This is the headquarters of the Fist of Tomorrow.[P] Most of our members are out on patrol right now, as we're pretty much the only organized force in the mines.[P] It's a lot of ground to cover.") ]])
        elseif(iSXUpgradeQuest == 2.0) then
            fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] Welcome back, SX-399.[P] Enjoy your night!") ]])
        else
            fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] JX-101 gives you the vouch, so we'll support you.[P] But if it were up to me...") ]])
        end
        fnCutsceneBlocker()
    
    -- |[DroidAH]|
    elseif(sActorName == "DroidAH") then
	
        --Variables.
        local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
        
        --Common.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        
        if(iSXUpgradeQuest < 2.0) then
            fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] Oh, the human.[P] Sorry about the trap...") ]])
        elseif(iSXUpgradeQuest == 2.0) then
            fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] Hey, SX-399![P] If you want, I think one of the computers had some photos of Darkmatters and kittens and puppies!") ]])
        else
            fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] 771852 huh.[P] Well, if JX-101 says you're all right, you're all right.") ]])
        end
        fnCutsceneBlocker()
    
    -- |[DroidAI]|
    elseif(sActorName == "DroidAI") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] I joined Fist of Tomorrow because I wanted to make a difference, but we're just barely staying alive.[P] How do the other settlements get by without a leader like JX-101?") ]])
        fnCutsceneBlocker()
    
    -- |[DroidAJ]|
    elseif(sActorName == "DroidAJ") then
	
        --Variables.
        local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
        
        --Common.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        
        if(iSXUpgradeQuest < 2.0) then
            fnCutscene([[ Append("AJ-99:[VOICE|Steam Droid] There's a secret path just to the east of the drop trap.[P] I'd hate to have to fish you out of the canyon again![P] Ha ha!") ]])
        elseif(iSXUpgradeQuest == 2.0) then
            fnCutscene([[ Append("AJ-99:[VOICE|Steam Droid] Oh, SX-399, I checked over the springs on the spike trap earlier.[P] Nice work with the torsion calibration, couldn't have done it better myself.") ]])
        else
            fnCutscene([[ Append("AJ-99:[VOICE|Steam Droid] I guess it's reassuring that our traps managed to stop a Command Unit, right?[P] Unfortunately, it happened to be the nicest Command Unit we stopped...") ]])
        end
        fnCutsceneBlocker()
    
    -- |[DroidAK]|
    elseif(sActorName == "DroidAK") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] We just got back from a scavenging job.[P] Found some pretty good replacement pipes!") ]])
        fnCutsceneBlocker()
    
    -- |[DroidAL]|
    elseif(sActorName == "DroidAL") then
	
        --Variables.
        local iFixedSensor= VM_GetVar("Root/Variables/Chapter5/Scenes/iFixedSensor", "N")
        local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
        
        --Variables.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        
        --Reputation time.
        if(iSXUpgradeQuest < 2.0) then
            
            --Hasn't fixed it yet.
            if(iFixedSensor == 0.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] I found this busted sensor out in the mines, and I've been trying to fix it.[P] No luck so far.[B][C]") ]])
                fnCutscene([[ Append("Christine:[VOICE|Christine] (I guess nobody is going to know I'm a repair unit.[P] Let's change that!)[B][C]") ]])
                fnCutscene([[ Append("Christine:[VOICE|Christine] I'm pretty handy with machines.[P] Mind if I take a look?[B][C]") ]])
                fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] Knock yourself out.[B][C]") ]])
                fnCutscene([[ Append("Christine:[VOICE|Christine] ...[B][C]") ]])
                fnCutscene([[ Append("Christine:[VOICE|Christine] ...[B][C]") ]])
                fnCutscene([[ Append("Christine:[VOICE|Christine] Uh huh, busted transistor.[P] Luckily, the RM-75 modules have a spare set on the back panel.[P] See?[B][C]") ]])
                fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] Oh, that's what that thing was?[P] They put spare parts inside the sensor?[B][C]") ]])
                fnCutscene([[ Append("Christine:[VOICE|Christine] The transistors tend to be the first thing to go.[P] Particularly if you drop the sensor.[B][C]") ]])
                fnCutscene([[ Append("Christine:[VOICE|Christine] ...[B][C]") ]])
                fnCutscene([[ Append("Christine:[VOICE|Christine] All fixed![B][C]") ]])
                fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] Amazing![P] I've never seen a human as good as you are with repairs![B][C]") ]])
                fnCutscene([[ Append("Christine:[VOICE|Christine] Well I'm not a - [P][CLEAR]") ]])
                fnCutscene([[ Append("Christine:[VOICE|Christine] I mean, it's a talent.[P] I've always been like this.[P] Some people read novels, I read repair manuals.[B][C]") ]])
                fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] I'll be sure to tell JX-101 you got this thing working.[P] I'm sure there's a lot of stuff you could help fix around here.[B][C]") ]])
                fnCutscene([[ Append("Thought:[VOICE|Leader] (Gained 25 Reputation!)") ]])
                fnCutsceneBlocker()
                
                VM_SetVar("Root/Variables/Chapter5/Scenes/iFixedSensor", "N", 1.0)
                local iSteamDroidReputation = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N")
                VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N", iSteamDroidReputation + 25)
            
            --Already fixed it.
            else
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] Thanks a lot![P] I'll tell JX-101 that you got this thing fixed![P] Thanks again!") ]])
                fnCutsceneBlocker()
            
            end
        elseif(iSXUpgradeQuest == 2.0) then
            fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] Are you two out having fun?[P] Maybe you should play a game of Penkak.[P] One day you're going to beat your mother, and I want to be there to see it.") ]])
        else
            fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] It figures you'd be a golem with repair skills like those.[P] I wonder if we can upgrade the other Steam Droids...") ]])
        end
        
        --Common.
        fnCutsceneBlocker()
    
    -- |[Gardener]|
    elseif(sActorName == "Gardener") then
	
        --Variables.
        local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
        local iSprocketGardening = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketGardening", "N")
        
        --No upgrade quest, or already done.
        if(iSXUpgradeQuest ~= 2.0 or iSprocketGardening == 1.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] These plants may be mechanical, but they're just as alive as we are.[P] They need care and maintenance to stay alive.") ]])
            fnCutsceneBlocker()
        
        --Let's lube up some plants!
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            LM_ExecuteScript(gsRoot .. "Maps/RegulusMines/SprocketCityA/SpecialScripts.lua", "Gardening")
        end
    
    -- |[Vendor]|
    elseif(sActorName == "Vendor") then
    
        --Variables.
        local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
        local iSprocketHats = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprocketHats", "N")
        
        --No upgrade quest, or already done.
        if(iSXUpgradeQuest ~= 2.0 or iSprocketHats == 1.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] I've got stuff from Gearsville to Steamston.[P] What can I get you?") ]])
            fnCutsceneBlocker()
            
            fnCutscene([[ AM_SetShopProperty("Show", "Steam Droid Merchant", gsRoot .. "Maps/RegulusMines/SprocketCityA/Shop Setup.lua", "Null") ]])
            fnCutsceneBlocker()
        
        --HAAAAAAAAAAAAAAATS
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            LM_ExecuteScript(fnResolvePath() .. "SpecialScripts.lua", "Hats")
        end
    
    -- |[TT-233]|
    elseif(sActorName == "TT-233") then
	
        --Common.
        TA_SetProperty("Face Character", "PlayerEntity")
        
        --Variables.
        local iSXMet55        = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXMet55", "N")
        local iSpokeWithTT233 = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeWithTT233", "N")
        
        --Normal dialogue.
        if(iSXMet55 == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("TT-233:[VOICE|TT-233] I'm not really much of a doctor, I used to work as a veterinarian.[P] You're lucky we found a shipment of repair nanites that would work on organic tissue.") ]])
            fnCutsceneBlocker()
        
        --Dialogue about SX-399.
        elseif(iSXMet55 == 1.0 and iSpokeWithTT233 == 0.0) then
        
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeWithTT233", "N", 1.0)
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "TT-233", "Neutral") ]])
            fnCutscene([[ Append("TT-233: Good morrow, Christine.[P] How are your legs feeling?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] A little tired from all the running around, but otherwise wonderful.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Say, I heard about SX-399's condition...[B][C]") ]])
            fnCutscene([[ Append("TT-233: Listen, I know you want to help, but it's best to stay out of it.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Well, I'm good at repairs.[P] I could give it a try.[B][C]") ]])
            fnCutscene([[ Append("TT-233: Oh...[P] What exactly have you heard?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I spoke to SX-399 and she said she needs to recharge for most of the year because her core is leaking.[B][C]") ]])
            fnCutscene([[ Append("TT-233: Yes...[P] That's what we told her.[B][C]") ]])
            fnCutscene([[ Append("TT-233: It's for her own good that she believes that...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Excuse me?[B][C]") ]])
            fnCutscene([[ Append("TT-233: *Sigh*[B][C]") ]])
            fnCutscene([[ Append("TT-233: Christine, please don't get her hopes up.[P] Believe me when I say that we've tried.[P] We can't fix her.[B][C]") ]])
            fnCutscene([[ Append("TT-233: It's a wonder she's alive at all.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Really?[B][C]") ]])
            fnCutscene([[ Append("TT-233: We Steam Droids are made of old, flawed technology.[P] We break down every now and then, but we can usually be fixed.[B][C]") ]])
            fnCutscene([[ Append("TT-233: But, SX-399 is different.[P] I don't know if it's because she was converted when she was so young, or maybe her body just couldn't handle the change.[B][C]") ]])
            fnCutscene([[ Append("TT-233: She's fundamentally damaged.[P] If you replace her power core, it will break again within the day.[P] We've tried.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Oh dear...[P] What about the connectors?[B][C]") ]])
            fnCutscene([[ Append("TT-233: Fried.[P] I think it's her full system distribution, but the only way to find out would...[P] kill her...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] But there's got to be something we can do.[B][C]") ]])
            fnCutscene([[ Append("TT-233: No.[P] There isn't.[B][C]") ]])
            fnCutscene([[ Append("TT-233: My estimate is that she's got one, maybe two more cycles before she just...[P] doesn't wake up again.[B][C]") ]])
            fnCutscene([[ Append("TT-233: She'd still be alive, but the distribution system wouldn't produce enough power to keep her conscious.[P] She's been slowly losing power cohesion more each year.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] And you haven't told her?[B][C]") ]])
            fnCutscene([[ Append("TT-233: JX-101's orders.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] What!?[B][C]") ]])
            fnCutscene([[ Append("TT-233: Do you want to be the one who tells a girl she's got maybe ten days of lucidity before she dies?[P] Better that she goes to sleep one day, happy and secure, than deal with the anxiety.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] But you can't just keep her in the dark like that![B][C]") ]])
            fnCutscene([[ Append("TT-233: Not my call.[P] We respect JX-101's decision.[P] I happen to agree with it.[P] Even if you don't, you best not tell her.[P] Not if you want to live in Sprocket City.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] (Yeah, JX-101 wouldn't help our rebellion if I did that...)[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Okay, I won't tell her.[P] But I'm not giving up.[B][C]") ]])
            fnCutscene([[ Append("TT-233: Believe me, I want you to succeed.[P] But...[P] We've been trying for a long time...") ]])
            fnCutsceneBlocker()
            
        --Dialogue about SX-399.
        elseif(iSXMet55 == 1.0 and iSpokeWithTT233 == 1.0) then
            
            --Variables.
            local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
            
            --Common.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            
            if(iSXUpgradeQuest < 2.0) then
                fnCutscene([[ Append("TT-233:[VOICE|TT-233] Please don't tell SX-399 the truth.[P] It's for the best.") ]])
            elseif(iSXUpgradeQuest == 2.0) then
                fnCutscene([[ Append("TT-233:[VOICE|TT-233] Oh, SX-399![P] I thought you'd be recharging by now.[P] Don't stay up too late.") ]])
            else
                fnCutscene([[ Append("TT-233:[VOICE|TT-233] Christine, I'm astounded by what you've done.[P] SX-399 owes you her life, and I owe you an apology.[P] I guess it really was possible to upgrade a Steam Droid.") ]])
            end
            fnCutsceneBlocker()
        end
    
    -- |[UE117]|
    elseif(sActorName == "UE117") then
        
        --Variables.
        local iMetUE117 = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetUE117", "N")
        local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
        
        --Impersonation.
        if(iSXUpgradeQuest == 2.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("SH-505:[VOICE|Steam Droid] 'ello, loves?[P] Enjoying the art studio?") ]])
        
        --First meeting.
        elseif(iMetUE117 == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("UE-117:[VOICE|Steam Droid] 'ello there, name's UE-117.[P] Pleased to meet ye.[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] Oh my goodness, is that a Portsmouth accent?[P] Are you from the UK?[B][C]") ]])
            fnCutscene([[ Append("UE-117:[VOICE|Steam Droid] Never 'eard of it.[P] What's a UK?[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] Oh, nevermind.[P] Must be a coincidence.[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] So, what's this piece here?[B][C]") ]])
            fnCutscene([[ Append("UE-117:[VOICE|Steam Droid] I call it 'Harley the Dog'.[P] He's a chihuahua.[P] You like it?[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] It's just lovely.[P] Well done![B][C]") ]])
            fnCutscene([[ Append("UE-117:[VOICE|Steam Droid] Wunnaful![P] I've just started carving, but I'm getting better every day.") ]])
            fnCutsceneBlocker()
            
            VM_SetVar("Root/Variables/Chapter5/Scenes/iMetUE117", "N", 1.0)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("UE-117:[VOICE|Steam Droid] Most 'a these pieces are by SH-505.[P] I prefer to do pixel work and carvings.") ]])
            fnCutsceneBlocker()
        end
    
    -- |[SH505]|
    elseif(sActorName == "SH505") then
	
        --Variables.
        local iMetSH505 = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetSH505", "N")
        local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
        
        --Impersonation.
        if(iSXUpgradeQuest == 2.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("SH-505:[VOICE|Steam Droid] Ugh, leave me alone.[P] I'm not in a good mood right now.[B][C]") ]])
            fnCutscene([[ Append("SH-505:[VOICE|Steam Droid] My straitjacket is supposed to arrive tomorrow.[P] That might cheer me up.") ]])
        
        --First meeting.
        elseif(iMetSH505 == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("SH-505:[VOICE|Steam Droid] Oh look, there's that human the other droids were talking about.[P] What do you want?[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] Hmm, I can't quite place your accent.[P] Where are you from?[B][C]") ]])
            fnCutscene([[ Append("SH-505:[VOICE|Steam Droid] Lerbow.[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] (Must be someplace on Pandemonium...)[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] So what are you working on?[B][C]") ]])
            fnCutscene([[ Append("SH-505:[VOICE|Steam Droid] This one is called 'Horse Bondage VII'.[P] It's -[P] well, I don't think it needs an explanation.[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] Well you're very up front about it.[B][C]") ]])
            fnCutscene([[ Append("SH-505:[VOICE|Steam Droid] Should I not be?[P] I like what I like, and I draw it.[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] I wasn't putting you down![P] I like it![B][C]") ]])
            fnCutscene([[ Append("SH-505:[VOICE|Steam Droid] I would say thank you, but the damage is already done.[P] Now I am depressed.[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] Come on, cheer up![B][C]") ]])
            fnCutscene([[ Append("SH-505:[VOICE|Steam Droid] ...[P] Mrow...[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] What can I do to make it up to you?[B][C]") ]])
            fnCutscene([[ Append("SH-505:[VOICE|Steam Droid] Nothing.[P] You can't cure it.[P] Mrow.") ]])
            fnCutsceneBlocker()
            
            VM_SetVar("Root/Variables/Chapter5/Scenes/iMetSH505", "N", 1.0)
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("SH-505:[VOICE|Steam Droid] ...[P] Mrow...") ]])
            fnCutsceneBlocker()
        end
    
    -- |[FormerPrisonerA]|
    elseif(sActorName == "FormerPrisonerA") then
	
        --Variables.
        local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
        
        --Common.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        
        if(iSXUpgradeQuest < 2.0) then
            fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] Ah, is that Christine?[P] I can't see anything, I'm afraid.[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] Yes, it's me.[P] Have your optical receptors not been repaired yet?[B][C]") ]])
            fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] TT-233 said it will be a little while yet, apparently we're going to make new ones based on some schematics?[P] But, at least the nightmare is over...") ]])
        elseif(iSXUpgradeQuest == 2.0) then
            fnCutscene([[ Append("Christine:[VOICE|Christine] Hello there.[B][C]") ]])
            fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] Ah, is that Christine?[P] I can't see anything, I'm afraid.[B][C]") ]])
            fnCutscene([[ Append("JX-101:[VOICE|JX-101] No no, this is SX-399.[P] Do you remember?[B][C]") ]])
            fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] Ah, your sweet cherry.[P] Glad to see -[P] or rather, hear -[P] you again.[P] But you sound an awful lot like Christine...") ]])
        else
            fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] Hello, Christine.[P] My optical receptors are still disabled, but I can tell from your walk that you are...[P] happy.[P] I'm happy for you, whatever the cause.") ]])
        end
        fnCutsceneBlocker()
    
    -- |[FormerPrisonerB]|
    elseif(sActorName == "FormerPrisonerB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] Ha ha, oops.[P] I'm trying to fix these vats, but my hands keep shaking.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] It'll be all right.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] I heard the other escapees made out a lot worse than I did.[P] I can live with shaking hands.") ]])
        fnCutsceneBlocker()
    
    -- |[FormerPrisonerC]|
    elseif(sActorName == "FormerPrisonerC") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] Not dead, not alive, bits and parts.[P] Outside inside.[P] What?[P] I have to stay how I am, I can't live without my life.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] (Seems she's still shaken from...[P] whatever the golems did to her...)") ]])
        fnCutsceneBlocker()
    
    -- |[FormerPrisonerD]|
    elseif(sActorName == "FormerPrisonerD") then
	
        --Variables.
        local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
        
        --Common.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        
        if(iSXUpgradeQuest < 2.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Good show, 771852![P] Everything is turning out like it did last time, but slightly different![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Why do you keep acting like you know what's going to happen?[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Emptiness![P] You're not empty right now, but you will be soon.[P] Or maybe you were earlier, it's hard to keep track.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] And what does that mean?[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] There are periods where we cease to exist, and then suddenly exist again.[P] Of course we don't know about them because we're not there to see them, but they happen.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] And they happen again and again and again.[P] You see how much we can learn when we open ourselves to new ways of thinking?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] It's just crazed babble...") ]])
            fnCutsceneBlocker()
        elseif(iSXUpgradeQuest == 2.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "JX-101", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Oh, oh![P] Very clever, very clever, makeup and sawdust.[P] But I see through it.[P] It's all clear![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("JX-101:[E|Neutral] Do not trouble yourself with this one, sweetie. She's not worth the effort.") ]])
        else
            fnCutscene([[ Append("Golem:[VOICE|GolemLord] It does not matter how many sides it has, most of the space is, in fact, empty.[P] The pressure of emptiness is so much higher than that of fullness, you see, so it holds its shape even in the void.[P] Ah ah.[P] No, that stings!") ]])
        end
        fnCutsceneBlocker()
    
    -- |[Psychologist]|
    elseif(sActorName == "Psychologist") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Psychologist:[VOICE|Steam Droid] This patient is completely lucid, but speaks incoherently about things seemingly at random.[P] She starves herself of power for days and then binges on oil, and scrawls gibberish on the walls with nail polish.[B][C]") ]])
        fnCutscene([[ Append("Psychologist:[VOICE|Steam Droid] I'm not educated on golem system architecture, but nothing seems to be wrong on the outside.[P] It is most troubling.") ]])
        fnCutsceneBlocker()
        
    -- |[RescueDroidA]|
    elseif(sActorName == "RescueDroidA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] Thank goodness you're all right!") ]])
        fnCutsceneBlocker()
        
    -- |[RescueDroidB]|
    elseif(sActorName == "RescueDroidB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] We were about to come rescue you, but I guess you rescued yourself.") ]])
        fnCutsceneBlocker()
        
    -- |[RescueDroidC]|
    elseif(sActorName == "RescueDroidC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] You don't want to get captured by the golems, or you'll wind up a robot slave...[P] forever...") ]])
        fnCutsceneBlocker()
    
    end
end
