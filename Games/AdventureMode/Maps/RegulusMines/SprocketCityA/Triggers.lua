-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Start the mines cutscene.
if(sObjectName == "IntroInBed") then

	--Variables.
	local iSawWakeUpIntro = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawWakeUpIntro", "N")
	if(iSawWakeUpIntro == 1.0) then return end
	
    --Get Christine's form at the start of this sequence.
    local sOriginalForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    
	--Spawn NPCs.
	fnCutsceneTeleport("TT-233", 60.25, 11.50)
	fnCutsceneTeleport("JX-101", 60.25, 12.50)
	fnCutsceneFace("JX-101", -1, 0)
	fnCutsceneFace("TT-233", -1, 0)
	
	--Remove 55.
	giFollowersTotal = 0
	gsaFollowerNames = {}
	giaFollowerIDs = {0}
    AdvCombat_SetProperty("Party Slot", 1, "Null")
	AL_SetProperty("Unfollow Actor Name", "Tiffany")

	--If the 55 entity happens to be on the field, move her off.
	if(EM_Exists("Tiffany") == true) then
		EM_PushEntity("Tiffany")
			TA_SetProperty("Position", -10, -10)
		DL_PopActiveObject()
	end

	--Flag to indicate she is not following Christine.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSawWakeUpIntro", "N", 1.0)
	
	--Black the screen out.
	fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Christine:[VOICE|Christine] (Unghh...[P] it hurts...[P] Sophie...[P] please fix me...)") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(185)
	fnCutsceneBlocker()
    
    --Topics
    WD_SetProperty("Unlock Topic", "SteamDroids", 1)
	
	--Quickly shift Christine to Golem for the purposes of this dream sequence.
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
	fnCutsceneWait(1)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] Honey, I've returned with the shopping![B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Neutral] The shopping?[P] Hee hee![P] Is that what you call it where you're from?[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Smirk] And I see we've upgraded to honey now.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Blush] I can call you anything you like, as long as it's sweet, like you.[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Smirk] I've always liked 'dearest'.[P] Would you like that?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Happy] Of course I would, dearest![B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Sad] Oh, I seem to have damaged myself.[P] Drat.[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Neutral] Let me take a look at it...[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Smirk] Nothing the repair nanites can't fix.[P] You'll be fine in no time.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Blush] Sophie, I love you.[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Smirk] Aww, I love you too, dearest![B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] *Kiss*[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Offended] Ack![P] Don't kiss me![B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Sad] But I - [B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Offended] Wake up, you stupid organic![P] Wake up!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Shift to human.
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "CostumeHandlers/Christine/Human_Nude.lua") ]])
	fnCutsceneWait(1)
	fnCutsceneBlocker()
	
	--Christine switches to special frames.
	fnCutsceneSetFrame("Christine", "BedSleep")
	fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "JX-101", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "TT-233", "Neutral") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] How is she doing, doc?[B][C]") ]])
	fnCutscene([[ Append("TT-233:[E|Neutral] Well, the nanites must be repairing her faster than expected, because she just tried to kiss me.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] By way of thanks?[B][C]") ]])
	fnCutscene([[ Append("TT-233:[E|Neutral] She's still sleeping.[P] Maybe organics kiss in their sleep.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] When you were an organic, did you kiss anyone in your sleep?[B][C]") ]])
	fnCutscene([[ Append("TT-233:[E|Neutral] And just how would I know that, JX?[P] I would be *asleep*.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Frame change.
	fnCutsceneSetFrame("Christine", "BedWake")
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Christine:[VOICE|Christine] Ungh...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Frame change.
	fnCutsceneSetFrame("Christine", "BedSleep")
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneSetFrame("Christine", "BedWake")
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneSetFrame("Christine", "BedSleep")
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneSetFrame("Christine", "BedWakeR")
	fnCutsceneWait(65)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "JX-101", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "TT-233", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Sad] ...[P] Mmm, was I dreaming?[P] What happened?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Sad] (Uh oh, I'm surrounded by Steam Droids and I can't move...)[B][C]") ]])
	fnCutscene([[ Append("TT-233:[E|Neutral] You hurt yourself pretty bad in that fall, so relax.[P] The nanites need more time to repair your legs and spine.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] The fall?[P] Oh, yes, the fall.[P] I remember the fall.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Offended] I don't seem to remember the landing, though.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Just what do you remember?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] JX-101?[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] That is correct.[P] I do not recall your face, have we met?[B][C]") ]])
    
    --Was a human to begin with:
    if(sOriginalForm == "Human") then
        fnCutscene([[ Append("Christine:[E|Neutral] (I seem to have survived the fall even with my weak human legs...[P] so that's something.)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] (I bet my golem legs would have been just fine...[EMOTION|Christine|Sad] Wait, 55!)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] (55 probably got really damaged...[P] I hope she made it out as well as I did...[P] Better play dumb until I can find her.)[B][C]") ]])
        
    --Raiju:
    elseif(sOriginalForm == "Raiju") then
        fnCutscene([[ Append("Christine:[E|Neutral] (I seem to have survived the fall even with my weak organic legs...[P] so that's something.)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] (I bet my golem legs would have been just fine...[EMOTION|Christine|Sad] Wait, 55!)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] (55 probably got really damaged...[P] I hope she made it out as well as I did...[P] Better play dumb until I can find her.)[B][C]") ]])
    
    --Golem:
    elseif(sOriginalForm == "Golem") then
        fnCutscene([[ Append("Christine:[E|Neutral] (I'm human again...[P] I must have transformed after the fall...)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] (But 55 -[P] Command Unit legs aren't as sturdy as Golem legs![P] I hope she's okay...)[B][C]") ]])
    
    --Doll:
    elseif(sOriginalForm == "Doll") then
        fnCutscene([[ Append("Christine:[E|Neutral] (I'm human again...[P] I must have transformed after the fall...)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] (I hope 55 is okay.[P] Her legs aren't as durable as mine.[P] Maybe because of the runestone's effects.)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] (I can probably fix her, if I can find her...)[B][C]") ]])
        
    --Latex Drone:
    elseif(sOriginalForm == "LatexDrone") then
        fnCutscene([[ Append("Christine:[E|Neutral] (I'm human again...[P] I must have transformed after the fall...)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] (I didn't liquify on impact.[P] Maybe that latex bondage suit kept me together enough to transform?)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] (Oh no, 55![P] I bet they got her![P] I hope she's in a better shape than I am...)[B][C]") ]])
    
    --Darkmatter:
    elseif(sOriginalForm == "Darkmatter") then
        fnCutscene([[ Append("Christine:[E|Neutral] (I'm human again...[P] I must have transformed after the fall...)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] (Drat, I should have spent more time mastering my starlight body.[P] I'm sure I could have dissociated and went through the rock...)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] (Oh no, 55![P] She can't dissociate![P] Her hardmatter legs would crack like an egg on that rock!)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] (Hurry up you stupid, squishy legs![P] Heal![P] I need to help 55!)[B][C]") ]])
    
    --Eldritch Dreamer:
    elseif(sOriginalForm == "Eldritch") then
        fnCutscene([[ Append("Christine:[E|Neutral] (I'm human again...[P] I must have transformed after the fall...)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] (I was knocked out, but -[P] I guess I transformed before I entered any sort of REM sleep.[P] Phew.[P] 55 would be so cross - )[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] (Oh no, 55![P] They must have got her too![P] Better play dumb!)[B][C]") ]])
        
    --Steam Droid:
    elseif(sOriginalForm == "SteamDroid") then
        fnCutscene([[ Append("Christine:[E|Neutral] (I'm human again...[P] I must have transformed after the fall...)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] (The steam droid chassis can take a beating, but I'm not sure even my golem form would have been okay.[P] I wonder how 55's chas - )[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] (Oh no, 55![P] They must have got her too![P] Better play dumb!)[B][C]") ]])
    
    --Electrosprite! Shocking!
    elseif(sOriginalForm == "Electrosprite") then
        fnCutscene([[ Append("Christine:[E|Neutral] (I'm human again...[P] I must have transformed after the fall...)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] (How does electricity get hurt from falling really far?[P] I bet 55 would have a theory - )[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] (Oh no, 55![P] They must have got her too![P] Better play dumb until I can get to her!)[B][C]") ]])
    
    --Secrebot!
    elseif(sOriginalForm == "Secrebot") then
        fnCutscene([[ Append("Christine:[E|Neutral] (I'm human again...[P] I must have transformed after the fall...)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] (Oh I hope my wheel won't be blown out when I re-transform.[P] I bet 55 will -)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] (Oh no, 55![P] They must have got her too![P] Better play dumb until I can get to her!)[B][C]") ]])
    end
    
	fnCutscene([[ Append("Christine:[E|Neutral] Erm, I'm not sure.[P] My memory is fuzzy.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] My name's 77 -[P] uh, Christine.[P] Christine Dormer.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(65)
	fnCutsceneBlocker()
	
	--Frames.
	fnCutsceneSetFrame("Christine", "BedSleep")
	fnCutsceneWait(125)
	fnCutsceneBlocker()
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Christine:[VOICE|Christine] Very sorry, but you two were being terribly loud.[P] It's bad form to walk into someone's room when they're sleeping.") ]])
	fnCutsceneBlocker()
	fnCutsceneSetFrame("Christine", "BedFullR")
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "JX-101", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "TT-233", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Wait, am I in a hospital?[B][C]") ]])
	fnCutscene([[ Append("TT-233:[E|Neutral] Of sorts.[P] We don't have many organic visitors, so I converted the visitor's quarters into a hospice.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Listen, Christine. We need your help.[P] Whatever you can tell us would be greatly appreciated.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] My help?[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Yes.[P] You may not remember it, but you triggered one of our traps.[P] We set them up around the entrances to the settlement.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] I assume that Command Unit that we found was chasing you?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] Uhhh...[P] Yes.[P] She was chasing me.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Do you know anything about her?[P] Her designation, perhaps?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] I'm sorry, but I don't know anything about her.[P] I -[P] I was kidnapped, I think, and I woke up in an airlock.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] I escaped, but those robot girls have been chasing me ever since.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] (I mean that's partially true, so hopefully 55 won't contradict my story later if they interrogate her...)[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Well, the thing that was chasing you is called a Command Unit.[P] They're high ranking in Regulus City.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Sad] Is she all right?[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] She's in system standby.[P] We can't seem to reactivate her, but she's alive.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Why do you ask?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Sad] ...[P] I don't want to hurt anyone.[P] I'm sorry for the trouble I've caused.[B][C]") ]])
	fnCutscene([[ Append("TT-233:[E|Neutral] You'll need to give the nanites some time to finish repairing you.[P] After that...[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] You're welcome to stay with us, if you like.[P] I'm sorry, but it is unlikely we will be able to return you home under the present circumstances.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] (Better act dumb...)[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] I suppose I don't have much of a choice.[P] I think I can walk again, almost...[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] Err, do I need to be naked for the nanites to work?[B][C]") ]])
	fnCutscene([[ Append("TT-233:[E|Neutral] Ah, of course.[P] I'll get your clothes.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Black the screen out.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	fnCutsceneWait(125)
	fnCutsceneBlocker()
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Christine:[VOICE|Christine] (This may be a lewd game, but it's still impolite to watch a girl change, you know.)") ]])
	fnCutsceneBlocker()
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "CostumeHandlers/Christine/Human_Normal.lua") ]])
	fnCutsceneWait(125)
	fnCutsceneBlocker()
	
	--Reposition and fade in.
	fnCutsceneSetFrame("Christine", "Null")
	fnCutsceneTeleport("Christine", 60.25, 11.50)
	fnCutsceneTeleport("JX-101", 52.25, 23.50)
	fnCutsceneTeleport("TT-233", 61.25, 15.50)
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Christine:[VOICE|Christine] (Those nanites really worked a treat![P] I feel almost as good as golem-me feels!)[B][C]") ]])
	fnCutscene([[ Append("Christine:[VOICE|Christine] (Considering I'm not a pancake after falling six stories, this is about as good as I can hope for.)[B][C]") ]])
	fnCutscene([[ Append("Christine:[VOICE|Christine] (All right, I should probably confer with 55 before I do anything else...)") ]])
	fnCutsceneBlocker()

--Go back and talk to JX-101.
elseif(sObjectName == "TalkToJX") then

	--Variables.
	local iMetJX101 = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetJX101", "N")
	if(iMetJX101 == 1.0) then return end
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Christine:[VOICE|Christine] (I should go speak to JX-101 before I head out...)") ]])
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneMove("Christine", 52.25, 18.50)
	fnCutsceneBlocker()
	
--55 headlock scene.
elseif(sObjectName == "55Headlock") then

	--Variables.
	local iSawHeadlockScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawHeadlockScene", "N")
	local iSprung55 = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprung55", "N")
	if(iSawHeadlockScene == 1.0 or iSprung55 == 0.0) then return end
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSawHeadlockScene", "N", 1.0)
	
	--Black the screen out.
	fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	
	--Set positions and poses.
	fnSpecialCharacter("Tiffany", 24, 7, gci_Face_South, false, nil)
	fnCutsceneTeleport("Christine", 24.25, 8.50)
	fnCutsceneTeleport("DroidAD", 26.25, 7.50)
	fnCutsceneTeleport("DroidAI", 21.25, 9.50)
	fnCutsceneTeleport("DroidAF", 28.25, 9.50)
	fnCutsceneTeleport("JX-101", 24.25, 18.50)
	fnCutsceneTeleport("TT-233", 23.25, 18.50)
	fnCutsceneFace("DroidAD", -1, 0)
	fnCutsceneFace("DroidAI", 1, 0)
	fnCutsceneFace("DroidAF", -1, 0)
	fnCutsceneBlocker()
	
	--Fade in.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()
	
	--55 and Christine move a bit.
	fnCutsceneMove("Tiffany", 24.25, 8.50, 0.30)
	fnCutsceneMove("Christine", 24.25, 9.50, 0.30)
	fnCutsceneBlocker()
	fnCutsceneMove("JX-101", 24.25, 11.50, 2.50)
	fnCutsceneMove("TT-233", 23.25, 11.50, 2.50)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Stop right there![P] Let the human go![B][C]") ]])
	fnCutscene([[ Append("55:[E|Upset] Order denied.[P] Clear my path, or the human dies.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] You're surrounded, and we will open fire if the human is not released in 3,[P][P] 2,[P][P] - [P][CLEAR]") ]])
	fnCutscene([[ Append("55:[E|Upset] In the time it takes you to pull the trigger, I will have computed the projectile trajectory and repositioned the human to intercept it.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] If you kill the girl, you won't leave here in one piece![B][C]") ]])
	fnCutscene([[ Append("55:[E|Upset] Your firearm is a Mk. IV Pulse Pistol.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] ...[P] Correct.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] You scavenged it from a waste dump.[P] The weapon has been superseded by the Mk. V series due to a flaw in the accelerator arrays.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] When not regularly maintained, as yours is, the array malfunctions and diffuses radiation through the barrel.[P] The pulse round loses half of its charge.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] You, and all your soldiers,[P] firing simultaneously,[P] will do little more than graze my reinforced chassis.[P] The human will be torn to shreds.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] It will not be I who kills her, but you.[P] Now...[B][C]") ]])
	fnCutscene([[ Append("55:[E|Angry] Stand.[P][P] Aside.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] .[P].[P].[P] Do what she says.[P] Weapons down.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Move.
	fnCutsceneMove("JX-101", 26.25, 11.50, 1.00)
	fnCutsceneMove("TT-233", 25.25, 11.50, 1.00)
	fnCutsceneFace("JX-101", -1, 0)
	fnCutsceneFace("TT-233", -1, 0)
	fnCutsceneBlocker()
	fnCutsceneMove("Tiffany", 24.25, 13.50, 0.30)
	fnCutsceneMove("Christine", 24.25, 14.50, 0.30)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutscene([[ Append("55:[E|Neutral] Attack me, and I will execute the human.[P] Attempt to follow me, and I will execute the human.[P] Is that clear?[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] ...[P] As crystal...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Movement.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneMove("Tiffany", 24.25, 16.50, 0.30)
	fnCutsceneMove("Christine", 24.25, 17.50, 0.30)
	fnCutsceneWait(180)
	fnCutsceneBlocker()
	
	--Move to the next map.
	fnCutscene([[ AL_BeginTransitionTo("TelluriumMinesE", "FORCEPOS:31.0x14.0x0") ]])
	fnCutsceneBlocker()

--After the cutscene where 55 drags Christine out.
elseif(sObjectName == "After55") then

	--Variables.
	local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
	local iSawAfter55Scene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawAfter55Scene", "N")
	local iSprung55 = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprung55", "N")
	
	--SX-399 Upgrade quest.
	if(iSXUpgradeQuest == 1.5) then
	
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N", 2.0)
		
		--Add JX-101 to the follower list.
		fnAddPartyMember("JX-101")
		
		--Lock fadeout.
		fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
		fnCutsceneBlocker()
		
		--Teleport JX-101 over.
		fnCutsceneTeleport("JX-101", 19.25, 18.50)
		fnCutsceneFace("Christine", 0, 1)
		
		--Fade in slowly.
		fnCutsceneWait(45)
		fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] (Okay, just gotta play it cool.[P] Act like SX-399, try to avoid too much contact with JX-101, but make sure I'm seen so nobody wonders where I am.[P] Easy.)") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Movement.
		fnCutsceneMove("Christine", 21.25, 37.50)
		fnCutsceneMove("Christine", 24.25, 34.50)
		fnCutsceneMove("Christine", 24.25, 32.50)
		fnCutsceneMove("Christine", 20.25, 29.50)
		fnCutsceneMove("Christine", 19.25, 27.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("JX-101:[VOICE|JX-101] Oh, there you are, sweetie!") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Movement.
		fnCutsceneFace("Christine", 0, -1)
		fnCutsceneMove("JX-101", 19.25, 26.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] Thank the gears![P] I was just about to put together a search team![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] (Great, that plan lasted all of five seconds.)[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Oh mother, you worry too much.[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] With good reason.[P] If you had seen the things we saw on the last mission...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] I'm sure I can imagine.[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] Well, sweetie, this is your last night before you go in for recharging.[P] No staying up late, all right?[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] Say, I was thinking we could...[P] spend it together?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Oh, mother, I was planning to - [P][CLEAR]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] I know, I know.[P] I'm willing to compromise.[P] You pick the activity.[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] Isn't that what you asked me for earlier?[P] Well, here it is![P] Isn't that great?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Well, as long as you let me pick...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] (Drat![P] I can't break character!)[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] I'll lead.[P] See if you can keep up, mother![P] Hee hee![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] (I'll need to find some activities to keep JX-101 distracted.[P] I think I'll need to do at least 3 things...)[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] (Examining things around town may provide me with some ideas...)") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneMove("JX-101", 19.25, 27.50)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
		
		--Remove JX-101's dialogue script and collision flag.
		EM_PushEntity("JX-101")
			TA_SetProperty("Clipping Flag", false)
			TA_SetProperty("Activation Script", "Null")
		DL_PopActiveObject()
		
		--Only Christine is in the party.
        AdvCombat_SetProperty("Party Slot", 1, "Null")
        AdvCombat_SetProperty("Party Slot", 2, "Null")
        AdvCombat_SetProperty("Party Slot", 3, "Null")
	
		return
	end

	--Rescue droids.
	if(iSawAfter55Scene == 1.0 or iSprung55 == 0.0) then return end
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSawAfter55Scene", "N", 1.0)
	
	--Black the screen out.
	fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	
	--Character positioning.
	fnCutsceneTeleport("JX-101", 20.25, 29.50)
	fnCutsceneFace("JX-101", 0, -1)
	TA_Create("RescueDroidA")
		TA_SetProperty("Position", 19, 27)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Clipping Flag", true)
		TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
		fnSetCharacterGraphics("Root/Images/Sprites/SteamDroid/", false)
	DL_PopActiveObject()
	TA_Create("RescueDroidB")
		TA_SetProperty("Position", 20, 27)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Clipping Flag", true)
		TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
		fnSetCharacterGraphics("Root/Images/Sprites/SteamDroid/", false)
	DL_PopActiveObject()
	TA_Create("RescueDroidC")
		TA_SetProperty("Position", 21, 27)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Clipping Flag", true)
		TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
		fnSetCharacterGraphics("Root/Images/Sprites/SteamDroid/", false)
	DL_PopActiveObject()
	
	--Reposition Christine.
	fnCutsceneTeleport("Christine", 20.25, 37.50)
	fnCutsceneBlocker()
	
	--Camera focus on JX-101..
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 5.0)
		CameraEvent_SetProperty("Focus Actor Name", "JX-101")
	DL_PopActiveObject()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Fade back in.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneMove("Christine", 24.25, 33.50)
	fnCutsceneMove("Christine", 24.25, 32.50)
	fnCutsceneFace("JX-101", 0, -1)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "JX-101", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] All right, listen up.[P] The objective is the Command Unit, and we need to intercept her before she reaches the surface.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] She's got a hostage, and will kill her if we're seen to be tailing them.[P] Stay out of sight.[B][C]") ]])
	fnCutscene([[ Append("Steam Droid: Ma'am?[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Do you have a question?[B][C]") ]])
	fnCutscene([[ Append("Steam Droid: Is the hostage that human that AJ-99 found?[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Yes.[B][C]") ]])
	fnCutscene([[ Append("Steam Droid: And does she wear a lot of violet clothes?[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Yes.[B][C]") ]])
	fnCutscene([[ Append("Steam Droid: And is she standing right behind you?") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneFace("JX-101", 0, 1)
	
	--Movement.
	fnCutsceneMove("Christine", 21.25, 29.50)
	fnCutsceneFace("Christine", -1, 0)
	fnCutsceneBlocker()
	fnCutsceneFace("JX-101", 1, 0)
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 1.0)
		CameraEvent_SetProperty("Focus Actor Name", "Christine")
	DL_PopActiveObject()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] Am I interrupting my own rescue briefing?[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] You're -[P] how did you escape?[P] What happened to the Command Unit?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] I convinced her to let me go.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] But how?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Sad] Well, a creature with more teeth than face showed up, and came after her.[P] It made a very cohesive argument, and I ran off as it attacked her.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Sad] She...[P] did not survive the encounter...[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] You are certain of this?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] The thing crushed her, and then dragged her off...[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Hm...[P] A fortunate turn of events.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Sad] For everyone except her.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Waste not your pity on the Command Units.[P] They are ruthless killers with no remorse.[P] They think of no one save their precious Central Administration, not even themselves.[P] It is better she is dead.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Sad] ...[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Well then.[P] I am glad you are well.[P] I will assemble a team to ascertain the final fate of this Command Unit, but as there is no time pressure, it may be best to wait.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Please, make yourself comfortable and see TT-233 if you have any injuries.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] I will, thank you.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

--Finale cutscene.
elseif(sObjectName == "SXCutsceneTrigger") then

	--Variables.
	local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
	if(iSXUpgradeQuest ~= 3.0) then return end

	--Execute.
	LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/500 Normal/Sprocket City Finale/Scene_B_SprocketCityA.lua")

--Go back and talk to AJ-99
elseif(sObjectName == "TalkToAJ") then

	--Variables.
	local iMetAJ99 = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetAJ99", "N")
	if(iMetAJ99 == 1.0) then return end
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iMetAJ99", "N", 1.0)
	
	--Facing.
	fnCutsceneFace("DroidAJ", 1, 0)
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] Hey, there she is!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneMove("Christine", 13.25, 38.50)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
	fnCutscene([[ Append("AJ-99:[E|Neutral] Hey there, human![P] I'm AJ-99![B][C]") ]])
	fnCutscene([[ Append("AJ-99:[E|Neutral] Well, you probably don't remember me, but I'm the one who fished you out of the ravine you fell into.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Happy] Oh![P] Thanks a lot![B][C]") ]])
	fnCutscene([[ Append("AJ-99:[E|Neutral] Don't mention it![B][C]") ]])
	fnCutscene([[ Append("AJ-99:[E|Neutral] If you're heading out, climb the ladder and then check the wall on the east side.[P] We've got a secret passage set up.[B][C]") ]])
	fnCutscene([[ Append("AJ-99:[E|Neutral] And, obviously, don't tell any of the golems about it, because then the trap is worthless.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] Thanks for the tip.[P] I'd rather not repeat that particular episode.") ]])
	fnCutsceneBlocker()
end
