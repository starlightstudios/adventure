-- |[Special Scripts]|
--These are used during the JX-101 distraction subquest. The argument provided is the name of the activity.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Activity name.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Chess/Penkak]|
--A game of memorization for pros and strategy for amateurs.
if(sObjectName == "Penkak") then
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSprocketChess", "N", 1.0)

	--Spawn some NPCs.
	TA_Create("ObserverA")
		TA_SetProperty("Position", -10, -10)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Clipping Flag", false)
		fnSetCharacterGraphics("Root/Images/Sprites/SteamDroid/", false)
	DL_PopActiveObject()
	TA_Create("ObserverB")
		TA_SetProperty("Position", -10, -10)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Clipping Flag", false)
		fnSetCharacterGraphics("Root/Images/Sprites/SteamDroid/", false)
	DL_PopActiveObject()
	TA_Create("ObserverC")
		TA_SetProperty("Position", -10, -10)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Clipping Flag", false)
		fnSetCharacterGraphics("Root/Images/Sprites/SteamDroid/", false)
	DL_PopActiveObject()

	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Ah, Penkak.[P] The ancient game of strategy.[P] Do you think tonight's the night you finally take your mother down?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] (Hoo, maybe it is!)[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] I've got some new strategies in mind.[P] It'll be like you're playing against a completely different person.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Is that so?[P] Well, show me what you're made of!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Blackout.
	fnCutsceneWait(25)
	fnCutscene([[ AL_SetProperty("Activate Fade", 25, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()

	--Teleport.
	fnCutsceneTeleport("Christine", 36.25, 27.50)
	fnCutsceneFace("Christine", 1, 0)
	fnCutsceneTeleport("JX-101", 38.25, 27.50)
	fnCutsceneFace("JX-101", -1, 0)
	fnCutsceneTeleport("ObserverA", 46.25, 33.50)
	fnCutsceneFace("ObserverA", 1, 0)
	fnCutsceneTeleport("ObserverB", 40.25, 19.50)
	fnCutsceneTeleport("ObserverC", 41.25, 19.50)
	fnCutsceneBlocker()
	
	--Fade in.
	fnCutsceneWait(125)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()
	
	--Move.
	fnCutsceneMove("ObserverB", 40.25, 27.50)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneFace("ObserverB", -1, 0)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] Look everyone![P] SX-399 and JX-101 are playing Penkak again![P] And SX-399 is winning!") ]])
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneMove("ObserverB", 40.25, 28.50, 1.70)
	fnCutsceneMove("ObserverB", 36.25, 28.50, 1.70)
	fnCutsceneFace("ObserverB", 0, -1)
	fnCutsceneMove("ObserverC", 41.25, 28.50, 1.70)
	fnCutsceneMove("ObserverC", 38.25, 28.50, 1.70)
	fnCutsceneFace("ObserverC", 0, -1)
	fnCutsceneMove("ObserverA", 45.25, 33.50, 1.20)
	fnCutsceneMove("ObserverA", 45.25, 29.50, 1.20)
	fnCutsceneMove("ObserverA", 44.25, 29.50, 1.20)
	fnCutsceneMove("ObserverA", 44.25, 28.50, 1.20)
	fnCutsceneMove("ObserverA", 39.25, 28.50, 1.20)
	fnCutsceneBlocker()
	fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
	fnCutsceneMove("ObserverA", 38.25, 28.50, 0.30)
	fnCutsceneMoveFace("ObserverC", 37.25, 28.50, 0, -1, 0.30)
	fnCutsceneBlocker()
	fnCutsceneFace("ObserverA", 0, -1)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Oh, they say you're winning, sweetie.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] *tap*[P] I think they're right![B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Shame that you've got only one queen to give away...[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Oh, I see.[P] And what if I do, this?[P] *tap*[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] !!![B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Ah ha![P] Check![P] Let's see you get out of this one!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(125)
	fnCutsceneBlocker()

	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] *tap*[P] Gotta sacrifice a pawn sometimes...[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] No, no.[P] The lives of your pawns are not to be thrown away.[P] You must buy something even greater if you sacrifice them.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Think in terms of what you gain for their loss.[P] *tap*[P] Position, pieces, victory, but never nothing.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(125)
	fnCutsceneBlocker()

	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Argh![B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Checkmate.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Fiddlesticks![B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Oh, don't pout, sweetie.[P] You did well, but this game is more about memorization than it truly is about strategy.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Your strategy this time was quite similar to one I played against...[P] a while ago...[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] ...[P] I'm not pouting...[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] That was fun, though![B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] You can be very competitive when you want to be.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] You take after me, even more so this time than usual![P] And -[P] well, I'm sure you'll get me next time...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneFace("JX-101", 0, 1)
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("JX-101:[VOICE|JX-101] Show's over, you louts.[P] Back to work!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Blackout.
	fnCutsceneWait(45)
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()

	--Teleport.
	fnCutsceneTeleport("Christine", 39.25, 27.50)
	fnCutsceneFace("Christine", 0, 1)
	fnCutsceneTeleport("JX-101", 39.25, 27.50)
	fnCutsceneFace("JX-101", 0, 1)
	fnCutsceneTeleport("ObserverA", -100.0, -100.0)
	fnCutsceneTeleport("ObserverB", -100.0, -100.0)
	fnCutsceneTeleport("ObserverC", -100.0, -100.0)
	fnCutsceneBlocker()
	fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
	
	--Fade in.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()

	--Round up.
	LM_ExecuteScript(fnResolvePath() .. "RoundupScript.lua")
	
-- |[Drawing]|
--Artistry!
elseif(sObjectName == "Drawing") then
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSprocketDrawing", "N", 1.0)

	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] You always gravitate back to the art studio.[P] Did you want to practice your drawing, sweetie?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Err...[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Now now, I may have said some harsh things before, but that was just to motivate you.[P] You're getting better.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Criticism can be tough but you have to endure it to improve.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Well, let me show you what I've learned!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Fade out.
	fnCutsceneWait(45)
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(125)
	fnCutsceneBlocker()

	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Your technique has...[P] improved![P] A great deal![P] Have you been practicing without me knowing?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] (!!!)[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] All I do is focus very hard on my hand.[P] To keep it from shaking.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Ah, very good.[P] I know your condition can make it hard to keep a steady hand, but you're barely shaking at all.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] So, who did you draw today?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] This is...[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] This resembles a golem...[P] but with green hair and a blue dress?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] It's not supposed to be a golem, mother![P] Oh bother...[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Oh my, I'm sorry.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] (Sheesh, what is wrong with me?[P] I was so focused on my technique that I forgot what I was drawing!)[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Well, it's a lovely piece.[P] I'll be hanging it in the war room tomorrow.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] But - [P][CLEAR]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] I'm proud of your work no matter what it is, sweetie.[P] So long as you try your best, you're a success in my eyes.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Teleport.
	fnCutsceneTeleport("Christine", 10.25, 10.50)
	fnCutsceneFace("Christine", 0, 1)
	fnCutsceneTeleport("JX-101", 10.25, 10.50)
	fnCutsceneFace("JX-101", 0, 1)
	fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
	
	--Fade in.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()

	--Round up.
	LM_ExecuteScript(fnResolvePath() .. "RoundupScript.lua")
	
-- |[Library]|
--Sorting books with mom.
elseif(sObjectName == "Library") then
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSprocketLibrary", "N", 1.0)

	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] The library...[P] Sheesh, the books are all out of order.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Oh, sweetie, do you remember Bidoof the Camel?[P] Those were always your favourite books when you were little.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Uh, yes, of course![P] Bidoof the Camel![P] Love him![B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Someone really should sort these books properly...[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Why not us?[P] Come on, let's show our support.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] You really want to spend a few hours sorting books?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] I'm taking responsibility for my city, mother.[P] It's the right thing to do.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Fade out.
	fnCutsceneWait(45)
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(125)
	fnCutsceneBlocker()

	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Hmm, here's one of the 'Magical Settlement' novellas.[P] These take me back.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] What are those?[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Oh sweetie, these were written a long time ago.[P] Nobody is sure who the real author is.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] They're about a boy who becomes a magical hero fighting for justice, but every time he makes a mistake, he becomes more like a girl.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] It's quite silly, but they have a special place in my power core.[P] They were the first thing I decided to read all on my own, without anyone telling me to.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] I suppose, in a way, I kind of took on that same role, didn't I?[P] But the reality isn't as glamorous or funny...[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] A single mom, fighting for justice while raising a daughter?[P] Damn the glamour, you're a hero in this 'bots book.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Teleport.
	fnCutsceneTeleport("Christine", 20.25, 18.50)
	fnCutsceneFace("Christine", 0, 1)
	fnCutsceneTeleport("JX-101", 20.25, 18.50)
	fnCutsceneFace("JX-101", 0, 1)
	fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
	
	--Fade in.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()

	--Round up.
	LM_ExecuteScript(fnResolvePath() .. "RoundupScript.lua")
	
-- |[Boilers]|
--Pressure!
elseif(sObjectName == "Boilers") then
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSprocketBoilers", "N", 1.0)

	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] *sigh*[P] Somebody has been shirking pressure duty.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Is it something we can fix ourselves?[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Sweetie, balancing the pressure in the steam pipes isn't a lot of fun.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] You said you'd let me choose the activity, so I'm choosing this one.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Besides, this is how I'm going to contribute.[P] Sprocket City needs every pair of hands![B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] You're really taking that to heart, aren't you?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] (If it buys time, we'll scrub the floors too...)[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Just show me what to do and we'll be done in no time.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Fade out.
	fnCutsceneWait(45)
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(125)
	fnCutsceneBlocker()

	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Okay, let some out of there...[P] and stoke that boiler, too.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Got it.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] You know, we make a good team.[P] Maybe someday I could take you on a mission with me?[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] S-[P]strictly as an observer![B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Of course mother, hee hee!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Teleport.
	fnCutsceneTeleport("Christine", 57.25, 25.50)
	fnCutsceneFace("Christine", 0, -1)
	fnCutsceneTeleport("JX-101", 57.25, 25.50)
	fnCutsceneFace("JX-101", 0, -1)
	fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
	
	--Fade in.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()

	--Round up.
	LM_ExecuteScript(fnResolvePath() .. "RoundupScript.lua")
	
-- |[Gardening]|
--Plants!
elseif(sObjectName == "Gardening") then
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSprocketGardening", "N", 1.0)

	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "JX-101", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
	fnCutscene([[ Append("Steam Droid:[E|Neutral] Oh, hello there.[P] Is this a special occasion, JX-101?[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] We're spending some time together, mother and daughter.[P] And you know how SX-399 loves tending to the plants.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] I do?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] I mean, of course I do!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Fade out.
	fnCutsceneWait(45)
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(125)
	fnCutsceneBlocker()

	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] SX-399, your methods are getting rather unorthodox.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] How so?[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] One does not normally tend to plants with a screwdriver.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Well, they're robots just like us.[P] We should use the same techniques, shouldn't we?[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Yes, but you can just let their brass roots soak up the lubricant.[P] You don't need to open the panels and pour it in.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] I'm just being...[P] efficient!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Teleport.
	fnCutsceneTeleport("Christine", 41.25, 25.50)
	fnCutsceneFace("Christine", 0, 1)
	fnCutsceneTeleport("JX-101", 41.25, 25.50)
	fnCutsceneFace("JX-101", 0, 1)
	fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
	
	--Fade in.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()

	--Round up.
	LM_ExecuteScript(fnResolvePath() .. "RoundupScript.lua")
	
-- |[Computers]|
--Fix that piece of junk up!
elseif(sObjectName == "Computers") then
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSprocketComputers", "N", 1.0)

	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Great, I see the backup terminal is broken.[P] Again.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Oh, I'm sure we can fix it if we put our heads together.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] And then we can look at pictures on it![P] Grab a screwdriver!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Fade out.
	fnCutsceneWait(45)
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(125)
	fnCutsceneBlocker()

	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Pass me that bolt-wrench, will you?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Here you go.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] *clank*[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Got it![B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Hey, it booted up![B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] (I bet I can distract JX-101 with pictures and videos on this thing...)") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Teleport.
	fnCutsceneTeleport("Christine", 61.75, 19.50)
	fnCutsceneFace("Christine", 0, -1)
	fnCutsceneTeleport("JX-101", 61.25, 19.50)
	fnCutsceneFace("JX-101", 0, -1)
	fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
	
	--Fade in.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()

	--Round up.
	LM_ExecuteScript(fnResolvePath() .. "RoundupScript.lua")
	
-- |[Photos]|
--OH MY GOD THEY'RE SO CUTE!
elseif(sObjectName == "Photos") then
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSprocketPhotos", "N", 1.0)

	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Mother, look![P] The terminal has photos of Darkmatters playing with kittens!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Fade out.
	fnCutsceneWait(45)
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(125)
	fnCutsceneBlocker()

	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] SX-399, where do you think these photos came from?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] I'm not sure.[P] It looks like a grassy, forested habitat.[P] Maybe Pandemonium?[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Or someone illicitly downloaded them from Regulus City's network.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Oh, maybe.[P] I suppose that'd mean the golems took them, then?[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] ...[P] Those killers would just as soon recycle the kittens.[P] It must have been taken on Pandemonium by a Steam Droid mage.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] That is...[P] quite a theory, mother.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Teleport.
	fnCutsceneTeleport("Christine", 61.75, 19.50)
	fnCutsceneFace("Christine", 0, -1)
	fnCutsceneTeleport("JX-101", 61.25, 19.50)
	fnCutsceneFace("JX-101", 0, -1)
	fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
	
	--Fade in.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()

	--Round up.
	LM_ExecuteScript(fnResolvePath() .. "RoundupScript.lua")
	
-- |[Hats]|
--TF2 JOKE!
elseif(sObjectName == "Hats") then
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSprocketHats", "N", 1.0)

	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "JX-101", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
	fnCutscene([[ Append("Steam Droid:[E|Neutral] JX-101, looking to do some shopping?[P] I got some kit from Gearsville...[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] This isn't an official visit.[P] What do you have in the more...[P] fashionable...[P] pursuits?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Oh my gosh, mother![P] Cute hats![P] Ribbons![P] Gloves![B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] ...[P] Which I have always enjoyed, and if this is the first time you are finding that out, it is not suspicious.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Of course, sweetie.[P] Let's try a few things on.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Fade out.
	fnCutsceneWait(45)
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(125)
	fnCutsceneBlocker()

	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutscene([[ Append("JX-101:[E|HatA] What do you think of this one?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] I don't think the color works.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Hmm...[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|HatB] This one, then?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] It's so you![B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Feh, but it's out of my price range.[P] Still, next time...[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] ...[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|HatC] All right, one more.[P] For the road.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Oh mother, you look great with or without the hat.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Flattery will get you everywhere, sweetie.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()

	--Teleport.
	fnCutsceneTeleport("Christine", 15.25, 11.50)
	fnCutsceneFace("Christine", 0, 1)
	fnCutsceneTeleport("JX-101", 15.25, 11.50)
	fnCutsceneFace("JX-101", 0, 1)
	fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
	
	--Fade in.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()

	--Round up.
	LM_ExecuteScript(fnResolvePath() .. "RoundupScript.lua")
	
-- |[Finale]|
--The exciting conclusion.
elseif(sObjectName == "Finale") then
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N", 3.0)

	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Well, I suppose this is it.[P] In you go.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] I had a lot of fun today, mother.[P] I'm glad we spent this time together.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] As am I, my sweet cherry.[P] I -[P] I - [B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] What is it?[P] Do you have something important to tell me?[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] [P].[P].[P].[P] No.[P] Just -[P] goodnight.[P] I'll see you in a year.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Goodnight.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneFace("Christine", 1, 0)
	fnCutsceneMove("JX-101", 70.25, 12.50)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Stop music.
	fnCutscene([[ AL_SetProperty("Music", "Null") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("JX-101:[VOICE|JX-101] Wait.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneFace("JX-101", -1, 0)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Your charge pump.[P] Where is it?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Well, I...[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] The special pump on the back of your neck![P] Why isn't it there?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] ...[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] You -[P] you are not SX-399![P] Are you?[P] I thought something was off, but I brushed the thought away![B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] You are not my daughter![P] Who are you?[P] What have you done with her?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Wait, no![P] I can explain![B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Enough of your lies!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Fade out.
	fnCutsceneWait(15)
	fnCutscene([[ AL_SetProperty("Activate Fade", 15, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(125)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Some time earlier...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(125)
	fnCutsceneBlocker()
	
	--Map transition.
	fnCutscene([[ AL_BeginTransitionTo("RegulusCityC", "FORCEPOS:19.0x13.0x0") ]])
end
