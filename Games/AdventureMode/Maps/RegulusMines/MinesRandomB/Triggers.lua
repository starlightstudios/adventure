-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Start the mines cutscene.
if(sObjectName == "StartCutscene") then

    --Variables.
    local iMineBTalkedToDroids = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineBTalkedToDroids", "N")
    if(iMineBTalkedToDroids == 1.0) then return end
    
    --Other variables.
    local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    local iHasSteamDroidForm = VM_GetVar("Root/Variables/Global/Christine/iHasSteamDroidForm", "N")
    local iSXUpgradeQuest    = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMineBTalkedToDroids", "N", 1.0)

    --Movement.
    fnCutsceneMove("Christine", 15.25, 15.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneMove("Tiffany", 14.25, 15.50)
    fnCutsceneFace("Tiffany", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Droids turn to face you.
    fnCutsceneFace("DroidLeader", -1, 0)
    fnCutsceneFace("DroidA", -1, 0)
    fnCutsceneFace("DroidB", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
    
    
    if(sChristineForm == "SteamDroid" or iSXUpgradeQuest >= 3.0) then
        fnCutscene([[ Append("Steam Droid:[E|Neutral] What the heck -[P] oh![P] Christine![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Scared] You know my name?[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Your reputation precedes you.[P] Fist of the Future, for life![B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] And you must be 55, right?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Phew![P] I was worried for a second.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] It is uncommon to encounter allies in the mines.[B][C]") ]])
        fnCutscene([[ Append("IN-12:[E|Neutral] Name's IN-12, and this is my squad.[P] You're pretty well known down here.[P] Care to lend a hand?[B][C]") ]])
        fnCutscene([[ Append("IN-12:[E|Neutral] Just take a look over that chasm.") ]])
        fnCutsceneBlocker()
    
    elseif(sChristineForm == "Human") then
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Oh goody, more creeps.[P] Will this day never end?[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] And it's a human and one of those doll girls?[P] Good.[P] I love it![P] Throw more at me![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Is this a bad time?[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Okay, wasn't expecting that.[P] Identify yourselves.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] My name's - [P][CLEAR]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Shut up, X.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I'm Y, and this is X.[P] You don't need to know our real names.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] ('How to Win Friends and Influence Robots' -[P] maybe I should get 55 a copy to read...)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I guess that will have to do.[P] We're here to help.[P] Are you in trouble?[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Yeah, you could say that.[P] Since you haven't opened fire yet, I assume you're on the level?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Quite.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] And your Command Unit friend?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] A maverick.[P] Enemy of Regulus City.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Yeah, sure, I trust you as far as I can throw you.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Considering your motivators versus my weight, and the fact that these chasms could guarantee a long flight time, I estimate you could throw me - [P][CLEAR]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] It's an expression, 'Y'.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] How can we help you?[B][C]") ]])
        fnCutscene([[ Append("IN-12:[E|Neutral] Name's IN-12, and this is my squad.[P] Take a look over there if you please.") ]])
        fnCutsceneBlocker()
    
    elseif(sChristineForm == "Golem") then
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Oh goody, more creeps.[P] Will this day never end?[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Though one of them is a command unit.[P] Don't usually see them amongst the crowds.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Wait![P] We're friends![P] Not enemies![P] Don't shoot![B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Well well well, they can talk coherently.[P] Interesting.[P] Identify yourselves.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] My name's - [P][CLEAR]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Shut up, X.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I'm Y, and this is X.[P] You don't need to know our real names.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] ('How to Win Friends and Influence Robots' -[P] maybe I should get 55 a copy to read...)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I guess that will have to do.[P] We're here to help.[P] Are you in trouble?[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Yeah, you could say that.[P] I take it you're not with Regulus City?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] We've gone maverick.[P] Fight the power, sister.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Yeah, sure, I trust you as far as I can throw you.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Considering your motivators versus my weight, and the fact that these chasms could guarantee a long flight time, I estimate you could throw me - [P][CLEAR]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] It's an expression, 'Y'.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] How can we help you?[B][C]") ]])
        fnCutscene([[ Append("IN-12:[E|Neutral] Name's IN-12, and this is my squad.[P] Take a look over there if you please.") ]])
        fnCutsceneBlocker()
    
    elseif(sChristineForm == "Doll") then
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Oh goody, more creeps.[P] Will this day never end?[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Two of them crazy dolls, though.[P] That's rare.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Wait![P] We're friends![P] Not enemies![P] Don't shoot![B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Well well well, they can talk coherently.[P] Interesting.[P] Identify yourselves.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] My name's - [P][CLEAR]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Shut up, X.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I'm Y, and this is X.[P] You don't need to know our real names.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] ('How to Win Friends and Influence Robots' -[P] maybe I should get 55 a copy to read...)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I guess that will have to do.[P] We're here to help.[P] Are you in trouble?[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Yeah, you could say that.[P] I take it you're not with Regulus City?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] We've gone maverick.[P] Fight the power, sister.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Yeah, sure, I trust you as far as I can throw you.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Considering your motivators versus my weight, and the fact that these chasms could guarantee a long flight time, I estimate you could throw me - [P][CLEAR]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] It's an expression, 'Y'.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] How can we help you?[B][C]") ]])
        fnCutscene([[ Append("IN-12:[E|Neutral] Name's IN-12, and this is my squad.[P] Take a look over there if you please.") ]])
        fnCutsceneBlocker()
    
    elseif(sChristineForm == "Darkmatter") then
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Now this is a new one.[P] Don't usually see Darkmatters down here.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Hello![P] Pleased to meet you![B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Talking Darkmatters, no less.[P] What the heck is going on in the mines lately?[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] You, Darkmatter.[P] Identify yourself.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] My name's - [P][CLEAR]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Shut up, X.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I'm Y, and this is X.[P] You don't need to know our real names.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] ('How to Win Friends and Influence Robots' -[P] maybe I should get 55 a copy to read...)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I guess that will have to do.[P] We're here to help.[P] Are you in trouble?[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Yeah, you could say that.[P] Is your Command Unit friend on the level?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] She's a maverick.[P] She's on your side.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Yeah, sure, I trust you as far as I can throw you.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Considering your motivators versus my weight, and the fact that these chasms could guarantee a long flight time, I estimate you could throw me - [P][CLEAR]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] It's an expression, 'Y'.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] How can we help you?[B][C]") ]])
        fnCutscene([[ Append("IN-12:[E|Neutral] Name's IN-12, and this is my squad.[P] Take a look over there if you please.") ]])
        fnCutsceneBlocker()
    
    elseif(sChristineForm == "Eldritch") then
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Oh lovely, one of those 'things'.[P] Maybe we can toss it off the edge when it rushes us.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Scared] No, no![P] We're here to help![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] Uh, ignore my appearance.[P] I'm not like them.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Oh yeah?[P] Prove it.[P] Identify yourself.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] My name's - [P][CLEAR]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Shut up, X.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I'm Y, and this is X.[P] You don't need to know our real names.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] ('How to Win Friends and Influence Robots' -[P] maybe I should get 55 a copy to read...)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I guess that will have to do.[P] We're here to help.[P] Are you in trouble?[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Why should I tell you anything?[P] Command Units and -[P] whatever you are -[P] normally attack us on sight.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] She's a maverick, and I'm -[P] awake.[P] We're on your side.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Yeah, sure, I trust you as far as I can throw you.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Considering your motivators versus my weight, and the fact that these chasms could guarantee a long flight time, I estimate you could throw me - [P][CLEAR]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] It's an expression, 'Y'.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] How can we help you?[B][C]") ]])
        fnCutscene([[ Append("IN-12:[E|Neutral] Name's IN-12, and this is my squad.[P] Take a look over there if you please.") ]])
        fnCutsceneBlocker()
    
    elseif(sChristineForm == "Electrosprite") then
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Uhh, one of those electricity girls I've been hearing about...[P] Are the rumours true?[P] Do you suck power out of innocent robots?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Scared] No, no![P] We're here to help![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] And you can call us Electrosprites if you want.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Whatever.[P] What's your name?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Call me - [P][CLEAR]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Shut up, X.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I'm Y, and this is X.[P] You don't need to know our real names.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] ('How to Win Friends and Influence Robots' -[P] maybe I should get 55 a copy to read...)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I guess that will have to do.[P] We're here to help.[P] Are you in trouble?[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Why should I tell you anything?[P] Command Units shoot Steam Droids on sight.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] She's a maverick.[P] We're on your side.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Yeah, sure, I trust you as far as I can throw you.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Considering your motivators versus my weight, and the fact that these chasms could guarantee a long flight time, I estimate you could throw me - [P][CLEAR]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] It's an expression, 'Y'.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] How can we help you?[B][C]") ]])
        fnCutscene([[ Append("IN-12:[E|Neutral] Name's IN-12, and this is my squad.[P] Take a look over there if you please.") ]])
        fnCutsceneBlocker()
    
    elseif(sChristineForm == "Raiju") then
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Back up, raiju.[P] Nice and easy.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Scared] Hey, put the gun down, we're here to help![B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] You can talk?[P] Those things haven't gotten to you?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I'm Y, and this is X.[P] You don't need to know our real names.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] We are unaffected by the ramblings of the others in this mine.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Yeah, we're here to help.[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] We'll take all the help we can get, but no sudden moves, command unit.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Please, I prefer Y.[B][C]") ]])
        fnCutscene([[ Append("IN-12:[E|Neutral] Okay 'Y', my name's IN-12, and this is my squad.[P] Take a look over there if you please.") ]])
        fnCutsceneBlocker()
    
    elseif(sChristineForm == "Secrebot") then
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Oh joy.[P] At least this secrebot isn't full of goop like the other ones.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Scared] Hey, put the gun down, we're here to help![B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Sure you are.[P] Keep your distance.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I'm Y, and this is X.[P] You don't need to know our real names.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] We are unaffected by the ramblings of the others in this mine.[P] This secrebot is not like the others.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Yeah, we're here to help.[P] We're mavericks![B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] We'll take all the help we can get, but no sudden moves, command unit.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Please, I prefer Y.[B][C]") ]])
        fnCutscene([[ Append("IN-12:[E|Neutral] Okay 'Y', my name's IN-12, and this is my squad.[P] Take a look over there if you please.") ]])
        fnCutsceneBlocker()
    
    elseif(sChristineForm == "LatexDrone") then
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Command Unit behind us![P] We're being flanked![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Scared] Wait![P] Don't shoot![B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] ...[P] The bondage drone talks first?[P] What the hey?[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] You, bondage drone.[P] Identify yourself.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Call me - [P][CLEAR]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Shut up, X.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I'm Y, and this is X.[P] You don't need to know our real names.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] ('How to Win Friends and Influence Robots' -[P] maybe I should get 55 a copy to read...)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I guess that will have to do.[P] We're here to help.[P] Are you in trouble?[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Why should I tell you anything?[P] You're with Regulus City aren't you?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] No, we're mavericks! We're on your side![B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] Yeah, sure, I trust you as far as I can throw you.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Considering your motivators versus my weight, and the fact that these chasms could guarantee a long flight time, I estimate you could throw me - [P][CLEAR]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] It's an expression, 'Y'.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] How can we help you?[B][C]") ]])
        fnCutscene([[ Append("IN-12:[E|Neutral] Name's IN-12, and this is my squad.[P] Take a look over there if you please.") ]])
        fnCutsceneBlocker()
    end
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Camera moves.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0)
        CameraEvent_SetProperty("Focus Position", (16.25 * gciSizePerTile), (33.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0)
        CameraEvent_SetProperty("Focus Actor Name", "Christine")
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
    fnCutscene([[ Append("IN-12:[E|Neutral] Look at all those wonderful crates.[P] And in those wonderful crates are doubly-wonderful supplies we're here to retrieve.[B][C]") ]])
    fnCutscene([[ Append("IN-12:[E|Neutral] Problem is there's a bunch of whack jobs between us and those crates.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] The golems over there -[P] are they okay?[B][C]") ]])
    fnCutscene([[ Append("IN-12:[E|Neutral] Beats me.[P] They gibber to themselves about something or other.[P] They rush you if you try to get close.[B][C]") ]])
    fnCutscene([[ Append("IN-12:[E|Neutral] Yelling stuff about how 'big it is' and 'we need to hide!' and whatnot.[P] They've clearly lost it.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smirk] You have guns.[P] Shoot a path to the supplies.[P] You do not need our help.[B][C]") ]])
    if(sChristineForm == "SteamDroid" or iSXUpgradeQuest >= 3.0) then
        fnCutscene([[ Append("IN-12:[E|Neutral] Would if I could, but can't so I won't.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Why not?[B][C]") ]])
        fnCutscene([[ Append("IN-12:[E|Neutral] We spent too many mags on the way here, and now we're almost out. We don't have enough to clear a path.[B][C]") ]])
        fnCutscene([[ Append("IN-12:[E|Neutral] Heck, with what we have we'd have a rough time just getting back to the nearest settlement.[B][C]") ]])
    else
        fnCutscene([[ Append("IN-12:[E|Neutral] You, 'Y', would fit in very well with my crew.[P] Physical appearance notwithstanding.[B][C]") ]])
        fnCutscene([[ Append("IN-12:[E|Neutral] And you are correct.[P] We do have guns.[P] But we can't shoot a path.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Why not?[B][C]") ]])
        fnCutscene([[ Append("IN-12:[E|Neutral] See, there's this thing that guns need.[P] It's called 'ammunition'.[P] Guns don't work without it.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] No need to be sarcastic.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Actually, some lasrifle designs do not need ammunition.[P] It is quite possible to be unfamiliar with the term.[B][C]") ]])
        fnCutscene([[ Append("IN-12:[E|Neutral] Well, I [P]*was*[P] being sarcastic, but 'Y' is right.[B][C]") ]])
        fnCutscene([[ Append("IN-12:[E|Neutral] Anyway, the ammunition we need?[P] It's in those crates.[P] We spent too much on our way here.[B][C]") ]])
    end
    
    --Common.
    fnCutscene([[ Append("IN-12:[E|Neutral] If we could get to those crates we could load up and head back to headquarters.[P] But we can't, so we're stuck here staring at them.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Okay![P] So, we'll go get the ammo from the crates for you![B][C]") ]])
    
    if(sChristineForm == "SteamDroid" or iSXUpgradeQuest >= 3.0) then
        fnCutscene([[ Append("IN-12:[E|Neutral] I expected no less![P] You're every bit the hero JX-101 said you were.[B][C]") ]])
        fnCutscene([[ Append("IN-12:[E|Neutral] Listen, if you can get the ammo for us, I'm sure she'll have something for you back at headquarters.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] We're on it.[P] Sit tight.[B][C]") ]])
        fnCutscene([[ Append("IN-12:[E|Neutral] Not like we were doing much otherwise.") ]])
    else
        fnCutscene([[ Append("IN-12:[E|Neutral] Huh.[P] Well, if you've survived in the mines this far then you've got to be tough.[B][C]") ]])
        fnCutscene([[ Append("IN-12:[E|Neutral] If you die, don't expect us to retrieve your corpses.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Very well.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] We'll be back before you know it.[P] You just sit tight.[B][C]") ]])
        fnCutscene([[ Append("IN-12:[E|Neutral] Not like we were doing much otherwise.[P] Get this done and I might just have a reward for you.") ]])
    end
    fnCutsceneBlocker()

    --Droids change facing.
    fnCutsceneFace("DroidLeader", 1, 0)
    fnCutsceneFace("DroidA", 1, 0)
    fnCutsceneFace("DroidB", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneMove("Tiffany", 15.25, 15.50)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Fold Party") ]])
    fnCutsceneBlocker()

end
