-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Subscript]|
--Run this script. If the variable gbCaughtScript is thrown to true, stop there.
gbCaughtScript = false
LM_ExecuteScript(gsRandomExaminationHandler, sObjectName)
if(gbCaughtScript == true) then
    gbCaughtScript = nil
    return
end

--Clear.
gbCaughtScript = nil

-- |[Examinables]|
if(sObjectName == "Boxes") then

    --Variables.
    local iMineBGotAmmo = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineBGotAmmo", "N")
    local iMineBLeft    = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineBLeft", "N")

    --Get the ammo.
    if(iMineBGotAmmo == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMineBGotAmmo", "N", 1.0)
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Supplies, spare parts, wiring, tools...)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ([SOUND|World|TakeItem]Lead slugs, frag explosives, casings, and propellant![P] This should do the job.[P] Better get it back to IN-12.)") ]])
        fnCutsceneBlocker()
        
    --Hasn't left the mines yet.
    elseif(iMineBLeft == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I've already gotten the ammo.[P] I should get it back to the Steam Droids.)") ]])
        fnCutsceneBlocker()

    --Has left the mines.
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The Steam Droids have already taken everything useful in here.[P] There's some organic rations that have long-since spoiled...)") ]])
        fnCutsceneBlocker()

    end

-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
