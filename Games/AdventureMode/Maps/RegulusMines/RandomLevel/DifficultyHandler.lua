-- |[ =================================== Difficulty Handler =================================== ]|
--This is the subscript for the random level generator that handles level difficulty. It also handles
-- spawning of enemies and treasure.

-- |[Constants]|
local ciParagonChance = 2

-- |[ ======================================== Variables ======================================= ]|
--We need to know the entry point because enemies within 6 tiles of it don't spawn. This is just a QoL feature.
local iEntranceX, iEntranceY = AdlevGenerator_GetProperty("Entrance Position")

--We also need to know what floor we're on. This determines enemy density and treasure quality.
-- Note that this describes the maximum number of patrols, not enemies. Enemies who are following one
-- another count as the same enemy for our purposes.
local iMaxEnemies = 0
local iMaxTreasures = 0
local iEnemyDoubleThreshold = 0
local iEnemyTripleThreshold = 0
local iMinesFloor = VM_GetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "I")

-- |[ ====================================== Enemy Density ===================================== ]|
-- |[Enemy Density]|
if(iMinesFloor < 5) then
	iMaxEnemies = 3
	iMaxTreasures = 2
    iEnemyDoubleThreshold = 90
    iEnemyTripleThreshold = 101
elseif(iMinesFloor < 10) then
	iMaxEnemies = 3
	iMaxTreasures = 2
    iEnemyDoubleThreshold = 70
    iEnemyTripleThreshold = 95
elseif(iMinesFloor < 15) then
	iMaxEnemies = 4
	iMaxTreasures = 3
    iEnemyDoubleThreshold = 70
    iEnemyTripleThreshold = 90
elseif(iMinesFloor < 20) then
	iMaxEnemies = 5
	iMaxTreasures = 3
    iEnemyDoubleThreshold = 50
    iEnemyTripleThreshold = 80
elseif(iMinesFloor < 25) then
	iMaxEnemies = 6
	iMaxTreasures = 4
    iEnemyDoubleThreshold = 50
    iEnemyTripleThreshold = 80
elseif(iMinesFloor < 30) then
	--iMaxEnemies = 7
	iMaxTreasures = 4
    --iEnemyDoubleThreshold = 30
    --iEnemyTripleThreshold = 75
    
	iMaxEnemies = 6
    iEnemyDoubleThreshold = 50
    iEnemyTripleThreshold = 80
elseif(iMinesFloor < 35) then
	--iMaxEnemies = 7
	iMaxTreasures = 5
    --iEnemyDoubleThreshold = 27
    --iEnemyTripleThreshold = 75
    
	iMaxEnemies = 6
    iEnemyDoubleThreshold = 50
    iEnemyTripleThreshold = 80
elseif(iMinesFloor < 40) then
	--iMaxEnemies = 8
	iMaxTreasures = 5
    --iEnemyDoubleThreshold = 27
    --iEnemyTripleThreshold = 75
    
	iMaxEnemies = 6
    iEnemyDoubleThreshold = 50
    iEnemyTripleThreshold = 80
elseif(iMinesFloor < 45) then
	--iMaxEnemies = 9
	iMaxTreasures = 6
    --iEnemyDoubleThreshold = 27
    --iEnemyTripleThreshold = 75
    
	iMaxEnemies = 6
    iEnemyDoubleThreshold = 50
    iEnemyTripleThreshold = 80
else
	--iMaxEnemies = 1000
	iMaxTreasures = 1000
    --iEnemyDoubleThreshold = 30
    --iEnemyTripleThreshold = 75
    
	iMaxEnemies = 6
    iEnemyDoubleThreshold = 50
    iEnemyTripleThreshold = 80

end

-- |[ ======================================= Enemy Paths ====================================== ]|
--Get how many paths there are. Store that data in a table for the enemies to read.
local iaPathData = {}
local iPathsTotal = AdlevGenerator_GetProperty("Enemy Paths Total")
for i = 0, iPathsTotal - 1, 1 do
	
	--Store.
	iaPathData[i] = {}
	iaPathData[i].iPathLength = AdlevGenerator_GetProperty("Enemy Path Length", i)
	iaPathData[i].iPathData = {}
	
	--Iterate across the path.
	for p = 0, iaPathData[i].iPathLength - 1, 1 do
		
		--Generate a path name.
		local sName = string.format("N%02i%02i", i, p)
		
		--Get position and place the node.
		local iPathX, iPathY = AdlevGenerator_GetProperty("Enemy Path Position", i, p)
		AL_CreateObject("Path Node", sName, iPathX * gciSizePerTile, iPathY * gciSizePerTile, gciSizePerTile, gciSizePerTile)
		
		--Store it.
		iaPathData[i].iPathData[p] = {iPathX, iPathY}
	end
end

-- |[ ================================== Visual Associations =================================== ]|
--Lookup table that associates all enemies with a sprite set.
local saAppearanceLookups = {}
local function fnAddAppearance(psEnemyName, psSpriteName, piToughness)
    local i = #saAppearanceLookups + 1
    saAppearanceLookups[i] = {}
    saAppearanceLookups[i].sEnemyName = psEnemyName
    saAppearanceLookups[i].sSpriteName = psSpriteName
    saAppearanceLookups[i].iToughness = piToughness
end

--Returns the index of the appearance, or -1 if not found.
local function fnResolveAppearance(psEnemyName)
    for i = 1, #saAppearanceLookups, 1 do
        if(saAppearanceLookups[i].sEnemyName == psEnemyName) then
            return i
        end
    end
    
    --Not found.
    return -1
end

--Create.
fnAddAppearance("Scraprat Scrounger", "Scraprat",         0)--
fnAddAppearance("Scraprat Forager",   "Scraprat",         1)--
fnAddAppearance("Scraprat Bomber",    "Scraprat",         2)--
fnAddAppearance("Wrecked Bot",        "SecurityBotBroke", 0)--
fnAddAppearance("Subverted Bot",      "SecurityBotBroke", 1)--
fnAddAppearance("Compromised Bot",    "SecurityBotBroke", 2)--
fnAddAppearance("Mr. Wipey",          "MrWipey",          0)--
fnAddAppearance("Mr. Wipesalot",      "MrWipey",          1)--
fnAddAppearance("Der Viper",          "MrWipey",          2)--
fnAddAppearance("Secrebot",           "Secrebot",         0)--
fnAddAppearance("Attendebot",         "Secrebot",         1)--
fnAddAppearance("Plannerbot",         "Plannerbot",       2)--
fnAddAppearance("Motilvac",           "Motilvac",         0)--
fnAddAppearance("Autovac",            "Motilvac",         1)--
fnAddAppearance("Intellivac",         "Motilvac",         2)--
fnAddAppearance("Darkmatter",         "DarkmatterGirl",   0)--
fnAddAppearance("Starseer",           "DarkmatterGirl",   1)--
fnAddAppearance("Nebula Sight",       "DarkmatterGirl",   2)--
fnAddAppearance("Waylighter",         "Horrible",         0)--
fnAddAppearance("Lightsman",          "Horrible",         1)--
fnAddAppearance("Blind Seer",         "Horrible",         2)--
fnAddAppearance("Void Rift",          "VoidRift",         0)--
fnAddAppearance("Spatial Tear",       "VoidRift",         1)--
fnAddAppearance("Billowing Maw",      "VoidRift",         2)--
fnAddAppearance("Latex Drone",        "LatexDrone",       0)--
fnAddAppearance("Latex Packmaster",   "LatexDrone",       1)--
fnAddAppearance("Latex Security",     "LatexDrone",       2)--
fnAddAppearance("Mad Worker",         "GolemSlaveP",      0)--
fnAddAppearance("Insane Golem",       "GolemSlaveP",      1)--
fnAddAppearance("Elite Security",     "GolemSlaveP",      2)--
fnAddAppearance("Mad Lord",           "GolemLordA",       0)--
fnAddAppearance("Insane Lord",        "GolemLordA",       1)--
fnAddAppearance("Elite Lord",         "GolemLordA",       2)--
fnAddAppearance("Mad Doll",           "Doll",             0)--
fnAddAppearance("Insane Doll",        "Doll",             1)--
fnAddAppearance("Security Chief",     "Doll",             2)--
fnAddAppearance("Geisha",             "InnGeisha",        0)--
fnAddAppearance("Mummy Imp",          "BandageImp",       0)--
fnAddAppearance("Hoodie",             "Hoodie",           0)--
fnAddAppearance("Raibie",             "Raibie",           0)--
fnAddAppearance("Dreamer",            "EldritchDream",    0)--

-- |[ ======================================= Floor Theme ====================================== ]|
--Enemy type and loot are determined by which floor we're on. The difficulty starts at Cryogenics
-- and proceeds all the way to early Biolabs.
--The floor number is scattered by 3 in either direction.
local iCurrentMinesFloor = VM_GetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "N") + LM_GetRandomNumber(-3, 3)

--Clamp.
if(iCurrentMinesFloor <  0) then iCurrentMinesFloor =  0 end
if(iCurrentMinesFloor > 50) then iCurrentMinesFloor = 50 end

-- |[Variables]|
local saEnemyList = {}
local iTotalRoll = 0

-- |[Functions]|
local function fnAddEnemy(iRollChance, saNamesTable)
    local i = #saEnemyList + 1
    saEnemyList[i] = {iRollChance, saNamesTable}
    iTotalRoll = iTotalRoll + iRollChance
end

-- |[ ===================================== Enemy Listings ===================================== ]|
-- |[Cryogenics]|
--Fixed, always the first 3 floors.
if(iCurrentMinesFloor < 3) then
    fnAddEnemy(100, {"Scraprat Scrounger"})
    fnAddEnemy(100, {"Scraprat Scrounger", "Scraprat Scrounger"})
    fnAddEnemy(100, {"Wrecked Bot"})
    fnAddEnemy(100, {"Wrecked Bot", "Wrecked Bot"})
    fnAddEnemy(100, {"Wrecked Bot", "Scraprat Scrounger"})

-- |[Regulus Surface]|
--Fixed, always floors 3 to 8.
elseif(iCurrentMinesFloor < 8) then
    fnAddEnemy(100, {"Mr. Wipey"})
    fnAddEnemy(100, {"Secrebot"})
    fnAddEnemy(100, {"Scraprat Forager", "Scraprat Forager", "Scraprat Forager"})
    fnAddEnemy(100, {"Motilvac", "Subverted Bot", "Subverted Bot"})
    fnAddEnemy(100, {"Secrebot", "Mr. Wipey"})
    fnAddEnemy(100, {"Secrebot", "Scraprat Forager", "Subverted Bot"})

-- |[Serenity Crater, Southern Cryogenics, Manufactory]|
--Any of these three can be rolled.
elseif(iCurrentMinesFloor < 15) then

    --Roll.
    local iTypeRoll = LM_GetRandomNumber(1, 100)

    --Serenity Crater
    if(iTypeRoll < 30) then
        fnAddEnemy( 50, {"Void Rift"})
        fnAddEnemy( 50, {"Void Rift", "Void Rift"})
        fnAddEnemy(150, {"Waylighter", "Void Rift"})
        fnAddEnemy( 50, {"Waylighter", "Void Rift", "Void Rift"})
        fnAddEnemy( 60, {"Waylighter", "Mad Worker"})
        fnAddEnemy( 60, {"Waylighter", "Mad Lord"})
        fnAddEnemy( 60, {"Waylighter", "Mad Doll"})
    
    --Serenity Crater, Darkmatters
    elseif(iTypeRoll < 40) then
        fnAddEnemy(100, {"Darkmatter"})
        fnAddEnemy( 50, {"Darkmatter", "Darkmatter"})

    --Southern Cryogenics
    elseif(iTypeRoll < 70) then
        fnAddEnemy(150, {"Autovac", "Subverted Bot", "Subverted Bot"})
        fnAddEnemy(100, {"Scraprat Forager", "Scraprat Forager", "Scraprat Forager"})
        fnAddEnemy(100, {"Mr. Wipesalot", "Attendebot"})
        fnAddEnemy( 50, {"Void Rift", "Void Rift"})
    
    --Manufactory-grade.
    else
        fnAddEnemy( 50, {"Mad Worker"})
        fnAddEnemy( 50, {"Mad Lord"})
        fnAddEnemy( 50, {"Mad Doll"})
        fnAddEnemy(100, {"Waylighter", "Mad Worker"})
        fnAddEnemy(100, {"Waylighter", "Mad Lord"})
        fnAddEnemy(100, {"Waylighter", "Mad Doll"})
    end

-- |[LRT West]|
--Fixed set.
elseif(iCurrentMinesFloor < 20) then
    fnAddEnemy(100, {"Latex Drone"})
    fnAddEnemy(100, {"Latex Drone", "Latex Drone"})
    fnAddEnemy(100, {"Latex Packmaster", "Scraprat Forager", "Scraprat Forager", "Scraprat Forager"})
    fnAddEnemy(100, {"Waylighter", "Mad Worker"})
    fnAddEnemy(100, {"Waylighter", "Latex Drone"})

-- |[Gala]|
--Fixed Set, robots.
elseif(iCurrentMinesFloor < 27) then
    fnAddEnemy(100, {"Scraprat Bomber"})
    fnAddEnemy(100, {"Compromised Bot"})
    fnAddEnemy(100, {"Intellivac"})
    fnAddEnemy(100, {"Der Viper"})
    fnAddEnemy(100, {"Plannerbot"})

-- |[Gala, Eldritch / Darkmatters]|
--Fixed set, the Vivify enemies and the Darkmatters opposing them.
elseif(iCurrentMinesFloor < 35) then

    --Roll.
    local iTypeRoll = LM_GetRandomNumber(1, 100)
    
    --Serenity Crater, Darkmatters
    if(iTypeRoll < 30) then
        fnAddEnemy(100, {"Starseer"})
        fnAddEnemy( 50, {"Starseer", "Starseer"})

    --Vivify's minions.
    else
        fnAddEnemy(100, {"Lightsman", "Spatial Tear"})
        fnAddEnemy(100, {"Lightsman", "Spatial Tear", "Spatial Tear"})
    end

-- |[Insane Regulans]|
--Robots but the mad variety. Small chance of Darkmatters.
elseif(iCurrentMinesFloor < 40) then

    --10% chance it's Nebula Sights.
    local iTypeRoll = LM_GetRandomNumber(1, 100)
    if(iTypeRoll < 10) then
        fnAddEnemy(100, {"Nebula Sight"})
        fnAddEnemy( 20, {"Nebula Sight", "Nebula Sight"})
        
    --Otherwise, robots.
    else
        fnAddEnemy(100, {"Insane Golem"})
        fnAddEnemy(100, {"Insane Lord"})
        fnAddEnemy(100, {"Insane Doll"})
    end

-- |[Biolabs]|
--Eldritch critters local to the biolabs.
elseif(iCurrentMinesFloor < 48) then

    --10% chance it's Raibies.
    local iTypeRoll = LM_GetRandomNumber(1, 100)
    if(iTypeRoll < 10) then
        fnAddEnemy(100, {"Raibie"})
        fnAddEnemy( 20, {"Raibie", "Raibie"})
    
    --20% chance it's high-ranking Regulan Security.
    elseif(iTypeRoll < 30) then
        fnAddEnemy( 50, {"Latex Security"})
        fnAddEnemy( 50, {"Elite Security"})
        fnAddEnemy( 50, {"Elite Lord"})
        fnAddEnemy(100, {"Security Chief", "Latex Security"})
        fnAddEnemy(100, {"Elite Lord",     "Latex Security"})
        fnAddEnemy( 20, {"Security Chief", "Elite Security", "Elite Security"})
        fnAddEnemy( 20, {"Security Chief", "Latex Security", "Latex Security"})

    --Otherwise, Vivify's minions.
    else
        fnAddEnemy(100, {"Geisha"})
        fnAddEnemy(100, {"Mummy Imp"})
        fnAddEnemy(100, {"Hoodie"})
        fnAddEnemy(100, {"Blind Seer", "Billowing Maw"})
        fnAddEnemy( 20, {"Blind Seer", "Hoodie"})
        fnAddEnemy( 20, {"Blind Seer", "Mummy Imp"})
        fnAddEnemy( 20, {"Blind Seer", "Billowing Maw", "Billowing Maw"})
    end

-- |[Dreamers]|
--Just these. Good luck.
else
    fnAddEnemy(100, {"Dreamer"})
end

-- |[ ====================================== Spawn Enemies ===================================== ]|
--Iterate across all spawn locations.
local iSpawnsTotal = AdlevGenerator_GetProperty("Enemy Spawns Total")
for i = 0, iSpawnsTotal - 1, 1 do
	
    -- |[Error Check]|
	--If we pass the spawn cap, stop.
	if(i >= iMaxEnemies) then break end
	
    -- |[Patrol Path]|
	--Get the starting position.
	local iSpawnX, iSpawnY = AdlevGenerator_GetProperty("Enemy Spawn Position", i)
	
	--Check distance to player start. If it's really close to the player start, flip it and spawn it
	-- at the far end of the patrol path.
	local fXDistSqr = (iEntranceX - iSpawnX) * (iEntranceX - iSpawnX)
	local fYDistSqr = (iEntranceY - iSpawnY) * (iEntranceY - iSpawnY)
	local fDistance = math.sqrt(fXDistSqr + fYDistSqr)
	
	--Normal case:
    local sPatrolPath = ""
	if(fDistance >= 13.0) then
	
		--Iterate across the path. Build the patrol string.
		for p = 0, iaPathData[i].iPathLength - 1, 1 do
			local sNextNode = string.format("N%02i%02i", i, p)
			sPatrolPath = sPatrolPath .. sNextNode .. "|"
		end
		
		--Build it backwards so the enemy paths back.
		for p = iaPathData[i].iPathLength - 1, 0, -1 do
			local sNextNode = string.format("N%02i%02i", i, p)
			sPatrolPath = sPatrolPath .. sNextNode .. "|"
		end
	
	
	--Too close. Run the patrol path backwards and use the last point as the spawn.
	else
		
		--Modify the spawn position.
		iSpawnX = iaPathData[i].iPathData[iaPathData[i].iPathLength - 1][1]
		iSpawnY = iaPathData[i].iPathData[iaPathData[i].iPathLength - 1][2]
		
		--Start at the end of the path.
		for p = iaPathData[i].iPathLength - 1, 0, -1 do
			local sNextNode = string.format("N%02i%02i", i, p)
			sPatrolPath = sPatrolPath .. sNextNode .. "|"
		end
		
		--Now run it forwards again.
		for p = 0, iaPathData[i].iPathLength - 1, 1 do
			local sNextNode = string.format("N%02i%02i", i, p)
			sPatrolPath = sPatrolPath .. sNextNode .. "|"
		end
	
    end

    -- |[Determine Creation by Theme]|
    local sParty = ""
    local sAppearance = ""
    
    --Roll a number, this determines which enemy spawn to use.
    local iSubRoll = LM_GetRandomNumber(0, iTotalRoll)
    
    --Error check:
    if(#saEnemyList < 1) then
        io.write("Warning, enemy list has " .. #saEnemyList .. " entries. Floor: " .. math.floor(iCurrentMinesFloor) .. ".\n")
    end
    
    --The enemy is the first entry in the associated cluster.
    local o = 0
    while(iSubRoll >= 0) do
        o = o + 1
        if(o >= #saEnemyList) then o = #saEnemyList end
        iSubRoll = iSubRoll - saEnemyList[o][1]
    end
    
    --Error check:
    if(o <            1) then o =            1 end
    if(o > #saEnemyList) then o = #saEnemyList end
    
    --Get info from the slot.
    local saEnemyRefList = saEnemyList[o][2]
    
    -- |[Appearance]|
    --Resolve appearance of first enemy in grouping.
    local sFirstEnemyName = saEnemyRefList[1]
    local sFirstEnemyAppearance = "Null"
    local iFirstEnemyToughness = 0
    local iSlot = fnResolveAppearance(sFirstEnemyName)
    if(iSlot ~= -1) then
        sFirstEnemyAppearance = saAppearanceLookups[iSlot].sSpriteName
        iFirstEnemyToughness = saAppearanceLookups[iSlot].iToughness
    else
        io.write("Error, no enemy appearance: " .. sFirstEnemyName .. "\n")
        sFirstEnemyAppearance = "Scraprat"
    end
    
    --Modify to paragon, if the roll is low enough.
    local iParagonRoll = LM_GetRandomNumber(1, 100)
    if(iParagonRoll <= ciParagonChance) then
        sFirstEnemyName = sFirstEnemyName .. " Paragon"
        sFirstEnemyAppearance = sFirstEnemyAppearance .. "Paragon"
    end

    -- |[Spawn]|
    --Creation call.
    AL_SetProperty("Add Enemy Spawn", "ScriptEnemy" .. i .. "A", iSpawnX * gciSizePerTile, iSpawnY * gciSizePerTile, sFirstEnemyName, sFirstEnemyAppearance, "Defeat_BackToSave", iFirstEnemyToughness, sPatrolPath, "Null")
    
    -- |[Additional Enemies]|
    --If there is more than one entry in the spawn cluster, spawn additional enemies following behind.
    local iAString = string.byte("A")
    if(#saEnemyRefList > 1) then
        for p = 2, #saEnemyRefList, 1 do
            
            --Get enemy name.
            local sNewEnemyName = saEnemyRefList[p]
            local sNewEnemyAppearance = "Null"
            local iNewEnemyToughness = 0
            
            --Resolve the letter.
            local sLetter = string.char(iAString + p - 1)
            local sLetterUnder = string.char(iAString + p - 2)
            
            --Resolve appearance, same as above.
            local iSlot = fnResolveAppearance(sNewEnemyName)
            if(iSlot ~= -1) then
                sNewEnemyAppearance = saAppearanceLookups[iSlot].sSpriteName
                iNewEnemyToughness = saAppearanceLookups[iSlot].iToughness
            else
                io.write("Error, no enemy appearance: " .. sNewEnemyName .. "\n")
                sNewEnemyAppearance = "Scraprat"
            end
    
            --Modify to paragon, if the roll is low enough.
            local iParagonRoll = LM_GetRandomNumber(1, 100)
            if(iParagonRoll <= ciParagonChance) then
                sNewEnemyName = sNewEnemyName .. " Paragon"
                sNewEnemyAppearance = sNewEnemyAppearance .. "Paragon"
            end
            
            --Spawn.
            AL_SetProperty("Add Enemy Spawn", "ScriptEnemy" .. i .. sLetter, iSpawnX * gciSizePerTile, iSpawnY * gciSizePerTile, sNewEnemyName, sNewEnemyAppearance, "Defeat_BackToSave", iNewEnemyToughness, "Null", "ScriptEnemy" .. i .. sLetterUnder)
    
    
        end
    end
end

-- |[ ======================================== Treasure ======================================== ]|
--LOOOOOT! This is "bonus objects" which can theoretically be NPCs and such, but is just treasure.

-- |[Loot Lookup Table Functions]|
local saLootList = {}
local iMaxLootRoll = 0
local function fnAddLoot(piChance, psaLootTable)
    local i = #saLootList + 1
    saLootList[i] = {}
    saLootList[i].iChance = piChance
    saLootList[i].saLootTable = psaLootTable
    iMaxLootRoll = iMaxLootRoll + piChance
end

-- |[Gems]|
--Gems. All gem colors share an entry on the drop table.
fnAddLoot( 10, {"Glintsteel Gem", "Phassicsteel Gem", "Arensteel Gem"})
fnAddLoot( 10, {"Yemite Gem", "Romite Gem", "Morite Gem"})
fnAddLoot( 10, {"Ardrion Gem", "Nockrion Gem", "Donion Gem"})
fnAddLoot( 10, {"Rubose Gem", "Piorose Gem", "Iniorose Gem"})
fnAddLoot( 10, {"Blurleen Gem", "Mordreen Gem", "Quorine Gem"})
fnAddLoot( 10, {"Qederphage Gem", "Phonophage Gem", "Thatophage Gem"})

--Damage-type gems are less likely to spawn than stat gems.
fnAddLoot(  1, {"Surfofine Gem", "Blunofine Gem", "Pokemfine Gem"})
fnAddLoot(  1, {"Pirofine Gem", "Arctofine Gem", "Execrine Gem"})
fnAddLoot(  1, {"Photofine Gem", "Corrofine Gem", "Psychofine Gem"})
fnAddLoot(  1, {"Hemofine Gem", "Toxofine Gem", "Entrofine Gem"})

-- |[Adamantite]|
--Adamantite powder and flakes can always spawn. Above floor 20, shards. Above 40, pieces.
fnAddLoot(180, {"Adamantite Powder x1"})
fnAddLoot( 90, {"Adamantite Flakes x1"})
if(iCurrentMinesFloor >= 20) then
    fnAddLoot( 45, {"Adamantite Shard x1"})
end
if(iCurrentMinesFloor >= 40) then
    fnAddLoot( 10, {"Adamantite Piece x1"})
end

-- |[Rep Items]|
--High-ish chances to spawn.
fnAddLoot(300, {"Recycleable Junk", "Assorted Parts", "Bent Tools"})
fnAddLoot(100, {"Credits Chip"})

-- |[Cash, XP, JP]|
--Amount to give scales with floor.
local iEffectiveFloor = iCurrentMinesFloor
if(iEffectiveFloor < 1) then iEffectiveFloor = 1 end
fnAddLoot(150, {"Platina x" .. math.floor(iEffectiveFloor * 15)})
fnAddLoot( 50, {"EXP x"     .. math.floor(iEffectiveFloor * 4)})
fnAddLoot( 50, {"JP x25"})

-- |[Combat Items]|
--These items can spawn on any floor.
fnAddLoot( 20, {"Nanite Injection", "Explication Spike", "Regeneration Mist", "Emergency Medkit"})

--Above floor 10, magnesium grenades can also spawn rarely.
if(iCurrentMinesFloor >= 10) then
    fnAddLoot( 10, {"Magnesium Grenade"})
end

-- |[Equipment]|
--These items can spawn at any depth. 
fnAddLoot( 20, {"Dispersion Cloak", "Adaptive Cloth Vest", "Battle Skirt", "Hyperweave Chemise", "Ceramic Weave Vest", "Neutronium Jacket", "Recoil Dampener"})
fnAddLoot( 20, {"Sphalite Ring", "Decorative Bracer", "Jade Eye Ring", "Enchanted Ring", "Jade Necklace", "Arm Brace", "Alacrity Bracer", "Distribution Frame", "Insulated Boots"})
fnAddLoot( 20, {"Kinetic Capacitor", "Viewfinder Module", "Sure-Grip Gloves", "Spear Foregrip"})
fnAddLoot( 20, {"Wide-Spectrum Scanner", "Pulse Radiation Dampener", "Magrail Harmonization Module", "Smoke Bomb"})
    
--Post-Gala grade equipment:
if(iCurrentMinesFloor >= 40) then
    fnAddLoot( 10, {"Neon Tanktop", "Brass Polymer Chestguard", "Titanweave Vest"})
end

--Weapons are sorted by floor cluster.
if(iCurrentMinesFloor < 17) then
    fnAddLoot( 20, {"Carbonweave Electrospear", "Cillium Cryospear", "Mk IV Pulse Diffractor"})
elseif(iCurrentMinesFloor < 32) then
    fnAddLoot( 20, {"Niobium Thermospear", "Silksteel Electrospear", "Silksteel Electrohalberd", "Mk IV Pulse Rifle", "Mk V Pulse Diffractor"})
elseif(iCurrentMinesFloor < 40) then
    fnAddLoot( 20, {"Yttrium Electrospear", "K-47 Assault Rifle", "Stalker Bullpup Rifle"})
else
    fnAddLoot( 20, {"Lithium Thermospear", "Magnetide Cryospear", "Supersteel Electrohalberd", "Mk VI Pulse Diffractor", "R-77 Pulse Diffractor"})
end


-- |[Chest Placement]|
--Place the chests.
local iTreasuresMax = AdlevGenerator_GetProperty("Treasures Total")
for i = 0, iTreasuresMax - 1, 1 do
	
    -- |[Error Check]|
	--If we pass the spawn cap, stop.
	if(i >= iMaxTreasures) then break end
	
    -- |[Position]|
	--Get the location of the treasure.
	local iSpawnX, iSpawnY = AdlevGenerator_GetProperty("Treasure Position", i)
    
    -- |[Loot Selection]|
    --Roll a cluster.
    local iItemRoll = LM_GetRandomNumber(0, iMaxLootRoll)
    local p = 0
    while(iItemRoll >= 0) do
        p = p + 1
        iItemRoll = iItemRoll - saLootList[p].iChance
    end
	
    --Clamp.
    if(p <           1) then p =           1 end
    if(p > #saLootList) then p = #saLootList end
    
    --Fast-access.
    local saItemList = saLootList[p].saLootTable
    
    -- |[Item Selection]|
    --Select an item.
    local sItemName = saLootList[p][1]
    local iSublistRoll = LM_GetRandomNumber(1, #saItemList)
    
    --Create chest.
	AL_SetProperty("Add Chest", "Chest" .. i, iSpawnX * gciSizePerTile, iSpawnY * gciSizePerTile, true, saItemList[iSublistRoll])
	
end
