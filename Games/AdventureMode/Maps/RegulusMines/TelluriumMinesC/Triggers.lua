-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Start the mines cutscene.
if(sObjectName == "StartCutscene") then

	--Variables.
	local iSawMinesCCutscene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMinesCCutscene", "N")
	if(iSawMinesCCutscene == 1.0) then return end
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesCCutscene", "N", 1.0)
	
	--Movement.
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	fnCutsceneMove("Christine", 24.25, 23.50)
	fnCutsceneFace("Christine", 1, 0)
	fnCutsceneMove("Tiffany", 23.25, 23.50)
	fnCutsceneFace("Tiffany", 1, 0)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("55:[VOICE|Tiffany] Wait.[P] I hear something.") ]])
	fnCutsceneBlocker()
	fnCutsceneFace("Christine", 0, -1)
	fnCutsceneFace("Tiffany", 0, -1)
	fnCutsceneWait(25)
	fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|ButtonClick") ]])
    fnCutscene([[ AL_SetProperty("Deactivate Player Light") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--SX-399 enters the scene.
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 3.0)
		CameraEvent_SetProperty("Focus Actor Name", "SX-399")
	DL_PopActiveObject()
	fnCutsceneMove("SX-399", 18.25, 19.50)
	fnCutsceneBlocker()
	fnCutsceneFace("SX-399", 0, 1)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
	fnCutscene([[ Append("Young Droid:[E|Neutral] Thought I heard someone for a moment there.[P] Best not tarry.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Face the wall.
	fnCutsceneFace("SX-399", 0, -1)
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
	fnCutscene([[ Append("Young Droid:[E|Neutral] Just a second -[P] got it!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 3.0)
		CameraEvent_SetProperty("Focus Actor Name", "Christine")
	DL_PopActiveObject()
	
	--Movement.
	fnCutsceneMove("SX-399", 25.25, 19.50)
	fnCutsceneMove("SX-399", 25.25, 15.50)
	fnCutsceneMove("SX-399", 27.25, 15.50)
	fnCutsceneFace("SX-399", 0, 1)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneTeleport("SX-399", -100.25, -100.50)
	fnCutsceneWait(25)
	fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|ButtonClick") ]])
    fnCutscene([[ AL_SetProperty("Activate Player Light", 100, 100) ]])
	fnCutsceneWait(5)
	fnCutsceneBlocker()
	fnCutsceneFace("Christine", -1, 0)
	fnCutsceneFace("Tiffany", 1, 0)
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] And there's our saboteur.[P] Good.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] She is a Steam Droid.[P] Doubtless there are more nearby.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Offended] She's wrecking our infrastructure![P] I've half a mind to go give her a good scolding![B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] It's a power conduit.[P] It's not a big deal.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Offended] Oh, not to you.[P] But you take out one conduit, and then the electrical load increases on the other conduits, and the chance of a blowout goes up.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Offended] Not to mention the extra load increases corrosion on the contacts when you're past the mandated maximum voltage.[P] They're designed for plus ten, no more.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Calm down.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Angry] Oh I'm very calm![P] Never been calmer![B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Offended] Ugh.[P] Let's follow her.[P] If we catch her in time, maybe I can teach her the ins and outs of power transfer subsystems.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Offended] It's just sloppy![P] She could at least have bypassed the circuit before leaving it![B][C]") ]])
	fnCutscene([[ Append("55:[E|Smug] I am not the correct unit to complain to.[P] I imagine 499323 would be far more interested.[P] Save it for her.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Sophie gets me...[P][EMOTION|Christine|Offended] unlike *some* units...") ]])
	fnCutsceneBlocker()

	--55 moves onto Christine.
	fnCutsceneMove("Tiffany", 24.25, 23.50)
	fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()

-- |[ ===================================== SX-399 in NC+ ====================================== ]|
--IF SX-399 should join the party on NC+, don't let the player descend without going west.
elseif(sObjectName == "Elevator") then

    -- |[Setup]|
    --Activation check.
    local iSXJoinedInNCPlus = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXJoinedInNCPlus", "N")
    if(iSXJoinedInNCPlus == 1.0) then return end

    --Check if this scene should fire. It will only do that if SX-399 is level 2 or above, meaning the player has
    -- had her in their party at least once, since she gains EXP when she joins the party in the biolabs.
    if(AdvCombat_GetProperty("Does Party Member Exist", "SX-399") == false) then return end
    AdvCombat_SetProperty("Push Party Member", "SX-399")
        local iLevel = AdvCombatEntity_GetProperty("Level")
    DL_PopActiveObject()
    io.write("Level is " .. iLevel .. "\n")
    if(iLevel < 1) then return end
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (We really should fix that elevator before we go further down.)") ]])
    fnCutsceneBlocker()
    fnCutsceneMove("Christine", 25.25, 20.50)
    fnAutoFoldParty()
    fnCutsceneBlocker()

--New Chapter Plus!
elseif(sObjectName == "NCPlus") then

    --Activation check.
    local iSXJoinedInNCPlus = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXJoinedInNCPlus", "N")
    if(iSXJoinedInNCPlus == 1.0) then return end

    --Check if this scene should fire. It will only do that if SX-399 is level 2 or above, meaning the player has
    -- had her in their party at least once, since she gains EXP when she joins the party in the biolabs.
    if(AdvCombat_GetProperty("Does Party Member Exist", "SX-399") == false) then return end
    AdvCombat_SetProperty("Push Party Member", "SX-399")
        local iLevel = AdvCombatEntity_GetProperty("Level")
    DL_PopActiveObject()
    io.write("Level is " .. iLevel .. "\n")
    if(iLevel < 1) then return end
end
