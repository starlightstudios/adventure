-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "WarpActivate") then
    fnWarpUnlockHandler("Root/Variables/Chapter5/Campfires/iBlackSiteA", 14.25, 18.50)
    
--TF Check, changes Christine to Human.
elseif(sObjectName == "TFCheck") then
	local iCompletedBlackSite = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedBlackSite", "N")
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	if(iCompletedBlackSite == 0.0 and sChristineForm ~= "Human") then
	
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](Better change forms to maintain my cover...)") ]])
		fnCutsceneBlocker()
	
		--Flashwhite.
		Cutscene_CreateEvent("Flash Christine White", "Actor")
			ActorEvent_SetProperty("Subject Name", "Christine")
			ActorEvent_SetProperty("Flashwhite Quickly", "Null")
		DL_PopActiveObject()
		fnCutsceneBlocker()

		fnCutsceneWait(75)
		fnCutsceneBlocker()
		fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
		fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
		fnCutsceneBlocker()
    
    end

--55 leaves the party.
elseif(sObjectName == "55Leaves") then
	
	--Variables.
	local iCompletedBlackSite = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedBlackSite", "N")
	if(iCompletedBlackSite == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iCompletedBlackSite", "N", 1.0)
		VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
		fnCutscene([[ Append("55:[E|Neutral] My motion tracker indicates the Steam Droids are already inside the facility.[P] This is where we part ways.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Right.[P] Good luck, 55...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
	
		--Move 55 away.
		fnCutsceneMove("Tiffany", 18.25, 13.50)
		fnCutsceneMove("Tiffany", 25.25, 13.50)
		fnCutsceneBlocker()
		fnCutsceneFace("Tiffany", 0, -1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneTeleport("Tiffany", -100.25, -100.50)
		fnCutsceneBlocker()
		
		--Remove 55 from the party. This includes the combat party.
		giFollowersTotal = 0
		gsaFollowerNames = {}
		giaFollowerIDs = {0}
		AL_SetProperty("Unfollow Actor Name", "Tiffany")
        for i = 0, gciCombat_MaxActivePartySize-1, 1 do
            local sMemberName = AdvCombat_GetProperty("Name of Active Member", i)
            if(sMemberName ~= "Christine") then
                AdvCombat_SetProperty("Party Slot", i, "Null")
            end
        end
	end
	
--JX-101 joins the party.
elseif(sObjectName == "JX101Joins") then
	--Variables.
	local iCompletedBlackSite = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedBlackSite", "N")
	if(iCompletedBlackSite == 1.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iCompletedBlackSite", "N", 2.0)
		
		--Add JX-101 to the party.
		fnAddPartyMember("JX-101")
        
        --Set JX-101 to level 9.
        local iTotalXP = AdvCombatEntity_GetProperty("Exp For Level", 9)
        AdvCombat_SetProperty("Push Party Member", "JX-101")
            AdvCombatEntity_SetProperty("Current Exp", iTotalXP)
        DL_PopActiveObject()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] What is going on?[P] Why aren't there alarms going off?") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
	
		--Movement.
		fnCutsceneMove("Christine", 23.25, 5.50)
		fnCutsceneMove("Christine", 21.25, 5.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneFace("JX-101", 1, 0)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Reporting for duty, ma'am.[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] Ah, good, right on time.[P] How are your legs?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] A little tired.[P] Why do you ask?[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] I gave you the shortest and most direct route.[P] You humans tend to get tired after physical activity.[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] There are some perks to being a Steam Droid despite everything that has happened...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] The oxygen levels in the mines are a little low.[P] I get tired more easily here.[P] I think I'm getting used to it, though.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] So, I'm ready for anything.[P] But -[P] is this the detention site?[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] No, this is its cover.[P] This used to be a tram servicing station, but the golems abandoned it when the mines were depleted.[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] They took out everything of use to them.[P] We scavenged what they didn't take, years ago.[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] To think I've walked past this place a hundred times without knowing the crimes they were committing under our noses...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Movement.
		fnCutsceneMove("JX-101", 20.25, 4.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutscene([[ AudioManager_PlaySound("Combat|Impact_Strike") ]])
		fnCutscene([[ AL_SetProperty("Set Layer Disabled", "FloorFake", true) ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneMove("JX-101", 20.25, 5.50)
		fnCutsceneBlocker()
		fnCutsceneFace("JX-101", 1, 0)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] Once you know where to look, their entrances aren't that hard to find.[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] Christine...[P] Something about this site is off.[P] Very off.[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] Normally we would expect a punitive force to have moved in by now, but none of their alarm systems are firing.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] (Hmm, I wonder if 55 is disabling their alarms?[P] But she didn't mention it...)[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Don't look a gift horse in the mouth.[P] That's a saying the Yanks are fond of.[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] I assume it means we are not to question good fortune.[P] Fine.[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] You and I are the spearhead.[P] The tight quarters of these sites prohibits large troop movements.[P] Our objective is to clear the area.[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] Locate high-priority targets and liquidate them.[P] Liberate any prisoners.[P] Acquire high-value material.[P] Are we clear?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] As crystal.[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] Good.[P] Let's go.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Move JX-101 onto Christine and fold the party.
		fnCutsceneMove("Christine", 20.25, 5.50)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	end

--Ending sequence.
elseif(sObjectName == "EndingSequence") then

	--Only fires at 9.0, which is the ending code.
	local iCompletedBlackSite = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedBlackSite", "N")
	if(iCompletedBlackSite ~= 9.0) then return end
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iCompletedBlackSite", "N", 10.0)
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroid500RepState", "N", 2.0)
	
	--Spawn 55.
	fnSpecialCharacter("Tiffany", -100, -100, gci_Face_South, false, nil)
	
	--Movement.
	fnCutsceneMove("Christine", 23.25, 8.50)
	fnCutsceneFace("Christine", 0, 1)
	fnCutsceneMove("JX-101", 24.25, 8.50)
	fnCutsceneFace("JX-101", 0, 1)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneFace("DroidBSAA", 1, 0)
	fnCutsceneFace("DroidBSAB", -1, 0)
	fnCutsceneFace("DroidBSAC", -1, 0)
	fnCutsceneFace("DroidBSAD", 1, 0)
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("JX-101:[VOICE|JX-101] Troops![P] Gather round and listen up!") ]])
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneMove("DroidBSAA", 22.25, 8.50)
	fnCutsceneMove("DroidBSAB", 25.25, 8.50)
	fnCutsceneMove("DroidBSAC", 24.25, 9.50)
	fnCutsceneFace("DroidBSAC", 0, -1)
	fnCutsceneMove("DroidBSAD", 14.25, 9.50)
	fnCutsceneMove("DroidBSAD", 23.25, 9.50)
	fnCutsceneFace("DroidBSAD", 0, -1)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "JX-101", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Mission accomplished, we're pulling out.[P] We've got several prisoners in tow, so we'll move in groups of two.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] ...[P] Still no alarms or patrols from Regulus City?[B][C]") ]])
	fnCutscene([[ Append("Steam Droid:[E|Neutral] No ma'am.[P] A repair golem came by earlier, but left without even seeing any of us.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Well, we won't be looking a gift yank in the mouth,[P] right Christine?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Close enough![B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Right, I need a volunteer to act as the rear guard.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Oh, I'll do it![B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Christine...[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Get the prisoners out of here.[P] If we had to carry one, my fleshy arms would just get tired.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] I'll not argue with the practical point, but don't take a risk.[P] And if the alarm does sound, you get right out of here.[P] Understood?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Completely.[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Christine has volunteered to cover the rear.[P] Fists, move out!") ]])
	fnCutsceneBlocker()
    
    --Extra scene if the player has JX-101's cuirass.
    if(AdInv_GetProperty("Item Count", "JX-101's Cuirass") > 0) then
        
        --Equip it back to JX-101.
        AdvCombat_SetProperty("Push Party Member", "JX-101")
            AdvCombatEntity_SetProperty("Equip Item To Slot", "Body", "JX-101's Cuirass")
        DL_PopActiveObject()
        
        --Wait a bit.
        fnCutsceneWait(25)
        fnCutsceneBlocker()
	
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "JX-101", "Neutral") ]])
        fnCutscene([[ Append("JX-101:[E|Neutral] Christine, before we move out, could you hand me back my cuirass?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Sure, here you go.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Why did you take it off, anyway?[B][C]") ]])
        fnCutscene([[ Append("JX-101:[E|Neutral] It can get a tad constricting.[P] We always charge on steam before we leave, which causes our chest cavities to swell.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Blush] ...[P] Bigger boobs whenever you want them?[B][C]") ]])
        fnCutscene([[ Append("JX-101:[E|Neutral] Ha ha![P] Oh to be young again.[B][C]") ]])
        fnCutscene([[ Append("JX-101:[E|Neutral] Thank you for holding on to it for me.[P] See you at Sprocket City.") ]])
        fnCutsceneBlocker()
    
    end
	
	--Fade to black.
	fnCutsceneWait(90)
	fnCutscene([[ AL_SetProperty("Activate Fade", 90, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	
	--Teleport everyone offscreen.
	fnCutsceneTeleport("DroidBSAA", -100.0, -100.0)
	fnCutsceneTeleport("DroidBSAB", -100.0, -100.0)
	fnCutsceneTeleport("DroidBSAC", -100.0, -100.0)
	fnCutsceneTeleport("DroidBSAD", -100.0, -100.0)
	fnCutsceneTeleport("JX-101", -100.0, -100.0)
	fnCutsceneTeleport("Christine", 14.25, 19.50)
	fnCutsceneTeleport("Tiffany", 25.25, 13.50)
	fnCutsceneFace("Christine", 1, -1)
	fnCutsceneBlocker()
	fnCutsceneWait(90)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("PDU:[VOICE|Tiffany] Hold position, I am on my way.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(55)
	fnCutsceneBlocker()
	
	--Fade in.
	fnCutscene([[ AL_SetProperty("Activate Fade", 90, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneWait(90)
	fnCutsceneBlocker()
	
	--Move 55.
	fnCutsceneMove("Tiffany", 19.25, 13.50)
	fnCutsceneMove("Tiffany", 16.25, 16.50)
	fnCutsceneMove("Tiffany", 16.25, 18.50)
	fnCutsceneMove("Tiffany", 15.25, 19.50)
	fnCutsceneFace("Tiffany", -1, 0)
	fnCutsceneBlocker()
	fnCutsceneFace("Christine", 1, 0)
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
	fnCutscene([[ Append("55:[E|Neutral] Unit 771852, report mission status.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Successful.[P] We found some useful blueprints.[P] I took a photo of them on my PDU without JX-101 noticing.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Excellent.[P] Forward them to Unit 499323.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Already done.[P] She said she'd look them over as soon as she can be sure she won't be seen.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] And do you believe you can synthesize a power core from them?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] It's going to be a kitbash, but I think so.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] A kitbash?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] You wouldn't understand unless you've made custom figurines before.[P] It may not be pretty, but it'll work.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] I'll need to head back to Sprocket City first.[P] SX-399 can probably tell me what some of these symbols are supposed to mean.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Good.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] And -[P] great job on keeping those alarms at bay, 55![P] We took no casualties![B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] The alarms had already been disabled before I entered the premises.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] ...[P] Excuse me?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] The alarms had already been disabled before I entered the premises.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] *Sigh*[P] No, 55, what does that mean?[P] Why?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] I do not know.[P] The alarm system had been rerouted to a disconnected network address.[P] It was deliberate, even a complete incompetent would have noticed the error.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] ...[P] Dash it, all we ever get is more questions.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Let us return to SX-399.[P] I hope she is -[P] pleased.[P] To hear the news.") ]])
	fnCutsceneBlocker()
    
    --Clear the party.
    for i = 0, 3, 1 do
        AdvCombat_SetProperty("Party Slot", 0, "Null")
    end
	
	--Remove JX-101 from the party.
	AL_SetProperty("Unfollow Actor Name", "JX-101")
	
	--Reassemble the party.
    AdvCombat_SetProperty("Party Slot", 0, "Christine")
	fnAddPartyMember("Tiffany")
	
	--Move 55 onto Christine and fold the party.
	fnCutsceneMove("Tiffany", 14.25, 19.50)
	fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
    
    EM_PushEntity("Tiffany")
        local iTiffanyID = RO_GetID()
    DL_PopActiveObject()
    
    --Followers storage.
    giFollowersTotal = 1
    gsaFollowerNames[1] = "Tiffany"
    gsaFollowerNames[2] = nil
    giaFollowerIDs[1] = iTiffanyID
    giaFollowerIDs[2] = nil
		
--Can't leave the area until the mission is completed.
elseif(sObjectName == "CantLeaveYet") then
	
	--Variables.
	local iCompletedBlackSite = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedBlackSite", "N")
	if(iCompletedBlackSite == 0.0 or iCompletedBlackSite == 10.0) then return end
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (I can't leave until the mission is complete!)") ]])
	fnCutsceneBlocker()
	
	--Get Christine's position.
	EM_PushEntity("Christine")
		local iX, iY = TA_GetProperty("Position")
	DL_PopActiveObject()
	
	--Move, keep the Y position.
	fnCutsceneMove("Christine", iX / 16.0, 24.50)
	if(iCompletedBlackSite >= 2.0) then
		fnCutsceneMove("JX-101", iX / 16.0, 24.50)
	end
	fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
	
--Walk back to MinesD.
elseif(sObjectName == "WalkToMines") then
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (Walk to the Sprocket City entrance?)[BLOCK]") ]])

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"No\") ")
	fnCutsceneBlocker()

-- |[Post Decision]|
elseif(sObjectName == "Yes") then
	WD_SetProperty("Hide")
	
	--Transition.
	fnCutscene([[ AL_BeginTransitionTo("TelluriumMinesD", "FORCEPOS:50.0x5.0x0") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "No") then
	WD_SetProperty("Hide")
	
	--Get Christine's position.
	EM_PushEntity("Christine")
		local iX, iY = TA_GetProperty("Position")
	DL_PopActiveObject()
	
	--Move, keep the Y position.
	fnCutsceneMove("Christine", 3.25, iY / 16.0)
	fnCutsceneMove("Tiffany", 3.25, iY / 16.0)
	fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()

end
