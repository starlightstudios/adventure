-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
    
    -- |[DroidBSAA]|
    if(sActorName == "DroidBSAA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] Just useless junk out here...") ]])
        fnCutsceneBlocker()
        
    -- |[DroidBSAB]|
    elseif(sActorName == "DroidBSAB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] We expect enemy reinforcements to be routed through this elevator.[P] We'll hold them off, you go inside.") ]])
        fnCutsceneBlocker()
    
    -- |[DroidBSAC]|
    elseif(sActorName == "DroidBSAC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] No enemy movement yet.[P] Are their alarms malfunctioning?") ]])
        fnCutsceneBlocker()
    
    -- |[DroidBSAD]|
    elseif(sActorName == "DroidBSAD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] The golems left their defragmentation pods, but they stripped out everything, even the wiring.") ]])
        fnCutsceneBlocker()
    end
end
