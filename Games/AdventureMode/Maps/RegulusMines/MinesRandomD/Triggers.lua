-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Start the mines cutscene.
if(sObjectName == "StartCutscene") then
    
    --Variables.
    local iMineDMet = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineDMet", "N")
    if(iMineDMet == 1.0) then return end

    --Flags.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMineDMet", "N", 1.0)
    
    --Events.
    fnCutsceneMove("Christine", 36.25, 21.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneMove("Tiffany", 37.25, 21.50)
    fnCutsceneFace("Tiffany", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneFace("PP-81", 0, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
    fnCutscene([[ Append("Steam Droid:[E|Neutral] Please, please, form a nice line.[P] One at a time, thank you.[B][C]") ]])
    fnCutscene([[ Append("Steam Droid:[E|Neutral] I'm afraid I am not doing autographs at the moment, due to -[P] extenuating circumstances.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Are circumstances ever [P]*not*[P] extenuating?[B][C]") ]])
    fnCutscene([[ Append("Steam Droid:[E|Neutral] No, I think that is all circumstances ever do.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] And who are you, exactly?[B][C]") ]])
    fnCutscene([[ Append("Steam Droid:[E|Neutral] Of course you don't recognize me.[P] This is what I get for shying away from the public eye.[B][C]") ]])
    fnCutscene([[ Append("PP-81:[E|Neutral] I am PP-81, the world-famous artist.[P] Surely you've heard of me now?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] (No.)[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Oh, of course![P] You did...[P] you know...[P] and...[P] the thing...[B][C]") ]])
    fnCutscene([[ Append("PP-81:[E|Neutral] I did, actually.[P] It's nice to meet a fan who knows how to properly show appreciation.[B][C]") ]])
    fnCutscene([[ Append("PP-81:[E|Neutral] Unlike the other fans who simply cannot take a hint.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] There's nobody else here...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] Wait do you mean the monsters?[B][C]") ]])
    fnCutscene([[ Append("PP-81:[E|Neutral] Hardly a proper way to refer to them.[P] Rude, yes, but monstrous?[P] That's a stretch.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] (Them trying to gnaw off your arms is a stretch?)[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Christine, is there a reason we are dealing with this unit?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] I think she needs our help.[P] Do you, PP-81?[B][C]") ]])
    fnCutscene([[ Append("PP-81:[E|Neutral] Kind of you to offer, but no.[P] Be on your way.[B][C]") ]])
    fnCutscene([[ Append("PP-81:[E|Neutral] Well I suppose there is one thing you could help me with.[B][C]") ]])
    fnCutscene([[ Append("PP-81:[E|Neutral] I was attempting a new mode of rendering.[P] I call it Temporal Cubism -[P] an attempt to render a scene from all four sides of a cube.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] (But a cube has six sides...)[B][C]") ]])
    fnCutscene([[ Append("PP-81:[E|Neutral] However, some over-eager fans of mine have made a real mess of my studio.[P] Such is the inevitable hunt for souveniers, I suppose.[B][C]") ]])
    fnCutscene([[ Append("PP-81:[E|Neutral] They always tire themselves out eventually, but if you could recover my paints from my studio I'd be most grateful.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Sure.[P] I think we can manage that.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smirk] *You think she has a reward for us?*[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] *Call it a hunch.*[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] We'll be right back with your art supplies.[P] Don't worry.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Fold the party up.
    fnCutsceneMove("Tiffany", 36.25, 21.50)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Fold Party") ]])
    fnCutsceneBlocker()

end
