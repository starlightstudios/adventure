-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Variables.
    local iMineDArtSupplies = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineDArtSupplies", "N")
    local iMineDTerminal    = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineDTerminal", "N")
    local iMineDCrate       = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineDCrate", "N")
    
    --Common.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    --Player has not got the art supplies yet:
    if(iMineDArtSupplies == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
        fnCutscene([[ Append("PP-81:[E|Neutral] A great artist is never appreciated in her time.[P] Does that mean I'm a terrible artist if I have such rabid fans?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] (Rabid...[P] Yeah that's how I'd put it.)") ]])
        
    --Looked for the art supplies and didn't find them.
    elseif(iMineDArtSupplies == 1.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMineDArtSupplies", "N", 2.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] We searched your workshop but we didn't find anything I'd describe as art supplies.[P] There were no paints there.[B][C]") ]])
        fnCutscene([[ Append("PP-81:[E|Neutral] Oh, of course there aren't.[P] You, my friend, do not understand metaphor.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Excuse me?[B][C]") ]])
        fnCutscene([[ Append("PP-81:[E|Neutral] Abstraction in art forms means I must put to canvas something which is not, in fact, there.[P] That is the entire purpose of Temporal Cubism.[B][C]") ]])
        fnCutscene([[ Append("PP-81:[E|Neutral] To render something during all times of its existence, simultaneously, and from every direction.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] So then where are the paints you need?[B][C]") ]])
        fnCutscene([[ Append("PP-81:[E|Neutral] They were paints in our primitive understanding of time's past.[P] They were paints, but now they are the painted object.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Which is?[B][C]") ]])
        fnCutscene([[ Append("PP-81:[E|Neutral] Take a look at the navigation terminal south of here.[P] You'll see what I mean.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Sheesh.[P] Okay.[P] C'mon, 55.") ]])
    
    --Repeats until the player checks the terminal.
    elseif(iMineDArtSupplies == 2.0 and iMineDTerminal == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
        fnCutscene([[ Append("PP-81:[E|Neutral] Please check the navigation terminal south of here.[P] Perhaps that will help you appreciate fine art.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Yeah, yeah, we're on it.") ]])
    
    --Checked the "Terminal"
    elseif(iMineDArtSupplies == 2.0 and iMineDTerminal == 1.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMineDTerminal", "N", 2.0)
        
        --Player did not check the crate yet.
        if(iMineDCrate == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] I think I understand Temporal Cubism a little better now.[P] You painted the terminal to look exactly like it did in the past, but in the present.[P] Clever![B][C]") ]])
            fnCutscene([[ Append("PP-81:[E|Neutral] Is that so?[P] Well, I have one final task for you.[B][C]") ]])
            fnCutscene([[ Append("PP-81:[E|Neutral] I left some things in a crate southwest of here.[P] I'd like you to retrieve them for me.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Don't ask how, but somehow I knew you would have one last task for us.[B][C]") ]])
            fnCutscene([[ Append("PP-81:[E|Neutral] Well, maybe you do understand Temporal Cubism, then.[P] But, evidently, not well enough.") ]])
        
        --Player checked the crate.
        else
            VM_SetVar("Root/Variables/Chapter5/Scenes/iMineDCrate", "N", 2.0)
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] I think I understand Temporal Cubism a little better now.[P] You painted the terminal to look exactly like it did in the past, but in the present.[P] Clever![B][C]") ]])
            fnCutscene([[ Append("PP-81:[E|Neutral] Is that so?[P] Well, I have one final task for you.[B][C]") ]])
            fnCutscene([[ Append("PP-81:[E|Neutral] I left some things in a crate southwest of here.[P] I'd like you to retrieve them for me.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Oh, did you want us to take the paints in there and give them to you?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Because we did.[P] Here you go.[B][C]") ]])
            fnCutscene([[ Append("PP-81:[E|Neutral] Oh my goodness, you're a true fan![P] Marvellous![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Uhhhh...[B][C]") ]])
            fnCutscene([[ Append("PP-81:[E|Neutral] Past, present, and future, all wrapped into one![P] The space between the space, the hole is empty![P] It's you![B][C]") ]])
            fnCutscene([[ Append("PP-81:[E|Neutral] You saw the past and present wrapped together in my piece, and found the future![P] You brought me my paints, unprompted![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Actually I was just a little suspicious and figured you'd have left something else for us to do.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] Your foresight is precisely the purpose of the art, Christine.[B][C]") ]])
            fnCutscene([[ Append("PP-81:[E|Neutral] I've been so inspired by my fans, you see.[P] It was from them that I got the idea of Temporal Cubism.[B][C]") ]])
            fnCutscene([[ Append("PP-81:[E|Neutral] Honestly, can we tell the difference between a prophet and a maddened rambler?[B][C]") ]])
            fnCutscene([[ Append("PP-81:[E|Neutral] Were the Gods to give us orders, would we be able to comprehend them?[P] Gold has no value to a God, nor anything else material, so what would they want?[B][C]") ]])
            fnCutscene([[ Append("PP-81:[E|Neutral] To a God, having certain things in certain places at certain times might be what they desire the way we desire food or water or family.[P] We would never be able to understand.[B][C]") ]])
            fnCutscene([[ Append("PP-81:[E|Neutral] There is much to be learned from insane gibbering.[P] I owe my fans a great deal.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I'm pretty sure they're still going to try eating you if they get the chance.[B][C]") ]])
            fnCutscene([[ Append("PP-81:[E|Neutral] That much is obvious.[P] I'm an artist, not a fool.[B][C]") ]])
            fnCutscene([[ Append("PP-81:[E|Neutral] Still, you have helped me with an important piece of performance art.[P] I will be returning to Steamston now.[B][C]") ]])
            fnCutscene([[ Append("PP-81:[E|Neutral] Fear not, word of your fine taste will be spread.[P] Perhaps I will even dedicate a piece to you.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] *I guess that's our reward.[P] Artists sure are cheap.*[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smug] *We won't be appreciated in our time.[P] That is the fate of great people, isn't it?*[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] *Oh you're in rare form today aren't you?*") ]])
    
        end
    
    --Repeats until the crate is checked.
    elseif(iMineDArtSupplies == 2.0 and iMineDTerminal == 2.0 and iMineDCrate == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
        fnCutscene([[ Append("PP-81:[E|Neutral] I left something in a crate southwest of here.[P] Retrieve it, if you would.[P] Thank you.") ]])
    
    --Crate checked.
    elseif(iMineDArtSupplies == 2.0 and iMineDTerminal == 2.0 and iMineDCrate == 1.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMineDCrate", "N", 2.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
        fnCutscene([[ Append("PP-81:[E|Neutral] Right on time with my paints.[P] As expected, well done, and all that.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] So you left the paints over there, and lied to us and said they were in your workshop?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Because the paints weren't the piece.[P] This is performance art we're meant to live through.[B][C]") ]])
        fnCutscene([[ Append("PP-81:[E|Neutral] Oh my, is this a glimmer of understanding?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] We were supposed to go look in the crate and find the paints right after checking the fake terminal, right?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Because the past, present, and future are simultaneous just as one views the sides of a cube simultaneously?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Well we didn't because there's monsters trying to kill us in the way.[P] Maybe you should account for that in your simultaneous future.[B][C]") ]])
        fnCutscene([[ Append("PP-81:[E|Neutral] Ah, too true.[P] I may be a great artist, but even I am not perfect.[P] Still, it seems you understand Temporal Cubism well enough.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Even something as simple as a storage crate gains new meaning in the future once we experience a certain version of the present.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] So why were we risking our lives for an art piece, again?[B][C]") ]])
        fnCutscene([[ Append("PP-81:[E|Neutral] I've been so inspired by my fans, you see.[P] It was from them that I got the idea of Temporal Cubism.[B][C]") ]])
        fnCutscene([[ Append("PP-81:[E|Neutral] Honestly, can we tell the difference between a prophet and a maddened rambler?[B][C]") ]])
        fnCutscene([[ Append("PP-81:[E|Neutral] Were the Gods to give us orders, would we be able to comprehend them?[P] Gold has no value to a God, nor anything else material, so what would they want?[B][C]") ]])
        fnCutscene([[ Append("PP-81:[E|Neutral] To a God, having certain things in certain places at certain times might be what they desire the way we desire food or water or family.[P] We would never be able to understand.[B][C]") ]])
        fnCutscene([[ Append("PP-81:[E|Neutral] There is much to be learned from insane gibbering.[P] I owe my fans a great deal.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] I'm pretty sure they're still going to try eating you if they get the chance.[B][C]") ]])
        fnCutscene([[ Append("PP-81:[E|Neutral] That much is obvious.[P] I'm an artist, not a fool.[B][C]") ]])
        fnCutscene([[ Append("PP-81:[E|Neutral] Still, you have helped me with an important piece of performance art.[P] I will be returning to Steamston now.[B][C]") ]])
        fnCutscene([[ Append("PP-81:[E|Neutral] Fear not, word of your fine taste will be spread.[P] Perhaps I will even dedicate a piece to you.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] *I guess that's our reward.[P] Artists sure are cheap.*[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smug] *We won't be appreciated in our time.[P] That is the fate of great people, isn't it?*[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] *Oh you're in rare form today aren't you?*") ]])
    
    --Repeats.
    elseif(iMineDArtSupplies == 2.0 and iMineDTerminal == 2.0 and iMineDCrate == 2.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
        fnCutscene([[ Append("PP-81:[E|Neutral] I shall be returning to Steamston soon enough.[P] Fret not, my friends, for your efforts will be immortalized in the pantheon of artistic understanding.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Yeah, thanks.[P] Try not to get mauled before you get immortalized, okay?") ]])
    end
end
