-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Subscript]|
--Run this script. If the variable gbCaughtScript is thrown to true, stop there.
gbCaughtScript = false
LM_ExecuteScript(gsRandomExaminationHandler, sObjectName)
if(gbCaughtScript == true) then
    gbCaughtScript = nil
    return
end

--Clear.
gbCaughtScript = nil

-- |[Examinables]|
if(sObjectName == "Painting") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Despair')") ]])
    fnCutsceneBlocker()
        
elseif(sObjectName == "Easels") then

    local iMineDArtSupplies = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineDArtSupplies", "N")
    if(iMineDArtSupplies == 0.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMineDArtSupplies", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Easels and blank canvases, but no paints.[P] There's no paints anywhere in the shop.[P] Maybe we should go ask PP-81 to be more specific?)") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Easels and blank canvases.[P] Nothing useful.)") ]])
        fnCutsceneBlocker()
        
    end
        
elseif(sObjectName == "Terminal") then

    --Variables.
    local iMineDArtSupplies = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineDArtSupplies", "N")
    local iMineDTerminal    = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineDTerminal", "N")
    
    --No special dialogue yet.
    if(iMineDArtSupplies < 2.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (One of the navigation computers that monitors movement of trams on the track.[P] It hasn't been used in years, but it looks to be in good shape.)") ]])
        fnCutsceneBlocker()
    
    --Next stage.
    elseif(iMineDArtSupplies == 2.0 and iMineDTerminal == 0.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMineDTerminal", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] This is the terminal she mentioned.[P] Looks pretty ordinary to me.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Yes.[P] However, she wanted us to pay attention to it.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Therefore, something about it is out of the ordinary.[P] I believe I know what.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Enlighten me.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smug] You'll need to use your own thinking, not mine.[P] I can't appreciate art for you.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] You're just soooo funny, aren't you?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Fine.[P] This terminal looks to be in pretty good shape despite being in an abandoned mineshaft.[P] Surprised the Steam Droids haven't stripped it for parts.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] ...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Laugh] Oh, I get it![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] This terminal [P]*was*[P] stripped for parts, but PP-81 painted over it into an exact replica of a terminal![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] The screen is painted on to an accuracy of 1 in 1000![B][C]") ]])
        fnCutscene([[ Append("55:[E|Smug] Very good.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] While purposeless from my perspective, PP-81 would be good at producing camouflage patterns.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] I think the point is that she has caused the terminal to appear as it did in the past, but in the future?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Because...[P] something something...[P] Temporal Cubism?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Presumably if we tell her we understood her project, she may give us the desired reward.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Hmm, I wonder if we should look around first...") ]])
    
    --Repeats.
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (This nav-terminal has been carefully painted to look like a pristine, functioning terminal.[P] It was actually stripped for parts years ago.)") ]])
        fnCutsceneBlocker()
    
    end

elseif(sObjectName == "Crate") then

    --Variables.
    local iMineDTerminal = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineDTerminal", "N")
    local iMineDCrate    = VM_GetVar("Root/Variables/Chapter5/Scenes/iMineDCrate", "N")
    
    --Normal case.
    if(iMineDTerminal < 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (A supply crate.[P] There's nothing in it, just the crate is here.)") ]])
        fnCutsceneBlocker()
    
    --Finding the paints.
    elseif(iMineDTerminal >= 1.0 and iMineDCrate == 0.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMineDCrate", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] An ordinary shipping crate, but of course it's probably got some trick to it...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Hmmm...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Oh.[P] I see the trick.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Not going to tell me again, right?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] No.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] How many sides does a cube have, Christine?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Six.[P] But PP-81 seemed to think they have four...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Unless -[P] oh![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] The bottom of the crate is missing, it's just the dirt painted to look like a crate![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Laugh] And the top folds out, and there's a collection of paints in it![B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] I noticed the top was thicker than the sides of the crate.[P] I concluded some sort of duplicity was in order.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Ever considered becoming an artist, 55?[P] You seem to have a knack for this.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] No.[P] Art is for its own sake.[P] My concerns are purely practical.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Well, we found PP-81's paints at any rate.[P] We should go return these to her.") ]])
    
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (A supply crate with a trick top and bottom.[P] It was ostensibly a cube, but it only had four sides.)") ]])
        fnCutsceneBlocker()
    
    end

-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
