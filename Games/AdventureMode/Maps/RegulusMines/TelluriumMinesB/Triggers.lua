-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "WarpActivate") then
    fnWarpUnlockHandler("Root/Variables/Chapter5/Campfires/iTelluriumMinesB", 14.25, 25.50)
    
--Either changes Christine to a Golem, or if she doesn't have authorization to enter the mines, moves her back.
elseif(sObjectName == "ChangeToGolem") then

	--Variables.
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	local iTalkedToMinesLord = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToMinesLord", "N")
	
	--Has not yet spoken to the Lord Golem in charge:
	if(iTalkedToMinesLord == 0.0) then
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
		fnCutscene([[ Append("Golem:[E|Neutral] Er, excuse me Lord Unit, but you're not on my authorization list...[B][C]") ]])
		fnCutscene([[ Append("Golem:[E|Neutral] Uh, my Lord Unit told me to make sure nobody enters the mines without authorization...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Oh, very good, Slave Unit.[P] You've performed your function well.[B][C]") ]])
		fnCutscene([[ Append("Golem:[E|Neutral] (Phew!)[B][C]") ]])
		fnCutscene([[ Append("Golem:[E|Neutral] She's just in the main hallway behind me.[P] Thank you, Lord Unit.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Move Christine and 55 back.
		fnCutsceneMove("Christine", 30.25, 24.50)
		fnCutsceneMove("Tiffany", 30.25, 24.50)
		fnCutsceneBlocker()
		
		--Fold the party.
		fnCutscene([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	
	--Christine is not in Golem Form, so switch her:
	elseif(sChristineForm ~= "Golem") then
	
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] (Better maintain my cover as a Golem Lord...)") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
	
		--Flashwhite.
		Cutscene_CreateEvent("Flash Christine White", "Actor")
			ActorEvent_SetProperty("Subject Name", "Christine")
			ActorEvent_SetProperty("Flashwhite Quickly", "Null")
		DL_PopActiveObject()
		fnCutsceneBlocker()

		fnCutsceneWait(75)
		fnCutsceneBlocker()
		fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
		fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
		fnCutsceneBlocker()
	
	end

--Cutscene where 55 explains what she's up to.
elseif(sObjectName == "55ExplainsHerself") then

    -- |[SX-399 Leaves]|
    local iSXIsInMinesParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXIsInMinesParty", "N")
    if(iSXIsInMinesParty == 1.0) then
    
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSXIsInMinesParty", "N", 0.0)
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] Better not be seen with you on the cameras.[P] Catch you later!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMergeParty()
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("SX-399", 19.25, 24.50, 2.00)
        fnCutsceneFace("SX-399", 0, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        fnCutsceneTeleport("SX-399", -1.25, -1.50)
        fnCutscenePlaySound("World|ClimbLadder")
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Remove from combat party.
        fnRemovePartyMember("SX-399", false)
        return
    end

    -- |[Normal Cutscene]|
	--Variables
	local i55ExplainedHerself = VM_GetVar("Root/Variables/Chapter5/Scenes/i55ExplainedHerself", "N")
	if(i55ExplainedHerself == 1.0) then return end
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/i55ExplainedHerself", "N", 1.0)
		
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ Append("55:[E|Neutral] We should be out of auditory range now.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] You have something to say, do you?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] We need to discuss our current objectives.[P] This entry site is the best of several options.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Oh, you mean that this work assignment appearing on my terminal wasn't a coincidence?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Honestly, 55, nothing with you ever is.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] If you're done playing coy, we can get to business.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] I scanned the layout of the mines and noted anomalies with volume and density.[P] The largest one is beneath this sector.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] The Steam Droids have small settlements in various areas under Regulus City.[P] We need to find them and ensure their cooperation in the future.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] The Steam Droids, huh.[P] I was under the impression that, if the administration knew where they were, they'd have eliminated them by now.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] They do not know where they are, but I'm sure we'll find them sooner or later.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] They are not as well hidden as they think, the administration merely does not place a high priority on their elimination.[P] Tracking them down would require valuable resources.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] ...[P] I'm still going to fix the elevator, if that's okay with you.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] It will help to maintain your cover.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Unfortunately, I was not able to determine anything but a vague set of directions.[P] We will need to search thoroughly.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Right.[P] So, where to, then?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] [P]Down.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Ha ha ha![P] Good one![B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Just get moving...") ]])

end
