-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Setup]|
local bActivateDecisionHandler = false

-- |[Exits]|
if(sObjectName == "ToMinesA") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("TelluriumMinesA", "FORCEPOS:19.0x12.0x0")
	VM_SetVar("Root/Variables/Chapter5/Scenes/iIsTramHere", "N", 0.0)
	
elseif(sObjectName == "MineElevator") then
    
    --Variables.
    local iFixedElevator10 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFixedElevator10", "N")
    local iFixedElevator20 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFixedElevator20", "N")
    local iFixedElevator30 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFixedElevator30", "N")
    local iFixedElevator40 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFixedElevator40", "N")
    local iFixedElevator50 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFixedElevator50", "N")
    
    --If any one variable is set:
    if(iFixedElevator10 == 1.0 or iFixedElevator20 == 1.0 or iFixedElevator30 == 1.0 or iFixedElevator40 == 1.0 or iFixedElevator50 == 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Which floor should I go to?)[BLOCK]") ]])
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        if(iFixedElevator10 == 1.0) then
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Floor 10\", " .. sDecisionScript .. ", \"Floor10\") ")
        end
        if(iFixedElevator20 == 1.0) then
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Floor 20\", " .. sDecisionScript .. ", \"Floor20\") ")
        end
        if(iFixedElevator30 == 1.0) then
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Floor 30\", " .. sDecisionScript .. ", \"Floor30\") ")
        end
        if(iFixedElevator40 == 1.0) then
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Floor 40\", " .. sDecisionScript .. ", \"Floor40\") ")
        end
        if(iFixedElevator50 == 1.0) then
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Floor 50?\", " .. sDecisionScript .. ", \"Floor50\") ")
        end
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Cancel\",  " .. sDecisionScript .. ", \"Cancel\") ")
        fnCutsceneBlocker()
    
    --Not ready.
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (This elevator will allow me to descend quickly into the mines, but I need to find and fix the exit points first.)") ]])
        fnCutsceneBlocker()
    
    end

-- |[Elevator Work]|
elseif(sObjectName == "Floor10") then
	WD_SetProperty("Hide")
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("TelluriumMinesC", "FORCEPOS:4.5x15.0x0")
    VM_SetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "N", 10)
    
elseif(sObjectName == "Floor20") then
	WD_SetProperty("Hide")
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("TelluriumMinesD", "FORCEPOS:23.5x6.0x0")
    VM_SetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "N", 20)
    
elseif(sObjectName == "Floor30") then
	WD_SetProperty("Hide")
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("TelluriumMinesF", "FORCEPOS:5.5x5.0x0")
    VM_SetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "N", 30)
    
elseif(sObjectName == "Floor40") then
	WD_SetProperty("Hide")
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("TelluriumMinesG", "FORCEPOS:16.5x17.0x0")
    VM_SetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "N", 40)
    
elseif(sObjectName == "Floor50") then
	WD_SetProperty("Hide")
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("TelluriumMinesH", "FORCEPOS:8.5x15.0x0")
    VM_SetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "N", 50)
    
elseif(sObjectName == "Cancel") then
	WD_SetProperty("Hide")

-- |[Mines RNG]|
elseif(sObjectName == "ToMinesRNG") then
    
    -- |[Baseline]|
	--SFX.
	AudioManager_PlaySound("World|ClimbLadder")
    
	-- |[Execute Script Handler]|
	--Set floor to 1.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor", "N", 1)
	
	--Call script.
	MapHelper:fnSelectNextLevel(1)

-- |[Examinables]|
elseif(sObjectName == "FizzyPopBlue") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (Fizzy Pop![P] For when you want to recharge and damn the consequences![P] This one is out of stock...)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "FizzyPopPink") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (Fizzy Pop![P] For when 100 isn't a high enough percent![P] The stock is almost entirely full...)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalA") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (This terminal monitors the defragmentation patterns of the Lord Unit here.[P] Seems she rests pretty easily and has few dreams.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "RVD") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (This RVD has been set to play a series of documentaries on mining history.[P] It's currently at the dynamite phase, which is still very common on Pandemonium.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalB") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (Defragmentation logs for this unit...[P] She has set a series of training manuals to be synchronized while she defragments.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalC") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (This unit has missed several defragmentation cycles recently, and defragments at random times.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalD") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (The defragmentation logs for this unit indicate she is one of the so-called 'low compatibility' units.[P] Her internal architecture is difficult to interface with, and cannot be logged coherently.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalE") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (Nothing particularly noteworthy on this defragmentation log.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalF") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (This unit has extensive and vivid dreams, mostly about farm animals.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalG") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (This unit has set her defragmentation terminal to download music for her.[P] Perhaps she listens to it during the day?)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalH") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (Security logs.[P] There has been a marked increase in security reports in the mines area as late...)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalI") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (Transit logs.[P] There are many sectors in the mines classified as 'High Risk', but no explanation is present for the classification.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalJ") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (Transit repair manifests.[P] Seems the golems here are running low on spare parts...[P] just like everywhere else, really.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalK") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (A chat log shows the Lord Golem placing formal complaints with Central Administration, requesting security personnel and equipment.[P] No reply is visible.)") ]])
	fnCutsceneBlocker()

-- |[Mines Terminal]|
--Allows configuration of the Tellurium Mines.
elseif(sObjectName == "MinesTerminal") then

    --Base.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (A terminal that contains information about the mines.)[BLOCK]") ]])
    bActivateDecisionHandler = true
    
-- |[Rewards]|
elseif(sObjectName == "Rewards") then
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
    bActivateDecisionHandler = true

    --Variables.
    local bAnyOneReport = false
    local iFixedElevator10 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFixedElevator10", "N")
    local iFixedElevator20 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFixedElevator20", "N")
    local iFixedElevator30 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFixedElevator30", "N")
    local iFixedElevator40 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFixedElevator40", "N")
    local iFixedElevator50 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFixedElevator50", "N")
    local iRewardElevator10 = VM_GetVar("Root/Variables/Chapter5/Scenes/iRewardElevator10", "N")
    local iRewardElevator20 = VM_GetVar("Root/Variables/Chapter5/Scenes/iRewardElevator20", "N")
    local iRewardElevator30 = VM_GetVar("Root/Variables/Chapter5/Scenes/iRewardElevator30", "N")
    local iRewardElevator40 = VM_GetVar("Root/Variables/Chapter5/Scenes/iRewardElevator40", "N")
    local iRewardElevator50 = VM_GetVar("Root/Variables/Chapter5/Scenes/iRewardElevator50", "N")
    
    --Display.
    if(iFixedElevator10 == 1.0 and iRewardElevator10 == 0.0) then
        bAnyOneReport = true
        VM_SetVar("Root/Variables/Chapter5/Scenes/iRewardElevator10", "N", 1.0)
        fnCutscene([[ Append("Thought:[VOICE|Leader] '50 Work Credits have been provided for repairing the elevators on Floor 10.[P] Thank you for your service.'[B][C]") ]])
        local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
        VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + 50)
    end
    if(iFixedElevator20 == 1.0 and iRewardElevator20 == 0.0) then
        bAnyOneReport = true
        VM_SetVar("Root/Variables/Chapter5/Scenes/iRewardElevator20", "N", 1.0)
        fnCutscene([[ Append("Thought:[VOICE|Leader] '75 Work Credits have been provided for repairing the elevators on Floor 20.[P] Thank you for your service.'[B][C]") ]])
        local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
        VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + 75)
    end
    if(iFixedElevator30 == 1.0 and iRewardElevator30 == 0.0) then
        bAnyOneReport = true
        VM_SetVar("Root/Variables/Chapter5/Scenes/iRewardElevator30", "N", 1.0)
        fnCutscene([[ Append("Thought:[VOICE|Leader] '100 Work Credits have been provided for repairing the elevators on Floor 30.[P] Thank you for your service.'[B][C]") ]])
        local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
        VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + 100)
    end
    if(iFixedElevator40 == 1.0 and iRewardElevator40 == 0.0) then
        bAnyOneReport = true
        VM_SetVar("Root/Variables/Chapter5/Scenes/iRewardElevator40", "N", 1.0)
        fnCutscene([[ Append("Thought:[VOICE|Leader] '150 Work Credits have been provided for repairing the elevators on Floor 40.[P] Thank you for your service.'[B][C]") ]])
        local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
        VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + 150)
    end
    if(iFixedElevator50 == 1.0 and iRewardElevator50 == 0.0) then
        bAnyOneReport = true
        VM_SetVar("Root/Variables/Chapter5/Scenes/iRewardElevator50", "N", 1.0)
        fnCutscene([[ Append("Thought:[VOICE|Leader] 'Error - Elevator to floor 50 repaired. Elevator shaft does not travel 50 floors. Error. Dispensing 250 work credits anyway.'[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I guess that will do...)[B][C]") ]])
        local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
        VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + 250)
    end
    
    --Ending case:
    if(bAnyOneReport == false) then
        fnCutscene([[ Append("Thought: (The repair grid does not indicate any pending rewards for Unit 771852.)[BLOCK]") ]])
    else
        fnCutscene([[ Append("Thought: (Unit 771852's rewards for serving the Cause of Science dispensed.[P] Have a preposterously nice day.)[BLOCK]") ]])
    end
    
-- |[Spelunker's Guide]|
elseif(sObjectName == "Guide") then
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (The Tellurium Mines are randomly generated floors with enemies, loot, and exits.[P] Every 10th floor is a special floor.)[B][C]") ]])
	fnCutscene([[ Append("Thought: (Be sure to repair the elevator on each 10th floor so you can return to it quickly.[P] You can use your entry ladder to return to Floor 0 at any time.)[B][C]") ]])
	fnCutscene([[ Append("Thought: (Enemies increase in density and strength every few floors.[P] The quantity of loot increase as well.[P] The mines end at Floor 50.)[B][C]") ]])
	fnCutscene([[ Append("Thought: (You can turn in the extra loot for Work Credits you acquire in the mines.[P] In addition, virtually every item in Chapter 5 can be found in the chests if you are lucky.)[B][C]") ]])
	fnCutscene([[ Append("Thought: (Good hunting down there.[P] Serve the Cause of Science well.)[BLOCK]") ]])
    bActivateDecisionHandler = true

-- |[Work Terminal]|
--Standard terminal, one of several on the station. Offloaded.
elseif(sObjectName == "WorkTerminal") then
    LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/300 Standards/Work Terminal/Execution.lua")
    
-- |[Information]|
elseif(sObjectName == "Statistics") then

    --Variables.
    local iLowestMinesFloor = VM_GetVar("Root/Variables/Chapter5/Scenes/iLowestMinesFloor", "I")
    local iTotalFloorsGenerated = VM_GetVar("Root/Variables/Chapter5/Scenes/iTotalFloorsGenerated", "I")
    local sLowestFloorString = "WD_SetProperty(\"Append\", \"Thought: (The lowest floor you have reached so far is Floor " .. iLowestMinesFloor .. ".)[B][C]\")"
    local sTotalFloorsString = "WD_SetProperty(\"Append\", \"Thought: (The total number of floors generated so far is " .. iTotalFloorsGenerated .. ".)[B][C]\")"

	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (Information and Statistics::)[B][C]") ]])
	fnCutscene(sLowestFloorString)
	fnCutscene(sTotalFloorsString)
	fnCutscene([[ Append("Thought: (Thank you for your service, Unit 771852.[P] Serve the Cause of Science well.)[BLOCK]") ]])
    bActivateDecisionHandler = true

-- |[Random Level Activation/Deactivation]|
elseif(sObjectName == "ActivateBypass") then
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (This option will disable random levels and skip directly to the story levels of the mines.[P] It is provided for debug purposes.)[B][C]") ]])
	fnCutscene([[ Append("Thought: (Random levels are now inactive.)[BLOCK]") ]])
    bActivateDecisionHandler = true
    
    VM_SetVar("Root/Variables/Chapter5/Scenes/iDebugBypassRandomLevels", "N", 1.0)
    
elseif(sObjectName == "DeactivateBypass") then
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (This option will disable random levels and skip directly to the story levels of the mines.[P] It is provided for debug purposes.)[B][C]") ]])
	fnCutscene([[ Append("Thought: (Random levels are now active.)[BLOCK]") ]])
    bActivateDecisionHandler = true
    
    VM_SetVar("Root/Variables/Chapter5/Scenes/iDebugBypassRandomLevels", "N", 0.0)
    
elseif(sObjectName == "Exit") then
	WD_SetProperty("Hide")
    
-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end

-- |[Decision Handler]|
--Used for the configuration terminal. Remember to not have a cutscene blocker at the end of the dialogue.
if(bActivateDecisionHandler == true) then

    --Variables.
    local iDebugBypassRandomLevels= VM_GetVar("Root/Variables/Chapter5/Scenes/iDebugBypassRandomLevels", "N")
    
    --Decision script.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    
    --Information.
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Spelunker's Guide\", " .. sDecisionScript .. ", \"Guide\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Statistics\", " .. sDecisionScript .. ", \"Statistics\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Rewards\", " .. sDecisionScript .. ", \"Rewards\") ")
    
    --Random level bypass.
    if(iDebugBypassRandomLevels == 0.0) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Don't Use Random Levels\", " .. sDecisionScript .. ", \"ActivateBypass\") ")
    else
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Use Random Levels\", " .. sDecisionScript .. ", \"DeactivateBypass\") ")
    end
    
    --Exit option.
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Exit\",  " .. sDecisionScript .. ", \"Exit\") ")
    fnCutsceneBlocker()
    
    
end
