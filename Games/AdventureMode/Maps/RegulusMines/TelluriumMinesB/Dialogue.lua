-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[MinesGolemA]|
    if(sActorName == "MinesGolemA") then
	
        --Variables.
        local iTalkedToMinesLord = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToMinesLord", "N")
        local iLowestMinesFloor = VM_GetVar("Root/Variables/Chapter5/Scenes/iLowestMinesFloor", "N")
        TA_SetProperty("Face Character", "PlayerEntity")
        
        --Has not talked to the lord here:
        if(iTalkedToMinesLord == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedToMinesLord", "N", 1.0)
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Hmm, we have visitors?[P] To what do I owe the pleasure?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Unit 771852 reporting.[P] Were you the one who put in the work request?[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Ah, 771852![P] I'm glad you've taken up the request, because I'm at my wit's end.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] And...[P] A command unit?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I am currently supervising Unit 771852's activities.[P] Please behave as though I am not here.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Performance reviews, is it?[P] I can sympathize.[B][C]") ]])
            fnCutscene([[ Append("692339:[E|Neutral] I am Unit 692339, and I am currently in charge of security and maintenance here.[P] It is under both categories that I require your aid.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Security and maintenance, but not mining?[B][C]") ]])
            fnCutscene([[ Append("692339:[E|Neutral] This sector was nicknamed 'The Tellurium Mines' due to its very rich ore deposits of rare-earth metals, and the interesting pink sheen the tellurium ore has.[B][C]") ]])
            fnCutscene([[ Append("692339:[E|Neutral] The very high concentration ore has since been mined out, and active extraction will likely resume when other high-concentration ore elsewhere is depleted.[B][C]") ]])
            fnCutscene([[ Append("692339:[E|Neutral] Until then, our task is to maintain the transit network and mothballed mining equipment.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I see.[P] So you don't have much of a security detachment?[B][C]") ]])
            fnCutscene([[ Append("692339:[E|Neutral] We did, but they were requisitioned recently.[P] It's now down to me and a handful of my Slave Units, who are not particularly well suited for violence.[B][C]") ]])
            fnCutscene([[ Append("692339:[E|Neutral] A spike in the number of security breaches has forced me to withdraw to this transit station.[P] The mines are effectively unpatrolled.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] So you want me to deal with the security breaches.[P] That shouldn't be too difficult.[B][C]") ]])
            fnCutscene([[ Append("692339:[E|Neutral] That, and your repair skills will be invaluable.[B][C]") ]])
            fnCutscene([[ Append("692339:[E|Neutral] We have quick-access mineshafts that can be accessed via elevator.[P] Unfortunately, our elevators have been damaged recently.[B][C]") ]])
            fnCutscene([[ Append("692339:[E|Neutral] Roughly once every ten floors, you will find an elevator.[P] If you can repair it, you can return to that floor quickly.[B][C]") ]])
            fnCutscene([[ Append("692339:[E|Neutral] The repair grid indicates that parts are being removed wholesale, not damaged.[P] Obviously, I am not in a position to investigate.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] All right, so fix the elevators and deal with the security issues.[P] Right up my alley![B][C]") ]])
            fnCutscene([[ Append("692339:[E|Neutral] Thank you, 771852.[P] I've set a terminal near the mine entrance southwest of here.[B][C]") ]])
            fnCutscene([[ Append("692339:[E|Neutral] The terminal will track your progress and dispense work credits as you complete the assignments.[P] If there's anything else we can do to help, let us know.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I'll have it taken care of before you know it.") ]])
            fnCutsceneBlocker()
            
        --Already spoken to the lord:
        else
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            
            if(iLowestMinesFloor < 1) then
                fnCutscene([[ Append("692339:[E|Neutral] Hello, 771852.[P] How goes the mission?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Haven't started yet.[P] I'm getting any additional information I can before heading out.[B][C]") ]])
                fnCutscene([[ Append("692339:[E|Neutral] I've instructed the Slave Units here to give you whatever assistance you need.[B][C]") ]])
                fnCutscene([[ Append("692339:[E|Neutral] Though it seems more likely that they will be asking you for assistance.[P] Please ignore their requests.[B][C]") ]])
                fnCutscene([[ Append("692339:[E|Neutral] Any assignments are to go through formal channels.[P] Please report any delinquents to me for discipline.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] (Yeah, not likely...)[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Of course.[P] We have regulations for a reason.[P] I'll be getting to it, then.") ]])
            elseif(iLowestMinesFloor < 10) then
                fnCutscene([[ Append("692339:[E|Neutral] Hello, 771852.[P] How goes the mission?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Made some initial progress.[P] There's a lot of ground to cover.[B][C]") ]])
                fnCutscene([[ Append("692339:[E|Neutral] I understand.[P] Do not take any risks unnecessarily, as retrieving a damaged unit from the mines is...[P] inefficient...") ]])
            elseif(iLowestMinesFloor < 20) then
                fnCutscene([[ Append("692339:[E|Neutral] Hello, 771852.[P] How goes the mission?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] I've fixed one of the elevators.[P] You're right, parts are being taken wholesale.[B][C]") ]])
                fnCutscene([[ Append("692339:[E|Neutral] Hmmm, the Darkmatters have been known to randomly move or knock over objects...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] (Is she not aware of the creatures down there?)[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Oh, yes, of course.[P] It's probably them.") ]])
            elseif(iLowestMinesFloor < 30) then
                fnCutscene([[ Append("692339:[E|Neutral] Hello, 771852.[P] How goes the mission?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] The security problems seem to get worse as I go deeper in the mines.[B][C]") ]])
                fnCutscene([[ Append("692339:[E|Neutral] To be expected, honestly.[P] I'm surprised you don't have a security escort with you.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Well, this Command Unit is no slouch in combat herself.[P] We manage.[B][C]") ]])
                fnCutscene([[ Append("692339:[E|Neutral] Is that a Mk. V Pulse Diffractor?[P] My my, you are well equipped...") ]])
            else
                
                local iCompletedMinesWorkOrder = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedMinesWorkOrder", "N")
                if(iCompletedMinesWorkOrder == 0.0) then
                    VM_SetVar("Root/Variables/Chapter5/Scenes/iCompletedMinesWorkOrder", "N", 1.0)
                    fnCutscene([[ Append("692339:[E|Neutral] Hello, 771852.[P] How goes the mission?[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Neutral] The repair situation should be under control now, but the security situation...[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Neutral] I won't be able to resolve that myself.[B][C]") ]])
                    fnCutscene([[ Append("692339:[E|Neutral] While I appreciate your efforts, I would not like you unnecessarily risking yourself.[B][C]") ]])
                    fnCutscene([[ Append("692339:[E|Neutral] Hopefully when whatever crisis is afflicting the city is resolved, we will be able to send proper patrols in.[P] As it is, I thank you for your efforts.") ]])
                else
                    fnCutscene([[ Append("692339:[E|Neutral] Hello, 771852.[P] You're always welcome in this sector.[P] Can I get you anything?[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Neutral] Oh no, I'm not here to impose.[P] Just visiting.") ]])
                end
            end
        end
    
    -- |[MinesGolemB]|
    elseif(sActorName == "MinesGolemB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I used to be a miner, but now I mostly service the transit lines.[P] It's not as fun...") ]])
        fnCutsceneBlocker()
    
    -- |[MinesGolemC]|
    elseif(sActorName == "MinesGolemC") then
	
        --Variables.
        local iTalkedPartsGolem = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedPartsGolem", "N")
        
        --First conversation:
        if(iTalkedPartsGolem == 0.0) then
        
            --Flag
            VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedPartsGolem", "N", 1.0)
        
            --Dialogue
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] L-[P]lord Golem?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Unit 771852 reporting.[P] How may I help you?[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Erm, are you asking me -[P] wait, you want to help me?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Of course, fellow Unit.[P] Your sector is in need of assistance, is it not?[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] (Is she for real, right now?)[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Erm, I don't think I'm supposed to put in informal requests, but all my official requests get denied.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] We're very low on spare parts.[P] As in, I don't think we can fix the next tram that comes in for servicing.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] But you can't requisition some?[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Everything is prioritized for security purposes right now.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Fabricate some?[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Backed up with work orders for security units.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Hmmm...[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Listen, 771852, this is none of your business.[P] We'll be okay.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I'm a Maintenance and Repair Unit.[P] I know how you feel.[P] Seeing damaged equipment and being unable to fix it...[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Isn't it just depressing?[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Well, uh, informally...[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] If you find any spare parts when you're in the mines, could you give me some?[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] I'll give you 6 work credits for every 1 Assorted Parts you provide.[P] Is that okay?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Really, you're providing work credits?[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] I've got a lot banked up, and it's not like I can spend them with the fabricators are booked solid.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I don't know if I can accept them...[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Please?[P] I'd feel awful if you risked your chassis without any reward.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Just come talk to me if you have any parts, and I'll take them off your hands.[P] They'll be put to good use.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] And -[P] I guess I'll have to figure out some way of fudging the logs.[P] Parts appearing from nowhere would look bad...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] PDU, please transfer some of your network access algorithms to this unit's console.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Those should allow you to [P]-ahem-[P] discreetly edit the work logs.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Wow...[P] You trust me with this?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] We're all on the same team, fellow Unit.[P] Now, I'll see about getting you those parts.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Th-[P]thank you, Lord Golem![P] Thank you so much!") ]])
            fnCutsceneBlocker()
        
        --Future conversations.
        else
        
            --Parts count.
            local iPartsCount = AdInv_GetProperty("Item Count", "Assorted Parts")
            local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
            
            --If it's 1 or more, work credits dialogue.
            if(iPartsCount >= 1) then
                
                --Dialogue.
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
                fnCutscene([[ Append("Golem:[E|Neutral] Do you have any spare parts, Lord Golem?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Here you go![SOUND|World|TakeItem][P] Make sure to edit the work logs.[B][C]") ]])
                
                --Variable string.
                local sString = "WD_SetProperty(\"Append\", \"Golem:[E|Neutral] Oh, of course.[P] That's " .. iPartsCount .. " parts for a total of " .. iPartsCount*6 .. " work credits.[P] Here you are.[B][C]\")"
                fnCutscene(sString)
                fnCutscene([[ Append("Golem:[E|Neutral] We always need more parts, so come back if you find more.[P] Thanks!") ]])
                
                --Provide work credits.
                VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + (iPartsCount * 6))
                
                --Remove the parts from the inventory.
                for i = 1, iPartsCount, 1 do
                    AdInv_SetProperty("Remove Item", "Assorted Parts")
                end
                
            --Otherwise, not enough:
            else
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
                fnCutscene([[ Append("Golem:[E|Neutral] Do you have any spare parts, Lord Golem?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Not at the moment, sorry.[P] I'll see what I can find.[B][C]") ]])
                fnCutscene([[ Append("Golem:[E|Neutral] Thanks so much for doing this.[P] You have no idea how much it means to me.") ]])
            end
        end
    
    -- |[MinesGolemD]|
    elseif(sActorName == "MinesGolemD") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] It's my job to keep a lookout for things coming in from the mines.[P] Fortunately, nothing has made an attempt yet...") ]])
        fnCutsceneBlocker()
    
    -- |[MinesGolemE]|
    elseif(sActorName == "MinesGolemE") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] We only have to service trams periodically, and we have little to fix them with.[P] We have a lot of downtime around here.[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] So, of course the Lord Golem keeps us tasked with pointless busywork...") ]])
        fnCutsceneBlocker()
    
    -- |[MinesGolemF]|
    elseif(sActorName == "MinesGolemF") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I swear the GPU on this thing is shot.[P] There's so much stutter and input lag - [B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Lord Unit![P] I was being -[P] productive![B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Oh, don't worry about me.[P] I used to play video games too, when I was younger.[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Really?[P] Have you ever played Mr. Needlemouse?[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Can't say that I have.[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Everyone says Needlemouse Mania is the best, but I really liked Mr. Needlemouse 2.[P] You should play it![B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Sure, next time I have some time off, I'll give it a whirl.") ]])
        fnCutsceneBlocker()
    
    -- |[MinesGolemG]|
    elseif(sActorName == "MinesGolemG") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I had to scavenge the heater from the oilmaker for repairs on a tram.[P] Now all the oil is cold...") ]])
        
    end
end
