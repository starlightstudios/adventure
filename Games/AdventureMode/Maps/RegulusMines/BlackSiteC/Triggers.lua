-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Sample object.
if(sObjectName == "BackUp") then

	--Variables.
	local iSawPartsScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawPartsScene", "N")
	if(iSawPartsScene == 0.0) then
	
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawPartsScene", "N", 1.0)

		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("JX-101:[VOICE|JX-101] *Christine, wait.[P] That's a security checkpoint.*") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Movement.
		fnCutsceneMove("Christine", 23.25, 22.50, 1.50)
		fnCutsceneFace("Christine", 0, -1)
		fnCutsceneMove("JX-101",     22.25, 22.50, 1.50)
		fnCutsceneFace("JX-101",     0, -1)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Drat, I don't think there's a way around.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Maybe I can hack this door open...[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] You're a network security expert too?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] No, but this PDU I found had a lot of hacking programs on it.[P] Let's see what it can do.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] *55?*[B][C]") ]])
		fnCutscene([[ Append("PDU:[VOICE|Tiffany] *Underway.[P] Hold your position.*") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Move the camera.
		Cutscene_CreateEvent("CameraEvent", "Camera")
			CameraEvent_SetProperty("Max Move Speed", 2.0)
			CameraEvent_SetProperty("Focus Position", (27.25 * gciSizePerTile), (19.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "GolemLordB", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
		fnCutscene([[ Append("Lord: Hrmph, Subject D-15 is permanently offline.[P] Pity.[B][C]") ]])
		fnCutscene([[ Append("Slave: I'm sorry, Lord Unit.[P] It seems her core sprung a leak.[B][C]") ]])
		fnCutscene([[ Append("Lord: And the replacement parts?[P] Why did you not repair the subject?[B][C]") ]])
		fnCutscene([[ Append("Slave: Err, cores?[P] I don't think those were in the schematics.[P] Can you replace those?[B][C]") ]])
		fnCutscene([[ Append("Lord: Tch, possibly.[P] We'll need to check the ruins again, and more thoroughly this time, and find a steam core schematic.[B][C]") ]])
		fnCutscene([[ Append("Slave: [P]Uh, Lord Golem, are you seeing this odd network traffic?[P] The doors - [P][CLEAR]") ]])
		fnCutscene([[ Append("Lord: Don't change the subject![P] I have not yet decided your punishment for failing to repair a test subject![B][C]") ]])
		fnCutscene([[ Append("Slave: But why are we bothering to repair the Steam Droids?[P] They'll just break again...[B][C]") ]])
		fnCutscene([[ Append("Lord: [P][SOUND|World|Thump][P]It is not your place to question orders![B][C]") ]])
		fnCutscene([[ Append("Lord: But since you persist in asking...[B][C]") ]])
		fnCutscene([[ Append("Lord: Prisoners do not grow like Empherean Fungus.[P] If we run out of Steam Droids to experiment on, well, we'd have to use other units.[B][C]") ]])
		fnCutscene([[ Append("Lord: Disobedient units.[P] Would you like to share Unit 688231's fate?[B][C]") ]])
		fnCutscene([[ Append("Slave: No, Lord Golem![P] Please![B][C]") ]])
		fnCutscene([[ Append("Lord: Then you will watch your tongue...[B][C]") ]])
		fnCutscene([[ Append("Lord: Why bother explaining this to you?[P] You'll forget it all when you are reprogrammed.[B][C]") ]])
		fnCutscene([[ Append("Lord: Honestly, I can't wait to wash my hands of this dreadful business.[P] The Cause of Science demands so much of us.[B][C]") ]])
		fnCutscene([[ Append("Slave: Yes, Lord Golem.[P] It demands so much.[B][C]") ]])
		fnCutscene([[ Append("Lord: That is the first intelligent thing you've said all day.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Move the camera.
		Cutscene_CreateEvent("CameraEvent", "Camera")
			CameraEvent_SetProperty("Max Move Speed", 2.0)
			CameraEvent_SetProperty("Focus Actor Name", "Christine")
		DL_PopActiveObject()
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] I'd love to scrap both of them...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Even the Slave Unit?[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] Pathetic collaborator, show some damn spine.[P] If they weren't all such cowards, these atrocities would not come to pass.[B][C]") ]])
		fnCutscene([[ Append("JX-101:[E|Neutral] The golems are all responsible.[P] Lord or Slave, their hands are stained with blood and lubricant.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Grrrr...[B][C]") ]])
		fnCutscene([[ Append("PDU:[VOICE|Tiffany] *I've bypassed the security circuit.[P] The door should be open.*[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] The door is open.[P] Let's get going.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Fold the party.
		fnCutsceneMove("JX-101", 23.25, 22.50)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	
	--Otherwise:
	else
	
		--Get Christine's position.
		EM_PushEntity("Christine")
			local iX, iY = TA_GetProperty("Position")
		DL_PopActiveObject()
	
		--Variables.
		local iIsJX101Following = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsJX101Following", "N")
		local iIs55Following    = VM_GetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N")
	
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] (Better not let the security units see us...)") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
	
		--Move, keep the Y position.
		fnCutsceneMove("Christine", 23.25, iY / gciSizePerTile)
		if(iIsJX101Following == 1.0) then
			fnCutsceneMove("JX-101", 23.25, iY / gciSizePerTile)
		end
		if(iIs55Following == 1.0) then
			fnCutsceneMove("Tiffany", 23.25, iY / gciSizePerTile)
		end
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	
	end
end
