-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "SouthExit") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("BlackSiteB", "FORCEPOS:22.5x16.0x0")
	
elseif(sObjectName == "Ladder") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("BlackSiteD", "FORCEPOS:20.0x8.0x0")
	
-- |[Examinables]|
elseif(sObjectName == "SecurityDoor") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] (We don't need to go into the security station.[P] Let's leave them to argue.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "RetiredGolem") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] (I'd ask the PDU for an autopsy, but there's barely anything left of her.)[B][C]") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] (But it looks like the corrosive material came from...[P] inside the golem?") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "DoorLockedSW") then

	--Variables.
	local iSawPartsScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawPartsScene", "N")
	
	--Not open.
	if(iSawPartsScene == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[SOUND|World|AutoDoorFail] Access denied.") ]])
		fnCutsceneBlocker()
	
	--Open.
	else
		AudioManager_PlaySound("World|AutoDoorOpen")
		AL_SetProperty("Open Door", "DoorLockedSW")
	end
	
elseif(sObjectName == "DoorLockedNE") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[SOUND|World|AutoDoorFail] Access denied.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Monstrosity") then
	
	--Variables.
	local iKilledMonstrosity= VM_GetVar("Root/Variables/Chapter5/Scenes/iKilledMonstrosity", "N")
	
	--First time.
	if(iKilledMonstrosity == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iKilledMonstrosity", "N", 1.0)
		
		--Open the door.
		fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
		fnCutscene([[ AL_SetProperty("Open Door", "MonstrosityDoor") ]])
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Prisoner moves up.
		fnCutsceneMove("Monstrosity", 24.25, 5.50, 0.20)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
        
        --It transforms.
        fnCutsceneSetFrame("Monstrosity", "Mutant")
		fnCutsceneWait(15)
		fnCutsceneBlocker()
        fnCutsceneSetFrame("Monstrosity", "Null")
		fnCutsceneWait(15)
		fnCutsceneBlocker()
        fnCutsceneSetFrame("Monstrosity", "Mutant")
		fnCutsceneWait(15)
		fnCutsceneBlocker()
        fnCutsceneSetFrame("Monstrosity", "Null")
		fnCutsceneWait(15)
		fnCutsceneBlocker()
        fnCutsceneSetFrame("Monstrosity", "Mutant")
		fnCutsceneWait(45)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] What is that thing!?[B][C]") ]])
		fnCutscene([[ Append("Creature:[VOICE|Golem] Circle, circle, circle.[P] Inside outside we are neither.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] K-[P]kill it![P] Right now!") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
        
        --Battle.
		fnCutscene([[ AdvCombat_SetProperty("Next Combat Music", "RottenTheme", 0.0000) ]])
        fnCutscene([[ AdvCombat_SetProperty("World Pulse", true) ]])
        fnCutscene([[ AdvCombat_SetProperty("Reinitialize") ]])
        fnCutscene([[ AdvCombat_SetProperty("Activate") ]])
        fnCutscene([[ AdvCombat_SetProperty("Unretreatable", true) ]])
        fnCutscene([[ AdvCombat_SetProperty("Unloseable", true) ]])
        fnCutscene([[ AdvCombat_SetProperty("Victory Script", gsRoot .. "Maps/RegulusMines/BlackSiteC/Combat_Victory.lua") ]])
        fnCutscene([[ AdvCombat_SetProperty("Defeat Script", gsStandardGameOver) ]])
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 5/000 Scripted Enemies/Boss Monstrosity.lua") ]])
        fnCutsceneBlocker()

	--Repeats.
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] (I would...[P] rather not go in there again...)") ]])
		fnCutsceneBlocker()
	
	end

-- |[Other]|
-- |[Debug]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
