-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
    
    -- |[DroidC]|
    if(sActorName == "DroidC") then
	
        --Dialogue.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "JX-101", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] 75 + 15 + -12 + 388.[P] Why doesn't it add up? Maybe -[P] it -[P] the numbers are expanding and contracting like it's breathing?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Don't worry, we're here to rescue you.[P] Follow us![B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] 12 + 12 + 12 + 12 + 12 + 12 + 12 + 12 = 12 = 12 = 12...[B][C]") ]])
        fnCutscene([[ Append("JX-101:[E|Neutral] What the hell is wrong with you?[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[E|Neutral] I am the equation, I don't add up![P] Argh, so frustrating![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] It's like she doesn't even know we're here.[P] Come on, take her hand.[P] She'll follow us if we lead her.") ]])
        fnCutsceneBlocker()
        
        --Get Christine's position.
        EM_PushEntity("Christine")
            local iX, iY = TA_GetProperty("Position")
        DL_PopActiveObject()
        
        --Move the droid to her.
        fnCutsceneMove("DroidC", iX / 16.0, iY / 16.0)
        fnCutsceneBlocker()
        fnCutsceneTeleport("DroidC", -100.0, -100.0)
        fnCutsceneBlocker()
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iRescuedDroidC", "N", 1.0)
            
        --Check ending case.
        local iRescuedDroidA = VM_GetVar("Root/Variables/Chapter5/Scenes/iRescuedDroidA", "N")
        local iRescuedDroidB = VM_GetVar("Root/Variables/Chapter5/Scenes/iRescuedDroidB", "N")
        local iRescuedDroidC = VM_GetVar("Root/Variables/Chapter5/Scenes/iRescuedDroidC", "N")
        local iRescuedGolemD = VM_GetVar("Root/Variables/Chapter5/Scenes/iRescuedGolemD", "N")
        local iFoundSchematics = VM_GetVar("Root/Variables/Chapter5/Scenes/iFoundSchematics", "N")
        if(iRescuedDroidA == 1.0 and iRescuedDroidB == 1.0 and iRescuedDroidC == 1.0 and iRescuedGolemD == 1.0 and iFoundSchematics == 1.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iCompletedBlackSite", "N", 9.0)
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
            fnCutscene([[ Append("JX-101:[E|Neutral] All right Christine, we've done enough.[P] I believe we've swept the site thoroughly.[B][C]") ]])
            fnCutscene([[ Append("JX-101:[E|Neutral] Let's get back to the entrance and get out of here before Regulus City figures out what we've done.") ]])
            fnCutsceneBlocker()
        end
    
    -- |[DroidD]|
    elseif(sActorName == "DroidD") then

        --Variables.
        local iLRTBossResult = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTBossResult", "N")
        local iCompletedSerenity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
        
        --Dialogue.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "JX-101", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] Wah ha ha![P] You're pretending, but I can see the real you![B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] 771852![P] Nobody knows but me, ha ha ha ha![B][C]") ]])
        fnCutscene([[ Append("JX-101:[E|Neutral] She's spouting gibberish.[P] Do you think she was an experiment, too?[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] Oh, bronze and green.[P] Not improved yet.[P] Is this the first time we've met?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Yes?[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] Hello, I am Lord Unit 820189011919089211947![P] Pleased to meet you, 771852 and 101![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] That designation is waaay past the valid range...[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] I touched it, you see.[P] It got in through my hand, but that's fine.[P] It taught me so much.[B][C]") ]])
        if(iLRTBossResult == 0.0 and iCompletedSerenity == 0.0) then
            fnCutscene([[ Append("Golem:[E|Neutral] Ah, you don't know what I'm talking about.[P] But you will.[P] You will, Christine Dormer.[B][C]") ]])
        elseif(iCompletedSerenity == 1.0) then
            fnCutscene([[ Append("Golem:[E|Neutral] Oh don't be coy.[P] You know what I'm referring to.[P] You saw it in Serenity Crater, didn't you?[P] Or has that happened yet?[B][C]") ]])
        else
            fnCutscene([[ Append("Golem:[E|Neutral] And -[P] you're her pupil?[P] The one she was talking about?[P] Oh, I'd be jealous, but we're beyond that, aren't we.[P] Aren't we?[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] *Vivify sends her regards, Christine.*[B][C]") ]])
        end
        fnCutscene([[ Append("Christine:[E|Neutral] !!![B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] Tease tease tease![P] But that's enough.[P] I'm very sleepy.[B][C]") ]])
        fnCutscene([[ Append("JX-101:[E|Neutral] I'll put a slug in your processor so you can sleep forever![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] JX-101, STOP![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Don't you see, she's a victim just as much as anyone else![B][C]") ]])
        fnCutscene([[ Append("JX-101:[E|Neutral] She got what's coming to her.[P] She's clearly insane as a result of whatever sick experiments they're doing here.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] And we have to help her![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Just -[P] please take her back to Sprocket City.[P] Maybe she can be cured?[B][C]") ]])
        fnCutscene([[ Append("JX-101:[E|Neutral] And have a Lord Golem in there, a ticking time bomb?[P] She's crazy and dangerous.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Do it for me, please, I'm begging you![P] Hasn't there been enough death in this place?[B][C]") ]])
        fnCutscene([[ Append("JX-101:[E|Neutral] ...[P] Fine. I consider this a personal favour.[P] I would not do this for just anyone.[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] Oh, this is the part where you take me to a bronze place.[P] I love this part![P] And then you upgrade the broken robot who marries the pale robot, right?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Just shut up and follow me.") ]])
        fnCutsceneBlocker()
        
        --Get Christine's position.
        EM_PushEntity("Christine")
            local iX, iY = TA_GetProperty("Position")
        DL_PopActiveObject()
        
        --Move the droid to her.
        fnCutsceneMove("DroidD", iX / gciSizePerTile, iY / gciSizePerTile)
        fnCutsceneBlocker()
        fnCutsceneTeleport("DroidD", -100.0, -100.0)
        fnCutsceneBlocker()
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iRescuedGolemD", "N", 1.0)
            
        --Check ending case.
        local iRescuedDroidA = VM_GetVar("Root/Variables/Chapter5/Scenes/iRescuedDroidA", "N")
        local iRescuedDroidB = VM_GetVar("Root/Variables/Chapter5/Scenes/iRescuedDroidB", "N")
        local iRescuedDroidC = VM_GetVar("Root/Variables/Chapter5/Scenes/iRescuedDroidC", "N")
        local iRescuedGolemD = VM_GetVar("Root/Variables/Chapter5/Scenes/iRescuedGolemD", "N")
        local iFoundSchematics = VM_GetVar("Root/Variables/Chapter5/Scenes/iFoundSchematics", "N")
        if(iRescuedDroidA == 1.0 and iRescuedDroidB == 1.0 and iRescuedDroidC == 1.0 and iRescuedGolemD == 1.0 and iFoundSchematics == 1.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iCompletedBlackSite", "N", 9.0)
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
            fnCutscene([[ Append("JX-101:[E|Neutral] All right Christine, we've done enough.[P] I believe we've swept the site thoroughly.[B][C]") ]])
            fnCutscene([[ Append("JX-101:[E|Neutral] Let's get back to the entrance and get out of here before Regulus City figures out what we've done.") ]])
            fnCutsceneBlocker()
        end
    end
end
