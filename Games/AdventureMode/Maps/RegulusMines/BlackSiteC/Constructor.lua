-- |[Constructor]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "BlackSiteC"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "EquinoxTheme")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("BlackSiteC")
	
	-- |[Spawn NPCS Here]|
	--NPCs set to spawn here will be available for post-exec cutscenes.

	-- |[Lights]|
    AL_SetProperty("Activate Lights")

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[Spawn NPCs Here]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
	fnStandardNPCByPosition("GolemCA")
	fnStandardNPCByPosition("GolemCB")
	
	--Steam Droid spawns if you haven't rescued her yet.
	local iRescuedDroidC = VM_GetVar("Root/Variables/Chapter5/Scenes/iRescuedDroidC", "N")
	if(iRescuedDroidC == 0.0) then
		TA_Create("DroidC")
			TA_SetProperty("Position", 33, 4)
			TA_SetProperty("Facing", gci_Face_East)
			TA_SetProperty("Clipping Flag", true)
			TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
			fnSetCharacterGraphics("Root/Images/Sprites/SteamDroid/", false)
		DL_PopActiveObject()
	end
	
	--Lord Golem spawns if you haven't rescued her yet.
	local iRescuedGolemD = VM_GetVar("Root/Variables/Chapter5/Scenes/iRescuedGolemD", "N")
	if(iRescuedGolemD == 0.0) then
		TA_Create("DroidD")
			TA_SetProperty("Position", 19, 4)
			TA_SetProperty("Facing", gci_Face_North)
			TA_SetProperty("Clipping Flag", true)
			TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
			fnSetCharacterGraphics("Root/Images/Sprites/GolemLordA/", false)
		DL_PopActiveObject()
	end
	
	--Monstrosity.
    local iKilledMonstrosity = VM_GetVar("Root/Variables/Chapter5/Scenes/iKilledMonstrosity", "N")
	TA_Create("Monstrosity")
		TA_SetProperty("Position", 24, 4)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Clipping Flag", true)
		fnSetCharacterGraphics("Root/Images/Sprites/GolemSlave/", false)
        TA_SetProperty("Wipe Special Frames")
        TA_SetProperty("Add Special Frame", "Mutant", "Root/Images/Sprites/Special/GolemSlave|Mutant")
	DL_PopActiveObject()
    if(iKilledMonstrosity == 1.0) then
        fnCutsceneSetFrame("Monstrosity", "Mutant")
    end
end
