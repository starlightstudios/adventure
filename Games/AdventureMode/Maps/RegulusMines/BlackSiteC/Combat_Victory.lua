-- |[Combat Victory]|
--The party won!

--Music change.
AudioManager_PlayMusic("EquinoxTheme")

-- |[Movement]|
--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()
		
--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("Creature:[VOICE|Golem] Don't feel bad.[P] I don't blame you.[P] You did the right thing.[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] ..?[B][C]") ]])
fnCutscene([[ Append("Creature:[VOICE|Golem] Now.[P] Leave.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
		
--Prisoner moves back.
fnCutsceneMoveFace("Monstrosity", 24.25, 4.50, 0, 1, 0.20)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
		
--Open the door.
fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
fnCutscene([[ AL_SetProperty("Close Door", "MonstrosityDoor") ]])
fnCutsceneWait(25)
fnCutsceneBlocker()

--Variables.
local iIsJX101Following = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsJX101Following", "N")
local iIs55Following = VM_GetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N")

--If JX-101 is present:
if(iIsJX101Following == 1.0) then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] We didn't even hurt it.[P] It's like we were just annoying it.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] I guess we should leave it alone...[B][C]") ]])
	fnCutscene([[ Append("JX-101:[E|Neutral] Another of the administration's crimes.[P] Unforgivable...") ]])
	fnCutsceneBlocker()

--55 is present:
elseif(iIs55Following == 1.0) then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
	fnCutscene([[ Append("55:[E|Neutral] The prisoner does not seem interested in rescue.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Nothing for us in there, then.[P] Let's leave it be.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Shame.[P] It was a Lord Unit, once.[P] Now it is just worthless flesh.") ]])
	fnCutsceneBlocker()
end
