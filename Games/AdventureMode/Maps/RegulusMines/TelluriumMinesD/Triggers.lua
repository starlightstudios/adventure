-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[ ======================================== Triggers ======================================== ]|
--Start the mines cutscene.
if(sObjectName == "StartCutscene") then

	--Variables.
	local iSawMinesDCutscene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMinesDCutscene", "N")
	if(iSawMinesDCutscene == 1.0) then return end
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesDCutscene", "N", 1.0)
	
	--Movement.
	fnCutsceneMove("Christine", 17.25, 9.50)
	fnCutsceneFace("Christine", 1, 0)
	fnCutsceneMove("Tiffany", 17.25, 10.50)
	fnCutsceneFace("Tiffany", 1, 0)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Christine:[VOICE|Christine] Shh.[P] Look, there's our droid...") ]])
	fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|ButtonClick") ]])
    fnCutscene([[ AL_SetProperty("Deactivate Player Light") ]])
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--SX-399 enters the scene.
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 3.0)
		CameraEvent_SetProperty("Focus Actor Name", "SX-399")
	DL_PopActiveObject()
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
	fnCutscene([[ Append("Young Droid:[E|Neutral] There, the station is operational.[P] Now let's see...[B][C]") ]])
	fnCutscene([[ Append("Young Droid:[E|Neutral] ...[B][C]") ]])
	fnCutscene([[ Append("Young Droid:[E|Neutral] Ah, good.[P] Transit manifests for the whole network.[P] Now we can hijack shipments at our leisure![B][C]") ]])
	fnCutscene([[ Append("Young Droid:[E|Neutral] I hope mother will be pleased...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Movement.
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 3.0)
		CameraEvent_SetProperty("Focus Actor Name", "Christine")
	DL_PopActiveObject()
	fnCutsceneMove("SX-399", 35.25, 10.50)
	fnCutsceneMove("SX-399", 35.25, 5.50)
	fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|ButtonClick") ]])
    fnCutscene([[ AL_SetProperty("Activate Player Light", 100, 100) ]])
	fnCutsceneWait(5)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneTeleport("SX-399", -100.25, -100.50)
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Offended] Steam Droids are common bandits, it seems.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Correct.[P] They have never been directly implicated, but there are many reports on the security logs accusing them of thievery.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] This is why I have chosen to seek them out.[P] If not for their combat ability, their knowledge of secondary routes under Regulus City is invaluable.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] She looked to be in pretty poor condition.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] They do not have fabrication machines, to my knowledge.[P] They likely have difficulty finding spare parts.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Hmm.[P] Well, let's tail her.[P] She'll probably lead us right to their hideout.") ]])
	fnCutsceneBlocker()

	--55 moves onto Christine.
	fnCutsceneMove("Tiffany", 17.25, 9.50)
	fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
	
--Start the second cutscene.
elseif(sObjectName == "ShesGone") then

	--Variables.
	local iSawMinesDCutsceneB = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMinesDCutsceneB", "N")
	if(iSawMinesDCutsceneB == 1.0) then return end
	
	--Other variables.
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesDCutsceneB", "N", 1.0)
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Uh, 55?[P] Do you see what I see?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Caverns.[P] Empty track.[P] Confirm ocular receiver calibration code 67-B.[B][C]") ]])
	
	--Organic:
	if(sChristineForm == "Human" or sChristineForm == "Electrosprite" or sChristineForm == "Darkmatter" or sChristineForm == "Eldritch" or sChristineForm == "Raiju") then
		fnCutscene([[ Append("Christine:[E|Neutral] Uh, confirming calibration code -[P] whatever you said.[P] I can't really recall them right now.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] But what I was pointing out was that the Steam Droid is nowhere in sight.[B][C]") ]])
	
	--Robot (or unhandled in case of debug usage)
	else
		fnCutscene([[ Append("Christine:[E|Neutral] Confirm calibration code 67-B.[P] But what I meant was, the track is empty.[P] No Steam Droids.[B][C]") ]])
	end
	fnCutscene([[ Append("55:[E|Neutral] A correct observation, but I do not require calibration on my ocular receivers.[P] You, however, do.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Movement.
	fnCutsceneMove("Tiffany", 44.25, 4.50)
	fnCutsceneMove("Tiffany", 45.25, 3.50)
	fnCutsceneFace("Tiffany", 0, -1)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
	fnCutscene([[ Append("55:[E|Neutral] As expected.[P] There is a secret entrance here.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] The large cavity I had flagged is just below us.[P] This is a good location to put an entrance...[P] I just need to find the release latch...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(65)
	fnCutsceneBlocker()
	fnCutscene([[ AudioManager_PlaySound("Combat|Impact_Strike") ]])
	fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Floor2Fake", true) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Move 55 back to Christine.
	fnCutsceneMove("Tiffany", 44.25, 4.50)
	fnCutsceneFace("Tiffany", -1, 0)
	fnCutsceneMove("Christine", 43.25, 4.50)
	fnCutsceneFace("Christine", 1, 0)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
	fnCutscene([[ Append("55:[E|Neutral] Considering I am a Command Unit, they may attack us before we can recruit them to our cause.[P] Make sure your weapons are ready.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] We should try not to hurt them.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Correct.[P] Better they are destroyed during combat with our enemies than with us.[P] Disable when possible.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Ah -[P] erm, technically true?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Good.[P] Proceed.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Move 55 onto Christine, fold the party.
	fnCutsceneMove("Tiffany", 43.25, 4.50)
	fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()

--Tell the player they can go to the black site.
elseif(sObjectName == "GoEast") then

	--Variables.
	local iTold55Go        = VM_GetVar("Root/Variables/Chapter5/Scenes/iTold55Go", "N")
	local iSawGoEastPrompt = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawGoEastPrompt", "N")
	if(iTold55Go == 0.0 or iSawGoEastPrompt == 1.0) then return end

	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSawGoEastPrompt", "N", 1.0)
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (If we go east from here, we should be able to make our way to the detention site...)") ]])
	fnCutsceneBlocker()

--Finale.
elseif(sObjectName == "SX399Scene") then

	--Variables.
	local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
	if(iSXUpgradeQuest ~= 3.0) then return end

	--Execute.
	LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/500 Normal/Sprocket City Finale/Scene_C_TelluriumMinesD.lua")

-- |[ ==================================== SX Leaves Party ===================================== ]|
--SX changes properties and walks to her neutral position.
elseif(sObjectName == "SX Leaves") then

    --Activation check.
    local iSXIsInMinesParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXIsInMinesParty", "N")
    if(iSXIsInMinesParty == 0.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iSXIsInMinesParty", "N", 0.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Probably best if I don't see myself, so I'll be over by the elevator.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("SX-399", 35.25,  5.50, 2.00)
    fnCutsceneMove("SX-399", 35.25, 11.50, 2.00)
    fnCutsceneMove("SX-399", 28.25, 11.50, 2.00)
    fnCutsceneMove("SX-399", 28.25,  7.50, 2.00)
    fnCutsceneFace("SX-399", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Flip on her collision flag and dialogue.
    EM_PushEntity("SX-399")
        TA_SetProperty("Clipping Flag", true)
        TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
    DL_PopActiveObject()
    
    --Remove from combat party.
    fnRemovePartyMember("SX-399", false)

-- |[ ======================================= Black Site ======================================= ]|
--Go to the black site.
elseif(sObjectName == "WalkToBlackSite") then

	--Variables.
	local iSawGoEastPrompt = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawGoEastPrompt", "N")
	if(iSawGoEastPrompt == 0.0) then return end
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (Walk to the detention site?)[BLOCK]") ]])

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"No\") ")
	fnCutsceneBlocker()

    
-- |[Post Decision]|
elseif(sObjectName == "Yes") then
	WD_SetProperty("Hide")
	
	--Transition.
	fnCutscene([[ AL_BeginTransitionTo("BlackSiteA", "FORCEPOS:2.0x30.0x0") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "No") then
	WD_SetProperty("Hide")
	
	--Get Christine's position.
	EM_PushEntity("Christine")
		local iX, iY = TA_GetProperty("Position")
	DL_PopActiveObject()
	
	--Move, keep the Y position.
	fnCutsceneMove("Christine", 50.25, iY / 16.0)
	fnCutsceneMove("Tiffany", 50.25, iY / 16.0)
	fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()

end
