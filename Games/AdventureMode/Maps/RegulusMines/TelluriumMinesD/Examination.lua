-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Subscript]|
--Run this script. If the variable gbCaughtScript is thrown to true, stop there.
gbCaughtScript = false
LM_ExecuteScript(gsRandomExaminationHandler, sObjectName)
if(gbCaughtScript == true) then
    gbCaughtScript = nil
    return
end

--Clear.
gbCaughtScript = nil

-- |[Exits]|
--Elevator, select any of the visited floors in increments of 10.
if(sObjectName == "Elevator") then

    --Variables.
    local iFixedElevator20 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFixedElevator20", "N")
    if(iFixedElevator20 == 0.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iFixedElevator20", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Hmm...[P] Just a software glitch, actually.[P] Just have to reboot it...)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (All fixed![P] Now I can travel to Floor 20 quickly by accessing the elevator!)") ]])
        fnCutsceneBlocker()
    else
	
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Take the elevator back to the surface?)[BLOCK]") ]])

        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"YesE\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"No\") ")
        fnCutsceneBlocker()
        
    end


-- |[Examinables]|
elseif(sObjectName == "Terminal") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (There's nothing in the transit network going through this way.[P] It's unlikely anyone would route a tram through such a dangerous area...)") ]])
	fnCutsceneBlocker()

-- |[Decisions]|
--Really exit the mines.
elseif(sObjectName == "Yes") then

	--Go to Mines B.
	WD_SetProperty("Hide")
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("TelluriumMinesB", "FORCEPOS:19.0x24.0x0")

--Really exit the mines.
elseif(sObjectName == "YesE") then

	--Go to Mines B.
	WD_SetProperty("Hide")
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("TelluriumMinesB", "FORCEPOS:15.5x23.0x0")

--Nope.
elseif(sObjectName == "No") then
	WD_SetProperty("Hide")

-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
