-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")
    
    -- |[SX-399]|
    if(sActorName == "SX-399") then
        
        --Variables.
        local iSXJoinedInNCPlus = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXJoinedInNCPlus", "N")
        
        --Common.
        TA_SetProperty("Face Character", "PlayerEntity")
        
        --First time:
        if(iSXJoinedInNCPlus == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSXJoinedInNCPlus", "N", 1.0)
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSXIsInMinesParty", "N", 1.0)
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
            fnCutscene([[ Append("SX-399:[E|Happy] Christine![P] Tiffany![P] Mind if I tag along in the mines?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Is that wise?[P] Didn't your mother assign you to train with the steam droid squads?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] This is an imposter.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Blush] (Taff me that didn't last long!)[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] Her chassis is damaged and worn, her weapons are nothing like what the steam droids have reported, and possibly the most egregious, she called me Tiffany.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Blush] Heh, heh, uhhhh...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Come on, out with it.[P] What's going on?[P] Who are you?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Okay, fine.[P] I'm SX-399, yes, and it's true my body is a bit scuffed and my weapons are different.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] I really don't want to tell you too much because I'm pretty sure I'm in the past.[P] I was fighting something in the mines and I think I fell through a time portal.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] This means I have the same stats and equipment and stuff from the previous playthrough.[P] New Chapter Plus and all that.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Oh yeah that seems totally reasonable.[P] I've been noticing some oddities myself.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] I see.[P] I still do not like it.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] However, if I were to ply you for future knowledge...[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] Don't worry, I already thought of that![B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] I can't even tell you, because so far, a bunch of little things have been different.[P] I'm not sure 'my' future is the one that's going to happen.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] I was thinking I might be able to slip in and help you in the mines, not interrupt things too much, and you wouldn't even know.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] We'd love to have you along, but we'll have to just pretend you're not there.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] And make sure you are scarce when we leave the mines.[P] I have no idea what will happen if you encounter yourself.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] You got it, boss.[P] I'll cover you![B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Happy] Gee whiz, this is like a dream come true![P] I always wanted to be right there with you guys busting baddies in the mines![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] It'll be nice to have you![P] And come back here if you do need to leave.[P] We'll pick you back up.[P] Let's go!") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Change collision flag and dialogue on SX-399.
            EM_PushEntity("SX-399")
                TA_SetProperty("Clipping Flag", false)
                TA_SetProperty("Activation Script", "Null")
            DL_PopActiveObject()
            
            --Field Party.
            EM_PushEntity("SX-399")
                local iCharacterID = RE_GetID()
            DL_PopActiveObject()
            table.insert(gsaFollowerNames, "SX-399")
            table.insert(giaFollowerIDs,   iCharacterID)
            giFollowersTotal = #gsaFollowerNames
            AL_SetProperty("Follow Actor ID", iCharacterID)
            
            --Combat Party.
            for i = 0, 3, 1 do
                if(AdvCombat_GetProperty("Name of Active Member", i) == "Null") then
                    AdvCombat_SetProperty("Party Slot", i, "SX-399")
                    break
                end
            end
            
            --Fold.
            fnAutoFoldParty()
            fnCutsceneBlocker()
            
            --Last save point defaults to the Tellurium Mines B which will cause SX-399 to leave if the player loses a battle.
            AL_SetProperty("Last Save Point", "TelluriumMinesB")
            
        --Repeats.
        else
        
            --Flags.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSXIsInMinesParty", "N", 1.0)
        
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
            fnCutscene([[ Append("SX-399:[E|Happy] Let's go kick some monster butt!") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Change collision flag and dialogue on SX-399.
            EM_PushEntity("SX-399")
                TA_SetProperty("Clipping Flag", false)
                TA_SetProperty("Activation Script", "Null")
            DL_PopActiveObject()
            
            --Field Party.
            EM_PushEntity("SX-399")
                local iCharacterID = RE_GetID()
            DL_PopActiveObject()
            table.insert(gsaFollowerNames, "SX-399")
            table.insert(giaFollowerIDs,   iCharacterID)
            giFollowersTotal = #gsaFollowerNames
            AL_SetProperty("Follow Actor ID", iCharacterID)
            
            --Combat Party.
            for i = 0, 3, 1 do
                if(AdvCombat_GetProperty("Name of Active Member", i) == "Null") then
                    AdvCombat_SetProperty("Party Slot", i, "SX-399")
                    break
                end
            end
            
            --Fold.
            fnAutoFoldParty()
            fnCutsceneBlocker()
            
            --Last save point defaults to the Tellurium Mines B which will cause SX-399 to leave if the player loses a battle.
            AL_SetProperty("Last Save Point", "TelluriumMinesB")
        end
    end
end
