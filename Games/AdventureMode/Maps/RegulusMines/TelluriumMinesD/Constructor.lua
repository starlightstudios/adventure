-- |[Constructor]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "TelluriumMinesD"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "RegulusTense")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("TelluriumMinesD")
	
	-- |[Spawn NPCS Here]|
	--NPCs set to spawn here will be available for post-exec cutscenes.
    
    -- |[Lights]|
    AL_SetProperty("Activate Lights")

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[Spawn NPCs Here]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
	local iSawMinesDCutscene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMinesDCutscene", "N")
	if(iSawMinesDCutscene == 0.0) then
		fnSpecialCharacter("SX-399", 33, 10, gci_Face_North, false, nil)
	end
	
	--Disable this layer if 55 has opened the entrance.
	local iSawMinesDCutsceneB = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMinesDCutsceneB", "N")
	if(iSawMinesDCutsceneB == 1.0) then
		AL_SetProperty("Set Layer Disabled", "Floor2Fake", true)
	end
    
    -- |[SX-399 in NC+]|
    --SX-399 must be over level 0 and the player must have completed Sprocket City. If they have then she will
    -- spawn and be a recruitable party member.
    if(AdvCombat_GetProperty("Does Party Member Exist", "SX-399") == false) then return end
    AdvCombat_SetProperty("Push Party Member", "SX-399")
        local iLevel = AdvCombatEntity_GetProperty("Level")
    DL_PopActiveObject()
    
    --Sprocket City completion state.
    local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
    
    --Checks passed:
    if(iLevel > 0 and iSXUpgradeQuest >= 4) then
		fnSpecialCharacter("SX-399", 27, 7, gci_Face_South, true, fnResolvePath() .. "Dialogue.lua")
    end
end
