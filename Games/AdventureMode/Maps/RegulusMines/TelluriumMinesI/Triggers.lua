-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Start the mines cutscene.
if(sObjectName == "StartCutscene") then

    --Flag.
    local iMetBoss50 = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetBoss50", "N") 
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMetBoss50", "N", 1.0)
    
    --Achievement.
    AM_SetPropertyJournal("Unlock Achievement", "FinishMines")

	--Variables.
	local iDefeatedBoss50 = VM_GetVar("Root/Variables/Chapter5/Scenes/iDefeatedBoss50", "N")
    local iHasDollForm    = VM_GetVar("Root/Variables/Global/Christine/iHasDollForm", "N")
	if(iDefeatedBoss50 == 1.0) then return end
    
    --Movement.
    fnCutsceneMove("Christine", 13.75, 12.50)
    fnCutsceneFace("Christine", 0, -1)
    fnCutsceneMove("Tiffany", 14.75, 12.50)
    fnCutsceneFace("Tiffany", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "609144", "Neutral") ]])
    fnCutscene([[ Append("609144:[E|Neutral] Christine.[B][C]") ]])
    if(iHasDollForm == 0.0) then
        fnCutscene([[ Append("Christine:[E|Neutral] Unit 609144.[P] I'm here.[B][C]") ]])
    else
        fnCutscene([[ Append("Christine:[E|Neutral] Unit 609144.[P] I'm here.[P] Again.[B][C]") ]])
    end
    if(iMetBoss50 == 0.0) then
        fnCutscene([[ Append("55:[E|Neutral] Do you know this unit from somewhere?[B][C]") ]])
        fnCutscene([[ Append("609144:[E|Neutral] Oh, she and I?[P] We go way forward.[B][C]") ]])
        fnCutscene([[ Append("609144:[E|Neutral] Nice to see you again for the first time as well, Unit 2855.[P] I suppose you're here to finish the job?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I do not know you.[P] I wiped my memories some time ago.[B][C]") ]])
        fnCutscene([[ Append("609144:[E|Neutral] I like that.[P] Fresh slate.[P] Clean it all off, ready to be degraded again just like it was before.[B][C]") ]])
        fnCutscene([[ Append("609144:[E|Neutral] I've made some improvements since last we met.[P] What do you think?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] What are you?[B][C]") ]])
        fnCutscene([[ Append("609144:[E|Neutral] I am me.[P] I made some improvements.[P] What do you think?[P] Ha ha ha![B][C]") ]])
        fnCutscene([[ Append("609144:[E|Neutral] Our bodies ought to be as malleable as our minds, ought they not?[P] Ought not ought not![P] Ha ha ha ha ha ha ha ha![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Unit 609144...[P] was the researcher in charge of the Cryogenics Facility.[B][C]") ]])
        fnCutscene([[ Append("609144:[E|Neutral] One of several, but yes.[P] I was in charge of the last specimen we held there.[P] The first, the last, the most, the least.[P] Such a thing, that one![B][C]") ]])
        fnCutscene([[ Append("609144:[E|Neutral] There are many things one can learn when they embrace alternate ways of thinking.[P] Of doing, of being.[P] Ing ing ing.[P] Ha ha ha![B][C]") ]])
        fnCutscene([[ Append("609144:[E|Neutral] So you're going to kill me.[P] I know.[P] You always have, you always will.[B][C]") ]])
        fnCutscene([[ Append("609144:[E|Neutral] We'll meet again, though.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] In the future, yes.[P] I won't know you then.[B][C]") ]])
        fnCutscene([[ Append("609144:[E|Neutral] Can we really say we know each other now?[P] Can we ever?[B][C]") ]])
        fnCutscene([[ Append("609144:[E|Neutral] To plumb the secrets of someone's mind, now that's real madness![P] All the things they keep from themselves, least of all others.[B][C]") ]])
        fnCutscene([[ Append("609144:[E|Neutral] Do you know what is below here, Christine?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] We're not interested.[B][C]") ]])
        fnCutscene([[ Append("609144:[E|Neutral] The beating heart of a dead world.[P] It's far down, very far, so far that if you fell you'd never reach the bottom.[P] But it's down there.[B][C]") ]])
        fnCutscene([[ Append("609144:[E|Neutral] I can hear it beating.[P] Thump thump thump![P] Ha ha![P] It never stops![B][C]") ]])
        fnCutscene([[ Append("609144:[E|Neutral] She showed me![P] She made me see it![P] It's going to come through and up and all of us will be devoured by it, incorporated into it![B][C]") ]])
        fnCutscene([[ Append("609144:[E|Neutral] The flesh will make us whole and new and all one big thing![P] One, big, thing![P] Gazing into infinity![B][C]") ]])
        fnCutscene([[ Append("609144:[E|Neutral] Dead for eternity![P] The flesh will bleed and we will join it![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I know.[P] I've seen it.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] But we still fight.[B][C]") ]])
        fnCutscene([[ Append("609144:[E|Neutral] *Sigh*[P] I have not yet finished reading my book.[P] That's because I haven't finished writing it, of course.[P] That will happen in ten billion years.[B][C]") ]])
        fnCutscene([[ Append("609144:[E|Neutral] Come, Christine.[P] Finish what you have already done.[P] There is only now, no past or future.[P] Now now now![P] Do it!") ]])
    else
        fnCutscene([[ Append("609144:[E|Neutral] Nice to see you again for the first time as well, Unit 2855.[P] I suppose you're here to finish the job?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] Have you already forgotten our previous encounter?[B][C]") ]])
        fnCutscene([[ Append("609144:[E|Neutral] Why are you so anchored to the now?[P] Things will be different soon, go there, do that.[B][C]") ]])
        fnCutscene([[ Append("609144:[E|Neutral] If you lost the last one then I think you win this one, and the next one, but not the one after that.[P] I don't know if I should try harder than usual.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] I'm sorry we have to fight again.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] I'm not.") ]])
    end
    fnCutsceneBlocker()
	
	-- |[Battle]|
	--Trigger a fight here.
    fnCutscene([[ AdvCombat_SetProperty("Next Combat Music", "RottenTheme", 0.0000) ]])
    fnCutscene([[ AdvCombat_SetProperty("World Pulse", true) ]])
    fnCutscene([[ AdvCombat_SetProperty("Reinitialize") ]])
    fnCutscene([[ AdvCombat_SetProperty("Activate") ]])
    fnCutscene([[ AdvCombat_SetProperty("Unretreatable", true) ]])
    fnCutscene([[ AdvCombat_SetProperty("Victory Script", gsRoot .. "Maps/RegulusMines/TelluriumMinesI/Combat_Victory.lua") ]])
    fnCutscene([[ AdvCombat_SetProperty("Defeat Script", gsStandardGameOver) ]])
    fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 5/000 Scripted Enemies/Boss 609144.lua") ]])
    fnCutsceneBlocker()
end
