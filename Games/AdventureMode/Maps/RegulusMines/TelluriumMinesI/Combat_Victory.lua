-- |[Combat Victory]|
--The party won!

--Flags.
VM_SetVar("Root/Variables/Chapter5/Scenes/iDefeatedBoss50", "N", 1.0)

--Music change.
AudioManager_PlayMusic("Null")

--Wait.
fnCutsceneWait(125)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "609144", "Neutral") ]])
fnCutscene([[ Append("609144:[E|Neutral] It is as it was, as it was it shall be.[B][C]") ]])
fnCutscene([[ Append("609144:[E|Neutral] I go now to meet the dead heart.[P] Farewell, Christine.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Walks over the pit.
fnCutsceneMoveFace("609144", 14.25, 8.50, 0, 1, 0.25)
fnCutsceneBlocker()
fnCutsceneWait(15)
fnCutsceneBlocker()
fnCutscene([[ AL_SetProperty("Set Layer Disabled", "RemoveOverlay", false) ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutscene([[ AudioManager_PlaySound("Combat|Impact_Pierce") ]])
for i = 1, 5, 1 do
    fnCutsceneTeleport("609144", 14.25, 8.50 + (i * 0.40))
    fnCutsceneWait(1)
    fnCutsceneBlocker()
end
fnCutsceneTeleport("609144", -100, -100)
fnCutsceneBlocker()
fnCutscene([[ AL_SetProperty("Set Layer Disabled", "RemoveOverlay", true) ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutscene([[ Append("Christine:[E|Neutral] So that's it.[P] Done.[P] Nothing here for us.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] She left a weapon.[P] Odd.[P] She wasn't using it.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] It's not her weapon, it's mine...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] Future memories are good for something, at least.[B][C]") ]])
fnCutscene([[ Append("Narrator: [SOUND|World|TakeItem](Received Tellurine Electrospear.)[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] But there isn't anything else we can do. Not here.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Drop a bomb down this shaft?[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Offended] You can't kill it.[P] It's like killing a building.[B][C]") ]])
fnCutscene([[ Append("55:[E|Smug] A bomb would kill a building.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Offended] Then killing a star.[P] It can't be done.[P] There's nothing to kill.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Then to what end did we come down here?[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] None, I guess.[P] None at all.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] This already happened.[P] What will happen is already set.[P] Nothing we can do can change that.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] And you are certain of this?[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] I know it in my bones...[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Good.[P] You are wrong so often that I have subcategorized it into 'normal' wrong and 'confrontationally' wrong.[P] There is therefore nothing to worry about.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] ...[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Regardless, this mission is complete.[P] Unit 771852, we have other assignments.[P] Move out.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] Yeah.[P] Unit 771852 following orders...") ]])
fnCutsceneBlocker()

--Gain an Electrospear.
LM_ExecuteScript(gsItemListing, "Tellurine Electrospear")

--Fold the party.
fnAutoFoldParty()
