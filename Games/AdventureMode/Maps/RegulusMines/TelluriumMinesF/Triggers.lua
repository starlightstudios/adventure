-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[ ======================================== Triggers ======================================== ]|
--Start the mines cutscene.
if(sObjectName == "StartCutscene") then
    
    --Variables.
    local iSawMinesFCutscene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMinesFCutscene", "N")
    if(iSawMinesFCutscene == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMinesFCutscene", "N", 1.0)
    
    --Scene.
    fnCutsceneMove("Christine", 16.25, 6.50)
    fnCutsceneMove("Tiffany", 17.25, 6.50)
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", -1, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Hauntingly beautiful...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 55, what is this stuff?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] I believe this is what's classified as Empherean Fungus.[P] A genetics experiment conducted to improve mining efficiency.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] The fungus grows its roots into the pores of the rock and extracts valuable minerals like a sponge.[P] It then transports them to an exposed cavity for collection.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Unfortunately the fungus grew too slowly to be of practical use and it was discontinued.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Amazing...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] So why is it here?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Likely a strain was released here for testing and the administrators decided cleaning it up was not an efficient use of resources.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] ...[P] Isn't the green glow beautiful?[P] Our own underground grotto...[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] That is likely the radioactive elements the fungus metabolizes to maintain biological functions.[P] There is precious little food energy available otherwise.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] This stuff is radioactive!?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Remember to have yourself scrubbed down when we reach the surface.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Tiffany", 16.25, 6.50)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Fold Party") ]])
    fnCutsceneBlocker()
end
