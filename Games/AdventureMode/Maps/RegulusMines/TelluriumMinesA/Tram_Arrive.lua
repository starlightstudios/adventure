-- |[Tram Arrival]|
--Called when the player is arriving via tram.
fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()
	
--Set flags.
VM_SetVar("Root/Variables/Chapter5/Scenes/iIsTramHere", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/sCurrentTramArea", "S", "TelluriumMinesA")

--Spawn the tram parts.
TA_Create("TramA")
	TA_SetProperty("Position", 33, 6)
	TA_SetProperty("Move Frame", gci_Face_West, 0, "Root/Images/Sprites/Tram/West|0")
	TA_SetProperty("Move Frame", gci_Face_West, 1, "Root/Images/Sprites/Tram/West|0")
	TA_SetProperty("Move Frame", gci_Face_West, 2, "Root/Images/Sprites/Tram/West|0")
	TA_SetProperty("Move Frame", gci_Face_West, 3, "Root/Images/Sprites/Tram/West|0")
	TA_SetProperty("Facing", gci_Face_West)
	TA_SetProperty("Set Ignore Clips", true)
DL_PopActiveObject()
TA_Create("TramB")
	TA_SetProperty("Position", 34, 6)
	TA_SetProperty("Move Frame", gci_Face_West, 0, "Root/Images/Sprites/Tram/North|0")
	TA_SetProperty("Move Frame", gci_Face_West, 1, "Root/Images/Sprites/Tram/North|0")
	TA_SetProperty("Move Frame", gci_Face_West, 2, "Root/Images/Sprites/Tram/North|0")
	TA_SetProperty("Move Frame", gci_Face_West, 3, "Root/Images/Sprites/Tram/North|0")
	TA_SetProperty("Facing", gci_Face_West)
	TA_SetProperty("Set Ignore Clips", true)
DL_PopActiveObject()
TA_Create("TramC")
	TA_SetProperty("Position", 35, 6)
	TA_SetProperty("Move Frame", gci_Face_West, 0, "Root/Images/Sprites/Tram/South|0")
	TA_SetProperty("Move Frame", gci_Face_West, 1, "Root/Images/Sprites/Tram/South|0")
	TA_SetProperty("Move Frame", gci_Face_West, 2, "Root/Images/Sprites/Tram/South|0")
	TA_SetProperty("Move Frame", gci_Face_West, 3, "Root/Images/Sprites/Tram/South|0")
	TA_SetProperty("Move Frame", gci_Face_East, 0, "Root/Images/Sprites/Tram/South|1")
	TA_SetProperty("Move Frame", gci_Face_East, 1, "Root/Images/Sprites/Tram/South|1")
	TA_SetProperty("Move Frame", gci_Face_East, 2, "Root/Images/Sprites/Tram/South|1")
	TA_SetProperty("Move Frame", gci_Face_East, 3, "Root/Images/Sprites/Tram/South|1")
	TA_SetProperty("Facing", gci_Face_West)
	TA_SetProperty("Set Ignore Clips", true)
    TA_SetProperty("Activation Script", gsRoot .. "Chapter 5/Dialogue/Tram/Root.lua")
DL_PopActiveObject()
TA_Create("TramD")
	TA_SetProperty("Position", 36, 6)
	TA_SetProperty("Move Frame", gci_Face_West, 0, "Root/Images/Sprites/Tram/North|1")
	TA_SetProperty("Move Frame", gci_Face_West, 1, "Root/Images/Sprites/Tram/North|1")
	TA_SetProperty("Move Frame", gci_Face_West, 2, "Root/Images/Sprites/Tram/North|1")
	TA_SetProperty("Move Frame", gci_Face_West, 3, "Root/Images/Sprites/Tram/North|1")
	TA_SetProperty("Facing", gci_Face_West)
	TA_SetProperty("Set Ignore Clips", true)
DL_PopActiveObject()
TA_Create("TramE")
	TA_SetProperty("Position", 37, 6)
	TA_SetProperty("Move Frame", gci_Face_West, 0, "Root/Images/Sprites/Tram/North|2")
	TA_SetProperty("Move Frame", gci_Face_West, 1, "Root/Images/Sprites/Tram/North|2")
	TA_SetProperty("Move Frame", gci_Face_West, 2, "Root/Images/Sprites/Tram/North|2")
	TA_SetProperty("Move Frame", gci_Face_West, 3, "Root/Images/Sprites/Tram/North|2")
	TA_SetProperty("Facing", gci_Face_West)
	TA_SetProperty("Set Ignore Clips", true)
DL_PopActiveObject()
TA_Create("TramF")
	TA_SetProperty("Position", 38, 6)
	TA_SetProperty("Move Frame", gci_Face_West, 0, "Root/Images/Sprites/Tram/North|3")
	TA_SetProperty("Move Frame", gci_Face_West, 1, "Root/Images/Sprites/Tram/North|3")
	TA_SetProperty("Move Frame", gci_Face_West, 2, "Root/Images/Sprites/Tram/North|3")
	TA_SetProperty("Move Frame", gci_Face_West, 3, "Root/Images/Sprites/Tram/North|3")
	TA_SetProperty("Facing", gci_Face_West)
	TA_SetProperty("Set Ignore Clips", true)
DL_PopActiveObject()

--Wait a bit.
fnCutsceneWait(125)
fnCutsceneBlocker()

--Teleport Christine offscreen. Also remove 55 if she's present.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 5.0) --Default is 5.0
	CameraEvent_SetProperty("Focus Position", (19.25 * gciSizePerTile), (7.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneTeleport("Christine", -100.25, -100.50)
fnCutsceneTeleport("Tiffany", -100.25, -100.50)
fnCutsceneBlocker()

--Fade in.
fnCutsceneWait(45)
fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()
		
--Move the trams over. They all forcible face west while moving.
fnCutsceneMoveFace("TramA", 17.25 + 6.0, 6.50, -1, 0, 2.50)
fnCutsceneMoveFace("TramB", 18.25 + 6.0, 6.50, -1, 0, 2.50)
fnCutsceneMoveFace("TramC", 19.25 + 6.0, 6.50, -1, 0, 2.50)
fnCutsceneMoveFace("TramD", 20.25 + 6.0, 6.50, -1, 0, 2.50)
fnCutsceneMoveFace("TramE", 21.25 + 6.0, 6.50, -1, 0, 2.50)
fnCutsceneMoveFace("TramF", 22.25 + 6.0, 6.50, -1, 0, 2.50)
fnCutsceneBlocker()
fnCutscene([[ AudioManager_PlaySound("World|TramStop") ]])
fnCutsceneMoveFace("TramA", 17.25 + 3.0, 6.50, -1, 0, 1.00)
fnCutsceneMoveFace("TramB", 18.25 + 3.0, 6.50, -1, 0, 1.00)
fnCutsceneMoveFace("TramC", 19.25 + 3.0, 6.50, -1, 0, 1.00)
fnCutsceneMoveFace("TramD", 20.25 + 3.0, 6.50, -1, 0, 1.00)
fnCutsceneMoveFace("TramE", 21.25 + 3.0, 6.50, -1, 0, 1.00)
fnCutsceneMoveFace("TramF", 22.25 + 3.0, 6.50, -1, 0, 1.00)
fnCutsceneBlocker()

--Trams slow down.
fnCutsceneMoveFace("TramA", 17.25 + 0.0, 6.50, -1, 0, 0.50)
fnCutsceneMoveFace("TramB", 18.25 + 0.0, 6.50, -1, 0, 0.50)
fnCutsceneMoveFace("TramC", 19.25 + 0.0, 6.50, -1, 0, 0.50)
fnCutsceneMoveFace("TramD", 20.25 + 0.0, 6.50, -1, 0, 0.50)
fnCutsceneMoveFace("TramE", 21.25 + 0.0, 6.50, -1, 0, 0.50)
fnCutsceneMoveFace("TramF", 22.25 + 0.0, 6.50, -1, 0, 0.50)
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("Voice:[VOICE|Golem] Now arriving at Regulus City, Sector 73, Tellurium Mines.[P] Have a stultifyingly safe day.") ]])
fnCutsceneBlocker()

--Door opens.
fnCutscene([[ AudioManager_PlaySound("World|TramArrive") ]])
fnCutsceneFace("TramC", 1, 0)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Christine appears.
fnCutsceneFace("Christine", 0, 1)
fnCutsceneTeleport("Christine", 19.25, 7.50)
fnCutsceneFace("Tiffany", 0, 1)
fnCutsceneTeleport("Tiffany", 19.25, 7.50)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Move her south a bit.
fnCutsceneMove("Christine", 19.25, 8.50)
fnCutsceneMove("Tiffany", 19.25, 8.50)
fnCutsceneBlocker()

--Fold the party.
fnCutscene([[ AL_SetProperty("Fold Party") ]])
fnCutsceneBlocker()
