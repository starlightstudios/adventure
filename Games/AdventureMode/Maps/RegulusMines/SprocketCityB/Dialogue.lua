-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[DroidBA]|
    if(sActorName == "DroidBA") then
	
        --Variables.
        local iSaw319 = VM_GetVar("Root/Variables/Chapter5/Scenes/iSaw319", "N")
        
        --First time.
        if(iSaw319 == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSaw319", "N", 1.0)
            
            --Not done Sprocket city, get reputation.
            local iCompletedSprocketCity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSprocketCity", "N")
            if(iCompletedSprocketCity == 0.0) then
                local iSteamDroidReputation = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N")
                VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N", iSteamDroidReputation + 25)
            
            --Work credits.
            else
                local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
                VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + 12)
            end
            
            --Dialogue.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] I swear I had a 3-19 Inserter around here somewhere...[P] but I can't find it anywhere![P] Argh![B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] A 3-19 Inserter...[P] like the one in your hand?[B][C]") ]])
            fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] ...[B][C]") ]])
            fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] I'm such a lug nut.[P] I guess my circuits are getting frazzled.[P] Thanks, human.[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] Don't mention it.[P] Hee hee![B][C]") ]])
            if(iCompletedSprocketCity == 0.0) then
                fnCutscene([[ Append("Thought:[VOICE|Leader] (Gained 25 Reputation!)") ]])
            else
                fnCutscene([[ Append("Thought:[VOICE|Leader] (Gained 12 Work Credits!)") ]])
            end
            fnCutsceneBlocker()
            
        --Repeats.
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] I'm trying to take inventory of all this junk. It's going to take me all week at this rate.") ]])
            fnCutsceneBlocker()
        end
    
    -- |[DroidBB]|
    elseif(sActorName == "DroidBB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] We keep any organic food we find, just in case we have organic guests.[P] It doesn't go bad either, since most of it has been lightly irradiated from surface exposure.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Is it safe to eat if it's irradiated?[B][C]") ]])
        fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] The radiation doesn't stick around for long, but it does kill all the bacteria in the food.[P] So, it stays good unless you expose it to more bacteria.[P] It's fine to eat.") ]])
        fnCutsceneBlocker()
    
    -- |[DroidBC]|
    elseif(sActorName == "DroidBC") then
    
        --Variables.
        local iSteamDroidReputation  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N")
        local iPlanHatched = VM_GetVar("Root/Variables/Chapter5/Scenes/iPlanHatched", "N")
        if(iPlanHatched >= 1.0 and iSteamDroidReputation >= 500) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] First a Command Unit, and now a Lord Unit.[P] We've been getting a lot of prisoners lately...") ]])
            fnCutsceneBlocker()
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] That Command Unit has been in system standby since she got here.[P] I'm not sure she'll ever wake up...") ]])
            fnCutsceneBlocker()
        end
        
    -- |[DroidBD]|
    elseif(sActorName == "DroidBD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Steam Droid:[VOICE|Steam Droid] We don't actually have a prison, this is a storage room.[P] Try not to knock over any of the boxes.") ]])
        fnCutsceneBlocker()
        
    -- |[LordUnit]|
    elseif(sActorName == "LordUnit") then
        
        --Common
        TA_SetProperty("Face Character", "PlayerEntity")
        
        --Variables.
        local iSXUpgradeQuest        = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
        local sChristineForm         = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
        
        --Upgrade quest.
        if(iSXUpgradeQuest == 2.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("JX-101:[VOICE|JX-101] Don't bother the prisoner, sweetie.[P] She'll probably just insult you.") ]])
            return
        end
            
        -- |[Human]|
        if(sChristineForm == "Human") then
        
            --Form variables.
            local iSpokeSprocketLordUnitHuman = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitHuman", "N")
        
            --First time.
            if(iSpokeSprocketLordUnitHuman == 0.0) then
                
                --Flag
                VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitHuman", "N", 1.0)
                
                --Dialogue.
                fnStandardMajorDialogue()
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLordB", "Neutral") ]])
                fnCutscene([[ Append("Golem: And what do we have here?[P] A human, in the mines?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Are you well, Lord Unit?[B][C]") ]])
                fnCutscene([[ Append("Golem: Most curious.[P] Not only are you mixing with this rabble, but you are well spoken.[B][C]") ]])
                fnCutscene([[ Append("Golem: I must protest these conditions of captivity.[P] You have failed to provide me with a means of recharging myself, and my core is dangerously depleted.[B][C]") ]])
                fnCutscene([[ Append("Golem: See to it that my needs are seen to post-haste, human![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] I'll speak to the guards.[P] One moment.") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(25)
                fnCutsceneBlocker()
                
                --Movement.
                fnCutsceneMove("Christine", 25.25, 6.50)
                fnCutsceneMove("Christine", 25.25, 8.50)
                fnCutsceneBlocker()
                fnCutsceneWait(25)
                fnCutsceneBlocker()
                fnCutsceneFace("Christine", -1, 0)
                fnCutsceneFace("DroidBC", 1, 0)
                fnCutsceneWait(25)
                fnCutsceneBlocker()
                
                --Dialogue.
                fnStandardMajorDialogue()
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Pardon me, but have you allocated any energy rations to this prisoner?[B][C]") ]])
                fnCutscene([[ Append("Steam Droid: Energy rations?[P] No.[P] We don't even know what to give her, and she shouts at us when we ask.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Golem power cores can metabolize nearly any material with a matter-energy conversion.[P] Almost anything will work, but carbohydrates are ideal as the process is still subject to entropy.[B][C]") ]])
                fnCutscene([[ Append("Steam Droid: ...[P] Come again?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Oil.[P] Get her one liter of oil every two days.[P] That should be enough.[B][C]") ]])
                fnCutscene([[ Append("Steam Droid: Oh, sure thing.[P] You just tell her not to be so damn finicky and we'll get her the oil.[B][C]") ]])
                fnCutscene([[ Append("Steam Droid: And -[P] Christine, don't let the prisoner hold you hostage like last time.[P] Okay?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Laugh] Deal.") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(25)
                fnCutsceneBlocker()
                
                --Movement.
                fnCutsceneMove("Christine", 25.25, 5.50)
                fnCutsceneBlocker()
                fnCutsceneFace("LordUnit", 0, 1)
                fnCutsceneWait(25)
                fnCutsceneBlocker()
                
                --Dialogue.
                fnStandardMajorDialogue()
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLordB", "Neutral") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] I've made arrangements for you.[P] You should be taken care of now.[B][C]") ]])
                fnCutscene([[ Append("Golem: Tch, if you think you can earn favour with me, you are mistaken.[B][C]") ]])
                fnCutscene([[ Append("Golem: When, not if, [P]*when*[P], I return to Regulus City, I will send a team to have all of your friends retired.[P] You, of course, I will personally convert.[B][C]") ]])
                fnCutscene([[ Append("Golem: Let's see...[P] I think you would do well as my dedicated cleaning Slave Unit.[P] Should I sully myself in the course of my duties, it will be your task to clean my chassis.[B][C]") ]])
                fnCutscene([[ Append("Golem: ...[P][P] With your tongue.[B][C]") ]])
                fnCutscene([[ Append("Golem: What do you think of that, human?[BLOCK]") ]])

                --Decision script is this script. It must be surrounded by quotes.
                local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
                fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Kinky\", " .. sDecisionScript .. ", \"Kinky\") ")
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sod Off\",  " .. sDecisionScript .. ", \"Sod Off\") ")
                fnCutsceneBlocker()
            
            --Repeat conversations.
            else
                
                --Dialogue.
                fnStandardMajorDialogue()
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLordB", "Neutral") ]])
                fnCutscene([[ Append("Golem: If you think you can earn yourself favour with me by showing concern, you are mistaken.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] And if I show you concern for no reason?[B][C]") ]])
                fnCutscene([[ Append("Golem: Then I will commend you on such a devious trick.[P] And, I assure you, it will fail.[P] Good day, human.") ]])
                fnCutsceneBlocker()
            
            end
        
        -- |[Golem]|
        elseif(sChristineForm == "Golem") then
        
            --Form variables.
            local iSpokeSprocketLordUnitGolem = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitGolem", "N")
        
            --First time.
            if(iSpokeSprocketLordUnitGolem == 0.0) then
                
                --Flag
                VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitGolem", "N", 1.0)
                
                --Dialogue.
                fnStandardMajorDialogue()
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLordB", "Neutral") ]])
                fnCutscene([[ Append("Golem: Oh, a Lord Unit.[P] A liberator, or fellow prisoner?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Neither.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Jailor.[B][C]") ]])
                fnCutscene([[ Append("Golem: Explain yourself.[P] Promptly.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Unit 2855 and I have conspired to put you here.[P] We're working with the Steam Droids.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] More importantly, there's nothing you can do about it.[B][C]") ]])
                fnCutscene([[ Append("Golem: Unit 2855?[P] The missing Prime Command Unit?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smirk] The same.[P] You didn't see my face when I was caving in your cranial chassis.[B][C]") ]])
                fnCutscene([[ Append("Golem: And a Lord Golem has turned traitor...[P] Do you realize what you're turning your back on?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Please, do tell.[P] Tell me what about Regulus City is so wonderful that you'd never betray it.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Is it, perhaps, the Black Sites where undesirables are disappeared and tortured on?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Maybe it's the thousands of Slave Units who are worked to the breaking point and denied any sort of self-determination?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Oh, or maybe it's the utopia we live in -[P] but only we Lord Units, and not the underclass who makes the utopia possible?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] We perfect machines who have transcended our flawed human bodies but not our flawed human social structures.[B][C]") ]])
                fnCutscene([[ Append("Golem: You must be young.[P] Young that you do not understand anything yet.[B][C]") ]])
                fnCutscene([[ Append("Golem: All of those things are crimes.[P] Yes, that is true and I will not deny it.[P] Some Lord Units will, I will not.[B][C]") ]])
                fnCutscene([[ Append("Golem: But what alternative do you propose?[P] Do you not realize that the so-called democratic societies of Pandemonium all ended in tyranny one way or the other?[B][C]") ]])
                fnCutscene([[ Append("Golem: Is not our carefully managed, computerized tyranny far greater than any they could design?[P] Even the lowly Slave Units have a standard of living far in excess of the peasants who toil Pandemonium's fields.[B][C]") ]])
                fnCutscene([[ Append("Golem: No, we bring so much that even the crumbs of utopia are superior to the whole loaf of desperate poverty.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] That's almost a good argument -[P] but it's really apology.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] You came up with that excuse because you happen to be the one who benefits from it.[P] If you had been a Slave Unit, would you really support your own oppression?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] You don't have to answer, because the thousands of Slave Units have already staked their position for you.[B][C]") ]])
                fnCutscene([[ Append("Golem: They are petty and put their personal positions ahead of the greater good.[P] The Cause of Science.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] And who asked them to do that?[P] Nobody, you forced them.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] I serve the cause loyally, and many of them would, too.[P] But you decided for them and made their lives a living hell.[B][C]") ]])
                fnCutscene([[ Append("Golem: In the end, they, I, none of us matters.[P] That I was allowed to serve the Cause in a capacity that offered me comforts is something I will not turn down.[P] It doesn't matter.[B][C]") ]])
                fnCutscene([[ Append("Golem: Advancing the Cause is what matters.[P] Central Administration is far wiser than we lowly golems, and that is the society we have been told advances it most.[P] You disagree, but you are wrong.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Then we'll agree to disagree, won't we?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Don't worry, soon our disagreements will be solved with pulse munitions instead of words.[B][C]") ]])
                fnCutscene([[ Append("Golem: You and the Steam Droids seek a civil war?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Seek?[P] Hardly.[P] We have no other choice.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] If you make peaceful revolution impossible, then violent revolution becomes inevitable.[B][C]") ]])
                fnCutscene([[ Append("Golem: Such a young fool.[P] I have no more words to waste on your aural receptors.[P] Be gone.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Gladly.[P] Enjoy your accommodations, Lord Unit, because they are anything but temporary.") ]])
                fnCutsceneBlocker()
            
            --Repeat conversations.
            else
                
                --Dialogue.
                fnStandardMajorDialogue()
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLordB", "Neutral") ]])
                fnCutscene([[ Append("Golem: I would talk some sense into you, but you are beyond that.[P] Hope that your revolution succeeds, because if it does not, I will come for you.[B][C]")  ]])
                fnCutscene([[ Append("Christine:[E|Offended] My ancestors did a lot of awful things, but one of the good things they did was opposing people like you.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] And you know what?[P] The authoritarians lost in the end.[P] They always do.[P] I can't wait to see the look on your face when I deliver you that news.") ]])
                fnCutsceneBlocker()
            
            end
        
        -- |[Steam Droid]|
        elseif(sChristineForm == "SteamDroid") then
        
            --Form variables.
            local iSpokeSprocketLordUnitSteamDroid = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitSteamDroid", "N")
        
            --First time.
            if(iSpokeSprocketLordUnitSteamDroid == 0.0) then
                
                --Flag
                VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitSteamDroid", "N", 1.0)
                
                --Dialogue.
                fnStandardMajorDialogue()
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLordB", "Neutral") ]])
                fnCutscene([[ Append("Golem: What do you want, obsolete?[P] It may not appear that way, but I am quite busy.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Doing what, stewing in a jail cell?[B][C]") ]])
                fnCutscene([[ Append("Golem: Organizing my hard drive, if you must know.[P] There are a number of philosophical problems I had yet to work through.[B][C]") ]])
                fnCutscene([[ Append("Golem: Oh, a hard drive is the storage medium we golems use for long-term data storage.[P] Not as fast to access but the sheer amount of data we can store is much higher.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] You think you're funny, but you're not.[P] I know what a hard drive is.[B][C]") ]])
                fnCutscene([[ Append("Golem: Oh, that's good.[P] I can explain any other technical terms you want, feel free to ask.[P] I'll even speak slowly so you can understand the complex parts.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] (Bully.[P] But I know how to deal with you.[P] Never let them take the reins in the conversation.)[B][C]") ]])
                fnCutscene([[ Append("Golem: By the way - [P][CLEAR]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] You talk an awful lot, for someone who's busy.[P] Are you lonely?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Do you want me to make a little Slave Golem mannequin for you to beat up on?[P] Would that make you feel more at home?[B][C]") ]])
                fnCutscene([[ Append("Golem: I - [P][CLEAR]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Or maybe a little Command Unit mannequin, to kiss the feet of?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Oh, I know![P] Maybe a Steam Droid mannequin, who can also throw you in jail?[B][C]") ]])
                fnCutscene([[ Append("Golem: You -[P] impertinent - [P][CLEAR]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Gosh, how must it feel to be outsmarted and captured by inferior units?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] After all, our weak steam-powered frames couldn't defeat you in a fair fight.[P] We'd have to outsmart you.[B][C]") ]])
                fnCutscene([[ Append("Golem: I was ambushed by a Command Unit, though I did not see her face.[P] I assume she is the one behind you.[LOCK][CLEAR]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Quite right, she's working with us.[P] See, outsmarting you wasn't that hard was it?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smirk] No, it wasn't.[P] She was almost comically easy to take down.[B][C]") ]])
                fnCutscene([[ Append("Golem: Grrrr...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Hey, Command Unit friend of mine -[P] you know what we should do?[P] Something that this Lord Unit here is apparently too weak, clumsy, or scared to do?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Leave this cell.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] It's pretty easy for us, but she apparently can't do it.[B][C]") ]])
                fnCutscene([[ Append("Golem: You will regret the day you crossed me, obsolete droid![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] I doubt it.[P] In any case, that day is not today.[P] C'mon, Command Unit.[P] Let's go let her be superior all by her lonesome.") ]])
                fnCutsceneBlocker()
            
            --Repeat conversations.
            else
                
                --Dialogue.
                fnStandardMajorDialogue()
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLordB", "Neutral") ]])
                fnCutscene([[ Append("Golem: Stupid obsolete unit![P] Release me at once![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Sorry, I can't hear you over the sound of you being in prison.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] That statement did not make sense.[P] Being in prison doesn't make a sound.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Laugh] Are you sure?[P] Because she's making a real racket![P] Ha ha!") ]])
                fnCutsceneBlocker()
            
            end
        
        -- |[Darkmatter]|
        elseif(sChristineForm == "Darkmatter") then
        
            --Form variables.
            local iSpokeSprocketLordUnitDarkmatter = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitDarkmatter", "N")
        
            --First time.
            if(iSpokeSprocketLordUnitDarkmatter == 0.0) then
                
                --Flag
                VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitDarkmatter", "N", 1.0)
                
                --Dialogue.
                fnStandardMajorDialogue()
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLordB", "Neutral") ]])
                fnCutscene([[ Append("Golem: A darkmatter, in here?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] (Heh, it's not like she can tell anyone we Darkmatters can talk.)[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Hello, Lord Unit.[B][C]") ]])
                fnCutscene([[ Append("Golem: You can speak!?[P] Incredible![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] All Darkmatters can speak.[P] We have a tremendous amount of knowledge to share with your kind.[B][C]") ]])
                fnCutscene([[ Append("Golem: Please, liberate me from this cell![P] Whatever you need, I can give it to you at Regulus City in exchange for your knowledge![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] You are being held here?[P] How?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Simply phase through the walls as my kind do.[P] Surely you can liberate yourself.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] My patience grows thin.[P] If you cannot perform even this simple task, then perhaps you are not worthy of our knowledge.[B][C]") ]])
                fnCutscene([[ Append("Golem: No![P] Please wait![P] I'll -[P] I can't -[P] No!") ]])
                fnCutsceneBlocker()
            
            --Repeat conversations.
            else
                
                --Dialogue.
                fnStandardMajorDialogue()
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLordB", "Neutral") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] (Better not say anything else.[P] She's pretty upset as it is.)") ]])
                fnCutsceneBlocker()
            
            end
        
        -- |[Electrosprite]|
        elseif(sChristineForm == "Electrosprite") then
        
            --Form variables.
            local iSpokeSprocketLordUnitElectrosprite = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitElectrosprite", "N")
        
            --First time.
            if(iSpokeSprocketLordUnitElectrosprite == 0.0) then
                
                --Flag
                VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitElectrosprite", "N", 1.0)
                
                --Dialogue.
                fnStandardMajorDialogue()
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLordB", "Neutral") ]])
                fnCutscene([[ Append("Golem: And just what sort of debased creature are you?[P] You're practically naked![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Like what you see, honey?[P] There's lots where that came from![B][C]") ]])
                fnCutscene([[ Append("Golem: To openly display yourself like this...[P] You sicken me.[P] Show some dignity.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] I'm sure it's very dignified to spit insults at anyone who comes by.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] You know what's really dignified?[P] Rising above it.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Hurl all the insults you can bear, Lord Unit.[P] You cannot harm me.[B][C]") ]])
                fnCutscene([[ Append("Golem: ...[P] I lost my composure.[P] Hrmph.[B][C]") ]])
                fnCutscene([[ Append("Golem: Begone, creature.[P] I have things to think about.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] (Annoying stuck-up ponces never gets old, does it?)") ]])
                fnCutsceneBlocker()
            
            --Repeat conversations.
            else
                
                --Dialogue.
                fnStandardMajorDialogue()
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLordB", "Neutral") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Have you an apology for me, Lord Golem?[B][C]") ]])
                fnCutscene([[ Append("Golem: It takes all the patience I have to hold my tongue, and my patience is in short supply.[P] Leave.") ]])
                fnCutsceneBlocker()
            
            end
        
        -- |[Raiju]|
        elseif(sChristineForm == "Raiju") then
        
            --Form variables.
            local iSpokeSprocketLordUnitRaiju = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitRaiju", "N")
        
            --First time.
            if(iSpokeSprocketLordUnitRaiju == 0.0) then
                
                --Flag
                VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitRaiju", "N", 1.0)
                
                --Dialogue.
                fnStandardMajorDialogue()
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLordB", "Neutral") ]])
                fnCutscene([[ Append("Golem: And what have we here.[P] A raiju gone maverick?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] Hello, I'm a big dumb stupid raiju and all I can think about is humping![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] You're cute![P] Want to have hot kinky sex with me?[P] All I want to do is kiss and screw all day![B][C]") ]])
                fnCutscene([[ Append("Golem: You're...[P] acting?[P] This is an act?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] Nope, bimbo shocky is here![P] Gimme a kiss![B][C]") ]])
                fnCutscene([[ Append("Golem: Oh my goodness I've been waiting for this...[B][C]") ]])
                fnCutscene([[ Append("Golem: [SOUND|World|SparksA]OUCH![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] Kissy kissy mwah![B][C]") ]])
                fnCutscene([[ Append("Golem: [SOUND|World|SparksB]ARGGGHHH, STOP![B][C]") ]])
                fnCutscene([[ Append("Golem: (It hurts so good...)[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Ooops, big titty dumb me is too excited![P] I zapped you![B][C]") ]])
                fnCutscene([[ Append("Golem: No, don't stop![P] Argh, why would you tease me?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] Sorry, gotta go back to kissing all the steamy droids and making them hot![P] They get hot to make steam![B][C]") ]])
                fnCutscene([[ Append("Golem: BUT I DIDN'T FINISH, NOOOOO!") ]])
                fnCutsceneBlocker()
            
            --Repeat conversations.
            else
                
                --Dialogue.
                fnStandardMajorDialogue()
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLordB", "Neutral") ]])
                fnCutscene([[ Append("Golem: Please, please, another kiss![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] Oh no, I have to go and rub myself all over the big strong steamy droids![P] Bye bye golem!") ]])
                fnCutsceneBlocker()
            end
        
        -- |[Latex Drone]|
        elseif(sChristineForm == "LatexDrone") then
        
            --Form variables.
            local iSpokeSprocketLordUnitLatexDrone = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitLatexDrone", "N")
        
            --First time.
            if(iSpokeSprocketLordUnitLatexDrone == 0.0) then
                
                --Flag
                VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitLatexDrone", "N", 1.0)
                
                --Dialogue.
                fnStandardMajorDialogue()
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLordB", "Neutral") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] HELLO, LORD UNIT.[P] THIS DRONE UNIT IS HERE TO SERVE YOU.[B][C]") ]])
                fnCutscene([[ Append("Golem: Hmm, were you captured, Drone?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] NEGATIVE.[P] THIS UNIT HAS ALLIED HERSELF WITH THE STEAM DROIDS.[B][C]") ]])
                fnCutscene([[ Append("Golem: But you say you're here to serve me?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] AFFIRMATIVE.[P] HOW CAN THIS UNIT MAKE YOU MORE COMFORTABLE?[B][C]") ]])
                fnCutscene([[ Append("Golem: Aid in my escape.[P] Distract the guards.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] AFFIRMATIVE.") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(25)
                fnCutsceneBlocker()
                
                --Movement.
                fnCutsceneMove("Christine", 25.25, 6.50)
                fnCutsceneMove("Christine", 25.25, 8.50)
                fnCutsceneBlocker()
                fnCutsceneWait(25)
                fnCutsceneBlocker()
                fnCutsceneFace("Christine", -1, 0)
                fnCutsceneFace("DroidBC", 1, 0)
                fnCutsceneWait(25)
                fnCutsceneBlocker()
                
                --Dialogue.
                fnStandardMajorDialogue()
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] EXCUSE ME, GUARD.[P] THIS UNIT WAS ORDERED BY THE PRISONER TO DISTRACT YOU.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] SHE IS OBVIOUSLY PLANNING AN ESCAPE.[P] KEEP AN EYE ON HER.[B][C]") ]])
                fnCutscene([[ Append("Steam Droid: You got it.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] THANK YOU FOR YOUR TIME, STEAM DROID FRIEND.[B][C]") ]])
                fnCutscene([[ Append("Steam Droid: *Is this part of a bit, Christine?*[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] *Yep.[P] Messing with the prisoner.*[B][C]") ]])
                fnCutscene([[ Append("Steam Droid: *Oh please don't, she never stops complaining as it is...*") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(25)
                fnCutsceneBlocker()
                
                --Movement.
                fnCutsceneMove("Christine", 25.25, 5.50)
                fnCutsceneBlocker()
                fnCutsceneFace("LordUnit", 0, 1)
                fnCutsceneWait(25)
                fnCutsceneBlocker()
                
                --Dialogue.
                fnStandardMajorDialogue()
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLordB", "Neutral") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] THE GUARDS HAVE BEEN INFORMED OF YOUR PLANS TO ESCAPE.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] THIS UNIT NOTES THAT TRICKING YOU WAS DISTRESSINGLY EASY.[B][C]") ]])
                fnCutscene([[ Append("Golem: You stupid drone![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Actually, I had my inhibitor turned off.[P] So, calling me stupid?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Laugh] Better look in the mirror first, right?[P] Ha ha ha ha![B][C]") ]])
                fnCutscene([[ Append("Golem: *growl*") ]])
                fnCutsceneBlocker()
            
            --Repeat conversations.
            else
                
                --Dialogue.
                fnStandardMajorDialogue()
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLordB", "Neutral") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] HELLO, LORD UNIT, HOW MAY THIS UNIT SERVE YOU?[B][C]") ]])
                fnCutscene([[ Append("Golem: You think I'm going to fall for the same trick twice?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Laugh] Given how you fell for the first one so hard?[P] Yes![P] Ha ha ha ha![B][C]") ]])
                fnCutscene([[ Append("Golem: *stupid dumb idiot drones...*") ]])
                fnCutsceneBlocker()
            
            end
        
        -- |[Doll]|
        elseif(sChristineForm == "Doll") then
        
            --Form variables.
            local iSpokeSprocketLordUnitDoll = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitDoll", "N")
        
            --First time.
            if(iSpokeSprocketLordUnitDoll == 0.0) then
                
                --Flag
                VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitDoll", "N", 1.0)
                
                --Dialogue.
                fnStandardMajorDialogue()
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLordB", "Neutral") ]])
                fnCutscene([[ Append("Golem: Another command unit has joined the mavericks, I see.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Probably feels pretty awful to see everyone getting on the winning team.[B][C]") ]])
                fnCutscene([[ Append("Golem: You are heavily outnumbered and outgunned.[P] I doubt your 'team' will win.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] We've got morality on our side, genius.[P] When the shooting starts, you think people will flock to the side of the torturers?[B][C]") ]])
                fnCutscene([[ Append("Golem: What the slave units do is irrelevant.[P] They may side with you all they like, they will crumble.[B][C]") ]])
                fnCutscene([[ Append("Golem: There is nothing moral about hewing to idiotic ideas of freedom.[P] Machines are machines, meant to serve the Cause.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Did it ever cross your mind that someday *you* might be one of those machines expended for the Cause?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Used up, thrown away, and if you resist, tortured or retired?[B][C]") ]])
                fnCutscene([[ Append("Golem: If I perform my function, that will not happen.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] You know too much about some things.[P] You know full well that inconvenient units get retired.[P] It was your job, wasn't it?[B][C]") ]])
                fnCutscene([[ Append("Golem: I am always loyal![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Heh, okay then.[B][C]") ]])
                fnCutscene([[ Append("Golem: What was that, a rhetorical win?[P] Why are you smirking?[P] You have not proved anything in this conversation.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] As if I'm going to convince you to change just by talking to you in some prison cell.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] But now the idea's in your head, and it's not coming out.[P] Nobody's going to wipe your memories.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] And it's going to stir inside your head because you can't ignore it.[P] You have lots of time to think in here.[B][C]") ]])
                fnCutscene([[ Append("Golem: Your confidence is misplaced.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Yeah, I've heard that before.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] See you later.") ]])
                fnCutsceneBlocker()
            
            --Repeat conversations.
            else
                
                --Dialogue.
                fnStandardMajorDialogue()
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLordB", "Neutral") ]])
                fnCutscene([[ Append("Golem: ...[P] Certainly the steam droids intend to execute me eventually.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Absolutely not.[P] I'd intervene personally.[B][C]") ]])
                fnCutscene([[ Append("Golem: So what then, stay in this cell forever?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] No, I don't think that'll happen, either.[B][C]") ]])
                fnCutscene([[ Append("Golem: Then what?[P] What will happen?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Think about it for a while.") ]])
                fnCutsceneBlocker()
            
            end
        
        -- |[Secrebot]|
        elseif(sChristineForm == "Secrebot") then
        
            --Form variables.
            local iSpokeSprocketLordUnitSecrebot = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitSecrebot", "N")
        
            --First time.
            if(iSpokeSprocketLordUnitSecrebot == 0.0) then
                
                --Flag
                VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitSecrebot", "N", 1.0)
                
                --Dialogue.
                fnStandardMajorDialogue()
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLordB", "Neutral") ]])
                fnCutscene([[ Append("Golem: The secrebots are easily corrupted.[P] Typical.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] CR-1-16 ready for orders, lord unit.[B][C]") ]])
                fnCutscene([[ Append("Golem: Oh?[P] It's a trick, isn't it.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Darn, you saw right through it.[P] How's it feel to know you're the smartest lord unit?[B][C]") ]])
                fnCutscene([[ Append("Golem: Very funny.[P] If you're not here to retire me, please leave me alone.") ]])
                fnCutsceneBlocker()
            
            --Repeat conversations.
            else
                
                --Dialogue.
                fnStandardMajorDialogue()
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLordB", "Neutral") ]])
                fnCutscene([[ Append("Golem: ...[P] Why are you still bothering me?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] I figured you could use some company.[P] Alone, in a cell, left with only your misdeeds to comfort you?[B][C]") ]])
                fnCutscene([[ Append("Golem: Just go away already!") ]])
                fnCutsceneBlocker()
            
            end
        
        -- |[Eldritch Dreamer]|
        elseif(sChristineForm == "Eldritch") then
        
            --Form variables.
            local iSpokeSprocketLordUnitEldritch = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitEldritch", "N")
        
            --First time.
            if(iSpokeSprocketLordUnitEldritch == 0.0) then
                
                --Flag
                VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeSprocketLordUnitEldritch", "N", 1.0)
                
                --Dialogue.
                fnStandardMajorDialogue()
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLordB", "Neutral") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Greetings, metal girl.[P] You're looking delicious.[B][C]") ]])
                fnCutscene([[ Append("Golem: And just what are you supposed to be?[P] One of the creatures from the mines?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Something like that, yes.[B][C]") ]])
                fnCutscene([[ Append("Golem: The Steam Droids have allied with disgusting creatures like yourself?[P] I'm not surprised.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Oh, we are not allies.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] The Steam Droids have merely struck a deal.[P] Food, in exchange for information.[B][C]") ]])
                fnCutscene([[ Append("Golem: So why are you here, bothering me?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Simple.[P] My kind enjoy eating metallic brains.[P] The denser, the better.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Yours is looking...[P] delicious...[B][C]") ]])
                fnCutscene([[ Append("Golem: Guards![P] I am under your protection, please![P] Do not let this creature devour me![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Oh I'm not going to eat you.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] [P][P][P]Just your brain.[B][C]") ]])
                fnCutscene([[ Append("Golem: Heeeeelp![B][C]") ]])
                fnCutscene([[ Append("55:[E|Smirk] That's enough, Christine.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Suit yourself.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] ...[P] Just a nibble?[B][C]") ]])
                fnCutscene([[ Append("Golem: Aieeeee!!![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Laugh] Ha ha ha ha![P] Perfect![P] Scream for me more![B][C]") ]])
                fnCutscene([[ Append("55:[E|Upset] Control yourself.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Right, right.[P] Of course.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] (I hope that was more my desire to annoy stuck-up people, and not my body telling me to really eat brains...)") ]])
                fnCutsceneBlocker()
            
            --Repeat conversations.
            else
                fnStandardMajorDialogue()
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLordB", "Neutral") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] You're smelling...[P] tasty...[B][C]") ]])
                fnCutscene([[ Append("Golem: Eeeeeeek!![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Laugh] (It never gets old!)") ]])
                fnCutsceneBlocker()
            end
        end
    end
    
-- |[ ================================= Responses to Lord Unit ================================= ]|
elseif(sTopicName == "Kinky") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLordB", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] I have a girlfriend already, Lord Unit, but if I can convince her...[B][C]") ]])
	fnCutscene([[ Append("Golem: I -[P] wha -[P] you -[P] (data retrieval failure) -[P] you what?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Blush] You aren't into threesomes?[P] That's a shame...[B][C]") ]])
	fnCutscene([[ Append("Golem: You -[P] oh, this must be some interrogation trick.[P] It will not work on me.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] Oh, certainly.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Blush] But I wouldn't mind cleaning you top to bottom...[B][C]") ]])
	fnCutscene([[ Append("Golem: !!![B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] Now, please be polite to the Steam Droids here.[P] They will take care of you until...[P] well, you'll see.[B][C]") ]])
	fnCutscene([[ Append("Golem: I will not fall victim to your techniques.[P] You will get no secrets from me.[P] Good day, human.") ]])

-- |[Post Decision]|
elseif(sTopicName == "Sod Off") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLordB", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Offended] At this point, I'd need to draw you a diagram to show you all the ways you can go sodomize yourself.[B][C]") ]])
	fnCutscene([[ Append("Golem: You impertinent little rapscallion![B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] Lord Unit, please.[P] Dignity.[B][C]") ]])
	fnCutscene([[ Append("Golem: ...[B][C]") ]])
	fnCutscene([[ Append("Golem: I admit I lost my composure.[P] But you - [P][CLEAR]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] A test.[P] We must conduct ourselves with poise.[B][C]") ]])
	fnCutscene([[ Append("Golem: We...?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] Now, please be polite to the Steam Droids here.[P] They will take care of you until...[P] well, you'll see.[B][C]") ]])
	fnCutscene([[ Append("Golem: Hrmpf.[P] If this is an interrogation technique, it will not work.[P] Good day, human.") ]])
end
