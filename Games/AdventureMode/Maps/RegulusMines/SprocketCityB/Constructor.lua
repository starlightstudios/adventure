-- |[Constructor]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "SprocketCityB"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "SprocketCity")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("SprocketCityB")
	
	-- |[Spawn NPCS Here]|
	--NPCs set to spawn here will be available for post-exec cutscenes.

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then
	fnStandardNPCByPosition("DroidBA")
	fnStandardNPCByPosition("DroidBB")
	
	--Extra NPCs.
	local iSprung55 = VM_GetVar("Root/Variables/Chapter5/Scenes/iSprung55", "N")
	if(iSprung55 == 0.0) then
		
		--Droids.
		fnStandardNPCByPosition("DroidBC")
		fnStandardNPCByPosition("DroidBD")
		
		--Spawn 55.
		fnSpecialCharacter("Tiffany", 25, 4, gci_Face_South, false, nil)
		EM_PushEntity("Tiffany")
			TA_SetProperty("Set Special Frame", "Downed")
		DL_PopActiveObject()
	end
	
	--Lord Unit, spawns her during the black site jobs.
	local iSteamDroidReputation  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroidReputation", "N")
	local iPlanHatched = VM_GetVar("Root/Variables/Chapter5/Scenes/iPlanHatched", "N")
	if(iPlanHatched >= 1.0 and iSteamDroidReputation >= 500) then
		
		--Droids.
		fnStandardNPCByPosition("DroidBC")
		fnStandardNPCByPosition("DroidBD")
		
		TA_Create("LordUnit")
			TA_SetProperty("Position", 25, 4)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Clipping Flag", true)
			TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
			fnSetCharacterGraphics("Root/Images/Sprites/GolemLordB/", false)
		DL_PopActiveObject()
	end
end
