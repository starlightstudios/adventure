-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "Crates") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Crates with a wide range of items in them.[P] Seems they really need some organization.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Barrel") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Water barrels, probably destined to be used as steam.[P] The label on the outside indicates they were filled using an automated condensor.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Junk") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Junk, but not just any Junk.[P] Junk that has not been placed in its appropriate container![P] The worst kind of Junk!)") ]])
    fnCutsceneBlocker()

-- |[Examinables]|
-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
