-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "Ladder") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("BlackSiteC", "FORCEPOS:7.0x10.0x0")
	
-- |[Examinables]|
elseif(sObjectName == "Pods") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (Steam Droid recharging pods...[P] but they're so old and rusted that they would be of no use to anyone.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "Computer") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Christine] (A Steam Droid computer, sitting here unused for decades.[P] The insides are rusted through.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Schematics") then
	
	--Variables.
	local iFoundSchematics = VM_GetVar("Root/Variables/Chapter5/Scenes/iFoundSchematics", "N")
	if(iFoundSchematics == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
		fnCutscene([[ Append("Thought:[VOICE|Christine] (!!![P] Steam Droid schematics![P] Arms,[P] legs,[P] transfer nodes -[P] power core![P] It's all here!)[B][C]") ]])
		fnCutscene([[ Append("Thought:[VOICE|Christine] (I'll just take a snapshot with my PDU...)[B][C]") ]])
		fnCutscene([[ Append("JX-101:[VOICE|JX-101] What's this?[P] Steam Droid schematics?[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] I think so, here.[P][SOUND|World|TakeItem] I'm sure you can put them to good use.[B][C]") ]])
		fnCutscene([[ Append("JX-101:[VOICE|JX-101] Unlikely, but I will give them to our mechanics teams.[P] But why are they here?[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] I think the golems were using the schematics here to fabricate new parts for their prisoners.[B][C]") ]])
		fnCutscene([[ Append("JX-101:[VOICE|JX-101] By the gears...[P] They kept them alive so they could torture them more...[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] We should go.[P] There's nothing more for us here...") ]])
		fnCutsceneBlocker()
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iFoundSchematics", "N", 1.0)
		
		--Check ending case.
		local iRescuedDroidA = VM_GetVar("Root/Variables/Chapter5/Scenes/iRescuedDroidA", "N")
		local iRescuedDroidB = VM_GetVar("Root/Variables/Chapter5/Scenes/iRescuedDroidB", "N")
		local iRescuedDroidC = VM_GetVar("Root/Variables/Chapter5/Scenes/iRescuedDroidC", "N")
		local iRescuedGolemD = VM_GetVar("Root/Variables/Chapter5/Scenes/iRescuedGolemD", "N")
		local iFoundSchematics = VM_GetVar("Root/Variables/Chapter5/Scenes/iFoundSchematics", "N")
		if(iRescuedDroidA == 1.0 and iRescuedDroidB == 1.0 and iRescuedDroidC == 1.0 and iRescuedGolemD == 1.0 and iFoundSchematics == 1.0) then
			
			--Flag.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iCompletedBlackSite", "N", 9.0)
			
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "JX-101", "Neutral") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] All right Christine, we've done enough.[P] I believe we've swept the site thoroughly.[B][C]") ]])
			fnCutscene([[ Append("JX-101:[E|Neutral] Let's get back to the entrance and get out of here before Regulus City figures out what we've done.") ]])
			fnCutsceneBlocker()
		end
		fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Schematics", true) ]])
		fnCutsceneBlocker()
	end
-- |[Other]|
-- |[Debug]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
