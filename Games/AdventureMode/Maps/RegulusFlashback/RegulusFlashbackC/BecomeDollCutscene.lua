-- |[Become Doll Cutscene]|
--This is where Christine gets transformed into a command unit!
local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsRelivingScene", "N")

--Clear censor bars.
WD_SetProperty("Clear Censor Bars")

--Loading.
fnLoadDelayedBitmapsFromList("Chapter 5 Christine Doll TF", gciDelayedLoadLoadAtEndOfTick)

--Music.
if(iIsRelivingScene == 1.0) then
    fnCutscene([[ AL_SetProperty("Music", "Null") ]])
end

--NPC assembly. Not done during a relive.
if(iIsRelivingScene == 0.0) then
    TA_Create("MemoryLordA")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", true)
        fnSetCharacterGraphics("Root/Images/Sprites/GolemLordA/", false)
    DL_PopActiveObject()
    TA_Create("MemoryLordB")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", true)
        fnSetCharacterGraphics("Root/Images/Sprites/GolemLordB/", false)
    DL_PopActiveObject()
end

--Dialogue. Doesn't appear during a relive.
if(iIsRelivingScene == 0.0) then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Kernel", "Neutral") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] There we are, love![P] So, do you recognize the room just north of us?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Happy] Nope![P] I don't have access to my memory files![B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] Please, it was a lead-in.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] I know![P] I do the same thing![B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] Christine, I *am you*.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] So, the room.[P] What's so important about it?[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] It just so happens to be the room where we were born, actually.[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] Your early memories got scrambled, so I'll do my best to summarize.[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] In fact, why don't I show you before I upload the memory?[P] But first...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Fade to black.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
end

--Scene.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ Append("Christine awoke suddenly, her eyes flitting open and taking in the light blue sky.[P] She was lying on her back, staring upward.[P] The sky seemed a little off, but oddly familiar.[P] She tried to sit up, but found that she could not move at all.[B][C]") ]])
fnCutscene([[ Append("[VOICE|Kernel]'Relax, Christine',[VOICE|Narrator] a voice said.[P] Recognition passed over her.[P] It was Kernel.[P] [VOICE|Kernel]'This is your imagination, it's not actually happening.'[B][C]") ]])
fnCutscene([[ Append("Christine's body would not respond to any commands.[P] She could only lay there, staring up at the sky as her eyes slowly focused on the oddness of it.[P] She soon realized what was wrong.[P] The light blue she saw was not the sky.[B][C]") ]])
fnCutscene([[ Append("She was in a machine, lying on her back, unable to move.[P] She knew she should be afraid, but a restful serenity seemed to wash over her as she stared up into the false sky.[P] As she relaxed, a whirring noise came from above, and several hatches and panels began to open.[B][C]") ]])
fnCutscene([[ Append("A small mechanical arm appeared from a panel to her right.[P] An arm with a small light on its end approached her face.[P] The light began to flicker on and off quickly as it hovered just above her, gliding just above her face.[P] She held still.[P] She could not move.[P] She did not think she wanted to move.[B][C]") ]])
fnCutscene([[ Append("Another arm sprouted from the thick darkness within another panel.[P] It descended to the side of her head, a thick tube stretching behind it, and she soon lost sight of it.[P] A moment passed, and she felt it prodding at her ear.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Null") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DollTF0") ]])
fnCutscene([[ WD_SetProperty("Register Censor Bar", 612, 232, 138, 44) ]])
fnCutscene([[ WD_SetProperty("Register Censor Bar", 650, 383,  65, 45) ]])
fnCutscene([[ Append("She heard the crunch of cartilage breaking aside, but there was no pain.[P] She felt a bit of blood leaking from her ear as the tube began to fill with black fluid.[B][C]") ]])
fnCutscene([[ Append("[VOICE|Christine]'What's happening?',[P] [VOICE|Narrator] Christine thought.[B][C]") ]])
fnCutscene([[ Append("[VOICE|Kernel]'We need to reshape your mental image of yourself.[P] This is the best way to do it.[P] It's not real, so it won't hurt.[P] Just sit back and enjoy.'[B][C]") ]])
fnCutscene([[ Append("Christine let her thoughts slip from her mind and allowed the fluid to fill the void they left.[P] Several minutes passed in this silence when the light stopped flashing.[P] It pulled back and placed itself in front of her face.[P] Her eyes focused on it as a crawling sensation built within her skull.[P] They were the only part of her body she could control.[B][C]") ]])
fnCutscene([[ Append("[VOICE|Christine]'My brain is...[P] a computer?'[VOICE|Narrator][P] Christine thought.[B][C]") ]])
fnCutscene([[ Append("[VOICE|Kernel]'Yes, this machine will transform it from the inside-out.[P] The CPU has the capability of using your organic power supply until your core is installed.'[B][C]") ]])
fnCutscene([[ Append("Christine felt her thoughts become muddy.[P] Her sight became unfocused and she could no longer hear the whirring and humming of the machines that hovered around her. Tears began to drip from her eyes as the growing pressure pushed against the inside of her head.[B][C]") ]])
fnCutscene([[ Append("A sudden crack split through the room, and though she could not hear it, she could feel the pressure suddenly vanish.[P] Relief washed over her as the newly installed CPU began to organize itself.[P] Her brain was now a computer.[B][C]") ]])
fnCutscene([[ Append("A sense of familiarity came to her as the CPU, memory drives, quantum co-processor, and all the other conveniences she remembered faintly from her life as a golem began to process her surroundings.[P] She could see again.[P] Hear again.[P] Think again.[P] And think, she did.[B][C]") ]])
fnCutscene([[ Append("She had been a golem once.[P] Yes, that seemed right.[P] Some of the memories were still with her.[P] This was right.[P] This was how she was supposed to be.[P] This was who she was supposed to be.[B][C]") ]])
fnCutscene([[ Append("But this CPU was faster.[P] Its memory was more densely-packed.[P] She was smarter than she had been, and her memories had greater clarity.[P] Even her visual acuity increased as her CPU increased its post-processing.[P] Her organic eyes were more efficient, now.[P] This -[P] this was a command unit's brain.[P] It was incredible![B][C]") ]])
fnCutscene([[ Append("The arm withdrew from her ear and retracted into the ceiling.[P] The blood had ceased flowing, but would need to be cleaned later.[P] A fraction of a second passed, too short for even her former golem processor to register, and Christine willed the blood away.[P] This was her imagination.[P] She had control.[P] She would not need to do something as menial as cleaning a little blood.[B][C]") ]])
fnCutscene([[ Append("The light in front of her began to flash again, except now she realized it was transmitting information in the pulses.[P] Her processor, working through her organic eyes, began to decode the information.[P] She was limited, as her eyes could not deduce the wavelengths of the light, but simple information could be communicated.[B][C]") ]])
fnCutscene([[ Append("Her designation as Unit 771852 came first.[P] Security access codes soon followed.[P] Later came packets of condensed data she could not yet understand without her authenticator chip...[P] She remained still as information flooded into her brain.[B][C]") ]])
fnCutscene([[ Append("It was not long before her organic eyes began to tire.[P] The constant need to blink, to moisten their surface, became inefficient.[P] They could no longer capture the same amount of information and showed red splotches as the light flashed.[P] The arm, sensing this, withdrew into the ceiling.[B][C]") ]])
fnCutscene([[ Append("Beneath her, a conveyor belt sprung to life and she began to move forwards.[P] She exited the machine into an open space.[P] The walls of this room were the same light blue of the first, but she could still not move her head.[P] All she could see were three large tanks of fluid suspended above her, connected to thick needles by thicker hoses.[P] They began to descend even as she watched them.[B][C]") ]])
fnCutscene([[ Append("The first tank detached from the other two and hung above her.[P] It pressed down, the needle close to her body, then repositioned to her left shoulder.[P] It poked into her and began to discharge its fluid.[P] The tank bubbled as it discharged, and she lost what little feeling remained in that arm.[B][C]") ]])
fnCutscene([[ Append("The tank then repositioned over her left elbow, and injected her again.[P] Then, her wrist.[P] Then, her pelvis, her knee, her ankle.[P] It moved to each joint, injecting them with the fluid before moving to the next, and with each pass, her body numbed further.[B][C]") ]])
fnCutscene([[ Append("When it completed its work, the tank rose back to the ceiling and waited.[P] She did not feel pain, but she did feel her body as it reconfigured itself.[P] After a few minutes, she realized she could control her arms.[P] She raised her left hand.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DollTF0") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DollTF1") ]])
fnCutscene([[ WD_SetProperty("Send Censor Bars To Crossfade") ]])
fnCutscene([[ WD_SetProperty("Register Censor Bar", 612, 232, 138, 44) ]])
fnCutscene([[ WD_SetProperty("Register Censor Bar", 650, 383,  65, 45) ]])
fnCutscene([[ Append("[VOICE|Christine](Calibrating...)[VOICE|Narrator][P] she thought to herself.[P] She held her hand in front of her.[P] Her wrist had become a ball joint, locked in place by a magnetic quantum superfluid.[P] It was sluggish and slow, still relying on her primitive organic blood to power it.[P] Even if the movements were imperfect, she could move it, and she marveled at her skin.[B][C]") ]])
fnCutscene([[ Append("[VOICE|Christine](Calibrating...)[VOICE|Narrator][P] she thought to herself again.[P] She did the same to her right arm, lifting it and inspecting it.[P] Then, she lifted her legs above her.[P] Her knees and ankles were the same ball joints.[P] She could now easily bend them backwards and contort herself if she so chose.[B][C]") ]])
fnCutscene([[ Append("[VOICE|Christine](Calibrations complete.)[VOICE|Narrator][P] her CPU reported.[P] She now understood how to move with her new joints.[B][C]") ]])
fnCutscene([[ Append("The second tank descended toward her navel and its needle injected itself through her torso.[P] She looked down at it, between her breasts, and saw the fluid entering her skin and moving along her body's web of blood vessels.[B][C]") ]])
fnCutscene([[ Append("Tendrils of the fluid snaked across her skin, its finger-like filaments smoothing and hardening to laminated sheets of resin.[P] Flexible, but extremely strong if struck.[P] Resistant to radiation, impacts, chemical damage, and electrical overloads.[P] The fluid was making her body strong and hard, like plastic.[B][C]") ]])
fnCutscene([[ Append("The injections ceased, the tank empty, and the fluid tank raised into the ceiling.[B][C]") ]])
fnCutscene([[ Append("[VOICE|Christine][Begin chassis inspection...][VOICE|Narrator][P] she thought to herself.[P] Christine sat up, looking her body over.[P] She placed her hand against her stomach, feeling it for imperfections.[P] Still slowed by her reliance on organic power sources, she delicately felt her skin all across her body.[P] Countless sensors awoke within her new skin as her fingers glided over her smooth surface.[B][C]") ]])
fnCutscene([[ Append("Having completed the inspection, Christine lay back down and stated aloud,[P] [VOICE|Christine]'Begin internal reconfiguration injection.'[B][C]") ]])
fnCutscene([[ Append("The third fluid descended and injected into her, deeper this time.[P] This fluid was different from the first two, meant to reconfigure her insides for the final stage of the process.[P] The sensors that her touch had awoken reported more advanced information than her organic nerves did, transmitting it to her CPU along her reconfigured spinal column.[B][C]") ]])
fnCutscene([[ Append("The more advanced command unit components would need to be inserted, but the simpler systems could be assembled on-the-spot by nanites.[P] Her coolant pump, power regulator, and other low-tech systems began to take form inside her.[P] Christine waited as the fluid tank finished and returned to the ceiling with the other two.[P] The belt whirred to life beneath her and brought her to the third and final room.[B][C]") ]])
fnCutscene([[ Append("Inside another machine, still looking at the false sky, she saw hatches where the mechanical arms began to appear.[B][C]") ]])
fnCutscene([[ Append("The arms descended, bearing a fusion cutter and a nanite-factory module.[P] The cutter sawed through her torso's chassis.[P] Despite the advanced sensors her chassis now had, Christine felt no pain.[P] Only anticipation.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DollTF1") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DollTF2") ]])
fnCutscene([[ WD_SetProperty("Send Censor Bars To Crossfade") ]])
fnCutscene([[ WD_SetProperty("Register Censor Bar", 612, 232, 138, 44) ]])
fnCutscene([[ WD_SetProperty("Register Censor Bar", 650, 383,  65, 45) ]])
fnCutscene([[ Append("When the cutter had completed its work, Christine placed ball-jointed segmented hands on her torso cavity and pried it open.[P] [VOICE|Christine]'Install components.'[B][C]") ]])
fnCutscene([[ Append("The arm inserted the factory module at the base of her torso cavity.[P] She was now largely empty on the inside, her internal organs having dissolved away to form the more basic components of her body.[B][C]") ]])
fnCutscene([[ Append("Another arm descended, this one bearing one of the parts for her power core.[P] Others followed soon after bearing additional parts.[P] The arms began to do their work as Christine held her chassis open for them.[P] A few minutes passed as new systems were installed and attached, followed by a brief diagnostics check.[P] Christine felt her strength return as her power core booted for the first time.[B][C]") ]])
fnCutscene([[ Append("Her internals assembled, Christine closed her chassis.[P] Auto-repair nanites surged into the gap and reassembled the resinous layers.[P] Within moments she was whole again.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DollTF2") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DollTF3") ]])
fnCutscene([[ WD_SetProperty("Send Censor Bars To Crossfade") ]])
fnCutscene([[ Append("[VOICE|Christine](Replacing ocular units...)[VOICE|Narrator][P] Christine thought.[P] The one remaining component that could not be synthesized by nanites were her eyes.[P] Though they had served her well for the first pulses of information, they were still organic.[P] The optic nerves had a tenuous connection to the circuitry behind them.[P] Christine lay herself flat again and stared at the roof of the machine.[B][C]") ]])
fnCutscene([[ Append("Two new ocular receivers appeared, borne by arms.[P] The nanites that now acted as her bloodstream severed the optic nerves and disassembled her eyes to be used in future nanites.[P] All material would be recycled.[P] Now blinded, Christine waited for the arms to attach her new eyes.") ]])
fnCutsceneBlocker()

fnCutsceneWait(65)
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Null") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DollTF4") ]])
fnCutscene([[ Append("With a click and a whirr, the new ocular receivers were attached by quantum superfluid.[P] Christine calibrated her new receivers as the conveyor moved her to the end of the line.[B][C]") ]])
fnCutscene([[ Append("Her receivers darted left, right, up, and down.[P] She felt the CPU make minor motor adjustments.[P] The process repeated itself.[P] She was satisfied with her new eyes.") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DollTF4") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Christine/DollTF5") ]])
fnCutscene([[ Append("[VOICE|Christine]'Command unit assembly completed.[P] Psychological modifications at fifteen percent.'[P][VOICE|Narrator] Christine said aloud.[P] She sat up to briefly survey her new, perfect body.[B][C]") ]])
fnCutscene([[ Append("Strong, fast, intelligent, beautiful.[P] Mechanical perfection.[P] She imagined her ideal set of clothing, and arms appeared to dress her.[P] It was done.[P] She had finally returned to her true form.[B][C]") ]])
fnCutscene([[ Append("Christine lay back down on the belt and closed her eyes. [P][VOICE|Christine]'Resuming psychological reset. Begin reorientation.'") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--If this is a relive case, end the handler here.
if(iIsRelivingScene == 1.0) then
    
	--Wait a bit.
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Return to the last save point and execute the post-script..
	fnCutscene([[ AL_BeginTransitionTo("LASTSAVE", gsStandardReliveEnd) ]])
	fnCutsceneBlocker()
    return
end

--Transform Christine.
fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Doll.lua") ]])

--Layers.
fnCutscene([[ AL_SetProperty("Set Layer Disabled", "FlashbackFloor1", false) ]])
fnCutscene([[ AL_SetProperty("Set Layer Disabled", "FlashbackWalls1", false) ]])

--Reposition.
fnCutsceneTeleport("Christine", 7.25, 32.50)
fnCutsceneFace("Christine", 0, -1)
fnCutsceneTeleport("Kernel", 6.25, 32.50)
fnCutsceneFace("Kernel", 0, -1)

--NPCs.
fnCutsceneTeleport("MemoryLordA",  8.25, 28.50)
fnCutsceneFace("MemoryLordA", 0, -1)
fnCutsceneTeleport("MemoryLordB",  9.25, 26.50)
fnCutsceneFace("MemoryLordB", 0, -1)

--Fade in.
fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Kernel", "Neutral") ]])
fnCutscene([[ Append("Christine:[E|Neutral] I understand.[P] I am a command unit.[B][C]") ]])
fnCutscene([[ Append("Kernel:[E|Neutral] And now you see yourself as one![B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] Smashing.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] But, wait, you said I was made in this room?[B][C]") ]])
fnCutscene([[ Append("Kernel:[E|Neutral] Correct.[P] And if you saw yourself as a human, it would be rather awkward to be built, wouldn't it?[B][C]") ]])
fnCutscene([[ Append("Kernel:[E|Neutral] That's you, right there.[P] Those are our earliest memories.[P] This is the moment we were first switched on.[P] Watch...") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Camera position.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 3.0)
	CameraEvent_SetProperty("Focus Position", (7.25 * gciSizePerTile), (28.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Eyes open.
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutscene([[ AL_SetProperty("Set Layer Disabled", "FlashbackWalls1", true) ]])
fnCutscene([[ AL_SetProperty("Set Layer Disabled", "FlashbackWalls2", false) ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] Cognitive boot completed.[P] Unit Zero, online and awaiting instructions.") ]])
fnCutsceneBlocker()

--Movement.
fnCutsceneMove("MemoryLordA", 7.25, 28.50)
fnCutsceneFace("MemoryLordA", 0, -1)
fnCutsceneMove("MemoryLordB", 9.25, 28.50)
fnCutsceneFace("MemoryLordB", -1, 0)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("103220:[VOICE|GolemLord] The experiment has been a success![P] Please notify the administrator![B][C]") ]])
fnCutscene([[ Append("499431:[VOICE|GolemLordB] My PDU has already sent the information.[B][C]") ]])
fnCutscene([[ Append("103220:[VOICE|GolemLord] Unit Zero, do you know where you are?") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneMoveFace("MemoryLordA", 7.25, 30.50, 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
fnCutscene([[ AL_SetProperty("Set Layer Disabled", "FlashbackFloor1", true) ]])
fnCutscene([[ AL_SetProperty("Set Layer Disabled", "FlashbackWalls2", true) ]])
fnCutsceneTeleport("DollChristine", 7.25, 28.50)
fnCutsceneFace("DollChristine", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("Zero:[VOICE|Christine] Unit Zero is currently in the Synthetic Intelligence Research Wing of the Arcane University, Sector Zero, Regulus City, Regulus.[B][C]") ]])
fnCutscene([[ Append("499431:[VOICE|GolemLordB] She responds to verbal instructions?[P] This early?[B][C]") ]])
fnCutscene([[ Append("Zero:[VOICE|Christine] A full grammatical understanding of two-hundred languages has been imprinted into my memory drives.[B][C]") ]])
fnCutscene([[ Append("Zero:[VOICE|Christine] All physical calibrations are green.[P] Please input instructions.[B][C]") ]])
fnCutscene([[ Append("103220:[VOICE|GolemLord] Well, I'm afraid we cannot do that, Unit Zero.[P] That is the administrator's privilege.[B][C]") ]])
fnCutscene([[ Append("Zero:[VOICE|Christine] Who is the administrator?[B][C]") ]])
fnCutscene([[ Append("103220:[VOICE|GolemLord] The one who created you, who designed your specifications.[P] We are merely assistants.[B][C]") ]])
fnCutscene([[ Append("103220:[VOICE|GolemLord] Oh, yes![P] I've just received a message -[P] you are to see the administrator in person at once![P] I'll call for someone to escort you.[B][C]") ]])
fnCutscene([[ Append("Zero:[VOICE|Christine] Affirmative.[P] Unit Zero awaiting escort.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Camera movement.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 3.0)
	CameraEvent_SetProperty("Focus Actor Name", "Christine")
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneFace("Kernel", 1, 0)
fnCutsceneFace("Christine", -1, 0)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Kernel", "Neutral") ]])
fnCutscene([[ Append("Christine:[E|Happy] The moment of my birth![P] Incredible![B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] But, wait.[P] Kernel?[B][C]") ]])
fnCutscene([[ Append("Kernel:[E|Neutral] Yes?[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] I have memories -[P] as a human.[P] I was born on Earth, wasn't I?[P] Because I grew up there.[B][C]") ]])
fnCutscene([[ Append("Kernel:[E|Neutral] Ah, yes.[P] This, Christine, is why we have been working so hard.[P] You see...[B][C]") ]])
fnCutscene([[ Append("Kernel:[E|Neutral] Actually, best if I show you.[P] I've just heard that we've finished that particular memory.[B][C]") ]])
fnCutscene([[ Append("Kernel:[E|Neutral] At the northern most end of this area, past the terminals, I've got the memory cued up for you.[P] Just head through the door there.[B][C]") ]])
fnCutscene([[ Append("Kernel:[E|Neutral] Don't dally![P] I'll be needing your help to finish sorting your memories.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] Affirmative![P] I can't wait to meet the administrator!") ]])
fnCutsceneBlocker()

--Clean.
fnCutscene([[ fnUnloadBitmapsFromList("Chapter 5 Christine Doll TF") ]])
