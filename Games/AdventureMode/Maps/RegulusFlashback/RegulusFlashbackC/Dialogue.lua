-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Get actor name if this is a dialogue script.
    local sActorName = TA_GetProperty("Name")
    if(sActorName == "Kernel") then
        
        --First pass: Trigger cutscene.
        local iFlashbackBecameDoll = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackBecameDoll", "N")
        if(iFlashbackBecameDoll == 0.0) then
            VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackBecameDoll", "N", 1.0)
            LM_ExecuteScript(fnResolvePath() .. "BecomeDollCutscene.lua")
            
        --Repeats:
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Kernel:[VOICE|Kernel] The northernmost door, Christine.[P] It's past our upload terminals.[P] Don't dally!") ]])
        end
        
    elseif(sActorName == "DollA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Doll:[VOICE|Doll] We received some very important lessons before we left. It helped blend into human society. Would you like to review the memory?[BLOCK]") ]])

        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"WatchLab\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"Cancel\") ")
        fnCutsceneBlocker()
        
    elseif(sActorName == "DollB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Doll:[VOICE|Doll] Our first practical work experience.[P] We're just smoothing out the last parts, do you want to watch the memory?[BLOCK]") ]])

        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Watch\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"Cancel\") ")
        fnCutsceneBlocker()
            
    elseif(sActorName == "DollC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Doll:[VOICE|Doll] Leftover junk from the imagination.[P] We'll get it cleaned up.") ]])
        
    elseif(sActorName == "DollD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Doll:[VOICE|Doll] Just ahead...[P] This is where we left from...") ]])
    end

--Watch the cutscene:
elseif(sTopicName == "Watch") then
	WD_SetProperty("Hide")
    
    --Fire the cutscene.
    LM_ExecuteScript(fnResolvePath() .. "DroneCutscene.lua")

--Watch the cutscene:
elseif(sTopicName == "WatchLab") then
	WD_SetProperty("Hide")
    
    --Fire the cutscene.
    LM_ExecuteScript(fnResolvePath() .. "LabCutscene.lua")

--Cancel:
elseif(sTopicName == "Cancel") then
	WD_SetProperty("Hide")

end
