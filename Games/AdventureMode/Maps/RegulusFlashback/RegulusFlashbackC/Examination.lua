-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToFlashbackB") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusFlashbackB", "FORCEPOS:4.0x6.0x0")
    
elseif(sObjectName == "ToFlashbackK") then
    
    --Variables.
    local iFlashbackBecameDoll = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackBecameDoll", "N")
    
    --Not a doll yet:
    if(iFlashbackBecameDoll == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Kernel told me to meet her by the westernmost room...)") ]])
        fnCutsceneBlocker()
    
    --Doll!
    else
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_BeginTransitionTo("RegulusFlashbackK", "FORCEPOS:21.5x63.0x0")
    end
    
-- |[Objects]|
elseif(sObjectName == "MemoryUpload") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (An upload terminal with various memories on it, undergoing the final stages of crosschecking before being placed in my memory permanently.)") ]])
    
elseif(sObjectName == "Terminal") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The terminal which contained my schematics.[P] I never actually read it myself.)") ]])
    
elseif(sObjectName == "FabBench") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The fabrication bench and tools used to create my non-standard components.)") ]])
    
elseif(sObjectName == "BirthChamber") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This is where my final assembly took place.[P] This is where I was born.)") ]])
    
elseif(sObjectName == "Garbage") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Imagination objects being sorted before being sent to a memory to hold them.)") ]])
    
elseif(sObjectName == "FizzyPop") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Fizzy Pop![P] Do these represent an addiction?)") ]])
    
elseif(sObjectName == "55") then
    local iFlashbackMetAdministrator = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackMetAdministrator", "N")
    if(iFlashbackMetAdministrator == 0.0) then return end
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Unit 2855.[P] Maverick command unit.[P] Memories pending descrambling.[P] Abrasive, intelligent, tactical.)") ]])
    
elseif(sObjectName == "SX-399") then
    local iFlashbackMetAdministrator = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackMetAdministrator", "N")
    if(iFlashbackMetAdministrator == 0.0) then return end
    
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    if(iSX399JoinsParty == 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (SX-399.[P] Refurbished steam droid.[P] Memories pending descrambling.[P] Rash, young, courageous, naive.)") ]])
    end
elseif(sObjectName == "Vivify") then
    local iFlashbackMetAdministrator = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackMetAdministrator", "N")
    if(iFlashbackMetAdministrator == 0.0) then return end
    
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Project Vivify.[P] Former human, true origins unknown.[P] Memories pending descrambling.[P] Personality assessment unavailable.)") ]])
    
elseif(sObjectName == "20") then
    local iFlashbackMetAdministrator = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackMetAdministrator", "N")
    if(iFlashbackMetAdministrator == 0.0) then return end
    
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('20'.[P] Golem, true origins unknown.[P] Memories pending descrambling.[P] Arrogant, vain, intelligent, dangerous.)") ]])
    
elseif(sObjectName == "Sophie") then
    local iFlashbackMetAdministrator = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackMetAdministrator", "N")
    if(iFlashbackMetAdministrator == 0.0) then return end
    
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Unit 499323 'Sophie'.[P] Golem.[P] Memories pending descrambling.[P] Beautiful, witty, kind, caring, smart, gorgeous, loving, cute, adorable, literally causes rooms to become brighter.)") ]])
    
elseif(sObjectName == "Golem") then
    local iFlashbackMetAdministrator = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackMetAdministrator", "N")
    if(iFlashbackMetAdministrator == 0.0) then return end
    
    local iSpokeWithDistressGolem = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeWithDistressGolem", "N")
    if(iSpokeWithDistressGolem >= 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Unit 599239, 'Hadasa'.[P] Golem.[P] Met once, present at Equinox Labs during disaster.[P] Cautious.[P] Further personality assessment unavailable.)") ]])
    end
    
elseif(sObjectName == "300910") then
    local iFlashbackMetAdministrator = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackMetAdministrator", "N")
    if(iFlashbackMetAdministrator == 0.0) then return end
    
    local iCompletedSerenity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
    if(iCompletedSerenity == 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Unit 300910.[P] Command Unit.[P] Memories pending descrambling.[P] Intelligent, organized, caring, curious, confident.)") ]])
    end
elseif(sObjectName == "Psue") then
    local iFlashbackMetAdministrator = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackMetAdministrator", "N")
    if(iFlashbackMetAdministrator == 0.0) then return end
    
    local iHasElectrospriteForm = VM_GetVar("Root/Variables/Global/Christine/iHasElectrospriteForm", "N")
    if(iHasElectrospriteForm == 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Psue.[P] Electrosprite.[P] Electrical elemental?[P] Shares knowledge and most memories due to temporary physical possession.[P] Reckless, rash, exuberant, adorable.)") ]])
    end

-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
