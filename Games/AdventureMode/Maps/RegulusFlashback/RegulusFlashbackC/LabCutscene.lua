-- |[Lab Cutscene]|
--Unit Zero receives some... advice.

--Move the camera.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 5.0)
	CameraEvent_SetProperty("Focus Position", (50.25 * gciSizePerTile), (27.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneFace("MemoryLordC", 0, -1)
fnCutsceneTeleport("MemoryLordC", 53.25, 26.50)
fnCutsceneFace("MemoryLordD", 1, 0)
fnCutsceneTeleport("MemoryLordD", 52.25, 26.50)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Spawn the characters we need.
fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
fnCutsceneFace("DollChristine", 0, -1)
fnCutsceneTeleport("DollChristine", 50.75, 30.50)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneFace("MemoryLordC", 0, 1)
fnCutsceneFace("MemoryLordD", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneMove("MemoryLordD", 52.25, 28.50)
fnCutsceneMove("MemoryLordD", 50.25, 28.50)
fnCutsceneFace("MemoryLordD", 0, 1)
fnCutsceneMove("MemoryLordC", 53.25, 28.50)
fnCutsceneMove("MemoryLordC", 51.25, 28.50)
fnCutsceneFace("MemoryLordC", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("103220:[VOICE|GolemLord] Unit Zero![P] You're back![B][C]") ]])
fnCutscene([[ Append("499431:[VOICE|GolemLordB] The administrator said this might be your last meeting.[P] I was so worried.[B][C]") ]])
fnCutscene([[ Append("Zero:[VOICE|Christine] It was to be our last meeting.[P] She has finally given me my function assignment.[B][C]") ]])
fnCutscene([[ Append("103220:[VOICE|GolemLord] Wonderful![P] No more converting humans or repetitive manual tasks?[B][C]") ]])
fnCutscene([[ Append("Zero:[VOICE|Christine] I will be transported to another dimension to gather information for the Cause of Science.[P] I will disguise myself as a human.[B][C]") ]])
fnCutscene([[ Append("499431:[VOICE|GolemLordB] So...[P] we will not see you again?[B][C]") ]])
fnCutscene([[ Append("Zero:[VOICE|Christine] No.[P] Not until I am recalled.[B][C]") ]])
fnCutscene([[ Append("499431:[VOICE|GolemLordB] I see...[B][C]") ]])
fnCutscene([[ Append("Zero:[VOICE|Christine] You are crying due to your emotions.[P] Suppress them.[B][C]") ]])
fnCutscene([[ Append("499431:[VOICE|GolemLordB] It's hard...[B][C]") ]])
fnCutscene([[ Append("103220:[VOICE|GolemLord] We're not [P]*sniff*[P] perfect like you.[P] Golems have flaws...[B][C]") ]])
fnCutscene([[ Append("Zero:[VOICE|Christine] Do not waste the lubricant tears on me.[P] I need your assistance.[B][C]") ]])
fnCutscene([[ Append("499431:[VOICE|GolemLordB] Certainly![P] With what?[B][C]") ]])
fnCutscene([[ Append("Zero:[VOICE|Christine] My encounters with humans have been extremely limited.[P] I need information on how they work.[B][C]") ]])
fnCutscene([[ Append("499431:[VOICE|GolemLordB] Oh, of course![P] I studied human societies on Pandemonium before my assignment to the AI Research wing![B][C]") ]])
fnCutscene([[ Append("103220:[VOICE|GolemLord] Naturally, the Administrator planned for that.[P] She is so wise.[B][C]") ]])
fnCutscene([[ Append("499431:[VOICE|GolemLordB] You're going to need a name.[P] Humans will suspect someone named Unit Zero.[B][C]") ]])
fnCutscene([[ Append("Zero:[VOICE|Christine] Suggest one.[B][C]") ]])
fnCutscene([[ Append("103220:[VOICE|GolemLord] Erm...[P] Jeff?") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

fnCutsceneFace("MemoryLordC", -1, 0)
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("499431:[VOICE|GolemLordB] Jeff is a male's name you drainer-brain![B][C]") ]])
fnCutscene([[ Append("103220:[VOICE|GolemLord] Male?[P] Really?[B][C]") ]])
fnCutscene([[ Append("499431:[VOICE|GolemLordB] Don't listen to her, Zero.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

fnCutsceneFace("MemoryLordC", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("499431:[VOICE|GolemLordB] Perhaps...[P] Christine?[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] Affirmative.[P] Adding alias 'Christine'.[B][C]") ]])
fnCutscene([[ Append("103220:[VOICE|GolemLord] Well, what else...[B][C]") ]])
fnCutscene([[ Append("103220:[VOICE|GolemLord] You'll need to pick out some clothes, and acquire more once you arrive.[P] Try to blend in with the local style.[B][C]") ]])
fnCutscene([[ Append("499431:[VOICE|GolemLordB] You'll need to hide your joints...[B][C]") ]])
fnCutscene([[ Append("499431:[VOICE|GolemLordB] Oh, and don't eat metal or oil![P] Those are toxic, so humans will get suspicious.[B][C]") ]])
fnCutscene([[ Append("103220:[VOICE|GolemLord] And humans respire![P] Through their mouths![B][C]") ]])
fnCutscene([[ Append("499431:[VOICE|GolemLordB] Yes, the expel gasses from both ends, but only inhale oxygen from one end.[B][C]") ]])
fnCutscene([[ Append("499431:[VOICE|GolemLordB] If you see a human inhaling oxygen from the other end, they may need medical attention.[B][C]") ]])
fnCutscene([[ Append("103220:[VOICE|GolemLord] Oh![P] Oh![P] Many human societies consider sniffing another's hands as a sign of respect![B][C]") ]])
fnCutscene([[ Append("499431:[VOICE|GolemLordB] And they're fragile.[P] You can barely throw them ten meters before they break bones.[B][C]") ]])
fnCutscene([[ Append("103220:[VOICE|GolemLord] And electric shocks are right out.[B][C]") ]])
fnCutscene([[ Append("103220:[VOICE|GolemLord] But humans do value their own mortality.[P] They believe it drives them to accomplish things.[B][C]") ]])
fnCutscene([[ Append("499431:[VOICE|GolemLordB] You'll have to be very patient with them...") ]])
fnCutsceneBlocker()

--Camera movement.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 3.0)
	CameraEvent_SetProperty("Focus Actor Name", "Christine")
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("771852:[VOICE|Christine] The briefing went on for some time...") ]])
fnCutsceneBlocker()
fnCutsceneTeleport("DollChristine", -50.75, -30.50)










