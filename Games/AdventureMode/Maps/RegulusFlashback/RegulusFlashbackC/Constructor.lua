-- |[Constructor]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusFlashbackC"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
    local iFlashbackBecameDoll = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackBecameDoll", "N")
    if(iFlashbackBecameDoll == 0.0) then
        AL_SetProperty("Music", "Null")
    else
        AL_SetProperty("Music", "ChristineDream")
    end
    
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("RegulusFlashbackC")
	
	-- |[Spawn NPCS Here]|
	--NPCs set to spawn here will be available for post-exec cutscenes.

	-- |[Lights]|
    AL_SetProperty("Activate Lights")

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[Spawn NPCs Here]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    local iFlashbackBecameDoll = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackBecameDoll", "N")
    local iFlashbackMetAdministrator = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackMetAdministrator", "N")
    if(iFlashbackBecameDoll == 0.0) then
        fnStandardNPCByPosition("Kernel")
    end
    if(iFlashbackMetAdministrator == 1.0) then
    
        --NPCs that always spawn:
        fnSpawnNPCPattern("Doll", "A", "D")
        fnStandardNPCByPosition("ImportantA")
        fnStandardNPCByPosition("ImportantC")
        fnStandardNPCByPosition("ImportantE")
        fnStandardNPCByPosition("ImportantF")
        
        --Conditional spawns:
        local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
        if(iSX399JoinsParty == 1.0) then fnStandardNPCByPosition("ImportantB") end
        local iCompletedSerenity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
        if(iCompletedSerenity == 1.0) then fnStandardNPCByPosition("ImportantG") end
        local iHasElectrospriteForm = VM_GetVar("Root/Variables/Global/Christine/iHasElectrospriteForm", "N")
        if(iHasElectrospriteForm == 1.0) then fnStandardNPCByPosition("ImportantH") end
        local iSpokeWithDistressGolem = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeWithDistressGolem", "N")
        if(iSpokeWithDistressGolem >= 1.0) then fnStandardNPCByPosition("ImportantD") end
        
    end
    
    -- |[Layers]|
    AL_SetProperty("Set Layer Disabled", "FlashbackFloor1", true)
    AL_SetProperty("Set Layer Disabled", "FlashbackWalls1", true)
    AL_SetProperty("Set Layer Disabled", "FlashbackWalls2", true)
    
    --Latex drone TF sequence.
    AL_SetProperty("Set Layer Disabled", "DroneFloor1HBlonde", true)
    AL_SetProperty("Set Layer Disabled", "DroneFloor1HBlack", true)
    AL_SetProperty("Set Layer Disabled", "DroneFloor1Latex", true)
    AL_SetProperty("Set Layer Disabled", "DroneFloor1Closed", true)
    AL_SetProperty("Set Layer Disabled", "DroneWalls1HBlonde", true)
    AL_SetProperty("Set Layer Disabled", "DroneWalls1HBlack", true)
    AL_SetProperty("Set Layer Disabled", "DroneWalls1LBlonde", true)
    AL_SetProperty("Set Layer Disabled", "DroneWalls1LBlack", true)
    AL_SetProperty("Set Layer Disabled", "DroneWalls1LRed", true)
    AL_SetProperty("Set Layer Disabled", "DroneWalls1Closed", true)
    
    --Spawn these NPCs for the drone cutscene. Put them offscreen.
    TA_Create("DollChristine")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        fnSetCharacterGraphics("Root/Images/Sprites/Christine_Doll/", false)
    DL_PopActiveObject()
    TA_Create("GolemAssistant")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        fnSetCharacterGraphics("Root/Images/Sprites/GolemSlaveP/", false)
    DL_PopActiveObject()
    TA_Create("HumanBlonde")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        fnSetCharacterGraphics("Root/Images/Sprites/GenericF0/", false)
    DL_PopActiveObject()
    TA_Create("HumanBlackA")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        fnSetCharacterGraphics("Root/Images/Sprites/GenericF1/", false)
    DL_PopActiveObject()
    TA_Create("HumanBlackB")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        fnSetCharacterGraphics("Root/Images/Sprites/GenericF1/", false)
    DL_PopActiveObject()
    TA_Create("DroneBlonde")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        fnSetCharacterGraphics("Root/Images/Sprites/LatexBlonde/", false)
    DL_PopActiveObject()
    TA_Create("DroneBlackA")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        fnSetCharacterGraphics("Root/Images/Sprites/LatexBrown/", false)
    DL_PopActiveObject()
    TA_Create("DroneBlackB")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        fnSetCharacterGraphics("Root/Images/Sprites/LatexRed/", false)
    DL_PopActiveObject()
    
    --Lab Cutscene.
    TA_Create("MemoryLordC")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", true)
        fnSetCharacterGraphics("Root/Images/Sprites/GolemLordA/", false)
    DL_PopActiveObject()
    TA_Create("MemoryLordD")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", true)
        fnSetCharacterGraphics("Root/Images/Sprites/GolemLordB/", false)
    DL_PopActiveObject()
end
