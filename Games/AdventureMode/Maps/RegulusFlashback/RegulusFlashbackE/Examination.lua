-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToFlashbackB") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusFlashbackB", "FORCEPOS:13.0x14.0x0")
    
-- |[Objects]|
elseif(sObjectName == "FantasyTerminal") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Input selection')[BLOCK]") ]])

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"The Peppered Moth\", " .. sDecisionScript .. ", \"Moth\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Christine Assimilated!\", " .. sDecisionScript .. ", \"Assimilate\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Attack of the Claygirls!\", " .. sDecisionScript .. ", \"Claygirls\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Rubber Army\", " .. sDecisionScript .. ", \"Rubber\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"The Puppetmaster\", " .. sDecisionScript .. ", \"Puppetmaster\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"APseummiliation!\", " .. sDecisionScript .. ", \"Psue\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Cancel\",  " .. sDecisionScript .. ", \"Cancel\") ")
    fnCutsceneBlocker()

-- |[Fantasies]|
elseif(sObjectName == "Moth") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(fnResolvePath() .. "Peppered Moth.lua")
    
elseif(sObjectName == "Assimilate") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(fnResolvePath() .. "Assimilated.lua")
    
elseif(sObjectName == "Claygirls") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(fnResolvePath() .. "AttackOfTheClaygirls.lua")
    
elseif(sObjectName == "Rubber") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(fnResolvePath() .. "RubberArmy.lua")
    
elseif(sObjectName == "Puppetmaster") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(fnResolvePath() .. "TheDollmaster.lua")
    --I intentionally named one Puppet and the other Doll to anger people with OCD.
    
elseif(sObjectName == "Psue") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(fnResolvePath() .. "Wiregirls.lua")
    
elseif(sObjectName == "Cancel") then
	WD_SetProperty("Hide")

-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
