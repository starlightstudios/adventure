-- |[Constructor]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusFlashbackE"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
    local iFlashbackBecameDoll = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackBecameDoll", "N")
    if(iFlashbackBecameDoll == 0.0) then
        AL_SetProperty("Music", "Null")
    else
        AL_SetProperty("Music", "ChristineDream")
    end
    
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("RegulusFlashbackE")
	
	-- |[Spawn NPCS Here]|
	--NPCs set to spawn here will be available for post-exec cutscenes.

	-- |[Lights]|
    AL_SetProperty("Activate Lights")

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[Spawn NPCs Here]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    fnStandardNPCByPosition("KernelA")
    fnStandardNPCByPosition("KernelB")
    fnStandardNPCByPosition("KernelC")
    fnStandardNPCByPosition("DollA")
    fnStandardNPCByPosition("DollB")
    fnStandardNPCByPosition("DollC")
    
    --Conditional: Must have done steam droid quests.
    local iHasSteamDroidForm = VM_GetVar("Root/Variables/Global/Christine/iHasSteamDroidForm", "N")
    if(iHasSteamDroidForm == 1.0) then
        fnStandardNPCByPosition("DollD")
        fnStandardNPCByPosition("DroidA")
        fnStandardNPCByPosition("DroidB")
        fnStandardNPCByPosition("DroidC")
    end
    
    --Conditional: Must have done darkmatter quest.
    local iHasDarkmatterForm = VM_GetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N")
    if(iHasDarkmatterForm == 1.0) then
        fnStandardNPCByPosition("DollE")
        fnStandardNPCByPosition("Darkmatter")
    end
    
    --Conditional: Must have done electrosprite quest.
    local iHasElectrospriteForm = VM_GetVar("Root/Variables/Global/Christine/iHasElectrospriteForm", "N")
    if(iHasElectrospriteForm == 1.0) then
        fnStandardNPCByPosition("DollF")
        fnStandardNPCByPosition("Psue")
        fnStandardNPCByPosition("Cmdy")
        fnStandardNPCByPosition("Algy")
    end
    
    --Performance Review
    fnStandardNPCByPosition("PerformanceDoll")
    fnStandardNPCByPosition("PerformanceLordA")
    fnStandardNPCByPosition("PerformanceLordB")
    fnStandardNPCByPosition("PerformanceLordC")
    
    --Hit-On
    fnStandardNPCByPosition("Trucy")
    
    --Dumb Drones
    fnSpawnNPCPattern("Drone", "A", "L")
    
    --Dummy Golem Christine
    TA_Create("GolemChristine")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        fnSetCharacterGraphics("Root/Images/Sprites/Christine_Golem/", false)
    DL_PopActiveObject()
    
    --Set Tiffany's costume to her normal one for this area.
    VM_SetVar("Root/Variables/Costumes/Tiffany/sCostumeDoll", "S", "Normal")
    
    --Special characters who appear in some scenes.
    fnSpecialCharacter("Tiffany", -100, -100, gci_Face_South, false, nil)
    fnSpecialCharacter("SX-399",  -100, -100, gci_Face_South, false, nil)
    fnSpecialCharacter("Sophie",  -100, -100, gci_Face_South, false, nil)
    
    --Make sure Sophie is in normal regalia.
    EM_PushEntity("Sophie")
        fnSetCharacterGraphics("Root/Images/Sprites/Sophie/", true)
        TA_SetProperty("Wipe Special Frames")
    DL_PopActiveObject()

    --Dialogue portraits.
    DialogueActor_Push("Sophie")
        DialogueActor_SetProperty("Add Emotion", "Neutral",   "Root/Images/Portraits/SophieDialogue/Neutral",   true)
        DialogueActor_SetProperty("Add Emotion", "Happy",     "Root/Images/Portraits/SophieDialogue/Happy",     true)
        DialogueActor_SetProperty("Add Emotion", "Blush",     "Root/Images/Portraits/SophieDialogue/Blush",     true)
        DialogueActor_SetProperty("Add Emotion", "Smirk",     "Root/Images/Portraits/SophieDialogue/Smirk",     true)
        DialogueActor_SetProperty("Add Emotion", "Sad",       "Root/Images/Portraits/SophieDialogue/Sad",       true)
        DialogueActor_SetProperty("Add Emotion", "Surprised", "Root/Images/Portraits/SophieDialogue/Surprised", true)
        DialogueActor_SetProperty("Add Emotion", "Offended",  "Root/Images/Portraits/SophieDialogue/Offended",  true)
    DL_PopActiveObject()
    
    --Layers.
    AL_SetProperty("Set Layer Disabled", "LanternFloor", true)
    AL_SetProperty("Set Layer Disabled", "LanternTable", true)
    
    
end
