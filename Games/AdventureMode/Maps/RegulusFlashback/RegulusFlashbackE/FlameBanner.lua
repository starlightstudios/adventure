-- |[Flame Banner]|
--This isn't a parody. How could you suggest that?
DialogueActor_Push("Christine")
    DialogueActor_SetProperty("Add Emotion", "Neutral",  "Root/Images/Portraits/ChristineDialogue/Golem|Neutral", true)
    DialogueActor_SetProperty("Add Emotion", "Happy",    "Root/Images/Portraits/ChristineDialogue/Golem|Happy", true)
    DialogueActor_SetProperty("Add Emotion", "Blush",    "Root/Images/Portraits/ChristineDialogue/Golem|Blush", true)
    DialogueActor_SetProperty("Add Emotion", "Smirk",    "Root/Images/Portraits/ChristineDialogue/Golem|Smirk", true)
    DialogueActor_SetProperty("Add Emotion", "Sad",      "Root/Images/Portraits/ChristineDialogue/Golem|Sad", true)
    DialogueActor_SetProperty("Add Emotion", "Scared",   "Root/Images/Portraits/ChristineDialogue/Golem|Scared", true)
    DialogueActor_SetProperty("Add Emotion", "Offended", "Root/Images/Portraits/ChristineDialogue/Golem|Offended", true)
    DialogueActor_SetProperty("Add Emotion", "Serious",  "Root/Images/Portraits/ChristineDialogue/Golem|Serious", true)
    DialogueActor_SetProperty("Add Emotion", "Cry",      "Root/Images/Portraits/ChristineDialogue/Golem|Cry", true)
    DialogueActor_SetProperty("Add Emotion", "Laugh",    "Root/Images/Portraits/ChristineDialogue/Golem|Laugh", true)
    DialogueActor_SetProperty("Add Emotion", "Angry",    "Root/Images/Portraits/ChristineDialogue/Golem|Angry", true)
    DialogueActor_SetProperty("Add Emotion", "PDU",      "Root/Images/Portraits/ChristineDialogue/Golem|PDU", true)
DL_PopActiveObject()

--Position entities.
fnCutsceneFace("Psue", 0, 1)
fnCutsceneTeleport("Psue", 26.25, 28.50)
fnCutsceneTeleport("Algy", -26.25, -28.50)
fnCutsceneTeleport("Cmdy", -26.25, -28.50)
fnCutsceneTeleport("GolemChristine", -24.25, -19.50)
    
--Move the camera.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 5.0)
	CameraEvent_SetProperty("Focus Position", (26.75 * gciSizePerTile), (30.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Christine enters.
fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
fnCutsceneFace("GolemChristine", 0, 1)
fnCutsceneTeleport("GolemChristine", 32.75, 32.50)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneMove("GolemChristine", 26.75, 32.50)
fnCutsceneFace("GolemChristine", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Electrosprite", "Neutral") ]])
fnCutscene([[ Append("Christine:[E|Smirk] Okay Psue, you made a big boast.[P] Let's see this secret project of yours.[B][C]") ]])
fnCutscene([[ Append("Psue:[E|Neutral] XD XD XD,[P] get out here girls!") ]])
fnCutsceneBlocker()
fnCutsceneWait(65)
fnCutsceneBlocker()

--Rotation Array
local iXArray = {1, 0, -1, 0}
local iYArray = {0, 1, 0, -1}

--Algy pops up.
fnCutscene([[ AudioManager_PlaySound("World|SparksD") ]])
fnCutsceneFace("Psue", 1, 0)
fnCutsceneTeleport("Algy", 28.25, 28.50)
fnCutsceneBlocker()
for i = 1, 5, 1 do
    for p = 1, 4, 1 do
        fnCutsceneFace("Algy", iXArray[p], iYArray[p])
        fnCutsceneWait(3)
        fnCutsceneBlocker()
    end
end
fnCutsceneFace("Algy", 0, 1)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Cmdy pops up.
fnCutscene([[ AudioManager_PlaySound("World|SparksB") ]])
fnCutsceneFace("Psue", 0, -1)
fnCutsceneTeleport("Cmdy", 26.25, 26.50)
fnCutsceneBlocker()
for i = 1, 5, 1 do
    for p = 1, 4, 1 do
        fnCutsceneFace("Cmdy", iXArray[p], iYArray[p])
        fnCutsceneWait(3)
        fnCutsceneBlocker()
    end
end
fnCutsceneFace("Cmdy", 0, 1)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("Psue:[VOICE|Electrosprite] Algy, everything ready on your end?[B][C]") ]])
fnCutscene([[ Append("Algy:[VOICE|Amanda] You bet![B][C]") ]])
fnCutscene([[ Append("Psue:[VOICE|Electrosprite] Cmdy, is the interface fixed?[B][C]") ]])
fnCutscene([[ Append("Cmdy:[VOICE|LatexDroneB] As if you have to ask![B][C]") ]])
fnCutscene([[ Append("Psue:[VOICE|Electrosprite] Let's get this show on the road![P] That's a phrase I borrowed from Christine's brain, I have no idea what it even means!") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneBlocker()

for i = 1, 5, 1 do
    for p = 4, 1, -1 do
        fnCutsceneFace("Cmdy", iXArray[p], iYArray[p])
        fnCutsceneFace("Psue", iXArray[p], iYArray[p])
        fnCutsceneFace("Algy", iXArray[p], iYArray[p])
        fnCutsceneWait(3)
        fnCutsceneBlocker()
    end
end
fnCutscene([[ AudioManager_PlaySound("World|SparksA") ]])
fnCutsceneTeleport("Psue", -26.25, -26.50)
fnCutsceneTeleport("Cmdy", -26.25, -26.50)
fnCutsceneTeleport("Algy", -26.25, -26.50)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("Psue:[VOICE|Electrosprite] Just use that console and start up Flame Banner:: Romance of the Three Lineages![P] We'll handle the rest![B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] Uhhh, okay.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneMove("GolemChristine", 28.75, 30.50)
fnCutsceneFace("GolemChristine", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] Flame Banner:: Romance of the Three Lineages.[P] Hm, name your character, pick an appearance.[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] Do the available appearances have to be sexy?[B][C]") ]])
fnCutscene([[ Append("Psue:[VOICE|Electrosprite] Yes.[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] Obviously.[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] Okay, watch a couple cutscenes...[P] I'm the chosen hero with a magic emblem...[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] Gee, where did you take inspiration from, Psue?[B][C]") ]])
fnCutscene([[ Append("Psue:[VOICE|Electrosprite] ::D[P] I mean I don't know what you're talking about![B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] Okay, we're on Pandemonium someplace, I'm a mercenary.[P] I have a...[P] Mark Five Pulse Carbine...[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] Command my troops...[P] Hmm, I have to aim the rifle myself to shoot the enemies...[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] This is very realistic, and the dialogue...[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] Psue, are you three playing the role of the NPCs?[B][C]") ]])
fnCutscene([[ Append("Psue:[VOICE|Electrosprite] XD[P] She figured it out, girls![B][C]") ]])
fnCutscene([[ Append("Algy:[VOICE|Amanda] Isn't it obvious?[B][C]") ]])
fnCutscene([[ Append("Cmdy:[VOICE|LatexDroneB] I wanted to be the girl with the axe but Psue called dibs.[B][C]") ]])
fnCutscene([[ Append("Psue:[VOICE|Electrosprite] But -[P] But, Christine, this is a realistic combat training game![B][C]") ]])
fnCutscene([[ Append("Psue:[VOICE|Electrosprite] We can get slave units to play it and it will teach them small-group tactics, weapon handling, and how to date cute girls![B][C]") ]])
fnCutscene([[ Append("Cmdy:[VOICE|LatexDroneB] And we can spy on them and identify sympathetic golems to join the rebellion.[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] You made an entire video game for that?[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] Psue, you're a genius![B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] Okay, I guess I'm going with Psue's lineage...[P] I like a red girl with an axe, what can I say.[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] And now my character is teaching you how to be a leader?[B][C]") ]])
fnCutscene([[ Append("Psue:[VOICE|Electrosprite] XD inorite?[P] It was Algy's idea.[B][C]") ]])
fnCutscene([[ Append("Cmdy:[VOICE|LatexDroneB] Yeah she gets picked to be a teacher after exactly one battle.[B][C]") ]])
fnCutscene([[ Append("Algy:[VOICE|Amanda] Players want to get right to the action![P] May as well get it over with quickly.[B][C]") ]])
fnCutscene([[ Append("Christine:[VOICE|Christine] I hope you three are ready for a full night because I intend to test this program extensively.[B][C]") ]])
fnCutscene([[ Append("Psue:[VOICE|Electrosprite] Counting on in it baybee!") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Camera moves.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 5.0)
	CameraEvent_SetProperty("Focus Actor Name", "Christine")
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ LM_ExecuteScript(gsRoot .. "Maps/RegulusFlashback/RegulusFlashbackE/ResetToDollEmotes.lua") ]])
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("Thought:[VOICE|Leader] (The game was a huge success and helped identify hundreds of units across the city for recruitment.[P] Video games are clearly powerful tools of learning and should be respected as the most legitimate art form.)") ]])
fnCutsceneBlocker()

fnCutsceneTeleport("GolemChristine", -100, -100)
