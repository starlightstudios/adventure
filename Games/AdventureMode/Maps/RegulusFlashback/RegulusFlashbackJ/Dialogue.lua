-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    if(sActorName == "Chris") then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackStartFinale", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "FakeChris", "Neutral") ]])
        fnCutscene([[ Append("Guy:[E|Neutral] H-[P]hello?[P] Who are you?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Me?[P] I'm... [P][CLEAR]") ]])
        fnCutscene([[ Append("Christine:[E|Angry] LIVID.[B][C]") ]])
        fnCutscene([[ Append("Guy:[E|Neutral] Excuse me?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] 55, I'm sorry I ever doubted you.[P] I was such an idiot to be taken in by such trickery.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] And now I understand everything.[P] The entire veil falls away, and I will never be misled again.[B][C]") ]])
        fnCutscene([[ Append("Guy:[E|Neutral] Uh, am I missing something?[P] Who are you?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] I am Unit 771852, Lord Golem of Maintenance and Repair, Sector 96.[P] My secondary designation is Christine.[B][C]") ]])
        fnCutscene([[ Append("Guy:[E|Neutral] H-[P]huh?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Just shut up and come with me.[P] I'm going to go tear Kernel a new co-routine.") ]])
        fnCutsceneBlocker()
        
        fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
        fnCutsceneBlocker()
        
        fnCutscene([[ AL_BeginTransitionTo("RegulusFlashbackD", "FORCEPOS:30.0x68.0x0") ]])
        fnCutsceneBlocker()
    end
end
