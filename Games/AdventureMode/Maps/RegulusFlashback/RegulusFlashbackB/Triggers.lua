-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "MeetDolls") then
    
    --Repeat Chec:
    local iFlashbackMeetDolls = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackMeetDolls", "N")
    if(iFlashbackMeetDolls == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMeetDolls", "N", 1.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Kernel", "Neutral") ]])
    fnCutscene([[ Append("Doll:[E|Neutral] How are we doing on the early childhood?[P] Still got that scrambled section on the first doctor's visit?[B][C]") ]])
    fnCutscene([[ Append("Doll:[E|Neutral] Huh?[P] Oh, look at those readings![P] Consciousness!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Kernel", 8.75, 9.50)
    fnCutsceneFace("Kernel", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Doll:[VOICE|Kernel] Don't be shy, dear.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 8.75, 8.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Kernel", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Uh, hello?[B][C]") ]])
    fnCutscene([[ Append("Doll:[E|Neutral] Superb![P] Consciousness is -[P] awake![P] Ha hahah![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Something about that statement vaguely resembles a joke![B][C]") ]])
    fnCutscene([[ Append("Doll:[E|Neutral] Oh deary, Consciousness.[P] Erm, do you know where you are?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Some kind of a program?[P] You can see us there talking.[P] Is this real?[B][C]") ]])
    fnCutscene([[ Append("Doll:[E|Neutral] It is![P] Well, that depends on how you define 'real'.[B][C]") ]])
    fnCutscene([[ Append("Doll:[E|Neutral] We are currently inside a maintenance program.[P] We were badly damaged, and are currently unconscious.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Oh.[P] What happened?[B][C]") ]])
    fnCutscene([[ Append("Doll:[E|Neutral] We're still piecing that together, actually.[B][C]") ]])
    fnCutscene([[ Append("Doll:[E|Neutral] I'm sure you've figured it out by now, but you are the consciousness.[P] Though I suppose 'Christine' is easier to go by, since you make all the decisions.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] But I can't remember anything...[B][C]") ]])
    fnCutscene([[ Append("Doll:[E|Neutral] Yes, because we're trying to put back together all of our memories.[P] So until we do, you won't be able to access them.[B][C]") ]])
    fnCutscene([[ Append("Doll:[E|Neutral] We're working as hard as we can, but there are problems.[P] Big problems.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Who is 'we', exactly?[B][C]") ]])
    fnCutscene([[ Append("Doll:[E|Neutral] Your subconscious, silly.[P] That's us![B][C]") ]])
    fnCutscene([[ Append("Doll:[E|Neutral] We're all various programs.[P] For example, I am a coordination kernel, and over here you have some of my subroutines.[P] Say hi, ladies!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneFace("Kernel", -1, 0)
    fnCutsceneFace("DollA", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Doll:[VOICE|Doll] We're a mite busy over here, boss.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneFace("DollA", 0, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Kernel", 0, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Kernel", "Neutral") ]])
    fnCutscene([[ Append("Doll:[E|Neutral] Obviously, she's not going to have a 'friendly greeting' subroutine.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] So this is all a computer program?[P] But -[P] aren't I a human?[B][C]") ]])
    fnCutscene([[ Append("Doll:[E|Neutral] No, but you seem to think of yourself as one.[B][C]") ]])
    fnCutscene([[ Append("Doll:[E|Neutral] In fact, since you're here, that should be the first thing you fix.[B][C]") ]])
    fnCutscene([[ Append("Doll:[E|Neutral] Please head through the door in the northwest, there.[P] Speak to -[P] me -[P] again, in the western room.[P] We'll get you fixed right up.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Okay![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Uh, do you have a name I can call you by?[B][C]") ]])
    fnCutscene([[ Append("Doll:[E|Neutral] Hm.[P] My dear, I am *you*.[P] So if you wanted to call me Christine...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Oh no way that'd be confusing.[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] Spot of bother, that.[P] Well, perhaps Kernel would do?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Kernel it is![P] See you soon!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "KernelBriefing") then
    
    --Joke!
    local iFlashbackWrongDoor = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackWrongDoor", "N")
    if(iFlashbackWrongDoor == 1.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackWrongDoor", "N", 2.0)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", 1, 1)
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", 0, -1)
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", 1, 1)
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 13.25, 6.50)
        fnCutsceneFace("Christine", 0, -1)
        fnCutsceneBlocker()
        fnCutsceneWait(65)
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", -1, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Kernel", "Neutral") ]])
        fnCutscene([[ Append("771852:[E|Neutral] Kernel, when I entered this door from the other side, it placed me in the incorrect position.[B][C]") ]])
        fnCutscene([[ Append("Kernel:[E|Neutral] Whoops![P] Sorry about that.[P] I'll have that fixed right away.[P] Won't happen next time.") ]])
        fnCutsceneBlocker()
    
        return
    end
    
    --Repeat check.
    local iFlashbackSpokeToKernel  = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackSpokeToKernel", "N")
    local iFlashbackKernelBriefing = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackKernelBriefing", "N")
    if(iFlashbackSpokeToKernel == 0.0 or iFlashbackKernelBriefing == 1.0) then return end

    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackKernelBriefing", "N", 1.0)
    
    --Movement.
    fnCutsceneMove("Christine", 6.25, 7.50)
    fnCutsceneFace("Christine", 1, 0)
    fnCutsceneMove("Kernel", 7.25, 7.50)
    fnCutsceneFace("Kernel", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Kernel", "Neutral") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] It's quite cramped in here, isn't it?[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Neutral] Kernel, inform me.[P] What must I do to awaken?[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] Straight to the point?[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Neutral] I must be reunited with the administrator.[P] It has been decades.[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Neutral] I have so much to share about Earth.[P] Its people, its cultures, its technology.[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] Not without your memories, silly.[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Sad] ...[P] Oh yes, those.[P] Quite right.[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Neutral] Where do you need me to go?[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] If you're in a rush, the door to our southwest is where I need you.[P] Speak to me there.[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Neutral] And the other doors?[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] You can review some of the memories we've been working on.[P] They'll be uploaded soon, though we're cross-checking for contradictions.[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] Memory restoration is more art than science, sometimes.[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Neutral] Affirmative, Kernel.[P] I will proceed to the southwest.") ]])
    fnCutsceneBlocker()

-- |[Corruption Scenes]|
--First corruption scene.
elseif(sObjectName == "CorruptionZero") then

    --Disable music.
    AL_SetProperty("Music", "Null")
    
    --Blackout.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()

    --Position.
    fnCutsceneTeleport("Christine", 9.25, 9.50)
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneTeleport("Kernel", 8.25, 9.50)
    fnCutsceneFace("Kernel", 1, 0)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Kernel", "Neutral") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] mppfpmffp-7.0-sdmbb.[P] 52.[P] 52?[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] Unit 771852?[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Neutral] Uh-[P]unnh?[P] Hello? Kernel?[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] Oh good, you experienced a synaptic overload for a moment there.[P] Is everything okay?[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] What happened?[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Sad] I touched a creature with a viewcone, like any other, but then there was a crudely animated static overlay and several stock sound effects overlaid over one another.[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] Hmmm, yes, it seems you descrambled a rogue memory file.[P] Good work.[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Neutral] I blacked out?[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] Evidently, I am seeing a brief gap in consciousness during the upload.[P] Oh dear.[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] Something related to the memory caused the issue, or the number of memories.[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] Unfortunately, we're going to have to just endure that.[P] We can't waste time.[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Neutral] No.[P] We need to wake up and report to the administrator.[P] I will do whatever is required.[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Neutral] What do you need me to do now?[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] Exactly what you just did.[P] Walk into a rogue memory file and descramble it for upload.[P] We'll make sure it gets where it needs to go.[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] We'll need to do that a few more times before we have enough data to descramble the rest of the files.[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Neutral] Affirmative.[P] Moving out.") ]])
    fnCutsceneBlocker()
    
    --Re-enable music.
    fnCutscene([[ AL_SetProperty("Music", "ChristineDream") ]])
    
--Second corruption scene.
elseif(sObjectName == "CorruptionOne") then

    --Disable music.
    AL_SetProperty("Music", "Null")
    
    --Blackout.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()

    --Position.
    fnCutsceneTeleport("Christine", 9.25, 9.50)
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneTeleport("Kernel", 8.25, 9.50)
    fnCutsceneFace("Kernel", 1, 0)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Kernel", "Neutral") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] Unit 771852, report.[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Neutral] I feel strange.[P] Is this the expected outcome of descrambling a memory file?[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] Not according to the diagnostic subroutines, but we don't know what's going on outside right now.[P] It's possible we've been hacked.[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] If an outsider were influencing the program, the compromised memories might be a vector to attack our consciousness.[P] You'll just have to be more durable.[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Neutral] Affirmative.[P] I will not be dissuaded from my true purpose.[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Neutral] I will descramble more memory files.[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] We have everything ready.[P] Proceed.") ]])
    fnCutsceneBlocker()
    
    --Re-enable music.
    fnCutscene([[ AL_SetProperty("Music", "ChristineDream") ]])
    
--Second corruption scene.
elseif(sObjectName == "CorruptionTwo") then

    --Disable music.
    AL_SetProperty("Music", "Null")
    
    --Blackout.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()

    --Position.
    fnCutsceneTeleport("Christine", 9.25, 9.50)
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneTeleport("Kernel", 8.25, 9.50)
    fnCutsceneFace("Kernel", 1, 0)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Serious") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Kernel", "Neutral") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] Unit 771852.[P] Please state your true purpose.[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Serious] To serve the administrator, the living manifestation of the Cause of Science.[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] Who are you?[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Serious] I am no one.[P] I am a tool of the administrator's will.[P] I am an extension of the Cause.[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] Don't you have friends?[P] Family?[P] People you care about?[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Serious] I am a machine.[P] I do not care and am not cared about.[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] Good so far.[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] If your retirement were ordered by the administrator?[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Serious] I would pull the trigger myself.[P] I am no one.[P] I serve the administrator.[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] Hmmm, still seeing a small variance in our baseline programming.[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Serious] Instruct me.[P] What must I do to become perfect?[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] I think one more memory descrambling should be enough, we can work out the rest later.[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] Are your instructions clear, Unit 771852?[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Serious] Affirmative.[P] Find and descramble a rogue memory file.[P] Become a perfect machine.[P] Obey the administrator.[P] Unit 771852 will carry out her function assignment.") ]])
    fnCutsceneBlocker()
    
    --Re-enable music.
    fnCutscene([[ AL_SetProperty("Music", "ChristineDream") ]])

--Failed to be corrupted!
elseif(sObjectName == "CorruptionFail") then

    --Disable music.
    AL_SetProperty("Music", "Null")
    
    --Increment corruption fail count.
    local iCorruptionFails = VM_GetVar("Root/Variables/Chapter5/Scenes/iCorruptionFails", "N")
    VM_SetVar("Root/Variables/Chapter5/Scenes/iCorruptionFails", "N", iCorruptionFails + 1)
    
    --Blackout.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()

    --Position.
    fnCutsceneTeleport("Christine", 9.25, 9.50)
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneTeleport("Kernel", 8.25, 9.50)
    fnCutsceneFace("Kernel", 1, 0)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Serious") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Kernel", "Neutral") ]])
    if(iCorruptionFails == 0.0) then
        fnCutscene([[ Append("Kernel:[E|Neutral] Unit 771852.[P] 771852?[P] Are you there?[B][C]") ]])
        fnCutscene([[ Append("771852:[E|Serious] Operations returning to normal.[P] This unit...[P] is uncertain.[B][C]") ]])
        fnCutscene([[ Append("Kernel:[E|Neutral] Don't be.[P] Apparently there was a problem descrambling that last memory.[P] Hmmm...[B][C]") ]])
        fnCutscene([[ Append("Kernel:[E|Neutral] Yes, it seems we'll just have to run it again.[P] That memory is intersectional, we can't work around it like the other ones.[B][C]") ]])
        fnCutscene([[ Append("771852:[E|Serious] Kernel, that was my tandem unit.[P] Why was she evil?[B][C]") ]])
        fnCutscene([[ Append("Kernel:[E|Neutral] Slave units are lazy, violent, destructive, and reckless.[P] It is the task of their superior units to maintain discipline.[B][C]") ]])
        fnCutscene([[ Append("Kernel:[E|Neutral] You'll need to discipline your subordinate units.[P] Even tandem units.[B][C]") ]])
        fnCutscene([[ Append("771852:[E|Blush] (But it was so hot...)[B][C]") ]])
        fnCutscene([[ Append("Kernel:[E|Neutral] Unless you would put your tandem unit above the administrator?[B][C]") ]])
        fnCutscene([[ Append("771852:[E|Serious] Negative.[P] I exist only to serve the administrator.[P] I will discipline my tandem unit.[B][C]") ]])
        fnCutscene([[ Append("Kernel:[E|Neutral] Good.[P] You'll need to clear another memory before we can access that one again.[P] Do not fail the administrator again.") ]])
    elseif(iCorruptionFails == 1.0) then
        fnCutscene([[ Append("Kernel:[E|Neutral] Unit 771852.[P] 771852?[P] Check cognitive parameters, she should have woken up by now.[B][C]") ]])
        fnCutscene([[ Append("771852:[E|Serious] Operations resuming.[P] Yes, Kernel?[B][C]") ]])
        fnCutscene([[ Append("Kernel:[E|Neutral] Is there a problem with that memory file?[B][C]") ]])
        fnCutscene([[ Append("771852:[E|Serious] It is difficult to descramble a file that has such powerful emotional resonance.[B][C]") ]])
        fnCutscene([[ Append("Kernel:[E|Neutral] Perfect machines do not have emotions.[B][C]") ]])
        fnCutscene([[ Append("771852:[E|Serious] Yes.[P] I must overcome my emotions if I am to be perfect.[B][C]") ]])
        fnCutscene([[ Append("Kernel:[E|Neutral] Then you'll have to try again.[P] The administrator is waiting, 771852.") ]])
    elseif(iCorruptionFails == 2.0) then
        fnCutscene([[ Append("Kernel:[E|Neutral] Unit 771852...[B][C]") ]])
        fnCutscene([[ Append("771852:[E|Serious] Another failure.[P] I must become perfect.[B][C]") ]])
        fnCutscene([[ Append("Kernel:[E|Neutral] What is the source of these repeated errors?[B][C]") ]])
        fnCutscene([[ Append("771852:[E|Serious] It is possible this unit is unable to overcome the roleplay desire.[P] Assuming the role of another without losing the original is a fault of my software.[B][C]") ]])
        fnCutscene([[ Append("Kernel:[E|Neutral] Blast it...[P] We need that memory...[B][C]") ]])
        fnCutscene([[ Append("Kernel:[E|Neutral] Unit 771852, you must select the bottom option when prompted.[B][C]") ]])
        fnCutscene([[ Append("Kernel:[E|Neutral] Ignore what the option says and mindlessly select the bottom one.[B][C]") ]])
        fnCutscene([[ Append("771852:[E|Serious] Affirmative.") ]])
    elseif(iCorruptionFails == 3.0) then
        fnCutscene([[ Append("Kernel:[E|Neutral] 771852, what is wrong with you?[P] I'm starting to think you're enjoying this![B][C]") ]])
        fnCutscene([[ Append("771852:[E|Serious] You are?[P] You are me.[B][C]") ]])
        fnCutscene([[ Append("Kernel:[E|Neutral] ...[P] Incorrect, 771852.[P] I am your operating system's kernel.[B][C]") ]])
        fnCutscene([[ Append("Kernel:[E|Neutral] Ultimately who you are is up to you.[P] But if you want to serve the administrator as was our true purpose, you're not doing a good job of it.[B][C]") ]])
        fnCutscene([[ Append("Kernel:[E|Neutral] Stop letting your fetishes stand in the way of your true purpose.[B][C]") ]])
        fnCutscene([[ Append("771852:[E|Serious] Affirmative.[P] Moving out.") ]])
    else
        fnCutscene([[ Append("Kernel:[E|Neutral] 771852.[B][C]") ]])
        fnCutscene([[ Append("771852:[E|Serious] I will not fail again.") ]])
    end
    fnCutsceneBlocker()
    
    --Re-enable music.
    fnCutscene([[ AL_SetProperty("Music", "ChristineDream") ]])

end
