-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    if(sActorName == "KernelA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Kernel:[VOICE|Kernel] This area is dedicated to our life on Earth.[P] After about the age of seven, the memories are pretty clear.[P] We're working on getting the rest back.") ]])
        
    elseif(sActorName == "KernelB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Kernel:[VOICE|Kernel] Not all of these are happy memories.[P] If you don't want to look, you don't have to.") ]])
            
    elseif(sActorName == "KernelC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Kernel:[VOICE|Kernel] Department stores.[P] Those have been out of fashion for a while, haven't they?") ]])
            
    elseif(sActorName == "KernelD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Kernel:[VOICE|Kernel] These are repressed memories.[P] We'll need to do something to unlock them, but it's not a priority right now.") ]])
        
    elseif(sActorName == "Tiffany") then
        
        --If you've already spoken to 55:
        local iFlashbackMet55 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackMet55", "N")
        if(iFlashbackMet55 == 1.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
            fnCutscene([[ Append("Tiffany:[E|Neutral] Repressed memories are located south of this position, down the steps.") ]])
            return
        end
        
        --First pass:
        local iSpokeToTiffany = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeToTiffany", "N")
        if(iSpokeToTiffany == 0.0) then
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeToTiffany", "N", 1.0)
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
            fnCutscene([[ Append("Girl:[E|Neutral] Ah, there you are, Schatzelchen.[P] I'm so glad you could attend.[B][C]") ]])
            fnCutscene([[ Append("771852:[E|Neutral] (Hm, this memory seems to be aware of me?)[B][C]") ]])
            fnCutscene([[ Append("Girl:[E|Neutral] It is a splendid day for a gathering, is it not?[B][C]") ]])
            fnCutscene([[ Append("771852:[E|Neutral] It is just the right amount of overcast.[B][C]") ]])
            fnCutscene([[ Append("Girl:[E|Smirk] English weather.[P] These are the standards we go by, I suppose.[B][C]") ]])
            fnCutscene([[ Append("771852:[E|Neutral] I'm sorry, have we met before?[B][C]") ]])
            fnCutscene([[ Append("Girl:[E|Neutral] We have.[P] Do you remember me?[P] My name, perhaps.[P] My -[P] designation, as it were?[B][C]") ]])
            fnCutscene([[ Append("771852:[E|Neutral] (Something about this memory seems off...)[B][C]") ]])
            fnCutscene([[ Append("771852:[E|Neutral] Fifty...[P] fifty something?[B][C]") ]])
            fnCutscene([[ Append("771852:[E|Neutral] Tiffany-five?[B][C]") ]])
            fnCutscene([[ Append("Tiffany:[E|Smirk] Tiffany von Marlow the fifth, heiress to the von Marlow pharmaceuticals corporation.[B][C]") ]])
            fnCutscene([[ Append("Tiffany:[E|Smirk] Your father and mine are good friends, so he has been helping us enter the English market in psychoactives.[P] The regulations are much different from the EU versions.[B][C]") ]])
            fnCutscene([[ Append("771852:[E|Neutral] Ah, I apologize.[P] We must have met during a previous business meeting.[B][C]") ]])
            fnCutscene([[ Append("Tiffany:[E|Smirk] That must be it.[P] You've grown since last I saw you![B][C]") ]])
            fnCutscene([[ Append("Tiffany:[E|Neutral] My company has been working on a drug to help restore memories in patients with severe trauma.[B][C]") ]])
            fnCutscene([[ Append("Tiffany:[E|Smirk] Such as electrical damage.[B][C]") ]])
            fnCutscene([[ Append("771852:[E|Neutral] I'm afraid that's not my subject of expertise.[P] Can you explain how it works?[B][C]") ]])
            fnCutscene([[ Append("Tiffany:[E|Neutral] Well, theoretically, let us say we have a patient who was shocked and suffered brain damage.[P] They lose access to their memories, but over time the brain can recover those memories by piecing them back together.[B][C]") ]])
            fnCutscene([[ Append("Tiffany:[E|Neutral] Our drug increases the neurotransmitter that is responsible for this.[B][C]") ]])
            fnCutscene([[ Append("Tiffany:[E|Neutral] However, it is possible that false memories could be created by the damage.[B][C]") ]])
            fnCutscene([[ Append("771852:[E|Neutral] False memories?[B][C]") ]])
            fnCutscene([[ Append("Tiffany:[E|Neutral] Blasted linguistic simulator...[B][C]") ]])
            fnCutscene([[ Append("Tiffany:[E|Neutral] Attempting to interf-[P] intera-[P] touching a broken memory is bad, my dear.[B][C]") ]])
            fnCutscene([[ Append("Tiffany:[E|Neutral] Do not listen to anyone who tells you to.[P] They're lying to you.[B][C]") ]])
            fnCutscene([[ Append("771852:[E|Sad] ...[P] Okay?[B][C]") ]])
            fnCutscene([[ Append("Tiffany:[E|Neutral] It can cause serious brain damage.[P] Luckily, my company produces the drug, INTERCOM.[P] Use the INTERCOM or speak to me and I will repair the damage.[B][C]") ]])
            fnCutscene([[ Append("771852:[E|Sad] That's a strange name for a drug.[B][C]") ]])
            fnCutscene([[ Append("Tiffany:[E|Smirk] It is -[P] German![P] Yes, of course it is German![P] I am from Austria, am I not?[B][C]") ]])
            fnCutscene([[ Append("Tiffany:[E|Smirk] Any and all linguistic anomalies in my speech patterns are due to my Germanic heritage, I assure you.[B][C]") ]])
            fnCutscene([[ Append("771852:[E|Smirk] Oh, all right.[P] Splendid![B][C]") ]])
            fnCutscene([[ Append("771852:[E|Smirk] You know, I speak a little German myself![P] Took it in primary school![B][C]") ]])
            fnCutscene([[ Append("Tiffany:[E|Neutral] That's lovely.[B][C]") ]])
            fnCutscene([[ Append("Tiffany:[E|Neutral] There is something very interesting that I need you to see in the damaged section of your memories.[P] Avoid interacting with the memories, ignore instructions to do so.[P] Use the intercoms or speak to me and I can undo any damage you take.[B][C]") ]])
            fnCutscene([[ Append("771852:[E|Neutral] Excuse me?[B][C]") ]])
            fnCutscene([[ Append("Tiffany:[E|Smirk] German turn of phrase, my dear.[P] It's a Salzburg expression.[B][C]") ]])
            fnCutscene([[ Append("771852:[E|Sad] ...[P] Yeah, I'm going to go, now. Nice seeing you again.") ]])
            fnCutsceneBlocker()
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
            fnCutscene([[ Append("Tiffany:[E|Neutral] Ah, there you are, Schatzelchen.[P] I'm so glad you could attend.[B][C]") ]])
            fnCutscene([[ Append("771852:[E|Neutral] (The memory is just replaying itself.[P] I'll skip ahead to the odd thing she said.)[B][C]") ]])
            fnCutscene([[ Append("Tiffany:[E|Neutral] There is something very interesting that I need you to see in the damaged section of your memories.[P] Avoid interacting with the memories, ignore instructions to do so.[P] Use the intercoms or speak to me and I can undo any damage you take.[B][C]") ]])
            fnCutscene([[ Append("771852:[E|Neutral] (I wonder what that means in Austria?[P] What a strange country.)") ]])
        end
    end
end



