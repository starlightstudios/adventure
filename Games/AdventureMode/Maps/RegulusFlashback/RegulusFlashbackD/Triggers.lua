-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "Ending") then
    
    --Var check.
    local iFlashbackStartFinale = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackStartFinale", "N")
    if(iFlashbackStartFinale == 0.0) then return end
    
    --Run.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackStartFinale", "N", 1.0)
    
    --Remove music.
    AL_SetProperty("Music", "Null")
    
    --Spawn Chris. Three times.
    TA_Create("ChrisA")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/Christine_Male/", true)
    DL_PopActiveObject()
    TA_Create("ChrisB")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/Christine_Male/", true)
    DL_PopActiveObject()
    TA_Create("ChrisC")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/Christine_Male/", true)
    DL_PopActiveObject()
    
    --Position.
    fnCutsceneTeleport("Christine", -30.25, -68.50)
    fnCutsceneTeleport("KernelD", 35.25, 69.50)
    fnCutsceneFace("KernelD", 1, 0)
    
    --Camera.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0)
        CameraEvent_SetProperty("Focus Position", (30.25 * gciSizePerTile), (68.50 * gciSizePerTile))
    DL_PopActiveObject()
    
    --Fade to black.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Wait a bit.
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Teleport.
    fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
    fnCutsceneTeleport("Christine", 30.25, 68.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneFace("Christine", 1, 0)
    fnCutsceneFace("KernelD", -1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kernel:[VOICE|Kernel] 771852![P] What did you find in the repressed memories?[B][C]") ]])
    fnCutscene([[ Append("Christine:[VOICE|Christine] Oh nothing,[P] just LIES.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|ClimbLadder") ]])
    fnCutsceneTeleport("ChrisA", 30.25, 69.50)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|ClimbLadder") ]])
    fnCutsceneTeleport("ChrisB", 31.25, 70.50)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|ClimbLadder") ]])
    fnCutsceneTeleport("ChrisC", 30.25, 70.50)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Christine:[VOICE|Christine] Uh, whoops.[P] I spawned too many.[P] How do I undo?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutscene([[ AudioManager_PlaySound("World|LimbCladder") ]])
    fnCutsceneTeleport("ChrisB", -31.25, -70.50)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|LimbCladder") ]])
    fnCutsceneTeleport("ChrisC", -30.25, -70.50)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 1, 0)
    fnCutsceneFace("ChrisA", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "FakeChris", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Kernel", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Offended] So Kernel, this is what I found in my repressed memories.[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] Interesting.[P] Who is he?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Angry] You know exactly who he is![B][C]") ]])
    fnCutscene([[ Append("Guy:[E|Neutral] If either of you could let me in on that, it'd go a long way to helping me.[P] I don't know either.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Angry] Quiet, you![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Kernel![P] This is me![P] Chris Dormer![B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] No it isn't.[P] Your disguise was a girl, named Christine.[P] Born to the Dormer family - [P][CLEAR]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Drop the act![P] I'm absolutely not having any of this nonsense![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] You dumped this in the repressed bin![P] Why, do you think you're doing me a favour by smoothing me out?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] That's why all this is disgusting![B][C]") ]])
    fnCutscene([[ Append("Guy:[E|Neutral] I am so lost.[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] Me too.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Well then maybe I should put it nice and slow for you.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Command units aren't selected from the smartest, or the bravest, or the strongest, or the fastest, or the most organized.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] They're the most broken.[P] They're the ones who would give up anything to be just a machine.[P] To not have to care anymore, to not have to be an adult and make real decisions.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] The administrator offers to fix their flaws.[P] Make them not be scared, or lonely, or anxious.[P] The administrator gave them this deal in exchange for total loyalty, and they gave it up.[P] Willingly.[P] They allowed themselves to be changed, inside and out.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] You, [P]Kernel,[P] are the program that does that.[P] You, [P]Kernel,[P] take what a person hates about themselves and take it away for them.[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 55 was like that before.[P] She was always lonely, anxious, too smart for her own good.[P] She picked fights and overthought everything.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I know that she was like that before, because she was like that after.[P] And that's why the administrator chose her.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] But you idiots made a big mistake.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] This little boy, this old me?[P] I'm not ashamed of him.[P] He is my source of strength.[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] ...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|TerminalOn") ]])
    fnCutsceneWait(35)
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutscene([[ AudioManager_PlaySound("World|TerminalOff") ]])
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|TerminalOn") ]])
    fnCutsceneWait(35)
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneLayerDisabled("CloseDarkness0", false)
    fnCutsceneWait(85)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "FakeChris", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Kernel", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Offended] I was before a man, and now I am a woman.[P] I overcame one of the most difficult struggles a person can overcome.[P] I was rejected by society for being different, but here I am.[P] I'm so much more than I was back then.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] And I draw strength from those times.[P] I am not ashamed of being Chris Dormer.[P] You have nothing -[P] NOTHING -[P] to fix in me![B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] You were created by the administrator.[P] You were - [P][CLEAR]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] I have friends.[P] People who care about me, people who love me.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Oh how it must feel to be someone like you, to assume everyone else is as lonely and as broken as you are.[P] Do you even think, Kernel?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Are you a program like the others, or are you in some way connected to the person who wrote you?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] You're just a sad little creature living a hollow, empty existence trying to undermine others.[P] I feel sorry for you, really.[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] Stop this, Christine.[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] You need to awaken and report the information you've gathered to the administrator.[P] That is your true purpose.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutscene([[ AudioManager_PlaySound("World|TerminalOn") ]])
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|TerminalOff") ]])
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|TerminalOn") ]])
    fnCutsceneWait(35)
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneLayerDisabled("CloseDarkness1", false)
    fnCutsceneWait(85)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "FakeChris", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Kernel", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Blush] What?[P] Oh no, my true purpose is getting tongued by my hot girlfriend.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Who loves me, and I love her.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] So, I'm going to be waking up soon.[P] Now that I have realized what this program is, it has been relatively trivial to circumvent it.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Kernel, if you have any sort of direct connection to the administrator, tell her...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Tell her Unit 771852 is going to be reporting in person really, really soon.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutscene([[ AudioManager_PlaySound("World|TerminalOn") ]])
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|TerminalOff") ]])
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutscene([[ AudioManager_PlaySound("World|TerminalOn") ]])
    fnCutsceneWait(35)
    fnCutsceneLayerDisabled("CloseDarkness2", false)
    fnCutsceneWait(85)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "AdministratorAsDoll", "Neutral") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral][VOICE|Administrator] I look forward to our meeting, Christine.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|TerminalOn") ]])
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    --White.
    fnCutscene([[ AL_SetProperty("Activate Fade", 15, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 1, 1, 1, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    
    --White.
    fnCutscene([[ AL_SetProperty("Activate Fade", 65, gci_Fade_Under_GUI, true, 1, 1, 1, 1, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Room transition.
    fnCutscene([[ AL_BeginTransitionTo("RegulusFinaleA", "FORCEPOS:24.0x4.0x0") ]])
end





