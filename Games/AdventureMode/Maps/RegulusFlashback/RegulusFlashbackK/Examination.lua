-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToFlashbackC") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusFlashbackC", "FORCEPOS:22.0x4.0x0")
    
-- |[Objects]|
elseif(sObjectName == "AdministratorConsole") then

    --Variables.
    local iFlashbackMetAdministrator = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackMetAdministrator", "N")
    
    --First time:
    if(iFlashbackMetAdministrator == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMetAdministrator", "N", 1.0)
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] (Oh, this is in the memory now?[P] I'll just let that take over...)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Administrator.[P] I am in the datacore now.[P] What are your instructions?[B][C]") ]])
        fnCutscene([[ Append("Administrator:[VOICE|Administrator] Very well done, Unit Zero.[P] Your performance in training has been exemplary so far.[B][C]") ]])
        fnCutscene([[ Append("Administrator:[VOICE|Administrator] It is thus with a heavy heart that I must finally give you your true purpose.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Affirmative, administrator.[P] Please input true purpose.[B][C]") ]])
        fnCutscene([[ Append("Administrator:[VOICE|Administrator] Unit Zero, what do you see around you?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] The server architecture that governs Regulus City.[B][C]") ]])
        fnCutscene([[ Append("Administrator:[VOICE|Administrator] Yes.[P] You see the knowledge we have gathered here.[P] Is it not breathtaking?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] This unit does not breathe.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] (Sheesh![P] Did I not have a sense of humour?)[B][C]") ]])
        fnCutscene([[ Append("Administrator:[VOICE|Administrator] Hahah![P] You remind me of another of my favoured command units.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Affirmative, administrator.[P] Thank you, administrator.[B][C]") ]])
        fnCutscene([[ Append("Administrator:[VOICE|Administrator] No, Unit Zero.[P] This knowledge we have gathered concerns but one of the realities that surround us.[P] There are more, several more.[P] We know nothing of them save legends that have filtered through the ages.[B][C]") ]])
        fnCutscene([[ Append("Administrator:[VOICE|Administrator] That is why I have created you.[P] That is why I have created the runestone you now hold.[B][C]") ]])
        fnCutscene([[ Append("Administrator:[VOICE|Administrator] I need my most loyal, most intelligent, most capable unit to go to these other realities.[P] You must go to them, learn from them, and bring back their knowledge.[B][C]") ]])
        fnCutscene([[ Append("Administrator:[VOICE|Administrator] And to these places, the Cause of Science will be spread.[P] What you see around you is what we have accomplished on one small moon in one tiny corner of one reality.[B][C]") ]])
        fnCutscene([[ Append("Administrator:[VOICE|Administrator] The knowledge we can gain if we can visit these other places at will...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I do not understand, administrator.[P] Why have you selected me?[B][C]") ]])
        fnCutscene([[ Append("Administrator:[VOICE|Administrator] Because, Unit Zero.[P] Because you are my daughter.[P] My perfect unit.[B][C]") ]])
        fnCutscene([[ Append("Administrator:[VOICE|Administrator] You shall visit the benighted people of this 'Still Plane' and bring to them the Cause of Science.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] And this runestone?[B][C]") ]])
        fnCutscene([[ Append("Administrator:[VOICE|Administrator] It will allow you to travel there, to integrate yourself among them.[P] It will keep you safe.[P] And it will recall you when you are needed here.[B][C]") ]])
        fnCutscene([[ Append("Administrator:[VOICE|Administrator] This is your true purpose, my favoured unit.[P] To execute my will.[B][C]") ]])
        fnCutscene([[ Append("Administrator:[VOICE|Administrator] I now entrust to you the judgement to act as you will.[P] The console you stand before has full access to the entire archives of Regulus City.[B][C]") ]])
        fnCutscene([[ Append("Administrator:[VOICE|Administrator] Take whatever information you think you need.[P] Whenever you are ready, clutch the runestone to your chest and concentrate.[B][C]") ]])
        fnCutscene([[ Append("Administrator:[VOICE|Administrator] Do not weep, my beloved daughter.[P] I shall see you again when you task is done.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Thank you, administrator.[P] I will not fail you.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] ...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] ...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I remember there was a flash of light, and then...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] That must be the end of the memory...") ]])
        fnCutsceneBlocker()
        
        --Spawn Kernel.
        TA_Create("Kernel")
            TA_SetProperty("Position", 20, 57)
            TA_SetProperty("Facing", gci_Face_East)
            TA_SetProperty("Clipping Flag", true)
            TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
            fnSetCharacterGraphics("Root/Images/Sprites/Doll/", false)
        DL_PopActiveObject()

    --Repeats:
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The administrator did not even see me off before I left...[P] But I shall be forever loyal...)") ]])
        fnCutsceneBlocker()
    end

-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
