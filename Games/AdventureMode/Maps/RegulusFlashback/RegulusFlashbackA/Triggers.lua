-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "FlashbackWakeup") then
    
    --Disable warps.
    VM_SetVar("Root/Variables/Chapter5/WorldState/iNoWarpsAllowed", "N", 1.0)
    
    --Repeat check.
    local iFlashbackTrueWakeup = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackTrueWakeup", "N")
    if(iFlashbackTrueWakeup == 1.0) then return end
    
    --Variables:
    local b55Exists = EM_Exists("Tiffany")
    local bSX399Exists = EM_Exists("SX-399")
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackTrueWakeup", "N", 1.0)
    
    --Position Christine. All other characters are offscreen.
    fnCutsceneTeleport("Christine", 6.75, 4.50)
    fnCutsceneFace("Christine", 0, -1)
    if(b55Exists == true) then fnCutsceneTeleport("Tiffany", -1.75, -1.50) end
    if(iSX399JoinsParty == 1.0 and bSX399Exists) then fnCutsceneTeleport("SX-399", -1.75, -1.50) end
    
    --Remove characters from the party.
    giFollowersTotal = 0
    gsaFollowerNames = {}
    giaFollowerIDs = {0}
	AL_SetProperty("Unfollow Actor Name", "Tiffany")
	VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
    if(iSX399JoinsParty == 1.0) then 
        AL_SetProperty("Unfollow Actor Name", "SX-399") 
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399IsFollowing", "N", 0.0)
    end
    
    --Remove all non-Christine members.
    for i = 0, gciCombat_MaxActivePartySize-1, 1 do
        local sMemberName = AdvCombat_GetProperty("Name of Active Member", i)
        if(sMemberName ~= "Christine") then
            AdvCombat_SetProperty("Party Slot", i, "Null")
        end
    end
    
    --Black screen.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    --Remove Christine's current special frame.
    fnCutsceneSetFrame("Christine", "NULL")
    
    --Sounds.
    fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(55)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Christine:[VOICE|Christine] No, it's not in here either.[B][C]") ]])
    fnCutscene([[ Append("Christine:[VOICE|Christine] If I knew what I was looking for, it'd be so much easier to find.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Sounds.
    fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(55)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Christine:[VOICE|Christine] Well, at least I found out my name is Christine.[P] Good thing that text box appeared as I was speaking to myself.[B][C]") ]])
    fnCutscene([[ Append("Christine:[VOICE|Christine] Wait, text box?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Christine:[VOICE|Christine] There's a text box at the bottom of the screen?[B][C]") ]])
    fnCutscene([[ Append("Christine:[VOICE|Christine] There's a screen?[B][C]") ]])
    fnCutscene([[ Append("Christine:[VOICE|Christine] Oh, yes, that makes sense.[P] I must be inside a program.[B][C]") ]])
    fnCutscene([[ Append("Christine:[VOICE|Christine] There's also a door over there.[P] Maybe I'll find whatever it was I was looking for over there?") ]])
    fnCutsceneBlocker()
    
end
