-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
-- |[Objects]|
if(sObjectName == "Closet") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (It's full of women's clothes.[P] A label on the outside says 'Goth Phase'.)") ]])
    
elseif(sObjectName == "Bed") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A bed.[P] It's too small to be mine.[P] Mine would be four times this size!)") ]])
    
elseif(sObjectName == "Bookshelf") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (All the titles are blank, and the book's pages are filled with nonsense about cleaning windows and bears eating porridge.)") ]])
    
elseif(sObjectName == "Plant") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (It's a plant.[P] I don't know what species.)") ]])
    
elseif(sObjectName == "Chair") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (It's a chair.[P] Often known as objects one can sit on, but never underestimate their potency as a projectile.)") ]])
    
elseif(sObjectName == "EndTable") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (An end table, not placed next to a chesterfield or a bed.[P] This room's Feng Shui is terrible.)") ]])
    
elseif(sObjectName == "CoatRack") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A coat rack covered in a drape.[P] There's a thick brown leather trenchcoat under it.)") ]])

-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
