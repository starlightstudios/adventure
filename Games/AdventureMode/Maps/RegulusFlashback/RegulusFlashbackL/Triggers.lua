-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "BDSMScene") then
    
    --Spawn characters.
    TA_Create("Kernel")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/Doll/", false)
    DL_PopActiveObject()
    fnSpecialCharacter("Sophie", -100, -100, gci_Face_East, false, nil)
    EM_PushEntity("Sophie")
        fnSetCharacterGraphics("Root/Images/Sprites/SophieLeather/", true)
    DL_PopActiveObject()
    
    --Change Sophie to her BDSM variant. She doesn't appear for the rest of the chapter anyway.
    DialogueActor_Push("Sophie")
        DialogueActor_SetProperty("Add Emotion", "Neutral",   "Root/Images/Portraits/SophieDialogueDomina/Neutral",   true)
        DialogueActor_SetProperty("Add Emotion", "Happy",     "Root/Images/Portraits/SophieDialogueDomina/Happy",     true)
        DialogueActor_SetProperty("Add Emotion", "Blush",     "Root/Images/Portraits/SophieDialogueDomina/Blush",     true)
        DialogueActor_SetProperty("Add Emotion", "Smirk",     "Root/Images/Portraits/SophieDialogueDomina/Smirk",     true)
    DL_PopActiveObject()

    --Blackout.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Load assets.
    fnLoadDelayedBitmapsFromList("Chapter 5 Leather Sophie", gciDelayedLoadLoadAtEndOfTick)
    
    --Position.
    fnCutsceneTeleport("Christine", 28.25, 13.50)
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneTeleport("Kernel", 23.25, 10.00)
    fnCutsceneFace("Kernel", 0, 1)
    fnCutsceneBlocker()
    
    --Wait a bit.
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutsceneWait(15)
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 26.25, 13.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Serious") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Kernel", "Neutral") ]])
    fnCutscene([[ Append("771852:[E|Serious] (Memory File 5506902-BBRCav.9, variant zero.[P] 'Meeting Tandem Unit'.[P] File is pending verification by consciousness algorithm.)[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] Unit 771852.[P] This should be the last file we need descrambled.[P] Are you ready?[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Serious] Affirmative.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Sophie appears.
    fnCutscene([[ AudioManager_PlaySound("World|Flash") ]])
    fnCutsceneWait(42)
    fnCutsceneBlocker()
    fnCutsceneTeleport("Sophie", 19.25, 13.50)
    fnCutsceneFace("Sophie", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(85)
    fnCutsceneBlocker()

    --Clear censor bars.
    WD_SetProperty("Clear Censor Bars")
    
    --Scene.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Activate Scene") ]])
    fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/SophieLeather/SophieLeather") ]])
    fnCutscene([[ Append("[VOICE|Christine]Unit 499323 'Sophie' wearing normal slave unit clothes.[P] She is visibly menacing and likely unproductive.[B][C]") ]])
    fnCutscene([[ Append("[VOICE|Christine]Begin scan for object anomalies...[P] Memory playback beginning.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Serious") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Greetings, tandem unit.[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Serious] Greetings, tandem unit.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Are you prepared to begin our manual tasks for the day?[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Serious] Affirmative.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Well, I'm not.[P] I'm afraid we'll be deviating from the usual.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] You upper-class types disgust me.[P] I've hated you since the day we met.[P] I've only been manipulating you.[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Serious] (No deviations from expected slave golem behavior detected.)[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Like all slave golems, efficiency can only be extracted through force.[P] Are you going to stop me, Unit 771852?[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Serious] No.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] No?[P] Then perhaps you'd like to get on the floor.[P] KNEEL![B][C]") ]])
    fnCutscene([[ Append("771852:[E|Blush] Kneel?[P] Kneel before you?[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Blush] (Memory descramble interrupted.[P] Consciousness algorithm requests clear-up protocol.[P] Proceed.)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Unload assets.
    fnCutscene([[ fnUnloadBitmapsFromList("Chapter 5 Leather Sophie") ]])
    
    --Movement.
    fnCutsceneMove("Christine", 20.25, 13.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Serious") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Blush") ]])
    fnCutscene([[ Append("771852:[E|Blush] Unit 499323...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] On your knees![P] Worship me, worm![B][C]") ]])
    fnCutscene([[ Append("771852:[E|Blush] ...[P] Oh my goodness...[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Blush] (Should I get on my knees and kiss her metal feet?)[BLOCK]") ]])

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Worship her...\", " .. sDecisionScript .. ", \"Worship\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Discipline the unit\",  " .. sDecisionScript .. ", \"Discipline\") ")
    fnCutsceneBlocker()

--Worship. Returns to main area.
elseif(sObjectName == "Worship") then
	WD_SetProperty("Hide")
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Blush") ]])
    fnCutscene([[ Append("Christine:[E|Blush] Mmmm, yes my goddess.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Kiss my feet, you worthless sack of bolts![P] Kiss them until your goddess tells you to stop![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] Right away my goddess...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] *smooch*[P] *lick*[P] mmmm[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Ha ha![P] Be careful, Christine![P] Your tongue tickles![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] *Don't break character, Sophie!*[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] I can't![P] Hee hee![P] It tickles![P] Oooh, I need to -[P] hee hee! -[P] disable the sensor![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] Mmmm, my goddess, have you forgotten to tell me to stop?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] R-[P]right![P] You pathetic unit![P] Your worship was tolerable at best![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Display your rear receiver port, I intend to punish it![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] Right away my goddess...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] Mmmm....") ]])
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Activate Fade", 85, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Blush") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Blush") ]])
    fnCutscene([[ Append("Christine:[E|Blush] [SOUND|World|WhipCrack]Mmmm...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] (Uh oh, it left a mark!)[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] (No, no, that's just the wax.[P] Obviously her chassis is going to be harder than a simple synth-leather whip!)[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] [SOUND|World|WhipCrack]This will teach you to disrespect your goddess![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] Yes my goddess, I will not fail you again.[P] I love you.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] I love you too.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Sophie![P] You keep breaking character![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] I'm sorry![P] This is my first time![P] I'm trying![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] That's okay, I think you did pretty good![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] Now come here.[P] Let me show you what else I can do...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Ooh![P] Ooh![P] Kiss your goddess!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    fnCutscene([[ AL_BeginTransitionTo("RegulusFlashbackB", "FORCEPOS:13.0x1.0x0") ]])
    fnCutsceneBlocker()

--Discipline. Proceeds.
elseif(sObjectName == "Discipline") then
	WD_SetProperty("Hide")
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("771852:[E|Serious] Negative, unit.[P] You are displaying insubordinate tendencies.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Wha?[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Serious] Command protocol requires disciplining of subordinate units.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Oh, oh?[P] We're doing it that way?[P] Okay![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] P-[P]punish away![P] This unit is ready![B][C]") ]])
    fnCutscene([[ Append("771852:[E|Serious] (What should I do to this unit?)[BLOCK]") ]])

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Whip her a few times...\", " .. sDecisionScript .. ", \"Whip\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Order her reprogrammed\",  " .. sDecisionScript .. ", \"Reprogram\") ")
    fnCutsceneBlocker()

--Whip. Returns to main area.
elseif(sObjectName == "Whip") then
	WD_SetProperty("Hide")
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Clearly you do not understand what sort of authority I have as your superior unit.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Bend over.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Affirmative, lord unit![P] Right away, lord unit![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Lord unit?[P] No, I believe 'mistress' will do.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Yes, my mistress!") ]])
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Activate Fade", 85, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Offended") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Blush") ]])
    fnCutscene([[ Append("Christine:[E|Offended] [SOUND|World|WhipCrack]Now, perhaps one stroke for speaking out of turn...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] (Eeee!)[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] [SOUND|World|WhipCrack]And another for not giving your mistress the proper respect?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] (Oh my![P] It feels so good!)[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] [SOUND|World|WhipCrack]One final to instill proper understanding into you.[P] Now, unit, who do you serve?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] My mistress![P] I only serve my mistress![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] I worship the ground she walks on![P] I live to be punished by her perfection![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] That's more like it.[P] Your mistress is pleased. For now.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] I love you, mistress![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] I love you, too, Sophie.[P] But you're not supposed to say that part![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Huh?[P] Oh, did I mess up?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Don't worry.[P] It was your first time.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Hee hee![P] Did you like it?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] I loved it.[P] Now...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Prepare your oral intake, worm![P] Your mistress needs her lower port massaged![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Yes, mistress![P] Executing oral service programs!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    fnCutscene([[ AL_BeginTransitionTo("RegulusFlashbackB", "FORCEPOS:13.0x1.0x0") ]])
    fnCutsceneBlocker()

--Reprogram. Proceeds along corruption path.
elseif(sObjectName == "Reprogram") then
	WD_SetProperty("Hide")
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("771852:[E|Serious] Considering your rebellious tendencies, reformation is unlikely.[P] Initiating reprogramming.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] C-[P]Christine![P] What are you doing?[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Serious] Initiating upload.[P] Do not resist.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Ah![P] Ah![P] No![P] Christine -[P] Christine don't   [P][CLEAR]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Reprogramming complete.[P] Unit 499323 reports cognitive activity at baseline.[P] Please provide function assignment.[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Serious] (What should I do with this unit?)[BLOCK]") ]])

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Order her to be my personal footrest\", " .. sDecisionScript .. ", \"Love\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Order her to betray the rebels\",  " .. sDecisionScript .. ", \"Betray\") ")
    fnCutsceneBlocker()

--Love. Returns to main area.
elseif(sObjectName == "Love") then
	WD_SetProperty("Hide")
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Serious] Unit 499323.[P] In a previous iteration, you aided a rebel uprising.[P] You were punished for this.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Affirmative, lord unit.[P] This unit deserved its punishment.[P] This unit now serves the Cause of Science.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Serious] Unit 499323, your function assignment is to act as a service unit for Unit 771852.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Serious] You will follow her everywhere, clean up after her, polish her chassis.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Serious] You will be her footrest if she so demands.[P] You are a machine, you will have no dignity whatsoever.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Serious] If at any time she orders sexual services, you will provide them without question.[P] Confirm.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Unit 499323's function assignment accepted.") ]])
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Activate Fade", 85, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Serious") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Blush") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] (Please order me to service you, Christine!)[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Serious] Unit 499323, you are currently out of standard.[P] Slave units are not to have visible hair folicles or wear non-protective clothing.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Serious] So, remove that dress.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Affirmative.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] No, no.[P] Slowly.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Like this?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Oh dear, slave units are so incompetent.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] I'll have to 'train' you...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Train me![P] Train me, lord unit![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Sophie![P] You're supposed to be a reprogrammed blank golem![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Oops![P] I'm sorry![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] That's okay, you're doing great for your first time.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Unit 499323 standing by.[P] Please begin training procedure.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] Your former self had a whip?[P] That will do.[P] Unit, expose rear port for training program.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Affirmative![P] Beginning training program![P] Hee hee!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    fnCutscene([[ AL_BeginTransitionTo("RegulusFlashbackB", "FORCEPOS:13.0x1.0x0") ]])
    fnCutsceneBlocker()

--Betray. Corrupted ending.
elseif(sObjectName == "Betray") then
	WD_SetProperty("Hide")
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Serious") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("771852:[E|Serious] Unit 499323.[P] In a previous iteration, you aided a rebel uprising.[P] You were punished for this.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Affirmative, lord unit.[P] This unit deserved its punishment.[P] This unit now serves the Cause of Science.[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Serious] Upload all information about known rebel activities.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Affirmative.[P] Beginning network upload.[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Serious] You must be brought to code.[P] Shave your head, remove your clothes.[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Serious] You are a machine, not an individual.[P] Confirm.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Unit 499323...[P] will...") ]])
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Activate Fade", 85, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Serious") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutscene([[ Append("Slave:[VOICE|Sophie] Unit 499323 reporting.[P] Please input function assignment.[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Serious] You will report to sector 42 for reassignment.[B][C]") ]])
    fnCutscene([[ Append("Slave:[VOICE|Sophie] Affirmative.[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Serious] Upload all remaining memory files of the rebel activity you supported.[P] Once upload is complete, delete those files.[B][C]") ]])
    fnCutscene([[ Append("Slave:[VOICE|Sophie] Affirmative.[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Serious] Work at maximum efficiency without complaint.[P] Machines do not require social interaction.[P] Obey the Cause of Science.[B][C]") ]])
    fnCutscene([[ Append("Slave:[VOICE|Sophie] Affirmative.[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Serious] Reprogramming satisfactory.[P] Memory file descrambled.[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Serious] All obstacles to reactivation of consciousness removed.[P] Unit 771852 activating consciousness...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    fnCutscene([[ AL_BeginTransitionTo("RegulusFinaleA", "FORCEPOS:29.0x3.0x0") ]])
    fnCutsceneBlocker()
end
