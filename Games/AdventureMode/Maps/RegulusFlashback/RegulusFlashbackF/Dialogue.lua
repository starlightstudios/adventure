-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    if(sActorName == "Kernel") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Kernel:[VOICE|Kernel] Simply walk into the memory files to un-corrupt them, 771852.[P] You'll need to do this several times before we can make any headway.") ]])
    
    elseif(sActorName == "Tiffany") then
        local iSpokeToTiffany = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeToTiffany", "N")
        if(iSpokeToTiffany == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Tiffany:[VOICE|Tiffany] Warning::[P] Unable to parse examination case::[P] Tiffany![B][C]") ]])
            fnCutscene([[ Append("771852:[VOICE|Christine] A parser error?[P] Maybe I should find a more complete version of this memory...") ]])
        else
            VM_SetVar("Root/Variables/Chapter5/Scenes/iChristineBumpCount", "N", 0.0)
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Tiffany:[VOICE|Tiffany] Hello -[P] hello greeting -[P] fragmentation.[P][SOUND|Combat|DoctorBag] INTERCOM memory dispenser.[P] Healing.[P] Thank you.") ]])
        end
    end
end
