-- |[Constructor]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusFlashbackF"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
    local iFlashbackBecameDoll = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackBecameDoll", "N")
    if(iFlashbackBecameDoll == 0.0) then
        AL_SetProperty("Music", "Null")
    else
        AL_SetProperty("Music", "ChristineDream")
    end
    
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("RegulusFlashbackF")
	
	-- |[Spawn NPCS Here]|
	--NPCs set to spawn here will be available for post-exec cutscenes.

	-- |[Lights]|
    AL_SetProperty("Activate Lights")

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

    -- |[Costumes]|
    VM_SetVar("Root/Variables/Costumes/Tiffany/sCostumeDoll", "S", "Sundress")

	-- |[Spawn NPCs Here]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    fnStandardNPCByPosition("Kernel")
    fnStandardNPCByPosition("Tiffany")
    
    --Run costume handler.
    LM_ExecuteScript(gsCharacterAutoresolve, "Tiffany")
	
	--Overlays.
    AL_SetProperty("Allocate Foregrounds", 1)
    AL_SetProperty("Foreground Image",          0, "Root/Images/Overlays/CRTNoise/CRT0")
    AL_SetProperty("Foreground Render Offsets", 0, 700.0, 400.0, 0.0, 0.0)
    AL_SetProperty("Foreground Alpha",          0, 0.00, 0)
    AL_SetProperty("Foreground Autoscroll",     0, 0.0, 0.0)
    AL_SetProperty("Foreground Scale",          0, 3.0)
    
    --NPC used for the static burst effect.
    TA_Create("StaticBurst")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        TA_SetProperty("Rendering Depth", -0.100)
        TA_SetProperty("Renders After Tiles", true)
		for i = 1, 8, 1 do
			for p = 1, 4, 1 do
				TA_SetProperty("Move Frame", i-1, p-1, "Root/Images/Sprites/StaticBurst/0")
				TA_SetProperty("Run Frame",  i-1, p-1, "Root/Images/Sprites/StaticBurst/0")
			end
		end
        TA_SetProperty("Wipe Special Frames")
        TA_SetProperty("Add Special Frame", "Burst0",  "Root/Images/Sprites/StaticBurst/0")
        TA_SetProperty("Add Special Frame", "Burst1",  "Root/Images/Sprites/StaticBurst/1")
        TA_SetProperty("Add Special Frame", "Burst2",  "Root/Images/Sprites/StaticBurst/2")
        TA_SetProperty("Add Special Frame", "Burst3",  "Root/Images/Sprites/StaticBurst/3")
        TA_SetProperty("Add Special Frame", "Burst4",  "Root/Images/Sprites/StaticBurst/4")
        TA_SetProperty("Add Special Frame", "Burst5",  "Root/Images/Sprites/StaticBurst/5")
        TA_SetProperty("Add Special Frame", "Burst6",  "Root/Images/Sprites/StaticBurst/6")
        TA_SetProperty("Add Special Frame", "Burst7",  "Root/Images/Sprites/StaticBurst/7")
        TA_SetProperty("Add Special Frame", "Burst8",  "Root/Images/Sprites/StaticBurst/8")
        TA_SetProperty("Add Special Frame", "Burst9",  "Root/Images/Sprites/StaticBurst/9")
        TA_SetProperty("Add Special Frame", "Burst10", "Root/Images/Sprites/StaticBurst/10")
        TA_SetProperty("Add Special Frame", "Burst11", "Root/Images/Sprites/StaticBurst/11")
        TA_SetProperty("Add Special Frame", "Burst12", "Root/Images/Sprites/StaticBurst/12")
    DL_PopActiveObject()
    
    --Create a parallel activity script.
    Cutscene_HandleParallel("Create", "StaticScript", gsRoot .. "Maps/RegulusFlashback/RegulusFlashbackF/StaticScript.lua")
    VM_SetVar("Root/Variables/Chapter5/Scenes/iStaticTimer", "N", 1000)
end
