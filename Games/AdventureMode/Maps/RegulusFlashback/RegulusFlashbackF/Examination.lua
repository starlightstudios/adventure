-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToFlashbackB") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusFlashbackB", "FORCEPOS:4.0x14.0x0")
    
elseif(sObjectName == "ToFlashbackG") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusFlashbackG", "FORCEPOS:9.0x20.0x0")
    
-- |[Objects]|
elseif(sObjectName == "Intercom") then
    local iSpokeToTiffany = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeToTiffany", "N")
    if(iSpokeToTiffany == 1.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iChristineBumpCount", "N", 0.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Tiffany:[VOICE|Tiffany] [SOUND|Combat|DoctorBag]Thank you for using our product.[P] We hope your brain feels better.") ]])
        fnCutsceneBlocker()
        
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (An intercom.[P] There's no response.)") ]])
        fnCutsceneBlocker()
    end

elseif(sObjectName == "Panic") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Today's special password is Pulse Picnic Panic![P] Remember that and input it at a save point for a fun surprise from your best friend!')") ]])
    fnCutsceneBlocker()

-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
