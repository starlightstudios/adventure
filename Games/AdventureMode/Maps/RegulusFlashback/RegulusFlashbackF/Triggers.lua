-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "KernelBriefing") then
    
    --Repeat check.
    local iFlashbackMemoryBrief = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackMemoryBrief", "N")
    if(iFlashbackMemoryBrief == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMemoryBrief", "N", 1.0)
    
    --Movement.
    fnCutsceneMove("Christine", 33.25, 6.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Camera.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 4.0)
        CameraEvent_SetProperty("Focus Position", (42.25 * gciSizePerTile), (18.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Camera.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 4.0)
        CameraEvent_SetProperty("Focus Position", (29.25 * gciSizePerTile), (26.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneMove("Kernel", 32.25, 6.50)
    
    --Camera.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 4.0)
        CameraEvent_SetProperty("Focus Position", (23.25 * gciSizePerTile), (15.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Refocus on Christine.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0)
        CameraEvent_SetProperty("Focus Actor Name", "Christine")
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Kernel", "Neutral") ]])
    fnCutscene([[ Append("771852:[E|Neutral] Quite a mess in here, isn't it?[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] The electrical damage was severe.[P] This is what happened to a lot of our memories.[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Neutral] Electrical damage?[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] ...[P] Well that's my assumption of what happened to us.[P] Electric currents can do a lot of brain damage.[P] And we were in human form at the time.[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] Though that could also be wrong.[P] We revert to human when injured, as part of the runestone's magic.[P] It's jumbled.[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Neutral] But the answer is in this mess, isn't it?[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] That is correct, 771852.[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Neutral] So what do you need me to do, then?[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] Put simply, walk into those memories you see wandering around.[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Neutral] The creatures with the viewcones?[P] Doesn't that trigger a battle?[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] Not here, because inside your own mind, you are in total control.[P] Instead, you will straighten out the rogue memory.[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] The sooner you do that, the sooner we can awaken and return to the administrator.[B][C]") ]])
    fnCutscene([[ Append("771852:[E|Neutral] I understand.[P] I will get my memories sorted so the administrator can view them.[P] Then I will have fulfilled my true purpose.[B][C]") ]])
    fnCutscene([[ Append("Kernel:[E|Neutral] I'll be over here if you need anything.[P] Good luck.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Kernel", 32.25, 4.50)
    fnCutsceneMove("Kernel", 31.25, 4.50)
    fnCutsceneFace("Kernel", 0, 1)
    fnCutsceneBlocker()


end
