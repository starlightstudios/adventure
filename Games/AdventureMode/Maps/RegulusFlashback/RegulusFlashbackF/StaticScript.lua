-- |[Static Script]|
--Script that handles the static overlay effect. Always runs, but the static effect only plays right after a corruption hit.
local iStaticTimer = VM_GetVar("Root/Variables/Chapter5/Scenes/iStaticTimer", "N")
local iTPF = 0.33
local iFrames = 23

if(iStaticTimer < 0) then
    fnCutsceneTeleport("StaticBurst", -100.25, -100.50)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iStaticTimer", "N", iStaticTimer + 1)

--Run the timer, update the sprite.
elseif(iStaticTimer < iTPF * iFrames) then
    
    --Increment.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iStaticTimer", "N", iStaticTimer + 1)
    
    --Compute current frame.
    local iFrame = math.floor(iStaticTimer / iTPF)
    if(iFrame > 12) then
        iFrame = 7 - (iFrame-12)
    end
    fnCutsceneSetFrame("StaticBurst", "Burst" .. iFrame)
    
    --Figure out where Christine is, and reposition the burst over her.
    EM_PushEntity("Christine")
        local fChristineX, fChristineY = TA_GetProperty("Position")
    DL_PopActiveObject()
    
    --Position the static burst there.
    fnCutsceneTeleport("StaticBurst", ((fChristineX+1) / gciSizePerTile) - 1.0, (fChristineY / gciSizePerTile) - 0.5)

--Over the edge.
else
    fnCutsceneTeleport("StaticBurst", -100.25, -100.50)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iStaticTimer", "N", 1000)
end
