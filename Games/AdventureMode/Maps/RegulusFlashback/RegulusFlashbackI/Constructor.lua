-- |[Constructor]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusFlashbackI"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music. Uses layering.
	AL_SetProperty("Music", "Null")
    
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("RegulusFlashbackI")
	
	-- |[Spawn NPCS Here]|
	--NPCs set to spawn here will be available for post-exec cutscenes.

	-- |[Lights]|
    AL_SetProperty("Activate Lights")

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

    -- |[Costumes]|
    VM_SetVar("Root/Variables/Costumes/Tiffany/sCostumeDoll", "S", "Normal")

	-- |[Spawn NPCs Here]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
	local iFlashbackMet55 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackMet55", "N")
	if(iFlashbackMet55 == 0.0) then
		fnSpecialCharacter("Tiffany", 10, 6, gci_Face_North, false, nil)
		EM_PushEntity("Tiffany")
			TA_SetProperty("Set Special Frame", "Chair0")
		DL_PopActiveObject()
		
		--Also turn the music off.
		AL_SetProperty("Music", "Null")
    
    else
		fnSpecialCharacter("Tiffany", 10, 6, gci_Face_North, false, nil)
		EM_PushEntity("Tiffany")
			TA_SetProperty("Set Special Frame", "Chair0")
		DL_PopActiveObject()
	end
    LM_ExecuteScript(gsCharacterAutoresolve, "Tiffany")
end
