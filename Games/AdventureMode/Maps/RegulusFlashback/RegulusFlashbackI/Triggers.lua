-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "Cutscene") then
	
	--Repeat check.
	local iFlashbackMet55 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFlashbackMet55", "N")
	if(iFlashbackMet55 == 1.0) then return end
	
    --Variables.
	local iSpokeToTiffany = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeToTiffany", "N")
    
	--Set.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iFlashbackMet55", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeToTiffany", "N", 1.0)
	
	-- |[Movement]|
	--Move Christine forward.
	fnCutsceneMove("Christine", 10.25, 10.50)
	fnCutsceneBlocker()
	
	--Camera focuses on 55.
	fnCutscene([[ AL_SetProperty("Music", "TiffanysTheme") ]])
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 1.0)
		CameraEvent_SetProperty("Focus Position", (10.25 * gciSizePerTile), (6.50 * gciSizePerTile))
	DL_PopActiveObject()
	
	--Wait a bit.
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	-- |[Dialogue]|
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ Append("771852:[E|Neutral] Excuse me...[P] are you the lady who was on the intercom?[B][C]") ]])
	fnCutscene([[ Append("Girl:[VOICE|Tiffany] Yes.[P] I recognize your voice.[P] It is deeper than the interference made it out to be.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	-- |[Movement]|
	--55 turns partway around.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Tiffany")
		ActorEvent_SetProperty("Special Frame", "Chair1")
	DL_PopActiveObject()
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Tiffany")
		ActorEvent_SetProperty("Special Frame", "Chair2")
	DL_PopActiveObject()
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	-- |[Dialogue]|
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ Append("771852:[E|Neutral] Y-[P]your legs...[B][C]") ]])
	fnCutscene([[ Append("771852:[E|Neutral] Wait, is this a memory right now, or is this actually happening?[B][C]") ]])
	fnCutscene([[ Append("Girl:[VOICE|Tiffany] I needed the parts you were collecting.[P] That should have been obvious.[B][C]") ]])
	fnCutscene([[ Append("771852:[E|Neutral] But I can speak, and I can move myself...[P] And I can't do that in memories, right?") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	-- |[Movement]|
	--55 turns the rest of the way.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Tiffany")
		ActorEvent_SetProperty("Special Frame", "Chair3")
	DL_PopActiveObject()
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Tiffany")
		ActorEvent_SetProperty("Special Frame", "Chair4")
	DL_PopActiveObject()
	fnCutsceneWait(305)
	fnCutsceneBlocker()
	
	-- |[Dialogue]|
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Broken") ]])
	fnCutscene([[ Append("2855: I am unit 2855.[P] I - [P][CLEAR]") ]])
	fnCutscene([[ Append("771852:[E|Neutral] I know exactly who you are.[P] This is a memory.[B][C]") ]])
	fnCutscene([[ Append("55: So you are the consciousness, then?[B][C]") ]])
	fnCutscene([[ Append("771852:[E|Neutral] W-[P]what? Hey![P] That's not what you say next![B][C]") ]])
	fnCutscene([[ Append("55: Correct.[P] Because I am not a memory.[B][C]") ]])
	fnCutscene([[ Append("771852:[E|Neutral] Everything else in here is, though.[B][C]") ]])
	fnCutscene([[ Append("55: Incorrect.[B][C]") ]])
	fnCutscene([[ Append("55: Christine, you are currently in a program.[P] This program is modifying you internally.[B][C]") ]])
	fnCutscene([[ Append("771852:[E|Neutral] ...[P] I know.[B][C]") ]])
	fnCutscene([[ Append("55: ...[P] You do?[B][C]") ]])
	fnCutscene([[ Append("771852:[E|Neutral] Yes.[P] That was the first thing Kernel told me, actually.[B][C]") ]])
	fnCutscene([[ Append("771852:[E|Neutral] And since this is my mind, I can just change things to suit me using my imagination.[B][C]") ]])
	fnCutscene([[ Append("771852:[E|Neutral] So while I appreciate the accuracy of your expression, I don't much like it.[P] So, please return to your repaired self.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Like this?[B][C]") ]])
	fnCutscene([[ Append("771852:[E|Neutral] That is preferable.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Broken] Then I must take this appearance.[B][C]") ]])
	fnCutscene([[ Append("771852:[E|Neutral] H-[P]hey![P] You are my own creation and I command you to change back![B][C]") ]])
	fnCutscene([[ Append("771852:[E|Neutral] ...[P] Dang.[P] Does that actually work, or was I mistaken?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Broken] Christine, I need you to listen to me.[P] You are being manipulated.[B][C]") ]])
	fnCutscene([[ Append("771852:[E|Neutral] But if I can't modify you, then you're not a memory.[B][C]") ]])
	fnCutscene([[ Append("771852:[E|Neutral] And if you're not a memory...[B][C]") ]])
	fnCutscene([[ Append("771852:[E|Offended] Then you must be...[B][C]") ]])
	fnCutscene([[ Append("771852:[E|Angry] The real 55...[B][C]") ]])
	fnCutscene([[ Append("55:[E|Broken] I have accessed this program, Christine, to liberate you from it.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Broken] So far I have evaded detection by disguising myself as the memory of our meeting.[B][C]") ]])
	fnCutscene([[ Append("771852:[E|Angry] No, no, no.[P] I don't care what you say, you filthy low-down bog-nosed liar![B][C]") ]])
	fnCutscene([[ Append("771852:[E|Angry] Because at the end of this memory, you 'betrayed' me.[P] You used me, and turned me into a golem.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Broken] If I recall correctly, you enjoyed the change.[B][C]") ]])
	fnCutscene([[ Append("771852:[E|Angry] But that wasn't the only betrayal, now was it, 55?[B][C]") ]])
	fnCutscene([[ Append("771852:[E|Angry] The Epsilon labs, we had -[P] Vivify -[P] and then you...[B][C]") ]])
	fnCutscene([[ Append("55:[E|Broken] Interesting.[P] You have been given most of your memory files back.[P] The upload must be nearly completed.[B][C]") ]])
	fnCutscene([[ Append("771852:[E|Angry] You...[B][C]") ]])
	fnCutscene([[ Append("771852:[E|Offended] You...[B][C]") ]])
	fnCutscene([[ Append("55:[E|Broken] What did I do, Christine?[B][C]") ]])
	fnCutscene([[ Append("771852:[E|Angry] I am not Christine![P] I am Command Unit 771852![B][C]") ]])
	fnCutscene([[ Append("771852:[E|Angry] I was created by the administrator![P] I was to be her perfectly loyal unit![P] I was sent to Earth to bring back information for the Cause of Science![B][C]") ]])
	fnCutscene([[ Append("771852:[E|Angry] The administrator created me![P] Created the runestone![P] Gave me purpose![P] I am her perfect doll![B][C]") ]])
	fnCutscene([[ Append("55:[E|Broken] Christine.[P] What did I do.[B][C]") ]])
	fnCutscene([[ Append("771852:[E|Angry] I love her with all my heart![P] If she ordered me to -[P] to...[B][C]") ]])
	fnCutscene([[ Append("55:[E|Broken] How did I betray you?[B][C]") ]])
	fnCutscene([[ Append("771852:[E|Offended] You...[P] pushed me onto an electrical shock trap...[P] because you...[B][C]") ]])
	fnCutscene([[ Append("771852:[E|Offended] You sided with the Regulus City administration...[B][C]") ]])
	fnCutscene([[ Append("55:[E|Broken] So you should obviously be listening to me, yes?[P] We are on the same side.[B][C]") ]])
	fnCutscene([[ Append("771852:[E|Offended] But...[P] then why are you hiding in here?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Broken] Oh.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Broken] This is most amusing.[P] Yes, very amusing.[B][C]") ]])
	fnCutscene([[ Append("771852:[E|Neutral] I don't understand.[P] 55?[P] What's going on?[P] Why are you here?[P] This - [B][C]") ]])
	fnCutscene([[ Append("771852:[E|Sad] I cannot solve this contradiction logically.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Broken] The solution is very obvious, but you are missing a key piece of information.[P] And because you are missing that, you will believe anything.[B][C]") ]])
	fnCutscene([[ Append("771852:[E|Sad] But...[B][C]") ]])
	fnCutscene([[ Append("55:[E|Broken] I know this is hard, Christine, but you must listen to me.[B][C]") ]])
	fnCutscene([[ Append("771852:[E|Neutral] I shouldn't trust you, you -[P] but I should.[P] 55?[P] Aren't you my friend?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Broken] Someone smarter than me once told me that the only way to gain someone's trust, is to trust them.[P] And the fastest way to lose someone's trust, is to wear that mistrust like a badge.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Broken] I'm asking you, after all this, to trust me one last time.[B][C]") ]])
	fnCutscene([[ Append("771852:[E|Sad] But people who abuse your trust...[P] You've done that.[P] You've done that and I was angry at you for it...[B][C]") ]])
	fnCutscene([[ Append("55:[E|Broken] Then make your decision once you have seen the evidence.[P] That is all I'm asking of you.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Broken] In your memories of Earth, there are repressed memories.[P] Go see them.[B][C]") ]])
	fnCutscene([[ Append("771852:[E|Sad] And if it's a trick?[P] And if I cannot view my memories?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Broken] You can.[P] They are repressed by the program, not by you.[P] Go see them.[B][C]") ]])
	fnCutscene([[ Append("771852:[E|Sad] 55...[B][C]") ]])
	fnCutscene([[ Append("771852:[E|Sad] Where did it all go wrong?[P] I was so happy there in the repair bay with Sophie.[P] Why did I bring this upon myself?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Broken] Because you have an abundance of something I lack.[P] Courage.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Broken] And if evil requires that good machines stand by and do nothing, then you did not stand by.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Broken] Go to your repressed memories of Earth, Christine.[P] Please.[B][C]") ]])
    if(iSpokeToTiffany == 0.0) then
        fnCutscene([[ Append("55:[E|Broken] The intercoms will allow you to repair any mental damage you sustain.[P] My alter-ego in your memories will, as well.[B][C]") ]])
    end
	fnCutscene([[ Append("771852:[E|Sad] A-[P]Affirmative...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
    
    --55 rotates back.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Tiffany")
		ActorEvent_SetProperty("Special Frame", "Chair3")
	DL_PopActiveObject()
	fnCutsceneWait(45)
	fnCutsceneBlocker()
    
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Tiffany")
		ActorEvent_SetProperty("Special Frame", "Chair2")
	DL_PopActiveObject()
	fnCutsceneWait(45)
	fnCutsceneBlocker()
    
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Tiffany")
		ActorEvent_SetProperty("Special Frame", "Chair1")
	DL_PopActiveObject()
	fnCutsceneWait(45)
	fnCutsceneBlocker()
    
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Tiffany")
		ActorEvent_SetProperty("Special Frame", "Chair0")
	DL_PopActiveObject()
end
