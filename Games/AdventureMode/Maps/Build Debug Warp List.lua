-- |[ ================================= Build Debug Warp List ================================== ]|
--Builds a list of locations the player can warp to. This is only used by the debug menu, as not all levels
-- are considered valid destinations for normal play. Some are used only for cutscenes, so only scripts can
-- warp there unless there is another reason to visit them.

-- |[Arguments]|
--Argument Listing:
-- 0: sChapterName - Name of the chapter to build a list for.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sChapterName = LM_GetScriptArgument(0)

-- |[Common]|
local zaListList = {}

-- |[ ========================= Chapter 1 ======================== ]|
if(sChapterName == "Chapter 1") then

	--Baseline lists.
	local saTrannadarList     = {"AlrauneChamber", "TrannadarTradingPost", "BreannesPitStop", "EvermoonW", "SaltFlats", "SaltFlatsCave", "EvermoonE", "EvermoonNE", "EvermoonNW", "EvermoonS","EvermoonCassandraA",
                                "EvermoonCassandraAMid", "EvermoonCassandraCC"}
	local saBeehiveList       = {"BeehiveInner", "BeehiveOuter", "BeehiveBasementA", "BeehiveBasementB", "BeehiveBasementC", "BeehiveBasementD", "BeehiveBasementE", "BeehiveBasementF", "BeehiveBasementScene"}
	local saNixNedarList      = {"NixNedarMain", "NixNedarSouthPath", "NixNedarSouthPathWander", "NixNedarHouseEast", "NidNedarHousePath", "NixNedarHouseSouthcenter", "NixNedarHouseSouthwest", "NixNedarMaramHouse",
                                 "NixNedarSeptimaHouse"}
	local saTrapBasement      = {"TrapBasementA", "TrapBasementB", "TrapBasementC", "TrapBasementD", "TrapBasementE", "TrapBasementF", "TrapBasementG", "TrapBasementH"}
	local saTrapDungeon       = {"TrapDungeonA", "TrapDungeonB", "TrapDungeonC", "TrapDungeonD", "TrapDungeonE", "TrapDungeonF", "TrapDungeonEntry"}
	local saTrapMain          = {"TrapMainFloorCentral", "TrapMainFloorEast", "TrapMainFloorExterior", "TrapMainFloorExteriorN", "TrapMainFloorSouthHall", "TrapUpperFloorMain", "TrapUpperFloorE", "TrapUpperFloorW"}
	local saQuantir           = {"SpookyExterior", "QuantirNW", "QuantirNWCave"}
	local saQuantirManse      = {"QuantirManseEntrance", "QuantirManseBasementW", "QuantirManseBasementE", "QuantirManseCentralW", "QuantirManseCentralE", "QuantirManseNEHall", "QuantirManseNWHall", "QuantirManseSEHall",
                                 "QuantirManseSWYard", "QuantirManseTruth"}
    local saArbonneList       = {"ArbonnePlainsA", "ArbonnePlainsB", "ArbonnePlainsC", "ArbonnePlainsD"}
    local saStarfieldList     = {"StarfieldSwampA", "StarfieldSwampB", "StarfieldSwampC", "StarfieldSwampD", "StarfieldSwampE", "StarfieldSwampF", "StarfieldSwampG", "StarfieldSwampH", "StarfieldSwampI", "StarfieldSwampJ"}
    local saStForasList       = {"StForasA", "StForasB", "StForasC", "StForasD", "StForasE", "StForasF", "StForasG", "StForasH", "StForasI"}
    local saRiverWildsList    = {"RiverWildsA", "RiverWildsB", "RiverWildsC", "RiverWildsD", "WildsTowerA", "WildsTowerB", "WildsTowerC", "WildsTowerD", "WildsTowerE", "DrinkingContest"}
    local saStarfieldCaveList = {"StarfieldCavesA", "StarfieldMausoleumA", "TrannadarDescentA", "TrannadarDescentC", "TrannadarReserveA"}
    local saTutorialList      = {"Island"}
    local saDebugList         = {"Enemy Testing Range"}

	--Add the lists to the master list.
	zaListList[1]  = {saTrannadarList,     "Trannadar/"}
	zaListList[2]  = {saTrapBasement,      "Trap Basement/"}
	zaListList[3]  = {saTrapMain,          "Trap Main + Upper/"}
	zaListList[4]  = {saQuantir,           "Quantir Exterior/"}
	zaListList[5]  = {saBeehiveList,       "Beehive/"}
	zaListList[6]  = {saTrapDungeon,       "Trap Dungeon/"}
	zaListList[7]  = {saQuantirManse,      "Quantir Mansion/"}
	zaListList[8]  = {saNixNedarList,      "Nix Nedar/"}
	zaListList[9]  = {saArbonneList,       "Arbonne Plains/"}
	zaListList[10] = {saStarfieldList,     "Starfield Swamp/"}
	zaListList[11] = {saStForasList,       "St. Fora's/"}
	zaListList[12] = {saRiverWildsList,    "River Wilds/"}
	zaListList[13] = {saStarfieldCaveList, "Starfield Caves/"}
	zaListList[14] = {saTutorialList,      "Tutorial/"}
	zaListList[15] = {saDebugList,         "Debug/"}

-- |[ ========================= Chapter 2 ======================== ]|
elseif(sChapterName == "Chapter 2") then

    --Baseline.
	local saShrineHeroList = {"LowerShrineB", "HeroShrineB", "HeroShrineC", "HeroShrineE", "LowerShrineH"}
    local saNorthwoodsList = {"NorthwoodsNEA", "NorthwoodsNWD", "NorthwoodsSED", "NorthwoodsSWC", "NorthwoodsSCD"}
    local saWestwoodsList  = {"WestwoodsNA", "WestwoodsCA", "WestwoodsNB", "WestwoodsNF", "WestwoodsCD", "WestwoodsCC"}
    local saMinesList      = {"SanyaCabinA", "SanyaMinesA", "SanyaMinesG"}
    local saVucaList       = {"VucaPassB"}
    local saGranvireList   = {"GranvirePassA", "GranvirePassD"}
    local saSarulenteList  = {"SarulenteG"}
    local saHarpyBaseList  = {"HarpyBaseA"}
    local saWarrensList    = {"WarrensCaveA", "WarrensC", "WarrensCaveAAlt"}
    local saDebugList      = {"Enemy Testing Range Ch2"}

	--Add the lists to the master list.
    table.insert(zaListList, {saShrineHeroList, "Hero Shrine/"})
    table.insert(zaListList, {saNorthwoodsList, "Northwoods/"})
    table.insert(zaListList, {saWestwoodsList,  "Westwoods/"})
    table.insert(zaListList, {saMinesList,      "Northwoods Mines/"})
    table.insert(zaListList, {saGranvireList,   "Granvire Pass/"})
    table.insert(zaListList, {saVucaList,       "Vuca Pass/"})
    table.insert(zaListList, {saSarulenteList,  "Mt. Sarulente/"})
    table.insert(zaListList, {saHarpyBaseList,  "Harpy Base/"})
    table.insert(zaListList, {saWarrensList,    "Warrens/"})
    table.insert(zaListList, {saDebugList,      "Debug/"})

-- |[ ========================= Chapter 5 ======================== ]|
elseif(sChapterName == "Chapter 5") then

	--Baseline lists.
    local saBiolabList   = {"RegulusBiolabsAmphibianA", "RegulusBiolabsA", "RegulusBiolabsDatacoreA", "RegulusBiolabsDatacoreB", "RegulusBiolabsBetaA", "RegulusBiolabsBetaMeltedA", "RegulusBiolabsGammaA", "RegulusBiolabsGammaB", "RegulusBiolabsGammaD", "RegulusBiolabsGammaWestA", "RegulusBiolabsGammaWestC", "RegulusBiolabsDeltaA", "RegulusBiolabsDeltaB", "RegulusBiolabsDeltaI", "RegulusBiolabsGeneticsH", "RegulusBiolabsMovieD", "RegulusBiolabsEpsilonE"}
	local saCryoList     = {"RegulusCryoA", "RegulusCryoB", "RegulusCryoC", "RegulusCryoG", "RegulusCryoCommandA", "RegulusCryoCommandB", "RegulusCryoCommandC", "RegulusCryoContainmentA", "RegulusCryoContainmentB", "RegulusCryoLowerA", "RegulusCryoLowerB", "RegulusCryoLowerC", "RegulusCryoLowerD", "RegulusCryoToFabricationA", "RegulusCryoToFabricationB", "RegulusCryoPowerCoreC"}
	local saCityList     = {"RegulusCityA", "RegulusCityB", "RegulusCityC", "RegulusCityD", "RegulusCityE", "RegulusCityF", "RegulusCityG", "RegulusCityZ", "RegulusCity119A", "RegulusCity119B", "RegulusCity198D", "Sector254A"}
	local saExteriorList = {"RegulusExteriorEA", "RegulusExteriorEB", "RegulusExteriorEC", "RegulusExteriorED", "RegulusExteriorSA", "RegulusExteriorSB", "RegulusExteriorSC", "RegulusExteriorWA", "RegulusExteriorWB", "RegulusExteriorWC"}
	local saLRTList      = {"RegulusLRTA", "RegulusLRTB", "RegulusLRTC", "RegulusLRTD", "RegulusLRTE", "RegulusLRTEA", "RegulusLRTF", "RegulusLRTG", "RegulusLRTHA", "RegulusLRTHF", "RegulusLRTIA", "RegulusLRTIG"}
	local saEquinoxList  = {"RegulusEquinoxA", "RegulusEquinoxB", "RegulusEquinoxC", "RegulusEquinoxD", "RegulusEquinoxE", "RegulusEquinoxF", "RegulusEquinoxG", "RegulusEquinoxH"}
	local saSerenityList = {"RegulusExteriorEE", "RegulusExteriorEF", "SerenityObservatoryA", "SerenityObservatoryE", "SerenityCraterA", "SerenityCraterF"}
	local saMinesList    = {"TelluriumMinesA", "TelluriumMinesB", "TelluriumMinesC", "TelluriumMinesD", "TelluriumMinesE", "TelluriumMinesF", "TelluriumMinesG", "SprocketCityA", "TelluriumMinesH", "MinesRandomA", "MinesRandomB", "BlackSiteA"}
	local saManufList    = {"RegulusManufactoryA", "RegulusManufactoryB", "RegulusManufactoryC", "RegulusManufactoryD", "RegulusManufactoryE", "RegulusManufactoryF"}
	local saArcaneList   = {"RegulusArcaneA", "RegulusArcaneB", "RegulusArcaneC", "RegulusArcaneD", "RegulusArcaneE", "RegulusArcaneF", "RegulusArcaneG", "RegulusArcaneH"}
	local saFlashbackList= {"RegulusFlashbackA"}
	local saFinaleList   = {"RegulusFinaleA"}
    local saDebugList    = {"Enemy Testing Range Ch5", "RegulusCityX"}

	--Add the lists to the master list.
	zaListList[ 1] = {saBiolabList,   "Regulus Biolabs/"}
	zaListList[ 2] = {saCryoList,     "Regulus Cryogenics/"}
	zaListList[ 3] = {saCityList,     "Regulus City/"}
	zaListList[ 4] = {saExteriorList, "Regulus Exterior/"}
	zaListList[ 5] = {saLRTList,      "Regulus LRT/"}
	zaListList[ 6] = {saEquinoxList,  "Regulus Equinox/"}
	zaListList[ 7] = {saSerenityList, "Regulus Serenity/"}
	zaListList[ 8] = {saMinesList,    "Regulus Mines/"}
	zaListList[ 9] = {saManufList,    "Regulus Manufactory/"}
	zaListList[10] = {saArcaneList,   "Regulus Arcane/"}
	zaListList[11] = {saFlashbackList,"Flashback/"}
	zaListList[12] = {saFinaleList,   "Finale/"}
	zaListList[13] = {saDebugList,    "Debug/"}

end

-- |[ ======================= List Upload ======================== ]|
-- |[Counting]|
--Iterate across the list of lists, counting all the entries:
local i = 1
local iGlobalCount = 0
while(zaListList[i] ~= nil) do
	
	--All members of this sublist have the second element appended before their name.
	local p = 1
	local saSublist = zaListList[i][1]
	while(saSublist[p] ~= nil) do
		iGlobalCount = iGlobalCount + 1
		p = p + 1
	end
	
	--Next.
	i = i + 1
	
end

-- |[Finalize List]|
--Set this as the total count.
ADebug_SetProperty("Warp Destinations Total", iGlobalCount)

--Re-iterate, except this time actually add them.
i = 1
iGlobalCount = 0
while(zaListList[i] ~= nil) do
	
	--All members of this sublist have the second element appended before their name.
	local p = 1
	local saSublist = zaListList[i][1]
	local sPrefix = zaListList[i][2]
	while(saSublist[p] ~= nil) do
		ADebug_SetProperty("Warp Destination", iGlobalCount, sPrefix .. saSublist[p])
		iGlobalCount = iGlobalCount + 1
		p = p + 1
	end
	
	--Next.
	i = i + 1
end