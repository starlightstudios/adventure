-- |[ ==================================== Chapter 2 Lookups =================================== ]|
--Sets the master list of map remaps for Chapter 2.
gsaMapLookups = {}

--Don't boot maps when loading the game if this flag is true. The first time the map lookups are called,
-- the game hasn't actually loaded yet and we don't want to waste time building map lookups.
if(gbDisallowMapConstruction == true) then return end

--Do nothing if chapter 2 is not active.
local iCurrentChapter = VM_GetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N")
if(iCurrentChapter ~= 2.0) then return end

--Clear any existing map lookups.
GUIMap:fnClearGlobalList()

-- |[ ======================================== Functions ======================================= ]|
-- |[Setup]|
--Variables used.
local iIndex = 1

-- |[fnAddLookup]|
--Adds a single lookup.
local function fnAddLookup(psName, psMap, piX, piY)
	gsaMapLookups[iIndex] = {psName, psMap, piX, piY}
    iIndex = iIndex + 1
end

-- |[fnAddLookups]|
--Adds a lookup between two letters, ex: "BeehiveBasement" "A", "C", will do A, B, and C, in order.
local function fnAddLookups(psNamePattern, psStartLetter, psEndLetter, psMap, piX, piY)
    
	--Setup.
	local sCurrentLetter = psStartLetter
	
	--Range check. If the letter is out of the normal range, fail.
	local iByteValue = string.byte(sCurrentLetter)
	if(iByteValue < string.byte("A") or iByteValue > string.byte("Z")) then return end
	
	--Iterate across the letters. Always stops on Z. Also always stops on non-capitals.
	while(true) do
		
		--Add it.
		local sLevelName = psNamePattern .. sCurrentLetter
        gsaMapLookups[iIndex] = {sLevelName, psMap, piX, piY}
        iIndex = iIndex + 1
		
		--If this was the ending letter, or the letter 'Z', stop.
		if(sCurrentLetter == psEndLetter or sCurrentLetter == "Z") then break end
		
		--Move to the next letter.
		local iByteValue = string.byte(sCurrentLetter)
		sCurrentLetter = string.char(iByteValue + 1)
	end
    
end

-- |[ ==================================== Set Coordinates ===================================== ]|
-- |[Northwoods]|
--These are only used for warp lookups. Otherwise, a different function handles rendering.
fnAddLookup("LowerShrineC",    "Northwoods",  2461,  193)
fnAddLookup("SanyaCabinA",     "Northwoods",  1458,  441)
fnAddLookup("NorthwoodsNEA",   "Northwoods",  1928,  244)
fnAddLookup("NorthwoodsNWE",   "Northwoods",   757,  175)
fnAddLookup("NorthwoodsSWC",   "Northwoods",  1063, 1375)
fnAddLookup("KitsuneVillageB", "Westwoods",   2006,  155)
fnAddLookup("VucaPassD",       "Westwoods",   1815,  529)
fnAddLookup("GranvirePassD",   "Westwoods",   1059,  573)
fnAddLookup("WestwoodsSEB",    "Westwoods",   1829, 1500)
fnAddLookup("HarpyBaseA",      "Westwoods",   2015, 1036)
fnAddLookup("SarulenteF",      "MtSarulente", 1099,  712)
