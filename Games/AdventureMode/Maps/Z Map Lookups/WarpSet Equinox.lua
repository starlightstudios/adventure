-- |[Warp Setter]|
--Script that gets called when the warp menu is activated for an area. Populates advanced map info for that area.
AM_SetProperty("Create Advanced Map Layer", "Layer0", "Root/Images/AdvMaps/Equinox/Equinox0")

AM_SetProperty("Create Advanced Map Layer", "Layer1", "Root/Images/AdvMaps/Equinox/Equinox1")
AM_SetProperty("Layer Render Chance",       "Layer1", 0, 10)

AM_SetProperty("Create Advanced Map Layer", "Layer2", "Root/Images/AdvMaps/Equinox/Equinox2")
AM_SetProperty("Layer Render Chance",       "Layer2", 11, 20)

AM_SetProperty("Create Advanced Map Layer", "Layer3", "Root/Images/AdvMaps/Equinox/Equinox3")
AM_SetProperty("Layer Render Chance",       "Layer3", 21, 30)

AM_SetProperty("Create Advanced Map Layer", "Layer4", "Root/Images/AdvMaps/Equinox/Equinox5")
AM_SetProperty("Layer Render Chance",       "Layer4", 31, 40)

AM_SetProperty("Create Advanced Map Layer", "Layer5", "Root/Images/AdvMaps/Equinox/Equinox4")
AM_SetProperty("Layer Render Chance",       "Layer5", 51, 1000)

AM_SetProperty("Create Advanced Map Layer", "LayerP", "Null")
AM_SetProperty("Layer Renders Player",      "LayerP", "Null", false, 0, 0)

AM_SetProperty("Create Advanced Map Layer", "LayerO", "Root/Images/AdvMaps/General/PDUFrame")
AM_SetProperty("Layer Is Fixed",            "LayerO", true)
