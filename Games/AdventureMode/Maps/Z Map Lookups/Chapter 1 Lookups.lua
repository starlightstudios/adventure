-- |[ ==================================== Chapter 1 Lookups =================================== ]|
--Sets the master list of map remaps for Chapter 1.
gsaMapLookups = {} --Makes the warp function not break if chapter 2/5 aren't loaded yet. Chapter 5 may get updated to the new format, until then this stays.

-- |[Chapter Lockout]|
--Don't boot maps when loading the game if this flag is true. The first time the map lookups are called,
-- the game hasn't actually loaded yet and we don't want to waste time building map lookups.
if(gbDisallowMapConstruction == true) then return end

--Do nothing if chapter 1 is not active.
local iCurrentChapter = VM_GetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N")
if(iCurrentChapter ~= 1.0) then return end

--Clear any existing map lookups.
GUIMap:fnClearGlobalList()

--Variables/constants.
local sVarPrefix = "Root/Variables/Chests/"
local sDeniseShowHealth     = "Root/Variables/Chapter1/Denise/iDeniseShow_Health"
local sDeniseShowPower      = "Root/Variables/Chapter1/Denise/iDeniseShow_Power"
local sDeniseShowInitiative = "Root/Variables/Chapter1/Denise/iDeniseShow_Initiative"
local sDeniseShowAccuracy   = "Root/Variables/Chapter1/Denise/iDeniseShow_Accuracy"
local sDeniseShowEvade      = "Root/Variables/Chapter1/Denise/iDeniseShow_Evasion"
local sDeniseShowSkills     = "Root/Variables/Chapter1/Denise/iDeniseShow_Skills"

-- |[ ================================= Trannadar Map Lookups ================================== ]|
--Setup.
local zTrannadarMap = GUIMap:fnRegisterNewMap("Trannadar Province")
--io.write("Registering chapter 1 lookups.\n")

--Layer Information
zTrannadarMap.iPieces          = gciTrannadarMapsTotal
zTrannadarMap.sBaseDLPath      = "Root/Images/AdvMaps/Trannadar/Base"
zTrannadarMap.sLayerPattern    = "Null"
zTrannadarMap.sVariablePattern = "Root/Variables/Chapter1/TrannadarMap/iRoom%02i"
zTrannadarMap.sPiecePattern    = "Root/Images/AdvMaps/Trannadar/Slice_%02i"
zTrannadarMap.sOverlayDLPath   = "Root/Images/AdvMaps/Trannadar/Overlay"

--Warp Information
zTrannadarMap.sCampfireWarpIconPath  = "Root/Images/AdventureUI/AdvCampfireWarpIcon/Trannadar"

-- |[ ========== Lookups =========== ]|
--Evermoon Forest
zTrannadarMap:fnRegisterLookup("AlrauneAndBeeScene",    "No Set",                                        609,  883)
zTrannadarMap:fnRegisterLookup("AlrauneChamber",        "No Set",                                        719, 1006)
zTrannadarMap:fnRegisterLookup("BreannesPitStop",       "No Set",                                        782,  637)
zTrannadarMap:fnRegisterLookup("EvermoonCassandraA",    "No Set",                                        850,  401)
zTrannadarMap:fnRegisterLookup("EvermoonCassandraAMid", "No Set",                                        918,  297)
zTrannadarMap:fnRegisterLookup("EvermoonCassandraB",    "No Set",                                        916,  175)
zTrannadarMap:fnRegisterLookup("EvermoonCassandraCC",   "No Set",                                        945,   49)
zTrannadarMap:fnRegisterLookup("EvermoonCassandraCE",   "No Set",                                       1094,  146)
zTrannadarMap:fnRegisterLookup("EvermoonCassandraCNE",  "No Set",                                       1017,   66)
zTrannadarMap:fnRegisterLookup("EvermoonCassandraCNW",  "No Set",                                        874,   57)
zTrannadarMap:fnRegisterLookup("EvermoonE",             "Root/Variables/Chapter1/TrannadarMap/iRoom07", 1084, 1027)
zTrannadarMap:fnRegisterLookup("EvermoonNE",            "Root/Variables/Chapter1/TrannadarMap/iRoom01",  848,  526)
zTrannadarMap:fnRegisterLookup("EvermoonNW",            "Root/Variables/Chapter1/TrannadarMap/iRoom03",  444,  819)
zTrannadarMap:fnRegisterLookup("EvermoonS",             "Root/Variables/Chapter1/TrannadarMap/iRoom08",  885, 1179)
zTrannadarMap:fnRegisterLookup("EvermoonSEA",           "Root/Variables/Chapter1/TrannadarMap/iRoom10", 1048, 1292)
zTrannadarMap:fnRegisterLookup("EvermoonSEB",           "No Set",                                       1024, 1389)
zTrannadarMap:fnRegisterLookup("EvermoonSEC",           "Root/Variables/Chapter1/TrannadarMap/iRoom11",  992, 1393)
zTrannadarMap:fnRegisterLookup("EvermoonSlimeScene",    "No Set",                                        814, 1099)
zTrannadarMap:fnRegisterLookup("EvermoonSlimeVillage",  "Root/Variables/Chapter1/TrannadarMap/iRoom09", 1286, 1229)
zTrannadarMap:fnRegisterLookup("EvermoonSW",            "Root/Variables/Chapter1/TrannadarMap/iRoom06",  618, 1107)
zTrannadarMap:fnRegisterLookup("EvermoonW",             "Root/Variables/Chapter1/TrannadarMap/iRoom05",  626,  983)
zTrannadarMap:fnRegisterLookup("PlainsC",               "No Set",                                        686,  648)
zTrannadarMap:fnRegisterLookup("PlainsNW",              "Root/Variables/Chapter1/TrannadarMap/iRoom02",  407,  700)
zTrannadarMap:fnRegisterLookup("SaltFlats",             "Root/Variables/Chapter1/TrannadarMap/iRoom12",  776, 1458)
zTrannadarMap:fnRegisterLookup("SaltFlatsCave",         "No Set",                                        776, 1458)
zTrannadarMap:fnRegisterLookup("TrannadarTradingPost",  "No Set",                                        514,  980)
zTrannadarMap:fnRegisterLookup("WerecatScene",          "No Set",                                        434, 1061)

--Beehive
zTrannadarMap:fnRegisterLookup("BeehiveBasementA",     "Root/Variables/Chapter1/TrannadarMap/iRoom13",  152,  578)
zTrannadarMap:fnRegisterLookup("BeehiveBasementB",     "Root/Variables/Chapter1/TrannadarMap/iRoom14",  138,  406)
zTrannadarMap:fnRegisterLookup("BeehiveBasementC",     "Root/Variables/Chapter1/TrannadarMap/iRoom15",  261,  240)
zTrannadarMap:fnRegisterLookup("BeehiveBasementD",     "No Set",                                        274,   92)
zTrannadarMap:fnRegisterLookup("BeehiveBasementE",     "Root/Variables/Chapter1/TrannadarMap/iRoom18",  205,  249)
zTrannadarMap:fnRegisterLookup("BeehiveBasementF",     "Root/Variables/Chapter1/TrannadarMap/iRoom17",  135,  107)
zTrannadarMap:fnRegisterLookup("BeehiveBasementScene", "Root/Variables/Chapter1/TrannadarMap/iRoom16",  339,  326)
zTrannadarMap:fnRegisterLookup("BeehiveInner",         "Root/Variables/Chapter1/TrannadarMap/iRoom19",  492,  270)
zTrannadarMap:fnRegisterLookup("BeehiveOuter",         "Root/Variables/Chapter1/TrannadarMap/iRoom00",  426,  532)

--Dimensional Trap
zTrannadarMap:fnRegisterLookup("TrapBasementA",      "Root/Variables/Chapter1/TrannadarMap/iRoom25", 2416, 1315)
zTrannadarMap:fnRegisterLookup("TrapBasementB",      "No Set",                                       2308, 1313)
zTrannadarMap:fnRegisterLookup("TrapBasementC",      "No Set",                                       2110, 1301)
zTrannadarMap:fnRegisterLookup("TrapBasementD",      "No Set",                                       2236, 1394)
zTrannadarMap:fnRegisterLookup("TrapBasementE",      "No Set",                                       2559, 1108)
zTrannadarMap:fnRegisterLookup("TrapBasementF",      "Root/Variables/Chapter1/TrannadarMap/iRoom27", 2629, 1058)
zTrannadarMap:fnRegisterLookup("TrapBasementG",      "Root/Variables/Chapter1/TrannadarMap/iRoom26", 2359, 1153)
zTrannadarMap:fnRegisterLookup("TrapBasementH",      "No Set",                                       2487,  997)
zTrannadarMap:fnRegisterLookup("TrapBasementSecret", "No Set",                                       2262, 1328)

zTrannadarMap:fnRegisterLookup("TrapMainFloorCentral",   "Root/Variables/Chapter1/TrannadarMap/iRoom20", 1585,  916)
zTrannadarMap:fnRegisterLookup("TrapMainFloorEast",      "Root/Variables/Chapter1/TrannadarMap/iRoom22", 1735,  800)
zTrannadarMap:fnRegisterLookup("TrapMainFloorExterior",  "No Set",                                        894, 1041)
zTrannadarMap:fnRegisterLookup("TrapMainFloorExteriorN", "Root/Variables/Chapter1/TrannadarMap/iRoom04",  954,  795)
zTrannadarMap:fnRegisterLookup("TrapMainFloorSouthHall", "Root/Variables/Chapter1/TrannadarMap/iRoom21", 1752,  933)
zTrannadarMap:fnRegisterLookup("TrapUpperFloorE",        "No Set",                                       2149,  792)
zTrannadarMap:fnRegisterLookup("TrapUpperFloorMain",     "Root/Variables/Chapter1/TrannadarMap/iRoom23", 2024,  833)
zTrannadarMap:fnRegisterLookup("TrapUpperFloorW",        "No Set",                                       1907,  791)

--Dimensional Trap Dungeon
zTrannadarMap:fnRegisterLookup("TrapDungeonA",     "Root/Variables/Chapter1/TrannadarMap/iRoom24",  857, 1008)
zTrannadarMap:fnRegisterLookup("TrapDungeonB",     "No Set",                                       1772, 1339)
zTrannadarMap:fnRegisterLookup("TrapDungeonC",     "No Set",                                       1772, 1233)
zTrannadarMap:fnRegisterLookup("TrapDungeonD",     "No Set",                                       1849, 1086)
zTrannadarMap:fnRegisterLookup("TrapDungeonE",     "No Set",                                       1697, 1102)
zTrannadarMap:fnRegisterLookup("TrapDungeonF",     "No Set",                                       1612, 1112)
zTrannadarMap:fnRegisterLookup("TrapDungeonEntry", "No Set",                                        857, 1008)

--Quantir / Mansion
zTrannadarMap:fnRegisterLookup("QuantirNW",                  "No Set",                                       1397,  598)
zTrannadarMap:fnRegisterLookup("QuantirNWCave",              "No Set",                                       1455,  514)
zTrannadarMap:fnRegisterLookup("QuantirManseBasementE",      "Root/Variables/Chapter1/TrannadarMap/iRoom37", 2394,  430)
zTrannadarMap:fnRegisterLookup("QuantirManseBasementW",      "Root/Variables/Chapter1/TrannadarMap/iRoom36", 2054,  288)
zTrannadarMap:fnRegisterLookup("QuantirManseCentralE",       "Root/Variables/Chapter1/TrannadarMap/iRoom31", 1815,  329)
zTrannadarMap:fnRegisterLookup("QuantirManseCentralW",       "Root/Variables/Chapter1/TrannadarMap/iRoom29", 1558,  316)
zTrannadarMap:fnRegisterLookup("QuantirManseEntrance",       "Root/Variables/Chapter1/TrannadarMap/iRoom30", 1684,  400)
zTrannadarMap:fnRegisterLookup("QuantirManseNEHall",         "Root/Variables/Chapter1/TrannadarMap/iRoom35", 1745,  148)
zTrannadarMap:fnRegisterLookup("QuantirManseNEHallFloor2",   "Root/Variables/Chapter1/TrannadarMap/iRoom34", 1469,  109)
zTrannadarMap:fnRegisterLookup("QuantirManseNWHall",         "Root/Variables/Chapter1/TrannadarMap/iRoom33", 1530,  258)
zTrannadarMap:fnRegisterLookup("QuantirManseSecretExit",     "No Set",                                       1667,  564)
zTrannadarMap:fnRegisterLookup("QuantirManseSEHall",         "Root/Variables/Chapter1/TrannadarMap/iRoom32", 1853,  413)
zTrannadarMap:fnRegisterLookup("QuantirManseSWYard",         "Root/Variables/Chapter1/TrannadarMap/iRoom28", 1477,  400)
zTrannadarMap:fnRegisterLookup("QuantirManseSWYardInterior", "No Set",                                       1439,  332)
zTrannadarMap:fnRegisterLookup("QuantirManseTruth",          "No Set",                                       1341,  213)
zTrannadarMap:fnRegisterLookup("SpookyExterior",             "No Set",                                       1562,  573)

--Trafal
zTrannadarMap:fnRegisterLookup("TrafalNW", "No Set", 559, 1279)

-- |[ ======== Warp Listing ======== ]|
zTrannadarMap:fnRegisterWarpDestination("Dimensional Trap Basement East", "TrapBasementB",      "Root/Variables/Chapter1/Campfires/iTrapBasementB", 2322, 1286)
zTrannadarMap:fnRegisterWarpDestination("Dimensional Trap Basement West", "TrapBasementG",      "Root/Variables/Chapter1/Campfires/iTrapBasementG")
zTrannadarMap:fnRegisterWarpDestination("Trannadar Trading Post",         "EvermoonW",          "Root/Variables/Chapter1/Campfires/iEvermoonW",      614,  994)
zTrannadarMap:fnRegisterWarpDestination("Evermoon Forest South",          "EvermoonS",          "Root/Variables/Chapter1/Campfires/iEvermoonS")
zTrannadarMap:fnRegisterWarpDestination("Evermoon Forest East",           "EvermoonE",          "Root/Variables/Chapter1/Campfires/iEvermoonE")
zTrannadarMap:fnRegisterWarpDestination("Evermoon Forest North",          "EvermoonCassandraA", "Root/Variables/Chapter1/Campfires/iEvermoonCassandraA")
zTrannadarMap:fnRegisterWarpDestination("Breanne's Pit Stop",             "PlainsC",            "Root/Variables/Chapter1/Campfires/iPlainsC")
zTrannadarMap:fnRegisterWarpDestination("Bee Hive Basement",              "BeehiveBasementA",   "Root/Variables/Chapter1/Campfires/iBeehiveBasementA")
zTrannadarMap:fnRegisterWarpDestination("Quantir Estate Grounds",         "SpookyExterior",     "Root/Variables/Chapter1/Campfires/iSpookyExterior")
zTrannadarMap:fnRegisterWarpDestination("Dimensional Trap Dungeon",       "TrapDungeonA",       "Root/Variables/Chapter1/Campfires/iTrapDungeonA")
zTrannadarMap:fnRegisterWarpDestination("Alicia's Cabin",                 "EvermoonSEA",        "Root/Variables/Chapter1/Campfires/iEvermoonSEA")
       
-- |[ ========== Map Pins ========== ]|
--Create set.
local zTrannadarPins = zTrannadarMap:fnCreateMapPinObject()

--Health Catalyst Pins
zTrannadarPins:fnRegisterPin( 448,  724, sDeniseShowHealth, sVarPrefix .. "PlainsNW/ChestA",               gcsMapPinSet_IcoHlt)
zTrannadarPins:fnRegisterPin( 815,  573, sDeniseShowHealth, sVarPrefix .. "EvermoonNE/ChestB",             gcsMapPinSet_IcoHlt)
zTrannadarPins:fnRegisterPin(2198, 1259, sDeniseShowHealth, sVarPrefix .. "TrapBasementC/ChestA",          gcsMapPinSet_IcoHlt)
zTrannadarPins:fnRegisterPin(2478,  993, sDeniseShowHealth, sVarPrefix .. "TrapBasementH/ChestA",          gcsMapPinSet_IcoHlt)
zTrannadarPins:fnRegisterPin(1285, 1207, sDeniseShowHealth, sVarPrefix .. "EvermoonSlimeVillage/ChestA",   gcsMapPinSet_IcoHlt)
zTrannadarPins:fnRegisterPin(1668,  565, sDeniseShowHealth, sVarPrefix .. "QuantirManseSecretExit/ChestA", gcsMapPinSet_IcoHlt)

--Attack Catalyst Pins
zTrannadarPins:fnRegisterPin(2630, 1219, sDeniseShowPower, sVarPrefix .. "TrapBasementE/ChestB",        gcsMapPinSet_IcoAtk)
zTrannadarPins:fnRegisterPin(1781,  766, sDeniseShowPower, sVarPrefix .. "TrapMainFloorEast/ChestA",    gcsMapPinSet_IcoAtk)
zTrannadarPins:fnRegisterPin(1541,  927, sDeniseShowPower, sVarPrefix .. "TrapMainFloorCentral/ChestA", gcsMapPinSet_IcoAtk)

--Initiative Catalyst Pins
zTrannadarPins:fnRegisterPin(1067, 1418, sDeniseShowInitiative, sVarPrefix .. "EvermoonSEC/ChestA",           gcsMapPinSet_IcoIni)
zTrannadarPins:fnRegisterPin( 771, 1453, sDeniseShowInitiative, sVarPrefix .. "SaltFlats/ChestA",             gcsMapPinSet_IcoIni)
zTrannadarPins:fnRegisterPin(2250,  330, sDeniseShowInitiative, sVarPrefix .. "QuantirManseBasementE/ChestF", gcsMapPinSet_IcoIni)

--Accuracy Catalyst Pins
zTrannadarPins:fnRegisterPin(1106, 1039, sDeniseShowAccuracy, sVarPrefix .. "EvermoonE/ChestB",            gcsMapPinSet_IcoAcc)
zTrannadarPins:fnRegisterPin( 530,  983, sDeniseShowAccuracy, sVarPrefix .. "TrannadarTradingPost/ChestB", gcsMapPinSet_IcoAcc)

--Evasion Catalyst Pins
zTrannadarPins:fnRegisterPin( 938, 1235, sDeniseShowEvade, sVarPrefix .. "EvermoonS/ChestA",             gcsMapPinSet_IcoEvd)
zTrannadarPins:fnRegisterPin( 168,  395, sDeniseShowEvade, sVarPrefix .. "BeehiveBasementB/ChestA",      gcsMapPinSet_IcoEvd)
zTrannadarPins:fnRegisterPin(2192,  236, sDeniseShowEvade, sVarPrefix .. "QuantirManseBasementW/ChestA", gcsMapPinSet_IcoEvd)

--Skill Catalyst Pins
zTrannadarPins:fnRegisterPin(2045,  863, sDeniseShowSkills, sVarPrefix .. "TrapUpperFloorMain/ChestA", gcsMapPinSet_IcoSki)
zTrannadarPins:fnRegisterPin( 586, 1303, sDeniseShowSkills, sVarPrefix .. "TrafalNW/ChestB",           gcsMapPinSet_IcoSki)
zTrannadarPins:fnRegisterPin( 205,  251, sDeniseShowSkills, sVarPrefix .. "BeehiveBasementE/ChestA",   gcsMapPinSet_IcoSki)
zTrannadarPins:fnRegisterPin( 636,  665, sDeniseShowSkills, sVarPrefix .. "PlainsC/ChestD",            gcsMapPinSet_IcoSki)
zTrannadarPins:fnRegisterPin( 878,  374, sDeniseShowSkills, sVarPrefix .. "EvermoonCassandraA/ChestA", gcsMapPinSet_IcoSki)
        
-- |[ =============================== Trannadar West Map Lookups =============================== ]|
--Setup.
local zTrannadarWestMap = GUIMap:fnRegisterNewMap("Trannadar West")

--Layer Information
zTrannadarWestMap.iPieces          = gciTrannadarWestMapsTotal
zTrannadarWestMap.sBaseDLPath      = "Root/Images/AdvMaps/TrannadarWest/Base"
zTrannadarWestMap.sLayerPattern    = "Null"
zTrannadarWestMap.sVariablePattern = "Root/Variables/Chapter1/TrannadarWestMap/iRoom%02i"
zTrannadarWestMap.sPiecePattern    = "Root/Images/AdvMaps/TrannadarWest/Slice_%02i"
zTrannadarWestMap.sOverlayDLPath   = "Root/Images/AdvMaps/TrannadarWest/Overlay"

--Warp Information
zTrannadarWestMap.sCampfireWarpIconPath  = "Root/Images/AdventureUI/AdvCampfireWarpIcon/TrannadarW"

-- |[ ========== Lookups =========== ]|
--Evermoon Forest
zTrannadarWestMap:fnRegisterLookup("ArbonnePlainsA", "No Set", 2257, 335)
zTrannadarWestMap:fnRegisterLookup("ArbonnePlainsB", "No Set", 2224, 493)
zTrannadarWestMap:fnRegisterLookup("ArbonnePlainsC", "No Set", 2039, 274)
zTrannadarWestMap:fnRegisterLookup("ArbonnePlainsD", "No Set", 2016,  95)

--Starfield Swamp
zTrannadarWestMap:fnRegisterLookup("StarfieldSwampA", "No Set",                                           2554,  781)
zTrannadarWestMap:fnRegisterLookup("StarfieldSwampB", "No Set",                                           2326,  772)
zTrannadarWestMap:fnRegisterLookup("StarfieldSwampC", "No Set",                                           2087,  783)
zTrannadarWestMap:fnRegisterLookup("StarfieldSwampD", "No Set",                                           1921,  771)
zTrannadarWestMap:fnRegisterLookup("StarfieldSwampE", "No Set",                                           1710,  777)
zTrannadarWestMap:fnRegisterLookup("StarfieldSwampF", "No Set",                                           1353,  773)
zTrannadarWestMap:fnRegisterLookup("StarfieldSwampG", "Root/Variables/Chapter1/TrannadarWestMap/iRoom04", 2265,  646)
zTrannadarWestMap:fnRegisterLookup("StarfieldSwampH", "No Set",                                           1930,  607)
zTrannadarWestMap:fnRegisterLookup("StarfieldSwampI", "No Set",                                           1928,  924)

--St. Fora's Convent
zTrannadarWestMap:fnRegisterLookup("StForasA", "Root/Variables/Chapter1/TrannadarWestMap/iRoom05", 2110, 1321)
zTrannadarWestMap:fnRegisterLookup("StForasB", "Root/Variables/Chapter1/TrannadarWestMap/iRoom06", 1971, 1149)
zTrannadarWestMap:fnRegisterLookup("StForasC", "Root/Variables/Chapter1/TrannadarWestMap/iRoom07", 2099, 1146)
zTrannadarWestMap:fnRegisterLookup("StForasD", "Root/Variables/Chapter1/TrannadarWestMap/iRoom10",  150, 1309)
zTrannadarWestMap:fnRegisterLookup("StForasE", "Root/Variables/Chapter1/TrannadarWestMap/iRoom11",  659, 1186)
zTrannadarWestMap:fnRegisterLookup("StForasF", "Root/Variables/Chapter1/TrannadarWestMap/iRoom12", 1514, 1224)
zTrannadarWestMap:fnRegisterLookup("StForasG", "Root/Variables/Chapter1/TrannadarWestMap/iRoom08", 1963, 1136)
zTrannadarWestMap:fnRegisterLookup("StForasH", "Root/Variables/Chapter1/TrannadarWestMap/iRoom09", 2289, 1145)
zTrannadarWestMap:fnRegisterLookup("StForasI", "No Set",                                           2516,  897)

--River Wilds
zTrannadarWestMap:fnRegisterLookup("DrinkingContest",      "No Set",                                           1036,  793)
zTrannadarWestMap:fnRegisterLookup("FishingVillageRubber", "No Set",                                           1036,  793)
zTrannadarWestMap:fnRegisterLookup("RiverWildsA",          "No Set",                                           1036,  793)
zTrannadarWestMap:fnRegisterLookup("RiverWildsB",          "No Set",                                            877,  578)
zTrannadarWestMap:fnRegisterLookup("RiverWildsC",          "No Set",                                            844,  299)
zTrannadarWestMap:fnRegisterLookup("RiverWildsD",          "No Set",                                           1024,  234)
zTrannadarWestMap:fnRegisterLookup("WildsTowerA",          "No Set",                                            779,  135)
zTrannadarWestMap:fnRegisterLookup("WildsTowerB",          "Root/Variables/Chapter1/TrannadarWestMap/iRoom00",  528,  638)
zTrannadarWestMap:fnRegisterLookup("WildsTowerC",          "Root/Variables/Chapter1/TrannadarWestMap/iRoom03",  246,  652)
zTrannadarWestMap:fnRegisterLookup("WildsTowerD",          "Root/Variables/Chapter1/TrannadarWestMap/iRoom01",  565,  387)
zTrannadarWestMap:fnRegisterLookup("WildsTowerE",          "Root/Variables/Chapter1/TrannadarWestMap/iRoom02",  528,  139)

-- |[ ======== Warp Listing ======== ]|
zTrannadarWestMap:fnRegisterWarpDestination("Fishing Village", "StarfieldSwampF", "Root/Variables/Chapter1/Campfires/iStarfieldSwampF")
zTrannadarWestMap:fnRegisterWarpDestination("Polaris' Cabin",  "StarfieldSwampG", "Root/Variables/Chapter1/Campfires/iStarfieldSwampG")
zTrannadarWestMap:fnRegisterWarpDestination("Saint Fora's",    "StForasA",        "Root/Variables/Chapter1/Campfires/iStForasA")
zTrannadarWestMap:fnRegisterWarpDestination("Dormine Tower",   "WildsTowerA",     "Root/Variables/Chapter1/Campfires/iWildsTowerA")

-- |[ ========== Map Pins ========== ]|
--Variables/constants.
--Create set.
local zTrannadarWestPins = zTrannadarWestMap:fnCreateMapPinObject()

--Health
zTrannadarWestPins:fnRegisterPin(1222, 1424, sDeniseShowHealth, sVarPrefix .. "StForasF/ChestF",       gcsMapPinSet_IcoHlt)
zTrannadarWestPins:fnRegisterPin(1900,  337, sDeniseShowHealth, sVarPrefix .. "ArbonnePlainsC/ChestF", gcsMapPinSet_IcoHlt)
zTrannadarWestPins:fnRegisterPin( 739,  150, sDeniseShowHealth, sVarPrefix .. "WildsTowerA/ChestB",    gcsMapPinSet_IcoHlt)

--Accuracy
zTrannadarWestPins:fnRegisterPin(1999,   62, sDeniseShowAccuracy, sVarPrefix .. "ArbonnePlainsD/ChestB", gcsMapPinSet_IcoAcc)

--Skill
zTrannadarWestPins:fnRegisterPin(2256,  768, sDeniseShowSkills, sVarPrefix .. "StarfieldSwampB/ChestE", gcsMapPinSet_IcoSki)


-- |[ ================================= Mausoleum Map Lookups ================================== ]|
--Setup.
local zMausoleumMap = GUIMap:fnRegisterNewMap("Mausoleum")

--Layer Information
zMausoleumMap.iPieces          = gciMausoleumMapsTotal
zMausoleumMap.sBaseDLPath      = "Root/Images/AdvMaps/Mausoleum/Base"
zMausoleumMap.sLayerPattern    = "Null"
zMausoleumMap.sVariablePattern = "Root/Variables/Chapter1/Mausoleum/iRoom%02i"
zMausoleumMap.sPiecePattern    = "Root/Images/AdvMaps/Mausoleum/Slice_%02i"
zMausoleumMap.sOverlayDLPath   = "Root/Images/AdvMaps/Mausoleum/Overlay"

--Warp Information
zMausoleumMap.sCampfireWarpIconPath  = "Root/Images/AdventureUI/AdvCampfireWarpIcon/TrannadarW"

-- |[ ========== Lookups =========== ]|
--All lookups are in the same set.
zMausoleumMap:fnRegisterLookup("StarfieldCavesA",     "Root/Variables/Chapter1/Mausoleum/iRoom00",  332,  440)
zMausoleumMap:fnRegisterLookup("StarfieldCavesB",     "Root/Variables/Chapter1/Mausoleum/iRoom01",  691,  507)
zMausoleumMap:fnRegisterLookup("StarfieldCavesC",     "Root/Variables/Chapter1/Mausoleum/iRoom02",  640,  234)
zMausoleumMap:fnRegisterLookup("StarfieldMausoleumA", "Root/Variables/Chapter1/Mausoleum/iRoom03", 1025,  244)
zMausoleumMap:fnRegisterLookup("StarfieldMausoleumB", "Root/Variables/Chapter1/Mausoleum/iRoom04", 1578,  251)
zMausoleumMap:fnRegisterLookup("StarfieldMausoleumC", "Root/Variables/Chapter1/Mausoleum/iRoom05", 1311,  657)
zMausoleumMap:fnRegisterLookup("StarfieldMausoleumD", "Root/Variables/Chapter1/Mausoleum/iRoom06", 1911,  642)
zMausoleumMap:fnRegisterLookup("StarfieldMausoleumE", "Root/Variables/Chapter1/Mausoleum/iRoom07", 2267,  399)
zMausoleumMap:fnRegisterLookup("TrannadarDescentA",   "Root/Variables/Chapter1/Mausoleum/iRoom08",  470, 1180)
zMausoleumMap:fnRegisterLookup("TrannadarDescentB",   "Root/Variables/Chapter1/Mausoleum/iRoom09", 1107, 1087)
zMausoleumMap:fnRegisterLookup("TrannadarDescentC",   "Root/Variables/Chapter1/Mausoleum/iRoom10", 1648, 1104)
zMausoleumMap:fnRegisterLookup("TrannadarReserveA",   "Root/Variables/Chapter1/Mausoleum/iRoom11", 2306, 1180)

-- |[ ======== Warp Listing ======== ]|
--No warp destinations in this area.

-- |[ ========== Map Pins ========== ]|
--Create set.
local zMausoleumMapPins = zMausoleumMap:fnCreateMapPinObject()

--Health
zMausoleumMapPins:fnRegisterPin(1670, 212, sDeniseShowHealth, sVarPrefix .. "StarfieldMausoleumB/ChestA", gcsMapPinSet_IcoHlt)