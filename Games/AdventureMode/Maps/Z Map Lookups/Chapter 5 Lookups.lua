-- |[Chapter 5 Lookups]|
--Sets the master list of map remaps for Chapter 5.
gsaMapLookups = {}

-- |[Important Note]|
--Many of the buildings in chapter 5 have local-area maps rendered in the PDU. The setups for these
-- are in the map constructors, not here. This handles large common maps.

-- |[Chapter Lockout]|
--Don't boot maps when loading the game if this flag is true. The first time the map lookups are called,
-- the game hasn't actually loaded yet and we don't want to waste time building map lookups.
if(gbDisallowMapConstruction == true) then return end

--Do nothing if chapter 5 is not active.
local iCurrentChapter = VM_GetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N")
if(iCurrentChapter ~= 5.0) then return end

--Clear any existing map lookups.
GUIMap:fnClearGlobalList()

-- |[Functions]|
--Variables used.
local iIndex = 1

--Adds a single lookup.
local fnAddLookup = function(psName, psMap, piX, piY)
	gsaMapLookups[iIndex] = {psName, psMap, piX, piY}
    iIndex = iIndex + 1
end

--Adds a lookup between two letters, ex: "BeehiveBasement" "A", "C", will do A, B, and C, in order.
local fnAddLookups = function(psNamePattern, psStartLetter, psEndLetter, psMap, piX, piY)
    
	--Setup.
	local sCurrentLetter = psStartLetter
	
	--Range check. If the letter is out of the normal range, fail.
	local iByteValue = string.byte(sCurrentLetter)
	if(iByteValue < string.byte("A") or iByteValue > string.byte("Z")) then return end
	
	--Iterate across the letters. Always stops on Z. Also always stops on non-capitals.
	while(true) do
		
		--Add it.
		local sLevelName = psNamePattern .. sCurrentLetter
        gsaMapLookups[iIndex] = {sLevelName, psMap, piX, piY}
        iIndex = iIndex + 1
		
		--If this was the ending letter, or the letter 'Z', stop.
		if(sCurrentLetter == psEndLetter or sCurrentLetter == "Z") then break end
		
		--Move to the next letter.
		local iByteValue = string.byte(sCurrentLetter)
		sCurrentLetter = string.char(iByteValue + 1)
	end
    
end

-- |[Map Coordinates]|
--Maps/RegulusArcane
fnAddLookups("RegulusArcane", "A", "H", "RegulusMap", 1198, 596)
fnAddLookup( "RegulusArcaneCAlt",       "RegulusMap", 1198, 596)

--Maps/RegulusBiolabs
fnAddLookup("RegulusBiolabsAlphaA",      "BiolabsMap", 1407, 1428)
fnAddLookup("RegulusBiolabsAlphaB",      "BiolabsMap", 1405, 1349)
fnAddLookup("RegulusBiolabsAlphaC",      "BiolabsMap", 1101, 1331)
fnAddLookup("RegulusBiolabsAlphaD",      "BiolabsMap", 1466, 1259)
fnAddLookup("RegulusBiolabsAmphibianA",  "BiolabsMap",  918,  194)
fnAddLookup("RegulusBiolabsAmphibianB",  "BiolabsMap",  893,  164)
fnAddLookup("RegulusBiolabsAmphibianC",  "BiolabsMap",  853,  132)
fnAddLookup("RegulusBiolabsAmphibianD",  "BiolabsMap",  853,   99)
fnAddLookup("RegulusBiolabsAmphibianE",  "BiolabsMap",  853,   99)
fnAddLookup("RegulusBiolabsBetaA",       "BiolabsMap",  713,  634)
fnAddLookup("RegulusBiolabsBetaB",       "BiolabsMap",  791,  567)
fnAddLookup("RegulusBiolabsBetaC",       "BiolabsMap", 1053,  599)
fnAddLookup("RegulusBiolabsBetaD",       "BiolabsMap",  971,  264)
fnAddLookup("RegulusBiolabsBetaE",       "BiolabsMap", 1258,  654)
fnAddLookup("RegulusBiolabsBetaMeltedA", "BiolabsMap",  713,  634)
fnAddLookup("RegulusBiolabsBetaMeltedB", "BiolabsMap",  791,  567)
fnAddLookup("RegulusBiolabsBetaMeltedC", "BiolabsMap", 1053,  599)
fnAddLookup("RegulusBiolabsBetaMeltedD", "BiolabsMap",  971,  264)
fnAddLookup("RegulusBiolabsBetaMeltedE", "BiolabsMap", 1258,  654)
fnAddLookup("RegulusBiolabsDatacoreA",   "BiolabsMap",  758, 1074)
fnAddLookup("RegulusBiolabsDatacoreB",   "BiolabsMap",  853,  954)
fnAddLookup("RegulusBiolabsDatacoreC",   "BiolabsMap",  853,  924)
fnAddLookup("RegulusBiolabsDatacoreD",   "BiolabsMap",  853,  924)
fnAddLookup("RegulusBiolabsDatacoreE",   "BiolabsMap",  853,  924)
fnAddLookup("RegulusBiolabsDatacoreF",   "BiolabsMap",  751,  897)
fnAddLookup("RegulusBiolabsDatacoreG",   "BiolabsMap",  716,  834)
fnAddLookup("RegulusBiolabsDatacoreH",   "BiolabsMap",  698,  714)
fnAddLookup("RegulusBiolabsDeltaA",      "BiolabsMap", 1738, 1234)
fnAddLookup("RegulusBiolabsDeltaB",      "BiolabsMap", 1878, 1232)
fnAddLookup("RegulusBiolabsDeltaC",      "BiolabsMap", 1988, 1239)
fnAddLookup("RegulusBiolabsDeltaD",      "BiolabsMap", 2018, 1317)
fnAddLookup("RegulusBiolabsDeltaE",      "BiolabsMap", 2083, 1252)
fnAddLookup("RegulusBiolabsDeltaF",      "BiolabsMap", 1890, 1297)
fnAddLookup("RegulusBiolabsDeltaG",      "BiolabsMap", 1908, 1354)
fnAddLookup("RegulusBiolabsDeltaH",      "BiolabsMap", 1760, 1371)
fnAddLookup("RegulusBiolabsDeltaI",      "BiolabsMap", 2018, 1371)
fnAddLookup("RegulusBiolabsEpsilonA",    "BiolabsMap", 1425,  652)
fnAddLookup("RegulusBiolabsEpsilonB",    "BiolabsMap", 1418,  469)
fnAddLookup("RegulusBiolabsEpsilonC",    "BiolabsMap", 1418,  469)
fnAddLookup("RegulusBiolabsEpsilonD",    "BiolabsMap", 1418,  469)
fnAddLookup("RegulusBiolabsEpsilonE",    "BiolabsMap", 1418,  469)
fnAddLookup("RegulusBiolabsGammaA",      "BiolabsMap", 1388,  807)
fnAddLookup("RegulusBiolabsGammaB",      "BiolabsMap", 1360,  952)
fnAddLookup("RegulusBiolabsGammaC",      "BiolabsMap", 1370, 1067)
fnAddLookup("RegulusBiolabsGammaD",      "BiolabsMap", 1553, 1079)
fnAddLookup("RegulusBiolabsGammaWestA",  "BiolabsMap", 1173,  952)
fnAddLookup("RegulusBiolabsGammaWestB",  "BiolabsMap", 1043,  884)
fnAddLookup("RegulusBiolabsGammaWestC",  "BiolabsMap", 1178,  877)
fnAddLookup("RegulusBiolabsGammaWestD",  "BiolabsMap", 1178,  877)
fnAddLookup("RegulusBiolabsGammaWestE",  "BiolabsMap", 1178,  877)
fnAddLookup("RegulusBiolabsGeneticsA",   "BiolabsMap", 2070, 1406)
fnAddLookup("RegulusBiolabsGeneticsB",   "BiolabsMap", 2070, 1406)
fnAddLookup("RegulusBiolabsGeneticsC",   "BiolabsMap", 2070, 1406)
fnAddLookup("RegulusBiolabsGeneticsD",   "BiolabsMap", 2070, 1406)
fnAddLookup("RegulusBiolabsGeneticsE",   "BiolabsMap", 2070, 1406)
fnAddLookup("RegulusBiolabsGeneticsF",   "BiolabsMap", 2070, 1406)
fnAddLookup("RegulusBiolabsGeneticsG",   "BiolabsMap", 2070, 1406)
fnAddLookup("RegulusBiolabsGeneticsH",   "BiolabsMap", 2070, 1406)
fnAddLookup("RegulusBiolabsHydroponicsA","BiolabsMap",  768, 1299)
fnAddLookup("RegulusBiolabsHydroponicsB","BiolabsMap",  768, 1299)
fnAddLookup("RegulusBiolabsHydroponicsC","BiolabsMap",  768, 1299)
fnAddLookup("RegulusBiolabsHydroponicsD","BiolabsMap",  768, 1062)
fnAddLookup("RegulusBiolabsRaijuRanchA", "BiolabsMap", 1910, 1159)
fnAddLookup("RegulusBiolabsRaijuRanchB", "BiolabsMap", 2003, 1122)
fnAddLookup("RegulusBiolabsRaijuRanchC", "BiolabsMap", 1910, 1077)
fnAddLookup("RegulusBiolabsTransitA",    "BiolabsMap", 1718, 1082)
fnAddLookup("RegulusBiolabsTransitB",    "BiolabsMap", 1750, 1159)

--Maps/RegulusCity
fnAddLookups("LowerRegulusCity", "A", "D", "RegulusMap", 1149, 696)
fnAddLookups("RegulusCity15",    "A", "C", "RegulusMap",  816, 679)
fnAddLookups("RegulusCity119",   "A", "B", "RegulusMap", 1043, 640)
fnAddLookups("RegulusCity198",   "A", "D", "RegulusMap", 1305, 638)
fnAddLookups("RegulusCity",      "A", "G", "RegulusMap", 1140, 711)
fnAddLookup("RegulusCityX",                "RegulusMap", 1140, 711)
fnAddLookup("RegulusCityZ",                "RegulusMap", 1140, 711)

--Maps/RegulusCryo
--  Uses local maps

--Maps/RegulusEquinox
fnAddLookups("RegulusEquinox", "A", "H", "RegulusMap", 472, 927)

--Maps/RegulusExterior
fnAddLookup("RegulusExteriorEA",       "RegulusMap", 1626,  722)
fnAddLookup("RegulusExteriorEB",       "RegulusMap", 1687,  748)
fnAddLookup("RegulusExteriorEC",       "RegulusMap", 1950,  737)
fnAddLookup("RegulusExteriorED",       "RegulusMap", 2112,  750)
fnAddLookup("RegulusExteriorEE",       "RegulusMap", 1697, 1050)
fnAddLookup("RegulusExteriorEF",       "RegulusMap", 1792, 1079)
fnAddLookup("RegulusExteriorSA",       "RegulusMap", 1163, 1094)
fnAddLookup("RegulusExteriorSB",       "RegulusMap", 1183,  986)
fnAddLookup("RegulusExteriorSC",       "RegulusMap", 1157,  793)
fnAddLookup("RegulusExteriorSD",       "RegulusMap", 1256,  774)
fnAddLookup("RegulusExteriorSE",       "RegulusMap", 1194,  901)
fnAddLookup("RegulusExteriorStationE", "RegulusMap", 1578,  689)
fnAddLookup("RegulusExteriorTRNA",     "RegulusMap", 1369, 1025)
fnAddLookup("RegulusExteriorTRNB",     "RegulusMap", 1369, 1025)
fnAddLookup("RegulusExteriorWA",       "RegulusMap",  756,  737)
fnAddLookup("RegulusExteriorWAUp",     "RegulusMap",  756,  737)
fnAddLookup("RegulusExteriorWB",       "RegulusMap",  637,  823)
fnAddLookup("RegulusExteriorWC",       "RegulusMap",  529,  955)

--Maps/RegulusFlashback
--  No Maps

--Maps/RegulusLRT
--  Uses local maps

--Maps/RegulusManufactory
fnAddLookups("RegulusManufactory", "A", "J", "RegulusMap", 1136, 321)

--Maps/RegulusMines
fnAddLookup("TelluriumMinesA", "RegulusMap", 1435, 480)
fnAddLookup("TelluriumMinesB", "RegulusMap", 1435, 480)
fnAddLookup("TelluriumMinesE", "RegulusMap", 1508, 520)
fnAddLookup("BlackSiteA",      "RegulusMap", 1590, 453)

--Maps/RegulusSerenity
fnAddLookups("SerenityCrater", "A", "E", "RegulusMap", 1830, 1154)
fnAddLookup( "SerenityCraterF",          "RegulusMap", 1841, 1249)
fnAddLookup( "SerenityCraterG",          "RegulusMap", 1841, 1249)
--SerenityCraterH does not have a map location.
fnAddLookups("SerenityObservatory", "A", "D", "RegulusMap", 1782, 1061)
fnAddLookup( "SerenityObservatoryE",          "RegulusMap", 1782, 1150)

