-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
-- |[Objects]|
if(sObjectName == "ToTransitB") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsTransitB", "FORCEPOS:8.0x26.0x0")
    
elseif(sObjectName == "SignA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Duck Pond)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Please help yourselves![P] The ducks love our fruit![P] -Biological Services')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Boxes") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Gardening equipment.[P] Watering cans, fertilizer, hand troughs.[P] Doesn't look made for Slave Units...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Crops") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Crops being grown without the usual indicators of Golem activity.[P] No automated sprinklers, no fertilizer pellets...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Ducks") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The ducks don't seem to notice the monsters, and the monsters don't seem to notice the ducks.)") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
