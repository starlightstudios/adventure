-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
-- |[Objects]|
if(sObjectName == "SecretDoor") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Sammy] (I can't leave now![P] I need to find that evidence!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Exit") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Sammy] (I can't leave now![P] I need to find that evidence!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BigBoard") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Sammy] ('This worksite 37 days without an accident')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Knives") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Sammy] (A table with some knives on it.[P] Nothing suspicious about this whatsoever.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Chests") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Sammy] (Some chests.[P] Nothing that a tough-as-nails agent like me needs.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Terminal") then
    
    --Spawn NPCs.
    TA_Create("Drone")
        TA_SetProperty("Position", -1, -1)
        TA_SetProperty("Facing", gci_Face_North)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/LatexDrone/", false)
        TA_SetProperty("Add Special Frame", "Arm0", "Root/Images/Sprites/Special/LatexDrone|Arm0")
        TA_SetProperty("Add Special Frame", "Arm1", "Root/Images/Sprites/Special/LatexDrone|Arm1")
        TA_SetProperty("Add Special Frame", "Arm2", "Root/Images/Sprites/Special/LatexDrone|Arm2")
        TA_SetProperty("Add Special Frame", "Arm3", "Root/Images/Sprites/Special/LatexDrone|Arm3")
        TA_SetProperty("Add Special Frame", "Arm4", "Root/Images/Sprites/Special/LatexDrone|Arm4")
    DL_PopActiveObject()
    
    --Collision removal.
    AL_SetProperty("Set Collision", 16, 7, 0, 0)
    AL_SetProperty("Set Collision", 16, 8, 0, 0)
    AL_SetProperty("Set Collision", 16, 9, 0, 0)
    AL_SetProperty("Set Collision", 17, 7, 0, 0)
    AL_SetProperty("Set Collision", 17, 8, 0, 0)
    AL_SetProperty("Set Collision", 17, 9, 0, 0)
    AL_SetProperty("Set Collision", 18, 7, 0, 0)
    AL_SetProperty("Set Collision", 18, 8, 0, 0)
    AL_SetProperty("Set Collision", 18, 9, 0, 0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Let's see here...[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Evil research, Evil engineering reports, Evil budget, Evil conference calls with evil clients...[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] *Click*[P] Hello, Evil Cloning?[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] *Click*[P] *Click*[P] That's -[P] that's me![P] A clone of me?[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Sexy] Those rapscallions made a duplicate Agent Almond and had her commit those crimes![P] Of course![B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Come in, Agent Coconut![P] I've got what we need![P] Is our exit ready?[B][C]") ]])
    fnCutscene([[ Append("Coconut:[E|Neutral] [VOICE|Coconut]All ready outside.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Lock on to my tracking beacon, I think there's a quicker way out.[P] Hurry!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneTeleport("Drone", 26.25, 6.50)
    fnCutsceneBlocker()
    fnCutsceneMove("Drone", 26.25, 5.50)
    fnCutsceneFace("Drone", -1, 0)
    fnCutsceneFace("Sammy", 1, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Sammy", 23.25, 9.50, 2.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Drone:[VOICE|LatexDrone] AFFIRMATIVE, DIRECTOR.[P] SEARCHING FOR SOURCE OF DATABASE QUERY.[P] WILL REPORT IF SITUATION IS NOT UP TO SPIFFINESS STANDARDS.[B][C]") ]])
    fnCutscene([[ Append("Almond:[VOICE|Sammy] (Crud![P] Where'd she come from!?[P] I gotta get out of here!)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sammy", 23.25, 5.50, 2.50)
    fnCutsceneFace("Sammy", 1, 0)
    fnCutsceneMove("Drone", 24.25, 5.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "LatexDrone", "Neutral") ]])
    fnCutscene([[ Append("Drone:[E|Neutral] UNAUTHORIZED MOVING TARGET DETECTED.[P] SUBJECT WILL BECOME AN UNAUTHORIZED STATIONARY TARGET.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Sexy] (Wits don't fail me now!)[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Unauthorized?[P] I work here![P] You're the one who is unauthorized![B][C]") ]])
    fnCutscene([[ Append("Drone:[E|Neutral] NEGATIVE.[P] THIS UNIT HAS MAXIMUM AUTHORIZATION FROM THE DIRECTOR.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Sexy] (Crud!)[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Yeah, well, uh, 2 + 2 = 5![B][C]") ]])
    fnCutscene([[ Append("Drone:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] (Hopefully that will cause a logic error!)[B][C]") ]])
    fnCutscene([[ Append("Drone:[E|Neutral] THIS UNIT WILL TAKE YOUR WORD FOR IT.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Sexy] (Double crud!)[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Uhm -[P] this statement is false![B][C]") ]])
    fnCutscene([[ Append("Drone:[E|Neutral] AFFIRMATIVE.[P] THE STATEMENT IS FALSE.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Ah-ha![P] But if the statement is false, it is the opposite of false![B][C]") ]])
    fnCutscene([[ Append("Drone:[E|Neutral] AFFIRMATIVE.[P] THE STATEMENT IS TRUE.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] But if the statement is true, then how can it be false?[B][C]") ]])
    fnCutscene([[ Append("Drone:[E|Neutral] IT IS NOT.[P] IT IS FALSE.[B][C]") ]])
    fnCutscene([[ Append("Drone:[E|Neutral] THIS DRONE THINKS THAT IS FAIRLY OBVIOUS, SINCE THAT IS WHAT YOU SAID THE FIRST TIME.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Sexy] (Triple crud![P] This unit is the ultimate security drone -[P] too dumb to trick!)[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Well -[P] what if we step over here and I do this?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sammy", 21.25, 5.50)
    fnCutsceneMove("Sammy", 21.25, 6.50)
    fnCutsceneMove("Sammy", 20.25, 6.50)
    fnCutsceneFace("Sammy", 1, 0)
    fnCutsceneMove("Drone", 21.25, 5.50)
    fnCutsceneMove("Drone", 21.25, 6.50)
    fnCutsceneFace("Drone", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    --fnCutsceneTeleport("Sammy", 20.25, 6.50)
   -- fnCutsceneTeleport("Drone", 21.25, 6.50)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Almond:[VOICE|Sammy] Just watch my hips...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Mandated Music Intensity", 60.0) ]])
    
    --Dancing!
    local fRadius = 1.0
    local iTicksPerMove = 4
    local iDegreeSpeed = 2
    local iTicksPerFrame = 6
    local iTicks = 1
    local iDanceFrame = 0
    local iSpeedupCount = 0
    for iLoops = 1, 5, 1 do
        
        --For each loop, compute rotations by degrees.
        local iDegrees = 0
        while(iDegrees < 360) do
        
            --Compute position.
            local fRadians = ((iDegrees * -1) + 180) * 3.1415926 / 180
            local fXPos = 21.25 + (math.cos(fRadians) * fRadius)
            local fYPos =  6.50 + (math.sin(fRadians) * fRadius)
            fnCutsceneTeleport("Sammy", fXPos, fYPos)
            fnCutsceneSetFrame("Sammy", "DanceE" .. iDanceFrame)
            fnCutsceneFaceTarget("Drone", "Sammy")
            fnCutsceneWait(iTicksPerMove)
            fnCutsceneBlocker()
            
            --Increment the ticks.
            iTicks = iTicks + iTicksPerMove
            if(iTicks > iTicksPerFrame) then
                iTicks = iTicks - iTicksPerFrame
                iDanceFrame = iDanceFrame + 1
                if(iDanceFrame >= 8) then iDanceFrame = 0 end
            end
            
            --Speedup counter.
            iSpeedupCount = iSpeedupCount + 1
            if(iSpeedupCount >= 10 and iDegreeSpeed < 30) then
                iSpeedupCount = 0
                iDegreeSpeed = iDegreeSpeed + 2
            end
            
            --Increment.
            iDegrees = iDegrees + iDegreeSpeed
        end
    
    end
    
    --At the end of that sequence, the drone continues to spin while Sammy does her finisher.
    fnCutsceneSetFrame("Sammy", "DanceF0")
    fnCutsceneWait(6)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Sammy", "DanceF1")
    fnCutsceneFace("Drone", 0, 1)
    fnCutsceneWait(6)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Sammy", "DanceF2")
    fnCutsceneFace("Drone", 1, 0)
    fnCutsceneWait(6)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Sammy", "DanceF3")
    fnCutsceneFace("Drone", 0, -1)
    fnCutsceneWait(6)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Sammy", "DanceF4")
    fnCutsceneFace("Drone", -1, 0)
    fnCutsceneWait(6)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Sammy", "DanceF5")
    fnCutsceneFace("Drone", 0, 1)
    fnCutsceneWait(6)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Sammy", "DanceF6")
    fnCutsceneFace("Drone", 1, 0)
    fnCutsceneWait(6)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Sammy", "DanceF7")
    fnCutsceneFace("Drone", 0, -1)
    fnCutsceneWait(6)
    fnCutsceneBlocker()
    fnCutsceneFace("Drone", -1, 0)
    fnCutsceneWait(6)
    fnCutsceneBlocker()
    fnCutsceneFace("Drone", 0, 1)
    fnCutsceneWait(6)
    fnCutsceneBlocker()
    
    --A bit more spinning.
    local iWaitTicks = 6
    for i = 1, 5, 1 do
        fnCutsceneFace("Drone", 1, 0)
        fnCutsceneWait(iWaitTicks)
        iWaitTicks = iWaitTicks + 1
        fnCutsceneBlocker()
        
        fnCutsceneFace("Drone", 0, -1)
        fnCutsceneWait(iWaitTicks)
        iWaitTicks = iWaitTicks + 1
        fnCutsceneBlocker()
        
        fnCutsceneFace("Drone", -1, 0)
        fnCutsceneWait(iWaitTicks)
        iWaitTicks = iWaitTicks + 1
        fnCutsceneBlocker()
        
        fnCutsceneFace("Drone", 0, 1)
        fnCutsceneWait(iWaitTicks)
        iWaitTicks = iWaitTicks + 1
        fnCutsceneBlocker()
    end
        
    
    --Dialogue.
    fnCutscene([[ AL_SetProperty("Mandated Music Intensity", 20.0) ]])
    fnCutsceneFace("Sammy", 1, 0)
    fnCutsceneSetFrame("Sammy", "Null")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "LatexDrone", "Neutral") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] (Yikes![P] I'm dizzy as hell, but I bet that drone is even dizzier!)[B][C]") ]])
    fnCutscene([[ Append("Drone:[E|Neutral] ...[P] THE ROOM IS SPINNING.[B][C]") ]])
    fnCutscene([[ Append("Drone:[E|Neutral] RECALIBRATING...[B][C]") ]])
    fnCutscene([[ Append("Drone:[E|Neutral] RECALIBRATIONS COMPLETED.[P] ROOM NO LONGER SPINNING.[P] GYROSCOPES STABILIZED.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Sexy] (Damn perfect superior robots![P] This isn't fair!)[B][C]") ]])
    fnCutscene([[ Append("Drone:[E|Neutral] UNAUTHORIZED DANCING DETECTED.[P] INITIATING ARREST PROTOCOLS.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Sexy] (Quadruple crud with crud biscuits for a side!)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutscene([[ AL_SetProperty("Mandated Music Intensity", 60.0) ]])
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneMoveFace("Sammy", 16.25, 6.50, 1, 0)
    fnCutsceneMoveFace("Drone", 17.25, 6.50, -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
        
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "LatexDrone", "Neutral") ]])
    fnCutscene([[ Append("Almond:[E|Gun] You're pretty tough, but I'm Agent Almond![B][C]") ]])
    fnCutscene([[ Append("Drone:[E|Neutral] AFFIRMATIVE.[P] YOU ARE AGENT ALMOND.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Was that a problem?[B][C]") ]])
    fnCutscene([[ Append("Drone:[E|Neutral] NEGATIVE.[P] THIS UNIT SIMPLY DIDN'T KNOW WHAT TO SAY AND DECIDED TO REPEAT WHAT YOU SAID TO SHOW ENGAGEMENT.[B][C]") ]])
    fnCutscene([[ Append("Drone:[E|Neutral] THIS UNIT IS TRYING TO ARREST YOU, NOT HURT YOUR FEELINGS.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Gun] Sorry, but I'm going to have to hurt more than your feelings![P] HIYAH!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --ACTION!
    fnCutsceneMoveFace("Drone", 16.25, 7.50, 0, -1, 2.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Sammy", 0, 1)
    fnCutsceneMoveFace("Drone", 15.25, 6.50, 1, 0, 2.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Sammy", -1, 0)
    fnCutsceneMoveFace("Drone", 15.00, 6.50, 1, 0, 2.50)
    fnCutsceneMoveFace("Drone", 13.40, 6.50, 1, 0, 1.50)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneMoveFace("Drone", 11.25, 6.50, 1, 0, 0.50)
    fnCutsceneBlocker()
    fnCutsceneWait(85)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Drone", 15.25, 6.50, 1, 0, 2.50)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneMoveFace("Drone", 16.25, 6.50, 1, 0, 2.50)
    fnCutsceneMoveFace("Sammy", 17.25, 6.50, -1, 0, 2.50)
    fnCutsceneMoveFace("Sammy", 18.25, 6.50, -1, 0, 1.50)
    fnCutsceneMoveFace("Sammy", 19.25, 6.50, -1, 0, 1.50)
    fnCutsceneBlocker()
    fnCutsceneWait(85)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Drone", 18.25, 6.50, 1, 0, 1.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Drone", 18.75, 6.50, 1, 0, 0.50)
    fnCutsceneMoveFace("Sammy", 19.75, 6.50, -1, 0, 0.50)
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Drone", 17.75, 6.50, 1, 0, 0.50)
    fnCutsceneMoveFace("Sammy", 18.75, 6.50, -1, 0, 0.50)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sammy", 20.25, 6.50, -1, 0, 0.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sammy", 20.25, 5.50, 0, -1, 2.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|TakeWeapon") ]])
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Knife", true) ]])
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "NoKnife", false) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Sammy", -1, 0)
    fnCutsceneMoveFace("Drone", 17.75, 5.50, 1, 0, 1.00)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
        
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "LatexDrone", "Neutral") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Ha![P] Knife to meet you![B][C]") ]])
    fnCutscene([[ Append("Drone:[E|Neutral] THIS UNIT IS PLEASED TO MEET YOU AS WELL.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Sexy] ...[P] For crying out loud...[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Stay back![P] I have this conveniently placed knife![B][C]") ]])
    fnCutscene([[ Append("Drone:[E|Neutral] PROCEEDING WITH ARREST...[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Bring it on!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
	fnCutscene([[ AL_SetProperty("Music", "NULL") ]])
    fnCutsceneMoveFace("Sammy", 24.25, 5.50, -1, 0, 2.50)
    fnCutsceneMoveFace("Drone", 23.25, 5.50, 1, 0, 2.50)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneWait(5)
    fnCutscene([[ AudioManager_PlaySound("Combat|Impact_Slash_Crit") ]])
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 1, 1, 1, 1, 1, 1, 1, 1) ]])
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Activate Fade", 125, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneTeleport("Sammy", 24.25, 5.50)
    fnCutsceneFace("Sammy", -1, 0)
    fnCutsceneTeleport("Drone", 26.25, 5.50)
    fnCutsceneFace("Drone", -1, 0)
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    fnCutsceneFace("Sammy", 0, 1)
    fnCutsceneFace("Drone", 0, 1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Sammy", 1, 0)
    fnCutsceneWait(125)
    fnCutsceneBlocker()
        
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "LatexDrone", "Neutral") ]])
    fnCutscene([[ Append("Drone:[E|Neutral] H-[P]HOW?[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] I'm sorry, but you're already dead.[B][C]") ]])
    fnCutscene([[ Append("Drone:[E|Neutral] YOU'LL NEVER ESCAPE OUR EVIL BASE...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(85)
    fnCutsceneBlocker()
    
    --Oh no!
    fnCutsceneWait(6)
    fnCutsceneSetFrame("Drone", "Arm0")
    fnCutsceneBlocker()
    fnCutsceneWait(6)
    fnCutsceneSetFrame("Drone", "Arm1")
    fnCutsceneBlocker()
    fnCutsceneWait(6)
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneSetFrame("Drone", "Arm2")
    fnCutsceneBlocker()
    fnCutsceneWait(6)
    fnCutsceneSetFrame("Drone", "Arm3")
    fnCutsceneBlocker()
    fnCutsceneWait(85)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Drone", "Arm4")
    fnCutsceneWait(85)
    fnCutsceneBlocker()
        
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Almond:[VOICE|Sammy] Uhhhh...[P] Whoops...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --The big board.
    fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "WallsAccidentA", true) ]])
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "WallsAccidentB", false) ]])
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "WallsAccidentHiA", true) ]])
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "WallsAccidentHiB", false) ]])
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("Sammy", 1, -1)
    fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "WallsAccidentB", true) ]])
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "WallsAccidentC", false) ]])
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "WallsAccidentHiB", true) ]])
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "WallsAccidentHiC", false) ]])
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "WallsAccidentC", true) ]])
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "WallsAccidentD", false) ]])
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "WallsAccidentHiC", true) ]])
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "WallsAccidentHiD", false) ]])
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "WallsAccidentD", true) ]])
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "WallsAccidentE", false) ]])
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "WallsAccidentHiD", true) ]])
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "WallsAccidentHiE", false) ]])
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneWait(185)
    fnCutsceneBlocker()
        
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Drone:[VOICE|LatexDrone] MY LORD GOLEM IS GOING TO KILL ME...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Disable collisions.
    fnCutsceneMoveFace("Sammy", 17.25, 5.50, 1, 0, 0.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
        
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Almond:[VOICE|Sammy] (Probably as good a time as any to show myself out...)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Move and sploosh.
    fnCutsceneMoveFace("Sammy", 17.25, 6.50, 0, 1, 1.00)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sammy", 17.25, 8.90, 0, 1, 2.50)
    fnCutsceneBlocker()
    fnCutsceneTeleport("Sammy", -1, -1)
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0)
        CameraEvent_SetProperty("Focus Position", (17.25 * gciSizePerTile), (8.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutscene([[ AudioManager_PlaySound("World|BigSplash") ]])
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Splash0", false) ]])
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Splash0", true) ]])
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Splash1", false) ]])
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Splash1", true) ]])
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Splash2", false) ]])
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Splash2", true) ]])
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Splash3", false) ]])
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Splash3", true) ]])
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    
    --Fade out.
    fnCutsceneWait(45)
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Transition level.
    fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsMovieE", "FORCEPOS:18.0x10.0x0") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
