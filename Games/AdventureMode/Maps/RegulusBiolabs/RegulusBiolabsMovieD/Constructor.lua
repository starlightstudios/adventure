-- |[Constructor]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusBiolabsMovieD"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music. Uses layering.
	AL_SetProperty("Music", "LAYER|SomeMothsDo")
    AL_SetProperty("Mandated Music Intensity", 20.0)
    
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("RegulusBiolabsMovieA")
	
	-- |[Spawn NPCS Here]|
	--NPCs set to spawn here will be available for post-exec cutscenes.

	-- |[Lights]|
    AL_SetProperty("Activate Lights")

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[Spawn NPCs Here]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    
    -- |[Layers]|
    AL_SetProperty("Set Layer Disabled", "NoKnife", true)
    AL_SetProperty("Set Layer Disabled", "WallsAccidentB", true)
    AL_SetProperty("Set Layer Disabled", "WallsAccidentC", true)
    AL_SetProperty("Set Layer Disabled", "WallsAccidentD", true)
    AL_SetProperty("Set Layer Disabled", "WallsAccidentE", true)
    AL_SetProperty("Set Layer Disabled", "WallsAccidentHiB", true)
    AL_SetProperty("Set Layer Disabled", "WallsAccidentHiC", true)
    AL_SetProperty("Set Layer Disabled", "WallsAccidentHiD", true)
    AL_SetProperty("Set Layer Disabled", "WallsAccidentHiE", true)
    AL_SetProperty("Set Layer Disabled", "Splash0", true)
    AL_SetProperty("Set Layer Disabled", "Splash1", true)
    AL_SetProperty("Set Layer Disabled", "Splash2", true)
    AL_SetProperty("Set Layer Disabled", "Splash3", true)
    
end
