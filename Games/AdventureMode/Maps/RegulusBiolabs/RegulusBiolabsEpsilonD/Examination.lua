-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToEpsilonC") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsEpsilonC", "FORCEPOS:48.5x30.0x0")
    
elseif(sObjectName == "ToEpsilonE") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsEpsilonE", "FORCEPOS:29.5x66.0x0")
    
-- |[Objects]|
elseif(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('My attempts to create an alternative personality for the substrate have all failed.[P] I believe the error may be in the method I am using for construction.[P] It seems that some range of similarity in the personality algorithm causes subsumption into the broader personality.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('In order to create a new personality, it must be so different from an existing personality.[P] This poses a problem, as the electromagnetic methods merely create copies.[P] I will think on this further.')") ]])
    
elseif(sObjectName == "TerminalB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I believe that the ability to modify one's algorithm, with or without a device, should have a special name.[P] Where is my assistant?[P] She is always so good at coming up with those.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Hyperaware?[P] Patternmind?[P] Autotransmogrifying?[P] I need to think about this, and find a way to classify individuals by whether or not they possess this property.')") ]])
    
elseif(sObjectName == "TerminalC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('It seems I was mistaken.[P] I do not possess the attributes for true self-awareness that is suggested by my own theories.[P] I have failed myself.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I would estimate that approximately one per two-billion algorithms would possess this characteristic innately.[P] It may also be learned, though lacking the knowledge of exactly what I could do to learn it, I am grasping at straws.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Finding an individual who affects reality, modifying their own algorithm to suit their needs, should be my next objective.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('How long have I been in the Epsilon labs?[P] When did I last see my own assistant?[P] I've been so caught up in here that my chronometer is reporting complete nonsense.[P] It hasn't been two-thousand years!')") ]])
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
