-- |[PuzzleFile]|
--Contains the puzzle descriptions based on which answer rolled correct.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
local iAmphibianCorrectSpecies = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianCorrectSpecies", "N")

-- |[Axolotl]|
if(iAmphibianCorrectSpecies == 1.0) then
    
    --Terminal B
    if(sObjectName == "TerminalB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('I'm pretty sure that unit 604223 is someone who works here's tandem unit.[P] She's always peppering me with questions about salamanders.[P] I just refill the oil machine, geez!')") ]])
    
    --Terminal C
    elseif(sObjectName == "TerminalC") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('A visiting unit asked me about limb regeneration.[P] I said we were researching it, and that all our results would be published.[P] She's so curious, if she weren't a Slave Unit I'd have requisitioned her for my research staff!')") ]])
    
    --Terminal D
    elseif(sObjectName == "TerminalD") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('External gills? I don't know what those are.[P] My Lord Golem just has me mop the floors when there are spills, I don't know anything!')") ]])
    end

-- |[Olm]|
elseif(iAmphibianCorrectSpecies == 2.0) then
    
    --Terminal B
    if(sObjectName == "TerminalB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('I'm pretty sure that unit 604223 is someone who works here's tandem unit.[P] She's always peppering me with questions about salamanders.[P] I just refill the oil machine, geez!')") ]])
    
    --Terminal C
    elseif(sObjectName == "TerminalC") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('A visiting unit asked me about cave-dwelling species.[P] I said we breed them in artificial habitats for research, and that all our results would be published.[P] She's so curious, if she weren't a Slave Unit I'd have requisitioned her for my research staff!')") ]])
    
    --Terminal D
    elseif(sObjectName == "TerminalD") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('So danger noodles aren't just snakes, but can be salamanders too?[P] That's nice.[P] I'm just going to mop the floor, Unit 604223.')") ]])
    end

-- |[Blackbelly Salamander]|
elseif(iAmphibianCorrectSpecies == 3.0) then
    
    --Terminal B
    if(sObjectName == "TerminalB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('I'm pretty sure that unit 604223 is someone who works here's tandem unit.[P] She's always peppering me with questions about salamanders.[P] I just refill the oil machine, geez!')") ]])
    
    --Terminal C
    elseif(sObjectName == "TerminalC") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('A visiting unit asked me about land-foraging salamanders.[P] Despite being amphibians, the ones she was talking about only travel a few centimeters from their streams at a time, and are thus almost entirely aquatic.[P] I commended her curiosity -[P] rare in a Slave Unit!')") ]])
    
    --Terminal D
    elseif(sObjectName == "TerminalD") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('I don't handle the eggs, I just clean the floors.[P] Stop asking me about them, 603223![P] You can't take one home, where would you get the water to incubate them in?')") ]])
    end

-- |[Red-Bellied Newt]|
elseif(iAmphibianCorrectSpecies == 4.0) then
    
    --Terminal B
    if(sObjectName == "TerminalB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('I'm pretty sure that unit 604223 is someone who works here's tandem unit.[P] She's always peppering me with questions about salamanders.[P] I just refill the oil machine, geez!')") ]])
    
    --Terminal C
    elseif(sObjectName == "TerminalC") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('A visiting unit asked me about toxic salamanders.[P] I told her that bright colors are a sure indicator of poison or toxicity -[P] it tells predators to stay away.[P] An effective defense mechanism!')") ]])
    
    --Terminal D
    elseif(sObjectName == "TerminalD") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('Ugh, I hate cleaning up after one of 603223's visits.[P] Those things are toxic, you can't just rinse your hands off![P] Use the special sink, or I have to purge the entire pipe system!')") ]])
    end

-- |[Bufo bufo]|
elseif(iAmphibianCorrectSpecies == 10.0) then
    
    --Terminal B
    if(sObjectName == "TerminalB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('I'm pretty sure that unit 604223 is someone who works here's tandem unit.[P] She's always peppering me with questions about frogs and toads.[P] I just refill the oil machine, geez!')") ]])
    
    --Terminal C
    elseif(sObjectName == "TerminalC") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('A visiting unit asked me about toxic amphibians.[P] I told her that while bright colours usually tell a predator to stay away, some species are toxic but dark colored.[P] She seemed oddly delighted with that information.')") ]])
    
    --Terminal D
    elseif(sObjectName == "TerminalD") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('Ugh, I hate cleaning up after one of 603223's visits.[P] Those things are toxic, you can't just rinse your hands off![P] Use the special sink, or I have to purge the entire pipe system!')") ]])
    end

-- |[Lemur frog]|
elseif(iAmphibianCorrectSpecies == 11.0) then
    
    --Terminal B
    if(sObjectName == "TerminalB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('I'm pretty sure that unit 604223 is someone who works here's tandem unit.[P] She's always peppering me with questions about frogs and toads.[P] I just refill the oil machine, geez!')") ]])
    
    --Terminal C
    elseif(sObjectName == "TerminalC") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('A visiting unit asked me about colour-changing species.[P] We have some in the habitat![P] In fact, some change colour based on the time of day, to better hunt at night.[P] She seemed delighted to learn that.')") ]])
    
    --Terminal D
    elseif(sObjectName == "TerminalD") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('Ugh, I hate cleaning up after one of 603223's visits.[P] She loves handling the specimens, and will turn over leaves to find her favourites.[P] It's so annoying -[P] don't damage the plant substrate!')") ]])
    end

-- |[Spadefoot Toad]|
elseif(iAmphibianCorrectSpecies == 12.0) then
    
    --Terminal B
    if(sObjectName == "TerminalB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('I'm pretty sure that unit 604223 is someone who works here's tandem unit.[P] She's always peppering me with questions about frogs and toads.[P] I just refill the oil machine, geez!')") ]])
    
    --Terminal C
    elseif(sObjectName == "TerminalC") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('A visiting unit asked me about cannibalism in amphibians.[P] I told her that only a few species do that, and even then only when under food stress.[P] In our experiments, they cannibalize when in response to drying waters to allow faster maturation.[P] She was quite enthused to learn that.')") ]])
    
    --Terminal D
    elseif(sObjectName == "TerminalD") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('Ugh, I hate cleaning up after one of 603223's visits.[P] She loves handling the specimens, and is poking at the soil to spot the camouflaged ones.[P] Don't damage the substrate!')") ]])
    end

-- |[Moor Frog]|
elseif(iAmphibianCorrectSpecies == 13.0) then
    
    --Terminal B
    if(sObjectName == "TerminalB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('I'm pretty sure that unit 604223 is someone who works here's tandem unit.[P] She's always peppering me with questions about frogs and toads.[P] I just refill the oil machine, geez!')") ]])
    
    --Terminal C
    elseif(sObjectName == "TerminalC") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('A visiting unit asked me about mating in amphibians.[P] The species she was interested in likes to form choruses to attract mates.[P] Some other species do that, but none of the ones in our public habitats.')") ]])
    
    --Terminal D
    elseif(sObjectName == "TerminalD") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('Ugh, I hate cleaning up after one of 603223's visits.[P] She loves handling the specimens, and loves to rub their unspotted bellies.[P] Weird!')") ]])
    end

-- |[Banded Bullfrog]|
elseif(iAmphibianCorrectSpecies == 14.0) then
    
    --Terminal B
    if(sObjectName == "TerminalB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('I'm pretty sure that unit 604223 is someone who works here's tandem unit.[P] She's always peppering me with questions about frogs and toads.[P] I just refill the oil machine, geez!')") ]])
    
    --Terminal C
    elseif(sObjectName == "TerminalC") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('A visiting unit asked me about eating habits in amphibians.[P] The ones she was talking about require entire insect colonies to maintain -[P] voracious eaters!')") ]])
    
    --Terminal D
    elseif(sObjectName == "TerminalD") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('Ugh, I hate cleaning up after one of 603223's visits.[P] She likes to buy feed with her work credits and give it to the specimens -[P] and they devour every bit.')") ]])
    end

-- |[Iwokramae Caecilia]|
elseif(iAmphibianCorrectSpecies == 20.0) then
    
    --Terminal B
    if(sObjectName == "TerminalB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('I'm pretty sure that unit 604223 is someone who works here's tandem unit.[P] She's always peppering me with questions about burrowing amphibians.[P] I just refill the oil machine, geez!')") ]])
    
    --Terminal C
    elseif(sObjectName == "TerminalC") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('A visiting unit asked me about how amphibians breathe.[P] I pointed out a species we have here that has no lungs, and breathes entirely through its skin.[P] She was positively delighted!')") ]])
    
    --Terminal D
    elseif(sObjectName == "TerminalD") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('Ugh, I hate cleaning up after one of 603223's visits.[P] She digs into the soil to see the burrowing amphibians and I have to clean up the mess.')") ]])
    end

-- |[Annalatus Caecilia]|
elseif(iAmphibianCorrectSpecies == 21.0) then
    
    --Terminal B
    if(sObjectName == "TerminalB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('I'm pretty sure that unit 604223 is someone who works here's tandem unit.[P] She's always peppering me with questions about burrowing amphibians.[P] I just refill the oil machine, geez!')") ]])
    
    --Terminal C
    elseif(sObjectName == "TerminalC") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('A visiting unit asked me about toxic amphibians.[P] I pointed her to one of the caecilians we have here, that secretes toxic mucous from a gland in its rear.[P] She asked so many questions I could hardly keep up!')") ]])
    
    --Terminal D
    elseif(sObjectName == "TerminalD") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('Ugh, I hate cleaning up after one of 603223's visits.[P] She digs into the soil to see the burrowing amphibians and I have to clean up the mess.')") ]])
    end

-- |[Rubber Eel]|
elseif(iAmphibianCorrectSpecies == 22.0) then
    
    --Terminal B
    if(sObjectName == "TerminalB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('I'm pretty sure that unit 604223 is someone who works here's tandem unit.[P] She's always peppering me with questions about aquatic legless amphibians.[P] I just refill the oil machine, geez!')") ]])
    
    --Terminal C
    elseif(sObjectName == "TerminalC") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('A visiting unit asked me about aquatic amphibians.[P] She thought those were eels, but in fact those are caecilians that are aquatic.[P] She watched them for at least an hour without moving.')") ]])
    
    --Terminal D
    elseif(sObjectName == "TerminalD") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('Ugh, I hate cleaning up after one of 603223's visits.[P] She puts her hands on the glass and I have to wipe them off, every single time.')") ]])
    end
    
end
