-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToAmphibianC") then
    
    --Variables.
    local iAmphibianGotAuthcode = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianGotAuthcode", "N")
    if(iAmphibianGotAuthcode == 1.0) then
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_BeginTransitionTo("RegulusBiolabsAmphibianC", "FORCEPOS:57.5x26.0x0")
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] [SOUND|World|AutoDoorFail](This is a restricted area and someone locked it down before they left.[P] We'll need access codes to get in.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "ToAmphibianALft") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsAmphibianA", "FORCEPOS:5.5x6.0x0")
    
elseif(sObjectName == "ToAmphibianARgt") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsAmphibianA", "FORCEPOS:12.0x4.0x0")

-- |[Secret Door]|
--Must be opened before it does anything.
elseif(sObjectName == "SecretDoor") then
    local iAmphibianUnlockSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianUnlockSequence", "N")
    if(iAmphibianUnlockSequence >= 5.0) then
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_BeginTransitionTo("RegulusBiolabsAmphibianE", "FORCEPOS:26.5x6.0x0")
    end
    
-- |[Habitats]|
elseif(sObjectName == "FrogHabitat") then
    local iAmphibianCorrectSpecies = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianCorrectSpecies", "N")
    if(iAmphibianCorrectSpecies >= 10.0 and iAmphibianCorrectSpecies <= 14.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (This is a frog habitat.[P] Looks like some golem left marks on the glass.)") ]])
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (This is a frog habitat.[P] Nothing unusual here.)") ]])
    end

elseif(sObjectName == "SalamanderHabitat") then
    local iAmphibianCorrectSpecies = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianCorrectSpecies", "N")
    if(iAmphibianCorrectSpecies >= 1.0 and iAmphibianCorrectSpecies <= 4.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (This is a salamander habitat.[P] Looks like some golem left marks on the glass.)") ]])
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (This is a salamander habitat.[P] Nothing unusual here.)") ]])
    end
    
elseif(sObjectName == "CaecilianHabitat") then
    local iAmphibianCorrectSpecies = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianCorrectSpecies", "N")
    if(iAmphibianCorrectSpecies >= 20.0 and iAmphibianCorrectSpecies <= 22.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (This is a caecilian habitat.[P] Looks like some golem left marks on the glass.)") ]])
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (This is a caecilian habitat.[P] Nothing unusual here.)") ]])
    end

-- |[Objects]|
elseif(sObjectName == "SignA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Please do not touch the glass.[P] It leaves dust traces that our units must clean off.')") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "SignB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('If you would like to handle a specimen, inform the staff.[P] Each species has special handling procedures.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Remember to decontaminate your chassis![P] Some species are poisonous and may harm organics elsewhere in the Biolabs.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('There is no need to feed the specimens -[P] we keep them very well fed!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Tell your friends to visit![P] Amphibian Research loves visitors!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('You know that golem who comes to visit every few days?[P] I wonder where she gets the work credits.[P] I keep telling her not to touch the glass![P] She leaves marks all the time!')") ]])
    
elseif(sObjectName == "TerminalB") then
    LM_ExecuteScript(fnResolvePath() .. "Puzzlefile.lua", sObjectName)
    
elseif(sObjectName == "TerminalC") then
    LM_ExecuteScript(fnResolvePath() .. "Puzzlefile.lua", sObjectName)
    
elseif(sObjectName == "TerminalD") then
    LM_ExecuteScript(fnResolvePath() .. "Puzzlefile.lua", sObjectName)
    
elseif(sObjectName == "Computer") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('It's been wonderful having so many Slave Unit guests.[P] Hey, if they can stump the work credits, I don't mind having them here.[P] Science belongs to everyone, not just Lord Units.')") ]])
    
elseif(sObjectName == "Soda") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Amphizzy Soda -[P] A Fizzy Pop! derived brand.')") ]])
    fnCutsceneBlocker()
    
    local iAmphibianUnlockSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianUnlockSequence", "N")
    if(iAmphibianUnlockSequence == 2.0) then
        AudioManager_PlaySound("World|Chirp")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianUnlockSequence", "N", 3.0)
    end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
