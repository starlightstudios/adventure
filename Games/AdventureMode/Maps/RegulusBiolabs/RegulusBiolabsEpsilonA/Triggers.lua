-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "Traps") then
    
    --Variables.
    local iSawTrapDialogue = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawTrapDialogue", "N")
    if(iSawTrapDialogue == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawTrapDialogue", "N", 1.0)
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ Append("Christine:[E|Neutral] 55, what are those on the floor?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Given the nature of the room and the context of the area, I would say these are part of the defenses here.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] The material on the surface is exposed conductive wire, but there is no pressure-activation mechanism.[P] Personnel walk over them on the way to the labs.[P] Therefore, they are likely manually activated.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Electrical discharge defenses?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Yes, but the wires are not receiving power.[P] We should be all right near them.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] They had ample cover, range, traps, and they couldn't stop the attack...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Unfortunate.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] Yeah...") ]])
        fnCutsceneBlocker()
        
    end
    
elseif(sObjectName == "MusicChange") then
    AdvCombat_SetProperty("Default Combat Music", "BattleThemeChristine", 0.0000)

--Level Transition.
elseif(sObjectName == "TransitionToGammaA") then
    AL_BeginTransitionTo("RegulusBiolabsGammaA", "FORCEPOS:45.5x3.0x0")

--Final cutscene before the flashback sequence.
elseif(sObjectName == "Betrayal") then

    --Variables.
    local bIsSX399Present = fnIsCharacterPresent("SX-399")
    local iMet20 = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet20", "N")
    local iBiolabsFinale = VM_GetVar("Root/Variables/Chapter5/Scenes/iBiolabsFinale", "N")
    if(iMet20 >= 1.0 and iBiolabsFinale == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iBiolabsFinale", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N", 60.0)
        
        --Spawn NPCs.
        TA_Create("2856")
            TA_SetProperty("Position", 7, 38)
            TA_SetProperty("Facing", gci_Face_North)
            TA_SetProperty("Clipping Flag", false)
            fnSetCharacterGraphics("Root/Images/Sprites/56/", true)
        DL_PopActiveObject()
        
        --Movement.
        if(bIsSX399Present == false) then
            fnCutsceneMove("Tiffany", 7.75, 16.50)
            fnCutsceneMove("Christine", 7.75, 18.50)
        else
            fnCutsceneMove("Tiffany", 7.75, 16.50)
            fnCutsceneMove("Christine", 7.25, 18.50)
            fnCutsceneMove("SX-399", 8.25, 18.50)
        end
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", 0, -1)
        fnCutsceneFace("Tiffany", 0, 1)
        if(bIsSX399Present == true) then
            fnCutsceneFace("SX-399", 0, -1)
        end
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("55:[E|Neutral] Christine, have you thought about what our next move will be?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Hmm?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Aren't you usually the one who thinks eight steps ahead?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I was wholly unable to predict what would happen in these circumstances.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Considering what we have seen in the Epsilon habitat...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] It's real.[P] I don't know how it works precisely, but it was and is real.[B][C]") ]])
        if(bIsSX399Present == true) then
            fnCutscene([[ Append("SX-399:[E|Neutral] It wasn't like anything I've seen in the mines, that's for sure.[B][C]") ]])
        end
        fnCutscene([[ Append("55:[E|Neutral] Still.[P] Our next objective.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Neutralize Unit 2856.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] 201890's goons seem to have backed off, for the moment, but she might not realize that.[P] We still have the advantage of surprise.[B][C]") ]])
        fnCutscene([[ Append("2856:[VOICE|Tiffany] Once again, you are incorrect.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("Tiffany", 7.75, 17.50)
        fnCutsceneMove("2856", 7.75, 21.50)
        fnCutsceneFace("Christine", 0, 1)
        if(bIsSX399Present == true) then
            fnCutsceneFace("SX-399", 0, 1)
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()

        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Oh, there you are.[P] On the plus side, you've saved us the trouble of hunting you down.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] On the minus side, I have to put up with talking to you again.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] I do so appreciate our repartee.[P] My company is usually Drone Units, who are poor conversationalists.[B][C]") ]])
        if(bIsSX399Present == true) then
            fnCutscene([[ Append("SX-399:[E|Neutral] So can I melt her now?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] No.[B][C]") ]])
        end
        fnCutscene([[ Append("Christine:[E|Offended] Ugh.[P] Much as I hate to say it...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] We'll be taking you alive.[P] Since you neglected to bring your bodyguards, and are unarmed.[P] Please come quietly.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] She will not, I presume.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Punchable] Correct.[P] If you would just take one step forward?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Right onto the electricity trap?[P] Which you have probably repaired and reactivated?[B][C]") ]])
        if(bIsSX399Present == true) then
            fnCutscene([[ Append("SX-399:[E|Neutral] She must think we're idiots.[B][C]") ]])
            fnCutscene([[ Append("2856:[E|Punchable] Have you given me any reason to doubt that so far?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] You're the one who tried to approach three heavily armed combat units with no support and make threats.[B][C]") ]])
        else
            fnCutscene([[ Append("Christine:[E|Offended] Not happening.[B][C]") ]])
        end
        fnCutscene([[ Append("2856:[E|Neutral] Fine.[P] I tire of this.[P] Let us conclude it.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement. Slight variation if SX-399 is present.
        if(bIsSX399Present == false) then
            fnCutsceneMoveFace("Tiffany", 7.75, 16.50, 0, 1, 0.50)
            fnCutsceneBlocker()
            fnCutsceneMoveFace("Tiffany", 7.75, 18.50, 0, 1, 2.00)
            fnCutsceneBlocker()
            fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
            fnCutsceneMoveFace("Christine", 7.75, 19.50, 0, 1, 2.00)
            fnCutsceneBlocker()
        
        --With SX-399:
        else
            fnCutsceneMoveFace("Tiffany", 7.75, 16.50, 0, 1, 0.50)
            fnCutsceneBlocker()
            fnCutsceneMoveFace("Tiffany", 7.75, 18.50, 0, 1, 2.00)
            fnCutsceneBlocker()
            fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
            fnCutsceneMoveFace("Christine", 7.25, 19.50, 0, 1, 2.00)
            fnCutsceneMoveFace("SX-399", 8.25, 19.50, 0, 1, 2.00)
            fnCutsceneBlocker()
        
        end
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Ouch![P] 55, did you - ") ]])
        fnCutsceneBlocker()
        
        --Shocking!
        fnCutsceneFace("Christine", 0, -1)
        if(bIsSX399Present == true) then fnCutsceneFace("SX-399", -1, 0) end
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Electroshock0", false) ]])
        fnCutsceneWait(15)
        fnCutscene([[ AudioManager_PlaySound("World|SparksA") ]])
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", 1, 1)
        if(bIsSX399Present == true) then fnCutsceneFace("SX-399", -1, 0) end
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Electroshock0", true) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Electroshock1", false) ]])
        fnCutsceneWait(17)
        fnCutscene([[ AudioManager_PlaySound("World|SparksB") ]])
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", -1, 0)
        if(bIsSX399Present == true) then fnCutsceneFace("SX-399", 1, -1) end
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Electroshock1", true) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Electroshock2", false) ]])
        fnCutsceneWait(15)
        fnCutscene([[ AudioManager_PlaySound("World|SparksC") ]])
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", 1, -1)
        if(bIsSX399Present == true) then fnCutsceneFace("SX-399", 1, 0) end
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Electroshock2", true) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Electroshock3", false) ]])
        fnCutsceneWait(15)
        fnCutscene([[ AudioManager_PlaySound("World|SparksA") ]])
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", 0, 1)
        if(bIsSX399Present == true) then fnCutsceneFace("SX-399", -1, 0) end
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Electroshock3", true) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Electroshock1", false) ]])
        fnCutsceneWait(15)
        fnCutscene([[ AudioManager_PlaySound("World|SparksA") ]])
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", 1, -1)
        if(bIsSX399Present == true) then fnCutsceneFace("SX-399", 1, 1) end
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Electroshock1", true) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Electroshock2", false) ]])
        fnCutsceneWait(15)
        fnCutscene([[ AudioManager_PlaySound("World|SparksB") ]])
        fnCutsceneBlocker()
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Electroshock2", true) ]])
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMoveFace("Christine", 6.25, 19.50, 1, -1, 0.20)
        if(bIsSX399Present == true) then
            fnCutsceneMoveFace("SX-399", 9.25, 19.50, 1, 1, 0.20)
        end
        fnCutsceneBlocker()
        fnCutsceneSetFrame("Christine", "Wounded")
        if(bIsSX399Present == true) then fnCutsceneSetFrame("SX-399", "Wounded") end
        fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        if(bIsSX399Present == false) then
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Sad") ]])
        else
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Sad") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Sad") ]])
        end
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "2856", "Neutral") ]])
        fnCutscene([[ Append("Christine:[E|Sad] Ahh...[P] I can't move...[B][C]") ]])
        if(bIsSX399Present == true) then
            fnCutscene([[ Append("Christine:[E|Sad] SX?[P] Can you...[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Sad] All my systems are...[B][C]") ]])
        end
        fnCutscene([[ Append("55:[E|Neutral] The last-minute calibrations were my doing.[P] I had presumed you would be in your Lord Golem form, and had to reduce the voltage accordingly.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] If I had wanted to terminate you, I would have.[P] So, be quiet and speak only when spoken to.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] But...[P] you...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] As expected, the properties of her golem body seem to be merging with her human body.[P] She absorbed electrical currents that would lead to total paralysis in a normal human.[B][C]") ]])
        fnCutscene([[ Append("56:[E|Neutral] Is it possible you miscalculated the voltage?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] It is always a possibility, but a remote one.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] 55...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Hm.[P] I think she is upset.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Are you upset that I have shocked you?[P] I recall the last time I did this yielded a similar result.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] T-[P]traitor...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Oh.[P] I see.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Do you think that word will hurt me?[P] Is this the last, desperate attempt to regain control over the situation?[B][C]") ]])
        fnCutscene([[ Append("56:[E|Neutral] Pathetic.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Agreed.[B][C]") ]])
        if(bIsSX399Present == true) then
            fnCutscene([[ Append("SX-399:[E|Sad] How...[P] could you...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] Simple.[P] I am a perfect machine.[P] I do not feel the guilt you are convinced I was feeling.[B][C]") ]])
        end
        fnCutscene([[ Append("Christine:[E|Sad] But...[P] you were...[P] making such good progress...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] 55...[P] you were my best friend...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] The 'progress' you believed I had been making was an act.[B][C]") ]])
        fnCutscene([[ Append("56:[E|Neutral] I contacted her on your PDU, Christine.[P] I gave her a simple offer.[B][C]") ]])
        fnCutscene([[ Append("56:[E|Neutral] Bring down Vivify, and then give me Christine, and I would see to it she is reinstated as Head of Security.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] A full pardon.[P] After all, I had merely been acting according to my own interests without my memories.[P] That is forgivable, given the circumstances.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] With my memories lost, I moved to recover them.[P] I have reasserted my understanding of the situation, and made the correct choice.[B][C]") ]])
        fnCutscene([[ Append("56:[E|Neutral] While the Administrator will likely be unhappy with me and my actions, I am prepared to face the consequences.[P] My sister, however, is innocent.[P] The Administrator will see the logic of the situation.[B][C]") ]])
        fnCutscene([[ Append("56:[E|Neutral] With Project Vivify neutralized, we can now deal with the rebellion you have instigated.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] But...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] There is no but.[P] You have lost, Christine.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] But it will not be the end for you.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] S-[P]Sophie...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Your tandem unit.[P] I will leave her fate up to you.[B][C]") ]])
        fnCutscene([[ Append("56:[E|Neutral] Oh?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] I am a machine, and will take no enjoyment whatsoever in watching you mete out just punishment for her rebellious behavior.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] I -[P] can't -[P] WHAT -[P] are you...[B][C]") ]])
        fnCutscene([[ Append("56:[E|Neutral] Oh, it's all right to show a little emotion, sister.[P] You're clearly enjoying it, the same way I do.[B][C]") ]])
        fnCutscene([[ Append("56:[E|Neutral] Just don't let anyone else know you love it.[P] But it's fine if it's between us.[B][C]") ]])
        fnCutscene([[ Append("56:[E|Neutral] Sister.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Sister.[B][C]") ]])
        if(bIsSX399Present == true) then
            fnCutscene([[ Append("SX-399:[E|Sad] She would...[P] never help you...[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Sad] To think...[P] I loved you...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] Yes, she will.[B][C]") ]])
        else
            fnCutscene([[ Append("Christine:[E|Sad] I won't...[P] help you...[P] *cough*[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] Yes, you will.[B][C]") ]])
        end
        fnCutscene([[ Append("55:[E|Smirk] My sister informs me that the Loyalty Program on new Command Units is extremely potent.[P] Look at what it has done to us.[B][C]") ]])
        fnCutscene([[ Append("56:[E|Neutral] Perfect, unflinching loyalty.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] As such, you shall become a new Command Unit, Christine.[P] This will make the process of experimenting on you and your runestone much more efficient.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Further, your infiltration and combat capabilities, when properly programmed, will be excellent.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] All that is necessary is removing that pesky independent streak you possess, and you will be -[P] perhaps even a better unit than I.[B][C]") ]])
        if(bIsSX399Present == true) then
            fnCutscene([[ Append("Christine:[E|Sad] I'd die first...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] Let SX-399 go...[P] This is between us...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Nothing to say, SX-399?[P] It seems she has entered system standby.[P] Pity.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] No, Christine, I will not let her go.[P] And no, I will not retire her here.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] There are doubtless many engineering advancements in her chassis.[P] Even if not, there are many novel improvements to be researched.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] She will keep our engineers occupied for at least a few months as we take her apart and analyze each piece.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] ...[P] Bitch...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Your insults do nothing to me.[P] I am a machine.[B][C]") ]])
        end
        fnCutscene([[ Append("Christine:[E|Sad] I...[P] I...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Yes?[B][C]") ]])
        fnCutscene([[ Append("56:[E|Neutral] It seems she has lost consciousness.") ]])
        fnCutsceneBlocker()
        
        --Re-dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "2856", "Neutral") ]])
        fnCutscene([[ Append("55:[E|Neutral] Not a great loss, then.[P] She tends to talk too much.[B][C]") ]])
        fnCutscene([[ Append("56:[E|Neutral] How does it feel to be back?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Same as ever.[P] Empty and mechanical.[B][C]") ]])
        fnCutscene([[ Append("56:[E|Neutral] Not even a hint of regret?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] None.[P] What is our next task?[B][C]") ]])
        fnCutscene([[ Append("56:[E|Neutral] I have sent word ahead.[P] There is a special chamber beneath Sector 5 where the Command Unit conversion facilities are located.[B][C]") ]])
        fnCutscene([[ Append("56:[E|Neutral] It can only be opened by two Command Units, but you are lacking authorization.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I understand.[P] You will have to lead the way, then.[B][C]") ]])
        fnCutscene([[ Append("56:[E|Neutral] We can make our way there through the transit station.[P] My units have cleared enough debris to move on foot.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Good.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] By the way, what if Christine fails to be swayed by the Loyalty Program?[P] What if she resists?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] She has demonstrated impressive resolve thus far.[B][C]") ]])
        fnCutscene([[ Append("56:[E|Neutral] It will be a shame, but she would have to be scrapped.[P] I am not sure myself.[B][C]") ]])
        fnCutscene([[ Append("56:[E|Neutral] The program was written by the Administrator, and is perfect.[P] It has never failed.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Is it possible that it will remove components of her psyche in order to ensure loyalty?[P] What if she is of no use to us afterwards?[B][C]") ]])
        fnCutscene([[ Append("56:[E|Neutral] We can still experiment on her and her runestone.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Ah, yes.[P] I am thinking from a security perspective, while you are using a research perspective.[P] I apologize.[B][C]") ]])
        fnCutscene([[ Append("56:[E|Neutral] Come now.[P] There is a rebellion raging.[B][C]") ]])
        fnCutscene([[ Append("56:[E|Neutral] Won't it be sweet to watch 771852 crush the very rebellion she is the figurehead of?[P] To watch the despair in the eyes of those Slave Units as they realize there is truly no hope?[B][C]") ]])
        fnCutscene([[ Append("56:[E|Neutral] It will be a decade before any revolutionary thoughts even cross their minds![B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] It will be so sweet.[P] But you didn't hear me say that.[P] I am an unfeeling machine.[B][C]") ]])
        fnCutscene([[ Append("56:[E|Neutral] Sister.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] ...[P][P][P][EMOTION|Tiffany|Smirk] Sister.") ]])
        fnCutsceneBlocker()
        
        --Remove 55 and SX-399 from the party.
        AdvCombat_SetProperty("Party Slot", 0, "Christine")
        AdvCombat_SetProperty("Party Slot", 1, "Null")
        AdvCombat_SetProperty("Party Slot", 2, "Null")
        AdvCombat_SetProperty("Party Slot", 3, "Null")
        
        --Remove the followers so it's just Christine.
        AL_SetProperty("Unfollow Actor Name", "Tiffany")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
        if(bIsSX399Present == true) then
            AL_SetProperty("Unfollow Actor Name", "SX-399")
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399IsFollowing", "N", 0.0)
        end
        
        --Wait a bit, fade to black.
        fnCutsceneWait(120)
        fnCutscene([[ AL_SetProperty("Activate Fade", 120, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
        fnCutsceneBlocker()
        
        --Transition to this new level where the player can save in expectation of the next content.
        fnCutscene([[ AL_BeginTransitionTo("RegulusFlashbackA", "FORCEPOS:7.0x6.0x0") ]])
        fnCutsceneBlocker()
    end
end
