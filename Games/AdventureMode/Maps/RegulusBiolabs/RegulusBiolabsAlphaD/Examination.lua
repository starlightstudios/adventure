-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
-- |[Objects]|
if(sObjectName == "ConversionDoor") then
    
    --Variables.
    local iSawConvertedScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawConvertedScene", "N")
    
    --Hasn't seen the scene yet.
    if(iSawConvertedScene == 0.0) then
        
        --Variables.
        local iSX399IsFollowing  = 0
        local iSophieIsFollowing = 0
        local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
        if(fnIsCharacterPresent("SX-399"))  then iSX399IsFollowing  = 1.0 end
        if(fnIsCharacterPresent("Sophie")) then iSophieIsFollowing = 1.0 end
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawConvertedScene", "N", 1.0)
    
        --Cutscene.
        fnCutscene([[ AL_SetProperty("Open Door", "DoorConversion") ]])
        fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Scraprat moves over.
        fnCutsceneMove("Scraprat", 17.25, 20.50)
        fnCutsceneFace("Scraprat", -1, 0)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Scraprat:[VOICE|Narrator] Scraprat helped![P] Scraprat helped![P] Hee hee, hehehehehe!") ]])
        fnCutsceneBlocker()
        
        --Move everyone onto Christine.
        fnCutsceneMergeParty()
        fnCutsceneBlocker()
    
        --Move the party in.
        fnCutsceneMove("Christine", 14.25, 20.50)
        fnCutsceneMove("Christine", 15.25, 20.50)
        fnCutsceneMove("Tiffany", 14.25, 20.50)
        fnCutsceneFace("Tiffany", 1, 0)
        if(iSX399IsFollowing == 1.0) then
            fnCutsceneMove("SX-399", 14.25, 21.50)
            fnCutsceneMove("SX-399", 15.25, 21.50)
            fnCutsceneFace("SX-399", 1, 0)
        end
        if(iSophieIsFollowing == 1.0) then
            fnCutsceneMove("Sophie", 14.25, 21.50)
            fnCutsceneFace("Sophie", 1, 0)
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Helped with what, little guy?[B][C]") ]])
        fnCutscene([[ Append("Scraprat:[VOICE|Narrator] Help help![P] Very good![P] Hehehehe![B][C]") ]])
        fnCutscene([[ Append("55:[VOICE|Tiffany] Their linguistic algorithms are quite lacking.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
        --Layer changes.
        fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Floor1AltA", false) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Walls1AltA", false) ]])
        fnCutsceneFace("Christine", 0, -1)
        fnCutsceneFace("Tiffany", 0, -1)
        if(iSX399IsFollowing == 1.0) then
            fnCutsceneFace("SX-399", 0, -1)
        end
        if(iSophieIsFollowing == 1.0) then
            fnCutsceneFace("Sophie", 0, -1)
        end
        fnCutsceneBlocker()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Tube:[VOICE|Golem] Conversion completed.[P] Disengaging...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Walls1AltB", false) ]])
        fnCutsceneBlocker()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Spatial estimation matrix online.[P] Physical calibrations complete.[P] Setting logical tables...[P] done.[P] Secondary boot complete.[P] Exiting conversion chamber.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        
        --SFX.
        fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutsceneTeleport("Converted", 15.25, 19.50)
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Floor1AltA", true) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Walls1AltA", true) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Walls1AltB", true) ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "PDU", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Golem", "Neutral") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] Superior units located, disengaging core overrides...[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] I -[P] I...[P] What happened?[P] Did I...[P] oh...[P] I'm a robot now...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] Was that not the expected outcome of stepping into a conversion chamber?[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] I didn't step into a -[P] oh.[P] I guess I did?[P] I don't remember.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] What do you remember?[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] Oops, um.[P] Command Unit...[P] I am reporting for function assignment.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] Your function assignment is to tell me what happened to you, without delay.[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] Y-[P]yes, Command Unit![B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] I heard the evacuation order over the network, and I started heading to the ranch as ordered.[P] But then I went into the transfer tube to the Gamma Labs and...[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] I'm not sure.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Impressive.[P] I didn't remember much when I was converted.[B][C]") ]])
        if(sChristineForm ~= "Golem" and sChristineForm ~= "Latex") then
            fnCutscene([[ Append("Golem:[E|Neutral] You were...[P] converted?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] It's a long story.[P] Continue, Unit.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Affirmative, Command Unit.[B][C]") ]])
        end
        fnCutscene([[ Append("Golem:[E|Neutral] I am Unit 732115, of the Breeding Program.[P] I guess I won't be attending the graduation ceremony...[B][C]") ]])
        if(iSophieIsFollowing == 1.0) then
            fnCutscene([[ Append("Sophie:[E|Neutral] Breeding Program humans are already trained on proper conversion, manufacturing, and inspection procedures.[P] Reprogramming is fairly light for them.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] There's no reason to rewrite their memories, is there?[P] They're practically already golems.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Unit...[P] Uh, woah, I can read authenticator chips now...[P] Unit 499323 is right.[P] But, I don't remember what happened right before I was converted...[B][C]") ]])
        else
            fnCutscene([[ Append("55:[E|Neutral] Breeding Program humans are already trained on proper conversion, manufacturing, and inspection procedures.[P] Reprogramming is fairly light for them.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Only a few routines need to be updated.[P] They tend to remember the majority of their organic lives.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Unit...[P] Uh, woah, I can read authenticator chips now...[P] Command Unit 2855 is right.[P] But, I don't remember what happened right before I was converted...[B][C]") ]])
        end
    
        if(iSX399IsFollowing == 1.0) then
            fnCutscene([[ Append("SX-399:[E|Neutral] The scraprat seems to.[P] He's been giggling this whole time.[B][C]") ]])
            fnCutscene([[ Append("Scraprat: Search and rescue![P] Scraprat did good![P] Hee hee hee![B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] This is probably the first time I've seen them do something useful...[B][C]") ]])
        else
            fnCutscene([[ Append("55:[E|Neutral] Scraprat, report.[B][C]") ]])
            fnCutscene([[ Append("Scraprat: Search and rescue![P] Scraprat did good![P] Hee hee hee![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Search and rescue?[P] You mean you're useful for something other than self-destructing and banging your head on fabricator benches?[B][C]") ]])
        end
        fnCutscene([[ Append("PDU:[VOICE|Narrator][EMOTION|Christine|PDU] If I may, Christine.[P] In the Biolabs, unexpected depressurization of an area triggers a search-and-rescue response by local security units.[P] Any humans found in the area are recovered.[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] Oh no, I must have blacked out when the tube depressurized.[B][C]") ]])
        fnCutscene([[ Append("PDU:[VOICE|Narrator] If an organic subject is severely damaged, rescue units are authorized to convert them.[P] That is likely what happened here.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Hmm, I remember that a human can survive for a few minutes in a vacuum, though severe brain damage can result.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Happy] So, welcome to the ranks of the mechanical![P] It's just great to have you![B][C]") ]])
        if(sChristineForm ~= "Golem" and sChristineForm ~= "Latex" and sChristineForm ~= "SteamDroid") then
            fnCutscene([[ Append("Golem:[E|Neutral] Uhhh...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] As I said, long story.[B][C]") ]])
        end
        fnCutscene([[ Append("Christine:[E|Smirk] I suppose we'll have to figure out why the tube depressurized.[P] 55, is this area safe?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] No safer than any other area of the Biolabs.[P] Unit 732115, your assignment is to report to the Raiju Ranch as soon as the passage is safe.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] The area is filled with hostiles.[P] If necessary, it may be best to move over the surface.[P] You are immune to the effects of vacuum now.[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] Affirmative, Command Unit.[P] I -[P] I guess I'll remain here and make a run for it when I can.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] We're trying to get the security situation under control.[P] If you see something unexpected, run.[P] Do not engage.[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] Of course...[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] I was really hoping I'd make Lord Golem after I graduated...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Move to Christine, fold party.
        fnCutsceneMove("Tiffany", 15.25, 20.50)
        if(iSX399IsFollowing == 1.0) then
            fnCutsceneMove("SX-399", 15.25, 20.50)
        end
        if(iSophieLeftInBiolabs == 0.0) then
            fnCutsceneMove("Sophie", 15.25, 20.50)
        end
        fnCutsceneBlocker()
        fnCutscene([[ AL_SetProperty("Fold Party") ]])
        fnCutsceneBlocker()
    
    --Just open the door.
    else
        AL_SetProperty("Open Door", "DoorConversion")
        AudioManager_PlaySound("World|AutoDoorOpen")
    
    end

--Airlock.
elseif(sObjectName == "AirlockS") then

    --SFX, open door.
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_SetProperty("Open Door", "DoorS")
    AL_SetProperty("Close Door", "DoorN")

elseif(sObjectName == "AirlockN") then

    --If Christine is an organic, we need a warning.
    local sChristineForm  = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    local iKnowsTubeHoles = VM_GetVar("Root/Variables/Chapter5/Scenes/iKnowsTubeHoles", "N")
    if(sChristineForm == "Human" or sChristineForm == "Raiju") then
    
        --Doesn't know about the holes.
        if(iKnowsTubeHoles == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iKnowsTubeHoles", "N", 1.0)

            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutscene([[ Append("Door:[VOICE|Narrator] Warning:: Depressurized area ahead.[P] Secure organic subjects for transport before exiting.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Organ- [EMOTION|Christine|Neutral]oh, right.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|PDU] PDU?[B][C]") ]])
            fnCutscene([[ Append("PDU:[VOICE|Narrator] Accessing local network...[P] error... [B][C]") ]])
            fnCutscene([[ Append("Christine:[E|PDU] Try to get the airlocks's opcodes.[P] I can figure it out from there.[B][C]") ]])
            fnCutscene([[ Append("PDU:[VOICE|Narrator] Success![P] Displaying...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|PDU] Seems the passage was depressurized about ten minutes after we left the gala, give or take.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] The timing is too convenient.[P] She's been through here...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Vivify?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] No...[P] Her disciple.[P] Vivify doesn't saw holes in anything.[P] She smashes.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I agree with that assessment.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I can change forms and we can come back this way.[P] We won't be going anywhere if I'm squishy like this.") ]])

        --Repeats.
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutscene([[ Append("Door:[VOICE|Narrator] Warning:: Depressurized area ahead.[P] Secure organic subjects for transport before exiting.[B][C]") ]])
            
            if(fnIsCharacterPresent("Sophie") == true) then
                fnCutscene([[ Append("Christine:[E|Offended] Better transform before going this way...[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Happy] What if I kissed you and pinched your nose shut?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] And shut my eyes, and formed a seal over my ears?[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Blush] A-[P]and your...[P] uhh...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Sounds absurdly risky for little gain.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Smirk] Y-yeah...[P] So let's, uh, not do that, right?[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Smirk] As if I need an excuse to kiss you...[B][C]") ]])
                fnCutscene([[ Append("55:[E|Upset] Risking 771852's life for cheap sexual thrill is wildly inefficient.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Upset] She's far more useful absorbing damage or distracting an enemy gunner.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Smirk] Things I guess she can't do while kissing me...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Yeah, so, I'll transform before we go this way.") ]])
            
            --Sophie isn't here:
            else
                fnCutscene([[ Append("Christine:[E|Offended] (Better change forms before stepping into hard vacuum.[P] I don't have much luck breathing vacuum.)") ]])
            end
        end

    --Open it.
    else
    
        --Doesn't know about the holes.
        if(iKnowsTubeHoles == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iKnowsTubeHoles", "N", 1.0)

            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutscene([[ Append("Door:[VOICE|Narrator] Warning:: Depressurized area ahead.[P] Exercise caution.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Depressurized? PDU, check the opcodes on the door, please.[B][C]") ]])
            fnCutscene([[ Append("PDU:[VOICE|Narrator] [EMOTION|Christine|PDU]Downloaded local opcodes. Displaying...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|PDU] Seems the passage was depressurized about ten minutes after we left the gala, give or take.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] The timing is too convenient.[P] She's been through here...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Vivify?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] No...[P] Her disciple.[P] Vivify doesn't saw holes in anything.[P] She smashes.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I agree with that assessment.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Stay alert, everyone...") ]])
            
        end
            
        fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutscene([[ AL_SetProperty("Open Door", "DoorN") ]])
        fnCutscene([[ AL_SetProperty("Close Door", "DoorS") ]])
    end

--Door.
elseif(sObjectName == "ToGammaC") then

    --Variables.
    local iRepoweredGamma          = VM_GetVar("Root/Variables/Chapter5/Scenes/iRepoweredGamma", "N")
    local iSawGammaAirlockSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawGammaAirlockSequence", "N")
    local i2856YelledAboutWestward = VM_GetVar("Root/Variables/Chapter5/Scenes/i2856YelledAboutWestward", "N")
    local iHasElectrospriteForm    = VM_GetVar("Root/Variables/Global/Christine/iHasElectrospriteForm", "N")
    local sChristineForm           = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

    --Door is not repowered.
    if(iRepoweredGamma == 0.0) then

        --First time.
        if(iSawGammaAirlockSequence == 0.0) then

            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSawGammaAirlockSequence", "N", 1.0)

            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutscene([[ Append("Christine:[E|Neutral] ^Hmmm...[P] the door's deadbolts were thrown...^[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|PDU] ^PDU, can you unset the bolts?^[B][C]") ]])
            fnCutscene([[ Append("PDU:[VOICE|Narrator] ^Negative, Christine.[P] The door is receiving power intermittently, and cannot build capacity to move the bolts.^[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|PDU] ^Huh?[P] What do you mean, intermittently?^[B][C]") ]])
            fnCutscene([[ Append("PDU:[VOICE|Narrator] ^It appears the load on the power circuit for the Gamma Labs is too high and the distribution computer cannot compensate.^[B][C]") ]])
            fnCutscene([[ Append("PDU:[VOICE|Narrator] ^Shall I contact a repair crew?^[B][C]") ]])
            if(fnIsCharacterPresent("Sophie") == true) then
                fnCutscene([[ Append("Sophie:[E|Neutral] ^Uh, hi.[P] Repair crew, reporting.[P] What do we need to do?^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] ^These doors have been sabotaged, Sophie.[P] They bolted the door, depressurized the connection tube...^[B][C]") ]])
            else
                fnCutscene([[ Append("Christine:[E|PDU] ^PDU, we [P]*are*[P] the repair crew.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] ^This is obviously an act of sabotage.[P] Bolt the door, depressurize the connection tube...^[B][C]") ]])
            end
            fnCutscene([[ Append("Christine:[E|PDU] ^2856, come in please.[P] We've got a problem.^[B][C]") ]])
            
            --Variation:
            if(i2856YelledAboutWestward == 1.0) then
                fnCutscene([[ Append("2856:[VOICE|Tiffany] ^YOU STUPID UNIT YOU'VE GOT SOME GALL!^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|PDU] ^The door to the Gamma Laboratories is unusable.[P] We have to find another way around.^[B][C]") ]])
                fnCutscene([[ Append("2856:[VOICE|Tiffany] ^WHAT?^[B][C]") ]])
                fnCutscene([[ Append("2856:[VOICE|Tiffany] ^ARE YOU TELLING ME YOU WENT THE WRONG WAY ON PURPOSE AND THEN DOUBLED BACK JUST TO ANGER ME?^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Laugh] (Lie to her lie to her lie to her lie to her)[P][CLEAR]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] ^Yes that is exactly what we did.[P] You seem surprised that I'd do it on purpose.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] ^If I were a betting woman, I'd bet I don't like you.^[B][C]") ]])
                if(fnIsCharacterPresent("SX-399") == true) then
                    fnCutscene([[ Append("SX-399:[E|Flirt] ^Gee, I wonder what the odds would be on that...^[B][C]") ]])
                end
                fnCutscene([[ Append("2856:[VOICE|Tiffany] [EMOTION|Christine|PDU]^YOU UNGRATEFUL AGGRAVATINGLY STUPID INSOLENT LITTLE - ^[P][CLEAR]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^The deadbolts are down and there's not enough power to raise them.[P] I don't have the tools needed to safely remove the airlock door.^[B][C]") ]])
                fnCutscene([[ Append("2856:[VOICE|Tiffany] [EMOTION|Christine|PDU]^Grrrr...^[B][C]") ]])
                fnCutscene([[ Append("2856:[VOICE|Tiffany] ^My sister has leftover blasting charges from her failed Gala Operation.[P] Use those.^[B][C]") ]])
            
            --Didn't go west first:
            else
                fnCutscene([[ Append("2856:[VOICE|Tiffany] ^Yes?[P] Are you at the Gamma Labs yet?^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] ^The door is unusable.[P] We'll find another entrance.^[B][C]") ]])
                fnCutscene([[ Append("2856:[VOICE|Tiffany] ^No, you'll go that way, it's much shorter.^[B][C]") ]])
                fnCutscene([[ Append("2856:[VOICE|Tiffany] ^You have leftover blasting charges, sister.[P] Use them.^[B][C]") ]])
            end
            
            if(fnIsCharacterPresent("SX-399") == true) then
                fnCutscene([[ Append("SX-399:[E|Sad] ^Are you kidding me?^[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Sad] ^You blow the airlock into this corridor, and that'll suck all the air right out of the labs!^[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Sad] ^Everything in there will die!^[B][C]") ]])
                fnCutscene([[ Append("2856:[VOICE|Tiffany] ^Very observant.[P] Plant the charges.^[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Angry] ^Don't you dare!^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Upset] ^I have no intention of using a blasting charge in this manner.^[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Blush] ^Uh, oops, my bad.[P] Sorry, cutie.[P] You and your sister's radio signatures are identical.[P] I got you mixed up.^[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Angry] ^And apparently, only one of you has a heart!^[B][C]") ]])
                fnCutscene([[ Append("2856:[VOICE|Tiffany] ^Stopping Project Vivify takes priority over everything else.[P] Organic life in the Gamma Labs can be replaced.[P] SO STOP MEWLING AND BLOW THE DOOR.^[B][C]") ]])
            else
                fnCutscene([[ Append("Christine:[E|Offended] ^Are you serious right now?^[B][C]") ]])
                fnCutscene([[ Append("2856:[VOICE|Tiffany] ^I DO NOT HAVE TIME FOR JOKES, MUCH LESS YOUR IDIOTIC WHINING.[P] BLOW THE DOOR.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] ^Absolutely not!^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Upset] ^We refuse.[P] Detonating a charge has a higher than thirty percent change of destroying the inner airlock door.^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Upset] ^If destroyed, that will expose this chamber, which you must know is depressurized.^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Upset] ^Every organic in the Gamma Labs will die in a few minutes.^[B][C]") ]])
                fnCutscene([[ Append("2856:[VOICE|Tiffany] ^Stopping Project Vivify takes priority over everything else.[P] Organic life in the Gamma Labs can be replaced.[P] SO, BLOW THE DOOR.^[B][C]") ]])
            end
            
            --Dialogue.
            fnCutscene([[ Append("Christine:[E|Offended] ^Oh, oh no![P] Unit 2856?[P] You're breaking up!^[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] ^Qrrrtkk![P] Qrrrt can't krrrk[P] hear kkkrrkkr you!^[B][C]") ]])
            fnCutscene([[ Append("2856:[VOICE|Tiffany][EMOTION|Christine|PDU] ^Are you making static sounds on a digital transmission through a network router?^[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Laugh] ^Qqrrrrr signal[P] kktktktkk jam![P] Call ykkrkrt ou later!^[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] ^I don't care if she bought it, let's find a way around.^[B][C]") ]])
            
            --Christine is in Electrosprite form:
            if(sChristineForm == "Electrosprite") then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iSkippedDatacore", "N", 1.0)
                fnCutscene([[ Append("55:[E|Smirk] ^Christine, can you move the bolts with your electric discharges?^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] ^I think it's worth a try. What are our alternatives?^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] ^We will need to go west through the Datacore and Beta labs to reach Gamma.^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] ^It will cost us time, however, if you are unable to move the bolts, it is our only option.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] ^PDU?[P] If Unit 2856 calls, send a bunch of static at her.[P] Digital static.^[B][C]") ]])
                fnCutscene([[ Append("PDU:[VOICE|Narrator][EMOTION|Christine|PDU] ^Affirmative.[P] I will randomize the static for maximum irritation.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] ^Good work, PDU.[P] Let's give this a try...^") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(45)
                fnCutsceneBlocker()
                fnCutscenePlaySound("World|SparksA")
                fnCutsceneWait(45)
                fnCutsceneBlocker()
                fnCutscenePlaySound("World|SparksB")
                fnCutsceneWait(45)
                fnCutsceneBlocker()
                fnCutscenePlaySound("World|SparksC")
                fnCutsceneWait(125)
                fnCutsceneBlocker()

                --Dialogue.
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
                fnCutscene([[ Append("55:[E|Neutral] ^Is something wrong?^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Angry] ^Stupid thing![P] This last one won't -[P] come -[P] out!^") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(5)
                fnCutsceneBlocker()
                fnCutscenePlaySound("World|SparksA")
                fnCutsceneWait(5)
                fnCutsceneBlocker()
                fnCutscenePlaySound("World|SparksB")
                fnCutsceneWait(5)
                fnCutsceneBlocker()
                fnCutscenePlaySound("World|SparksC")
                fnCutsceneWait(5)
                fnCutsceneBlocker()
                fnCutsceneWait(5)
                fnCutsceneBlocker()
                fnCutscenePlaySound("World|SparksA")
                fnCutsceneWait(5)
                fnCutsceneBlocker()
                fnCutscenePlaySound("World|SparksB")
                fnCutsceneWait(5)
                fnCutsceneBlocker()
                fnCutscenePlaySound("World|SparksC")
                fnCutsceneWait(5)
                fnCutsceneBlocker()
                fnCutsceneWait(125)
                fnCutsceneBlocker()

                --Dialogue.
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
                fnCutscene([[ Append("Christine:[E|Sad] ^G-[P]got it...^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] ^Did you damage yourself?^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^I'll be fine.[P] We can use the door now.[P] Phew![P] Hope I don't have to do that again!^") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(25)
                fnCutsceneBlocker()
                
                --Move to next area.
                fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
                fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsGammaC", "FORCEPOS:37.0x50.0x0") ]])
                
            --Christine is not in Electrosprite form, but has it:
            elseif(iHasElectrospriteForm == 1.0) then
                fnCutscene([[ Append("55:[E|Smirk] ^Christine. Do you think a zap from an electrosprite would be able to move the bolts?^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] ^I think it's worth a try. I probably won't blow anything up even if it doesn't work. We can come back and try it.^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] ^Otherwise, we will need to go west through the Datacore and Beta labs to reach Gamma.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] ^PDU?[P] If Unit 2856 calls, send a bunch of static at her.[P] Digital static.^[B][C]") ]])
                fnCutscene([[ Append("PDU:[VOICE|Narrator][EMOTION|Christine|PDU] ^Affirmative.[P] I will randomize the static for maximum irritation.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] ^Good work, PDU.[P] Let's move out.^") ]])
            
            --Find a way around!
            else
                fnCutscene([[ Append("55:[E|Smirk] ^The map suggests a passage through the Datacore and Beta labs on the western end.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] ^Then let's go that way.[P] And, PDU?[P] If Unit 2856 calls, send a bunch of static at her.[P] Digital static.^[B][C]") ]])
                fnCutscene([[ Append("PDU:[VOICE|Narrator][EMOTION|Christine|PDU] ^Affirmative.[P] I will randomize the static for maximum irritation.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] ^Good work, PDU.[P] Let's move out.^") ]])
            end
        
        --Still locked.
        else
            --Christine is in Electrosprite form:
            if(sChristineForm == "Electrosprite") then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iSkippedDatacore", "N", 1.0)
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
                fnCutscene([[ Append("Christine:[E|Smirk] ^All right everyone, may want to stand back.[P] I'm going to try to electromagnetically push the bolts through the wall.^") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(45)
                fnCutsceneBlocker()
                fnCutscenePlaySound("World|SparksA")
                fnCutsceneWait(45)
                fnCutsceneBlocker()
                fnCutscenePlaySound("World|SparksB")
                fnCutsceneWait(45)
                fnCutsceneBlocker()
                fnCutscenePlaySound("World|SparksC")
                fnCutsceneWait(125)
                fnCutsceneBlocker()

                --Dialogue.
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
                fnCutscene([[ Append("55:[E|Neutral] ^Is something wrong?^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Angry] ^Stupid thing![P] This last one won't -[P] come -[P] out!^") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(5)
                fnCutsceneBlocker()
                fnCutscenePlaySound("World|SparksA")
                fnCutsceneWait(5)
                fnCutsceneBlocker()
                fnCutscenePlaySound("World|SparksB")
                fnCutsceneWait(5)
                fnCutsceneBlocker()
                fnCutscenePlaySound("World|SparksC")
                fnCutsceneWait(5)
                fnCutsceneBlocker()
                fnCutsceneWait(5)
                fnCutsceneBlocker()
                fnCutscenePlaySound("World|SparksA")
                fnCutsceneWait(5)
                fnCutsceneBlocker()
                fnCutscenePlaySound("World|SparksB")
                fnCutsceneWait(5)
                fnCutsceneBlocker()
                fnCutscenePlaySound("World|SparksC")
                fnCutsceneWait(5)
                fnCutsceneBlocker()
                fnCutsceneWait(125)
                fnCutsceneBlocker()

                --Dialogue.
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
                fnCutscene([[ Append("Christine:[E|Sad] ^G-[P]got it...^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] ^Did you damage yourself?^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^I'll be fine.[P] We can use the door now.[P] Phew![P] Hope I don't have to do that again!^") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(25)
                fnCutsceneBlocker()
                
                --Move to next area.
                fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
                fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsGammaC", "FORCEPOS:37.0x50.0x0") ]])
            
            --Normal:
            else
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ Append("Thought:[VOICE|Leader] (The deadbolts on the door are down, and it's not receiving power.[P] We can't even pry it open...)") ]])
            end
        end
    
    --Door is repowered.
    else
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_BeginTransitionTo("RegulusBiolabsGammaC", "FORCEPOS:37.0x50.0x0")
    
    end

--Object
elseif(sObjectName == "Racks") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The plants in here are all dead from exposure to a vacuum.[P] There's no saving them...)") ]])

--Object
elseif(sObjectName == "ConversionChamber") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A conversion chamber, used for lifesaving if an individual is exposed to hard vacuum.[P] That scraprat is a real hero!)") ]])

--Object
elseif(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Looks like a log of the experiment going on in the connection tube.[P] Seems the researchers were testing various species of plant and how they cope with exposure to radiation from space.[P] Amazingly, soybeans are the most resilient by an order of magnitude!)") ]])

--Object
elseif(sObjectName == "TerminalB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (AUTOMATED NOTICE::[P] All monitored subjects are compromised.[P] Contact the research lead immediately!)") ]])

--Object
elseif(sObjectName == "Boxes") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Seeds, watering cans, and other gardening equipment.[P] The seeds are sorted by 'phenotype' but the labels are just random letters, I think.)") ]])
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
