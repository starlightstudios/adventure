-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
-- |[Objects]|
if(sObjectName == "ToDatacoreG") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsDatacoreG", "FORCEPOS:4.0x8.0x0")
    
elseif(sObjectName == "ToBiolabsBetaA") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    local iAmphibianPipesReleased = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianPipesReleased", "N")
    if(iAmphibianPipesReleased == 0.0) then
        AL_BeginTransitionTo("RegulusBiolabsBetaA", "FORCEPOS:4.0x22.0x0")
    else
        AL_BeginTransitionTo("RegulusBiolabsBetaMeltedA", "FORCEPOS:4.0x22.0x0")
    end
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
