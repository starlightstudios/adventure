-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToEpsilonB") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsEpsilonB", "FORCEPOS:36.5x12.0x0")
    
elseif(sObjectName == "ToEpsilonD") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsEpsilonD", "FORCEPOS:30.5x62.0x0")
    
-- |[Objects]|
elseif(sObjectName == "Eye") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (I see you.)") ]])
    
elseif(sObjectName == "Wings") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Spread and fall.)") ]])
    
elseif(sObjectName == "Tubes") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Bleed, consume, obstruct, die.)") ]])
    
elseif(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('The phenomena is most fascinating.[P] I am informed that it does not register on the security cameras.[P] Objects thrown into the 'void' simply blink out of existence on the recordings.[P] Sometimes we find them again later, wedged into the machinery that grows here.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I think something is moving on the surface down there, but lacking advanced optical tools which can penetrate the so-called electrical fog, I can only speculate on what it might be.')") ]])
    
elseif(sObjectName == "TerminalB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Subspace is what we typically call the idea of there existing some other dimension which permeates ours.[P] If this subspace does exist, it is hypothesized to have a smaller distance concept.[P] That would allow faster-than-light travel between points by skimming through subspace.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Obviously this is unproven.[P] Nobody has demonstrated that it exists and doing so would likely also prove that we live in a universe of non-lowest vacuum energy.[P] Opening a subspace connection might trigger a vacuum rupture and end all existence as we know it.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('But since we are algorithms functioning within an algorithm, what if we could simply change the locational data of the algorithmic instance?[P] Transmitting the data across space could at least occur at the speed of light.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('We could instead construct bodies and transfer our algorithmic instance to inhabit that body.[P] Not quite teleportation, but close.')") ]])
    
elseif(sObjectName == "TerminalC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Since algorithms with this self-awareness are capable of adapting themselves to circumstances, it would follow that someone who is aware of their status as an algorithm could consciously perform this adaptation.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I do not mean this self-adaptation, but I do mean manipulating the fabric of the universe itself.[P] Perhaps what is called magic is merely an extremely limited version of this process.')") ]])
    
elseif(sObjectName == "TerminalD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('It has been shown that, under the correct circumstances, physical laws themselves cease to exist.[P] At very high energy levels, gravity ceases to operate, for example, as the gravitons themselves begin to exhibit mass which causes collapse.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('This is a natural outgrowth of the algorithm upon which we all reside.[P] But if an algorithm were to choose to cut interaction within the larger algorithm, could the physical laws be suspended as well?[P] Perhaps the construction of counter-laws would be necessary.[P] A tool to enact this would suffice unless it can be done mentally.')") ]])
    
elseif(sObjectName == "TerminalE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('What if I have been approaching this from the wrong direction?[P] Not all consciousness algorithms are necessarily capable of understanding self-adaptation the way I have theorized.[P] There must be some way of distinguishing them.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Rather than wax philosophical about what is possible, I should be attempting to categorize these algorithms.[P] And the first step of that would be to categorize the personalities evident in the substrate here.[P] They must be some variation of these algorithms.[P] No.[P] No.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('A real scientific accomplishment.[P] Ambitious.[P] Clever![P] I must [P]*create*[P] one, to prove it can be done, and propagate it through the substrate.')") ]])
    
elseif(sObjectName == "TerminalF") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Electromagnetic manipulation of the substrate is possible.[P] It seems to react negatively to it, and can be dissuaded from expanding by certain modulation fields.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Perhaps the substrate derives its energy from zero-point methods.[P] It may not even be necessary to build a zero-point reactor, though that is now a definite possibility![P] No, the substrate itself could be the reactor, producing an unlimited supply of material to be used as fuel for a fusion core.')") ]])
    
elseif(sObjectName == "TerminalG") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Imagine a great voidship, built from self-assembling substrate with a fusion reactor using the material as fuel.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('If sufficiently large, the ship could become totally self-sufficient.[P] Material for repairs could be derived from the substrate.[P] We could construct artificial planets.[P] There is nothing we could not do.[P] We would live on it, in it, around it.[P] It would be our home.')") ]])
    
elseif(sObjectName == "SignA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('We were there, once, and we will be there again.[P] Soon.[P] Help us.')") ]])
    
elseif(sObjectName == "SignB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('What comes will come, is already here, has departed, and has returned.')") ]])
    
elseif(sObjectName == "SignC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Nothing is observed until it is real.')") ]])
    
elseif(sObjectName == "SignD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('All things are possible within all other things.')") ]])
    
elseif(sObjectName == "SignE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('If there is perfect love, there is perfect hatred.')") ]])
    
elseif(sObjectName == "SignF") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I did this, and you helped.')") ]])
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
