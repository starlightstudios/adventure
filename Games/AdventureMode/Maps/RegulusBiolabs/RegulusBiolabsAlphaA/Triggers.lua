-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "WarpActivate") then
    fnWarpUnlockHandler("Root/Variables/Chapter5/Campfires/iRegulusBiolabsAlphaA", 13.25, 25.50)

elseif(sObjectName == "EntryScene") then
    
    --Remove the map.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iReceivedMap", "N", 0.0)
        
    --Achievement.
    AM_SetPropertyJournal("Unlock Achievement", "ReachBiolabs")
    
    --Variables.
	local iSophieKnowsAboutRunestone = VM_GetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N")
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    VM_SetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N", 1.0)
    
    --Quest flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N", 50.0)
        
    --Topics for 55.
    WD_SetProperty("Unlock Topic", "Biolabs_2856", 1)
    WD_SetProperty("Unlock Topic", "Biolabs_Biolabs", 1)
    WD_SetProperty("Unlock Topic", "Biolabs_Biology", 1)
    WD_SetProperty("Unlock Topic", "Biolabs_Magic", 1)
    WD_SetProperty("Unlock Topic", "Biolabs_Runestones", 1)
    
    --Topics for SX-399.
    WD_SetProperty("Unlock Topic", "Biolabs_Latex", 1)
    WD_SetProperty("Unlock Topic", "Biolabs_Oxygen", 1)
    
    --Topics for Sophie
    local iHasElectrospriteForm = VM_GetVar("Root/Variables/Global/Christine/iHasElectrospriteForm", "N")
    WD_SetProperty("Unlock Topic", "Biolabs_Runestone", 1)
    if(iHasElectrospriteForm == 1.0) then
        WD_SetProperty("Unlock Topic", "Biolabs_Electrosprites", 1)
    end
    
    --Turn off everyone's collision flags, in case they are still on.
    EM_PushEntity("Tiffany")
        TA_SetProperty("Clipping Flag", false)
    DL_PopActiveObject()
    EM_PushEntity("Sophie")
        TA_SetProperty("Clipping Flag", false)
    DL_PopActiveObject()
    if(iSX399JoinsParty == 1.0) then
        EM_PushEntity("SX-399")
            TA_SetProperty("Clipping Flag", false)
        DL_PopActiveObject()
    end
    
    --Set this flag to indicate we're in the biolabs.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iReachedBiolabs", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/i55IsFollowing", "N", 1.0)
    if(iSX399JoinsParty == 1.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399IsFollowing", "N", 1.0)
    end
    
    --Unlock the gala dresses for Sophie and Christine.
    VM_SetVar("Root/Variables/Costumes/Christine/iGolemGala", "N", 1.0)
    VM_SetVar("Root/Variables/Costumes/Sophie/iGolemGala", "N", 1.0)
    
    --Warps are still disallowed.
    VM_SetVar("Root/Variables/Chapter5/WorldState/iNoWarpsAllowed", "N", 1.0)
    
    --Black the screen out.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Undo this collision.
    AL_SetProperty("Set Collision", 23, 29, 0, 0)
    
    --Position the party.
    fnCutsceneTeleport("Christine", 14.25, 31.50)
    fnCutsceneTeleport("Tiffany", 14.25, 31.50)
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneTeleport("SX-399", 14.25, 31.50)
    end
    fnCutsceneTeleport("Sophie", 14.25, 31.50)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    
    --Movement.
    local fSpeed = 2.50
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneMove("Christine", 15.25, 31.50, fSpeed)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 16.25, 31.50, fSpeed)
        fnCutsceneMove("Tiffany", 15.25, 31.50, fSpeed)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 17.25, 31.50, fSpeed)
        fnCutsceneMove("Tiffany", 16.25, 31.50, fSpeed)
        fnCutsceneMove("SX-399", 15.25, 31.50, fSpeed)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 18.25, 31.50, fSpeed)
        fnCutsceneMove("Tiffany", 17.25, 31.50, fSpeed)
        fnCutsceneMove("SX-399", 16.25, 31.50, fSpeed)
        fnCutsceneMove("Sophie", 15.25, 31.50, fSpeed)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 22.25, 31.50, fSpeed)
        fnCutsceneFace("Christine", 1, -1)
        fnCutsceneMove("Tiffany", 21.25, 31.50, fSpeed)
        fnCutsceneMove("SX-399", 20.25, 31.50, fSpeed)
        fnCutsceneMove("Sophie", 19.25, 31.50, fSpeed)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    else
        fnCutsceneMove("Christine", 15.25, 31.50, fSpeed)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 16.25, 31.50, fSpeed)
        fnCutsceneMove("Tiffany", 15.25, 31.50, fSpeed)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 17.25, 31.50, fSpeed)
        fnCutsceneMove("Tiffany", 16.25, 31.50, fSpeed)
        fnCutsceneMove("Sophie", 15.25, 31.50, fSpeed)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 18.25, 31.50, fSpeed)
        fnCutsceneMove("Tiffany", 17.25, 31.50, fSpeed)
        fnCutsceneMove("Sophie", 16.25, 31.50, fSpeed)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 22.25, 31.50, fSpeed)
        fnCutsceneFace("Christine", 1, -1)
        fnCutsceneMove("Tiffany", 21.25, 31.50, fSpeed)
        fnCutsceneMove("Sophie", 20.25, 31.50, fSpeed)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    end
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    if(iSX399JoinsParty == 1.0) then
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Tiffany", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "SX-399", "Neutral") ]])
    else
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
    end
    fnCutscene([[ Append("Christine:[E|Offended] Gee, I wonder if she went this way...[B][C]") ]])
    fnCutscene([[ Append("55:[E|Upset] Obviously, she did.[P] Security units do not use acid to destroy walls or leave corpses in their wake.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Sarcasm, 55.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] (I'd tell her to lighten up, but considering the situation, maybe I shouldn't be so facetious.)[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] (Nah.[P] I gotta keep screwing with her...)[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Christine, do you have to stay so slimy and...[P] tentacled?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Does this help?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
			
    --Flashwhite.
    Cutscene_CreateEvent("Flash Christine White", "Actor")
        ActorEvent_SetProperty("Subject Name", "Christine")
        ActorEvent_SetProperty("Flashwhite Quickly", "Null")
    DL_PopActiveObject()
    fnCutsceneBlocker()

    fnCutsceneWait(75)
    fnCutsceneBlocker()
    fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
    fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    if(iSX399JoinsParty == 1.0) then
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Tiffany", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "SX-399", "Neutral") ]])
    else
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
    end
    fnCutscene([[ Append("Christine:[E|Laugh] Smashing![P] I still have my dress on and everything![P] It's not even scuffed![B][C]") ]])
    
    --Sophie knows about the runestone:
    if(iSophieKnowsAboutRunestone == 1.0) then
        fnCutscene([[ Append("Sophie:[E|Happy] So much better![B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] Wasting time, 771852...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Yeah, yeah.[P] Let's go, team.[B][C]") ]])
    else
        fnCutscene([[ Append("Sophie:[E|Happy] Wow, that's so cool![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] 55 told me you could transform yourself but this is the first time I've seen it![B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] She is most uncreative in the use of this ability.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Now Sophie, my other forms may be more useful in here.[P] So don't get upset if I change again.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Offended] Promise me no tentacles.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Laugh] I promise![B][C]") ]])
    end
    fnCutscene([[ Append("Christine:[E|Smirk] Oh, I can also use the Costumes menu at a save point to switch to my normal outfit if I want to![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Me too, now that nobody cares about me being a slave unit.[P] Let's go!") ]])
    
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Christine", 23.25, 30.50)
    fnCutsceneMove("Christine", 23.25, 27.50)
    fnCutsceneMove("Tiffany", 22.25, 31.50)
    fnCutsceneMove("Tiffany", 23.25, 30.50)
    fnCutsceneMove("Tiffany", 23.25, 27.50)
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneMove("SX-399", 22.25, 31.50)
        fnCutsceneMove("SX-399", 23.25, 30.50)
        fnCutsceneMove("SX-399", 23.25, 27.50)
    end
    fnCutsceneMove("Sophie", 22.25, 31.50)
    fnCutsceneMove("Sophie", 23.25, 30.50)
    fnCutsceneMove("Sophie", 23.25, 27.50)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Fold Party") ]])
    fnCutsceneBlocker()
    
    --Reset this collision.
    fnCutscene([[ AL_SetProperty("Set Collision", 23, 29, 0, 1) ]])
    fnCutsceneBlocker()
    
    -- |[Catalysts]|
    --Set catalyst counters for the biolabs.
    AdInv_SetProperty("Resolve Catalysts By String", "Chapter 5-2")

--Get the map!
elseif(sObjectName == "DumbGuards") then

    --Repeat check.
    local iReceivedMap = VM_GetVar("Root/Variables/Chapter5/Scenes/iReceivedMap", "N")
    if(iReceivedMap == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iReceivedMap", "N", 1.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "LatexDrone", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "LatexDroneB", "Neutral") ]])
    fnCutscene([[ Append("Drone:[E|Neutral] EXCUSE THE INTERRUPTION, SUPERIOR UNITS, BUT THIS UNIT REQUIRES YOUR ASSISTANCE.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Oh?[P] What do you need help with?[B][C]") ]])
    fnCutscene([[ Append("Drone:[E|Neutral] THIS UNIT WAS ORDERED TO [P][VOICE|2856]'Get that worthless piece of trash away from me' [VOICE|LatexDrone][P]BY COMMAND UNIT 2856.[B][C]") ]])
    fnCutscene([[ Append("Drone:[E|Neutral] THIS UNIT FOUND IT AND THOUGHT THE COLOURS WERE PRETTY.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Hmm?[P] Let me take a look.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] It is a tourist brochure.[P] Its previous owner scribbled some notes on it.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Look at the little raiju face![B][C]") ]])
    fnCutscene([[ Append("55:[E|Smirk] [EMOTION|Sophie|Smirk]It is also a map of the area we can refer to.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] You didn't download the map before we came here?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Wireframe area schematics only.[P] This map contains additional details and connectivity paths.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Excellent work, drone![P] Well done![B][C]") ]])
    fnCutscene([[ Append("Drone:[E|Neutral] THIS UNIT IS PLEASED IT COULD DISPOSE OF ITS GARBAGE IN A WAY THAT PLEASED YOU.[B][C]") ]])
    fnCutscene([[ Append("Drone:[E|Neutral] WOULD YOU LIKE IT TO PROVIDE ANY FURTHER WORTHLESS GARBAGE FOR YOU?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] No thank you, drone.[B][C]") ]])
    fnCutscene([[ Append("Drone:[E|Neutral] AFFIRMATIVE.[P] HAVE A SPIFFY DAY.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] [SOUND|Menu|SpecialItem](Received a map of the biolabs!)[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Christine, one thing before we continue.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Oh?[P] What is it?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] I have been analyzing tactical data from our teamwork over the past few weeks.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] I believe I can make use of a new combat algorithm I have made explicitly to take advantage of your unique abilities.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] You mean my hard head?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Precisely.[P] In honor of your choice of weapon, I have named this algorithm 'Spearhead'.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] It should allow me to overclock my systems, causing damage to myself, in exchange for major increases in performance.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] I will be relying on your repair and covering abilities to keep me optimal in this algorithm.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Splendid![P] So I'll keep doing what I always do, and you'll just be better at what you do![B][C]") ]])
    fnCutscene([[ Append("55:[E|Smirk] That is one way of expressing the idea, yes.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smirk] (55 now has the 'Spearhead' Job!)") ]])
    fnCutsceneBlocker()
    
    fnResolveMapLocation("RegulusBiolabsAlphaA")
    
    --Give Tiffany the spearhead job and set her to it, which will unlock it.
    VM_SetVar("Root/Variables/Global/Tiffany/iHasJob_Spearhead", "N", 1.0)
    AdvCombat_SetProperty("Push Party Member", "Tiffany")
        AdvCombatEntity_SetProperty("Active Job", "Spearhead")
    DL_PopActiveObject()
    

end
