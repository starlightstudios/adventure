-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[SecurityA / B]|
    if(sActorName == "SecurityA" or sActorName == "SecurityB") then
        
        --Variables.
        local iSpokeTo2856Biolabs = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeTo2856Biolabs", "N")
        
        --Not spoken to 2856 yet.
        if(iSpokeTo2856Biolabs == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Drone:[VOICE|Golem] COMMAND UNIT 2856 TOLD US TO TELL YOU TO SPEAK TO HER AS SOON AS POSSIBLE.[B][C]") ]])
            fnCutscene([[ Append("Drone:[VOICE|Golem] SO, SPEAK TO COMMAND UNIT 2856.[P] AS SOON AS POSSIBLE. THANK YOU.") ]])
        
        --Spoken to 2856.
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Drone:[VOICE|Golem] THIS UNIT WILL REMAIN IN THIS AREA TO SAFEGUARD COMMAND UNIT 2856.[P] THANK YOU FOR YOUR PATIENCE.") ]])
            fnCutsceneBlocker()
        end
    
    -- |[2856]|
    elseif(sActorName == "2856") then
    
        --Variables.
        local iSpokeTo2856Biolabs = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeTo2856Biolabs", "N")
        local iRepoweredGamma     = VM_GetVar("Root/Variables/Chapter5/Scenes/iRepoweredGamma", "N")
        local iSX399JoinsParty    = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
        
        --ANGRY:
        if(iRepoweredGamma == 1.0) then
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            if(iSX399JoinsParty == 1.0) then
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
            else
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
            end
            fnCutscene([[ Append("2856:[E|Yelling] WHAT ARE YOU DOING HERE!?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Didn't you say you wanted to talk?[B][C]") ]])
            fnCutscene([[ Append("2856:[E|Yelling] YOU DISCONNECTED THE VIDEO CALL AND I TOLD YOU TO GO TO THE TRANSIT STATION.[P] WHY ARE YOU DELIBERATELY WASTING MY TIME?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Precisely to see exactly this reaction.[P] See you at the transit station!") ]])
            fnCutsceneBlocker()
        
        --First time.
        elseif(iSpokeTo2856Biolabs == 0.0) then 
        
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeTo2856Biolabs", "N", 1.0)
            DialogueActor_Push("2856")
                DialogueActor_SetProperty("Remove Alias", "55")
                DialogueActor_SetProperty("Remove Alias", "2855")
            DL_PopActiveObject()
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            if(iSX399JoinsParty == 1.0) then
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
            else
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
            end
            fnCutscene([[ Append("2856:[E|Neutral] So you survived, again.[P] Perhaps I was right to enlist your aid.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Do you have an alternative?[P] I think you're stuck with us.[B][C]") ]])
            fnCutscene([[ Append("2856:[E|Punchable] Considering the situation, a nuclear detonation is rapidly becoming more viable.[B][C]") ]])
            fnCutscene([[ Append("2856:[E|Neutral] Oh, don't worry.[P] I intend to wait until after you fail before unleashing one.[P] We don't even have any stockpiled, it may take some time to synthesize.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Great.[P] Peachy.[P] Better stop Vivify, then.[B][C]") ]])
            fnCutscene([[ Append("2856:[E|Punchable] Please do.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Distant explosion.
            fnCutscene([[ AudioManager_PlaySound("World|Explosion") ]])
            fnCutsceneBlocker()
            fnCutsceneFace("2856", 1, 1)
            fnCutsceneFace("Christine", 1, 1)
            fnCutsceneBlocker()
            fnCutsceneWait(125)
            fnCutsceneBlocker()
            fnCutsceneFace("2856", 0, -1)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            if(iSX399JoinsParty == 1.0) then
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
            else
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
            end
            fnCutscene([[ Append("Christine:[E|Sad] What was that?[P] Did you hear that?[B][C]") ]])
            fnCutscene([[ Append("2856:[E|Neutral] Don't play dumb.[P] You know exactly what it was.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] The vibration came through the ground, not the air, so it was external to the biolabs.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] That was likely an explosive charge set off within the city.[P] It has begun.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Wh-[P]what?[P] But - [P][CLEAR]") ]])
            fnCutscene([[ Append("55:[E|Neutral] We detonated a set of charges to seal the basement.[P] Our sympathizers took that as the signal to attack.[B][C]") ]])
            fnCutscene([[ Append("2856:[E|Punchable] So you got what you wanted, didn't you?[P] Even if I ordered security units to help us, they're now going to be tied up fighting your rebels.[P] Nice work.[B][C]") ]])
            fnCutscene([[ Append("2856:[E|Neutral] Now no help is coming,[P] it's all your fault,[P] [EMOTION|2856|Yelling]AND WE ARE NO CLOSER TO STOPPING VIVIFY THAN BEFORE.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Damn it...[B][C]") ]])
            fnCutscene([[ Append("2856:[E|Angry] I don't like this, and I don't like having to rely on dumb bolts like you, 771852.[P] You are unreliable.[B][C]") ]])
            fnCutscene([[ Append("2856:[E|Neutral] Most of my remaining teams are handling the evacuation and fighting a rearguard action.[P] I have...[P] six...[P] units in the biolabs.[B][C]") ]])
            fnCutscene([[ Append("2856:[E|Neutral] It looks like they made their way to the Raiju Ranch north of here.[P] YOU are going to clear a path for me.[B][C]") ]])
            fnCutscene([[ Append("2856:[E|Neutral] We'll discuss our next move at the ranch.[P] Is that understood?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] The opportunity to spend quality time with you?[P] Wouldn't miss it for the world.[B][C]") ]])
            fnCutscene([[ Append("2856:[E|Punchable] Your sarcastic tendencies are noted.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Come on, team.") ]])
            fnCutsceneBlocker()
        else
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            if(iSX399JoinsParty == 1.0) then
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
            else
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
            end
            fnCutscene([[ Append("2856:[E|Neutral] Maybe if I stare at this terminal hard enough, you'll [EMOTION|2856|Yelling]GET BACK TO WORK.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Fine![P] We're leaving!") ]])
            fnCutsceneBlocker()
        end
    end
end
