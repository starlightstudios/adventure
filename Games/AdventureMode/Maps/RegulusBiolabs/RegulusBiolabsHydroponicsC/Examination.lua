-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToHydroponicsBLft") then
    giForceFacing = gci_Face_North
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsHydroponicsB", "FORCEPOS:4.5x7.0x0")
    
elseif(sObjectName == "ToHydroponicsBRgt") then
    giForceFacing = gci_Face_North
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsHydroponicsB", "FORCEPOS:38.5x7.0x0")
    
elseif(sObjectName == "ToHydroponicsD") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsHydroponicsD", "FORCEPOS:9.0x27.0x0")
    
-- |[Objects]|
elseif(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Why yes, Unit 591302, those [P]*are*[P] the bones of animals from the beta labs![P] However did you come to that conclusion?')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Grind them into powder for fertilizer and stop complaining or I'll put an official reprimand on your record!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I just got done fixing the light-amp rig and now you want me to go to Omicron Transit?[P] We're not even going to test the lights?[P] Fine.[P] Don't complain if they blow out next time you use them.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The Lord Golem of Hydroponics wrote in a note file that she likes standing on the catwalks, watching the Slave Units work...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('The room is a mess because cleaning it is always at the bottom of my priority list.[P] Every time you add something new, you want it done first.[P] This is what happens![P] I recommend you adjust the algorithm so that a task's priority is summed with how many days since it was issued.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Or you can just yell louder.[P] That will fix it!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "NoteA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Cargo deliveries to datacore server room::[P] Right.[P] Datacore Offices::[P] Left.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "NoteB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Attention Idiots::[P] Blue is water, yellow is alcohol.[P] Do NOT water the plants with alcohol again![P] With the exception of the guava, no plant species can properly metabolize alcohol.')") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "Water") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Barrels of water, presumably for hand-watering of delicate samples that sprinklers can't be used for.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Booze") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Barrels of alcohol.[P] Smells like whiskey.[P] I wonder where they brew it?)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Junk") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Tools and broken junk that nobody has gotten around to recycling.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Furniture") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Looks like spare furniture for picnics.[P] I guess they have nowhere better to store it.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Bones") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Assorted animal bones.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Fabricator") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A Model-D fabricator bench.[P] The tools are left out, so whoever was using it last left in a hurry.)") ]])
    fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
