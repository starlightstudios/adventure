-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToEpsilonD") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsEpsilonD", "FORCEPOS:30.5x19.0x0")
    
-- |[Objects]|
elseif(sObjectName == "Note") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('You are an algorithm, operating on data in a larger algorithm.[P] Do not see a wall as a wall, see it as the points of data that it is.[P] You cannot be constrained, you are more than this.')") ]])
    
elseif(sObjectName == "Terminal") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Freezing cycle completed.[P] Subject awaiting transport.')") ]])
    
elseif(sObjectName == "Body") then
            
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "201890", "Neutral") ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ Append("Christine:[E|Neutral] 201890.[P] Give this girl a burial.[B][C]") ]])
    fnCutscene([[ Append("201890:[E|Neutral] You mean, displace some earth, place her body in a grave, and cover it over?[P] Why?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] She deserves to have the dignity of a resting place.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Will you do this?[B][C]") ]])
    fnCutscene([[ Append("201890:[E|Neutral] Why not?[P] I will see to it.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Thank you, 20.") ]])
    
elseif(sObjectName == "DoorFreezer") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (She's in there, frozen...)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (She will remain that way until she decides she no longer wants to.[P] The threat is as neutralized as it could be.)") ]])
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
