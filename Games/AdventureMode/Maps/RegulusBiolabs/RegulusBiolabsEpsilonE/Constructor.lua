-- |[Constructor]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusBiolabsEpsilonE"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music. Uses layering.
	AL_SetProperty("Music", "Null")
    
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("RegulusBiolabsEpsilonE")
	
	-- |[Spawn NPCS Here]|
	--NPCs set to spawn here will be available for post-exec cutscenes.

	-- |[Lights]|
    --AL_SetProperty("Activate Lights")
    
    -- |[Loading]|
    --Loads if the images aren't already loaded.
    if(DL_Exists("Root/Images/AdventureUI/MapUnderlays/Clouds") == false) then
        
        --Underlays use linear filtering.
        local iLinear = DM_GetEnumeration("GL_LINEAR")
        ALB_SetTextureProperty("MagFilter", iLinear)
        ALB_SetTextureProperty("MinFilter", iLinear)

        --Underlays need to wrap.
        local iRepeatEnum = DM_GetEnumeration("GL_REPEAT")
        ALB_SetTextureProperty("Wrap S", iRepeatEnum)
        ALB_SetTextureProperty("Wrap T", iRepeatEnum)

        --Load.
        SLF_Open(gsDatafilesPath .. "UIAdventure.slf")
        DL_AddPath("Root/Images/AdventureUI/MapUnderlays/")
        DL_ExtractBitmap("Underlay|Clouds",   "Root/Images/AdventureUI/MapUnderlays/Clouds")
        DL_ExtractBitmap("Underlay|UmumAsru", "Root/Images/AdventureUI/MapUnderlays/UmumAsru")

        --Return to normal.
        ALB_SetTextureProperty("Restore Defaults")
        SLF_Close()
    end

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[Spawn NPCs Here]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
	
	-- |[Underlays]|
    local fMoveScale = 0.010
    local fMapScale = 0.50
    local fCloudScale = 0.50
    local fCloudScroll = 0.05
    AL_SetProperty("Allocate Backgrounds", 2)
    AL_SetProperty("Background Image",          0, "Root/Images/AdventureUI/MapUnderlays/UmumAsru")
    AL_SetProperty("Background Render Offsets", 0, 800.0, 1500.0, fMoveScale, fMoveScale)
    AL_SetProperty("Background Alpha",          0, 1.0, 0)
    AL_SetProperty("Background Autoscroll",     0, 0.0, 0.0)
    AL_SetProperty("Background Scale",          0, -fMapScale)
    
    AL_SetProperty("Background Image",          1, "Root/Images/AdventureUI/MapUnderlays/Clouds")
    AL_SetProperty("Background Render Offsets", 1, 0.0, 0.0, fMoveScale, fMoveScale)
    AL_SetProperty("Background Alpha",          1, 1.0, 0)
    AL_SetProperty("Background Autoscroll",     1, fCloudScroll, fCloudScroll)
    AL_SetProperty("Background Scale",          1, -fCloudScale)
    
    --Vivify if the cutscene has already fired:
    local iMet20 = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet20", "N")
    if(iMet20 == 1.0) then
    TA_Create("Vivify")
            TA_SetProperty("Position", 26, 4)
            TA_SetProperty("Facing", gci_Face_South)
            TA_SetProperty("Clipping Flag", false)
            fnSetCharacterGraphics("Root/Images/Sprites/Vivify/", false)
            TA_SetProperty("Rendering Offsets", gcfTADefaultXOffset - 9.0, gcfTADefaultYOffset - 18.0)
            TA_SetProperty("Add Special Frame", "Freeze0", "Root/Images/Sprites/Special/Vivify|Freeze0")
            TA_SetProperty("Add Special Frame", "Freeze1", "Root/Images/Sprites/Special/Vivify|Freeze1")
            TA_SetProperty("Add Special Frame", "Freeze2", "Root/Images/Sprites/Special/Vivify|Freeze2")
            TA_SetProperty("Add Special Frame", "Freeze3", "Root/Images/Sprites/Special/Vivify|Freeze3")
            TA_SetProperty("Add Special Frame", "Freeze4", "Root/Images/Sprites/Special/Vivify|Freeze4")
        DL_PopActiveObject()
        fnCutsceneTeleport("Vivify", 26.25, 4.00)
        fnCutsceneSetFrame("Vivify", "Freeze4")
    end
    
    --Disable these layers.
    for i = 0, 99, 1 do
        iFirst = math.floor(i / 10)
        iSecond = math.floor(i % 10)
        AL_SetProperty("Set Layer Disabled", "CloudsAnim" .. iFirst .. iSecond, true)
    end
end
