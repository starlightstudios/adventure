-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "TimeAtLast") then
    
    --Variables.
    local bIsSX399Present = fnIsCharacterPresent("SX-399")
    local iMet20 = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet20", "N")
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    if(iMet20 == 0.0) then
        
        --Achievement.
        AM_SetPropertyJournal("Unlock Achievement", "FinishEpsilon")
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMet20", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N", 53.0)
        
        --Spawn entities.
        TA_Create("20")
            TA_SetProperty("Position", 26, 32)
            TA_SetProperty("Clipping Flag", true)
            fnSetCharacterGraphics("Root/Images/Sprites/20/", false)
            TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
            TA_SetProperty("Facing", gci_Face_South)
        DL_PopActiveObject()
        TA_Create("Vivify")
            TA_SetProperty("Position", 26, 28)
            TA_SetProperty("Facing", gci_Face_South)
            TA_SetProperty("Clipping Flag", false)
            fnSetCharacterGraphics("Root/Images/Sprites/Vivify/", false)
            TA_SetProperty("Rendering Offsets", gcfTADefaultXOffset - 9.0, gcfTADefaultYOffset - 18.0)
            TA_SetProperty("Add Special Frame", "Freeze0", "Root/Images/Sprites/Special/Vivify|Freeze0")
            TA_SetProperty("Add Special Frame", "Freeze1", "Root/Images/Sprites/Special/Vivify|Freeze1")
            TA_SetProperty("Add Special Frame", "Freeze2", "Root/Images/Sprites/Special/Vivify|Freeze2")
            TA_SetProperty("Add Special Frame", "Freeze3", "Root/Images/Sprites/Special/Vivify|Freeze3")
            TA_SetProperty("Add Special Frame", "Freeze4", "Root/Images/Sprites/Special/Vivify|Freeze4")
        DL_PopActiveObject()
        fnCutsceneTeleport("Vivify", 26.25, 28.00)
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("201890:[VOICE|201890] Wherever I go, there you are.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Merge and move.
        fnCutsceneMergeParty()
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 26.25, 44.50)
        fnCutsceneMove("Tiffany", 26.25, 44.50)
        if(bIsSX399Present == false) then
            fnCutsceneMove("Christine", 25.75, 44.50)
            fnCutsceneFace("Christine", 0, -1)
            fnCutsceneMove("Tiffany", 26.75, 44.50)
            fnCutsceneFace("Tiffany", 0, -1)
        else
            fnCutsceneMove("SX-399", 26.25, 44.50)
            fnCutsceneMove("Christine", 26.25, 44.50)
            fnCutsceneFace("Christine", 0, -1)
            fnCutsceneMove("Tiffany", 25.25, 44.50)
            fnCutsceneFace("Tiffany", 0, -1)
            fnCutsceneMove("SX-399", 27.25, 44.50)
            fnCutsceneFace("SX-399", 0, -1)
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Transform Christine.
        if(sChristineForm ~= "Eldritch") then

            --Flashwhite.
            Cutscene_CreateEvent("Flash Christine White", "Actor")
                ActorEvent_SetProperty("Subject Name", "Christine")
                ActorEvent_SetProperty("Flashwhite Quickly", "Null")
            DL_PopActiveObject()
            fnCutsceneBlocker()

            fnCutsceneWait(75)
            fnCutsceneBlocker()
            fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Eldritch.lua") ]])
            fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Facing.
            if(bIsSX399Present == false) then
                fnCutsceneFace("Tiffany", -1, 0)
            else
                fnCutsceneFace("Tiffany", 1, 0)
                fnCutsceneFace("SX-399", -1, 0)
            end
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutscene([[ Append("55:[E|Neutral] What are you doing, Christine?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I have to be like this, 55.[P] Trust me.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Very well...") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Facing.
            if(bIsSX399Present == false) then
                fnCutsceneFace("Tiffany", 0, -1)
            else
                fnCutsceneFace("Tiffany", 0, -1)
                fnCutsceneFace("SX-399", 0, -1)
            end
        end
        
        --20 moves up.
        fnCutsceneMove("20", 26.25, 42.50)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "201890", "Neutral") ]])
        fnCutscene([[ Append("201890:[E|Neutral] Ah, Christine.[P] You come unwelcome to a place neither here nor there.[P] Can't you just leave for good?[B][C]") ]])
        fnCutscene([[ Append("201890:[E|Neutral] What sort of test must you fail before her faith is shaken?[B][C]") ]])
        if(bIsSX399Present == false) then
            fnCutscene([[ Append("Christine:[E|Neutral] Let me handle this, 55.[B][C]") ]])
        else
            fnCutscene([[ Append("Christine:[E|Neutral] Let me handle this, everyone.[B][C]") ]])
        end
        fnCutscene([[ Append("201890:[E|Neutral] How imperious.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I understand far, far more than you ever will, 20.[P] You can see mere glimpses of the future.[P] I see it in all its glory.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Glory?[B][C]") ]])
        if(bIsSX399Present == true) then
            fnCutscene([[ Append("SX-399:[E|Neutral] Christine, why are you saying that?[B][C]") ]])
        end
        fnCutscene([[ Append("Christine:[E|Neutral] To see something far greater than you, witness its impossible power and be a mere speck in its presence.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] To know that you are hewn from that greater thing...[P] That is what I know.[P] I see it when I look.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I am of it.[P] I am animated by it.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Its being is terrifying.[P] I bow before it.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] It is glorious.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] Christine![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] No, 55.[P] There is nothing wrong.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] I see this thing that exists in the future, and I respect it.[P] But I also know that it is powerless before me.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Because those who exist in the past are far, far greater than those that exist in the future.[P] They must have our blessing to do so much as be.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk][EMOTION|Tiffany|Neutral] To know that this awe-inspiring devil's machine will exist only with my permission...[P] That, too, is glorious.[B][C]") ]])
        fnCutscene([[ Append("201890:[E|Neutral] So that's it, is it?[P] You cling to the idea that you can change the future?[B][C]") ]])
        fnCutscene([[ Append("201890:[E|Neutral] You've seen it.[P] We are empty.[P] But you still hold to it?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] This, 201890, is why you are not her favoured disciple.[P] You hew so close to her teachings that you cannot overcome them.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] She has taught you much, but what have you taught her?[B][C]") ]])
        fnCutscene([[ Append("201890:[E|Neutral] What could we mere gnats teach them?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Right from wrong, now from then, life from death.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] She is weeping in her song and you stand here.[P] You don't even care.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Stand aside.[B][C]") ]])
        fnCutscene([[ Append("201890:[E|Neutral] She is weeping?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Listen to the song.[P] Let it flow into you.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Scene.
        fnCutsceneWait(45)
        fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
        fnCutsceneBlocker()
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The song washes over me.[P] I peer into the future.[P] I peer into the past.[P] I am not me.[P] I am nothing.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I awaken.[P] I was not asleep, I am not awake, I am merely aware where before I was not.[P] Nothing has changed.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I see before me walls of skin, dead and bleeding.[P] I look up.[P] I see the stars.[P] They stretch to infinity.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Somewhere far away, a great tendril of sinew and muscle thrashes at the void.[P] It waves and makes no sound.[P] I walk.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I see a wall.[P] A hole in the wall beckons to me.[P] I press myself against it, and the wall allows me inside.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I am surrounded by the flesh, we are one and the same.[P] I forget myself.[P] I forget time.[P] A million years pass.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I step away from the wall, out the other side.[P] Nothing has changed.[P] I walk.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I see another like me.[P] She stands above a small tendril.[P] Her eyes betray no recognition.[P] I am not aware she is there, she is not aware she is there.[P] She merely is.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The tendril extends up.[P] It finds her soft fold and presses into her.[P] It impales her, lifts her off the ground.[P] Something squirms up the tendril, and into her.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The tendril releases her, and she walks.[P] She walks for some time.[P] I watch her walk.[P] The thing that was in her falls out, dead.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I know what I must do.[P] I walk to the tendril, and it presses inside me.[P] I feel it pushing against my insides, filling me with dead, cold flesh.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I am full.[P] The tendril puts me down, and I walk a step.[P] I feel the stillborn thing stir within me.[P] Nothing is created.[P] It falls out.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (She has failed, again.[P] Nothing can work, infinite time and infinite failure.[P] She cries.[P] She wails into the void.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (All of us shudder and fall.[P] We writhe on the mat of flesh in time to her song.[P] And the cycle repeats as it always has and always will.[P] Dead.[P] Dying.[P] Unchanging.[P] Eternal.)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Fade in.
        fnCutsceneWait(25)
        fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "201890", "Neutral") ]])
        fnCutscene([[ Append("201890:[E|Neutral] I saw it...[P] so clearly...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I was helping you.[P] Do you feel her sorrow now?[B][C]") ]])
        fnCutscene([[ Append("201890:[E|Neutral] I...[P] I...[P] I am so sorry, Christine.[B][C]") ]])
        fnCutscene([[ Append("201890:[E|Neutral] I did not realize the mistake I was making.[P] Have I been hurting her?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] No, she is beyond that.[P] But I can help her.[P] Please.[B][C]") ]])
        fnCutscene([[ Append("201890:[E|Neutral] Of course.[P] Of course![P] We are sorry we tried to stop you.[B][C]") ]])
        fnCutscene([[ Append("201890:[E|Neutral] But the things I have said...[P] The others will not believe me.[P] They will try to stop you.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Christine?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] We must speak with her.[P] Come.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("20", 26.25, 43.50)
        fnCutsceneMove("20", 27.25, 43.50)
        fnCutsceneFace("20", -1, 0)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 26.25, 43.50)
        fnCutsceneMove("Tiffany", 26.25, 44.50)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 26.25, 42.50)
        fnCutsceneMove("Tiffany", 26.25, 43.50)
        if(bIsSX399Present == true) then
            fnCutsceneMove("SX-399", 26.25, 44.49)
        end
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 26.25, 31.50)
        fnCutsceneMove("Tiffany", 26.25, 32.00)
        fnCutsceneMove("Tiffany", 25.25, 32.00)
        fnCutsceneFace("Tiffany", 0, -1)
        if(bIsSX399Present == true) then
            fnCutsceneMove("SX-399", 26.25, 32.00)
            fnCutsceneMove("SX-399", 27.25, 32.00)
            fnCutsceneFace("SX-399", 0, -1)
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ Append("Christine:[E|Neutral] The...[P] vibrations...[P] merge...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] All parts join.[P] Which was I?[P] I see myself twice.[P] Who are these?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Christine, remain in control of yourself.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Maturity.[P] This flesh was maturity.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("Christine", 26.25, 30.50)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", 0, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        if(bIsSX399Present == false) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Vivify", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] The vibrations join together, and we are one.[P] I am maturity, I am life, I am the giver.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Presumably, when the proximity is reduced, Christine's old self will reassert itself.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Though I am no longer speaking to her, am I?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Christine.[P] Can you understand me?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] We can.[P] What the flesh sees, we see.[P] What the flesh hears, we hear.[P] What the flesh understands, we understand.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Project Vivify, I have come to negotiate.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I wish for an end to the hostilities between our people.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] No reaction...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] What would Christine do in this situation?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] ...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Why are you sad, Vivify?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] This flesh...[P] It is not moving.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] We have vibrated and moved and pushed, but it is not moving.[P] Why is it not moving?[P] This makes us sad.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] You mean the girl there?") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneFace("Christine", 0, -1)
            fnCutsceneMove("Tiffany", 24.25, 32.00)
            fnCutsceneMove("Tiffany", 24.25, 29.50)
            fnCutsceneMove("Tiffany", 24.75, 29.50)
            fnCutsceneFace("Tiffany", 1, 0)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Vivify", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
            fnCutscene([[ Append("55:[E|Neutral] This human has been dead for some time.[P] Her body is cold.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] The flesh isn't different from the others, but it does not move.[P] It makes us sad.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] ...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Did you kill her?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] The flesh resisted.[P] It stopped resisting, now it does not move.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] You have retired many units, and yet somehow, only now, are you noticing?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Hmmm...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] Vivify.[P] What is death?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] We.[P] That word.[P] The flesh knows the word but we do not.[P] What is death?[P] Death?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Die.[P] Dying.[P] To Die.[P] Death.[P] Inflicted, carried, borne.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] When living creatures are harmed, they die.[P] Permanently.[P] Sometimes they can be saved, but only a few moments after dying.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] When dead, they can not be brought back to life.[P] Ever.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] The flesh is the same as this.[P] It only changed slightly and now it does not move.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] The difference between a living and dead human is slight, true, but life is fragile.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] You, Project Vivify were created when an artifact was placed within the dead body of a human in this very facility.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] It began to move, but showed no life signs.[P] It was restrained and transported to the new Cryogenics facility south of the city.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] You are not alive, and have never been alive.[P] You don't know what life is, do you?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] The flesh is dead, but does not move...[P] We have done this...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] We have done this many times.[P] We...[P] cannot change this?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] All is not repeatable, all is not set?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] No.[P] This girl is dead, you caused it, and you can never take it back.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] You must live with this burden and the burden you have placed on her friends.[P] They will now have a hole in their lives that cannot be filled.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] You cannot escape it, you cannot ignore it.[P] You must make amends as best you can, and you must move on...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] We...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] We...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] We require time to think.[P] We must think about ourselves.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] We will release this flesh.[P] We are...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] Sorry.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Sorry.[P] Apologizing.[P] Admitting error.[P] Regret.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] We are sorry.[P] We will go now.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Camera refocuses on Vivify.
            Cutscene_CreateEvent("CameraEvent", "Camera")
                CameraEvent_SetProperty("Max Move Speed", 2.0)
                CameraEvent_SetProperty("Focus Actor Name", "Vivify")
            DL_PopActiveObject()
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneFace("Tiffany", 1, -1)
            fnCutsceneFace("Christine", 0, -1)
            fnCutsceneMove("Vivify", 26.25, 27.50, 0.50)
            fnCutsceneBlocker()
            fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
            fnCutscene([[ AL_SetProperty("Open Door", "DoorFreezer") ]])
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Moves into the freezer.
            fnCutsceneMove("Vivify", 26.25, 24.50, 0.50)
            fnCutsceneFace("Vivify", 0, 1)
            fnCutsceneBlocker()
            fnCutsceneWait(35)
            fnCutsceneBlocker()
            fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
            fnCutscene([[ AL_SetProperty("Close Door", "DoorFreezer") ]])
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Narrator: Beginning cryogenic freezing cycle.[P] Please clear the chamber.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Do that here.
            fnCutscene([[ AudioManager_PlaySound("World|FreezeOver") ]])
            fnCutsceneSetFrame("Vivify", "Freeze0")
            fnCutsceneWait(60)
            fnCutsceneBlocker()
            fnCutsceneSetFrame("Vivify", "Freeze1")
            fnCutsceneWait(60)
            fnCutsceneBlocker()
            fnCutsceneSetFrame("Vivify", "Freeze2")
            fnCutsceneWait(60)
            fnCutsceneBlocker()
            fnCutsceneSetFrame("Vivify", "Freeze3")
            fnCutsceneWait(60)
            fnCutsceneBlocker()
            fnCutsceneSetFrame("Vivify", "Freeze4")
            fnCutsceneWait(180)
            fnCutsceneBlocker()
            
            --Camera refocuses on Christine.
            Cutscene_CreateEvent("CameraEvent", "Camera")
                CameraEvent_SetProperty("Max Move Speed", 2.0)
                CameraEvent_SetProperty("Focus Actor Name", "Christine")
            DL_PopActiveObject()
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            Cutscene_CreateEvent("CameraEvent", "Camera")
                CameraEvent_SetProperty("Max Move Speed", 5.0)
                CameraEvent_SetProperty("Focus Actor Name", "Christine")
            DL_PopActiveObject()
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
            fnCutscene([[ Append("55:[E|Neutral] She has voluntarily decided to freeze herself?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] The Epsilon Laboratories were where the original Cryogenics was built, before it was moved.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Even in all this, some of the equipment is still here and functioning.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] True, but that does nothing to aid us.[P] She had broken containment before.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneMove("20", 26.25, 43.50)
            fnCutsceneMove("20", 26.25, 31.50)
            fnCutsceneBlocker()
            fnCutsceneFaceTarget("Christine", "20")
            fnCutsceneFaceTarget("Tiffany", "20")
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "201890", "Neutral") ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ Append("201890:[E|Neutral] I see.[P] Even one such as her did not know all and see all.[B][C]") ]])
            fnCutscene([[ Append("201890:[E|Neutral] Could she be wrong about the future?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Yes and no.[P] The future has happened, but it is not set.[B][C]") ]])
            fnCutscene([[ Append("201890:[E|Neutral] But how can this be?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Because the past is not set.[P] All that exists is the present, and the future and past are selected to create the present.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] We sit at the nadir with all possible pasts and futures before us.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] You must expand your scope of thought.[P] There is nothing wrong with being wrong.[B][C]") ]])
            fnCutscene([[ Append("201890:[E|Neutral] I can see why you are the favoured.[P] Very well.[B][C]") ]])
            fnCutscene([[ Append("201890:[E|Neutral] Those that still listen to me will aid me.[P] We will move the master someplace away from here.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Yes, she will be this way for some time.[P] I fear the machine people here will try to use her as a weapon again.[B][C]") ]])
            fnCutscene([[ Append("201890:[E|Neutral] Perhaps the machine people should not have this knowledge forced on them.[P] They are not ready.[P] I know they are not, because it seems I was not.[B][C]") ]])
            fnCutscene([[ Append("201890:[E|Neutral] But this does not equalize us, Christine.[P] I will continue to study, and I will supercede you.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] You will not.[P] You think only in terms of hierarchy and advantage.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] If you have always one eye on your goal, then you have only one eye on your path.[B][C]") ]])
            fnCutscene([[ Append("201890:[E|Neutral] More wisdom from the master?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] No.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Wisdom from Earth.[B][C]") ]])
            fnCutscene([[ Append("201890:[E|Neutral] Hrmph.[B][C]") ]])
            fnCutscene([[ Append("201890:[E|Neutral] Leave, Christine.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Come on, 55.[P] We're done here.") ]])
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneMove("20", 30.25, 31.50)
            fnCutsceneFace("20", -1, 0)
            fnCutsceneBlocker()
            fnCutsceneMove("Tiffany", 24.75, 30.50)
            fnCutsceneMove("Tiffany", 26.25, 30.50)
            fnCutsceneBlocker()
            fnCutscene([[ AL_SetProperty("Fold Party") ]])
            fnCutsceneBlocker()
            
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Vivify", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] The vibrations join together, and we are one.[P] I am maturity, I am life, I am the giver.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Presumably, when the proximity is reduced, Christine's old self will reassert itself.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Sad] Presumably?[P] Is that good enough for you?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Sad] We need to blast that thing and get Christine far away from it![B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] No.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Angry] How can you be calm at a time like this!?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] We must be calm, precise, and rational.[P] If we make a mistake, we will lose Christine and the city.[P] Forever.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Angry] ...[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] I hate how often you're right.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Maturity.[P] It said the flesh was that.[P] I believe the flesh refers to Christine's body.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Maturity is a mistranslation of something?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Perhaps, perhaps not.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Christine.[P] Can you understand me?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] We can.[P] What the flesh sees, we see.[P] What the flesh hears, we hear.[P] What the flesh understands, we understand.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Project Vivify, we have come to negotiate.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] We wish for an end to the hostilities between our people.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] It's not working...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Sad] T-[P]try something else![B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] What would Christine do in this situation?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] ...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Why are you sad, Vivify?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] This flesh...[P] It is not moving.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] We have vibrated and moved and pushed, but it is not moving.[P] Why is it not moving?[P] This makes us sad.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] You mean the girl there?") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneFace("Christine", 0, -1)
            fnCutsceneMove("Tiffany", 24.25, 32.00)
            fnCutsceneMove("Tiffany", 24.25, 29.50)
            fnCutsceneMove("Tiffany", 24.75, 29.50)
            fnCutsceneFace("Tiffany", 1, 0)
            fnCutsceneMove("SX-399", 28.25, 32.00)
            fnCutsceneMove("SX-399", 28.25, 29.50)
            fnCutsceneMove("SX-399", 27.75, 29.50)
            fnCutsceneFace("SX-399", -1, 0)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Vivify", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
            fnCutscene([[ Append("55:[E|Neutral] This human has been dead for some time.[P] Her body is cold.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] The flesh isn't different from the others, but it does not move.[P] It makes us sad.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] ...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Did you kill her?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] The flesh resisted.[P] It stopped resisting, now it does not move.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Sad] It did kill her.[P] Why is that different?[P] Haven't they done that a hundred times?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Hmmm...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] Vivify.[P] What is death?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] We.[P] That word.[P] The flesh knows the word but we do not.[P] What is death?[P] Death?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Die.[P] Dying.[P] To Die.[P] Death.[P] Inflicted, carried, borne.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] When living creatures are harmed, they die.[P] Permanently.[P] Sometimes they can be saved, but only a few moments after dying.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] When dead, they can not be brought back to life.[P] Ever.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] The flesh is the same as this.[P] It only changed slightly and now it does not move.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] The difference between a living and dead human is slight, true, but life is fragile.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] But how could you not know that?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Project Vivify was created when an artifact was placed within the dead body of a human in this very facility.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] It began to move, but showed no life signs.[P] It was restrained and transported to the new Cryogenics facility south of the city.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Hence the name, Vivify.[P] She is not alive, but moving.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] But killing is wrong, Vivify.[P] Are you really saying you did not know?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] The flesh is dead, but does not move...[P] We have done this...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] We have done this many times.[P] We...[P] cannot change this?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] All is not repeatable, all is not set?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] No.[P] This girl is dead, you caused it, and you can never take it back.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] You must live with this burden and the burden you have placed on her friends.[P] They will now have a hole in their lives that cannot be filled.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] You cannot escape it, you cannot ignore it.[P] You must make amends as best you can, and you must move on...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] We...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] We...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] We require time to think.[P] We must think about ourselves.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] We will release this flesh.[P] We are...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] Sorry.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Sorry.[P] Apologizing.[P] Admitting error.[P] Regret.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] We are sorry.[P] We will go now.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Camera refocuses on Vivify.
            Cutscene_CreateEvent("CameraEvent", "Camera")
                CameraEvent_SetProperty("Max Move Speed", 2.0)
                CameraEvent_SetProperty("Focus Actor Name", "Vivify")
            DL_PopActiveObject()
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneFace("Tiffany", 1, -1)
            fnCutsceneFace("Christine", 0, -1)
            fnCutsceneFace("SX-399", -1, -1)
            fnCutsceneMove("Vivify", 26.25, 27.50, 0.50)
            fnCutsceneBlocker()
            fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
            fnCutscene([[ AL_SetProperty("Open Door", "DoorFreezer") ]])
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Moves into the freezer.
            fnCutsceneMove("Vivify", 26.25, 24.50, 0.50)
            fnCutsceneFace("Vivify", 0, 1)
            fnCutsceneBlocker()
            fnCutsceneWait(35)
            fnCutsceneBlocker()
            Cutscene_CreateEvent("CameraEvent", "Camera")
                CameraEvent_SetProperty("Max Move Speed", 2.0)
                CameraEvent_SetProperty("Focus Position", (26.25 * gciSizePerTile), (24.50 * gciSizePerTile))
            DL_PopActiveObject()
            fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
            fnCutscene([[ AL_SetProperty("Close Door", "DoorFreezer") ]])
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Narrator: Beginning cryogenic freezing cycle.[P] Please clear the chamber.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Do that here.
            local iTPF = 6
            fnCutscene([[ AudioManager_PlaySound("World|FreezeOver") ]])
            
            --Once the audio begins, start running across the layers like frames of an animation. There are 100 layers 
            -- at 3TPF for 300 ticks, or 5 seconds.
            local sThisAnim, sNextAnim, iFirst, iSecond
            fnCutsceneLayerDisabled("CloudsAnim00", false)
            fnCutsceneWait(iTPF)
            fnCutsceneBlocker()
            for i = 1, 99, 1 do
                
                --Hide this layer.
                iFirst = math.floor((i-1) / 10)
                iSecond = math.floor((i-1) % 10)
                sThisAnim = "CloudsAnim" .. iFirst .. iSecond
                fnCutsceneLayerDisabled(sThisAnim, true)
                
                --Show this layer.
                iFirst = math.floor(i / 10)
                iSecond = math.floor(i % 10)
                sNextAnim = "CloudsAnim" .. iFirst .. iSecond
                fnCutsceneLayerDisabled(sNextAnim, false)
                
                --At specific moments, change Vivify's freeze.
                if(i == 40) then
                    fnCutsceneSetFrame("Vivify", "Freeze0")
                elseif(i == 50) then
                    fnCutsceneSetFrame("Vivify", "Freeze1")
                elseif(i == 60) then
                    fnCutsceneSetFrame("Vivify", "Freeze2")
                elseif(i == 70) then
                    fnCutsceneSetFrame("Vivify", "Freeze3")
                elseif(i == 80) then
                    fnCutsceneSetFrame("Vivify", "Freeze4")
                end
                
                --Time.
                fnCutsceneWait(iTPF)
                fnCutsceneBlocker()
                
            end
            
            --Hide the last layer.
            fnCutsceneLayerDisabled("CloudsAnim99", true)
            fnCutsceneWait(125)
            fnCutsceneBlocker()
            
            --Camera refocuses on Christine.
            Cutscene_CreateEvent("CameraEvent", "Camera")
                CameraEvent_SetProperty("Max Move Speed", 1.0)
                CameraEvent_SetProperty("Focus Actor Name", "Christine")
            DL_PopActiveObject()
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            Cutscene_CreateEvent("CameraEvent", "Camera")
                CameraEvent_SetProperty("Max Move Speed", 5.0)
                CameraEvent_SetProperty("Focus Actor Name", "Christine")
            DL_PopActiveObject()
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
            fnCutscene([[ Append("SX-399:[E|Sad] She...[P] froze herself?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] The Epsilon Laboratories were where the original Cryogenics was built, before it was moved.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Even in all this, some of the equipment is still here and functioning.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] But what now?[P] Is she just going to sit in there?") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneMove("20", 26.25, 43.50)
            fnCutsceneMove("20", 26.25, 31.50)
            fnCutsceneBlocker()
            fnCutsceneFaceTarget("Christine", "20")
            fnCutsceneFaceTarget("Tiffany", "20")
            fnCutsceneFaceTarget("SX-399", "20")
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "201890", "Neutral") ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ Append("201890:[E|Neutral] I see.[P] Even one such as her did not know all and see all.[B][C]") ]])
            fnCutscene([[ Append("201890:[E|Neutral] Could she be wrong about the future?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Yes and no.[P] The future has happened, but it is not set.[B][C]") ]])
            fnCutscene([[ Append("201890:[E|Neutral] But how can this be?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Because the past is not set.[P] All that exists is the present, and the future and past are selected to create the present.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] We sit at the nadir with all possible pasts and futures before us.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] You must expand your scope of thought.[P] There is nothing wrong with being wrong.[B][C]") ]])
            fnCutscene([[ Append("201890:[E|Neutral] I can see why you are the favoured.[P] Very well.[B][C]") ]])
            fnCutscene([[ Append("201890:[E|Neutral] Those that still listen to me will aid me.[P] We will move the master someplace away from here.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Yes, she will be this way for some time.[P] I fear the machine people here will try to use her as a weapon again.[B][C]") ]])
            fnCutscene([[ Append("201890:[E|Neutral] Perhaps the machine people should not have this knowledge forced on them.[P] They are not ready.[P] I know they are not, because it seems I was not.[B][C]") ]])
            fnCutscene([[ Append("201890:[E|Neutral] But this does not equalize us, Christine.[P] I will continue to study, and I will supercede you.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] You will not.[P] You think only in terms of hierarchy and advantage.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] If you have always one eye on your goal, then you have only one eye on your path.[B][C]") ]])
            fnCutscene([[ Append("201890:[E|Neutral] More wisdom from the master?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] No.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Wisdom from Earth.[B][C]") ]])
            fnCutscene([[ Append("201890:[E|Neutral] Hrmph.[B][C]") ]])
            fnCutscene([[ Append("201890:[E|Neutral] Leave, Christine.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Come on, everyone.[P] We're done here.") ]])
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneMove("20", 30.25, 31.50)
            fnCutsceneFace("20", -1, 0)
            fnCutsceneBlocker()
            fnCutsceneMove("SX-399", 27.75, 30.50)
            fnCutsceneMove("SX-399", 26.25, 30.50)
            fnCutsceneMove("Tiffany", 24.75, 30.50)
            fnCutsceneMove("Tiffany", 26.25, 30.50)
            fnCutsceneBlocker()
            fnCutscene([[ AL_SetProperty("Fold Party") ]])
            fnCutsceneBlocker()
            
        end
    end

--Switch forms.
elseif(sObjectName == "NoGoingBack") then

    --Variables.
    local bIsSX399Present = fnIsCharacterPresent("SX-399")
    local iMet20 = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet20", "N")
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    if(iMet20 == 1.0 and sChristineForm ~= "Human") then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMet20", "N", 2.0)
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("55:[E|Neutral] Unit 771852?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Yes, Unit 2855?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Are you intending to remain in that form?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Is it a problem?[P] The threat has passed, I am myself.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] You are certain of that, but I am not.[B][C]") ]])
        if(bIsSX399Present == true) then
            fnCutscene([[ Append("SX-399:[E|Neutral] You don't trust her?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I trust Christine a great deal.[P] But I have not gotten as far as I have by being reckless.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] That's fair.[B][C]") ]])
        end
        fnCutscene([[ Append("Christine:[E|Neutral] Am I making you uncomfortable?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Please?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] As a favour to you?[P] Certainly.") ]])
        fnCutsceneBlocker()

        --Flashwhite.
        Cutscene_CreateEvent("Flash Christine White", "Actor")
            ActorEvent_SetProperty("Subject Name", "Christine")
            ActorEvent_SetProperty("Flashwhite Quickly", "Null")
        DL_PopActiveObject()
        fnCutsceneBlocker()

        fnCutsceneWait(75)
        fnCutsceneBlocker()
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
        fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
            
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Christine:[E|Neutral] Unit 771852, back in squishy form![B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Interesting.[P] I was anticipating your golem form.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I...[P] want to breathe.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] It feels right to have air go in and out of my lungs, somehow.[B][C]") ]])
        if(bIsSX399Present == true) then
            fnCutscene([[ Append("SX-399:[E|Smirk] Organics.[P] They're all the same, aren't they?[B][C]") ]])
        end
        fnCutscene([[ Append("55:[E|Neutral] This form is acceptable for our purposes.[P] Let us continue.") ]])
        fnCutsceneBlocker()
        
    end
end
