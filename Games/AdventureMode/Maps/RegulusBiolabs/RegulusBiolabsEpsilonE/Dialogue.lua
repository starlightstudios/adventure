-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Special: No need to go further if just backing out of dialogue.

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = "None"
if(sTopicName ~= "YesChristine" and sTopicName ~= "Yes55" and sTopicName ~= "NoCancel") then
    sActorName = TA_GetProperty("Name")
end

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    if(sActorName == "20") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "201890", "Neutral") ]])
        fnCutscene([[ Append("20:[E|Neutral] We will see to the master, Christine.[P] Otherwise, we have nothing to discuss.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Will out next encounter be peaceful?[B][C]") ]])
        fnCutscene([[ Append("20:[E|Neutral] Who can say?[P] If the future is not set...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] You can say.[P] It is you who determines what you do, and no one else.[B][C]") ]])
        fnCutscene([[ Append("20:[E|Neutral] ...") ]])
        fnCutsceneBlocker()
    end
end
