-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "Hydrophobe") then
    
    --Variables.
    local iAquaticsSawHydrophobe = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticsSawHydrophobe", "N")
    if(iAquaticsSawHydrophobe == 1.0) then return end
    
    --Variables.
    local bIsSX399Present = fnIsCharacterPresent("SX-399")
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    
    --Topics.
    WD_SetProperty("Unlock Topic", "Biolabs_Hydrophobia", 1)
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsSawHydrophobe", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsChristineIsAlone", "N", 1.0)
    
    --Movement.
    fnCutsceneMove("Christine", 37.25, 27.50)
    fnCutsceneFace("Christine", 1, -1)
    fnCutsceneMove("Tiffany", 36.25, 27.50)
    fnCutsceneFace("Tiffany", 1, 0)
    if(bIsSX399Present == true) then
        fnCutsceneMove("SX-399", 35.25, 27.50)
        fnCutsceneFace("SX-399", 1, 0)
    end
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Christine:[E|Neutral] Well well well, what's this?[P] An elevator that goes down into the water?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] 55 this is the part where you tell me what this is, because you already know.[B][C]") ]])
    if(bIsSX399Present == true) then
        fnCutscene([[ Append("SX-399:[E|Smirk] Because you know everything.[B][C]") ]])
    end
    fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Soooo...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] This elevator goes into the water because research staff have underwater labs set up.[P] After all, a golem isn't afraid of getting wet.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Lack of oxygen and high pressure are nothing to a robot.[P] Right?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 55 I'm guessing here.[P] I don't know if that's true.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] We need to find another way.[B][C]") ]])
    if(bIsSX399Present == true) then
        fnCutscene([[ Append("SX-399:[E|Smirk] This [P]*is*[P] the other way, unless you think you can jump that gap over there.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] We cannot jump that gap.[P] We must find another way.[B][C]") ]])
    end
    fnCutscene([[ Append("Christine:[E|Sad] (Okay, something is up...)[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] 55, what do you think is at the bottom?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Down] Look down there, Christine.[P] What if you fell in, and damaged yourself.[P] Imagine being trapped at the bottom of a pool of water, unable to climb out.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Down] Your power core would be able to keep running at reduced efficiency, almost indefinitely, by using the water as a power source.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Down] You would be partially conscious, trapped in the dark, and never escape.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] Uh huh.[P] But someone would find you, or you could go into system standby and build up power charge for improved but intermittent consciousness.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] So even if something went wrong, you could escape.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Down] But -[P] no, there is a chance even that would not work.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] It's all right, 55.[P] I understand.[B][C]") ]])
    
    --SX-399 is present.
    if(bIsSX399Present == true) then
        fnCutscene([[ Append("SX-399:[E|Neutral] I don't.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Uh, okay,[P] one second.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("Christine", 39.25, 27.50)
        fnCutsceneFace("Christine", 0, 1)
        fnCutsceneMove("SX-399", 39.25, 28.50)
        fnCutsceneFace("SX-399", 0, -1)
        fnCutsceneBlocker()
        fnCutsceneMove("Tiffany", 35.25, 27.50)
        fnCutsceneFace("Tiffany", 0, -1)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
        fnCutscene([[ Append("Christine:[E|Sad] Now this is just a guess, but I think 55 might be hydrophobic.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] Fear of water?[P] Hmm...[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] But why?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Fears aren't rational, she just has it.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] But she sees herself as totally rational, which means it's going to be even harder to help her.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] ...[P] Fiddlesticks.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] No matter what we do, she's not coming with us.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] Can you go ahead without us?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I suppose I must.[P] Keep her company?[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Flirt] As if you had to ask.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("Christine", 37.25, 27.50)
        fnCutsceneMove("SX-399", 37.25, 28.50)
        fnCutsceneBlocker()
        fnCutsceneFace("Tiffany", 1, 0)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Christine:[E|Neutral] Right, strategic decision.[P] I will proceed ahead, alone, while you two cover the rear.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] But - [P][CLEAR]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I will remain in contact via the PDU.[P] If I get in trouble, you can send SX-399 to support.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] Remember, Christine can transform.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Smirk] I'm sure her golem body can withstand the pressure just fine.[P] She'll be all right.[B][C]") ]])
        if(sChristineForm ~= "Golem") then
            fnCutscene([[ Append("Christine:[E|Neutral] Don't forget about my Steam Droid form![B][C]") ]])
        elseif(sChristineForm == "SteamDroid") then
            fnCutscene([[ Append("Christine:[E|Neutral] All right, Steam Droid Christine, going deep![B][C]") ]])
        else
            fnCutscene([[ Append("Christine:[E|Neutral] What if I transformed into a Steam Droid?[P] Would that work better?[B][C]") ]])
        end
        fnCutscene([[ Append("SX-399:[E|Neutral] Ah -[P] no.[P] Don't do that.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] I'm considerably better insulated than your average Steam Droid, but you won't be.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] The water will suck the heat out of your frame and greatly reduce your energy.[P] Steam Droids were not made for underwater work.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Well, scratch that, then.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Happy] Meanwhile, I'll make sure Cutie-5 -[P][P] keeps you covered.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] You are absolutely sure of this, 771852?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Yes.[P] I'm prepared to risk my life.[P] This is war.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] That is -[P] good.[P] We will remain here.[P] Make sure to keep us updated.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Count on it.[P] I'll be back before you know it.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Laugh] But that means the glory will be all mine![P] Ha ha!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("Tiffany", 23.25, 27.50)
        fnCutsceneMove("Tiffany", 23.25, 25.50)
        fnCutsceneFace("Tiffany", 1, 0)
        fnCutsceneMove("SX-399", 23.25, 28.50)
        fnCutsceneMove("SX-399", 23.25, 26.50)
        fnCutsceneFace("SX-399", 1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Remove everyone else from the party.
        giFollowersTotal = 0
        gsaFollowerNames = {}
        giaFollowerIDs = {0}
        AL_SetProperty("Unfollow Actor Name", "Tiffany")
        AL_SetProperty("Unfollow Actor Name", "SX-399")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399IsFollowing", "N", 0.0)
        
        --Scan slots, remove invalid members.
        AdvCombat_SetProperty("Clear Party")
        AdvCombat_SetProperty("Party Slot", 0, "Christine")
    
    --Just 55.
    else
        fnCutscene([[ Append("Christine:[E|Sad] (55 must be scared, but this really isn't the time or the place for a therapy session...)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] 55, I think it's best if we split up.[P] I'll proceed alone, you cover the rear and make sure my exit is clear.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] And if something goes wrong, and you require rescue?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I'll remain in contact via my PDU.[P] You'll be able to track me if you need to.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] And besides -[P] I'm the tough one, right?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] You are absolutely sure of this, 771852?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Yes.[P] I'm prepared to risk my life.[P] This is war.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] That is -[P] good.[P] I will remain here.[P] Keep me updated.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Count on it.[P] I'll be back before you know it.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Laugh] But that means the glory will be all mine![P] Ha ha!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("Tiffany", 23.25, 27.50)
        fnCutsceneMove("Tiffany", 23.25, 25.50)
        fnCutsceneFace("Tiffany", 1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Remove everyone else from the party.
        giFollowersTotal = 0
        gsaFollowerNames = {}
        giaFollowerIDs = {0}
        AL_SetProperty("Unfollow Actor Name", "Tiffany")
        AL_SetProperty("Unfollow Actor Name", "SX-399")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399IsFollowing", "N", 0.0)
        
        --Reset party.
        AdvCombat_SetProperty("Clear Party")
        AdvCombat_SetProperty("Party Slot", 0, "Christine")

    end
    fnCutsceneBlocker()
    
--55 and SX-399 rejoin the party.
elseif(sObjectName == "Rejoin") then

    --Variables.
    local iSX399JoinsParty          = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    local iAquaticsSawHydrophobe    = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticsSawHydrophobe", "N")
    local iAquaticsChristineIsAlone = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticsChristineIsAlone", "N")
    local iAquaticsActionScene      = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticsActionScene", "N")
    if(iAquaticsChristineIsAlone == 0.0 or iAquaticsSawHydrophobe == 0.0) then return end
    
    -- |[Normal]|
    if(iAquaticsActionScene ~= 2.0) then

        --Flags.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsChristineIsAlone", "N", 0.0)

        --Characters rejoin the party.
        fnCutsceneMove("Tiffany", 23.25, 27.50)
        fnCutsceneFace("Tiffany", 1, 0)
        if(iSX399JoinsParty == 1.0) then
            fnCutsceneMove("SX-399", 23.25, 28.50)
            fnCutsceneFace("SX-399", 1, 0)
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        if(iSX399JoinsParty == 0.0) then
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
        else
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "SX-399", "Neutral") ]])
        end
        fnCutscene([[ Append("55:[E|Neutral] Anything to report?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Not yet. Let's join up for a moment.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Affirmative.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Party merging.
        fnCutsceneMove("Christine", 23.25, 27.50)
        if(iSX399JoinsParty == 1.0) then
            fnCutsceneMove("SX-399", 23.25, 27.50)
        end
        fnCutsceneBlocker()
        
        --Clear combat party.
        AdvCombat_SetProperty("Clear Party")
        AdvCombat_SetProperty("Party Slot", 0, "Christine")
        
        --Rejoin 55.
        EM_PushEntity("Tiffany")
            local i55ID = RE_GetID()
        DL_PopActiveObject()
        giFollowersTotal = 1
        gsaFollowerNames = {"Tiffany"}
        giaFollowerIDs = {i55ID}
        AL_SetProperty("Follow Actor ID", i55ID)
        AdvCombat_SetProperty("Party Slot", 1, "Tiffany")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 1.0)
        
        --SX-399 also rejoins.
        if(iSX399JoinsParty == 1.0) then
        
            EM_PushEntity("SX-399")
                local iSX399ID = RE_GetID()
            DL_PopActiveObject()
            giFollowersTotal = 2
            gsaFollowerNames = {"Tiffany", "SX-399"}
            giaFollowerIDs = {i55ID, iSX399ID}
            AL_SetProperty("Follow Actor ID", iSX399ID)
            AdvCombat_SetProperty("Party Slot", 2, "SX-399")
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399IsFollowing", "N", 1.0)
        end
        
        --Fold.
        fnCutscene([[ AL_SetProperty("Fold Party") ]])
        fnCutsceneBlocker()
    
    -- |[Rejoin Scene]|
    else

        --Variables.
        local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    
        --Flags.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsChristineIsAlone", "N", 0.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsActionScene", "N", 3.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsCompleted", "N", 1.0)
        
        --Movement.
        fnCutsceneMove("Christine", 22.25, 27.50)
        fnCutsceneFace("Christine", 0, -1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --If SX-399 is not in the party, a much shorter scene plays.
        if(iSX399JoinsParty == 0.0) then
            
            --Movement.
            fnCutsceneFace("Tiffany", 0, 1)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            fnCutsceneMove("Tiffany", 22.25, 26.50)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Smirk") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
            fnCutscene([[ Append("55:[E|Neutral] Your smile suggests a smug self-satisfaction that often accompanies a successful mission.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Wow, 55.[P] People who are happy often have good news to report.[P] What sort of insight will you present next?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] Extra sarcasm in a response comment?[P] No casualties, then?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Other than yourself, evidently.[B][C]") ]])
            if(sChristineForm == "Darkmatter") then
                fnCutscene([[ Append("Christine:[E|Neutral] What's a little starlight here and there?[P] You said yourself that Darkmatters are hard to influence with conventional weapons.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] (Doesn't stop it from hurting a ton, though...)[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] If you say you are well, I am in no position to override your assessment.[B][C]") ]])
            else
                fnCutscene([[ Append("Christine:[E|Neutral] This dent in my cranial chassis?[P] Oh it'll buff right out.[B][C]") ]])
            end
            fnCutscene([[ Append("Christine:[E|Sad] ...[P] Don't tell Sophie or she'll worry...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] At your insistence.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] If that is all, our next assignment should be letting the captain know.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Yeah, in person.[P] I don't want to risk anyone overhearing us on the radio.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Correct.[P] Secrecy is important.[P] Let's move, then.") ]])
            fnCutsceneBlocker()
        
            --Clear combat party.
            AdvCombat_SetProperty("Clear Party")
            AdvCombat_SetProperty("Party Slot", 0, "Christine")
        
            --Rejoin 55.
            EM_PushEntity("Tiffany")
                local i55ID = RE_GetID()
            DL_PopActiveObject()
            giFollowersTotal = 1
            gsaFollowerNames = {"Tiffany"}
            giaFollowerIDs = {i55ID}
            AL_SetProperty("Follow Actor ID", i55ID)
            AdvCombat_SetProperty("Party Slot", 1, "Tiffany")
            VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 1.0)
            fnCutsceneMove("Christine", 22.25, 26.50)
            fnCutsceneBlocker()
        
            --Fold.
            fnCutscene([[ AL_SetProperty("Fold Party") ]])
            fnCutsceneBlocker()
            
        --A slightly longer scene plays out if SX-399 is present.
        else
        
            --Camera movement.
            Cutscene_CreateEvent("CameraEvent", "Camera")
                CameraEvent_SetProperty("Max Move Speed", 5.0)
                CameraEvent_SetProperty("Focus Actor Name", "Tiffany")
            DL_PopActiveObject()
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
            fnCutscene([[ Append("55:[E|Down] ...[P] But that is the concern I have.[P] I have done research on similar situations.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] You looked this sort of thing up?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] I have no experience with relationships.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] According to the camera footage I reviewed, even my previous self never had a tandem unit.[P] I - [B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] Have I ever?[P] Before I became this?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] But you do now, isn't that enough?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] No, it is not.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Units who find love during dangerous or stressful situations tend to form unstable connections.[P] My research suggests this.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] So you think it won't work out because of the circumstances.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] But as long as it's not all relationships, we can make it work.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] That is also not my concern.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] My concern is that, while this may be genuine, I do not want it to be temporary.[P] I do not want to hurt you emotionally if I am inadequate.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Blush] You aren't helping your case one bit, no ma'am![B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Please explain.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Blush] It's one thing to be attracted to one another.[P] I grant you that, and it certainly helps...[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] But attraction is about wanting and understanding.[P] And the effort you put into understanding me means that your mistakes are just that, mistakes.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] As opposed to?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] I don't know.[P] I can't say an error is deliberate, but a refusal to learn from an error is something like it.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] You won't abuse me.[P] You won't manipulate me.[P] You won't betray me.[P] Because if you do, I know you'll learn from it.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] ...[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] Sorry, those seem like pretty strong words.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] I'm just saying I trust you.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] ...[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] What?[P] What is it?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] Christine has been listening to us...") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneFace("Tiffany", 0, 1)
            fnCutsceneFace("SX-399", 0, 1)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            fnCutsceneMove("Tiffany", 22.25, 25.50, 0.50)
            fnCutsceneMove("SX-399", 21.25, 25.50, 0.50)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "SX-399", "Neutral") ]])
            fnCutscene([[ Append("Christine:[E|Sad] Before you lay into me, I just want to say that I didn't want to interrupt you.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Well you were still eavesdropping.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] F-[P]forgive me![P] I'm sorry![B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Unit 771852, mission status report.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Mission accomplished![P] Three units were rescued and will be making their way here underwater.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] We should let captain Jo know so she can have someone pick them up.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] Great work.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] I guess you get to live another day.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] I said I'm sorry![B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] I'm just toying with you, pal.[P] It's all cool.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Though eavesdropping is not appreciated.[P] Please exercise discretion in the future.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Affirmative.[P] Shall we be off?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Lead the way.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneMove("Tiffany", 22.25, 27.50)
            fnCutsceneMove("SX-399", 22.25, 27.50)
            fnCutsceneBlocker()
        
            --Clear combat party.
            AdvCombat_SetProperty("Clear Party")
            AdvCombat_SetProperty("Party Slot", 0, "Christine")
        
            --Rejoin 55.
            EM_PushEntity("Tiffany")
                local i55ID = RE_GetID()
            DL_PopActiveObject()
            giFollowersTotal = 1
            gsaFollowerNames = {"Tiffany"}
            giaFollowerIDs = {i55ID}
            AL_SetProperty("Follow Actor ID", i55ID)
            AdvCombat_SetProperty("Party Slot", 1, "Tiffany")
            VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 1.0)
        
            --Rejoin SX-399.
            EM_PushEntity("SX-399")
                local iSX399ID = RE_GetID()
            DL_PopActiveObject()
            giFollowersTotal = 2
            gsaFollowerNames = {"Tiffany", "SX-399"}
            giaFollowerIDs = {i55ID, iSX399ID}
            AL_SetProperty("Follow Actor ID", iSX399ID)
            AdvCombat_SetProperty("Party Slot", 2, "SX-399")
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399IsFollowing", "N", 1.0)
        
            --Fold.
            fnCutscene([[ AL_SetProperty("Fold Party") ]])
            fnCutsceneBlocker()
            
        end
    end
    
end
