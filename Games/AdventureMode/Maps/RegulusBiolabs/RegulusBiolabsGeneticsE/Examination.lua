-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToGeneticsB") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsGeneticsB", "FORCEPOS:47.0x16.0x0")
    
elseif(sObjectName == "ToGeneticsF") then
    
    --Plot Variables
    local iSX399JoinsParty          = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    local iAquaticsChristineIsAlone = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticsChristineIsAlone", "N")
    
    --Form Variables.
    local bCauseFormDecision   = false
    local sChristineForm       = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    local iHasDollForm         = VM_GetVar("Root/Variables/Global/Christine/iHasDollForm", "N")
    local iHasLatexForm        = VM_GetVar("Root/Variables/Global/Christine/iHasLatexForm", "N")
    local iHasDarkmatterForm   = VM_GetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N")
    local iAquaticsActionScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticsActionScene", "N")
    
    --Subquest is finished.
    if(iAquaticsActionScene >= 2.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (No need to go down there again.)") ]])
        fnCutsceneBlocker()
    
    --If Christine is not alone:
    elseif(iAquaticsChristineIsAlone == 0.0) then
        
        --Remove Tiffany and SX-399.
        giFollowersTotal = 0
        gsaFollowerNames = {}
        giaFollowerIDs = {0}
        AL_SetProperty("Unfollow Actor Name", "Tiffany")
        AL_SetProperty("Unfollow Actor Name", "SX-399")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399IsFollowing", "N", 0.0)
        
        --Clear combat party.
        AdvCombat_SetProperty("Clear Party")
        AdvCombat_SetProperty("Party Slot", 0, "Christine")
            
        --If Christine is in an acceptable underwater form:
        if(sChristineForm == "Golem" or sChristineForm == "Doll" or sChristineForm == "LatexDrone" or sChristineForm == "Darkmatter" or sChristineForm == "Doll") then
        
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            if(iSX399JoinsParty == 0.0) then
                fnCutscene([[ Append("55:[VOICE|Tiffany] I will remain here.[P] Keep me posted.") ]])
            else
                fnCutscene([[ Append("55:[VOICE|Tiffany] We will remain here.[P] Keep us posted.") ]])
            end
            fnCutsceneBlocker()
            
            --Move to the next room.
            fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
            fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsGeneticsF", "FORCEPOS:25.0x28.0x0") ]])
        
        elseif(sChristineForm == "Eldritch") then
        
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            if(iSX399JoinsParty == 0.0) then
                fnCutscene([[ Append("55:[VOICE|Tiffany] I will remain here.[P] Keep me posted.[B][C]") ]])
            else
                fnCutscene([[ Append("55:[VOICE|Tiffany] We will remain here.[P] Keep us posted.[B][C]") ]])
            end
            fnCutscene([[ Append("Christine:[VOICE|Christine] (This body's eyes might not see well underwater.[P] I should change forms.)[BLOCK]") ]])
            bCauseFormDecision = true
        
        elseif(sChristineForm == "Electrosprite" or sChristineForm == "Raiju") then
        
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            if(iSX399JoinsParty == 0.0) then
                fnCutscene([[ Append("55:[VOICE|Tiffany] I will remain here.[P] Keep me posted.[B][C]") ]])
            else
                fnCutscene([[ Append("55:[VOICE|Tiffany] We will remain here.[P] Keep us posted.[B][C]") ]])
            end
            fnCutscene([[ Append("Christine:[VOICE|Christine] (This body...[P] Well, I really have no idea what will happen if I take a dip.[P] Better not risk it.)[BLOCK]") ]])
            bCauseFormDecision = true
        
        elseif(sChristineForm == "Human") then
        
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            if(iSX399JoinsParty == 0.0) then
                fnCutscene([[ Append("55:[VOICE|Tiffany] I will remain here.[P] Keep me posted.[B][C]") ]])
            else
                fnCutscene([[ Append("55:[VOICE|Tiffany] We will remain here.[P] Keep us posted.[B][C]") ]])
            end
            fnCutscene([[ Append("Christine:[VOICE|Christine] (I don't know if my organic endurance will hold out long enough to swim this.[P] Better pick something more durable.)[BLOCK]") ]])
            bCauseFormDecision = true
        
        elseif(sChristineForm == "SteamDroid") then
        
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            if(iSX399JoinsParty == 0.0) then
                fnCutscene([[ Append("55:[VOICE|Tiffany] I will remain here.[P] Keep me posted.[B][C]") ]])
            else
                fnCutscene([[ Append("55:[VOICE|Tiffany] We will remain here.[P] Keep us posted.[B][C]") ]])
            end
            fnCutscene([[ Append("Christine:[VOICE|Christine] (SX-399 said this body wouldn't work well underwater.[P] Better pick something a little more modern.)[BLOCK]") ]])
            bCauseFormDecision = true
        
        elseif(sChristineForm == "Secrebot") then
        
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            if(iSX399JoinsParty == 0.0) then
                fnCutscene([[ Append("55:[VOICE|Tiffany] I will remain here.[P] Keep me posted.[B][C]") ]])
            else
                fnCutscene([[ Append("55:[VOICE|Tiffany] We will remain here.[P] Keep us posted.[B][C]") ]])
            end
            fnCutscene([[ Append("Christine:[VOICE|Christine] (I have no idea how a secrebot's wheels are going to work underwater, better pick something else.)[BLOCK]") ]])
            bCauseFormDecision = true
        end
        
        --If a form decision is required:
        if(bCauseFormDecision == true) then
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Golem\", " .. sDecisionScript .. ", \"Golem\") ")
            if(iHasDollForm == 1.0) then
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Doll\",  " .. sDecisionScript .. ", \"Doll\") ")
            end
            if(iHasLatexForm == 1.0) then
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Latex Drone\",  " .. sDecisionScript .. ", \"Drone\") ")
            end
            if(iHasDarkmatterForm == 1.0) then
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Darkmatter\",  " .. sDecisionScript .. ", \"Darkmatter\") ")
            end
            fnCutsceneBlocker()
        end
    
    --Christine is alone:
    else
        --If Christine is in an acceptable underwater form:
        if(sChristineForm == "Golem" or sChristineForm == "LatexDrone" or sChristineForm == "Darkmatter" or sChristineForm == "Doll") then
            
            --Move to the next room.
            fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
            fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsGeneticsF", "FORCEPOS:25.0x28.0x0") ]])
        
        elseif(sChristineForm == "Eldritch") then
        
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] (This body's eyes might not see well underwater.[P] I should change forms.)[BLOCK]") ]])
            bCauseFormDecision = true
        
        elseif(sChristineForm == "Electrosprite" or sChristineForm == "Raiju") then
        
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] (This body...[P] Well, I really have no idea what will happen if I take a dip.[P] Better not risk it.)[BLOCK]") ]])
            bCauseFormDecision = true
        
        elseif(sChristineForm == "Human") then
        
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] (I don't know if my organic endurance will hold out long enough to swim this.[P] Better pick something more durable.)[BLOCK]") ]])
            bCauseFormDecision = true
        
        elseif(sChristineForm == "SteamDroid") then
        
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] (SX-399 said this body wouldn't work well underwater.[P] Better pick something a little more modern.)[BLOCK]") ]])
            bCauseFormDecision = true
        
        elseif(sChristineForm == "Secrebot") then
        
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] (I'm not sure how well my wheels are going to do underwater, better pick another form.)[BLOCK]") ]])
            bCauseFormDecision = true
        end
        
        --If a form decision is required:
        if(bCauseFormDecision == true) then
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Golem\", " .. sDecisionScript .. ", \"Golem\") ")
            if(iHasDollForm == 1.0) then
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Doll\",  " .. sDecisionScript .. ", \"Doll\") ")
            end
            if(iHasLatexForm == 1.0) then
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Latex Drone\",  " .. sDecisionScript .. ", \"Drone\") ")
            end
            if(iHasDarkmatterForm == 1.0) then
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Darkmatter\",  " .. sDecisionScript .. ", \"Darkmatter\") ")
            end
            fnCutsceneBlocker()
        end
    
    end

-- |[Transformations]|
elseif(sObjectName == "Golem" or sObjectName == "Drone" or sObjectName == "Darkmatter" or sObjectName == "Doll") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")

    --Wait a bit.
    fnCutsceneWait(30)
    fnCutsceneBlocker()

    --Flash the active character to white. Immediately after, execute the transformation.
    Cutscene_CreateEvent("Flash Christine White", "Actor")
        ActorEvent_SetProperty("Subject Name", "Christine")
        ActorEvent_SetProperty("Flashwhite Quickly")
    DL_PopActiveObject()
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    if(sObjectName == "Golem") then
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
    elseif(sObjectName == "Doll") then
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Doll.lua") ]])
    elseif(sObjectName == "Drone") then
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Latex.lua") ]])
    elseif(sObjectName == "Darkmatter") then
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Darkmatter.lua") ]])
    end
    fnCutsceneWait(gci_Flashwhite_Ticks_Total)
    fnCutsceneBlocker()

    --Now wait a little bit.
    fnCutsceneWait(30)
    fnCutsceneBlocker()
    
    --Transfer to the next room.
    fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsGeneticsF", "FORCEPOS:25.0x28.0x0") ]])
    
-- |[Objects]|
elseif(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I was running late after a meeting at the manufactory, so I decided to take a shortcut and get off the tram at the cargo bay.[P] Big mistake.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('The drones there gave me a frisk, searched my pack, and then searched my quarters afterwards.[P] I didn't get an answer when I asked why, but they're drones so they probably don't know either.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Whatever they're searching for, it sure is annoying.[P] Won't be taking that shortcut again.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Yeah, I wrote that in a paper actually.[P] Free will is a required component of scientific endeavour, but Slave Units?[P] We could reprogram them for total obedience and not miss a beat.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Now, the counterargument is that Slave Units having free will allows them to optimize their own workspaces and suggest improvements.[P] But let's face it, Slave Units are terribly stupid and lazy.[P] What could they optimize?[P] What have they truly ever designed?')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This computer's background is a Darkmatter playing with a ball of string.[P] The image has probably been altered, but it's still cute.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A portable computer that is locked.[P] Probably not worth hacking.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Device") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Water pH and chemical composition are within expected parameters, according to this sensor.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Fabricator") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A fabrication bench in fairly good working order.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Bridge") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Just like they said, the bridge is blown up.[P] We'll have to find a way around.)") ]])
    fnCutsceneBlocker()

-- |[Skillbook]|
elseif(sObjectName == "Skillbook") then
    AL_SetProperty("Set Layer Disabled", "SkillbookIco", true)
	LM_ExecuteScript(gsChristineSkillbook, 6)

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
