-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Special: No need to go further if just backing out of dialogue.

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = "None"
if(sTopicName ~= "YesChristine" and sTopicName ~= "Yes55" and sTopicName ~= "NoCancel") then
    sActorName = TA_GetProperty("Name")
end

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    -- |[Rebel Golems]|
    if(sActorName == "GolemA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Rebel:[VOICE|RebelGolem] You know Captain Jo used to be just a welding unit?[P] She's got the courage to stare down a bullet train!") ]])
        
    elseif(sActorName == "GolemB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Rebel:[VOICE|RebelGolem] I joined up so I could be just like 771852 one day.[P] And now, here we are, face to face![P] It's an honour, ma'am.") ]])
        
    elseif(sActorName == "GolemC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Rebel:[VOICE|RebelGolem] I set up a monitoring application.[P] Everyone who got hit is in one of the domiciles above, and they can send a signal if they need something.") ]])
        
    elseif(sActorName == "GolemD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Rebel:[VOICE|RebelGolem] We all decided to drop our designations and give ourselves proper names.[P] I'm Leela, and the captain over there is named Jo.[P] I think she's a good Jo, don't you?") ]])
        
    elseif(sActorName == "GolemF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Rebel:[VOICE|RebelGolem] I'll keep watch on the door.[P] Do what you gotta do.") ]])
        
    elseif(sActorName == "PolishRebel") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Rebel:[VOICE|RebelGolem] The only way to get the prisoner to shut up is to order her to polish my chassis.[P] It was her idea to use her tongue, don't judge me!") ]])
        
    elseif(sActorName == "GolemLord") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lord:[VOICE|GolemLord] Affirmative, Rebel Unit.[P] Whatever you ask, Rebel Unit.[P] Mmmmmmmm...") ]])
        
    elseif(sActorName == "Jo") then
        
        --Variables.
        local iAquaticsCompleted = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticsCompleted", "N")
        local iSX399JoinsParty   = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
        
        --Player has not completed the subquest:
        if(iAquaticsCompleted == 0.0) then
    
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "RebelGolem", "Neutral") ]])
            fnCutscene([[ Append("Jo:[E|Neutral] Anything I can do for you lot?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Give me a quick refresher on the situation east of here.[P] What do we need to do?[B][C]") ]])
            fnCutscene([[ Append("Jo:[E|Neutral] While I'm against this...[B][C]") ]])
            fnCutscene([[ Append("Jo:[E|Neutral] The security units locked off the eastern access with a blue seal.[P] Not only does it lock the doors, it prevents network access.[B][C]") ]])
            fnCutscene([[ Append("Jo:[E|Neutral] You'll need to get a blue keycard somewhere.[P] I'm sure you can find one if you look.[B][C]") ]])
            fnCutscene([[ Append("Jo:[E|Neutral] The security team also blew up an access bridge, presumably to cover their retreat.[P] You'll need to either fix it, or find a way around.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] And then?[B][C]") ]])
            fnCutscene([[ Append("Jo:[E|Neutral] Assuming you get past the hostiles, locked doors, and blown bridge?[P] There's a gun-nest overlooking a bridge with no cover.[B][C]") ]])
            fnCutscene([[ Append("Jo:[E|Neutral] Our units are on the other side of that bridge.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I'm a repair unit.[P] Even if I can't haul them back, I can at least stabilize them.[B][C]") ]])
            fnCutscene([[ Append("Jo:[E|Neutral] That'd be more than enough.[B][C]") ]])
            fnCutscene([[ Append("Jo:[E|Neutral] But absolutely don't do any hero crap.[P] This war has been on a few hours and I've already lost a lot of friends.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I'll make sure you don't lose any here.[P] We're fighting for peace.[B][C]") ]])
            fnCutscene([[ Append("Jo:[E|Neutral] A lasting one, I hope.[P] Good hunting out there.") ]])
        
        --Subquest completed:
        elseif(iAquaticsCompleted == 1.0) then
        
            --Flag
            VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsCompleted", "N", 2.0)
    
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "RebelGolem", "Neutral") ]])
            fnCutscene([[ Append("Jo:[E|Neutral] Well well well, if it isn't the hero of the units.[B][C]") ]])
            fnCutscene([[ Append("Jo:[E|Neutral] Rescued three units by selflessly almost getting yourself killed?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Hello, Jo![P] We have a successful mission to report![B][C]") ]])
            fnCutscene([[ Append("Jo:[E|Neutral] We saw the whole thing on the security cameras.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] You said you were unable to access the systems on the east side of the facility.[B][C]") ]])
            fnCutscene([[ Append("Jo:[E|Neutral] That's right, boss.[P] But then, someone, and I won't say who, opened the blue lockdown door.[B][C]") ]])
            fnCutscene([[ Append("Jo:[E|Neutral] So apparently, as long as the keycard is in that door, we could access the network by piggybacking the door controls.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] A security vulnerability I was unaware of.[P] Excellent work, captain.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] Presumably you set up a tunneler for when the card is removed?[B][C]") ]])
            fnCutscene([[ Append("Jo:[E|Neutral] Probably, you'd have to ask my network unit.[B][C]") ]])
            fnCutscene([[ Append("Jo:[E|Neutral] Point is, we now have a videograph of Christine doing idiot-heroics to rescue downed units.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Terrific, right?[B][C]") ]])
            fnCutscene([[ Append("Jo:[E|Neutral] Does she have a death wish?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I believe so.[B][C]") ]])
            if(iSX399JoinsParty == 1.0) then
                fnCutscene([[ Append("SX-399:[E|Flirt] Zest for life means risking death.[B][C]") ]])
            end
            fnCutscene([[ Append("Christine:[E|Smirk] So have you sent anyone to pick up the units?[B][C]") ]])
            fnCutscene([[ Append("Jo:[E|Neutral] They made contact a moment ago.[P] We'll be dredging them up in a few minutes and getting them the repairs they need.[B][C]") ]])
            fnCutscene([[ Append("Jo:[E|Neutral] Now, obviously I can't go around condoning such madness.[P] There was a much larger chance that hit you took would be fatal.[B][C]") ]])
            fnCutscene([[ Append("Jo:[E|Neutral] Regardless of your transformative abilities, you're not invincible.[B][C]") ]])
            fnCutscene([[ Append("Jo:[E|Neutral] But...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] But I'm more important to the revolution than mere battlefield activities?[B][C]") ]])
            fnCutscene([[ Append("Jo:[E|Neutral] But...[P] Thank you for saving my friends.[B][C]") ]])
            fnCutscene([[ Append("Jo:[E|Neutral] When this is all over, I'll buy you a synthtwister and we'll laugh about how dumb we were.[B][C]") ]])
            if(iSX399JoinsParty == 1.0) then
                fnCutscene([[ Append("SX-399:[E|Smirk] Are.[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Smirk] I've known Christine for a little while now and I'll tell you one thing.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Becoming a robot has made her braver...[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Flirt] But it sure hasn't made her any smarter![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Angry] Hey![B][C]") ]])
                fnCutscene([[ Append("55:[E|Smug] We had to do something while covering your escape route.[P] So we came up with quips.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Among other things.[B][C]") ]])
            end
            fnCutscene([[ Append("Jo:[E|Neutral] Right, that's all I can ask of you.[B][C]") ]])
            fnCutscene([[ Append("Jo:[E|Neutral] We'll get ourselves ship-shape and move out to the transit station as soon as possible.[B][C]") ]])
            fnCutscene([[ Append("Jo:[E|Neutral] It might be some time before we see each other again, so I'll say it now.[P] On behalf of everyone here, it's been an honour.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Likewise.[P] Take care, captain.") ]])
        
        --Repeats.
        else
    
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "RebelGolem", "Neutral") ]])
            fnCutscene([[ Append("Jo:[E|Neutral] We'll be sending out a videograph of a crazed hero saving downed units and taking a bullet while doing it.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] It makes a great morale booster![B][C]") ]])
            fnCutscene([[ Append("Jo:[E|Neutral] Oh, no.[P] It's to terrify the administration.[B][C]") ]])
            fnCutscene([[ Append("Jo:[E|Neutral] Look at how absolutely bonkers our units are![P] The loyalists don't have a chance!") ]])
        
        end
        
    elseif(sActorName == "Ellie") then
        
        --Variables.
        local bIsSX399Present = fnIsCharacterPresent("SX-399")
        local iAquaticsSpokeToEllie = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticsSpokeToEllie", "N")
        
        --First encounter:
        if(iAquaticsSpokeToEllie == 0.0) then
        
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsSpokeToEllie", "N", 1.0)
    
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Ellie", "Neutral") ]])
            fnCutscene([[ Append("Ellie:[E|Neutral] (Oh criminy, it's Christine!)[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Ellie, was it?[B][C]") ]])
            fnCutscene([[ Append("Ellie:[E|Neutral] (And she knows my name!)[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Hello.[B][C]") ]])
            fnCutscene([[ Append("Ellie:[E|Neutral] (Aaaaack!)[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] Are you all right?[P] You're not saying anything...[B][C]") ]])
            
            --SX-399 variation.
            if(bIsSX399Present == true) then
                fnCutscene([[ Append("SX-399:[E|Smirk] She's awestruck.[B][C]") ]])
            end
            fnCutscene([[ Append("Ellie:[E|Neutral] ...............[P] oof.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] That's kind of your catch phrase, isn't it?[B][C]") ]])
            fnCutscene([[ Append("Ellie:[E|Neutral] I'm not trying to be rude, Christine.[P] It's just -[P] you're the reason I'm here.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Laugh] Really?[P] I'm flattered![B][C]") ]])
            fnCutscene([[ Append("55:[E|Smug] It was my idea to record the recruitment videos.[B][C]") ]])
            fnCutscene([[ Append("Ellie:[E|Neutral][EMOTION|Christine|Smirk] Yes, but...[B][C]") ]])
            fnCutscene([[ Append("Ellie:[E|Neutral] I had heard some mutterings among my staff.[P] I -[P] I'm not much of a leader.[B][C]") ]])
            fnCutscene([[ Append("Ellie:[E|Neutral] I worked in janitorial services at the Arcane University.[P] You know, making sure the rooms were clean and the oil makers were full.[B][C]") ]])
            fnCutscene([[ Append("Ellie:[E|Neutral] And a couple days ago, my second in command comes up to me and says she thinks the Slave Units are up to something.[B][C]") ]])
            fnCutscene([[ Append("Ellie:[E|Neutral] I went and asked, and...[P] they showed me a video.[P] Of you.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] The same one playing all over the city?[B][C]") ]])
            fnCutscene([[ Append("Ellie:[E|Neutral] Yeah, that one...[B][C]") ]])
            fnCutscene([[ Append("Ellie:[E|Neutral] And I realized that I didn't have to be the bad guy.[P] So I got in contact with the big boss...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Ellie is one of a few Lord Units who have decided to back our movement.[P] Her access codes have been very useful.[B][C]") ]])
            fnCutscene([[ Append("Ellie:[E|Neutral] ...[P] Too late to go back now, right?[P] Now I'm carrying around a pulse rifle and fighting the good fight.[B][C]") ]])
            fnCutscene([[ Append("Ellie:[E|Neutral] And it's all thanks to you.[P] You showed me that it's possible for a Lord Golem to be one of the good guys.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Odd, though.[P] I thought most Lord Units would want leadership roles.[B][C]") ]])
            fnCutscene([[ Append("Ellie:[E|Neutral] ...[P] oof.[P] No thanks.[B][C]") ]])
            fnCutscene([[ Append("Ellie:[E|Neutral] Jo is the best leader I've seen bar none.[P] I would probably just panic and screw something up.[B][C]") ]])
            fnCutscene([[ Append("Ellie:[E|Neutral] Not all of us got picked to be Lord Units because we're good leaders.[P] Some of us got picked because we didn't question orders.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] What about the rest of your staff?[P] Some of them might not forgive you for the things you've done to them.[B][C]") ]])
            fnCutscene([[ Append("Ellie:[E|Neutral] ...[P] Yeah.[P] My record isn't exactly clean.[B][C]") ]])
            fnCutscene([[ Append("Ellie:[E|Neutral] But if I provide covering fire for someone like Jo, well, that's as good a redemption as I'll get.[B][C]") ]])
            fnCutscene([[ Append("Ellie:[E|Neutral] I'm going to try to improve as much as I can.[P] Maybe someday I'll be as good a leader as she is.[B][C]") ]])
            fnCutscene([[ Append("Ellie:[E|Neutral] But for now, I'm just glad I got to meet you in person.[P] It's been a real pleasure.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Hey, it's not over yet.[P] There's going to be some dark times ahead.[B][C]") ]])
            fnCutscene([[ Append("Ellie:[E|Neutral] ...[P] oof.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] But if you switched sides, maybe some other Lord Units will.[P] Maybe the fighting will be over before you know it.[B][C]") ]])
            fnCutscene([[ Append("Ellie:[E|Neutral] I sure hope so.[P] Good luck, Christine.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Same to you, Ellie.") ]])
        
        --Repeats.
        else
    
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Ellie", "Neutral") ]])
            fnCutscene([[ Append("Ellie:[E|Neutral] You know, I've heard the phrase 'You should never meet your heroes'.[B][C]") ]])
            fnCutscene([[ Append("Ellie:[E|Neutral] Probably because they're not what you think they are in reality.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] And?[B][C]") ]])
            fnCutscene([[ Append("Ellie:[E|Neutral] Sometimes they are.[P] That's all.") ]])
        
        
        end
    end
end
