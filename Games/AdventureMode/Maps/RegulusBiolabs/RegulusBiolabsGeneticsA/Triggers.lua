-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "WarpActivate") then
    fnWarpUnlockHandler("Root/Variables/Chapter5/Campfires/iRegulusBiolabsGeneticsA", 16.25, 7.50)

elseif(sObjectName == "Meeting") then
    
    --Variables.
    local iAquaticMeeting = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticMeeting", "N")
    if(iAquaticMeeting == 1.0) then return end
    
    --Variables.
    local bIsSX399Present = fnIsCharacterPresent("SX-399")
    
    --Disable Ellie's collision flag.
    TA_ChangeCollisionFlag("Ellie", false)
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticMeeting", "N", 1.0)
    
    --Spread the party.
    fnCutsceneMove("Christine", 5.75, 12.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneMove("Tiffany", 6.75, 12.50)
    fnCutsceneFace("Tiffany", 0, 1)
    if(bIsSX399Present == true) then
        fnCutsceneMove("SX-399", 4.75, 12.50)
        fnCutsceneFace("SX-399", 0, 1)
    end
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Golem:[VOICE|RebelGolem] Hold it right there!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMoveFace("GolemF", 5.25, 17.50, 0, -1, 2.50)
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "RebelGolem", "Neutral") ]])
    fnCutscene([[ Append("Rebel:[E|Neutral] ...[B][C]") ]])
    if(bIsSX399Present == false) then
        fnCutscene([[ Append("Rebel:[E|Neutral] Unit 771852?[P] Unit 2855?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Present.[P] What company are you?[B][C]") ]])
    else
        fnCutscene([[ Append("Rebel:[E|Neutral] Unit 771852?[P] Unit 2855?[P] And -[P] uhhh...[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] SX-399.[P] Steam Droid heavy support.[B][C]") ]])
    end
    fnCutscene([[ Append("Rebel:[E|Neutral] H-[P]hold on...[B][C]") ]])
    fnCutscene([[ Append("Rebel:[E|Neutral] Ellie, get up here!") ]])
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Ellie", 12.75, 25.50, 2.00)
    fnCutsceneFace("Ellie", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Open Door", "DoorC") ]])
    fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneBlocker()
    fnCutsceneMove("Ellie", 12.75, 21.50, 2.00)
    fnCutsceneBlocker()
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Open Door", "DoorB") ]])
    fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneBlocker()
    fnCutsceneMove("Ellie", 12.75, 18.50, 2.00)
    fnCutsceneMove("Ellie", 6.25, 18.50, 2.00)
    fnCutsceneMove("Ellie", 6.25, 17.50, 2.00)
    fnCutsceneFace("Ellie", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneFace("Ellie", 0, -1)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "RebelGolem", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Ellie", "Neutral") ]])
    fnCutscene([[ Append("Ellie:[E|Neutral] Are we under attack?[P] I'm here![P] Ack![B][C]") ]])
    fnCutscene([[ Append("Rebel:[E|Neutral] Calm down.[P] I just need you to escort these units to Jo.[B][C]") ]])
    fnCutscene([[ Append("Ellie:[E|Neutral] Is that it?[P] Oof...[P] Sorry...[B][C]") ]])
    fnCutscene([[ Append("Rebel:[E|Neutral] You don't need to apologize.[P] Just take them to the back.[P] I'll keep watch up here.[B][C]") ]])
    fnCutscene([[ Append("Ellie:[E|Neutral] Wait...[P] Is that...?[B][C]") ]])
    fnCutscene([[ Append("Rebel:[E|Neutral] Right now, Ellie![B][C]") ]])
    fnCutscene([[ Append("Ellie:[E|Neutral] Sorry![P] I'm on it!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Ellie", 9.25, 17.50)
    fnCutsceneFace("Ellie", 0, -1)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Open Door", "DoorA") ]])
    fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Ellie:[VOICE|Ellie] This way, please.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 9.25, 15.50)
    fnCutsceneMove("Christine", 9.25, 16.50)
    fnCutsceneMove("Tiffany", 9.25, 14.50)
    fnCutsceneMove("Tiffany", 9.25, 15.50)
    if(bIsSX399Present == true) then
        fnCutsceneMove("SX-399", 9.25, 13.50)
        fnCutsceneMove("SX-399", 9.25, 14.50)
    end
    fnCutsceneBlocker()
    fnCutsceneMove("Ellie", 9.25, 18.50)
    fnCutsceneMove("Ellie", 12.75, 18.50)
    fnCutsceneMove("Ellie", 12.75, 34.50)
    fnCutsceneMove("Ellie", 11.25, 34.50)
    fnCutsceneFace("Ellie", 1, 0)
    fnCutsceneMove("Christine", 9.25, 18.50)
    fnCutsceneMove("Christine", 12.75, 18.50)
    fnCutsceneMove("Christine", 12.75, 32.50)
    fnCutsceneMove("Christine", 13.25, 32.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneMove("Tiffany", 9.25, 18.50)
    fnCutsceneMove("Tiffany", 12.75, 18.50)
    fnCutsceneMove("Tiffany", 12.75, 32.50)
    fnCutsceneMove("Tiffany", 12.25, 32.50)
    fnCutsceneFace("Tiffany", 0, 1)
    if(bIsSX399Present == true) then
        fnCutsceneMove("SX-399", 9.25, 18.50)
        fnCutsceneMove("SX-399", 12.75, 18.50)
        fnCutsceneMove("SX-399", 12.75, 31.50)
        fnCutsceneFace("SX-399", 0, 1)
    end
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Ellie:[VOICE|Ellie] She'll be right out.[P] Hey, boss![P] Visitors!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Jo", 17.25, 31.50)
    fnCutsceneMove("Jo", 17.25, 32.50)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Open Door", "DoorD") ]])
    fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutsceneMove("Jo", 17.25, 34.50)
    fnCutsceneMove("Jo", 14.25, 34.50)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Jo:[VOICE|RebelGolem] Yes?[P] What is it?[B][C]") ]])
    fnCutscene([[ Append("Ellie:[VOICE|Ellie] We have some distinguished guests.") ]])
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Jo", 13.25, 34.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Jo", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Jo", -1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Jo", 0, -1)
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutsceneFace("Jo", -1, 0)
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutsceneFace("Jo", 0, -1)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "RebelGolem", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Ellie", "Neutral") ]])
    fnCutscene([[ Append("Jo:[E|Neutral] We are really up a creek now...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] Really?[P] Why is that?[B][C]") ]])
    fnCutscene([[ Append("Jo:[E|Neutral] Oh, no reason.[P] Just the big cheeses showing up out of nowhere for an inspection when everything has gone wrong.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Christine, this is Captain Jo.[P] Former welding unit from Sector 34.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Nice to meet you.[P] I am - [P][CLEAR]") ]])
    fnCutscene([[ Append("Jo:[E|Neutral] Unit 771852.[P] We've seen you on the videographs.[P] Unit 2855 told me all about your -[P] abilities.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Great![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] So let's get to the brass tacks.[P] What's gone wrong?[B][C]") ]])
    if(bIsSX399Present == true) then
        fnCutscene([[ Append("SX-399:[E|Smirk] We're here to help, no matter the risk.[B][C]") ]])
    end
    fnCutscene([[ Append("Jo:[E|Neutral] Huh?[P] Oh.[P] Uh, no.[P] You can't help.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Please state your reasons, Captain.[B][C]") ]])
    fnCutscene([[ Append("Jo:[E|Neutral] Simple.[P] We lose Christine, and it'll be a massive morale hit.[B][C]") ]])
    fnCutscene([[ Append("Jo:[E|Neutral] Like it or not, you're very important.[P] When units see you on the RVDs, they have hope.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] If you will recall the videographs we filmed, Christine.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Yeah, the ones we did a month ago?[P] Where I tell everyone it'll be okay and to rise up and join the rebels?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smirk] If things are on schedule, those videographs are playing all across Regulus City right now.[B][C]") ]])
    fnCutscene([[ Append("Jo:[E|Neutral] They are.[P] Some of the golems here joined our company because of those.[B][C]") ]])
    fnCutscene([[ Append("Jo:[E|Neutral] Which is why we absolutely can't have you getting retired.[P] So, you'll need to find another exfiltration route.[P] This one is blocked.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Please fill us in on the situation, Captain.[P] Specifically, why your company is so far out of position.[B][C]") ]])
    fnCutscene([[ Append("Jo:[E|Neutral] Right, these are the facts as I understand them.[B][C]") ]])
    fnCutscene([[ Append("Jo:[E|Neutral] My company was assigned to capture and hold Transit Station Upsilon-C.[P] We did capture the station without resistance, but suffered a counterattack from security forces.[B][C]") ]])
    fnCutscene([[ Append("Jo:[E|Neutral] They were disorganized but outnumbered us.[P] We prioritized evacuating the civilians out along the track.[B][C]") ]])
    fnCutscene([[ Append("Jo:[E|Neutral] We made our way between stations, but a number of them had been collapsed.[P] I think the demolition crews were off-target.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Hmm.[P] I think the 6th Shock Company may have overcharged their explosives.[B][C]") ]])
    fnCutscene([[ Append("Jo:[E|Neutral] Either way, we got pretty far off course before we finally found a surface entrance, which led us to the loading bay on the far east side of this facility.[B][C]") ]])
    fnCutscene([[ Append("Jo:[E|Neutral] We moved in as an evacuation order went off.[P] Security presence was non-existent.[B][C]") ]])
    fnCutscene([[ Append("Jo:[E|Neutral] Then, we came under heavy fire from a security team turret emplacement.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] Oh no...[B][C]") ]])
    fnCutscene([[ Append("Jo:[E|Neutral] We took a lot of hits, but they did more damage to the building than to us.[P] Still, about half of my company is in need of repairs.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] But we can fix them, right?[B][C]") ]])
    fnCutscene([[ Append("Ellie:[E|Neutral] No, because not all of them are with us.[B][C]") ]])
    fnCutscene([[ Append("Jo:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] What happened?[B][C]") ]])
    fnCutscene([[ Append("Ellie:[E|Neutral] I was on the B-team when the shooting started.[P] We were on a bridge and badly exposed, no cover whatsoever.[P] We got split up.[B][C]") ]])
    fnCutscene([[ Append("Ellie:[E|Neutral] Some of us made it here and linked up with the rest of the company, and some us are still back there...[B][C]") ]])
    fnCutscene([[ Append("Jo:[E|Neutral] And we're way too beat up to make a move back there.[B][C]") ]])
    fnCutscene([[ Append("Ellie:[E|Neutral] It's...[P] my fault, Captain.[B][C]") ]])
    fnCutscene([[ Append("Jo:[E|Neutral] And beating yourself up about it is going to help how?[B][C]") ]])
    fnCutscene([[ Append("Ellie:[E|Neutral] ...[P] oof...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] All right.[P] So that's the situation.[P] We're on it.[B][C]") ]])
    fnCutscene([[ Append("Jo:[E|Neutral] Excuse me, did you not hear the part about the turret emplacement?[B][C]") ]])
    fnCutscene([[ Append("Jo:[E|Neutral] Maybe that wasn't enough.[P] They also blew up the bridge leading to the east side of the facility, and locked it down.[B][C]") ]])
    fnCutscene([[ Append("Jo:[E|Neutral] So you'd need an access card, and you'd need to jump a fifty-meter gap, [P]*and*[P] find the trapped units, assuming any of them are operational.[B][C]") ]])
    fnCutscene([[ Append("Jo:[E|Neutral] As I said, you're too valuable to do that.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] And the alternative is leaving them behind?[B][C]") ]])
    fnCutscene([[ Append("Jo:[E|Neutral] I'm not trading twenty units for five.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Both of you are correct.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Huh?[B][C]") ]])
    if(bIsSX399Present == true) then
        fnCutscene([[ Append("SX-399:[E|Smirk] Well, obviously.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Flirt] If we rescue these units, we're heroes.[P] That's a story that gets around.[B][C]") ]])
        fnCutscene([[ Append("Jo:[E|Neutral] And if you get shot?[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Smirk] Martyrs.[P] Double-or-nothing.[B][C]") ]])
        fnCutscene([[ Append("Jo:[E|Neutral] Tch.[P] You're far too optimistic about this.[B][C]") ]])
    else
        fnCutscene([[ Append("55:[E|Neutral] Rescuing downed units is a propaganda victory.[P] It is a story that will catch the attention of the masses.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] It will be particularly effective if it happens to be true.[B][C]") ]])
        fnCutscene([[ Append("Jo:[E|Neutral] And if you get shot and scrapped?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] A heroic death is often just as potent.[B][C]") ]])
        fnCutscene([[ Append("Jo:[E|Neutral] There's excellent logic behind every great folly.[B][C]") ]])
    end
    fnCutscene([[ Append("Jo:[E|Neutral] This is war.[P] We're going to take losses.[P] You have to accept that.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I am prepared to accept it.[P] I know we can't save everyone.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] But I wouldn't be doing this if I didn't believe it was the right thing to do.[P] Those units are damaged because of me, because of us.[B][C]") ]])
    fnCutscene([[ Append("Jo:[E|Neutral] This isn't [P]*just*[P] your revolution, Christine.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] It isn't, and you're right.[P] It belongs to everyone willing to take up arms.[B][C]") ]])
    fnCutscene([[ Append("Ellie:[E|Neutral] Boss, I think we should let them try.[B][C]") ]])
    fnCutscene([[ Append("Jo:[E|Neutral] As if we could stop them.[P] Some idiots are hell-bent on getting themselves retired.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Leading a rebellion is the task of idealists and lunatics.[B][C]") ]])
    fnCutscene([[ Append("Jo:[E|Neutral] ...[P] I suppose that's the kind of crazy that changes the world.[B][C]") ]])
    fnCutscene([[ Append("Jo:[E|Neutral] Commander 2855, make sure she survives the mission.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smirk] We've been through worse.[B][C]") ]])
    fnCutscene([[ Append("Jo:[E|Neutral] Since you're here, what are our orders?[P] We're obviously not capturing the transit station while stuck here.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Finish repairs and escort the civilians in your group to the Raiju Ranch northwest of here.[P] The security presence there is light.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] From there, move to Transit Station Omicron.[P] There is a group of civilian Lord Units there.[B][C]") ]])
    fnCutscene([[ Append("Jo:[E|Neutral] Take them prisoner?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Correct.[P] More importantly, keep them safe from other hostiles.[P] There are unidentified hostile creatures in the Biolabs.[B][C]") ]])
    fnCutscene([[ Append("Jo:[E|Neutral] Affirmative.[P] It will be some time before we're ready to move out.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Good.[P] Your company is doing well so far.[P] Keep it up.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] And show them Sector 34's best, Captain![B][C]") ]])
    fnCutscene([[ Append("Jo:[E|Neutral] Of course.[P] As you were, everyone.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Jo moves back to the console.
    fnCutsceneMove("Jo", 17.25, 34.50)
    fnCutsceneMove("Jo", 17.25, 31.50)
    fnCutsceneMove("Jo", 15.25, 31.50)
    fnCutsceneMove("Jo", 15.25, 31.00)
    
    --Party fold.
    fnAutoFoldParty()
    fnCutsceneBlocker()
    
    --Re-enable Ellie's collision flag.
    fnCutscene([[ TA_ChangeCollisionFlag("Ellie", true) ]])
    
end
