-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToDeltaI") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsDeltaI", "FORCEPOS:51.5x9.0x0")
    
elseif(sObjectName == "ToGeneticsB") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsGeneticsB", "FORCEPOS:4.5x27.0x0")
    
-- |[Objects]|
elseif(sObjectName == "SignA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Welcome to the Aquatic Genetics research lab![P] Please check your coat with the attendant -[P] things get wet here![P] Enjoy your stay!')") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "SignB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('These tanks house the large aquatic mammals under study.[P] There is an underwater passage they can swim through that connects to the next room.[P] Watch out -[P] the seals love to boop guests!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Water") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (An aquatic habitat that connects to the next room underwater.[P] The connection hatch is closed, so there's nothing in here right now.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "AquariumA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Some small tropical fish are darting here and there, nibbling on bits of food.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "AquariumB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (There are some small crabs walking to and fro on the bottom of the habitat.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "AquariumC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A sign on the aquarium says 'Currently being cleaned'.[P] There is a lobster in the tank, nibbling on the muck within.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "AquariumD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Some colorful tropical fish are swimming in circles.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "AquariumE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (I think I can see the eyes of an eel hidden in the shadowy recesses of the habitat.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "AquariumF") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This aquarium houses some corals.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "AquariumG") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (There is a robotic fish in here.[P] It flits about with a camera on it, recording the other fish without drawing attention to itself.[P] Stealthy!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A guest list, indicating whose coat belong to who.[P] An evacuation notice blinks on the top, and only one administrator is currently listed as present.[P] Looks like she's in the southern transit office.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The administrator's console.[P] The rebels have a program scanning it for tactical information.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A list of appointments for the administrator here.[P] They are all marked as cancelled.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Looks like the staff here all evacuated.[P] The rebels are using their quarters for repair work.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RVDA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Hey, it's a recording I made a month ago![P] 55 and I made it to encourage everyone.[P] I'm telling them that they need to keep calm and stay strong, and work together for the future.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RVDB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The RVD is powered off.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Oilmaker") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The rebels drank all the oil.[P] They used every flavour and even drank some unflavoured oil -[P] desperate times, and all that.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Computer") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Jo has repurposed the administrator's portable computer and has the building's schematics displayed along with targets and objectives.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Banner") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A banner depicting the rebel symbol -[P] a killphrase plate, broken in half.[P] Never shall we again be slaves.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "FizzyPop") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Fizzy Pop! is a fantastic way to deal with the stress of being shot at![P] Is there nothing it can't do?)") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
